var checkinSelezionaPostoOptions = {};

// document ready
$( document ).ready( function() {

    $('.checkin-seleziona-posto').foundation();

    checkinSelezionaPostoOptions.initFlightSelector();
    checkinSelezionaPostoOptions.initPlaneSectionSelector();
} );

checkinSelezionaPostoOptions.initPlaneSectionSelector = function() {
    /*$('.checkin-seleziona-posto .section-selector a').click(function() {
        $(this).addClass('is-active').parent().siblings().find('a').removeClass('is-active');
        return false;
    });*/
    $('.checkin-seleziona-posto .section-selector .imageRows__row').click(function() {

        // interazione sul selettore a lato con immagine aereoplanino di sfondo
        $(this).addClass('active').siblings().removeClass('active');
        $('.image img', $(this).closest('.imageRows')).attr('src', $(this).data('image'));

        // accensione del settore corrispondente
        var _this = this;
        $('.chooseSeat__passengerSeats').removeClass('is-active').filter(function() {
            return $(this).data('fila') == $(_this).data('fila');
        }).addClass('is-active');
    });

    // interazione da frecce sopra-sotto nel blocco del settore --> sposta il settore selezionato e modifica anche il settore a lato
    $('.seatmap-section-nav a').click(function() {
        // accensione del settore corrispondente
        var _this = this;
        $('.chooseSeat__passengerSeats').removeClass('is-active').filter(function() {
            console.log($(this).data('fila'), $(_this).data('fila'));
            return $(this).data('fila') == $(_this).data('fila');
        }).addClass('is-active');

        $('.checkin-seleziona-posto .section-selector .imageRows__row').removeClass('active').filter(function() {
            return $(this).data('fila') == $(_this).data('fila');
        }).addClass('active');

        return false;
    });
}

checkinSelezionaPostoOptions.initFlightSelector = function() {

    $('.seats-list li a').click(function() {

        var size = Foundation.MediaQuery.current;
        if (size == 'medium' || size == 'small') {
            $('body').addClass('is-reveal-open');
            $('.seat-configurator').addClass('reveal-mode');
        }

        return false;
    });

    $('.seat-configurator .close-button, .seat-configurator .checkin-button-wrap .button').not('.tooltip .close-button').click(function() {
        $('body').removeClass('is-reveal-open');
        $('.seat-configurator').removeClass('reveal-mode');
    });

    $(window).on('changed.zf.mediaquery', function(event, newSize, oldSize){
        if (newSize == 'large') {
            $('body').removeClass('is-reveal-open');
            $('.seat-configurator').removeClass('reveal-mode');
        }
    });
}