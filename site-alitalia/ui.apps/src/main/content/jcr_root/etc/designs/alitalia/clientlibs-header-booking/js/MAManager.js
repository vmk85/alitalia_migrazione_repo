var maManager = {};

$(document).ready(function() {
	/** sidebar mobile non loggato **/
	$('.sidebar-link').on('click', function () {
		$(this).toggleClass('open');
		$('.sidebar-nav-mob .menu').slideToggle();
		return false;
	});

	/** sidebar mobile non loggato - voce di menu **/
	$('.info-user__personal-area-toggle').on('click', function () {
		$(this).toggleClass('open');
		$('.info-user__personal-area-content').slideToggle(400, function(){
			verticalCenter();
		});
		verticalCenter();
		return false;
	});
});

// maManager.checkAccount = function() {
//     getUserMAInfo(function(data) {
//         if(data.errorCode == 0) {
//             saveUserMAToModel();
//             location.reload();
//         }
//     });
// }