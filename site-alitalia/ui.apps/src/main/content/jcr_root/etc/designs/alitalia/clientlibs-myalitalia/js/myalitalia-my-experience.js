var myAlitaliaFeedbackOptions = {};

// document ready
$( document ).ready( function() {

	$('.edit-feedback').hide();
	$('.new-feedback').hide();
	$('.specific-flight').hide();

	myAlitaliaFeedbackOptions.initRadioFeedback();

	myAlitaliaFeedbackOptions.initStepNavigation();

	// myAlitaliaFeedbackOptions.initRadioFlight();
} );

myAlitaliaFeedbackOptions.initRadioFeedback = function() {
	//$('.new-feedback').show();
	if($('.myexperience__form .radio-wrap input:checked').length) {
		$( this ).closest('.radio-wrap').addClass('checked');
	}
	$(document).on('click', '.myexperience__form .radio-wrap input', function() {
		$('.myexperience__form .radio-wrap input[name='+ $( this ).attr( 'name' ) + ']' ).closest('.radio-wrap').removeClass('checked');
		$(this).closest('.radio-wrap').addClass('checked');
		myAlitaliaFeedbackOptions.toggleForm( $( this ).attr( 'id' ) );
	});

};

myAlitaliaFeedbackOptions.toggleForm = function( $inputName ) {
	switch( $inputName ) {

    case 'switch-new--feedback':
			$('.edit-feedback').hide();
			$('.new-feedback').show();
      break;

    case 'switch-edit--feedback':
			$('.new-feedback').hide();
			$('.edit-feedback').show();
      break;

    case 'switch-flight-yes':
			$('.specific-flight').show();
      break;

    case 'switch-flight-no':
			$('.specific-flight').hide();
      break;

    default:
        break;
		}

		if( $inputName.slice( 0, 13 ) == 'switch-flight' ) {
			$( '.button-step-4' ).removeClass( 'hide' );
		}

};

myAlitaliaFeedbackOptions.initStepNavigation = function() {
	$('.step.step--disabled').find('.step__content').hide();

	$(".button-step-1").click(function() {
		// check campi step 1

		if (true) { // valore per forzare la validazione positiva, sostituire con controlli sui campi
			$(this).closest('.step').addClass('step--completed');
			$('.myexperience__form .step-2').removeClass('step--disabled');
			$('.step-2 .step__content').slideDown();
		} else {
			// show error feedback
		}
	});
	$(".button-step-2").click(function() {
		// check campi step 1 e 2

		if (true) { // valore per forzare la validazione positiva, sostituire con controlli sui campi
			$(this).closest('.step').addClass('step--completed');
			$('.myexperience__form .step-3').removeClass('step--disabled');
			$('.step-3 .step__content').slideDown();
		} else {
			// show error feedback
		}
	});
	$(".button-step-3").click(function() {
		// check campi step 1, 2 e 3

		if (true) { // valore per forzare la validazione positiva, sostituire con controlli sui campi
			$(this).closest('.step').addClass('step--completed');
			$('.myexperience__form .step-4').removeClass('step--disabled');
			$('.step-4 .step__content').slideDown();
		} else {
			// show error feedback
		}
	});
	$(".button-step-4").click(function() {
		// check campi step 1, 2, 3 e 4

		if (true) { // valore per forzare la validazione positiva, sostituire con controlli sui campi
			$(this).closest('.step').addClass('step--completed');
			$('.new-feedback, .section--select-feedback, .edit-feedback').hide();
			$('.form-feedback').show();
		} else {
			// show error feedback
		}
	});
};
