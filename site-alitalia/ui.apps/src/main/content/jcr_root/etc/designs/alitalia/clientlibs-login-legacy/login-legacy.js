/* global */

/**
 * Keeps legacy login init data.
 */
var alitaliaLegacyLoginData = {
	apBaseUrl: '',
	mmBaseUrl: ''
};


/* main functions */

/**
 * Should be invoked externally to receive legacy login initialization parameters.
 */
function initLegacyLogin(apBaseUrl, mmBaseUrl) {
	alitaliaLegacyLoginData.apBaseUrl = apBaseUrl;
	alitaliaLegacyLoginData.mmBaseUrl = mmBaseUrl;
	// setup a client context listener to automatically trigger legacy login
	if (window['registerLoginContextProfileCallback']) {
		window['registerLoginContextProfileCallback'](onLegacyLoginClientContextProfileInitialized);
	} else {
		console.error("Expected function registerLoginContextProfileCallback not defined.");
	}
}

/**
 * Should be invoked externally to perform legacy logout process.
 */
function performLegacyLogout(apBaseUrl, mmBaseUrl, successCallback, failureCallback) {
	alitaliaLegacyLoginManager.attemptLogout(apBaseUrl, mmBaseUrl, successCallback, failureCallback);
}


/* internals */

/**
 * Invoked internally when the client context store is ready,
 * in order to trigger legacy login attempt if required.
 */
function checkPostponedLegacyLogin(apBaseUrl, mmBaseUrl) {
	// (login context functions defined in login-context clientlibs)
	var legacyLoginStatus =	getLoginContextProfileProperty('legacyLoginStatus');
	if (isLoginContextAuthenticated()) {
		if (legacyLoginStatus == "" || legacyLoginStatus == null) {
			// authenticated in AEM, legacy login not yet attempted
			performLegacyMilleMigliaLogin(apBaseUrl, mmBaseUrl, legacyLoginSuccess, legacyLoginFailure);
		}
	} else {
		if (legacyLoginStatus == 'success') {
			// not authenticated in AEM, legacy logout not yet attempted
			performLegacyLogout(apBaseUrl, mmBaseUrl, legacyLogoutSuccess, legacyLogoutFailure);
		}
	}
}

/**
 * Manages functions and state about legacy login process.
 */
var alitaliaLegacyLoginManager = {
	state: {
		plainLoginToken: "",
		plainLoginAP: "",
		plainLoginMM: "",
		customerCode: "",
		pinCode: "",
		loginData: null,
		pendingJSONPCallback: null
	},
	log: function (item) {
	},
	clearState: function() {
		alitaliaLegacyLoginManager.log("Clearing data.");
		alitaliaLegacyLoginManager.state.successCallback = null;
		alitaliaLegacyLoginManager.state.failureCallback = null;
		alitaliaLegacyLoginManager.state.plainLoginToken = "";
		alitaliaLegacyLoginManager.state.plainLoginAP = "";
		alitaliaLegacyLoginManager.state.plainLoginMM = "";
		alitaliaLegacyLoginManager.state.plainLogoutAP = "";
		alitaliaLegacyLoginManager.state.plainLogoutMM = "";
		alitaliaLegacyLoginManager.state.customerCode = "";
		alitaliaLegacyLoginManager.state.pinCode = "";
		alitaliaLegacyLoginManager.state.loginData = null;
		alitaliaLegacyLoginManager.state.pendingJSONPCallback = null;
	},
	attemptLogin: function(apBaseUrl, mmBaseUrl, mmCode, mmPin, successCallback, failureCallback) {
		alitaliaLegacyLoginManager.log("Starting legacy login process...");
		alitaliaLegacyLoginManager.clearState();
		alitaliaLegacyLoginManager.state.successCallback = successCallback;
		alitaliaLegacyLoginManager.state.failureCallback = failureCallback;
		alitaliaLegacyLoginManager.state.plainLoginToken = apBaseUrl + "/Login/gettoken";
		alitaliaLegacyLoginManager.state.plainLoginAP = apBaseUrl + "/Login/MilleMigliaLogin";
		alitaliaLegacyLoginManager.state.plainLoginMM = mmBaseUrl + "/Login.aspx?op=mmLogin";
		alitaliaLegacyLoginManager.state.customerCode = mmCode;
		alitaliaLegacyLoginManager.state.pinCode = mmPin;
		if (alitaliaLegacyLoginManager.state.customerCode.length > 0 &&
				alitaliaLegacyLoginManager.state.pinCode.length > 0) {
			if ($("#hiddenExternalLoginForm").length == 0) {
				alitaliaLegacyLoginManager.log("Creating hidden iframe and form for external gettoken submit...");
				var frame = document.createElement("iframe");
				frame.setAttribute("id", "hiddenLoginFrame");
				frame.setAttribute("name", "hiddenLoginFrame");
				frame.setAttribute("style", "display:none");
				var form = document.createElement("form");
				form.setAttribute("id", "hiddenExternalLoginForm");
				form.setAttribute("action", alitaliaLegacyLoginManager.state.plainLoginToken);
				form.setAttribute("method", "post");
				form.setAttribute("target", "hiddenLoginFrame");
				var hiddenInput1 = document.createElement("input");
				hiddenInput1.setAttribute("type", "hidden");
				hiddenInput1.setAttribute("name", "customercode");
				var hiddenInput2 = document.createElement("input");
				hiddenInput2.setAttribute("type", "hidden");
				hiddenInput2.setAttribute("name", "pincode");
				form.appendChild(hiddenInput1);
				form.appendChild(hiddenInput2);
				document.getElementsByTagName("body")[0].appendChild(frame);
				document.getElementsByTagName("body")[0].appendChild(form);
				if (frame.attachEvent) {
					frame.attachEvent('onload', alitaliaLegacyLoginManager.getTokenCallback);
				} else if (frame.addEventListener) {
					frame.addEventListener('load', alitaliaLegacyLoginManager.getTokenCallback, false);
				} else {
					frame.onload = alitaliaLegacyLoginManager.getTokenCallback;
				}
			}
			$("#hiddenExternalLoginForm input[name=customercode]").val(alitaliaLegacyLoginManager.state.customerCode);
			$("#hiddenExternalLoginForm input[name=pincode]").val(alitaliaLegacyLoginManager.state.pinCode);
			document.getElementById('hiddenExternalLoginForm').submit();
			alitaliaLegacyLoginManager.log("Submitting hidden external login form to gettoken...");
		} else {
			alitaliaLegacyLoginManager.performFailureCallback("", "Missing code or pin.");
		}
	},
	getTokenCallback: function () {
		alitaliaLegacyLoginManager.log("Submit for gettoken completed.");
		alitaliaLegacyLoginManager.performPlainLoginPhase1();
	},
	performPlainLoginPhase1: function () {
		alitaliaLegacyLoginManager.log("Performing PLAIN login phase 1 (call to AreaPersonale)...");
		alitaliaLegacyLoginManager.state.pendingJSONPCallback = alitaliaLegacyLoginManager.plainLoginPhase1Callback;
		$.ajax({
			url: alitaliaLegacyLoginManager.state.plainLoginAP,
			dataType: "jsonp",
			jsonpCallback: "_jqjsp"
		});
	},
	plainLoginPhase1Callback: function (data) {
		if (data.Status == "KO") {
			alitaliaLegacyLoginManager.log('PLAIN Login phase 1 FAILURE.');
			alitaliaLegacyLoginManager.performFailureCallback(data.ErrorCode, data.Msg);
			return;
		}
		alitaliaLegacyLoginManager.log('PLAIN Login phase 1 success.');
		alitaliaLegacyLoginManager.log(data);
		alitaliaLegacyLoginManager.state.loginData = data;
		alitaliaLegacyLoginManager.performLoginPhase2();
	},
	performLoginPhase2: function () {
		alitaliaLegacyLoginManager.log("Performing PLAIN login phase 2 (call to MilleMiglia)...");
		alitaliaLegacyLoginManager.state.pendingJSONPCallback = alitaliaLegacyLoginManager.plainLoginPhase2Callback;
		$.ajax({
			url: alitaliaLegacyLoginManager.state.plainLoginMM + "&CryptedData=" +
				encodeURIComponent(alitaliaLegacyLoginManager.state.loginData.Token),
			dataType: "jsonp",
			jsonp: "jsoncallback",
			jsonpCallback: "_jqjsp"
		});
	},
	plainLoginPhase2Callback: function (data) {
		if (data.Status == "KO") {
			alitaliaLegacyLoginManager.log('PLAIN Login phase 2 FAILURE.');
			alitaliaLegacyLoginManager.performFailureCallback(data.ErrorCode, data.ErrorMessage);
			return;
		}
		alitaliaLegacyLoginManager.log('PLAIN Login phase 2 success.');
		alitaliaLegacyLoginManager.log(data);
		alitaliaLegacyLoginManager.legacyLoginComplete();
	},
	legacyLoginComplete: function() {
		alitaliaLegacyLoginManager.log("Legacy login process completed.");
		alitaliaLegacyLoginManager.performSuccessCallback();
	},
	attemptLogout: function (apBaseUrl, mmBaseUrl, successCallback, failureCallback) {
		alitaliaLegacyLoginManager.log("Starting legacy logout process...");
		alitaliaLegacyLoginManager.clearState();
		alitaliaLegacyLoginManager.state.successCallback = successCallback;
		alitaliaLegacyLoginManager.state.failureCallback = failureCallback;
		alitaliaLegacyLoginManager.state.plainLogoutMM = mmBaseUrl + "/Logout.aspx";
		alitaliaLegacyLoginManager.state.plainLogoutAP = apBaseUrl + "/Login/MilleMigliaLogout";
		alitaliaLegacyLoginManager.performPlainLogoutPhase1();
	},
	performPlainLogoutPhase1: function () {
		alitaliaLegacyLoginManager.log("Performing PLAIN logout phase 1 (call to MM)...");
		alitaliaLegacyLoginManager.state.pendingJSONPCallback = alitaliaLegacyLoginManager.plainLogoutPhase1Callback;
		$.ajax({
			url: alitaliaLegacyLoginManager.state.plainLogoutMM,
			dataType: "jsonp",
			jsonp: "jsoncallback",
			jsonpCallback: "_jqjsp"
		});
	},
	plainLogoutPhase1Callback: function (data) {
		if (data.Status == "KO") {
			alitaliaLegacyLoginManager.log('PLAIN Logout phase 1 FAILURE.');
			alitaliaLegacyLoginManager.performFailureCallback(data.ErrorCode, data.Msg);
			return;
		}
		alitaliaLegacyLoginManager.log('PLAIN Logout phase 1 success.');
		alitaliaLegacyLoginManager.log(data);
		alitaliaLegacyLoginManager.performPlainLogoutPhase2();
	},
	performPlainLogoutPhase2: function () {
		alitaliaLegacyLoginManager.log("Performing PLAIN logout phase 2 (call to AP)...");
		alitaliaLegacyLoginManager.state.pendingJSONPCallback = alitaliaLegacyLoginManager.plainLogoutPhase2Callback;
		$.ajax({
			url: alitaliaLegacyLoginManager.state.plainLogoutAP,
			dataType: "jsonp",
			jsonpCallback: "_jqjsp"
		});
	},
	plainLogoutPhase2Callback: function (data) {
		if (data.Status == "KO") {
			alitaliaLegacyLoginManager.log('PLAIN Logout phase 2 FAILURE.');
			alitaliaLegacyLoginManager.performFailureCallback(data.ErrorCode, data.Msg);
			return;
		}
		alitaliaLegacyLoginManager.log('PLAIN Logout phase 2 success.');
		alitaliaLegacyLoginManager.log(data);
		alitaliaLegacyLoginManager.legacyLogoutComplete();
	},
	legacyLogoutComplete: function() {
		alitaliaLegacyLoginManager.log("Legacy logout process completed.");
		alitaliaLegacyLoginManager.performSuccessCallback();
	},
	performSuccessCallback: function() {
		alitaliaLegacyLoginManager.log("Performing success callback...");
		if (alitaliaLegacyLoginManager.state.successCallback) {
			alitaliaLegacyLoginManager.state.successCallback(
					alitaliaLegacyLoginManager.state.customerCode, alitaliaLegacyLoginManager.state.pinCode);
		}
	},
	performFailureCallback: function(code, message) {
		alitaliaLegacyLoginManager.log("Performing failure callback...");
		if (alitaliaLegacyLoginManager.state.failureCallback) {
			alitaliaLegacyLoginManager.state.failureCallback(code, message);
		}
	}
};

/**
 * Common JSONP callback function required for call to legacy REST services.
 */
function _jqjsp(data) {
	var callbackFn = alitaliaLegacyLoginManager.state.pendingJSONPCallback;
	if (callbackFn != null) {
		alitaliaLegacyLoginManager.state.pendingJSONPCallback = null;
		callbackFn(data);
	}
}

/**
 * Starts the legacy login process. 
 */
function performLegacyMilleMigliaLogin(apBaseUrl, mmBaseUrl, 
		successCallback, failureCallback) {
	retrieveCurrentUserMMCredentials( 
			function (customerNumber, customerPinCode) {
				alitaliaLegacyLoginManager.attemptLogin(apBaseUrl, mmBaseUrl, 
						customerNumber, customerPinCode,
						successCallback, failureCallback);
			},
			function (errorCode, errorDescription) {
				failureCallback(errorCode, errorDescription);
			});
}

/**
 * Retrieves the credentials of the current user using a REST servlet.
 */
function retrieveCurrentUserMMCredentials(successCallback, failureCallback) {
	$.ajax({
		url: getServiceUrl('getmillemigliacredentials'),
		method: 'POST'
	})
	.done(function(data) {
		if (data.successful == true) {
			successCallback(data.mmCode, data.mmPin);
		} else {
			failureCallback(data.errorCode, data.errorDescription);
		}
	})
	.fail(function() { 
		failureCallback("", "Failed ajax call - get millemiglia credentials");
	});
}

/**
 * Internal callback to manage a successful legacy login.
 */
function legacyLoginSuccess() {
	setLoginContextProfileProperty('legacyLoginStatus', 'success')
}

/**
 * Internal callback to manage a failed legacy login.
 */
function legacyLoginFailure(errorCode, errorDescription) {
	console.error("Legacy login problem: " + errorCode + ", " + errorDescription);
	setLoginContextProfileProperty('legacyLoginStatus', 'failure');
}

/**
 * Internal callback to manage a successful legacy logout.
 */
function legacyLogoutSuccess() {
	setLoginContextProfileProperty('legacyLoginStatus', '');
}

/**
 * Internal callback to manage a failed legacy login.
 */
function legacyLogoutFailure(errorCode, errorDescription) {
	console.error("Failed legacy logout: " + errorCode + ", " + errorDescription);
	setLoginContextProfileProperty('legacyLoginStatus', 'logoutFailure');
}


/* event handlers */

/**
 * Event handlers for client context profile store.
 */
function onLegacyLoginClientContextProfileInitialized() {
	checkPostponedLegacyLogin(alitaliaLegacyLoginData.apBaseUrl, alitaliaLegacyLoginData.mmBaseUrl);
}
