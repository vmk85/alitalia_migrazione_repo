var lmn = lmn || {};
lmn.Cannonball = function (a) {
    this.cannonball = a.cannonball;
};
lmn.Cannonball.prototype.init = function () {
    var f = this.cannonball.find(".more-options-container"), b = this.cannonball.find(".btn-details"), d = this.cannonball.find(".guest-details-container"), a = this, e, c;
    this.cannonball.on("click", ".btn-details", function (g) {
        g.preventDefault();
        a.slideEvents(this);
        $(".btn-details").focus();
        d.fadeToggle(100);
        c = $(this);
        c.toggleClass("icon-arrow_down icon-arrow_up");
        e = c.parent();
        $(e).toggleClass("active")
    });
};
lmn.Cannonball.prototype.slideEvents = function (a) {
    $("html, body").show()//.animate({ scrollTop: ($(a).offset().top - 40) }, 300)
};
var lmn = (function (a) {
    a.selectCannonball = {
        start: function () {
            var c = $("head"), f = $(".mod-cannonball"), b = f.find(".cannonball-img"), e = f.find(".cannonball-img[data-href]"), d = this;
            lmn.flags = { dp: false, hfhdp: false, hotels: false, flights: false, euroStar: false, train: false, theatre: false, theatreNew: false, holidays: false, spa: false, restaurants: false, experiences: false, villas: false, cars: false };
            lmn.cID = null;
            lmn.locale = c.attr("data-locale");
            lmn.lang = c.attr("data-lang");
            lmn.sitename = c.attr("data-site-name");
            this.setActiveTab(f);
            lmn.cID = f.find(".tab-pane.active").attr("id");
            this.initializeSearchForm();
            $("a[data-cannonball]").on("click", function () {
                lmn.cID = $(this).data("cannonball");
                if (lmn.calendar) {
                    lmn.calendar.removeCalendar()
                } $(".mod-cannonball").off("setUpCalendarFilters");
                d.initializeSearchForm();
                d.setCannonballTitle(f, lmn.cID)
            });
            if (e.length) {
                e.click(function (g) {
                    if (/cannonball-img/.test(g.target.className) || g.target.className === "cannonball-content") {
                        window.location = b.data("href")
                    }
                })
            }
        }, initializeSearchForm: function () {
            switch (lmn.cID) {
                case "flights-cannonball": lmn.selectCannonball.activeForm = "flights";
                    lmn.createFlightsCannonball();
                    break;
            }
        }, setActiveTab: function (d) {
            var e = d.data("active-form"), b, c;
            if (e && d.find(".tab-pane#" + e).length > 0) {
                b = d.find('.nav-tabs li a[data-cannonball="' + e + '"]').parent();
                c = d.find(".tab-pane#" + e)
            } else {
                b = d.find(".nav-tabs li:first");
                c = d.find(".tab-pane:first");
                e = b.find("a").attr("data-cannonball")
            } b.addClass("active");
            c.addClass("in active");
            this.setCannonballTitle(d, e)
        }, setCannonballTitle: function (c, d) {
            var b = c.find(".cannonball-header." + d + "-header");
            c.find(".cannonball-header").removeClass("active");
            if (b[0]) {
                b.addClass("active")
            } else {
                c.find(".cannonball-header.main").addClass("active")
            }
        }
    };
    return a
}(lmn || {}));
var lmn = (function (a) {
    a.createFlightsCannonball = function () {
        var b = { id: "flights", cannonball: $(".flights-cannonball"), localDatabase: "brgFlightsPopulate", localDatabaseLife: 7, templatePaths: ["#flights-room-guest-details"], calendarContainer: ".flights-calendar", selector: "flights-cannonball", configId: "S72722479", maxRooms: 4, maxNumberDays: 353, defaultTravellers: [1, 0, 0], dateLead: 3, dateDuration: 10, total: 0, template: "", track: { max: 9, roomCount: 0, numberAdults: 0, numberChildren: 0, numberInfants: 0 }, autocompleteParams: { "flights-search-from": {}, "flights-search-to": {} } };
        if (!lmn.flags.flights) {
            lmn.flightsCannonball = new lmn.Cannonball(b);
            lmn.flightsCannonball.init()
        } $(".mod-cannonball").on("setUpCalendarFilters", function () {
            lmn.flightsCannonball.calendarFilters()
        });
        lmn.flags.flights = true
    };
    return a
}(lmn || {}));
$(function () {
    lmn.selectCannonball.start();
});

$(document).mouseup(function (e) {
    var container = $(".guest-details-container");
    var click = $(".apt-horiz");
    var click2 = $(".scroll");
    if (!click.is(e.target)  // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
        $(".btn-details").removeClass("icon-arrow_down icon-arrow_up");
    }

});

