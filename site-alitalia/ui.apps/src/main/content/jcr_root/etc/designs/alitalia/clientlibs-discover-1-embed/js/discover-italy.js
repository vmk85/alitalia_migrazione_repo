var discoverItalyOptions = {};
// document ready
$( document ).ready( function() {
	discoverItalyOptions.initBgImage();
	verticalCenter();
	
	$(window).on('load resize', verticalCenter);
    $( '.discover-1' ).on( 'click', function(e) {
        e.preventDefault();
        e.stopPropagation();
		var discoverItalyLink = ($( this ).find( 'a' ).attr( 'href' ));
		window.open(discoverItalyLink, '_blank');
        return;
	});
});

discoverItalyOptions.initBgImage = function() {
	$( '.discover-italy-wrap' ).foundation();
}
