/**
 * handle counter
 */
; (function () {
    'use strict';
    $.fn.handleCounterOverlay = function (options) {
        var $input,
            $btnMinus,
            $btnPlugs,
            $btnPlugsOther,
            minimum,
            maximize,
            writable,
            onChange,
            onMinimum,
            onMaximize;
        var $handleCounterOverlay = this
        $btnMinus = $handleCounterOverlay.find('.h-font-minus');
        $input = $handleCounterOverlay.find('input')
        $btnPlugs = $handleCounterOverlay.find('.h-font-plus');
        $btnPlugsOther = $handleCounterOverlay.find('.h-font-plus-other');
        var defaultOpts = {
            writable: true,
            minimum: 1,
            maximize: null,
            onChange: function () { },
            onMinimum: function () { },
            onMaximize: function () { }
        }
        var settings = $.extend({}, defaultOpts, options)
        minimum = settings.minimum
        maximize = settings.maximize
        writable = settings.writable
        onChange = settings.onChange
        onMinimum = settings.onMinimum
        onMaximize = settings.onMaximize
        if (!$.isNumeric(minimum)) {
            minimum = defaultOpts.minimum
        }
        if (!$.isNumeric(maximize)) {
            maximize = defaultOpts.maximize
        }
        var inputVal = $input.val()
        if (isNaN(parseInt(inputVal))) {
            inputVal = $input.val(0).val()
        }
        if (!writable) {
            $input.prop('disabled', true)
        }

        changeVal(inputVal)
        $input.val(inputVal)
        $btnMinus.on('touchstart', function () {
            if (!$btnMinus.hasClass("locked")) {
                var num = parseInt($input.val())
                if (num > minimum) {
                    $input.val(num - 1)
                    changeVal(num - 1)
                }
            }
        })
        $btnPlugs.on('touchstart', function () {
            var num = parseInt($input.val())
            var a = $('#adultOverlay').val();
            var maximize = a;
            if (maximize == null || num < maximize) {
                $input.val(num + 1)
                changeVal(num + 1)
            }
        })
        $btnPlugsOther.on('touchstart', function () {
            var ya = $('#youngAdultOverlay').val();
            var c = $('#childOverlay').val();
            var a = $('#adultOverlay').val();
            var num = parseInt($input.val())
            var tot = parseInt(ya) + parseInt(c) + parseInt(a)
            if (maximize == null || tot < maximize) {
                $input.val(num + 1)
                changeVal(tot + 1)
            }
        })
        var keyUpTime
        $input.keyup(function () {
            clearTimeout(keyUpTime)
            keyUpTime = setTimeout(function () {
                var num = $input.val()
                if (num == '') {
                    num = minimum
                    $input.val(minimum)
                }
                var reg = new RegExp("^[\\d]*$")
                if (isNaN(parseInt(num)) || !reg.test(num)) {
                    $input.val($input.data('num'))
                    changeVal($input.data('num'))
                } else if (num < minimum) {
                    $input.val(minimum)
                    changeVal(minimum)
                } else if (maximize != null && num > maximize) {
                    $input.val(maximize)
                    changeVal(maximize)
                } else {
                    changeVal(num)
                }
            }, 300)
        })
        $input.focus(function () {
            var num = $input.val()
            if (num == 0) $input.select()
        })

        function changeVal(num) {
            $input.data('num', num)

            if (num <= minimum) {
                $btnMinus.addClass('locked');
                onMinimum.call(this, num)
            }
            else {
                $btnMinus.removeClass('locked');
            }

            if (maximize != null && num >= maximize) {
                $btnPlugs.addClass('locked');
                onMaximize.call(this, num)
            }
            else {
                $btnPlugs.removeClass('locked');
            }

            UpdatePassengers();
            DisableEnableButton($input, num);
            onChange.call(this, num)
        }

        function UpdatePassengers() {
            var a = $('#adultOverlay').val();
            var ya = $('#youngAdultOverlay').val();
            var c = $('#childOverlay').val();
            var e = $('#infantOverlay').val();
            var tot = parseInt(a) + parseInt(ya) + parseInt(c) + parseInt(e);
            $(".passengers-horiz").text(tot);
            var totNoAdult = parseInt(ya) + parseInt(c) + parseInt(e);

            if(tot > 1){

                if(totNoAdult > 0){
					$("#aptOverlay").text(passengerstranslation);
                }else{
					$("#aptOverlay").text(adultstranslation);
                }

            }
            else {
            	$("#aptOverlay").text(adulttranslation);
            }
        }
        function DisableEnableButton(input, num) {

            var a = $('#adultOverlay').val();
            var ya = $('#youngAdultOverlay').val();
            var c = $('#childOverlay').val();
            var e = $('#infantOverlay').val();

            var tot = parseInt(ya) + parseInt(c) + parseInt(a)

            if (tot == 7) {
                $('.h-font-plus-other').addClass('locked');
            }
            else {
                $('.h-font-plus-other').removeClass('locked');

            }

            if (a == e) {
                $('#btnAdultOverlay').addClass('locked');
                $('.h-font-plus').addClass('locked');
            }
            else {
                if (a != 1) {
                    $('#btnAdultOverlay').removeClass('locked');
                    $('.h-font-plus').removeClass('locked');
                }
            }
        }
        return $handleCounterOverlay
    };
})(jQuery)