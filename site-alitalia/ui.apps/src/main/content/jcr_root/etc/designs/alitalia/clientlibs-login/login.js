
/* global */

/**
 * Keeps info about current status of login process.
 */
var alitaliaLoginData = {
	busy: false,
	rememberMe: '0',
	targetPage: null,
	unexpectedErrorCallback: null,
	loginFailureCallback: null,
	loginUserLockedCallback: null
};

var SSW_REDEMPTION_REDIRECT_PAR_NAME = "SSW_RED";
var SSW_REDEMPTION_PARAMETERS = "SSW_RED_PARAMS";

/* main functions */

/**
 * Initial function externally invoked when a login process should starts
 * with MilleMiglia credentials.
 */
function startLoginWithMilleMigliaCredentials(mmCode, mmPin, rememberMe, 
		unexpectedErrorCallback, loginFailureCallback, targetPage, loginUserLockedCallback, isHomePage, selector) {
	if (alitaliaLoginData.busy) {
		console.error("A login/logout attempt is already in progres");
		return false;
	}
	toggleLoginOverlay(true);
	alitaliaLoginData.busy = true;
	alitaliaLoginData.rememberMe = (rememberMe == true ? '1' : '0');
	if (targetPage) {
		alitaliaLoginData.targetPage = targetPage;
	}
	alitaliaLoginData.unexpectedErrorCallback = unexpectedErrorCallback;
	alitaliaLoginData.loginFailureCallback = loginFailureCallback;
	alitaliaLoginData.loginUserLockedCallback = loginUserLockedCallback
	/*performMilleMigliaLogin(mmCode, mmPin,
			alitaliaLoginData.rememberMe, alitaliaLoginData.targetPage);*/
	checkMilleMigliaLogin(mmCode, mmPin, isHomePage, selector);
	return true;
}

/**
 * Initial function externally invoked when a login process should starts
 * with MilleMiglia credentials.
 */
function startLoginSAWithMilleMigliaCredentials(mmCode, mmPassword, rememberMe,
                                              unexpectedErrorCallback, loginFailureCallback, targetPage, loginUserLockedCallback, isHomePage, selector) {
    if (alitaliaLoginData.busy) {
        console.error("A login/logout attempt is already in progres");
        return false;
    }
    toggleLoginSAOverlay(true);
    toggleLoginOverlay(true);
    alitaliaLoginData.busy = true;
    alitaliaLoginData.rememberMe = (rememberMe == true ? '1' : '0');
    if (targetPage) {
        alitaliaLoginData.targetPage = targetPage;
    }
    alitaliaLoginData.unexpectedErrorCallback = unexpectedErrorCallback;
    alitaliaLoginData.loginFailureCallback = loginFailureCallback;
    alitaliaLoginData.loginUserLockedCallback = loginUserLockedCallback
    /*performMilleMigliaLogin(mmCode, mmPin,
            alitaliaLoginData.rememberMe, alitaliaLoginData.targetPage);*/
    checkMilleMigliaLoginSA(mmCode, mmPassword, isHomePage, selector);
    return true;
}


/**
 * Initial function invoked when a login process starts with Gigya credentials.
 */
function startLoginWithGigyaCredentials(gigyaId, gigyaSignature, gigyaProvider, 
		gigyaUserFirstName, gigyaUserLastName, gigyaUserThumbnailURL, rememberMe,
		unexpectedErrorCallback, loginFailureCallback, targetPage) {
	if (alitaliaLoginData.busy) {
		console.error("A login/logout attempt is already in progres");
		return false;
	}
	toggleLoginOverlay(true);
	alitaliaLoginData.busy = true;
	alitaliaLoginData.rememberMe = false;
	if (targetPage) {
		alitaliaLoginData.targetPage = targetPage;
	}
	alitaliaLoginData.unexpectedErrorCallback = unexpectedErrorCallback;
	alitaliaLoginData.loginFailureCallback = loginFailureCallback;
	checkSocialLogin(gigyaId, gigyaSignature, gigyaProvider, 
			gigyaUserFirstName, gigyaUserLastName, gigyaUserThumbnailURL,
			targetPage);
	return true;
}

/**
 * Function invoked to continue a social login with the association between
 * a Gigya id and MilleMiglia credentials.
 */
function continueSocialLoginWithMilleMigliaCredentials(gigyaId, gigyaSignature,
		mmCode, mmPin, unexpectedErrorCallback, loginFailureCallback) {
	$("#form-set-social-account input[name=gigyaId]").val(gigyaId);
	$("#form-set-social-account input[name=gigyaSignature]").val(gigyaSignature);
	$("#form-set-social-account input[name=mmCode]").val(mmCode);
	$("#form-set-social-account input[name=mmPin]").val(mmPin);
	if (alitaliaLoginData.busy) {
		console.error("A login/logout attempt is already in progres");
		return false;
	}
	toggleLoginOverlay(true);
	alitaliaLoginData.busy = true;
	alitaliaLoginData.rememberMe = false;
	alitaliaLoginData.unexpectedErrorCallback = unexpectedErrorCallback;
	alitaliaLoginData.loginFailureCallback = loginFailureCallback;
	performSubmit("setsocialaccount", "#form-set-social-account", 
			successSetSocialAccount, failureSetSocialAccount);
}

/**
 * Function invoked to start a logout process.
 */
function startLogout() {
	if (alitaliaLoginData.busy) {
		console.error("A login/logout attempt is already in progress.");
		return false;
	}
	toggleLoginOverlay(true);
	alitaliaLoginData.busy = true;
	alitaliaLoginData.rememberMe = false;
	alitaliaLoginData.unexpectedErrorCallback = null;
	alitaliaLoginData.loginFailureCallback = null;
	// first step, try to social logout
	performSocialLogout();
}


/* overlay management */

function toggleLoginOverlay(active) {
	if (active) {
		var overlayLoading = document.createElement("div");
		$(overlayLoading).attr("id", "loginOverlayLoading");
		$(overlayLoading).attr("class", "overlayLoading");
		$(".mainWrapper").append(overlayLoading);
	} else {
		$("#loginOverlayLoading").remove();
	}
}

/* overlay management */

function toggleLoginSAOverlay(active) {
    if (active) {
        var overlayLoading = document.createElement("div");
        $(overlayLoading).attr("id", "loginOverlayLoading");
        $(overlayLoading).attr("class", "overlayLoading");
        $(".mainWrapper").append(overlayLoading);
    } else {
        $("#loginOverlayLoading").remove();
    }
}


/* internal check millemiglia login functions */

/**
 * Start preliminary verification of MilleMiglia credentials using a REST servlet.
 */
function checkMilleMigliaLogin(mmCode, mmPin, isHomePage, selector) {
	$("#form-check-millemiglia-login input[name=mmCode]").val(mmCode);
	$("#form-check-millemiglia-login input[name=mmPin]").val(mmPin);
	performSubmit("checkmillemiglialogin", "#form-check-millemiglia-login", 
			function(data) {
				successCheckMilleMigliaLogin(data,isHomePage,selector);
			} 
			,failureCheckMilleMigliaLogin
	);
}

/**
 * Start preliminary verification of MilleMiglia credentials using a REST servlet.
 */
function checkMilleMigliaLoginSA(mmCode, mmPassword, isHomePage, selector) {
    $("#form-check-millemiglia-login input[name=mmCode]").val(mmCode);
    $("#form-check-millemiglia-login input[name=mmPin]").val(mmPassword);
    performSubmit("checkCredenzialiMillemiglia", "#form-check-millemiglia-login",
        function(data) {
            successCheckMilleMigliaLoginSA(data,isHomePage,selector);
        }
        ,failureCheckMilleMigliaLoginSA
    );
}

/**
 * Callback for verification of preliminary MilleMiglia credentials check.
 */
function successCheckMilleMigliaLoginSA(data, isHomePage, selector) {
	if (data.successful == true) {
		// credentials check passed, continue with AEM login
		performMilleMigliaLoginSA(data.mmCode, data.mmPin,
				alitaliaLoginData.rememberMe, alitaliaLoginData.targetPage);
	} else {
		if (data.isLocked) {
			//SHOW LOCKED MESSAGE
            displayLoginSAUserLockedError();
		} else {
			// credentials check not passed, show user error message
			displayLoginSAFailureError(data.errorCode, data.errorDescription);
		}
	}
}

function showCaptchaLogin(enabled,isHomePage,selector) {
	if (isHomePage) {
//		if ($("#captcha_checkin").length > 0) {
//			invokeShowCaptcha("captcha_checkin",enabled);
//		}
//		if ($("#captcha_mmb").length > 0) {
//			invokeShowCaptcha("captcha_mmb",enabled);
//		}
		if ($("#captcha_header").length > 0) {
			invokeShowCaptcha("captcha_header",enabled);
		}
		if ($("#captcha_login_spacialpages").length > 0) {
			invokeShowCaptcha("captcha_login_spacialpages",enabled);
		}
//		if ($("#captcha_cash_miles").length > 0) {
//			invokeShowCaptcha("captcha_cash_miles",enabled);
//		}
		
	} else {
		invokeShowCaptcha(selector,enabled);
	}
}


/**
 * Callback for failures trying to verify MilleMiglia credentials.
 */
function failureCheckMilleMigliaLogin() {
	console.error("Failed ajax call - check millemiglia login");
	displayGenericLoginFailureError();
}

/**
 * Callback for failures trying to verify MilleMiglia credentials.
 */
function failureCheckMilleMigliaLoginSA() {
    console.error("Failed ajax call - check millemiglia login");
    displayGenericLoginSAFailureError();
}


/* internal check social login functions */

/**
 * Start preliminary verification of Gigya identifier using a REST servlet.
 */
function checkSocialLogin(gigyaId, gigyaSignature, gigyaProvider, 
		gigyaUserFirstName, gigyaUserLastName, gigyaUserThumbnailURL) {
	$("#form-check-social-login input[name=gigyaId]").val(gigyaId);
	$("#form-check-social-login input[name=gigyaSignature]").val(gigyaSignature);
	$("#form-check-social-login input[name=gigyaProvider]").val(gigyaProvider);
	$("#form-check-social-login input[name=gigyaUserFirstName]").val(gigyaUserFirstName);
	$("#form-check-social-login input[name=gigyaUserLastName]").val(gigyaUserLastName);
	$("#form-check-social-login input[name=gigyaUserThumbnailURL]").val(gigyaUserThumbnailURL);
	performSubmit("checksociallogin", "#form-check-social-login", 
			successCheckSocialLogin, failureCheckSocialLogin);
}

/**
 * Callback for verification of Gigya identifier.
 */
function successCheckSocialLogin(data) {
	if (data.successful == true) {
		if (data.mmCode && data.mmCode != "" && data.mmCode != null) {
			// MilleMiglia user found, continue with AEM login
			performMilleMigliaLogin(data.mmCode, data.mmPin,
					alitaliaLoginData.rememberMe, alitaliaLoginData.targetPage);
		} else {
			// MilleMiglia user not found, navigate to account association page
			if (data.setSocialAccountRedirectUrl && 
					data.setSocialAccountRedirectUrl != null && 
					data.setSocialAccountRedirectUrl != "") {
				window.location.href = data.setSocialAccountRedirectUrl;
			} else {
				displayLoginFailureError("", "Invalid user account association page");
			}
		}
	} else {
		// credentials check not passed, show user error message
		displayLoginFailureError(data.errorCode, data.errorMessage);
	}
}

/**
 * Callback for failures trying to verify Gigya identifier.
 */
function failureCheckSocialLogin() {
	console.error("Failed ajax call - check social login");
	displayGenericLoginFailureError();
}


/* internal set social account functions */

/**
 * Callback for association of Gigya identifier to MilleMiglia credentials.
 */
function successSetSocialAccount(data) {
	if (data.successful == true) {
		performMilleMigliaLogin(data.mmCode, data.mmPin);
	} else {
		displayLoginFailureError(data.errorCode, data.errorMessage);
	}
}

/**
 * Callback for failures trying to associate Gigya id and MilleMiglia credentials.
 */
function failureSetSocialAccount() {
	console.error("Failed ajax call - set social account");
	displayGenericLoginFailureError();
}


/* internal perform login functions */

/**
 * Request actual AEM login using entered or resolved MilleMiglia credentials.
 */
function performMilleMigliaLogin(mmCode, mmPin, mmRememberMe, targetPage) {
	// clear client context session local storage when we are about to login
	clearClientContextProfile(); // (in login-context clientlibs)

	if (mmCode) {
		mmCode= mmCode.replace(/^0+/, '');
	}

	$("#form-perform-login input[name=j_username]").val("MM:" + mmCode);
	$("#form-perform-login input[name=j_password]").val(mmPin);
	$("#form-perform-login input[name=mm_remember_me]").val(mmRememberMe);
	
	// FIXME: sync login
	/*var resource = $("#form-perform-login input[name=resource]").val();
	if (resource == "") {
		var defaultResource;
		if (targetPage) {
			defaultResource = targetPage;
		} else {
			defaultResource = $("#form-perform-login input[name=default_resource]").val();
		}
		$("#form-perform-login input[name=resource]").val(defaultResource);
	}
	$("#form-perform-login").submit();*/
	
	/*
	FIXME: async login
	$("#form-perform-login input[name=j_validate]").val("true");
	performSubmit($("#form-perform-login").attr("action"),
			'#form-perform-login', performAsyncMilleMigliaLoginDone,
			displayGenericLoginFailureError, null, 'login');
	*/
	
	// FIXME: async login if targetPage is not specified, sync login otherwhise
	var defaultResource;
	if (targetPage) {
		defaultResource = targetPage;
		$("#form-perform-login input[name=resource]").val(defaultResource);
		$("#form-perform-login").submit();
	} else{
		
		$("#form-perform-login input[name=j_validate]").val("true");
		performSubmit($("#form-perform-login").attr("action"),
				'#form-perform-login', performAsyncMilleMigliaLoginDone,
				displayGenericLoginFailureError, null, 'login');
	}
}


/**
 * Request actual AEM login using entered or resolved MilleMiglia credentials.
 */
function performMilleMigliaLoginSA(mmCode, mmPin, mmRememberMe, targetPage) {
    // clear client context session local storage when we are about to login
    clearClientContextProfile(); // (in login-context clientlibs)

    if (mmCode) {
        mmCode= mmCode.replace(/^0+/, '');
    }

    $("#form-perform-login input[name=j_username]").val("MM:" + mmCode);
    $("#form-perform-login input[name=j_password]").val(mmPin);
    $("#form-perform-login input[name=mm_remember_me]").val(mmRememberMe);

    // FIXME: sync login
    /*var resource = $("#form-perform-login input[name=resource]").val();
    if (resource == "") {
        var defaultResource;
        if (targetPage) {
            defaultResource = targetPage;
        } else {
            defaultResource = $("#form-perform-login input[name=default_resource]").val();
        }
        $("#form-perform-login input[name=resource]").val(defaultResource);
    }
    $("#form-perform-login").submit();*/

    /*
    FIXME: async login
    $("#form-perform-login input[name=j_validate]").val("true");
    performSubmit($("#form-perform-login").attr("action"),
            '#form-perform-login', performAsyncMilleMigliaLoginDone,
            displayGenericLoginFailureError, null, 'login');
    */

    // FIXME: async login if targetPage is not specified, sync login otherwhise
    var defaultResource;
    if (targetPage) {
        defaultResource = targetPage;
        $("#form-perform-login input[name=resource]").val(defaultResource);
        $("#form-perform-login").submit();
    } else{

        $("#form-perform-login input[name=j_validate]").val("true");
        performSubmit($("#form-perform-login").attr("action"),
            '#form-perform-login', performAsyncMilleMigliaLoginSADone,
            displayGenericLoginSAFailureError, null, 'login');
    }
}

function performAsyncMilleMigliaLoginSADone(data) {
    loadCQAnalytics(function() {
        if (window.Granite && window.Granite.csrf &&
            typeof window.Granite.csrf.refreshToken !== 'undefined' &&
            typeof window.Granite.csrf.refreshToken === 'function') {
            window.Granite.csrf.refreshToken();
        }
        executeLoginContextProfileCallbacksHistory();
        var isSSWRedirectDone = performSSWRedirectIfRequired();
        if(!isSSWRedirectDone){
            alitaliaLoginData.busy = false;
            $('.login-included-if-authenticated-mm').removeClass("hide");
            $('.login-included-if-anonymous-mm').addClass("hide");
            $('.login-included-if-authenticated').removeClass("hide");
            $('.login-included-if-anonymous').addClass("hide");
            if ($("#loginMenu").hasClass("isActive")) {
                $("a.userMenu__login.j-userMenuLink").trigger("click");
            }
            toggleLoginSAOverlay(false);
            toggleLoginOverlay(false);
        }
    });
}

function performAsyncMilleMigliaLoginDone(data) {
	loadCQAnalytics(function() {
		if (window.Granite && window.Granite.csrf &&
				typeof window.Granite.csrf.refreshToken !== 'undefined' &&
				typeof window.Granite.csrf.refreshToken === 'function') {
			window.Granite.csrf.refreshToken();
		}
		executeLoginContextProfileCallbacksHistory();
		var isSSWRedirectDone = performSSWRedirectIfRequired();
		if(!isSSWRedirectDone){
			alitaliaLoginData.busy = false;
			if ($("#loginMenu").hasClass("isActive")) {
				$("a.userMenu__login.j-userMenuLink").trigger("click");
			}
			toggleLoginOverlay(false);	
		}
	});
}

/* internal perform logout functions */ 

function performSocialLogout() {
	setLoginContextProfileProperty('userImageThumbnailStatus', '');
	setLoginContextProfileProperty('userImageThumbnailURL', '');
	if (window['gigya']) {
		// second logout step - social logout
		window['gigya'].socialize.logout({
				callback: socialLogoutCallback
			});
	} else {
		// gigya not found - skip to millemiglia logout
		performMilleMigliaLogout();
	}
}

function socialLogoutCallback() {
	performMilleMigliaLogout();
}

function performMilleMigliaLogout() {
	$("#form-perform-logout").submit();
}


/* internal common login functions */

/**
 * Used to display unexpected technical errors during the login process.
 */
function displayGenericLoginFailureError() {
	//console.error("Unexpected login error");
	alitaliaLoginData.busy = false;
	toggleLoginOverlay(false);
	if (alitaliaLoginData.unexpectedErrorCallback) {
		alitaliaLoginData.unexpectedErrorCallback();
	}
}



/**
 * Used to display unexpected technical errors during the login process.
 */
function displayGenericLoginSAFailureError() {
    //console.error("Unexpected login error");
    alitaliaLoginData.busy = false;
    toggleLoginSAOverlay(false);
    toggleLoginOverlay(false);
    if (alitaliaLoginData.unexpectedErrorCallback) {
        alitaliaLoginData.unexpectedErrorCallback();
    }
}

/**
 * Used to display user messages about a failed login attempt.
 */
function displayLoginSAFailureError(errorCode, errorMessage) {
	//console.info("Login failed: " + errorCode + ", " + errorMessage);
	alitaliaLoginData.busy = false;
	toggleLoginSAOverlay(false);
	toggleLoginOverlay(false);
	if (alitaliaLoginData.loginFailureCallback) {
		alitaliaLoginData.loginFailureCallback(errorCode, errorMessage);
	}
}


/**
 * Used to display user messages about a failed login attempt.
 */
function displayLoginFailureError(errorCode, errorMessage) {
    //console.info("Login failed: " + errorCode + ", " + errorMessage);
    alitaliaLoginData.busy = false;
    toggleLoginSAOverlay(false);
    toggleLoginOverlay(false);
    if (alitaliaLoginData.loginFailureCallback) {
        alitaliaLoginData.loginFailureCallback(errorCode, errorMessage);
    }
}

function displayLoginSAUserLockedError() {
	alitaliaLoginData.busy = false;
	toggleLoginSAOverlay(false);
	toggleLoginOverlay(false);
	if (alitaliaLoginData.loginUserLockedCallback) {
		alitaliaLoginData.loginUserLockedCallback();
	}
}

function displayLoginUserLockedError() {
    alitaliaLoginData.busy = false;
    toggleLoginOverlay(false);
    if (alitaliaLoginData.loginUserLockedCallback) {
        alitaliaLoginData.loginUserLockedCallback();
    }
}


/* internal event handlers */

/**
 * Event handler on document ready to setup gigya listener.
 */
function onLoginDocumentReady(e) {
	if (window['gigya']) {
		window['gigya'].socialize.addEventHandlers({
			onLogin: onGigyaLogin,
			context: 'social'
		});
	}
	// setup a client context listener to read user image thumbnail URL
	// from local profile
	if (window['registerLoginContextProfileCallback']) {
		window['registerLoginContextProfileCallback'](
				onLoginClientContextProfileInitialized);
	} else {
		console.error("Expected function registerLoginContextProfileCallback " +
				"not defined.");
	}
}

/**
 * Handler on gigya login event, triggering social login based on context data.
 */
function onGigyaLogin(gigyaData) {
	if (gigyaData.context && gigyaData.context.action == 'startSocialLogin') {
		var gigyaId = gigyaData.UID;
		var gigyaSignature = gigyaData.UIDSignature;
		var gigyaProvider = gigyaData.provider;
		var gigyaUserFirstName = gigyaData.user.firstName;
		var gigyaUserLastName = gigyaData.user.lastName;
		var gigyaUserThumbnailURL = gigyaData.user.thumbnailURL;
		startLoginWithGigyaCredentials(gigyaId, gigyaSignature, gigyaProvider, 
				gigyaUserFirstName, gigyaUserLastName, gigyaUserThumbnailURL,
				false, gigyaData.context.unexpectedErrorCallback,
				gigyaData.context.loginFailureCallback);
	}
}

/**
 * Handler on client context init event.
 */
function onLoginClientContextProfileInitialized() {
	processUserImageThumbnailUrl();
}

/**
 * Invoked to process the user image thumbnail URL.
 */
function processUserImageThumbnailUrl() {
	// (login context functions defined in login-context clientlibs)
	if (isLoginContextAuthenticated()) {
		var userImageThumbnailStatus =
			getLoginContextProfileProperty('userImageThumbnailStatus');
		if (userImageThumbnailStatus == "" || userImageThumbnailStatus == null) {
			if (window['gigya']) {
				window['gigya'].socialize.getUserInfo({
					callback: getSocialUserInfoCallback
				});
			} else {
				setLoginContextProfileProperty('userImageThumbnailStatus',
						'unavailable');
			}
		}
	}
}

/**
 * Callback for social user info.
 */
function getSocialUserInfoCallback(response) {
	// (login context functions defined in login-context clientlibs)
	if (response.errorCode == 0 && response.user && response.user.thumbnailURL) {
		setLoginContextProfileProperty('userImageThumbnailStatus', 'success');
		setLoginContextProfileProperty('userImageThumbnailURL', response.user.thumbnailURL);
	} else {
		setLoginContextProfileProperty('userImageThumbnailStatus', 'unavailable');
		setLoginContextProfileProperty('userImageThumbnailURL', '');
	}
	applyClientContextProfile();
}

function performSSWRedirectIfRequired(){
	var sswRedemptionUrl = $("input[name='SSWRedirectLink']").val();// $("#SSWRedirectLink").val();
	var isSswRedemptionRedirect = getURLParameter(SSW_REDEMPTION_REDIRECT_PAR_NAME);
	if(isSswRedemptionRedirect === "1"){
		sswRedemptionUrl = addParameters(sswRedemptionUrl, decodeURIComponent(getURLParameter(SSW_REDEMPTION_PARAMETERS)));
		window.location.href = sswRedemptionUrl;
		return true;
	}
	return false;
}

function addParameters(url, params){
	var result = url;
	if(params){
		result = url.endsWith("?") || url.endsWith("&") ? url.substring(0, url.length -1) : url; 
		result = result + (result.match(/\?/) ? "" : "?");
		result = result + (result.endsWith("?") ? "" : "&") 
			+ (params.startsWith("&") || params.startsWith("?") 
					? params.substring(1, params.length) : params);
	}
	return result;
}


/* execution code */

// add listener for document ready event to setup social events listeners
$(document).ready(onLoginDocumentReady);