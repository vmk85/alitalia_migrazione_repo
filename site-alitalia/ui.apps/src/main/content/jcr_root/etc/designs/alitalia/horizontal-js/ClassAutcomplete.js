$(function () {

    var data =
    [
        {
            label: businesstranslation,
        },
    {
        label: economytranslation,
    },
    ];

    $("#class").autocomplete({
        delay: 0,
        source: function (request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response($.grep(data, function (value) {
                return matcher.test(value.label);
            }))
        },
        select: function (event, ui) {
            $("#cl").text(ui.item.label);
            $("#class").val(ui.item.label);
            $(".ui-helper-hidden-accessible").hide();
            return false;
        },
        open: function () {
            $(this).autocomplete('widget').css('z-index', 3000);
            $(this).autocomplete('widget').removeClass('ui-widget-content');
            $(this).autocomplete('widget').children().children().removeClass('ui-menu-item-wrapper');

            $(".ui-helper-hidden-accessible").hide();
            return false;
        },
        messages: {
            noResults: '',
            results: function () {
                $(".ui-helper-hidden-accessible").hide();
            }

        },
        minLength: 0
    }).autocomplete("instance")._renderItem = function (ul, item) {
        data;
        return $("<li></li>")
           .append("<div class=\"autocomplete-suggestion-class\"><div class=\"sugg_dest_data-class\"><span class=\"sugg_city_name_class toPass \">" + item.label + "</span></div></div>")
           .appendTo(ul);
    };

    $("#class").on('click', function () {
        $("#class").val("");
        $(this).autocomplete('search');
    });


    $("#classInputOverlayEconomy").on('click', function () {
        $("#cl").text($("#classInputOverlayEconomy").val());
        $("#class").val($("#classInputOverlayEconomy").val());
    });
    $("#classInputOverlayBusiness").on('click', function () {
        $("#cl").text($("#classInputOverlayBusiness").val());
        $("#class").val($("#classInputOverlayBusiness").val());
    });

})