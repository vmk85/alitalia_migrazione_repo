var myAlitaliaPreferenzeOptions = {};

// document ready
$( document ).ready( function() {
	myAlitaliaPreferenzeOptions.initRadio();
	myAlitaliaPreferenzeOptions.togglePanelOption();
	myAlitaliaPreferenzeOptions.togglePanel();
	myAlitaliaPreferenzeOptions.initAccordion();
} );

myAlitaliaPreferenzeOptions.initRadio = function() {
	if($('.panel-wrap-content .radio-wrap input:checked').length) {
		$('.panel-wrap-content .radio-wrap input:checked').closest('.radio-wrap').addClass('checked');
	}
	$(document).on('click', '.panel-wrap-content .radio-wrap .placeholder', function() {
		$(this).parent().find('input').click();
	});
	$(document).on('click', '.panel-wrap-content .radio-wrap input', function() {
		$('.panel-wrap-content .radio-wrap input[name='+$(this).attr('name')+']').closest('.radio-wrap').removeClass('checked');
		$(this).closest('.radio-wrap').addClass('checked');
	});
};

myAlitaliaPreferenzeOptions.togglePanelOption = function() {
	$(document).on('click', '.cta-deny', function() {
		$(this).closest('.panel-wrap-content').slideUp();
		$(this).closest('.customize-row').find('.cta, .button').show();
	});
	$(document).on('click', '.button-confirm', function() {
		$(this).closest('.panel-wrap-content').slideUp();
		$(this).closest('.customize-row').find('.cta, .button').show();
	});
};

myAlitaliaPreferenzeOptions.togglePanel = function() {
	$(document).on('click', '.cta-wrap-accordion .cta, .button-wrap-accordion .button', function() {
	    if($(this).hasClass("delete-preference")){
	        return false;
	    }
		var relatedContent = $(this).closest('.customize-row').find('.panel-wrap-content');
		if (relatedContent.length > 0 && relatedContent.is(':visible')) {
			relatedContent.slideUp();
		} else {
			
			relatedContent.slideDown(300, function(){
				// checkinCustomizeOptions.slider.update();
				// checkinCustomizeOptions.slider.pagination.update();
					if ($(relatedContent).find('.slider-row').length && checkinCustomizeOptions.swiper == null) {
						checkinCustomizeOptions.initSlider();
					}
			});
			$(".delete-preference").hide();
			$(this).hide();
		}
	}); 

	$(document).on('click', '.panel-wrap-content .close', function() {
		$(this).closest('.panel-wrap-content').slideUp();
		$(this).closest('.customize-row').find('.cta, .button').show();
	});

}

myAlitaliaPreferenzeOptions.initAccordion = function() {
	$( '.flight-row.accordion' ).foundation();
};