var checkinCustomizeOptions = {};

// document ready
$( document ).ready( function() {
	checkinCustomizeOptions.togglePanel();
	checkinCustomizeOptions.initAccordion();
	checkinCustomizeOptions.customizeNumber();
	// checkinCustomizeOptions.initSlider();
	checkinCustomizeOptions.swiper = null;
	$( '.swiper-slide .bg-image' ).foundation();
	
	checkinCustomizeOptions.passengerInputDisable();
} );

checkinCustomizeOptions.togglePanel = function() {
	$(document).on('click', '.cta-wrap-accordion .cta, .button-wrap-accordion .button', function() {
		var relatedContent = $(this).closest('.customize-row').find('.panel-wrap-content');
		if (relatedContent.length > 0 && relatedContent.is(':visible')) {
			relatedContent.slideUp();
		} else {
			
			relatedContent.slideDown(300, function(){
				// checkinCustomizeOptions.slider.update();
				// checkinCustomizeOptions.slider.pagination.update();
					if ($(relatedContent).find('.slider-row').length && checkinCustomizeOptions.swiper == null) {
						checkinCustomizeOptions.initSlider();
					}
			});
			$(this).hide();
		}
	}); 

	$(document).on('click', '.panel-wrap-content .close', function() {
		$(this).closest('.panel-wrap-content').slideUp();
		$(this).closest('.customize-row').find('.cta, .button').show();
	});

}

checkinCustomizeOptions.initAccordion = function() {
	$( '.flight-row.accordion' ).foundation();
};

checkinCustomizeOptions.customizeNumber = function() {
    $('[data-quantity="plus"]').click(function(e){
        e.preventDefault();
        fieldName = $(this).attr('data-field');
        var currentVal = parseInt($(this).closest('.input-wrap').find('input[name='+fieldName+']').val());
        //var currentVal = parseInt($('input[name='+fieldName+']').val());
        if (!isNaN(currentVal)) {
            $(this).closest('.input-wrap').find('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            $(this).closest('.input-wrap').find('input[name='+fieldName+']').val(0);
        }
		checkinCustomizeOptions.passengerInputDisable();
    });
    $('[data-quantity="minus"]').click(function(e) {
        e.preventDefault();
        fieldName = $(this).attr('data-field');
        var currentVal = parseInt($(this).closest('.input-wrap').find('input[name='+fieldName+']').val());
        //var currentVal = parseInt($('input[name='+fieldName+']').val());
        if (!isNaN(currentVal) && currentVal > 0) {
            $(this).closest('.input-wrap').find('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            $(this).closest('.input-wrap').find('input[name='+fieldName+']').val(0);
        }

		checkinCustomizeOptions.passengerInputDisable();
    });
};

checkinCustomizeOptions.passengerInputDisable = function() {
	var fieldName, currentVal;
	$( '[data-quantity="minus"]' ).each(function (){
	  	fieldName = $( this ).attr( 'data-field' );
	  	currentVal = parseInt( $( 'input[name=' + fieldName + ']' ).val() );
		if ( currentVal == 0 ){
				$(this).addClass('no-active');
				$(this).attr('disabled', true);
			} else { 
				$(this).removeClass('no-active');
				$(this).attr('disabled', false);
		}
    });
};

checkinCustomizeOptions.initSlider = function() {
    checkinCustomizeOptions.swiper = new Swiper('.slider-row .swiper-container', {
      pagination: {
        el: '.swiper-pagination',
      },
    });
    /*
	checkinCustomizeOptions.slider = new Swiper('.swiper-container', {
		'loop': true,
		'speed': 1500,
		autoplay: {
			delay: 5000,
		},
		pagination: {
        	el: '.swiper-pagination',
    	},
	});

	$( '.swiper-slide .bg-image' ).foundation();*/
};