// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
// MIT license

$(document).ready(function() {
    gigya.socialize.showLoginUI({containerID: "socialIcons", showTermsLink: false});
});

(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

function bookingReady() {
	if($('.booking').length > 0 || $('.bookingAward').length > 0 || $('.manageBooking').length > 0 || $('.checkin').length > 0 || $('.carnet').length > 0) {
		book.init();

		//resize calculate position
		$(window).on('resize',function(){
			var timeOuter;
			clearTimeout(timeOuter);
			timeOuter = setTimeout(function(){
				book.updateTopPosition();
				
				/*if($('.j-bookInfoBox').length > 0) {
					book.infoBoxTop = book.infoBox.offset().top;
				}*/

				if ( $('.booking').length > 0 ) {
					proportionSetter();
				}
				
			},50);

		});
	}
}

//general disable fixing booking
window.bookingDisableFixed= false;
window.bookingZoomDisableFixed = false;
window.bookingHeaderStop= false;
window.bookingDisableFixedJustCard = false;

var book = {
	infoBox : null,
	infoBoxTop : 0,
	sliders: null,
	init : function () {
		sliders = [];
		this.infoBox = $('.j-bookInfoBox');
		this.departureHeader=$(".j-bookingTable__header--departure");
		this.returnHeader= $(".j-bookingTable__header--return");
		this.recapDeparture= $('.j-bookingRecapDep');
		this.cookieBar = $('.j-cookieBar');
		this.supportTouch= 'ontouchstart' in document.documentElement;
		this.mobileVersion =( window.breakpointM > window.windowW);
		this.ticking=false;
		this.updateTopPosition();

		if ( $('.booking').length > 0 ) {
			proportionSetter();
		}

		$('.booking__dateListCover').each(function(index, el) {
			//get start
			var offsetSlide= 2;
			var ref=this;
			var pos= $(this).find('.booking__dateListItem.isActive').index();
			var startSlide = Math.max(0,pos-offsetSlide);
			if (  window.breakpointM > window.windowW    ){
				startSlide = 6
			}else if ( window.breakpointM > window.windowW  ){
				startSlide = 5
			}

			if(window.isRtl){
				//make slides rtl
				$(this).find('.booking__dateListItem').css({direction: 'rtl'});

				//remove rtl from slider
				$(this).css({direction: 'ltr'});

				//reorder slides
				var dateList = $(this).find('.booking__dateList');
				dateList.children().each(function(i, slide) {
					dateList.prepend(slide);
				});
			}

			var ios = $(this).iosSlider({
				//tabToAdvance: true,
				desktopClickDrag: true,
				snapToChildren: true,
				autoSlide: false,
				infiniteSlider: false,
				startAtSlide: startSlide
			});
			sliders.push(ios);

			if(window.isRtl){
				$(this).siblings('.booking__dateArrow.next').click(function() {
					var ct= $(ref).data('args').currentSlideNumber;
					$(ref).iosSlider('goToSlide', Math.max(ct-3,1));
				});

				$(this).siblings('.booking__dateArrow.prev').click(function() {
					var ct= $(ref).data('args').currentSlideNumber;
					$(ref).iosSlider('goToSlide', ct+3);
				});

			}
			else{
				$(this).siblings('.booking__dateArrow.next').click(function() {
					var ct= $(ref).data('args').currentSlideNumber;
					$(ref).iosSlider('goToSlide', ct+3);
				});

				$(this).siblings('.booking__dateArrow.prev').click(function() {
					var ct= $(ref).data('args').currentSlideNumber;
					$(ref).iosSlider('goToSlide', Math.max(ct-3,1));
				});	
			}

			
		});

		//paymentform
		$('.j-radioButtonCard').change(function () {

			$('.bookingPaymentForm__itemCard').removeClass('activeTab');
			$('.j-radioButtonCardImage').addClass('card--opacity');
			$(this).siblings('.j-radioButtonCardImage').removeClass('card--opacity');
			$(this).closest('.bookingPaymentForm__itemCard').addClass('activeTab');
			$('.j-paymentFieldset').removeClass('isActive');
			$('#'+$(this).attr('data-target')).addClass('isActive');

			var dataElChecked =  $(this).val();
			// $('.j-selectPaymentType').find('[value="'+ dataElChecked +'"]').prop('selected', true);
			$('.j-selectPaymentType').find('[value="'+ dataElChecked +'"]').prop('selected', true);

			var payform= $(this).closest('.bookingPaymentForm__itemCard.activeTab');
			if (payform.is(':visible')){
				var __top = payform.offset().top;
				var __height =  payform.outerHeight();

				$('html').velocity("scroll", {
					duration: 300,
					delay: 100,
					offset: __top - __height
				});
			}

		});

		$('.j-selectPaymentType').change(function () {
			var newVal = $(this).val();

			$('.j-selectPaymentType').removeClass('curretSelectedPayment');
			$(this).addClass('curretSelectedPayment');

			$('.j-selectPaymentType').each(function(){
				if ( !$(this).hasClass('curretSelectedPayment') ){
					$(this).prop('selectedIndex', 0);
				}
			})

			if(newVal == ''){
				$('.j-paymentFieldset').removeClass('isActive');
				$('.bookingPaymentForm__itemCard').removeClass('activeTab');
				$('.j-radioButtonCardImage').removeClass('card--opacity');
			} else {
				$('.j-radioButtonCard[value="'+newVal+'"]').click().trigger('change');
				var payform = $(this);
				var __top = payform.offset().top;
				var __height =  payform.outerHeight();

				var menuIsVisible = 0
				if (!$('[data-fixedheader]').data('fixedheader')){
					menuIsVisible = $('[data-fixedheader]').outerHeight()
				}
				console.log('menuIsVisible',menuIsVisible)
				$('html').velocity("scroll", {
					duration: 300,
					delay: 100,
					offset: __top - menuIsVisible - __height - 25
				});
			}
		});
		
		$('.j-wantInvoice').change(function () {
			if( $(this).is(':checked') ){
				$(this).closest(".bookingPaymentBlock").find('.j-wantInvoiceContainer').addClass('isActive').velocity('slideDown');
			} else {
			 	$(this).closest(".bookingPaymentBlock").find('.j-wantInvoiceContainer').removeClass('isActive').velocity('slideUp');
			}
		});

		$('.j-bookingAccordionPaymentTrigger').fastClick(function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			var isOpen = $('.j-bookingAccordionPaymentCover').is(':visible');
			$('.j-bookingAccordionPaymentCover').velocity( isOpen ? 'slideUp' : 'slideDown', {
				duration: 500,
				complete: function(){
					book.resetFixedElement();
					book.updateTopPosition();
					$(window).triggerHandler('scroll');
					resizeAccordions($(this).find('.j-accordion'));
				}
			});

			if (isOpen){
				$('.j-bookingAccordionPaymentCover').addClass('opened');
			} else {
				$('.j-bookingAccordionPaymentCover').removeClass('opened');
				$(window).trigger('resize');
			}
			$('.j-bookingAccordionPaymentTrigger').animateAnchor();
		});


		$('.j-showBackFlights').fastClick(function(e){
			e.preventDefault();
			var _active_element;
			var _table_;
			if($(e.currentTarget).attr('data-flight') === 'departure'){
				_table_ = $('.j-bookingTableDeparture');
				_active_element = $('body').data('element_to_open_departure');
				_loader_ = $('.j-bookingTableDeparture .j-bookingLoader');
				_title_= $('.j-titleDeparture');
				$('.j-bookingTableDeparture .j-bookTableRow.activeRow').show();
				$('.j-bookingRecapDep').velocity('slideUp');
				$(this).hide();
			}else{

				_table_ = $('.j-bookingTableReturn');
				_active_element = $('body').data('element_to_open_return');
				_loader_ = $('.j-bookingTableReturn .j-bookingLoader');
				_title_ = $('.j-titleReturn');
				$('.j-bookingTableReturn .j-bookTableRow.activeRow').show();
				$('.j-bookingRecapRet').velocity('slideUp');
				window.bookingDisableFixed= false;
				window.bookingDisableFixedJustCard = false;
				$(this).hide();

			}

			_title_.velocity('slideDown');
			_date_ = _title_.next();
			_date_.velocity('slideDown').iosSlider('update')
			_table_.velocity('slideDown',{

				complete: function(){
					//$(window).trigger('resize');
					book.priceSelectorResetFast(_table_);
					if (window.breakpointM > window.windowW ){
						_active_element.velocity('slideDown');
					} else {
						_active_element.find('.j-priceSelector').trigger('click');	

					}
				}
			});
		});


		$('.j-loadMoreBookingRow').fastClick(function(e){
			e.preventDefault()
			var _loader = $(e.currentTarget).closest('.j-bookingTable').find('.j-bookingLoader')
			_loader.velocity('transition.slideDownIn', {
				duration: 250,
				complete: function(){

					be_loadMoreFlights($(this), function(new_element){
						new_element.find('.j-priceSelectorCol').fastClick( book.priceSelectorCol );
						new_element.find('.j-priceSelector').fastClick( book.priceSelector );
						new_element.find('.j-businessTrigger').fastClick( book.businessTrigger );
						$('.j-flightDetails').fastClick(function(e){
							book.getFlightDetails($(e.currentTarget));
						});
						new_element.find('.j-mobile-openOptions').fastClick( book.openOptionMobile );
						new_element.find('.j-closeTableRow').fastClick( book.priceSelectorReset );
						new_element.find('.j-goToReturn').fastClick( book.goToReturn );
						new_element.find('.j-selectReturn').fastClick( book.selectedReturn );
						_loader.velocity('transition.slideDownOut');
						$(window).trigger('resize.accordion');
						// Mehr da finire
					});

				}
			})
		})

		if(book.supportTouch) {
			// blur
			//disable header for select
			$('select').on('focus',  function(e) {
				book.checkScale();
		    }).on('blur', function(e) {
		    	book.checkScale();
		    });

			$(document).on("gestureend", function onPinch(event) {
				var scale = event.originalEvent.scale;
				book.checkScale(scale);
			});
			if( $('.j-fixedClone').length == 0 ){
				$('.j-wrapperFixed').append($('<div class="j-fixedClone"></div>'));
			}
		}

		//accordion element
		/*
		$('.j-accordions').on("accordion.opened", function (e) {
			book.resetFixedElement();
			book.updateTopPosition();
			$(window).triggerHandler('scroll');
		}).on("accordion.closed", function (e) {
			book.resetFixedElement();
			book.updateTopPosition();
			$(window).triggerHandler('scroll');
		});*/


		//fast click
		$('.j-priceSelectorCol').fastClick( book.priceSelectorCol );
		$('.j-priceSelector').fastClick( book.priceSelector );

		//switch automatically if focus in and not visible

		/*
		$('.j-priceSelector').focusin(function(el) {
			var isEconomy= String($(this).attr("data-type")).toLowerCase().indexOf("economy") != -1;
			var businessActivated = $(this).find(".j-bookingTable").hasClass("businessActivated");
			if(isEconomy == businessActivated ) $(this).closest('.bookingTable__col').siblings('.j-businessTrigger').trigger("click");
		});*/

		$('.j-businessTrigger').unfastClick().fastClick(function(e){
			e.preventDefault();
			book.businessTrigger($(e.currentTarget));

		});
		$('.j-flightDetails').unfastClick().fastClick(function(e){
			book.getFlightDetails($(e.currentTarget))
		});
		$('.j-mobile-openOptions').fastClick( book.openOptionMobile );
		$('.j-closeTableRow').fastClick( book.priceSelectorReset );
		$('.j-goToReturn').fastClick( book.goToReturn );
		$('.j-selectReturn').fastClick( book.selectedReturn );

		//
		$('.j-toggleBasketAccordion').fastClick(function(e){
			e.preventDefault();
            book.openBasket($(this));
		});
        $('.close.j-toggleBasketAccordion').fastClick(function(e){
			/* setTimeout(scrollDownBasketAccordion, 0);*/
            scrollDownBasketAccordion();
		});
		if($('.j-toggleBasketAccordion').hasClass('opened')){
			var toShow = $('.j-toggleBasketAccordion').attr('href')
			$(toShow).show();
			window.bookingHeaderStop=true;
			resizeAccordions($('.j-accordion'));
		}
		if($('.j-openFlightFinderCover').is(':visible')){
			window.bookingHeaderStop=true;
		}
		//
		$('.j-openFlightFinder').fastClick(function(e){
			e.preventDefault();
			book.openBasket($(this));
		});

		if(book.supportTouch)  {
			$( window ).on( "load",function() {
				book.checkScale();
			});
		}
		book.stickyHeaderBooking();
		this.swicthTabIndex();
	},

	swicthTabIndex : function() {
		//getposition
		$('.j-bookingTable').each(function() {
			var pos= $(this).find('.j-bookTableRow .j-businessTrigger').eq(0).index();
			if(pos >0) {
				var isbusiness=$(this).hasClass("businessActivated");
				$(this).find('.j-bookTableRow .bookingTable__col').each(function() {
					if(!isbusiness) {
						if($(this).index() > pos) {
							$(this).children().attr("tabindex", "-1");
						} else {
							$(this).children().removeAttr("tabindex");
						}
					} else {
						if($(this).index() >= pos) {
							$(this).children().removeAttr("tabindex");
						} else {
							$(this).children().attr("tabindex", "-1");
						}
					}
				});
			}
		});
	},

	checkScale : function (value){
		clearTimeout(window.checkScaleID);
		window.checkScaleID=setTimeout(function() {
			var scale = (value != undefined) ? value : book.zoomValue();
			if(scale) {
				if( scale > 1 ){
					book.resetFixedElement();
					window.bookingZoomDisableFixed= true;
				} else {
					window.bookingZoomDisableFixed= false;
					$(window).triggerHandler('scroll');
				}
			}
		},250);
	},
	zoomValue : function() {
		return document.documentElement.clientWidth / window.innerWidth;
	},
	updateInfoBoxPosition : function() {
		if (this.infoBox && this.infoBox.length>0 ) {
			this.infoBoxTop = this.infoBox.closest(".j-wrapperFixed").offset().top;
		}
	},
	updateDepartureHeaderPosition : function() {
		if (this.departureHeader && this.departureHeader.length>0) {
			this.departureHeaderTop= this.departureHeader.closest(".j-wrapperFixed").offset().top;
		}
	},
	updateReturnHeaderPosition : function() {
		if (this.returnHeader && this.returnHeader.length>0) {
			this.returnHeaderTop= this.returnHeader.closest(".j-wrapperFixed").offset().top;
		}
	},
	updateRecapDeparturePosition : function() {
		if ($('.j-bookingRecapDep').length > 0) {
			this.bookingRecapDepTop= $('.j-bookingRecapDep').closest(".j-wrapperFixed").offset().top;
		}
	},
	updateCookiePosition: function(){
		if($('.j-cookieBar').length > 0){
			if($('.j-cookieBar').is(':visible') && (!$('.j-cookieBar').hasClass('notFixed'))) {
				this.cookieHeight = $('.j-cookieBar').outerHeight();
			}else{
				this.cookieHeight = 0;
			}
		}
	},
	updateTopPosition : function() {
		this.updateInfoBoxPosition();
		this.updateReturnHeaderPosition();
		this.updateDepartureHeaderPosition();
		this.updateRecapDeparturePosition();
		this.updateCookiePosition();
		if(sliders.length>0){
			for(var i=0;i<sliders.length;i++){
				slider = $(sliders[i]);
				slider.iosSlider('update');
			}
		}

	},
	openBasket : function (that){
		var tab = that.attr('href');
		if( $(tab).is(':visible') ){
			book.infoBox.removeClass('opened');

			window.bookingHeaderStop = false;
			$(tab).velocity("slideUp", {
				duration: 300,
				complete: function(){
					book.resetFixedElement();
					book.updateTopPosition();
				}
			});
		} else {
			if ( $('.j-openFlightFinderCover').is(':visible') ) {
				$('.j-openFlightFinderCover').removeClass('opened').velocity("slideUp");
			} else if ( $('.booking__basketAccordionContainer').is(':visible') ) {
				$('.j-bookInfoBox').removeClass('opened')
				$('.booking__basketAccordionContainer').velocity("slideUp");

			}
			book.infoBox.addClass('opened');
			window.bookingHeaderStop = true;
			$(tab).velocity("slideDown", {
				delay: 100,
				duration: 300,
				complete: function(){
					book.resetFixedElement();
					book.updateTopPosition();
					$(window).triggerHandler('scroll');
					// $(window).trigger('resize.accordion');
					resizeAccordions($('.j-accordion'));
				}
			});
		}

		var __top_infoBoxTop = book.infoBoxTop;

		if($('.j-cookieBar').length > 0 && $('.j-cookieBar').is(':visible') && (!$('.j-cookieBar').hasClass('notFixed')) ){
			__top_infoBoxTop -= $('.j-cookieBar').outerHeight();
		}
		if($('.mainMenu.posFixed').length > 0){
			__top_infoBoxTop -= $('.mainMenu.posFixed').outerHeight();
		}
		$('html').velocity("scroll", {
			duration: 300,
			delay: 100,
			offset: __top_infoBoxTop,
			complete: function(){
			}
		});
		return false;
	},
	resetFixedElement: function() {
		$('.j-wrapperFixed').each(function() {
			book.unfixElement($(this).find(".isFixed"));
		});
	},
	fixElement : function(el) {
		var hel = el.outerHeight(true);
		if(el.hasClass("isFixed")) return hel;
		el.addClass('isFixed').closest('.j-wrapperFixed').css({"height":hel});
		if(book.supportTouch) {
			el.siblings('.j-fixedClone').css({'visibility':'visible','z-index':el.css("z-index")}).append(el);
		}
		return hel;
	},
	unfixElement : function(el) {
		if(!el.hasClass("isFixed")) return;
		el.removeClass('isFixed').css({"top":""}).closest('.j-wrapperFixed').attr("style","");
		if(book.supportTouch) {
			el.closest('.j-wrapperFixed').append(el);
			el.siblings('.j-fixedClone').empty().css({'visibility':'hidden'});
		}
	},

	requestDrawFixedPosition: function() {
		if(!this.ticking) {
			requestAnimationFrame(this.drawFixedPosition);
		}
		this.ticking = true;
	},
	drawFixedPosition : function() {
		//requestAnimationFrame(book.drawFixedPosition);
		//console.time("end");
			book.ticking = false;
			if(window.bookingDisableFixed || window.bookingHeaderStop || window.bookingZoomDisableFixed) {
				return;
			}
			var _this= book;
			var value = $(window).scrollTop();
			if(value ===_this.lastScrollTop ) return;
			_this.lastScrollTop= value;
			var topLimit,proceduralHeight,infoBoxTop_start,offsetInfoBox;
			topLimit=proceduralHeight=infoBoxTop_start=offsetInfoBox=0;

			var headerDepartureIsFixed = false;
			var cookieBarVisibleFixed = _this.cookieBar.is(':visible') && (!_this.cookieBar.hasClass('notFixed'));
			var mainMenuFixed = $('.mainMenu.posFixed');


			topLimit = book.infoBoxTop;
			if ( _this.mobileVersion) {
				offsetInfoBox =book.infoBox.find('.bookInfoBoxItinerary').outerHeight()+11;
				topLimit+= offsetInfoBox;
			} 
			
			if(mainMenuFixed.length > 0){
				var mh=mainMenuFixed.outerHeight();
				topLimit -= mh;
				proceduralHeight+= mh;
			}
			if(cookieBarVisibleFixed){
				topLimit -= _this.cookieHeight;
				proceduralHeight += _this.cookieHeight;
			}
			//reset

			removeTooltip();

			if (value >= topLimit ){
				// proceduralHeight+= _this.fixElement(_this.infoBox);
				if(_this.infoBox && _this.infoBox.is(':visible') ){
					if(_this.mobileVersion) proceduralHeight-= offsetInfoBox;
					var ht= _this.fixElement(_this.infoBox);
					_this.infoBox.css({"top":proceduralHeight+"px" });
					proceduralHeight+=ht;
				}
				
				//if(this.mobileVersion) _this.infoBox.css({"top": -offsetInfoBox });
			} else {
				_this.unfixElement(_this.infoBox);
			}


			//exit check
			if(_this.mobileVersion) return;
			if(_this.recapDeparture.is(':visible')) {
				if(window.bookingDisableFixedJustCard){return;}
				if (value >= (book.bookingRecapDepTop - proceduralHeight)){
					var ht=_this.fixElement(_this.recapDeparture);
					_this.recapDeparture.css({"top": proceduralHeight });
					proceduralHeight+=ht;
				} else {
					_this.unfixElement(_this.recapDeparture);
				}
			}
			//-------------------------------//;
			if(_this.departureHeader.length && _this.departureHeader.is(':visible')) {
				if(window.bookingDisableFixedJustCard){return;}
				if (value >= (_this.departureHeaderTop - proceduralHeight)){
					var departureHeaderHeight = _this.departureHeader.outerHeight();
					var posEl = proceduralHeight;

					var table=$('.j-bookingTableDeparture');
					var pt = proceduralHeight+departureHeaderHeight;
					var pth = table.offset().top + table.outerHeight();
					var diffy = value-pth+pt;
					if (diffy > 0) posEl-=diffy;


					_this.fixElement(_this.departureHeader);
					_this.departureHeader.css({'top':posEl});
				} else {
					_this.unfixElement(_this.departureHeader);
				}
			}

			//-------------------------------//;
			if(_this.returnHeader.length && _this.returnHeader.is(':visible')) {
				if(window.bookingDisableFixedJustCard){return;}
				if (value >= (book.returnHeaderTop - proceduralHeight)){
					var returnHeaderHeight = _this.returnHeader.outerHeight();
					var posEl = proceduralHeight;

					var table=$('.j-bookingTableReturn');
					var pt = proceduralHeight+returnHeaderHeight;
					var pth = table.offset().top + table.outerHeight();
					var diffy = value-pth+pt;
					if (diffy > 0) posEl-=diffy;

					_this.fixElement(_this.returnHeader);
					_this.returnHeader.css({'top':posEl});

				} else {
					_this.unfixElement(_this.returnHeader);
				}
			}

			//-------------------------------//;
			//console.timeEnd("end");
	},

	stickyHeaderBooking :  function (){

		//touchmove.booking  -> performance
		var mobileevent="scroll.booking touchstart.booking touchend.booking";
		var event_type= book.supportTouch ? mobileevent  : "scroll.booking";
		$(window).unbind(mobileevent).bind(event_type, function (event) {
			book.requestDrawFixedPosition();
		}).triggerHandler('scroll');
	},
	destroyToolTip : function() {
		//close other tolltip and destrouy delete
		$('.j-overlayLinkTooltip').each(function () {
			clearInterval($(this).qtip('hide').data('showID'));
		});
	},
	priceSelectorCol : function () {
		var pos=$(this).parent().find('.j-priceSelectorCol').index($(this));
		$(this).closest('.j-bookTableRowBody').prev().find('.j-priceSelector').eq(pos).trigger("click");
	},
	priceSelector : function (e) {
		e.preventDefault();
		var self = $(this).parent();
		var _this = $(this);
		//DESKTOP
		/*
		MODIFICA DEL 29/05/2017
		PER TABLET
		if( window.windowW > window.breakpointS ) {

		 */
		if( window.windowW > window.breakpointM ) {
			if( self.hasClass('isActive') ){
				return
			}
			row = self.closest('.j-bookTableRow'),
			body = row.find('.j-bookTableRowBody'),
			className = self.data('type');
			var _headertoresetFixed = self.closest('.j-bookingTable').find('.bookingTable__header').data('num');

			var __is_open = (self.siblings().hasClass('isActive') ? true : false);

			var tableCell= self.closest('.j-bookingTable');
			var headCell= self.closest('.bookingTable__header');

			tableCell.find('.bookingTable__col').removeClass('isActive');
			body.find('.'+className).addClass('isActive');


			var tablesActiveRow= $('.j-bookingTable').find('.j-bookTableRow.activeRow');
			var isTableReturn = tableCell.hasClass('j-bookingTableReturn');
			var blockCellTooltip = tableCell.find('.'+className +' .j-overlayLinkTooltip');

			var preventSelected= tableCell.find('.activeRow');
			var extraHeight=0;
			if(preventSelected.length == 1) {
				// if(row.index() >preventSelected.index()) {
				// 	extraHeight= preventSelected.find('.j-bookTableRowBody').outerHeight();
				// }
			}
			if (tableCell.find('.j-bookTableRowBody:visible').length > 0) {
				book.priceSelectorReset(__is_open, tableCell);
			}

			var blockCell= $('.bookingTable__header').find('.'+className);
			blockCell.addClass('headingActive');
			book.destroyToolTip();

			//show tooltip
			blockCellTooltip.qtip('show');
			blockCellTooltip.data('showID', setTimeout( function() { blockCellTooltip.qtip('hide'); }, 2500));

			row.addClass('activeRow');
			self.addClass('isActive');

			row.find('.j-flightDetails__condition').velocity('slideUp');
			row.find('.j-flightDetails__condition[data-class="'+className+'"]').velocity('slideDown');
			//animazione apertura
			window.bookingLoaderMove = false;
			if(!__is_open){
				//return;

				var _loader = $(e.currentTarget).closest('.j-bookingTable').find('.j-bookingLoader');
				if(!_loader.is(':visible')){
					_loader.velocity('fadeIn');
				}
				book.getFlightDetailsRemove();


				// this function callsBackend
				be_priceSelector( $(this) , function(){

					body.velocity("slideDown", {
						duration: 400 ,
						complete: function(){
							book.updateTopPosition();

							window.bookingLoaderMove = true;

							//check if one is open
							//calculate end position
							
							if(bookingHeaderStop) {
								var endPos=row.offset().top-extraHeight;
							} else {
								var endPos=row.offset().top-extraHeight;
								endPos-= book.infoBox.outerHeight();
								if(book.departureHeader.is(":visible")) {
									endPos-=book.departureHeader.outerHeight();
								} else if(book.returnHeader.is(":visible")) {
									endPos-=book.returnHeader.outerHeight();
								}
								if(book.recapDeparture.is(":visible")) endPos-=book.recapDeparture.outerHeight();
							}

							$("html, body").animate({ scrollTop: endPos+"px" }, {
								duration:400,
								complete: function() {
									book.updateTopPosition();
								}
							});
							_loader.velocity('fadeOut');
							initAccordions();

							if( window.changeTariff ){
								//changeTariffRow();
							}
						}
					});
					
				});

			} else {
				// add here logic for conditional partx
			}
	//MOBILE

	}else{

		if(self.closest('.j-bookingTable').hasClass('j-bookingTableDeparture')){
			$('body').data('element_to_open_departure',$(e.currentTarget).closest('.j-bookTableRow.mobileRowActive'));
			$('.j-bookingTableDeparture .j-bookingLoader').velocity('transition.slideDownIn',{
				duration: 250,
				complete: function(){				
					// console.log('be_goToReturn')
					be_goToReturn( _this , function(){
						$('.j-bookingTableDeparture .j-bookTableRow.mobileRowActive').velocity("slideUp", {
							duration: 400 ,
							complete: function(){
								$('.j-bookingRecapDep').velocity('transition.slideDownIn',{
									duration: 250,
									complete: function(){
										$(window).trigger('resize.accordion');
										$('.j-bookingFlyoutTooltip').velocity( window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn");
										$('.j-bookingTableDeparture , .j-titleDeparture, .booking__date:eq(0)').velocity('slideUp',{
											complete: function(){
												$('.j-bookingTableDeparture .j-bookingLoader').velocity('transition.slideDownOut', {
													duration: 200,
													complete: function(){

														var newPos = $('.j-bookingRecapDep').offset().top - $('.j-bookInfoBox').outerHeight();;
														$('html').velocity("scroll", {
															duration: 400,
															offset: newPos,
															complete: function(){
																book.updateTopPosition();
															}
														});

														setTimeout(function(){
															$('.j-bookingFlyoutTooltip').velocity( window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
															$('.j-showBackFlights:eq(0)').fadeIn();
															initAccordions();
														},1500)
													}
												});
											}
										});
									}
								});
							}
						});
					}, function(){
						$('.j-bookingTableDeparture .j-bookingLoader').velocity('transition.slideDownOut');
					});
				}
			});

		} else {
			$('body').data('element_to_open_return',$(e.currentTarget).closest('.j-bookTableRow.mobileRowActive'));

			$('.j-bookingTableReturn .j-bookingLoader').velocity('transition.slideDownIn',{
				duration: 250,
				complete: function(){
					// console.log('be_selectedReturn 2')
					be_selectedReturn( _this , function(){
						$('.j-bookingTableReturn .j-bookTableRow.mobileRowActive').velocity("slideUp", {
							duration: 400 ,
							complete: function(){
								$('.j-bookingRecapRet').velocity('transition.slideDownIn',{
									duration: 250,
									complete: function(){

										$(window).trigger('resize.accordion');
										$('.j-bookingFlyoutTooltip').velocity( window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn");

										$('.j-bookingTableReturn , .j-titleReturn, .booking__date').velocity('slideUp',{
											complete: function(){
												$('.j-bookingTableReturn .j-bookingLoader').velocity('transition.slideDownOut', {
													duration: 200,
													complete: function(){
														var newPos = $('.j-bookingRecapDep').offset().top - $('.j-bookInfoBox').outerHeight();
														$('html').velocity("scroll", {
															duration: 400,
															offset: newPos,
															complete: function(){
																// $('.bookingRecap .j-accordion').addClass('active');
																// $( window ).trigger('resize.accordion');
																$('.j-showBackFlights').fadeIn();

																//book.resetFixedElement();
																book.updateTopPosition();

																setTimeout(function(){
																	$('.j-bookingFlyoutTooltip').velocity( window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
																},1500);


																$( window ).trigger('resize.accordion');


															}
														});
													}
												});
											}
										});

										initAccordions();

										$('.bookingRecap .j-accordion-header').each(function(){
											if (!$(this).hasClass('opening')){
												$(this).trigger('click');	
											}
										});
									}
								});
							}
						});
					});
				}
			});

		}

	}

	},
	priceSelectorReset : function(_is_open, table){

		if(typeof(_is_open) !== "boolean"){
			_is_open.preventDefault();
			table = $(_is_open.currentTarget).closest('.j-bookingTable');
		}
		table.find('.bookingTable__col').removeClass('headingActive');
		table.find('.j-bookTableRow').removeClass('activeRow');
		table.find('.j-priceSelector').parent().removeClass('isActive');

		var __to_close = table.find('.j-bookTableRowBody:visible');
		var __header_to_calculate = __to_close.closest('.j-bookingTable').find('.bookingTable__header').data('num')

		if(!_is_open || typeof(_is_open) !== "boolean"){
			__to_close.velocity("slideUp", {

				duration: 400,
				complete: function(){

				}
			});
		}

	},
	priceSelectorResetFast : function(table){
		$(window).trigger('resize');
		table.find('.bookingTable__col').removeClass('headingActive');
		table.find('.j-bookTableRow').removeClass('activeRow');
		table.find('.j-priceSelector').parent().removeClass('isActive');

		var __to_close = table.find('.j-bookTableRowBody:visible');
		var __header_to_calculate = __to_close.closest('.j-bookingTable').find('.bookingTable__header').data('num')

		__to_close.hide();
	},
	businessTrigger : function(sel){
		
		var selector = sel.closest('.j-bookingTable')
		selector.toggleClass('businessActivated');
		this.swicthTabIndex();
		var directionAnimation = {}
		if(window.isLtr) {
			directionAnimation = {'left' : 0}
		}
		if ( selector.hasClass('businessActivated') && window.isLtr ) {
			directionAnimation = {'left' : '-92%'};
		}

		if(window.isRtl) {
			directionAnimation = {'right' : 0}
		}
		if ( selector.hasClass('businessActivated') && window.isRtl ) {
			directionAnimation = {'right' : '-92%'};
		}
		selector.find('.bookingTable__rightInner').velocity(directionAnimation , {duration: 500});
		return
	},

	getFlightDetails : function(element){
		// be_getFlightDetails
		// if ($('.flyoutBookingFlightDetails').length > 0) {
		// 	$('.flyoutBookingFlightDetails').velocity('transition.slideDownOut', {
		// 		duration: 10,
		// 		complete: function(){
		// 			$('.flyoutBookingFlightDetails').remove();
		// 		}
		// 	});
		// } 
		
		$('.flyoutBookingFlightDetails').remove();
		$('.j-flightDetails').data('isOpen',false);

		if( window.breakpointM > window.windowW) {
			// mobile
			// TODO: MOBILE loader needs to be addded
			// var _loader = '<div class="j-loader"><div class="miniLoader"></div></div>'
			// $(_loader).insertAfter(element)
			if ( !element.closest('.j-bookTableRow').find('.j-bookTableRowBody').is(':visible') ) {
				startPageLoader(true);
			}

			be_getFlightDetails( element ,function() {
				var selector = element.closest('.j-bookTableRow').find('.j-bookTableRowBody');
				var __booking_table_right = element.closest('.j-bookTableRow').find('.bookingTable__pricePart');

				if ( selector.is(':visible') ){
					//close
					selector.velocity('slideUp', { duration: 300 });
					element.closest('.j-bookTableRow').removeClass('mobileRowActive')
				} else {
					//open
					selector.velocity('slideDown', {
						duration: 300,
						complete: function(){
							$('html, body').animate({
								scrollTop: selector.closest('.j-bookTableRow').offset().top - 60,
							});
						}
					});
					element.closest('.j-bookTableRow').addClass('mobileRowActive')
					// close other info
					if(__booking_table_right.is(':visible')){
						__booking_table_right.velocity('slideUp', { duration: 300 });
					}
				}
				var __other_table_row = element.closest('.j-bookTableRow').siblings();
				var __other_table_row_body = __other_table_row.find('.j-bookTableRowBody');
				var __other_table_row_body_price = __other_table_row.find('.bookingTable__pricePart');


				// close other info
				if(__other_table_row.hasClass('mobileRowActive')){
					if(__other_table_row_body.is(':visible')){
						__other_table_row_body.velocity('slideUp', {
							duration: 300,
							complete: function(){
								__other_table_row.removeClass('mobileRowActive');
							}
						});
					}
					if(__other_table_row_body_price.is(':visible')){
						__other_table_row_body_price.velocity('slideUp', {
							duration: 300,
							complete: function(){
								__other_table_row.removeClass('mobileRowActive');
							}
						});
					}
				}
				startPageLoader(false);
			});
		} else {
			if(element.closest('.j-bookTableRow').hasClass('activeRow')){return;}
			if(element.data('isOpen') === true){
				book.getFlightDetailsRemove();
				return;
			}else{
				element.data('isOpen',true);
			}
			_loader = '<div class="j-loader"><div class="miniLoader"></div></div>'
			$('<div class="flyoutBookingFlightDetails">'+_loader+'</div>').appendTo('body').show()
			$('.flyoutBookingFlightDetails').find('.j-loader').show()

			var positionTop = element.offset().top;
			var positionLeft = element.offset().left;

			$('.flyoutBookingFlightDetails').data('element',element);
			$('.flyoutBookingFlightDetails').css({
					left: positionLeft - 15,
					top: positionTop + 25,
					width: element.closest('.booking__fightPreviewWrapper').outerWidth()
				})
			be_getFlightDetails( element ,function(){

				var detailCont = element.closest('.j-bookTableRow').find('.j-bookingFlightDetails').clone();
				//
				$('.flyoutBookingFlightDetails').find('.j-loader').remove()
				$('.flyoutBookingFlightDetails').append('<a class="flyoutBookingFlightDetails__close" href="javascript:;">&times;</a>'+detailCont.html());
				$('.flyoutBookingFlightDetails__close').focus();
				$('.flyoutBookingFlightDetails__close').fastClick(book.getFlightDetailsRemove);
			})
		// 	be_getFlightDetails( element ,function(){

		// 		if(element.closest('.j-bookTableRow').hasClass('activeRow')){return;}
		// 		// if ( $('.flyoutBookingFlightDetails').length > 0 ){
		// 		// 	book.getFlightDetailsRemove();
		// 		// 	return;
		// 		// }

		// 		var detailCont = element.closest('.j-bookTableRow').find('.j-bookingFlightDetails').clone();
		// 		var positionTop = element.offset().top;
		// 		var positionLeft =element.offset().left;

		// 		book.getFlightDetailsRemove();

		// 		$('<div class="flyoutBookingFlightDetails"><a class="flyoutBookingFlightDetails__close" href="javascript:;">&times;</a>'+detailCont.html()+'</div>').appendTo('body');
		// 		$('.flyoutBookingFlightDetails').css({
		// 			left: positionLeft - 15,
		// 			top: positionTop + 25,
		// 			//width: $('.booking__fightPreviewWrapper').outerWidth()
		// 		})

		// 		if($(window).width() > window.breakpointL){
		// 			$('.flyoutBookingFlightDetails').css({
		// 				width: $('.booking__fightPreviewWrapper').outerWidth()
		// 			});
		// 		}


		// 		$('.flyoutBookingFlightDetails').velocity(
		// 			'transition.slideDownIn',
		// 			{ duration: 250 },
		// 			{ complete: function(){
		// 				$('.flyoutBookingFlightDetails__close').focus();
		// 			}
		// 		});

		// 		$('.flyoutBookingFlightDetails__close').fastClick(book.getFlightDetailsRemove);
		// 	});
		}

	},

	getFlightDetailsRemove : function(){

		if( $('.flyoutBookingFlightDetails').length > 0 ){
			$('.flyoutBookingFlightDetails').data('element').data('isOpen',false);
			$('.flyoutBookingFlightDetails').velocity('transition.slideDownOut', {
				duration: 10,
				complete: function(){
					$('.flyoutBookingFlightDetails').remove();
				}
			});
		}

	},

	openOptionMobile: function(){
		if( window.windowW > window.breakpointM ) {
			return
		}

		var row = $(this).closest('.j-bookTableRow');
		var table = row.find('.j-bookingTable');
		var selector = row.find('.bookingTable__pricePart');


		var opened = row.siblings('.mobileRowActive');
		if (opened.length > 0) {
			opened.removeClass('mobileRowActive');
			if(opened.find('.bookingTable__pricePart').is(':visible')){
				opened.find('.bookingTable__pricePart').velocity('slideUp', { duration: 300 });
			}else if(opened.find('.j-bookTableRowBody').is(':visible')){
				opened.find('.j-bookTableRowBody').velocity('slideUp', { duration: 300 });
			}

		}
		//$('.j-bookTableRow').removeClass('mobileRowActive');
		row.addClass('mobileRowActive');

		var __table_row_body = $(this).closest('.j-bookTableRow').find('.j-bookTableRowBody')
		if ( selector.is(':visible') ){
			row.removeClass('mobileRowActive');
			selector.velocity('slideUp', { duration: 300 });
		} else {
			//open
			selector.velocity('slideDown', {
				duration: 300,
				complete: function(){
					$('html, body').animate({
						scrollTop: row.offset().top - 60,
					});
				}
			});
			// close other info
			if(__table_row_body.is(':visible')){
				__table_row_body.velocity('slideUp', { duration: 300 });
			}
		}
	},
	goToReturn: function(e){
		book.destroyToolTip();
		e.preventDefault();
		var _this = $(this);
		var title_departure= $('.j-titleDeparture');
		var date_departure = title_departure.next();
		//$('body').data('type_of_table',$('.j-bookingTableDeparture'));
		$('body').data('element_to_open_departure',$(e.currentTarget).closest('.j-bookTableRow').find('.bookingTable__rightInner').find('.bookingTable__col.isActive'));
		$('.j-bookingTableDeparture .j-bookingLoader').velocity('fadeIn',{
			duration: 700,
			complete: function(){
				// console.log('be_goToReturn')
				be_goToReturn(_this, function(){

					title_departure.velocity('slideUp');
					date_departure.velocity('slideUp');
					// animation to return

					$('.j-bookingTableDeparture').velocity('slideUp',{
						duration: 400,
						complete: function(){
							book.updateTopPosition();
							$('.j-bookingFlyoutTooltip').velocity( window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn").delay(3000).velocity( window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
						}
					});
					$('.j-showBackFlights:eq(0)').fadeIn();

					$('html').velocity("scroll", {
						// offset: $('.j-titleReturn').offset().top - $('.booking__informationBoxCover').outerHeight() - $('.j-bookingRecapDep.isFixed').outerHeight(), // TODO: change this value
						offset: 0,
						complete: function(){
							//book.updateTopPosition();
						}
					});
					book.recapDeparture.velocity('transition.slideDownIn',{
						duration: 400,
						complete: function(){
							$('.j-bookingRecapDep').attr("style",'').show();
							book.updateTopPosition();
							initAccordions();
							if ($('.j-bookingTableReturn').length == 0){
								$('.bookingRecap .j-accordion-header').trigger('click');
							}
						}
					});
					$(window).trigger('resize.accordion');
				});

			}
		});

	},
	selectedReturn: function(e){
		book.destroyToolTip();
		var _this = $(this)
		var title_return=$('.j-titleReturn');
		var date_return =title_return.next();

		$('body').data('element_to_open_return',$(e.currentTarget).closest('.j-bookTableRow').find('.bookingTable__rightInner').find('.bookingTable__col.isActive'));

		$('.j-bookingTableReturn .j-bookingLoader').velocity('transition.slideDownIn',{
			duration: 250,
			complete: function(){
				// console.log('be_selectedReturn 1')
				be_selectedReturn(_this, function(){

					$('.j-bookingTableReturn .j-bookTableRow.activeRow').velocity("slideUp", {
						duration: 400 ,
						complete: function(){
							$('.j-bookingRecapRet').velocity('transition.slideDownIn',{
								duration: 250,
								complete: function(){

									$(window).trigger('resize.accordion');

									$('.j-bookingFlyoutTooltip').velocity( window.breakpointM > window.windowW ? "transition.slideUpIn" : "transition.slideLeftIn");

									date_return.velocity('slideUp');
									$('.j-titleReturn').velocity('slideUp');
									$('.j-bookingTableReturn').velocity('slideUp',{
										complete: function(){
											$('.j-bookingTableReturn .j-bookingLoader').velocity('transition.slideDownOut', {
												duration: 250,
												complete: function(){
													var newPos = $('.j-bookingRecapDep').offset().top - $('.j-bookInfoBox').outerHeight();

													$('html').velocity("scroll", {
														duration: 400,
														delay: 100,
														offset: newPos,
														complete: function(){


															$('.j-showBackFlights').fadeIn();
															//$('.bookingRecap .j-accordion').addClass('active');

															// anchoraggio carello anche dopo aver scelto due voli
															//window.bookingDisableFixed= true;
															window.bookingDisableFixedJustCard= true;
															book.resetFixedElement();
															//resize accordion
															$('.bookingRecap .j-accordion-header').each(function(){
																if (!$(this).hasClass('opening')){
																	$(this).trigger('click');	
																}
															});
															$( window ).trigger('resize.accordion');

															setTimeout(function(){
																$('.j-bookingFlyoutTooltip').velocity( window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
															},2000);
															initAccordions();
														}
													});

												}
											});
										}
									});
								}
							});
						}
					});
				}, function(){
					$('.j-bookingTableReturn .j-bookingLoader').velocity('transition.slideDownOut');
				});
			}
		});

	},
	loaderInit: function(which) {
		$('.j-bookingLoader').velocity('transition.slideDownIn',{
			duration: 250/*,
			complete: function(){
			}*/
		})
	},
	discountTooltip: function() {

		$('.j-bookingFlyoutTooltipDiscount, .bookInfoBoxBasket__ecopuonIcon').velocity( window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn", {
			complete: function(){
				setTimeout(function(){
					$('.j-bookingFlyoutTooltipDiscount').velocity( window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");	
				},1500);
			}
		});
	}
}
/* GENERIC FUNCTION */
function hideElementAndReplace(el, text) {
	if( !(el instanceof jQuery)) {
		var el = $(el).eq(0);
	}
	if(el.length > 0) {
		el.velocity("slideUp", {
			complete: function(){
				el.html(text);
				el.velocity("slideDown");
			}
		});
	}
}







$(function(){
	if ($('.j-upselling').length > 0){
		upsellingsManager.init();
	}
});
var upsellingsManager = {
	upsellings: null,
	tooltip: null,
	email: null,
	phone: null,
	init: function(){
		upsellings = $('.j-upselling');
		tooltip = $('.j-bookingFlyoutTooltip');
		loader = $('.j-bookingLoader');
		email = null;
		phone = null;
		for(var i=0;i< upsellings.length;i++){
			upselling = $(upsellings[i]);
			upsellingsManager.initStep(upselling);
		}
	},

	initStep : function(_section){
		steps = _section.find('.j-upsellingStep');
		_section.data('stepNum',steps.length);
			//currentStep
		activeStep = _section.find('.j-upsellingStep.currentStep');
		activeIndex = steps.index(activeStep)

		if(activeIndex !== -1){
			_section.data('step',activeIndex);
		}else{
			_section.data('step',0);
		}

		_section.data('startStep',_section.data('step'));

		stepBtn = _section.find('.j-upsellingBtn');
		cancelBtn = _section.find('.j-upsellingBtn-cancel');

		if (stepBtn.hasClass("j-changeData")) {
				email = steps.find('.mBpassengerBoxEmail__value');
				dataEmail = email[0].innerHTML;
				phone = steps.find('.mBpassengerBoxPhone__value');
				dataPhone = phone[0].innerHTML;
			};

		cancelBtn.unfastClick().fastClick(function(e){
			e.preventDefault();
			btn = $(e.currentTarget);
			sectionstep = $(e.currentTarget).closest('.j-upsellingStep');
			section = sectionstep.closest('.j-upselling');
			step = 0;
			section.data('step',step);
			hasFunction = $(e.currentTarget).attr('data-call') && $(e.currentTarget).attr('data-call')!== "" ? true : false;
			loader = section.find('.j-loader');
			sectionstep.hide();
			//cancellare dati reset dei componenti va aggiunto
			loader.velocity('transition.fadeIn',{
				duration: 500,
				complete: function(){
					if(hasFunction){
						window[$(e.currentTarget).attr('data-call')](section,function(){
							upsellingsManager.callBackEliminate(section);
						});
					}else{
						upsellingsManager.callBackEliminate(section);
					}
				}
			});
		})

		stepBtn.unfastClick().fastClick(function(e){
			e.preventDefault();
			btnE = e;
			btn = $(e.currentTarget);
			dir = $(e.currentTarget).hasClass('j-next') ? 1 : -1;
			sectionstep = $(e.currentTarget).closest('.j-upsellingStep');
			if(sectionstep.hasClass('insurance__teaser')){
				if(!sectionstep.find('.j-insuranceCheck').is(':checked')){
					sectionstep.find('.j-insuranceCheck').closest('.upselling__checkbox').addClass('isError')
					return;
				}else{
					sectionstep.find('.j-insuranceCheck').closest('.upselling__checkbox').removeClass('isError')
				}
			}
			section = sectionstep.closest('.j-upselling');
			hasFunction = $(e.currentTarget).attr('data-call') && $(e.currentTarget).attr('data-call')!== "" ? true : false;
			loader = section.find('.j-loader');
			sectionstep.hide();
			step = section.data('step');
			stepsNum = section.data('stepNum');
			step +=dir;
			section.data('step',step);
			loader.velocity('transition.fadeIn',{
				duration: 500,
				complete: function(){
					if(hasFunction){
						console.log(section);
						window[$(e.currentTarget).attr('data-call')](btnE, btn, section,function(){
							upsellingsManager.callBack(btnE,btn,step,section);
						});
					}else{
						upsellingsManager.callBack(btnE,btn,step,section);
					}
				}
			});
			if (email!==null && email.length>0) {
				changeData(section, dataEmail, dataPhone, stepBtn);
			};
		});
	},
	callBackEliminate: function(section){
		$(".j-continueButton").removeClass("isDisabled");
		loader = section.find('.j-loader');
		section.find('.j-upsellingStep').eq(0).show();
		section.find('.j-upsellingStep').removeClass('currentStep');
		section.find('.j-upsellingStep').eq(0).addClass('currentStep');

		$(window).trigger('resize');
		loader.velocity('transition.fadeOut',{
			duration: 250,
			complete: function(){
				newPos = section.closest('.j-accordion').offset().top - $('.j-bookInfoBox').outerHeight();
				if($('.j-bookInfoBox').length > 0 && $('.j-bookInfoBox').hasClass('opened')){
					newPos = newPos + $('.j-bookInfoBox').outerHeight()
				}
				$('html').velocity("scroll", {
					duration: 400,
					delay: 100,
					offset: newPos
				});
			}
		});
	},
	callBack : function(e,btn,step,section){
		$(".j-continueButton").toggleClass("isDisabled", step === 1);
		loader = section.find('.j-loader');
		showTooltip = btn.hasClass('j-addToCart') ? true : false;
		section.find('.j-upsellingStep').eq(step).show();
		section.find('.j-upsellingStep').removeClass('currentStep');
		section.find('.j-upsellingStep').eq(step).addClass('currentStep');
		if(section.find('.j-upsellingStep').eq(step).hasClass('booking__chooseSeat--choose')){
			if ($('.j-chooseSeatSlider').length > 0){
				$('.chooseSeat__sliderCover').iosSlider('destroy');
				bookingSeatSlider.sliderLoaded =  null;
				bookingSeatSlider.init();
			}
		}
		$(window).trigger('resize');
		loader.velocity('transition.fadeOut',{
			duration: 250,
			complete: function(){
				showTooltip ? upsellingsManager.fastShowTooltip() : null;
				newPos = section.closest('.j-accordion').offset().top - $('.j-bookInfoBox').outerHeight();
				if($('.j-bookInfoBox').length > 0 && $('.j-bookInfoBox').hasClass('opened')){
					newPos = newPos + $('.j-bookInfoBox').outerHeight()
				}
				$('html').velocity("scroll", {
					duration: 400,
					delay: 100,
					offset: newPos
				});
			}
		});
	},
	fastShowTooltip: function(){
		var tooltip = $('.j-bookingFlyoutTooltip');
		tooltip.velocity( window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn");
		setTimeout(function(){
			tooltip.velocity( window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
		},2000);
	},

	scrollToSection: function(section){
		if( !(section instanceof jQuery)) {
			var section = $(section);
		}
		if(section.length > 0 ) {
			var ib= $('.j-bookInfoBox');
			var mm = $('.mainMenu.posFixed');
			var newPos = section.closest('.j-accordion').offset().top - ib.outerHeight()-mm.outerHeight();
			if(ib.length > 0 && ib.hasClass('opened')){
				newPos = newPos + ib.outerHeight()
			}
			$('html').velocity("scroll", {
				duration: 400,
				delay: 100,
				offset: newPos
			});
		}
	}
}

function changeData(section, email, phone, btn) {
	newMail = section.find('.mBpassengerBox__newEmail').find('.form__element').find('.form__inputCover').find('input');
	newPhone = section.find('.mBpassengerBox__newPhone').find('.form__element').find('.form__inputCover').find('input');
	newMail.attr('value',email);
	newPhone.attr('value',phone);
	updateBtn = section.find('.j-updateBtn');
	updateBtn.click(function(e){
		updateMail = newMail[0].value;
		updatePhone = newPhone[0].value;
		email = section.find('.mBpassengerBoxEmail__updateValue');
		email[0].innerHTML = updateMail;
		phone = section.find('.mBpassengerBoxPhone__updateValue');
		phone[0].innerHTML = updatePhone;

	}) ;

}

function bookingSeatReady(){
	var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

	window.BookingSeat = (function() {
		function BookingSeat(ref) {

			// TODO FIXME temporary emergency exit fix
			//$('.nearEmergencyExit').parent().addClass('occupied').removeClass('available').removeClass('extraComfort');
			//$('.nearEmergencyExit').parent().html('&times;');

			this.ref = ref;
			this.setNextPassenger = bind(this.setNextPassenger, this);
			this.removePreviouslySelectedSeat = bind(this.removePreviouslySelectedSeat, this);
			this.seatsMapCloseMobileMode = bind(this.seatsMapCloseMobileMode, this);
			this.checkAllPassengers = bind(this.checkAllPassengers, this);
			this.setPassengerSeat = bind(this.setPassengerSeat, this);
			this.takeSeat = bind(this.takeSeat, this);
			this.verifySeat = bind(this.verifySeat, this);
			this.activePassenger = bind(this.activePassenger, this);
			this.getSeatType = bind(this.getSeatType, this);
			this.setPricePopOverPosition = bind(this.setPricePopOverPosition, this);
			this.setPriceOver = bind(this.setPriceOver, this);
			this.initPricePopOver = bind(this.initPricePopOver, this);
			this.isMobile = bind(this.isMobile, this);
			this.isMobileSize = bind(this.isMobileSize, this);
			this.loadSeats = bind(this.loadSeats, this);
			this.setLoadMoreSeatsBtns = bind(this.setLoadMoreSeatsBtns, this);
			this.setInfoBoxMapBtns = bind(this.setInfoBoxMapBtns, this);
			this.initSeatsMap = bind(this.initSeatsMap, this);
			this.checkIfExist = bind(this.checkIfExist, this);
			this.init = bind(this.init, this);
			this.loadInfo = bind(this.loadInfo, this);
			this.setOnResize = bind(this.setOnResize, this);
			this.allSeats = this.ref.find('.chooseSeat__allseats');
			this.passengerName = this.allSeats.find('.chooseSeat__passengerName--mobile');
			this.passengers = this.ref.find('.chooseSeat__passengerItem');
			this.loadSeatsBtn = this.ref.find('.selecRow');
			this.infoBoxMap = this.ref.find('.chooseSeat__infoBox');
			this.infoBoxBtns = this.infoBoxMap.find('[data-fila]');
			this.infoBoxImage = this.infoBoxMap.find('.image img');
			this.passengerIsSelected = false;
			this.actualPassengerNum = 0;
			this.comfortClass = 'extraComfort';
			this.occupiedClass = 'occupied';
			this.availableClass = 'available';
			this.assignedClass = 'seatAssigned';


			this.init();
		}
		BookingSeat.prototype.setPreviouslySelectedSeats = function() {
			for(var j=0;j<window.bookingSeatSelections.length;j++){
				selectedPassenger = window.bookingSeatSelections[j];
				if(this.ref.attr('data-flight') === selectedPassenger.flight){
					for (i = k = 0, ref = this.seats.length; 0 <= ref ? k < ref : k > ref; i = 0 <= ref ? ++k : --k) {
						seat = $(this.seats[i]);
						passenger = this.ref.find("[data-passenger="+selectedPassenger.passenger+"]");
						passenger.data('comfortSeatPrice', selectedPassenger.comfortSeatPrice);
						passenger.data('paymentSeatPrice', selectedPassenger.paymentSeatPrice);
						passenger.data('comfortSeatPriceCurrency', selectedPassenger.comfortSeatPriceCurrency);

						if(seat.data('num') === selectedPassenger.seatNumber){
							this.actualPassengerNum = passenger.index()
							number = passenger.find('.chooseSeat__selectedSeat .number');
							number.text(selectedPassenger.seatNumber);
							seat.hasClass('extraComfort') ? number.addClass('extraComfort') : number.removeClass('extraComfort');
							passenger.data('takenSeat', selectedPassenger.seatNumber);
							passenger.addClass('isSelected');
							seat.addClass('seatAssigned');
							seat.find('a').text(this.actualPassengerNum + 1);




						}
						if(selectedPassenger.isChangeable === 'false'){
							passenger.unfastClick();
						}
						if(selectedPassenger.hasFreeComfortSeat === 'true'){
							passenger.addClass('hasFreeComfortSeat');
						}
						if(selectedPassenger.isChild === 'true'){
							passenger.addClass('isChild');
						}
						if(selectedPassenger.hasInfant === 'true'){
							passenger.addClass('hasInfant');

						}
					}
				}
			}
			this.checkAllPassengers();
			return bookingSeatSlider.checkAllFlights();
		};
		BookingSeat.prototype.init = function() {
			var j, k, passenger, ref;
			if(window['bookingSeatSelections']===null || window['bookingSeatSelections']===undefined ){
				window.bookingSeatSelections = []
			}
			for (j = k = 0, ref = this.passengers.length; 0 <= ref ? k < ref : k > ref; j = 0 <= ref ? ++k : --k) {
				passenger = $(this.passengers[j]);
				passenger.data('num', j);
				passenger.data('takenSeat', null);
				passenger.fastClick((function(_this) {

					return function(e) {
						e.preventDefault();
						if(_this.isMobileSize() && _this.passengerName !== undefined){
							_this.passengerName.html($(e.currentTarget).find('.chooseSeat__passengerName').text());
						}
						// check for viewport and what to show
						return _this.activePassenger($(e.currentTarget));
					};
				})(this));
			}
			// fix for first passenger bug
			// if (!this.isMobileSize() && window.bookingSeatSelections.length===0) {
			if (!this.isMobileSize() ) {
				$(this.passengers[0]).click();
			}
			this.initSeatsMap();
			this.setLoadMoreSeatsBtns();
			this.setInfoBoxMapBtns();
			this.setOnResize();
			!this.isMobileSize() ? $(this.infoBoxBtns[0]).click() : null;
			window.bookingSeatSelections.length > 0 ? this.setPreviouslySelectedSeats() : null;
			if (!this.isMobileSize()) this.setFirstPassengerWithoutSeat();
		};
		BookingSeat.prototype.initSeatsMap = function() {
			var i, k, ref, seat;
			this.seats = this.ref.find('[data-col]');
			this.pricePopOver = this.ref.find('.chooseSeat__overlay');
			this.seatsMap = this.ref.find('.chooseSeat__passengerSeats');
			this.seatsMap.hide();
			this.pricePopOver.hide();
			var isMobile= this.isMobile();


			for (i = k = 0, ref = this.seats.length; 0 <= ref ? k < ref : k > ref; i = 0 <= ref ? ++k : --k) {
				seat = $(this.seats[i]);
				seat.data('num', seat.attr('data-row') + seat.attr('data-col'));
				var seat_type=this.getSeatType(seat);
				if (seat_type === 'available' || seat_type === 'comfort' || seat_type === 'nearEmergency') {
					seat.unfastClick().fastClick((function(_this) {
						return function(e) {
							e.preventDefault();
							if (_this.passengerIsSelected && _this.getSeatType($(e.currentTarget)) !== 'assigned') {
								return _this.verifySeat($(e.currentTarget));
							}
						};
					})(this));
				}

				if(!isMobile) {

					/*if (seat_type === 'comfort' || seat_type=== 'nearEmergency') {
						this.setPriceOver(seat);
					}*/
					this.setPriceOver(seat);


				}
			}



			if (this.isMobile()) {
				this.initPricePopOver();
			}
			if(this.isMobileSize()){
				return this.seatsMap.find('.j-overlayClose').fastClick((function(_this) {
					return function(e) {
						e.preventDefault();
						return _this.seatsMapCloseMobileMode();
					};
				})(this));
			}
		};

		BookingSeat.prototype.setInfoBoxMapBtns = function() {
			return this.infoBoxBtns.fastClick((function(_this) {
				return function(e) {
					e.preventDefault();
					if (!$(e.currentTarget).hasClass('active')) {
						_img_src = $(e.currentTarget).attr('data-image');
						_this.infoBoxImage.attr('src',_img_src);
						$(e.currentTarget).siblings().removeClass('active');
						_this.loadSeats($(e.currentTarget).attr('data-fila'));
						return $(e.currentTarget).addClass('active');
					}
				};
			})(this));
		};

		BookingSeat.prototype.setLoadMoreSeatsBtns = function() {
			return this.loadSeatsBtn.fastClick((function(_this) {
				return function(e) {
					if($('.chooseSeat__overlay').is(':visible')){
						$('.chooseSeat__overlay').hide();
					}
					e.preventDefault();
					($('.imageRows__text').find('.active')).removeClass('active');
					changeText = ($('.imageRows__text').find("[data-fila='" + $(e.currentTarget).attr('data-fila') + "']"))
					changeText.addClass('active');
					_img_src = changeText.attr('data-image');
					changeImage = $('.imageRows').find('.image img');
					changeImage.attr('src',_img_src);
					return _this.loadSeats($(e.currentTarget).attr('data-fila'));

				};
			})(this));
		};

		BookingSeat.prototype.loadSeats = function(_seats,_bl) {
			var i, k, ref, results, seats_map;
			results = [];
			_bl===undefined ? _bl=false: null;

			for (i = k = 0, ref = this.seatsMap.length; 0 <= ref ? k < ref : k > ref; i = 0 <= ref ? ++k : --k) {
				seats_map = $(this.seatsMap[i]);
				if (seats_map.attr('data-fila') === _seats) {
					results.push(seats_map.show());
					//_bl ? results.push(seats_map.find('.chooseSeat__seats').show()) : results.push(seats_map.find('.chooseSeat__seats').velocity("ransition.slideDownIn"));
				} else {
					//_bl ? results.push(seats_map.find('.chooseSeat__seats').hide()) : results.push(seats_map.find('.chooseSeat__seats').velocity("ransition.slideDownOut"));
					results.push(seats_map.hide());
				}
			}
			// fix for the chooseSeat__passengerSeats to be visible
			$('.chooseSeat__cabinClass.isBusiness').find('.chooseSeat__passengerSeats').show();
			$('.chooseSeat__cabinClass.isPremiumEconomy').find('.chooseSeat__passengerSeats').show();
			return results;
		};

		BookingSeat.prototype.isMobile = function() {
			if (window.ismobile || window.supportsTouch) {
				return true;
			} else {
				return false;
			}
		};

		BookingSeat.prototype.isMobileSize = function() {
			if (window.breakpointM > window.windowW) {
				return true;
			} else {
				return false;
			}
		};

		BookingSeat.prototype.initPricePopOver = function() {
			this.closeBtn = this.pricePopOver.find('.j-overlayPopupClose');
			this.verifyBtn = this.pricePopOver.find('.chooseSeat__btn');
			this.closeBtn.fastClick((function(_this) {
				return function(e) {
					e.preventDefault();
					return _this.pricePopOver.hide();
				};
			})(this));
			return this.verifyBtn.fastClick((function(_this) {
				return function(e) {
					e.preventDefault();
					_this.pricePopOver.hide();
					if(_this.getSeatType(_this.pricePopOver.data('seatData')) !== 'assigned'){
						return _this.takeSeat(_this.pricePopOver.data('seatData'));
					}
				};
			})(this));
		};

		BookingSeat.prototype.setText = function(_seat, _this) {

			var selectedPassenger = $('.chooseSeat__container:visible .chooseSeat__passengerItem.isActive');
			if (selectedPassenger.length === 0) { return }
			//_this.setPricePopOverPosition($(e.currentTarget));

			// pop up show casses and messages
			var hasInfant = selectedPassenger.hasClass('hasInfant');
			var isChild = selectedPassenger.hasClass('isChild');
			var nearEmergencyExit = _seat.find('.j-availableSeat').hasClass('nearEmergencyExit');
			var hasFreeComfortSeat = selectedPassenger.hasClass('hasFreeComfortSeat');


			var paymentSeatPrice = selectedPassenger.data('paymentSeatPrice');
			var hasPaymentSeatPrice = paymentSeatPrice  != "" && _seat.hasClass('available');

			var comfortSeat = _seat.hasClass('extraComfort');
			var passengerWithKids = ($('.chooseSeat__passengerItem.hasInfant').length + $('.chooseSeat__passengerItem.hasKids').length) === 0 ? false : true;

			var displayPopup = (comfortSeat && !hasFreeComfortSeat) || nearEmergencyExit || hasPaymentSeatPrice;

			var allowPopupAcquire = comfortSeat && !hasFreeComfortSeat && !(nearEmergencyExit && (hasInfant || isChild));
			var popupMessage = nearEmergencyExit ? ( (hasInfant || isChild) ? "blocking-warning-msg" : "non-blocking-warning-msg" ) : ( comfortSeat ? "comfort-msg" : (hasPaymentSeatPrice ? "payment-msg" : "") );
			//possible class
			//comfort-msg
			//non-blocking-warning-msg
			//blocking-warning-msg


			$('.chooseSeat__overlayTextCover .editorialArea').removeClass('activated hasWarning');
			$('.chooseSeat__overlay .price').show();
			$('.chooseSeat__overlay .chooseSeat__btn').attr('style', '');
			if (comfortSeat) {
				$('.chooseSeatOverlay__txt .text').show();
				$('.chooseSeatOverlay__txt .textEmpty').hide();
			} else {
				$('.chooseSeatOverlay__txt .text').hide();
				$('.chooseSeatOverlay__txt .textEmpty').show();
			}
			$('.chooseSeat__overlay .price').show();

			if(displayPopup) {
				if ( popupMessage === "blocking-warning-msg"){
					$('.j-textPopup-emergencyBlocking').addClass('activated hasWarning');
					$('.chooseSeat__overlay .chooseSeat__btn').hide();
				} else if ( popupMessage === "non-blocking-warning-msg"){
					$('.j-textPopup-emergency').addClass('activated hasWarning');
				} else if ( popupMessage === "comfort-msg" ) {
					$('.j-textPopup-default').addClass('activated');
				} else if ( popupMessage === "payment-msg" ) {
					$('.j-textPopup-payment').addClass('activated');
				}
				//hide price if free

				if(hasFreeComfortSeat && popupMessage === "comfort-msg") {
					$('.chooseSeat__overlay .price').hide();
				}
				if(!comfortSeat && !hasPaymentSeatPrice) {
					$('.chooseSeatOverlay__txt .text, .chooseSeat__overlay .price').hide();
				}

				//active map
				var activeState;
				$(this.seatsMap).each(function(x,y){
					if ( $(this).is(':visible') ) {
						activeState = x;
					}
				});
				var currentSeatMap=$(this.seatsMap[activeState]);
				var seatOverlay = currentSeatMap.find('.chooseSeat__overlay');
				var pricenumber = seatOverlay.find('.priceNumber');
				var currency = seatOverlay.find('.currency');
				var number = seatOverlay.find('.number');

				//fil text pop upand show with price
				pricenumber.empty();
				currency.empty();
				number.empty();

				if( selectedPassenger.length > 0 ) {
					pricenumber.text(selectedPassenger.data((comfortSeat) ?'comfortSeatPrice' :'paymentSeatPrice'));
					currency.html(selectedPassenger.data('comfortSeatPriceCurrency'));
					number.text(_seat.data('num'));
				}

				_this.setPricePopOverPosition(_seat);
				return _this.pricePopOver.show();

			} else {
				if(hasFreeComfortSeat && popupMessage === "comfort-msg") {
					$('.chooseSeat__overlay .price').hide();
				}
				return _this.pricePopOver.hide();
			}

		}

		BookingSeat.prototype.setPriceOver = function(_seat) {
			return _seat.unbind('mouseenter mouseleave').hover((function(_this) {
				return function(e) {
					_this.setText(_seat, _this);
					//commented called from _seat included in _seat
					//_this.setPricePopOverPosition($(e.currentTarget));

				};
			})(this), (function(_this) {
				return function(e) {
					$('.j-textPopup-default').removeClass('activated');
					$('.j-textPopup-emergency').removeClass('activated hasWarning');
					$('.j-textPopup-emergencyBlocking').removeClass('activated hasWarning');
					return _this.pricePopOver.hide();
				};
			})(this));
		};

		BookingSeat.prototype.setPricePopOverPosition = function(_seat) {
			var _left, _top, ppoHeight, ppoWidth, seatHeight, seatPos, seatWidth, seatsMapHeight, seatsMapPos, activeState;
			$(this.seatsMap).each(function(x,y){
				if ( $(this).is(':visible') ) {
					activeState = x;
				}
			});
			var currentSeatMap=$(this.seatsMap[activeState]);



			seatPos = _seat.offset();
			seatsMapPos = currentSeatMap.offset();
			seatsMapHeight = currentSeatMap.height();
			seatHeight = _seat.outerHeight();
			seatWidth = _seat.width();


			var selectedPassenger = $('.chooseSeat__container:visible .chooseSeat__passengerItem.isActive');
			var __overlay = currentSeatMap.find('.chooseSeat__overlay')
			/*if (selectedPassenger.length > 0) {
				__overlay.find('.priceNumber').text(selectedPassenger.data('comfortSeatPrice'));
				__overlay.find('.currency').html(selectedPassenger.data('comfortSeatPriceCurrency'));
			} else {
				__overlay.find('.priceNumber').text('');
				__overlay.find('.currency').text('');
			}
			__overlay.find('.number').text(_seat.data('num'));*/

			__overlay.show()
			ppoHeight = __overlay.outerHeight();
			ppoWidth = __overlay.width();


			if (ppoHeight === 0) {
				if( window.windowW > window.breakpointM){
					ppoHeight = 229;

				} else {
					ppoHeight = 111;
				}

			}

			if (seatPos.top - ppoHeight < seatsMapPos.top) {
				_top = seatPos.top - seatsMapPos.top + seatHeight + 12;
				__overlay.removeClass('up');
				__overlay.addClass('down');
			} else {
				_top = seatPos.top - seatsMapPos.top - ppoHeight - seatHeight + 12;
				//_top = seatPos.top - seatsMapPos.top - ppoHeight - seatHeight + 12;
				__overlay.removeClass('down');
				__overlay.addClass('up');
			}
			_left = seatPos.left - seatsMapPos.left - ppoWidth / 2;

			if (_left < 0 && this.comfortClass){
				__overlay.removeClass('right middle').addClass('left');
				_left += 80
			}
			else if (_left + 15 > $(this.seatsMap[activeState]).width() - ppoWidth) {
			// 	_left = seatPos.left - ppoWidth + seatWidth;
				__overlay.removeClass('left middle').addClass('right');
				_left -= 80
			} else {
				__overlay.removeClass('left right').addClass('middle');
			}

			return __overlay.css({
				'top': _top + 'px',
				'left': _left + 'px'
			});
		};

		BookingSeat.prototype.getSeatType = function(_seat) {
			var _seat_type = 'noType'
			if (_seat.hasClass(this.comfortClass)) {
				_seat_type = _seat.hasClass(this.assignedClass) ? 'assigned' : 'comfort';
			} else if (_seat.hasClass(this.availableClass)) {
				_seat_type = _seat.hasClass(this.assignedClass) ? 'assigned' : 'available';
			} else if (_seat.hasClass(this.occupiedClass)) {
				_seat_type = 'occupied';
			}
			if (_seat.children('.nearEmergencyExit').length != 0){
				_seat_type = 'nearEmergency';
			}
			return _seat_type;
		};

		BookingSeat.prototype.activePassenger = function(_passenger) {
			this.passengerIsSelected = true;
			_passenger.siblings().removeClass('isActive');
			_passenger.addClass('isActive');
			var pasClass = _passenger.attr('data-cabinclass');
			var whatToShow = _passenger.closest('.chooseSeat__container').find('.chooseSeat__cabinClass.'+pasClass);
			if (!whatToShow.is(':visible')){
				_passenger.closest('.chooseSeat__container').find('.chooseSeat__cabinClass').hide();
				if (this.isMobileSize()) {
					whatToShow.show();
				} else {
					whatToShow.velocity('transition.slideDownIn');
				}
			}

			if (this.isMobileSize()) {
				if(flightsCont) {
					for (var j=0;j<flightsCont.length;j++){
						$(flightsCont[j])[0].style[Modernizr.prefixed('transform')] = "none";
					}

			        $('.booking__informationBox').css('z-index', 0);
			        $('#box_bag').css('z-index', 0);
			        $('#box_insurance').css('z-index', 0);
			        $('#box_seat').css('z-index', 0);
			        $('#box_menu').css('z-index', 0);
					
					$('body').addClass('overlayOpenSeats');
					this.seatsMap.velocity("transition.expandIn");
					//this.seatsMap.find('.chooseSeat__seats').show();
				}
			}

		};

		BookingSeat.prototype.verifySeat = function(_seat) {
			var selectedPassenger = $('.chooseSeat__container:visible .chooseSeat__passengerItem.isActive');
			if (selectedPassenger.length === 0) { return }
			//_this.setPricePopOverPosition($(e.currentTarget));

			// pop up show casses and messages
			var hasInfant = selectedPassenger.hasClass('hasInfant');
			var isChild = selectedPassenger.hasClass('isChild');
			var nearEmergencyExit = _seat.find('.j-availableSeat').hasClass('nearEmergencyExit');
			var blockdSeat= nearEmergencyExit && (hasInfant || isChild);

			var paymentSeatPrice = selectedPassenger.data('paymentSeatPrice');
			var hasPaymentSeatPrice = paymentSeatPrice != undefined &&  paymentSeatPrice != "" && _seat.hasClass('available');

			if (this.getSeatType(_seat) === 'comfort' && this.isMobile() || this.getSeatType(_seat) === 'nearEmergency' && this.isMobile() ||
					hasPaymentSeatPrice && this.isMobile()) {

				this.setText(_seat, this);
				//this.setPricePopOverPosition(_seat);
				this.pricePopOver.data('seatData', _seat);
				return this.pricePopOver.show();
			} else {
				if(!blockdSeat) {
					return this.takeSeat(_seat);
				}
			}
		};

		BookingSeat.prototype.takeSeat = function(_seat) {
			var col, row, seatNumber;
			col = _seat.attr('data-col');
			row = _seat.attr('data-row');
			seatNumber = row + col;
			return this.setPassengerSeat(seatNumber, _seat);
		};

		BookingSeat.prototype.setPassengerSeat = function(_seatNumber, _seat) {
			var  j, k, passenger, ref;
			this.passengerIsSelected = false;
			for (j = k = 0, ref = this.passengers.length; 0 <= ref ? k < ref : k > ref; j = 0 <= ref ? ++k : --k) {
				passenger = $(this.passengers[j]);
				if (passenger.hasClass('isActive')) {

					number = passenger.find('.chooseSeat__selectedSeat .number');
					number.text(_seatNumber);

					_seat.hasClass('extraComfort') ? number.addClass('extraComfort') : number.removeClass('extraComfort');
					passenger.removeClass('isActive');
					if (passenger.data('takenSeat') !== null) {
						this.removePreviouslySelectedSeat(passenger.data('takenSeat'));
					}
					if($('.chooseSeat__overlay').is(':visible')){
						$('.chooseSeat__overlay').hide();
					}
					passenger.data('takenSeat', _seatNumber);
					passenger.addClass('isSelected');
					this.actualPassengerNum = passenger.data('num');
					_seat.addClass('seatAssigned');
					_seat.find('a').text(this.actualPassengerNum + 1);


					if(!this.checkIfExist(this.ref.attr('data-flight'),passenger.attr('data-passenger'),_seatNumber)){
						window.bookingSeatSelections.push({
							'passenger': passenger.attr('data-passenger'),
							'flight': this.ref.attr('data-flight'),
							'seatNumber': _seatNumber,
							'isChangeable': "true"
						})
					}

					break;
				}
			}
			if (this.isMobileSize()) {
				this.seatsMapCloseMobileMode();
			} else {
				this.setNextPassenger();
			}
			this.checkAllPassengers();
			return bookingSeatSlider.checkAllFlights();
		};

		BookingSeat.prototype.checkIfExist = function(_flight,_passenger,_seatNumber){
			var __isExist = false
			if(window.bookingSeatSelections.length > 0){
				for(var i=0;i<window.bookingSeatSelections.length;i++){
					___flight = window.bookingSeatSelections[i].flight
					___passenger = window.bookingSeatSelections[i].passenger
					if( ___passenger === _passenger && ___flight === _flight){
						window.bookingSeatSelections[i].seatNumber = _seatNumber;
						__isExist = true;
						break;
					}else{
						__isExist = false;
					}
				}
			}else{
				__isExist = false;
			}
			return __isExist;
		};
		BookingSeat.prototype.checkAllPassengers = function() {
			var b, j, k, passenger, ref, one, minDone;
			b = 0;
			minDone = false;
			one = false
			for (j = k = 0, ref = this.passengers.length; 0 <= ref ? k < ref : k > ref; j = 0 <= ref ? ++k : --k) {
				passenger = $(this.passengers[j]);
				if (passenger.hasClass('isSelected')) {
					b += 1;
					minDone = true;
					one = true;
				}
			}
			if (b === this.passengers.length) {
				this.passengers.closest('.chooseSeat__container').addClass('done');
			}
			if(minDone === true){
				this.passengers.closest('.chooseSeat__container').addClass('min-done');
			}
			if(one === true){
				this.passengers.closest('.chooseSeat__container').addClass('one');
			}
		};

		BookingSeat.prototype.seatsMapCloseMobileMode = function() {
			this.pricePopOver.hide();
			
	        $('.booking__informationBox').css('z-index', 80);
	        $('#box_bag').css('z-index', 38);
	        $('#box_insurance').css('z-index', 38);
	        $('#box_seat').css('z-index', 38);
	        $('#box_menu').css('z-index', 38);
			
			$('body').removeClass('overlayOpenSeats');
			return this.seatsMap.velocity("transition.expandOut");
		};

		BookingSeat.prototype.removePreviouslySelectedSeat = function(_takenSeat) {
			var i, k, ref, results, seat;
			results = [];
			for (i = k = 0, ref = this.seats.length; 0 <= ref ? k < ref : k > ref; i = 0 <= ref ? ++k : --k) {
				seat = $(this.seats[i]);
				if (seat.data('num') === _takenSeat) {
					seat.removeClass('seatAssigned');
					results.push(seat.find('a').text(''));
				} else {
					results.push(void 0);
				}
			}
			return results;
		};

		BookingSeat.prototype.setNextPassenger = function() {
			var nextPassenger;
			nextPassenger = this.actualPassengerNum + 1 < this.passengers.length ? $(this.passengers[this.actualPassengerNum + 1]) : $(this.passengers[0]);

			// if (nextPassenger.data('takenSeat') === null) {
				return this.activePassenger(nextPassenger);
			// }
		};

		BookingSeat.prototype.setFirstPassengerWithoutSeat = function() {
			for(var j=0; j < this.passengers.length; j++ ) {
				var pas=$(this.passengers[j]);
				if( pas.data('takenSeat') === null) {
					return this.activePassenger(pas);
				}
			}
		};

		BookingSeat.prototype.setOnResize = function() {
			return $(window).on('resize', (function(_this) {
				return function() {
					var _data_fila, _infoBoxbtn, j, k, ref, results;
					if ($(window).width() < 641) {
						return _this.seatsMap.hide();
					} else {
						results = [];
						for (j = k = 0, ref = _this.infoBoxBtns.length; 0 <= ref ? k < ref : k > ref; j = 0 <= ref ? ++k : --k) {
							_infoBoxbtn = $(_this.infoBoxBtns[j]);
							if (_infoBoxbtn.hasClass('active')) {
								_data_fila = _infoBoxbtn.attr('data-fila');
								results.push(_this.loadSeats(_data_fila,true));
							} else {
								results.push(void 0);
							}
						}
						return results;
					}
				};
			})(this));
		};
		return BookingSeat;
	})();

	var __bookingSeat = $('.chooseSeat__container');
	if (__bookingSeat.length > 0){
		for (var j=0;j<__bookingSeat.length;j++){
			new BookingSeat($(__bookingSeat[j]));
		}
	}
}
bookingSeatReady();
var bookingSeatSlider = {
	slider: null,
	sliderCont: null,
	nextBtn: null,
	prevBtn: null,
	offsetSlide: null,
	pos: null,
	bookingSlider: null,
	itemNum: null,
	itemWidth: null,
	slidersBtns: null,
	flightsCont: null,
	upsellingBtn: null,
	sliderLoaded: null,
	windowWidth: $(window).width(),
	init: function(){
		if(bookingSeatSlider.sliderLoaded !== null){
			$('.chooseSeat__sliderItem')[0].click();
		}else{
			bookingSeatSlider.sliderLoaded = true;
			sliderCont =  $('.j-chooseSeatSlider').find('.booking__date');
			upsellingBtn = $('.j-chooseSeatSlider').closest('.j-upsellingStep').find('.j-upsellingBtn');
			upsellingBtn.hide();
			nextFlightBtns = $('.js-next-flight');
			slider = sliderCont.find('.chooseSeat__sliderCover');
			sliderList = sliderCont.find('.chooseSeat__sliderList');
			nextBtn = sliderCont.find('a.next');
			prevBtn = sliderCont.find('a.prev');
			flightsCont = $('.chooseSeat__container');
			flightsCont.hide();
			slidersBtns = slider.find('.chooseSeat__sliderItem');
			pos = slider.find('.chooseSeat__sliderItem .isActive').index();
			itemNum = slider.find('.chooseSeat__sliderItem').length;
			itemWidth = slider.find('.chooseSeat__sliderItem').outerWidth();
			offsetSlide = 2;
			setTimeout(function(){
				if (itemNum*itemWidth > slider.outerWidth()){
						bookingSeatSlider.setSlider();

				}else{
					nextBtn.hide();
					prevBtn.hide();
				}
				bookingSeatSlider.setOnResize();
				bookingSeatSlider.initBtns();
				bookingSeatSlider.initNextFlightBtns();

			},400)
			//$('.chooseSeat__sliderItem')[0].click();

		}
	},
	initBtns : function(){
		for(var j=0;j<slidersBtns.length;j++){
			var sliderBtn = $(slidersBtns[j]);

			sliderBtn.unbind().click(function(e){
				e.preventDefault();
				var flight = $(e.currentTarget).attr('data-flight');
				bookingSeatSlider.setFlight(flight);
			});
		}
		$(slidersBtns[0]).click();
	},
	isAndroid: function(){
		var ua = navigator.userAgent.toLowerCase();
		var isAndroid = ua.indexOf("android") > -1;
		if(isAndroid){
			return true;
		}else{
			return false;
		}
	},
	initNextFlightBtns: function(){
		for(var i=0;i<nextFlightBtns.length;i++){
			var nextFlightBtn = $(nextFlightBtns[i]);
			nextFlightBtn.unbind().click(function(e){

				e.preventDefault();
				__flight = $(e.currentTarget).attr('data-flight');
				//_seatCont = $('.chooseSeat__container[data-flight="'+__flight+'"]');
				//_numSlide = $('.chooseSeat__sliderItem[data-flight="'+__flight+'"]').index() + 1;
				//$('.chooseSeat__sliderCover:eq(0)').iosSlider('goToSlide', _numSlide);

				// if(!_seatCont.hasClass('done')) {
				// 	bookingSeatSlider.setFlight(__flight);
				// }else{
				// 	bookingSeatSlider.findNextFlightToSetSeats(flightsCont.index(_seatCont));
				// }
				bookingSeatSlider.setFlight(__flight);
			})
		}
	},
	findNextFlightToSetSeats: function(_index){
		_index= _index+1 < flightsCont.length ? _index+1 : 0;
		_next_flight = $(flightsCont[_index]);
		!_next_flight.hasClass('done') ? bookingSeatSlider.setFlight(_next_flight.attr('data-flight')) : bookingSeatSlider.findNextFlightToSetSeats(_index);
	},
	setFlight: function(_flight){

		if(bookingSeatSlider.bookingSlider !== null) {
			slider.iosSlider('update');
			var _numSlide = $('.chooseSeat__sliderItem[data-flight="'+_flight+'"]').index() + 1;
			$('.chooseSeat__sliderCover:eq(0)').iosSlider('goToSlide', _numSlide);
		}

		for(var k=0;k<slidersBtns.length; k++){
			var sliderBtn = $(slidersBtns[k]);
			if(sliderBtn.attr('data-flight')===_flight){
				sliderBtn.addClass('isActive');
			}else{
				sliderBtn.removeClass('isActive');
			}
		}
		for(var i=0;i<flightsCont.length;i++){
			var flight = $(flightsCont[i]);
			if(flight.attr('data-flight') === _flight){
				bookingSeatSlider.checkPrevFlight(i);
				flight.velocity("transition.slideDownIn",{
					complete: function(){
						if(bookingSeatSlider.isAndroid()){
							$(window).trigger('resize');
						}
					}
				});
				$('html').velocity("scroll", {
					offset: slidersBtns.offset().top - $(".mainMenu[data-fixedheader='false']").outerHeight() -$('.j-bookInfoBox').outerHeight()
				});


			}else{
				flight.hide()
			}
		}


		$(window).trigger('resize');
		bookingSeatSlider.checkAllFlights();
	},
	checkPrevFlight: function(_index){
		if(_index !==0){
			for(var j=_index;j>-1;j--){
				_prev_flight = $(flightsCont[j]);
				if(!_prev_flight.hasClass('min-done')){
					_prev_flight.addClass('min-done');
				}
			}
		}
	},
	setOnResize : function (){
		$(window).on('resize',function(){
			var ww=$(window).width();
			if(ww != bookingSeatSlider.windowWidth ) {
				if ($('html').hasClass('android')) {
					 slider.iosSlider('destroy');
					 bookingSeatSlider.bookingSlider= null;
				}
				if(bookingSeatSlider.bookingSlider !== null) slider.iosSlider('update');

				if (itemNum*itemWidth > slider.outerWidth()){
					if(bookingSeatSlider.bookingSlider === null) {
						bookingSeatSlider.setSlider();
					}
					nextBtn.show();
					prevBtn.show();
				}else{
					nextBtn.hide();
					prevBtn.hide();
				}
			}
			bookingSeatSlider.windowWidth=ww;
		});
	},
	checkAllFlights: function(){
		var t = 0;
		var m = 0;
		var flightsCont = $('.chooseSeat__container');
		var nextFlightBtns = $('.js-next-flight');
		var upsellingBtn = $('.j-chooseSeatSlider').closest('.j-upsellingStep').find('.j-upsellingBtn');
		for(var i=0;i<flightsCont.length;i++){
			var flight = $(flightsCont[i]);
			//flight.hasClass('done') ? t+=1 : null;
			flight.hasClass('min-done') ? m+=1 : null;
			flight.hasClass('one') ? t+=1 : null;
		}
		if(t!==0 && m===flightsCont.length){
			nextFlightBtns.hide();
			upsellingBtn.show();
			$( window ).trigger('resize.accordion');
		}
	},
	setSlider: function(){
		if(slider!==null && slider!==undefined){
			var current=slider.find(".chooseSeat__sliderItem.isActive").index() +1;

			this.bookingSlider = slider.iosSlider({
				desktopClickDrag: true,
				snapToChildren: true,
				autoSlide: false,
				infiniteSlider: false,
				startAtSlide: current
			});


			slider.data('args').currentSlideNumber = current;
			nextBtn.unbind().click(function() {
				var ct= slider.data('args').currentSlideNumber;
				slider.iosSlider('goToSlide', ct+1);
			});
			prevBtn.unbind().click(function() {
				var ct= slider.data('args').currentSlideNumber;
				slider.iosSlider('goToSlide', Math.max(ct-1,1));
			});
		}
	}
}

$(function(){
	if ($('.j-multiJourney').length > 0){
		multiJourney.init();
	}
	if($('.j-journeyType').length > 0){
		findFlightMenu.init()
	}
});
var findFlightMenu = {
	menuItems: null,
	multiJourneyForm: null,
	signleDestinationForm:null,
	searchBtn: null,
	init: function(){
		menuItems = $('.j-journeyType');
		multiJourneyForm = $('.j-multiJourney');
		signleDestinationForm = $('.j-internalFlightFinder');
		searchBtn = $('.multiJourney__searchBtn >a');
		multiJourneyForm.hide();
		signleDestinationForm.hide();
		for(var i=0;i<menuItems.length;i++){
			menuItem = $(menuItems[i]);
			menuItem.find('input.j-typeOfFlight').change(function(e){
				if($(e.currentTarget).is(":checked")){
					if($(e.currentTarget).closest('.j-journeyType').attr('data-multijourney') ==='false'){
						signleDestinationForm.show();
						multiJourneyForm.hide();
						searchBtn.removeClass('isDisabled');
					}else{
						multiJourneyForm.show();
						signleDestinationForm.hide();
						searchBtn.addClass('isDisabled');
						multiJourney.checkSearchBtn();
					}
				}
			})
		}
		menuItems.eq(0).find('input.j-typeOfFlight').attr('checked', true).change();
	}
}
var multiJourney = {
	multiJourneyItem: null,
	addJourneyBtn: null,
	searchBtn: null,
	stateA: '.multiJourney__teaser',
	stateB: '.multiJourney__choose',
	stateC: '.multiJourney__modify',
	init:function(){
		multiJourneyItems = $('.j-multiJourney').find('.j-multiJourneyItem');
		addJourneyBtn = multiJourneyItems.find('.j-addJourney');
		modifyJourneyBtn = multiJourneyItems.find('.multiJourney__modifyBtn');
		confirmModifiedBtn = multiJourneyItems.find('.j-confirmModifiedJourney');
		searchBtn = $('.multiJourney__searchBtn >a');


		for(var i=0;i<multiJourneyItems.length;i++){
			// multiJourneyItems = $(multiJourneyItems[i]);
			// multiJourneyItem.find('.j-autoCompleteInput.departure').attr('id','ac_departure_'+i);
			// multiJourneyItem.find('.j-autoCompleteInput.arrival').attr('id','ac_arrival_'+i);
			// multiJourney.setAutoComplete($('#ac_departure_'+i),$('#ac_arrival_'+i));
			// multiJourney.setAutoComplete($('#ac_arrival_'+i));
			multiJourney.setDatePickers('multi-journey_'+i);
		}
		modifyJourneyBtn.fastClick(function(e){
			e.preventDefault();
			btn = $(e.currentTarget);
			journeyItem = btn.closest('.j-multiJourneyItem');
			journeyItem.addClass('modify');
			journeyItem.find(multiJourney.stateC).hide();
			journeyItem.find(multiJourney.stateB).show();

		})
		confirmModifiedBtn.fastClick(function(e){
			e.preventDefault();
			btn = $(e.currentTarget);
			journeyItem = btn.closest('.j-multiJourneyItem');
			journeyItem.removeClass('modify');
			_index = multiJourneyItems.index(journeyItem)
			multiJourney.setPrevJourney(_index);
			multiJourney.checkAllDepartureData(_index);
		})
		addJourneyBtn.fastClick(function(e){
			e.preventDefault();
			btn = $(e.currentTarget);
			journeyItem = btn.closest('.j-multiJourneyItem');
			_index = multiJourneyItems.index(journeyItem) - 1;
			next_index = multiJourneyItems.index(journeyItem) + 1;
			if(multiJourney.UnblockNextStep(_index)){
				journeyItem.find(multiJourney.stateA).hide();
				journeyItem.find(multiJourney.stateB).show();
				multiJourney.setLowRangeDatePicker(_index);
				multiJourney.setPrevJourney(_index);
				next_index < multiJourneyItems.length ? multiJourney.enableAddingNextJourney(next_index) : null;
			}
		})
		multiJourneyItems.hide();
		multiJourneyItems.eq(0).show();
		multiJourneyItems.eq(1).show();
	},
	checkSearchBtn : function(){
		multiJourneyItems_selected = $('.j-multiJourney').find('.j-multiJourneyItem.selected');
		multiJourneyItems_selected.length > 1 ? searchBtn.removeClass('isDisabled') : null;
	},
	checkAllDepartureData : function (index){
		for(var i=index+1;i<multiJourneyItems.length;i++){
			multiJourneyItem = $(multiJourneyItems[i]);

			if(multiJourneyItem.data('selected')===true){
				_stateB = multiJourneyItem.find(multiJourney.stateB);
				_stateC = multiJourneyItem.find(multiJourney.stateC);
				_stateC.hide();
				_stateB.show();
				multiJourneyItem.addClass('modify');
				_dateOfFlight = _stateB.find('.dateOfFlight');
				_dateOfFlight.addClass('hasError');
				_dateOfFlight.find('li:eq(0)').append('<a class="form__errorIcon" href="#multiJourneyerrorField'+i+'" aria-describedby="multiJourneyerrorField'+i+'"></a><div class="form__errorField" id="multiJourneyerrorField'+i+'">la data deve essere successiva alla tratta precedente</div>');
			}

			multiJourney.setLowRangeDatePicker(i-1);
		}
	},
	enableAddingNextJourney: function(index){
		multiJourneyItems.eq(index).show();
		_stateA = multiJourneyItems.eq(index).find(multiJourney.stateA);
		index > 2 ? searchBtn.removeClass('isDisabled') : null;
		_stateA.show();
	},
	UnblockNextStep: function(index){
		_unblockStep = false;
		_stateB = multiJourneyItems.eq(index).find(multiJourney.stateB);
		_dep = _stateB.find('.customInput--flightFinder.departure');
		_rtrn = _stateB.find('.customInput--flightFinder.arrival');
		if(multiJourneyItems.eq(index).hasClass('selected') && _dep.hasClass('selected') && _rtrn.hasClass('selected')){
			_unblockStep = true;
		}
		return _unblockStep;
	},
	setLowRangeDatePicker :function(index){
		datePickerController.setRangeLow(('multi-journey_'+(index+1)), datePickerController.getSelectedDate('multi-journey_'+index));
	},
	setPrevJourney : function(index){
		_stateB = multiJourneyItems.eq(index).find(multiJourney.stateB);
		_stateC = multiJourneyItems.eq(index).find(multiJourney.stateC);
		_dep = _stateB.find('.customInput--flightFinder.departure').find(".apt").text();
		_rtrn = _stateB.find('.customInput--flightFinder.arrival').find(".apt").text();
		_stateB.hide();
		_stateC.show();
		infoFlight = _stateC.find('.multiJourneyInfoFlight');
		_departure = infoFlight.find('.departure');
		_arrival = infoFlight.find('.return');
		_date = infoFlight.find('.date');
		_departure.text(_dep);
		_arrival.text(_rtrn);
		_date.text(datePickerController.getSelectedDate('multi-journey_'+index).toLocaleDateString())
		multiJourneyItems.eq(index).data('selected',true);
	},
	setDatePickers : function(dp){
		var formElts = {};
		formElts[dp] = pageSettings.dateFormat;
		datePickerController.createDatePicker({
			formElements: formElts,
			"noTodayButton": true,
			"fillGrid" : true,
			rangeLow : new Date(),
			rangeHigh : maxRange,
			constrainSelection: false,
			noFadeEffect: true,
			wrapped:true,
			callbackFunctions:{
				"dateset":[multiJourney.enableNextJourney]
			}
		});
	},
	enableNextJourney: function(dp){
		if(datePickerController.getSelectedDate(dp.id)!==null){
			journeyItem = $('#'+dp.id).closest('.j-multiJourneyItem');

			journeyItem.addClass('selected');
			if(journeyItem.find('.dateOfFlight').hasClass('hasError')){
				journeyItem.find('.dateOfFlight').removeClass('hasError');
				journeyItem.remove('.form__errorIcon');
				journeyItem.remove('.form__errorField');
			}
		}
	},
	setAutoComplete: function(inInput,outInput){
		inInput.autocomplete({
			serviceUrl: '/data/airport.json',
			preserveInput: false,
			appendTo: inInput.parent(),
			minChars: 3,
			onSearchComplete : function (val, suggestions) {
				//update with value
				for (var j=0; j < suggestions.length; j++ ) {
					var obj= suggestions[j];
					obj.value = obj.city + " " + obj.type;
				}
			},
			formatResult: function (suggestion, currentValue) {
				return '<div class="sugg_dest_data"><span class="sugg_city_name toPass ' + (suggestion.best ? "best" : "") + '">'
					+ suggestion.city + '</span> - <span class="sugg_city_nation ">'
					+ suggestion.country + '</span><br><span class="sugg_airport">'
					+ suggestion.airport + '</span><span class="sugg_airportCode">('
					+ suggestion.code + ')</span></span></div>';
			},
			onInvalidateSelection: function () {},
			onSelect: function (suggestion) {
				$(this).val(suggestion.value);
				$(this).siblings(".apt").text(suggestion.city);
				$(this).siblings(".city").text(suggestion.type);
				$(this).siblings("#"+$(this).attr("id")+"_country").val(suggestion.code);
				$(this).siblings(".j-countryCode").val(suggestion.code);
				$(this).closest('.customInput--flightFinder').addClass('selected');
				outInput!==undefined && outInput!==null ? outInput.focus() : null;
		   }
    });
	}
}




var awardsDatePicker = {
	init: function(dp, bAwardData){
		var _id = dp.attr('id');
		var formElts = {};
		formElts[_id] = pageSettings.dateFormat;
		datePickerController.createDatePicker({
			formElements:formElts,
			staticPos: true, // position static
			hideInput: true, // hide input
			noTodayButton: true,
			fillGrid: true,
			rangeLow: bAwardData.rangeLow,
			rangeHigh: maxRange,
			noFadeEffect: true,
			constrainSelection: false,
			wrapped:true,
			callbackFunctions:{
				dateset: [function(formElts){
					if(formElts['id']==="bookingAward_andata"){
						setReturn()
					}else{
						setDeparture()
					}
				}],
				redraw: [function(data){
					setTimeout(function(){
						$("#fd-"+_id+" tbody td").each(function(){
							var
								$this=$(this),
								classes=$this.attr("class").split(/\s+/),
								cellContent,day,daynumber,monthnumber,yearnumber;
							if(!$this.is(".out-of-range")){
								for(var i in classes){
									if(classes[i].substr(0,9)==="yyyymmdd-"){
										yearnumber=parseInt(classes[i].substr(-8,4));
										monthnumber=parseInt(classes[i].substr(-4,2));
										day=classes[i].substr(-2);
										daynumber=parseInt(day);
										break;
									}
								}
								if(bAwardData[yearnumber]&&bAwardData[yearnumber][monthnumber]&&bAwardData[yearnumber][monthnumber][daynumber]){
									cellContent='';
									if(bAwardData[yearnumber][monthnumber][daynumber].type !== undefined){
										cellContent +='<p class="bookingAward__dot">'
										if(bAwardData[yearnumber][monthnumber][daynumber].type === 'economy'){
											cellContent += '<span class="bookingAward__dotEconomy"></span>'
										}else if(bAwardData[yearnumber][monthnumber][daynumber].type === 'business'){
											cellContent += '<span class="i-dot bookingAward__dotBusiness"></span>'
										}else{
											cellContent += '<span class="bookingAward__dotEconomy"></span><span class="i-dot bookingAward__dotBusiness"></span>'
										}
										cellContent += '</p>'
									}
								$this.html("<div class='bookingAward__dateWrapper'><span class='bookingAward__date'>"+daynumber+"</span>"+cellContent+"</div>");

								}
							}
						});
					});
				}]
			}
		})
	
	function setReturn(){
		datePickerController.setRangeLow("bookingAward_ritorno", datePickerController.getSelectedDate("bookingAward_andata"));
		}
	function setDeparture(){
		datePickerController.setRangeHigh("bookingAward_andata", datePickerController.getSelectedDate("bookingAward_ritorno"));
		}
	}
}

function chekinReady() {
	checkingBoardingPassSelector();
	addMorePassenger();
	selectAllPassengers();
	selectOnePassenger();


	alignHeight();



	$(window).on('resize',function(){
		var timeOuter;
		clearTimeout(timeOuter);
		timeOuter = setTimeout(function(){
			alignHeight();
		}, 50);
	});
}

function checkingBoardingPassSelector() {
	$('.checkin .j-accordion-header a').click(function(event) {
		var sel = $(this).attr('href');
		console.log( sel )
	});

	$('.j-checkingBoardingPassSelectorTrigger input').each(function(){
		if ($(this).is(':checked')) {
			$(this).closest('.j-checkingBoardingPassHead').siblings('.j-checkingBoardingPassBody').velocity('slideDown');
		}
	});

	$('.j-checkingBoardingPassSelectorTrigger input').off().on('change',function(){
		var selector = $(this).closest('.j-checkingBoardingPassHead').siblings('.j-checkingBoardingPassBody')
		$('.j-checkingBoardingPassBody').velocity('slideUp');
		if ($(this).is(':checked')){
			selector.velocity('slideDown');
		} else {
			selector.velocity('slideUp');
		}
	});

	$('.j-openlist').off().on('change',function(){
		var _this = $(this),
			id = _this.val(),
			toOpen = $('#'+id);

		if (_this.is(':checked')){
			toOpen.velocity('slideDown');
		} else {
			toOpen.velocity('slideUp');
		}
	});

	$('.j-openSections').unfastClick().fastClick(function(e){
		e.preventDefault();
		var _this = $(this),
			id = _this.attr('href'),
			toOpen = $(id);

		toOpen.velocity('slideDown', {
			complete : function(){
				$('html').velocity("scroll", {
					duration: 300,
					delay: 100,
					offset: toOpen.offset().top - $('.menu.mainMenu').height() - $('.booking__informationBoxCover').height() - 20
				});
				_this.velocity('transition.fadeOut');
			}
		});
	});

	$('.j-closeSections').unfastClick().fastClick(function(e){
		e.preventDefault();
		var _this = $(this),
			id = _this.attr('href'),
			toOpen = $(id);

		toOpen.velocity('slideUp', {
			complete : function(){
				$('html').velocity("scroll", {
					duration: 300,
					delay: 100,
					offset: $('.checkin__seatPro').offset().top - $('.menu.mainMenu').height() - $('.booking__informationBoxCover').height() - 20
				});

				$('.j-openSections[href="'+id+'"]').velocity('transition.fadeIn');
			}
		});
	});

	$('.checkin__fakeAccordionTitle').unfastClick().fastClick(function(e){
		e.preventDefault();
		$(this).toggleClass('isActive');
		$('.checkin__fakeAccordionContainer').toggleClass('isActive');
	});

}

function addMorePassenger (){
	$('.j-addMorePassenger').unfastClick().fastClick(function(e){
		e.preventDefault();

		if ( !$(this).hasClass('caseRemove') ) {
			$(this).addClass('caseRemove');
			$('.j-addMorePassengerForm').velocity('slideDown', {
				complete : function(){
					$('html').velocity("scroll", {
						duration: 300,
						delay: 100,
						offset: $('.j-addMorePassenger').offset().top - 60
					});
				}
			});
		} else {
			$(this).removeClass('caseRemove');
			$('.j-addMorePassengerForm').velocity('slideUp');
		}
	});
}

function selectAllPassengers(){
	$('.j-selectAllPasengers input').on('change',function(){

		if ($(this).is(':checked')){
			$('.j-isPasenger').each(function(index, el) {
				$(this).find('input').prop({'checked': true})
			});
		} else {
			$('.j-isPasenger').each(function(index, el) {
				$(this).find('input').prop({'checked': false})
			});
		}
		$('.j-selectedPassengers').text($('.j-isPasenger input[type="checkbox"]:checked').length);
	});

}

function selectOnePassenger(){
	$('.j-isPasenger input[type="checkbox"]').on('change', function(){
		if (!$(this).is(':checked')){
			$('.j-selectAllPasengers input').prop({'checked': false});
		}
		var checked = $('.j-isPasenger input[type="checkbox"]:checked');
		var all = $('.j-isPasenger input[type="checkbox"]');
		if (checked.length == all.length) {
			$('.j-selectAllPasengers input').prop({'checked': true});
		}
		$('.j-selectedPassengers').text(checked.length);
	});
}



function alignHeight() {
	var leftSelector = $('.checkin__main');
	var rightSelector = $('.checkin__faqAside');

	if (leftSelector.length > 0 && rightSelector.length > 0) {
		rightSelector.attr('style','');
		leftSelector.attr('style','');
		var left = leftSelector.height();
		var right = rightSelector.height();

		if (window.breakpointM > window.windowW) {
			rightSelector.attr('style','');
			leftSelector.attr('style','');
		} else {

			if (  right > left ) {
				leftSelector.height(right);
				rightSelector.height(right);
			} else {
				rightSelector.height(left);
				leftSelector.height(left);
			}
		}
	}
}
//

function carnetReady() {

	buttonTriggers();

	$(window).on('resize',function(){
		var timeOuter;
		clearTimeout(timeOuter);
		timeOuter = setTimeout(function(){
			// alignHeight();
		}, 50);
	});
}


function buttonTriggers() {
	$(document).on("click", '.j-carnet__loginContTrigger', function(e){
		e.preventDefault();
		var _this = $(this)
		if ( window.breakpointL >= window.windowW ){

			var cont = $('.j-carnet__loginCont');
			if (cont.is(':visible')){
				cont.velocity('slideUp' , {
					begin: function(){
						_this.removeClass('isActive');
					}
				});
			} else {
				cont.velocity('slideDown' , {
					begin: function(){
						_this.addClass('isActive');
					}
				});
			}
		} else {
			return false
		}
	});
}



