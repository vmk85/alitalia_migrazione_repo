$(function() {
    $(".foodOnBoard--teaser .j-demoUpselling-food").on("click", function(a) {
        a.preventDefault(), $(".foodOnBoard--teaser").hide(), $(".foodOnBoard__choose").show(), 
        $(window).trigger("resize.accordion"), $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn"), 
        setTimeout(function() {
            $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
        }, 2e3);
    }), $(".foodOnBoard__choose .j-demoUpselling-food").on("click", function(a) {
        a.preventDefault(), $(".foodOnBoard__choose").hide(), $(".foodOnBoard__feedback").show(), 
        $(window).trigger("resize.accordion"), $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn"), 
        setTimeout(function() {
            $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
        }, 2e3);
    }), $(".foodOnBoard__feedback .j-demoUpselling-food").on("click", function(a) {
        a.preventDefault(), $(".foodOnBoard__feedback").hide(), $(".foodOnBoard--teaser").show(), 
        $(window).trigger("resize.accordion"), $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn"), 
        setTimeout(function() {
            $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
        }, 2e3);
    }), $(".fastTrack--teaser .j-demoUpselling-fast").on("click", function(a) {
        a.preventDefault(), $(".fastTrack--teaser").hide(), $(".fastTrack__choose").show(), 
        $(window).trigger("resize.accordion"), $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn"), 
        setTimeout(function() {
            $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
        }, 2e3);
    }), $(".fastTrack__choose .j-demoUpselling-fast").on("click", function(a) {
        a.preventDefault(), $(".fastTrack__choose").hide(), $(".fastTrack__feedback").show(), 
        $(window).trigger("resize.accordion"), $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn"), 
        setTimeout(function() {
            $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
        }, 2e3);
    }), $(".fastTrack__feedback .j-demoUpselling-fast").on("click", function(a) {
        a.preventDefault(), $(".fastTrack__feedback").hide(), $(".fastTrack--teaser").show(), 
        $(window).trigger("resize.accordion"), $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn"), 
        $(".bookInfoBoxBasketBtn.firstButton").removeClass("isDisabled"), setTimeout(function() {
            $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
        }, 2e3);
    }), $(".insurance__teaser .j-demoUpselling-insuarance").on("click", function(a) {
        a.preventDefault(), $(".insurance__teaser").hide(), $(".insurance__feedback").show(), 
        $(window).trigger("resize.accordion"), $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn"), 
        $(".bookInfoBoxBasketBtn.firstButton").removeClass("isDisabled"), setTimeout(function() {
            $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
        }, 2e3);
    }), $(".extraBagage__teaser .j-demoUpselling-baggage").on("click", function(a) {
        a.preventDefault(), $(".extraBagage__teaser").hide(), $(".extraBagage__choose").show(), 
        $(window).trigger("resize.accordion"), $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn"), 
        $(".bookInfoBoxBasketBtn.firstButton").removeClass("isDisabled"), setTimeout(function() {
            $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
        }, 2e3);
    }), $(".extraBagage__choose .j-demoUpselling-baggage").on("click", function(a) {
        a.preventDefault(), $(".extraBagage__choose").hide(), $(".extraBagage_feedback").show(), 
        $(window).trigger("resize.accordion"), $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn"), 
        $(".bookInfoBoxBasketBtn.firstButton").removeClass("isDisabled"), setTimeout(function() {
            $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
        }, 2e3);
    }), $("#departureInput").autocomplete({
        serviceUrl: "/data/airport.json",
        preserveInput: !1,
        appendTo: $("#departureInput").parent(),
        minChars: 3,
        onSearchComplete: function(a, b) {
            for (var c = 0; c < b.length; c++) {
                var d = b[c];
                d.value = d.city + " " + d.type;
            }
        },
        formatResult: function(a, b) {
            return '<div class="sugg_dest_data"><span class="sugg_city_name toPass ' + (a.best ? "best" : "") + '">' + a.city + '</span> - <span class="sugg_city_nation ">' + a.country + '</span><br><span class="sugg_airport">' + a.airport + '</span><span class="sugg_airportCode">(' + a.code + ")</span></span></div>";
        },
        onInvalidateSelection: function() {},
        onSelect: function(a) {
            $(this).val(a.value), $(this).siblings(".apt").text(a.city), $(this).siblings(".city").text(a.type), 
            $(this).siblings("#" + $(this).attr("id") + "_country").val(a.code), $(this).siblings(".j-countryCode").val(a.code), 
            $("#arrivalInput").focus();
        }
    }), $("#arrivalInput").autocomplete({
        serviceUrl: "/data/airport.json",
        preserveInput: !1,
        appendTo: $("#arrivalInput").parent(),
        minChars: 3,
        onSearchComplete: function(a, b) {
            for (var c = 0; c < b.length; c++) {
                var d = b[c];
                d.value = d.city + " " + d.type;
            }
        },
        formatResult: function(a, b) {
            return '<div class="sugg_dest_data"><span class="sugg_city_name toPass ' + (a.best ? "best" : "") + '">' + a.city + '</span> - <span class="sugg_city_nation ">' + a.country + '</span><br><span class="sugg_airport">' + a.airport + '</span><span class="sugg_airportCode">(' + a.code + ")</span></span></div>";
        },
        onInvalidateSelection: function() {},
        onSelect: function(a) {
            $(this).val(a.value), $(this).siblings(".apt").text(a.city), $(this).siblings(".city").text(a.type), 
            $(this).siblings("#" + $(this).attr("id") + "_country").val(a.code), $(this).siblings(".j-countryCode").val(a.code), 
            $("input.j-typeOfFlight").eq(0).focus(), datePickerController.show("andata");
        }
    }), $("#infovoli_departures").autocomplete({
        serviceUrl: "/data/airport.json",
        preserveInput: !1,
        minChars: 3,
        onSearchComplete: function(a, b) {
            for (var c = 0; c < b.length; c++) {
                var d = b[c];
                d.value = d.city + " " + d.type;
            }
        },
        formatResult: function(a, b) {
            return '<div class="sugg_dest_data"><span class="sugg_city_name toPass ' + (a.best ? "best" : "") + '">' + a.city + '</span> - <span class="sugg_city_nation ">' + a.country + '</span><br><span class="sugg_airport">' + a.airport + '</span><span class="sugg_airportCode">(' + a.code + ")</span></span></div>";
        },
        onInvalidateSelection: function() {},
        onSelect: function(a) {
            $(this).val(a.value), $(this).siblings(".apt").text(a.city), $(this).siblings(".city").text(a.type), 
            $(this).siblings("#" + $(this).attr("id") + "_country").val(a.code), $(this).siblings(".j-countryCode").val(a.code), 
            $("#infovoli_arrival").focus();
        }
    }), $("#infovoli_arrival").autocomplete({
        serviceUrl: "/data/airport.json",
        preserveInput: !1,
        minChars: 3,
        onSearchComplete: function(a, b) {
            for (var c = 0; c < b.length; c++) {
                var d = b[c];
                d.value = d.city + " " + d.type;
            }
        },
        formatResult: function(a, b) {
            return '<div class="sugg_dest_data"><span class="sugg_city_name toPass ' + (a.best ? "best" : "") + '">' + a.city + '</span> - <span class="sugg_city_nation ">' + a.country + '</span><br><span class="sugg_airport">' + a.airport + '</span><span class="sugg_airportCode">(' + a.code + ")</span></span></div>";
        },
        onInvalidateSelection: function() {},
        onSelect: function(a) {
            $(this).val(a.value), $(this).siblings(".apt").text(a.city), $(this).siblings(".city").text(a.type), 
            $(this).siblings("#" + $(this).attr("id") + "_country").val(a.code), $(this).siblings(".j-countryCode").val(a.code), 
            $("#flightStatusDatePicker2").focus();
        }
    });
}), $(function() {
    if (0 !== $("section.mod.flightSel").length) {
        var a = "EUR", b = "20150701", c = "20150708", d = {
            "20150630_20150708": 2345,
            "20150701_20150709": 2345,
            "20150630_20150711": 2345,
            "20150704_20150705": 2345,
            "20150704_20150706": 2345,
            "20150704_20150707": 2345,
            "20150704_20150708": 2345,
            "20150704_20150709": 2345,
            "20150704_20150710": 2345,
            "20150704_20150711": 2345
        }, e = {
            2015: {
                7: {
                    1: 2345,
                    2: 2345,
                    3: {
                        amount: 1234,
                        highlight: !0
                    },
                    4: 2345,
                    5: 2345,
                    6: 2345,
                    7: {
                        amount: 1234,
                        highlight: !0
                    },
                    8: 2345,
                    9: 2345,
                    10: 2345,
                    11: 2345,
                    12: 2345,
                    14: 2345,
                    15: 2345,
                    16: {
                        amount: 1234,
                        highlight: !0
                    },
                    17: {
                        amount: 1234,
                        highlight: !0
                    },
                    18: 2345,
                    19: 2345,
                    20: {
                        amount: 1234,
                        highlight: !0
                    },
                    21: 2345,
                    22: 2345,
                    23: 2345,
                    24: 2345,
                    25: 2345,
                    26: 2345,
                    27: 2345,
                    28: 2345,
                    29: {
                        amount: 1234,
                        highlight: !0
                    },
                    30: 2345,
                    31: 2345
                },
                8: {
                    1: 2345,
                    2: 2345,
                    3: {
                        amount: 1234,
                        highlight: !0
                    },
                    4: 2345,
                    5: 2345,
                    6: 2345,
                    7: 2345,
                    8: 2345,
                    9: 2345,
                    10: 2345,
                    11: 2345,
                    12: {
                        amount: 1234,
                        highlight: !0
                    },
                    14: 2345,
                    15: 2345,
                    16: 2345,
                    17: 2345,
                    18: 2345,
                    19: 2345,
                    20: 2345,
                    21: 2345,
                    22: 2345,
                    23: 2345,
                    24: 2345,
                    25: {
                        amount: 1234,
                        highlight: !0
                    },
                    26: 2345,
                    27: 2345,
                    28: 2345,
                    29: 2345,
                    30: 2345,
                    31: 2345
                }
            }
        };
        !function() {
            $("input.j-typeOfFlight").on("change", function() {
                "a" == $(this).val() ? $(".mod.flightSel").addClass("fs-oneWay").removeClass("fs-roundTrip") : $(".mod.flightSel").addClass("fs-roundTrip").removeClass("fs-oneWay");
            });
        }(), function() {
            var a = $(".dateMatrix"), b = function(b, c, d) {
                a.find(".c" + b + ".r" + c).addClass(d);
            };
            a.find("td").hover(function() {
                var c, d, e = $(this).attr("class").split(/\s+/);
                for (var f in e) e[f].match(/^c\d$/) ? c = e[f].substr(1) : e[f].match(/^r\d$/) && (d = e[f].substr(1));
                a.find(".activeCell,.activeCell--hint,.activeCell--header").removeClass("activeCell activeCell--hint activeCell--header"), 
                b(c, d, "activeCell"), b(c, 0, "activeCell--header"), b(0, d, "activeCell--header");
                for (var f = 1; c > f; f++) b(f, d, "activeCell--hint");
                for (var f = 1; d > f; f++) b(c, f, "activeCell--hint");
            }, function() {
                a.find(".activeCell,.activeCell--hint,.activeCell--header").removeClass("activeCell activeCell--hint activeCell--header");
            });
        }(), function() {
            var e = $(".dateMatrix"), f = function(a, b) {
                return e.find(".c" + a + ".r" + b);
            }, g = function(a) {
                var b = a.getFullYear().toString(), c = (a.getMonth() + 1).toString(), a = a.getDate().toString();
                return b + (10 > c ? "0" + c : c) + (10 > a ? "0" + a : a);
            }, h = function(a) {
                return new Date(a.substr(0, 4), a.substr(4, 2) - 1, a.substr(6, 2));
            }, i = function(a, b) {
                var c = new Date(a);
                return c.setDate(a.getDate() + b), c;
            }, j = function(a) {
                var b = pageSettings.dayAbbrs;
                $(".j-changeDay").length > 0 && (b = [ "Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab" ]);
                var c = pageSettings.monthAbbrs, d = a.getDate();
                return '<span class="dateMatrix__day">' + b[a.getDay()] + '</span><span class="dateMatrix__dayNum">' + (10 > d ? "0" + d : d) + '</span><span class="dateMatrix__dayMonth">' + c[a.getMonth()] + "</span>";
            }, k = function() {
                var e = h(b), k = h(c);
                findMore = '<span class="i-lens"></span><span class="txt">Entdecken</span>';
                for (var l = 1; 8 > l; l++) f(0, l).html(j(i(e, l - 4)));
                for (var m = 1; 8 > m; m++) {
                    var n = i(k, m - 4), o = g(n);
                    f(m, 0).html(j(n));
                    for (var l = 1; 8 > l; l++) {
                        var p = i(e, l - 4), q = g(p);
                        d[q + "_" + o] ? f(m, l).html('<a class="dateMatrix__offer" href="javascript:;"><span class="dateMatrix__currency">' + a + '</span><span class="dateMatrix__amount">' + d[q + "_" + o] + "</span></a>") : f(m, l).html('<a class="dateMatrix__findMore" href="javascript:;">' + findMore + "</a>");
                    }
                }
                dateMatrixOnResize();
            };
            e.find("[data-shift]").on("click", function() {
                var a = $(this);
                return a.is(".dateMatrix__navDeparture *") ? b = g(i(h(b), $(this).data("shift"))) : c = g(i(h(c), $(this).data("shift"))), 
                k(), !1;
            }), k(), console.log("--->sdfsd"), $(".dateMatrix__offer, .dateMatrix__findMore").on("click", function() {
                console.log($(this), "sdfsd"), $(".dateMatrix__offer, .dateMatrix__findMore").parent().removeClass("isActive"), 
                $(this).parent().addClass("isActive");
            }), $(".j-loadDateMatrix").on("click", function() {
                $(".j-loadDateMatrix").removeClass("isActive"), $(this).addClass("isActive"), $(".j-loader").addClass("isActive"), 
                e.removeClass("isRendered"), setTimeout(function() {
                    $(".j-loader").removeClass("isActive"), e.addClass("isRendered");
                }, 3e3);
            });
        }(), function() {
            datePickerController.destroyDatePicker("solo_andata"), datePickerController.createDatePicker({
                formElements: {
                    soloAndata: "%d/%m/%Y"
                },
                staticPos: !0,
                hideInput: !0,
                noTodayButton: !0,
                fillGrid: !0,
                rangeLow: "20150701",
                rangeHigh: maxRange,
                noFadeEffect: !0,
                constrainSelection: !1,
                wrapped: !0,
                callbackFunctions: {
                    dateset: [ openAndSetReturn ],
                    redraw: [ function(b) {
                        setTimeout(function() {
                            $("#fd-soloAndata tbody td").each(function() {
                                var b, c, d, f, g, h = $(this).removeClass("flightSelOneWay__highlight"), i = h.attr("class").split(/\s+/);
                                if (!h.is(".out-of-range")) {
                                    for (var j in i) if ("yyyymmdd-" === i[j].substr(0, 9)) {
                                        g = parseInt(i[j].substr(-8, 4)), f = parseInt(i[j].substr(-4, 2)), c = i[j].substr(-2), 
                                        d = parseInt(c);
                                        break;
                                    }
                                    e[g] && e[g][f] && e[g][f][d] ? (b = "<span class='flightSelOneWay__currency'>" + a + "</span><span class='flightSelOneWay__amount'>" + (e[g][f][d].amount ? e[g][f][d].amount : e[g][f][d]) + "</span>", 
                                    e[g][f][d].highlight && h.addClass("flightSelOneWay__highlight")) : b = "<span class='flightSelOneWay__scopri'>Scopri</span>", 
                                    h.html("<span class='flightSelOneWay__giorno'>" + c + "</span>" + b);
                                }
                            });
                        });
                    } ]
                }
            }), datePickerController.setCursorDate("soloAndata", "20150701"), $("#soloAndata").on("focus mousedown", function() {
                datePickerController.show($(this).attr("id"));
            }).on("blur", function() {
                datePickerController.hide($(this).attr("id"));
            });
        }();
    }
}), $(function() {
    $(".j-specialOffers__button").on("click", function() {
        $("#departureInput_country").val() != $("#arrivalInput_country").val() ? "a" == $("input.j-typeOfFlight").filter(":checked").val() ? location.href = "/patterns/04-pages-04-flightSel-intl1w/04-pages-04-flightSel-intl1w.html" : location.href = "/patterns/04-pages-04-flightSel-intl/04-pages-04-flightSel-intl.html" : "a" == $("input.j-typeOfFlight").filter(":checked").val() ? location.href = "/patterns/04-pages-04-flightSel-oneWay/04-pages-04-flightSel-oneWay.html" : location.href = "/patterns/04-pages-04-flightSel/04-pages-04-flightSel.html";
    });
}), $(window).on('load', function() {
    $(window).trigger("resize.accordion");
});