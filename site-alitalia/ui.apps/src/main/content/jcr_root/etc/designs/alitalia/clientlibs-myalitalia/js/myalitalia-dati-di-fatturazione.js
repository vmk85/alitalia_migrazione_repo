var myAlitaliaDatiDiFatturazione = {};

// Document ready
$(document).ready(function () {
	myAlitaliaDatiDiFatturazione.initRadio();
});

myAlitaliaDatiDiFatturazione.initRadio = function () {
	// Inputs radio
	var personaFisica = $('#persona-fisica');
	var azienda = $('#azienda');

	// Form collegati agli inputs radio
	var personaFisicaForm = $('#personaFisicaForm');
	var aziendaForm = $('#aziendaForm');

	// Input radio personaFisica checked di base
	personaFisica.closest('.radio-wrap').addClass('checked');

	// Attiva form azienda e nascondi form personaFisica
	azienda.on('click', function () {
		personaFisicaForm.addClass('hide');
		aziendaForm.removeClass('hide');
	});

	// Attiva form personaFisica e nascondi form azienda
	personaFisica.on('click', function () {
		aziendaForm.addClass('hide');
		personaFisicaForm.removeClass('hide');
	});
};