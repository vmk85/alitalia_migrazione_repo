
/* global */

/**
 * Keeps info about current status of login process.
 */
var alitaliaLoginContextData = {
	profileReady: false,
	registeredProfileCallbacks: [],
	registeredProfileAsyncLoginCallbacks: []
};
var millemigliaCode;
var norepeat = false;
var millemigliaSurname;
var millemigliaName;


/* profile client context management functions */

/**
 * Global function that can be used to register a callback that is
 * invoked when the profile store is initialized and available.
 */
function registerLoginContextProfileCallback(callback) {
	if (alitaliaLoginContextData.profileReady) {
		callback();
	} else {
		alitaliaLoginContextData.registeredProfileCallbacks.push(callback);
		alitaliaLoginContextData.registeredProfileAsyncLoginCallbacks.push(callback);
	}
}

function executeLoginContextProfileCallbacksHistory() {
	var _rpch = alitaliaLoginContextData.registeredProfileAsyncLoginCallbacks;
	for (var i = 0; i < _rpch.length; i++) {
		try {
			var callback = _rpch[i];
			callback();
		} catch (e) {
			// unable to execute callback
		}
	}
	alitaliaLoginContextData.registeredProfileAsyncLoginCallbacks = [];
}

/**
 * Clears the locally stored client context profile data, in order to trigger a reload
 * in the next page refresh.
 */
function clearClientContextProfile() {
	var profileStore = CQ_Analytics.ClientContextMgr.getRegisteredStore('profile');
	if (profileStore) {
		profileStore.clear();
	} else {
		console.warn("Cannot clear profile data, client context profile data store not found.");
	}
}

/**
 * Read the data from the local client context, and apply it in the DOM
 * (e.g. user data, miles, etc.) through class-based jquery selectors.
 */
function applyClientContextProfile() {
    var isAuthenticated = false;
    var isNotAdminUser = false;
    var profileStore = CQ_Analytics.ClientContextMgr.getRegisteredStore('profile');
    if (profileStore) {
        var isAlitaliaConsumerUser = profileStore.getInitProperty('isAlitaliaConsumerUser', false);
        isNotAdminUser = profileStore.getProperty('authorizableId').indexOf("MM") != -1;
        isAuthenticated = (isAlitaliaConsumerUser == true || isAlitaliaConsumerUser == 'true');
    } else {
        console.error("Cannot apply profiledata , profile data store not found or not yet ready.");
    }
    if (isAuthenticated && isNotAdminUser) {
        // load properties that are read-only from the client from init data (refreshed on each page load)
        var customerNumber = profileStore.getInitProperty('customerNumber', false);
        var customerName = profileStore.getInitProperty('customerName', false);
        var customerSurname = profileStore.getInitProperty('customerSurname', false);
        var customerNickName = profileStore.getInitProperty('customerNickName', false);
        var customerBirthDate = profileStore.getInitProperty('customerBirthDate', false);
        var customerGender = profileStore.getInitProperty('customerGender', false);
        var milleMigliaTierCode = profileStore.getInitProperty('milleMigliaTierCode', false);
        var milleMigliaTotalMiles = profileStore.getInitProperty('milleMigliaTotalMiles', false);
        var milleMigliaTotalQualifiedMiles = profileStore.getInitProperty('milleMigliaTotalQualifiedMiles', false);
        var milleMigliaRemainingMiles = profileStore.getInitProperty('milleMigliaRemainingMiles', false);
        var milleMigliaTotalSpentMiles = profileStore.getInitProperty('milleMigliaTotalSpentMiles', false);
        var milleMigliaQualifiedFlight = profileStore.getInitProperty('milleMigliaQualifiedFlight', false);
        var millemigliaExpirationDate = profileStore.getInitProperty('millemigliaExpirationDate', false);
        var customerMailingType = profileStore.getInitProperty('customerMailingType', false);
        var mm = profileStore.getInitProperty('authorizableId',false);
        millemigliaCode = mm;
        millemigliaName = customerName;
        millemigliaSurname = customerSurname;
        if(isNaN(parseInt(milleMigliaRemainingMiles))){
            milleMigliaRemainingMiles = 0;
        }

        if(isNaN(parseInt(milleMigliaTotalMiles))){
            milleMigliaTotalMiles = 0;
        }

        var spentMiles = milleMigliaTotalMiles - milleMigliaRemainingMiles;
        $('#header').removeClass();
        // load properties that can be written on the client from storage data (kept between page loads)
        var userImageThumbnailStatus = profileStore.getProperty('userImageThumbnailStatus', false);
        var userImageThumbnailURL = profileStore.getProperty('userImageThumbnailURL', false);
        // prepare data
        var milleMigliaTierClass = '';

        var milleMigliaTierName = 'Millemiglia';
        if (milleMigliaTierCode == 'Plus') {
            $(".loggeduser_info").css("background","url('/etc/designs/alitalia/clientlibs/images/ico-user-freccia-alata-plus.svg') no-repeat left center");
            $(".loggeduser_info").css("background-size","22px 22px")
            milleMigliaTierClass = 'i-mm_tier4';
            milleMigliaTierName = 'Freccia Alata Plus';
            $('#header').addClass('freccia-alata-plus');
        } else if (milleMigliaTierCode == 'FrecciaAlata') {
            $(".loggeduser_info").css("background","url('/etc/designs/alitalia/clientlibs/images/ico-user-freccia-alata.svg') no-repeat left center");
            $(".loggeduser_info").css("background-size","22px 22px")
            milleMigliaTierClass = 'i-mm_tier3';
            milleMigliaTierName = 'Freccia Alata';
            $('#header').addClass('freccia-alata');
        } else if (milleMigliaTierCode == 'Ulisse') {
            $(".loggeduser_info").css("background","url('/etc/designs/alitalia/clientlibs/images/ico-user-ulisse.svg') no-repeat left center");
            $(".loggeduser_info").css("background-size","22px 22px")
            milleMigliaTierClass = 'i-mm_tier2';
            milleMigliaTierName = 'Ulisse';
            $('#header').addClass('ulisse');
        } else if (milleMigliaTierCode == 'Basic') {
            $(".loggeduser_info").css("background","url('/etc/designs/alitalia/clientlibs/images/ico-user-millemiglia.svg') no-repeat left center");
            $(".loggeduser_info").css("background-size","22px 22px");
            $('#header').addClass('millemiglia');
        } else{
            milleMigliaTierClass = 'i-mm_tier1';
            $('#header').addClass('millemiglia');
        }
        if($('html').attr('dir') == "rtl"){
            $(".loggeduser_info").css('background-position','right');
        }else{
            $(".loggeduser_info").css('background-position','left');
        }
        function resizeicon(x) {
            if (x.matches) { // If media query matches
                $(".loggeduser_info").css("background-size","35px 35px")
            } else {
                $(".loggeduser_info").css("background-size","22px 22px")
            }
        }

        var x = window.matchMedia("(max-width: 1023px)")
        resizeicon(x) // Call listener function at run time
        x.addListener(resizeicon) // Attach listener function on state changes
        var customerFullName = customerName + ' ' + customerSurname;
        // apply data
        if (userImageThumbnailStatus == "success") {
            $('.binding-mm-thumbnail-image').attr('src', userImageThumbnailURL);
            $('.binding-mm-thumbnail-generic').hide();
            $('.binding-mm-thumbnail-image').show();
        } else {
            $('.binding-mm-thumbnail-image').attr('src', '');
            $('.binding-mm-thumbnail-generic').show();
            $('.binding-mm-thumbnail-image').hide();
        }

        //binding usato
        $('.binding-name').text(customerName);
        $('.binding-mm-full-name').text(customerFullName);
        $('.binding-mm-tier').attr('class','binding-mm-tier user__programma').addClass(milleMigliaTierClass);
        $('.binding-mm-tier').text(milleMigliaTierName);
        $('.binding-mm-total-qualified-miles').text(addThousandSeparator(milleMigliaTotalQualifiedMiles,'.'));
        $('.binding-mm-remaining-miles').text(addThousandSeparator(milleMigliaRemainingMiles,'.'));
        $('a.loggeduser_info div.label-name').html(capitalize_Words($('a.loggeduser_info div.label-name').html()));


        $('input#name').val(capitalize_Words(customerName));
        $('input#firstName').val(capitalize_Words(customerName));
        $('input#surname').val(capitalize_Words(customerSurname));
        $('input#lastName').val(capitalize_Words(customerSurname));
        $('input#name_surname').val(capitalize_Words(customerSurname));
        $('input#name_code').val(millemigliaCode.replace('MM:',""));



        $('a.loggeduser_info div.label-name').html(capitalize_Words($('a.loggeduser_info div.label-name').html()));

        //visualizzo radio option per millemiglia
        //ProfileMillemiglia(true);

        $('.menu-heading-mm').show();
        $('.menu-heading-ma').hide();

        $('.menu-ma-item').hide();

        $('.login-included-if-authenticated-mm').show();
        $('.login-included-if-anonymous-mm').hide();

        $('#user-login-checkin-codice').hide();


        if(!jQuery("#estratto_conto_list_activities").data("mmtransitionprogram")) {
            $('.binding-mm-remaining-miles').text(addThousandSeparator(milleMigliaRemainingMiles,'.'));
            $('.binding-mm-total-miles').text(addThousandSeparator(milleMigliaTotalMiles,'.'));
            $('.binding-mm-total-qualified-miles').text(addThousandSeparator(milleMigliaTotalQualifiedMiles,'.'));
            $('.binding-mm-total-spent-miles').text(addThousandSeparator(spentMiles,'.'));
            $('.binding-mm-qualified-flights').text(milleMigliaQualifiedFlight);
        } else {
            //GG MilleMiglia 2013-2017: se attivo mmtransitionprogram, setto milleMigliaRemainingMiles
            // solo per il riepilogo miglia su menu
            jQuery('.user__saldo .binding-mm-remaining-miles').text(addThousandSeparator(milleMigliaRemainingMiles,'.'));
            jQuery('.user__saldo .binding-mm-total-qualified-miles').text(addThousandSeparator(milleMigliaTotalQualifiedMiles,'.'));
        }

        //binding non usato
        $('.binding-mm-number').text(customerNumber);
        $('.binding-mm-surname').text(customerSurname);
        $('.binding-mm-nickname').text(customerNickName);
        // $('.binding-mm-name').text(customerName);

//		$('.binding-mm-total-miles').text(addThousandSeparator(milleMigliaTotalMiles,'.'));
//		$('.binding-mm-total-spent-miles').text(addThousandSeparator(spentMiles,'.'));
//		$('.binding-mm-qualified-flights').text(milleMigliaQualifiedFlight);

//		$('.login-included-if-anonymous').hide();
//		$('.login-included-if-authenticated').show();

        //TODO: update wcag aria label
        // if(countryCode=="us"){
        // 	$('.userMenu__login.j-userMenuLink').focus();
        // 	$('#headLoginLink').attr('aria-describedby', 'wcag-loggedin');
        // 	$('#headLoginLink').attr('aria-label', $('#wcag-loggedin').text())
        // }

        loadMenuPersonalArea();

        if($('#showLoggedMMTabCheckin').length){
            if(millemigliaCode != undefined && !window.location.href.indexOf('check-in') != -1 && !norepeat){
                activateCheckin();
                norepeat = true;
            }
        }

        loginOptions.nameLogo();

        if($("#MM-Login").hasClass("hide")){
            $("#MM-Login").removeClass("hide");
        }
        if(!$("#MA-login").hasClass("hide")){
            $("#MA-login").addClass("hide");
        }
        // menu desktop login MM
        if(!$(".login-included-if-anonymous").hasClass("hide")){
            $(".login-included-if-anonymous").addClass("hide");
        }
        if($(".login-included-if-authenticated").hasClass("hide")){
            $(".login-included-if-authenticated").removeClass("hide");
        }

        $(".headerButtonLogout").attr("href","javascript:performMilleMigliaLogout();");


    } else if($("#header").hasClass("myalitalia")){
        // if(!$("#MM-Login").hasClass("hide")){
        //     $("#MM-Login").addClass("hide");
        // }
        // if(!$("#MA-login").hasClass("hide")){
        //     $("#MA-login").addClass("hide");
        // }

        if(!$(".login-included-if-anonymous").hasClass("hide")){
            $(".login-included-if-anonymous").addClass("hide");
            // $(".login-included-if-anonymous").show();

        }
        if($(".login-included-if-authenticated").hasClass("hide")){
            $(".login-included-if-authenticated").removeClass("hide");
        }
    }else  {

        $('.binding-mm-thumbnail-image').attr('src', '');
        $('.binding-mm-thumbnail-generic').hide();
        $('.binding-mm-thumbnail-image').hide();
        $('.binding-mm-name').text('');
        $('.binding-mm-tier').attr('class','binding-mm-tier user__programma').text('');
        $('.binding-mm-full-name').text('');
        $('.binding-mm-total-qualified-miles').text('0');
        $('.binding-mm-remaining-miles').text('0');

        $('.binding-mm-nickname').text('');
        $('.binding-mm-surname').text('');
        $('.binding-mm-nickname').text("");

//        $('.binding-mm-total-miles').text('0');
//        $('.binding-mm-total-spent-miles').text('0');
//        $('.binding-mm-qualified-flights').text('0');

        $('.login-included-if-authenticated-mm').hide();
        $('.login-included-if-anonymous-mm').show();

        if(!$("#MM-Login").hasClass("hide")){
            $("#MM-Login").addClass("hide");
        }
        if($("#MA-login").hasClass("hide")){
            $("#MA-login").removeClass("hide");
        }
        if(!$('.login-included-if-authenticated').hasClass("hide")){
            $('.login-included-if-authenticated').addClass("hide");
        }
        if($('.login-included-if-anonymous').hasClass("hide")){
            $('.login-included-if-anonymous').removeClass("hide");
        }
    }

    // patch -------------------------------
    //$('.reveal-overlay').css('display','none');
    $('.reveal-overlay').not('.mm-recupera-credenziali .empty').css('display','none');
    // patch -------------------------------

    $('html, body').removeClass('is-reveal-open');

}
function setHeaderBothLogged(dataMM,dataMA){

        setHeaderMM(dataMM);

        $('.menu-ma-item').show();

//        $('.login-included-if-authenticated').show();
//		$('.login-included-if-anonymous').hide();

		loadMenuPersonalArea();

    }
function setHeaderNotLogged(){
        $('.binding-mm-thumbnail-image').attr('src', '');
		$('.binding-mm-thumbnail-generic').hide();
		$('.binding-mm-thumbnail-image').hide();
		$('.binding-mm-name').text('');
		$('.binding-mm-tier').attr('class','binding-mm-tier user__programma').text('');
		$('.binding-mm-full-name').text('');
		$('.binding-mm-total-qualified-miles').text('0');
		$('.binding-mm-remaining-miles').text('0');

//        $('.binding-mm-nickname').text('');
//        $('.binding-mm-surname').text('');
//        $('.binding-mm-total-miles').text('0');
//        $('.binding-mm-total-spent-miles').text('0');
//        $('.binding-mm-qualified-flights').text('0');

//		$('.login-included-if-authenticated').hide();
//		$('.login-included-if-anonymous').show();

        $('#user-login-checkin-codice').show();


        //nascondo opszione millemiglia nel check-in
		ProfileMillemiglia(false);
    }


/**
 * Returns true if a user is currently logged in, based on local init data of client context.
 */
function isLoginContextAuthenticated() {
	var isAuthenticated = false;
	var profileStore = CQ_Analytics.ClientContextMgr.getRegisteredStore('profile');
	if (profileStore) {
		var isAlitaliaConsumerUser = profileStore.getInitProperty('isAlitaliaConsumerUser', false);
		isAuthenticated = (isAlitaliaConsumerUser == true || isAlitaliaConsumerUser == 'true');
	} else {
		console.error("Cannot check authentication status, profile data store not found or not yet ready.");
	}
	return isAuthenticated;
}

/**
 * Returns the value of a profile property, based on local data of client context.
 */
function getLoginContextProfileProperty(propertyName) {
	var propertyValue = null;
	var profileStore = CQ_Analytics.ClientContextMgr.getRegisteredStore('profile');
	if (profileStore) {
		propertyValue = profileStore.getProperty(propertyName, false);
	} else {
		console.error("Cannot get profile data property, profile store not found or not yet ready.");
	}
	return propertyValue;
}

/**
 * Set the value of a profile property in the local data of client context.
 */
function setLoginContextProfileProperty(propertyName, propertyValue) {
	var profileStore = CQ_Analytics.ClientContextMgr.getRegisteredStore('profile');
	if (profileStore) {
		profileStore.setProperty(propertyName, propertyValue);
	} else {
		console.error("Cannot set profile data property, profile data store not found or not yet ready.");
	}
}


/* event handlers */

/**
 * Event handler on document ready to setup client context listener.
 */
function onLoginContextDocumentReady() {
	CQ_Analytics.ClientContextUtils.onStoreInitialized('profile',
			onLoginContextProfileInitialized, true);
}

/**
 * Event handler to trigger application of user data and
 * callback executions as soon as profile store is available.
 */
function onLoginContextProfileInitialized(event) {
	var profileStore = CQ_Analytics.ClientContextMgr.getRegisteredStore('profile');
	if (profileStore && profileStore.initProperty) {
		applyClientContextProfile();
		alitaliaLoginContextData.profileReady = true;
		for (var i = 0; i < alitaliaLoginContextData.registeredProfileCallbacks.length; i++) {
			try {
				var callback = alitaliaLoginContextData.registeredProfileCallbacks[i];
				callback();
			} catch (e) {
				//console.log("Error during invocation of login context profile callback function");
			}
		}
		alitaliaLoginContextData.registeredProfileCallbacks = [];
	}
}

function capitalize_Words(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

/* execution code */

// add listener for document ready event to setup social events listeners
$(document).ready(onLoginContextDocumentReady);

function activateCheckin(){

    var data = {
            frequentFlyer : CQ_Analytics.ProfileDataMgr.data.customerNumber,
            lastName : CQ_Analytics.ProfileDataMgr.data.customerSurname,
        };
        checkinMMloginEnableLoader();
        performSubmit('searchbyfrequentflyer', data, cercaByFFSucessSearchBox, cercaByFFFail);

}

function cercaByFFSucessSearchBox(data) {


    $('.input-wrap.radio-wrap.millemiglia-profile-checkin').addClass('checked');
    $('.input-wrap.radio-wrap.millemiglia-profile-checkin.hide').removeClass('hide');
    $('.booking_code').addClass('hide');
    $('label[for="booking_code"]').parent().removeClass('checked')
    $('.millemiglia_code').addClass('hide');
    $('label[for="millemiglia_code"]').parent().removeClass('checked');
    $('.millemiglia_profile.millemiglia-profile-checkin').removeClass('hide');
    if (data.isError == false && data.flights > 0) {
		$('.millemiglia_profile.millemiglia-profile-checkin').removeClass('no-flight');
		$('#MMavailableFlightsNumber').html(data.flights);
        $('#MMavailableFlightsNumber').removeClass('hide');
        var label = CQ.I18n.get('myalitalia.checkin.availableFlights',[parseInt(CQ_Analytics.ProfileDataMgr.data.customerNumber), data.flights]);
        $('.hasAvailableFlights').html(label);
        $('.hasAvailableFlights').removeClass('hide');
    }else{
		$('.noAvailableFlights').removeClass('hide');
    }
    checkinMMloginDisableLoader();
}

function cercaByFFFail() {

    checkinMMloginDisableLoader();
}


function ProfileMillemiglia(enabled){
    if(enabled){
        $(".millemiglia-profile-checkin").removeClass('hide');
        $(".millemiglia-profile-checkin").addClass('checked');
        $("#millemiglia_profile").attr('checked', true);

        $("#booking_code").prop("checked", false);
        $("#booking_code").parent().removeClass('checked');

        $('#millemiglia_code').prop("checked", false);
        $('#millemiglia_code').parent().removeClass('checked');

        $("#form-cercaPnr").addClass('hide');
        $("#form-cercaByFF").addClass('hide');
    }else{
        $(".millemiglia-profile-checkin").addClass('hide');
        $(".millemiglia-profile-checkin").removeClass('checked');
        $("#millemiglia_profile").attr('checked', false);

        $('#millemiglia_code').prop("checked", false);
        $('#millemiglia_code').parent().removeClass('checked');

        $("#booking_code").prop("checked", true);
        $("#booking_code").parent().addClass('checked');
        $("#form-cercaPnr").removeClass('hide');
        $("#form-cercaByFF").addClass('hide');
    }

}

function resizeicon(x) {
     if (x.matches) { // If media query matches
        $(".loggeduser_info").css("background-size","35px 35px")
     } else {
        $(".loggeduser_info").css("background-size","22px 22px")
     }
}

function isMilleMigliaAuthenticated(){
    var isAuthenticated = false;
	var profileStore = CQ_Analytics.ClientContextMgr.getRegisteredStore('profile');
	if (profileStore) {
		var isAlitaliaConsumerUser = profileStore.data.isAlitaliaConsumerUser;
		isAuthenticated = (isAlitaliaConsumerUser == true || isAlitaliaConsumerUser == 'true');
	} else {
		console.error("Cannot apply profiledata , profile data store not found or not yet ready.");
	}
	return isAuthenticated;
}

// function isGigyaAuthenticated(){
//     gigya.accounts.getAccountInfo({callback:function(ev){console.log(ev)}});
// }

