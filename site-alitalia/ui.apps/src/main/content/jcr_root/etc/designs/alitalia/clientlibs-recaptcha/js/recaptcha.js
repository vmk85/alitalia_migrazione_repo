function recaptchaUserCallback(response) {
	$("input[name='header_recaptchaResponse']").val(response);
	$("input[name='recaptchaResponse']").val(response);
}

//var mievoliRecaptchaID;
//var checkinRecaptchaID;
var spacialPagesRecaptchaID;
var headerRecaptchaID;
var cashAndMilesRecpatchaID;

function recaptchaOnload() {
	
	if ($("#recaptchaContainer").length > 0) {
		if (spacialPagesRecaptchaID !== undefined) {
			$("#recaptchaContainer").html('');
		} 
		spacialPagesRecaptchaID = grecaptcha.render('recaptchaContainer', {
				'sitekey' : pageSettings.dataRecaptchaSiteKey,
				'callback' : recaptchaUserCallback,
				'theme' : 'light'
		});
	}
	
	if ($("#header_recaptchaContainer").length > 0) {
		if (headerRecaptchaID !== undefined) {
			$("#header_recaptchaContainer").html('');
		} 
		headerRecaptchaID = grecaptcha.render('header_recaptchaContainer', {
				'sitekey' : pageSettings.dataRecaptchaSiteKey,
				'callback' : recaptchaUserCallback,
				'theme' : 'light'
		});
		
		if ($("#loginMenu").hasClass("isActive")) {
			$("#loginMenu").css("height",$("#loginMenu").get(0).scrollHeight);
		}
	}
	
	if ($("#cashAndMiles_login_recaptchaContainer").length > 0) {
		if (cashAndMilesRecpatchaID !== undefined) {
			$("#cashAndMiles_login_recaptchaContainer").html('');
		} 
		cashAndMilesRecpatchaID = grecaptcha.render('cashAndMiles_login_recaptchaContainer', {
				'sitekey' : pageSettings.dataRecaptchaSiteKey,
				'callback' : recaptchaUserCallback,
				'theme' : 'light'
		});
	}
	
}

function refreshReCaptchaLogin(recaptchaWidgetId){
	if (typeof(grecaptcha) !== "undefined") {
		grecaptcha.reset(recaptchaWidgetId);	
	}
}