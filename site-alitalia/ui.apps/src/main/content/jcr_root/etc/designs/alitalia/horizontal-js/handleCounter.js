/**
 * handle counter
 */
; (function () {
    'use strict';
    $.fn.handleCounter = function (options) {
        var $input,
            $btnMinus,
            $btnPlugs,
            $btnPlugsOther,
            minimum,
            maximize,
            writable,
            onChange,
            onMinimum,
            onMaximize;
        var $handleCounter = this
        $btnMinus = $handleCounter.find('.h-font-minus');
        $input = $handleCounter.find('input')
        $btnPlugs = $handleCounter.find('.h-font-plus');
        $btnPlugsOther = $handleCounter.find('.h-font-plus-other');
        var defaultOpts = {
            writable: true,
            minimum: 1,
            maximize: null,
            onChange: function () { },
            onMinimum: function () { },
            onMaximize: function () { }
        }
        var settings = $.extend({}, defaultOpts, options)
        minimum = settings.minimum
        maximize = settings.maximize
        writable = settings.writable
        onChange = settings.onChange
        onMinimum = settings.onMinimum
        onMaximize = settings.onMaximize
        if (!$.isNumeric(minimum)) {
            minimum = defaultOpts.minimum
        }
        if (!$.isNumeric(maximize)) {
            maximize = defaultOpts.maximize
        }
        var inputVal = $input.val()
        if (isNaN(parseInt(inputVal))) {
            inputVal = $input.val(0).val()
        }
        if (!writable) {
            $input.prop('disabled', true)
        }

        changeVal(inputVal)
        $input.val(inputVal)
        $btnMinus.on('click touchstart', function () {
            if (!$btnMinus.hasClass("locked")) {
                var num = parseInt($input.val())
                if (num > minimum) {
                    $input.val(num - 1)
                    changeVal(num - 1)
                }
            }
        })
        $btnPlugs.on('click touchstart', function () {
            var num = parseInt($input.val())
            var a = $('#adult').val();
            var maximize = a;
            if (maximize == null || num < maximize) {
                $input.val(num + 1)
                changeVal(num + 1)
            }
        })
        $btnPlugsOther.on('click touchstart', function () {
            var ya = $('#youngAdult').val();
            var c = $('#child').val();
            var a = $('#adult').val();
            var num = parseInt($input.val())
            var tot = parseInt(ya) + parseInt(c) + parseInt(a)
            if (maximize == null || tot < maximize) {
                $input.val(num + 1)
                changeVal(tot + 1)
            }
        })
        var keyUpTime
        $input.keyup(function () {
            clearTimeout(keyUpTime)
            keyUpTime = setTimeout(function () {
                var num = $input.val()
                if (num == '') {
                    num = minimum
                    $input.val(minimum)
                }
                var reg = new RegExp("^[\\d]*$")
                if (isNaN(parseInt(num)) || !reg.test(num)) {
                    $input.val($input.data('num'))
                    changeVal($input.data('num'))
                } else if (num < minimum) {
                    $input.val(minimum)
                    changeVal(minimum)
                } else if (maximize != null && num > maximize) {
                    $input.val(maximize)
                    changeVal(maximize)
                } else {
                    changeVal(num)
                }
            }, 300)
        })
        $input.focus(function () {
            var num = $input.val()
            if (num == 0) $input.select()
        })

        function changeVal(num) {
            $input.data('num', num)

            if (num <= minimum) {
                $btnMinus.addClass('locked');
                onMinimum.call(this, num)
            }
            else {
                $btnMinus.removeClass('locked');
            }

            if (maximize != null && num >= maximize) {
                $btnPlugs.addClass('locked');
                onMaximize.call(this, num)
            }
            else {
                $btnPlugs.removeClass('locked');
            }

            UpdatePassengers();
            DisableEnableButton($input, num);
            onChange.call(this, num)
        }

        function UpdatePassengers() {
            var a = $('#adult').val();
            var ya = $('#youngAdult').val();
            var c = $('#child').val();
            var e = $('#infant').val();
            var tot = parseInt(a) + parseInt(ya) + parseInt(c) + parseInt(e);
            var totNoAdult = parseInt(ya) + parseInt(c) + parseInt(e);
            $(".passengers-horiz").text(tot);

            if(tot > 1){

                if(totNoAdult > 0){
					$("#apt").text(passengerstranslation);
                }else{
					$("#apt").text(adultstranslation);
                }

            }
            else {
            	$("#apt").text(adulttranslation);
            }
        }
        function DisableEnableButton(input, num) {

            var a = $('#adult').val();
            var ya = $('#youngAdult').val();
            var c = $('#child').val();
            var e = $('#infant').val();

            var tot = parseInt(ya) + parseInt(c) + parseInt(a)

            if (tot == 7) {
                $('.h-font-plus-other').addClass('locked');
            }
            else {
                $('.h-font-plus-other').removeClass('locked');

            }

            if (a == e) {
                $('#btnAdult').addClass('locked');
                $('.h-font-plus').addClass('locked');
            }
            else {
                if (a != 1) {
                    $('#btnAdult').removeClass('locked');
                    $('.h-font-plus').removeClass('locked');
                }
            }
        }
        return $handleCounter
    };
})(jQuery)