

$(document).ready( function() {

$('.checkinOnExternalSite').click(function(){
    deepLinkClick++;
    updateDataLayer('deepLinkClick',deepLinkClick);
});

$('.webCheckInDeepLink').click(function(){
    deepLinkClick++;
    updateDataLayer('deepLinkClick',deepLinkClick);
});

});

function countBPprinted(){

    bpPrinted++;
    updateDataLayer('bpPrinted',bpPrinted);
}

function countbpSent(){

    bpSent++;
    updateDataLayer('bpSent',bpSent);
}

function countOffload(){

    offloadDone++;
    updateDataLayer('offloadDone',offloadDone);
}

function countBpReprinted(){

    bpReprinted++;
    updateDataLayer('bpReprinted',bpReprinted);
}

function getCheckinComplete(){

    var checkinComplete;
    if($('.passenger').length){
        checkinComplete = $('.passenger').length;
    }
    updateDataLayer('checkinComplete',checkinComplete);

}

function updateDataLayer(key,value){

    localStorage.setItem(key,value);
}

function refreshDataLayer(){

    localStorage.removeItem('pnr');
    localStorage.removeItem('origin');
    localStorage.removeItem('originCity');
    localStorage.removeItem('destination');
    localStorage.removeItem('destinationCity');
    localStorage.removeItem('departureDate');
    localStorage.removeItem('roundTrip');
    localStorage.removeItem('flight');
    localStorage.removeItem('Returnflight');
    localStorage.removeItem('bpPrinted');
    localStorage.removeItem('bpSent');
    localStorage.removeItem('offloadDone');
    localStorage.removeItem('deepLinkClick');
    localStorage.removeItem('removedAncillary');
    localStorage.removeItem('revenueAncillary');
    localStorage.removeItem('insurance');
    localStorage.removeItem('bpReprinted');
    localStorage.removeItem('checkinComplete');
    localStorage.removeItem('checkinFail');
    localStorage.removeItem('checkinError');
    localStorage.removeItem('paxType');
    localStorage.removeItem('baggage');
    localStorage.removeItem('seat');
    localStorage.removeItem('lounge');
    localStorage.removeItem('fastTrack');
    localStorage.removeItem('insurance');
}

function getBaggageAmount(){

    var amount = 0.0;
    var baggageAmount = [];
    var baggage = "";

    $('div[data-ancillary-group="BG"]').each(function(){

        var baggageArr = [];
        var amountString = $(this).find($('.block-price')).find("span").text().split(" ");
        amount = parseFloat(amountString[0]);

        baggageArr.push(1);
        baggageArr.push(amount);
        baggageArr.push($(this).data("ancillary-code"));
        baggageAmount.push(baggageArr);
    });

    for(i = 0;i < baggageAmount.length;i++){
        var a = baggageAmount[i];
            if(i == 0){
            baggage = a[0] + ":" + a[1] + ":" + a[2];
            }
            if(i >= 1){
            baggage = baggage + "," + a[0] + ":" + a[1] + ":" + a[2];
            }
    }
    updateDataLayer('baggage',baggage);
}

function getSeatAmount(){

    var amount = 0.0;
    var seatAmount = [];
    var seat = "";

    $('div[data-ancillary-group="SA"]').each(function(){

        var seatArr = [];
        var amountString = $(this).find($('.block-price')).find("span").text().split(" ");
        amount = parseFloat(amountString[0]);

        seatArr.push(1);
        seatArr.push(amount);
        seatArr.push($(this).data("ancillary-code"));
        seatAmount.push(seatArr);
    });

        for(i = 0;i < seatAmount.length;i++){
            var a = seatAmount[i];
                if(i == 0){
                seat = a[0] + ":" + a[1] + ":" + a[2];
                }
                if(i >= 1){
                seat = seat + "," + a[0] + ":" + a[1] + ":" + a[2];
                }
        }
        updateDataLayer('seat',seat);
}

function getLoungeAmount(){

    var amount = 0.0;
    var loungeAmount = [];
    var lounge = "";

    $('div[data-ancillary-group="LG"]').each(function(){

        var loungeArr = [];
        var amountString = $(this).find($('.block-price')).find("span").text().split(" ");
        amount = parseFloat(amountString[0]);

            loungeArr.push(1);
            loungeArr.push(amount);
            loungeArr.push($(this).data("ancillary-code"));
            loungeAmount.push(loungeArr);
    });

            for(i = 0;i < loungeAmount.length;i++){
                var a = loungeAmount[i];
                    if(i == 0){
                    lounge = a[0] + ":" + a[1] + ":" + a[2];
                    }
                    if(i >= 1){
                    lounge = lounge + "," + a[0] + ":" + a[1] + ":" + a[2];
                    }
            }
            updateDataLayer('lounge',lounge);
}

function getFastTrackAmount(){

    var amount = 0.0;
    var fastTrackAmount = [];
    var fastTrack = "";

    $('div[data-ancillary-group="TS"]').each(function(){

        var fastTrackArr = [];
        var amountString = $(this).find($('.block-price')).find("span").text().split(" ");
        amount = parseFloat(amountString[0]);

        fastTrackArr.push(1);
        fastTrackArr.push(amount);
        fastTrackArr.push($(this).data("ancillary-code"));
        fastTrackAmount.push(fastTrackArr);
    });

     for(i = 0;i < fastTrackAmount.length;i++){
         var a = fastTrackAmount[i];
            if(i == 0){
             fastTrack = a[0] + ":" + a[1] + ":" + a[2];
             }
             if(i >= 1){
             fastTrack = fastTrack + "," + a[0] + ":" + a[1] + ":" + a[2];
             }
     }
     updateDataLayer('fastTrack',fastTrack);
}




