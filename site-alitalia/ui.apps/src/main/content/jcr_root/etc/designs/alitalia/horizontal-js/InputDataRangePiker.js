function SetDateInputCalendar(date, tipo, locale) {
    var d = new Date(date);
    var year = date.year();
    var month = date.month();
    var day = d.getDate();

    var mese=month+1;
	//var dataStringa = month + 1 + '/' + day + '/' + year;
    var dataStringa = day + '/' + mese + '/' + year;


    var monthString = locale.monthNames[month]
    if (tipo == "start") {
        $("#calendar").hide();
        var text = monthString + " " + year;
        $("#Depart").text(text);
        $("#DayDeparted").show();
        $("#DayDeparted").text(day);
        
        $("#DepartureDate").val(dataStringa);
   
        
    }
    else if (tipo == "end") {
        $("#calendar1").hide();
        var text = monthString + " " + year;
        $("#Return").text(text);
        $("#DayReturn").show();
        $("#DayReturn").text(day);
        $("#ReturnDate").val(dataStringa);
        
    }
};

