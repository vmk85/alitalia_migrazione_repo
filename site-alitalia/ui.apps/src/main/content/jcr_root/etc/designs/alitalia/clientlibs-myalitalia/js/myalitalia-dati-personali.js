var myAlitaliaDatiPersonali = {};

// Document ready
$(document).ready(function () {
	myAlitaliaDatiPersonali.initRadio();
});

myAlitaliaDatiPersonali.initRadio = function () {
	// Inputs radio
	var abitazione = $('#abitazione');
	var ufficio = $('#ufficio');
	abitazione.click();

	// Form collegati agli inputs radio
	var nomeAzienda = $('#nomeAzienda');
	// var ufficioForm = $('#ufficioForm');

	// Input radio abitazione checked di base
	// abitazione.closest('.radio-wrap').addClass('checked');

	// Attiva form ufficio e nascondi form abitazione
	ufficio.on('click', function () {
		if (!ufficio.is(':disabled')) {
			nomeAzienda.removeClass('hide');
		}
	});

	// Attiva form abitazione e nascondi form ufficio
	abitazione.on('click', function () {
		if (!abitazione.is(':disabled')) {
			if (!nomeAzienda.hasClass('hide'))
			nomeAzienda.addClass('hide');
		}
	});
};