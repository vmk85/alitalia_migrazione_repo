var ww, wh;
var tooltipOptions = {};
currentScrollbarWidth = 0;
var currHeight = 0;
var prevHeight = 0;

$( document ).ready( function() {
	tooltipOptions.tooltips = {};
	
	$('.skiplink__link').click(function(e){
    	var selector = $(this).attr('href');
        $(selector).focus();
    });
	
	getWindowSize();

	currHeight = prevHeight = wh;
	
	// init all tooltips
	$( '.has-tip' ).each( function( i, el ) {
		tooltipOptions.tooltips[ i ] = new Foundation.Tooltip( $(el), {
			allowHtml: true,
		} );
	} );
	currentScrollbarWidth = scrollbarWidth();

    if($('html').attr('dir') == "rtl"){
        $('.swiper-container').addClass('swiper-container-rtl');
    }
    // fix IOS virtual keyboard
    $( 'input, button, select, a' ).on( 'focus', function() {
     if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
      fixIosVirtualKeyboard($(this));
    }
    } );

    $('form.no-submit').on('submit', function(e){
      e.preventDefault;
      return false;
    });

} );

$( window ).on( 'resize', function() {
	getWindowSize();
	prevHeight = currHeight;
    currHeight = wh;
	currentScrollbarWidth = scrollbarWidth();
} );

$( window ).on( 'open.zf.reveal', function( event, name ) {
	if( Foundation.MediaQuery.atLeast( 'large' ) ) {
		$( 'body' ).css( 'padding-right', currentScrollbarWidth );
	}
 } );

$( window ).on( 'closed.zf.reveal', function( event, name ) {
	if( Foundation.MediaQuery.atLeast( 'large' ) ) {
		$( 'body' ).css( 'padding-right', 0 );
	}
 } );

$(window).on('changed.zf.mediaquery', function(event, name) {
    removeBodyLock();
    fixIosVirtualKeyboard();
});

function redirect(key,par) {
    if(key != null && key == "refresh") location.reload();
    else if(key != null) {
        var base = internalUrl1.substring(0, internalUrl1.length-"homepage".length);
        var path = base + key + ".html" + (par ? "?" + par : "");
        location.href = path;

}}

function scrollbarWidth() {
	// Scrollbalken im Body ausschalten
	document.body.style.overflow = 'hidden';
	var width = document.body.clientWidth;

	// Scrollbalken
	document.body.style.overflow = 'scroll';

	width -= document.body.clientWidth;

	// Der IE im Standardmode
	if(!width) width = document.body.offsetWidth-document.body.clientWidth;

	// ursprüngliche Einstellungen wiederherstellen
	document.body.style.overflow = '';

	return width;
}

function setBodyLock(){
    $('body').addClass('lock');
    $('body').height(window.innerHeight);
}

function removeBodyLock(){
    $('body').removeClass('lock');
    $('body').removeAttr('style');
}

function fixIosVirtualKeyboard($this) {
    if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
      $( 'input, button, select, a' ).attr( 'tabindex', -1 );
      if( typeof $this !== 'undefined' ) {
        $this.closest( 'form' ).find( 'input, button, select, a' ).removeAttr( 'tabindex' );
      }
    } else {
      $( 'input, button, select, a' ).removeAttr( 'tabindex' );
      $( '#panel-travel-options [id*="passenger"] input' ).attr( 'tabindex', -1 );
    }
 }

// open loader - passare elemento del dom (button) per il submit
function enableLoader($this) {
        $this.closest('.loader-wrap').addClass('loading');
    };

// close loader - passare elemento del dom (button) per il submit
function disableLoader($this) {
        $this.closest('.loader-wrap').removeClass('loading');
    };
