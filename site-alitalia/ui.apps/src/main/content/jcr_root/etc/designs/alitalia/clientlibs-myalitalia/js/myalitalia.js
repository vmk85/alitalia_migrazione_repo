var myAlitaliaOptions = {};
var feedbackRow;
// document ready
$( document ).ready( function() {

    // Setto la lingua della pagina corretta per le traduzioni
    Granite.I18n.setLocale(pageLanguage.toUpperCase());

    verticalCenter();
    $(window).resize(verticalCenter);

    /** sidebar mobile non loggato **/
//	$('.sidebar-link').on('click', function () {
//		$(this).toggleClass('open');
//		$('.sidebar-nav-mob .menu').slideToggle();
//		return false;
//	});
//
//	/** sidebar mobile non loggato - voce di menu **/
//	$('.info-user__personal-area-toggle').on('click', function () {
//		$(this).toggleClass('open');
//		$('.info-user__personal-area-content').slideToggle(400, function(){
//			verticalCenter();
//		});
//		verticalCenter();
//		return false;
//	});


    $(document).on("click", ".link__toggle", function() {
        var linkToggle = $(this);
        $(this).closest(".link__toggle-wrap").find(".content__toggle").stop().slideToggle(function() {
            if ($(this).is(':visible')) {
                $(".link__toggle-open", linkToggle).addClass("hide");
                $(".link__toggle-close", linkToggle).removeClass("hide");
            } else {
                $(".link__toggle-close", linkToggle).addClass("hide");
                $(".link__toggle-open", linkToggle).removeClass("hide");
            }
            $('html,body').animate({
                scrollTop: $(linkToggle).closest('.flight-info').find('.flight-info__title').offset().top},'slow');
        });
        if ($(this).parent('.flight-info__link').length > 0) {
            $(this).parent('.flight-info__link').toggleClass('open');
        }
        return false;
    });

    myAlitaliaOptions.initCheckbox();

    // Sticky footer
    var siteWrap = $('.myalitalia-site-wrap');
    var footer = $('.footer');
    var push = $('.push');

    push.css('height', footer.height());
    siteWrap.css('margin-bottom', 0 - footer.height());

    $(window).resize(function() {
        push.css('height', footer.height());
        siteWrap.css('margin-bottom', 0 - footer.height());

        // Display block on desktop
        ww = $(window).width();

        if (ww >= 768) {
            $('.flight-info .content__toggle').removeAttr('style');
            $('.flight-info .flight-info__link').removeClass('open');
            $('.flight-info .link__toggle-close').addClass('hide');
            $('.flight-info .link__toggle-open').removeClass('hide');
        }
    });
    // Sticky footer ends

    // Slider Tutorial e Novità
    $( '[data-remove] [data-close]' ).on( 'click', function() {
        var needAlert = $( this ).data( 'close' ) == 'alert';
        var restartSlide = $( this ).data( 'close' ) == 'restart';
        if( needAlert ) {
            $( this ).closest( '[data-remove]' ).find( '.alert-close' ).show().animate( { opacity: 1, }, 400 );
        } else if( restartSlide ) {
            if( $( this ).closest( '[data-remove]' ).hasClass( 'tutorial-stripe' ) ) {
                myAlitaliaOptions.sliderTutorial.slideTo(0, 0);
            }
            if( $( this ).closest( '[data-remove]' ).hasClass( 'novita__slider' ) ) {
                myAlitaliaOptions.sliderNovita.slideTo(0, 0);
            }
            $( this ).closest( '[data-remove]' ).find( '.alert-close' ).hide().css( { opacity: 0, } );
        } else {
            $( this ).closest( '[data-remove]' ).hide().css( { opacity: 0, } );
        }
    } );

    myAlitaliaOptions.sliderTutorial = new Swiper('.tutorial-stripe .swiper-container', {
        slidesPerView: 'auto',
        resistanceRatio: 0,
        pagination: {
            el: '.tutorial-stripe .swiper-pagination',
            clickable: true
        }
        // 	el: '.novita__slider .swiper-pagination',
        // 	clickable: true
        // }
    });
    myAlitaliaOptions.sliderTutorial.on('reachEnd', function() {
        $( '.tutorial-stripe .alert-close' ).show().animate( { opacity: 1, }, 400 );
    });
    // Slider Tutorial e Novità ends

    // MILLEMIGLIA
    $( '.prizes-and-services [data-interchange]' ).foundation();
    // height equalizer
    if( $('.prizes-and-services').length ) {
        $( '.prizes-and-services .bg-image' ).each( function( i, el ) {
            $( el ).height( $( el ).closest( '.custom-card' ).find( '.custom-card__section--text' ).height() );
        } );
    }
    $( window ).on( 'resize', function() {
        // height equalizer
        $( '.prizes-and-services .bg-image' ).each( function( i, el ) {
            $( el ).height( $( el ).closest( '.custom-card' ).find( '.custom-card__section--text' ).height() );
        } );
    } );

    $( '.widget-messaggi .message [data-toggle]' ).on( 'click', function() {
        // se unread segna come letto
        $( this ).closest( '.message' ).hasClass( 'unread' ) ? $( this ).closest( '.message' ).removeClass( 'unread' ) : false;
        $( this ).closest( '.message' ).toggleClass( 'open' );
    } );
    // MILLEMIGLIA

    // INIZIO copiato da checkin.js

    myAlitaliaOptions.fixedSlidePosition = false;

    myAlitaliaOptions.sliderNovita = new Swiper('.novita__slider .swiper-container', {
        slidesPerView: 'auto',
        resistanceRatio: 0,
        pagination: {
            el: '.novita__slider .swiper-pagination',
            clickable: true
        }
    });

    // Slider Tutorial e Novità
    myAlitaliaOptions.sliderNovita.on('reachEnd', function() {
        $( '.novita__slider .alert-close' ).show().animate( { opacity: 1, }, 400 );
    });
    // Slider Tutorial e Novità ends

    $('.novita__slider .bg-image').foundation();

    myAlitaliaOptions.sliderPaginationVisibility();
    myAlitaliaOptions.equalizeNovitaSliderHeight();

    // FINE copiato da checkin.js

    // INIZIO copiato da lista-offerte.js

    //  select
    $( '.select--departure' ).on( 'change', myAlitaliaOptions.getDestinationList );

    myAlitaliaOptions.initSlider();
    // myAlitaliaOptions.initAccordion();
    myAlitaliaOptions.equalizeListaOfferteSliderHeight();
    myAlitaliaOptions.displaySelectValue();

    setTimeout(function(){
        if( Foundation.MediaQuery.atLeast( 'large' ) ) {
            // myAlitaliaOptions.setAccordionDesktop();
            // myAlitaliaOptions.equalizeAccordionHeight();
        } else {
            // myAlitaliaOptions.setAccordionMobile();
        }
    }, 100);

    //myAlitaliaOptions.initSlider(); // duplicato?

    // FINE copiato da lista-offerte.js

    myAlitaliaOptions.initFiltriSelector();

    // Perché registrarsi - tabs mobile/tablet

    $('.millemiglia-tabs').foundation();

    /** MYALITALIA DATI PERSONALI **/


    // initRadio function
    initRadio();

    function initRadio() {
        $('.radio-wrap input:checked').closest('.radio-wrap').addClass('checked');
        $(document).on('click', '.radio-wrap .placeholder', function () {
            $(this).parent().find('input').click();
        });
        $(document).on('click', '.radio-wrap input', function () {
            $('.radio-wrap input[name="' + $(this).attr('name') + '"]').closest('.radio-wrap').removeClass('checked');
            $('.radio-wrap input[name="' + $(this).attr('name') + '"]').attr("checked", false);
            $(this).closest('.radio-wrap').addClass('checked');
            $(this).closest('.radio-wrap').find("input").attr("checked", true);
        });
    }

    // Form Modifica Dati


    var disabledInputs = $('.disabled-input');
    var disabledLabels = $('.disabled-label');
    var feedbackRow = $('.notification-row');
    var feedbackMessage = '<p>Dati salvati correttamente!</p>';


    /** FINE MYALITALIA DATI PERSONALI **/
    myAlitaliaOptions.initReveal();

    /** RICHIEDI FATTURA */
    var richiediFattura = $('.richiedi-fattura');
    var annullaFattura = $('.annulla-fattura');

    richiediFattura.on('click', function() {
        // Mostra div fattura
        $(this).closest('.widget-research').find('.fattura-richiesta').toggleClass('hide');

        // Nascondi richiedi-fattura
        $(this).toggleClass('hide');

        // Nascondi colonne modifica-dati-fattura e cancella-fattura
        $(this).closest('.widget-research').find('.modifica-dati-fattura').toggleClass('hide');
        $(this).closest('.widget-research').find('.cancella-fattura').toggleClass('hide');

        // Leva la classe border-top
        $(this).parent().toggleClass('border-top');

        // Leva classe riepilogo
        $(this).closest('.widget-research').parent().toggleClass('riepilogo');
    });

    annullaFattura.on('click', function(e) {
        e.preventDefault();

        // Nascondi div fattura
        $(this).closest('.widget-research').find('.fattura-richiesta').toggleClass('hide');

        // Mostra richiedi-fattura
        $(this).closest('.widget-research').find('.richiedi-fattura').toggleClass('hide');

        // Mostra colonne modifica-dati-fattura e cancella-fattura
        $(this).closest('.widget-research').find('.modifica-dati-fattura').toggleClass('hide');
        $(this).closest('.widget-research').find('.cancella-fattura').toggleClass('hide');

        // Aggiungi la classe border-top
        $(this).closest('.widget-research').find('.richiedi-fattura').parent().toggleClass('border-top');

        // Aggiungi la classe riepilogo
        $(this).closest('.widget-research').parent().toggleClass('riepilogo');
    });

    /** FINE RICHIEDI FATTURA */


});

// INIZIO copiato da checkin.js

var ww = $(window).width();
$(window).resize(function() {
    if(ww < 768 && !myAlitaliaOptions.fixedSlidePosition) {
        setTimeout(function() {
            if ($('.novita__slider .swiper-container').length) myAlitaliaOptions.sliderNovita.slideTo(0, 0);
            myAlitaliaOptions.fixedSlidePosition = true;
        }, 1);
    } else if(ww >= 768) myAlitaliaOptions.fixedSlidePosition = false;

    setTimeout(function() {
        myAlitaliaOptions.equalizeNovitaSliderHeight();
    }, 100);

});


myAlitaliaOptions.equalizeNovitaSliderHeight = function() {
    $( '.novita__slider .custom-card__section--text' ).removeAttr( 'style' );
    myAlitaliaOptions.cardMaxHeight = $( '.novita__slider  .swiper-wrapper' ).outerHeight() - $( '.novita__slider .custom-card__section.bg-image' ).outerHeight();
    $( '.novita__slider .custom-card__section--text' ).css( 'height', myAlitaliaOptions.cardMaxHeight );
};

myAlitaliaOptions.sliderPaginationVisibility = function() {
    if( $('.novita__slider .swiper-slide').length < 2 ){
        $('.novita__slider .swiper-pagination').addClass('hide-for-large');
    }
};

// FINE copiato da checkin.js


myAlitaliaOptions.initCheckbox = function() {
    $('.checkbox-wrap input:checked').each(function(i, el){
        $(el).closest('.field-wrap').addClass('checked');
    });


    $(document).on('click', '.checkbox-wrap .placeholder', function() {
        $(this).parent().find('input').click();
    });
    $(document).on('click', '.checkbox-wrap input', function() {
        if($(this).closest('.field-wrap').length > 0)
            if($(this).closest('.field-wrap').hasClass('checked')) {
                $(this).closest('.field-wrap').removeClass('checked');
            } else {
                $(this).closest('.field-wrap').addClass('checked');
            } else {
            if(typeof $(this).attr("checked") == typeof undefined) {
                $(this).attr("checked", true);
            } else $(this).removeAttr("checked");
        }

        var relatedContent = $(this).closest('.passenger-wrap').find('.passenger-wrap-content');
        if (relatedContent.length > 0 && relatedContent.is(':visible')) {
            relatedContent.slideUp();
        } else {
            relatedContent.slideDown();
        }
    });

//	$(document).on('click', '.passenger-wrap .edit', function() {
//		$(this).closest('.passenger-wrap').find('.passenger-wrap-content').slideDown();
//		$(this).hide();
//	});

    $(document).on('click', '.passenger-wrap-content .close', function() {
//		$(this).closest('.passenger-wrap').find('.edit').show();
        $(this).closest('.passenger-wrap-content').slideUp();
    });

}

myAlitaliaOptions.resetCheckbox = function() {
    if($('.checkbox-wrap input').not(':checked').length) {
        $('.checkbox-wrap input').not(':checked').closest('.field-wrap').removeClass('checked');
    }
}



// INIZIO copiato da lista-offerte.js

$( window ).on( 'resize', function() {

    setTimeout( function() {
        myAlitaliaOptions.equalizeListaOfferteSliderHeight();

        if( Foundation.MediaQuery.atLeast( 'large' ) ) {
            // myAlitaliaOptions.equalizeAccordionHeight();
        }
    }, 100 );

} );

// change media query
$(window).on('changed.zf.mediaquery', function(event, name) {

    // If breakpoint is large and up, reinit the tabs
    if( Foundation.MediaQuery.atLeast( 'large' ) ) {
        // accordion
        // myAlitaliaOptions.setAccordionDesktop();
    } else {
        // slider
        setTimeout( function() {
            if ($('.lista-offerte__slider .swiper-container').length) myAlitaliaOptions.sliderListaOfferte.slideTo( 0, 0 );
        }, 1 );
        // accordion
        // myAlitaliaOptions.setAccordionMobile();
        // remove accordion equalizer
        $( '.lista-offerte__accordion .lista-destinazioni__item' ).removeAttr( 'style' );
    }

});

myAlitaliaOptions.displaySelectValue = function() {
    $( '.lista-offerte__select .current-departure__content' ).html(
        $( '.lista-offerte__select .select--departure option:selected' ).html()
    );
};

myAlitaliaOptions.initSlider = function() {
    if( myAlitaliaOptions.sliderListaOfferte !== undefined ) {
        myAlitaliaOptions.sliderListaOfferte.destroy();
    }

    myAlitaliaOptions.sliderListaOfferte = new Swiper( '.lista-offerte__slider .swiper-container', {
        slidesPerView: 'auto',
        resistanceRatio: 0,
        pagination: {
            el: '.lista-offerte__slider .swiper-pagination',
            clickable: true
        }
    } );

    $( '.lista-offerte__slider .bg-image' ).foundation();
};

myAlitaliaOptions.equalizeListaOfferteSliderHeight = function() {
    $( '.lista-offerte__slider .custom-card__section--text' ).removeAttr( 'style' );
    myAlitaliaOptions.cardMaxHeight = $( '.lista-offerte__slider .swiper-wrapper' ).outerHeight() - $( '.lista-offerte__slider .custom-card__section.bg-image' ).outerHeight();
    $( '.lista-offerte__slider .custom-card__section--text' ).css( 'height', myAlitaliaOptions.cardMaxHeight );
};

myAlitaliaOptions.equalizeAccordionHeight = function() {
    $( '.lista-offerte__accordion .lista-destinazioni__item' ).removeAttr( 'style' );
    myAlitaliaOptions.accordionItemMaxHeight = 0;
    // $( '.lista-offerte__accordion .lista-destinazioni__item' ).removeAttr( 'style' );
    $( '.lista-offerte__accordion .lista-destinazioni__item' ).each( function( i, el ) {
        if( $( el ).outerHeight() > myAlitaliaOptions.accordionItemMaxHeight ) {
            myAlitaliaOptions.accordionItemMaxHeight = $( el ).outerHeight();
        }
    } );
    $( '.lista-offerte__accordion .lista-destinazioni__item' ).css( 'height', myAlitaliaOptions.accordionItemMaxHeight );
};

myAlitaliaOptions.initAccordion = function() {
    $( '.lista-offerte__accordion .accordion' ).foundation();
    myAlitaliaOptions.accordion = new Foundation.Accordion( $( '.lista-offerte__accordion .accordion' ), {
        // slideSpeed: 0,
        multiExpand: false,
        allowAllClosed: true,
    } );
};

myAlitaliaOptions.setAccordionDesktop = function() {
    $( '.lista-offerte__accordion .accordion' ).foundation( 'down', $( '.lista-offerte__accordion .accordion-content' ) );
    $( '.lista-offerte__accordion .accordion' ).attr( 'disabled', true );
};

myAlitaliaOptions.setAccordionMobile = function() {
    $( '.lista-offerte__accordion .accordion' ).removeAttr( 'disabled' );
    $( '.lista-offerte__accordion .accordion' ).foundation( 'up', $( '.lista-offerte__accordion .accordion-content' ) );
    // $( '.lista-offerte__accordion .accordion' ).foundation( 'down', $( '.lista-offerte__accordion .accordion-item:first-child .accordion-content' ) );
};

myAlitaliaOptions.getDestinationList = function() {
    myAlitaliaOptions.displaySelectValue();

    // get new destionation slider
    $.ajax( {
        type: "GET",
        url: 'etc/designs/alitalia/clientlibs-lista-offerte/inc/' + $( '.select--departure' ).val().toLowerCase() + '-slider.html',
        dataType: 'html',
        success: function( data ) {
            $( '.lista-offerte__slider' ).html( data ).ready( function() {
                myAlitaliaOptions.initSlider();
            } );
        },
        complete: function() {
            myAlitaliaOptions.equalizeListaOfferteSliderHeight();
        },
        error: function() {
            console.log( 'errore chiamata slider' );
        }
    } );

    // get new destionation accordion
    $.ajax( {
        type: "GET",
        url: 'etc/designs/alitalia/clientlibs-lista-offerte/inc/' + $( '.select--departure' ).val().toLowerCase() + '-accordion.html',
        dataType: 'html',
        success: function( data ) {
            $( '.lista-offerte__accordion' ).html( data ).ready( function() {
                // myAlitaliaOptions.initAccordion();
            } );
        },
        complete: function() {
            setTimeout( function() {
                if( Foundation.MediaQuery.is( 'large only' ) ) {
                    // myAlitaliaOptions.setAccordionDesktop();
                    // myAlitaliaOptions.equalizeAccordionHeight();
                } else {
                    // myAlitaliaOptions.setAccordionMobile();
                }
            }, 100 );
        },
        error: function() {
            console.log( 'errore chiamata accordion' );
        }
    });
};

// FINE copiato da lista-offerte.js





myAlitaliaOptions.initFiltriSelector = function() {

    $('.action__icon--filtri').click(function() {

        var size = Foundation.MediaQuery.current;
        if (size == 'medium' || size == 'small') {
            $('body').addClass('is-reveal-open');
            $('.filters').addClass('reveal-mode');
        }

        return false;
    });

    $('.filters .close-button, .filters .button-wrap .button').click(function() {
        $('body').removeClass('is-reveal-open');
        $('.filters').removeClass('reveal-mode');
    });

    $(window).on('changed.zf.mediaquery', function(event, newSize, oldSize){
        if (newSize == 'large') {
            $('body').removeClass('is-reveal-open');
            $('.filters').removeClass('reveal-mode');
        }
    });
}

// INIT REVEAL

myAlitaliaOptions.initReveal = function() {
    setTimeout(function() {
        if ($('#signin-myalitalia').length) $('#signin-myalitalia').foundation('open');
    }, 1000);

    $('.reveal--tutorial').foundation();
}

myAlitaliaOptions.feedbackSuccess = function() {
    if(feedbackRow){
        feedbackRow.hide().removeClass("alert").addClass('confirm-status').html("<p>" + CQ.I18n.get("myalitalia.feedbackSuccess.saveData") + "</p>").delay(100).slideDown().delay(5000).slideUp();
        $("html, body").animate({ scrollTop: 0 });
        $("#errorPass").addClass("hide");
        $("input[type='password'][name='currentP']").removeClass("border-red");
    }
}

myAlitaliaOptions.flightSuccess = function() {
    if(feedbackRow){
        feedbackRow.hide().removeClass("alert").addClass('confirm-status').html("<p>" + CQ.I18n.get("myalitalia.feedbackSuccess.pnrAdded") + "</p>").delay(100).slideDown().delay(5000).slideUp();
        $("html, body").animate({ scrollTop: 0 });
    }
}

myAlitaliaOptions.feedbackFail = function() {
    feedbackRow.hide().removeClass("confirm-status").addClass('alert').html("<p>" + CQ.I18n.get("myalitalia.feedbackFail.NotSaveData") + "</p>").delay(100).slideDown().delay(5000).slideUp();
    $("html, body").animate({ scrollTop: 0 });
}

myAlitaliaOptions.feedbackFailPassword = function() {
//    feedbackRow.hide().removeClass("confirm-status").addClass('alert').html("<p>" + CQ.I18n.get("myalitalia.feedbackFailPassword.saveData") + "</p>").delay(100).slideDown().delay(5000).slideUp();
    $("#errorPass").removeClass("hide").text(CQ.I18n.get("myalitalia.feedbackFailPassword.saveData")).addClass("alert");
    $("input[type='password'][name='currentP']").addClass("border-red");
//    $("html, body").animate({ scrollTop: 0 });
}

myAlitaliaOptions.feedbackDelete= function(label) {
    $("[data-close]").trigger("click");
    feedbackRow.hide().removeClass("alert").addClass('confirm-status').html("<p>" + CQ.I18n.get(label) + "</p>").delay(100).slideDown().delay(5000).slideUp();
    $("html, body").animate({ scrollTop: 0 })
}
myAlitaliaOptions.feedbackDeleteError= function(label) {
    feedbackRow.addClass("alert").html("<p>" + CQ.I18n.get(label) + "</p>").delay(100).slideDown().delay(5000).slideUp();
    $("html, body").animate({ scrollTop: 0 });
}

myAlitaliaOptions.feedbackSuccessWAnchor = function(anchor) {
    feedbackRow.hide().removeClass("alert").addClass('confirm-status').html("<p>" + CQ.I18n.get("myalitalia.feedbackSuccess.saveData") + "</p>").delay(100).slideDown().delay(5000).slideUp();
//    $("html, body").animate({ scrollTop: 0 });
    $(document).scrollTop( $("#" + anchor).offset().top );
}

