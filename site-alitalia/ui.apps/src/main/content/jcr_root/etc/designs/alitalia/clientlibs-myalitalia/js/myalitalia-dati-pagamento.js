var paymentOptions = {};

$(document).ready( function() {
	paymentOptions.initRadio();
	paymentOptions.initSelect();
	paymentOptions.onChangeField();
	paymentOptions.submitForm();
});

paymentOptions.initRadio = function() {
	if($('.myalitalia-payment .radio-wrap input:checked').length) {
		$('.myalitalia-payment .radio-wrap input:checked').closest('.myalitalia-payment .radio-wrap').addClass('checked');
	}
	$(document).on('click', '.myalitalia-payment .radio-wrap .placeholder', function() {
		$(this).parent().find('input').click();
	});
	$(document).on('click', '.myalitalia-payment .radio-wrap input', function() {

		$('.myalitalia-payment .radio-wrap input[name='+$(this).attr('name')+']').closest('.myalitalia-payment .radio-wrap').removeClass('checked');
		$(this).closest('.myalitalia-payment .radio-wrap').addClass('checked');
		// switch payment form radio

		if ($(this).closest('.radio-wrap').is(':visible')) {
			if ($(".myalitalia-payment .radio-wrap .checked").attr("value", $(this).val())) {
				$(".payment-form").addClass("hide");
				$("#" + $(this).attr('value')).removeClass("hide");
				$(".select-card select").val($(this).val());
			}
		}
	});
};

// switch payment form select
paymentOptions.initSelect = function() {
		$(".select-card select").on('change',  function() {
			if ($(".select-card").is(":visible")) {
				if ($(".select-card select option:selected").val()) {
					$(".payment-form").addClass("hide");
					$("#" + $(this).val()).removeClass("hide");
					$('.radio-wrap input[value="' + $(".select-card select option:selected").val() + '"]').click();
				}
			}
		});
};
// on change input / select
paymentOptions.onChangeField = function() {
	$(".myalitalia-payment input, .myalitalia-payment select").on('change', function () {
		console.log('change')
		paymentOptions.chekFieldEmpty('#'+$(this).closest('form').attr('id'));
	});
}
// check field empty
paymentOptions.chekFieldEmpty = function(formId) {
	var empty = 0;
	$.each($('input', formId), function(i, el){
		if ($(el).val() == '') empty++;
	});
	console.log($(formId + ' .button-wrap button'))
	if (empty == 0){
		$('.button-wrap .button-form-submit', formId).removeAttr('disabled').removeClass('button--disabled');
	} else {
		$('.button-wrap .button-form-submit', formId).attr('disabled', 'disabled');
		$('.button-wrap .button-form-submit', formId).addClass('button--disabled');
	}
}

paymentOptions.submitForm = function() {
	$(".button-form-submit").on('click', function () {
		if(true) { // validazione a buon fine
			$(this).closest('form').submit();
		} else {
			// show feedback
		}
	});
}
