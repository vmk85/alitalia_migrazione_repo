var seatmapOptions = {};
var pageInitialized = false;
// document ready
$( document ).ready( function() {




    // seatmap.initAccordion();

    seatmapOptions.initPlaceTooltips();
    seatmapOptions.initRevealConfirm();


//start new code
//$(function()
//{
    if(pageInitialized) return;
    pageInitialized = true;
    //console.log("TUTTO OK?");
    $('.seats-list ul li a').on('click',function(){
        $("#seats-list-mobile").html($(this).html());

        //var idPassenger = $(this).find('span.id').html();
        var idPassenger = $(this).parent().data("passenger");
        //console.log("PARENT ",$(this).parent().parent());
        seatmapOptions.selectOne(idPassenger);
    });

//});


} );

seatmapOptions.selectOne = function(passenger){
    console.log("seatmapOptions.selectOne passenger--> ",passenger);
    $('.seats-list ul li').removeClass('selected');
    var indexUl = parseInt(passenger);
    $('.seats-list ul li:nth-child('+indexUl+')').addClass('selected');
    $('.seatMapDiv').css('display','none');
    $('.seatMapDiv[data-passenger-id='+indexUl+']').css('display','block');


}
//end new code

/*faqOptions.initAccordion = function() {
	$( '.faq__accordion .accordion' ).foundation();
	faqOptions.accordion = new Foundation.Accordion( $( '.faq__accordion .accordion' ), {
		// slideSpeed: 0,
		multiExpand: false,
		allowAllClosed: true,
	} );
};*/

seatmapOptions.initPlaceTooltips = function() {
    $('.chooseSeat__seats .extraComfort').children('a').click(function() {
        if( Foundation.MediaQuery.is( 'large' ) ) {
            $('.tooltip-desktop[data-row="' + $(this).parent().data('row') + '"][data-col="' + $(this).parent().data('col') + '"]').show();
        } else {
            $('.tooltip-mobile[data-row="' + $(this).parent().data('row') + '"][data-col="' + $(this).parent().data('col') + '"]').show();
        }
        return false;
    });
    $('.tooltip .close-button').click(function() {
        $(this).closest('.tooltip').hide();
    });


}
seatmapOptions.initRevealConfirm = function() {
    $('.checkin-seleziona-posto .seat-configurator .checkin-button-wrap .button').click(function() {
        if( !Foundation.MediaQuery.is( 'large' ) ) {
            $(this).closest('.seat-configurator').find('.close-button').trigger('click');
        }
    });

}