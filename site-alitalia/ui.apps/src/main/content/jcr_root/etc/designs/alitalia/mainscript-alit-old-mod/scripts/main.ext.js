// base js

mobileCheck = function() {
	var check;
	check = false;
	(function(a) {
		if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
			return check = true;
		}
	})(navigator.userAgent || navigator.vendor || window.opera);
	return check;
};


window.supportsTouch = Modernizr.touch || ("ontouchstart" in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0) || (navigator.userAgent.indexOf("IEMobile") !== -1) || false;
window.ismobile = mobileCheck();


window.isLtr = true;
window.isRtl = false;
if ( $("html").attr("dir")=== "rtl" ){
	window.isLtr = false;
	window.isRtl = true;
}

// breakpoints

window.breakpointS = 641
window.breakpointM = 801
window.breakpointL = 1024


window.menuSpeed = 500
window.menuSpeedTimer = 600

// window width
window.windowW = $(window).width();

window.bookingHeaderStop = false;

window.bookingLoaderMove = false;

window.proportionValue = 1;

$(function(){
	if ( window.supportsTouch ){
		$('body').addClass('isTouch');
	}
});

function proportionSetter() {
	var size = viewportSize()
	if ( size  === 'zoomout') {
		$('body').addClass('bookingPage');
	} else {
		$('body').removeClass('bookingPage');
	}
}

function viewportSize() {
	/* retrieve the content value of .cd-main::before to check the actua mq */
	// return window.getComputedStyle(document.querySelector('body'), '::before').getPropertyValue('content').replace(/"/g, "").replace(/'/g, "");
	return window.getComputedStyle(document.getElementsByTagName("body")[0], '::before').getPropertyValue('content').replace(/"/g, "").replace(/'/g, "");

}

$(window).resize(function(event) {

	window.windowW = $(window).width();
	if($('body').hasClass('userMenuIsActive') && window.windowW > 800){
		_data_group = $('.j-groupContainer.isActive').attr('data-group');
		if( window.isRtl && (_data_group === "notificationGroup")) {
			_offset_left = $(".userMenu__notifications").offset().left;
		} else if( window.isRtl && (_data_group === "recentGroup")) {
			_offset_left = $(".userMenu__recent").offset().left;
		} else {
			_offset_left = $('.header').find('.j-userMenuLink[data-group="'+_data_group+'"]').offset().left - $('.j-groupContainer.isActive').outerWidth() + $('.header').find('.j-userMenuLink[data-group="'+_data_group+'"]').outerWidth();
		}
		$('.j-groupContainer.isActive').css({'left': _offset_left+'px'});
	}
	if($('.flyoutBookingFlightDetails').length > 0){
		var positionTop = $('.flyoutBookingFlightDetails').data('element').offset().top;
		var positionLeft = $('.flyoutBookingFlightDetails').data('element').offset().left ;
		$('.flyoutBookingFlightDetails').css({
			'left': positionLeft - 15 + 'px',
			'top': positionTop + 25 + 'px',
		})
	}
	if($('#fd-flightStatusDatePicker').length > 0){
		var topPos = $('#fd-but-flightStatusDatePicker').offset().top + $('#fd-but-flightStatusDatePicker').outerHeight();
		if ( $('.infoVoli__main').length > 0 ) {
			var leftPos = $('.infoVoli__main').offset().left + 18;
		}
		else if ( $('.tabsInfoAccordion__body').length > 0 ) {
			var leftPos = $('.tabsInfo__content').offset().left + 10;
		}
		$('#fd-flightStatusDatePicker').css({
				'left': leftPos + 'px',
				'top': topPos + 'px'
		});
	}
	if($('#fd-flightStatusDatePicker2').length > 0){
		var topPos = $('#fd-but-flightStatusDatePicker2').offset().top + $('#fd-but-flightStatusDatePicker2').outerHeight();
		if ( $('.infoVoli__main').length > 0 ) {
			var leftPos = $('.infoVoli__main').offset().left + 18;
			$('#fd-flightStatusDatePicker2').css({
				'left': leftPos + 'px',
				'top': topPos + 'px'
			});
		}

	}

});

/*
 * Metadata - jQuery plugin for parsing metadata from elements
 *
 * Copyright (c) 2006 John Resig, Yehuda Katz, Jörn Zaefferer, Paul McLanahan
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 */

/**
 * Sets the type of metadata to use. Metadata is encoded in JSON, and each property
 * in the JSON will become a property of the element itself.
 *
 * There are three supported types of metadata storage:
 *
 *   attr:  Inside an attribute. The name parameter indicates *which* attribute.
 *          
 *   class: Inside the class attribute, wrapped in curly braces: { }
 *   
 *   elem:  Inside a child element (e.g. a script tag). The
 *          name parameter indicates *which* element.
 *          
 * The metadata for an element is loaded the first time the element is accessed via jQuery.
 *
 * As a result, you can define the metadata type, use $(expr) to load the metadata into the elements
 * matched by expr, then redefine the metadata type and run another $(expr) for other elements.
 * 
 * @name $.metadata.setType
 *
 * @example <p id="one" class="some_class {item_id: 1, item_label: 'Label'}">This is a p</p>
 * @before $.metadata.setType("class")
 * @after $("#one").metadata().item_id == 1; $("#one").metadata().item_label == "Label"
 * @desc Reads metadata from the class attribute
 * 
 * @example <p id="one" class="some_class" data="{item_id: 1, item_label: 'Label'}">This is a p</p>
 * @before $.metadata.setType("attr", "data")
 * @after $("#one").metadata().item_id == 1; $("#one").metadata().item_label == "Label"
 * @desc Reads metadata from a "data" attribute
 * 
 * @example <p id="one" class="some_class"><script>{item_id: 1, item_label: 'Label'}</script>This is a p</p>
 * @before $.metadata.setType("elem", "script")
 * @after $("#one").metadata().item_id == 1; $("#one").metadata().item_label == "Label"
 * @desc Reads metadata from a nested script element
 * 
 * @param String type The encoding type
 * @param String name The name of the attribute to be used to get metadata (optional)
 * @cat Plugins/Metadata
 * @descr Sets the type of encoding to be used when loading metadata for the first time
 * @type undefined
 * @see metadata()
 */

(function($) {

$.extend({
	metadata : {
		defaults : {
			type: 'class',
			name: 'metadata',
			cre: /({.*})/,
			single: 'metadata'
		},
		setType: function( type, name ){
			this.defaults.type = type;
			this.defaults.name = name;
		},
		get: function( elem, opts ){
			var settings = $.extend({},this.defaults,opts);
			// check for empty string in single property
			if ( !settings.single.length ) settings.single = 'metadata';
			
			var data = $.data(elem, settings.single);
			// returned cached data if it already exists
			if ( data ) return data;
			
			data = "{}";
			
			if ( settings.type == "class" ) {
				var m = settings.cre.exec( elem.className );
				if ( m )
					data = m[1];
			} else if ( settings.type == "elem" ) {
				if( !elem.getElementsByTagName )
					return undefined;
				var e = elem.getElementsByTagName(settings.name);
				if ( e.length )
					data = $.trim(e[0].innerHTML);
			} else if ( elem.getAttribute != undefined ) {
				var attr = elem.getAttribute( settings.name );
				if ( attr )
					data = attr;
			}
			
			if ( data.indexOf( '{' ) <0 )
			data = "{" + data + "}";
			
			data = eval("(" + data + ")");
			
			$.data( elem, settings.single, data );
			return data;
		}
	}
});

/**
 * Returns the metadata object for the first member of the jQuery object.
 *
 * @name metadata
 * @descr Returns element's metadata object
 * @param Object opts An object contianing settings to override the defaults
 * @type jQuery
 * @cat Plugins/Metadata
 */
$.fn.metadata = function( opts ){
	return $.metadata.get( this[0], opts );
};

})(jQuery);

/* qTip2 v2.2.1 | Plugins: tips modal viewport svg imagemap ie6 | Styles: core basic css3 | qtip2.com | Licensed MIT | Sat Sep 06 2014 23:12:07 */

!function(a,b,c){!function(a){"use strict";"function"==typeof define&&define.amd?define(["jquery"],a):jQuery&&!jQuery.fn.qtip&&a(jQuery)}(function(d){"use strict";function e(a,b,c,e){this.id=c,this.target=a,this.tooltip=F,this.elements={target:a},this._id=S+"-"+c,this.timers={img:{}},this.options=b,this.plugins={},this.cache={event:{},target:d(),disabled:E,attr:e,onTooltip:E,lastClass:""},this.rendered=this.destroyed=this.disabled=this.waiting=this.hiddenDuringWait=this.positioning=this.triggering=E}function f(a){return a===F||"object"!==d.type(a)}function g(a){return!(d.isFunction(a)||a&&a.attr||a.length||"object"===d.type(a)&&(a.jquery||a.then))}function h(a){var b,c,e,h;return f(a)?E:(f(a.metadata)&&(a.metadata={type:a.metadata}),"content"in a&&(b=a.content,f(b)||b.jquery||b.done?b=a.content={text:c=g(b)?E:b}:c=b.text,"ajax"in b&&(e=b.ajax,h=e&&e.once!==E,delete b.ajax,b.text=function(a,b){var f=c||d(this).attr(b.options.content.attr)||"Loading...",g=d.ajax(d.extend({},e,{context:b})).then(e.success,F,e.error).then(function(a){return a&&h&&b.set("content.text",a),a},function(a,c,d){b.destroyed||0===a.status||b.set("content.text",c+": "+d)});return h?f:(b.set("content.text",f),g)}),"title"in b&&(d.isPlainObject(b.title)&&(b.button=b.title.button,b.title=b.title.text),g(b.title||E)&&(b.title=E))),"position"in a&&f(a.position)&&(a.position={my:a.position,at:a.position}),"show"in a&&f(a.show)&&(a.show=a.show.jquery?{target:a.show}:a.show===D?{ready:D}:{event:a.show}),"hide"in a&&f(a.hide)&&(a.hide=a.hide.jquery?{target:a.hide}:{event:a.hide}),"style"in a&&f(a.style)&&(a.style={classes:a.style}),d.each(R,function(){this.sanitize&&this.sanitize(a)}),a)}function i(a,b){for(var c,d=0,e=a,f=b.split(".");e=e[f[d++]];)d<f.length&&(c=e);return[c||a,f.pop()]}function j(a,b){var c,d,e;for(c in this.checks)for(d in this.checks[c])(e=new RegExp(d,"i").exec(a))&&(b.push(e),("builtin"===c||this.plugins[c])&&this.checks[c][d].apply(this.plugins[c]||this,b))}function k(a){return V.concat("").join(a?"-"+a+" ":" ")}function l(a,b){return b>0?setTimeout(d.proxy(a,this),b):void a.call(this)}function m(a){this.tooltip.hasClass(ab)||(clearTimeout(this.timers.show),clearTimeout(this.timers.hide),this.timers.show=l.call(this,function(){this.toggle(D,a)},this.options.show.delay))}function n(a){if(!this.tooltip.hasClass(ab)&&!this.destroyed){var b=d(a.relatedTarget),c=b.closest(W)[0]===this.tooltip[0],e=b[0]===this.options.show.target[0];if(clearTimeout(this.timers.show),clearTimeout(this.timers.hide),this!==b[0]&&"mouse"===this.options.position.target&&c||this.options.hide.fixed&&/mouse(out|leave|move)/.test(a.type)&&(c||e))try{a.preventDefault(),a.stopImmediatePropagation()}catch(f){}else this.timers.hide=l.call(this,function(){this.toggle(E,a)},this.options.hide.delay,this)}}function o(a){!this.tooltip.hasClass(ab)&&this.options.hide.inactive&&(clearTimeout(this.timers.inactive),this.timers.inactive=l.call(this,function(){this.hide(a)},this.options.hide.inactive))}function p(a){this.rendered&&this.tooltip[0].offsetWidth>0&&this.reposition(a)}function q(a,c,e){d(b.body).delegate(a,(c.split?c:c.join("."+S+" "))+"."+S,function(){var a=y.api[d.attr(this,U)];a&&!a.disabled&&e.apply(a,arguments)})}function r(a,c,f){var g,i,j,k,l,m=d(b.body),n=a[0]===b?m:a,o=a.metadata?a.metadata(f.metadata):F,p="html5"===f.metadata.type&&o?o[f.metadata.name]:F,q=a.data(f.metadata.name||"qtipopts");try{q="string"==typeof q?d.parseJSON(q):q}catch(r){}if(k=d.extend(D,{},y.defaults,f,"object"==typeof q?h(q):F,h(p||o)),i=k.position,k.id=c,"boolean"==typeof k.content.text){if(j=a.attr(k.content.attr),k.content.attr===E||!j)return E;k.content.text=j}if(i.container.length||(i.container=m),i.target===E&&(i.target=n),k.show.target===E&&(k.show.target=n),k.show.solo===D&&(k.show.solo=i.container.closest("body")),k.hide.target===E&&(k.hide.target=n),k.position.viewport===D&&(k.position.viewport=i.container),i.container=i.container.eq(0),i.at=new A(i.at,D),i.my=new A(i.my),a.data(S))if(k.overwrite)a.qtip("destroy",!0);else if(k.overwrite===E)return E;return a.attr(T,c),k.suppress&&(l=a.attr("title"))&&a.removeAttr("title").attr(cb,l).attr("title",""),g=new e(a,k,c,!!j),a.data(S,g),g}function s(a){return a.charAt(0).toUpperCase()+a.slice(1)}function t(a,b){var d,e,f=b.charAt(0).toUpperCase()+b.slice(1),g=(b+" "+rb.join(f+" ")+f).split(" "),h=0;if(qb[b])return a.css(qb[b]);for(;d=g[h++];)if((e=a.css(d))!==c)return qb[b]=d,e}function u(a,b){return Math.ceil(parseFloat(t(a,b)))}function v(a,b){this._ns="tip",this.options=b,this.offset=b.offset,this.size=[b.width,b.height],this.init(this.qtip=a)}function w(a,b){this.options=b,this._ns="-modal",this.init(this.qtip=a)}function x(a){this._ns="ie6",this.init(this.qtip=a)}var y,z,A,B,C,D=!0,E=!1,F=null,G="x",H="y",I="width",J="height",K="top",L="left",M="bottom",N="right",O="center",P="flipinvert",Q="shift",R={},S="qtip",T="data-hasqtip",U="data-qtip-id",V=["ui-widget","ui-tooltip"],W="."+S,X="click dblclick mousedown mouseup mousemove mouseleave mouseenter".split(" "),Y=S+"-fixed",Z=S+"-default",$=S+"-focus",_=S+"-hover",ab=S+"-disabled",bb="_replacedByqTip",cb="oldtitle",db={ie:function(){for(var a=4,c=b.createElement("div");(c.innerHTML="<!--[if gt IE "+a+"]><i></i><![endif]-->")&&c.getElementsByTagName("i")[0];a+=1);return a>4?a:0/0}(),iOS:parseFloat((""+(/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent)||[0,""])[1]).replace("undefined","3_2").replace("_",".").replace("_",""))||E};z=e.prototype,z._when=function(a){return d.when.apply(d,a)},z.render=function(a){if(this.rendered||this.destroyed)return this;var b,c=this,e=this.options,f=this.cache,g=this.elements,h=e.content.text,i=e.content.title,j=e.content.button,k=e.position,l=("."+this._id+" ",[]);return d.attr(this.target[0],"aria-describedby",this._id),f.posClass=this._createPosClass((this.position={my:k.my,at:k.at}).my),this.tooltip=g.tooltip=b=d("<div/>",{id:this._id,"class":[S,Z,e.style.classes,f.posClass].join(" "),width:e.style.width||"",height:e.style.height||"",tracking:"mouse"===k.target&&k.adjust.mouse,role:"alert","aria-live":"polite","aria-atomic":E,"aria-describedby":this._id+"-content","aria-hidden":D}).toggleClass(ab,this.disabled).attr(U,this.id).data(S,this).appendTo(k.container).append(g.content=d("<div />",{"class":S+"-content",id:this._id+"-content","aria-atomic":D})),this.rendered=-1,this.positioning=D,i&&(this._createTitle(),d.isFunction(i)||l.push(this._updateTitle(i,E))),j&&this._createButton(),d.isFunction(h)||l.push(this._updateContent(h,E)),this.rendered=D,this._setWidget(),d.each(R,function(a){var b;"render"===this.initialize&&(b=this(c))&&(c.plugins[a]=b)}),this._unassignEvents(),this._assignEvents(),this._when(l).then(function(){c._trigger("render"),c.positioning=E,c.hiddenDuringWait||!e.show.ready&&!a||c.toggle(D,f.event,E),c.hiddenDuringWait=E}),y.api[this.id]=this,this},z.destroy=function(a){function b(){if(!this.destroyed){this.destroyed=D;var a,b=this.target,c=b.attr(cb);this.rendered&&this.tooltip.stop(1,0).find("*").remove().end().remove(),d.each(this.plugins,function(){this.destroy&&this.destroy()});for(a in this.timers)clearTimeout(this.timers[a]);b.removeData(S).removeAttr(U).removeAttr(T).removeAttr("aria-describedby"),this.options.suppress&&c&&b.attr("title",c).removeAttr(cb),this._unassignEvents(),this.options=this.elements=this.cache=this.timers=this.plugins=this.mouse=F,delete y.api[this.id]}}return this.destroyed?this.target:(a===D&&"hide"!==this.triggering||!this.rendered?b.call(this):(this.tooltip.one("tooltiphidden",d.proxy(b,this)),!this.triggering&&this.hide()),this.target)},B=z.checks={builtin:{"^id$":function(a,b,c,e){var f=c===D?y.nextid:c,g=S+"-"+f;f!==E&&f.length>0&&!d("#"+g).length?(this._id=g,this.rendered&&(this.tooltip[0].id=this._id,this.elements.content[0].id=this._id+"-content",this.elements.title[0].id=this._id+"-title")):a[b]=e},"^prerender":function(a,b,c){c&&!this.rendered&&this.render(this.options.show.ready)},"^content.text$":function(a,b,c){this._updateContent(c)},"^content.attr$":function(a,b,c,d){this.options.content.text===this.target.attr(d)&&this._updateContent(this.target.attr(c))},"^content.title$":function(a,b,c){return c?(c&&!this.elements.title&&this._createTitle(),void this._updateTitle(c)):this._removeTitle()},"^content.button$":function(a,b,c){this._updateButton(c)},"^content.title.(text|button)$":function(a,b,c){this.set("content."+b,c)},"^position.(my|at)$":function(a,b,c){"string"==typeof c&&(this.position[b]=a[b]=new A(c,"at"===b))},"^position.container$":function(a,b,c){this.rendered&&this.tooltip.appendTo(c)},"^show.ready$":function(a,b,c){c&&(!this.rendered&&this.render(D)||this.toggle(D))},"^style.classes$":function(a,b,c,d){this.rendered&&this.tooltip.removeClass(d).addClass(c)},"^style.(width|height)":function(a,b,c){this.rendered&&this.tooltip.css(b,c)},"^style.widget|content.title":function(){this.rendered&&this._setWidget()},"^style.def":function(a,b,c){this.rendered&&this.tooltip.toggleClass(Z,!!c)},"^events.(render|show|move|hide|focus|blur)$":function(a,b,c){this.rendered&&this.tooltip[(d.isFunction(c)?"":"un")+"bind"]("tooltip"+b,c)},"^(show|hide|position).(event|target|fixed|inactive|leave|distance|viewport|adjust)":function(){if(this.rendered){var a=this.options.position;this.tooltip.attr("tracking","mouse"===a.target&&a.adjust.mouse),this._unassignEvents(),this._assignEvents()}}}},z.get=function(a){if(this.destroyed)return this;var b=i(this.options,a.toLowerCase()),c=b[0][b[1]];return c.precedance?c.string():c};var eb=/^position\.(my|at|adjust|target|container|viewport)|style|content|show\.ready/i,fb=/^prerender|show\.ready/i;z.set=function(a,b){if(this.destroyed)return this;{var c,e=this.rendered,f=E,g=this.options;this.checks}return"string"==typeof a?(c=a,a={},a[c]=b):a=d.extend({},a),d.each(a,function(b,c){if(e&&fb.test(b))return void delete a[b];var h,j=i(g,b.toLowerCase());h=j[0][j[1]],j[0][j[1]]=c&&c.nodeType?d(c):c,f=eb.test(b)||f,a[b]=[j[0],j[1],c,h]}),h(g),this.positioning=D,d.each(a,d.proxy(j,this)),this.positioning=E,this.rendered&&this.tooltip[0].offsetWidth>0&&f&&this.reposition("mouse"===g.position.target?F:this.cache.event),this},z._update=function(a,b){var c=this,e=this.cache;return this.rendered&&a?(d.isFunction(a)&&(a=a.call(this.elements.target,e.event,this)||""),d.isFunction(a.then)?(e.waiting=D,a.then(function(a){return e.waiting=E,c._update(a,b)},F,function(a){return c._update(a,b)})):a===E||!a&&""!==a?E:(a.jquery&&a.length>0?b.empty().append(a.css({display:"block",visibility:"visible"})):b.html(a),this._waitForContent(b).then(function(a){c.rendered&&c.tooltip[0].offsetWidth>0&&c.reposition(e.event,!a.length)}))):E},z._waitForContent=function(a){var b=this.cache;return b.waiting=D,(d.fn.imagesLoaded?a.imagesLoaded():d.Deferred().resolve([])).done(function(){b.waiting=E}).promise()},z._updateContent=function(a,b){this._update(a,this.elements.content,b)},z._updateTitle=function(a,b){this._update(a,this.elements.title,b)===E&&this._removeTitle(E)},z._createTitle=function(){var a=this.elements,b=this._id+"-title";a.titlebar&&this._removeTitle(),a.titlebar=d("<div />",{"class":S+"-titlebar "+(this.options.style.widget?k("header"):"")}).append(a.title=d("<div />",{id:b,"class":S+"-title","aria-atomic":D})).insertBefore(a.content).delegate(".qtip-close","mousedown keydown mouseup keyup mouseout",function(a){d(this).toggleClass("ui-state-active ui-state-focus","down"===a.type.substr(-4))}).delegate(".qtip-close","mouseover mouseout",function(a){d(this).toggleClass("ui-state-hover","mouseover"===a.type)}),this.options.content.button&&this._createButton()},z._removeTitle=function(a){var b=this.elements;b.title&&(b.titlebar.remove(),b.titlebar=b.title=b.button=F,a!==E&&this.reposition())},z._createPosClass=function(a){return S+"-pos-"+(a||this.options.position.my).abbrev()},z.reposition=function(c,e){if(!this.rendered||this.positioning||this.destroyed)return this;this.positioning=D;var f,g,h,i,j=this.cache,k=this.tooltip,l=this.options.position,m=l.target,n=l.my,o=l.at,p=l.viewport,q=l.container,r=l.adjust,s=r.method.split(" "),t=k.outerWidth(E),u=k.outerHeight(E),v=0,w=0,x=k.css("position"),y={left:0,top:0},z=k[0].offsetWidth>0,A=c&&"scroll"===c.type,B=d(a),C=q[0].ownerDocument,F=this.mouse;if(d.isArray(m)&&2===m.length)o={x:L,y:K},y={left:m[0],top:m[1]};else if("mouse"===m)o={x:L,y:K},(!r.mouse||this.options.hide.distance)&&j.origin&&j.origin.pageX?c=j.origin:!c||c&&("resize"===c.type||"scroll"===c.type)?c=j.event:F&&F.pageX&&(c=F),"static"!==x&&(y=q.offset()),C.body.offsetWidth!==(a.innerWidth||C.documentElement.clientWidth)&&(g=d(b.body).offset()),y={left:c.pageX-y.left+(g&&g.left||0),top:c.pageY-y.top+(g&&g.top||0)},r.mouse&&A&&F&&(y.left-=(F.scrollX||0)-B.scrollLeft(),y.top-=(F.scrollY||0)-B.scrollTop());else{if("event"===m?c&&c.target&&"scroll"!==c.type&&"resize"!==c.type?j.target=d(c.target):c.target||(j.target=this.elements.target):"event"!==m&&(j.target=d(m.jquery?m:this.elements.target)),m=j.target,m=d(m).eq(0),0===m.length)return this;m[0]===b||m[0]===a?(v=db.iOS?a.innerWidth:m.width(),w=db.iOS?a.innerHeight:m.height(),m[0]===a&&(y={top:(p||m).scrollTop(),left:(p||m).scrollLeft()})):R.imagemap&&m.is("area")?f=R.imagemap(this,m,o,R.viewport?s:E):R.svg&&m&&m[0].ownerSVGElement?f=R.svg(this,m,o,R.viewport?s:E):(v=m.outerWidth(E),w=m.outerHeight(E),y=m.offset()),f&&(v=f.width,w=f.height,g=f.offset,y=f.position),y=this.reposition.offset(m,y,q),(db.iOS>3.1&&db.iOS<4.1||db.iOS>=4.3&&db.iOS<4.33||!db.iOS&&"fixed"===x)&&(y.left-=B.scrollLeft(),y.top-=B.scrollTop()),(!f||f&&f.adjustable!==E)&&(y.left+=o.x===N?v:o.x===O?v/2:0,y.top+=o.y===M?w:o.y===O?w/2:0)}return y.left+=r.x+(n.x===N?-t:n.x===O?-t/2:0),y.top+=r.y+(n.y===M?-u:n.y===O?-u/2:0),R.viewport?(h=y.adjusted=R.viewport(this,y,l,v,w,t,u),g&&h.left&&(y.left+=g.left),g&&h.top&&(y.top+=g.top),h.my&&(this.position.my=h.my)):y.adjusted={left:0,top:0},j.posClass!==(i=this._createPosClass(this.position.my))&&k.removeClass(j.posClass).addClass(j.posClass=i),this._trigger("move",[y,p.elem||p],c)?(delete y.adjusted,e===E||!z||isNaN(y.left)||isNaN(y.top)||"mouse"===m||!d.isFunction(l.effect)?k.css(y):d.isFunction(l.effect)&&(l.effect.call(k,this,d.extend({},y)),k.queue(function(a){d(this).css({opacity:"",height:""}),db.ie&&this.style.removeAttribute("filter"),a()})),this.positioning=E,this):this},z.reposition.offset=function(a,c,e){function f(a,b){c.left+=b*a.scrollLeft(),c.top+=b*a.scrollTop()}if(!e[0])return c;var g,h,i,j,k=d(a[0].ownerDocument),l=!!db.ie&&"CSS1Compat"!==b.compatMode,m=e[0];do"static"!==(h=d.css(m,"position"))&&("fixed"===h?(i=m.getBoundingClientRect(),f(k,-1)):(i=d(m).position(),i.left+=parseFloat(d.css(m,"borderLeftWidth"))||0,i.top+=parseFloat(d.css(m,"borderTopWidth"))||0),c.left-=i.left+(parseFloat(d.css(m,"marginLeft"))||0),c.top-=i.top+(parseFloat(d.css(m,"marginTop"))||0),g||"hidden"===(j=d.css(m,"overflow"))||"visible"===j||(g=d(m)));while(m=m.offsetParent);return g&&(g[0]!==k[0]||l)&&f(g,1),c};var gb=(A=z.reposition.Corner=function(a,b){a=(""+a).replace(/([A-Z])/," $1").replace(/middle/gi,O).toLowerCase(),this.x=(a.match(/left|right/i)||a.match(/center/)||["inherit"])[0].toLowerCase(),this.y=(a.match(/top|bottom|center/i)||["inherit"])[0].toLowerCase(),this.forceY=!!b;var c=a.charAt(0);this.precedance="t"===c||"b"===c?H:G}).prototype;gb.invert=function(a,b){this[a]=this[a]===L?N:this[a]===N?L:b||this[a]},gb.string=function(a){var b=this.x,c=this.y,d=b!==c?"center"===b||"center"!==c&&(this.precedance===H||this.forceY)?[c,b]:[b,c]:[b];return a!==!1?d.join(" "):d},gb.abbrev=function(){var a=this.string(!1);return a[0].charAt(0)+(a[1]&&a[1].charAt(0)||"")},gb.clone=function(){return new A(this.string(),this.forceY)},z.toggle=function(a,c){var e=this.cache,f=this.options,g=this.tooltip;if(c){if(/over|enter/.test(c.type)&&e.event&&/out|leave/.test(e.event.type)&&f.show.target.add(c.target).length===f.show.target.length&&g.has(c.relatedTarget).length)return this;e.event=d.event.fix(c)}if(this.waiting&&!a&&(this.hiddenDuringWait=D),!this.rendered)return a?this.render(1):this;if(this.destroyed||this.disabled)return this;var h,i,j,k=a?"show":"hide",l=this.options[k],m=(this.options[a?"hide":"show"],this.options.position),n=this.options.content,o=this.tooltip.css("width"),p=this.tooltip.is(":visible"),q=a||1===l.target.length,r=!c||l.target.length<2||e.target[0]===c.target;return(typeof a).search("boolean|number")&&(a=!p),h=!g.is(":animated")&&p===a&&r,i=h?F:!!this._trigger(k,[90]),this.destroyed?this:(i!==E&&a&&this.focus(c),!i||h?this:(d.attr(g[0],"aria-hidden",!a),a?(this.mouse&&(e.origin=d.event.fix(this.mouse)),d.isFunction(n.text)&&this._updateContent(n.text,E),d.isFunction(n.title)&&this._updateTitle(n.title,E),!C&&"mouse"===m.target&&m.adjust.mouse&&(d(b).bind("mousemove."+S,this._storeMouse),C=D),o||g.css("width",g.outerWidth(E)),this.reposition(c,arguments[2]),o||g.css("width",""),l.solo&&("string"==typeof l.solo?d(l.solo):d(W,l.solo)).not(g).not(l.target).qtip("hide",d.Event("tooltipsolo"))):(clearTimeout(this.timers.show),delete e.origin,C&&!d(W+'[tracking="true"]:visible',l.solo).not(g).length&&(d(b).unbind("mousemove."+S),C=E),this.blur(c)),j=d.proxy(function(){a?(db.ie&&g[0].style.removeAttribute("filter"),g.css("overflow",""),"string"==typeof l.autofocus&&d(this.options.show.autofocus,g).focus(),this.options.show.target.trigger("qtip-"+this.id+"-inactive")):g.css({display:"",visibility:"",opacity:"",left:"",top:""}),this._trigger(a?"visible":"hidden")},this),l.effect===E||q===E?(g[k](),j()):d.isFunction(l.effect)?(g.stop(1,1),l.effect.call(g,this),g.queue("fx",function(a){j(),a()})):g.fadeTo(90,a?1:0,j),a&&l.target.trigger("qtip-"+this.id+"-inactive"),this))},z.show=function(a){return this.toggle(D,a)},z.hide=function(a){return this.toggle(E,a)},z.focus=function(a){if(!this.rendered||this.destroyed)return this;var b=d(W),c=this.tooltip,e=parseInt(c[0].style.zIndex,10),f=y.zindex+b.length;return c.hasClass($)||this._trigger("focus",[f],a)&&(e!==f&&(b.each(function(){this.style.zIndex>e&&(this.style.zIndex=this.style.zIndex-1)}),b.filter("."+$).qtip("blur",a)),c.addClass($)[0].style.zIndex=f),this},z.blur=function(a){return!this.rendered||this.destroyed?this:(this.tooltip.removeClass($),this._trigger("blur",[this.tooltip.css("zIndex")],a),this)},z.disable=function(a){return this.destroyed?this:("toggle"===a?a=!(this.rendered?this.tooltip.hasClass(ab):this.disabled):"boolean"!=typeof a&&(a=D),this.rendered&&this.tooltip.toggleClass(ab,a).attr("aria-disabled",a),this.disabled=!!a,this)},z.enable=function(){return this.disable(E)},z._createButton=function(){var a=this,b=this.elements,c=b.tooltip,e=this.options.content.button,f="string"==typeof e,g=f?e:"Close tooltip";b.button&&b.button.remove(),b.button=e.jquery?e:d("<a />",{"class":"qtip-close "+(this.options.style.widget?"":S+"-icon"),title:g,"aria-label":g}).prepend(d("<span />",{"class":"ui-icon ui-icon-close",html:"&times;"})),b.button.appendTo(b.titlebar||c).attr("role","button").click(function(b){return c.hasClass(ab)||a.hide(b),E})},z._updateButton=function(a){if(!this.rendered)return E;var b=this.elements.button;a?this._createButton():b.remove()},z._setWidget=function(){var a=this.options.style.widget,b=this.elements,c=b.tooltip,d=c.hasClass(ab);c.removeClass(ab),ab=a?"ui-state-disabled":"qtip-disabled",c.toggleClass(ab,d),c.toggleClass("ui-helper-reset "+k(),a).toggleClass(Z,this.options.style.def&&!a),b.content&&b.content.toggleClass(k("content"),a),b.titlebar&&b.titlebar.toggleClass(k("header"),a),b.button&&b.button.toggleClass(S+"-icon",!a)},z._storeMouse=function(a){return(this.mouse=d.event.fix(a)).type="mousemove",this},z._bind=function(a,b,c,e,f){if(a&&c&&b.length){var g="."+this._id+(e?"-"+e:"");return d(a).bind((b.split?b:b.join(g+" "))+g,d.proxy(c,f||this)),this}},z._unbind=function(a,b){return a&&d(a).unbind("."+this._id+(b?"-"+b:"")),this},z._trigger=function(a,b,c){var e=d.Event("tooltip"+a);return e.originalEvent=c&&d.extend({},c)||this.cache.event||F,this.triggering=a,this.tooltip.trigger(e,[this].concat(b||[])),this.triggering=E,!e.isDefaultPrevented()},z._bindEvents=function(a,b,c,e,f,g){var h=c.filter(e).add(e.filter(c)),i=[];h.length&&(d.each(b,function(b,c){var e=d.inArray(c,a);e>-1&&i.push(a.splice(e,1)[0])}),i.length&&(this._bind(h,i,function(a){var b=this.rendered?this.tooltip[0].offsetWidth>0:!1;(b?g:f).call(this,a)}),c=c.not(h),e=e.not(h))),this._bind(c,a,f),this._bind(e,b,g)},z._assignInitialEvents=function(a){function b(a){return this.disabled||this.destroyed?E:(this.cache.event=a&&d.event.fix(a),this.cache.target=a&&d(a.target),clearTimeout(this.timers.show),void(this.timers.show=l.call(this,function(){this.render("object"==typeof a||c.show.ready)},c.prerender?0:c.show.delay)))}var c=this.options,e=c.show.target,f=c.hide.target,g=c.show.event?d.trim(""+c.show.event).split(" "):[],h=c.hide.event?d.trim(""+c.hide.event).split(" "):[];this._bind(this.elements.target,["remove","removeqtip"],function(){this.destroy(!0)},"destroy"),/mouse(over|enter)/i.test(c.show.event)&&!/mouse(out|leave)/i.test(c.hide.event)&&h.push("mouseleave"),this._bind(e,"mousemove",function(a){this._storeMouse(a),this.cache.onTarget=D}),this._bindEvents(g,h,e,f,b,function(){return this.timers?void clearTimeout(this.timers.show):E}),(c.show.ready||c.prerender)&&b.call(this,a)},z._assignEvents=function(){var c=this,e=this.options,f=e.position,g=this.tooltip,h=e.show.target,i=e.hide.target,j=f.container,k=f.viewport,l=d(b),q=(d(b.body),d(a)),r=e.show.event?d.trim(""+e.show.event).split(" "):[],s=e.hide.event?d.trim(""+e.hide.event).split(" "):[];d.each(e.events,function(a,b){c._bind(g,"toggle"===a?["tooltipshow","tooltiphide"]:["tooltip"+a],b,null,g)}),/mouse(out|leave)/i.test(e.hide.event)&&"window"===e.hide.leave&&this._bind(l,["mouseout","blur"],function(a){/select|option/.test(a.target.nodeName)||a.relatedTarget||this.hide(a)}),e.hide.fixed?i=i.add(g.addClass(Y)):/mouse(over|enter)/i.test(e.show.event)&&this._bind(i,"mouseleave",function(){clearTimeout(this.timers.show)}),(""+e.hide.event).indexOf("unfocus")>-1&&this._bind(j.closest("html"),["mousedown","touchstart"],function(a){var b=d(a.target),c=this.rendered&&!this.tooltip.hasClass(ab)&&this.tooltip[0].offsetWidth>0,e=b.parents(W).filter(this.tooltip[0]).length>0;b[0]===this.target[0]||b[0]===this.tooltip[0]||e||this.target.has(b[0]).length||!c||this.hide(a)}),"number"==typeof e.hide.inactive&&(this._bind(h,"qtip-"+this.id+"-inactive",o,"inactive"),this._bind(i.add(g),y.inactiveEvents,o)),this._bindEvents(r,s,h,i,m,n),this._bind(h.add(g),"mousemove",function(a){if("number"==typeof e.hide.distance){var b=this.cache.origin||{},c=this.options.hide.distance,d=Math.abs;(d(a.pageX-b.pageX)>=c||d(a.pageY-b.pageY)>=c)&&this.hide(a)}this._storeMouse(a)}),"mouse"===f.target&&f.adjust.mouse&&(e.hide.event&&this._bind(h,["mouseenter","mouseleave"],function(a){return this.cache?void(this.cache.onTarget="mouseenter"===a.type):E}),this._bind(l,"mousemove",function(a){this.rendered&&this.cache.onTarget&&!this.tooltip.hasClass(ab)&&this.tooltip[0].offsetWidth>0&&this.reposition(a)})),(f.adjust.resize||k.length)&&this._bind(d.event.special.resize?k:q,"resize",p),f.adjust.scroll&&this._bind(q.add(f.container),"scroll",p)},z._unassignEvents=function(){var c=this.options,e=c.show.target,f=c.hide.target,g=d.grep([this.elements.target[0],this.rendered&&this.tooltip[0],c.position.container[0],c.position.viewport[0],c.position.container.closest("html")[0],a,b],function(a){return"object"==typeof a});e&&e.toArray&&(g=g.concat(e.toArray())),f&&f.toArray&&(g=g.concat(f.toArray())),this._unbind(g)._unbind(g,"destroy")._unbind(g,"inactive")},d(function(){q(W,["mouseenter","mouseleave"],function(a){var b="mouseenter"===a.type,c=d(a.currentTarget),e=d(a.relatedTarget||a.target),f=this.options;b?(this.focus(a),c.hasClass(Y)&&!c.hasClass(ab)&&clearTimeout(this.timers.hide)):"mouse"===f.position.target&&f.position.adjust.mouse&&f.hide.event&&f.show.target&&!e.closest(f.show.target[0]).length&&this.hide(a),c.toggleClass(_,b)}),q("["+U+"]",X,o)}),y=d.fn.qtip=function(a,b,e){var f=(""+a).toLowerCase(),g=F,i=d.makeArray(arguments).slice(1),j=i[i.length-1],k=this[0]?d.data(this[0],S):F;return!arguments.length&&k||"api"===f?k:"string"==typeof a?(this.each(function(){var a=d.data(this,S);if(!a)return D;if(j&&j.timeStamp&&(a.cache.event=j),!b||"option"!==f&&"options"!==f)a[f]&&a[f].apply(a,i);else{if(e===c&&!d.isPlainObject(b))return g=a.get(b),E;a.set(b,e)}}),g!==F?g:this):"object"!=typeof a&&arguments.length?void 0:(k=h(d.extend(D,{},a)),this.each(function(a){var b,c;return c=d.isArray(k.id)?k.id[a]:k.id,c=!c||c===E||c.length<1||y.api[c]?y.nextid++:c,b=r(d(this),c,k),b===E?D:(y.api[c]=b,d.each(R,function(){"initialize"===this.initialize&&this(b)}),void b._assignInitialEvents(j))}))},d.qtip=e,y.api={},d.each({attr:function(a,b){if(this.length){var c=this[0],e="title",f=d.data(c,"qtip");if(a===e&&f&&"object"==typeof f&&f.options.suppress)return arguments.length<2?d.attr(c,cb):(f&&f.options.content.attr===e&&f.cache.attr&&f.set("content.text",b),this.attr(cb,b))}return d.fn["attr"+bb].apply(this,arguments)},clone:function(a){var b=(d([]),d.fn["clone"+bb].apply(this,arguments));return a||b.filter("["+cb+"]").attr("title",function(){return d.attr(this,cb)}).removeAttr(cb),b}},function(a,b){if(!b||d.fn[a+bb])return D;var c=d.fn[a+bb]=d.fn[a];d.fn[a]=function(){return b.apply(this,arguments)||c.apply(this,arguments)}}),d.ui||(d["cleanData"+bb]=d.cleanData,d.cleanData=function(a){for(var b,c=0;(b=d(a[c])).length;c++)if(b.attr(T))try{b.triggerHandler("removeqtip")}catch(e){}d["cleanData"+bb].apply(this,arguments)}),y.version="2.2.1",y.nextid=0,y.inactiveEvents=X,y.zindex=15e3,y.defaults={prerender:E,id:E,overwrite:D,suppress:D,content:{text:D,attr:"title",title:E,button:E},position:{my:"top left",at:"bottom right",target:E,container:E,viewport:E,adjust:{x:0,y:0,mouse:D,scroll:D,resize:D,method:"flipinvert flipinvert"},effect:function(a,b){d(this).animate(b,{duration:200,queue:E})}},show:{target:E,event:"mouseenter",effect:D,delay:90,solo:E,ready:E,autofocus:E},hide:{target:E,event:"mouseleave",effect:D,delay:0,fixed:E,inactive:E,leave:"window",distance:E},style:{classes:"",widget:E,width:E,height:E,def:D},events:{render:F,move:F,show:F,hide:F,toggle:F,visible:F,hidden:F,focus:F,blur:F}};var hb,ib="margin",jb="border",kb="color",lb="background-color",mb="transparent",nb=" !important",ob=!!b.createElement("canvas").getContext,pb=/rgba?\(0, 0, 0(, 0)?\)|transparent|#123456/i,qb={},rb=["Webkit","O","Moz","ms"];if(ob)var sb=a.devicePixelRatio||1,tb=function(){var a=b.createElement("canvas").getContext("2d");return a.backingStorePixelRatio||a.webkitBackingStorePixelRatio||a.mozBackingStorePixelRatio||a.msBackingStorePixelRatio||a.oBackingStorePixelRatio||1}(),ub=sb/tb;else var vb=function(a,b,c){return"<qtipvml:"+a+' xmlns="urn:schemas-microsoft.com:vml" class="qtip-vml" '+(b||"")+' style="behavior: url(#default#VML); '+(c||"")+'" />'};d.extend(v.prototype,{init:function(a){var b,c;c=this.element=a.elements.tip=d("<div />",{"class":S+"-tip"}).prependTo(a.tooltip),ob?(b=d("<canvas />").appendTo(this.element)[0].getContext("2d"),b.lineJoin="miter",b.miterLimit=1e5,b.save()):(b=vb("shape",'coordorigin="0,0"',"position:absolute;"),this.element.html(b+b),a._bind(d("*",c).add(c),["click","mousedown"],function(a){a.stopPropagation()},this._ns)),a._bind(a.tooltip,"tooltipmove",this.reposition,this._ns,this),this.create()},_swapDimensions:function(){this.size[0]=this.options.height,this.size[1]=this.options.width},_resetDimensions:function(){this.size[0]=this.options.width,this.size[1]=this.options.height},_useTitle:function(a){var b=this.qtip.elements.titlebar;return b&&(a.y===K||a.y===O&&this.element.position().top+this.size[1]/2+this.options.offset<b.outerHeight(D))},_parseCorner:function(a){var b=this.qtip.options.position.my;return a===E||b===E?a=E:a===D?a=new A(b.string()):a.string||(a=new A(a),a.fixed=D),a},_parseWidth:function(a,b,c){var d=this.qtip.elements,e=jb+s(b)+"Width";return(c?u(c,e):u(d.content,e)||u(this._useTitle(a)&&d.titlebar||d.content,e)||u(d.tooltip,e))||0},_parseRadius:function(a){var b=this.qtip.elements,c=jb+s(a.y)+s(a.x)+"Radius";return db.ie<9?0:u(this._useTitle(a)&&b.titlebar||b.content,c)||u(b.tooltip,c)||0},_invalidColour:function(a,b,c){var d=a.css(b);return!d||c&&d===a.css(c)||pb.test(d)?E:d},_parseColours:function(a){var b=this.qtip.elements,c=this.element.css("cssText",""),e=jb+s(a[a.precedance])+s(kb),f=this._useTitle(a)&&b.titlebar||b.content,g=this._invalidColour,h=[];return h[0]=g(c,lb)||g(f,lb)||g(b.content,lb)||g(b.tooltip,lb)||c.css(lb),h[1]=g(c,e,kb)||g(f,e,kb)||g(b.content,e,kb)||g(b.tooltip,e,kb)||b.tooltip.css(e),d("*",c).add(c).css("cssText",lb+":"+mb+nb+";"+jb+":0"+nb+";"),h},_calculateSize:function(a){var b,c,d,e=a.precedance===H,f=this.options.width,g=this.options.height,h="c"===a.abbrev(),i=(e?f:g)*(h?.5:1),j=Math.pow,k=Math.round,l=Math.sqrt(j(i,2)+j(g,2)),m=[this.border/i*l,this.border/g*l];return m[2]=Math.sqrt(j(m[0],2)-j(this.border,2)),m[3]=Math.sqrt(j(m[1],2)-j(this.border,2)),b=l+m[2]+m[3]+(h?0:m[0]),c=b/l,d=[k(c*f),k(c*g)],e?d:d.reverse()},_calculateTip:function(a,b,c){c=c||1,b=b||this.size;var d=b[0]*c,e=b[1]*c,f=Math.ceil(d/2),g=Math.ceil(e/2),h={br:[0,0,d,e,d,0],bl:[0,0,d,0,0,e],tr:[0,e,d,0,d,e],tl:[0,0,0,e,d,e],tc:[0,e,f,0,d,e],bc:[0,0,d,0,f,e],rc:[0,0,d,g,0,e],lc:[d,0,d,e,0,g]};return h.lt=h.br,h.rt=h.bl,h.lb=h.tr,h.rb=h.tl,h[a.abbrev()]},_drawCoords:function(a,b){a.beginPath(),a.moveTo(b[0],b[1]),a.lineTo(b[2],b[3]),a.lineTo(b[4],b[5]),a.closePath()},create:function(){var a=this.corner=(ob||db.ie)&&this._parseCorner(this.options.corner);return(this.enabled=!!this.corner&&"c"!==this.corner.abbrev())&&(this.qtip.cache.corner=a.clone(),this.update()),this.element.toggle(this.enabled),this.corner},update:function(b,c){if(!this.enabled)return this;var e,f,g,h,i,j,k,l,m=this.qtip.elements,n=this.element,o=n.children(),p=this.options,q=this.size,r=p.mimic,s=Math.round;b||(b=this.qtip.cache.corner||this.corner),r===E?r=b:(r=new A(r),r.precedance=b.precedance,"inherit"===r.x?r.x=b.x:"inherit"===r.y?r.y=b.y:r.x===r.y&&(r[b.precedance]=b[b.precedance])),f=r.precedance,b.precedance===G?this._swapDimensions():this._resetDimensions(),e=this.color=this._parseColours(b),e[1]!==mb?(l=this.border=this._parseWidth(b,b[b.precedance]),p.border&&1>l&&!pb.test(e[1])&&(e[0]=e[1]),this.border=l=p.border!==D?p.border:l):this.border=l=0,k=this.size=this._calculateSize(b),n.css({width:k[0],height:k[1],lineHeight:k[1]+"px"}),j=b.precedance===H?[s(r.x===L?l:r.x===N?k[0]-q[0]-l:(k[0]-q[0])/2),s(r.y===K?k[1]-q[1]:0)]:[s(r.x===L?k[0]-q[0]:0),s(r.y===K?l:r.y===M?k[1]-q[1]-l:(k[1]-q[1])/2)],ob?(g=o[0].getContext("2d"),g.restore(),g.save(),g.clearRect(0,0,6e3,6e3),h=this._calculateTip(r,q,ub),i=this._calculateTip(r,this.size,ub),o.attr(I,k[0]*ub).attr(J,k[1]*ub),o.css(I,k[0]).css(J,k[1]),this._drawCoords(g,i),g.fillStyle=e[1],g.fill(),g.translate(j[0]*ub,j[1]*ub),this._drawCoords(g,h),g.fillStyle=e[0],g.fill()):(h=this._calculateTip(r),h="m"+h[0]+","+h[1]+" l"+h[2]+","+h[3]+" "+h[4]+","+h[5]+" xe",j[2]=l&&/^(r|b)/i.test(b.string())?8===db.ie?2:1:0,o.css({coordsize:k[0]+l+" "+(k[1]+l),antialias:""+(r.string().indexOf(O)>-1),left:j[0]-j[2]*Number(f===G),top:j[1]-j[2]*Number(f===H),width:k[0]+l,height:k[1]+l}).each(function(a){var b=d(this);b[b.prop?"prop":"attr"]({coordsize:k[0]+l+" "+(k[1]+l),path:h,fillcolor:e[0],filled:!!a,stroked:!a}).toggle(!(!l&&!a)),!a&&b.html(vb("stroke",'weight="'+2*l+'px" color="'+e[1]+'" miterlimit="1000" joinstyle="miter"'))})),a.opera&&setTimeout(function(){m.tip.css({display:"inline-block",visibility:"visible"})},1),c!==E&&this.calculate(b,k)},calculate:function(a,b){if(!this.enabled)return E;var c,e,f=this,g=this.qtip.elements,h=this.element,i=this.options.offset,j=(g.tooltip.hasClass("ui-widget"),{});return a=a||this.corner,c=a.precedance,b=b||this._calculateSize(a),e=[a.x,a.y],c===G&&e.reverse(),d.each(e,function(d,e){var h,k,l;
e===O?(h=c===H?L:K,j[h]="50%",j[ib+"-"+h]=-Math.round(b[c===H?0:1]/2)+i):(h=f._parseWidth(a,e,g.tooltip),k=f._parseWidth(a,e,g.content),l=f._parseRadius(a),j[e]=Math.max(-f.border,d?k:i+(l>h?l:-h)))}),j[a[c]]-=b[c===G?0:1],h.css({margin:"",top:"",bottom:"",left:"",right:""}).css(j),j},reposition:function(a,b,d){function e(a,b,c,d,e){a===Q&&j.precedance===b&&k[d]&&j[c]!==O?j.precedance=j.precedance===G?H:G:a!==Q&&k[d]&&(j[b]=j[b]===O?k[d]>0?d:e:j[b]===d?e:d)}function f(a,b,e){j[a]===O?p[ib+"-"+b]=o[a]=g[ib+"-"+b]-k[b]:(h=g[e]!==c?[k[b],-g[b]]:[-k[b],g[b]],(o[a]=Math.max(h[0],h[1]))>h[0]&&(d[b]-=k[b],o[b]=E),p[g[e]!==c?e:b]=o[a])}if(this.enabled){var g,h,i=b.cache,j=this.corner.clone(),k=d.adjusted,l=b.options.position.adjust.method.split(" "),m=l[0],n=l[1]||l[0],o={left:E,top:E,x:0,y:0},p={};this.corner.fixed!==D&&(e(m,G,H,L,N),e(n,H,G,K,M),(j.string()!==i.corner.string()||i.cornerTop!==k.top||i.cornerLeft!==k.left)&&this.update(j,E)),g=this.calculate(j),g.right!==c&&(g.left=-g.right),g.bottom!==c&&(g.top=-g.bottom),g.user=this.offset,(o.left=m===Q&&!!k.left)&&f(G,L,N),(o.top=n===Q&&!!k.top)&&f(H,K,M),this.element.css(p).toggle(!(o.x&&o.y||j.x===O&&o.y||j.y===O&&o.x)),d.left-=g.left.charAt?g.user:m!==Q||o.top||!o.left&&!o.top?g.left+this.border:0,d.top-=g.top.charAt?g.user:n!==Q||o.left||!o.left&&!o.top?g.top+this.border:0,i.cornerLeft=k.left,i.cornerTop=k.top,i.corner=j.clone()}},destroy:function(){this.qtip._unbind(this.qtip.tooltip,this._ns),this.qtip.elements.tip&&this.qtip.elements.tip.find("*").remove().end().remove()}}),hb=R.tip=function(a){return new v(a,a.options.style.tip)},hb.initialize="render",hb.sanitize=function(a){if(a.style&&"tip"in a.style){var b=a.style.tip;"object"!=typeof b&&(b=a.style.tip={corner:b}),/string|boolean/i.test(typeof b.corner)||(b.corner=D)}},B.tip={"^position.my|style.tip.(corner|mimic|border)$":function(){this.create(),this.qtip.reposition()},"^style.tip.(height|width)$":function(a){this.size=[a.width,a.height],this.update(),this.qtip.reposition()},"^content.title|style.(classes|widget)$":function(){this.update()}},d.extend(D,y.defaults,{style:{tip:{corner:D,mimic:E,width:6,height:6,border:D,offset:0}}});var wb,xb,yb="qtip-modal",zb="."+yb;xb=function(){function a(a){if(d.expr[":"].focusable)return d.expr[":"].focusable;var b,c,e,f=!isNaN(d.attr(a,"tabindex")),g=a.nodeName&&a.nodeName.toLowerCase();return"area"===g?(b=a.parentNode,c=b.name,a.href&&c&&"map"===b.nodeName.toLowerCase()?(e=d("img[usemap=#"+c+"]")[0],!!e&&e.is(":visible")):!1):/input|select|textarea|button|object/.test(g)?!a.disabled:"a"===g?a.href||f:f}function c(a){k.length<1&&a.length?a.not("body").blur():k.first().focus()}function e(a){if(i.is(":visible")){var b,e=d(a.target),h=f.tooltip,j=e.closest(W);b=j.length<1?E:parseInt(j[0].style.zIndex,10)>parseInt(h[0].style.zIndex,10),b||e.closest(W)[0]===h[0]||c(e),g=a.target===k[k.length-1]}}var f,g,h,i,j=this,k={};d.extend(j,{init:function(){return i=j.elem=d("<div />",{id:"qtip-overlay",html:"<div></div>",mousedown:function(){return E}}).hide(),d(b.body).bind("focusin"+zb,e),d(b).bind("keydown"+zb,function(a){f&&f.options.show.modal.escape&&27===a.keyCode&&f.hide(a)}),i.bind("click"+zb,function(a){f&&f.options.show.modal.blur&&f.hide(a)}),j},update:function(b){f=b,k=b.options.show.modal.stealfocus!==E?b.tooltip.find("*").filter(function(){return a(this)}):[]},toggle:function(a,e,g){var k=(d(b.body),a.tooltip),l=a.options.show.modal,m=l.effect,n=e?"show":"hide",o=i.is(":visible"),p=d(zb).filter(":visible:not(:animated)").not(k);return j.update(a),e&&l.stealfocus!==E&&c(d(":focus")),i.toggleClass("blurs",l.blur),e&&i.appendTo(b.body),i.is(":animated")&&o===e&&h!==E||!e&&p.length?j:(i.stop(D,E),d.isFunction(m)?m.call(i,e):m===E?i[n]():i.fadeTo(parseInt(g,10)||90,e?1:0,function(){e||i.hide()}),e||i.queue(function(a){i.css({left:"",top:""}),d(zb).length||i.detach(),a()}),h=e,f.destroyed&&(f=F),j)}}),j.init()},xb=new xb,d.extend(w.prototype,{init:function(a){var b=a.tooltip;return this.options.on?(a.elements.overlay=xb.elem,b.addClass(yb).css("z-index",y.modal_zindex+d(zb).length),a._bind(b,["tooltipshow","tooltiphide"],function(a,c,e){var f=a.originalEvent;if(a.target===b[0])if(f&&"tooltiphide"===a.type&&/mouse(leave|enter)/.test(f.type)&&d(f.relatedTarget).closest(xb.elem[0]).length)try{a.preventDefault()}catch(g){}else(!f||f&&"tooltipsolo"!==f.type)&&this.toggle(a,"tooltipshow"===a.type,e)},this._ns,this),a._bind(b,"tooltipfocus",function(a,c){if(!a.isDefaultPrevented()&&a.target===b[0]){var e=d(zb),f=y.modal_zindex+e.length,g=parseInt(b[0].style.zIndex,10);xb.elem[0].style.zIndex=f-1,e.each(function(){this.style.zIndex>g&&(this.style.zIndex-=1)}),e.filter("."+$).qtip("blur",a.originalEvent),b.addClass($)[0].style.zIndex=f,xb.update(c);try{a.preventDefault()}catch(h){}}},this._ns,this),void a._bind(b,"tooltiphide",function(a){a.target===b[0]&&d(zb).filter(":visible").not(b).last().qtip("focus",a)},this._ns,this)):this},toggle:function(a,b,c){return a&&a.isDefaultPrevented()?this:void xb.toggle(this.qtip,!!b,c)},destroy:function(){this.qtip.tooltip.removeClass(yb),this.qtip._unbind(this.qtip.tooltip,this._ns),xb.toggle(this.qtip,E),delete this.qtip.elements.overlay}}),wb=R.modal=function(a){return new w(a,a.options.show.modal)},wb.sanitize=function(a){a.show&&("object"!=typeof a.show.modal?a.show.modal={on:!!a.show.modal}:"undefined"==typeof a.show.modal.on&&(a.show.modal.on=D))},y.modal_zindex=y.zindex-200,wb.initialize="render",B.modal={"^show.modal.(on|blur)$":function(){this.destroy(),this.init(),this.qtip.elems.overlay.toggle(this.qtip.tooltip[0].offsetWidth>0)}},d.extend(D,y.defaults,{show:{modal:{on:E,effect:D,blur:D,stealfocus:D,escape:D}}}),R.viewport=function(c,d,e,f,g,h,i){function j(a,b,c,e,f,g,h,i,j){var k=d[f],s=u[a],t=v[a],w=c===Q,x=s===f?j:s===g?-j:-j/2,y=t===f?i:t===g?-i:-i/2,z=q[f]+r[f]-(n?0:m[f]),A=z-k,B=k+j-(h===I?o:p)-z,C=x-(u.precedance===a||s===u[b]?y:0)-(t===O?i/2:0);return w?(C=(s===f?1:-1)*x,d[f]+=A>0?A:B>0?-B:0,d[f]=Math.max(-m[f]+r[f],k-C,Math.min(Math.max(-m[f]+r[f]+(h===I?o:p),k+C),d[f],"center"===s?k-x:1e9))):(e*=c===P?2:0,A>0&&(s!==f||B>0)?(d[f]-=C+e,l.invert(a,f)):B>0&&(s!==g||A>0)&&(d[f]-=(s===O?-C:C)+e,l.invert(a,g)),d[f]<q&&-d[f]>B&&(d[f]=k,l=u.clone())),d[f]-k}var k,l,m,n,o,p,q,r,s=e.target,t=c.elements.tooltip,u=e.my,v=e.at,w=e.adjust,x=w.method.split(" "),y=x[0],z=x[1]||x[0],A=e.viewport,B=e.container,C=(c.cache,{left:0,top:0});return A.jquery&&s[0]!==a&&s[0]!==b.body&&"none"!==w.method?(m=B.offset()||C,n="static"===B.css("position"),k="fixed"===t.css("position"),o=A[0]===a?A.width():A.outerWidth(E),p=A[0]===a?A.height():A.outerHeight(E),q={left:k?0:A.scrollLeft(),top:k?0:A.scrollTop()},r=A.offset()||C,("shift"!==y||"shift"!==z)&&(l=u.clone()),C={left:"none"!==y?j(G,H,y,w.x,L,N,I,f,h):0,top:"none"!==z?j(H,G,z,w.y,K,M,J,g,i):0,my:l}):C},R.polys={polygon:function(a,b){var c,d,e,f={width:0,height:0,position:{top:1e10,right:0,bottom:0,left:1e10},adjustable:E},g=0,h=[],i=1,j=1,k=0,l=0;for(g=a.length;g--;)c=[parseInt(a[--g],10),parseInt(a[g+1],10)],c[0]>f.position.right&&(f.position.right=c[0]),c[0]<f.position.left&&(f.position.left=c[0]),c[1]>f.position.bottom&&(f.position.bottom=c[1]),c[1]<f.position.top&&(f.position.top=c[1]),h.push(c);if(d=f.width=Math.abs(f.position.right-f.position.left),e=f.height=Math.abs(f.position.bottom-f.position.top),"c"===b.abbrev())f.position={left:f.position.left+f.width/2,top:f.position.top+f.height/2};else{for(;d>0&&e>0&&i>0&&j>0;)for(d=Math.floor(d/2),e=Math.floor(e/2),b.x===L?i=d:b.x===N?i=f.width-d:i+=Math.floor(d/2),b.y===K?j=e:b.y===M?j=f.height-e:j+=Math.floor(e/2),g=h.length;g--&&!(h.length<2);)k=h[g][0]-f.position.left,l=h[g][1]-f.position.top,(b.x===L&&k>=i||b.x===N&&i>=k||b.x===O&&(i>k||k>f.width-i)||b.y===K&&l>=j||b.y===M&&j>=l||b.y===O&&(j>l||l>f.height-j))&&h.splice(g,1);f.position={left:h[0][0],top:h[0][1]}}return f},rect:function(a,b,c,d){return{width:Math.abs(c-a),height:Math.abs(d-b),position:{left:Math.min(a,c),top:Math.min(b,d)}}},_angles:{tc:1.5,tr:7/4,tl:5/4,bc:.5,br:.25,bl:.75,rc:2,lc:1,c:0},ellipse:function(a,b,c,d,e){var f=R.polys._angles[e.abbrev()],g=0===f?0:c*Math.cos(f*Math.PI),h=d*Math.sin(f*Math.PI);return{width:2*c-Math.abs(g),height:2*d-Math.abs(h),position:{left:a+g,top:b+h},adjustable:E}},circle:function(a,b,c,d){return R.polys.ellipse(a,b,c,c,d)}},R.svg=function(a,c,e){for(var f,g,h,i,j,k,l,m,n,o=(d(b),c[0]),p=d(o.ownerSVGElement),q=o.ownerDocument,r=(parseInt(c.css("stroke-width"),10)||0)/2;!o.getBBox;)o=o.parentNode;if(!o.getBBox||!o.parentNode)return E;switch(o.nodeName){case"ellipse":case"circle":m=R.polys.ellipse(o.cx.baseVal.value,o.cy.baseVal.value,(o.rx||o.r).baseVal.value+r,(o.ry||o.r).baseVal.value+r,e);break;case"line":case"polygon":case"polyline":for(l=o.points||[{x:o.x1.baseVal.value,y:o.y1.baseVal.value},{x:o.x2.baseVal.value,y:o.y2.baseVal.value}],m=[],k=-1,i=l.numberOfItems||l.length;++k<i;)j=l.getItem?l.getItem(k):l[k],m.push.apply(m,[j.x,j.y]);m=R.polys.polygon(m,e);break;default:m=o.getBBox(),m={width:m.width,height:m.height,position:{left:m.x,top:m.y}}}return n=m.position,p=p[0],p.createSVGPoint&&(g=o.getScreenCTM(),l=p.createSVGPoint(),l.x=n.left,l.y=n.top,h=l.matrixTransform(g),n.left=h.x,n.top=h.y),q!==b&&"mouse"!==a.position.target&&(f=d((q.defaultView||q.parentWindow).frameElement).offset(),f&&(n.left+=f.left,n.top+=f.top)),q=d(q),n.left+=q.scrollLeft(),n.top+=q.scrollTop(),m},R.imagemap=function(a,b,c){b.jquery||(b=d(b));var e,f,g,h,i,j=(b.attr("shape")||"rect").toLowerCase().replace("poly","polygon"),k=d('img[usemap="#'+b.parent("map").attr("name")+'"]'),l=d.trim(b.attr("coords")),m=l.replace(/,$/,"").split(",");if(!k.length)return E;if("polygon"===j)h=R.polys.polygon(m,c);else{if(!R.polys[j])return E;for(g=-1,i=m.length,f=[];++g<i;)f.push(parseInt(m[g],10));h=R.polys[j].apply(this,f.concat(c))}return e=k.offset(),e.left+=Math.ceil((k.outerWidth(E)-k.width())/2),e.top+=Math.ceil((k.outerHeight(E)-k.height())/2),h.position.left+=e.left,h.position.top+=e.top,h};var Ab,Bb='<iframe class="qtip-bgiframe" frameborder="0" tabindex="-1" src="javascript:\'\';"  style="display:block; position:absolute; z-index:-1; filter:alpha(opacity=0); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";"></iframe>';d.extend(x.prototype,{_scroll:function(){var b=this.qtip.elements.overlay;b&&(b[0].style.top=d(a).scrollTop()+"px")},init:function(c){var e=c.tooltip;d("select, object").length<1&&(this.bgiframe=c.elements.bgiframe=d(Bb).appendTo(e),c._bind(e,"tooltipmove",this.adjustBGIFrame,this._ns,this)),this.redrawContainer=d("<div/>",{id:S+"-rcontainer"}).appendTo(b.body),c.elements.overlay&&c.elements.overlay.addClass("qtipmodal-ie6fix")&&(c._bind(a,["scroll","resize"],this._scroll,this._ns,this),c._bind(e,["tooltipshow"],this._scroll,this._ns,this)),this.redraw()},adjustBGIFrame:function(){var a,b,c=this.qtip.tooltip,d={height:c.outerHeight(E),width:c.outerWidth(E)},e=this.qtip.plugins.tip,f=this.qtip.elements.tip;b=parseInt(c.css("borderLeftWidth"),10)||0,b={left:-b,top:-b},e&&f&&(a="x"===e.corner.precedance?[I,L]:[J,K],b[a[1]]-=f[a[0]]()),this.bgiframe.css(b).css(d)},redraw:function(){if(this.qtip.rendered<1||this.drawing)return this;var a,b,c,d,e=this.qtip.tooltip,f=this.qtip.options.style,g=this.qtip.options.position.container;return this.qtip.drawing=1,f.height&&e.css(J,f.height),f.width?e.css(I,f.width):(e.css(I,"").appendTo(this.redrawContainer),b=e.width(),1>b%2&&(b+=1),c=e.css("maxWidth")||"",d=e.css("minWidth")||"",a=(c+d).indexOf("%")>-1?g.width()/100:0,c=(c.indexOf("%")>-1?a:1)*parseInt(c,10)||b,d=(d.indexOf("%")>-1?a:1)*parseInt(d,10)||0,b=c+d?Math.min(Math.max(b,d),c):b,e.css(I,Math.round(b)).appendTo(g)),this.drawing=0,this},destroy:function(){this.bgiframe&&this.bgiframe.remove(),this.qtip._unbind([a,this.qtip.tooltip],this._ns)}}),Ab=R.ie6=function(a){return 6===db.ie?new x(a):E},Ab.initialize="render",B.ie6={"^content|style$":function(){this.redraw()}}})}(window,document);
//# sourceMappingURL=jquery.qtip.min.js.map

/*
 * iosSlider - http://iosscripts.com/iosslider/
 * 
 * Touch Enabled, Responsive jQuery Horizontal Content Slider/Carousel/Image Gallery Plugin
 *
 * A jQuery plugin which allows you to integrate a customizable, cross-browser 
 * content slider into your web presence. Designed for use as a content slider, carousel, 
 * scrolling website banner, or image gallery.
 * 
 * Copyright (c) 2013 Marc Whitbread
 * 
 * Version: v1.3.43 (06/17/2014)
 * Minimum requirements: jQuery v1.4+
 *
 * Advanced requirements:
 * 1) jQuery bind() click event override on slide requires jQuery v1.6+
 *
 * Terms of use:
 *
 * 1) iosSlider is licensed under the Creative Commons – Attribution-NonCommercial 3.0 License.
 * 2) You may use iosSlider free for personal or non-profit purposes, without restriction.
 *	  Attribution is not required but always appreciated. For commercial projects, you
 *	  must purchase a license. You may download and play with the script before deciding to
 *	  fully implement it in your project. Making sure you are satisfied, and knowing iosSlider
 *	  is the right script for your project is paramount.
 * 3) You are not permitted to make the resources found on iosscripts.com available for
 *    distribution elsewhere "as is" without prior consent. If you would like to feature
 *    iosSlider on your site, please do not link directly to the resource zip files. Please
 *    link to the appropriate page on iosscripts.com where users can find the download.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

;(function($) {
	
	/* global variables */
	var scrollbarNumber = 0;
	var xScrollDistance = 0;
	var yScrollDistance = 0;
	var scrollIntervalTime = 10;
	var scrollbarDistance = 0;
	var isTouch = 'ontouchstart' in window || (navigator.msMaxTouchPoints > 0);
	var supportsOrientationChange = 'onorientationchange' in window;
	var isWebkit = false;
	var has3DTransform = false;
	var isIe7 = false;
	var isIe8 = false;
	var isIe9 = false;
	var isIe = false;
	var isGecko = false;
	var grabOutCursor = 'pointer';
	var grabInCursor = 'pointer';
	var onChangeEventLastFired = new Array();
	var autoSlideTimeouts = new Array();
	var iosSliders = new Array();
	var iosSliderSettings = new Array();
	var isEventCleared = new Array();
	var slideTimeouts = new Array();
	var activeChildOffsets = new Array();
	var activeChildInfOffsets = new Array();
	var infiniteSliderOffset = new Array();
	var sliderMin = new Array();
	var sliderMax = new Array();
	var sliderAbsMax = new Array();
	var touchLocks = new Array();
	
	/* private functions */
	var helpers = {
    
        showScrollbar: function(settings, scrollbarClass) {
			
			if(settings.scrollbarHide) {
				$('.' + scrollbarClass).css({
					opacity: settings.scrollbarOpacity,
					filter: 'alpha(opacity:' + (settings.scrollbarOpacity * 100) + ')'
				});
			}
			
		},
		
		hideScrollbar: function(settings, scrollTimeouts, j, distanceOffsetArray, scrollbarClass, scrollbarWidth, stageWidth, scrollMargin, scrollBorder, sliderNumber) {
			
			if(settings.scrollbar && settings.scrollbarHide) {
					
				for(var i = j; i < j+25; i++) {
					
					scrollTimeouts[scrollTimeouts.length] = helpers.hideScrollbarIntervalTimer(scrollIntervalTime * i, distanceOffsetArray[j], ((j + 24) - i) / 24, scrollbarClass, scrollbarWidth, stageWidth, scrollMargin, scrollBorder, sliderNumber, settings);
					
				}
			
			}
			
		},
		
		hideScrollbarInterval: function(newOffset, opacity, scrollbarClass, scrollbarWidth, stageWidth, scrollMargin, scrollBorder, sliderNumber, settings) {
	
			scrollbarDistance = (newOffset * -1) / (sliderMax[sliderNumber]) * (stageWidth - scrollMargin - scrollBorder - scrollbarWidth);
			
			helpers.setSliderOffset('.' + scrollbarClass, scrollbarDistance);
			
			$('.' + scrollbarClass).css({
				opacity: settings.scrollbarOpacity * opacity,
				filter: 'alpha(opacity:' + (settings.scrollbarOpacity * opacity * 100) + ')'
			});
			
		},
		
		slowScrollHorizontalInterval: function(node, slideNodes, newOffset, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, activeChildOffset, originalOffsets, childrenOffsets, infiniteSliderWidth, numberOfSlides, slideNodeOuterWidths, sliderNumber, centeredSlideOffset, endOffset, settings) {
			
			if(settings.infiniteSlider) {
				
				if((newOffset <= (sliderMax[sliderNumber] * -1)) || (newOffset <= (sliderAbsMax[sliderNumber] * -1))) {
					
					var scrollerWidth = $(node).width();

					if(newOffset <= (sliderAbsMax[sliderNumber] * -1)) {
						
						var sum = originalOffsets[0] * -1;
						$(slideNodes).each(function(i) {
							
							helpers.setSliderOffset($(slideNodes)[i], sum + centeredSlideOffset);
							if(i < childrenOffsets.length) {
								childrenOffsets[i] = sum * -1;
							}
							sum = sum + slideNodeOuterWidths[i];
							
						});
						
						newOffset = newOffset + childrenOffsets[0] * -1;
						sliderMin[sliderNumber] = childrenOffsets[0] * -1 + centeredSlideOffset;
						sliderMax[sliderNumber] = sliderMin[sliderNumber] + scrollerWidth - stageWidth;
						infiniteSliderOffset[sliderNumber] = 0;
						
					}
						
					while(newOffset <= (sliderMax[sliderNumber] * -1)) {
						
						var lowSlideNumber = 0;
						var lowSlideOffset = helpers.getSliderOffset($(slideNodes[0]), 'x');
						$(slideNodes).each(function(i) {
							
							if(helpers.getSliderOffset(this, 'x') < lowSlideOffset) {
								lowSlideOffset = helpers.getSliderOffset(this, 'x');
								lowSlideNumber = i;
							}
							
						});
						
						var tempOffset = sliderMin[sliderNumber] + scrollerWidth;
						helpers.setSliderOffset($(slideNodes)[lowSlideNumber], tempOffset);
						
						sliderMin[sliderNumber] = childrenOffsets[1] * -1 + centeredSlideOffset;
						sliderMax[sliderNumber] = sliderMin[sliderNumber] + scrollerWidth - stageWidth;

						childrenOffsets.splice(0, 1);
						childrenOffsets.splice(childrenOffsets.length, 0, tempOffset * -1 + centeredSlideOffset);

						infiniteSliderOffset[sliderNumber]++;
						
					}
					
				}
				
				if((newOffset >= (sliderMin[sliderNumber] * -1)) || (newOffset >= 0)) {

					var scrollerWidth = $(node).width();
					
					if(newOffset > 0) {
						
						var sum = originalOffsets[0] * -1;
						$(slideNodes).each(function(i) {
							
							helpers.setSliderOffset($(slideNodes)[i], sum + centeredSlideOffset);
							if(i < childrenOffsets.length) {
								childrenOffsets[i] = sum * -1;
							}
							sum = sum + slideNodeOuterWidths[i];
							
						});
						
						newOffset = newOffset - childrenOffsets[0] * -1;
						sliderMin[sliderNumber] = childrenOffsets[0] * -1 + centeredSlideOffset;
						sliderMax[sliderNumber] = sliderMin[sliderNumber] + scrollerWidth - stageWidth;
						infiniteSliderOffset[sliderNumber] = numberOfSlides;
						
						while(((childrenOffsets[0] * -1 - scrollerWidth + centeredSlideOffset) > 0)) {
							
							var highSlideNumber = 0;
							var highSlideOffset = helpers.getSliderOffset($(slideNodes[0]), 'x');
							$(slideNodes).each(function(i) {
								
								if(helpers.getSliderOffset(this, 'x') > highSlideOffset) {
									highSlideOffset = helpers.getSliderOffset(this, 'x');
									highSlideNumber = i;
								}
								
							});

							var tempOffset = sliderMin[sliderNumber] - slideNodeOuterWidths[highSlideNumber];
							helpers.setSliderOffset($(slideNodes)[highSlideNumber], tempOffset);
							
							childrenOffsets.splice(0, 0, tempOffset * -1 + centeredSlideOffset);
							childrenOffsets.splice(childrenOffsets.length-1, 1);

							sliderMin[sliderNumber] = childrenOffsets[0] * -1 + centeredSlideOffset;
							sliderMax[sliderNumber] = sliderMin[sliderNumber] + scrollerWidth - stageWidth;

							infiniteSliderOffset[sliderNumber]--;
							activeChildOffsets[sliderNumber]++;
							
						}

					} 
					
					while(newOffset > (sliderMin[sliderNumber] * -1)) {
						
						var highSlideNumber = 0;
						var highSlideOffset = helpers.getSliderOffset($(slideNodes[0]), 'x');
						$(slideNodes).each(function(i) {
							
							if(helpers.getSliderOffset(this, 'x') > highSlideOffset) {
								highSlideOffset = helpers.getSliderOffset(this, 'x');
								highSlideNumber = i;
							}
							
						});						
					
						var tempOffset = sliderMin[sliderNumber] - slideNodeOuterWidths[highSlideNumber];
						helpers.setSliderOffset($(slideNodes)[highSlideNumber], tempOffset);

						childrenOffsets.splice(0, 0, tempOffset * -1 + centeredSlideOffset);
						childrenOffsets.splice(childrenOffsets.length-1, 1);

						sliderMin[sliderNumber] = childrenOffsets[0] * -1 + centeredSlideOffset;
						sliderMax[sliderNumber] = sliderMin[sliderNumber] + scrollerWidth - stageWidth;

						infiniteSliderOffset[sliderNumber]--;
						
					}
				
				}
				
			}

			var slideChanged = false;
			var newChildOffset = helpers.calcActiveOffset(settings, newOffset, childrenOffsets, stageWidth, infiniteSliderOffset[sliderNumber], numberOfSlides, activeChildOffset, sliderNumber);
			var tempOffset = (newChildOffset + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;
			
			if(settings.infiniteSlider) {
								
				if(tempOffset != activeChildInfOffsets[sliderNumber]) slideChanged = true;
					
			} else {
			
				if(newChildOffset != activeChildOffsets[sliderNumber]) slideChanged = true;
			
			}
			
			if(slideChanged) {

				var args = new helpers.args('change', settings, node, $(node).children(':eq(' + tempOffset + ')'), tempOffset, endOffset);
				$(node).parent().data('args', args);
				
				if(settings.onSlideChange != '') {
				
					settings.onSlideChange(args);
				
				}
			
			}
			
			activeChildOffsets[sliderNumber] = newChildOffset;
			activeChildInfOffsets[sliderNumber] = tempOffset;
			
			newOffset = Math.floor(newOffset);
			
			if(sliderNumber != $(node).parent().data('args').data.sliderNumber) return true;
			helpers.setSliderOffset(node, newOffset);

			if(settings.scrollbar) {
				
				scrollbarDistance = Math.floor((newOffset * -1 - sliderMin[sliderNumber] + centeredSlideOffset) / (sliderMax[sliderNumber] - sliderMin[sliderNumber] + centeredSlideOffset) * (scrollbarStageWidth - scrollMargin - scrollbarWidth));
				var width = scrollbarWidth - scrollBorder;
				
				if(newOffset >= (sliderMin[sliderNumber] * -1 + centeredSlideOffset)) {

					width = scrollbarWidth - scrollBorder - (scrollbarDistance * -1);
					
					helpers.setSliderOffset($('.' + scrollbarClass), 0);
					
					$('.' + scrollbarClass).css({
						width: width + 'px'
					});
				
				} else if(newOffset <= ((sliderMax[sliderNumber] * -1) + 1)) {
					
					width = scrollbarStageWidth - scrollMargin - scrollBorder - scrollbarDistance;
					
					helpers.setSliderOffset($('.' + scrollbarClass), scrollbarDistance);
					
					$('.' + scrollbarClass).css({
						width: width + 'px'
					});
					
				} else {
					
					helpers.setSliderOffset($('.' + scrollbarClass), scrollbarDistance);
					
					$('.' + scrollbarClass).css({
						width: width + 'px'
					});
				
				}
				
			}
			
		},
		
		slowScrollHorizontal: function(node, slideNodes, scrollTimeouts, scrollbarClass, xScrollDistance, yScrollDistance, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, currentEventNode, snapOverride, centeredSlideOffset, settings) {
			
			var nodeOffset = helpers.getSliderOffset(node, 'x');
			var distanceOffsetArray = new Array();
			var xScrollDistanceArray = new Array();
			var snapDirection = 0;
			var maxSlideVelocity = 25 / 1024 * stageWidth;
			var changeSlideFired = false;
			frictionCoefficient = settings.frictionCoefficient;
			elasticFrictionCoefficient = settings.elasticFrictionCoefficient;
			snapFrictionCoefficient = settings.snapFrictionCoefficient;
				
			if((xScrollDistance > settings.snapVelocityThreshold) && settings.snapToChildren && !snapOverride) {
				snapDirection = 1;
			} else if((xScrollDistance < (settings.snapVelocityThreshold * -1)) && settings.snapToChildren && !snapOverride) {
				snapDirection = -1;
			}
			
			if(xScrollDistance < (maxSlideVelocity * -1)) {
				xScrollDistance = maxSlideVelocity * -1;
			} else if(xScrollDistance > maxSlideVelocity) {
				xScrollDistance = maxSlideVelocity;
			}
			
			if(!($(node)[0] === $(currentEventNode)[0])) {
				snapDirection = snapDirection * -1;
				xScrollDistance = xScrollDistance * -2;
			}
			
			var tempInfiniteSliderOffset = infiniteSliderOffset[sliderNumber];
			
			if(settings.infiniteSlider) {
			
				var tempSliderMin = sliderMin[sliderNumber];
				var tempSliderMax = sliderMax[sliderNumber];
			
			}
			
			var tempChildrenOffsets = new Array();
			var tempSlideNodeOffsets = new Array();

			for(var i = 0; i < childrenOffsets.length; i++) {
				
				tempChildrenOffsets[i] = childrenOffsets[i];
				
				if(i < slideNodes.length) {
					tempSlideNodeOffsets[i] = helpers.getSliderOffset($(slideNodes[i]), 'x');
				}
				
			}
			
			while((xScrollDistance > 1) || (xScrollDistance < -1)) {
				
				xScrollDistance = xScrollDistance * frictionCoefficient;
				nodeOffset = nodeOffset + xScrollDistance;

				if(((nodeOffset > (sliderMin[sliderNumber] * -1)) || (nodeOffset < (sliderMax[sliderNumber] * -1))) && !settings.infiniteSlider) {
					xScrollDistance = xScrollDistance * elasticFrictionCoefficient;
					nodeOffset = nodeOffset + xScrollDistance;
				}
				
				if(settings.infiniteSlider) {
					
					if(nodeOffset <= (tempSliderMax * -1)) {
						
						var scrollerWidth = $(node).width();
							
						var lowSlideNumber = 0;
						var lowSlideOffset = tempSlideNodeOffsets[0];
						for(var i = 0; i < tempSlideNodeOffsets.length; i++) {
							
							if(tempSlideNodeOffsets[i] < lowSlideOffset) {
								lowSlideOffset = tempSlideNodeOffsets[i];
								lowSlideNumber = i;
							}
							
						}
						
						var newOffset = tempSliderMin + scrollerWidth;
						tempSlideNodeOffsets[lowSlideNumber] = newOffset;
						
						tempSliderMin = tempChildrenOffsets[1] * -1 + centeredSlideOffset;
						tempSliderMax = tempSliderMin + scrollerWidth - stageWidth;
						
						tempChildrenOffsets.splice(0, 1);
						tempChildrenOffsets.splice(tempChildrenOffsets.length, 0, newOffset * -1 + centeredSlideOffset);

						tempInfiniteSliderOffset++;
						
					}
					
					if(nodeOffset >= (tempSliderMin * -1)) {
						
						var scrollerWidth = $(node).width();
						
						var highSlideNumber = 0;
						var highSlideOffset = tempSlideNodeOffsets[0];
						for(var i = 0; i < tempSlideNodeOffsets.length; i++) {
							
							if(tempSlideNodeOffsets[i] > highSlideOffset) {
								highSlideOffset = tempSlideNodeOffsets[i];
								highSlideNumber = i;
							}
							
						}

						var newOffset = tempSliderMin - slideNodeOuterWidths[highSlideNumber];
						tempSlideNodeOffsets[highSlideNumber] = newOffset;
						
						tempChildrenOffsets.splice(0, 0, newOffset * -1 + centeredSlideOffset);
						tempChildrenOffsets.splice(tempChildrenOffsets.length-1, 1);

						tempSliderMin = tempChildrenOffsets[0] * -1 + centeredSlideOffset;
						tempSliderMax = tempSliderMin + scrollerWidth - stageWidth;

						tempInfiniteSliderOffset--;
					
					}
						
				}

				distanceOffsetArray[distanceOffsetArray.length] = nodeOffset;
				xScrollDistanceArray[xScrollDistanceArray.length] = xScrollDistance;
				
			}

			var slideChanged = false;
			var newChildOffset = helpers.calcActiveOffset(settings, nodeOffset, tempChildrenOffsets, stageWidth, tempInfiniteSliderOffset, numberOfSlides, activeChildOffsets[sliderNumber], sliderNumber);

			var tempOffset = (newChildOffset + tempInfiniteSliderOffset + numberOfSlides)%numberOfSlides;

			if(settings.snapToChildren) {
			
				if(settings.infiniteSlider) {
				
					if(tempOffset != activeChildInfOffsets[sliderNumber]) {
						slideChanged = true;
					}
						
				} else {
				
					if(newChildOffset != activeChildOffsets[sliderNumber]) {
						slideChanged = true;
					}
				
				}

				if((snapDirection < 0) && !slideChanged) {
				
					newChildOffset++;
					
					if((newChildOffset >= childrenOffsets.length) && !settings.infiniteSlider) newChildOffset = childrenOffsets.length - 1;
					
				} else if((snapDirection > 0) && !slideChanged) {
				
					newChildOffset--;
					
					if((newChildOffset < 0) && !settings.infiniteSlider) newChildOffset = 0;
					
				}
				
			}

			if(settings.snapToChildren || (((nodeOffset > (sliderMin[sliderNumber] * -1)) || (nodeOffset < (sliderMax[sliderNumber] * -1))) && !settings.infiniteSlider)) {
				
				if(((nodeOffset > (sliderMin[sliderNumber] * -1)) || (nodeOffset < (sliderMax[sliderNumber] * -1))) && !settings.infiniteSlider) {
					distanceOffsetArray.splice(0, distanceOffsetArray.length);					
				} else {
					distanceOffsetArray.splice(distanceOffsetArray.length * 0.10, distanceOffsetArray.length);
					nodeOffset = (distanceOffsetArray.length > 0) ? distanceOffsetArray[distanceOffsetArray.length-1] : nodeOffset;
				}

				while((nodeOffset < (tempChildrenOffsets[newChildOffset] - 0.5)) || (nodeOffset > (tempChildrenOffsets[newChildOffset] + 0.5))) {
					
					nodeOffset = ((nodeOffset - (tempChildrenOffsets[newChildOffset])) * snapFrictionCoefficient) + (tempChildrenOffsets[newChildOffset]);
					distanceOffsetArray[distanceOffsetArray.length] = nodeOffset;

				}
				
				distanceOffsetArray[distanceOffsetArray.length] = tempChildrenOffsets[newChildOffset];
			}

			var jStart = 1;
			if((distanceOffsetArray.length%2) != 0) {
				jStart = 0;
			}
			
			var lastTimeoutRegistered = 0;
			var count = 0;
			
			for(var j = 0; j < scrollTimeouts.length; j++) {
				clearTimeout(scrollTimeouts[j]);
			}
			
			var endOffset = (newChildOffset + tempInfiniteSliderOffset + numberOfSlides)%numberOfSlides;
			
			var lastCheckOffset = 0;
			for(var j = jStart; j < distanceOffsetArray.length; j = j + 2) {
				
				if((j == jStart) || (Math.abs(distanceOffsetArray[j] - lastCheckOffset) > 1) || (j >= (distanceOffsetArray.length - 2))) {
				
					lastCheckOffset	= distanceOffsetArray[j];
					
					scrollTimeouts[scrollTimeouts.length] = helpers.slowScrollHorizontalIntervalTimer(scrollIntervalTime * j, node, slideNodes, distanceOffsetArray[j], scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, newChildOffset, originalOffsets, childrenOffsets, infiniteSliderWidth, numberOfSlides, slideNodeOuterWidths, sliderNumber, centeredSlideOffset, endOffset, settings);
				
				}
				
			}
			
			var slideChanged = false;
			var tempOffset = (newChildOffset + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;
			
			if(settings.infiniteSlider) {
				
				if(tempOffset != activeChildInfOffsets[sliderNumber]) {
					slideChanged = true;
				}
					
			} else {
			
				if(newChildOffset != activeChildOffsets[sliderNumber]) {
					slideChanged = true;
				}
			
			}
			
			if(settings.onSlideComplete != '' && (distanceOffsetArray.length > 1)) {
				
				scrollTimeouts[scrollTimeouts.length] = helpers.onSlideCompleteTimer(scrollIntervalTime * (j + 1), settings, node, $(node).children(':eq(' + tempOffset + ')'), endOffset, sliderNumber);
				
			}
			
			scrollTimeouts[scrollTimeouts.length] = helpers.updateBackfaceVisibilityTimer(scrollIntervalTime * (j + 1), slideNodes, sliderNumber, numberOfSlides, settings);
			
			slideTimeouts[sliderNumber] = scrollTimeouts;
			
			helpers.hideScrollbar(settings, scrollTimeouts, j, distanceOffsetArray, scrollbarClass, scrollbarWidth, stageWidth, scrollMargin, scrollBorder, sliderNumber);
				
		},
		
		onSlideComplete: function(settings, node, slideNode, newChildOffset, sliderNumber) {
			
			var isChanged = (onChangeEventLastFired[sliderNumber] != newChildOffset) ? true : false;
			var args = new helpers.args('complete', settings, $(node), slideNode, newChildOffset, newChildOffset);
			$(node).parent().data('args', args);
				
			if(settings.onSlideComplete != '') {
				settings.onSlideComplete(args);
			}
			
			onChangeEventLastFired[sliderNumber] = newChildOffset;
		
		},
		
		getSliderOffset: function(node, xy) {
			
			var sliderOffset = 0;
			xy = (xy == 'x') ? 4 : 5;
			
			if(has3DTransform && !isIe7 && !isIe8) {
				
				var transforms = new Array('-webkit-transform', '-moz-transform', 'transform');
				var transformArray;
				
				for(var i = 0; i < transforms.length; i++) {
					
					if($(node).css(transforms[i]) != undefined) {
						
						if($(node).css(transforms[i]).length > 0) {
						
							transformArray = $(node).css(transforms[i]).split(',');
							
							break;
							
						}
					
					}
				
				}
				
				sliderOffset = (transformArray[xy] == undefined) ? 0 : parseInt(transformArray[xy], 10);

			} else {
			
				sliderOffset = parseInt($(node).css('left'), 10);
			
			}
			
			return sliderOffset;
		
		},
		
		setSliderOffset: function(node, sliderOffset) {
			
			sliderOffset = parseInt(sliderOffset, 10);
			
			if(has3DTransform && !isIe7 && !isIe8) {
				
				$(node).css({
					'msTransform': 'matrix(1,0,0,1,' + sliderOffset + ',0)',
					'webkitTransform': 'matrix(1,0,0,1,' + sliderOffset + ',0)',
					'MozTransform': 'matrix(1,0,0,1,' + sliderOffset + ',0)',
					'transform': 'matrix(1,0,0,1,' + sliderOffset + ',0)'
				});
			
			} else {

				$(node).css({
					left: sliderOffset + 'px'
				});
			
			}

		},
		
		setBrowserInfo: function() {
			
			if(navigator.userAgent.match('WebKit') != null) {
				isWebkit = true;
				grabOutCursor = '-webkit-grab';
				grabInCursor = '-webkit-grabbing';
			} else if(navigator.userAgent.match('Gecko') != null) {
				isGecko = true;
				grabOutCursor = 'move';
				grabInCursor = '-moz-grabbing';
			} else if(navigator.userAgent.match('MSIE 7') != null) {
				isIe7 = true;
				isIe = true;
			} else if(navigator.userAgent.match('MSIE 8') != null) {
				isIe8 = true;
				isIe = true;
			} else if(navigator.userAgent.match('MSIE 9') != null) {
				isIe9 = true;
				isIe = true;
			}
			
		},
		
		has3DTransform: function() {
			
			var has3D = false;
			
			var testElement = $('<div />').css({
				'msTransform': 'matrix(1,1,1,1,1,1)',
				'webkitTransform': 'matrix(1,1,1,1,1,1)',
				'MozTransform': 'matrix(1,1,1,1,1,1)',
				'transform': 'matrix(1,1,1,1,1,1)'
			});
			
			if(testElement.attr('style') == '') {
				has3D = false;
			} else if(isGecko && (parseInt(navigator.userAgent.split('/')[3], 10) >= 21)) {
				//bug in v21+ which does not render slides properly in 3D
				has3D = false;
			} else if(testElement.attr('style') != undefined) {
				has3D = true;
			}
			
			return has3D;
			
		},
		
		getSlideNumber: function(slide, sliderNumber, numberOfSlides) {
			
			return (slide - infiniteSliderOffset[sliderNumber] + numberOfSlides) % numberOfSlides;
		
		}, 

        calcActiveOffset: function(settings, offset, childrenOffsets, stageWidth, infiniteSliderOffset, numberOfSlides, activeChildOffset, sliderNumber) {

			var isFirst = false;
			var arrayOfOffsets = new Array();
			var newChildOffset;
			
			if(offset > childrenOffsets[0]) newChildOffset = 0;
			if(offset < (childrenOffsets[childrenOffsets.length-1])) newChildOffset = numberOfSlides - 1;
			
			for(var i = 0; i < childrenOffsets.length; i++) {
								
				if((childrenOffsets[i] <= offset) && (childrenOffsets[i] > (offset - stageWidth))) {
				
					if(!isFirst && (childrenOffsets[i] != offset)) {
						
						arrayOfOffsets[arrayOfOffsets.length] = childrenOffsets[i-1];
						
					}
					
					arrayOfOffsets[arrayOfOffsets.length] = childrenOffsets[i];
					
					isFirst = true;
						
				}
			
			}
			
			if(arrayOfOffsets.length == 0) {
				arrayOfOffsets[0] = childrenOffsets[childrenOffsets.length - 1];
			}
			
			var distance = stageWidth;
			var closestChildOffset = 0;
			
			for(var i = 0; i < arrayOfOffsets.length; i++) {
				
				var newDistance = Math.abs(offset - arrayOfOffsets[i]);

				if(newDistance < distance) {
					closestChildOffset = arrayOfOffsets[i];
					distance = newDistance;
				}
				
			}
			
			for(var i = 0; i < childrenOffsets.length; i++) {
				
				if(closestChildOffset == childrenOffsets[i]) {
					newChildOffset = i;
						
				}
				
			}
			
			return newChildOffset;
		
		},
		
		changeSlide: function(slide, node, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings) {
			
			helpers.autoSlidePause(sliderNumber);
			
			for(var j = 0; j < scrollTimeouts.length; j++) {
				clearTimeout(scrollTimeouts[j]);
			}
			
			var steps = Math.ceil(settings.autoSlideTransTimer / 10) + 1;
			var startOffset = helpers.getSliderOffset(node, 'x');
			var endOffset = childrenOffsets[slide];
			var offsetDiff = endOffset - startOffset;
			var direction = slide - (activeChildOffsets[sliderNumber] + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;

			if(settings.infiniteSlider) {
				
				slide = (slide - infiniteSliderOffset[sliderNumber] + numberOfSlides * 2)%numberOfSlides;
				
				var appendArray = false;
				if((slide == 0) && (numberOfSlides == 2)) {
					
					slide = numberOfSlides;
					childrenOffsets[slide] = childrenOffsets[slide-1] - $(slideNodes).eq(0).outerWidth(true);
					appendArray = true;
					
				}
				
				endOffset = childrenOffsets[slide];
				offsetDiff = endOffset - startOffset;
								
				var offsets = new Array(childrenOffsets[slide] - $(node).width(), childrenOffsets[slide] + $(node).width());
				
				if(appendArray) {
					childrenOffsets.splice(childrenOffsets.length-1, 1);
				}
				
				for(var i = 0; i < offsets.length; i++) {
					
					if(Math.abs(offsets[i] - startOffset) < Math.abs(offsetDiff)) {
						offsetDiff = (offsets[i] - startOffset);
					}
				
				}
				
			}
			
			if((offsetDiff < 0) && (direction == -1)) {
				offsetDiff += $(node).width();
			} else if((offsetDiff > 0) && (direction == 1)) {
				offsetDiff -= $(node).width();
			}
			
			var stepArray = new Array();
			var t;
			var nextStep;

			helpers.showScrollbar(settings, scrollbarClass);

			for(var i = 0; i <= steps; i++) {

				t = i;
				t /= steps;
				t--;
				nextStep = startOffset + offsetDiff*(Math.pow(t,5) + 1);
				
				stepArray[stepArray.length] = nextStep;
				
			}
			
			var tempOffset = (slide + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;

			var lastCheckOffset = 0;
			for(var i = 0; i < stepArray.length; i++) {
				
				if((i == 0) || (Math.abs(stepArray[i] - lastCheckOffset) > 1) || (i >= (stepArray.length - 2))) {

					lastCheckOffset	= stepArray[i];
					
					scrollTimeouts[i] = helpers.slowScrollHorizontalIntervalTimer(scrollIntervalTime * (i + 1), node, slideNodes, stepArray[i], scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, slide, originalOffsets, childrenOffsets, infiniteSliderWidth, numberOfSlides, slideNodeOuterWidths, sliderNumber, centeredSlideOffset, tempOffset, settings);
						
				}
				
				if((i == 0) && (settings.onSlideStart != '')) {
				
					var tempOffset2 = (activeChildOffsets[sliderNumber] + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;	
					settings.onSlideStart(new helpers.args('start', settings, node, $(node).children(':eq(' + tempOffset2 + ')'), tempOffset2, slide));
					
				}
					
			}

			var slideChanged = false;
			
			if(settings.infiniteSlider) {
				
				if(tempOffset != activeChildInfOffsets[sliderNumber]) {
					slideChanged = true;
				}
					
			} else {
			
				if(slide != activeChildOffsets[sliderNumber]) {
					slideChanged = true;
				}
			
			}
	
			if(slideChanged && (settings.onSlideComplete != '')) {

				scrollTimeouts[scrollTimeouts.length] = helpers.onSlideCompleteTimer(scrollIntervalTime * (i + 1), settings, node, $(node).children(':eq(' + tempOffset + ')'), tempOffset, sliderNumber);
				
			}
			
			/*scrollTimeouts[scrollTimeouts.length] = setTimeout(function() {
				activeChildOffsets[sliderNumber] = activeChildOffsets[sliderNumber];
			}, scrollIntervalTime * (i + 1));*/
			
			slideTimeouts[sliderNumber] = scrollTimeouts;
			
			helpers.hideScrollbar(settings, scrollTimeouts, i, stepArray, scrollbarClass, scrollbarWidth, stageWidth, scrollMargin, scrollBorder, sliderNumber);
			
			helpers.autoSlide(node, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings);
			
		},
		
		changeOffset: function(endOffset, node, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings) {
		
			helpers.autoSlidePause(sliderNumber);
			
			for(var j = 0; j < scrollTimeouts.length; j++) {
				clearTimeout(scrollTimeouts[j]);
			}
			
			if(!settings.infiniteSlider) {
				endOffset = (endOffset > (sliderMin[sliderNumber] * -1 + centeredSlideOffset)) ? sliderMin[sliderNumber] * -1 + centeredSlideOffset : endOffset;
				endOffset = (endOffset < (sliderMax[sliderNumber] * -1)) ? sliderMax[sliderNumber] * -1 : endOffset;
			}
			
			var steps = Math.ceil(settings.autoSlideTransTimer / 10) + 1;
			var startOffset = helpers.getSliderOffset(node, 'x');
			var slide = (helpers.calcActiveOffset(settings, endOffset, childrenOffsets, stageWidth, infiniteSliderOffset, numberOfSlides, activeChildOffsets[sliderNumber], sliderNumber) + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;
			var testOffsets = childrenOffsets.slice();
			
			if(settings.snapToChildren && !settings.infiniteSlider) {
				endOffset = childrenOffsets[slide];
			} else if(settings.infiniteSlider && settings.snapToChildren) {
				while(endOffset >= testOffsets[0]) {
					testOffsets.splice(0, 0, testOffsets[numberOfSlides-1] + $(node).width());
					testOffsets.splice(numberOfSlides, 1);
				}
				
				while(endOffset <= testOffsets[numberOfSlides-1]) {
					testOffsets.splice(numberOfSlides, 0, testOffsets[0] - $(node).width());
					testOffsets.splice(0, 1);
				}
				
				slide = helpers.calcActiveOffset(settings, endOffset, testOffsets, stageWidth, infiniteSliderOffset, numberOfSlides, activeChildOffsets[sliderNumber], sliderNumber);
				endOffset = testOffsets[slide];
				
			}
			
			var offsetDiff = endOffset - startOffset;
						
			var stepArray = new Array();
			var t;
			var nextStep;

			helpers.showScrollbar(settings, scrollbarClass);

			for(var i = 0; i <= steps; i++) {

				t = i;
				t /= steps;
				t--;
				nextStep = startOffset + offsetDiff*(Math.pow(t,5) + 1);
				
				stepArray[stepArray.length] = nextStep;
				
			}
			
			var tempOffset = (slide + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;
			
			var lastCheckOffset = 0;
			for(var i = 0; i < stepArray.length; i++) {
				
				if((i == 0) || (Math.abs(stepArray[i] - lastCheckOffset) > 1) || (i >= (stepArray.length - 2))) {

					lastCheckOffset	= stepArray[i];
					
					scrollTimeouts[i] = helpers.slowScrollHorizontalIntervalTimer(scrollIntervalTime * (i + 1), node, slideNodes, stepArray[i], scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, slide, originalOffsets, childrenOffsets, infiniteSliderWidth, numberOfSlides, slideNodeOuterWidths, sliderNumber, centeredSlideOffset, tempOffset, settings);
						
				}
				
				if((i == 0) && (settings.onSlideStart != '')) {
					var tempOffset = (activeChildOffsets[sliderNumber] + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;		
				
					settings.onSlideStart(new helpers.args('start', settings, node, $(node).children(':eq(' + tempOffset + ')'), tempOffset, slide));
				}
					
			}

			var slideChanged = false;
			
			if(settings.infiniteSlider) {
				
				if(tempOffset != activeChildInfOffsets[sliderNumber]) {
					slideChanged = true;
				}
					
			} else {
			
				if(slide != activeChildOffsets[sliderNumber]) {
					slideChanged = true;
				}
			
			}
				
			if(slideChanged && (settings.onSlideComplete != '')) {

				scrollTimeouts[scrollTimeouts.length] = helpers.onSlideCompleteTimer(scrollIntervalTime * (i + 1), settings, node, $(node).children(':eq(' + tempOffset + ')'), tempOffset, sliderNumber);
			}
			
			slideTimeouts[sliderNumber] = scrollTimeouts;
			
			helpers.hideScrollbar(settings, scrollTimeouts, i, stepArray, scrollbarClass, scrollbarWidth, stageWidth, scrollMargin, scrollBorder, sliderNumber);
			
			helpers.autoSlide(node, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings);
			
		},
		
		autoSlide: function(scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings) {
			
			if(!iosSliderSettings[sliderNumber].autoSlide) return false;
			
			helpers.autoSlidePause(sliderNumber);

			autoSlideTimeouts[sliderNumber] = setTimeout(function() {

				if(!settings.infiniteSlider && (activeChildOffsets[sliderNumber] > childrenOffsets.length-1)) {
					activeChildOffsets[sliderNumber] = activeChildOffsets[sliderNumber] - numberOfSlides;
				}
				
				var nextSlide = activeChildOffsets[sliderNumber] + infiniteSliderOffset[sliderNumber] + 1;

				helpers.changeSlide(nextSlide, scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings);
				
				helpers.autoSlide(scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings);
				
			}, settings.autoSlideTimer + settings.autoSlideTransTimer);
			
		},
				
		autoSlidePause: function(sliderNumber) {

			clearTimeout(autoSlideTimeouts[sliderNumber]);

		},
		
		isUnselectable: function(node, settings) {

			if(settings.unselectableSelector != '') {
				if($(node).closest(settings.unselectableSelector).length == 1) return true;
			}
			
			return false;
			
		},
		
		/* timers */
		slowScrollHorizontalIntervalTimer: function(scrollIntervalTime, node, slideNodes, step, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, slide, originalOffsets, childrenOffsets, infiniteSliderWidth, numberOfSlides, slideNodeOuterWidths, sliderNumber, centeredSlideOffset, endOffset, settings) {
		
			var scrollTimeout = setTimeout(function() {
				helpers.slowScrollHorizontalInterval(node, slideNodes, step, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, slide, originalOffsets, childrenOffsets, infiniteSliderWidth, numberOfSlides, slideNodeOuterWidths, sliderNumber, centeredSlideOffset, endOffset, settings);
			}, scrollIntervalTime);
			
			return scrollTimeout;
		
		},
		
		onSlideCompleteTimer: function(scrollIntervalTime, settings, node, slideNode, slide, scrollbarNumber) {

			var scrollTimeout = setTimeout(function() {
				helpers.onSlideComplete(settings, node, slideNode, slide, scrollbarNumber);
			}, scrollIntervalTime);
			
			return scrollTimeout;
		
		},
		
		hideScrollbarIntervalTimer: function(scrollIntervalTime, newOffset, opacity, scrollbarClass, scrollbarWidth, stageWidth, scrollMargin, scrollBorder, sliderNumber, settings) {

			var scrollTimeout = setTimeout(function() {
				helpers.hideScrollbarInterval(newOffset, opacity, scrollbarClass, scrollbarWidth, stageWidth, scrollMargin, scrollBorder, sliderNumber, settings);
			}, scrollIntervalTime);
		
			return scrollTimeout;
		
		},
		
		updateBackfaceVisibilityTimer: function(scrollIntervalTime, slideNodes, sliderNumber, numberOfSlides, settings) {
		
			var scrollTimeout = setTimeout(function() {
				helpers.updateBackfaceVisibility(slideNodes, sliderNumber, numberOfSlides, settings);
			}, scrollIntervalTime);
			
			return scrollTimeout;
			
		},
		
		updateBackfaceVisibility: function(slideNodes, sliderNumber, numberOfSlides, settings) {

			var slide = (activeChildOffsets[sliderNumber] + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;
			var usedSlideArray = Array();
			
			//loop through buffered slides
			for(var i = 0; i < (settings.hardwareAccelBuffer * 2); i++) {
				
				var slide_eq = helpers.mod(slide+i-settings.hardwareAccelBuffer, numberOfSlides);
				
				//check if backface visibility applied
				if($(slideNodes).eq(slide_eq).css('-webkit-backface-visibility') == 'visible') {
					
					usedSlideArray[usedSlideArray.length] = slide_eq;
					
					var eq_h = helpers.mod(slide_eq+settings.hardwareAccelBuffer*2, numberOfSlides);
					var eq_l = helpers.mod(slide_eq-settings.hardwareAccelBuffer*2, numberOfSlides);
					
					//buffer backface visibility
					$(slideNodes).eq(slide_eq).css('-webkit-backface-visibility', 'hidden');
					
					if(usedSlideArray.indexOf(eq_l) == -1) 
						$(slideNodes).eq(eq_l).css('-webkit-backface-visibility', '');
						
					if(usedSlideArray.indexOf(eq_h) == -1) 
						$(slideNodes).eq(eq_h).css('-webkit-backface-visibility', '');
					
				}
				
			}
			
		},
		
		mod: function(x, mod) {
		
			var rem = x % mod;
			
		    return rem < 0 ? rem + mod : rem;
			
		},
						
		args: function(func, settings, node, activeSlideNode, newChildOffset, targetSlideOffset) {
			
			this.prevSlideNumber = ($(node).parent().data('args') == undefined) ? undefined : $(node).parent().data('args').prevSlideNumber;
			this.prevSlideObject = ($(node).parent().data('args') == undefined) ? undefined : $(node).parent().data('args').prevSlideObject;
			this.targetSlideNumber = targetSlideOffset + 1;
			this.targetSlideObject = $(node).children(':eq(' + targetSlideOffset + ')');
			this.slideChanged = false;
			
			if(func == 'load') {
				this.targetSlideNumber = undefined;
				this.targetSlideObject = undefined;
			} else if(func == 'start') {
				this.targetSlideNumber = undefined;
				this.targetSlideObject = undefined;
			} else if(func == 'change') {
				this.slideChanged = true;
				this.prevSlideNumber = ($(node).parent().data('args') == undefined) ? settings.startAtSlide : $(node).parent().data('args').currentSlideNumber;
				this.prevSlideObject = $(node).children(':eq(' + this.prevSlideNumber + ')');
			} else if(func == 'complete') {
				this.slideChanged = $(node).parent().data('args').slideChanged;
			}
			
			this.settings = settings;
			this.data = $(node).parent().data('iosslider');
			this.sliderObject = node;
			this.sliderContainerObject = $(node).parent();

			this.currentSlideObject = activeSlideNode;
			this.currentSlideNumber = newChildOffset + 1;
			this.currentSliderOffset = helpers.getSliderOffset(node, 'x') * -1;
			
		},
		
		preventDrag: function(event) {
			event.preventDefault();
		},
		
		preventClick: function(event) {
			event.stopImmediatePropagation();
			return false;
		},
		
		enableClick: function() {
			return true;
		}
        
    }
    
    helpers.setBrowserInfo();
    
    var methods = {
		
		init: function(options, node) {

			has3DTransform = helpers.has3DTransform();
			
			var settings = $.extend(true, {
				'elasticPullResistance': 0.6, 		
				'frictionCoefficient': 0.92,
				'elasticFrictionCoefficient': 0.6,
				'snapFrictionCoefficient': 0.92,
				'snapToChildren': false,
				'snapSlideCenter': false,
				'startAtSlide': 1,
				'scrollbar': false,
				'scrollbarDrag': false,
				'scrollbarHide': true,
				'scrollbarPaging': false,
				'scrollbarLocation': 'top',
				'scrollbarContainer': '',
				'scrollbarOpacity': 0.4,
				'scrollbarHeight': '4px',
				'scrollbarBorder': '0',
				'scrollbarMargin': '5px',
				'scrollbarBackground': '#000',
				'scrollbarBorderRadius': '100px',
				'scrollbarShadow': '0 0 0 #000',
				'scrollbarElasticPullResistance': 0.9,
				'desktopClickDrag': false,
				'keyboardControls': false,
				'tabToAdvance': false,
				'responsiveSlideContainer': true,
				'responsiveSlides': true,
				'navSlideSelector': '',
				'navPrevSelector': '',
				'navNextSelector': '',
				'autoSlideToggleSelector': '',
				'autoSlide': false,
				'autoSlideTimer': 5000,
				'autoSlideTransTimer': 750,
				'autoSlideHoverPause': true,
				'infiniteSlider': false,
				'snapVelocityThreshold': 5,
				'slideStartVelocityThreshold': 0,
				'horizontalSlideLockThreshold': 5,
				'verticalSlideLockThreshold': 3,
				'hardwareAccelBuffer': 5,
				'stageCSS': {
					position: 'relative',
					top: '0',
					left: '0',
					overflow: 'hidden',
					zIndex: 1
				},
				'unselectableSelector': '',
				'onSliderLoaded': '',
				'onSliderUpdate': '',
				'onSliderResize': '',
				'onSlideStart': '',
				'onSlideChange': '',
				'onSlideComplete': ''
			}, options);
			
			if(node == undefined) {
				node = this;
			}
			
			return $(node).each(function(i) {
				
				scrollbarNumber++;
				var sliderNumber = scrollbarNumber;
				var scrollTimeouts = new Array();
				iosSliderSettings[sliderNumber] = $.extend({}, settings);
				sliderMin[sliderNumber] = 0;
				sliderMax[sliderNumber] = 0;
				var minTouchpoints = 0;
				var xCurrentScrollRate = new Array(0, 0);
				var yCurrentScrollRate = new Array(0, 0);
				var scrollbarBlockClass = 'scrollbarBlock' + scrollbarNumber;
				var scrollbarClass = 'scrollbar' + scrollbarNumber;
				var scrollbarNode;
				var scrollbarBlockNode;
				var scrollbarStageWidth;
				var scrollbarWidth;
				var containerWidth;
				var containerHeight;
				var centeredSlideOffset = 0;
				var stageNode = $(this);
				var stageWidth;
				var stageHeight;
				var slideWidth;
				var scrollMargin;
				var scrollBorder;
				var lastTouch;
				var isFirstInit = true;
				var newChildOffset = -1;
				var webkitTransformArray = new Array();
				var childrenOffsets;
				var originalOffsets = new Array();
				var scrollbarStartOpacity = 0;
				var xScrollStartPosition = 0;
				var yScrollStartPosition = 0;
				var currentTouches = 0;
				var scrollerNode = $(this).children(':first-child');
				var slideNodes;
				var slideNodeWidths;
				var slideNodeOuterWidths;
				var numberOfSlides = $(scrollerNode).children().not('script').length;
				var xScrollStarted = false;
				var lastChildOffset = 0;
				var isMouseDown = false;
				var currentSlider = undefined;
				var sliderStopLocation = 0;
				var infiniteSliderWidth;
				infiniteSliderOffset[sliderNumber] = 0;
				var shortContent = false;
				onChangeEventLastFired[sliderNumber] = -1;
				var isAutoSlideToggleOn = false;
				iosSliders[sliderNumber] = stageNode;
				isEventCleared[sliderNumber] = false;
				var currentEventNode;
				var intermediateChildOffset = 0;
				var tempInfiniteSliderOffset = 0;
				var preventXScroll = false;
				var snapOverride = false;
				var clickEvent = 'touchstart.iosSliderEvent click.iosSliderEvent';
				var scrollerWidth;
				var anchorEvents;
				var onclickEvents;
				var allScrollerNodeChildren;
				touchLocks[sliderNumber] = false;
				slideTimeouts[sliderNumber] = new Array();
				if(settings.scrollbarDrag) {
					settings.scrollbar = true;
					settings.scrollbarHide = false;
				}
				var $this = $(this);
				var data = $this.data('iosslider');	
				if(data != undefined) return true;

				if(parseInt($().jquery.split('.').join(''), 10) >= 14.2) {
					$(this).delegate('img', 'dragstart.iosSliderEvent', function(event) { event.preventDefault(); });	
				} else {
					$(this).find('img').bind('dragstart.iosSliderEvent', function(event) { event.preventDefault(); });
				}
		   		
				if(settings.infiniteSlider) {
					settings.scrollbar = false;
				}
				
				if(settings.infiniteSlider && (numberOfSlides == 1)) {
					settings.infiniteSlider = false;
				}
						
				if(settings.scrollbar) {
					
					if(settings.scrollbarContainer != '') {
						$(settings.scrollbarContainer).append("<div class = '" + scrollbarBlockClass + "'><div class = '" + scrollbarClass + "'></div></div>");
					} else {
						$(scrollerNode).parent().append("<div class = '" + scrollbarBlockClass + "'><div class = '" + scrollbarClass + "'></div></div>");
					}
				
				}
				
				if(!init()) return true;
				
				$(this).find('a').bind('mousedown', helpers.preventDrag);
				$(this).find("[onclick]").bind('click', helpers.preventDrag).each(function() {
						
					$(this).data('onclick', this.onclick);
				
				});
				
				var newChildOffset = helpers.calcActiveOffset(settings, helpers.getSliderOffset($(scrollerNode), 'x'), childrenOffsets, stageWidth, infiniteSliderOffset[sliderNumber], numberOfSlides, undefined, sliderNumber);
				var tempOffset = (newChildOffset + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;
				
				var args = new helpers.args('load', settings, scrollerNode, $(scrollerNode).children(':eq(' + tempOffset + ')'), tempOffset, tempOffset);
				$(stageNode).data('args', args);

				if(settings.onSliderLoaded != '') {

					settings.onSliderLoaded(args);
					
				}
				
				onChangeEventLastFired[sliderNumber] = tempOffset;

				function init() {
					
					helpers.autoSlidePause(sliderNumber);
					
					anchorEvents = $(scrollerNode).find('a');
					onclickEvents = $(scrollerNode).find('[onclick]');
					allScrollerNodeChildren = $(scrollerNode).find('*');
					
					$(stageNode).css('width', '');
					$(stageNode).css('height', '');
					$(scrollerNode).css('width', '');
					slideNodes = $(scrollerNode).children().not('script').get();
					slideNodeWidths = new Array();
					slideNodeOuterWidths = new Array();
					
					if(settings.responsiveSlides) {
						$(slideNodes).css('width', '');
					}
					
					sliderMax[sliderNumber] = 0;
					childrenOffsets = new Array();
					containerWidth = $(stageNode).parent().width();
					stageWidth = $(stageNode).outerWidth(true);
					//console.log(containerWidth,stageWidth, 'ios')
					if(settings.responsiveSlideContainer) {
						stageWidth = ($(stageNode).outerWidth(true) > containerWidth) ? containerWidth : $(stageNode).width();
					}

					//console.log('after',stageWidth)
					$(stageNode).css({
						position: settings.stageCSS.position,
						top: settings.stageCSS.top,
						left: settings.stageCSS.left,
						overflow: settings.stageCSS.overflow,
						zIndex: settings.stageCSS.zIndex,
						'webkitPerspective': 1000,
						'webkitBackfaceVisibility': 'hidden',
						'msTouchAction': 'pan-y',
						width: stageWidth
					});
					
					$(settings.unselectableSelector).css({
						cursor: 'default'
					});
						
					for(var j = 0; j < slideNodes.length; j++) {
						
						slideNodeWidths[j] = $(slideNodes[j]).width();

						slideNodeOuterWidths[j] = $(slideNodes[j]).outerWidth(true);

						//console.log('...',slideNodeWidths[j],'...',slideNodeOuterWidths[j]);

						var newWidth = slideNodeOuterWidths[j];
						
						if(settings.responsiveSlides) {

							if(slideNodeOuterWidths[j] > stageWidth) {
								
								newWidth = stageWidth + (slideNodeOuterWidths[j] - slideNodeWidths[j]) * -1;
								slideNodeWidths[j] = newWidth;
								slideNodeOuterWidths[j] = stageWidth;
								
							} else {

								newWidth = slideNodeWidths[j];
								
							}
							
							$(slideNodes[j]).css({
								width: newWidth
							});
					
						}
						
						$(slideNodes[j]).css({
							overflow: 'hidden',
							position: 'absolute'
						});
						
						childrenOffsets[j] = sliderMax[sliderNumber] * -1;
						
						sliderMax[sliderNumber] = sliderMax[sliderNumber] + newWidth + (slideNodeOuterWidths[j] - slideNodeWidths[j]);
					
					}
					
					if(settings.snapSlideCenter) {
						centeredSlideOffset = (stageWidth - slideNodeOuterWidths[0]) * 0.5;
						
						if(settings.responsiveSlides && (slideNodeOuterWidths[0] > stageWidth)) {
							centeredSlideOffset = 0;
						}
					}
					
					sliderAbsMax[sliderNumber] = sliderMax[sliderNumber] * 2;
					
					for(var j = 0; j < slideNodes.length; j++) {
						
						helpers.setSliderOffset($(slideNodes[j]), childrenOffsets[j] * -1 + sliderMax[sliderNumber] + centeredSlideOffset);
						
						childrenOffsets[j] = childrenOffsets[j] - sliderMax[sliderNumber];
					
					}
					
					if(!settings.infiniteSlider && !settings.snapSlideCenter) {
					
						for(var i = 0; i < childrenOffsets.length; i++) {
							
							if(childrenOffsets[i] <= ((sliderMax[sliderNumber] * 2 - stageWidth) * -1)) {
								break;
							}
							
							lastChildOffset = i;
							
						}
						
						childrenOffsets.splice(lastChildOffset + 1, childrenOffsets.length);
						childrenOffsets[childrenOffsets.length] = (sliderMax[sliderNumber] * 2 - stageWidth) * -1;
					
					}
					
					for(var i = 0; i < childrenOffsets.length; i++) {
						originalOffsets[i] = childrenOffsets[i];
					}
					
					if(isFirstInit) {
						
						iosSliderSettings[sliderNumber].startAtSlide = (iosSliderSettings[sliderNumber].startAtSlide > childrenOffsets.length) ? childrenOffsets.length : iosSliderSettings[sliderNumber].startAtSlide;
						if(settings.infiniteSlider) {
							iosSliderSettings[sliderNumber].startAtSlide = (iosSliderSettings[sliderNumber].startAtSlide - 1 + numberOfSlides)%numberOfSlides;
							activeChildOffsets[sliderNumber] = (iosSliderSettings[sliderNumber].startAtSlide);
						} else {
							iosSliderSettings[sliderNumber].startAtSlide = ((iosSliderSettings[sliderNumber].startAtSlide - 1) < 0) ? childrenOffsets.length-1 : iosSliderSettings[sliderNumber].startAtSlide;	
							activeChildOffsets[sliderNumber] = (iosSliderSettings[sliderNumber].startAtSlide-1);
						}
						activeChildInfOffsets[sliderNumber] = activeChildOffsets[sliderNumber];
					}
					
					sliderMin[sliderNumber] = sliderMax[sliderNumber] + centeredSlideOffset;

					$(scrollerNode).css({
						position: 'relative',
						cursor: grabOutCursor,
						'webkitPerspective': '0',
						'webkitBackfaceVisibility': 'hidden',
						width: sliderMax[sliderNumber] + 'px'
					});
					
					scrollerWidth = sliderMax[sliderNumber];
					sliderMax[sliderNumber] = sliderMax[sliderNumber] * 2 - stageWidth + centeredSlideOffset * 2;
					
					shortContent = (((scrollerWidth + centeredSlideOffset) < stageWidth) || (stageWidth == 0)) ? true : false;

					if(shortContent) {
						
						$(scrollerNode).css({
							cursor: 'default'
						});
						
					}
					
					containerHeight = $(stageNode).parent().outerHeight(true);
					stageHeight = $(stageNode).height();
					
					if(settings.responsiveSlideContainer) {
						stageHeight = (stageHeight > containerHeight) ? containerHeight : stageHeight;
					}
					
					$(stageNode).css({
						height: stageHeight
					});

					helpers.setSliderOffset(scrollerNode, childrenOffsets[activeChildOffsets[sliderNumber]]);
					
					if(settings.infiniteSlider && !shortContent) {
						
						var currentScrollOffset = helpers.getSliderOffset($(scrollerNode), 'x');
						var count = (infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides * -1;
						
						while(count < 0) {
							
							var lowSlideNumber = 0;
							var lowSlideOffset = helpers.getSliderOffset($(slideNodes[0]), 'x');
							$(slideNodes).each(function(i) {
								
								if(helpers.getSliderOffset(this, 'x') < lowSlideOffset) {
									lowSlideOffset = helpers.getSliderOffset(this, 'x');
									lowSlideNumber = i;
								}
								
							});
							
							var newOffset = sliderMin[sliderNumber] + scrollerWidth;
							helpers.setSliderOffset($(slideNodes)[lowSlideNumber], newOffset);
							
							sliderMin[sliderNumber] = childrenOffsets[1] * -1 + centeredSlideOffset;
							sliderMax[sliderNumber] = sliderMin[sliderNumber] + scrollerWidth - stageWidth;

							childrenOffsets.splice(0, 1);
							childrenOffsets.splice(childrenOffsets.length, 0, newOffset * -1 + centeredSlideOffset);

							count++;
							
						}
						
						while(((childrenOffsets[0] * -1 - scrollerWidth + centeredSlideOffset) > 0) && settings.snapSlideCenter && isFirstInit) {
							
							var highSlideNumber = 0;
							var highSlideOffset = helpers.getSliderOffset($(slideNodes[0]), 'x');
							$(slideNodes).each(function(i) {
								
								if(helpers.getSliderOffset(this, 'x') > highSlideOffset) {
									highSlideOffset = helpers.getSliderOffset(this, 'x');
									highSlideNumber = i;
								}
								
							});

							var newOffset = sliderMin[sliderNumber] - slideNodeOuterWidths[highSlideNumber];
							helpers.setSliderOffset($(slideNodes)[highSlideNumber], newOffset);
							
							childrenOffsets.splice(0, 0, newOffset * -1 + centeredSlideOffset);
							childrenOffsets.splice(childrenOffsets.length-1, 1);

							sliderMin[sliderNumber] = childrenOffsets[0] * -1 + centeredSlideOffset;
							sliderMax[sliderNumber] = sliderMin[sliderNumber] + scrollerWidth - stageWidth;

							infiniteSliderOffset[sliderNumber]--;
							activeChildOffsets[sliderNumber]++;
							
						}
						
						while(currentScrollOffset <= (sliderMax[sliderNumber] * -1)) {
							
							var lowSlideNumber = 0;
							var lowSlideOffset = helpers.getSliderOffset($(slideNodes[0]), 'x');
							$(slideNodes).each(function(i) {
								
								if(helpers.getSliderOffset(this, 'x') < lowSlideOffset) {
									lowSlideOffset = helpers.getSliderOffset(this, 'x');
									lowSlideNumber = i;
								}
								
							});
							
							var newOffset = sliderMin[sliderNumber] + scrollerWidth;
							helpers.setSliderOffset($(slideNodes)[lowSlideNumber], newOffset);	
							
							sliderMin[sliderNumber] = childrenOffsets[1] * -1 + centeredSlideOffset;
							sliderMax[sliderNumber] = sliderMin[sliderNumber] + scrollerWidth - stageWidth;

							childrenOffsets.splice(0, 1);
							childrenOffsets.splice(childrenOffsets.length, 0, newOffset * -1 + centeredSlideOffset);

							infiniteSliderOffset[sliderNumber]++;
							activeChildOffsets[sliderNumber]--;
							
						}
					
					}
					
					helpers.setSliderOffset(scrollerNode, childrenOffsets[activeChildOffsets[sliderNumber]]);
					
					helpers.updateBackfaceVisibility(slideNodes, sliderNumber, numberOfSlides, settings);
					
					if(!settings.desktopClickDrag) {
						
						$(scrollerNode).css({
							cursor: 'default'
						});
						
					}
					
					if(settings.scrollbar) {
						
						$('.' + scrollbarBlockClass).css({ 
							margin: settings.scrollbarMargin,
							overflow: 'hidden',
							display: 'none'
						});
						
						$('.' + scrollbarBlockClass + ' .' + scrollbarClass).css({ 
							border: settings.scrollbarBorder
						});
						
						scrollMargin = parseInt($('.' + scrollbarBlockClass).css('marginLeft')) + parseInt($('.' + scrollbarBlockClass).css('marginRight'));
						scrollBorder = parseInt($('.' + scrollbarBlockClass + ' .' + scrollbarClass).css('borderLeftWidth'), 10) + parseInt($('.' + scrollbarBlockClass + ' .' + scrollbarClass).css('borderRightWidth'), 10);
						scrollbarStageWidth = (settings.scrollbarContainer != '') ? $(settings.scrollbarContainer).width() : stageWidth;
						scrollbarWidth = (stageWidth / scrollerWidth) * (scrollbarStageWidth - scrollMargin);
		
						if(!settings.scrollbarHide) {
							scrollbarStartOpacity = settings.scrollbarOpacity;
						}
						
						$('.' + scrollbarBlockClass).css({ 
							position: 'absolute',
							left: 0,
							width: scrollbarStageWidth - scrollMargin + 'px',
							margin: settings.scrollbarMargin
						});
						
						if(settings.scrollbarLocation == 'top') {
							$('.' + scrollbarBlockClass).css('top', '0');
						} else {
							$('.' + scrollbarBlockClass).css('bottom', '0');
						}

						$('.' + scrollbarBlockClass + ' .' + scrollbarClass).css({ 
							borderRadius: settings.scrollbarBorderRadius,
							background: settings.scrollbarBackground,
							height: settings.scrollbarHeight,
							width: scrollbarWidth - scrollBorder + 'px',
							minWidth: settings.scrollbarHeight,
							border: settings.scrollbarBorder,
							'webkitPerspective': 1000,
							'webkitBackfaceVisibility': 'hidden',
							'position': 'relative',
							opacity: scrollbarStartOpacity,
							filter: 'alpha(opacity:' + (scrollbarStartOpacity * 100) + ')',
							boxShadow: settings.scrollbarShadow
						});
						
						helpers.setSliderOffset($('.' + scrollbarBlockClass + ' .' + scrollbarClass), Math.floor((childrenOffsets[activeChildOffsets[sliderNumber]] * -1 - sliderMin[sliderNumber] + centeredSlideOffset) / (sliderMax[sliderNumber] - sliderMin[sliderNumber] + centeredSlideOffset) * (scrollbarStageWidth - scrollMargin - scrollbarWidth)));
		
						$('.' + scrollbarBlockClass).css({
							display: 'block'
						});
						
						scrollbarNode = $('.' + scrollbarBlockClass + ' .' + scrollbarClass);
						scrollbarBlockNode = $('.' + scrollbarBlockClass);						
						
					}
					
					if(settings.scrollbarDrag && !shortContent) {
						$('.' + scrollbarBlockClass + ' .' + scrollbarClass).css({
							cursor: grabOutCursor
						});
					}
					
					if(settings.infiniteSlider) {
					
						infiniteSliderWidth = (sliderMax[sliderNumber] + stageWidth) / 3;
						
					}
					
					if(settings.navSlideSelector != '') {
								
						$(settings.navSlideSelector).each(function(j) {
						
							$(this).css({
								cursor: 'pointer'
							});
							
							$(this).unbind(clickEvent).bind(clickEvent, function(e) {
								
								if(e.type == 'touchstart') {
									$(this).unbind('click.iosSliderEvent');
								} else {
									$(this).unbind('touchstart.iosSliderEvent');
								}
								clickEvent = e.type + '.iosSliderEvent';

								helpers.changeSlide(j, scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings);
								
							});
						
						});
								
					}	
					
					if(settings.navPrevSelector != '') {
						
						$(settings.navPrevSelector).css({
							cursor: 'pointer'
						});
						
						$(settings.navPrevSelector).unbind(clickEvent).bind(clickEvent, function(e) {	
							
							if(e.type == 'touchstart') {
								$(this).unbind('click.iosSliderEvent');
							} else {
								$(this).unbind('touchstart.iosSliderEvent');
							}
							clickEvent = e.type + '.iosSliderEvent';

							var slide = (activeChildOffsets[sliderNumber] + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;
											
							if((slide > 0) || settings.infiniteSlider) {
								helpers.changeSlide(slide - 1, scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings);
							}
						});
					
					}
					
					if(settings.navNextSelector != '') {
						
						$(settings.navNextSelector).css({
							cursor: 'pointer'
						});
						
						$(settings.navNextSelector).unbind(clickEvent).bind(clickEvent, function(e) {
							
							if(e.type == 'touchstart') {
								$(this).unbind('click.iosSliderEvent');
							} else {
								$(this).unbind('touchstart.iosSliderEvent');
							}
							clickEvent = e.type + '.iosSliderEvent';
							
							var slide = (activeChildOffsets[sliderNumber] + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;
							
							if((slide < childrenOffsets.length-1) || settings.infiniteSlider) {
								helpers.changeSlide(slide + 1, scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings);
							}
						});
					
					}
					
					if(settings.autoSlide && !shortContent) {
						
						if(settings.autoSlideToggleSelector != '') {
						
							$(settings.autoSlideToggleSelector).css({
								cursor: 'pointer'
							});
							
							$(settings.autoSlideToggleSelector).unbind(clickEvent).bind(clickEvent, function(e) {
								
								if(e.type == 'touchstart') {
									$(this).unbind('click.iosSliderEvent');
								} else {
									$(this).unbind('touchstart.iosSliderEvent');
								}
								clickEvent = e.type + '.iosSliderEvent';
							
								if(!isAutoSlideToggleOn) {
								
									helpers.autoSlidePause(sliderNumber);
									isAutoSlideToggleOn = true;
									
									$(settings.autoSlideToggleSelector).addClass('on');
									
								} else {
									
									helpers.autoSlide(scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings);
									
									isAutoSlideToggleOn = false;
									
									$(settings.autoSlideToggleSelector).removeClass('on');
									
								}
							
							});
						
						}
					
					}
					
					helpers.autoSlide(scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings);

					$(stageNode).bind('mouseleave.iosSliderEvent', function() {

						if(isAutoSlideToggleOn) return true;
						
						helpers.autoSlide(scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings);
						
					});
					
					$(stageNode).bind('touchend.iosSliderEvent', function() {
						
						if(isAutoSlideToggleOn) return true;
						
						helpers.autoSlide(scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings);
					
					});

					if(settings.autoSlideHoverPause) {
						$(stageNode).bind('mouseenter.iosSliderEvent', function() {
							helpers.autoSlidePause(sliderNumber);
						});
					}
						
					$(stageNode).data('iosslider', {
						obj: $this,
						settings: settings,
						scrollerNode: scrollerNode,
						slideNodes: slideNodes,
						numberOfSlides: numberOfSlides,
						centeredSlideOffset: centeredSlideOffset,
						sliderNumber: sliderNumber,
						originalOffsets: originalOffsets,
						childrenOffsets: childrenOffsets,
						sliderMax: sliderMax[sliderNumber],
						scrollbarClass: scrollbarClass,
						scrollbarWidth: scrollbarWidth, 
						scrollbarStageWidth: scrollbarStageWidth,
						stageWidth: stageWidth, 
						scrollMargin: scrollMargin, 
						scrollBorder: scrollBorder, 
						infiniteSliderOffset: infiniteSliderOffset[sliderNumber], 
						infiniteSliderWidth: infiniteSliderWidth,
						slideNodeOuterWidths: slideNodeOuterWidths,
						shortContent: shortContent
					});
					
					isFirstInit = false;
					
					return true;
				
				}
				
				if(settings.scrollbarPaging && settings.scrollbar && !shortContent) {
					
					$(scrollbarBlockNode).css('cursor', 'pointer');
					
					$(scrollbarBlockNode).bind('click.iosSliderEvent', function(e) {
						
						if(this == e.target) {
							
							if(e.pageX > $(scrollbarNode).offset().left) {
								methods.nextPage(stageNode);
							} else {
								methods.prevPage(stageNode);
							}
							
						}
					
					});
					
				}
				
				if(iosSliderSettings[sliderNumber].responsiveSlides || iosSliderSettings[sliderNumber].responsiveSlideContainer) {
					
					var orientationEvent = supportsOrientationChange ? 'orientationchange' : 'resize';
					
					$(window).bind(orientationEvent + '.iosSliderEvent-' + sliderNumber, function() {

						if(!init()) return true;
						
						var args = $(stageNode).data('args');
				
						if(settings.onSliderResize != '') {
					    	settings.onSliderResize(args);
					    }
						
					});
					
				}
				
				if((settings.keyboardControls || settings.tabToAdvance) && !shortContent) {

					$(document).bind('keydown.iosSliderEvent', function(e) {
						
						if((!isIe7) && (!isIe8)) {
							var e = e.originalEvent;
						}
						
						if(e.target.nodeName == 'INPUT') return true;
						
						if(touchLocks[sliderNumber]) return true;
						
						if((e.keyCode == 37) && settings.keyboardControls) {
							
							e.preventDefault();
							
							var slide = (activeChildOffsets[sliderNumber] + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;

							if((slide > 0) || settings.infiniteSlider) {
								helpers.changeSlide(slide - 1, scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings);
							} 
								
						} else if(((e.keyCode == 39) && settings.keyboardControls) || ((e.keyCode == 9) && settings.tabToAdvance)) {
							
							e.preventDefault();
							
							var slide = (activeChildOffsets[sliderNumber] + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;
								
							if((slide < childrenOffsets.length-1) || settings.infiniteSlider) {
								helpers.changeSlide(slide + 1, scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, centeredSlideOffset, settings);
							}
								
						}
					
					});
					
				}

				if(isTouch || settings.desktopClickDrag) {
					
					var touchStartFlag = false;
					var touchEndFlag = false;
					var touchSelection = $(scrollerNode);
					var touchSelectionMove = $(scrollerNode);
					var preventDefault = null;
					var isUnselectable = false;
					
					if(settings.scrollbarDrag) {
					
						touchSelection = touchSelection.add(scrollbarNode);
						touchSelectionMove = touchSelectionMove.add(scrollbarBlockNode);

					}
					
					$(touchSelection).bind('mousedown.iosSliderEvent touchstart.iosSliderEvent', function(e) {
						
						//if scroll starts, unbind dom from slider touch override
						$(window).one('scroll.iosSliderEvent', function(e) { touchStartFlag = false; });
						
						if(touchStartFlag) return true;
						touchStartFlag = true;
						touchEndFlag = false;
						
						if(e.type == 'touchstart') {
							$(touchSelectionMove).unbind('mousedown.iosSliderEvent');
						} else {
							$(touchSelectionMove).unbind('touchstart.iosSliderEvent');
						}
						
						if(touchLocks[sliderNumber] || shortContent) {
							touchStartFlag = false;
							xScrollStarted = false;
							return true;
						}
						
						isUnselectable = helpers.isUnselectable(e.target, settings);
						
						if(isUnselectable) {
							touchStartFlag = false;
							xScrollStarted = false;
							return true;
						}
						
						currentEventNode = ($(this)[0] === $(scrollbarNode)[0]) ? scrollbarNode : scrollerNode;

						if((!isIe7) && (!isIe8)) {
							var e = e.originalEvent;
						}

						helpers.autoSlidePause(sliderNumber);
						
						allScrollerNodeChildren.unbind('.disableClick');
						
						if(e.type == 'touchstart') {
							
							eventX = e.touches[0].pageX;
							eventY = e.touches[0].pageY;
							
						} else {
						
							if (window.getSelection) {
								if (window.getSelection().empty) {
									window.getSelection().empty();
								} else if (window.getSelection().removeAllRanges) {
									window.getSelection().removeAllRanges();
								}
							} else if (document.selection) {
								if(isIe8) {
									try { document.selection.empty(); } catch(e) { /* absorb ie8 bug */ }
								} else {
									document.selection.empty();
								}
							}
							
							eventX = e.pageX;
							eventY = e.pageY;
							
							isMouseDown = true;
							currentSlider = scrollerNode;

							$(this).css({
								cursor: grabInCursor
							});

						}
						
						xCurrentScrollRate = new Array(0, 0);
						yCurrentScrollRate = new Array(0, 0);
						xScrollDistance = 0;
						xScrollStarted = false;
						
						for(var j = 0; j < scrollTimeouts.length; j++) {
							clearTimeout(scrollTimeouts[j]);
						}
						
						var scrollPosition = helpers.getSliderOffset(scrollerNode, 'x');

						if(scrollPosition > (sliderMin[sliderNumber] * -1 + centeredSlideOffset + scrollerWidth)) {
							
							scrollPosition = sliderMin[sliderNumber] * -1 + centeredSlideOffset + scrollerWidth;

							helpers.setSliderOffset($('.' + scrollbarClass), scrollPosition);
							
							$('.' + scrollbarClass).css({
								width: (scrollbarWidth - scrollBorder) + 'px'
							});
							
						} else if(scrollPosition < (sliderMax[sliderNumber] * -1)) {
						
							scrollPosition = sliderMax[sliderNumber] * -1;

							helpers.setSliderOffset($('.' + scrollbarClass), (scrollbarStageWidth - scrollMargin - scrollbarWidth));
							
							$('.' + scrollbarClass).css({
								width: (scrollbarWidth - scrollBorder) + 'px'
							});
							
						}
						
						var scrollbarSubtractor = ($(this)[0] === $(scrollbarNode)[0]) ? (sliderMin[sliderNumber]) : 0;
						
						xScrollStartPosition = (helpers.getSliderOffset(this, 'x') - eventX - scrollbarSubtractor) * -1;
						yScrollStartPosition = (helpers.getSliderOffset(this, 'y') - eventY) * -1;
						
						xCurrentScrollRate[1] = eventX;
						yCurrentScrollRate[1] = eventY;
						
						snapOverride = false;

					});
					
					$(document).bind('touchmove.iosSliderEvent mousemove.iosSliderEvent', function(e) {
						
						if((!isIe7) && (!isIe8)) {
							var e = e.originalEvent;
						}
						
						if(touchLocks[sliderNumber] || shortContent || isUnselectable || !touchStartFlag) return true;
						
						var edgeDegradation = 0;

						if(e.type == 'touchmove') {
						
							eventX = e.touches[0].pageX;
							eventY = e.touches[0].pageY;
							
						} else {
						
							if(window.getSelection) {
								if(window.getSelection().empty) {
									//window.getSelection().empty(); /* removed to enable input fields within the slider */
								} else if(window.getSelection().removeAllRanges) {
									window.getSelection().removeAllRanges();
								}
							} else if(document.selection) {
								if(isIe8) {
									try { document.selection.empty(); } catch(e) { /* absorb ie8 bug */ }
								} else {
									document.selection.empty();
								}
							}
						
							eventX = e.pageX;
							eventY = e.pageY;
							
							if(!isMouseDown) {
								return true;
							}
							
							if(!isIe) {
								if((typeof e.webkitMovementX != 'undefined' || typeof e.webkitMovementY != 'undefined') && e.webkitMovementY === 0 && e.webkitMovementX === 0) {
									return true;
								}
							}
							
						}
						
						xCurrentScrollRate[0] = xCurrentScrollRate[1];
						xCurrentScrollRate[1] = eventX;
						xScrollDistance = (xCurrentScrollRate[1] - xCurrentScrollRate[0]) / 2;
						
						yCurrentScrollRate[0] = yCurrentScrollRate[1];
						yCurrentScrollRate[1] = eventY;
						yScrollDistance = (yCurrentScrollRate[1] - yCurrentScrollRate[0]) / 2;

						if(!xScrollStarted) {

							var slide = (activeChildOffsets[sliderNumber] + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;
							var args = new helpers.args('start', settings, scrollerNode, $(scrollerNode).children(':eq(' + slide + ')'), slide, undefined);
							$(stageNode).data('args', args);

							if(settings.onSlideStart != '') {
								settings.onSlideStart(args);
							}
							
						}
						
						if(((yScrollDistance > settings.verticalSlideLockThreshold) || (yScrollDistance < (settings.verticalSlideLockThreshold * -1))) && (e.type == 'touchmove') && (!xScrollStarted)) {
						
							preventXScroll = true;
							
						}
						
						if(((xScrollDistance > settings.horizontalSlideLockThreshold) || (xScrollDistance < (settings.horizontalSlideLockThreshold * -1))) && (e.type == 'touchmove')) {
						
							e.preventDefault();
							
						}
						
						if(((xScrollDistance > settings.slideStartVelocityThreshold) || (xScrollDistance < (settings.slideStartVelocityThreshold * -1)))) {
						
							xScrollStarted = true;
						
						}
						
						if(xScrollStarted && !preventXScroll) {
							
							var scrollPosition = helpers.getSliderOffset(scrollerNode, 'x');
							var scrollbarSubtractor = ($(currentEventNode)[0] === $(scrollbarNode)[0]) ? (sliderMin[sliderNumber]) : centeredSlideOffset;
							var scrollbarMultiplier = ($(currentEventNode)[0] === $(scrollbarNode)[0]) ? ((sliderMin[sliderNumber] - sliderMax[sliderNumber] - centeredSlideOffset) / (scrollbarStageWidth - scrollMargin - scrollbarWidth)) : 1;
							var elasticPullResistance = ($(currentEventNode)[0] === $(scrollbarNode)[0]) ? settings.scrollbarElasticPullResistance : settings.elasticPullResistance;
							var snapCenteredSlideOffset = (settings.snapSlideCenter && ($(currentEventNode)[0] === $(scrollbarNode)[0])) ? 0 : centeredSlideOffset;
							var snapCenteredSlideOffsetScrollbar = (settings.snapSlideCenter && ($(currentEventNode)[0] === $(scrollbarNode)[0])) ? centeredSlideOffset : 0;

							if(e.type == 'touchmove') {
								if(currentTouches != e.touches.length) {
									xScrollStartPosition = (scrollPosition * -1) + eventX;
								}
								
								currentTouches = e.touches.length;
							}

							if(settings.infiniteSlider) {

								if(scrollPosition <= (sliderMax[sliderNumber] * -1)) {
									
									var scrollerWidth = $(scrollerNode).width();
									
									if(scrollPosition <= (sliderAbsMax[sliderNumber] * -1)) {

										var sum = originalOffsets[0] * -1;
										$(slideNodes).each(function(i) {
											
											helpers.setSliderOffset($(slideNodes)[i], sum + centeredSlideOffset);
											if(i < childrenOffsets.length) {
												childrenOffsets[i] = sum * -1;
											}
											sum = sum + slideNodeOuterWidths[i];
											
										});
										
										xScrollStartPosition = xScrollStartPosition - childrenOffsets[0] * -1;
										sliderMin[sliderNumber] = childrenOffsets[0] * -1 + centeredSlideOffset;
										sliderMax[sliderNumber] = sliderMin[sliderNumber] + scrollerWidth - stageWidth;
										infiniteSliderOffset[sliderNumber] = 0;
										
									} else {

										var lowSlideNumber = 0;
										var lowSlideOffset = helpers.getSliderOffset($(slideNodes[0]), 'x');
										$(slideNodes).each(function(i) {
											
											if(helpers.getSliderOffset(this, 'x') < lowSlideOffset) {
												lowSlideOffset = helpers.getSliderOffset(this, 'x');
												lowSlideNumber = i;
											}
											
										});

										var newOffset = sliderMin[sliderNumber] + scrollerWidth;
										helpers.setSliderOffset($(slideNodes)[lowSlideNumber], newOffset);
										
										sliderMin[sliderNumber] = childrenOffsets[1] * -1 + centeredSlideOffset;
										sliderMax[sliderNumber] = sliderMin[sliderNumber] + scrollerWidth - stageWidth;

										childrenOffsets.splice(0, 1);
										childrenOffsets.splice(childrenOffsets.length, 0, newOffset * -1 + centeredSlideOffset);

										infiniteSliderOffset[sliderNumber]++;
										
									}
									
								}
								
								if((scrollPosition >= (sliderMin[sliderNumber] * -1)) || (scrollPosition >= 0)) {
		
									var scrollerWidth = $(scrollerNode).width();
									
									if(scrollPosition >= 0) {

										var sum = originalOffsets[0] * -1;
										$(slideNodes).each(function(i) {
											
											helpers.setSliderOffset($(slideNodes)[i], sum + centeredSlideOffset);
											if(i < childrenOffsets.length) {
												childrenOffsets[i] = sum * -1;
											}
											sum = sum + slideNodeOuterWidths[i];
											
										});
										
										xScrollStartPosition = xScrollStartPosition + childrenOffsets[0] * -1;
										sliderMin[sliderNumber] = childrenOffsets[0] * -1 + centeredSlideOffset;
										sliderMax[sliderNumber] = sliderMin[sliderNumber] + scrollerWidth - stageWidth;
										infiniteSliderOffset[sliderNumber] = numberOfSlides;
										
										while(((childrenOffsets[0] * -1 - scrollerWidth + centeredSlideOffset) > 0)) {
				
											var highSlideNumber = 0;
											var highSlideOffset = helpers.getSliderOffset($(slideNodes[0]), 'x');
											$(slideNodes).each(function(i) {
												
												if(helpers.getSliderOffset(this, 'x') > highSlideOffset) {
													highSlideOffset = helpers.getSliderOffset(this, 'x');
													highSlideNumber = i;
												}
												
											});
				
											var newOffset = sliderMin[sliderNumber] - slideNodeOuterWidths[highSlideNumber];
											helpers.setSliderOffset($(slideNodes)[highSlideNumber], newOffset);
											
											childrenOffsets.splice(0, 0, newOffset * -1 + centeredSlideOffset);
											childrenOffsets.splice(childrenOffsets.length-1, 1);
				
											sliderMin[sliderNumber] = childrenOffsets[0] * -1 + centeredSlideOffset;
											sliderMax[sliderNumber] = sliderMin[sliderNumber] + scrollerWidth - stageWidth;
				
											infiniteSliderOffset[sliderNumber]--;
											activeChildOffsets[sliderNumber]++;
											
										}

									} else {

										var highSlideNumber = 0;
										var highSlideOffset = helpers.getSliderOffset($(slideNodes[0]), 'x');
										$(slideNodes).each(function(i) {
											
											if(helpers.getSliderOffset(this, 'x') > highSlideOffset) {
												highSlideOffset = helpers.getSliderOffset(this, 'x');
												highSlideNumber = i;
											}
											
										});
										
										var newOffset = sliderMin[sliderNumber] - slideNodeOuterWidths[highSlideNumber];
										helpers.setSliderOffset($(slideNodes)[highSlideNumber], newOffset);									

										childrenOffsets.splice(0, 0, newOffset * -1 + centeredSlideOffset);
										childrenOffsets.splice(childrenOffsets.length-1, 1);

										sliderMin[sliderNumber] = childrenOffsets[0] * -1 + centeredSlideOffset;
										sliderMax[sliderNumber] = sliderMin[sliderNumber] + scrollerWidth - stageWidth;

										infiniteSliderOffset[sliderNumber]--;

									}
								
								}
								
							} else {
								
								var scrollerWidth = $(scrollerNode).width();
								
								if(scrollPosition > (sliderMin[sliderNumber] * -1 + centeredSlideOffset)) {

									edgeDegradation = (sliderMin[sliderNumber] + ((xScrollStartPosition - scrollbarSubtractor - eventX + snapCenteredSlideOffset) * -1 * scrollbarMultiplier) - scrollbarSubtractor) * elasticPullResistance * -1 / scrollbarMultiplier;
									
								}
								
								if(scrollPosition < (sliderMax[sliderNumber] * -1)) {
									
									edgeDegradation = (sliderMax[sliderNumber] + snapCenteredSlideOffsetScrollbar + ((xScrollStartPosition - scrollbarSubtractor - eventX) * -1 * scrollbarMultiplier) - scrollbarSubtractor) * elasticPullResistance * -1 / scrollbarMultiplier;
										
								}
							
							}
							
							helpers.setSliderOffset(scrollerNode, ((xScrollStartPosition - scrollbarSubtractor - eventX - edgeDegradation) * -1 * scrollbarMultiplier) - scrollbarSubtractor + snapCenteredSlideOffsetScrollbar);
							
							if(settings.scrollbar) {
								
								helpers.showScrollbar(settings, scrollbarClass);

								scrollbarDistance = Math.floor((xScrollStartPosition - eventX - edgeDegradation - sliderMin[sliderNumber] + snapCenteredSlideOffset) / (sliderMax[sliderNumber] - sliderMin[sliderNumber] + centeredSlideOffset) * (scrollbarStageWidth - scrollMargin - scrollbarWidth) * scrollbarMultiplier);

								var width = scrollbarWidth;
								
								if(scrollbarDistance <= 0) {

									width = scrollbarWidth - scrollBorder - (scrollbarDistance * -1);
									
									helpers.setSliderOffset($('.' + scrollbarClass), 0);
									
									$('.' + scrollbarClass).css({
										width: width + 'px'
									});
									
								} else if(scrollbarDistance >= (scrollbarStageWidth - scrollMargin - scrollBorder - scrollbarWidth)) {

									width = scrollbarStageWidth - scrollMargin - scrollBorder - scrollbarDistance;
									
									helpers.setSliderOffset($('.' + scrollbarClass), scrollbarDistance);
									
									$('.' + scrollbarClass).css({
										width: width + 'px'
									});
									
								} else {
								
									helpers.setSliderOffset($('.' + scrollbarClass), scrollbarDistance);
									
								}
								
							}
							
							if(e.type == 'touchmove') {
								lastTouch = e.touches[0].pageX;
							}
							
							var slideChanged = false;
							var newChildOffset = helpers.calcActiveOffset(settings, (xScrollStartPosition - eventX - edgeDegradation) * -1, childrenOffsets, stageWidth, infiniteSliderOffset[sliderNumber], numberOfSlides, undefined, sliderNumber);
							var tempOffset = (newChildOffset + infiniteSliderOffset[sliderNumber] + numberOfSlides)%numberOfSlides;
							
							if(settings.infiniteSlider) {
								
								if(tempOffset != activeChildInfOffsets[sliderNumber]) {
									slideChanged = true;
								}
									
							} else {
							
								if(newChildOffset != activeChildOffsets[sliderNumber]) {
									slideChanged = true;
								}
							
							}

							if(slideChanged) {
								
								activeChildOffsets[sliderNumber] = newChildOffset;
								activeChildInfOffsets[sliderNumber] = tempOffset;
								snapOverride = true;
								
								var args = new helpers.args('change', settings, scrollerNode, $(scrollerNode).children(':eq(' + tempOffset + ')'), tempOffset, tempOffset);
								$(stageNode).data('args', args);
								
								if(settings.onSlideChange != '') {
									settings.onSlideChange(args);
								}
								
								helpers.updateBackfaceVisibility(slideNodes, sliderNumber, numberOfSlides, settings);
								
							}
							
						}
						
					});
					
					var eventObject = $(window);
					
					if(isIe8 || isIe7) {
						var eventObject = $(document); 
					}
					
					$(touchSelection).bind('touchcancel.iosSliderEvent touchend.iosSliderEvent', function(e) {
						
						var e = e.originalEvent;
						
						if(touchEndFlag) return false;
						touchEndFlag = true;
						
						if(touchLocks[sliderNumber] || shortContent) return true;
						
						if(isUnselectable) return true;
						
						if(e.touches.length != 0) {
							
							for(var j = 0; j < e.touches.length; j++) {
								
								if(e.touches[j].pageX == lastTouch) {
									helpers.slowScrollHorizontal(scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, xScrollDistance, yScrollDistance, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, currentEventNode, snapOverride, centeredSlideOffset, settings);
								}
								
							}
							
						} else {
							
							helpers.slowScrollHorizontal(scrollerNode, slideNodes, scrollTimeouts, scrollbarClass, xScrollDistance, yScrollDistance, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, currentEventNode, snapOverride, centeredSlideOffset, settings);
							
						}
						
						preventXScroll = false;
						touchStartFlag = false;
						
						return true;
						
					});
						
					$(eventObject).bind('mouseup.iosSliderEvent-' + sliderNumber, function(e) {
						
						if(xScrollStarted) {
							anchorEvents.unbind('click.disableClick').bind('click.disableClick', helpers.preventClick);
						} else {
							anchorEvents.unbind('click.disableClick').bind('click.disableClick', helpers.enableClick);
						}
						
						onclickEvents.each(function() {
							
							this.onclick = function(event) {
								if(xScrollStarted) { 
									return false;
								}
								
								if($(this).data('onclick')) $(this).data('onclick').call(this, event || window.event);
							}
							
							this.onclick = $(this).data('onclick');
							
						});
						
						if(parseFloat($().jquery) >= 1.8) {
							
							allScrollerNodeChildren.each(function() {
									
								var clickObject = $._data(this, 'events');
								
								if(clickObject != undefined) {
									if(clickObject.click != undefined) {

										if(clickObject.click[0].namespace != 'iosSliderEvent') {
											
											if(!xScrollStarted) { 
												return false;
											}

											$(this).one('click.disableClick', helpers.preventClick);
										    var handlers = $._data(this, 'events').click;
										    var handler = handlers.pop();
										    handlers.splice(0, 0, handler);
											
										}
										
									}
								}
								
							});
						
						} else if(parseFloat($().jquery) >= 1.6) {
						
							allScrollerNodeChildren.each(function() {
									
								var clickObject = $(this).data('events');
								
								if(clickObject != undefined) {
									if(clickObject.click != undefined) {

										if(clickObject.click[0].namespace != 'iosSliderEvent') {
											
											if(!xScrollStarted) { 
												return false;
											}
										
											$(this).one('click.disableClick', helpers.preventClick);
										    var handlers = $(this).data('events').click;
										    var handler = handlers.pop();
										    handlers.splice(0, 0, handler);
											
										}
										
									}
								}
								
							});
						
						}
						
						if(!isEventCleared[sliderNumber]) {
						
							if(shortContent) return true;
							
							if(settings.desktopClickDrag) {
								$(scrollerNode).css({
									cursor: grabOutCursor
								});
							}
							
							if(settings.scrollbarDrag) {
								$(scrollbarNode).css({
									cursor: grabOutCursor
								});
							}
							
							isMouseDown = false;
							
							if(currentSlider == undefined) {
								return true;
							}

							helpers.slowScrollHorizontal(currentSlider, slideNodes, scrollTimeouts, scrollbarClass, xScrollDistance, yScrollDistance, scrollbarWidth, stageWidth, scrollbarStageWidth, scrollMargin, scrollBorder, originalOffsets, childrenOffsets, slideNodeOuterWidths, sliderNumber, infiniteSliderWidth, numberOfSlides, currentEventNode, snapOverride, centeredSlideOffset, settings);
							
							currentSlider = undefined;
						
						}
						
						preventXScroll = false;
						touchStartFlag = false;
						
					});
				
				}
				
			});	
			
		},
		
		destroy: function(clearStyle, node) {
			
			if(node == undefined) {
				node = this;
			}
			
			return $(node).each(function() {
			
				var $this = $(this);
				var data = $this.data('iosslider');
				if(data == undefined) return false;
				
				if(clearStyle == undefined) {
		    		clearStyle = true;
		    	}
		    	
	    		helpers.autoSlidePause(data.sliderNumber);
		    	isEventCleared[data.sliderNumber] = true;
		    	$(window).unbind('.iosSliderEvent-' + data.sliderNumber);
		    	$(document).unbind('.iosSliderEvent-' + data.sliderNumber);
		    	$(document).unbind('keydown.iosSliderEvent');
		    	$(this).unbind('.iosSliderEvent');
	    		$(this).children(':first-child').unbind('.iosSliderEvent');
	    		$(this).children(':first-child').children().unbind('.iosSliderEvent');
		    	$(data.settings.scrollbarBlockNode).unbind('.iosSliderEvent');
		    	
		    	if(clearStyle) {
	    			$(this).attr('style', '');
		    		$(this).children(':first-child').attr('style', '');
		    		$(this).children(':first-child').children().attr('style', '');

		    		$(data.settings.navSlideSelector).attr('style', '');
		    		$(data.settings.navPrevSelector).attr('style', '');
		    		$(data.settings.navNextSelector).attr('style', '');
		    		$(data.settings.autoSlideToggleSelector).attr('style', '');
		    		$(data.settings.unselectableSelector).attr('style', '');
	    		}
	    		
	    		if(data.settings.scrollbar) {
	    			$('.scrollbarBlock' + data.sliderNumber).remove();
	    		}
	    		
	    		var scrollTimeouts = slideTimeouts[data.sliderNumber];
	    		
	    		for(var i = 0; i < scrollTimeouts.length; i++) {
					clearTimeout(scrollTimeouts[i]);
				}
	    		
	    		$this.removeData('iosslider');
	    		$this.removeData('args');
		    	
			});
		
		},
		
		update: function(node) {
			
			if(node == undefined) {
				node = this;
			}
			
			return $(node).each(function() {

				var $this = $(this);
				var data = $this.data('iosslider');
				if(data == undefined) return false;

				var curSlideNum = 1;
				if (typeof $this.data('args') !== 'undefined') {
					curSlideNum = $this.data('args').currentSlideNumber;
				}
				data.settings.startAtSlide = curSlideNum;
				
				methods.destroy(false, this);
				
				if((data.numberOfSlides != 1) && data.settings.infiniteSlider) {
				 	data.settings.startAtSlide = (activeChildOffsets[data.sliderNumber] + 1 + infiniteSliderOffset[data.sliderNumber] + data.numberOfSlides)%data.numberOfSlides;
				}

				methods.init(data.settings, this);
				
				var args = new helpers.args('update', data.settings, data.scrollerNode, $(data.scrollerNode).children(':eq(' + (data.settings.startAtSlide - 1) + ')'), data.settings.startAtSlide - 1, data.settings.startAtSlide - 1);
				$(data.stageNode).data('args', args);
				
				if(data.settings.onSliderUpdate != '') {
			    	data.settings.onSliderUpdate(args);
			    }
		    	
			});
		
		},
		
		addSlide: function(slideNode, slidePosition) {

			return this.each(function() {
				
				var $this = $(this);
				var data = $this.data('iosslider');
				if(data == undefined) return false;
				
				if($(data.scrollerNode).children().length == 0) {
				
					$(data.scrollerNode).append(slideNode);
					$this.data('args').currentSlideNumber = 1;
					
				} else if(!data.settings.infiniteSlider) {
				
					if(slidePosition <= data.numberOfSlides) {
						$(data.scrollerNode).children(':eq(' + (slidePosition - 1) + ')').before(slideNode);
					} else {
						$(data.scrollerNode).children(':eq(' + (slidePosition - 2) + ')').after(slideNode);
					}
					
					if($this.data('args').currentSlideNumber >= slidePosition) {
						$this.data('args').currentSlideNumber++;
					}
					
				} else {
					
					if(slidePosition == 1) {
						$(data.scrollerNode).children(':eq(0)').before(slideNode);
					} else {
						$(data.scrollerNode).children(':eq(' + (slidePosition - 2) + ')').after(slideNode);
					}
					
					if((infiniteSliderOffset[data.sliderNumber] < -1) && (true)) {
						activeChildOffsets[data.sliderNumber]--;
					}
					
					if($this.data('args').currentSlideNumber >= slidePosition) {
						activeChildOffsets[data.sliderNumber]++;
					}
					
				}
					
				$this.data('iosslider').numberOfSlides++;
				
				methods.update(this);
			
			});
		
		},
		
		removeSlide: function(slideNumber) {
		
			return this.each(function() {
			
				var $this = $(this);
				var data = $this.data('iosslider');
				if(data == undefined) return false;

				$(data.scrollerNode).children(':eq(' + (slideNumber - 1) + ')').remove();
				if(activeChildOffsets[data.sliderNumber] > (slideNumber - 1)) {
					activeChildOffsets[data.sliderNumber]--;
				}
				
				$this.data('iosslider').numberOfSlides--;

				methods.update(this);
			
			});
		
		},
		
		goToSlide: function(slide, duration, node) {
			
			if(node == undefined) {
				node = this;
			}
			
			return $(node).each(function() {
					
				var $this = $(this);
				var data = $this.data('iosslider');
				
				if((data == undefined) || data.shortContent) return false;
				
				slide = (slide > data.childrenOffsets.length) ? data.childrenOffsets.length - 1 : slide - 1;

				if(duration != undefined)
					data.settings.autoSlideTransTimer = duration;
				
				helpers.changeSlide(slide, $(data.scrollerNode), $(data.slideNodes), slideTimeouts[data.sliderNumber], data.scrollbarClass, data.scrollbarWidth, data.stageWidth, data.scrollbarStageWidth, data.scrollMargin, data.scrollBorder, data.originalOffsets, data.childrenOffsets, data.slideNodeOuterWidths, data.sliderNumber, data.infiniteSliderWidth, data.numberOfSlides, data.centeredSlideOffset, data.settings);

			});
			
		},
		
		prevSlide: function(duration) {
			
			return this.each(function() {
					
				var $this = $(this);
				var data = $this.data('iosslider');
				if((data == undefined) || data.shortContent) return false;
				
				var slide = (activeChildOffsets[data.sliderNumber] + infiniteSliderOffset[data.sliderNumber] + data.numberOfSlides)%data.numberOfSlides;
				
				if(duration != undefined)
					data.settings.autoSlideTransTimer = duration;
				
				if((slide > 0) || data.settings.infiniteSlider) {
					helpers.changeSlide(slide - 1, $(data.scrollerNode), $(data.slideNodes), slideTimeouts[data.sliderNumber], data.scrollbarClass, data.scrollbarWidth, data.stageWidth, data.scrollbarStageWidth, data.scrollMargin, data.scrollBorder, data.originalOffsets, data.childrenOffsets, data.slideNodeOuterWidths, data.sliderNumber, data.infiniteSliderWidth, data.numberOfSlides, data.centeredSlideOffset, data.settings);
				}
				
				activeChildOffsets[data.sliderNumber] = slide;

			});
			
		},
		
		nextSlide: function(duration) {
			
			return this.each(function() {
					
				var $this = $(this);
				var data = $this.data('iosslider');
				if((data == undefined) || data.shortContent) return false;
				
				var slide = (activeChildOffsets[data.sliderNumber] + infiniteSliderOffset[data.sliderNumber] + data.numberOfSlides)%data.numberOfSlides;
				
				if(duration != undefined)
					data.settings.autoSlideTransTimer = duration;
				
				if((slide < data.childrenOffsets.length-1) || data.settings.infiniteSlider) {
					helpers.changeSlide(slide + 1, $(data.scrollerNode), $(data.slideNodes), slideTimeouts[data.sliderNumber], data.scrollbarClass, data.scrollbarWidth, data.stageWidth, data.scrollbarStageWidth, data.scrollMargin, data.scrollBorder, data.originalOffsets, data.childrenOffsets, data.slideNodeOuterWidths, data.sliderNumber, data.infiniteSliderWidth, data.numberOfSlides, data.centeredSlideOffset, data.settings);
				}
				
				activeChildOffsets[data.sliderNumber] = slide;

			});
			
		},
		
		prevPage: function(duration, node) {
			
			if(node == undefined) {
				node = this;
			}
			
			return $(node).each(function() {

				var $this = $(this);
				var data = $this.data('iosslider');
				if(data == undefined) return false;
				
				var newOffset = helpers.getSliderOffset(data.scrollerNode, 'x') + data.stageWidth;
				
				if(duration != undefined)
					data.settings.autoSlideTransTimer = duration;
				
				helpers.changeOffset(newOffset, $(data.scrollerNode), $(data.slideNodes), slideTimeouts[data.sliderNumber], data.scrollbarClass, data.scrollbarWidth, data.stageWidth, data.scrollbarStageWidth, data.scrollMargin, data.scrollBorder, data.originalOffsets, data.childrenOffsets, data.slideNodeOuterWidths, data.sliderNumber, data.infiniteSliderWidth, data.numberOfSlides, data.centeredSlideOffset, data.settings);
			
			});
		
		},
		
		nextPage: function(duration, node) {
			
			if(node == undefined) {
				node = this;
			}
			
			return $(node).each(function() {

				var $this = $(this);
				var data = $this.data('iosslider');
				if(data == undefined) return false;
				
				var newOffset = helpers.getSliderOffset(data.scrollerNode, 'x') - data.stageWidth;
				
				if(duration != undefined)
					data.settings.autoSlideTransTimer = duration;
				
				helpers.changeOffset(newOffset, $(data.scrollerNode), $(data.slideNodes), slideTimeouts[data.sliderNumber], data.scrollbarClass, data.scrollbarWidth, data.stageWidth, data.scrollbarStageWidth, data.scrollMargin, data.scrollBorder, data.originalOffsets, data.childrenOffsets, data.slideNodeOuterWidths, data.sliderNumber, data.infiniteSliderWidth, data.numberOfSlides, data.centeredSlideOffset, data.settings);
			
			});
		
		},
		
		lock: function() {
			
			return this.each(function() {
			
				var $this = $(this);
				var data = $this.data('iosslider');
				if((data == undefined) || data.shortContent) return false;
				
				$(data.scrollerNode).css({
					cursor: 'default'
				});
				touchLocks[data.sliderNumber] = true;
			
			});
			
		},
		
		unlock: function() {
		
			return this.each(function() {

				var $this = $(this);
				var data = $this.data('iosslider');
				if((data == undefined) || data.shortContent) return false;
			
				$(data.scrollerNode).css({
					cursor: grabOutCursor
				});
				touchLocks[data.sliderNumber] = false;
			
			});
		
		},
		
		getData: function() {
		
			return this.each(function() {
			
				var $this = $(this);
				var data = $this.data('iosslider');
				if((data == undefined) || data.shortContent) return false;
				
				return data;
			
			});	
		
		},
		
		autoSlidePause: function() {
			
			return this.each(function() {
			
				var $this = $(this);
				var data = $this.data('iosslider');
				if((data == undefined) || data.shortContent) return false;

				iosSliderSettings[data.sliderNumber].autoSlide = false;
				
				helpers.autoSlidePause(data.sliderNumber);
				
				return data;
			
			});	
		
		},
		
		autoSlidePlay: function() {
			
			return this.each(function() {
			
				var $this = $(this);
				var data = $this.data('iosslider');
				if((data == undefined) || data.shortContent) return false;
				
				iosSliderSettings[data.sliderNumber].autoSlide = true;
				
				helpers.autoSlide($(data.scrollerNode), $(data.slideNodes), slideTimeouts[data.sliderNumber], data.scrollbarClass, data.scrollbarWidth, data.stageWidth, data.scrollbarStageWidth, data.scrollMargin, data.scrollBorder, data.originalOffsets, data.childrenOffsets, data.slideNodeOuterWidths, data.sliderNumber, data.infiniteSliderWidth, data.numberOfSlides, data.centeredSlideOffset, data.settings);
				
				return data;
			
			});	
			
		}
	
	}
	
	/* public functions */
	$.fn.iosSlider = function(method) {

		if(methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('invalid method call!');
		}
	
    };

}) (jQuery);



/**
 * jQuery.fastClick.js
 *
 * Work around the 300ms delay for the click event in some mobile browsers.
 *
 * Code based on <http://code.google.com/mobile/articles/fast_buttons.html>
 *
 * @usage
 * $('button').fastClick(function() {alert('clicked!');});
 *
 * @license MIT
 * @author Dave Hulbert (dave1010)
 * @version 1.0.0 2013-01-17
 */

/*global document, window, jQuery, Math */

(function($) {

$.fn.fastClick = function(handler) {
	return $(this).each(function(){
		$.FastButton($(this)[0], handler);
	});
};

$.fn.unfastClick = function(handler) {
	return $(this).each(function(){
		$(this).trigger('removeFastClick');
	});
};

$.FastButton = function(element, handler) {
	var startX, startY;

	var reset = function() {
		$(element).unbind('touchend');
		$("body").unbind('touchmove.fastClick');
	};

	var onClick = function(event) {
		event.stopPropagation();
		reset();
		handler.call(this, event);

		if (event.type === 'touchend') {
			$.clickbuster.preventGhostClick(startX, startY);
		}
	};

	var onTouchMove = function(event) {
		if (Math.abs(event.originalEvent.touches[0].clientX - startX) > 10 ||
			Math.abs(event.originalEvent.touches[0].clientY - startY) > 10) {
			reset();
		}
	};

	 var resetFastButton = function(event) {
		$(element).unbind({
				touchstart: onTouchStart,
				click: onClick,
				removeFastClick : resetFastButton
			});
	 };

	var onTouchStart = function(event) {
		event.stopPropagation();

		$(element).bind('touchend', onClick);
		$("body").bind('touchmove.fastClick', onTouchMove);

		startX = event.originalEvent.touches[0].clientX;
		startY = event.originalEvent.touches[0].clientY;
	};

	$(element).bind({
		touchstart: onTouchStart,
		click: onClick,
		removeFastClick : resetFastButton
	});
};

$.clickbuster = {
	coordinates: [],

	preventGhostClick: function(x, y) {
		$.clickbuster.coordinates.push(x, y);
		window.setTimeout($.clickbuster.pop, 2500);
	},

	pop: function() {
		$.clickbuster.coordinates.splice(0, 2);
	},

	onClick: function(event) {
		var x, y, i;
		for (i = 0; i < $.clickbuster.coordinates.length; i += 2) {
			x = $.clickbuster.coordinates[i];
			y = $.clickbuster.coordinates[i + 1];
			if (Math.abs(event.clientX - x) < 25 && Math.abs(event.clientY - y) < 25) {
				event.stopPropagation();
				event.preventDefault();
			}
		}
	}
};

$(function(){
	if (document.addEventListener){
		document.addEventListener('click', $.clickbuster.onClick, true);
	} else if (document.attachEvent){
		// for IE 7/8
		document.attachEvent('onclick', $.clickbuster.onClick);
	}
});

}(jQuery));


/*
CSS Browser Selector v0.4.0 (Nov 02, 2010)
Rafael Lima (http://rafael.adm.br)
http://rafael.adm.br/css_browser_selector
License: http://creativecommons.org/licenses/by/2.5/
Contributors: http://rafael.adm.br/css_browser_selector#contributors

v0.5.0 2011-08-24
andrew relkin

modified, now detects:
any version of Firefox
more versions of Windows (Win8, Win7, Vista, XP, Win2k)
more versions of IE under unique conditions
more detailed support for Opera
if "no-js" in HTML class: removes and replaces with "js" (<html class="no-js">)

identifies
	browsers: Firefox; IE; Opera; Safari; Chrome, Konqueror, Iron
	browser versions: (most importantly: ie6, ie7, ie8, ie9)
	rendering engines: Webkit; Mozilla; Gecko
	platforms/OSes: Mac; Win: Win7, Vista, XP, Win2k; FreeBSD; Linux/x11 
	devices: Ipod; Ipad; Iphone; WebTV; Blackberry; Android; J2me; mobile(generic)
	enabled technology: JS

v0.6.3 2014-03-06
@silasrm <silasrm@gmail.com>
    - Added support to IE11 
        @see http://msdn.microsoft.com/en-us/library/ie/hh869301(v=vs.85).aspx
        @see http://msdn.microsoft.com/en-us/library/ie/bg182625(v=vs.85).aspx
*/

showLog=false;
function log(m) {if ( window.console && showLog ) {console.log(m); }  }

function css_browser_selector(u) {
	var	uaInfo = {},
		screens = [320, 360, 384, 375, 390, 400, 412, 414, 432, 480, 600, 640, 720, 768, 800, 1024, 1152, 1280, 1440, 1680, 1920, 2560],
		allScreens = screens.length,
		ua=u.toLowerCase(),
		is=function(t) { return RegExp(t,"i").test(ua);  },
		version = function(p,n) 
			{ 
			n=n.replace(".","_"); var i = n.indexOf('_'),  ver=""; 
			while (i>0) {ver += " "+ p+n.substring(0,i);i = n.indexOf('_', i+1);} 
			ver += " "+p+n; return ver; 
			},
		g='gecko',
		w='webkit',
		c='chrome',
		f='firefox',
		s='safari',
		o='opera',
		m='mobile',
		a='android',
		bb='blackberry',
		lang='lang_',
		dv='device_',
		html=document.documentElement,
		b=	[
		
			// browser
			((!(/opera|webtv/i.test(ua))&&/msie\s(\d+)/.test(ua)||(/trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.test(ua))))?('ie ie'+(/trident\/4\.0/.test(ua) ? '8' : RegExp.$1 == '11.0'?'11':RegExp.$1))
			:is('firefox/')?g+ " " + f+(/firefox\/((\d+)(\.(\d+))(\.\d+)*)/.test(ua)?' '+f+RegExp.$2 + ' '+f+RegExp.$2+"_"+RegExp.$4:'')	
			:is('gecko/')?g
			:is('opera')?o+(/version\/((\d+)(\.(\d+))(\.\d+)*)/.test(ua)?' '+o+RegExp.$2 + ' '+o+RegExp.$2+"_"+RegExp.$4 : (/opera(\s|\/)(\d+)\.(\d+)/.test(ua)?' '+o+RegExp.$2+" "+o+RegExp.$2+"_"+RegExp.$3:''))
			:is('konqueror')?'konqueror'
	
			:is('blackberry') ? 
				( bb + 
					( /Version\/(\d+)(\.(\d+)+)/i.test(ua)
						? " " + bb+ RegExp.$1 + " "+bb+ RegExp.$1+RegExp.$2.replace('.','_')
						: (/Blackberry ?(([0-9]+)([a-z]?))[\/|;]/gi.test(ua) 
							? ' ' +bb+RegExp.$2 + (RegExp.$3?' ' +bb+RegExp.$2+RegExp.$3:'')
							: '')
					)
				) // blackberry
	
			:is('android') ? 
				(  a +
					( /Version\/(\d+)(\.(\d+))+/i.test(ua)
						? " " + a+ RegExp.$1 + " "+a+ RegExp.$1+RegExp.$2.replace('.','_')
						: '')
					+ (/Android (.+); (.+) Build/i.test(ua)
						? ' '+dv+( (RegExp.$2).replace(/ /g,"_") ).replace(/-/g,"_")
						:''	)
				) //android
	
			:is('chrome')?w+   ' '+c+(/chrome\/((\d+)(\.(\d+))(\.\d+)*)/.test(ua)?' '+c+RegExp.$2 +((RegExp.$4>0) ? ' '+c+RegExp.$2+"_"+RegExp.$4:''):'')	
			
			:is('iron')?w+' iron'
			
			:is('applewebkit/') ? 
				( w+ ' '+ s + 
					( /version\/((\d+)(\.(\d+))(\.\d+)*)/.test(ua)
						?  ' '+ s +RegExp.$2 + " "+s+ RegExp.$2+RegExp.$3.replace('.','_')
						:  ( / Safari\/(\d+)/i.test(ua) 
							? 
							( (RegExp.$1=="419" || RegExp.$1=="417" || RegExp.$1=="416" || RegExp.$1=="412" ) ? ' '+ s + '2_0' 
								: RegExp.$1=="312" ? ' '+ s + '1_3'
								: RegExp.$1=="125" ? ' '+ s + '1_2'
								: RegExp.$1=="85" ? ' '+ s + '1_0'
								: '' )
							:'')
						)
				) //applewebkit	
		
			:is('mozilla/')?g
			:''
			
			// mobile
			,is("android|mobi|mobile|j2me|iphone|ipod|ipad|blackberry|playbook|kindle|silk")?m:''
			
			// os/platform
			,is('j2me')?'j2me'
			:is('ipad|ipod|iphone')?  
				( 
					(
						/CPU( iPhone)? OS (\d+[_|\.]\d+([_|\.]\d+)*)/i.test(ua)  ?
						'ios' + version('ios',RegExp.$2) : ''
					) + ' ' + ( /(ip(ad|od|hone))/gi.test(ua) ?	RegExp.$1 : "" )
				) //'iphone'
			//:is('ipod')?'ipod'
			//:is('ipad')?'ipad'
			:is('playbook')?'playbook'
			:is('kindle|silk')?'kindle'
			:is('playbook')?'playbook'
			:is('mac')?'mac'+ (/mac os x ((\d+)[.|_](\d+))/.test(ua) ?    ( ' mac' + (RegExp.$2)  +  ' mac' + (RegExp.$1).replace('.',"_")  )     : '' )
			:is('win')?'win'+
					(is('windows nt 6.2')?' win8'
					:is('windows nt 6.1')?' win7'
					:is('windows nt 6.0')?' vista'
					:is('windows nt 5.2') || is('windows nt 5.1') ? ' win_xp' 
					:is('windows nt 5.0')?' win_2k'
					:is('windows nt 4.0') || is('WinNT4.0') ?' win_nt'
					: ''
					) 
			:is('freebsd')?'freebsd'
			:(is('x11|linux'))?'linux'
			:''
			
			// user agent language
			,(/[; |\[](([a-z]{2})(\-[a-z]{2})?)[)|;|\]]/i.test(ua))?(lang+RegExp.$2).replace("-","_")+(RegExp.$3!=''?(' '+lang+RegExp.$1).replace("-","_"):''):''
		
			// beta: test if running iPad app
			,( is('ipad|iphone|ipod') && !is('safari') )  ?  'ipad_app'  : ''
		
		
		]; // b

    function screenSize() {
		var w = window.outerWidth || html.clientWidth;
		var h = window.outerHeight || html.clientHeight;
		uaInfo.orientation = ((w<h) ? "portrait" : "landscape");
        // remove previous min-width, max-width, client-width, client-height, and orientation
        html.className = html.className.replace(/ ?orientation_\w+/g, "").replace(/ [min|max|cl]+[w|h]_\d+/g, "")
        for (var i=(allScreens-1);i>=0;i--) { if (w >= screens[i] ) { uaInfo.maxw = screens[i]; break; }}
		widthClasses="";
        for (var info in uaInfo) { widthClasses+=" "+info+"_"+ uaInfo[info]  };
		html.className =  ( html.className +widthClasses  );
		return widthClasses;
	} // screenSize
	
    window.onresize = screenSize;
	screenSize();	

    function retina(){
      var r = window.devicePixelRatio > 1;
      if (r) {      	
        html.className+=' retina';
      }
      else {
        html.className+=' non-retina';
      }
    }
    retina();

	var cssbs = (b.join(' ')) + " js ";
	html.className =   ( cssbs + html.className.replace(/\b(no[-|_]?)?js\b/g,"")  ).replace(/^ /, "").replace(/ +/g," ");

	return cssbs;
}
	
css_browser_selector(navigator.userAgent);
// var isIE11 = !!navigator.userAgent.match(/Trident\/7\./);

// console.log('--browser version--',isIE11,'----');

/*
CSS Browser Selector v0.4.0 (Nov 02, 2010)
Rafael Lima (http://rafael.adm.br)
http://rafael.adm.br/css_browser_selector
License: http://creativecommons.org/licenses/by/2.5/
Contributors: http://rafael.adm.br/css_browser_selector#contributors

v0.5.0 2011-08-24
andrew relkin

modified, now detects:
any version of Firefox
more versions of Windows (Win8, Win7, Vista, XP, Win2k)
more versions of IE under unique conditions
more detailed support for Opera
if "no-js" in HTML class: removes and replaces with "js" (<html class="no-js">)

identifies
	browsers: Firefox; IE; Opera; Safari; Chrome, Konqueror, Iron
	browser versions: (most importantly: ie6, ie7, ie8, ie9)
	rendering engines: Webkit; Mozilla; Gecko
	platforms/OSes: Mac; Win: Win7, Vista, XP, Win2k; FreeBSD; Linux/x11 
	devices: Ipod; Ipad; Iphone; WebTV; Blackberry; Android; J2me; mobile(generic)
	enabled technology: JS

v0.6.3 2014-03-06
@silasrm <silasrm@gmail.com>
    - Added support to IE11 
        @see http://msdn.microsoft.com/en-us/library/ie/hh869301(v=vs.85).aspx
        @see http://msdn.microsoft.com/en-us/library/ie/bg182625(v=vs.85).aspx
*/

// showLog=false;
// function log(m) {if ( window.console && showLog ) {console.log(m); }  }

// function css_browser_selector(u) {
// 	var	uaInfo = {},
// 		screens = [320, 480, 640, 768, 1024, 1152, 1280, 1440, 1680, 1920, 2560],
// 		allScreens = screens.length,
// 		ua=u.toLowerCase(),
// 		is=function(t) { return RegExp(t,"i").test(ua);  },
// 		version = function(p,n) 
// 			{ 
// 			n=n.replace(".","_"); var i = n.indexOf('_'),  ver=""; 
// 			while (i>0) {ver += " "+ p+n.substring(0,i);i = n.indexOf('_', i+1);} 
// 			ver += " "+p+n; return ver; 
// 			},
// 		g='gecko',
// 		w='webkit',
// 		c='chrome',
// 		f='firefox',
// 		s='safari',
// 		o='opera',
// 		m='mobile',
// 		a='android',
// 		bb='blackberry',
// 		lang='lang_',
// 		dv='device_',
// 		html=document.documentElement,
// 		b=	[
		
// 			// browser
// 			((!(/opera|webtv/i.test(ua))&&/msie\s(\d+)/.test(ua)||(/trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.test(ua))))?('ie ie'+(/trident\/4\.0/.test(ua) ? '8' : RegExp.$1 == '11.0'?'11':RegExp.$1))
// 			:is('firefox/')?g+ " " + f+(/firefox\/((\d+)(\.(\d+))(\.\d+)*)/.test(ua)?' '+f+RegExp.$2 + ' '+f+RegExp.$2+"_"+RegExp.$4:'')	
// 			:is('gecko/')?g
// 			:is('opera')?o+(/version\/((\d+)(\.(\d+))(\.\d+)*)/.test(ua)?' '+o+RegExp.$2 + ' '+o+RegExp.$2+"_"+RegExp.$4 : (/opera(\s|\/)(\d+)\.(\d+)/.test(ua)?' '+o+RegExp.$2+" "+o+RegExp.$2+"_"+RegExp.$3:''))
// 			:is('konqueror')?'konqueror'
	
// 			:is('blackberry') ? 
// 				( bb + 
// 					( /Version\/(\d+)(\.(\d+)+)/i.test(ua)
// 						? " " + bb+ RegExp.$1 + " "+bb+ RegExp.$1+RegExp.$2.replace('.','_')
// 						: (/Blackberry ?(([0-9]+)([a-z]?))[\/|;]/gi.test(ua) 
// 							? ' ' +bb+RegExp.$2 + (RegExp.$3?' ' +bb+RegExp.$2+RegExp.$3:'')
// 							: '')
// 					)
// 				) // blackberry
	
// 			:is('android') ? 
// 				(  a +
// 					( /Version\/(\d+)(\.(\d+))+/i.test(ua)
// 						? " " + a+ RegExp.$1 + " "+a+ RegExp.$1+RegExp.$2.replace('.','_')
// 						: '')
// 					+ (/Android (.+); (.+) Build/i.test(ua)
// 						? ' '+dv+( (RegExp.$2).replace(/ /g,"_") ).replace(/-/g,"_")
// 						:''	)
// 				) //android
	
// 			:is('chrome')?w+   ' '+c+(/chrome\/((\d+)(\.(\d+))(\.\d+)*)/.test(ua)?' '+c+RegExp.$2 +((RegExp.$4>0) ? ' '+c+RegExp.$2+"_"+RegExp.$4:''):'')	
			
// 			:is('iron')?w+' iron'
			
// 			:is('applewebkit/') ? 
// 				( w+ ' '+ s + 
// 					( /version\/((\d+)(\.(\d+))(\.\d+)*)/.test(ua)
// 						?  ' '+ s +RegExp.$2 + " "+s+ RegExp.$2+RegExp.$3.replace('.','_')
// 						:  ( / Safari\/(\d+)/i.test(ua) 
// 							? 
// 							( (RegExp.$1=="419" || RegExp.$1=="417" || RegExp.$1=="416" || RegExp.$1=="412" ) ? ' '+ s + '2_0' 
// 								: RegExp.$1=="312" ? ' '+ s + '1_3'
// 								: RegExp.$1=="125" ? ' '+ s + '1_2'
// 								: RegExp.$1=="85" ? ' '+ s + '1_0'
// 								: '' )
// 							:'')
// 						)
// 				) //applewebkit	
		
// 			:is('mozilla/')?g
// 			:''
			
// 			// mobile
// 			,is("android|mobi|mobile|j2me|iphone|ipod|ipad|blackberry|playbook|kindle|silk")?m:''
			
// 			// os/platform
// 			,is('j2me')?'j2me'
// 			:is('ipad|ipod|iphone')?  
// 				( 
// 					(
// 						/CPU( iPhone)? OS (\d+[_|\.]\d+([_|\.]\d+)*)/i.test(ua)  ?
// 						'ios' + version('ios',RegExp.$2) : ''
// 					) + ' ' + ( /(ip(ad|od|hone))/gi.test(ua) ?	RegExp.$1 : "" )
// 				) //'iphone'
// 			//:is('ipod')?'ipod'
// 			//:is('ipad')?'ipad'
// 			:is('playbook')?'playbook'
// 			:is('kindle|silk')?'kindle'
// 			:is('playbook')?'playbook'
// 			:is('mac')?'mac'+ (/mac os x ((\d+)[.|_](\d+))/.test(ua) ?    ( ' mac' + (RegExp.$2)  +  ' mac' + (RegExp.$1).replace('.',"_")  )     : '' )
// 			:is('win')?'win'+
// 					(is('windows nt 6.2')?' win8'
// 					:is('windows nt 6.1')?' win7'
// 					:is('windows nt 6.0')?' vista'
// 					:is('windows nt 5.2') || is('windows nt 5.1') ? ' win_xp' 
// 					:is('windows nt 5.0')?' win_2k'
// 					:is('windows nt 4.0') || is('WinNT4.0') ?' win_nt'
// 					: ''
// 					) 
// 			:is('freebsd')?'freebsd'
// 			:(is('x11|linux'))?'linux'
// 			:''
			
// 			// user agent language
// 			,(/[; |\[](([a-z]{2})(\-[a-z]{2})?)[)|;|\]]/i.test(ua))?(lang+RegExp.$2).replace("-","_")+(RegExp.$3!=''?(' '+lang+RegExp.$1).replace("-","_"):''):''
		
// 			// beta: test if running iPad app
// 			,( is('ipad|iphone|ipod') && !is('safari') )  ?  'ipad_app'  : ''
		
		
// 		]; // b

//     function screenSize() {
// 		var w = window.outerWidth || html.clientWidth;
// 		var h = window.outerHeight || html.clientHeight;
// 		uaInfo.orientation = ((w<h) ? "portrait" : "landscape");
//         // remove previous min-width, max-width, client-width, client-height, and orientation
//         html.className = html.className.replace(/ ?orientation_\w+/g, "").replace(/ [min|max|cl]+[w|h]_\d+/g, "")
//         for (var i=(allScreens-1);i>=0;i--) { if (w >= screens[i] ) { uaInfo.maxw = screens[i]; break; }}
// 		widthClasses="";
//         for (var info in uaInfo) { widthClasses+=" "+info+"_"+ uaInfo[info]  };
// 		html.className =  ( html.className +widthClasses  );
// 		return widthClasses;
// 	} // screenSize
	
//     window.onresize = screenSize;
// 	screenSize();	

//     function retina(){
//       var r = window.devicePixelRatio > 1;
//       if (r) {      	
//         html.className+=' retina';
//       }
//       else {
//         html.className+=' non-retina';
//       }
//     }
//     retina();

// 	var cssbs = (b.join(' ')) + " js ";
// 	html.className =   ( cssbs + html.className.replace(/\b(no[-|_]?)?js\b/g,"")  ).replace(/^ /, "").replace(/ +/g," ");

// 	return cssbs;
// }
	
// css_browser_selector(navigator.userAgent);

/*

jQuery Browser Plugin
	* Version 2.3
	* 2008-09-17 19:27:05
	* URL: http://jquery.thewikies.com/browser
	* Description: jQuery Browser Plugin extends browser detection capabilities and can assign browser selectors to CSS classes.
	* Author: Nate Cavanaugh, Minhchau Dang, & Jonathan Neal
	* Copyright: Copyright (c) 2008 Jonathan Neal under dual MIT/GPL license.
	* JSLint: This javascript file passes JSLint verification.
*//*jslint
		bitwise: true,
		browser: true,
		eqeqeq: true,
		forin: true,
		nomen: true,
		plusplus: true,
		undef: true,
		white: true
*//*global
		jQuery
*/

(function ($) {
	$.browserTest = function (a, z) {
		var u = 'unknown', x = 'X', m = function (r, h) {
			for (var i = 0; i < h.length; i = i + 1) {
				r = r.replace(h[i][0], h[i][1]);
			}

			return r;
		}, c = function (i, a, b, c) {
			var r = {
				name: m((a.exec(i) || [u, u])[1], b)
			};

			r[r.name] = true;

			r.version = (c.exec(i) || [x, x, x, x])[3];

			if (r.name.match(/safari/) && r.version > 400) {
				r.version = '2.0';
			}

			if (r.name === 'presto') {
				r.version = ($.browser.version > 9.27) ? 'futhark' : 'linear_b';
			}
			r.versionNumber = parseFloat(r.version, 10) || 0;
			r.versionX = (r.version !== x) ? (r.version + '').substr(0, 1) : x;
			r.className = r.name + r.versionX;

			return r;
		};

		a = (a.match(/Opera|Navigator|Minefield|KHTML|Chrome/) ? m(a, [
			[/(Firefox|MSIE|KHTML,\slike\sGecko|Konqueror)/, ''],
			['Chrome Safari', 'Chrome'],
			['KHTML', 'Konqueror'],
			['Minefield', 'Firefox'],
			['Navigator', 'Netscape']
		]) : a).toLowerCase();

		$.browser = $.extend((!z) ? $.browser : {}, c(a, /(camino|chrome|firefox|netscape|konqueror|lynx|msie|opera|safari)/, [], /(camino|chrome|firefox|netscape|netscape6|opera|version|konqueror|lynx|msie|safari)(\/|\s)([a-z0-9\.\+]*?)(\;|dev|rel|\s|$)/));

		$.layout = c(a, /(gecko|konqueror|msie|opera|webkit)/, [
			['konqueror', 'khtml'],
			['msie', 'trident'],
			['opera', 'presto']
		], /(applewebkit|rv|konqueror|msie)(\:|\/|\s)([a-z0-9\.]*?)(\;|\)|\s)/);

		$.os = {
			name: (/(win|mac|linux|sunos|solaris|iphone)/.exec(navigator.platform.toLowerCase()) || [u])[0].replace('sunos', 'solaris')
		};

		if (!z) {
			$('html').addClass([$.os.name, $.browser.name, $.browser.className, $.layout.name, $.layout.className].join(' '));
		}
	};

	$.browserTest(navigator.userAgent);
})(jQuery);


//	Usage:
//
//		$("selector").anchor();
//			instantly scrolls the page to the top of the first "selector"
//
//		$("selector").anchor(offset);
//			instantly scrolls the page to the top of the first "selector" plus a number of pixels defined by offset
//
//		$("selector").animateAnchor();
//			scrolls the page to the top of the first "selector"
//
//		$("selector").animateAnchor(offset, speed);
//			scrolls the page to the top of the first "selector" plus a number of pixels defined by offset (optional parameter speed is the duration of the scroll animation in ms)

$.fn.anchor = function(offset) {
	offset = offset || 0;
	$(window).scrollTop(this.offset().top - $.fn.anchor.cookieHeight() - $.fn.anchor.menuHeight() - $.fn.anchor.infoBoxHeight() + offset);
	return this;
	// return this.animateAnchor(offset, 1);
};

$.fn.animateAnchor = function(offset, speed) {
	offset = offset || 0;
	speed = speed || 200;
	$('html, body').stop().animate({scrollTop: this.offset().top - $.fn.anchor.menuHeight() - $.fn.anchor.cookieHeight() - $.fn.anchor.infoBoxHeight() + offset}, speed);

	// this
	// 	.velocity('stop')
	// 	.velocity('scroll', {
	// 		offset: -($.fn.anchor.menuHeight() - offset),
	// 		duration: speed
	// 	});

	return this;
};

$.fn.anchor.menuHeight = function() {
	return ($.fn.anchor.isFixed() ? $('.mainMenu').outerHeight() : 0);
};

$.fn.anchor.cookieHeight = function() {
	return ($('.j-cookieBar').is(':visible') && $.fn.anchor.isFixed() ? $('.j-cookieBar').outerHeight() : 0);
};

$.fn.anchor.infoBoxHeight = function() {
	return ($(".booking__informationBox").length > 0 ? $(".booking__informationBox").outerHeight() : 0);
};

$.fn.anchor.isFixed = function() {
	return !($('[data-fixedheader]').data('fixedheader'));
};

// $.fn.anchor.menuHeight = 60;


/*! DatePicker v6.3.6 MIT/GPL2 @freqdec */

var datePickerController = (function datePickerController() {

    "use strict";

    var debug               = false,
        isOpera             = Object.prototype.toString.call(window.opera) === "[object Opera]",
        describedBy         = "",
        languageInfo        = parseUILanguage(),
        nbsp                = String.fromCharCode(160),
        datePickers         = {},
        weeksInYearCache    = {},
        bespokeTitles       = {},
        uniqueId            = 0,
        finalOpacity        = 100,
        cssAnimations       = null,
        transitionEnd       = "",
        buttonTabIndex      = true,
        mouseWheel          = true,
        deriveLocale        = true,
        localeImport        = false,
        nodrag              = false,
        langFileFolder      = false,
        returnLocaleDate    = false,
        kbEvent             = false,
        dateParseFallback   = true,
        cellFormat          = "%d %F %Y",
        titleFormat         = "%F %d, %Y",
        statusFormat        = "",
        formatParts         = isOpera ? ["%j"] : ["%j", " %F %Y"],
        dPartsRegExp        = /%([d|j])/,
        mPartsRegExp        = /%([M|F|m|n])/,
        yPartsRegExp        = /%[y|Y]/,
        noSelectionRegExp   = /date-picker-unused|out-of-range|day-disabled|not-selectable/,
        formatTestRegExp    = /%([d|j|M|F|m|n|Y|y])/,
        formatSplitRegExp   = /%([d|D|l|j|N|w|S|W|M|F|m|n|t|Y|y])/,
        rangeRegExp         = /^((\d\d\d\d)(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01]))$/,
        wcDateRegExp        = /^(((\d\d\d\d)|(\*\*\*\*))((0[1-9]|1[012])|(\*\*))(0[1-9]|[12][0-9]|3[01]))$/,
        wsCharClass         = "\u0009\u000A\u000B\u000C\u000D\u0020\u00A0\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029",
        // https://gist.github.com/padolsey/527683
        oldIE               = (function(){var undef,v = 3,div = document.createElement('div'),all = div.getElementsByTagName('i');while (div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',all[0]); return v > 4 ? v : undef;}());

    (function() {
        var scriptFiles = document.getElementsByTagName('script'),
            json        = parseJSON(String(scriptFiles[scriptFiles.length - 1].innerHTML).replace(/[\n\r\s\t]+/g, " ").replace(/^\s+/, "").replace(/\s+$/, ""));

        if(typeof json === "object" && !("err" in json)) {
            affectJSON(json);
        };

        if(deriveLocale && typeof(fdLocale) != "object") {
            /*
            var head   = document.getElementsByTagName("head")[0] || document.documentElement,
                loc    = langFileFolder ? langFileFolder : scriptFiles[scriptFiles.length - 1].src.substr(0, scriptFiles[scriptFiles.length - 1].src.lastIndexOf("/")) + "/lang/",
                script,
                i;

            for(i = 0; i < languageInfo.length; i++) {
                script          = document.createElement('script');
                script.type     = "text/javascript";
                script.src      = loc + languageInfo[i] + ".js";
                script.charSet  = "utf-8";

                if(oldIE && oldIE < 8) {
                    var bases = document.getElementsByTagName('base');
                    if (bases.length && bases[0].childNodes.length) {
                            bases[0].appendChild(script);
                    } else {
                            head.appendChild(script);
                    };
                    bases = null;
                } else {
                    head.appendChild(script);
                };
            };

            script = null;
            */
        } else {
            returnLocaleDate = true;
        };
    })();

    function removeChildNodes(elem) {
        while(elem.firstChild) {
            elem.removeChild(elem.firstChild);
        };
    };

    function addClass(e, c) {
        if(new RegExp("(^|[" + wsCharClass + "])" + c + "([" + wsCharClass + "]|$)").test(e.className)) {
            return;
        };
        e.className += ( e.className ? " " : "" ) + c;
    };

    function removeClass(e, c) {
        e.className = !c ? "" : e.className.replace(new RegExp("(^|[" + wsCharClass + "])" + c + "([" + wsCharClass + "]|$)"), " ").replace(new RegExp("/^[" + wsCharClass + "][" + wsCharClass + "]*/"), '').replace(new RegExp("/[" + wsCharClass + "][" + wsCharClass + "]*$/"), '');
    };

    // Attempts to parse the current language from the HTML element. Defaults to "en" if none given
    function parseUILanguage() {
        var languageTag = document.getElementsByTagName('html')[0].getAttribute('lang') || document.getElementsByTagName('html')[0].getAttribute('xml:lang');
        languageTag = !languageTag ? "en" : languageTag.toLowerCase();
        return languageTag.search(/^([a-z]{2,3})-([a-z]{2})$/) != -1 ? [languageTag.match(/^([a-z]{2,3})-([a-z]{2})$/)[1], languageTag] : [languageTag];
    };

    // Cross browser split from http://blog.stevenlevithan.com/archives/cross-browser-split
    var cbSplit = function(str, separator, limit) {
        // if `separator` is not a regex, use the native `split`
        if(Object.prototype.toString.call(separator) !== "[object RegExp]") {
                return cbSplit._nativeSplit.call(str, separator, limit);
        };

        var output        = [],
            lastLastIndex = 0,
            flags         = "",
            separator     = RegExp(separator.source, "g"),
            separator2, match, lastIndex, lastLength;

        str = str + "";

        if(!cbSplit._compliantExecNpcg) {
            separator2 = RegExp("^" + separator.source + "$(?!\\s)", flags);
        };

        /* behavior for `limit`: if it's...
        - `undefined`: no limit.
        - `NaN` or zero: return an empty array.
        - a positive number: use `Math.floor(limit)`.
        - a negative number: no limit.
        - other: type-convert, then use the above rules. */
        if(limit === undefined || +limit < 0) {
            limit = Infinity;
        } else {
            limit = Math.floor(+limit);
            if(!limit) {
                return [];
            };
        };

        while(match = separator.exec(str)) {
            lastIndex = match.index + match[0].length; // `separator.lastIndex` is not reliable cross-browser

            if (lastIndex > lastLastIndex) {
                output.push(str.slice(lastLastIndex, match.index));

                // fix browsers whose `exec` methods don't consistently return `undefined` for nonparticipating capturing groups
                if(!cbSplit._compliantExecNpcg && match.length > 1) {
                    match[0].replace(separator2, function () {
                        for (var i = 1; i < arguments.length - 2; i++) {
                            if(arguments[i] === undefined) {
                                match[i] = undefined;
                            };
                        };
                    });
                };

                if(match.length > 1 && match.index < str.length) {
                    Array.prototype.push.apply(output, match.slice(1));
                };

                lastLength = match[0].length;
                lastLastIndex = lastIndex;

                if(output.length >= limit) {
                    break;
                };
            };

            if(separator.lastIndex === match.index) {
                // avoid an infinite loop
                separator.lastIndex++;
            };
        };

        if(lastLastIndex === str.length) {
            if (lastLength || !separator.test("")) {
                output.push("");
            };
        } else {
            output.push(str.slice(lastLastIndex));
        };

        return output.length > limit ? output.slice(0, limit) : output;
    };
    // NPCG: nonparticipating capturing group
    cbSplit._compliantExecNpcg = /()??/.exec("")[1] === undefined;
    cbSplit._nativeSplit = String.prototype.split;

    // Affects the JSON passed to the script
    function affectJSON(json) {
        if(!(typeof json === "object")) {
            return;
        };

        var key,
            switchObj = {
                "debug":function(value) {
                    debug = !!value;
                    return true;
                },
                "lang":function(value) {
                    if(typeof value === "string" && value.search(/^[a-z]{2,3}(-([a-z]{2}))?$/i) != -1) {
                        languageInfo = [value.toLowerCase()];
                        returnLocaleDate = true;
                        deriveLocale = true;
                    };
                    return true;
                },
                "nodrag":function(value) {
                    nodrag = !!value;
                    return true;
                },
                "buttontabindex":function(value) {
                    buttonTabIndex = !!value;
                    return true;
                },
                "derivelocale":function(value) {
                    deriveLocale = !!value;
                    return true;
                },
                "mousewheel":function(value) {
                    mouseWheel = !!value;
                    return true;
                },
                "cellformat":function(value) {
                    if(typeof value === "string") {
                        parseCellFormat(value);
                    };
                    return true;
                },
                "titleformat":function(value) {
                    if(typeof value === "string") {
                        titleFormat = value;
                    };
                    return true;
                },
                "statusformat":function(value) {
                    if(typeof value === "string") {
                        statusFormat = value;
                    };
                    return true;
                },
                "describedby":function(value) {
                    if(typeof value === "string") {
                        describedBy = value;
                    };
                    return true;
                },
                "finalopacity":function(value) {
                    if(typeof value === 'number' && (+value > 20 && +value <= 100)) {
                        finalOpacity = parseInt(value, 10);
                    };
                    return true;
                },
                "bespoketitles":function(value) {
                    if(typeof value === "object") {
                        bespokeTitles = {};
                        for(var dt in value) {
                            if(value.hasOwnProperty(dt) && String(dt).match(wcDateRegExp) != -1) {
                                bespokeTitles[dt] = String(value[dt]);
                            };
                        };
                    };
                    return true;
                },
                "dateparsefallback":function(value) {
                    dateParseFallback = !!value;
                    return true;
                },
                "languagefilelocation":function(value) {
                    langFileFolder = value;
                    return true;
                },
                "_default":function() {
                    if(debug) {
                        throw "Unknown key located within JSON data: " + key;
                    };
                    return true;
                }
            };

        for(key in json) {
            if(!json.hasOwnProperty(key)) {
                continue;
            };
            (switchObj.hasOwnProperty(String(key).toLowerCase()) && switchObj[String(key).toLowerCase()] || switchObj._default)(json[key]);
        };
    };

    // Parses the JSON passed either between the script tags or by using the
    // setGlobalOptions method
    function parseJSON(str) {
        if(!(typeof str === 'string') || str == "") {
            return {};
        };
        try {
            // Does a JSON (native or not) Object exist
            if(typeof JSON === "object" && JSON.parse) {
                return window.JSON.parse(str);
            // Genious code taken from: http://kentbrewster.com/badges/
            } else if(/debug|lang|nodrag|buttontabindex|derivelocale|mousewheel|cellformat|titleformat|statusformat|describedby|finalopacity|bespoketitles|dateparsefallback/.test(str.toLowerCase())) {
                var f = Function(['var document,top,self,window,parent,Number,Date,Object,Function,',
                    'Array,String,Math,RegExp,Image,ActiveXObject;',
                    'return (' , str.replace(/<\!--.+-->/gim,'').replace(/\bfunction\b/g,'function-') , ');'].join(''));
                return f();
            };
        } catch (e) { };

        if(debug) {
            throw "Could not parse the JSON object";
        };

        return {"err":1};
    };

    // Parses the cell format to use whenever the datepicker has keyboard focus
    function parseCellFormat(value) {
        if(isOpera) {
            // Don't use hidden text for opera due to the default
            // "blue" browser focus outline stretching outside of the viewport
            // and degrading visual accessibility. Harsh & hackish though...
            formatParts = ["%j"];
            cellFormat  = "%j %F %Y";
            return;
        };

        // If no day part stipulated then use presets
        if(value.match(/%([d|j])/) == -1) {
            return;
        };

        // Basic split on the %j or %d modifiers
        formatParts = cbSplit(value, /%([d|j])/);
        cellFormat  = value;
    };

    function pad(value, length) {
        length = Math.min(4, length || 2);
        return "0000".substr(0,length - Math.min(String(value).length, length)) + value;
    };

    // Very, very basic event functions
    function addEvent(obj, type, fn) {
        if(obj.addEventListener) {
            obj.addEventListener(type, fn, true);
        } else if(obj.attachEvent) {
            obj.attachEvent("on"+type, fn);
        };
    };
    function removeEvent(obj, type, fn) {
        try {
            if(obj.removeEventListener) {
                obj.removeEventListener(type, fn, true);
            } else if(obj.detachEvent) {
                obj.detachEvent("on"+type, fn);
            };
        } catch(err) {};
    };
    function stopEvent(e) {
        e = e || document.parentWindow.event;
        if(e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
        };

        if(oldIE) {
            e.cancelBubble = true;
            e.returnValue = false;
        };

        return false;
    };

    function setARIARole(element, role) {
        if(element && element.tagName) {
            element.setAttribute("role", role);
        };
    };

    function setARIAProperty(element, property, value) {
        if(element && element.tagName) {
            element.setAttribute("aria-" + property, value);
        };
    };

    // Sets a tabindex attribute on an element, bends over for IE.
    function setTabIndex(e, i) {
        e.setAttribute(oldIE ? "tabIndex" : "tabindex", i);
        e.tabIndex = i;
    };

    function dateToYYYYMMDD(dt) {
        return dt instanceof Date && !isNaN(dt) ? dt.getFullYear() + pad(dt.getMonth() + 1) + "" + pad(dt.getDate()) : dt;
    };

    // The datePicker object itself
    function datePicker(options) {
        this.dateSet             = null;
        this.timerSet            = false;
        this.visible             = false;
        this.fadeTimer           = null;
        this.timer               = null;
        this.yearInc             = 0;
        this.monthInc            = 0;
        this.dayInc              = 0;
        this.mx                  = 0;
        this.my                  = 0;
        this.x                   = 0;
        this.y                   = 0;
        this.created             = false;
        this.disabled            = false;
        this.opacity             = 0;
        this.opacityTo           = 100;
        this.finalOpacity        = 100;
        this.inUpdate            = false;
        this.kbEventsAdded       = false;
        this.fullCreate          = false;
        this.selectedTD          = null;
        this.cursorTD            = null;
        this.cursorDate          = options.cursorDate ? options.cursorDate : "",
        this.date                = options.cursorDate ? new Date(+options.cursorDate.substr(0,4), +options.cursorDate.substr(4,2) - 1, +options.cursorDate.substr(6,2),5,0,0) : new Date();
        this.defaults            = {};
        this.dynDisabledDates    = {};
        this.dateList            = [];
        this.bespokeClass        = options.bespokeClass;
        this.firstDayOfWeek      = localeImport.firstDayOfWeek;
        this.interval            = new Date();
        this.clickActivated      = false;
        this.showCursor          = false;
        this.noFocus             = true;
        this.kbEvent             = false;
        this.delayedUpdate       = false;
        this.bespokeTitles       = {};
        this.bespokeTabIndex     = options.bespokeTabIndex;

        for(var thing in options) {
            if(!options.hasOwnProperty(thing) || String(thing).search(/^(callbacks|formElements|enabledDates|disabledDates)$/) != -1) {
                continue;
            };
            this[thing] = options[thing];
        };

        if(oldIE) {
            this.iePopUp = null;
        };

        for(var i = 0, prop; prop = ["callbacks", "formElements"][i]; i++) {
            this[prop] = {};
            if(prop in options) {
                for(thing in options[prop]) {
                    if(options[prop].hasOwnProperty(thing)) {
                            this[prop][thing] = options[prop][thing];
                    };
                };
            };
        };

        // Adjust time to stop daylight savings madness on windows
        this.date.setHours(5);

        // Called from an associated form elements onchange event
        this.changeHandler = function() {
            // In a perfect world this shouldn't ever happen
            if(o.disabled) {
                return;
            };
            o.setDateFromInput();
            o.callback("dateset", o.createCbArgObj());
        };

        // Creates the object passed to the callback functions
        this.createCbArgObj = function() {
            return this.dateSet ? {
                "id"    :this.id,
                "date"  :this.dateSet,
                "dd"    :pad(this.date.getDate()),
                "mm"    :pad(this.date.getMonth() + 1),
                "yyyy"  :this.date.getFullYear()
                } : {
                "id"    :this.id,
                "date"  :null,
                "dd"    :null,
                "mm"    :null,
                "yyyy"  :null
                };
        };

        // Attempts to grab the window scroll offsets
        this.getScrollOffsets = function() {
            if(typeof(window.pageYOffset) == 'number') {
                //Netscape compliant
                return [window.pageXOffset, window.pageYOffset];
            } else if(document.body && (document.body.scrollLeft || document.body.scrollTop)) {
                //DOM compliant
                return [document.body.scrollLeft, document.body.scrollTop];
            } else if(document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
                //IE6 standards compliant mode
                return [document.documentElement.scrollLeft, document.documentElement.scrollTop];
            };
            return [0,0];
        };

        // Calculates the current list of disabled & enabled dates for a specific year/month
        this.getDateExceptions = function(y, m) {

            m = pad(m);

            var obj     = {},
                lower   = o.firstDateShown,
                upper   = o.lastDateShown,
                rLength = o.dateList.length,
                rNumber, workingDt, workingY, workingM, dtLower, dtUpper, i, dt, dt1, dt2, rngLower, rngUpper, cDate;

            if(!upper || !lower) {
                lower = o.firstDateShown = y + pad(m) + "01";
                upper = o.lastDateShown  = y + pad(m) + pad(daysInMonth(m, y));
            };

            dtLower = Number(lower.substr(0,6));
            dtUpper = Number(upper.substr(0,6));

            workingDt = String(dtLower);

            while(+workingDt <= dtUpper) {
                workingY = workingDt.substr(0,4);
                workingM = workingDt.substr(4,2);

                for(rNumber = 0; rNumber < rLength; rNumber++) {
                    dt1 = String(o.dateList[rNumber].rLow).replace(/^(\*\*\*\*)/, workingY).replace(/^(\d\d\d\d)(\*\*)/, "$1"+workingM);
                    dt2 = String(o.dateList[rNumber].rHigh).replace(/^(\*\*\*\*)/, workingY).replace(/^(\d\d\d\d)(\*\*)/, "$1"+workingM);

                    // Single date
                    if(dt2 == 1) {
                        if(+dt1 >= +o.firstDateShown && +dt1 <= +o.lastDateShown) {
                            obj[dt1] = o.dateList[rNumber].type;
                        };
                        continue;
                    };

                    // Date Range
                    if(dt1 <= dt2
                       &&
                       workingDt >= dt1.substr(0,6)
                       &&
                       workingDt <= dt2.substr(0,6)
                       ) {
                        rngLower = Math.max(dt1, Math.max(String(workingDt) + "01", this.firstDateShown));
                        rngUpper = Math.min(dt2, Math.min(String(workingDt) + "31", this.lastDateShown));
                        for(i = rngLower; i <= rngUpper; i++) {
                            obj[i] = o.dateList[rNumber].type;
                        };
                    };
                };

                // Let the Date Object take care of month overflowss
                workingDt = new Date(workingY, +workingM, 2);
                workingDt = workingDt.getFullYear()+""+pad(workingDt.getMonth()+1);
            };

            return obj;
        };

        //mehr//
        this.checkPosition = function(){
            var is_onscreen = ($(o.div).isOnScreen(0,1));
            if(!is_onscreen){
                // Request to move top of the page instead of just make visible
                // var ht = $('html').hasClass('android') ? window.screen.height-80 : $(window).height();
                // var oy = Math.min($(o.div).outerHeight(true) - ht, -60);
                // var _pos = $(o.div).offset().top + oy;
                var _pos = $(o.div).offset().top - $(o.div).prev().outerHeight(true) - 60 - (($(".j-cookieBar").is(":visible") && $(".j-cookieBar").outerHeight()) || 0);
                $('html').velocity("scroll", {
                    offset: _pos
                });
            }
        };
        // Repositions the datepicker beside the button - to the bottom by
        // preference but to the top if there is not enough room to display the
        // entire U.I. at the bottom (it really should be updated to favour
        // bottom positioning if not enough room to display the entire U.I. at
        // the top in that scenario though)
        this.reposition = function() {
            if(!o.created || o.staticPos) {
                return;
            };

            o.div.style.visibility = "hidden";
            o.div.style.display = "block";
            if(o.wrapped) return;
            o.div.style.left = o.div.style.top = "0px";

            //disable repositioning;
            //return;
            //console.log("repo");
            var osh         = o.div.offsetHeight,
                osw         = o.div.offsetWidth,
                elem        = document.getElementById('fd-but-' + o.id),
                pos         = o.truePosition(elem),
                trueBody    = (document.compatMode && document.compatMode!="BackCompat") ? document.documentElement : document.body,
                sOffsets    = o.getScrollOffsets(),
                scrollTop   = sOffsets[1],
                scrollLeft  = sOffsets[0],
                tSpace      = parseInt(pos[1] - 2) - parseInt(scrollTop),
                bSpace      = parseInt(trueBody.clientHeight + scrollTop) - parseInt(pos[1] + elem.offsetHeight + 2);

            o.div.style.visibility = "visible";

            o.div.style.left = Number(parseInt(trueBody.clientWidth+scrollLeft) < parseInt(osw+pos[0]) ? Math.abs(parseInt((trueBody.clientWidth+scrollLeft) - osw)) : pos[0]) + "px";
            o.div.style.top  = (bSpace > tSpace) ? Math.abs(parseInt(pos[1] + elem.offsetHeight + 2)) + "px" : Math.abs(parseInt(pos[1] - (osh + 2))) + "px";
            o.div.style.top  = Math.abs(parseInt(pos[1] + elem.offsetHeight + 2)) + "px";


            if(oldIE === 6) {
                o.iePopUp.style.top    = o.div.style.top;
                o.iePopUp.style.left   = o.div.style.left;
                o.iePopUp.style.width  = osw + "px";
                o.iePopUp.style.height = (osh - 2) + "px";
            };
            // added to reposition mehr
            $(window).trigger('resize');

        };

        this.removeCursorHighlight = function() {
            var td = document.getElementById(o.id + "-date-picker-hover");
            if(td) {
                removeClass(td, "date-picker-hover");
            };
        };

        this.addCursorHighlight = function() {
            var td = document.getElementById(o.id + "-date-picker-hover");
            if(td) {
                addClass(td, "date-picker-hover");
            };
        };
        // Resets the tabindex of the previously focused cell
        this.removeOldFocus = function() {
            var td = document.getElementById(o.id + "-date-picker-hover");
            if(td) {
                try {
                    setTabIndex(td, -1);
                    removeClass(td, "date-picker-hover");
                    td.id = "";
                    td.onblur  = null;
                    td.onfocus = null;
                } catch(err) {};
            };
        };

        // Sets the tabindex & focus on the currently highlighted cell
        this.setNewFocus = function() {
            var td = document.getElementById(o.id + "-date-picker-hover");
            if(td) {
                try {
                    setTabIndex(td, 0);
                    if(this.showCursor) {
                        addClass(td, "date-picker-hover");
                    };
                    // If opened with the keyboard then add focus & blur events to the cell
                    if(!this.clickActivated) {
                        td.onblur    = o.onblur;
                        td.onfocus   = o.onfocus;
                    };

                    // If opened with the keyboard (and not in opera) then add a screen-reader friendly date format
                    if(!isOpera && !this.clickActivated) {
                        o.addAccessibleDate();
                    };

                    // Try to programmatically set focus on the cell
                    if(!this.noFocus && !this.clickActivated) {
                        setTimeout(function() { try { td.focus(); } catch(err) {}; }, 0);
                    };
                } catch(err) { };
            };
        };

        // Adds a screen-reader friendly date to the current cell whenever
        // the datepicker has been opened with the keyboard
        this.addAccessibleDate = function() {
            var td   = document.getElementById(o.id + "-date-picker-hover");

            if(td && !(td.getElementsByTagName("span").length)) {
                var ymd = td.className.match(/cd-([\d]{4})([\d]{2})([\d]{2})/),
                    noS = td.className.search(noSelectionRegExp) != -1,
                    spn = document.createElement('span'),
                    spnC;

                spn.className       = "fd-screen-reader";

                removeChildNodes(td);

                if(noS) {
                    spnC = spn.cloneNode(false);
                    spnC.appendChild(document.createTextNode(getTitleTranslation(13)));
                    td.appendChild(spnC);
                };

                for(var pt = 0, part; part = formatParts[pt]; pt++) {
                    if(part == "%j" || part == "%d") {
                        td.appendChild(document.createTextNode(printFormattedDate(new Date(ymd[1], +ymd[2]-1, ymd[3], 5, 0, 0), part, true)));
                    } else {
                        spnC = spn.cloneNode(false);
                        spnC.appendChild(document.createTextNode(printFormattedDate(new Date(ymd[1], +ymd[2]-1, ymd[3], 5, 0, 0), part, true)));
                        td.appendChild(spnC);
                    };
                };
            };
        };

        // Sets the current cursor to a specific date
        this.setCursorDate = function(yyyymmdd) {
            if(String(yyyymmdd).search(/^([0-9]{8})$/) != -1) {
                this.date = new Date(+yyyymmdd.substr(0,4), +yyyymmdd.substr(4,2) - 1, +yyyymmdd.substr(6,2), 5, 0, 0);
                this.cursorDate = yyyymmdd;

                if(this.staticPos) {
                    this.updateTable();
                };
            };
        };

        // Updates the table used to display the datepicker
        this.updateTable = function(noCallback) {
            if(!o || o.inUpdate || !o.created) {
                return;
            };

            // We are currently updating (used to stop public methods from firing)
            o.inUpdate = true;

            // Remove the focus from the currently highlighted cell
            o.removeOldFocus();

            o.div.dir = localeImport.rtl ? "rtl" : "ltr";

            // If the update timer initiated
            if(o.timerSet && !o.delayedUpdate) {
                // Are we incrementing/decrementing the month
                if(o.monthInc) {
                    var n = o.date.getDate(),
                        d = new Date(o.date);

                    d.setDate(2);
                    d.setMonth(d.getMonth() + o.monthInc * 1);
                    // Don't go over the days in the month
                    d.setDate(Math.min(n, daysInMonth(d.getMonth(),d.getFullYear())));

                    o.date = new Date(d);
                } else {
                    o.date.setDate(Math.min(o.date.getDate()+o.dayInc, daysInMonth(o.date.getMonth()+o.monthInc,o.date.getFullYear()+o.yearInc)));
                    o.date.setMonth(o.date.getMonth() + o.monthInc);
                    o.date.setFullYear(o.date.getFullYear() + o.yearInc);
                };
            };

            // Make sure the internal date is within range
            o.outOfRange();

            // Disable/enable the today button
            if(!o.noToday) {
                o.disableTodayButton();
            };

            // Disable/enable the month & year buttons
            o.showHideButtons(o.date);

            var cd = o.date.getDate(),
                cm = o.date.getMonth(),
                cy = o.date.getFullYear(),
                cursorDate = (String(cy) + pad(cm+1) + pad(cd)),
                tmpDate    = new Date(cy, cm, 1, 5, 0, 0);

            tmpDate.setHours(5);

            var dt, dts, cName, row, td, i, currentDate, cellAdded, col, currentStub, abbr, bespokeRenderClass, spnC, dateSetD, selectable, weekDay,
                // Weekday of the fist of the month
                weekDayC            = (tmpDate.getDay() + 6) % 7,
                // The column index this weekday will occupy
                firstColIndex       = (((weekDayC - o.firstDayOfWeek) + 7 ) % 7) - 1,
                // The number of days in the current month
                dpm                 = daysInMonth(cm, cy),
                // Today as a Date Object
                today               = new Date(),
                // Today as a YYYYMMDD String
                today               = today.getFullYear() + pad(today.getMonth()+1) + pad(today.getDate()),
                // A Sring date stub in a YYYYMM format for the current date
                stub                = String(tmpDate.getFullYear()) + pad(tmpDate.getMonth()+1),
                //
                cellAdded           = [4,4,4,4,4,4],
                // The first day of the previous month as a Date Object
                lm                  = new Date(cy, cm-1, 1, 5, 0, 0),
                // The first day of the next month as a Date Object
                nm                  = new Date(cy, cm+1, 1, 5, 0, 0),
                // The number of days in the previous month
                daySub              = daysInMonth(lm.getMonth(), lm.getFullYear()),
                // YYYYMM String date stub for the next month
                stubN               = String(nm.getFullYear()) + pad(nm.getMonth()+1),
                // YYYYMM String date stub for the previous month
                stubP               = String(lm.getFullYear()) + pad(lm.getMonth()+1),
                weekDayN            = (nm.getDay() + 6) % 7,
                weekDayP            = (lm.getDay() + 6) % 7,
                // A SPAN node to clone when adding dates to individual cells
                spn                 = document.createElement('span');

            // Give the "fd-screen-reader" class to the span in order to hide them in the UI
            // but keep them accessible to screen-readers
            spn.className       = "fd-screen-reader";

            // The first & last dates shown on the datepicker UI - could be a date from the previous & next month respectively
            o.firstDateShown    = !o.constrainSelection && o.fillGrid && (0 - firstColIndex < 1) ? String(stubP) + (daySub + (0 - firstColIndex)) : stub + "01";
            o.lastDateShown     = !o.constrainSelection && o.fillGrid ? stubN + pad(41 - firstColIndex - dpm) : stub + String(dpm);

            // Store a reference to the current YYYYMM String representation of the current month
            o.currentYYYYMM     = stub;

            bespokeRenderClass  = o.callback("redraw", {id:o.id, dd:pad(cd), mm:pad(cm+1), yyyy:cy, firstDateDisplayed:o.firstDateShown, lastDateDisplayed:o.lastDateShown}) || {};

            // An Object of dates that have been explicitly disabled (1) or enabled (0)
            dts                 = o.getDateExceptions(cy, cm+1);

            // Double check current date within limits etc
            o.checkSelectedDate();
            //
            dateSetD            = (o.dateSet != null) ? o.dateSet.getFullYear() + pad(o.dateSet.getMonth()+1) + pad(o.dateSet.getDate()) : false;

            // If we have selected a date then set its ARIA selected property
            // to false. We then set the ARIA selected property to true on the
            // newly selected cell after redrawing the table
            if(this.selectedTD != null) {
                setARIAProperty(this.selectedTD, "selected", false);
                this.selectedTD = null;
            };

            // Redraw all of the table cells representing the date parts of the UI
            for(var curr = 0; curr < 42; curr++) {
                // Current row
                row  = Math.floor(curr / 7);
                // Current TD node
                td   = o.tds[curr];
                // Clone our SPAN node
                spnC = spn.cloneNode(false);
                // Remove any previous contents from the cell
                removeChildNodes(td);

                // If the current cell contains a date
                if((curr > firstColIndex && curr <= (firstColIndex + dpm)) || o.fillGrid) {
                    currentStub     = stub;
                    weekDay         = weekDayC;
                    dt              = curr - firstColIndex;
                    cName           = [];
                    selectable      = true;

                    // Are we drawing last month
                    if(dt < 1) {
                        dt              = daySub + dt;
                        currentStub     = stubP;
                        weekDay         = weekDayP;
                        selectable      = !o.constrainSelection;
                        cName.push("month-out");
                    // Are we drawing next month
                    } else if(dt > dpm) {
                        dt -= dpm;
                        currentStub     = stubN;
                        weekDay         = weekDayN;
                        selectable      = !o.constrainSelection;
                        cName.push("month-out");
                    };

                    // Calcuate this cells weekday
                    weekDay = (weekDay + dt + 6) % 7;

                    // Push a classname representing the weekday e.g. "day-3"
                    cName.push("day-" + weekDay + " cell-" + curr);

                    // A YYYYMMDD String representation of this cells date
                    currentDate = currentStub + String(dt < 10 ? "0" : "") + dt;

                    // If this cells date is out of range
                    if(o.rangeLow && +currentDate < +o.rangeLow || o.rangeHigh && +currentDate > +o.rangeHigh) {
                        // Add a classname to style the cell and stop selection
                        td.className = "out-of-range";
                        // Reset this TD nodes title attribute
                        td.title = "";
                        // Append the cells date as a text node to the TD
                        td.appendChild(document.createTextNode(dt));
                        // Jaysus, what the feck does this line do again...
                        if(o.showWeeks) {
                            cellAdded[row] = Math.min(cellAdded[row], 2);
                        };
                    // This cells date is within the lower & upper ranges (or no ranges have been defined)
                    } else {
                        // If it's a date from last or next month and the "constrainSelection" option
                        // is false then give the cell a CD-YYYYMMDD class
                        if(selectable) {
                            td.title = titleFormat ? printFormattedDate(new Date(+String(currentStub).substr(0,4), +String(currentStub).substr(4, 2) - 1, +dt, 5, 0, 0), titleFormat, true) : "";
                            cName.push("cd-" + currentDate + " yyyymmdd-" + currentDate + " yyyymm-" + currentStub + " mmdd-" + currentStub.substr(4,2) + pad(dt));
                        // Otherwise give a "not-selectable" class (which shouldn't be styled in any way, it's for internal use)
                        } else {
                            td.title = titleFormat ? getTitleTranslation(13) + " " + printFormattedDate(new Date(+String(currentStub).substr(0,4), +String(currentStub).substr(4, 2) - 1, +dt, 5, 0, 0), titleFormat, true) : "";
                            cName.push("yyyymmdd-" + currentDate + " yyyymm-" + currentStub + " mmdd-" + currentStub.substr(4,2) + pad(dt) + " not-selectable");
                        };

                        // Add a classname if the current cells date is today
                        if(currentDate == today) {
                            cName.push("date-picker-today");
                        };

                        if(o.startPeriod != null && o.endPeriod != null) {
                            if(currentDate > o.startPeriod && currentDate < o.endPeriod) {
                                 cName.push("date-picker-period");
                            };
                        }

                        if(currentDate === o.startPeriod) {
                            cName.push("date-picker-startperiod");
                        }
                        if(currentDate === o.endPeriod) {
                            cName.push("date-picker-endperiod");
                        }

                        // If this cell represents the currently selected date
                        if(dateSetD == currentDate) {
                            // Add a classname (for styling purposes)
                            cName.push("date-picker-selected-date");
                            // Set the ARIA selected property to true
                            setARIAProperty(td, "selected", "true");
                            // And cache a reference to the current cell
                            this.selectedTD = td;
                        };

                        // If the current cell has been explicitly disabled
                        if(((currentDate in dts) && dts[currentDate] == 1)
                           // or
                           ||
                           // ... the current weekday has been disabled
                           (o.disabledDays[weekDay]
                            &&
                           // ... and the current date has not been explicitly enabled
                           !((currentDate in dts) && dts[currentDate] == 0)
                           )
                          ) {
                            // Add a classname to style the cell and stop selection
                            cName.push("day-disabled");
                            // Update the current cells title to say "Disabled date: ..." (or whatever the translation says)
                            if(titleFormat && selectable) {
                                td.title = getTitleTranslation(13) + " " + td.title;
                            };
                        };

                        // Has the redraw callback given us a bespoke classname to add to this cell
                        if(currentDate in bespokeRenderClass) {
                            cName.push(bespokeRenderClass[currentDate]);
                        };



                        // Do we need to highlight this cells weekday representation
                        if(o.highlightDays[weekDay]) {
                            cName.push("date-picker-highlight");
                        };

                        // Is the current onscreen cursor set to this cells date
                        if(cursorDate == currentDate) {
                            td.id = o.id + "-date-picker-hover";
                        };

                        // Add the date to the TD cell as a text node. Note: If the datepicker has been given keyboard
                        // events, this textnode is replaced by a more screen-reader friendly date during the focus event
                        td.appendChild(document.createTextNode(dt));

                        // Add the classnames to the TD node
                        td.className = cName.join(" ");

                        // If the UI displays week numbers then update the celladded
                        if(o.showWeeks) {
                            cellAdded[row] = Math.min(cName[0] == "month-out" ? 3 : 1, cellAdded[row]);
                        };
                    };
                // The current TD node is empty i.e. represents no date in the UI
                } else {
                    // Add a classname to style the cell
                    td.className = "date-picker-unused";
                    // Add a non-breaking space to unused TD node (for IEs benefit mostly)
                    td.appendChild(document.createTextNode(nbsp));
                    // Reset the TD nodes title attribute
                    td.title = "";
                };
                // Do we update the week number for this row
                if(o.showWeeks && curr - (row * 7) == 6) {
                    removeChildNodes(o.wkThs[row]);
                    o.wkThs[row].appendChild(document.createTextNode(cellAdded[row] == 4 && !o.fillGrid ? nbsp : getWeekNumber(cy, cm, curr - firstColIndex - 6)));
                    o.wkThs[row].className = "date-picker-week-header" + (["",""," out-of-range"," month-out",""][cellAdded[row]]);
                };
            };

            // Update the UI title bar displaying the year & month
            var span = o.titleBar.getElementsByTagName("span");
            removeChildNodes(span[0]);
            removeChildNodes(span[1]);
            span[0].appendChild(document.createTextNode(getMonthTranslation(cm, false) + nbsp));
            span[1].appendChild(document.createTextNode(cy));

            // If we are in an animation
            if(o.timerSet) {
                // Speed the timer up a little bit to make the pause between updates quicker
                o.timerInc = 50 + Math.round(((o.timerInc - 50) / 1.8));
                // Recall this function in a timeout
                o.timer = window.setTimeout(o.updateTable, o.timerInc);
            };

            // We are not currently updating the UI
            o.inUpdate = o.delayedUpdate = false;
            // Focus on the correct TD node
            o.setNewFocus();
        };

        // Removes all scaffold from the DOM & events from memory
        this.destroy = function() {

            // Remove the button if it exists
            if(document.getElementById("fd-but-" + this.id)) {
                document.getElementById("fd-but-" + this.id).parentNode.removeChild(document.getElementById("fd-but-" + this.id));
            };

            if(!this.created) {
                return;
            };

            // Event cleanup for Internet Explorers benefit
            removeEvent(this.table, "mousedown", o.onmousedown);
            removeEvent(this.table, "mouseover", o.onmouseover);
            removeEvent(this.table, "mouseout", o.onmouseout);
            removeEvent(document, "mousedown", o.onmousedown);
            removeEvent(document, "mouseup",   o.clearTimer);

            if (window.addEventListener && !window.devicePixelRatio) {
                try {
                    window.removeEventListener('DOMMouseScroll', this.onmousewheel, false);
                } catch(err) {};
            } else {
                removeEvent(document, "mousewheel", this.onmousewheel);
                removeEvent(window,   "mousewheel", this.onmousewheel);
            };
            o.removeOnFocusEvents();
            clearTimeout(o.fadeTimer);
            clearTimeout(o.timer);

            if(oldIE === 6 && !o.staticPos) {
                try {
                    o.iePopUp.parentNode.removeChild(o.iePopUp);
                    o.iePopUp = null;
                } catch(err) {};
            };

            if(this.div && this.div.parentNode) {
                this.div.parentNode.removeChild(this.div);
            };

            o = null;
        };

        this.resizeInlineDiv = function()  {
            o.div.style.width = o.table.offsetWidth + "px";
            o.div.style.height = o.table.offsetHeight + "px";
        };

        this.reset = function() {
            var elemID, elem;
            for(elemID in o.formElements) {
                elem = document.getElementById(elemID);
                if(elem) {
                    if(elem.tagName.toLowerCase() == "select") {
                        elem.selectedIndex = o.defaultVals[elemID];
                    } else {
                        elem.value = o.defaultVals[elemID];
                    };
                };
            };
            o.changeHandler();
        };

        // Creates the DOM scaffold
        this.create = function() {

            if(document.getElementById("fd-" + this.id)) {
                return;
            };

            var tr, row, col, tableHead, tableBody, tableFoot;

            this.noFocus = true;

            function createTH(details) {
                var th = document.createElement('th');
                if(details.thClassName) {
                    th.className = details.thClassName;
                };
                if(details.colspan) {
                    th.setAttribute(oldIE ? 'colSpan' : "colspan", details.colspan);
                };
                th.unselectable = "on";
                return th;
            };
            function createThAndButton(tr, obj) {
                for(var i = 0, details; details = obj[i]; i++) {
                    var th = createTH(details);
                    tr.appendChild(th);
                    var but = document.createElement('span');
                    but.className = details.className;
                    but.id = o.id + details.id;
                    but.appendChild(document.createTextNode(details.text || o.nbsp));
                    but.title = details.title || "";
                    but.unselectable = "on";
                    th.appendChild(but);
                };
            };

            this.div                     = document.createElement('div');
            this.div.id                  = "fd-" + this.id;
            this.div.className           = "date-picker" + (cssAnimations ? " fd-dp-fade " : "") + this.bespokeClass;

            // Attempt to hide the div from screen readers during content creation
            this.div.style.visibility = "hidden";
            this.div.style.display = "none";

            // Set the ARIA describedby property if the required block available
            if(this.describedBy && document.getElementById(this.describedBy)) {
                setARIAProperty(this.div, "describedby", this.describedBy);
            };

            // Set the ARIA labelled property if the required label available
            if(this.labelledBy) {
                setARIAProperty(this.div, "labelledby", this.labelledBy.id);
            };

            this.idiv                     = document.createElement('div');

            this.table             = document.createElement('table');
            this.table.className   = "date-picker-table";
            this.table.onmouseover = this.onmouseover;
            this.table.onmouseout  = this.onmouseout;
            this.table.onclick     = this.onclick;

            if(this.finalOpacity < 100) {
                this.idiv.style.opacity = Math.min(Math.max(parseInt(this.finalOpacity, 10) / 100, .2), 1);
            };

            if(this.staticPos) {
                this.table.onmousedown  = this.onmousedown;
            };

            this.div.appendChild(this.idiv);
            this.idiv.appendChild(this.table);

            var dragEnabledCN = !this.dragDisabled ? " drag-enabled" : "";



            if(!this.staticPos) {
                this.div.style.visibility = "hidden";
                this.div.className += dragEnabledCN;


                if(!this.wrapped) {

                    document.getElementsByTagName('body')[0].appendChild(this.div);
                } else {
                    var elem = document.getElementById(this.positioned ? this.positioned : this.id);
                    if(!elem) {
                        this.div = null;
                        if(debug) {
                            throw this.positioned ? "Could not locate a datePickers associated parent element with an id:" + this.positioned : "Could not locate a datePickers associated input with an id:" + this.id;
                        };
                        return;
                    };
                    //console.log(elem);
                    elem.parentNode.insertBefore(this.div, elem.nextSibling);
                    this.div.className += " wrapped-datepicker";


                }
                if(oldIE === 6) {
                    this.iePopUp = document.createElement('iframe');
                    this.iePopUp.src = "javascript:'<html></html>';";
                    this.iePopUp.setAttribute('className','iehack');
                    // Remove iFrame from tabIndex
                    this.iePopUp.setAttribute("tabIndex", -1);
                    // Hide it from ARIA aware technologies
                    setARIARole(this.iePopUp, "presentation");
                    setARIAProperty(this.iePopUp, "hidden", "true");
                    this.iePopUp.scrolling = "no";
                    this.iePopUp.frameBorder = "0";
                    this.iePopUp.name = this.iePopUp.id = this.id + "-iePopUpHack";
                    document.body.appendChild(this.iePopUp);
                };

                // Aria "hidden" property for non active popup datepickers
                setARIAProperty(this.div, "hidden", "true");
            } else {
                var elem = document.getElementById(this.positioned ? this.positioned : this.id);
                if(!elem) {
                    this.div = null;
                    if(debug) {
                        throw this.positioned ? "Could not locate a datePickers associated parent element with an id:" + this.positioned : "Could not locate a datePickers associated input with an id:" + this.id;
                    };
                    return;
                };

                this.div.className += " static-datepicker";

                if(this.positioned) {
                    elem.appendChild(this.div);
                } else {
                    elem.parentNode.insertBefore(this.div, elem.nextSibling);
                };

                if(this.hideInput) {
                    for(var elemID in this.formElements) {
                        elem = document.getElementById(elemID);
                        if(elem) {
                            elem.className += " fd-hidden-input";
                        };
                    };
                };

                setTimeout(this.resizeInlineDiv, 300);
            };
            // ARIA Application role
            setARIARole(this.div, "application");
            //setARIARole(this.table, "grid");

            if(this.statusFormat) {
                tableFoot = document.createElement('tfoot');
                this.table.appendChild(tableFoot);
                tr = document.createElement('tr');
                tr.className = "date-picker-tfoot";
                tableFoot.appendChild(tr);
                this.statusBar = createTH({thClassName:"date-picker-statusbar" + dragEnabledCN, colspan:this.showWeeks ? 8 : 7});
                tr.appendChild(this.statusBar);
                this.updateStatus();
            };

            tableHead = document.createElement('thead');
            tableHead.className = "date-picker-thead";
            this.table.appendChild(tableHead);

            tr  = document.createElement('tr');
            setARIARole(tr, "presentation");

            tableHead.appendChild(tr);

            // Title Bar
            this.titleBar = createTH({thClassName:"date-picker-title" + dragEnabledCN, colspan:this.showWeeks ? 8 : 7});

            tr.appendChild(this.titleBar);
            tr = null;

            var span = document.createElement('span');
            span.appendChild(document.createTextNode(nbsp));
            span.className = "month-display" + dragEnabledCN;
            this.titleBar.appendChild(span);

            span = document.createElement('span');
            span.appendChild(document.createTextNode(nbsp));
            span.className = "year-display" + dragEnabledCN;
            this.titleBar.appendChild(span);

            span = null;

            tr  = document.createElement('tr');
            setARIARole(tr, "presentation");
            tableHead.appendChild(tr);

            createThAndButton(tr, [
            {className:"prev-but prev-year",  id:"-prev-year-but", text:"\u00AB", title:getTitleTranslation(2) },
            {className:"prev-but prev-month", id:"-prev-month-but", text:"\u2039", title:getTitleTranslation(0) },
            {colspan:this.showWeeks ? 4 : 3, className:"today-but", id:"-today-but", text:getTitleTranslation(4)},
            {className:"next-but next-month", id:"-next-month-but", text:"\u203A", title:getTitleTranslation(1)},
            {className:"next-but next-year",  id:"-next-year-but", text:"\u00BB", title:getTitleTranslation(3) }
            ]);

            tableBody = document.createElement('tbody');
            this.table.appendChild(tableBody);

            var colspanTotal = this.showWeeks ? 8 : 7,
                colOffset    = this.showWeeks ? 0 : -1,
                but, abbr, formElemId, formElem;

            for(var rows = 0; rows < 7; rows++) {
                row = document.createElement('tr');

                if(rows != 0) {
                    // ARIA Grid role
                    setARIARole(row, "row");
                    tableBody.appendChild(row);
                } else {
                    tableHead.appendChild(row);
                };

                for(var cols = 0; cols < colspanTotal; cols++) {
                    if(rows === 0 || (this.showWeeks && cols === 0)) {
                        col = document.createElement('th');
                    } else {
                        col = document.createElement('td');
                        setARIAProperty(col, "describedby", this.id + "-col-" + cols + (this.showWeeks ? " " + this.id + "-row-" + rows : ""));
                        setARIAProperty(col, "selected", "false");
                    };

                    if(oldIE) {
                        col.unselectable = "on";
                    };

                    row.appendChild(col);
                    if((this.showWeeks && cols > 0 && rows > 0) || (!this.showWeeks && rows > 0)) {
                        //setARIARole(col, "gridcell");
                    } else {
                        if(rows === 0 && cols > colOffset) {
                            col.className = "date-picker-day-header";
                            col.scope = "col";
                            //setARIARole(col, "columnheader");
                            col.id = this.id + "-col-" + cols;
                        } else {
                            col.className = "date-picker-week-header";
                            col.scope = "row";
                            //setARIARole(col, "rowheader");
                            col.id = this.id + "-row-" + rows;
                        };
                    };
                };
            };

            col = row = null;

            this.ths = this.table.getElementsByTagName('thead')[0].getElementsByTagName('tr')[2].getElementsByTagName('th');

            for (var y = 0; y < colspanTotal; y++) {

                if(y == 0 && this.showWeeks) {
                    this.ths[y].appendChild(document.createTextNode(getTitleTranslation(6)));
                    this.ths[y].title = getTitleTranslation(8);
                    continue;
                };

                if(y > (this.showWeeks ? 0 : -1)) {
                    but = document.createElement("span");
                    but.className = "fd-day-header";
                    if(oldIE) {
                        but.unselectable = "on";
                    };
                    this.ths[y].appendChild(but);
                };
            };

            but = null;

            this.trs             = this.table.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
            this.tds             = this.table.getElementsByTagName('tbody')[0].getElementsByTagName('td');
            this.butPrevYear     = document.getElementById(this.id + "-prev-year-but");
            this.butPrevMonth    = document.getElementById(this.id + "-prev-month-but");
            this.butToday        = document.getElementById(this.id + "-today-but");
            this.butNextYear     = document.getElementById(this.id + "-next-year-but");
            this.butNextMonth    = document.getElementById(this.id + "-next-month-but");

            if(this.noToday) {
                this.butToday.style.display = "none";
            };

            if(this.showWeeks) {
                this.wkThs = this.table.getElementsByTagName('tbody')[0].getElementsByTagName('th');
                this.div.className += " weeks-displayed";
            };

            tableBody = tableHead = tr = createThAndButton = createTH = null;

            this.updateTableHeaders();
            this.created = true;
            this.updateTable();

            if(this.staticPos) {
                this.visible = true;
                this.opacity = 100;
                this.div.style.visibility = "visible";
                this.div.style.display = "block";
                this.noFocus = true;
                this.fade();
                //this.checkPosition();
            } else {
                this.reposition();
                this.div.style.visibility = "visible";
                this.fade();
                this.noFocus = true;
                //this.checkPosition();
            };

            this.callback("domcreate", { "id":this.id });
        };

        this.transEnd = function() {
            o.div.style.display     = "none";
            o.div.style.visibility  = "hidden";
            setARIAProperty(o.div, "hidden", "true");
        };

        this.fade = function() {
            window.clearTimeout(o.fadeTimer);
            o.fadeTimer = null;
            if(cssAnimations) {
                o.opacity = o.opacityTo;
                if(o.opacityTo == 0) {
                    o.visible = false;
                    addEvent(o.div, transitionEnd, o.transEnd);
                    addClass(o.div, "fd-dp-fade");
                } else {
                    removeEvent(o.div, transitionEnd, o.transEnd);
                    o.visible               = true;
                    o.div.style.display     = "block";
                    o.div.style.visibility  = "visible";
                    setARIAProperty(o.div, "hidden", "false");
                    removeClass(o.div, "fd-dp-fade");
                };
                return;
            };

            var diff = Math.round(o.opacity + ((o.opacityTo - o.opacity) / 4));
            o.setOpacity(diff);

            if(Math.abs(o.opacityTo - diff) > 3 && !o.noFadeEffect) {
                o.fadeTimer = window.setTimeout(o.fade, 50);
            } else {
                o.setOpacity(o.opacityTo);
                if(o.opacityTo == 0) {
                    o.div.style.display    = "none";
                    o.div.style.visibility = "hidden";
                    setARIAProperty(o.div, "hidden", "true");
                    o.visible = false;
                } else {
                    //add x bug ie9
                    o.div.style.display     = "block";
                    o.div.style.visibility  = "visible";
                    //
                    setARIAProperty(o.div, "hidden", "false");
                    o.visible = true;
                };
            };
        };
        this.trackDrag = function(e) {
            e = e || window.event;
            var diffx = (e.pageX?e.pageX:e.clientX?e.clientX:e.x) - o.mx;
            var diffy = (e.pageY?e.pageY:e.clientY?e.clientY:e.Y) - o.my;
            o.div.style.left = Math.round(o.x + diffx) > 0 ? Math.round(o.x + diffx) + 'px' : "0px";
            o.div.style.top  = Math.round(o.y + diffy) > 0 ? Math.round(o.y + diffy) + 'px' : "0px";
            if(oldIE === 6 && !o.staticPos) {
                o.iePopUp.style.top    = o.div.style.top;
                o.iePopUp.style.left   = o.div.style.left;
            };
        };
        this.stopDrag = function(e) {
            var b = document.getElementsByTagName("body")[0];
            removeClass(b, "fd-drag-active");
            removeEvent(document,'mousemove',o.trackDrag, false);
            removeEvent(document,'mouseup',o.stopDrag, false);
            o.div.style.zIndex = 9999;
        };
        this.onmousedown = function(e) {
            e = e || document.parentWindow.event;
            var el     = e.target != null ? e.target : e.srcElement,
                origEl = el,
                hideDP = true,
                reg    = new RegExp("^fd-(but-)?" + o.id + "$");

            o.mouseDownElem = null;

            // Are we within the wrapper div or the button
            while(el) {
                if(el.id && el.id.length && el.id.search(reg) != -1) {
                    hideDP = false;
                    break;
                };
                try {
                    el = el.parentNode;
                } catch(err) {
                    break;
                };
            };

            // If not, then ...
            if(hideDP) {
                hideAll();
                return true;
            };

            if((o.div.className + origEl.className).search('fd-disabled') != -1) {
                return true;
            };

            // We check the mousedown events on the buttons
            if(origEl.id.search(new RegExp("^" + o.id + "(-prev-year-but|-prev-month-but|-next-month-but|-next-year-but)$")) != -1) {

                o.mouseDownElem = origEl;

                addEvent(document, "mouseup", o.clearTimer);
                addEvent(origEl, "mouseout",  o.clearTimer);

                var incs = {
                        "-prev-year-but":[0,-1,0],
                        "-prev-month-but":[0,0,-1],
                        "-next-year-but":[0,1,0],
                        "-next-month-but":[0,0,1]
                    },
                    check = origEl.id.replace(o.id, ""),
                    dateYYYYMM = Number(o.date.getFullYear() + pad(o.date.getMonth()+1));

                o.timerInc      = 800;
                o.timerSet      = true;
                o.dayInc        = incs[check][0];
                o.yearInc       = incs[check][1];
                o.monthInc      = incs[check][2];
                o.accellerator  = 1;

                if(!(o.currentYYYYMM == dateYYYYMM)) {
                    if((o.currentYYYYMM < dateYYYYMM && (o.yearInc == -1 || o.monthInc == -1)) || (o.currentYYYYMM > dateYYYYMM && (o.yearInc == 1 || o.monthInc == 1))) {
                        o.delayedUpdate = false;
                        o.timerInc = 1200;
                    } else {
                        o.delayedUpdate = true;
                    };
                };

                o.updateTable();

                return stopEvent(e);

            } else if(el.className.search("drag-enabled") != -1) {
                    o.mx = e.pageX ? e.pageX : e.clientX ? e.clientX : e.x;
                    o.my = e.pageY ? e.pageY : e.clientY ? e.clientY : e.Y;
                    o.x  = parseInt(o.div.style.left, 10);
                    o.y  = parseInt(o.div.style.top, 10);
                    addEvent(document,'mousemove',o.trackDrag, false);
                    addEvent(document,'mouseup',o.stopDrag, false);
                    addClass(document.getElementsByTagName("body")[0], "fd-drag-active");
                    o.div.style.zIndex = 10000;

                    return stopEvent(e);
            };
            return true;
        };
        this.onclick = function(e) {
            if((!cssAnimations && o.opacity != o.opacityTo) || o.disabled) {
                return stopEvent(e);
            };

            e = e || document.parentWindow.event;
            var el = e.target != null ? e.target : e.srcElement;

            while(el.parentNode) {

                // Are we within a valid i.e. clickable TD node
                if(el.tagName && el.tagName.toLowerCase() == "td") {

                    if(el.className.search(/cd-([0-9]{8})/) == -1 || el.className.search(noSelectionRegExp) != -1) {
                        return stopEvent(e);
                    };

                    var cellDate = el.className.match(/cd-([0-9]{8})/)[1];
                    o.date       = new Date(cellDate.substr(0,4),cellDate.substr(4,2)-1,cellDate.substr(6,2), 5, 0, 0);
                    o.dateSet    = new Date(o.date);
                    o.noFocus    = true;
                    o.callback("dateset", { "id":o.id, "date":o.dateSet, "dd":o.dateSet.getDate(), "mm":o.dateSet.getMonth() + 1, "yyyy":o.dateSet.getFullYear() });
                    o.returnFormattedDate();
                    o.hide();
                    o.stopTimer();
                    break;
                } else if(el.id && el.id == o.id + "-today-but") {
                    o.date = new Date();
                    o.updateTable();
                    o.stopTimer();
                    break;
                } else if(el.className.search(/date-picker-day-header/) != -1) {
                    var cnt = o.showWeeks ? -1 : 0,
                        elem = el;

                    while(elem.previousSibling) {
                        elem = elem.previousSibling;
                        if(elem.tagName && elem.tagName.toLowerCase() == "th") {
                            cnt++;
                        };
                    };

                    o.firstDayOfWeek = (o.firstDayOfWeek + cnt) % 7;
                    o.updateTableHeaders();
                    break;
                };
                try {
                    el = el.parentNode;
                } catch(err) {
                    break;
                };
            };

            return stopEvent(e);
        };

        this.show = function(autoFocus) {
            if(this.staticPos) {
                return;
            };

            var elem, elemID;
            for(elemID in this.formElements) {
                elem = document.getElementById(this.id);
                if(!elem || (elem && elem.disabled)) {
                    return;
                };
            };

            this.noFocus = true;

            // If the datepicker doesn't exist in the dom
            if(!this.created || !document.getElementById('fd-' + this.id)) {
                this.created    = false;
                this.fullCreate = false;
                this.create();
                this.fullCreate = true;
            } else {
                this.setDateFromInput();
                this.reposition();
            };

            this.noFocus = !!!autoFocus;

            if(this.noFocus) {
                this.clickActivated = true;
                this.showCursor = false;
                addEvent(document, "mousedown", this.onmousedown);
                if(mouseWheel) {
                    if (window.addEventListener && !window.devicePixelRatio) {
                        window.addEventListener('DOMMouseScroll', this.onmousewheel, false);
                    } else {
                        addEvent(document, "mousewheel", this.onmousewheel);
                        addEvent(window,   "mousewheel", this.onmousewheel);
                    };
                };
            } else {
                this.clickActivated = false;
                this.showCursor = true;
            };

            this.opacityTo = 100;
            this.div.style.display = "block";

            if(oldIE === 6) {
                this.iePopUp.style.width    = this.div.offsetWidth + "px";
                this.iePopUp.style.height   = this.div.offsetHeight + "px";
                this.iePopUp.style.display  = "block";
            };

            this.setNewFocus();
            this.fade();
            var butt = document.getElementById('fd-but-' + this.id);
            if(butt) {
                addClass(butt, "date-picker-button-active");
            };
            this.checkPosition();
        };

        this.hide = function() {
            if(!this.visible || !this.created || !document.getElementById('fd-' + this.id)) {
                return;
            };

            this.kbEvent = false;

            removeClass(o.div, "date-picker-focus");

            this.stopTimer();
            this.removeOnFocusEvents();
            this.clickActivated = false;
            this.noFocus = true;
            this.showCursor = false;
            this.setNewFocus();

            if(this.staticPos) {
                return;
            };

            if(this.statusBar) {
                this.updateStatus(getTitleTranslation(9));
            };

            var butt = document.getElementById('fd-but-' + this.id);

            if(butt) {
                removeClass(butt, "date-picker-button-active");
            };

            removeEvent(document, "mousedown", this.onmousedown);

            if(mouseWheel) {
                if (window.addEventListener && !window.devicePixelRatio) {
                    try {
                        window.removeEventListener('DOMMouseScroll', this.onmousewheel, false);
                    } catch(err) {};
                } else {
                    removeEvent(document, "mousewheel", this.onmousewheel);
                    removeEvent(window,   "mousewheel", this.onmousewheel);
                };
            };


            if(oldIE === 6) {
                this.iePopUp.style.display = "none";
            };

            this.opacityTo = 0;
            this.fade();
        };

        this.onblur = function(e) {
            o.removeCursorHighlight();
            o.hide();
        };
        // The current cursor cell gains focus
        this.onfocus = function(e) {
            o.noFocus = false;
            addClass(o.div, "date-picker-focus");
            if(o.statusBar) {
                o.updateStatus(printFormattedDate(o.date, o.statusFormat, true));
            };
            o.showCursor = true;
            o.addCursorHighlight();
            o.addOnFocusEvents();
        };
        this.onmousewheel = function(e) {
            e = e || document.parentWindow.event;
            var delta = 0;

            if (e.wheelDelta) {
                delta = e.wheelDelta/120;
                if (isOpera && window.opera.version() < 9.2) {
                    delta = -delta;
                };
            } else if(e.detail) {
                delta = -e.detail/3;
            };

            var n = o.date.getDate(),
                d = new Date(o.date),
                inc = delta > 0 ? 1 : -1;

            d.setDate(2);
            d.setMonth(d.getMonth() + inc * 1);
            d.setDate(Math.min(n, daysInMonth(d.getMonth(),d.getFullYear())));

            if(o.outOfRange(d)) {
                return stopEvent(e);
            };

            o.date = new Date(d);

            o.updateTable();

            if(o.statusBar) {
                o.updateStatus(printFormattedDate(o.date, o.statusFormat, true));
            };

            return stopEvent(e);
        };
        this.onkeydown = function (e) {
            o.stopTimer();

            if(!o.visible) {
                return false;
            };

            e = e || document.parentWindow.event;

            var kc = e.keyCode ? e.keyCode : e.charCode;

            if(kc == 13) {
                // RETURN/ENTER: close & select the date
                var td = document.getElementById(o.id + "-date-picker-hover");
                if(!td || td.className.search(/cd-([0-9]{8})/) == -1 || td.className.search(/out-of-range|day-disabled/) != -1) {
                    return stopEvent(e);
                };
                o.dateSet = new Date(o.date);
                o.callback("dateset", o.createCbArgObj());
                o.returnFormattedDate();
                o.hide();
                return stopEvent(e);
            } else if(kc == 27) {
                // ESC: close, no date selection, refocus on popup button
                if(!o.staticPos) {
                    o.hide();
                    var butt = document.getElementById('fd-but-' + o.id);
                    if(butt) {
                        setTimeout(function(){try{butt.focus()}catch(err){}},0);
                    };
                    return stopEvent(e);
                };
                return true;
            } else if(kc == 32 || kc == 0) {
                // SPACE: goto todays date
                o.date = new Date();
                o.updateTable();

                return stopEvent(e);
            } else if(kc == 9) {
                // TAB: pass focus - non popup datepickers only
                if(!o.staticPos) {
                    return stopEvent(e);
                };
                return true;
            };
            // TODO - test the need for the IE specific stuff in IE9

            // Internet Explorer fires the keydown event faster than the JavaScript engine can
            // update the interface. The following attempts to fix this.

            if(oldIE) {
                if(new Date().getTime() - o.interval.getTime() < 50) { return stopEvent(e); };
                o.interval = new Date();
            };

            // A number key has been pressed so change the first day of the week
            if((kc > 49 && kc < 56) || (kc > 97 && kc < 104)) {
                if(kc > 96) {
                    kc -= (96-48);
                };
                kc -= 49;
                o.firstDayOfWeek = (o.firstDayOfWeek + kc) % 7;
                o.updateTableHeaders();
                return stopEvent(e);
            };

            // If outside any other tested keycodes then let the keystroke pass
            if(kc < 33 || kc > 40) {
                return true;
            };

            var d = new Date(o.date),
                cursorYYYYMM = o.date.getFullYear() + pad(o.date.getMonth()+1),
                tmp;

            // HOME: Set date to first day of current month
            if(kc == 36) {
                d.setDate(1);
            // END: Set date to last day of current month
            } else if(kc == 35) {
                d.setDate(daysInMonth(d.getMonth(),d.getFullYear()));
            // PAGE UP & DOWN
            } else if ( kc == 33 || kc == 34) {
                var inc = (kc == 34) ? 1 : -1;

                // CTRL + PAGE UP/DOWN: Moves to the same date in the previous/next year
                if(e.ctrlKey) {
                    d.setFullYear(d.getFullYear() + inc * 1);
                // PAGE UP/DOWN: Moves to the same date in the previous/next month
                } else {
                    var n = o.date.getDate();

                    d.setDate(2);
                    d.setMonth(d.getMonth() + inc * 1);
                    d.setDate(Math.min(n, daysInMonth(d.getMonth(),d.getFullYear())));
                };
            // LEFT ARROW
            } else if ( kc == 37 ) {
                d = new Date(o.date.getFullYear(), o.date.getMonth(), o.date.getDate() - 1, 5, 0, 0);
            // RIGHT ARROW
            } else if ( kc == 39 || kc == 34) {
                d = new Date(o.date.getFullYear(), o.date.getMonth(), o.date.getDate() + 1, 5, 0, 0);
            // UP ARROW
            } else if ( kc == 38 ) {
                d = new Date(o.date.getFullYear(), o.date.getMonth(), o.date.getDate() - 7, 5, 0, 0);
            // DOWN ARROW
            } else if ( kc == 40 ) {
                d = new Date(o.date.getFullYear(), o.date.getMonth(), o.date.getDate() + 7, 5, 0, 0);
            };

            // If the new date is out of range then disallow action
            if(o.outOfRange(d)) {
                return stopEvent(e);
            };

            // Otherwise set the new cursor date
            o.date = d;

            // Update the status bar if needs be
            if(o.statusBar) {
                o.updateStatus(o.getBespokeTitle(o.date.getFullYear(),o.date.getMonth() + 1,o.date.getDate()) || printFormattedDate(o.date, o.statusFormat, true));
            };

            // YYYYMMDD format String of the current cursor date
            var t = String(o.date.getFullYear()) + pad(o.date.getMonth()+1) + pad(o.date.getDate());

            // If we need to redraw the UI completely
            if(e.ctrlKey || (kc == 33 || kc == 34) || t < o.firstDateShown || t > o.lastDateShown) {
                o.updateTable();
                if(oldIE) {
                    o.interval = new Date();
                };
            // Just highlight current cell
            } else {
                // Do we need to disable the today button for this date
                if(!o.noToday) {
                    o.disableTodayButton();
                };
                // Remove focus from the previous cell
                o.removeOldFocus();
                // Show/hide the month & year buttons
                o.showHideButtons(o.date);

                // Locate this TD
                for(var i = 0, td; td = o.tds[i]; i++) {
                    if(td.className.search("cd-" + t) == -1) {
                        continue;
                    };

                    td.id = o.id + "-date-picker-hover";
                    o.setNewFocus();
                    break;
                };
            };

            return stopEvent(e);
        };
        this.onmouseout = function(e) {
            e = e || document.parentWindow.event;
            var p = e.toElement || e.relatedTarget;

            while(p && p != this) {
                try {
                    p = p.parentNode;
                } catch(e) {
                    p = this;
                };
            };

            if(p == this) {
                return false;
            };

            if(o.clickActivated || (o.staticPos && !o.kbEventsAdded)) {
                o.showCursor = false;
                o.removeCursorHighlight();
            };

            if(o.currentTR) {
                o.currentTR.className = "";
                o.currentTR = null;
            };

            if(o.statusBar) {
                o.updateStatus(o.dateSet ? o.getBespokeTitle(o.dateSet.getFullYear(),o.dateSet.getMonth() + 1,o.dateSet.getDate()) || printFormattedDate(o.dateSet, o.statusFormat, true) : getTitleTranslation(9));
            };
        };
        this.onmouseover = function(e) {
            e = e || document.parentWindow.event;
            var el = e.target != null ? e.target : e.srcElement;
            while(el.nodeType != 1) {
                el = el.parentNode;
            };

            if(!el || ! el.tagName) {
                return;
            };

            o.noFocus = true;

            var statusText = getTitleTranslation(9);
            if(o.clickActivated || (o.staticPos && !o.kbEventsAdded)) {
                o.showCursor = false;
            };


            switch (el.tagName.toLowerCase()) {
                case "td":
                    if(el.className.search(/date-picker-unused|out-of-range/) != -1) {
                        statusText = getTitleTranslation(9);
                    } if(el.className.search(/cd-([0-9]{8})/) != -1) {
                        o.showCursor = true;

                        o.stopTimer();
                        var cellDate = el.className.match(/cd-([0-9]{8})/)[1];

                        o.removeOldFocus();
                        el.id = o.id+"-date-picker-hover";
                        o.setNewFocus();

                        o.date = new Date(+cellDate.substr(0,4),+cellDate.substr(4,2)-1,+cellDate.substr(6,2), 5, 0, 0);
                        if(!o.noToday) {
                            o.disableTodayButton();
                        };

                        statusText = o.getBespokeTitle(+cellDate.substr(0,4),+cellDate.substr(4,2),+cellDate.substr(6,2)) || printFormattedDate(o.date, o.statusFormat, true);
                    };
                    break;
                case "th":
                    if(!o.statusBar) {
                        break;
                    };
                    if(el.className.search(/drag-enabled/) != -1) {
                        statusText = getTitleTranslation(10);
                    } else if(el.className.search(/date-picker-week-header/) != -1) {
                        var txt = el.firstChild ? el.firstChild.nodeValue : "";
                        statusText = txt.search(/^(\d+)$/) != -1 ? getTitleTranslation(7, [txt, txt < 3 && o.date.getMonth() == 11 ? getWeeksInYear(o.date.getFullYear()) + 1 : getWeeksInYear(o.date.getFullYear())]) : getTitleTranslation(9);
                    };
                    break;
                case "span":
                    if(!o.statusBar) {
                        break;
                    };

                    if(el.className.search(/day-([0-6])/) != -1) {
                        var day = el.className.match(/day-([0-6])/)[1];
                        statusText = getTitleTranslation(11, [getDayTranslation(day, false)]);
                    } else if(el.className.search(/(drag-enabled|today-but|prev-(year|month)|next-(year|month))/) != -1 && el.className.search(/disabled/) == -1) {
                        statusText = getTitleTranslation({"drag-enabled":10,"prev-year":2,"prev-month":0,"next-year":3,"next-month":1,"today-but":12}[el.className.match(/(drag-enabled|today-but|prev-(year|month)|next-(year|month))/)[0]]);
                    };

                    break;
                default:
                    statusText = "";
            };
            while(el.parentNode) {
                el = el.parentNode;
                if(el.nodeType == 1 && el.tagName.toLowerCase() == "tr") {
                    if(o.currentTR) {
                        if(el == o.currentTR) {
                            break;
                        };
                        o.currentTR.className = "";
                    };
                    el.className = "dp-row-highlight";
                    o.currentTR = el;
                    break;
                };
            };
            if(o.statusBar && statusText) {
                o.updateStatus(statusText);
            };

            if(!o.showCursor) {
                o.removeCursorHighlight();
            };
        };
        this.clearTimer = function() {
            o.stopTimer();
            o.timerInc      = 800;
            o.yearInc       = 0;
            o.monthInc      = 0;
            o.dayInc        = 0;

            removeEvent(document, "mouseup", o.clearTimer);
            if(o.mouseDownElem != null) {
                removeEvent(o.mouseDownElem, "mouseout",  o.clearTimer);
            };
            o.mouseDownElem = null;
        };

        var o = this;

        this.setDateFromInput();

        if(this.staticPos) {
            this.create();
        } else {
            this.createButton();
        };

        (function() {
            var elemID,
                elem,
                elemCnt = 0;

            for(elemID in o.formElements) {
                elem = document.getElementById(elemID);
                if(elem && elem.tagName && elem.tagName.search(/select|input/i) != -1) {
                    addEvent(elem, "change", o.changeHandler);
                    if(elemCnt == 0 && elem.form) {
                        addEvent(elem.form, "reset", o.reset);
                    };
                    elemCnt++;
                };

                if(!elem || elem.disabled == true) {
                    o.disableDatePicker();
                };
            };
        })();

        // We have fully created the datepicker...
        this.fullCreate = true;
    };
    datePicker.prototype.addButtonEvents = function(but) {
        function buttonEvent (e) {
            e = e || window.event;

            var inpId     = this.id.replace('fd-but-',''),
                dpVisible = isVisible(inpId),
                autoFocus = false,
                kbEvent   = datePickers[inpId].kbEvent;


            if(kbEvent) {
                datePickers[inpId].kbEvent = false;
                return;
            };

            if(e.type == "keydown") {
                var kc = e.keyCode != null ? e.keyCode : e.charCode;
                if(kc != 13) return true;
                datePickers[inpId].kbEvent = true;
                if(dpVisible) {
                    removeClass(this, "date-picker-button-active");
                    hideAll();
                    return stopEvent(e);
                };
                autoFocus = true;
            } else {
                datePickers[inpId].kbEvent = false;
            };

            if(!dpVisible) {
                addClass(this, "date-picker-button-active");
                hideAll(inpId);
                showDatePicker(inpId, autoFocus);

            } else {
                removeClass(this, "date-picker-button-active");
                hideAll();
            };

            return stopEvent(e);
        };

        but.onclick     = buttonEvent;
        but.onkeydown   = buttonEvent;

        if(!buttonTabIndex) {
            setTabIndex(but, -1);
        } else {
            setTabIndex(but, this.bespokeTabIndex);
        };
    };

    datePicker.prototype.createButton = function() {

        if(this.staticPos || document.getElementById("fd-but-" + this.id)) {
                return;
        };

        var inp         = document.getElementById(this.id),
            span        = document.createElement('span'),
            but         = document.createElement('a');

        but.href        = "#" + this.id;
        but.className   = "date-picker-control";
        but.title       = getTitleTranslation(5);
        but.id          = "fd-but-" + this.id;

        span.appendChild(document.createTextNode(nbsp));
        but.appendChild(span);

        span = document.createElement('span');
        span.className = "fd-screen-reader";
        span.appendChild(document.createTextNode(but.title));
        but.appendChild(span);

        // Set the ARIA role to be "button"
        setARIARole(but, "button");

        // Set a "haspopup" ARIA property
        setARIAProperty(but, "haspopup", true);

        if(this.positioned && document.getElementById(this.positioned)) {
            document.getElementById(this.positioned).appendChild(but);
        } else {
            inp.parentNode.insertBefore(but, inp.nextSibling);
        };

        this.addButtonEvents(but);
        //ADDED FOCUS ON INPUT // REMOVE FOCUS MOBILE

        var ___input = document.getElementById(this.id);
        if(___input){
          this.addButtonEvents(___input);
        }

        but = null;

        this.callback("dombuttoncreate", {id:this.id});
    };
    datePicker.prototype.setBespokeTitles = function(titles) {
        this.bespokeTitles = {};
        this.addBespokeTitles(titles);
    };
    datePicker.prototype.addBespokeTitles = function(titles) {
        for(var dt in titles) {
            if(titles.hasOwnProperty(dt)) {
                this.bespokeTitles[dt] = titles[dt];
            };
        };
    };
    datePicker.prototype.getBespokeTitle = function(y,m,d) {
        var dt,
            dtFull,
            yyyymmdd = y + String(pad(m)) + pad(d);

        // Try the datepickers bespoke titles
        for(dt in this.bespokeTitles) {
            if(this.bespokeTitles.hasOwnProperty(dt)) {
                dtFull = String(dt).replace(/^(\*\*\*\*)/, y).replace(/^(\d\d\d\d)(\*\*)/, "$1"+ pad(m));
                if(dtFull == yyyymmdd) {
                    return this.bespokeTitles[dt];
                };
            };
        };

        // Try the generic bespoke titles
        for(dt in bespokeTitles) {
            if(bespokeTitles.hasOwnProperty(dt)) {
                dtFull = String(dt).replace(/^(\*\*\*\*)/, y).replace(/^(\d\d\d\d)(\*\*)/, "$1"+ pad(m));
                if(dtFull == yyyymmdd) {
                    return bespokeTitles[dt];
                };
            };
        };

        return false;
    };
    datePicker.prototype.returnSelectedDate = function() {
        return this.dateSet;
    };
    datePicker.prototype.setRangeLow = function(range) {
        if(String(range).search(rangeRegExp) == -1) {
            if(debug) {
                throw "Invalid value passed to setRangeLow method: " + range;
            };
            return false;
        };
        this.rangeLow = range;
        if(!this.inUpdate) {
            this.setDateFromInput();
        };
    };
    datePicker.prototype.setRangeHigh = function(range) {
        if(String(range).search(rangeRegExp) == -1) {
            if(debug) {
                throw "Invalid value passed to setRangeHigh method: " + range;
            };
            return false;
        };
        this.rangeHigh = range;
        if(!this.inUpdate) {
            this.setDateFromInput();
        };
    };
    datePicker.prototype.setPeriodHighlight = function(start, end) {
        /*if(String(start).search(rangeRegExp) == -1) {
            if(debug) {
                throw "Invalid value passed to setRangeHigh method: " + start;
            };
            return false;
        };*/
        this.startPeriod = start;
        this.endPeriod = end;

    };
    datePicker.prototype.setDisabledDays = function(dayArray) {
        if(!dayArray.length || dayArray.join("").search(/^([0|1]{7})$/) == -1) {
            if(debug) {
                throw "Invalid values located when attempting to call setDisabledDays";
            };
            return false;
        };
        this.disabledDays = dayArray;
        if(!this.inUpdate) {
            this.setDateFromInput();
        };
    };

    datePicker.prototype.setDisabledDates = function(dateObj) {
        this.filterDateList(dateObj, true);
    };
    datePicker.prototype.setEnabledDates = function(dateObj) {
        this.filterDateList(dateObj, false);
    };
    datePicker.prototype.addDisabledDates = function(dateObj) {
        this.addDatesToList(dateObj, true);
    };
    datePicker.prototype.addEnabledDates = function(dateObj) {
        this.addDatesToList(dateObj, false);
    };
    datePicker.prototype.filterDateList = function(dateObj, type) {
        var tmpDates = [];
        for(var i = 0; i < this.dateList.length; i++) {
            if(this.dateList[i].type != type) {
                tmpDates.push(this.dateList[i]);
            };
        };

        this.dateList = tmpDates.concat();
        this.addDatesToList(dateObj, type);
    };
    datePicker.prototype.addDatesToList = function(dateObj, areDisabled) {
        var startD;
        for(startD in dateObj) {
            if(String(startD).search(wcDateRegExp) != -1 && (dateObj[startD] == 1 || String(dateObj[startD]).search(wcDateRegExp) != -1)) {

                if(dateObj[startD] != 1 && Number(String(startD).replace(/^\*\*\*\*/, 2010).replace(/^(\d\d\d\d)(\*\*)/, "$1"+"22")) > Number(String(dateObj[startD]).replace(/^\*\*\*\*/, 2010).replace(/^(\d\d\d\d)(\*\*)/, "$1"+"22"))) {
                    continue;
                };

                this.dateList.push({
                    type:!!(areDisabled),
                    rLow:startD,
                    rHigh:dateObj[startD]
                });
            };
        };

        if(!this.inUpdate) {
            this.setDateFromInput();
        };
    };
    datePicker.prototype.setSelectedDate = function(yyyymmdd) {
        if(String(yyyymmdd).search(wcDateRegExp) == -1) {
            return false;
        };

        var match = yyyymmdd.match(rangeRegExp),
            dt    = new Date(+match[2],+match[3]-1,+match[4], 5, 0, 0);

        if(!dt || isNaN(dt) || !this.canDateBeSelected(dt)) {
            return false;
        };

        this.dateSet = new Date(dt);

        if(!this.inUpdate) {
            this.updateTable();
        };

        this.callback("dateset", this.createCbArgObj());
        this.returnFormattedDate();
    };
    datePicker.prototype.checkSelectedDate = function() {
        if(this.dateSet && !this.canDateBeSelected(this.dateSet)) {
            this.dateSet = null;
        };
        if(!this.inUpdate) {
            this.updateTable();
        };
    };
    datePicker.prototype.addOnFocusEvents = function() {
        //FOCUS EVENTS ADDED

        if(this.kbEventsAdded || this.noFocus) {
            return;
        };

        addEvent(document, "keypress", this.onkeydown);
        addEvent(document, "mousedown", this.onmousedown);

        if(oldIE) {
            removeEvent(document, "keypress", this.onkeydown);
            addEvent(document, "keydown", this.onkeydown);
        };
        if(window.devicePixelRatio) {
            removeEvent(document, "keypress", this.onkeydown);
            addEvent(document, "keydown", this.onkeydown);
        };
        this.noFocus = false;
        this.kbEventsAdded = true;
    };
    datePicker.prototype.removeOnFocusEvents = function() {

        if(!this.kbEventsAdded) {
            return;
        };
        removeEvent(document, "keypress",  this.onkeydown);
        removeEvent(document, "keydown",   this.onkeydown);
        removeEvent(document, "mousedown", this.onmousedown);

        this.kbEventsAdded = false;
    };
    datePicker.prototype.stopTimer = function() {
        this.timerSet = false;
        window.clearTimeout(this.timer);
    };
    datePicker.prototype.setOpacity = function(op) {
        this.div.style.opacity = op/100;
        this.div.style.filter = 'alpha(opacity=' + op + ')';
        this.opacity = op;
    };
    datePicker.prototype.truePosition = function(element) {
        var pos = this.cumulativeOffset(element);
        if(isOpera) {
                return pos;
        };
        var iebody      = (document.compatMode && document.compatMode != "BackCompat")? document.documentElement : document.body,
            dsocleft    = document.all ? iebody.scrollLeft : window.pageXOffset,
            dsoctop     = document.all ? iebody.scrollTop  : window.pageYOffset,
            posReal     = this.realOffset(element);
        return [pos[0] - posReal[0] + dsocleft, pos[1] - posReal[1] + dsoctop];
    };
    datePicker.prototype.realOffset = function(element) {
        var t = 0, l = 0;
        do {
            t += element.scrollTop  || 0;
            l += element.scrollLeft || 0;
            element = element.parentNode;
        } while(element);
        return [l, t];
    };
    datePicker.prototype.cumulativeOffset = function(element) {
        var t = 0, l = 0;
        do {
            t += element.offsetTop  || 0;
            l += element.offsetLeft || 0;
            element = element.offsetParent;
        } while(element);
        return [l, t];
    };
    datePicker.prototype.outOfRange = function(tmpDate) {

        if(!this.rangeLow && !this.rangeHigh) {
            return false;
        };

        var level = false;

        if(!tmpDate) {
            level   = true;
            tmpDate = this.date;
        };

        var d  = pad(tmpDate.getDate()),
            m  = pad(tmpDate.getMonth() + 1),
            y  = tmpDate.getFullYear(),
            dt = String(y)+String(m)+String(d);

        if(this.rangeLow && +dt < +this.rangeLow) {
            if(!level) {
                return true;
            };
            this.date = new Date(this.rangeLow.substr(0,4), this.rangeLow.substr(4,2)-1, this.rangeLow.substr(6,2), 5, 0, 0);
            return false;
        };
        if(this.rangeHigh && +dt > +this.rangeHigh) {
            if(!level) {
                return true;
            };
            this.date = new Date(this.rangeHigh.substr(0,4), this.rangeHigh.substr(4,2)-1, this.rangeHigh.substr(6,2), 5, 0, 0);
        };
        return false;
    };
    datePicker.prototype.canDateBeSelected = function(tmpDate) {
        if(!tmpDate || isNaN(tmpDate)) {
            return false;
        };

        var d  = pad(tmpDate.getDate()),
            m  = pad(tmpDate.getMonth() + 1),
            y  = tmpDate.getFullYear(),
            dt = y + "" + m + "" + d,
            dd = this.getDateExceptions(y, m),
            wd = tmpDate.getDay() == 0 ? 7 : tmpDate.getDay();

        // If date out of range
        if((this.rangeLow && +dt < +this.rangeLow)
           ||
           (this.rangeHigh && +dt > +this.rangeHigh)
           ||
           // or the date has been explicitly disabled
           ((dt in dd) && dd[dt] == 1)
           ||
           // or the date lies on a disabled weekday and it hasn't been explicitly enabled
           (this.disabledDays[wd-1] && (!(dt in dd) || ((dt in dd) && dd[dt] == 1)))) {
                return false;
        };

        return true;
    };
    datePicker.prototype.updateStatus = function(msg) {
        removeChildNodes(this.statusBar);

        // All this arseing about just for sups in the footer... nice typography and all that...
        if(msg && this.statusFormat.search(/%S/) != -1 && msg.search(/([0-9]{1,2})(st|nd|rd|th)/) != -1) {
            msg = cbSplit(msg.replace(/([0-9]{1,2})(st|nd|rd|th)/, "$1<sup>$2</sup>"), /<sup>|<\/sup>/);
            var dc = document.createDocumentFragment();
            for(var i = 0, nd; nd = msg[i]; i++) {
                if(/^(st|nd|rd|th)$/.test(nd)) {
                    var sup = document.createElement("sup");
                    sup.appendChild(document.createTextNode(nd));
                    dc.appendChild(sup);
                } else {
                    dc.appendChild(document.createTextNode(nd));
                };
            };
            this.statusBar.appendChild(dc);
        } else {
            this.statusBar.appendChild(document.createTextNode(msg ? msg : getTitleTranslation(9)));
        };
    };

    /* Still needs work... */
    datePicker.prototype.setDateFromInput = function() {
        var origDateSet = this.dateSet,
            m           = false,
            but         = this.staticPos ? false : document.getElementById("fd-but-" + this.id),
            e           = localeImport.imported ? [].concat(localeDefaults.fullMonths).concat(localeDefaults.monthAbbrs) : [],
            l           = localeImport.imported ? [].concat(localeImport.fullMonths).concat(localeImport.monthAbbrs) : [],
            eosRegExp   = /(3[01]|[12][0-9]|0?[1-9])(st|nd|rd|th)/i,
            elemCnt     = 0,
            dt          = false,
            allFormats, i, elemID, elem, elemFmt, d, y, elemVal, dp, mp, yp;

        // Reset the internal dateSet variable
        this.dateSet = null;

        // Try and get a year, month and day from the form element values
        for(elemID in this.formElements) {

            elem = document.getElementById(elemID);

            if(!elem) {
                return false;
            };

            elemCnt++;

            elemVal = String(elem.value);

            if(!elemVal) {
                continue;
            };

            elemFmt     = this.formElements[elemID];
            allFormats  = [elemFmt];
            dt          = false;
            dp          = elemFmt.search(dPartsRegExp) != -1;
            mp          = elemFmt.search(mPartsRegExp) != -1;
            yp          = elemFmt.search(yPartsRegExp) != -1;

            // Try to assign some default date formats to throw at
            // the (simple) regExp parser for single date parts.
            if(!(dp && mp && yp)) {
                if(yp && !(mp || dp)) {
                    allFormats = allFormats.concat([
                        "%Y",
                        "%y"
                        ]);
                } else if(mp && !(yp || dp)) {
                    allFormats = allFormats.concat([
                        "%M",
                        "%F",
                        "%m",
                        "%n"
                        ]);
                } else if(dp && !(yp || mp)) {
                    allFormats = allFormats.concat([
                        "%d%",
                        "%j"
                        ]);
                };
            };

            for(i = 0; i < allFormats.length; i++) {
                dt = parseDateString(elemVal, allFormats[i]);

                if(dt) {
                    if(!d && dp && dt.d) {
                        d = dt.d;
                    };
                    if(m === false && mp && dt.m) {
                        m = dt.m;
                    };
                    if(!y && yp && dt.y) {
                        y = dt.y;
                    };
                };

                if(((dp && d) || !dp)
                   &&
                   ((mp && !m === false) || !mp)
                   &&
                   ((yp && y) || !yp)) {
                    break;
                };
            };
        };

        // Last ditch attempt at date parsing for single inputs that
        // represent the day, month and year parts of the date format.
        // I'm - thankfully - passing this responsibility off to the browser.
        // Date parsing in js sucks but the browsers' in-built Date.parse method
        // will inevitably be better than anything I would hazard to write.
        // Date.parse is implementation dependant though so don't expect
        // consistency, rhyme or reason.
        if(dateParseFallback && (!d || m === false || !y) && dp && mp && yp && elemCnt == 1 && elemVal) {
            // If locale imported then replace month names with English
            // counterparts if necessary
            if(localeImport.imported) {
                for(i = 0; i < l.length; i++) {
                    elemVal = elemVal.replace(new RegExp(l[i], "i"), e[i]);
                };
            };

            // Remove English ordinal suffix
            if(elemVal.search(eosRegExp) != -1) {
                elemVal = elemVal.replace(eosRegExp, elemVal.match(eosRegExp)[1]);
            };

            // Older browsers have problems with dashes so we replace with
            // slashes which appear to be supported by all and then try to use
            // the in-built Date Object to parse a valid date
            dt = new Date(elemVal.replace(new RegExp("\-", "g"), "/"));

            if(dt && !isNaN(dt)) {
                d = dt.getDate();
                m = dt.getMonth() + 1;
                y = dt.getFullYear();
            };
        };

        dt = false;

        if(d && !(m === false) && y) {
            if(+d > daysInMonth(+m - 1, +y)) {
                d  = daysInMonth(+m - 1, +y);
                dt = false;
            } else {
                dt = new Date(+y, +m - 1, +d, 5, 0, 0);
            };
        };

        if(but) {
            removeClass(but, "date-picker-dateval");
        };

        if(!dt || isNaN(dt)) {
            var newDate = new Date(y || new Date().getFullYear(), !(m === false) ? m - 1 : new Date().getMonth(), 1, 5, 0, 0);
            this.date = this.cursorDate ? new Date(+this.cursorDate.substr(0,4), +this.cursorDate.substr(4,2) - 1, +this.cursorDate.substr(6,2), 5, 0, 0) : new Date(newDate.getFullYear(), newDate.getMonth(), Math.min(+d || new Date().getDate(), daysInMonth(newDate.getMonth(), newDate.getFullYear())), 5, 0, 0);

            this.outOfRange();
            if(this.fullCreate) {
                this.updateTable();
            };
            return;
        };

        dt.setHours(5);
        this.date = new Date(dt);
        this.outOfRange();

        if(dt.getTime() == this.date.getTime() && this.canDateBeSelected(this.date)) {
            this.dateSet = new Date(this.date);
            if(but) {
                addClass(but, "date-picker-dateval");
            };
            this.returnFormattedDate(true);
        };

        if(this.fullCreate) {
            this.updateTable();
        };
    };
    datePicker.prototype.setSelectIndex = function(elem, indx) {
        for(var opt = elem.options.length-1; opt >= 0; opt--) {
            if(elem.options[opt].value == indx) {
                elem.selectedIndex = opt;
                return;
            };
        };
    };
    datePicker.prototype.returnFormattedDate = function(noFocus) {
        var but = this.staticPos ? false : document.getElementById("fd-but-" + this.id);

        if(!this.dateSet) {
            if(but) {
                removeClass(but, "date-picker-dateval");
            };
            return;
        };

        var d   = pad(this.dateSet.getDate()),
            m   = pad(this.dateSet.getMonth() + 1),
            y   = this.dateSet.getFullYear(),
            el  = false,
            elemID, elem, elemFmt, fmtDate;

        noFocus = !!noFocus;

        for(elemID in this.formElements) {
            elem    = document.getElementById(elemID);

            if(!elem) {
                return;
            };

            if(!el) {
                el = elem;
            };

            elemFmt = this.formElements[elemID];

            fmtDate = printFormattedDate(this.dateSet, elemFmt, returnLocaleDate);
            if(elem.tagName.toLowerCase() == "input") {
                elem.value = fmtDate;
            } else {
                this.setSelectIndex(elem, fmtDate);
            };
        };

        if(this.staticPos) {
            this.noFocus = true;
            this.updateTable();
            this.noFocus = false;
        } else if(but) {
            addClass(but, "date-picker-dateval");
        };

        if(this.fullCreate) {
            if(el.type && el.type != "hidden" && !noFocus) {
                try{
                    el.focus();
                } catch(err) {};
            };
        };

        if(!noFocus) {
            this.callback("datereturned", this.createCbArgObj());
        };
    };
    datePicker.prototype.disableDatePicker = function() {
        if(this.disabled) {
            return;
        };

        if(this.staticPos) {
            this.removeOnFocusEvents();
            this.removeOldFocus();
            this.noFocus = true;
            addClass(this.div, "date-picker-disabled");
            this.table.onmouseover = this.table.onclick = this.table.onmouseout = this.table.onmousedown = null;

            removeEvent(document, "mousedown", this.onmousedown);
            removeEvent(document, "mouseup",   this.clearTimer);
        } else {
            if(this.visible) {
                this.hide();
            };
            var but = document.getElementById("fd-but-" + this.id);
            if(but) {
                addClass(but, "date-picker-control-disabled");
                // Set a "disabled" ARIA state
                setARIAProperty(but, "disabled", true);
                but.onkeydown = but.onclick = function() {
                    return false;
                };
                setTabIndex(but, -1);
                but.title = "";
            }
        };

        clearTimeout(this.timer);
        this.disabled = true;
    };
    datePicker.prototype.enableDatePicker = function() {

        if(!this.disabled) {
            return;

        };

        if(this.staticPos) {
            this.removeOldFocus();

            if(this.dateSet != null) {
                this.date = this.dateSet;
            };
            this.noFocus = true;
            this.updateTable();
            removeClass(this.div, "date-picker-disabled");
            this.disabled = false;
            this.table.onmouseover = this.onmouseover;
            this.table.onmouseout  = this.onmouseout;
            this.table.onclick     = this.onclick;
            this.table.onmousedown = this.onmousedown;
        } else {
            var but = document.getElementById("fd-but-" + this.id);
            if(but) {
                removeClass(but, "date-picker-control-disabled");
                // Reset the "disabled" ARIA state
                setARIAProperty(but, "disabled", false);
                this.addButtonEvents(but);
                but.title = getTitleTranslation(5);
            };
            // //AGGIUNGO FOCUS SU INPUT
            // var ___input = document.getElementById(this.id);
            // if(___input){
            //   this.addButtonEvents(___input);
            // }
        };

        this.disabled = false;
    };
    datePicker.prototype.disableTodayButton = function() {
        var today = new Date();
        removeClass(this.butToday, "fd-disabled");
        if(this.outOfRange(today)
           ||
           (this.date.getDate() == today.getDate()
            &&
            this.date.getMonth() == today.getMonth()
            &&
            this.date.getFullYear() == today.getFullYear())
            ) {
            addClass(this.butToday, "fd-disabled");
        };
    };
    datePicker.prototype.updateTableHeaders = function() {
        var colspanTotal = this.showWeeks ? 8 : 7,
            colOffset    = this.showWeeks ? 1 : 0,
            d, but;

        for(var col = colOffset; col < colspanTotal; col++ ) {
            d = (this.firstDayOfWeek + (col - colOffset)) % 7;
            this.ths[col].title = getDayTranslation(d, false);

            if(col > colOffset) {
                but = this.ths[col].getElementsByTagName("span")[0];
                removeChildNodes(but);

                but.appendChild(document.createTextNode(getDayTranslation(d, true)));
                but.title = this.ths[col].title;
                but = null;
            } else {
                removeChildNodes(this.ths[col]);
                this.ths[col].appendChild(document.createTextNode(getDayTranslation(d, true)));
            };

            removeClass(this.ths[col], "date-picker-highlight");
            if(this.highlightDays[d]) {
                addClass(this.ths[col], "date-picker-highlight");
            };
        };

        if(this.created) {
            this.updateTable();
        };
    };
    datePicker.prototype.callback = function(type, args) {
        if(!type || !(type in this.callbacks)) {
            return false;
        };

        var ret = false,
            func;

        for(func = 0; func < this.callbacks[type].length; func++) {
            ret = this.callbacks[type][func](args || this.id);
        };

        return ret;
    };
    datePicker.prototype.showHideButtons = function(tmpDate) {
        if(!this.butPrevYear) {
            return;
        };

        var tdm = tmpDate.getMonth(),
            tdy = tmpDate.getFullYear();

        if(this.outOfRange(new Date((tdy - 1), tdm, daysInMonth(+tdm, tdy-1), 5, 0, 0))) {
            addClass(this.butPrevYear, "fd-disabled");
            if(this.yearInc == -1) {
                this.stopTimer();
            };
        } else {
            removeClass(this.butPrevYear, "fd-disabled");
        };

        if(this.outOfRange(new Date(tdy, (+tdm - 1), daysInMonth(+tdm-1, tdy), 5, 0, 0))) {
            addClass(this.butPrevMonth, "fd-disabled");
            if(this.monthInc == -1) {
                this.stopTimer();
            };
        } else {
            removeClass(this.butPrevMonth, "fd-disabled");
        };

        if(this.outOfRange(new Date((tdy + 1), +tdm, 1, 5, 0, 0))) {
            addClass(this.butNextYear, "fd-disabled");
            if(this.yearInc == 1) {
                this.stopTimer();
            };
        } else {
            removeClass(this.butNextYear, "fd-disabled");
        };

        if(this.outOfRange(new Date(tdy, +tdm + 1, 1, 5, 0, 0))) {
            addClass(this.butNextMonth, "fd-disabled");
            if(this.monthInc == 1) {
                this.stopTimer();
            };
        } else {
            removeClass(this.butNextMonth, "fd-disabled");
        };
    };
    var localeDefaults = {
        fullMonths:["January","February","March","April","May","June","July","August","September","October","November","December"],
        monthAbbrs:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
        fullDays:  ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],
        dayAbbrs:  ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"],
        titles:    ["Previous month","Next month","Previous year","Next year", "Today", "Show Calendar", "wk", "Week [[%0%]] of [[%1%]]", "Week", "Select a date", "Click \u0026 Drag to move", "Display \u201C[[%0%]]\u201D first", "Go to Today\u2019s date", "Disabled date :"],
        rtl:       false,
        firstDayOfWeek:0,
        imported:  false
    };
    var joinNodeLists = function() {
        if(!arguments.length) {
            return [];
        };
        var nodeList = [];
        for (var i = 0; i < arguments.length; i++) {
            for (var j = 0, item; item = arguments[i][j]; j++) {
                nodeList[nodeList.length] = item;
            };
        };
        return nodeList;
    };
    var cleanUp = function() {
        var dp, fe;
        for(dp in datePickers) {
            for(fe in datePickers[dp].formElements) {
                if(!document.getElementById(fe)) {
                    datePickers[dp].destroy();
                    datePickers[dp] = null;
                    delete datePickers[dp];
                    break;
                };
            };
        };
    };
    var hideAll = function(exception) {
        var dp;
        for(dp in datePickers) {
            if(!datePickers[dp].created || (exception && exception == datePickers[dp].id)) {
                continue;
            };
            datePickers[dp].hide();
        };
    };
    var hideDatePicker = function(inpID) {
        if(inpID in datePickers) {
            if(!datePickers[inpID].created || datePickers[inpID].staticPos) {
                return;
            };
            datePickers[inpID].hide();
        };
    };
    var showDatePicker = function(inpID, autoFocus) {

        if(!(inpID in datePickers)) {
            return false;
        };

        datePickers[inpID].clickActivated = !!!autoFocus;
        datePickers[inpID].show(autoFocus);

        return true;
    };
    var destroy = function(e) {
        e = e || window.event;

        // Don't remove datepickers if it's a pagehide/pagecache event (webkit et al)
        if(e.persisted) {
            return;
        };

        var dp;
        for(dp in datePickers) {
            datePickers[dp].destroy();
            datePickers[dp] = null;
            delete datePickers[dp];
        };
        datePickers = null;

        removeEvent(window, 'unload', datePickerController.destroy);
    };
    var destroySingleDatePicker = function(id) {
        if(id && (id in datePickers)) {
            datePickers[id].destroy();
            datePickers[id] = null;
            delete datePickers[id];
        };
    };
    var getTitleTranslation = function(num, replacements) {
        replacements = replacements || [];
        if(localeImport.titles.length > num) {
             var txt = localeImport.titles[num];
             if(replacements && replacements.length) {
                for(var i = 0; i < replacements.length; i++) {
                    txt = txt.replace("[[%" + i + "%]]", replacements[i]);
                };
             };
             return txt.replace(/[[%(\d)%]]/g,"");
        };
        return "";
    };
    var getDayTranslation = function(day, abbreviation) {
        var titles = localeImport[abbreviation ? "dayAbbrs" : "fullDays"];
        return titles.length && titles.length > day ? titles[day] : "";
    };
    var getMonthTranslation = function(month, abbreviation) {
        var titles = localeImport[abbreviation ? "monthAbbrs" : "fullMonths"];
        return titles.length && titles.length > month ? titles[month] : "";
    };
    var daysInMonth = function(nMonth, nYear) {
        nMonth = (nMonth + 12) % 12;
        return (((0 == (nYear%4)) && ((0 != (nYear%100)) || (0 == (nYear%400)))) && nMonth == 1) ? 29: [31,28,31,30,31,30,31,31,30,31,30,31][nMonth];
    };
    var getWeeksInYear = function(Y) {
        if(Y in weeksInYearCache) {
            return weeksInYearCache[Y];
        };

        var X1 = new Date(Y, 0, 4),
            X2 = new Date(Y, 11, 28);

        X1.setDate(X1.getDate() - (6 + X1.getDay()) % 7);
        X2.setDate(X2.getDate() + (7 - X2.getDay()) % 7);

        weeksInYearCache[Y] = Math.round((X2 - X1) / 604800000);

        return weeksInYearCache[Y];
    };

    var getWeekNumber = function(y,m,d) {
        var d   = new Date(y, m, d, 0, 0, 0),
            DoW = d.getDay(),
            ms;

        d.setDate(d.getDate() - (DoW + 6) % 7 + 3);
        ms = d.valueOf();
        d.setMonth(0);
        d.setDate(4);
        return Math.round((ms - d.valueOf()) / (7 * 864e5)) + 1;
    };

    var printFormattedDate = function(date, fmt, useImportedLocale) {
        if(!date || isNaN(date)) {
            return fmt;
        };

        var d           = date.getDate(),
            D           = date.getDay(),
            m           = date.getMonth(),
            y           = date.getFullYear(),
            locale      = useImportedLocale ? localeImport : localeDefaults,
            fmtParts    = String(fmt).split(formatSplitRegExp),
            fmtParts    = cbSplit(fmt, formatSplitRegExp),
            fmtNewParts = [],
            flags       = {
                        "d":pad(d),
                        "D":locale.dayAbbrs[D == 0 ? 6 : D - 1],
                        "l":locale.fullDays[D == 0 ? 6 : D - 1],
                        "j":d,
                        "N":D == 0 ? 7 : D,
                        "w":D,
                        "W":getWeekNumber(y,m,d),
                        "M":locale.monthAbbrs[m],
                        "F":locale.fullMonths[m],
                        "m":pad(m + 1),
                        "n":m + 1,
                        "t":daysInMonth(m, y),
                        "y":String(y).substr(2,2),
                        "Y":y,
                        "S":["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
                        },
            len         = fmtParts.length,
            currFlag, f;

        for(f = 0; f < len; f++) {
            currFlag = fmtParts[f];
            fmtNewParts.push(currFlag in flags ? flags[currFlag] : currFlag);
        };

        return fmtNewParts.join("");
    };
    var parseDateString = function(str, fmt) {
        var d     = false,
            m     = false,
            y     = false,
            dp    = fmt.search(dPartsRegExp) != -1 ? 1 : 0,
            mp    = fmt.search(mPartsRegExp) != -1 ? 1 : 0,
            yp    = fmt.search(yPartsRegExp) != -1 ? 1 : 0,
            now   = new Date(),
            parts = cbSplit(fmt, formatSplitRegExp),
            str   = "" + str,
            len   = parts.length,
            pt, part, l;

        loopLabel:
        for(pt = 0; pt < len; pt++) {
            part = parts[pt];

            if(part === "") {
                continue loopLabel;
            };

            if(str.length == 0) {
                break;
            };

            switch(part) {
                // Dividers - be easy on them all i.e. accept them all when parsing...
                case "/":
                case ".":
                case " ":
                case "-":
                case ",":
                case ":":
                    str = str.substr(1);
                    break;
                // DAY
                case "d":
                    // Day of the month, 2 digits with leading zeros (01 - 31)
                    if(str.search(/^(3[01]|[12][0-9]|0[1-9])/) != -1) {
                        d = str.substr(0,2);
                        str = str.substr(2);
                        break;
                    } else {
                        return false;
                    };
                case "j": // Day of the month without leading zeros (1 - 31)
                    if(str.search(/^(3[01]|[12][0-9]|[1-9])/) != -1) {
                        d = +str.match(/^(3[01]|[12][0-9]|[1-9])/)[0];
                        str = str.substr(str.match(/^(3[01]|[12][0-9]|[1-9])/)[0].length);
                        break;
                    } else {
                        return false;
                    };
                case "D": // A textual representation of a day, three letters (Mon - Sun)
                case "l": // A full textual representation of the day of the week (Monday - Sunday)
                          // Accept English & imported locales and both modifiers
                    l = localeDefaults.fullDays.concat(localeDefaults.dayAbbrs);
                    if(localeImport.imported) {
                        l = l.concat(localeImport.fullDays).concat(localeImport.dayAbbrs);
                    };

                    for(var i = 0; i < l.length; i++) {
                        if(new RegExp("^" + l[i], "i").test(str)) {
                            str = str.substr(l[i].length);
                            continue loopLabel;
                        };
                    };

                    break;
                case "N": // ISO-8601 numeric representation of the day of the week (added in PHP 5.1.0) 1 (for Monday) through 7 (for Sunday)
                case "w": // Numeric representation of the day of the week 0 (for Sunday) through 6 (for Saturday)
                    if(str.search(part == "N" ? /^([1-7])/ : /^([0-6])/) != -1) {
                        str = str.substr(1);
                    };
                    break;
                case "S": // English ordinal suffix for the day of the month, 2 characters: st, nd, rd or th
                    if(str.search(/^(st|nd|rd|th)/i) != -1) {
                        str = str.substr(2);
                    };
                    break;
                // WEEK
                case "W": // ISO-8601 week number of year, weeks starting on Monday (added in PHP 4.1.0): 1 - 53
                    if(str.search(/^([1-9]|[1234[0-9]|5[0-3])/) != -1) {
                        str = str.substr(str.match(/^([1-9]|[1234[0-9]|5[0-3])/)[0].length);
                    };
                    break;
                // MONTH
                case "M": // A short textual representation of a month, three letters
                case "F": // A full textual representation of a month, such as January or March
                          // Accept English & imported locales and both modifiers
                    l = localeDefaults.fullMonths.concat(localeDefaults.monthAbbrs);
                    if(localeImport.imported) {
                        l = l.concat(localeImport.fullMonths).concat(localeImport.monthAbbrs);
                    };
                    for(var i = 0; i < l.length; i++) {
                        if(str.search(new RegExp("^" + l[i],"i")) != -1) {
                            str = str.substr(l[i].length);
                            m = ((i + 12) % 12) + 1;
                            continue loopLabel;
                        };
                    };
                    return false;
                case "m": // Numeric representation of a month, with leading zeros
                    l = /^(1[012]|0[1-9])/;
                    if(str.search(l) != -1) {
                        m = +str.substr(0, 2);
                        str = str.substr(2);
                        break;
                    } else {
                        return false;
                    };
                case "n": // Numeric representation of a month, without leading zeros
                          // Accept either when parsing
                    l = /^(1[012]|[1-9])/;
                    if(str.search(l) != -1) {
                        m = +str.match(l)[0];
                        str = str.substr(str.match(l)[0].length);
                        break;
                    } else {
                        return false;
                    };
                case "t": // Number of days in the given month: 28 through 31
                    if(str.search(/2[89]|3[01]/) != -1) {
                        str = str.substr(2);
                        break;
                    } else {
                        return false;
                    };
                // YEAR

                case "Y": // A full numeric representation of a year, 4 digits
                    if(str.search(/^(\d{4})/) != -1) {
                        y = str.substr(0,4);
                        str = str.substr(4);
                        break;
                    } else {
                        return false;
                    };
                case "y": // A two digit representation of a year
                    if(str.search(/^(0[0-9]|[1-9][0-9])/) != -1) {
                        y = str.substr(0,2);
                        y = +y < 50 ? '20' + String(y) : '19' + String(y);
                        str = str.substr(2);
                        break;
                    } else {
                        return false;
                    };
                default:
                   str = str.substr(part.length);
            };
        };

        if((dp && d === false) || (mp && m === false) || (yp && y === false)) {
            return false;
        };

        if(dp && mp && yp && +d > daysInMonth(+m - 1, +y)) {
            return false;
        };

        return {
            "d":dp ? +d : false,
            "m":mp ? +m : false,
            "y":yp ? +y : false
            };
    };

    var findLabelForElement = function(element) {
        var label;
        if(element.parentNode && element.parentNode.tagName.toLowerCase() == "label") {
            label = element.parentNode;
        } else {
            var labelList = document.getElementsByTagName('label');
            // loop through label array attempting to match each 'for' attribute to the id of the current element
            for(var lbl = 0; lbl < labelList.length; lbl++) {
                // Internet Explorer requires the htmlFor test
                if((labelList[lbl]['htmlFor'] && labelList[lbl]['htmlFor'] == element.id) || (labelList[lbl].getAttribute('for') == element.id)) {
                    label = labelList[lbl];
                    break;
                };
            };
        };

        if(label && !label.id && element.id) {
            label.id = element.id + "_label";
        };

        return label;
    };
    var updateLanguage = function() {
        if(typeof(pageSettings) == "object" ) {

            if(typeof(pageSettings) == "object" ) {
                localeImport = {
                    titles          : pageSettings.titles,
                    fullMonths      : pageSettings.fullMonths,
                    monthAbbrs      : pageSettings.monthAbbrs,
                    fullDays        : pageSettings.fullDays,
                    dayAbbrs        : pageSettings.dayAbbrs,
                    firstDayOfWeek  : ("firstDayOfWeek" in pageSettings) ? pageSettings.firstDayOfWeek : 0,
                    rtl             : ("rtl" in pageSettings) ? !!(pageSettings.rtl) : false,
                    imported        : true
                };
            } else {
                localeImport = {
                    titles          : fdLocale.titles,
                    fullMonths      : fdLocale.fullMonths,
                    monthAbbrs      : fdLocale.monthAbbrs,
                    fullDays        : fdLocale.fullDays,
                    dayAbbrs        : fdLocale.dayAbbrs,
                    firstDayOfWeek  : ("firstDayOfWeek" in fdLocale) ? fdLocale.firstDayOfWeek : 0,
                    rtl             : ("rtl" in fdLocale) ? !!(fdLocale.rtl) : false,
                    imported        : true
                };
            }
        } else if(!localeImport) {
            localeImport = localeDefaults;
        };
    };
    var loadLanguage = function() {
        updateLanguage();
        var dp;
        for(dp in datePickers) {
            if(!datePickers[dp].created) {
                continue;
            };
            datePickers[dp].updateTable();
        };
    };
    var checkElem = function(elem) {
        return !(!elem || !elem.tagName || !((elem.tagName.toLowerCase() == "input" && (elem.type == "text" || elem.type == "hidden")) || elem.tagName.toLowerCase() == "select"));
    };
    var addDatePicker = function(options) {
        updateLanguage();

        if(cssAnimations === null) {
            cssAnimations = testCSSAnimationSupport();
        };

        if(!options.formElements) {
            if(debug) {
                throw "No form elements stipulated within initialisation parameters";
            };
            return;
        };

        options.id            = (options.id && (options.id in options.formElements)) ? options.id : "";

        options.enabledDates  = false;
        options.disabledDates = false;

        var partsFound  = {d:0,m:0,y:0},
            defaultVals = {},
            cursorDate  = false,
            myMin       = 0,
            myMax       = 0,
            fmt, opts, dtPartStr, elemID, elem, dt, i;

        for(elemID in options.formElements) {
            elem = document.getElementById(elemID);

            if(!checkElem(elem)) {
                if(debug) {
                    throw "Element '" + elemID + "' is of the wrong type or does not exist within the DOM";
                };
                return false;
            };

            if(!(options.formElements[elemID].match(formatTestRegExp))) {
                if(debug) {
                    throw "Element '" + elemID + "' has a date format that does not contain either a day (d|j), month (m|F|n) or year (y|Y) part: " + options.formElements[elemID];
                };
                return false;
            };

            if(!options.id) {
                options.id = elemID;
            };

            defaultVals[elemID] = elem.tagName == "select" ? elem.selectedIndex || 0 : elem.defaultValue;

            fmt             = {
                "value":options.formElements[elemID]
            };

            fmt.d = fmt.value.search(dPartsRegExp) != -1;
            fmt.m = fmt.value.search(mPartsRegExp) != -1;
            fmt.y = fmt.value.search(yPartsRegExp) != -1;

            if(fmt.d) {
                partsFound.d++;
            };
            if(fmt.m) {
                partsFound.m++;
            };
            if(fmt.y) {
                partsFound.y++;
            };

            if(elem.tagName.toLowerCase() == "select") {
                // If we have a selectList, then try to parse the higher and lower limits
                var selOptions = elem.options;

                // Check the yyyymmdd
                if(fmt.d && fmt.m && fmt.y) {
                    cursorDate = false;

                    // Dynamically calculate the available "enabled" dates
                    options.enabledDates = {};
                    options.disabledDates = {};

                    for(i = 0; i < selOptions.length; i++) {
                        dt = parseDateString(selOptions[i].value, fmt.value);

                        if(dt && dt.y && !(dt.m === false) && dt.d) {

                            dtPartStr = dt.y + "" + pad(dt.m) + pad(dt.d);
                            if(!cursorDate) {
                                cursorDate = dtPartStr;
                            };

                            options.enabledDates[dtPartStr] = 1;

                            if(!myMin || +dtPartStr < +myMin) {
                                myMin = dtPartStr;
                            };

                            if(!myMax || +dtPartStr > +myMax) {
                                myMax = dtPartStr;
                            };
                        };
                    };

                    // Automatically set cursor to first available date (if no bespoke cursorDate was set);
                    if(!options.cursorDate && cursorDate) {
                        options.cursorDate = cursorDate;
                    };

                    options.disabledDates[myMin] = myMax;

                } else if(fmt.m && fmt.y) {

                    for(i = 0; i < selOptions.length; i++) {
                        dt = parseDateString(selOptions[i].value, fmt.value);
                        if(dt.y && !(dt.m === false)) {
                            dtPartStr = dt.y + "" + pad(dt.m);

                            if(!myMin || +dtPartStr < +myMin) {
                                myMin = dtPartStr;
                            };

                            if(!myMax || +dtPartStr > +myMax) {
                                myMax = dtPartStr;
                            };
                        };
                    };

                    // Round the min & max values to be used as rangeLow & rangeHigh
                    myMin += "" + "01";
                    myMax += "" + daysInMonth(+myMax.substr(4,2) - 1, +myMax.substr(0,4));

                } else if(fmt.y) {
                    for(i = 0; i < selOptions.length; i++) {
                        dt = parseDateString(selOptions[i].value, fmt.value);
                        if(dt.y) {
                            if(!myMin || +dt.y < +myMin) {
                                myMin = dt.y;
                            };

                            if(!myMax || +dt.y > +myMax) {
                                myMax = dt.y;
                            };
                        };
                    };

                    // Round the min & max values to be used as rangeLow & rangeHigh
                    myMin += "" + "0101";
                    myMax += "" + "1231";
                };
            };
        };

        if(!(partsFound.d == 1 && partsFound.m == 1 && partsFound.y == 1)) {
            if(debug) {
                throw "Could not find all of the required date parts within the date format for element: " + elem.id;
            };
            return false;
        };

        options.rangeLow = dateToYYYYMMDD(options.rangeLow || false);
        options.rangeHigh = dateToYYYYMMDD(options.rangeHigh || false);
        options.cursorDate = dateToYYYYMMDD(options.cursorDate || false);
        options.startPeriod = dateToYYYYMMDD(options.startPeriod || false);
        options.endPeriod = dateToYYYYMMDD(options.endPeriod || false);



        if(myMin && (!options.rangeLow  || (+options.rangeLow < +myMin))) {
            options.rangeLow = myMin;
        };
        if(myMax && (!options.rangeHigh || (+options.rangeHigh > +myMax))) {
            options.rangeHigh = myMax;
        };

        opts = {
            formElements:options.formElements,
            // default values
            defaultVals:defaultVals,
            // Form element id
            id:options.id,
            // Non popup datepicker required
            staticPos:!!(options.staticPos || options.nopopup),
            wrapped:options.wrapped || false,
            // Position static datepicker or popup datepicker's button
            positioned:options.positioned && document.getElementById(options.positioned) ? options.positioned : "",
            // Ranges stipulated in YYYYMMDD format
            rangeLow:options.rangeLow && String(options.rangeLow).search(rangeRegExp) != -1 ? options.rangeLow : "",
            rangeHigh:options.rangeHigh && String(options.rangeHigh).search(rangeRegExp) != -1 ? options.rangeHigh : "",
            // Status bar format
            statusFormat:options.statusFormat || statusFormat,
            // No fade in/out effect
            noFadeEffect:!!(options.staticPos) ? true : !!(options.noFadeEffect),
            // No drag functionality
            dragDisabled:nodrag || !!(options.staticPos) ? true : !!(options.dragDisabled),
            // Bespoke tabindex for this datePicker (or its activation button)
            bespokeTabIndex:options.bespokeTabindex && typeof options.bespokeTabindex == 'number' ? parseInt(options.bespokeTabindex, 10) : 0,
            // Bespoke titles
            bespokeTitles:options.bespokeTitles || (bespokeTitles || {}),
            // Final opacity
            finalOpacity:options.finalOpacity && typeof options.finalOpacity == 'number' && (options.finalOpacity > 20 && options.finalOpacity <= 100) ? parseInt(+options.finalOpacity, 10) : (!!(options.staticPos) ? 100 : finalOpacity),
            // Do we hide the form elements on datepicker creation
            hideInput:!!(options.hideInput),
            // Do we hide the "today" button
            noToday:!!(options.noTodayButton),
            // Do we show week numbers
            showWeeks:!!(options.showWeeks),
            // Do we fill the entire grid with dates
            fillGrid:!!(options.fillGrid),
            // Do we constrain selection of dates outside the current month
            constrainSelection:"constrainSelection" in options ? !!(options.constrainSelection) : true,
            // The date to set the initial cursor to
            cursorDate:options.cursorDate && String(options.cursorDate).search(rangeRegExp) != -1 ? options.cursorDate : "",
            // Locate label to set the ARIA labelled-by property
            labelledBy:findLabelForElement(elem),
            // Have we been passed a describedBy to set the ARIA decribed-by property...
            describedBy:(options.describedBy && document.getElementById(options.describedBy)) ? options.describedBy : describedBy && document.getElementById(describedBy) ? describedBy : "",
            // Callback functions
            callbacks:options.callbackFunctions ? options.callbackFunctions : {},
            // Days of the week to highlight (normally the weekend)
            highlightDays:options.highlightDays && options.highlightDays.length && options.highlightDays.length == 7 ? options.highlightDays : [0,0,0,0,0,1,1],
            // Days of the week to disable
            disabledDays:options.disabledDays && options.disabledDays.length && options.disabledDays.length == 7 ? options.disabledDays : [0,0,0,0,0,0,0],
            // A bespoke class to give the datepicker
            bespokeClass:options.bespokeClass ? " " + options.bespokeClass : ""
        };

        datePickers[options.id] = new datePicker(opts);

        if("disabledDates" in options && !(options.disabledDates === false)) {
            datePickers[options.id].setDisabledDates(options.disabledDates)
        };

        if("enabledDates" in options && !(options.enabledDates === false)) {
            datePickers[options.id].setEnabledDates(options.enabledDates)
        };

        datePickers[options.id].callback("create", datePickers[options.id].createCbArgObj());
    };

    // Used by the button to dictate whether to open or close the datePicker
    var isVisible = function(id) {
        return (!id || !(id in datePickers)) ? false : datePickers[id].visible;
    };

    var updateStatic = function() {
        var dp;
        for(dp in datePickers) {
            if(datePickers.hasOwnProperty(dp)) {
                datePickers[dp].changeHandler();
            };
        };
    };

    var testCSSAnimationSupport = function() {
        var domPrefixes     = ["Webkit","Moz", "ms", "O"],
            elm             = document.createElement('div'),
            transitions     = ["WebkitTransition", "transition", "OTransition", "MozTransition", "msTransition"],
            t;

        for(t = 0; t < transitions.length; t++) {
            if(transitions[t] in elm.style) {
                transitionEnd = transitions[t] == "webkitTransition" || transitions[t] == "OTransition" ? transitions[t] + "End" : "transitionend"
                break;
            }
        }

        if(!transitionEnd) {
            return false;
        };

        if(elm.style.animationName) { return true; }

        for( var i = 0; i < domPrefixes.length; i++ ) {
            if(elm.style[domPrefixes[i] + "AnimationName"] !== undefined) {
                return true;
            };
        };

        return false;
    };

    addEvent(window, 'unload', destroy);
    addEvent(window, "load", function() { setTimeout(updateStatic, 0); });

    // Add oldie class if needed for IE < 9
    if(oldIE) {
        addClass(document.documentElement, "oldie");
    };

    return {
        // General event functions...
        addEvent:               function(obj, type, fn) { return addEvent(obj, type, fn); },
        removeEvent:            function(obj, type, fn) { return removeEvent(obj, type, fn); },
        stopEvent:              function(e) { return stopEvent(e); },
        // Show a single popup datepicker
        show:                   function(inpID) { return showDatePicker(inpID, false); },
        // Hide a popup datepicker
        hide:                   function(inpID) { return hideDatePicker(inpID); },
        // Create a new datepicker
        createDatePicker:       function(options) { addDatePicker(options); },
        // Destroy a datepicker (remove events and DOM nodes)
        destroyDatePicker:      function(inpID) { destroySingleDatePicker(inpID); },
        // Check datePicker form elements exist, if not, destroy the datepicker
        cleanUp:                function() { cleanUp(); },
        // Pretty print a date object according to the format passed in
        printFormattedDate:     function(dt, fmt, useImportedLocale) { return printFormattedDate(dt, fmt, useImportedLocale); },
        // Update the internal date using the form element value
        setDateFromInput:       function(inpID) { if(!inpID || !(inpID in datePickers)) return false; datePickers[inpID].setDateFromInput(); },
        // Set low and high date ranges
        setRangeLow:            function(inpID, yyyymmdd) { if(!inpID || !(inpID in datePickers)) { return false; }; datePickers[inpID].setRangeLow(dateToYYYYMMDD(yyyymmdd)); },
        setRangeHigh:           function(inpID, yyyymmdd) { if(!inpID || !(inpID in datePickers)) { return false; }; datePickers[inpID].setRangeHigh(dateToYYYYMMDD(yyyymmdd)); },
        // Set bespoke titles for a datepicker instance
        setBespokeTitles:       function(inpID, titles) {if(!inpID || !(inpID in datePickers)) { return false; }; datePickers[inpID].setBespokeTitles(titles); },
        // Add bespoke titles for a datepicker instance
        addBespokeTitles:       function(inpID, titles) {if(!inpID || !(inpID in datePickers)) { return false; }; datePickers[inpID].addBespokeTitles(titles); },
        // Attempt to parse a valid date from a date string using the passed in format
        parseDateString:        function(str, format) { return parseDateString(str, format); },
        // Change global configuration parameters
        setGlobalOptions:       function(json) { affectJSON(json); },
        // Forces the datepickers "selected" date
        setSelectedDate:        function(inpID, yyyymmdd) { if(!inpID || !(inpID in datePickers)) { return false; }; datePickers[inpID].setSelectedDate(dateToYYYYMMDD(yyyymmdd)); },
        // Is the date valid for selection i.e. not outside ranges etc
        dateValidForSelection:  function(inpID, dt) { if(!inpID || !(inpID in datePickers)) return false; return datePickers[inpID].canDateBeSelected(dt); },
        // Add disabled and enabled dates
        addDisabledDates:       function(inpID, dts) { if(!inpID || !(inpID in datePickers)) return false; datePickers[inpID].addDisabledDates(dts); },
        setDisabledDates:       function(inpID, dts) { if(!inpID || !(inpID in datePickers)) return false; datePickers[inpID].setDisabledDates(dts); },
        addEnabledDates:        function(inpID, dts) { if(!inpID || !(inpID in datePickers)) return false; datePickers[inpID].addEnabledDates(dts); },
        setEnabledDates:        function(inpID, dts) { if(!inpID || !(inpID in datePickers)) return false; datePickers[inpID].setEnabledDates(dts); },
        // set period

        setPeriodHighlight:     function(inpID, dts, ets) { if(!inpID || !(inpID in datePickers)) return false; datePickers[inpID].setPeriodHighlight(dateToYYYYMMDD(dts), dateToYYYYMMDD(ets))},
        // Disable and enable the datepicker
        disable:                function(inpID) { if(!inpID || !(inpID in datePickers)) return false; datePickers[inpID].disableDatePicker(); },
        enable:                 function(inpID) { if(!inpID || !(inpID in datePickers)) return false; datePickers[inpID].enableDatePicker(); },
        // Set the cursor date
        setCursorDate:          function(inpID, yyyymmdd) { if(!inpID || !(inpID in datePickers)) return false; datePickers[inpID].setCursorDate(dateToYYYYMMDD(yyyymmdd)); },
        // Whats the currently selected date
        getSelectedDate:        function(inpID) { return (!inpID || !(inpID in datePickers)) ? false : datePickers[inpID].returnSelectedDate(); },
        // Attempt to update the language (causes a redraw of all datepickers on the page)
        loadLanguage:           function() { loadLanguage(); },
        // Set the debug level i.e. throw errors or fail silently
        setDebug:               function(dbg) { debug = !!(dbg); },
        // Converts Date Object to a YYYYMMDD formatted String
        dateToYYYYMMDDStr:      function(dt) { return dateToYYYYMMDD(dt); }
    };
})();

/*! plugin juery to find if element is on screen */

$.fn.isOnScreen = function(x, y){

    if(x == null || typeof x == 'undefined') x = 1;
    if(y == null || typeof y == 'undefined') y = 1;

    var win = $(window);

    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var height = this.outerHeight();
    var width = this.outerWidth();

    if(!width || !height){
        return false;
    }

    var bounds = this.offset();
    bounds.right = bounds.left + width;
    bounds.bottom = bounds.top + height;

    var visible = (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

    if(!visible){
        return false;
    }

    var deltas = {
        top : Math.min( 1, ( bounds.bottom - viewport.top ) / height),
        bottom : Math.min(1, ( viewport.bottom - bounds.top ) / height),
        left : Math.min(1, ( bounds.right - viewport.left ) / width),
        right : Math.min(1, ( viewport.right - bounds.left ) / width)
    };

    return (deltas.left * deltas.right) >= x && (deltas.top * deltas.bottom) >= y;

};


/*! VelocityJS.org (1.2.2). (C) 2014 Julian Shapiro. MIT @license: en.wikipedia.org/wiki/MIT_License */
/*! VelocityJS.org jQuery Shim (1.0.1). (C) 2014 The jQuery Foundation. MIT @license: en.wikipedia.org/wiki/MIT_License. */
!function(e){function t(e){var t=e.length,r=$.type(e);return"function"===r||$.isWindow(e)?!1:1===e.nodeType&&t?!0:"array"===r||0===t||"number"==typeof t&&t>0&&t-1 in e}if(!e.jQuery){var $=function(e,t){return new $.fn.init(e,t)};$.isWindow=function(e){return null!=e&&e==e.window},$.type=function(e){return null==e?e+"":"object"==typeof e||"function"==typeof e?a[o.call(e)]||"object":typeof e},$.isArray=Array.isArray||function(e){return"array"===$.type(e)},$.isPlainObject=function(e){var t;if(!e||"object"!==$.type(e)||e.nodeType||$.isWindow(e))return!1;try{if(e.constructor&&!n.call(e,"constructor")&&!n.call(e.constructor.prototype,"isPrototypeOf"))return!1}catch(r){return!1}for(t in e);return void 0===t||n.call(e,t)},$.each=function(e,r,a){var n,o=0,i=e.length,s=t(e);if(a){if(s)for(;i>o&&(n=r.apply(e[o],a),n!==!1);o++);else for(o in e)if(n=r.apply(e[o],a),n===!1)break}else if(s)for(;i>o&&(n=r.call(e[o],o,e[o]),n!==!1);o++);else for(o in e)if(n=r.call(e[o],o,e[o]),n===!1)break;return e},$.data=function(e,t,a){if(void 0===a){var n=e[$.expando],o=n&&r[n];if(void 0===t)return o;if(o&&t in o)return o[t]}else if(void 0!==t){var n=e[$.expando]||(e[$.expando]=++$.uuid);return r[n]=r[n]||{},r[n][t]=a,a}},$.removeData=function(e,t){var a=e[$.expando],n=a&&r[a];n&&$.each(t,function(e,t){delete n[t]})},$.extend=function(){var e,t,r,a,n,o,i=arguments[0]||{},s=1,l=arguments.length,u=!1;for("boolean"==typeof i&&(u=i,i=arguments[s]||{},s++),"object"!=typeof i&&"function"!==$.type(i)&&(i={}),s===l&&(i=this,s--);l>s;s++)if(null!=(n=arguments[s]))for(a in n)e=i[a],r=n[a],i!==r&&(u&&r&&($.isPlainObject(r)||(t=$.isArray(r)))?(t?(t=!1,o=e&&$.isArray(e)?e:[]):o=e&&$.isPlainObject(e)?e:{},i[a]=$.extend(u,o,r)):void 0!==r&&(i[a]=r));return i},$.queue=function(e,r,a){function n(e,r){var a=r||[];return null!=e&&(t(Object(e))?!function(e,t){for(var r=+t.length,a=0,n=e.length;r>a;)e[n++]=t[a++];if(r!==r)for(;void 0!==t[a];)e[n++]=t[a++];return e.length=n,e}(a,"string"==typeof e?[e]:e):[].push.call(a,e)),a}if(e){r=(r||"fx")+"queue";var o=$.data(e,r);return a?(!o||$.isArray(a)?o=$.data(e,r,n(a)):o.push(a),o):o||[]}},$.dequeue=function(e,t){$.each(e.nodeType?[e]:e,function(e,r){t=t||"fx";var a=$.queue(r,t),n=a.shift();"inprogress"===n&&(n=a.shift()),n&&("fx"===t&&a.unshift("inprogress"),n.call(r,function(){$.dequeue(r,t)}))})},$.fn=$.prototype={init:function(e){if(e.nodeType)return this[0]=e,this;throw new Error("Not a DOM node.")},offset:function(){var t=this[0].getBoundingClientRect?this[0].getBoundingClientRect():{top:0,left:0};return{top:t.top+(e.pageYOffset||document.scrollTop||0)-(document.clientTop||0),left:t.left+(e.pageXOffset||document.scrollLeft||0)-(document.clientLeft||0)}},position:function(){function e(){for(var e=this.offsetParent||document;e&&"html"===!e.nodeType.toLowerCase&&"static"===e.style.position;)e=e.offsetParent;return e||document}var t=this[0],e=e.apply(t),r=this.offset(),a=/^(?:body|html)$/i.test(e.nodeName)?{top:0,left:0}:$(e).offset();return r.top-=parseFloat(t.style.marginTop)||0,r.left-=parseFloat(t.style.marginLeft)||0,e.style&&(a.top+=parseFloat(e.style.borderTopWidth)||0,a.left+=parseFloat(e.style.borderLeftWidth)||0),{top:r.top-a.top,left:r.left-a.left}}};var r={};$.expando="velocity"+(new Date).getTime(),$.uuid=0;for(var a={},n=a.hasOwnProperty,o=a.toString,i="Boolean Number String Function Array Date RegExp Object Error".split(" "),s=0;s<i.length;s++)a["[object "+i[s]+"]"]=i[s].toLowerCase();$.fn.init.prototype=$.fn,e.Velocity={Utilities:$}}}(window),function(e){"object"==typeof module&&"object"==typeof module.exports?module.exports=e():"function"==typeof define&&define.amd?define(e):e()}(function(){return function(e,t,r,a){function n(e){for(var t=-1,r=e?e.length:0,a=[];++t<r;){var n=e[t];n&&a.push(n)}return a}function o(e){return g.isWrapped(e)?e=[].slice.call(e):g.isNode(e)&&(e=[e]),e}function i(e){var t=$.data(e,"velocity");return null===t?a:t}function s(e){return function(t){return Math.round(t*e)*(1/e)}}function l(e,r,a,n){function o(e,t){return 1-3*t+3*e}function i(e,t){return 3*t-6*e}function s(e){return 3*e}function l(e,t,r){return((o(t,r)*e+i(t,r))*e+s(t))*e}function u(e,t,r){return 3*o(t,r)*e*e+2*i(t,r)*e+s(t)}function c(t,r){for(var n=0;m>n;++n){var o=u(r,e,a);if(0===o)return r;var i=l(r,e,a)-t;r-=i/o}return r}function p(){for(var t=0;b>t;++t)w[t]=l(t*x,e,a)}function f(t,r,n){var o,i,s=0;do i=r+(n-r)/2,o=l(i,e,a)-t,o>0?n=i:r=i;while(Math.abs(o)>h&&++s<v);return i}function d(t){for(var r=0,n=1,o=b-1;n!=o&&w[n]<=t;++n)r+=x;--n;var i=(t-w[n])/(w[n+1]-w[n]),s=r+i*x,l=u(s,e,a);return l>=y?c(t,s):0==l?s:f(t,r,r+x)}function g(){V=!0,(e!=r||a!=n)&&p()}var m=4,y=.001,h=1e-7,v=10,b=11,x=1/(b-1),S="Float32Array"in t;if(4!==arguments.length)return!1;for(var P=0;4>P;++P)if("number"!=typeof arguments[P]||isNaN(arguments[P])||!isFinite(arguments[P]))return!1;e=Math.min(e,1),a=Math.min(a,1),e=Math.max(e,0),a=Math.max(a,0);var w=S?new Float32Array(b):new Array(b),V=!1,C=function(t){return V||g(),e===r&&a===n?t:0===t?0:1===t?1:l(d(t),r,n)};C.getControlPoints=function(){return[{x:e,y:r},{x:a,y:n}]};var T="generateBezier("+[e,r,a,n]+")";return C.toString=function(){return T},C}function u(e,t){var r=e;return g.isString(e)?v.Easings[e]||(r=!1):r=g.isArray(e)&&1===e.length?s.apply(null,e):g.isArray(e)&&2===e.length?b.apply(null,e.concat([t])):g.isArray(e)&&4===e.length?l.apply(null,e):!1,r===!1&&(r=v.Easings[v.defaults.easing]?v.defaults.easing:h),r}function c(e){if(e){var t=(new Date).getTime(),r=v.State.calls.length;r>1e4&&(v.State.calls=n(v.State.calls));for(var o=0;r>o;o++)if(v.State.calls[o]){var s=v.State.calls[o],l=s[0],u=s[2],f=s[3],d=!!f,m=null;f||(f=v.State.calls[o][3]=t-16);for(var y=Math.min((t-f)/u.duration,1),h=0,b=l.length;b>h;h++){var S=l[h],w=S.element;if(i(w)){var V=!1;if(u.display!==a&&null!==u.display&&"none"!==u.display){if("flex"===u.display){var C=["-webkit-box","-moz-box","-ms-flexbox","-webkit-flex"];$.each(C,function(e,t){x.setPropertyValue(w,"display",t)})}x.setPropertyValue(w,"display",u.display)}u.visibility!==a&&"hidden"!==u.visibility&&x.setPropertyValue(w,"visibility",u.visibility);for(var T in S)if("element"!==T){var k=S[T],A,F=g.isString(k.easing)?v.Easings[k.easing]:k.easing;if(1===y)A=k.endValue;else{var E=k.endValue-k.startValue;if(A=k.startValue+E*F(y,u,E),!d&&A===k.currentValue)continue}if(k.currentValue=A,"tween"===T)m=A;else{if(x.Hooks.registered[T]){var j=x.Hooks.getRoot(T),H=i(w).rootPropertyValueCache[j];H&&(k.rootPropertyValue=H)}var N=x.setPropertyValue(w,T,k.currentValue+(0===parseFloat(A)?"":k.unitType),k.rootPropertyValue,k.scrollData);x.Hooks.registered[T]&&(i(w).rootPropertyValueCache[j]=x.Normalizations.registered[j]?x.Normalizations.registered[j]("extract",null,N[1]):N[1]),"transform"===N[0]&&(V=!0)}}u.mobileHA&&i(w).transformCache.translate3d===a&&(i(w).transformCache.translate3d="(0px, 0px, 0px)",V=!0),V&&x.flushTransformCache(w)}}u.display!==a&&"none"!==u.display&&(v.State.calls[o][2].display=!1),u.visibility!==a&&"hidden"!==u.visibility&&(v.State.calls[o][2].visibility=!1),u.progress&&u.progress.call(s[1],s[1],y,Math.max(0,f+u.duration-t),f,m),1===y&&p(o)}}v.State.isTicking&&P(c)}function p(e,t){if(!v.State.calls[e])return!1;for(var r=v.State.calls[e][0],n=v.State.calls[e][1],o=v.State.calls[e][2],s=v.State.calls[e][4],l=!1,u=0,c=r.length;c>u;u++){var p=r[u].element;if(t||o.loop||("none"===o.display&&x.setPropertyValue(p,"display",o.display),"hidden"===o.visibility&&x.setPropertyValue(p,"visibility",o.visibility)),o.loop!==!0&&($.queue(p)[1]===a||!/\.velocityQueueEntryFlag/i.test($.queue(p)[1]))&&i(p)){i(p).isAnimating=!1,i(p).rootPropertyValueCache={};var f=!1;$.each(x.Lists.transforms3D,function(e,t){var r=/^scale/.test(t)?1:0,n=i(p).transformCache[t];i(p).transformCache[t]!==a&&new RegExp("^\\("+r+"[^.]").test(n)&&(f=!0,delete i(p).transformCache[t])}),o.mobileHA&&(f=!0,delete i(p).transformCache.translate3d),f&&x.flushTransformCache(p),x.Values.removeClass(p,"velocity-animating")}if(!t&&o.complete&&!o.loop&&u===c-1)try{o.complete.call(n,n)}catch(d){setTimeout(function(){throw d},1)}s&&o.loop!==!0&&s(n),i(p)&&o.loop===!0&&!t&&($.each(i(p).tweensContainer,function(e,t){/^rotate/.test(e)&&360===parseFloat(t.endValue)&&(t.endValue=0,t.startValue=360),/^backgroundPosition/.test(e)&&100===parseFloat(t.endValue)&&"%"===t.unitType&&(t.endValue=0,t.startValue=100)}),v(p,"reverse",{loop:!0,delay:o.delay})),o.queue!==!1&&$.dequeue(p,o.queue)}v.State.calls[e]=!1;for(var g=0,m=v.State.calls.length;m>g;g++)if(v.State.calls[g]!==!1){l=!0;break}l===!1&&(v.State.isTicking=!1,delete v.State.calls,v.State.calls=[])}var f=function(){if(r.documentMode)return r.documentMode;for(var e=7;e>4;e--){var t=r.createElement("div");if(t.innerHTML="<!--[if IE "+e+"]><span></span><![endif]-->",t.getElementsByTagName("span").length)return t=null,e}return a}(),d=function(){var e=0;return t.webkitRequestAnimationFrame||t.mozRequestAnimationFrame||function(t){var r=(new Date).getTime(),a;return a=Math.max(0,16-(r-e)),e=r+a,setTimeout(function(){t(r+a)},a)}}(),g={isString:function(e){return"string"==typeof e},isArray:Array.isArray||function(e){return"[object Array]"===Object.prototype.toString.call(e)},isFunction:function(e){return"[object Function]"===Object.prototype.toString.call(e)},isNode:function(e){return e&&e.nodeType},isNodeList:function(e){return"object"==typeof e&&/^\[object (HTMLCollection|NodeList|Object)\]$/.test(Object.prototype.toString.call(e))&&e.length!==a&&(0===e.length||"object"==typeof e[0]&&e[0].nodeType>0)},isWrapped:function(e){return e&&(e.jquery||t.Zepto&&t.Zepto.zepto.isZ(e))},isSVG:function(e){return t.SVGElement&&e instanceof t.SVGElement},isEmptyObject:function(e){for(var t in e)return!1;return!0}},$,m=!1;if(e.fn&&e.fn.jquery?($=e,m=!0):$=t.Velocity.Utilities,8>=f&&!m)throw new Error("Velocity: IE8 and below require jQuery to be loaded before Velocity.");if(7>=f)return void(jQuery.fn.velocity=jQuery.fn.animate);var y=400,h="swing",v={State:{isMobile:/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),isAndroid:/Android/i.test(navigator.userAgent),isGingerbread:/Android 2\.3\.[3-7]/i.test(navigator.userAgent),isChrome:t.chrome,isFirefox:/Firefox/i.test(navigator.userAgent),prefixElement:r.createElement("div"),prefixMatches:{},scrollAnchor:null,scrollPropertyLeft:null,scrollPropertyTop:null,isTicking:!1,calls:[]},CSS:{},Utilities:$,Redirects:{},Easings:{},Promise:t.Promise,defaults:{queue:"",duration:y,easing:h,begin:a,complete:a,progress:a,display:a,visibility:a,loop:!1,delay:!1,mobileHA:!0,_cacheValues:!0},init:function(e){$.data(e,"velocity",{isSVG:g.isSVG(e),isAnimating:!1,computedStyle:null,tweensContainer:null,rootPropertyValueCache:{},transformCache:{}})},hook:null,mock:!1,version:{major:1,minor:2,patch:2},debug:!1};t.pageYOffset!==a?(v.State.scrollAnchor=t,v.State.scrollPropertyLeft="pageXOffset",v.State.scrollPropertyTop="pageYOffset"):(v.State.scrollAnchor=r.documentElement||r.body.parentNode||r.body,v.State.scrollPropertyLeft="scrollLeft",v.State.scrollPropertyTop="scrollTop");var b=function(){function e(e){return-e.tension*e.x-e.friction*e.v}function t(t,r,a){var n={x:t.x+a.dx*r,v:t.v+a.dv*r,tension:t.tension,friction:t.friction};return{dx:n.v,dv:e(n)}}function r(r,a){var n={dx:r.v,dv:e(r)},o=t(r,.5*a,n),i=t(r,.5*a,o),s=t(r,a,i),l=1/6*(n.dx+2*(o.dx+i.dx)+s.dx),u=1/6*(n.dv+2*(o.dv+i.dv)+s.dv);return r.x=r.x+l*a,r.v=r.v+u*a,r}return function a(e,t,n){var o={x:-1,v:0,tension:null,friction:null},i=[0],s=0,l=1e-4,u=.016,c,p,f;for(e=parseFloat(e)||500,t=parseFloat(t)||20,n=n||null,o.tension=e,o.friction=t,c=null!==n,c?(s=a(e,t),p=s/n*u):p=u;;)if(f=r(f||o,p),i.push(1+f.x),s+=16,!(Math.abs(f.x)>l&&Math.abs(f.v)>l))break;return c?function(e){return i[e*(i.length-1)|0]}:s}}();v.Easings={linear:function(e){return e},swing:function(e){return.5-Math.cos(e*Math.PI)/2},spring:function(e){return 1-Math.cos(4.5*e*Math.PI)*Math.exp(6*-e)}},$.each([["ease",[.25,.1,.25,1]],["ease-in",[.42,0,1,1]],["ease-out",[0,0,.58,1]],["ease-in-out",[.42,0,.58,1]],["easeInSine",[.47,0,.745,.715]],["easeOutSine",[.39,.575,.565,1]],["easeInOutSine",[.445,.05,.55,.95]],["easeInQuad",[.55,.085,.68,.53]],["easeOutQuad",[.25,.46,.45,.94]],["easeInOutQuad",[.455,.03,.515,.955]],["easeInCubic",[.55,.055,.675,.19]],["easeOutCubic",[.215,.61,.355,1]],["easeInOutCubic",[.645,.045,.355,1]],["easeInQuart",[.895,.03,.685,.22]],["easeOutQuart",[.165,.84,.44,1]],["easeInOutQuart",[.77,0,.175,1]],["easeInQuint",[.755,.05,.855,.06]],["easeOutQuint",[.23,1,.32,1]],["easeInOutQuint",[.86,0,.07,1]],["easeInExpo",[.95,.05,.795,.035]],["easeOutExpo",[.19,1,.22,1]],["easeInOutExpo",[1,0,0,1]],["easeInCirc",[.6,.04,.98,.335]],["easeOutCirc",[.075,.82,.165,1]],["easeInOutCirc",[.785,.135,.15,.86]]],function(e,t){v.Easings[t[0]]=l.apply(null,t[1])});var x=v.CSS={RegEx:{isHex:/^#([A-f\d]{3}){1,2}$/i,valueUnwrap:/^[A-z]+\((.*)\)$/i,wrappedValueAlreadyExtracted:/[0-9.]+ [0-9.]+ [0-9.]+( [0-9.]+)?/,valueSplit:/([A-z]+\(.+\))|(([A-z0-9#-.]+?)(?=\s|$))/gi},Lists:{colors:["fill","stroke","stopColor","color","backgroundColor","borderColor","borderTopColor","borderRightColor","borderBottomColor","borderLeftColor","outlineColor"],transformsBase:["translateX","translateY","scale","scaleX","scaleY","skewX","skewY","rotateZ"],transforms3D:["transformPerspective","translateZ","scaleZ","rotateX","rotateY"]},Hooks:{templates:{textShadow:["Color X Y Blur","black 0px 0px 0px"],boxShadow:["Color X Y Blur Spread","black 0px 0px 0px 0px"],clip:["Top Right Bottom Left","0px 0px 0px 0px"],backgroundPosition:["X Y","0% 0%"],transformOrigin:["X Y Z","50% 50% 0px"],perspectiveOrigin:["X Y","50% 50%"]},registered:{},register:function(){for(var e=0;e<x.Lists.colors.length;e++){var t="color"===x.Lists.colors[e]?"0 0 0 1":"255 255 255 1";x.Hooks.templates[x.Lists.colors[e]]=["Red Green Blue Alpha",t]}var r,a,n;if(f)for(r in x.Hooks.templates){a=x.Hooks.templates[r],n=a[0].split(" ");var o=a[1].match(x.RegEx.valueSplit);"Color"===n[0]&&(n.push(n.shift()),o.push(o.shift()),x.Hooks.templates[r]=[n.join(" "),o.join(" ")])}for(r in x.Hooks.templates){a=x.Hooks.templates[r],n=a[0].split(" ");for(var e in n){var i=r+n[e],s=e;x.Hooks.registered[i]=[r,s]}}},getRoot:function(e){var t=x.Hooks.registered[e];return t?t[0]:e},cleanRootPropertyValue:function(e,t){return x.RegEx.valueUnwrap.test(t)&&(t=t.match(x.RegEx.valueUnwrap)[1]),x.Values.isCSSNullValue(t)&&(t=x.Hooks.templates[e][1]),t},extractValue:function(e,t){var r=x.Hooks.registered[e];if(r){var a=r[0],n=r[1];return t=x.Hooks.cleanRootPropertyValue(a,t),t.toString().match(x.RegEx.valueSplit)[n]}return t},injectValue:function(e,t,r){var a=x.Hooks.registered[e];if(a){var n=a[0],o=a[1],i,s;return r=x.Hooks.cleanRootPropertyValue(n,r),i=r.toString().match(x.RegEx.valueSplit),i[o]=t,s=i.join(" ")}return r}},Normalizations:{registered:{clip:function(e,t,r){switch(e){case"name":return"clip";case"extract":var a;return x.RegEx.wrappedValueAlreadyExtracted.test(r)?a=r:(a=r.toString().match(x.RegEx.valueUnwrap),a=a?a[1].replace(/,(\s+)?/g," "):r),a;case"inject":return"rect("+r+")"}},blur:function(e,t,r){switch(e){case"name":return v.State.isFirefox?"filter":"-webkit-filter";case"extract":var a=parseFloat(r);if(!a&&0!==a){var n=r.toString().match(/blur\(([0-9]+[A-z]+)\)/i);a=n?n[1]:0}return a;case"inject":return parseFloat(r)?"blur("+r+")":"none"}},opacity:function(e,t,r){if(8>=f)switch(e){case"name":return"filter";case"extract":var a=r.toString().match(/alpha\(opacity=(.*)\)/i);return r=a?a[1]/100:1;case"inject":return t.style.zoom=1,parseFloat(r)>=1?"":"alpha(opacity="+parseInt(100*parseFloat(r),10)+")"}else switch(e){case"name":return"opacity";case"extract":return r;case"inject":return r}}},register:function(){9>=f||v.State.isGingerbread||(x.Lists.transformsBase=x.Lists.transformsBase.concat(x.Lists.transforms3D));for(var e=0;e<x.Lists.transformsBase.length;e++)!function(){var t=x.Lists.transformsBase[e];x.Normalizations.registered[t]=function(e,r,n){switch(e){case"name":return"transform";case"extract":return i(r)===a||i(r).transformCache[t]===a?/^scale/i.test(t)?1:0:i(r).transformCache[t].replace(/[()]/g,"");case"inject":var o=!1;switch(t.substr(0,t.length-1)){case"translate":o=!/(%|px|em|rem|vw|vh|\d)$/i.test(n);break;case"scal":case"scale":v.State.isAndroid&&i(r).transformCache[t]===a&&1>n&&(n=1),o=!/(\d)$/i.test(n);break;case"skew":o=!/(deg|\d)$/i.test(n);break;case"rotate":o=!/(deg|\d)$/i.test(n)}return o||(i(r).transformCache[t]="("+n+")"),i(r).transformCache[t]}}}();for(var e=0;e<x.Lists.colors.length;e++)!function(){var t=x.Lists.colors[e];x.Normalizations.registered[t]=function(e,r,n){switch(e){case"name":return t;case"extract":var o;if(x.RegEx.wrappedValueAlreadyExtracted.test(n))o=n;else{var i,s={black:"rgb(0, 0, 0)",blue:"rgb(0, 0, 255)",gray:"rgb(128, 128, 128)",green:"rgb(0, 128, 0)",red:"rgb(255, 0, 0)",white:"rgb(255, 255, 255)"};/^[A-z]+$/i.test(n)?i=s[n]!==a?s[n]:s.black:x.RegEx.isHex.test(n)?i="rgb("+x.Values.hexToRgb(n).join(" ")+")":/^rgba?\(/i.test(n)||(i=s.black),o=(i||n).toString().match(x.RegEx.valueUnwrap)[1].replace(/,(\s+)?/g," ")}return 8>=f||3!==o.split(" ").length||(o+=" 1"),o;case"inject":return 8>=f?4===n.split(" ").length&&(n=n.split(/\s+/).slice(0,3).join(" ")):3===n.split(" ").length&&(n+=" 1"),(8>=f?"rgb":"rgba")+"("+n.replace(/\s+/g,",").replace(/\.(\d)+(?=,)/g,"")+")"}}}()}},Names:{camelCase:function(e){return e.replace(/-(\w)/g,function(e,t){return t.toUpperCase()})},SVGAttribute:function(e){var t="width|height|x|y|cx|cy|r|rx|ry|x1|x2|y1|y2";return(f||v.State.isAndroid&&!v.State.isChrome)&&(t+="|transform"),new RegExp("^("+t+")$","i").test(e)},prefixCheck:function(e){if(v.State.prefixMatches[e])return[v.State.prefixMatches[e],!0];for(var t=["","Webkit","Moz","ms","O"],r=0,a=t.length;a>r;r++){var n;if(n=0===r?e:t[r]+e.replace(/^\w/,function(e){return e.toUpperCase()}),g.isString(v.State.prefixElement.style[n]))return v.State.prefixMatches[e]=n,[n,!0]}return[e,!1]}},Values:{hexToRgb:function(e){var t=/^#?([a-f\d])([a-f\d])([a-f\d])$/i,r=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i,a;return e=e.replace(t,function(e,t,r,a){return t+t+r+r+a+a}),a=r.exec(e),a?[parseInt(a[1],16),parseInt(a[2],16),parseInt(a[3],16)]:[0,0,0]},isCSSNullValue:function(e){return 0==e||/^(none|auto|transparent|(rgba\(0, ?0, ?0, ?0\)))$/i.test(e)},getUnitType:function(e){return/^(rotate|skew)/i.test(e)?"deg":/(^(scale|scaleX|scaleY|scaleZ|alpha|flexGrow|flexHeight|zIndex|fontWeight)$)|((opacity|red|green|blue|alpha)$)/i.test(e)?"":"px"},getDisplayType:function(e){var t=e&&e.tagName.toString().toLowerCase();return/^(b|big|i|small|tt|abbr|acronym|cite|code|dfn|em|kbd|strong|samp|var|a|bdo|br|img|map|object|q|script|span|sub|sup|button|input|label|select|textarea)$/i.test(t)?"inline":/^(li)$/i.test(t)?"list-item":/^(tr)$/i.test(t)?"table-row":/^(table)$/i.test(t)?"table":/^(tbody)$/i.test(t)?"table-row-group":"block"},addClass:function(e,t){e.classList?e.classList.add(t):e.className+=(e.className.length?" ":"")+t},removeClass:function(e,t){e.classList?e.classList.remove(t):e.className=e.className.toString().replace(new RegExp("(^|\\s)"+t.split(" ").join("|")+"(\\s|$)","gi")," ")}},getPropertyValue:function(e,r,n,o){function s(e,r){function n(){u&&x.setPropertyValue(e,"display","none")}var l=0;if(8>=f)l=$.css(e,r);else{var u=!1;if(/^(width|height)$/.test(r)&&0===x.getPropertyValue(e,"display")&&(u=!0,x.setPropertyValue(e,"display",x.Values.getDisplayType(e))),!o){if("height"===r&&"border-box"!==x.getPropertyValue(e,"boxSizing").toString().toLowerCase()){var c=e.offsetHeight-(parseFloat(x.getPropertyValue(e,"borderTopWidth"))||0)-(parseFloat(x.getPropertyValue(e,"borderBottomWidth"))||0)-(parseFloat(x.getPropertyValue(e,"paddingTop"))||0)-(parseFloat(x.getPropertyValue(e,"paddingBottom"))||0);return n(),c}if("width"===r&&"border-box"!==x.getPropertyValue(e,"boxSizing").toString().toLowerCase()){var p=e.offsetWidth-(parseFloat(x.getPropertyValue(e,"borderLeftWidth"))||0)-(parseFloat(x.getPropertyValue(e,"borderRightWidth"))||0)-(parseFloat(x.getPropertyValue(e,"paddingLeft"))||0)-(parseFloat(x.getPropertyValue(e,"paddingRight"))||0);return n(),p}}var d;d=i(e)===a?t.getComputedStyle(e,null):i(e).computedStyle?i(e).computedStyle:i(e).computedStyle=t.getComputedStyle(e,null),"borderColor"===r&&(r="borderTopColor"),l=9===f&&"filter"===r?d.getPropertyValue(r):d[r],(""===l||null===l)&&(l=e.style[r]),n()}if("auto"===l&&/^(top|right|bottom|left)$/i.test(r)){var g=s(e,"position");("fixed"===g||"absolute"===g&&/top|left/i.test(r))&&(l=$(e).position()[r]+"px")}return l}var l;if(x.Hooks.registered[r]){var u=r,c=x.Hooks.getRoot(u);n===a&&(n=x.getPropertyValue(e,x.Names.prefixCheck(c)[0])),x.Normalizations.registered[c]&&(n=x.Normalizations.registered[c]("extract",e,n)),l=x.Hooks.extractValue(u,n)}else if(x.Normalizations.registered[r]){var p,d;p=x.Normalizations.registered[r]("name",e),"transform"!==p&&(d=s(e,x.Names.prefixCheck(p)[0]),x.Values.isCSSNullValue(d)&&x.Hooks.templates[r]&&(d=x.Hooks.templates[r][1])),l=x.Normalizations.registered[r]("extract",e,d)}if(!/^[\d-]/.test(l))if(i(e)&&i(e).isSVG&&x.Names.SVGAttribute(r))if(/^(height|width)$/i.test(r))try{l=e.getBBox()[r]}catch(g){l=0}else l=e.getAttribute(r);else l=s(e,x.Names.prefixCheck(r)[0]);return x.Values.isCSSNullValue(l)&&(l=0),v.debug>=2&&console.log("Get "+r+": "+l),l},setPropertyValue:function(e,r,a,n,o){var s=r;if("scroll"===r)o.container?o.container["scroll"+o.direction]=a:"Left"===o.direction?t.scrollTo(a,o.alternateValue):t.scrollTo(o.alternateValue,a);else if(x.Normalizations.registered[r]&&"transform"===x.Normalizations.registered[r]("name",e))x.Normalizations.registered[r]("inject",e,a),s="transform",a=i(e).transformCache[r];else{if(x.Hooks.registered[r]){var l=r,u=x.Hooks.getRoot(r);n=n||x.getPropertyValue(e,u),a=x.Hooks.injectValue(l,a,n),r=u}if(x.Normalizations.registered[r]&&(a=x.Normalizations.registered[r]("inject",e,a),r=x.Normalizations.registered[r]("name",e)),s=x.Names.prefixCheck(r)[0],8>=f)try{e.style[s]=a}catch(c){v.debug&&console.log("Browser does not support ["+a+"] for ["+s+"]")}else i(e)&&i(e).isSVG&&x.Names.SVGAttribute(r)?e.setAttribute(r,a):e.style[s]=a;v.debug>=2&&console.log("Set "+r+" ("+s+"): "+a)}return[s,a]},flushTransformCache:function(e){function t(t){return parseFloat(x.getPropertyValue(e,t))}var r="";if((f||v.State.isAndroid&&!v.State.isChrome)&&i(e).isSVG){var a={translate:[t("translateX"),t("translateY")],skewX:[t("skewX")],skewY:[t("skewY")],scale:1!==t("scale")?[t("scale"),t("scale")]:[t("scaleX"),t("scaleY")],rotate:[t("rotateZ"),0,0]};$.each(i(e).transformCache,function(e){/^translate/i.test(e)?e="translate":/^scale/i.test(e)?e="scale":/^rotate/i.test(e)&&(e="rotate"),a[e]&&(r+=e+"("+a[e].join(" ")+") ",delete a[e])})}else{var n,o;$.each(i(e).transformCache,function(t){return n=i(e).transformCache[t],"transformPerspective"===t?(o=n,!0):(9===f&&"rotateZ"===t&&(t="rotate"),void(r+=t+n+" "))}),o&&(r="perspective"+o+" "+r)}x.setPropertyValue(e,"transform",r)}};x.Hooks.register(),x.Normalizations.register(),v.hook=function(e,t,r){var n=a;return e=o(e),$.each(e,function(e,o){if(i(o)===a&&v.init(o),r===a)n===a&&(n=v.CSS.getPropertyValue(o,t));else{var s=v.CSS.setPropertyValue(o,t,r);"transform"===s[0]&&v.CSS.flushTransformCache(o),n=s}}),n};var S=function(){function e(){return l?T.promise||null:f}function n(){function e(e){function p(e,t){var r=a,i=a,s=a;return g.isArray(e)?(r=e[0],!g.isArray(e[1])&&/^[\d-]/.test(e[1])||g.isFunction(e[1])||x.RegEx.isHex.test(e[1])?s=e[1]:(g.isString(e[1])&&!x.RegEx.isHex.test(e[1])||g.isArray(e[1]))&&(i=t?e[1]:u(e[1],o.duration),e[2]!==a&&(s=e[2]))):r=e,t||(i=i||o.easing),g.isFunction(r)&&(r=r.call(n,w,P)),g.isFunction(s)&&(s=s.call(n,w,P)),[r||0,i,s]}function f(e,t){var r,a;return a=(t||"0").toString().toLowerCase().replace(/[%A-z]+$/,function(e){return r=e,""}),r||(r=x.Values.getUnitType(e)),[a,r]}function d(){var e={myParent:n.parentNode||r.body,position:x.getPropertyValue(n,"position"),fontSize:x.getPropertyValue(n,"fontSize")},a=e.position===N.lastPosition&&e.myParent===N.lastParent,o=e.fontSize===N.lastFontSize;N.lastParent=e.myParent,N.lastPosition=e.position,N.lastFontSize=e.fontSize;var s=100,l={};if(o&&a)l.emToPx=N.lastEmToPx,l.percentToPxWidth=N.lastPercentToPxWidth,l.percentToPxHeight=N.lastPercentToPxHeight;else{var u=i(n).isSVG?r.createElementNS("http://www.w3.org/2000/svg","rect"):r.createElement("div");v.init(u),e.myParent.appendChild(u),$.each(["overflow","overflowX","overflowY"],function(e,t){v.CSS.setPropertyValue(u,t,"hidden")}),v.CSS.setPropertyValue(u,"position",e.position),v.CSS.setPropertyValue(u,"fontSize",e.fontSize),v.CSS.setPropertyValue(u,"boxSizing","content-box"),$.each(["minWidth","maxWidth","width","minHeight","maxHeight","height"],function(e,t){v.CSS.setPropertyValue(u,t,s+"%")}),v.CSS.setPropertyValue(u,"paddingLeft",s+"em"),l.percentToPxWidth=N.lastPercentToPxWidth=(parseFloat(x.getPropertyValue(u,"width",null,!0))||1)/s,l.percentToPxHeight=N.lastPercentToPxHeight=(parseFloat(x.getPropertyValue(u,"height",null,!0))||1)/s,l.emToPx=N.lastEmToPx=(parseFloat(x.getPropertyValue(u,"paddingLeft"))||1)/s,e.myParent.removeChild(u)}return null===N.remToPx&&(N.remToPx=parseFloat(x.getPropertyValue(r.body,"fontSize"))||16),null===N.vwToPx&&(N.vwToPx=parseFloat(t.innerWidth)/100,N.vhToPx=parseFloat(t.innerHeight)/100),l.remToPx=N.remToPx,l.vwToPx=N.vwToPx,l.vhToPx=N.vhToPx,v.debug>=1&&console.log("Unit ratios: "+JSON.stringify(l),n),l}if(o.begin&&0===w)try{o.begin.call(m,m)}catch(y){setTimeout(function(){throw y},1)}if("scroll"===k){var S=/^x$/i.test(o.axis)?"Left":"Top",V=parseFloat(o.offset)||0,C,A,F;o.container?g.isWrapped(o.container)||g.isNode(o.container)?(o.container=o.container[0]||o.container,C=o.container["scroll"+S],F=C+$(n).position()[S.toLowerCase()]+V):o.container=null:(C=v.State.scrollAnchor[v.State["scrollProperty"+S]],A=v.State.scrollAnchor[v.State["scrollProperty"+("Left"===S?"Top":"Left")]],F=$(n).offset()[S.toLowerCase()]+V),s={scroll:{rootPropertyValue:!1,startValue:C,currentValue:C,endValue:F,unitType:"",easing:o.easing,scrollData:{container:o.container,direction:S,alternateValue:A}},element:n},v.debug&&console.log("tweensContainer (scroll): ",s.scroll,n)}else if("reverse"===k){if(!i(n).tweensContainer)return void $.dequeue(n,o.queue);"none"===i(n).opts.display&&(i(n).opts.display="auto"),"hidden"===i(n).opts.visibility&&(i(n).opts.visibility="visible"),i(n).opts.loop=!1,i(n).opts.begin=null,i(n).opts.complete=null,b.easing||delete o.easing,b.duration||delete o.duration,o=$.extend({},i(n).opts,o);var E=$.extend(!0,{},i(n).tweensContainer);for(var j in E)if("element"!==j){var H=E[j].startValue;E[j].startValue=E[j].currentValue=E[j].endValue,E[j].endValue=H,g.isEmptyObject(b)||(E[j].easing=o.easing),v.debug&&console.log("reverse tweensContainer ("+j+"): "+JSON.stringify(E[j]),n)}s=E}else if("start"===k){var E;i(n).tweensContainer&&i(n).isAnimating===!0&&(E=i(n).tweensContainer),$.each(h,function(e,t){if(RegExp("^"+x.Lists.colors.join("$|^")+"$").test(e)){var r=p(t,!0),n=r[0],o=r[1],i=r[2];if(x.RegEx.isHex.test(n)){for(var s=["Red","Green","Blue"],l=x.Values.hexToRgb(n),u=i?x.Values.hexToRgb(i):a,c=0;c<s.length;c++){var f=[l[c]];o&&f.push(o),u!==a&&f.push(u[c]),h[e+s[c]]=f}delete h[e]}}});for(var R in h){var O=p(h[R]),z=O[0],q=O[1],M=O[2];R=x.Names.camelCase(R);var I=x.Hooks.getRoot(R),B=!1;if(i(n).isSVG||"tween"===I||x.Names.prefixCheck(I)[1]!==!1||x.Normalizations.registered[I]!==a){(o.display!==a&&null!==o.display&&"none"!==o.display||o.visibility!==a&&"hidden"!==o.visibility)&&/opacity|filter/.test(R)&&!M&&0!==z&&(M=0),o._cacheValues&&E&&E[R]?(M===a&&(M=E[R].endValue+E[R].unitType),B=i(n).rootPropertyValueCache[I]):x.Hooks.registered[R]?M===a?(B=x.getPropertyValue(n,I),M=x.getPropertyValue(n,R,B)):B=x.Hooks.templates[I][1]:M===a&&(M=x.getPropertyValue(n,R));var W,G,D,X=!1;if(W=f(R,M),M=W[0],D=W[1],W=f(R,z),z=W[0].replace(/^([+-\/*])=/,function(e,t){return X=t,""}),G=W[1],M=parseFloat(M)||0,z=parseFloat(z)||0,"%"===G&&(/^(fontSize|lineHeight)$/.test(R)?(z/=100,G="em"):/^scale/.test(R)?(z/=100,G=""):/(Red|Green|Blue)$/i.test(R)&&(z=z/100*255,G="")),/[\/*]/.test(X))G=D;else if(D!==G&&0!==M)if(0===z)G=D;else{l=l||d();var Y=/margin|padding|left|right|width|text|word|letter/i.test(R)||/X$/.test(R)||"x"===R?"x":"y";switch(D){case"%":M*="x"===Y?l.percentToPxWidth:l.percentToPxHeight;break;case"px":break;default:M*=l[D+"ToPx"]}switch(G){case"%":M*=1/("x"===Y?l.percentToPxWidth:l.percentToPxHeight);break;case"px":break;default:M*=1/l[G+"ToPx"]}}switch(X){case"+":z=M+z;break;case"-":z=M-z;break;case"*":z=M*z;break;case"/":z=M/z}s[R]={rootPropertyValue:B,startValue:M,currentValue:M,endValue:z,unitType:G,easing:q},v.debug&&console.log("tweensContainer ("+R+"): "+JSON.stringify(s[R]),n)}else v.debug&&console.log("Skipping ["+I+"] due to a lack of browser support.")}s.element=n}s.element&&(x.Values.addClass(n,"velocity-animating"),L.push(s),""===o.queue&&(i(n).tweensContainer=s,i(n).opts=o),i(n).isAnimating=!0,w===P-1?(v.State.calls.push([L,m,o,null,T.resolver]),v.State.isTicking===!1&&(v.State.isTicking=!0,c())):w++)}var n=this,o=$.extend({},v.defaults,b),s={},l;switch(i(n)===a&&v.init(n),parseFloat(o.delay)&&o.queue!==!1&&$.queue(n,o.queue,function(e){v.velocityQueueEntryFlag=!0,i(n).delayTimer={setTimeout:setTimeout(e,parseFloat(o.delay)),next:e}}),o.duration.toString().toLowerCase()){case"fast":o.duration=200;break;case"normal":o.duration=y;break;case"slow":o.duration=600;break;default:o.duration=parseFloat(o.duration)||1}v.mock!==!1&&(v.mock===!0?o.duration=o.delay=1:(o.duration*=parseFloat(v.mock)||1,o.delay*=parseFloat(v.mock)||1)),o.easing=u(o.easing,o.duration),o.begin&&!g.isFunction(o.begin)&&(o.begin=null),o.progress&&!g.isFunction(o.progress)&&(o.progress=null),o.complete&&!g.isFunction(o.complete)&&(o.complete=null),o.display!==a&&null!==o.display&&(o.display=o.display.toString().toLowerCase(),"auto"===o.display&&(o.display=v.CSS.Values.getDisplayType(n))),o.visibility!==a&&null!==o.visibility&&(o.visibility=o.visibility.toString().toLowerCase()),o.mobileHA=o.mobileHA&&v.State.isMobile&&!v.State.isGingerbread,o.queue===!1?o.delay?setTimeout(e,o.delay):e():$.queue(n,o.queue,function(t,r){return r===!0?(T.promise&&T.resolver(m),!0):(v.velocityQueueEntryFlag=!0,void e(t))}),""!==o.queue&&"fx"!==o.queue||"inprogress"===$.queue(n)[0]||$.dequeue(n)}var s=arguments[0]&&(arguments[0].p||$.isPlainObject(arguments[0].properties)&&!arguments[0].properties.names||g.isString(arguments[0].properties)),l,f,d,m,h,b;if(g.isWrapped(this)?(l=!1,d=0,m=this,f=this):(l=!0,d=1,m=s?arguments[0].elements||arguments[0].e:arguments[0]),m=o(m)){s?(h=arguments[0].properties||arguments[0].p,b=arguments[0].options||arguments[0].o):(h=arguments[d],b=arguments[d+1]);var P=m.length,w=0;if(!/^(stop|finish)$/i.test(h)&&!$.isPlainObject(b)){var V=d+1;b={};for(var C=V;C<arguments.length;C++)g.isArray(arguments[C])||!/^(fast|normal|slow)$/i.test(arguments[C])&&!/^\d/.test(arguments[C])?g.isString(arguments[C])||g.isArray(arguments[C])?b.easing=arguments[C]:g.isFunction(arguments[C])&&(b.complete=arguments[C]):b.duration=arguments[C]}var T={promise:null,resolver:null,rejecter:null};l&&v.Promise&&(T.promise=new v.Promise(function(e,t){T.resolver=e,T.rejecter=t}));var k;switch(h){case"scroll":k="scroll";break;case"reverse":k="reverse";break;case"finish":case"stop":$.each(m,function(e,t){i(t)&&i(t).delayTimer&&(clearTimeout(i(t).delayTimer.setTimeout),i(t).delayTimer.next&&i(t).delayTimer.next(),delete i(t).delayTimer)});var A=[];return $.each(v.State.calls,function(e,t){t&&$.each(t[1],function(r,n){var o=b===a?"":b;return o===!0||t[2].queue===o||b===a&&t[2].queue===!1?void $.each(m,function(r,a){a===n&&((b===!0||g.isString(b))&&($.each($.queue(a,g.isString(b)?b:""),function(e,t){g.isFunction(t)&&t(null,!0)}),$.queue(a,g.isString(b)?b:"",[])),"stop"===h?(i(a)&&i(a).tweensContainer&&o!==!1&&$.each(i(a).tweensContainer,function(e,t){t.endValue=t.currentValue
}),A.push(e)):"finish"===h&&(t[2].duration=1))}):!0})}),"stop"===h&&($.each(A,function(e,t){p(t,!0)}),T.promise&&T.resolver(m)),e();default:if(!$.isPlainObject(h)||g.isEmptyObject(h)){if(g.isString(h)&&v.Redirects[h]){var F=$.extend({},b),E=F.duration,j=F.delay||0;return F.backwards===!0&&(m=$.extend(!0,[],m).reverse()),$.each(m,function(e,t){parseFloat(F.stagger)?F.delay=j+parseFloat(F.stagger)*e:g.isFunction(F.stagger)&&(F.delay=j+F.stagger.call(t,e,P)),F.drag&&(F.duration=parseFloat(E)||(/^(callout|transition)/.test(h)?1e3:y),F.duration=Math.max(F.duration*(F.backwards?1-e/P:(e+1)/P),.75*F.duration,200)),v.Redirects[h].call(t,t,F||{},e,P,m,T.promise?T:a)}),e()}var H="Velocity: First argument ("+h+") was not a property map, a known action, or a registered redirect. Aborting.";return T.promise?T.rejecter(new Error(H)):console.log(H),e()}k="start"}var N={lastParent:null,lastPosition:null,lastFontSize:null,lastPercentToPxWidth:null,lastPercentToPxHeight:null,lastEmToPx:null,remToPx:null,vwToPx:null,vhToPx:null},L=[];$.each(m,function(e,t){g.isNode(t)&&n.call(t)});var F=$.extend({},v.defaults,b),R;if(F.loop=parseInt(F.loop),R=2*F.loop-1,F.loop)for(var O=0;R>O;O++){var z={delay:F.delay,progress:F.progress};O===R-1&&(z.display=F.display,z.visibility=F.visibility,z.complete=F.complete),S(m,"reverse",z)}return e()}};v=$.extend(S,v),v.animate=S;var P=t.requestAnimationFrame||d;return v.State.isMobile||r.hidden===a||r.addEventListener("visibilitychange",function(){r.hidden?(P=function(e){return setTimeout(function(){e(!0)},16)},c()):P=t.requestAnimationFrame||d}),e.Velocity=v,e!==t&&(e.fn.velocity=S,e.fn.velocity.defaults=v.defaults),$.each(["Down","Up"],function(e,t){v.Redirects["slide"+t]=function(e,r,n,o,i,s){var l=$.extend({},r),u=l.begin,c=l.complete,p={height:"",marginTop:"",marginBottom:"",paddingTop:"",paddingBottom:""},f={};l.display===a&&(l.display="Down"===t?"inline"===v.CSS.Values.getDisplayType(e)?"inline-block":"block":"none"),l.begin=function(){u&&u.call(i,i);for(var r in p){f[r]=e.style[r];var a=v.CSS.getPropertyValue(e,r);p[r]="Down"===t?[a,0]:[0,a]}f.overflow=e.style.overflow,e.style.overflow="hidden"},l.complete=function(){for(var t in f)e.style[t]=f[t];c&&c.call(i,i),s&&s.resolver(i)},v(e,p,l)}}),$.each(["In","Out"],function(e,t){v.Redirects["fade"+t]=function(e,r,n,o,i,s){var l=$.extend({},r),u={opacity:"In"===t?1:0},c=l.complete;l.complete=n!==o-1?l.begin=null:function(){c&&c.call(i,i),s&&s.resolver(i)},l.display===a&&(l.display="In"===t?"auto":"none"),v(this,u,l)}}),v}(window.jQuery||window.Zepto||window,window,document)});
/* VelocityJS.org UI Pack (5.0.4). (C) 2014 Julian Shapiro. MIT @license: en.wikipedia.org/wiki/MIT_License. Portions copyright Daniel Eden, Christian Pucci. */
!function(t){"function"==typeof require&&"object"==typeof exports?module.exports=t():"function"==typeof define&&define.amd?define(["velocity"],t):t()}(function(){return function(t,a,e,r){function n(t,a){var e=[];return t&&a?($.each([t,a],function(t,a){var r=[];$.each(a,function(t,a){for(;a.toString().length<5;)a="0"+a;r.push(a)}),e.push(r.join(""))}),parseFloat(e[0])>parseFloat(e[1])):!1}if(!t.Velocity||!t.Velocity.Utilities)return void(a.console&&console.log("Velocity UI Pack: Velocity must be loaded first. Aborting."));var i=t.Velocity,$=i.Utilities,s=i.version,o={major:1,minor:1,patch:0};if(n(o,s)){var l="Velocity UI Pack: You need to update Velocity (jquery.velocity.js) to a newer version. Visit http://github.com/julianshapiro/velocity.";throw alert(l),new Error(l)}i.RegisterEffect=i.RegisterUI=function(t,a){function e(t,a,e,r){var n=0,s;$.each(t.nodeType?[t]:t,function(t,a){r&&(e+=t*r),s=a.parentNode,$.each(["height","paddingTop","paddingBottom","marginTop","marginBottom"],function(t,e){n+=parseFloat(i.CSS.getPropertyValue(a,e))})}),i.animate(s,{height:("In"===a?"+":"-")+"="+n},{queue:!1,easing:"ease-in-out",duration:e*("In"===a?.6:1)})}return i.Redirects[t]=function(n,s,o,l,c,u){function f(){s.display!==r&&"none"!==s.display||!/Out$/.test(t)||$.each(c.nodeType?[c]:c,function(t,a){i.CSS.setPropertyValue(a,"display","none")}),s.complete&&s.complete.call(c,c),u&&u.resolver(c||n)}var p=o===l-1;a.defaultDuration="function"==typeof a.defaultDuration?a.defaultDuration.call(c,c):parseFloat(a.defaultDuration);for(var d=0;d<a.calls.length;d++){var g=a.calls[d],y=g[0],m=s.duration||a.defaultDuration||1e3,X=g[1],Y=g[2]||{},O={};if(O.duration=m*(X||1),O.queue=s.queue||"",O.easing=Y.easing||"ease",O.delay=parseFloat(Y.delay)||0,O._cacheValues=Y._cacheValues||!0,0===d){if(O.delay+=parseFloat(s.delay)||0,0===o&&(O.begin=function(){s.begin&&s.begin.call(c,c);var a=t.match(/(In|Out)$/);a&&"In"===a[0]&&y.opacity!==r&&$.each(c.nodeType?[c]:c,function(t,a){i.CSS.setPropertyValue(a,"opacity",0)}),s.animateParentHeight&&a&&e(c,a[0],m+O.delay,s.stagger)}),null!==s.display)if(s.display!==r&&"none"!==s.display)O.display=s.display;else if(/In$/.test(t)){var v=i.CSS.Values.getDisplayType(n);O.display="inline"===v?"inline-block":v}s.visibility&&"hidden"!==s.visibility&&(O.visibility=s.visibility)}d===a.calls.length-1&&(O.complete=function(){if(a.reset){for(var t in a.reset){var e=a.reset[t];i.CSS.Hooks.registered[t]!==r||"string"!=typeof e&&"number"!=typeof e||(a.reset[t]=[a.reset[t],a.reset[t]])}var s={duration:0,queue:!1};p&&(s.complete=f),i.animate(n,a.reset,s)}else p&&f()},"hidden"===s.visibility&&(O.visibility=s.visibility)),i.animate(n,y,O)}},i},i.RegisterEffect.packagedEffects={"callout.bounce":{defaultDuration:550,calls:[[{translateY:-30},.25],[{translateY:0},.125],[{translateY:-15},.125],[{translateY:0},.25]]},"callout.shake":{defaultDuration:800,calls:[[{translateX:-11},.125],[{translateX:11},.125],[{translateX:-11},.125],[{translateX:11},.125],[{translateX:-11},.125],[{translateX:11},.125],[{translateX:-11},.125],[{translateX:0},.125]]},"callout.flash":{defaultDuration:1100,calls:[[{opacity:[0,"easeInOutQuad",1]},.25],[{opacity:[1,"easeInOutQuad"]},.25],[{opacity:[0,"easeInOutQuad"]},.25],[{opacity:[1,"easeInOutQuad"]},.25]]},"callout.pulse":{defaultDuration:825,calls:[[{scaleX:1.1,scaleY:1.1},.5,{easing:"easeInExpo"}],[{scaleX:1,scaleY:1},.5]]},"callout.swing":{defaultDuration:950,calls:[[{rotateZ:15},.2],[{rotateZ:-10},.2],[{rotateZ:5},.2],[{rotateZ:-5},.2],[{rotateZ:0},.2]]},"callout.tada":{defaultDuration:1e3,calls:[[{scaleX:.9,scaleY:.9,rotateZ:-3},.1],[{scaleX:1.1,scaleY:1.1,rotateZ:3},.1],[{scaleX:1.1,scaleY:1.1,rotateZ:-3},.1],["reverse",.125],["reverse",.125],["reverse",.125],["reverse",.125],["reverse",.125],[{scaleX:1,scaleY:1,rotateZ:0},.2]]},"transition.fadeIn":{defaultDuration:500,calls:[[{opacity:[1,0]}]]},"transition.fadeOut":{defaultDuration:500,calls:[[{opacity:[0,1]}]]},"transition.flipXIn":{defaultDuration:700,calls:[[{opacity:[1,0],transformPerspective:[800,800],rotateY:[0,-55]}]],reset:{transformPerspective:0}},"transition.flipXOut":{defaultDuration:700,calls:[[{opacity:[0,1],transformPerspective:[800,800],rotateY:55}]],reset:{transformPerspective:0,rotateY:0}},"transition.flipYIn":{defaultDuration:800,calls:[[{opacity:[1,0],transformPerspective:[800,800],rotateX:[0,-45]}]],reset:{transformPerspective:0}},"transition.flipYOut":{defaultDuration:800,calls:[[{opacity:[0,1],transformPerspective:[800,800],rotateX:25}]],reset:{transformPerspective:0,rotateX:0}},"transition.flipBounceXIn":{defaultDuration:900,calls:[[{opacity:[.725,0],transformPerspective:[400,400],rotateY:[-10,90]},.5],[{opacity:.8,rotateY:10},.25],[{opacity:1,rotateY:0},.25]],reset:{transformPerspective:0}},"transition.flipBounceXOut":{defaultDuration:800,calls:[[{opacity:[.9,1],transformPerspective:[400,400],rotateY:-10},.5],[{opacity:0,rotateY:90},.5]],reset:{transformPerspective:0,rotateY:0}},"transition.flipBounceYIn":{defaultDuration:850,calls:[[{opacity:[.725,0],transformPerspective:[400,400],rotateX:[-10,90]},.5],[{opacity:.8,rotateX:10},.25],[{opacity:1,rotateX:0},.25]],reset:{transformPerspective:0}},"transition.flipBounceYOut":{defaultDuration:800,calls:[[{opacity:[.9,1],transformPerspective:[400,400],rotateX:-15},.5],[{opacity:0,rotateX:90},.5]],reset:{transformPerspective:0,rotateX:0}},"transition.swoopIn":{defaultDuration:850,calls:[[{opacity:[1,0],transformOriginX:["100%","50%"],transformOriginY:["100%","100%"],scaleX:[1,0],scaleY:[1,0],translateX:[0,-700],translateZ:0}]],reset:{transformOriginX:"50%",transformOriginY:"50%"}},"transition.swoopOut":{defaultDuration:850,calls:[[{opacity:[0,1],transformOriginX:["50%","100%"],transformOriginY:["100%","100%"],scaleX:0,scaleY:0,translateX:-700,translateZ:0}]],reset:{transformOriginX:"50%",transformOriginY:"50%",scaleX:1,scaleY:1,translateX:0}},"transition.whirlIn":{defaultDuration:850,calls:[[{opacity:[1,0],transformOriginX:["50%","50%"],transformOriginY:["50%","50%"],scaleX:[1,0],scaleY:[1,0],rotateY:[0,160]},1,{easing:"easeInOutSine"}]]},"transition.whirlOut":{defaultDuration:750,calls:[[{opacity:[0,"easeInOutQuint",1],transformOriginX:["50%","50%"],transformOriginY:["50%","50%"],scaleX:0,scaleY:0,rotateY:160},1,{easing:"swing"}]],reset:{scaleX:1,scaleY:1,rotateY:0}},"transition.shrinkIn":{defaultDuration:750,calls:[[{opacity:[1,0],transformOriginX:["50%","50%"],transformOriginY:["50%","50%"],scaleX:[1,1.5],scaleY:[1,1.5],translateZ:0}]]},"transition.shrinkOut":{defaultDuration:600,calls:[[{opacity:[0,1],transformOriginX:["50%","50%"],transformOriginY:["50%","50%"],scaleX:1.3,scaleY:1.3,translateZ:0}]],reset:{scaleX:1,scaleY:1}},"transition.expandIn":{defaultDuration:700,calls:[[{opacity:[1,0],transformOriginX:["50%","50%"],transformOriginY:["50%","50%"],scaleX:[1,.625],scaleY:[1,.625],translateZ:0}]]},"transition.expandOut":{defaultDuration:700,calls:[[{opacity:[0,1],transformOriginX:["50%","50%"],transformOriginY:["50%","50%"],scaleX:.5,scaleY:.5,translateZ:0}]],reset:{scaleX:1,scaleY:1}},"transition.bounceIn":{defaultDuration:800,calls:[[{opacity:[1,0],scaleX:[1.05,.3],scaleY:[1.05,.3]},.4],[{scaleX:.9,scaleY:.9,translateZ:0},.2],[{scaleX:1,scaleY:1},.5]]},"transition.bounceOut":{defaultDuration:800,calls:[[{scaleX:.95,scaleY:.95},.35],[{scaleX:1.1,scaleY:1.1,translateZ:0},.35],[{opacity:[0,1],scaleX:.3,scaleY:.3},.3]],reset:{scaleX:1,scaleY:1}},"transition.bounceUpIn":{defaultDuration:800,calls:[[{opacity:[1,0],translateY:[-30,1e3]},.6,{easing:"easeOutCirc"}],[{translateY:10},.2],[{translateY:0},.2]]},"transition.bounceUpOut":{defaultDuration:1e3,calls:[[{translateY:20},.2],[{opacity:[0,"easeInCirc",1],translateY:-1e3},.8]],reset:{translateY:0}},"transition.bounceDownIn":{defaultDuration:800,calls:[[{opacity:[1,0],translateY:[30,-1e3]},.6,{easing:"easeOutCirc"}],[{translateY:-10},.2],[{translateY:0},.2]]},"transition.bounceDownOut":{defaultDuration:1e3,calls:[[{translateY:-20},.2],[{opacity:[0,"easeInCirc",1],translateY:1e3},.8]],reset:{translateY:0}},"transition.bounceLeftIn":{defaultDuration:750,calls:[[{opacity:[1,0],translateX:[30,-1250]},.6,{easing:"easeOutCirc"}],[{translateX:-10},.2],[{translateX:0},.2]]},"transition.bounceLeftOut":{defaultDuration:750,calls:[[{translateX:30},.2],[{opacity:[0,"easeInCirc",1],translateX:-1250},.8]],reset:{translateX:0}},"transition.bounceRightIn":{defaultDuration:750,calls:[[{opacity:[1,0],translateX:[-30,1250]},.6,{easing:"easeOutCirc"}],[{translateX:10},.2],[{translateX:0},.2]]},"transition.bounceRightOut":{defaultDuration:750,calls:[[{translateX:-30},.2],[{opacity:[0,"easeInCirc",1],translateX:1250},.8]],reset:{translateX:0}},"transition.slideUpIn":{defaultDuration:900,calls:[[{opacity:[1,0],translateY:[0,20],translateZ:0}]]},"transition.slideUpOut":{defaultDuration:900,calls:[[{opacity:[0,1],translateY:-20,translateZ:0}]],reset:{translateY:0}},"transition.slideDownIn":{defaultDuration:900,calls:[[{opacity:[1,0],translateY:[0,-20],translateZ:0}]]},"transition.slideDownOut":{defaultDuration:900,calls:[[{opacity:[0,1],translateY:20,translateZ:0}]],reset:{translateY:0}},"transition.slideLeftIn":{defaultDuration:1e3,calls:[[{opacity:[1,0],translateX:[0,-20],translateZ:0}]]},"transition.slideLeftOut":{defaultDuration:1050,calls:[[{opacity:[0,1],translateX:-20,translateZ:0}]],reset:{translateX:0}},"transition.slideRightIn":{defaultDuration:1e3,calls:[[{opacity:[1,0],translateX:[0,20],translateZ:0}]]},"transition.slideRightOut":{defaultDuration:1050,calls:[[{opacity:[0,1],translateX:20,translateZ:0}]],reset:{translateX:0}},"transition.slideUpBigIn":{defaultDuration:850,calls:[[{opacity:[1,0],translateY:[0,75],translateZ:0}]]},"transition.slideUpBigOut":{defaultDuration:800,calls:[[{opacity:[0,1],translateY:-75,translateZ:0}]],reset:{translateY:0}},"transition.slideDownBigIn":{defaultDuration:850,calls:[[{opacity:[1,0],translateY:[0,-75],translateZ:0}]]},"transition.slideDownBigOut":{defaultDuration:800,calls:[[{opacity:[0,1],translateY:75,translateZ:0}]],reset:{translateY:0}},"transition.slideLeftBigIn":{defaultDuration:800,calls:[[{opacity:[1,0],translateX:[0,-75],translateZ:0}]]},"transition.slideLeftBigOut":{defaultDuration:750,calls:[[{opacity:[0,1],translateX:-75,translateZ:0}]],reset:{translateX:0}},"transition.slideRightBigIn":{defaultDuration:800,calls:[[{opacity:[1,0],translateX:[0,75],translateZ:0}]]},"transition.slideRightBigOut":{defaultDuration:750,calls:[[{opacity:[0,1],translateX:75,translateZ:0}]],reset:{translateX:0}},"transition.perspectiveUpIn":{defaultDuration:800,calls:[[{opacity:[1,0],transformPerspective:[800,800],transformOriginX:[0,0],transformOriginY:["100%","100%"],rotateX:[0,-180]}]],reset:{transformPerspective:0,transformOriginX:"50%",transformOriginY:"50%"}},"transition.perspectiveUpOut":{defaultDuration:850,calls:[[{opacity:[0,1],transformPerspective:[800,800],transformOriginX:[0,0],transformOriginY:["100%","100%"],rotateX:-180}]],reset:{transformPerspective:0,transformOriginX:"50%",transformOriginY:"50%",rotateX:0}},"transition.perspectiveDownIn":{defaultDuration:800,calls:[[{opacity:[1,0],transformPerspective:[800,800],transformOriginX:[0,0],transformOriginY:[0,0],rotateX:[0,180]}]],reset:{transformPerspective:0,transformOriginX:"50%",transformOriginY:"50%"}},"transition.perspectiveDownOut":{defaultDuration:850,calls:[[{opacity:[0,1],transformPerspective:[800,800],transformOriginX:[0,0],transformOriginY:[0,0],rotateX:180}]],reset:{transformPerspective:0,transformOriginX:"50%",transformOriginY:"50%",rotateX:0}},"transition.perspectiveLeftIn":{defaultDuration:950,calls:[[{opacity:[1,0],transformPerspective:[2e3,2e3],transformOriginX:[0,0],transformOriginY:[0,0],rotateY:[0,-180]}]],reset:{transformPerspective:0,transformOriginX:"50%",transformOriginY:"50%"}},"transition.perspectiveLeftOut":{defaultDuration:950,calls:[[{opacity:[0,1],transformPerspective:[2e3,2e3],transformOriginX:[0,0],transformOriginY:[0,0],rotateY:-180}]],reset:{transformPerspective:0,transformOriginX:"50%",transformOriginY:"50%",rotateY:0}},"transition.perspectiveRightIn":{defaultDuration:950,calls:[[{opacity:[1,0],transformPerspective:[2e3,2e3],transformOriginX:["100%","100%"],transformOriginY:[0,0],rotateY:[0,180]}]],reset:{transformPerspective:0,transformOriginX:"50%",transformOriginY:"50%"}},"transition.perspectiveRightOut":{defaultDuration:950,calls:[[{opacity:[0,1],transformPerspective:[2e3,2e3],transformOriginX:["100%","100%"],transformOriginY:[0,0],rotateY:180}]],reset:{transformPerspective:0,transformOriginX:"50%",transformOriginY:"50%",rotateY:0}}};for(var c in i.RegisterEffect.packagedEffects)i.RegisterEffect(c,i.RegisterEffect.packagedEffects[c]);i.RunSequence=function(t){var a=$.extend(!0,[],t);a.length>1&&($.each(a.reverse(),function(t,e){var r=a[t+1];if(r){var n=e.o||e.options,s=r.o||r.options,o=n&&n.sequenceQueue===!1?"begin":"complete",l=s&&s[o],c={};c[o]=function(){var t=r.e||r.elements,a=t.nodeType?[t]:t;l&&l.call(a,a),i(e)},r.o?r.o=$.extend({},s,c):r.options=$.extend({},s,c)}}),a.reverse()),i(a[0])}}(window.jQuery||window.Zepto||window,window,document)});

var maxRange = set11Month();

function set11Month(__date){
	var x = 11;
	var CurrentDate;
	if(__date !== null && __date !== undefined) {
		CurrentDate = __date;
	}else{
		CurrentDate = new Date();
	}

	CurrentDate = CurrentDate.setMonth(CurrentDate.getMonth() + x);
	var time = new Date(CurrentDate),
		theyear = time.getFullYear(),
		themonth = 10 > (time.getMonth()+1) ? '0'+(time.getMonth()+1) : time.getMonth()+1,
		thetoday = time.getDate();
	var next11month = new Date(theyear, themonth, thetoday);
	return next11month;
	//return theyear+""+themonth+""+thetoday

}

function next10Days() {
	var today = new Date();
	var next10Day = new Date(today.getFullYear(), today.getMonth(), today.getDate()+10);
	return next10Day;
}
function nextxDays(value) {
	var today = new Date();
	var nextxDay = new Date(today.getFullYear(), today.getMonth(), today.getDate() + value );
	return nextxDay;
}
function prev2Days() {
	var today = new Date();
	var prev2Day = new Date(today.getFullYear(), today.getMonth(), today.getDate()-2);
	return prev2Day;
}
var next10Days = next10Days();
var next7Days = nextxDays(7);
var prev2Days = prev2Days();

function highlightPeriod() {
	var ad= datePickerController.getSelectedDate("andata");
	var rd= $('#soloandata').is(":checked") ? null : datePickerController.getSelectedDate("ritorno");
	//check if end is bigger than start
	if(ad != null && rd != null) {
		if(rd < ad) {
			$('#ritorno').val("");
			rd= null;
		}
		datePickerController.setPeriodHighlight("ritorno", ad, rd);
	}
	datePickerController.setPeriodHighlight("andata", ad, rd);
	datePickerController.setPeriodHighlight("ritorno", ad, rd);
}
function openAndSetReturn() {
	highlightPeriod();
	var __newDate = datePickerController.getSelectedDate("andata");
	if ( __newDate !== null){
		if( $('#soloandata').is(":checked") ) {
			showToolset();
		} else {
			datePickerController.setRangeLow("ritorno", datePickerController.getSelectedDate("andata"));
			datePickerController.show("ritorno");
			if(window.supportsTouch){
				$('#andata').blur().focusout();
			}
		}
	}	
}
function checkIfRtl() {
		if(window.isRtl){
		$('.date-picker').attr('dir', 'rtl')
		}
}
function openToolStage() {
	highlightPeriod();
	if ( datePickerController.getSelectedDate("ritorno") !== null && datePickerController.getSelectedDate("andata") !== null ){
		showToolset();
	} else if ( datePickerController.getSelectedDate("ritorno") !== null && datePickerController.getSelectedDate("andata") === null ){
		datePickerController.show("andata");
	}
}
function resizeElement(){
	if($('#fd-flightStatusDatePicker').length > 0){
		var topPos = $('#fd-but-flightStatusDatePicker').offset().top + $('#fd-but-flightStatusDatePicker').outerHeight();
		if ( $('.infoVoli__main').length > 0 ) {
			var leftPos = $('.infoVoli__main').offset().left + 18;
		}
		else if ( $('.tabsInfoAccordion__body').length > 0 ) {
			var leftPos = $('.tabsInfo__content').offset().left + 10;
		}
		$('#fd-flightStatusDatePicker').css({
				'left': leftPos + 'px',
				'top': topPos + 'px'
			});
		}
	}

function resizeElement2(){
	if($('#fd-flightStatusDatePicker2').length > 0){
		var topPos = $('#fd-but-flightStatusDatePicker2').offset().top + $('#fd-but-flightStatusDatePicker2').outerHeight();
		if ( $('.infoVoli__main').length > 0 ) {
			var leftPos = $('.infoVoli__main').offset().left + 18;
			$('#fd-flightStatusDatePicker2').css({
				'left': leftPos + 'px',
				'top': topPos + 'px'
			});
		}
		
	}
}
function showToolset() {
	$('.flightFinder, .innerFlightFinder').removeClass('ff-stage4').addClass('ff-stage5');
	if(window.supportsTouch){
		$('#ritorno').blur().focusout();
	}
}
function openAndSetDepart() {
	// datePickerController.getSelectedDate("ritorno")
	// datePickerController.setRangeHigh("andata", datePickerController.getSelectedDate("ritorno"));
	// datePickerController.show("andata");

}

//init datepickers
datePickerController.setGlobalOptions({
	"nodrag": true,
	"mousewheel": false
});

if (window.breakpointS > window.windowW || window.supportsTouch ) {
	// remove focus
	$('#andata').attr('readonly',"true")
	$('#ritorno').attr('readonly',"true")

}
//home pickers
datePickerController.createDatePicker({
	formElements:{
		"andata" : pageSettings.dateFormat,
	},
	"noTodayButton": true,
	"fillGrid" : true,
	rangeLow : new Date(),
	rangeHigh : maxRange,
	noFadeEffect: true,
	constrainSelection: false,
	wrapped:true,
	callbackFunctions:{
		"dateset": [openAndSetReturn],
		"redraw": [checkIfRtl]

	}
});

datePickerController.createDatePicker({
	formElements:{
		"ritorno" : pageSettings.dateFormat,
	},
	"noTodayButton": true,
	"fillGrid" : true,
	rangeLow : new Date(),
	rangeHigh : maxRange,
	constrainSelection: false,
	noFadeEffect: true,
	wrapped:true,
	callbackFunctions:{
		"dateset":[openToolStage],
		"redraw": [checkIfRtl]
	}
});

//info flights datepicker
datePickerController.createDatePicker({
	formElements:{
		"flightStatusDatePicker" : pageSettings.dateFormat,
	},
	"noTodayButton": true,
	"fillGrid" : true,
	rangeLow : prev2Days,
	rangeHigh : next7Days,
	noFadeEffect: true,
	constrainSelection: false,
	wrapped: false,
	callbackFunctions:{
		"domcreate":[resizeElement]
	}
});

//info flight orari datepicker 
datePickerController.createDatePicker({
	formElements:{
		"flightStatusDatePicker2" : pageSettings.dateFormat,
	},
	"noTodayButton": true,
	"fillGrid" : true,
	rangeLow : new Date(),
	noFadeEffect: true,
	constrainSelection: false,
	wrapped: false,
	callbackFunctions:{
		"domcreate":[resizeElement2]
	}	
});








'use strict';

$(function () {
	createTooltips();
});
function createTooltips() {
	$('[data-tooltip]').each(function () {
		var auto = $(this).attr('data-tooltipAuto');
		var menter = 'mouseenter focus';
		var mleave = 'mouseleave blur';
		if (Modernizr.touch) {
			menter = 'click';
			mleave = 'click';
		}
		if (auto) {
			menter = null;
			mleave = null;
		}
		$(this).qtip({
			metadata: {
				type: 'attr',
				name: 'qtipOpts'
			},
			content: {
				text: function text(val) {
					var text = $(this).attr('data-tooltip');
					var value = $('<div/>').html(text);
					return value;
				},

				title: $(this).attr('data-tooltipTitle')
			},
			style: 'dark',
			position: {
				viewport: true,
				effect: false
			},
			show: {
				event: menter
			},
			hide: {
				event: mleave
			},
			style: {
				tip: {
					corner: true
				}
			}
		});
	});
}


$(function(){

	


	var colWrapper = '<div class="mainMenu__secondLevelOrganizer"></div>';

	$('.mainMenu__secondLevelList').each(function(){
		$(this).find('.j-colType1').wrapAll(colWrapper);
		$(this).find('.j-colType2').wrapAll(colWrapper);
		$(this).find('.j-colType3').wrapAll(colWrapper);
		$(this).find('.j-colType4').wrapAll(colWrapper);
		$(this).find('.j-colType5').wrapAll(colWrapper);
		$(this).find('.j-colType6').wrapAll(colWrapper);

	});


	dropDownInit()


	$('.j-goBackThird').fastClick(function(e){
		e.preventDefault();
		$('.mainMenu__thirdLevelCover').removeClass('isActiveThird');
	});

	$('.j-goBackSecond').fastClick(function(e){
		e.preventDefault();
		$('.mainMenu__secondLevelCover').removeClass('isActiveSecond');
	});


	$('.j-openMobileMenu').fastClick(function(e){
		e.preventDefault();
		$('body').toggleClass('showMenuActivated').unbind().one(cssTransitionEnd, function(event) {
			$('.mainMenu__secondLevelCover').removeClass('isActiveSecond');
			$('.mainMenu__thirdLevelCover').removeClass('isActiveThird');
		});

	});
	$('.mainMenu__secondLevelCover').fastClick(function(e){
		return false;
	});

});


function openHoverMenu (self) {
	// console.log('opeeen')
	var selector = $('.mainMenu__firstLevelItem').eq(self);
	if (!selector.hasClass('noSecondLevel')){
		selector.addClass('isActiveFirst').find('.mainMenu__secondLevelCover').velocity('transition.slideDownIn',{ duration: 250 });	
	}
}


function hideHoverMenu (self) {
	$('.mainMenu__firstLevelItem').removeClass('isActiveFirst');
	$('.mainMenu__firstLevelItem').find('.mainMenu__secondLevelCover').velocity('transition.slideDownOut',{ duration: 250 });
}

function changeLinks(type) {
	var firstLevel = $('.mainMenu__firstLevelLink')
		firstLevelLink = firstLevel.attr('data-href'),
		secondLevel = $('.mainMenu__secondLevelLink'),
		secondLevelLink = secondLevel.attr('data-href')
		stopLink = 'javascript:;';


		if (type){
			firstLevel.each(function(){

				var self = $(this);
				if( !$(this).hasClass('noSecondLevel') ){
					self.attr({
						'href': stopLink
					});	
				}
			});

			secondLevel.each(function(){

				var self = $(this);
				if( !$(this).hasClass('noThirdLevel') ){

					self.attr({
						'href': stopLink
					});
				}

			});

		} else {

			firstLevel.each(function(){

				var self = $(this);
				self.attr({
					'href': firstLevelLink
				});	

			});

			secondLevel.each(function(){

				var self = $(this);
				self.attr({
					'href': secondLevelLink
				});	

			});
		}
}

function dropDownInit() {

	var limitForDesktop = $(window).width() > 1023 ? true : false;

	if( !window.ismobile && !window.supportsTouch && limitForDesktop ){
		var timerMenu;
		$('.mainMenu__firstLevelItem, .mainMenu__secondLevelItem, .mainMenu__thirdLevelLink').unfastClick();
		$('.mainMenu__firstLevelItem').unbind('mouseenter mouseleave');
		$('.mainMenu__firstLevelItem').mouseenter(function(){
			$('.customSelect--big select').blur();
			if ( $(this).find('.mainMenu__firstLevelLink').hasClass('noSecondLevel') ){
				return;
			}

			var self = $(this).index();
			clearTimeout(timerMenu);
			timerMenu = setTimeout(function(){
				openHoverMenu(self);
			}, window.menuSpeedTimer);

		}).mouseleave(function() {
			
			hideHoverMenu($(this));
			clearTimeout(timerMenu);

		});

	} else {

		changeLinks(true);
		$('.mainMenu__firstLevelItem').unbind('mouseenter mouseleave');

		$('.mainMenu__firstLevelItem, .mainMenu__secondLevelItem, .mainMenu__thirdLevelLink').unfastClick();
		$('.mainMenu__firstLevelItem').fastClick(function(e){
			e.preventDefault();
			var linkChecker = $(this).find('.mainMenu__firstLevelLink');
			if( linkChecker.hasClass('noSecondLevel') ) {
				if ( linkChecker.attr('target') === '_blank' ){
					window.open(linkChecker.attr('href'), '_blank');
				} else {
					window.location.assign(linkChecker.attr('href'));	
				}
				
			}
			if( $(this).hasClass('isActiveFirst') ){

				$('.mainMenu__firstLevelItem').removeClass('isActiveFirst')
				$('.mainMenu__secondLevelCover').velocity('transition.slideDownOut',{ duration: window.menuSpeed });
				$('.mainMenu__secondLevelItem').removeClass('isActiveSecond');

			} else {

				if( limitForDesktop ){
					$('.mainMenu__firstLevelItem').removeClass('isActiveFirst');
					$('.mainMenu__secondLevelCover:visible').velocity('transition.slideDownOut',{ duration: window.menuSpeed });
					$('.mainMenu__secondLevelItem').removeClass('isActiveSecond');

					$(this).addClass('isActiveFirst').find('.mainMenu__secondLevelCover').velocity('transition.slideDownIn',{ duration: window.menuSpeed });

				} else {
					// console.log($(this).find('.mainMenu__firstLevelLink').text())
					$(this).find('.mainMenu__firstLevelLink').next('.mainMenu__secondLevelCover').show().addClass('isActiveSecond');

				}
			}

		});

		$('.mainMenu__secondLevelItem').fastClick(function(e){
			e.preventDefault();
			
			var selector = $(this).find('.mainMenu__secondLevelLink')
			if ( selector.hasClass('noThirdLevel') ) {

				window.location.href = selector.attr('href');

			} else {

				selector.next('.mainMenu__thirdLevelCover').addClass('isActiveThird');
				
			}
		});
		
		$('.mainMenu__thirdLevelLink').fastClick(function(){
			var thirdLevel = $(this).attr('href');
			var linkToGo = window.location.origin + thirdLevel;
			window.location.assign(linkToGo);
		});
		// $('.mainMenu__thirdLevelLink').unfastClick();
		// $('.noSecondLevel').parent().unfastClick();
		// $('.noThirdLevel').parent().unfastClick();

	}
}

(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.SliderRange = (function() {
    function SliderRange(ref) {
      this.ref = ref;
      this.range_max = parseInt(this.ref.attr('data-max'));
      this.range_min = parseInt(this.ref.attr('data-min'));
      this.range = this.range_max - this.range_min;
      this.step = parseInt(this.ref.attr('data-step'));
      this.total_steps = Math.round(this.range / this.step);
      this.gotoStep = __bind(this.gotoStep, this);
      this.updatePosition = __bind(this.updatePosition, this);
      this.isIE = __bind(this.isIE, this);
      this.onTouchEnd = __bind(this.onTouchEnd, this);
      this.onTouchMove = __bind(this.onTouchMove, this);
      this.onTouchStart = __bind(this.onTouchStart, this);
      this.onMouseUp = __bind(this.onMouseUp, this);
      this.onMouseDown = __bind(this.onMouseDown, this);
      this.onMouseMove = __bind(this.onMouseMove, this);
      this.setInteractions = __bind(this.setInteractions, this);
      this.sliderRangeValue = this.ref.find('.sliderRange-value span');
      this.sliderRangeValue_min = this.ref.find('.sliderRange__amount__min .number');
      this.sliderRangeValue_max = this.ref.find('.sliderRange__amount__max .number');

      this.window_ref = $(window);
      this.is_ie = this.isIE();
      this.is_mobile = navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/Android/i) ? true : false;
      this.b = $('body,html');
      this.controller = this.ref.find('.js-controller');
      this.dragger = this.controller.find('.js-slider');
      this.slider = this.controller.find('.js-bar');
      this.notify = this.controller.find('.js-notify');
      if( window.isRtl ) {
        this.dragger.css('right', '0px');
      } else {
        this.dragger.css('left', '0px');
      }
      this.slide_amp = this.dragger.width();
      this.angles = [];

      this.current_pos = 0;
      this.current_step = 0;
      
      this.mouse_x = 0;
      this.mouse_y = 0;
      this.firstAngle = 0
      this.setInteractions();
      this.setTexts();
    }
    SliderRange.prototype.setTexts = function(){
      this.sliderRangeValue.text(this.range_min);
      this.sliderRangeValue_min.text(this.range_min);
      this.sliderRangeValue_max.text(this.range_max);
    }; 

    SliderRange.prototype.setInteractions = function() {
      
      if (!this.is_mobile) {
        this.b.bind('mousemove', this.onMouseMove);
        this.dragger.bind('mousedown', this.onMouseDown);
      } else {
        this.dragger.bind('touchstart', this.onTouchStart);
        this.dragger.bind('touchmove', this.onTouchMove);
        this.dragger.bind('touchend', this.onTouchEnd);
      }
    };

    SliderRange.prototype.onMouseMove = function(e) {
      if (this.is_ie) {
        this.mouse_x = e.clientX;
      } else {
        this.mouse_x = e.pageX;
      }
      this.mouse_x = parseInt(this.draggerPos) + (this.mouse_x - this.startX);
      //this.maxLeft = this.slider.closest('.sliderRange__bar').width() - this.dragger.find('.sliderRange__feed').width();
      this.maxLeft = this.slider.closest('.sliderRange__bar').width();
      if( window.isRtl ) {
        0 > this.mouse_x ? this.mouse_x = Math.abs(this.mouse_x) : this.mouse_x = 0;
        this.mouse_x = this.mouse_x < 0 ? 0 : this.mouse_x > this.maxLeft ? this.maxLeft : this.mouse_x;
      } else {
        this.mouse_x = this.mouse_x < 0 ? 0 : this.mouse_x > this.maxLeft ? this.maxLeft : this.mouse_x; 
      }
      if (this.is_scrolling) {
        return this.updatePosition();
      }
    };

    SliderRange.prototype.onMouseDown = function(e) {
      e.preventDefault();
      if (this.is_ie) {
        this.startX = e.clientX;
      } else {
        this.startX = e.pageX;
      }
      this.is_scrolling = true;
      this.b.bind('mouseup', this.onMouseUp);
      return this.draggerPos = window.isRtl ? this.dragger.css('right') : this.dragger.css('left');
    };

    SliderRange.prototype.onMouseUp = function(e) {
      e.preventDefault();
      this.b.unbind('mouseup');
      return this.is_scrolling = false;
    };

    SliderRange.prototype.onTouchStart = function(e) {
      var touchobj;
      touchobj = e.originalEvent.changedTouches[0];
      this.is_scrolling = true;
      this.startX = touchobj.clientX;
      if( window.isLtr ) {
        this.draggerPos = this.dragger.css('left');
      }
      if( window.isRtl ) {
        this.draggerPos = this.dragger.css('right');
      }
    };

    SliderRange.prototype.onTouchMove = function(e) {
      var touchobj;
      e.preventDefault();
      touchobj = e.originalEvent.changedTouches[0];
      this.mouse_x = touchobj.clientX;
      this.mouse_x = parseInt(this.draggerPos) + (this.mouse_x - this.startX);
      this.maxLeft = this.slider.closest('.sliderRange__bar').width();
      this.mouse_x = this.mouse_x < 0 ? 0 : this.mouse_x > this.maxLeft ? this.maxLeft : this.mouse_x;


      if (this.is_scrolling) {
        return this.updatePosition();
      }
    };


    SliderRange.prototype.onTouchEnd = function(e) {
      return this.is_scrolling = false;

    };

    SliderRange.prototype.isIE = function() {
      var __nav;
      __nav = navigator.userAgent.toLowerCase();
      if (__nav.indexOf('msie') !== -1) {
        return parseInt(__nav.split('msie')[1]);
      } else {
        return false;
      }
    };

    SliderRange.prototype.updatePosition = function() {
      var state;
      this.area_wx = this.slider.closest('.sliderRange__bar').width();
      state = Math.round((this.mouse_x * this.range_max) / this.area_wx);
      var real_selected_value = Math.round((this.mouse_x * this.range) / this.area_wx) + this.range_min
      if (state === this.range_max) {
        state = this.range_max - 1;
      }
      if (state !== this.current_state) {
        this.current_state = state;
      }
      if( window.isLtr ) {
        this.dragger.css({
          left: "" + this.mouse_x + "px"
        });
      }

      if( window.isRtl ) {
        this.dragger.css({
          right: this.mouse_x + "px"
        });
      }

      this.slider.css({
        width: this.mouse_x + "px"
      });
      
      var selected_value_step =  Math.round((this.mouse_x * this.total_steps) / this.area_wx) * this.step;
      var visible_selected_value = selected_value_step + this.range_min
      if (visible_selected_value > this.range_max){visible_selected_value = this.range_max;}
      this.sliderRangeValue.text(visible_selected_value);
    };

    SliderRange.prototype.gotoStep = function() {
      var mx;

      this.current_state = this.firstAngle;
      mx = (this.area_wx * this.firstAngle) / this.range_max;
      return this.dragger.css({
        left: "" + mx + "px"
      });
    };

    return SliderRange;

  })();

}).call(this);



var sliderRange = $('.js-sliderRange');
  if (sliderRange.length) {
    sliderRange.each(function (i, el) {
      new SliderRange($(el));
    });
  }



$.fn.responsiveTable = function(settings) {

	var selector = this.selector.replace('.','') + '',
		beforeStyle = '@media screen and (max-width: '+settings.maxWidth+'px) {',
		windowWidth = $(window).width();


	if ( $('style'+selector).length != 0 ){
		$('style'+selector).remove();
	}



	this.each(function(i,val){
		var typeTable = $(this).attr('data-tableType');

		if ( $(this).find("thead").length === 0 ) {
			// var thElement = $(this).find("tbody tr:eq(0)");
			var thElement = $(this).find("tbody tr").eq(0).find("th").parent('tr');
			if(thElement.length > 0) {
				$(this).prepend("<thead>"+thElement.html()+"</thead>");
				thElement.remove();
			}
		}
		//various
		if ( typeTable === "threeCol--repeat" ||  typeTable === "fourCol--repeat" ||  typeTable === "fiveCol--repeat" ){
			$(this).addClass('repeatVersion');
		}

		if ( typeTable === "twoCol"){
			var thTitle0 = ifHasSpan($(this).find('thead tr th').eq(0)),
				thTitle1 = ifHasSpan($(this).find('thead tr th').eq(1));

				thTitle0 === "" ? beforeStyle += "" : beforeStyle += '.respTable'+i+' .thTitle0:before { content: "'+thTitle0+'"; }' ;
				beforeStyle += '.respTable'+i+' .thTitle1:before { content: "'+thTitle1+'"; }';

			$(this).addClass('respTable'+i).find('tbody tr').each(function(index, el) {
				$(this).find('td').eq(0).addClass('title').addClass('thTitle0');
				$(this).find('td').eq(1).addClass('thTitle1');
			});
		}

		if ( typeTable === "threeCol" ||  typeTable === "threeCol--repeat"){
			var thTitle0 = ifHasSpan($(this).find('thead tr th').eq(0)),
				thTitle1 = ifHasSpan($(this).find('thead tr th').eq(1)),
				thTitle2 = ifHasSpan($(this).find('thead tr th').eq(2));

				thTitle0 === "" ? beforeStyle += "" : beforeStyle += '.respTable'+i+' .thTitle0:before { content: "'+thTitle0+'"; }' ;
				beforeStyle += '.respTable'+i+' .thTitle1:before { content: "'+thTitle1+'"; }';
				beforeStyle += '.respTable'+i+' .thTitle2:before { content: "'+thTitle2+'"; }';

			$(this).addClass('respTable'+i).find('tbody tr').each(function(index, el) {
				if ( $(this).find('td').length === 3 ){
					$(this).find('td').eq(0).addClass('thTitle0 title');
					$(this).find('td').eq(1).addClass('thTitle1');
					$(this).find('td').eq(2).addClass('thTitle2');
				} else {
					$(this).find('td').eq(0).addClass('thTitle1');
					$(this).find('td').eq(1).addClass('thTitle2');
				}

				if (typeTable === "threeCol"){
					var trh =$(this);
					var title1_height = $(this).find('.thTitle1').height();
					var title2_height = $(this).find('.thTitle2').height();

					if(title1_height > title2_height) {
						trh.removeClass("rightFix").addClass("leftFix");
					}	else {
						trh.removeClass("leftFix").addClass("rightFix");
					}
				}
			});
		}

		if ( typeTable === "fourCol" ||  typeTable === "fourCol--repeat"){
				$(this).addClass('respTable'+i);
				/*
				var txt=$(this).text().trim();
				var thead =$(this).find('thead tr th');

				var thTitle0 = ifHasSpan(thead.eq(0)),
					thTitle1 = ifHasSpan(thead.eq(1)),
					thTitle2 = ifHasSpan(thead.eq(2)),
					thTitle3 = ifHasSpan(thead.eq(3));

					if ( thTitle0 === "") {
						beforeStyle += ""
					} else {
						beforeStyle += '.respTable'+i+' .thTitle0:before { content: "'+thTitle0+'"; }'
					}

					beforeStyle += '.respTable'+i+' .thTitle1:before { content: "'+thTitle1+'"; }';
					beforeStyle += '.respTable'+i+' .thTitle2:before { content: "'+thTitle2+'"; }';
					beforeStyle += '.respTable'+i+' .thTitle3:before { content: "'+thTitle3+'"; }';

				var skipTimes = 0;
				var counterSkipTimes = 0;*/

				var tr_inside= $(this).find('tbody tr');

				/*

				tr_inside.each(function(x,y){

					if(counterSkipTimes > 0) {

						//check row
						var td=$(this).find('td');
						var cp0= td.eq(0).clone().addClass('thTitle1').addClass("showOnMobile");
						var cp1= td.eq(1).clone().addClass('thTitle2').addClass("showOnMobile");
						var tr_nw= tr_inside.eq(x-(skipTimes-counterSkipTimes+1));
						$(this).addClass("hideOnMobile");
						tr_nw.find(".thTitle2:last").after(cp0);
						tr_nw.find(".thTitle1:last").after(cp1);

						var left = cp0.outerHeight()
						var right = cp1.outerHeight()

						if (left >= right) {
							cp0.addClass('rightFix')
						} else {
							cp1.addClass('leftFix')
						}
						counterSkipTimes -= 1;
						return true;
					}

					var td=$(this).find('td');
					td.eq(0).addClass('title thTitle0 j-openTable');
					td.eq(1).addClass('thTitle1');
					td.eq(2).addClass('thTitle2');
					td.eq(3).addClass('thTitle3');

					//checkrowspan
					var rowspan = parseInt(td.attr("rowspan"));
					var rowspan2 = parseInt(td.eq(3).attr("rowspan"));
					if(rowspan > 1) {
						$(this).addClass("hasRowSpan");
						skipTimes = counterSkipTimes = rowspan -1;
						$('.hasRowSpan').each(function(){
							var left = $(this).find('.thTitle1').outerHeight();
							var right = $(this).find('.thTitle2').outerHeight();
							if (left >= right) {
								$(this).find('.thTitle1').addClass('rightFix')
							} else {
								$(this).find('.thTitle2').addClass('leftFix')
							}
						});
					} else if (rowspan2 > 1){ // when the last item has a rowspan it will cover at this case
						
					}*/
					//});
					//check rowspan for table


					var tr=$(this).find('tbody tr');
					//var td=$(this).find('td');
					tr.each(function() {
						var td=$(this).find('td');
						//create table;
						for(var t=0; t< td.length; t++ ) {
							var selTd = td.eq(t);
							var rs = parseInt(selTd.attr("rowspan"));
							if(rs >1) {
								var el = selTd.clone().removeAttr("rowspan").addClass('showOnMobile');
								var indexRow = selTd.closest('tr').index() + 1;
								var endRow= (indexRow + rs - 1);
								var rows= tr;
								for (var c= indexRow; c < endRow; c++){
									var rowToInsert= rows.eq(c);
									var rowToInsert_td=rowToInsert.find("td");
									if(rowToInsert_td.eq(t).length > 0) {
										rowToInsert_td.eq(t).before(el);
									} else {
										rowToInsert_td.eq(t-1).after(el);
									}
								}
							}
						}
					});

					//add title
					var th=$(this).find('thead tr th');
					for (var k=0; k < th.length; k++) {
						var title_th = ifHasSpan(th.eq(k));
						if(title_th !== "") {
							beforeStyle += '.respTable'+i+' .thTitle'+k+':before { content: "'+title_th+'"; }'
						}
						tr.each(function() {
							var td=$(this).find('td, th');
							td.eq(0).addClass('title j-openTable');
							for(var h=0; h< td.length; h++ ) {
								td.eq(h).addClass('thTitle'+(h)).attr("height",0);
							}
						});
					}
		}

		if ( typeTable === "fiveCol--repeat" || typeTable === "five--repeat" || typeTable === "six--repeat" || typeTable === "seven--repeat"){

				var txt=$(this).text().trim();

				var thTitle0 = ifHasSpan($(this).find('thead tr th').eq(0)),
					thTitle1 = ifHasSpan($(this).find('thead tr th').eq(1)),
					thTitle2 = ifHasSpan($(this).find('thead tr th').eq(2)),
					thTitle3 = ifHasSpan($(this).find('thead tr th').eq(3)),
					thTitle4 = ifHasSpan($(this).find('thead tr th').eq(4)),
					thTitle5 = ifHasSpan($(this).find('thead tr th').eq(5)),
					thTitle6 = ifHasSpan($(this).find('thead tr th').eq(6)),
					thTitle7 = ifHasSpan($(this).find('thead tr th').eq(7));

					if ( thTitle0 === "") {
						beforeStyle += ""
					} else {
						beforeStyle += '.respTable'+i+' .thTitle0:before { content: "'+thTitle0+'"; }'
					}
					beforeStyle += '.respTable'+i+' .thTitle1:before { content: "'+thTitle1+'"; }';
					beforeStyle += '.respTable'+i+' .thTitle2:before { content: "'+thTitle2+'"; }';
					beforeStyle += '.respTable'+i+' .thTitle3:before { content: "'+thTitle3+'"; }';
					beforeStyle += '.respTable'+i+' .thTitle4:before { content: "'+thTitle4+'"; }';
					beforeStyle += '.respTable'+i+' .thTitle5:before { content: "'+thTitle5+'"; }';
					beforeStyle += '.respTable'+i+' .thTitle6:before { content: "'+thTitle6+'"; }';
					beforeStyle += '.respTable'+i+' .thTitle7:before { content: "'+thTitle7+'"; }';

				var skipTimes = 0;
				var counterSkipTimes = 0;
				var tr_inside= $(this).addClass('respTable'+i).find('tbody tr');
				tr_inside.each(function(x,y){
					if(counterSkipTimes > 0) {

						//check row
						var td=$(this).find('td');
						var cp0= td.eq(0).clone().addClass('thTitle1').addClass("showOnMobile");
						var cp1= td.eq(1).clone().addClass('thTitle2').addClass("showOnMobile");
						var tr_nw= tr_inside.eq(x-(skipTimes-counterSkipTimes+1));
						$(this).addClass("hideOnMobile");
						tr_nw.find(".thTitle2:last").after(cp0);
						tr_nw.find(".thTitle1:last").after(cp1);

							var left = cp0.outerHeight()
							var right = cp1.outerHeight()

							if (left >= right) {
								cp0.addClass('rightFix')
							} else {
								cp1.addClass('leftFix')
							}


						counterSkipTimes -= 1;
						return true;
					}

					var td=$(this).find('td');
					td.eq(0).addClass('title thTitle0 j-openTable');
					td.eq(1).addClass('thTitle1');
					td.eq(2).addClass('thTitle2');
					td.eq(3).addClass('thTitle3');
					td.eq(4).addClass('thTitle4');
					td.eq(5).addClass('thTitle5');
					td.eq(6).addClass('thTitle6');
					td.eq(7).addClass('thTitle7');
					var rowspan=parseInt(td.attr("rowspan"));
					if(rowspan > 1) {
						$(this).addClass("hasRowSpan");
						skipTimes = counterSkipTimes = rowspan -1;

						$('.hasRowSpan').each(function(){
							var left = $(this).find('.thTitle1').outerHeight()
							var right = $(this).find('.thTitle2').outerHeight()

							if (left >= right) {
								$(this).find('.thTitle1').addClass('rightFix')
							} else {
								$(this).find('.thTitle2').addClass('leftFix')
							}
						});
					}

				});

		}

	});

	beforeStyle += '}';
	$('head').append('<style id="'+selector+'">'+beforeStyle+'</style>');

	$('.j-openTable').unfastClick().fastClick(function(){
		// console.log (settings.maxWidth , $(window).width())
		if ( settings.maxWidth > $(window).width() ){
			$(this).closest('tr').toggleClass('isActive');
			$( window ).trigger('resize.accordion');
		}
	});

	$(window).resize(function(){

		var timeOuter;
		if( $(window).width() !== windowWidth ){
			windowWidth = $(window).width()
			clearTimeout(timeOuter);
			$('.j-responsiveTable').hide();
			timeOuter = setTimeout(function(){
				$('.j-responsiveTable').show();
			}, 50);
		}

	});
};


function ifHasSpan (element){
	if ( element.find('span').length > 0 || element.find('p').length > 0  ) {
		var selector = element.find('span').attr('class');
		if( typeof selector !== "undefined" ) {
			if ( selector.indexOf('i-') > -1 ) {
				var beforeTag = window.getComputedStyle(document.querySelector('.'+selector), '::before').getPropertyValue('content').replace(/"/g, "").replace(/'/g, "");	
				return beforeTag +' '+ element.text().trim();
			} else {
				return element.text().trim();
			}
		} else {
			return element.text().trim();	
		}
	} else {
		return element.text().trim();
	}
}
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

$(function () {
  var SelectFilering = (function () {
    function SelectFilering(wrap) {
      _classCallCheck(this, SelectFilering);

      this.wrap = wrap;
      this.filterTargets = wrap.find('[data-filter]');
      this.filterSelect = wrap.find('[data-filterSelect]');
      this.init();
    }

    _createClass(SelectFilering, [{
      key: 'init',
      value: function init() {
        this.hideItems();
        this.listners();
        this.showItems('*');
      }
    }, {
      key: 'showItems',
      value: function showItems(filter) {
        if (filter !== '') {
          if (filter !== '*') {
            var _items = this.wrap.find('[data-filter="' + filter + '"]');
            this.hideItems();
            if (_items.length) {
              _items.show().attr({ 'aria-hidden': "false" });
            }
          } else {
            this.filterTargets.show().attr({ 'aria-hidden': "false" });;
          }
        } else {
          this.hideItems();
        }
      }
    }, {
      key: 'hideItems',
      value: function hideItems() {
        this.filterTargets.hide().attr({ 'aria-hidden': "true" });
      }
    }, {
      key: 'listners',
      value: function listners() {
        var _this = this;

        this.filterSelect.on('change', function (event) {
          var _filter = _this.filterSelect.val();
          _this.showItems(_filter);
        });
      }
    }]);

    return SelectFilering;
  })();

  var filters = $('[data-selectFiltering]');
  filters.each(function (index, cur) {
    $(cur).data('selectFilter', new SelectFilering($(cur)));
  });
});


if (!Date.now) {
    Date.now = function() { return new Date().getTime(); }
}

var inputFocuseID;
var focusInTime;
var focusOutTime;

//search open/close
function openSearchInputBar() {
	if($('.mainMenu__navigationSearch').hasClass("open")){
		return;
	}	
	$('.ac-renderer').velocity('stop', true).velocity('transition.fadeIn');
	var howBig;		
	if(window.windowW > 1023) {
		howBig = $('.mainMenu__nav').width() + $('.mainMenu__navigationSearch').width() + 20;
		$('.mainMenu__navigationSearch').velocity('stop', true).velocity({width : howBig}).addClass("open");

	} else if(window.windowW > 800) {
		howBig = 400;
		$('.mainMenu__navigationSearch').velocity('stop', true).velocity({width : howBig}).addClass("open");
	}
	checkIfInputIsFocused();
}
function closeSearchInputBar() {
	$('.mainMenu__navigationSearch').velocity('stop', true).velocity('reverse').attr('style','').removeClass("open");
	$('.ac-renderer').velocity('stop', true).velocity('transition.fadeOut');
}

function checkIfInputIsFocused() {
	clearTimeout(inputFocuseID);	
	inputFocuseID= setTimeout(function() {
		if ($('#mainMenu__search:focus').length<1) {
			$('#mainMenu__search').focus();
		}
	},10);
}


$(function(){
	//script
	$('.mainMenu__navigationSearchIcon').focusin(function(event) {
		focusInTime=Date.now();
		openSearchInputBar();
	}).focusout(function(event) {
		focusOutTime=Date.now();
		//fix error firefox
		if((focusOutTime - focusInTime) < 100) {
			checkIfInputIsFocused();
			return;
		}
		//DELAY
		setTimeout(function() {
			if ($('.mainMenu__navigationSearchSubmit:focus').length<1) {
				closeSearchInputBar();
			}
		},50);
	});

	
	$('.mainMenu__navigationSearchSubmit').focusin(function(event) {
		openSearchInputBar();
		
	}).focusout(function(event) {
		closeSearchInputBar();

	});


	$('.mainMenu__navigationSearchSubmit').click(function(event) {
		event.preventDefault();
		event.stopPropagation();
		event.stopImmediatePropagation();
		var str_length=$('#mainMenu__search').val().length;
		if(str_length>0) {
			$(this).closest("form").submit();
		}
	});

	$('.mainMenu__navigationSearchSubmit').mousedown(function(event) {
		//return
		var fm=$('#mainMenu__navigationSearch');
		event.preventDefault();
		event.stopPropagation();
		event.stopImmediatePropagation();
		if(!fm.hasClass("open")) {
			$('.mainMenu__navigationSearchIcon').focus();
		}
	});

	$('.j-openSearch').click(function(event) {
		$('.header').toggleClass("openSearchMobile");

	});

	$('.j-readNotification').on('click',function(e){
		$(this).parent().addClass('isRead');
		return false;
	});

});


function fixPositions() {
	if( $('.j-groupContainer[data-group="notificationGroup"]').length > 0){
		var _group1 = $('.j-groupContainer[data-group="notificationGroup"]'),
			_group2 = $('.j-groupContainer[data-group="recentGroup"]'),
			_this1 = $('.j-userMenuLink[data-group="notificationGroup"]'),
			_this2 = $('.j-userMenuLink[data-group="recentGroup"]'),
			modMargin = parseInt($('.header').find('.mod').css('margin-left').replace('px','')),
			posLeft1 = _this1.offset().left,
			selWidth1 = _this1.outerWidth(),
			selMarginLeft1 = parseInt(_this1.css('margin-left').replace('px','')),
			elemWidth1 = _group1.outerWidth(),
			posLeft2 = _this2.offset().left,
			selWidth2 = _this2.outerWidth(),
			selMarginLeft2 = parseInt(_this2.css('margin-left').replace('px','')),
			elemWidth2 = _group2.outerWidth();
		if(window.breakpointS > windowW ){
			return
		}
		if( window.isRtl ) {
			_group1.css({left: $(".userMenu__notifications").offset().left});
			_group2.css({left: $(".userMenu__recent").offset().left});
		} else {
			_group1.css({
				left : posLeft1  - (elemWidth1-selWidth1)
			});
			_group2.css({
				left : posLeft2  - (elemWidth2-selWidth2)
			});
		}
	}
}

'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

$(function () {
	var TopBar = (function () {
		function TopBar(header) {
			_classCallCheck(this, TopBar);

			this.ref = header;
			this.doc = $(document);
			this.body = $('body');
			this.lastOpen = null;
			this.isAnimating = false;
			this.eventType = 'click.topBar';
			this.init();
		}

		_createClass(TopBar, [{
			key: 'init',
			value: function init() {
				this.listeners();
			}
		}, {
			key: 'closeLast',
			value: function closeLast() {
				var _this = this;

				var cond = arguments.length <= 0 || arguments[0] === undefined ? true : arguments[0];

				var defer = $.Deferred();
				if (this.lastOpen == null) {
					defer.resolve();
				} else {
					var children = this.lastOpen.submenu.find('[tabindex],input,select,a');
					children.attr('tabindex', -1);
					this.lastOpen.cur.removeClass('isActive');
					var target = this.lastOpen.cur.data().group;
					var animation = this.getAnimationType(target, false);
					this.lastOpen.submenu.velocity(animation, {
						complete: function complete() {
							if (_this.lastOpen.submenu) _this.lastOpen.submenu.removeClass('isActive');
							_this.lastOpen = null;
							_this.body.removeClass('userMenuIsActive');
							defer.resolve();
						},
						duration: 200
					});
				}
				return defer.promise();
			}
		}, {
			key: 'focusOnChild',
			value: function focusOnChild(submenu) {
				var children = submenu.find('[tabindex],input,select,a	');
				children.attr('tabindex', 1).eq(0).focus();
			}
		}, {
			key: 'getAnimationType',
			value: function getAnimationType(submenutype) {
				var cond = arguments.length <= 1 || arguments[1] === undefined ? true : arguments[1];

				var scrollHeightAni = {
					opacity: 1,
					height: function height() {
						return $(this).get(0).scrollHeight;
					}
				};
				switch (submenutype) {
					case 'languageGroup':
					case 'loginGroup':
					case 'supportGroup':
						if (cond) {
							return scrollHeightAni;
						} else {
							return { height: 0 };
						}
						break;
					case 'notificationGroup':
					case 'recentGroup':
						if (cond) {
							if ($(window).width() >= 800) return "slideDown";else return scrollHeightAni;
						} else {
							if ($(window).width() >= 800) return "slideUp";else return { height: 0 };
						}
				}
			}
		}, {
			key: 'onAnimationComplete',
			value: function onAnimationComplete(cur, submenu) {
				cur.addClass('isActive');
				submenu.addClass('isActive');
				this.lastOpen = {
					cur: cur,
					submenu: submenu
				};
			}
		}, {
			key: 'closeMainMenu',
			value: function closeMainMenu() {
				this.body.removeClass('showMenuActivated');
				$('.mainMenu__thirdLevelContainer, .mainMenu__thirdLevel, .mainMenu__secondLevel, .mainMenu__navigation__firstLevelItemLink').removeClass('isActive');
			}
		}, {
			key: 'toggleSubmenu',
			value: function toggleSubmenu(target, cur) {
				var _this2 = this;

				var _isOpen = cur.hasClass('isActive');
				var submenu = $('.j-groupContainer[data-group="' + target + '"]');
				this.closeMainMenu();
				if (!_isOpen) {
					$("#header_code").focus();
					cur.addClass('isActive');
					if (submenu.length) {
						this.isAnimating = true;
						switch (target) {
							case 'languageGroup':
							case 'loginGroup':
							case 'supportGroup':

								var _height = submenu.get(0).scrollHeight;
								this.closeLast().then(function () {
									submenu.velocity(_this2.getAnimationType(target, true), {
										queue: false,
										duration: 300,
										begin: function begin() {
											return _this2.body.addClass('userMenuIsActive');
										},
										complete: function complete() {
											_this2.onAnimationComplete(cur, submenu);
											_this2.isAnimating = false;
											_this2.focusOnChild(submenu);
											$(window).trigger("resize");
										}
									});
								});
								break;
							case 'notificationGroup':
								$('.userMenu__notificationsCount').text(0);
							case 'recentGroup':
								this.closeLast(false).then(function () {
									submenu.show().velocity(_this2.getAnimationType(target, true), {
										begin: function begin() {
											return _this2.body.addClass('userMenuIsActive');
										},
										queue: false,
										duration: 300,
										complete: function complete() {
											_this2.onAnimationComplete(cur, submenu);
											_this2.isAnimating = false;
											_this2.focusOnChild(submenu);
											$(window).trigger("resize");
										}
									});
								});
								break;

						}
					}
				} else {
					if (submenu.length) {
						cur.removeClass('isActive');
						switch (target) {
							case 'languageGroup':
							case 'loginGroup':
							case 'supportGroup':
								submenu.velocity(this.getAnimationType(target, false), {
									queue: false,
									complete: function complete() {
										cur.removeClass('isActive');
										submenu.removeClass('isActive');
										_this2.body.removeClass('userMenuIsActive');
										$(window).trigger("resize");
									}
								});
								break;
							case 'notificationGroup':
							case 'recentGroup':
								submenu.velocity(this.getAnimationType(target, false), {
									begin: function begin() {
										return _this2.body.addClass('userMenuIsActive');
									},
									queue: false,
									duration: 150,
									complete: function complete() {
										cur.removeClass('isActive');
										submenu.removeClass('isActive');
										_this2.body.removeClass('userMenuIsActive');
										_this2.lastOpen = null;
										$(window).trigger("resize");
									}
								});
								break;

						}
					}
				}
			}
		}, {
			key: 'isMenuLink',
			value: function isMenuLink(e) {
				var cur = $(e.target);
				_parent = cur.closest('.j-userMenuLink');
				if (_parent.length) return true;else return false;
			}
		}, {
			key: 'listeners',
			value: function listeners() {
				var _this3 = this;

				this.doc.on('MAINMENU:TOGGLE', function () {
					_this3.closeLast();
				});
				this.doc.on(this.eventType, function (e) {});

				this.doc.on(this.eventType, function (e) {
					var cur = $(e.target);
					if (cur.is('.j-userMenuLink') || _this3.isMenuLink(e)) {
						e.preventDefault();
						if (_this3.isMenuLink(e)) cur = cur.closest('.j-userMenuLink');
						var submenu = cur.data().group || null;
						if (submenu !== null) {
							if (!_this3.isAnimating) _this3.toggleSubmenu(submenu, cur);
						}
					} else {
						if (_this3.lastOpen !== null) {
							// e.preventDefault();
							if (_this3.lastOpen.cur.is('.userMenu__notifications.j-userMenuLink') || _this3.lastOpen.cur.is('.userMenu__recent.j-userMenuLink')) _this3.closeLast();
						}
					}
				});
			}
		}]);

		return TopBar;
	})();

	window.TopBarRef = new TopBar($('.header'));
});

'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

$.fn.visibleHeight = function () {
	var elBottom, elTop, scrollBot, scrollTop, visibleBottom, visibleTop;
	scrollTop = $(window).scrollTop();
	scrollBot = scrollTop + $(window).height();
	elTop = this.offset().top;
	elBottom = elTop + this.outerHeight();
	visibleTop = elTop < scrollTop ? scrollTop : elTop;
	visibleBottom = elBottom > scrollBot ? scrollBot : elBottom;

	return visibleBottom - visibleTop;
};

var Accordion = (function () {
	function Accordion(el) {
		_classCallCheck(this, Accordion);

		this.$doc = $(document);
		this.mode = $(el).data("mode");
		this.accordions = $(el).find('.j-accordion');
		this.accordions.data('firstTime', true);
		this.window_width = $(window).width();
		this.atleast = $(el).data("atleast") == true;
		this.sync = $(el).data("sync") == true;
		this.isNestedAccordion = $(el).closest(".j-accordion, .j-infowidget-accordion").length > 0;

		this.accordionChild = this.accordions.find('.j-accordion');
		this.hasAccordionChild = this.accordionChild.length > 0;

		this.resizeID;
		this.init();
	}

	_createClass(Accordion, [{
		key: 'init',
		value: function init() {
			var _this = this;

			this.listners();
			this.accordions.attr('tabindex', 0);
			//this.accordions.css('transition','none');
			this.resizeHeights();
			//open active
			this.accordions.siblings(".active").each(function (e, i) {
				_this.openAccordion($(i));
			});
		}
	}, {
		key: 'checkAutoCollapse',
		value: function checkAutoCollapse(el) {
			if (this.mode == "nocollapse") {
				return $.Deferred().resolve();
			} else {
				var openAccordion = el.siblings(".active");
				if (openAccordion.length > 0) {
					if (this.sync) {
						this.closeAccordion(openAccordion);
						return $.Deferred().resolve();
					} else {
						return this.closeAccordion(openAccordion);
					}
				} else {
					return $.Deferred().resolve();
				}
			}
		}
	}, {
		key: 'openAccordion',
		value: function openAccordion(el) {
			var _this2 = this;

			var accordionToOpen = el;
			this.checkAutoCollapse(accordionToOpen).then(function () {
				accordionToOpen.addClass('active').addClass('opening');
				//accessibility
				accordionToOpen.attr("aria-selected", "true").find(".j-accordion-body").attr("aria-hidden", "false");

				//resize content child
				if (_this2.hasAccordionChild) {
					resizeAccordions(_this2.accordionChild);
				}
				el.height(el.data('closed_height'));

				var _height = accordionToOpen.get(0).scrollHeight + 2;
				if (_height === undefined || _height <= 2) {
					_height = 'auto';
					return;
				}

				if ($('.j-accordion.tabsInfoAccordion').length > 0) {
					el.css({ 'overflow': 'hidden' });
				}
				//update iosslider
				if ($('.chooseSeat__sliderCover').length > 0) $('.chooseSeat__sliderCover').iosSlider('update');

				var $el = $(el);
				var scrollElement = $el.hasClass('bookingBox') || $el.hasClass('millemiglia__accordion');
				var bookingAllOpenedElement = $el.hasClass('bookingUpsell__accordion');
				var scrollElementCheckin = $('.checkin .tabsInfoAccordion').length > 0;
				var firstTime = $el.data('firstTime');
				var velocityOpen = bookingAllOpenedElement && firstTime ? 0 : 300;

				if (scrollElementCheckin) {
					$('.j-accordion').each(function (index, el) {
						$(this).data('firstTime', false);
					});
				}

				var top = $el.closest(".j-accordions").offset().top - $(".mainMenu").outerHeight();
				if (top < $(window).scrollTop()) $("html, body").stop().animate({ scrollTop: top }, 200);

				accordionToOpen.velocity("stop").velocity({
					height: _height
				}, {
					duration: velocityOpen,
					queue: false,
					begin: function begin(el) {
						if ($('.isError').length > 0) removeErrors();
					},
					complete: function complete(el) {
						$el.trigger("accordion.opened").height("auto");
						//do scroll execptionon begin
						if ($('.j-accordion.tabsInfoAccordion').length > 0) {
							$el.css({ 'overflow': 'visible' });
						}
						if (bookingAllOpenedElement) {
							if (firstTime) {} else {
								_this2.doScroll($el);
							}
						} else if (scrollElement) {
							_this2.doScroll($el);
						} else if (scrollElementCheckin && !firstTime) {
							_this2.doScroll($el);
						} else if ($el.closest(".checkin__faqAside").length !== 0) {
							_this2.doScroll($el);
						} else if ($el.closest(".booking__accordionStyle").length !== 0) {
							$el.animateAnchor(-10);
						} else {

							var controlElements = $('.j-accordion.tabsInfoAccordion').length > 0 || $('.j-infowidget-accordion').length > 0 || $('.thankyoupage__accordionGroup').length > 0;
							if (controlElements || firstTime && $el.closest('.thankyoupage').length === 0) {} else {
								_this2.doScroll($el);
							}
						}

						if ($('.checkin__faqAside').length > 0) {
							alignHeight();
						}
						$el.data('firstTime', false);
					}
				});
			});
		}
	}, {
		key: 'closeAccordion',
		value: function closeAccordion(el) {

			var defer = $.Deferred();
			var accordionToClose = el;

			if (accordionToClose !== null) {

				accordionToClose.removeClass('opening').attr("aria-selected", "false");
				accordionToClose.find(".j-accordion-body").attr("aria-hidden", "true");
				var ch = accordionToClose.data('closed_height');

				if (ch == 0) return;

				accordionToClose.velocity("stop").velocity({
					height: ch
				}, {
					duration: 300,
					queue: false,
					begin: function begin() {},
					complete: function complete(el) {
						accordionToClose.removeClass('active');
						defer.resolve();
						$(el).trigger("accordion.closed");
						if ($('.firstPayment').length == 0) {
							$(window).trigger('resize');
						}
					}
				});
			} else {
				defer.resolve();
			}
			return defer.promise();
		}
	}, {
		key: 'resizeHeights',
		value: function resizeHeights() {
			resizeAccordions(this.accordions);
		}
	}, {
		key: 'doScroll',
		value: function doScroll(accordionToOpen) {
			var scrollPos = $(window).scrollTop();
			var accordionHeight = accordionToOpen.get(0).scrollHeight + 2;
			var windowHeight = $(window).height();

			var mm = $(".mainMenu[data-fixedheader='false']");
			var offsetPos = accordionToOpen.offset().top - mm.outerHeight();
			var visibleHeight = Math.abs(accordionToOpen.visibleHeight());

			//extra offset
			if ($('.j-cookieBar').length > 0 && $('.j-cookieBar').is(':visible')) {
				offsetPos -= $('.j-cookieBar').outerHeight();
			}
			if ($('.checkin').length < 1 && $('.j-bookInfoBox').length > 0 && $('.j-bookInfoBox').is(':visible')) {
				offsetPos -= $('.j-bookInfoBox').outerHeight();
			}
			$('html').velocity("scroll", {
				offset: offsetPos
			});
		}
	}, {
		key: 'listners',
		value: function listners() {
			var _this3 = this;

			var header = this.accordions.find('.j-accordion-header');
			header.children().attr('tabindex', -1);
			$(window).unbind("resize.accordion").on('resize.accordion', function (e) {
				if ($(window).width() !== _this3.window_width) {
					_this3.window_width = $(window).width();
					clearTimeout(_this3.resizeID);
					_this3.resizeID = setTimeout(function () {
						_this3.resizeHeights();
					}, 300);
				}
			});
			this.accordions.unbind();
			this.accordions.on('recalculate', function (e) {
				_this3.resizeHeights();
			});
			this.accordions.on('keypress.accordion', function (e) {
				if (e.which == 13) {
					//Enter key pressed
					e.preventDefault();
					var accordion = $(e.currentTarget);
					if (accordion.is(':focus')) {
						if (!accordion.hasClass('opening')) {
							return _this3.openAccordion(accordion);
						} else {
							return _this3.closeAccordion(accordion);
						}
					}
				}
			});
			//fix click inside iframe
			this.accordions.find('.j-accordion-header a').unbind().on('click.accordion', function (e) {
				e.preventDefault();
				e.stopImmediatePropagation();
				$(e.currentTarget).parent().trigger("click.accordion");
				return false;
			});
			this.accordions.find('.js-closeAccordion').unbind().on('click.accordion', function (e) {
				e.preventDefault();
				e.stopImmediatePropagation();
				var accordion = $(e.currentTarget).closest('.j-accordion');
				if (!accordion.hasClass('opening')) {
					_this3.openAccordion(accordion);
				} else {
					if (!_this3.atleast) {
						_this3.closeAccordion(accordion);
						/*$('html').velocity("scroll", {
      	offset: accordion.find('.j-accordion-header').offset().top
      });*/
					}
				}
				return false;
			});
			this.accordions.on('click.accordion', '.j-accordion-header', function (e) {
				e.stopPropagation();
				e.preventDefault();
				var _cur = $(e.currentTarget);
				if (_cur.hasClass('j-accordion-header')) {
					var accordion = $(e.currentTarget).closest('.j-accordion');
					if (!accordion.hasClass('opening')) {
						_this3.openAccordion(accordion);
					} else {
						if (!_this3.atleast) {
							_this3.closeAccordion(accordion);
						}
					}
				}
				return false;
			});
		}
	}]);

	return Accordion;
})();

function initAccordions() {
	var page = $('.j-accordions');
	if (page.length > 0) {
		page.each(function (i, el) {
			$(el).removeData("obj-accordion");
			$(el).data("obj-accordion", new Accordion(el));
		});
	}
}
function resizeAccordions(accordions) {
	accordions.each(function (i, e) {
		var el = $(this);
		var header = el.children().not("script").eq(0);
		//var content = el.children().eq(1);
		var header_h = header.outerHeight();
		var oh = el.height('auto').outerHeight();
		//var ch= !content.is(":visible") ? oh : header_h;
		var ch = header_h;
		el.data('closed_height', ch);
		el.data('opened_height', oh);
		//el.height(el.hasClass('active') ? oh : ch);
		el.height(el.hasClass('active') || ch == 0 ? 'auto' : ch);
	});
}

$(function () {
	initAccordions();
});

"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TabsInfo = (function () {
	function TabsInfo(el) {
		_classCallCheck(this, TabsInfo);

		this.$doc = $(document);
		this.mode = $(el).data("mode");
		this.window_width = $(window).width();
		this.el = $(el);
		this.accordions = $(el).find('.j-infowidget-accordion');
		this.headersDesktop = this.el.find(".j-infowidget-tab");
		this.lastAccordion = null;
		this.currentBreakpoint;
		this.resizeID;
		this.init();
	}

	_createClass(TabsInfo, [{
		key: "init",
		value: function init() {
			this.listners();
			this.accordions.attr('tabindex', 0);
			this.accordions.css('transition', 'none');
			this.resizeHeights();
		}
	}, {
		key: "checkAutoCollapse",
		value: function checkAutoCollapse(el) {
			if (this.currentBreakpoint == "s" || el.is(this.lastAccordion)) {
				return $.Deferred().resolve();
			} else {
				return this.closeAccordion();
			}
		}
	}, {
		key: "detectBreakpoint",
		value: function detectBreakpoint() {
			var wh = $(window).width();
			return br = wh <= 640 ? "s" : wh > 800 ? "l" : "m";
		}
	}, {
		key: "changeBreakpoint",
		value: function changeBreakpoint() {
			//reset
			this.accordions.attr("style", "");
			this.accordions.removeClass("active");
			this.headersDesktop.removeClass("active");
			if (this.currentBreakpoint == "l" || this.currentBreakpoint == "m") {
				this.headersDesktop.eq(0).trigger("click");
			}
		}
	}, {
		key: "openAccordion",
		value: function openAccordion(el) {
			var _this = this;

			this.headersDesktop.removeClass('active');
			this.headersDesktop.eq(el.index()).addClass('active');
			this.checkAutoCollapse(el).then(function () {
				//desktop resize
				el.addClass('active').addClass('opening');
				el.find(".j-accordion").triggerHandler("recalculate");
				var _height = el.get(0).scrollHeight;
				_this.lastAccordion = el;

				resizeTabInfoAccordions(_this.accordions);
				el.height(el.data('closed_height'));

				if (_height === undefined) {
					_height = 'auto';
				}

				if (window.breakpointS > window.windowW && el.attr('data-href') !== undefined && el.attr('data-href') !== "") {
					location.assign(el.attr('data-href'));
				} else {
					var objAnim = _this.currentBreakpoint == "l" ? { opacity: 1 } : { height: _height };
					var duration = _this.currentBreakpoint == "l" ? 200 : 400;
					//if(_height==0) return;
					el.velocity("stop").velocity(objAnim, {
						queue: false,
						duration: duration,
						begin: function begin() {},
						complete: function complete(e) {
							// to resolve the bug, on the mmb infoBox
							el.height("auto");
							if ($('.j-bookInfoBox').is(':visible')) {
								book.resetFixedElement();
								book.updateTopPosition();
							}

							if ($('.isError').length > 0) {
								removeErrors();
							}
						}
					});
				}
			});
		}
	}, {
		key: "closeAccordion",
		value: function closeAccordion(el) {
			var _this2 = this;

			var defer = $.Deferred();
			//desktop
			if (this.currentBreakpoint == "s") this.lastAccordion = el;
			if (this.lastAccordion !== null) {
				this.lastAccordion.removeClass('opening');
				var objAnim = this.currentBreakpoint == "l" ? { opacity: 0, duration: 100 } : { height: this.lastAccordion.data('closed_height') };
				var duration = this.currentBreakpoint == "l" ? 200 : 400;
				this.lastAccordion.velocity("stop").velocity(objAnim, {
					queue: false,
					duration: duration,
					begin: function begin() {/*this.lastAccordion = null;*/},
					complete: function complete(el) {
						_this2.lastAccordion.removeClass('active');
						_this2.lastAccordion = null;
						defer.resolve();
					}
				});
			} else {
				defer.resolve();
			}
			return defer.promise();
		}
	}, {
		key: "resizeHeights",
		value: function resizeHeights() {
			if (this.detectBreakpoint() != this.currentBreakpoint) {
				this.currentBreakpoint = br;
				this.changeBreakpoint();
				if (this.currentBreakpoint == "l") resizeTabInfoAccordions(this.accordions);
			}
		}
	}, {
		key: "listners",
		value: function listners() {
			var _this3 = this;

			var header = this.accordions.find('.j-infowidget-header');
			header.children().attr('tabindex', -1);
			$(window).on('resize.accordion', function (e) {
				if ($(window).width() !== _this3.window_width) {
					_this3.window_width = $(window).width();
					clearTimeout(_this3.resizeID);
					_this3.resizeID = setTimeout(function () {
						_this3.resizeHeights();
					}, 200);
				}
			});
			this.accordions.on('keypress.accordion', function (e) {
				if (e.which == 13) {
					//Enter key pressed
					e.preventDefault();
					var accordion = $(e.currentTarget);
					if (accordion.is(':focus')) {
						if (!accordion.hasClass('opening')) {
							return _this3.openAccordion(accordion);
						} else {
							return _this3.closeAccordion(accordion);
						}
					}
				}
			});
			this.headersDesktop.unbind().on('click.accordion', function (e) {
				var _cur = $(e.currentTarget);
				e.preventDefault();
				_this3.accordions.eq(_cur.index()).find('.j-infowidget-header').trigger("click");
			});
			this.accordions.unbind().on('click.accordion', '.j-infowidget-header', function (e) {
				e.preventDefault();
				var _cur = $(e.currentTarget);
				if (_cur.hasClass('j-infowidget-header')) {
					var accordion = $(e.currentTarget).closest('.j-infowidget-accordion');
					if (!accordion.hasClass('active')) {
						return _this3.openAccordion(accordion);
					} else {
						if (_this3.currentBreakpoint != "l") return _this3.closeAccordion(accordion);
					}
				}
			});
		}
	}]);

	return TabsInfo;
})();

function initTabs() {
	var page = $('.j-infowidget');
	if (page.length) {
		page.each(function (i, el) {
			new TabsInfo(el);
		});
	}
}

function resizeTabInfoAccordions(accordions) {
	accordions.each(function (i, e) {
		var el = $(this);
		var header = el.children().first();
		var ch = header.is(":visible") ? header.height() : 0;
		var oh = el.height('auto').height();
		el.data('closed_height', ch);
		el.data('opened_height', oh);
		//el.height(el.hasClass('active') ? oh : ch);
		el.height(el.hasClass('active') ? 'auto' : ch);
	});
}

$(function () {
	initTabs();
});



function myexpReady() {
	// console.log('myexp ready');

	// disable category select
	myexp_disableCat();

	// show 'new experience' or 'update' panel
	$('.myexp__intro input').on('change', function() {
		// console.log('changed: ' + this.className);

		if ($(this).hasClass('j-myexp-update')) {
			// console.log('update');
			myexp_showUpdate();
		}
		else {
			// console.log('new');
			myexp_showNew();
		}
	});

	// enable 'category' dropdown
	$('.j-myexp-type').on('change', function() {
		// console.log('changed: ' + this.className);

		if ($(this).find('select').val() != '') {
			myexp_enableCat();
		}
		else {
			myexp_disableCat();
		}
	});

	// show 'step2' panel
	$('.j-myexp-category').on('change', function() {
		if ($(this).find('select').val() != '') {
			// console.log('show step 2');
			myexp_showStep(2);
		} else {
			// console.log('hide step 2');
			myexp_hideStep(2);
		}
	});

	// show 'step3' panel
	$('.j-myexp-step2-submit').on('click', function() {
		// console.log('show step 3');
		myexp_showStep(3);
	});

	// show 'step4' panel
	$('.j-myexp-step3-submit').on('click', function() {
		// console.log('show step 4');
		myexp_showStep(4);
	});

	// show 'specific flight' block
	$('.myexp__intro--step4 input').on('change', function() {

		if ($(this).hasClass('j-myexp-specific')) {
			// console.log('specific');
			myexp_showSpecific();
		}
		else {
			// console.log('generic');
			myexp_showGeneric();
		}
	});

	// show 'feedback' panel
	$('.j-myexp-new-submit').on('click', function() {
		// console.log('new experience submit');
		myexp_showFeedback();
	});
	$('.j-myexp-update-submit').on('click', function() {
		// console.log('updated experience submit');
		myexp_showFeedback();
	});
}

function myexp_disableCat() {
	$('.j-myexp-category').addClass('disabled');
	$('.j-myexp-category select').prop('disabled', true);
}

function myexp_enableCat() {
	$('.j-myexp-category').removeClass('disabled');
	$('.j-myexp-category select').prop('disabled', false);
}

function myexp_showNew() {
	$('.myexp__new').addClass('active').animateAnchor(-10, 500);
	$('.myexp__update').removeClass('active');

	$('.myexp__step1 .j-accordion-body').velocity('slideDown', { duration: 500 });
}

function myexp_showUpdate() {
	$('.myexp__new').removeClass('active');
	$('.myexp__update').addClass('active').animateAnchor(-10, 500);

	$('.myexp__step1 .j-accordion-body').velocity('slideUp', { duration: 500 });
}

function myexp_showSpecific() {
	$('.myexp__content--step4').addClass('active');
	// $('.myexp__content--step4').velocity('slideDown', { duration: 1000 });
	$('.myexp__buttonCover--step4').addClass('active');
}

function myexp_showGeneric() {
	$('.myexp__content--step4').removeClass('active');
	// $('.myexp__content--step4').velocity('slideUp', { duration: 1000 });
	$('.myexp__buttonCover--step4').addClass('active');
}

function myexp_showFeedback() {
	$('.myexp__intro').removeClass('active');
	$('.myexp__new').removeClass('active');
	$('.myexp__update').removeClass('active');
	$('.myexp__feedback').addClass('active');
	$('body').animateAnchor(0, 500);
}

function myexp_showStep(step) {
	var dur = (step <= 3) ? 1000 : 500;

	$myexpStep = $('.myexp__step' + step + ' .j-accordion');

	$myexpStep.addClass('active').animateAnchor(-10, 500);
	$myexpStep.find('.j-accordion-body').attr('aria-hidden', false).velocity('slideDown', { duration: dur });
}

function myexp_hideStep(step) {
	var dur = (step <= 3) ? 1000 : 500;

	$myexpStep = $('.myexp__step' + step + ' .j-accordion');

	$myexpStep.removeClass('active');
	$myexpStep.find('.j-accordion-body').attr('aria-hidden', true).velocity('slideUp', { duration: dur });
}





'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

$(function () {
	var MilleMiglia = (function () {
		function MilleMiglia() {
			_classCallCheck(this, MilleMiglia);

			this.$doc = $(document);
			this.accordions = $('.millemiglia__accordion-');
			this.services = $('.j-millemiglia__service');
			this.lastAccordion = null;
			this.resizeID;
			this.serviceFeatures = $('.j-millemiglia__service_features');
			this.init();
		}

		_createClass(MilleMiglia, [{
			key: 'init',
			value: function init() {
				this.listners();
				this.accordions.prop('open', false);
				this.accordions.attr('tabindex', 0);
				this.accordions.css('transition', 'none');
			}
		}, {
			key: 'openAccordion',
			value: function openAccordion(el) {
				var _this = this;

				this.closeAccordion().then(function () {
					el.prop('open', "true");
					el.addClass('active');
					var _parent = el.get(0);
					var _height = _parent.scrollHeight;
					_this.lastAccordion = el;
					// let _childWrap = el.find('.millemiglia__accordion__body');
					// _childWrap.children('input,checkbox,select').eq(0).focus();
					el.data('oheight', el.outerHeight());
					if (_height === undefined) {
						_height = 'auto';
					}
					el.velocity({
						height: _height
					}, {
						queue: false,
						complete: function complete() {
							//el.addClass('active');
						}
					});
				});
			}
		}, {
			key: 'closeAccordion',
			value: function closeAccordion() {
				var _this2 = this;

				var defer = $.Deferred();
				if (this.lastAccordion !== null) {
					this.lastAccordion.velocity({
						height: this.lastAccordion.data('oheight')
					}, {
						queue: false,
						begin: function begin() {
							_this2.lastAccordion = null;
						},
						complete: function complete(el) {
							_this2.accordions.removeClass('active');
							$(el).prop('open', false);
							defer.resolve();
						}
					});
				} else {
					defer.resolve();
				}
				return defer.promise();
			}
		}, {
			key: 'openService',
			value: function openService(el) {
				if (el.length && this.services.length) {
					this.closeService();
					el.addClass('isActive');
					var _feature = el.data('featureid');
					var _featureEL = $('[data-feature="' + _feature + '"]');
					var _cloned = _featureEL.clone();
					_cloned.addClass('cloned-feature');
					if (_featureEL.length) {
						if ($(window).width() <= 800) {
							_cloned.height(0);
							el.after(_cloned);
							_cloned.addClass('isActive');
							_cloned.height(_cloned.get(0).scrollHeight);
							return;
						} else $('.cloned-feature').remove();
						return _featureEL.addClass('isActive');
					}
				}
			}
		}, {
			key: 'closeService',
			value: function closeService() {
				var el = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];

				if (el == null) {
					$('.cloned-feature').each(function (index, cur) {
						cur = $(cur);
						cur.height(0);
						setTimeout(function () {
							cur.remove();
						}, 450);
					});
					this.services.removeClass('isActive');
					this.serviceFeatures.removeClass('isActive');
				} else {
					$('.cloned-feature').remove();
					el.removeClass('isActive');
					this.serviceFeatures.removeClass('isActive');
				}
			}
		}, {
			key: 'resizeHeights',
			value: function resizeHeights() {
				this.accordions.each(function (i, e) {
					var el = $(this);
					el.data('oheight', el.children().first().outerHeight());
					$(this).height('auto').height($(this).height());
				});
			}
		}, {
			key: 'listners',
			value: function listners() {
				var _this3 = this;

				this.accordions.prop('open', true);
				/*this.$doc.on('focus.millemiglia','.millemiglia__accordion',(e) => {
    	console.log(e.type);
    	e.preventDefault();
    	//let accordion  = $(e.currentTarget);
    	//let _this = this;
    	//if(!accordion.hasClass('active')) {
    		//console.log("---> close");
    	//}
    });*/
				$(window).on('resize.millemiglia', function (e) {
					clearTimeout(_this3.resizeID);
					_this3.resizeID = setTimeout(function () {
						_this3.resizeHeights();
					}, 200);
				});
				this.$doc.on('keypress.millemiglia', '.millemiglia__accordion', function (e) {
					if (e.which == 13) {
						//Enter key pressed
						e.preventDefault();
						var accordion = $(e.currentTarget);
						if (accordion.is(':focus')) {
							if (!accordion.hasClass('active')) {
								return _this3.openAccordion(accordion);
							} else {
								return _this3.closeAccordion(accordion);
							}
						}
					}
					//	return this.openAccordion(accordion);
				});
				this.$doc.on('click.millemiglia', '.millemiglia__accordion__header,.j-millemiglia__service,.millemiglia__service__close', function (e) {
					var _cur = $(e.currentTarget);
					if (_cur.hasClass('millemiglia__accordion__header')) {
						e.preventDefault();
						var accordion = $(e.currentTarget).closest('.millemiglia__accordion');
						if (!accordion.hasClass('active')) {
							return _this3.openAccordion(accordion);
						} else {
							return _this3.closeAccordion();
						}
					}
					if (_cur.hasClass('j-millemiglia__service')) {
						e.preventDefault();
						if (_cur.hasClass('isActive')) return _this3.closeService();else return _this3.openService(_cur);
					}
					if (_cur.hasClass('millemiglia__service__close')) {
						e.preventDefault();
						return _this3.closeService();
					}
				});
			}
		}]);

		return MilleMiglia;
	})();

	var page = $('.millemiglia');
	if (page.length) {
		var millemigliaRef = new MilleMiglia();
	}
});

'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

$(function () {
	var RespondImage = (function () {
		function RespondImage() {
			_classCallCheck(this, RespondImage);

			this.win = $(window);

			this.loaded = {};

			this.settings = {
				sizes: {
					'small': 0,
					'medium': 641,
					'large': 1024
				},
				wrapClass: '.respond-image',
				imageClass: {
					'small': 'respond-image__small',
					'medium': 'respond-image__medium',
					'large': 'respond-image__large'
				}
			};

			this.init();
		}

		_createClass(RespondImage, [{
			key: 'init',
			value: function init() {
				this.setViewPortListners();
				this.win.trigger('resize');
			}
		}, {
			key: 'setViewPortListners',
			value: function setViewPortListners() {
				var _this = this;

				var _sizes = Object.keys(this.settings.sizes);

				this.win.on("resize", function (e) {

					var ww = _this.win.width();

					_sizes.forEach(function (size, i) {

						var value = _this.settings.sizes[size];
						var nextSize = 0;
						var nextValue = 0;

						if (i + 1 << _sizes.length) {

							nextSize = _sizes[i + 1];
							nextValue = _this.settings.sizes[nextSize];

							if (i === 0) value = 0;

							if (typeof nextValue == 'undefined') nextValue = ww;

							if (ww >= value && ww <= nextValue) {
								return _this.loadImages(size);
							}
						}
					});
				});
			}
		}, {
			key: 'loadImages',
			value: function loadImages(size) {
				var _this2 = this;

				var _images = $(this.settings.wrapClass);

				if (_images.length) {

					Array.prototype.forEach.call(_images, function (image) {

						var _cur = $(image);
						var _isbg = false;

						if (_cur.data('setbg') !== undefined) _isbg = true;

						if (!_cur.find('.' + _this2.settings.imageClass[size]).length) {

							var _data = _cur.data();

							if (_data[size] !== undefined) {
								if (!_isbg) {
									var img = new Image();
									img.onload = function () {
										_cur.removeClass('isBusy');
									};
									img.src = _data[size];
									img.className = _this2.settings.imageClass[size];
									var alt = $(_cur.find("noscript").text()).attr("alt");
                                    img.alt = alt ? alt : "";
									_cur.append(img);
									_cur.addClass('isBusy');
								} else {
									_cur.css('backgroundImage', 'url("' + _data[size] + '")');
								}
							}
						}
					});
				}
			}
		}]);

		return RespondImage;
	})();

	window.RespondImageRef = new RespondImage();
});


/**
 * HTML element names for the search form, the spellchecking suggestion, and the
 * cluster suggestions. The search form must have the following input elements:
 * "q" (for search box), "site", "client".
 * @type {string}
 */
var ss_form_element = 'suggestion_form'; // search form

/**
 * Name of search suggestion drop down.
 * @type {string}
 */
var ss_popup_element = 'search_suggest'; // search suggestion drop-down

/**
 * Types of suggestions to include.  Just one options now, but reserving the
 * code for more types
 *   g - suggest server
 * Array sequence determines how different suggestion types are shown.
 * Empty array would effectively turn off suggestions.
 * @type {object}
 */
var ss_seq = [ 'g' ];

/**
 * Suggestion type name to display when there is only one suggestion.
 * @type {string}
 */
var ss_g_one_name_to_display =
    "Suggestion";

/**
 * Suggestion type name to display when there are more than one suggestions.
 * @type {string}
 */
var ss_g_more_names_to_display =
    "Suggestions";

/**
 * The max suggestions to display for different suggestion types.
 * No-positive values are equivalent to unlimited.
 * For key matches, -1 means using GSA default (not tagging numgm parameter),
 * 0 means unlimited.
 * Be aware that GSA has a published max limit of 10 for key matches.
 * @type {number}
 */
var ss_g_max_to_display = 10;

/**
 * The max suggestions to display for all suggestion types.
 * No-positive values are equivalent to unlimited.
 * @type {number}
 */
var ss_max_to_display = 12;

/**
 * Idling interval for fast typers.
 * @type {number}
 */
var ss_wait_millisec = 300;

/**
 * Delay time to avoid contention when drawing the suggestion box by various
 * parallel processes.
 * @type {number}
 */
var ss_delay_millisec = 30;

/**
 * Host name or IP address of GSA.
 * Null value can be used if the JS code loads from the GSA.
 * For local test, use null if there is a <base> tag pointing to the GSA,
 * otherwise use the full GSA host name
 * @type {string}
 */
var ss_gsa_host = null;

/**
 * Constant that represents legacy output format.
 * @type {string}
 */
var SS_OUTPUT_FORMAT_LEGACY = 'legacy';

/**
 * Constant that represents OpenSearch output format.
 * @type {string}
 */
var SS_OUTPUT_FORMAT_OPEN_SEARCH = 'os';

/**
 * Constant that represents rich output format.
 * @type {string}
 */
var SS_OUTPUT_FORMAT_RICH = 'rich';

/**
 * What suggest request API to use.
 *   legacy - use current protocol in 6.0
 *            Request: /suggest?token=<query>&max_matches=<num>&use_similar=0
 *            Response: [ "<term 1>", "<term 2>", ..., "<term n>" ]
 *                   or
 *                      [] (if no result)
 *   os -     use OpenSearch protocol
 *            Request: /suggest?q=<query>&max=<num>&site=<collection>&client=<frontend>&access=p&format=os
 *            Response: [
 *                        "<query>",
 *                        [ "<term 1>", "<term 2>", ... "<term n>" ],
 *                        [ "<content 1>", "<content 2>", ..., "<content n>" ],
 *                        [ "<url 1>", "<url 2>", ..., "<url n>" ]
 *                      ] (where the last two elements content and url are optional)
 *                   or
 *                      [ <query>, [] ] (if no result)
 *   rich -   use rich protocol from search-as-you-type
 *            Request: /suggest?q=<query>&max=<num>&site=<collection>&client=<frontend>&access=p&format=rich
 *            Response: {
 *                        "query": "<query>",
 *                        "results": [
 *                          { "name": "<term 1>", "type": "suggest", "content": "<content 1>", "style": "<style 1>", "moreDetailsUrl": "<url 1>" },
 *                          { "name": "<term 2>", "type": "suggest", "content": "<content 2>", "style": "<style 2>", "moreDetailsUrl": "<url 2>" },
 *                          ...,
 *                          { "name": "<term n>", "type": "suggest", "content": "<content n>", "style": "<style n>", "moreDetailsUrl": "<url n>" }
 *                        ]
 *                      } (where type, content, style, moreDetailsUrl are optional)
 *                   or
 *                      { "query": <query>, "results": [] } (if no result)
 * If unspecified or null, using legacy protocol.
 * @type {string}
 */
var ss_protocol = SS_OUTPUT_FORMAT_RICH;

/**
 * Whether to allow non-query suggestion items.
 * Setting it to false can bring results from "os" and "rich" responses into
 * backward compatible with "legacy".
 * @type {boolean}
 */
var ss_allow_non_query = true;

/**
 * Default title text when the non-query suggestion item does not have a useful
 * title.
 * The default display text should be internalionalized.
 * @type {string}
 */
var ss_non_query_empty_title =
    "No Title";

/**
 * Whether debugging is allowed.  If so, toggle with F2 key.
 * @type {boolean}
 */
var ss_allow_debug = false;

$(function(){

	// $('.j-print').fastClick(function(e){
	// 	e.preventDefault();
	// 	var printClass = $(this).attr("data-printClass")
	// 	console.log('printClass')
	// 	$('body').addClass(printClass);
	// 	window.print();
	// 	$('body').removeClass(printClass);
	// });

	$('.j-printCard').fastClick(function(e){
		e.preventDefault();
		$('body').addClass('printOnlyCard');
		window.print();
		$('body').removeClass('printOnlyCard');
	});

	$('.j-printMessages').fastClick(function(e){
		e.preventDefault();
		$('body').addClass('printMessages');
		window.print();
		$('body').removeClass('printMessages');
	});

	$('.j-printEstrattoConto').fastClick(function(e){
		e.preventDefault();
		$('body').addClass('printEstrattoConto');
		window.print();
		$('body').removeClass('printEstrattoConto');
	});
/*
	$('.j-printReceipt').fastClick(function(e){
		e.preventDefault();
		$('body').addClass('printReceipt');
		window.print();
		$('body').removeClass('printReceipt');
	});
*/

	$('[data-printer]').fastClick(function(e){
		e.preventDefault();
		var className = $(this).attr('data-printer');
		$('body').addClass(className);
		window.print();
		$('body').removeClass(className);
	});
});


var cssTransitionEnd = "webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend";
var cookieBar_height = 0;
var header_status;
var menuTop = parseInt($(".menu.mainMenu").css('top'));


$(function(){

	// skipLinkTrigger
	skipLinkTrigger();

	//FastClick.attach(document.body);


	// var hammertime = new Hammer(document.body,{
	// 		preventDefault:false,
	// 		touchAction: 'auto'
	// });
	// hammertime.get('pan').set({ enable: false });
	// hammertime.get('press').set({ enable: false });
	// hammertime.get('rotate').set({ enable: false });
	// hammertime.get('tap').set({ enable: false });
	// hammertime.get('swipe').set({ enable: false });
	// hammertime.get('pinch').set({ enable: true });

	// hammertime.on('pinch',function(e){
	// 	if(document.body.clientWidth){
	// 		if(window.innerWidth < document.body.clientWidth){
	// 			$('body').addClass('isPinched');
	// 		}
	// 		else{
	// 			$('body').removeClass('isPinched');
	// 		}
	// 	}
	// });


	$('#departureDate').on('change', function(event) {
		if (!$('#returnDate').is(':disabled')){
			if ( $('#returnDate').val() !== "" ){
				$('.flightFinder, .innerFlightFinder').removeClass('ff-stage4').addClass('ff-stage5');
			} else {
				$('#returnDate').focus();
			}
		} else {
			$('.flightFinder, .innerFlightFinder').removeClass('ff-stage4').addClass('ff-stage5');
		}
	});

	$('#returnDate').on('change', function(event) {
		if ( $('#departureDate').val() !== "" ){
			$('.flightFinder, .innerFlightFinder').removeClass('ff-stage4').addClass('ff-stage5');
		} else {
			$('#departureDate').focus();
		}
	});


	$('.j-responsiveTable').responsiveTable({
		maxWidth: 640
	});

	$('.j-responsiveTable.multiTratta').responsiveTable({
		maxWidth: 800
	});

	$('#arrivalInput').on('focus',function(){
		$('.flightFinder, .innerFlightFinder').removeClass('ff-stage1').addClass('ff-stage4');
		$('.heroCarousel').iosSlider('autoSlidePause');
	});

	//touchstart

	// $("#andata, #ritorno").on("focus mousedown",function(){
	// 	datePickerController.show($(this).attr("id"));
	// }).on("blur",function(){
	//  	datePickerController.hide($(this).attr("id"));
	// });

	listSorter( $('.specialOffers__list:visible'), 'name' );

	$('.j-sorter').on('change', function(event) {
		var selector = $('.specialOffers__list:visible');
		var sortType = $(this).find('input').val();

		listSorter( selector , sortType );
	});

	$('.j-messageSorter').fastClick(function(e){
		e.preventDefault();
		var type = $(this).data('sorter')
		messageSorter(type);
		$('.j-messageSorter').removeClass('isActive');
		$(this).addClass('isActive');

	});

	$('.j-openFooter').on('click',function(event) {
		$(this).toggleClass('isActive');
		$('.footer__row.first').toggleClass('isActive');
	});


	$('.j-openAccordionMobile').on('click',function(){
		var _this = $(this),
			isToggle = _this.hasClass('isActive');
		$('.j-openAccordionMobile').removeClass('isActive');
		if(!isToggle){
			_this.addClass('isActive');
		}
	});

	$('.j-openFooterSections').on('click',function(){
		var _this = $(this),
			isToggle = _this.closest('.mobileExpendable').hasClass('isActive');

		$('.mobileExpendable').removeClass('isActive');
		if(!isToggle){
			_this.closest('.mobileExpendable').addClass('isActive');
		}
	});


	$('.j-flightFinder__containerClose').on('click',function(){
		$('.heroCarousel').iosSlider('autoSlidePlay');
		$('.flightFinder, .innerFlightFinder').removeClass('ff-stage2 ff-stage3 ff-stage4 ff-stage5').addClass('ff-stage1');
		$('#arrivalInput, #andata, #ritorno').val('');
		$('.j-numberSelector-input').val(0)
		$('.adulti .j-numberSelector-input').val(1)

	});

	$('input.j-typeOfFlight').on('change', function(event) {
		var _val = $(this).val();
		if ( _val === 'a' ){
			$('#ritorno').attr('disabled','disabled').attr('aria-disabled','true');
			$('#returnDate').attr('disabled','disabled').attr('aria-disabled','true').parent().velocity({opacity: .3});
			if ( $('#andata').val().length > 0 ){
				$('.flightFinder, .innerFlightFinder').removeClass('ff-stage4').addClass('ff-stage5');
			}
		} else {
			$('#ritorno').removeAttr('disabled').removeAttr('aria-disabled');
			$('#returnDate').removeAttr('disabled').removeAttr('aria-disabled').parent().velocity({opacity: 1});
		}
		highlightPeriod();
		showTheYoungPassenger();
	});

	//checkcookie
	if($('.j-cookieBar').length > 0 ) cookieController();

	stickyHeaderNavigation();

	numberSelector();
	$(window).on('load',function(){
		fixPositions();

	});


	// overlay logic
	initLightBox();


	$('.j-accordionHead').on('click', function(e) {
		var selected = $(this).data('accordion');
		$('.j-accordionHead[data-accordion="'+selected+'"], .j-accordionContainer[data-accordion="'+selected+'"]').toggleClass('isActive');
	});

	$('.j-sliderDefault').iosSlider({
		desktopClickDrag: true,
		snapToChildren: true,
		autoSlide: true,
		autoSlideTimer: 8000,
		infiniteSlider: true
	});

	// create indicators for carosel
	if($('.heroCarousel__item').length > 1) {

		$('.heroCarousel').append('<ul class="heroCarousel__indicators"></ul>')
		for (var i = 0; i < $('.heroCarousel__item').length; i++) {
			$('.heroCarousel__indicators').append('<li class="heroCarousel__indicatorsItem"></li>');
		};

	} else {
		$('.heroCarousel__next, .heroCarousel__prev').hide();
	}
	$('.heroCarousel').iosSlider({
		desktopClickDrag: true,
		snapToChildren: true,
		autoSlide: true,
		autoSlideTimer: 8000,
		infiniteSlider: true,
		keyboardControls: true,
		navSlideSelector: '.heroCarousel__indicatorsItem',
		navNextSelector: $('.heroCarousel__next'),
	    navPrevSelector: $('.heroCarousel__prev'),
		onSliderLoaded: heroOnSliderLoaded,
		onSlideChange: heroSlideChange,
		onSlideComplete: heroOnSlideComplete
		// onSliderLoaded:
		// onSliderUpdate:
		// onSliderResize:
		// onSlideComplete:
		// onSlideStart
	});
	function heroOnSliderLoaded(args){
		// $('.heroCarousel__image').each(function(){
		// 	var url = $(this).attr('src');
		// 	$(this).closest('.heroCarousel__item').css({
		// 		'background-image': 'url('+ url +')'
		// 	});
		// 	$(this).hide();

		// });
		$('.heroCarousel__indicatorsItem').eq(0).addClass('active');
	}
	function heroSlideChange(args){
		var indicators = $('.heroCarousel__indicatorsItem');
		indicators.removeClass('active');
		indicators.eq(args.currentSlideNumber - 1).addClass('active');
	}
	function heroOnSlideComplete(args){
		$('.next, .prev').removeClass('unselectable');

		if(args.currentSlideNumber == 1) {
			$('.heroCarousel__prev').addClass('unselectable');
		} else if(args.currentSliderOffset == args.data.sliderMax) {
			$('.heroCarousel__next').addClass('unselectable');
		}
	}

	$('.j-tabs ul li a').on('click', function(e) {
		e.preventDefault();
		var _this = $(this),
			selected = 0;

		$('.j-tabs ul li, .j-tabs ul li div').removeClass('tabActive');
		_this.parent().addClass('tabActive');
		selected = _this.data('tab');
		$('.j-tabsContainer').removeClass('tabActive');
		$('.j-tabsContainer[data-tab="'+selected+'"]').addClass('tabActive');
		return false;
	});


	$('.j-openOverlay').on('click',function(){
		var open = $(this).data('overlay');
		$('.overlay__container[data-overlay="'+open+'"]').addClass('isActive');
		$('body').addClass('overlayActive');
	});

	$('.j-closePopup').on('click',function(){
		$('body, .overlayBg').removeClass('overlayActive');
		$(this).closest('.overlay__container').removeClass('isActive');
	});


	$('#departureInput, #arrivalInput').on('keyup focus',function(e){
		var
			$this = $(this),
			$next = $this.next(),
			nextOpen = $next.data("open") || false,
			$suggestions = $next.find(".autocomplete-suggestion"),
			$selectedSuggestion = $suggestions.filter(".autocomplete-selected"),
			n;
		///var
		if($this.val().length < 1){
			if(!nextOpen)
				setTimeout(function() {
					$selectedSuggestion.removeClass("autocomplete-selected")
					$suggestions.first().addClass("autocomplete-selected");
					$next.stop().fadeIn().data("open", true);
				}, 100);
			if(e.type === "keyup")
				switch(e.keyCode) {
					case 13: // cr
						$selectedSuggestion.trigger("click");
						if($this.is("#departureInput")) {
							$("#arrivalInput").focus();
							setTimeout(function() {$this.parent().find(".autocomplete-suggestions").hide()},100);
						} else {
							$this.blur();
						}
						e.preventDefault();
						break;
					case 38: // up
						n = $suggestions.index($selectedSuggestion) - 1;
						if(n < 0)
							n = $suggestions.length - 1;
						$selectedSuggestion.removeClass("autocomplete-selected");
						$suggestions.eq(n).addClass("autocomplete-selected");
						e.preventDefault();
						break;
					case 40: // down
						n = $suggestions.index($selectedSuggestion) + 1;
						if(n >= $suggestions.length)
							n = 0;
						$selectedSuggestion.removeClass("autocomplete-selected");
						$suggestions.eq(n).addClass("autocomplete-selected");
						e.preventDefault();
				}
		} else {
			if(nextOpen) {
				$next.stop().fadeOut().data("open", false);
			}
		}

	}).on("blur",function(){
		$(this).next().data("open", false);
		$('.flightFinder__suggest').fadeOut();
		showTheYoungPassenger();
	});

	//$('.j-switchDestination').on('click',function(){
	//$('.j-switchDestination').on('click',function(){

	$('.j-switchDestination').fastClick(function() {
		var dep = $('#departureInput').val(),
			arr = $('#arrivalInput').val(),
			newArr = '',
			newDep = ''

			dep_p_1 = dep.slice(0, dep.length-3),
			dep_p_2 = dep.slice(dep.length-3, dep.length),
		 	selectorDeparture = $('.customInput--flightFinder.departure'),

			arr_p_1 = arr.slice(0, arr.length-3),
			arr_p_2 = arr.slice(arr.length-3, arr.length),
		 	selectorArrival = $('.customInput--flightFinder.arrival');


		selectorArrival.find('.j-countryCode').val(dep_p_2);
		selectorArrival.find('.apt').text(dep_p_1);
		selectorArrival.find('.city').text(dep_p_2);
		newArr = dep_p_1.trim()+' '+dep_p_2.trim();
		$('#arrivalInput').val( newArr.trim() );

		selectorDeparture.find('.j-countryCode').val(arr_p_2);
		selectorDeparture.find('.apt').text(arr_p_1);
		selectorDeparture.find('.city').text(arr_p_2);
		newDep = arr_p_1+' '+arr_p_2;
		$('#departureInput').val( newDep.trim() );

		showTheYoungPassenger();
	});

	// CR 12928
	$('#departureInput, #arrivalInput').on('focusin', function(){
		$(this).select();
	});

	//$('.flightFinder__suggest .autocomplete-suggestion')
	$('.flightFinder__suggest .autocomplete-suggestion').click(function(e) {
		e.preventDefault();
		var city = $(this).find('.sugg_city_name').text();
		var apt = $(this).find('.sugg_airportCode').text().replace('(','').replace(')','');

		var whereTo = $(this).closest('.customInput--flightFinder');
		whereTo.find('.city').text(apt);
		whereTo.find('.apt').text(city);
		whereTo.find('.j-autoCompleteInput').val(city + ' ' + apt);
		whereTo.find('.j-countryCode').val(apt);

		if ( whereTo.hasClass('arrival') ){
			$('input.j-typeOfFlight').eq(0).focus();
			datePickerController.show('andata');
		}
	});

	// if (!Modernizr.details) {

	// 	$(document).on('click', 'summary', function(event) {
	// 		var $details, $summary;
	// 		$summary = $(this);
	// 		$details = $summary.parent();
	// 		if ($details.attr('open')) {
	// 		  $details.removeAttr('open');
	// 		} else {
	// 		  $details.attr('open', 'open');
	// 		}
	// 	});
	// }

	// adding info circle icon to the custom fields

	$('.flightSel__infoText').each(function(){
		var selector = $(this).find('p').eq(0);
		if ( selector.length > 0 ){
			selector.prepend('<span class="i-infoCircle"></span>');
		}
	});


	$('.j-showHideToggle').fastClick(function(e){
		e.preventDefault()
		var cont = $(this).closest('.j-showHide');
		var detail = cont.find('.j-showHideDetail');
		$('.j-showHide').removeClass('opened');
		$('.j-showHideDetail').velocity('slideUp');

		var isOpen = detail.is(':visible');

		detail.velocity( isOpen ? 'slideUp' : 'slideDown', {
			complete:function(){
				if(!isOpen) {
					cont.addClass('opened');
				}
			}
		});
	});


});

$(window).resize(function(){
	var timeOuter;

	clearTimeout(timeOuter);
	timeOuter = setTimeout(function(){

		destinationCarousel();

		dropDownInit();

		transferFlightCartFix();
	}, 50);


	dateMatrixOnResize();
	// NO SUPPORT ANDROID
	$(document).on("gestureend", function onPinch(event) {
		var scale = event.originalEvent.scale;
		if( scale > 1 ){
			$('body').addClass('isPinched');
		} else {
			$('body').removeClass('isPinched');
		}
	});
	if ($('html').hasClass('android')){
		setTimeout(function(){
			$('.heroCarousel').iosSlider('update');
		},300);
	}

});

function SetCookie(e, t, o) {
    if (o) {
        var n = new Date;
        n.setTime(n.getTime() + 24 * o * 60 * 60 * 1e3);
        var r = ";expires=" + n.toGMTString()
    } else var r = "";
    document.cookie = e + "=" + String(t+"") + r + ";path=/";
}

function GetCookie(e) {
    for (var t = e + "=", o = document.cookie.split(";"), n = 0; n < o.length; n++) {
        for (var r = o[n];
            " " == r.charAt(0);) r = r.substring(1, r.length);
        if (0 == r.indexOf(t)) return (r.substring(t.length, r.length) === "true");
    }
    return null
}


function stickyHeaderNavigation(anim) {
	if ( !$('[data-fixedheader]').data('fixedheader') ) {

		var scrollTop = 0;
		var limit = 40;
		var whereToScroll = $(window);
		cookieBar_height = 0;
		var cb = $('.j-cookieBar').is(":visible");
		if(cb) {
			cookieBar_height = $('.j-cookieBar').outerHeight();
		}

		$('.mainWrapper').velocity({
			"margin-top": cookieBar_height
		}, {duration: (anim)?250:0});
		if ( $('body').hasClass('hasAlert') ){
			limit = $('.menu.mainMenu').offset().top;
		}

		whereToScroll.unbind("scroll.cookie").bind("scroll.cookie", function (event, animated) {
			scrollTop = whereToScroll.scrollTop();
			var userMenuIsActive = $('body').hasClass('userMenuIsActive');
			if(0 > scrollTop) scrollTop = 0;
			var temp_status = (scrollTop < limit) ? "static" : "header";

			removeTooltip(scrollTop);

			if(temp_status != header_status) {
				if ( temp_status=="static" ) {
					// $('.menu.mainMenu').removeClass('posFixed').css({'top': 'auto'});
					$('.menu.mainMenu').removeClass('posFixed');
					(animated) ? $('.menu.mainMenu').velocity({'top': 41}, {duration: 250}) : $('.menu.mainMenu').css({'top': 41})  ;
				} else {
					$('.menu.mainMenu').addClass('posFixed');
					(animated) ? $('.menu.mainMenu').velocity({'top': cookieBar_height}, {duration: 250}) : $('.menu.mainMenu').css({'top': cookieBar_height});
					}
				header_status = temp_status;
			}

		});
		whereToScroll.trigger('scroll', anim);
	} else {
		if($('.j-cookieBar').is(':visible')){
			$('.j-cookieBar').css("position", "relative");
			$('.j-cookieBar').addClass('notFixed');
			_top = menuTop + $('.j-cookieBar').outerHeight()
			$('.menu.mainMenu').css({'top':_top+'px'});
		}else{
			if(parseInt($('.menu.mainMenu').css('top')) !== menuTop){
				$('.menu.mainMenu').css({'top':menuTop+'px'});
			}
		}
	}
}
//cookie
/***********************************/
function cookieController() {
  $('.j-cookieBar').hide();
  if ( !GetCookie('alitaliaCookieConfirmed') ){
  //if ( true ){
    COOKIE_CONFIRMED=false;
    $('.j-cookieBar').show();
    stickyHeaderNavigation();
    $('.j-closeCookie').bind("click", function(e){
    	e.preventDefault();
			setCookieHandler();
    });
	$(window).bind("resize.cookie", function() {
		if($('.j-cookieBar').outerHeight() != cookieBar_height) {
			header_status = undefined;
			stickyHeaderNavigation();
		}
	});
  }
}
function setCookieHandler() {
  SetCookie('alitaliaCookieConfirmed', true, 365);
  COOKIE_CONFIRMED=true;
  $(window).unbind("resize.cookie");

  $('.j-cookieBar').fadeOut(200, function() {
  	header_status = undefined;
  	$(this).remove();
  	stickyHeaderNavigation(true);
  	if($('.booking').length > 0 || $('.bookingAward').length > 0 || $('.manageBooking').length > 0 || $('.checkin').length > 0) {
  		book.resetFixedElement();
		book.updateTopPosition();
	}

	$(window).triggerHandler('scroll');
  });
}

function numberSelector(){
	$('.j-numberSelector-plus').on('click',function(){
		var _this = $(this),
			_cont = _this.closest('.j-numberSelector'),
			_input = _cont.find('.j-numberSelector-input'),
			// _contNumber = _cont.find('.j-numberSelector-container'),
			getNumber = parseInt(_input.val()),
			setNumber;

			setNumber = getNumber + 1
			if (setNumber > 10){
				setNumber = 10
			}
			// _contNumber.text(setNumber);
			_input.val(setNumber);
	});
	$('.j-numberSelector-minus').on('click',function(){
		var _this = $(this),
			_cont = _this.closest('.j-numberSelector'),
			_input = _cont.find('.j-numberSelector-input'),
			// _contNumber = _cont.find('.j-numberSelector-container'),
			getNumber = parseInt(_input.val()),
			setNumber;

			setNumber = getNumber - 1
			if (setNumber < 0){
				setNumber = 0
			}
			// _contNumber.text(setNumber);
			_input.val(setNumber);
	});
}

function listSorter(selector, sort){
	var mylist = selector;
	var listitems = mylist.children('li').get();
	listitems.sort(function(a, b) {
		if (sort === 'name'){
			var compA = $(a).data(sort).toUpperCase();
			var compB = $(b).data(sort).toUpperCase();
		} else {
			var compA = parseInt($(a).data(sort));
			var compB = parseInt($(b).data(sort));
		}
		return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
	});
	$.each(listitems, function(idx, itm) {
		mylist.append(itm);
	});
}

function messageSorter(sort){
	var mylist = $('.millemiglia__acordions--messages');
	var listitems = mylist.children('.millemiglia__accordion').get();
	listitems.sort(function(a, b) {
		if (sort === 'name'){
			var compA = $(a).data('title').toUpperCase();
			var compB = $(b).data('title').toUpperCase();
		} else if (sort === 'number'){
			var compA = parseInt($(a).data('date'));
			var compB = parseInt($(b).data('date'));
		}
		return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
	});
	$.each(listitems, function(idx, itm) {
		mylist.append(itm);
	});
}

function destinationCarousel(){

	if( $('.destinationCarousel__listContainer').length === 0 ){
		return
	}
	// this resets carousel
	$('.destinationCarousel__listContainer').iosSlider('destroy');
	$('.destinationCarousel__indicators').remove();


	if( 641 > $(window).width() ){
		if ( $('.destinationItem__imageCover').length === 0 ){
			$('.destinationItem__image').wrap(function() {
				return '<div class="destinationItem__imageCover"></div>';
				// style="background-image : url('+$(this).attr('src') +')"
			});
		}
		var imgH = $('.destinationItem__imageCover').outerHeight();
		var contH = $('.destinationItem__imageCover').siblings('.verticalAlignBottom').height();
		var isCreated = $('.destinationCarousel__indicatorsItem').length;

		$('.destinationCarousel__list, .destinationCarousel__listContainer').height(imgH + contH);

		if( isCreated === 0 ){
			$('.destinationCarousel__listContainer').append('<ul class="destinationCarousel__indicators"></ul>')
			for (var i = 0; i < $('.destinationItem').length; i++) {
				$('.destinationCarousel__indicators').append('<li class="destinationCarousel__indicatorsItem"><span class="i-dot"></span></li>')
			};

		}
		$('.destinationCarousel__listContainer').iosSlider('destroy');
		if( $('.destinationCarousel__listContainer').attr('style') !== '' ){

			$('.destinationCarousel__listContainer').iosSlider({
				desktopClickDrag: true,
				snapToChildren: true,
				autoSlide: true,
				autoSlideTimer: 8000,
				infiniteSlider: true,
				navSlideSelector: '.destinationCarousel__indicatorsItem',
				onSliderLoaded: destinationCarouselLoaded,
				onSlideChange: destinationCarouselSlideChange,
				onSlideComplete: destinationCarouselSlideComplete
			});
		}
	} else {

		$('.destinationCarousel__listContainer').iosSlider('destroy');
		$('.destinationCarousel__indicatorsItem').removeClass('active')

	}

}
function destinationCarouselLoaded(args){
	$('.destinationCarousel__indicatorsItem').eq(0).addClass('active');
}
function destinationCarouselSlideChange(args){
	var indicators = $('.destinationCarousel__indicatorsItem');
	indicators.removeClass('active');
	indicators.eq(args.currentSlideNumber - 1).addClass('active');
}
function destinationCarouselSlideComplete(args){
	// $('.next, .prev').removeClass('unselectable');
	// if(args.currentSlideNumber == 1) {
	// 	$('.heroCarousel__prev').addClass('unselectable');
	// } else if(args.currentSliderOffset == args.data.sliderMax) {
	// 	$('.heroCarousel__next').addClass('unselectable');
	// }
}

function dateMatrixOnResize(){
	if( !$('.dateMatrix__caption').length > 0){
		return;
	}
	// console.log('dateMatrix__caption');

	if($(window).width() > 641 ){
		$('.dateMatrix__caption').attr({colspan: 2});
	} else {
		$('.dateMatrix__caption').attr({colspan: 1});
	}
}

function transferFlightCartFix(){
	if ( window.windowW > window.breakpointL && $('.transferFlights__container').length > 0) {
		var hCart = $('.transferFlights__container').height();
		$('.bookInfoBoxBasket--transfer').height(hCart - 10);
	} else {
		$('.bookInfoBoxBasket--transfer').attr('style','');
	}
}



function ajaxLoadIframeLogic(link) {
	var e = '<iframe border="0" src="'+link+'"></iframe>';
	var overlayItems = '<div class="overlay__bg j-overlayBg"></div><div class="customOverlay overlay__container j-overlayContainer"><div class="overlay__closeCont j-overlayCloseCont"><a class="customOverlay__close j-overlayClose" href="#">'+pageSettings.overlayClose+' <span class="customOverlay__closeIcon">&times;</span></a></div><div class="customOverlay__main">'+e+'</div></div>'

	$('body').append(overlayItems).addClass('overlayActive');
	$('.j-overlayBg, .j-overlayContainer, .j-overlayCloseCont, .j-overlayClose').velocity('transition.fadeIn', {
		complete: function(){

			$('.j-overlayClose').css('display','inline-block');

			$("iframe").contents().find(".closePopup").on('click',function(event){
				event.preventDefault();
				$('.j-overlayBg, .j-overlayContainer, .j-overlayClose').velocity('transition.fadeOut' , {
					complete: function(){
						$(this).remove();
						$('body').removeClass('overlayActive');
					}
				});
			});

			$('.j-overlayClose, .j-overlayBg').on('click',function(event){
				event.preventDefault();
				$('.j-overlayBg, .j-overlayContainer, .j-overlayClose').velocity('transition.fadeOut' , {
					complete: function(){
						$(this).remove();
						$('body').removeClass('overlayActive');
					}
				});
			});
			setTimeout(function(){
				$('.j-overlayClose').focus();
			},200);

		}
	});

}

function inputOnlyNumber() {
	$(".j-onlyNumber").keydown(function (e) {
		/*
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || (e.keyCode >= 35 && e.keyCode <= 40)) {
			return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}*/
	});
}

function inputOnlyNumber() {
	$(".j-onlyText").keydown(function (e) {
		// console.log('TODO only text to add');

  	});
}


function startPageLoader(status) {
	if (status){
		$('.j-pageLoader').velocity('transition.fadeIn');
		$('body').addClass('pageLoaderActivated');
	} else {
		$('.j-pageLoader').velocity('transition.fadeOut');
		$('body').removeClass('pageLoaderActivated');
	}
}

function specialAssistanceTrigger() {

	$('.j-specialAssistanceTrigger input').each(function(){
		if ($(this).is(':checked')) {
			$(this).closest('.j-specialAssistanceHead').siblings('.j-specialAssistanceBody').velocity('slideDown');
		}
	});

	$('.j-specialAssistanceTrigger input').on('change',function(){
		var selector = $(this).closest('.j-specialAssistanceHead').siblings('.j-specialAssistanceBody')
		if ($(this).is(':checked')){
			selector.velocity('slideDown', {
				complete : function(){
					$('html').velocity("scroll", {
						duration: 300,
						delay: 100,
						offset: $(this).offset().top - 90
					});
				}
			});
		} else {
			selector.velocity('slideUp');
		}
	});

	$('.j-specialAssistanceTriggerSub select').on('change',function(){
		var selector  = $(this).closest('.j-specialAssistanceBody').find('.j-specialAssistanceExtraForm');

		if ( $('option:selected',this).hasClass("extraInfo") ) {
			selector.velocity('slideDown', {
				complete : function(){
					$('html').velocity("scroll", {
						duration: 300,
						delay: 100,
						offset: selector.offset().top - $('.menu.mainMenu').outerHeight() - 15
					});
				}
			});
		} else {
			selector.velocity('slideUp');
		}
	});
}

function removeTooltip(value){
	// TODO: how to make it better
	if ( window.supportsTouch || !window.bookingLoaderMove ){
		$('.qtip-default').hide();
	}
}

function callAjaxLoad(selector){
	var _target;
	if ( !selector ){
		_target = $('.j-ajaxLoad')
	} else {
		_target = $(selector).find('.j-ajaxLoad')
	}
	_target.each(function(index, el) {
		var _this = $(this)
		var link = _this.attr('href');
		_this.attr({
			'data-link':link,
			'aria-haspopup': true
		}).removeAttr('target');
	});

	_target.unfastClick().fastClick(function(event) {
		event.preventDefault();

		var link = $(this).attr('data-link');
		var overlayClass = $(this).attr('data-overlayClass') || '';

		ajaxLoadPopupLogic(link, overlayClass)
	});


	setTimeout(function(){
		$('.j-overlayClose').focus();
	},200)
}

function ajaxLoadPopupLogic(link, extraClass, be_callback) {
	$.ajax({
		url: link,
		context: document.body
	}).done(function(e) {

		var overlayItems = '<div class="overlay__bg j-overlayBg"></div><div class="overlay__container customOverlay j-overlayContainer '+ extraClass +' "><div class="overlay__closeCont j-overlayCloseCont"><a class="customOverlay__close j-overlayClose" href="#">'+pageSettings.overlayClose+' <span class="customOverlay__closeIcon">&times;</span></a></div><div class="customOverlay__main">'+e+'</div></div>'
		$('body').append(overlayItems).addClass('overlayActive');
		$('.j-overlayBg, .j-overlayContainer, .j-overlayCloseCont, .j-overlayClose').velocity('transition.fadeIn');
		$('.j-overlayClose').css('display','inline-block');

		if ($('.j-splitCols').length > 0){ splitCols(); }

		$('.j-overlayClose, .j-overlayBg').on('click',function(event){
			event.preventDefault();
			$('.j-overlayBg, .j-overlayContainer, .j-overlayClose').velocity('transition.fadeOut', {
				complete: function() {
					$(this).remove();
				}
			});
			$('body').removeClass('overlayActive');
		});
		if (be_callback) {
			be_callback();
		}
		setTimeout(function(){
			$('.j-overlayClose').focus();
		},200)
			
		
		
	});
}

function popupLogicReady() {
	$('body').addClass('overlayActive');
	$('.j-overlayBgReady, .j-overlayContainerReady, .j-overlayCloseContReady, .j-overlayCloseReady').velocity('transition.fadeIn');
	$('.j-overlayCloseReady').css('display','inline-block');

	$('.j-overlayCloseReady, .j-overlayBgReady').unfastClick().fastClick(function(event){
		event.preventDefault();
		$('.j-overlayBgReady, .j-overlayContainerReady, .j-overlayCloseReady').velocity('transition.fadeOut');

		$('body').removeClass('overlayActive');
	});

	setTimeout(function(){
		$('.j-overlayCloseReady').focus();
	},200)
		
	
}


function skipLinkTrigger(){
	$('.skiplink__link').unfastClick().fastClick(function(e){
		var selector = $(this).attr('href');
		$(selector).attr('tabIndex',-1).focus();
	});
}

function initLightBox() {
	$('.j-ajaxLoad, .j-ajaxLoadIframe').each(function(index, el) {
		var _this = $(this)
		var link = _this.attr('href');
		_this.attr({
			'data-link': link,
			'aria-haspopup': true
		}).removeAttr('target');
	});

	// overlay logic

	callAjaxLoad();


	$('.j-ajaxLoadIframe').on('click', function(event) {
		event.preventDefault();
		var link = $(this).attr('data-link');
		ajaxLoadIframeLogic( link );
	});
}

function splitCols(){
	$('.j-splitCols').each(function(){

		var num_cols = 3,
		container = $(this),
		listItem = 'li',
		listClass = 'sub-list';

		$(this).addClass('splitList');
		container.each(function() {
			var items_per_col = new Array(),
			items = $(this).find(listItem),
			min_items_per_col = Math.floor(items.length / num_cols),
			difference = items.length - (min_items_per_col * num_cols);
			for (var i = 0; i < num_cols; i++) {
				if (i < difference) {
					items_per_col[i] = min_items_per_col + 1;
				} else {
					items_per_col[i] = min_items_per_col;
				}
			}
			for (var i = 0; i < num_cols; i++) {
				$(this).append($('<ul ></ul>').addClass(listClass));
				for (var j = 0; j < items_per_col[i]; j++) {
					var pointer = 0;
					for (var k = 0; k < i; k++) {
						pointer += items_per_col[k];
					}
					$(this).find('.' + listClass).last().append(items[j + pointer]);
				}
			}
		});

	});
}

function showTheYoungPassenger() {

	var travelType = false
	var departure = false;
	var arrival = false;
	var toShow = false;
	
	if ($("#cercaVoloMultitrattaForm").length > 0) {
		var j = 1;
		var trovato = false;
		while (j < 5 && !trovato) { 
			var dep = $("input[name='from[" + j + "]'").val();
			
			var Things = pageSettings.gbAirports;
			for (var i = Things.length - 1; i >= 0; i--) {
				if (dep == Things[i].toUpperCase() ) {
					trovato = true;
				}
			}
			j++;
		}
		departure = trovato;
	} else {
		if( $('#Origin').length > 0) {
			var dep = $('#Origin').val().toUpperCase();
			var arr = $('#Destination').val().toUpperCase();
		
			if ( dep === '' || arr === ''){
				return
			}
		
			travelType = $('#andataeritorno').is(':checked');
			var Things = pageSettings.gbAirports;
		
			for (var i = Things.length - 1; i >= 0; i--) {
				if (dep == Things[i].toUpperCase() ) {
					departure = true;
				}
		
				if (arr == Things[i].toUpperCase() ) {
					arrival = true
				}
			}
		}
	}
	
	if (arrival && travelType ) {
		toShow = true;
	} else if (departure) {
		toShow = true;
	} else {
		toShow = false;
	}

	if (toShow) {
		$('.passengers.passengers--flightFinder').addClass('has4items');
		$('.innerFlightFinder .passengers').addClass('has4items');
		if ($("#CUG").val() == "FAM") {
			$("#kids--label").text($("#kidsFAMLabelY15").val());
		}
	} else {
		$('.passengers.passengers--flightFinder').removeClass('has4items');
		$('.innerFlightFinder .passengers').removeClass('has4items');
		if ($("#CUG").val() == "FAM") {
			$("#kids--label").text($("#kidsFAMLabel").val());
	}
}
}

function changeTariffStart() {
	changeTariffStartSection('departure');
	changeTariffStartSection('return');
}

function changeTariffStartSection(dir) {
	var prefix = (dir === 'departure') ? '.j-bookingTableDeparture' : '.j-bookingTableReturn';

	$(prefix + '.j-bookingTable').addClass('changeTariff');
	var indexElement1 = $(prefix + ' .bookingTable__rightInner:visible .bookingTable__col.premiumEconomy:not(.copy):eq(0)').index(),
	    indexElement2 = $(prefix + ' .bookingTable__rightInner:visible .bookingTable__col.businessClassic:not(.copy):eq(0)').index();

	if ((indexElement1 > -1) && (indexElement2 > -1)) {
		$(prefix + ' .bookingTable__rightInner').each(function(){
			var _this = $(this);

			var element1 = _this.find('.premiumEconomy').clone(true).addClass('copy beBusiness'),
				element2 = _this.find('.businessClassic').clone(true).addClass('copy beStandard');

			var insAftSelector1 = _this.find('.bookingTable__col').eq(indexElement2),
				insAftSelector2 = _this.find('.bookingTable__col').eq(indexElement1);

			element1.insertAfter(insAftSelector1);
			element2.insertAfter(insAftSelector2);
		});

		$(prefix + ' .bookingTable__rightInner .bookingTable__col.premiumEconomy:not(.copy)').remove();
		$(prefix + ' .bookingTable__rightInner .bookingTable__col.businessClassic:not(.copy)').remove();
	}
}

function changeTariffRow(body) {

	$('.j-bookTableRowBody').each(function(){
		var __this = $(this);
		if( __this.css("display") == "block" ){

			var indexElement1 = __this.find('.bookingTable__rightInner:visible .bookingTable__col.premiumEconomy:not(.copy):eq(0)').index(),
				indexElement2 = __this.find('.bookingTable__rightInner:visible .bookingTable__col.businessClassic:not(.copy):eq(0)').index();

			if ((indexElement1 > -1) && (indexElement2 > -1)) {
				__this.find('.bookingTable__rightInner').each(function(){
					var _this = $(this);

					var element1 = _this.find('.premiumEconomy').clone(true).addClass('copy beBusiness'),
						element2 = _this.find('.businessClassic').clone(true).addClass('copy beStandard');

					var insAftSelector1 = _this.find('.bookingTable__col').eq(indexElement2),
						insAftSelector2 = _this.find('.bookingTable__col').eq(indexElement1);

					element1.insertAfter(insAftSelector1);
					element2.insertAfter(insAftSelector2);
				});

				__this.find('.bookingTable__rightInner .bookingTable__col.premiumEconomy:not(.copy)').remove();
				__this.find('.bookingTable__rightInner .bookingTable__col.businessClassic:not(.copy)').remove();
			}
		}
	});

}


var resultsLoading = {
	ref : null,
	_tm : null,
	loadingTime : null,
	waitingTime: null,
	counter: null,
	timerTotal:null,
	pageURL:null,
	init : function(){
		ref = $('.j-waitingPage');
		waitingTime = ref.attr('data-waitingTime');
		pageURL = ref.attr('data-pageUrl');
		loadingTime = parseInt(waitingTime);
		timerTotal = loadingTime/1000;
		timer = timerTotal;
		counter = ref.find('.j-waitingCounter');
		ref.velocity('transition.expandIn',{
			complete: function(){
				resultsLoading.startLoading();
			}
		});
	},
	startLoading: function(){
		ref.addClass('loaded');
		counter.text(timerTotal);
		_tm = setInterval(resultsLoading.checkCounter,1000);
	},
	checkCounter :function(){
		clearInterval(_tm);
		timer -=1;
		counter.text(timer);
		if(timer>0){
			_tm = setInterval(resultsLoading.checkCounter,1000);
		}
		// else{
		// 	window.location.assign(pageURL);
		// }
	}
}





// external libs
// @import "libs/gridset-overlay.js";
