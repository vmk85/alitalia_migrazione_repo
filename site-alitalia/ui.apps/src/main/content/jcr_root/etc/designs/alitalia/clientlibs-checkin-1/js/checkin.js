var checkinOptions = {};
var ancillaryDeleted = false;
var currency = '';
var passengersInBaggageComponent;
var loungeNotInCart = [];
var fasttrackNotInCart = [];
var validNavigation = false;
var decimale2 = "";
var separatore = "";


/*
function checkinWireUpEvents() {
  var dont_confirm_leave = 0; //set dont_confirm_leave to 1 when you want the user to be able to leave without confirmation
  var leave_message = 'You sure you want to leave?'
  function goodbye(e) {
    if (!validNavigation) {
      if (dont_confirm_leave!==1) {
        if(!e) e = window.event;
        //e.cancelBubble is supported by IE - this will kill the bubbling process.
        e.cancelBubble = true;
        e.returnValue = leave_message;
        //e.stopPropagation works in Firefox.
        if (e.stopPropagation) {
          e.stopPropagation();
          e.preventDefault();
        }
        //return works for Chrome and Safari
        return leave_message;
      }
    }
  }

  function resetcheckinnav() {
    if (!validNavigation) {
        var xhr,
            params = {"ev": "unload"};

        if (navigator.sendBeacon) {
          navigator.sendBeacon(getServiceUrl("checkincancel"), JSON.stringify(params));
        } else {
          xhr = new XMLHttpRequest();
          xhr.open('post', getServiceUrl("checkincancel"), false);
          xhr.send(JSON.stringify(params));
        }
    }
  }

  window.onbeforeunload=goodbye;
  window.onunload=resetcheckinnav;

  // Attach the event keypress to exclude the F5 refresh
  $(document).bind('keypress', function(e) {
    if (e.keyCode == 116){
      validNavigation = true;
    }
  });

    // Attach the event click for all links in the page
    $("a").bind("click", function() {
        validNavigation = true;
    });

    // Attach the event click for all links in the page
    $("button").bind("click", function() {
        validNavigation = true;
    });

    // Attach the event submit for all forms in the page
    $("form").bind("submit", function() {
        validNavigation = true;
    });

    // Attach the event click for all inputs in the page
    $("input[type=button]").bind("click", function() {
        validNavigation = true;
    });

        if (navigator.sendBeacon) {
          navigator.sendBeacon(getServiceUrl("checkincancel"), JSON.stringify(params));
        } else {
          xhr = new XMLHttpRequest();
          xhr.open('post', getServiceUrl("checkincancel"), false);
          xhr.send(JSON.stringify(params));
        }
    }
  }

  window.onbeforeunload=goodbye;
  window.onunload=resetcheckinnav;

  // Attach the event keypress to exclude the F5 refresh
  $(document).bind('keypress', function(e) {
    if (e.keyCode == 116){
      validNavigation = true;
    }
  });

    // Attach the event click for all links in the page
    $("a").bind("click", function() {
        validNavigation = true;
    });

    // Attach the event click for all links in the page
    $("button").bind("click", function() {
        validNavigation = true;
    });

    // Attach the event submit for all forms in the page
    $("form").bind("submit", function() {
        validNavigation = true;
    });

    // Attach the event click for all inputs in the page
    $("input[type=button]").bind("click", function() {
        validNavigation = true;
    });

    // Attach the event click for all inputs in the page
    $("input[type=submit]").bind("click", function() {
        validNavigation = true;
    });

}
*/
// document ready
$( document ).ready( function() {
    var a = "";
    $(".passenger").each(function(k,f){
        a = $(f).text().split(" ");
        $(f).text("");
        $(a).each(function(k,v){
            $(f).text($(f).text()+ v.charAt(0).toUpperCase() + v.slice(1).toLowerCase()+" ");
        });
    });
    $(".name").each(function(k,f){
        a = $(f).text().split(" ");
        $(f).text("");
        $(a).each(function(k,v){
            $(f).text($(f).text()+ v.charAt(0).toUpperCase() + v.slice(1).toLowerCase()+" ");
        });
    });
    passengersInBaggageComponent = $('.bagagli').closest('.customize-row').find('.flight-content');

    $(window).resize(function(){
        if ($(window).width() <= 1023){
            $("footer").css("padding-bottom",$(".checkin-button-wrap--fixed").height()-1);
        }else{
            $("footer").css("padding-bottom",0);
        }
    });

    $( window ).scroll(function(){
        if ($(window).width() <= 1023){
            $("footer").css("padding-bottom",$(".checkin-button-wrap--fixed").height()-1);
        }else{
            $("footer").css("padding-bottom",0);
        }
    });

    currency = $('#binding-currency').data('currency');

    verticalCenter();
    $(window).resize(verticalCenter);

    /** sidebar mobile non loggato **/
    $('.sidebar-link').on('click', function () {
        $(this).toggleClass('open');
        $('.sidebar-nav-mob .menu').slideToggle();
        return false;
    });

    /** sidebar mobile non loggato - voce di menu (funzione inserita su header, conflitti con my alitalia )**/
    // $('.info-user__personal-area-toggle').on('click', function () {
    //     $(this).toggleClass('open');
    //     $('.info-user__personal-area-content').slideToggle(400, function(){
    //         verticalCenter();
    //     });
    //     verticalCenter();
    //     return false;
    // });


    $(".link__toggle").click(function() {
        var linkToggle = $(this);
        $(this).closest(".link__toggle-wrap").find(".content__toggle").stop().slideToggle(function() {
            if ($(this).is(':visible')) {
                $(".link__toggle-open", linkToggle).addClass("hide");
                $(".link__toggle-close", linkToggle).removeClass("hide");
            } else {
                $(".link__toggle-close", linkToggle).addClass("hide");
                $(".link__toggle-open", linkToggle).removeClass("hide");
            }
            $('html,body').animate({
                scrollTop: $(linkToggle).closest('.flight-info').find('.flight-info__title').offset().top},'slow');
        });
        if ($(this).parent('.flight-info__link').length > 0) {
            $(this).parent('.flight-info__link').toggleClass('open');
        }
        return false;
    });

    checkinOptions.initCheckbox();

    // Sticky footer
    var siteWrap = $('.checkin-site-wrap');
    var footer = $('.footer');
    var push = $('.push');

if (document.documentElement.clientWidth > 500) {
    push.css('height', footer.height());
    siteWrap.css('margin-bottom', 0 - footer.height());

    $(window).resize(function() {
        push.css('height', footer.height());
        siteWrap.css('margin-bottom', 0 - footer.height());

        // Display block on desktop
        ww = $(window).width();

        if (ww >= 768) {
            $('.flight-info .content__toggle').removeAttr('style');
            $('.flight-info .flight-info__link').removeClass('open');
            $('.flight-info .link__toggle-close').addClass('hide');
            $('.flight-info .link__toggle-open').removeClass('hide');
        }
      });
    }
    // Sticky footer ends

    //capitalize name
    checkinOptions.capitalizeName();

} );

function removeAncillary(ancillaryId)
{
    enableLoaderCheckin();
    ancillaryDeleted = true;
    var data = {
        ancillaryId: ancillaryId
    }

    performSubmit('clearancillariescart', data, clearAncillarySuccess, clearAncillaryFail);
}

function clearAncillarySuccess()
{
    removedAncillary++;
    updateDataLayer('removedAncillary',removedAncillary);
    performSubmit('checkingetcart','#frmCart',loadCartSuccess,loadCartFail);

}

function clearAncillaryFail()
{
    disableLoaderCheckin();


}

function removeInsurance()
{
    var data = {

    }

    performSubmit('deleteinsurancerest', data, removeInsuranceSuccess, removeInsuranceFail);
}

function removeInsuranceSuccess()
{
    $('#chkwaveinsurance').trigger('click');
    performSubmit('checkingetcart','#frmCart',loadCartSuccess,loadCartFail);

}

function removeInsuranceFail()
{

}

//function validationInputs(type){
//    var inputPnr, inputFF,inputName,inputLastname;
//    var thirtheenNumbers = /^[0-9]{13}$/;
//    var sixAlphanumeric = new RegExp("^[a-zA-Z0-9]{6}$");
//    var noNumbers = /[^0-9]$/;
//
//    var input1Val,input2Val,input3Val;
//    var errorInputs = [];
//    if (type == "FF"){
//        inputFF = $( '#frequentFlyer' ).val();
//        inputLastname = $( '#lastName' ).val();
//        input1Val = ( inputFF !== ''  && thirtheenNumbers.test(inputFF)) ? 1 : 0;
//        input2Val = ( inputLastname !== '' && noNumbers.test(inputLastname)) ? 1 : 0;
//        if( input1Val && input2Val) {
//            return true;
//        } else {
//            if (!input1Val){
//                errorInputs.push($( 'input#frequentFlyer' ));
//            }
//            if (!input2Val){
//                errorInputs.push($( 'input#lastName'));
//            }
//        }
//    }
//    else if (type == "pnr"){
//        inputPnr = $( '#pnr' ).val();
//        inputName = $( '#firstName' ).val();
//        inputLastname = $( '#lastName' ).val();
//
//        input1Val = ( inputPnr !== '' &&( thirtheenNumbers.test(inputPnr) || sixAlphanumeric.test(inputPnr))) ? 1 : 0;
//        input2Val = ( inputName !== '' && noNumbers.test(inputName)) ? 1 : 0;
//        input3Val = ( inputLastname !== '' && noNumbers.test(inputLastname)) ? 1 : 0;
//        if( input1Val && input2Val && input3Val) {
//
//            return true;
//        } else {
//            if (!input1Val){
//                errorInputs.push($( 'input#pnr' ));
//                //document.getElementById("pnr").style.border = "2px solid #e30512";
//                //document.getElementById("pnr").style.color = "#e30512";
//            }
//            if (!input2Val){
//                errorInputs.push($( 'input#firstName'));
//
//                //document.getElementById("firstName").style.border = "2px solid #e30512";
//            }
//            if (!input3Val){
//                errorInputs.push($( 'input#lastName'));
//                //document.getElementById("lastName").style.border = "2px solid #e30512";
//            }
//        }
//    }
//    setErrorInput(errorInputs);
//    $('#labelCheckinError').css('display','block');
//    return false;
//}
//
//
//function setErrorInput(inputArr){
//    resetErrors();
//    var messageError = "Errore";
//    inputArr.forEach(function(inp){
//        $(inp).addClass('is-invalid-input');
//    });
//    if (messageError){
//        $('#labelCheckinError').css('display','block');
//        // $('#labelCheckinError').html(messageError);
//    }
//
//}
//function resetErrors(){
//    $('input').removeClass('is-invalid-input');
//    $('#labelCheckinError').css('display','none');
//}
//
//function cercaPnrMyFlight(e){
//    e.preventDefault();
//    e.stopPropagation();
//    if( validationInputs('pnr') ) {
//
//        // enable loader
//        enableLoader();
//        resetErrors();
//        performSubmit('checkinpnrsearch','#form-myFlightSearch',cercaPnrMyFlightsSuccess,cercaPnrMyFlightsFail);
//    }
//}
//function cercaByFF(e){
//    console.log("DEBUG");
//    e.preventDefault();
//    e.stopPropagation();
//    if (validationInputs('FF')){
//        // enable loader
//        enableLoader();
//
//        resetErrors();
//        performSubmit('searchbyfrequentflyer','#form-cercaByFF',cercaByFFSuccess,cercaByFFFail);
//    }
//}
//
//
//function cercaByFFSuccess(data) {
//    if (!data.isError) {
//        window.location.href = data.successPage;
//    }else{
//
//        // disable loader
//        disableLoader();
//
//        //$('#labelCheckinError').show();
//    }
//    $('#cercaByFFSubmit').bind('click', cercaByFF);
//}
//
//function cercaByFFFail() {
//    // disable loader
//    disableLoader();
//
//    $('#labelCheckinError').show();
//
//    $('#cercaByFFSubmit').bind('click', cercaByFF);
//}
//
//function cercaPnrMyFlightsSuccess(data) {
//    if (!data.isError) {
//        window.location.href = data.successPage;
//    }else{
//
//        // disable loader
//        disableLoader();
//
//        // $('#labelCheckinError').show();
//        // $('#labelCheckinError').text(data.errorMessage);
//    }
//    $('#cercamyFlightSubmit').bind('click', cercaPnrMyFlight);
//}
//function cercaPnrMyFlightsFail() {
//    // disable loader
//    disableLoader();
//
//    $('#labelCheckinError').show();
//    $('#labelCheckinError').text('Errore (servlet FAIL) ');
//    $('#cercamyFlightSubmit').bind('click', cercaPnrMyFlight);
//}
//function cercaByFF(){
//    if( validationInputs('FF') ) {
//        resetErrors();
//        enableLoader();
//        performSubmit('searchbyfrequentflyer','#form-cercaByFF',cercaByFFSuccess,cercaByFFFail);
//    }
//}
//function cercaByFFSuccess(data) {
//    if (!data.isError) {
//        window.location.href = data.successPage;
//    }else{
//        disableLoader();
//        $('#labelCheckinError').show();
//    }
//    $('#cercaByFFSubmit').bind('click', cercaByFF);
//}
//function cercaByFFFail() {
//    disableLoader();
//    $('#labelCheckinError').show();
//    $('#cercaByFFSubmit').bind('click', cercaByFF);
//}
checkinOptions.capitalizeName = function(){
    if ($('.passengerName').html() != undefined){
        $('.sub .infantName').each(function(el){
            if ($(this).html() != undefined){
                $(this).html($(this).html().toLowerCase().replace(/\b[a-z]/g, function(letter) {
                    return letter.toUpperCase();
                }));
            }
        });
        $('.passengerName').each(function(el){
            if ($(this).html() != undefined){
                $(this).html($(this).html().toLowerCase().replace(/\b[a-z]/g, function(letter) {
                    return letter.toUpperCase();
                }));
            }else{
                setTimeout(function(){
                    checkinOptions.capitalizeName();
                },300);
            }
        });
    }
}

checkinOptions.initCheckbox = function() {
    $('.checkbox-wrap input:checked').each(function(i, el){
        $(el).closest('.field-wrap').addClass('checked');
    });


    $(document).on('click', '.checkbox-wrap .placeholder', function() {
        $(this).parent().find('input').click();
    });
    $(document).on('click', '.checkbox-wrap input', function() {
        if($(this).closest('.field-wrap').hasClass('checked')) {
            $(this).closest('.field-wrap').removeClass('checked');
        } else {
            $(this).closest('.field-wrap').addClass('checked');
        }

        /**************** commentato al fine di evitare che i box con i dati passeggero si aprano/chiudano ad ogni cambio di valore della checkbox
         var relatedContent = $(this).closest('.passenger-wrap').find('.passenger-wrap-content');
         if (relatedContent.length > 0 && relatedContent.is(':visible')) {
			relatedContent.slideUp();
		} else {
			relatedContent.slideDown();
		}
         */
    });

//	$(document).on('click', '.passenger-wrap .edit', function() {
//		$(this).closest('.passenger-wrap').find('.passenger-wrap-content').slideDown();
//		$(this).hide();
//	});

    $(document).on('click', '.passenger-wrap-content .close', function() {
//		$(this).closest('.passenger-wrap').find('.edit').show();
        $(this).closest('.passenger-wrap-content').slideUp();
    });

}

checkinOptions.resetCheckbox = function() {
    if($('.checkbox-wrap input').not(':checked').length) {
        $('.checkbox-wrap input').not(':checked').closest('.field-wrap').removeClass('checked');
    }
}

function loadCartSuccess(data)
{
    currency = $('#binding-currency').data('currency');
    var totCountBaggage = 0;
    var totCountFast = 0;
    var totCountLounge = 0;
    var insourance = 0;
    $(".block-insurance").remove();
    $(".from-tablet").nextAll().remove();

    if(data){
        var obj = jQuery.parseJSON(data.data);
        //console.log("PARSED OBJ:");
        //console.log(obj);
    }else{
        // se non arriva nessun data rimetto il prezzo
        $(".cart-review .price").html($(".cart-review .price").attr('data-price')+" " + currency);
        return false;
    }

    if(data.decimalSeparator){
        separatore = data.decimalSeparator;

    }
    decimale2="";
    var decimale = parseInt(data.decimalDigit);
    for (var i = 0; i < decimale; i++){
        decimale2 = "0"+decimale2;
    }
    //console.log(separatore + " "+ decimale);



    var dataPriceData = "";

    $(".price-fasttrack").each(function(k,v){
        $(v).text(function(){

            dataPriceData = String($("#data-fasttrack-price").attr("data-price-fast")).split(".");
            if (dataPriceData != "undefined" ) {

                dataPriceData = adjustAncillaryPrice(dataPriceData);

                return dataPriceData + " " + $("#data-fasttrack-price").attr("data-currency-fast");
            } else {
                return "";
            }
        });
    });

    $(".price-lounge").each(function(k,v){
        $(v).text(function(){

            dataPriceData = String($("#data-lounge-price").attr("data-price-lounge")).split(".");
            if (dataPriceData != "undefined" ){

                dataPriceData = adjustAncillaryPrice(dataPriceData);

                return dataPriceData + " " + $("#data-lounge-price").attr("data-currency-lounge");
            } else {
                return "";
            }
        });
    });


    $("#priceLoungeAggiornato").each(function(k,v){
        $(v).text(function(){

            dataPriceData = String($("#data-lounge-price").attr("data-price-lounge")).split(".");
            if (dataPriceData != "undefined" ){

                dataPriceData = adjustAncillaryPrice(dataPriceData);

                return dataPriceData + " " + $("#data-lounge-price").attr("data-currency-lounge");
            } else {
                return "";
            }

        }).attr("data-price", $(".pricelounge").text());
    });


    $("#FSpriceAgg").text(function(){
        dataPriceData = String($("#data-fasttrack-price").attr("data-price-fast")).split(".");
        if (dataPriceData != "undefined" ) {

            dataPriceData = adjustAncillaryPrice(dataPriceData);

            return dataPriceData + " " + $("#data-fasttrack-price").attr("data-currency-fast");
        } else {
            return "";
        }
    }).attr("data-price", $("#price-fasttrack").text());

    dataPriceData = "";

    if (obj.cart.checkinCart != undefined || obj.cart.aziInsurancePolicy != undefined)
    {
        var vociCarrelloText = CQ.I18n.get('checkin.ancillary.cart.vociCarrello.label');
        $(".cart-content .big").html(vociCarrelloText);


        var totLounge = 0;
        var totFast = 0;
        var totBaggage = 0;
        var totAssicurazione = 0;

        var checkLounge = [];
        var checkFast = [];
        var selBaggage = [];

        var currency = $("#binding-currency.price").attr("data-currency");
        var totCart = "0";

        if (obj.cart.checkinCart != undefined)
        {

            totCart = adjustPriceDecimal(obj.cart.checkinCartObj.cartPriceToBeIssued, data);

            // totCart = obj.cart.checkinCartObj.cartPriceToBeIssued.toString();

            totCart = totCart.replace(new RegExp("\\.|,", "g"),separatore);


            var str = '';

            var count = 0;
            for(var x=0; x<obj.cart.checkinCart.aZFlightCart.length;x++)
            {

                var flight = obj.cart.checkinCart.aZFlightCart[x];

                for(var y=0; y<flight.aZSegmentCart.length;y++)
                {

                    var segment = flight.aZSegmentCart[y];

                    if (segment.aZCartPassenger.length > 0)
                    {

                        str += '<div class="flight-info" data-flight-number="' + segment.flightNumber + '">';

                        if(ancillaryDeleted){
                            str += '<div class="title-wrap open" data-toggle="flight-' + count + '" aria-controls="flight-' + count + '">';
                        }else{
                            str += '<div class="title-wrap open" data-toggle="flight-' + count + '" aria-controls="flight-' + count + '">';
                        }


                        str += '<span>' + segment.boardPoint + '-' + segment.offPoint + '</span>';
                        str += '</div>';

                        if(ancillaryDeleted){
                            str += '<ul id="flight-' + count + '" class="info-container mostra expanded" data-toggler=".expanded" aria-expanded="true">';
                        }else{
                            str += '<ul id="flight-' + count + '" class="info-container mostra expanded" data-toggler=".expanded" aria-expanded="true">';
                        }


                        count++;

                        for(var z=0; z<segment.aZCartPassenger.length;z++)
                        {

                            var passenger = segment.aZCartPassenger[z];
                            var airportCode = segment.boardPoint;

                            str += '<li class="passenger" data-passenger-id="'+passenger.passengerID+'">';
                            str += '<div class="block block-name">';
                            str += '<span class="passenger-name">' + passenger.firstName + ' ' + passenger.lastName + '</span>';
                            str += '</div>';
                            str += '<hr class="strong">';

                            for(var j=0; j<passenger.aZCartAncillary.length;j++)
                            {

                                var ancillary = passenger.aZCartAncillary[j];

                                var priceString = String(ancillary.price);
                                var single;

                                priceString = priceString.split(",");

                                if (priceString.length > 1){
                                    priceString = addThousandSeparator(priceString[0],$("#thousandSeparator").attr("data-thousandSeparator")) + separatore + (priceString[1].length > 1? priceString[1]:priceString[1] + "0");
                                    single = false;
                                }else{
                                    priceString = String(ancillary.price);
                                    priceString = priceString.split(".");
                                    if (priceString.length > 1){
                                        priceString = addThousandSeparator(priceString[0],$("#thousandSeparator").attr("data-thousandSeparator")) + separatore + (priceString[1].length > 1? priceString[1]:priceString[1] + "0");
                                        single = false;
                                    }else{
                                        priceString = String(ancillary.price);
                                        single = true;

                                    }
                                }

                                ancillary.price=priceString;

                                //console.log(ancillary);

                                if (ancillary.group == "BG"){
                                    totCountBaggage = totCountBaggage+1;
                                }
                                if (ancillary.group == "TS"){
                                    totCountFast = totCountFast+1;
                                }
                                if (ancillary.group == "LG"){
                                    totCountLounge = totCountLounge+1;
                                }

                                var commercialName = '';
                                currency = ancillary.currency;

                                if (ancillary.group == 'SA')
                                {
                                    var seatText = CQ.I18n.get('checkin.confirmation.recap.seat');
                                    var edit = CQ.I18n.get('checkin.confirmation.recap.changeSeats.label');
                                    str += '<div class="block-container" data-ancillary-group="' + ancillary.group + '" data-ancillary-code="' + ancillary.rficSubcode + '">';
                                    str += '<div class="block block-seat-title">';
                                    str += '<span class="seat-title">'+seatText+'</span>';
                                    str += '</div>';
                                    str += '<div class="block block-seat">';
                                    str += '<span class="seat-type">' + ancillary.commercialName + '</span>';
                                    str += '<span class="seat-number">' + ancillary.pdcSeat + '</span>';
                                    str += '</div>';
                                    str += '<div class="block block-price">';
                                    str += '<span class="price" style="">'+(single? ancillary.price + (decimale != 0? separatore +decimale2:"")  : ancillary.price) + ' ' + ancillary.currency +  '</span>';
                                    str += '<a style="margin-left: 10px" class="action__icon action__icon--medium action__icon--modifica" href="./check-in-seatmap.html"><span class="show-for-large" style="font-size: 16px; color: #006643"></span><span class="show-for-sr"></span></a>';
                                    str += '</div>';
                                    str += '</div>';

                                } else if (ancillary.group == 'BG') {
                                    commercialName = CQ.I18n.get('checkin.ancillary.bagaglioExtra.label');

                                    str += '<div class="block-container" data-ancillary-group="' + ancillary.group + '" data-ancillary-code="' + ancillary.rficSubcode + '">';
                                    str += '<div class="block block-baggage-title">';
                                    str += '<span class="baggage-title">' + commercialName + '</span>';
                                    str += '</div>';
                                    str += '<div class="block block-baggage">';
                                    str += '<span class="baggage-number">1</span>';
                                    str += '</div>';
                                    str += '<div class="block block-price">';

                                    var passengerBagsCount = 0;
                                    passenger.aZCartAncillary.forEach(function(item){
                                        if(item.group == 'BG'){
                                            passengerBagsCount ++;
                                        }
                                    });

                                    if (j === passengerBagsCount - 1) {
                                        str += '<span class="price">' +(single? ancillary.price + (decimale != 0? separatore +decimale2:"")  : ancillary.price) + ' ' + ancillary.currency +  '</span>';
                                        str += '<a class="action__icon action__icon--delete removeAncillary" data-ancillary-id="' + ancillary.ancillaryID + '" href="javascript:removeAncillary(' + ancillary.ancillaryID + ');"></a>';
                                    } else {
                                        str += '<span class="price" style="margin-right: 26px">' + addThousandSeparator(ancillary.price,$("#thousandSeparator").attr("data-thousandSeparator")) +(decimale != 0? separatore +decimale2:"") + ' ' + ancillary.currency + '</span>';
                                        // str += '<a class="action__icon action__icon--delete removeAncillary" data-ancillary-id="' + ancillary.ancillaryID + '" href="javascript:removeAncillary(' + ancillary.ancillaryID + ');"></a>';
                                    }

                                    str += '</div>';
                                    str += '</div>';

                                    totBaggage += Number(ancillary.price );


                                } else if (ancillary.group == 'LG') {
                                    commercialName = ancillary.commercialName;
                                    //var loungeAirport = $('.lounge').closest('.customize-row').find('[data-passenger-id="'+passenger.passengerID+'"]').find('[name="chklounge"]').closest('.box-airport').find('.airport-code').html();
                                    var loungeAirport = airportCode;
                                    str += '<div class="block-container" data-ancillary-group="' + ancillary.group + '" data-ancillary-code="' + ancillary.rficSubcode + '">';
                                    str += '<div class="block block-baggage-title">';
                                    str += '<span class="baggage-title">' + commercialName + '</span>';
                                    str += '</div>';
                                    str += '<div class="block block-baggage">';
                                    str += '<span class="baggage-number">' + loungeAirport + '</span>';
                                    str += '</div>';
                                    str += '<div class="block block-price">';
                                    str += '<span class="price">' +(single? ancillary.price + (decimale != 0? separatore +decimale2:"") : ancillary.price)+ ' ' + ancillary.currency +   '</span>';
                                    str += '<a class="action__icon action__icon--delete removeAncillary" data-ancillary-id="' + ancillary.ancillaryID + '" href="javascript:removeAncillary(' + ancillary.ancillaryID + ');"></a>';

                                    str += '</div>';
                                    str += '</div>';

                                    totLounge += Number(ancillary.price);

                                } else if (ancillary.group == 'TS') {
                                    commercialName = ancillary.commercialName;
                                    //var fastTrackAirport = $('.fast-track').closest('.customize-row').find('[data-passenger-id="'+passenger.passengerID+'"]').find('[name="chkfasttrack"]').closest('.box-airport').find('.airport-code').html();
                                    var fastTrackAirport = airportCode;
                                    str += '<div class="block-container" data-ancillary-group="' + ancillary.group + '" data-ancillary-code="' + ancillary.rficSubcode + '">';
                                    str += '<div class="block block-baggage-title">';
                                    str += '<span class="baggage-title">' + commercialName + '</span>';
                                    str += '</div>';
                                    str += '<div class="block block-baggage">';
                                    str += '<span class="baggage-number">' + fastTrackAirport + '</span>';
                                    str += '</div>';
                                    str += '<div class="block block-price">';

                                    str += '<span class="price">' +(single? ancillary.price + (decimale != 0? separatore +decimale2:"")  : ancillary.price) + ' ' + ancillary.currency +  '</span>';
                                    str += '<a class="action__icon action__icon--delete removeAncillary" data-ancillary-id="' + ancillary.ancillaryID + '" href="javascript:removeAncillary(' + ancillary.ancillaryID + ');"></a>';

                                    str += '</div>';
                                    str += '</div>';

                                    totFast += Number(ancillary.price);
                                }


                                str += '<hr>';

                                /*<div class="block-container">
                                    <div class="block block-baggage-title">
                                        <span class="baggage-title">Bagaglio extra</span>
                                    </div>
                                    <div class="block block-baggage">
                                        <span class="baggage-number">1</span>
                                    </div>

                                    <div class="block block-price">
                                        <span class="price">999.999 RUB</span>
                                        <a class="action__icon action__icon--delete" href="javascript:;"></a>
                                    </div>
                                </div>
                                <hr>*/

                            }


                            str += '</li>';

                        }

                        str += '</ul>';
                        str += '</div>';

                    }

                }

            }

        }

        var strInsurance = "";
        if (obj.cart.aziInsurancePolicy != undefined && obj.cart.aziInsurancePolicy.totalInsuranceCost !=0)
        {

            insourance = insourance+1;

            if (totCart != "0"){

                var actualCart = totCart.split(separatore);
                var insuranceCost = obj.cart.aziInsurancePolicy.totalInsuranceCost.toString().split(".");

                var sumPreDec = parseInt(actualCart[0]) + parseInt(insuranceCost[0]);
                var sumPostDec = 0;

                if (insuranceCost[1] != undefined){
                    sumPostDec = parseInt(actualCart[1]) + parseInt(insuranceCost[1]);
                    totCart = sumPreDec.toString() + separatore +  sumPostDec.toString();
                } else {
                    sumPostDec = actualCart[1];
                    totCart = sumPreDec.toString() + separatore +  sumPostDec.toString();

                }
            } else {
                totCart = obj.cart.aziInsurancePolicy.totalInsuranceCost.toString();

                totCart = totCart+  separatore +  decimale2;
            }



            strInsurance += '<div class="block block-insurance">';
            strInsurance += '<p class="insurance-paragraph"><strong>'+CQ.I18n.get('checkin.ancillary.assicurazioneAll.label')+'</strong></p>';
            strInsurance += '<span class="ace">'+CQ.I18n.get('checkin.ancillary.assicurazioneChubb.label')+'</span>';
            strInsurance += '<span class="total-price">' + obj.cart.aziInsurancePolicy.totalInsuranceCost + (decimale != 0? separatore +decimale2:"") + ' '+currency+'</span>';
            strInsurance += '<a class="action__icon action__icon--delete" href="javascript:removeInsurance();"></a>';
            strInsurance += '</div>';

        }

        totCart = adjustCartTotalPrice(totCart);

        $(".cart-review .price").html(totCart + " " + currency);

        $(".cart-review .price").attr('data-price', totCart);

        $(".from-tablet").before(strInsurance);

        $(".from-tablet").after(str);

        $( '[data-toggle*="flight"]' ).on( 'click', function() {
            var fId = $(this).data("toggle");
            if ($(this).hasClass("open"))
            {
                $(this).removeClass("open");
                $("#" + fId).removeClass("expanded");
                $("#" + fId).attr("aria-expanded", "false");
            }
            else
            {
                $(this).addClass("open");
                $("#" + fId).addClass("expanded");
                $("#" + fId).attr("aria-expanded", "true");
            }
        } );

        // Aggiorno i dati

        // $("#InspriceAgg").text(function(){
        //     return parseInt($("#data-fasttrack-price").attr("data-price-fast")) + separatore + decimale2 + $("#data-fasttrack-price").attr("data-currency-fast");
        // }).attr("data-price", $("#price-fasttrack").text());
        // // $(".priceLounge").html(totLounge + " " + currency);
        $(".priceBaggage").html(totBaggage + " " + currency);

        if(ancillaryDeleted) {
            for(var p = 0; p < passengersInBaggageComponent.length; p++) {
                passengersInBaggageComponent.get(p).inCart = false;
            }
            syncronizeAncillaries();
            clearAncillariesFromPassengersNotInCart(true);
        }



    }
    else
    {
        clearAncillariesFromPassengersNotInCart(false);

        $(".cart-content .big").html(CQ.I18n.get('checkin.ancillary.cart.emptyCart.label'));
        $(".cart-review .price").html("0 "+currency);
        $(".cart-review .price").attr("data-price", "0");
    }

    ancillaryDeleted = false;
    disableLoaderCheckin();
    $("#priceCart").text($("#payLabel").attr("data-priceCart")+ " : " +$("#binding-currency").text());

    if ($("#binding-currency").attr("data-price") == "0"){
        $("button.button.button--inline").text($("#textProdegui").attr("data-val"));
        $("button.button.button--inline").attr("onclick","typ()");
    }

    if (totCountBaggage != 0){
        var label = CQ.I18n.get('checkin.ancillary.baggages.totalBaggage.label',[totCountBaggage]);
        $("#dataFeedbackBaggage").text(label);
        $("#dataFeedbackBaggage").attr("data-baggage",totCountBaggage);
        $("#feedback").removeClass("hide");
        $("#buttonFeedback").addClass("hide");

    } else {
        $("#feedback").addClass("hide");
        $("#buttonFeedback").removeClass("hide");
    }

    if (totCountLounge  != 0){
        var labelLounge = CQ.I18n.get('checkin.ancillary.totalLounge.label',[totCountLounge]);
        $("#dataFeedbackLounge").text(labelLounge);
        $("#dataFeedbackLounge").attr("data-lounge",totCountLounge);

        $("#feedbackLoungeSec").removeClass("hide");
        $("#feedbackLounge").addClass("hide");

    } else {
        $("#feedbackLoungeSec").addClass("hide");
        $("#feedbackLounge").removeClass("hide");
    }
    if (totCountFast != 0){
        var labelFast = CQ.I18n.get('checkin.ancillary.baggages.fastTrack.label',[totCountFast]);
        $("#dataFeedbackFast").text(labelFast);
        $("#dataFeedbackFast").attr("data-fast",totCountFast);
        $("#feedbackFastSec").removeClass("hide");
        $("#feedbackFast").addClass("hide");

    } else {
        $("#feedbackFastSec").addClass("hide");
        $("#feedbackFast").removeClass("hide");
    }
    if (insourance != 0){
        $("#dataFeedbackIns").text(insourance + " " + $("#dataFeedbackIns").attr("data-label"));
        $("#dataFeedbackIns").attr("data-ins",insourance);
        $("#feedbackInsSec").removeClass("hide");
        $("#feedbackIns").addClass("hide");

    } else {
        $("#feedbackInsSec").addClass("hide");
        $("#feedbackIns").removeClass("hide");
    }

    var a;
    $(".passenger-name").each(function(k,f){
        a = $(f).text().split(" ");
        $(f).text("");
        $(a).each(function(k,v){
            $(f).text($(f).text()+ v.charAt(0).toUpperCase() + v.slice(1).toLowerCase()+" ");
        });
    });



}
function typ(){
    enableLoaderCheckin();

    document.location.href = './check-in-thank-you.html'+(editBp == "true"? "?&editBp=true&flight="+flight:"");

}

function syncronizeAncillaries() {
    var cart = $('.cart-content');
    var passengersLength = $('.bagagli').closest('.customize-row').find('.flight-content').length;
    var segment = cart.find('.flight-info');
    if(segment.length) {
        segment.each(function (flightIndex) {
            var flightNumber = $(this).attr('data-flight-number');
            var passengers = $(this).find('.passenger');
            passengers.each(function (passengerIndex) {
                var passengerId = $(this).attr('data-passenger-id');
                var bags = $(this).find('[data-ancillary-group="BG"]').length;
                loungeNotInCart[passengerIndex] = $(this).find('[data-ancillary-group="LG"]').length > 0 ? false : true;
                fasttrackNotInCart[passengerIndex] = $(this).find('[data-ancillary-group="TS"]').length > 0 ? false : true;

                for(var p = 0; p < passengersInBaggageComponent.length; p++) {
                    if($(passengersInBaggageComponent.get(p)).data('passenger-id') === passengerId) {
                        passengersInBaggageComponent.get(p).inCart = true;
                    }
                }

                var passengerBagRow = $('.bagagli').closest('.customize-row').find('[data-flight-number="' + flightNumber + '"][data-passenger-id="' + passengerId + '"]');

                while (bags < passengerBagRow.find('[id^="chkwavefieldbag"]').val()) {
                    passengerBagRow.find('.button.minus').click();
                }
                while (bags > passengerBagRow.find('[id^="chkwavefieldbag"]').val()) {
                    passengerBagRow.find('.button.plus').click();
                }

            });

        });
    } else {
        var passengersLength = $('.bagagli').closest('.customize-row').find('.flight-content').length;

    }
}

function clearAncillariesFromPassengersNotInCart(deleted){
    // carrello vuoto o caricato senza aver cancellato un ancillare
    if(!deleted) {
        for (var p = 0; p < passengersInBaggageComponent.length; p++) {
            passengersInBaggageComponent.get(p).inCart = false;
        }
    }

    for(var p = 0; p < passengersInBaggageComponent.length; p++) {
        if (!passengersInBaggageComponent.get(p).inCart) {
            $(passengersInBaggageComponent.get(p)).find('.button.minus').click();
        }
    }


    for(var p1 = 0; p1 < passengersInBaggageComponent.length; p1++) {
        var jqueryPassenger = $(passengersInBaggageComponent.get(p1));

        if(loungeNotInCart[p1] || !passengersInBaggageComponent.get(p1).inCart) {
            var lounge = $('.lounge').closest('.customize-row').find('[data-flight-number="' + jqueryPassenger.data('flight-number') + '"][data-passenger-id="' + jqueryPassenger.data('passengerId') + '"] .chkwavelounge');
            if(lounge.prop('checked')) {
                lounge.click();
            }
        }

        if(fasttrackNotInCart[p1] || !passengersInBaggageComponent.get(p1).inCart) {
            var fasttrack = $('.fast-track').closest('.customize-row').find('[data-flight-number="' + jqueryPassenger.data('flight-number') + '"][data-passenger-id="' + jqueryPassenger.data('passengerId') + '"] .chkwavefasttrack');
            if(fasttrack.prop('checked')){
                fasttrack.click();
            }
        }
    }

}

// open loader
function enableLoader() {
    $('.cerca-volo__inner').addClass('loading');
};

// close loader
function disableLoader() {
    $('.cerca-volo__inner').removeClass('loading');
};
function loadCartFail()
{
    disableLoaderCheckin();

    //console.log("Errore");
}

// open loader
function enableLoaderCheckin() {
    $('.circle-loader.loader-check-in').show();
};

// close loader
function disableLoaderCheckin() {
    $('.circle-loader.loader-check-in').hide();
};

function loadMenuPersonalAreaSuccess(data){
    $("#millemigliaMenu").html(data);

    $('#form-cercaByFF #frequentFlyer').val(millemigliaCode.replace('MM:',""));
    $('#form-cercaByFFSidebar #frequentFlyer').val(millemigliaCode.replace('MM:',""));
    $('#form-cercaByFFSidebar #lastName').val(millemigliaSurname);

    $('.sidebar-nav.sidebar-logged').show();
    $('.sidebar-nav.sidebar-not-logged').hide();
}
function invokeShowCaptcha(selector, enabled) {
    var enable = '';
    if (enabled) {
        enable = "." + enabled;
    }
    invokeServiceCaptchaLogin("captcha-login-partial" + "." + selector + enable, "html", null,
        function(data) {
            if (selector) {
                $("#" + selector + ".j-showCaptcha").html(data);
                $.ajax({
                    url : getServiceUrl("captcha-login-partial.script" + enable, "html", false),
                    context : document.body
                }).done(function(data) {
                    if ($("#recaptchaScript").length > 0) {
                        $("#recaptchaScript").replaceWith(data);
                    } else {
                        $("head").append(data);
                    }
                    if (selector == 'captcha_header' && enabled == "0" && $("#loginMenu").hasClass("isActive")) {
                        $("#loginMenu").css("height", $("#loginMenu .mod").get(0).scrollHeight);
                    }
                });
                if (enabled == "1") {
                    $("input[name$='pin']").each(function(i,elem) {
                        $(elem).val('');
                    });
                }
            }
        }
    );
}
function invokeServiceCaptchaLogin(selector, extension, data, done, fail) {
    settings = {
        url: getServiceUrl(selector, extension, false),
        context : document.body
    };

    if (data) {
        settings["data"] = data;
    }

    $.ajax(settings).done(function(data) {
        if (done) {
            done(data);
        }
    })
        .fail(function() {
            if (fail) {
                fail();
            }
        });
}

function validationInputs(type){
    var inputPnr, inputFF,inputName,inputLastname;
    // var regex1 = RegExp("[^0-9]$");
    var sixNumbers = /^[0-9]{4,10}$/;
    var thirteenNumbers = /^[0-9]{13}$/;
    var sixAlphanumeric = new RegExp("^[a-zA-Z0-9]{6}$");
    var fiveAlphanumeric = new RegExp("^[a-zA-Z0-9]{5}$");
    var noNumbers = /[^0-9]$/;
    var fiveToTenNumbers = /^[0-9]{5,10}$/;
    var fourNumbers = /^[0-9]{4}$/;


    var input1Val,input2Val,input3Val;
    var errorInputs = [];
    if (type == "FF"){
        inputFF = $( '#form-cercaByFF #frequentFlyer' ).val();
        inputLastname = $( '#form-cercaByFF #lastName' ).val();
        input1Val = ( inputFF !== ''  && sixNumbers.test(inputFF)) ? 1 : 0;
        input2Val = ( inputLastname !== '' && noNumbers.test(inputLastname)) ? 1 : 0;
        if( input1Val && input2Val) {
            return true;
        } else {
            if (!input1Val){
                errorInputs.push($( 'input#frequentFlyer' ));
            }
            if (!input2Val){
                errorInputs.push($( '.millemiglia_code input#lastName'));
            }
        }
    }
    if (type == "FFSidebar"){
        inputFF = $( '#form-cercaByFFSidebar #frequentFlyer' ).val();
        inputLastname = $( '#form-cercaByFFSidebar #lastName' ).val();
        input1Val = ( inputFF !== ''  && sixNumbers.test(inputFF)) ? 1 : 0;
        input2Val = ( inputLastname !== '' && noNumbers.test(inputLastname)) ? 1 : 0;
        if( input1Val && input2Val) {
            return true;
        } else {
            if (!input1Val){
                errorInputs.push($( 'input#frequentFlyer' ));
            }
            if (!input2Val){
                errorInputs.push($( '.millemiglia_code input#lastName'));
            }
        }
    }
    else if (type == "pnr"){
        inputPnr = $( '#form-cercaPnr #pnr' ).val();
        inputName = $( '#form-cercaPnr #firstName' ).val();
        inputLastname = $( '#form-cercaPnr #lastName' ).val();

//        input1Val = ( inputPnr !== '' &&( thirteenNumbers.test(inputPnr) || sixAlphanumeric.test(inputPnr) || fiveAlphanumeric.test(inputPnr))) ? 1 : 0;
        input1Val = ( inputPnr !== '' &&( thirteenNumbers.test(inputPnr) || sixAlphanumeric.test(inputPnr))) ? 1 : 0;
        input2Val = ( inputName !== '' && noNumbers.test(inputName)) ? 1 : 0;
        input3Val = ( inputLastname !== '' && noNumbers.test(inputLastname)) ? 1 : 0;
        if( input1Val && input2Val && input3Val) {

            return true;
        } else {
            if (!input1Val){
                errorInputs.push($( 'input#pnr' ));
                //document.getElementById("pnr").style.border = "2px solid #e30512";
                //document.getElementById("pnr").style.color = "#e30512";
            }
            if (!input2Val){
                errorInputs.push($( 'input#firstName'));

                //document.getElementById("firstName").style.border = "2px solid #e30512";
            }
            if (!input3Val){
                errorInputs.push($( 'input#lastName'));
                //document.getElementById("lastName").style.border = "2px solid #e30512";
            }
        }
    }else if (type == "login"){
        pin = $( '#pin' ).val().trim();
        mm = $( '#millemiglia' ).val().trim();
        loginOptions.input1Val = ( mm !== '' && fiveToTenNumbers.test(mm)) ? 1 : 0;
        loginOptions.input2Val = ( pin !== '' && fourNumbers.test(pin)) ? 1 : 0;
        if( loginOptions.input1Val && loginOptions.input2Val) {
            return true;
        } else {
            if (!loginOptions.input1Val){
                errorInputs.push($( '#millemiglia' ));
            }
            if (!loginOptions.input2Val){
                errorInputs.push($( '#pin'));
            }
        }
    }
    setErrorInput(errorInputs);
    $('#labelCheckinError').css('display','block');
    return false;
}
function setErrorInput(inputArr){
    resetErrors();
    var messageError = "";
    inputArr.forEach(function(inp){
        $(inp).addClass('is-invalid-input');
        $(inp).prev().addClass('is-invalid-input');
        $(inp).next().addClass('is-invalid-input');
        messageError = '<p>';
        switch ($(inp).attr('id')){
            case 'code':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.pnrTicketNumber.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('checkin.common.genericUncheckedField.label') + ". ";
                }
                break;
            case 'pnr':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.pnrTicketNumber.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('checkin.common.genericUncheckedField.label') + ". ";
                }
                break;
            case 'name':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.nome.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('checkin.common.genericUncheckedField.label') + ". ";
                }
                break;
            case 'surname':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.cognome.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('checkin.common.genericUncheckedField.label') + ". ";
                }
                break;
            case 'name_surname':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.cognome.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('checkin.common.genericUncheckedField.label') + ". ";
                }
                break;
            case 'frequentFlyer':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.frequentFlyer.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('checkin.common.genericUncheckedField.label') + ". ";
                }
                break;
            case 'firstName':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.nome.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('checkin.common.genericUncheckedField.label') + ". ";
                }
                break;
            case 'lastName':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.cognome.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('checkin.common.genericUncheckedField.label') + ". ";
                }
                break;
            case 'millemiglia':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.frequentFlyer.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('checkin.common.genericUncheckedField.label') + ". ";
                }
                break;
            case 'pin':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.pin.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('checkin.common.genericUncheckedField.label') + ". ";
                }
                break;
               }
                messageError += '</p>';
                $(".fb-" + $(inp).attr('id')).html(messageError);
    });
}
function resetErrors(){
    $("#header-wave-login-errors").html("");
    $("#header-wave-login-errors").css('display','none');

    $('input').removeClass('is-invalid-input');
    $('input').prev().removeClass('is-invalid-input');
    $('input').next().removeClass('is-invalid-input');
    $('#labelCheckinError').css('display','none');
    $('#labelCheckinError').html("");
    $('.labelErrorInfoStatoVolo').html("");
    $('.labelErrorInfoStatoVolo').css('display','none');
    $('.labelError.is-invalid-input').find('p').html("");

    $("[class^='fb-']").html("");
}


function adjustPriceDecimal(price, data){

    if (
        (data.decimalSeparator != null || data.decimalSeparator != undefined)
        &&
        (
            (data.decimalDigit != null || data.decimalDigit != undefined)
            &&
            parseInt(data.decimalDigit) != 0
        )
    ){
        if(
            (!price.indexOf(new RegExp("\\.|,", "g")))
        ){
            price = price + data.decimalSeparator + decimale2;
        }
    }
    return price;
}

function adjustAncillaryPrice(dataPriceData){
    if(decimale2 != ""){
        if (dataPriceData.length > 1) {
            dataPriceData = addThousandSeparator(dataPriceData[0],$("#thousandSeparator").attr("data-thousandSeparator")) + separatore + (dataPriceData[1].length < decimale2.length ? dataPriceData[1] + decimale2.substring(0,decimale2.length - dataPriceData[1].length) : (dataPriceData[1].length > decimale2.length ? dataPriceData[1].substring(0,dataPriceData[1].length - decimale2.length):dataPriceData[1]))
        } else {
            dataPriceData = addThousandSeparator(dataPriceData[0],$("#thousandSeparator").attr("data-thousandSeparator")) + separatore + decimale2
        }

    } else {
        dataPriceData = addThousandSeparator(dataPriceData[0],$("#thousandSeparator").attr("data-thousandSeparator"));
    }
    return dataPriceData;
}

function adjustCartTotalPrice(totCart){
    totCart = totCart.split(separatore);
    if (totCart[1] == undefined){
        totCart.push("");
    }
    if (decimale2 != "" ){
            if (totCart[1].length < decimale2.length ){
            totCart[1] = totCart[1] + decimale2.substring(0,(decimale2.length - totCart[1].length));
        } else if (decimale2.length < totCart[1].length)
            {
            totCart[1] = totCart[1].substring(0,(totCart[1].length -decimale2.length ));
        }
    }
    if (totCart[1] == ""){
        return addThousandSeparator(totCart[0],$("#thousandSeparator").attr("data-thousandSeparator"));

    }else {
        return addThousandSeparator(totCart[0], $("#thousandSeparator").attr("data-thousandSeparator")) + separatore + totCart[1];
    }
}

function  splitAdjust(totalPrice) {
    totalPrice = totalPrice.split(separatore);

    return addThousandSeparator(totalPrice[0],$("#thousandSeparator").attr("data-thousandSeparator")) + (totalPrice[1] != undefined ? separatore + totalPrice[1]: "");
}