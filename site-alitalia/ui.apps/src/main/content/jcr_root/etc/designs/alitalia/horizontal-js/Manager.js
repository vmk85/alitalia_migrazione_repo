$(document).ready(function () {
    $("#DayDeparted").hide();
    $("#DayReturn").hide();

    InitializeStart(false, moment());

    $("#andataeritorno").change(function () {
        $("#retRange").removeAttr("disabled");
        $("#calendar1").removeClass("trip-disabled");
        $("#Return").removeClass("trip-disabled");
        $("#DayReturn").hide();
        $("#Return").text('return');

        var startDate = $('#depRange').data('daterangepicker').startDate;
        InitializeStart(false, startDate);

    });

    $("#soloandata").change(function () {
        $("#retRange").attr("disabled", "disabled");
        $("#DayReturn").hide();
        $("#Return").text('return');
        $("#calendar1").addClass("trip-disabled");
        $("#Return").addClass("trip-disabled");
        $("#calendar1").show();

        var startDate = $('#depRange').data('daterangepicker').startDate;
        InitializeStart(true, startDate);
    });

   
    $('#depOverlay').popup({
        transition: 'all 0.3s'
    });
    $('#arrOverlay').popup({
        transition: 'all 0.3s'
    });
    $('#passengersOverlay').popup({
        transition: 'all 0.3s'
    });
    $('#classOverlay').popup({
        transition: 'all 0.3s'
    });

    InitializeHandle()
	 InitializeHandleOverlay();


});


function InitializeHandle() {
    var options = {
        minimum: 1,
        maximize: 7,
    }
    $('#adultCounter').handleCounter(options);
    $('#youngAdultCounter').handleCounter({ minimum: 0, maximize: 7});
    $('#childCounter').handleCounter({ minimum: 0, maximize: 7});
    $('#infantCounter').handleCounter({ minimum: 0, maximize: 7 });
}
function InitializeHandleOverlay() {
    var options = {
        minimum: 1,
        maximize: 7,
    }
    $('#adultCounterOverlay').handleCounterOverlay(options);
    $('#youngAdultCounterOverlay').handleCounterOverlay({ minimum: 0, maximize: 7 });
    $('#childCounterOverlay').handleCounterOverlay({ minimum: 0, maximize: 7});
    $('#infantCounterOverlay').handleCounterOverlay({ minimum: 0, maximize: 7 });
}

function InitializeStart(isSingle, startDate) {
    var options = {};
    options.parentEl = $('#parentEl').val();
    options.startDate = startDate;
    options.endDate = options.startDate
    options.minDate = moment();
    options.maxDate = moment().clone().add(1, 'year');
    options.id = "start";
    options.singleDatePicker = isSingle;
    //options.opens = "center";
    //options.drops = "down";

    $('#depRange').daterangepicker(options);
    //$('#depRangeCalendarOverlay').daterangepicker(options);
    if (!isSingle) {
        InitializeEnd(startDate);
    }
}

function InitializeEnd(startDate) {
    var options = {};
    options.parentEl = $('#parentEl').val();
    options.startDate = startDate;
    options.endDate = "undefined";
    options.minDate = startDate;
    options.maxDate = moment().clone().add(1, 'year');
    options.id = "end";

    $('#retRange').daterangepicker(options);
}

function anchorDep() {
    $('#departureInputOverlay').val($('#departureInput').val())
    $('#anchorOverlayDep').click();
}
function anchorArr() {
    $('#arrivalInputOverlay').val($('#arrivalInput').val())
    $('#anchorOverlayArr').click();
}
function anchorClass() {
    $('#anchorOverlayClass').click();
}