
/* global */

/**
 * Keeps social share init data.
 */
var alitaliaSocialShareData = {
	enabled: false,
	enabledProviders: '',
	moreEnabledProviders: '',
	operationMode: '',
	showMoreButton: true,
	showEmailButton: true,
	actionLinkCaption: ''
};


/* external functions */

/**
 * Invoked on page load to initialize social share configuration options.
 */
function initSocialShare(enabled, enabledProviders, moreEnabledProviders, operationMode, 
		showMoreButton, showEmailButton, actionLinkCaption) {
	alitaliaSocialShareData.enabled = enabled;
	alitaliaSocialShareData.enabledProviders = enabledProviders;
	alitaliaSocialShareData.moreEnabledProviders = moreEnabledProviders;
	alitaliaSocialShareData.operationMode = operationMode;
	alitaliaSocialShareData.showMoreButton = showMoreButton;
	alitaliaSocialShareData.showEmailButton = showEmailButton;
	alitaliaSocialShareData.actionLinkCaption = actionLinkCaption;
}

/**
 * Update the DOM data related to social share, applying visibility
 * based on preferences. Called automatically on document ready,
 * should be manually invoked when social share widget is dinamically
 * added to the page.
 */
function refreshSocialShareData() {
	generateSocialShareDivsIdentifiers();
	if (alitaliaSocialShareData.enabled) {
		$('.binding-social-share-ui').removeClass('hidden');
	} else {
		$('.binding-social-share-ui').addClass('hidden');
	}
}

/**
 * Generic function to display a share UI with the provided params.
 */
function socialShare(title, description, linkBack, imageUrl, snapElementId, 
		successCallback, errorCallback, context) {
	if (alitaliaSocialShareData.enabled) {
		// prepare action
		var action = new gigya.socialize.UserAction();
		action.setTitle(title);
		action.setDescription(description);
		action.setLinkBack(prepareSocialShareAbsoluteUrl(linkBack));
		action.addActionLink(alitaliaSocialShareData.actionLinkCaption, linkBack);
		if (imageUrl && imageUrl != "") {
			var actionImage = {
				src: prepareSocialShareAbsoluteUrl(imageUrl),
				href: prepareSocialShareAbsoluteUrl(linkBack),
				type: 'image'
			};
			action.addMediaItem(actionImage);
		}
		// prepare params
		var params = {
	          userAction: action
	        , enabledProviders: alitaliaSocialShareData.enabledProviders
	        , moreEnabledProviders: alitaliaSocialShareData.moreEnabledProviders
			, operationMode: alitaliaSocialShareData.operationMode
			, showMoreButton: alitaliaSocialShareData.showMoreButton
			, showEmailButton: alitaliaSocialShareData.showEmailButton
			, snapToElementID: snapElementId
			, onError: errorCallback
			, onSendDone: successCallback
			, context: context
		};
		if (errorCallback == null) {
			params.onError = defaultSocialShareErrorCallback;
		}
		if (successCallback == null) {
			params.onSendDone = defaultSocialShareSendDoneCallback;
		}
		// display share UI
		gigya.socialize.showShareUI(params);
	} else {
		console.error('Social share is disabled by configuration.');
	} 
}


/* internal methods */

/**
 * On document ready, generate a unique identifier for all social share button divs.
 */
function generateSocialShareDivsIdentifiers() {
	$('.social-share-button').each(function(index, item) {
	    $(item).attr('id', 'socialShareButton' + index);
	});
}

/**
 * Convert a relative URL to an absolute one based on the current page path.
 */
function prepareSocialShareAbsoluteUrl(relativeUrl) {
	var link = document.createElement("a");
	link.href = relativeUrl;
	var absoluteLink = link.protocol + "//" + link.host + link.pathname + link.search + link.hash;
	return absoluteLink;
}


/* callbacks */

/**
 * Default social share error callback function, used if a custom one is not provided.
 */
function defaultSocialShareErrorCallback() {
	console.error("an error occurred with social share.");
}

/**
 * Default social share send done callback function, used if a custom one is not provided.
 */
function defaultSocialShareSendDoneCallback() {
	console.info("Social share performed.");
}


/* event listeners */

/**
 * Apply visibility of social share actions based on configuration.
 */
function onSocialShareDocumentReady(e) {
	refreshSocialShareData();
}

/**
 * Event handler method used in generic social share button template.
 */
function onSocialShareButtonClick(e) {
	socialShare(
			$(e.currentTarget).siblings('.socialShare_title').val(),
			$(e.currentTarget).siblings('.socialShare_description').val(),
			$(e.currentTarget).siblings('.socialShare_linkBack').val(),
			$(e.currentTarget).siblings('.socialShare_imageUrl').val(),
			$(e.currentTarget).attr('id'));
	return false;
}


/* execution code */

//add listener for document ready event to apply social share configuration
$(document).ready(onSocialShareDocumentReady);