var paymentOptions = {};

$(document).ready( function() {
	paymentOptions.initRadio();
});

paymentOptions.initRadio = function() {
	if($('.payment-methods .radio-wrap input:checked').length) {
		$('.payment-methods .radio-wrap input:checked').closest('.payment-methods .radio-wrap').addClass('checked');
	}
	$(document).on('click', '.payment-methods .radio-wrap .placeholder', function() {
		$(this).parent().find('input').click();
	});
	$(document).on('click', '.payment-methods .radio-wrap input', function() {
		$('.payment-methods .radio-wrap input[name='+$(this).attr('name')+']').closest('.payment-methods .radio-wrap').removeClass('checked');
		$(this).closest('.payment-methods .radio-wrap').addClass('checked');
	});
};
