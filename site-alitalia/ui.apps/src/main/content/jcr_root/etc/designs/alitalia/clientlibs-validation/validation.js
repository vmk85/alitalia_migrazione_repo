$(window).ready(initForms);

function initForms() {
    $('input[data-validation],select[data-validation]').off('blur', formAddValidation);
    $('input[data-validation],select[data-validation]').blur(formAddValidation);
}


function formAddValidation() {
    var elementSeparator = "#;#";
    var functions = $(this).attr("data-validation");
    console.log("formAddValidation in validation.js functions: ",functions);
    functions = functions.split(elementSeparator);
    var messages = $(this).attr("data-messages");
    messages = messages.split(elementSeparator);
    var data = {
    	error : false,
    	fields : []
    };
    var k;
    for (k=0; k<functions.length; k++) {
        var fun = functions[k];
        var parameter = $(this).val();
        var result = validate(fun, parameter);
        if (!result) {
        	data.error = true;
            var key = $(this).attr("name");
            data.fields[key] = messages[k];
        }
    }
    if (data.error) {
    	showErrors(data);
    	if(typeof window["analytics_trackBookingErrors"] === "function"){
			window["analytics_trackBookingErrors"](data.fields);
		}
    	else if(typeof window["analytics_trackCheckinErrors"] == "function"){
			analytics_trackCheckinErrors(data.fields);
		}
    }
    else {
    	removeErrors();
    }
}

function validate(fun, parameter) {
    var _f = window[fun];
    if (typeof _f == 'function') {
		return _f(parameter);
    }
}

function isIn(value, allowedValues) {
	for (k=0; k<allowedValues.length; k++) {
		if (value == allowedValues[k]) {
			return true;
		}
	}
	return false;
}


function isNotEmpty(value) {
    console.log("isNotEMpty in validation.js value: ",value);
	return value!=null && value!=undefined && value!="";
}

function isNumber(value) {
	var regex = new RegExp("^[0-9]*$");
	return value == null || regex.test(value);
}


function isNumberWithSpace(value) {
	var regex = new RegExp("^[0-9\\s]*$");
	return value == null || regex.test(value);
}


function isNumberWithDecimalPrecision(value) {
	var regex = new RegExp("^[0-9]+|[0-9]+[.,][0-9]{1,2}");
	return value == null || regex.test(value);
}

function isNumberExtended(value) {
	var regex = new RegExp("^[0-9\\-]*$");
	return value == null || regex.test(value);
}


function isAmount(value) {
	var regex = new RegExp("^\\d+(\\,{0,1}\\d{2})$");
	return value == null || regex.test(value);
}


function isInvoiceCode(value) {
	var regex = new RegExp("^[a-zA-Z0-9?!()-]*$");
	return value == null || regex.test(value);
}


function isAlphabetic(value) {
	var regex = new RegExp("^[a-zA-Z]*$");
	return value == null || value == "" || regex.test(value);
}


function isAlphabeticWithSpaces(value) {
	var regex = new RegExp("^[a-zA-Z\\s]*$");
	return value == null || value == "" || regex.test(value);
}


function isAlphabeticWithAccents(value) {
	var regex = new RegExp("^[a-zA-ZàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝ]*$");
	return value == null || value == "" || regex.test(value);
}


function isAlphabeticWithAccentAndSpaces(value) {
	var regex = new RegExp("^[a-zA-Z\\sàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝ]*$");
	return value == null || value == "" || regex.test(value);
}


function isAlphabeticWithAccentAndSpacesExtended(value) {
	var regex = new RegExp("^[a-zA-Z\\sàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝ\\-!?\\(\\)\\'\\.]*$");
	return value == null || value == "" || regex.test(value);
}


function isAlphaNumeric(value) {
	var regex = new RegExp("^[a-zA-Z0-9]*$");
	return value == null || regex.test(value);
}


function isEmail(value) {
	var regex = new RegExp("^[A-Za-z0-9._-]+@[A-Za-z0-9._-]+\\.[A-Za-z]{2,8}$");
	return value == null || regex.test(value);
}

function isAlphaNumericNotOnlyNumber(value){
    var regex = /^[a-z]+[\w]{8,40}/;
	return value == null || regex.test(value);
}

function isPassword(value){
    var regex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})");
    return value == null || regex.test(value);
}

function isPasswordStrongAuthentication(value){

    if (value.length < 8)
        return false; //alert("bad password");

    var hasUpperCase = /[A-Z]/.test(value);
    var hasLowerCase = /[a-z]/.test(value);
    var hasNumbers = /\d/.test(value);
    //var hasNonalphas = /[@#\-_!£]/.test(value);
    var hasNonalphas = (/\W/.test(value) || value.indexOf('_')>-1 );
    if (hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas < 4)
        return false; //alert("bad password");
    else
        return true;

}

function isPromoCode(value) {
	var regex = new RegExp("^[A-Za-z]{2}[0-9]{8}$");
	return value == null || regex.test(value);
}


function isIataAptCode(value) {
	var regex = new RegExp("^[A-Z]{3}$");
	return value == null || regex.test(value);
}


function isTrue(value) {
	return value!=null && value!=undefined && (value=="true" || value=="True" || value=="TRUE");
}


function isNumeroVolo(value) {
	if (value.length <= 4 && isNumber(value)) {
		return true;
	}
	return false;
}


function isPnrOrTicketNumber(value) {
	if (isAlphaNumeric(value) && value.length==6) {
		return true;
	}
	if (isNumber(value) && value.length==13) {
		return true;
	}
	return false;
}


function isLessThanMaxFrequentFlyers(value, max) {
	var parsedValue = parseInt(value);
	var parsedMax = parseInt(max);
	return (parsedValue <= parsedMax);
}


function isRangeAdults(value) {
	var regex = new RegExp("^[1-7]{1}$");
	return value != null || regex.test(value);
}


function isRangeKids(value) {
	var regex = new RegExp("^[0-6]{1}$");
	return value != null || regex.test(value);
}


function isRangeBabies(value) {
	var regex = new RegExp("^[0-7]{1}$");
	return value != null || regex.test(value);
}


function isRangeAdultForFamily(nadults) {
	var regex = new RegExp("^[2-7]{1}$");
	return value != null || regex.test(value);
}


function isNumeroTelefonoValido(value) {
	var regex = new RegExp("^[0-9]{5,15}$");
	if (isNotEmpty(value)) {
		return regex.test(value);
	}
	return true;
}


function isNomeValido(value) {
	var regex = new RegExp("^[^\\\u00B6\\\"\\\"\\!\\#\\$\\%\\&\\\u00A3\\(\\)\\*\\+\\,\\-\\.\\/\\:\\;\\<\\=\\>\\?\\@\\[\\\\\\]\\^\\_\\`\\{\\|\\}\\~\\\u00A6\\\u00F8\\\u00BF\\\u00AC\\\u00BD\\\u00BC\\\u00AB\\\u00BB\\\u00A6\\+\\-\\\u00A4\\\u00F0\\\u00AF\\\u00FE\\\u00DE\\\u00AF\\\u00B4\\\u00AC\\\u00B1\\=\\\u00BE\\\u00F7\\\u00B8\\\u00A8\\\u20221234567890\u20AC]{0,27}$");
	return value.length>=2 && regex.test(value);
}


function isCodiceBluebizValido(value) {
	var regex = new RegExp("^[a-zA-Z0-9]{0,9}$");
	return regex.test(value);
}

function isCodiceSkyBonusValido(value) {
	var regex = new RegExp("^((US|CA)\\d{7,9}|(ES|ER|EQ)\\d{3}|SME\\d{7})$");
	return regex.test(value);
}


function  isCuitCuilCode(value) {
	if (isNumber(value) && value.length == 11) {
		return true;
	}
	return false;
}


function isMonth(value) {
	var regex = new RegExp("[0-9][0-9]");
	return value!=null && regex.test(value);
}


function isYear(value) {
	var regex = new RegExp("[0-9][0-9][0-9][0-9]");
	return value!=null && regex.test(value);
}


function isValidCodiceFiscaleByConcatNomeCognomeCF(value) {
	value = value.replace(" ","");
	var params = value.split('|');
	nome = params[0];
	cognome = params[1];
	CF = params[2];
	var vocali = ["A", "E", "I", "O", "U"];
	var cogToCompare = "";
	var cont = 0;
	
	if (CF.length<6) {
		return false;
	}
	if (nome == null || nome == undefined || nome == "" || cognome == null || cognome == undefined || cognome == "") {
		return false;
	}
    if (cognome.length<3) {
		cogToCompare += cognome;
		while (cogToCompare.length<3) {
			cogToCompare += "X";
		}
	}
	else {
		var i;
		for (i=0; i<cognome.length; i++) {
			if (cont==3) {
				break;
			}
			if (!isIn(cognome.charAt(i), vocali)) {
				cogToCompare += cognome.charAt(i);
				cont = cont+1;
			}
		}
		
		
		while(cont<3) {
			for (i=0; i<cognome.length; i++) {
				if (cont==3) {
					break;
				}
				if (isIn(cognome.charAt(i), vocali)) {
					cogToCompare += cognome.charAt(i);
					cont = cont+1;
				}
			}
		}
	}
	
	cont = 0;
	var nomToCompare = "";
	if (nome.length<3) {
		nomToCompare += nome;
		while (nomToCompare.length<3) {
			nomToCompare += "X";
		}
		cont=3;
	}
	
	var nomeCom = nome;
	var k;
	for (k=0; k<vocali.length; k++) {
		nomeCom = nomeCom.replace(vocali[k], "");
	}
	if (nomeCom.length>=4) {
		nomToCompare = nomCompare.substring(0,1) + nomCompare.substring(2,4);
	}
	else {
			if (nomeCom.length==3){
				nomToCompare = nomeCom;
			}
			else {
				nomToCompare = nomeCom;
				cont = nomToCompare.length;
				for (k=0; k<nome.length; k++) {
					if (cont == 3) {
						break;
					}
					if (isIn(nome.charAt(i), vocali)) {
						nomToCompare += nome.charAt(i);
						cont = cont+1;
					}
				}
			}
	}
	var cFCog = CF.substring(0, 3);
	var cFNome = CF.substring(3, 6);
	
	return (cFCog == cogToCompare) && (cFNome == nomToCompare);
}


function isPartitaIva(piva) {
	if (isNotEmpty(piva)) {
		var regex = new RegExp("^([0-9]){11}$");
		return regex.test(piva);
	}
	return true;
}


function isCVCFourDigits(cvc) {
	return cvc.length == 4;
}


function isCVCThreeDigits(cvc) {
	return cvc.length == 3;
}


function isAirport(value) {
	if (isAlphabetic(value))
		return true;
	else
		return false;
}


function getDate(value) {
	var params = value.split('|');
	value = params[0];
	var dateFormat = params[1];
	if (dateFormat=="dd/MM/yyyy") {
		var day = value.substring(0,2);
		var month = value.substring(3,5);
		value = month + "/" + day + "/" + value.substring(6);
	}
	return new Date(value);
}


function isBeforeNow(value) {
	var date = getDate(value);
	var today = new Date();
	return (date < today);
}


function isLinguaCorrispondenzaValid(linguaCorrispondenza, nazioneSelezionata) {
	if (nazioneSelezionata == "IT" && linguaCorrispondenza != "Italiano") {
		return false;
	}
	return true;
}


function isDate(value) {
	if (isNotEmpty(value)) {
		var date = getDate(value, dateFormat);
		if (date!=null && date!=undefined && date!="Invalid Date") {
			return true;
		}
	}
	return false;
	
}


function isFuture(value) {
	if (isNotEmpty(value)) {
		var date = getDate(value);
		var today = new Date();
		return (date > today);
	}
	return false;
}




function isBeforeTodayPlusSevenDay(value) {
	var date = getDate(value);
	var today = new Date();
	var todayPlusSeven = new Date();
	todayPlusSeven.setDate(today.getDate() + 8);
	return (date>today && date<todayPlusSeven);
}


function isValidFlightDate(value) {
	if (isNotEmpty(value)) {
		var date = getDate(value);
		var today = new Date();
		var lastDay = new Date();
		lastDay.setDate(today.getDate() + 7);
		var firstDay = new Date();
		firstDay.setDate(today.getDate() - 3);
		if (date>firstDay && date<lastDay) {
			return true;
		}
	}
	return false;
}


function isBeforeToday(value) {
	var date = getDate(value);
	var today = new Date();
	return (date < today);
}

function isMMUserName(value) {
    if(!/[^a-zA-Z0-9]/.test(value)) {
    var regex = new RegExp("^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[A-Za-z0-9]).{8,40}$"); //almeno 1 lettera almeno un numero nimimo 8 massimo 40
    return value!=null && regex.test(value);
    }

    else return false
}