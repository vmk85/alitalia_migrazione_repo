function isMilleMigliaAuthenticated(){
    var isAuthenticated = false;
	var profileStore = CQ_Analytics.ClientContextMgr.getRegisteredStore('profile');
	if (profileStore) {
		var isAlitaliaConsumerUser = profileStore.data.isAlitaliaConsumerUser;
		isAuthenticated = (isAlitaliaConsumerUser == true || isAlitaliaConsumerUser == 'true');
	} else {
		console.error("Cannot apply profiledata , profile data store not found or not yet ready.");
	}
	return isAuthenticated;
}

// function isGigyaAuthenticated(){
//     gigya.accounts.getAccountInfo({callback:function(ev){console.log(ev)}});
// }