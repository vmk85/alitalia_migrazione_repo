var manageRevealOptions = {};

var idx = 0;

var mailType = "";

var validateEmailOnKeyPress = false;

var params = {}, queries, temp, i, l;
// Split into key/value pairs
queries = window.location.search.split("&");
// Convert the array of strings into an object
for ( i = 0, l = queries.length; i < l; i++ ) {
    temp = queries[i].split('=');
    params[temp[0]] = temp[1];
}
var url = parseUri(window.location.href);
var editBp = params["editBp"];
var flight = params["flight"];

var sendEmailLoading = false;
var sendSmsLoading = false;


$(document).ready( function() {

    $('.reveal-wrap.getBoardingPassError').foundation();

    if (url.queryKey.payment =="true"){
        $("#mailSend").show();
    }

    if (editBp == "true"){
        $("#tornaGestioneCarte").removeClass("hide");
        $("#form-gestisci input#flightitinerary").val(flight);
    } else {
        $("#tornaGestioneCarte").addClass("hide");

    }

//quando si arriva nella thank-you page significa che il check-in è stato completato
//quindi non serve più fare l'offload, anche se l'utente chiude la pagina web prima di
//aver scaricato la carta d'imbarco: validNavigation = true appena si atterra in pagina.
    validNavigation = true;

    //$('#btPrintPdf').bind('click', printPdf(0));

     var is_mobile = navigator.userAgent.match(/iPhone/i)
        if (is_mobile){
        $('.icon--close').on('click', function() {
            $('.reveal-overlay').css('display','none');
            $('html').removeClass('is-reveal-open');
        });
        }

    $('#btSendEmail').bind('click', sendEmail);

    $(".bntWaveCheckinPnr.button.button--light").click(function(){
        cercaPnr();
    });

    $(".print").click(function(){

        var idx = $(this).attr("idx");
        var employee = $(this).hasClass("emp_true");
        countBPprinted();

        printPdf(idx,employee);

    });

    $(".sendAll").click(function(){
        idx = $(this).attr('idx');
        mailType = "All";
    });

    $(".sendSingle").click(function(){
        idx = $(this).attr('idx');
        var name_surname = $('.passenger_'+idx).html();
        $('#form-pdf-' + idx).append('<input type="hidden" name="name_surname" value="' + name_surname + '">');
        mailType = "Single";
    });

    $(".sendSingleRecap").click(function(){
        idx = $(this).attr('idx');
        var name_surname = $('.passenger_'+idx).html();
        $('#form-pdf-' + idx).append('<input type="hidden" name="name_surname" value="' + name_surname + '">');
        $('#form-pdf-' + idx).append('<input type="hidden" name="recap">');
        mailType = "Single";
    });

    $(".sendAllRecap").click(function(){
        idx = $(this).attr('idx');
        mailType = "All";
        $('#frmMultiBoards').append('<input type="hidden" name="recap">');
    });

    $('input#btSendMail').click(function(){
        var emailList = [];
        var elList = $(this).parents(".send").find(".mailList").children();
        $.each(elList, function(i, el) {
            var mail = $(el).find(".mailAddress").text();
            emailList.push(mail);
        });
        $(".mails").html(emailList.join("; "));
        if (mailType == "Single") {
            sendEmailLoading = true;
            performSubmit('getboardinigpassinfobyname','#form-pdf-' + idx, boardingPassSuccess, boardingPassFail);
            var tels = [];
            $(".telList-"+idx).find("span.telNumber").each(function(){
                tels.push($(this).html());
            });
            if(tels.length > 0)
            {
                $("#tel-"+idx).val(tels.toString());
                sendSmsLoading = true;
                performSubmit('checkisendmoreboardyngpassbyphone','#formSendSmd-' + idx, sendTelSuccess, sendTelFail);
            }
            enableLoaderSendAddress();
        }
        if (mailType == "All") {
            sendEmailLoading = true;
            performSubmit('getmoreboardinigpassinfobyname','#frmMultiBoards', boardingPassMultipleSuccess, boardingPassMultipleFail);
            var tels = [];
            $(".telList-all").find("span.telNumber").each(function(){
                tels.push($(this).html());
            });
            if(tels.length > 0)
            {
                $("#tel-all").val(tels.toString());
                sendSmsLoading = true;
                performSubmit('checkisendmoreboardyngpassbyphone','#frmSendSmsAll', sendTelSuccess, sendTelFail);
            }
            enableLoaderSendAddress();
        }

    });

    function sendTelSuccess(data)
    {
        if(!sendEmailLoading) {
            disableLoaderSendAddress();
            $('.reveal--manage-send-address').parent().css('display','none');
            $("#openPanelSend").trigger("click");

        }
        sendSmsLoading = false;
    }

    function sendTelFail()
    {
        if(!sendEmailLoading) {
            disableLoaderSendAddress();
            $('.reveal--manage-send-address').parent().css('display','none');
            $("#openPanelSend").trigger("click");
        }
        sendSmsLoading = false;
    }


    $("#btPrintAllPdf").click(function(){

        countBPprinted();
        $(".seat_to_set").each(function(){

            var _pid = $(this).data("pid-x");
            var _origin = $(this).data("origin-x");
            var _val = $("input[data-pid='" + _pid+ "'][data-origin='" + _origin+ "']").val();
            $(this).val(_val);

        });
        enableLoaderCheckin();
        performSubmit('getmoreboardinigpassinfobyname','#frmMultiBoards', printMultiPdfSuccess, printMultiPdfFail);

    });


    $(".manage-dropdown").on("click", function() {
        var dropP = $(this).attr('data-toggle');
        var $dropdown = new Foundation.Dropdown( $('#'+ dropP));
        $dropdown.close();
    });

    $(".manage-toggle").on("click", function() {
        var toggleP = $(this).attr('data-toggle');
        var $toggle = new Foundation.Toggler( $('#'+ toggleP));
    });


    $('.details__pane').on('on.zf.toggler',function(e){
        var trigRef= $(this).attr('id');
        $('a[data-toggle='+trigRef+']').closest('.row--heading').removeClass('details-expanded');
        $('a[data-toggle='+trigRef+']').removeClass('action__icon--arrow-up').addClass('action__icon--arrow-down');
        $('a[data-toggle='+trigRef+'] .toggle--open').hide();
        $('a[data-toggle='+trigRef+'] .toggle--closed').show();
    });
    $('.details__pane').on('off.zf.toggler',function(e){
        var trigRef= $(this).attr('id');
        $('a[data-toggle='+trigRef+']').closest('.row--heading').addClass('details-expanded');
        $('a[data-toggle='+trigRef+']').addClass('action__icon--arrow-up').removeClass('action__icon--arrow-down');
        $('a[data-toggle='+trigRef+'] .toggle--open').show();
        $('a[data-toggle='+trigRef+'] .toggle--closed').hide();
    });

    manageRevealOptions.reveals = [];
//    manageRevealOptions.reveals.sendAddress =  new Foundation.Reveal( $( '.reveal--manage-send-address' ), {
//        // animationIn: 'fade',
//        // animationOut: 'fade',
//    } );
//    manageRevealOptions.reveals.sendAddressdeleteCheckin =  new Foundation.Reveal( $( '.reveal--manage-delete-checkin' ), {
//        // animationIn: 'fade',
//        // animationOut: 'fade',
//    } );

    	manageRevealOptions.reveals = [];
    //	manageRevealOptions.reveals.sendAddress =  new Foundation.Reveal( $( '.reveal--manage' ), {
    //		// animationIn: 'fade',
    //		// animationOut: 'fade',
    //	} );
    //	manageRevealOptions.reveals.sendAddressdeleteCheckin =  new Foundation.Reveal( $( '.reveal--manage-delete-checkin' ), {
    //		// animationIn: 'fade',
    //		// animationOut: 'fade',
    //	} );

    	if ($( '#manage-send-address-all' ).length) {
    		$( '#manage-send-address-all' ).each(function(i, el){
    			var singleReveal = new Foundation.Reveal( $( el ), {
    		// animationIn: 'fade',
    		// animationOut: 'fade',
    	} );
    			manageRevealOptions.reveals.push(singleReveal);
    	} );
    	}

    	manageRevealOptions.reveals.push(new Foundation.Reveal($("#manage-send-address-success"), {}));

    	if ($( 'div[idx]' ).length) {
    		$( 'div[idx]' ).each(function(i, el){
    			var singleReveal = new Foundation.Reveal( $( el ), {
    		// animationIn: 'fade',
    		// animationOut: 'fade',
    	} );
    			manageRevealOptions.reveals.push(singleReveal);
    	} );
    	}

//    $(".removeTel").bind("click", function(){
//        $(this).remove();
//    });
//
//    $(".removeMail").bind("click", function(){
//        $(this).remove();
//    });

    $(".addMail").on("click", function(){
        var idx = $(this).attr('idx');
        addMail(idx);
        checkSendList($(this).parent());
    });

    $(".addTel").on("click", function(){
        var idx = $(this).attr("idx");
        addTel(idx);
        checkSendList($(this).parent());
    });


    $('.addTelGestioneKeyUp').keyup(function(e)
    {
        if(e.keyCode == 13)
        {
            var idx= $(this).attr("idx");
            addTel(idx);
            checkSendList(this);
        }
    });

    $(".addTelGestione").on("click", function(e){
        var idx= $(this).attr("idx");
        addTel(idx);
        checkSendList(this);
    });

    $('#txtEmail').keyup(function(e)
    {
        if(e.keyCode == 13)
            addMail();
    });

    $('#txtEmail').keyup(function(e)
    {
        if(validateEmailOnKeyPress){
            var email = $(this).val();

            if (email != ''){
                if(isEmail(email)){
                    $('#warning-email').addClass('hide');
                    $('#txtEmail').removeClass('is-invalid-input');
                }else{
                    $('#txtEmail').addClass('is-invalid-input');
                    $('#warning-email').removeClass('hide')
                }
            }else{
                $('#warning-email').addClass('hide');
                $('#txtEmail').removeClass('is-invalid-input');
            }
        }
    });

    getCheckinComplete();

});

function checkSendList(element) {
    var el = $(element).parents(".send");
    var telList = el.find(".telList");
    var mailList = el.find(".mailList");
    el.find("input#btSendMail").prop("disabled", !(telList.children().length > 0 || mailList.children().length > 0));
}

function removeMailToken(tokenId)
{
    var parent = $("#" + tokenId).parent();
    $("#" + tokenId).remove();
    checkSendList(parent);
}

function removeTel(el)
{
    var parent = $(el).parent();
    el.remove();
    checkSendList(parent);
}

function addMail(idx)
{
    var email = $("#txtEmail_"+idx).val();
    if (email != ''){
        if(isEmail(email)){
            $('#warning-email').addClass('hide');
            $('#txtEmail_'+idx).removeClass('is-invalid-input');
            var rnd = Math.random().toString().replace(".","");
            var str = '<button id="' + rnd + '" onclick="javascript:removeMailToken(\'' + rnd + '\');" class="removeMail">'
            str += '<span class="mailAddress">' + email + '</span>';
            str += '<span class="show-for-sr removeMail2">Elimina</span>';
            str += '</button>';
            $(".mailList_"+idx).append(str);
            $("#txtEmail_"+idx).val('');
            validateEmailOnKeyPress = false;
        }else{
            validateEmailOnKeyPress = true;
            $('#txtEmail').addClass('is-invalid-input');
            $('#warning-email').removeClass('hide')
        }
    }
}

function addTel(idx){

    var tel = '';
    if (idx != null && idx != '' && idx != undefined)
    {
        tel = $("#txtTel-"+idx).val();
    }
    else
    {
        tel = $("#txtTel-all").val();
    }

    if (tel != '' && $.isNumeric(tel) && Math.floor(tel) == tel)
    {
        var str = '<span onclick="javascript:removeTel(this);" class="removeTel">';
        str += '<span class="telNumber">' + tel + '</span>';
        str += '<span class="show-for-sr removeTel">Elimina</span>';
        str += '</span>';
        if (idx != null && idx != '' && idx != undefined)
        {
            $("#txtTel-"+idx).val("");
            $(".telList-"+idx).append(str);
            $("#txtTel-"+idx).removeClass('is-invalid-input');
            $("#warning-tel-"+idx).addClass('hide');
        }
        else
        {
            $("#txtTel-all").val("");
            $(".telList-all").append(str);
            $('#txtTel-all').removeClass('is-invalid-input');
            $('#warning-tel-all').addClass('hide');
        }
    }
    else
    {
        if (idx != null && idx != '' && idx != undefined)
        {
            $("#txtTel-"+idx).addClass('is-invalid-input');
            $("#warning-tel-"+idx).removeClass('hide');
        }
        else
        {
            $('#txtTel-all').addClass('is-invalid-input');
            $('#warning-tel-all').removeClass('hide')
        }
    }
}

function enableLoaderSendAddress() {
    $(".loader-send-address").show();
}

function disableLoaderSendAddress() {
    $(".loader-send-address").hide();
}

function boardingPassSuccess(data)
{
    if(!data.isError) {
        countbpSent();
        var mails = [];
        $(".mailList_" + idx).find("span.mailAddress").each(function () {
            mails.push($(this).html());
        });
        $("#mailAddress-" + idx).val(mails.toString());
        enableLoaderSendAddress();
        performSubmit('checkisendboardyngpassbymail', '#form-pdf-' + idx, sendEmailSuccess, sendEmailFail);
    } else {
        disableLoaderCheckin();
        $("#lnkErrorGetBoardingpass").trigger("click");
    }
}

function boardingPassFail()
{
    disableLoaderSendAddress();
    $("#lnkErrorGetBoardingpass").trigger("click");
}

function boardingPassMultipleSuccess(data) {
    if(!data.isError) {
        countbpSent();
        var mails = [];
        $(".mailList_all").find("span.mailAddress").each(function () {
            mails.push($(this).html());
        });
        $("#mailAddress-" + idx).val(mails.toString());
        enableLoaderSendAddress();
        performSubmit('checkisendboardyngpassbymail', '#frmMultiBoards', sendEmailSuccess, sendEmailFail);
    } else {
        $('.reveal--manage-send-address').parent().css('display','none');
        disableLoaderCheckin();
        $("#lnkErrorGetBoardingpass").trigger("click");
    }
}

function boardingPassMultipleFail()
{
    disableLoaderSendAddress();
    $("#lnkErrorGetBoardingpass").trigger("click");
}

function printPdfSuccess(data)
{
    if(!data.isError) {
        countbpSent();
        var url = removeUrlSelector() + ".createpdf.json?q=checkin-1";
        window.location.assign(url);
    } else {
        $("#lnkErrorGetBoardingpass").trigger("click");
    }
    disableLoaderCheckin();
}

function printPdf(idx,employee){

    // $('#btPrintPdf').unbind('click', printPdf);
    enableLoaderCheckin();
    if(employee){
        performSubmit('getboardinigpassinfobyname','#form-pdf-' + idx,printPdfSuccess,printPdfFail);
    }else{
        performSubmit('getboardinigpassinfo','#form-pdf-' + idx,printPdfSuccess,printPdfFail);
    }
}

function printPdfFail()
{
    disableLoaderCheckin();
    $("#lnkErrorGetBoardingpass").trigger("click");
}

function printMultiPdfSuccess(data)
{
    if(!data.isError) {
        countbpSent();
        var url = removeUrlSelector() + ".createpdf.json?q=checkin-1";
        window.location.assign(url);
    } else {
        $("#lnkErrorGetBoardingpass").trigger("click");
    }
    disableLoaderCheckin();
}

function printMultiPdfFail()
{
    disableLoaderCheckin();
    $("#lnkErrorGetBoardingpass").trigger("click");
}

function sendEmail(){
    $('#btSendEmail').unbind('click', sendEmail);
    performSubmit('#','#form-email', sendEmailSuccess, sendEmailFail);
}

function sendEmailSuccess(data)
{
    if(!data.isError) {
        if (!sendSmsLoading) {
            disableLoaderSendAddress();
            $('.reveal--manage-send-address').parent().css('display', 'none');
            $("#openPanelSend").trigger("click");
        }
        sendEmailLoading = false;
    } else {
        disableLoaderCheckin();
        $("#lnkErrorGetBoardingpass").trigger("click");
    }
}

function sendEmailFail(data)
{
    if(!sendSmsLoading) {
        disableLoaderSendAddress();
        if (data == null || !data.isError){ $("#openPanelSend").trigger("click");$('.reveal--manage-send-address').parent().css('display','none');}
        /*$('#divError').show();
        $('#labelCheckinError').text('Errore (servlet FAIL) ');
        $('#btProcediSubmit').bind('click', savePassengers);*/
    }
    sendEmailLoading = false;
    disableLoaderCheckin();
    $("#lnkErrorGetBoardingpass").trigger("click");
}

function performValidation(service, form, done, fail, always, selector) {
    var additionalParams = {
        '_action' : 'validate'
    };
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector, additionalParams);
}

function performSubmit(service, form, done, fail, always, selector) {
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector);
}

function validation(e, service, form, done, fail, always, selector) {
    e.preventDefault();
    return performValidation(service, form, done, fail, always, selector);
}

function validationFormFile(e, service, form, done, fail, always, selector,
                            additionalParams) {
    e.preventDefault();
    additionalParams._action = "validate";
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector, additionalParams);
}

function getDropdownData(requestData, done, fail, always) {
    return invokeGenericFormService('staticdatalistservlet', 'GET', '', done,
        fail, always, 'json', requestData);
}

function getServiceUrl(service, selector, secure) {
    if (selector == 'login') {
        return service;
    }
    selector = selector || "json";
    var protocol = location.protocol;
    if (jsonProtocol && (selector == "json" || secure)) {
        protocol = 'https:';
    }
    if(service == 'menu-millemiglia-header'){
        return protocol + '//' + location.host + internalUrl1 + "."
            + service + "." + selector;
    } else{
        return protocol + '//' + location.host + internalUrl1 + "/."
            + service + "." + selector;
    }
}

function removeUrlSelector() {
    var url = location.pathname;
    url = url.substr(0, location.pathname.indexOf('.'));
    return location.protocol + '//' + location.host + url;
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex
        .exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g,
        " "));
}

function goToGestioneCarte(){
    refreshDataLayer();
    gestisciPnr();
}

function showErrors(data, noanchor, checkboxnew) {
    removeErrors();
    for (var key in data.fields) {
        var selector = '[name="' + key + '"]';
        if ($(selector).css('display') == 'none' && $(selector).closest(".j-showCaptcha").lenght == 0) {
            selector = $(selector).next();
        }
        $(selector).attr("aria-invalid", "true");
        $(selector).attr("aria-describedby", key + '_error' );
        var $contElement = $(selector).parent();
        if ($(selector).prop("tagName") == 'SELECT' ||
            ($(selector).attr("type") == 'checkbox' && checkboxnew)) {
            $contElement = $(selector).parent().parent();
        }
        var linkClass = "form__errorIcon";
        var msgClass = "form__alert form__errorField";
        if ($(selector).attr("type") === "checkbox") {
            linkClass = linkClass + " left";
            msgClass = msgClass + " left";
        }
        $contElement.addClass("isError");
        $($contElement).append(
            '<a class="' + linkClass +
            ($(selector).attr("type") == 'checkbox' && checkboxnew ? '" style="display:none' : '') +
            '"></a><div id="' + key + '_error'
            + '" class="' + msgClass + '">'
            + data.fields[key] + '</div>');
    }

    // $('[aria-invalid="true"]:first').focus();
    removePopups();

    if (!noanchor) {
        if ($('.isError').length > 0) {
            $(".isError").first().animateAnchor(-32, 400);
        }
    }
    return false;
}