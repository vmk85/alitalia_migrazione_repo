function checkInDone(){
	bookingReady();
	$(".j-radioButtonCard").on("click", function() {
		removeErrors();
		$(".j-radioButtonCard").prop("checked", false);
		$(this).closest('form')[0].reset();
		showCountriesUSA();
		$(this).prop("checked", true);
		$(".bookInfoBoxBasketBtn").removeClass("isDisabled");
	});
	$(".j-wantInvoice").on("click", function() {
		removeErrors();
	});
	$('input[name="tipologiaCarta"]').change(function() { 
		manageCreditCards(this); 
	});
	$('select[name="tipologiaCarta"]').change(function() { 
		manageCreditCards(this); 
	});
	manageCreditCards();
	
	$("#paese").off('change',showCountriesUSA);
	$("#paese").on('change',showCountriesUSA);
	
	$('.bookInfoBoxBasketBtn').on('click', acquistaDaCarrello);

	$("#booking-acquista-cdc-submit").on("click", acquistaCDCSubmit);
	
	/*Non svuta il carrello cliccando sul tasto indietro*/
	$("#paymentBackButton").click(function(){
		window["cancelCart"] = false;
	});
	
	$("[data-tooltip]").each(function() {
	    var a = $(this).attr("data-tooltipAuto"), b = "mouseenter focus", c = "mouseleave blur";
	    Modernizr.touch && (b = "click", c = "click"), a && (b = null, c = null), $(this).qtip({
	        metadata: {
	            type: "attr",
	            name: "qtipOpts"
	        },
	        content: {
	            text: function d(a) {
	                var d = $(this).attr("data-tooltip"), b = $("<div/>").html(d);
	                return b;
	            },
	            title: $(this).attr("data-tooltipTitle")
	        },
	        style: "dark",
	        position: {
	            viewport: !0,
	            effect: !1
	        },
	        show: {
	            event: b
	        },
	        hide: {
	            event: c
	        },
	        style: {
	            tip: {
	                corner: !0
	            }
	        }
	    });
	});
	$(".bookingPayment").animateAnchor();
}

function acquistaDaCarrello(e) {
	if (!$(this).hasClass("isDisabled") && (metodoPagamento = $(".j-radioButtonCard:checked")).length == 1) {
		$('.bookInfoBoxBasketBtn').off("click", acquistaDaCarrello);
		var form = metodoPagamento.closest("form");
		if (form.is("#booking-acquista-cdc-form")) {
			acquistaCDCSubmit(e);
		}
	}
}

function acquistaCDCSubmit(e) {
	startPageLoader(true);
	$("#booking-acquista-cdc-submit").off("click", acquistaCDCSubmit);
	validation(e, "checkinpaymentconsumer", "#booking-acquista-cdc-form", function(data) { 
		donePurchaseData(data, "booking-acquista-cdc-form");
	}, failValidationBookingPurchase);
}


function failValidationBookingPurchase(){
	window.location.replace($("#redirectFailureValidationPage").val());
}

function donePurchaseData(data, selector) {
	removeErrors();
	$("#genericErrorMessageDiv").hide();
	if (data.result) {
		doPayment(selector);
	} else {
		showErrors(data);
		riattivaForm(selector);
		/*Tracking errors*/
		if(typeof window["analytics_trackCheckinErrors"] == "function"){
			analytics_trackCheckinErrors(data.fields);
		}
	}	
}

function riattivaForm(selector){
	if (selector == "booking-acquista-cdc-form") {
		$("#booking-acquista-cdc-submit").on("click", acquistaCDCSubmit);
	}
	startPageLoader(false);
}

function doPayment(selector) {
	// start waiting area
	resultsLoading.init();
	performSubmit('checkinpaymentconsumer', '#' + selector, function(data) {
		doneDoPayment(data, selector);
	}, failDoPayment);
}

function doneDoPayment(data, selector) {
	if (data.redirect) {
		/*Non svuota il carrello se il pagamento è andato a buon fine*/
		window["cancelCart"] = false;
		window.location.replace(data.redirect);
	} else if (!data.result) {
		riattivaForm(selector);
		if (data.message) {
			$("#genericErrorMessageDiv .genericErrorMessage__text").text(data.message);
			$("#genericErrorMessageDiv").show();
			/*Tracking errors*/
			if(typeof window["analytics_trackCheckinErrors"] == "function"){
				analytics_trackCheckinErrors(data.message);
			}
		}
		hideWaiting(data);
	} else {
		hideWaiting(data);
	}
}

function failDoPayment() {
	window.location.replace($("#redirectFailurePage").val());
}

function hideWaiting(data) {
	$('.waitingPage').hide();
}

function manageCreditCards(e) {
	if (e && $(e) && $(e).val() && $(e).val() == 'AX') {
		$('div.cvc input#cvc').attr('maxlength', 4);
		$('.extrafields').show();
		$('.hide-uatp').show();
	} else if (e && $(e) && $(e).val() && $(e).val() == 'TP') {
		$('.extrafields').hide();
		$('.hide-uatp').hide();
	} else {
		$('div.cvc input#cvc').attr('maxlength', 3);
		$('.extrafields').hide();
		$('.hide-uatp').show();
	}
}

function showCountriesUSA(){
	var elem = $("#paese option:selected").val();
	if(elem == "US") {
		$("#extraFieldCountriesUSA").show();
	} else {
		$("select[name='countriesUSA']").val("");
		$("#extraFieldCountriesUSA").hide(); 
	}
}