"use strict";
use(function() {
    try {
        var accounts = this.accounts;
        var identities = this.info;
        var social = ["googleplus", "facebook", "linkedin"];
        var resp = { connected:[], notConnected:[]};
        var img = "";
        var nick = "";
        accounts = accounts.split(",");
        var found = false;
        for(var j in social) {
            for(var i in accounts) {
                if(accounts[i] != "site") {
                    if(accounts[i] == social[j]) {
                        found = true;
                        for(var k in identities) {
                            if(identities[k].provider == social[j]) {img = identities[k].photoURL; nick = identities[k].nickname};
                        }
                    }
                }
            }
            var icon;
            if(social[j] == "facebook") icon = "fb";
            else if(social[j] == "googleplus") icon = "gp";
            else if(social[j] == "linkedin" || social[j] == "twitter") icon = "in";
            if(!found) { resp.notConnected.push({name: social[j], icon: icon});}
            else resp.connected.push({name: social[j], icon: icon, img: img, nickname: nick});
            found = false;
        }
        return resp;
    } catch (err) {
        return {
            notConnected: [{
                    name: "googleplus",
                    icon: "gp"
                }, {
                    name: "facebook",
                    icon: "fb"
                }, {
                    name: "linkedin",
                    icon: "in"
            }]
        }
    }
})