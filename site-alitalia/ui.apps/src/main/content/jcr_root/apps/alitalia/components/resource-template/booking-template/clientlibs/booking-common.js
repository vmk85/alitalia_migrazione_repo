function getCurrentDate() {
	return new Date().getTime();
}

function checkBookingNavigationProcess(destPhase, done, always) {
	var baseUrl = removeUrlSelector();
	var residency = getParameterByName("residency");
	$.ajax({
		url: baseUrl + ".bookingchecknavigationprocessconsumer.json",
		data : "destPhase=" + destPhase + "&nocache=" + getCurrentDate() + "&residency=" + residency,
		method: "GET",
		context: document.body
	})
	.done(
		function(data) {
			if (data.redirect) {
				window.location.replace(data.redirect);
			} else {
				if (done) {
					done();
				}
			}
		})
	.fail(
		function() {
			console.error("Error invoking check navigation service.");
		})
	.always(
		function() {
			if (always) {
				always();
			}
		}
	);
}

function initDatePicker() {
	var cursorDateArr=$('#departureDate').val();
	var cursorDateReturn=$('#arrivalDate').val();
	datePickerController.createDatePicker({
		formElements : {
			andata : pageSettings.dateFormat
		},
		cursorDate: cursorDateArr,
		noTodayButton : !0,
		fillGrid : !0,
		rangeLow : new Date(),
		rangeHigh : maxRange,
		noFadeEffect : !0,
		constrainSelection : !1,
		wrapped : !0,
		callbackFunctions : {
			dateset : [ openAndSetReturn ],
		}
	}), datePickerController.createDatePicker({
		formElements : {
			ritorno : pageSettings.dateFormat
		},
		cursorDate: cursorDateReturn,
		noTodayButton : true,
		fillGrid : !0,
		rangeLow : new Date(),
		rangeHigh : maxRange,
		constrainSelection : !1,
		noFadeEffect : !0,
		wrapped : !0,
		callbackFunctions : {
			dateset : [ openToolStage ]
		}
	})
}

function initSlider() {
	var sliderRange = $('.js-sliderRange');
	if (sliderRange.length) {
		sliderRange.each(function (i, el) {
			new SliderRange($(el));
		});
	}
}

function initSeatMap(){
	var __bookingSeat = $('.chooseSeat__container');
	if (__bookingSeat.length > 0){
		for (var j=0;j<__bookingSeat.length;j++){
			new BookingSeat($(__bookingSeat[j]));
		}
	}
	if ($('.j-chooseSeatSlider').length > 0){
		bookingSeatSlider.init();
	}
}

function invokeService(selector, extension, data, done, fail) {
	settings = {
		url: removeUrlSelector() + "." + selector + "." + extension,
		context : document.body
	};
	
	
	if (!data && extension == "json") {
		data = {
			nocache : getCurrentDate()
		}
	} else if (data && extension == "json") {
		data["nocache"] = getCurrentDate();
	}
	
	if (data) {
		settings["data"] = data;
	}
	
	$.ajax(settings).done(function(data) {
		if (done) {
			done(data);
		}
	})
	.fail(function() {
		if (fail) {
			fail();
		}
	});
}

$(document).ready(function(){
	
	$.ajaxSetup({ cache: false });
});
