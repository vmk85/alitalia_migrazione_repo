function initMmbAncillaryPartials() {
	initPartialMmbCart();
	initPartialMmbInsurance();
	initPartialMmbSeat();
	initPartialMmbMeal();
	initPartialMmbExtraBaggage();
	initPartialMmbFastTrack();
	initPartialMmbLounge();
}

/**
 * function to manage the removing of all specified ancillary
 * by deselecting all checkbox and click on Confirm
 * It is used from extraBaggage, but in can be used by fastTrack and Lounge
 */
function performLastBaggageSelectedOperation(section, addToCart, callBackRemoveFromCart) {
	var allCheckBox = section.find(".extraBagage_detail .placeCheck + input");
	var buttonConfirm = section.find(".fifthButton.j-upsellingBtn.j-next.j-addToCart");
	if (allElementsAreNotSelected(allCheckBox)) {
		buttonConfirm.attr("data-call",callBackRemoveFromCart);
	} else {
		buttonConfirm.attr("data-call",addToCart);
	}
}
function allElementsAreNotSelected(allCheckBox){
	var count = 0;
	allCheckBox.each(function(index, elem) {
		var checkBox = $(elem);
		if (checkBox.val().split("#")[1] == 0) {
			count++;
		}
	});
	return count == allCheckBox.length;
}

/* partial invoke functions */

function invokePartialMmbAncillary(partialName, displayFeedbackStep, done, fail) {
	data = {};
	if (displayFeedbackStep) {
		data.displayfeedbackstep = "1";
	}
	$.ajax({
		url: removeUrlSelector() + "." + partialName,
		data: data,
		context : document.body
	})
	.done(function(data) { if (done) { done(data); }})
	.fail(function() { if (fail) { fail(); }});
}

function invokePartialMmbCart(done, fail) {
	invokePartialMmbAncillary("cart-partial.html", false, done, fail);
}

function invokePartialMmbInsurance(displayFeedbackStep, done, fail) {
	invokePartialMmbAncillary("box-insurance-partial.html", displayFeedbackStep, done, fail);
}

function invokePartialMmbSeat(displayFeedbackStep, done, fail) {
	invokePartialMmbAncillary("box-seat-partial.html", displayFeedbackStep, done, fail);
}

function invokePartialMmbMeal(displayFeedbackStep, done, fail) {
	invokePartialMmbAncillary("box-meal-partial.html", displayFeedbackStep, done, fail);
}

function invokePartialMmbFastTrack(displayFeedbackStep, done, fail) {
	invokePartialMmbAncillary("box-fasttrack-partial.html", displayFeedbackStep, done, fail);
}

function invokePartialMmbExtraBaggage(displayFeedbackStep, done, fail) {
	invokePartialMmbAncillary("box-extrabaggage-partial.html", displayFeedbackStep, done, fail);
}

function invokePartialMmbLounge(displayFeedbackStep, done, fail) {
	invokePartialMmbAncillary("box-lounge-partial.html", displayFeedbackStep, done, fail);
}


/* partial refresh functions */

function refreshMmbPartialCart(showTooltip) {
	invokePartialMmbCart(function(data) {
		$(".mmb-partial-cart").replaceWith(data);
		initPartialMmbCart();
		if(typeof window['initMmb_tracking'] == "function"){
			initMmb_tracking();
		}
		bookingReady();
		if (showTooltip) {
			upsellingsManager.fastShowTooltip();
		}
		$('.j-responsiveTable').responsiveTable({
			  maxWidth: 640
		});
	});
}

function refreshMmbPartialInsurance(displayFeedbackStep, showTooltipCart) {
	invokePartialMmbInsurance(displayFeedbackStep, function(data) {
		$(".mmb-partial-insurance").replaceWith(data);
		upsellingsManager.initStep($(".mmb-partial-insurance").find(".j-upselling"));
		initPartialMmbInsurance();
		if(typeof window['initMmb_tracking'] == "function"){
			initMmb_tracking();
		}
		upsellingsManager.scrollToSection($(".mmb-partial-insurance").find(".j-upselling"));
		refreshMmbPartialCart(showTooltipCart);
	});
}

function refreshMmbPartialSeat(displayFeedbackStep, showTooltipCart) {
	invokePartialMmbSeat(displayFeedbackStep, function(data) {
		$(".mmb-partial-seat").replaceWith(data);
		upsellingsManager.initStep($(".mmb-partial-seat").find(".j-upselling"));
		initPartialMmbSeat();
		if(typeof window['initMmb_tracking'] == "function"){
			initMmb_tracking();
		}
		upsellingsManager.scrollToSection($(".mmb-partial-seat").find(".j-upselling"));
		refreshMmbPartialCart(showTooltipCart);
	});
}

function refreshMmbPartialMeal(displayFeedbackStep, showTooltipCart) {
	invokePartialMmbMeal(displayFeedbackStep, function(data) {
		$(".mmb-partial-meal").replaceWith(data);
		upsellingsManager.initStep($(".mmb-partial-meal").find(".j-upselling"));
		initPartialMmbMeal();
		if(typeof window['initMmb_tracking'] == "function"){
			initMmb_tracking();
		}
		upsellingsManager.scrollToSection($(".mmb-partial-meal").find(".j-upselling"));
		refreshMmbPartialCart(showTooltipCart);
	});
}

function refreshMmbPartialFastTrack(displayFeedbackStep, showTooltipCart) {
	invokePartialMmbFastTrack(displayFeedbackStep, function(data) {
		$(".mmb-partial-fasttrack").replaceWith(data);
		upsellingsManager.initStep($(".mmb-partial-fasttrack").find(".j-upselling"));
		initPartialMmbFastTrack();
		if(typeof window['initMmb_tracking'] == "function"){
			initMmb_tracking();
		}
		upsellingsManager.scrollToSection($(".mmb-partial-fasttrack").find(".j-upselling"));
		refreshMmbPartialCart(showTooltipCart);
	});
}

function refreshMmbPartialExtraBaggage(displayFeedbackStep, showTooltipCart) {
	invokePartialMmbExtraBaggage(displayFeedbackStep, function(data) {
		$(".mmb-partial-extra-baggage").replaceWith(data);
		upsellingsManager.initStep($(".mmb-partial-extra-baggage").find(".j-upselling"));
		initPartialMmbExtraBaggage();
		if(typeof window['initMmb_tracking'] == "function"){
			initMmb_tracking();
		}
		upsellingsManager.scrollToSection($(".mmb-partial-extra-baggage").find(".j-upselling"));
		refreshMmbPartialCart(showTooltipCart);
	});
}

function refreshMmbPartialLounge(displayFeedbackStep, showTooltipCart) {
	invokePartialMmbLounge(displayFeedbackStep, function(data) {
		$(".mmb-partial-lounge").replaceWith(data);
		upsellingsManager.initStep($(".mmb-partial-lounge").find(".j-upselling"));
		initPartialMmbLounge();
		if(typeof window['initMmb_tracking'] == "function"){
			initMmb_tracking();
		}
		upsellingsManager.scrollToSection($(".mmb-partial-lounge").find(".j-upselling"));
		refreshMmbPartialCart(showTooltipCart);
	});
}


/* common error managements functions and callbacks */

function mmbHandleInvokeError(jqXHR, textStatus, errorThrown) {
	startPageLoader(false);
	console.log('MMB invocation failed.');
	console.log(textStatus);
	var errorMessage = getI18nTranslate('booking.service.generic.error');
	mmbDisplayErrorMessage(errorMessage);
}

function mmbHandleServiceError(data) {
	if (data.isError) {
		console.log('MMB service error.');
		console.log(data);
		var errorMessage = "";
		if (data.errorMessage) {
			errorMessage = data.errorMessage;
		} else {
			errorMessage = getI18nTranslate('booking.service.generic.error');
		}
		if (data.errorMessageDetail) {
			errorMessage += ": " + data.errorMessageDetail;
		}
		mmbDisplayErrorMessage(errorMessage);
	} else {
		console.log("Unknown error data for mmbHandleServiceError");
	}
}

function mmbDisplayErrorMessage(errorMessage) {
	$('#mmbErrorContentText').text(errorMessage);
	$('#mmbErrorContent').height('auto');
	$('#mmbErrorContent').velocity('transition.fadeIn');
	$('html, body').animate({scrollTop: 0}, 500);
}
