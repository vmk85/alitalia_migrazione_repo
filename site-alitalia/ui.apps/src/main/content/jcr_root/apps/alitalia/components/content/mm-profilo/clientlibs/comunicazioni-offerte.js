setI18nTranslate('millemiglia.comunicazioniOfferte.error');
setI18nTranslate('millemiglia.comunicazioniOfferte.ajaxError');
setI18nTranslate('millemiglia.comunicazioniOfferte.success');

$('#comunicazioniOfferteSumbit').bind('click', function(e) {
	validation(e, 'comunicazionioffertesubmit', '#comunicazioniOfferteEdit',
			comunicazioniOfferteEditSuccess, comunicazioniOfferteEditError);
	return false;
});

function comunicazioniOfferteEditSuccess(data) {
	if (data.result) {
		removeErrors();
		$('#comunicazioniOfferteSumbit').unbind('click');
		$('#comunicazioniOfferteEdit').submit();
	} else {
		showErrors(data);
		showFormFeedbackError('comunicazioniOfferteEdit',
				'millemiglia.comunicazioniOfferte.error');
	}
	return false;
}

function comunicazioniOfferteEditError(data) {
	showFormFeedbackError('comunicazioniOfferteEdit',
			'millemiglia.comunicazioniOfferte.ajaxError');
	return false;
}

$(window).on("load", function (e) {

	var $accordionComunicazioniOfferte = $('#accordion1_tab4');
	if (resultOfferteComunicazioniSubmit == "true") {
		$accordionComunicazioniOfferte.click();
		showFormFeedbackSuccess('comunicazioniOfferteEdit',
				'millemiglia.comunicazioniOfferte.success');
	} else if (resultOfferteComunicazioniSubmit == "false") {
		$accordionComunicazioniOfferte.click();
		showFormFeedbackError('comunicazioniOfferteEdit',
				'millemiglia.comunicazioniOfferte.ajaxError');
	}
	if (resultOfferteComunicazioniSubmit == "true"
		|| resultOfferteComunicazioniSubmit == "false") {
		$accordionComunicazioniOfferte[0].scrollIntoView();
	}
	return false;
});

/*
$(window).load(function() {
	var $accordionComunicazioniOfferte = $('#accordion1_tab4');
	if (resultOfferteComunicazioniSubmit == "true") {
		$accordionComunicazioniOfferte.click();
		showFormFeedbackSuccess('comunicazioniOfferteEdit',
				'millemiglia.comunicazioniOfferte.success');
	} else if (resultOfferteComunicazioniSubmit == "false") {
		$accordionComunicazioniOfferte.click();
		showFormFeedbackError('comunicazioniOfferteEdit',
				'millemiglia.comunicazioniOfferte.ajaxError');
	}
	if (resultOfferteComunicazioniSubmit == "true"
		|| resultOfferteComunicazioniSubmit == "false") {
		$accordionComunicazioniOfferte[0].scrollIntoView();
	}
	return false;
});
*/
$(document).ready(function() {
	var requestData = {
		'linguaCorrispondenza': defaultSelectedLinguaCorrispondenza
	};
	getDropdownData(requestData, dropdownDataSuccess, dropdownDataFail);
	
	function dropdownDataSuccess(data) {
		var linguaCorrispondenza = data.linguaCorrispondenza; 
		for (index in linguaCorrispondenza) {
			var option = '';
			if (linguaCorrispondenza[index].code == defaultSelectedCountry) {
				option = '<option value="' + linguaCorrispondenza[index].name
					+ '" selected>' + linguaCorrispondenza[index].value + '</option>';
			} else {
				option = '<option value="' + linguaCorrispondenza[index].name + '">'
					+ linguaCorrispondenza[index].value + '</option>';
			}
			$('div.customSelect').find('select#linguaCorrispondenza').append(option);
		}
		return false;
	}
	
	function dropdownDataFail() {
		return false;
	}
	
	return false;
});
