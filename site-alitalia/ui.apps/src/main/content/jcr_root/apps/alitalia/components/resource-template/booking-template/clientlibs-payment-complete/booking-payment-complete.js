// start waiting area
resultsLoading.init();
var token = getURLParameter("token");
console.log('token');
console.log(token);
var additionalParams = {
		'token' : token
	};
invokeGenericFormService('bookingpurchasereturnconsumer', 'GET', '', doneCompletePayment, failCompletePayment, undefined, undefined, additionalParams);

function doneCompletePayment(data) {
	if (data.redirect) {
		parent.location.replace(data.redirect);
	} else if (!data.authorizeAlreadyInvoked){
		failCompletePayment();
	}
}

function failCompletePayment() {
	window.location.replace($("#redirectFailurePage").val());
}