setI18nTranslate('millemiglia.preferenzeViaggio.error');
setI18nTranslate('millemiglia.preferenzeViaggio.ajaxError');
setI18nTranslate('millemiglia.preferenzeViaggio.success');

$('#preferenzeViaggioSumbit').bind('click', function(e) {
	validation(e, 'preferenzeviaggiosubmit', '#preferenzeViaggioEdit',
			preferenzeViaggioEditSuccess, preferenzeViaggioEditError);
	return false;
});

function preferenzeViaggioEditSuccess(data) {
	if (data.result) {
		removeErrors();
		$('#preferenzeViaggioSumbit').unbind('click');
		$('#preferenzeViaggioEdit').submit();
	} else {
		showErrors(data);
		showFormFeedbackError('preferenzeViaggioEdit',
				'millemiglia.preferenzeViaggio.error');
	}
	return false;
}

function preferenzeViaggioEditError(data) {
	showFormFeedbackError('preferenzeViaggioEdit',
			'millemiglia.preferenzeViaggio.ajaxError');
	return false;
}

$(window).on('load', function() {
	var $accordionPreferenzeViaggio = $('#accordion1_tab5');
	if (resultPreferenzeViaggioSubmit == "true") {
		$accordionPreferenzeViaggio.click();
		showFormFeedbackSuccess('preferenzeViaggioEdit',
				'millemiglia.preferenzeViaggio.success');
	} else if (resultPreferenzeViaggioSubmit == "false") {
		$accordionPreferenzeViaggio.click();
		showFormFeedbackError('preferenzeViaggioEdit',
				'millemiglia.preferenzeViaggio.ajaxError');
	}
	if (resultPreferenzeViaggioSubmit == "true" || resultPreferenzeViaggioSubmit == "false") {
		$accordionPreferenzeViaggio[0].scrollIntoView();
	}
	return false;
});


$(document).ready(function() {
	var requestData = {
		'tipoPosto': defaultSelectedTipoPosto,
		'tipoPasto': defaultSelectedTipoPasto
	};
	
	getDropdownData(requestData, dropdownDataSuccess, dropdownDataFail);
	
	function dropdownDataSuccess(data) {
		var tipoPosto = data.tipoPosto; 
		for (index in tipoPosto) {
			var option = '';
			if (tipoPosto[index].code == defaultSelectedTipoPosto) {
				option = '<option value="' + tipoPosto[index].code
					+ '" selected>'
					+ tipoPosto[index].description + '</option>';
			} else {
				option = '<option value="' + tipoPosto[index].code + '">'
					+ tipoPosto[index].description + '</option>';
			}
			$('div.customSelect').find('select#tipoPosto').append(option);
		}
		
		var tipoPasto = data.tipoPasto; 
		for (index in tipoPasto) {
			var option = '';
			if (tipoPasto[index].code == defaultSelectedTipoPasto) {
				option = '<option value="' + tipoPasto[index].code
					+ '" selected>' + tipoPasto[index].description + '</option>';
			} else {
				option = '<option value="' + tipoPasto[index].code + '">'
					+ tipoPasto[index].description + '</option>';
			}
			$('div.customSelect').find('select#tipoPasto').append(option);
		}
		
		return false;
	}
	
	function dropdownDataFail() {
		return false;
	}
	
	return false;
});
