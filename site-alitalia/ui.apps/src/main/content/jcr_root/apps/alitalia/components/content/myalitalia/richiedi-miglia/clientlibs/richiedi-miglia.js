//setI18nTranslate('millemiglia.richiediMiglia.error');
//setI18nTranslate('common.monthsOfYear.january');
//setI18nTranslate('millemiglia.richiediMiglia.success');

//$(document).ready(function() {
//    if (resultRichiediMigliaSubmit == "true") {
//		clearRichiediMigliaMessages();
//		showFormFeedbackSuccess('millemiglia.richiediMiglia.success');
//	} else {
//		showFormFeedbackError('millemiglia.richiediMiglia.ajaxError');
//	}
//})

//$(window).load(function() {
//	alert("window loaded");
//});


$('#richiediMigliaSubmit').bind('click', function(e) {
	clearRichiediMigliaMessages();
	validation(e, 'richiedimigliasubmit', '#richiediMigliaForm',
			 richiediMigliaSuccess, richiediMigliaError);
	return false;
});

function richiediMigliaSuccess(data) {
	if (data.result) {
		removeMMErrors();
		$('#richiediMigliaSubmit').unbind('click');
		$('#richiediMigliaForm').submit();
	} else {
		showMMError(data, 'number');
	}
	return false;
}

function clearRichiediMigliaMessages(){
    $('#richiediMigliaErrMsg').empty();
    $('#richiediMigliaErrMsg').hide();
}

function richiediMigliaError(data) {
	showFormFeedbackError('millemiglia.richiediMiglia.ajaxError');
	return false;
}

function showFormFeedbackError(errorMessageI18nKey) {
    var errorMsg = getI18nTranslate(errorMessageI18nKey);
    $('#labelErrorMilleMiglia').show();
    $('#labelErrorMilleMiglia').text(errorMsg);
}

function showFormFeedbackSuccess(successMessageI18nKey){
    var successMsg = getI18nTranslate(successMessageI18nKey);
    $('#labelErrorMilleMiglia').show();
    $('#labelErrorMilleMiglia').text(successMsg);
    $('#labelErrorMilleMiglia').addClass('success');
}


