function refreshTablePartial(indexRoute, more, done) {
	invokeTablePartial(indexRoute, more, function(data) {
		var typeRoute = indexRoute == "0" ? "Departure" : "Return";
		$("#editorialAreaTopTable" + typeRoute).remove();
		$(".solutionForOtherDates" + typeRoute).remove();
		$(".bookingRecap.j-bookingRecap" + (indexRoute == "0" ? "Dep" : "Ret")).parent().remove();
		$('.j-showBackFlights.bookin__showBackFlights[data-flight="' + (indexRoute == "0" ? 'departure' : 'return') + '"]').remove();
		$("#bookingAward-route-" + typeRoute).replaceWith(data);
		if (done) {
			done(jQuery(jQuery.parseHTML(data)));
		}
		book.init();
		refreshBindings();
		bindMileageRecap();
		//bindTableEvents();
//		applyBusinessFilter();
//		bindPriceRecap();
		callAjaxLoad($(".j-bookingTable" + typeRoute));
		createTooltips();
	});
}

function invokeTablePartial(indexRoute, more, done, fail) {
	orderType = $("#select__timeFilter-" + indexRoute).val();
	directFlightOnly = $("#checkbox_directFlightOnly-" + indexRoute).is(":checked");
	
	data = {
		moreFlights: more,
		orderType: orderType,
		directFlightOnly: directFlightOnly,
	};
	invokeService("flight-select-table-partial." + indexRoute, "html", data, done, fail);
}


function executeFlightSearch(errorUrl) {
	// start airplane
	resultsLoading.init();
	performSubmit("bookingawardflightsearch", {}, function(data) {
		flightSearchDoneCallback(data, errorUrl);
	}, function(){
		flightSearchFailCallback(errorUrl);
	});
}

function flightSearchDoneCallback(data, errorUrl) {
	if (data.redirect) {
		window.location.replace(data.redirect);
	} else if (data.isError && errorUrl) {
		window.location.replace(errorUrl);
	} else {
		displayFlightResults();
	}
}

function flightSearchFailCallback(errorUrl) {
	if (errorUrl) {
		window.location.replace(errorUrl);
	}
}

function displayFlightResults() {
	invokeService("flight-select-main-partial", "html", false, function(data) {
		$("#booking-award-flight-select").html(data);
		$("#booking-award-flight-select").show();
		$(".waitingPage__loaderMobile").hide();
		book.init();
		updateDatePicker();
		numberSelector();
		refreshBindings();
		bindMileageRecap();
		analytics_bookingPartialCallback();
		$(".waitingPage").hide();
	});
	
}

/**
 * Funzione per il recupero dati del dettaglio del volo (al click su radio button)
 */
function be_priceSelector(selector, done) {
	var indexRoute = selector.attr("data-details-route");
	var indexFlight = selector.attr("data-details-flight");
	var positionBrand = selector.attr("data-col-index");
	var type = selector.closest(".j-bookingTableDeparture").length ? "Dep" : "Ret";
	var style= selector.closest("div.bookingTable__rightInner").attr("style");
	var isBusiness = false;
	if(style == "left: -90%;"){
		isBusiness = true;
	}
	
	$(".j-bookingTable__header--" + (type == "Dep" ? "departure" : "return") + " .j-ajaxLoad").each(function() {
		var target = $(this).attr("href");
		target = target.replace(/indexFlight\=[0-9]+/, "indexFlight=" + indexFlight);
		$(this).attr("href", target);
		$(this).attr("data-link", target);
	})
	
	
	invokeService("flight-select-flight-details-partial", "html", {indexRoute: indexRoute, indexFlight: indexFlight, indexBrand: positionBrand, isBusiness: isBusiness} , function(data) {
		$("#details-" + indexRoute + "-" + indexFlight).html(data);
		var totalDuration = $($("#details-" + indexRoute + "-" + indexFlight).siblings(".booking__fightPreviewWrapper").find(".flightTime")[0]).text();
		$("#details-" + indexRoute + "-" + indexFlight).find(".time").html(totalDuration);
		done(selector);
		bindDetailsEvents(selector);
	}, function() {
		done(selector);
	});
	
	
	
}

/**
 * Funzione per il recupero dati del dettaglio del volo (al click sul link per il tooltip)
 */
function be_getFlightDetails(selector, done) {
	be_priceSelector(selector, done);
}

function bindDetailsEvents(selector){
	$('.j-priceSelectorCol').fastClick( book.priceSelectorCol );
	$('.j-businessTrigger').unfastClick();
	$('.j-businessTrigger').fastClick(function(e){
		e.preventDefault();
		book.businessTrigger($(e.currentTarget));
	});
	$('.j-closeTableRow').fastClick( book.priceSelectorReset );
	$('.j-goToReturn').fastClick( book.goToReturn );
	$('.j-selectReturn').fastClick( book.selectedReturn );
	callAjaxLoad(selector.closest(".bookingTable__bodyRow"));

}

function be_loadMoreFlights(selector, done){
	var indexRoute = selector.parent().find(".j-loadMoreBookingRow").attr("data-details-route");
	refreshTablePartial(indexRoute, true, done);
}


function refreshBindings() {
	/*Select del criterio di ordinamento*/
	$("[id^=select__timeFilter-]").off("change", filterAndOrderFlights);
	$("[id^=select__timeFilter-]").change(filterAndOrderFlights);
	/*checkbox mostra solo voli diretti*/
	$("[id^=checkbox_directFlightOnly-]").off("change", filterAndOrderFlights);
	$("[id^=checkbox_directFlightOnly-]").change(filterAndOrderFlights);
}


function filterAndOrderFlights(){
	var indexRoute = $(this).attr("data-details-route");
	refreshTablePartial(indexRoute, false);
}

function be_goToReturn(selector, done) {
	confirmSelection(selector, done, "Dep");
}

function be_selectedReturn(selector, done) {
	confirmSelection(selector, done, "Ret");
}


function showBuyPointsMessage() {
	$('.notEnoughPoints').show();
	
	$("a[href='#acquistamiglia']").on('click', function(e) {
		e.preventDefault();
		$("#ssoGatewayBuy").submit();
	});
}

/**
 * Funzione eseguita al click sul bottone scegli e continua.
 * Conferma la selezione del volo refreshando la matrice corretta.
 */
function confirmSelection(selector, done, type) {
	$('.notEnoughPoints').hide();
	var indexRoute = selector.attr("data-details-route");
	var indexFlight = selector.attr("data-details-flight");
	var indexBrand = selector.attr("data-index-brand");
	var solutionID = selector.attr("data-solutionID");
	$(".j-priceSelector").each(function(i,elem){
		if ($(elem).attr("data-details-route") === indexRoute 
				&& $(elem).attr("data-details-flight") === indexFlight) { 
			if ($(elem).find($(".isActive .fakeRadio")).length > 0 ){
				indexBrand = $(elem).attr("data-index-brand");
				solutionID = $(elem).attr("data-solutionID");
			}
		}
	});
	if (indexBrand && solutionID) {
		data = {
				indexRoute: indexRoute,
				indexFlight: indexFlight,
				indexBrand: indexBrand,
				solutionId: solutionID
			};
		invokeService("bookingawardperformselection", "json", data, function(data) {
			if (data.redirect) {
				window.location.replace(data.redirect);
			} else {
				if (data.error) {
					showBuyPointsMessage();
					$('#buyMilesLink').attr('href', data.error);
					$('.j-bookingTableDeparture .j-bookingLoader').velocity('transition.slideDownOut');
					$('.j-bookingTableReturn .j-bookingLoader').velocity('transition.slideDownOut');
					var genericErrorHeader = (!$('[data-fixedheader]').data('fixedheader')) ? $('.mainMenu').outerHeight() : 0;
					var genericErrorScroll = $('.genericErrorMessage').offset().top  - genericErrorHeader ;
					$('html').velocity("scroll", {offset: genericErrorScroll});
					
				}
				else {
					var indexToRefresh = data.indexToRefresh;
					refreshInfoBoxPartial();
						
					if (indexToRefresh == -1) {
						$(".selectSubmit").removeClass("isDisabled");
						$(".bookInfoBoxBasketBtn").removeClass("isDisabled");
						$(".bookInfoBoxBasketBtn").attr("href", $(".selectSubmit").attr("href"));
					}
					addFlightInfoRecap(selector, done, type);
					
					
				}
				
			}
			
		}, function() {
			console.log("error: sellup");
			addFlightInfoRecap(selector, done, type);
		});
	}
}

function addFlightInfoRecap(selector, done, type) {
	var divLeft = selector.closest(".bookingTable__bodyRow").find(".bookingTable__left.booking__fightPreviewWrapper");
	var divLeftBody = selector.closest(".bookingTable__bodyRow").find(".bookingTable__rowBody.j-bookTableRowBody .bookingTable__left");
	var target = $(".j-bookingRecap" + type);
	var item;
	
	var date = $(".j-bookingRecap" + type + " .bookingRecap__previewText").text();
	target.find(".bookingRecap__previewText").html(date ? date.substring(0, date.lastIndexOf("-") + 1) + " " : "");
	
	if ((item = divLeftBody.find(".bookingFlightDetails__listScale").first().find(".third .bookingFlightDetails__airport")).length > 0) {
		target.find(".first .booking__fightPreviewDepArrCont__iata").html(item.text());
	}
	if ((item = divLeftBody.find(".bookingFlightDetails__listScale").first().find(".third .bookingFlightDetails__airportName")).length > 0) {
		target.find(".first .booking__fightPreviewDepArrCont__airport").html(item.text());
	}
	if ((item = divLeftBody.find(".bookingFlightDetails__listScale").last().find(".fifth .bookingFlightDetails__airport")).length > 0) {
		target.find(".third .booking__fightPreviewDepArrCont__iata").html(item.text());
	}
	if ((item = divLeftBody.find(".bookingFlightDetails__listScale").last().find(".fifth .bookingFlightDetails__airportName")).length > 0) {
		target.find(".third .booking__fightPreviewDepArrCont__airport").html(item.text());
	}
	
	if ((item = divLeft.find(".first .booking__fightPreviewDepArrCont__time")).length > 0) {
		target.find(".first .booking__fightPreviewDepArrCont__time").html(item.text());
		target.find(".bookingRecap__previewText").append(item.text());
	}
	if ((item = divLeft.find(".third .booking__fightPreviewDepArrCont__time")).length > 0) {
		target.find(".third .booking__fightPreviewDepArrCont__time").html(item.text());
		target.find(".bookingRecap__previewText").append(" > " + item.text());
	}
	if ((item = divLeft.find(".booking__fightPreviewInfoDesktop .transferNum")).length > 0) {
		target.find(".bookingRecap__scaleRecapLiner").html(item.text().toLowerCase() + " ");
		
	}
	if ((item = divLeft.find(".booking__fightPreviewInfoDesktop .flightTime")).length > 0) {
		target.find(".bookingRecap__scaleRecapLiner").append($('<span class="bookingRecap__scaleRecap"></span>').html(" - " + item.text()));
	}
	if ((item = divLeft.find(".booking__fightPreviewInfoDesktop .flightComp")).length > 0) {
		target.find(".bookingRecap__previewText").append(" | ");
		target.find(".bookingRecap__previewText").append($('<span></span>').html(item.text()));
	}
	initAccordions();
	done(selector);
}

function bindMileageRecap() {
	$(".j-priceSelector").on("click", function() {
		var type = $(this).closest(".j-bookingTableDeparture").length ? "Dep" : "Ret";
		var item;
		if ((item = $(this).find(".mmlPoints")).length > 0) {
	        $(".j-bookingRecap" + type + " .mmlPrice .valMml").html(item.text());
		}
	});
}

function updateDatePicker() {
	initDatePicker();
	$("#andata").val($("#departureInputMod").val());
	if($('input.j-typeOfFlight:checked').val() === 'a')
		$('#ritorno').attr('disabled','disabled').attr('aria-disabled','true');
	else
		$("#ritorno").val($("#arrivalInputMod").val());
		
	
	$('input.j-typeOfFlight').on('change', function(event) {
		var _val = $(this).val();
		if ( _val === 'a' ){
			$('#ritorno').attr('disabled','disabled').attr('aria-disabled','true');
			$('#returnDate').attr('disabled','disabled').attr('aria-disabled','true').parent().velocity({opacity: .3});
			if ( $('#andata').val().length > 0 ){
				$('.flightFinder, .innerFlightFinder').removeClass('ff-stage4').addClass('ff-stage5');
			}
		} else {
			$('#ritorno').removeAttr('disabled').removeAttr('aria-disabled');
			$('#returnDate').removeAttr('disabled').removeAttr('aria-disabled').parent().velocity({opacity: 1});
		}
		highlightPeriod();
	});
}

function bindBasketEvents(){
	$('.j-toggleBasketAccordion').unfastClick().fastClick(function(e){
		e.preventDefault();
		book.openBasket($(this));
	});

	if($('.j-toggleBasketAccordion').hasClass('opened')){
		var toShow = $('.j-toggleBasketAccordion').attr('href')
		$(toShow).show();
		window.bookingHeaderStop=true;
	}
	updateDatePicker();
	$('.j-openFlightFinder').fastClick(function(e){
		e.preventDefault();
		book.openBasket($(this));
		window.bookingHeaderStop=true;
	});
	$(".bookInfoBoxBasketBtn").attr("href", $(".selectSubmit").attr("href"));
	if (!$(".selectSubmit").hasClass("isDisabled")) {
		$(".bookInfoBoxBasketBtn").removeClass("isDisabled");
	}
	
	initAccordions();
	numberSelector();
}

function invokeInfoBoxPartial(done, fail) {
	invokeService("flight-select-infoBox-partial", "html", false, done, fail);
}

function refreshInfoBoxPartial() {
	invokeInfoBoxPartial(function(data) {
		var elemContent = $(data).find(".booking__informationBox.j-bookInfoBox").children();
		$(".booking__informationBox.j-bookInfoBox").html(elemContent);
		
		bindBasketEvents();
		//$(window).trigger("resize");
	});
}