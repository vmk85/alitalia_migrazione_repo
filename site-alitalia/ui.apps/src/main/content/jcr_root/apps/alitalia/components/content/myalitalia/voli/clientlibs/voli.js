
// var tipoUtente = ''; // può essere MM | MA | BOTH
// $(document).ready( function() {
//     // controlliamo se utente è MA
//     getUserMAInfo( function(resp) {
//         if (resp.status == 'FAIL') {
//             if( CQ_Analytics.ProfileDataMgr.data &&
//                 CQ_Analytics.ProfileDataMgr.data.isLoggedIn == "true") {
//                 tipoUtente = 'MM';
//
//                 // popolaFormMM( CQ_Analytics.ProfileDataMgr.data );
//             } else {
//                console.log( 'utente non autenticato');
//             }
//         } else {
//             if( CQ_Analytics.ProfileDataMgr.data &&
//                                CQ_Analytics.ProfileDataMgr.data.isLoggedIn == "true") {
//                 tipoUtente = 'BOTH';
//                 // console.log( CQ_Analytics.ProfileDataMgr.data );
//                 // popolaFormMA( resp, CQ_Analytics.ProfileDataMgr.data );
//             } else {
//                 tipoUtente = 'MA';
//                 // console.log( CQ_Analytics.ProfileDataMgr.data );
//                 // popolaFormMA( resp );
//             }
//         }
//
//     });
// });

function getServiceUrl(service, selector, secure) {
    if (selector == 'login') {
        return service;
    }
    selector = selector || "json";
    var protocol = location.protocol;
    if (jsonProtocol && (selector == "json" || secure)) {
        protocol = 'https:';
    }
    if(service == 'menu-millemiglia-header'){
        return protocol + '//' + location.host + internalUrl1 + "."
            + service + "." + selector;
    } else{
        return protocol + '//' + location.host + internalUrl1 + "/."
            + service + "." + selector;
    }
}

/**
 * inizializza i campi della form con i dati dell'utente MA
 */
// function popolaFormMA( data, userMM ) {
//
//     // nascondiamo div per adesione mille miglia young
//     $(".divAdesioneMilleMigliaYoung").hide();
//     if (tipoUtente == 'MA' ) {
//
//
//         //hidden = (resp.data.hiddenElements!=null?resp.data.hiddenElements:[]);
//         $(".userName").html(data.profile.firstName + " " + data.profile.lastName);
//         $(".userImg").attr("src", (data.profile.photoURL)? data.profile.photoURL:"/etc/designs/alitalia/clientlibs-myalitalia/images/avatar.jpg");
//         $('.userDataNascita').text('Data nascita');
//         $("#gender option[value='" + data.profile.gender + "']").attr("selected", "selected");
//
//         if(data.data.address1_Type != null) {
//             if(data.data.address1_Type == "home") {
//
//                 $(".radio-wrap:eq(0)").find("span").trigger("click");
//                 $("#haddress").val((data.data.address1_Address!=null?data.data.address1_Address:""));
//                 $("#hcap").val((data.data.address1_Zip!=null?data.data.address1_Zip:""));
//                 $("#hcity").val((data.data.address1_City!=null?data.data.address1_City:""));
//                 $("#hprov").val((data.data.address1_StateCode!=null?data.data.address1_StateCode:""));
//                 $("#htown").val((data.data.address1_CountryCode!=null?data.data.address1_CountryCode:""));
//                 $("#hcell").val((data.data.mobilePhone!=null?data.data.mobilePhone:""));
//                 $("#htel").val((data.data.phone1Number!=null?data.data.phone1Number:""));
//                 $("#htel-prefix").val((data.data.phone1CountryCode!=null?data.data.phone1CountryCode:""));
//                 $("#hprefix").val((data.data.phone1AreaCode!=null?data.data.phone1AreaCode:""));
//                 $("#hprefix option[value='" + (data.data.phone1_AreaCode!=null?data.data.phone1_AreaCode:"") + "']").attr("selected", "selected");
//
//             } else if(data.data.address1_Type == "company") {
//
//                 $(".radio-wrap:eq(1)").find("span").trigger("click");
//                 $("#company").val((data.data.address1_CompanyName!=null?data.data.address1_CompanyName:""));
//                 $("#caddress").val((data.data.address1_Address!=null?data.data.address1_Address:""));
//                 $("#ccap").val((data.data.address1_Zip!=null?data.data.address1_Zip:""));
//                 $("#ccity").val((data.data.address1_City!=null?data.data.address1_City:""));
//                 $("#cprov").val((data.data.address1_StateCode!=null?data.data.address1_StateCode:""));
//                 $("#ctown").val((data.data.address1_CountryCode!=null?data.data.address1_CountryCode:""));
//                 $("#ccell").val((data.data.mobilePhone!=null?data.data.mobilePhone:""));
//                 $("#ctel").val((data.data.phone1Number!=null?data.data.phone1Number:""));
//                 $("#ctel-prefix").val((data.data.phone1CountryCode!=null?data.data.phone1CountryCode:""));
//                 $("#cprefix").val((data.data.phone1AreaCode!=null?data.data.phone1AreaCode:""));
//                 $("#cprefix option[value='" + (data.data.phone1_AreaCode!=null?data.data.phone1_AreaCode:"") + "']").attr("selected", "selected");
//                 $("#cmmy").prop("checked", (data.data.mmIsYoung==null?data.data.mmIsYoung:false));
//
//             }
//             $("#profession").val((data.data.professionalHeadline!=null?data.data.professionalHeadline:""));
//         }
//         if(data.data.id_mm == null) {
//             $(".millemiglia").css("display", "none");
//             $(".user-data").hide();
//         } else {
//             $(".millemiglia").css("display", "block");
//             $(".user-data").show();
//         }
//     } else {
//
//         $(".millemiglia").css("display", "block");
//         $(".user-data").css("display", "block");
//         checkProfileMMState(userMM);
//         $(".userName").html(capitalizeName(userMM.customerName) + " " + capitalizeName(userMM.customerSurname));
//         $(".userImg").attr("src", (userMM.avatar)?userMM.avatar:"/etc/designs/alitalia/clientlibs-myalitalia/images/avatar.jpg");
//         $("span.value").html(parseInt(userMM.milleMigliaTotalMiles).toLocaleString());
//
//
//     }
//
// }

// function popolaFormMM( data ) {
//
//     $(".millemiglia").css("display", "block");
//     $(".user-data").css("display", "block");
//     checkProfileMMState(data);
// 	$(".userName").html(capitalizeName(data.customerName) + " " + capitalizeName(data.customerSurname));
//     $(".userImg").attr("src", (data.avatar)?data.avatar:"/etc/designs/alitalia/clientlibs-myalitalia/images/avatar.jpg");
//     $("span.value").html(parseInt(data.milleMigliaTotalMiles).toLocaleString());
//
//     var level = getMMLevel(data.milleMigliaTierCode);
//     $(".special-freccia-alata").html("<img src=" + "/etc/designs/alitalia/clientlibs-myalitalia/images/ico-step-" + level.lvl + ".svg> " + level.value);
//
//
//
// /*
//     $("#gender option[value='" + userMM.customerGender + "']").attr("selected", "selected");
//
//     if(userMA.data.address1_Type != null) {
//         if(userMA.data.address1_Type == "home") {
//
//             $(".radio-wrap:eq(0)").find("span").trigger("click");
//             $("#haddress").val((userMA.data.address1_Address!=null?userMA.data.address1_Address:""));
//             $("#hcap").val((userMA.data.address1_Zip!=null?userMA.data.address1_Zip:""));
//             $("#hcity").val((userMA.data.address1_City!=null?userMA.data.address1_City:""));
//             $("#hprov").val((userMA.data.address1_StateCode!=null?userMA.data.address1_StateCode:""));
//             $("#htown").val((userMA.data.address1_CountryCode!=null?userMA.data.address1_CountryCode:""));
//             $("#hcell").val((userMA.data.mobilePhone!=null?userMA.data.mobilePhone:""));
//             $("#htel").val((userMA.data.phone1Number!=null?userMA.data.phone1Number:""));
//             $("#htel-prefix").val((userMA.data.phone1CountryCode!=null?userMA.data.phone1CountryCode:""));
//             $("#hprefix").val((userMA.data.phone1AreaCode!=null?userMA.data.phone1AreaCode:""));
//             $("#hprefix option[value='" + (userMA.data.phone1_AreaCode!=null?userMA.data.phone1_AreaCode:"") + "']").attr("selected", "selected");
//
//         } else if(userMA.data.address1_Type == "company") {
//
//             $(".radio-wrap:eq(1)").find("span").trigger("click");
//             $("#company").val((userMA.data.address1_CompanyName!=null?userMA.data.address1_CompanyName:""));
//             $("#caddress").val((userMA.data.address1_Address!=null?userMA.data.address1_Address:""));
//             $("#ccap").val((userMA.data.address1_Zip!=null?userMA.data.address1_Zip:""));
//             $("#ccity").val((userMA.data.address1_City!=null?userMA.data.address1_City:""));
//             $("#cprov").val((userMA.data.address1_StateCode!=null?userMA.data.address1_StateCode:""));
//             $("#ctown").val((userMA.data.address1_CountryCode!=null?userMA.data.address1_CountryCode:""));
//             $("#ccell").val((userMA.data.mobilePhone!=null?userMA.data.mobilePhone:""));
//             $("#ctel").val((userMA.data.phone1Number!=null?userMA.data.phone1Number:""));
//             $("#ctel-prefix").val((userMA.data.phone1CountryCode!=null?userMA.data.phone1CountryCode:""));
//             $("#cprefix").val((userMA.data.phone1AreaCode!=null?userMA.data.phone1AreaCode:""));
//             $("#cprefix option[value='" + (userMA.data.phone1_AreaCode!=null?userMA.data.phone1_AreaCode:"") + "']").attr("selected", "selected");
//             $("#cmmy").prop("checked", (userMA.data.mmIsYoung==null?userMA.data.mmIsYoung:false));
//
//         }
//         $("#profession").val((userMA.data.professionalHeadline!=null?userMA.data.professionalHeadline:""));
//     }
//     if(userMA.data.id_mm == null) {
//         $(".millemiglia").css("display", "none");
//         $(".user-data").hide();
//     } else {
//         $(".millemiglia").css("display", "block");
//         $(".user-data").show();
//     }
//     */
// }

// function checkProfileMMState(data) {
// 	var state = VAL_STEP;
//     $(".progress-bar").find("p").text(state + "%");
//     for(var i=0; i<(state/VAL_STEP); i++) {
//         $(".progress-bar").find("span:eq(" + i + ")").removeClass("empty");
//         $(".progress-bar").find("span:eq(" + i + ")").addClass("fill");
//     }
// }

// $("#save").click(function() {
//
//     if (tipoUtente == 'MM'  ) {
//
//     } else {
//
//         enableLoaderMyAlitalia();
//
//         // salva utente MA
//         var params = {};
//         if($("#abitazione").is(":checked")) {
//
//             params["address1_Type"] = "home";
//             if($("#hcap").val() != "")
//                 params["address1_Zip"] = $("#hcap").val();
//             if( $("#hcity").val() != "")
//                 params["address1_City"] = $("#hcity").val();
//             if($("#htown").val() != "")
//                 params["address1_CountryCode"] = $("#htown").val();
//             if($("#hprov").val() != "")
//                 params["address1_StateCode"] = $("#hprov").val();
//             if($("#haddress").val() != "")
//                 params["address1_Address"] = $("#haddress").val();
//             if($("#htel").val() != "") {
//                 params["phone1Type"] = "telephone";
//                 params["phone1Number"] = $("#htel").val();
//             }
//             if($("#hcell").val() != "")
//                 params["mobilePhone"] = $("#hcell").val();
//             if($("#htel-prefix").val() != "")
//                 params["phone1CountryCode"] = $("#htel-prefix").val();
//             if($("#h-prefix").val() != "")
//                 params["phone1AreaCode"] = $("#hprefix").val();
//             //params["mmIsYoung"] = $("#hmmy").is(":checked");
//
//         } else {
//
//             params["address1_Type"] = "company";
//             if($("#company").val() != "")
//                 params["address1_CompanyName"] = $("#company").val();
//             if($("#ccap").val() != "")
//                 params["address1_Zip"] = $("#ccap").val();
//             if( $("#ccity").val() != "")
//                 params["address1_City"] = $("#ccity").val();
//             if($("#ctown").val() != "")
//                 params["address1_CountryCode"] = $("#ctown").val();
//             if($("#cprov").val() != "")
//                 params["address1_StateCode"] = $("#cprov").val();
//             if($("#caddress").val() != "")
//                 params["address1_Address"] = $("#caddress").val();
//             if($("#ctel").val() != "") {
//                 params["phone1Type"] = "telephone";
//                 params["phone1Number"] = $("#ctel").val();
//             }
//             if($("#ccell").val() != "")
//                 params["mobilePhone"] = $("#ccell").val();
//             if($("#ctel-prefix").val() != "")
//                 params["phone1CountryCode"] = $("#ctel-prefix").val();
//             if($("#cprefix").val() != "")
//                 params["phone1AreaCode"] = $("#cprefix").val();
//             //params["mmIsYoung"] = $("#cmmy").is(":checked");
//
//         }
//         if($("#profession").val() != "")
//             params["professionalHeadline"] = $("#profession").val();
//
//         var par = {
//             callback: function(resp) {
//                 console.log(resp);
//                 saveUserMAToModel();
//             },
//             data: params
//         };
//
//         gigya.accounts.setAccountInfo(par);
//
//         disableLoaderMyAlitalia();
//     }
//
// });


$("#cprov").on("keyup", function() {
    $(this).val($(this).val().toUpperCase());
});

function showFeedbackSaved(id) {
    $(id).css("display", "block");
    $(id).delay(3000).fadeOut("slow");
}

