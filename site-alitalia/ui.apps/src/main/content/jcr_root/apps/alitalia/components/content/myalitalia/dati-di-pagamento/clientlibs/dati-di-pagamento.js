var isCreditCardToggled = false;
var isCreditCardValid = false;
var selectedPaymentMethod = null;
var formPayment = null;

var placeholder_numeroCarta = CQ.I18n.get("checkin.payment.numeroCarta.label");
var placeholder_mese = CQ.I18n.get("checkin.payment.month.label");
var placeholder_anno = CQ.I18n.get("checkin.payment.year.label");
var placeholder_cvc = CQ.I18n.get("checkin.payment.cvc.label");
var placeholder_nome = CQ.I18n.get("checkin.payment.nomeTitolare.label");
var placeholder_cognome = CQ.I18n.get("checkin.payment.cognomeTitolare.label");
var placeholder_email = CQ.I18n.get("millemiglia.email.label");


$('#addpayment').click(function() {
	toggleCreditCardForm();
});

$('input[name="switch-payment"]').click(function() {
	selectedPaymentMethod = $(this).val();
    $('.payment-methods .radio-wrap').closest('.payment-methods .radio-wrap').removeClass('checked');
	$(this).closest('.payment-methods .radio-wrap').addClass('checked');
    if (formPayment) {
    	$('#'+formPayment).removeClass('show');
    	$('#'+formPayment).addClass('hide');
    	resetForm();
    }
	formPayment = $(this).attr("data-target");
	$('#' + $(this).attr("data-target")).removeClass('hide');
	$('#' + $(this).attr("data-target")).addClass('show');
	addKeyPressListeners();
});

$(".closePaymentForm").on("click", cancelnewpayment);

$(document).ready(function () {
    $('.reveal-wrap').foundation();
	
    $(".checkValidCC").css('background-image','url("/etc/designs/alitalia/clientlibs-checkin-pay-1-embed/images/mc_ok_bw.png")');
    $(".checkValidCC").css('background-position','right center');
    $(".checkValidCC").css('background-repeat','no-repeat');

	$(".countryList option[value=HK]").html(CQ.I18n.get("countryData.HK"));
	$(".countryList option[value=MO]").html(CQ.I18n.get("countryData.MO"));
	$(".countryList option[value=TW]").html(CQ.I18n.get("countryData.TW"));

	$(".delete").click(function(){
        idx = $(this).attr("idx");
        deleteCdc(idx);
	});
	
    $('.button--back').click(function(){
        $('.close-button').trigger('click');
    });

    $('.close-button').on('click',function(){
        $('.error-delete').attr('style','display: none;');
        $('.success-').attr('style','display: none;');
    });
    
    $('.action__icon--delete').click(function(e) {
    	if (isCreditCardToggled) {
    		hideCreditCardForm();
    	}
    });

});

function cancelnewpayment(e) {
    e.stopImmediatePropagation();
	$('#addpayment').click();
	$("html, body").animate({ scrollTop: 0 }, "slow");
};

function savenewpayment(e) {
    $(this).off('click', savenewpayment);
    enableLoader();
    validateSave(e);
};

function validateSave(event) {
    var fieldsValid = true;
    paymentErrors = "";
    
   	$('#'+formPayment+' .pay-isRequired').each(function(i, obj){
        if($(obj).prop('name')=='cardHolderFirstName'){
            if(isCredentialValid($(obj).val())){
                hideCurrentFeedbackErrors($(obj));
            }else{
                disableLoader();
                fieldsValid = false;
                showCurrentFeedbackErrorsNameSurname($(obj));
            }
        }

        if($(obj).prop('name')=='cardHolderLastName'){
            if(isCredentialValid($(obj).val())){
                hideCurrentFeedbackErrors($(obj));
            }else{
                disableLoader();
                fieldsValid = false;
                showCurrentFeedbackErrorsNameSurname($(obj));
            }
        }

        if($(obj).prop('name')=='cardSecurityCode'){
            var validCVV = validateCVV($(obj).val(),formPayment)
            if(validCVV){
                hideCurrentFeedbackErrors($(obj));
            }else{
                disableLoader();
                fieldsValid = false;
                showCurrentFeedbackErrorsCVV($(obj),validCVV);
            }
        }
        
        if(!$(obj).val()){
            disableLoader();
            fieldsValid = false;
            showCurrentFeedbackErrors($(obj));
        }
    });

    if(isCreditCardValid){
        hideCurrentFeedbackErrors($('#'+formPayment).find('.checkValidCC'));
    }else{
        disableLoader();
        fieldsValid = false;
        showCurrentFeedbackErrors($('#'+formPayment).find('.checkValidCC'));
    }

    if(fieldsValid){
        save();
    }else{
        disableLoader();
        $('#'+formPayment).addClass('validateOnKeyPress');
        validateOnKeyPress = true;
        //showFeedbackErrors();
        dataLayer[1].checkinError = paymentErrors;
//        _satellite.track("CIError");
        dataLayer[1].checkinError = "";

    }

}

function validateCVV(value,creditcardform){
    var cvvLengthRequired = 3;
    if(creditcardform == "form_american_express"){
        cvvLengthRequired = 4;
    }
    if(value.length == cvvLengthRequired){
        return true;
    }else{
        return false;
    }
}

function isCredentialValid(string) {
    if(string.search( new RegExp("\[0-9]")) > -1){
        return false;
    } else {
        return true;
    }
}

function showCurrentFeedbackErrors(el){
    $(el).parent().find('.feedback-error').removeClass('hide');
    $(el).attr("style",'border-color:red');
    if(paymentErrors.length == 0){
        paymentErrors = $(el).parent().find('.feedback-error').text();
    }else{
        paymentErrors = paymentErrors + "; " + $(el).parent().find('.feedback-error').text();
    }
}

function showCurrentFeedbackErrorsNameSurname(el){
    if($(el).val() != ''){
        $(el).parent().find('.feedback-error').text(CQ.I18n.get('checkin.common.invalidData.label'));
    }
    $(el).parent().find('.feedback-error').removeClass('hide');
    $(el).attr("style",'border-color:red');
    if(paymentErrors.length == 0){
        paymentErrors = $(el).parent().find('.feedback-error').text();
    }else{
        paymentErrors = paymentErrors + "; " + $(el).parent().find('.feedback-error').text();
    }
}

function showCurrentFeedbackErrorsCVV(el,validCVV){
    if(!validCVV){
        $(el).parent().find('.feedback-error').text(CQ.I18n.get('checkin.common.genericInvalidField.label'));
    }
    $(el).parent().find('.feedback-error').removeClass('hide');
    $(el).attr("style",'border-color:red');
    if(paymentErrors.length == 0){
        paymentErrors = $(el).parent().find('.feedback-error').text();
    }else{
        paymentErrors = paymentErrors + "; " + $(el).parent().find('.feedback-error').text();
    }
}

function hideCurrentFeedbackErrors(el){
    $(el).parent().find('.feedback-error').addClass('hide');
    $(el).attr("style",'');
}

function resetForm() {
	if (formPayment) {
		$('#'+formPayment).find("input[type=text], textarea").val("");
		$('#'+formPayment).find("input[type=text], textarea, select").parent().find('.feedback-error').addClass('hide');
		$('#'+formPayment).find("input[type=text], textarea, select").attr("style",'');
	    $('#'+formPayment+' .checkValidCC').css('background-image','url("/etc/designs/alitalia/clientlibs-checkin-pay-1-embed/images/mc_ok_bw.png")');
	    $('#'+formPayment+' .checkValidCC').css('background-position','right center');
	    $('#'+formPayment+' .checkValidCC').css('background-repeat','no-repeat');
        $('.error-'+formPayment).attr('style','display: none;');
	}
}

function enableLoader() {
    $('.circle-loader').show();
};

function disableLoader() {
    $('.circle-loader').hide();
};

function addKeyPressListeners(){
    validateOnKeyPress = $('#form_'+selectedPaymentMethod).hasClass('validateOnKeyPress') ? true : false;

    $('#form_'+selectedPaymentMethod+' .checkValidCC').keyup(function(){
        var result = $(this).validateCreditCard();

        var strTest=selectedPaymentMethod;

        if(selectedPaymentMethod == "visa_electron"){
            strTest = "visaelectron";
        }
        if(selectedPaymentMethod == "american_express"){
            strTest = "americanexpress";
        }
        if(selectedPaymentMethod == "diners_club"){
            strTest = "diners";
        }

        var strCC=(result.card_type == null ? 'error' : result.card_type.name);

        var isValid=(result.valid && result.length_valid && result.luhn_valid && (strCC==strTest));

        if (isValid) {
            isCreditCardValid = true;
            $(this).css('background-image','url("/etc/designs/alitalia/clientlibs-checkin-pay-1-embed/images/mc_ok.png")');
            $(this).css('background-position','right center');
            $(this).css('background-repeat','no-repeat');
        }else{
            isCreditCardValid = false;
            $(this).css('background-image','url("/etc/designs/alitalia/clientlibs-checkin-pay-1-embed/images/mc_ok_bw.png")');
            $(this).css('background-position','right center');
            $(this).css('background-repeat','no-repeat');
        }

        if(validateOnKeyPress){
            if(isCreditCardValid){
                hideCurrentFeedbackErrors($(this));
            }else{
                showCurrentFeedbackErrors($(this));
            }
        }
    });

    $('#form_'+selectedPaymentMethod+' .pay-isRequired').keyup(function(){
        if(validateOnKeyPress){
            if($(this).prop('type')=='email'){
                if(isEmailValid()){
                    hideCurrentFeedbackErrors($(this));
                }else{
                    showCurrentFeedbackErrors($(this));
                }
            }else{
                if($(this).val()){
                    hideCurrentFeedbackErrors($(this));
                }else{
                    showCurrentFeedbackErrors($(this));
                }
            }
        }
    });
}

function toggleCreditCardForm() {
	if (isCreditCardToggled) {
		hideCreditCardForm();
	} else {
		showCreditCardForm();
	}
}

function hideCreditCardForm() {
	resetForm();

	$("#form_creditcards").removeClass('show');
	$("#form_creditcards").addClass('hide');
    $('.payment-methods .radio-wrap').closest('.payment-methods .radio-wrap').removeClass('checked');
    $('input[name="switch-payment"]').each(function() {
    	var switchPaymentRadio = $(this).attr("data-target");
    	$('#' + switchPaymentRadio).removeClass('show');
    	$('#' + switchPaymentRadio).addClass('hide');
    });

	isCreditCardToggled = !isCreditCardToggled;
}

function showCreditCardForm() {
	$("#form_creditcards").removeClass('hide');
	$("#form_creditcards").addClass('show');

	isCreditCardToggled = !isCreditCardToggled;
}

function deleteCdc(idx){
    enableLoader();
    var params = { selectedCreditCardId: idx };
    performSubmit('madeletemycreditcard', params, deleteSuccess, deleteFail);
}

function deleteSuccess(data) {
	if (data!=null && data.result==='OK') {
//	    reloadCdc();
	    reloadSuccess(data);
	} else {
		disableLoader();
        $('.error-delete').attr('style','display: block;');
	}
}

function deleteFail(data) {
	disableLoader();
    $('.error-delete').attr('style','display: block;');
}

function save() {
	enableLoader();
    encryptData(formPayment);
    performSubmit('masavemycreditcard', '#'+formPayment, saveSuccess, saveFail);
}

function saveSuccess(data) {
	if (data!=null && data.result==='OK') {
//	    reloadCdc();
	    reloadSuccess(data);
	} else {
		disableLoader();
        $('.error-'+formPayment).attr('style','display: block;');
	}
}

function saveFail(data) {
	disableLoader();
    $('.error-'+formPayment).attr('style','display: block;');
}

function reloadCdc(){
    performGet('maretrievemycreditcard', reloadSuccess, reloadFail);
}

function reloadSuccess(data) {
//	location.reload(true);
	document.location.reload(true);
}

function reloadFail(data) {}

function performGet(service, done, fail) {
	return $.ajax({
	    url : getServiceUrl(service, null),
	    method : 'GET',
	}).done(function(data) {
	    if (done) {
	        done(data);
	    }
	}).fail(function() {
	    if (fail) {
	        fail();
	    }
	}).always(function() {
	});
}

function encryptData(idFormFrom) {

    var options = {};

    var formFrom = document.getElementById(idFormFrom);
    var AdyenCryptingKey=$("#" + formFrom.id + " [name='adyen-encrypted-data']").attr("data-key-encrypt");

    var cseInstance = adyen.encrypt.createEncryption(AdyenCryptingKey, options);

    var postData = {};

    var cardData = {
        number: formFrom.elements["cardNumber"].value,
        cvc: formFrom.elements["cardSecurityCode"].value,
        holderName: formFrom.elements["cardHolderFirstName"].value + " " + formFrom.elements["cardHolderLastName"].value,
        expiryMonth: formFrom.elements["expireMonth"].value,
        expiryYear: formFrom.elements["expireYear"].value,
        generationtime: $("#" + formFrom.id + " [data-encrypted-name='generationtime']").val()
    };

    postData['adyen-encrypted-data'] = cseInstance.encrypt(cardData);

    $("#" + formFrom.id + " [name='adyen-encrypted-data']").val(postData['adyen-encrypted-data']);
}