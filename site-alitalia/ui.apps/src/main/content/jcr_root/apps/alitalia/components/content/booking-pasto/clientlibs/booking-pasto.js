setI18nTranslate('booking.service.generic.error');

var defaultSelectedTipoPasto = "";
$(document).ready(function() {
	var requestData = {
		'tipoPasto' : defaultSelectedTipoPasto
	};
	getDropdownData(requestData, dropdownDataSuccess, dropdownDataFail);
});	
	
function dropdownDataSuccess(data) {
	var tipiPasto = data.tipoPasto;
	
	$('.food').each(function(index, elem) {
		window.defaultSelectedTipoPasto = $(this).parent().children(".mealDefault").text();
		for (index in tipiPasto) {
			var option = '';
			 if (tipiPasto[index].code == window.defaultSelectedTipoPasto) {
				option = '<option value="' + tipiPasto[index].code + '" selected>' + tipiPasto[index].description + '</option>';
			 	} else {
				option = '<option value="' + tipiPasto[index].code + '">' + tipiPasto[index].description + '</option>';
			 	}
			if(tipiPasto[index].description==kosherMealCode && showKosherMeal=="false"){
				option='';
			}
				$(this).append(option);

			}
	
	
});
}

function dropdownDataFail() {
	console.log('fail retrive meals');
}

function be_cancelFood(section,done) {
	performSubmit('bookingremovemealspreferenceconsumer', '#form-booking-meals',
			function(){
				refreshInfoBoxPartial();
				if (done) {
					done();
				}
			},
			function(){
				if (done) {
					done();
				}
			}
	);
}

function be_changeFood(e,btn,selector,done) {
	validation(e, 'bookingmealsconsumer', '#form-booking-meals',
			function(data){ 
				mealsPreferenceSuccess(data,done); 
			},
			function(){
				removeErrors();
				if (done) {
					done();
				}
			}
	);
}

function mealsPreferenceSuccess(data,done) {
	if (data.result) {
		removeErrors();
		performSubmit('bookingmealsconsumer', '#form-booking-meals',
				function(){ 
					updateSummary();
					refreshInfoBoxPartial();
					if (done) {
						done();
					}
				},
				function(){
					$('#errorFieldMealsChoose').text(getI18nTranslate('booking.service.generic.error'));
				}
		);
	} else {
		showErrors(data);
	}
}

function updateSummary() {
	$('#food_summary .bookingExtra__infoLine').each(function(i,elem) {
		var index = i + 1;
		var selector = "#passenger" + index;
		$(selector).text($('#food' + index + ' option:selected ').text());
	});
}

function removeErrors() {
	$('#errorFieldMealsChoose').text('');
	$('#errorFieldSummary').text('');
}