
function initPartialMmbFastTrack() {
	$('.fast-track-checkbox').change(function(e) {
		var checkbox = e.currentTarget;
		var hidden = $(checkbox).siblings('input[type="hidden"].fast-track-checkbox-field');
		var currentVal = $(hidden).val();
		var newVal = currentVal.substring(0, currentVal.length - 1) + ( $(checkbox).is(':checked') ? '1' : '0' );
		$(hidden).val(newVal);
	});
}


/* modify form */

function be_addFastTrack(e, btn, section, callback) {
	performValidation('mmbancillarycartfasttrack', '#mmb-ancillary-modify-fasttrack-form', 
			mmbAncillaryFastTrackModifyValidationSuccess, mmbHandleInvokeError);
};

function mmbAncillaryFastTrackModifyValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('mmbancillarycartfasttrack', '#mmb-ancillary-modify-fasttrack-form', 
				mmbAncillaryFastTrackModifySubmitSuccess, mmbHandleInvokeError);
	} else {
		showErrors(data);
	}
}

function mmbAncillaryFastTrackModifySubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		if (data.isError) {
			mmbHandleServiceError(data);
		}
		refreshMmbPartialFastTrack(true, true);
	}
}


/* clear form */

function be_clearFastTrack(e, btn, section, callback) {
	performValidation('mmbancillarycartfasttrack', '#mmb-ancillary-clear-fasttrack-form', 
			mmbAncillaryFastTrackClearValidationSuccess, mmbHandleInvokeError);
};

function mmbAncillaryFastTrackClearValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('mmbancillarycartfasttrack', '#mmb-ancillary-clear-fasttrack-form', 
				mmbAncillaryFastTrackClearSubmitSuccess, mmbHandleInvokeError);
	} else {
		showErrors(data);
	}
}

function mmbAncillaryFastTrackClearSubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		if (data.isError) {
			mmbHandleServiceError(data);
		}
		refreshMmbPartialFastTrack(false, true);
	}
}