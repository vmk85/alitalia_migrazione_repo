	function analytics_carnetPartialCallback(){
		if(typeof window["analytics_carnet_step"] !== "undefined"){
			addCarnetAnalyticsData(analytics_carnet_step);
			activateGTMOnce("carnet");
			activateWebtrendsOnce("carnet");
		}
	}
	
	function addCarnetAnalyticsData(carnetStep){
		var DTMRule = "";
		switch(carnetStep){
		case 1:
			DTMRule = "CarnetBuyInfoPax";
			break;
		case 2:
			DTMRule = "CarnetBuyPayment";
			break;
		case 3:
			DTMRule = "CarnetBuyReceipt";
			break;
		}
		setDataLayerVariables();
		cleanDataLayerVariables();
		if(DTMRule){
			activateDTMRule(DTMRule);
		}
	}

	
	
	function setDataLayerVariables(){
		if(typeof(window["analytics_dl_var"])  == "object"){
			for(key in window["analytics_dl_var"]){
				if(analytics_dl_var[key] === ""){
					delete analytics_dl_var[key];
				}
			}
			addAnalyticsData(analytics_dl_var);
		}
	}
	
	function cleanDataLayerVariables(){
	}
	
	function analytics_trackCheckinErrors(errors, areaName){
		var errorMessages = "";
		/*compute error messages*/
		if(typeof errors === "object"){
			for(key in errors){
				errorMessages = errorMessages + key + " "  + errors[key] + "; ";
			}	
			if(errorMessages.length > 0)
				errorMessages = errorMessages.slice(0,-2);
		}
		else if (typeof errors === "string"){
			errorMessages = errors;
		}
		if(errorMessages.length > 0){
			pushAnalyticsData({"CarnetError": errorMessages});
			activateDTMRule("CarnetError");
		}
	}
	
	
	