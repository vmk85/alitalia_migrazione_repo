$(document).ready(function() {
	$("#checkbox-voli-diretti").click(timeTable_filterdirectFlights);
	$(".flightInfo").click(timeTable_flightInfoClick);
	$('.flightDetail').click(timeTable_flightDetailClick);
	$('.airportDetail').click(timeTable_airportDetailClick);
	/*Datepicker cambio data*/
	datePickerController.createDatePicker({
		 formElements:{
		  "timeTableSearchResult__changeData" : pageSettings.dateFormat,
		 },
		 "noTodayButton": true,
		 "fillGrid" : true,
		 rangeLow : new Date(),
		 noFadeEffect: true,
		 constrainSelection: false,
		 wrapped: false,
		 callbackFunctions:{
			 "datereturned": [timeTable_printDate],
			 "dateset": [timeTable_onDateSelected]
		 } 
	});
	
	
});

function timeTable_printDate(){
	var dataCercata= giornoCercato + " "
	+ pageSettings["fullMonths"][meseCercato-1]
	+ " " + annoCercato;
	if(americanCountry=="true"){
		dataCercata=pageSettings["fullMonths"][meseCercato-1]+" "+giornoCercato+" " + annoCercato;
	}
	jQuery("#timeTableSearchResult__changeData").val(dataCercata);
}


function timeTable_onDateSelected(obj){
	if(obj["dd"] !== null){
		console.log(typeof obj["dd"]);
		var refreshURL = window.location.origin + window.location.pathname; 
		var dep = getURLParameter("infovoli_departures");
		var arr = getURLParameter("infovoli_arrival");
		var month=obj["mm"]+"";
		var day=obj["dd"]+"";;	
		twoDigitMonth= month.length < 2 ? '0' + obj["mm"] : '' + obj["mm"];
		twoDigitDay=day.length <  2 ? '0' + obj["dd"] : '' + obj["dd"];	
		
		var data= twoDigitDay+"/" + twoDigitMonth + "/" + obj["yyyy"];
		if(americanCountry=="true"){
			data=twoDigitMonth+"/" + twoDigitDay + "/" + obj["yyyy"];
		}
		refreshURL = refreshURL + "?infovoli_departures=" + 
			dep + "&infovoli_arrival=" + arr + "&dataPartenza=" + 
			encodeURIComponent(data);
		window.location.href = refreshURL;
	}
	else{
		timeTable_printDate();
	}
}

function timeTable_filterdirectFlights(e) {
	var numberOfFlight = $(".timeTable__body").children().length;
	var numberOfConnecting = $(".timeTable__body").children(".hasScalo").length;
	if (numberOfFlight == numberOfConnecting) {
		$(".hasScalo").toggle();
		$("#noFlightsFound").toggle();
	} else {
		$(".hasScalo").toggle();
	}
}

function timeTable_flightInfoClick(e){
	e.preventDefault();
	var form = {
			formType : 'STATO_VOLO_BOOKING_TIME_TABLE',
			numeroVolo : $(this).attr("rel"),
			vettore : $(this).attr("vect"),
			dataPartenza :  $('#dataCercata').val()
	};
	performSubmit("preparaviaggiosubmit", form, 
			function(data){timeTable_flightInfoSuccess(data,form)}, timeTable_flightInfoFail);
}

function timeTable_flightInfoSuccess(data, form) {
	if (data.result && data.result == "OK") {
		/*For avoiding caching*/
		$('#openLightboxFlightInfo').attr("data-link",
				$('#openLightboxFlightInfo').attr("href") 
				+ "?flight=" + encodeURIComponent(form.vettore) + encodeURIComponent(form.numeroVolo)
				+ "&date=" + encodeURIComponent(form.dataPartenza.replace(/\//g,"-")));
		$('#openLightboxFlightInfo').trigger("click");
	}
	else{
		console.error("error in flight info");
	}
}

function timeTable_flightInfoFail() {
	console.error("error in flight info");
}

function timeTable_flightDetailClick(e) {
	e.preventDefault();
	var $cont = $(this).parents(".row");
	$cont.children(".row__inner").each(function (ind, elem) {
		var arrParam = $(elem).find(".flightDetail").attr("rel").split('-');
		var data = {
			flightnum: arrParam[1],
			flightvector: arrParam[0],
			flightdate: $('#dataCercata').val()
		};
		performSubmit("flightdetailconsumer", data, 
				function(data) { timeTable_flightDetailSuccess(data, elem); }, 
				timeTable_flightDetailFail);
	});
}

function timeTable_flightDetailSuccess(data, elem) {
	$(elem).find(".flightDetail").off("click", timeTable_flightDetailClick);
	var detailsContainer = $(elem).find(".timeTableExtraInfo");
	detailsContainer.parent().find(".timeTable_Duration").html(data.duration);
	detailsContainer.find(".miles").html(data.miles);
	detailsContainer.find(".aircraft").html(data.aircraft);
}

function timeTable_flightDetailFail() {
	console.error("error in flight details");
}

function timeTable_airportDetailClick(e) {
	e.preventDefault();
	var airportCode = $(this).children(".timeTable__iata").text();
	var $elem = $(this);
	var form = { airportCode: airportCode };
	performSubmit("airportdetailsconsumer", form, 
			function(data) { timeTable_airportDetailSuccess(data, form); }, 
			timeTable_airportDetailFail);
}

function timeTable_airportDetailSuccess(data, form) {
	if (data.result && data.result == "OK") {
		$('#openLightboxAirportDetails').attr("data-link",
				$('#openLightboxAirportDetails').attr("href") 
				+ "?apt=" + encodeURIComponent(form.airportCode));
		$("#openLightboxAirportDetails").trigger("click");
	}
	else{
		console.error("error in airport details");
	}
}

function timeTable_airportDetailFail() {
	console.error("error in airport details");
}