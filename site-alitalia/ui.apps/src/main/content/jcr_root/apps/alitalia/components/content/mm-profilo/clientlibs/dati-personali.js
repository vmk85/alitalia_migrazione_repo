setI18nTranslate('millemiglia.datiPersonali.error');
setI18nTranslate('millemiglia.datiPersonali.ajaxError');
setI18nTranslate('millemiglia.datiPersonali.success');
$('#addressHome').click(function() {
	$('#nomeAzienda').prop('disabled', true);
	$('#nomeAzienda').val('');
	$("#nomeAzienda").parent().removeClass("isError");
});

$('#addressOffice').click(function() {
 $('#nomeAzienda').prop('disabled', false);
});

$('#datiPersonaliSumbit').bind('click', function(e) {
	validation(e, 'datipersonalisubmit', '#datiPersonaliEdit',
			datiPersonaliEditSuccess, datiPersonaliEditError);
	return false;
});

function datiPersonaliEditSuccess(data) {
	if (data.result) {
		removeErrors();
		$('#datiProfiloSumbit').unbind('click');
		$('#datiPersonaliEdit').submit();
	} else {
		showErrors(data);
		showFormFeedbackError('datiPersonaliEdit',
				'millemiglia.datiPersonali.error');
		if (typeof(data.fields["cap"]) != "undefined") {
			if (typeof(window["analytics_CAP_Error"]) === "function"){
				window["analytics_CAP_Error"]();
			}
		}
	}
	return false;
}

function datiPersonaliEditError(data) {
	showFormFeedbackError('datiPersonaliEdit',
			'millemiglia.datiPersonali.ajaxError');
	return false;
}

$(window).on("load", function (e) {

	var $accordionDatiPersonali = $('#accordion1_tab1');
	if (resultDatiPersonaliSubmit == "true") {
		$accordionDatiPersonali.click();
		showFormFeedbackSuccess('datiPersonaliEdit',
				'millemiglia.datiPersonali.success');
	} else if (resultDatiPersonaliSubmit == "false") {
		$accordionDatiPersonali.click();
		showFormFeedbackError('datiPersonaliEdit',
				'millemiglia.datiPersonali.ajaxError');
	}
	if (resultSocialLoginSubmit == "true"
		|| resultSocialLoginSubmit == "false") {
		$accordionDatiPersonali[0].scrollIntoView();
	}
	return false;
});
/*
$(window).load(function() {
	var $accordionDatiPersonali = $('#accordion1_tab1');
	if (resultDatiPersonaliSubmit == "true") {
		$accordionDatiPersonali.click();
		showFormFeedbackSuccess('datiPersonaliEdit',
				'millemiglia.datiPersonali.success');
	} else if (resultDatiPersonaliSubmit == "false") {
		$accordionDatiPersonali.click();
		showFormFeedbackError('datiPersonaliEdit',
				'millemiglia.datiPersonali.ajaxError');
	}
	if (resultSocialLoginSubmit == "true"
		|| resultSocialLoginSubmit == "false") {
		$accordionDatiPersonali[0].scrollIntoView();
	}
	return false;
});
*/
$('div.customSelect').find('select#nazione').on('change', function(e) {
	defaultSelectedCountry = $('div.customSelect').find('select#nazione')
		.find(':selected').val();
	
	var requestData = {
		'states': defaultSelectedCountry
	};
	
	getDropdownData(requestData, dropdownDataSuccess, dropdownDataFail);
	
	function dropdownDataSuccess(data) {
		$('div.customSelect').find('select#provincia').empty();
		var states = data.states;
		for (index in states) {
			var option = '';
			if (states[index].code == defaultSelectedProvincia) {
				option = '<option value="' + states[index].code + '" selected>'
					+ states[index].name + '</option>';
			} else {
				option = '<option value="' + states[index].code + '">'
					+ states[index].name + '</option>';
			}
			$('div.customSelect').find('select#provincia').append(option);
		}
	}
	
	function dropdownDataFail() {
		return false;
	}
	
	return false;
	
});

$(document).ready(function() {
	var requestData = {
		'countries': defaultSelectedCountry,
		'professioni': defaultSelectedProfessione,
		'tipiTelefono': defaultSelectedTipoTelefono,
		'prefissiNazionali': defaultSelectedPrefissoNazionale,
		'states': defaultSelectedCountry,
        'secureQuestions': defaultSelectedDomanda
	};

	getDropdownData(requestData, dropdownDataSuccess, dropdownDataFail);

	function dropdownDataSuccess(data) {
		var countries = data.countries;
		for (index in countries) {
			var option = '';
			if (countries[index].code == defaultSelectedCountry) {
				option = '<option value="' + countries[index].code + '" selected>'
					+ countries[index].description + '</option>';
			} else {
				option = '<option value="' + countries[index].code + '">'
					+ countries[index].description + '</option>';
			}
			$('div.customSelect').find('select#nazione').append(option);
		}

		var states = data.states;
		for (index in states) {
			var option = '';
			if (states[index].code == defaultSelectedProvincia) {
				option = '<option value="' + states[index].code + '" selected>'
					+ states[index].name + '</option>';
			} else {
				option = '<option value="' + states[index].code + '">'
					+ states[index].name + '</option>';
			}
			$('div.customSelect').find('select#provincia').append(option);
		}

		var professioni = data.professioni;
		for (index in professioni) {
			var option = '';
			if (professioni[index].name == defaultSelectedProfessione) {
				option = '<option value="' + professioni[index].name
					+ '" selected>'
					+ professioni[index].value + '</option>';
			} else {
				option = '<option value="' + professioni[index].name + '">'
					+ professioni[index].value + '</option>';
			}
			$('div.customSelect').find('select#professione').append(option);
		}

		var tipiTelefono = data.tipiTelefono;
		$('div.customSelect').find('select#tipoTelefono').append('<option></option>');
		for (index in tipiTelefono) {
			var option = '';
			if (tipiTelefono[index].name == defaultSelectedTipoTelefono) {
				option = '<option value="' + tipiTelefono[index].name
					+ '" selected>'
					+ tipiTelefono[index].value + '</option>';
			} else {
				option = '<option value="' + tipiTelefono[index].name + '">'
					+ tipiTelefono[index].value + '</option>';
			}
			$('div.customSelect').find('select#tipoTelefono').append(option);
		}

		var prefissiNazionali = data.prefissiNazionali;
		$('div.customSelect').find('select#prefissoNazionale').append('<option></option>');
		for (index in prefissiNazionali) {
			var option = '';
			var optionForSA = '';
			//defaultSelectedPrefissoSA
			var differentForSA = false;
			if (prefissiNazionali[index].prefix == defaultSelectedPrefissoNazionale) {
				option = '<option value="' + prefissiNazionali[index].prefix
					+ '" selected>' + prefissiNazionali[index].description
					+ ' (+' + prefissiNazionali[index].prefix + ') </option>';
			} else {
				option = '<option value="' + prefissiNazionali[index].prefix
					+ '">' + prefissiNazionali[index].description
					+ ' (+' + prefissiNazionali[index].prefix + ') </option>';
			}
			if (prefissiNazionali[index].prefix == defaultSelectedPrefissoSA){
			    optionForSA = '<option value="' + prefissiNazionali[index].prefix
            					+ '" selected>' + prefissiNazionali[index].description
            					+ ' (+' + prefissiNazionali[index].prefix + ') </option>';
			}else{
			    optionForSA = option;
			}
			$('div.customSelect').find('select#prefissoNazionale').append(option);
            $('div.customSelect').find('select#nationalPrefix').append(optionForSA);
		}

        var secretQ = data.secureQuestions;


        for (index in secretQ ) {
			var option = '';
			if (secretQ[index].id == defaultSelectedDomanda) {
				option = '<option value="' + secretQ[index].id
					+ '" selected>' + secretQ[index].description
					 '</option>';
			} else {
				option = '<option value="' + secretQ[index].id
					+ '">' + secretQ[index].description
					+  '</option>';
			}
			$('div.customSelect').find('select#domanda').append(option);
        }
        //$("#domanda option").each(function()
        //{
        //    alert('a');
            // Add $(this).val() to your list
        //});


		return false;
	}
	
	function dropdownDataFail() {
		return false;
	}

    cellulare.oninput = function () {
        if (this.value.length > 12) {
            this.value = this.value.slice(0,12);
        }
    }

	return false;
});