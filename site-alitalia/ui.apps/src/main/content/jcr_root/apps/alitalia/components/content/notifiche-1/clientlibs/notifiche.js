var headerNotificationOptions = {};


// document ready
$( document ).ready( function() {
    headerNotificationOptions.openPanel();
    headerNotificationOptions.deleteNotification();
    headerNotificationOptions.checkDrilldownOpen();
} );


headerNotificationOptions.cloneItems = function() {
    var el = $('.notification-cta .notification-wrap').find('ul').eq(0);
    el.clone().addClass('menu').appendTo('.mobile-notify-wrap');
};

headerNotificationOptions.openPanel = function() {
    // desktop version
    $('.notification-label').click(
        function(){
            $('.notification-count').hide();
            window.setTimeout(function() {
                $('.notification-cta').toggleClass('active');
            }, 10);
            if($('.notification-wrap').hasClass('closed')){
                $('.notification-wrap').slideUp(50).toggleClass('closed');
            } else {
                $('.notification-wrap').slideDown(50).toggleClass('closed');
            }
        }
    )
};

headerNotificationOptions.checkDrilldownOpen = function (event) {
    // mobile version
    var _target = $(event.target).find('.is-active');
    if(_target.closest('.mobile-notify-wrap').length) {
        $('.notification-count').hide();
    }
}

headerNotificationOptions.deleteNotification = function() {
    $('.icon--close--black').click(
        function(){
            var context = $(this).closest('.notification-list');

            // height after close notification
            var calculatedNextHeight = $('.is-drilldown').height() - $(this).closest('.notification-single-item').outerHeight();

            $(this).parent('.notification-single-item').remove();

            setTimeout(function() {
                    // fix container height
                    $('.is-drilldown').css({
                            'height': calculatedNextHeight,
                    })
            }, 10);


            if($('.notification-single-item', context).length <= 1 ){
                $('.no-notification').show();

                setTimeout(function() {
                        // fix container height
                        $('.is-drilldown').css({
                                'height': calculatedNextHeight + $('.is-submenu-item.no-notification').outerHeight(),
                        })
                }, 50);
            }
        }
    )
};
