
function getServiceUrl(service, selector, secure) {
	if (selector == 'login') {
		return service;
	}
	selector = selector || "json";
	var protocol = location.protocol;
	/*
	if (jsonProtocol && (selector == "json" || secure)) {
		protocol = 'https:';
	}
	*/

	return protocol + '//' + location.host + internalUrl1 + "/." + service + "." + selector;
}

function getServiceUrl_1(service, selector) {
    if (selector == 'login') {
        return service;
    }
    selector = selector + ".json"|| "json";
    var protocol = location.protocol;

    return protocol + '//' + location.host + internalUrl1 + "/." + service + "." + selector;
}

var all_airports;
/*
function getAirportsList() {
	var url = getServiceUrl("airportslistconsumer");

    $.ajax({
     complete: function(){
    	
        },
     error: function(jqXHR,  textStatus,  errorThrown ){
        //alert(textStatus);
        //alert(errorThrown);
        
        },
     type: 'GET',    
     url:url,
     success: function(msg){

		all_airports = msg.airports;
        //alert(msg);
     }
     });
}
*/


function getAutocompleteSize() {
	
	return ["zh","ja","ko"].indexOf(language) > -1 ? 2 : 3;
}


$(function () {

	function sortAirportAndFilterList(query) {
		
		var list = [];
		var keys = [];
		//debugger;
		/*
		jQuery.each(all_airports, function(i, v) {
			list.push(v);
		});
		*/
		//list.push(all_airports[3]);
		//return list;
		
		if (query && query.length >= getAutocompleteSize()) {
			var size = 7;
			keys = [];
			var regExString = "^" + query + ".*";
			jQuery.each(all_airports, function(i, v) {
				if (keys.length < size
						&& v.city.search(new RegExp(regExString, "i")) != -1) {
					if (-1 === keys.indexOf(v.code)) {
						keys.push(v.code);
						list.push(v);
					}
				}
			});
			jQuery.each(all_airports, function(i, v) {
				if (keys.length < size
						&& v.airport.search(new RegExp(regExString, "i")) != -1) {
					if (-1 === keys.indexOf(v.code)) {
						keys.push(v.code);
						list.push(v);
					}
				}
			});
			if (query.length == 3) {
				jQuery.each(all_airports, function(i, v) {
					if (keys.length < size
							&& v.code.search(new RegExp(regExString, "i")) != -1) {
						if (-1 === keys.indexOf(v.code)) {
							keys.push(v.code);
							list.push(v);
						}
					}
				});
			}
		}
		return list;
	}
	
	
	function airportSource(request, response) {
			
		//var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
        response($.grep(sortAirportAndFilterList(request.term), function (value) {
            return true;//matcher.test(value.airport) || matcher.test(value.code) || matcher.test(value.city);
        }))
	}

	
	/*
	 source: function (request, response) {
        	airportSource(request, response);
        }
	 */

    $("#departureInputHoriz").autocomplete({
        source: function (request, response) {
        	airportSource(request, response);
        },
        select: function (event, ui) {
            var text = ui.item.city + " " + ui.item.code;

            $("#" + $(this).attr("id") + "_country").val(event.country);
            $(".j-countryCode").val(event.code);
            $("#Origin").val(ui.item.code);
            $("#originKeyCityCode").text(event.keyCityCode);
            $("#originKeyCountryCode").text(event.keyCountryCode);
            $("#originKeyAirportName").text(event.keyAirportName);


            $("#departureInputHoriz").val(text);
            $("#airportCode").val(ui.item.code);
            $("#Sapt").text(ui.item.code);
            $("#Scity").text(ui.item.city);
            $("#departure_sugg").hide();
            $(".ui-helper-hidden-accessible").hide();
            return false;
        },
        open: function () {
            $(this).autocomplete('widget').css('z-index', 3000);
            $(this).autocomplete('widget').removeClass('ui-widget-content');
            $(".ui-helper-hidden-accessible").hide();
            return false;
        },
        messages: {
            noResults: '',
            results: function () { }
        },
         minLength: getAutocompleteSize()
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li></li>")
           .append("<div class=\"autocomplete-suggestion-horiz\" data-index=\"0\"><div class=\"sugg_dest_data-horiz\"><span class=\"sugg_city_name-horiz toPass \">" + item.city  + "</span> - <span class=\"sugg_city_nation-horiz\">" + item.country + "</span><br><span class=\"sugg_airport-horiz\">" + item.airport + "</span><span class=\"sugg_airportCode-horiz\">(" + item.code + ")</span></div></div>")
           .appendTo(ul);
    };


    $("#arrivalInputHoriz").autocomplete({
    	source: function (request, response) {
        	airportSource(request, response);
        },
        select: function (event, ui) {
            var text = ui.item.city + " " + ui.item.code;

		$("#" + $(this).attr("id") + "_country").val(event.country);
		$("#Destination").val(ui.item.code);
		$("#destinationKeyCityCode").text(event.keyCityCode);
		$("#destinationKeyCountryCode").text(event.keyCountryCode);
		$("#destinationKeyAirportName").text(event.keyAirportName);

            $("#arrivalInputHoriz").val(text);
            $("#arrivaAirportCode").val(ui.item.code);
            $("#Sapt2").text(ui.item.code);
            $("#Scity2").text(ui.item.city);
            $("#destination_sugg").hide();
            $(".ui-helper-hidden-accessible").hide();
            return false;
        },
        open: function () {
            $(this).autocomplete('widget').css('z-index', 3000);
            $(this).autocomplete('widget').removeClass('ui-widget-content');
            $(".ui-helper-hidden-accessible").hide();
            return false;
        },
        messages: {
            noResults: '',
            results: function () { }
        },
        minLength: getAutocompleteSize()
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li></li>")
           .append("<div class=\"autocomplete-suggestion-horiz\" data-index=\"0\"><div class=\"sugg_dest_data-horiz\"><span class=\"sugg_city_name-horiz toPass \">" + item.city  + "</span> - <span class=\"sugg_city_nation-horiz\">" + item.country + "</span><br><span class=\"sugg_airport-horiz\">" + item.airport + "</span><span class=\"sugg_airportCode-horiz\">(" + item.code + ")</span></div></div>")
           .appendTo(ul);
    };

    $("#departureInputOverlay").autocomplete({
    	source: function (request, response) {
        	airportSource(request, response);
        },
        select: function (event, ui) {
            var text = ui.item.city + " " + ui.item.code;

            $("#" + $(this).attr("id") + "_country").val(event.country);
            $(".j-countryCode").val(event.code);
            $("#Origin").val(ui.item.code);
            $("#originKeyCityCode").text(event.keyCityCode);
            $("#originKeyCountryCode").text(event.keyCountryCode);
            $("#originKeyAirportName").text(event.keyAirportName);
            $("#departureInputHoriz").val(text);
            $("#departureInputOverlay").val(text);


            $("#airportCode").val(ui.item.code);
            $("#Sapt").text(ui.item.code);
            $("#Scity").text(ui.item.city);
            $("#departure_sugg").hide();
            $(".ui-helper-hidden-accessible").hide();
            $(".depOverlay_close").click();
            return false;
        },
        open: function () {
            $(this).autocomplete('widget').css('z-index', 3000);
            $(this).autocomplete('widget').removeClass('ui-widget-content');
            $(".ui-helper-hidden-accessible").hide();
            return false;
        },
        messages: {
            noResults: '',
            results: function () { }
        },
        minLength: getAutocompleteSize()
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li></li>")
           .append("<div class=\"autocomplete-suggestion-horiz\" data-index=\"0\"><div class=\"sugg_dest_data-horiz\"><span class=\"sugg_city_name-horiz toPass \">" + item.city  + "</span> - <span class=\"sugg_city_nation-horiz\">" + item.country + "</span><br><span class=\"sugg_airport-horiz\">" + item.airport + "</span><span class=\"sugg_airportCode-horiz\">(" + item.code + ")</span></div></div>")
           .appendTo(ul);
    };

    $("#arrivalInputOverlay").autocomplete({
    	source: function (request, response) {
        	airportSource(request, response);
        },
        select: function (event, ui) {
            var text = ui.item.city + " " + ui.item.code;

            		$("#" + $(this).attr("id") + "_country").val(event.country);
		$("#Destination").val(ui.item.code);
		$("#destinationKeyCityCode").text(event.keyCityCode);
		$("#destinationKeyCountryCode").text(event.keyCountryCode);
		$("#destinationKeyAirportName").text(event.keyAirportName);
         $("#arrivalInputHoriz").val(text);
            $("#arrivalInputOverlay").val(text);
            $("#arrivaAirportCode").val(ui.item.code);
            $("#Sapt2").text(ui.item.code);
            $("#Scity2").text(ui.item.city);
            $("#destination_sugg").hide();
            $(".arrOverlay_close").click();
            $(".ui-helper-hidden-accessible").hide();
            return false;
        },
        open: function () {
            $(this).autocomplete('widget').css('z-index', 3000);
            $(this).autocomplete('widget').removeClass('ui-widget-content');
            $(".ui-helper-hidden-accessible").hide();
            return false;
        },
        messages: {
            noResults: '',
            results: function () { }
        },
        minLength: getAutocompleteSize()
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li></li>")
           .append("<div class=\"autocomplete-suggestion-horiz\" data-index=\"0\"><div class=\"sugg_dest_data-horiz\"><span class=\"sugg_city_name-horiz toPass \">" + item.city  + "</span> - <span class=\"sugg_city_nation-horiz\">" + item.country + "</span><br><span class=\"sugg_airport-horiz\">" + item.airport + "</span><span class=\"sugg_airportCode-horiz\">(" + item.code + ")</span></div></div>")
           .appendTo(ul);
    };

})

