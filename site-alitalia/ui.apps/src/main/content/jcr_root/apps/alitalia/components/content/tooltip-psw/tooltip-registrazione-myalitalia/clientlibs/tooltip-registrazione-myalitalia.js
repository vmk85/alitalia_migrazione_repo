$(document).ready(function () {
    manageTooltipMaRegistration();
});

function manageTooltipMaRegistration(){
    $(document).on("keyup", ".password", manageLevelTooltipMaRegistration);
    $(document).on("click", ".password", function () {
        $(".password").trigger("keyup");
    });

    $(document).on("focusout", ".password", function () {
        $(".tooltiptext").hide();
    });
}

function manageLevelTooltipMaRegistration() {
    var current = $(this);
    var popup = $(".tooltiptext");
    if (current.val().length > 4) {
        popup.show();
        switch (validateTooltipMaRegistration($("#pass").val().trim())) {

            case 0:
            case 1:
                $("#colorlevel").addClass("level-1");
                $("#textlevel").html(CQ.I18n.get("psw.text.level.1")).css("color","red");
                break;
            case 2:
                $("#colorlevel").addClass("level-2");
                $("#textlevel").html(CQ.I18n.get("psw.text.level.2")).css("color","green");
                break;
            case 3:
                $("#colorlevel").addClass("level-3");
                $("#textlevel").html(CQ.I18n.get("psw.text.level.3")).css("color","#4ABABC");
                break;
            case 4:
                $("#colorlevel").addClass("level-4");
                $("#textlevel").html(CQ.I18n.get("psw.text.level.4")).css("color","#4ABABC");
                break;
        }
    }
    else {
        popup.hide();
    }
}

function validateTooltipMaRegistration(psw){
    var pointValid = 0;

    var M = /[A-Z]/;
    var m = /[a-z]/;
    var number = /[0-9]/;
    var special = /\W/;
    var space = /^\S+$/;

    if (psw.length >= 8 && space.test(psw)) {

        if(M.test(psw)){
            pointValid += 1;
        }
        if(m.test(psw)){
            pointValid += 1;
        }
        if(number.test(psw)){
            pointValid += 1;
        }
        if(special.test(psw)){
            pointValid += 1;
        }

    }
    $("#colorlevel").attr("class","");

    return pointValid;
}