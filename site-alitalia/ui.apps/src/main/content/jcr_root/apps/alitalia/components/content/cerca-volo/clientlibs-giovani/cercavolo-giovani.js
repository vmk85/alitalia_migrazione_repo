function setYoung() {
	jQuery(".typeOfFlight").find("#giovaniLastMinute").prop("checked", false);
	jQuery(".typeOfFlight").find("#giovani").prop("checked", true);

	jQuery("#specialOffers__tab--2").removeClass('tabActive');
	jQuery("#specialOffers__tab--1").addClass('tabActive');
	jQuery(".j-tabsContainer[data-tab='1']").removeClass('tabActive');
	jQuery(".j-tabsContainer[data-tab='0']").addClass('tabActive');

	jQuery(".dateOfFlight").find("ol#lastMinuteSelect").hide();
	jQuery(".dateOfFlight").find("ol#datePicker").show();

	jQuery(".dateOfFlight").find("#ritorno").val("");
	if (jQuery('input[name=SearchType]:checked', '#cercaVoliForm').val() === 'a') {
		jQuery(".dateOfFlight").find("#ritorno").attr("disabled", "disabled").attr("aria-disabled", "true");
	} else {
		jQuery(".dateOfFlight").find("#ritorno").removeAttr("disabled").removeAttr("aria-disabled");
	}
	jQuery(".dateOfFlight").find("#andata").val("");

}

function setLastMinute() {
	jQuery(".typeOfFlight").find("#giovani").prop("checked", false);
	jQuery(".typeOfFlight").find("#giovaniLastMinute").prop("checked", true);

	jQuery(".dateOfFlight").find("#andata").attr("value", "");
	jQuery(".dateOfFlight").find("#ritorno").attr("value", "");

	jQuery("#specialOffers__tab--1").removeClass('tabActive');
	jQuery("#specialOffers__tab--2").addClass('tabActive');
	jQuery(".j-tabsContainer[data-tab='0']").removeClass('tabActive');
	jQuery(".j-tabsContainer[data-tab='1']").addClass('tabActive');

	jQuery(".dateOfFlight").find("ol#datePicker").hide();
	jQuery(".dateOfFlight").find("ol#lastMinuteSelect").show();

	if (jQuery('input[name=SearchType]:checked', '#cercaVoliForm').val() === 'a') {
		
		jQuery(".dateOfFlight").find("#returnDate").attr("disabled", "disabled").attr("aria-disabled", "true").parent()
		.velocity({
			opacity : 0.3
		});
	} else {
		
		jQuery(".dateOfFlight").find("#returnDate").removeAttr("disabled").removeAttr("aria-disabled").parent().velocity({
			opacity : 1
		});
	}

	filterDepartureDate();
	filterArrivalDate();

}

var daysOfWeek = [];
var monthsOfYear = [];
/* offset sets the number of days after today the function should return */
function getToday(offset, compact) {
	if (isNaN(offset)) {
		offset = 0;
	}
	if (daysOfWeek.length == 0) {
		daysOfWeek = pageSettings["fullDays"];

		/*
		 * jQuery("ol#daysOfWeek").children().each(function() {
		 * daysOfWeek.push(jQuery(this).text()); });
		 */
	}
	if (monthsOfYear.length == 0) {
		monthsOfYear = pageSettings["fullMonths"];
		/*
		 * jQuery("ol#monthsOfYear").children().each(function() {
		 * monthsOfYear.push(jQuery(this).text()); });
		 */
	}
	var today = new Date();
	today.setDate(today.getDate() + offset);
	var ww = (today.getDay() + 6) % 7;
	var dd = today.getDate();
	var mm = today.getMonth(); // January is 0!
	var yyyy = today.getFullYear();

	if (dd < 10) {
		dd = '0' + dd
	}
	
	if (compact){
		today = dd + "/" + (mm+1) + "/" + yyyy;
	}
	else{
		today = daysOfWeek[ww] + ", " + dd + " " + monthsOfYear[mm] + " " + yyyy;
	}
	today = jQuery('<textarea />').html(today).text();
	return today;
}

/* acquire a date in format "<dayOfWeek>, <dd> <monthOfYear> <yyyy>" */
function isToday(date) {
	var today = getToday(0);
	return today == date;
}

function filterDepartureDate() {
	dateSelect = jQuery("#departureDate");
	today = getToday();
	todayVal = getToday(0, true);
	tomorrow = getToday(1);
	tomorrowVal = getToday(1, true);
	/* dateSelect.empty(); */
	dateSelect.find('option:gt(0)').remove();
	dateSelect.append("<option value=\"" + todayVal + "\">" + today + "</option>");
	dateSelect.append("<option value=\"" + tomorrowVal + "\">" + tomorrow + "</option>");
}

function filterArrivalDate() {
	departureDate = jQuery("select#departureDate option:selected").val();
	var maxOffset = 3;
	if (departureDate && !isToday(departureDate)) {
		maxOffset = 4;
	}
	dateSelect = jQuery("#returnDate");
	/* dateSelect.empty(); */
	dateSelect.find('option:gt(0)').remove();
	for (i = maxOffset - 3; i <= maxOffset; i++) {
		arrDate = getToday(i)
		arrDateVal = getToday(i,true);
		dateSelect.append("<option value=\"" + arrDateVal + "\">" + arrDate + "</option>");
	}

}

jQuery(document).ready(function() {

	/* Switch from Young to Young Last Minute and vice-versa */
	jQuery(".typeOfFlight").find("#giovani").on("click", function(evt) {
		setYoung();
	});
	jQuery(".typeOfFlight").find("#giovaniLastMinute").on("click", function(evt) {
		//setLastMinute();
	});
	
	jQuery("#specialOffers__tab--1").children().on("click", function() {
		setYoung();
	});
	jQuery("#specialOffers__tab--2").children().on("click", function() {
		//setLastMinute();
	});

	/* needed if one press back button in browser */
	if (jQuery("input[name='SearchType_Giovani']:checked").val() == "JumpUP") {
		//setLastMinute();
	}

	/*
	 * Enable and disable departure date in either Young and Young Last Minute
	 * offers
	 */
	jQuery(".typeOfFlight").find("#andataeritorno").on("click", function() {
		jQuery(".dateOfFlight").find("#ritorno").prop("disabled", false);
		jQuery(".dateOfFlight").find("#returnDate").prop("disabled", false);

	});
	jQuery(".typeOfFlight").find("#soloandata").on("click", function() {
		jQuery(".dateOfFlight").find("#ritorno").prop("disabled", true);
		jQuery(".dateOfFlight").find("#returnDate").prop("disabled", true);
	});

	/* Filter date if is last minute
	jQuery(".itinerary.itinerary--innerFlightFinder").find("#arrivalInput").on("click", function() {
		if (jQuery("input#giovaniLastMinute").is(':checked')) {
			filterDepartureDate();
			filterArrivalDate();
		}
	});*/

	/* Needed for correct post request */
	jQuery(".dateOfFlight").find("#returnDate").on("change", function() {
		jQuery(".dateOfFlight").find("#ritorno").val(jQuery(this).val());
	});
	jQuery(".dateOfFlight").find("#departureDate").on("change", function() {
		jQuery(".dateOfFlight").find("#andata").val(jQuery(this).val());
		filterArrivalDate();
	});

});
