"use strict";
use(function() {
    var code = this.code;
    var response = {};
    response.value = "0";
    response.email = false;
    response.app = false;

    if(code == "006" || code == "007" || code == "008"){
        response.value = "1";
    }
    if(code == "009" || code == "010" || code == "011"){
        response.value = "2";
    }
    if(code == "005"){
        response.value = "0";
        response.show = false;
    }

    if(code == "008" || code == "011"){
        response.email = true;
        response.app = true;
    }

    if(code == "006" || code == "009"){
        response.email = true;
    }
    if(code == "007" || code == "010"){
        response.app = true;
    }
    if(code==null || code==""){
        response.show = false;
    }


    return response;
})