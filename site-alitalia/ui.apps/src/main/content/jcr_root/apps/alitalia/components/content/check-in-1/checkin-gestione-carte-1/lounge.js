"use strict";
use(function () {

    var segmentsJ = this.segments;
    var passenger = this.pax;
    var emdLg = new Array();

    for (var i = 0; i < segmentsJ.size(); i++) {
        var segment = segmentsJ.get(i);
        for (var i = 0; i < segmentsJ.size(); i++) {

            for (var x = 0; x < segment.passengers.size(); x++) {
                var pax = segment.passengers.get(x);
                if (pax.nome == passenger.nome && pax.cognome == passenger.cognome && pax.emd.size() != null) {
                    for (var y = 0; y < pax.emd.size(); y++) {
                        var emd = pax.emd.get(y);
                        if (emd.group == 'LG') {
                            var lg = {
                                origin: segment.origin.name,
                                code: emd.number
                            };
                            emdLg.push(lg);
                        }
                    }
                }
            }
        }
    }
    return emdLg;
});