setI18nTranslate("Italian");
String.prototype.replaceAt=function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
};
// TODO il seguente modulo e la pagina millemiglia-recupera-credenziali.html sono ad oggi (10/08/2018) in lavorazione
//
// Allineare gli Id dei campi/form della maschera html (Da Ultimare)
// Definire il o i selettori per invocare il metodo performSubmit della Servlet : MillemigliaRecuperaCredenzialiServlet
// Verificare l'avanzamento del campo 'stato' del model, per fa evolvere correttamente il flusso

var defaultSelectedCountry = "";
var defaultSelectedProfessione = "";
var defaultSelectedTipoTelefono = "";
var defaultSelectedPrefissoNazionale = "";
var defaultSelectedLinguaCorrispondenza = "";
var defaultSelectedTipoPosto = "";
var defaultSelectedTipoPasto = "";
var defaultSelectedProvincia = "";
var defaultSelectedSecretQuestions = "";
var nation = "";
var bClickedMM;


var registrazione = true;
var adeguamento = false;
var modifica = false;
var checkPresenza = false;

var flagsVerifica = {

    flagVerificaUserName_OK : false,
    flagVerificaPassword_OK : false,
    flagVerificaCellulare_OK : false,
    flagVerificaRisposta_OK : false,
    flagVerificaEmailUser_OK : false,
    flagVerifica_OTP_DS_OK : false
};

//mock
var valoreConfrontoSicurezza = null ; // E' il valore che scrivi nella maschera o per codiceotp o per risposta segreta;
var currentPageUrl = 'https://www.alitalia.it';
var homePageUrl = 'https://www.alitalia.it';

$(document).ready(function() {
    var otpSms = {
        phone: "0"
    };

    $('#RecuperoCredenzialiSubmit').bind('click', RecuperoCredenzialiHandler);
    $('#RecuperoCredenzialiRestart').bind('click', RecuperoCredenzialiRestartHandler);

    // TODO Riattivare per OTP
     //per chiudere le form modali
     $('.mm-recupera-credenziali .reveal .icon.icon--close').on('click',function() {
         $('.reveal-overlay.mm-recupera-credenziali').css('display','none');
         $('.mm-recupera-credenziali .reveal').css('display','none');
     });

    // TODO Riattivare per OTP
    //bottone che utente clicca per verifica gli OTP
    //$('.choiceOTP_Mode').on('click',sceltaOTP());

    // TODO Riattivare per OTP
    //bottone che utente clicca per verifica gli OTP
    //$('.insertCodeSMS_OTP').on('click',verificaOTP());

    // da verificare
    // $('#RecuperoCredenzialiSubmit').on('click',function(e){
    //     e.preventDefault();e.stopPropagation();
    //
    // });
    var passo = $('#passo').val();
    var retrieveResetChoice = $('#retrieveResetChoice').val();

    if (passo == 3 && retrieveResetChoice == "retrieveUsername") {
        RecuperoCredenzialiHandler();
    }
});

function RecuperoCredenzialiRestartHandler(e) {
    $('#passo').val(0);
    RecuperoCredenzialiHandler(e);
    return false;
}

function RecuperoCredenzialiHandler(e) {
    //e.preventDefault();
	//enableLoaderMyAlitalia();
    console.log("RecuperoCredenzialiHandler(e) ");
    var passo = $('#passo').val();
    // console.log("passo : ", passo);

    // TODO da decommentare quando riattivati gli OTP
     if (passo == 3) {
            //$('#flagVerifica_OTP_DS_OK').val("True");
         var sendotp=true;
         var retrieveResetChoice = $('#retrieveResetChoice').val();
         if (retrieveResetChoice == "resetPassword"){
            var password = $('#password').val();
            var confermaPassword = $('#confermaPassword').val();

            if(!isNotEmpty(password)){

                sendotp=false;
                var data = {
                    isSuccess: false,
                    fields:{
                        "password": CQ.I18n.get("message.generic.emptyfield")
                    }
                };
                showErrors(data);

            }else if (!isNotEmpty(confermaPassword)) {
                sendotp=false;
                var data = {
                    isSuccess: false,
                    fields:{
                        "confermaPassword": CQ.I18n.get("message.generic.emptyfield")
                    }
                };
                showErrors(data);

            }else if (password !==confermaPassword) {
                 sendotp=false;
                var data = {
                    isSuccess: false,
                    fields:{
                        "confermaPassword": CQ.I18n.get("specialpage.recuperacredenziali.message.nopasswordsmatching")
                    }
                };
                showErrors(data);

             }

         }
         if(sendotp==true){
            //se c'è più di una opzione per l'otp rendo visibile il popup disambiguatore
             if (cellulareSAVerificato == "true" && rispostaSAVerificato == "true")  {
                 $('.reveal-overlay.mm-recupera-credenziali.modal_otp_ds_choice').css('display','block');
                 $('.mm-recupera-credenziali.modal_otp_ds_choice .reveal').css('display','block');
             } else if (cellulareSAVerificato == "true") {
                //se c'è solo il cellulare come otp faccio direttamente otp sms senza popup disambiguatore
                 sendOTPsms();
             } else {
                //se c'è solo la domanda segreta come otp faccio direttamente otp domanda senza popup disambiguatore
                 sendOTPSecureAnswer();
             }
         }else{
            return false;
         }


     }else{

         performSubmit('millemiglia-recuperacredenziali-submit',
             '#form-millemiglia-recupera-credenziali', RecuperoCredenzialiSuccess, RecuperoCredenzialiRecuperoFail);
     }
    return false;

}



function RecuperoCredenzialiSuccess(data) {
    console.log("RecuperoCredenzialiSuccess(data), data: ",data );

    //return (data.result ? confirmSceltaRecuperoSuccess(false) : showErrors(data));
    if ((data.currentPageUrl !== undefined || data.currentPageUrl !== null || data.currentPageUrl !== "" ) && data.currentPageUrl.split('/').length>3){
        currentPageUrl = data.currentPageUrl.replace(data.currentPageUrl.split('/')[3],location.pathname.substr(1,5));
    }
    if ((data.homePageUrl !== undefined || data.homePageUrl !== null || data.homePageUrl !== "")&& data.homePageUrl.split('/').length>3){
        homePageUrl = data.homePageUrl.replace(data.homePageUrl.split('/')[3],location.pathname.substr(1,5));
    }
    if(data.isError=="FALSE"){
            var passo = $('#passo').val();
        if (passo == 4){
            window.location.href=homePageUrl;
        }else {
            window.location.href=currentPageUrl;
        }
    }else{
        showFormFeedbackError('form-millemiglia-recupera-credenziali', data.msgId);
        //showErrors(data);
    }
    return false;
}
function RecuperoCredenzialiRecuperoFail() {
    console.log("RecuperoCredenzialiRecuperoFail ");
    return false;
}



function padMobilePhonePrefix (str, max) {
    str = str.toString();
    return str.length < max ? padMobilePhonePrefix("0" + str, max) : str;
}
function sendOTPsms(){

    $('#InsertSmsOTP').css('display','block');
    $('#OtpInfoMessage').css('display','block');

    $('#DomandaSegretaText').css('display','none');
    $('#DomandaSegretaInfoMessage').css('display','none');
    $('#input_insertCode_OTP').attr('placeholder',CQ.I18n.get("specialpage.millemiglia.insertCodeOTP"));

    // todo letteger dal model
    var mobilephoneprefix =  padMobilePhonePrefix($("#certifiedCountryNumber").val(),4);   // dal model certifiedCountryNumber
    var mobilephonenumber = $("#certifiedPhoneNumber").val();                      // dal model certifiedPhoneNumber

    var data = {
        phone : mobilephoneprefix + mobilephonenumber
    };

    var number = $("#certifiedPhoneNumber").val().trim();

    for(var i = 2; i < (number.length - 3);i++)
    {
        number = number.replaceAt(i,"*");
    }

    $('#phoneEmailNumberHidden').text(number);

    $("#otpPopupTitle").html(CQ.I18n.get("specialpage.millemiglia.insertCodeSmsOtp"));
    $("#input_insertCode_OTP").attr("data-type", "SMS");


    SendSmsOtpToClientAndEraseOtpTxt();

    $('.reveal-overlay.mm-recupera-credenziali.modal_otp_sms').css('display','block');
    $('.mm-recupera-credenziali.modal_otp_sms .reveal').css('display','block');
}

function SendSmsOtpToClientAndEraseOtpTxt(){

    $('#input_insertCode_OTP').val("");

    var mobilephoneprefix =  padMobilePhonePrefix($("#certifiedCountryNumber").val(),4);   // dal model certifiedCountryNumber
    var mobilephonenumber = $("#certifiedPhoneNumber").val();                      // dal model certifiedPhoneNumber

    var data = {
        phone : mobilephoneprefix + mobilephonenumber
    };

    invokeGenericFormService('millemigliacomunicazionisubmit-sendotpsms', 'POST', '',
        sendOTPsmsSuccess, sendOTPsmsFails, sendOTPsmsAlways, undefined, data);
}

function sendOTPsmsSuccess(data){
    //feedback --> ok

    valoreConfrontoSicurezza=data.OTP;
    otpSMSExpiryDate=data.ExpiryDate;
    otpSMSSentDate=data.SentDate;

    console.log("sendOTPsmsSuccess");

    if(data.isError)
    {
        console.log("sendOTPsmsSuccess.internalError.Fake");
        //ultra fake ////
        valoreConfrontoSicurezza="123456"; //per ora... in prod togliere
        otpSMSSentDate=new Date();
        otpSMSExpiryDate="2099-12-31 10:00:00";
    }

}
function sendOTPsmsFails(data){

    //feedback --> ERROR --> impossobile inviare SMS

    //ultra fake ////
    valoreConfrontoSicurezza="123456"; //per ora... in prod togliere
    otpSMSSentDate=Date.Now();
    otpSMSExpiryDate="2099-12-31 10:00:00";

    console.log("sendOTPsmsFails");

}
function sendOTPsmsAlways(data) {
}

function sendOTPSecureAnswer(){

    $('#DomandaSegretaText').css('display','block');
    $('#InsertSmsOTP').css('display','none');

    $('#DomandaSegretaInfoMessage').css('display','block');
    $('#OtpInfoMessage').css('display','none');
    $('#input_insertCode_OTP').attr('placeholder',CQ.I18n.get("specialpage.millemiglia.insertSecureAnswer"));



    $("#otpPopupTitle").html(CQ.I18n.get("specialpage.millemiglia.insertSecureAnswer"));
    $( "#input_insertCode_OTP" ).attr( "data-type", "SecureAnswer" );

    $('#input_insertCode_OTP').val("");

    //TODO invocare servelet per recuperare OTP via EMAIL per secure ancwer
    //performSubmit(sendCodeOtpviaEmail,ecc);
    // invokeGenericFormService('millemiglia-recuperacredenziali-GetSecureAnswerMD5', 'POST', '',
    //     sendOTPEmailSuccess, sendOTPEmailFails, sendOTPEmailAlways, undefined, data);

    valoreConfrontoSicurezza=$("#rispostaSegreta_MD5").val();       // predente il valore dal model che Ã¨ gia MD5 - rispostaSegreta_MD5



    $('.reveal-overlay.mm-recupera-credenziali.modal_otp_sms').css('display','block');
    $('.mm-recupera-credenziali .reveal#modal_otp_sms').css('display','block');
}

function sendOTPEmail(){
    $('#InsertSmsOTP').show();
    $('#DomandaSegretaText').hide();

    $('#OtpInfoMessage').show();
    $('#DomandaSegretaInfoMessage').hide();
    $('#input_insertCode_OTP').attr('placeholder',CQ.I18n.get("specialpage.millemiglia.insertCodeOTP"));

    var email=$("#certifiedEmailAccount").val(); // leggere dal model - certifiedEmailAccount
    var data = {
        email : email
    };

    var email = $("#certifiedEmailAccount").val().trim();
    for(var i = 2; i < (email.length - 3);i++)
    {
        email = email.replaceAt(i,"*");
    }
    $('#phoneEmailNumberHidden').text(email);

    $("#otpPopupTitle").html(CQ.I18n.get("specialpage.millemiglia.insertCodeEmailOtp"));
    $( "#input_insertCode_OTP" ).attr( "data-type", "Email" );

    SendEmailOtpToClientAndEraseOtpTxt();

    $('.reveal-overlay.mm-recupera-credenziali.modal_otp_sms').css('display','block');
    $('.mm-recupera-credenziali.modal_otp_sms .reveal').css('display','block');
}


function SendEmailOtpToClientAndEraseOtpTxt(){

    var email=$("#certifiedEmailAccount").val(); // leggere dal model - certifiedEmailAccount
    var data = {
        email : email
    };

    $('#input_insertCode_OTP').val("");

    invokeGenericFormService('millemigliacomunicazionisubmit-sendotpemail', 'POST', '',
        sendOTPEmailSuccess, sendOTPEmailFails, sendOTPEmailAlways, undefined, data);
}

function sendOTPEmailSuccess(data){
    //feedback --> ok

    valoreConfrontoSicurezza=data.OTP;
    otpSMSExpiryDate=data.ExpiryDate;
    otpSMSSentDate=data.SentDate;

    console.log("sendOTPEmailSuccess");

    if(data.isError)
    {
        console.log("sendOTPEmailSuccess.internalError.Fake");
        //ultra fake ////
        valoreConfrontoSicurezza="123456"; //per ora... in prod togliere
        otpSMSSentDate=new Date();
        otpSMSExpiryDate="2099-12-31 10:00:00";
    }

}
function sendOTPEmailFails(data){

    //feedback --> ERROR --> impossobile inviare EMAIL

    //ultra fake ////
    valoreConfrontoSicurezza="123456"; //per ora... in prod togliere

    console.log("sendOTPEmailFails");

}
function sendOTPEmailAlways(data) {
}

//***********
//**LA FUNZIONE NON SEMBRA MAI CHIAMATA LA COMMENTO PER VERIFICARE SE LA PAGINA FUNZIONA SENZA
//**LASCIARLA PUO' FUORVIARE CHI LEGGE IL CODICE
//***********
//function otpCheckValidations(){
//    console.log("otpCheckValidations()");
//
//    if($("#input_newdelivery").val().trim()!= '' && flagsVerifica.flagVerificaCellulare_OK == false){
//        sendOTPsms();
//    }
//    else if($("#input_email").val().trim()!= '' && flagsVerifica.flagVerificaEmailUser_OK == false)
//    {
//        sendOTPEmail();
//    }
//    else if ( $('#secureAnswer').val().trim() && flagsVerifica.FlagVerificaRisposta_OK == false){
//        sendOTPSecureAnswer();
//    }
//
//    return false;
//
//}


//***********
//**LA FUNZIONE NON SEMBRA MAI CHIAMATA LA COMMENTO PER VERIFICARE SE LA PAGINA FUNZIONA SENZA
//**LASCIARLA PUO' FUORVIARE CHI LEGGE IL CODICE
//***********
//function sendOTP(){
//    console.log("sendOTP function ...");
//
//    if (flagsVerifica.flagVerificaEmailUser_OK){
//        if (checkPresenza){
//            if (flagsVerifica.flagVerificaCellulare_OK){
//                if (flagsVerifica.flagVerificaDomanda_OK){
//                }else{
//                    if ($('#secretQuestion').val().trim() != ""){
//                        sendOTPSecureAnswer();
//                    }
//                }
//            }else{
//                if ($('#input_newdelivery').val().trim() != ""){
//                    sendOTPsms();
//                }
//
//            }
//        }
//    }else{
//        if (checkPresenza){
//            if (flagsVerifica.flagVerificaCellulare_OK){
//                if (flagsVerifica.flagVerificaDomanda_OK){
//                }else{
//                    if ($('#secretQuestion').val().trim() != ""){
//                        sendOTPSecureAnswer();
//                    }
//                }
//            }else{
//                if ($('#input_newdelivery').val().trim() != ""){
//                    sendOTPsms();
//                }else{
//                    sendOTPEmail();
//
//                }
//
//            }
//            //sendOTPSecureAnswer();
//            //sendOTPsms();
//
//
//
//        }
//    }
//}
function removeData() {
    var passo = $('#passo').val();
    if (passo == 3) {
        performSubmit('millemigliaremovedatasubmit');
    }
    return false;
}

function sceltaOTP() {
    console.log("sceltaOTP()");

    //inuscita dal popup di disambiguazione di scelta del metodo di otp

    if($("#SMSOTP").is(":checked")){
    	$("#modal_otp_ds_choice").hide();
    	$(".reveal-overlay.mm-recupera-credenziali.modal_otp_ds_choice").hide();
        sendOTPsms();
    }
    else if ( $("#DSOTP").is(":checked")){
    	$("#modal_otp_ds_choice").hide();
    	$(".reveal-overlay.mm-recupera-credenziali.modal_otp_ds_choice").hide();
        sendOTPSecureAnswer();
    }
//    else if($("#MailOTP").is(":checked"))
//    {
//        sendOTPEmail();
//    }

    return false;

}

function  isOtpExpired(otpExpiryDate){
    var isExpired=true;
    try {
        var dateOtpExpiry=new Date(Date.UTC(otpExpiryDate.substr(0,4), otpExpiryDate.substr(5,2)-1, otpExpiryDate.substr(8,2), otpExpiryDate.substr(11,2), otpExpiryDate.substr(14,2), otpExpiryDate.substr(17,2)));
        var dateNow=new Date();

        isExpired=dateOtpExpiry<dateNow;

    }catch (e) {
        console.log(e);
        isExpired=true;
    }

    console.log('dateNow: ' + dateNow);
    console.log('dateOtpExpiry: ' + dateOtpExpiry);
    console.log('OtpExpired: ' + isExpired);

    return isExpired;
}

function verificaOTP() {
    flagsVerifica.flagVerifica_OTP_DS_OK= false;
    //verifico gli OTP
    var inputValue = $('#input_insertCode_OTP').val();
    var dataType = $( "#input_insertCode_OTP" ).attr( "data-type"); //discriminante

    if (dataType == "SMS")
    {
        var isExpired=isOtpExpired(otpSMSExpiryDate);
        var isNotExpired=!isExpired;

        //verificare ExpiryDate !!!!!
        if (inputValue == valoreConfrontoSicurezza && isNotExpired){
            flagsVerifica.flagVerifica_OTP_DS_OK= true;
        }else{
            smsOtpError++;
            if (smsOtpError>=   maxSsOtpTry){
                smsOtpError=0;
                $('.reveal-overlay.mm-recupera-credenziali').css('display','none');
                $('.mm-recupera-credenziali .reveal').css('display','none');
            }else{
                var errMsg=CQ.I18n.get("millemiglia.otp.error.invalid");
                var data = {
                        isSuccess: false,
                        fields:{
                            "input_insertCode_OTP": errMsg
                        }
                    };
                showErrors(data);
                $('#input_insertCode_OTP_error').css('position','relative');
                SendSmsOtpToClientAndEraseOtpTxt();
            }
        }
    }else if(dataType == "Email"){
        //verificare ExpiryDate !!!!!
        if (inputValue == valoreConfrontoSicurezza){
            flagsVerifica.flagVerifica_OTP_DS_OK= true;
        }else{
             mailOtpError++;
             if (mailOtpError>=   maxMailOtpTry){
                 mailOtpError=0;
                 $('.reveal-overlay.mm-recupera-credenziali').css('display','none');
                 $('.mm-recupera-credenziali .reveal').css('display','none');
             }else{
                 var errMsg=CQ.I18n.get("millemiglia.otp.error.invalid");
                 var data = {
                         isSuccess: false,
                         fields:{
                             "input_insertCode_OTP": errMsg
                         }
                     };
                 showErrors(data);
                 $('#input_insertCode_OTP_error').css('position','relative');
                 SendEmailOtpToClientAndEraseOtpTxt();
             }
         }
    }else if(dataType == "SecureAnswer"){
        if ($.md5(inputValue).toUpperCase() == valoreConfrontoSicurezza.toUpperCase()){// verificare che funzione MD5

            flagsVerifica.flagVerifica_OTP_DS_OK= true;
        }else{

            secureQuesOtpError++;
            if (secureQuesOtpError>=   maxSecureQuesOtpTry){
                secureQuesOtpError=0;
                $('.reveal-overlay.mm-recupera-credenziali').css('display','none');
                $('.mm-recupera-credenziali .reveal').css('display','none');
            }else{
                var errMsg=CQ.I18n.get("millemiglia.otp.error.invalid");
                var data = {
                     isSuccess: false,
                     fields:{
                         "input_insertCode_OTP": errMsg
                     }
                 };
                showErrors(data);
                $('#input_insertCode_OTP_error').css('position','relative');
                $('#input_insertCode_OTP').val("");
            }

        }
    }



    if (flagsVerifica.flagVerifica_OTP_DS_OK){
        $('.reveal-overlay.mm-recupera-credenziali').css('display','none');
        $('.mm-recupera-credenziali .reveal').css('display','none');

        $('#flagVerifica_OTP_DS_OK').val("True");

        //todo: mostrare modale conferma otpEmail, IF OK procedi ELSE mantenere comportamento di REGISTRAZIONE
        if(dataType == "SecureAnswer") {

            sendOTPEmail();

        }else{

            performSubmit('millemiglia-recuperacredenziali-submit',
                '#form-millemiglia-recupera-credenziali', RecuperoCredenzialiSuccess, RecuperoCredenzialiRecuperoFail);
        }


    }else {
        // todo presento un errore
        //showErrors(CQ.I18n.get("specialpage.millemiglia.ErroreControlloOTP"))// da tradurre

    }

    console.log("FLAGS: ",flagsVerifica);
    return false;
}

function isMMOneOfRequiredGroup(value) {
    var result = false;
    var elementSeparator = ";";
    var fields = "username;codiceMillemiglia;alias";
    console.log("isMMOneOfRequiredGroup in MilleMigliaRecuperaCredenziali.js isNotEmpty verification between that group of fields: ",fields);
    fields = fields.split(elementSeparator);
    var k;
    for (k=0; k<fields.length; k++) {
        var field = '#' + fields[k];
        result = isNotEmpty($(field).val());

        if (result) {return result;}
    }
    return false;
}
