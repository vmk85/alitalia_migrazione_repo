function passengerDataSubmit(e) {
	startPageLoader(true);
	$('#passengersSubmit').off('click', passengerDataSubmit);
	$('#passengersSubmit').addClass('isDisabled');
	validation(e, 'checkinpassengersdata', '#passengersForm', passengerDataSubmitSuccess, passengerDataSubmitError);
}

function passengerDataSubmitSuccess(data) {
	if (data.result) {
		removeErrors();
		$("#noAdultsErrorMessage").hide();
		
		performSubmit('checkinpassengersdata', '#passengersForm', 
		function(data) {
			startPageLoader(false);
			if (data.lightboxInfant){
				window["analytics_youngType"] = data.youngType;
				$("#lightboxInfant").trigger("click");
			} else if (data.lightboxSpecialFare){
				$("#lightboxSpecialFare").trigger("click");
			}
			if (!data.lightboxInfant && !data.lightboxSpecialFare) {
				window.location.replace(data.redirect);
			}
		}, 
		passengerDataSubmitError);
		
	} else {
		startPageLoader(false);
		$('#passengersSubmit').on('click', passengerDataSubmit);
		$('#passengersSubmit').removeClass('isDisabled');
		showErrors(data);
		$("#noAdultsErrorMessage").hide();
		if(data.fields.noAdult){
			$("#noAdultsErrorMessage").show();
		}
		/*Tracking errors*/
		if(typeof window["analytics_trackCheckinErrors"] == "function"){
			analytics_trackCheckinErrors(data.fields);
		}
	}
}

function passengerDataSubmitError() {
	startPageLoader(false);
	$('#passengersSubmit').on('click', passengerDataSubmit);
	$('#passengersSubmit').removeClass('isDisabled');
	console.log('fail');
}

function checkInDone() {
	$('div.j-isPasenger div.checkbox input:first').click();
	
	$('#passengersSubmit').off('click', passengerDataSubmit);
	$('#passengersSubmit').on('click', passengerDataSubmit);
	$('#addExtraPassengerSubmit').off('click', addExtraPassenger);
	$('#addExtraPassengerSubmit').on('click', addExtraPassenger);
	callAjaxLoad($("div.booking__buttonCover.withMaxWidth"));
}

function addExtraPassenger(e){
	$('#addExtraPassengerSubmit').off('click', addExtraPassenger);
	$('#addExtraPassengerSubmit').addClass('isDisabled');
	validation(e, 'checkinaddextrapassenger', '#passengersForm', addExtraPassengerSuccess, addExtraPassengerError);
}


function addExtraPassengerSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('checkinaddextrapassenger', '#passengersForm', 
			function(data) {
				if (data.redirect) {
					window.location.replace(data.redirect);
				} else {
					if (data.isError) {
						//FIXME show error message: capire che errore mostrare
						$("#errorAddPassenger").show();
					} else {
						//Errore non gestito nel json
						addExtraPassengerError();
					}
				}
			}, 
			addExtraPassengerError);
	} else {
		$('#addExtraPassengerSubmit').on('click', addExtraPassenger);
		$('#addExtraPassengerSubmit').removeClass('isDisabled');
		showErrors(data);
		/*Tracking errors*/
		if(typeof window["analytics_trackCheckinErrors"] == "function"){
			analytics_trackCheckinErrors(data.fields);
		}
	}
}

function addExtraPassengerError() {
	$('#addExtraPassengerSubmit').on('click', addExtraPassenger);
	$('#addExtraPassengerSubmit').removeClass('isDisabled');
	//FIXME show error message: capire che errore mostrare
	$("#errorAddPassenger").show();
	/*Tracking errors*/
	var errorMessage = $("#errorAddPassenger").text().trim();
	if(typeof window["analytics_trackCheckinErrors"] == "function"){
		analytics_trackCheckinErrors(errorMessage);
	}
}