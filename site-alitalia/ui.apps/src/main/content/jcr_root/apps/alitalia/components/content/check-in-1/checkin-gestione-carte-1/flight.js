"use strict";
use(function () {

    var flights = this.flights;

    var flight;

    // Se ho un solo volo prendo il primo
    if (flights.flightsRender.size() == 1)
    {
        flight = flights.flightsRender.get(0);
    }
    else
    {
        // Se ho più voli controllo se il primo è aperto al checkIN, se no prendo il secondo (ritorno)
        if (flights.flightsRender.get(0).segments.get(0).openCI)
            flight = flights.flightsRender.get(0);
        else
            flight = flights.flightsRender.get(1);
    }

    return {
        flight: flight
    };

});