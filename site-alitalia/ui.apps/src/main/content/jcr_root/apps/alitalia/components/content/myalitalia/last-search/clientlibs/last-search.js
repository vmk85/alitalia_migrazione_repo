$(document).ready(function() {
	populate_last_search();
});

function populate_last_search() {
    var num = 0;
    var today = new Date();
    today.setHours(0,0,0,0);
    var items = (JSON.parse(localStorage.getItem("ricercheRecenti"))!=null?JSON.parse(localStorage.getItem("ricercheRecenti")):[]);
    var structure = "";
    if(items != null) {
        if(items.length>0){
            if(items.length > 3) items.length = 3;
            items.forEach(function(item, index) {
                var dateParts = item.dataPartenza.split("/");
                var dataPartenza = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);

                if(dataPartenza>=today){
                    num++;
                    var classe;
                    if(item.classeViaggio == "cercaVolo.label.business")
                        classe = "Business";
                    else if(item.classeViaggio == "cercaVolo.label.economy")
                        classe = "Economy";
                    structure += "<li>";
                    structure += '  <span class="ico"><img class="vcenter" src="/etc/designs/alitalia/clientlibs-myalitalia/images/ico-plane.svg"></span>';
                    structure += '  <div class="float-right vcenter"><a href="#" class="ico-arrow" onclick="triggerSearch(' + index + ')"></a></div>';
                    structure += '  <div class="info">';
                    structure += '    <p>' + item.cittaPartenza.nomeT + ' <strong> ' + item.cittaPartenza.codiceAeroporto + ' </strong> - <strong> ' + item.cittaArrivo.codiceAeroporto + ' </strong></p>';
                    structure += '    <p><span> ' + setDate(item.dataPartenza) + ' </span><span> ' + (item.dataRitorno?setDate(item.dataRitorno):"") + '</span><span> ' + checkPassengers(item.nAdulti, item.nBambini, item.nNeonati) + '</span><span> ' + classe + '</span></p>';
                    structure += '  </div>';
                    structure += '</li>';
                }
            });

            $(".flights-list").find("ul").html(structure);

        }else{
            structure = '<p>' + CQ.I18n.get('myalitalia.no_recent_search') + '</p>';
            $(".flights-list").append(structure);
            $(".last-search").hide();
        }
    }
    if(num == 0) $(".last-search").hide();
}

function checkPassengers(adult, kid, baby) {
    var string = "";
    if(adult > 0) {
        string += adult;
        if(adult == 1)
            string += " Adulto";
        else 
            string += " Adulti";
    }
    if(kid > 0) {
        string += ", " + kid;
        if(kid == 1)
            string += " Bambino";
        else
            string += " Bambini";
    }
    if(baby > 0) {
        string += ", " + baby;
        if(baby == 1)
            string += " Neonato";
        else
            string += " Neonati";
    }
    return string;
}

function setDate(rawDate) {
    date = rawDate.split("/");
    var rawDate = new Date(date[2], date[1]-1, date[0]);
    var week, day, month, year;
    var weeks = ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"];
    var months = ["months:", "gen", "feb", "mar", "apr", "mag", "giu", "lug", "ago", "set", "ott", "nov", "dic"];
    week = weeks[rawDate.getDay()];
    day = date[0];
    month = months[date[1]*1];
    year = date[2];
    return week + " " + day + " " + month + "\n" + year;
}

function triggerSearch(index) {    
    var items = JSON.parse(localStorage.getItem("ricercheRecenti"));
    $("#roundTrip--prenota").val(items[index]["tipoViaggio"]);
    $("#departureAirportCode--prenota").val(items[index]["cittaPartenza"]["codiceAeroporto"]);
    $("#departureAirport--prenota").val(items[index]["cittaPartenza"]["nomeT"]);
    $("#arrivalAirportCode--prenota").val(items[index]["cittaArrivo"]["codiceAeroporto"]);
    $("#arrivalAirport--prenota").val(items[index]["cittaArrivo"]["nomeT"]);
    $("#departureDate--prenota").val(items[index]["dataPartenza"]);
    $("#returnDate--prenota").val(items[index]["dataRitorno"]);
    $("#seatType--prenota").val(items[index]["classeViaggio"]);
    $("#adultPassenger--prenota").val(items[index]["nAdulti"]);
    //$("#youngAdultPassenger--prenota").val(items[index]["yAdultNum"]);
    $("#kidPassenger--prenota").val(items[index]["nBambini"]);
    $("#babyPassenger--prenota").val(items[index]["nNeonati"]);
    $("#hiddenForm--prenota").trigger("submit");
}