setI18nTranslate('millemiglia.datiLogin.error');
setI18nTranslate('millemiglia.datiLogin.ajaxError');
setI18nTranslate('millemiglia.datiLogin.success');

String.prototype.replaceAt=function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
};

var valoreConfrontoSicurezza = null ; //"ok";
var otpControlForSaveSA = "";

var otpSMSExpiryDate=null;

$('#datiLoginSumbit').bind('click', function(e) {

    //****TOLTO TEMPORANEAMENTE PER PROVARE SALVATAGGIO
    //**perché va in errore
    //invokeGenericFormService('datiloginsubmit-sendalertcomunication', 'POST', '',
    //            null, null, null, null, null);


	//validation(e, 'datiloginsubmit', '#datiLoginEdit',
	//		datiLoginEditSuccess, datiLoginEditError);
    removeErrors();
	sendUpdateAlert();
	return false;
});

    function sendUpdateAlert(){
        var data = {

        };
        invokeGenericFormService('datiloginsubmit-sendalertcomunication', 'POST', '',
            sendUpdateAlertAfter, sendUpdateAlertAfter, undefined, undefined, data);

    }

    function sendUpdateAlertAfter(){
            if (allDatiSicurezzaCheck()){
                /*if(checkPresenza())
                {*/
                    if(checkVerificaPassword())
                    {
                        checkVerificaUsernameDuplicate();
                    }else{
                        ///presenta errore
                        var data = {
                            isSuccess: false,
                            fields:{
                                "confpassword": CQ.I18n.get("specialpage.recuperacredenziali.message.nopasswordsmatching")
                            }
                        };
                        showErrors(data);

                    }
                /*}else{
                    ///presenta errore
                    var data = {
                        isSuccess: false,
                        fields:{
                            "risposta": CQ.I18n.get("specialpage.millemiglia.security.isMMOneOfRequiredGroup"),
                            "cellulare": CQ.I18n.get("specialpage.millemiglia.security.isMMOneOfRequiredGroup")
                        }
                    };
                    showErrors(data);
                }*/
            }

     }

     function allDatiSicurezzaCheck(){
             var retval = true;

             var username = $('#username').val();
             var password = $('#password').val();
             var confpasswordinput = $('#confpasswordinput').val();
             var emailSA = $('#emailSA').val();
             var cellulare = $('#cellulare').val();

            var data = {
                isSuccess: false,
                fields:{
                }
            };

             if (!isNotEmpty(username)){
                retval=false;
                data.fields.username=CQ.I18n.get("message.generic.emptyfield");
             }else{
                if (!isMMUserName(username)){
                    retval=false;
                    data.fields.username=CQ.I18n.get("millemiglia.username.error.notstrong");
                 }
             }

             if (!isNotEmpty(password)){
                if (_UserName.trim() == "" ) {
                    retval=false;
                    data.fields.password=CQ.I18n.get("message.generic.emptyfield");
                }

             }else{
                if (!isPasswordStrongAuthentication(password)){
                    retval=false;
                    data.fields.password=CQ.I18n.get("millemiglia.password.error.notstrong");
                 }
             }

             if (!isNotEmpty(emailSA)){
                retval=false;
                data.fields.emailSA=CQ.I18n.get("specialpage.email.error.empty");
             }else{
                if (!isEmail(emailSA)){
                    retval=false;
                    data.fields.emailSA=CQ.I18n.get("specialpage.email.error.notvalid");
                 }
             }

             if (isNotEmpty(cellulare)){
                if (!isNumber(cellulare)){
                    retval=false;
                    data.fields.cellulare=CQ.I18n.get("message.generic.invalidfield");
                 }
             }

            if (!retval){
                showErrors(data);
            }

             return retval;
         }

    function checkVerificaPassword(){
        console.log("checkVerificaPassword ");

        var p = $('#password').val();
        var cp = $('#confpasswordinput').val();
        if (p == cp){

            return true;

        }
        return false;
    }




    function checkPresenza(){

        var cell_ds=0;

        if ($('#risposta').val().trim() != "" || verificaSQ.trim() != ""){
        //
            cell_ds++;
        }

        if ($('#cellulare').val().trim() != ""){
            cell_ds++;
        }

        if(cell_ds>0){
            return true;
        }

        return false;
    }

    function checkVerificaUsernameDuplicate(){
        if (_flagusername=='false'){
            console.log("checkVerificaUsername");
            var username = $('#username').val().trim();


            var usernameDaVerificare = {
                username : username,
                profileId : loggedUserProfileId
            };


            invokeGenericFormService('millemigliacomunicazionisubmit-checkusernameduplicate', 'POST', '',
                checkVerificaUsernameDuplicateSuccess, checkVerificaUsernameDuplicateFails, undefined, undefined, usernameDaVerificare);
        }else{
            checkVerificaEmailUsernameDuplicate();
        }


    }

    function checkVerificaUsernameDuplicateSuccess(data){
        //feedback --> ok
        if(!data.flagVerificaUserNameNotDuplicate){
            console.log("checkVerificaUsernameSuccess: username non duplicata");
            checkVerificaEmailUsernameDuplicate();
        }else{
            console.log("checkVerificaUsernameSuccess: username duplicata");
            var data = {
                isSuccess: false,
                fields:{
                    "username": CQ.I18n.get("millemiglia.username.error.invalid.alreadyused")   //"username gia in uso"
                }
            };

            showErrors(data);
        }


    }
    function checkVerificaUsernameDuplicateFails(){
        console.log("checkVerificaUsernameFails");
        //feedback --> ERROR --> username duplicato, o già esistente
    }

    function checkVerificaEmailUsernameDuplicate(){
        if (mailSAVerificata=="false"){
            console.log("checkVerificaEmailUsernameDuplicate");
            var emailUser = $("#emailSA").val();

            var data = {
                input_email : emailUser,
                profileId : loggedUserProfileId,
                notFromRegistrazione : "TRUE"
            };

            invokeGenericFormService('millemigliacomunicazionisubmit-checkemailusernameduplicate', 'POST', '',
                checkVerificaEmailUsernameDuplicateSuccess, checkVerificaEmailUsernameDuplicateFails, undefined, undefined, data);
        }else{
            afterDuplicateControl();
        }
    }
    function checkVerificaEmailUsernameDuplicateSuccess(data){
        //feedback --> ok
        console.log("checkVerificaEmailUsernameDuplicateSuccess");
        if(!data.flagVerificaEmailUserNameNotDuplicate){
            afterDuplicateControl();
        }else{
            var data = {
                isSuccess: false,
                fields:{
                    "emailSA": CQ.I18n.get("millemiglia.email.error.invalid.alreadyused")   //"email gia in uso"
                }
            };

            showErrors(data);
        }

    }
    function checkVerificaEmailUsernameDuplicateFails(){
        console.log("checkVerificaEmailUsernameDuplicateFails");
        //feedback --> ERROR --> emailUsername duplicato, o già esistente
        flagsVerifica.flagVerificaEmailUserNotDuplicate = false;
    }

    function afterDuplicateControl(){

        if (cellulareSAVerificato=="false" && $("#cellulare").val().length > 0 ){
            otpControlForSaveSA = "NEW";
            sendOTPsms();
            //verifica otp nuovo cellulare
        } else if (mailSAVerificata=="false") {
            otpControlForSaveSA = "NEW";
                 sendOTPEmail();
        } else {
            otpControlForSaveSA = "OLD";
            performValidation( 'datiloginsubmit', '#datiLoginEdit',
                    datiLoginEditSuccess, datiLoginEditError);
            return false;
            //popup conrichiesta di quale controllo otp fare
        }

        //validation(e, 'datiloginsubmit', '#datiLoginEdit',
        //            datiLoginEditSuccess, datiLoginEditError);
        //    return false;
    }

    function sendOTPEmail(){


        var email=$("#emailSA").val();//$("#email").val();
        var data = {
            email : email
        };

        var emailwithStars=$("#emailSA").val();

        for(var i = 2; i < (emailwithStars.length - 3);i++)
        {
            emailwithStars = emailwithStars.replaceAt(i,"*");
        }
        $('#phoneEmailNumberHidden').text(emailwithStars);
        otpControlForSaveSA = "NEW";

        $("#otpPopupTitle").html(CQ.I18n.get("specialpage.millemiglia.insertCodeEmailOtp"));

        $( "#input_insertCode_OTP" ).attr( "data-type", "Email" );

        $('#input_insertCode_OTP').val("");

        invokeGenericFormService('millemigliacomunicazionisubmit-sendotpemail', 'POST', '',
            sendOTPEmailSuccess, sendOTPEmailFails, sendOTPEmailAlways, undefined, data);

        $('.reveal-overlay.mm-profolo').css('display','block');$('.mm-profolo .reveal').css('display','block');
    }

    function sendOTPEmailSuccess(data){
        //feedback --> ok
        valoreConfrontoSicurezza=data.OTP;

        //otpSMSExpiryDate=data.ExpiryDate;
        //otpSMSSentDate=data.SentDate;

        var requestTime = new Date();
        otpSMSSentDate=requestTime;
        otpSMSExpiryDate=requestTime.setSeconds(requestTime.getSeconds() + 30);


        console.log("sendOTPEmailSuccess");

        if(data.isError)
        {
            console.log("sendOTPEmailSuccess.internalError.Fake");
            //ultra fake ////
            valoreConfrontoSicurezza="123456"; //per ora... in prod togliere
            otpSMSSentDate=new Date();
            otpSMSExpiryDate="2099-12-31 10:00:00";
        }
    }
    function sendOTPEmailFails(data){

        //feedback --> ERROR --> impossobile inviare EMAIL

        //ultra fake ////
        valoreConfrontoSicurezza="123456"; //per ora... in prod togliere

        console.log("sendOTPEmailFails");

    }
    function sendOTPEmailAlways(data) {
    }

/* function padMobilePhonePrefix (str, max) {
    str = str.toString();
    return str.length < max ? padMobilePhonePrefix("0" + str, max) : str;
}*/
function sendOTPsms(){
    //  var mobilephoneprefix =  padMobilePhonePrefix($("#nationalPrefix").val(),4);
    var zeri = "00";
    var mobilephoneprefix = zeri + $("#nationalPrefix").val();
    var mobilephonenumber = $("#cellulare").val();
    var data = {
        phone : mobilephoneprefix + mobilephonenumber
    };


    var number = $("#cellulare").val().trim();
        for(var i = 2; i < (number.length - 3);i++)
        {
            number = number.replaceAt(i,"*");
        }
        $('#phoneEmailNumberHidden').text(number);

        $("#otpPopupTitle").html(CQ.I18n.get("specialpage.millemiglia.insertCodeSmsOtp"));

        $( "#input_insertCode_OTP" ).attr( "data-type", "SMS" );

        $('#input_insertCode_OTP').val("");

        invokeGenericFormService('millemigliacomunicazionisubmit-sendotpsms', 'POST', '',
            sendOTPsmsSuccess, sendOTPsmsFails, undefined, undefined, data);


        $('.reveal-overlay.mm-profolo').css('display','block');$('.mm-profolo .reveal').css('display','block');
    }

        function sendOTPsmsSuccess(data){
            //feedback --> ok

            valoreConfrontoSicurezza=data.OTP;
            otpSMSExpiryDate=data.ExpiryDate;
            otpSMSSentDate=data.SentDate;

            console.log("sendOTPsmsSuccess");

            if(data.isError)
            {
                console.log("sendOTPsmsSuccess.internalError.Fake");
                //ultra fake ////
                valoreConfrontoSicurezza="123456"; //per ora... in prod togliere
                otpSMSSentDate=new Date();
                otpSMSExpiryDate="2099-12-31 10:00:00";
            }

        }
        function sendOTPsmsFails(data){

            //feedback --> ERROR --> impossobile inviare SMS

            //ultra fake ////
            valoreConfrontoSicurezza="123456"; //per ora... in prod togliere
            otpSMSSentDate=new Date();
            otpSMSExpiryDate="2099-12-31 10:00:00";

            console.log("sendOTPsmsFails");

        }

        $('.insertCodeSMS_OTP').on('click',function(){
                removeErrors();
                //verifico gli OTP
                var wrongOTP = false;
                var inputValue = $('#input_insertCode_OTP').val();

                var dataType = $( "#input_insertCode_OTP" ).attr( "data-type"); //discriminante

                var invokeSave=false;

                if (dataType == "SMS")
                {
                    //verificare ExpiryDate !!!!!

                    var isExpired=isOtpExpired(otpSMSExpiryDate);
                    var isNotExpired=!isExpired;

                    if (inputValue == valoreConfrontoSicurezza && isNotExpired){
                        $('.reveal-overlay.mm-profolo').css('display','none');
                        $('.mm-profolo .reveal').css('display','none');
                        cellulareSAVerificato= "true";
                        if (otpControlForSaveSA == "OLD"){
                            invokeSave=true;
                        }else{
                            if (mailSAVerificata=="false") {
                                 sendOTPEmail();
                            }else{
                                invokeSave=true;
                            }
                        }
                    }else{
                        wrongOTP =true;
                        smsOtpError++;
                        if (smsOtpError>=   maxSsOtpTry){
                            smsOtpError=0;
                            $('.reveal-overlay.mm-profolo').css('display','none');
                            $('.mm-profolo .reveal').css('display','none');
                        }else{
                            sendOTPsms();
                        }
                    }
                }else if(dataType == "Email"){
                    //verificare ExpiryDate !!!!!
                    if (inputValue == valoreConfrontoSicurezza){
                        $('.reveal-overlay.mm-profolo').css('display','none');
                        $('.mm-profolo .reveal').css('display','none');
                        mailSAVerificata= "true";
                        invokeSave=true;
                    }else{
                        wrongOTP = true;
                        mailOtpError++;
                        if (mailOtpError>=   maxMailOtpTry){
                            mailOtpError=0;
                            $('.reveal-overlay.mm-profolo').css('display','none');
                            $('.mm-profolo .reveal').css('display','none');
                        }else{
                            sendOTPEmail();
                        }
                    }
                }else if(dataType == "SecureAnswer"){
                    var MD5 = function(d){result = M(V(Y(X(d),8*d.length)));return result.toLowerCase()};function M(d){for(var _,m="0123456789ABCDEF",f="",r=0;r<d.length;r++)_=d.charCodeAt(r),f+=m.charAt(_>>>4&15)+m.charAt(15&_);return f}function X(d){for(var _=Array(d.length>>2),m=0;m<_.length;m++)_[m]=0;for(m=0;m<8*d.length;m+=8)_[m>>5]|=(255&d.charCodeAt(m/8))<<m%32;return _}function V(d){for(var _="",m=0;m<32*d.length;m+=8)_+=String.fromCharCode(d[m>>5]>>>m%32&255);return _}function Y(d,_){d[_>>5]|=128<<_%32,d[14+(_+64>>>9<<4)]=_;for(var m=1732584193,f=-271733879,r=-1732584194,i=271733878,n=0;n<d.length;n+=16){var h=m,t=f,g=r,e=i;f=md5_ii(f=md5_ii(f=md5_ii(f=md5_ii(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_ff(f=md5_ff(f=md5_ff(f=md5_ff(f,r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+0],7,-680876936),f,r,d[n+1],12,-389564586),m,f,d[n+2],17,606105819),i,m,d[n+3],22,-1044525330),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+4],7,-176418897),f,r,d[n+5],12,1200080426),m,f,d[n+6],17,-1473231341),i,m,d[n+7],22,-45705983),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+8],7,1770035416),f,r,d[n+9],12,-1958414417),m,f,d[n+10],17,-42063),i,m,d[n+11],22,-1990404162),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+12],7,1804603682),f,r,d[n+13],12,-40341101),m,f,d[n+14],17,-1502002290),i,m,d[n+15],22,1236535329),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+1],5,-165796510),f,r,d[n+6],9,-1069501632),m,f,d[n+11],14,643717713),i,m,d[n+0],20,-373897302),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+5],5,-701558691),f,r,d[n+10],9,38016083),m,f,d[n+15],14,-660478335),i,m,d[n+4],20,-405537848),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+9],5,568446438),f,r,d[n+14],9,-1019803690),m,f,d[n+3],14,-187363961),i,m,d[n+8],20,1163531501),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+13],5,-1444681467),f,r,d[n+2],9,-51403784),m,f,d[n+7],14,1735328473),i,m,d[n+12],20,-1926607734),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+5],4,-378558),f,r,d[n+8],11,-2022574463),m,f,d[n+11],16,1839030562),i,m,d[n+14],23,-35309556),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+1],4,-1530992060),f,r,d[n+4],11,1272893353),m,f,d[n+7],16,-155497632),i,m,d[n+10],23,-1094730640),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+13],4,681279174),f,r,d[n+0],11,-358537222),m,f,d[n+3],16,-722521979),i,m,d[n+6],23,76029189),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+9],4,-640364487),f,r,d[n+12],11,-421815835),m,f,d[n+15],16,530742520),i,m,d[n+2],23,-995338651),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+0],6,-198630844),f,r,d[n+7],10,1126891415),m,f,d[n+14],15,-1416354905),i,m,d[n+5],21,-57434055),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+12],6,1700485571),f,r,d[n+3],10,-1894986606),m,f,d[n+10],15,-1051523),i,m,d[n+1],21,-2054922799),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+8],6,1873313359),f,r,d[n+15],10,-30611744),m,f,d[n+6],15,-1560198380),i,m,d[n+13],21,1309151649),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+4],6,-145523070),f,r,d[n+11],10,-1120210379),m,f,d[n+2],15,718787259),i,m,d[n+9],21,-343485551),m=safe_add(m,h),f=safe_add(f,t),r=safe_add(r,g),i=safe_add(i,e)}return Array(m,f,r,i)}function md5_cmn(d,_,m,f,r,i){return safe_add(bit_rol(safe_add(safe_add(_,d),safe_add(f,i)),r),m)}function md5_ff(d,_,m,f,r,i,n){return md5_cmn(_&m|~_&f,d,_,r,i,n)}function md5_gg(d,_,m,f,r,i,n){return md5_cmn(_&f|m&~f,d,_,r,i,n)}function md5_hh(d,_,m,f,r,i,n){return md5_cmn(_^m^f,d,_,r,i,n)}function md5_ii(d,_,m,f,r,i,n){return md5_cmn(m^(_|~f),d,_,r,i,n)}function safe_add(d,_){var m=(65535&d)+(65535&_);return(d>>16)+(_>>16)+(m>>16)<<16|65535&m}function bit_rol(d,_){return d<<_|d>>>32-_}

                    var md5Value = MD5(inputValue);

                    if (md5Value == verificaSQ){
                        $('.reveal-overlay.mm-profolo').css('display','none');
                        $('.mm-profolo .reveal').css('display','none');
                        invokeSave=true;
                    }else{
                        wrongOTP = true;
                    }
                }

                if (wrongOTP==true){
                    //Codice di verifica non valido. Attendi ne verrà inviato uno nuovo.
                    //(questo per 2 tentativi)
                    var errMsg=CQ.I18n.get("millemiglia.otp.error.invalid");
                    if(
                        (mailOtpError==0 && dataType == "Email")
                        ||
                        (smsOtpError==0 && dataType == "SMS")
                    ){
                        //Superato il numero massimo di tentativi. Chiudi la finestra e ripeti la procedura.
                       //(questo al 3° tentativo, al fine di evitare un invio eccessivo di SMS e quindi aumentare i costi)
                        errMsg=CQ.I18n.get("millemiglia.otp.error.invalid.limit.reached");
                    }
                    var data = {
                        isSuccess: false,
                        fields:{
                            "input_insertCode_OTP": errMsg
                        }
                    };
                    showErrors(data);
                    $('#input_insertCode_OTP_error').css('position','relative');
                }


                if(invokeSave==true){
                    performValidation( 'datiloginsubmit', '#datiLoginEdit',
                            datiLoginEditSuccess, datiLoginEditError);
                    return false;
                }


                return false;
            });

function datiLoginEditSuccess(data) {
	if (data.result) {
		removeErrors();
		$('#datiLoginSumbit').unbind('click');
		$('#datiLoginEdit').submit();
	} else {
		showErrors(data);
		showFormFeedbackError('datiLoginEdit', 'millemiglia.datiLogin.error');
		if (typeof(data.fields["pin"]) != "undefined") {
			if (typeof(window["analytics_nuovo_pin_Error"]) === "function") {
				window["analytics_nuovo_pin_Error"]();
			}
		}
	}
	return false;
}

function datiLoginEditError(data) {
	showFormFeedbackError('datiLoginEdit', 'millemiglia.datiLogin.ajaxError');
	return false;
}

function closePopup() {
	$('.reveal-overlay.mm-profolo').css('display','none');
    $('.mm-profolo .reveal').css('display','none');
}

$(window).on("load", function (e) {
//    $('.mm-profolo .reveal .icon.icon--close').on('click',function(){
//    $('.reveal-overlay.mm-profolo').css('display','none');
//    $('.mm-profolo .reveal').css('display','none');
//    })
	var $accordionDatiLogin = $('#accordion1_tab2');
	if (resultDatiLoginSubmit == "true") {
		$accordionDatiLogin.click();
		showFormFeedbackSuccess('datiLoginEdit', 'millemiglia.datiLogin.success');
	} else if (resultDatiLoginSubmit == "false") {
		$accordionDatiLogin.click();
		showFormFeedbackError('datiLoginEdit', 'millemiglia.datiLogin.ajaxError');
	}
	if (resultSocialLoginSubmit == "true" || resultSocialLoginSubmit == "false") {
		$accordionDatiLogin[0].scrollIntoView();
	}

	return false;
});


/*
$(window).load(function() {
	var $accordionDatiLogin = $('#accordion1_tab2');
	if (resultDatiLoginSubmit == "true") {
		$accordionDatiLogin.click();
		showFormFeedbackSuccess('datiLoginEdit', 'millemiglia.datiLogin.success');
	} else if (resultDatiLoginSubmit == "false") {
		$accordionDatiLogin.click();
		showFormFeedbackError('datiLoginEdit', 'millemiglia.datiLogin.ajaxError');
	}
	if (resultSocialLoginSubmit == "true" || resultSocialLoginSubmit == "false") {
		$accordionDatiLogin[0].scrollIntoView();
	}
	return false;
});
*/


function  isOtpExpired(otpExpiryDate){
    var isExpired=true;
    try {
        var dateOtpExpiry=new Date(Date.UTC(otpExpiryDate.substr(0,4), otpExpiryDate.substr(5,2)-1, otpExpiryDate.substr(8,2), otpExpiryDate.substr(11,2), otpExpiryDate.substr(14,2), otpExpiryDate.substr(17,2)));
        var dateNow=new Date();

        isExpired=dateOtpExpiry<dateNow;

    }catch (e) {
        console.log(e);
        isExpired=true;
    };

    console.log('dateNow: ' + dateNow)
    console.log('dateOtpExpiry: ' + dateOtpExpiry)
    console.log('OtpExpired: ' + isExpired);

    return isExpired;
}
