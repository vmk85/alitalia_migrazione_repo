var reloadingMatrix = false;

function initOfferPage(currency, labelScopri, edit, homepath) {
	
	var from = getParameterByName("from");
	var to = getParameterByName("to");
	var young = getParameterByName("isYoung");
    var selectedMonth = getParameterByName("month");
	
	// default
	if (to) {
		
		// parameter from retrieved from local storage
		if (!from) {
			
			var ricercheRecenti;
			var items = localStorage.getItem("ricercheRecenti");
			if (items == null || items == 'undefined' || items == '[]' || items.length == 0) {
				ricercheRecenti = [];
			} else {
				ricercheRecenti = JSON.parse(items);
			}
			
			if (ricercheRecenti.length > 0) {
				var departureAirport = ricercheRecenti[0];
				from = ricercheRecenti[0].cittaPartenza.codiceAeroporto;
			}
		}
		getOfferImage(to);
		retrieveOfferData(from, to, selectedMonth, young, currency, labelScopri, homepath);
	}
	
	// edit mode
	else if (edit) {
		insertTabs();
		getOfferImage("default");
		renderDataMock(currency, labelScopri);
	}
	
	// redirect to home
	else {
		if (homepath) {
			window.location.href = homepath;
		}
	}
	
	$(function(){
		(function(){ // matrix hover hint
			var
				$dateMatrix=$(".dateMatrix"),
				setCellClass=function(c,r,cl){
					$dateMatrix.find(".c"+c+".r"+r).addClass(cl);
				};
			///var
			$dateMatrix.find("td").hover(function(){
				var
					classes=$(this).attr("class").split(/\s+/),
					c,r;
				///var
				for(var i in classes)
					if(classes[i].match(/^c\d$/))
						c=classes[i].substr(1);
					else if(classes[i].match(/^r\d$/))
						r=classes[i].substr(1);
				$dateMatrix.find(".activeCell,.activeCell--hint,.activeCell--header").removeClass("activeCell activeCell--hint activeCell--header");
				setCellClass(c,r,"activeCell");
				setCellClass(c,0,"activeCell--header");
				setCellClass(0,r,"activeCell--header");
				for(var i=1;i<c;i++)
					setCellClass(i,r,"activeCell--hint");
				for(var i=1;i<r;i++)
					setCellClass(c,i,"activeCell--hint");
			},function(){
				$dateMatrix.find(".activeCell,.activeCell--hint,.activeCell--header").removeClass("activeCell activeCell--hint activeCell--header");
			});

		})(); // matrix hover hint
	});
}

function getOfferImage(destination) {
	$.ajax({
		url: removeUrlSelector() + ".banner." + destination + ".html",
		context : document.body
	})
	.done(function(data) {
		$(".flightSel__coverCanvas .heroCarousel__itemContainer").prepend(data);
		$(".flightSel__coverCanvas .heroCarousel__itemContainer").show();
		$(window).trigger('resize');
	})
	.fail(function() {});
}

function getOfferSocialShare(destination, price, flightTypeCode) {
	$.ajax({
		url: removeUrlSelector() + ".socialshare-partial." + destination + "." + price + "." + flightTypeCode + ".html",
		context : document.body
	})
	.done(function(data) {
		$(".flightSel__coverCanvas .heroCarousel__itemContainer").prepend(data);
		$(".flightSel__coverCanvas .heroCarousel__itemContainer").show();
		$(window).trigger('resize');
	})
	.fail(function() {});
}

function setFormValues(departureAirport, arrivalAirport, roundTrip, young, values) {
		
		$("#Origin").attr("value", departureAirport.code);
		$("#originKeyCountryCode").text("airportsData." + departureAirport.countryCode + ".country");
		$("#originKeyAirportName").text("airportsData." + departureAirport.code + ".name");
		$("#originKeyCityCode").text("airportsData." + departureAirport.code + ".city");
		
		$("#Destination").attr("value", arrivalAirport.code);
		$("#destinationKeyCountryCode").text("airportsData." + arrivalAirport.countryCode + ".country");
		$("#destinationKeyAirportName").text("airportsData." + arrivalAirport.code + ".name");
		$("#destinationKeyCityCode").text("airportsData." + arrivalAirport.code + ".city");
		
		if (roundTrip) {
			if (young) {
				$("#andataeritorno").attr("value", "Youth");
				$("#searchTypeGiovani").attr("value", "Youth");
				$("#CUG").attr("value", "YTH");
			} else {
				$("#andataeritorno").attr("value", "BrandSearch");
				$("#searchTypeGiovani").remove();
			}
		} else {
			if (young) {
				$("#andataeritorno").attr("value", "a");
				$("#searchTypeGiovani").attr("value", "Youth");
				$("#CUG").attr("value", "YTH");
			} else {
				$("#andataeritorno").attr("value", "a");
				$("#searchTypeGiovani").remove();
			}
			
			if (values) {
				var year = "";
				var month = "";
				var day = "";
				for (var v in values) {
					year = v;
					for (var x in values[v]) {
						month = x;
						for (var y in values[v][x]) {
							day = y;
							break;
						}
						break;
					}
					break;
				}
				var defaultDate = day + "/" + month + "/" + year;
				$(".flightSel__footer input[name='DepartureDate']").attr("value", defaultDate);
			}
			
		}
		
		if (!roundTrip) {
			$(".flightSel__footer input[name='ReturnDate']").remove();
		}
}

function retrieveOfferData(departure, arrival, selectedMonth, young, currency, labelScopri,
		homepath) {
	
	params = {
		from: departure,
		to: arrival,
        selectedMonth: selectedMonth
	};
	
	if (young) {
		params["isYoung"] = true;
		$(".passengers .adulti").remove();
		$(".passengers .bambini").remove();
		$(".passengers .neonati").remove();
		
	} else {
		$(".passengers .giovani").remove();
	}
	$(".passengers").show();
	
	$.ajax({
		url : removeUrlSelector() + ".offerdetail.json",
		data : params,
		context : document.body
	})
	.done(function(data) {
		if (data.result && data.result.values) {
			renderMain(data, young, currency, labelScopri,selectedMonth);
			if (data.result.roundTrip) {
				$.each(data.result.values, function(year, yearData) {
					$.each(yearData, function(month, monthData) {
						$.each(monthData, function(day, dayData) {
						    if (selectedMonth!=null && selectedMonth!="") {
                                if (dayData.selectedMonthFound) {
                                    retrieveMatrixData(departure, arrival,
                                        dayData.departureDate,
                                        dayData.returnDate, young, currency,
                                        labelScopri);
                                }
                            }else{
                                if (dayData.bestPrice) {
                                    retrieveMatrixData(departure, arrival,
                                        dayData.departureDate,
                                        dayData.returnDate, young, currency,
                                        labelScopri);
                                }
                            }

						});
					});
				});
			}
			getOfferSocialShare(arrival, data.result.bestPrice, data.result.roundTrip);
		} else {
			if (homepath) {
				window.location.href = homepath;
			}
		}
	})
	.fail(function() {});
}

function retrieveMatrixData(departure, arrival, departureDate, returnDate,
		young, currency, labelScopri) {
	
	params = {
		from: departure,
		to: arrival,
		departureDate: departureDate,
		returnDate: returnDate
	};
	
	if (young) {
		params["isYoung"] = true;
	}
	
	var dateMatrix = $(".dateMatrix");
	
	dateMatrix.removeClass("isRendered");
	$('.j-loader').addClass('isActive');
	
	$.ajax({
		url : removeUrlSelector() + ".offermatrix.json",
		data : params,
		context : document.body
	})
	.done(function(data) {
		$('.j-loader').removeClass('isActive');
		dateMatrix.addClass("isRendered");
		if (data.result && data.result.matrixData) {
			renderDateMatrix(currency, labelScopri, data.result.matrixDeparture,
					data.result.matrixReturn, data.result.matrixData,
					data.result.bestPrice);
		}
	})
	.fail(function() {
		$('.j-loader').removeClass('isActive');
		dateMatrix.addClass("isRendered");
	})
	.always(function() {
		$('.j-loadDateMatrix').removeClass("notactive");
		reloadingMatrix = false;
	});
}

function renderMain(data, young, currency, labelScopri, selectedMonth) {
	$(".heroCarousel__textContainerTitle").text(data.result.arrival.city);
	$(".flightSel__origin .location").text(data.result.departure.city);
	$(".flightSel__origin .apt").text(data.result.departure.code);
	$(".flightSel__destination .location").text(data.result.arrival.city);
	$(".flightSel__destination .apt").text(data.result.arrival.code);
	$(".heroCarousel__textContainerOffer .price").html(currency.symbol + " " + data.result.bestPrice);
	
	// roundTrip/oneWay
	if (data.result.roundTrip) {
		$(".heroCarousel__textContainerSubtitles.text-oneWay").hide();
		$(".heroCarousel__textContainerSubtitles.text-roundTrip").show();
	} else {
		$(".heroCarousel__textContainerSubtitles.text-oneWay").show();
		$(".heroCarousel__textContainerSubtitles.text-roundTrip").hide();
	}
	
	$(".heroCarousel__textContainer").show();
	
	setFormValues(data.result.departure, data.result.arrival,
			data.result.roundTrip, young, data.result.values);
	
	if (data.result.roundTrip) {
		
		// update histogram data
		
		var values = data.result.values;
		var dateHistogram = $(".dateHistogram");
		
		$.each(values, function(year, yearData) {
			$.each(yearData, function(month, monthData) {
				$.each(monthData, function(day, dayData) {
					var histogramItem = "<a class='dateHistogram__item j-loadDateMatrix";
                    if(selectedMonth!=null && selectedMonth!="") {
                        if (dayData.selectedMonthFound) {
                            histogramItem += ' bestPrice isActive';
                        }
                    }else{
                        if (dayData.bestPrice) {
                            histogramItem += ' bestPrice isActive';
                        }
                    }

					histogramItem += "' href='javascript:;' style='padding-top: ";
					histogramItem += dayData.height;
					histogramItem += "px'><div class='dateHistogram__period'><span class='dateHistogram__month long'>";
					if (typeof pageSettings !== 'undefined') {
						histogramItem += pageSettings.fullMonths[month - 1];
					}
					histogramItem += "</span><span class='dateHistogram__month short'>";
					if (typeof pageSettings !== 'undefined') {
						histogramItem += pageSettings.monthAbbrs[month - 1];
					}
					histogramItem += "</span><span class='dateHistogram__year'>";
					histogramItem += year;
					histogramItem += "</span></div><div class='dateHistogram__price' style='width: ";
					histogramItem += 20 + ((dayData.height - 100) / 2); 
					histogramItem += "%'><span class='dateHistogram__currency'>";
					histogramItem += currency.code;
					histogramItem += " </span> <span class='dateHistogram__amount'>";
					histogramItem += dayData.amount + detailOfferMarketStar;
					histogramItem += "</span></div>";
					histogramItem += "<div class='hidden departureDate'>";
					histogramItem += dayData.departureDate;
					histogramItem += "</div>";
					histogramItem += "<div class='hidden returnDate'>";
					histogramItem += dayData.returnDate;
					histogramItem += "</div></a>";
					
					histogramItem = $(histogramItem);
					dateHistogram.append(histogramItem);
				})
			});
		});
		
		(function() { // click on histogram item
			$dateMatrix = $(".dateMatrix");
			$('.j-loadDateMatrix').on("click", function() {
				if (!reloadingMatrix) {
					reloadingMatrix = true;
					$('.j-loadDateMatrix').removeClass('isActive');
					$('.j-loadDateMatrix').addClass("notactive");
					$(this).addClass('isActive');
					$(this).removeClass("notactive");
					retrieveMatrixData(data.result.departure.code,
							data.result.arrival.code, 
							$(this).find(".departureDate").text(),
							$(this).find(".returnDate").text(), young, currency,
							labelScopri);
				}
			});
		})(); // click on histogram item
		
	} else {
		
		// update calendar data
		
		var dpData = data.result.values;
	
		datePickerController.destroyDatePicker("solo_andata");
		datePickerController.createDatePicker({
			formElements: {
				soloAndata: "%d/%m/%Y",
			},
			staticPos: true, // position static
			hideInput: true, // hide input
			noTodayButton: true,
			fillGrid: true,
			rangeLow: data.result.rangeLow,
			rangeHigh: data.result.rangeHigh,
			noFadeEffect: true,
			constrainSelection: false,
			wrapped:true,
			callbackFunctions:{
				dateset: [openAndSetReturn],
				redraw: [function(data){
					setTimeout(function(){
						$("#fd-soloAndata tbody td").each(function(){
							var
								$this=$(this).removeClass("flightSelOneWay__highlight"),
								classes=$this.attr("class").split(/\s+/),
								cellContent,day,daynumber,monthnumber,yearnumber;
							if(!$this.is(".out-of-range")){
								for(var i in classes)
									if(classes[i].substr(0,9)==="yyyymmdd-"){
										yearnumber=parseInt(classes[i].substr(-8,4));
										monthnumber=parseInt(classes[i].substr(-4,2));
										day=classes[i].substr(-2);
										daynumber=parseInt(day);
										break;
									}
								if(dpData[yearnumber]&&dpData[yearnumber][monthnumber]&&dpData[yearnumber][monthnumber][daynumber]){
									cellContent="<span class='flightSelOneWay__currency'>"+currency.code+"</span><span class='flightSelOneWay__amount'>"+(dpData[yearnumber][monthnumber][daynumber].amount?dpData[yearnumber][monthnumber][daynumber].amount:dpData[yearnumber][monthnumber][daynumber])+"</span>";
									if(dpData[yearnumber][monthnumber][daynumber].highlight)
										$this.addClass("flightSelOneWay__highlight");
								}else
									cellContent="<span class='flightSelOneWay__scopri'>" + labelScopri + "</span>";
								
								cellContent += "<span class='flightSelOneWay__fulldate'>"+day+"/"+monthnumber+"/"+yearnumber+"</span>";
								$this.html("<span class='flightSelOneWay__giorno'>"+day+"</span>"+cellContent);
								
								// valorizzazione data cerca-volo
								$this.click(function() {
									$(".flightSel__footer input[name='DepartureDate']").attr("value", 
											$this.find(".flightSelOneWay__fulldate").text());
								});
							}
						});
					});
				}]
			}
		});
		datePickerController.setCursorDate("soloAndata", "20150701");
		//touchstart
		$("#soloAndata").on("focus mousedown",function(){
			datePickerController.show($(this).attr("id"));
		}).on("blur",function(){
			datePickerController.hide($(this).attr("id"));
		});
	}
	
	// roundTrip/oneWay
	if (data.result.roundTrip) {
		$(".flightSel__roundTrip").show();
		$(".flightSel__oneWay").hide();
		$("section.flightSel").addClass("fs-roundTrip");
		$("section.flightSel").removeClass("fs-oneWay");
		$(".cercaVolo-infoText-roundTrip").show();
		$(".flightSel__callToAction").hide();
		
	} else {
		$(".flightSel__oneWay").show();
		$(".flightSel__roundTrip").hide();
		$("section.flightSel").addClass("fs-oneWay");
		$("section.flightSel").removeClass("fs-roundTrip");
		$(".flightSel__callToAction").show();
		$(".cercaVolo-infoText-roundTrip").hide();
	}
	
	// international/domestic
	if (data.result.international) {
		$("section.flightSel").addClass("fs-international");
		$("section.flightSel").removeClass("fs-national");
	} else {
		$("section.flightSel").addClass("fs-national");
		$("section.flightSel").removeClass("fs-international");
	}
	
	$(".flightSel__callToAction .firstButton").on("click", function(){
		$(".flightSel__footer input[name='fromCtaSpecialOffers']").attr("value", "true");
		$("#cercaVoliSubmit").click();
	});
}

function renderDateMatrix(currency, labelScopri, matrixDeparture, matrixReturn,
		matrixData, bestPrice) {
	
	(function(){ // matrix period switch
		var
			$dateMatrix=$(".dateMatrix"),
			getCell=function(c,r){
				return $dateMatrix.find(".c"+c+".r"+r);
			},
			dateToYYYYMMDD=function(d){
				var
					y=d.getFullYear().toString(),
					m=(d.getMonth()+1).toString(),
					d=d.getDate().toString();
				///var
				return y+(m<10?"0"+m:m)+(d<10?"0"+d:d);
			},
			YYYYMMDDtoDate=function(d){
				return new Date(d.substr(0,4),d.substr(4,2)-1,d.substr(6,2));
			},
			shiftDate=function(d,offset){
				var tmp=new Date(d);
				tmp.setDate(d.getDate()+offset);
				return tmp;
			},
			makeHeadDate=function(d){
				
				var giorni = pageSettings.dayAbbrsSunday;
				var mesi = pageSettings.monthAbbrs,
					day=d.getDate();
				///var
				return '<span class="dateMatrix__day">'+giorni[d.getDay()]+'</span><span class="dateMatrix__dayNum">'+(day<10?"0"+day:day)+'</span><span class="dateMatrix__dayMonth">'+mesi[d.getMonth()]+'</span>';

			},
			
			depCode=getParameterByName("from"),
			arrCode=getParameterByName("to"),
			young=getParameterByName("isYoung"),
			labelScopri=labelScopri,
			//currency=currency,
			
			render=function(){
				var
					dep = YYYYMMDDtoDate(matrixDeparture),
					ret = YYYYMMDDtoDate(matrixReturn);
					// findMore = 'Entdecken' // Scopri
					findMore = '<span class="i-lens"></span><span class="txt">' + labelScopri + '</span>' // Scopri

				///var
				for(var r=1;r<8;r++)
					getCell(0,r).html(makeHeadDate(shiftDate(dep,r-4)));
				for(var c=1;c<8;c++){
					var
						d_c=shiftDate(ret,c-4),
						d_c_YYYYMMDD=dateToYYYYMMDD(d_c);
					///var
					getCell(c,0).html(makeHeadDate(d_c));
					for(var r=1;r<8;r++){
						var
							d_r=shiftDate(dep,r-4),
							d_r_YYYYMMDD=dateToYYYYMMDD(d_r);
						///var
						getCell(c,r).removeClass('isActive');
						if (matrixData[d_r_YYYYMMDD+"_"+d_c_YYYYMMDD]) {
							getCell(c,r).html('<a class="dateMatrix__offer' + (bestPrice == d_r_YYYYMMDD+"_"+d_c_YYYYMMDD ? ' bestPrice' : '') + '" href="javascript:;">'+
								'<span class="dateMatrix__currency">'+currency.code+'</span>'+
								'<span class="dateMatrix__amount">'+matrixData[d_r_YYYYMMDD+"_"+d_c_YYYYMMDD] + detailOfferMarketStar + '</span>'+
								'<span class="dateMatrix__fulldateDep" style="display:none">'+d_r_YYYYMMDD.substring(6,8)+"/"+d_r_YYYYMMDD.substring(4,6)+"/"+d_r_YYYYMMDD.substring(0,4)+'</span>'+
								'<span class="dateMatrix__fulldateRet" style="display:none">'+d_c_YYYYMMDD.substring(6,8)+"/"+d_c_YYYYMMDD.substring(4,6)+"/"+d_c_YYYYMMDD.substring(0,4)+'</span>'+
								'</a>');
							addClickListeners(getCell(c,r).children('.dateMatrix__offer'));
						} else{
							getCell(c,r).html(' ');
							addClickListeners(getCell(c,r).children('.dateMatrix__findMore'));
						}

					}
				}
				
				dateMatrixOnResize();
			};
			
			addClickListeners = function(element){
				element.off().on("click",function(e){
					$('.dateMatrix__offer, .dateMatrix__findMore').parent().removeClass('isActive');
					$(e.currentTarget).parent().addClass('isActive');
				});

			}
			
		///var
		$dateMatrix.find("[data-shift]").off("click");
		$dateMatrix.find("[data-shift]").on("click",function() {
			var $this=$(this);
			if ($this.is(".dateMatrix__navDeparture *")) {
				matrixDeparture = dateToYYYYMMDD(shiftDate(
						YYYYMMDDtoDate(matrixDeparture), $(this).data("shift")));
			} else {
				matrixReturn = dateToYYYYMMDD(shiftDate(
						YYYYMMDDtoDate(matrixReturn), $(this).data("shift")));
			}
			//render();
			retrieveMatrixData(depCode, arrCode, matrixDeparture, matrixReturn,
					young, currency, labelScopri);
			return false;
		});
		
		render();

		/*$('.dateMatrix__offer, .dateMatrix__findMore').on("click",function(){
			$('.dateMatrix__offer, .dateMatrix__findMore').parent().removeClass('isActive')
			$(this).parent().addClass('isActive');
			
			//update form data
			$(".flightSel__footer input[name='DepartureDate']").attr("value", $(this).find(".dateMatrix__fulldateDep").text());
			$(".flightSel__footer input[name='ReturnDate']").attr("value", $(this).find(".dateMatrix__fulldateRet").text());
		});*/
		$('.dateMatrix__offer, .dateMatrix__findMore').on("click",function(){
			$('.dateMatrix__offer, .dateMatrix__findMore').parent().removeClass('isActive')
			$(this).parent().addClass('isActive');
			var market = $('html').attr('lang').split('-')[1];
			var depDate = $(this).find(".dateMatrix__fulldateDep").text();
			var retDate = $(this).find(".dateMatrix__fulldateRet").text();
			if (market == "us" || market == "ca") {
				if (depDate.length > 0) {
					var depDateElems = depDate.split("/");
					depDate = depDateElems[1] + "/" + depDateElems[0] + "/" + depDateElems[2];
				}
				if (retDate.length > 0) {
					var retDateElems = retDate.split("/");
					retDate = retDateElems[1] + "/" + retDateElems[0] + "/" + retDateElems[2];
				}
			}

			//update form data
			$(".flightSel__footer input[name='DepartureDate']").attr("value", depDate);
			$(".flightSel__footer input[name='ReturnDate']").attr("value", retDate);
		});
		
		$dateMatrix.addClass("isRendered");
		$(".dateMatrix__offer.bestPrice").trigger("click");
	})(); // matrix period switch
}

function renderDataMock(currency, labelScopri) {
	renderMain({"result":{"arrival":{"city":"New York","code":"NYC","countryCode":"IT"},"bestPrice":"100","departure":{"city":"Roma","code":"ROM","countryCode":"IT"},"international":true,"rangeHigh":"20160115","rangeLow":"20150915","roundTrip":true,"values":{"2015":{"9":{"15":{"amount":"100","height":"100","returnDate":"20152609"}},"10":{"15":{"amount":"200","height":"120","returnDate":"20152610"}},"11":{"15":{"amount":"300","bestPrice":true,"height":"140","returnDate":"20152611"}},"12":{"15":{"amount":"400","height":"160","returnDate":"20152612"}}},"2016":{"1":{"15":{"amount":"500","height":"180","returnDate":"20162601"}},"2":{"15":{"amount":"601","height":"200","returnDate":"20162602"}}}}}}, false, currency, labelScopri);
	renderMain({"result":{"departure":{"code":"MIL","city":"Milano","countryCode":"IT"},"arrival":{"code":"ROM","city":"Roma","countryCode":"IT"},"international":false,"bestPrice":"100","roundTrip":false,"rangeLow":"20150915","rangeHigh":"20160115","values":{"2015":{"9":{"15":{"highlight":true,"amount":"100"},"16":"200","17":"300","18":"400","19":"500"}}}}}, false, currency, labelScopri);
	$(".dateHistogram__item").off("click");
	renderDateMatrix(currency, labelScopri, "20150701", "20150708", {
		"20150630_20150708": 2345,
		"20150701_20150709": 2345,
		"20150630_20150711": 2345,
		"20150704_20150705": 2345,
		"20150704_20150706": 2345,
		"20150704_20150707": 2345,
		"20150704_20150708": 2345,
		"20150704_20150709": 2345,
		"20150704_20150710": 2345,
		"20150704_20150711": 2345
	}, "20150704_20150711");
	getOfferSocialShare("ROM", "100", "false");
}

function insertTabs() {
	//defaultEmptyDivPreview
	var destinationSelect = '<section class="userMenu destinationMenu defaultEmptyDivPreview">'+
	'<a class="userMenu__support active" id="selectOneWay" href="javascript:;"><span class="userMenu__text">Nazionale solo andata</span></a>'+
	'<a class="userMenu__support" id="selectRoundTrip" href="javascript:;"><span class="userMenu__text">Internazionale A/R</span></a></section>';
	$("body").prepend(destinationSelect);
	$(".destinationMenu .userMenu__support").click(function() {
		if ($(this).is("#selectOneWay")) {
			$(this).addClass("active");
			$("#selectRoundTrip").removeClass("active");
			$("section.flightSel").addClass("fs-oneWay");
			$("section.flightSel").removeClass("fs-roundTrip");
			$("section.flightSel").addClass("fs-national");
			$("section.flightSel").removeClass("fs-international");
			$(".flightSel__oneWay").show();
			$(".flightSel__roundTrip").hide();
			$(".heroCarousel__textContainerSubtitles.text-oneWay").show();
			$(".heroCarousel__textContainerSubtitles.text-roundTrip").hide();
			$(".flightSel__callToAction").show();
			$(".cercaVolo-infoText-roundTrip").hide();
		} else {
			$(this).addClass("active");
			$("#selectOneWay").removeClass("active");
			$("section.flightSel").addClass("fs-roundTrip");
			$("section.flightSel").removeClass("fs-oneWay");
			$("section.flightSel").addClass("fs-international");
			$("section.flightSel").removeClass("fs-national");
			$(".flightSel__roundTrip").show();
			$(".flightSel__oneWay").hide();
			$(".heroCarousel__textContainerSubtitles.text-roundTrip").show();
			$(".heroCarousel__textContainerSubtitles.text-oneWay").hide();
			$(".cercaVolo-infoText-roundTrip").show();
			$(".flightSel__callToAction").hide();
		}
	});
}