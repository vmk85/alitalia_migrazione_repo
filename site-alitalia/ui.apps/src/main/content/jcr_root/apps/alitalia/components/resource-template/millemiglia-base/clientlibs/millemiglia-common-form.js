function showFormFeedbackError(formId, errorMessageI18nKey) {
	var $formFeedback = $('form#' + formId).find(
			'.millemiglia__formFeedback.j-formFeedback');

	// remove success
	$formFeedback.removeClass('isActive');
	$formFeedback.find('p').removeClass('success');
	$formFeedback.find('span.millemiglia__formFeedbackIcon').removeClass(
			'i-check_full');

	// add fail
	$formFeedback.find('p').addClass('fail');
	var classes = $formFeedback.find('span.millemiglia__formFeedbackIcon')
			.attr('class');
	classes = 'i-alert ' + classes;
	$formFeedback.find('span.millemiglia__formFeedbackIcon').attr('class',
			classes);
	errorMsg = $formFeedback.find('span.millemiglia__formFeedbackText').text();
	if (errorMsg.length == 0) {
		errorMsg = getI18nTranslate(errorMessageI18nKey, true);
	}
	$formFeedback.find('span.millemiglia__formFeedbackText').text(errorMsg);
	$formFeedback.addClass('isActive');
}

function showFormFeedbackSuccess(formId, successMessageI18nKey) {
	var $formFeedback = $('form#' + formId).find(
			'.millemiglia__formFeedback.j-formFeedback');

	// remove fail
	$formFeedback.removeClass('isActive');
	$formFeedback.find('p').removeClass('fail');
	$formFeedback.find('span.millemiglia__formFeedbackIcon').removeClass(
			'i-alert');

	// add success
	$formFeedback.find('p').addClass('success');
	var classes = $formFeedback.find('span.millemiglia__formFeedbackIcon')
			.attr('class');
	classes = 'i-check_full ' + classes;
	$formFeedback.find('span.millemiglia__formFeedbackIcon').attr('class',
			classes);
	$formFeedback.find('span.millemiglia__formFeedbackText').text(
			getI18nTranslate(successMessageI18nKey));
	$formFeedback.addClass('isActive');
}
