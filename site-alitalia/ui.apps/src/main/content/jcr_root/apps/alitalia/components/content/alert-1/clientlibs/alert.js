var alertOptions = {};

//document ready
$(document).ready(function(){
	$('.bg-alert .ico-close').click(function(){
		alertOptions.closeAlert();
 	    settingCookie("closeAlert", "Alert Visualizzato", 20);
	});
});

alertOptions.closeAlert = function() {
    $('.bg-alert').slideUp(200, function() {
    cercaVoloOptions.calculateAccordionTop();
    });
};

function settingCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (1000*60*exdays));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var nameEQ = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return "";
}