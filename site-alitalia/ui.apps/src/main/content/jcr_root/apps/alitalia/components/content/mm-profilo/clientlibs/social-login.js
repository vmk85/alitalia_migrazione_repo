setI18nTranslate('millemiglia.socialLogin.ajaxError');
setI18nTranslate('millemiglia.socialLogin.success');

$('#socialLoginSumbit').on('click', function() {
	$('form#socialLoginEdit').submit();
});

$(window).on('load', function() {
	$accordionSocialLogin = $('#accordion1_tab3');

	if (resultSocialLoginSubmit == "true") {
		$accordionSocialLogin.click();
		showFormFeedbackSuccess('socialLoginEdit',
				'millemiglia.socialLogin.success');
	} else if (resultSocialLoginSubmit == "false") {
		$accordionSocialLogin.click();
		showFormFeedbackError('socialLoginEdit',
				'millemiglia.socialLogin.ajaxError');
	}

	if (resultSocialLoginSubmit == "true"
		|| resultSocialLoginSubmit == "false") {
		$accordionSocialLogin[0].scrollIntoView();
	}
});