var checkinFormId = "checkInSearch";
var checkinLoginFormId = "checkInLogInSearch";
var mmbFormId = "myFlightSearch";
var PNRSize = 6;

$(document).ready(function(){
    $('#checkInSubmit').fastClick(
    		function(e){
    			widgetValidate(e, checkinFormId);
    		});
    $('#checkInLoginSubmit').fastClick(
    		function(e){
    			widgetValidate(e, checkinLoginFormId);
    		});
    $('#myFlightSubmit').fastClick(
    		function(e){
    			widgetValidate(e, mmbFormId);
    		});
    
    /*Show PNR search error*/
    var success = getParameterByName('success');
	if (success == 'false') {
		$('#errorField_ins').show();
		$('.form__errorField').addClass('isError');
	}
});

function widgetValidate(e, idForm){
	validation(e, "preparaviaggiosubmit", "#" + idForm
			, function(data){
				widgetSubmit(data , idForm);
			}
			, function(data){
				widgetFailure(data, idForm);
			});
}

function widgetSubmit(data, idForm){
	if (data.result) {
		removeErrors();
		if(isPnrSearch(idForm)){
			showWaitingPage();
		}
		performSubmit('preparaviaggiosubmit', "#" + idForm
				, function(data){
					widgetSubmitSuccess(data , idForm);
				}
				, function(data){
					widgetFailure(data, idForm);
				});
	} else {
		showErrors(data);
		hideWaitingPage();
		/*Track errors*/
		if(idForm == checkinFormId && idForm == checkinLoginFormId 
				&&  typeof window["analytics_trackCheckinErrors"] == "function"){
			analytics_trackCheckinErrors(data.fields);
		}
	}
}

function widgetSubmitSuccess(data, idForm){
	if (data.result && data.redirect) {
		window.location.href = data.redirect;
	}
	else{
		/*TODO manage errors*/
		showErrors(data);
		hideWaitingPage();
		if(idForm == checkinFormId && idForm == checkinLoginFormId  
				&&  typeof window["analytics_trackCheckinErrors"] == "function"){
			analytics_trackCheckinErrors(data.fields);
		}
	}
}

function widgetFailure(idForm){
	console.log('fail ' + idForm);
	hideWaitingPage();
}

function isPnrSearch(idForm){
	return (idForm === checkinFormId || idForm === mmbFormId) && $("#" + idForm + " [name='code']").val().length === PNRSize;
}

function showWaitingPage(){
	$("header").hide();
	$("#cercaVoliForm").hide();
	$(".boxNews").hide();
	resultsLoading.init();
}

function hideWaitingPage(){
	$('.waitingPage').hide();
	$("header").show();
	$("#cercaVoliForm").show();
	$(".boxNews").show();
}