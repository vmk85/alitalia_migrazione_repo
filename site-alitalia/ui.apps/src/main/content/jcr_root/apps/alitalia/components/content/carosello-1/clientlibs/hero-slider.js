var heroSliderOptions = {};

$(document).ready(function() {

    if($('html').attr('dir') == "ltr"){
	heroSliderOptions.foregroundSlider = new Swiper('.hero__slider_foreground .swiper-container', {
		'speed': 1000,
		'effect': 'fade',
		allowTouchMove: false,
		fadeEffect: { crossFade: true },
	});

	heroSliderOptions.backgroundSlider = new Swiper('.hero__slider_background .swiper-container', {
		'loop': true,
		'speed': 1500,
		autoplay: {
			delay: 5000,
		},
		'allowTouchMove': true,
		pagination: {
			el: '.hero__slider .swiper-pagination',
			clickable: true,
		},

		// NOTA: questo meccanismo non funziona benissimo, confonde gli indici quando gli swiper sono in loop.
		/*controller: {
			control: heroSliderOptions.foregroundSlider
		}*/
		on: {
			'slideChange': function() {
				heroSliderOptions.foregroundSlider.slideTo(this.realIndex);
			}
		}
	});
    }

    if($('html').attr('dir') == "rtl"){

	heroSliderOptions.foregroundSlider = new Swiper('.hero__slider_foreground .swiper-container-rtl', {
		'speed': 1000,
		'effect': 'fade',
		allowTouchMove: false,
		fadeEffect: { crossFade: true },
	});


	heroSliderOptions.backgroundSlider = new Swiper('.hero__slider_background .swiper-container-rtl', {
    		'loop': true,
    		'speed': 1500,
    		autoplay: {
    			delay: 5000,
    		},
    		'allowTouchMove': true,
    		pagination: {
    			el: '.hero__slider .swiper-pagination',
    			clickable: true,
    		},

    		// NOTA: questo meccanismo non funziona benissimo, confonde gli indici quando gli swiper sono in loop.
    		/*controller: {
    			control: heroSliderOptions.foregroundSlider
    		}*/
    		on: {
    			'slideChange': function() {
    				heroSliderOptions.foregroundSlider.slideTo(this.realIndex);
    			}
    		}
    	});
    }

	$('.hero__slider .bg-image').foundation();

	// clickable slider
	$( '.hero__slider' ).on( 'click', function(e) {
		// if click on hero slider, but not slider bullet
		if(!$(e.target).hasClass('swiper-pagination-bullet')) {
			// current link in foreground slider
			var currHeroSliderLink = ($( this ).find( '.hero__slider_foreground .swiper-slide-active a' ).attr( 'href' ));
			// change location to current link
			window.location = currHeroSliderLink;
		}
	});
});