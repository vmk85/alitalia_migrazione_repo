
function initPartialCheckinInsurance() {
}


/* add insurance */

function be_addInsurance(e, btn, section, callback) {
	performValidation('checkinancillarycartaddinsurance', '#checkin-insuranceBox-form', 
			function(data){
			checkinAncillaryAddInsuranceValidationSuccess(data);
			callback(e);
			}, checkinHandleInvokeError);
}

function checkinAncillaryAddInsuranceValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('checkinancillarycartaddinsurance', '#checkin-insuranceBox-form', 
				checkinAncillaryAddInsuranceSubmitSuccess, checkinHandleInvokeError);
	} else {
		showErrors(data);
	}
}

function checkinAncillaryAddInsuranceSubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		if (data.isError) {
			checkinHandleServiceError(data);
		}
		jQuery(".insurance__feedback").show();
		refreshCheckinPartialCart();
//		refreshCheckinPartialInsurance(true, true);
	}
}


/* clear insurance */

function be_cancelInsurance(e, btn, section, callback) {
	performSubmit('checkinancillarycartremoveinsurance', '#checkin-insuranceBox-form',
			checkinAncillaryClearInsuranceValidationSuccess, checkinHandleInvokeError);
}

function checkinAncillaryClearInsuranceValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('checkinancillarycartremoveinsurance', '#checkin-insuranceBox-form', 
				checkinAncillaryClearInsuranceSubmitSuccess, checkinHandleInvokeError);
	} else {
		showErrors(data);
	}
}

function checkinAncillaryClearInsuranceSubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		if (data.isError) {
			checkinHandleServiceError(data);
		}
		refreshCheckinPartialInsurance(true, true);
		refreshCheckinPartialCart();
	}
}
