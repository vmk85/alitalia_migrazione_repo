"use strict";

use(function() {
    var firstname = this.firstname;
    var lastname = this.lastname;

    return (firstname != null && lastname != null ? {firstName: firstname.toLowerCase(), lastName: lastname.toLowerCase()} : false);
});