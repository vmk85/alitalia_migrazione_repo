(function($) {
	$(window).ready(function() {
		if (typeof countries != 'undefined' && typeof urls != 'undefined' && countries && urls) {
			var selectedCountry;
			var selectedLanguage;
			var $userMenuLanguage =
				$('.userMenu__language').find('span.userMenu__text');
			
			$('#languageMenu-country').empty();
			if (countries.countries && countries.countries.length > 0) {
				for (var i = 0; i < $(countries.countries).length; i++) {
					var selected = '';
					if (i === countries.countrySelected) {
						selected = 'selected';
						selectedCountry = countries.countries[i].name;
					}
					$('#languageMenu-country').append('<option ' + selected
							+ ' value="' + countries.countries[i].code + '">'
							+ countries.countries[i].name
							+ '</option>');
				}
							
				
				$('#languageMenu-country').bind('change', function() {
					var valueCountrySelected = document.getElementById("languageMenu-country").value;
					$('#languageMenu-language').empty();
					for (var i = 0; i < $(countries.countries).length; i++) {

						if (countries.countries[i].code === valueCountrySelected) {
							for (var s = 0; s < $(countries.countries[i].languages).length; s++) {
                                $('#languageMenu-language').append('<option value="'
									+ countries.countries[i].languages[s].code + '">'
									+ countries.countries[i].languages[s].name
									+ '</option>');
                            }
                        }
					}

				});
				
			}
			
			$('#languageMenu-language').empty();
			if (countries.countries && countries.countries.length > 0) {
				for (var i = 0; i < $(countries.countries[countries.countrySelected].languages).length; i++) {
					var selected = '';
					if (i === countries.languageSelected) {
						selected = 'selected';
						selectedLanguage = countries.countries[countries.countrySelected].languages[i].name;
					}
					$('#languageMenu-language').append('<option ' + selected
						+ ' value="' + countries.countries[countries.countrySelected].languages[i].code + '">'
						+ countries.countries[countries.countrySelected].languages[i].name
						+ '</option>');
				}
				
				$('#confirmButton').bind('click', function() {
					countryCode = $('#languageMenu-country').find(':selected').val();
					languageCode = $('#languageMenu-language').find(':selected').val();
					countryLangCode = languageCode + '_' + countryCode;
					urlDirect = urls[countryLangCode];
					if ($('#rememberMeLang').is(':checked')) {
						try {
							if (typeof (Storage) !== "undefined"
									&& typeof (localStorage) !== "undefined") {
								var languageObject = {
									country: $('#languageMenu-country')
										.find(':selected').text(),
									language: $('#languageMenu-language')
										.find(':selected').text()
								}
								languageObject = JSON.stringify(languageObject);
								localStorage.setItem('languageObject', languageObject);
							}
						} catch (e) {}
						
						var expires = new Date();
				        expires.setTime(expires.getTime() + (10 * 24 * 60 * 60 * 365 *1000));
				        document.cookie= 'alitalia_consumer_locale' + "=" + countryCode+'_'+languageCode + '; expires='+ expires.toUTCString() + "; path=/";
					}
					window.location = urlDirect;
				});
				
			}
			
			var s = document.getElementById("languageMenu-country");
			if (!(s.value == "us")) {


                $("#languageMenu-country").append($("#languageMenu-country option").remove().sort(function(a, b) {
                    var at = $(a).text(), bt = $(b).text();
                    return (at > bt)?1:((at < bt)?-1:0);
                }));

            	for (var i = 0; i < $(countries.countries).length; i++) {
                        if (i === countries.countrySelected) {
                            selectedCountry = countries.countries[i].name;
                            $("#languageMenu-country option")
                                .removeAttr('selected')
                                .filter('[value='+countries.countries[i].code+']')
                                .attr('selected', true);
                        }
                }
            }

			$userMenuLanguage.empty();
			$userMenuLanguage.append(selectedCountry + ' - ' + selectedLanguage);
		}

		$("a.j-closeCookie").on("click", function() {
			var adesso = new Date();
			var scadenza = new Date();
			scadenza.setTime(adesso.getTime() - 1);
			document.cookie = "no3rdparty=; expires=" + scadenza.toGMTString() + "; path=/";
			document.cookie = "nomarketing=; expires=" + scadenza.toGMTString() + "; path=/";
		});
		/*Fix WCAG menu access with keyboard*/
        $(".mainMenu__firstLevelLink").focus(function(evt){
            $(this).parent().siblings().children("a").mouseout();
          });
           $(".mainMenu__firstLevelLink").keydown(function(evt){
        	   /*SPACE*/
               if (evt.which == 32) {
                   evt.preventDefault();
                   $(this).mouseover();
                   return false;
               }
          });
	});

})(jQuery);

function bindAutocompleteGSA (gsaHost, site, client, marketCode, languageCode) {
	$( "#mainMenu__search" ).autocomplete({
		lookup : function(query, done) {
	        $.ajax({
		          url: gsaHost+"/suggest",
		          dataType: 'jsonp',
		          jsonpCallback: 'populateGSAField',
		          jsonp: false,
		          async: false,
		          data: {
		        	max: "5",
		            site: site+"_"+languageCode+"_"+marketCode,
		            client: client+"_"+languageCode+"_"+marketCode,
		            access: "p",
		            format: "rich",
		            callback: "populateGSAField",
		            q: query
		          }
		        });
	      },
	      minLength: 1
	  });
	
}
function populateGSAField (data) {
	$('#suggestion_data').empty();
	jQuery.each(data.results, function(i, v) {
		$('#suggestion_data').append ('<div style="-webkit-user-select: none;" class="ss-gac-c" onClick="setSuggestion(\''+v.name+'\')">'+v.name+'</div>');
	});
}

function setSuggestion (value) {
	$('#mainMenu__search').val(value);
	$('#suggestion_form').submit();
}

