function initMyFlights() {
	$('.mmb').bind('click', function(e) {
		e.preventDefault();
		var pnr = $(this).parent('.iMieiVoli__showBtn').children('.pnr').val();
		visualizzaVoliHandler(pnr);
	});
}

function visualizzaVoliHandler(pnr) {
	var passengerName = $('#name').val();
	var passengerSurname = $('#surname').val();
	var form = {
		mieivoli__code : pnr,
		mieivoli__name : passengerName,
		mieivoli__surname : passengerSurname
	
	};
	
	performSubmit('mmbsessioninit', form,
			visualizzaVoliSuccess, visualizzaVoliError);
		
}

function visualizzaVoliSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		// FIXME: manage error
	}
}

function visualizzaVoliError() {
	// FIXME: manage error
}

