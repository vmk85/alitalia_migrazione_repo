$(document).ready( function (){
    $('#richiestaRimborsoButton').click(function () {
        if (formIsCompleted()) {
            enableLoaderMyAlitalia();
            performValidation("richiesta-rimborso-submit", "#form-richiesta-rimborso", richiestaRimborsoValidationSuccess, richiestaRimborsoValidationFail)
        } else {
        }
     });
});

function formIsCompleted () {
    if (validateNameIsFull()){

     if (validateSurnameIsFull()) {

      if (validateTicketLength()) {

       if (validatePNRLength()) {

        if(validateReasonIsFull()) {

             if (validateQuantityIsFull()) {

                  if (validatePrivacyIsFull()) {
                     return true;
                  } else {
                     return false;
                  }

             } else {
                return false;
             }

        } else {
           return false;
        }

       } else {
            $("html, body").delay(200).animate({scrollTop: $('#pnr').offset().top }, 500);
            return false;
       }

      } else {
        $("html, body").delay(200).animate({scrollTop: $('#numeroBiglietto').offset().top }, 500);
        return false;
      }

     } else {
          $("html, body").delay(200).animate({scrollTop: $('#cognome').offset().top }, 500);
          return false;
     }

    } else {
         $("html, body").delay(200).animate({scrollTop: $('#nome').offset().top }, 500);
         return false;
    }
   };



function richiestaRimborsoValidationSuccess(data) {
    if (data.result){

        performSubmit("richiesta-rimborso-submit", "#form-richiesta-rimborso", richiestaRimborsoSubmitSuccess, richiestaRimborsoSubmitFail)

    } else {
        disableLoaderMyAlitalia();
        showErrors(data)
    }

}

function richiestaRimborsoValidationFail(data) {
    console.log("Errore sulla validazione");
    disableLoaderMyAlitalia();
}

function richiestaRimborsoSubmitSuccess(data) {
    if (!data.isError){
        $('.formPage__sectionCover').hide();
        $('.richiesta-rimborso-success').show();
        disableLoaderMyAlitalia();
    } else {
        console.log("Errore invio mail");
        disableLoaderMyAlitalia();
    }
}

function richiestaRimborsoSubmitFail(data) {
    console.log("Errore sull'invio dei dati");
    disableLoaderMyAlitalia();
}

function validatePNRLength() {
    var pnr = $("#pnr").val();
    if (pnr.length == 6) {
        $("#pnr").removeClass("isErrore");
        $("#pnrError").addClass("hide");
        return true;
    } else {
        $("#pnr").addClass("isErrore");
        $("#pnrError").removeClass("hide");
        return false;
    }
}

function validateTicketLength() {
    var ticketNumber = $("#numeroBiglietto").val();
    if (ticketNumber.length == 13) {
        $("#numeroBiglietto").removeClass("isErrore");
        $("#ticketError").addClass("hide");
        return true;
    } else {
        $("#numeroBiglietto").addClass("isErrore");
        $("#ticketError").removeClass("hide");
        return false;
    }
}

function validateNameIsFull() {
        var name = $("#nome").val();
        $("#nomeError").addClass("hide");
        $("#nomeErrorChar").addClass("hide");
    if (name.length > 0) {
        $("#nome").removeClass("isErrore");
        $("#nomeError").addClass("hide");
        $("#nomeErrorChar").addClass("hide");
        if (isAlphabeticWithAccentAndSpaces(name)) {
        $("#nomeErrorChar").addClass("hide");
        return true;
        } else {
            $("#nome").addClass("isErrore");
            $("#nomeErrorChar").removeClass("hide");
            return false
        }
    } else {
        $("#nome").addClass("isErrore");
        $("#nomeError").removeClass("hide");
        return false;
    }
}

function validateSurnameIsFull() {
        var surname = $("#cognome").val();
        $("#cognomeError").addClass("hide");
        $("#cognomeErrorChar").addClass("hide");
    if (surname.length > 0) {
        $("#cognome").removeClass("isErrore");
        if (isAlphabeticWithAccentAndSpaces(surname)) {
        $("#cognomeErrorChar").addClass("hide");
        return true;
        } else {
            $("#cognome").addClass("isErrore");
            $("#cognomeErrorChar").removeClass("hide");
            return false
        }
    } else {
        $("#cognome").addClass("isErrore");
        $("#cognomeError").removeClass("hide");
        return false;
    }
}

function validateReasonIsFull() {
    var reason = $("#motivo").val();
    if (reason == "") {
        $("#motivoError").removeClass("hide");
        return false;
    } else {
        $("#motivoError").addClass("hide");
        return true;
    }
}

function validateQuantityIsFull() {
    if ( $('input[name="tipo"]').is(':checked') ) {
        $("#quantityError").addClass("hide");
        return true;
    } else {
        $("#quantityError").removeClass("hide");
        return false;
    }
}

function validatePrivacyIsFull() {
    if ( $('input[name="checkAgreement"]').is(':checked') ) {
        $("#privacyError").addClass("hide");
        return true;
    } else {
        $("#privacyError").removeClass("hide");
        return false;
    }
}

function isAlphabeticWithAccentAndSpaces(value) {
	var regex = new RegExp("^[a-zA-Z\\sàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝ]*$");
	return value == null || value == "" || regex.test(value);
}