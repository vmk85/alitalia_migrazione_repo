$(document).ready(function() {
	$('#datiPersonaliSubmit').on('click', personalDataSubmit);
	
	$( "#nation" ).change(function() {
		selectStates($('#nation').val());
	});
});


function personalDataSubmit(e) {
	$('#datiPersonaliSubmit').off('click', personalDataSubmit);
	$('#datiPersonaliSubmit').addClass('isDisabled');
	$(".genericErrorMessage").hide();
	validation(e, 'carnetpersonaldata', '#personalDataForm', personalDataSubmitSuccess, personalDataSubmitError);
}

function personalDataSubmitSuccess(data) {
	if (data.result) {
		removeErrors();
		
		performSubmit('carnetpersonaldata', '#personalDataForm', 
		function(data) {
			if (data.redirect) {
				window.location.replace(data.redirect);
			}
		}, 
		personalDataSubmitError);
		
	} else {
		$('#datiPersonaliSubmit').on('click', personalDataSubmit);
		$('#datiPersonaliSubmit').removeClass('isDisabled');
		showErrors(data);
		
		if(typeof window["analytics_trackCheckinErrors"] == "function"){
			analytics_trackCheckinErrors(data.fields);
		}
		
	}
}

function personalDataSubmitError(data) {
	$('#datiPersonaliSubmit').on('click', personalDataSubmit);
	$('#datiPersonaliSubmit').removeClass('isDisabled');
	showFailMessage(data.errorMessage);
}

function showFailMessage(message) {
	$(".genericErrorMessage").show();
	if (message) {
		$(".genericErrorMessage__text").text(message);
		if(typeof window["analytics_trackCheckinErrors"] == "function"){
			analytics_trackCheckinErrors(message);
		}
	}	
}

function selectStates(nation) {
	nation = nation.toUpperCase();
	var states="";
	if (nation=="CA") {
		states = $('#stCA').val();
	}
	if (nation=="IT") {
		states = $('#prov').val();
	}
	if (nation=="US") {
		states = $('#stUS').val();
	}
	getDropDownValues(states);
}

function getDropDownValues(value) {
	var option = "";
	$('div.customSelect').find('select#province').text(option);
	if (value!="") {
		var states = value.split('#');
		var k;
		for (k=0; k<states.length; k++) {		
			var singleProvince = states[k].split('\|');
			option = '<option value="' + singleProvince[0] + '"> ' + singleProvince[1] + ' </option>';
			$('div.customSelect').find('select#province').append(option);
		}
	}
}