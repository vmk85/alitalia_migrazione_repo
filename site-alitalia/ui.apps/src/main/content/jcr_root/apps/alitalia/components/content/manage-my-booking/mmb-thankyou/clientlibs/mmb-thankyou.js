/**
 * Re-inizializzazione della sessione mmb
 */
function initMmbThankYou() {

	var form = {
			mieivoli__code: tripId,
			mieivoli__name: passengerName, 
			mieivoli__surname: passengerSurname,
			force__clear: '1'
	};
	
	performSubmit('mmbsessioninit', form,
		function(data) {
			if (data.result == 'OK') {
				console.log("re-init mmb session success");
			} else if (data.result == 'NOK') {
				console.log("re-init mmb session fail");
			}
		}, 
		function() {
			console.log("re-init mmb session error");
		}
	);
	
	
	/**
	 * Duplicato di una funzione anonima di main.js - da eliminare se Bitmama darà un nome alla funzione in main.js
	 */
	$('[data-printer]').unfastClick().fastClick(function(e){
		e.preventDefault();
		var className = $(this).attr('data-printer');
		$('body').addClass(className);
		window.print();
		$('body').removeClass(className);
	});
	
};