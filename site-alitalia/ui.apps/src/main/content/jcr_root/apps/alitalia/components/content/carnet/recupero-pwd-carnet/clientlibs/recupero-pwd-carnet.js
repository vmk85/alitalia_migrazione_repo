function recuperaPasswordHandler(e) {
	validation(e, 'retrievepassword', '#form-recupera-password-carnet', recuperaPasswordSuccess, recuperaPasswordError);
}

function recuperaPasswordSuccess(data) {
	return (data.result ? recuperaPasswordContinue(false) : showErrors(data));
}

function recuperaPasswordError() {
	return recuperaPasswordContinue(true);
}

function recuperaPasswordContinue(stop) {
	removeErrors();
	return stop || $('#recuperapasswordSubmit').click();
}

$(document).ready(function(){
	$('#recuperapasswordSubmit').bind('click', recuperaPasswordHandler);
});