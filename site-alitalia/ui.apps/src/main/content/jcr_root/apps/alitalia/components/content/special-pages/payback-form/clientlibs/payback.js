jQuery(document).ready(function(){
	jQuery('#paybackSubmit').on('click', paybackAccrualHandler);
});
function paybackAccrualHandler(e) {
	$('#paybackSubmit').off('click');
	validation(e, 'paybackaccrualsubmit', '#specialpageform_payback', paybackValidationSuccess, paybackAccrualError);
	return false;
}

function paybackValidationSuccess(data) {
	if(data.result){
		removeErrors();
		$("#specialpageform_payback").submit();
	}
	else{
		showErrors(data);
		jQuery('#paybackSubmit').on('click', paybackAccrualHandler);
	}
}

function paybackAccrualError() {
	console.log("Errore in paybackAccrual")
	removeErrors();
	jQuery('#paybackSubmit').on('click', paybackAccrualHandler);
}

