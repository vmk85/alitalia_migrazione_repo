jQuery(document).ready(function() {
	//error messages logic

	//feedback error cookies
 if(getURLParameter("success") == "false" && getURLParameter("from") == "cookiessubmit"){
		$(".formPage").html(CQ.I18n.get('specialpage.cookies.error.label')).css({ "font-size": "200%"});
	}
    if (getURLParameter("success") == "true" && getURLParameter("from") == "cookiessubmit"){
        $(".formPage").html(CQ.I18n.get('specialpage.cookies.success.label')).css({ "font-size": "200%"});
    }

	// date controls
	jQuery("#month").on("change", dateChanged);
	jQuery("#year").on("change", dateChanged);
	dateChanged();

	var from = window.location.search.substr(1);
	if (from) from = window.location.search.substr(1).split("&");
	if (from[1]) {
		from = from[1].split("=");
		if (from[1]) {
			from = from[1];
			if (from == 'cookiessubmit') {
				$("#backtohomebutton").hide();
			}
		}
	}
});

function dateChanged() {
	maxDay = jQuery("#day option:last").val();
	selectedDay = jQuery("#day").val();
	month = jQuery("#month").val();
	year = jQuery("#year").val();
	newMaxDay = daysInMonth(parseInt(month), parseInt(year));
	if (maxDay != newMaxDay) {
		jQuery("#day option:gt(0)").remove();
		for (var i = 1; i <= newMaxDay; i++) {
			jQuery("#day").append(
					"<option value=\"" + i + "\">" + i + "</option>");
		}
		if (selectedDay && selectedDay <= newMaxDay) {
			jQuery("#day").val(selectedDay);
		} else {
			jQuery("#day").val("");
		}
	}
}

function daysInMonth(m, y) {
	switch (m) {
		case 2:
			return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
		case 0:
		case 4:
		case 6:
		case 9:
		case 11:
			return 30;
		default:
			return 31;
	}
}

function showFormFeedbackError(formId, errorMessageI18nKey) {
	$form = $('form#' + formId);
	if(typeof formId == "undefined" || !formId){
		$form = jQuery("form[id*=specialpageform_]");
	}
	var $formFeedback = $form.find('.millemiglia__formFeedback.j-formFeedback');
	if(typeof errorMessageI18nKey == "undefined" || !errorMessageI18nKey){
		errorMessageI18nKey = $formFeedback.find('span.millemiglia__formFeedbackText').text();
	}


	//remove success
	$formFeedback.removeClass('isActive');	
	$formFeedback.find('p').removeClass('success');
	$formFeedback.find('span.millemiglia__formFeedbackIcon').removeClass('i-check_full');
	
	//add fail
	$formFeedback.find('p').addClass('fail');
	var classes = $formFeedback.find('span.millemiglia__formFeedbackIcon').attr('class');
	classes = 'i-alert ' + classes;
	$formFeedback.find('span.millemiglia__formFeedbackIcon').attr('class', classes);
	$formFeedback.find('span.millemiglia__formFeedbackText').text(getI18nTranslate(errorMessageI18nKey, true));
	$formFeedback.addClass('isActive');
}