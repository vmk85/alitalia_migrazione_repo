"use strict";
use(function () {
    var code = this.code;
    var codeName = "";

    if(code == "YC"){
    codeName = "Economy Classic";
    }
    if(code == "YX"){
    codeName = "Economy Semi Flex";
    }
    if(code == "YD"){
    codeName = "Economy Promo";
    }
    if(code == "YP"){
    codeName = "Economy Premium";
    }
    if(code == "JC"){
    codeName = "Business Classic";
    }
    if(code == "JF"){
    codeName = "Business Flex";
    }
    if(code == "CO"){
    codeName = "Economy Comfort";
    }
    if(code == "YS"){
    codeName = "Economy Saver";
    }
    if(code == "YL"){
    codeName = "Economy Light";
    }
    if(code == "YF"){
    codeName = "Economy Flex";
    }

    return codeName;
});