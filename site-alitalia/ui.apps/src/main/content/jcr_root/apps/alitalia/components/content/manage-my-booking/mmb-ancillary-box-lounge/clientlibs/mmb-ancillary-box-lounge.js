
function initPartialMmbLounge() {
	$('.lounge-checkbox').change(function(e) {
		var checkbox = e.currentTarget;
		var hidden = $(checkbox).siblings('input[type="hidden"].lounge-checkbox-field');
		var currentVal = $(hidden).val();
		var newVal = currentVal.substring(0, currentVal.length - 1) + ( $(checkbox).is(':checked') ? '1' : '0' );
		$(hidden).val(newVal);
	});
}

function be_addLounge(e, btn, section, callback) {
	performValidation('mmbancillarycartlounge', '#mmb-ancillary-add-lounge-form', 
			mmbAncillaryLoungeAddValidationSuccess, mmbHandleInvokeError);
};

function mmbAncillaryLoungeAddValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('mmbancillarycartlounge', '#mmb-ancillary-add-lounge-form', 
				mmbAncillaryLoungeAddSubmitSuccess, mmbHandleInvokeError);
	} else {
		showErrors(data);
	}
}

function mmbAncillaryLoungeAddSubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		if (data.isError) {
			mmbHandleServiceError(data);
		}
		refreshMmbPartialLounge(true, true);  
	}
}

/* clear form */

function be_cancelLounge(e, btn, section, callback) {
	performValidation('mmbancillarycartlounge', '#mmb-ancillary-clear-lounge-form', 
			mmbAncillaryLoungeClearValidationSuccess, mmbHandleInvokeError);
};

function mmbAncillaryLoungeClearValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('mmbancillarycartlounge', '#mmb-ancillary-clear-lounge-form', 
				mmbAncillaryLoungeClearSubmitSuccess, mmbHandleInvokeError);
	} else {
		showErrors(data);
	}
}

function mmbAncillaryLoungeClearSubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		if (data.isError) {
			mmbHandleServiceError(data);
		}
		refreshMmbPartialLounge(false, true);
	}
}