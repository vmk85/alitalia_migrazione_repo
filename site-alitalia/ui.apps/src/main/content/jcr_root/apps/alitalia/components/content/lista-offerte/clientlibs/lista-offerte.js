$(document).ready(function() {
	var dataListCity = {};
	dataListCity.dest = null;
	if (typeof localStorage !== "undefined"
			&& typeof (Storage) !== "undefined") {
		try {
			var items = localStorage.getItem("ricercheRecenti");
			if (items != null && items != 'undefined' && items != '[]'
					&& items.length > 0) {
				items = JSON.parse(items);
				var airportCode = items[0].cittaPartenza.codiceAeroporto;
				dataListCity = {
					dest : airportCode
				};
			}
		} catch (e) {}
	}
	getCitiesForOffers(dataListCity);
	return false;
});

$('.destinationCarousel__link').click(function(){
	  var specialOfferLink = $(this).attr('href');
	  $(this).attr('href',specialOfferLink+'?from='+$('#selectDest').val());
	});

function getCitiesForOffers(dataListCity){
	var url = getServiceUrl("lista-city-partial", "html");
	$.ajax({
		url : url,
		data : {
			destination: dataListCity.dest || "",
			isHomePage: true,
			isYoung: false,
			paxType: "ad"
		},
		context : document.body
	})
	.done(successCities)
	.fail(failCities);
	return false;
}

function successCities(dataCity) {
	$('#partialListCities').empty();
	$('#partialListCities').append(dataCity);
	var data = {
		dest : $('#selectDest').val()
	};
	$('#selectDest').bind('change', function(e) {
		data.dest = $('#selectDest').val();
		$("#partialContentListOfferte .overlayLoading").show();
		getOffersDefaultCity(data);
	});
	getOffersDefaultCity(data);
	return false;
}

function failCities() {
	return false;
}

function getOffersDefaultCity(data) {
	tab1TopEditAreaContent = jQuery.trim(jQuery(
	"#editorialAreaOffers").children("p")
	.text());

	istab1TopEditAreaContentEmpty = (tab1TopEditAreaContent.length == 0);


	var url = getServiceUrl("lista-offerte-partial." + data.dest, "html");
	$.ajax({
		url : url,
		data : {
			from : data.dest,
			isHomePage : true,
			tab1AsteriskInPrice : !istab1TopEditAreaContentEmpty,
			tab2AsteriskInPrice : !istab1TopEditAreaContentEmpty,
			isYoung : false,
			paxType : "ad",
		},
		context : document.body
	})
	.done(successListOffer)
	.fail(failListOffer);
	return false;
}

function successListOffer(data) {
	$('#partialContentListOfferte').empty();
	$('#partialContentListOfferte').append(data);
	setTimeout(function() {
		$("#partialContentListOfferte .overlayLoading").hide();
	}, 1000);
	destinationCarousel();
	return false;
}

function failListOffer() {
	setTimeout(function() {
		$("#partialContentListOfferte .overlayLoading").hide();
	}, 1000);
	return false;
}
