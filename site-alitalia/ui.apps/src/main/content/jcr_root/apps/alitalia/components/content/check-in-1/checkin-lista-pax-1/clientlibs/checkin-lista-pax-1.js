var idxX = 0;
var isExtra = false;
var usaNazionalita = "";
var issueCountry = "";
var selectedPass = "";

$(document).ready(function () {

    setExpiredDocs();
    $("input[name='frequentFlyerNumber']").on('input', function() {
      if($(this).val() != '') {
      } else {
        $("input[name='frequentFlyerNumber']").removeClass('is-invalid-input');
      }
    });

    if ($("input[name='frequentFlyer']").val() == undefined && $("input[name='frequentFlyerNumber']").val() == "") {
        $("input[name='frequentFlyerNumber']").removeClass('is-invalid-input');
    }

    $('.reveal-wrap').foundation();

    // giorni nelle dropdown giorno
    for (var i = 1; i <= 31; i++) {
        $(".days").append("<option value='" + ("0" + i).slice(-2) + "'>" + ("0" + i).slice(-2) + "</option>");
    }

    // mesi nelle dropdown mese
    //for(var i=1; i<=12; i++)
    //{
    //$(".months").append("<option value='" + ("0" + i).slice(-2) + "'>" + ("0" + i).slice(-2) + "</option>");
    //}
    //new mese

    var currentDate = new Date();

    var i18nroot = "common.monthsOfYear.";
    var months = ["months", CQ.I18n.get(i18nroot + "january"), CQ.I18n.get(i18nroot + "february"), CQ.I18n.get(i18nroot + "march"), CQ.I18n.get(i18nroot + "april"), CQ.I18n.get(i18nroot + "may"), CQ.I18n.get(i18nroot + "june"), CQ.I18n.get(i18nroot + "july"), CQ.I18n.get(i18nroot + "august"), CQ.I18n.get(i18nroot + "september"), CQ.I18n.get(i18nroot + "october"), CQ.I18n.get(i18nroot + "november"), CQ.I18n.get(i18nroot + "december")];

    var startMonth = 1;

    var iM;
    for (iM = startMonth; iM <= 12; iM++) {
        $(".months").append('<option value="' + ("0" + iM).slice(-2) + '">' + months[iM] + " " + '</option>');
    }


    //new mese


    var year = (new Date()).getFullYear();

    // anni nel dropdown scadenza
    for (var i = year - 10; i <= year + 20; i++) {
        $(".years-scad").append("<option value='" + i + "'>" + i + "</option>");
    }

    // anni nel dropdown nascita
    for (var i = year - 110; i <= year; i++) {
        $(".years-born").append("<option value='" + i + "'>" + i + "</option>");
    }

    // anni del dropdown emissione
    for(var i = year - 10; i <=year; i++) {
        $(".emission").append($("<option>", {
            text: i,
            value: i
        }));
    }

    // In base ai vari input seleziono i valori dei dropdown delle date
    $(".dateToSplit").each(function () {

        try {
            var value = $(this).val();
            //console.log($(this).attr('id') + "=" + $(this).val());
            if (value != '') {
                var dt = value.split("T")[0].split("-");
                var fName = $(this).attr('idx');
                $("#day-" + fName).val(dt[2]);
                $("#month-" + fName).val(dt[1]);
                $("#year-" + fName).val(dt[0]);
            }
        }
        catch (err) {
        }

    });

    $('#btProcediSubmit').bind('click', savePassengers);

    $('.btConfirm').bind('click', confirmPassenger);

    $('#btAddPassenger').bind('click', addPassenger);

    $('.passengerLinkEdit').bind('click', showHideWrapContent);

    $("#procediCotiPax").click(function () {
        enableLoaderCheckin();
        performSubmit('checkinselectedpassenger', '#form-passengers', savePassengersSuccess, savePassengersFail);
    });

    // controlli sulle varie dati per validazione
    $(".checkDate").change(function () {

        var idx = $(this).attr("idx");
        var type = $(this).attr("type");
        var day = $("#day-" + type + "-" + idx).val();
        var month = $("#month-" + type + "-" + idx).val();
        var year = $("#year-" + type + "-" + idx).val();
        if (month != '' && year != '') {
            var old = 0;
            var days = getNumberOfDays(year, parseInt(month) - 1);
            if (days > 0) {
                old = $("#day-" + type + "-" + idx).val();
                $("#day-" + type + "-" + idx + ' option').each(function () {
                    if ($(this).val() != '') {
                        $(this).remove();
                    }
                });
            }
            for (var i = 1; i <= days; i++) {
                var sel = i == old ? 'selected' : '';
                $("#day-" + type + "-" + idx).append("<option value='" + ("0" + i).slice(-2) + "' " + sel + ">" + ("0" + i).slice(-2) + "</option>");
            }
        }

    });

    $("select[name='frequentFlyer']").change(function () {

        var form = $(this).closest('form');
        form.find("input[name='codeFFchanged']").val($(this).val());
        //console.log("Change FF");

    });

    $("select[name='gender']").change(function (e) {
        var gender = $(this).find('option:selected').val();
        $('.passengerSex select').val(gender).trigger('change');
    });


    $("input[name='frequentFlyerNumber']").keyup(function () {
        var form = $(this).closest('form');
        form.find("input[name='numberFFchanged']").val($(this).val());
        //console.log("Change FF Number");
    });


    // Se al caricamento della pagina il passeggero è selezionato allora devo settare il campo hidden a true
    $(".chkPassenger").each(function () {

        var idx = $(this).attr("idx");

        var passengerType = $("#passenger_type_" + idx).val();
        var numDocsReq = $("#numDocumentsRequired-" + idx).val();

        //if(passengerType=="CHD"){
        if (numDocsReq > 0) {
            $(".passengerDataError" + idx).show();
            $(".passengerDataSuccess" + idx).addClass('hide');
            if ($(".check-required-0").val() != "") {

                $("#passengerData" + idx).addClass("passenger-wrap-completed");
            }
        } else {
            if ($(".check-required-0").val() != "") {

                $(".passengerDataError" + idx).hide();
                $(".passengerDataSuccess" + idx).removeClass('hide');
                $("#passengerData" + idx).addClass("passenger-wrap-completed");
            }
        }
        /*}else{
            if(hasDocsReq>0){
                $("#passengerDataError"+idx).show();
            }else{
                $("#passengerDataError"+idx).hide();
            }
        }*/


        var checkBox = $("#cb_" + idx);

        if (checkBox.is(':checked')) {
            $("#passenger_checked_" + idx).val("true");
            $("#passengerLinkEdit" + idx).removeClass('hide');
        } else {
            $("#passenger_checked_" + idx).val("");
            $("#passengerLinkEdit" + idx).addClass('hide');
        }
    });

    // setto il passeggero selezionato prima di fare il checkin
    $("input.chkPassenger").click(function () { //modifica per revert

        var idx = $(this).attr("idx");

        //var isChecked = $(this).hasClass("checked");
        var isChecked = $("#passenger_checked_" + idx).val();

        //console.log("isChecked = " + isChecked);

        if (isChecked) {
            $(this).removeClass("checked");
            $("#passengerLinkEdit" + idx).addClass('hide');
            // $("[idx="+idx+"]").removeClass("checked");
            $("#cb_" + idx).attr("checked", false);
            $("#passenger_checked_" + idx).val("");
            $('#form-passenger-' + idx).find('.passenger-wrap-content').slideUp();
        }
        else {
            $(this).addClass("checked");
            $("#passengerLinkEdit" + idx).removeClass('hide');
            // $("[idx="+idx+"]").addClass("checked");
            $("#cb_" + idx).attr("checked", true);
            $("#passenger_checked_" + idx).val("true");
        }

        verifyCheckin();
    });

    //check modifica dati passengero
    $('.passenger-wrap-content a.close').on('click', function () {

        var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

        if(!isSafari) $(this).closest('.passenger-wrap-content').slideUp();
        else $(this).closest('.passenger-wrap-content').css("display", "none");
        event.stopPropagation();
        /*
        $('.field-wrap.checked').removeClass('checked');
        setTimeout(function(){
            $('.chkPassenger').trigger('click');
        }, 1000);*/
    });
    $('.interactive.passengerLinkEdit a.edit').on('click', function () {

        var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

        if(!isSafari) {
            $('html, body').animate({scrollTop: $(this).closest('.passenger-wrap').offset().top - 10}, 'slow');
            $(this).closest('.passenger-wrap-content').slideDown();
            var relatedContent = $(this).closest('.passenger-wrap').find('.passenger-wrap-content');
            if (relatedContent.length > 0 && relatedContent.is(':visible')) {
                relatedContent.slideUp();
            } else {
                relatedContent.slideDown();
            }
            event.stopPropagation();
        } else {
            //Bug 4459: Check-in Lista passeggeri - Browser Safari - I box dei pax non si aprono correttamente
            var idx = $(this).attr("idx");
            if(idx != null) {
                var el = $(".passenger-wrap-content:eq("+idx+")");
                el.css("display", (el.css("display") == "block" ? "none" : "block"));
            }
        }

        /*
        if ($('.field-wrap').hasClass('checked')){
            $('.field-wrap').removeClass('checked');
            setTimeout(function(){
                $('.chkPassenger').trigger('click');
            }, 1000);
        }
        */
    });

    verifyCheckin();
    $(".destinationUs").each(function () {
        var idx = $(this).attr("idx");
        usaNazionalita = $("#nationalityUS-" + idx).val();
        issueCountry = $("#issueCountry-" + idx).val();
        checkUsaResidenzaNazionalita(idx);
    });


    $("select[name=nationality_0]").on("change", function () {
        var passID = $(this).attr("id").split('-')[1];
        usaNazionalita = $(this).val();
        issueCountry = $("#issueCountry-" + passID).val();
        $("#nationalityUSInf-" + passID).val(usaNazionalita);
        checkUsaResidenzaNazionalita(passID);
    });

    $("select[name=issueCountry_0]").on("change", function () {
        var passID = $(this).attr("id").split('-')[1];
        usaNazionalita = $("#nationalityUS-" + passID).val();
        issueCountry = $(this).val();
        checkUsaResidenzaNazionalita(passID);
    });

    $("select[name=documentTypeAdult]").on("change", function () {
        var passID = $(this).attr("idx");
        checkIfEsta($(this).val(), passID, "documentTypeAdult-wrapper");
    });

    $("select[name=documentTypeInfant]").on("change", function () {
        var passID = $(this).attr("idx");
        checkIfEsta($(this).val(), passID, "documentTypeInfant-wrapper");
    });

    fixEstaBox();

    $.each($("[name*='year-born-'][type='born']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "born")
        });
    });

    $.each($("[name*='month-born-'][type='born']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "born")
        });
    });

    $.each($("[name*='day-born-'][type='born']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "born")
        });
    });

    $.each($("[name*='year-scad-'][type='scad']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad")
        });
    });

    $.each($("[name*='month-scad-'][type='scad']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad")
        });
    });

    $.each($("[name*='day-scad-'][type='scad']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad")
        });
    });

    $.each($("[name*='year-scad-extra-'][type='scad-extra']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad-extra")
        });
    });

    $.each($("[name*='month-scad-extra'][type='scad-extra']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad-extra")
        });
    });

    $.each($("[name*='day-scad-extra-'][type='scad-extra']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad-extra")
        });
    });

    $.each($("[name*='year-scad-greencard-'][type='scad-greencard']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad-greencard")
        });
    });

    $.each($("[name*='month-scad-greencard'][type='scad-greencard']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad-greencard")
        });
    });

    $.each($("[name*='day-scad-greencard-'][type='scad-greencard']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad-greencard")
        });
    });

    $.each($("[name*='year-scad-extra-'][type='emission']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "emission")
        });
    });

    $.each($("[name*='month-scad-extra'][type='emission']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "emission")
        });
    });

    $.each($("[name*='day-scad-extra'][type='emission']"), function (i, el) {
        $(el).change(function () {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "emission")
        });
    });


    getPaxType();

$(".countryList option[value=HK]").html(CQ.I18n.get("countryData.HK"));
$(".countryList option[value=MO]").html(CQ.I18n.get("countryData.MO"));
$(".countryList option[value=TW]").html(CQ.I18n.get("countryData.TW"));

    if($("[data-scroll='yes']").length == 1) {
        setTimeout(function() {
            $('html,body').animate({
                    scrollTop: $("[data-scroll='yes']").offset().top
            }, 'slow');
        }, 100);
    }
});

function replaceAccents(tag) {
//      var str = $(".passenger-wrap-content *> input[name=streetAddressDestination]").val();
      var str = $(tag).val();
      var defaultDiacriticsRemovalMap = [
        {'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
        {'base':'AA','letters':/[\uA732]/g},
        {'base':'AE','letters':/[\u00C6\u01FC\u01E2]/g},
        {'base':'AO','letters':/[\uA734]/g},
        {'base':'AU','letters':/[\uA736]/g},
        {'base':'AV','letters':/[\uA738\uA73A]/g},
        {'base':'AY','letters':/[\uA73C]/g},
        {'base':'B', 'letters':/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},
        {'base':'C', 'letters':/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},
        {'base':'D', 'letters':/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},
        {'base':'DZ','letters':/[\u01F1\u01C4]/g},
        {'base':'Dz','letters':/[\u01F2\u01C5]/g},
        {'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
        {'base':'F', 'letters':/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},
        {'base':'G', 'letters':/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},
        {'base':'H', 'letters':/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
        {'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
        {'base':'J', 'letters':/[\u004A\u24BF\uFF2A\u0134\u0248]/g},
        {'base':'K', 'letters':/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},
        {'base':'L', 'letters':/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},
        {'base':'LJ','letters':/[\u01C7]/g},
        {'base':'Lj','letters':/[\u01C8]/g},
        {'base':'M', 'letters':/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},
        {'base':'N', 'letters':/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},
        {'base':'NJ','letters':/[\u01CA]/g},
        {'base':'Nj','letters':/[\u01CB]/g},
        {'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
        {'base':'OI','letters':/[\u01A2]/g},
        {'base':'OO','letters':/[\uA74E]/g},
        {'base':'OU','letters':/[\u0222]/g},
        {'base':'P', 'letters':/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},
        {'base':'Q', 'letters':/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},
        {'base':'R', 'letters':/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},
        {'base':'S', 'letters':/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},
        {'base':'T', 'letters':/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},
        {'base':'TZ','letters':/[\uA728]/g},
        {'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
        {'base':'V', 'letters':/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},
        {'base':'VY','letters':/[\uA760]/g},
        {'base':'W', 'letters':/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
        {'base':'X', 'letters':/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},
        {'base':'Y', 'letters':/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},
        {'base':'Z', 'letters':/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},
        {'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
        {'base':'aa','letters':/[\uA733]/g},
        {'base':'ae','letters':/[\u00E6\u01FD\u01E3]/g},
        {'base':'ao','letters':/[\uA735]/g},
        {'base':'au','letters':/[\uA737]/g},
        {'base':'av','letters':/[\uA739\uA73B]/g},
        {'base':'ay','letters':/[\uA73D]/g},
        {'base':'b', 'letters':/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
        {'base':'c', 'letters':/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
        {'base':'d', 'letters':/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
        {'base':'dz','letters':/[\u01F3\u01C6]/g},
        {'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
        {'base':'f', 'letters':/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
        {'base':'g', 'letters':/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
        {'base':'h', 'letters':/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
        {'base':'hv','letters':/[\u0195]/g},
        {'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
        {'base':'j', 'letters':/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
        {'base':'k', 'letters':/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
        {'base':'l', 'letters':/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
        {'base':'lj','letters':/[\u01C9]/g},
        {'base':'m', 'letters':/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
        {'base':'n', 'letters':/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
        {'base':'nj','letters':/[\u01CC]/g},
        {'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
        {'base':'oi','letters':/[\u01A3]/g},
        {'base':'ou','letters':/[\u0223]/g},
        {'base':'oo','letters':/[\uA74F]/g},
        {'base':'p','letters':/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
        {'base':'q','letters':/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
        {'base':'r','letters':/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
        {'base':'s','letters':/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
        {'base':'t','letters':/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
        {'base':'tz','letters':/[\uA729]/g},
        {'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
        {'base':'v','letters':/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
        {'base':'vy','letters':/[\uA761]/g},
        {'base':'w','letters':/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
        {'base':'x','letters':/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
        {'base':'y','letters':/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
        {'base':'z','letters':/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}
      ];

      for(var i=0; i<defaultDiacriticsRemovalMap.length; i++) {
        str = str.replace(defaultDiacriticsRemovalMap[i].letters, defaultDiacriticsRemovalMap[i].base);
      }
      str = str.replace(/[-'`~!@#°§$%^&*()_|+=?;:'",.<>\{\}\[\]\\\/]/gi, ' ');

//      $(".passenger-wrap-content *> input[name=streetAddressDestination]").val(str);
      $(tag).val(str);
}

function clickCheckBox() {
    if (/^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {


    } else {

    }

}

var YEAR = new Date().getFullYear();
var MONTH = new Date().getMonth() + 1;
var DAY = new Date().getDate();

function getMonthText(month) {
    var i18nroot = "common.monthsOfYear.";
    var months = ["months", CQ.I18n.get(i18nroot + "january"), CQ.I18n.get(i18nroot + "february"), CQ.I18n.get(i18nroot + "march"), CQ.I18n.get(i18nroot + "april"), CQ.I18n.get(i18nroot + "may"), CQ.I18n.get(i18nroot + "june"), CQ.I18n.get(i18nroot + "july"), CQ.I18n.get(i18nroot + "august"), CQ.I18n.get(i18nroot + "september"), CQ.I18n.get(i18nroot + "october"), CQ.I18n.get(i18nroot + "november"), CQ.I18n.get(i18nroot + "december")];
    return months[month];
}

function setDropDown(start, end, sel, ctrl, initValue, initText, isMonth, setStart) {
    var curr;
    if (setStart) {
        curr = $(sel).val();
    }
    $(sel).html($("<option>", {
        value: initValue,
        text: initText
    }));
    for (var i = start; i <= end; i++) {
        var val = (ctrl && i <= 9 ? "0" + i : i);
        $(sel).append($("<option>", {
            value: val,
            text: (isMonth ? getMonthText(i) : val)
        }));
    }
    if (setStart && $(sel + " option[value='" + curr + "']").length > 0) $(sel).val(curr);
    //else $(sel).val(initValue);
}

function setExpiredDocs() {
    var types = ["scad", "scad-greencard", "scad-greencard-inf"];
    $.each(types, function (i, type) {
        $.each($("[name*='year-" + type + "-'][type='" + type + "']"), function (i, el) {
            var idx = $(el).attr("idx");
            setDropDown(YEAR, YEAR + 20, "[name='year-" + type + "-" + idx + "']", false, "", CQ.I18n.get("specialpage.myexperience.year.label"), false, true);
            if ($(el).val() > 0) {
                if ($(el).val() == YEAR) {
                    setDropDown(MONTH, 12, "[name='month-" + type + "-" + idx + "']", true, "", CQ.I18n.get("specialpage.myexperience.month.label"), true, true);
                    setDropDown(DAY, getDayFromMonth(MONTH), "[name='day-" + type + "-" + idx + "']", true, "", CQ.I18n.get("specialpage.myexperience.day.label"), false, true);
                } else {
                    setDropDown(1, 12, "[name='month-" + type + "-" + idx + "']", true, "", CQ.I18n.get("specialpage.myexperience.month.label"), true, true);
                    setDropDown(1, getDayFromMonth(1), "[name='day-" + type + "-" + idx + "']", true, "", CQ.I18n.get("specialpage.myexperience.day.label"), false, true);
                }
            } else {
                setDropDown(1, 12, "[name='month-" + type + "-" + idx + "']", true, "", CQ.I18n.get("specialpage.myexperience.month.label"), true, true);
                setDropDown(1, getDayFromMonth(1), "[name='day-" + type + "-" + idx + "']", true, "", CQ.I18n.get("specialpage.myexperience.day.label"), false, true);
                $("[name='month-" + type + "-" + idx + "']").val("");
                $("[name='day-" + type + "-" + idx + "']").val("");
            }
        });
    });
}

function updateDateSelect(index, type) {
    var day = "[name='day-" + (type == "emission" ? "scad-extra" : type) + "-" + index + "'][type='" + type + "']";
    var month = "[name='month-" + (type == "emission" ? "scad-extra" : type) + "-" + index + "'][type='" + type + "']";
    var year = "[name='year-" + (type == "emission" ? "scad-extra" : type) + "-" + index + "'][type='" + type + "']";
    if ($(year).val() == YEAR) {
        var mv = $(month).val();
        if (type == "born" || type == "emission") setDropDown(1, MONTH, month, true, "", CQ.I18n.get("specialpage.myexperience.month.label"), true);
        else if (type.substring(0, 4) == "scad") setDropDown(MONTH, 12, month, true, "", CQ.I18n.get("specialpage.myexperience.month.label"), true);
        if ($(month + " option[value='" + mv + "']").length > 0) $(month).val(mv);
        if ($(month).val() == MONTH) {
            var dv = $(day).val();
            if (type == "born" || type == "emission") setDropDown(1, DAY - 1, day, true, "", CQ.I18n.get("specialpage.myexperience.day.label"));
            else if (type.substring(0, 4) == "scad") setDropDown(DAY + 1, getDayFromMonth($(month).val()), day, true, "", CQ.I18n.get("specialpage.myexperience.day.label"));
            if ($(day + " option[value='" + dv + "']").length > 0) $(day).val(dv);
        } else {
            var dv = $(day).val();
            if (type == "born" || type == "emission") setDropDown(1, getDayFromMonth($(month).val(), $(year).val()), day, true, "", CQ.I18n.get("specialpage.myexperience.day.label"));
            else if (type.substring(0, 4) == "scad") setDropDown(1, getDayFromMonth($(month).val(), $(year).val()), day, true, "", CQ.I18n.get("specialpage.myexperience.day.label"));
            if ($(day + " option[value='" + dv + "']").length > 0) $(day).val(dv);
        }
    } else {
        var mv = $(month).val();
        setDropDown(1, 12, month, true, "", CQ.I18n.get("specialpage.myexperience.month.label"), true);
        if ($(month + " option[value='" + mv + "']").length > 0) $(month).val(mv);
        var dv = $(day).val();
        setDropDown(1, getDayFromMonth($(month).val(), $(year).val()), day, true, "", CQ.I18n.get("specialpage.myexperience.day.label"));
        if ($(day + " option[value='" + dv + "']").length > 0) $(day).val(dv);
    }
}

function getDayFromMonth(month, year) {
    try {
        switch (parseInt(month)) {
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                return (year != "" && parseInt(year) % 4 == 0 ? 29 : 28);
            default:
                return 31;
        }
    } catch (e) {
        return 31;
    }
}

function fixEstaBox() {
    $("select[name=documentTypeInfant]").each(function (i, obj) {
        var passID = $(this).attr("idx");
        checkIfEsta($(obj).val(), passID, "documentTypeInfant-wrapper");
    });

    $("select[name=documentTypeAdult]").each(function (i, obj) {
        var passID = $(this).attr("idx");
        checkIfEsta($(obj).val(), passID, "documentTypeAdult-wrapper");
    });
}

function checkUsaResidenzaNazionalita(passID) {
    if (passID != "") {
        if ((usaNazionalita == "" && issueCountry == "") || (usaNazionalita == "US" && issueCountry == "US")) {
            $("#form-passenger-" + passID + " .destination-wrapper").hide();
            $("#form-passenger-" + passID + " .destination-wrapper input").addClass('hide');
            $("#form-passenger-" + passID + " .destination-wrapper select").addClass('hide');

            $("#form-passenger-" + passID + " .residenza-wrapper").hide();
            $("#form-passenger-" + passID + " .residenza-wrapper input").addClass('hide');
            $("#form-passenger-" + passID + " .residenza-wrapper select").addClass('hide');

            $("#form-passenger-" + passID + " .otherDocuments-wrapper").hide();
            $("#form-passenger-" + passID + " .otherDocuments-wrapper input").addClass('hide');
            $("#form-passenger-" + passID + " .otherDocuments-wrapper select").addClass('hide');

        } else if (usaNazionalita != "" && issueCountry != "") {
            $("#form-passenger-" + passID + " .destination-wrapper").show();
            $("#form-passenger-" + passID + " .destination-wrapper input").removeClass('hide');
            $("#form-passenger-" + passID + " .destination-wrapper select").removeClass('hide');

            $("#form-passenger-" + passID + " .residenza-wrapper").show();
            $("#form-passenger-" + passID + " .residenza-wrapper input").removeClass('hide');
            $("#form-passenger-" + passID + " .residenza-wrapper select").removeClass('hide');

            $("#form-passenger-" + passID + " .otherDocuments-wrapper").show();
            $("#form-passenger-" + passID + " .otherDocuments-wrapper input").removeClass('hide');
            $("#form-passenger-" + passID + " .otherDocuments-wrapper select").removeClass('hide');
            fixEstaBox();
        } else {
            $("#form-passenger-" + passID + " .destination-wrapper").hide();
            $("#form-passenger-" + passID + " .destination-wrapper input").addClass('hide');
            $("#form-passenger-" + passID + " .destination-wrapper select").addClass('hide');

            $("#form-passenger-" + passID + " .residenza-wrapper").hide();
            $("#form-passenger-" + passID + " .residenza-wrapper input").addClass('hide');
            $("#form-passenger-" + passID + " .residenza-wrapper select").addClass('hide');

            $("#form-passenger-" + passID + " .otherDocuments-wrapper").hide();
            $("#form-passenger-" + passID + " .otherDocuments-wrapper input").addClass('hide');
            $("#form-passenger-" + passID + " .otherDocuments-wrapper select").addClass('hide');
        }
    } else {
        if ((usaNazionalita == "" && issueCountry == "") || (usaNazionalita == "US" && issueCountry == "US")) {
            $(".destination-wrapper").hide();
            $(".destination-wrapper input").addClass('hide');
            $(".destination-wrapper select").addClass('hide');

            $(".residenza-wrapper").hide();
            $(".residenza-wrapper input").addClass('hide');
            $(".residenza-wrapper select").addClass('hide');

            $(".otherDocuments-wrapper").hide();
            $(".otherDocuments-wrapper input").addClass('hide');
            $(".otherDocuments-wrapper select").addClass('hide');
        } else if (usaNazionalita != "" && issueCountry != "") {
            $(".destination-wrapper").show();
            $(".destination-wrapper input").removeClass('hide');
            $(".destination-wrapper select").removeClass('hide');

            $(".residenza-wrapper").show();
            $(".residenza-wrapper input").removeClass('hide');
            $(".residenza-wrapper select").removeClass('hide');

            $(".otherDocuments-wrapper").show();
            $(".otherDocuments-wrapper input").removeClass('hide');
            $(".otherDocuments-wrapper select").removeClass('hide');
            fixEstaBox();
        } else {
            $(".destination-wrapper").hide();
            $(".destination-wrapper input").addClass('hide');
            $(".destination-wrapper select").addClass('hide');

            $(".residenza-wrapper").hide();
            $(".residenza-wrapper input").addClass('hide');
            $(".residenza-wrapper select").addClass('hide');

            $(".otherDocuments-wrapper").hide();
            $(".otherDocuments-wrapper input").addClass('hide');
            $(".otherDocuments-wrapper select").addClass('hide');
        }
    }

}

function checkIfEsta(val, id, type) {
    if (id != "") {
        if (val == "esta") {
            showEsta(true, id, type);
            showVisto(false, id, type);
            showGreenCard(false, id, type);
        }
        if (val == "visto") {
            showVisto(true, id, type);
            showEsta(false, id, type);
            showGreenCard(false, id, type);
        }
        if (val == "green") {
            showGreenCard(true, id, type);
            showEsta(false, id, type);
            showVisto(false, id, type);
        }
    } else {
        if (val == "esta") {
            showEsta(true, "", type);
            showVisto(false, "", type);
            showGreenCard(false, "", type);
        }
        if (val == "visto") {
            showVisto(true, "", type);
            showEsta(false, "", type);
            showGreenCard(false, "", type);
        }
        if (val == "green") {
            showGreenCard(true, "", type);
            showEsta(false, "", type);
            showVisto(false, "", type);
        }
    }

}

function showEsta(val, id, type) {
    if (id != "") {
        if (val) {
            $("#form-passenger-" + id + " ." + type + " .estaChkbox-wrapper").show();
            $("#form-passenger-" + id + " ." + type + " .estaChkbox-wrapper input").removeClass('hide');
            $("#form-passenger-" + id + " ." + type + " .estaChkbox-wrapper select").removeClass('hide');
        } else {
            $("#form-passenger-" + id + " ." + type + " .estaChkbox-wrapper").hide();
            $("#form-passenger-" + id + " ." + type + " .estaChkbox-wrapper input").addClass('hide');
            $("#form-passenger-" + id + " ." + type + " .estaChkbox-wrapper select").addClass('hide');
        }
    } else {
        if (val) {
            $("." + type + " .estaChkbox-wrapper").show();
            $("." + type + " .estaChkbox-wrapper input").removeClass('hide');
            $("." + type + " .estaChkbox-wrapper select").removeClass('hide');
        } else {
            $("." + type + " .estaChkbox-wrapper").hide();
            $("." + type + " .estaChkbox-wrapper input").addClass('hide');
            $("." + type + " .estaChkbox-wrapper select").addClass('hide');
        }
    }
}

function showVisto(val, id, type) {
    if (id != "") {
        if (val) {
            $("#form-passenger-" + id + " ." + type + " .document-type-info-wrapper input").removeClass('hide');
            $("#form-passenger-" + id + " ." + type + " .document-type-info-wrapper select").removeClass('hide');
            $("#form-passenger-" + id + " ." + type + " .document-type-info-wrapper").show();
        } else {
            $("#form-passenger-" + id + " ." + type + " .document-type-info-wrapper input").addClass('hide');
            $("#form-passenger-" + id + " ." + type + " .document-type-info-wrapper select").addClass('hide');
            $("#form-passenger-" + id + " ." + type + " .document-type-info-wrapper").hide();
        }
    } else {
        if (val) {
            $("." + type + " .document-type-info-wrapper").show();
            $("." + type + " .document-type-info-wrapper input").removeClass('hide');
            $("." + type + " .document-type-info-wrapper select").removeClass('hide');
        } else {
            $("." + type + " .document-type-info-wrapper").hide();
            $("." + type + " .document-type-info-wrapper input").addClass('hide');
            $("." + type + " .document-type-info-wrapper select").addClass('hide');
        }
    }
}

function showGreenCard(val, id, type) {
    if (id != "") {
        if (val) {
            $("#form-passenger-" + id + " ." + type + " .document-type-info-wrapper-greenCard input").removeClass('hide');
            $("#form-passenger-" + id + " ." + type + " .document-type-info-wrapper-greenCard select").removeClass('hide');
            $("#form-passenger-" + id + " ." + type + " .document-type-info-wrapper-greenCard").show();
        } else {
            $("#form-passenger-" + id + " ." + type + " .document-type-info-wrapper-greenCard input").addClass('hide');
            $("#form-passenger-" + id + " ." + type + " .document-type-info-wrapper-greenCard select").addClass('hide');
            $("#form-passenger-" + id + " ." + type + " .document-type-info-wrapper-greenCard").hide();
        }
    } else {
        if (val) {
            $("." + type + " .document-type-info-wrapper-greenCard input").removeClass('hide');
            $("." + type + " .document-type-info-wrapper-greenCard select").removeClass('hide');
            $("." + type + " .document-type-info-wrapper-greenCard").show();
        } else {
            $("." + type + " .document-type-info-wrapper-greenCard input").addClass('hide');
            $("." + type + " .document-type-info-wrapper-greenCard select").addClass('hide');
            $("." + type + " .document-type-info-wrapper-greenCard").hide();
        }
    }
}

function verifyCheckin() {
    var disabled = false;
    var numCheckedPass = 0;
    var numDocsReq;

    $("input.chkPassenger").each(function () {
        var idx = $(this).attr("idx");

        var isChecked = $("#passenger_checked_" + idx).val();

        if (isChecked) {
            numCheckedPass++;
            numDocsReq = $("#numDocumentsRequired-" + idx).val();
            if (numDocsReq > 0) {
                disabled = true;
            }
        }
    });

    if (numCheckedPass == 0) {
        disabled = true;
    }

    if (disabled) {
        $('#btProcediSubmit').prop('disabled', true);
    } else {
        $('#btProcediSubmit').prop('disabled', false);
    }

}

function savePassengers() {

    var cotiPaxNum = 0;
    var adultChecked = false;
    var childChecked = false;
    var childPresent = false;

    $("input.chkPassenger").each(function () {
        var idx = $(this).attr("idx");

        var isChecked = $("#passenger_checked_" + idx).val();
        var type = $("#passenger_type_" + idx).val();

        if (type == "CHD") {
            childPresent = true;
        }
        if (isChecked) {
            if (type == "ADT") {
                adultChecked = true;
            }
            if (type == "CHD") {
                childChecked = true;
            }
            var cotiPax = $("#passenger_coti_pax_" + idx).val();
            if (cotiPax == "1") {
                cotiPaxNum++;
            }
        }
    });

    if (childChecked == true && adultChecked == false) {
        $("#errorMessage").html(CQ.I18n.get('checkin.passengerList.onlyChild.label'));
        $("#lnkError").trigger("click");
    } else {
        if (cotiPaxNum > 0) {
            if (cotiPaxNum == 1) {
                $("#notifyMessage").html(CQ.I18n.get('checkin.passengerList.cotiPax.label'));
                $("#lnkNotify").trigger("click");
            } else {
                $("#notifyMessage").html(CQ.I18n.get('checkin.passengerList.cotiPaxMulti.label'));
                $("#lnkNotify").trigger("click");
            }
        } else {
            if (childPresent) {
                $("#notifyMessage").html(CQ.I18n.get('checkin.passengerList.withChild.label'));
                $("#lnkNotify").trigger("click");
            } else {
                enableLoaderCheckin();
                performSubmit('checkinselectedpassenger', '#form-passengers', savePassengersSuccess, savePassengersFail);
            }
        }
    }

}


function savePassengersSuccess(data) {

    if (data) {
        if (!data.isError) {
            window.location.href = data.successPage;
        } else {
            disableLoaderCheckin();
            $("#errorMessage").html(CQ.I18n.get("checkin.checkinFailedNotClear.label"));
            $("#lnkError").trigger("click");
            dataLayer[1].checkinError = data.errorMessage;
            if(typeof window["_satellite"] !== "undefined"){
            	_satellite.track("CIError");
            } else {
            	console.error("Cannot activate CIError. _satellite object not found");
            }
            dataLayer[1].checkinError = "";
        }
    }
    else {
        disableLoaderCheckin();
        $("#errorMessage").html(CQ.I18n.get("checkin.checkinFailedNotClear.label"));
        $("#lnkError").trigger("click");
    }
    $('#btProcediSubmit').bind('click', savePassengers);
}

function savePassengersFail(data) {
    disableLoaderCheckin();
    $('#btProcediSubmit').bind('click', savePassengers);
}

function addPassenger(e) {
    e.preventDefault();

    if (validationInputsPassExtra()) {
        resetErrors();
        $('#btAddPassenger').unbind('click', addPassenger);
        enableLoaderCheckin();
        performSubmit('checkinaddpassenger', '#add-passenger', addPassengerSuccess, addPassengerFail);
    }

}


function addPassengerSuccess(data) {
    if (data.errorMessage != undefined && !data.errorMessage.indexOf("Unexpected invalid form on submit request")) {
        var errorInputs = [];
        var clone = $("#lnkError").clone();

        var start = data.errorMessage.indexOf("{");
        var end = data.errorMessage.indexOf("}");
        var errorString = data.errorMessage.substr(start + 1, end - start - 1);

        var errorsArray = errorString.split(",");
        for (i = 0; i < errorsArray.length; i++) {
            errorsArray[i] = $.trim(errorsArray[i]);
            errorsArray[i] = errorsArray[i].substr(0, errorsArray[i].indexOf("="));

            errorInputs.push($('input#' + errorsArray[i]));
        }

        $("#lnkError").detach();   //non faccio aprire la modale

        disableLoaderCheckin();
        $('#btAddPassenger').bind('click', addPassenger);
        setErrorInput(errorInputs, 1);
        //return;
    }

    if (!data.isError) {
        updateDataLayer('paxAdd', 'true');
        window.location.href = data.successPage;
    }
    else {
        if (data.sabreStatusCode == "grp") {
            data.errorMessage = CQ.I18n.get('checkin.common.overPax.error');
        }

            disableLoaderCheckin();
            $("#errorMessage").html(CQ.I18n.get("checkin.checkinFailedNotClear.label"));
            $("#lnkError").trigger("click");

    }
    $('#btAddPassenger').bind('click', addPassenger);

     if ($('#errorMessage').val('Unexpected invalid form on submit request:'))
     {
    	$("body").append(clone); //ritorna la modale di errore
     }
}

function addPassengerFail() {
    disableLoaderCheckin();
    $('#divError').show();
    $('#labelCheckinError').text('Errore (servlet FAIL) ');
    $('#btAddPassenger').bind('click', addPassenger);
}

// check dati singolo passeggero
function confirmPassenger() {
    var idx = $(this).attr('idx');
     $.each($(".destination-wrapper input, .residenza-wrapper input"),function(k,v){
        replaceAccents(v);
    });
    isExtra = false;
    if ($(this).data("extra") == "1")
        isExtra = true;

    var ok = true;

    $("#passengerData" + idx).removeClass("passenger-wrap-completed");
    $("#passengerLinkEdit" + idx).hide();
    $(".passengerDataError" + idx).hide();

    $(".check-required-" + idx).each(function () {
        if ($(this).closest('.hide').length > 0 || $(this).hasClass('hide')) {
            return true;
        }
        if ($(this).is(':checkbox')) {
            $(this).parent().find('label').removeClass("is-invalid-checkbox");
            if ($(this).prop('checked') == false) {
                //console.log($(this).attr("name"));
                if($(this).attr("name") == "estaConfirm" && !(usaNazionalita == "US" && issueCountry == "US")) {
                    //console.log("skip esta validation since nationality is US");
                    $(this).parent().find('label').addClass('is-invalid-checkbox');
                    ok = false;
                }
            }
        } else {
            $(this).removeClass("is-invalid-input");
            if ($(this).val() == '') {
                //console.log($(this).attr("name"));
                $(this).addClass("is-invalid-input");
                ok = false;
            }
        }

        $(this).change(function () {
            if ($(this).closest('.hide').length > 0 || $(this).hasClass('hide')) {
                return false;
            }
            if ($(this).is(':checkbox')) {
                $(this).parent().find('label').removeClass("is-invalid-checkbox");
                if ($(this).prop('checked') == false) {
                    //console.log($(this).attr("name"));
                    $(this).parent().find('label').addClass('is-invalid-checkbox');
                    ok = false;
                }
            } else {
                $(this).removeClass("is-invalid-input");
                if ($(this).val() == '') {
                    //console.log($(this).attr("name"));
                    $(this).addClass("is-invalid-input");
                    ok = false;
                }
            }

        });

    });


    // Validazione del numero di passaporto (Alfanumerico e caratteri compresi tra 6 e 12)
    var reg = /^[0-9a-zA-Z]+$/;
    var form = $("#passengerData" + idx);
    var passport = form.find("[name^='number_']");
    var type = form.find("[name^='type_']");
    if (passport.length > 0) {
        if ((!reg.test(passport.val()) || passport.val().length < 6 || passport.val().length > 12) && (type.val() == "PASSPORT" || type.val() == "NATIONALID")) {
            $(passport).addClass("is-invalid-input");
            ok = false;
        }
    }


    $("#passengerLinkEdit" + idx).show();

    if (ok) {
        // $("[idx="+idx+"]").addClass("checked"); //commentato per revert
        // $("#cb_" + idx).attr("checked", true);
        // $("#passenger_checked_" + idx).val("true");
        // Sistemo le date prima di passarle

        if ($("#dateOfBirth-" + idx).length)
            $("#dateOfBirth-" + idx).val($("#year-born-" + idx).val() + "-" + $("#month-born-" + idx).val() + "-" + $("#day-born-" + idx).val() + "T00:00:00.000Z");

        if ($("#dateOfBirthInfant-" + idx).length)
            $("#dateOfBirthInfant-" + idx).val($("#year-born-inf-" + idx).val() + "-" + $("#month-born-inf-" + idx).val() + "-" + $("#day-born-inf-" + idx).val() + "T00:00:00.000Z");

        if ($("#expirationDate-" + idx).length)
            $("#expirationDate-" + idx).val($("#year-scad-" + idx).val() + "-" + $("#month-scad-" + idx).val() + "-" + $("#day-scad-" + idx).val() + "T00:00:00.000Z");

        if ($("#expirationDateInfant-" + idx).length)
            $("#expirationDateInfant-" + idx).val($("#year-scad-inf-" + idx).val() + "-" + $("#month-scad-inf-" + idx).val() + "-" + $("#day-scad-inf-" + idx).val() + "T00:00:00.000Z");

        if ($("#issueDateAdult-" + idx).length)
            $("#issueDateAdult-" + idx).val($("#year-scad-extra-" + idx).val() + "-" + $("#month-scad-extra-" + idx).val() + "-" + $("#day-scad-extra-" + idx).val() + "T00:00:00.000Z");

        if ($("#issueDateInfant-" + idx).length)
            $("#issueDateInfant-" + idx).val($("#year-scad-extra-inf-" + idx).val() + "-" + $("#month-scad-extra-inf-" + idx).val() + "-" + $("#day-scad-extra-inf-" + idx).val() + "T00:00:00.000Z");

        if ($("#expireDateGreenCardAdult-" + idx).length)
            $("#expireDateGreenCardAdult-" + idx).val($("#year-scad-greencard-" + idx).val() + "-" + $("#month-scad-greencard-" + idx).val() + "-" + $("#day-scad-greencard-" + idx).val() + "T00:00:00.000Z");

        if ($("#expireDateGreenCardInfant-" + idx).length)
            $("#expireDateGreenCardInfant-" + idx).val($("#year-scad-greencard-inf-" + idx).val() + "-" + $("#month-scad-greencard-inf-" + idx).val() + "-" + $("#day-scad-greencard-inf-" + idx).val() + "T00:00:00.000Z");


        idxX = idx;

        // Chiamata alla servlet per salvare i dati
        if (isExtra) {
            enableLoaderCheckin();
            performSubmit('checkinupdatepassengerextra', '#form-passenger-' + idx, saveConfirmSuccess, saveConfirmFail);
        } else {
            enableLoaderCheckin();
            performSubmit('checkinupdatepassenger', '#form-passenger-' + idx, saveConfirmSuccess, saveConfirmFail);
        }


        //$(this).parents("div.passenger-wrap-content").slideUp();
    } else {
        disableLoaderCheckin();
        $(".passengerDataError" + idx).show();
        $(".passengerDataSuccess" + idx).addClass('hide');
        $("#numDocumentsRequired-" + idx).val(1);

        var tot = $(this).parents(".passenger-wrap-content").find("[class*=is-invalid]").first().offset().top - 40;
        $('html, body').animate({scrollTop: tot}, 'slow');
    }
    verifyCheckin();

    $(".check-required-" + idx).change(function () {
        if ($(this).val() == '')
            $(this).removeClass("is-invalid-input");
    });

    // Scroll della pagina per centrare la lista passeggeri

    //var offset = $("#lista_pass").offset();
    //$('html, body').animate({scrollTop:(offset.top)}, 'slow');


}

function saveConfirmSuccess(data) {

    try {
        disableLoaderCheckin();
        // console.log(idxX);
        if (data.passengers && data.passengers[0].result == 'OK') {
            $("#numDocumentsRequired-" + idxX).val(0);
            $(".passengerDataSuccess" + idxX).removeClass('hide');
            $( "input[name='frequentFlyerNumber']" ).removeClass('is-invalid-input')
            $("#passengerData" + idxX).addClass("passenger-wrap-completed");
            if (!$("#cb_" + idxX).hasClass("checked")) {
                $("#cb_" + idxX).trigger("click");
            }

            $('#form-passenger-' + idxX).find('.passenger-wrap-content').slideUp();
            var offset = $(".checkin__heading-wrapper").offset();
            $('html, body').animate({scrollTop: (offset.top)}, 'slow');
        } else {
            $("input[name='frequentFlyerNumber']").addClass('is-invalid-input');
            $(".passengerDataSuccess" + idx).addClass('hide');
        }

    }
    catch (err) {
        disableLoaderCheckin();
        //document.getElementById("demo").innerHTML = err.message;
    }
    verifyCheckin();
    // console.log(data);
}

function saveConfirmFail() {
    disableLoaderCheckin();
    verifyCheckin();
}


function getNumberOfDays(year, month) {
    var isLeap = ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0));
    return [31, (isLeap ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
}

function showHideWrapContent() {

    $("#cb_" + $(this).attr("idx")).trigger("click");

}

function validationInputsPassExtra() {
    var inputTicketNumber, inputName, inputLastname;
    // var regex1 = RegExp("[^0-9]$");
    var sixNumbers = /^[0-9]{6,10}$/;
    var thirteenNumbers = /^[0-9]{13}$/;
    var sixAlphanumeric = new RegExp("^[a-zA-Z0-9]{6}$");
    var fiveAlphanumeric = new RegExp("^[a-zA-Z0-9]{5}$");
    var noNumbers = /[^0-9]$/;
    var regexNomeCognome = /^[a-zA-zÀÁèéÈÉìíÌÍòóÒÓùúÙÚÁáÀàÂâÃãÄäĄąĆćÇçÈèÉéÊêËëĘęÌìÍíÎîÏïJ́j́ŁłŃńÑñŐőÔôÒòÓóÔôÖöÕõŚśÚúŰűÙùÛûÜüŸÿÝýŹźŻż\']+$/;

    var input1Val, input2Val, input3Val;
    var errorInputs = [];

    inputTicketNumber = $('#ticketNumber').val();
    inputFirstName = $('#firstName').val();
    inputLastname = $('#lastName').val();

    input1Val = (inputTicketNumber !== '' && thirteenNumbers.test(inputTicketNumber)) ? 1 : 0;

    //input2Val = ( inputName !== '' && noNumbers.test(inputFirstName)) ? 1 : 0;
    input2Val = (inputName !== '' && regexNomeCognome.test(inputFirstName)) ? 1 : 0;

    //input3Val = ( inputLastname !== '' && noNumbers.test(inputLastname)) ? 1 : 0;
    input3Val = (inputLastname !== '' && regexNomeCognome.test(inputLastname)) ? 1 : 0;
    if (input1Val && input2Val && input3Val) {
        return true;
    } else {
        if (!input1Val) {
            errorInputs.push($('input#ticketNumber'));
        }
        if (!input2Val) {
            errorInputs.push($('input#firstName'));
        }
        if (!input3Val) {
            errorInputs.push($('input#lastName'));
        }
    }
    setErrorInput(errorInputs, 0);
    return false;
}

function setErrorInput(inputArr, type) {
    resetErrors();
    var messageError = "";
    inputArr.forEach(function (inp) {
        $(inp).addClass('is-invalid-input');
        $(inp).prev().addClass('is-invalid-input');
        $(inp).next().addClass('is-invalid-input');

        messageError = '<p>';
        switch ($(inp).attr('id')) {
            case 'ticketNumber':
                var $currInp = $(inp);
                if ($currInp.val() !== "") {
                    messageError = messageError + CQ.I18n.get('preparaViaggio.pnrTicketNumber.error.invalid') + ". ";
                } else {
                    messageError = messageError + CQ.I18n.get('checkin.common.genericUncheckedField.label') + ". ";
                }
                break;
            case 'firstName':
                if (type > 0) {
                    messageError = messageError + CQ.I18n.get('specialpage.businessconnect.solo_caratteri_latini') + ". ";
                } else {
                    var $currInp = $(inp);
                    if ($currInp.val() !== "") {
                        messageError = messageError + CQ.I18n.get('preparaViaggio.nome.error.invalid') + ". ";
                    } else {
                        messageError = messageError + CQ.I18n.get('checkin.common.genericUncheckedField.label') + ". ";
                    }
                    if ($currInp == $('.data-er')) {
                        //alert('')
                    }
                }

                break;
            case 'lastName':
                if (type > 0) {
                    messageError = messageError + CQ.I18n.get('specialpage.businessconnect.solo_caratteri_latini') + ". ";
                } else {
                    var $currInp = $(inp);
                    if ($currInp.val() !== "") {
                        messageError = messageError + CQ.I18n.get('preparaViaggio.cognome.error.invalid') + ". ";
                    } else {
                        messageError = messageError + CQ.I18n.get('checkin.common.genericUncheckedField.label') + ". ";
                    }
                }
                break;
        }
        messageError += '</p>';
        $(".fb-" + $(inp).attr('id')).html(messageError);
    });


}

function resetErrors() {
    $('input').removeClass('is-invalid-input');
    $('input').prev().removeClass('is-invalid-input');
    $('input').next().removeClass('is-invalid-input');

    $("[class^='fb-']").html("");
}


function invokeGenericFormService(service, method, form, done, fail, always,
                                  selector, additionalParams) {
    var actualData;
    if ($.type(form) === "string" || !form) {
        if(form=='#'){
            form = '';
        }
        var serializedData = $(form).serialize();
        if (serializedData != "") {
            serializedData += "&";
        }
        serializedData += "_isAjax=true";
        if (additionalParams) {
            for (var paramName in additionalParams) {
                serializedData += "&" + paramName + "="
                    + encodeURIComponent(additionalParams[paramName]);
            }
        }
        actualData = serializedData;
    } else if ($.isPlainObject(form)) {
        actualData = {};
        if (form) {
            for (var fieldName in form) {
                actualData[fieldName] = form[fieldName];
            }
        }
        actualData['_isAjax'] = true;
        if (additionalParams) {
            for (var paramName in additionalParams) {
                actualData[paramName] = additionalParams[paramName];
            }
        }
    }
    return $.ajax({
        url: getServiceUrl(service, selector),
        method: method,
        data: actualData,
        context: document.body
    }).done(function (data) {
        if (done) {
            done(data);
        }
    }).fail(function () {
        if (fail) {
            fail();
        }
    }).always(function () {
        if (always) {
            always();
        }
    });
}

function performValidation(service, form, done, fail, always, selector) {
    var additionalParams = {
        '_action': 'validate'
    };
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector, additionalParams);
}

function performSubmit(service, form, done, fail, always, selector) {
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector);
}

function validation(e, service, form, done, fail, always, selector) {
    e.preventDefault();
    return performValidation(service, form, done, fail, always, selector);
}

function validationFormFile(e, service, form, done, fail, always, selector,
                            additionalParams) {
    e.preventDefault();
    additionalParams._action = "validate";
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector, additionalParams);
}

function getDropdownData(requestData, done, fail, always) {
    return invokeGenericFormService('staticdatalistservlet', 'GET', '#', done,
        fail, always, 'json', requestData);
}

function getServiceUrl(service, selector, secure) {
    if (selector == 'login') {
        return service;
    }
    selector = selector || "json";
    var protocol = location.protocol;
    if (jsonProtocol && (selector == "json" || secure)) {
        protocol = 'https:';
    }
    if (service == 'menu-millemiglia-header') {
        return protocol + '//' + location.host + internalUrl1 + "."
            + service + "." + selector;
    } else {
        return protocol + '//' + location.host + internalUrl1 + "/."
            + service + "." + selector;
    }
}

function removeUrlSelector() {
    var url = location.pathname;
    url = url.substr(0, location.pathname.indexOf('.'));
    return location.protocol + '//' + location.host + url;
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex
        .exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g,
        " "));
}

function showErrors(data, noanchor, checkboxnew) {
    removeErrors();
    for (var key in data.fields) {
        var selector = '[name="' + key + '"]';
        if ($(selector).css('display') == 'none' && $(selector).closest(".j-showCaptcha").lenght == 0) {
            selector = $(selector).next();
        }
        $(selector).attr("aria-invalid", "true");
        $(selector).attr("aria-describedby", key + '_error');
        var $contElement = $(selector).parent();
        if ($(selector).prop("tagName") == 'SELECT' ||
            ($(selector).attr("type") == 'checkbox' && checkboxnew)) {
            $contElement = $(selector).parent().parent();
        }
        var linkClass = "form__errorIcon";
        var msgClass = "form__alert form__errorField";
        if ($(selector).attr("type") === "checkbox") {
            linkClass = linkClass + " left";
            msgClass = msgClass + " left";
        }
        $contElement.addClass("isError");
        $($contElement).append(
            '<a class="' + linkClass +
            ($(selector).attr("type") == 'checkbox' && checkboxnew ? '" style="display:none' : '') +
            '"></a><div id="' + key + '_error'
            + '" class="' + msgClass + '">'
            + data.fields[key] + '</div>');
    }

    // $('[aria-invalid="true"]:first').focus();
    removePopups();

    if (!noanchor) {
        if ($('.isError').length > 0) {
            $(".isError").first().animateAnchor(-32, 400);
        }
    }
    return false;
}

function getPaxType() {

    var paxAdult = 0;
    var paxChild = 0;
    var paxInfant = 0;
    var paxYoung = 0;

    $('input[name="passengerType"]').each(function () {
        if ($(this).val() == 'ADT') {
            paxAdult++;
        }
        if ($(this).val() == 'CHD') {
            paxChild++;
        }
    });

    $('input[name="infantName"]').each(function () {
        if ($(this).val() != '') {
            paxInfant++;
        }
    });


    updateDataLayer('paxAdult', paxAdult);
    updateDataLayer('paxChild', paxChild);
    updateDataLayer('paxInfant', paxInfant);
    updateDataLayer('paxYoung', paxYoung);

}
