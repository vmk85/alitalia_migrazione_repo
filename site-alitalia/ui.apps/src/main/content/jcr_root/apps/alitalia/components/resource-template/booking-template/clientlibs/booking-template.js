function initBooking(pageName) {

	$(function(){
		
		var queryString = "";
		if (getParameterByName("error")) {
			queryString = "?error=" + getParameterByName("error");
		}
		
		$.ajax({
			url: removeUrlSelector() + "." + pageName + "-partial.html" + queryString,
			context : document.body
		})
		.done(function(data) {
			$(".booking.mod").html(data);
			initAccordions();
			initSlider();
			callAjaxLoad();
			bookingReady();
			initSeatMap();
			initForms();
			upsellingsManager.init();
			if ($("#ancillaryConfirm").length > 0) {
				$(".bookInfoBoxBasketBtn").attr("href", $("#ancillaryConfirm").attr("href"));
			}
			analytics_bookingPartialCallback();
			if(typeof insertTotalPrice == 'function'){
				insertTotalPrice();
			}
			if($('.j-responsiveTable').length>0){
			    $('.j-responsiveTable').responsiveTable({
                    maxWidth: 640
                });
			}

            if($('.j-responsiveTable.multiTratta').length>0){
                $('.j-responsiveTable.multiTratta').responsiveTable({
                    maxWidth: 800
                });
            }

			setTimeout(function(){
				$(window).trigger('resize');
			},1500);
			//Logica per aggiungere o rimuovere un contatto nella form dati passeggero
			var passengerData = $('div.bookingPassenger');
			if(passengerData.length != 0){
				var addExtraContactLimitCounter = ($(".j-addExtraContact").length, 0);
				$('.j-addExtraContactTrigger').unfastClick().fastClick(function(e){
					e.preventDefault();
					$(this).hide();
					$('.j-addExtraContact:eq('+addExtraContactLimitCounter+')').velocity("transition.slideDownIn");
					//addExtraContactLimitCounter++;
					$('.j-deleteExtraConctatTrigger').css('display', 'inline-block');
				});
				$('.j-deleteExtraConctatTrigger').unfastClick().fastClick(function(e){
					e.preventDefault();
					$(this).hide();
					$('.j-addExtraContact:eq('+addExtraContactLimitCounter+')').velocity("transition.slideUpOut");
					//addExtraContactLimitCounter--;
					$('.j-addExtraContactTrigger').css('display', 'inline-block');
				});
			}
			if ($(".bookingPayment").length > 0) {
				window.bookingHeaderStop=true;
				$("#paese").off('change',showCountriesUSA);
				$("#paese").on('change',showCountriesUSA);
			}
			/*fTest();*/
		})
		.fail(function() {});
	});
}
/*
function makeid() {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	for( var i=0; i < 6; i++ )
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

function fTestStep1() {

	console.log("Ready...");
	if (localStorage.getItem("fValue") != null && 
		localStorage.getItem("fValue") == "true") {
		$(".j-priceSelector .price").first().click();
		setTimeout(function() {
			console.log("-- 0 --");
			$(".j-goToReturn").first().click();
			setTimeout(function() {
				console.log("-- 1 --");
				window.location = "https://cvt.alitalia.com/content/alitalia/alitalia-it/it/booking/flight-select.bookingflightdataconsumer.json";
			}, 12000);
		}, 5000);
	}
}

function fTest() {

	console.log("Ready...");
	if (localStorage.getItem("fValue") != null && 
		localStorage.getItem("fValue") == "true") {

		// Step 2
		if (window.location.pathname.indexOf("passengers-data.html") > -1) {
			console.log("-- 2 --");
			$("#nomeAdulto_1").val(makeid());
			$("#cognomeAdulto_1").val(makeid());
			$("#email").val("test@alit000001.it");
			$("#valoreRecapito_1").val("123456");
			$(".checkbox.left label").click();
			$("#datiPasseggeroSubmit").click();
		}

		// Step 3
		if (window.location.pathname.indexOf("ancillary.html") > -1) {
			console.log("-- 3 --");
			window.location = "https://cvt.alitalia.com/content/alitalia/alitalia-it/it/booking/ancillary.confirmancillarydata.json";
		}

		// Step 4
		if (window.location.pathname.indexOf("payment.html") > -1) {
			console.log("-- 4 --");
			$("#card4-3-label").click();
			$("#numeroCarta").val("374251018720018");
			$("#meseScadenza").val("08");
			$("#annoScadenza").val("2018");
			$("#cvc").val("7373");
			$("#nome").val("test");
			$("#cognome").val("test");
			$("#indirizzo").val("Via del Corso");
			$("#cap").val("00186");
			$("#citta").val("Roma");
			$("#paese").val("IT");
			$("#booking-acquista-cdc-submit").click();
		}
	}
}

$(document).ready(function() {
	// Step 5
	if (localStorage.getItem("fValue") != null && 
		localStorage.getItem("fValue") == "true") {
		if (window.location.pathname.indexOf("confirmation.html") > -1) {
			window.close();
		}
	}
});*/