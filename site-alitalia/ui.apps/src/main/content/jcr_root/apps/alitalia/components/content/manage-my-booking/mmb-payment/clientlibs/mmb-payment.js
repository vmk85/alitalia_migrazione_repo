/**
 * Gestione della validazione e del submit della form di pagamento con carta di credito
 */
function initMmbPayment() {
	$('a.confirmPaymentButton').click(mmbAncillaryPayment);
}

function mmbAncillaryPayment(e) {
	e.preventDefault();
	performValidation('mmbpaymentconsumer', '#paymentForm', 
			mmbPaymentValidationSuccess, mmbHandleInvokeError);
}

function mmbPaymentValidationSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		if (data.result) {
			removeErrors();
			resultsLoading.init();
			performSubmit('mmbpaymentconsumer', '#paymentForm', 
					mmbPaymentSubmitSuccess, mmbHandleInvokeError);
		} else {
			showErrors(data);
			/*analytics track errors*/
			if(window["analytics_trackMmbErrors"] 
				&& typeof window["analytics_trackMmbErrors"] === "function"){
				window["analytics_trackMmbErrors"](data.fields);
			}
			else{
				console.error("function analytics_trackMmbErrors not found");
			}
		}
	}
}

function mmbPaymentSubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect + "?origin=" + mmbAnalytics_getOrigin());
		return;
	}
}

function mmbAnalytics_getOrigin(){
	var param = getURLParameter("origin");
	if(!param){
		param = "mmb"
	}
	return param;
}
