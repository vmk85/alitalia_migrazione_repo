(function() {
	resultsLoading.init();

	performSubmit('bookingmetasearch', '', 
		function(data) {
			window.location.replace(data.redirect);
		}, 
		function() {
			console.log('bookingmetasearch performSubmit FAIL');
		}
	);
})();
