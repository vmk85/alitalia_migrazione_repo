var cercaVoloCheckInOptions = {};

// document ready
$( document ).ready( function() {
    cercaVoloCheckInOptions.switchForm();
} );

cercaVoloCheckInOptions.switchForm = function() {
    $('#cerca-volo-checkin-wrapper input[type="radio"]').click(function () {
        if ($(this).hasClass('radio-booking_code')) {
            $('#cerca-volo-checkin-wrapper .booking_code').removeClass('hide');
            $('#cerca-volo-checkin-wrapper .millemiglia_profile, .millemiglia_code, #cerca-volo-checkin-wrapper .millemiglia_code, .cerca-volo__content--check-in .millemiglia_profile, #cerca-volo-checkin-wrapper .myalitalia_profile').addClass('hide');

        } else if ($(this).hasClass('radio-millemiglia_code')) {
            $('#cerca-volo-checkin-wrapper .millemiglia_code').removeClass('hide');
            $('#cerca-volo-checkin-wrapper .millemiglia_profile, .booking_code, #cerca-volo-checkin-wrapper .booking_code, .cerca-volo__content--check-in .millemiglia_profile, #cerca-volo-checkin-wrapper .myalitalia_profile').addClass('hide');

        } else if ($(this).hasClass('radio-millemiglia_profile')) {
            $('#cerca-volo-checkin-wrapper .millemiglia_profile').removeClass('hide');
            $('#cerca-volo-checkin-wrapper .millemiglia_code, .booking_code, #cerca-volo-checkin-wrapper .millemiglia_code, #cerca-volo-checkin-wrapper .booking_code').addClass('hide');

        } else if ($(this).hasClass('radio-myalitalia_profile')) {
            $('#cerca-volo-checkin-wrapper .myalitalia_profile').removeClass('hide');
            $('#cerca-volo-checkin-wrapper .millemiglia_code, .booking_code, #cerca-volo-checkin-wrapper .millemiglia_code, #cerca-volo-checkin-wrapper .booking_code').addClass('hide');

        } else {
            $('#cerca-volo-checkin-wrapper .millemiglia_code').removeClass('hide');
            $('#cerca-volo-checkin-wrapper .booking_code, #cerca-volo-checkin-wrapper .millemiglia_code').addClass('hide');
        }
    });
    $('.cerca-volo__content--check-in .open_booking_code').click(function () {
        if($(this).hasClass('tabMyFlights')){
            $("#myalitalia_myflights_searchcode").trigger("click");
        }else{
            $('.cerca-volo__content--check-in .booking_code').removeClass('hide');
            $('.cerca-volo__content--check-in .millemiglia_code, .cerca-volo__content--check-in .millemiglia_profile').addClass('hide');
            $('.cerca-volo__content--check-in .millemiglia_code, .cerca-volo__content--check-in .myalitalia_profile').addClass('hide');
            $('#millemiglia_profile').attr('checked',false);
            $('#millemiglia_profile').parent().removeClass('checked');
            $('#myalitalia_profile').attr('checked',false);
            $('#myalitalia_profile').parent().removeClass('checked');
            $('#booking_code').attr('checked',true);
            $('#booking_code').parent().addClass('checked');
        }

    });



    $('.cerca-volo__content--miei-voli input[type="radio"]').click(function () {
        if ($(this).hasClass('radio-myalitalia_profile_myflight')) {
            $('.cerca-volo__content--miei-voli .my_flight_search').removeClass('hide');
            $('.cerca-volo__content--miei-voli .flight_search_code').addClass('hide');
            $('input.radio-myalitalia_searchcode').attr('checked',false);
            $('input.radio-myalitalia_searchcode').parent().removeClass('checked');

        } else if ($(this).hasClass('radio-myalitalia_searchcode')) {
            $('.cerca-volo__content--miei-voli .flight_search_code').removeClass('hide');
            $('.cerca-volo__content--miei-voli .my_flight_search').addClass('hide');
            $('input.radio-myalitalia_profile_myflight').attr('checked',false);
            $('input.radio-myalitalia_profile_myflight').parent().removeClass('checked');
        }
    });
};
