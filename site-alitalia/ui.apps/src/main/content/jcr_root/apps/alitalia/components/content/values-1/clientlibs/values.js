var valuesOptions = {};

$(document).ready(function() {

	valuesOptions.fixedSlidePosition = false;

	valuesOptions.slider = new Swiper('.values__slider .swiper-container', {
		slidesPerView: 'auto',
		resistanceRatio: 0,
		pagination: {
			el: '.values__slider .swiper-pagination',
			clickable: true
		}
	});

	$('.values__slider .bg-image').foundation();

	valuesOptions.sliderPaginationVisibility();
	valuesOptions.equalizeSliderHeight();
	
});

var ww = $(window).width();
$(window).resize(function() {
	if(ww < 768 && !valuesOptions.fixedSlidePosition) {
		setTimeout(function() {
			valuesOptions.slider.slideTo(0, 0);
			valuesOptions.fixedSlidePosition = true;
		}, 1);
	} else if(ww >= 768) valuesOptions.fixedSlidePosition = false;

	setTimeout(function() {
		valuesOptions.equalizeSliderHeight();
	}, 100);

});


valuesOptions.equalizeSliderHeight = function() {
	$( '.values__slider .custom-card__section--text' ).removeAttr( 'style' );
	valuesOptions.cardMaxHeight = $( '.values__slider .swiper-wrapper' ).outerHeight() - $( '.values__slider .custom-card__section.bg-image' ).outerHeight();
	$( '.values__slider .custom-card__section--text' ).css( 'height', valuesOptions.cardMaxHeight );
};

valuesOptions.sliderPaginationVisibility = function() {
	if( $('.values__slider .swiper-slide').length <= 3 ){
		$('.values__slider .swiper-pagination').addClass('hide-for-large');
	}
};