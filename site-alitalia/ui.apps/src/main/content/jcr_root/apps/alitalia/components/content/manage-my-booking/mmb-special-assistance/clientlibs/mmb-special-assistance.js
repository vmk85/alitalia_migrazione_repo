var form;
var formId;

function initSpecialAssistance() {

	specialAssistanceTrigger();
	
	$('.special_assistance_submit').on('click', function(e) {
		e.preventDefault();
		specialAssistanceTrigger();
		window.formId = $(this).parent().children(".formId").val();
		window.form = "#special-assistance-form" + window.formId;
		console.log($(window.form));
		performValidation('specialassistanceservlet', window.form, 
				specialAssistanceValidationSuccess, specialAssistanceValidationError);
	});
}




function specialAssistanceValidationSuccess(data) {
	var feedbackSelector = '#feedbackMessage' + window.formId;
	console.log($(feedbackSelector));
	if (data.result) {
		performSubmit('specialassistanceservlet', window.form, 
				specialAssistanceSubmitSuccess, specialAssistanceSubmitError);
	} else {
		//showErrors(data);
		showFailMessage('Validation Error. Wrong data submitted');
		
	}
}

function specialAssistanceValidationError() {
	console.log('fail specialAssistanceValidationError');
	
}

function specialAssistanceSubmitSuccess(data) {
	var feedbackSelector = '#feedbackMessage' + window.formId;
	if (data.result == "OK") {
		showSuccessMessage();
		
	} else {
		showFailMessage('Service Error. Please try again');
	}
}

function specialAssistanceSubmitError() {
	console.log('fail specialAssistanceSubmitError');
}

function showSuccessMessage() {
	var feedbackSelector = '#feedbackMessage' + window.formId;
	$(feedbackSelector).addClass('success');
	$(feedbackSelector).find('span.booking__formFeedbackIcon').addClass('i-check_full');
	$(feedbackSelector).find('span.booking__formFeedbackText').text('Success. Your request has been processed, you will receive an e-mail with the details of your request');
	$(feedbackSelector).parent().addClass('isActive');
}

function showFailMessage(message) {
	var feedbackSelector = '#feedbackMessage' + window.formId;
	$(feedbackSelector).addClass('fail');
	$(feedbackSelector).children('.booking__formFeedbackIcon').addClass('i-alert');
	$(feedbackSelector).children('.booking__formFeedbackText').text(message);
	$(feedbackSelector).parent().addClass('isActive');
}

