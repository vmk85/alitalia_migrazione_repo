var cercaVoloPrenotaOptions = {};
var prenotaDataOptions=null;
var market = "";

// document ready
$( document ).ready( function() {

    initButtons();

    market = $('html').attr('lang').split('-')[1];

if(/ipad|iphone|ipod/i.test(navigator.userAgent.toLowerCase())){
    $('#data-andata--prenota-desk').attr('readonly','true');
    $('#data-ritorno--prenota-desk').attr('readonly','true');
    $('#departure_date--flight-status-desk').attr('readonly','true');
    $('#departure_date--timetables-desk').attr('readonly','true');
}




    prenotaDataOptions = {
        //dateFormat: "D d M yy",
        dateFormat: "dd/mm/yyyy",
        checkDisabledDays : true,
        startDate: null,
        endDate: null,
        from: $('input[data-calendar="prenotaDataFromOptions"]'),
        to: $('input[data-calendar="prenotaDataToOptions"]'),
        altFormat: (market != "us" && market != "ca") ? "dd/mm/yy" : "mm/dd/yy", // controllo fake per US - impostazione formato calendario
        altFieldFrom: $('input[data-calendar="prenotaDataFromOptions"]').data('ref'),
        altFieldTo: $('input[data-calendar="prenotaDataToOptions"]').data('ref'),
        curRange: null,
        curInput: null,
        y1: 0,
        y2: 0,
        curYFrom: 0,
        curYTo: 0,
        curY: 0,
        numberOfMonths: 2
    };
    setupCalendarPicker(prenotaDataOptions);


    var prenotaDataOptionsMobile = {
        dateFormat: "dd/mm/yyyy",
        checkDisabledDays : true,
        startDate: null,
        endDate: null,
        from: $('input[data-calendar="prenotaDataFromOptionsMobile"]'),
        to: $('input[data-calendar="prenotaDataToOptionsMobile"]'),
        altFormat: (market != "us" && market != "ca") ? "dd/mm/yy" : "mm/dd/yy", // controllo fake per US - impostazione formato calendario
        altFieldFrom: $('input[data-calendar="prenotaDataFromOptionsMobile"]').data('ref'),
        altFieldTo: $('input[data-calendar="prenotaDataToOptionsMobile"]').data('ref'),
        curRange: null,
        curInput: null,
        y1: 0,
        y2: 0,
        curYFrom: 0,
        curYTo: 0,
        curY: 0,
        numberOfMonths: 1
    };
    setupCalendarPicker(prenotaDataOptionsMobile);

    cercaVoloPrenotaOptions.switchElement();
    cercaVoloPrenotaOptions.passengerInputControls();


    // init collapsable panel
    $( '[data-toggle-panel]' ).on( 'click', function() {
        if( $( $( this ).data( 'toggle-panel' ) ).hasClass( 'open') ) {
            cercaVoloPrenotaOptions.closeAllCollapsablePanels();
            $( '.histogram-toggle-wrap' ).removeClass( 'hide' );
        } else {
            cercaVoloPrenotaOptions.openCollapsablePanel( $( this ).data( 'toggle-panel' ), $( this ) );
            $( '.histogram-toggle-wrap' ).addClass( 'hide' );
        }
    } );

    $( '[data-toggle-panel-mobile]' ).on( 'click', function() {
        if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
            if( $( $( this ).data( 'toggle-panel' ) ).hasClass( 'open') ) {
                cercaVoloPrenotaOptions.closeAllCollapsablePanels();
            } else {
                cercaVoloPrenotaOptions.openCollapsablePanel( $( this ).data( 'toggle-panel-mobile' ), $( this ) );
            }
        }
    } );

    // init validate button
    /*
    $( '[data-validate]' ).on( 'click', function( e ) {
        e.preventDefault();
        // cercaVoloPrenotaOptions.isValidResearch( '#testing' );
        if( cercaVoloPrenotaOptions.isValidResearch( e ) ) {
            if ($( this ).data( 'validate' ) == '#panel-flight-status') {
                // enable loader
                cercaVoloOptions.enableLoader();
                $.ajax( {
                    type: "GET",
                    url: '/content/cerca-volo/airportslistconsumer.json', // url servizio info voli per flight status, abbiamo inserito una URL fasulla per far aprire il pannello in demo
                    dataType: 'json',
                    success: function( data ) {
                        // recupero dati servizio e popolo il dom
                        console.log('test')
                    },
                    complete: function( data ) {
                        // callback che apre il pannello - set timeout finto di 3 secondi per mostrare il funzionamento del loader, al complete va rimosso con la funzione apposita
                        setTimeout(function(){
                            // disable loader
                            cercaVoloOptions.disableLoader();

                        cercaVoloPrenotaOptions.openCollapsablePanel( $('#panel-flight-status') );
                        }, 3000);
                    },
                    error: function() {
                        console.log('error')
                    }
                } );
            } else {
                cercaVoloPrenotaOptions.openCollapsablePanel( $( this ).data( 'validate' ) );
                $('#ui-datepicker-div').removeAttr('style').hide();
            }
        }
    } );*/

    // init validate button
    $( '[data-validate]' ).on( 'click', function( e ) {
        e.preventDefault();
        $('input').removeClass('is-invalid-input');
        $('input').next('span').removeClass('is-invalid-input');
        $('input').prev('span').removeClass('is-invalid-input');
        if ($(this).attr('id')=="validate_date"){
            if ($('section.section--cerca-volo').hasClass('on-focus')){
                if( cercaVoloPrenotaOptions.isValidResearch( e ) ) {
                    cercaVoloPrenotaOptions.closeAllCollapsablePanels();
                    cercaVoloPrenotaOptions.openCollapsablePanel( $( this ).data( 'validate' ) );
                    $('#ui-datepicker-div').removeAttr('style').hide();
                }
            }else{
                if ( cercaVoloOptions.desktopOverlayOpened == 0 ) {
                    cercaVoloOptions.openOverlayDesktop();
                }
            }
        } else{
            if( cercaVoloPrenotaOptions.isValidResearch( e ) ) {
                cercaVoloPrenotaOptions.closeAllCollapsablePanels();
                cercaVoloPrenotaOptions.openCollapsablePanel( $( this ).data( 'validate' ) );
                $('#ui-datepicker-div').removeAttr('style').hide();
            }
        }
    });

    // // open pop up fake span
    // $( 'span.input-placeholder' ).on( 'click', function() {
    // 	cercaVoloPrenotaOptions.openCollapsablePanel( $( this ).data( 'toggle-panel' ) );
    // } );

    // // close open panel on focus
    // $( '.cerca-volo__content--prenota input[data-ref]' ).on( 'focus', function() {
    // 	// se radio non svuotare
    // 	if( $( this ).attr( 'type' ) !== 'radio' && $( $( this ).data( 'ref' ) ).val() !== 'not-set' ) {
    // 		// clear input to prevent autocomplete to open
    // 		$( this ).val( '' );
    // 		// clear hidden input
    // 		$( $( this ).data( 'ref' ) ).val( 'not-set' );
    // 	}
    // 	// render this frontend input
    // 	cercaVoloOptions.renderFrontendThisInput( $( this ) );
    // 	// close all panels
    // 	cercaVoloPrenotaOptions.inputOnFocusCloseAllPanels();
    // } );

    $( '.cerca-volo__content--prenota input[data-ref]' ).on( 'focus', function() {
    		// se radio non svuotare
    //		if( $( this ).attr( 'type' ) !== 'radio' && $( $( this ).data( 'ref' ) ).val() !== 'not-set' ) {
    //			// clear input to prevent autocomplete to open
    //			$( this ).val( '' );
    //			// clear hidden input
    //			$( $( this ).data( 'ref' ) ).val( 'not-set' );
    //		}
        if ($( this ).data( 'ref' ) != '#roundTrip--prenota'){
            $(this).css('border','2px solid #006643');$(this).next('span.is-invalid-input').removeClass('is-invalid-input');$(this).removeClass('is-invalid-input');
        }
            
            


    		if( $( this ).data( 'ref' ) == '#departureDate--prenota' || $( this ).data( 'ref' ) == '#returnDate--prenota' ) {
    			isValidDatePickerOnFocus = 1;
    		} else {
    			isValidDatePickerOnFocus = 0;


    		}

    		$( this ).select();

    		// render this frontend input
    		cercaVoloOptions.renderFrontendThisInput( $( this ) );
    		// close all panels
    		cercaVoloPrenotaOptions.inputOnFocusCloseAllPanels();
    	} );

    // reverse departure / destination button
    $( '.reverse-button' ).on( 'click', function(){
        cercaVoloPrenotaOptions.controlReverseButton();
        // FIX 20180321: La funzione $('input[name="destination--prenota"]').attr('data-type') restituiva valore vuoto causando errore
        var origin = cercaVoloAutocomplete.getCityCode(cercaVoloPrenotaOptions.inputArrivalCode);// cercaVoloAutocomplete.getCityCode($('input[name="destination--prenota"]').attr('data-type'));
        var dest = cercaVoloAutocomplete.getCityCode(cercaVoloPrenotaOptions.inputDepartureCode); //cercaVoloAutocomplete.getCityCode($('input[name="list_andata--prenota"]').attr('data-type'));
        $('input[name="list_andata--prenota"]').attr('data-type',origin);
        $('input[name="destination--prenota"]').attr('data-type',dest);

        //evita di chiamare il servizio per la versione "mobile" e quando non sono definite (entrambe) origine e destinazione
        var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;

        if (!isMobile && origin!=undefined && dest!=undefined && origin.length==3 && dest.length==3){
            getOfferDetails(origin, dest);
        }

    } );

    /*$('input#luogo-arrivo--prenota-mobile').focusin(function(){
        setTimeout(function(){
            alert('ciao');
            if ($(this).val == '')
                $(".toggle-all-destination-mobile").removeClass("hide");

        }, 1000);
    });*/


    // open panel histogram
    // $( '[data-ref="#departureAirport--prenota"], [data-ref="#arrivalAirport--prenota"]' ).on( 'change', function(){
    // 	console.log($('[data-ref="#departureAirport--prenota"]').val());
    // 	console.log($('[data-ref="#arrivalAirport--prenota"]').val());
    // 	if( $('[data-ref="#departureAirport--prenota"]').val() !== '' && $('[data-ref="#arrivalAirport--prenota"]').val() !== '' )  {
    // 		cercaVoloPrenotaOptions.openCollapsablePanel( $('#panel-histogram') );
    // 	}
    // });

    //to focus input of origin and destination every time
    /*$('span[data-toggle-panel="#panel-list_andata--prenota"]').on('click',function(){
        $( "input#luogo-partenza--prenota-mobile.list_andata--prenota" ).focus();
    });
    $('span[data-toggle-panel="#panel-all-destination"]').on('click',function(){
        $( "input#luogo-arrivo--prenota-mobile.destination--prenota" ).focus();
    });*/


//to keep focus when clicking outside the panle prenota when inputs are focused
// var lstfocus=$("input")[0];
// console.log(lstfocus);
// $("body").click(function(e){
//     if ($(':focus').is("input")    ){
//
//         lstfocus= $(':focus');
//     }
//     //fix per evitare il focus sull ultimo input al click di qualsiasi bottone di ricerca nelle 4 tab
//     //e per evitare il focus sull ultimo input alla perdita dell'overlay del cerca volo
//     var input = '' + e.target.classList;
//     if(!input.includes('button') && $('div.overlay--desk').css('display')== "block" && e.target.classList != 'histogram__bar'){
//     lstfocus.focus();
//     }
//     if ($(':focus').is("input#luogo-arrivo--prenota-desk")){
//     		$('button.button.toggle-all-destination-desk').removeClass('hide');
//     		setTimeout(function(){
//     			console.log("$('#panel-all-destination').css('display') == block", $('#panel-all-destination').css('display') == "block");
//     			if ($('#panel-all-destination').css('display') == "block"){
//     				$('button.button.toggle-all-destination-desk').addClass('hide');
//     			}else{
//     				if ($('#luogo-arrivo--prenota-desk').val() == ""){
//     					$('button.button.toggle-all-destination-desk').removeClass('hide');
//     				}
//
//     			}
//
//     		},2);
//     	}
// });

    // Commentato...a cosa serve? boh
    /*
	$(document).on('click',function(e){
		if (!(/Mobi/.test(navigator.userAgent))) {
			console.log("NOT MOBILE");

			if ($( '.section--cerca-volo.on-focus' ).length !== 0 ){
				if(e.target.classList != 'histogram__bar'){
				//e.preventDefault();
				//e.stopPropagation();
				}
                if(e.target.classList != 'radio-millemiglia_code' || e.target.classList != 'radio-booking_code' ){
                    console.log("E RADIO ");

                }
				if ($('input[data-ref="#arrivalAirport--prenota"]:focus').length){
					console.log("desktop e arrivo in focus");
					$('button.button.toggle-all-destination-desk').removeClass('hide');
					// $('input[data-ref="#arrivalAirport--prenota"]').focus();
					// $('input[data-ref="#arrivalAirport--prenota"]').blur(function (event) {
					//     setTimeout(function () { $('input[data-ref="#arrivalAirport--prenota"]').focus(); }, 20);
					// });
				}
				if ($('input[data-ref="#departureAirport--prenota"]:focus').length){
					console.log("desktop e partenza in focus");
					// $('input[data-ref="#departureAirport--prenota"]').focus();
					// $('input[data-ref="#arrivalAirport--prenota"]').blur();
					// $('input[data-ref="#arrivalAirport--prenota"]').blur(function (event) {
					//     setTimeout(function () { $('input[data-ref="#arrivalAirport--prenota"]').focus(); }, 20);
					// });
				}
			}else{
				console.log("desktop e on focus chiuso --> should prevent event");
			}
		}
	})*/

	// navigazione tastiera istogramma
    	// entrata istogramma
    	$( '#luogo-arrivo--prenota-desk' ).on( 'keydown', function(e) {
    		if(e.originalEvent.key == 'Tab')
    		{
    		    if ($( '#panel-histogram' ).is(':visible'))
    		    {
    			    e.preventDefault();
    			    $( '#panel-histogram .row--histogram .col:nth-child(2) a' ).focus();
    			}
    		}
    		if(e.originalEvent.key == 'Enter')
            {
                // if ( $( '#panel-histogram' ).hasClass('open'))
                setTimeout(function(){
                    if (!$( '#panel-histogram' ).is(':visible'))
                        $( '#data-andata--prenota-desk' ).focus();
                }, 500);

            }
    	} );
    	// uscita istogramma
    	$( '#panel-histogram .cta-close-panel' ).on( 'keydown', function(e) {
    		if(e.originalEvent.key == 'Tab' || e.originalEvent.key == 'Enter') {
    			e.preventDefault();
    			$( '#data-andata--prenota-desk' ).focus();
    		}
    	} );

    	$( '#luogo-partenza--timetables-desk' ).on( 'keydown', function(e) {
            if(e.originalEvent.key == 'Enter')
            {
                $( '#luogo-arrivo--timetables-desk' ).focus();
            }
        } );

        $( '#luogo-arrivo--timetables-desk' ).on( 'keydown', function(e) {
            if(e.originalEvent.key == 'Enter')
            {
                $( '#departure_date--timetables-desk' ).focus();
            }
        } );

    $('#luogo-arrivo--prenota-desk').on('blur', function(e) {
        if ($('#labelErrorInfoStatoVolo').html()!=""){
         cercaVoloPrenotaOptions.isValidResearch( e,"arrival" );
        }
    });
    $('.cerca-volo__title').on('click', function() {

        if( ww >= 1024 )
            cercaVoloPrenotaOptions.closeAllCollapsablePanels();

        var b=$(this).closest("div").not('.is-active');
        var c=b.find("li").not('.is-active');
        c.find("span").removeClass('is-invalid-input');

        var $myDiv=$('div#labelErrorInfoStatoVolo');
        $myDiv.html("");
        $myDiv.css('display','none');
        resetErrors();
        // for(k=0;k<myDiv.length;k++){
        //     var item=myDiv[k];
        //     var s=item.closest('li')

        //     $('label#labelCheckinError')["0"].innerText="";
        //     $('input#code').removeClass('is-invalid-input');
        //     $('input#name').removeClass('is-invalid-input');
        //     $('input#surname').removeClass('is-invalid-input');

        //     h=item.attributes.css;

        //     $(item).css('style','display:none');
        //     item.innerHTML="";

        //     $('label#labelCheckinError')["0"].innerText="";
        // }
    });

// new media query
    $(window).on('changed.zf.mediaquery', function(event, name) {
        cercaVoloPrenotaOptions.switchElement();
        // close open panels
        if( $( '[id*="panel-"].open' ).length !== 0 ) {
            cercaVoloPrenotaOptions.closeAllCollapsablePanels();
        }
    });

    // evento change tab buca ricerca
    $( '[data-responsive-accordion-tabs]' ).on( 'change.zf.tabs', function( e ) {
        // close open panels
        if( $( '[id*="panel-"].open' ).length !== 0 ) {
            cercaVoloPrenotaOptions.closeAllCollapsablePanels();
        }
    } );

} );




cercaVoloPrenotaOptions.switchForm = function( $inputWrap ) {

    if( $inputWrap.find( 'input' ).attr( 'id' ) == 'switch-from--prenota-mobile' ) {
        // disable input & change val
        $('[id*="data-ritorno--prenota"]').attr('disabled', true).val( '' );
        // add class disable for disabling span
        $('[id*="data-ritorno--prenota"]').parent().find( '.input-placeholder' ).addClass( 'disabled' );
        // clear hidden form & render frontend
        $( '#returnDate--prenota' ).val( 'not-set' );
        cercaVoloOptions.renderFrontendThisInput( $( 'input[id*="data-ritorno--prenota"]' ) );
        // close all panels
        cercaVoloPrenotaOptions.closeAllCollapsablePanels();

    } else if( $inputWrap.find( 'input' ).attr( 'id' ) == 'switch-to--prenota-mobile'  ) {
        $('[id*="data-ritorno--prenota"]').attr( 'disabled', false);
        $('[id*="data-ritorno--prenota"]').parent().find( '.input-placeholder' ).removeClass( 'disabled' );
        cercaVoloPrenotaOptions.closeAllCollapsablePanels();
    }

};


cercaVoloPrenotaOptions.switchElement = function() {
    if( Foundation.MediaQuery.is( 'small only' ) ) {
        //HISTORY
        cercaVoloPrenotaOptions.history = $(".history").detach();
        $(".history-wrap--mobile").append(cercaVoloPrenotaOptions.history);
        //INPUT WRAP 1
        cercaVoloPrenotaOptions.inputwrap1 = $(".partenza-destinazione").detach();
        $(".input-wrap-1--mobile").append(cercaVoloPrenotaOptions.inputwrap1);
        //RADIO
        cercaVoloPrenotaOptions.radio = $(".radio-travel-type").detach();
        $(".radio-wrap--mobile").append(cercaVoloPrenotaOptions.radio);
        //INPUT WRAP 2
        cercaVoloPrenotaOptions.inputwrap2 = $(".date-viaggio").detach();
        $(".input-wrap-2--tablet").append(cercaVoloPrenotaOptions.inputwrap2);
        //LINK
        cercaVoloPrenotaOptions.link = $(".external-link").detach();
        $(".link-wrap--mobile").append(cercaVoloPrenotaOptions.link);

    } else if ( Foundation.MediaQuery.is( 'medium only' ) ) {
        //HISTORY
        cercaVoloPrenotaOptions.history = $(".history").detach();
        $(".history-wrap--mobile").append(cercaVoloPrenotaOptions.history);
        //INPUT WRAP 1
        cercaVoloPrenotaOptions.inputwrap1 = $(".partenza-destinazione").detach();
        $(".input-wrap-1--mobile").append(cercaVoloPrenotaOptions.inputwrap1);
        //RADIO
        cercaVoloPrenotaOptions.radio = $(".radio-travel-type").detach();
        $(".radio-wrap--mobile").append(cercaVoloPrenotaOptions.radio);
        //INPUT WRAP 2
        cercaVoloPrenotaOptions.inputwrap2 = $(".date-viaggio").detach();
        $(".input-wrap-2--tablet").append(cercaVoloPrenotaOptions.inputwrap2);
        //LINK
        cercaVoloPrenotaOptions.link = $(".external-link").detach();
        $(".link-wrap--tablet").append(cercaVoloPrenotaOptions.link);
    } else {
        //HISTORY
        cercaVoloPrenotaOptions.history = $(".history").detach();
        $(".history-wrap--desktop").append(cercaVoloPrenotaOptions.history);
        //INPUT WRAP 1
        cercaVoloPrenotaOptions.inputwrap1 = $(".partenza-destinazione").detach();
        $(".input-wrap-1--mobile").append(cercaVoloPrenotaOptions.inputwrap1);
        //RADIO
        cercaVoloPrenotaOptions.radio = $(".radio-travel-type").detach();
        $(".radio-wrap--desktop").append(cercaVoloPrenotaOptions.radio);
        //INPUT WRAP 2
        cercaVoloPrenotaOptions.inputwrap2 = $(".date-viaggio").detach();
        $(".input-wrap-2--desktop").append(cercaVoloPrenotaOptions.inputwrap2);
        //LINK
        cercaVoloPrenotaOptions.link = $(".external-link").detach();
        $(".link-wrap--desktop").append(cercaVoloPrenotaOptions.link);
    }
};
cercaVoloPrenotaOptions.resetErrors = function(){
    $('input').removeClass('is-invalid-input');
    $('input').prev().removeClass('is-invalid-input');
    $('input').next().removeClass('is-invalid-input');
    $('#labelCheckinError').css('display','none');
    $('.labelErrorInfoStatoVolo').html("");
    $('#labelErrorInfoStatoVolo').html("");
    $('#labelErrorInfoStatoVolo').css('display','none');
    $('.labelErrorInfoStatoVolo').css('display','none');
    $('#labelErrorInfoOrarioVolo').css('display','none');


}
cercaVoloPrenotaOptions.setErrorInput = function(inputArr,type){
    if (type == "arrival"){

    }else{
        cercaVoloPrenotaOptions.resetErrors();    
    }
    
    var messageError = "";
	inputArr.forEach(function(inp){
		if ($(inp).attr('data-ref') =='#returnDate--prenota' || $(inp).attr('data-ref') =='#departureDate--prenota' || $(inp).attr('id')=="luogo-partenza--prenota-desk"){
			$(inp).addClass('is-invalid-input');
			$(inp).prev("span.input-placeholder").addClass('is-invalid-input');
		}else{

			$(inp).addClass('is-invalid-input');
			$(inp).next("span.input-placeholder").addClass('is-invalid-input');
		}
         switch ($(inp).attr('id')){
            case 'luogo-partenza--prenota-desk':
                messageError = messageError + CQ.I18n.get('preparaViaggio.aeroportoPartenza.error.empty') + " ";
                break;
            case 'luogo-arrivo--prenota-desk':
                messageError = messageError + CQ.I18n.get('preparaViaggio.aeroportoArrivo.error.empty') + " ";
                break;
            case 'data-andata--prenota-desk':
                messageError = messageError + CQ.I18n.get('preparaViaggio.dataPartenza.error.empty') + " ";
                break;
            case 'data-ritorno--prenota-desk':
                messageError = messageError + CQ.I18n.get('preparaViaggio.dataRitorno.error.empty')+ " ";
                break;
        }
	});
     if (messageError && !type){
        //errore unico
        messageError = CQ.I18n.get('input.dati.error');
        // console.log("messageError ",messageError);
        $('.labelErrorInfoStatoVolo').css('display','block');
        $('.labelErrorInfoStatoVolo').addClass('feedback-error');
        $('.labelErrorInfoStatoVolo').html(messageError);
        $('#labelErrorInfoStatoVolo').html(messageError);
        $('#labelErrorInfoStatoVolo').css('display','block');
        $('#labelErrorInfoOrarioVolo').html(messageError);
        $('#labelErrorInfoOrarioVolo').css('display','block');

    }
}

cercaVoloPrenotaOptions.isValidResearch = function(e,input) {
    if (input == "arrival"){

        // console.log("controllare solo arrivo");
        var errorInputs = [];
        cercaVoloPrenotaOptions.input2Val = ( $( '#arrivalAirport--prenota' ).val() !== '' && $( '#arrivalAirport--prenota' ).val() !== 'not-set' && $( '#arrivalAirport--prenota' ).val() !== 'Destinazione' ) ? 1 : 0;
        if (cercaVoloPrenotaOptions.input2Val){
            return true;
        }else{
            errorInputs.push($( 'input[data-ref="#arrivalAirport--prenota"]'));
            cercaVoloPrenotaOptions.setErrorInput(errorInputs, input);
        }
    }else{
        // e.preventDefault();
        // variabili 4 input da validare ( partenza, destinazione, data andata e ritorno )
        var errorInputs = [];
        cercaVoloPrenotaOptions.input1Val = ( $( '#departureAirport--prenota' ).val() !== '' && $( '#departureAirport--prenota' ).val() !== 'not-set' && $( '#departureAirport--prenota' ).val() !== 'Partenza' ) ? 1 : 0;
        cercaVoloPrenotaOptions.input2Val = ( $( '#arrivalAirport--prenota' ).val() !== '' && $( '#arrivalAirport--prenota' ).val() !== 'not-set' && $( '#arrivalAirport--prenota' ).val() !== 'Destinazione' ) ? 1 : 0;
        cercaVoloPrenotaOptions.input3Val = ( $( '#departureDate--prenota' ).val() !== '' && $( '#departureDate--prenota' ).val() !== 'not-set' ) ? 1 : 0;

        if ($( '.radio-travel-type #switch-from--prenota-mobile' ).parent().hasClass( 'checked' )){
            //solo andata
            cercaVoloPrenotaOptions.input4Val = 1;
        }else {
            //andata e ritorno
            if ($( '.radio-travel-type #switch-to--prenota-mobile' ).parent().hasClass( 'checked' )){
                cercaVoloPrenotaOptions.input4Val = ( $( '#returnDate--prenota' ).val() !== '' && $( '#returnDate--prenota' ).val() !== 'not-set' && $( '#returnDate--prenota' ).val() && $( '#switch-to--prenota-mobile' ).parent().hasClass( 'checked' ) ) ? 1 : 0;
            }
        }

        // se 4 campi validi apri il pannello opzioni di volo
        if( cercaVoloPrenotaOptions.input1Val && cercaVoloPrenotaOptions.input2Val && cercaVoloPrenotaOptions.input3Val && cercaVoloPrenotaOptions.input4Val ) {
            // cercaVoloPrenotaOptions.openCollapsablePanel( '#panel-travel-options' );
            cercaVoloPrenotaOptions.resetErrors();
            return true;
        }else{
            if (!cercaVoloPrenotaOptions.input1Val){
                errorInputs.push($( 'input[data-ref="#departureAirport--prenota"]' ));
            }
            if (!cercaVoloPrenotaOptions.input2Val){
                errorInputs.push($( 'input[data-ref="#arrivalAirport--prenota"]'));
            }
            if (!cercaVoloPrenotaOptions.input3Val){
                errorInputs.push($( 'input[data-ref="#departureDate--prenota"]' ));
            }
            if ($( '.radio-travel-type #switch-to--prenota-mobile' ).parent().hasClass( 'checked' )){
                //andata e ritorno
                if (!cercaVoloPrenotaOptions.input4Val){
                    errorInputs.push($( 'input[data-ref="#returnDate--prenota"]' ));
                }
            }
            cercaVoloPrenotaOptions.setErrorInput(errorInputs);
            return false;
        }
    }
    
};

cercaVoloPrenotaOptions.openCollapsablePanel = function( target, trigger ) {
    // se qualche panel aperto chiudi
    if( $( '[id*="panel-"].open' ).length !== 0 ) {
        cercaVoloPrenotaOptions.closeAllCollapsablePanels();
    }

    // color green history button
    if( typeof trigger !== undefined && $( trigger ).data( 'toggle-panel' ) == '#panel-history' ) {
        $( '.history [data-toggle-panel]' ).addClass( 'active-toggle' );
    }

    // hide desktop validate button & history button
    if( target == '#panel-travel-options' && Foundation.MediaQuery.atLeast( 'large' ) ) {
        $( '.validate-search--desktop button' ).fadeOut(300);
        $( '.history .cta' ).hide();
    }

    // apri il pannello attuale
    if( Foundation.MediaQuery.atLeast( 'large' ) ) {
        // se overlay desk chiuso, aprilo
        if( $( '.section--cerca-volo.on-focus' ).length === 0 ) {
            cercaVoloOptions.openOverlayDesktop();

        }
            $('#luogo-arrivo--prenota-desk').removeClass('is-invalid-input');
            $('#span_luogo-arrivo--prenota-desk').removeClass('is-invalid-input');
        if (!$('input#luogo-partenza--prenota-desk').hasClass('is-invalid-input') && !$('input#data-andata--prenota-desk').hasClass('is-invalid-input') && !$('input#data-ritorno--prenota-desk').hasClass('is-invalid-input')){
            $('#labelErrorInfoStatoVolo').html('');
            $('#labelErrorInfoStatoVolo').hide();
            $('#luogo-arrivo--prenota-desk').removeClass('is-invalid-input');
            $('#span_luogo-arrivo--prenota-desk').removeClass('is-invalid-input');


        }
        $( target ).addClass( 'open' ).stop().slideDown(400, function() {
            // on desktop when finish animation to open panel; if panell == panel-all-destination
            if ( target == '#panel-all-destination' ) {
                // set focus to first letter
                $('.initials-wrap [data-letter="a"]').focus();
            }
        });
        $( target ).closest( 'section' ).addClass( 'open-panel' );
    } else {
        cercaVoloPrenotaOptions.currentScrollTop = $( window ).scrollTop();
        $( target ).removeAttr( 'style' ).addClass( 'open' ).show();
    }
    $( '.section--cerca-volo' ).addClass( 'open-panel' );
    if( !Foundation.MediaQuery.atLeast( 'large' ) )
    {
        var handleAutofocus = $( target ).data('autofocus');
        if ($( target ).find('.date-input').length)
        {
            setTimeout(function(){
                if( handleAutofocus ) {
                    $(target).find( 'input' ).focus();
                }
        	}, 100);
        }
        else
        {
            if( handleAutofocus ) {
                $(target).find( 'input' ).focus();
            }
        }
    }
    /*
        if($("#originCountry").val() == "Gran Bretagna" || $("#destCountry").val() == "Gran Bretagna") {
            $(".young").css("position", "relative");
            $(".young").css("top", "0");
        } else {
            $(".young").css("position", "absolute");
            $(".young").css("top", "-9999px");
        }
    */
    // set lock body
    setBodyLock();
};

cercaVoloPrenotaOptions.closeAllCollapsablePanels = function() {

    // remove lock body
    removeBodyLock();

    // show desktop validate button
    if( $( '#panel-travel-options.open' ).length /* && Foundation.MediaQuery.atLeast( 'large' ) */ ) {
        $( '.validate-search--desktop button' ).fadeIn(300);
        $( '.history .cta' ).show();
    }

    // classe per colorare verde cta ultime ricerche
    $( '[data-toggle-panel]' ).removeClass( 'active-toggle' );

    // close autocomplete
    $('.cerca-volo .suggestion-box').empty();

    // chiudi tutti pannelli
    if( Foundation.MediaQuery.atLeast( 'large' ) ) {
        $( '[id*="panel-"]' ).stop().slideUp(300, function() {
            $( this ).removeClass( 'open' );
        });

    } else {
        // mobile scolltop
        $( window ).scrollTop( cercaVoloPrenotaOptions.currentScrollTop );
        $( '[id*="panel-"]' ).removeClass( 'open' ).hide();
    }
    $( '.section--cerca-volo' ).removeClass( 'open-panel' );
    cercaVoloOptions.histogramCounter = 0;
};

cercaVoloPrenotaOptions.passengerInputControls = function() {
    //var fieldName, currentVal;
    /*
        $( '[data-quantity="plus"]' ).on( 'click', function( e ) {
            e.preventDefault();
            fieldName = $( this ).attr( 'data-field' );
            currentVal = parseInt( $( 'input[name=' + fieldName + ']' ).val() );
            if( !isNaN( currentVal ) ) {
                $( 'input[name=' + fieldName + ']' ).val( currentVal + 1 );
            } else {
                $( 'input[name=' + fieldName + ']' ).val( 0 );
            }
            $( this ).parent().find( 'input' ).trigger( 'change' );
        });

        $('[data-quantity="minus"]').click(function(e) {
            // alert( 'meno' )
            e.preventDefault();
            fieldName = $( this ).attr( 'data-field' );
            currentVal = parseInt( $( 'input[name=' + fieldName + ']' ).val() );
            if( !isNaN( currentVal ) && currentVal > 0 ) {
                $( 'input[name=' + fieldName + ']' ).val( currentVal - 1 );
            } else {
                $( 'input[name=' + fieldName + ']' ).val( 0 );
            }
            $( this ).parent().find( 'input' ).trigger( 'change' );
        });
    */
    var MAX_ADULTS = 7;
    var MAX_KIDS = 6;
    var MIN_ADULTS = 1;
    $( '[data-quantity="plus"]' ).on( 'click', function( e ) {
        e.preventDefault();
        var nadults = parseInt($("#adult-passenger--prenota-mobile").val());
        var nkids = parseInt($("#kids-passenger--prenota-mobile").val());
        var nbabies = parseInt($("#baby-passenger--prenota-mobile").val());
        var totAK = nadults + nkids;
        var check = false;
        var fieldName = $( this ).attr( 'data-field' );
        var currentVal = parseInt( $( 'input[name=' + fieldName + ']' ).val() );
        if(fieldName == "adult_quantity") {
            if(nadults < MAX_ADULTS && totAK < MAX_ADULTS)
                check = true;
        } else if(fieldName == "kids_quantity") {
            if(nkids < MAX_KIDS && totAK < MAX_ADULTS)
                check = true;
        } else if(fieldName == "baby_quantity") {
            if(nbabies < nadults)
                check = true;
        }
        if(check)
            $( 'input[name=' + fieldName + ']' ).val( currentVal + 1 );
        initButtons();
    });
    $('[data-quantity="minus"]').click(function(e) {
        e.preventDefault();
        var nadults = parseInt($("#adult-passenger--prenota-mobile").val());
        var nkids = parseInt($("#kids-passenger--prenota-mobile").val());
        var nbabies = parseInt($("#baby-passenger--prenota-mobile").val());
        var check = false;
        var fieldName = $( this ).attr( 'data-field' );
        var currentVal = parseInt( $( 'input[name=' + fieldName + ']' ).val() );
        if(fieldName == "adult_quantity") {
            if((nadults-1) >= nbabies && (nadults-1) >= MIN_ADULTS)
                check = true;
        } else if(fieldName == "kids_quantity") {
            if((nkids-1) >= 0)
                check = true;
        } else if(fieldName == "baby_quantity") {
            if((nbabies-1) >= 0)
                check = true;
        }
        if(check)
            $( 'input[name=' + fieldName + ']' ).val( currentVal - 1 );
        initButtons();
    });
};

function initButtons() {
    var MAX_ADULTS = 7;
    var MAX_KIDS = 6;
    var MIN_ADULTS = 1;
    var nadults = parseInt($("#adult-passenger--prenota-mobile").val());
    var nkids = parseInt($("#kids-passenger--prenota-mobile").val());
    var nbabies = parseInt($("#baby-passenger--prenota-mobile").val());
    var totAK = nadults + nkids;
    if(nadults == MAX_ADULTS || totAK == MAX_ADULTS) {
        $("#addAdults").addClass("no-active");
        if(nadults == nbabies){
            $("#subAdults").addClass("no-active");
        }
        else if(nadults > 1){
            $("#subAdults").removeClass("no-active");
            $('#subAdults').removeAttr('disabled');
        }
    } else {
        $("#addAdults").removeClass("no-active");
        $('#addAdults').removeAttr('disabled');
        if(nadults == 1 || nadults == nbabies)
            $("#subAdults").addClass("no-active");
        else
            $("#subAdults").removeClass("no-active");
            $('#subAdults').removeAttr('disabled');
    }
    if(nbabies == 0){
        $("#subBabies").addClass("no-active");
    }else{
        $("#subBabies").removeClass("no-active");
        $('#subBabies').removeAttr('disabled');   
    }
    if(nkids == MAX_KIDS  || totAK == MAX_ADULTS) {
        $("#addKids").addClass("no-active");
        if(nkids > 0)
            $("#subKids").removeClass("no-active");
            $('#subKids').removeAttr('disabled');
    } else {
        $("#addKids").removeClass("no-active");
        $('#addKids').removeAttr('disabled');
        if(nkids == 0)
            $("#subKids").addClass("no-active");
        else{
            $("#subKids").removeClass("no-active");
            $('#subKids').removeAttr('disabled');
        }
    }
    if(nbabies == nadults) {
        $("#addBabies").addClass("no-active");
        $("#subAdults").addClass("no-active");
        if(nbabies == 0){
            $("#subBabies").addClass("no-active");
        }
        else{
            $("#subBabies").removeClass("no-active");
            $('#subBabies').removeAttr('disabled');
        }
    } else {
        $("#addBabies").removeClass("no-active");
        $('#addBabies').removeAttr('disabled');
        
    }
}

cercaVoloPrenotaOptions.inputOnFocusCloseAllPanels = function() {
    if( Foundation.MediaQuery.atLeast( 'large' ) ) {
        cercaVoloPrenotaOptions.closeAllCollapsablePanels();
    }
};
cercaVoloPrenotaOptions.isSet = function(field){
    var labelDestination = CQ.I18n.get('cercaVolo.label.destinazione');
    var labelOrigin = CQ.I18n.get('cercaVolo.label.partenza');
    return !(field == "not-set" || field == "" || field == labelDestination || field == labelOrigin);
}
cercaVoloPrenotaOptions.controlReverseButton = function() {
    var destination = CQ.I18n.get('cercaVolo.label.destinazione');
    var origin = CQ.I18n.get('cercaVolo.label.partenza');

    cercaVoloPrenotaOptions.inputDeparture = $('#departureAirport--prenota').val();
    cercaVoloPrenotaOptions.inputDepartureCode = $('#departureAirportCode--prenota').val();
    cercaVoloPrenotaOptions.inputArrival = $('#arrivalAirport--prenota').val();
    cercaVoloPrenotaOptions.inputArrivalCode = $('#arrivalAirportCode--prenota').val();

    if (cercaVoloPrenotaOptions.isSet(cercaVoloPrenotaOptions.inputArrival) && cercaVoloPrenotaOptions.isSet(cercaVoloPrenotaOptions.inputDeparture)){
        var iataArrivo = cercaVoloPrenotaOptions.inputArrival.slice(-3);
        var iataPartenza = cercaVoloPrenotaOptions.inputDeparture.slice(-3);
        cercaVoloAutocomplete.reverseFieldsPrenota("",iataArrivo);

    }else if (cercaVoloPrenotaOptions.isSet(cercaVoloPrenotaOptions.inputArrival)){
        var iata = cercaVoloPrenotaOptions.inputArrival.slice(-3);
        cercaVoloAutocomplete.reverseFieldsPrenota(origin,iata);

    }else if  (cercaVoloPrenotaOptions.isSet(cercaVoloPrenotaOptions.inputDeparture)){
        var iata = cercaVoloPrenotaOptions.inputDeparture.slice(-3);
        cercaVoloAutocomplete.reverseFieldsPrenota(destination,iata);
    }
    if (cercaVoloPrenotaOptions.inputArrival == destination){
        $('#departureAirport--prenota').val(origin);
        $('#airCodeOrigin').val("");

    }else{
        $('#departureAirport--prenota').val( cercaVoloPrenotaOptions.inputArrival );

        $('#departureAirportCode--prenota').val( iataArrivo);
        $('#originCountry').val($('#destCountry').val());
        $('#airCodeOrigin').val(iataArrivo);

        $('#luogo-partenza--prenota-desk').val(cercaVoloPrenotaOptions.inputArrival);
        $('#luogo-partenza--prenota-desk').attr('value',cercaVoloPrenotaOptions.inputArrival);
    }
    if (cercaVoloPrenotaOptions.inputDeparture == origin){
        $('#arrivalAirport--prenota').val(destination);
        $('#airCodeDes').val("");
    }else{
        $('#arrivalAirport--prenota').val( cercaVoloPrenotaOptions.inputDeparture );
        $('#arrivalAirportCode--prenota').val( iataPartenza );
        $('#destCountry').val($('#originCountry').val());
        $('#airCodeDest').val(iataPartenza);
        $('#luogo-arrivo--prenota-desk').val(cercaVoloPrenotaOptions.inputDeparture);
        $('#luogo-arrivo--prenota-desk').attr('value',cercaVoloPrenotaOptions.inputDeparture);
    }


	// MOBILE reverse button change airport value
	$('input[name="list_andata--prenota"], input[name="destination--prenota"]').each( function(i, el) {
		$( el ).parent().find( '.input-placeholder' ).html( $($( el ).data( 'ref' )).val().slice(0, -4) + '<strong class="iata-code">' + $($( el ).data( 'ref' )).val().slice(-4, $($( el ).data( 'ref' )).val().length ) + '</strong>' );
		$( el ).val( $($( el ).data( 'ref' )).val() );
	} );

    cercaVoloOptions.renderFrontendAllInputs();
    cercaVoloAutocomplete.setIataColor(origin);
    cercaVoloAutocomplete.setIataColor(destination);

};

