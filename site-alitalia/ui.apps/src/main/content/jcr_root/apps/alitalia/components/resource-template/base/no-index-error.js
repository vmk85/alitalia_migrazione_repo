"use strict";
use(function () {

    var path = this.path;
    var MetaNoIndexAllowError = false;

    if(path.indexOf('failure') > -1 || path.indexOf('no-solutions') > -1){
        MetaNoIndexAllowError = true;
    }
    return MetaNoIndexAllowError;

});