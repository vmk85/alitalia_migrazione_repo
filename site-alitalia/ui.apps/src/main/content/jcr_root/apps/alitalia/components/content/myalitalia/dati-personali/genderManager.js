"use strict";
use(function(){
    var type = this.type;
    var root = "myalitalia.datiPersonali.option.gender.";
    if(type.toLowerCase() == "u") return root + "unknow";
    if(type.toLowerCase() == "m") return root + "maschio";
    if(type.toLowerCase() == "f") return root + "femmina";
})