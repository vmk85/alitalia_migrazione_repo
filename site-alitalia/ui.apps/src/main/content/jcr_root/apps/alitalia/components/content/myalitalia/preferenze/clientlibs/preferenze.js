var allAirports; 
var filteredAir;
var seats = [];
var isMM;
var actionPref = {};
$(document).ready(function() {
    // enableLoaderMyAlitalia();
    getDDData();

    // performSubmit("GetMyAlitaliaCRMDataToSession","",Login.Session.success.getCrmDataPreferenze,Login.Session.error.getCrmData);
    $("#preferenzeViaggioEdit").submit(function( e ) {
      e.preventDefault();
    });

    $(".cta--icon.cta--edit,.button-wrap.button-wrap-accordion").click(function(){
        $('html,body').animate({scrollTop: $(this).closest(".customize-row").offset().top -10 },'slow');

    });
});

function destroy() {
    location.href = internalUrl1 + ".html";
}

function getDDData() {
    var params = {
        'tipoPosto': defaultSelectedTipoPosto,
        'tipoPasto': defaultSelectedTipoPasto
    };
    getDropdownData(params, dropDownDataSuccess, dropdownDataFail);
}

function dropDownDataSuccess(data) {
    var pasti = data.tipoPasto;
    var posti = data.tipoPosto;

    $("#food").html("");
    $("#food").append($("<option>", {
        value: "",
        text: CQ.I18n.get("myalitalia.preferenze.tipoMenu.default")
    }));
    for (index in pasti) {
        $("#food").append($("<option>", {
            value: pasti[index].code,
            text: pasti[index].description
        }));
        seats
    }
    for (index in posti) {
        $(".radio-wrap:eq(" + index + ")").find("label").text(posti[index].description);
        $(".radio-wrap:eq(" + index + ")").find("input").val(index);
        seats.push(posti[index].code);
    }

    if (foodPref != "0"){
        createSelected("pasto", "#food option[value='" + convertFoodNumber(foodPref) + "']");
    }
    $("#food").val(convertFoodNumber(foodPref));
}

function dropdownDataFail() {
    
}

function getServiceUrl(service, selector, secure) {
	if (selector == 'login') {
		return service;
	}
	selector = selector || "json";
	var protocol = location.protocol;
	return protocol + '//' + location.host + internalUrl1 + "/."
		+ service + "." + selector;
}

// function initPreferencesMA(user) {
//
//     if(user.pasto) {
//         if(user.pasto != 0) {
//             createSelected("pasto", "#food option[value='" + convertFoodNumber(user.pasto) + "']");
//             $(".hide-if-pref-meal").hide();
//             $("#food").val(convertFoodNumber(user.pasto));
//         } else {
//             $("select#food").val(user.pasto);
//             $(".pasto").html("").addClass("hide");
//             $(".hide-if-pref-meal").show();
//         }
//     } else {
//         $("select#food").val(user.pasto);
//         $(".pasto").html("").addClass("hide");
//         $(".hide-if-pref-meal").show();
//     }
//     if(user.posto) {
//         var id;
//         var make = true;
//         switch(parseInt(user.posto)) {
//             case 0:
//                 id = "switch-seat-no-preference";
//                 make = false;
//                 break;
//             case 1:
//                 id = "switch-seat-finestrino";
//                 break;
//             case 2:
//                 id = "switch-seat-corridoio";
//                 break;
//             default:
//                 make = false;
//                 break;
//         }
//         $(".radio-wrap").removeClass("checked");
//         $("#" + id).parent().addClass("checked");
//         if(make) {
//             $(".hide-if-pref-seat").hide();
//             createSelected("posto", "label[for='" + id + "']");
//         } else {
//             $("#switch-seat-no-preference").parent().addClass("checked");
//             $(".posto").html("").addClass("hide");
//             $(".hide-if-pref-seat").show();
//         }
//     } else {
//         $("#switch-seat-no-preference").parent().addClass("checked");
//         $(".posto").html("").addClass("hide");
//         $(".hide-if-pref-seat").show();
//     }
//     /** non presente in crm */
//     if(user.aeroportoPreferito && user.aeroportoPreferito.length > 0) {
//         createSelected("airport", user.aeroportoPreferito);
//         $(".hide-if-pref-airport").hide();
//         $("#list_airports").val(user.aeroportoPreferito);
//     } else {
//         $(".airport").html("").addClass("hide");
//         $(".hide-if-pref-airport").show();
//     }
//     if(true) {
//         createSelected("family-sel", convertPrefFamily(user.viaggiInFamiglia));
//         $(".hide-if-pref-family").hide();
//         $(".family").find("input[value='" +  convertPrefFamily(user.viaggiInFamiglia) + "']").prop("checked", true);
//         $(".family").find("input[value='" +  convertPrefFamily(user.viaggiInFamiglia) + "']").next().click();
//     } else {
//         $(".hide-if-pref-family").show();
//         $(".family-sel").html("").addClass("hide");
//     }
//
//     disableLoaderMyAlitalia();
// }
//
// function initPreferencesMM() {
//     if(defaultSelectedTipoPasto) {
//         if(defaultSelectedTipoPasto != "QLSS") {
//             createSelected("pasto", "#food option[value='" + defaultSelectedTipoPasto + "']");
//             $(".hide-if-pref-meal").hide();
//             $("#food").val(defaultSelectedTipoPasto);
//         } else {
//             $("select#food").val(defaultSelectedTipoPasto);
//             $(".pasto").html("");
//             $(".hide-if-pref-meal").show();
//         }
//     } else {
//         $("select#food").val(defaultSelectedTipoPasto);
//         $(".pasto").html("");
//         $(".hide-if-pref-meal").show();
//     }
//
//      if(defaultSelectedTipoPosto) {
//         var id;
//         var make = true;
//
//         if(defaultSelectedTipoPosto=="QLSS"){
//             defaultSelectedTipoPosto = "0";
//         }
//         if(defaultSelectedTipoPosto=="NSSA"){
//             defaultSelectedTipoPosto = "1";
//         }
//         if(defaultSelectedTipoPosto=="NSSW"){
//             defaultSelectedTipoPosto = "2";
//         }
//
//         switch(parseInt(defaultSelectedTipoPosto)) {
//             case 0:
//                 id = "switch-seat-no-preference";
//                 make = false;
//                 break;
//             case 1:
//                 id = "switch-seat-finestrino";
//                 break;
//             case 2:
//                 id = "switch-seat-corridoio";
//                 break;
//             default:
//                 make = false;
//                 break;
//         }
//         $(".radio-wrap").removeClass("checked");
//         $("#" + id).parent().addClass("checked");
//         if(make) {
//             $(".hide-if-pref-seat").hide();
//             createSelected("posto", "label[for='" + id + "']");
//         } else {
//             $("#switch-seat-no-preference").parent().addClass("checked");
//             $(".posto").html("");
//             $(".hide-if-pref-seat").show();
//         }
//     } else {
//         $("#switch-seat-no-preference").parent().addClass("checked");
//         $(".posto").html("");
//         $(".hide-if-pref-seat").show();
//     }
//
//     if(flightPrefFF!="") {
//         createSelected("ff", flightPrefFF);
//         $(".hide-if-pref-ff").hide();
//     } else {
//         $("#frequentFlyerCompaniesSelected-wrapper input").prop("checked", false);
//         $(".ff").html("");
//         $(".hide-if-pref-ff").show();
//     }
//
// }

function createSelected(type, sel) {
    var str = "";
    if(type=="airport"){
        str += '<div class="column large-8 medium-8 small-8 box-confirm">';
    }else{
        str += '<div class="column large-9 medium-9 small-9 box-confirm">';
    }
    str += '  <div class="confirm-label">' + getValue(type, sel) + '</div>';
    str += '</div>'
    if(type=="airport"){
        str += '<div class="box-cta column large-4 medium-4 small-4" style="width:33.33333%;">';
    }else{
        str += '<div class="box-cta column large-2 medium-3 small-2">';
    }
    str += '  <div class="cta-wrap cta-wrap-accordion">';
    if(type=="airport"){
        str += '    <a class="cta cta--primary cta--icon cta--edit" style="margin-left:10px;">';
    }else{
        str += '    <a class="cta cta--primary cta--icon cta--edit">';
    }
    str += '      <span>' + CQ.I18n.get('myalitalia.preferenzeViaggio.salvato.modifica') + '</span>';
    str += '    </a>';
    if(type=="airport"){
        str += '    <a class="cta cta--primary cta--icon cta--edit delete-preference" onclick="deleteAirPortPref()">';
        str += '      <span>' + CQ.I18n.get('myalitalia.datiViaggio.riepologo.cancella') + '</span>';
        str += '    </a>';
    }
    str += '  </div>';
    str += '</div>';
    if ($("." + type).hasClass("hide")){
        $("." + type).removeClass("hide");
    }
    $("." + type).html(str);
}

function getValue(type, val) {
    switch(type) {
        case "posto":
        case "pasto":
            return $(val).text();
        case "ff":
            return val + " " + (val==1 ? CQ.I18n.get('myalitalia.preferenzeViaggio.salvato.selezionato') : CQ.I18n.get('myalitalia.preferenzeViaggio.salvato.selezionati'));
        case "airport": case "family-sel":
            return val;
        default:
            return "";
    }
}

function deleteAirPortPref(){
    enableLoaderMyAlitalia();
    var CRMData = {
        data : "",
        type : "preferenzeAeroporto"
    };
    actionPref.init = function () {
        $(".box-cta.column.large-3.small-12.hide-if-pref-airport").removeClass("hide");
        $(".row-cta.airport").html("");
    };
    performSubmit("SetMyAlitaliaCrmDataPreferenze",CRMData,saveAirportPrefSuccess,saveAirportPrefError);

}

function savePrefSeatSuccess(response) {
    if (!response.isError){
        actionPref.init();
        disableLoaderMyAlitalia();
    } else {
        disableLoaderMyAlitalia();
    }
}

function savePrefSeatError() {
    disableLoaderMyAlitalia();
}

function savePrefSeat() {
    enableLoaderMyAlitalia();
	var seat = $("input[name='tipoPosto']:checked").val();
	    if(seat == "QLSS"){
	        seat = 0;
            actionPref.init = function () {
                $(".box-cta.column.large-3.small-12.hide-if-pref-seat").removeClass("hide");
                $(".row-cta.posto").html("");
            }
	    }
	    if(seat == "NSSA"){
	        seat = 1;
            actionPref.init = function () {
                $(".box-cta.column.large-3.small-12.hide-if-pref-seat").addClass("hide");
                createSelected("posto", "label[for='switch-seat-finestrino']");
            }
	    }
	    if(seat == "NSSW"){
	        seat = 2;
            actionPref.init = function () {
                $(".box-cta.column.large-3.small-12.hide-if-pref-seat").addClass("hide");
                createSelected("posto", "label[for='switch-seat-corridoio']");
            }
	    }

	    var CRMData = {
	        data : seat,
            type : "posto"
	    };

    performSubmit("SetMyAlitaliaCrmDataPreferenze",CRMData,savePrefSeatSuccess,savePrefSeatError);

}

function setCRMDataPreferenze(CRMData) {
    enableLoaderMyAlitalia();
    performSubmit("SetMyAlitaliaCrmDataPreferenze",CRMData,Login.Session.success.setCrmDataPreferenze,Login.Session.error.getCrmData);
}

// function preferenzeViaggioEditSuccess(data) {
//     // console.log(data);
// 	return false;
// }
//
// function preferenzeViaggioEditError(data) {
//     // console.log(data);
// 	return false;
// }

function saveFoodPrefSuccess(response) {
    if(!response.isError){
        actionPref.init();
        disableLoaderMyAlitalia();
    } else {
        disableLoaderMyAlitalia();
    }
}

function saveFoodPrefError() {
    disableLoaderMyAlitalia();
}

function saveFoodPref() {
    enableLoaderMyAlitalia();
	var food = $("#food").val() || null;
    foodPref = food;

	food = convertFoodCode(food);

    var CRMData = {
        data : food,
        type : "pasto"
    };

    if (foodPref != null ){
        actionPref.init = function () {
            createSelected("pasto", "#food option[value='" + foodPref + "']");
            $(".box-cta.column.large-3.small-12.hide-if-pref-meal").addClass("hide");
        };
    } else {
        actionPref.init = function () {
            $(".row-cta.pasto").html("");
            $(".box-cta.column.large-3.small-12.hide-if-pref-meal").removeClass("hide");
        };
    }

    performSubmit("SetMyAlitaliaCrmDataPreferenze",CRMData,saveFoodPrefSuccess,saveFoodPrefError);

}

function saveFFPrefSuccess(response) {

        actionPref.init();
        disableLoaderMyAlitalia();
}

function saveFFPrefError() {
    disableLoaderMyAlitalia();
}

function saveFFPref() {

    enableLoaderMyAlitalia();
    var ffpref = [];
    $.each($("[class*='programma-']:checked"), function(i, el) {
        ffpref.push($(el).val());
    });

    var CRMData = {
        gigya : JSON.stringify({flightPref_FF : ffpref})
    };
    if (ffpref.length > 0){
        actionPref.init = function () {
            $(".box-cta.column.large-3.small-12.hide-if-pref-ff").addClass("hide");
            createSelected("ff", ffpref.length);
        };
    } else {
        actionPref.init = function () {
            $(".row-cta.ff").html("");
            $(".box-cta.column.large-3.small-12.hide-if-pref-ff").removeClass("hide");
        };
    }
    performSubmit("SetMyAlitaliaCrmDataPreferenze",CRMData,saveFFPrefSuccess,saveFFPrefError);
}

function saveAirportPrefSuccess(response) {
    if (!response.isError){
        actionPref.init();
        disableLoaderMyAlitalia();
    } else {
        disableLoaderMyAlitalia();
    }
}

function saveAirportPrefError() {
    disableLoaderMyAlitalia();
}

function saveAirportPref() {
    enableLoaderMyAlitalia();
    var myAirport = $("#preferenza-aeroporto").val();

    var CRMData = {
        data : myAirport,
        type : "preferenzeAeroporto"
    };
    actionPref.init = function () {
        $("#list_airports").val(myAirport);
        createSelected("airport", myAirport);
        $(".box-cta.column.large-3.small-12.hide-if-pref-airport").addClass("hide");
    };

    performSubmit("SetMyAlitaliaCrmDataPreferenze",CRMData,saveAirportPrefSuccess,saveAirportPrefError);
}

function saveFamilyPrefSuccess(response) {
    if (!response.isError){
        actionPref.init();
        disableLoaderMyAlitalia();
    } else {
        disableLoaderMyAlitalia();
    }
}

function saveFamilyPrefError() {
    disableLoaderMyAlitalia();
}

function saveFamilyPref() {
    enableLoaderMyAlitalia();
    var val;
    var val2;
    val = $("[name='switch--family']:checked").val();
    val2 = $("[name='switch--family']:checked").val();
    val = convertFamilyPref(val);
    var CRMData = {
        data : val,
        type : "viaggiInFamiglia"
    };
    actionPref.init = function () {
        createSelected("family-sel", val2);
        $(".hide-if-pref-family").hide();
        $(".family").find("input[value='" +  val2 + "']").prop("checked", true);
        $(".family").find("input[value='" +  val2+ "']").next().click();
    };

    performSubmit("SetMyAlitaliaCrmDataPreferenze",CRMData,saveFamilyPrefSuccess,saveFamilyPrefError);
}

//function findAirport(query, results) {
//	var size = 7;
//	var airportResults = {};
//	airportResults.suggestions = [];
//	airportResults.keys = [];
//	var regExString = "^" + query + ".*";
//	jQuery.each(results, function(i, v) {
//		if (airportResults.keys.length < size
//				&& v.city.search(new RegExp(regExString, "i")) != -1) {
//			if (-1 === airportResults.keys.indexOf(v.code)) {
//				airportResults.keys.push(v.code);
//				var c = v;
//				airportResults.suggestions.push(copyValue(v));
//			}
//		}
//	});
//	jQuery.each(results, function(i, v) {
//		if (airportResults.keys.length < size
//				&& v.airport.search(new RegExp(regExString, "i")) != -1) {
//			if (-1 === airportResults.keys.indexOf(v.code)) {
//				airportResults.keys.push(v.code);
//				var c = v;
//				airportResults.suggestions.push(copyValue(v));
//			}
//		}
//	});
//	if (query.length == 3) {
//		jQuery.each(results, function(i, v) {
//			if (airportResults.keys.length < size
//					&& v.code.search(new RegExp(regExString, "i")) != -1) {
//				if (-1 === airportResults.keys.indexOf(v.code)) {
//					airportResults.keys.push(v.code);
//					airportResults.suggestions.push(copyValue(v));
//				}
//			}
//		});
//	}
//
//	return airportResults.suggestions;
//}
//
//function copyValue(v) {
//	var c = {
//        "value" : v.value,
//        "city" : v.city,
//        "country" : v.country,
//        "airport" : v.airport,
//        "type" : v.type,
//        "code" : v.code,
//        "best" : v.best,
//        "keyCityCode" : v.keyCityCode,
//        "keyCountryCode" : v.keyCountryCode,
//        "keyAirportName" : v.keyAirportName
//	};
//	return c;
//}
//
//function setInputValue(value) {
//    $("#list_airports").val(value);
//    $(".suggestion-box").empty();
//}

function getDropdownData(requestData, done, fail, always) {
	return invokeGenericFormService('staticdatalistservlet', 'GET', '', done,
			fail, always, 'json', requestData);
}

function convertFoodCode(code){

    switch (code){
        case null:
            code = 0;
            break;
        case "QLSS":
            code = 1;
            break;
        case "DBML":
            code = 2;
            break;
        case "KSML":
            code = 3;
            break;
        case "HNML":
            code = 4;
            break;
        case "LCML":
            code = 5;
            break;
        case "LFML":
            code = 6;
            break;
        case "LSML":
            code = 7;
            break;
        case "MOML":
            code = 8;
            break;
        case "SFML":
            code = 9;
            break;
        case "BBML":
            code = 10;
            break;
        case "GFML":
            code = 11;
            break;
        case "NLML":
            code = 12;
            break;
        case "VGML":
            code = 13;
            break;
        case "AVML":
            code = 14;
            break;
        case "VLML":
            code = 15;
            break;
    }

    return code;
}

function convertFoodNumber(code){

    code = parseInt(code);

    switch (code){
        case 0:
            code = null;
            break;
        case 1:
            code = "QLSS";
            break;
        case 2:
            code = "DBML";
            break;
        case 3:
            code = "KSML";
            break;
        case 4:
            code = "HNML";
            break;
        case 5:
            code = "LCML";
            break;
        case 6:
            code = "LFML";
            break;
        case 7:
            code = "LSML";
            break;
        case 8:
            code = "MOML";
            break;
        case 9:
            code = "SFML";
            break;
        case 10:
            code = "BBML";
            break;
        case 11:
            code = "GFML";
            break;
        case 12:
            code = "NLML";
            break;
        case 13:
            code = "VGML";
            break;
        case 14:
            code = "AVML";
            break;
        case 15:
            code = "VLML";
            break;
    }

    return code;
}

function convertFamilyPref(val){

    switch (val){

        case "non impostato":
            val = "false";
            break;
       case "si":
            val = "true";
            break;
       case "no":
            val = "false";
            break;

    }

    return val;

}

// function convertPrefFamily(val){
//
//     if (val){
//         val = "si";
//     } else if (!val) {
//         val = "no";
//     }
//
//     return val;
// }