// var onGigyaServiceReady = function () {
//
//     var type = linkFromEmail();
//     if(type == "reset" && !location.href.includes("resetpassword")){
//         gigya.accounts.showScreenSet({
//             screenSet: "MA-RegistrationLogin",
//             startScreen: "gigya-reset-password-screen",
//             containerID: "container",
//             onAfterSubmit: function(resp) {
//                 if(resp.response.errorCode == 0){
//                     $("#ma-reset-password-form").find(".close-button").trigger("click");
//                     $("a[data-open='success-reset']").click();
//                 }
//             },
//             onAfterScreenLoad: manageFormReset
//         });
//     }
//
// }

function manageFormReset() {

    $(document).on("keyup", ".password", managePasswordResetPopup);
    $(document).on("click", ".password", function () {
        var current = $(this);
        if (current.val().length < 4) $(".password").trigger("keyup");
    });

    function managePasswordResetPopup() {
        var current = $(this);
        var popup = $(".gigya-password-strength-bubble");
        if (current.val().length > 4) {
            popup.show();
            $(".password").removeClass("no-border");
            /*$("[data-bound-to='password']").removeClass("hide");*/
        }
        else {
            popup.hide();
            $(".password").addClass("no-border");
            /*$("[data-bound-to='password']").addClass("hide");*/
        }
    }



}

function removeRememberMA() {
    $("#userID").val(getCookie("emailresetsuccess"));
    setGigyaCookie("emailresetsuccess",$("#emailForReset").val(),-1);
}
