"use strict";
use(function() {
    var tier = this.tier;
    var response = {};

    if(tier == "") response.tier = "";
    if(tier == "FAL") response.tier = "Freccia Alata";
    if(tier == "FAP") response.tier = "Freccia Alata Plus";
    if(tier == "CRPR") response.tier = "CRPR";
    if(tier == "CRPP") response.tier = "CRPP";
    if(tier == "MIL" || tier == "AZ") response.tier = "Millemiglia";
    if(tier == "ULC") response.tier = "Ulisse";

    if(tier == "FAL" || tier == "FAP" || tier == "CRPP" || tier == "CRPR") {
        response.showLabel = true;
    } else {
        response.showLabel = false;
    }

    return response;
})