var regScreenSet = "MA-RegistrationLogin";
var profileScreenSet = "MA-ProfileUpdate";
var linkAccScreenSet = "Default-LinkAccounts";
/** Variabili per il salvataggio dei consensi nel CRM */
var flagAccettazioneCRM = [];
var flagAccettazioneCRMnewsletter = "";
var flagAccettazioneCRMprofiling = "";
var flagAccettazioneCRMprofilingOld = "";
var CanaleComunicazioni = [];
var canaleComunicazionePreferito = "";
/** */
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
}

if(mm<10) {
    mm = '0'+mm
}

today = yyyy + '-' + mm + '-' + dd ;

$(document).ready(function () {
    email = getEmailAddressFromQuerystring();
    if(email=='nomail' || email.regMail) {
        $(".crm").css("display","none");
        $(".ma").css("display","block");

    }else{
        $(".inner-row.crm").css("box-shadow","");
        $(".column.large-10.large-centered.banner-advantages.crm").css("-webkit-box-shadow","");
        $(".crm").css("display","block");
        $(".ma").css("display","none");
    }
    manageForm();
    var requestData = {"prefissiNazionali": ""};
    getDropDownData(requestData, dropDownDataSuccess, dropDownDataFail);

    performSubmit("setupemailtemplates", {suHttps: true}, updateEmailTemplatesSuccess, updateEmailTemplatesFail);

});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function restError() {
    $("#errorGigya p").text("");
    $("#errorMailGigya p").text("");
    $("#errorPassGigya p").text(CQ.I18n.get(""));
    $("#errorAuth1 p").text("");
    $("#errorAuth2 p").text("");
}

function validAuth() {
    return ($("#c").is(":checked") || $("#d").is(":checked")) && ($("#f").is(":checked") || $("#e").is(":checked"));
}

function validatePsw(psw) {

    var pointValid = 0;

    var M = /[A-Z]/;
    var m = /[a-z]/;
    var number = /[0-9]/;
    var special = /\W/;
    var space = /^\S+$/;

    if (psw.length >= 8 && space.test(psw)) {

        if(M.test(psw)){
            pointValid += 1;
        }
        if(m.test(psw)){
            pointValid += 1;
        }
        if(number.test(psw)){
            pointValid += 1;
        }
        if(special.test(psw)){
            pointValid += 1;
        }


    }

    return (pointValid >= 2 ? true:false);

}

function validateRegistrationCredential(){

    var validate = 0;
    if (validateEmail($("#email").val().trim())){
        validate += 1;
    } else {
        $("#errorMailGigya p").text(CQ.I18n.get("myalitalia.invalid.email"));
    }

    if (validatePsw($("#pass").val().trim())){
        validate += 1;
    } else{
        $("#errorPassGigya p").text(CQ.I18n.get("myalitalia.invalid.psw"));
    }

    if (validAuth()){
        validate += 1;
    } else {
        $("#errorAuth1 p").text(CQ.I18n.get("myalitalia.gigya.consensi"));
        $("#errorAuth2 p").text(CQ.I18n.get("myalitalia.gigya.consensi"));
    }

    return validate == 3 ? true : false;

}


function registrazioneMa() {

    restError();
    if(validateRegistrationCredential()){

        enableLoaderMyAlitalia();

        flagAccettazioneCRM = [];

        CanaleComunicazioni = [];

        flagAccettazioneCRMnewsletter = {
            tipoConsenso:"COM",
            flagConsenso:$("#c").is(":checked"),
            dataUltimoAggiornamento: today
        };
        flagAccettazioneCRMprofiling = {
            tipoConsenso:"PRN",
            flagConsenso:$("#e").is(":checked"),
            dataUltimoAggiornamento: today
        }
        flagAccettazioneCRMprofilingOld = {
            tipoConsenso:"PRO",
            flagConsenso:$("#e").is(":checked"),
            dataUltimoAggiornamento: today
        };
        canaleComunicazionePreferito = {
            tipo : "mail" ,
            selezionato : $("#c").is(":checked") ,
            dataUltimoAggiornamento : today
        };

        CanaleComunicazioni.push(canaleComunicazionePreferito);
        flagAccettazioneCRM.push(flagAccettazioneCRMnewsletter);
        flagAccettazioneCRM.push(flagAccettazioneCRMprofiling);
        flagAccettazioneCRM.push(flagAccettazioneCRMprofilingOld);
        var crmRegistration =
            {
                email: $("#email").val(),
                password: $("#pass").val(),
                crmData : JSON.stringify({
                    listaConsensi : flagAccettazioneCRM,
                    canaleComunicazionePreferito : CanaleComunicazioni
                }),
            };

        /** Invio i dati alla servlet per censire il nuovo utente */
        performSubmit("RegistrerMyAlitaliaCrmUser", crmRegistration,Login.Registration.success,Login.Registration.error);
    }

}

function updateEmailTemplatesSuccess(resp) {
    console.log(resp);
}

function updateEmailTemplatesFail(resp) {
    console.log(resp);
}

function getDropDownData(requestData, done, fail, always) {
    return invokeGenericFormService('staticdatalistservlet', 'GET', '', done,
        fail, always, 'json', requestData);
}

function dropDownDataSuccess(data) {
    var prefix = data.prefissiNazionali;
    $("select#prefix-list").html($("<option>", {text: "Scegli il prefisso..", value: ""}))
    for (var i in prefix) {
        $("select#prefix-list").append($("<option>", {
            value: prefix[i].prefix,
            text: prefix[i].description + " (+" + prefix[i].prefix + ")"
        }));
    }
}

function dropDownDataFail() {
}

// function doRegistration() {
//     gigya.accounts.initRegistration({
//         callback: function (resp) {
//             if (resp.errorCode != 0) {
//                 alert(resp.errorMessage + " (" + resp.errorCode + ")");
//             } else {
//                 console.log(resp);
//                 var now = new Date().toISOString();
//                 var token = resp.regToken;
//                 var params = {
//                     email: $("#email").val(),
//                     password: $("#pass").val(),
//                     regToken: token,
//                     profile: {
//                         firstName: $("#fistName").val(),
//                         lastName: $("#lastName").val(),
//                         gender: $("#gender").val()
//                     },
//                     data: {
//                         auth_Com: $("#a").is(":checked"),
//                         auth_Com_Update : now,
//                         auth_Data: true,
//                         auth_Data_Update: now,
//                         auth_Profile: $("#e").is(":checked"),
//                         auth_Profile_Update: now
//
//                     },
//                     finalizeRegistration: true,
//                     callback: function (regResp) {
//                         console.log(regResp);
//                         checkErrors(regResp);
//                     }
//                 };
//                 gigya.accounts.register(params);
//             }
//         }
//     });
// }
// Logs out currently logged in user
// function doLogout() {
//     gigya.accounts.logout({
//         forceProvidersLogout: false
//     });
// }

function checkErrors(regResp) {
    if (regResp.errorCode != 0) {
        var errorResp = regResp.errorCode + " - " + regResp.errorMessage + "<br>";
        if (regResp.validationErrors != null && regResp.validationErrors.length > 0) {
            var i = 0;
            errorResp += "<ul style='border: 1px solid white'>";
            while (i < regResp.validationErrors.length) {
                errorResp += "<li>" + regResp.validationErrors[i].fieldName + ": " + regResp.validationErrors[i].message + " (" + regResp.validationErrors[i].errorCode + ")</li>";
                i++;
            }
            errorResp += "</ul>";
        }
        $("#errorLog").html(errorResp);
        $("#errorLog").css("display", "block");
    } else {
        $("#errorLog").css("display", "none");
        console.log(regResp);
    }
}

// function doLogin() {
//     var user = $("#userID").val();
//     var pwd = $("#userPWD").val();
//     gigya.accounts.login({
//         loginID: user,
//         password: pwd,
//         callback: function (resp) {
//             checkErrors(resp);
//         }
//     });
// }

function getEmailAddressFromQuerystring() {
    var sPageURL = decodeURIComponent(window.location.search.substring(1));
    var sURLVariables  = sPageURL.split("&");
    var sParameterName;

    for (var i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === 'success') {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        } else if(sParameterName[0] == 'regMail'){
            return sParameterName[1] === undefined ? true : {
                regMail:sParameterName[1],
            };
        }
    }

    return "nomail";
}

function prefillEmail() {
    email = getEmailAddressFromQuerystring();
    if(email=='nomail') {
        $("#regMaNoNl").css("display","block");
    }else if (email.regMail){
        $("#regMaNoNl").css("display","block");
        $("#email").val(email.regMail);
        $(".gigya-input-text.emailcrm").val(email.regMail).css("border-color","#006643");
        email.auth_Com = getParamByQuerystring("auth_Com");
        email.auth_Profile = getParamByQuerystring("auth_Profile");
        if (email.auth_Com == "true"){
            $("#c").click();
        } else if (email.auth_Com == "false"){
            $("#d").click();
            $("#f").click();
        }
        if (email.auth_Profile == "true" && email.auth_Com != "false"){
            $("#e").click();
        } else if (email.auth_Profile == "false") {
            if (!$("#f").is(":checked")){
                $("#f").click();
            }
        }
    }else{
        $(".crm").css("display","block");
        $(".ma").css("display","none");
        $("#regMaNoNl").hide();
        // $(".gigya-input-text.emailcrm").val(email).css("border-color","#006643");
        //$(".gigya-input-text.emailcrm").prop('disabled', true);
        // $("#c").click();
        // $("#COMNL").hide();
    }
}

function manageForm() {

    prefillEmail();

    $(".gigya-input-text.email").prop('disabled', true);

    $(document).on("focusout", "[name='email']", function () {
        $("[data-bound-to='email']").removeClass("hide");
        $("[name='email']").removeClass("no-border");
    });

    $(document).on("focusin", "[name='email']", function () {
        $("[data-bound-to='email']").addClass("hide");
        $("[name='email']").addClass("no-border");
    });

    $(document).on("click", "[data-auth]", function (event) {
        var current = $(this).attr("data-auth");

        // if(current>3)$("[data-auth]:eq("+(current-1)+")").trigger("click");

        if (!($("#" + retriveIdByDataAuth(current)).is(":disabled"))) {
            if (current == 4) {
                enableCheck("e");
                enableCheck("f");
            } else if (current == 5) {
                enableCheck("d");
                enableCheck("f");
                enableCheck("c");
            } else if (current == 7) {
                disableCheck("e");
                enableCheck("f");
            } else if (current == 8) {
                enableCheck("c");
                enableCheck("d");
            }
        }
    });

    function disableCheck(id) {
        document.getElementById(id).disabled = true;
        $("label[for="+id+"]").css("color","#c1c1c1");
        if ($("#"+id).is(":checked")){
            $("#"+id).removeAttr('checked').parent(".radio-wrap").removeClass("checked");
        }
        var inputs_disabled = $("input#"+id).parent().parent().find("input:not(:disabled)");
        inputs_disabled.each(function(i, obj) {
            $(obj).attr('checked', true);
            $(obj).parent(".radio-wrap").addClass("checked");
        });
    }

    function enableCheck(id) {
        document.getElementById(id).disabled = false;
        $("label[for="+id+"]").css("color","black");

    }

    function retriveIdByDataAuth(dataAuth) {
        var id = "";
        switch (parseInt(dataAuth)) {
            case 3:
                id = "a";
                break;
            case 4:
                id = "c";
                break;
            case 5:
                id = "e";
                break;
            case 6:
                id = "b";
                break;
            case 7:
                id = "d";
                break;
            case 8:
                id = "f";
                break;
        }
        return id;
    }

}

function getParamByQuerystring(param) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1))
    var sURLVariables  = sPageURL.split("&");
    var sParameterName;
    for(var i = 0; sURLVariables.length; i++ ){
        if (sURLVariables[i].indexOf(param)!=-1){
            sParameterName = sURLVariables[i].split('=');
            return sParameterName[1] === undefined ? "" : sParameterName[1];
        }
    }
    return false;
}