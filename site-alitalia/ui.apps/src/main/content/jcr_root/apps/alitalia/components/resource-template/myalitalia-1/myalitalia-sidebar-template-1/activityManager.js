"use strict";
use(function() {
    var activity = this.activity;
    var response = "";
    if(activity == "Flight"){
        response = "millemiglia.attivita.voli.label";
    }else if(activity == "Financial"){
        response = "millemiglia.attivita.serviziFinanziari.label";
    }else if(activity == "Hotel"){
        response = "millemiglia.attivita.alberghi.label";
    }else if(activity == "CarRental"){
        response = "millemiglia.attivita.autoNoleggi.label";
    }else{
        response = "millemiglia.attivita.altre.label";
    }

    return response;
})