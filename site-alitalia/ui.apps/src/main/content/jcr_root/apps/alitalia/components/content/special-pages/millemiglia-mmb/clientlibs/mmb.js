
function onLockAccountButtonClick() {
    // per il momento location.search sembra piu' efficace, andra' parsata in ogni caso
    var paramFromUrl = location.search.substr(1);
    alert('** onResetAccountButtonClick ** ' + paramFromUrl);
    // TODO: parsing della url per recuperare il parametro
    // se il parametro in QueryString e' uno solo, non e' necessaria nessuna modifica
    // es : ?9wqUGLHD3HIba2wA4WOlfChve2b6sTpE
    console.log('paramFromUrl: ', paramFromUrl);
    $("#encodedUrlAccount").val(paramFromUrl);

    // solo per test !! Rimuovere
    // var strForm = $( "#form-mmb" ).serialize();
    // console.log(strForm);

///// Invocare il metodo servlet per la decodifica
   var data = {
       encodedUrlAccount : paramFromUrl
   };

//
//    invokeGenericFormService('checkCredenzialiMillemiglia', 'POST', '',
//        successResetAccount, failureResetAccount, alwaysResetAccount, undefined, data);

/////  oppure
   performSubmit("millemigliacomunicazionisubmit-lockaccount", "#form-mmb",
       successLockAccount, failureLockAccount, alwaysLockAccount);
}

function successLockAccount(data) {
    console.log('successResetAccount Success !!');
}

function failureLockAccount(data) {
    console.log('failureResetAccount FAIL !!');
}

function alwaysLockAccount(data) {
    // console.log('alwaysResetAccount Always !!');
}


/**
 * Retrive the value of a GET parameter in URL
 * @param val the parameter name
 * @returns {String} the value of the parameter if found, "" (blank) otherwise
 */
// function getURLParameter(val) {
//     var result = "", tmp = [];
//     var items = location.search.substr(1).split("&");
//     for (var index = 0; index < items.length; index++) {
//         tmp = items[index].split("=");
//         if (tmp[0] === val) {
//             result = decodeURIComponent(tmp[1]);
//             for (var j = 2; j < tmp.length; j++) {
//                 result = result + "=" + decodeURIComponent(tmp[j]);
//             }
//         }
//     }
//     return result;
// }

function onMmbDocumentReady(e) {
    $("#encodedUrlAccount").val("");
    $("#resetAccountSubmit").unbind('click', onLockAccountButtonClick);
    $("#resetAccountSubmit").bind('click', onLockAccountButtonClick);
}

$(document).ready(onMmbDocumentReady);