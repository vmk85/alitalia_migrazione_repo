package apps.alitalia.components.content.link_footer;

import com.adobe.cq.sightly.WCMUse;
import com.day.cq.wcm.api.WCMMode;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import com.adobe.granite.ui.components.Value;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

public class LinkFooter extends WCMUse {

private Map fieldList;
	
	@Override
    public void activate() throws Exception {
    	
		ValueMap properties = getProperties();
		
		fieldList = fromJcrMultifieldToMapArrayObject(getResource(), "items");
		
		
	}
	
	public Map getFieldList() {
        return fieldList;
    }
	
	//Metodo d'utilità per prendere i dati dai nodi jcr creati dal multifield
	private Map fromJcrMultifieldToMapArrayObject(Resource resource, String childName) throws JSONException {
		
		Resource item = resource.getChild(childName);
		
		Map<String, Object> multifield = new LinkedHashMap<String, Object>();
		Map<String, Object> field;
		int i = 0;
		
		if(item!=null){
			for (Resource multifieldResource : item.getChildren()) {
				ValueMap multifieldMap = multifieldResource.getValueMap();
				String[] multifieldMapKeys = 
						(String[]) multifieldMap.keySet().toArray(new String[multifieldMap.keySet().size()]); 
				String fieldValue = "";
				
				field = new LinkedHashMap<String, Object>();
	
				for (String valueMapKey : multifieldMapKeys) {
					if (!valueMapKey.equals("jcr:primaryType")) {
						fieldValue = multifieldMap.get(valueMapKey).toString();
						
						Map<String, Object> nestedMultifield = null;
						
						if (Pattern.matches("^\\[(\\{(\"[A-z0-9\\-\\.]*\"\\:\".*\"\\,?)*\\}\\,?)*\\]", fieldValue)) {
							//Se è un multifield nested
							JSONArray jsonArray = new JSONArray(fieldValue);
	
							nestedMultifield = new LinkedHashMap<String, Object>();
							Map<String, String> nestedFieldValue;
							
							for (int j = 0; j < jsonArray.length(); j++) {
								JSONObject jsonObject = jsonArray.getJSONObject(j);
								Iterator<String> keysIterator = jsonObject.keys();
								
								nestedFieldValue = new LinkedHashMap<String, String>();
								
								while (keysIterator.hasNext()) {
									String nextNestedKey = keysIterator.next();
									nestedFieldValue.put(nextNestedKey, jsonObject.getString(nextNestedKey));
								}
								
								nestedMultifield.put(Integer.toString(j), nestedFieldValue);
							}
							
							field.put(valueMapKey, nestedMultifield);
							
							
						} else {
							field.put(valueMapKey, fieldValue);
						}
					}
				}
				
				multifield.put(Integer.toString(i++), field);
			}
		}
		return multifield;
	}

}
