var mmSupport = {};
mmSupport.validation = function(e, service, form, done, fail, always, selector) {
	e.preventDefault();
	return mmSupport.performValidation(service, form, done, fail, always, selector);
}

mmSupport.performValidation = function(service, form, done, fail, always, selector) {
	var additionalParams = {
		'_action' : 'validate'
	};
	return mmSupport.invokeGenericFormService(service, 'POST', form, done, fail, always,
			selector, additionalParams);
}

mmSupport.getServiceUrl = function(service, selector, secure) {
	if (selector == 'login') {
		return service;
	}
	selector = selector || "json";
	var protocol = location.protocol;
	if (jsonProtocol && (selector == "json" || secure)) {
		protocol = 'https:';
	}
    if(service == 'menu-millemiglia-header'){
		return protocol + '//' + location.host + internalUrl1 + "."
			+ service + "." + selector;
    } else{
		return protocol + '//' + location.host + internalUrl1 + "/."
			+ service + "." + selector;
    }
}

mmSupport.invokeGenericFormService = function(service, method, form, done, fail, always,
		selector, additionalParams) {
	var actualData;
	if ($.type(form) === "string" || !form) {
		var serializedData = $(form).serialize();
		if (serializedData != "") {
			serializedData += "&";
		}
		serializedData += "_isAjax=true";
		if (additionalParams) {
			for ( var paramName in additionalParams) {
				serializedData += "&" + paramName + "="
						+ encodeURIComponent(additionalParams[paramName]);
			}
		}
		actualData = serializedData;
	} else if ($.isPlainObject(form)) {
		actualData = {};
		if (form) {
			for (var fieldName in form) {
				actualData[fieldName] = form[fieldName];
			}
		}
		actualData['_isAjax'] = true;
		if (additionalParams) {
			for (var paramName in additionalParams) {
				actualData[paramName] = additionalParams[paramName];
			}
		}
	}
	return $.ajax({
		url : mmSupport.getServiceUrl(service, selector),
		method : method,
		data : actualData,
		context : document.body
	}).done(function(data) {
		if (done) {
			done(data);
		}
	}).fail(function() {
		if (fail) {
			fail();
		}
	}).always(function() {
		if (always) {
			always();
		}
	});
}

mmSupport.init = function() {
    if(globalInit.length > 0)
        for(var i=0; i<globalInit.length; i++)
            globalInit[i]();
}

mmSupport.getDropDownData = function(requestData, done, fail, always) {
	return invokeGenericFormService('staticdatalistservlet', 'GET', '', done,
			fail, always, 'json', requestData);
}