var editLink = $('#editLink');
var saveBtn = $('#saveBtn');
var saveBtnInput = $('#saveBtn button');
var saveBtnMobile = $('#saveBtnMobile');
var saveBtnInputMobile = $('#saveBtnMobile button');
var disabledInputs = $('.disabled-input');
var disabledLabels = $('.disabled-label');
var tipoUtente = ''; // può essere MM | MA | BOTH

var first_name = "";
var last_name = "";
var date_gender = "";
var date_profession = "";
var date_birthday_day = "";
var date_birthday_month = "";
var date_birthday_year = "";
var date_home_address = "";
var date_home_cap = "";
var date_home_city = "";
var date_home_state = "";
var date_home_country = "";
var date_home_phone = "";
var date_home_prefix = "";
var date_home_tel_prefix = "";
var date_home_tel = "";
var date_office_company = "";
var date_office_address = "";
var date_office_cap = "";
var date_office_city = "";
var date_office_state = "";
var date_office_country = "";
var date_office_phone = "";
var date_office_prefix = "";
var date_office_tel_prefix = "";
var date_office_tel = "";
var defaultSelectedTipoTelefono = "";
var defaultSelectedPrefissoNazionale = "";
var defaultSelectedBirthDateDay = "";
var defaultSelectedBirthDateMonth = "";
var defaultSelectedBirthDateYear = "";

var ma;

function camelize(text) {
   return text.substring(0,1).toUpperCase() + text.substring(1,text.length).toLowerCase()
}

$(document).ready( function() {

    feedbackRow = $('.notification-row');

    setBirthDate();
    var requestData = {
        'professioni': "",
        'tipiTelefono': "",
        'prefissiNazionali': ""
    };

    mmSupport.getDropDownData(requestData, dropdownDataSuccess, dropdownDataFail);

});

function dropdownDataSuccess(data) {
    var tipiTelefono = data.tipiTelefono;
    for (index in tipiTelefono) {
        var option = '';
        if (tipiTelefono[index].name == defaultSelectedTipoTelefono) {
            option = '<option value="' + tipiTelefono[index].name
                + '" selected>'
                + tipiTelefono[index].value + '</option>';
        } else {
            option = '<option value="' + tipiTelefono[index].name + '">'
                + tipiTelefono[index].value + '</option>';
        }
        $('#phoneTypeHome').append(option);
        $('#phoneTypeOffice').append(option);
    }


    var prefissiNazionali = data.prefissiNazionali;
    for (index in prefissiNazionali) {
        var option = '';
        if (prefissiNazionali[index].prefix == defaultSelectedPrefissoNazionale) {
            option = '<option value="' + prefissiNazionali[index].prefix
                + '" selected>' + prefissiNazionali[index].description
                + ' (+' + prefissiNazionali[index].prefix + ') </option>';
        } else {
            option = '<option value="' + prefissiNazionali[index].prefix
                + '">' + prefissiNazionali[index].description
                + ' (+' + prefissiNazionali[index].prefix + ') </option>';
        }
        $('#hprefix').append(option);
        $('#cprefix').append(option);
    }
    var professioni = data.professioni;
    for(index in professioni) {
        $("select#profession").append($("<option>", {
            value: professioni[index].name,
            text: professioni[index].value
        }));
    }

    $("#profession").val(camelize(professione) || "Altro");
    $("#phoneTypeHome").val(getTipo(phoneTypeHome));
    $("#hprefix").val(hprefix);
    if(tipoIndirizzo.toUpperCase() == "HOME"){
        $("#abitazione+.placeholder").click();
    } else if(tipoIndirizzo.toUpperCase() == "COMPANY"){
        $("#ufficio+.placeholder").click();
    } else {
        $("#ufficio").attr("checked","").closest(".radio-wrap").removeClass("checked");
        $("#abitazione").attr("checked","").closest(".radio-wrap").removeClass("checked");
        $("#ufficioForm").addClass("hide");
        $("#abitazioneForm").addClass("hide");
    }    // performSubmit("GetMyAlitaliaCRMDataToSession", "", Login.Session.success.getCrmDataUtente, Login.Session.error.getCrmData);

    // ma.profile.professionalHeadline ? $("select#profession").val(ma.profile.professionalHeadline) : '';
}
function dropdownDataFail(data) {
}

function setBirthDate() {
    for(var i=1; i<=31; i++) {
        $("#birthDateDay").append($("<option>", {
            value: i,
            text: (i<10?"0"+i.toString():i)
        }));
        if(i <= 12) {
            $("#birthDateMonth").append($("<option>", {
                value: i,
                text: (i<10?"0"+i.toString():i)
            }));
        }
    }
    var currentYear = new Date().getFullYear();

    for(var i = currentYear-100; i<currentYear; i++) {
        $("#birthDateYear").append($("<option>", {
            value: i,
            text: i
        }));
    }

    for(var i = 0; i < dataNascita.length; i++){
        if(dataNascita[i].length > 1){
            if (dataNascita[i].substring(0,1) == "0"){
                dataNascita[i] = dataNascita[i].substring(1,dataNascita[i].length)
            }
        }
    }

    $("#birthDateDay").val(parseInt(dataNascita[2])!="00"? dataNascita[2]:"GG");
    $("#birthDateMonth").val(parseInt(dataNascita[1])!="00"? dataNascita[1]:"MM");
    $("#birthDateYear").val(parseInt(dataNascita[0])!="0000"? dataNascita[0]:"AAAA");

    // var selDay = parseInt(defaultSelectedBirthDateDay) < 10 ? defaultSelectedBirthDateDay.split('')[1] : defaultSelectedBirthDateDay;
    // var selMonth = parseInt(defaultSelectedBirthDateMonth) < 10 ? defaultSelectedBirthDateMonth.split('')[1] : defaultSelectedBirthDateMonth;
    //
    // $("#birthDateDay").val(defaultSelectedBirthDateDay!='' ? selDay : 'GG');
    // $("#birthDateMonth").val(defaultSelectedBirthDateMonth!='' ? selMonth : 'MM');
    // $("#birthDateYear").val(defaultSelectedBirthDateYear!='' ? defaultSelectedBirthDateYear : 'AAAA');
}

/**
 * inizializza i campi della form con i dati dell'utente MA
 */
// function popolaFormMAData( data, userMM ) {
//
//     // nascondiamo div per adesione mille miglia young
//     $(".divAdesioneMilleMigliaYoung").hide();
//     $("#hcountry").val("");
//     $("#ccountry").val("");
//
//     if(data.infoCliente.dataNascita != ""){
//         var dataNascita = data.infoCliente.dataNascita.split("-");
//     } else {
//         var dataNascita = ["AAAA","MM","GG"]
//     }
//     $("#firstName").val(data.infoCliente.nome);
//     $("#lastName").val(data.infoCliente.cognome);
//     $("#secondName").val(data.infoCliente.secondoNome);
//     $("#profession").val(data.infoCliente.professione.substring(0,1)+data.infoCliente.professione.substring(1,data.infoCliente.professione.length).toLowerCase());
//     $("#birthDateDay").val(parseInt(dataNascita[2]));
//     $("#birthDateMonth").val(parseInt(dataNascita[1]));
//     $("#birthDateYear").val(dataNascita[0]);
//     $("#gender").val(data.infoCliente.sesso);
//
//     if(data.infoCliente.tipoIndirizzo.toUpperCase() == "HOME"){
//         $("#abitazione+.placeholder").click()
//         $("#phoneTypeHome").val(getTipo(data.recapitiTelefonici.tipo));
//         $("#hprefix").val(data.recapitiTelefonici.prefissioNazionale);
//         if (data.recapitiTelefonici.tipo == "MobileBusiness" || data.recapitiTelefonici.tipo == "MobileBusiness"){
//             $("#htel").val(data.recapitiTelefonici.cellulare);
//         } else {
//             $("#htel").val(data.recapitiTelefonici.numero);
//         }
//         $("#haddress").val(data.infoCliente.indirizzo);
//         $("#hcap").val(data.infoCliente.cap);
//         $("#hcity").val(data.infoCliente.citta);
//         // $("#hstate").val(data.infoCliente.stato);
//         $("#hcountry").val(data.infoCliente.nazione);
//     } else if(data.infoCliente.tipoIndirizzo.toUpperCase() == "COMPANY"){
//         $("#ufficio+.placeholder").click();
//         $("#company").val(getTipo(data.infoCliente.nomeAzienda));
//         $("#phoneTypeOffice").val(data.recapitiTelefonici.tipo);
//         $("#cprefix").val(data.recapitiTelefonici.prefissioNazionale);
//         if (data.recapitiTelefonici.tipo == "MobileBusiness" || data.recapitiTelefonici.tipo == "MobileBusiness"){
//             $("#ctel").val(data.recapitiTelefonici.cellulare);
//         } else {
//             $("#ctel").val(data.recapitiTelefonici.numero);
//         }
//         $("#caddress").val(data.infoCliente.indirizzo);
//         $("#ccap").val(data.infoCliente.cap);
//         $("#ccity").val(data.infoCliente.citta);
//         // $("#cstate").val(data.infoCliente.stato);
//         $("#ccountry").val(data.infoCliente.nazione);
//     } else {
//         $("#ufficio").attr("checked","").closest(".radio-wrap").removeClass("checked");
//         $("#abitazione").attr("checked","").closest(".radio-wrap").removeClass("checked");
//         $("#ufficioForm").addClass("hide");
//         $("#abitazioneForm").addClass("hide");
//     }
//     //  SALVATAGGIO VALORE CAMPI
//     first_name = $("#firstName").val();
//     last_name = $("#lastName").val();
//     date_gender = $("#gender").val();
//     date_profession = $("#profession").val();
//     date_birthday_day = $("#birthDateDay").val();
//     date_birthday_month = $("#birthDateMonth").val();
//     date_birthday_year = $("#birthDateYear").val();
// // abitazione
//     date_home_address = $("#haddress").val();
//     date_home_cap = $("#hcap").val();
//     date_home_city = $("#hcity").val();
//     date_home_state = $("#hstate").val();
//     date_home_country = $("#hcountry").val();
//     date_home_phone = $("#phoneTypeHome").val();
//     date_home_prefix = $("#hprefix").val();
//     date_home_tel_prefix = $("#htel-prefix").val();
//     date_home_tel = $("#htel").val();
// // ufficio
//     date_office_company = $("#company").val();
//     date_office_address = $("#caddress").val();
//     date_office_cap = $("#ccap").val();
//     date_office_city = $("#ccity").val();
//     date_office_state = $("#cstate").val();
//     date_office_country = $("#ccountry").val();
//     date_office_phone = $("#phoneTypeOffice").val();
//     date_office_prefix = $("#cprefix").val();
//     date_office_tel_prefix = $("#ctel-prefix").val();
//     date_office_tel = $("#ctel").val();
//     disableLoaderMyAlitalia();
//
//     getUserMAInfo(function (dataGigya) {
//         if (dataGigya.data.address1_StateCode){
//             $("#hstate").val(dataGigya.data.address1_StateCode);
//         }
//     });
//
// }

function checkProfileMMState(data) {
	var state = VAL_STEP;
    $(".progress-bar").find("p").text(state + "%");
    for(var i=0; i<(state/VAL_STEP); i++) {
        $(".progress-bar").find("span:eq(" + i + ")").removeClass("empty");
        $(".progress-bar").find("span:eq(" + i + ")").addClass("fill");
    }
}

function requiredField(fieldId) {
    if ( ! $("#" + fieldId).val() ) {
        $("#" + fieldId).addClass("is-invalid-input");
        $("#"  + fieldId + "-feedback-error").text("Invalid data");
        $("#"  + fieldId + "-feedback-error").show();
        result = false;
    } else {
        $("#" + fieldId).removeClass("is-invalid-input");
        $("#"  + fieldId + "-feedback-error").text("");
        $("#"  + fieldId + "-feedback-error").hide();
    }

}
//
//function resetError(field){
//    field.removeClass("is-invalid-input");
//    field.parent().find('.feedback-error').remove();
//}
//
//function setError(field){
//    var label = CQ.I18n.get("myalitalia.common.invalid-data.label");
//    field.addClass("is-invalid-input");
//    field.parent().append('<div class="feedback-error">'+label+'</div>');
//    field.bind("change", validaForm);
//}
///**
// * restituisce true o false se i dati inseriti sono validi / sufficienti. Evidenzia anche sulla GUI i campi con problemi
// *
// */
//function validaForm( data ) {
//    var isValid = true;
//    $('.validate-input').each(function(i,obj) {
//        if( $(obj).val() == "" ||  $(obj).val() == null){
//            isValid = false;
//            setError( $(obj) );
//        }else{
//            resetError( $(obj) );
//        }
//    });
//
//    if($("#abitazione").is(":checked")) {
//        $('.validate-input-abitazione').each(function(i, obj) {
//            if( $(obj).val() == "" ||  $(obj).val() == null){
//                $(obj).addClass("is-invalid-input");
//                isValid = false;
//            }
//        });
//    }else{
//        $('.validate-input-ufficio').each(function(i, obj) {
//
//        });
//    }
//    return isValid;
//
////   let result =  true;
////    if($("#abitazione").is(":checked")) {
////
////        requiredField("haddress");
////        // TODO completare campi
////
////    } else {
////        requiredField("company");
////        requiredField("caddress");
////        requiredField("ccap");
////        // TODO completare campi
////    }
////    return result;
//}

function getTipo(val) {

    var tipo="";

    switch(val){
        case "MobileBusiness":
            tipo ="MOBILEBUSI";
            break;
        case "MobileHome":
            tipo = "MOBILEHOME";
            break;
        case "HomePhone":
            tipo = "HOMEPHONE";
            break;
        case "BusinessPhone":
            tipo = "BUSINESSPH";
            break;
        case "MOBILEBUSI":
            tipo ="MobileBusiness";
            break;
        case "MOBILEHOME":
            tipo = "MobileHome";
            break;
        case "HOMEPHONE":
            tipo = "HomePhone";
            break;
        case "BUSINESSPH":
            tipo = "BusinessPhone";
            break;
    }


    return tipo;
}

$("#save, #saveMobile").click(function() {

        enableLoaderMyAlitalia();

        var indirizzo;
        var tipoIndirizzo = convertType($("#ufficio[checked]").val() || $("#abitazione[checked]").val());
        var cap;
        var citta;
        var nazione;
        var stato;
        var tipo;
        // var cellulare = "";
        var numero = "";
        var prefissioNazionale = "";

        if(tipoIndirizzo == "HOME"){
            tipo = getTipo($("#phoneTypeHome").val());
            indirizzo = $("#haddress").val();
            cap = $("#hcap").val();
            citta = $("#hcity").val();
            nazione = $("#hcountry").val();
            // stato = $("#hstate").val();
            numero = $("#htel").val();
            prefissioNazionale = $("#hprefix").val();
        } else if(tipoIndirizzo == "COMPANY"){
            tipo = getTipo($("#phoneTypeOffice").val());
            indirizzo = $("#caddress").val();
            cap = $("#ccap").val();
            citta = $("#ccity").val();
            nazione = $("#ccountry").val();
            // stato = $("#cstate").val();
            numero = $("#ctel").val();
            prefissioNazionale = $("#cprefix").val();
        } else {
            indirizzo = "";
        }
        var annoNascita;
        var meseNascita;
        var giornoNascita;
        if ($("#birthDateYear").val() == "AAAA"|| $("#birthDateYear").val()==null){
            annoNascita = "0000";
        } else {
            annoNascita = $("#birthDateYear").val();

        }
        if ($("#birthDateMonth").val() == "MM" || $("#birthDateMonth").val() == null){
            meseNascita = "00";
        } else {
            meseNascita = $("#birthDateMonth").val().length <2 ? "0"+$("#birthDateMonth").val():$("#birthDateMonth").val();

        }
        if ($("#birthDateDay").val() == "GG" || $("#birthDateDay").val() == null){
            giornoNascita = "00";
        } else {
            giornoNascita = $("#birthDateDay").val().length <2 ? "0"+$("#birthDateDay").val():$("#birthDateDay").val();

        }

    // salva utente MA
        var params = {};
//         var profile = {};
//         $("#firstName").val(capWord($("#firstName").val()));
//         $("#secondName").val(capWord($("#secondName").val()));
//         $("#lastName").val(capWord($("#lastName").val()));
//
//         profile["firstName"] = $("#firstName").val() || null;
//         params["secondoName"] = $("#secondName").val() || null;
//         profile["lastName"] = $("#lastName").val() || null;
//         profile["birthDay"] = ($("#birthDateDay").val() != "GG" ? $("#birthDateDay").val() : null);
//         profile["birthMonth"] = ($("#birthDateMonth").val() != "MM" ? $("#birthDateMonth").val() : null);
//         profile["birthYear"] = ($("#birthDateYear").val() != "AAAA" ? $("#birthDateYear").val() : null);
//
//         if($('input[name=switch-address]:checked').val() == "abitazione") {
//
//             params["address1_Type"] = "home";
// //            if($("#hcap").val() != "")
//                 params["address1_Zip"] = $("#hcap").val() || null;
// //            if( $("#hcity").val() != "")
//                 params["address1_City"] = $("#hcity").val() || null;
// //            if($("#htown").val() != "")
//                 params["address1_CountryCode"] = $("#hcountry").val() || null;
           if($("#hstate").val() != "")
                params["address1_StateCode"] = $("#hstate").val().toUpperCase() || null;
// //            if($("#haddress").val() != "")
//                 params["address1_Address"] = $("#haddress").val() || null;
//            if($("#htel").val() != "")
//                 params["phone1Number"] = $("#htel").val() || null;
//            if($("#hprefix").val() != "")
//                 params["phone1CountryCode"] = $("#hprefix").val() || null;
// //            if($("#h-prefix").val() != "")
//                 params["phone1AreaCode"] = $("#htel-prefix").val() || null;
//             params["phone1Type"] = $("#phoneTypeHome").val() ? $("#phoneTypeHome").val() : $("#phoneTypeOffice").val();
            //params["mmIsYoung"] = $("#hmmy").is(":checked");
//
//         } else {
//             // provincia o stato
//             params["address1_Type"] = "company";
//
//             if($("#company").val() != "")
//                 params["address1_CompanyName"] = $("#company").val() || null;
//             if($("#cstate").val() != "")
                // params["address1_StateCode"] = $("#cstate").val() || null;
//             if($("#ccap").val() != "")
//                 params["address1_Zip"] = $("#ccap").val() || null;
//             if($("#caddress").val() != "")
//                 params["address1_Address"] = $("#caddress").val() || null;
//             if( $("#ccity").val() != "")
//                 params["address1_City"] = $("#ccity").val() || null;
//             if($("#ccountry").val() != "")
//                 params["address1_CountryCode"] = $("#ccountry").val() || null;
//
//             if($("#ctel").val() != "") {
//                 params["phone1Number"] = $("#ctel").val() || null;
//             }
//             if($("#cprefix").val() != "")
//                 params["phone1CountryCode"] = $("#cprefix").val();
//             if($("#cprefix").val() != "")
//                 params["phone1AreaCode"] = $("#ctel-prefix").val();
//             //params["mmIsYoung"] = $("#cmmy").is(":checked");
//             params["phone1Type"] = $("#phoneTypeOffice").val() || null;
//         }
//         if($("#profession").val() != "")
//             profile["professionalHeadline"] = $("#profession").val() || null;
//         profile["gender"] = $("#gender").val();
//
//         var par = {
//             callback: function(resp) {
//                 if(resp.errorCode == 0) {
//                     checkProfileStateMA();
//                     gigya.accounts.getAccountInfo({
//                         callback: function(resp) {
//
//                         }
//                     });
//                     // salvataggio utente MA in servlet
//
//                 } else myAlitaliaOptions.feedbackFail();
//             },
//             data: params,
//             // profile: profile
//         };


        var crmRequest = { data : JSON.stringify(
                {
                    infoCliente : {
                        nome: $("#firstName").val(),
                        cognome: $("#lastName").val(),
                        secondoNome: $("#secondName").val(),
                        titolo: "",
                        sesso: $("#gender").val(),
                        dataNascita: annoNascita + "-" +  meseNascita + "-" + giornoNascita,
                        professione: $("#profession").val(),
                        tipoIndirizzo: tipoIndirizzo,
                        nomeAzienda: $("#company").val(),
                        indirizzo: indirizzo,
                        cap: cap,
                        citta: citta,
                        nazione: nazione,
                        stato: ""
                    },
                    recapitiTelefonici:{
                        prefissioNazionale : prefissioNazionale,
                        prefissoArea : "",
                        numero : numero,
                        tipo : tipo,
                        cellulare : ""
                    }
                }),
            gigya : JSON.stringify(params)
        };

        performSubmit("SetMyAlitaliaCrmDataProfiloPersonale",crmRequest,Login.Session.success.setProfileData,Login.Session.error);

        // gigya.accounts.setAccountInfo(par);
});

function datiSucces(data) {
    console.log(data);
    disabledInputs.attr('disabled', true);

    // Rimuovi la classe disabled-label dalle label degli input radio
    disabledLabels.addClass('disabled-label');

    // Nascondi il link per modificare i dati
    // Mostra il pulsante per salvare i dati
    editLink.removeClass('hide');
    /*saveBtn.addClass('hide');*/
    myAlitaliaOptions.feedbackSuccess();
	/*if (data.result) {
		removeErrors();
		$('#datiProfiloSumbit').unbind('click');
		$('#datiPersonaliEdit').submit();
	} else {
		showErrors(data);
		showFormFeedbackError('datiPersonaliEdit',
				'millemiglia.datiPersonali.error');
		if (typeof(data.fields["cap"]) != "undefined") {
			if (typeof(window["analytics_CAP_Error"]) === "function"){
				window["analytics_CAP_Error"]();
			}
		}
	}*/
	return false;
}

function datiError(data) {
    console.log(data);
    myAlitaliaOptions.feedbackFail();
	/*showFormFeedbackError('datiPersonaliEdit',
			'millemiglia.datiPersonali.ajaxError');*/
	return false;
}

function getServiceUrl(service, selector, secure) {
    if (selector == 'login') {
        return service;
    }
    selector = selector || "json";
    var protocol = location.protocol;
    if (jsonProtocol && (selector == "json" || secure)) {
        protocol = 'https:';
    }
    if(service == 'menu-millemiglia-header'){
        return protocol + '//' + location.host + internalUrl1 + "."
            + service + "." + selector;
    } else{
        return protocol + '//' + location.host + internalUrl1 + "/."
            + service + "." + selector;
    }
}

editLink.on('click', function () {
    // var disabledInputs = $('.disabled-input');
    // var disabledLabels = $('.disabled-label');
    //var saveBtn = $('#saveBtn');

    // Rimuovi l'attributo disabled da tutti gli input
    disabledInputs.removeAttr('disabled');

    // Rimuovi la classe disabled-label dalle label degli input radio
    disabledLabels.removeClass('disabled-label');

    // Nascondi il link per modificare i dati
    // Mostra il pulsante per salvare i dati
    editLink.addClass('hide');
    /*saveBtn.removeClass('hide');*/
});



$("#cprov").on("keyup", function() {
    $(this).val($(this).val().toUpperCase());
});


$(".btn_annulla#annulla, .btn_annulla#annullaMobile").click(function(){
        // ripristino i dati precedenti
        $("#firstName").val(first_name);
        $("#lastName").val(last_name);
        $("#gender").val(date_gender);
        $("#profession").val(date_profession);
        $("#birthDateDay").val(date_birthday_day);
        $("#birthDateMonth").val(date_birthday_month);
        $("#birthDateYear").val(date_birthday_year);
        $("#haddress").val(date_home_address);
        $("#hcap").val(date_home_cap);
        $("#hcity").val(date_home_city);
        $("#hstate").val(date_home_state);
        $("#hcountry").val(date_home_country);
        $("#phoneTypeHome").val(date_home_phone);
        $("#hprefix").val(date_home_prefix);
        $("#htel-prefix").val(date_home_tel_prefix);
        $("#htel").val(date_home_tel);
        $("#company").val(date_office_company);
        $("#caddress").val(date_office_address);
        $("#ccap").val(date_office_cap);
        $("#ccity").val(date_office_city);
        $("#cstate").val(date_office_state);
        $("#ccountry").val(date_office_country);
        $("#phoneTypeOffice").val(date_office_phone);
        $("#cprefix").val(date_office_prefix);
        $("#ctel-prefix").val(date_office_tel_prefix);
        $("#ctel").val(date_office_tel);


        // Aggiungi l'attributo disabled da tutti gli input
        disabledInputs.prop("disabled", true);

        // Aggiungi la classe disabled-label dalle label degli input radio
        disabledLabels.addClass('disabled-label');

        // Mostra il link per modificare i dati
        // Nascondi il pulsante per salvare i dati
        editLink.removeClass('hide');
        /*saveBtn.addClass('hide');*/

});

function capWord(word) {
  if (word != null && word != '') {
    word = word.split("");
    word[0] = word[0].toUpperCase();
    word = word.join("");
    return word;
  } else {
    return '';
  }
}

function convertType(type) {

    switch (type){
        case "abitazione":
            type = "HOME";
            break;
        case "ufficio":
            type = "COMPANY";
            break;
        default:
            type = "";
            break;
    }

    return type;

}
