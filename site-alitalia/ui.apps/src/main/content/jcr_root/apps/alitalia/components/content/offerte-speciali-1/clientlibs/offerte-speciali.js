var offerteSpecialiOptions = {};

$(document).ready(function() {

    offerteSpecialiOptions.fixedSlidePosition = false;

    offerteSpecialiOptions.slider = new Swiper('.offerte-speciali__slider .swiper-container', {
        slidesPerView: 'auto',
        resistanceRatio: 0,
        pagination: {
            el: '.offerte-speciali__slider .swiper-pagination',
            clickable: true
        }
    });

    $('.offerte-speciali__slider .bg-image').foundation();
});

$(window).resize(function() {
    if(ww < 768 && !offerteSpecialiOptions.fixedSlidePosition) {
        setTimeout(function() {
            offerteSpecialiOptions.slider.slideTo(0, 0);
            offerteSpecialiOptions.fixedSlidePosition = true;
        }, 1);
    } else if(ww >= 768) offerteSpecialiOptions.fixedSlidePosition = false;
});
