var cercaVoloOptions = {};
cercaVoloOptions.currValDep = 0;
cercaVoloOptions.currValArr = 0;
cercaVoloOptions.isHistogramFirstTime = 1;
cercaVoloOptions.accordionTitleOffset = [];
cercaVoloOptions.desktopOverlayOpened = 0;
cercaVoloOptions.lastActiveElement = document.activeElement;
var newCity;



// document ready
$( document ).ready( function() {

    var useragent = navigator.userAgent.toLowerCase();

    var ismobile = (/iphone|ipod|android|blackberry|opera|mini|windows\sce|palm|smartphone|iemobile/i.test(navigator.userAgent.toLowerCase()));
    var istablet = (/ipad|android 3.0|xoom|sch-i800|playbook|tablet|kindle/i.test(navigator.userAgent.toLowerCase()));

    //different padding-top for tablet when opening new pages for inputs
    if (istablet){
        $('[id*="panel-"] .panel__content').css('padding-top','130px');
    }
    //prevent opening keyboard on focus of input dates
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $('#data-andata--prenota-desk').attr('readonly',true);
        $('#data-ritorno--prenota-desk').attr('readonly',true);
        $('#departure_date--flight-status-desk').attr('readonly',true);
        $('#departure_date--timetables-desk').attr('readonly',true);
    }
    //when going in landscape orientation set content of tabs visible again. Without the setTimeout it's not working
    $( window ).on( "orientationchange", function( event ) {
        if (window.orientation == 90 || window.orientation == -90) {
            setTimeout(function(){
                cercaVoloOptions.closeOverlayDesktop();
                $('.tabs-content[data-tabs-content="cerca-volo__widget"]').css('z-indez','1');
                $('.tabs-content[data-tabs-content="cerca-volo__widget"]').css({ 'position': 'relative' });
                $('.tabs-panel.is-active').css('z-indez','1');
                $('.tabs-panel.is-active').css({ 'position': 'relative' });
            },500);
        }
    });
    //it's enough to make close tooltip when click outside of it
    $('#panel-travel-options').on('click',function(){});
    //hide panel-histogram for Firefox
     $( "#cerca-volo__close-button" ).click(function() {
     $('#luogo-arrivo--prenota-desk').attr('data-type',"");
    });

    function isSameActiveElement() {
        var currentActiveElement = document.activeElement;
        if(cercaVoloOptions.lastActiveElement != currentActiveElement) {
            cercaVoloOptions.lastActiveElement = currentActiveElement;
            return false;
        }
        return true;
    }
    function detectFocus() {
        if (document.activeElement.id == "check-in-search" || document.activeElement.id == "booking-search" || document.activeElement.id == "my-flights" || document.activeElement.id == "flights-info" ){
            var idd = "#"+document.activeElement.id;
            $(idd).trigger('click');
        }
    }
    function attachEvents() {
        window.addEventListener ? window.addEventListener('focus', detectFocus, true) : window.attachEvent('onfocusout', detectFocus);
    }
    attachEvents();

    var isDesktop = !(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(useragent)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(useragent.substr(0,4)));
    if (window.innerWidth == 1024 && isDesktop){
        $('.cerca-volo__content.cerca-volo__content--prenota.tabs-panel.is-active').css('display','block');
    }



    //svuoto il campo aeroporto di ritorno alla chiusura dell'overlay desktop
    $('.icon--close').on("click", function(){
    $('#luogo-arrivo--prenota-desk').attr('data-type',"");
      });

    // get accordion title offset top value
    cercaVoloOptions.calculateAccordionTop();

    cercaVoloOptions.histogramCounter = 0;

    // fill hidden From
    cercaVoloOptions.updateHiddenFormAllValues( cercaVoloOptions.renderFrontendAllInputs );

    // update hidden form & render frontend on input change
    $( '[data-ref]' ).on( 'change', function() {
        cercaVoloOptions.currInputID = $( this ).attr( 'id' );
        cercaVoloOptions.updateHiddenFormOnChange( $( this ) );
        //cercaVoloOptions.updateHiddenFormOnChange( $( this ) );
        //setTimeout( function() {
        //	if( cercaVoloOptions.isValidHistogram() ) {
        //		cercaVoloPrenotaOptions.openCollapsablePanel( $('#panel-histogram') );
        //	}
        //}, 200 );
    } ).on( 'blur', function(e) {
        // preserve inserted value if autocomplete input
        if( $( e.target ).attr( 'name' ) == 'list_andata--prenota' || $( e.target ).attr( 'name' ) == 'destination--prenota' || $( e.target ).attr( 'name' ) == 'list_andata--timetables' || $( e.target ).attr( 'name' ) == 'destination--timetables' ) {

            // if input already has value (iata code)
            if( $($(this).data( 'ref' ).replace( '--' , 'Code--' )).val().length === 3 && $($(this).data( 'ref' )).val() !== 'not-set' ) {

                // split value to make IATA code bold
                $( '[name="' + $( this ).attr( 'name' ) + '"]' ).each( function(i, el) {
                    $( el ).parent().find( '.input-placeholder' ).html( $($(this).data( 'ref' )).val().slice(0, -4) + '<strong class="iata-code">' + $($(this).data( 'ref' ).replace( '--' , 'Code--' )).val() + '</strong>' );
                } );
            }
        }

    } ).on( 'keydown', function(e) {

        // set value on 'enter' departure date and move to next tab
        if( $(e.target).attr('id') == 'data-andata--prenota-desk' && Foundation.MediaQuery.atLeast( 'large' ) ) {
            if(e.originalEvent.code == 'Enter' || e.originalEvent.code == 'NumpadEnter') {
                e.preventDefault();
                setTimeout(function() {
                    prenotaDataOptions.to.datepicker( "show" );
                }, 100);
            }
        }

    } );

    // init radio
    cercaVoloOptions.initRadio();

    // ready disable data ritorno
    // if( $( '#switch-from--prenota-mobile' ).attr( 'checked' ) === 'checked' ) {
    // set current radio on back (checkin, info voli, prenota)
    // set current radio on back (checkin, info voli, prenota)
    $( '.cerca-volo__content .radio-group' ).each( function(i, el) {
        var currCheckedRadio = $( el ).find( '.checked input' ).val();
        if( !$(el).closest('.cerca-volo__content--prenota').length ) {
            // checkin - info-voli
            $( '.' + currCheckedRadio ).removeClass('hide');
        } else {
            // prenota
            if( currCheckedRadio == 'solo-andata' ) {
                $('[id*="data-ritorno--prenota"]').attr('disabled', true);
                $('[id*="data-ritorno--prenota"]').parent().find( '.input-placeholder' ).addClass( 'disabled' );
            }
        }
    } );
    // custom input group behaviour
    cercaVoloOptions.inputGoupFocus();

    // apri prima tab su desk
    if( ww >= 1024 ) {
        $( '#cerca-volo__widget  .cerca-volo__tab--prenota' ).addClass( 'is-active' );
    }

    initAccordionTab(initAccordionTabCb);

    //make radio button clickable
    /*$('.input-wrap.radio-wrap').on('click',function(){
        if (!Foundation.MediaQuery.atLeast( 'large' )){
             $('.input-wrap.radio-wrap.checked').removeClass('checked');
             $(this).removeClass('checked').addClass('checked');
             console.log($('#switch-from--prenota-mobile').parent().hasClass( 'checked' ) ,$('#switch-from--prenota-mobile').attr( 'checked' ) === 'checked' );
             if( $('#switch-from--prenota-mobile').parent().hasClass( 'checked' )) {
             	$('#data-ritorno--prenota-desk').attr('disabled', 'disabled');
             	$('label[for="data-ritorno--prenota-desk"]').next().removeClass('disabled').addClass( 'disabled' );
             	//$('[id*="switch-from--prenota-mobile"]').parent().find( '.input-placeholder' ).addClass( 'disabled' );
             }else{
             	$('#data-ritorno--prenota-desk').attr('disabled', false);
             	$('label[for="data-ritorno--prenota-desk"]').next().removeClass('disabled');

             }
        }
    })*/

    // init responsive accordion tab
    // cercaVoloOptions.initAccordionTab = new Foundation.ResponsiveAccordionTabs( $( '#cerca-volo__widget' ), {} );

    // prevent scroll to top on ios
    if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
        $('input[data-ref="#departureAirport--prenota"], input[data-ref="#arrivalAirport--prenota"], input[data-ref="#departureDate--prenota"], input[data-ref="#returnDate--prenota"], input[data-ref="#flightNumber--flight-status"], input[data-ref="#departureDate--flight-status"], input[data-ref="#departureAirport--timetables"], input[data-ref="#arrivalAirport--timetables"], input[data-ref="#departureDate--timetables"]').on('focus', function () {
            window.scrollTo(0, 0);
            document.body.scrollTop = 0;
        });
    }

    // fix tooltip on scroll mobile
    $(window).on('scroll', function () {
        if (!Foundation.MediaQuery.atLeast('large')) {
            $('.has-tip').foundation('hide');
        }
    });

    $('#panel-travel-options .panel__content').on('scroll', function () {
        if (!Foundation.MediaQuery.atLeast('large')) {
            $('.has-tip').foundation('hide');
        }
    });


    // widget desktop overlay controls
    	$( '.cerca-volo input[type="text"]' ).on( 'focus', function() {
		if(Foundation.MediaQuery.atLeast('large')) {
			cercaVoloOptions.openOverlayDesktop();
		}
	} );
    $( '.cerca-volo__title, .cerca-volo__content input, .cerca-volo__content .cta-history--form, .cerca-volo__content .reverse-button' ).on( 'focus', function(e) {
        if ( cercaVoloOptions.desktopOverlayOpened == 0 ) {
            cercaVoloOptions.openOverlayDesktop();
        }
    });
    $( '#cerca-volo__close-button' ).on( 'click', function(e) {
        if ( cercaVoloOptions.desktopOverlayOpened == 1 ) {
            cercaVoloOptions.closeOverlayDesktop();
        }
        return false;
    });

    // submit hidden forms
    $( '[id*="submitHidden--"]' ).on( 'click', function() {
        if ((($(this).data('submit')) == "#hiddenForm--prenota") && (($('#roundTrip--prenota').val()) == "a")) {
            $("#returnDate--prenota").val("");
        }
        $("#adultPassenger--prenota").val($('#adult-passenger--prenota-mobile').val());
        $("#kidPassenger--prenota").val($('#kids-passenger--prenota-mobile').val());
        $("#babyPassenger--prenota").val($('#baby-passenger--prenota-mobile').val());

        $( $( this ).data( 'submit' ) ).submit();
        saveIntoLocalStorage();
    } );

    $( '*' ).on( 'focus', function(e) {
        if($('.section--cerca-volo').hasClass('on-focus')) {
            if( $(this).closest( '.section--cerca-volo' ).length === 0 ) {
                if(!isIE() && !($(this).hasClass('closeCookie'))) {
                    cercaVoloOptions.closeOverlayDesktop();
                }
            }
        }
    } );

    // prevent animation queue
    $( '#cerca-volo__widget .accordion-title' ).on( 'click', function() {
        $( '.cerca-volo__tab' ).addClass( 'animating' );

        var currSiblingsID = $( this ).parent().data( 'index' );



        // scroll to active tab
        if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
            $( 'html, body' ).animate( {
                'scrollTop' : cercaVoloOptions.accordionTitleOffset[currSiblingsID],
            }, 300 );
        }

    } );

    //inizio modifica per non permettere la digitazione di lettere nei campi delle date

    /*$('#data-andata--prenota-desk').keypress(function(e){
                e.preventDefault();
    });

    $('#data-andata--prenota-desk').keyup(function(e){
                       if(e.keyCode == 13){
                        $('#ui-datepicker-div').hide();
                       }
    });

    $('#data-ritorno--prenota-desk').keypress(function(e){
                e.preventDefault();
    });

    $('#data-ritorno--prenota-desk').keyup(function(e){
                       if(e.keyCode == 13){
                        $('#ui-datepicker-div').hide();
                       }
    });

    $('#departure_date--flight-status-desk').keypress(function(e){
                e.preventDefault();
    });

    $('#departure_date--flight-status-desk').keyup(function(e){
                       if(e.keyCode == 13){
                        $('#ui-datepicker-div').hide();
                       }
    });

    $('#departure_date--timetables-desk').keypress(function(e){
                e.preventDefault();
    });
    */

    $('#departure_date--flight-status-desk').on('blur', function(e){
        e.preventDefault();
        currentFocusDatePicker = undefined;
    });

    $('#departure_date--timetables-desk').keyup(function(e){
        if(e.keyCode == 13){
            $('#ui-datepicker-div').hide();
        }
    });
    //fine modifica



    //if greek market font-size of input-placeholder for dates and icon calendars smaller
    if ($('html').attr('lang') == "el-gr" ){

        $('.section--cerca-volo .cerca-volo .cerca-volo__content .partenza-destinazione .input-wrap, .section--cerca-volo .cerca-volo .cerca-volo__content .date-viaggio .input-wrap, .section--cerca-volo .cerca-volo .cerca-volo__content .timetables .input-wrap, .section--cerca-volo .cerca-volo .cerca-volo__content .flight_status .input-wrap').addClass('greek');

        $('[data-toggle-panel="#panel-date-andata--prenota"]').css('padding-left','24px').css('font-size','14px');

        $('[data-toggle-panel="#panel-date-ritorno--prenota"]').css('padding-left','24px').css('font-size','14px');
        $('[data-toggle-panel="#panel-list_andata--prenota"]').css('font-size','14px');

    }else {
        if ($('html').attr('lang') == "ru-ru"){
            if ($('html').attr('lang') == "ru-ru"){
                $('.section--cerca-volo .cerca-volo .cerca-volo__content .partenza-destinazione .input-wrap, .section--cerca-volo .cerca-volo .cerca-volo__content .date-viaggio .input-wrap, .section--cerca-volo .cerca-volo .cerca-volo__content .timetables .input-wrap, .section--cerca-volo .cerca-volo .cerca-volo__content .flight_status .input-wrap').addClass('russian');

                $('[data-toggle-panel="#panel-date-andata--prenota"]').css('font-size','16px').css('padding-left','30px');
                $('[data-toggle-panel="#panel-list_andata--prenota"]').css('font-size','16px');
                $('#span_luogo-arrivo--prenota-desk').css('font-size','16px');
                $('[data-toggle-panel="#panel-date-ritorno--prenota"]').css('font-size','16px').css('padding-left','30px');

            }
        }

    }
} );

function isIE() {
    //userAgent = userAgent || navigator.userAgent;
	/*if(typeof(userAgent) !="undefined") {
		return userAgent.indexOf("MSIE ") > -1 ; //|| userAgent.indexOf("Trident/") > -1 || userAgent.indexOf("Edge/") > -1;
	}*/
    // AMS Bugfix #4583
    var ua = navigator.userAgent;
    var is_ie = false;
    if (typeof(ua) != "undefined") {
        is_ie = /MSIE|Trident/.test(ua);
    }
    return is_ie;
}

function saveIntoLocalStorage() {
    var ricercheRecenti = [];
    var items = localStorage.getItem("ricercheRecenti");
    if (items == null || items == 'undefined' || items == '[]' || items.length == 0) {
        ricercheRecenti = [];
    } else {
        ricercheRecenti = JSON.parse(items);
    }
    var rdate;
    var tipoViaggio = "cercaVolo.label.";
    if( $("#roundTrip--prenota").val() == "a"){
        rdate = "";
        tipoViaggio += "soloandata";
    } else {
        rdate = $("#returnDate--prenota").val();
        tipoViaggio += "andataritorno";
    }
    var npass = ($("#adultPassenger--prenota").val()*1) + ($("#youngAdultPassenger--prenota").val()*1) + ($("#kidPassenger--prenota").val()*1) + ($("#babyPassenger--prenota").val()*1);
    var item = {
        "cittaPartenza": {
            "nome": "",
            "nomeT": $("#departureAirport--prenota").val().substring(0,$("#departureAirport--prenota").val().length-$("#departureAirportCode--prenota").val().length),
            "nazione": "",
            "nazioneT": "",
            "aeroporto": "",
            "aeroportoT": "",
            "codiceAeroporto": $("#departureAirportCode--prenota").val()
        },
        "cittaArrivo": {
            "nome": "",
            "nomeT": $("#arrivalAirport--prenota").val().substring(0,$("#arrivalAirport--prenota").val().length-$("#arrivalAirportCode--prenota").val().length),
            "nazione": "",
            "nazioneT": "",
            "aeroporto": "",
            "aeroportoT": "",
            "codiceAeroporto": $("#arrivalAirportCode--prenota").val()
        },
        "tipoViaggioLabel": tipoViaggio,
        "tipoViaggio": $("#roundTrip--prenota").val(),
        "dataPartenza": $("#departureDate--prenota").val(),
        "dataRitorno": rdate,
        "classeViaggio": "cercaVolo.label." + $("#seatType--prenota").val(),
        "nPasseggeri": npass,
        "nAdulti": ($("#adultPassenger--prenota").val()*1),
        "nBambini": ($("#kidPassenger--prenota").val()*1),
        "nNeonati": ($("#babyPassenger--prenota").val()*1),
        "cug": "ADT",
        "fromSpecialOffer": ""
    };
    ricercheRecenti.unshift(item);
    if (ricercheRecenti.length > 3) {
        ricercheRecenti.splice(3, ricercheRecenti.length-1);
    }
    localStorage.setItem("ricercheRecenti", JSON.stringify(ricercheRecenti));
}

// mobile accordion envents ( open )
$( window ).on( 'down.zf.accordion', function( event, name ) {
    // prevent animation queue
    $( '.cerca-volo__tab' ).removeClass( 'animating' );
    if( event.target.id == 'cerca-volo__widget' && !Foundation.MediaQuery.atLeast( 'large' ) ) {
        cercaVoloOptions.openOverlayMobile();
    }

} );

// mobile accordion envents ( close )
$( window ).on( 'up.zf.accordion', function( event, name ) {
    // prevent animation queue
    $( '.cerca-volo__tab' ).removeClass( 'animating' );
    if( event.target.id == 'cerca-volo__widget' && !Foundation.MediaQuery.atLeast( 'large' )) {
        cercaVoloOptions.closeOverlayMobile();
    }
} );

// on media query chance (desktop / mobile)
$(window).on('changed.zf.mediaquery', function(event, name) {

    if( Foundation.MediaQuery.atLeast( 'large' ) ) {
        cercaVoloOptions.closeOverlayMobile();
        cercaVoloOptions.closeOverlayDesktop();
        setTimeout(  function() {
            $( '.tabs-content[data-tabs-content="cerca-volo__widget"] .cerca-volo__content' ).removeAttr( 'style' );
            $( '#cerca-volo__widget' ).foundation( 'selectTab', $( '.cerca-volo__content:first-child' ) );
        }, 50 );
    } else {
        $( '#cerca-volo__widget .accordion-content' ).css( 'display', 'none' );
        $( '#cerca-volo__widget .is-active' ).removeClass( 'is-active' );
        // get accordion title offset top value
        setTimeout(function() {
            cercaVoloOptions.calculateAccordionTop();
        }, 400);
    }
});

// init all cerca volo radio
cercaVoloOptions.initRadio = function() {
    if($('.section--cerca-volo .radio-wrap input:checked').length) {
        $('.section--cerca-volo .radio-wrap input:checked').closest('.radio-wrap').addClass('checked');
    }
    $(document).on('click', '.section--cerca-volo .radio-wrap .placeholder', function() {
        $(this).parent().find('input').click();
    });
    $(document).on('click', '.section--cerca-volo .radio-wrap input', function() {
        $('.section--cerca-volo .radio-wrap input[name='+$(this).attr('name')+']').closest('.radio-wrap').removeClass('checked');
        $(this).closest('.radio-wrap').addClass('checked');
        cercaVoloPrenotaOptions.switchForm( $( this ).parent() );
        // remove input error
        if($(this).val() == 'solo-andata')
        {
            $( '[id*="data-ritorno--prenota"]' ).parent().removeClass( 'has-input-error' );
        }
    });

    $(document).on('focus', '.section--cerca-volo .radio-wrap input', function() {
        $(this).closest('.radio-group').find('on-focus').removeClass('on-focus');
        $(this).closest('.radio-wrap').addClass('on-focus');
    });
    $(document).on('blur', '.section--cerca-volo .radio-wrap input', function() {
        $(this).closest('.radio-wrap').removeClass('on-focus');
    });
};

cercaVoloOptions.openOverlayMobile = function() {
    $( '.section--cerca-volo' ).addClass( 'overlay--mobile' );
};

cercaVoloOptions.closeOverlayMobile = function() {
    $( '.section--cerca-volo' ).removeClass( 'overlay--mobile' );
    $('.reveal-overlay').css('display','none');

};

cercaVoloOptions.openOverlayDesktop = function() {
    if( Foundation.MediaQuery.atLeast( 'large' ) ) {
        if( $( '.section--cerca-volo.on-focus' ).length === 0 ) {
            $( '.section--cerca-volo' ).addClass( 'on-focus' );
            $('.cookie-bar').css('z-index','73');
            $('#panel-all-destination').css('z-index','101');
        }
        $("li.notification-cta.active, a.notification-label, .notification-wrap").addClass("change-index")
        $( 'html, body' ).animate( {
            scrollTop: $( '.section--cerca-volo' ).offset().top
        }, 300, 'swing', function() {
            // callback
            // date picker position
            setCalendarPickerPosition();
        } );

        if ($(".logged-user-bar").hasClass("open")){
            $(".logged-user-bar").removeClass("open");
            $(".logged-user-bar").css("display","none")
        }

        cercaVoloOptions.desktopOverlayOpened = 1;

    }
};

cercaVoloOptions.closeOverlayDesktop = function() {
    if( Foundation.MediaQuery.atLeast( 'large' ) && $( '.section--cerca-volo.on-focus' ).length !== 0 ) {
        $( '.section--cerca-volo' ).removeClass( 'on-focus' );
        $('.cookie-bar').css('z-index','93');
        $('#panel-all-destination').css('z-index','90');

        $("li.notification-cta.active, a.notification-label, .notification-wrap").removeClass("change-index");

    }
    // close open panels
    if( $( '[id*="panel-"].open' ).length !== 0 ) {
        cercaVoloPrenotaOptions.closeAllCollapsablePanels();
    }
    //close all suggestions
    $(".toggle-all-destination-desk").addClass("hide");
    $('#ui-datepicker-div').hide();

    // clear feedback error
    $( '.section--cerca-volo .has-input-error, .section--cerca-volo .is-invalid-input' ).removeClass( 'has-input-error is-invalid-input' );
    $( '.feedback-error' ).html('').removeClass( 'feedback-error' );
    $( '.labelError' ).hide();
    $( '.section--cerca-volo input[type="text"]').val('');

    // clear hidden fields
    cercaVoloOptions.clearHiddenFormAllValues();
    cercaVoloOptions.renderFrontendAllInputs();

    // reset passengers
    $( '#adult-passenger--prenota-mobile' ).val('1').trigger('change');
    $( '#young-adult-passenger--prenota-mobile, #kids-passenger--prenota-mobile, #baby-passenger--prenota-mobile' ).val('0').trigger('change');
    $( '#panel-travel-options .minus' ).attr('disabled', 'disabled');

    // reset radio
    var radioWrap = $( '.section--cerca-volo .radio-wrap' );
    $( '#switch-to--prenota-mobile', radioWrap).trigger('click');
    $( '#booking_code', radioWrap).trigger('click');
    $( '#flight_status', radioWrap).trigger('click');
    $( '#economy--prenota-mobile', radioWrap).trigger('click');

    cercaVoloOptions.desktopOverlayOpened = 0;

    $(".partenza-destinazione .input-wrap").removeClass('selected');
    clickFromCloseOverlay=true;
    //populateDepartureInput(null);  //sostituire NULL con aeroporto geolocalizzato
    getAirportsList();
    $('div.partenza-destinazione a.button').removeClass('hide').addClass('hide');


    //resetDatePicker
    prenotaDataOptions.startDate=undefined;
    prenotaDataOptions.endDate=undefined;
};

cercaVoloOptions.inputGoupFocus = function() {
    $( '.section--cerca-volo .custom-input-group input' ).on( 'focus', function() {
        $( this ).closest( '.custom-input-group' ).find( '.input-wrap' ).removeClass( 'selected not-selected' ).addClass( 'not-selected' );
        $( this ).parent().removeClass( 'not-selected' ).addClass( 'selected' );
    } );
};

cercaVoloOptions.updateHiddenFormAllValues = function( callback ) {
    // set all hidden input value
    $( '[data-ref]' ).each( function( i, el ) {
        if( $( el ).val() === '' || $( el ).attr( 'type' ) == 'radio' ) {
            $( $( el ).data( 'ref' ) ).val( 'not-set' );
        } else {
            $( $( el ).data( 'ref' ) ).val( $( el ).val() );
        }
        // set all placeholder text to span
        $( el ).parent().find( '.input-placeholder' ).text( $( el ).parent().find( '.input-placeholder' ).data( 'placeholder' ) );
    } );
    // set checked radio value to hidden form
    /*$( '#roundTrip--prenota' ).val( $( '[data-ref="#roundTrip--prenota"][checked]' ).val() );
    $( '#seatType--prenota' ).val( $( '[data-ref*="seat"][checked]' ).val() );*/
    $( '#roundTrip--prenota' ).val( $( '[data-ref="#roundTrip--prenota"]:checked' ).val() );
    $( '#seatType--prenota' ).val( $( '[data-ref*="seat"]:checked' ).val() );

    $(".cta-close-panel").click(function() { if(!cercaVoloCrea.showHistogram) $(".histogram-toggle-wrap").hide(); });

    //set on click focus on departure and arrival input text
    $( "#luogo-partenza--prenota-desk" ).on("click",function() {$( this ).select();});
    $( "#luogo-arrivo--prenota-desk" ).on("click",function() {$( this ).select();});

    //set on click MOBILE
    $( ".input-placeholder").on("click",function() {$( "#luogo-partenza--prenota-mobile" ).select();});
    $( ".input-placeholder" ).on("click",function() {$("#luogo-arrivo--prenota-mobile" ).select();});

    if( typeof callback == 'function' ) {
        callback();
    }
};

cercaVoloOptions.clearHiddenFormAllValues = function( callback ) {
    // bind round type
    $( '[id*="hiddenForm"] input' ).each( function( i, el ) {
        // empty hidden form
        if($(el).attr("id") != "CUG--prenota" ){
            $( el ).val( 'not-set' );
        }

        // empty frontend input value
        $( '[data-ref="#' + $( el ).attr( 'id' ) + '"]' ).val( '' );
    } );

    if( typeof callback == 'function' ) {
        callback();
    }
};

cercaVoloOptions.renderFrontendAllInputs = function(  ) {
    // alert()
    $( '[id*="hiddenForm"] input' ).each( function( i, el ) {
        if( $( el ).val()  == 'not-set' || $( el ).val()  === '' ) {
            // if val = 0 reset placeholer span
            $( '[data-ref="#' + $( el ).attr( 'id' ) + '"]' ).parent().find( '.input-placeholder' ).text( $( '[data-ref="#' + $( el ).attr( 'id' ) + '"]' ).parent().find( '.input-placeholder' ).data( 'placeholder' ) );
        } else {
            if ( $( el ).attr('id') != "departureDate--prenota" &&  $( el ).attr('id') != "returnDate--prenota"){
                // populate span placeholder with value
                $( '[data-ref="#' + $( el ).attr( 'id' ) + '"]' ).parent().find( '.input-placeholder' ).text( $( el ).val() );
            }
        }
    } );
};

cercaVoloOptions.updateHiddenFormOnChange = function( $this, cb ) {
    if( $this.val() === '' ) {
        $( $this.data( 'ref' ) ).val( 'not-set' );
        var handleCodeHiddenForm = (
            $this.data( 'ref' ).indexOf('#departureAirport') >= 0 ||
            $this.data( 'ref' ).indexOf('#arrivalAirport') >= 0
        );

        if( handleCodeHiddenForm ) {
            $( $this.data( 'ref' ).replace( '--', 'Code--' ) ).val( 'not-set' );
        }
    } else {
        // change val of departure or arrival input
        if( $this.data('ref').indexOf( 'departureAirport' ) !== -1 || $this.data('ref').indexOf( 'arrivalAirport' ) !== -1 )
        {

            // // check if current IATA different from hidden IATA && isValidIataCode
            var newIata = $this.val().slice(-3);
            var hiddenIata = $( $this.data( 'ref' ).replace( '--', 'Code--' ) ).val();

            if( typeof newIata !== 'undefined' && typeof hiddenIata !== 'undefined' && ( newIata.toLowerCase() !== hiddenIata.toLowerCase() ) && cercaVoloOptions.isValidCurrentIata( newIata ) ) {
                $( $this.data( 'ref' ) ).val( newCity + ' ' + newIata.toUpperCase() );
                $( $this.data( 'ref' ).replace( '--', 'Code--' ) ).val( newIata.toUpperCase() )
            }

        } else if (!$this.hasClass('hasDatepicker'))
            $( $this.data( 'ref' ) ).val( $this.val() );
    }
    cercaVoloOptions.renderFrontendThisInput( $this );
};

cercaVoloOptions.renderFrontendThisInput = function( $this ) {

    if( $this.attr( 'id' ) !== undefined ) {
        cercaVoloOptions.currentInputId = $this.attr( 'id' ).substr( 0, $this.attr( 'id' ).lastIndexOf( '-' ) );
    }

    if ($this.hasClass('hasDatepicker'))
    {
        if( $this.val()  == 'not-set' || $this.val()  === '' ) {
            // if val = 0 reset placeholer span
            $( '[id*="' + cercaVoloOptions.currentInputId + '"]' ).parent().find( '.input-placeholder' ).text( $this.parent().find( '.input-placeholder' ).data( 'placeholder' ) );
        } else {
            // populate span placeholder with value
            $( '[id*="' + cercaVoloOptions.currentInputId + '"]' ).parent().find( '.input-placeholder' ).text( $this.val() );
        }
    } else {
        if( $($this.data('ref')).val()  == 'not-set' || $($this.data('ref')).val()  === '' ) {
            $( '[id*="' + cercaVoloOptions.currentInputId + '"]' ).parent().find( '.input-placeholder' ).text( $this.parent().find( '.input-placeholder' ).data( 'placeholder' ) );
        } else {
            // populate span placeholder with value
            $( '[id*="' + cercaVoloOptions.currentInputId + '"]' ).parent().find( '.input-placeholder' ).text( $( $this.data( 'ref' ) ).val() );
        }
    }
};

cercaVoloOptions.handleHistogramStatus = function( $this ) {
    if( cercaVoloOptions.isValidHistogram() ) {
        cercaVoloPrenotaOptions.openCollapsablePanel( $('#panel-histogram') );
    }
};

cercaVoloOptions.isValidHistogram = function( $this ) {

    if( Foundation.MediaQuery.atLeast( 'large' )) {

        cercaVoloOptions.isValidDepCode = $('#departureAirportCode--prenota').val() === $('#departureAirportCode--prenota').val().toUpperCase() && $('#departureAirportCode--prenota').val().length == 3;
        cercaVoloOptions.isValidArrCode = $('#arrivalAirportCode--prenota').val() === $('#arrivalAirportCode--prenota').val().toUpperCase() && $('#arrivalAirportCode--prenota').val().length == 3;
        cercaVoloOptions.isDepDiffFromArr = $('#departureAirportCode--prenota').val() !== $('#arrivalAirportCode--prenota').val();

        if( cercaVoloOptions.isValidDepCode && cercaVoloOptions.isValidArrCode && cercaVoloOptions.isDepDiffFromArr ) {
            return true;
        }
    }
};
/*
cercaVoloOptions.isValidHistogram = function( $this ) {
	if( $('#departureAirport--prenota').val() !== 'not-set' && $('#arrivalAirport--prenota').val() !== 'not-set' && $( '#departureDate--prenota' ).val() == 'not-set' && $( '#returnDate--prenota' ).val() == 'not-set' && cercaVoloOptions.histogramCounter === 0 && cercaVoloCrea.showHistogram) {
		cercaVoloOptions.histogramCounter = 1;
		return true;
	}
};*/

// open loader
cercaVoloOptions.enableLoader = function() {
    $('.cerca-volo__inner').addClass('loading');
};

// close loader
cercaVoloOptions.disableLoader = function() {
    $('.cerca-volo__inner').removeClass('loading');
};

cercaVoloOptions.calculateAccordionTop = function() {

    cercaVoloOptions.accordionTitleOffset = [];

    if ($( '.cerca-volo__tab--prenota .accordion-title' ).length) {

        var firstTitleOffset = $( '.cerca-volo__tab--prenota .accordion-title' ).offset().top;
        var firstItemHeight = $( '.cerca-volo__tab--prenota .accordion-title' ).outerHeight();

        for( var i = 0; i < $( '#cerca-volo__widget .accordion-title' ).length; i++ ) {
            cercaVoloOptions.accordionTitleOffset.push( firstTitleOffset + ( firstItemHeight * i ) );
        }
    }
};
function initAccordionTab(cb) {
    cercaVoloOptions.accordionTab = new Foundation.ResponsiveAccordionTabs( $( '#cerca-volo__widget' ), {} );

    if( typeof cb == 'function' ) {
        cb();
    }
}

function initAccordionTabCb() {
    $('.cerca-volo').removeClass('is-loading');
}

cercaVoloOptions.isValidCurrentIata = function( IataToValidate ) {
	try {
		if(typeof(allAirports) != 'undefined') {
			for (var airportIndex=0; airportIndex < allAirports.airports.length; airportIndex++) {

		        var currentIataCode = allAirports.airports[ airportIndex ].code;

		        if( currentIataCode.toLowerCase() == IataToValidate.toLowerCase() ) {
		            newCity = allAirports.airports[ airportIndex ].city;
		            return true;
		        }
		    }
		}

	} catch (err) {
		console.log('Impossible to execute isValidCurrentIata');
	}

}