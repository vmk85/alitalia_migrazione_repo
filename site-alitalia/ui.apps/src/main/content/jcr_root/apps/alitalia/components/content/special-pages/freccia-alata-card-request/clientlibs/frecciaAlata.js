jQuery(document).ready(function(){
	jQuery('#frecciaalataSubmit').on('click', frecciaAlataHandler);
});

function frecciaAlataHandler(e) {
	validation(e, 'frecciaalatarequestsubmit', '#specialpageform_frecciaalatarequestform', frecciaAlataSuccess, frecciaAlataError);
}

function frecciaAlataSuccess(data) {
	return (data.result ? frecciaAlataContinue() : showErrors(data));
}

function frecciaAlataError() {
	return frecciaAlataContinue(true);
}

function frecciaAlataContinue(stop) {
	removeErrors();
	jQuery('#frecciaalataSubmit').off('click', frecciaAlataHandler);
	return stop || jQuery('#frecciaalataSubmit').click();
}
