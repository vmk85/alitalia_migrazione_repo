var sn1 = "";
var sh1 = "";
var act1 = "";

$(document).ready(function() {
    closeModal = true;

    feedbackRow = $('.notification-row');

    // var params = {
    //     showTermsLink:false,
    //     enabledProviders: "facebook, linkedin, googleplus",
    //     containerID: "socialEdit-wrapper"
    // };
    //
    // gigya.socialize.showEditConnectionsUI(params);

    var requestData = {
        "linguaCorrispondenza": null
    };
    getDropDownData(requestData, getDropDownDataSuccess, getDropDownDataFail);

    // $("#saveResetMode").on("click", function() {
    //     gigya.accounts.setAccountInfo({
    //         data: { resetPwdSms: $("[name='security']:checked").val() },
    //         callback: function(e) {
    //             if(e.errorCode == 0) {
    //                 saveUserMAToModel();
    //                 $("[data-close]").trigger("click");
    //             }
    //             console.log(e);
    //         }
    //     });
    // });

    $("#needUserSettings").find(".close-button").removeClass("hide");
    $(".NUSLogout").addClass("hide");

});

$(document).on("click", "[data-auth]", function(event) {
    var current = $(this).attr("data-auth");
    if(current>3)$("[data-auth]:eq("+(current-1)+")").trigger("click");
});


function getDropDownData(requestData, done, fail, always) {
    return invokeGenericFormService('staticdatalistservlet', 'GET', '', done, fail, always, 'json', requestData);
}

function getDropDownDataSuccess(data) {


    try {
        for(i in data.linguaCorrispondenza) {
            // if(defaultSelectedLinguaCorrispondenza == data.linguaCorrispondenza[i].value){
            //     $("#linguaCorrispondenza").append($("<option>", {
            //         value: data.linguaCorrispondenza[i].value,
            //         text: data.linguaCorrispondenza[i].name,
            //         selected: true
            //     }));
            // }

                $("#linguaCorrispondenza").append($("<option>", {
                    value: data.linguaCorrispondenza[i].value,
                    text: data.linguaCorrispondenza[i].name
                }));


        }
        $("#linguaCorrispondenza").val(getLingua(linguaComunicazioniCommerciali));

    } catch (err) {
        console.log("Errore nel chiamata [GetDropDrownData]");
        console.log(err);
    }


    // performSubmit("GetMyAlitaliaCRMDataToSession","",Login.Session.success.optionConsensi,Login.Session.error.getCrmData);

}

function getDropDownDataFail() {
    alert("Errore nel chiamata [GetDropDrownData]");
}

function modifyPasswordSuccess(response) {
    if (!response.isError){
        myAlitaliaOptions.feedbackSuccess();
        $("#annullabut").click();
        disableLoaderMyAlitalia();
    } else {
        myAlitaliaOptions.feedbackFailPassword();
        disableLoaderMyAlitalia();

    }
}

function modifyPasswordError() {
    myAlitaliaOptions.feedbackFailPassword();
    disableLoaderMyAlitalia();
}

function modifyPassword() {
    $("#errorPass").addClass("hide").text("");

    if (validatePsw($("input[name='newP']").val().trim())) {

        enableLoaderMyAlitalia();
        var oldP = $("[name='currentP']").val() || null;
        var newP = $("[name='newP']").val() || null;
        if(oldP != null && newP != null) {
            var gigyaData = {
                gigya : JSON.stringify({
                    password: oldP,
                    newPassword: newP
                })
            }
            performSubmit("SetMyAlitaliaPassword",gigyaData,modifyPasswordSuccess,modifyPasswordError);
        }
    } else {
        $("#errorPass").removeClass("hide").text(CQ.I18n.get("myalitalia.invalid.psw"));
    }

}

$(".editAuth").on("click", function() {
    var par = $(this).parents("ul").find(".action--expandible");
    if(par.hasClass("expanded")) par.removeClass("expanded");
    else par.addClass("expanded");
});

function socialProfileSucces(response) {

    if (!response.isError){
        var uri = location.origin + internalUrl1.substring(0, internalUrl1.length-"homepage".length) + "myalitalia/popup.html?associa=true";
        var popup = window.open(encodeURI("https://socialize.eu1.gigya.com/socialize.addConnection?oauth_token=" + response.cookie + "&redirect_uri=" + uri + "&x_provider=" + sn1
            + "&response_type=token&x_sessionExpiration=3600"), "MsgWindow", "width=500,height=500");
    }
}

function socialProfileRefreshSuccess(response) {
    if (!response.isError) {
        var data = JSON.parse(response.data);
        setGigyaCookie(data.cookieName, data.cookieValue, 1);
        var url = "";
        var nikname = "";
        for (var i = 0; i < data.identities.length; i++) {
            if (data.identities[i].provider == sn1) {
                url = data.identities[i].photoURL;
                nikname = data.identities[i].nikname;
            }

        }
        refreshSocialConnection(sn1, sh1, act1, url, nikname);
    } else {
        disableLoaderMyAlitalia();
    }
}

function socialProfileRefreshError() {
    disableLoaderMyAlitalia();
}

function socialProfileError() {


}

function socialProfileDeleteError() {
    disableLoaderMyAlitalia();
}

function socialProfileDeleteSucces(response) {
    if (!response.isError){
        refreshSocialConnection(sn1, sh1, act1,null,null);
        disableLoaderMyAlitalia();
    } else {
        disableLoaderMyAlitalia();
        alert("errore dissociazione account");
    }
}

function socialProfile(sn, sh, act, img, nick) {
    sn1 = sn;
    sh1 = sh;
    act1 = act;

    // var par = {
    //     provider: sn,
    //     callback: function(resp) {
    //         console.log(resp);
    //         if(resp.errorCode == 0) {
    //             if(act) refreshSocialConnection(sn, sh, act, resp.user.identities[sn].photoURL, resp.user.nickname);
    //             else refreshSocialConnection(sn, sh, act, null, null);
    //         }
    //
    //
    //     }
    // };
    // if(act) gigya.socialize.addConnection(par);
    // else gigya.socialize.removeConnection(par);
    if (act){
        performSubmit("GetGigyaCookie","",socialProfileSucces,socialProfileError);
    } else {
        enableLoaderMyAlitalia();
        var data = {
            provider : sn
        }
        performSubmit("deleteAccountAssociated",data,socialProfileDeleteSucces,socialProfileDeleteError);
    }
}

function socialConnect() {
    enableLoaderMyAlitalia();
    performSubmit("MyAlitaliaServlet-UpdateSession","",socialProfileRefreshSuccess,socialProfileRefreshError);
}

function refreshSocialConnection(provider, sh, action, img, nick) {
    $("#" + provider).remove();
    if(action) $(".manage-social").append(createItemConnected(provider, sh, img, nick));
    else $(".manage-social").prepend(createItemNotConnected(provider, sh, img, nick));
}

function createItemConnected(provider, sh, img, nick) {
    CQ.I18n.setLocale(languageCode.toUpperCase());
    var str = "";
    str += "<li class='clearfix' id='" + provider + "'>";
    str += "  <span class='icon " + sh + "'></span>";
    str += "  <div class='action'>";
    str += '    <img class="action__img" src="' + img + '" width="40" height="40" alt="immagine profilo">';
    str += '    <div class="action__name">';
    str += '      <span>' + nick + '</span>';
    str += '      <a href="javascript:socialProfile(&quot;' + provider + '&quot;, &quot;' + sh + '&quot;, false);" id="link-' + sh + '">' + CQ.I18n.get('myalitalia.impostazioni.action.socialLogin.disconnettiProfilo')  + '</a>';
    str += '    </div>';
    str += '  </div>';
    str += '</li>';
    disableLoaderMyAlitalia();
    return str;
}

function createItemNotConnected(provider, sh, img, nick) {
    var str = "";
    str += "<li class='clearfix' id='" + provider + "'>";
    str += "  <span class='icon " + sh + "'></span>";
    str += "  <div class='action action--connect'>";
    str += '      <a href="javascript:socialProfile(&quot;' + provider + '&quot;, &quot;' + sh + '&quot;, true, );" id="link-' + sh + '">' + CQ.I18n.get('myalitalia.impostazioni.action.socialLogin.connettiProfilo') + '</a>';
    ;
    str += '  </div>';
    str += '</li>';
    return str;
}

$("#btRemoveProfile").click(function(){

    enableLoaderMyAlitalia();

    // gigya.accounts.getAccountInfo({ callback: removeAccount });
    removeAccount()
});

function removeAccount(response)
{
    // if ( response.errorCode == 0 )
    // {
    //     var UID = response['UID'];
    //     console.log(UID);
    //
    //     data = {};
    //     data.email = response.profile.email;
    //     data.UID = UID;
    //     data.mailsUser = $("#mailsUser").text();
    //     data.templateMail = $("#templateMail").text();
    //
    //
    // } else {
    //     removeError(data);
    // }
    performSubmit('removeusersubmit', "", removeSucces, removeError);
}

function removeSucces(data) {
        if (!data.isError){
            disableLoaderMyAlitalia();
            myAlitaliaOptions.feedbackDelete("myalitalia.eliminaUtenza.feedback");
            // performMALogout();
            setGigyaCookie(data.cookieName,data.cookieValue,-1);
            setTimeout(function(){
                redirect('homepage', '');
                }, 3000);
        } else {
            disableLoaderMyAlitalia();
            // console.log(data);
            myAlitaliaOptions.feedbackDeleteError("myalitalia.eliminaUtenza.feedbackerror");
        }
}

function removeError(data) {
    console.log(data);
    myAlitaliaOptions.feedbackDeleteError("myalitalia.eliminaUtenza.feedbackerror");
    disableLoaderMyAlitalia();
}