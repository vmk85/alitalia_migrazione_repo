function getUserData(){
	$.ajax({
		url: removeUrlSelector() + ".userdatacarnet.json",
		data : {
        _isAjax: true
		},
		context : document.body
	})
	.done(successUserData)
	.fail(failUserData);
	return false;
}

function successUserData(data){
	if(data.userData){
		var text = $('#numeroVoliCarnet').text();
		$('#numeroVoliCarnet').html(data.userData[1].user.numeroVoli + ' ' + text);
	    $('#scadenzaVoliCarnet').append(' ' + data.userData[1].user.scadenzaCarnet);
	    $('#nomeUtenteCarnet').append(' ' + data.userData[1].user.utente);
        if(data.userData[1].user.numeroVoli <= 0){
            $('#errorMessageEmptyFlights').show();
            $('#cercaVoliSubmit').remove();
            $('#adult').val(0);
        }
	} else if(data.isError){
        console.log("error userData ");
        if(data.redirect){
        	window.location.replace(data.redirect);
        }
	}
}

function failUserData(){
	console.log("error getUserData");
}

function logoutCarnetSubmit(){
    console.log("logoutSubmit");
    $('#logoutCarnetSubmit').click();
}

function checkErrorNoFlight(){
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    idError,
    i;
	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] === 'resultError') {
			if(sParameterName[1] === 'noFlights'){
				$("#errorMessageNoSearch").show();
			}
		}
	}
}

$(document).ready(function () {
    getUserData();
    checkErrorNoFlight();
    $('#logoutCarnetButton').on('click', logoutCarnetSubmit);
});