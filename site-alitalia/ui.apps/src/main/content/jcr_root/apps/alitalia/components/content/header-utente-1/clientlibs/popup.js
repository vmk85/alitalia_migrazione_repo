var isResetOtpPsw = false;
var token = "";
var provider = "";

function isAvailableIdResetSuccess(response) {
    if(!response.isError){
        var data = JSON.parse(response.data);
        resetGenericErrors("#emailForReset", "#labelErrorLogin-ma-reset");
        disableLoaderMyAlitalia();
        if (data.numero != undefined){
            isResetOtpPsw = true;
            closeModal = true;
            $(".cotpDescr").text(CQ.I18n.get("myalitalia.OTP.modal.SMStitle", data.numero));
            $("[data-open='ma-check-otp']").trigger("click");
            $(".titleCOTP").text(CQ.I18n.get("myalitalia.otp.resetPassowrd.title"));
        } else {
            setGigyaCookie("emailresetsuccess",$("#emailForReset").val(),1);
            disableLoaderOtp();
            $("a[data-open='success-reset']").trigger("click");
        }
    } else {
        if (response.errorMessage == "myalitalia.notValidEmail"){
            setGenericErrors("#emailForReset", "#labelErrorLogin-ma-reset", CQ.I18n.get("myalitalia.invalid.credential"));
            disableLoaderMyAlitalia();
        } else {
            $("a[data-open='contactHelpDesk']").trigger("click");
            disableLoaderMyAlitalia();
        }
     }
}

function isAvailableIdErrorSuccess() {
    setGenericErrors("#emailForReset", "#labelErrorLogin-ma-reset", CQ.I18n.get("myalitalia.isNotEmail"));
    disableLoaderMyAlitalia();
}

function linkAccount(e) {
    e.preventDefault();
    enableLoaderMyAlitalia();
    if(isValidFormLinkAccount()) {
        $("#socialLinkAccountLogin").show();
        $("[data-provider='googleplus']").show();
        $("[data-provider='facebook']").show();
        $("[data-provider='linkedin']").show();
        $("#pswLoginLinkAccount").show();
        $("#btnLoginLinkAccount").show();
        $("#webLinkAccountLogin").show();
        $("#fullLinkAccountLogin").show();
        $("#myalitalia-connect-social .icon.icon--close").click();
        var dataLogin = {
            authToken: token,
            password: $("#userPWDLinkAccount").val()
        };
        performSubmit("MyAlitaliaServlet-LinkAccount", dataLogin, Login.Session.successLinkAccount, Login.Session.errorLinkAccount);
    }
}

function socialConnectLinkAccount (){
    provider = null;
    performSubmit("MyAlitaliaServlet-UpdateSession","",Login.Session.refreshSuccess,Login.Session.refreshError);
}

function socialConnectLinkAccountSocial(){
    provider = null;
    performSubmit("MyAlitaliaServlet-UpdateSession","",Login.Session.refreshSuccess,Login.Session.refreshError);
}

$(document).ready(function() {
    $("#gigya-link-account-screen").submit(function(e){
        linkAccount(e);
    });
    $("#callMaLogin").submit(function(e){
        callMALogin(e);
    });
	$('.reveal--myalitalia-login').foundation();
	$('.reveal--success').foundation();
	$(".accesso-singolo").foundation();
    $(".reveal--myalitalia").foundation();
    //	$('.reveal--millemiglia-login').foundation();
	$('.reveal--ma-o-mm-login').foundation();
	$('.ma-o-mm-login').foundation();


    $(".toggle-password").click(function() {

      $(this).toggleClass("eye eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });

    $(".sendResetPassword").on("click", function(){
        closeModal = true;
        enableLoaderMyAlitalia();
        if($("#emailForReset").val() != "" && isEmail($("#emailForReset").val())) {
            resetGenericErrors("#emailForReset", "#labelErrorLogin-ma-reset");
            var data = {
                email:$("#emailForReset").val()
            };
            performSubmit("MyAlitalia-resetPsw",data,isAvailableIdResetSuccess,isAvailableIdErrorSuccess);
            // gigya.accounts.isAvailableLoginID({
            //     loginID: $("#emailForReset").val(),
            //     callback: function(event) {
            //         if(event.isAvailable) setGenericErrors("#emailForReset", "#labelErrorLogin-ma-reset", CQ.I18n.get("myalitalia.notValidEmail"));
            //         else {
            //             resetGenericErrors("#emailForReset", "#labelErrorLogin-ma-reset");
            //             gigya.accounts.resetPassword({
            //                 loginID: $("#emailForReset").val(),
            //                 secretAnswer: "secretA",
            //                 callback: function(e) {
            //                     console.log(e);
            //                     if(e.status == "OK") {
            //                         resetGenericErrors("#emailForReset", "#labelErrorLogin-ma-reset");
            //                         console.log("Email Inviata");
            //                         $("a[data-open='success']").trigger("click");
            //                     } else {
            //                         console.log("Errore nel reset della password");
            //                         setGenericErrors("#emailForReset", "#labelErrorLogin-ma-reset", e.errorDetails);
            //                     }
            //                 }
            //             });
            //         }
            //     }
            // })
        } else {
            if($("#emailForReset").val() == "") setGenericErrors("#emailForReset", "#labelErrorLogin-ma-reset", CQ.I18n.get("myalitalia.fieldObligatory"));
            else if(!isEmail($("#emailForReset").val()))setGenericErrors("#emailForReset", "#labelErrorLogin-ma-reset", CQ.I18n.get("myalitalia.isNotEmail"));
            disableLoaderMyAlitalia();
        }
    });

    $(".rememberMeDataMA").on("click", function() { $(this).toggleClass("checked"); })

    rememberData();

    $(".manageSL").on("click", function(e) {
        var user = $(this).attr("data-user");
        provider = $(this).attr("data-provider");
        var ak, uri;
        if(user == "ma") {
            ak = maAk;
            uri = location.origin + internalUrl1.substring(0, internalUrl1.length-"homepage".length) + "myalitalia/popup.html";
            var popup = window.open(encodeURI("https://socialize.eu1.gigya.com/socialize.login?client_id=" + ak + "&redirect_uri=" + uri + "&x_provider=" + provider
            	  + "&response_type=token&x_sessionExpiration=3600"), "MsgWindow", "width=500,height=500");
        } else if(user == "mm") {
            ak = mmAk;
            uri = location.origin + internalUrl1.substring(0, internalUrl1.length-"homepage".length) + "popup.html";
            var popup = window.open(encodeURI("https://socialize.us1.gigya.com/socialize.login?client_id=" + ak + "&redirect_uri=" + uri + "&x_provider=" + provider
            	  + "&response_type=token&x_sessionExpiration=3600"), "MsgWindow", "width=500,height=500");
        } else return false;
    });

    $(".manageSLLink").on("click", function(e) {
        var providers = $(this).attr("data-provider");
        var ak, uri;
        ak = maAk;
        uri = location.origin + internalUrl1.substring(0, internalUrl1.length-"homepage".length) + "myalitalia/popup.html?loginLinkAccount=true";
        var popup = window.open(encodeURI("https://socialize.eu1.gigya.com/socialize.login?client_id=" + ak + "&redirect_uri=" + uri + "&x_provider=" + providers
        + "&response_type=token&x_sessionExpiration=3600"), "MsgWindow", "width=500,height=500");
    });

});

function isValidForm() {
    var email = $("#userID");
    var emailError = $("#labelErrorLogin-ma-user").find("p");
    var pass = $("#userPWD");
    var passError = $("#labelErrorLogin-ma-pwd").find("p");
    var valid = true;
    if(email.val() == "") { setEmailError(CQ.I18n.get("myalitalia.fieldObligatory")); valid = false;}
    else {
        if(!isEmail(email.val())) {setEmailError(CQ.I18n.get("myalitalia.isNotEmail")); valid = false;}
        else resetEmailError();
    }
    if(pass.val() == "") { setPassErrorMa(CQ.I18n.get("myalitalia.fieldObligatory")); valid = false; }
    else resetPassErrorMa();

    return valid;
}

function isValidFormLinkAccount() {
    var pass = $("#userPWDLinkAccount");
    var passError = $("#labelErrorLogin-ma-pwdLinkAccount").find("p");
    var valid = true;
    if(pass.val() == "") { setPassErrorMa(CQ.I18n.get("myalitalia.fieldObligatory")); valid = false; }
    else resetPassErrorMa();

    return valid;
}

function setEmailError(txt) {
    $("#userID").addClass("is-invalid-input");
    $("#labelErrorLogin-ma-user").find("p").html(txt);
}

function setPassErrorMa(txt) {
    $("#userPWD").addClass("is-invalid-input");
    $("#labelErrorLogin-ma-pwd").find("p").html(txt);
}

function resetEmailError() {
    $("#userID").removeClass("is-invalid-input");
    $("#labelErrorLogin-ma-user").find("p").html("");
}

function resetPassErrorMa() {
    $("#userPWD").removeClass("is-invalid-input");
    $("#labelErrorLogin-ma-pwd").find("p").html("");
}

function setGenericErrors(inp, lab, text) {
    $(inp).addClass("is-invalid-input");
    $(lab).find("p").html(text);
    $(lab).removeClass("hide");
}

function resetGenericErrors(inp, lab) {
    $(inp).removeClass("is-invalid-input");
    $(lab).find("p").html("");
    $(lab).addClass("hide");
}

function loginMASuccess(data) {
    console.log(data);
}

function loginMAFailure(data) {
    console.log(data);
    if(data.errorCode == "206002") {
        $("[data-open='ma-pending-verification']").trigger("click");
    }
    $("#userPWD").addClass("is-invalid-input");
    var mess = data.errorDetails;
    if(data.errorDetails == 'invalid loginID or password')
        mess = 'myAlitalia.passwordErrata';

    $("#labelErrorLogin-ma-pwd").find("p").html(CQ.I18n.get(mess));
    disableLoaderMyAlitalia();
}

function isEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

//var socialClick = "";

function callbackSocialLoginMM(ev) {
    console.log("Callback Social Login MM");
    console.log(ev);
    var UID, UIDSig, provider, firstName, lastName, img;
    UID = ev.UID;
    UIDSig = ev.UIDSig;
    provider = ev.user.providers[0];
    firstName = ev.user.fistName;
    lastName = ev.user.lastName;
    img = ev.user.thumbnailURL;
    startLoginWithGigyaCredentials(UID, UIDSig, provider, firstName, lastName, img);
    // https://socialize.us1.gigya.com/socialize.getUserInfo?apiKey=3_4r6kgvXIafmrDHkQtQJthcXZh05rY0cHRSfg-H9pz-mK8v-zpaCDn6XnsuXPPqJ_&regToken=st2.08MiJdtOEZVLViZJFdrfVKF3MWc.SDhyMAg1QeNOOiv1eegfdw._N5y5QGukVjI7z92Za-Z3p_wYSs
}

function callbackSocialLogin(ev) {
    console.log(ev);
    token = ev.regToken;
    switch (ev.errorCode) {
        case 206003:
        case 206001:
            $("#socialLoginRegistrationSubmit").attr("data-token",token);
            $('[data-open="ma-pending-registration"]').trigger("click");


            //account pending registration
            //getAccountInfo tramite regToken (parametro da passare rinominare in access_token
            //     gigya.accounts.showScreenSet({
            //         screenSet : "MA-RegistrationLogin",
            //         startScreen : "gigya-complete-registration-screen",
            //         regToken : ev.regToken,
            //         onError : function(err) {
            //           console.log(err);
            //         },
            //         onAfterScreenLoad:function(ev){
            //             // Mostro la modale di registrazione
            //             $('[data-open="ma-pending-registration"]').trigger("click");
            //             SARegistrationflag = true;
            //             disableLoaderMyAlitalia();
            //         },
            //         onBeforeSubmit: function(e) {
            //             console.log("Before Submit");
            //         },
            //         onAfterSubmit : function(sub) {
            //             enableLoaderMyAlitalia();
            //             if (sub.response.errorCode == 0) {
            //                 flagAccettazioneCRM.push(flagAccettazioneCRMnewsletter);
            //                 flagAccettazioneCRM.push(flagAccettazioneCRMprofiling);
            //                 flagAccettazioneCRM.push(flagAccettazioneCRMprofilingOld);
            //                 var crmRegistration =
            //                     {
            //                         data: JSON.stringify(
            //                             {
            //                                 UID: sub.response.UID,
            //                                 profile: sub.profile,
            //                             }),
            //                         listaConsensi: JSON.stringify({
            //                             listaConsensi: flagAccettazioneCRM
            //                         })
            //                     };
            //
            //                 /** Invio i dati alla servlet per censire il nuovo utente */
            //                 performSubmit("RegistrerMyAlitaliaCrmUser", crmRegistration, Login.Session.success, Login.Session.error);
            //             }
            //
            //             console.log(sub);
            //           var finalizeRegParams = {
            //             regToken : ev.regToken
            //           };
            //           gigya.accounts.finalizeRegistration(finalizeRegParams);
            //           var params = {
            //             screenSet : "gigya-complete-registration-screen"
            //           };
            //           gigya.accounts.hideScreenSet(params);
            //           $("#ma-pending-registration .close-button").click();
            //         },
            //         containerID : "reset"
            //     });
              break;
        case 403043: // conflicting account

            enableLoaderMyAlitalia();
            var dataLogin = {
                authToken: token
            };
            performSubmit("MyAlitaliaCheckLinkAccount",dataLogin,Login.Session.successCheckLinkAccount,Login.Session.errorCheckLinkAccount);
                // console.log("LINK ACCOUNT");
                // gigya.accounts.showScreenSet({
                //       screenSet : "MA-LinkAccounts",
                //       startScreen : "gigya-link-account-screen",
                //       regToken : ev.regToken,
                //       onError : function(err) {
                //         console.log(err);
                //       },
                //     onAfterScreenLoad:function(ev){
                //         // Mostro la modale di link account
                //         $('[data-open="myalitalia-connect-social"]').trigger("click");
                //     },
                //     onAfterSubmit : function(sub) {
                //         enableLoaderMyAlitalia();
                //         if (sub.response.errorCode == 0) {
                //             flagAccettazioneCRM.push(flagAccettazioneCRMnewsletter);
                //             flagAccettazioneCRM.push(flagAccettazioneCRMprofiling);
                //             flagAccettazioneCRM.push(flagAccettazioneCRMprofilingOld);
                //             var crmRegistration =
                //                 {
                //                     data: JSON.stringify(
                //                         {
                //                             UID: sub.response.UID,
                //                             profile: sub.profile,
                //                         }),
                //                     listaConsensi: JSON.stringify({
                //                         listaConsensi: flagAccettazioneCRM
                //                     })
                //                 };
                //
                //             /** Invio i dati alla servlet per censire il nuovo utente */
                //             performSubmit("RegistrerMyAlitaliaCrmUser", crmRegistration, Login.Session.success, Login.Session.error);
                //         }
                //         console.log(sub);
                //         var finalizeRegParams = {
                //           regToken : ev.regToken
                //         };
                //         gigya.accounts.finalizeRegistration(finalizeRegParams);
                //         var params = {
                //           screenSet : "gigya-complete-registration-screen"
                //         };
                //         gigya.accounts.hideScreenSet(params);
                //         $("#ma-pending-registration .close-button").click();
                //     },
                //     containerID : "linkAccountMA"
                // });
            break;
        default:
            enableLoaderMyAlitalia();
            var finalizeRegParams = {
                authToken: ev.access_token
            };
            // gigya.accounts.finalizeRegistration(finalizeRegParams);
            performSubmit("MyAlitaliaServlet-loginMa",finalizeRegParams,Login.Session.success, Login.Session.error);
    }
}

function callbackSocialLoginLinkAccount(ev) {
    enableLoaderMyAlitalia();
    console.log(ev);
    token = ev.regToken;
    var finalizeRegParams = {
        authToken: ev.access_token
    };
    // gigya.accounts.finalizeRegistration(finalizeRegParams);
    performSubmit("MyAlitaliaServlet-loginMa",finalizeRegParams,Login.Session.successLinkAccountSocial, Login.Session.error);

}

//function socialLogin(sn) {
//  socialClick = sn;
//  var apiKey = gigya.thisScript.APIKey;
//  var redirect_uri = location.origin + internalUrl1.substring(0, internalUrl1.length-"homepage".length) + "myalitalia/popup.html";
//  var popup = window.open(encodeURI("https://socialize.eu1.gigya.com/socialize.login?client_id=" + apiKey + "&redirect_uri=" + redirect_uri + "&x_provider=" + sn
//	  + "&response_type=token&x_sessionExpiration=3600"), "MsgWindow", "width=500,height=500");
//    gigya.accounts.socialLogin({provider: sn, callback: function(e){console.log(e)}});
//}

function showLoginLoaderMA() {
    $(".maLogin-loader").show();
}

function hideLoginLoaderMA() {
    $(".maLogin-loader").hide();
}

function callMALogin(e) {
    e.preventDefault();
    enableLoaderMyAlitalia();
    if(isValidForm()) {
        // gigya.accounts.isAvailableLoginID({
        //     loginID: $("#userID").val(),
        //     callback: function(resp) {
        //         if(resp.isAvailable) {
        //             setEmailError(CQ.I18n.get("myalitalia.notValidEmail"));
        //             disableLoaderMyAlitalia();
        //
        //         } else {
        //             resetEmailError();
        //             resetPassError();
        //             doMALogin($("#userID").val(), $("#userPWD").val(), afterLogged, loginMAFailure);
        //         }
        //     }
        // });
        var dataLogin = {
            email: $( "#userID").val()
        }
        performSubmit("MyAlitalia-isAvailableId",dataLogin,Login.Session.check, Login.Session.error);

    } else {disableLoaderMyAlitalia();}}

function rememberData() {
    var user = JSON.parse(localStorage.getItem("rememberMeMA"));
    if(user != null) {
        $("#userID").val(user.email);
        $("#userPWD").val(user.pwd);
        $(".rememberMeDataMA").trigger("click");
    } else $(".rememberMeDataMA").removeClass("checked");
}

function afterLogged() {
    console.log(e);
    var user = {};
    if($(".rememberMeDataMA").hasClass("checked")) {
        user.email = $("#userID").val();
        user.pwd = $("#userPWD").val();
        localStorage.setItem("rememberMeMA", JSON.stringify(user));
    } else localStorage.removeItem("rememberMeMA");

    $("#userID").val(" ");
    $("#userPWD").val(" ");
}



