<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:a="http://www.alitalia.com/services/payment/contracts" exclude-result-prefixes="msxsl"> 
 <xsl:output method="html" indent="yes" encoding ="utf-8" /> 
 <xsl:key name="distE-Ticket" match="//a:rsPax/a:Tickets/a:rsTicket" use="substring(a:Number, 1,13)" /> 
 <xsl:template match="node() | *"> 
{HTML_MAIL_PLACEHOLDER}
 </xsl:template> 
</xsl:stylesheet>
