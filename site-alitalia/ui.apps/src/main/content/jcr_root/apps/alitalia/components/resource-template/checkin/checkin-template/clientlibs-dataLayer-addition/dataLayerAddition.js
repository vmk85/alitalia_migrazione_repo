var DTMRule = "";

function analytics_checkinPartialCallback(){
	addCheckinAnalyticsData(window['analytics_checkin_step']);
	addDataLayerVar();
	setWebtrendsMeta();
	cleanWebtrendsMeta();
	activateGTMOnce("checkin");
	activateWebtrendsOnce("checkin");
	if(DTMRule){
		activateDTMRule(DTMRule);
	}
}

	function addCheckinAnalyticsData(checkinStep){
		switch(checkinStep){
		case "1":
			DTMRule = "CInFlightList";
			break;
		case "2":
			DTMRule = "CInPassengerData";
			break;
		case "3":
			DTMRule = "CInAncillary";
			initCheckIn_ancillaryTracking();
			onCheckInTrackingBoxLoaded();
			break;
		case "4":
			DTMRule = "CInEMDPayment";
			initCheckIn_ancillaryTracking();
			break;
		case "5":
			DTMRule = "CInEMDReceipt";
			checkInAncillaryThankyou_setMeta();
			break;
		case "6":
			DTMRule = "CInBoardingPass";
			break;
		case "7":
			DTMRule = "CInComplete";
			break;
		default:
			console.log("Invalid checkin step");
		}	
		
	}

	function addDataLayerVar(){
		if(typeof window["analytics_dl_var"] == "object"){
			addAnalyticsData(window["analytics_dl_var"]);
		}
	}

	function setWebtrendsMeta(){
		if(typeof(window["analytics_wt_meta"])  !== "undefined"){
			for(key in window["analytics_wt_meta"]){
				jQuery("meta[name='"+key+"']").attr("content", window["analytics_wt_meta"][key]);
			}
		}
	}
	
	function cleanWebtrendsMeta(){
		jQuery("meta[name^='WT.'],meta[name^='DCSext.']").each(function(ind, elem){
			if($(elem).attr("content") == "?"){
				$(elem).attr("content", "");
			}
			else if($(elem).attr("content").length == 0){
				$(elem).remove();
			}
		});
	}
	
	
	function checkin_analyticsUpgradeChoosed(compClass, numUpgrade){
		$("#checkinSeatChoose").attr("data-click-evt", "conferma_upgrade");
		$("#checkinSeatChoose").attr("data-sku", "upgrade");
		$("#checkinSeatChoose").attr("data-qt", numUpgrade);
	}
	
	function analytics_trackCheckinErrors(errors){
		var errorMessages = "";
		/*compute error messages*/
		if(typeof errors === "object"){
			for(key in errors){
				errorMessages = errorMessages + key + " "  + errors[key] + "; ";
			}	
			if(errorMessages.length > 0)
				errorMessages = errorMessages.slice(0,-2);
		}
		else if (typeof errors === "string"){
			errorMessages = errors;
		}
		if(errorMessages.length > 0){
			/*Add or update meta tag*/
			if($("meta[name='DCSext.wci_error']").length ==  0){
				var htmlMeta = "<meta name='DCSext.wci_error' content=\""+errorMessages.replace(/"/g, "&#34;")+"\">";
				jQuery("head").append(htmlMeta);
			}
			else{
				$("meta[name='DCSext.wci_error']").attr("content", errorMessages);
			}
			pushAnalyticsData({"CInError": errorMessages});
			activateDTMRule("CInError");
		}
	}	
	
	
	function initCheckIn_ancillaryTracking(){
		/*Ancillary*/
		jQuery(".j-analytics-checkIn-evt").off("mousedown", onCheckInTrackingEvent);
		jQuery(".j-analytics-checkIn-evt").on("mousedown", onCheckInTrackingEvent);
		jQuery(".j-analytics-checkIn-click").off("mousedown", onCheckInTrackingClick);
		jQuery(".j-analytics-checkIn-click").on("mousedown", onCheckInTrackingClick);
		jQuery(".j-analytics-checkIn-product").off("mousedown", onCheckInTrackingProduct);
		jQuery(".j-analytics-checkIn-product").on("mousedown", onCheckInTrackingProduct);
		/*Payment*/
		jQuery(".j-analytics-checkIn-cc").off("mousedown", onMmbCCSelected);
		jQuery(".j-analytics-checkIn-cc").on("mousedown", onMmbCCSelected);
	}

	/**
	 * selezione carta di pagamento
	 */
	function onMmbCCSelected(){
		jQuery("meta[name='WT.si_x']").attr("content", "2");
	}
	
	/**
	 * click sui tab, sui tasti modifica e sugli accordion
	 */
	function onCheckInTrackingEvent(){
		var evtName = $(this).attr("data-evt");
		var stepName = $(this).attr("data-step");
		if(typeof window["checkIn_dcs_multitrack_event"] != "undefined"){
			checkIn_dcs_multitrack_event(evtName, stepName);
		}
	}

	/**
	 * click sui tasti "scegli" dei box
	 * e' possibile scatenare piu' di un evento separandoli con 
	 * una virgola
	 */
	function onCheckInTrackingClick(){
		var evtNames = $(this).attr("data-click-evt").split(",");
		var stepNames = $(this).attr("data-click-step").split(",");
		var boxNames = $(this).parents(".j-analytics-checkIn-box").attr("data-box").split(",");
		for(ind in evtNames){
			var evtName = evtNames[ind];
			var stepName = stepNames[ind % stepNames.length];
			var boxName = boxNames[ind % boxNames.length];
			if(typeof window["checkIn_dcs_multitrack_btn_click"] != "undefined"){
				checkIn_dcs_multitrack_btn_click(evtName, stepName, boxName);
			}
		}
	}

	/**
	 * richiamata quando i box degli ancillary sono caricati in pagina
	 */
	function onCheckInTrackingBoxLoaded(){
		/* calcolo elenco dei box visualizzati */
		var visible_boxes = "";
		jQuery(".j-analytics-checkIn-ancillary-box").each(function(ind, elem){
			visible_boxes = visible_boxes + $(elem).attr("data-box").split(",")[0] + ";";
		});
		if(visible_boxes.length > 0){
			/*remove last ";" */
			visible_boxes = visible_boxes.slice(0, -1);
		}
		
		jQuery("meta[name='DCSext.wck_v2_boxvisible']").attr("content", visible_boxes);

		/*al cambio tab verrano settati i meta tag
		jQuery("#analytics-checkIn-boxes").text(visible_boxes);
		
		/*tab cambiato prima che i box fossero caricati
		if(jQuery("#analytics-checkIn-tab").text() == "1"){
			jQuery("meta[name='DCSext.checkIn_v2_boxvisible']").attr("content", visible_boxes);
//			jQuery("meta[name='WT.pn_sku']").attr("content", visible_boxes);
		}*/
	}


	/**
	 * traccia prodotti visti/aggiunti/rimossi
	 */
	function onCheckInTrackingProduct(){
		var prodName = $(this).attr("data-sku");
		var prodState = $(this).attr("data-state");
		var prodQt = $(this).attr("data-qt");
		if(prodQt == ""){
			/*calcola quantita'*/
			prodQt = computeCheckInSelectedAncillaryQuantity(
					$(this).parents(".j-analytics-checkIn-ancillary-box"));
		}
		jQuery("meta[name='WT.pn_sku']").attr("content", prodName);
		jQuery("meta[name='WT.tx_u']").attr("content", prodQt);
		jQuery("meta[name='WT.tx_e']").attr("content", prodState);
	}

	function computeCheckInSelectedAncillaryQuantity(ancillary){
		var quantity = 1;
		var ancType = $(ancillary).attr("data-box");
		/*FIXME considerare upgrade*/
		if(ancType == "assicurati_posto_migliore"){
			quantity = 0;
			for (var i = 0; i < window.bookingSeatSelections.length; i++) {
				var seatSelection = window.bookingSeatSelections[i];
				if(seatSelection.previousSeatNumber != seatSelection.seatNumber){
					quantity = 	quantity + 1;
				}
			}
		}else if(ancType == "risparmia_bagaglio"){
			quantity= $(ancillary).find(".extra-baggage-checkbox:checked").length;
		}
		else if(ancType == "upgrade"){
			quantity= $(ancillary).find(".upgradePax:checked").length;
		}
		/*TODO
		 * else if(ancType == "upgrade"){
			quantity= $(ancillary).find(".foodOnBoard__select option:selected:not(:empty)").length;
		}*/
		return quantity;
	}
	
	
	function checkInAncillaryThankyou_setMeta(){
		var paymentType = getURLParameter("type");
		var paymentTypeMeta = paymentType == "free" 
			? "riepilogoservizigratuiti" : "ricevutaserviziapagamento"
		jQuery("meta[name='DCSext.arsummary']").attr("content", "Webcheckin");
		jQuery("meta[name='DCSext.wck_v2_" + paymentTypeMeta + "']").attr("content", "1");
//		if(paymentType == "free" ){
//			/*remove unnecessary meta*/
//			jQuery("meta[name='WT.si_n']").remove();
//			jQuery("meta[name='WT.si_x']").remove();
//		}
	}