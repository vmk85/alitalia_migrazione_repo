function initCheckIn(pageName) {
	
	$(document).ready(function() {
		startPageLoader(true);
		$.ajax({
			url: removeUrlSelector() + "." + pageName + "-partial.html" + location.search,
			context : document.body
		})
		.done(function(data) {
			$("#checkin-inner-content").append(data);
			chekinReady();
			upsellingsManager.init();
			initAccordions();
			initErrorOverlay();
			initLightBox();
			checkingBoardingPassSelector();
			/*bookingSeatReady();*/
			analytics_checkinPartialCallback();
			if (typeof checkInDone !== 'undefined' && typeof checkInDone === 'function') {
				checkInDone();
			}
		})
		.fail(function() {})
		.always(function() { startPageLoader(); });
	});
	
}

/*function invokeService(selector, extension, data, done, fail) {
	settings = {
		url: removeUrlSelector() + "." + selector + "." + extension,
		context : document.body
	};
	
	if (!data && extension == "json") {
		data = {
			nocache : getCurrentDate()
		}
	} else if (data && extension == "json") {
		data["nocache"] = getCurrentDate();
	}
	
	if (data) {
		settings["data"] = data;
	}
	
	$.ajax(settings).done(function(data) {
		if (done) {
			done(data);
		}
	})
	.fail(function() {
		if (fail) {
			fail();
		}
	});
}




function getCurrentDate() {
	return new Date().getTime();
}

$(document).ready(function() {
	$.ajaxSetup({ cache: false });
});*/
