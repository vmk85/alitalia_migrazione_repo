
    var dataCheckinPnr = {};
	var add = false;
	var recentFlight = '';

    function goToPassengersList(res) {

        if (!res.isError){
            console.log(res);
            performSubmit('checkinpnrselected', dataCheckinPnr, selectPnrSuccessPnr, selectPnrFail);
        } else {
            disableLoaderMyAlitalia();
        }
    }

    function selectPnrFail() {
        //alert($('#form-selectPnr').data("genericerror"));
        disableLoaderMyAlitalia();
    }

    function selectPnrSuccessPnr(data) {
        if (!data.isError) {
            window.location.href = data.successPage;
        } else {
            //alert($('#form-selectPnr').data("genericerror"));
            disableLoaderMyAlitalia();
        }
    }

    function goToPassengersListFail(result) {

        console.log(result);
        disableLoaderMyAlitalia();

    }

    $(document).ready( function() {

        // if (loadAll)
        //     gigya.accounts.getAccountInfo({ callback: loadAllPnrResponse });
        // else
		 //    gigya.accounts.getAccountInfo({ callback: loadPnrResponse });
        loadAllPnrResponse();


		$("#btAddFlight").click(function(){
			$("#addFlight").show();
			$("#addFlightToRemove").hide();
		});

		$("#addPnrSubmit").click(function(){
		    enableLoaderMyAlitalia();
            var data = {
                pnr : $("#pnr").val(),
                firstName : $("#firstName").val(),
                lastName : $("#lastName").val(),
                format : "json"
            };
            // performSubmitWithParams('myflightspnrsearch',data ,cercaPnrSuccess,cercaPnrFail,false,'', {'pnr' : $("#pnr").val(), 'firstname' : $("#firstName").val(), 'lastname' : $("#lastName").val()});IULAMV
            performSubmit("myflightspnrsearch",data,cercaPnrSuccess,cercaPnrFail)

            //     gigya.accounts.getAccountInfo({
		//         callback : function(response){
		//             if ( response.errorCode == 0 )
		//             {
		//                 var flights = response.data.flights;
		//                 var alreadyExists = false;
		//                 if(flights){
        //                     flights.forEach(function(flight) {
        //                         if(flight.flight_pnr == $("#pnr").val() ){
        //                             alreadyExists = true;
        //                         }
        //                     });
		//                 }
		//                 if(alreadyExists){
		//                     $("#labelCheckinError").html(CQ.I18n.get("myalitalia.myflights.pnrAlreadyExists"));
        //                     $("#labelCheckinError").show();
		//                     disableLoaderMyAlitalia();
		//                 }else{
        //                     // Controllo che siano stati inseriti i dati
        //                     if ($("#pnr").val() != '' && $("#firstName").val() != '' && $("#lastName").val() != '')
        //                     {
        //                         $("#addPnrSubmit").prop("disabled",true);
        //
        //                         var data = {
        //                             pnr : $("#pnr").val(),
        //                             firstName : $("#firstName").val(),
        //                             lastName : $("#lastName").val(),
        //                             format : "json"
        //                         };
        //
        //                         $(".loader-myalitalia").show();
        //                         $("#labelCheckinError").html('');
        //                         $("#labelCheckinError").hide();
        //
        //                         add = true;
        //
        //                         performSubmitWithParams('myflightspnrsearch',data ,cercaPnrSuccess,cercaPnrFail,false,'', {'pnr' : $("#pnr").val(), 'firstname' : $("#firstName").val(), 'lastname' : $("#lastName").val()});
        //
        //                     }
        //                     else
        //                     {
        //                         $("#labelCheckinError").html(CQ.I18n.get('myalitalia.add_flight.all_fields'));
        //                         $("#labelCheckinError").show();
        //                         disableLoaderMyAlitalia()
        //                     }
		//                 }
		//             }else{
        //                 disableLoaderMyAlitalia();
		//             }
        //
		//         }
		//     });

        //
        //
        //
		});

		$(document).on("click", ".checkinMyAlitalia", function(){
		    enableLoaderMyAlitalia();
            dataCheckinPnr = {
                pnr: $(this).attr("data-pnr"),
                firstName: $(this).attr("data-firstname"),
                lastName: $(this).attr("data-lastname"),
                flightitinerary:$(this).attr("data-flightitinerary"),
                managecheckin:$(this).attr("data-manage")

            };


            performSubmit('updatePassengerMyFlights', dataCheckinPnr, performSubmit('checkinpnrsearch', dataCheckinPnr, goToPassengersList, goToPassengersListFail) , null);

		});

		$(document).on("click", ".manageMyAlitaliaFlight", function(){
			cercaPnrMyFlight($(this));
		});



	});

	var flightitinerary, pnr;
	var manage = '';
	function checkinStep1(el)
	{
		pnr = $(el).data("pnr");
		flightitinerary = $(el).data("flightitinerary");
		manage = $(el).data("manage");
		var data = {
			pnr : pnr,
			firstName : $(el).data("firstname"),
			lastName : $(el).data("lastname"),
			format : "json"
		};
		enableLoaderMyAlitalia();
        performSubmitWithParams('myflightspnrsearch',data ,checkinStep1Succes,checkinStep1Fail,false,'', {'pnr' : pnr});

	}

	function checkinStep1Succes(data)
	{
		var data = {
			pnr : pnr,
			flightitinerary : flightitinerary,
			managecheckin : manage
		};

		performSubmit('checkinpnrselected',data ,checkinStep2Succes,checkinStep2Fail);
	}
	function checkinStep1Fail(data)
	{
	    disableLoaderMyAlitalia();
	}

	function checkinStep2Succes(data)
	{
		if (!data.isError)
           window.location.href = data.successPage +"?&flight=" + flightitinerary;

	}

	function checkinStep2Fail(data)
	{
	    disableLoaderMyAlitalia();
	}

	function loadPnrResponse(response)
	{
		if ( response.errorCode == 0 )
		{

			// Ciclo sui voli, elimino poi quelli non più attivi
			var flights = response.data.flights;
			var new_flights  = [];
			var ok_flight = '';
			var today = new Date();

			if(flights==null){
			    flights = {};
			}

			for(var x=0; x<flights.length; x++)
			{
			    if( new Date(flights[x].flight_departureDate) < today)
			        continue;
				if (ok_flight == '' || ok_flight.flight_departureDate > flights[x].flight_departureDate)
					ok_flight = flights[x];
			}

            if (ok_flight != '')
            {
                recentFlight = ok_flight;
                var data = {
                    pnr : ok_flight.flight_pnr,
                    firstName : ok_flight.flight_firstName,
                    lastName : ok_flight.flight_lastName,
                    format : "json"
                };
                $('.loader-myalitalia').show();
                performSubmitWithParams('myflightspnrsearch',data ,cercaPnrSuccess,cercaPnrFail,false,'', {'pnr' : ok_flight.flight_pnr, 'firstname' : ok_flight.flight_firstName, 'lastname' : ok_flight.flight_lastName});
            }else{
                var html = '<p>'+CQ.I18n.get('checkin.flightList.noFlights')+'</p>';
                $("#listaVoli").append(html);
                $('.loader-myalitalia').hide();
                $("#listaVoli").hide();
            }


		}
		else {
			alert('Error :' + response.errorMessage);
		}
	}

	function loadAllPnrResponse(response)
    	{
            $(".loader-myalitalia").show();
            performSubmit("myflightsGetAllPnr","",cercaPnrSuccessAll,cercaPnrFail)

            // 	if ( response.errorCode == 0 )
    	// 	{
        //
    	// 		// Ciclo sui voli, elimino poi quelli non più attivi
    	// 		var flights = response.data.flights;
    	// 		var new_flights  = [];
    	// 		var ok_flight = '';
        //
    	// 		if(flights==null){
    	// 		    flights = {};
    	// 		}
        //
    	// 		flights.sort(function (a,b) {
    	// 		    return new Date(a.flight_departureDate) - new Date(b.flight_departureDate);
    	// 		});
        //
    	// 		var p = $.when();
        //         flights.forEach(function(flight) {
        //         console.log(flight);
        //             p = p.then(function() {
        //                 var data = {
        //                     pnr : flight.flight_pnr,
        //                     firstName : flight.flight_firstName,
        //                     lastName : flight.flight_lastName,
        //                     format : "json"
        //                 };
        //                 $('.loader-myalitalia').show();
        //                 return performSubmitWithParams('myflightspnrsearch',data ,cercaPnrSuccess,cercaPnrFail,false,'', {'pnr' : flight.flight_pnr, 'firstname' : flight.flight_firstName, 'lastname' : flight.flight_lastName});
        //             });
        //         });

//    			for(var x=0; x<flights.length; x++)
//    			{
//
//    			    var data = {
//                        pnr : flights[x].flight_pnr,
//                        firstName : flights[x].flight_firstName,
//                        lastName : flights[x].flight_lastName,
//                        format : "json"
//                    };
//                    $('.loader-myalitalia').show();
//                    performSubmitWithParams('checkinpnrsearch',data ,cercaPnrSuccess,cercaPnrFail,false,'', {'pnr' : flights[x].flight_pnr, 'firstname' : flights[x].flight_firstName, 'lastname' : flights[x].flight_lastName});
//
//    			}

    		// }
    		// else {
    		// 	alert('Error :' + response.errorMessage);
    		// }
    	}

	var profile;
	function getAccountInfoResponse(response)
	{
		if ( response.errorCode == 0 )
		{
			var params = {};
			var flights = response.data.flights;
			if (flights == undefined)
            	flights = [];
			var flight = {};
			flight["flight_pnr"] = $("#pnr").val();
			flight["flight_firstName"] = $("#firstName").val();
			flight["flight_lastName"] = $("#lastName").val();
			flight["flight_departureDate"] = response.context.pnrData.pnr[0].flights[0].segments[0].departureDate;
			flights.push(flight);
			params["flights"] = flights;

			$("#pnr").val('');
            $("#firstName").val('');
            $("#lastName").val('');

			var par = {
				callback: function(resp) {
				    location.reload();
				},
				data: params
			};

			// gigya.accounts.setAccountInfo(par);

			//updateDepartureDate(response.context);

		}
		else {
			alert('Error :' + response.errorMessage);
		}
	}

	/*function updateDepartureDate(obj)
	{

		gigya.accounts.getAccountInfo({ callback: updatePnr, context: obj });

	}

	// Aggiorno data partenza PNR appena aggiunto
	function updatePnr(response)
	{

		var flights = response.data.flights;

		for(var x=0; x<flights.length; x++)
		{

			if (flights[x].flight_pnr == response.context.pnrData.pnr[0].number)
			{
				flights[x].flight_departureDate = response.context.pnrData.pnr[0].flights[i].segments[j].departureDate;
			}

		}

		var params = {};
		params["flights"] = flights;

		var par = {
			callback: function(resp) {
				console.log(resp);
			},
			data: params
		};

		gigya.accounts.setAccountInfo(par);

	}*/

	function cercaPnrSuccess(data) {
        $(".loader-myalitalia").hide();

		if (!data.isError)
		{
		    if (!data.inSession){
                var obj = jQuery.parseJSON(data.data);
                var nomePnr = obj.nome;
                var cognomePnr = obj.cognome;
                obj = obj.checkinPnrSearchResponse;

                // if (add)
                //     gigya.accounts.getAccountInfo({ callback: getAccountInfoResponse, context:obj });

                $("#addPnrSubmit").prop("disabled",false);
                $("#hide").show();

                try
                {
                    myAlitaliaOptions.flightSuccess();
                    if (recentFlight != '' && !loadAll)
                    {
                        if (obj.pnrData.pnr.length>0 && obj.pnrData.pnr[0].flights[0].segments[0].departureDate < recentFlight.flight_departureDate)
                        {
                            $("#listaVoli").html('');
                        }
                    }
                }
                catch(err)
                {
                    console.log('error');
                }

                var html = '';
//			html += '<div class="wrap-title wrap-title--novita">';
//		html += '<h3>Il tuo prossimo volo</h3>';
//	html += '</div>';

                if ($("#listaVoli").html() == '<p>'+CQ.I18n.get('checkin.flightList.noFlights')+'</p>')
                    $("#listaVoli").html('');

                if (obj.pnrData.pnr.length>0 && obj.pnrData.pnr[0].flights != undefined)
                {
                    if(obj.pnrData.pnr[0].flights.length==0){
                        html += '<p>'+CQ.I18n.get('checkin.flightList.noFlights')+'</p>';
                        $("#listaVoli").hide();
                    }

                    for(var i=0; i<obj.pnrData.pnr[0].flights.length;i++)
                    {
                        html += '<div class="flight-info link__toggle-wrap">';
                        html += '<div class="flight-info__codes show-for-small-only">';
                        html += '<span>' + obj.pnrData.pnr[0].flights[i].segments[0].origin.cityCode + '</span>-<span>' + obj.pnrData.pnr[0].flights[i].segments[0].destination.cityCode + '</span>';
                        html += '</div>';

                        html += '<div class="inner-row flight-info__title {isOpen-'+obj.pnrData.pnr[0].number+'}">';

                        if (obj.pnrData.pnr[0].flights[i].tooLate)
                        {

                            html += '<div class="column medium-8 small-12">';

                            if (obj.pnrData.pnr[0].flights[i].segments[0].flightStatus != 'departed')
                                html += '<p>' + CQ.I18n.get('checkin.flightslist.closed.label') + "</p>";

                            if (obj.pnrData.pnr[0].flights[i].segments[0].flightStatus == 'departed')
                                html += '<p>' + CQ.I18n.get('checkin.flightslist.closedAndDeparted.label') + "</p>";

                            html += '</div>';

                            html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','flight-info__title--ko');

                        }
                        else
                        {

                            if (obj.pnrData.pnr[0].flights[i].isWebCheckinPermitted)
                            {

                                // Conto quanti hanno fatto il checkin
                                var tot_p = 0;
                                var tot_p_c = 0;
                                if (obj.pnrData.pnr[0].flights[i].segments[0].passengers != undefined)
                                {
                                    for (var k=0; k<obj.pnrData.pnr[0].flights[i].segments[0].passengers.length; k++)
                                    {
                                        tot_p++;
                                        if (obj.pnrData.pnr[0].flights[i].segments[0].passengers[k].checkInComplete)
                                            tot_p_c++;
                                    }
                                }

                                if (i == 0)
                                {

                                    if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                    {

                                        if (tot_p_c == 0)
                                        {
                                            html += '<div class="column medium-8 small-12">';
                                            html += '<p>' + CQ.I18n.get('checkin.flightslist.url.available.label') + "</p>";
                                            html += '</div>';

                                            html += '<div class="column medium-4 small-12">';
                                            html += '<div class="button-wrap">';

                                            if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                            {
                                                html += '<a href="javascript:;" class="bntWaveCheckinPnr checkinMyAlitalia button button--light" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '" data-flightitinerary="' + i + '" data-manage="">';
                                                html += CQ.I18n.get('checkin.flightslist.docheckin.label');
                                                html += '</a>';
                                            }

                                            html += '</div>';
                                            html += '</div>';
                                        }
                                        else
                                        {

                                            var partial = CQ.I18n.get('checkin.flightslist.donepartial.label');
                                            partial = partial.replace("{0}", tot_p_c);
                                            partial = partial.replace("{1}", tot_p);
                                            html += '<div class="column medium-8 small-12">';
                                            html += '<p>' + partial + "</p>";
                                            html += '</div>';

                                            html += '<div class="column medium-4 small-12">';
                                            html += '<div class="button-wrap">';

                                            if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                            {
                                                html += '<a href="javascript:;" class="bntWaveCheckinPnr checkinMyAlitalia button button--light" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '" data-flightitinerary="' + i + '"  data-manage="manage">';
                                                html += CQ.I18n.get('checkin.flightslist.gestisci.checkin.label');
                                                html += '</a>';
                                            }

                                            html += '</div>';
                                            html += '</div>';

                                        }
                                        html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','');
                                    }
                                    else
                                    {
                                        html += '<div class="column medium-8 small-12">';
                                        html += '<p>' + CQ.I18n.get('checkin.flightslist.url.notavailable.label') + "</p>";
                                        html += '</div>';

                                        html += '<div class="column medium-4 small-12">';
                                        html += '<div class="button-wrap">';
                                        html += '<a href="javascript:;" class="button manageMyAlitaliaFlight" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '">';
                                        html += CQ.I18n.get('checkin.flightslist.manageflight.label');
                                        html += '</a>';
                                        html += '</div>';
                                        html += '</div>';

                                        html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','flight-info__title--ko');
                                    }

                                }
                                else
                                {

                                    var tot_p_0 = 0;
                                    var tot_p_c_0 = 0;
                                    if (obj.pnrData.pnr[0].flights[0].segments[0].passengers != undefined)
                                    {
                                        for (var k=0; k<obj.pnrData.pnr[0].flights[0].segments[0].passengers.length; k++)
                                        {
                                            tot_p_0++;
                                            if (obj.pnrData.pnr[0].flights[0].segments[0].passengers[k].checkInComplete)
                                                tot_p_c_0++;
                                        }
                                    }

                                    if (obj.pnrData.pnr[0].flights[0].segments[0].openCI)
                                    {

                                        if (tot_p_0 != tot_p_c_0)
                                        {

                                            html += '<div class="column medium-8 small-12">';

                                            if (obj.pnrData.pnr[0].flights[1].segments[0].openCI)
                                                html += '<p>' + CQ.I18n.get('checkin.flightslist.url.available.label') + "." + CQ.I18n.get('checkin.flightList.doCheckinPreviousFlight') + "</p>";

                                            if (!obj.pnrData.pnr[0].flights[1].segments[0].openCI)
                                                html += '<p>' + CQ.I18n.get('checkin.flightslist.url.notavailable.label') + "</p>";

                                            html += '</div>';

                                            html += '<div class="column medium-4 small-12">';
                                            html += '<div class="button-wrap">';
                                            html += '<a href="javascript:;" class="button manageMyAlitaliaFlight" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '">';
                                            html += CQ.I18n.get('checkin.flightslist.manageflight.label');
                                            html += '</a>';
                                            html += '</div>';
                                            html += '</div>';

                                            html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','flight-info__title--ko');

                                            html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','flight-info__title--ko');

                                        }
                                        else
                                        {

                                            if (obj.pnrData.pnr[0].flights[1].segments[0].openCI)
                                            {

                                                if (tot_p_c == 0)
                                                {

                                                    html += '<div class="column medium-8 small-12">';
                                                    html += '<p>' + CQ.I18n.get('checkin.flightslist.url.available.label') + "</p>";
                                                    html += '</div>';

                                                    html += '<div class="column medium-4 small-12">';
                                                    html += '<div class="button-wrap">';

                                                    if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                                    {
                                                        html += '<a href="javascript:;" class="bntWaveCheckinPnr checkinMyAlitalia button button--light" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '" data-flightitinerary="' + i + '" data-manage="">';
                                                        html += CQ.I18n.get('checkin.flightslist.docheckin.label');
                                                        html += '</a>';
                                                    }

                                                    html += '</div>';
                                                    html += '</div>';



                                                }
                                                else
                                                {

                                                    html += '<div class="column medium-8 small-12">';
                                                    html += '<p>' + CQ.I18n.get('checkin.flightslist.url.available.label') + "</p>";
                                                    html += '</div>';

                                                    html += '<div class="column medium-4 small-12">';
                                                    html += '<div class="button-wrap">';

                                                    if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                                    {
                                                        html += '<a href="javascript:;" class="bntWaveCheckinPnr checkinMyAlitalia button button--light" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '" data-flightitinerary="' + i + '" data-manage="manage">';
                                                        html += CQ.I18n.get('checkin.flightslist.gestisci.checkin.label');
                                                        html += '</a>';
                                                    }

                                                    html += '</div>';
                                                    html += '</div>';

                                                }

                                                html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','');

                                            }
                                            else
                                            {
                                                html += '<div class="column medium-8 small-12">';
                                                html += '<p>' + CQ.I18n.get('checkin.flightslist.url.notavailable.label') + "</p>";
                                                html += '</div>';

                                                html += '<div class="column medium-4 small-12">';
                                                html += '<div class="button-wrap">';
                                                html += '<a href="javascript:;" class="button manageMyAlitaliaFlight" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '">';
                                                html += CQ.I18n.get('checkin.flightslist.manageflight.label');
                                                html += '</a>';
                                                html += '</div>';
                                                html += '</div>';

                                                html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','flight-info__title--ko');
                                            }

                                        }




                                    }
                                    else
                                    {

                                        if (obj.pnrData.pnr[0].flights[1].segments[0].openCI)
                                        {

                                            if (tot_p_c == 0)
                                            {

                                                html += '<div class="column medium-8 small-12">';
                                                html += '<p>' + CQ.I18n.get('checkin.flightslist.url.available.label') + "</p>";
                                                html += '</div>';

                                                html += '<div class="column medium-4 small-12">';
                                                html += '<div class="button-wrap">';

                                                if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                                {
                                                    html += '<a href="javascript:;" class="bntWaveCheckinPnr checkinMyAlitalia button button--light" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '" data-flightitinerary="' + i + '" data-manage="">';
                                                    html += CQ.I18n.get('checkin.flightslist.docheckin.label');
                                                    html += '</a>';
                                                }

                                                html += '</div>';
                                                html += '</div>';

                                            }
                                            else
                                            {

                                                html += '<div class="column medium-8 small-12">';
                                                html += '<p>' + CQ.I18n.get('checkin.flightslist.url.available.label') + "</p>";
                                                html += '</div>';

                                                html += '<div class="column medium-4 small-12">';
                                                html += '<div class="button-wrap">';

                                                if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                                {
                                                    html += '<a href="javascript:;" class="bntWaveCheckinPnr checkinMyAlitalia button button--light" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '" data-flightitinerary="' + i + '" data-manage="manage">';
                                                    html += CQ.I18n.get('checkin.flightslist.gestisci.checkin.label');
                                                    html += '</a>';
                                                }

                                                html += '</div>';
                                                html += '</div>';

                                            }

                                            html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','');

                                        }
                                        else
                                        {
                                            html += '<div class="column medium-8 small-12">';
                                            html += '<p>' + CQ.I18n.get('checkin.flightslist.url.notavailable.label') + "</p>";
                                            html += '</div>';

                                            html += '<div class="column medium-4 small-12">';
                                            html += '<div class="button-wrap">';
                                            html += '<a href="javascript:;" class="button manageMyAlitaliaFlight" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '">';
                                            html += CQ.I18n.get('checkin.flightslist.manageflight.label');
                                            html += '</a>';
                                            html += '</div>';
                                            html += '</div>';

                                            html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','flight-info__title--ko');
                                        }

                                    }


                                }

                            }
                            else
                            {
                                if (obj.pnrData.pnr[0].flights[i].segments[0].passengers[0] != undefined)
                                {
                                    if (obj.pnrData.pnr[0].flights[i].segments[0].passengers[0].webCheckInDeepLink == '')
                                    {
                                        html += '<div class="column medium-8 small-12">';
                                        html += '<p>'+ CQ.I18n.get("checkin.flightslist.notavailablewithlink.label") +'</p>';
                                        html += '</div>';
                                    }
                                    else
                                    {
                                        html += '<div class="column medium-8 small-12">';
                                        html += '<p>'+ CQ.I18n.get("checkin.flightslist.availablelocale")+ ':' + ' ' + '<a class="webCheckInDeepLink" href="'+ obj.pnrData.pnr[0].flights[i].segments[0].passengers[0].webCheckInDeepLink + '" target="_blank">' + CQ.I18n.get("checkin.refwebsite.label") +'</p>';
                                        html += '</div>';
                                    }
                                    /*else if
                                    {
                                       html += '<div class="column medium-8 small-12">';
                                       *//*<sly data-sly-test.carrier="${ '{0}.{1}.{2}' @ format=['carriersData',flightItem.CheckinOnExternalSite,'description']}"/>
                        <sly data-sly-test.carrierLink="${ '{0}.{1}.{2}' @ format=['carriersData',flightItem.CheckinOnExternalSite,'link']}"/>*//*
                        html += '<p>'+ CQ.I18n.get("checkin.flightslist.checkinonexternalsite.label") + '<a class="checkinOnExternalSite" href=""  target="_blank">' + ${carrier @ i18n} +'</a></p>'
                        html += '</div>';
                     }*/
                                }

                                else

                                {
                                    html += '<div class="column medium-8 small-12">';
                                    html += '<p>'+ CQ.I18n.get("checkin.flightslist.notavailablewithlink.label") +'</p>';
                                    html += '</div>';
                                }



                            }
                        }
                        html += '</div>';

                        html += '<div class="show-for-small-only">';
                        html += '<div class="flight-info__link">';
                        html += '<a class="link__toggle" href="#">';
                        html += '<span class="link__toggle-open">'+CQ.I18n.get("checkin.flightslist.showdetails.label")+'</span>';
                        html += '<span class="link__toggle-close hide">'+CQ.I18n.get("checkin.flightslist.closedetails.label")+'</span>';
                        html += '</a>';
                        html += '</div>';
                        html += '</div>';

                        html += '<div class="flight-info__content">';
                        html += '<div class="flight-info__detail content__toggle">';

                        for(var j=0; j<obj.pnrData.pnr[0].flights[i].segments.length;j++)
                        {
                            var segment = obj.pnrData.pnr[0].flights[i].segments[j];
                            var options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
                            var departureDate  = new Date(segment.departureDate);
                            var departureDateFormatted  = departureDate.toLocaleDateString("it-IT", options);
                            var departureTime = addZero(departureDate.getUTCHours()) + ":" + addZero(departureDate.getUTCMinutes() == 0 ? "00" : departureDate.getUTCMinutes());

                            var arrivalDate  = new Date(segment.arrivalDate);
                            var arrivalDateFormatted  = arrivalDate.toLocaleDateString("it-IT", options);
                            var arrivalTime = addZero(arrivalDate.getUTCHours()) + ":" + addZero(arrivalDate.getMinutes() == 0 ? "0" : arrivalDate.getMinutes());
                            //var duration = segment.duration;
                            var newDuration;
                            var dSplit = segment.duration.split(":");
                            var newDuration = parseInt(dSplit[1]);
                            function addZero(i) {
                                if (i < 10) {
                                    i = "0" + i;
                                }
                                return i;
                            }

                            if (parseInt(dSplit[1]) > 0) {
                                newDuration = dSplit[0] + "H" + ":" + dSplit[1] + "'";
                                //return newDuration;
                            } else if (parseInt(dSplit[1]) == 0) {
                                newDuration = dSplit[0] + "H" + "    " ;
                                //return newDuration;
                            }

                            html += '<div class="inner-row">';
                            html += '<div class="column medium-6 flight-info__block">';
                            html += '<ul class="flight-info__date">';
                            html += '<li class="flight-info__date-departure" style="text-transform: capitalize">' + departureDateFormatted  + '</li>';
                            html += '<li class="flight-info__date-clock">' + departureTime + '</li>';
                            html += '</ul>';
                            html += '<div class="flight-info__airport">';
                            html += '<p>';
                            html += '<span class="airport__name"><strong>' + segment.origin.city + '</strong><br>';
                            html += '<span>' + segment.origin.name + ' ' + '</span>';
                            html += '</span>';
                            html += '<span class="airport__short"><strong>' + segment.origin.code + '</strong></span>';
                            html += '</p>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class="column medium-6 flight-info__block">';
                            html += '<ul class="flight-info__date">';
                            html += '<li class="flight-info__date-arrival" style="text-transform: capitalize">' + arrivalDateFormatted + '</li>';
                            html += '<li class="flight-info__date-clock">' + arrivalTime + '</li>';
                            html += '</ul>';
                            html += '<div class="flight-info__airport">';
                            html += '<p>';
                            html += '<span class="airport__name"><strong>' + segment.destination.city + '</strong><br>';
                            html += '<span>' + segment.destination.name + ' ' + '</span>';
                            html += '</span>';
                            html += '<span class="airport__short"><strong>' + segment.destination.code + '</strong></span>';
                            html += '</p>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class="inner-row">';
                            html += '<div class="column medium-5 small-12">';

                            html += '<p><strong>' + segment.airline + segment.flight + '</strong> - '+CQ.I18n.get("checkin.pdf.operatoDa.label")+' <strong>' + CQ.I18n.get('carriersData.' + segment.airlineCode + '.description') + '</strong></p>';
                            html += '</div>';
                            html += '<div class="column medium-2 small-12">';
                            html += '<p>'+CQ.I18n.get("checkin.duration.label")+' <strong id="duration">' + newDuration + '</strong></p>';
                            html += '</div>';
                            html += '<div class="column medium-4 small-12">';

                            if (obj.pnrData.pnr[0].flights[i].tooLate) {

                                if (obj.pnrData.pnr[0].flights[i].segments[0].flightStatus != 'departed')
                                    html += '<p>'+CQ.I18n.get("checkin.flightstatus.label")+' <strong>'+CQ.I18n.get("checkin.flighsList.statusDelay.labe")+'</strong></p>';

                                if (obj.pnrData.pnr[0].flights[i].segments[0].flightStatus == 'departed')
                                    html += '<p>'+CQ.I18n.get("checkin.flightstatus.label")+' <strong>'+CQ.I18n.get("checkin.flightList.tooLate.label")+'</strong></p>';
                            }
                            else {
                                html += '<p>'+CQ.I18n.get("checkin.flightstatus.label")+' <strong>'+CQ.I18n.get("checkin.flighsList.statusOnTime.label")+'</strong></p>';
                            }
                            html += '</div>';
                            html += '</div>';

                            if (segment.waiting != '00:00:00')
                            {
                                var tmpW = '';
                                try
                                {
                                    var waiting = segment.waiting;
                                    var waitingA = waiting.split(":");
                                    tmpW = waitingA[0] + "H:" + waitingA[1] + "'";
                                }
                                catch(err)
                                {
                                    tmpW = '';
                                }
                                html += '<div class="flight-info__separator">';
                                html += '<span>'+CQ.I18n.get("checkin.flightslist.transit.label")+' ' + tmpW + '</span>';
                                html += '</div>';
                            }
                        }

                        html += '</div>';
                        html += '</div>';

                        html += '</div>';

                        html += '</div>';

                    }

                }

                $("#listaVoli").append(html);
                $("#listaVoli").show();

                $("#labelCheckinError").html("");
                $("#labelCheckinError").hide();

                $("#addFlightToRemove").show();
                $("#addFlight").hide();

            }
		}
		else
		{

            $("#labelCheckinError").html(CQ.I18n.get(data.errorMessage));
		    $("#labelCheckinError").show();

			// Elimino pnr da Gygia
			// if (!add)
			    // gigya.accounts.getAccountInfo({ callback: removePnr, context:pnr });

		}

		$("#addPnrSubmit").prop("disabled",false);

		add = false;

        disableLoaderMyAlitalia();

	}

	function cercaPnrSuccessAll(data, pnr) {

		if (!data.isError)
		{

            var obj2 = jQuery.parseJSON(data.data);

			$("#addPnrSubmit").prop("disabled",false);
			$("#hide").show();

			$.each(obj2,function(k,obj){
			    var nomePnr = obj.nome;
			    var cognomePnr = obj.cognome;
			    obj = obj.checkinPnrSearchResponse;
                try
                {
                    myAlitaliaOptions.flightSuccess();
                    if (recentFlight != '' && !loadAll)
                    {
                        if (obj.pnrData.pnr.length>0 && obj.pnrData.pnr[0].flights[0].segments[0].departureDate < recentFlight.flight_departureDate)
                        {
                            $("#listaVoli").html('');
                        }
                    }
                }
                catch(err)
                {
                    console.log('error');
                }

                var html = '';
//			html += '<div class="wrap-title wrap-title--novita">';
//		html += '<h3>Il tuo prossimo volo</h3>';
//	html += '</div>';

                if ($("#listaVoli").html() == '<p>'+CQ.I18n.get('checkin.flightList.noFlights')+'</p>')
                    $("#listaVoli").html('');

                if (obj.pnrData.pnr.length>0 && obj.pnrData.pnr[0].flights != undefined)
                {
                    if(obj.pnrData.pnr[0].flights.length==0){
                        html += '<p>'+CQ.I18n.get('checkin.flightList.noFlights')+'</p>';
                        $("#listaVoli").hide();
                    }

                    for(var i=0; i<obj.pnrData.pnr[0].flights.length;i++)
                    {
                        html += '<div class="flight-info link__toggle-wrap">';
                        html += '<div class="flight-info__codes show-for-small-only">';
                        html += '<span>' + obj.pnrData.pnr[0].flights[i].segments[0].origin.cityCode + '</span>-<span>' + obj.pnrData.pnr[0].flights[i].segments[0].destination.cityCode + '</span>';
                        html += '</div>';

                        html += '<div class="inner-row flight-info__title {isOpen-'+obj.pnrData.pnr[0].number+'}">';

                        if (obj.pnrData.pnr[0].flights[i].tooLate)
                        {

                            html += '<div class="column medium-8 small-12">';

                            if (obj.pnrData.pnr[0].flights[i].segments[0].flightStatus != 'departed')
                                html += '<p>' + CQ.I18n.get('checkin.flightslist.closed.label') + "</p>";

                            if (obj.pnrData.pnr[0].flights[i].segments[0].flightStatus == 'departed')
                                html += '<p>' + CQ.I18n.get('checkin.flightslist.closedAndDeparted.label') + "</p>";

                            html += '</div>';

                            html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','flight-info__title--ko');

                        }
                        else
                        {

                            if (obj.pnrData.pnr[0].flights[i].isWebCheckinPermitted)
                            {

                                // Conto quanti hanno fatto il checkin
                                var tot_p = 0;
                                var tot_p_c = 0;
                                if (obj.pnrData.pnr[0].flights[i].segments[0].passengers != undefined)
                                {
                                    for (var k=0; k<obj.pnrData.pnr[0].flights[i].segments[0].passengers.length; k++)
                                    {
                                        tot_p++;
                                        if (obj.pnrData.pnr[0].flights[i].segments[0].passengers[k].checkInComplete)
                                            tot_p_c++;
                                    }
                                }

                                if (i == 0)
                                {

                                    if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                    {

                                        if (tot_p_c == 0)
                                        {
                                            html += '<div class="column medium-8 small-12">';
                                            html += '<p>' + CQ.I18n.get('checkin.flightslist.url.available.label') + "</p>";
                                            html += '</div>';

                                            html += '<div class="column medium-4 small-12">';
                                            html += '<div class="button-wrap">';

                                            if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                            {
                                                html += '<a href="javascript:;" class="bntWaveCheckinPnr checkinMyAlitalia button button--light" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '" data-flightitinerary="' + i + '" data-manage="">';
                                                html += CQ.I18n.get('checkin.flightslist.docheckin.label');
                                                html += '</a>';
                                            }

                                            html += '</div>';
                                            html += '</div>';
                                        }
                                        else
                                        {

                                            var partial = CQ.I18n.get('checkin.flightslist.donepartial.label');
                                            partial = partial.replace("{0}", tot_p_c);
                                            partial = partial.replace("{1}", tot_p);
                                            html += '<div class="column medium-8 small-12">';
                                            html += '<p>' + partial + "</p>";
                                            html += '</div>';

                                            html += '<div class="column medium-4 small-12">';
                                            html += '<div class="button-wrap">';

                                            if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                            {
                                                html += '<a href="javascript:;" class="bntWaveCheckinPnr checkinMyAlitalia button button--light" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '" data-flightitinerary="' + i + '"  data-manage="manage">';
                                                html += CQ.I18n.get('checkin.flightslist.gestisci.checkin.label');
                                                html += '</a>';
                                            }

                                            html += '</div>';
                                            html += '</div>';

                                        }
                                        html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','');
                                    }
                                    else
                                    {
                                        html += '<div class="column medium-8 small-12">';
                                        html += '<p>' + CQ.I18n.get('checkin.flightslist.url.notavailable.label') + "</p>";
                                        html += '</div>';

                                        html += '<div class="column medium-4 small-12">';
                                        html += '<div class="button-wrap">';
                                        html += '<a href="javascript:;" class="button manageMyAlitaliaFlight" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '">';
                                        html += CQ.I18n.get('checkin.flightslist.manageflight.label');
                                        html += '</a>';
                                        html += '</div>';
                                        html += '</div>';

                                        html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','flight-info__title--ko');
                                    }

                                }
                                else
                                {

                                    var tot_p_0 = 0;
                                    var tot_p_c_0 = 0;
                                    if (obj.pnrData.pnr[0].flights[0].segments[0].passengers != undefined)
                                    {
                                        for (var k=0; k<obj.pnrData.pnr[0].flights[0].segments[0].passengers.length; k++)
                                        {
                                            tot_p_0++;
                                            if (obj.pnrData.pnr[0].flights[0].segments[0].passengers[k].checkInComplete)
                                                tot_p_c_0++;
                                        }
                                    }

                                    if (obj.pnrData.pnr[0].flights[0].segments[0].openCI)
                                    {

                                        if (tot_p_0 != tot_p_c_0)
                                        {

                                            html += '<div class="column medium-8 small-12">';

                                            if (obj.pnrData.pnr[0].flights[1].segments[0].openCI)
                                                html += '<p>' + CQ.I18n.get('checkin.flightslist.url.available.label') + "." + CQ.I18n.get('checkin.flightList.doCheckinPreviousFlight') + "</p>";

                                            if (!obj.pnrData.pnr[0].flights[1].segments[0].openCI)
                                                html += '<p>' + CQ.I18n.get('checkin.flightslist.url.notavailable.label') + "</p>";

                                            html += '</div>';

                                            html += '<div class="column medium-4 small-12">';
                                            html += '<div class="button-wrap">';
                                            html += '<a href="javascript:;" class="button manageMyAlitaliaFlight" data-firstName="' + nomePnr + '" data-lastName="' + nomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '">';
                                            html += CQ.I18n.get('checkin.flightslist.manageflight.label');
                                            html += '</a>';
                                            html += '</div>';
                                            html += '</div>';

                                            html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','flight-info__title--ko');

                                            html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','flight-info__title--ko');

                                        }
                                        else
                                        {

                                            if (obj.pnrData.pnr[0].flights[1].segments[0].openCI)
                                            {

                                                if (tot_p_c == 0)
                                                {

                                                    html += '<div class="column medium-8 small-12">';
                                                    html += '<p>' + CQ.I18n.get('checkin.flightslist.url.available.label') + "</p>";
                                                    html += '</div>';

                                                    html += '<div class="column medium-4 small-12">';
                                                    html += '<div class="button-wrap">';

                                                    if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                                    {
                                                        html += '<a href="javascript:;" class="bntWaveCheckinPnr checkinMyAlitalia button button--light" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '" data-flightitinerary="' + i + '" data-manage="">';
                                                        html += CQ.I18n.get('checkin.flightslist.docheckin.label');
                                                        html += '</a>';
                                                    }

                                                    html += '</div>';
                                                    html += '</div>';



                                                }
                                                else
                                                {

                                                    html += '<div class="column medium-8 small-12">';
                                                    html += '<p>' + CQ.I18n.get('checkin.flightslist.url.available.label') + "</p>";
                                                    html += '</div>';

                                                    html += '<div class="column medium-4 small-12">';
                                                    html += '<div class="button-wrap">';

                                                    if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                                    {
                                                        html += '<a href="javascript:;" class="bntWaveCheckinPnr checkinMyAlitalia button button--light" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '" data-flightitinerary="' + i + '" data-manage="manage">';
                                                        html += CQ.I18n.get('checkin.flightslist.gestisci.checkin.label');
                                                        html += '</a>';
                                                    }

                                                    html += '</div>';
                                                    html += '</div>';

                                                }

                                                html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','');

                                            }
                                            else
                                            {
                                                html += '<div class="column medium-8 small-12">';
                                                html += '<p>' + CQ.I18n.get('checkin.flightslist.url.notavailable.label') + "</p>";
                                                html += '</div>';

                                                html += '<div class="column medium-4 small-12">';
                                                html += '<div class="button-wrap">';
                                                html += '<a href="javascript:;" class="button manageMyAlitaliaFlight" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '">';
                                                html += CQ.I18n.get('checkin.flightslist.manageflight.label');
                                                html += '</a>';
                                                html += '</div>';
                                                html += '</div>';

                                                html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','flight-info__title--ko');
                                            }

                                        }




                                    }
                                    else
                                    {

                                        if (obj.pnrData.pnr[0].flights[1].segments[0].openCI)
                                        {

                                            if (tot_p_c == 0)
                                            {

                                                html += '<div class="column medium-8 small-12">';
                                                html += '<p>' + CQ.I18n.get('checkin.flightslist.url.available.label') + "</p>";
                                                html += '</div>';

                                                html += '<div class="column medium-4 small-12">';
                                                html += '<div class="button-wrap">';

                                                if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                                {
                                                    html += '<a href="javascript:;" class="bntWaveCheckinPnr checkinMyAlitalia button button--light" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '" data-flightitinerary="' + i + '" data-manage="">';
                                                    html += CQ.I18n.get('checkin.flightslist.docheckin.label');
                                                    html += '</a>';
                                                }

                                                html += '</div>';
                                                html += '</div>';

                                            }
                                            else
                                            {

                                                html += '<div class="column medium-8 small-12">';
                                                html += '<p>' + CQ.I18n.get('checkin.flightslist.url.available.label') + "</p>";
                                                html += '</div>';

                                                html += '<div class="column medium-4 small-12">';
                                                html += '<div class="button-wrap">';

                                                if (obj.pnrData.pnr[0].flights[i].segments[0].openCI)
                                                {
                                                    html += '<a href="javascript:;" class="bntWaveCheckinPnr checkinMyAlitalia button button--light" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '" data-flightitinerary="' + i + '" data-manage="manage">';
                                                    html += CQ.I18n.get('checkin.flightslist.gestisci.checkin.label');
                                                    html += '</a>';
                                                }

                                                html += '</div>';
                                                html += '</div>';

                                            }

                                            html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','');

                                        }
                                        else
                                        {
                                            html += '<div class="column medium-8 small-12">';
                                            html += '<p>' + CQ.I18n.get('checkin.flightslist.url.notavailable.label') + "</p>";
                                            html += '</div>';

                                            html += '<div class="column medium-4 small-12">';
                                            html += '<div class="button-wrap">';
                                            html += '<a href="javascript:;" class="button manageMyAlitaliaFlight" data-firstName="' + nomePnr + '" data-lastName="' + cognomePnr + '" data-pnr="' + obj.pnrData.pnr[0].number + '">';
                                            html += CQ.I18n.get('checkin.flightslist.manageflight.label');
                                            html += '</a>';
                                            html += '</div>';
                                            html += '</div>';

                                            html = html.replace('{isOpen-'+obj.pnrData.pnr[0].number+'}','flight-info__title--ko');
                                        }

                                    }


                                }

                            }
                            else
                            {
                                if (obj.pnrData.pnr[0].flights[i].segments[0].passengers[0] != undefined)
                                {
                                    if (obj.pnrData.pnr[0].flights[i].segments[0].passengers[0].webCheckInDeepLink == '')
                                    {
                                        html += '<div class="column medium-8 small-12">';
                                        html += '<p>'+ CQ.I18n.get("checkin.flightslist.notavailablewithlink.label") +'</p>';
                                        html += '</div>';
                                    }
                                    else
                                    {
                                        html += '<div class="column medium-8 small-12">';
                                        html += '<p>'+ CQ.I18n.get("checkin.flightslist.availablelocale")+ ':' + ' ' + '<a class="webCheckInDeepLink" href="'+ obj.pnrData.pnr[0].flights[i].segments[0].passengers[0].webCheckInDeepLink + '" target="_blank">' + CQ.I18n.get("checkin.refwebsite.label") +'</p>';
                                        html += '</div>';
                                    }
                                    /*else if
                                    {
                                       html += '<div class="column medium-8 small-12">';
                                       *//*<sly data-sly-test.carrier="${ '{0}.{1}.{2}' @ format=['carriersData',flightItem.CheckinOnExternalSite,'description']}"/>
                        <sly data-sly-test.carrierLink="${ '{0}.{1}.{2}' @ format=['carriersData',flightItem.CheckinOnExternalSite,'link']}"/>*//*
                        html += '<p>'+ CQ.I18n.get("checkin.flightslist.checkinonexternalsite.label") + '<a class="checkinOnExternalSite" href=""  target="_blank">' + ${carrier @ i18n} +'</a></p>'
                        html += '</div>';
                     }*/
                                }

                                else

                                {
                                    html += '<div class="column medium-8 small-12">';
                                    html += '<p>'+ CQ.I18n.get("checkin.flightslist.notavailablewithlink.label") +'</p>';
                                    html += '</div>';
                                }



                            }
                        }
                        html += '</div>';

                        html += '<div class="show-for-small-only">';
                        html += '<div class="flight-info__link">';
                        html += '<a class="link__toggle" href="#">';
                        html += '<span class="link__toggle-open">'+CQ.I18n.get("checkin.flightslist.showdetails.label")+'</span>';
                        html += '<span class="link__toggle-close hide">'+CQ.I18n.get("checkin.flightslist.closedetails.label")+'</span>';
                        html += '</a>';
                        html += '</div>';
                        html += '</div>';

                        html += '<div class="flight-info__content">';
                        html += '<div class="flight-info__detail content__toggle">';

                        for(var j=0; j<obj.pnrData.pnr[0].flights[i].segments.length;j++)
                        {
                            var segment = obj.pnrData.pnr[0].flights[i].segments[j];
                            var options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
                            var departureDate  = new Date(segment.departureDate);
                            var departureDateFormatted  = departureDate.toLocaleDateString("it-IT", options);
                            var departureTime = addZero(departureDate.getUTCHours()) + ":" + addZero(departureDate.getUTCMinutes() == 0 ? "00" : departureDate.getUTCMinutes());

                            var arrivalDate  = new Date(segment.arrivalDate);
                            var arrivalDateFormatted  = arrivalDate.toLocaleDateString("it-IT", options);
                            var arrivalTime = addZero(arrivalDate.getUTCHours()) + ":" + addZero(arrivalDate.getMinutes() == 0 ? "0" : arrivalDate.getMinutes());
                            //var duration = segment.duration;
                            var newDuration;
                            var dSplit = segment.duration.split(":");
                            var newDuration = parseInt(dSplit[1]);
                            function addZero(i) {
                                if (i < 10) {
                                    i = "0" + i;
                                }
                                return i;
                            }

                            if (parseInt(dSplit[1]) > 0) {
                                newDuration = dSplit[0] + "H" + ":" + dSplit[1] + "'";
                                //return newDuration;
                            } else if (parseInt(dSplit[1]) == 0) {
                                newDuration = dSplit[0] + "H" + "    " ;
                                //return newDuration;
                            }

                            html += '<div class="inner-row">';
                            html += '<div class="column medium-6 flight-info__block">';
                            html += '<ul class="flight-info__date">';
                            html += '<li class="flight-info__date-departure" style="text-transform: capitalize">' + departureDateFormatted  + '</li>';
                            html += '<li class="flight-info__date-clock">' + departureTime + '</li>';
                            html += '</ul>';
                            html += '<div class="flight-info__airport">';
                            html += '<p>';
                            html += '<span class="airport__name"><strong>' + segment.origin.city + '</strong><br>';
                            html += '<span>' + segment.origin.name + ' ' + '</span>';
                            html += '</span>';
                            html += '<span class="airport__short"><strong>' + segment.origin.code + '</strong></span>';
                            html += '</p>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class="column medium-6 flight-info__block">';
                            html += '<ul class="flight-info__date">';
                            html += '<li class="flight-info__date-arrival" style="text-transform: capitalize">' + arrivalDateFormatted + '</li>';
                            html += '<li class="flight-info__date-clock">' + arrivalTime + '</li>';
                            html += '</ul>';
                            html += '<div class="flight-info__airport">';
                            html += '<p>';
                            html += '<span class="airport__name"><strong>' + segment.destination.city + '</strong><br>';
                            html += '<span>' + segment.destination.name + ' ' + '</span>';
                            html += '</span>';
                            html += '<span class="airport__short"><strong>' + segment.destination.code + '</strong></span>';
                            html += '</p>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class="inner-row">';
                            html += '<div class="column medium-5 small-12">';

                            html += '<p><strong>' + segment.airline + segment.flight + '</strong> - '+CQ.I18n.get("checkin.pdf.operatoDa.label")+' <strong>' + CQ.I18n.get('carriersData.' + segment.airlineCode + '.description') + '</strong></p>';
                            html += '</div>';
                            html += '<div class="column medium-2 small-12">';
                            html += '<p>'+CQ.I18n.get("checkin.duration.label")+' <strong id="duration">' + newDuration + '</strong></p>';
                            html += '</div>';
                            html += '<div class="column medium-4 small-12">';

                            if (obj.pnrData.pnr[0].flights[i].tooLate) {

                                if (obj.pnrData.pnr[0].flights[i].segments[0].flightStatus != 'departed')
                                    html += '<p>'+CQ.I18n.get("checkin.flightstatus.label")+' <strong>'+CQ.I18n.get("checkin.flighsList.statusDelay.labe")+'</strong></p>';

                                if (obj.pnrData.pnr[0].flights[i].segments[0].flightStatus == 'departed')
                                    html += '<p>'+CQ.I18n.get("checkin.flightstatus.label")+' <strong>'+CQ.I18n.get("checkin.flightList.tooLate.label")+'</strong></p>';
                            }
                            else {
                                html += '<p>'+CQ.I18n.get("checkin.flightstatus.label")+' <strong>'+CQ.I18n.get("checkin.flighsList.statusOnTime.label")+'</strong></p>';
                            }
                            html += '</div>';
                            html += '</div>';

                            if (segment.waiting != '00:00:00')
                            {
                                var tmpW = '';
                                try
                                {
                                    var waiting = segment.waiting;
                                    var waitingA = waiting.split(":");
                                    tmpW = waitingA[0] + "H:" + waitingA[1] + "'";
                                }
                                catch(err)
                                {
                                    tmpW = '';
                                }
                                html += '<div class="flight-info__separator">';
                                html += '<span>'+CQ.I18n.get("checkin.flightslist.transit.label")+' ' + tmpW + '</span>';
                                html += '</div>';
                            }
                        }

                        html += '</div>';
                        html += '</div>';

                        html += '</div>';

                        html += '</div>';

                    }

                }

                $(".loader-myalitalia").hide();
                $("#listaVoli").append(html);
                $("#listaVoli").show();

                $("#labelCheckinError").html("");
                $("#labelCheckinError").hide();

                $("#addFlightToRemove").show();
                $("#addFlight").hide();
            });

		}
		else
		{
		    $(".loader-myalitalia").hide();


            // Elimino pnr da Gygia
			// if (!add)
			    // gigya.accounts.getAccountInfo({ callback: removePnr, context:pnr });

		}
		disableLoaderMyAlitalia();

	}

	function removePnr(response)
	{


		if ( response.errorCode == 0 )
		{
			var flights = response.data.flights;

			var found = -1;
			for(var x=0; x<flights.length; x++)
			{

			    if (flights[x].flight_pnr == response.context.pnr)
            		found = x;

			}

			if (found >= 0)
				flights.splice(found, 1);

			var params = {};
			params["flights"] = flights;

			var par = {
				callback: function(resp) {
					console.log(resp);
				},
				data: params
			};

			// gigya.accounts.setAccountInfo(par);
		}

	}


	function cercaPnrFail(data)
	{
        $("#labelCheckinError").html(CQ.I18n.get("myalitalia.myflights.errorMessage"));
        $("#labelCheckinError").show();
        disableLoaderMyAlitalia();
        $(".loader-myalitalia").hide();
	}


	function performSubmitWithParams(service, form, done, fail, always, selector, additionalParams) {
    	return invokeGenericFormServiceWithParams(service, 'POST', form, done, fail, always,
    			selector, additionalParams);
    }

    function cercaPnrMyFlight(el){
        $("#pnr_manage").val($(el).data("pnr"));
        $("#firstname_manage").val($(el).data("firstname"));
        $("#lastname_manage").val($(el).data("lastname"));
        enableLoaderMyAlitalia();
        performSubmit('preparaviaggiosubmit', '#form-myFlightSearch' ,cercaPnrMyFlightsSuccess,cercaPnrMyFlightsFail);
        //}
    }

    function cercaPnrMyFlightsSuccess(data) {
        disableLoaderMyAlitalia();
        if (!data.isError) {
            window.open(data.redirect);
        }
    }

    function cercaPnrMyFlightsFail() {
        // disable loader
        disableLoaderMyAlitalia();
    }

    function invokeGenericFormServiceWithParams(service, method, form, done, fail, always,
    		selector, additionalParams) {
    	var actualData;
    	if ($.type(form) === "string" || !form) {
    		var serializedData = $(form).serialize();
    		if (serializedData != "") {
    			serializedData += "&";
    		}
    		serializedData += "_isAjax=true";
    		if (additionalParams) {
    			for ( var paramName in additionalParams) {
    				serializedData += "&" + paramName + "="
    						+ encodeURIComponent(additionalParams[paramName]);
    			}
    		}
    		actualData = serializedData;
    	} else if ($.isPlainObject(form)) {
    		actualData = {};
    		if (form) {
    			for (var fieldName in form) {
    				actualData[fieldName] = form[fieldName];
    			}
    		}
    		actualData['_isAjax'] = true;
    		if (additionalParams) {
    			for (var paramName in additionalParams) {
    				actualData[paramName] = additionalParams[paramName];
    			}
    		}
    	}
    	return $.ajax({
    		url : getServiceUrl(service, selector),
    		method : method,
    		data : actualData,
    		context : document.body
    	}).done(function(data) {
    		if (done) {
    			done(data, additionalParams);
    		}
    	}).fail(function() {
    		if (fail) {
    			fail();
    		}
    	}).always(function() {
    		if (always) {
    			always();
    		}
    	});
    }