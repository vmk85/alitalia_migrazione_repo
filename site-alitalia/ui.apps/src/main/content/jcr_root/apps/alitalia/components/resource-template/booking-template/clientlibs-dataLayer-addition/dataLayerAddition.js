var DTMRule = "";
var DTMRuleLater = false; 

function analytics_bookingPartialCallback(){
	if(typeof window["analytics_booking_step"] !== "undefined"){
		addBookingAnalyticsData(analytics_booking_step);
		activateGTMOnce("booking");
		activateWebtrendsOnce("booking");
		if(DTMRule && !DTMRuleLater){
			activateDTMRule(DTMRule);
		}
	}
}

function addBookingAnalyticsData(bookingStep){
		addDataLayerVar();
		var carnet = isCarnet();
		switch(bookingStep){
		case -1:
			addFailurePageBookingAnalyticsData();
		case 0:
			addHomePageErrorBookingAnalyticsData();
			break;
		case 1:
			DTMRule = "SelectFlight";
			if(carnet){
				DTMRule = "CarnetUseSelectFlight";
			}
			break;
		case 2:
			DTMRule = "PassengerEdit";
			if(carnet){
				DTMRule = "CarnetUseInfoPax";
			}
			break;
		case 3:
			DTMRule = "Ancillary";
			if(carnet){
				DTMRule = "CarnetUseAncillary";
			}
			break;
		case 4:
			DTMRule = "Payment";
			if(carnet){
				DTMRule = "CarnetUseEmetti";
			}
			break;
		case 5:
			/*DTMRuleLater = true;*/
			DTMRule = "Receipt";
			if(carnet){
				DTMRule = "CarnetUseReceipt";
			}
			break;
		}
		setWebtrendsMeta();
		cleanWebtrendsMeta();
		if(isAward() && isAwardCalendar()){
			DTMRule = "CalendarAward";
		}
	}
	
	function addDataLayerVar(){
		if(typeof window["analytics_dl_var"] == "object"){
			addAnalyticsData(window["analytics_dl_var"]);
		}
	}

	/*function addDataLayerVar(){
		if(typeof window["analytics_dl_var"] == "object"){
            /*Renaming*//*
            if(window["analytics_dl_var"]["TktPerPNR"]){
				window["analytics_dl_var"]["TktperPNR"] = window["analytics_dl_var"]["TktPerPNR"];
                delete window["analytics_dl_var"]["TktPerPNR"];
            }
            if(window["analytics_dl_var"]["DayOfWeekA"]){
				window["analytics_dl_var"]["DayofWeekA"] = window["analytics_dl_var"]["DayOfWeekA"];
                delete window["analytics_dl_var"]["DayOfWeekA"];
            }
            if(window["analytics_dl_var"]["DayOfWeekR"]){
				window["analytics_dl_var"]["DayofWeekR"] = window["analytics_dl_var"]["DayOfWeekR"];
                delete window["analytics_dl_var"]["DayOfWeekR"];
            }
             if(window["analytics_dl_var"]["telefono"]){
				window["analytics_dl_var"]["Telefono"] = window["analytics_dl_var"]["telefono"];
                delete window["analytics_dl_var"]["telefono"];
            }
			addAnalyticsData(window["analytics_dl_var"]);
		}
	}*/



	function addFailurePageBookingAnalyticsData(){
		var errorMessages = jQuery(".innerPage__body").find("strong").text();
		addAnalyticsData({"BError": errorMessages});
		activateDTMRule("Error");
	}
	
	function addHomePageErrorBookingAnalyticsData(){
		var errorMessages = jQuery("#main-content .genericErrorMessage__text").text();
		if(errorMessages){
			addAnalyticsData({"BError": errorMessages});
			activateDTMRule("Error");
		}
	}	
	
	function setWebtrendsMeta(){
		if(typeof(window["analytics_wt_meta"])  !== "undefined"){
			for(key in window["analytics_wt_meta"]){
				jQuery("meta[name='"+key+"']").attr("content", window["analytics_wt_meta"][key]);
			}
		}
	}
	
	function cleanWebtrendsMeta(){
		/*remove empty WT and DCSext meta*/
		jQuery("meta[name^='WT.'],meta[name^='DCSext.']").each(function(ind, elem){
			if($(elem).attr("content").length == 0){
				$(elem).remove();
			}
		});
		/*remove other empty meta*/
		jQuery("meta[content='DELETEME']").each(function(ind, elem){
			$(elem).remove();
		});
	}
	
	function analytics_setBookingNewsletterCheckboxMeta(){
		jQuery("meta[name='DCSext.checknewsletter']").attr("content", "1");
	}
	
	function analytics_unsetBookingNewsletterCheckboxMeta(){
		jQuery("meta[name='DCSext.checknewsletter']").attr("content", "");
	}
	
	function analytics_trackBookingErrors(errors, areaName){
		if(!areaName){
			areaName = jQuery(".analytics_InfoTarget").attr("data-errorArea");
			areaName = areaName ? areaName : "";
		}
		var errorMessages = "";
		/*compute error messages*/
		if(typeof errors === "object"){
			for(key in errors){
				errorMessages = errorMessages + key + " "  + errors[key] + "; ";
			}	
			if(errorMessages.length > 0)
				errorMessages = errorMessages.slice(0,-2);
		}
		else if (typeof errors === "string"){
			errorMessages = errors;
		}
		if(errorMessages.length > 0){
			if(isCarnet()){
				pushAnalyticsData({"CarnetError": errorMessages});
				activateDTMRule("CarnetError");
			}
			else{
			/*Add or update meta tag*/
			if($("meta[name='DCSext.BError']").length ==  0){
				var htmlMeta = "<meta name='DCSext.BError' content=\""+errorMessages.replace(/"/g, "&#34;")+"\">";
				jQuery("head").append(htmlMeta);
			}
			else{
				$("meta[name='DCSext.BError']").attr("content", errorMessages);
			}
			booking_dcs_multitrack_errors(errorMessages, areaName);
			pushAnalyticsData({"BError": errorMessages});
			activateDTMRule("Error");
		}
	}
	}
	
	function isAward(){
		/*FIXME spostare in backend*/
		return $("meta[name='DCSext.booking_award_control']").length > 0;
	}
	
	function isAwardCalendar(){
		return $("meta[name='DCSext.BFormItem']").attr("content") == "result";
	}
	
	function isCarnet(){
		/*FIXME spostare in backend*/
		return getDataLayerData("CarnetUseStep");
	}
	
	
	
