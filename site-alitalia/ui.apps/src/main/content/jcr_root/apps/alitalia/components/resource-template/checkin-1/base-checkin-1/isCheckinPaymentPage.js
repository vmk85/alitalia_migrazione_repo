"use strict";
use(function () {

    var path = this.path;
    var isCheckinPaymentPage = false;

    if(path.indexOf('check-in-payment') > -1){
        isCheckinPaymentPage = true;
    }
    return isCheckinPaymentPage;

});