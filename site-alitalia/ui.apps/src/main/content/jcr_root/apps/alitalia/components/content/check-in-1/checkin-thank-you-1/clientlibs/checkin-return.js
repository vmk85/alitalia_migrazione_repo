function cercaPnr() {

    enableLoaderCheckin();


    // enable loader
        // Faccio prima una chiamata per capire se il volo esiste, se

        var data = {
            pnr: $('#pnr').val(),
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val()
        };

        //todo controllare se va bene 'checkinpnrsearch', notare che nella success viene chiamata la servlet 'prpeparaviaggiosubmit'. Forse per andare al vecchio check-in??
        //performSubmit('checkinpnrsearch',data,cercaPnrBeforeSuccess,cercaPnrBeforeFail);
        performSubmit('checkinpnrsearch', '#form-cercaPnr', cercaPnrSuccess, cercaPnrFail);

}

function cercaPnrSuccess(data) {
    checkinStarted++;
    updateDataLayer('checkinStarted', checkinStarted);
    if (!data.isError) {
        performSubmit('checkinpnrselected', '#form-cercaPnr', selectPnrSuccess, selectPnrFail);
    } else {
        disableLoaderCheckin();
    }
}

function cercaPnrFail() {
    disableLoaderCheckin();

}

function selectPnrSuccess(data) {
    if (!data.isError) {
        window.location.href = data.successPage;
    } else {
        //alert($('#form-selectPnr').data("genericerror"));
        disableLoaderCheckin();
    }
}


function selectPnrSuccessEdit(data) {
    if (!data.isError) {
        window.location.href = data.successPage+"?&flight="+flight;
    } else {
        //alert($('#form-selectPnr').data("genericerror"));
        disableLoaderCheckin();
    }
}

function selectPnrFail() {
    //alert($('#form-selectPnr').data("genericerror"));
    disableLoaderCheckin();
}

function gestisciPnr() {
    enableLoaderCheckin();

    if ($('#frequentFlyer').val()){

        performSubmit('searchbyfrequentflyer', '#form-gestisci', cercaPnrSuccessGestisci, cercaPnrFail);


    } else {


        // enable loader
        // Faccio prima una chiamata per capire se il volo esiste, se

        var data = {
            pnr: $('#pnr').val(),
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val()
        };

        //todo controllare se va bene 'checkinpnrsearch', notare che nella success viene chiamata la servlet 'prpeparaviaggiosubmit'. Forse per andare al vecchio check-in??
        //performSubmit('checkinpnrsearch',data,cercaPnrBeforeSuccess,cercaPnrBeforeFail);
        performSubmit('checkinpnrsearch', '#form-gestisci', cercaPnrSuccessGestisci, cercaPnrFail);
    }
}

function cercaPnrSuccessGestisci(data) {
    checkinStarted++;
    updateDataLayer('checkinStarted', checkinStarted);
    if (!data.isError) {
        performSubmit('checkinpnrselected', '#form-gestisci', selectPnrSuccessEdit, selectPnrFail);
    } else {
        disableLoaderCheckin();
    }

}