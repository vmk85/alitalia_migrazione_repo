$(document).ready(function() {
	$('#baggageSubmit').bind('click.baggageHandlers', function baggageHandler(e) {
		validation(e, 'baggageclaimsubmit', '#specialpageform_baggageclaim', baggageClaimSuccess, baggageClaimError);
	});
	$("#reason").bind('change', function() {
		return ($(this).val() == "OTHER_REASON") ? 
				$("#reason_explain").show() : $("#reason_explain").hide();
	});
});

function baggageClaimSuccess(data) {
	return (data.result ? baggageClaimContinue() : showErrors(data));
}

function baggageClaimError() {
	return baggageClaimContinue(true);
}

function baggageClaimContinue(stop) {
	removeErrors();
	$('#baggageSubmit').removeAttr('onclick').unbind('click');
	return stop || $('#baggageSubmit').click();
}