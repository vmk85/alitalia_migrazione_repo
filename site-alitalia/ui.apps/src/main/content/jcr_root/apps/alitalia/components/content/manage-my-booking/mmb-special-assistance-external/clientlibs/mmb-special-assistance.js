$(document).ready(function() {
	$('#special_assistance_submit').on('click', specialAssistanceValidate);
	specialAssistanceTrigger();
});

function specialAssistanceValidate(e) {
	e.preventDefault();
	$('#special_assistance_submit').off('click', specialAssistanceValidate);
	startPageLoader(true);
	validation(e, 'specialassistanceservletnew', '#special-assistance-form', 
			specialAssistanceValidationSuccess, function() {
				console.log("Fail specialAssistanceValidate");
				$('#special_assistance_submit').on('click', specialAssistanceValidate);
				startPageLoader(false);
	});
}

function specialAssistanceValidationSuccess(data) {
	removeErrors();
	if (data.result) {
		performSubmit('specialassistanceservletnew', '#special-assistance-form', 
				specialAssistanceSubmitSuccess, function() {
					console.log("Fail specialAssistanceValidationSuccess");
					$('#special_assistance_submit').on('click', specialAssistanceValidate);
					startPageLoader(false);
		});
	} else {
		$('#special_assistance_submit').on('click', specialAssistanceValidate);
		startPageLoader(false);
		showErrors(data);
	}
}

function specialAssistanceSubmitSuccess(data) {
	$('#special_assistance_submit').on('click', specialAssistanceValidate);
	startPageLoader(false);
	if (data.result == "OK") {
		showSuccessMessage();
	} else {
		showFailMessage();
	}
}

function showSuccessMessage() {
	$('#failMessage').hide();
	$('#successMessage').show();
}

function showFailMessage() {
	$('#successMessage').hide();
	$('#failMessage').show();
}