$(function() {
/*
	function onSelectDeparture(suggestion) {
		$(this).val(suggestion.value);
		$(this).siblings(".apt").text(suggestion.city);
		$(this).siblings(".city").text(suggestion.type);
		$(this).siblings("#" + $(this).attr("id") + "_country").val(suggestion.country);
		$(this).siblings(".j-countryCode").val(suggestion.code);
		$(this).siblings("#Origin").val(suggestion.type);
		$(this).siblings("#originKeyCityCode").text(suggestion.keyCityCode);
		$(this).siblings("#originKeyCountryCode").text(suggestion.keyCountryCode);
		$(this).siblings("#originKeyAirportName").text(suggestion.keyAirportName);
		$(this).closest('.customInput--flightFinder').addClass('selected');
		$('#arrivalInput').focus();
	}
	
	//togliere riga che selezione .customInput--flightFindernelle funzioni usate dal cerca volo
	function onSelectDepartureMultileg(suggestion) {
		$(this).val(suggestion.value);
		$(this).siblings(".apt").text(suggestion.city);
		$(this).siblings(".city").text(suggestion.type);
		$(this).siblings(".origin").val(suggestion.type);
		$(this).siblings(".originKeyCityCode").text(suggestion.keyCityCode);
		$(this).siblings(".originKeyCountryCode").text(suggestion.keyCountryCode);
		$(this).siblings(".originKeyAirportName").text(suggestion.keyAirportName);
		$(this).closest('.customInput--flightFinder').addClass('selected');
		$('.departure.autoComplete-multileg').focus();
	}
	
	function onSelectArrival(suggestion) {
		$(this).val(suggestion.value);
		$(this).siblings(".apt").text(suggestion.city);
		$(this).siblings(".city").text(suggestion.type);
		$(this).siblings("#" + $(this).attr("id") + "_country")
			.val(suggestion.country);
		$(this).siblings(".j-countryCode").val(suggestion.code);
		$(this).siblings("#Destination").val(suggestion.type);
		$(this).siblings("#destinationKeyCityCode")
			.text(suggestion.keyCityCode);
		$(this).siblings("#destinationKeyCountryCode")
			.text(suggestion.keyCountryCode);
		$(this).siblings("#destinationKeyAirportName")
			.text(suggestion.keyAirportName);
		$(this).closest('.customInput--flightFinder').addClass('selected');
		$('input.j-typeOfFlight').eq(0).focus();
		datePickerController.show('andata');
	}
	
	function onSelectArrivalMultileg(suggestion) {
		$(this).val(suggestion.value);
		$(this).siblings(".apt").text(suggestion.city);
		$(this).siblings(".city").text(suggestion.type);
		$(this).siblings(".destination").val(suggestion.type);
		$(this).siblings(".destinationKeyCityCode")
			.text(suggestion.keyCityCode);
		$(this).siblings(".destinationKeyCountryCode")
			.text(suggestion.keyCountryCode);
		$(this).siblings(".destinationKeyAirportName")
			.text(suggestion.keyAirportName);
		$(this).closest('.customInput--flightFinder').addClass('selected');
		$('input.j-typeOfFlight').eq(0).focus();
	}
	
	function getAutocompleteSettings(onSelect, appendTo) {
		return {
			lookup : function(query, done) {
				if (findAirport(query)) {
					done(airports);
				}
			},
			preserveInput : false,
			appendTo : appendTo,
			minChars : getAutocompleteSize(),
			autoSelectFirst: true,
			onSearchComplete : function(val, suggestions) {
				for (var j = 0; j < suggestions.length; j++) {
					var obj = suggestions[j];
					obj.value = obj.city + " " + obj.type;
				}
			},
			formatResult : function(suggestion, currentValue) {
				return '<div class="sugg_dest_data">'
						+ '<span class="sugg_city_name toPass '
						+ (suggestion.best ? "best" : "") + '">'
						+ suggestion.city
						+ '</span> - <span class="sugg_city_nation ">'
						+ suggestion.country
						+ '</span><br><span class="sugg_airport">'
						+ suggestion.airport
						+ '</span><span class="sugg_airportCode">('
						+ suggestion.code + ')</span></span></div>';

                <div class=\"autocomplete-suggestion-horiz\" data-index=\"0\"><div class=\"sugg_dest_data-horiz\"><span class=\"sugg_city_name-horiz toPass \">" + item.city  + "</span> - <span class=\"sugg_city_nation-horiz\">" + item.country + "</span><br><span class=\"sugg_airport-horiz\">" + item.airport + "</span><span class=\"sugg_airportCode-horiz\">(" + item.code + ")</span></div></div>


			},
			onInvalidateSelection : function() {
			},
			onSelect : onSelect
		};
	}


	$('#departureInput').autocomplete(getAutocompleteSettings(onSelectDeparture, $('#departureInput').parent()));
	$('#arrivalInput').autocomplete(getAutocompleteSettings(onSelectArrival, $('#arrivalInput').parent()));
	
	$('.departure.autoComplete-multileg').each(function(index){
		$(this).autocomplete(getAutocompleteSettings(onSelectDepartureMultileg, $('.departure.autoComplete-multileg').eq(index).parent()))
		});
	$('.arrival.autoComplete-multileg').each(function(index){
		$(this).autocomplete(getAutocompleteSettings(onSelectArrivalMultileg, $('.arrival.autoComplete-multileg').eq(index).parent()))
		});
*/
	
});

function getAirportsList() {

	var url = getServiceUrl("airportslistconsumer");
	$.ajax({
		url : url,
		context : document.body
	}).done(function(data) {
		all_airports = data.airports;

        //**ATTTENZIONE TOGLIERE DA QUI' E RI ABILITARE SOTTO
        //**MESSO QUI' XChe GEOLOC NON FUNI
		//geoAirport = data.airports;
        populateDepartureInput();
        //**ATTTENZIONE TOGLIERE DA QUI' E RI ABILITARE SOTTO        
		var url = getServiceUrl("airportslistconsumergeo");
		$.ajax({
			url : url,
			context : document.body
		}).done(function(data) {
			geoAirport = data.geoAirport;
			populateDepartureInput();
		});
	});

}

var all_airports;
var geoAirport;
var airports = {
	"query" : "Unit",
	"suggestions" : []
};

function copyValue(v) {
	var c = {
		"value" : v.value,
		"city" : v.city,
		"country" : v.country,
		"airport" : v.airport,
		"type" : v.type,
		"code" : v.code,
		"best" : v.best,
		"keyCityCode" : v.keyCityCode,
		"keyCountryCode" : v.keyCountryCode,
		"keyAirportName" : v.keyAirportName
	};
	return c;
}

function getAutocompleteSize() {
	return ["zh","ja","ko"].indexOf(languageCode) > -1 ? 2 : 3;
}

function findAirport(query) {
	if (query && query.length >= getAutocompleteSize()) {
		var size = 7;
		airports.suggestions = [];
		airports.keys = [];
		var regExString = "^" + query + ".*";
		jQuery.each(all_airports, function(i, v) {
			if (airports.keys.length < size
					&& v.city.search(new RegExp(regExString, "i")) != -1) {
				if (-1 === airports.keys.indexOf(v.code)) {
					airports.keys.push(v.code);
					var c = v;
					airports.suggestions.push(copyValue(v));
				}
			}
		});
		jQuery.each(all_airports, function(i, v) {
			if (airports.keys.length < size
					&& v.airport.search(new RegExp(regExString, "i")) != -1) {
				if (-1 === airports.keys.indexOf(v.code)) {
					airports.keys.push(v.code);
					var c = v;
					airports.suggestions.push(copyValue(v));
				}
			}
		});
		if (query.length == 3) {
			jQuery.each(all_airports, function(i, v) {
				if (airports.keys.length < size
						&& v.code.search(new RegExp(regExString, "i")) != -1) {
					if (-1 === airports.keys.indexOf(v.code)) {
						airports.keys.push(v.code);
						airports.suggestions.push(copyValue(v));
					}
				}
			});
		}
		return true;
	}
	return false;
}

function getAirportData(query) {
	if (query && query.length == 3) {
		for (var i = 0; i < all_airports.length; i++) {
			if (all_airports[i].code == query) {
				return all_airports[i];
			}
		}
	}
	return {};
}

function searchFlightUniquefy(items, type) {
	var unique_items = new Array();
	for (var i = 0; i < items.length; i++) {
		if ("DEP" === type) {
			if (!unique_items[items[i].cittaPartenza.codiceAeroporto]) {
				unique_items[items[i].cittaPartenza.codiceAeroporto] = items[i];
			}
		} else {
			if (!unique_items[items[i].cittaArrivo.codiceAeroporto]) {
				unique_items[items[i].cittaArrivo.codiceAeroporto] = items[i];
			}
		}
	}
	var items = new Array();
	for (var i in unique_items){
		items.push(unique_items[i]);
	}
	return items;
}

function populateWithRecentSearch(e, input, type) {
	try {
		var $apt = jQuery(input.parent().find('span.apt-horiz')[0]);
		var $city = jQuery(input.parent().find('span.city-horiz')[0]);
		var $autocompleteSuggestionsDiv =
			jQuery(input.parent().find('div.autocomplete-suggestions-horiz')[0]);
		var items = localStorage.getItem("ricercheRecenti");

		if ((items != null && items != 'undefined' && items != '[]' 
				&& items.length > 0)
				) {

            var suggestionContainertoHide = input.parent()
				.find(".flightFinder__suggest");

            /*
			var suggestionContainer = input.parent()
				.find(".flightFinder__suggest").find('#recently');
			*/

			var suggestionContainer = suggestionContainertoHide.find('#recently');

			items = searchFlightUniquefy(JSON.parse(items), type);
			var length = (items.length > 5 ? 5 : items.length);
			var aptRecentlySearched='';
			for (var i = 0; i < length; i++) {
				var citta = (type === 'DEP' ? items[i].cittaPartenza
						: items[i].cittaArrivo);

					
				aptRecentlySearched+=''
                + '<a href="javascript:;"style="text-decoration: none !important; border-bottom: 1px solid #fff;" '
					+'data-index="' + i + '">'
					+ '<div class="sugg_dest_data-horiz">'
					+ '<span class="sugg_city_name-horiz toPass">'
					+ (citta.nomeT || citta.nome)
					+ '</span> - <span class="sugg_city_nation-horiz">'
					+ (citta.nazioneT || citta.nazione)
					+ '</span><br>'
					+ '<span class="sugg_airport-horiz">'
					+ (citta.aeroportoT || citta.aeroporto)
					+ '</span>'
					+ '<span class="sugg_airportCode-horiz">'
					+ '(' + citta.codiceAeroporto +')</span>'
					+ '</div>' + '</a>';

			}
			jQuery(suggestionContainer).empty().append(aptRecentlySearched);
			jQuery(suggestionContainer).find('a').unbind("click");


			jQuery(suggestionContainer).find('a').bind("click", function(e) {

				e.preventDefault();

				var city = $(this).find('.sugg_city_name-horiz').text();
				var apt = $(this).find('.sugg_airportCode-horiz').text().replace('(','').replace(')','');

				var whereTo = $(this).closest('.customInput-horiz--flightFinder');
				whereTo.find('.apt-horiz').text(apt);
				whereTo.find('.city-horiz').text(city);
				whereTo.find('.j-autoCompleteInput-horiz').val(city + ' ' + apt);
				whereTo.find('.j-countryCode').val(apt);
                whereTo.find('.city_sugg').hide();

//
               jQuery(suggestionContainertoHide).hide();
//

				if ( whereTo.hasClass('arrival') ){
				$('input.j-typeOfFlight').eq(0).focus();
					datePickerController.show('andata');
				}
			});
	
			jQuery(suggestionContainer).parent().show();
			jQuery(suggestionContainer).parent().css("z-index", "999");
		}
	} catch(e) {}
}


function populateDepartureInput() {
	try {
		var $apt = jQuery(jQuery("#departureInputHoriz").parent().find('span.apt-horiz')[0]);
		var $city = jQuery(jQuery("#departureInputHoriz").parent().find('span.city-horiz')[0]);
		var $Origin = jQuery(jQuery("#departureInputHoriz").parent().find('#Origin')[0]);
        var $city_sugg = jQuery(jQuery("#departureInputHoriz").parent().find('span.city_sugg')[0]);

		var items = localStorage.getItem("ricercheRecenti");
		if (items != null && items != 'undefined' && items != '[]'
				&& items.length > 0) {
			items = JSON.parse(items);
			$apt.empty();
			$apt.append(items[0].cittaPartenza.nomeT || items[0].cittaPartenza.nome);
			$city.empty();
			$city.append(items[0].cittaPartenza.codiceAeroporto);
	
			$Origin.val(items[0].cittaPartenza.codiceAeroporto);
			jQuery("#originKeyCityCode").text(items[0].cittaPartenza.nome);
			jQuery("#originKeyCountryCode").text(items[0].cittaPartenza.nazione);
			jQuery("#originKeyAirportName").text(items[0].cittaPartenza.aeroporto);
            $city_sugg.hide();
		} else {
			if (typeof geoAirport != "undefined" && geoAirport.city
					&& geoAirport.country && geoAirport.airport) {
				$apt.empty();
				$apt.append(geoAirport.city);
				$city.empty();
				$city.append(geoAirport.code);
				$Origin.val(geoAirport.code);
				jQuery("#originKeyCityCode").text(geoAirport.city);
				jQuery("#originKeyCountryCode").text(geoAirport.country);
				jQuery("#originKeyAirportName").text(geoAirport.airport);
                $city_sugg.hide();
			}
		}
		var departureInput = $apt.text() + " " + $city.text();
		if(departureInput != " "){
			$('#departureInputHoriz').val(departureInput);
        }
	} catch(e) {}
}

/*
function populateDepartureInput() {
	try {

		var $apt = jQuery(jQuery("#departureInputHoriz").parent().find('span.apt-horiz')[0]);
		var $city = jQuery(jQuery("#departureInputHoriz").parent().find('span.city-horiz')[0]);
		var $Origin = jQuery(jQuery("#departureInputHoriz").parent().find('#Origin')[0]);
        var $city_sugg = jQuery(jQuery("#departureInputHoriz").parent().find('span.city_sugg')[0]);
		debugger;
		var items = localStorage.getItem("ricercheRecenti");
		if (items != null && items != 'undefined' && items != '[]'
				&& items.length > 0) {
			items = JSON.parse(items);
			$apt.empty();
			$apt.append(items[0].cittaPartenza.nomeT || items[0].cittaPartenza.nome);
			$city.empty();
			$city.append(items[0].cittaPartenza.codiceAeroporto);
	
			$Origin.val(items[0].cittaPartenza.codiceAeroporto);
			jQuery("#originKeyCityCode").text(items[0].cittaPartenza.nome);
			jQuery("#originKeyCountryCode").text(items[0].cittaPartenza.nazione);
			jQuery("#originKeyAirportName").text(items[0].cittaPartenza.aeroporto);
            $city_sugg.hide();
		} else {
			if (typeof geoAirport != "undefined" && geoAirport[0].city
					&& geoAirport[0].country && geoAirport[0].airport) {
				$apt.empty();
				$apt.append(geoAirport[0].city);
				$city.empty();
				$city.append(geoAirport[0].code);
				$Origin.val(geoAirport.code);
				jQuery("#originKeyCityCode").text(geoAirport[0].city);
				jQuery("#originKeyCountryCode").text(geoAirport[0].country);
				jQuery("#originKeyAirportName").text(geoAirport[0].airport);
                $city_sugg.hide();
			}
		}
		var departureInput = $apt.text() + " " + $city.text();
		if(departureInput != " "){
			$('#departureInputHoriz').val(departureInput);
        }
	} catch(e) {}
}
*/

jQuery(document).ready(function() {

	getAirportsList();
	jQuery(document).mouseup(function(e) {
		var containerDeparture = jQuery('.customInput-horiz--flightFinder.departure');
		var containerArrival = jQuery('.customInput-horiz--flightFinder.arrival');

		if (!containerDeparture.is(e.target)
				&& containerDeparture.has(e.target).length === 0) {
			hideRecentSearch(e, containerDeparture);
		}
		if (!containerArrival.is(e.target)
				&& containerArrival.has(e.target).length === 0) {
			hideRecentSearch(e, containerArrival);
		}
	});

	jQuery("#departureInputHoriz").bind('focus', function(e) {
	    populateWithRecentSearch(e, jQuery(this), "DEP");
	});

	jQuery("#arrivalInputHoriz").bind('focus', function(e) {
	    populateWithRecentSearch(e, jQuery(this), "ARR");
	});

	jQuery("#departureInputHoriz").keypress(function(e) {
		hideRecentSearch(e, jQuery(this));
	});

	jQuery("#arrivalInputHoriz").keypress(function(e) {
		hideRecentSearch(e, jQuery(this));
	});

	function hideRecentSearch(e, input) {
		suggestionContainer = input.parent().find(".flightFinder__suggest");
		jQuery(suggestionContainer).hide();
	}
});