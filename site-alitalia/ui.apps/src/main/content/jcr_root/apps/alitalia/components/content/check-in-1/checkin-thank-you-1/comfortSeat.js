"use strict";
use(function () {

    var pax = this.pax;
    var emd = this.emd;
    var labelComfort = 'checkin.mail.ancillary.seat';
    var label = 'checkin.confirmation.recap.seat';
    var seat = {
        label: label,
        num : pax.numeroPostoAssegnato
    };
    for (var y = 0; y < pax.emd.size(); y++) {
        var emd = pax.emd.get(y);
        if (emd.group == 'SA') {
            if (emd.pdcSeat.substring(0,1) == "0"){
                emd.pdcSeat = emd.pdcSeat.substring(1,emd.pdcSeat.length());
            }
            if (emd.pdcSeat == pax.numeroPostoAssegnato ) {
                seat = {}
                seat = {
                    label: labelComfort,
                    num: emd.pdcSeat
                };
            }
        };
    }
    return seat;
})
