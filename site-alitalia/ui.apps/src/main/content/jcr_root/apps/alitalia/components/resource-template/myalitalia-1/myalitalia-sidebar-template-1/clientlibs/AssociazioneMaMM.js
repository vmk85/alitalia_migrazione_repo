$(document).ready(function () {
    $("#associaMM").click(function () {
       var data = {
            url:window.location.href
        }
        performSubmit("associazioneMaMM",data,associazioneMMSuccess,associazioneMMError);
    });
});


function associazioneMMSuccess(data) {
    if (!data.isError){
        redirect('./special-pages/associa-myalitalia.html');
    } else {
        console.log("errore url in sessione");
    }
}

function associazioneMMError(data) {
    console.log("errore connessione servlet"+ data.toString());
}