"use strict";
use(function () {

    var segmentsJ = this.segments;
    var passenger = this.pax;
    var emdSeat = new Array();

    for (var i = 0; i < segmentsJ.size(); i++) {
        var segment = segmentsJ.get(i);
        if (segment.passengers!=null){
            for (var x = 0; x < segment.passengers.size(); x++) {
                var pax = segment.passengers.get(x);
                if (pax.nome == passenger.nome && pax.cognome == passenger.cognome && pax.emd.size() != null) {
                    for (var y = 0; y < pax.emd.size(); y++) {
                        var emd = pax.emd.get(y);
                        if (emd.group == 'SA') {
                            if (emd.pdcSeat.substring(0,1) == "0"){
                                emd.pdcSeat = emd.pdcSeat.substring(1,emd.pdcSeat.length());
                            }
                            var seat = {
                                origin: segment.origin.city + " - " + segment.destination.city,
                                code : emd.number,
                                num : emd.pdcSeat
                            };
                            emdSeat.push(seat);
                        }
                    }
                }
            }
        }
    }
    return emdSeat;
});