/*evento alla chiusura del browser*/
window.onbeforeunload = clearTBIAncillaryFromCart;
window["cancelCart"] = true;


function initPartialCheckinCart() {
	$(".remove-ancillary-cart").click(checkinAncillaryRemoveFromCart);
	$('.j-responsiveTable').responsiveTable({
		  maxWidth: 640
	});
	book.init();
}

/* remove from cart */

function checkinAncillaryRemoveFromCart(e) {
	e.preventDefault();
	var split = e.target.id.split('#');
	var form = {
			ancillaryType: split[0],
			passengerIndex: split[1]
	}
	performSubmit('checkinremoveancillaryfromcart', form, 
			checkinAncillaryRemoveFromCartSubmitSuccess);
	startPageLoader(true);
}

function checkinAncillaryRemoveFromCartSubmitSuccess(data) {
	if (data.redirect) {
		startPageLoader(false);
		//window.location.replace(data.redirect);
		//return;
	} else {
		startPageLoader(false);
		if (data.ancillaryType == 'Seat' || data.ancillaryType == 'Upgrade') {
			refreshCheckinPartialSeat(function(){
				reloadSeatmapChoose(false);
			});
		} else if (data.ancillaryType == 'ExtraBaggage') {
			reloadExtraBaggage(function() {
				refreshCheckinPartialCart(false);
			});
		} else if (data.ancillaryType == 'Insurance') {
			refreshCheckinPartialInsurance(false, false);
		}
	}
}


/*Chiusura browser*/

$("#cartProsegui").click(function(){
	window["cancelCart"] = false;
});

$("#ancillaryProsegui").click(function(){
	window["cancelCart"] = false;
});


function clearTBIAncillaryFromCart(){
	if(window["cancelCart"] && !window["isAncillaryConfirmation"]){
		clearCart_sendSyncXHR("checkincancelcartconsumer")
	}
}

function clearCart_sendSyncXHR(serviceName){
	/*jQuery refused to send a synchronous request in firefox*/
	var xhttp;
	if (window.XMLHttpRequest) {
	    // code for modern browsers
		xhttp = new XMLHttpRequest();
	 } else {
	    // code for old IE browsers
		 xhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xhttp.open('POST', getServiceUrl(serviceName), false);  // `false` makes the request synchronous
	xhttp.send(null);
	if (xhttp.status === 200) {
		/*OK*/
	}
}

/*Fine chiusura browser*/