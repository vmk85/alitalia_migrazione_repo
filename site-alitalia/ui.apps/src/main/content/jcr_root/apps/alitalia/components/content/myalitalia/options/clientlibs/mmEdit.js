var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
}

if(mm<10) {
    mm = '0'+mm
}

today = yyyy + '-' + mm + '-' + dd;


/* Dati Login Edit */
$("#datiLoginSubmit").on("click", function(e) {
    mmSupport.validation(e, 'datiloginsubmit', '#datiLoginEdit', datiLoginEditSuccess, datiLoginEditError);
	return false;
});

function datiLoginEditSuccess(data) {
    if(data.result) 
        $("#datiLoginEdit").submit();
    else alert("error");
}

function datiLoginEditError() {
    alert("error");
}

/* Comunicazione Offerte Edit */
$("#comunicazioniOfferteProfSubmit").on("click", function(e) {
enableLoaderMyAlitalia();
    var canalePreferitoArray = [];

    /** Variabile per set dati CRM*/
    var canaleComunicazionePreferito = [];

    if($("#notification-email").is(":checked")) {
        canalePreferitoArray.push("email");
    }
    canaleComunicazionePreferito.push({ tipo : "mail" , selezionato : $("#notification-email").is(":checked") , dataUltimoAggiornamento : today });


    if($("#notification-app").is(":checked")) {
        canalePreferitoArray.push("app");
    }
    canaleComunicazionePreferito.push({ tipo : "push" , selezionato : $("#notification-app").is(":checked") , dataUltimoAggiornamento : today });


    if($("#notification-sms").is(":checked")) {
        canalePreferitoArray.push("sms");
    }
    canaleComunicazionePreferito.push({ tipo : "sms" , selezionato : $("#notification-sms").is(":checked") , dataUltimoAggiornamento : today });

    $("#checkSms").val(true);


    $("#tipoContatto").val(canalePreferitoArray.join(":"));

    if($(this).hasClass("isMM")) mmSupport.validation(e, 'comunicazionioffertesubmit', '#comunicazioniOfferteProfEdit', comunicazioniOfferteEditSuccess, comunicazioniOfferteEditError);
    else if($(this).hasClass("isMA")) {
        var dataToUpdate = {};
        var currentDate = new Date().toISOString();

        var canalePreferito = 0;
        if($("#notification-email").is(":checked")){
            canalePreferito += 1;
        }
        if($("#notification-app").is(":checked")){
            canalePreferito += 5;
        }
        if($("#notification-sms").is(":checked")){
            canalePreferito += 9;
        }


        //var datiPersonali = $("[name='consenso-dati-personali']:checked").val();
        var newsletter = $("[name='newsletter']:checked").val();
        var profiling = $("[name='profilazione']:checked").val();
        var lingua = $("#linguaCorrispondenza").val();
        // var smsAuthorization = $("#notification-sms").is(":checked");
        // var datiPersonali = true;
        //
        //
        // var isAuthComModified = false;
        // var isAuthDataModified = false;
        // var isAuthProfileModified = false;

        // var auth_Com = userMA.data.auth_Com ? userMA.data.auth_Com : '';
        // var auth_Data = userMA.data.auth_Data ? userMA.data.auth_Data : '';
        // var auth_Profile = userMA.data.auth_Profile ? userMA.data.auth_Profile : '';
        // if(auth_Com!=newsletter){
        //     isAuthComModified = true;
        // }
        // if(auth_Data!=datiPersonali){
        //     isAuthDataModified = true;
        // }
        // if(auth_Profile!=profiling){
        //     isAuthProfileModified = true;
        // }
        //
        // dataToUpdate.auth_Com=newsletter;
        // dataToUpdate.auth_Data=datiPersonali;
        // dataToUpdate.auth_Profile=profiling;
        // dataToUpdate.generalPref_Lang01=lingua;
        // dataToUpdate.generalPref_Channel=canalePreferito;
        // dataToUpdate.smsAuthorization=smsAuthorization;
        //
        // if(isAuthComModified){
        //     dataToUpdate.auth_Com_Update = currentDate;
        // }
        // if(isAuthDataModified){
        //     dataToUpdate.auth_Data_Update = currentDate;
        // }
        // if(isAuthProfileModified){
        //     dataToUpdate.auth_Profile_Update = currentDate;
        // }

        // var params = {
        //     data: dataToUpdate,
        //     callback: function(resp) {
        //         // console.log(resp);
        //         saveUserMAToModel();
        //         // myAlitaliaOptions.feedbackSuccess();
        //
        //     }
        // };
        // gigya.accounts.setAccountInfo(params);


        /** Chiamata alla servlet MyAlitalia per la comunicazione con il CRM ( set dei dati ) */
        var crmRequest = { data:JSON.stringify({
                listaConsensi:[
                    {
                        tipoConsenso:"COM",
                        flagConsenso:newsletter,
                        dataUltimoAggiornamento: today
                    },
                    {
                        tipoConsenso:"PRN",
                        flagConsenso:profiling,
                        dataUltimoAggiornamento: today
                    },{
                        tipoConsenso:"PRO",
                        flagConsenso:profiling,
                        dataUltimoAggiornamento: today
                    }
                ],
                preferenzePersonali:{
                    linguaComunicazioniCommerciali:lingua,
                    linguaComunicazioniServizio:lingua
                },
                canaleComunicazionePreferito:canaleComunicazionePreferito
            })
        };

        performSubmit("SetMyAlitaliaCrmDataConsensi",crmRequest,Login.Session.successmmEDIT,Login.Session.error);
    }
	return false;
});

function comunicazioniOfferteEditSuccess(data) {
    if(data.result) $("#comunicazioniOfferteProfEdit").submit();
    else alert("error");
}

function comunicazioniOfferteEditError() {
    console.log("error");
}