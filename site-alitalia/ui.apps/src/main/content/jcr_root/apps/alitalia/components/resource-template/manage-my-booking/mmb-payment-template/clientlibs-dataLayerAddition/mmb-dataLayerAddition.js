var DTMRule = "";

function analytics_mmbPartialCallback(){
	addMmbAnalyticsData(window['analytics_mmb_step']);
	addMmbDataLayerVar();
	activateGTMOnce("mmb");
	activateWebtrendsOnce("mmb");
	if(DTMRule){
		activateDTMRule(DTMRule);
	}
}

function addMmbAnalyticsData(mmbStep){
		switch(mmbStep){
		case "3":
			DTMRule = "MMBPayment";
			break;
		default:
			console.log("Invalid mmb step");
		}	
	}

function addMmbDataLayerVar(){
	if(typeof window["analytics_mmb_dl_var"] == "object"){
		addAnalyticsData(window["analytics_mmb_dl_var"]);
	}
}

function initMmb_tracking(){
	jQuery(".j-analytics-mmb-click").off("mousedown", onMmbTrackingClick);
	jQuery(".j-analytics-mmb-click").on("mousedown", onMmbTrackingClick);
	jQuery(".j-analytics-mmb-cc").off("mousedown", onMmbCCSelected);
	jQuery(".j-analytics-mmb-cc").on("mousedown", onMmbCCSelected);
}

/**
 * selezione carta di pagamento
 */
function onMmbCCSelected(){
	jQuery("meta[name='WT.si_x']").attr("content", "2");
}

/**
 * click sui tasti dei box
 * e' possibile scatenare piu' di un evento separandoli con 
 * una virgola nell'attributo data-evt
 */
function onMmbTrackingClick(){
	var evtNames = $(this).attr("data-evt").split(",");
	var stepNames = $(this).attr("data-step").split(",");
	var boxNames = $(this).parents(".j-analytics-mmb-box").attr("data-box").split(",");
	for(ind in evtNames){
		var evtName = evtNames[ind];
		var stepName = stepNames[ind % stepNames.length];
		var boxName = boxNames[ind % boxNames.length];
		console.log("evtName: " + evtName + "; stepName: " + stepName + "; boxName: " + boxName);
		if(typeof window["mmb_dcs_multitrack_btn_click"] != "undefined"){
			mmb_dcs_multitrack_btn_click(evtName, stepName, boxName);
		}
	}
}


function analytics_trackMmbErrors(errors){
	var errorMessages = "";
	/*compute error messages*/
	if(typeof errors === "object"){
		for(key in errors){
			errorMessages = errorMessages + key + " "  + errors[key] + "; ";
		}	
		if(errorMessages.length > 0)
			errorMessages = errorMessages.slice(0,-2);
	}
	else if (typeof errors === "string"){
		errorMessages = errors;
	}
	if(errorMessages.length > 0){
		pushAnalyticsData({"MMBError": errorMessages});
		activateDTMRule("MMBError");
	}
}	