var cercaVoloHistoryOptions = {};

// document ready
$( document ).ready( function() {
  cercaVoloHistoryOptions.openPanel();
	cercaVoloHistoryOptions.closePanel();
} );



cercaVoloHistoryOptions.openPanel = function() {
  $('.cta-history--form').click(
    function() {
      if($('.section--cerca-volo').hasClass('on-focus')){
        $('.history-panel').slideToggle();
        $(this).toggleClass('active');
      } else {
        cercaVoloOptions.openOverlayDesktop();
        $('.history-panel').slideToggle();
        $(this).toggleClass('active');
      }
    }
  );
};
cercaVoloHistoryOptions.closePanel = function() {
  $('.cta-close-panel').click(
    function() {
        $('.history-panel').slideUp();
    }
  );
};
