

$( document ).ready( function() {

        if(email=='') {
            $('#newsletterunsubsubmit').bind('click', newsletterUnsubHandler);
        }else{
            //debugger;
            //$("input#email").attr("disabled", true);
            $("input#email").val(email);
        }

} );

/*function getEmailAddressFromQuerystring() {
    var sPageURL = decodeURIComponent(window.location.search.substring(1))
    var sURLVariables  = sPageURL.split("&");
    var sParameterName;

    for (var i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === 'a') {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
    return "";
}*/

function newsletterUnsubscribeSuccess(data) {

    if (data.isError) {
        showFormFeedbackError("specialpageform_form-newsletter-unsub",data.errorMessage);
    }else{
        window.location.href = "./newsletter-unsub.html?success=true";
    }

}

function newsletterUnsubscribeFail(data) {

    window.location.href = "./newsletter-unsub.html?success=false";
}

function newsletterUnsubHandler(e) {
    validation(e, 'newsletterunsubsubmit', '#specialpageform_form-newsletter-unsub', newsletterUnsubSuccess, newsletterUnsubError);
}

function newsletterUnsubSuccess(data) {
    return (data.result ? performSubmit('newsletter-unsubscribe-crm', '#specialpageform_form-newsletter-unsub', newsletterUnsubscribeSuccess, newsletterUnsubscribeFail) : showErrors(data));
}

function newsletterUnsubError() {
    return newsletterUnsubContinue(true);
}

function newsletterUnsubContinue(stop) {
    removeErrors();
    $('#newsletterunsubsubmit').unbind('click', newsletterUnsubHandler);
    return stop || $('#newsletterunsubsubmit').click();
}