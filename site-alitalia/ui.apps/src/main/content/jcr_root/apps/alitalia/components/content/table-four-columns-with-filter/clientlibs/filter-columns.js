$( document ).ready(function() {
	$(".tableFilter").each(
        function(index){
         $(this).empty();
         $(this).append('<option value="*" selected>' + CQ.I18n.get("alitalia.support.common.all") + '</option>');
         $(this).parents(".wrapperFilter").find("table td").filter(":first-child").each(
				function(index2){
					filter = $(this).parents(".wrapperFilter").find(".tableFilter");
					key = $(this).text();
					exist = 0 != filter.children('option[value="'+key+'"]').length;
					if(!exist){
						filter.append('<option value="' + key + '">' + key + '</option>');
					}	
				}
			);
    	}
    );

    $(".tableFilter").on('change', function() {
    	var selectValue = $(this).val();
    	$(this).parent().parent().find("table td").filter(":first-child").each(
            function(index){
                if($(this).text() != selectValue && ($(this).text() != '*')){
                    $(this).parent().hide();
                }
                if(($(this).text() == selectValue) || selectValue == '*'){
                    $(this).parent().show();
                }
            }
		);
	});
});

