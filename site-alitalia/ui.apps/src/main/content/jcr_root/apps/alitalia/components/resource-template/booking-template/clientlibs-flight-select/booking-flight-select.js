function performInitialFlightSearch(errorUrl) {
	var isResidency = getParameterByName("residency") || "false";
	var status = 0;
	checkBookingNavigationProcess("flightsSearch", function(){
		executeFlightSearch(errorUrl);
	});
}

function executeFlightSearch(errorUrl){
	var isResidency = getParameterByName("residency") || "false";
	var params = { isResidency: isResidency };
	// start airplane
	resultsLoading.init();
	performSubmit("bookingflightsearchserviceconsumer", params, function(data){
		flightSearchDoneCallback(data, errorUrl);
	}, function(){
		flightSearchFailCallback(errorUrl);
	});
}

function flightSearchDoneCallback(data, errorUrl) {
	if (data.redirect) {
		window.location.replace(data.redirect);
	} else if (data.isError) {
		window.location.replace(errorUrl);
	} else {
		displayInitialFlightSearchResult(data);
	}
}

function flightSearchFailCallback(errorUrl) {
	window.location.replace(errorUrl);
}

/**
 * Ricerca effettuata. Nel JSON di risposta verifico se sono state
 * trovate soluzioni per la data richiesta, o in alternativa per 
 * delle date a ridosso della data richiesta
 * @param data
 */
function displayInitialFlightSearchResult(data) {
	var messageId = false;
	if (data) {
		messageId = checkAvailableSolution(data);
	}
	refreshMainPartial(messageId);
}

/**
 * Verifica la presenza di soluzioni ottenute dalla ricerca corrente
 * @param data
 */
function checkAvailableSolution(data) {
	if (data.solutionNotFound == "allSolutions") {
		window.location.replace(data.redirect);
	} else if (data.solutionNotFound == "youth") {
		return "alertYouth";
	} else if (data.solutionNotFound == "family") {
		return "alertFamily";
	} else if (data.solutionNotFound == "military") {
		return "alertMilitary";
	} else if (data.solutionNotFound == "territorialContinuity") {
		return "alertTerritorialContinuity";
	}
}

function showErrorMessage(message) {
	$("#genericErrorMessageDiv .genericErrorMessage__text").text(message);
	$("#genericErrorMessageDiv").show();
}

function hideErrorMessage() {
	$("#genericErrorMessageDiv").hide();
}

function refreshBindings() {
	/*Select del criterio di ordinamento*/
	$("[id^=select__timeFilter-]").off("change", filterAndOrderFlights);
	$("[id^=select__timeFilter-]").change(filterAndOrderFlights);
	/*checkbox mostra solo voli diretti*/
	$("[id^=checkbox_directFlightOnly-]").off("change", filterAndOrderFlights);
	$("[id^=checkbox_directFlightOnly-]").change(filterAndOrderFlights);
	$(".j-businessTrigger a").off("click", insertAriaLabelonArrow);
	$(".j-businessTrigger a").click(insertAriaLabelonArrow);
	$(".j-businessTrigger").off("click", businessTrigger);
	$(".j-businessTrigger").click(businessTrigger);
	$(".j-priceSelector").off("click", onFlightPriceSelection);
	$(".j-priceSelector").click(onFlightPriceSelection);
}

function removeEmptyRichText() {
	$(".editorialArea.regularTariff").each(function(i,elem){
		if ($(elem).find("p[data-emptytext]").length > 0){
			$(elem).remove();
		}
	});
}

function bindRibbonEvents(){
	$(".booking__dateListItem").not(".isActive").find(".dateBlockWrapper").not(".notAvailable").unfastClick();
	$(".booking__dateListItem").not(".isActive").find(".dateBlockWrapper").not(".notAvailable").fastClick(changeTab);
}

function bindTableEvents(){
	/*$('.j-loadMoreBookingRow').fastClick(function(e){
		e.preventDefault()
		var _loader = $(e.currentTarget).closest('.j-bookingTable').find('.j-bookingLoader')
		_loader.velocity('transition.slideDownIn', {
			duration: 250,
			complete: function(){

				be_loadMoreFlights($(this), function(new_element){
					new_element.find('.j-priceSelectorCol').fastClick( book.priceSelectorCol );
					new_element.find('.j-priceSelector').fastClick( book.priceSelector );
					new_element.find('.j-businessTrigger').fastClick( book.businessTrigger );
					$('.j-flightDetails').fastClick(function(e){
						book.getFlightDetails($(e.currentTarget));
					});
					new_element.find('.j-mobile-openOptions').fastClick( book.openOptionMobile );
					new_element.find('.j-closeTableRow').fastClick( book.priceSelectorReset );
					new_element.find('.j-goToReturn').fastClick( book.goToReturn );
					new_element.find('.j-selectReturn').fastClick( book.selectedReturn );
					_loader.velocity('transition.slideDownOut');
					$(window).trigger('resize.accordion');
					// Mehr da finire
				});

			}
		})	
	});
	
	$('.j-priceSelector').fastClick( book.priceSelector );
	$('.j-businessTrigger').fastClick( book.businessTrigger );
	$('.j-flightDetails').fastClick(function(e){
		book.getFlightDetails($(e.currentTarget))
	});
	$('.j-mobile-openOptions').fastClick( book.openOptionMobile );*/
}

function bindDetailsEvents(selector){
	$('.j-priceSelectorCol').fastClick( book.priceSelectorCol );
	$('.j-businessTrigger').unfastClick();
	$('.j-businessTrigger').fastClick(function(e){
		e.preventDefault();
		book.businessTrigger($(e.currentTarget));
	});
	$('.j-closeTableRow').fastClick( book.priceSelectorReset );
	$('.j-goToReturn').unfastClick().fastClick( book.goToReturn );
	$('.j-selectReturn').unfastClick().fastClick( book.selectedReturn );
	callAjaxLoad(selector.closest(".bookingTable__bodyRow"));
}

function bindBasketEvents(){
	$('.j-toggleBasketAccordion').unfastClick().fastClick(function(e){
		e.preventDefault();
		book.openBasket($(this));
	});

	if($('.j-toggleBasketAccordion').hasClass('opened')){
		var toShow = $('.j-toggleBasketAccordion').attr('href')
		$(toShow).show();
		window.bookingHeaderStop=true;
	}
	updateDatePicker();
	$('.j-openFlightFinder').fastClick(function(e){
		e.preventDefault();
		book.openBasket($(this));
		window.bookingHeaderStop=true;
	});
	$(".bookInfoBoxBasketBtn").attr("href", $(".selectSubmit").attr("href"));
	if (!$(".selectSubmit").hasClass("isDisabled")) {
		$(".bookInfoBoxBasketBtn").removeClass("isDisabled");
	}
	
	initAccordions();
	numberSelector();
}
/**
 * Funzioni per le chiamate AJAX per i refresh
 */

function invokeMainPartial(done, fail) {
	invokeService("flight-select-main-partial", "html", false, done, fail);
}

function invokeInfoBoxPartial(done, fail) {
	invokeService("flight-select-infoBox-partial", "html", false, done, fail);
}

function invokeRibbonPartial(indexRoute, done, fail) {
	invokeService("flight-select-ribbon-partial." + indexRoute, "html", false, done, fail);
}

function invokeTablePartial(indexRoute, more, isBusiness, done, fail) {
	orderType = $("#select__timeFilter-" + indexRoute).val();
	directFlightOnly = $("#checkbox_directFlightOnly-" + indexRoute).is(":checked");
	
	var data = {
		moreFlights: more,
		orderType: orderType,
		directFlightOnly: directFlightOnly,
		isBusiness: isBusiness
	};
	invokeService("flight-select-table-partial." + indexRoute, "html", data, done, fail);
}

function invokeFlightDetailsPartial(indexRoute, indexFlight, indexBrand, isBusiness, done, fail) {
	
	var data = {
		indexRoute: indexRoute,
		indexFlight: indexFlight,
		indexBrand: indexBrand,
		isBusiness: isBusiness
	};
	invokeService("flight-select-flight-details-partial", "html", data, done, fail);
}

function invokeChangeTab(indexRoute, selectedTab, done, fail) {
	
	var data = {
		indexRoute: indexRoute,
		selectedTab: selectedTab
	};
	invokeService("bookingchangetabconsumer", "json", data, done, fail);
}

function invokeConfirmSelection(indexRoute, indexFlight, indexBrand, solutionID, done, fail) {
	
	var data = {
		indexRoute: indexRoute,
		indexFlight: indexFlight,
		indexBrand: indexBrand,
		solutionId: solutionID
	};
	invokeService("bookingrefreshsearchconsumer", "json", data, done, fail);
}

/**
 * Funzioni di refresh dei vari partial
 */

function refreshLoan() {
	/*var show = $('#showLoan').val();
	if (show == 'true') {
		$('#loanElement').show();
		var price = $('#basketPrice').text();
		price = price.replace(",","");
		price = price.replace(".","");
		price = price.replace(" ","");
		$('#importo').val(price);
	}
	else {*/
		$('#loanElement').hide();
	//}
}

function refreshMainPartial(messageId) {
	invokeMainPartial(function(data) {
		$("#booking-flight-select-selection").html(data);
		$("header").show();
		$("footer").show();
		$(".booking__header").show();
		book.init();
		numberSelector();
		refreshBindings();
		removeEmptyRichText();
		initAccordions();
		updateDatePicker();
		bindRibbonEvents();
		applyBusinessFilter();
		bindPriceRecap();
		callAjaxLoad($(".j-bookingTableDeparture"));
		callAjaxLoad($(".j-bookingTableReturn"));
		createTooltips();
		$(window).trigger("resize");
		/*$(".selectSubmit").fastClick(function(e) {
			e.preventDefault();
			if (!$(this).hasClass("isDisabled")) {
				$(this).unfastClick();
				$(this).click();
			} else {
				console.log("button is disabled");
			}
		});*/
		if (messageId) {
			$("#" + messageId).show();
		}
		$(".waitingPage").hide();
		// Se provengo dalla call to action nelle special offers 
		// devo mostrare la ricerca aperta e il focus sul ritorno
		if ($(".j-openFlightFinderCover").is(":visible")) {
			$('#andata').focus();
			datePickerController.show("andata");
		}
		analytics_bookingPartialCallback();
		if (typeof cashAndMilesAjaxRefresh != 'undefined') {
			cashAndMilesAjaxRefresh();
		}
		/*fTestStep1();*/
	});
	
}
function refreshInfoBoxPartial() {
	invokeInfoBoxPartial(function(data) {
		var elemContent = $(data).find(".booking__informationBox.j-bookInfoBox").children();
		$(".booking__informationBox.j-bookInfoBox").html(elemContent);
		bindBasketEvents();
		refreshLoan();
		$(window).trigger("resize");
	});
}

function refreshRibbonPartial(indexRoute) {
	invokeRibbonPartial(indexRoute, function(data) {
		var typeRoute = indexRoute == "0" ? "Departure" : "Return";
		$(".booking__titleGroup.j-title" + typeRoute).remove();
		$(".booking__date" + typeRoute).replaceWith(data);
		//book.init();
		bindRibbonEvents();
	});
}

function refreshTablePartial(indexRoute, more, done) {
	var isBusiness = false;
	var style= $("div.bookingTable__rightInner").attr("style");
	if(style && (style.indexOf("left: -") >= 0 || style.indexOf("right: -") >= 0)){
		isBusiness = true;
	}
	
	invokeTablePartial(indexRoute, more, isBusiness, function(data) {
		var typeRoute = indexRoute == "0" ? "Departure" : "Return";
		$("#editorialAreaTopTable" + typeRoute).remove();
		$(".solutionForOtherDates" + typeRoute).remove();
		$(".bookingRecap.j-bookingRecap" + (indexRoute == "0" ? "Dep" : "Ret")).parent().remove();
		$('.j-showBackFlights.bookin__showBackFlights[data-flight="' + (indexRoute == "0" ? 'departure' : 'return') + '"]').remove();
		$(".j-bookingTable" + typeRoute).replaceWith(data);
		if (done) {
			done(jQuery(jQuery.parseHTML(data)));
		}
		book.init();
		refreshBindings();
		removeEmptyRichText();
		//bindTableEvents();
		applyBusinessFilter();
		bindPriceRecap();
		callAjaxLoad($(".j-bookingTable" + typeRoute));
		createTooltips();
		
	});
}

function refreshECouponPartial(insertedNow) {
	var url = removeUrlSelector() + ".ecoupon-partial.html";
	var data = {};
	if (insertedNow) {
		data.insertedNow = "true";
	}
	$.ajax({
		url: url,
		data: data,
		success: function(result) {
			$('#ecoupon-container').replaceWith(result);
			initAccordions();
			if (insertedNow) {
				$("#ecoupon-container").find(".bookingBox__title.j-accordion-header").click();
			}
			refreshInfoBoxPartial();
		}
	});
}

function applyBusinessFilter(){
	if ($(".bookingTable").attr("data-filter") == 'Business') {
		var cssDir = $("html").attr("dir") == "rtl" ? "right" : "left";
		$(".bookingTable__rightInner").css(cssDir, "-92%");
		$(".j-bookingTable").addClass("businessActivated");
	}
}

function updateDatePicker() {
	$("#andata").val($("#departureInputMod").val());
    if($('input.j-typeOfFlight:checked').val() !== 'a')
		$("#ritorno").val($("#arrivalInputMod").val());

	initDatePicker();

    if($('input.j-typeOfFlight:checked').val() === 'a')
        $('#ritorno').attr('disabled','disabled').attr('aria-disabled','true');
	
	$('input.j-typeOfFlight').on('change', function(event) {
		var _val = $(this).val();
		if ( _val === 'a' ){
			$('#ritorno').attr('disabled','disabled').attr('aria-disabled','true');
			$('#returnDate').attr('disabled','disabled').attr('aria-disabled','true').parent().velocity({opacity: .3});
			if ( $('#andata').val().length > 0 ){
				$('.flightFinder, .innerFlightFinder').removeClass('ff-stage4').addClass('ff-stage5');
			}
		} else {
			$('#ritorno').removeAttr('disabled').removeAttr('aria-disabled');
			$('#returnDate').removeAttr('disabled').removeAttr('aria-disabled').parent().velocity({opacity: 1});
		}
		highlightPeriod();
	});
}

/**
 * Funzione per il recupero dati al cambio del tab della data
 */
function changeTab() {
	var selectedTab = $(this).attr("data-tab-index");
	var indexRoute = $(this).attr("data-tab-route");
	startPageLoader(true);
	invokeChangeTab(indexRoute, selectedTab, function(data){
		if (indexRoute == 0) {
			refreshRibbonPartial(0);
			refreshTablePartial(0, false);
			if ($(".j-titleReturn").length > 0) {
				refreshRibbonPartial(1);
				refreshTablePartial(1, false);
			}
		} else {
			refreshRibbonPartial(0);
			refreshTablePartial(0, false);
			refreshRibbonPartial(1);
			refreshTablePartial(1, false);
		}
		startPageLoader(false);
		refreshInfoBoxPartial();
		changeFanPlayrRibbonClickData(data);
	}, function() { startPageLoader(false); });
}

/**
 * Funzione per il recupero dati del dettaglio del volo (al click su radio button)
 */
function be_priceSelector(selector, done) {
	var indexRoute = selector.attr("data-details-route");
	var indexFlight = selector.attr("data-details-flight");
	var indexBrand = selector.attr("data-index-brand");
	var type = selector.closest(".j-bookingTableDeparture").length ? "Dep" : "Ret";
	var style= selector.closest("div.bookingTable__rightInner").attr("style");
	var isBusiness = false;
	if(style && (style.indexOf("left: -") >= 0 || style.indexOf("right: -") >= 0)){
		isBusiness = true;
	}
	
	$(".j-bookingTable__header--" + (type == "Dep" ? "departure" : "return") + " .j-ajaxLoad").each(function() {
		var target = $(this).attr("href");
		if(target){
		target = target.replace(/indexFlight\=[0-9]+/, "indexFlight=" + indexFlight);
		$(this).attr("href", target);
		$(this).attr("data-link", target);
		}
	})
	
	invokeFlightDetailsPartial(indexRoute, indexFlight, indexBrand, isBusiness, function(data) {
		$("#details-" + indexRoute + "-" + indexFlight).html(data);
		var totalDuration = $($("#details-" + indexRoute + "-" + indexFlight).siblings(".booking__fightPreviewWrapper").find(".flightTime")[0]).text();
		$("#details-" + indexRoute + "-" + indexFlight).find(".time").html(totalDuration);
		
		done(selector);
		
		//book.init();
		bindDetailsEvents(selector);
	}, function() {
		done(selector);
	});
}

/** Funzione richiamata ogni volta che viene effettuato un click 
 * su un radio button di un volo
 * */
function onFlightPriceSelection(){
	var isCarnet=$(this).attr("data-is-carnet");
	var indexRoute = $(this).attr("data-details-route");
	var indexFlight = $(this).attr("data-details-flight");
	var indexBrand = $(this).attr("data-index-brand");
	/*time, brand, cost and flight number*/
	if(isCarnet!="1"){
	changeFanPlayrFlightClickData(this);
}
}

function bindPriceRecap() {
	$(".j-priceSelector").on("click", function() {
		var type = $(this).closest(".j-bookingTableDeparture").length ? "Dep" : "Ret";
		var item;
		if ((item = $(this).find(".price")).length > 0) {
	        $(".j-bookingRecap" + type + " .price").html(item.text());
	        $(".j-bookingRecap" + type + " .booking__fightPreview__price").html(item.text());
		}
	});
}


/**
 * Funzione per il recupero dati del dettaglio del volo (al click sul link per il tooltip)
 */
function be_getFlightDetails(selector, done) {
	be_priceSelector(selector, done);
}

/**
 * Funzione eseguita al click sul bottone scegli e continua.
 * Conferma la selezione del volo refreshando la matrice corretta.
 */
function confirmSelection(selector, done, type) {
	
	var indexRoute = selector.attr("data-details-route");
	var indexFlight = selector.attr("data-details-flight");
	var indexBrand = selector.attr("data-index-brand");
	var solutionID = selector.attr("data-solutionID");
	$(".j-priceSelector").each(function(i,elem){
		if ($(elem).attr("data-details-route") === indexRoute 
				&& $(elem).attr("data-details-flight") === indexFlight) { 
			if ($(elem).find($(".isActive .fakeRadio")).length > 0 ){
				indexBrand = $(elem).attr("data-index-brand");
				solutionID = $(elem).attr("data-solutionID");
			}
		}
	});
	//if (indexBrand && solutionID) {
		invokeConfirmSelection(indexRoute, indexFlight, indexBrand, solutionID, function(data){
			if (data.redirect) {
				window.location.replace(data.redirect);
			} else {
				
				var indexToRefresh = data.indexToRefresh;
				//refresh ecoupon e al suo interno anche il carrello
				refreshECouponPartial(false);
				
				//refreshCashAndMiles
				if (typeof cashAndMilesAjaxRefresh != 'undefined') {
					cashAndMilesAjaxRefresh();
				}
				initAccordions();
				
				if (indexToRefresh == 0 || indexToRefresh == 1) {
					//refresh matrice a indice indexToRefresh
					refreshTablePartial(indexToRefresh);
					var refreshRibbon = data.refreshRibbon;
					if (refreshRibbon == 1) {
						if (indexToRefresh == 0){
							refreshRibbonPartial(0);
						} else {
							refreshRibbonPartial(1);
						}
					}
				} else {
					// indexToRefresh == -1
					$(".selectSubmit").removeClass("isDisabled");
					$(".bookInfoBoxBasketBtn").removeClass("isDisabled");
					$(".bookInfoBoxBasketBtn").attr("href", $(".selectSubmit").attr("href"));
				}
				addFlightInfoRecap(selector, done, type);
				var currentLink = $(".selectSubmit").attr("href");
				if(currentLink.indexOf('?') == -1){
					$(".selectSubmit").attr("href", currentLink + "?solutionIdFirstSlice=" + solutionID);
				}
			    
			    /*FareBasis, fare, taxes, etc..*/
			    changeFanPlayrFlightSelectionData(data, indexRoute);
			    
			   
			}
			
		}, function() {
			addFlightInfoRecap(selector, done, type);
		});
	//}
}

function addFlightInfoRecap(selector, done, type) {
	var divLeft = selector.closest(".bookingTable__bodyRow").find(".bookingTable__left.booking__fightPreviewWrapper");
	var divLeftBody = selector.closest(".bookingTable__bodyRow").find(".bookingTable__rowBody.j-bookTableRowBody .bookingTable__left");
	var target = $(".j-bookingRecap" + type);
	var item;
	
	var date = $(".j-bookingRecap" + type + " .bookingRecap__previewText").text();
	target.find(".bookingRecap__previewText").html(date ? date.substring(0, date.lastIndexOf("-") + 1) + " " : "");
	
	if ((item = divLeftBody.find(".bookingFlightDetails__listScale").first().find(".third .bookingFlightDetails__airport")).length > 0) {
		target.find(".first .booking__fightPreviewDepArrCont__iata").html(item.text());
	}
	if ((item = divLeftBody.find(".bookingFlightDetails__listScale").first().find(".third .bookingFlightDetails__airportName")).length > 0) {
		target.find(".first .booking__fightPreviewDepArrCont__airport").html(item.text());
	}
	if ((item = divLeftBody.find(".bookingFlightDetails__listScale").last().find(".fifth .bookingFlightDetails__airport")).length > 0) {
		target.find(".third .booking__fightPreviewDepArrCont__iata").html(item.text());
	}
	if ((item = divLeftBody.find(".bookingFlightDetails__listScale").last().find(".fifth .bookingFlightDetails__airportName")).length > 0) {
		target.find(".third .booking__fightPreviewDepArrCont__airport").html(item.text());
	}
	
	if ((item = divLeft.find(".first .booking__fightPreviewDepArrCont__time")).length > 0) {
		target.find(".first .booking__fightPreviewDepArrCont__time").html(item.text());
		target.find(".bookingRecap__previewText").append(item.text());
	}
	if ((item = divLeft.find(".third .booking__fightPreviewDepArrCont__time")).length > 0) {
		target.find(".third .booking__fightPreviewDepArrCont__time").html(item.text());
		target.find(".bookingRecap__previewText").append(" > " + item.text());
	}
	if ((item = divLeft.find(".booking__fightPreviewInfoDesktop .transferNum")).length > 0) {
		target.find(".bookingRecap__scaleRecapLiner").html(item.text().toLowerCase() + " ");
		
	}
	if ((item = divLeft.find(".booking__fightPreviewInfoDesktop .flightTime")).length > 0) {
		target.find(".bookingRecap__scaleRecapLiner").append($('<span class="bookingRecap__scaleRecap"></span>').html(" - " + item.text()));
	}
	if ((item = divLeft.find(".booking__fightPreviewInfoDesktop .flightComp")).length > 0) {
		target.find(".bookingRecap__previewText").append(" | ");
		target.find(".bookingRecap__previewText").append($('<span></span>').html(item.text()));
	}
	
	if ((item = divLeft.find(".booking__fightPreviewInfoDesktop .voloSoggAppr")).length > 0) {
		target.find(".voloSoggAppr").html(item.text());
		
	}
	
	done(selector);
}

function be_goToReturn(selector, done) {
	confirmSelection(selector, done, "Dep");
}

function be_selectedReturn(selector, done) {
	confirmSelection(selector, done, "Ret");
}

/**
 * Funzione di callback invocata dal componente ecoupon.
 */
function couponProcessedCallback(data) {
	refreshECouponPartial(true);
	fanPlayrEcouponApplied(data);
	
};

function be_loadMoreFlights(selector, done){
	var indexRoute = selector.parent().find(".j-loadMoreBookingRow").attr("data-details-route");
	refreshTablePartial(indexRoute, true, done);
}

function filterAndOrderFlights(){
	var indexRoute = $(this).attr("data-details-route");
	refreshTablePartial(indexRoute, false);
}
function insertAriaLabelonArrow(){
		if($(this).attr('aria-describedby')){
			var indexRoute = $(this).attr("data-details-route");
			var id1='wcag-arrow1-'+indexRoute;
			var id2='wcag-arrow2-'+indexRoute;
				if($(this).attr('aria-describedby')==id1){
					$(this).attr('aria-describedby',id2);
					$(this).attr('aria-label',$('#'+id2).text());
				}else{
					
					$(this).attr('aria-describedby',id1);
					$(this).attr('aria-label',$('#'+id1).text());

				}
			
		}
		
	}

function businessTrigger(event){
	var currentTable = $(this).parents(".bookingTable");
	var currentIndexRoute = currentTable.attr("data-index-route");
	$(".bookingTable").not("[data-index-route='"+ currentIndexRoute + "']").each(
		function(index, elem){
			book.businessTrigger($(elem).find(".j-businessTrigger a").first());			
		});
}

function changeFanPlayrFlightClickData(flight){
	indexRoute = $(flight).attr("data-details-route");
	var prefix = indexRoute == 0 ? "dep" : "ret";
	var time = $(flight).attr("data-flight-time").split(":");
	/*AM PM nell'ora invece che nei minuti*/
	if(time[1].match(/PM/) != null){
		time[1] = time[1].slice(0,-3);
		time[0] = time[0] + " PM";
	}
	else if(time[1].match(/AM/) != null){
		time[1] = time[1].slice(0,-3);
		time[0] = time[0] + " AM";
	}
	var brand = $(flight).attr("data-flight-brandName");
	var price = $(flight).attr("data-flight-price");
	var flightNumber = $(flight).attr("data-flight-number");
	var obj = {};
	obj[prefix + "Hours" ]= time[0];
	obj[prefix + "Minutes"]= time[1];
	obj[prefix + "Brand"]= brand;
	obj[prefix + "FlightNumber" ]= flightNumber;
	obj[prefix + "cost"]=  price;
	pushAnalyticsData(obj);
	pushFanplayrEvent();
}

function changeFanPlayrFlightSelectionData(data, indexRoute){
	var obj = data["analytics_flightInfo"];
	if(typeof obj !== "undefined"){
		pushAnalyticsData(obj);		
		pushFanplayrEvent();
		/*all flights are selected*/
		if(typeof obj["depFareBasis"] !== "undefined" 
			&& obj["depFareBasis"].length > 0){
			activateDTMRule("flightSerpReturnFlight");
	}
			}
}

function changeFanPlayrRibbonClickData(data){
	var obj = data["analytics_flightInfo"];
	if(typeof obj !== "undefined"){
		pushAnalyticsData(obj);		
		pushFanplayrEvent();
	}
}

function fanPlayrEcouponApplied(data){
	var obj = {"eCoupon": "YES"};
	if(data["fanplayr"] && data["fanplayr"] == true){
		obj["eCoupon"] = "FANPLAYR";
	}
	pushAnalyticsData(obj);	
	pushAnalyticsEcouponEvent();
}


