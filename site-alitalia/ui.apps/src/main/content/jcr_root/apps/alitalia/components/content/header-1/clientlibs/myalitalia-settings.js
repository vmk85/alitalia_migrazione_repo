var userMA = {};
var userMM = {};
var am;
var globalInit = [];
var globalDestroy = [];
var labelAvailableFlightsMA = "";
var labelAvailableMyFlightsMA = "";
// var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var referrer = document.referrer;

var flagAccettazioneCRM = [];
var flagAccettazioneCRMnewsletter = "";
var flagAccettazioneCRMprofiling = "";
var flagAccettazioneCRMprofilingOld = "";
var CanaleComunicazioni = [];
var canaleComunicazionePreferito = "";
/** */
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

var regTokenConfPsw = "";

if(dd<10) {
    dd = '0'+dd
}

if(mm<10) {
    mm = '0'+mm
}

today = yyyy + '-' + mm + '-' + dd ;


var SARegistrationflag = false;
function resetPassword() {
    $("#gigya-reset-error-message").text("");

    if (validatePsw($("#gigya-password-newPassword").val().trim())) {

        if ($("#gigya-password-newPassword").val().trim() == $("#gigya-passsord-passwordRetype").val()){
            enableLoaderMyAlitalia();

            var regToken = regTokenConfPsw;
            var data = {
                type: "reset",
                psw: $("#gigya-password-newPassword").val(),
                pswConf: $("#gigya-passsord-passwordRetype").val(),
                regToken: regToken
            };
            performSubmit("MyAlitalia-resetPsw", data, resetPasswordSuccess, resetPasswordError);
        } else {
            $("#gigya-reset-error-message").text(CQ.I18n.get("myalitalia.invalid.psw.mismatch"));
        }
    } else {
        $("#gigya-reset-error-message").text(CQ.I18n.get("myalitalia.invalid.psw"));
    }
}

function resetPasswordSuccess(response){

    if (!response.isError){
        $("#ma-reset-password-form").find(".close-button").trigger("click");
        $("a[data-open='reset-complete']").click();
        disableLoaderMyAlitalia();
    } else {
        $("#gigya-reset-error-message").text(CQ.I18n.get(response.errorMessage));
        disableLoaderMyAlitalia();
    }
}

function resetPasswordError(){
    disableLoaderMyAlitalia();
}

function validatePsw(psw) {

    var pointValid = 0;

    var M = /[A-Z]/;
    var m = /[a-z]/;
    var number = /[0-9]/;
    var special = /\W/;
    var space = /^\S+$/;

    if (psw.length >= 8 && space.test(psw)) {

        if(M.test(psw)){
            pointValid += 1;
        }
        if(m.test(psw)){
            pointValid += 1;
        }
        if(number.test(psw)){
            pointValid += 1;
        }
        if(special.test(psw)){
            pointValid += 1;
        }


    }

    return (pointValid >= 2 ? true:false);

}

$(window).bind("load", function() {
        var type = linkFromEmail();
        if(type == "reset" && !location.href.includes("resetpassword")){
                $("#openResetPasswordForm").trigger("click");
            resetModalOpened = true;
            // manageFormReset();
            setTimeout(function() {
                $("#open-ma-reset-password-form").click();

            }, 2000);

            // if(typeof(gigya) != 'undefined') {
	        //     gigya.accounts.showScreenSet({
	        //         screenSet: "MA-RegistrationLogin",
	        //         startScreen: "gigya-reset-password-screen",
	        //         containerID: "container",
	        //         onAfterSubmit: function(resp) {
	        //             if(resp.response.errorCode == 0){
	        //                 $("#ma-reset-password-form").find(".close-button").trigger("click");
	        //                 $("a[data-open='success-reset']").click();
	        //             }
	        //         },
	        //         onAfterScreenLoad: manageFormReset
	        //     });
            // }
        }
});

$(document).on("click", "[data-auth-sc]", function (event) {
    var current = $(this).attr("data-auth-sc");
    // if(current>3)$("[data-auth]:eq("+(current-1)+")").trigger("click");

    if (!($("#" + retriveIdByDataAuthSC(current)).is(":disabled"))) {
        if (current == 1) {
            flagAccettazioneCRMnewsletter = {
                tipoConsenso:"COM",
                flagConsenso:true,
                dataUltimoAggiornamento: today
            };

            flagAccettazioneCRMprofilingOld = {
                tipoConsenso:"PRO",
                flagConsenso:true,
                dataUltimoAggiornamento: today
            };
            canaleComunicazionePreferito = {
                tipo : "mail" ,
                selezionato : true ,
                dataUltimoAggiornamento : today
            };
        } else if (current == 2) {
            flagAccettazioneCRMnewsletter = {
                tipoConsenso:"COM",
                flagConsenso:false,
                dataUltimoAggiornamento: today
            };
            flagAccettazioneCRMprofilingOld = {
                tipoConsenso:"PRO",
                flagConsenso:false,
                dataUltimoAggiornamento: today
            };
            canaleComunicazionePreferito = {
                tipo : "mail" ,
                selezionato : false ,
                dataUltimoAggiornamento : today
            };
        } else if (current == 3) {
            flagAccettazioneCRMprofiling = {
                tipoConsenso:"PRN",
                flagConsenso:true,
                dataUltimoAggiornamento: today
            }
        } else if (current == 4) {
            flagAccettazioneCRMprofiling = {
                tipoConsenso:"PRN",
                flagConsenso:false,
                dataUltimoAggiornamento: today
            }
        }
    }
});


function socialLoginRegistration(){

    enableLoaderMyAlitalia();

    var regToken = $("#socialLoginRegistrationSubmit").attr("data-token");

    $("#ma-pending-registration .close-button").trigger("click");

    CanaleComunicazioni.push(canaleComunicazionePreferito);
    flagAccettazioneCRM.push(flagAccettazioneCRMnewsletter);
    flagAccettazioneCRM.push(flagAccettazioneCRMprofiling);
    flagAccettazioneCRM.push(flagAccettazioneCRMprofilingOld);
    var crmRegistration =
        {
            regToken : regToken,
            crmData : JSON.stringify({
                listaConsensi : flagAccettazioneCRM,
                canaleComunicazionePreferito : CanaleComunicazioni
            }),
        };

    /** Invio i dati alla servlet per censire il nuovo utente */
    performSubmit("RegistrerMyAlitaliaCrmUser", crmRegistration, Login.RegistrationSocial.Success, Login.RegistrationSocial.error);

}

function retriveIdByDataAuthSC(dataAuth) {
    var id = "";
    switch (parseInt(dataAuth)) {
        case 1:
            id = "consenso_news-yes";
            break;
        case 2:
            id = "consenso_news-no";
            break;
        case 3:
            id = "consenso_dati-yes";
            break;
        case 4:
            id = "consenso_dati-no";
            break;

    }
    return id;
}

function disableCheck(id) {
    document.getElementById(id).disabled = true;
    $("label[for="+id+"]").css("color","#c1c1c1");
    if ($("#"+id).is(":checked")){
        $("#"+id).removeAttr('checked').closest(".radio-wrap").removeClass("checked");
    }
    var inputs_disabled = $("input#"+id).parent().parent().parent().find("input:not(:disabled)");
    inputs_disabled.each(function(i, obj) {
        $(obj).attr('checked', true);
        $(obj).parent().parent(".radio-wrap").addClass("checked");
    });
}

function enableCheck(id) {
    document.getElementById(id).disabled = false;
    $("label[for="+id+"]").css("color","black");

}

function getParameterVal() {
    var url = window.location.href;
    var params = url.split("&");
    var newObj = {};
    newObj.originalUrl = url;
    if (url.indexOf("regToken") > -1) {
        newObj.regToken = getSingleParam(params, "regToken");
    }
    return newObj;
}

function getSingleParam(params, paramName) {
    for (var i = 0; i < params.length; i++) {
        if (params[i].split("=")[0] == paramName) {
            return params[i].split("=")[1].split("+-+")[0];
        }
    }
    return false;
}

function reloadAll(){
    $("#socialLinkAccountLogin").show();
    $("[data-provider='googleplus']").show();
    $("[data-provider='facebook']").show();
    $("[data-provider='linkedin']").show();
    $("#pswLoginLinkAccount").show();
    $("#btnLoginLinkAccount").show();
    $("#webLinkAccountLogin").show();
    $("#fullLinkAccountLogin").show();
}

$(document).ready(function() {

    var obj = getParameterVal();
    if (obj){
        if (location.href.indexOf("regToken=")!= -1){
            var data = {
                authToken:obj.regToken
            }
            setGigyaCookie("firstLogin","true",1);
            enableLoginHeaderLoader();
            performSubmit("MyAlitaliaServlet-UpdateSession",data,Login.Session.refreshSuccess,Login.Session.refreshError);
        } else {
            if (document.cookie.indexOf(maAk) != -1 && !$(".login-included-if-anonymous").hasClass("hide")){
                enableLoginHeaderLoader();
                performSubmit("MyAlitaliaServlet-UpdateSession","",Login.Session.refreshSuccess,Login.Session.refreshError);
            } else {
                var data = {
                    refresh: false
                }
                performSubmit("MyAlitaliaServlet-UpdateSession",data,Login.Session.refreshSuccess,Login.Session.refreshError);
            }
        }
    }
    labelAvailableFlightsMA = $(".hasAvailableFlightsMA").html();
    labelAvailableMyFlightsMA = $(".hasAvailableMyFlightsMA").html();

    $("#redirectProfile").click(function () {
        if ($(this).attr("url") != undefined) {

            enableLoaderMyAlitalia();
            var url = "";
            if (window.location.href.indexOf("/myalitalia/") != -1) {
                url = $(this).attr("url");
            } else {
                url = window.location.origin + "/" + languageCode + "_" + marketCode + "/myalitalia/" + $(this).attr("url");
            }

            window.location.href = url;

        }
    });

    if ($(window).width() <= 800 ){
        $("#ma-o-mm-login").parent(".reveal-overlay").css("overflow-y","hidden");
    }

        $(".reveal-overlay:has(> #association-success)").on("click", function(e){
            enableLoaderMyAlitalia();
            redirectWithParameters("myalitalia/myalitalia-bacheca", "page=dashboard&isAssociated=true");
         });


    $(".scroolDown .no-repeate").click(function(){
        $(this).next(".submenu").slideToggle();
    });

    $(document).on("click", ".lista-offerte__slider a", function(){
        enableLoaderMyAlitalia();
    });
    var type = linkFromEmail();
    // if(typeof(gigya) != 'undefined') {
    // 	gigya.accounts.addEventHandlers({
    //         onLogin : loginEventHandler,
    //         onLogout : logoutEventHandler
    //     });
    // }
    //
    try {
        // getUserMAInfo(function(data) {
        //     if(data && data.profile && data.profile.email){
        //         checkFlights(data);
        //         if (data.profile.photoURL)
        //             $("#imgUtente").attr("src",data.profile.photoURL)
        //         performSubmit("GetMyAlitaliaCRMDataToSession","",Login.Session.success.getCrmDataToSessionForFill,Login.Session.error.getCrmData);
        //         $(".show-if-user-logged").removeClass("hide");
        //     }
        //     (
        //         window.location.href.indexOf("booking") != -1
        //         &&
        //         data.status != "FAIL"
        //         ?
        //             performSubmit("GetMyAlitaliaCRMDataToSession","",Login.Session.success.getCrmDataToSessionForFill,Login.Session.error.getCrmData)
        //         :
        //             ""
        //     );
        // });
        // checkFlights();
        setTimeout(function() {
            var modal = getCookie("openModal");
            if(modal != null && modal != "") {
                $("[data-open='" + modal + "']").trigger("click");
                setCookie("openModal", "", -1); // Cancello il cookie. Usato questo metodo perché la funzione "deleteCookie" attualmente non funziona
            }
         }, 5000);

    } catch (err) {
    }

});

function enableLoaderMyAlitalia(){
    $("body").append("\t<div class=\"circle-loader loader-my-alitalia small-12\" style=\"z-index: 10050;position:fixed;\">\n" +
        "\t\t<div class=\"fade-circle\" id='loaderMyAlitalia'></div>\n" +
        "\t</div>");
    $('.circle-loader.loader-my-alitalia').show();
}

function disableLoaderMyAlitalia() {
    $('.circle-loader.loader-my-alitalia').remove();
    // $("#loaderMyAlitalia").remove();

}

function enableLoginHeaderLoader(){
    $(".user-area").css("position","relative");
    $(".login-included-if-anonymous").prepend("\t<div class=\"circle-loader loader-my-alitalia small-12\" style=\"z-index: 10050;\">\n" +
        "\t\t<div class=\"fade-circle\" id='loaderMyAlitalia'></div>\n" +
        "\t</div>");
    $('.circle-loader.loader-my-alitalia').show();
}

/*Funzione in sostituzione dell'uso dell'oggetto URL che non � compatibile con IE11*/
function getQueryString() {
	var key = false, res = {}, itm = null;
	// get the query string without the ?
	var qs = location.search.substring(1);
	// check for the key as an argument
	if (arguments.length > 0 && arguments[0].length > 1)
		key = arguments[0];
	// make a regex pattern to grab key/value
	var pattern = /([^&=]+)=([^&]*)/g;
	// loop the items in the query string, either
	// find a match to the argument, or build an object
	// with key/value pairs
	while (itm = pattern.exec(qs)) {
		if (key !== false && decodeURIComponent(itm[1]) === key)
			return decodeURIComponent(itm[2]);
		else if (key === false)
			res[decodeURIComponent(itm[1])] = decodeURIComponent(itm[2]);
	}

  return key === false ? res : null;
}

function linkFromEmail() {
   // var url = new URL(location.href);
    //var param = url.searchParams.get("errorCode");
    	var param = getQueryString('errorCode');
    if(param && param == "206005") {
        return "login";
    } else {
        //param = url.searchParams.get("pwrt");
        param = getQueryString("pwrt");
        regTokenConfPsw = param;
        if(param && param.length > 0) return "reset";
    }
    return false;
}

function saveUserMA(data, key, value) {
    if(data.status !== "FAIL") {
        $("[name='userMAAsJsonString']").val(JSON.stringify(data));
        performSubmit("savemauser", "#form-user-ma", function (resp) {
            saveComplete(resp, key, value);
        }, function (resp) {
            console.log(resp)
        });
    } else if(!key.includes("myalitalia") || key.includes("resetpassword"))  redirectWithParameters(key, value);
    else location.href = internalUrl1 + ".html";
}

function saveComplete(resp, key, value) {
    console.log(resp);
    redirectWithParameters(key, value);
}

function redirectCompleteProfile(){
    location.href = $(this).attr("url");
}

function saveUserMAToModel(key, value) {
    getUserMAInfo(saveUserMA, key, value);
}


function getLoginPinMMError(data){
    console.log("error login millemiglia "+data);
    disableLoaderMyAlitalia()
}

function loginEventHandler(ev) {
    Login.Session.setSession(ev,"MyAlitaliaServlet-loginMa");
        // else redirect("refresh");
}

function logoutEventHandler() {
    userMA = {};
    enableLoaderMyAlitalia();
    $(".login-included-if-authenticated").addClass("hide");
    $(".login-included-if-anonymous").removeClass("hide");
    if(globalDestroy.length > 0)
        for(index in globalDestroy)
            globalDestroy[index]();
    $(".show-if-user-logged").addClass("hide");
    closeModal = true;
    $("#needUserSettings .close-button").click();
    Logout.Session.setSession("MyAlitaliaServlet-logoutMa");
}

function fillPostLogin(data){
    if (data.nome && data.cognome){
        var nome = data.nome.substring(0,1).toUpperCase() + data.nome.substring(1,data.nome.length).toLowerCase();
        var cognome = data.cognome.substring(0,1).toUpperCase() + data.cognome.substring(1,data.cognome.length).toLowerCase();
        $("#binding-name-profile").text(nome + " " + cognome);
        $("#credential").text(nome + " " + cognome);
    } else {
        var email = data.email.substring(0,1).toUpperCase() + data.email.substring(1,data.email.length).toLowerCase();
        $(".binding-name.label-name").text(email);
        $("#credential").text(email);

    }
    $(".progress-bar").addClass("myalitalia");
    $("#percent").text($("#percent").attr("label") + " " + data.percent + "%");
    $("#percentualeDesk").text($("#percentualeDesk").attr("label") + " " + data.percent + "%");
    fillPercent(data.percent);
    $("#redirectProfile").attr("url", data.redirectUrl).text(CQ.I18n.get(data.text));
    $("#urlPercent").attr("href", data.redirectUrl).text(CQ.I18n.get(data.text));
    // if(typeof(gigya) != 'undefined') {
	//     gigya.accounts.addEventHandlers({
	//         onLogout : logoutEventHandler
	//     });
    // }
    // disableLoaderMyAlitalia();
    // getUserMAInfo(function (response) {
    //     if (data.eventName != "login"){
    //         Login.Session.setSession(response,"MyAlitaliaServlet-UpdateSession");
    //     }
    // })
}

function emptyFill() {

    $("#percent1").removeClass("fill");
    $("#percent2").removeClass("fill");
    $("#percent3").removeClass("fill");
    $("#percent4").removeClass("fill");
    $("#percent5").removeClass("fill");
    $("#percent1desk").removeClass("fill");
    $("#percent2desk").removeClass("fill");
    $("#percent3desk").removeClass("fill");
    $("#percent4desk").removeClass("fill");
    $("#percent5desk").removeClass("fill");

}

function fillPercent(data){

    emptyFill();

    data = parseInt(data);

    if (data >= 20 && (!$("#percent1").hasClass("fill"))){
        $("#percent1").addClass("fill");
    }
    if (data >= 40 && (!$("#percent2").hasClass("fill"))){
        $("#percent2").addClass("fill");
    }
    if (data >= 60 && (!$("#percent3").hasClass("fill"))){
        $("#percent3").addClass("fill");
    }
    if (data >= 80 && (!$("#percent4").hasClass("fill"))){
        $("#percent4").addClass("fill");
    }
    if (data >= 100 && (!$("#percent5").hasClass("fill"))){
        $("#percent5").addClass("fill");
    }


    if (data >= 20 && (!$("#percent1desk").hasClass("fill"))){
        $("#percent1desk").addClass("fill");
    }
    if (data >= 40 && (!$("#percent2desk").hasClass("fill"))){
        $("#percent2desk").addClass("fill");
    }
    if (data >= 60 && (!$("#percent3desk").hasClass("fill"))){
        $("#percent3desk").addClass("fill");
    }
    if (data >= 80 && (!$("#percent4desk").hasClass("fill"))){
        $("#percent4desk").addClass("fill");
    }
    if (data >= 100 && (!$("#percent5desk").hasClass("fill"))){
        $("#percent5desk").addClass("fill");
    }
}


function percentual(data) {

    var percentual = 0;

    percentual += percentualPersonalData(data);
    percentual += percentualDatiDiViaggio(data);
    percentual += percentualPreferenzeViaggio(data);
    // percentual += percentualFatturazione(data);

    setTextUrlForCompletePercent(data,percentual);

    if (percentual == 65){
        percentual = 100;
    }
    return percentual;

}

function setTextUrlForCompletePercent(data,percentual) {

    var text = "";
    var url = "";

    if (percentualPersonalData(data) < 20){
        text =  CQ.I18n.get("myalitalia.info.completaDatiPersonali");
        url = "./myalitalia-dati-personali.html?page=personalData";
    }
    if (percentualDatiDiViaggio(data) < 20){
        text =  CQ.I18n.get("myalitalia.info.completaDatiViaggio");
        url = "./myalitalia-dati-di-viaggio.html?page=dataTravel";
    }
    if (percentualPreferenzeViaggio(data) < 20){
        text =  CQ.I18n.get("myalitalia.info.completaPreferenza");
        url = "./myalitalia-millemiglia-preferenze-viaggio.html?page=preferences";
    }

    if (text != "" && url != ""){
        $("#redirectProfile").attr("url", url).text(text);
        $("#urlPercent").attr("href", url).text(text);
    }
}

function percentualPersonalData(data) {

    var percentual = 0;

    if (validatorPercentData(data.infoCliente.nome) && validatorPercentData(data.infoCliente.cognome))
        percentual += 4;

    if (validatorPercentData(data.infoCliente.sesso) && validatorPercentData(data.infoCliente.dataNascita))
        percentual += 4;

    if (validatorPercentData(data.infoCliente.indirizzo) && validatorPercentData(data.infoCliente.cap))
        percentual += 4;

    if (validatorPercentData(data.infoCliente.citta) && validatorPercentData(data.infoCliente.nazione))
        percentual += 4;

    if ((validatorPercentData(data.recapitiTelefonici.numero) || validatorPercentData(data.recapitiTelefonici.cellulare)) && validatorPercentData(data.recapitiTelefonici.tipo) && validatorPercentData(data.recapitiTelefonici.prefissioNazionale))
        percentual += 4;

        return percentual;
}

function percentualDatiDiViaggio(data) {

    var percentual = 0;

    if(data.elencoDocumenti.length > 0){

        for (var i = 0; i < data.elencoDocumenti.length; i++){
                percentual++;
        }

    }

    if(percentual == 1){
        return 10;
    }else if(percentual >= 2){
        return 20;
    }else{
        return 0;
    }

}

function validatorPercentData(data){
    if (data != undefined && data != "") {
        return true;
    } else {
        return false;
    }
}

function percentualPreferenzeViaggio(data) {

    var percentual = 0;

    if(data.preferenzeViaggio.posto != 0) {
        percentual+=5;
    }
    if(data.preferenzeViaggio.pasto != 0) {
        percentual+=5;
    }
    // if(validatorPercentData(data.data.flightPref_FF) && (data.data.flightPref_FF.length > 0)) {
    //     percentual+=5;
    // }
    if(data.preferenzeViaggio.aeroportoPreferito != null && data.preferenzeViaggio.aeroportoPreferito != "") {
        percentual+=10;
    }
    if(data.preferenzeViaggio.viaggiInFamiglia != null) {
        percentual+=5;
    }

    return percentual;

}

// function getUserMAInfo(callback, key, value) {
// 	if(typeof(gigya) != 'undefined') {
// 		gigya.accounts.getAccountInfo({
// 	        include: "profile,data,identities-all",
// 	        extraProfileFields: "professionalHeadline",
// 	        callback: function(resp) {
// 	            if (resp.status != 'FAIL') {
// 	                userMA["isLogged"] = true;
// 	                userMA = resp;
// 	            } else userMA["isLogged"] = false;
// 	            callback(resp, key, value);
// 	        }
// 	    });
// 	}
// }

function setUserMMInfo(data) {
    if(data.personal.firstname != "") data["isLogged"] = true;
    else data["isLogged"] = false;
    userMM = data;
}

function setUserMAInfo(data) {
    userMA = data;
}


// function doMALogin(user, pass, success, fail) {
//     gigya.accounts.login({
//         loginID: user,
//         password: pass,
//         callback: function(resp) {
//             console.log(resp);
//             if(resp.errorCode == 0) {
//                 checkFlights(resp);
//                 if(success)
//                     success(resp);
//             } else if(fail) fail(resp);
//         }
//     });
//     //alert(user + " = " + pass);
// }
function saveUserMASuccess(resp) {
    console.log(resp);
}

function saveUserMAFail(resp) {
    console.log(resp);
}

// function checkFlights(user){
//     var flights = user.data.flights;
//     var now = new Date();
//     var deferreds = [];
//
//     $.each(flights, function ( index, value ){
//         var departureDate = new Date(value.flight_departureDate);
//         if(departureDate>now){
//             var data = {
//                 pnr : value.flight_pnr,
//                 firstName : value.flight_firstName,
//                 lastName : value.flight_lastName,
//                 format : "json"
//             };
//
//             deferreds.push(req);
//         }
//     });

//    $.when.apply($, deferreds).then(function(){
//        availableFlightsForMA(true);
//        availableMyFlightsForMA(true);
//    }).fail(function(err){
//        console.log(err)
//    });
// }

function updateCounters(resp){
    if(!resp.isError){
        var obj = JSON.parse(resp.data);

        if(obj.pnrData.pnr[0].roundTrip){
            if (obj.pnrData.pnr[0].flights[0].isWebCheckinPermitted){
                if(obj.pnrData.pnr[0].flights[0].segments[0].openCI){
                    totalFlightsAvailableForCheckin++;
                }else{
                    totalFlightsForManage++;
                }
            }else{
                totalFlightsForManage++;
            }
        }else{
            for(var i=0; i<obj.pnrData.pnr[0].flights.length;i++){
                if (obj.pnrData.pnr[0].flights[i].isWebCheckinPermitted){
                    if(obj.pnrData.pnr[0].flights[i].segments[0].openCI){
                        totalFlightsAvailableForCheckin++;
                    }else{
                        totalFlightsForManage++;
                    }
                }else{
                    totalFlightsForManage++;
                }
            }
        }

    }

    availableFlightsForMA(true);
    availableMyFlightsForMA(true);
}

function updateCountersFail(resp){
    console.log(resp);
}


function setHeaderMA(data){

    if (data.nome && data.cognome){
        customerName =  data.nome.substring(0,1) + data.nome.substring(1,data.nome.length).toLowerCase() + " " + data.cognome.substring(0,1) + data.cognome.substring(1,data.cognome.length).toLowerCase();
    } else {
        customerName = data.email.substring(0,1).toUpperCase() + data.email.substring(1,data.email.length).toLowerCase();

    }
    $(".user__logout").attr("href", "javascript:logoutEventHandler();");
    $('.binding-name').text(customerName);

    $('.menu-mm-item').hide();
    $(".menu-ma-item").show();
    $('.menu-heading-mm').hide();
    $('.menu-heading-ma').show();

    $('.login-included-if-authenticated').removeClass("hide").show();
    $('.login-included-if-anonymous').addClass("hide");

    $('#header').removeClass();
    $('#header').addClass('myalitalia');

    // ProfileMillemiglia(false);
    //
    // loadMenuPersonalArea();
}

function availableFlightsForMA(show,totalFlightsAvailableForCheckin){
    if(show){
        $('.hasSingleAvailableFlightMA').addClass('hide');
        $('.hasAvailableFlightsMA').addClass('hide');
        $('.noAvailableFlightsMA').addClass('hide');
        if(totalFlightsAvailableForCheckin>0){
            $('.myalitalia_profile').removeClass('no-flight');
            $("#notification-count-MA").text(totalFlightsAvailableForCheckin);
            $("#notification-count-MA").removeClass('hide');
            $("#notification-count-MA").show();
            $(".linkFlights").attr("href", "javascript:;");
            $(".linkFlights").attr("onclick", "searchFlights()");
            $(".linkFlights").addClass("goToFlights");
            if(totalFlightsAvailableForCheckin==1){
                $('.hasSingleAvailableFlightMA').removeClass('hide');
            }else{
                if (labelAvailableFlightsMA != undefined){
                    var label = labelAvailableFlightsMA.replace("{0}",totalFlightsAvailableForCheckin);
                }
                $(".hasAvailableFlightsMA").html(label);
                $('.hasAvailableFlightsMA').removeClass('hide');
            }
        }else{
            //$('.myalitalia_profile').find('.custom-col').hide();
            $('.myalitalia_profile').addClass('no-flight');
            $('.noAvailableFlightsMA').removeClass('hide');
            $("#notification-count-MA").addClass('hide');
        }

        $(".myalitalia-profile-checkin").removeClass('hide');
        $(".myalitalia-profile-checkin").addClass('checked');
        $("#myalitalia_profile").attr('checked', true);

        $("#booking_code").prop("checked", false);
        $("#booking_code").parent().removeClass('checked');

        $('#millemiglia_code').prop("checked", false);
        $('#millemiglia_code').parent().removeClass('checked');

        $("#form-cercaPnr").addClass('hide');
        $("#form-cercaByFF").addClass('hide');
        $("#checkInSearch").addClass('hide');
    }else{
        $(".myalitalia-profile-checkin").addClass('hide');
        $(".myalitalia-profile-checkin").removeClass('checked');
        $("#myalitalia_profile").attr('checked', false);

        $('#millemiglia_code').prop("checked", false);
        $('#millemiglia_code').parent().removeClass('checked');

        $("#booking_code").prop("checked", true);
        $("#booking_code").parent().addClass('checked');
        $("#form-cercaPnr").removeClass('hide');
        $("#form-cercaByFF").addClass('hide');
        $("#checkInSearch").removeClass('hide');
    }
}


function availableMyFlightsForMA(show,totalFlightsForManage){
    $('.hasSingleAvailableMyFlightMA').addClass('hide');
    $('.hasAvailableMyFlightsMA').addClass('hide');
    $('.noAvailableMyFlightsMA').addClass('hide');

    if(show){
        if(totalFlightsForManage>0){
            $("#form-FlightSearchCode").css("margin-top","0");
            $('.myalitalia_profile.my_flight_search').removeClass('no-flight');
            $("#notification-count-myFlights-MA").text(totalFlightsForManage);
            $("#notification-count-myFlights-MA").removeClass('hide');
            $("#notification-count-myFlights-MA").show();
            $(".linkMyFlights").attr("href", "javascript:;");
            $(".linkMyFlights").attr("onclick", "searchFlights()");
            $(".linkMyFlights").addClass("goToFlights");
            if(totalFlightsForManage==1){
                $('.hasSingleAvailableMyFlightMA').removeClass('hide');
            }else{
                if (labelAvailableMyFlightsMA != undefined){
                    var label = labelAvailableMyFlightsMA.replace("{0}",totalFlightsForManage);
                }
                $(".hasAvailableMyFlightsMA").html(label);
                $('.hasAvailableMyFlightsMA').removeClass('hide');
            }
        }else{
            $("#form-FlightSearchCode").css("margin-top","12px");
            //$('.myalitalia_profile').find('.custom-col').hide();
            $('.myalitalia_profile.my_flight_search').addClass('no-flight');
            $('.noAvailableMyFlightsMA').removeClass('hide');
            $("#notification-count-myFlights-MA").addClass('hide');
        }

        $('.cerca-volo__content--miei-voli .flight_search_code').removeClass('hide');
        //$('.cerca-volo__content--miei-voli .my_flight_search').addClass('hide');
        $('.cerca-volo__content--miei-voli .myalitalia-profile-my-flights.my-flights-profile').removeClass('hide');
        $('input.radio-myalitalia_searchcode').attr('checked',false);
        $('input.radio-myalitalia_searchcode').parent().removeClass('checked');
        $('input.radio-myalitalia_profile_myflight').attr('checked',true);
        $('input.radio-myalitalia_profile_myflight').parent().addClass('checked');
        $('#form-myFlightSearch').addClass('hide');
        $('#form-FlightSearchCode').removeClass('hide');
    }else{
        $('.cerca-volo__content--miei-voli .flight_search_code').addClass('hide');
        $('.cerca-volo__content--miei-voli .my_flight_search').removeClass('hide');
        $('.cerca-volo__content--miei-voli .myalitalia-profile-my-flights.my-flights-profile').addClass('hide');
        $('input.radio-myalitalia_searchcode').attr('checked',true);
        $('input.radio-myalitalia_searchcode').parent().addClass('checked');
        $('input.radio-myalitalia_profile_myflight').attr('checked',false);
        $('input.radio-myalitalia_profile_myflight').parent().removeClass('checked');
        $('#form-myFlightSearch').removeClass('hide');
        $('#form-FlightSearchCode').addClass('hide');
    }
}


function ProfileMillemiglia(enabled){
    if(enabled){
        $(".millemiglia-profile-checkin").removeClass('hide');
        $(".millemiglia-profile-checkin").addClass('checked');
        $("#millemiglia_profile").attr('checked', true);

        $("#booking_code").prop("checked", false);
        $("#booking_code").parent().removeClass('checked');

        $('#millemiglia_code').prop("checked", false);
        $('#millemiglia_code').parent().removeClass('checked');

        $("#form-cercaPnr").addClass('hide');
        $("#form-cercaByFF").addClass('hide');
    }else{
        $(".millemiglia-profile-checkin").addClass('hide');
        $(".millemiglia-profile-checkin").removeClass('checked');
        $("#millemiglia_profile").attr('checked', false);

        $('#millemiglia_code').prop("checked", false);
        $('#millemiglia_code').parent().removeClass('checked');

        $("#booking_code").prop("checked", true);
        $("#booking_code").parent().addClass('checked');
        $("#form-cercaPnr").removeClass('hide');
        $("#form-cercaByFF").addClass('hide');
    }

}



// function checkProfileStateMA() {
//     getUserMAInfo(function(data) {
//         var profileState = { completed: 0, URL: '', text: '' };
//         var skip = false;
//
//         if(checkDatiPersonaliMA(data)>0) {
//             profileState.completed += checkDatiPersonaliMA(data);
//             if(checkDatiPersonaliMA(data)<20){
//                 profileState.URL = './myalitalia-dati-personali.html?page=personalData';
//                 profileState.text = CQ.I18n.get('myalitalia.info.completaDatiPersonali');
//                 skip = true;
//             }
//         }else{
//             profileState.URL = './myalitalia-dati-personali.html?page=personalData';
//             profileState.text = CQ.I18n.get('myalitalia.info.completaDatiPersonali');
//             skip = true;
//         }
//
//         if(checkDatiViaggioMA(data)>0) {
//             profileState.completed += checkDatiViaggioMA(data);
//             if(checkDatiViaggioMA(data)<20){
//                 if(!skip){
//                     profileState.URL = "./myalitalia-dati-di-viaggio.html?page=dataTravel";
//                     profileState.text = CQ.I18n.get('myalitalia.info.completaDatiViaggio');
//                     skip = true;
//                 }
//             }
//         }else{
//             if(!skip){
//                 profileState.URL = "./myalitalia-dati-di-viaggio.html?page=dataTravel";
//                 profileState.text = CQ.I18n.get('myalitalia.info.completaDatiViaggio');
//                 skip = true;
//             }
//         }
//
//         if(checkPreferenzeViaggio(data)>0) {
//             profileState.completed += checkPreferenzeViaggio(data);
//             if(checkPreferenzeViaggio(data)<30){
//                 if(!skip){
//                     profileState.URL = "./myalitalia-millemiglia-preferenze-viaggio.html?page=preferences";
//                     profileState.text = CQ.I18n.get('myalitalia.info.completaPreferenza');
//                     skip = true;
//                 }
//             }
//         }else{
//             if(!skip){
//                 profileState.URL = "./myalitalia-millemiglia-preferenze-viaggio.html?page=preferences";
//                 profileState.text = CQ.I18n.get('myalitalia.info.completaPreferenza');
//                 skip = true;
//             }
//         }
//
//         if(checkDatiFatturazione(data)>0) {
//             profileState.completed += checkDatiFatturazione(data);
//             if(checkDatiFatturazione(data)<30){
//                 if(!skip){
//                 profileState.URL = "#";
//                 profileState.text = '';
//                     //profileState.URL = "./myalitalia-dati-difatturazione.html";
//                     //profileState.text =  CQ.I18n.get('myalitalia.info.completaDatiPagamento');
//                 }
//             }
//         }else{
//             if(!skip){
//                 profileState.URL = "#";
//                 profileState.text = '';
//             }
//         }
//
//         //TOGLIERE IL CONTROLLO UNA VOLTA IMPLEMENTATA LA PARTE DEI DATI DI PAGAMENTO E FATTURAZIONE
//         if(profileState.completed == 70){
//             profileState.completed = 100;
//             profileState.URL = "#";
//             profileState.text = CQ.I18n.get('myalitalia.info.allCompleted');
//         }
//
//
//         if(profileState.completed >= 20) {
//             var max = profileState.completed / 20;
//             for(var i=0; i<max; i++) {
//                 $(".bar").find("span:eq(" + i + ")").removeClass("empty");
//                 $(".bar").find("span:eq(" + i + ")").addClass("fill");
//             }
//             for(var i=max; i<5; i++) {
//                 $(".bar").find("span:eq(" + i + ")").removeClass("fill");
//                 $(".bar").find("span:eq(" + i + ")").addClass("empty");
//             }
//         } else $(".bar").find("span").removeClass("fill").addClass("empty");
//
//         $(".progress-bar").find("p").html(CQ.I18n.get('myalitalia.info.profileComplete') + ' ' + profileState.completed + "%");
//         $(".cta-complete").attr("href", profileState.URL);
//         $(".cta-complete").html(profileState.text);
//     });
// }


function checkDatiPersonaliMA(user) {
    var data = user.data;
    var profile = user.profile;
    var total = 0;

    if(profile.firstName != null && profile.lastName != null){
        total += 4;
    }
    if( (profile.gender != null && profile.gender != 'u') && profile.birthDay != null && profile.birthMonth != null && profile.birthYear != null && profile.professionalHeadline != null){
        total += 4;
    }
    if(data.phone1Type != null && data.phone1Number!=null && data.phone1CountryCode!=null){
        total += 4;
    }
    if(data.address1_Zip!=null && data.address1_Address!=null){
        total += 4;
    }
    if(data.address1_City!=null && data.address1_CountryCode!=null && data.address1_StateCode!=null){
        total += 4;
    }

    return total;
    //eturn data.address1_Type!=null && data.address1_Zip!=null && data.address1_City!=null && data.address1_CountryCode!=null && data.address1_StateCode!=null && data.address1_Address!=null && data.phone1Type!=null && data.phone1Number!=null && data.phone1CountryCode!=null && data.phone1AreaCode!=null && user.profile.professionalHeadline!=null;
}

function checkDatiViaggioMA(user) {
    var data = user.data;
    var numDocs = 0;
    if(data.identityCard_Number != null && data.identityCard_ValidFrom != null && data.identityCard_ExpireOn != null && data.identityCard_CountryCode != null)
        numDocs++;
    if(data.passport_Number != null && data.passport_ValidFrom != null && data.passport_ExpireOn != null && data.passport_CountryCode != null)
        numDocs++;
    if(data.green_Number != null /*&& data.green_CountryCode != null && data.green_NationalityCode != null*/ && data.green_ExpireOn != null && data.green_BornDate != null)
        numDocs++;
    if(data.visa_Number != null && data.visa_ValidFrom != null && data.visa_CountryCode != null)
        numDocs++;


    if(numDocs==1){
        return 10;
    }else if(numDocs >=2){
        return 20;
    }else{
        return 0;
    }
}

function checkPreferenzeViaggio(user) {
    var data = user.data;
    var total = 0;

    if(data.flightPref_Seat != null){
        total += 5;
    }
    if(data.flightPref_Meal != null){
        total += 5;
    }
    if(data.flightPref_FF != null && data.flightPref_FF.length>0){
        total += 5;
    }
    if(data.flightPref_Airport != null){
        total += 10;
    }
    if(data.flightPref_Family != null){
        total += 5;
    }

    return total;
    /*data.data.flightPref_Family  &&return  data.flightPref_Airport != null && data.flightPref_Airport.length() > 0;*/
}

function checkDatiFatturazione(user){
    var data = user.data;
    return 0;
}

function checkMMConnection(data) {
    /*return data.data.id_mm;*/
    return true;
}


function performMALogin(user, pass) {
    doMALogin(user, pass, loginMASuccess, loginMAFail);
}

function loginMASuccess(resp) {
    $("[aria-label='Close Accessible Modal']").trigger("click");
    $(".errorLoginMA").text("");
    //setHeaderMA(resp);

    // salvataggio utente MA in servlet
//    $("#form-user-ma input[name=userMAAsJsonString]").val(JSON.stringify(resp));
//    performSubmit("savemauser", "#form-user-ma",
//    			saveUserMASuccess, saveUserMAFail);
}