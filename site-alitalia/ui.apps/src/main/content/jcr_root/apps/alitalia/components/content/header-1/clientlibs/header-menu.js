var headerMenuOptions = {};
var lastHeightArr = [];
var startFixedHeader = null;

$('#header .navigation').foundation();
$("#header").removeClass("hide");
if(navigator.platform.indexOf("iPhone") != -1){
    $(document).foundation();
}

$( document ).ready( function() {
    // applyClientContextProfile();
    // $("#menuTogglable").removeClass("hide");
    applyClientContextProfile();
    //!!Da decommentare una volta che viene fixato il contatore notifiche!!!!
    headerNotificationOptions.cloneItems(headerMenuOptions.initDrillDown);
    var headerHeight = $("#header").outerHeight();
    if ($('.hero__slider').length) {
        startFixedHeader = $(".hero__slider").outerHeight() + headerHeight;
    } else if ($('.booking').length) {
        startFixedHeader = $(".hero__slider").outerHeight();
    } else if ($('.checkin__stepbar').length) {
        startFixedHeader = $(".hero__slider").outerHeight();
    } else {
        startFixedHeader = 300;
    }
    $(".nav-wrapper").css("height",headerHeight);
    if(getCookie("closeAlert") == "") { $(".alert").css("display", "block"); }
    else { $(".alert").css("display", "none"); }



    // Patch for a Bug in v6.3.1
    // NOTA: in realtà lo uso per aggiornare i back, che altrimenti nella transizione desktop -> mobile si perdono
    // https://foundation.zurb.com/sites/docs/responsive-navigation.html#drilldown-accordion-menu
    $(window).on('changed.zf.mediaquery', function() {
        $('.is-accordion-submenu.invisible').removeClass('invisible');
        headerMenuOptions.updateDrilldownMenuBack();
    });

    headerMenuOptions.updateDrilldownMenuBack();


    $(window).on('open.zf.drilldown', function( event, name ) {
        lastHeightArr.push($('.is-drilldown').height());
        headerNotificationOptions.checkDrilldownOpen(event);
        if($(name).hasClass('mobile-notify-wrap')) {
            setTimeout(function() {
                $('.is-drilldown').height(lastHeightArr[lastHeightArr.length - 1]);
            }, 100);
        }
    });

    $(window).on('hide.zf.drilldown', function( event, name ) {
            lastHeightArr.pop();
            $('.is-drilldown').height(lastHeightArr[lastHeightArr.length - 1]);
    });

    $( '#header .menu-toggle' ).on( 'click', function(e) {
        if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
        	/*SetBodyLock è definito solo nei template che si riferiscono a base-1*/
        	if (typeof(setBodyLock) == 'undefined') {
        		$('body').addClass('lock');
        	    $('body').height(window.innerHeight);
        	} else {
        		setBodyLock();
        	}
        }
    });


    var ismobile = (/iphone|ipod|android|blackberry|opera|mini|windows\sce|palm|smartphone|iemobile/i.test(navigator.userAgent.toLowerCase()));
    var istablet = (/ipad|android 3.0|xoom|sch-i800|playbook|tablet|kindle/i.test(navigator.userAgent.toLowerCase()));

    $( '#header .mobile-menu-tools .close-button' ).on( 'click', function(e) {
        if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
        	/*removeBodyLock è definito solo nei template che si riferiscono a base-1*/
        	if (typeof(removeBodyLock) == 'undefined') {
        		$('body').removeClass('lock');
        	    $('body').removeAttr('style');
        	} else {
        		removeBodyLock();
        	}
        }
        if (ismobile || istablet){
            $('.mainmenu>.is-drilldown>ul>li.is-drilldown-submenu-parent>ul.menu.submenu.is-drilldown-submenu').removeClass('is-active').addClass('invisible');
            $('.is-drilldown').css('height',$('.menu.drilldown').height())
        }
        //reset menu so when user opens it again is at first level -only for devices
        if (/^((?!chrome|android).)*safari/i.test(navigator.userAgent) && ((/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
)){
            $('.mainmenu>.is-drilldown>ul>li.is-drilldown-submenu-parent>ul.menu.submenu.is-drilldown-submenu').removeClass('is-active').addClass('invisible');
        $('.is-drilldown').css('height',$('.menu.drilldown').height())
        }
    });

    $(".box-support-title").text(capitalize($(".box-support-title").text()));

    $(".is-drilldown").css("min-height", "-webkit-fill-available");


    // Apertura Personal Area MyAlitalia Mobile
    $('.info-user__personal-area-toggle').on('click', function () {
        $(this).toggleClass('open');
        $('.info-user__personal-area-content').slideToggle(400, function(){
            verticalCenter();
        });
        verticalCenter();
        return false;
    });
});

headerMenuOptions.initDrillDown = function() {
    $('#header .navigation').foundation();
};

headerMenuOptions.updateDrilldownMenuBack = function() {
    // Per mettere al posto dell'etichetta "back" nel menu drilldown la voce del menu parent.
    // https://stackoverflow.com/questions/43281429/custom-text-for-drilldown-menu-back-button-foundation-6
    $('.js-drilldown-back a').each(function(){
        var backTxt = $(this).parent().closest('.is-drilldown-submenu-parent').find('> a').html();
        $(this).html(backTxt);
    });
}

function getCookie(cname) {
    var nameEQ = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return "";
}
function capitalize(stringa) {
    stringa = stringa.toLowerCase();
    return stringa.charAt(0).toUpperCase()+stringa.slice(1);
}

function setBodyLock(){
    $('body').addClass('lock');
    $('body').height(window.innerHeight);
}

$("[aria-label='Close menu']").on("click", function() {
    $('html,body').animate({scrollTop:0},0);
});

function resizeHeaderFixed(){

    var header = $("#header");
    	var secondaryMenu = $(".menu-secondary");
    	var logoWidth = 0;
    	var logoSkyWidth = 0;
        var logoWidthFixed = 0;
        var logoSkyWidthFixed = 0;
        var windowWidth = window.innerWidth;
        if (windowWidth < 768) {
            logoWidth = "100px";
            logoSkyWidth = "26px";
            logoWidthFixed = "100px";
            logoSkyWidthFixed = "26px";
        } else if (windowWidth >= 768 && windowWidth < 1024 ) {
            logoWidth = "130px";
            logoSkyWidth = "34px";
            logoWidthFixed = "130px";
            logoSkyWidthFixed = "34px";
        } else if (windowWidth == 1024) {
            logoWidth = "225px";
            logoSkyWidth = "54px";
            logoWidthFixed = "190px";
            logoSkyWidthFixed = "50px";
        } else if (windowWidth > 1024) {
            logoWidth = "225px";
            logoSkyWidth = "54px";
            logoWidthFixed = "190px";
            logoSkyWidthFixed = "50px";
        };
        var scroll = $(window).scrollTop();
        var closeToFooter = $( document ).height() - $(".footer").outerHeight() - 500;
		if (scroll >= startFixedHeader && scroll <= closeToFooter) {
			header.removeClass("close-animation").addClass("open-animation sticky-header");
			secondaryMenu.addClass("hide");
			$("#menu-header-1").addClass("pad-top-large");
			$('.mainlogo').children('img').css("max-width",logoWidthFixed);
			$('#skyteam-header').children('img').css("max-width",logoSkyWidthFixed);
//			$('.mainlogo').parent('.column').css("margin-top","8px")
        } else if ((scroll <= startFixedHeader && scroll > 150) || scroll > closeToFooter){
            header.removeClass("open-animation").addClass("close-animation");
		} else {
			header.removeClass("close-animation sticky-header open-animation");
			secondaryMenu.removeClass("hide");
			$("#menu-header-1").removeClass("pad-top-large");
			$('.mainlogo').children('img').css("max-width",logoWidth);
			$('#skyteam-header').children('img').css("max-width",logoSkyWidth);
//			$('.mainlogo').parent('.column').css("margin-top","0px")
		}

}

$(window).scroll(function() {
    resizeHeaderFixed();
});



   if (window.location.href.indexOf("he_il") != -1) {

                $("#menuTogglable").removeClass("position-right").addClass("position-left");
                }


//open.zf.drilldownMenu     Fires when the submenu has opened.
//hide.zf.drilldownMenu