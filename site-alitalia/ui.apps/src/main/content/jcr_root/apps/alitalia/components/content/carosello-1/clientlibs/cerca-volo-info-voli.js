var cercaVoloInfoVoliOptions = {};
var cercaVoloInfoVoliRevealOptions={};

var inputAeroportoPartenza;
var inputAeroportoArrivo;
var infovoli_departures;
var infovoli_arrival;
var dataPartenza;

var market = "";

// document ready
$( document ).ready( function() {
	cercaVoloInfoVoliOptions.switchForm();

    market = $('html').attr('lang').split('-')[1];

    $( '.cerca-volo__content--info-voli [data-ref]' ).on( 'focus', function() {
    		cercaVoloPrenotaOptions.closeAllCollapsablePanels();
//    		pulisciInfoStatoVolo();
    } );
	// cercaVoloInfoVoliOptions.updateHiddenFormAllValues();

	var infoVoliStatoDataOptions = {
		//dateFormat: "D d M yy",
		dateFormat: "dd/mm/yyyy",
		checkDisabledDays : false,
		startDate: null,
		endDate: null,
		from: $('input[data-calendar="infoVoliStatoDataOptions"]'),
		to: null,
        altFormat: (market != "us" && market != "ca") ? "dd/mm/yy" : "mm/dd/yy", // controllo fake per US - impostazione formato calendario
		altFieldFrom: $('input[data-calendar="infoVoliStatoDataOptions"]').data('ref'),
		altFieldTo: null,
		curRange: null,
		curInput: null,
		y1: 0,
		y2: 0,
		curYFrom: 0,
        curYTo: 0,
		curY: 0,
		numberOfMonths: 2,
        maxDate: "+7d",
        minDate: "-2d"
	};
	setupCalendarPicker(infoVoliStatoDataOptions);

	var infoVoliStatoDataOptionsMobile = {
		//dateFormat: "D d M yy",
		dateFormat: "dd/mm/yyyy",
		checkDisabledDays : false,
		startDate: null,
		endDate: null,
		from: $('input[data-calendar="infoVoliStatoDataOptionsMobile"]'),
		to: null,
        altFormat: (market != "us" && market != "ca") ? "dd/mm/yy" : "mm/dd/yy", // controllo fake per US - impostazione formato calendario
		altFieldFrom: $('input[data-calendar="infoVoliStatoDataOptionsMobile"]').data('ref'),
		altFieldTo: null,
		curRange: null,
		curInput: null,
		y1: 0,
		y2: 0,
		curYFrom: 0,
        curYTo: 0,
		curY: 0,
		numberOfMonths: 1,
        maxDate: "+7d",
        minDate: "-2d"
	};
	setupCalendarPicker(infoVoliStatoDataOptionsMobile);

	var infoVoliOrariDataOptions = {
		//dateFormat: "D d M yy",
		dateFormat: "dd/mm/yyyy",
		checkDisabledDays : false,
		startDate: null,
		endDate: null,
		from: $('input[data-calendar="infoVoliOrariDataOptions"]'),
		to: null,
        altFormat: (market != "us" && market != "ca") ? "dd/mm/yy" : "mm/dd/yy", // controllo fake per US - impostazione formato calendario
		altFieldFrom: $('input[data-calendar="infoVoliOrariDataOptions"]').data('ref'),
		altFieldTo: null,
		curRange: null,
		curInput: null,
		y1: 0,
		y2: 0,
		curYFrom: 0,
        curYTo: 0,
		curY: 0,
		numberOfMonths: 2
	};
	setupCalendarPicker(infoVoliOrariDataOptions);

	var infoVoliOrariDataOptionsMobile = {
		//dateFormat: "D d M yy",
		dateFormat: "dd/mm/yyyy",
		checkDisabledDays : false,
		startDate: null,
		endDate: null,
		from: $('input[data-calendar="infoVoliOrariDataOptionsMobile"]'),
		to: null,
        altFormat: (market != "us" && market != "ca") ? "dd/mm/yy" : "mm/dd/yy", // controllo fake per US - impostazione formato calendario
		altFieldFrom: $('input[data-calendar="infoVoliOrariDataOptionsMobile"]').data('ref'),
		altFieldTo: null,
		curRange: null,
		curInput: null,
		y1: 0,
		y2: 0,
		curYFrom: 0,
        curYTo: 0,
		curY: 0,
		numberOfMonths: 1
	};
	setupCalendarPicker(infoVoliOrariDataOptionsMobile);

    $('#flight-number--flight-status-mobile').keyup(function(e){
       if(e.keyCode == 13){
            cercaVoloPrenotaOptions.closeAllCollapsablePanels();
       }
    });

    $("#flight-number--flight-status-desk").on('blur', function(){
        if ($(this).val().length > 4 || !$.isNumeric($(this).val()))
        {
            $('.labelErrorInfoStatoVolo').show();
            $('.labelErrorInfoStatoVolo').addClass('feedback-error');
            $('.labelErrorInfoStatoVolo').html(CQ.I18n.get('preparaViaggio.numeroVolo.error.invalid'));
        }
        else
        {
            $('.labelErrorInfoStatoVolo').hide();
            $('.labelErrorInfoStatoVolo').removeClass('feedback-error');
            $('.labelErrorInfoStatoVolo').html("");
        }
    });

    $(".checkFlightStatus").click(function(){
        if ($("#flight-number--flight-status-mobile").val().length > 4 || !$.isNumeric($("#flight-number--flight-status-mobile").val()))
        {
            $('.labelErrorInfoStatoVolo').show();
            $('.labelErrorInfoStatoVolo').addClass('feedback-error');
            $('.labelErrorInfoStatoVolo').html(CQ.I18n.get('preparaViaggio.numeroVolo.error.invalid'));
        }
        else
        {
            $('.labelErrorInfoStatoVolo').hide();
            $('.labelErrorInfoStatoVolo').removeClass('feedback-error');
            $('.labelErrorInfoStatoVolo').html("");
        }
    });



function resetErrors(){
    $('input').removeClass('is-invalid-input');
    $('input').prev('span').removeClass('is-invalid-input');
    $('input').next('span').removeClass('is-invalid-input');
    $('.labelErrorInfoStatoVolo').css('display','none');
    $('.labelErrorInfoStatoVolo').html("");
    $('.labelErrorInfoStatoVolo').css('display','none');
    $('#labelErrorInfoOrarioVolo').css('display','none');
}
function validationInputs(type){
    var inputPnr, inputFF,inputName,inputLastname;
    var twoToFourNumbers = /^[0-9]{2,4}$/;
    var errorInputs = [];
    resetErrors();
    if (type == "info voli"){
    	if ($('#flight_status').parent().hasClass('checked')){


    		//radio stato del volo checked

    		if (Foundation.MediaQuery.atLeast('large')) {
    			var _type = 'desk';
            }
            else {
				var _type = 'mobile';
            }
            var inputFNumber = $( '#flight-number--flight-status-' + _type ).val().trim();

            // var inputFNumber2 = $( '#flight-number--flight-status-desk').val().trim();
            var dateInput = $( '#departure_date--flight-status-' + _type ).val();
            //        checkInOptions.input1Val = ( $( '#frequentFlyer' ).val() !== '' ) ? 1 : 0;
            //        checkInOptions.input3Val = ( $( '.lastNameCercaByFFSubmit' ).val() !== '') ? 1 : 0;
            checkInOptions.input1Val = ( inputFNumber !== '' && twoToFourNumbers.test(inputFNumber)) ? 1 : 0;
    		checkInOptions.input2Val = ( dateInput !== '') ? 1 : 0;
        	// checkInOptions.input2Val = ( $( '#departure_date--flight-status-' + _type ).val() !== '') ? 1 : 0;
        	/*if (!checkInOptions.input1Val && !checkInOptions.input2Val)
            {
                _type = 'mobile';
                checkInOptions.input1Val = ( $( '#flight-number--flight-status-' + _type ).val() !== '' ) ? 1 : 0;
                checkInOptions.input2Val = ( $( '#departure_date--flight-status-' + _type ).val() !== '') ? 1 : 0;
            } */
        	if( checkInOptions.input1Val && checkInOptions.input2Val) {
        		return true;
        	}else{
        		if (!checkInOptions.input1Val){
                	errorInputs.push($( 'input#flight-number--flight-status-' + _type ));
                    // errorInputs.push($( 'input#flight-number--flight-status-desk'));
           	 	}
            	if (!checkInOptions.input2Val){
                    errorInputs.push($( 'input#departure_date--flight-status-' + _type ));
                	// errorInputs.push($( 'input#departure_date--flight-status-desk'));
            	}
           
        	}

    	}else{
    		if ($('#timetables').parent().hasClass('checked')){
    			//radio orari checked

    			if (Foundation.MediaQuery.atLeast('large')) {
    				var _type = 'desk';
            	}
            	else {
					var _type = 'mobile';
            	}

    			// checkInOptions.input1Val = ( $( '#luogo-partenza--timetables-' + _type ).val() !== '' ) ? 1 : 0;
       //  		checkInOptions.input2Val = ( $( '#luogo-arrivo--timetables-' + _type ).val() !== '') ? 1 : 0;
        		checkInOptions.input3Val = ( $( '#departure_date--timetables-' + _type ).val() !== '') ? 1 : 0;

                checkInOptions.input1Val = ( $( '#luogo-partenza--timetables-desk').val().trim() !== '' ) ? 1 : 0;
                checkInOptions.input2Val = ( $( '#luogo-arrivo--timetables-desk').val().trim() !== '') ? 1 : 0;
                // checkInOptions.input3Val = ( $( '#departure_date--timetables-desk').val().trim()!== '') ? 1 : 0;

        	/*	if (!checkInOptions.input1Val && !checkInOptions.input2Val && !checkInOptions.input3Val)
        		{
        		    _type = 'mobile';

                    checkInOptions.input1Val = ( $( '#luogo-partenza--timetables-' + _type ).val() !== '' ) ? 1 : 0;
                    checkInOptions.input2Val = ( $( '#luogo-arrivo--timetables-' + _type ).val() !== '') ? 1 : 0;
                    checkInOptions.input3Val = ( $( '#departure_date--timetables-' + _type ).val() !== '') ? 1 : 0;
        		} */

				if( checkInOptions.input1Val && checkInOptions.input2Val  && checkInOptions.input3Val) {
            		return true;
        		} else {
            		if (!checkInOptions.input1Val){
                        errorInputs.push($( 'input#luogo-partenza--timetables-desk'));
		                // errorInputs.push($( 'input#luogo-partenza--timetables-' + _type ));
		                //document.getElementById("pnr").style.border = "2px solid #e30512";
		                //document.getElementById("pnr").style.color = "#e30512";
		            }
		            if (!checkInOptions.input2Val){
                        errorInputs.push($( 'input#luogo-arrivo--timetables-desk'));
		                // errorInputs.push($( 'input#luogo-arrivo--timetables-' + _type));
		                //document.getElementById("firstName").style.border = "2px solid #e30512";
		            }
		            if (!checkInOptions.input3Val){
                        // errorInputs.push($( 'input#departure_date--timetables-desk'));
		                errorInputs.push($( 'input#departure_date--timetables-' + _type));
		                //document.getElementById("lastName").style.border = "2px solid #e30512";
		            }
		        }

    		}
    	}
    }
    setErrorInputInfoVoli(errorInputs);
    return false;
}


	$('#flight-status-search').click(function(){
		console.log("flight-status-search in cerca-volo-info-voli.js");
		if (validationInputs('info voli')){
		    $('.cerca-volo__inner').addClass('loading');
			performSubmit("preparaviaggiosubmit",recuperaDati(),infoVoliSubmitSuccess, infoVoliSubmitFail);
		}
        
	});

    /*
	$('#submitHidden--timetables').on('click',function(e){
		e.preventDefault();
		e.stopPropagation();
		if (validationInputs('info voli')){
			performSubmit("preparaviaggiosubmit",recuperaDati(),infoVoliSubmitSuccess, infoVoliSubmitFail);	
		}
	})
    */




	$('#flight-list-search').click(function(){

        if (validationInputs('info voli')){
            $('.cerca-volo__inner').addClass('loading');

            $('#labelErrorInfoOrarioVolo').removeClass('feedback-error');
            $('#labelErrorInfoOrarioVolo').html('');

            performSubmit("preparaviaggiosubmit",recuperaDatiOrari(),infoVoliOrariSubmitSuccess, infoVoliOrariSubmitFail);
        }

    });

    /*$( '[data-responsive-accordion-tabs]' ).on('change.zf.tabs',function(e){
        pulisciInfoStatoVolo();
    });*/

    $('#cerca-volo__close-button').on('click',function(e){
        pulisciCercaVoloPrenota();
        pulisciInfoStatoVolo();
        pulisciOrari();
    });
} );

function setErrorInputInfoVoli(inputArr){
    resetErrors();
    var messageError = "";
    inputArr.forEach(function(inp){
        $(inp).addClass('is-invalid-input');
        $(inp).prev().addClass('is-invalid-input');
        $(inp).next().addClass('is-invalid-input');
        


        switch ($(inp).attr('id')){
            case 'flight-number--flight-status-desk':
                var $currInp = $(inp);
                $('.flight-status-span').addClass("is-invalid-input");
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.numeroVolo.error.invalid') + ". ";    
                }else{
                    messageError = messageError + CQ.I18n.get('preparaViaggio.numeroVolo.error.empty') + ". ";
                }
                break;
            case 'flight-number--flight-status-mobile':
                $('.flight-status-span').addClass("is-invalid-input");
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.numeroVolo.error.invalid') + ". ";    
                }else{
                    messageError = messageError + CQ.I18n.get('preparaViaggio.numeroVolo.error.empty') + ". ";
                }
                break;
            case 'departure_date--flight-status-desk':
                $('span[data-toggle-panel="#panel-data--flight-status"]').addClass("is-invalid-input");
                
                messageError = messageError + CQ.I18n.get('preparaViaggio.dataPartenza.error.empty') + ". ";
                break;
            case 'departure_date--flight-status-mobile':
                $('span[data-toggle-panel="#panel-data--flight-status"]').addClass("is-invalid-input");
                messageError = messageError + CQ.I18n.get('preparaViaggio.dataPartenza.error.empty') + ". ";
                break;
            case 'luogo-partenza--timetables-desk':
                messageError = messageError + CQ.I18n.get('preparaViaggio.aeroportoPartenza.error.empty') + ". ";
                break;
            case 'luogo-partenza--timetables-mobile':
                messageError = messageError + CQ.I18n.get('preparaViaggio.aeroportoPartenza.error.empty') + ". ";
                break;
            case 'luogo-arrivo--timetables-desk':
                messageError = messageError + CQ.I18n.get('preparaViaggio.aeroportoArrivo.error.empty') + ". ";
                break;
            case 'luogo-arrivo--timetables-mobile':
                messageError = messageError + CQ.I18n.get('preparaViaggio.aeroportoArrivo.error.empty') + ". ";
                break;
            case 'departure_date--timetables-desk':
                messageError = messageError + CQ.I18n.get('preparaViaggio.dataPartenza.error.empty') + ". ";
                $('#departure_date--timetables-mobile').addClass("is-invalid-input");
                $('[data-toggle-panel="#panel-data--timetables"]').addClass('is-invalid-input');
                
                break;
            case 'departure_date--timetables-mobile':
                messageError = messageError + CQ.I18n.get('preparaViaggio.dataPartenza.error.empty') + ". ";
                $('#departure_date--timetables-desk').addClass("is-invalid-input");
                $('[data-toggle-panel="#panel-data--timetables"]').addClass('is-invalid-input');
                break;
        }
    });
    if (messageError){
        $('#labelCheckinError').css('display','block');
        $('.labelErrorInfoStatoVolo').css('display','block');
        $('.labelErrorInfoStatoVolo').addClass('feedback-error');
        $('.labelErrorInfoStatoVolo').html(messageError);
        $('#labelErrorInfoStatoVolo').html(messageError);
        $('#labelErrorInfoStatoVolo').css('display','block');
        $('#labelCheckinError').html(messageError);
        $('#labelErrorInfoOrarioVolo').html(messageError);
        $('#labelErrorInfoOrarioVolo').css('display','block');
    }

}
// $('#cercamyFlightSubmit').on('click',function(e){
// 	e.preventDefault();
// 	e.stopPropagation();
// 	console.log("BUTTON cercamyFlightSubmit CLICK");
// });
// $('#flight-status-search').on('click',function(){
// 	console.log($(this));

// });


cercaVoloInfoVoliOptions.switchForm = function() {
	$('.cerca-volo__content--info-voli input[type="radio"]').click(function() {
	    cercaVoloPrenotaOptions.closeAllCollapsablePanels();
	    pulisciInfoStatoVolo();

       if($(this).hasClass('radio-flight_status')) {
       		$('.cerca-volo__content--info-voli .flight_status').removeClass('hide');
       		$('.cerca-volo__content--info-voli .timetables').addClass('hide');
       } else {
       		$('.cerca-volo__content--info-voli .timetables').removeClass('hide');
       		$('.cerca-volo__content--info-voli .flight_status').addClass('hide');
       }
   });
};

function recuperaDati(){

    var data = {
    			formType : $('#formType').val(),
    			numeroVolo : $('#flightNumber--flight-status').val(),
    			dataPartenza : $('#departureDate--flight-status').val()
    		};
    return data;
}

function recuperaDatiOrari(){

    inputAeroportoPartenza = $("#luogo-partenza--timetables-desk").val();
    if (inputAeroportoPartenza == '')
        inputAeroportoPartenza = $("#luogo-partenza--timetables-mobile").val();

    inputAeroportoArrivo = $("#luogo-arrivo--timetables-desk").val();
    if (inputAeroportoArrivo == '')
        inputAeroportoArrivo = $("#luogo-arrivo--timetables-mobile").val();

    infovoli_departures = inputAeroportoPartenza.slice(-3);
    infovoli_arrival = inputAeroportoArrivo.slice(-3);
    dataPartenza = $("#departureDate--timetables").val();

    var data = {
    			formType:"ORARI",
                infovoli_departures:infovoli_departures,
                infovoli_arrival:infovoli_arrival,
                dataPartenza:dataPartenza,
                origin:inputAeroportoPartenza,
                destination:inputAeroportoArrivo,
                _action:"validate"
    		};

    return data;
}

function infoVoliSubmitSuccess(data) {

    $('.cerca-volo__inner').removeClass('loading');

	if (data.result == "OK") {
		var numeroVolo = $('#flightNumber--flight-status').val();

		var dataPartenza = $('#departureDate--flight-status').val();

        $.ajax({
            type:"GET",
            url: location.protocol + "//" + location.host + internalUrl1.substring(0, internalUrl1.lastIndexOf("/")) + '/special-pages/flight-info-1.html',
            dataType: 'html',
            data:{
                flight: "AZ" + encodeURIComponent(numeroVolo),
                date: encodeURIComponent(dataPartenza.replace(/\//g,"-"))
                }
            })
             .done(function(e){
                if($('.labelErrorInfoStatoVolo').hasClass('feedback-error')){
                    $('.labelErrorInfoStatoVolo').removeClass('feedback-error');
                    $('.labelErrorInfoStatoVolo').html('');
                }

                $('.cerca-volo__inner').removeClass('loading');

                $('#panel-flight-status div.panel__header-mobile').next().remove();
                $('#panel-flight-status div.panel__header-mobile').after(e);

                cercaVoloPrenotaOptions.openCollapsablePanel( $('#panel-flight-status') );

                currentFocusDatePicker = undefined;

                 // document.getElementById("error_partenza").style.color = "#000000";
                 // document.getElementById("error_partenza").style.border = "1px solid #c1c1c1";
                 // document.getElementById("error_numero_volo").style.color = "#000000";
                 // document.getElementById("error_numero_volo").style.border = "1px solid #c1c1c1";
            })
            .fail(function(){
                $('.labelErrorInfoStatoVolo').addClass('feedback-error');
                $('.labelErrorInfoStatoVolo').html(CQ.I18n.get('specialpage.error.service'));
            });
	} else {
        $('.labelErrorInfoStatoVolo').addClass('feedback-error');
        if (data.errorMessage !== undefined) {
            //response della validazione lato servlet AEM
            var invalidFlight = data.errorMessage.includes("preparaViaggio.numeroVolo.error.invalid");
            if (invalidFlight) {
                var invalidFlightMessage = CQ.I18n.get('preparaViaggio.numeroVolo.error.invalid')
                $('.labelErrorInfoStatoVolo').css('display', 'block');
                $('.labelErrorInfoStatoVolo').html(invalidFlightMessage);
                $('.flight-status-span').addClass("is-invalid-input");


            } else {
                $('#labelCheckinError').show();
                $('#labelCheckinError').text(CQ.I18n.get('specialpage.error.service'));
            }
        } else {
            //response della validazione lato servizio BL
            if (data.fields !== undefined) {
                $('.labelErrorInfoStatoVolo').html(data.fields.numeroVolo);
                $('.labelErrorInfoStatoVolo').show();
            } else {
                var dataString = JSON.stringify(data);
                if (dataString.includes('numeroVolo=preparaViaggio.numeroVolo.error.invalid, dataPartenza=preparaViaggio.dataPartenza.error.invalid')) {
                    $('.labelErrorInfoStatoVolo').html(CQ.I18n.get('preparaViaggio.numeroVolo.error.invalid') + ' ' + CQ.I18n.get('preparaViaggio.dataPartenza.error.invalid'));
                    $('span[data-toggle-panel="#panel-data--flight-status"]').addClass("is-invalid-input");
                    $('.flight-status-span').addClass("is-invalid-input");

                }
                if (dataString.includes('{dataPartenza=preparaViaggio.dataPartenza.error.invalid}')) {
                    $('.labelErrorInfoStatoVolo').html(CQ.I18n.get('preparaViaggio.dataPartenza.error.invalid'));
                    $('span[data-toggle-panel="#panel-data--flight-status"]').addClass("is-invalid-input");
                }
                if (dataString.includes('{numeroVolo=preparaViaggio.numeroVolo.error.invalid}')) {
                    $('.labelErrorInfoStatoVolo').html(CQ.I18n.get('preparaViaggio.numeroVolo.error.invalid'));
                    $('.flight-status-span').addClass("is-invalid-input");
                }
                // $('input').next('span').removeClass('is-invalid-input');
            }
        }
    }
}

function infoVoliSubmitFail(data) {
	$('.labelErrorInfoStatoVolo').addClass('feedback-error');
    $('.labelErrorInfoStatoVolo').html(CQ.I18n.get('specialpage.error.service'));
	return false;
}

function infoVoliOrariSubmitSuccess(data) {

    $('.cerca-volo__inner').removeClass('loading');

    if (data.result == true)
	{

        var url = '&infovoli_departures=' + inputAeroportoPartenza +
                    '&infovoli_arrival=' + inputAeroportoArrivo +
                    '&dataPartenza=' + dataPartenza +
                    '&origin=' + inputAeroportoPartenza +
                    '&destination=' + inputAeroportoArrivo;

// Cosi' funziona negli ambienti tipo: stage.alitalia.com/it_it e .../content/alitalia/master-it/it/homepage.html
        window.location.href = location.protocol + "//" + location.host + internalUrl1.substring(0, internalUrl1.lastIndexOf("/")) + '/booking/flight-info-list.html?formType=ORARI' + url;

    } else {

        var errorMsg = "";

        var obj = data.fields;
        for(var key in obj)
        {
            errorMsg += obj[key] + ". ";
        }

        $('#labelErrorInfoOrarioVolo').addClass('feedback-error');
        $('#labelErrorInfoOrarioVolo').html(errorMsg);

    }

}

function infoVoliOrariSubmitFail(data)
{
    $('.cerca-volo__inner').removeClass('loading');

	$('#labelErrorInfoOrarioVolo').addClass('feedback-error');
    console.log("DATA ",data);
    $('#labelErrorInfoOrarioVolo').html(CQ.I18n.get('specialpage.error.service'));
	return false;
}

function pulisciInfoStatoVolo(){
    var $data = $('#departure_date--flight-status-desk').datepicker();
    //$data.datepicker('setDate', null);
    $('span[data-toggle-panel="#panel-numero-volo--flight-status"]').html(CQ.I18n.get('preparaViaggio.numeroVolo.label'));
    $('input#flight-number--flight-status-desk').focus(function(){($(this).val(""));});
    $('span[data-toggle-panel="#panel-data--flight-status"]').html(CQ.I18n.get('preparaViaggio.label.dataPartenza'));
    $('input#departure_date--flight-status-desk').focus(function(){($(this).val(""));});
    currentFocusDatePicker = undefined;
}

function pulisciCercaVoloPrenota()
{

    // Luogo partenza
    $("#luogo-partenza--prenota-desk").removeClass("is-invalid-input");
    $('span[data-toggle-panel="#panel-list_andata--prenota"]').removeClass("is-invalid-input");

    // Luogo arrivo
    $("#luogo-arrivo--prenota-desk").removeClass("is-invalid-input");
    $('span[data-toggle-panel="#panel-all-destination"]').removeClass("is-invalid-input");

    // Data partenza
    $("#data-andata--prenota-desk").removeClass("is-invalid-input");
    $('span[data-toggle-panel="#panel-date-andata--prenota"]').removeClass("is-invalid-input");

    // Data ritorno
    $("#data-ritorno--prenota-desk").removeClass("is-invalid-input");
    $('span[data-toggle-panel="#panel-date-ritorno--prenota"]').removeClass("is-invalid-input");

    $(".labelErrorInfoStatoVolo").html("");
    currentFocusDatePicker = undefined;
}

function pulisciOrari()
{

    // Luogo partenza
    $("#luogo-partenza--timetables-desk").removeClass("is-invalid-input");
    $('span[data-toggle-panel="#panel-list_andata--timetables"]').removeClass("is-invalid-input");

    // Luogo arrivo
    $("#luogo-arrivo--timetables-desk").removeClass("is-invalid-input");
    $('span[data-toggle-panel="#panel-destination--timetables"]').removeClass("is-invalid-input");

    // Data partenza
    $("#departure_date--timetables-desk").removeClass("is-invalid-input");
    $('span[data-toggle-panel="#panel-data--timetables"]').removeClass("is-invalid-input");
    currentFocusDatePicker = undefined;

}