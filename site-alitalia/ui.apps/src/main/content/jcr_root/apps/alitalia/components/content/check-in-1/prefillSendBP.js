"use strict";

use(function() {
    var profile = this.profile;

    try {
        // Controllo il tipo di utenza
        if(profile != null && profile.getCustomerMa() != null && profile.getCustomerMa().getMaCustomer() != null && profile.getCustomerMa().getMaCustomer().getProfile() != null) {
            // L'utenza è MyAlitalia
            var data = profile.getCustomerMa().getMaCustomer().getProfile();
            if(data.getFirstName() != null && data.getLastName() != null && data.getEmail() != null) return {firstName: data.getFirstName().toLowerCase(), lastName: data.getLastName().toLowerCase(), email: data.getEmail()};
            else return "isMA";
        } else if(profile != null && profile.customer != null && profile.customer.mmCustomer != null) {
            // L'utenza è MilleMiglia
            var data = profile.customer.mmCustomer;
            if(data.customerName != null && data.customerSurname != null && data.email != null) return {firstName: data.customerName.toLowerCase(), lastName: data.customerSurname.toLowerCase(), email: data.email};
            else return "isMM";
        } else return "notLogged";
    } catch(e) {  return e.toString(); }

});