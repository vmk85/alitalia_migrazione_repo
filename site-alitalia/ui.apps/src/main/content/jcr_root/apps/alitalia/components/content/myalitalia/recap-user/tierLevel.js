"use strict";
use(function() {
    try {
        var user = this.tier;
        var tier = user.getCustomer().getMmCustomer().getTierCode().value();
        if(tier.toLowerCase() == "basic") return "millemiglia";
        else if(tier.toLowerCase() == "ulisse") return "ulisse";
        else if(tier.toLowerCase() == "frecciaalata") return "freccia-alata";
        else if(tier.toLowerCase() == "plus") return "freccia-alata-plus";
        else if(tier.toLowerCase() == "elite") return "elite";
        else if(tier.toLowerCase() == "elite plus") return "elite-plus";
        else return "myalitalia";
        return tier;
    } catch (err) {
        return err.toString();
    }
})