jQuery(document).ready(function(){
	jQuery('#millemigliakidsSubmit').on('click', millemigliaKidsHandler);
});
function millemigliaKidsHandler(e) {
	validation(e, 'millemigliakidssubmit', '#specialpageform_frm_millemiglia_kids', millemigliaKidsSuccess, millemigliaKidsError);
}

function millemigliaKidsSuccess(data) {
	return (data.result ? millemigliaKidsContinue(false) : showErrors(data));
}

function millemigliaKidsError() {
	return millemigliaKidsContinue(true);
}

function millemigliaKidsContinue(stop) {
	removeErrors();
	$('#millemigliakidsSubmit').off('click');
	return stop || $('#millemigliakidsSubmit').click();
}

