$(function initMmb() {
	$(function(){
		$.ajax({
			url: removeUrlSelector() + ".thankyou-main-partial.html",
			context : document.body
		})
		.done(function(data) {
			$(".manageBooking.mod.manageBookingPaymentMetod").append(data);
			
			$('.j-hideOnCLick').click();
			initAccordions();
			bookingReady();
			initMmbThankYou();
			initPartialMmbCart();
			analytics_mmbPartialCallback();
			onMmbTrackingPurchasedItemsInfo();
			$(window).trigger('resize');
		})
		.fail(function() {});
	});
});
