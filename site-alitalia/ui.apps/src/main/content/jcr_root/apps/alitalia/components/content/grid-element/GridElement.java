package apps.alitalia.components.content.grid_element;

import java.util.Iterator;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUse;

public class GridElement extends WCMUse {

	private String path;
	private String resourceType;
	
	@Override
	public void activate() throws Exception {

		ValueMap properties = getProperties();
		String pathRisorsa = properties.get("risorsa", String.class) != null ? properties.get("risorsa", String.class) : null;

		if (pathRisorsa != null) {
			Iterator<Resource> resourceIterator = getResourceResolver().findResources(
					"SELECT * FROM [cq:PageContent] "
					+ "WHERE ISDESCENDANTNODE([" + pathRisorsa + "])", "JCR-SQL2");

			Resource risorsa = null;
			while (resourceIterator.hasNext()) {
				risorsa = resourceIterator.next();
			}

			if (risorsa != null) {
				Iterator<Resource> childResource = risorsa.listChildren();
				while (childResource.hasNext()) {
					path = pathRisorsa + "/jcr:content/" + childResource.next().getName();
				}
				resourceType = risorsa.getResourceType();
            } else{
                path = "null";
				resourceType = "null";
            }
		} else {
			path = "null";
			resourceType = "null";
			//FIXME
		}

	}
	
	public String getPath() {
		return path;
	}
	
	public String getResourceType() {
		return resourceType;
	}
	
}