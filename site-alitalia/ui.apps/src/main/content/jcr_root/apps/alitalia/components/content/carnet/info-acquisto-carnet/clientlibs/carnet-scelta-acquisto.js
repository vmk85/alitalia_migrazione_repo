function getPrices(){
	startLoader(".form__buttonCover");
	$.ajax({
		url: removeUrlSelector() + ".prezzicarnet.json",
		data : {
		},
		context : document.body
	})
	.done(successPrices)
	.fail(failPrices)
	.always(function() {
		stopLoader(".form__buttonCover");
	});
	return false;
}

function successPrices(dataPrices){
    $('#carnet6').append(' ' + dataPrices.carnets[0].firstCarnet);
    $('#carnet12').append(' ' + dataPrices.carnets[1].secondCarnet);
    var price6 =  dataPrices.carnets[0].code;
    var price12 =  dataPrices.carnets[1].code;
    $("input[name='idCarnet1']").val(price6);
    $("input[name='idCarnet2']").val(price12);
    $('#chooseCarnetSubmitButton1').on('click', carnetChooseSubmit1);
    $('#chooseCarnetSubmitButton2').on('click', carnetChooseSubmit2);
}

function failPrices(){
	console.log("error getPrice");
}

function carnetChooseSubmit1(e) {
	validation(e, 'choosecarnet', '#choosecarnet1', chooseCarnetSubmitSuccess1, chooseCarnetSubmitError);
}

function carnetChooseSubmit2(e) {
	validation(e, 'choosecarnet', '#choosecarnet2', chooseCarnetSubmitSuccess2, chooseCarnetSubmitError);
}

function chooseCarnetSubmitSuccess1(data) {
	if (data.result) {
		removeErrors();
		performSubmit('choosecarnet', '#choosecarnet1', 
		function(data) {
			if(data.redirect){
				window.location.replace(data.redirect);
			}
		}, 
		chooseCarnetSubmitError);
		
	} else {
		showErrors(data,true);
		$("#choosecarnet1").anchor();
	}
}

function chooseCarnetSubmitSuccess2(data) {
	if (data.result) {
		removeErrors();
		performSubmit('choosecarnet', '#choosecarnet2', 
		function(data) {
			if(data.redirect){
				window.location.replace(data.redirect);
			}
		}, 
		chooseCarnetSubmitError);
		
	} else {
		showErrors(data,true);
		$("#choosecarnet2").anchor();
	}
}

function chooseCarnetSubmitError() {
	console.log('fail chooseCarnet');
}

$(document).ready(function () {
    getPrices();
});