function initBookingAward(pageName, done) {

	$(function(){
		
		var queryString = "";
		if (getParameterByName("error")) {
			queryString = "?error=" + getParameterByName("error");
		}
		
		$.ajax({
			url: removeUrlSelector() + "." + pageName + "-partial.html" + queryString,
			context : document.body
		})
		.done(function(data) {
			$(".bookingAward.mod").append(data);
			analytics_bookingPartialCallback();
			if (done) {
				done();
			}
		})
		.fail(function() {});
	});
}

function refreshCalendar(done) {

	$(function(){
		
		var queryString = "";
		if (getParameterByName("error")) {
			queryString = "?error=" + getParameterByName("error");
		}
		
		$.ajax({
			url: removeUrlSelector() + ".date-partial.html" + queryString,
			context : document.body
		})
		.done(function(data) {
			$("#booking-award-calendar").replaceWith(data);
            if (done) {
				done();
			}
			
		})
		.fail(function() {});
	});
}

function invokeService(selector, extension, data, done, fail) {
	settings = {
		url: removeUrlSelector() + "." + selector + "." + extension,
		context : document.body
	};
	
	if (!data && extension == "json") {
		data = {
			nocache : getCurrentDate()
		}
	} else if (data && extension == "json") {
		data["nocache"] = getCurrentDate();
	}
	
	if (data) {
		settings["data"] = data;
	}
	
	$.ajax(settings).done(function(data) {
		if (done) {
			done(data);
		}
	})
	.fail(function() {
		if (fail) {
			fail();
		}
	});
}

function initDatePicker() {
	console.log("initDatePicker");
	datePickerController.createDatePicker({
		formElements : {
			andata : pageSettings.dateFormat
		},
		noTodayButton : !0,
		fillGrid : !0,
		rangeLow : new Date(),
		rangeHigh : maxRange,
		noFadeEffect : !0,
		constrainSelection : !1,
		wrapped : !0,
		callbackFunctions : {
			dateset : [ openAndSetReturn ]
		}
	}), datePickerController.createDatePicker({
		formElements : {
			ritorno : pageSettings.dateFormat
		},
		noTodayButton : !0,
		fillGrid : !0,
		rangeLow : new Date(),
		rangeHigh : maxRange,
		constrainSelection : !1,
		noFadeEffect : !0,
		wrapped : !0,
		callbackFunctions : {
			dateset : [ openToolStage ]
		}
	})
}



function getCurrentDate() {
	return new Date().getTime();
}

$(document).ready(function() {
	$.ajaxSetup({ cache: false });
});
