function refreshCarnetSession(){
	$.ajax({
		url: removeUrlSelector() + ".refreshcarnetsession.json",
		data : {
        _isAjax: true,
		},
		context : document.body
	})
	.done(successRefreshCarnetSession)
	.fail(failRefreshCarnetSession);
	return false;
}

function successRefreshCarnetSession(data){
	if(data.redirect){
    	window.location.replace(data.redirect);
    } else{
    	console.log("refresh eseguito correttamente");
    }
}
function failRefreshCarnetSession(){
	console.log("errore durante il refresh. Eseguo logout");
	//TODO: effettuare chiamata logout
}

function typViewStatistic(){
    performSubmit('bookingtypviewstatisticconsumer', {"showCallCenterMsg": getParameterByName("showCallCenterMsg")}, 
		function(data) {
			if(data.isError || data.result === "false"){
				console.log("error in typ view statistic");
			}
			else{
				console.log("typ viewstatistic ok");
			}
		}
		,function(){
			console.log("error in typ view statistic");
		});
}

$(document).ready(function () {
	//banner hidden on mobile and tablet
	if (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase())){
		$('.bannerTrenitalia').hide();
		$('.bannerTrenitalia_EN').hide();
	} else {
	    $('.bannerTrenitalia_m').hide();
        $('.bannerTrenitalia_EN_m').hide();
    }

	var gigyaData = {
		gigya : JSON.stringify({
			flight_pnr: $("#savePnrData input[name=PNR]").val(),
			flight_firstName: $("#savePnrData input[name=NAME]").val(),
			flight_lastName: $("#savePnrData input[name=LASTNAME]").val(),
			flight_departureDate: $("#savePnrData input[name=DEPARTURE_DATE]").val(),
			flight_nationDestination: $("#savePnrData input[name=DESTINATION_NATION]").val(),
		}),
		pnr:$("#savePnrData input[name=PNR]").val(),
		firstName:$("#savePnrData input[name=NAME]").val(),
		lastName:$("#savePnrData input[name=LASTNAME]").val()
	};

	performSubmit("SetMyAlitaliaGigyaFlights",gigyaData);

	// gigya.accounts.getAccountInfo({
    //     callback: function(resp) {
    //     	if(resp.errorCode == 0 && resp.status) {
    //             var currentFlights = resp.data.flights;
    //             if(currentFlights==null){
    //                 currentFlights = [];
    //             }
    //     	    var pnr = $("#savePnrData input[name=PNR]").val();
    //     	    var firstName = $("#savePnrData input[name=NAME]").val();
    //     	    var lastName = $("#savePnrData input[name=LASTNAME]").val();
    //     	    var departureDate = $("#savePnrData input[name=DEPARTURE_DATE]").val();
    //     	    var destinationNation = $("#savePnrData input[name=DESTINATION_NATION]").val();
	//
    //     	    if(pnr!="" || pnr!=null){
    //     	        var params = {};
    //                 var flight = {};
    //                 var alreadyExists = false;
	//
	//
    //                 $.each( currentFlights, function( i, flight ){
    //                     if(flight.flight_pnr == pnr){
    //                         alreadyExists = true;
    //                     }
    //                 });
	//
    //                 if(alreadyExists == false){
    //                     flight["flight_pnr"] = pnr;
    //                     flight["flight_firstName"] = firstName;
    //                     flight["flight_lastName"] = lastName;
    //                     flight["flight_departureDate"] = departureDate;
    //                     flight["flight_nationDestination"] = destinationNation;
    //                     currentFlights.push(flight);
    //                     params["flights"] = currentFlights;
	//
    //                     var par = {
    //                         callback: function(resp) {
    //                         console.log(resp);
    //                         },
    //                         data: params
    //                     };
	//
    //                     gigya.accounts.setAccountInfo(par);
    //                 }
    //     	    }
    //         }
	//
    //     }
  	// });

	typViewStatistic();
	refreshCarnetSession();
});

