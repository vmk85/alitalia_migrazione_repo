$(document).ready(function() {
	fillGiorni(31);
	//[GG 05/12/2017] MilleMiglia: Transizione Programma 2013-2017
	var mmtransitionprogram = jQuery("#estratto_conto_list_activities").data("mmtransitionprogram");
	fillAnni(mmtransitionprogram ? "2017" : null);
	if(mmtransitionprogram) {
	    jQuery(".millemiglia__brief__block__info .heading").append("<br /><span>2013-2017</span>");
	}

	return false;
});

$('select#giorno').on('focus', function(e) {
	var sizeGiorni = $('select#giorno').children().size();
	var mese = $('select#mese').find(":selected").val();
	var anno = $('select#anno').find(":selected").val();
	var mesi31 = [ '01', '03', '05', '07', '08', '10', '12' ];
	var mesi30 = [ '04', '06', '09', '11' ];

	if (jQuery.inArray(mese, mesi31) > -1 && sizeGiorni < 31) {
		fillGiorni(31);
	} else if (jQuery.inArray(mese, mesi30) > -1
			&& (sizeGiorni > 30 || sizeGiorni <= 29)) {
		fillGiorni(30);
	}

	if (mese == '02' && (anno % 4) == 0
			&& (sizeGiorni == 28 || sizeGiorni >= 30)) {
		fillGiorni(29);
	} else if (mese == '02' && (anno % 4) > 0 && (sizeGiorni > 28)) {
		fillGiorni(28);
	}
	return false;
});

$('select#mese').on('change', refreshGiorni);
$('select#anno').on('change', refreshGiorni);

function fillGiorni(giorniMese) {
	var $giorno = $('select#giorno');
	$giorno.empty();
	for (var i = 1; i <= giorniMese; i++) {
		if (i < 10) {
			$giorno.append('<option value="0' + i + '">0' + i + '</option>');
		} else {
			$giorno.append('<option value="' + i + '">' + i + '</option>');
		}
	}
	return false;
}

function refreshGiorni() {
	var giorno = $('select#giorno').find(":selected").val();
	var mese = $('select#mese').find(":selected").val();
	var anno = $('select#anno').find(":selected").val();
	var mesi31 = [ '01', '03', '05', '07', '08', '10', '12' ];
	var mesi30 = [ '04', '06', '09', '11' ];

	if (jQuery.inArray(mese, mesi30) > -1 && giorno > 30) {
		fillGiorni(30);
	}

	if (mese == '02' && (anno % 4) == 0 && giorno > 29) {
		fillGiorni(29);
	} else if (mese == '02' && (anno % 4) > 0 && giorno > 28) {
		fillGiorni(28);
	}
	return false;
}

function fillAnni(start_anno) {
	var now = new Date();
	var anno = (typeof start_anno != "undefined" && !isNaN(parseInt(start_anno))) ? start_anno : now.getFullYear();
	var $anno = $('select#anno');
	$anno.empty();
	$anno.append('<option value="' + anno + '">' + anno + '</option>');
	$anno.append('<option value="' + (anno - 1) + '">' + (anno - 1)
			+ '</option>');
	$anno.append('<option value="' + (anno - 2) + '">' + (anno - 2)
			+ '</option>');
	$anno.append('<option value="' + (anno - 3) + '">' + (anno - 3)
			+ '</option>');
	return false;
}