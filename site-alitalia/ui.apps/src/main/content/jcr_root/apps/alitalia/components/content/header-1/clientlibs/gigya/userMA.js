var userMA = null;
var userMAData = null;
var userMAProfile = null;
var userMAUtils = {};
var userMASession = {};
var gigyaInit = [];
var timer;

var onGigyaServiceReady = function() {
    userMAUtils.init();
}

userMAUtils.init = function() {

    getUserMAInfo(function(data){
        userMA = data;
        userMAData = data.data;
        userMAProfile = data.profile;

        if(gigyaInit.length > 0) {
            for(var index in gigyaInit) gigyaInit[index]();
        }
    });

    var type = linkFromEmail();
    if(type == "reset" && !location.href.includes("resetpassword")){
        setTimeout(function() {
            $("#openResetPasswordForm").trigger("click");
            gigya.accounts.showScreenSet({
                screenSet: "MA-RegistrationLogin",
                startScreen: "gigya-reset-password-screen",
                containerID: "container",
                onAfterSubmit: function(resp) {
                    if(resp.response.errorCode == 0){
                        $("#ma-reset-password-form").find(".close-button").trigger("click");
                        $("a[data-open='success-reset']").click();
                    }
                },
                onAfterScreenLoad: manageFormReset
            });
        }, 1500);
    }
}

userMAUtils.setInfo = function(data) {
    if(data != null) {
        userMA = data;
        if(data.data != null) userMAData = data.data;
        if(data.profile != null) userMAProfile = data.profile;
    }
}

userMAUtils.needUserSettings = function() {
    if(userMAData != null && (userMAData.isStrong == null || userMAData.isStrong == "false" || userMAData.isStrong == false)) {
        timer = setInterval(function() {
            if($("#needUserSettings").attr("aria-hidden") == "true" && $("#ma-check-otp").attr("aria-hidden") == "true" && $("#success").attr("aria-hidden") == "true") {
                if((userMAData.isStrong != null && (userMAData.isStrong == "true" || userMAData.isStrong == true)) || (userMAData.isFirstAccess == null || (userMAData.isFirstAccess == "true" || userMAData.isFirstAccess == true))) clearInterval(timer);
                else $("[data-open='needUserSettings']").trigger("click");
            }
        }, 100);
    } else clearInterval(timer);
}

userMAUtils.setAccountInfoData = function(data, callback) {
    gigya.accounts.setAccountInfo({
        data: data,
        callback: function(response) {
            if(callback) callback(response);
        }
    });
}

userMAUtils.gigyaExists = function() {
    return (typeof gigya != "undefined" && gigya != null);
}