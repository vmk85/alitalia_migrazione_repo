
$(document).ready(function(){


        if ($(window).width() <= 1024) {
// In caso di larghezza inferiore a 1024 , le label venivano troncate erroneamente a 5 caratteri ...
//            $("#duration").html($("#duration").html().substring(0, 5));
//            $("#waiting").html($("#waiting").html().substring(0, 5));
        } else {

        }

    $('#cercaByFFSubmitSidebar').bind('click', cercaByFFSidebar);

});

function selectPnr(){
    enableLoaderCheckin();

    //debugger;
       $('.bntWaveCheckinPnr').off('click', selectPnr);
       $('#form-selectPnr input[name=pnr]').val($( this ).data("pnr"));

       updateDataLayer('pnr',$( this ).data("pnr"));
       updateDataLayer('origin',$( this ).data("origin_code"));
       updateDataLayer('originCity',$( this ).data("origin"));
       updateDataLayer('destination',$( this ).data("destination_code"));
       updateDataLayer('destinationCity',$( this ).data("destination"));

       var flights = "";
       $(this).parent().parent().parent().parent().find($('strong#flights')).each(function(i,el){
            if(i == 0){
                flights = $(el).text();
            }
            if(i > 0){
                flights = flights + "-" +$(el).text();
            }
       });
       updateDataLayer('flight',flights);

       var date = $(this).data('departuredate');
       var departureDate = date.split('T');
       updateDataLayer('departureDate',departureDate[0]);
       var dateSplit = departureDate[0].split("-");

       var dd = now.getDate();
       var mm = now.getMonth()+1; //January is 0!
       var yyyy = now.getFullYear();

       if(dd<10) {
           dd = '0'+dd
       }

       if(mm<10) {
           mm = '0'+mm
       }

       if(dateSplit[0] == yyyy && dateSplit[1] == mm){
            var toFlight = dateSplit[2] - dd;
       }
       updateDataLayer('daysToFlight',toFlight);

       if($("#roundtrip").length){
            updateDataLayer('roundTrip',"true");
            var Returnflight = "";
            $(this).parent().parent().parent().parent().parent().find($('.flight-info.link__toggle-wrap')).last().find($('strong#flights')).each(function(i,el){
            if(i == 0){
                Returnflight = $(el).text();
            }
            if(i > 0){
                Returnflight = Returnflight + "-" +$(el).text();
            }
       });
       updateDataLayer('Returnflight',Returnflight);
       }
       else{
            updateDataLayer('roundTrip',"false");
            updateDataLayer('Returnflight','');
       }
       $('#form-selectPnr input[name=flightitinerary]').val($( this ).data("flightitinerary"));
       $('#form-selectPnr input[name=managecheckin]').val($( this ).data("managecheckin"));
       performSubmit('checkinpnrselected','#form-selectPnr',selectPnrSuccess,selectPnrFail);
   }

   function selectPnrSuccess(data) {
       if (!data.isError) {
           window.location.href = data.successPage +"?&flight="+$('#form-selectPnr input[name=flightitinerary]').val();
       }else{
          //alert($('#form-selectPnr').data("genericerror"));
       }
       $('.bntWaveCheckinPnr').on('click', selectPnr);
   }

   function selectPnrFail() {
       //alert($('#form-selectPnr').data("genericerror"));
       $('.bntWaveCheckinPnr').on('click', selectPnr);
   }

   function invokeGenericFormService(service, method, form, done, fail, always,
                                     selector, additionalParams) {
       var actualData;
       if ($.type(form) === "string" || !form) {
           var serializedData = $(form).serialize();
           if (serializedData != "") {
               serializedData += "&";
           }
           serializedData += "_isAjax=true";
           if (additionalParams) {
               for ( var paramName in additionalParams) {
                   serializedData += "&" + paramName + "="
                       + encodeURIComponent(additionalParams[paramName]);
               }
           }
           actualData = serializedData;
       } else if ($.isPlainObject(form)) {
           actualData = {};
           if (form) {
               for (var fieldName in form) {
                   actualData[fieldName] = form[fieldName];
               }
           }
           actualData['_isAjax'] = true;
           if (additionalParams) {
               for (var paramName in additionalParams) {
                   actualData[paramName] = additionalParams[paramName];
               }
           }
       }
       return $.ajax({
           url : getServiceUrl(service, selector),
           method : method,
           data : actualData,
           context : document.body
       }).done(function(data) {
           if (done) {
               done(data);
           }
       }).fail(function() {
           if (fail) {
               fail();
           }
       }).always(function() {
           if (always) {
               always();
           }
       });
   }

   function performValidation(service, form, done, fail, always, selector) {
       var additionalParams = {
           '_action' : 'validate'
       };
       return invokeGenericFormService(service, 'POST', form, done, fail, always,
           selector, additionalParams);
   }

   function performSubmit(service, form, done, fail, always, selector) {
       return invokeGenericFormService(service, 'POST', form, done, fail, always,
           selector);
   }

   //function validation(e, service, form, done, fail, always, selector) {
   //    e.preventDefault();
   //    return performValidation(service, form, done, fail, always, selector);
   //}

   //function validationFormFile(e, service, form, done, fail, always, selector,
   //                            additionalParams) {
   //    e.preventDefault();
   //    additionalParams._action = "validate";
   //    return invokeGenericFormService(service, 'POST', form, done, fail, always,
   //        selector, additionalParams);
   //}

   function getServiceUrl(service, selector, secure) {
       if (selector == 'login') {
           return service;
       }
       selector = selector || "json";
       var protocol = location.protocol;
       if (jsonProtocol && (selector == "json" || secure)) {
           protocol = 'https:';
       }
       if(service == 'menu-millemiglia-header'){
           return protocol + '//' + location.host + internalUrl1 + "."
               + service + "." + selector;
       } else{
           return protocol + '//' + location.host + internalUrl1 + "/."
               + service + "." + selector;
       }
   }

   //function removeUrlSelector() {
   //    var url = location.pathname;
   //    url = url.substr(0, location.pathname.indexOf('.'));
   //    return location.protocol + '//' + location.host + url;
   //}

   //function getParameterByName(name) {
   //    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
   //    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex
   //        .exec(location.search);
   //    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g,
   //        " "));
   //}

   function onLoadWaveGestisciCheckin() {
       $('.bntWaveCheckinPnr').on('click', selectPnr);
       if(CQ_Analytics.ProfileDataMgr.data.isLoggedIn) $(".hinl").show();
       else $(".hinl").hide();
   }

   $( document ).ready(onLoadWaveGestisciCheckin);

   $('.checkinAllFlights').on('click', function(data){
   	var pnr = $(this).attr('data-pnr');
   	var checkinAllFlights = 'Y';
   	var flightitinerary = '0_1';
   	var managecheckin = '';
   	$.ajax({
   		url : getServiceUrl('checkinpnrselected'),
           async: false,
           method : 'POST',
           data : {pnr : pnr, checkinallflights : checkinAllFlights, flightitinerary : flightitinerary, managecheckin : managecheckin},
           context : document.body,
           success : function (data) {
               //alert("OK!!!!");
               window.location.href = data.successPage;
           },
           error : function (data) {
               //alert("E' evvenuto un errore.");
               //alert($('#form-selectPnr').data("genericerror"));
           }
           })
   });

function cercaByFFSidebar(){
    resetErrors();
    if( validationInputs('FFSidebar') ) {
        resetErrors();
        enableLoaderCheckin();
        performSubmit('searchbyfrequentflyer','#form-cercaByFFSidebar',cercaByFFSuccess,cercaByFFFail);
    }
}

function cercaByFFSuccess(data) {
    checkinStarted++;
    updateDataLayer('checkinStarted',checkinStarted);
    if (!data.isError) {
        window.location.href = data.successPage;
    }else{
        disableLoaderCheckin();
        $('#divError').show();
        $('#labelCheckinError').text(data.errorMessage);
        dataLayer[1].checkinError = data.errorMessage;
        updateDataLayer('checkinError',checkinError);
        if(typeof window["_satellite"] !== "undefined"){
        	_satellite.track("CIError");
        } else {
        	console.error("Cannot activate CIError. _satellite object not found");
        }
    }
}

function cercaByFFFail() {
    disableLoaderCheckin();
    $('#divError').show();
    $('#labelCheckinError').text('Errore (servlet FAIL) ');
}