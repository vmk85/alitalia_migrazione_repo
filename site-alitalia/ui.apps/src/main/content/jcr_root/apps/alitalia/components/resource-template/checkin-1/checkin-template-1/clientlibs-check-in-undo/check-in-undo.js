function passengerDataSubmit(e) {
	$("#confirmUndo").removeClass("isActive");
	$('#passengersSubmit').off('click', passengerDataSubmit);
	$('#undoBackHome').off('click', undoBackHome);
	$('#passengersSubmit').addClass('isDisabled');
	validation(e, 'checkinundo', '#passengersForm', passengerDataValidationSuccess, passengerDataSubmitError);
}

function passengerDataValidationSuccess(data) {
	startPageLoader(true);
	if (data.result) {
		removeErrors();
		performSubmit('checkinundo', '#passengersForm', passengerDataSubmitSuccess, passengerDataSubmitError);
		
	} else {
		startPageLoader(false);
		$('#passengersSubmit').on('click', passengerDataSubmit);
		$('#undoBackHome').on('click', undoBackHome);
		$('#passengersSubmit').removeClass('isDisabled');
		showErrors(data);
		/*Tracking errors*/
		if(typeof window["analytics_trackCheckinErrors"] == "function"){
			analytics_trackCheckinErrors(data.fields);
		}
	}
}

function passengerDataSubmitSuccess (data) {
	startPageLoader(false);
	if(data['isError']){
		$('#passengersSubmit').on('click', passengerDataSubmit);
		$('#undoBackHome').on('click', undoBackHome);
		$('#passengersSubmit').removeClass('isDisabled');
		$(".errorUndo").show();
		/*Tracking errors*/
		var errorMessage = $(".errorUndo").text().trim();
		if(typeof window["analytics_trackCheckinErrors"] == "function"){
			analytics_trackCheckinErrors(errorMessage);
		}
	}else{
		$('#undoBackHome').on('click', undoBackHome);	
		$("#confirmUndo").addClass("isActive");
		/*Track Undo ok*/
		if(typeof window["pushAnalyticsEvent"] == "function"){
			window["pushAnalyticsEvent"]("event" , "CInUndo");
		}
		if(typeof window["activateDTMRule"] == "function"){
			var DTMRule = "CInUndo";
			window["activateDTMRule"](DTMRule);
		}
		
		if(typeof window["checkIn_dcs_multitrack_undo"] == "function"){
			checkIn_dcs_multitrack_undo();
		}
	}
}

function passengerDataSubmitError() {
	startPageLoader(false);
	$('#passengersSubmit').on('click', passengerDataSubmit);
	$('#undoBackHome').on('click', undoBackHome);
	$('#passengersSubmit').removeClass('isDisabled');
	console.log('fail');

}

function checkInDone() {
	if ($('.j-isPasenger input[type="checkbox"]').length == 1) {
		$('.j-selectAllPasengers input').click();
	}
	$('#passengersSubmit').on('click', passengerDataSubmit);
	$('#undoBackHome').on('click', undoBackHome);
}

function undoBackHome () {
	startPageLoader(true);
	performSubmit('checkinbackhp', '#passengersForm', checkinBackHPSuccess, checkinBackHPError);
}

function checkinBackHPSuccess (data) {
	
	if(data.redirect){
		window.location.replace(data.redirect);
	}else{
		startPageLoader(false);
	}
}

function checkinBackHPError () {
	startPageLoader(false);
}

