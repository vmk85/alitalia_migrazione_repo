
function initPartialMmbMeal() {
}


/* modify form */

function be_addMeal(e, btn, section, callback) {
	performValidation('mmbancillarycartmeals', '#mmb-ancillary-modify-meal-form', 
			mmbAncillaryMealModifyValidationSuccess, mmbHandleInvokeError);
};

function mmbAncillaryMealModifyValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('mmbancillarycartmeals', '#mmb-ancillary-modify-meal-form', 
				mmbAncillaryMealModifySubmitSuccess, mmbHandleInvokeError);
	} else {
		showErrors(data);
	}
}

function mmbAncillaryMealModifySubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		if (data.isError) {
			mmbHandleServiceError(data);
		}
		refreshMmbPartialMeal(true, true);
	}
}