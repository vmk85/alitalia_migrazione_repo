
function initPartialCheckinSeat() {
	
	if (window['getInitialBookingSeatSelections']) {
		window.bookingSeatSelections = window['getInitialBookingSeatSelections'](); // defined by template script
		initSeatMap();
	}
	/*Upgrade*/
	$(".j-upgradeClass").fastClick(initCheckinUpgradeSeats);
	$(".j-openSeatMap").fastClick(simulateClickOnFirstPassenger);
	
}

function initSeatMap(){
	var __bookingSeat = $('.chooseSeat__container');
	if (__bookingSeat.length > 0){
		for (var j=0;j<__bookingSeat.length;j++){
			new BookingSeat($(__bookingSeat[j]));
		}
	}
	if ($('.j-chooseSeatSlider').length > 0){
		bookingSeatSlider.init();
	}
}


/* modify form */

function getBookingSeatSelectionsFormData() {
	var seatSelectionParams = [];
	for (var i = 0; i < window.bookingSeatSelections.length; i++) {
		var seatSelection = window.bookingSeatSelections[i];
		if (seatSelection.isChangeable == "true" 
					&& seatSelection.seatNumber != ""
					&& seatSelection.seatNumber != seatSelection.previousSeatNumber) {
			var ids = seatSelection.ancillaryIds;
			if(seatSelection.upgradeChoosed){
				ids = seatSelection.upgradeAncillaryIds[seatSelection.upgradeChoosed];
			}
			var isUpgradeAddition = seatSelection.upgradeChoosed ? "-" + seatSelection.upgradeChoosed +'_UPGRADE' : '';
			var seatSelectionParam = ids + "#" + seatSelection.seatNumber + isUpgradeAddition;
			seatSelectionParams.push(seatSelectionParam);
		}
	}
	var form = { 'seat-selection': seatSelectionParams };
	return form;
}

function be_chooseSeats(e, btn, section, callback) {
	performSubmit('checkinchangeseat', getBookingSeatSelectionsFormData(), 
		function(data) {
			if (data.redirect || !data.result) {
				//console.log("KO Seat");
			} else {
				for (var i = 0; i < window.bookingSeatSelections.length; i++) {
					var seatSelection = window.bookingSeatSelections[i];
					seatSelection.previousSeatNumber = seatSelection.seatNumber;
				}
			}
			refreshCheckinPartialSeat(
				function(){
					if (callback) {
						callback();
					}
				}
			);
		}, function() {
			seatMapError(callback);
	});
};

function be_cancelSeats(selector, callback){
	performSubmit('checkinremoveseats', {},
		function(data) {
			if (data.redirect || !data.result) {
				//console.log("KO Seat");
			}
			refreshCheckinPartialSeat(
				function(){
					reloadSeatmapChoose(callback);
				}
			);
			
		}, function() {
			seatMapError(callback);
		});
	
}

function seatMapError(done) {
	//console.log('fail');
	if (done) {
		done();
	}
}

function initCheckinUpgradeSeats(){
	
	var cabinClass = $(this).attr("data-cabinClass");
	
	/*Change passenger seatMap*/
	var numUpgrade = 0;
	$(this).parent(".checkinUpgradePassengerList").find("input[type='checkbox']:checked").each(function(ind, elem){
		var paxId = $(elem).attr("data-paxId");
		$($(".booking__chooseSeat--choose .chooseSeat__passengerList .chooseSeat__passengerItem")[paxId]).attr("data-cabinClass", "is"+cabinClass);
		for(var i = 0; i < window.bookingSeatSelections.length; i++){
			if(window.bookingSeatSelections[i]["passenger"] == (parseInt(paxId)+1)){
				window.bookingSeatSelections[i]["upgradeChoosed"] = cabinClass;
			}
		}
		numUpgrade = numUpgrade + 1;
	});
	
	simulateClickOnFirstPassenger();
	if(typeof window["checkin_analyticsUpgradeChoosed"] == "function"){
		checkin_analyticsUpgradeChoosed(cabinClass, numUpgrade);
	}
}

function simulateClickOnFirstPassenger(){
	$(".chooseSeat__passengerItem[data-passenger='1']").click();
}

function reloadSeatmapChoose(callback){
	refreshCheckinPartialSeatChoose(function(){
		if (callback) {
			callback();
		}
		if (window['getInitialBookingSeatSelections']) {
			window.bookingSeatSelections = window['getInitialBookingSeatSelections']();
		}
		initSeatMap();
	});
}