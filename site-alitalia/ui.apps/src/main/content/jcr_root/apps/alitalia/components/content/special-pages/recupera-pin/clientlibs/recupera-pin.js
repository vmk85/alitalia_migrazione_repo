$('#recuperapinSubmit').bind('click', recuperaPinHandler);

function recuperaPinHandler(e) {
	validation(e, 'recuperapinsubmit', '#form-recupera-pin', recuperaPinSuccess, recuperaPinError);
}

function recuperaPinSuccess(data) {
	return (data.result ? recuperaPinContinue(false) : showErrors(data));
}

function recuperaPinError() {
	return recuperaPinContinue(true);
}

function recuperaPinContinue(stop) {
	removeErrors();
	$('#recuperapinSubmit').unbind('click', recuperaPinHandler);
	return stop || $('#recuperapinSubmit').click();
}