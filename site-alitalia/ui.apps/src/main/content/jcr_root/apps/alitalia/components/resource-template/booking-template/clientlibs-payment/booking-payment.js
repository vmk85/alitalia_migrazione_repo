/*
 * JS per tooltip
 */

$("[data-tooltip]").each(function() {
    var a = $(this).attr("data-tooltipAuto"), b = "mouseenter focus", c = "mouseleave blur";
    Modernizr.touch && (b = "click", c = "click"), a && (b = null, c = null), $(this).qtip({
        metadata: {
            type: "attr",
            name: "qtipOpts"
        },
        content: {
            text: function d(a) {
                var d = $(this).attr("data-tooltip"), b = $("<div/>").html(d);
                return b;
            },
            title: $(this).attr("data-tooltipTitle")
        },
        style: "dark",
        position: {
            viewport: !0,
            effect: !1
        },
        show: {
            event: b
        },
        hide: {
            event: c
        },
        style: {
            tip: {
                corner: !0
            }
        }
    });
});

$(".j-radioButtonCard").on("click", function() {
	removeErrors();
	$(".j-radioButtonCard").prop("checked", false);
	$(this).closest('form')[0].reset();
	showCountriesUSA();
	$(this).prop("checked", true);

	$('#myCircuit_1').val($(this).val().toLowerCase());
	if($(this).attr("data-mdp") != null) $("#myCircuit_1").attr("data-mdp", $(this).attr("data-mdp").toLowerCase());
	else $("#myCircuit_1").attr("data-mdp", $(this).val().toLowerCase());


	$(".j-wantInvoiceContainer").hide();
	$(".bookInfoBoxBasketBtn").removeClass("isDisabled");
});
$(".j-wantInvoice").on("click", function() {
	removeErrors();

	$(".j-wantInvoiceContainer").find("input[type=text]").val("");

});
$("#booking-acquista-altri-pagamenti-form .j-radioButtonCard").on("click", function() {
	$('#labelLottomatica').hide();
	if ($(this).val() == "PayPal" || $(this).val() == "PosteID" || $(this).val() == "MasterPass" || $(this).val() == "Findomestic" || $(this).val() == "Discover" || $(this).val() == "CarteBleue" || $(this).val() == "JCB" || $(this).val() == "Yandex" || $(this).val() == "WebMoney" || $(this).val() == "Ideal") {
		$('#booking-acquista-altri-pagamenti-form input[name="tipoPagamento"]').val("BonificoOnLine");
	} else if ($(this).val() == "Lottomatica" || $(this).val() == "PosteID" || $(this).val() == "PayAtTO") {
		$('#booking-acquista-altri-pagamenti-form input[name="tipoPagamento"]').val("PagaDopo");
	}
	if ($(this).val() == "Lottomatica" || $(this).val() == "Findomestic"){
		//$(this).attr("data-target", "fieldset-3-2");
		//$("#otherPaymentsSelect").attr("data-target", "fieldset-3-2");
		//$("#booking-acquista-altri-pagamenti-form").find(".j-selectPaymentType > option[value='"+ $(this).val() + "']").attr("data-target", "fieldset-3-2")
		//$('#fieldset-3-1').hide();
		//$('#fieldset-3-2').show();
		if ($(this).val() == "Lottomatica"){
			$('.extrafields').hide();
			$('#labelLottomatica').show();
		} else {
			$('.extrafields').show();
		}
	} else {
		if($(this).val() == "PayAtTO"){
			$('#labelTicketOffice').show();
		}
		//$(this).attr("data-target", "fieldset-3-1");
		//$("#booking-acquista-altri-pagamenti-form").find(".j-selectPaymentType > option[value='"+ $(this).val() + "']").attr("data-target", "fieldset-3-1")
		//$('#fieldset-3-2').hide();
		//$('#fieldset-3-1').show();
	}
})

$("#booking-acquista-bonifico-form .j-radioButtonCard").on("click", function() {
	$('#labelSofort').hide();
	if ($(this).val() == "BancaIntesa" || $(this).val() == "Unicredit"){
		$('#labelSofort').show();
	}
})

$('input[name="tipologiaCarta"]').change(function() { 
	manageCreditCards(this);
	showFee();
});
$('select[name="tipologiaCarta"]').change(function() { 
	manageCreditCards(this); 
});
$('input[name="nomeBanca"]').change(function() { 
	hideFee();
});
$('input[name="paymentGroup"]').change(function() { 
	showFee();
});


manageCreditCards();

$('img#refreshCaptcha').on('click', refreshCaptcha);

function refreshCaptcha(){
	$('img#captchaImg').attr("src",".jcaptcha?timestamp=" + new Date().getTime());
	$('input#captcha').val('');
}

$('select[name="rate"]').change(function() { 
	var totalPrice = parseFloat($('#basketPrice').text().replace(' ','').replace('.','').replace(',','.'));
	console.log("Importo totale: "+totalPrice);
	var numRate = parseFloat($('#rate').val());
	console.log("numRate: "+numRate);
	var rataMensile = "blank";
	if (numRate!=""){
		if(isNaN(numRate)){
			rataMensile = totalPrice;
		} else {
			rataMensile = parseFloat(totalPrice/numRate).toFixed(2);
		}
	}
	console.log("rataMensile: "+rataMensile);
	$('#importo-rata').val(rataMensile);
});

$('#paeseFattura-1').change(function() { manageInvoice(this, 1); } );
$('#paeseFattura-2').change(function() { manageInvoice(this, 2); } );
$('#paeseFattura-3').change(function() { manageInvoice(this, 3); } );

$('#booking-acquista-cdc-form input[name="tipoIntestatario"]').on('change', 
		function() { 
	$('#booking-acquista-cdc-form .j-wantInvoiceContainer').find("input[type=text]").val("");

	manageBilling(this, 1); } );
$('#booking-acquista-bonifico-form input[name="tipoIntestatario"]').on('change', 
		function() { 
	$('#booking-acquista-bonifico-form .j-wantInvoiceContainer').find("input[type=text]").val("");
	manageBilling(this, 2); } );
$('#booking-acquista-altri-pagamenti-form input[name="tipoIntestatario"]').on('change', 
		function() { 
	$('#booking-acquista-altri-pagamenti-form .j-wantInvoiceContainer').find("input[type=text]").val("");

	manageBilling(this, 3); } );

manageInvoice();
manageBilling();

$('.bookInfoBoxBasketBtn').on('click', acquistaDaCarrello);

function acquistaDaCarrello(e) {
	if (!$(this).hasClass("isDisabled") && (metodoPagamento = $(".j-radioButtonCard:checked")).length == 1) {
		$('.bookInfoBoxBasketBtn').off("click", acquistaDaCarrello);
		var form = metodoPagamento.closest("form");
		if (form.is("#booking-acquista-cdc-form")) {
			acquistaCDCSubmit(e);
		} else if (form.is("#booking-acquista-br-credit-card-form")) {
			acquistabrCDCSubmit(e);
		} else if (form.is("#booking-acquista-bonifico-form")) {
			acquistaBonificoSubmit(e);
		} else if (form.is("#booking-acquista-altri-pagamenti-form")) {
			acquistaAltriPagamentiSubmit(e);
		} else if (form.is("#booking-acquista-oneclick-form")) {
			acquistaOneClickSubmit(e);
		} else if (form.is("#booking-acquista-zeropayment-form")) {
			acquistaZeroPaymentSubmit(e);
		}
	} else {
		if ($("#booking-acquista-zeropayment-form").length == 1) {
			acquistaZeroPaymentSubmit(e);
		}
	}
}

$("#booking-acquista-oneclick-submit").on("click", acquistaOneClickSubmit);

function acquistaOneClickSubmit(e) {
	$("#booking-acquista-oneclick-submit").off("click", acquistaOneClickSubmit);
	validation(e, "bookingpurchasedataconsumer", "#booking-acquista-oneclick-form", function(data) { 
		donePurchaseData(data, "booking-acquista-oneclick-form");
	}, failValidationBookingPurchase);
}

$("#booking-acquista-cdc-submit").on("click", acquistaCDCSubmit);

function acquistaCDCSubmit(e) {
	$("#booking-acquista-cdc-submit").off("click", acquistaCDCSubmit);
	validation(e, "bookingpurchasedataconsumer", "#booking-acquista-cdc-form", function(data) { 
		donePurchaseData(data, "booking-acquista-cdc-form");
	}, failValidationBookingPurchase);
}

$("#booking-acquista-br-cdc-submit").on("click", acquistabrCDCSubmit);

function acquistabrCDCSubmit(e) {
	$("#booking-acquista-br-cdc-submit").off("click", acquistabrCDCSubmit);
	validation(e, "bookingpurchasedataconsumer", "#booking-acquista-br-credit-card-form", function(data) { 
		donePurchaseData(data, "booking-acquista-br-credit-card-form");
	}, failValidationBookingPurchase);
}

$("#booking-acquista-bonifico-submit").on("click", acquistaBonificoSubmit);

function acquistaBonificoSubmit(e) {
	$("#booking-acquista-bonifico-submit").off("click", acquistaBonificoSubmit);
	validation(e, "bookingpurchasedataconsumer", "#booking-acquista-bonifico-form", function(data) {
		donePurchaseData(data, "booking-acquista-bonifico-form");
	}, failValidationBookingPurchase);
}

$("#booking-acquista-altri-pagamenti-submit").on("click", acquistaAltriPagamentiSubmit);
$("#booking-acquista-altri-pagamenti-submit-2").on("click", acquistaAltriPagamentiSubmit2);

function acquistaAltriPagamentiSubmit(e) {
	$("#booking-acquista-altri-pagamenti-submit").off("click", acquistaAltriPagamentiSubmit);
	validation(e, "bookingpurchasedataconsumer", "#booking-acquista-altri-pagamenti-form", function(data) {
		donePurchaseData(data, "booking-acquista-altri-pagamenti-form");
	}, failValidationBookingPurchase);
}

function acquistaAltriPagamentiSubmit2(e) {
	$("div.form_captcha_input").addClass("form__inputCover");
	$("#booking-acquista-altri-pagamenti-submit-2").off("click", acquistaAltriPagamentiSubmit2);
	validation(e, "bookingpurchasedataconsumer", "#booking-acquista-altri-pagamenti-form", function(data) {
		donePurchaseData(data, "booking-acquista-altri-pagamenti-form");
	}, failValidationBookingPurchase);
}

$("#booking-acquista-zeropayment-submit").on("click", acquistaZeroPaymentSubmit);

function acquistaZeroPaymentSubmit(e) {
	$("#booking-acquista-zeropayment-submit").off("click", acquistaZeroPaymentSubmit);
	validation(e, "bookingpurchasedataconsumer", "#booking-acquista-zeropayment-form", function(data) {
		donePurchaseData(data, "booking-acquista-zeropayment-form");
	}, failValidationBookingPurchase);
}

function failValidationBookingPurchase(){
	window.location.replace($("#redirectFailureValidationPage").val());
}

function donePurchaseData(data, selector) {
	removeErrors();
	$("#genericErrorMessageDiv").hide();
	if (data.result) {
		doPayment(selector);
	} else {
		refreshCaptcha();
		showErrors(data);
		/*analytics track errors*/
		if(window["analytics_trackBookingErrors"] && typeof window["analytics_trackBookingErrors"] === "function"){
			window["analytics_trackBookingErrors"](data.fields, "payment");
		}
		else{
			console.error("function analytics_trackBookingErrors not found");
		}
		riattivaForm(selector);
	}	
}

function riattivaForm(selector){
	console.log('Riattivazione form');
	if (selector == "booking-acquista-bonifico-form") {
		$("#booking-acquista-bonifico-submit").on("click", acquistaBonificoSubmit);
	} else if (selector == "booking-acquista-altri-pagamenti-form") {
		$("#booking-acquista-altri-pagamenti-submit").on("click", acquistaAltriPagamentiSubmit);
		$("#booking-acquista-altri-pagamenti-submit-2").on("click", acquistaAltriPagamentiSubmit2);
	} else if (selector == "booking-acquista-cdc-form") {
		$("#booking-acquista-cdc-submit").on("click", acquistaCDCSubmit);
	} else if (selector == "booking-acquista-br-credit-card-form") {
		$("#booking-acquista-br-cdc-submit").on("click", acquistabrCDCSubmit);
	} else if (selector == "booking-acquista-oneclick-form") {
		$("#booking-acquista-oneclick-submit").on("click", acquistaOneClickSubmit);
	} else if (selector == "booking-acquista-zeropayment-form") {
		$("#booking-acquista-zeropayment-submit").on("click", acquistaZeroPaymentSubmit);
	}
}

function doPayment(selector) {
	// start waiting area
	resultsLoading.init();
	beirut(
			function (beirutString) {
				doPayment2(selector, beirutString);
			});
	
}

function doPayment2(selector, beirutString){
	$('#' + selector + " input[name='beirutString']").val(beirutString);
	performSubmit('bookingpurchasedataconsumer', '#' + selector, function(data) {
		doneDoPayment(data, selector);
	}, failDoPayment);
}

function doneDoPayment(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
	} else if (!data.result) {
		if (data.message) {
			$("#genericErrorMessageDiv .genericErrorMessage__text").text(data.message);
			$("#genericErrorMessageDiv").show();
			/*analytics track errors*/
			if(window["analytics_trackBookingErrors"] && typeof window["analytics_trackBookingErrors"] === "function"){
				window["analytics_trackBookingErrors"](data.message, "payment");
			}
			else{
				console.error("function analytics_trackBookingErrors not found");
			}
		}
		hideWaiting(data);
	} else if (data.complete) {
		doCompletePayment(data.complete);
	} else {
		hideWaiting(data);
	}
}

function doneDoPayment(data, selector) {
	if (data.redirect) {
		window.location.replace(data.redirect);
	} else if (!data.result) {
		riattivaForm(selector);
		if (data.message) {
			$("#genericErrorMessageDiv .genericErrorMessage__text").text(data.message);
			$("#genericErrorMessageDiv").show();
			/*analytics track errors*/
			if(window["analytics_trackBookingErrors"] && typeof window["analytics_trackBookingErrors"] === "function"){
				window["analytics_trackBookingErrors"](data.message, "payment");
			}
			else{
				console.error("function analytics_trackBookingErrors not found");
			}
		}
		hideWaiting(data);
	} else if (data.complete) {
		doCompletePayment(data.complete);
	} else {
		hideWaiting(data);
	}
}

function doCompletePayment(data) {
	if (data.method == 'GET'){
		window.location.replace(data.redirect);
	} else {
		var newForm = $('<form>', {
	        'action': data.redirect,
	        'method': data.method
	    })
	    for (var key in data.params) {
	        if (data.params.hasOwnProperty(key)) {
	        	newForm.append($('<input>', {
	    	        'name': key,
	    	        'value': data.params[key],
	    	        'type': 'hidden'
	    	    }));
	         }
	    }
		newForm.append($('<input>', {
	        'name': 'submitForm',
	        'value': 'Click me',
	        'type': 'submit'
	    }));
		newForm.appendTo("body");
	    newForm.submit();
	}
}

function failDoPayment() {
	window.location.replace($("#redirectFailurePage").val());
}

function hideWaiting(data) {
	$('.waitingPage').hide();
}

function manageCreditCards(e) {
	if (e && $(e) && $(e).val() && $(e).val() == 'AmericanExpress') {
		$('div.cvc input#cvc').attr('maxlength', 4);
		$('.extrafields').show();
		$('.hide-uatp').show();
	} else if (e && $(e) && $(e).val() && $(e).val() == 'Uatp') {
		$('.extrafields').hide();
		$('.hide-uatp').hide();
	} else {
		$('div.cvc input#cvc').attr('maxlength', 3);
		$('.extrafields').hide();
		$('.hide-uatp').show();
	}
	if ($('#myCircuit').val() == 'AmericanExpress') {
		$('#cvc0').attr('maxlength', 4);
	}
	// Gestione Maestro tra le CDC
	var market =document.getElementById("marketString");
	if (e && $(e) && $(e).val() && $(e).val() == 'Maestro' && market.value != 'it') {
		$('#booking-acquista-cdc-form input[name="tipoPagamento"]').val("BonificoOnLine");
		$('.cdc-fields').hide();
	} else {
		$('#booking-acquista-cdc-form input[name="tipoPagamento"]').val("CDC");
		$('.cdc-fields').show();
	}
}

function manageInvoice(e, i) {
	if (e && $(e) && $(e).val() && $(e).val() == 'IT') {
		$('.extrafield-' + i).show();
	} else {
		if (i) {
			$('.extrafield-' + i).hide();
		} else {
			$('.extrafield').hide();
		}
	}
}

function manageBilling(e, i) {
	removeErrors();
	if (e && $(e) && $(e).val() && $(e).val() == 'PF') {
		$('.intFattura' + i).hide();
	} else {
		$('.intFattura' + i).show();
	}
}

function showCountriesUSA(){
	var elem = $("#paese option:selected").val();
	if(elem == "US") {
		$("#extraFieldCountriesUSA").show();
	} else {
		$("select[name='countriesUSA']").val("");
		$("#extraFieldCountriesUSA").hide(); 
	}
}

function insertTotalPrice(){
	var totalPrice = $('#basketPrice-text').text();
	$('#priceToPay').append(totalPrice);
	var feePrice = $('#basketPriceFee-text').text();
	$('#priceToPayFee').append(feePrice);
}

jQuery(document).ready(function(){

	$('#numeroCarta').validateCreditCard(function(result) {
        var strTest=$('#myCircuit_1').attr("data-mdp");

        var strCC=(result.card_type == null ? 'error' : result.card_type.name);

        var isValid=(result.valid && result.length_valid && result.luhn_valid && (strCC==strTest));
        //$('.log').html((isValid ? '&nbsp;' : 'Errore'));

        if (isValid) {
            $('#numeroCarta').css('background-image','url("/content/dam/alitalia/mc_ok.png")');
            $('#numeroCarta').css('background-position','right top');
            $('#numeroCarta').css('background-repeat','no-repeat');
            $('#isNumberValid').val("1");
        }else{
            $('#numeroCarta').css('background-image','url("/content/dam/alitalia/mc_ok_bw.png")');
            $('#numeroCarta').css('background-position','right top');
            $('#numeroCarta').css('background-repeat','no-repeat');
            $('#isNumberValid').val("a");
        }

    });

	if($("#genericErrorMessageDiv p").text().trim().length > 0 ){
		if(window["analytics_trackBookingErrors"] && typeof window["analytics_trackBookingErrors"] === "function"){
			window["analytics_trackBookingErrors"]($("#genericErrorMessageDiv p").text(), "payment");
		}
		else{
			console.error("function analytics_trackBookingErrors not found");
		}
	}
	initForms();
});

function showFee() {
	$('#fee-label').show();
	$('#fee-price').show();
	$('#total-price').hide();
	$('#total-price-fee').show();
	$('#basketPrice-text').hide();
	$('#basketPriceFee-text').show();
	$('#priceToPay').hide();
	$('#priceToPayFee').show();
}

function hideFee() {
	$('#fee-label').hide();
	$('#fee-price').hide();
	$('#total-price').show();
	$('#total-price-fee').hide();
	$('#basketPrice-text').show();
	$('#basketPriceFee-text').hide();
	$('#priceToPay').show();
	$('#priceToPayFee').hide();
	
}

