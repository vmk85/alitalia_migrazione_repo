/* event handlers */

/**
 * Document ready event handler, setup UI bindings
 */
function onHeaderLoginDocumentReady(e) {
	prepareHeaderLoginSocial();
	invokeShowCaptcha("captcha_header", "1");
	// $("#headerButtonLogin").unbind('click', onHeaderButtonLoginClick);
	// $("#headerButtonLogin").bind('click', onHeaderButtonLoginClick);
	$("#headerButtonLogout").unbind('click', onHeaderButtonLogoutClick);
	$("#headerButtonLogout").bind('click', onHeaderButtonLogoutClick);
	/* Strong Auth - Begin */
    $("#headerButtonLogin-sa").unbind('click', onHeaderButtonSALoginClick);
    $("#headerButtonLogin-sa").bind('click', onHeaderButtonSALoginClick);
	/* Strong Auth - End */
}

/**
 * Event handler for header login button click, start login process with MilleMiglia credentials.
 */
function onHeaderButtonLoginClick(e) {
	removeErrors();
	$("#form-mm-code").val($("#header_code").val());
	$("#form-mm-pin").val($("#header_pin").val());
	performValidation('millemiglialoginvalidation', '#form-millemiglia-login-1',
			milleMigliaHeaderLoginValidationSuccess, milleMigliaHeaderLoginValidationFailure);
	return false;
}

/**
 * Event handler for header logout button click, start logout process.
 */
function onHeaderButtonLogoutClick(e) {
	startLogout();
	return false;
}


/* internal methods */

function prepareHeaderLoginSocial(div) {
	if (window['gigya']) {
		window['gigya'].socialize.showLoginUI({
			  versione: 2
			, height: 30
			, width: '100%'
			, buttonsStyle: 'standard'
			, showTermsLink: false
			, hideGigyaLink: true
			, showWhatsThis: false
			, containerID: div || 'headerLoginDiv'
			, context: { 
				action: 'startSocialLogin',
				unexpectedErrorCallback: headerLoginUnexpectedErrorCallback,
				loginFailureCallback: headerLoginFailureCallback
			}
			, onLoad: function(event) {
				$("#headerLoginDiv").find("center").contents().unwrap();
				$("#checkinCheckinLoginDiv").find("center").contents().unwrap();
				$("#checkinMyFlightLoginDiv").find("center").contents().unwrap();
				$("#headerLoginDiv").find('table').each(addCaptionHeaderUtente);
				$("#checkinCheckinLoginDiv").find('table').each(addCaptionHeaderUtente);
				$("#checkinMyFlightLoginDiv").find('table').each(addCaptionHeaderUtente);
			}
			, cid: ''
		});
	}
}

function addCaptionHeaderUtente(index, elem) {
	if ($(elem).find('caption').length == 0) {
		$(elem).prepend('<caption class="hidden">Social Account</caption>');
	}
}


/* callbacks */


/**
 * Callback for successful loginSA form validation call.
 */
function milleMigliaHeaderLoginSALValidationSuccess(data) {

	var userName = $("#header_code").val();
	var password = $("#header_pin").val();

    if (data.result) {

        var rememberMe = "0";
        if ($("#header_rememberme").is(":checked")) {
            rememberMe = "1";
        }
        startLoginSAWithMilleMigliaCredentials(userName, password, rememberMe,
            headerLoginUnexpectedErrorCallback, headerLoginSAFailureCallback,null,loginUserLockedErrorCallback,true);
    } else {
        if (typeof refreshReCaptchaLogin !== 'undefined') {
            refreshReCaptchaLogin(headerRecaptchaID);
        }
        showErrors(data);
    }
}


/**
 * Callback for successful loginSA form validation call.
 */
function milleMigliaHeaderLoginSAValidationFailure(data) {
    if (typeof refreshReCaptchaLogin !== 'undefined') {
        refreshReCaptchaLogin(headerRecaptchaID);
    }
    headerLoginUnexpectedErrorCallback();
}


/**
 * Callback for failed login form validation call.
 */
function milleMigliaHeaderLoginValidationSuccess(data) {
	if (data.result) {
		var mmCode = $('#form-mm-code').val();
		var mmPin = $('#form-mm-pin').val();
		var rememberMe = "0";
		if ($("#header_rememberme").is(":checked")) {
			rememberMe = "1";
		}
		startLoginWithMilleMigliaCredentials(mmCode, mmPin, rememberMe, 
				headerLoginUnexpectedErrorCallback, headerLoginFailureCallback,null,loginUserLockedErrorCallback,true);
	} else {
		if (typeof refreshReCaptchaLogin !== 'undefined') {
			refreshReCaptchaLogin(headerRecaptchaID);
		}
		showErrors(data);
	}
}

function loginUserLockedErrorCallback(){
	$('.j-captchaUserLocked.j-hideOnCLick').velocity('transition.fadeIn');
}

/**
 * Callback for failed login form validation call.
 */
function milleMigliaHeaderLoginValidationFailure() {
	if (typeof refreshReCaptchaLogin !== 'undefined') {
		refreshReCaptchaLogin(headerRecaptchaID);
	}
	headerLoginUnexpectedErrorCallback();
}

/**
 * Callback passed to the login functions to receive event about
 * unexpected login error.
 */
function headerLoginUnexpectedErrorCallback() {
	$('.j-loginError.j-hideOnCLick').velocity('transition.fadeIn');
}

/**
 * Callback passed to the login functions to receive event about
 * unexpected login error.
 */
function headerLoginFailureCallback(errorCode, errorDescription) {
	$('.j-loginError.j-hideOnCLick').velocity('transition.fadeIn');
}


/**
 * Callback passed to the login functions to receive event about
 * unexpected login error.
 */
function headerLoginSAFailureCallback(errorCode, errorDescription) {
    $('.j-loginError.j-hideOnCLick').velocity('transition.fadeIn');
}

/* Strong Auth - Begin */
function onHeaderButtonSALoginClick(e) {
    removeErrors();
    console.log('onHeaderButtonSALoginClick - Begin');
    /* - Modifiche Strong Auht  - Start */
    // $("#form-mm-code").val($("#header-code-sa").val());
    // $("#form-mm-pin").val($("#header-password-sa").val());
    // $("#header_code").val($("#header-code-sa").val());
    $("#form-mm-code").val($("#header_code").val());
    $("#form-mm-pin").val($("#header_pin").val());

    performValidation('millemiglialoginsavalidation', '#form-millemiglia-login-1',
        milleMigliaHeaderLoginSALValidationSuccess, milleMigliaHeaderLoginSAValidationFailure);
    return false;
}

function checkCredenzialiMillemigliaHeaderSuccess(data){
    if (data) {
        if (!data.successful){
            setPassError(CQ.I18n.get('header.millemiglia.invalidCredential'));
        }
        else {
            dataResponse = data;
            $("#header_code").val(parseInt(data.mmCode).toString());
            $("#header_pin").val(data.mmPin);

            //// captcha
            // $("#form-mm-code").val(dataResponse.mmCode);
            // $("#form-mm-pin").val(dataResponse.mmPin);
            // performValidation('millemiglialoginvalidation', '#form-millemiglia-login-1',
            //     milleMigliaHeaderLoginValidationSuccess, milleMigliaHeaderLoginValidationFailure);
            onHeaderButtonLoginClick();
            // dataResponse.result = data.successful;
            // milleMigliaHeaderLoginValidationSuccess(dataResponse);
            // successCheckMilleMigliaLogin(dataResponse, true, null);

        }
    }
}
function checkCredenzialiMillemigliaHeaderFailure(data){
	console.log('checkCredenzialiMillemigliaHeaderFailure');
}
function checkCredenzialiMillemigliaHeaderAlways(data){

}

/* Strong Auth - End */

/* code execution */

// setup document ready listener to initialize UI binding events
// $(document).ready(onHeaderLoginDocumentReady);
