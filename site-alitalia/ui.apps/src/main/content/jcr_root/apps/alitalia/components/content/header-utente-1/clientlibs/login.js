

/* global */

/**
 * Keeps info about current status of login process.
 */
var alitaliaLoginData = {
    busy: false,
    rememberMe: '0',
    targetPage: null,
    unexpectedErrorCallback: null,
    loginFailureCallback: null,
    loginUserLockedCallback: null
};

var SSW_REDEMPTION_REDIRECT_PAR_NAME = "SSW_RED";
var SSW_REDEMPTION_PARAMETERS = "SSW_RED_PARAMS";

/* main functions */

/*SA */
var saUpdateDone = true;
/*SA */

/**
 * Initial function externally invoked when a login process should starts
 * with MilleMiglia credentials.
 */
function startLoginWithMilleMigliaCredentials(mmCode, mmPin, rememberMe,
                                              unexpectedErrorCallback, loginFailureCallback, targetPage, loginUserLockedCallback, isHomePage, selector) {
    if (alitaliaLoginData.busy) {
        console.error("A login/logout attempt is already in progres");
        return false;
    }
    toggleLoginOverlay(true);
    alitaliaLoginData.busy = true;
    alitaliaLoginData.rememberMe = (rememberMe == true ? '1' : '0');
    if (targetPage) {
        alitaliaLoginData.targetPage = targetPage;
    }
    alitaliaLoginData.unexpectedErrorCallback = unexpectedErrorCallback;
    alitaliaLoginData.loginFailureCallback = loginFailureCallback;
    alitaliaLoginData.loginUserLockedCallback = loginUserLockedCallback
    /*performMilleMigliaLogin(mmCode, mmPin,
            alitaliaLoginData.rememberMe, alitaliaLoginData.targetPage);*/
    checkMilleMigliaLogin(mmCode, mmPin, isHomePage, selector);
    return true;
}

/**
 * Initial function invoked when a login process starts with Gigya credentials.
 */
function startLoginWithGigyaCredentials(gigyaId, gigyaSignature, gigyaProvider,
                                        gigyaUserFirstName, gigyaUserLastName, gigyaUserThumbnailURL, rememberMe,
                                        unexpectedErrorCallback, loginFailureCallback, targetPage) {
    if (alitaliaLoginData.busy) {
        console.error("A login/logout attempt is already in progres");
        return false;
    }
    toggleLoginOverlay(true);
    alitaliaLoginData.busy = true;
    alitaliaLoginData.rememberMe = false;
    if (targetPage) {
        alitaliaLoginData.targetPage = targetPage;
    }
    alitaliaLoginData.unexpectedErrorCallback = unexpectedErrorCallback;
    alitaliaLoginData.loginFailureCallback = loginFailureCallback;
    checkSocialLogin(gigyaId, gigyaSignature, gigyaProvider,
        gigyaUserFirstName, gigyaUserLastName, gigyaUserThumbnailURL,
        targetPage);
    return true;
}

/**
 * Function invoked to continue a social login with the association between
 * a Gigya id and MilleMiglia credentials.
 */
function continueSocialLoginWithMilleMigliaCredentials(gigyaId, gigyaSignature,
                                                       mmCode, mmPin, unexpectedErrorCallback, loginFailureCallback) {
    $("#form-set-social-account input[name=gigyaId]").val(gigyaId);
    $("#form-set-social-account input[name=gigyaSignature]").val(gigyaSignature);
    $("#form-set-social-account input[name=mmCode]").val(mmCode);
    $("#form-set-social-account input[name=mmPin]").val(mmPin);
    if (alitaliaLoginData.busy) {
        console.error("A login/logout attempt is already in progres");
        return false;
    }
    toggleLoginOverlay(true);
    alitaliaLoginData.busy = true;
    alitaliaLoginData.rememberMe = false;
    alitaliaLoginData.unexpectedErrorCallback = unexpectedErrorCallback;
    alitaliaLoginData.loginFailureCallback = loginFailureCallback;
    performSubmit("setsocialaccount", "#form-set-social-account",
        successSetSocialAccount, failureSetSocialAccount);
}

/**
 * Function invoked to start a logout process.
 */
function startLogout() {
    if (alitaliaLoginData.busy) {
        console.error("A login/logout attempt is already in progress.");
        return false;
    }
    toggleLoginOverlay(true);
    alitaliaLoginData.busy = true;
    alitaliaLoginData.rememberMe = false;
    alitaliaLoginData.unexpectedErrorCallback = null;
    alitaliaLoginData.loginFailureCallback = null;
    // first step, try to social logout
    performSocialLogout();
}


/* overlay management */

function toggleLoginOverlay(active) {
    if (active) {
//		var overlayLoading = document.createElement("div");
//		$(overlayLoading).attr("id", "loginOverlayLoading");
//		$(overlayLoading).attr("class", "overlayLoading");
//		$(".mainWrapper").append(overlayLoading);
        $("#login-submit").addClass("disabled");
        $('.reveal__inner .loader').css('display', 'block');
    } else {
        $("#login-submit").removeClass("disabled");
        $('.reveal__inner .loader').css('display', 'none');
    }
}


/* internal check millemiglia login functions */

/**
 * Start preliminary verification of MilleMiglia credentials using a REST servlet.
 */
function checkMilleMigliaLogin(mmCode, mmPin, isHomePage, selector) {
    $("#form-check-millemiglia-login input[name=mmCode]").val(mmCode);
    $("#form-check-millemiglia-login input[name=mmPin]").val(mmPin);
    performSubmit("checkmillemiglialogin", "#form-check-millemiglia-login",
        function(data) {
                successCheckMilleMigliaLogin(data,isHomePage,selector);
        }
        ,failureCheckMilleMigliaLogin
    );
}

/**
 * Callback for verification of preliminary MilleMiglia credentials check.
 */
function successCheckMilleMigliaLogin(data, isHomePage, selector) {
    if (data.successful == true) {
        // credentials check passed, continue with AEM login
        if (typeof data.saUpdateDone != "undefined") {
        	/*DA DECOMMENTARE QUANDO VIENE RIMESSO ADEGUAMENTO*/
            //saUpdateDone = data.saUpdateDone;
            /*DA COMMENTARE QUANDO VIENE RIMESSO ADEGUAMENTO*/
            document.cookie = "saUpdateDone="+saUpdateDone;
            document.cookie = "redirectMMOld=false";
        }

        //per forzare redirect a aggiorna profilo (in caso di credenziali non adeguate)
        //saUpdateDone = false;

        if(window.location.href.indexOf("millemiglia-login")>-1 && typeof($("#form-perform-login input[name=resource]"))!='undefined') {
        	alitaliaLoginData.targetPage=$("#form-perform-login input[name=resource]").val();
        }
        performMilleMigliaLogin(data.mmCode, data.mmPin,
            alitaliaLoginData.rememberMe, alitaliaLoginData.targetPage);
    } else {
        if (data.isLocked) {
            //SHOW LOCKED MESSAGE
            displayLoginUserLockedError();
        } else {
            if (!data.errorDescription) {
                // credentials check not passed, show user error message
                displayLoginFailureError(data.errorCode, data.errorDescription);
            }else{
                headerLoginFailureShowError(data.errorDescription);
            }

        }
    }
}

function headerLoginFailureShowError(errorDescription) {
    grecaptcha.reset();
    alitaliaLoginData.busy = false;
    toggleLoginOverlay(false);
    $('#header-wave-login-errors').html(errorDescription);
    $('#header-wave-login-errors').html(CQ.I18n.get('header.socialLogin.error'));
    $('#header-wave-login-errors-SA').html(errorDescription);
    $('#header-wave-login-errors').css("display","block");
    $('#header-wave-login-errors-SA').css("display","block");
    $('.reveal__inner .loader').css('display', 'none');
//	$('.j-loginError.j-hideOnCLick').velocity('transition.fadeIn');
}

function showCaptchaLogin(enabled,isHomePage,selector) {
    if (isHomePage) {
//		if ($("#captcha_checkin").length > 0) {
//			invokeShowCaptcha("captcha_checkin",enabled);
//		}
//		if ($("#captcha_mmb").length > 0) {
//			invokeShowCaptcha("captcha_mmb",enabled);
//		}
        if ($("#captcha_header").length > 0) {
            invokeShowCaptcha("captcha_header",enabled);
        }
        if ($("#captcha_login_spacialpages").length > 0) {
            invokeShowCaptcha("captcha_login_spacialpages",enabled);
        }
//		if ($("#captcha_cash_miles").length > 0) {
//			invokeShowCaptcha("captcha_cash_miles",enabled);
//		}

    } else {
        invokeShowCaptcha(selector,enabled);
    }
}


/**
 * Callback for failures trying to verify MilleMiglia credentials.
 */
function failureCheckMilleMigliaLogin() {
    console.error("Failed ajax call - check millemiglia login");
    displayGenericLoginFailureError();
}


/* internal check social login functions */

/**
 * Start preliminary verification of Gigya identifier using a REST servlet.
 */
function checkSocialLogin(gigyaId, gigyaSignature, gigyaProvider,
                          gigyaUserFirstName, gigyaUserLastName, gigyaUserThumbnailURL) {
    $("#form-check-social-login input[name=gigyaId]").val(gigyaId);
    $("#form-check-social-login input[name=gigyaSignature]").val(gigyaSignature);
    $("#form-check-social-login input[name=gigyaProvider]").val(gigyaProvider);
    $("#form-check-social-login input[name=gigyaUserFirstName]").val(gigyaUserFirstName);
    $("#form-check-social-login input[name=gigyaUserLastName]").val(gigyaUserLastName);
    $("#form-check-social-login input[name=gigyaUserThumbnailURL]").val(gigyaUserThumbnailURL);
    performSubmit("checksociallogin", "#form-check-social-login",
        successCheckSocialLogin, failureCheckSocialLogin);
}

/**
 * Callback for verification of Gigya identifier.
 */
function successCheckSocialLogin(data) {
    if (data.successful == true) {
        if (data.mmCode && data.mmCode != "" && data.mmCode != null) {
            // MilleMiglia user found, continue with AEM login
            performMilleMigliaLogin(data.mmCode, data.mmPin,
                alitaliaLoginData.rememberMe, alitaliaLoginData.targetPage);
        } else {
            // MilleMiglia user not found, navigate to account association page
            if (data.setSocialAccountRedirectUrl &&
                data.setSocialAccountRedirectUrl != null &&
                data.setSocialAccountRedirectUrl != "") {
                window.location.href = data.setSocialAccountRedirectUrl;
            } else {
                displayLoginFailureError("", "Invalid user account association page");
            }
        }
    } else {
        // credentials check not passed, show user error message
        displayLoginFailureError(data.errorCode, data.errorMessage);
    }
}

/**
 * Callback for failures trying to verify Gigya identifier.
 */
function failureCheckSocialLogin() {
    console.error("Failed ajax call - check social login");
    displayGenericLoginFailureError();
}


/* internal set social account functions */

/**
 * Callback for association of Gigya identifier to MilleMiglia credentials.
 */
function successSetSocialAccount(data) {
    if (data.successful == true) {
        performMilleMigliaLogin(data.mmCode, data.mmPin);
    } else {
        displayLoginFailureError(data.errorCode, data.errorMessage);
    }
}

/**
 * Callback for failures trying to associate Gigya id and MilleMiglia credentials.
 */
function failureSetSocialAccount() {
    console.error("Failed ajax call - set social account");
    displayGenericLoginFailureError();
}


/* internal perform login functions */

/**
 * Request actual AEM login using entered or resolved MilleMiglia credentials.
 */
function performMilleMigliaLogin(mmCode, mmPin, mmRememberMe, targetPage) {
    // clear client context session local storage when we are about to login
    clearClientContextProfile(); // (in login-context clientlibs)

	if (mmCode) {
		mmCode= mmCode.replace(/^0+/, '');
	}

    $("#form-perform-login input[name=j_username]").val("MM:" + mmCode);
    $("#form-perform-login input[name=j_password]").val(mmPin);
    $("#form-perform-login input[name=mm_remember_me]").val(mmRememberMe);

    // FIXME: sync login
    /*var resource = $("#form-perform-login input[name=resource]").val();
    if (resource == "") {
        var defaultResource;
        if (targetPage) {
            defaultResource = targetPage;
        } else {
            defaultResource = $("#form-perform-login input[name=default_resource]").val();
        }
        $("#form-perform-login input[name=resource]").val(defaultResource);
    }
    $("#form-perform-login").submit();*/

    /*
    FIXME: async login
    $("#form-perform-login input[name=j_validate]").val("true");
    performSubmit($("#form-perform-login").attr("action"),
            '#form-perform-login', performAsyncMilleMigliaLoginDone,
            displayGenericLoginFailureError, null, 'login');
    */

    // FIXME: async login if targetPage is not specified, sync login otherwhise
    var defaultResource;
    if (targetPage) {
        defaultResource = targetPage;
        $("#form-perform-login input[name=resource]").val(defaultResource);
        $("#form-perform-login").submit();
    } else{

        $("#form-perform-login input[name=j_validate]").val("true");
        performSubmit($("#form-perform-login").attr("action"),
            '#form-perform-login', performAsyncMilleMigliaLoginDone,
            displayGenericLoginFailureError, null, 'login');
    }
}

function performAsyncMilleMigliaLoginDone(data) {
    loadCQAnalytics(function() {
        if (saUpdateDone!=true){
            lockScroll();
            //enableLoaderMyAlitalia();

            /*
            $.each($('div.reveal-overlay'),function(i,o){
                if(o.innerHTML==""){
                    $(o).css('display','block');
                    $(o).addClass('empty');
                    //$(o).innerHTML("<div class='fade-circle' id='loaderMySA'></div>");
                }
            });
            */
/*
            Tolta redirect su your profile in caso di utente non adeguato
            redirectToProfile('personal-area/your-profile');
*/
        } else {
            if (window.Granite && window.Granite.csrf &&
                typeof window.Granite.csrf.refreshToken !== 'undefined' &&
                typeof window.Granite.csrf.refreshToken === 'function') {
                window.Granite.csrf.refreshToken();
            }
            executeLoginContextProfileCallbacksHistory();
            var isSSWRedirectDone = performSSWRedirectIfRequired();

            if(!isSSWRedirectDone){
                alitaliaLoginData.busy = false;
    //			if ($("#loginMenu").hasClass("isActive")) {
                $('#login.reveal .close-button').trigger("click");
                toggleLoginOverlay(false);
                if($('.binding-name').text() == ""){
                $(".user__logout").attr("onclick", "performMMLogout()");
                $('.binding-name').text(CQ_Analytics.ProfileDataMgr.data.customerName);

                // Gestione utente loggato/non loggato per vecchio e nuovo header
                // login-included-if-authenticated-mm, login-included-if-anonymous-mm = VECCHIO HEADER
                // login-included-if-authenticated, login-included-if-anonymous = NUOVO HEADER
                if($(".login-included-if-authenticated").length) {
                    $('.login-included-if-authenticated').removeClass("hide");
                    $('.login-included-if-anonymous').addClass("hide");
                } else if($(".login-included-if-authenticated-mm").length) {
                    $('.login-included-if-authenticated-mm').removeClass("hide");
                    $('.login-included-if-anonymous-mm').addClass("hide");
                    }
                }
            }
        }

    });
}

function lockScroll() {
    var scrollPosition = [
        self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
        self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
    ];
    var html = jQuery('html'); // it would make more sense to apply this to body, but IE7 won't have that
    html.data('scroll-position', scrollPosition);
    html.data('previous-overflow', html.css('overflow'));
    html.css('overflow', 'hidden');
    window.scrollTo(scrollPosition[0], scrollPosition[1]);
}

function unlockScroll() {
    var html = jQuery('html');
    var scrollPosition = html.data('scroll-position');
    html.css('overflow', html.data('previous-overflow'));
    window.scrollTo(scrollPosition[0], scrollPosition[1])
}


/* internal perform logout functions */

function performSocialLogout() {
    setLoginContextProfileProperty('userImageThumbnailStatus', '');
    setLoginContextProfileProperty('userImageThumbnailURL', '');
    if (window['gigya']) {
        // second logout step - social logout
        window['gigya'].socialize.logout({
            callback: socialLogoutCallback
        });
    } else {
        // gigya not found - skip to millemiglia logout
        performMilleMigliaLogout();
    }
}

function socialLogoutCallback() {
    performMilleMigliaLogout();
}

function performMilleMigliaLogout() {
    performSubmit("savemauser", ".mm-logout", function(resp) {console.log(resp);}, function(resp) {console.log(resp)});
    $("#form-perform-logout").submit();
}


/* internal common login functions */

/**
 * Used to display unexpected technical errors during the login process.
 */
function displayGenericLoginFailureError() {
    //console.error("Unexpected login error");
    alitaliaLoginData.busy = false;
    toggleLoginOverlay(false);
    if (alitaliaLoginData.unexpectedErrorCallback) {
        alitaliaLoginData.unexpectedErrorCallback();
    }
}

/**
 * Used to display user messages about a failed login attempt.
 */
function displayLoginFailureError(errorCode, errorMessage) {
    //console.info("Login failed: " + errorCode + ", " + errorMessage);
    alitaliaLoginData.busy = false;
    toggleLoginOverlay(false);
    if (alitaliaLoginData.loginFailureCallback) {
        alitaliaLoginData.loginFailureCallback(errorCode, errorMessage);
    }
}

function displayLoginUserLockedError() {
    alitaliaLoginData.busy = false;
    toggleLoginOverlay(false);
    if (alitaliaLoginData.loginUserLockedCallback) {
        alitaliaLoginData.loginUserLockedCallback();
    }
}


/* internal event handlers */

/**
 * Event handler on document ready to setup gigya listener.
 */
function onLoginDocumentReady(e) {
    if (window['gigya']) {
        window['gigya'].socialize.addEventHandlers({
            onLogin: onGigyaLogin,
            context: 'social'
        });
    }
    // setup a client context listener to read user image thumbnail URL
    // from local profile
    if (window['registerLoginContextProfileCallback']) {
        window['registerLoginContextProfileCallback'](
            onLoginClientContextProfileInitialized);
    } else {
        console.error("Expected function registerLoginContextProfileCallback " +
            "not defined.");
    }

    //bind click userbar logged
    $( ".user-logged" ).hover(function() {
        if( Foundation.MediaQuery.atLeast( 'large' ) )
        {
            $(".user-area.user-logged").addClass('active');
            // $( ".logged-user-bar" ).slideToggle( "slow").toggleClass('open');
        }
        else
        {
            $( ".logged-user-bar" ).removeAttr( 'style' ).addClass( 'open' ).show();
            setBodyLock();
        }
    });

     $( ".user-logged" ).click(function() {
            if( Foundation.MediaQuery.atLeast( 'large' ) )
            {
                $(".user-area.user-logged").toggleClass('active');
                // $( ".logged-user-bar" ).slideToggle( "slow").toggleClass('open');
            }
            else
            {
                $( ".logged-user-bar" ).removeAttr( 'style' ).addClass( 'open' ).show();
                setBodyLock();
            }
        });

    $(".user-logged").on("mouseleave", function() { $(this).removeClass("active"); })

    $( '.logged-user-bar .close-button' ).on( 'click', function() {
        if( $('.logged-user-bar').hasClass( 'open') ) {
            $(".user-area.user-logged").removeClass('active');
            $('.logged-user-bar').removeAttr( 'style' ).removeClass('open').hide();
            removeBodyLock();
        }
    } );

    $(window).on('changed.zf.mediaquery', function(event, name) {
        if( $('.logged-user-bar').hasClass( 'open') ) {
            $(".user-area.user-logged").removeClass('active');
            $('.logged-user-bar').removeClass('open').hide();
            removeBodyLock();
        }
    });


}

/**
 * Handler on gigya login event, triggering social login based on context data.
 */
function onGigyaLogin(gigyaData) {
    if (gigyaData.context && gigyaData.context.action == 'startSocialLogin') {
        var gigyaId = gigyaData.UID;
        var gigyaSignature = gigyaData.UIDSignature;
        var gigyaProvider = gigyaData.provider;
        var gigyaUserFirstName = gigyaData.user.firstName;
        var gigyaUserLastName = gigyaData.user.lastName;
        var gigyaUserThumbnailURL = gigyaData.user.thumbnailURL;
        startLoginWithGigyaCredentials(gigyaId, gigyaSignature, gigyaProvider,
            gigyaUserFirstName, gigyaUserLastName, gigyaUserThumbnailURL,
            false, gigyaData.context.unexpectedErrorCallback,
            gigyaData.context.loginFailureCallback);
    }
}

/**
 * Handler on client context init event.
 */
function onLoginClientContextProfileInitialized() {
    processUserImageThumbnailUrl();
}

/**
 * Invoked to process the user image thumbnail URL.
 */
function processUserImageThumbnailUrl() {
    // (login context functions defined in login-context clientlibs)
    if (isLoginContextAuthenticated()) {
        var userImageThumbnailStatus =
            getLoginContextProfileProperty('userImageThumbnailStatus');
        if (userImageThumbnailStatus == "" || userImageThumbnailStatus == null) {
            if (window['gigya']) {
                window['gigya'].socialize.getUserInfo({
                    callback: getSocialUserInfoCallback
                });
            } else {
                setLoginContextProfileProperty('userImageThumbnailStatus',
                    'unavailable');
            }
        }
    }
}

/**
 * Callback for social user info.
 */
function getSocialUserInfoCallback(response) {
    // (login context functions defined in login-context clientlibs)
    if (response.errorCode == 0 && response.user && response.user.thumbnailURL) {
        setLoginContextProfileProperty('userImageThumbnailStatus', 'success');
        setLoginContextProfileProperty('userImageThumbnailURL', response.user.thumbnailURL);
    } else {
        setLoginContextProfileProperty('userImageThumbnailStatus', 'unavailable');
        setLoginContextProfileProperty('userImageThumbnailURL', '');
    }
    applyClientContextProfile();
}

function performSSWRedirectIfRequired(){
    var sswRedemptionUrl = $("input[name='SSWRedirectLink']").val();//$("#SSWRedirectLink").val();
    var isSswRedemptionRedirect = getURLParameter(SSW_REDEMPTION_REDIRECT_PAR_NAME);
    if(isSswRedemptionRedirect === "1"){
        sswRedemptionUrl = addParameters(sswRedemptionUrl, decodeURIComponent(getURLParameter(SSW_REDEMPTION_PARAMETERS)));
        window.location.href = sswRedemptionUrl;
        return true;
    }
    return false;
}

function addParameters(url, params){
    var result = url;
    if(params){
        result = url.endsWith("?") || url.endsWith("&") ? url.substring(0, url.length -1) : url;
        result = result + (result.match(/\?/) ? "" : "?");
        result = result + (result.endsWith("?") ? "" : "&")
            + (params.startsWith("&") || params.startsWith("?")
                ? params.substring(1, params.length) : params);
    }
    return result;
}


//function performMMLogout() {}
//
//function performMALogout() {

//
//    gigya.accounts.logout();

//
//}

/* execution code */

// add listener for document ready event to setup social events listeners
$(document).ready(
    onLoginDocumentReady
).delay(5000).find(".mainmenu .is-drilldown").removeAttr("style");

