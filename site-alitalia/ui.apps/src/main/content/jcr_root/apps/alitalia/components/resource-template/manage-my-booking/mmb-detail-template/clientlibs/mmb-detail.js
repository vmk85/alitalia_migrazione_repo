$(function initMmb() {
	$(function(){
		$.ajax({
			url: removeUrlSelector() + ".main-partial.html",
			context : document.body
		})
		.done(function(data) {
			$(".manageBooking__body.mod").append(data);
			initTabs();
			initLightBox();
			initMmbManage();
			initSpecialAssistance();
			initAccordions();
			initForms();
			if (window['mmbPreselectedTab'] && window['mmbPreselectedTab'] != "") {
				$(window['mmbPreselectedTab']).trigger('click');
			}
			$(window).trigger('resize.accordion');
			$.ajax({
				url: removeUrlSelector() + ".ancillary-partial.html",
				context : document.body
			})
			.done(function(data) {
				$(".manageBooking__ancillary").replaceWith(data);
				$('.j-hideOnCLick').click(function(e){
					e.preventDefault();
					$('.j-hideOnCLick').velocity('transition.fadeOut');
				});
				$('.j-hideOnCLick').click();
				bookingReady(); // FIXME sostituire con nuova funzione book.stickyHeaderBooking() dopo fix Bitmana
				upsellingsManager.init();
				initMmbAncillaryPartials();
				initAccordions();
				analytics_mmbPartialCallback();
				initMmb_tracking();
				onMmbTrackingBoxLoaded();
				$(window).trigger('resize');
			})
			.fail(function() {
				console.error("Failure loading MMB ancillary partial.")
			});
			initMmb_tracking();
		})
		.fail(function() {
			console.error("Failure loading MMB main partial.")
		});
	});
});