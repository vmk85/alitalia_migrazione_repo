//var currentFocusDatePicker = null;
var pickerMy, pickerAt, pickerWidth;
var disabledDaysJson = null; // variabile contenente il json che arriva da O&D, di base vuota
var isValidDatePickerClick = 0;
var isValidDatePickerOnFocus = 0;

var market = "";

// funzione di esempio da chiamare una volta ottenuto il json - di fatto si tratta di caricare il json ottenuto nella variabile
// setDisabledDays({"availability":{"iataOrigine":"FCO","iataDestinazione":"LIN","mondaySummer":true,"tuesdaySummer":true,"wednesdaySummer":true,"thursdaySummer":true,"fridaySummer":false,"saturdaySummer":true,"sundaySummer":false,"mondayWinter":true,"tuesdayWinter":false,"wednesdayWinter":false,"thursdayWinter":true,"fridayWinter":true,"saturdayWinter":true,"sundayWinter":true,"startSummer":"2018-06-01T00:00:00Z","endSummer":"2018-08-31T00:00:00Z","startWinter":"2018-09-01T00:00:00Z","endWinter":"2018-05-31T00:00:00Z"},"conversationID":"43613672-91bc-47a7-ab52-78697b8234b8"});

$(document).ready(function() {

    market = $('html').attr('lang').split('-')[1];

    $.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;
    $.datepicker._updateDatepicker = function(inst) {
        $.datepicker._updateDatepicker_original(inst);
        var afterShow = this._get(inst, 'afterShow');
        if (afterShow)
            afterShow.apply((inst.input ? inst.input[0] : null));
    };

    //Add event handler when focused on an element with which has datepicker intialised
    $('.hasDatepicker').on('focus', function(e) {
        currentFocusDatePicker = $(this);
        setCalendarPickerPosition();

        setTimeout(function(){
            setCalendarPickerPosition();
        }, 1);
        $('#ui-datepicker-div').removeClass('desktop-datepicker mobile-datepicker');
        if( Foundation.MediaQuery.is( 'large' ) ) {
            $('#ui-datepicker-div').addClass('desktop-datepicker');
        }
        else {
            $('#ui-datepicker-div').addClass('mobile-datepicker');
        }
    });


    $(window).resize(function(){
        if (currHeight == prevHeight) $('#ui-datepicker-div').hide();
    });



});

function setCalendarPickerPosition() {

    if(typeof currentFocusDatePicker !== 'undefined') {
        if( Foundation.MediaQuery.is( 'large' ) ) {
            // store the element for use inside the position function
            var $target = currentFocusDatePicker.closest('.cerca-volo__content');
            var pickerMy =  'left+20 top';
            var pickerAt = 'left bottom-30';
            var pickerWidth = ($target.width() - 70) + 'px';
            // get the elements datepicker widget object and set it's position based on the target element
        } else {
            // store the element for use inside the position function
            var $target = currentFocusDatePicker.closest('.panel__content');
            var pickerMy =  'left+20 top';
            var pickerAt = 'left bottom';
            var pickerWidth = ($target.width()) + 'px';
            // get the elements datepicker widget object and set it's position based on the target element
        }

        $('#ui-datepicker-div').show();

        $target.datepicker('widget').position({
            my: pickerMy,
            at: pickerAt,
            of: $target,
            collision: 'none'
        }).css('width', pickerWidth);

    }
// invalidate return date if change radio-button
if ($('.switch-to--prenota-mobile').click()) {
	prenotaDataOptions.endDate=undefined;
   }
}

function setupCalendarPicker(obj) {
    var isRTL = $('html').attr('dir') == 'rtl';
    var firstDay = (market != "us" && market != "ca") ? 1 : 0; // controllo fake per US - impostazione primo giorno della settimana - 0 : Sunday
    if (obj.from) {
        obj.from.data('obj', obj);
        obj.from.datepicker({
            dateFormat: obj.altFormat,
            numberOfMonths: obj.numberOfMonths,
            minDate: obj.minDate ? obj.minDate : "+0d",
            maxDate: obj.maxDate ? obj.maxDate : "+364d",
            isRTL: isRTL,
            firstDay: firstDay,
            afterShow : function(inst) {
                if(Foundation.MediaQuery.atLeast( 'large' )) setTimeout(function() {$('#ui-datepicker-div').removeClass('hide'); setCalendarPickerPosition()}, 1);
                if(obj.to && obj.to.attr('disabled')) {
                    $('.ui-datepicker td').removeClass('ui-datepicker-highlight ui-checkout');
                    $('#ui-datepicker-div').addClass('ui-single-selection');
                }
                obj.curRange = $('.ui-datepicker td.ui-datepicker-highlight');
                setupYearNavigation(obj);
            },
            onSelect: function(dateText) {
                obj.startDate = $.datepicker.parseDate(obj.altFormat, dateText).getTime();
                if ($(this).attr("id") == "data-andata--prenota-mobile"){
                    var dataRitorno = $('#data-ritorno--prenota-mobile').val();
                    if (dataRitorno != '')
                    {
                        dataRitorno = dataRitorno.split("/");
                        var dataRitornoOk = dataRitorno[2]+"-"+dataRitorno[1]+"-"+dataRitorno[0];
                        if (isAfter(obj.startDate,new Date(dataRitornoOk).getTime()))
                        {
                            $("#data-ritorno--prenota-mobile").val('');
                            var label = CQ.I18n.get('preparaViaggio.dataRitorno.label');
                            $('span[data-toggle-panel="#panel-date-ritorno--prenota"]').html(label);
                        }
                    }
                }
                if (obj.to) {
                    obj.to.datepicker( "option", "minDate", dateText );
                    obj.to.trigger('change');
                    $(obj.altFieldTo).val($.datepicker.formatDate(obj.altFormat, obj.to.datepicker('getDate')));
                }
                obj.curYFrom = $.datepicker.parseDate(obj.altFormat, dateText).getFullYear();
                isValidDatePickerClick = 1;
                obj.from.trigger('change');
                $(obj.altFieldFrom).val($.datepicker.formatDate(obj.altFormat, obj.from.datepicker('getDate')));

            },
            onClose: function(dateText) {
                if (dateText) obj.startDate = $.datepicker.parseDate(obj.altFormat, dateText).getTime();
                if(Foundation.MediaQuery.atLeast( 'large' ) && obj.startDate && (!obj.endDate || obj.startDate > obj.endDate) && obj.to && !obj.to.attr('disabled') && isValidDatePickerClick) {
                    obj.to.datepicker( "show" );
                    isValidDatePickerClick = 0;
                }
                if(!obj.startDate && obj.to) {
                    obj.to.datepicker( "option", "minDate", 0 );
                }
                cercaVoloPrenotaOptions.closeAllCollapsablePanels();


            },
            beforeShow: function() {
                obj.curInput = 'from';
                obj.curY = 0;
                if(Foundation.MediaQuery.atLeast( 'large' )) $('#ui-datepicker-div').addClass('hide');
                if (obj.to) {
                    $('#ui-datepicker-div').removeClass('ui-single-selection');
                } else {
                    $('#ui-datepicker-div').addClass('ui-single-selection');
                }
            },
            beforeShowDay: function(date) {
                return beforeShowDay(date, obj.startDate, obj.endDate, obj.checkDisabledDays);
            },
            onChangeMonthYear: function(year, month, inst) {
            // salva anno
            obj.curY = year;
            setupYearNavigation(obj);
            }
        });
    }

    if (obj.to) {
        obj.to.data('obj', obj);
        obj.to.datepicker({
            dateFormat: obj.altFormat,
            numberOfMonths: obj.numberOfMonths,
            minDate: obj.minDate ? obj.minDate : "+0d",
            maxDate: obj.maxDate ? obj.maxDate : "+364d",
            isRTL: isRTL,
            firstDay: firstDay,
            afterShow : function(inst) {
                if(Foundation.MediaQuery.atLeast( 'large' )) setTimeout(function() {$('#ui-datepicker-div').removeClass('hide'); setCalendarPickerPosition()}, 1);
                obj.curRange = $('.ui-datepicker td.ui-datepicker-highlight');
                setupYearNavigation(obj);
            },
            onSelect: function(dateText) {
                obj.endDate = $.datepicker.parseDate(obj.altFormat, dateText).getTime();
                obj.curYTo = $.datepicker.parseDate(obj.altFormat, dateText).getFullYear();
            },
            onClose: function(dateText) {
                if (dateText) obj.endDate = $.datepicker.parseDate(obj.altFormat, dateText).getTime();
                if(!obj.startDate) {
                    obj.to.datepicker( "option", "minDate", 0 );
                }
                obj.to.trigger('change');
                $(obj.altFieldTo).val($.datepicker.formatDate(obj.altFormat, obj.to.datepicker('getDate')));
                // close all panel
                cercaVoloPrenotaOptions.closeAllCollapsablePanels();
                if (Foundation.MediaQuery.atLeast( 'large' ) && obj.to.closest('.cerca-volo__content--prenota').length > 0) $('.cerca-volo__content--prenota .validate-search--desktop').trigger('click');
            },
            beforeShow: function() {
                if(Foundation.MediaQuery.atLeast( 'large' )) $('#ui-datepicker-div').addClass('hide');
                obj.curInput = 'to';
                obj.curY = 0;
                if(!obj.startDate) {
                    var d = new Date();
                    obj.startDate = d.getTime();
                }

                if (obj.from && ($(obj.altFieldFrom).val() == 'not-set' || $(obj.altFieldFrom).val() == '')) {
                    obj.from.datepicker( "setDate", obj.startDate );
                    $(obj.altFieldFrom).val($.datepicker.formatDate(obj.altFormat, obj.from.datepicker('getDate')));
                    obj.from.trigger('change');
                }
                if(!Foundation.MediaQuery.atLeast( 'large' )) {
                    var d = new Date(obj.startDate);
                }
            },
            beforeShowDay: function(date) {
                return beforeShowDay(date, obj.startDate, obj.endDate, obj.checkDisabledDays,"r");
                /*return beforeShowDay(date, obj.startDate, obj.endDate, obj.checkDisabledDays);*/
            },
            onChangeMonthYear: function(year, month, inst) {
            // salva anno
            obj.curY = year;
            setupYearNavigation(obj);
            }
        });
    }

    $(document).on('click', '.ui-datepicker-prev', function() {
        setCalendarPickerPosition();
    });
    $(document).on('click', '.ui-datepicker-next', function() {
        setCalendarPickerPosition();
    });

    $(document).on('click', '#ui-datepicker-div .year-nav a', function() {

        if($(this).hasClass('selected')) return;

        var _obj = $($.datepicker._curInst.input[0]).data('obj');

        if ($(this).data('year') < $.datepicker.formatDate('yy', _obj.from.datepicker('getDate')) && _obj.curInput == 'to') {
            return;
        }
        _obj.curY = $(this).data('year');
        $('#ui-datepicker-div .year-nav a').toggleClass('selected');

        if(_obj.curY == _obj.y1) {
            var d = new Date();
            setTimeout(function() {
                if (_obj.curInput == 'from') gotoDate(_obj.from, d.getMonth(), _obj.y1);
                else gotoDate(_obj.to, d.getMonth(), _obj.y1);
            }, 1);
        } else if(_obj.curY == _obj.y2) {
            setTimeout(function() {
                if (_obj.curInput == 'from') gotoDate(_obj.from, 0, _obj.y2);
                else gotoDate(_obj.to, 0, _obj.y2);
            }, 1);
        }
    });

    $(document).on('mouseover', '.ui-datepicker td', function(){
        $('.ui-datepicker td').removeClass('ui-datepicker-hover ui-datepicker-highlight ui-datepicker-hover-right ui-datepicker-hover-left');
        // if it doesn't have year data (old month or unselectable date)
        if ($(this).data('year') == undefined) return;

        // datepicker state is not in date range select, depart date wasn't chosen, or return date already chosen then exit

        var _obj = $($.datepicker._curInst.input[0]).data('obj');

        if (!_obj.startDate || !_obj.to || _obj.to.attr('disabled')) return;

        // get date from hovered cell
        var hoverDate = $(this).data('year')+'-'+($(this).data('month')+1)+'-'+$('a',this).html();

        // parse hovered date into milliseconds
        hoverDate = $.datepicker.parseDate('yy-mm-dd', hoverDate).getTime();

        $('.ui-datepicker td').each(function(){

            // compare each table cell if it's date is in date range between selected date and hovered
            if ($(this).data('year') == undefined) return;

            var year = $(this).data('year'),
                month = $(this).data('month'),
                day = $('a', this).html();

            var cellDate = $(this).data('year')+'-'+($(this).data('month')+1)+'-'+$('a',this).html();

            // convert cell date into milliseconds for further comparison
            cellDate = $.datepicker.parseDate('yy-mm-dd', cellDate).getTime();

            // handle hover
            if(_obj.curInput == 'to') {
                if ( cellDate > _obj.startDate && cellDate < hoverDate ) { // le celle in mezzo
                    $(this).addClass('ui-datepicker-hover ui');
                } else if ( cellDate == _obj.startDate ) {
                    $(this).addClass('ui-datepicker-hover ui-datepicker-hover-left'); // la prima cella
                } else if ( cellDate == hoverDate ) {
                    $(this).addClass('ui-datepicker-hover ui-datepicker-hover-right'); // l'ultima cella
                } else {
                    $(this).removeClass('ui-datepicker-hover ui-datepicker-hover-left ui-datepicker-hover-right'); // le celle esterne
                }
            } else if (_obj.endDate) {
                if ( cellDate <= _obj.endDate && cellDate >= hoverDate ) {
                    $(this).addClass('ui-datepicker-hover ui-datepicker-hover-left');
                } else {
                    $(this).removeClass('ui-datepicker-hover ui-datepicker-hover-left');
                }
            }

        });
    });
}

function beforeShowDay(date, startDate, endDate, checkDisabledDays,andataRitorno) {
    var highlight = false,
        currentTime,
        departureDate = startDate ? new Date(startDate) : null,
        departureTimestamp,
        arrivalDate = endDate ? new Date(endDate) : null,
        arrivalTimestamp,
        classes = 'ui-datepicker-highlight';

    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);
    currentTime = date.getTime();

    if(departureDate) {
        departureDate.setHours(0);
        departureDate.setMinutes(0);
        departureDate.setSeconds(0);
        departureDate.setMilliseconds(0);
        departureTimestamp = departureDate.getTime();
        startDate = departureTimestamp;
    }

    if(arrivalDate) {
        arrivalDate.setHours(0);
        arrivalDate.setMinutes(0);
        arrivalDate.setSeconds(0);
        arrivalDate.setMilliseconds(0);
        arrivalTimestamp = arrivalDate.getTime();
        endDate = arrivalTimestamp;
    }

    if ( departureTimestamp && currentTime == departureTimestamp ) {
        classes += ' ui-checkin';
    }
    if (arrivalTimestamp && currentTime == arrivalTimestamp) {
        classes += ' ui-checkout';
    }

    if ((startDate && startDate == currentTime) || (endDate && (currentTime >= startDate && currentTime <= endDate)))
    {
        highlight = true;
        classes += ' disabledDateinRange';
    }

    var fieldActive = true;
    if (checkDisabledDays == true && disabledDaysJson !== null) {

        var string = $.datepicker.formatDate('dd/mm/yyyy', date);
        var startSummer = new Date(disabledDaysJson.availability.startSummer);
        var endSummer = new Date(disabledDaysJson.availability.endSummer);
        var enableDays = [];

        // bl restitusce solo la summer valorizzata correttamente: summer da applicare a tutto l'anno
        if (date >= startSummer && date <= endSummer) {
        //estate
            if (andataRitorno=='r'){
                enableDays = [
                    disabledDaysJson.availability_ret.sundaySummer,
                    disabledDaysJson.availability_ret.mondaySummer,
                    disabledDaysJson.availability_ret.tuesdaySummer,
                    disabledDaysJson.availability_ret.wednesdaySummer,
                    disabledDaysJson.availability_ret.thursdaySummer,
                    disabledDaysJson.availability_ret.fridaySummer,
                    disabledDaysJson.availability_ret.saturdaySummer
                ];
            }else{
                enableDays = [
                    disabledDaysJson.availability.sundaySummer,
                    disabledDaysJson.availability.mondaySummer,
                    disabledDaysJson.availability.tuesdaySummer,
                    disabledDaysJson.availability.wednesdaySummer,
                    disabledDaysJson.availability.thursdaySummer,
                    disabledDaysJson.availability.fridaySummer,
                    disabledDaysJson.availability.saturdaySummer
                ];
            }
        }

        currentDay = date.getDay();
        fieldActive = enableDays[currentDay];
    }

    return [fieldActive, highlight ? classes : ""];
}

function setupYearNavigation(obj) {
    var d = new Date();
    obj.y1 = d.getFullYear();
    d.setDate(d.getDate() + 364);
    obj.y2 = d.getFullYear();
    var currentObj =(obj.curInput == 'from') ? obj.from : obj.to;

    var currentYear = 0;
	var cls1 = '', cls2 = '';

	// se ho cambiato anno
	if (obj.curY != 0){
		cls1 = (obj.curY == obj.y1) ? 'selected' : '';
		cls2 = (obj.curY == obj.y2) ? 'selected' : '';
	} else {
		// se sono from
		if (obj.curInput == 'from') {
			currentYear = obj.curYFrom;
			cls1 = (currentYear == null || currentYear == 0 || currentYear == obj.y1) ? 'selected' : '';
            cls2 = (currentYear == obj.y2) ? 'selected' : '';
        } else {
            currentYear = obj.curYTo;
            // se non ho impostato data in TO
        if (currentYear == null || currentYear == 0) {
            currentYear = obj.curYFrom;
            cls1 = (currentYear == null || currentYear == 0 || currentYear == obj.y1) ? 'selected' : '';
            cls2 = (currentYear == obj.y2) ? 'selected' : '';
        } else {
            cls1 = (currentYear == obj.y1) ? 'selected' : '';
            cls2 = (currentYear == obj.y2) ? 'selected' : '';
        }
    }
    }
    if (currentYear != 0) obj.curY = 0;


    if(obj.y1 != obj.y2 /*&& $('#ui-datepicker-div .year-nav').length == 0*/) {
        $('#ui-datepicker-div .year-nav').remove();
        $('#ui-datepicker-div').prepend('<div class="year-nav">\n' +
            '  <a href="javascript:;" class="' + cls1 + '" data-year="' + obj.y1 + '">' + obj.y1 + '</a>\n' +
            '  <a href="javascript:;" class="' + cls2 + '" data-year="' + obj.y2 + '">' + obj.y2 + '</a>\n' +
            '</div>');
    }
}

function gotoDate($j, month, year) { /* https://stackoverflow.com/questions/9605322/datepicker-jump-to-a-date */
    $j.each(function (i, el) {
        var inst = $.datepicker._getInst(el);
        inst.drawMonth = inst.selectedMonth = month;
        inst.drawYear = inst.selectedYear = year;
        $.datepicker._notifyChange(inst);
        $.datepicker._adjustDate(el);
    });
    setCalendarPickerPosition();
}

function setDisabledDaysFrom(json) {
    disabledDaysJson = json;
}

function setDisabledDaysTo(json) {
    disabledDaysJsonSecond = json;
}

function isAfter(date1,date2){
    var dat1 =  new Date(date1);
    var dat2 =  new Date(date2);
    return dat1>dat2;
}