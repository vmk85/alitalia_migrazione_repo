$(document).ready(function () {
    manageTooltip();
});

function manageTooltip(){
    $(document).on("keyup", "#gigya-password-newPassword", manageModalResetPasswordPopup);
    $(document).on("click", "#gigya-password-newPassword", function () {
       $("#gigya-password-newPassword").trigger("keyup");
    });

    $(document).on("focusout", "#gigya-password-newPassword", function () {
        $(".tooltiptext-modal-reset").hide();
    });
}

function manageModalResetPasswordPopup() {
    var current = $(this);
    var popup = $(".tooltiptext-modal-reset");
    if (current.val().length > 4) {
        popup.show();
        switch (validateModalResetPswTooltip($("#gigya-password-newPassword").val().trim())) {

            case 0:
            case 1:
                $("#colorlevel-modal-reset").addClass("level-1");
                $("#textlevel-modal-reset").html(CQ.I18n.get("psw.text.level.1")).css("color","red");
                break;
            case 2:
                $("#colorlevel-modal-reset").addClass("level-2");
                $("#textlevel-modal-reset").html(CQ.I18n.get("psw.text.level.2")).css("color","green");
                break;
            case 3:
                $("#colorlevel-modal-reset").addClass("level-3");
                $("#textlevel-modal-reset").html(CQ.I18n.get("psw.text.level.3")).css("color","#4ABABC");
                break;
            case 4:
                $("#colorlevel-modal-reset").addClass("level-4");
                $("#textlevel-modal-reset").html(CQ.I18n.get("psw.text.level.4")).css("color","#4ABABC");
                break;
        }
    }
    else {
        popup.hide();
    }
}

function validateModalResetPswTooltip(psw){
    var pointValid = 0;

    var M = /[A-Z]/;
    var m = /[a-z]/;
    var number = /[0-9]/;
    var special = /\W/;
    var space = /^\S+$/;

    if (psw.length >= 8 && space.test(psw)) {

        if(M.test(psw)){
            pointValid += 1;
        }
        if(m.test(psw)){
            pointValid += 1;
        }
        if(number.test(psw)){
            pointValid += 1;
        }
        if(special.test(psw)){
            pointValid += 1;
        }

    }
    $("#colorlevel-modal-reset").attr("class","");

    return pointValid;
}