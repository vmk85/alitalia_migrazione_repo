/**
 * Init necessaria per il bind dei listner al caricamento della partial
 */
function initMmbManage() {
	$('.passaportoButtonForm').click(passaportoButtonFormClickHandler);
	$('.frequentFlyerButtonForm').click(frequentFlyerButtonFormClickHandler);
	$('#modificaContattiButtonForm').click(modificaContattiButtonFormClickHandler);
	$('#richiediFatturaButtonForm').click(richiediFatturaButtonFormClickHandler);
	$('.flightStatusLink').click(flightStatusLinkClickHandler);
	$('.j-sendMailTrigger').fastClick(function() {
		$("#sendMailButton").off("click", sendMailActionClickHandler);
		$("#sendMailButton").click(sendMailActionClickHandler);
		popupLogicReady();
	});
	
	/**
	 * Duplicato di una funzione anonima di main.js - da eliminare se Bitmama darà un nome alla funzione in main.js
	 */
	$('[data-printer]').unfastClick().fastClick(function(e){
		e.preventDefault();
		var className = $(this).attr('data-printer');
		$('body').addClass(className);
		window.print();
		$('body').removeClass(className);
	});
	
	/* gestione tipo intestatario fattura */
	$('[name=tipoFattura]').on('change', function(e) {
		if ($(this).val() == 'PF') {
			$('.form__element.intFattura').hide();
		} else if ($(this).val() == 'A') {
			$('.form__element.intFattura').show();
		}
	});
	
	var printLink = removeUrlSelector() + ".createpdf.json?q=booking";
	$("#downloadPDFMmb").attr("href",printLink);
}


/**
 * Gestione della validazione e del submit delle form di modifica dei dati del passaporto
 */
var passaportoFormId;

function passaportoButtonFormClickHandler(e) {
	passaportoFormId = $($(e.currentTarget).parents('div')).parent('form').attr('id');
	validation(e, 'mmbmodifypnrpassengerinfo', '#' + passaportoFormId, 
			modifyPnrPassengerInfoSuccess, modifyPnrPassengerInfoError);
}

function modifyPnrPassengerInfoSuccess(data) {
	if (data.result) {
		removeErrors();
		
		performSubmit('mmbmodifypnrpassengerinfo', '#' + passaportoFormId, 
		function(data) {
			if (data.result == 'OK') {
				showFormFeedbackSuccess(passaportoFormId, data.message);
			} else if (data.result == 'NOK') {
				showFormFeedbackError(passaportoFormId, data.errorMessage);
			}
		}, 
		modifyPnrPassengerInfoError());
		
	} else {
		showErrors(data);
	}
}

function modifyPnrPassengerInfoError() {
	console.log('passport data edit fail');
}


/**
 * Gestione della validazione e del submit delle form di inserimento codice frequent flyer
 */
var frequentFlyerFormId;

function frequentFlyerButtonFormClickHandler(e) {
	frequentFlyerFormId = $($(e.currentTarget).parents('div')).parent('form').attr('id');
	validation(e, 'mmbmodifypnrpassengerfrequentflyer', '#' + frequentFlyerFormId, 
			modifyPnrPassengerFrequentFlyerSuccess, modifyPnrPassengerFrequentFlyerError);
}

function modifyPnrPassengerFrequentFlyerSuccess(data) {
	if (data.result) {
		startPageLoader(true);
		removeErrors();
		performSubmit('mmbmodifypnrpassengerfrequentflyer', '#' + frequentFlyerFormId, 
		function(data) {
			if (data.result == 'OK') {
				var frequentFlyerCode =$("#" + frequentFlyerFormId).find("input[name='codiceFrequentFlyer']").val();
				hideElementAndReplace($("#"+frequentFlyerFormId).closest(".j-accordions"), '<div class="row"><span class="codeLabel">' + data.message + ' </span><span class="code">' + frequentFlyerCode + '</span></div>') 
			} else if (data.result == 'NOK') {
				showFormFeedbackError(frequentFlyerFormId, data.errorMessage);
			}
			startPageLoader(false);
		}, 
		function(){
			modifyPnrPassengerFrequentFlyerError();
			startPageLoader(false);
		});
		
		
	} else {
		showErrors(data);
	}
}

function modifyPnrPassengerFrequentFlyerError() {
	console.log('frequent flyer data edit fail');
}


/**
 * Gestione della validazione e del submit della form di modifica dei dati di contatto
 */
var contattiFormId;

function modificaContattiButtonFormClickHandler(e) {
	contattiFormId = "modificaContattiForm";
	validation(e, 'mmbmodifypnrcontacts', '#' + contattiFormId, modifyPnrContactsSuccess, modifyPnrContactsError);
}

function modifyPnrContactsSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('mmbmodifypnrcontacts', '#' + contattiFormId, 
		function(data) {
			if (data.result == 'OK') {
				showFormFeedbackSuccess("step3EditContacts", data.message);
			} else if (data.result == 'NOK') {
				showFormFeedbackError("step3EditContacts", data.errorMessage);
			}
		}, 
		modifyPnrContactsError());
		
	} else {
		showErrors(data);
	}
}

function modifyPnrContactsError() {
	console.log('contacts edit fail');
}

/**
 * Gestione della validazione e del submit della form di inserimento dati fatturazione
 */
var richiediFatturaFormId;

function richiediFatturaButtonFormClickHandler(e) {
	richiediFatturaFormId = "richiediFatturaForm";
	validation(e, 'mmbpnrinvocerequest', '#' + richiediFatturaFormId, pnrInvoiceRequestSuccess, pnrInvoiceRequestError);
}

function pnrInvoiceRequestSuccess(data) {
	if (data.result) {
		removeErrors();
		
		performSubmit('mmbpnrinvocerequest', '#' + richiediFatturaFormId, 
		function(data) {
			if (data.result == 'OK') {
				showFormFeedbackSuccess(richiediFatturaFormId, data.message);
			} else if (data.result == 'NOK') {
				showFormFeedbackError(richiediFatturaFormId, data.errorMessage);
			}
		}, 
		pnrInvoiceRequestError());
		
	} else {
		showErrors(data);
	}
}

function pnrInvoiceRequestError() {
	console.log('pnr invoice requestError fail');
}


/**
 * Gestione listner invio email
 */

var sendMailFormId;

function sendMailActionClickHandler(e) {
	e.preventDefault();
	sendMailFormId = "mmbEmailForm";
	validation(e, 'mmbsendmail', '#' + sendMailFormId, sendMailtSuccess, sendMailError);
}

function sendMailtSuccess(data) {
	if (data.result) {
		removeErrors();
	
		performSubmit('mmbsendmail', '#' + sendMailFormId,
			function(data) {
				if (data.result == 'OK') {
					$('a.customOverlay__close.j-overlayCloseReady').click();
				} else if (data.result == 'NOK') {
					console.log("MMB - Send Mail Error");
				}
			},
			sendMailError()
		);
	} else {
		showErrors(data);
	}
}

function sendMailError() {
	console.log('MMB - Send Mail Fail');
}


/**
 * Gestione listner STATO VOLO
 */
var statoVoloLinkId = '';

function flightStatusLinkClickHandler(e) {
	e.preventDefault();
	
	$("#widgetOverlay").show();
	
	statoVoloLinkId = e.target.id
	var split = statoVoloLinkId.split('_');
	var form = {
			formType: 'STATO_VOLO',
			numeroVolo: split[0], 
			dataPartenza: split[1].replace(/\-/g, '/')
	}
	
	performSubmit("preparaviaggiosubmit", form,
		function(data) {
			if (data.result && data.result == "OK") {
				$('#' + statoVoloLinkId + '_LightBox').trigger("click");
			} else {
				console.log('MMB - Stato volo error');
			}
			$("#widgetOverlay").hide();
		}, 
		function() {
			console.log('MMB - Stato volo fail');
			$("#widgetOverlay").hide();
		}
	);
	
}


/**
 *  Funzioni di utilità per mostrare il messaggio di errore o di successo della form 
 */
function showFormFeedbackError(formId, errorMessage) {
	var $formFeedback = $('#' + formId).find('.millemiglia__formFeedback.j-formFeedback');

	// remove success
	$formFeedback.removeClass('isActive');
	$formFeedback.find('p').removeClass('success');
	$formFeedback.find('span.millemiglia__formFeedbackIcon').removeClass('i-check_full');

	// add fail
	$formFeedback.find('p').addClass('fail');
	var classes = $formFeedback.find('span.millemiglia__formFeedbackIcon').attr('class');
	classes = 'i-alert ' + classes;
	$formFeedback.find('span.millemiglia__formFeedbackIcon').attr('class', classes);
	
	$formFeedback.find('span.millemiglia__formFeedbackText').text(errorMessage);
	$formFeedback.addClass('isActive');
}

function showFormFeedbackSuccess(formId, errorMessage) {
	var $formFeedback = $('#' + formId).find('.millemiglia__formFeedback.j-formFeedback');

	// remove fail
	$formFeedback.removeClass('isActive');
	$formFeedback.find('p').removeClass('fail');
	$formFeedback.find('span.millemiglia__formFeedbackIcon').removeClass('i-alert');

	// add success
	$formFeedback.find('p').addClass('success');
	var classes = $formFeedback.find('span.millemiglia__formFeedbackIcon').attr('class');
	classes = 'i-check_full ' + classes;
	$formFeedback.find('span.millemiglia__formFeedbackIcon').attr('class', classes);
	
	$formFeedback.find('span.millemiglia__formFeedbackText').text(errorMessage);
	$formFeedback.addClass('isActive');
}

function refreshManagePartial() {
	$.ajax({
		url: removeUrlSelector() + ".manage-partial.html",
		context : document.body
	}).done(function(data) { 
		$(".mmb-manage-partial").html(data);
		upsellingsManager.initStep($(".mmb-manage-partial").find(".mBpassengerContact"));
		initAccordions();
		initMmbManage();
		initSpecialAssistance();
		
	}).fail(function() { 
		console.error("Error refreshing MMB manage partial")
	});
}
