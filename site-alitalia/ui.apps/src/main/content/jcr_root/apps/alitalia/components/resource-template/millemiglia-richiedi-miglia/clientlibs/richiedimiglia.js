jQuery(document).ready(function() {
	jQuery('#richiedimigliaSubmit').on('click', function(e) {
		validation(e, 'richiedimigliasubmit', '#richiedimiglia',
				richiediMigliaSuccess, richiediMigliaError);
	});
	return false;
});

function richiediMigliaSuccess(data) {
	if (data.result) {
		removeErrors();
		jQuery('#richiedimigliaSubmit').off('click').click();
	} else {
		showErrors(data);
	}
	return false;
}

function richiediMigliaError(data) {
	return false;
}