"use strict";
use(function () {

    var isOpen = true;
    var ctx = this.ctx;

    if (ctx.checkinSelectedPassengers != null) {
        for (var x = 0; x < ctx.checkinSelectedPassengers.size(); x++) {
            var pax = ctx.checkinSelectedPassengers.get(x);
            if (pax.checkInComplete) {
                isOpen = false;
            }
        }
    }

    return isOpen;
});