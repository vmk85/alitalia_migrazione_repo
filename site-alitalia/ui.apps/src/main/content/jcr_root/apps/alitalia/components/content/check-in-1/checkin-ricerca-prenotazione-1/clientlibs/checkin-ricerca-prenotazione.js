var ricercaPrenotazioneOptions = {};

$(document).ready( function() {
    refreshDataLayer();

    ricercaPrenotazioneOptions.initRadio();
    ricercaPrenotazioneOptions.switchForm();
    ricercaPrenotazioneOptions.inputGoupFocus();

    $('#cercaPnrSubmit').bind('click', cercaPnr);
    $('#cercaByFFSubmit').bind('click', cercaByFF);
    $('#cercaByFFSubmitSidebar').bind('click', cercaByFFSidebar);
    $('#divError').hide();

    $("#form-cercaPnr #pnr, #form-cercaPnr #firstName, #form-cercaPnr #lastName").on('keyup', function (e) {
        if (e.keyCode == 13) {
            cercaPnr();
        }
    });

    $("#form-cercaByFF #frequentFlyer, #form-cercaByFF #lastName").on('keyup', function (e) {
        if (e.keyCode == 13) {
            cercaByFF();
        }
    });

    localStorage.removeItem("checkinSearchFail");
    localStorage.removeItem("checkinSearchError");
    localStorage.removeItem("checkinStarted");

});

ricercaPrenotazioneOptions.initRadio = function() {
    if($('.booking-research .radio-wrap input:checked').length) {
        $('.booking-research .radio-wrap input:checked').closest('.radio-wrap').addClass('checked');
        $('input').removeClass('is-invalid-input');
        $('#labelCheckinError').html("");
        $("[class^='fb-']").html("")
    }
    $(document).on('click', '.booking-research .radio-wrap .placeholder', function() {
        $(this).parent().find('input').click();
        $('input').removeClass('is-invalid-input');
        $('#labelCheckinError').html("");
        $("[class^='fb-']").html("")
    });
    $(document).on('click', '.booking-research .radio-wrap input', function() {
        $('.booking-research .radio-wrap input[name='+$(this).attr('name')+']').closest('.radio-wrap').removeClass('checked');
        $(this).closest('.radio-wrap').addClass('checked');
        $('input').removeClass('is-invalid-input');
        $('#labelCheckinError').html("");
        $("[class^='fb-']").html("")
    });
};

ricercaPrenotazioneOptions.switchForm = function() {
    $('.booking-research input[type="radio"]').click(function() {
        if($(this).hasClass('radio-booking_code')) {
            $('.booking-research .booking_code').removeClass('hide');
            $('.booking-research .millemiglia_code').addClass('hide');
        } else {
            $('.booking-research .millemiglia_code').removeClass('hide');
            $('.booking-research .booking_code').addClass('hide');
        }
    });

};

ricercaPrenotazioneOptions.inputGoupFocus = function() {
    $( '.booking-research .custom-input-group input' ).on( 'focus', function() {
        $( this ).closest( '.custom-input-group' ).find( '.input-wrap' ).removeClass( 'selected not-selected' ).addClass( 'not-selected' );
        $( this ).parent().removeClass( 'not-selected' ).addClass( 'selected' );
    } );
};

function cercaPnr(){
    resetErrors();
    updateDataLayer('pnr',$( '#pnr' ).val());
    if( validationInputs('pnr') ) {

        // enable loader
        enableLoaderCheckinSearch();

        var nome = $( '#form-cercaPnr #firstName' ).val().replace(/[^\w\s]/gi, '');
        var cognome = $( '#form-cercaPnr #lastName' ).val().replace(/[^\w\s]/gi, '');

        // Faccio prima una chiamata per capire se il volo esiste, se

        var data = {
            pnr : $( '#form-cercaPnr #pnr' ).val(),
            firstName : nome,
            lastName : cognome,
            checkinSearchType: "code"
        };

        //todo controllare se va bene 'checkinpnrsearch', notare che nella success viene chiamata la servlet 'prpeparaviaggiosubmit'. Forse per andare al vecchio check-in??
        //performSubmit('checkinpnrsearch',data,cercaPnrBeforeSuccess,cercaPnrBeforeFail);
        performSubmit('checkinpnrsearch',data ,cercaPnrSuccess,cercaPnrFail);
    }
}
function cercaPnrBeforeSuccess(data) {
    if (!data.isError)
    {
        enableLoaderCheckinSearch();
        performSubmit('preparaviaggiosubmit','#form-cercaPnr',cercaPnrSuccess,cercaPnrFail);
    }
    else{

        // disable loader
        disableLoaderCheckinSearch();
        var invalid_pnr = data.errorMessage.indexOf("checkin.common.pnrInvalid.error");
        var invalid_pnr_message = CQ.I18n.get('checkin.common.pnrInvalid.error')
        if (invalid_pnr){
            $('#labelCheckinError').show();
            $('#labelCheckinError').text(invalid_pnr_message);
        }else{
            $('#labelCheckinError').show();
            $('#labelCheckinError').text(data.errorMessage);
        }
    }

}

function cercaPnrBeforeFail() {
    // disable loader
    disableLoaderCheckinSearch();

    $('#labelCheckinError').show();
    $('#labelCheckinError').text(CQ.I18n.get('specialpage.error.service'));
    //$('#cercaPnrSubmit').bind('click', cercaPnr);
}

function cercaPnrSuccess(data) {
    checkinStarted++;
    updateDataLayer('checkinStarted',checkinStarted);
    if (!data.isError) {
        window.location.href = data.successPage;
    }else{

        disableLoaderCheckinSearch();
        var invalid_pnr = data.errorMessage.indexOf("checkin.common.pnrInvalid.error");
        var invalid_pnr_message = CQ.I18n.get('checkin.common.pnrInvalid.error');
        var invalid_fields = data.errorMessage.indexOf("checkin.common.genericInvalidField.label");
        var invalid_fields_message = CQ.I18n.get('checkin.common.genericInvalidField.label');
        if(data.sabreStatusCode != "grp") {
            if (invalid_pnr) {
                $('#labelCheckinError').show();
                $('#labelCheckinError').text(invalid_pnr_message);
                checkinError = invalid_pnr_message;
            } else {
                $('#labelCheckinError').show();
                if (invalid_fields) {
                    $('#labelCheckinError').text(invalid_fields_message);
                    checkinError = invalid_fields_message;
                } else {
                    $('#labelCheckinError').text(data.errorMessage);
                    checkinError = data.errorMessage;
                }

            }
        }else{
            $('#labelCheckinError').show();
            $('#labelCheckinError').text(CQ.I18n.get('checkin.common.overPax.error'));
            checkinError = data.errorMessage;
        }
        dataLayer[1].checkinError = CQ.I18n.get(checkinError);
    // _satellite.track("CIError");
    }
}



function cercaPnrFail() {
    disableLoaderCheckinSearch();
    $('#divError').show();
    $('#labelCheckinError').text('Errore (servlet FAIL) ');
    //$('#cercaPnrSubmit').bind('click', cercaPnr);
}

function cercaByFF(){
    resetErrors();
    if( validationInputs('FF') ) {
        resetErrors();
        enableLoaderCheckinSearch();
        performSubmit('searchbyfrequentflyer','#form-cercaByFF',cercaByFFSuccess,cercaByFFFail);
    }
}

function cercaByFFSidebar() {
    resetErrors();
    if (validationInputs('FFSidebar')) {
        resetErrors();
        enableLoaderCheckinSearch();
        performSubmit('searchbyfrequentflyer', '#form-cercaByFFSidebar', cercaByFFSuccess, cercaByFFFail);
    }
}

function cercaByFFSuccess(data) {

    checkinStarted++;
    updateDataLayer('checkinStarted',checkinStarted);
    if (!data.isError) {
        window.location.href = data.successPage;
    }else{
        disableLoaderCheckinSearch();
        $('#divError').show();
        $('#labelCheckinError').text(data.errorMessage);
        dataLayer[1].checkinError = data.errorMessage;
        if(typeof window["_satellite"] !== "undefined"){
        	_satellite.track("CIError");
        } else {
        	console.error("Cannot activate CIError. _satellite object not found");
        }
        dataLayer[1].checkinError = "";
    }
    //$('#cercaByFFSubmit').bind('click', cercaByFF);
}

function cercaByFFFail() {
    disableLoaderCheckinSearch();
    $('#divError').show();
    $('#labelCheckinError').text('Errore (servlet FAIL) ');
    //$('#cercaByFFSubmit').bind('click', cercaByFF);
}

function invokeGenericFormService(service, method, form, done, fail, always,
                                  selector, additionalParams) {
    var actualData;
    if ($.type(form) === "string" || !form) {
        var serializedData = $(form).serialize();
        if (serializedData != "") {
            serializedData += "&";
        }
        serializedData += "_isAjax=true";
        if (additionalParams) {
            for ( var paramName in additionalParams) {
                serializedData += "&" + paramName + "="
                    + encodeURIComponent(additionalParams[paramName]);
            }
        }
        actualData = serializedData;
    } else if ($.isPlainObject(form)) {
        actualData = {};
        if (form) {
            for (var fieldName in form) {
                actualData[fieldName] = form[fieldName];
            }
        }
        actualData['_isAjax'] = true;
        if (additionalParams) {
            for (var paramName in additionalParams) {
                actualData[paramName] = additionalParams[paramName];
            }
        }
    }
    return $.ajax({
        url : getServiceUrl(service, selector),
        method : method,
        data : actualData,
        context : document.body
    }).done(function(data) {
        if (done) {
            done(data);
        }
    }).fail(function() {
        if (fail) {
            fail();
        }
    }).always(function() {
        if (always) {
            always();
        }
    });
}

function performValidation(service, form, done, fail, always, selector) {
    var additionalParams = {
        '_action' : 'validate'
    };
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector, additionalParams);
}

function performSubmit(service, form, done, fail, always, selector) {
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector);
}

function validation(e, service, form, done, fail, always, selector) {
    e.preventDefault();
    return performValidation(service, form, done, fail, always, selector);
}

function validationFormFile(e, service, form, done, fail, always, selector,
                            additionalParams) {
    e.preventDefault();
    additionalParams._action = "validate";
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector, additionalParams);
}

function getDropdownData(requestData, done, fail, always) {
    return invokeGenericFormService('staticdatalistservlet', 'GET', '#', done,
        fail, always, 'json', requestData);
}

function getServiceUrl(service, selector, secure) {
    if (selector == 'login') {
        return service;
    }
    selector = selector || "json";
    var protocol = location.protocol;
    if (jsonProtocol && (selector == "json" || secure)) {
        protocol = 'https:';
    }
    if(service == 'menu-millemiglia-header'){
        return protocol + '//' + location.host + internalUrl1 + "."
            + service + "." + selector;
    } else{
        return protocol + '//' + location.host + internalUrl1 + "/."
            + service + "." + selector;
    }
}

function removeUrlSelector() {
    var url = location.pathname;
    url = url.substr(0, location.pathname.indexOf('.'));
    return location.protocol + '//' + location.host + url;
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex
        .exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g,
        " "));
}

function showErrors(data, noanchor, checkboxnew) {
    removeErrors();
    for (var key in data.fields) {
        var selector = '[name="' + key + '"]';
        if ($(selector).css('display') == 'none' && $(selector).closest(".j-showCaptcha").lenght == 0) {
            selector = $(selector).next();
        }
        $(selector).attr("aria-invalid", "true");
        $(selector).attr("aria-describedby", key + '_error' );
        var $contElement = $(selector).parent();
        if ($(selector).prop("tagName") == 'SELECT' ||
            ($(selector).attr("type") == 'checkbox' && checkboxnew)) {
            $contElement = $(selector).parent().parent();
        }
        var linkClass = "form__errorIcon";
        var msgClass = "form__alert form__errorField";
        if ($(selector).attr("type") === "checkbox") {
            linkClass = linkClass + " left";
            msgClass = msgClass + " left";
        }
        $contElement.addClass("isError");
        $($contElement).append(
            '<a class="' + linkClass +
            ($(selector).attr("type") == 'checkbox' && checkboxnew ? '" style="display:none' : '') +
            '"></a><div id="' + key + '_error'
            + '" class="' + msgClass + '">'
            + data.fields[key] + '</div>');
    }

    // $('[aria-invalid="true"]:first').focus();
    removePopups();

    if (!noanchor) {
        if ($('.isError').length > 0) {
            $(".isError").first().animateAnchor(-32, 400);
        }
    }
    return false;
}

function removePopups(){
    $('[aria-invalid="true"]').focus(function(){
        $('.isError').addClass('isDirty');
    });
}

function removeErrors() {
    $('.isError input[aria-invalid=true]').attr("aria-invalid", "false");
    $('.isError').removeClass('isError isDirty');
    $('.form__errorIcon').remove();
    $('.form__alert').remove();
    return false;
}

function getMarket() {
    return getMarketCode();
}

function getMarketCode(){
    return $("#languageMenu-country").val();
}

function getLanguageCode() {
    if ($('html').attr('lang')) {
        var lang = $('html').attr('lang').split('-');
        return lang[0];
    }
    else {
        var pathArray = window.location.pathname.split("/");
        pathArray = pathArray[1] ? pathArray[1].split("_") : ["en"];
        return pathArray[0];
    }
}

function populateTranslatedMessages() {
    if (translatedMessagesContext.keys.length > 0) {
        $.ajax({
            url : getServiceUrl('translatemessages'),
            method : 'POST',
            data : { messages : translatedMessagesContext.keys },
            context : document.body
        }).done(function(data) {
            if (data.messages) {
                translatedMessagesContext.messages = data.messages;
                translatedMessagesContext.ready = true;
            }
        });
    }
    return false;
}

function addThousandSeparator(x, separator) {
    if(x != undefined && x != null){
        x = x.toString();
        var pattern = /(-?\d+)(\d{3})/;
        while (pattern.test(x))
            x = x.replace(pattern, "$1" + separator + "$2");
    }
    return x;
}


//$(document).ready(function(){
//    initErrorOverlay();
//    initAccordions();
//    ariaAccordions();
//    return false;
//});

function initErrorOverlay(){
    $('.j-hideOnCLick').off('click',eventErrorOverlay);
    $('.j-hideOnCLick').on('click',eventErrorOverlay);
    $('.j-hideOnCLick').click();
}

function eventErrorOverlay(e){
    e.preventDefault();
    $('.j-hideOnCLick').velocity('transition.fadeOut');
}

function ariaAccordions() {
    $("div.j-accordions").not("#millemiglia_list_messages")
        .each(function(i, o) {
            $($(o), "h3.accordion__header").attr("id", "ariaAccodion" + i);
            $("#ariaAccodion" + i + " h3.accordion__header")
                .attr("id", "accordion_panel_label" + i);
            $("#ariaAccodion" + i + " h3.accordion__header a")
                .attr("href", "#accordion2_panel" + i);
            $("#ariaAccodion" + i + " div.accordion__body")
                .attr("id", "accordion_panel" + i)
                .attr("aria-labelledby", "accordion_panel_label" + i);
        });
}

// function loadMenuPersonalArea(){
//     if (typeof isLoginContextAuthenticated != 'undefined'
//         && isLoginContextAuthenticated()) {
//         var url = getServiceUrl("menu-millemiglia-header", "html", true);
//         $.ajax({
//             url : url,
//             context : document.body
//         })
//             .done(loadMenuPersonalAreaSuccess);
//         return false;
//     }
// }

// function loadMenuPersonalAreaSuccess(data){
//     $("#millemigliaMenu").html(data);
// }

/**
 * Retrive the value of a GET parameter in URL
 * @param val the parameter name
 * @returns {String} the value of the parameter if found, "" (blank) otherwise
 */
function getURLParameter(val) {
    var result = "", tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === val){
            result = decodeURIComponent(tmp[1]);
            for (var j = 2; j < tmp.length; j++) {
                result = result + "=" + decodeURIComponent(tmp[j]);
            }
        }
    }
    return result;
}

// open loader
function enableLoaderCheckinSearch() {
    $('.loader-check-in-search').show();
};

// close loader
function disableLoaderCheckinSearch() {
    $('.loader-check-in-search').hide();
};
$(document).ready(function(){
    $("#pnr").blur(function(){
        $(this).val($(this).val().replace(/\s/g,''));
    });
});