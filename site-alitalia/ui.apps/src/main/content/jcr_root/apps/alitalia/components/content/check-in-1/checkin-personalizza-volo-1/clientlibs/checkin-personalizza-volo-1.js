var params = {}, queries, temp, i, l;
// Split into key/value pairs
queries = window.location.search.split("&");
// Convert the array of strings into an object
for ( i = 0, l = queries.length; i < l; i++ ) {
    temp = queries[i].split('=');
    params[temp[0]] = temp[1];
}
var editBp = params["editBp"];
var flight = params["flight"];

$(document).ready(function () {

    $("#loungeX").click(function(){
        $("#priceLoungeAggiornato").show();
        $("#FSpriceAgg").show();
    });

    $("#fastX").click(function(){
        $("#priceLoungeAggiornato").show();
        $("#FSpriceAgg").show();
    });

    if (editBp == "true"){
        $("#hrefButton").attr("href",$("#hrefButton").attr("href")+"?&editBp=true&flight="+flight)
        $("#hrefButton2").attr("href",$("#hrefButton2").attr("href")+"?&editBp=true&flight="+flight)
    }

    performSubmit('checkingetcart', '#frmCart', loadCartSuccess, loadCartFail);

    $(".button-wrap.button-wrap-accordion > button").click(function (){
        $("#priceLoungeAggiornato").hide();
        $("#FSpriceAgg").hide();
        var topDiv = $(this).parents("#content-align").offset().top;
        var topStick = $(".checkin-topbar-container").height();
        var topThis = $(this).parents(".white-block.ancillary-row").height();
        var tot = topDiv  - topStick + topThis;
        $('html, body').animate({scrollTop:tot}, 'slow');
    });

    $(".cta.cta--primary.cta--icon.cta--edit").click(function (){
        var topDiv = $(this).parents("#content-align").offset().top;
        var topThis = $(this).parents(".white-block.ancillary-row").height();
        var topStick = $(".checkin-topbar-container").height();
        var tot = topDiv  - topStick + topThis;
        $('html, body').animate({scrollTop:tot}, 'slow');
    });

});

function getServiceUrl(service, selector, secure) {
    if (selector == 'login') {
        return service;
    }
    selector = selector || "json";
    var protocol = location.protocol;
    if (jsonProtocol && (selector == "json" || secure)) {
        protocol = 'https:';
    }
    if (service == 'menu-millemiglia-header') {
        return protocol + '//' + location.host + internalUrl1 + "."
            + service + "." + selector;
    } else {
        return protocol + '//' + location.host + internalUrl1 + "/."
            + service + "." + selector;
    }
}

function savePassengers() {
    //debugger;
    //$('#btProcediSubmit').unbind('click', cercaPnr);
    //performSubmit('checkinupdatepassenger','#form-passengers',savePassengersSuccess,savePassengersFail);
}

function savePassengersSuccess(data) {
    if (!data.isError) {
        window.location.href = data.successPage;
    }
    else {
        $('#divError').show();
        $('#labelCheckinError').text(data.errorMessage);
    }
    $('#btProcediSubmit').bind('click', savePassengers);
}

function savePassengersFail() {
    $('#divError').show();
    $('#labelCheckinError').text('Errore (servlet FAIL) ');
    $('#btProcediSubmit').bind('click', savePassengers);
}

var checkinCustomizeOptions = checkinCustomizeOptions || {};

// document ready
$(document).ready(function () {
    checkinCustomizeOptions.togglePanel();

/*    $(".accordion-title").click(function () {
        if ($(this).parent(".accordion-item").hasClass("is-active")) {
            $(this).next().slideUp().attr("aria-expanded", "");
            $(this).parent(".accordion-item").removeClass("is-active");
        } else {

            $(this).next().slideDown().attr("aria-expanded", "true");
            $(this).parent(".accordion-item").addClass("is-active");
        }
    });*/

        var ua = navigator.userAgent,
        pickclick = (ua.match(/iPad/i) || ua.match(/iPhone/)) ? "touchend" : "click";

       $('.accordion-title').on(pickclick, function(e) {
        e.preventDefault();
        if ($(this).parent(".accordion-item").hasClass("is-active")) {
            $(this).next().slideUp().attr("aria-expanded", "");
            $(this).parent(".accordion-item").removeClass("is-active");
        } else {

            $(this).next().slideDown().attr("aria-expanded", "true");
            $(this).parent(".accordion-item").addClass("is-active");
        }
    });

    checkinCustomizeOptions.checkinWaveAncillaryBaggage();
    checkinCustomizeOptions.checkinWaveAncillaryLounge();
    checkinCustomizeOptions.checkinWaveAncillaryFastTrack();
    checkinCustomizeOptions.checkinWaveAncillaryInsurance();

    // checkinCustomizeOptions.initSlider();
    checkinCustomizeOptions.swiper = null;
    $('.swiper-slide .bg-image').foundation();
});

checkinCustomizeOptions.togglePanel = function () {
    $(document).on('click', '.cta-wrap-accordion .cta, .button-wrap-accordion .button', function () {
        var relatedContent = $(this).closest('.customize-row').find('.panel-wrap-content');
        if (relatedContent.length > 0 && relatedContent.is(':visible')) {
            relatedContent.slideUp();
        } else {

            relatedContent.slideDown(300, function () {
                // checkinCustomizeOptions.slider.update();
                // checkinCustomizeOptions.slider.pagination.update();
                if ($(relatedContent).find('.slider-row').length && checkinCustomizeOptions.swiper == null) {
                    checkinCustomizeOptions.initSlider();
                }
            });
            $(this).hide();
        }
    });

    $(document).on('click', '.panel-wrap-content .close', function () {
        $(this).closest('.panel-wrap-content').slideUp();
        $(this).closest('.customize-row').find('.cta, .button').show();
    });

}

checkinCustomizeOptions.initAccordion = function () {
    $('.flight-row.accordion').foundation();
};

checkinCustomizeOptions.initSlider = function () {
    checkinCustomizeOptions.swiper = new Swiper('.slider-row .swiper-container', {
        pagination: {
            el: '.swiper-pagination',
        },
    });
    /*
    checkinCustomizeOptions.slider = new Swiper('.swiper-container', {
        'loop': true,
        'speed': 1500,
        autoplay: {
            delay: 5000,
        },
        pagination: {
            el: '.swiper-pagination',
        },
    });

    $( '.swiper-slide .bg-image' ).foundation();*/
};


function avanti() {


    enableLoaderCheckin();

    var price = $(".cart-review .price").attr("data-price");
    if(price.indexOf(",")){
        price = price.replace(",",".")
    }
    var priceFloat = parseFloat(price);
    if (priceFloat > 0)
        document.location.href = './check-in-payment.html'+(editBp == "true"? "?&editBp=true&flight="+flight:"");
    else
        document.location.href = './check-in-thank-you.html'+(editBp == "true"? "?&editBp=true&flight="+flight:"");

}

function clickCheckBox(){
    if ( /^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {


    }else{

    }

}