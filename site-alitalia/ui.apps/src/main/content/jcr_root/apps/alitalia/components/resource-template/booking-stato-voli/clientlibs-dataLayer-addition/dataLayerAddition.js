jQuery(document).ready(function(){
	registerWebtrendsCallback(onTTFSTrackingViewEvent);
	initTTFS_tracking();
	/*init custom lightbox*/
	jQuery(".j-ajaxLoad-withCallback").unfastClick().fastClick(ajaxLoadWithCallback);	
});

function initTTFS_tracking(){
	jQuery(".j-analytics-ttfs-click-evt").off("mousedown", onTTFSTrackingClickEvent);
	jQuery(".j-analytics-ttfs-click-evt").on("mousedown", onTTFSTrackingClickEvent);
}

/**
 * visualizzazione di TT e FS
 */
function onTTFSTrackingViewEvent(){
	jQuery(".j-analytics-ttfs-view-evt").each(function(ind, elem){
		var evtName = $(elem).attr("data-evt");
		var stepName = $(elem).attr("data-step");
		if(typeof window["ttfs_dcs_multitrack_event"] != "undefined"){
			ttfs_dcs_multitrack_event(evtName, stepName);
		}
		$(elem).removeClass("j-analytics-ttfs-view-evt");
	});
}

/**
 * click sui determinati controlli di TT e FS
 * e' possibile scatenare piu' di un evento separandoli con 
 * una virgola
 */
function onTTFSTrackingClickEvent(){
	var evtNames = $(this).attr("data-evt").split(",");
	var stepNames = $(this).attr("data-step").split(",");
	for(ind in evtNames){
		var evtName = evtNames[ind];
		var stepName = stepNames[ind % stepNames.length];
		if(typeof window["mmb_dcs_multitrack_btn_click"] != "undefined"){
			ttfs_dcs_multitrack_event(evtName, stepName);
		}
	}
}


function ajaxLoadWithCallback(event) {
	event.preventDefault();
	var link = $(this).attr('data-link');
	var overlayClass = $(this).attr('data-overlayClass') || '';
	var callback = window[$(this).attr('data-callback')];
	ajaxLoadPopupLogic(link, overlayClass, callback);
}