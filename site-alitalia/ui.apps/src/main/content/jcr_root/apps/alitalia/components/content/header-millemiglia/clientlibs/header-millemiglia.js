$(document).ready(function() {
	
	$('body').removeClass();
	setMillemigliaCartBarLevel(1);
	
	if (userTierCode == 'Ulisse') {
		$('body').addClass('mm_tier2');
		setMillemigliaCartBarLevel(2);
	} else if (userTierCode == 'FrecciaAlata') {
		$('body').addClass('mm_tier3');
		setMillemigliaCartBarLevel(3);
	} else if (userTierCode == 'Plus') {
		$('body').addClass('mm_tier4');
		setMillemigliaCartBarLevel(4);
	} else if(userTierCode == 'Basic'){
		$('body').addClass('mm_tier1');
	}


// if ($('#username').val().trim() == "" || flagusername == false){
// 	$('.millemiglia__accordion.j-accordion').css('display','none');
// 	$('#mm_accordion_dati_login').css('display','block');
// 	}
}
);

function setMillemigliaCartBarLevel(lvl) {
	$('div.level01, div.level02, div.level03, div.level04').removeClass('isActive');
	$('#millemiglia-card-image').attr("src", "/etc/designs/alitalia/clientlibs/images/cardMM_tier_" + lvl + ".png");
	for (var i = 1; i <= lvl; i++) {
		$('div.level0' + i).addClass('isActive');
	}
}