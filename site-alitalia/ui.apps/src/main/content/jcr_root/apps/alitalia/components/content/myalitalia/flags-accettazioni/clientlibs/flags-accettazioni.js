var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();


if(dd<10) {
    dd = '0'+dd
}

if(mm<10) {
    mm = '0'+mm
}

today = yyyy + '-' + mm + '-' + dd ;


$(document).ready(function() {

        $(document).foundation();
        $('#reveal-wrap').foundation();

        var requestData = {
            "linguaCorrispondenza": defaultSelectedLinguaCorrispondenza
        };
        mmSupport.getDropDownData(requestData, getDropDownDataSuccess, getDropDownDataFail);

        $('[data-open="reveal-offerte"]').on("click", function() {

            $(".hide-if-success").show();

        });

    // performSubmit("GetMyAlitaliaCRMDataToSession","",Login.Session.success.optionConsensi,Login.Session.error.getCrmData);

});

$("#subscribeBtn").on("click", function() {
    $(".messagge").show();
    $(".success").hide();
});


function getDropDownDataSuccess(data) {
    try {
        for(i in data.linguaCorrispondenza) {
            if(defaultSelectedLinguaCorrispondenza == data.linguaCorrispondenza[i].value){
                $("#lang").append($("<option>", {
                    value: data.linguaCorrispondenza[i].value,
                    text: data.linguaCorrispondenza[i].name,
                    selected: true
                }));
            }else{
                $("#lang").append($("<option>", {
                    value: data.linguaCorrispondenza[i].value,
                    text: data.linguaCorrispondenza[i].name
                }));
            }
        }
        $("#lang").val(getLingua(linguaComunicazioniCommerciali));

    } catch (err) {
        console.log("Errore nel chiamata [GetDropDrownData]");
        console.log(err);
    }
}

function getDropDownDataFail() {
    alert("Errore nel chiamata [GetDropDrownData]");
}

$("#comunicazioniOfferteProfSubmit").on("click", function(e){

    enableLoaderMyAlitalia();

    var canalePreferitoArray = [];
    /** Variabile per set dati CRM*/
    var canaleComunicazionePreferito = [];

    if($("#notification-email").is(":checked")) {
        canalePreferitoArray.push("email");
    }
        canaleComunicazionePreferito.push({ tipo : "mail" , selezionato : $("#notification-email").is(":checked") , dataUltimoAggiornamento : today });


    if($("#notification-app").is(":checked")) {
        canalePreferitoArray.push("app");
    }
        canaleComunicazionePreferito.push({ tipo : "push" , selezionato : $("#notification-app").is(":checked") , dataUltimoAggiornamento : today });


    if($("#notification-sms").is(":checked")) {
        canalePreferitoArray.push("sms");
    }
        canaleComunicazionePreferito.push({ tipo : "sms" , selezionato : $("#notification-sms").is(":checked") , dataUltimoAggiornamento : today });

        $("#checkSms").val(true);


    $("#tipoContatto").val(canalePreferitoArray.join(":"));

    if($(this).hasClass("MM"))
        mmSupport.validation(e, 'comunicazionioffertesubmit', '#comunicazioniOfferteProfEdit', comunicazioniOfferteEditSuccess, comunicazioniOfferteEditError);
    else {
        // var dataToUpdate = {};
        // var currentDate = new Date().toISOString();
        //
        // var canalePreferito = 0;
        // if($("#notification-email").is(":checked")){
        //     canalePreferito += 1;
        // }
        // if($("#notification-app").is(":checked")){
        //     canalePreferito += 5;
        // }
        // if($("#notification-sms").is(":checked")){
        //     canalePreferito += 9;
        // }

        var newsletter = $("[name='newsletter']:checked").val();
        var profiling = $("[name='consensoProfilazione']:checked").val();
        var lingua = $("#lang").val();
        // var smsAuthorization = $("#notification-sms").is(":checked");
        // var datiPersonali = true;
        //
        //
        // var isAuthComModified = false;
        // var isAuthDataModified = false;
        // var isAuthProfileModified = false;
        //
        // var auth_Com = userMA.data.auth_Com ? userMA.data.auth_Com : '';
        // var auth_Data = userMA.data.auth_Data ? userMA.data.auth_Data : '';
        // var auth_Profile = userMA.data.auth_Profile ? userMA.data.auth_Profile : '';
        //
        // if(auth_Com!=newsletter){
        //     isAuthComModified = true;
        // }
        // if(auth_Data!=datiPersonali){
        //     isAuthDataModified = true;
        // }
        // if(auth_Profile!=profiling){
        //     isAuthProfileModified = true;
        // }
        //
        // dataToUpdate.auth_Com=newsletter;
        // dataToUpdate.auth_Data=datiPersonali;
        // dataToUpdate.auth_Profile=profiling;
        // dataToUpdate.generalPref_Lang01=lingua;
        // dataToUpdate.generalPref_Channel=canalePreferito;
        // dataToUpdate.smsAuthorization=smsAuthorization;
        //
        // if(isAuthComModified){
        //     dataToUpdate.auth_Com_Update = currentDate;
        // }
        // if(isAuthDataModified){
        //     dataToUpdate.auth_Data_Update = currentDate;
        // }
        // if(isAuthProfileModified){
        //     dataToUpdate.auth_Profile_Update = currentDate;
        // }

        // var par = {
        //     data: dataToUpdate,
        //     callback: function(data) {
        //         if(data.errorCode == 0) {
        //             console.log("Salvaggio andato a buon fine");
        //             // saveUserMAToModel();
        //             Login.Session.setSession(data,"MyAlitaliaServlet-UpdateSession");
        //             showSuccessNews();
        //             if(dataToUpdate.auth_Com == true || dataToUpdate.auth_Com == "true") $(".flags-accettazioni").hide();
        //         }
        //     }
        // };
        //
        // gigya.accounts.setAccountInfo(par);

        /** Chiamata alla servlet MyAlitalia per la comunicazione con il CRM ( set dei dati ) */



        var crmRequest = { data:JSON.stringify({
                listaConsensi:[
                    {
                        tipoConsenso:"COM",
                        flagConsenso:newsletter,
                        dataUltimoAggiornamento: today
                    },
                    {
                        tipoConsenso:"PRN",
                        flagConsenso:profiling,
                        dataUltimoAggiornamento: today
                    },{
                        tipoConsenso:"PRO",
                        flagConsenso:profiling,
                        dataUltimoAggiornamento: today
                    }
                ],
                preferenzePersonali:{
                    linguaComunicazioniCommerciali:lingua,
                    linguaComunicazioniServizio:lingua
                },
                canaleComunicazionePreferito:canaleComunicazionePreferito
            })
        };

        performSubmit("SetMyAlitaliaCrmDataConsensi",crmRequest,Login.Session.successAccettazioni,Login.Session.error);


    }
});

function comunicazioniOfferteEditSuccess(data) {
    console.log("[comunicazionioffertesubmit] success");
}

function comunicazioniOfferteEditError() {
    console.log("[comunicazionioffertesubmit] fail");
}

function showSuccessNews() {
    $(".messagge").hide();
    $(".success").show();
    $(".hide-if-success").hide();
    disableLoaderMyAlitalia();
}

$('#subscribeBtn').on('click',function(){
    $('#reveal-offerte').foundation('open');
});

function scroll_to(id) {
    $('html,body').animate({
        scrollTop: $('#'+id).offset().top
    },'slow');
}



$('#result-close-button').on('click',function(){
    console.log("close-button");
    //saveUserMAToModel(res);
});

$(document).on("click", "[data-auth]", function (event) {
    var current = $(this).attr("data-auth");
    // if(current>3)$("[data-auth]:eq("+(current-1)+")").trigger("click");

    if (!($("#" + retriveIdByDataAuth(current)).is(":disabled"))) {
        if (current == 4) {
            enableCheck("consenso_profilazione-yes");
            enableCheck("consenso_profilazione-no");
        } else if (current == 5) {
            enableCheck("consenso_news-no");
            enableCheck("consenso_profilazione-no");
            enableCheck("consenso_news-yes");
        } else if (current == 7) {
            disableCheck("consenso_profilazione-yes");
            enableCheck("consenso_profilazione-no");
        } else if (current == 8) {
            enableCheck("consenso_news-yes");
            enableCheck("consenso_news-no");
        }
    }
});


function disableCheck(id) {
    document.getElementById(id).disabled = true;
    $("label[for="+id+"]").css("color","#c1c1c1");
    if ($("#"+id).is(":checked")){
        $("#"+id).removeAttr('checked').closest(".radio-wrap").removeClass("checked");
    }
    var inputs_disabled = $("input#"+id).parent().parent().parent().find("input:not(:disabled)");
    inputs_disabled.each(function(i, obj) {
         $(obj).attr('checked', true);
         $(obj).parent().parent(".radio-wrap").addClass("checked");
    });
}

function enableCheck(id) {
    document.getElementById(id).disabled = false;
    $("label[for="+id+"]").css("color","black");

}

function retriveIdByDataAuth(dataAuth) {
    var id = "";
    switch (parseInt(dataAuth)) {
        case 3:
            id = "consenso_dati-yes";
            break;
        case 4:
            id = "consenso_news-yes";
            break;
        case 5:
            id = "consenso_profilazione-yes";
            break;
        case 6:
            id = "consenso_dati-no";
            break;
        case 7:
            id = "consenso_news-no";
            break;
        case 8:
            id = "consenso_profilazione-no";
            break;
    }
    return id;
}



function popolateFlag(listaConsensi) {

    for(var i=0; i<listaConsensi.length; i++){
        var consenso = listaConsensi[i];

        if(consenso.tipoConsenso == "COM"){
            if (consenso.flagConsenso){
                $("#comunicazioniOfferteProfEdit #consenso_news-yes[name='newsletter']").click();
            } else {
                $("#comunicazioniOfferteProfEdit #consenso_news-no[name='newsletter']").click();
            }

        }

        if(consenso.tipoConsenso == "PRN"){
            if (consenso.flagConsenso){
                $("#consenso_profilazione-yes").click();
            } else {
                $("#consenso_profilazione-no").click() ;
            }

        }

    }

}

function popolatePreferences(canaleComunicazionePreferito) {

    $(".clearfix [type='checkbox']").closest(".field-wrap").removeClass("checked");

    for(var i=0; i<canaleComunicazionePreferito.length; i++){
        var canale = canaleComunicazionePreferito[i];

        switch (canale.tipo){
            case"MAIL":
                $("#notification-email").click();
                break;
            case"SMS":
                $("#notification-sms").click();
                break;
            case"PUSH":
                $("#notification-app").click();
                break;
        }
    }

}

function getLingua(linguaComunicazioniCommerciali) {
    var lingua;
    switch (linguaComunicazioniCommerciali){
        case"IT":
            lingua="Italiano";
            break;
        case"FR":
            lingua="Francese";
            break;
        case"JA":
            lingua="Giapponese";
            break;
        case"EN":
            lingua="Inglese";
            break;
        case"ES":
            lingua="Spagnolo";
            break;
        case"DE":
            lingua="Tedesco";
            break;
        case"PT":
            lingua="Portoghese";
            break;
        default:
            lingua = "Italiano";
            break;
    }
    return lingua;
}

function popolateLanguage(linguaComunicazioniCommerciali) {
    $("#lang").val(getLingua(linguaComunicazioniCommerciali));
}

function popolateConsensiOption(response) {
    popolateFlag(response.listaConsensi);
    popolatePreferences(response.canaleComunicazionePreferito);
    popolateLanguage(response.preferenzePersonali.linguaComunicazioniCommerciali);
}
