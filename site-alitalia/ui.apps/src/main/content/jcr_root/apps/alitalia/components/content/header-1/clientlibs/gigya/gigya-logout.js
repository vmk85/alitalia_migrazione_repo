var Logout = {};

Logout.Session = {};

// Logout.doLogout = function () {
//     gigya.accounts.logout();
// };

// Logout.eventHandler = function () {
//     userMA = {};
//     $(".login-included-if-authenticated").addClass("hide");
//     $(".login-included-if-anonymous").removeClass("hide");
//     if(globalDestroy.length > 0)
//         for(index in globalDestroy)
//             globalDestroy[index]();
//     $(".show-if-user-logged").addClass("hide");
//     Logout.Session.setSession("MyAlitaliaServlet-logoutMa");
// };

Logout.Session.setSession = function (action) {
    performSubmit(action,"",Logout.Session.success,Logout.Session.error);
};

Logout.Session.success = function (response) {
    // console.log(response);
    var data = JSON.parse(response.data);
    setGigyaCookie(data.cookieName,data.cookieValue,-1);
    setGigyaCookie("firstLogin","true",-1);
    if (window.location.href.indexOf("myalitalia") != -1){
        redirect("homepage");
    } else {
        availableFlightsForMA(false);
        availableMyFlightsForMA(false);
        $("#header").removeClass("myalitalia");
        availableMyFlightsForMA(true,0);
        availableFlightsForMA(true,0);
        $("#MA-login .close-button").click();
        disableLoaderMyAlitalia();
    }

};

Logout.Session.error = function (response) {
    console.log(response);
    disableLoaderMyAlitalia();
};
function setGigyaCookie(nomeCookie,valoreCookie, days) {
    var scadenza = new Date();
    var adesso = new Date();
    scadenza.setTime(adesso.getTime() + (days * (24*60*60*1000)));
    document.cookie = nomeCookie + '=' + escape(valoreCookie) + '; expires=' + scadenza.toGMTString() + '; path=/';
}



