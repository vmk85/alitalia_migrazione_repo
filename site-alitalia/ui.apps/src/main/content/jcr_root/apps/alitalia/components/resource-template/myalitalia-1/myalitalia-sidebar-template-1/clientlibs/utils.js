var MYDATE = new Date();

function getUrlForRequest() {
	return location.protocol + "//" + location.host + location.pathname.substring(0, location.pathname.length-5);
}

function getMessages() {
    $.ajax({
        url: getUrlForRequest() + ".lista-messaggi-partial-1.html",
        context: document.body
    })
    .done(function(data) {
        $(".messages-list").empty();
		$(".messages-list").append(data);
    })
    .fail(function() {
		console.log("fail getting messages");
    });
}

function setDatePickerWithMax(id, start, end, ctrl, initValue, noSelVal, noSelText) {
    $(id).html($("<option>", {
        value: noSelVal,
        text: noSelText
    }));
    var app;
    for(var i=start; i<=end; i++) {
        app = i;
        if(ctrl)
            app = (i <= 9 ? "0" + i.toString() : i);
        $(id).append($("<option>", {
            value: i,
            text: app
        }));
    }
    if(initValue != null) {
		if($(id + " option[value='" + initValue + "']").length != 0)
            $(id).val(initValue);
        else
            $(id).val(noSelVal);
    } else $(id).val(noSelVal);
}

function updateDatePickerWithCtrl(base, base1) {
    var startD = ($(base + "Day").val()!="GG"?parseInt($(base + "Day").val()):false);
    var startM = ($(base + "Month").val()!="MM"?parseInt($(base + "Month").val()):false);
    var startY = ($(base + "Year").val()!="AAAA"?parseInt($(base + "Year").val()):false);
    if(!startD || !startM || !startY)
        return false;
    var endD = ($(base1 + "Day").val()!="GG"?parseInt($(base1 + "Day").val()):0);
    var endM = ($(base1 + "Month").val()!="MM"?parseInt($(base1 + "Month").val()):0);
    var endY = ($(base1 + "Year").val()!="AAAA"?parseInt($(base1 + "Year").val()):0);
    setDatePickerWithMax(base1+"Year", startY, startY+100, false, endY, "AAAA", "AAAA");
    if(endY != MYDATE.getFullYear()) {
        setDatePickerWithMax(base1+"Month", 1, 12, true, endM, "MM", "MM");
        setDatePickerWithMax(base1+"Day", 1, getDaysByMonthAndOrYear(endM, endY), true, endD, "GG", "GG");
    } else {
        setDatePickerWithMax(base1+"Month", startM, 12, true, endM, "MM", "MM");
        setDatePickerWithMax(base1+"Day", startD, getDaysByMonthAndOrYear(endM, endY), true, endD, "GG", "GG");
    }
}

function getDaysByMonthAndOrYear(month, year) {
	var maxDays = 31;
    var maxFeb;
    if(year != null) {
		if(parseInt(year)%4 == 0) maxFeb = 29;
        else maxFeb = 28;
    }
    switch(parseInt(month)) {
        case 4:
        case 6:
        case 9:
        case 11:
            return 30;
        case 2:
            return maxFeb;
        default:
            return 31;
    }
}

function undoComplete(base) {
    undo(base);
    undoWithNoValues(base + "Exp");
    $("#docNum").val("");
    $("#docType").val("type");
}

function undo(base) {
    $("select" + base + "Day").val("GG");
    $("select" + base + "Month").val("MM");
    $("select" + base + "Year").val("AAAA");
}

function undoWithNoValues(base) {
    $("select" + base + "Day").html($("<option>", {
        text: "GG",
        value: "GG"
    }));
    $("select" + base + "Month").html($("<option>", {
        text: "MM",
        value: "MM"
    }));
    $("select" + base + "Year").html($("<option>", {
        text: "AAAA",
        value: "AAAA"
    }))
}



function capitalizeName(input) {
	input = input.split(" ");
    var finale = "";
    if(input.length > 1) {
        for(var i=0; i<input.length; i++)
            finale += capitalizeName(input[i].toLowerCase()) + (i < input.length-1 ? " " : "");
        return finale;
    } else {
        finale = input[0].toLowerCase();
    	return finale.substring(0, 1).toUpperCase() + finale.substring(1, finale.length);
    }
}

function initECSelect() {
    var date = new Date();
	setDatePickerWithMax("#selECDay", 1, 31, true, null, "GG", "GG");
	setDatePickerWithMax("#selECMonth", 1, 12, true, null, "MM", "MM");
	setDatePickerWithMax("#selECYear", 1990, date.getFullYear(), false, null, "AAAA", "AAAA");
}

// funzioni estratto conto

function getStructureECTable() {
	var stru = "";
    stru += "<tr>";
    stru += "  <th>Data</th>";
    stru += "  <th>Attività</th>";
    stru += "  <th>Classe</th>";
    stru += "  <th>Descrizione</th>";
    stru += "  <th>Miglia</th>";
    stru += "</tr>";
    return stru;
}

function getEstrattoConto() {
    $.ajax({
        url: getUrlForRequest() + ".estratto-conto-partial-1.html",
        context: document.body
    }).done(function(data){
        $(".conto").html(getStructureECTable());
		$(".conto").append(data);
    }).fail(function() {
        console.log("fail getting estratto conto")
    });
}

function getEstrattoContoByDate() {
    var params = {
        activity : $('#attivita').val(),
        day: $("#selECDay").val(),
        month: $("#selECMonth").val(),
        year: $("#selECYear").val()
    };
    $.ajax({
        url: getUrlForRequest() + ".estratto-conto-partial-1.html",
        context: document.body,
        data: params
    }).done(function(data){
		console.log(data);
        $(".conto").html(getStructureECTable());
		$(".conto").append(data);
    }).fail(function() {
        console.log("fail getting estratto conto from " + params.day + "/" + params.month + "/" + params.year)
    });
}

