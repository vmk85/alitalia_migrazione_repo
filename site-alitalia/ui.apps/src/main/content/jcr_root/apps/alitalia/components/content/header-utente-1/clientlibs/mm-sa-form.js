
/***  mm_strong_password ***/
/**
 * Selettori Servlet :
 * checkCredenzialiMM_Selector
 * getDatiSicurezzaProfiloMM_Selector
 * setDatiSicurezzaProfiloMM_Selector
 * findProfileMM_Selector
 * getElencoDomandeSicurezzaMM_Selector
 * **/

/*** oggetto di risposta dalla servlet  ***/
var dataResponse;
/*** Oggetto per gestione recaptcha invisible ***/
var headerWaveRecaptchaIDSA;

/*** CheckCredenzialiMillemiglia - Begin ***/
function checkCredenzialiMillemiglia(userName, password){

    var data = {
        mmCode : userName,
        mmPin : password,
        notCaptchaValidationExist : true,
    };

    invokeGenericFormService('checkCredenzialiMillemiglia', 'POST', '',
        checkCredenzialiMillemigliaSuccess, checkCredenzialiMillemigliaFailure, checkCredenzialiMillemigliaAlways, undefined, data);

}

function checkCredenzialiMillemigliaSuccess(data){
// risposta positiva dalla servlet parametri:
// FlagCredenziali_OK, IDProfilo;
    if (data) {
        if (!data.successful){
            //setPassError(data.errorDescription);
            setPassError(CQ.I18n.get('header.millemiglia.invalidCredential'));
        }
        else {
            dataResponse = data;
            successCheckMilleMigliaLogin(dataResponse, true, null);
            //setPassError('header.millemiglia.login.internal.error');
        }
    }
}

function checkCredenzialiMillemigliaFailure(data){
// risposta negativa dalla servlet parametri:
//

}

function checkCredenzialiMillemigliaAlways(data){

}
/*** CheckCredenzialiMillemiglia - End ***/

/*** GetDatiSicurezzaProfilo - Begin ***/
function getDatiSicurezzaProfiloMM(idProfilo){

    var data = {
        idProfilo : idProfilo,
    };

    invokeGenericFormService('getDatiSicurezzaProfiloMM_Selector', 'POST', '',
        getDatiSicurezzaProfiloMMSuccess, getDatiSicurezzaProfiloMMFailure, getDatiSicurezzaProfiloMMAlways, undefined, data);

}

function getDatiSicurezzaProfiloMMSuccess(data){
// risposta positiva dalla servlet;
// Parametri:
// FlagIDProfiloExists,
// TipoProfilo (MMKids,MMPersoneFisiche, MMYoung, mmBusiness),
// Username, FlagVerificaUserName_OK, Password, FlagVerificaPassword_OK,
// Cellulare, FlagVerificaCellulare_OK, IDDomandaSegreta, Risposta,
// FlagVerificaRisposta_OK, EmailAccount, FlagVerificaEmailAccount_ok,
// SocialUser_ID, SocialToken, FlagSocialSyncro_OK, FlagRememberPassword
    if (data) {
        dataResponse = data;
    }
}

function getDatiSicurezzaProfiloMMFailure(data){
// risposta negativa dalla servlet;
// Parametri:
//

}

function getDatiSicurezzaProfiloMMAlways(data){

}
/*** GetDatiSicurezzaProfilo - End ***/


/*** setDatiSicurezzaProfilo - Begin ***/
var credentialRequestData = {
    flagIDProfiloExists : false,
    tipoProfilo : '',
    flagVerificaUserName_OK : false,
    password : '',
    flagVerificaPassword_OK : false,
    cellulare : '',
    flagVerificaCellulare_OK : false,
    idDomandaSegreta : '',
    idRispostaSegreta : '',
    FlagVerificaRisposta_OK : false,
    EmailAccount : '',
    FlagVerificaEmailAccount_OK : false,
    SocialUser_ID : '',
    SocialToken : '',
    FlagSocialSyncro_OK : false,
    FlagRememberPassword : false
};

function setDatiSicurezzaProfiloMMSuccess(credentialRequestData){
// risposta positiva dalla servlet parametri:
// FlagIDProfiloExists,
// TipoProfilo (MMKids,MMPersoneFisiche, MMYoung, mmBusiness),
// Username, FlagVerificaUserName_OK, Password, FlagVerificaPassword_OK,
// Cellulare, FlagVerificaCellulare_OK, IDDomandaSegreta, Risposta,
// FlagVerificaRisposta_OK, EmailAccount, FlagVerificaEmailAccount_ok,
// SocialUser_ID, SocialToken, FlagSocialSyncro_OK, FlagRememberPassword

    invokeGenericFormService('setDatiSicurezzaProfiloMM_Selector', 'POST', '',
        setDatiSicurezzaProfiloMMSuccess, setDatiSicurezzaProfiloMMFailure, setDatiSicurezzaProfiloMMAlways, undefined, credentialRequestData);

}

function setDatiSicurezzaProfiloMMSuccess(data){
// risposta positiva dalla servlet parametri:
// FlagIDProfiloSicurezzaSaved (boolean)
    if (data) {
        dataResponse = data;
    }
}

function setDatiSicurezzaProfiloMMFailure(data){
// risposta negativa dalla servlet parametri:
//

}

function setDatiSicurezzaProfiloMMAlways(data){

}
/*** SetDatiSicurezzaProfilo - End ***/

/*** FindProfile  - Begin ***/

var findProfileRequest= {
    username: '',
    alias: '',
    codiceMM: '',
    cognome: ''
};

function findProfile(data){

    if (data) {
        credentialRequestData = data;

        invokeGenericFormService('findProfileMM_Selector', 'POST', '',
            findProfileMMSuccess, findProfileMMFailure, findProfileMMAlways, undefined, credentialRequestData);
    }
}

function findProfileMMSuccess(data){
// risposta positiva dalla servlet parametri:
// FlagIDProfiloSicurezzaSaved (boolean)
    if (data) {
        dataResponse = data;
    }
}

function findProfileMMFailure(data){
// risposta negativa dalla servlet parametri:
//

}

function findProfileMMAlways(data){

}
/* FindProfile  - End*/

/* getElencoDomandeSicurezza - Begin*/
function getElencoDomandeSicurezza(){

    invokeGenericFormService('getElencoDomandeSicurezzaMM_Selector', 'POST', '',
        getElencoDomandeSicurezzaMMSuccess, getElencoDomandeSicurezzaMMFailure, getElencoDomandeSicurezzaMMAlways, undefined, null);
}

function getElencoDomandeSicurezzaMMSuccess(data){
// risposta positiva dalla servlet ;
// parametri:
// Collezione di: idDomandaSegreta, idFieldLabelForTranslation)
    if (data) {
        dataResponse = data;
    }
}

function getElencoDomandeSicurezzaMMFailure(data){
// risposta negativa dalla servlet parametri:
//

}

function getElencoDomandeSicurezzaMMAlways(data){

}
/* getElencoDomandeSicurezza - End*/


/* metodi di servizio si basso livello  */
function performValidation(service, form, done, fail, always, selector) {
    var additionalParams = {
        '_action' : 'validate'
    };
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector, additionalParams);
}
function performSubmit(service, form, done, fail, always, selector) {
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector);
}

function invokeGenericFormService(service, method, form, done, fail, always,
                                  selector, additionalParams) {
    var actualData;
    if ($.type(form) === "string" || !form) {
        var serializedData = $(form).serialize();
        if (serializedData != "") {
            serializedData += "&";
        }
        serializedData += "_isAjax=true";
        if (additionalParams) {
            for ( var paramName in additionalParams) {
                serializedData += "&" + paramName + "="
                    + encodeURIComponent(additionalParams[paramName]);
            }
        }
        actualData = serializedData;
    } else if ($.isPlainObject(form)) {
        actualData = {};
        if (form) {
            for (var fieldName in form) {
                actualData[fieldName] = form[fieldName];
            }
        }
        actualData['_isAjax'] = true;
        if (additionalParams) {
            for (var paramName in additionalParams) {
                actualData[paramName] = additionalParams[paramName];
            }
        }
    }
    return $.ajax({
        url : getServiceUrl(service, selector),
        method : method,
        data : actualData,
        context : document.body
    }).done(function(data) {
        if (done) {
            done(data);
        }
    }).fail(function() {
        if (fail) {
            fail();
        }
    }).always(function() {
        if (always) {
            always();
        }
    });
}

function preCheckCredential(){
    console.log('preCheckCredential');
    var authType;
    try {
        authType = validationInputsLoginAuth('NOSA');

    } catch (exception) {
        /* TODO Creare div Errore  */
        console.error(exception);
    }
}

/**
 * Event handler for header login button click, start login process with MilleMiglia credentials.
 */
function onHeaderButtonLoginClickSA(e) {
    // removeErrors();
    performValidation('millemiglialoginsavalidation', '#form-millemiglia-login-1',
        milleMigliaHeaderLoginSAValidationSuccess, milleMigliaHeaderLoginSAValidationFailure);


    //when service not working after 15 seconds error message displayed
    function tryLogin(){
        if ($.active > 0){
            setTimeout(tryLogin,2000);
        }else{
            grecaptcha.reset();
            refreshReCaptchaLogin(headerWaveRecaptchaIDSA);
        }

    };
    tryLogin();

    return false;
}

function preCheckStrongAuthCredential(){
    console.log('preCheckStrongAuthCredential');

    var authType;
    try {
        // authType = validationInputsLoginAuth('SA');
        // console.log(authType ? 'StrongAuth' : 'Pincode');
        var userName = $( '#millemiglia-sa' ).val().trim();
        var password = $( '#password' ).val().trim();
        // if (userName === '' || userName === undefined || userName === null){
        //     /* TODO errore username non valorizzato*/
        // }
        // if (password === '' || password === undefined || password === null){
        //     /* TODO errore  password non valorizzato*/
        // }

        //removeErrors();
        $("#form-mm-username").val(userName);
        $("#form-mm-pin").val(password);

        if ($("#submit-feedback-login-SA").length > 0) {
            if (headerWaveRecaptchaIDSA === undefined) {

                headerWaveRecaptchaIDSA = grecaptcha.render('submit-feedback-login-SA',{
                    'sitekey':invisibleRecaptchaSiteKey,
                    'callback':"onHeaderButtonLoginClickSA",
                    'size':"invisible"
                });
            }
            grecaptcha.execute(headerWaveRecaptchaIDSA);
            removeErrors();
        }

        //performValidation('millemiglialoginsavalidation', '#form-millemiglia-login-1',
        //    milleMigliaHeaderLoginSAValidationSuccess, milleMigliaHeaderLoginSAValidationFailure);
        return false;

    } catch(exception) {
        /* TODO msg Errore */
        console.error(exception);
    }
}

// function validationInputsLoginAuth(type) {
//     // console.log(type);
//     var noNumbers = /^([^0-9]*)$/;
//     var isNumbers = /^\d+$/;
//
//     var pinTest = /^[0-9]{4}$/;
//     var mmCodeTest = /^[0-9]{6}$/;
//     var validationReturn;
//
//     /* Validazione Strong Authentication */
//     if (type == 'SA'){
//         var userName = $( '#millemiglia' ).val().trim();
//         var password = $( '#password' ).val().trim();
//         console.log('userName: ', userName);
//         console.log('userName: ', userName);
//         validationReturn = ( userName !== '' && noNumbers.test(userName)) ? 1 : 0;
//     } else if (type == 'NOSA') {/* Validazione MilleMiglia Pin Code */
//         var mmCode = $( '#millemiglia-code' ).val().trim();
//         var pinNumber = $( '#pin' ).val().trim();
//         validationReturn = ( mmCode !== '' && mmCodeTest.test(mmCode) && pinTest.test(pinNumber)) ? 1 : 0;
//     }
//
//     return validationReturn;
//
// }

function validationInputsLoginAuth_SA() {

    var noNumbers = /^([^0-9]*)$/;
    var isNumbers = /^\d+$/;

    var pinTest = /^[0-9]{4}$/;
    var validationReturn;

    var userName = $("#millemiglia-sa");
    // var email = $("#millemiglia");
    var emailError = $("#labelErrorLogin-sa-millemiglia").find("p");
    var pass = $("#password");
    var passError = $("#labelErrorLogin-sa-password").find("p");
    var valid = true;
    if(userName.val() === ""  || userName.val() === undefined || userName.val() === null) {
        setUserError(CQ.I18n.get("mm.strong.auth.fieldObligatory")); valid = false;
    }
    else {
        // if(!isEmail(userName.val())) {setUserError(CQ.I18n.get("mm.strong.auth.isNotEmail")); valid = false;}
        // else resetEmailError();
        resetUserError();
    }
    if(pass.val() === "" || pass.val() === undefined || pass.val() === null)  {
        setPassError(CQ.I18n.get("mm.strong.auth.fieldObligatory")); valid = false;
    }
    else resetPassError();

    if (isNumbers.test(userName)){
        console.log('Old MM Auth')
    } else {
        console.log('Strong Auth')
    }
    return valid;
}

function setUserError(txt) {
    $("#millemiglia-sa").addClass("is-invalid-input");
    $("#labelErrorLogin-sa-millemiglia").find("p").html(txt);
}

function setPassError(txt) {
    $("#password").addClass("is-invalid-input");
    $("#labelErrorLogin-sa-password").find("p").html(txt);
}

function resetUserError() {
    $("#millemiglia-sa").removeClass("is-invalid-input");
    $("#labelErrorLogin-sa-millemiglia").find("p").html("");
}

function resetPassError() {
    $("#password").removeClass("is-invalid-input");
    $("#labelErrorLogin-sa-password").find("p").html("");
}

/* Strong Auth Login */
function callSALogin() {

    resetUserError();
    resetPassError();

    if (validationInputsLoginAuth_SA()) {
        preCheckStrongAuthCredential();
    }
}

function validationLoginNewUsername(username) {

    $('#header-wave-login-errors-SA').html('');
    var regUser = new RegExp("^[0-9]{0,10}$");
    if(regUser.test(username)){
        return true;
    } else {
        return false;
    }

}

function validationLoginNewPassword(passworld) {

    var regPassworld  = new RegExp("^[0-9]{4}$");
    if(regPassworld.test(passworld)){
        return true;
    } else {
        return false;
    }

}
function callSALoginOldLogin() {

    if (validationLoginNewUsername($('#millemiglia-sa').val())){
        if(validationLoginNewPassword($('#password').val())){
            loginOptions.captchaSaOldLogin();
        } else if (isPasswordStrongAuthentication($('#password').val())){
           //TODO: password non conforme
            callSALogin();
        } else {
            $('#labelErrorLogin-sa-password').html(CQ.I18n.get("millemiglia.password.error.notstrong")).show();
        }
    } else if(isUsernameStrongAuthentication($('#millemiglia-sa').val())){
        if(isPasswordStrongAuthentication($('#password').val())){
            callSALogin();
        } else {
            //TODO: password non conforme
            $('#labelErrorLogin-sa-password').html(CQ.I18n.get("millemiglia.password.error.notstrong")).show();
        }
    } else {
        //TODO: username non conforme
        $('#header-wave-login-errors-SA').html('');
        $('#labelErrorLogin-sa-millemiglia').html(CQ.I18n.get("millemiglia.username.error.notstrong")).show();
    }
}

// function validateSAlogin(user, psw) {
//     var u = isUsernameStrongAuthentication(user);
//     var p = isPasswordStrongAuthentication(psw);
//     return  u && p;
// }
function isUsernameStrongAuthentication(value) {
    $('#labelErrorLogin-sa-millemiglia').html('');
    $('#header-wave-login-errors-SA').html('');
    var el = "";
    var regexnum = /^[0-9]*$/;
    if(value.length > 0) {
        el = value;
    }
    if (el.length >0 && regexnum.test(el)) {
        return true;
    } else if (isMMUserName(el)){
        return true;
    } else {
        $('#labelErrorLogin-sa-millemiglia').html(CQ.I18n.get("millemiglia.username.error.notstrong")).show();
        return false;
    }
}
function isPasswordStrongAuthentication(value){

    $('#labelErrorLogin-sa-password').html('');
    var el = "";
    if(value.length > 0) {
        el = value;
    }
    if  (el.length <8 ) {
        $('#labelErrorLogin-sa-password').html(CQ.I18n.get("millemiglia.password.error.notstrong")).show();
        // alert("password non conforme")
        return false;
    }
    //alert("bad password");

    var hasUpperCase = /[A-Z]/.test(el);
    var hasLowerCase = /[a-z]/.test(el);
    var hasNumbers = /\d/.test(el);
    //var hasNonalphas = /[@#\-_!£]/.test(value);
    var hasNonalphas = (/\W/.test(el) || el.indexOf('_')>-1 );
    if (hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas < 4){
        $('#labelErrorLogin-sa-password').html(CQ.I18n.get("millemiglia.password.error.notstrong")).show();
        // alert("password non conforme")
        return false; //alert("bad password");
    } else
        return true;

}

function isMMUserName(value) {
    if(!/[^a-zA-Z0-9]/.test(value)) {
        var regex = new RegExp("^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[A-Za-z0-9]).{8,40}$"); //almeno 1 lettera almeno un numero nimimo 8 massimo 40
        if (value!=null && regex.test(value)){
            return true
        } else {
            return false
        }
    } else {
        return false
    }
}

function validationInputsLoginAuth() {

    var noNumbers = /^([^0-9]*)$/;
    var isNumbers = /^\d+$/;

    var pinTest = /^[0-9]{4}$/;
    var mmCodeTest = /^[0-9]$/;
    var validationReturn;

    // var userName = $("#millemiglia-code");
    var millemigliaCode = $("#millemiglia-code");
    var emailError = $("#labelErrorLogin-millemiglia").find("p");
    // var pass = $("#pin");
    var pinCode = $("#pin");
    var passError = $("#labelErrorLogin-pin").find("p");
    var valid = true;
    if(millemigliaCode.val() == "" && !mmCodeTest.test(millemigliaCode) ) { setMMCodeError(CQ.I18n.get("mm.strong.auth.fieldObligatory")); valid = false;}
    else {
        if(mmCodeTest.test(millemigliaCode.val())) {setMMCodeError(CQ.I18n.get("mm.strong.auth.isNotPMMCodeNumber")); valid = false;}
        else resetMMCodeError();
    }
    if(pinCode.val() == "" ) { setPinError(CQ.I18n.get("mm.strong.auth.fieldObligatory")); valid = false; }
    else if (pinTest.test(pinCode)) {setPinError(CQ.I18n.get("mm.strong.auth.fieldObligatory")); valid = false;}
    else resetPinError();
    return valid;
}

function setMMCodeError(txt) {
    $("#millemiglia-code").addClass("is-invalid-input");
    $("#labelErrorLogin-millemiglia").find("p").html(txt);
}

function setPinError(txt) {
    $("#pin").addClass("is-invalid-input");
    $("#labelErrorLogin-pin").find("p").html(txt);
}

function resetMMCodeError() {
    $("#millemiglia-code").removeClass("is-invalid-input");
    $("#labelErrorLogin-millemiglia").find("p").html("");
}

function resetPinError() {
    $("#pin").removeClass("is-invalid-input");
    $("#labelErrorLogin-pin").find("p").html("");
}

function callNoSALogin() {
    resetMMCodeError();
    resetPinError();
    if (validationInputsLoginAuth()){
        preCheckCredential();
    }
}

// function onLoginSADocumentReady(e) {
//     var formId = $(this).closest("form").attr("id");
//     if ( formId === "form-millemiglia-header-login-sa") {
//         $('#login-submit-sa').bind('click', preCheckStrongAuthCredential);
//     } else if (formId === "form-millemiglia-header-login") {
//         $('#login-submit').bind('click', preCheckCredential);
//     }
// }
// add listener for document ready event to setup input form
// $(document).ready(onLoginSADocumentReady);

$(document).ready(function(){
    //onLoginSADocumentReady;
    $('#openSaLogin').on('click',function(){
        resetUserError();
        resetPassError();
        resetMMCodeError();
        resetPinError();
        //invokeShowCaptcha("captcha_header", "1");
    });
});


/**
 * Callback for successful loginSA form validation call.
 */
function milleMigliaHeaderLoginSAValidationSuccess(data) {

    var userName = $("#millemiglia-sa").val();
    var password = $("#password").val();

    if (data.result) {

        var rememberMe = "0";
        if ($("#header_rememberme").is(":checked")) {
            rememberMe = "1";
        }

        startLoginSAWithMilleMigliaCredentials(userName, password, rememberMe,
            headerLoginUnexpectedErrorCallback, headerLoginFailureCallback,null,loginUserLockedErrorCallback,true);
    } else {
        if (typeof refreshReCaptchaLogin !== 'undefined') {
            refreshReCaptchaLogin(headerRecaptchaID);
        }
        showErrors(data);
    }
}


/**
 * Callback for successful loginSA form validation call.
 */
function milleMigliaHeaderLoginSAValidationFailure(data) {
    if (typeof refreshReCaptchaLogin !== 'undefined') {
        refreshReCaptchaLogin(headerRecaptchaID);
    }
    headerLoginUnexpectedErrorCallback();
}

