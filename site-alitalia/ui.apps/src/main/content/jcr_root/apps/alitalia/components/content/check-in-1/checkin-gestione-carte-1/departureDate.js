"use strict";
use(function () {

    var segment = this.segment;

    var dt = segment.originalDepartureDate.split("T")[0];

    return {
        dt: dt
    };

});