
function initPartialCheckinExtraBaggage() {
	$('.extra-baggage-checkbox').change(function(e) {
		var checkbox = e.currentTarget;
		var hidden = $(checkbox).siblings('input[type="hidden"].extra-baggage-checkbox-field');
		var currentVal = $(hidden).val();
		var newVal = currentVal.substring(0, currentVal.length - 1) + ( $(checkbox).is(':checked') ? '1' : '0' );
		$(hidden).val(newVal);
		performLastBaggageSelectedOperation($('.extra-baggage-checkbox').closest(".upsellingChoose"), "be_addExtraBaggage", "be_clearExtraBaggageByCheckBox");
	});
}

function be_addExtraBaggage(e, btn, selector, done) {
	performValidation('checkinancillarycartextrabaggage', '#extraBaggageSelectionForm', 
			function(data){
				checkinAncillaryExtraBaggageAddValidationSuccess(data,done);
			},
			function(){
				checkinHandleInvokeError(done);
			}
	);
}

function checkinAncillaryExtraBaggageAddValidationSuccess(data,done) {
	if (data.result) {
		removeErrors();
		performSubmit('checkinancillarycartextrabaggage', '#extraBaggageSelectionForm', 
				function(data){
					checkinAncillaryExtraBaggageAddSubmitSuccess(data,done);
				},
				function(){
					checkinHandleInvokeError(done);
				}
		);
	} else {
		showErrors(data);
	}
}

function checkinAncillaryExtraBaggageAddSubmitSuccess(data,done) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		refreshCheckinPartialExtraBaggage(true, true,
				function(){
					if (done) {
						done();
					}
				}
		);
	}
}


/* clear form */
function be_clearExtraBaggageByCheckBox(e, btn, selector, done) {
	be_clearExtraBaggage(e, done, true);
}

function be_clearExtraBaggage(e, done, clearAll) {
	performValidation('checkinancillarycartextrabaggage', '#checkin-ancillary-clear-extrabaggage-form', 
		function(data) {
			checkinAncillaryExtraBaggageClearValidationSuccess(data,done,clearAll);
		},
		function(){
			checkinHandleInvokeError(done);
		}
	);
}

function checkinAncillaryExtraBaggageClearValidationSuccess(data,done,clearAll) {
	if (data.result) {
		removeErrors();
		performSubmit('checkinancillarycartextrabaggage', '#checkin-ancillary-clear-extrabaggage-form', 
				function(data) {
					checkinAncillaryExtraBaggageClearSubmitSuccess(data,done,clearAll);
				},
				function(){
					checkinHandleInvokeError(done);
				}
		);
	} else {
		showErrors(data);
	}
}

function checkinAncillaryExtraBaggageClearSubmitSuccess(data,done,clearAll) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		refreshCheckinPartialCart();
		if (clearAll) {
			reloadExtraBaggage(done);
		} else {
			if (done) {
				done();
			}
		}
	}
}

function reloadExtraBaggage(done){
	$.ajax({
		url: removeUrlSelector() + ".box-extrabaggage-reloaded-partial.html",
		context : document.body
	})
	.done(function(data) {
		$(".checkin-partial-extra-baggage").replaceWith(data);
		upsellingsManager.initStep($(".checkin-partial-extra-baggage").find(".j-upselling"));
		initPartialCheckinExtraBaggage();
		if (done) {
			done();
		}
	})
	.fail(function() {
		initPartialCheckinExtraBaggage();
		if (done) {
			done();
		}
	});
}

function checkinHandleInvokeError(done){
	//console.log("Error");
	if (done) {
		done();
	}
}