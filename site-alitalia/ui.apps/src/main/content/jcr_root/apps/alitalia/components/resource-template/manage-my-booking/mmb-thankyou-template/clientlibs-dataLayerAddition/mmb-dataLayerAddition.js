var DTMRule = "";


function analytics_mmbPartialCallback(){
	addMmbAnalyticsData(window['analytics_mmb_step']);
	addMmbDataLayerVar();
	mmbThankyou_setMeta();
	activateGTMOnce("mmb");
	activateWebtrendsOnce("mmb");
	if(DTMRule){
		activateDTMRule(DTMRule);
	}
}

function addMmbAnalyticsData(mmbStep){
		switch(mmbStep){
		case "4":
			DTMRule = "MMBReceipt";
			break;
		default:
			console.log("Invalid mmb step");
		}	
	}

function addMmbDataLayerVar(){
	if(typeof window["analytics_mmb_dl_var"] == "object"){
		addAnalyticsData(window["analytics_mmb_dl_var"]);
	}
}
function initMmb_tracking(){
	jQuery(".j-analytics-mmb-click").off("mousedown", onMmbTrackingClick);
	jQuery(".j-analytics-mmb-click").on("mousedown", onMmbTrackingClick);
	jQuery(".j-analytics-mmb-cc").off("mousedown", onMmbCCSelected);
	jQuery(".j-analytics-mmb-cc").on("mousedown", onMmbCCSelected);
}

function mmbThankyou_setMeta(){
	var paymentType = getURLParameter("type");
	var paymentTypeMeta = paymentType == "free" 
		? "riepilogoservizigratuiti" : "ricevutaserviziapagamento"
	var origin = getURLParameter("origin");
	var originMeta = origin == "typ" ? "ThankYouPage" : "ManageMyBooking"
	jQuery("meta[name='DCSext.arsummary']").attr("content", originMeta);
	jQuery("meta[name='DCSext.mmb_v2_" + paymentTypeMeta + "']").attr("content", "1");
	if(paymentType == "free" ){
		/*remove unnecessary meta*/
		jQuery("meta[name='WT.si_n']").remove();
		jQuery("meta[name='WT.si_x']").remove();
	}
}

/**
 * selezione carta di pagamento
 */
function onMmbCCSelected(){
	jQuery("meta[name='WT.si_x']").attr("content", "2");
}

/**
 * click sui tasti dei box
 * e' possibile scatenare piu' di un evento separandoli con 
 * una virgola nell'attributo data-evt
 */
function onMmbTrackingClick(){
	var evtNames = $(this).attr("data-evt").split(",");
	var stepNames = $(this).attr("data-step").split(",");
	var boxNames = $(this).parents(".j-analytics-mmb-box").attr("data-box").split(",");
	for(ind in evtNames){
		var evtName = evtNames[ind];
		var stepName = stepNames[ind % stepNames.length];
		var boxName = boxNames[ind % boxNames.length];
		if(typeof window["mmb_dcs_multitrack_btn_click"] != "undefined"){
			mmb_dcs_multitrack_btn_click(evtName, stepName, boxName);
		}
	}
}


function onMmbTrackingPurchasedItemsInfo(){
	if(typeof window["mmbAnalytics_purchasedProducts"] != "undefined"){
		jQuery("meta[name='DCSext.mmb_v2_quantitaemd']").attr("content", mmbAnalytics_emdQuantity);
		jQuery("meta[name='DCSext.arproduct']").attr("content", mmbAnalytics_purchasedProducts);
		jQuery("meta[name='DCSext.arquantity']").attr("content", mmbAnalytics_purchasedQuantities);
		jQuery("meta[name='WT.tx_u']").attr("content", mmbAnalytics_purchasedQuantities);
		jQuery("meta[name='WT.pn_sku']").attr("content", mmbAnalytics_purchasedProducts);
		jQuery("meta[name='WT.tx_s']").attr("content", mmbAnalytics_purchasedPrices);
		jQuery("meta[name='WT.tx_i']").attr("content", mmbAnalytics_invoiceNumber);
		jQuery("meta[name='WT.tx_id']").attr("content", mmbAnalytics_purchaseDate);
		jQuery("meta[name='WT.tx_it']").attr("content", mmbAnalytics_purchaseTime);
	}
}