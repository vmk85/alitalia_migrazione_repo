var paymentOptions = {};
var selectedPaymentMethod = "";
var isCreditCardValid = false;
var validateOnKeyPress;
var params = {}, queries, temp, i, l;
// Split into key/value pairs
queries = window.location.search.split("&");
// Convert the array of strings into an object
for ( i = 0, l = queries.length; i < l; i++ ) {
    temp = queries[i].split('=');
    params[temp[0]] = temp[1];
}
var editBp = params["editBp"];
var flight = params["flight"];
var paymentErrors = "";
var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
var isNotFirstSelectMobile = false;

$(document).ready(function () {
    performSubmit('checkingetcart', '#frmCart', loadCartSuccess, loadCartFail);

    if (editBp == "true"){
        $("#hrefButton").attr("href",$("#hrefButton").attr("href")+"?&editBp=true&flight="+flight);
        $("#hrefButton2").attr("href",$("#hrefButton2").attr("href")+"?&editBp=true&flight="+flight);
    }

    $("input[name='maUserMemorizzaCarta']").click(function(){
        if($(this).val() == "checked"){
            $(this).val("");
        } else {
            $(this).val("checked");
        }
    });

    selectedPaymentMethod = "visa";
    paymentOptions.initRadio();

    var requestData = {
        'countries': ''
    };

    $(".checkValidCC").css('background-image','url("/etc/designs/alitalia/clientlibs-checkin-pay-1-embed/images/mc_ok_bw.png")');
    $(".checkValidCC").css('background-position','right center');
    $(".checkValidCC").css('background-repeat','no-repeat');

    addKeyPressListeners();

$(".countryList option[value=HK]").html(CQ.I18n.get("countryData.HK"));
$(".countryList option[value=MO]").html(CQ.I18n.get("countryData.MO"));
$(".countryList option[value=TW]").html(CQ.I18n.get("countryData.TW"));

    $('.reveal-wrap').foundation();

    if($(".radioCC").length > 0) $(".radioCC:eq(0) > .placeholder").trigger("click");

});

function addKeyPressListeners(){
    validateOnKeyPress = $('#form_'+selectedPaymentMethod).hasClass('validateOnKeyPress') ? true : false;

    $('#form_'+selectedPaymentMethod+' .checkValidCC').keyup(function(){
        var result = $(this).validateCreditCard();

        var strTest=selectedPaymentMethod;

        if(selectedPaymentMethod == "visa_electron"){
            strTest = "visaelectron";
        }
        if(selectedPaymentMethod == "american_express"){
            strTest = "americanexpress";
        }
        if(selectedPaymentMethod == "diners_club"){
            strTest = "diners";
        }


        var strCC=(result.card_type == null ? 'error' : result.card_type.name);

        var isValid=(result.valid && result.length_valid && result.luhn_valid && (strCC==strTest));
        //$('.log').html((isValid ? '&nbsp;' : 'Errore'));

        if (isValid) {
            isCreditCardValid = true;
            $(this).css('background-image','url("/etc/designs/alitalia/clientlibs-checkin-pay-1-embed/images/mc_ok.png")');
            $(this).css('background-position','right center');
            $(this).css('background-repeat','no-repeat');
        }else{
            isCreditCardValid = false;
            $(this).css('background-image','url("/etc/designs/alitalia/clientlibs-checkin-pay-1-embed/images/mc_ok_bw.png")');
            $(this).css('background-position','right center');
            $(this).css('background-repeat','no-repeat');
        }

        if(validateOnKeyPress){
            if(isCreditCardValid){
                hideCurrentFeedbackErrors($(this));
            }else{
                showCurrentFeedbackErrors($(this));
            }
        }

    });

    $('#form_'+selectedPaymentMethod+' .pay-isRequired').keyup(function(){
        if(validateOnKeyPress){
            if($(this).prop('type')=='email'){
                if(isEmailValid()){
                    hideCurrentFeedbackErrors($(this));
                }else{
                    showCurrentFeedbackErrors($(this));
                }
            }else{
                if($(this).val()){
                    hideCurrentFeedbackErrors($(this));
                }else{
                    showCurrentFeedbackErrors($(this));
                }
            }
        }
    });
}


function getDropdownDataCheckIn(requestData, done, fail, always) {
    return invokeGenericFormService('staticdatalistservlet', 'GET', '#', done, fail, always, 'json', requestData);
}

function fillDropDownDone(data) {
    var states = data.states;
    for (index in states) {
        var value = states[index].code;
        var descr = states[index].name;
        dropdownAppendWithPreselection("nation", value, descr);
    }
}

function fillDropDownFail() {
    return;
}

function dropdownAppendWithPreselection(dropdownType, value, descr) {
    var selectedOption = '<option selected="selected" value="' + value + '">' + descr + '</option>';
    var option = '<option value="' + value + '">' + descr + '</option>';
    $("select[data-dropdown='" + dropdownType + "']").each(
        function (ind, elem) {
            if ($(elem).attr("data-preselection") === value) {
                $(elem).append(selectedOption);
            }
            else {
                $(elem).append(option);
            }
        });
}

paymentOptions.initRadio = function () {
//    if ($('.payment-methods .radio-wrap input:checked').length) {
//        $('.payment-methods .radio-wrap input:checked').closest('.payment-methods .radio-wrap').addClass('checked');
//    }

    $(document).on('click', '.payment-methods .radio-wrap .placeholder', function () {
        $(this).parent().find('input').click();
    });

    $(document).on('click', '.payment-methods .radio-wrap input', function () {
        $('.payment-methods .radio-wrap input[name=' + $(this).attr('name') + ']').closest('.payment-methods .radio-wrap').removeClass('checked');
        $(this).closest('.payment-methods .radio-wrap').addClass('checked');
        var formCC = $(this).attr("data-target");
        $("#" + formCC + " *> input, select").not(':input[type=button]').val("");
        selectedPaymentMethod = $(this).closest('.payment-methods .creditcard-inner').find('.payment-type').attr('class');
        selectedPaymentMethod = selectedPaymentMethod.replace("payment-type", "").trim();
        $('#form_'.concat(selectedPaymentMethod)).removeClass('hide');
        $('#form_'.concat(selectedPaymentMethod)).show();
        $('#form_'.concat(selectedPaymentMethod)).siblings('[id^="form"]').addClass('hide');
    	//Tolentino - Inizio
        $('#form_checkin_acquista_myalitalia_oneclick').removeClass('hide');
    	//Tolentino - Fine
    	$("[name='cardBin").val("");

        if($(this).attr("data-pay") == null) {
            $(".btnPay").addClass("hide");
            $("#form_".concat(selectedPaymentMethod).concat(" *> .btnPay")).removeClass("hide");
        } else if($(this).attr("data-pay") == "ma") {
            $(".btnPay").addClass("hide");
            var idx = $(this).attr("idx");
            if(idx != null) {
                $("#btnPay-".concat(idx).concat(" > input")).removeClass("hide");
            }
        }

        addKeyPressListeners();
    });

	//Tolentino - Inizio
    $('input[name="switch-payment-cardmaoc"]').change(function() {
        $('.payment-methods .radio-wrap').closest('.payment-methods .radio-wrap').removeClass('checked');

        $(this).closest('.payment-methods .radio-wrap').addClass('checked');
        $('input[name="idPagamento"]').val($(this).val());
    	$('input[name="maocCardSecurityCode"]').val('');
        $("#" + $(this).attr("data-target")).toggle(true);
		selectedPaymentMethod = "checkin_acquista_myalitalia_oneclick";

		$('#form_'.concat(selectedPaymentMethod)).removeClass('hide');
		$('#form_'.concat(selectedPaymentMethod)).siblings('[id^="form"]').addClass('hide');
        $('#form_checkin_acquista_myalitalia_oneclick').removeClass('hide');

    	$('input[name="switch-payment"]').attr("checked", false);
        $('input[name="switch-payment"]').each(function() {
        	var switchPaymentRadio = $(this).attr("data-target");
        	$('#' + switchPaymentRadio).toggle(false);
        });

        var bin = $(this).attr("data-bin");
        if(bin != null && bin.length > 0) $("[name='cardBin']").val(bin);

        var id = $(this).attr("id");
        if(id != null && id.length) scrollTo(id);
    });

    $('input[name="switch-payment"]').on("click", function() {

//        $(".radioCC").removeClass("checked");

        if(utilsCC.appCvcForm != null) {
            $("#cvcForm").html(utilsCC.appCvcForm);
            utilsCC.appCvcForm = null;
        }

        $('.payment-methods .radio-wrap').closest('.payment-methods .radio-wrap').removeClass('checked');

		$(this).closest('.payment-methods .radio-wrap').addClass('checked');
		$('#' + $(this).attr("data-target")).toggle(true);
		selectedPaymentMethod = $(this).closest('.payment-methods .creditcard-inner').find('.payment-type').attr('class');
		selectedPaymentMethod = selectedPaymentMethod.replace("payment-type", "").trim();

		$('#form_'.concat(selectedPaymentMethod)).removeClass('hide');
		$('#form_'.concat(selectedPaymentMethod)).siblings('[id^="form"]').addClass('hide');
        $('#form_checkin_acquista_myalitalia_oneclick').removeClass('hide');

    	$('input[name="idPagamento"]').val('');
    	$('input[name="maocCardSecurityCode"]').val('');
    	var switchPaymentCardmaocRadio = $('input[name="switch-payment-cardmaoc"]').attr("data-target");
    	$('#' + switchPaymentCardmaocRadio).toggle(false);
    	$('input[name="switch-payment-cardmaoc"]').prop("checked", false);
        $('.payment-methods .radio-wrap input[name="switch-payment-cardmaoc"]').closest('.payment-methods .radio-wrap').removeClass('checked');
        $(".btnPay > input").addClass("hide");
    });

    if (selectedPaymentMethod && !isMobile) {
    	$('#' + selectedPaymentMethod).click();
    }
	//Tolentino - Fine

};

function beirut1(callback) {
    $("#" + beirutStringID).val("");
    var resource = getServiceUrl("resourceconsumer", "png");
    nsbr.setResource(resource, true,
        function () {
            nsbr.pasedina(beirutStringID);
            var beirutString = $("#" + beirutStringID).val();
            callback(beirutString);
        });
}

function pay() {
    $('.pay-desktop').off('click', pay);
    $('.pay-mobile').off('click', pay);
    enableLoaderCheckin();
    validatePay();
}

function validatePay() {

    var fieldsValid = true;
    paymentErrors = "";
    $('#form_'+selectedPaymentMethod+' .pay-isRequired').each(function(i, obj){
        if($(obj).prop('type')=='email'){
            if(isEmailValid()){
                hideCurrentFeedbackErrors($(obj));
            }else{
                disableLoaderCheckin();
                fieldsValid = false;
                showCurrentFeedbackErrors($(obj));
            }
        }


        if($(obj).prop('name')=='cardHolderFirstName'){
            if(isCredentialValid($(obj).val())){
                hideCurrentFeedbackErrors($(obj));
            }else{
                disableLoaderCheckin();
                fieldsValid = false;
                showCurrentFeedbackErrorsNameSurname($(obj));
            }
        }

        if($(obj).prop('name')=='cardHolderLastName'){
            if(isCredentialValid($(obj).val())){
                hideCurrentFeedbackErrors($(obj));
            }else{
                disableLoaderCheckin();
                fieldsValid = false;
                showCurrentFeedbackErrorsNameSurname($(obj));
            }
        }

        if($(obj).prop('name')=='cardSecurityCode'){
            var validCVV = validateCVV($(obj).val(),selectedPaymentMethod)
            if(validCVV){
                hideCurrentFeedbackErrors($(obj));
            }else{
                disableLoaderCheckin();
                fieldsValid = false;
                showCurrentFeedbackErrorsCVV($(obj),validCVV);
            }
        }

    	//Tolentino - Inizio
        if($(obj).prop('name')=='maocCardSecurityCode'){
            var validCVV = validateCVV($(obj).val(),selectedPaymentMethod)
            if(validCVV){
                hideCurrentFeedbackErrors($(obj));
            }else{
                disableLoaderCheckin();
                fieldsValid = false;
                showCurrentFeedbackErrorsCVV($(obj),validCVV);
            }
        }
    	//Tolentino - Fine


        if(!$(obj).val()){
            disableLoaderCheckin();
            fieldsValid = false;
            showCurrentFeedbackErrors($(obj));
        }
    });

	//Tolentino - Inizio
   	if (selectedPaymentMethod != "checkin_acquista_myalitalia_oneclick") {
   	//Tolentino - Fine
   	    if(isCreditCardValid){
   	        hideCurrentFeedbackErrors($('#form_'+selectedPaymentMethod).find('.checkValidCC'));
   	    }else{
   	        disableLoaderCheckin();
   	        fieldsValid = false;
   	        showCurrentFeedbackErrors($('#form_'+selectedPaymentMethod).find('.checkValidCC'));
   	    }
 	//Tolentino - Inizio
   	}
	//Tolentino - Fine

    if(fieldsValid){
        //hideFeedBackErrors();
        //se ok
        beirut1(
            function(beirutString){
                pay2(beirutString);
            }
        );
    }else{
        disableLoaderCheckin();
        $('#form_'+selectedPaymentMethod).addClass('validateOnKeyPress');
        validateOnKeyPress = true;
        //showFeedbackErrors();
        dataLayer[1].checkinError = paymentErrors;
        if(typeof window["_satellite"] !== "undefined"){
        	_satellite.track("CIError");
        } else {
        	console.error("Cannot activate CIError. _satellite object not found");
        }
        dataLayer[1].checkinError = "";

    }

}

function scrollTo (id) {
   $('html,body').animate({
      scrollTop: $("#"+id).offset().top - $(window).height()/2
   }, 1000);
}

function validateCVV(value,creditcard){

    var cvvLengthRequired = 3;
    if(creditcard == "american_express"){
        cvvLengthRequired = 4;
    }
    if(value.length == cvvLengthRequired){
        return true;
    }else{
        return false;
    }

}

function isCredentialValid(string) {

    if(string.search( new RegExp("\[0-9]")) > -1){
        return false;
    } else {
        return true;
    }

}

function isEmailValid(){
    var email = $('#form_'+selectedPaymentMethod).find('input[name="email"]').val();
    return isEmail(email);
}

function pay2(beirutString) {
    if ($('#form_' + selectedPaymentMethod + " input[name='maUserMemorizzaCarta']").val() == "checked"){
        encryptDataAdyenCard('form_' + selectedPaymentMethod );
    }

    $('#form_' + selectedPaymentMethod + " input[name='beirutString']").val(beirutString);
    performSubmit('checkininitpayment', "#form_".concat(selectedPaymentMethod), paySuccess, payFail);
}

function paySuccess(data) {
    if(data.isError) {
        if(data.redirectPage === "") {
            // reveal popup di errore
            $('#error').html(data.errorMessage);
            $("#pay-error-anchor").trigger("click");

            // tracciamento errore pagamento
            disableLoaderCheckin();
            dataLayer[1].checkinError = data.originalErrorMessage;
            if(typeof window["_satellite"] !== "undefined"){
            	_satellite.track("CIError");
            } else {
            	console.error("Cannot activate CIError. _satellite object not found");
            }
            dataLayer[1].checkinError = "";
        } else {
            window.location.replace(data.redirectPage+"?payment=true"+(editBp == "true"? "&editBp=true&flight="+flight:""));
        }
    } else {
        if($('#insuranceAmount').length){
            var insuranceAmount = "1:" + $('#insuranceAmount').text() + ":INS";
            updateDataLayer('insurance',insuranceAmount);
        }
        updateDataLayer('revenueAncillary',$('#binding-currency').data('price'));
        getBaggageAmount();
        getSeatAmount();
        getLoungeAmount();
        getFastTrackAmount();
        updateDataLayer('paymentType','CreditCard;' + selectedPaymentMethod);
        if (data.successPage) {
            window.location.replace(data.successPage+"?&payment=true"+(editBp == "true"? "?&editBp=true&flight="+flight:""));
        } else if (data.redirectPage) {
            $.ajax({
                url: data.redirectPage, //'http://localhost:11111/content/alitalia/master-it/it/check-in-search/.checkinredirect3ds',
                method: 'POST',
                async: false,
                context: document.body,
                success: function(data) {
                    if (data.successPage) {
                        window.location.replace(data.successPage+"?$payment=false"+(editBp == "true"? "?&editBp=true&flight="+flight:""));
                    }
                },
                error: function() {
                    console.log("errore");
                }
            })
        }
    }
}

function payComplete(data) {
    if (data.method == 'GET') {
        //window.location.replace(data.redirect);
    } else {
        var newForm = $('<form>', {
            'action': data.redirect,
            'method': data.method
        })
        for (var key in data.params) {
            if (data.params.hasOwnProperty(key)) {
                newForm.append($('<input>', {
                    'name': key,
                    'value': data.params[key],
                    'type': 'hidden'
                }));
            }
        }
        newForm.append($('<input>', {
            'name': 'submitForm',
            'value': 'Click me',
            'type': 'submit'
        }));
        newForm.appendTo("body");
        newForm.submit();
    }
}

function payFail() {
    disableLoaderCheckin();
}

function showFeedbackErrors(){
    $('#form_'+selectedPaymentMethod).find('.feedback-error').removeClass('hide');

}
function hideFeedBackErrors(){
    $('#form_'+selectedPaymentMethod).find('.feedback-error').addClass('hide');
}

function showCurrentFeedbackErrors(el){
    $(el).parent().find('.feedback-error').removeClass('hide');
    $(el).attr("style",'border-color:red');
    if(paymentErrors.length == 0){
        paymentErrors = $(el).parent().find('.feedback-error').text();
    }else{
        paymentErrors = paymentErrors + "; " + $(el).parent().find('.feedback-error').text();
    }

}

function showCurrentFeedbackErrorsNameSurname(el){
    if($(el).val() != ''){
        $(el).parent().find('.feedback-error').text(CQ.I18n.get('checkin.common.invalidData.label'));
    }
    $(el).parent().find('.feedback-error').removeClass('hide');
    $(el).attr("style",'border-color:red');
    if(paymentErrors.length == 0){
        paymentErrors = $(el).parent().find('.feedback-error').text();
    }else{
        paymentErrors = paymentErrors + "; " + $(el).parent().find('.feedback-error').text();
    }

}

function showCurrentFeedbackErrorsCVV(el,validCVV){
    if(!validCVV){
        $(el).parent().find('.feedback-error').text(CQ.I18n.get('checkin.common.genericInvalidField.label'));
    }
    $(el).parent().find('.feedback-error').removeClass('hide');
    $(el).attr("style",'border-color:red');
    if(paymentErrors.length == 0){
        paymentErrors = $(el).parent().find('.feedback-error').text();
    }else{
        paymentErrors = paymentErrors + "; " + $(el).parent().find('.feedback-error').text();
    }

}
function hideCurrentFeedbackErrors(el){
    $(el).parent().find('.feedback-error').addClass('hide');
    $(el).attr("style",'');

}

function encryptDataAdyenCard(idFormFrom) {
    var options = {}; // See adyen.encrypt.nodom.html for details

    var formFrom = document.getElementById(idFormFrom);
    var AdyenCryptingKey=$("#" + formFrom.id + " [name='adyen-encrypted-data']").attr("data-key-encrypt");

    var cseInstance = adyen.encrypt.createEncryption(AdyenCryptingKey, options);

    var postData = {};

    var cardData = {
        number : formFrom.elements["cardNumber"].value,
        cvc : formFrom.elements["cardSecurityCode"].value,
        holderName : formFrom.elements["cardHolderFirstName"].value + " " + formFrom.elements["cardHolderLastName"].value,
        expiryMonth : formFrom.elements["expireMonth"].value,
        expiryYear : formFrom.elements["expireYear"].value,
        generationtime: $("#" + formFrom.id + " [data-encrypted-name='generationtime']").val()
    };

    postData['adyen-encrypted-data'] = cseInstance.encrypt(cardData);

    $("#" + formFrom.id +" [name='adyen-encrypted-data']").val(postData['adyen-encrypted-data']);
}

$('.select-card').find('select').change(function(){
    var ccSelected = $('.select-card').find('select').val();
    if(isNotFirstSelectMobile){
        $('#form_' + selectedPaymentMethod).addClass('hide');
    }else{
        isNotFirstSelectMobile = true;
    }
    selectedPaymentMethod = ccSelected;
    $('#form_' + ccSelected).removeClass('hide');
});