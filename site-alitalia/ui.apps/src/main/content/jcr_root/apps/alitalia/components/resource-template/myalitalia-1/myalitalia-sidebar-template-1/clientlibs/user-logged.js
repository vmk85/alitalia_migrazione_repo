var hidden = [];
var uid;
var VAL_STEP = 20;

$(document).ready(function() {
    // try {
    //     getUserMAInfo(function(resp) {
    //         userMA = resp;
    //         if(userMA.errorCode == 0) {
    //             // MAUser();
    //         } else if(userMM && userMM.personal.firstname != "") {
    //             // MMUser();
    //         } else {
    //             location.href = "./myalitalia-registrati.html";
    //         }
    //     });
    // } catch(err) {}
    mmSupport.init();
});

// function MMUser() {
//     var data = CQ_Analytics.ProfileDataMgr.data;
//     $(".millemiglia").css("display", "block");
//     $(".user-data").css("display", "block");
//     checkProfileMMState(data);
// 	$(".userName").html(capitalizeName(data.customerName) + " " + capitalizeName(data.customerSurname));
//     $(".userImg").attr("src", (data.avatar)?data.avatar:"/etc/designs/alitalia/clientlibs-myalitalia/images/avatar.jpg");
//     $("span.value").html(parseInt(data.milleMigliaTotalMiles).toLocaleString());
//     var level = getMMLevel(data.milleMigliaTierCode);
//     $(".special-freccia-alata").html("<img src=" + "/etc/designs/alitalia/clientlibs-myalitalia/images/ico-step-" + level.lvl + ".svg> " + level.value);
//     // gigya.accounts.isAvailableLoginID({
//     //     loginID: userEmail,
//     //     callback: function(resp) {
//     //         if(!resp.isAvailable)
//     //             $(".accCon").css("display", "block");
//     //     }
//     // });
//     /*
//     if(userMM.personal.address.type == "Business") {
//     	$(".radio-wrap:eq(1)").find("span").trigger("click");
//     }*/
// }

function getMMLevel(tier) {
    switch(tier) {
        case "Basic":
            return {lvl: 1, value: "Basic", ico: "/etc/designs/alitalia/clientlibs-myalitalia/images/ico-freccia-alata.svg"};
        case "Ulisse":
            return {lvl: 2, value: "Ulisse", ico: "/etc/designs/alitalia/clientlibs-myalitalia/images/ico-freccia-alata.svg"};
        case "FrecciaAlata":
            return {lvl: 3, value: "Freccia Alata", ico: "/etc/designs/alitalia/clientlibs-myalitalia/images/ico-freccia-alata.svg"};
        case "Plus":
            return {lvl: 4, value: "Plus", ico: "/etc/designs/alitalia/clientlibs-myalitalia/images/ico-freccia-alata.svg"};
        default:
            return 0;
    }
}



function closeTutorial(name) {
    try {
        var hidden = ((getCookie("hidden-elements:" + userMA.UID)!="")?JSON.parse(getCookie("hidden-elements:" + userMA.UID)):null);
        var found = false;
        if(hidden != null) {
            hidden.forEach(function(item, index) {
                if(item.name == name) {
                    item.value = true;
                    found = true;
                }
            });
            if(!found) {
                var el = {
                    name: name,
                    value: true
                };
                hidden.push(el);
                setCookie("hidden-elements:" + userMA.UID, JSON.stringify(hidden), (365 * 10));
            }
        } else {
            var el = {
                name: name,
                value: true
            };
            hidden = el;
            setCookie("hidden-elements:" + userMA.UID, JSON.stringify(hidden), (365 * 10));
        }
    } catch (err) {
    }
}

function checkHiddenElements() {
    var hidden = (getCookie("hidden-elements:" + userMA.UID)!=null?JSON.parse(getCookie("hidden-elements:" + userMA.UID)):null);
    if(hidden != null) {
    	hidden.forEach(function(item, index) {
			if(item.value)
        	    $("#" + item.name).hide();
    	});
    }
}

function setCookie(nomeCookie,valoreCookie, days) {
  var scadenza = new Date();
  var adesso = new Date();
  scadenza.setTime(adesso.getTime() + (days * (24*60*60*1000)));
  document.cookie = nomeCookie + '=' + escape(valoreCookie) + '; expires=' + scadenza.toGMTString() + '; path=/';
}

function getCookie(nomeCookie) {
  if (document.cookie.length > 0)
  {
    var inizio = document.cookie.indexOf(nomeCookie + "=");
    if (inizio != -1)
    {
      inizio = inizio + nomeCookie.length + 1;
      var fine = document.cookie.indexOf(";",inizio);
      if (fine == -1) fine = document.cookie.length;
      return unescape(document.cookie.substring(inizio,fine));
    }else{
       return null;
    }
  }
  return null;
}

function deleteCookie(nomeCookie) {
  setCookie(nomeCookie,'',-1);
}

// function logoutWithOpenModal() {
//     setCookie("openModal", "login", 1);
//     gigya.accounts.logout();
// }