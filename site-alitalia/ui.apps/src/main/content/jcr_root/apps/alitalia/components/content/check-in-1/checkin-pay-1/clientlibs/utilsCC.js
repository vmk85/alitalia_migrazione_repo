utilsCC = {};

utilsCC.allRadio = ".radioCC";
utilsCC.appCvcForm = null;

$(".radioCC .placeholder").on("click", function() {
    var idx = $(this).attr("idx");
    var current = "#radio-" + idx;

    // Rimuovo il campo "checked" a tutti i radio de 'Le mie Carte di Credito'
    $(".radioCC").removeClass("checked");
    $(".radioCC *> input").attr("checked", false);

    // Rimuovo tutti i pulsanti paga adesso
    $(".btnPay > input").addClass("hide");

    // Setto i campi necessari per il radio selezionato
    $(current).addClass("checked");
    $(current + " > input").attr("checked", true);

    if(utilsCC.appCvcForm == null) {
        utilsCC.appCvcForm = $("#cvcForm > label").clone();
        $("#cvcForm > label").remove();
    }
    $("#cvcForm-" + idx).html(utilsCC.appCvcForm);
    $("#btnPay-" + idx + " > input").removeClass("hide");
});

utilsCC.myCCNoteSelected = function() {
    $.each($(".radioCC"), function(i, el) {
        var idx = $(el).attr("idx");
        $(el).removeClass("checked");
        $(el).find("input").attr("checked", false);
        $("#cvcForm-" + idx).addClass("hide");
        $(".btnPay > input").addClass("hide");
    });
}