$("#booking-dati-ecoupon-submit").unfastClick().fastClick(eCouponSubmit);

$("#booking-dati-ecoupon-clear").unfastClick().fastClick(eCouponClear);

$("#booking-dati-ecoupon-modify-search").unfastClick().fastClick(eCouponModifySearch);

function eCouponSubmit(e) {
	e.preventDefault();
	performValidation('bookingcouponconsumer', '#form-ecoupon', successApplyCouponValidation);
}

function eCouponClear(e) {
	e.preventDefault();
	performValidation('bookingclearcouponconsumer', '#form-ecoupon', successClearCouponValidation);
}


function eCouponModifySearch(e) {
	e.preventDefault();
	$(".bookInfoBoxItinerary__modify.j-openFlightFinder").click();
}

function successApplyCouponValidation(data) {
	$("#inputEcoupon").removeClass('error-form');
	if (data.result) {
		// TODO [ecoupon] show loading feedback
		performSubmit('bookingcouponconsumer', '#form-ecoupon', successCouponProcessing, failureCouponProcessing);
	} else {
		showErrors(data);
	}
}

function successClearCouponValidation(data) {
	$("#inputEcoupon").removeClass('error-form');
	if (data.result) {
		// TODO [ecoupon] show loading feedback
		performSubmit('bookingclearcouponconsumer', '#form-ecoupon', successCouponProcessing, failureCouponProcessing);
	} else {
		showErrors(data);
	}
}

function successCouponProcessing(data) {
	// TODO [ecoupon] dismiss loading feedback
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	}
	// invoke a callback in external component, if defined, to reload 
	// all the affected partials (including the e-coupon one)
	if (window['couponProcessedCallback']) {
		window['couponProcessedCallback'](data);
	} else {
		console.info("No callback function found by ecoupon component in the external component.");
	}
}

function failureCouponProcessing() {
	// TODO [ecoupon] dismiss loading feedback
	console.error("failureCouponProcessing");
}


/* FanPlayr */ 

/**
 * Invocato da FanPlayr all'applicazione di un codice sconto
 */
function AddFanplayrCoupon(couponNumber) {
	var data = {
		fanplayr: 'true',
		inputEcoupon: couponNumber
	};
	// TODO [ecoupon] show loading feedback
	performSubmit('bookingcouponconsumer', data, successCouponProcessing, failureCouponProcessing);
}
