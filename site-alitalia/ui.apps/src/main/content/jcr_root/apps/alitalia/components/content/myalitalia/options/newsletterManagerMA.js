"use strict";
use(function() {
    var comunicazione = this.comunicazioni;
    var response = {};
    response.email = "";
    response.app = "";
    response.sms = "";
    if (comunicazione != null){
        for (var a = 0; a < comunicazione.size(); a++){
            if (comunicazione.get(a).tipo.toUpperCase() == "MAIL"){
                response.email = comunicazione.get(a).selezionato;
            } else if (comunicazione.get(a).tipo.toUpperCase() == "PUSH"){
                response.app = comunicazione.get(a).selezionato;
            } else if (comunicazione.get(a).tipo.toUpperCase() == "SMS"){
                response.sms = comunicazione.get(a).selezionato;
            }
        }
    }
    return response;
})