use(function () {
    var maxItems = parseInt(this.bagsMax) || 0;
    var allowedItems = parseInt(this.bagsAllow) || 0;
    var bagsPurchased = parseInt(this.bagsPurchased) || 0;
    var totalBags = allowedItems + bagsPurchased;

    return {
        totalBags: totalBags,
        maxitems: (maxItems - totalBags),
    };
});