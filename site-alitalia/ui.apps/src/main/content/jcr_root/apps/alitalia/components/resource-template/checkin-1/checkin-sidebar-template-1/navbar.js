use(function () {
    var currentPage = this.current;

    var urlSearch = this.link + ".html";
    var urlFlights = this.link + "/check-in-flights-list.html";
    var urlFaq = this.link + "/check-in-faq.html";

    var searchPage = false;
    var flightsPage = false;
    var FaqPage = false;

    if(currentPage == urlSearch){
        searchPage = true;
    }

    if(currentPage == urlFlights){
        flightsPage = true;
    }

    if(currentPage == urlFaq){
        FaqPage = true;
    }

    return {
        currentPage: currentPage,
        urlSearch: urlSearch,
        searchPage: searchPage,
        flightsPage: flightsPage,
        FaqPage: FaqPage
    };
});