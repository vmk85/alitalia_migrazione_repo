//TODO fix funzionalita' initSlider e equalizeSliderHeight

var listaOfferteOptions = {};


// document ready
$( document ).ready( function() {

    var originCity = {};
    originCity.dest = null;
    if (typeof localStorage !== "undefined"
        && typeof (Storage) !== "undefined") {
        try {
            var items = localStorage.getItem("ricercheRecenti");
            if (items != null && items != 'undefined' && items != '[]'
                && items.length > 0) {
                items = JSON.parse(items);
                var airportCode = items[0].cittaPartenza.codiceAeroporto;
                originCity = {
                    dest : airportCode
                };
            }
        } catch (e) {}

    }
    listaOfferteOptions.getOriginList(originCity);
    $(document).on('focus', '.lista-offerte__select .select--departure', function() {
		$(this).closest('.current-departure').addClass('on-focus');
	});
	$(document).on('blur', '.lista-offerte__select .select--departure', function() {
		$(this).closest('.current-departure').removeClass('on-focus');
	});
} );

// window load
$( window ).on( 'load', function() {

    if( Foundation.MediaQuery.atLeast( 'large' ) ) {
        listaOfferteOptions.setAccordionDesktop();
        listaOfferteOptions.equalizeAccordionHeight();
    } else {
        listaOfferteOptions.setAccordionMobile();
    }


} );

$( window ).on( 'resize', function() {

    setTimeout( function() {
        listaOfferteOptions.equalizeSliderHeight();

        if( Foundation.MediaQuery.atLeast( 'large' ) ) {
            listaOfferteOptions.equalizeAccordionHeight();
        }
    }, 100 );

} );

// change media query
$(window).on('changed.zf.mediaquery', function(event, name) {

    // If breakpoint is large and up, reinit the tabs
    if( Foundation.MediaQuery.atLeast( 'large' ) ) {
        // accordion
        listaOfferteOptions.setAccordionDesktop();
    } else {
        // slider
        setTimeout( function() {
            listaOfferteOptions.slider.slideTo( 0, 0 );
        }, 1 );
        // accordion
        listaOfferteOptions.setAccordionMobile();
        // remove accordion equalizer
        $( '.lista-offerte__accordion .lista-destinazioni__item' ).removeAttr( 'style' );
    }

});

listaOfferteOptions.displaySelectValue = function() {
    $( '.lista-offerte__select .current-departure__content' ).html(
        $( '.lista-offerte__select .select--departure option:selected' ).html()
    );
};

listaOfferteOptions.initSlider = function() {
    if( listaOfferteOptions.slider !== undefined ) {
        listaOfferteOptions.slider.destroy();
    }

    listaOfferteOptions.slider = new Swiper( '.lista-offerte__slider .swiper-container', {
        slidesPerView: 'auto',
        resistanceRatio: 0,
        pagination: {
            el: '.lista-offerte__slider .swiper-pagination',
            clickable: true
        }
    } );

    $( '.lista-offerte__slider .bg-image' ).foundation();
};

listaOfferteOptions.equalizeSliderHeight = function() {
    $( '.lista-offerte__slider .custom-card__section--text' ).removeAttr( 'style' );
    listaOfferteOptions.cardMaxHeight = $( '.lista-offerte__slider .swiper-wrapper' ).outerHeight() - $( '.lista-offerte__slider .custom-card__section.bg-image' ).outerHeight();
    $( '.lista-offerte__slider .custom-card__section--text' ).css( 'height', listaOfferteOptions.cardMaxHeight );
};

listaOfferteOptions.equalizeAccordionHeight = function() {
    $( '.lista-offerte__accordion .lista-destinazioni__item' ).removeAttr( 'style' );
    listaOfferteOptions.accordionItemMaxHeight = 0;
    // $( '.lista-offerte__accordion .lista-destinazioni__item' ).removeAttr( 'style' );
    $( '.lista-offerte__accordion .lista-destinazioni__item' ).each( function( i, el ) {
        if( $( el ).outerHeight() > listaOfferteOptions.accordionItemMaxHeight ) {
            listaOfferteOptions.accordionItemMaxHeight = $( el ).outerHeight();
        }
    } );
    $( '.lista-offerte__accordion .lista-destinazioni__item' ).css( 'height', listaOfferteOptions.accordionItemMaxHeight );
};

listaOfferteOptions.initAccordion = function(cb) {
    $( '.lista-offerte__accordion .accordion' ).foundation();
    listaOfferteOptions.accordion = new Foundation.Accordion( $( '.lista-offerte__accordion .accordion' ), {
        // slideSpeed: 0,
        multiExpand: false,
        allowAllClosed: true,
    } );

    if( typeof cb == 'function' ) {
        cb();
    }
};

listaOfferteOptions.setAccordionDesktop = function() {
	try{
		$( '.lista-offerte__accordion .accordion' ).foundation( 'down', $( '.lista-offerte__accordion .accordion-content' ) );
	    $( '.lista-offerte__accordion .accordion' ).attr( 'disabled', true );
	    //Valorizzo con codice iata la partenza per corretta redirect sulle offerte
	    $(".current-departure option:contains("+$('.current-departure__content').html()+")").attr('selected', 'selected');
	} catch (err) {
		console.log('listaOfferteOptions.setAccordionDesktop: '+err);
	}
    
};

listaOfferteOptions.setAccordionMobile = function() {
	try{
	    $( '.lista-offerte__accordion .accordion' ).removeAttr( 'disabled' );
	    $( '.lista-offerte__accordion .accordion' ).foundation( 'up', $( '.lista-offerte__accordion .accordion-content' ) );
	    // $( '.lista-offerte__accordion .accordion' ).foundation( 'down', $( '.lista-offerte__accordion .accordion-item:first-child .accordion-content' ) );
	} catch (err) {
		console.log('listaOfferteOptions.setAccordionMobile: '+err);
	}
};

listaOfferteOptions.getOriginList = function (originCity) {
    var dest=originCity.dest || "FCO";
    // get new destionation slider
    $.ajax({
        type: "GET",
        url: location.protocol + '//' + location.host + internalUrl1 + '/jcr:content/parsys/lista-cities-1.' + dest + '.html',
        dataType: 'html',
        /*data: {
            destination: originCity.dest || "",
            isHomePage: true,
            isYoung: false,
            paxType: "ad"
        }*/
    })
        .done(successCities1)
        //always equalize slider height
        .fail(failCities1);
}

function successCities1(originCitiesList) {
    $('.select--departure').append(originCitiesList);
    var isInOfferts=false;
    var isInOffertsLong=false;

    var selectedAirport = $("#airport-pref").attr("data-airport");
    var selectedAirportLong = "";
    if (selectedAirport != undefined){
        selectedAirport = selectedAirport.split(" ");

        if (selectedAirport[1] != undefined){
            selectedAirportLong = selectedAirport[0];
            selectedAirport = selectedAirport[1];
            if (selectedAirport != undefined){
                $(".select--departure option").each(function(k,v){
                    if ($(v).val() == selectedAirport ){
                        $( '.lista-offerte__select .current-departure__content' ).html(
                            $("option[value='"+selectedAirport+"']").html()
                        );
                        isInOfferts = true;
                    }
                });
                if (!isInOfferts){
                    $(".select--departure option").each(function(k,v){
                        if ($(v).html().toUpperCase() == selectedAirportLong.toUpperCase()){
                            $( '.lista-offerte__select .current-departure__content' ).html(
                                $("option[value='"+$(v).val()+"']").html()
                            );
                            selectedAirportLong = $(v).val();
                            isInOffertsLong = true;
                        }
                    });
                }
            }
        }
    }


    if(isInOffertsLong){
        originCitySelected = selectedAirportLong
    } else if (isInOfferts){
        originCitySelected = selectedAirport
    } else{
        listaOfferteOptions.displaySelectValue();
        originCitySelected = $('.select--departure').val();

    }
    listaOfferteOptions.getDestinationList(originCitySelected);


    $('.select--departure').change(function () {
            listaOfferteOptions.displaySelectValue();
            originCitySelected = $('.select--departure').val();
            listaOfferteOptions.getDestinationList(originCitySelected);
            listaOfferteOptions.initAccordion();
        }
    )
    return false;
};

function failCities1(){
    listaOfferteOptions.getDestinationList('ROM');
    return false;
}

listaOfferteOptions.getDestinationList = function (originCitySelected) {
    var termsAndContitions = $('.lista-offerte__terms:contains("*")').length > 0;
    var italyWorldListsOnly = $('.italyWorldListsOnly').length > 0;
    var isHomePage=true;
    var isYoung=false;
    var paxType="ad";
    tab1AsteriskInPrice = termsAndContitions;
    tab2AsteriskInPrice = termsAndContitions;
    $.ajax({
        type: "GET",
        url: location.protocol + '//' + location.host + internalUrl1 + '/jcr:content/parsys/lista-destinations-1.' + originCitySelected + '.' + isHomePage + '.' + isYoung + '.' + paxType + '.' + tab1AsteriskInPrice + '.' + tab2AsteriskInPrice + '.' + italyWorldListsOnly + '.html',
        dataType: 'html',
        /*data: {
            from: originCitySelected,
            isHomePage: true,
            isYoung: false,
            paxType: "ad",
            tab1AsteriskInPrice: termsAndContitions,
            tab2AsteriskInPrice: termsAndContitions,
            italyWorldListsOnly: italyWorldListsOnly,
        },*/
        context: document.body
    })
        .done(successDestinationList1)
        .fail(failListOffer1)
        .always(function(){
            listaOfferteOptions.initSlider();
            listaOfferteOptions.equalizeSliderHeight();
            listaOfferteOptions.initAccordion();
        })
}

function successDestinationList1(destinationsList) {
    // $('.lista-offerte').empty();

    $('.lista-offerte__slider').remove();
    $('.lista-offerte__accordion').remove();
    $('.lista-offerte').append(destinationsList);
    if (Function('/*@cc_on return document.documentMode===10@*/')()){
        $("div[data-interchange]").each(function(){
            $(this).foundation();
        });
    }

    //rende cliccabile l'intero div delle offerte di ogni singola citta
    $(".lista-destinazioni__item").click(function(){
        window.location=$(this).find("a").attr("href");
        return false;
    });

    setTimeout( function() {
        if( Foundation.MediaQuery.atLeast( 'large' ) ) {
            listaOfferteOptions.setAccordionDesktop();
            listaOfferteOptions.equalizeAccordionHeight();
        } else {
            listaOfferteOptions.setAccordionMobile();
        }
    });

    return false;
}

function failListOffer1() {
    return false;
}

// function completeListOffer1() {
//     listaOfferteOptions.initAccordion();
// }