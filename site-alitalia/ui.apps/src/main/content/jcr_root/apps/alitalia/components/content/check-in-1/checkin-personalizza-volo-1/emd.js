"use strict";
use(function() {
    try {
        var pass = this.pass;
        var index = this.index;
        var emdList = pass.getEmd();
        var res = { lounge: false };
        for(var i=0; i<emdList.size(); i++) {
            if(emdList.get(i).commercialName.toLowerCase().equals("lounge access")) res.lounge = true;
        }
        return res;
    } catch(e) {
        return { lounge: false, debug: e.message };
    }
});