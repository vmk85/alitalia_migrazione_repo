jQuery(document).ready(function(){
		jQuery('#comfortseatsSubmit').on('click', comfortSeatsHandler);

		//popolazione del menu a tendina delle ragioni
		var requestData = {
				'ragioniReclamoPostoComfort': defaultSelectedRagione
		};
		
		getDropdownData(requestData, dropdownDataSuccess, dropdownDataFail);
});

function comfortSeatsHandler(e) {
	validation(e, 'comfortseatsclaimsubmit', '#specialpageform_comfortseatsclaim', comfortSeatsClaimSuccess, comfortSeatsClaimError);
}

function comfortSeatsClaimSuccess(data) {
	if(data.result){
		comfortSeatsClaimContinue(false)
	}
	else{
		 showErrors(data);
	}
}

function comfortSeatsClaimError() {
	return comfortSeatsClaimContinue(true);
}

function comfortSeatsClaimContinue(stop) {
	removeErrors();
	jQuery('#comfortseatsSubmit').off('click');
	return stop || jQuery('#comfortseatsSubmit').click();
}

function dropdownDataSuccess(data){
	// dropdown ragioni rimborso
	var ragioniRimborso = data.ragioniReclamoPostoComfort; 
	var option = '';
	for (index in ragioniRimborso) {
		if (ragioniRimborso[index].name == defaultSelectedRagione) {
			option = '<option value="' + ragioniRimborso[index].name + '" selected>' + ragioniRimborso[index].value + '</option>';
		} else {
			option = '<option value="' + ragioniRimborso[index].name + '">' + ragioniRimborso[index].value + '</option>';
		}
		jQuery('div.customSelect').find('select#reason').append(option);
	}
}

function dropdownDataFail(){
	console.error("Si è verificato un errore durante il caricamento del menu a tendina della ragione");
}
