$(document).ready( function() {
    $(".bg-image").foundation();
    $('.reveal-wrap').foundation();
    $('.btProcedi').bind('click', confirmPassengers);

});

$(document).on("closed.zf.reveal", "#manage-error-msg[data-reveal]", function() {
    location.href = location.protocol.concat("//").concat(location.host)
});

function confirmPassengers()
{
    $('.btProcedi').unbind('click', confirmPassengers);
    enableLoaderCheckin();
    performSubmit('checkinselectedpassenger', "#form-passengers", savePassengersSuccess, savePassengersFail);
}

function savePassengersSuccess(data)
{

    if(data)
    {
        if (!data.isError)
        {
            window.location.href = data.successPage;
        }else{
            // controllo se ci sono passeggeri con notClear a true per far uscire un popup diverso
            disableLoaderCheckin();
            if(data.sabreStatusCode == "7252"){
                // TODO: verificare se il link alla homepage funziona o se serve prenderlo dalla configurazione
                $("#goToHomepage").attr("href","../home-page.html");
                $("#errorMessageNotClear").html(CQ.I18n.get("checkin.checkinFailedNotClear.label"));
                $("#lnkErrorNotClear").trigger("click");
            }else{
            $("#errorMessage").html(CQ.I18n.get("checkin.checkinFailed.label"));
                $("#lnkError").trigger("click");
            }
        }
    }else{
        disableLoaderCheckin()
        $("#lnkError").trigger("click");
        $("#errorMessage").html(CQ.I18n.get("checkin.checkinFailed.label"));
    }
    $('.btProcedi').bind('click', confirmPassengers);
}

function savePassengersFail(data)
{
    disableLoaderCheckin()
    $('.btProcedi').bind('click', confirmPassengers);
}