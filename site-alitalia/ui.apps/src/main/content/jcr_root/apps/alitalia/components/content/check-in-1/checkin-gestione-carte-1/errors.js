"use strict";
use(function () {
    try {
        var passenger = this.passenger;
        var listErrors = "LIFTED,USED,VOID,EXCH,PRT,PRTX,CTRL,NOGO,RPRTINT,OK/OS,RFND,PRFD,IROP".split(",");
        var allErr = ["ETO", "NS", "Nessun etkt trovato"];
        if (passenger.getTicketStatus() != "OK") {
            var i = 0;
            while (i < listErrors.length - 1) {
                if (tStatus == "VOID") {
                    return {
                        status: "VOID",
                        errorMessage: "checkin.passeggeri.void.label",
                        isError: true
                    };
                }
                else if (passenger.ticketStatus == listErrors[i]) {
                    return {
                        status: listErrors[i],
                        errorMessage: "checkin.ticketStatus.OTHER",
                        isError: true
                    };
                }
                i++;
            }
            var tStatus = passenger.getTicketStatus();
            for (i in allErr) {
                if (tStatus == "VOID") {
                    return {
                        status: "VOID",
                        errorMessage: "checkin.passeggeri.void.label",
                        isError: true
                    };
                }
                else if (tStatus == allErr[i]) {
                    return {
                        status: allErr[i],
                        errorMessage: "checkin.ticketStatus." + allErr[i].replace(/\s/g, ''),
                        isError: true
                    };
                }
            }
        } else return {isError: false};
    } catch (e) {
        return {
            error: e.message
        };
    }
});