function webtrendsLoaded_callback() {}

var WTCallbacks = [];

function registerWebtrendsCallback(callback){}

/* Errori */
function analytics_CAP_Error() {
	mm_dcs_multitrack_errors('CAP - ERROR', 'Il Tuo Profilo');
}

function analytics_nuovo_pin_Error() {
	mm_dcs_multitrack_errors('NUOVO PIN - ERROR', 'Il Tuo Profilo');
}

function analytics_num_biglietto_Error() {
	mm_dcs_multitrack_errors('NUMERO BIGLIETTO - ERROR', 'Richiedi Miglia');
}

function analytics_millemiglia_login() {
	mm_dcs_multitrack_area('PersonalArea');
	activateDTMRule("MM");
}

function mm_dcs_multitrack_area(areaName) {}

function mm_dcs_multitrack_event(evtName, areaName) {}


function mm_dcs_multitrack_errors(errorMessage, areaName) {}

function booking_dcs_multitrack_errors(errorMessage, areaName) {}


function mmb_dcs_multitrack_event(evtName, stepName){}

function mmb_dcs_multitrack_btn_click(evtName, stepName, boxName){}

function ttfs_dcs_multitrack_event(evtName, stepName){}

function checkIn_dcs_multitrack_event(evtName, stepName){}

function checkIn_dcs_multitrack_btn_click(evtName, stepName, boxName){}

function checkin_dcs_multitrack_passengerLightboxYes(youngType){}

function checkIn_dcs_multitrack_undo(){}

/**
 * Handler on client context init event.
 */
function onAnalyticsLoginClientContextProfileInitialized() {
	// (login context functions defined in login-context clientlibs)
	if (isLoginContextAuthenticated()) {
		var analyticsLoginCompleted = getLoginContextProfileProperty(
				'analyticsLoginCompleted');
		if (analyticsLoginCompleted == "" || analyticsLoginCompleted == null) {
			analytics_millemiglia_login();
			setLoginContextProfileProperty('analyticsLoginCompleted', 'unavailable');
		}
	}
}