var novitaOptions = {};

$(document).ready(function() {

	novitaOptions.fixedSlidePosition = false;

	novitaOptions.slider = new Swiper('.novita__slider .swiper-container', {
		slidesPerView: 'auto',
		resistanceRatio: 0,
		pagination: {
			el: '.novita__slider .swiper-pagination',
			clickable: true
		}
	});

	$('.novita__slider .bg-image').foundation();

	novitaOptions.sliderPaginationVisibility();
	novitaOptions.equalizeSliderHeight();

});

var ww = $(window).width();
$(window).resize(function() {
	if(ww < 768 && !novitaOptions.fixedSlidePosition) {
		setTimeout(function() {
			try {
				novitaOptions.slider.slideTo(0, 0);
				novitaOptions.fixedSlidePosition = true;
			} catch (err) {
				console.log('Impossible to execute slideTo');
			}
			
		}, 1);
	} else if(ww >= 768) novitaOptions.fixedSlidePosition = false;

	setTimeout(function() {
		novitaOptions.equalizeSliderHeight();
	}, 100);

});


novitaOptions.equalizeSliderHeight = function() {
	$( '.novita__slider .custom-card__section--text' ).removeAttr( 'style' );
	novitaOptions.cardMaxHeight = $( '.novita__slider  .swiper-wrapper' ).outerHeight() - $( '.novita__slider .custom-card__section.bg-image' ).outerHeight();
	$( '.novita__slider .custom-card__section--text' ).css( 'height', novitaOptions.cardMaxHeight );
};

novitaOptions.sliderPaginationVisibility = function() {
	if( $('.novita__slider .swiper-slide').length < 2 ){
		$('.novita__slider .swiper-pagination').addClass('hide-for-large');
	}
};