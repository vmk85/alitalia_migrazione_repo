var checkinCustomizeOptions = checkinCustomizeOptions || {};
checkinCustomizeOptions.checkinWaveAncillaryInsurance = function() {
    function successInsuranceProcessing(data) {
        $("#addInsuranceBtn").removeClass("disabled");
        if (!$('#chkwaveinsurance').is(':checked')){
            $('#addInsuranceBtn').before('<div id="feedback-error" class="feedback-error" style="color: #e30512;" >Devi accettare le condizioni</div>');

        } else {
            $('#chkwaveinsurancenotification').addClass("confirm-status").text(message_baggage_ok.replace("{0}", "X"));
            $('#feedback-error').hide();
            performSubmit('checkingetcart','#frmCart',loadCartSuccess,loadCartFail);
            $("#insXBT").trigger( "click" );
            $("#insX").trigger( "click" );
        }
        disableLoaderCheckin();

    }
    function failureInsuranceProcessing(data) {
        disableLoaderCheckin();
        $("#addInsuranceBtn").removeClass("disabled");
        if(data.isError == true) {
            // alert("Error: " + data.errorMessage);
        }
    }

    function adjustCurrentInsurancePrice() {
            $(".chkwaveinsurance.total-row .price").text(parseInt($('#chkwaveinsurance').val()) + ( $("#data-insourance-price").attr("data-decimale") != undefined ? $("#data-insourance-price").attr("data-separatore"):"") + $("#data-insourance-price").attr("data-decimale")+" " + $('#chkwaveinsurance').data("currency"));
    }

    $('#chkwaveinsurance').click(function(e) {
        adjustCurrentInsurancePrice();
    });

    $('#addInsuranceBtn').click(function(){
        if(!$(this).hasClass("disabled")) {
            $(this).addClass("disabled");

            var form = $("<form>").attr("id","form-checkinwave-insurance").appendTo("body");
            var checkInsurance = $('#chkwaveinsurance').is(":checked") || false;

            insuranceTotal

            $('<input>').attr("type", "hidden").attr("name", "insurance").val(checkInsurance).appendTo(form);
            $('<input>').attr("type", "hidden").attr("name", "total").val(insuranceTotal).appendTo(form);
            enableLoaderCheckin();
            performSubmit('setinsurancerest', '#form-checkinwave-insurance', successInsuranceProcessing, failureInsuranceProcessing)
        }
    });
    adjustCurrentInsurancePrice();
}