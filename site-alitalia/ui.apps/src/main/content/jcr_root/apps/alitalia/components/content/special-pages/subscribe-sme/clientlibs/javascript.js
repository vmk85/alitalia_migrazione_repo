var email_reg_exp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-]{2,})+\.)+([a-zA-Z0-9]{2,})+$/;
var piva_IT_reg_exp = /^[0-9]{11}$/;
var zipCode_IT_reg_exp = /^[0-9]{5}$/;
var isCompanyEmailValid = false;
var isAdminAccountEmailValid=false;
var comboProvinciaText=$('#company_state_prov').text();
var comboProvinciaVal=$('#company_state_prov').val();

var erroreCampoObbligatorio=$('#campoObbligatorio').val();
var errorePivaItalia=$('#pivaItalia').val();
var erroreEmailNonValida=$('#emailNonValida').val();
var erroreZipCodeNonValido=$('#zipCodeNonValido').val();

function isItalianMarket(){
    var url = $(location).attr("href");
    if ((url.indexOf("it_it") >= 0)||(url.indexOf("en_it") >= 0)||(url.indexOf("alitalia-it") >= 0)||(url.indexOf("master-it") >= 0)) {
        return true
    }else{return false}
}
function validateEmail(email){
    if (!email_reg_exp.test(email) || (email == "") || (email == "undefined")) {
       return false;
    }else{return true;}
}

function validatePiva(piva,nazione) {
    if ((nazione=="IT")&&(!piva_IT_reg_exp.test(piva))) {
        return false;
    }else{return true;}
}

function validateZipCode(zipCode,nazione) {
    if ((nazione=="IT")&&(!zipCode_IT_reg_exp.test(zipCode))) {
        return false;
    }else{return true;}
}

$('#company_email, #admin_account_email').bind('blur',function(){
    try{
        isCompanyEmailValid=validateEmail ($('#company_email').val());
        isAdminAccountEmailValid=validateEmail ($('#admin_account_email').val());
    }
    catch(err){
        //alert('err='+err)
    };
});

function TornaTab1() {
    document.getElementById("menu_business").style.color="#949494";
    document.getElementById("icon_form_business").style.visibility="hidden";
    document.getElementById("tab1_azienda").checked = true;
}

function TornaTab2() {
    document.getElementById("menu_business1").style.color="#949494";
    document.getElementById("icon_form_business1").style.visibility="hidden";
    document.getElementById("tab2_contatti").checked = true;
}

function TornaTab3() {
    document.getElementById("menu_business2").style.color="#949494";
    document.getElementById("icon_form_business2").style.visibility="hidden";
    document.getElementById("tab3_amministrazione").checked = true;
}

function prosegui (tab){

    if(tab=="2")
    {
        VaiAlTab2();
    }
    else if(tab=="3")
    {
        VaiAlTab3();
    }

    else if(tab=="4")
    {
        VaiAlTab4();
    }
}


function VaiAlTab2(){
    document.getElementById("ErroreAzi").textContent ="";
    document.getElementById("ErrorePiva").textContent ="";
    document.getElementById("ErroreCountry").textContent ="";
    document.getElementById("ErroreStateProv").textContent ="";

    if (document.getElementById("company_name").value == "")
    {
        document.getElementById("ErroreAzi").textContent =erroreCampoObbligatorio;
        document.getElementById("company_name").focus();
    }
    else if ((document.getElementById("company_piva").value == "")&&(isItalianMarket()))
    {
        document.getElementById("ErrorePiva").textContent=erroreCampoObbligatorio;
        document.getElementById("company_piva").focus();
    }
    else if (!validatePiva(($('#company_piva').val()),($('#company_country').val())))
    {
        document.getElementById("ErrorePiva").textContent=errorePivaItalia;
        document.getElementById("company_piva").focus();
    }
    else if ((document.getElementById("company_country").checkValidity() == false) || ($('#company_country').val()=="0"))
    {
        document.getElementById("ErroreCountry").textContent=erroreCampoObbligatorio;
    }
    else if (((document.getElementById("company_state_prov").checkValidity() == false) || ($('#company_state_prov').val()=="0")) && ($('#company_country').val()=="IT"))
    {
        document.getElementById("ErroreStateProv").textContent =erroreCampoObbligatorio;
    }
    else
    {
        document.getElementById("tab2_contatti").checked = true;
        document.getElementById("icon_form_business").style.visibility="visible";
        document.getElementById("menu_business").style.color="black";
    }
}

function VaiAlTab3(){
    document.getElementById("ErroreAdress").textContent ="";
    document.getElementById("ErroreCity").textContent ="";
    document.getElementById("ErroreZipCode").textContent ="";
    document.getElementById("ErroreEmail").textContent ="";
    document.getElementById("ErroreNationalPrefix").textContent ="";
    document.getElementById("ErroreAreaPrefix").textContent ="";
    document.getElementById("ErrorePhoneNumber").textContent ="";

    if (document.getElementById("company_address").checkValidity() == false)
    {
        document.getElementById("ErroreAdress").textContent =erroreCampoObbligatorio;
        document.getElementById("company_address").focus();
    }
    else if (document.getElementById("company_city").checkValidity() == false)
    {
        document.getElementById("ErroreCity").textContent=erroreCampoObbligatorio;
        document.getElementById("company_city").focus();
    }
    else if (document.getElementById("company_zipcode").checkValidity() == false)
    {
        document.getElementById("ErroreZipCode").textContent=erroreCampoObbligatorio;
        document.getElementById("company_zipcode").focus();
    }
    else if (!validateZipCode(($('#company_zipcode').val()),($('#company_country').val())))
    {
        document.getElementById("ErroreZipCode").textContent=erroreZipCodeNonValido;
        document.getElementById("company_zipcode").focus();
    }
    else if (document.getElementById("company_email").checkValidity() == false)
    {
        document.getElementById("ErroreEmail").textContent =erroreCampoObbligatorio;
        document.getElementById("company_email").focus();
    }
    else if (isCompanyEmailValid==false)
    {
        document.getElementById("ErroreEmail").textContent =erroreEmailNonValida;
        document.getElementById("company_email").focus();
    }
    else if (document.getElementById("company_phone_nationalprefix").value == "")
    {
        document.getElementById("ErroreNationalPrefix").textContent =erroreCampoObbligatorio;
    }
    else if (document.getElementById("company_area_prefix").value == "")
    {
        document.getElementById("ErroreAreaPrefix").textContent =erroreCampoObbligatorio;
        document.getElementById("company_area_prefix").focus();
    }
    else if (document.getElementById("company_phone_number").value == "")
    {
        document.getElementById("ErrorePhoneNumber").textContent =erroreCampoObbligatorio;
        document.getElementById("company_phone_number").focus();
    }
    else
    {
        document.getElementById("tab3_amministrazione").checked = true;
        document.getElementById("icon_form_business1").style.visibility="visible";
        document.getElementById("menu_business1").style.color="black";
    }
}

function VaiAlTab4(){
    document.getElementById("ErroreAccountName").textContent ="";
    document.getElementById("ErroreAccountSurname").textContent ="";
    document.getElementById("ErroreDay").textContent ="";
    document.getElementById("ErroreMonth").textContent ="";
    document.getElementById("ErroreYear").textContent ="";
    document.getElementById("ErroreAccMail").textContent ="";
    document.getElementById("ErroreAccPhonePrefix").textContent ="";
    document.getElementById("ErroreAccPrefix").textContent ="";
    document.getElementById("ErroreNumber").textContent ="";
    if (document.getElementById("admin_account_name").checkValidity() == false)
    {
        document.getElementById("ErroreAccountName").textContent =erroreCampoObbligatorio;
        document.getElementById("admin_account_name").focus();
    }
    else if (document.getElementById("admin_account_surname").checkValidity() == false)
    {
        document.getElementById("ErroreAccountSurname").textContent=erroreCampoObbligatorio;
        document.getElementById("admin_account_surname").focus();
    }
    else if (document.getElementById("day").value == "")
    {
        document.getElementById("ErroreDay").textContent=erroreCampoObbligatorio;
        document.getElementById("day").focus();
    }
    else if (document.getElementById("month").value == "")
    {
        document.getElementById("ErroreMonth").textContent =erroreCampoObbligatorio;
    }
    else if (document.getElementById("year").value == "")
    {
        document.getElementById("ErroreYear").textContent =erroreCampoObbligatorio;
    }
    else if (document.getElementById("admin_account_email").checkValidity() == false)
    {
        document.getElementById("ErroreAccMail").textContent =erroreCampoObbligatorio;
        document.getElementById("admin_account_email").focus();
    }
    else if (isAdminAccountEmailValid ==false)
    {
        document.getElementById("ErroreAccMail").textContent =erroreEmailNonValida;
        document.getElementById("admin_account_email").focus();
    }
    else if (document.getElementById("admin_account_phone_nationalprefix").value == "")
    {
        document.getElementById("ErroreAccPhonePrefix").textContent =erroreCampoObbligatorio;
    }
    else if (document.getElementById("admin_account_area_prefix").checkValidity() == false)
    {
        document.getElementById("ErroreAccPrefix").textContent =erroreCampoObbligatorio;
        document.getElementById("admin_account_area_prefix").focus();
    }
    else if (document.getElementById("admin_account_phone_number").checkValidity() == false)
    {
        document.getElementById("ErroreNumber").textContent =erroreCampoObbligatorio;
        document.getElementById("admin_account_phone_number").focus();
    }
    else
    {
        document.getElementById("tab4_comunicazione").checked = true;
        document.getElementById("icon_form_business2").style.visibility="visible";
        document.getElementById("menu_business2").style.color="black";
    }
}

function inviaBusinessConnect(e){
    document.getElementById("ErrorePreferCom").textContent ="";
    document.getElementById("ErrorePreferLang").textContent ="";
    document.getElementById("ErrorePrefAccepted").textContent ="";
    document.getElementById("ErroreCaptcha").textContent ="";

    if (document.getElementById("preferences_comunication").value == "")
    {
        document.getElementById("ErrorePreferCom").textContent=erroreCampoObbligatorio;
    }
    else if (document.getElementById("preferences_language").value == "")
    {
        document.getElementById("ErrorePreferLang").textContent=erroreCampoObbligatorio;
    }
    else if (document.getElementById("preferences_accepted").checked == "")
    {
        document.getElementById("ErrorePrefAccepted").textContent=erroreCampoObbligatorio;
    }
    else if (document.getElementById("captcha").value == "")
    {
        //MOSTRA MESSAGGIO OBBLIGATORIETA' DEL CAPTCHA
        document.getElementById("ErroreCaptcha").textContent=erroreCampoObbligatorio;
        refreshCaptcha();
    }
    else
    {
        validation(e, 'subscriptionsme','#form-subscriptionsme', validation_ok, validation_fail);
    }
}

//validation_ok viene eseguita anche se il captcha e' errato, in questo caso data.result = false

function validation_ok(data) {
    if(data.result){
        removeErrors();
        performSubmit('subscriptionsme', '#form-subscriptionsme',submit_ok, submit_fail );
    } else {
        validation_fail(data);
    }
}

function validation_fail(data) {
    document.getElementById("ErroreCaptcha").textContent = data.fields.captcha;
    refreshCaptcha();
}

function submit_ok() {
    document.getElementById("tab5_typ").checked = true;
}

function submit_fail() {
    alert('Email NON inviata');
}


function hideWaiting(data) {
    $('.waitingPage').hide();
}
