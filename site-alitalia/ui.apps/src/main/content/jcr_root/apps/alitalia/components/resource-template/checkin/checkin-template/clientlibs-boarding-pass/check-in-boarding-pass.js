function boardingPassSubmit(e) {
	e.preventDefault();
	boardingPassSubmitAction();
	validation(e, 'checkinboardingpass', '#boardingForm',
			boardingPassSubmitSuccess, boardingPassSubmitError);	
}

function boardingPassSubmitSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('checkinboardingpass', '#boardingForm',
				boardingPassDone, boardingPassSubmitError);
		
	} else {
		boardingPassSubmitAction(true);
		showErrors(data);
		/*Tracking errors*/
		if(typeof window["analytics_trackCheckinErrors"] == "function"){
			analytics_trackCheckinErrors(data.fields);
		}
	}
}

function boardingPassDone(data) {
	if (data.pdf) {
		window.open(data.pdf, '_blank');
	}
	window.location.replace(data.redirect);
}

function boardingPassSubmitError() {
	boardingPassSubmitAction(true);
}

function boardingPassSubmitAction(on) {
	if (on) {
		$('#boardingSubmit').on('click', boardingPassSubmit);
		$('#boardingSubmit').removeClass('isDisabled');
		startPageLoader();
	} else {
		startPageLoader(true);
		$('#boardingSubmit').addClass('isDisabled');
		$('#boardingSubmit').off('click', boardingPassSubmit);
	}
}

function checkInDone() {
	$('#boardingSubmit').on('click', boardingPassSubmit);
}