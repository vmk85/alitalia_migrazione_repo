var checkInOptions = {};

$(document).ready( function() {
    $('input[type=radio][name=switch--check-in]:radio').change(function() {
        resetErrors();
        if ( cercaVoloOptions.desktopOverlayOpened == 0 ) {
            cercaVoloOptions.openOverlayDesktop();
        }
    });
    $('input[type=radio][name=switch--info-voli]:radio').change(function() {
        resetErrors();
        if ( cercaVoloOptions.desktopOverlayOpened == 0 ) {
            cercaVoloOptions.openOverlayDesktop();
        }
    });
    $('input[type=radio][name=switch--prenota]:radio').change(function() {
        resetErrors();
        if ( cercaVoloOptions.desktopOverlayOpened == 0 ) {
            cercaVoloOptions.openOverlayDesktop();
        }
    });
    $('.tabs-title').on('click',function(e){
        resetErrors();
    });
    $( '[value ="Cerca"]' ).on( 'click', function( e ) {
        if ( cercaVoloOptions.desktopOverlayOpened == 0 ) {
            cercaVoloOptions.openOverlayDesktop();
        }
    });

    refreshDataLayer();
    $('input#firstName').on('change',function(){
        //add REGEXP
        if ($('input#firstName').val()!=""){
            $('input#firstName').removeClass('is-invalid-input');
        }
    });
    $('input#pnr').on('change',function(){
        //add REGEXP
        if ($('input#pnr').val()!=""){
            $('input#pnr').removeClass('is-invalid-input');
        }
    });
    $('input#lastName').on('change',function(){
        //add REGEXP
        if ($('input#lastName').val()!=""){
            $('input#lastName').removeClass('is-invalid-input');
        }
    });

    $('#cercaPnrSubmit').on('click', cercaPnr);
    $('#cercaPnrSubmit_oldCheckin').on('click', cercaPnr_oldCheckin);


    $('#cercaByFFSubmit').on('click', cercaByFF);
    $('#cercaByFFSubmit_oldCheckin').on('click', cercaByFF_oldCheckin);

    $('#cercamyFlightSubmit').on('click', cercaPnrMyFlight);

    $('#labelCheckinError').hide();
    resetErrors();


});
function validationInputs(type){
    var inputPnr, inputFF,inputName,inputLastname;
    var twoToFourNumbers = /^[0-9]{2,4}$/;
    var fourNumbers = /^[0-9]{4}$/;
    var sixNumbers = /^[0-9]{6}$/;
    var thirteenNumbers = /^[0-9]{13}$/;
    var fiveToTenNumbers = /^[0-9]{4,10}$/;
    var sixAlphanumeric = new RegExp("^[a-zA-Z0-9]{6}$");
    var fiveAlphanumeric = new RegExp("^[a-zA-Z0-9]{5}$");
    var noNumbers = /^([^0-9]*)$/;
    var checkinError = false;

    resetErrors();
    // variabili 3 input da validare ( partenza, destinazione, data andata e ritorno )
    var errorInputs = [];
    if (type == "FF"){
        inputFF = $( '#name_code' ).val().trim();
        inputLastname = $( '#name_surname' ).val().trim();
//        checkInOptions.input1Val = ( $( '#frequentFlyer' ).val() !== '' ) ? 1 : 0;
//        checkInOptions.input3Val = ( $( '.lastNameCercaByFFSubmit' ).val() !== '') ? 1 : 0;
        checkInOptions.input1Val = ( inputFF !== '' && fiveToTenNumbers.test(inputFF)) ? 1 : 0;
        checkInOptions.input3Val = ( inputLastname !== '' && noNumbers.test(inputLastname)) ? 1 : 0;
        if( checkInOptions.input1Val && checkInOptions.input3Val) {
            return true;
        } else {
            checkinError = true;
            if (!checkInOptions.input1Val){
//                errorInputs.push($( 'input#frequentFlyer' ));
                errorInputs.push($( '#name_code' ));
            }
            if (!checkInOptions.input3Val){
//                errorInputs.push($( 'input.lastNameCercaByFFSubmit'));
                errorInputs.push($( '#name_surname'));
            }
        }
    }
    else if (type == "pnr"){
        checkinError = true;
        inputPnr = $( '#code' ).val().trim();
        inputName = $( '#name' ).val().trim();
        inputLastname = $( '#surname' ).val().trim();
//        checkInOptions.input1Val = ( $( '#pnr' ).val() !== '' ) ? 1 : 0;
//        checkInOptions.input2Val = ( $( '#firstName' ).val() !== '') ? 1 : 0;
//        checkInOptions.input3Val = ( $( '#lastName' ).val() !== '') ? 1 : 0;
//        checkInOptions.input1Val = ( inputPnr !== '' && (thirteenNumbers.test(inputPnr) || sixAlphanumeric.test(inputPnr) || fiveAlphanumeric.test(inputPnr))) ? 1 : 0;                checkInOptions.input1Val = ( inputPnr !== '' && (thirteenNumbers.test(inputPnr) || sixAlphanumeric.test(inputPnr) || fiveAlphanumeric.test(inputPnr))) ? 1 : 0;
        checkInOptions.input1Val = ( inputPnr !== '' && (thirteenNumbers.test(inputPnr) || sixAlphanumeric.test(inputPnr))) ? 1 : 0;
        checkInOptions.input2Val = (inputName !== '' && noNumbers.test(inputName)) ? 1 : 0;
        checkInOptions.input3Val = ( inputLastname !== '' && noNumbers.test(inputLastname)) ? 1 : 0;
        if( checkInOptions.input1Val && checkInOptions.input2Val && checkInOptions.input3Val) {
            return true;
        } else {
            checkinError = true;
            if (!checkInOptions.input1Val){
//                errorInputs.push($( 'input#pnr' ));
                errorInputs.push($( 'input#code' ));
            }
            if (!checkInOptions.input2Val){
//                errorInputs.push($( 'input#firstName'));
                errorInputs.push($( 'input#name'));
            }
            if (!checkInOptions.input3Val){
//                errorInputs.push($( 'input#lastName'));
                errorInputs.push($( 'input#surname'));
            }
        }
    }else if (type == "pnr myFlight"){
        inputPnr = $( '#form-myFlightSearch #pnr' ).val().trim();
        inputName = $( '#form-myFlightSearch #firstName' ).val().trim();
        inputLastname = $( '#form-myFlightSearch #lastName' ).val().trim();
//        checkInOptions.input1Val = ( $( '#form-myFlightSearch #pnr' ).val() !== '' ) ? 1 : 0;
//        checkInOptions.input2Val = ( $( '#form-myFlightSearch #firstName' ).val() !== '') ? 1 : 0;
//        checkInOptions.input3Val = ( $( '#form-myFlightSearch #lastName' ).val() !== '') ? 1 : 0;
//        checkInOptions.input1Val = ( inputPnr !== '' && ( sixAlphanumeric.test(inputPnr) || fiveAlphanumeric.test(inputPnr))) ? 1 : 0;
        checkInOptions.input1Val = ( inputPnr !== '' && ( sixAlphanumeric.test(inputPnr))) ? 1 : 0;
        checkInOptions.input2Val = (inputName !== '' && noNumbers.test(inputName)) ? 1 : 0;
        checkInOptions.input3Val = ( inputLastname !== '' && noNumbers.test(inputLastname)) ? 1 : 0;
        if( checkInOptions.input1Val && checkInOptions.input2Val && checkInOptions.input3Val) {
            return true;
        } else {
            if (!checkInOptions.input1Val){
                errorInputs.push($( '#form-myFlightSearch input#pnr' ));
            }
            if (!checkInOptions.input2Val){
                errorInputs.push($( '#form-myFlightSearch input#firstName'));
            }
            if (!checkInOptions.input3Val){
                errorInputs.push($( '#form-myFlightSearch input#lastName'));
            }
        }
    }else if (type == "login"){
        pin = $( '#pin' ).val().trim();
        mm = $( '#millemiglia' ).val().trim();
        loginOptions.input1Val = ( mm !== '' && fiveToTenNumbers.test(mm)) ? 1 : 0;
        loginOptions.input2Val = ( pin !== '' && fourNumbers.test(pin)) ? 1 : 0;
        if( loginOptions.input1Val && loginOptions.input2Val) {
            return true;
        } else {
            if (!loginOptions.input1Val){
                errorInputs.push($( '#millemiglia' ));
            }
            if (!loginOptions.input2Val){
                errorInputs.push($( '#pin'));
            }
        }
    }
    setErrorInput(errorInputs,type);
    return false;
}

function setErrorInput(inputArr,type){
    resetErrors();
    var messageError = "";
    inputArr.forEach(function(inp){
        $(inp).addClass('is-invalid-input');
        $(inp).prev().addClass('is-invalid-input');
        $(inp).next().addClass('is-invalid-input');

        switch ($(inp).attr('id')){
            case 'code':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.pnrTicketNumber.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('preparaViaggio.pnrTicketNumber.error.empty') + ". ";
                }
                break;
            case 'pnr':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.pnrTicketNumber.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('preparaViaggio.pnrTicketNumber.error.empty') + ". ";
                }
                break;
            case 'name':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.nome.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('preparaViaggio.nome.error.empty') + ". ";
                }
                break;
            case 'surname':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.cognome.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('preparaViaggio.cognome.error.empty') + ". ";
                }
                break;
            case 'name_surname':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.cognome.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('preparaViaggio.cognome.error.empty') + ". ";
                }
                break;
            case 'name_code':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.frequentFlyer.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('preparaViaggio.frequentFlyer.error.empty') + ". ";
                }
                break;

            case 'firstName':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.nome.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('preparaViaggio.nome.error.empty') + ". ";
                }
                break;
            case 'lastName':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.cognome.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('preparaViaggio.cognome.error.empty') + ". ";
                }
                break;
            case 'millemiglia':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.frequentFlyer.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('preparaViaggio.frequentFlyer.error.empty') + ". ";
                }
                break;
            case 'pin':
                var $currInp = $(inp);
                if ($currInp.val()!== ""){
                    messageError = messageError + CQ.I18n.get('preparaViaggio.pin.error.invalid') + ". ";
                }else{
                    messageError = messageError + CQ.I18n.get('preparaViaggio.pin.error.empty') + ". ";
                }
                break;
        }
    });
    if (messageError){
        if (type == "login"){
            var mess = $('#header-wave-login-errors').html();
            $('#header-wave-login-errors').html(mess+" "+messageError);
            $('#header-wave-login-errors').css('display','block');
        }else{
            $('#labelCheckinError').css('display','block');
            $('#labelCheckinError').html(messageError);
            $('.labelErrorInfoStatoVolo').css('display','block');
            $('.labelErrorInfoStatoVolo').addClass('feedback-error');
            $('.labelErrorInfoStatoVolo').html(messageError);
        }
        if(checkinError){
            dataLayer[1].checkinError = messageError;
            if(typeof window["_satellite"] !== "undefined"){
            	_satellite.track("CIError");
            } else {
            	console.error("Cannot activate CIError. _satellite object not found");
            }
            dataLayer[1].checkinError = "";
        }
    }
}

function resetErrors(){
    $("#header-wave-login-errors").html("");
    $("#header-wave-login-errors").css('display','none');

    $('input').removeClass('is-invalid-input');
    $('input').prev().removeClass('is-invalid-input');
    $('input').next().removeClass('is-invalid-input');
    $('#labelCheckinError').css('display','none');
    $('.labelErrorInfoStatoVolo').html("");
    $('.labelErrorInfoStatoVolo').css('display','none');
}

function cercaPnrMyFlight(){
    if( validationInputs('pnr myFlight') ) {
        // enable loader
        cercaVoloOptions.enableLoader();
        performSubmit('preparaviaggiosubmit','#form-myFlightSearch',cercaPnrMyFlightsSuccess,cercaPnrMyFlightsFail);
    }
}

function cercaPnrMyFlightsSuccess(data) {
    if (!data.isError) {
        window.location.href = data.redirect;
    }else{
        // disable loader
        cercaVoloOptions.disableLoader();

        var invalid_pnr = data.errorMessage.indexOf("preparaViaggio.pnrTicketNumber.error.invalid");
        var invalid_pnr_message = CQ.I18n.get('preparaViaggio.pnrTicketNumber.error.invalid')
        if (invalid_pnr){
            $('.labelErrorInfoStatoVolo').css('display','block');
            $('.labelErrorInfoStatoVolo').addClass('feedback-error');
            $('.labelErrorInfoStatoVolo').html(invalid_pnr_message);
        }else{
            $('#labelCheckinError').show();
            $('#labelCheckinError').text(CQ.I18n.get('specialpage.error.service'));
        }
    }
    $('#cercamyFlightSubmit').bind('click', cercaPnrMyFlight);
}

function cercaPnrMyFlightsFail() {
    // disable loader
    cercaVoloOptions.disableLoader();
    $('.labelErrorInfoStatoVolo').css('display','block');
    $('.labelErrorInfoStatoVolo').addClass('feedback-error');
    $('.labelErrorInfoStatoVolo').html(CQ.I18n.get('specialpage.error.service'));
    //$('#labelCheckinError').show();
    //$('#labelCheckinError').text('Errore (servlet FAIL) ');
    $('#cercamyFlightSubmit').bind('click', cercaPnrMyFlight);
}

function cercaPnr(){
    if( validationInputs('pnr') ) {
    updateDataLayer('pnr',$( '#pnr' ).val());
        // enable loader
        cercaVoloOptions.enableLoader();

        var data = {
            pnr : $( '#code' ).val(),
            firstName : $( '#name' ).val(),
            lastName : $( '#surname' ).val()
        };
        performSubmit('checkinpnrsearch',data,cercaPnrSuccess,cercaPnrBeforeFail);
    }
}

function cercaPnr_oldCheckin(){
    if( validationInputs('pnr') ) {
        // enable loader
        cercaVoloOptions.enableLoader();
        var data = {
            isError : false
        };
        cercaPnrBeforeSuccess(data);
    }
}

function cercaPnrBeforeSuccess(data) {
    if (!data.isError)
    {
        performSubmit('preparaviaggiosubmit','#checkInSearch',cercaPnrSuccess_oldCheckin,cercaPnrFail);
    }
    else{
        // disable loader
        cercaVoloOptions.disableLoader();
        var invalid_pnr = data.errorMessage.indexOf("checkin.common.pnrInvalid.error");
        var invalid_pnr_message = CQ.I18n.get('checkin.common.pnrInvalid.error')
        if (invalid_pnr){
            //$('#labelCheckinError').show();
            //$('#labelCheckinError').text(invalid_pnr_message);
        }else{
            $('#labelCheckinError').show();
            $('#labelCheckinError').text(data.errorMessage);
        }
    }
}

function cercaPnrBeforeFail() {
    // disable loader
    cercaVoloOptions.disableLoader();

    $('#labelCheckinError').show();
    $('#labelCheckinError').text(CQ.I18n.get('specialpage.error.service'));
}

function cercaPnrSuccess(data) {
    if (!data.isError) {
        window.location.href = data.successPage;
    }else{
        // disable loader
        cercaVoloOptions.disableLoader();
        $('#labelCheckinError').show();
        $('#labelCheckinError').text(CQ.I18n.get(data.errorMessage));

        dataLayer[1].checkinError = CQ.I18n.get(data.errorMessage);
        if(typeof window["_satellite"] !== "undefined"){
        	_satellite.track("CIError");
        } else {
        	console.error("Cannot activate CIError. _satellite object not found");
        }
    }
}

function cercaPnrSuccess_oldCheckin(data) {
    if (data.result == "OK") {
        window.location.href = data.redirect;
    }else{
     if (!data.errorMessage.indexOf("Unexpected invalid form on submit request")){
            data.errorMessage = CQ.I18n.get('specialpage.businessconnect.solo_caratteri_latini');
        }
        if (data.sabreStatusCode == "grp") {
            data.errorMessage = CQ.I18n.get('checkin.common.overPax.error');
        }        // disable loader
        cercaVoloOptions.disableLoader();
        $('#labelCheckinError').show();
        $('#labelCheckinError').text(data.errorMessage);
    }
    //$('#cercaPnrSubmit').bind('click', cercaPnr);
    $('#cercaPnrSubmit_oldCheckin').on('click', cercaPnr_oldCheckin);
}

function cercaPnrFail() {
    // disable loader
    cercaVoloOptions.disableLoader();

    $('#labelCheckinError').show();
    $('#labelCheckinError').text(CQ.I18n.get('specialpage.error.service'));
    $('#cercaPnrSubmit').bind('click', cercaPnr);
    $('#cercaPnrSubmit_oldCheckin').on('click', cercaPnr_oldCheckin);
}


function cercaByFF(){
    if( validationInputs('FF') ) {
        // enable loader
        cercaVoloOptions.enableLoader();
        var data = {
            frequentFlyer : $( '#name_code' ).val(),
            lastName : $( '#name_surname' ).val(),
        };
        performSubmit('searchbyfrequentflyer', data, cercaByFFSuccess, cercaByFFFail);
    }
}

function cercaByFF_oldCheckin(){
    if( validationInputs('FF') ) {
        // enable loader
        cercaVoloOptions.enableLoader();
        performSubmit('preparaviaggiosubmit','#checkInLogInSearch',cercaByFFSuccess_oldCheckin,cercaByFFFail);
    }
}

function cercaByFFSuccess(data) {
    if (!data.isError) {
        window.location.href = data.successPage;
    }else{
        // disable loader
        cercaVoloOptions.disableLoader();
        $('#labelCheckinError').show();
    }
    $('#cercaByFFSubmit').bind('click', cercaByFF);
    //$('#cercaByFFSubmit_oldCheckin').bind('click', cercaByFF_oldCheckin);
}

function cercaByFFSuccess_oldCheckin(data) {
    if (data.result == "OK") {
        window.location.href = data.redirect;
    }else{
        // disable loader
        cercaVoloOptions.disableLoader();
        $('#labelCheckinError').show();
    }
    //$('#cercaByFFSubmit').bind('click', cercaByFF);
    $('#cercaByFFSubmit_oldCheckin').bind('click', cercaByFF_oldCheckin);
}

function cercaByFFFail() {
    // disable loader
    cercaVoloOptions.disableLoader();
    $('#labelCheckinError').show();
    $('#labelCheckinError').text(CQ.I18n.get('specialpage.error.service'));
    $('#cercaByFFSubmit').bind('click', cercaByFF);
    $('#cercaByFFSubmit_oldCheckin').bind('click', cercaByFF_oldCheckin);
}


function getCheckinComplete(){

    var checkinComplete;
    if($('.passenger').length){
        checkinComplete = $('.passenger').length;
    }
    updateDataLayer('checkinComplete',checkinComplete);

}

function updateDataLayer(key,value){

    localStorage.setItem(key,value);
}

/*Aggiunto try catch per gestire errore su Dynatrace: Access is denied*/
function refreshDataLayer(){
	try {
		localStorage.removeItem('pnr');
	    localStorage.removeItem('origin');
	    localStorage.removeItem('originCity');
	    localStorage.removeItem('destination');
	    localStorage.removeItem('destinationCity');
	    localStorage.removeItem('departureDate');
	    localStorage.removeItem('roundTrip');
	    localStorage.removeItem('flight');
	    localStorage.removeItem('Returnflight');
	    localStorage.removeItem('bpPrinted');
	    localStorage.removeItem('bpSent');
	    localStorage.removeItem('offloadDone');
	    localStorage.removeItem('deepLinkClick');
	    localStorage.removeItem('removedAncillary');
	    localStorage.removeItem('revenueAncillary');
	    localStorage.removeItem('insurance');
	    localStorage.removeItem('bpReprinted');
	    localStorage.removeItem('checkinComplete');
	    localStorage.removeItem('checkinFail');
	    localStorage.removeItem('checkinError');
	    localStorage.removeItem('paxType');
	    localStorage.removeItem('baggage');
	    localStorage.removeItem('seat');
	    localStorage.removeItem('lounge');
	    localStorage.removeItem('fastTrack');
	    localStorage.removeItem('insurance');
	} catch (e) {
		console.log('Cannot execute refreshDataLayer');
	}

}

$(document).ready(function(){
    $("#code").blur(function(){
        $(this).val($(this).val().replace(/\s/g,''));
        console.log($(this).val());
    });
});