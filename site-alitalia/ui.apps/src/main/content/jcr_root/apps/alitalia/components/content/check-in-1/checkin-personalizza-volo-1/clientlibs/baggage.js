var checkinCustomizeOptions = checkinCustomizeOptions || {};

function checkButtons(bValue, maxVal){
    var MAX_BAGS = 6;

    if (!isNaN(maxVal) )
		MAX_BAGS = maxVal;

    var btnMin = $('button.chkwavebaggage[data-quantity="minus"]');
    var btnMax = $('button.chkwavebaggage[data-quantity="plus"]');
    //console.log(bValue);
    var totBags = bValue;
    if (totBags < 1 ) {
        btnMin.addClass("no-active");
    } else {
        btnMin.removeClass("no-active");
    }
    if (totBags >= MAX_BAGS ) {
        btnMax.addClass("no-active");
    } else {
        btnMax.removeClass("no-active");
    }
}

checkinCustomizeOptions.checkinWaveAncillaryBaggage = function() {

    function successBaggageProcessing(data) {
        $("#addBaggageBtn").removeClass("disabled");
        if(data.isError == true) {
            //alert("Error: " + data.errorMessage);
        } else {
            $('#chkwavebaggagenotification').addClass("confirm-status").text(message_baggage_ok.replace("{0}", "X"));
            // Aggiorno il carrello
            performSubmit('checkingetcart','#frmCart',loadCartSuccess,loadCartFail);
        }
        disableLoaderCheckin();
    }
    function failureBaggageProcessing(data) {
        disableLoaderCheckin();
        $("#addBaggageBtn").removeClass("disabled");
        if(data.isError == true) {
            // alert("Error: " + data.errorMessage);
        }
    }

    function adjustTotalBaggagePrice() {
        var totalPrice = 0;
        $(".chkwavebaggage.col_price").each(function( index ) {
            var priceTotPrice= String($( this ).attr("data-price"));
            var totSum = String(totalPrice).split(".");
            var totAct = priceTotPrice.split(".");
            var totResultPost = (totSum[1] == undefined ? 0:parseInt(totSum[1]) ) + (totAct[1] == undefined ? 0:parseInt(totAct[1]) );
            var totResultPre = parseInt(totSum[0]) +parseInt(totAct[0]);


            totalPrice = totResultPre + (totResultPost == 0? "":"." + totResultPost);
        });
        totalPrice = String(totalPrice).split(".");
        if (totalPrice.length > 1){
            totalPrice = totalPrice[0] + (totalPrice[1] != undefined ? separatore:"") + (totalPrice[1].length > 1? totalPrice[1] : totalPrice[1] + "0" );
        } else {
            totalPrice = totalPrice[0] + (decimale2 != "" ? separatore : "") + decimale2 ;
        }

        totalPrice = splitAdjust(totalPrice);

        $(".chkwavebaggage.total-row .price").text(totalPrice + " " + currency);
    }

    function adjustCurrentBaggagePrice(field,totBags) {
        var itemNum = parseInt(field.val()) || 0;
        var curPrice = 0;
        var totalPrice = 0;

        var morePrice = parseInt(field.data("morebagprice")) || 0;
        var secondPrice = parseInt(field.data("secondbagprice")) || 0;
        var firstPrice = parseInt(field.data("firstbagprice")) || 0;



//        if(itemNum > 2) {
//            curPrice = morePrice;
//            totalPrice = ((itemNum - 2) * morePrice) + secondPrice + firstPrice;
//        } else if(itemNum === 2) {
//            curPrice = secondPrice;
//            totalPrice = secondPrice + firstPrice;
//        } else if(itemNum === 1) {
//            curPrice = firstPrice;
//            totalPrice = firstPrice;
//        }


        if(totBags >= 2){
            totalPrice = itemNum * morePrice;
        }

        if(totBags === 1){
            if(itemNum >= 2) {
                curPrice = morePrice;
                totalPrice = ((itemNum - totBags) * morePrice) + secondPrice;
            }else if(itemNum === 1) {
                curPrice = secondPrice;
                totalPrice = secondPrice;
            } else {
                curPrice = firstPrice;
            }
        }

        if(totBags === 0){
            if(itemNum > 2) {
                curPrice = morePrice;
                totalPrice = ((itemNum - 2) * morePrice) + secondPrice + firstPrice;
            } else if(itemNum === 2) {
                curPrice = morePrice;
                totalPrice = secondPrice + firstPrice;
            } else if(itemNum === 1) {
                curPrice = secondPrice;
                totalPrice = firstPrice;
            } else {
                curPrice = firstPrice;
            }
        }

        var priceLayer = field.parents("div.flight-content").children(".col_price");
        totalPrice = String(totalPrice).split(".");
        if (totalPrice.length > 1){
            totalPrice = totalPrice[0] + (totalPrice[1] != undefined ? separatore:"") + (totalPrice[1].length > 1? totalPrice[1] : totalPrice[1] + "0" );
        } else {
            totalPrice = totalPrice[0] + (decimale2 != "" ? separatore : "") + decimale2 ;
        }

        var totalPrice2;
        totalPrice2 = splitAdjust(totalPrice);


        priceLayer.text(totalPrice2 + " " + currency);
        priceLayer.attr("data-price", totalPrice);
    }

    $('button.chkwavebaggage[data-quantity="plus"]').click(function (e) {
        $("#feedButton").attr("data-click","true");

        e.preventDefault();
        fieldName = $(this).attr('data-field');
        field = $(this).closest('.input-wrap').find('input[name='+fieldName+']');

        var maxVal = parseInt(field.data("maxitems"));
        var currentVal = parseInt(field.val()) || 0;
        var totBags = parseInt(field.data("totbags"));

        if(!isNaN(maxVal) && currentVal < maxVal) {
            currentVal++;
        }

        field.val(currentVal);
        checkButtons(currentVal, maxVal);
        adjustCurrentBaggagePrice(field,totBags);
        adjustTotalBaggagePrice();
    });
    $('button.chkwavebaggage[data-quantity="minus"]').click(function(e) {
        e.preventDefault();

        fieldName = $(this).attr('data-field');
        field = $(this).closest('.input-wrap').find('input[name='+fieldName+']');
        var totBags = parseInt(field.data("totbags"));

        var maxVal = parseInt(field.data("maxitems"));
        var currentVal = parseInt(field.val()) || 0;
        if (currentVal > 0) {
            currentVal--;
        }
        field.val(currentVal);
        checkButtons(currentVal, maxVal);
        adjustCurrentBaggagePrice(field,totBags);
        adjustTotalBaggagePrice();
    });

    //addToCart
    $('#addBaggageBtn').click(function () {
           if ($("#feedButton").attr("data-click") == "true") {
            if (!$(this).hasClass("disabled")) {
                $(this).addClass("disabled");

            $('#form-checkinwave-baggage').remove();

            var form = $("<form>").attr("id","form-checkinwave-baggage").appendTo("body");
            $('<input>').attr("type", "hidden").attr("name", "numeroPasseggeri").val(chkwaveData.baggage.length).appendTo(form);
            chkwaveData.baggage.forEach(function(item, index){

               //console.log('['+index+'] '+item);
               var bags = [];
               $('input#chkwavefieldbag-' + item.index).each(function(){
                    bags.push(parseInt($(this).val()) + parseInt($(this).data('totbags')));
               });

               $('<input>').attr("type", "hidden").attr("name", "nomePasseggero_"+index).val(item.nomePasseggero).appendTo(form);
               $('<input>').attr("type", "hidden").attr("name", "cognomePasseggero_"+index).val(item.cognomePasseggero).appendTo(form);
               $('<input>').attr("type", "hidden").attr("name", "idPasseggero_"+index).val(item.idPasseggero).appendTo(form);
               $('<input>').attr("type", "hidden").attr("name", "baggage_" + index).val(bags).appendTo(form);

                });
    //            form.append("body");
                enableLoaderCheckin();
                performSubmit('checkinsetbaggage', '#form-checkinwave-baggage', successBaggageProcessing, failureBaggageProcessing).promise().done(function () {
                    $(this).parents().find(".panel-wrap-content .close").first().trigger("click");
                    $(this).parents().find(".cta.cta--primary.cta--icon.cta--edit").first().trigger("click");
                    $("#feedButton").attr("data-click","");

                });
                // $.when(performSubmit('checkinsetbaggage', '#form-checkinwave-baggage', successBaggageProcessing, failureBaggageProcessing)).then(function(){
                //     $(this).parents().find(".panel-wrap-content .close").first().trigger( "click" );
                //     $(this).parents().find(".cta.cta--primary.cta--icon.cta--edit").first().trigger( "click" );
                // });
                // $( document ).ajaxComplete(function(){

                // });
            }
        }
    });

    adjustCurrentBaggagePrice($('input[name=add_bag]'));
    adjustTotalBaggagePrice();
};