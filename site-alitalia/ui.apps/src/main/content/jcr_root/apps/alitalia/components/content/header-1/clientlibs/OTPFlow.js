var closeModal = false;

var OtpModalList = {};

OtpModalList.isModal = function (id){
    var isModal = false;

    switch(id){

        case "needUserSettings":
            isModal = true;
            break;
        case "ma-reset-password-OTP":
            isModal = true;
            break;
        case "ma-check-otp":
            isModal = true;
            break;
        case "ma-reset-password-secretQ":
            isModal = true;
            break;
    }

    return isModal
}

$(document).ready(function () {

    initPrefixOTP();
});

function initPrefixOTP() {
    var requestData = {
        'prefissiNazionali': ""
    };

    getDropdownData(requestData, dropdownDataResetSuccess, dropdownDataResetFail);

}

function dropdownDataResetSuccess(data) {

    var prefix = data.prefissiNazionali;

    for (var i in prefix) {
        $("#prefixForReset").append($("<option>", {
            value: prefix[i].prefix,
            text: prefix[i].description + "(+" + prefix[i].prefix + ")"
        }))
    }

}


function dropdownDataResetFail() {
    alert("fail");
}

$(document).on("closed.zf.reveal", ".reveal--myalitalia-login-section", manageClosingModals);

$(".reveal__header-mobile .icon.icon--close--black").on("click",function (e) {
    if (!closeModal && OtpModalList.isModal($(this).closest(".reveal--myalitalia-login-section").attr("id"))){
        setTimeout(function () {
            showModalOTP()
        }, 0.1);
    }
})

function manageClosingModals(e) {
    if (!closeModal && OtpModalList.isModal(e.target.id)) {
        setTimeout(function () {
            showModalOTP()
        }, 0.1);
    }
}

function showModalOTP() {
    $(".show-if-not-strong").removeClass("hide");
    $("[data-open='needUserSettings']").click();
    $("#prefixForReset").val("");
    $("#telephoneNumber").val("");
}
function showModalOTPReset() {
    $(".show-if-not-strong").addClass("hide");
    $("[data-open='needUserSettings']").click();
    $("#prefixForReset").val("");
    $("#telephoneNumber").val("");
}

$("#ma-otp").on("keyup", function(){
    //Check OTP: invalido la CTA "invia" se ha meno di 6 cifre
    var inp = $(this);
    var isDisabled = !(inp.val() != null && inp.val().length > 5);
    $(".changeEvent").attr("disabled", isDisabled);
});

function checkMAOTP(){
    if($("#ma-otp").val() != null) {
        enableLoaderOtp();
        cleanResetErrors("#ma-otp", "#labelErrorLogin-ma-otp");
        var data = {
            checkOtp : $("#ma-otp").val()
        }
        // performSubmit("MyAlitaliaCheckOtp", data, checkOTPSuccess, checkOTPFail);
        performSubmit("MyAlitaliaCheckOtp", data, MyAlitaliaCheckOtpSuccess, MyAlitaliaCheckOtpError);
    } else {
        setResetErrors("#ma-otp", "#labelErrorLogin-ma-otp", CQ.I18n.get("myalitalia.fieldObligatory"));
    }
}

function MyAlitaliaCheckOtpSuccess(response) {
    $("#ma-otp").val("");
    $(".changeEvent").attr("disabled", true);
    if (!response.isError){
        closeModal = true;
        if (isResetOtpPsw){
            isResetOtpPsw = false;
            var data = {
                email:$("#emailForReset").val(),
                retrieve: "true"
            };
            performSubmit("MyAlitalia-resetPsw",data,isAvailableIdResetSuccess,isAvailableIdErrorSuccess);
        } else {
            $("[data-open='success']").click();
        }
    } else {
        if (response.countError < 3 ){
            setResetErrors("#ma-otp", "#labelErrorLogin-ma-otp", CQ.I18n.get("myalitalia.checkOTP.newOTPSent"));
            disableLoaderOtp();
        } else {
            if (isResetOtpPsw){
                $("#emailForReset").val("");
                setResetErrors(null, "labelErrorLogin-ma-reset", CQ.I18n.get("myalitalia.checkOTP.tooAttempts"));
                $("a[data-open='ma-reset-password']").trigger("click");
            } else {
                setResetErrors(null, "#labelErrorLogin-ma-nus-sms-generic", CQ.I18n.get("myalitalia.checkOTP.tooAttempts"));
                $("#ma-check-otp .close-button").click();
            }
            disableLoaderOtp();
        }
    }
}

function MyAlitaliaCheckOtpError() {

}

function MyAlitaliaSendOtpSuccess(data) {
    if (!data.isError){
        closeModal = true;
        $("#needUserSettings .close-button").click();
        closeModal = false;
        $(".cotpDescr").text(CQ.I18n.get("myalitalia.OTP.modal.SMStitle", $("#telephoneNumber").val()));
        $(".titleCOTP").text(CQ.I18n.get("myalitalia.needUserSettings.title"));
        $("[data-open='ma-check-otp']").click();

        disableLoaderOtp();
    } else {
        setResetErrors(null, "#labelError-genericNUS", CQ.I18n.get("myalitalia.otp.serverError"));
        disableLoaderOtp();
    }
}

function MyAlitaliaSendOtpError() {
    setResetErrors(null, "#labelError-genericNUS", CQ.I18n.get("myalitalia.otp.serverError"));
    disableLoaderOtp();
}

function sendOtpMyAlitalia() {
    var prefix = $("#prefixForReset");
    var num = $("#telephoneNumber");
    cleanResetErrors(prefix, "#labelErrorLogin-ma-nus-sms-prefix");
    cleanResetErrors(num, "#labelErrorLogin-ma-nus-sms-number");

    if (prefix.val() != "") {
        if (num.val() != "") {
            enableLoaderOtp();
            var data = {
                prefix : prefix.val(),
                num : num.val(),
            };
            performSubmit("MyAlitaliaSendOtp",data,MyAlitaliaSendOtpSuccess,MyAlitaliaSendOtpError)
        } else {
            setResetErrors(num, "#labelErrorLogin-ma-nus-sms-number", CQ.I18n.get("myalitalia.fieldObligatory"));
        }
    } else {
        setResetErrors(prefix, "#labelErrorLogin-ma-nus-sms-prefix", CQ.I18n.get("myalitalia.fieldObligatory"))
    }
}

function setResetErrors(input, label, message) {
    if (input != null) $(input).addClass("is-invalid-input");
    if (label != null && message != null) {
        $(label).find("p").text(message);
        $(label).removeClass("hide");
    }
}

function cleanResetErrors(input, label) {
    if (input != null) $(input).removeClass("is-invalid-input");
    if (label != null) {
        $(label).find("p").text("");
        $(label).addClass("hide");
    }
}

function enableLoaderOtp(){
    var loader = "";
    loader += "<div class='Dloader Dloading otpLoader'>";
    loader += "  <div class='fade-circle'></div>";
    loader += "</div>";
    var modal = $(".reveal[aria-hidden='false']").attr("id");
    var id = modal;
    if(modal != null) {
        modal = $("#" + modal).find(".reveal__inner");
        if(modal != null) modal.append(loader);
    }

    $(modal).append(loader);
    setTimeout(function() { disableLoaderOtp() }, 15000); //Chiusura automatica dei Loader in modale allo scadere del timeout

}

function disableLoaderOtp(){
    $(".otpLoader").remove();
}