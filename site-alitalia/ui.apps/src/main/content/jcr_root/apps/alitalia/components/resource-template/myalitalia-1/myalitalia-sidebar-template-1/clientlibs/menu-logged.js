$(document).ready(function() {
    var url = new URL(location.href);

    var page = url.searchParams.get("page");
    var isAssociated = url.searchParams.get("isAssociated");

    var current = $("[data-page='" + page + "']");

    if(isAssociated != null && (isAssociated == true || isAssociated == "true")) {
        $(".millemiglia.side-menu").trigger("click");
    }

    if(page != null && page != "") {
        current.removeAttr("href");
        current.trigger("click");
        if(current.parents(".side-menu").length > 0)
            current.parents(".side-menu").find("ul").removeClass("hide");
    }
});


$(".ma-menu").click(function(e) {
    e.stopPropagation();
    $(".side-menu").removeClass("selected");
    $(".ma-menu").removeClass("custom-selected");
    if($(this).hasClass("custom-selected")) $(this).removeClass("custom-selected");
    else $(this).addClass("custom-selected");
});

$(".side-menu").on("click", function(){
    $(".side-menu").removeClass("selected");
    $(".ma-menu").removeClass("custom-selected");
    $(this).addClass("selected");
    if($(this).find("ul").hasClass("hide")) $(this).find("ul").removeClass("hide");
    else $(this).find("ul").addClass("hide");
})
