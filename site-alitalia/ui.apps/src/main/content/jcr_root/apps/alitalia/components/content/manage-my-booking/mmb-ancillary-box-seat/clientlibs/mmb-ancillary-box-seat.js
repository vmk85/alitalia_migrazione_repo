
function initPartialMmbSeat() {
	if (window['getInitialBookingSeatSelections']) {
		window.bookingSeatSelections = window['getInitialBookingSeatSelections'](); // defined by template script
		initSeatMap();
	}
}

function initSeatMap(){
	var __bookingSeat = $('.chooseSeat__container');
	if (__bookingSeat.length > 0){
		for (var j=0;j<__bookingSeat.length;j++){
			new BookingSeat($(__bookingSeat[j]));
		}
	}
	if ($('.j-chooseSeatSlider').length > 0){
		bookingSeatSlider.init();
	}
}


/* modify form */

function getBookingSeatSelectionsFormData() {
	var seatSelectionParams =  [];
	for (var i = 0; i < window.bookingSeatSelections.length; i++) {
		var seatSelection = window.bookingSeatSelections[i];
		if (seatSelection.isChangeable == "true" 
					&& seatSelection.seatNumber != ""
					&& seatSelection.seatNumber != seatSelection.previousSeatNumber) {
			var seatSelectionParam = seatSelection.ancillaryIds + "#" + seatSelection.seatNumber;
			seatSelectionParams.push(seatSelectionParam);
		}
	}
	var form = { 'seat-selection': seatSelectionParams };
	return form;
}

function be_addSeats(e, btn, section, callback) {
	performValidation('mmbancillarycartseats', getBookingSeatSelectionsFormData(), 
			mmbAncillarySeatModifyValidationSuccess, mmbHandleInvokeError);
};

function mmbAncillarySeatModifyValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('mmbancillarycartseats', getBookingSeatSelectionsFormData(), 
				mmbAncillarySeatModifySubmitSuccess, mmbHandleInvokeError);
	} else {
		showErrors(data);
	}
}

function mmbAncillarySeatModifySubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		if (data.isError) {
			mmbHandleServiceError(data);
		}
		refreshMmbPartialSeat(true, true);
	}
}
