var myAlitaliaImpostazioniOptions = {};

// document ready
$( document ).ready( function() {
    // enableLoaderMyAlitalia();
	myAlitaliaImpostazioniOptions.initReveal();

	// init toggle
    $( '[data-toggle="expand"]' ).on( 'click', function(e) {
        e.preventDefault();
        $( this ).hide();
        $( this ).closest( '.action' ).find(".form-wrap").slideDown();
        $(this).closest(".action.action--expandible").children(".cta-wrap").children("a").hide()
        $('html,body').animate({scrollTop:$( this ).closest( '.floating-card' ).offset().top -10},"slow");

    } );
    $( '[data-close]' ).on( 'click', function(e) {
        e.preventDefault();
        $( this ).closest( '.action' ).find(".form-wrap").slideUp().end().closest( '.action' ).find(".cta--icon").show();
        $('[data-toggle="expand"]').show()

    } );

	// reveal pass
	$( '.pw-reveal' ).on( 'click', function() {
		if( $( this ).parent().find('input').attr( 'type' ) == 'password' ) {
			$( this ).parent().find('input').attr( 'type', 'text' );
		} else {
			$( this ).parent().find('input').attr( 'type', 'password' );
		}
	} );


    $(document).on("click", "[data-auth-2]", function (event) {
        var current = $(this).attr("data-auth-2");
        // if(current>3)$("[data-auth]:eq("+(current-1)+")").trigger("click");
        if (!($("#" + retriveIdByDataAuth(current)).is(":disabled"))) {
            if (current == 4) {
                enableCheck("profiling-yes");
                enableCheck("profiling-no");
            } else if (current == 5) {
                enableCheck("news-alitaliaAndPartners");
                enableCheck("profiling-no");
                enableCheck("news-alitalia");
            } else if (current == 7) {
                disableCheck("profiling-yes");
                enableCheck("profiling-no");
            } else if (current == 8) {
                enableCheck("news-alitalia");
                enableCheck("news-alitaliaAndPartners");
            }
        }
    });
});


function disableCheck(id) {
    document.getElementById(id).disabled = true;
    $("label[for="+id+"]").css("color","#c1c1c1");
    if ($("#"+id).is(":checked")){
        $("#"+id).removeAttr('checked').parent(".radio-wrap").removeClass("checked");
    }
    var inputs_disabled = $("input#"+id).parent().parent().find("input:not(:disabled)");
    inputs_disabled.each(function(i, obj) {
         $(obj).attr('checked', true);
         $(obj).parent(".radio-wrap").addClass("checked");
    });

}

function enableCheck(id) {
    document.getElementById(id).disabled = false;
    $("label[for="+id+"]").css("color","black");

}

function retriveIdByDataAuth(dataAuth) {
    var id = "";
    switch (parseInt(dataAuth)) {
        case 3:
            id = "consenso-dati-personali-si";
            break;
        case 4:
            id = "news-alitalia";
            break;
        case 5:
            id = "profiling-yes";
            break;
        case 6:
            id = "consenso-dati-personali-no";
            break;
        case 7:
            id = "news-alitaliaAndPartners";
            break;
        case 8:
            id = "profiling-no";
            break;
    }
    return id;
}

myAlitaliaImpostazioniOptions.initReveal = function() {
	// init reveal
	$( '#reveal-nickname-pin, #mm-reveal-nickname-pin, #reveal-social-login, #mm-reveal-social-login, #my-reveal-social-login' ).foundation();
};

function popolateFlag(listaConsensi) {

    for(var i=0; i<listaConsensi.length; i++){
        var consenso = listaConsensi[i];

        if(consenso.tipoConsenso == "COM"){
            if (consenso.flagConsenso){
                $("#news-alitalia").click();
            } else {
                $("#news-alitaliaAndPartners").click();
            }

        }

        if(consenso.tipoConsenso == "PRN"){
            if (consenso.flagConsenso){
                $("#profiling-yes").click();
            } else {
                $("#profiling-no").click();
            }

        }

    }

}

function popolatePreferences(canaleComunicazionePreferito) {

    $("#notification-email").click();
    $("#notification-sms").click();
    $("#notification-app").click();

    for(var i=0; i<canaleComunicazionePreferito.length; i++){
        var canale = canaleComunicazionePreferito[i];

        switch (canale.tipo){
            case"MAIL":
                $("#notification-email").click();
                break;
            case"SMS":
                $("#notification-sms").click();
                break;
            case"PUSH":
                $("#notification-app").click();
                break;
        }
    }

}

function getLingua(linguaComunicazioniCommerciali) {
    var lingua;
    switch (linguaComunicazioniCommerciali){
        case"IT":
            lingua="Italiano";
            break;
        case"FR":
            lingua="Francese";
            break;
        case"JA":
            lingua="Giapponese";
            break;
        case"EN":
            lingua="Inglese";
            break;
        case"ES":
            lingua="Spagnolo";
            break;
        case"DE":
            lingua="Tedesco";
            break;
        case"PT":
            lingua="Portoghese";
            break;
        default:
            lingua = "Italiano";
            break;
    }
    return lingua;
}

function popolateLLanguage(linguaComunicazioniCommerciali) {
    $("#linguaCorrispondenza").val(getLingua(linguaComunicazioniCommerciali));
}

function popolateConsensiOption(response) {
    popolateFlag(response.listaConsensi);
    popolatePreferences(response.canaleComunicazionePreferito);
    popolateLanguage(response.preferenzePersonali.linguaComunicazioniCommerciali);
}

function changeTitle(){
    $("#titleOtp").text(CQ.I18n.get("myalitalia.otp.options"));
}

