var cercaVoloAutocomplete = {};
var originIataCode = "";
var originTimetablesIataCode = "";
var destinationIataCode = "";
var destinationTimetablesIataCode = "";
var pageInitialized = false;
var allAirports;
var clickFromCloseOverlay=false;

$(document).ready(function () {

    // toggle visibility of clear autocomplete button
    $('[data-clear-autocomplete] input').on('keyup focus', function () {
        if ($(this).val() !== '') {
            $(this).parent().find('.clear-input').removeClass('hide');
        } else {
            $(this).parent().find('.clear-input').addClass('hide');
        }
    }).on('blur', function (e) {
        //    if(!$(e.relatedTarget).hasClass('clear-input')) {
        //      $( this ).parent().find( '.clear-input' ).addClass( 'hide' );
        //    }
    });

    // clear autocomplete input
    $('[data-clear-autocomplete] .clear-input').on('click', function () {
        $(this).parent().find('input').val('').focus();

        if ($(this).closest('form').find('#luogo-arrivo--prenota-mobile').length > 0 && $('#departureAirportCode--prenota').val().length == 3) {
            $('.toggle-all-destination-mobile').removeClass('hide');
            $('.all-destination').addClass('hide');
        }
    });

    $("#data-andata--prenota-desk").on("blur", function () {
        currentFocusDatePicker = undefined;

    });

    $("#data-ritorno--prenota-desk").on("blur", function () {
        currentFocusDatePicker = undefined;
    });

    $("#departure_date--timetables-desk").on("blur", function () {
        currentFocusDatePicker = undefined;
    });

    $("#luogo-partenza--prenota-desk").change(function () {
        if ($(this).val().trim() == "") {
            $('#originCountry').val("");
            $('#airCodeOrigin').val("");
        }

    });
    $("#luogo-ritorno--prenota-desk").change(function () {
        if ($(this).val().trim() == "") {
            $('#destCountry').val("");
            $('#airCodeDest').val("");
        }
    });


});

function initCercaVoloAutocomplete(allAirports) {
    if (!pageInitialized) {
        cercaVoloAutocomplete.allAirports = allAirports;
        pageInitialized = true;
    }

    // init prenota autocomplete desk partenza
    cercaVoloAutocomplete.initAutocomplete(allAirports, '#luogo-partenza--prenota-desk', '#suggestion_partenza--prenota-desk');
    // init prenota autocomplete desk ritorno
    cercaVoloAutocomplete.initAutocomplete(allAirports, '#luogo-arrivo--prenota-desk', '#suggestion_ritorno--prenota-desk');
    // init prenota autocomplete mobile partenza
    cercaVoloAutocomplete.initAutocomplete(allAirports, '#luogo-partenza--prenota-mobile', '#suggestion_andata--prenota-mobile');
    // init prenota autocomplete mobile ritorno
    cercaVoloAutocomplete.initAutocomplete(allAirports, '#luogo-arrivo--prenota-mobile', '#suggestion_ritorno--prenota-mobile');
    // init info voli timetables autocomplete desk partenza
    cercaVoloAutocomplete.initAutocomplete(allAirports, '#luogo-partenza--timetables-desk', '#suggestion_partenza--timetables-desk');
    // init info voli timetables autocomplete desk ritorno
    cercaVoloAutocomplete.initAutocomplete(allAirports, '#luogo-arrivo--timetables-desk', '#suggestion_ritorno--timetables-desk');
    // init info voli timetables autocomplete mobile partenza
    cercaVoloAutocomplete.initAutocomplete(allAirports, '#luogo-partenza--timetables-mobile', '#suggestion_partenza--timetables-mobile');
    // init info voli timetables autocomplete mobile ritorno
    cercaVoloAutocomplete.initAutocomplete(allAirports, '#luogo-arrivo--timetables-mobile', '#suggestion_arrivo--timetables-mobile');
}

cercaVoloAutocomplete.filterAirports = function (airportsArray, pattern) {
    if (airportsArray) {
        if (airportsArray.airports) {
            var newArrayAirports = airportsArray.airports.filter(function (el) {
                return el.value.toLowerCase().indexOf(pattern) !== -1;
            });

        }
        return newArrayAirports;
    }
}
cercaVoloAutocomplete.initAutocomplete = function (suggestions, inputID, suggestionWrap) {
    var searchChar = getIndiceLinguaggio(); //vale 2 in caso di mercato cinese, giapponese, coreano o israeliano e 3 in tutti gli altri casi
    $(inputID).autocomplete({
        minChars: searchChar,
        preserveInput: false,
        appendTo: $(suggestionWrap),
        autoSelectFirst: true,
        lookup: function (query, done) {
            // format json for autocomplete plugin & filter based on current query
            var resultsFiltered = {
                suggestions: suggestions.airports.filter(function (obj) {
                    return (
                        removeAccents(obj.code.toLowerCase()) == removeAccents(query.toLowerCase()) ||
                        removeAccents(obj.city.toLowerCase().substr(0, query.length)) == removeAccents(query.toLowerCase()) ||
                        removeAccents(obj.airport.toLowerCase().substr(0, query.length)) == removeAccents(query.toLowerCase())
                    );
                }).map(function (curr, inde, arr) {
                    curr.value = curr.city + ' ' + curr.code;
                    return curr;
                })
            };

            // callback to pass results to the lookup
            done(resultsFiltered);

        },
        formatResult: function (suggestion, currentValue) {
            // format the returned suggestion string
            return suggestion.city + ' (' + suggestion.country + ') ' + suggestion.airport + ' <strong>' + suggestion.code + '</strong>';
        },
        beforeRender: function () {

            $('.suggestion_new-box').addClass('hide');
            // remove built-in inline style from the suggestion-wrap
            $('.suggestion_new-box .autocomplete-suggestions').removeAttr('style');
            // show current suggestion-wrap
            $(suggestionWrap).removeClass('hide');
        },
        onSelect: function (suggestion) {

            // hide suggestion-wrap
            $('.suggestion_new-box').addClass('hide');

            // function to pass selected data
            var thisName = $(inputID).attr('name');

            // In base al campo selezionato cambio la lista aereoporti della destinazione
            var isMobile = $(inputID).attr('id').indexOf("mobile") > 0 ? true : false;

            cercaVoloAutocomplete.setAirportInputValue(thisName, suggestion.city, suggestion.code, suggestion.country, suggestion.type, isMobile);

        }
    });

};

cercaVoloAutocomplete.setAirportInputValue = function (inputName, currCity, currCode, inputValueCountry, inputValueType, isMobile) {
    var destination = CQ.I18n.get('cercaVolo.label.destinazione');
    var origin = CQ.I18n.get('cercaVolo.label.partenza');
    var destinationAirport = CQ.I18n.get('preparaViaggio.aeroportoArrivo.label');
    var originAirport = CQ.I18n.get('preparaViaggio.aeroportoPartenza.label');
    var origDest;

    $('input[name="' + inputName + '"]').val(currCity + ' ' + currCode).trigger('change');
    cercaVoloAutocomplete.lastIataCode = currCode;
    cercaVoloAutocomplete.lastAirport = currCity;
    console.log("Setto valore " + inputName);
    // set IATA code in hiddenForm
    switch (inputName) {
        case 'list_andata--prenota':
            origDest = origin;
            cercaVoloAllDestinationsOptions.getAvailableAirport(currCode);
            $('#departureAirportCode--prenota').val(currCode).trigger('change');
            originIataCode = currCode;
            cercaVoloAutocomplete.setIataColor(origDest);
            $("#originCountry").val(inputValueCountry);
            $("#airCodeOrigin").val(currCode);
            if (inputValueType != '')
                $('input[name="list_andata--prenota"]').attr('data-type', inputValueType);

            var city = $('#span_luogo-arrivo--prenota-desk').text().slice(0, -3);
            var destinationCode = $('#span_luogo-arrivo--prenota-desk').text().slice(-3);
            if (destinationIataCode) {
                cercaVoloAutocomplete.getJourneyslist(destinationIataCode);
                var found = cercaVoloAutocomplete.findJourney(originIataCode);
            }
            if ((currCode == destinationCode) || (currCity == city) || !found) {
                cercaVoloAutocomplete.resetDestination();
            }

            cercaVoloAutocomplete.resetErrors($('input[name="list_andata--prenota"]'),"cercavoloprenota");

            if (Foundation.MediaQuery.atLeast('large')) {
                cercaVoloOptions.currValDep = currCode;
            }

            checkValues();
            break;
        case 'destination--prenota':
            origDest = destination;
            $('#arrivalAirportCode--prenota').val(currCode);
            if (Foundation.MediaQuery.atLeast('large')) {
                cercaVoloOptions.currValArr = currCode;
            }
            cercaVoloAutocomplete.setIataColor(origDest);
            $('#arrivalAirportCode--prenota').val(currCode).trigger('change');
            destinationIataCode = currCode;
            var city = $('#luogo-partenza--prenota-desk').val().slice(0, -3);
            var originCode = $('#airCodeOrigin').val();
            $("#destCountry").val(inputValueCountry);
            $("#airCodeDest").val(currCode);
            $('input[name="destination--prenota"]').attr('data-type', inputValueType);
            if (currCode) {
                cercaVoloAutocomplete.getJourneyslist(originCode);
                var found = cercaVoloAutocomplete.findJourney(currCode);
            }
            if ((currCode == originCode) || (currCity == city) || !found) {
                cercaVoloAutocomplete.resetOrigin();
            }
            cercaVoloAutocomplete.resetErrors($('input[name="destination--prenota"]'),"cercavoloprenota");

            checkValues();
            break;
        case 'list_andata--timetables':
            origDest = originAirport;
            $('#departureAirportCode--timetables').val(currCode);
            cercaVoloAutocomplete.setIataColor(origDest);
            originTimetablesIataCode = currCode;
            // originTimetablesIataCode = inputValueIATA;

            var city = $('span.input-placeholder[data-placeholder="' + destinationAirport + '"]').text().slice(0, -4);
            var destinationCode = $('span.input-placeholder[data-placeholder="' + destinationAirport + '"]').text().slice(-3);
            if ((currCode == destinationCode) || (currCity == city)) {
                cercaVoloAutocomplete.resetDestination("timetables");
            }

            break;
        case 'destination--timetables':
            origDest = destinationAirport;
            cercaVoloAutocomplete.setIataColor(origDest);
            $('#arrivalAirportCode--timetables').val(currCode);
            destinationTimetablesIataCode = currCode;
            var city = $('span.input-placeholder[data-placeholder="' + originAirport + '"]').text().slice(0, -4);
            var originCode = $('span.input-placeholder[data-placeholder="' + originAirport + '"]').text().slice(-3);
            if ((currCode == originCode) || (currCity == city)) {
                cercaVoloAutocomplete.resetOrigin("timetables");
            }
            // Se mobile devo nascondere il pannello
            break;
    }

    if (isMobile)
        cercaVoloPrenotaOptions.closeAllCollapsablePanels();

    // split value to make IATA code bold
    $('input[name="' + inputName + '"]').each(function (i, el) {
        $(el).parent().find('.input-placeholder').html(currCity + '<strong class="iata-code">' + currCode + '</strong>');
    });

    if (originIataCode != "" && destinationIataCode != "") {
        return cercaVoloAutocomplete.getOeDAvailability(originIataCode, destinationIataCode);
    }
    if (originTimetablesIataCode != "" && destinationTimetablesIataCode != "") {
        return cercaVoloAutocomplete.getOeDAvailability(originTimetablesIataCode, destinationTimetablesIataCode);
    }

    // if input partenza
    if (Foundation.MediaQuery.atLeast('large')) {

        $('input[name="' + inputName + '"]').each(function (i, el) {

            if ($(el).attr('id') == 'luogo-partenza--prenota-desk') {

                if ($(el).data('first') != '1') {
                    if (!clickFromCloseOverlay) {
                        $('#luogo-arrivo--prenota-desk').focus();
                    }
                    if ($('#luogo-arrivo--prenota-desk').val() === '' && !clickFromCloseOverlay) {
                        $('.toggle-all-destination-desk').removeClass('hide');
                    }else{
                        clickFromCloseOverlay=false;
                    }
                }
                $(el).data('first', "0");
            }


        });


    }
};


cercaVoloAutocomplete.resetDestination = function (type) {
    if (type == "timetables") {
        cercaVoloAutocomplete.listDestinationTimetablesAirports = undefined;
        //$('span.input-placeholder[data-placeholder="Aeroporto di arrivo"').html("");
        var label = CQ.I18n.get('preparaViaggio.aeroportoArrivo.label');
        $('span[data-toggle-panel="#panel-destination--timetables"]').html(label);

        $('#luogo-arrivo--timetables-desk').val("");
        $('#luogo-arrivo--timetables-mobile').val("");
        $('#arrivalAirportCode--timetables').val("")
        $('#arrivalAirport--timetables').val("");
        $('#arrivalAirportCode--timetables').val("");


    } else {
        cercaVoloAutocomplete.listDestinationAirports = undefined;

        $('#luogo-arrivo--prenota-mobile').val("");
        $('#luogo-arrivo--prenota-desk').val("");
        $('#luogo-arrivo--prenota-mobile').attr('data-type', "");
        $('#luogo-arrivo--prenota-desk').attr('data-type', "");
        $('#airCodeDest').val("");
        $('#destCountry').val("");
        $('#arrivalAirportCode--prenota').val("");
        $('#arrivalAirport--prenota').val("");


        var label = CQ.I18n.get('cercaVolo.label.destinazione');
        $('span.input-placeholder[data-toggle-panel="#panel-all-destination"]').html(label);
        // $('#arrivalAirport--prenota').val('Destinazione');
    }
};

cercaVoloAutocomplete.resetOrigin = function (type) {
    if (type == "timetables") {
        cercaVoloAutocomplete.listOriginTimetablesAirports = undefined;
        //$('span.input-placeholder[data-placeholder="Aeroporto di arrivo"').html("");
        var label = CQ.I18n.get('preparaViaggio.aeroportoPartenza.label');
        $('span[data-toggle-panel="#panel-list_andata--timetables"]').html(label);


        $('#luogo-partenza--timetables-desk').val("");
        $('#luogo-partenza--timetables-mobile').val("");
        $('#departureAirportCode--timetables').val("")
        $('#departureAirport--timetables').val("");
        $('#departureAirportCode--timetables').val("");


    } else {
        cercaVoloAutocomplete.listOriginAirports = undefined;

        $('#luogo-partenza--prenota-mobile').val("");
        $('#luogo-partenza--prenota-desk').val("");
        $('#luogo-partenza--prenota-mobile').attr('data-type', "");
        $('#luogo-partenza--prenota-desk').attr('data-type', "");
        $('#airCodeOrigin').val("");
        $('#originCountry').val("");
        $('#departureAirportCode--prenota').val("");
        $('#departureAirport--prenota').val("");


        var label = CQ.I18n.get('cercaVolo.label.partenza');
        $('span.input-placeholder[data-toggle-panel="#panel-list_andata--prenota"]').html(label);
        // $('#arrivalAirport--prenota').val('Destinazione');
    }
};
cercaVoloAutocomplete.reverseFieldsPrenota = function (field, iata) {
    var destination = CQ.I18n.get('cercaVolo.label.destinazione');
    var origin = CQ.I18n.get('cercaVolo.label.partenza');
    //var field = CQ.I18n.get('field');
    var helperArray = cercaVoloAutocomplete.listOriginAirports;
    cercaVoloAutocomplete.listOriginAirports = cercaVoloAutocomplete.listDestinationAirports;
    cercaVoloAutocomplete.listDestinationAirports = helperArray;
    if (field == origin) {
        if (iata) {
            originIataCode = iata;

        }
    } else if (field == destination) {
    } else {
        originIataCode = iata;
    }

    //inverti O&D
    if(typeof(disabledDaysJson) != 'undefined') {
    	var tmp_1=disabledDaysJson.availability;
        var tmp_2=disabledDaysJson.availability_ret;
        disabledDaysJson.availability=tmp_2;
        disabledDaysJson.availability_ret=tmp_1;
    }

};
cercaVoloAutocomplete.getCityCode = function (iata) {
    var res;
    if (iata == "ROM") {
        res = iata;
    }
    if (iata) {
        if (iata.length > 3) {
            iata = iata.split('.')[1]
        }
        if (cercaVoloAutocomplete.allAirports) {
            cercaVoloAutocomplete.allAirports.airports.filter(function (obj) {
                if ($.trim(iata) == $.trim(obj.code)) {
                    res = obj.type;
                }
            })[0];
        }
    }
    return res;
}

function getOfferDetails(codeOr, codeDe) {
    var params = {
        from: codeOr,
        to: codeDe
    };
    console.log(params);
    $.ajax({
        url: getServiceUrl('offerdetail'),
        data: params,
        async: true,
        context: document.body
    })
        .done(function (data) {
            if (data && data.result && data.result.international) {
                cercaVoloOptions.histogramCounter = 1;
                cercaVoloCrea.showHistogram = true;
                makeHistogram(data);
            } else {
                makeHistogram(false);
                setTimeout(function () {
                    $('#panel-histogram').css('display', 'none');
                }, 100);
            }
        })
        .fail(function () {
            makeHistogram(false);
        });
}

function checkValues() {
    if ($('input[name="list_andata--prenota"]').attr('data-type') != "" && $('input[name="destination--prenota"]').attr('data-type') != "") {
        var origin = cercaVoloAutocomplete.getCityCode($('input[name="list_andata--prenota"]').attr('data-type'));
        var dest = cercaVoloAutocomplete.getCityCode($('input[name="destination--prenota"]').attr('data-type'));

        //evita di chiamare il servizio per la versione "mobile" e quando non sono definite (entrambe) origine e destinazione
        var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;

        if (!isMobile && origin != undefined && dest != undefined && origin.length == 3 && dest.length == 3) {
            getOfferDetails(origin, dest);
        }

    }
}

cercaVoloAutocomplete.setIataColor = function (type) {
    var destination = CQ.I18n.get('cercaVolo.label.destinazione');
    var origin = CQ.I18n.get('cercaVolo.label.partenza');
    if ($('span.input-placeholder[data-placeholder="' + type + '"]').html() != destination && $('span.input-placeholder[data-placeholder="' + type + '"]').html() != origin) {
        var string1 = $('span.input-placeholder[data-placeholder="' + type + '"]').html().slice(0, -4);
        var string2 = $('span.input-placeholder[data-placeholder="' + type + '"]').html().slice(-3);
        $('span.input-placeholder[data-placeholder="' + type + '"]').html(string1 + ' <span  style="color:#006643;"><strong>' + string2 + '</strong></span>');
    }
}

cercaVoloAutocomplete.resetErrors = function (inp,type) {
    $(inp).removeClass('is-invalid-input');
    $(inp).prev('span').removeClass('is-invalid-input');
    $(inp).next('span').removeClass('is-invalid-input');
    if (type == "cercavoloprenota"){
        if ($('#data-ritorno--prenota-desk').hasClass('is-invalid-input') || $('#data-andata--prenota-desk').hasClass('is-invalid-input')) {
        }else{

            $('#labelErrorInfoStatoVolo').html();
            $('#labelErrorInfoStatoVolo').hide();
        }
    }else{
        $('#labelErrorInfoStatoVolo').html();
        $('#labelErrorInfoStatoVolo').hide();
    }



}
cercaVoloAutocomplete.setAirportInputValueX = function (inputName, inputValue, inputValueIATA, inputValueCountry, inputValueType) {
    var destination = CQ.I18n.get('cercaVolo.label.destinazione');
    var origin = CQ.I18n.get('cercaVolo.label.partenza');
    var destinationAirport = CQ.I18n.get('preparaViaggio.aeroportoArrivo.label');
    var originAirport = CQ.I18n.get('preparaViaggio.aeroportoPartenza.label');
    var origDest;
    if (inputName == "destination--prenota") {
        origDest = destination;
    } else if (inputName == "list_andata--prenota") {
        origDest = origin;
    } else if (inputName == "destination--timetables") {
        origDest = destinationAirport;
    } else if (inputName = "list_andata--timetables") {
        origDest = originAirport;
    }
    $('input[name="' + inputName + '"]').val(inputValue + " " + inputValueIATA).trigger('change');

    if (inputName == 'destination--prenota') {
        cercaVoloAutocomplete.setIataColor(origDest);
        $('#arrivalAirportCode--prenota').val(inputValueIATA).trigger('change');
        destinationIataCode = inputValueIATA;
        $("#destCountry").val(inputValueCountry);
        $("#airCodeDest").val(inputValueIATA);
        $('input[name="destination--prenota"]').attr('data-type', inputValueType);
        cercaVoloAutocomplete.resetErrors($('input[name="destination--prenota"]'));

        checkValues();
    }

    if (inputName == 'list_andata--prenota') {
        cercaVoloAllDestinationsOptions.getAvailableAirport(inputValueIATA);
        $('#departureAirportCode--prenota').val(inputValueIATA).trigger('change');
        originIataCode = inputValueIATA;
        cercaVoloAutocomplete.setIataColor(origDest);
        $("#originCountry").val(inputValueCountry);
        $("#airCodeOrigin").val(inputValueIATA);
        $('input[name="list_andata--prenota"]').attr('data-type', inputValueType);
        var city = $('span.input-placeholder[data-placeholder="' + destination + '"]').text().slice(0, -4);
        var destinationCode = $('span.input-placeholder[data-placeholder="' + destination + '"]').text().slice(-3);
        if (destinationIataCode) {
            cercaVoloAutocomplete.getJourneyslist(destinationIataCode);
            var found = cercaVoloAutocomplete.findJourney(originIataCode);
        }
        if ((inputValueIATA == destinationCode) || (inputValue == city) || !found) {
            cercaVoloAutocomplete.resetDestination();
        }
        cercaVoloAutocomplete.resetErrors($('input[name="list_andata--prenota"]'));
        checkValues();
    }

    if (inputName == 'list_andata--timetables') {
        cercaVoloAutocomplete.setIataColor(origDest);
        $('#departureAirportCode--timetables').val(inputValueIATA).trigger('change');
        originTimetablesIataCode = inputValueIATA;

        var city = $('span.input-placeholder[data-placeholder="' + destinationAirport + '"]').text().slice(0, -4);
        var destinationCode = $('span.input-placeholder[data-placeholder="' + destinationAirport + '"]').text().slice(-3);
        if ((inputValueIATA == destinationCode) || (inputValue == city)) {
            cercaVoloAutocomplete.resetDestination("timetables");
        }
    }


    if (inputName == 'destination--timetables') {
        cercaVoloAutocomplete.setIataColor(origDest);
        $('#arrivalAirportCode--timetables').val(inputValueIATA).trigger('change');
        destinationTimetablesIataCode = inputValueIATA;
    }
    $('.suggestion-box').empty();
    //fare chiamata O&D
    if (originIataCode != "" && destinationIataCode != "") {
        return cercaVoloAutocomplete.getOeDAvailability(originIataCode, destinationIataCode);
    }
    if (originTimetablesIataCode != "" && destinationTimetablesIataCode != "") {
        return cercaVoloAutocomplete.getOeDAvailability(originIataCode, destinationIataCode);
    }
};

cercaVoloAutocomplete.findJourney = function (iata) {
    var found = false;
    var list = cercaVoloAutocomplete.listPossibleJourneys.airports,
        listLength = list.length;
    console.log("LIST listPossibleJourneys LENGHT", listLength);
    for (var i = 0; i < listLength; i++) {
        if ($.trim(list[i].code) === $.trim(iata)) {
            found = true;
        }
    }
    return found;
}
cercaVoloAutocomplete.getOeDAvailability = function (originIataCode, destinationIataCode) {
    var availabilityUrl = getServiceUrl("oedavailabilityconsumer_ret");
    $.ajax({
        url: availabilityUrl,
        type: 'GET',
        dataType: 'json',
        async: true,
        data: {originIATA: originIataCode, destinationIATA: destinationIataCode},
        success: function (data) {
            setDisabledDaysFrom(data);
        }
    });
}
cercaVoloAutocomplete.getJourneyslist = function (destinationIATA) {
    $.ajax({
        //url: getServiceUrl('airportdestinationconsumer'),
        url: getServiceUrl_1('airportdestinationconsumer', destinationIATA),
        dataType: 'json',
        async: false,
        //data: {iata: destinationIATA},
        success: function (data) {
            cercaVoloAutocomplete.listPossibleJourneys = data;
            return data;
        }
    });
}
cercaVoloAutocomplete.findAirport = function (query, results) {
    var size = 7;
    var airportResults = {};
    airportResults.suggestions = [];
    airportResults.keys = [];
    var regExString = "^" + query + ".*";
    jQuery.each(results, function (i, v) {
        if (airportResults.keys.length < size
            && v.city.search(new RegExp(regExString, "i")) != -1) {
            if (-1 === airportResults.keys.indexOf(v.code)) {
                airportResults.keys.push(v.code);
                var c = v;
                airportResults.suggestions.push(copyValue(v));
            }
        }
    });
    jQuery.each(results, function (i, v) {
        if (airportResults.keys.length < size
            && v.airport.search(new RegExp(regExString, "i")) != -1) {
            if (-1 === airportResults.keys.indexOf(v.code)) {
                airportResults.keys.push(v.code);
                var c = v;
                airportResults.suggestions.push(copyValue(v));
            }
        }
    });
    if (query.length == 3 || query.length == 2) {
        jQuery.each(results, function (i, v) {
            if (airportResults.keys.length < size
                && v.code.search(new RegExp(regExString, "i")) != -1) {
                if (-1 === airportResults.keys.indexOf(v.code)) {
                    airportResults.keys.push(v.code);
                    airportResults.suggestions.push(copyValue(v));
                }
            }
        });
    }

    return airportResults.suggestions;
}

function copyValue(v) {
    var c = {
        "value": v.value,
        "city": v.city,
        "country": v.country,
        "airport": v.airport,
        "type": v.type,
        "code": v.code,
        "best": v.best,
        "keyCityCode": v.keyCityCode,
        "keyCountryCode": v.keyCountryCode,
        "keyAirportName": v.keyAirportName
    };
    return c;
}

function getIndiceLinguaggio() {
    return ["zh", "ja", "ko", "he"].indexOf(languageCode) > -1 ? 2 : 3;
}

function removeAccents(strAccents){
    strAccents = strAccents.split('');
    strAccentsOut = new Array();
    strAccentsLen = strAccents.length;
    var accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var accentsOut = ['A','A','A','A','A','A','a','a','a','a','a','a','O','O','O','O','O','O','O','o','o','o','o','o','o','E','E','E','E','e','e','e','e','e','C','c','D','I','I','I','I','i','i','i','i','U','U','U','U','u','u','u','u','N','n','S','s','Y','y','y','Z','z'];
    for (var y = 0; y < strAccentsLen; y++) {
        if (accents.indexOf(strAccents[y]) != -1) {
            strAccentsOut[y] = accentsOut[accents.indexOf(strAccents[y])];
        }
        else
            strAccentsOut[y] = strAccents[y];
    }
    strAccentsOut = strAccentsOut.join('');
    return strAccentsOut;
}