jQuery(document).ready(function() {
	getDropdownData({"countries":""}, dropdownSuccess, dropdownFail);
	$('#invoiceSubmit').bind('click.invoiceHandlers', function invoiceHandler(e) {
		validation(e, 'invoicesubmit', '#specialpageform_frm_invoice', invoiceSuccess, invoiceError);
	});
});

function invoiceSuccess(data) {
	return (data.result ? invoiceContinue() : showErrors(data));
}

function invoiceError() {
	return invoiceContinue(true);
}

function invoiceContinue(stop) {
	removeErrors();
	$('#invoiceSubmit').removeAttr('onclick').unbind('click');
	return stop || $('#invoiceSubmit').click();
}

function dropdownSuccess(data) {
	if (data.countries) {
		for (var i = 0; i < data.countries.length; i++) {
			$('#nation').append(
					'<option value="' + data.countries[i].code + '">'
							+ data.countries[i].description + '</option>');
		}
	} else {
		dropdownFail();
	}
	return false;
}

function dropdownFail() {
	console.error("Unable to preload dropdown");
	return false;
}