var seatmapOptions = {};
var pageInitialized = false;
var currentSeat;
var tooltipSelector = '';
// document ready
$( document ).ready( function() {

    $('.reveal-wrap').foundation();
    // seatmap.initAccordion();

    seatmapOptions.initPlaceTooltips();
    seatmapOptions.initRevealConfirm();

    if(pageInitialized) return;
    pageInitialized = true;
    $('.seats-list ul li a').on('click',function(){
        seatmapOptions.selectOne
        // var idPassenger = $(this).find('span.id').html();
        var idPassenger = $(this).parent().data("passenger");
        if (!$(this).parent().hasClass('selected'))
        {
            seatmapOptions.selectOne(idPassenger);    
        }
        
    });


} );
seatmapOptions.closeAllTooltips = function(){
    $('.tooltip').css('display','none');
}
seatmapOptions.selectOne = function(passenger){
    if (!$('li[data-passenger="'+passenger+'"]').hasClass('selected')){
        $('.seats-list ul li').removeClass('selected');
        var indexUl = passenger;
        var indexUlIDX = parseInt(passenger.toString().slice(-1));
        $('.seats-list ul li:nth-child('+indexUlIDX+')').addClass('selected');
        $('.seatMapDiv').css('display','none');
        seatmapOptions.closeAllTooltips();
        $('.seatMapDiv[data-passenger-id='+indexUl+']').css('display','block');
        var selectedSector = $('.imageRows__row.active').data('fila');
        $('.seatMapDiv[data-passenger-id='+indexUl+'] .chooseSeat__passengerSeats.sector[data-fila='+selectedSector+']').addClass('is-active');

    }
}
/*faqOptions.initAccordion = function() {
	$( '.faq__accordion .accordion' ).foundation();
	faqOptions.accordion = new Foundation.Accordion( $( '.faq__accordion .accordion' ), {
		// slideSpeed: 0,
		multiExpand: false,
		allowAllClosed: true,
	} );
};*/

seatmapOptions.initPlaceTooltips = function() {
    $('.chooseSeat__seats .extraComfort').children('a').click(function() {
        if( Foundation.MediaQuery.is( 'large' ) ) {
            $('.tooltip-desktop[data-row="' + $(this).parent().data('row') + '"][data-col="' + $(this).parent().data('col') + '"]').show();
            tooltipSelector = '.tooltip-desktop[data-row="' + $(this).parent().data('row') + '"][data-col="' + $(this).parent().data('col') + '"]';
        } else {
            $('.tooltip-mobile[data-row="' + $(this).parent().data('row') + '"][data-col="' + $(this).parent().data('col') + '"]').show();
            tooltipSelector = '.tooltip-mobile[data-row="' + $(this).parent().data('row') + '"][data-col="' + $(this).parent().data('col') + '"]';
        }
        return false;
    });
    $('.tooltip .close-button').click(function() {
        $(this).closest('.tooltip').hide();
    });


}
seatmapOptions.initRevealConfirm = function() {
    $('.checkin-seleziona-posto .seat-configurator .checkin-button-wrap .button').click(function() {
        if( !Foundation.MediaQuery.is( 'large' ) ) {
            $(this).closest('.seat-configurator').find('.close-button').trigger('click');
        }
    });

}

$( "#confermaPostoEmergenza" ).click(function() {
    var element = currentSeat.parents("td.emergency").find("a");
    changeSeat(element);
    $('#manage-notify-emergency').foundation('close');
    close_popup.trigger("click");
});

function openModal(el) {
    $("#manage-notify-emergency").parent().css("z-index","1010");
    $("#lnkPostoEmergenza").trigger("click");
    currentSeat = $("[data-seat='" + $(el).data('seat') + "']")
}

function openModalItci() {
    $("#external-flight").attr("href","#");
    $("#external-flight-tab").parent().css("z-index","1010");
    $("#externalFlight").trigger("click");
}