function populateDepartureInput(airport) {
    if(!$(".partenza-destinazione .input-wrap").hasClass('selected')){
        //inserisco il rissultato della geolocalizzazione solo se l'utente non ha ancora selezionato l'input per scrivere origine o destinazione 
        if(airport != null && airport.value){
            cercaVoloAutocomplete.setAirportInputValue("list_andata--prenota", airport.city, airport.code, airport.country,airport.type, false);
            cercaVoloAutocomplete.listOriginAirports = allAirports;
            //setto lista offerte in base all'aeroporto geolocalizzato
            $('select.select--departure option[selected="selected"]').removeAttr('selected');
            $('select.select--departure').find('option:contains("'+airport.city+'")').attr("selected",true);
            $('select.select--departure').find('option:contains("'+airport.city+'")').change();

        }
        else{
            cercaVoloAutocomplete.listOriginAirports = allAirports;
            cercaVoloAutocomplete.setAirportInputValue("list_andata--prenota", "Roma", "FCO", "Italia","ROM", false);
            $('input[name="list_andata--prenota"]').attr('data-type',"ROM");
            //setto lista offerte in base all'aeroporto geolocalizzato
            $('select.select--departure option[selected="selected"]').removeAttr('selected');
            $('select.select--departure').find('option:contains("Roma")').attr("selected",true);
            $('select.select--departure').find('option:contains("Roma")').change();
        }
    }
}
/*--- Fix chiusura pannello al click degli input --*/
$("#luogo-partenza--prenota-desk").click(function(){
    cercaVoloPrenotaOptions.closeAllCollapsablePanels();
});

$("#luogo-arrivo--prenota-desk").click(function(){
    cercaVoloPrenotaOptions.closeAllCollapsablePanels();
});

$("#data-andata--prenota-desk").click(function(){
    cercaVoloPrenotaOptions.closeAllCollapsablePanels();
});

$("#data-ritorno--prenota-desk").click(function(){
    cercaVoloPrenotaOptions.closeAllCollapsablePanels();
});
/*--- FINE --*/

/*--- Fix chiusura suggestion se input minore di 3 lettere ---*/
$("#luogo-partenza--prenota-desk").on("keyup", function() { 
    if($("#luogo-partenza--prenota-desk").val().length < 3) 
        $(".suggestion_andata--prenota").empty(); 
});

$("#luogo-arrivo--prenota-desk").on("keyup", function() { 
    if($("#luogo-arrivo--prenota-desk").val().length < 3) 
        $(".suggestion_destination--prenota").empty(); 
});

$("#luogo-partenza--prenota-mobile").on("keyup", function(){ 
    if($("#luogo-partenza--prenota-mobile").val().length < 3)
        $(".suggestion_andata--prenota").empty();
});

$("#luogo-arrivo--prenota-mobile").on("keyup", function(){ 
    if($("#luogo-arrivo--prenota-mobile").val().length < 3)
        $(".suggestion_destination--prenota").empty();
});
/*--- FINE ---*/

// open loader
cercaVoloOptions.enableLoader = function() {
    $('.cerca-volo__inner').addClass('loading');
};

// close loader
cercaVoloOptions.disableLoader = function() {
    $('.cerca-volo__inner').removeClass('loading');
};


checkinMMloginEnableLoader = function() {
    $('.cerca-volo__inner').addClass('loading');
};


checkinMMloginDisableLoader = function() {
    $('.cerca-volo__inner').removeClass('loading');
};