var bClicked;

function subscriptionSmeHandler(e) {
	if (bClicked === false){
		bClicked = true;
		validation(e, 'subscriptionsme',
				'#form-subscriptionsme', subscriptionSmeSuccess, subscriptionSmeError);
		return false;
    } else {
		return false;
    }
}

function subscriptionSmeSuccess(data) {
	if(data.result){
		removeErrors();
        $('#form-subscriptionsme').submit();
	}
	else{
		if (typeof refreshReCaptchaLogin != 'undefined') {
			refreshReCaptchaLogin(spacialPagesRecaptchaID);
		}
		showErrors(data, undefined, true);
        bClicked = false;
	}
}


function subscriptionSmeError() {
	removeErrors();
	//$('#subscriptionSmeSubmit').unbind('click', subscriptionSmeHandler);
	if (typeof refreshReCaptchaLogin != 'undefined') {
		refreshReCaptchaLogin(spacialPagesRecaptchaID);
	}
	$('#form-subscriptionsme').submit();
    bClicked = false;
}


function retrieveData() {
	var countrySelected = $("#company_country").attr("data-preselection") || window["countryCode"].toUpperCase();
	var requestData = {
			'countries': "",
			'prefissiNazionali': "",
			'linguaCorrispondenza': "",
			'states': countrySelected
	};
	getDropdownDataSme(requestData, dropdownDataSuccess, dropdownDataFail);
	return false;
}

function dropdownDataSuccess(data) {
	var countries = data.countries; 
	for (index in countries) {
		var value = countries[index].code;
		var descr = countries[index].description;
		dropdownAppendWithPreselection("nation", value, descr);
	}
	
	var prefissiNazionali = data.prefissiNazionali;
	for (index in prefissiNazionali) {
		var value = prefissiNazionali[index].prefix;
		var descr = prefissiNazionali[index].description + ' (+' + prefissiNazionali[index].prefix + ')';
		dropdownAppendWithPreselection("nationalPrefix", value, descr);
	}
	
	var lingueCorrispondenza = data.linguaCorrispondenza;
	for (index in lingueCorrispondenza) {
		var value = lingueCorrispondenza[index].name;
		var descr = lingueCorrispondenza[index].value;
		dropdownAppendWithPreselection("language", value, descr);		
	}
	
	
	return false;
}

function statesDropdownDataSuccess(data) {	
	var states = data.states;
	for (index in states) {
		var value = states[index].code;
		var descr = states[index].name;
		dropdownAppendWithPreselection("provincia", value, descr);
	}
	

	return false;
}

function dropdownAppendWithPreselection(dropdownType, value, descr){
	var selectedOption = '<option selected="selected" value="' + value + '">' + descr + '</option>';
	var option = '<option value="' + value + '">' + descr + '</option>';
	$("select[data-dropdown='"+dropdownType+"']").each(
			function(ind, elem){
				if($(elem).attr("data-preselection") === value){
					$(elem).append(selectedOption);
				}
				else{
					$(elem).append(option);
				}
			});
}

function dropdownDataFail() {
	return false;
}

$(document).ready(function() {

    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results==null){
            return null;
        }
        else{
            return decodeURI(results[1]) || 0;
        }
    }

    if (isItalianMarket()){
    	var label_piva=$("#lblPiva")[0].textContent;
        $("#lblPiva")[0].textContent=label_piva + " *";
	}

	bClicked = false;

	retrieveData();

	$('#subscriptionSmeSubmit').bind('click', inviaBusinessConnect);


	$('div.table_form').find('select#company_country').on('change', function(e) {
		defaultSelectedCountry = $('div select#company_country option:selected').val();

		var requestData = {
			'states': defaultSelectedCountry
		};

		/*Clear select*/
		$("select[data-dropdown='provincia']").find("option").remove();
        $('#company_state_prov').append(new Option(comboProvinciaText, comboProvinciaVal));
		getDropdownDataSme(requestData, statesDropdownDataSuccess, dropdownDataFail);

	});

	//invokeShowCaptcha("captcha_login_spacialpages", "1");
});

function getDropdownDataSme(requestData, done, fail, always) {
	return invokeGenericFormService('staticdatalistservletsme', 'GET', '#', done,
			fail, always, 'json', requestData);
}

$('img#refreshCaptcha').on('click', function(){
	$('img#captchaImg').attr("src",".jcaptcha?timestamp=" + new Date().getTime());
	$('input#captcha').val('');
});

function refreshCaptcha(){
        $('img#captchaImg').attr("src",".jcaptcha?timestamp=" + new Date().getTime());
        $('input#captcha').val('');
}