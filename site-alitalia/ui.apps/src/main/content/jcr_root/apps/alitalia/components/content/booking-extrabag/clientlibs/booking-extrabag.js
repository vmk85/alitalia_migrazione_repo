
var options = {
        minimum: 0,
        maximize: 6
    }

$(document).ready(function () {
	
	var flightcounters = $(".flight_row_counter");


    
    $.each(flightcounters, function (index, item) {
    	var counters =$(this).find(".bag_row_counter");
    	 $.each(counters, function (indexP, item) {

	        $("#passenger" + index + "_" + indexP ).handleCounter(options);
	    });
        //$("#passenger" + index).handleCounter(options);
    });
    
    $(".css-checkbox").change(function () {

        if (!$(this).is(':checked')) {
            var c2 = $("#first_container_0 :text");
            var $handleCountercontainer;
            $("#" + this.name + " :text").each(function (index, item) {
                var elem = $(this);

                var dataItem = elem.data("item");
                elem.val(c2[dataItem].value);
                changeValByElement(elem)
                
            });
        }
        else {

            $("#" + this.name + " :text").each(function (index, item) {
                var elem = $(this);
                var dataItem = elem.data("item");
                elem.val("0");
                changeValByElement(elem)
            });
        }
    });

    /*
    $.each(counters, function (index, item) {
    	debugger;
        $("#passenger" + index).handleCounter(options);
    });

*/

});

function changeValByElement(element) {

	var num = element.val()
	$handleCountercontainer = element.parent();
	$btnMinus = $handleCountercontainer.find('.h-font-minus-bag');
    $input = $handleCountercontainer.find('input')
    $btnPlugs = $handleCountercontainer.find('.h-font-plus-other-bag');
    var price = parseInt($handleCountercontainer.parent().find('.priceHidden')[0].innerText);
    $handleCountercontainer.parent().find('.price')[0].innerText = num * price;
    
    element.data('num', num)
    $btnMinus.prop('disabled', false)
    $btnPlugs.prop('disabled', false)
    $handleCountercontainer.find('.h-font-minus-bag').removeClass('locked');
    $handleCountercontainer.find('.h-font-plus-other-bag').removeClass('locked');
    if (num <= options.minimum) {
        $btnMinus.prop('disabled', true)
        $handleCountercontainer.find('.h-font-minus-bag').addClass('locked');
        //onMinimum.call(this, num)
    } else if (options != null && options.maximize != null && num >= 6) {
        $btnPlugs.prop('disabled', true)
        $handleCountercontainer.find('.h-font-plus-other-bag').addClass('locked');
        //onMaximize.call(this, num)
    }

    //onChange.call(this, num)
    setTotalPrice();
    
}