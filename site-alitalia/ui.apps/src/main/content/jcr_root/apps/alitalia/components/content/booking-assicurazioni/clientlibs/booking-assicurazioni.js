setI18nTranslate('booking.service.generic.error');
function be_addInsurance(e, btn, selector, done) {
	validation(e, 'bookingaddinsuranceconsumer','#form-booking-assicurazioni',
			function(data){
				insuranceSuccess(data, e, selector, done);
			},
			function(){
				console.log("error validation insurance");
				if (done) {
					done(selector);
				}
			}
	);
}

function be_cancelInsurance(section, done) {
	performSubmit('bookingremoveinsuranceconsumer', '#form-booking-assicurazioni', 
			function(data){
				refreshInfoBoxPartial();
				if (done) {
					done();
				}
			},
			function(){
				console.log("error add insurance");
				if (done) {
					done();
				}
			}
	);
}

function insuranceSuccess(data, e, selector, done) {
	if (data.result) {
		removeErrors();
		performSubmit('bookingaddinsuranceconsumer', '#form-booking-assicurazioni',
				function(data){
					if (done) {
						refreshInfoBoxPartial();
						jQuery("#insuranceAdd").addClass("j-addToCart");
						done(selector);
					}
				},
				function(){
					console.log("error add insurance");
					if (done) {
						done(selector);
					}
				}
		);
	} else {
		showErrors(data);
		if(window["analytics_trackBookingErrors"] && typeof window["analytics_trackBookingErrors"] === "function"){
			window["analytics_trackBookingErrors"](data.fields, "ancillary");
		}
		/*in attesa di funzione per togliere il loader senza aver acquistato assicurazione*/
		loader = section.find('.j-loader');
		sectionstep = $(e.currentTarget).closest('.j-upsellingStep');
		section = sectionstep.closest('.j-upselling');
		section.data('step', 0);
		section.find('.j-upsellingStep').eq(0).show();
		if(section.find('.j-upsellingStep').eq(0).hasClass('booking__chooseSeat--choose')){
			if ($('.j-chooseSeatSlider').length > 0){
				bookingSeatSlider.init();
			}
		}
		$(window).trigger('resize');
		loader.velocity('transition.fadeOut',{
			duration: 250,
			complete: function(){
				newPos = section.closest('.j-accordion').offset().top - $('.j-bookInfoBox').outerHeight();
				if($('.j-bookInfoBox').length > 0 && $('.j-bookInfoBox').hasClass('opened')){
					newPos = newPos + $('.j-bookInfoBox').outerHeight()
				}
				$('html').velocity("scroll", {
					duration: 400,
					delay: 100,
					offset: newPos
				});
			}
		});
	}
}

//function insuranceError() {
//	return insuranceContinue(true);
//}

//function insuranceContinue(stop) {
//	removeErrors();
//	$('#insuranceSubmit').unbind('click', insuranceHandler);
//}

//function insuranceSubmitSuccess() {
//	$('#insurance_buy').hide();
//	$('#insurance_summary').show();
//}

//function insuranceRemoveSuccess() {
//	$('#insurance_buy').show();
//	$('#insurance_summary').hide();
//
//}

function insuranceRemoveError() {
	$('#errorField').text(
			getI18nTranslate('booking.service.generic.error'));

}

function insuranceSubmitError() {
	$('#errorField').text(
			getI18nTranslate('booking.service.generic.error'));
}

function showErrors(data) {
	$('#errorField').text(data.fields['accept']);
}

function removeErrors() {
	$('#errorField').text('');
}

