$(document).ready(function() {
	getMessages();
	initECSelect();
	$("#messaggiSearch").on("click", function(){
		day = $("#selECDay").val();
		month = $("#selECMonth").val();
		year = $("#selECYear").val();
		getMessagesByDate(day, month, year);
	});

    $(document).on("click", ".widget-messaggi .message [data-toggle]", function() {
		// se unread segna come letto
		$( this ).closest( '.message' ).hasClass( 'unread' ) ? $( this ).closest( '.message' ).removeClass( 'unread' ) : false;
		$( this ).closest( '.message' ).toggleClass( 'open' );
    });


	return false;
});

function getMessagesByDate(day, month, year) {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	params = {};
	if (isValidDate(day, month, year)) {
		params.day = day;
		params.month = month;
		params.year = year;
	}
	$.ajax({
		url : url + ".lista-messaggi-partial-1.html",
		context : document.body,
		data : params
	})
	.done(successListMessages)
	.fail(failListMessages);
	return false;
}

function successListMessages(data) {
//	$('#millemiglia_list_messages div:gt(0)').remove();
//	$('#millemiglia_list_messages').append(data);
    $(".messages-list").empty();
    if(data.trim()!=''){
        $(".messages-list").append(data);
    }else{
        $(".messages-list").append('<li>Nessun messaggio trovato.</li>');
    }

	//initAccordions();
	return false;
}

function failListMessages() {
	return false;
}

function isValidDate(day, month, year) {
	if (typeof day === "undefined" || typeof month === "undefined"
		|| typeof year === "undefined") {
		return false;
	}
	if (month > 12 || day > daysInMonth(month, year)) {
		return false;
	}
	return true;
}

function daysInMonth(m, y) {
	switch (m) {
	case 2:
		return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
		return 31;
	default:
		return 30
	}
}
