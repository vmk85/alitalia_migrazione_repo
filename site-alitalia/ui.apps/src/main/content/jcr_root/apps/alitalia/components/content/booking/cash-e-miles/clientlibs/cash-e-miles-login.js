
function onCashAndMilesButtonLoginClick(e) {
	form = {};
	form.fieldprefix = "cashAndMiles_login_";
	form.cashAndMiles_login_code = jQuery("#cashAndMiles_login_code").val();
	form.cashAndMiles_login_pin = jQuery("#cashAndMiles_login_pin").val();
	form.recaptchaResponse = jQuery("#cashAndMiles_login_recaptchaResponse").val();
	performValidation('millemiglialoginvalidation', form,
			cashAndMilesLoginValidationSuccess,
			headerLoginUnexpectedErrorCallback);
	return false;
}

function cashAndMilesLoginValidationSuccess(data) {
	if (data.result) {
		var mmCode = $('#cashAndMiles_login_code').val();
		var mmPin = $('#cashAndMiles_login_pin').val();
		var rememberMe = "0";
		if ($("#cashAndMiles_login_rememberMeLang").is(":checked")) {
			rememberMe = "1";
		}
		
		alitaliaLoginContextData.registeredProfileAsyncLoginCallbacks.push(
				cashAndMilesAjaxRefresh);
		
		startLoginWithMilleMigliaCredentials(mmCode, mmPin, rememberMe,
				headerLoginUnexpectedErrorCallback,
				headerLoginFailureCallback,null,loginUserLockedErrorCallback,true);
	} else {
		if (typeof refreshReCaptchaLogin != 'undefined') {
			refreshReCaptchaLogin(cashAndMilesRecpatchaID);
		}
		showErrors(data);
	}
}

function prepareCashAndMilesLoginSocial() {
	if (window['gigya']) {
		window['gigya'].socialize.showLoginUI({
			  versione: 2
			, height: 30
			, width: '100%'
			, buttonsStyle: 'standard'
			, showTermsLink: false
			, hideGigyaLink: true
			, showWhatsThis: false
			, containerID: 'cashAndMiles_login_social'
			, context: { 
				action: 'startSocialLogin',
				unexpectedErrorCallback: headerLoginUnexpectedErrorCallback,
				loginFailureCallback: headerLoginFailureCallback
			}
			, onLoad: function(event){
				$("#cashAndMiles_login_social").find("center").contents().unwrap();	
				$('#cashAndMiles_login_social').find('table').each(
					  function(index, elem) {
						if ($(elem).find('caption').length == 0) {
							$(elem).prepend(
								'<caption class="hidden">Social Account</caption>');
						}
					});
			}	
			, cid: ''
			
		});
	}
}