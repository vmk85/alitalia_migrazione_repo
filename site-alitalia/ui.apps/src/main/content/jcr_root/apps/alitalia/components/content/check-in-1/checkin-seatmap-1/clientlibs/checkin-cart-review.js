var cartReviewOptions = {};

$(document).ready( function() {
	// init sticky topbar
		cartReviewOptions.initStickyTopBar();
		cartReviewOptions.rotateArrow();

		// open/close cart-review
		$( '[data-toggle-cart]' ).on( 'click', function() {
			cartReviewOptions.toggleCartContent();
		} );
		// fix click on black overlay, z-index and pointer-events problem
		$( window ).on( 'click', function( e ) {
			if( $( '[data-cart-expanded]' ).attr( 'data-cart-expanded' ) == 'open' && $(e.target).closest( '.cart-content-wrap' ).length > 0 && $(e.target).find( '.cart-content' ).length > 0 ) {
				cartReviewOptions.toggleCartContent();
			}
		} );

});

// on media query chance (desktop / mobile)
// $(window).on('sticky.zf.unstuckfrom:top', function(event, name) {
// 	if( $( '[data-cart-expanded]' ).attr( 'data-cart-expanded' ) == 'open' ) {
// 		cartReviewOptions.toggleCartContent();
// 	}
// });
// sticky.zf.unstuckfrom:top
// sticky.zf.unstuckfrom:bottom
// sticky.zf.stuckto:top
// sticky.zf.stuckto:bottom

// on media query chance (desktop / mobile)
// $(window).on('changed.zf.mediaquery', function(event, name) {

// 	cartReviewOptions.initStickyTopBar();

// });

cartReviewOptions.initStickyTopBar = function() {

	$( '.checkin-topbar-container .inner-fixed' ).foundation();
	// cartReviewOptions.stickyTopBar = new Foundation.Sticky( $( '.checkin-topbar-container .inner-fixed' ), {
	// 	anchor: 'main-content',
	// 	marginTop: 0,
	// 	stickyOn: 'medium',
	// 	// checkEvery: true,
	// } );

};

cartReviewOptions.toggleCartContent = function() {
	if( $( '[data-cart-expanded]' ).attr( 'data-cart-expanded' ) !== 'open' ) {

		// if topbar still not fixed, scroll page till topbar is fixed
		if( $( 'html, body' ).scrollTop() < $( '[data-cart-expanded]' ).offset().top ) {
			$( 'html, body' ).animate( {
				scrollTop: $( '[data-cart-expanded]' ).offset().top,
			}, 300, 'swing', function() {
				// open overlay
				$( '[data-cart-expanded]' ).attr( { 'data-cart-expanded': 'open' } );
				$( '.cart-overlay' ).attr( { 'data-status': 'open' } );
                $('body').addClass('noScroll');
                $('html').addClass('noScroll');
                $("#header").addClass("hide");

            } /* , callback */ );
		} else {
				// open overlay
				$( '[data-cart-expanded]' ).attr( { 'data-cart-expanded': 'open' } );
				$( '.cart-overlay' ).attr( { 'data-status': 'open' } );
				$('body').addClass('noScroll');
				$('html').addClass('noScroll');
				$("#header").addClass("hide");
        }


	} else {

		// close overlay
		$( '[data-cart-expanded]' ).attr( { 'data-cart-expanded': 'close' } );
		$( '.cart-overlay' ).attr( { 'data-status': 'close' } );
        $('body').removeClass('noScroll');
        $('html').removeClass('noScroll');
        $("#header").removeClass("hide");

    }

	// fix inline style attached by foundation sticky
	$( '.sticky-container' ).removeAttr( 'style' );

};

cartReviewOptions.rotateArrow = function() {
	$( '[data-toggle*="flight"]' ).on( 'click', function() {
		$( this ).toggleClass( 'open' );
	} );
}