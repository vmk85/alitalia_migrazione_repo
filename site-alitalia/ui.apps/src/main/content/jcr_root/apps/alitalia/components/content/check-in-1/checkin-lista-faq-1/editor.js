"use strict";
use(function () {

    var prop = this.properties;

    var a = String(prop.get("faq11"));
    var b = String(prop.get("faq12"));
    var c = String(prop.get("faq13"));
    var d = String(prop.get("answer11"));
    var e = String(prop.get("answer12"));
    var f = String(prop.get("answer13"));

    var a1 = parseInt(a.length);
    var faq11 = a.substring(3,a1 - 4);
    a1 = parseInt(b.length);
    var faq12 = b.substring(3,a1 - 4);
    a1 = parseInt(c.length);
    var faq13 = c.substring(3,a1 - 4);
    a1 = parseInt(d.length);
    var answer11 = d.substring(3,a1 - 4);
    a1 = parseInt(e.length);
    var answer12 = e.substring(3,a1 - 4);
    a1 = parseInt(f.length);
    var answer13 = f.substring(3,a1 - 4);

    return {
        faq11:faq11,
        faq12:faq12,
        faq13:faq13,
        answer11:answer11,
        answer12:answer12,
        answer13:answer13
    };

});