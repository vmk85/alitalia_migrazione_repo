// function setFlagChannel(){
//     gigya.accounts.getAccountInfo(
//         {
//             callback:function(resp){
//                 setNotificationMethod(resp.data.generalPref_Channel);
//         }
//     });
// }

function setNotificationMethod(data) {
    if (data != 0){
        switch (data){
            case 1:
                setChecked(["email"]);
                break;
            case 5:
                setChecked(["app"]);
                break;
            case 9:
                setChecked(["sms"]);
                break;
            case 6:
                setChecked(["email","app"]);
                break;
            case 10:
                setChecked(["email","sms"]);
                break;
            case 14:
                setChecked(["app","sms"]);
                break;
            case 15:
                setChecked(["email","sms","app"]);
                break;

        }
    }
}
function setChecked(data) {

    var notificationMethod = getNotificationMethodArray();

    for (var chiave in data){
        $("#" + notificationMethod[data[chiave]]).attr('checked', 'checked').closest(".field-wrap").addClass("checked");
    }
}

function getNotificationMethodArray() {
    var notificationMethod = new Array();
    notificationMethod["app"] = "notification-app";
    notificationMethod["email"] = "notification-email";
    notificationMethod["sms"] = "notification-sms";
    return notificationMethod;

}