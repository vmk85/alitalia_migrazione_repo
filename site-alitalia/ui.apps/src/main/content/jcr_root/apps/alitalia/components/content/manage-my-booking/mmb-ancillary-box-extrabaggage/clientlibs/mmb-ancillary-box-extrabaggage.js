
function initPartialMmbExtraBaggage() {
	$('.extra-baggage-checkbox').change(function(e) {
		var checkbox = e.currentTarget;
		var hidden = $(checkbox).siblings('input[type="hidden"].extra-baggage-checkbox-field');
		var currentVal = $(hidden).val();
		var newVal = currentVal.substring(0, currentVal.length - 1) + ( $(checkbox).is(':checked') ? '1' : '0' );
		$(hidden).val(newVal);
		performLastBaggageSelectedOperation($('.extra-baggage-checkbox').closest(".upsellingChoose"), "be_addExtraBaggage", "be_clearExtraBaggage");
	});
}

function be_addExtraBaggage(e, btn, selector, done) {
	performValidation('mmbancillarycartextrabaggage', '#extraBaggageSelectionForm', 
			mmbAncillaryExtraBaggageAddValidationSuccess, mmbHandleInvokeError);
}

function mmbAncillaryExtraBaggageAddValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('mmbancillarycartextrabaggage', '#extraBaggageSelectionForm', 
				mmbAncillaryExtraBaggageAddSubmitSuccess, mmbHandleInvokeError);
	} else {
		showErrors(data);
	}
}

function mmbAncillaryExtraBaggageAddSubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		if (data.isError) {
			mmbHandleServiceError(data);
		}
		refreshMmbPartialExtraBaggage(true, true);
	}
}


/* clear form */

function be_clearExtraBaggage(e, btn, section, callback) {
	performValidation('mmbancillarycartextrabaggage', '#mmb-ancillary-clear-extrabaggage-form', 
			mmbAncillaryExtraBaggageClearValidationSuccess, mmbHandleInvokeError);
};

function mmbAncillaryExtraBaggageClearValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('mmbancillarycartextrabaggage', '#mmb-ancillary-clear-extrabaggage-form', 
				mmbAncillaryExtraBaggageClearSubmitSuccess, mmbHandleInvokeError);
	} else {
		showErrors(data);
	}
}

function mmbAncillaryExtraBaggageClearSubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		if (data.isError) {
			mmbHandleServiceError(data);
		}
		refreshMmbPartialExtraBaggage(false, true);
	}
}