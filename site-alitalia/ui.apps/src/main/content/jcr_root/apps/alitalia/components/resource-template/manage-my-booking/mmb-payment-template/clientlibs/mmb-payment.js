$(function initMmb() {
	$(function(){
		$.ajax({
			url: removeUrlSelector() + ".payment-main-partial.html",
			context : document.body
		})
		.done(function(data) {
			$(".manageBooking.mod.manageBookingPaymentMetod").append(data);
			
			$('.j-hideOnCLick').click();
			initAccordions();
			bookingReady();
			initMmbPayment();
			initPartialMmbCart();
			analytics_mmbPartialCallback();
			initMmb_tracking();
			$(window).trigger('resize');
		})
		.fail(function() {});
	});
});
