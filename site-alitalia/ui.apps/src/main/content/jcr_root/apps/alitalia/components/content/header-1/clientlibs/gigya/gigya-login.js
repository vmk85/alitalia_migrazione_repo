/** Oggetto Login */
var Login = {};

Login.Session = {};

Login.Registration = {};

Login.RegistrationSocial = {};

Login.Session.refreshError = function (){

};

Login.Session.refreshSuccess = function (response) {

    if (!response.isError){
        if (response.data){
            var data = JSON.parse(response.data);
            $("#login-ma .icon--close").click();
            setHeaderMA(data);
            fillPostLogin(data);
            $(".show-if-user-logged").removeClass("hide");
            /** Miei voli in homepage */
            if (data.totalFlightsAvailableForCheckin > 0){
                availableFlightsForMA(true,data.totalFlightsAvailableForCheckin);
            }
            if (data.totalFlightsForManage > 0){
                availableMyFlightsForMA(true,data.totalFlightsForManage);
            }
            /** Setto il cookie di sessione per l'utente MA loggato */
            if(data.cookieName){
                setGigyaCookie(data.cookieName,data.cookieValue,1);
            }
            var panel = $(".login-included-if-authenticated.user-area.user-logged");
            /** Inizializzazione componeni */
            if (globalInit.length > 0){
                for (index in globalInit) {
                    globalInit[index]();
                }
            }
            setTimeout(function () {
                $(".user-menu").fadeOut(2000, function () {
                    $('.user-menu').removeAttr("style");
                    panel.removeClass("active")
                });
                //panel.removeClass("active");
            }, 5000);
            if (data.showModal && getCookie("firstLogin") != "true"){
                setTimeout(function () {
                    showModalOTP();
                    //panel.removeClass("active");
                }, 1000);
            }
        }
    }

    /* AMS Bugfix #5045
    else {
        if (!$('.login-included-if-authenticated').hasClass("hide")) {
            $(".user__logout").attr("href", "javascript:;");
            $('.binding-name').text("");
            $('.menu-mm-item').show();
            $(".menu-ma-item").hide();
            $('.menu-heading-mm').show();
            $('.menu-heading-ma').hide();
            $('.login-included-if-authenticated').addClass("hide");
            $('.login-included-if-anonymous').removeClass("hide");
            $('#header').removeClass();
        }
    }
    Fine Bugfix #5045 */

    disableLoaderMyAlitalia();
};

function getCookie(cname) {
    var nameEQ = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return "";
}

Login.Session.check = function (data) {

    if (!data.isError) {

        resetEmailError();
        resetPassError();

        var dataLogin = {

            email: $("#userID").val(),
            password: $("#userPWD").val()

        };

        performSubmit("MyAlitaliaServlet-loginMa", dataLogin, Login.Session.success, Login.Session.error);

    } else {

        setEmailError(CQ.I18n.get("myalitalia.notValidEmail"));
        disableLoaderMyAlitalia()

    }

};

Login.Session.setSession = function (response, action) {
    enableLoaderMyAlitalia();
    var data = {LoginUserJson: JSON.stringify(response)};
    performSubmit(action, data, Login.Session.success, Login.Session.error);
};

Login.Session.success = function (response) {

    if (!response.isError) {
        if (response.data){
            var data = JSON.parse(response.data);
            $("#userID").val();
            $("#userPWD").val();
            $("#login-ma .icon--close").click();
            setHeaderMA(data);
            fillPostLogin(data);
            $(".show-if-user-logged").removeClass("hide");
            /** Miei voli in homepage */
            if (data.totalFlightsAvailableForCheckin > 0){
                availableFlightsForMA(true,data.totalFlightsAvailableForCheckin);
            } else {
                availableFlightsForMA(true,0);
            }
            if (data.totalFlightsForManage > 0){
                availableMyFlightsForMA(true,data.totalFlightsForManage);
            } else {
                availableMyFlightsForMA(true,0);
            }
            disableLoaderMyAlitalia();
            var panel = $(".login-included-if-authenticated.user-area.user-logged");
            /** Inizializzazione componeni */
            if (globalInit.length > 0){
                for (index in globalInit) {
                    globalInit[index]();
                }
            }
            setTimeout(function () {
                $(".user-menu").fadeOut(2000, function () {
                    $('.user-menu').removeAttr("style");
                    panel.removeClass("active")
                });
                //panel.removeClass("active");
            }, 5000);

            /** Setto il cookie di sessione per l'utente MA loggato */
            setGigyaCookie(data.cookieName,data.cookieValue,1);

            /** Verifico se mostrare la modale di inserimento otp */
            if (data.showModal){
                showModalOTP();
            }
//         getUserMAInfo(function(ev){
//
//                     $("#login-ma .icon--close").click();
//                     // console.log(ev);
//                     userMA = ev;
//                     if(globalInit.length > 0)
//                         for(index in globalInit)
//                             globalInit[index]();
//                     $(".show-if-user-logged").removeClass("hide");
//                     var type = linkFromEmail();
// //    afterLogged();
//                     if(type == "login") redirect("myalitalia/myalitalia-bacheca", "page=dashboard");
//                     else if(type == "reset" && !location.href.includes("resetpassword")){
//                         //redirect("myalitalia/myalitalia-resetpassword");
//                         //$("#openResetPasswordForm").trigger("click");
//                         performSubmit("GetMyAlitaliaCRMDataToSession","",Login.Session.success.getCrmDataToSessionForFill,Login.Session.error.getCrmData);
//                     }
            // else if (window.location.href.indexOf("booking/flight-select") != -1){
//                     else {
//                         // console.log("loggato");
//                         performSubmit("GetMyAlitaliaCRMDataToSession","",Login.Session.success.getCrmDataToSessionForFill,Login.Session.error.getCrmData);
//                     }
//                     var panel = $(".login-included-if-authenticated.user-area.user-logged");
//                     panel.addClass("active");
//                     setTimeout(function() {
//                         $(".user-menu").fadeOut(2000, function(){$('.user-menu').removeAttr("style");panel.removeClass("active")});
//                         //panel.removeClass("active");
//                     }, 5000);
//
//                     if(ev.data.id_mm != undefined ){
//
//                         if (ev.data.id_mm != ""){
//
//                             var data = {
//                                 "mm_pin": ev.data.id_mm
//                             };
//
//                             performSubmit("millemigliacomunicazionisubmit-get-login-ma-mm", data, getLoginPinMMSuccess, getLoginPinMMError);
//
//                         }
//
//                     }
//
//                 });
        }

    } else {

        if(response.errorCode == 206002){
            disableLoaderMyAlitalia();
            $("[data-open='ma-pending-verification']").trigger("click");

        } else {

            $("#gigya-login-error-message p").text(CQ.I18n.get(response.errorMessage));
            disableLoaderMyAlitalia();

        }
        // setTimeout(function() {
        //     gigya.accounts.logout();
        // }, 5000);
    }

};

function getLoginPinMMSuccess(data) {

    if (!data.isError) {

        performMilleMigliaLogin(data.MM_code, data.pw, false, null);

    }
}


Login.Session.success.setProfileData = function (response) {

    if (!response.isError) {
        performSubmit("MyAlitaliaServlet-UpdateSession","",Login.Session.refreshSuccess,Login.Session.refreshError);
        myAlitaliaOptions.feedbackSuccess();
        performSubmit("CRMdataPercent","",Login.Session.success.dataPercent,Login.Session.error.dataPercent);
    } else {
        myAlitaliaOptions.feedbackFail();
        disableLoaderMyAlitalia();
    }

};


Login.Session.setSession = function (response,action) {
    var data = {LoginUserJson: JSON.stringify(response)};
    performSubmit(action,data,Login.Session.success,Login.Session.error);
    saveUserMA(response);
};

function getLoginPinMMSuccess(data){

    if (!data.isError){

        performMilleMigliaLogin(data.MM_code, data.pw,false, null);
        disableLoaderMyAlitalia();

    }

    else{console.log(data)}

}

Login.Session.error = function (response) {
    disableLoaderMyAlitalia();
};

Login.Session.error.setDocumenti = function (response) {
    myAlitaliaOptions.feedbackFail();
};

Login.Session.success.setDocumenti = function (response) {
    if (!response.isError) {
        actionDoc.init();
        // var documento = JSON.parse(response.documento);
        // checkCards(getTipoCrmdData(tipo), documento);
        // myAlitaliaOptions.feedbackSuccessWAnchor(getTipoCrmdData(tipo));
        // $("#" + getTipoCrmdData(tipo)).before(notification_type_succ_err);
        // fillCard(documento);
        // performSubmit("GetMyAlitaliaCRMDataToSession", "", Login.Session.success.getCrmDataToSessionForFill, Login.Session.error.getCrmData);
    } else {
        myAlitaliaOptions.feedbackFail();
        disableLoaderMyAlitalia();
    }
};

Login.Session.success.editDocumenti = function (response) {

    if (!response.isError) {
        // var documento = JSON.parse(response.documento);
        actionDoc.init();
        // fillCard(documento);
        // performSubmit("GetMyAlitaliaCRMDataToSession", "", Login.Session.success.getCrmDataToSessionForFill, Login.Session.error.getCrmData);
    } else {
        myAlitaliaOptions.feedbackFail();
        disableLoaderMyAlitalia();
    }

};

Login.Session.success.deleteDocumenti = function (response) {
    if (!response.isError) {
        actionDoc.init();
        // performSubmit("GetMyAlitaliaCRMDataToSession", "", Login.Session.success.getCrmDataToSessionForFill, Login.Session.error.getCrmData);
    } else {
        disableLoaderMyAlitalia();
        alert("Errore nella cancellazione dei dati");
    }
};

Login.Session.success.getCrmDataDocument = function (response) {
    if (!response.isError) {
        var Documenti = response.response.elencoDocumenti;
        if (Documenti != undefined) {
            for (var i = 0; i < Documenti.length; i++) {
                checkCards(getTipoCrmdData(Documenti[i].tipo), Documenti[i]);
            }
        }
    } else {

    }
    disableLoaderMyAlitalia();
};

Login.Session.error.getCrmData = function (response) {
    disableLoaderMyAlitalia();
};

Login.Session.success.optionConsensi = function (response) {

    if (!response.isError) {
        popolateConsensiOption(response.response);
        disableLoaderMyAlitalia();
    } else {

    }

};

Login.Session.success.getCrmDataToSessionForFill = function (response) {
    if (!response.isError) {
        setHeaderMA(response.response);
        fillPostLogin(response.response);

    } else {
        $(".login-included-if-anonymous.user-area.fM").each(function (k, v) {
            $(v).removeClass("hide");
        });
        $(".login-included-if-authenticated.user-area.user-logged").hide();
    }
    disableLoaderMyAlitalia();
};

Login.Session.success.setCrmDataPreferenze = function (response) {
    if (!response.isError) {
        var crmData = JSON.parse(response.crmData);
        performSubmit("CRMdataPercent","",Login.Session.success.dataPercent,Login.Session.error.dataPercent);
        initPreferencesMA(crmData.preferenzeViaggio);
    } else {
        disableLoaderMyAlitalia();
    }
};

Login.Session.success.getCrmDataPreferenze = function (response) {
    if (!response.isError) {
        if (response.response != null) {
            initPreferencesMA(response.response.preferenzeViaggio);
            if (response.gigya){
                $.each(response.gigya, function(i, el) {
                    $("[name='" + el + "']").prop("checked", "checked");
                });
                $(".hide-if-pref-ff").hide();
                createSelected("ff", response.gigya.length);
            }
        }
    } else {

    }
    disableLoaderMyAlitalia();
};

// Login.Session.success.getCrmDataUtente = function (response) {
//     if (!response.isError) {
//         if (response.response != null) {
//             popolaFormMAData(response.response);
//         }
//     } else {
//
//     }
//     disableLoaderMyAlitalia();
// };

Login.Session.successAccettazioni = function (response) {
    if (!response.isError) {
        showSuccessNews();
    } else {

    }
};

Login.Session.successCheckLinkAccount = function (response) {

    if (!response.isError){
        if(response.token){
            $("#btnLoginLinkAccount input").attr("data-socialLoginToken",response.token);
        }
        if(!response.linkedin){
            $(".manageSLLink[data-provider='linkedin']").hide();
        } else {
            $(".manageSLLink[data-provider='linkedin']").attr("dara-regMail",response.regMail);
        }
        if(!response.facebook){
            $(".manageSLLink[data-provider='facebook']").hide();
        } else {
            $(".manageSLLink[data-provider='facebook']").attr("dara-regMail",response.regMail);
        }
        if(!response.googleplus){
            $(".manageSLLink[data-provider='googleplus']").hide();
        } else {
            $(".manageSLLink[data-provider='googleplus']").attr("dara-regMail",response.regMail);
        }
        $("[data-provider='"+provider+"']").hide();
        if (response.isOnlySocial){
            $("#pswLoginLinkAccount").hide();
            $("#btnLoginLinkAccount").hide();
            $("#webLinkAccountLogin").hide();
            $("#fullLinkAccountLogin").hide();
            $('[data-open="myalitalia-connect-social"]').trigger("click");
            disableLoaderMyAlitalia();
        } else if (response.isOnlyWeb){
            $("#fullLinkAccountLogin").hide();
            $("#socialLinkAccountLogin").hide();
            $('[data-open="myalitalia-connect-social"]').trigger("click");
            disableLoaderMyAlitalia();
        } else if (response.isFull){
            $("#socialLinkAccountLogin").hide();
            $("#webLinkAccountLogin").hide();
            $('[data-open="myalitalia-connect-social"]').trigger("click");
            disableLoaderMyAlitalia();
        }
    }

};

Login.Session.errorCheckLinkAccount = function () {
    disableLoaderMyAlitalia();
};

Login.Session.successLinkAccount = function (response) {
    if (!response.isError){
        if (response.data){
            var data = JSON.parse(response.data);
            if(data.cookieName){
                setGigyaCookie(data.cookieName,data.cookieValue,1);
            }
            var uri = location.origin + internalUrl1.substring(0, internalUrl1.length - "homepage".length) + "myalitalia/popup.html?associaLinkAccount=true";
            var popup = window.open(encodeURI("https://socialize.eu1.gigya.com/socialize.addConnection?oauth_token=" + data.cookieValue + "&redirect_uri=" + uri + "&x_provider=" + provider
                + "&response_type=token&x_sessionExpiration=3600"), "MsgWindow", "width=500,height=500");
        }
    }
};

Login.Session.successLinkAccountSocial = function (response) {
    $("#myalitalia-connect-social .icon.icon--close").click();
    if (!response.isError){
        var data = {
            authToken: $(".manageSLLink[data-provider='googleplus']").attr("dara-regMail")
        }
        performSubmit("MyAlitaliaServlet-UpdateSession",data);
        if (response.data){
            var data = JSON.parse(response.data);
            if(data.cookieName){
                setGigyaCookie(data.cookieName,data.cookieValue,1);
            }
            var uri = location.origin + internalUrl1.substring(0, internalUrl1.length - "homepage".length) + "myalitalia/popup.html?associaLinkAccount=true";
            var popup = window.open(encodeURI("https://socialize.eu1.gigya.com/socialize.addConnection?oauth_token=" + data.cookieValue + "&redirect_uri=" + uri + "&x_provider=" + provider
                + "&response_type=token&x_sessionExpiration=3600"), "MsgWindow", "width=500,height=500");
        }
    } else {
        disableLoaderMyAlitalia();
    }
};

Login.Session.errorLinkAccount = function (response) {
    disableLoaderMyAlitalia();

};

Login.Session.successmmEDIT = function (response) {
    if (!response.isError) {
        myAlitaliaOptions.feedbackSuccess();
        disableLoaderMyAlitalia();
    } else {
        myAlitaliaOptions.feedbackFail();
        disableLoaderMyAlitalia();
    }
};

function getTipoCrmdData(tipo) {

    switch (tipo) {

        case"PA":
            tipo = "Pass";
            break;
        case"GC":
            tipo = "greenCardRecap";
            break;
        case"VS":
            tipo = "vistoRecap";
            break;

    }

    return tipo

}

Login.Registration.success = function (response) {

    if (!response.isError) {

        disableLoaderMyAlitalia();
        $(".hide-if-pending").hide();
        $("#container").hide();
        $(".title").hide();
        $("#regSuccess").show();
        // gigya.accounts.showScreenSet({
        //     screenSet: regScreenSet,
        //     startScreen: "gigya-verification-pending-screen",
        //     containerID: "containerRegSuccess"
        // });

    } else {
        $("#errorGigya p").text(CQ.I18n.get(response.errorDescription));
        disableLoaderMyAlitalia();
    }
};

Login.RegistrationSocial.Success = function (response) {

    if (!response.isError) {

        performSubmit("MyAlitaliaServlet-loginMa", "", Login.Session.success, Login.Session.error);

    }

};

Login.RegistrationSocial.error = function () {
    alert("Errore Inaspettato in fase di registrazione, provare più tardi");
    disableLoaderMyAlitalia();
};

Login.Registration.error = function (response) {
    alert("Errore Inaspettato in fase di registrazione, provare più tardi");
    disableLoaderMyAlitalia();
};

Login.Session.success.dataPercent = function (response){

    if (!response.isError){
        $(".progress-bar").addClass("myalitalia");
        $("#percent").text($("#percent").attr("label") + " " + response.percent + "%");
        $("#percentualeDesk").text($("#percentualeDesk").attr("label") + " " + response.percent + "%");
        fillPercent(response.percent);
        $("#redirectProfile").attr("url", response.redirectUrl).text(CQ.I18n.get(response.testo));
        $("#urlPercent").attr("href", response.redirectUrl).text(CQ.I18n.get(response.testo));
        disableLoaderMyAlitalia();
    } else {
        disableLoaderMyAlitalia();
    }

};

Login.Session.error.dataPercent = function (response) {
    disableLoaderMyAlitalia();
};

function setGigyaCookie(nomeCookie,valoreCookie, days) {
    var scadenza = new Date();
    var adesso = new Date();
    scadenza.setTime(adesso.getTime() + (days * (24*60*60*1000)));
    document.cookie = nomeCookie + '=' + escape(valoreCookie) + '; expires=' + scadenza.toGMTString() + '; path=/';
}