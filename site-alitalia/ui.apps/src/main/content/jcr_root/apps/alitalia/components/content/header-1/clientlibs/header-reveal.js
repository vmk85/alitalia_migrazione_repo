var headerRevealOptions = {};

// document ready
$( document ).ready( function() {
	$('body').on('click',function(e){
	if ($('.reveal.reveal--large.reveal--search').parent().css('display') == "block"){
		$('#search-bar__input').focus();
	}
	$('.icon.icon--close').on('click',function(){
		$('#search').foundation('close');
		$("#search-bar__input").val("");
		$("#search-bar__input").keyup();
		//remove errors from reveal login
		$('#header-wave-login-errors').html("");
		$('#header-wave-login-errors-SA').html("");
		$('input').removeClass('is-invalid-input');
		// $('.reveal--search .suggestion-box').empty();
	});
	document.getElementById("buttonCloseRevealSearch").addEventListener("click", function( event ) {
	    // display the current click count inside the clicked div
	    event.target.textContent = "click count: " + event.detail;
	    $('#search').foundation('close');
		$("#search-bar__input").val("");
		$("#search-bar__input").keyup();
		//remove errors from reveal login
		$('#header-wave-login-errors').html("");
		$('#header-wave-login-errors-SA').html("");
		$('input').removeClass('is-invalid-input');
  	}, false);
	if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
		$('.reveal-overlay .icon--close--black').on('click',function(){
			$('.reveal-overlay').hide();
			$( '.section--cerca-volo' ).removeClass( 'overlay--mobile' );
    		$('.reveal-overlay').css('display','none');
			$('html').removeClass('is-reveal-open');
			$('body').removeClass('is-reveal-open');

		});
	}

});
	headerRevealOptions.reveals = {};
	// init support popup

	if(isPublishEnvironment == "true") {

        headerRevealOptions.reveals.support =  new Foundation.Reveal( $( '.reveal--support' ), {
            // animationIn: 'fade',
            // animationOut: 'fade',
        } );
        headerRevealOptions.reveals.lingua =  new Foundation.Reveal( $( '.reveal--language' ), {
            // animationIn: 'fade',
            // animationOut: 'fade',
        } );

	}
	headerRevealOptions.reveals.login =  new Foundation.Reveal( $( '.reveal--login' ), {
		// animationIn: 'fade',
		// animationOut: 'fade',
	} );
	headerRevealOptions.reveals.login =  new Foundation.Reveal( $( '.reveal--sa-login' ), {
    		// animationIn: 'fade',
    		// animationOut: 'fade',
    	} );
	headerRevealOptions.reveals.login =  new Foundation.Reveal( $( '.reveal--offerte' ), {
		// animationIn: 'fade',
		// animationOut: 'fade',
	} );
	headerRevealOptions.reveals.search =  new Foundation.Reveal( $( '.reveal--search' ), {
        closeOnClick:false,
        closeOnBackgroundClick:false,
        closeOnEsc:false
		// animationIn: 'fade',
		// animationOut: 'fade',
	} );
	headerRevealOptions.reveals.resetPsw =  new Foundation.Reveal( $( '.reveal--ma-reset-password-form' ), {
		closeOnClick:false,
		closeOnBackgroundClick:false,
		closeOnEsc:false,
		// animationIn: 'fade',
		// animationOut: 'fade',
	} );
	headerRevealOptions.reveals.resetPsw =  new Foundation.Reveal( $( '.reveal--myalitalia-otp' ), {
		closeOnClick:false,
		closeOnBackgroundClick:false,
		closeOnEsc:false
		// animationIn: 'fade',
		// animationOut: 'fade',
	} );
} );

$(document).on('open.zf.reveal', function(){
    $('#search-bar__input').focus();
});