$(function() {

	$("#infovoli_departures").autocomplete({
		lookup: function (query, done) {
			if (findAirport(query)) { done(airports); };
		},
        preserveInput: !1,
        minChars: 3,
        onSearchComplete: function(a, b) {
            for (var c = 0; c < b.length; c++) {
                var d = b[c];
                d.value = d.city + " " + d.type;
            }
        },
        formatResult: function(a, b) {
            return '<div class="sugg_dest_data"><span class="sugg_city_name toPass ' + (a.best ? "best" : "") + '">' + a.city + '</span> - <span class="sugg_city_nation ">' + a.country + '</span><br><span class="sugg_airport">' + a.airport + '</span><span class="sugg_airportCode">(' + a.code + ")</span></span></div>";
        },
        onInvalidateSelection: function() {},
        onSelect: function(a) {
            $(this).val(a.value), $(this).siblings(".apt").text(a.city), $(this).siblings(".city").text(a.type), 
            $(this).siblings("#" + $(this).attr("id") + "_country").val(a.code), $(this).siblings(".j-countryCode").val(a.code), 
            $("#infovoli_origin").val(a.value),
            $("#infovoli_arrival").focus();
        }
    }),
    
    $("#infovoli_arrival").autocomplete({
    	lookup: function (query, done) {
			if (findAirport(query)) { done(airports); };
		},
        preserveInput: !1,
        minChars: 3,
        onSearchComplete: function(a, b) {
            for (var c = 0; c < b.length; c++) {
                var d = b[c];
                d.value = d.city + " " + d.type;
            }
        },
        formatResult: function(a, b) {
            return '<div class="sugg_dest_data"><span class="sugg_city_name toPass ' + (a.best ? "best" : "") + '">' + a.city + '</span> - <span class="sugg_city_nation ">' + a.country + '</span><br><span class="sugg_airport">' + a.airport + '</span><span class="sugg_airportCode">(' + a.code + ")</span></span></div>";
        },
        onInvalidateSelection: function() {},
        onSelect: function(a) {
            $(this).val(a.value), $(this).siblings(".apt").text(a.city), $(this).siblings(".city").text(a.type), 
            $(this).siblings("#" + $(this).attr("id") + "_country").val(a.code), $(this).siblings(".j-countryCode").val(a.code),
            $("#infovoli_destination").val(a.value),
            $("#flightStatusDatePicker2").focus();
        }
    });
});
