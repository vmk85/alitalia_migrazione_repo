
var MAX_ADULTS = 7;
var MAX_KIDS = 6;
var MAX_NEWBORN = 6;
var MAX_ADULTS_KIDS = 7;
var MAX_YOUTHS = 7;

var MIN_ADULTS = 1;
var MIN_KIDS = 0;
var MIN_NEWBORN = 0;
var MIN_YOUTHS = 1;

function checkAddValues(event) {

    var nadults = parseInt($('#adult').val());
    var nkids = parseInt($('#kids').val());
    var nnewborn = parseInt($('#newborn').val());
    var nyouths = parseInt($('#youth').val());

    var inputID = $(this).parent().children("input").attr('id');
    if (inputID == "adult") {
        if (nadults > MAX_ADULTS) {
            $('#adult').val(MAX_ADULTS);
        }
        if (nadults + nkids > MAX_ADULTS_KIDS) {
            $('#adult').val(MAX_ADULTS_KIDS - nkids);
        }
    } else if (inputID == "kids") {
        if (nkids > MAX_KIDS) {
            $('#kids').val(MAX_KIDS);
        }
        if (nadults + nkids > MAX_ADULTS_KIDS) {
            $('#kids').val(MAX_ADULTS_KIDS - nadults );
        }
    } else if (inputID == "newborn") {
        if (nnewborn> MAX_NEWBORN) {
            $('#newborn').val(MAX_NEWBORN);
        }
        if (nnewborn >= nadults) {
            $('#newborn').val(nadults);
        }
    } else if (inputID == "youth") {
        if (nyouths > MAX_YOUTHS) {
            $('#youth').val(MAX_YOUTHS);
        } else {
            $('#adult').val(nyouths);
        }
    }
}

function checkSubValues(event) {

    var nadults = parseInt($('#adult').val());
    var nkids = parseInt($('#kids').val());
    var nnewborn = parseInt($('#newborn').val());
    var nyouths = parseInt($('#youth').val());

    var inputID = $(this).parent().children("input").attr('id');
    if (inputID == "adult") {
        if (nadults < MIN_ADULTS) {
            $('#adult').val(MIN_ADULTS);
        }
        if (nadults  < nnewborn) {
            $('#adult').val(nnewborn);
        }
    } else if (inputID == "kids") {
        if (nkids  < MIN_KIDS) {
            $('#kids').val(MIN_KIDS);
        }
    } else if (inputID == "newborn") {
        if (nnewborn < MIN_NEWBORN) {
            $('#newborn').val(MIN_NEWBORN);
        }
    } else if (inputID == "youth") {
        if (nyouths < MIN_YOUTHS) {
            $('#youth').val(MIN_YOUTHS);
        } else {
            $('#adult').val(nyouths);
        }
    }
}

function checkAddValuesCarnet(event) {
	var maxPassengers;
	var typeOfSearch = $('input[name=SearchType]:checked', '#cercaVoliForm').val();
	if (typeOfSearch == 'a+r') {
		maxPassengers = parseInt($('#maxPassengersAR').val());
	}
	else {
		maxPassengers = parseInt($('#maxPassengersA').val());
	}
	var nadults = parseInt($('#adult').val());
	
	if (nadults + 1 > maxPassengers) {
		$('#adult').val(maxPassengers - 1);
	}
	
	
}

function checkSubValuesCarnet(event) {

	var nadults = parseInt($('#adult').val());
	if (nadults == 1) {
		$('#adult').val(2);
	}
	
	
}

$(document).ready(function() {
	if ($('#isCarnet').val() == 'CRN') {
		$('a.j-numberSelector-plus').on('click', checkAddValuesCarnet);
		$('a.j-numberSelector-minus').on('click', checkSubValuesCarnet);
		
		$('input[name=SearchType]:radio').change(function () {
			
            if ($("input[name=SearchType]:checked").val() == 'a+r') {
            	var maxPassengers = parseInt($('#maxPassengersAR').val());
                if ($('#adult').val() > maxPassengers) {
                	
                	$('#adult').val(maxPassengers);
                }
            }
           
        });
	}
	else {
		$('a.j-numberSelector-plus').on('click', checkAddValues);
		$('a.j-numberSelector-minus').on('click', checkSubValues);
	}
	
	if ($('#CUG').val() == 'FAM') {
		MIN_ADULTS = 2;
	}
	$('#cercaVoliForm').on('keyup keypress', function(e) {
		  if (e.which === 13) { 
		    e.preventDefault();
		    return false;
		  }
	});
	$('#cercaVoliSubmit').on('click', function(e) {
		validation(e, 'flightsearchconsumer', '#cercaVoliForm',
			flightSearchValidationSuccess,
			flightSearchValidationError);
	});
});

function flightSearchValidationSuccess(data) {
	if (data.result) {
		saveDataIntoLocalstorage();
		removeErrors();
		$('#cercaVoliSubmit').off('click');
		$('#cercaVoliForm').submit();
	} else {
		showErrors(data);
		if(window["analytics_trackBookingErrors"] && typeof window["analytics_trackBookingErrors"] === "function"){
			window["analytics_trackBookingErrors"](data.fields, "searchflight");
		}
	}
}

function flightSearchValidationError() {
	removeErrors();
	$('#cercaVoliSubmit').off('click');
}

function saveDataIntoLocalstorage() {
	try {
		
		var cp = getAirportData($('input#Origin').val());
		var cittaPartenza = {
			aptCode : $('input#Origin').val(),
			nazione : $('#originKeyCountryCode').text(),
			nazioneT : cp.country || false,
			aptName : $('#originKeyAirportName').text(),
			aptNameT : cp.airport || false,
			cityName : $('#originKeyCityCode').text(),
			cityNameT : cp.city || false,
		};
		
		var cd = getAirportData($('input#Destination').val());
		var cittaDestinazione = {
			aptCode : $('input#Destination').val(),
			nazione : $('#destinationKeyCountryCode').text(),
			nazioneT : cd.country || false,
			aptName : $('#destinationKeyAirportName').text(),
			aptNameT : cd.airport || false,
			cityName : $('#destinationKeyCityCode').text(),
			cityNameT : cd.city || false,
		};
	
		var dataPartenza = $('input[name=DepartureDate]').val();
		var dataRitorno = $('input[name=ReturnDate]').val();
		var nAdulti = parseInt($('input#adult').val());
		var nBambini = parseInt($('input#kids').val());
		var nNeonati = parseInt($('input#newborn').val());
		var nGiovani = parseInt($('input#youth').val());
		var tipoOfferteGiovani = 
			$("input[name='SearchType_Giovani']:checked").val();
		var tipoViaggio = 
			$('input[name=SearchType]:checked', '#cercaVoliForm').val();
		var classeVolo =
			$('input[name=classOfFlight]:checked', '#cercaVoliForm').val() || '';
		var CUG = $('#CUG').val();
		var fromSpecialOffer =
			$('input[name=FromSpecialOffer]', '#cercaVoliForm').val() || '';
	
		var ricercheRecenti;
		var items = localStorage.getItem("ricercheRecenti");
		if (items == null || items == 'undefined' || items == '[]'
				|| items.length == 0) {
			ricercheRecenti = [];
		} else {
			ricercheRecenti = JSON.parse(items);
		}
	
		var item = {
			cittaPartenza : {
				nome : cittaPartenza.cityName,
				nomeT : cittaPartenza.cityNameT,
				nazione : cittaPartenza.nazione,
				nazioneT : cittaPartenza.nazioneT,
				aeroporto : cittaPartenza.aptName,
				aeroportoT : cittaPartenza.aptNameT,
				codiceAeroporto : cittaPartenza.aptCode,
			},
			cittaArrivo : {
				nome : cittaDestinazione.cityName,
				nomeT : cittaDestinazione.cityNameT,
				nazione : cittaDestinazione.nazione,
				nazioneT : cittaDestinazione.nazioneT,
				aeroporto : cittaDestinazione.aptName,
				aeroportoT : cittaDestinazione.aptNameT,
				codiceAeroporto : cittaDestinazione.aptCode,
			},
			tipoViaggioLabel : (tipoViaggio == 'a+r') ?
					'cercaVolo.label.andataritorno' : 'cercaVolo.label.soloandata',
			tipoViaggio : tipoViaggio,
			dataPartenza : dataPartenza,
			dataRitorno : dataRitorno,
			classeViaggio : (classeVolo == 'economy') ?
					'cercaVolo.label.economy'
					: ((classeVolo == '') ? '' : 'cercaVolo.label.business'),
			nPasseggeri : nAdulti + nBambini + nNeonati,
			nAdulti : nAdulti,
			nBambini : nBambini,
			nNeonati : nNeonati,
			cug : CUG,
			fromSpecialOffer : fromSpecialOffer,
			tipoOfferteGiovani : tipoOfferteGiovani
		}
	
		ricercheRecenti.unshift(item);
		if (ricercheRecenti.length > 20) {
			ricercheRecenti.pop();
		}
		localStorage.setItem("ricercheRecenti", JSON.stringify(ricercheRecenti));
		
	} catch(e) {}
}