$(document).ready(function() {
	//getMessages();
	$("#messaggiSearch").on("click", function(){
		day = $('select#giorno').find(':selected').val();
		month = $('select#mese').find(':selected').val();
		year = $('select#anno').find(':selected').val();
		getMessages(day, month, year);
	});
	//return false;
});

function getMessages(day, month, year) {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	params = {};
	if (isValidDate(day, month, year)) {
		params.day = day;
		params.month = month;
		params.year = year;
	}
	$.ajax({
		url : url + ".lista-messaggi-partial.html",
		context : document.body,
		data : params
	})
	.done(successListMessages)
	.fail(failListMessages);
	return false;
}

function successListMessages(data) {
	$('#millemiglia_list_messages div:gt(0)').remove();
	$('#millemiglia_list_messages').append(data);
	initAccordions();
	return false;
}

function failListMessages() {
	return false;
}

function isValidDate(day, month, year) {
	if (typeof day === "undefined" || typeof month === "undefined"
		|| typeof year === "undefined") {
		return false;
	}
	if (month > 12 || day > daysInMonth(month, year)) {
		return false;
	}
	return true;
}

function daysInMonth(m, y) {
	switch (m) {
	case 2:
		return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
		return 31;
	default:
		return 30
	}
}