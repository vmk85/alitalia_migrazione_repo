var checkinSelezionaPostoOptions = {};

var pid = "";
var seatPosto;
var nuovoPosto;
var postoComfort;
var comfortSeatPrice;
var data_row;
var data_col;
var close_popup;
var idx ;
var origin = "";
var params = {}, queries, temp, i, l;
// Split into key/value pairs
queries = window.location.search.split("&");
// Convert the array of strings into an object
for ( i = 0, l = queries.length; i < l; i++ ) {
    temp = queries[i].split('=');
    params[temp[0]] = temp[1];
}
var editBp = params["editBp"];
var flight = params["flight"];var interval = null;


// document ready
$( document ).ready( function() {

    if ($(document).width() < 1024){
        $(".full-bg-section").css("padding-top","70px");
    }



    if (editBp == "true"){
        $("#hrefButton").attr("href",$("#hrefButton").attr("href")+"?&editBp=true&flight="+flight)
        $("#hrefButton2").attr("href",$("#hrefButton").attr("href")+"?&editBp=true&flight="+flight)
    }

    $(".tabs-title").each(function(){
        if ($(this).hasClass("is-active"))
            origin = $(this).data("origin");
    });

    if (origin == '')
    {
        $(".accordion-item").each(function(){
            if ($(this).hasClass("is-active"))
                origin = $(this).data("origin");
        });
    }


	$('.checkin-seleziona-posto').foundation();

	checkinSelezionaPostoOptions.initFlightSelector();
	checkinSelezionaPostoOptions.initPlaneSectionSelector();

	$('a.j-availableSeat').not( ".comfort" ).not( ".emergency" ).on('click', checkinSelezionaPostoOptions.changeSeats);

	//$('.seats-list ul li a').off();
	$('.seats-list ul li a').bind('click', changePassenger);

    var arraySeats = [];
    $('li[data-passenger]').each(function(){
        var passenger = $(this).attr("data-passenger");
        var seat = $(this).find('.seat').html();
        arraySeats.push(seat+"-"+passenger);
    });
    jQuery.each(arraySeats, function(index, item) {
        var numSeat =  item.split('-')[0].slice(1);
        var letterSeat = item.split('-')[0].slice(0,1);
        var passengerId = item.split('-')[1];
        $('div.seatMapDiv').each(function(){
            var html = $(this).find('td[data-row = "'+numSeat+'"][data-col = "'+letterSeat+'"] a').removeClass('j-availableSeat').removeClass('j-assignedSeat').addClass('j-assignedSeat').html(passengerId);
            if ($(this).find('td[data-row = "'+numSeat+'"][data-col = "'+letterSeat+'"]')){
                $(this).find('td[data-row = "'+numSeat+'"][data-col = "'+letterSeat+'"]').removeClass('available').addClass('seatAssigned');
            }
        });


    });
    //Tooltip closing right
    $('td.extraComfort a.comfort').on('click', showPopup);

    $('tr.emergencyRow td a').on('click', showPopupEmergency);

    $(".clickExtraComfort").on("click", function(){
        var element = $("[data-seat='" + $(this).data('seat') + "']");
        changeSeat(element);
        close_popup.trigger("click");
    });

    $(".clickNearEmergency").on("click", function(){
        var element = $("[data-seat='" + $(this).data('seat') + "']");
        element = $(this).parents("td.emergency").find("a");
        changeSeat(element);
        close_popup.trigger("click");
    });

    //$("#btCart").on("click", function(){
        performSubmit('checkingetcart','#frmCart',loadCartSuccess,loadCartFail);
    //});

    $(".tabs>li.tabs-title").click(function(e){
        e.preventDefault();

        origin = $(this).data("origin");
        $(".seats-list ul li").each(function(){
            if ($(this).hasClass("selected"))
            {
                if ($(this).hasClass(origin))
                    pid = $(this).data("passenger");
            }
        });

        var _pid = pid.toString().substring(0, 2);
        var fila = "";

        // Cerco il link attivo della tratta
        var ok = false;
        if ($(".imageRows__row.active[data-pid='" + _pid + "']").length > 0)
        {
            fila = $(".imageRows__row.active[data-pid='" + _pid + "']").data("fila");
            setTimeout(function(){
                $(".imageRows__row.active[data-fila='" + fila + "'][data-pid='" + _pid + "']").trigger("click");
            }, 100);
            ok = true;
        }

        if (!ok)
        {
            // Prendo il primo link della tratta
            setTimeout(function(){
                $(".imageRows__row[data-pid='" + _pid + "']").first().trigger("click");
                fila = $(".imageRows__row[data-pid='" + _pid + "']").first().data("fila");
            }, 100);
        }

        $(".seatMapDiv[data-passenger-id='" + pid + "']").css('display','block');
        $(".seatMapDiv[data-passenger-id='" + pid + "'] .chooseSeat__passengerSeats.sector[data-fila='" + fila + "']").addClass('is-active');


    });

    interval = setInterval(function() {
        gotoPassengersSection("");
    }, 200);

    $('.tabs').on('change.zf.tabs', function() {
        interval = setInterval(function() {
            gotoPassengersSection("");
        }, 200);
        setTimeout(function( ) { clearInterval(interval); }, 2000)
    });

    $(".passenger-exclude-chd a").click(function() {
        var id = $(this).find(".id").first().text();
        interval = setInterval(function() {
            gotoPassengersSection(id);
        }, 200);
        setTimeout(function( ) { clearInterval(interval); }, 2000)
    });

    setTimeout(function( ) { clearInterval(interval); }, 2000);
    $("#main-content").css("padding-top", $(".checkin-topbar-container").css("height"))



} );

function gotoPassengersSection(id){
    //controllo nella quale sezione è assegnato il posto del passeggero e visualizzo quella sezione

    var activeSeatmap = $(".tabs-content").find(".tabs-panel.is-active");
    var assignedSeat;
    if(id==""){
        assignedSeat = $(activeSeatmap).find(".j-assignedSeat")[0];
    }else{
        assignedSeat = $(activeSeatmap).find(".passengerSeatId-"+id)[0];
    }


    var sections = [];
    var assignedSeatRow = "";

    if(assignedSeat){
        assignedSeatRow = $(assignedSeat).parent().data('row');
    }

    $(activeSeatmap).find('.seatmap-section-nav-up').each(function(i, obj) {
        sections.push($(obj).data("fila"));
    });
    $(activeSeatmap).find('.seatmap-section-nav-down').each(function(i, obj) {
        sections.push($(obj).data("fila"));
    });


    var uniqueSections = [];
    $.each(sections, function(i, el){
        if($.inArray(el, uniqueSections) === -1) uniqueSections.push(el);
    });

    var passengerSection="";

    $.each(uniqueSections, function(i, el){
        var from = parseInt(el.split("-")[0]);
        var to = parseInt(el.split("-")[1]);
        for (var i = from; i <= to; i++) {
            if(assignedSeatRow == i){
                passengerSection = el;
            }
        }
    });

    var triggered = false;
    if($(activeSeatmap).find(".chooseSeat__seats").is(':visible')){
        if($(activeSeatmap).find('a.seatmap-section-nav-up[data-fila="'+passengerSection+'"]').length>0){
            $($(activeSeatmap).find('a.seatmap-section-nav-up[data-fila="'+passengerSection+'"]')[0]).trigger("click");
            triggered = true;
        }
        if(triggered == false){
            $($(activeSeatmap).find('a.seatmap-section-nav-down[data-fila="'+passengerSection+'"]')[0]).trigger("click");
        }

        clearInterval(interval);
    }

}


checkinSelezionaPostoOptions.initPlaneSectionSelector = function() {
	/*$('.checkin-seleziona-posto .section-selector a').click(function() {
		$(this).addClass('is-active').parent().siblings().find('a').removeClass('is-active');
		return false;
	});*/
	$('.checkin-seleziona-posto .section-selector .imageRows__row').click(function() {

        origin = '';
	    $(".tabs-title").each(function(){
            if ($(this).hasClass("is-active"))
                origin = $(this).data("origin");
        });
        if (origin == '')
        {
            $(".accordion-item").each(function(){
                if ($(this).hasClass("is-active"))
                    origin = $(this).data("origin");
            });
        }

        var fila = $(this).data('fila');

        $(".seats-list ul li").each(function(){
            if ($(this).hasClass("selected"))
            {
                if ($(this).hasClass(origin))
                    pid = $(this).data("passenger");
            }
        });

		// interazione sul selettore a lato con immagine aereoplanino di sfondo
		$(this).addClass('active').siblings().removeClass('active');
		$('.image img', $(this).closest('.imageRows')).attr('src', $(this).data('image'));

        $('.chooseSeat__passengerSeats').each(function(){

            $(this).removeClass('is-active');
            if ($(this).data('pid') == pid.toString().substring(0, 2) && $(this).data('fila') == fila)
                $(this).addClass('is-active');

        });
        /*
		// accensione del settore corrispondente
		var _this = this;
		$('.chooseSeat__passengerSeats').removeClass('is-active').filter(function() {
		    console.log($(this).data('fila'), $(_this).data('fila'));
			return $(this).data('fila') == $(_this).data('fila') && pid == $(_this).data('pid');
		}).addClass('is-active');*/
	});

	// interazione da frecce sopra-sotto nel blocco del settore --> sposta il settore selezionato e modifica anche il settore a lato
	$('.seatmap-section-nav a').click(function() {
		// accensione del settore corrispondente

		pid = $(this).data('pid');
        var fila = $(this).data('fila');

        $('.chooseSeat__passengerSeats').each(function(){

            $(this).removeClass('is-active');
            if ($(this).data('pid') == pid && $(this).data('fila') == fila)
                $(this).addClass('is-active');

        });

		var _this = this;
		/*$('.chooseSeat__passengerSeats').removeClass('is-active').filter(function() {
			console.log($(this).data('fila'), $(_this).data('fila'));
			return $(this).data('fila') == $(_this).data('fila');
		}).addClass('is-active');*/

		$('.checkin-seleziona-posto .section-selector .imageRows__row').removeClass('active').filter(function() {
			return $(this).data('fila') == $(_this).data('fila');
		}).addClass('active');

		return false;
	});
}

checkinSelezionaPostoOptions.initFlightSelector = function() {


    $('.seats-list li a').click(function() {

        var size = Foundation.MediaQuery.current;
        if (size == 'medium' || size == 'small') {
            $('body').addClass('is-reveal-open');
            $('.seat-configurator').addClass('reveal-mode');
            $('#hrefButton2').hide();
            $('#hrefButton').hide();
            // $(".full-bg-section").hide();
            $('footer').hide();
        }

        return false;
    });

    $('.seat-configurator .close-button, .seat-configurator .checkin-button-wrap .button').not('.tooltip .close-button').click(function() {
        $('body').removeClass('is-reveal-open');
        $('.seat-configurator').removeClass('reveal-mode');
        $('#hrefButton2').show();
        $('#hrefButton').show();
        // $(".full-bg-section").show();
        $('footer').show();
    });

    $(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
        if (newSize == 'large') {
            $('body').removeClass('is-reveal-open');
            $('.seat-configurator').removeClass('reveal-mode');
            $('#hrefButton2').show();
            $('#hrefButton').show();
            // $(".full-bg-section").show();
            $('footer').show();


        }
    });
}

checkinSelezionaPostoOptions.changeSeats = function(e) {

    e.preventDefault();

    var seat = e.target;

    if(tooltipSelector != undefined && tooltipSelector != ''){
        $(tooltipSelector).hide();
    }
    if(seat.parentElement.parentElement.getAttribute('class')!= 'emergencyRow'){
	    changeSeat($(this));
    }
}

function showPopup()
{
    var topDiv =  $(this).offset().top -$(this).parent().find(".tooltip").height() ;
    var topStick = $(".checkin-topbar-container").height();
    var tot = topDiv - topStick - 50;
    $('html, body,.column.large-9.show-for-large.seat-configurator.reveal-mode').animate({scrollTop:tot}, 'slow');
    // console.log($(this).parent().find(".tooltip.tooltip-desktop"));
    $('.tooltip').css('display','none');
    $(this).parent().find('.tooltip').css('display','block');
    close_popup = $(this).parent().find('.tooltip').find(".close-button");
}

function showPopupEmergency(e)
{
    e.stopImmediatePropagation();
    if(tooltipSelector != undefined && tooltipSelector != ''){
            $(tooltipSelector).hide();
    }
    $('.tooltip.emergency').css('display','none');
    $(this).parent().find('.tooltip.emergency').css('display','block');
    close_popup = $(this).parent().find('.tooltip.emergency').find(".close-button");
}

function changeSeat(element)
{
    // se in precedenza e' stato selezionato un posto comfort, nascondo il relativo popup
	var comfortPopup = $('.tooltip.emergency');
	if (comfortPopup !== undefined && comfortPopup.length > 0)
		comfortPopup.css('display','none');
    enableLoaderCheckin();

    var origin = '';
    $(".tabs-title").each(function(){
        if ($(this).hasClass("is-active"))
            origin = $(this).data("origin");
    });
    if (origin == '')
    {
        $(".accordion-item").each(function(){
            if ($(this).hasClass("is-active"))
                origin = $(this).data("origin");
        });
    }

    // console.log("Cambio posto");

    // Controllo se è un posto confort oppure

    $('a.j-availableSeat').unbind('click', checkinSelezionaPostoOptions.changeSeats);

    $('td.extraComfort a.comfort').unbind('click', showPopup);

    // Disabilito anche il cambio passeggero
    $('.seats-list ul li a').unbind('click', changePassenger);


    var cercaPosto;
    var cambioPostoText = CQ.I18n.get('checkin.seatMap.changinSeat.label');


    seatPosto = $( ".seats-list" ).find(".selected." + origin).find(".seat");

    if (!seatPosto.hasClass("adapt")){
        seatPosto.addClass("adapt");
    }

    seatPosto.html(cambioPostoText);


    $(".seats-list ul li").each(function(){
        if ($(this).hasClass("selected"))
        {
            if ($(this).hasClass(origin))
                pid = $(this).data("passenger");
        }
    });

    if ($(element.parent('.available')).length == 0) {
        cercaPosto = $(element.parent('.extraComfort'));

    }
    else {
        cercaPosto = $(element.parent('.available'));
     }

    nuovoPosto = cercaPosto.attr('data-row') + cercaPosto.attr('data-col');
    data_row = cercaPosto.attr('data-row');
    data_col = cercaPosto.attr('data-col');

    var elementoID = $('.selected.' + origin).find('.passengerdata');

    idx = element.attr("idx");
    comfortSeatPrice = "";
    if(cercaPosto.find('.price')){
        comfortSeatPrice = cercaPosto.find('.price').text().split(' ')[0];
    }

    $("#nuovoPosto" + idx).val(cercaPosto.attr('data-row') + cercaPosto.attr('data-col'));
    $("#postoComfort" + idx).val(cercaPosto.hasClass('extraComfort'));
    $("#pricePostoComfort" + idx).val(comfortSeatPrice);
    $("#nome" + idx).val(elementoID.attr('data-value-nome'));
    $("#cognome" + idx).val(elementoID.attr('data-value-cognome'));
    $("#bookingClass" + idx).val(elementoID.attr('data-value-class'));
    $("#pnr" + idx).val(elementoID.attr('data-value-pnr'));

    performSubmit('changeseatsrest','#frmSeat'+idx,changeSeatSuccess,changeSeatFail);

}

function changeSeatSuccess(data)
{

    if (data.isError) {
        changeSeatFail(data);
    } else {

        origin = '';
        $(".tabs-title").each(function () {
            if ($(this).hasClass("is-active"))
                origin = $(this).data("origin");
        });
        if (origin == '')
        {
            $(".accordion-item").each(function(){
                if ($(this).hasClass("is-active"))
                    origin = $(this).data("origin");
            });
        }

        $(".seats-list").find(".selected." + origin).find(".seat." + pid).html(nuovoPosto).removeClass("adapt");
        $("#seats-list-mobile").find(".seat").html(nuovoPosto);


        var divMap = $('.seatMapDiv[data-passenger-id="' + pid + '"]');

        var cur_row = divMap.find(".seatAssigned").not('.occupied').data('row');
        var cur_col = divMap.find(".seatAssigned").not('.occupied').data('col');

        // Rimuovo classe verde e numero posto precedente
        var classes = divMap.find(".seatAssigned").not('.occupied').find("a").attr("class");
        classes = classes.split(" ");
        var passengerClass;
        var i;
        for (i = 0; i < classes.length; ++i) {
            if(classes[i].indexOf("passengerSeatId-") >= 0){
                passengerClass = classes[i];
            }
        }
        divMap.find('.'+passengerClass).removeClass(passengerClass);
        divMap.find(".seatAssigned").not('.occupied').find("a").html("&nbsp;");
        divMap.find(".seatAssigned").not('.occupied').find("a").removeClass("j-assignedSeat").addClass("j-availableSeat");
        divMap.find(".seatAssigned").not('.occupied').removeClass("seatAssigned").addClass("available");

        // Aggiungo il nuovo posto prenotato
        divMap.find('.available[data-row="' + data_row + '"][data-col="' + data_col + '"]').find("a").html(pid.toString().slice(-1));
        divMap.find('.available[data-row="' + data_row + '"][data-col="' + data_col + '"]').removeClass("available").addClass("seatAssigned");
        divMap.find('.seatAssigned[data-row="' + data_row + '"][data-col="' + data_col + '"] a').removeClass("j-availableSeat ").addClass("j-assignedSeat");
        divMap.find('.seatAssigned[data-row="' + data_row + '"][data-col="' + data_col + '"] a').removeClass(passengerClass).addClass(passengerClass);

        // $('.seatMapDiv.' + pid).each(function () {
        // Solo la mappa dello stesso volo filtrata per volo/segmento (prime due cifre pid)
        $('.seatMapDiv[class*=' + pid.toString().slice(0,2) + ']').each(function ()
        {

            // Metto il nuovo posto selezionato a non selezionabile per gli altri passeggeri
            if ($(this).data('passenger-id') != pid) {

                var passengerNumber = pid.toString().charAt(pid.toString().length-1);

                /*$(this).find('.available[data-row="' + data_row + '"][data-col="' + data_col + '"]').removeClass("available").addClass("seatAssigned").addClass("occupied");
                $(this).find('.seatAssigned[data-row="' + data_row + '"][data-col="' + data_col + '"]').find('a').remove();
                $(this).find('.seatAssigned[data-row="' + data_row + '"][data-col="' + data_col + '"]').append('<span>X</span>');*/

                $(this).find('.available[data-row="' + data_row + '"][data-col="' + data_col + '"]').removeClass("available").addClass("occupied other-passenger");
                $(this).find('.occupied[data-row="' + data_row + '"][data-col="' + data_col + '"]').find('a').remove();
                $(this).find('.occupied[data-row="' + data_row + '"][data-col="' + data_col + '"]').append('<span class="other-passenger-number">' + passengerNumber + '</span>');

                // $(this).find('.seatAssigned[data-row="' + cur_row + '"][data-col="' + cur_col + '"]').removeClass("seatAssigned").removeClass("occupied").addClass("available");
                $(this).find('.occupied[data-row="' + cur_row + '"][data-col="' + cur_col + '"]').removeClass("seatAssigned").removeClass("occupied").addClass("available");
                $(this).find('.available[data-row="' + cur_row + '"][data-col="' + cur_col + '"]').find('span').remove();
                $(this).find('.available[data-row="' + cur_row + '"][data-col="' + cur_col + '"]').append('<a class="j-availableSeat " href="javascript:;" idx="' + idx + '">&nbsp;</a>');
                // $(this).find('.available[data-row="' + cur_row + '"][data-col="' + cur_col + '"] a').bind('click', checkinSelezionaPostoOptions.changeSeats);

            }

        });

        $('a.j-availableSeat').not(".comfort").bind('click', checkinSelezionaPostoOptions.changeSeats);

        $('td.extraComfort a.comfort').bind('click', showPopup);

        $('.seats-list ul li a').bind('click', changePassenger);

        //if (divMap.find('.seatAssigned[data-row="' + data_row + '"][data-col="' + data_col + '"]').find('.price').html() !== undefined) {
        $(".cart-review .price").html(CQ.I18n.get('checkin.ancillary.updating.label'));
        performSubmit('checkingetcart', '#frmCart', loadCartSuccess, loadCartFail);
        //}

    }

    disableLoaderCheckin();

}

function changePassenger()
{
    // console.log("Cambio passeggero");
    var idPassenger = $(this).parent().data("passenger");
    //var idPassenger = $(this).find('span.id').html();
    seatmapOptions.selectOne(idPassenger);
}

function changeSeatFail(data)
{

    origin = '';
    $(".tabs-title").each(function(){
        if ($(this).hasClass("is-active"))
            origin = $(this).data("origin");
    });
    if (origin == '')
    {
        $(".accordion-item").each(function(){
            if ($(this).hasClass("is-active"))
                origin = $(this).data("origin");
        });
    }

    $( ".seats-list" ).find(".selected." + origin).find(".seat").html("impossibile scegliere questo posto");

    var old_row = $('.seatMapDiv[data-passenger-id="' + pid + '"]').find(".seatAssigned").not('.occupied').data('row');
    var old_col = $('.seatMapDiv[data-passenger-id="' + pid + '"]').find(".seatAssigned").not('.occupied').data('col');
    
    setTimeout(function(){
        $( ".seats-list" ).find(".selected." + origin).find(".seat").html(old_row + "" + old_col).removeClass("adapt");
    }, 2000);
    
    $('a.j-availableSeat').not( ".comfort" ).bind('click', checkinSelezionaPostoOptions.changeSeats);
    $('td.extraComfort a.comfort').bind('click', showPopup);

    disableLoaderCheckin();


}