"use strict";
use(function() {
    var flights = this.flights;
    var bp = flights.flightsRender.get(0).isBpPrintPermitted;
    return bp;
});