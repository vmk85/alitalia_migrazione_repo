"use strict";
use(function(){
    try {
        var index = this.index;
        return {bg: granite.resource.properties["bg"+index], title: granite.resource.properties["titleTuto"+index]};
    } catch (err) {
        return null;
    }
})