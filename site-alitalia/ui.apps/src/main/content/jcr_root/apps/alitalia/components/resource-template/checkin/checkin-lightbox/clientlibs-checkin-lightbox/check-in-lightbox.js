$('a#yesButton').on("click", function(e){
	var lightboxType = $('div.customOverlay__main.j-lightboxType').attr('id');
	$('input#'+ lightboxType).val("true");
	if(typeof window['checkin_dcs_multitrack_passengerLightboxYes'] == 'function'
		&& window["analytics_youngType"]){
		var youngType = window["analytics_youngType"];
		checkin_dcs_multitrack_passengerLightboxYes(youngType);
	}
	passengerDataSubmit(e);
});

$('a#noButton').on("click", function(event){
	event.preventDefault();
	$('.j-overlayBg, .j-overlayContainer, .j-overlayClose').velocity('transition.fadeOut', {
		complete: function() {
			$(this).remove();
		}
	});
	$('body').removeClass('overlayActive');

	enableSubmitButton();
});

$('.j-overlayClose, .j-overlayBg').on('click',function(event){
	enableSubmitButton();
});

function enableSubmitButton(){
	$('#passengersSubmit').on('click', passengerDataSubmit);
	$('#passengersSubmit').removeClass('isDisabled');
}
