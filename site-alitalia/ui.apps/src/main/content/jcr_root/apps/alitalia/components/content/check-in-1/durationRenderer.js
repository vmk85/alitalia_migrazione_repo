"use strict";

use(function() {
    var inputDuration = this.duration;
    var newDuration;
    var dSplit = inputDuration.split(":");
    var newDuration = parseInt(dSplit[1]);
    //console.log("len : " + dSplit.length);
    if (parseInt(dSplit[1]) > 0) {
    	newDuration = dSplit[0] + "H" + ":" + dSplit[1] + "'";
    } else if (parseInt(dSplit[1]) == 0) {
        newDuration = dSplit[0] + "H" + "    " ;
    }
    return newDuration;
});