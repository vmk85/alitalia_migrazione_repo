"use strict";
use(function () {

    var cart = this.cartTmp;

    var totLounge = 0;
    var totFast = 0;
    var totBaggage = 0;

    var checkLounge = [];
    var checkFast = [];
    var selBaggage = [];

    var currency = this.currencyTmp;

    var checkInsurance = false;

    if (cart.checkinCart != null)
    {
        for (var x=0; x < cart.checkinCart.AZFlightCart.size(); x++)
        {

            var AZFlightCart = cart.checkinCart.AZFlightCart.get(x);

            for (var y=0; y < AZFlightCart.AZSegmentCart.size(); y++)
            {

                var AZSegmentCart = AZFlightCart.AZSegmentCart.get(y);

                for (var z=0; z < AZSegmentCart.AZCartPassenger.size(); z++)
                {

                    var AZCartPassenger = AZSegmentCart.AZCartPassenger.get(z);

                    var passengerID = AZCartPassenger.passengerID;

                    checkLounge[z] = false;
                    checkFast[z] = false;
                    selBaggage[z] = 0;

                    for (var k=0; k < AZCartPassenger.AZCartAncillary.size(); k++)
                    {

                        var AZCartAncillary = AZCartPassenger.AZCartAncillary.get(k);

                        if (AZCartAncillary.group == 'BG')
                        {
                            totBaggage += Number(AZCartAncillary.price);
                            selBaggage[z] += 1;
                        }

                        if (AZCartAncillary.group == 'LG')
                        {
                            totLounge += Number(AZCartAncillary.price);
                            checkLounge[z] = true;

                        }

                        if (AZCartAncillary.group == 'TS')
                        {
                            totFast += Number(AZCartAncillary.price);
                            checkFast[z] = true;

                        }

                        currency = AZCartAncillary.currency;

                    }

                }

            }

        }
    }
    if (cart.aziInsurancePolicy != null)
    {
        checkInsurance = true;
    }

    return {
        totBaggage: totBaggage,
        totLounge: totLounge,
        totFast: totFast,
        selBaggage: selBaggage,
        checkLounge: checkLounge,
        checkFast: checkFast,
        checkInsurance: checkInsurance,
        currency: currency
    };

});