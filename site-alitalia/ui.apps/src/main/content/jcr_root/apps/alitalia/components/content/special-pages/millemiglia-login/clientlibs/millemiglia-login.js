setI18nTranslate('captcha.userLocked.error');
setI18nTranslate('header.socialLogin.error');
/* event handlers */
var targetPage;

function onPageLoginDocumentReady(e) {
	preparePageLoginSocial();
	// $('#loginSubmit').bind('click', onPageLoginButtonClick);
	invokeShowCaptcha("captcha_login_spacialpages", "1");
	/* Strong Auth - Begin */
     $("#loginSubmit-").unbind('click', onPageLoginButtonSAClick);
     $("#loginSubmit").bind('click', onPageLoginButtonSAClick);
    /* Strong Auth - End */
}

function onPageLoginButtonClick(e) {
	e.preventDefault();
	hideLoginPassanteError();
	removeErrors();
	performValidation('millemiglialoginvalidation', '#form-millemiglia-login',
			onPageLoginValidationSuccess, onPageLoginValidationError);
}

function onPageLoginValidationSuccess(data) {

	if (data.result) {
		var mmCode = $('#mmcode').val();
		var mmPin = $('#pincode').val();
		var rememberMe = "0";
		if ($("#rememberme").is(":checked")) {
			rememberMe = "1";
		}
		targetPage=$("#form-perform-login input[name=resource]").val();
		startLoginWithMilleMigliaCredentials(mmCode, mmPin, rememberMe,
				componentLoginUnexpectedErrorCallback, componentLoginFailureCallback,targetPage,componentLoginUserLockedError,true); // (defined in clientlibs-login)
	} else {
		if (typeof refreshReCaptchaLogin != 'undefined') {
			refreshReCaptchaLogin(spacialPagesRecaptchaID);
		}
		showErrors(data);
	}
}

function loginOldSenzaAdeguamento() {

	hideLoginPassanteError();

    $("#form-mm-pin").val($("#pincode").val());

    $('[name="code"]').val($("#mmcode").val());
    $('[name="millemiglia"]').val($("#mmcode").val());

    if(!$(this).hasClass("disabled")) {
        if($("#mmcode").val() == "" || $("#pincode").val() == "") {
            
            showLoginPassanteError(CQ.I18n.get("message.generic.empty"));
            
        } else {
            try {
                if ($("#submit-feedback-login").length > 0) {
                    if (headerWaveRecaptchaID === undefined) {
                        //                    $("#submit-feedback-login").html('');

                        headerWaveRecaptchaID = grecaptcha.render('submit-feedback-login',{
                            'sitekey':invisibleRecaptchaSiteKey,
                            'callback':"onHeaderButtonLoginClick",
                            'size':"invisible"
                        });
                    }
                    grecaptcha.execute(headerWaveRecaptchaID);
                    removeErrors();
                }
            } catch(exception) {
                console.log(exception);
            }
        }

    }else{
        return false;
    }
	
}

function onPageLoginSAValidationSuccess(data) {
    var userName =  $("#mmcode").val();
    var password =  $("#pincode").val();


    if (data.result) {

        var rememberMe = "0";
        if ($("#rememberme").is(":checked")) {
            rememberMe = "1";
        }
        targetPage=$("#form-perform-login input[name=resource]").val();
        
        /*Check aggiunto per eliminazione adeguamento - DA ELIMINARE QUANDO SI RIPRISTINA ADEGUAMENTO*/
        if (validationLoginNewUsername($('#mmcode').val())){
            if(validationLoginNewPassword($('#pincode').val())){
            	
            	loginOldSenzaAdeguamento();
            	
            } else if (isPasswordStrongAuthentication($('#pincode').val())){
               //TODO: password non conforme
            	startLoginSAWithMilleMigliaCredentials(userName, password, rememberMe,
                	    componentLoginUnexpectedErrorCallback, componentLoginFailureCallback,targetPage,componentLoginUserLockedError,true);
            } else {
            	showLoginPassanteError(CQ.I18n.get("millemiglia.password.error.notstrong"));
            }
        } else if(isUsernameStrongAuthentication($('#mmcode').val())){
            if(isPasswordStrongAuthentication($('#pincode').val())){
            	startLoginSAWithMilleMigliaCredentials(userName, password, rememberMe,
            	    componentLoginUnexpectedErrorCallback, componentLoginFailureCallback,targetPage,componentLoginUserLockedError,true);
            } else {
                //TODO: password non conforme
            	showLoginPassanteError(CQ.I18n.get("millemiglia.password.error.notstrong"));
            }
        } else {
            //TODO: username non conforme
        	hideLoginPassanteError();
            showLoginPassanteError(CQ.I18n.get("millemiglia.username.error.notstrong"));
        }
        
        /*Fine Check - DA ELIMINARE QUANDO SI RIPRISTINA ADEGUAMENTO*/

        /*Da decoomentare quando si ripristina l'adeguamento----INIZIO-----*/
        //startLoginSAWithMilleMigliaCredentials(userName, password, rememberMe,
        //    componentLoginUnexpectedErrorCallback, componentLoginFailureCallback,targetPage,componentLoginUserLockedError,true);
        /*Da decoomentare quando si ripristina l'adeguamento----FINE-----*/
    
    } else {
        if (typeof refreshReCaptchaLogin !== 'undefined') {
            refreshReCaptchaLogin(spacialPagesRecaptchaID);
        }
        showErrors(data);
    }
}

function onPageLoginValidationError() {
	componentLoginUnexpectedErrorCallback();
	if (typeof refreshReCaptchaLogin != 'undefined') {
		refreshReCaptchaLogin(spacialPagesRecaptchaID);
	}
}

function onPageLoginSAValidationError() {
	componentLoginUnexpectedErrorCallback();
	if (typeof refreshReCaptchaLogin != 'undefined') {
		refreshReCaptchaLogin(spacialPagesRecaptchaID);
	}
}

function componentLoginUserLockedError(){
	showLoginPassanteError(CQ.I18n.get("captcha.userLocked.error"));
}

function showLoginPassanteError(errorMessageI18nKey) {
	var $formFeedback = $('.millemiglia__formFeedback.j-formFeedback');
	$formFeedback.find('span.millemiglia__formFeedbackText').text(getI18nTranslate(errorMessageI18nKey, true));
	$formFeedback.addClass('isActive');
}

function hideLoginPassanteError() {
	var $formFeedback = $('.millemiglia__formFeedback.j-formFeedback');
	$formFeedback.find('span.millemiglia__formFeedbackText').text('');
	$formFeedback.removeClass('isActive');
}


/* internal methods */

function preparePageLoginSocial() {
	if (window['gigya']) {
		window['gigya'].socialize.showLoginUI({
			  version: 2
			, height: 30
			, width: '100%'
			, buttonsStyle: 'standard'
			, showTermsLink: false
			, hideGigyaLink: true
			, showWhatsThis: false
			, containerID: 'pageLoginDiv'
			, context: { 
					action: 'startSocialLogin',
					unexpectedErrorCallback: componentLoginUnexpectedErrorCallback,
					loginFailureCallback: componentLoginFailureCallback
				}
			, onLoad: function(event){
					$("#pageLoginDiv").find("center").contents().unwrap();	
					$('#pageLoginDiv').find('table').each(
					  function(index, elem) {
						if ($(elem).find('caption').length == 0) {
							$(elem).prepend(
								'<caption class="hidden">Social Account</caption>');
						}
					});
			}	
			, cid: ''
		});
	}
}


/* callbacks */

/**
 * Callback passed to the login functions to receive event about unexpected login error.
 */
function componentLoginUnexpectedErrorCallback() {
	showLoginPassanteError(CQ.I18n.get('header.socialLogin.error'));
}

/**
 * Callback passed to the login functions to receive event about unexpected login error.
 */
function componentLoginFailureCallback(errorCode, errorDescription) {
	showLoginPassanteError(CQ.I18n.get('header.socialLogin.error'));
}


/* Strong Auth - Begin */
function onPageLoginButtonSAClick(e) {
    e.preventDefault();
    hideLoginPassanteError();
    removeErrors();
    performValidation('millemiglialoginsavalidation', '#form-millemiglia-login',
        onPageLoginSAValidationSuccess, onPageLoginSAValidationError);
}

function checkCredenzialiMillemigliaHeaderSuccess(data){
    if (data) {
        if (!data.successful){
            setPassError(CQ.I18n.get('header.millemiglia.invalidCredential'));
        }
        else {
            dataResponse = data;
            $("#mmcode").val(parseInt(data.mmCode).toString());
            $("#pincode").val(data.mmPin);

            onPageLoginButtonClick();
            // successCheckMilleMigliaLogin(dataResponse, true, null);

        }
    }
}
function checkCredenzialiMillemigliaHeaderFailure(data){
	console.log('checkCredenzialiMillemigliaHeaderFailure');
}
function checkCredenzialiMillemigliaHeaderAlways(data){

}

/* Strong Auth - End */
/* execution code */

//add listener for document ready event to setup social events listeners
$(document).ready(onPageLoginDocumentReady);