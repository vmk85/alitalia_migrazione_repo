setI18nTranslate("Italian");

var defaultSelectedCountry = "";
var defaultSelectedProfessione = "";
var defaultSelectedTipoTelefono = "";
var defaultSelectedPrefissoNazionale = "";
var defaultSelectedLinguaCorrispondenza = "";
var defaultSelectedTipoPosto = "";
var defaultSelectedTipoPasto = "";
var defaultSelectedProvincia = "";
var defaultSelectedSecretQuestions = "";
var nation = "";
var bClickedMM;


var registrazione = true;
var adeguamento = false;
var modifica = false;
var checkPresenza = false;

var flagsVerifica = {

    flagVerificaUserName_OK : false,    //FlagVerificaUsername
    flagVerificaPassword_OK : false,  //FlagVerificaPassword
    flagVerificaPasswordConfirmed_OK : false,
    flagVerificaCellulare_OK : false, //PhoneCertificateFlag
    flagVerificaDomanda_OK : false,
    flagVerificaRisposta_OK : false,    //FlagVerificaRisposta
    flagVerificaEmailUser_OK : false, //EmailCertificateFlag
    flagVerifica_OTP_DS_OK : false,
    flagVerificaEmailUserNotDuplicate: false,
    flagVerificaUserNameNotDuplicate: false
};

//mock
var valoreConfrontoSicurezza = null ; //"ok";

// var fakeJson = {
//     "secureQuestions": [
//         {
//             "code": "CLS.MM.DS.CaneGatto",
//             "description": "Come si chiama il tuo cane o il tuo gatto?"
//         },
//         {
//             "code": "CLS.MM.DS.MadreNubile",
//             "description": "Qual è il cognome da nubile di tua madre?",
//         },
//         {
//             "code": "CLS.MM.DS.AnnoNascita",
//             "description": "Qual è l’anno della tua nascita?",
//         },
//         {
//             "code": "CLS.MM.DS.NumeroFortunato",
//             "description": "Qual è il tuo numero fortunato?",
//         },
//         {
//             "code": "CLS.MM.DS.SegnoZodiacale",
//             "description": "Qual è il tuo segno zodiacale?",
//         },
//         {
//             "code": "CLS.MM.DS.AttorePreferito",
//             "description": "Chi è il tuo attore/attrice preferito?",
//         }
//     ]
// } ;




$(document).ready(function() {
    bClickedMM = false;
    if($('[id="home"]').is(':checked')){
        $('#company').prop('disabled', true);
    }

    $('#home').click(function(){
        $('#company').prop('disabled', true);
    });

    $('#office').click(function(){
        $('#company').prop('disabled', false);
    });

    $('#registratiSubmit').bind('click', registratiHandler);
    $('#completaSubmit').bind('click', completaHandler);


    removeData();

    function registratiHandler(e) {

        console.log("registratiHandler(e) ")
        e.stopPropagation();
        e.preventDefault();

            validation(e, 'millemigliaregistratisubmit',
                '#form-millemiglia-registrati', registratiSuccess, registratiError);

            return false;
    }

    function checkPresenza(){

        var cell_ds=0;;;;;;

        if ($('#secureAnswer').val().trim() != ""){
            cell_ds++;
        }

        if ($('#input_newdelivery').val().trim() != ""){
            cell_ds++;
        }

        if(cell_ds>0){
            return true;
        }

        return false;
    }

    function checkPrefix(){

        if ($('#phonePrefix').val().trim() == "" && $('#input_newdelivery').val().trim() != ""){
                return false;
        } else {
                return true;
            }
        }

    function checkSecretQuestion(){

        if ($('#secretQuestion').val().trim() == "" && $('#secureAnswer').val().trim() != ""){
               return false;
        } else {
               return true;
            }
        }

    function checkVerificaPassword(){
        console.log("checkVerificaPassword ");

        var p = $('#input_password').val();
        var cp = $('#input_confpassword').val();
        if (p != "" && cp != "" && p == cp){
            flagsVerifica.flagVerificaPassword_OK = true;
            flagsVerifica.flagVerificaPasswordConfirmed_OK = true;
            return true;

        }
        return false;
    }

    function registratiSuccess(data) {
        console.log("registratiSuccess(data), data: ",data )
        //return (data.result ? registratiContinue(false) : showErrors(data));

        if(data.result){
            if(checkPresenza())
            {
            if(checkPrefix())
            {
            if(checkSecretQuestion())
            {
                if(checkVerificaPassword())
                {
                    checkVerificaUsernameDuplicate();
                }else{
                    ///presenta errore
                    var data = {
                        isSuccess: false,
                        fields:{
                            "input_confpassword": "Password e Conferma Password non corrispondono"
                        }
                    };
                    showErrors(data);
                }
            }else{
                ///presenta errore
                var data = {
                    isSuccess: false,
                    fields:{
                        "secureAnswer": CQ.I18n.get("millemiglia.insert.secureAnswer")
                    }
                };
                showErrors(data);
            }
            }else{
                ///presenta errore
                var data = {
                    isSuccess: false,
                    fields:{
                        "input_newdelivery": CQ.I18n.get("millemiglia.insert.prefix")
                    }
                };
                showErrors(data);
            }
            }else{
                ///presenta errore
                var data = {
                    isSuccess: false,
                    fields:{
                        "secureAnswer": CQ.I18n.get("specialpage.millemiglia.security.isMMOneOfRequiredGroup"),
                        "input_newdelivery": CQ.I18n.get("specialpage.millemiglia.security.isMMOneOfRequiredGroup")
/*
                        "phonePrefix": "Valorizzare almeno uno dei seguenti campi: Cellulare, Domanda di sicurezza"
*/
/*
                        "secretQuestion": "Valorizzare almeno uno dei seguenti campi: Cellulare, Domanda di sicurezza"
*/
                    }
                };
                showErrors(data);
                $('#phonePrefixInput').addClass('isError');
                $('#secretQuestionInput').addClass('isError');
            }


        }else{
            showErrors(data);
        }
    }
    function registratiError() {
        console.log("registratiError ");
        return registratiContinue(true);
    }

    function checkVerificaUsernameDuplicate(){
        console.log("checkVerificaUsername");
        var username = $('#input_username').val().trim();

        var usernameDaVerificare = {
            username : username
        };

        //performSubmit('millemigliacomunicazionisubmit-checkusernameduplicate',username,checkVerificaUsernameSuccess,checkVerificaUsernameFails);

        invokeGenericFormService('millemigliacomunicazionisubmit-checkusernameduplicate', 'POST', '',
            checkVerificaUsernameDuplicateSuccess, checkVerificaUsernameDuplicateFails, undefined, undefined, usernameDaVerificare);

        //checkVerificaUsernameSuccess();F
    }
    function checkVerificaUsernameDuplicateSuccess(data){
        //feedback --> ok
        if(!data.flagVerificaUserNameNotDuplicate){
            console.log("checkVerificaUsernameSuccess: username non duplicata");
            flagsVerifica.flagVerificaUserNameNotDuplicate = true;

            checkVerificaEmailUsernameDuplicate();
        }else{
            console.log("checkVerificaUsernameSuccess: username duplicata");
            flagsVerifica.flagVerificaUserNameNotDuplicate = false;

            var data = {
                isSuccess: false,
                fields:{
                    "input_username": CQ.I18n.get("millemiglia.username.error.invalid.alreadyused")   //"username gia in uso"
                }
            };

            showErrors(data);
        }


    }
    function checkVerificaUsernameDuplicateFails(){
        console.log("checkVerificaUsernameFails");
        //feedback --> ERROR --> username duplicato, o già esistente
    }

    function checkVerificaEmailUsernameDuplicate(){
        console.log("checkVerificaEmailUsernameDuplicate");
        var emailUser = $('#input_email').val().trim();

        var data = {
            input_email : emailUser
        };

        invokeGenericFormService('millemigliacomunicazionisubmit-checkemailusernameduplicate', 'POST', '',
            checkVerificaEmailUsernameDuplicateSuccess, checkVerificaEmailUsernameDuplicateFails, undefined, undefined, data);

        //checkVerificaUsernameSuccess();F
    }
    function checkVerificaEmailUsernameDuplicateSuccess(data){
        //feedback --> ok
        console.log("checkVerificaEmailUsernameDuplicateSuccess");
        if(!data.flagVerificaEmailUserNameNotDuplicate){
            flagsVerifica.flagVerificaEmailUserNotDuplicate = true;

            otpCheckValidations();
        }else{
            flagsVerifica.flagVerificaEmailUserNotDuplicate = false;

            var data = {
                isSuccess: false,
                fields:{
                    "input_email": CQ.I18n.get("millemiglia.email.error.invalid.alreadyused")   // "email gia in uso"
                }
            };

            showErrors(data);
        }

    }
    function checkVerificaEmailUsernameDuplicateFails(){
        console.log("checkVerificaEmailUsernameDuplicateFails");
        //feedback --> ERROR --> emailUsername duplicato, o già esistente
        flagsVerifica.flagVerificaEmailUserNotDuplicate = false;
    }


    function registratiContinue(stop) {
        removeErrors();

        console.log("qui in registratiContinue(stop)");

        if(checkPresenza()){

            //otpCheckValidations();

            if(stop)
            {
                console.log("//$('#registratiSubmit').click();");

                // $('#registratiSubmit').unbind('click', registratiHandler);
                // $('#registratiSubmit').click();

                return false;
            }
        }
    }





    var otpSms = {
        phone: "0"
    };

  /*  function padMobilePhonePrefix (str, max) {
        str = str.toString();
        return str.length < max ? padMobilePhonePrefix("0" + str, max) : str;
    }
*/

    function sendOTPsms(){
        if (!flagsVerifica.flagVerificaCellulare_OK){
            /////// phonePrefix          >>> secondo me dovrebbe chiamarsi mobilephoneprefix
            /////// input_newdelivery   >>> secondo me dovrebbe chiamarsi mobilephonenumber

           // var mobilephoneprefix =  padMobilePhonePrefix($("#phonePrefix").val(),4);
            var zeri = "00";
            var mobilephoneprefix = $("#phonePrefix").val();
            var mobilephonenumber = $("#input_newdelivery").val();


            var data = {
                phone : zeri + mobilephoneprefix + mobilephonenumber
            };

            $("#otpPopupTitle").html(CQ.I18n.get("specialpage.millemiglia.insertCodeSmsOtp"));
            $( "#input_insertCode_OTP" ).attr( "data-type", "SMS" );

            $('#input_insertCode_OTP').val("");

            invokeGenericFormService('millemigliacomunicazionisubmit-sendotpsms', 'POST', '',
                sendOTPsmsSuccess, sendOTPsmsFails, undefined, undefined, data);


            $('.reveal-overlay.mm-registrati').css('display','block');$('.mm-registrati .reveal').css('display','block');
        }
    }

    function sendOTPsmsSuccess(data){
        //feedback --> ok

        valoreConfrontoSicurezza=data.OTP;
        otpSMSExpiryDate=data.ExpiryDate;
        otpSMSSentDate=data.SentDate;

        console.log("sendOTPsmsSuccess");

        if(data.isError)
        {
            console.log("sendOTPsmsSuccess.internalError.Fake");
            //ultra fake ////
            valoreConfrontoSicurezza="123456"; //per ora... in prod togliere
            otpSMSSentDate=new Date();
            otpSMSExpiryDate="2099-12-31 10:00:00";
        }

    }
    function sendOTPsmsFails(data){

        //feedback --> ERROR --> impossobile inviare SMS

        //ultra fake ////
        valoreConfrontoSicurezza="123456"; //per ora... in prod togliere
        otpSMSSentDate=Date.Now();
        otpSMSExpiryDate="2099-12-31 10:00:00";

        console.log("sendOTPsmsFails");

    }



    function sendOTPSecureAnswer(){
        var email=$("#input_email").val();
        var data = {
            email : email
        };


        $("#otpPopupTitle").html(CQ.I18n.get("specialpage.millemiglia.insertCodeEmailOtp"));
        $( "#input_insertCode_OTP" ).attr( "data-type", "SecureAnswer" );

        $('#input_insertCode_OTP').val("");

        //TODO invocare servelet per recuperare OTP via EMAIL per secure ancwer
        //performSubmit(sendCodeOtpviaEmail,ecc);
        invokeGenericFormService('millemigliacomunicazionisubmit-sendotpemail', 'POST', '',
            sendOTPEmailSuccess, sendOTPEmailFails, sendOTPEmailAlways, undefined, data);


        $('.reveal-overlay.mm-registrati').css('display','block');
        $('.mm-registrati .reveal').css('display','block');
    }

    function sendOTPEmail(){

        var email=$("#input_email").val();
        var data = {
            email : email
        };

        $("#otpPopupTitle").html(CQ.I18n.get("specialpage.millemiglia.insertCodeEmailOtp"));
        $( "#input_insertCode_OTP" ).attr( "data-type", "Email" );

        $('#input_insertCode_OTP').val("");

        //TODO invocare servelet per recuperare OTP via EMAIL
        //performSubmit(sendCodeSmsOtpviaEmail,ecc);
        invokeGenericFormService('millemigliacomunicazionisubmit-sendotpemail', 'POST', '',
            sendOTPEmailSuccess, sendOTPEmailFails, sendOTPEmailAlways, undefined, data);

        $('.reveal-overlay.mm-registrati').css('display','block');$('.mm-registrati .reveal').css('display','block');
    }
    function sendOTPEmailSuccess(data){
        //feedback --> ok

        valoreConfrontoSicurezza=data.OTP;
        otpSMSExpiryDate=data.ExpiryDate;
        otpSMSSentDate=data.SentDate;

        console.log("sendOTPEmailSuccess");

        if(data.isError)
        {
            console.log("sendOTPEmailSuccess.internalError.Fake");
            //ultra fake ////
            valoreConfrontoSicurezza="123456"; //per ora... in prod togliere
            otpSMSSentDate=new Date();
            otpSMSExpiryDate="2099-12-31 10:00:00";
        }

    }
    function sendOTPEmailFails(data){

        //feedback --> ERROR --> impossobile inviare EMAIL

        //ultra fake ////
        valoreConfrontoSicurezza="123456"; //per ora... in prod togliere

        console.log("sendOTPEmailFails");

    }
    function sendOTPEmailAlways(data) {
    }


    String.prototype.replaceAt=function(index, replacement) {
        return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
    };


    function otpCheckValidations(){
        console.log("otpCheckValidations()");

        if($("#input_newdelivery").val().trim()!= '' && flagsVerifica.flagVerificaCellulare_OK == false){
            var number = $("#input_newdelivery").val().trim();
            for(var i = 2; i < (number.length - 3);i++)
            {
                number = number.replaceAt(i,"*");
            }
            $('#phoneEmailNumberHidden').text(number);
            sendOTPsms();
        }
        else if($("#input_email").val().trim()!= '' && flagsVerifica.flagVerificaEmailUser_OK == false)
        {
            var email = $("#input_email").val().trim();
            for(var i = 2; i < (email.length - 3);i++)
            {
                email = email.replaceAt(i,"*");
            }
            $('#phoneEmailNumberHidden').text(email);
            sendOTPEmail();
        }
        else if ( $('#secureAnswer').val().trim() && flagsVerifica.FlagVerificaRisposta_OK == false){
            sendOTPSecureAnswer();
        }

        $('#flagVerificaUserName_OK').val(flagsVerifica.flagVerificaUserName_OK);
        $('#flagVerificaPassword_OK').val(flagsVerifica.flagVerificaPassword_OK);
        $('#flagVerificaCellulare_OK').val(flagsVerifica.flagVerificaCellulare_OK);
        $('#flagVerificaRisposta_OK').val(flagsVerifica.flagVerificaRisposta_OK);
        $('#flagVerificaEmailUser_OK').val(flagsVerifica.flagVerificaEmailUser_OK);


        return false;

    }

    function sendOTP(){
        console.log("sendOTP function ...");

        if (flagsVerifica.flagVerificaEmailUser_OK){
            if (checkPresenza){
                if (flagsVerifica.flagVerificaCellulare_OK){
                    if (flagsVerifica.flagVerificaDomanda_OK){
                    }else{
                        if ($('#secretQuestion').val().trim() != ""){
                            sendOTPSecureAnswer();
                        }
                    }
                }else{
                    if ($('#input_newdelivery').val().trim() != ""){
                        sendOTPsms();
                    }

                }
            }
        }else{
            if (checkPresenza){
                if (flagsVerifica.flagVerificaCellulare_OK){
                    if (flagsVerifica.flagVerificaDomanda_OK){
                    }else{
                        if ($('#secretQuestion').val().trim() != ""){
                            sendOTPSecureAnswer();
                        }
                    }
                }else{
                    if ($('#input_newdelivery').val().trim() != ""){
                        sendOTPsms();
                    }else{
                        sendOTPEmail();

                    }

                }
                //sendOTPSecureAnswer();
                //sendOTPsms();



            }
        }
    }


    function removeData() {
        var passo = $('#passo').val();
        if (passo == 3) {
            performSubmit('millemigliaremovedatasubmit');
        }
        return false;
    }

    $('.mm-registrati .reveal .icon.icon--close').on('click',function(){$('.reveal-overlay.mm-registrati').css('display','none');$('.mm-registrati .reveal').css('display','none');})


    //bottone che utente clicca per verifica gli OTP
    $('.insertCodeSMS_OTP').on('click',function(){

        //verifico gli OTP

        var inputValue = $('#input_insertCode_OTP').val();

        var dataType = $( "#input_insertCode_OTP" ).attr( "data-type"); //discriminante

        if (dataType == "SMS")
        {
            //verificare ExpiryDate !!!!!
        	 var isExpired=isOtpExpired(otpSMSExpiryDate);
             var isNotExpired=!isExpired;

            if (inputValue == valoreConfrontoSicurezza && isNotExpired){
                flagsVerifica.flagVerificaCellulare_OK = true;
                flagsVerifica.flagVerifica_OTP_DS_OK= true;
            }else{
                flagsVerifica.flagVerificaCellulare_OK = false;
            }
        }else if(dataType == "Email"){
            //verificare ExpiryDate !!!!!
            if (inputValue == valoreConfrontoSicurezza){
                flagsVerifica.flagVerificaEmailUser_OK = true;
                flagsVerifica.flagVerifica_OTP_DS_OK= true;
            }else{
                flagsVerifica.flagVerificaEmailUser_OK = false;
            }
        }else if(dataType == "SecureAnswer"){
            if (inputValue == valoreConfrontoSicurezza){
                flagsVerifica.flagVerificaDomanda_OK = true;
                flagsVerifica.flagVerificaRisposta_OK = true;
                flagsVerifica.flagVerifica_OTP_DS_OK= true;
            }else{
                flagsVerifica.flagVerificaDomanda_OK = false;
                flagsVerifica.flagVerificaRisposta_OK = false;
            }

        }

        $('.reveal-overlay.mm-registrati').css('display','none');
        $('.mm-registrati .reveal').css('display','none');
        /*fix show error otp*/
        if((!flagsVerifica.flagVerificaCellulare_OK && dataType == "SMS") ||
          (!flagsVerifica.flagVerificaEmailUser_OK && dataType == "Email") ||
            (!flagsVerifica.flagVerificaRisposta_OK && dataType == "SecureAnswer")) {
		        var errMsg=CQ.I18n.get("millemiglia.otp.error.invalid");
		        var data = {
		                isSuccess: false,
		                fields:{
		                    "input_insertCode_OTP": errMsg
		                }
		            };
		        showErrors(data);
		        $('#input_insertCode_OTP_error').css('position','relative');
		        $('.isError .form__errorIcon').css('position','fixed');
        } else {
            $('.codiceSicurezza').removeClass('isError');
        }
        
        
        console.log("FLAGS: ",flagsVerifica);

        if(flagsVerifica.flagVerifica_OTP_DS_OK) {
            flagsVerifica.flagVerificaUserName_OK=true;
            flagsVerifica.flagVerificaPassword_OK=true;
            flagsVerifica.FlagVerificaRisposta_OK=true;
        }

        if(
            flagsVerifica.flagVerificaUserName_OK &&
            flagsVerifica.flagVerificaPassword_OK &&
            flagsVerifica.flagVerificaEmailUser_OK &&
            (
                flagsVerifica.FlagVerificaRisposta_OK
                ||
                $('#secureAnswer').val().trim() === ""
            )
            &&
            (
                flagsVerifica.flagVerificaCellulare_OK
                ||
                $('#input_newdelivery').val().trim() === ""
            )

        )
        {

            $('#registratiSubmit').unbind('click', registratiHandler);
            $('#registratiSubmit').click();
        }else{
            //se la persona >>> NON <<< ha risposto correttamente a OTP SMS/ OTP Email/ DomandaSegreta

            otpCheckValidations()

            console.log("FLAGS: ",flagsVerifica);
        }


        return false;
    });




    $('#registratiSubmit').on('click',function(e){
        //e.preventDefault();e.stopPropagation();
        //telephone check verify
        /* if ($('#input_newdelivery').val().trim() != ""){
             $("#otpPopupTitle").html(CQ.I18n.get("specialpage.millemiglia.insertCodeSmsOtp"));

             //performSubmit(sendCodeSmsOtpviaSMS,ecc);
             $('.reveal-overlay').css('display','block');
         }else{
             if ($('#secretQuestion').val().trim() != ""){
             //performSubmit(sendCodeSmsOtpviaEmail,ecc);
                 $("#otpPopupTitle").html(CQ.I18n.get("specialpage.millemiglia.insertCodeEmailOtp"));
                 $('.reveal-overlay').css('display','block');
             }

         }*/
    });

});

$('div.customSelect').find('select#nation').on('change', function(e) {
    defaultSelectedCountry = $('div.customSelect').find('select#nation')
        .find(':selected').val();

    var requestData = {
        'states': defaultSelectedCountry
    };

    getDropdownData(requestData, dropdownDataSuccess, dropdownDataFail);

    function dropdownDataSuccess(data) {
        $('div.customSelect').find('select#province').empty();
        var states = data.states;
        if (states.length > 0) {
            $("#province__selector").show();
            for (index in states) {
                var option = '';
                if (states[index].code == defaultSelectedProvincia) {
                    option = '<option value="' + states[index].code + '" selected>' + states[index].name + '</option>';
                } else {
                    option = '<option value="' + states[index].code + '">' + states[index].name + '</option>';
                }
                $('div.customSelect').find('select#province').append(option);
            }
        } else {
            $("#province__selector").hide();
        }
        return false;
    }

    function dropdownDataFail() {
        return false;
    }

});

function completaHandler(e) {
    e.preventDefault();
    if (bClickedMM === false){
        bClickedMM = true;
        validation(e, 'millemigliacomunicazionisubmit',
            '#form-millemiglia-comunicazioni', completaSuccess, completaError);
        return false;
    } else {
        return false;
    }
}

function completaSuccess(data) {
    if(data.result){
        completaContinue(false);
    }else{
        console.log("completaSuccess(data) data: ", data);
        showErrors(data);
        bClickedMM = false;
    }
}

function completaError() {
    bClickedMM = false;
    return completaContinue(true);
}

function completaContinue(stop) {
    removeErrors();
    $('#completaSubmit').unbind('click', completaHandler);
    return stop || $('#form-millemiglia-comunicazioni').submit();
}

function retrieveDataPasso1() {
    var requestData = {
        'countries': defaultSelectedCountry,
        'professioni': defaultSelectedProfessione,
        'tipiTelefono': defaultSelectedTipoTelefono,
        'prefissiNazionali': defaultSelectedPrefissoNazionale,
        'secureQuestions': defaultSelectedSecretQuestions
    };
    console.log("requestData: ",requestData);
    getDropdownData(requestData, dropdownDataSuccess1, dropdownDataFail);
    return false;
}

function retrieveDataPasso2() {
    console.log("retrievePasso2, $('#nation').val(): ",$('#nation').val());
    window.nation = $('#nation').val();

    var requestData = {
        'linguaCorrispondenza' : defaultSelectedLinguaCorrispondenza,
        'tipoPosto' : defaultSelectedTipoPosto,
        'tipoPasto' : defaultSelectedTipoPasto
    };
    if (window.nation == "IT") {
        requestData["states"] = window.nation;
    }
    getDropdownData(requestData, dropdownDataSuccess2, dropdownDataFail);
    return false;
}

function dropdownDataSuccess1(data) {
    var secretQ = data.secureQuestions;
    //var secretQ = fakeJson.secureQuestions;
    for (index in secretQ ) {

        option = '<option value="' + secretQ[index].id + '">' + secretQ[index].description + '</option>';

        $('div.customSelect').find('select#secretQuestion').append(option);
    }

    var countries = data.countries;
    for (index in countries) {

        var option = '';
        if (countries[index].code == defaultSelectedCountry) {
            option = '<option value="' + countries[index].code + '" selected>' + countries[index].description + '</option>';
        } else {
            option = '<option value="' + countries[index].code + '">' + countries[index].description + '</option>';
        }
        $('div.customSelect').find('select#nation').append(option);
    }

    var states = data.states;
    for (index in states) {
        var option = '';
        if (states[index].code == defaultSelectedProvincia) {
            option = '<option value="' + states[index].code + '" selected>' + states[index].name + '</option>';
        } else {
            option = '<option value="' + states[index].code + '">' + states[index].name + '</option>';
        }
        $('div.customSelect').find('select#provincia').append(option);
    }

    var professioni = data.professioni;
    for (index in professioni) {
        var option = '';
        if (professioni[index].name == defaultSelectedProfessione) {
            option = '<option value="' + professioni[index].name + '" selected>' + professioni[index].value + '</option>';
        } else {
            option = '<option value="' + professioni[index].name + '">' + professioni[index].value + '</option>';
        }
        $('div.customSelect').find('select#profession').append(option);
    }

    var tipiTelefono = data.tipiTelefono;
    for (index in tipiTelefono) {
        var option = '';
        if (tipiTelefono[index].name == defaultSelectedTipoTelefono) {
            option = '<option value="' + tipiTelefono[index].name + '" selected>' + tipiTelefono[index].value + '</option>';
        } else {
            option = '<option value="' + tipiTelefono[index].name + '">' + tipiTelefono[index].value + '</option>';
        }
        $('div.customSelect').find('select#numberType').append(option);
        $('div.customSelect').find('select#typedelivery').append(option);
    }

    var prefissiNazionali = data.prefissiNazionali;
    for (index in prefissiNazionali) {
        var option = '';
        if (prefissiNazionali[index].prefix == defaultSelectedPrefissoNazionale) {
            option = '<option value="' + prefissiNazionali[index].prefix + '" selected>' + prefissiNazionali[index].description + ' (+' + prefissiNazionali[index].prefix + ') </option>';
        } else {
            option = '<option value="' + prefissiNazionali[index].prefix + '">' + prefissiNazionali[index].description + ' (+' + prefissiNazionali[index].prefix + ') </option>';
        }
        $('div.customSelect').find('select#nationalPrefix').append(option);
        $('div.customSelect').find('select#phonePrefix').append(option);

    }

    return false;
}

function dropdownDataSuccess2(data) {
    var passo = $('#passo').val();
    if (passo == 2) {
        var lingueCorrispondenza = data.linguaCorrispondenza;
        for (index in lingueCorrispondenza) {
            var option = '';
            /*if (window.nation == "IT") {
                var translatedLanguage = getI18nTranslate("Italian", true);
                if (lingueCorrispondenza[index].value.toUpperCase() == translatedLanguage.toUpperCase()) {
                    option = '<option value="' + lingueCorrispondenza[index].name
                        + '" selected>' + lingueCorrispondenza[index].value + '</option>';
                    $('div.customSelect').find('select#language').append(option);
                }
            }
            else {
                if (lingueCorrispondenza[index].prefix == defaultSelectedLinguaCorrispondenza) {
                    option = '<option value="' + lingueCorrispondenza[index].name
                        + '" selected>' + lingueCorrispondenza[index].value + '</option>';
                } else {
                    option = '<option value="' + lingueCorrispondenza[index].name
                        + '">' + lingueCorrispondenza[index].value + '</option>';
                }
                $('div.customSelect').find('select#language').append(option);
            }*/

            if (lingueCorrispondenza[index].prefix == defaultSelectedLinguaCorrispondenza) {
                option = '<option value="' + lingueCorrispondenza[index].name
                    + '" selected>' + lingueCorrispondenza[index].value + '</option>';
            } else {
                option = '<option value="' + lingueCorrispondenza[index].name
                    + '">' + lingueCorrispondenza[index].value + '</option>';
            }
            $('div.customSelect').find('select#language').append(option);

        }
    }

    var tipiPosto = data.tipoPosto;
    for (index in tipiPosto) {
        var option = '';
        if (tipiPosto[index].prefix == defaultSelectedTipoPosto) {
            option = '<option value="' + tipiPosto[index].code + '" selected>'
                + tipiPosto[index].description + '</option>';
        } else {
            option = '<option value="' + tipiPosto[index].code + '">'
                + tipiPosto[index].description + '</option>';
        }
        $('div.customSelect').find('select#posto').append(option);
    }

    var tipiPasto = data.tipoPasto;
    for (index in tipiPasto) {
        var option = '';
        if (tipiPasto[index].prefix == defaultSelectedTipoPasto) {
            option = '<option value="' + tipiPasto[index].code + '" selected>' + tipiPasto[index].description + '</option>';
        } else {
            option = '<option value="' + tipiPasto[index].code + '">' + tipiPasto[index].description + '</option>';
        }
        $('div.customSelect').find('select#pasto').append(option);
    }
    return false;
}


function dropdownDataFail() {
    return false;
}

//### SA - funzioni per validazione con attributi data-validation #############
function isNotEmailUsernameDuplicate(value){
    console.log("isEmailUsernameDuplicate: " + value);

    var isValidationOK=false;

    var data = {
        input_email : value
    };

    //invoco servelet per verifica univocita Email
    invokeGenericFormService_Sync(
        'millemigliacomunicazionisubmit-checkemailusernameduplicate',
        'POST',
        '',
        function(event) {
            //success
            if(!event.flagVerificaEmailUserNameNotDuplicate){
                console.log("checkemailusernameduplicate: email non duplicata");
            }else {
                console.log("checkemailusernameduplicate: email duplicata");
            }
            isValidationOK =  !event.flagVerificaEmailUserNameNotDuplicate;
        },
        function(event) {
            //fail
            //per definzione se non riesco a testare il valore del campo non va bene
            isValidationOK = false;
        },
        undefined,
        undefined,
        data);

    return isValidationOK;
}

function isNotUsernameDuplicate(value){
    console.log("isNotUsernameDuplicate: " + value);

    var isValidationOK=false;

    var data = {
        username : value,
        profileId : null
    };

    //invoco servelet per verifica univocita Username
    invokeGenericFormService_Sync(
        'millemigliacomunicazionisubmit-checkusernameduplicate',
        'POST',
        '',
        function(event) {
            //success

            if(!event.flagVerificaUserNameNotDuplicate){
                console.log("checkusernameduplicate: username non duplicata");
                isValidationOK =  true;
            }else {
                console.log("checkusernameduplicate: username duplicata");
                isValidationOK =  false;
            }

        },
        function(event) {
            //fail
            //per definzione se non riesco a testare il valore del campo non va bene
            isValidationOK = false;
        },
        undefined,
        undefined,
        data);

    return isValidationOK;
}

function isVerificaPasswordOK(){
    console.log("isVerificaPasswordOK");

    var p = $('#input_password').val();
    var cp = $('#input_confpassword').val();
    if (p == cp){
        console.log("isVerificaPasswordOK = true");
        return true;
    }
    console.log("isVerificaPasswordOK = false");
    return false;
}

function isSecurityFieldsOK(){

    var cell_ds=0;;;;;;

    if ($('#secretQuestion').val().trim() != "" && $('#secureAnswer').val().trim() != ""){
        cell_ds++;
    }

    if ($('#input_newdelivery').val().trim() != ""){
        cell_ds++;
    }

    if ($('#phonePrefix').val().trim() != ""){
        cell_ds++;
    }

    if(cell_ds>0){
        return true;
    }

    return false;
}

function invokeGenericFormService_Sync(service, method, form, done, fail, always,
                                  selector, additionalParams) {
    var actualData;
    if ($.type(form) === "string" || !form) {
        var serializedData = $(form).serialize();
        if (serializedData != "") {
            serializedData += "&";
        }
        serializedData += "_isAjax=true";
        if (additionalParams) {
            for ( var paramName in additionalParams) {
                serializedData += "&" + paramName + "="
                    + encodeURIComponent(additionalParams[paramName]);
            }
        }
        actualData = serializedData;
    } else if ($.isPlainObject(form)) {
        actualData = {};
        if (form) {
            for (var fieldName in form) {
                actualData[fieldName] = form[fieldName];
            }
        }
        actualData['_isAjax'] = true;
        if (additionalParams) {
            for (var paramName in additionalParams) {
                actualData[paramName] = additionalParams[paramName];
            }
        }
    }
    return $.ajax({
        url : getServiceUrl(service, selector),
        method : method,
        data : actualData,
        async: false,
        context : document.body
    }).done(function(data) {
        if (done) {
            done(data);
        }
    }).fail(function() {
        if (fail) {
            fail();
        }
    }).always(function() {
        if (always) {
            always();
        }
    });
}

function  isOtpExpired(otpExpiryDate){
    var isExpired=true;
    try {
        var dateOtpExpiry=new Date(Date.UTC(otpExpiryDate.substr(0,4), otpExpiryDate.substr(5,2)-1, otpExpiryDate.substr(8,2), otpExpiryDate.substr(11,2), otpExpiryDate.substr(14,2), otpExpiryDate.substr(17,2)));
        var dateNow=new Date();

        isExpired=dateOtpExpiry<dateNow;

    }catch (e) {
        console.log(e);
        isExpired=true;
    };

    console.log('dateNow: ' + dateNow)
    console.log('dateOtpExpiry: ' + dateOtpExpiry)
    console.log('OtpExpired: ' + isExpired);

    return isExpired;
}

//################