$(function() {
    ($(".booking").length > 0 || $(".bookingAward").length > 0 || $('.manageBooking').length > 0) 
    		&& (book.init(), $(window).on("resize", function() {
        var a;
        clearTimeout(a), a = setTimeout(function() {
            book.updateTopPosition();
        }, 50);
    }));
}), window.bookingDisableFixed = !1, window.bookingZoomDisableFixed = !1, window.bookingHeaderStop = !1;

var book = {
    infoBox: null,
    infoBoxTop: 0,
    init: function() {
        if (this.infoBox = $(".j-bookInfoBox"), this.departureHeader = $(".j-bookingTable__header--departure"), 
        this.returnHeader = $(".j-bookingTable__header--return"), this.recapDeparture = $(".j-bookingRecapDep"), 
        this.supportTouch = "ontouchstart" in document.documentElement, this.updateTopPosition(), 
        $(".booking__dateListCover").each(function(a, b) {
            var c = 2, d = this, e = $(this).find(".booking__dateListItem.isActive").index(), f = Math.max(0, e - c);
            window.breakpointS > window.windowW ? f = 6 : window.breakpointM > window.windowW && (f = 5);
            $(this).iosSlider({
                desktopClickDrag: !0,
                snapToChildren: !0,
                autoSlide: !1,
                infiniteSlider: !1,
                startAtSlide: f
            });
            $(this).siblings(".booking__dateArrow.next").click(function() {
                var a = $(d).data("args").currentSlideNumber;
                $(d).iosSlider("goToSlide", a + 3);
            }), $(this).siblings(".booking__dateArrow.prev").click(function() {
                var a = $(d).data("args").currentSlideNumber;
                $(d).iosSlider("goToSlide", Math.max(a - 3, 1));
            });
        }), $(".j-radioButtonCard").change(function() {
            $(".bookingPaymentForm__itemCard").removeClass("activeTab"), $(".j-radioButtonCardImage").addClass("card--opacity"), 
            $(this).siblings(".j-radioButtonCardImage").removeClass("card--opacity"), $(this).closest(".bookingPaymentForm__itemCard").addClass("activeTab"), 
            $(".j-paymentFieldset").removeClass("isActive"), $("#" + $(this).attr("data-target")).toggleClass("isActive");
        }), $(".j-selectPaymentType").change(function() {
            $(".j-paymentFieldset").removeClass("isActive"), $("#" + $(this).find(":selected").attr("data-target")).toggleClass("isActive");
        }), $(".j-wantInvoice").change(function() {
            $(this).is(":checked") ? $(".j-wantInvoiceContainer").show() : $(".j-wantInvoiceContainer").hide();
        }), book.supportTouch && ($("select").on("focus", function(a) {
            book.checkScale();
        }).on("blur", function(a) {
            book.checkScale();
        }), $(document).on("gestureend", function(a) {
            var b = a.originalEvent.scale;
            book.checkScale(b);
        }), $(".j-wrapperFixed").append($('<div class="j-fixedClone"></div>'))), $(".j-accordions").on("accordion.opened", function(a) {
            book.resetFixedElement(), book.updateTopPosition(), $(window).triggerHandler("scroll");
        }).on("accordion.closed", function(a) {
            book.resetFixedElement(), book.updateTopPosition(), $(window).triggerHandler("scroll");
        }), $(".j-priceSelectorCol").fastClick(book.priceSelectorCol), $(".j-priceSelector").fastClick(book.priceSelector), 
        $(".j-businessTrigger").fastClick(book.businessTrigger), $(".j-flightDetails").fastClick(book.getFlightDetails), 
        $(".j-mobile-openOptions").fastClick(book.openOptionMobile), $(".j-closeTableRow").fastClick(book.priceSelectorReset), 
        $(".j-goToReturn").fastClick(book.goToReturn), $(".j-selectReturn").fastClick(book.selectedReturn), 
        $(".j-toggleBasketAccordion").fastClick(function(a) {
            a.preventDefault(), book.openBasket($(this));
        }), $(".j-toggleBasketAccordion").hasClass("opened")) {
            var a = $(".j-toggleBasketAccordion").attr("href");
            $(a).show(), window.bookingHeaderStop = !0;
        }
        book.supportTouch && $(window).on('load', function() {
            book.checkScale();
        }), book.stickyHeaderBooking();
    },
    checkScale: function(a) {
        clearTimeout(window.checkScaleID), window.checkScaleID = setTimeout(function() {
            var b = void 0 != a ? a : book.zoomValue();
            b && (b > 1 ? (book.resetFixedElement(), window.bookingZoomDisableFixed = !0) : (window.bookingZoomDisableFixed = !1, 
            $(window).triggerHandler("scroll")));
        }, 250);
    },
    zoomValue: function() {
        return document.documentElement.clientWidth / window.innerWidth;
    },
    updateInfoBoxPosition: function() {
        this.infoBox && this.infoBox.length > 0 && (this.infoBoxTop = this.infoBox.closest(".j-wrapperFixed").offset().top);
    },
    updateDepartureHeaderPosition: function() {
        this.departureHeader && this.departureHeader.length > 0 && (this.departureHeaderTop = this.departureHeader.closest(".j-wrapperFixed").offset().top);
    },
    updateReturnHeaderPosition: function() {
        this.returnHeader && this.returnHeader.length > 0 && (this.returnHeaderTop = this.returnHeader.closest(".j-wrapperFixed").offset().top);
    },
    updateRecapDeparturePosition: function() {
        $(".j-bookingRecapDep").length > 0 && (this.bookingRecapDepTop = $(".j-bookingRecapDep").closest(".j-wrapperFixed").offset().top);
    },
    updateTopPosition: function() {
        this.updateInfoBoxPosition(), this.updateReturnHeaderPosition(), this.updateDepartureHeaderPosition(), 
        this.updateRecapDeparturePosition();
    },
    openBasket: function(a) {
        var b = a.attr("href");
        return $(b).is(":visible") ? (book.infoBox.removeClass("opened"), window.bookingHeaderStop = !1, 
        $(b).velocity("slideUp", {
            duration: 300,
            complete: function() {
                book.resetFixedElement(), book.updateTopPosition();
            }
        })) : (book.infoBox.addClass("opened"), window.bookingHeaderStop = !0, $(b).velocity("slideDown", {
            delay: 100,
            duration: 300,
            complete: function() {
                book.resetFixedElement(), book.updateTopPosition(), $(window).triggerHandler("scroll"), 
                $(window).trigger("resize.accordion");
            }
        })), $("html").velocity("scroll", {
            duration: 300,
            delay: 100,
            offset: book.infoBoxTop,
            complete: function() {}
        }), !1;
    },
    resetFixedElement: function() {
        $(".j-wrapperFixed").each(function() {
            book.unfixElement($(this).find(".isFixed"));
        });
    },
    fixElement: function(a) {
        var b = a.outerHeight(!0);
        return a.hasClass("isFixed") ? b : (a.addClass("isFixed"), a.closest(".j-wrapperFixed").css({
            height: b
        }), book.supportTouch && a.siblings(".j-fixedClone").css({
            visibility: "visible",
            "z-index": a.css("z-index")
        }).append(a), b);
    },
    unfixElement: function(a) {
        a.hasClass("isFixed") && (a.removeClass("isFixed").css({
            top: ""
        }).closest(".j-wrapperFixed").attr("style", ""), book.supportTouch && (a.closest(".j-wrapperFixed").append(a), 
        a.siblings(".j-fixedClone").empty().css({
            visibility: "hidden"
        })));
    },
    stickyHeaderBooking: function() {
        var a, b, c, d = $(window), e = 0, f = this, c = 0, g = window.breakpointS > window.windowW, b = book.infoBox.find(".bookInfoBoxItinerary").outerHeight() + 11, h = book.supportTouch ? "scroll touchmove touchstart touchend" : "scroll";
        d.bind(h, function(h) {
            if (!(window.bookingDisableFixed || window.bookingHeaderStop || window.bookingZoomDisableFixed)) {
                c = g ? book.infoBoxTop + b : book.infoBoxTop;
                var i = 0;
                if (e = d.scrollTop(), e >= c ? (i += f.fixElement(f.infoBox), g && f.infoBox.css({
                    top: -b
                })) : f.unfixElement(f.infoBox), !g) {
                    if (f.recapDeparture.is(":visible")) if (e >= book.bookingRecapDepTop - i) {
                        var j = f.fixElement(f.recapDeparture);
                        f.recapDeparture.css({
                            top: i
                        }), i += j;
                    } else f.unfixElement(f.recapDeparture);
                    if (f.departureHeader.length && f.departureHeader.is(":visible")) if (e >= f.departureHeaderTop - i) {
                        var k = f.departureHeader.outerHeight(), l = i, m = $(".j-bookingTableDeparture"), n = i + k, o = m.offset().top + m.outerHeight(), p = e - o + n;
                        p > 0 && (l -= p), f.fixElement(f.departureHeader), f.departureHeader.css({
                            top: l
                        });
                    } else f.unfixElement(f.departureHeader);
                    if (f.returnHeader.length && f.returnHeader.is(":visible")) if (e >= book.returnHeaderTop - i) {
                        var q = f.returnHeader.outerHeight(), l = i, m = $(".j-bookingTableReturn"), n = i + q, o = m.offset().top + m.outerHeight(), p = e - o + n;
                        p > 0 && (l -= p), f.fixElement(f.returnHeader), f.returnHeader.css({
                            top: l
                        });
                    } else f.unfixElement(f.returnHeader);
                    a = e;
                }
            }
        }), $(window).triggerHandler("scroll");
    },
    destroyToolTip: function() {
        $(".j-overlayLinkTooltip").each(function() {
            clearInterval($(this).qtip("hide").data("showID"));
        });
    },
    priceSelectorCol: function() {
        var a = $(this).parent().find(".j-priceSelectorCol").index($(this));
        $(this).closest(".j-bookTableRowBody").prev().find(".j-priceSelector").eq(a).trigger("click");
    },
    priceSelector: function(a) {
        a.preventDefault();
        var b = $(this).parent();
        if (window.windowW > window.breakpointS) {
            if (b.hasClass("isActive")) return;
            row = b.closest(".j-bookTableRow"), body = row.find(".j-bookTableRowBody"), className = b.data("type");
            {
                var c = (b.closest(".j-bookingTable").find(".bookingTable__header").data("num"), 
                b.siblings().hasClass("isActive") ? !0 : !1), d = b.closest(".j-bookingTable");
                b.closest(".bookingTable__header");
            }
            d.find(".bookingTable__col").removeClass("isActive"), body.find("." + className).addClass("isActive");
            var e = ($(".j-bookingTable").find(".j-bookTableRow.activeRow"), d.hasClass("j-bookingTableReturn"), 
            d.find("." + className + " .j-overlayLinkTooltip")), f = d.find(".activeRow"), g = 0;
            1 == f.length && row.index() > f.index() && (g = f.find(".j-bookTableRowBody").outerHeight()), 
            d.find(".j-bookTableRowBody:visible").length > 0 && book.priceSelectorReset(c, d);
            var h = $(".bookingTable__header").find("." + className);
            if (h.addClass("headingActive"), book.destroyToolTip(), e.qtip("show"), e.data("showID", setTimeout(function() {
                e.qtip("hide");
            }, 2500)), row.addClass("activeRow"), b.addClass("isActive"), !c) {
                if (body.velocity("slideDown", {
                    duration: 400,
                    complete: function() {
                        book.updateTopPosition();
                    }
                }), bookingHeaderStop) var i = row.offset().top - g; else {
                    var i = row.offset().top - g;
                    i -= book.infoBox.outerHeight(), book.departureHeader.is(":visible") ? i -= book.departureHeader.outerHeight() : book.returnHeader.is(":visible") && (i -= book.returnHeader.outerHeight()), 
                    book.recapDeparture.is(":visible") && (i -= book.recapDeparture.outerHeight());
                }
                $("html, body").animate({
                    scrollTop: i + "px"
                }, {
                    duration: 400,
                    complete: function() {
                        book.updateTopPosition();
                    }
                });
            }
        } else b.closest(".j-bookingTable").hasClass("j-bookingTableDeparture") ? $(".j-bookingTableDeparture .j-bookingLoader").velocity("transition.slideDownIn", {
            duration: 250,
            complete: function() {
                $(".j-bookingTableDeparture .j-bookTableRow.mobileRowActive").velocity("slideUp", {
                    duration: 400,
                    complete: function() {
                        $(".j-bookingRecapDep").velocity("transition.slideDownIn", {
                            duration: 250,
                            complete: function() {
                                $(window).trigger("resize.accordion"), $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn"), 
                                $(".j-bookingTableDeparture").velocity("slideUp", {
                                    complete: function() {
                                        $(".j-bookingTableDeparture, .j-titleDeparture, .booking__date").velocity("transition.slideDownOut", {
                                            duration: 250,
                                            complete: function() {
                                                var a = $(".j-bookingRecapDep").offset().top - $(".j-bookInfoBox").outerHeight();
                                                $("html").velocity("scroll", {
                                                    duration: 400,
                                                    offset: a,
                                                    complete: function() {
                                                        book.updateTopPosition();
                                                    }
                                                }), setTimeout(function() {
                                                    $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
                                                }, 2e3);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }) : $(".j-bookingTableReturn .j-bookingLoader").velocity("transition.slideDownIn", {
            duration: 250,
            complete: function() {
                $(".j-bookingTableReturn .j-bookTableRow.mobileRowActive").velocity("slideUp", {
                    duration: 400,
                    complete: function() {
                        $(".j-bookingRecapRet").velocity("transition.slideDownIn", {
                            duration: 250,
                            complete: function() {
                                $(window).trigger("resize.accordion"), $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn"), 
                                $(".j-bookingTableReturn , .j-titleReturn").velocity("slideUp", {
                                    complete: function() {
                                        $(".j-bookingTableReturn .j-bookingLoader").velocity("transition.slideDownOut", {
                                            duration: 250,
                                            complete: function() {
                                                var a = $(".j-bookingRecapDep").offset().top - $(".j-bookInfoBox").outerHeight();
                                                $("html").velocity("scroll", {
                                                    duration: 400,
                                                    offset: a,
                                                    complete: function() {
                                                        $(".bookingRecap .j-accordion").addClass("active"), $(window).trigger("resize.accordion"), 
                                                        $(".j-showBackFlights").fadeIn(), book.updateTopPosition(), setTimeout(function() {
                                                            $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
                                                        }, 2e3);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    },
    priceSelectorReset: function(a, b) {
        "boolean" != typeof a && (a.preventDefault(), b = $(a.currentTarget).closest(".j-bookingTable")), 
        b.find(".bookingTable__col").removeClass("headingActive"), b.find(".j-bookTableRow").removeClass("activeRow"), 
        b.find(".j-priceSelector").parent().removeClass("isActive");
        {
            var c = b.find(".j-bookTableRowBody:visible");
            c.closest(".j-bookingTable").find(".bookingTable__header").data("num");
        }
        a && "boolean" == typeof a || c.velocity("slideUp", {
            duration: 400,
            complete: function() {}
        });
    },
    businessTrigger: function() {
        $(".j-bookingTable").toggleClass("businessActivated");
        var a = 0;
        $(".j-bookingTable").hasClass("businessActivated") && (a = "-90%"), $(".bookingTable__rightInner").velocity({
            left: a
        }, {
            duration: 500
        });
    },
    getFlightDetails: function() {
        if (window.breakpointS > window.windowW) {
            var a = $(this).closest(".j-bookTableRow").find(".j-bookTableRowBody"), b = $(this).closest(".j-bookTableRow").find(".bookingTable__right");
            a.is(":visible") ? a.velocity("slideUp", {
                duration: 300
            }) : (a.velocity("slideDown", {
                duration: 300
            }), b.is(":visible") && b.velocity("slideUp", {
                duration: 300
            }));
        } else {
            if ($(this).closest(".j-bookTableRow").hasClass("activeRow")) return;
            if ($(".flyoutBookingFlightDetails").length > 0) return void book.getFlightDetailsRemove();
            var c = $(this).closest(".j-bookTableRow").find(".j-bookingFlightDetails").clone(), d = $(this).offset().top, e = $(this).offset().left;
            book.getFlightDetailsRemove(), $('<div class="flyoutBookingFlightDetails"><a class="flyoutBookingFlightDetails__close" href="javascript:;">&times;</a>' + c.html() + "</div>").appendTo("body"), 
            $(".flyoutBookingFlightDetails").css({
                left: e - 15,
                top: d + 25,
                width: $(".booking__fightPreviewWrapper").outerWidth()
            }).velocity("transition.slideDownIn", {
                duration: 250
            }, {
                complete: function() {
                    $(".flyoutBookingFlightDetails__close").focus();
                }
            }), $(".flyoutBookingFlightDetails__close").fastClick(book.getFlightDetailsRemove);
        }
    },
    getFlightDetailsRemove: function() {
        $(".flyoutBookingFlightDetails").length > 0 && $(".flyoutBookingFlightDetails").velocity("transition.slideDownOut", {
            duration: 250,
            complete: function() {
                $(".flyoutBookingFlightDetails").remove();
            }
        });
    },
    openOptionMobile: function() {
        if (!(window.windowW > window.breakpointS)) {
            var a = $(this).closest(".j-bookTableRow"), b = (a.find(".j-bookingTable"), a.find(".bookingTable__right")), c = a.siblings(".mobileRowActive");
            c.length > 0 && (c.removeClass("mobileRowActive"), c.find(".bookingTable__right").velocity("slideUp", {
                duration: 300
            })), a.addClass("mobileRowActive");
            var d = $(this).closest(".j-bookTableRow").find(".j-bookTableRowBody");
            b.is(":visible") ? (a.removeClass("mobileRowActive"), b.velocity("slideUp", {
                duration: 300
            })) : (b.velocity("slideDown", {
                duration: 300
            }), d.is(":visible") && d.velocity("slideUp", {
                duration: 300
            }));
        }
    },
    goToReturn: function(a) {
        book.destroyToolTip(), a.preventDefault();
        var b = $(".j-titleDeparture"), c = b.next();
        $(".j-bookingTableDeparture .j-bookingLoader").velocity("fadeIn", {
            duration: 700,
            complete: function() {
                b.velocity("slideUp"), c.velocity("slideUp"), $(".j-bookingTableDeparture").velocity("slideUp", {
                    duration: 400,
                    complete: function() {
                        book.updateTopPosition(), $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn").delay(3e3).velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
                    }
                }), $(".j-showBackFlights:eq(0)").fadeIn(), $("html").velocity("scroll", {
                    offset: 0,
                    complete: function() {}
                }), book.recapDeparture.velocity("transition.slideDownIn", {
                    duration: 400,
                    complete: function() {
                        $(".j-bookingRecapDep").attr("style", "").show(), book.updateTopPosition();
                    }
                }), $(window).trigger("resize.accordion");
            }
        });
    },
    selectedReturn: function() {
        book.destroyToolTip();
        var a = $(".j-titleReturn"), b = a.next();
        $(".j-bookingTableReturn .j-bookingLoader").velocity("transition.slideDownIn", {
            duration: 250,
            complete: function() {
                $(".j-bookingTableReturn .j-bookTableRow.activeRow").velocity("slideUp", {
                    duration: 400,
                    complete: function() {
                        $(".j-bookingRecapRet").velocity("transition.slideDownIn", {
                            duration: 250,
                            complete: function() {
                                $(window).trigger("resize.accordion"), $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideUpIn" : "transition.slideLeftIn"), 
                                b.velocity("slideUp"), $(".j-titleReturn").velocity("slideUp"), $(".j-bookingTableReturn").velocity("slideUp", {
                                    complete: function() {
                                        $(".j-bookingTableReturn .j-bookingLoader").velocity("transition.slideDownOut", {
                                            duration: 250,
                                            complete: function() {
                                                var a = $(".j-bookingRecapDep").offset().top - $(".j-bookInfoBox").outerHeight();
                                                $("html").velocity("scroll", {
                                                    duration: 400,
                                                    delay: 100,
                                                    offset: a,
                                                    complete: function() {
                                                        $(".j-showBackFlights").fadeIn(), $(".bookingRecap .j-accordion").addClass("active"), 
                                                        window.bookingDisableFixed = !0, book.resetFixedElement(), $(window).trigger("resize.accordion"), 
                                                        $(".bookingRecap .j-accordion-header").trigger("click"), setTimeout(function() {
                                                            $(".j-bookingFlyoutTooltip").velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
                                                        }, 2e3);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    },
    loaderInit: function(a) {
        $(".j-bookingLoader").velocity("transition.slideDownIn", {
            duration: 250
        });
    }
};

$(function() {
    $(".j-upselling").length > 0 && upsellingsManager.init();
});

var upsellingsManager = {
    upsellings: null,
    tooltip: null,
    init: function() {
        upsellings = $(".j-upselling"), tooltip = $(".j-bookingFlyoutTooltip"), loader = $(".j-bookingLoader");
        for (var a = 0; a < upsellings.length; a++) upselling = $(upsellings[a]), steps = upselling.find(".j-upsellingStep"), 
        upselling.data("stepNum", steps.length), upselling.data("step", 0), stepBtn = steps.find(".j-upsellingBtn"), 
        stepBtn.click(function(a) {
            a.preventDefault(), showTooltip = $(a.currentTarget).hasClass("j-addToCart") ? !0 : !1, 
            dir = $(a.currentTarget).hasClass("j-next") ? 1 : -1, sectionstep = $(a.currentTarget).closest(".j-upsellingStep"), 
            section = sectionstep.closest(".j-upselling"), loader = section.find(".j-loader"), 
            sectionstep.hide(), step = section.data("step"), stepsNum = section.data("stepNum"), 
            step += dir, section.data("step", step), loader.velocity("transition.fadeIn", {
                duration: 500,
                complete: function() {
                    section.find(".j-upsellingStep").eq(step).show(), $(window).trigger("resize"), loader.velocity("transition.fadeOut", {
                        duration: 250,
                        complete: function() {
                            showTooltip ? upsellingsManager.fastShowTooltip() : null, newPos = section.closest(".j-accordion").offset().top - $(".j-bookInfoBox").outerHeight(), 
                            $("html").velocity("scroll", {
                                duration: 400,
                                delay: 100,
                                offset: newPos
                            });
                        }
                    });
                }
            });
        });
    },
    fastShowTooltip: function() {
        tooltip.velocity(window.breakpointM > window.windowW ? "transition.slideDownIn" : "transition.slideLeftIn"), 
        setTimeout(function() {
            tooltip.velocity(window.breakpointM > window.windowW ? "transition.slideDownOut" : "transition.slideLeftOut");
        }, 2e3);
    }
};

$(function() {
    $(".chooseSeat").length > 0 && bookingSeat.init(), $(".j-chooseSeatSlider").length > 0 && bookingSeatSlider.init();
});

var bookingSeatSlider = {
    slider: null,
    sliderCont: null,
    nextBtn: null,
    prevBtn: null,
    offsetSlide: null,
    pos: null,
    bookingSlider: null,
    itemNum: null,
    itemWidth: null,
    init: function() {
        sliderCont = $(".j-chooseSeatSlider").find(".booking__date"), slider = sliderCont.find(".chooseSeat__sliderCover"), 
        sliderList = sliderCont.find(".chooseSeat__sliderList"), nextBtn = sliderCont.find("a.next"), 
        prevBtn = sliderCont.find("a.prev"), pos = slider.find(".chooseSeat__sliderItem .isActive").index(), 
        itemNum = slider.find(".chooseSeat__sliderItem").length, itemWidth = slider.find(".chooseSeat__sliderItem").outerWidth(), 
        offsetSlide = 2, itemNum * itemWidth > slider.outerWidth() ? bookingSeatSlider.setSlider() : (nextBtn.hide(), 
        prevBtn.hide()), bookingSeatSlider.setOnResize();
    },
    setOnResize: function() {
        $(window).on("resize", function() {
            itemNum * itemWidth > slider.outerWidth() ? (null === bookingSeatSlider.bookingSlider && bookingSeatSlider.setSlider(), 
            nextBtn.show(), prevBtn.show()) : (nextBtn.hide(), prevBtn.hide());
        });
    },
    setSlider: function() {
        bookingSlider = slider.iosSlider({
            desktopClickDrag: !0,
            snapToChildren: !0,
            autoSlide: !1,
            infiniteSlider: !1,
            startAtSlide: Math.max(0, pos - offsetSlide)
        }), nextBtn.click(function() {
            var a = slider.data("args").currentSlideNumber;
            slider.iosSlider("goToSlide", a + 3);
        }), prevBtn.click(function() {
            var a = slider.data("args").currentSlideNumber;
            slider.iosSlider("goToSlide", Math.max(a - 3, 1));
        });
    }
}, bookingSeat = {
    seats: null,
    passengers: null,
    passengerIsSelected: !1,
    actualPassengerNum: 0,
    comfortClass: "extraComfort",
    occupiedClass: "occupied",
    availableClass: "available",
    seatsMap: null,
    pricePopOver: null,
    loadSeatsBtn: null,
    infoBoxMap: null,
    init: function() {
        passengers = $(".chooseSeat__passengerItem"), loadSeatsBtn = $(".selecRow"), infoBoxMap = $(".chooseSeat__infoBox"), 
        infoBoxBtns = infoBoxMap.find("[data-fila]"), infoBoxImage = infoBoxMap.find(".image");
        for (var a = 0; a < passengers.length; a++) passenger = $(passengers[a]), passenger.data("num", a), 
        passenger.data("takenSeat", null), passenger.fastClick(function(a) {
            a.preventDefault(), bookingSeat.activePassenger($(a.currentTarget));
        });
        bookingSeat.isMobile() || $(passengers[0]).click(), bookingSeat.initSeatsMap(), 
        bookingSeat.setLoadMoreSeatsBtns(), bookingSeat.setInfoBoxMapBtns();
    },
    initSeatsMap: function() {
        seats = $("[data-col]"), pricePopOver = $(".chooseSeat__overlay"), seatsMap = $(".chooseSeat__passengerSeats"), 
        dataFila = seatsMap.find(".chooseSeat__seats").attr("data-fila"), pricePopOver.hide();
        for (var a = 0; a < seats.length; a++) seat = $(seats[a]), seat.data("num", seat.attr("data-col") + seat.attr("data-row")), 
        ("available" === bookingSeat.getSeatType(seat) || "comfort" === bookingSeat.getSeatType(seat)) && seat.fastClick(function(a) {
            a.preventDefault(), bookingSeat.passengerIsSelected && bookingSeat.verifySeat($(a.currentTarget));
        }), "comfort" !== bookingSeat.getSeatType(seat) || bookingSeat.isMobile() || bookingSeat.setPriceOver(seat);
        bookingSeat.isMobile() && (bookingSeat.initPricePopOver(), seatsMap.find(".j-overlayClose").fastClick(function(a) {
            a.preventDefault(), bookingSeat.seatsMapCloseMobileMode();
        }));
        for (var b = 0; b < infoBoxBtns.length; b++) infoBoxBtn = $(infoBoxBtns[b]), console.log(infoBoxBtn.attr("data-fila"), dataFila), 
        infoBoxBtn.attr("data-fila") === dataFila ? (infoBoxBtn.addClass("active"), infoBoxImage.attr("src", infoBoxBtn.attr("data-image"))) : infoBoxBtn.removeClass("active");
    },
    setInfoBoxMapBtns: function() {
        infoBoxBtns.click(function(a) {
            a.preventDefault(), $(a.currentTarget).hasClass("active") || bookingSeat.loadSeats($(a.currentTarget).attr("data-href"));
        });
    },
    setLoadMoreSeatsBtns: function() {
        loadSeatsBtn.click(function(a) {
            a.preventDefault(), bookingSeat.loadSeats($(a.currentTarget).attr("href"));
        });
    },
    loadSeats: function(a) {
        $.ajax({
            url: a,
            context: document.body
        }).done(function(a) {
            seatsMap.velocity("transition.slideDownOut", {
                duration: 250,
                complete: function() {
                    seatsMap.replaceWith(a), bookingSeat.initSeatsMap(), seatsMap.velocity("transition.slideUpIn");
                }
            });
        });
    },
    isMobile: function() {
        return window.breakpointS > window.windowW || window.ismobile || window.supportsTouch ? !0 : !1;
    },
    initPricePopOver: function() {
        closeBtn = pricePopOver.find(".j-overlayClose"), verifyBtn = pricePopOver.find(".chooseSeat__btn"), 
        closeBtn.fastClick(function(a) {
            a.preventDefault(), pricePopOver.hide();
        }), verifyBtn.fastClick(function(a) {
            a.preventDefault(), bookingSeat.takeSeat(pricePopOver.data("seatData"));
        });
    },
    setPriceOver: function(a) {
        a.hover(function(a) {
            bookingSeat.setPricePopOverPosition($(a.currentTarget)), pricePopOver.show();
        }, function(a) {
            pricePopOver.hide();
        });
    },
    setPricePopOverPosition: function(a) {
        seatPos = a.offset(), seatsMapPos = seatsMap.offset(), ppoWidth = pricePopOver.width(), 
        ppoHeight = pricePopOver.outerHeight(), seatsMapHeight = seatsMap.height(), seatHeight = a.outerHeight(), 
        seatWidth = a.width(), seatPos.top - ppoHeight < seatsMapPos.top ? (_top = seatPos.top - seatsMapPos.top + seatHeight + 12, 
        pricePopOver.removeClass("up"), pricePopOver.addClass("down")) : (_top = seatPos.top - seatsMapPos.top - ppoHeight - seatHeight + 12, 
        pricePopOver.removeClass("down"), pricePopOver.addClass("up")), _left = seatPos.left - seatsMapPos.left - ppoWidth / 2, 
        pricePopOver.css({
            top: _top + "px",
            left: _left + "px"
        });
    },
    getSeatType: function(a) {
        return a.hasClass(bookingSeat.comfortClass) ? "comfort" : a.hasClass(bookingSeat.availableClass) ? "available" : a.hasClass(bookingSeat.occupiedClass) ? "occupied" : "noType";
    },
    activePassenger: function(a) {
        bookingSeat.passengerIsSelected = !0, a.siblings().removeClass("isActive"), a.addClass("isActive"), 
        bookingSeat.isMobile() && seatsMap.velocity("transition.expandIn");
    },
    verifySeat: function(a) {
        "comfort" === bookingSeat.getSeatType(a) && bookingSeat.isMobile() ? (bookingSeat.setPricePopOverPosition(a), 
        pricePopOver.data("seatData", a), pricePopOver.show()) : bookingSeat.takeSeat(a);
    },
    takeSeat: function(a) {
        var b = a.attr("data-col"), c = a.attr("data-row"), d = b + c;
        bookingSeat.setPassengerSeat(d, a);
    },
    setPassengerSeat: function(a, b) {
        bookingSeat.passengerIsSelected = !1;
        for (var c = 0; c < passengers.length; c++) if (passenger = $(passengers[c]), passenger.hasClass("isActive")) {
            passenger.find(".chooseSeat__selectedSeat .number").text(a), passenger.removeClass("isActive"), 
            null !== passenger.data("takenSeat") && bookingSeat.removePreviouslySelectedSeat(passenger.data("takenSeat")), 
            passenger.data("takenSeat", a), passenger.addClass("isSelected"), actualPassengerNum = passenger.data("num"), 
            b.addClass("seatAssigned"), b.find("a").text(actualPassengerNum + 1);
            break;
        }
        bookingSeat.isMobile() ? bookingSeat.seatsMapCloseMobileMode() : bookingSeat.setNextPassenger();
    },
    seatsMapCloseMobileMode: function() {
        pricePopOver.hide(), seatsMap.velocity("transition.expandOut");
    },
    removePreviouslySelectedSeat: function(a) {
        for (var b = 0; b < seats.length; b++) seat = $(seats[b]), seat.data("num") === a && (seat.removeClass("seatAssigned"), 
        seat.find("a").text(""));
    },
    setNextPassenger: function() {
        var a = $(actualPassengerNum + 1 < passengers.length ? passengers[actualPassengerNum + 1] : passengers[0]);
        null === a.data("takenSeat") && bookingSeat.activePassenger(a);
    }
};

$(function() {
    $(".j-multiJourney").length > 0 && multiJourney.init(), $(".j-journeyType").length > 0 && findFlightMenu.init();
});

var findFlightMenu = {
    menuItems: null,
    multiJourneyForm: null,
    signleDestinationForm: null,
    searchBtn: null,
    init: function() {
        menuItems = $(".j-journeyType"), multiJourneyForm = $(".j-multiJourney"), signleDestinationForm = $(".j-internalFlightFinder"), 
        searchBtn = $(".multiJourney__searchBtn >a"), multiJourneyForm.hide(), signleDestinationForm.hide();
        for (var a = 0; a < menuItems.length; a++) menuItem = $(menuItems[a]), menuItem.find("input.j-typeOfFlight").change(function(a) {
            $(a.currentTarget).is(":checked") && ("false" === $(a.currentTarget).closest(".j-journeyType").attr("data-multijourney") ? (signleDestinationForm.show(), 
            multiJourneyForm.hide(), searchBtn.removeClass("isDisabled")) : (multiJourneyForm.show(), 
            signleDestinationForm.hide(), searchBtn.addClass("isDisabled"), multiJourney.checkSearchBtn()));
        });
        menuItems.eq(0).find("input.j-typeOfFlight").attr("checked", !0).change();
    }
}, multiJourney = {
    multiJourneyItem: null,
    addJourneyBtn: null,
    searchBtn: null,
    stateA: ".multiJourney__teaser",
    stateB: ".multiJourney__choose",
    stateC: ".multiJourney__modify",
    init: function() {
        multiJourneyItems = $(".j-multiJourney").find(".j-multiJourneyItem"), addJourneyBtn = multiJourneyItems.find(".j-addJourney"), 
        modifyJourneyBtn = multiJourneyItems.find(".multiJourney__modifyBtn"), confirmModifiedBtn = multiJourneyItems.find(".j-confirmModifiedJourney"), 
        searchBtn = $(".multiJourney__searchBtn >a");
        for (var a = 0; a < multiJourneyItems.length; a++) multiJourneyItem = $(multiJourneyItems[a]), 
        multiJourneyItem.find(".j-autoCompleteInput.departure").attr("id", "ac_departure_" + a), 
        multiJourneyItem.find(".j-autoCompleteInput.arrival").attr("id", "ac_arrival_" + a), 
        multiJourney.setAutoComplete($("#ac_departure_" + a), $("#ac_arrival_" + a)), multiJourney.setAutoComplete($("#ac_arrival_" + a)), 
        multiJourney.setDatePickers("multi-journey_" + a);
        modifyJourneyBtn.fastClick(function(a) {
            a.preventDefault(), btn = $(a.currentTarget), journeyItem = btn.closest(".j-multiJourneyItem"), 
            journeyItem.addClass("modify"), journeyItem.find(multiJourney.stateC).hide(), journeyItem.find(multiJourney.stateB).show();
        }), confirmModifiedBtn.fastClick(function(a) {
            a.preventDefault(), btn = $(a.currentTarget), journeyItem = btn.closest(".j-multiJourneyItem"), 
            journeyItem.removeClass("modify"), _index = multiJourneyItems.index(journeyItem), 
            multiJourney.setPrevJourney(_index), multiJourney.checkAllDepartureData(_index);
        }), addJourneyBtn.fastClick(function(a) {
            a.preventDefault(), btn = $(a.currentTarget), journeyItem = btn.closest(".j-multiJourneyItem"), 
            _index = multiJourneyItems.index(journeyItem) - 1, next_index = multiJourneyItems.index(journeyItem) + 1, 
            multiJourney.UnblockNextStep(_index) && (journeyItem.find(multiJourney.stateA).hide(), 
            journeyItem.find(multiJourney.stateB).show(), multiJourney.setLowRangeDatePicker(_index), 
            multiJourney.setPrevJourney(_index), next_index < multiJourneyItems.length ? multiJourney.enableAddingNextJourney(next_index) : null);
        }), multiJourneyItems.hide(), multiJourneyItems.eq(0).show(), multiJourneyItems.eq(1).show();
    },
    checkSearchBtn: function() {
        multiJourneyItems_selected = $(".j-multiJourney").find(".j-multiJourneyItem.selected"), 
        multiJourneyItems_selected.length > 1 ? searchBtn.removeClass("isDisabled") : null;
    },
    checkAllDepartureData: function(a) {
        for (var b = a + 1; b < multiJourneyItems.length; b++) multiJourneyItem = $(multiJourneyItems[b]), 
        multiJourneyItem.data("selected") === !0 && (_stateB = multiJourneyItem.find(multiJourney.stateB), 
        _stateC = multiJourneyItem.find(multiJourney.stateC), _stateC.hide(), _stateB.show(), 
        multiJourneyItem.addClass("modify"), _dateOfFlight = _stateB.find(".dateOfFlight"), 
        _dateOfFlight.addClass("hasError"), _dateOfFlight.find("li:eq(0)").append('<a class="form__errorIcon" href="#multiJourneyerrorField' + b + '" aria-describedby="multiJourneyerrorField' + b + '"></a>\n						<div class="form__errorField" id="multiJourneyerrorField' + b + '">la data deve essere successiva alla tratta precedente</div>')), 
        multiJourney.setLowRangeDatePicker(b - 1);
    },
    enableAddingNextJourney: function(a) {
        multiJourneyItems.eq(a).show(), _stateA = multiJourneyItems.eq(a).find(multiJourney.stateA), 
        a > 2 ? searchBtn.removeClass("isDisabled") : null, _stateA.show();
    },
    UnblockNextStep: function(a) {
        return _unblockStep = !1, _stateB = multiJourneyItems.eq(a).find(multiJourney.stateB), 
        _dep = _stateB.find(".customInput--flightFinder.departure"), _rtrn = _stateB.find(".customInput--flightFinder.arrival"), 
        multiJourneyItems.eq(a).hasClass("selected") && _dep.hasClass("selected") && _rtrn.hasClass("selected") && (_unblockStep = !0), 
        _unblockStep;
    },
    setLowRangeDatePicker: function(a) {
        datePickerController.setRangeLow("multi-journey_" + (a + 1), datePickerController.getSelectedDate("multi-journey_" + a));
    },
    setPrevJourney: function(a) {
        _stateB = multiJourneyItems.eq(a).find(multiJourney.stateB), _stateC = multiJourneyItems.eq(a).find(multiJourney.stateC), 
        _dep = _stateB.find(".customInput--flightFinder.departure").find(".apt").text(), 
        _rtrn = _stateB.find(".customInput--flightFinder.arrival").find(".apt").text(), 
        _stateB.hide(), _stateC.show(), infoFlight = _stateC.find(".multiJourneyInfoFlight"), 
        _departure = infoFlight.find(".departure"), _arrival = infoFlight.find(".return"), 
        _date = infoFlight.find(".date"), _departure.text(_dep), _arrival.text(_rtrn), _date.text(datePickerController.getSelectedDate("multi-journey_" + a).toLocaleDateString()), 
        multiJourneyItems.eq(a).data("selected", !0);
    },
    setDatePickers: function(a) {
        var b = {};
        b[a] = "%Y-%m-%d", datePickerController.createDatePicker({
            formElements: b,
            noTodayButton: !0,
            fillGrid: !0,
            rangeLow: new Date(),
            rangeHigh: maxRange,
            constrainSelection: !1,
            noFadeEffect: !0,
            wrapped: !0,
            callbackFunctions: {
                dateset: [ multiJourney.enableNextJourney ]
            }
        });
    },
    enableNextJourney: function(a) {
        null !== datePickerController.getSelectedDate(a.id) && (journeyItem = $("#" + a.id).closest(".j-multiJourneyItem"), 
        journeyItem.addClass("selected"), journeyItem.find(".dateOfFlight").hasClass("hasError") && (journeyItem.find(".dateOfFlight").removeClass("hasError"), 
        journeyItem.remove(".form__errorIcon"), journeyItem.remove(".form__errorField")));
    },
    setAutoComplete: function(a, b) {
        a.autocomplete({
            serviceUrl: "/data/airport.json",
            preserveInput: !1,
            appendTo: a.parent(),
            minChars: 3,
            onSearchComplete: function(a, b) {
                for (var c = 0; c < b.length; c++) {
                    var d = b[c];
                    d.value = d.city + " " + d.type;
                }
            },
            formatResult: function(a, b) {
                return '<div class="sugg_dest_data"><span class="sugg_city_name toPass ' + (a.best ? "best" : "") + '">' + a.city + '</span> - <span class="sugg_city_nation ">' + a.country + '</span><br><span class="sugg_airport">' + a.airport + '</span><span class="sugg_airportCode">(' + a.code + ")</span></span></div>";
            },
            onInvalidateSelection: function() {},
            onSelect: function(a) {
                $(this).val(a.value), $(this).siblings(".apt").text(a.city), $(this).siblings(".city").text(a.type), 
                $(this).siblings("#" + $(this).attr("id") + "_country").val(a.code), $(this).siblings(".j-countryCode").val(a.code), 
                $(this).closest(".customInput--flightFinder").addClass("selected"), void 0 !== b && null !== b ? b.focus() : null;
            }
        });
    }
}, dpData = {
    2015: {
        11: {
            1: {
                type: "business",
                highlight: !0
            },
            2: {
                type: "business-economy",
                highlight: !0
            },
            3: {
                type: "economy",
                highlight: !0
            },
            4: {},
            5: {},
            6: {},
            7: {
                amount: 1234,
                highlight: !0
            },
            8: {},
            9: {},
            10: {},
            11: {},
            12: {},
            14: {},
            15: {},
            16: {
                type: "economy",
                highlight: !0
            },
            17: {
                type: "economy",
                highlight: !0
            },
            18: {},
            19: {},
            20: {
                type: "business-economy",
                highlight: !0
            },
            21: {},
            22: {},
            23: {},
            24: {},
            25: {},
            26: {},
            27: {},
            28: {},
            29: {
                type: "business",
                highlight: !0
            },
            30: {},
            31: 2345
        },
        12: {
            1: {},
            2: {},
            3: {
                type: "economy",
                highlight: !0
            },
            4: {},
            5: {},
            6: {},
            7: {},
            8: {},
            9: {},
            10: {},
            11: {},
            12: {
                type: "business-economy",
                highlight: !0
            },
            14: {},
            15: {},
            16: {},
            17: {},
            18: {},
            19: {},
            20: {},
            21: {},
            22: {},
            23: {},
            24: {},
            25: {
                type: "economy",
                highlight: !0
            },
            26: {},
            27: {},
            28: {},
            29: {},
            30: {}
        }
    }
};

$(function() {
    $("#bookingAward_andata").length > 0 && awardsDatePicker.init($("#bookingAward_andata")), 
    $("#bookingAward_ritorno").length > 0 && awardsDatePicker.init($("#bookingAward_ritorno"));
});

var awardsDatePicker = {
    init: function(a) {
        var b = a.attr("id"), c = {};
        c[b] = "%Y-%m-%d", datePickerController.createDatePicker({
            formElements: c,
            staticPos: !0,
            hideInput: !0,
            noTodayButton: !0,
            fillGrid: !0,
            rangeLow: new Date(),
            rangeHigh: maxRange,
            noFadeEffect: !0,
            constrainSelection: !1,
            wrapped: !0,
            callbackFunctions: {
                redraw: [ function(a) {
                    setTimeout(function() {
                        $("#fd-" + b + " tbody td").each(function() {
                            var a, b, c, d, e, f = $(this), g = f.attr("class").split(/\s+/);
                            if (!f.is(".out-of-range")) {
                                for (var h in g) if ("yyyymmdd-" === g[h].substr(0, 9)) {
                                    e = parseInt(g[h].substr(-8, 4)), d = parseInt(g[h].substr(-4, 2)), b = g[h].substr(-2), 
                                    c = parseInt(b);
                                    break;
                                }
                                dpData[e] && dpData[e][d] && dpData[e][d][c] && (a = "", void 0 !== dpData[e][d][c].type && (a += '<p class="bookingAward__dot">', 
                                a += "economy" === dpData[e][d][c].type ? '<span class="i-dot bookingAward__dotEconomy"></span>' : "business" === dpData[e][d][c].type ? '<span class="i-dot bookingAward__dotBusiness"></span>' : '<span class="i-dot bookingAward__dotEconomy"></span>\n												<span class="i-dot bookingAward__dotBusiness"></span>', 
                                a += "</p>"), f.html("<div class='bookingAward__dateWrapper'><span class='bookingAward__date'>" + b + "</span>" + a + "</div>"));
                            }
                        });
                    });
                } ]
            }
        });
    }
};