var userMiles;
var milesCashRatio;

/* funzione da richiamare per far aggiornare il box cash and miles */
function cashAndMilesAjaxRefresh() {
	$.ajax({
		url : removeUrlSelector() + ".cash-and-miles-partial.html",
		data : {},
		context : document.body
	})
	.done(cashAndMilesAjaxRefreshSuccess)
	.fail(cashAndMilesAjaxRefreshFail);
}

function cashAndMilesAjaxRefreshSuccess(data) {
	
	var prevAriaHidden = jQuery("[id^=cashAndMilesAccordion_panel_]").attr(
			"aria-hidden");
	var showPanel = false;
	if (prevAriaHidden === "false") {
		showPanel = true;
	}
	jQuery("#cash-e-miles-header").nextUntil().remove();
	jQuery("#cash-and-miles-container").append(data);
	if ($("#captcha_cash_miles").length > 0) {
		invokeShowCaptcha("captcha_cash_miles");
	}
	cashAndMilesPanelChanged(showPanel);
	initErrorOverlay();
	$(window).trigger("resize");
}

function cashAndMilesAjaxRefreshFail() {
	// console.error("Unable to refresh box cash and miles");
}

function cashAndMilesPanelChanged(showPanel) {
	if (jQuery("#cashAndMilesAccordion_panel_selectMiles").length > 0) {
		initCashAndMiles();
	} else {
		if (jQuery("#cashAndMilesAccordion_panel_login").length > 0) {
			prepareCashAndMilesLoginSocial();
		}
	}
	jQuery("#cashAndMiles_loginButton").on('click',
			onCashAndMilesButtonLoginClick);
	if (showPanel) {
		jQuery("[id^=cashAndMilesAccordion_panel_]").attr("aria-hidden",
				"false");
	}
}

function cashAndMilesDiscount(evt) {
	var milesUsed = jQuery("#cashAndMiles_milesSelected").text();
	validation(evt, "bookingcashmilesconsumer", {
		"milesUsed" : milesUsed
	}, cashAndMilesDiscountSuccessValidation, cashAndMilesDiscountFail);
}

function cashAndMilesDiscountSuccessValidation(data) {
	if (data.result) {
		jQuery("#cashAndMilesConfirmDiscount").off("click");
		var milesUsed = jQuery(".js-sliderRange .sliderRange-value .number")
				.first().text();
		performSubmit("bookingcashmilesconsumer", {
			"milesUsed" : milesUsed
		}, cashAndMilesDiscountSuccessSubmit, cashAndMilesDiscountFail);
	} else {
		showErrors(data);
	}
}

function cashAndMilesDiscountSuccessSubmit(data) {
	/* cash and miles ok */
	refreshInfoBoxPartial();
	jQuery("#cashAndMilesConfirmDiscount").addClass("selected");
	jQuery("#cashAndMilesCancelDiscount").removeClass("selected");
	jQuery("#cashAndMilesCancelDiscount").on("click",
			cashAndMilesCancelDiscount);
}

function cashAndMilesDiscountFail() {
	// console.log("cashAndMilesDiscountFail");
}

function cashAndMilesCancelDiscount() {
	var milesUsed = jQuery(".js-sliderRange .sliderRange-value .number")
			.first().text();
	performSubmit("bookingcashmilesconsumer", {
		"milesUsed" : milesUsed,
		"_cashAndMiles_action" : "cancel"
	}, cashAndMilesCancelDiscountSuccess, cashAndMilesCancelDiscountFail);
}

function cashAndMilesCancelDiscountSuccess(data) {
	/* cancellazione del cash and miles ok */
	refreshInfoBoxPartial();
	jQuery("#cashAndMilesCancelDiscount").off("click");
	jQuery("#cashAndMilesCancelDiscount").addClass("selected");
	jQuery("#cashAndMilesConfirmDiscount").removeClass("selected");
	jQuery("#cashAndMilesConfirmDiscount").on("click", cashAndMilesDiscount);
}

function cashAndMilesCancelDiscountFail() {
	// console.log("cashAndMilesCancelDiscountFail");
}

function cashAndMilesUpdateDiscount() {
	var milesUsed = jQuery("#cashAndMiles_milesSelected").text();
	jQuery("#cashAndMiles_milesAmount").text(userMiles - milesUsed);
	var discountAmount = parseInt(milesUsed * milesCashRatio);
	var currencyHTML = jQuery("<div />").append(
			$("#cashAndMiles_discountAmount .currency").clone()).html();
	jQuery("#cashAndMiles_discountAmount").html(
			currencyHTML.concat(discountAmount.toString()));
}

function initCashAndMiles() {
	var is_mobile = navigator.userAgent.match(/iPhone/i) 
	 	|| navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i) 
	 	|| navigator.userAgent.match(/Android/i) ? true : false;
	if(is_mobile){
		jQuery("#cashAndMiles_milesSelectController").on("touchstart", function() {
			jQuery("body").on("touchmove", cashAndMilesUpdateDiscount);
			jQuery("body").on("touchend", function() {
				jQuery("body").off("touchmove", cashAndMilesUpdateDiscount);
			});
		});
	}
	else{
		jQuery("#cashAndMiles_milesSelectController").mousedown(function() {
			jQuery("body").mousemove(cashAndMilesUpdateDiscount);
			jQuery("body").mouseup(function() {
				jQuery("body").off("mousemove", cashAndMilesUpdateDiscount);
			});
		});
	}
	jQuery("#cashAndMiles_milesSelected").text(
			jQuery("#cashAndMiles_slider").attr("data-min"));
	userMiles = jQuery("#cashAndMiles_milesAmount").text();
	milesCashRatio = jQuery("#cashAndMiles_slider").attr("data-factor");
	cashAndMilesUpdateDiscount();
	jQuery("#cashAndMilesConfirmDiscount").on("click", cashAndMilesDiscount);
	initSlider();
}