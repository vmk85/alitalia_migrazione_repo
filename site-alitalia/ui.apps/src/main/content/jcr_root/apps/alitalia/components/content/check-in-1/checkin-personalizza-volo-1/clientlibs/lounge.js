var checkinCustomizeOptions = checkinCustomizeOptions || {};
checkinCustomizeOptions.checkinWaveAncillaryLounge = function() {

    function successLoungeProcessing(data) {
        $("#addLoungeBtn").removeClass("disabled");
        if(data.isError == true) {
            //alert("Error: " + data.errorMessage);
        } else {
            $('#chkwaveloungenotification').addClass("confirm-status").text(message_baggage_ok.replace("{0}", "X"));
            performSubmit('checkingetcart','#frmCart',loadCartSuccess,loadCartFail);
        }
        disableLoaderCheckin();

    }
    function failureLoungeProcessing(data) {
        disableLoaderCheckin();
        $("#addLoungeBtn").removeClass("disabled");
        if(data.isError == true) {
            // alert("Error: " + data.errorMessage);
        }
    }

    function adjustCurrentLoungePrice() {
        var totalPrice = 0;
        $('input.chkwavelounge[name=chklounge]:checked').each(function( index ) {
            var priceTotPrice= String($(this).val());
            var totSum = String(totalPrice).split(".");
            var totAct = priceTotPrice.split(".");
            var totResultPost = (totSum[1] == undefined ? 0:parseInt(totSum[1]) ) + (totAct[1] == undefined ? 0:parseInt(totAct[1]) );
            var totResultPre = parseInt(totSum[0]) +parseInt(totAct[0]);


            totalPrice = totResultPre + (totResultPost == 0? "":"." + totResultPost);
        });

        totalPrice = String(totalPrice).split(".");
        if (totalPrice.length > 1){
            totalPrice = totalPrice[0] + (totalPrice[1] != undefined ? separatore:"") + (totalPrice[1].length > 1? totalPrice[1] : totalPrice[1] + "0" )+ " " + currency;
        } else {
            totalPrice = totalPrice[0] + (decimale2 != "" ? separatore:"") + decimale2 + " " + currency;
        }

        totalPrice = splitAdjust(totalPrice);

        $(".chkwavelounge.total-row .price").text(totalPrice);
    }

    $('input.chkwavelounge[name=chklounge]').click(function(e) {
        adjustCurrentLoungePrice();
    });

    $('#addLoungeBtn').click(function(){
        if(!$(this).hasClass("disabled")) {
            $(this).addClass("disabled");

            $('#form-checkinwave-lounge').remove();

            var form = $("<form>").attr("id","form-checkinwave-lounge").appendTo("body");
            $('<input>').attr("type", "hidden").attr("name", "numeroPasseggeri").val(chkwaveData.lounge.length).appendTo(form);
            chkwaveData.lounge.forEach(function(item, index){
               var FF = item.frequentFlyer == "true";

               /*
               var checkLounge = [];
               var elementsChecked = [];
               $('input#chkwavelounge_' + item.key).each(function(){
                    elementsChecked.push($(this).is(":checked") || false);
               });
               if(FF){
                    loungeAllow.forEach(function(el){
                    if(el){
                        checkLounge.push(FF);
                    }
                    if(!el){
                        checkLounge.push(false);
                    }
                    });
               }else{
                    var count=0;
                    loungeAllow.forEach(function(el){
                    if(el){
                        checkLounge.push(elementsChecked[count]);
                        count++;
                    }
                    if(!el){
                        checkLounge.push(false);
                    }
                    });
               }
                */
               $.each( item, function( key, value )
              {
               $('<input>').attr("type", "hidden").attr("name", key).val(value).appendTo(form);
              });
              if (FF)
                $('<input>').attr("type", "hidden").attr("name", "lounge_" + item.key).val("true").appendTo(form);
              else
                $('<input>').attr("type", "hidden").attr("name", "lounge_" + item.key).val($('input#chkwavelounge_' + item.key).is(":checked")).appendTo(form);

            });
            enableLoaderCheckin();
            performSubmit('setlounge', '#form-checkinwave-lounge', successLoungeProcessing, failureLoungeProcessing).promise().done(function(){
                $("#loungeXBT").trigger( "click" );
                $("#loungeX").trigger( "click" );
            });
        }
    });
    adjustCurrentLoungePrice();
}