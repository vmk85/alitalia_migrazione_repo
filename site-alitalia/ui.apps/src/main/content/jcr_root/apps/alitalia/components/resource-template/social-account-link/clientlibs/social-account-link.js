/* event handlers */

function onLogoutRequested(e) {
	startLogout();
}

function onAccountLinkConfirmRequested(e) {
	e.preventDefault();
	performValidation('millemigliasocialloginsubmit', '#form-millemiglia-social', 
			accountLinkValidationSuccess, accountLinkValidationError);
}


/* callbacks */

function accountLinkValidationSuccess(data) {
	return (data.result ? continueAccountLink(false) : showErrors(data));
}

function accountLinkValidationError() {
	return socialLoginContinue(true);
}

function continueAccountLink(stop) {
	removeErrors();
	if (!stop) {
		var code = $('#code').val();
		var pin = $('#pin').val();
		var id = $('#id').val();
		var signature = $('#signature').val();
		continueSocialLoginWithMilleMigliaCredentials(id, signature, code, pin, unexpectedError, loginFailure);
	}
}

function unexpectedError() {
	// TODO [consumer/socialaccountlink] applicare stili corretti per visualizzazione errori in pagina (al momento non sono visibili)
	$('#form-millemiglia-social').append('<a class="form__errorIcon"></a><div id="unexpectedError" class="form__alert form__errorField"> ERRORE DURANTE LA PROCEDURA DI LOGIN</div>');
}

function loginFailure() {
	// TODO [consumer/socialaccountlink] applicare stili corretti per visualizzazione errori in pagina (al momento non sono visibili)
	$('#form-millemiglia-social').append('<a class="form__errorIcon"></a><div id="loginError" class="form__alert form__errorField"> LE CREDENZIALI CHE HAI INSERITO NON SONO CORRETTE</div>');
}


/* executable code */

$(document).ready(function() {
	$('#socialLoginSubmit').bind('click', onAccountLinkConfirmRequested);
	$('#logout').bind('click', onLogoutRequested);
});
