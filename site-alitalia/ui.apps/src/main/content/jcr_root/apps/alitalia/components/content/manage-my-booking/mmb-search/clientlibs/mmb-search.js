var isMmbFromHomePage = false;
var webCheckinPaeseLingua;
var _checkin_output = 'checkin';
var _myflight_output = 'mieivoli';


$(document).ready(function(){
	var success = getParameterByName('success');
	if (success == 'false') {
		$('#errorField_ins').show();
		$('.form__errorField').addClass('isError');
	}

});

$('#mieiVoliSubmit').click(function(e) {
	e.preventDefault();
	isMmbFromHomePage = $(e.currentTarget).hasClass("mmbHomePageButton");
	if (isMmbFromHomePage) {
		widget_showOverlay();
	} else {
		startPageLoader(true);
	}
	startMmbSearch();
});


	
$(document).ready(function(){
	//webCheckinPaeseLingua = "/" + getLanguageCode() + '_' + getMarketCode();
	 webCheckinPaeseLingua = "/" + getMarketCode() + '_' + getLanguageCode();
    $('#myFlightSubmit').bind('click', function(e) {
    	webCheckinValidate(e, 'MIEI_VOLI', _myflight_output); });
   
	if (window['registerLoginContextProfileCallback']) {
		window['registerLoginContextProfileCallback'](
				webCheckinLoginContextProfileCallback);
	}
});

function webCheckinLoginContextProfileCallback() {
	prepareHeaderLoginSocial('checkinCheckinLoginDiv');
	prepareHeaderLoginSocial('checkinMyFlightLoginDiv');
	
    if (typeof isLoginContextAuthenticated != 'undefined' 
    		&& isLoginContextAuthenticated()) {
    	var user = getLoginContextProfileProperty('customerName') + ' ' + 
    		getLoginContextProfileProperty('customerSurname');
        $('#checkin_username').html(user);
        $('#myflight_username').html(user);
        $('#checkin_search').removeClass('active opening');
        $('#myflight_search').removeClass('active opening');
        $('#checkin_login').hide();
        $('#myflight_login').hide();
        $('#checkin_user').show().addClass('active opening');
        $('#myflight_user').show().addClass('active opening');
        webCheckinRetrieveDataMyFlight();
    } else {
        $('#checkin_user').hide();
        $('#myflight_user').hide();    	
    }
    
    $('#checkin_login .fifthButton').bind('click', function(e) {
    	webCheckinSocialLogin(e, _checkin_output, 'c'); });
    $('#myflight_login .fifthButton').bind('click', function(e) {
    	webCheckinSocialLogin(e, _myflight_output, 'f'); });
}

function webCheckinSocialLogin(e, c, u) {
	e.preventDefault();
	removeErrors();
	$('#' + c + '_code').val($('#' + c + '_code').val().trim());
	var data = {};
	var callBack = "";
	if (c == _checkin_output) {
		data = {
			_action : 'validate',
			fieldprefix : 'checkin_',
			checkin_code : $('#' + c + '_code').val(),
			checkin_pin : $('#' + c + '_pin').val()
		};
		callBack = webCheckinSocialLoginValidationCheckinSuccess;
		isCheckInFromHomePage = $('#checkinSearchFrom').val()=="homepage"?true:false;
	} else if (c == _myflight_output) {
		data = {
			_action : 'validate',
			fieldprefix : 'mieivoli_',
			mieivoli_code : $('#' + c + '_code').val(),
			mieivoli_pin : $('#' + c + '_pin').val()
		};
		callBack = webCheckinSocialLoginValidationMyFlightSuccess;
	} else {
		return false;
	}

	invokeGenericFormService('millemiglialoginvalidation', 'POST', null,
			callBack, headerLoginUnexpectedErrorCallback, null, null, data);
	return false;
}

function webCheckinSocialLoginValidationCheckinSuccess(data) {
	return webCheckinSocialLoginValidationSuccess(data, 'c', _checkin_output);
}

function webCheckinSocialLoginValidationMyFlightSuccess(data) {
	return webCheckinSocialLoginValidationSuccess(data, 'f', _myflight_output);
}

function webCheckinSocialLoginValidationSuccess(data, u, c) {
	if (data.result) {
		// FIXME: reactivate for SYNC login
		/*var url = "";
		if (u == 'c') url = 'https://check-in.alitalia.com/WebCheckIn'
			+ webCheckinPaeseLinguaOld;
		if (u == 'f') url = boxLoginRedirect;
		$('#form-perform-login input[name=resource]').val(url);*/
	
		// async login
		startLoginWithMilleMigliaCredentials(
				$('#' + c + '_code').val(),
				$('#' + c + '_pin').val(),
				false, 
				headerLoginUnexpectedErrorCallback,
				headerLoginFailureCallback);
	} else {
		showErrors(data, true);
	}
}

function webCheckinValidate(e, formName, c) {
	e.preventDefault();
	var requestData;
	var serviceName;
	var submitHandler;
	var pnr = $('#' + c + '__code').val($('#' + c + '__code').val().trim()).val();
	var passengerName = $('#' + c + '__name').val($('#' + c + '__name').val().trim()).val();
	var passengerSurname = $('#' + c + '__surname').val($('#' + c + '__surname').val().trim()).val();
	if (formName == 'MIEI_VOLI') {
		submitHandler = webCheckinSubmitMyFlight;
		serviceName = 'mmbsessioninit';
	requestData = {
				mieivoli__code : pnr,
				mieivoli__name : passengerName,
				mieivoli__surname : passengerSurname,
				_action : 'validate'
			};
	} else if (formName == 'CHECK_IN') {
		submitHandler = newWebCheckinSubmitCheckin;
		serviceName = 'checkinsessioninit';
		requestData = {
				checkin__code : pnr,
				checkin__name : passengerName,
				checkin__surname : passengerSurname,
				checkinSearchType : 'code',
				formType : formName,
				_action : 'validate'
		};
		isCheckInFromHomePage = $('#checkinSearchFrom').val()=="homepage"?true:false;
		if (isCheckInFromHomePage) {
			widget_showOverlay();
		} else {
			startPageLoader(true);
		}
	} else {
		submitHandler = webCheckinSubmitCheckin;
		serviceName = 'preparaviaggiosubmit';
		requestData = {
				codiceCheckIn : pnr,
				nome : passengerName,
				cognome : passengerSurname,
				formType : formName,
				_action : 'validate'
		};
	}
	invokeGenericFormService(serviceName, 'POST', '#',
			submitHandler, webCheckinSubmitError, null, 'json',
			requestData);
	return false;
}

function webCheckinSubmitCheckin(data) {
	var url = webCheckinBaseUrlCheckin + webCheckinPaeseLingua +
		webCheckinBaseUrlCheckinRedirectHandler;
	return webCheckinSubmit(data, url, _checkin_output);
}

function newWebCheckinSubmitCheckin (data) {
	if (data.result) {
		removeErrors();
		performSubmit('checkinsessioninit', '#checkInSearch', checkInSearchSubmitSuccess, checkInSearchSubmitError);
	} else {
		showErrors(data);
		if (isCheckInFromHomePage) {
			widget_hideOverlay();
		} else {
			startPageLoader(false);
		}
	}
}

function webCheckinSubmitMyFlight(data) {
	if (data.result) {
		removeErrors();
	var pnr = $('#mieivoli__code').val();
	var passengerName = $('#mieivoli__name').val();
	var passengerSurname = $('#mieivoli__surname').val();
	var form = {
		mieivoli__code : pnr,
		mieivoli__name : passengerName,
		mieivoli__surname : passengerSurname
	};
	performSubmit('mmbsessioninit', form,
			startMmb, MyFlightError);
		widget_showOverlay();
	} else {
		showErrors(data, true);
	}
}

function startMmb(data) {
	window.location.replace(data.redirect);
	return;
}

function MyFlightError() {
	console.error("MyFlightError");
	widget_hideOverlay();
}

function webCheckinSubmit(data, url, selector) {
	if (data.result) {
		removeErrors();
		window.location.href = url
				+ "?Name=" + $('input#' + selector + '__name').val()
				+ "&LastName=" + $('input#' + selector + '__surname').val()
				+ '&TripId=' + $('input#' + selector + '__code').val();
	} else {
		showErrors(data, true);
	}
	return false;
}

function webCheckinSubmitError() {
	return false;
}

function webCheckinRetrieveDataMyFlight() {
	var data = {};
	performSubmit('mmbcheckmyflights', data,
			webCheckinRetrieveMyFlight, webCheckinFailMyFlight);
}

function webCheckinRetrieveMyFlight(data) {
	webCheckinRetrieve(data, '#myflight_user', _myflight_output);
}

function webCheckinRetrieve(data, selector, output) {
	if (data.result && !data.isError) {// && data.pnr) {
		switch (output) {
			case _checkin_output:
//				isCheckInFromHomePage = $(e.currentTarget).hasClass("checkInHomePageButton");
				isCheckInFromHomePage = $('#checkinSearchFrom').val()=="homepage"?true:false;
				if (isCheckInFromHomePage) {
					var _l = "";
					var _r = data.Routes;
					for (var i = 0; i < _r.length; i++) {
						if (_r[i].Status == "Available") {
							$('#checkin_submit').attr('href', data.redirect);
							$(selector + ' .checkin_empty').hide();
							$(selector + ' span.checkin_full').show();
							$('#checkin_submit').show();
							break;
						}
					}
				}
				else if ($(location).attr('pathname').contains('check-in.html')){
					if (data.redirect) {
						window.location.replace(data.redirect);
					}
				}
				break;

			case _myflight_output:
				var number = parseInt(data.pnr);
				if (number > 0) {
					$('#box_prenotations').show();
					$('#box_prenotations_button').show();
					$('#mieiVoliSubmit').attr('href', data.redirect);
					$('#box_prenotations').text(data.message);
					$('#box_no_prenotations').hide();	
				}
				else {
					$('#box_prenotations').hide();
					$('#box_prenotations_button').hide();
					$('#box_no_prenotations').show();
				}
				break;
		}
	} else {
		switch (output) {
			case _checkin_output:
				break;
			case _myflight_output:
				$('#mieiVoliSubmit').attr('href', data.redirect);
				break;
		}
		$(window).trigger("resize");
		return webCheckinFail(selector);
	}
	$(window).trigger("resize");
	return false;
}


function webCheckinFailMyFlight() {
	webCheckinFail('#myflight_user');
}




function startMmbSearch() {
	$('#errorField_ins').hide();
	performValidation('mmbsessioninit', '#mieiVoliSearch', mmbSearchValidationSuccess, mmbSearchValidationError);
}

function mmbSearchValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('mmbsessioninit', '#mieiVoliSearch', mmbSearchSubmitSuccess, mmbSearchSubmitError);
	} else {
		showErrors(data);
		if (isMmbFromHomePage) {
			widget_hideOverlay();
		} else {
			startPageLoader(false);
		}
	}
}

function mmbSearchValidationError() {
	console.log('fail mmbSearchValidationError');
	if (isMmbFromHomePage) {
		widget_hideOverlay();
	} else {
		startPageLoader(false);
	}
}

function mmbSearchSubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	}
	if (isMmbFromHomePage) {
		widget_hideOverlay();
	} else {
		startPageLoader(false);
	}
}

function mmbSearchSubmitError() {
	console.log('fail mmbSearchSubmitError');
	if (isMmbFromHomePage) {
		widget_hideOverlay();
	} else {
		startPageLoader(false);
	}
}
