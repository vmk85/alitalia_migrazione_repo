$(document).ready(function(){

    CQ.I18n.setLocale(languageCode);

    //Reset all modals on close
    $(document).on("closed.zf.reveal", ".reveal--myalitalia-login-section", manageClosingModals);

    // handler apertura modale di adeguamento
    $(document).on("open.zf.reveal", "#needUserSettings", function(){
        // se l'utente è STRONG nascondo del testo, altrimenti lo mostro
        if(userMAData != null && (userMAData.isStrong == null || userMAData.isStrong == false || userMAData.isStrong == "false"))
            $(".show-if-not-strong").removeClass("hide");
        else $(".show-if-not-strong").addClass("hide");
    });

    var defaultSecureQuestion = "";
    var requestData = {
        'prefissiNazionali': ""
    };
    getDropdownData(requestData, dropdownDataResetSuccess, dropdownDataResetFail);

    $("#prefixForReset, #telephoneNumber, #passwordForResetOTP").on("keyup change", function() {

        var tarPrefix = $("#prefixForReset");
        var tarNumber = $("#telephoneNumber");
        var tarPwd = $("#passwordForResetOTP");
    });

    userMAUtils.init();
    gigyaInit.push(initGigya);
//    gigyaInit.push(userMAUtils.init);

    $("#ma-otp").on("keyup", function(){
        //Check OTP: invalido la CTA "invia" se ha meno di 6 cifre
        var inp = $(this);
        var isDisabled = !(inp.val() != null && inp.val().length > 5);
        $(".changeEvent").attr("disabled", isDisabled);
    });

});

function manageClosingModals(e) {
    var id = e.target.id;
    if(id != null) {
        if($("#" + id + " *> .otpLoader").length == 0) {
            if(!OTPUtils.loaderTOManager.isNull())
                setTimeout(function(){$("[data-open='" + OTPUtils.loaderTOManager.modalName + "']").trigger("click")}, 1);
            else OTPUtils.resetModals();
        } else setTimeout(function(){$("[data-open='" + id + "']").trigger("click")}, 1);
    }
}

function initGigya() {
    userMAUtils.needUserSettings();
    initNUS();
}

function dropdownDataResetSuccess(data) {

    var prefix = data.prefissiNazionali;

    for(var i in prefix) {
        $("#prefixForReset").append($("<option>", {
            value: prefix[i].prefix,
            text: prefix[i].description + "(+" + prefix[i].prefix + ")"
        }))
    }

}

function dropdownDataResetFail() {
    alert("fail");
}

function searchTypeReset() {
    $("#gigyaSearchForm > [name='email']").val(userMASession.email);
    performSubmit("gigyaSearchServlet", "#gigyaSearchForm", gigyaSearchSuccess,gigyaSearchFail);
}

function gigyaSearchSuccess(resp) {
    if(typeof resp == "string") resp = JSON.parse(resp);
    if(!resp.isError) {
        if(resp.blearNumber != null) userMASession.cryptData = resp.blearNumber;
        resetViaSms();
    } else callSupport();
}

function callSupport() {
    OTPUtils.disableLoader();
    $("[data-open='contactHelpDesk']").trigger("click");
}

function resetViaSms() {

    userMASession.typeReset = "SMS";

    $("#sendMAOTPForm > input[name='scope']").val("getOTP");

    performSubmit("sendMAOTPServlet", "#sendMAOTPForm", getOTPSuccess, getOTPFail);
}

function gigyaSearchFail() {
    OTPUtils.disableLoader();
}

function getOTPSuccess(response) {
    OTPUtils.disableLoader();
    cleanResetErrors("#reset-ma-secretA", "#ma-reset-password-secretQ");
    if(!response.isError) {
        var cryptData = userMASession.cryptData;
        $("[data-open='ma-check-otp']").trigger("click");
        if(userMASession.caller.toLowerCase() == "nus") $(".titleCOTP").text(CQ.I18n.get("myalitalia.needUserSettings.title"));
        else $(".titleCOTP").text(CQ.I18n.get("myalitalia.otp.resetPassowrd.title"));
        $(".cotpDescr").text(CQ.I18n.get("myalitalia.OTP.modal.SMStitle", cryptData));
    } else {
        gigyaSearchFail();
    }
}

function checkOTPSuccess(response) {
    if(!response.isError) {
        cleanResetErrors("#ma-otp", "ma-check-otp");
        OTPUtils.enableLoader();
        if(userMAUtils.gigyaExists()) {
            gigya.accounts.resetPassword({
                loginID:userMASession.email,
                callback: function(response){
                    OTPUtils.disableLoader();
                    if(response.errorCode == 0) {
                        $("[data-open='success-reset']").trigger("click");
                        userMASession.email != null && localStorage.setItem("er-ma", userMASession.email); // Setto l'email che sarà prefillata nel
                    }
                }
            });
        } else OTPUtils.disableLoader();
    } else {
        // Check OTP: invalidata la CTA invia
        $("#ma-otp").val("");
        $("#ma-otp").trigger("keyup");

        OTPUtils.disableLoader();
        if(response.errorCode == 5003 || response.errorCode == "5003") {
            setResetErrors(null, "#labelError-genericNUS", response.feedback);
            $("#labelError-genericNUS");
            if(userMASession.caller != null && userMASession.caller == "resetPWD") $("[data-open='login-ma']").trigger("click");
            if(userMASession.caller != null && userMASession.caller == "NUS") $("[data-open='needUserSettings']").trigger("click");
        } else if(response.errorCode == 5005 && response.errorCode == "5005" && (userMASession.resetType != null && userMASession.resetType == "SQ" && userMASession.email != null)) {
            $(".resendOTPMail").removeClass("hide");
        }else setResetErrors("#ma-otp", "#labelErrorLogin-ma-otp", response.feedback);
    }
}

function getOTPFail() {
    setResetErrors("#reset-ma-secretA", "#ma-reset-password-secretQ", CQ.I18n.get("myalitalia.genericerror"));
    setResetErrors("#emailForReset", "#ma-reset-password-secretQ", CQ.I18n.get("myalitalia.genericerror"));
    OTPUtils.disableLoader();
}

function checkOTPFail() {
    OTPUtils.disableLoader();
    setResetErrors("#ma-otp", "#labelErrorLogin-ma-otp", CQ.I18n.get("myalitalia.wrongOTP"));
}

function checkMAOTP() {
    if($("#ma-otp").val() != null) {
        cleanResetErrors("#ma-otp", "#labelErrorLogin-ma-otp");
        $("#sendMAOTPForm > input[name='data']").val($("#ma-otp").val());
        $("#sendMAOTPForm > input[name='scope']").val("checkOTP");
        OTPUtils.enableLoader();
        performSubmit("sendMAOTPServlet", "#sendMAOTPForm", checkOTPSuccess, checkOTPFail);
    } else {
        setResetErrors("#ma-otp", "#labelErrorLogin-ma-otp", CQ.I18n.get("myalitalia.fieldObligatory"));
    }
}

function resetPasswordMAsecretQ() {
    var newPassword = $("#ma-new-password-otp").val();
    var email = userMASession.email;
    var tarSecretA = $("#reset-ma-secretA");
    var secretA = tarSecretA.val();

    if(secretA != null && secretA != "") {
        $("#resetPasswordMA > [name='secretA']").val(secretA);
        OTPUtils.enableLoader();
        performSubmit("sendMAOTPServletEmail", {data:email, scope:"getOTP"}, getOTPSuccess, getOTPFail);
    } else setResetErrors(tarSecretA, "#labelErrorLogin-ma-reset-secretA", CQ.I18n.get("myalitalia.fieldObligatory"));
}


function resetPasswordMASuccess(response) {
    OTPUtils.disableLoader();
    if(!response.isError) {
        $("[data-open='success-reset']").trigger("click");
    } else {
        setResetErrors("#ma-otp", "#labelErrorLogin-ma-otp", CQ.I18n.get("myalitalia.secretA.iswrong"));
    }
}

function resetPasswordMAFail() {
    OTPUtils.disableLoader();
}

function saveCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function deleteCookie(nomeCookie) {
  saveCookie(nomeCookie,'',-1);
}

function setTelephoneNumber(data) {
    var target = $("#telephoneNumber");
    if(data.phone1Number != null) {
        target.val(data.phone1Number);
    } else {
        target.val("");
    }
    if(data.phone1CountryCode != null) $("#prefixForReset").val(data.phone1CountryCode);
}

function cryptTel(tel) {
    if(tel.length > 0) {
        var cifre = tel.length -4;
        var crypt = "";
        if(cifre > 0) {
            for(var i=0; i<cifre; i++) {
                crypt += "*";
            }
            return crypt + tel.substring(tel.length -4);
        }
    }
}

$(".radioGigyaReset").on("click", function(e){
    var current = $(this);
    $(".radioGigyaReset").removeClass("checked");
    $(".radioGigyaReset").find("input").removeAttr("checked");
    $(current).addClass("checked");
    $(current).find("input").attr("checked", "checked");

    if($(current).find("input").val() == "true") {
        $("#telFormReset").show();
        $("#queFormReset").hide();
    } else if($(current).find("input").val() == "false") {
        $("#telFormReset").hide();
        $("#queFormReset").show();
    }
});

$(".saveResetTypeSms").on("click", function(e){

    userMASession.resetType = "SMS";
    userMASession.caller = "nus";

    var current = $(this);
    var prefix, number, pwd, type = true, uid;

    var tarPrefix = $("#prefixForReset");
    var tarNumber = $("#telephoneNumber");

    prefix = tarPrefix.val();

    number = tarNumber.val();

    userMASession.telephone = number;

    userMASession.cryptData = cryptTel(number);

    if(current.attr("disabled") == null) {

        if(prefix != null && number != null && prefix != "" && number != "") {

            userMASession.caller = "NUS";
            userMASession.type = "SMS";

            var emailReset, resetType, form;

            emailReset = userMAProfile.email;
            resetType = "OTP";

            cleanResetErrors(tarPrefix, "#labelErrorLogin-ma-nus-sms-prefix");
            cleanResetErrors(tarNumber, "#labelErrorLogin-ma-nus-sms-number");

            OTPUtils.enableLoader();
            OTPFlow.sendOTP(prefix, number, "clean",OTPFLowSendSuccess, OTPFLowSendFail);
        } else {

            if(prefix == null || prefix == "") setResetErrors(tarPrefix, "#labelErrorLogin-ma-nus-sms-prefix", CQ.I18n.get("myalitalia.fieldObligatory"));
            else cleanResetErrors(tarPrefix, "#labelErrorLogin-ma-nus-sms-prefix");

            if(number == null || number == "") setResetErrors(tarNumber, "#labelErrorLogin-ma-nus-sms-number", CQ.I18n.get("myalitalia.fieldObligatory"));
            else cleanResetErrors(tarNumber, "#labelErrorLogin-ma-nus-sms-number");

            current.attr("disabled", true);

        }

    }

});

function OTPFLowSendSuccess(data) {
    OTPUtils.disableLoader();
    if(!data.isError) {
        $(OTPFlow.action).val("");
        $(".changeEvent").attr("onclick", "OTPFlowCheck()");
        var cryptData = userMASession.cryptData;
        $(".cotpDescr").text(CQ.I18n.get("myalitalia.OTP.modal.SMStitle", cryptTel(userMASession.telephone)));
        if(userMASession != null && userMASession.caller != null)
            $(".titleCOTP").text((userMASession.caller.toLowerCase() == "nus" ? CQ.I18n.get("myalitalia.needUserSettings.title") : CQ.I18n.get("myalitalia.resetPassword.title")));
        $("[data-open='ma-check-otp']").trigger("click");
    }
}

function cryptEmail(email) {
    if(email != null) {
        var se = email.split("@");
        var nc = se[0].length;
        var word = se[0].substring(nc, se[0].length-3); //y96
        var add = "";
        for(var i=0; i<nc-3; i++) {
            add = add.concat("*");
        }
        word = add + word + "@" + se[1];
        return word;
    }
}

function OTPFlowCheck() {
    OTPUtils.enableLoader();
    OTPFlow.checkOTP($("#ma-otp").val(), OTPFlowCheckSuccess, OTPFlowCheckFail);
}

function OTPFlowCheckSuccess(response) {
    if(!response.isError) {
        cleanResetErrors("#ma-otp", "ma-check-otp");
        var prefix = $("#prefixForReset").val();
        var number = $("#telephoneNumber").val();
        if(prefix != null && prefix != "" && number != null && number != "") {
            var params = { phone1Number: number, phone1CountryCode: prefix, isStrong: "true", resetPwdSms: true };
            if(userMAUtils.gigyaExist())
                userMAUtils.setAccountInfoData(params, function(e) {
                    OTPUtils.disableLoader();
                    if(e.errorCode == 0) {

                        userMAData.phone1Number = number;
                        userMAData.phone1CountryCode = prefix,
                        userMAData.resetPwdSms = true;
                        userMAData.isStrong = "true";
                        userMA.data = userMAData;

                        $("#successTitle").text(CQ.I18n.get("myalitalia.nus.dataSaved"));
                        $("[data-open='success']").trigger("click");
                    }
                });
            else OTPUtils.disableLoader();
        }
    } else {
        OTPUtils.disableLoader();
        $("#ma-otp").val("");
        $("#ma-otp").trigger("keyup");
        if(response.erroCode == 5003 || response.errorCode == "5003") {
            setResetErrors(null, "#labelError-genericNUS", response.feedback);
            $("[data-open='needUserSettings']").trigger("click");
        } else setResetErrors("#ma-otp", "#labelErrorLogin-ma-otp", response.feedback);
    }
}

function OTPFlowCheckFail() { OTPUtils.disableLoader(); }

function OTPFLowSendFail() { OTPUtils.disableLoader(); }

function setAccountInfoSuccess(data) {
    OTPUtils.disableLoader();
    if(!data.isError) {
        $("#successTitle").text(CQ.I18n.get("myalitalia.resetPasswordSetted.success"));
        $("[data-open='success']").trigger("click");
    } else {
        if(data.errorCode == 403042) {
            setResetErrors("#passwordForResetOTP", "#labelErrorLogin-ma-nus-sms-pwd", CQ.I18n.get("myalitalia.common.wrongPassword"));
        } else {
            setResetErrors(null, "#labelErrorLogin-ma-nus-sms-generic", CQ.I18n.get("myalitalia.resetForm.errors"));
        }
    }
}

function setAccountInfoFail() {
    OTPUtils.disableLoader();

}

$(".saveResetTypeQue").on("click", function(e) {

    userMASession.resetType = "SQ";

    var secretA, secretQ, pwd, type = false;
    var current = $(this);

    var tarSecretQ = $("#selectQuestions");
    var tarSecretA = $("#inputAnswer");
    var tarPwd = $("#passwordForResetSecretQ");

    secretA = tarSecretA.val();
    secretQ = tarSecretQ.val();
    pwd = tarPwd.val();

    if(current.attr("disabled") == null) {

        if(secretA != null && secretQ != null && pwd != null && secretA != "" && secretQ != "" && pwd != "") {

            gigya.accounts.setAccountInfo({
                secretQuestion: secretQ,
                secretAnswer: secretA,
                password: pwd,
                data: {
                    resetPwdSms: type,
                    isStrong: "true"
                },
                callback: function(e) {
                    console.log(e);

                    cleanResetErrors(tarSecretQ, "#labelErrorLogin-ma-nus-secret-que");
                    cleanResetErrors(tarSecretA, "#labelErrorLogin-ma-nus-secret-ans");
                    cleanResetErrors(tarPwd, "#labelErrorLogin-ma-nus-secret-pwd");
                    cleanResetErrors(null, "#labelErrorLogin-ma-nus-secret-generic");

                    if(e.errorCode == 0) {

                        Object.assign(userMA.data, e.requestParams.data);
                        userMAData = userMA.data;

                        userMASession.caller = "NUS";
                        $("#successTitle").text(CQ.I18n.get("myalitalia.resetPasswordSetted.success"));
                        $("[data-open='success']").trigger("click");
                    } else if(e.errorCode == 403042) {
                        setResetErrors(tarPwd, "#labelErrorLogin-ma-nus-secret-pwd", CQ.I18n.get("myalitalia.common.wrongPassword"));
                    } else {
                        setResetErrors(null, "#labelErrorLogin-ma-nus-secret-generic", CQ.I18n.get("myalitalia.resetForm.errors"));
                    }
                }
            });

        } else {

            if(secretQ == null || secretQ == "") setResetErrors(tarSecretQ, "#labelErrorLogin-ma-nus-secret-que", CQ.I18n.get("myalitalia.fieldObligatory"));
            else cleanResetErrors(tarSecretQ, "#labelErrorLogin-ma-nus-secret-que");

            if(secretA == null || secretA == "") setResetErrors(tarSecretA, "#labelErrorLogin-ma-nus-secret-ans", CQ.I18n.get("myalitalia.fieldObligatory"));
            else cleanResetErrors(tarSecretA, "#labelErrorLogin-ma-nus-secret-ans");

            if(pwd == null || pwd == "") setResetErrors(tarPwd, "#labelErrorLogin-ma-nus-secret-pwd", CQ.I18n.get("myalitalia.fieldObligatory"));
            else cleanResetErrors(tarPwd, "#labelErrorLogin-ma-nus-secret-pwd");

        }
    }
});

$(".checkEMailReset").on("click", function(){
    //vari controlli
    userMASession.email = $("#emailForReset").val();
    userMASession.caller = "reset";
    OTPUtils.enableLoader();
    var email = $("#emailForReset").val();
    if(email == null || email == "") {
        setResetErrors("#emailForReset", "#labelErrorLogin-ma-reset", CQ.I18n.get("myalitalia.fieldObligatory"));
        OTPUtils.disableLoader();
    } else {
        if(userMAUtils.gigyaExist()) {
            gigya.accounts.isAvailableLoginID({
                loginID: email,
                callback: function(e) {
                    if(e.isAvailable) {
                        OTPUtils.disableLoader();
                        setResetErrors("#emailForReset", "#labelErrorLogin-ma-reset", CQ.I18n.get("myalitalia.notValidEmail"));
                    } else {
                        userMASession.caller = "resetPWD";
                        cleanResetErrors("#emailForReset", "#labelErrorLogin-ma-reset");
    //                    $("[data-open='ma-reset-password-OTP']").trigger("click");
                        searchTypeReset();
                    }
                }
            });
        } else OTPUtils.disableLoader();
    }
});

function resetViaSecretQuestion() {
    userMASession.resetType = "SQ";
    gigya.accounts.resetPassword({
      loginID: userMASession.email,
      callback: function(e) {
        OTPUtils.disableLoader();
         $("[data-open='ma-reset-password-secretQ']").trigger("click");
         $("#reset-ma-secretQ").text(e.secretQuestion);
         console.log(e);
      }
   });
}

function initNUS() {
    if(userMA != null && userMAData != null && userMA.errorCode == 0) {
        var data = userMAData;
        if(data != null && data.isStrong != null) {
            if(data.isStrong == "true") {
                if(data.resetPwdSms == true && data.phone1Number != null && data.phone1CountryCode != null) {
                    $("#telFormReset *> #prefixForReset").val(data.phone1CountryCode);
                    $("#telFormReset *> #telephoneNumber").val(data.phone1Number);
                    $(".radioGigyaReset > input[value='true']").trigger("click");
                } else if(data.resetPwdSms == false) $(".radioGigyaReset > input[value='false']").trigger("click")
            }
        }
    }
}

function getDropdownData(requestData, done, fail, always) {
	return invokeGenericFormService('staticdatalistservlet', 'GET', '', done,
			fail, always, 'json', requestData);
}

function invokeGenericFormService(service, method, form, done, fail, always,
		selector, additionalParams) {
	var actualData;
	if ($.type(form) === "string" || !form) {
	    if(form=='#'){
            form = '';
        }
		var serializedData = $(form).serialize();
		if (serializedData != "") {
			serializedData += "&";
		}
		serializedData += "_isAjax=true";
		if (additionalParams) {
			for ( var paramName in additionalParams) {
				serializedData += "&" + paramName + "="
						+ encodeURIComponent(additionalParams[paramName]);
			}
		}
		actualData = serializedData;
	} else if ($.isPlainObject(form)) {
		actualData = {};
		if (form) {
			for (var fieldName in form) {
				actualData[fieldName] = form[fieldName];
			}
		}
		actualData['_isAjax'] = true;
		if (additionalParams) {
			for (var paramName in additionalParams) {
				actualData[paramName] = additionalParams[paramName];
			}
		}
	}
	return $.ajax({
		url : getServiceUrlReset(service, selector),
		method : method,
		data : actualData,
		context : document.body
	}).done(function(data) {
		if (done) {
			done(data);
		}
	}).fail(function() {
		if (fail) {
			fail();
		}
	}).always(function() {
		if (always) {
			always();
		}
	});
}

function getServiceUrlReset(service, selector, secure) {
	if (selector == 'login') {
		return service;
	}
	selector = selector || "json";
	var protocol = location.protocol;

	return protocol + '//' + location.host + internalUrl1 + "/." + service + "." + selector;
}

function setResetErrors(input, label, message) {
    if(input != null) $(input).addClass("is-invalid-input");
    if(label != null && message != null) {
        $(label).find("p").text(message);
        $(label).removeClass("hide");
    }
}

function cleanResetErrors(input, label) {
    if(input != null) $(input).removeClass("is-invalid-input");
    if(label != null) {
        $(label).find("p").text("");
        $(label).addClass("hide");
    }
}

function isPwdValid(password) {
  var pwdlength = password.length;
  var score = 0;
  var min = 2; // La password deve avere almeno 2 delle seguenti condizioni:
  score += password.match(/[A-Z]/g) !== null ? 1 : 0; //-> Contiene maiuscole
  score += password.match(/[a-z]/g) !== null ? 1 : 0; //-> Contiene minuscole
  score += password.match(/[0-9]/g) !== null ? 1 : 0; //-> Contiene Numeri
  score += password.match(/[!@#\$%\^\&*\)\(+=._-]/g) !== null ? 1 : 0; //-> Contiene caratteri speciali
  return score >= 2 && pwdlength > 7;
}

function resendEmailOTP() {
    OTPUtils.enableLoader();
    performSubmit("sendMAOTPServletEmail", {data:userMASession.email}, resendOTPMailSuccess, resendOTPMailFail);
}

function resendOTPMailSuccess(response) {
    OTPUtils.disableLoader();
    if(response.isError) resendOTPMailFail();
    else $(".resendOTPMail").addClass("hide");
}

function resendOTPMailFail() {
    OTPUtils.disableLoader();
    $("#labelError-resendOTPMail > p").text(CQ.I18n.get("myalitalia.resendOTPMail.error"));
}

function resetFlow() {
    OTPUtils.enableLoader();
    var pwdTarget = $("#ma-new-password-otp");
    if(pwdTarget != null && isPwdValid(pwdTarget.val())) {
        $("#resetPasswordMA > input[name='newPassword']").val($("#ma-new-password-otp").val());
        resetViaSms();
    } else {
        setResetErrors("#ma-new-password-otp", "#labelErrorLogin-ma-newPassword-otp", CQ.I18n.get("myalitalia.resetPassword.passwordNotValid"));
        OTPUtils.disableLoader();
    }
}
