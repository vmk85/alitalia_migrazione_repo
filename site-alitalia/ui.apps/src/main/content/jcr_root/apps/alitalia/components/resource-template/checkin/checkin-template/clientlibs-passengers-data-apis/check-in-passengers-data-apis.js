function passengerDataSubmit(e) {
	startPageLoader(true);
	$('#passengersSubmit').off('click', passengerDataSubmit);
	$('#passengersSubmit').addClass('isDisabled');
	validation(e, 'checkinpassengersdataapis', '#passengersForm', passengerDataSubmitSuccess, passengerDataSubmitError);
}

function passengerDataSubmitSuccess(data) {
	if (data.result) {
		removeErrors();
		
		performSubmit('checkinpassengersdataapis', '#passengersForm', 
		function(data) {
			startPageLoader(false);
			window.location.replace(data.redirect);
		}, 
		passengerDataSubmitError);
		
	} else {
		startPageLoader(false);
		$('#passengersSubmit').on('click', passengerDataSubmit);
		$('#passengersSubmit').removeClass('isDisabled');
		showErrors(data);
		/*Tracking errors*/
		if(typeof window["analytics_trackCheckinErrors"] == "function"){
			analytics_trackCheckinErrors(data.fields);
		}
	}
}

function passengerDataSubmitError() {
	startPageLoader(false);
	$('#passengersSubmit').on('click', passengerDataSubmit);
	$('#passengersSubmit').removeClass('isDisabled');
	console.log('fail');
}

function checkInDone() {
	$('#passengersSubmit').on('click', passengerDataSubmit);
}