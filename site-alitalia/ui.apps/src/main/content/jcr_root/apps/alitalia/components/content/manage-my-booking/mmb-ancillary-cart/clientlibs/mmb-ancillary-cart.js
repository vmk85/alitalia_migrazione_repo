
function initPartialMmbCart() {
	$(".remove-ancillary-cart").click(mmbAncillaryRemoveFromCart);
	$("#mmbContinueButton").click(mmbAncillaryContinue);
	$('.j-responsiveTable').responsiveTable({
		  maxWidth: 640
	});
}


/* remove from cart */

function mmbAncillaryRemoveFromCart(e) {
	e.preventDefault();
	var split = e.target.id.split('#');
	var form = {
			ancillaryType: split[0],
			passengerIndex: split[1]
	}
	performSubmit('mmbremoveancillaryfromcart', form, 
			mmbAncillaryRemoveFromCartSubmitSuccess, mmbHandleInvokeError);
	startPageLoader(true);
}

function mmbAncillaryRemoveFromCartSubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		startPageLoader(false);
		if (data.isError) {
			mmbHandleServiceError(data);
		}
		refreshMmbPartialCart(false);
		if (data.ancillaryType == 'Seat') {
			refreshMmbPartialSeat(false, false);
		} else if (data.ancillaryType == 'ExtraBaggage') {
			refreshMmbPartialExtraBaggage(false, false);
		} else if (data.ancillaryType == 'FastTrack') {
			refreshMmbPartialFastTrack(false, false);
		} else if (data.ancillaryType == 'Meal') {
			refreshMmbPartialMeal(false, false);
		} else if (data.ancillaryType == 'VipLounge') {
			refreshMmbPartialLounge(false, false);
		} else if (data.ancillaryType == 'Insurance') {
			refreshMmbPartialInsurance(false, false);
		}
	}
}


/* continue to payment / typ */

function mmbAncillaryContinue(e) {
	e.preventDefault();
	performSubmit('mmbprepaymentconsumer', null, 
			mmbPrePaymentSubmitSuccess, mmbHandleInvokeError);
}

function mmbPrePaymentSubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect + getAnalyticsMmbParameter(data["paymentStepRequired"]));
		return;
	} else {
		if (data.isError) {
			mmbHandleServiceError(data);
		}
		refreshMmbPartialCart(false);
	}
}


function getAnalyticsMmbParameter(paymentStepRequired){
	return "?origin="+ ( analytics_pageMacroSection == "booking" ? "typ" : "mmb") 
		+"&type="+ (!paymentStepRequired ? "free" : "pay");
}
