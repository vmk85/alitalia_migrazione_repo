var loginOptions = {};
var headerWaveRecaptchaID;

function performValidation(service, form, done, fail, always, selector) {
    var additionalParams = {
        '_action' : 'validate'
    };
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector, additionalParams);
}
function performSubmit(service, form, done, fail, always, selector) {
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector);
}
function invokeGenericFormService(service, method, form, done, fail, always,
                                  selector, additionalParams) {
    var actualData;
    if ($.type(form) === "string" || !form) {
        if(form=='#'){
            form = '';
        }
        var serializedData = $(form).serialize();
        if (serializedData != "") {
            serializedData += "&";
        }
        serializedData += "_isAjax=true";
        if (additionalParams) {
            for ( var paramName in additionalParams) {
                serializedData += "&" + paramName + "="
                    + encodeURIComponent(additionalParams[paramName]);
            }
        }
        actualData = serializedData;
    } else if ($.isPlainObject(form)) {
        actualData = {};
        if (form) {
            for (var fieldName in form) {
                actualData[fieldName] = form[fieldName];
            }
        }
        actualData['_isAjax'] = true;
        if (additionalParams) {
            for (var paramName in additionalParams) {
                actualData[paramName] = additionalParams[paramName];
            }
        }
    }
    return $.ajax({
        url : getServiceUrl(service, selector),
        method : method,
        data : actualData,
        context : document.body
    }).done(function(data) {
        if (done) {
            done(data);
        }
    }).fail(function() {
        if (fail) {
            fail();
        }
    }).always(function() {
        if (always) {
            always();
        }
    });
}
//logout temporaneo
// function tempLogoutClick(e){
//     enableLoaderMyAlitalia();
//     if(userMA && userMA.status && userMA.status == "OK") gigya.accounts.logout();
//     else {
//         clearClientContextProfile();
//         performSubmit("savemauser", ".mm-logout", function(resp) {console.log(resp);}, function(resp) {console.log(resp)});
//         $("#form-perform-logout").submit();
//
//     }
// }

function getServiceUrl(service, selector, secure) {
    if (selector == 'login') {
        return service;
    }
    selector = selector || "json";
    var protocol = location.protocol;
    if (jsonProtocol && (selector == "json" || secure)) {
        protocol = 'https:';
    }
    if(service == 'menu-millemiglia-header'){
        return protocol + '//' + location.host + internalUrl1 + "."
            + service + "." + selector;
    } else{
        return protocol + '//' + location.host + internalUrl1 + "/."
            + service + "." + selector;
    }
}

function onHeaderLoginDocumentReady(e) {
    // setUserHeader();
	prepareHeaderLoginSocial();
//	 $(".headerButtonLogin").unbind('click', onHeaderButtonLoginClick);
//	 $(".headerButtonLogin").bind('click', onHeaderButtonLoginClick);
	 $(".headerButtonLogout").unbind('click', onHeaderButtonLogoutClick);
	 $(".headerButtonLogout").bind('click', onHeaderButtonLogoutClick);
	 $(".user__logout").bind('click', tempLogoutClick);
}

/**
 * Event handler for header login button click, start login process with MilleMiglia credentials.
 */
 function onHeaderButtonLoginClick(e) {
    // removeErrors();
    performValidation('millemiglialoginvalidation1', '#form-millemiglia-login-1',
            milleMigliaHeaderLoginValidationSuccess, milleMigliaHeaderLoginValidationFailure);

    //when service not working after 15 seconds error message displayed
    function tryLogin(){
        if ($.active > 0){
           setTimeout(tryLogin,2000);
        }else{
//            $('#header-wave-login-errors').html(CQ.I18n.get('header.socialLogin.error'));
//            $('#header-wave-login-errors').css("display","block");
//            $('.reveal__inner .loader').css('display', 'none');
            grecaptcha.reset();
            refreshReCaptchaLogin(headerWaveRecaptchaID);
        }

    };
    tryLogin();

    return false;
}

/**
 * Event handler for header logout button click, start logout process.
 */
function onHeaderButtonLogoutClick(e) {
	startLogout();
	clearClientContextProfile();
	performSubmit("savemauser", ".mm-logout", function(resp) {console.log(resp);}, function(resp) {console.log(resp)});
	return false;
}


/* internal methods */

function prepareHeaderLoginSocial(div) {
	// if (window['gigya']) {
	// 	window['gigya'].socialize.showLoginUI({
	// 		  versione: 2
	// 		, height: 30
	// 		, width: '100%'
	// 		, buttonsStyle: 'standard'
	// 		, showTermsLink: false
	// 		, hideGigyaLink: true
	// 		, showWhatsThis: false
	// 		, containerID: div || 'socialGigyaIcons'
	// 		, context: {
	// 			action: 'startSocialLogin',
	// 			unexpectedErrorCallback: headerLoginUnexpectedErrorCallback,
	// 			loginFailureCallback: headerLoginFailureCallback
	// 		}
	// 		, onLoad: function(event) {
	// 			// $("#headerLoginDiv").find("center").contents().unwrap();
	// 			// $("#checkinCheckinLoginDiv").find("center").contents().unwrap();
	// 			// $("#checkinMyFlightLoginDiv").find("center").contents().unwrap();
	// 			// $("#headerLoginDiv").find('table').each(addCaptionHeaderUtente);
	// 			// $("#checkinCheckinLoginDiv").find('table').each(addCaptionHeaderUtente);
	// 			// $("#checkinMyFlightLoginDiv").find('table').each(addCaptionHeaderUtente);
	// 		}
	// 		, cid: ''
	// 	});
	// }
}

// function addCaptionHeaderUtente(index, elem) {
// 	if ($(elem).find('caption').length == 0) {
// 		$(elem).prepend('<caption class="hidden">Social Account</caption>');
// 	}
// }


/* callbacks */

function milleMigliaHeaderLoginValidationSuccess(data) {
	if (data.result) {
		var mmCode = $('#form-mm-code').val();
		var mmPin = $('#form-mm-pin').val();
		var rememberMe = "0";
		if ($("#remind-login-data").is(":checked")) {
			rememberMe = "1";
		}
		startLoginWithMilleMigliaCredentials(mmCode, mmPin, rememberMe,
				headerLoginUnexpectedErrorCallback, headerLoginFailureCallback,null,loginUserLockedErrorCallback,true);
	} else {
		if (typeof refreshReCaptchaLogin != 'undefined') {
			refreshReCaptchaLogin(headerWaveRecaptchaID);
		}
		showErrors(data);
	}
}

function loginUserLockedErrorCallback(){
//	$('.j-captchaUserLocked.j-hideOnCLick').velocity('transition.fadeIn');
}

function milleMigliaHeaderLoginValidationFailure() {
	if (typeof refreshReCaptchaLogin != 'undefined') {
		refreshReCaptchaLogin(headerWaveRecaptchaID);
	}
	headerLoginUnexpectedErrorCallback();
}

function headerLoginUnexpectedErrorCallback() {
//	$('.j-loginError.j-hideOnCLick').velocity('transition.fadeIn');
}

function headerLoginFailureCallback(errorCode, errorDescription) {
    grecaptcha.reset();
    $('#header-wave-login-errors').html(CQ.I18n.get('header.socialLogin.error'));

    var errorDescriptionBefore = errorDescription;
    var errorDescriptionLabel;
    if (errorDescriptionBefore.indexOf("wrong username")>-1 || errorDescriptionBefore.indexOf("wrong password")>-1) {
    	errorDescriptionBefore = 'header.socialLogin.error';
    } else if(errorDescriptionBefore.indexOf(";")>-1) {
    	errorDescriptionBefore = errorDescriptionBefore.slice(0, -2).replace(/ /g,'').replace(/;/g,'.');
    } else if (errorDescriptionBefore.indexOf("Unexpected error")>-1) {
    	errorDescriptionBefore = "millemiglia.unexpected.error";
    }
    errorDescriptionLabel = CQ.I18n.get(errorDescriptionBefore);
    //console.log('errorDescriptionLabel: '+errorDescriptionLabel);
    $('#header-wave-login-errors-SA').html(CQ.I18n.get(errorDescriptionLabel));

    $('#header-wave-login-errors').css("display","block");
    $('#header-wave-login-errors-SA').css("display","block");
    $('.reveal__inner .loader').css('display', 'none');
//	$('.j-loginError.j-hideOnCLick').velocity('transition.fadeIn');
}

/**
 * Retrive the value of a GET parameter in URL
 * @param val the parameter name
 * @returns {String} the value of the parameter if found, "" (blank) otherwise
 */
function getURLParameter(val) {
    var result = "", tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
      tmp = items[index].split("=");
      if (tmp[0] === val){
        result = decodeURIComponent(tmp[1]);
    	  for (var j = 2; j < tmp.length; j++) {
    		  result = result + "=" + decodeURIComponent(tmp[j]);
    }
      }
    }
    return result;
}

function refreshReCaptchaLogin(recaptchaWidgetId){
	if (typeof(grecaptcha) !== "undefined") {
		grecaptcha.reset(recaptchaWidgetId);
	}
}

//TODO: showErrors Temporary (original scripts.js)
function showErrors(data, noanchor, checkboxnew) {
//header-wave-login-errors
	removeErrors();
	for (var key in data.fields) {
		var selector = '[name="' + key + '"]';
		if ($(selector).css('display') == 'none' && $(selector).closest(".j-showCaptcha").lenght == 0) {
			selector = $(selector).next();
		}
		var labelitem = key === "code" ? "millemiglia" : key;
		var label = $("label[for='"+labelitem+"']").text();
		$("#header-wave-login-errors").append(label + ": " + data.fields[key] + "<br />");
		$("#header-wave-login-errors-SA").append(label + ": " + data.fields[key] + "<br />");
	}
	return false;
}
function removeErrors() {
    $("#header-wave-login-errors").text("");
    $("#header-wave-login-errors-SA").text("");
    $('input').removeClass('is-invalid-input');
    $('input').prev().removeClass('is-invalid-input');
    $('input').next().removeClass('is-invalid-input');
}


$(document).ready(function(){

    // onHeaderLoginDocumentReady();
    loginOptions.captcha();
    $('.loggeduser_info').on('click',function(){
        if (!$(this).hasClass("loggeduser_info--myalitalia")) {
            loginOptions.nameLogo();
        }
    });
});

loginOptions.captchaSaOldLogin = function() {
    // $('#login-submit').click(function (e) {
    //     e.preventDefault();
        $('#labelErrorLogin-sa-millemiglia').hide();
        $('#labelErrorLogin-sa-password').hide();
        $('#labelErrorLogin-sa-millemiglia').html(''),
        $('#labelErrorLogin-sa-password').html(''),
            $("#password").removeClass('is-invalid-input');
        $("#millemiglia-sa").removeClass('is-invalid-input');

        // $("#form-mm-code").val($("#millemiglia").val());
        $("#form-mm-pin").val($("#password").val());

        $('[name="code"]').val($("#millemiglia-sa").val());
        $('[name="millemiglia"]').val($("#millemiglia-sa").val());

        if(!$(this).hasClass("disabled")) {
            if($("#millemiglia-sa").val() == "") {
                $('#labelErrorLogin-sa-millemiglia').html(CQ.I18n.get('header.accedi.millemiglia.code.info')+': '+CQ.I18n.get('message.generic.empty'));
                $("#millemiglia-sa").addClass('is-invalid-input');
                if($("#password").val() == "") {
                    $('#labelErrorLogin-sa-password').append('<br>' +CQ.I18n.get('header.pincode.label')+': '+CQ.I18n.get('message.generic.empty'));
                    $("#password").addClass('is-invalid-input');
                }
                $('#header-wave-login-errors').show();
            } else if($("#password").val() == "") {
                $('#labelErrorLogin-sa-password').html(CQ.I18n.get('header.pincode.label')+': '+CQ.I18n.get('message.generic.empty'));
                $("#password").addClass('is-invalid-input');
                $('#labelErrorLogin-sa-password').show();
            } else {
                try {
                    if ($("#submit-feedback-login").length > 0) {
                        if (headerWaveRecaptchaID === undefined) {
                            //                    $("#submit-feedback-login").html('');

                            headerWaveRecaptchaID = grecaptcha.render('submit-feedback-login',{
                                'sitekey':invisibleRecaptchaSiteKey,
                                'callback':"onHeaderButtonLoginClick",
                                'size':"invisible"
                            });
                        }
                        grecaptcha.execute(headerWaveRecaptchaID);
                        removeErrors();
                    }
                } catch(exception) {
                    console.log(exception);
                }
            }

        }else{
            return false;
        }
    // }
    // )
};

loginOptions.nameLogo = function() {
     if ($('.binding-mm-full-name.user__name').html() == "") {
       setTimeout(loginOptions.nameLogo, 500); /* this checks the flag every 100 milliseconds*/
    } else {

     	var letterSurname = $('.binding-mm-full-name.user__name').html().split(" ").slice(-1)[0].slice(0,1);
        var letterName=$('.binding-mm-full-name.user__name').html().split(" ")[0].slice(0,1);
        var letters = letterName+letterSurname;

		if (letters){
			console.log(letters);
			$('.name-thumbnail-image').html(letters);
		}else{
			setTimeout(nameLogo, 500);
		}
    }

}

loginOptions.captcha = function() {
    $('#login-submit').click(function (e) {
        e.preventDefault();
        $('#header-wave-login-errors').hide();
        $('#header-wave-login-errors').html(''),
        $("#pin").removeClass('is-invalid-input');
        $("#millemiglia").removeClass('is-invalid-input');
       
        // $("#form-mm-code").val($("#millemiglia").val());
        $("#form-mm-pin").val($("#pin").val());

        $('#form-mm-username').val($("#mmcode").val());
        $('#name_code').val($("#mmcode").val());
        $('[name="millemiglia"]').val($("#millemiglia").val());

        if(!$(this).hasClass("disabled")) {
        	if($("#millemiglia").val() == "") {
        		 $('#header-wave-login-errors').html(CQ.I18n.get('header.accedi.millemiglia.code.info')+': '+CQ.I18n.get('message.generic.empty'));
        		 $("#millemiglia").addClass('is-invalid-input');
        		 if($("#pin").val() == "") {
        			 $('#header-wave-login-errors').append('<br>' +CQ.I18n.get('header.pincode.label')+': '+CQ.I18n.get('message.generic.empty'));
        			 $("#pin").addClass('is-invalid-input');
        		 }
        		 $('#header-wave-login-errors').show();
        	} else if($("#pin").val() == "") {
        		 $('#header-wave-login-errors').html(CQ.I18n.get('header.pincode.label')+': '+CQ.I18n.get('message.generic.empty'));
        		 $("#pin").addClass('is-invalid-input');
        		 $('#header-wave-login-errors').show();
        	} else {
        		try {
                    if ($("#submit-feedback-login").length > 0) {
                        if (headerWaveRecaptchaID === undefined) {
        //                    $("#submit-feedback-login").html('');

                            headerWaveRecaptchaID = grecaptcha.render('submit-feedback-login',{
                                'sitekey':invisibleRecaptchaSiteKey,
                                'callback':"onHeaderButtonLoginClick",
                                'size':"invisible"
                            });
                        }
                        grecaptcha.execute(headerWaveRecaptchaID);
                        removeErrors();
                    }
                } catch(exception) {
                    console.log(exception);
                }
        	}
            
        }else{
            return false;
        }
    })
};

function createLoader() {
    var stru = "";
    stru += "<div class='loader-wrap' id='globalLoader'>";
    stru += "  <div class='loader' style='display: block; position: fixed; width: 100%; height: 100%;'>";
    stru += "    <div class='fade-circle'></div>";
    stru += "  </div>";
    stru += "</div>";
    $("body").append(stru);
}

function deleteLoader() {
    $("#globalLoader").remove();
}

function cercaByFFMASuccess(data) {
    // deleteLoader();
    redirectWithParameter("myalitalia/myalitalia-voli", "flights");
}

function redirectWithParameter(key, par) {
    var base = internalUrl1.substring(0, internalUrl1.length-"homepage".length);
    var path = base + key + ".html?page=" + par;
    location.href = path;
}

function cercaByFFMAFail() {
    deleteLoader();
    alert("Errore nella chiamata alla servlet \"searchbyfrequentflyer\"");
}

$(".goToFlights").on("click", function() {
    searchFlights();
});

function searchFlights() {
    createLoader();
    performSubmit('searchbyfrequentflyer','#form-cercaByFF-MA',cercaByFFMASuccess,cercaByFFMAFail);
}

function setUserHeader() {
        var mm = CQ_Analytics.ClientContextMgr.getRegisteredStore("profile");
    	var logged = ((mm && mm.getInitProperty("isLoggedIn") != null && mm.getInitProperty("isLoggedIn") != false));
        if(logged) {

            if($(".login-included-if-authenticated.user-area.user-logged").hasClass("hide")) {
                $(".login-included-if-authenticated.user-area.user-logged").removeClass("hide");
            }
            if(!$(".login-included-if-anonymous.user-area").hasClass("hide")) {
                $(".login-included-if-anonymous.user-area").addClass("hide");
            }

        } else {
            if(!$(".login-included-if-authenticated.user-area.user-logged").hasClass("hide")) {
                $(".login-included-if-authenticated.user-area.user-logged").addClass("hide");
            }
            if($(".login-included-if-anonymous.user-area").hasClass("hide")) {
                $(".login-included-if-anonymous.user-area").removeClass("hide");
            }
            $("#header").removeAttr("class");
        }
}
/*Fix close reveal in IOS*/
$(document).ready(function() {
    /*Fix close reveal in IOS*/
    $('.reveal .close-button .icon--close--black').on('click',function(){
        $(this).parents('.reveal-overlay').hide();
    });
	/*Remove errors al cambio di modal di login*/
    $("#sa-login a").on('click',function(){
        removeErrors();
    });
 });