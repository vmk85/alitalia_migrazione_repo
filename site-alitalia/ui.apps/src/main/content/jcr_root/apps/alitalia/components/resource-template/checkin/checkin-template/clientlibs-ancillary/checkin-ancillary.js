function checkInDone() {
	initCheckinAncillaryPartials();
	$("#ancillaryProsegui").off('click',preparePayment);
	$("#ancillaryProsegui").on('click',preparePayment);
	
	$("#cartProsegui").off('click',preparePayment);
	$("#cartProsegui").on('click',preparePayment);
	
	$('#undoBackHome').on('click', undoBackHome);
	$('#undoCheckinAll').on('click', undoCheckinAll);
	
	if ($(".bookingUpsell__accordion").length) {
		$(".bookingUpsell__accordion").animateAnchor();
	}
}

function initCheckinAncillaryPartials() {
	if (typeof initPartialCheckinCart !== 'undefined' && 
			typeof initPartialCheckinCart === 'function') {
		initPartialCheckinCart();
	}
	if (typeof initPartialCheckinInsurance !== 'undefined' && 
			typeof initPartialCheckinInsurance === 'function') {
		initPartialCheckinInsurance();
	}
	if (typeof initPartialCheckinSeat !== 'undefined' && 
			typeof initPartialCheckinSeat === 'function') {
		initPartialCheckinSeat();
	}
	if (typeof initPartialCheckinExtraBaggage !== 'undefined' && 
			typeof initPartialCheckinExtraBaggage === 'function') {
		initPartialCheckinExtraBaggage();
	}
	
	$('#sendMailSubmit').on('click', sendMailValidation);
}

function sendMailValidation(e) {
	$('#sendMailSubmit').off('click', sendMailSubmit);
	$('#sendMailSubmit').addClass('isDisabled');
	$("#confirmMail").removeClass("isActive");
	$(".genericErrorMessage").hide();
	validation(e, 'checkinrefusedmail', '#sendMailForm',
			sendMailSubmit, showErrorMessage);	
}

function sendMailSubmit(data) {
	if (data.result) {
		removeErrors();
		performSubmit('checkinrefusedmail', '#sendMailForm', sendMailSubmitSuccess, sendMailSubmitError);
		
	} else {
		showErrors(data);
	}
	
}

function sendMailSubmitSuccess(data) {
	if (data.result == "OK") {
		showSuccessMessage();
	}
	else {
		showErrorMessage();
	}
}

function sendMailSubmitError() {
	showErrorMessage();
}

function showSuccessMessage() {
	$('#sendMailSubmit').on('click', sendMailSubmit);
	$('#sendMailSubmit').removeClass('isDisabled');
	$("#confirmMail").addClass("isActive");
}

function showErrorMessage() {
	$('#sendMailSubmit').on('click', sendMailSubmit);
	$(".genericErrorMessage").show();
}




function preparePayment(e) {
	var $elem = $(e.target);
	if(!$elem.hasClass("isDisabled") 
			&& !$elem.parent("a").hasClass("isDisabled")){
	startPageLoader(true);
	e.preventDefault();
	
	$.ajax({
		url: removeUrlSelector() + ".checkinprepaymentconsumer.json",
		context : document.body
	})
	.done(function(data) { 
		if (data.redirect) {
			window.location.replace(data.redirect + getAnalyticsCheckinParameter(data["paymentStepRequired"]));
		}
		startPageLoader(false);
	})
	.fail(function() {
		startPageLoader(false);
		console.log("error");
	});
}
}

/**
 * function to manage the removing of all specified ancillary
 * by deselecting all checkbox and click on Confirm
 * It is used from extraBaggage, but in can be used by fastTrack and Lounge
 */
function performLastBaggageSelectedOperation(section, addToCart, callBackRemoveFromCart) {
	var allCheckBox = section.find(".extraBagage_detail .placeCheck + input");
	var buttonConfirm = section.find(".fifthButton.j-upsellingBtn.j-next.j-addToCart");
	if (allElementsAreNotSelected(allCheckBox)) {
		buttonConfirm.attr("data-call",callBackRemoveFromCart);
	} else {
		buttonConfirm.attr("data-call",addToCart);
	}
}

function allElementsAreNotSelected(allCheckBox){
	var count = 0;
	allCheckBox.each(function(index, elem) {
		var checkBox = $(elem);
		if (checkBox.val().split("#")[1] == 0) {
			count++;
		}
	});
	return count == allCheckBox.length;
}


/* partial invoke functions */

function invokePartialCheckinAncillary(partialName, displayFeedbackStep, done, fail) {
	data = {};
	if (displayFeedbackStep) {
		data.displayfeedbackstep = "true";
	}
	$.ajax({
		url: removeUrlSelector() + "." + partialName,
		data: data,
		context : document.body
	})
	.done(function(data) { if (done) { done(data); }})
	.fail(function() { if (fail) { fail(); }});
}

function invokePartialCheckinCart(done, fail) {
	invokePartialCheckinAncillary("cart-partial.html", false, done, fail);
}

function invokePartialCheckinInsurance(displayFeedbackStep, done, fail) {
	invokePartialCheckinAncillary("box-insurance-partial.html", displayFeedbackStep, done, fail);
}

function invokePartialCheckinSeatTeaserPax(done, fail) {
	invokePartialCheckinAncillary("box-seat-partial-teaser-passengers.html", true, done, fail);
}

function invokePartialCheckinSeatFeedback(done, fail) {
	invokePartialCheckinAncillary("box-seat-partial-feedback.html", true, done, fail);
}

function invokePartialCheckinSeatChoose(done, fail) {
	invokePartialCheckinAncillary("box-seat-partial-choose.html", true, done, fail);
}


function invokePartialCheckinExtraBaggage(displayFeedbackStep, done, fail) {
	invokePartialCheckinAncillary("box-extrabaggage-partial.html", displayFeedbackStep, done, fail);
}


/* partial refresh functions */

function refreshCheckinPartialCart(showTooltip) {
	invokePartialCheckinCart(function(data) {
		$(".checkin-partial-cart").replaceWith(data);
		initPartialCheckinCart();
		if(typeof window['initCheckIn_ancillaryTracking'] == "function"){
			initCheckIn_ancillaryTracking();
		}
		/*this.infoBox = $('.j-bookInfoBox');
		$('.j-toggleBasketAccordion').unfastClick().fastClick(function(e){
			e.preventDefault();
			book.openBasket($(this));
			
		});
		if($('.j-toggleBasketAccordion').hasClass('opened')){
			var toShow = $('.j-toggleBasketAccordion').attr('href')
			$(toShow).show();
			window.bookingHeaderStop=true;
			resizeAccordions($('.j-accordion'));
		}*/
		if (showTooltip) {
			upsellingsManager.fastShowTooltip();
		}
		$('.j-responsiveTable').responsiveTable({
			  maxWidth: 640
		});
		if (typeof preparePayment !== 'undefined' && 
				typeof preparePayment === 'function') {
			$("#cartProsegui").off('click',preparePayment);
			$("#cartProsegui").on('click',preparePayment);
		}
	});
}

function refreshCheckinPartialInsurance(displayFeedbackStep, showTooltipCart) {
	invokePartialCheckinInsurance(displayFeedbackStep, function(data) {
		$(".checkin-partial-insurance").replaceWith(data);
		upsellingsManager.initStep($(".checkin-partial-insurance").find(".j-upselling"));
		initPartialCheckinInsurance();
		upsellingsManager.scrollToSection($(".checkin-partial-insurance").find(".j-upselling"));
		refreshCheckinPartialCart(showTooltipCart);
	});
}

function refreshCheckinPartialSeat(callback) {
	invokePartialCheckinSeatTeaserPax(function(data) {
		$("#seat-map-teaser-passengers").html(data);
		invokePartialCheckinSeatFeedback(function(data) {
			$("#seat-map-feedback").html(data);
			refreshCheckinPartialCart();
			if (callback) {
				callback();
			}
		});
	});
}

function refreshCheckinPartialSeatChoose(callback) {
	invokePartialCheckinSeatChoose(function(data) {
		$("#seat-map-choose").html(data);
			refreshCheckinPartialCart();
			if (callback) {
				callback();
			}
	});
}

function refreshCheckinPartialExtraBaggage(displayFeedbackStep, showTooltipCart, callback) {
	invokePartialCheckinExtraBaggage(displayFeedbackStep, function(data) {
		$("#baggage-feedback").html(data);
		initPartialCheckinExtraBaggage();
		refreshCheckinPartialCart();
		if (callback) {
			callback();
		}
	});
}


/* common error managements functions and callbacks */

function checkinHandleInvokeError(jqXHR, textStatus, errorThrown) {
	startPageLoader(false);
	console.log('Checkin invocation failed.');
	console.log(textStatus);
	var errorMessage = getI18nTranslate('booking.service.generic.error');
	checkinDisplayErrorMessage(errorMessage);
}

function checkinHandleServiceError(data) {
	if (data.isError) {
		console.log('Checkin service error.');
		console.log(data);
		var errorMessage = "";
		if (data.errorMessage) {
			errorMessage = data.errorMessage;
		} else {
			errorMessage = getI18nTranslate('booking.service.generic.error');
		}
		if (data.errorMessageDetail) {
			errorMessage += ": " + data.errorMessageDetail;
		}
		checkinDisplayErrorMessage(errorMessage);
	} else {
		console.log("Unknown error data for checkinHandleServiceError");
	}
}

function checkinDisplayErrorMessage(errorMessage) {
	/*manage errors*/
}

function undoBackHome() {
	startPageLoader(true);
	performSubmit('checkinbackhp', {}, function(data) {
		if (data.redirect){
			window.location.replace(data.redirect);
		} else {
			startPageLoader(false);
		}
	}, function() {
		startPageLoader(false);
	});
}

function getAnalyticsCheckinParameter(paymentStepRequired){
	return "?type="+ (!paymentStepRequired ? "free" : "pay");
}

function undoCheckinAll() {
	startPageLoader(true);
	performSubmit('checkinundoall', {}, function(data) {
		if (data.redirect) {
			window.location.replace(data.redirect);
		} else {
			startPageLoader(false);
		}
	}, function() {
		startPageLoader(false);
	});
}