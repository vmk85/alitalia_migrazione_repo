"use strict";
use(function () {

    var segment = this.segment;
    var pax = this.passenger;
    var isPermitted = false;

    if (segment.passengers != null) {
        for (var x = 0; x < segment.passengers.size(); x++) {
            var pax2 = segment.passengers.get(x);
            if (pax.nome.toUpperCase() == pax2.nome.toUpperCase() && pax.cognome.toUpperCase() == pax2.cognome.toUpperCase() && pax2.checkInComplete == true) {
                isPermitted = true;
                break;
            }
        }
    }
    if (segment.passengersExtra != null) {
        for (var x = 0; x < segment.passengersExtra.size(); x++) {
            var pax2 = segment.passengersExtra.get(x);
            if (pax.nome.toUpperCase() == pax2.passenger.nome.toUpperCase() && pax.cognome.toUpperCase() == pax2.passenger.cognome.toUpperCase() && pax2.passenger.checkInComplete == true) {
                isPermitted = true;
                break;
            }
        }
    }

    return isPermitted;

});