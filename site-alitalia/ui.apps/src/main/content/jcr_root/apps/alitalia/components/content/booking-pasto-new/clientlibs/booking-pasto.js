setI18nTranslate('booking.service.generic.error');

var defaultSelectedTipoPasto = "";
$(document).ready(function() {
	var requestData = {
		'tipoPasto' : defaultSelectedTipoPasto
	};
	getDropdownData(requestData, dropdownDataSuccess, dropdownDataFail);
});	
	
function dropdownDataSuccess(data) {
	var tipiPasto = data.tipoPasto;
	
	$('.ulfoodList').each(function(index, elem) {
		//debugger;
		window.defaultSelectedTipoPasto = $(this).parent().children(".mealDefault").text();

		for (index in tipiPasto) {
			var option = '';

			 if (tipiPasto[index].code == window.defaultSelectedTipoPasto) {
				 option = '<li data-value="' + tipiPasto[index].code + '">' + tipiPasto[index].description + '</li>';
				//option = '<option value="' + tipiPasto[index].code + '" selected>' + tipiPasto[index].description + '</option>';
			 	} else {
			 		
			 		option = '<li data-value="' + tipiPasto[index].code + '">' + tipiPasto[index].description + '</li>';
			 		//option = '<option value="' + tipiPasto[index].code + '">' + tipiPasto[index].description + '</option>';
			 	}
			if(tipiPasto[index].description==kosherMealCode && showKosherMeal=="false"){
				option='';
			}
				$(this).append(option);

			}
		});
	
	$('.ulfoodListOverlay').each(function(indexelement, elem) {

		for (index in tipiPasto) {
			var elemento = '';
			elemento = '<input class="classInputOverlayEconomy Rettangolo_21" type="text" data-name="afood' + (indexelement + 1) + '"  data-id="foodselected' + (indexelement + 1) + '" data-code="' + tipiPasto[index].code  + '" value="' + tipiPasto[index].description + '" readonly>';
			 
			if(tipiPasto[index].description==kosherMealCode && showKosherMeal=="false"){
				elemento='';
			}
				$(this).append(elemento);

		}	
	});
	
		$(".dropdown-content ul li").on("click",function(){
			$(this).parent().parent().siblings(".dropbtn").text( $(this).text() );
			$(this).parent().parent().siblings(".foodSelected").val( $(this).attr("data-value") );
			
		});	
		$('.depOverlay').popup({
	        transition: 'all 0.3s'
	    });
		
		$('.classInputOverlayEconomy').on('click touchstart', function () {
	        var elem = $(this);
	        var name = elem.data("name");
	        var id = elem.data("id");
	        $('#' + name).text(this.value);
	        $('#' + id).val( $(this).attr("data-code")) ;
	    });
}

function dropdownDataFail() {
	console.log('fail retrive meals');
}

function be_cancelFood(section,done) {
	performSubmit('bookingremovemealspreferenceconsumer', '#form-booking-meals',
			function(){
				refreshInfoBoxPartial();
				if (done) {
					done();
				}
			},
			function(){
				if (done) {
					done();
				}
			}
	);
}

function be_changeFood(e,btn,selector,done) {
	validation(e, 'bookingmealsconsumer', '#form-booking-meals',
			function(data){ 
				mealsPreferenceSuccess(data,done); 
			},
			function(){
				removeErrors();
				if (done) {
					done();
				}
			}
	);
}

function mealsPreferenceSuccess(data,done) {
	if (data.result) {
		removeErrors();
		performSubmit('bookingmealsconsumer', '#form-booking-meals',
				function(){ 
					updateSummary();
					refreshInfoBoxPartial();
					slideToogle4();

					
					if (done) {
						done();
					}
				},
				function(){
					$('#errorFieldMealsChoose').text(getI18nTranslate('booking.service.generic.error'));
				}
		);
	} else {
		showErrors(data);
	}
}

function updateSummary() {
	$('#food_summary .bookingExtra__infoLine').each(function(i,elem) {
		var index = i + 1;
		var selector = "#passenger" + index;
		$(selector).text($('#food' + index + ' option:selected ').text());
	});
}

function removeErrors() {
	$('#errorFieldMealsChoose').text('');
	$('#errorFieldSummary').text('');
}