var checkinCustomizeOptions = checkinCustomizeOptions || {};
checkinCustomizeOptions.checkinWaveAncillaryFastTrack = function() {

    function successFastTrackProcessing(data) {
        $("#addFastTrackBtn").removeClass("disabled");
        if(data.isError == true) {
            //alert("Error: " + data.errorMessage);
        } else {
            $('#chkwavefasttracknotification').addClass("confirm-status").text(message_baggage_ok.replace("{0}", "X"));
            performSubmit('checkingetcart','#frmCart',loadCartSuccess,loadCartFail);
        }
        disableLoaderCheckin();

    }
    function failureFastTrackProcessing(data) {
        disableLoaderCheckin();
        $("#addFastTrackBtn").removeClass("disabled");
        if(data.isError == true) {
            // alert("Error: " + data.errorMessage);
        }
    }

    function adjustCurrentFastTrackPrice(field) {
        var totalPrice = 0;
        $('input.chkwavefasttrack[name=chkfasttrack]:checked').each(function( index ) {
            var priceTotPrice= String($(this).val());
            var totSum = String(totalPrice).split(".");
            var totAct = priceTotPrice.split(".");
            var totResultPost = (totSum[1] == undefined ? 0:parseInt(totSum[1]) ) + (totAct[1] == undefined ? 0:parseInt(totAct[1]) );
            var totResultPre = parseInt(totSum[0]) +parseInt(totAct[0]);


            totalPrice = totResultPre + (totResultPost == 0? "":"." + totResultPost);
        });

        totalPrice = String(totalPrice).split(".");
        if (totalPrice.length > 1){
            totalPrice = totalPrice[0] + (totalPrice[1] != undefined ? separatore:"") + (totalPrice[1].length > 1? totalPrice[1] : totalPrice[1] + "0" )+ " " + currency;
        } else {
            totalPrice = totalPrice[0] + (decimale2 != "" ? separatore:"") + decimale2 + " " + currency;
        }

        totalPrice = splitAdjust(totalPrice);

        $(".chkwavefasttrack.total-row .price").text(totalPrice);
    }

    $('input.chkwavefasttrack[name=chkfasttrack]').click(function(e) {
        adjustCurrentFastTrackPrice($(this));
    });

    $('#addFastTrackBtn').click(function(){
        if(!$(this).hasClass("disabled")) {
            $(this).addClass("disabled");

            var form = $("<form>").attr("id","form-checkinwave-fasttrack").appendTo("body");
            $('<input>').attr("type", "hidden").attr("name", "numeroPasseggeri").val(chkwaveData.baggage.length).appendTo(form);
            chkwaveData.fasttrack.forEach(function(item, index){

               $.each( item, function( key, value )
               {
                $('<input>').attr("type", "hidden").attr("name", key).val(value).appendTo(form);
               });
               // Se item = allow -> ff
               if (item.allow == "true")
                $('<input>').attr("type", "hidden").attr("name", "fasttrack_" + item.key).val("true").appendTo(form);
               else
                $('<input>').attr("type", "hidden").attr("name", "fasttrack_" + item.key).val($('input#chkwavefasttrack_' + item.key).is(":checked")).appendTo(form);

               /*var checkFastTrack = $('#chkwavefasttrack_' + item.idPasseggero).is(":checked") || false;

               $('<input>').attr("type", "hidden").attr("name", "nomePasseggero_"+index).val(item.nomePasseggero).appendTo(form);
               $('<input>').attr("type", "hidden").attr("name", "cognomePasseggero_"+index).val(item.cognomePasseggero).appendTo(form);
               $('<input>').attr("type", "hidden").attr("name", "idPasseggero_"+index).val(item.idPasseggero).appendTo(form);
               $('<input>').attr("type", "hidden").attr("name", "fasttrack_" + index).val(checkFastTrack).appendTo(form);*/

            });
            enableLoaderCheckin();
            performSubmit('setfasttrack', '#form-checkinwave-fasttrack', successFastTrackProcessing, failureFastTrackProcessing).promise().done(function(){
                $("#fastX").trigger( "click" );
                $("#fastXBT").trigger( "click" );
            });
        }
    });
    adjustCurrentFastTrackPrice();
}