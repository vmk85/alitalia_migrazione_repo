jQuery(document).ready(function() {
	$('#cookiesSubmit').bind('click.cookiesHandlers', function cookiesHandler(e) {
		validation(e, 'cookiessubmit', '#specialpageform_cookies',
				cookiesSuccess, cookiesError);
	});
});

function cookiesSuccess(data) {
	return (data.result ? cookiesContinue() : showErrors(data));
}

function cookiesError() {
	return cookiesContinue(true);
}

function cookiesContinue(stop) {
	removeErrors();
	$('#cookiesSubmit').removeAttr('onclick').unbind('click');
	return stop || $('#cookiesSubmit').click();
}