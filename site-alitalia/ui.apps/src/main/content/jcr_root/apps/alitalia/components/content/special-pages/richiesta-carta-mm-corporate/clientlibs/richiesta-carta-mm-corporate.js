function millemigliaCorporateHandler(e) {
	validation(e, 'mmcorporatecardsubmit',
			'#specialpageform_mmcorporatecard', millemigliaCorporateSuccess, millemigliaCorporateError);
	return false;
}

function millemigliaCorporateSuccess(data) {
	if(data.result){
		removeErrors();
		$('#mmCorporateCardSubmit').unbind('click', millemigliaCorporateHandler);
		$('#mmCorporateCardSubmit').click();
	}
	else{
		showErrors(data);
	}
}

function millemigliaCorporateError() {
	removeErrors();
	$('#mmCorporateCardSubmit').unbind('click', millemigliaCorporateHandler);
	$('#mmCorporateCardSubmit').click();
}


$(document).ready(function() {
	$('#mmCorporateCardSubmit').bind('click', millemigliaCorporateHandler);

	/*$('img#refreshCaptcha').on('click', function(){
		$('img#captchaImg').attr("src",".jcaptcha?timestamp=" + new Date().getTime());
		$('input#captcha').val('');
	});*/
});


