var overlayTimeout = false;
var offersItaly = [];
var offersWorld = [];
var orderedOffersItaly = [];
var orderedOffersWorld = [];
var departureCityCode;

jQuery(document).ready(function() {
	var idLabelOffersSelector= jQuery('#speacialOfferSelect_label');
	var cityFromUrl = getParameterByName("from");
	jQuery(idLabelOffersSelector).replaceWith(
			'<label id="speacialOfferSelect_label" for="speacialOfferSelect" class="specialOffers__headerTitle">'+$(idLabelOffersSelector).text()+'</label>');
	
	var dataListCityForOffers = {};
	dataListCityForOffers.dest = null;
    if(cityFromUrl != ""){
		dataListCityForOffers = {
            dest : cityFromUrl.toUpperCase()
        };
    } 
	else if (typeof localStorage !== "undefined"
			&& typeof (Storage) !== "undefined") {
		try {
			var items = localStorage.getItem("ricercheRecenti");
			if (items != null && items != 'undefined'
					&& items != '[]' && items.length > 0) {
				items = JSON.parse(items);
				var airportCode = items[0].cittaPartenza.codiceAeroporto;
				dataListCityForOffers = {
					dest : airportCode
				};
			}
		} catch (e) {
		}
	}
	showOverlay();
	getCitiesForOffers(dataListCityForOffers);
	$("input[name='alfabetic-price-italia']").change(function(evt){
		evt.stopImmediatePropagation();
		sortOffers("partialContentListOfferteTab1", 
				$(this).val() == "price" ? "P" : "A");
	});
	$("input[name='alfabetic-price-mondo']").change(function(evt){
		evt.stopImmediatePropagation();
		sortOffers("partialContentListOfferteTab2", 
				$(this).val() == "price" ? "P" : "A");
	});
	return false;
});

function getCitiesForOffers(dataListCity) {
	offerType = jQuery("#offersType").text();
	isYoungPar = offerType == "G";
	paxTypePar = isYoungPar ? "yg" : "ad";
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	$.ajax({
		url : url + ".lista-city-partial.html",
		data : {
			destination : dataListCity.dest,
			isHomePage : false,
			isYoung : isYoungPar,
			paxType : paxTypePar
		},
		context : document.body
	})
	.done(successCitiesForOffers)
	.fail(failCitiesForOffers);
}

function successCitiesForOffers(dataCity) {
	jQuery('#partialListCitiesForOffers').empty();
	jQuery('#partialListCitiesForOffers').append(dataCity);
	var data = {
		dest : jQuery('#speacialOfferSelect').val()
	};
	jQuery('#speacialOfferSelect').bind('change', function(e) {
		data.dest = jQuery('#speacialOfferSelect').val();
		showOverlay();
		$("#alfabeticoItalia").click();
		changeDepartureCity(data.dest);
		getOffersDefaultCityForOffers(data);
	});
	showOverlay();
	changeDepartureCity(data.dest);
	getOffersDefaultCityForOffers(data);
	return false;
}

function failCitiesForOffers() {
	return false;
}

function getOffersDefaultCityForOffers(data) {
	offerType = jQuery("#offersType").text();
	isYoungPar = offerType == "G";
	paxTypePar = isYoungPar ? "yg" : "ad";
	tab1TopEditAreaContent = jQuery.trim(jQuery(
			"#editorialAreaItalia").children("p")
			.text());
	tab2TopEditAreaContent = jQuery.trim(jQuery(
			"#editorialAreaMondo").children("p")
			.text());
	istab1TopEditAreaContentEmpty = (tab1TopEditAreaContent.length == 0);
	istab2TopEditAreaContentEmpty = (tab2TopEditAreaContent.length == 0);
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	$.ajax({
		url : url + ".lista-offerte-speciali-partial." + data.dest + ".html",
		data : {
			from : data.dest,
			isHomePage : false,
			tab1AsteriskInPrice : !istab1TopEditAreaContentEmpty,
			tab2AsteriskInPrice : !istab2TopEditAreaContentEmpty,
			isYoung : isYoungPar,
			paxType : paxTypePar
		},
		context : document.body
	})
	.done(successListSpecialOffer)
	.fail(failListSpecialOffer)
	.always(hideOverlay);
	return false;
}

function successListSpecialOffer(data) {
	populateOffers(data);
	return false;
}

function failListSpecialOffer() {
	return false;
}

function populateOffers(data) {
	if (data) {
		var contTab1 = 0;
		var contTab2 = 0;
		$("#partialContentListOfferteTab1").empty();
		$("#partialContentListOfferteTab2").empty();
		$(data).find("div.serviceOffersList").each(function(i, d) {
			var selector = "";
			if (i === 0 && (
					$(d).find("li.destinationItem").first().attr("data-area") == "ITALIA" || 
					jQuery("#offersType").text() === 'G')) {
				selector = '#partialContentListOfferteTab1';
				selectorId = 'partialContentListOfferteTab1';
				contTab1 = $(d).find("li.destinationItem").length;
			} else {
				selector = '#partialContentListOfferteTab2';
				selectorId = 'partialContentListOfferteTab2';
				contTab2 = $(d).find("li.destinationItem").length;
			}
			if (d) {
				jQuery(selector).replaceWith(
					'<ul id="' + selectorId + '" class="specialOffers__list">'
					+ $(d).html() + ' </ul>');
				jQuery(selector).replaceWith(
					'<ul id="' + selectorId + '" class="specialOffers__list">'
					+ $(d).html() + ' </ul>');
			} else {
				jQuery(selector).replaceWith(
					'<div id="' + selectorId
					+ '" class="specialOffers__list"></div>');
				jQuery(selector).replaceWith(
					'<div id="' + selectorId
					+ '" class="specialOffers__list"></div>');
			}
		});
		var min = jQuery("#filtersMinNum").text();
		if (!isNaN(min)) {
			if (contTab1 < min) {
				jQuery("#filtri_offerte_tab1").hide();
			} else {
				jQuery("#filtri_offerte_tab1").show();
			}
		}
		if (!isNaN(min)) {
			if (contTab2 < min) {
				jQuery("#filtri_offerte_tab2").hide();
			} else {
				jQuery("#filtri_offerte_tab2").show();
			}

		}
		/*Controllo se tab Italia selezionato e' vuoto*/
	    if($(".tabActive #partialContentListOfferteTab1").length > 0 
	    		&& $("#partialContentListOfferteTab1 li").length == 0){
	        $("#specialOffers__tab--2 a").click();
	    }
	    /*Controllo se tab Mondo selezionato e' vuoto*/
	    if($(".tabActive #partialContentListOfferteTab2").length > 0
	    		&& $("#partialContentListOfferteTab2 li").length == 0){
	        $("#specialOffers__tab--1 a").click();
	    }
	}
	sortOffers("partialContentListOfferteTab1");
	sortOffers("partialContentListOfferteTab2");
	return false;
}

function hideOverlay() {
	if (overlayTimeout) {
		jQuery('.overlayLoading').hide();
	} else {
		setTimeout(function() {
			jQuery('.overlayLoading').hide();
		}, 1000);
	}
	setTimeout(function() {
		jQuery('.overlayLoading').hide();
	}, 20000);
	overlayTimeout = false;
	return false;
}

function showOverlay() {
	overlayTimeout = false;
	jQuery('.tabActive .overlayLoading').show();
	setTimeout(function() {
		overlayTimeout = true;
	}, 1000);
	return false;
}

function changeDepartureCity(dest) {
	if (typeof all_airports !== 'undefined' && dest) {
		jQuery.each(all_airports, function(i, v) {
			if (v.code.search(new RegExp(dest, "i")) != -1) {
				selectedAirport = copyValue(v);
			}
		});
		if (typeof selectedAirport !== 'undefined') {
			jQuery("input#Origin").attr("value", selectedAirport.code);
			jQuery('#originKeyCountryCode')
					.text(selectedAirport.keyCountryCode);
			jQuery('#originKeyAirportName')
					.text(selectedAirport.keyAirportName);
			jQuery('#originKeyCityCode').text(selectedAirport.keyCityCode);
			jQuery("div.customInput--flightFinder.departure").find(".apt")
					.empty();
			jQuery("div.customInput--flightFinder.departure").find(".apt")
					.append(selectedAirport.city);
			jQuery("div.customInput--flightFinder.departure").find(".city")
					.empty();
			jQuery("div.customInput--flightFinder.departure").find(".city")
					.append(selectedAirport.code);
		}
	} else if (typeof all_airports === 'undefined') {
		setTimeout(function() {
			changeDepartureCity(dest)
		}, 1000);
	}
	return false;
}

function sortOffers(containerId, order){
	var $container = $("#"+containerId);
	if(!order) {
		order = $('#orderFilter').val();
	}
	var attrPrice  = "data-price";
	var attrName = "data-name";
	
	var result = $($container.children()).sort(function (a, b) {
	      var contentA_name =$(a).attr(attrName);
	      var contentA_price = parseFloat($(a).attr(attrPrice).replace(/ /g, "").replace(/\./g, "").replace(/,/g, "."));
	      var contentB_name =$(b).attr(attrName);
	      var contentB_price = parseFloat($(b).attr(attrPrice).replace(/ /g, "").replace(/\./g, "").replace(/,/g, "."));
	      var confronto = 0;
	      if(order == "P"){
		      confronto = (contentA_price < contentB_price) ? -1 
		    		  : (contentA_price > contentB_price) ? 1 
		    				  : (contentA_name < contentB_name) ? -1 
		    						  : (contentA_name > contentB_name) ? 1 
		    			    				  : 0;
	      }
	      else{
		      confronto = (contentA_name < contentB_name) ? -1 
		    		  : (contentA_name > contentB_name) ? 1 
		    				  : (contentA_price < contentB_price) ? -1 
		    						  : (contentA_price > contentB_price) ? 1 
		    			    				  : 0;
	      }
	      return confronto;
	   });
	$container.html(result);
	
}

