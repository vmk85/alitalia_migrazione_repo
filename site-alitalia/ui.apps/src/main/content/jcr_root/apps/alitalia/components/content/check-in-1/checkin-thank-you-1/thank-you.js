"use strict";
use(function () {

    var segments = this.segments;

    var segment = this.segments.get(0);

    var moreSegments = segments.size() > 1 ? true : false;

    var dt = segment.originalDepartureDate.split("T")[0];

    // creo la lista dei paggeri che hanno effettuato il checkin e possono scaricare la carta d'imbarco
    var passengersAllowed = [];
    var passengersCheckinDone = [];

    for (var i=0; i<segment.passengers.size(); i++){
        var passenger = segment.passengers.get(i);
        if(passenger.checkInComplete == true && passenger.selectee == false){
            passengersAllowed.push(passenger);
        }
    }

    var totalPassengersCheckinDone = 0;

    for (var i=0; i<segment.passengers.size(); i++){
        var passenger = segment.passengers.get(i);
        if(passenger.checkInComplete == true){
            passengersCheckinDone.push(passenger);
            totalPassengersCheckinDone++;
        }
    }

    return {
        dt: dt,
        moreSegments: moreSegments,
        passengersAllowed : passengersAllowed,
        passengersCheckinDone : passengersCheckinDone,
        totalPassengersCheckinDone : totalPassengersCheckinDone
    };

});