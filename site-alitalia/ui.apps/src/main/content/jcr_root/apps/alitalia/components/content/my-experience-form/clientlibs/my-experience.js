function myexpInit(edit) {
	myexpReady();
	initSelectA();
	
	$("#selectA").change(function(){
		emptySelectValues("#selectC");
		if ($(".myexp__step2 .myexpAccordion").hasClass("active")) {
			myexp_hideStep(2);
		}
		if ($(".myexp__step3 .myexpAccordion").hasClass("active")) {
			myexp_hideStep(3);
		}
		if ($(".myexp__step4 .myexpAccordion").hasClass("active")) {
			myexp_hideStep(4);
		}
		
		var values = [];
		if ($(this).val()) {
	        $.each(selectValues[$(this).val()], function(key){
	        	values.push(key);
	        });
	        replaceSelectValues("#selectB", values);
		} else {
			emptySelectValues("#selectB");
		}
        
	});
	
	$("#selectB").change(function(){
		if ($(this).val()) {
			replaceSelectValues("#selectC", selectValues[$("#selectA").val()][$(this).val()]);
		} else {
			emptySelectValues("#selectC");
		}
	});
	
	if (!edit) {
		retrieveCountries();
		retrieveFFTypes();
		
		$(".j-myexp-step2-submit").off("click");
		$(".j-myexp-step3-submit").off("click");
		$(".j-myexp-new-submit").off("click");
		$(".j-myexp-update-submit").off("click");
		
		$(".j-myexp-step2-submit").on("click", myexpValidateStep2);
		$(".j-myexp-step3-submit").on("click", myexpValidateStep3);
		$(".j-myexp-new-submit").on("click", myexpValidateNew);
		$(".j-myexp-update-submit").on("click", myexpValidateUpdate);
	}
}

function initSelectA() {
	var values = [];
    $.each(selectValues, function(key){
    	values.push(key);
    });
    replaceSelectValues("#selectA", values);
}

function replaceSelectValues(target, values) {
	var select = $(target);
	select.empty();
	select.append("<option value=''>" + selectDefaultValue + "</option>");
	for (index in values) {	
		var option = '';
		option = "<option value='" + values[index] + "'>" + values[index] + "</option>";;
		select.append(option);
	}
}

function emptySelectValues(target) {
	replaceSelectValues(target, []);
}

function retrieveCountries() {
	var requestData = {
			'countries': "true",
	};
	getDropdownData(requestData, retrieveCountriesSuccess);
}

function retrieveCountriesSuccess(data) {
	var countries = data.countries; 
	for (index in countries) {
		
		var option = '';
		option = '<option value="' + countries[index].code + '">' + countries[index].description + '</option>';
		$('#country').append(option);
	}
}

function retrieveFFTypes() {
	var requestData = {
			'frequentFlyerTypes': "true",
	};
	getDropdownData(requestData, retrieveFFTypesSuccess);
}

function retrieveFFTypesSuccess(data) {
	var ffTypes = data.frequentFlyerTypes; 
	for (index in ffTypes) {
		
		var option = '';
		option = '<option value="' + ffTypes[index].code + '">' + ffTypes[index].description + '</option>';
		$('#airlineProgram').append(option);
	}
}

function myexpValidateStep2(e) {
	$("#step").val(2);
	$(".j-myexp-step2-submit").off("click", myexpValidateStep2);
	$(".j-myexp-step2-submit").addClass("isDisabled");
	validation(e, 'myexperience', '#myExperienceForm',
		myexpValidateStep2Success,
		function() {
			$(".j-myexp-step2-submit").on("click", myexpValidateStep2);
	});
}

function myexpValidateStep2Success(data) {
	removeErrors();
	if (data.result) {
		myexp_showStep(3);
	} else {
		showErrors(data, false, true);
	}
	$(".j-myexp-step2-submit").on("click", myexpValidateStep2);
}

function myexpValidateStep3(e) {
	$("#step").val(3);
	$(".j-myexp-step3-submit").off("click", myexpValidateStep3);
	$(".j-myexp-step3-submit").addClass("isDisabled");
	validation(e, 'myexperience', '#myExperienceForm', 
		myexpValidateStep3Success,
		function() {
			$(".j-myexp-step3-submit").on("click", myexpValidateStep3);
	});
}

function myexpValidateStep3Success(data) {
	removeErrors();
	if (data.result) {
		myexp_showStep(4);
	} else {
		showErrors(data, false, true);
	}
	$(".j-myexp-step3-submit").on("click", myexpValidateStep3);
}

function myexpValidateNew(e) {
	$("#step").val("");
	$(".j-myexp-new-submit").off("click", myexpValidateNew);
	$(".j-myexp-new-submit").addClass("isDisabled");
	validation(e, 'myexperience', '#myExperienceForm',
		myexpValidateNewSuccess,
		function() {
			$(".j-myexp-new-submit").on("click", myexpValidateNew);
	});
}

function myexpValidateNewSuccess(data) {
	removeErrors();
	if (data.result) {
		performSubmit('myexperience', '#myExperienceForm', function(data) {
			if (!data.isError && data.result) {
				myexp_showFeedback();
			}
			$(".j-myexp-new-submit").on("click", myexpValidateNew);
			startPageLoader(false);
		}, function() {
			$(".j-myexp-new-submit").on("click", myexpValidateNew);
			startPageLoader(false);
		});
	} else {
		showErrors(data, false, true);
		$(".j-myexp-new-submit").on("click", myexpValidateNew);
	}
}

function myexpValidateUpdate(e) {
	$("#step").val("");
	$(".j-myexp-update-submit").off("click", myexpValidateUpdate);
	$(".j-myexp-update-submit").addClass("isDisabled");
	validation(e, 'myexperience', '#myExperienceForm', 
		myexpValidateUpdateSuccess,
		function() {
			$(".j-myexp-update-submit").on("click", myexpValidateUpdate);
	});
}

function myexpValidateUpdateSuccess(data) {
	removeErrors();
	if (data.result) {
		startPageLoader(true);
		performSubmit('myexperience', '#myExperienceForm', function(data) {
			if (!data.isError && data.result) {
				myexp_showFeedback();
			}
			$(".j-myexp-update-submit").on("click", myexpValidateUpdate);
			startPageLoader(false);
		}, function() {
			$(".j-myexp-update-submit").on("click", myexpValidateUpdate);
			startPageLoader(false);
		});
	} else {
		showErrors(data, false, true);
		$(".j-myexp-update-submit").on("click", myexpValidateUpdate);
	}
}
