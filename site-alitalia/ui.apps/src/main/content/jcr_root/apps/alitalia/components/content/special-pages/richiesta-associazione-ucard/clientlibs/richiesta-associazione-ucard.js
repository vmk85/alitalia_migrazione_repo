$(document).ready(function() {
$("#ucardassociationsubmit").bind('click', millemigliaUCardSubmit);
checkIsSuccess();
whiteBackgroundHeight();
});

function whiteBackgroundHeight() {
    var formPageHeight = $('.formPage').outerHeight(true);
    var formHeight = $('#specialpageform_ucard').outerHeight(true);
    var whiteHeight = formPageHeight - formHeight + 30;
    $('.white-background').css('height', whiteHeight);
}

function millemigliaUCardSubmit(e) {
    e.preventDefault();
    if (formIsCompleted() && ($("#checkReCaptcha").val() == "true")) {
        if (headerWaveRecaptchaIDSA === undefined) {
            $("#recaptchaResponseUcard").val("true");
            headerWaveRecaptchaIDSA = grecaptcha.render('submit-feedback-ucard',{
                'sitekey':invisibleRecaptchaSiteKey,
                'callback':"sendForm",
                'size':"invisible"
            });
        } else {
            grecaptcha.reset();
        }
        grecaptcha.execute(headerWaveRecaptchaIDSA);
	} else if (formIsCompleted() && ($("#checkReCaptcha").val() != "true")) {
	    sendForm();
	}
}

function sendForm() {
    enableLoaderMyAlitalia();
    $("#ucardassociationsubmit").unbind('click', millemigliaUCardSubmit);
    $("#ucardassociationsubmit").click();
}

function checkIsSuccess() {
    var url = window.location.href;
    var success = url.indexOf("success=true&from=ucardassociationsubmit");
    var failure = url.indexOf("success=false");
    if (success != -1) {
        millemigliaUCardSuccess();
        whiteBackgroundHeight();
        url = url.split("?")[0];
        window.history.pushState({"pageTitle":document.title},"", url);
    } else if (failure != -1) {
        millemigliaUCardError();
        whiteBackgroundHeight();
        url = url.split("?")[0];
        window.history.pushState({"pageTitle":document.title},"", url);
    }
}

function formIsCompleted() {
    if (validateMMcode()) {
        if (validateNameIsFull()) {
            if (validateSurnameIsFull()) {
                if(isEmailValid()) {
                    if (validateUcode()) {
                        if (validatePrivacyIsFull()) {
                            if (validateEmployeeIsFull()) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                       return false;
                    }
                } else {
                    return false;
                }
            } else {
                $("html, body").delay(200).animate({scrollTop: $('#appl_surname').offset().top }, 500);
                return false;
            }
        } else {
            $("html, body").delay(200).animate({scrollTop: $('#appl_name').offset().top }, 500);
            return false;
        }
    } else {
        $("html, body").delay(200).animate({scrollTop: $('#mm_code_descr').offset().top }, 500);
        return false;
    }
}

function millemigliaUCardSuccess() {
    $('.formPage__sectionBig').hide();
    $('.ucard-success').show();
    disableLoaderMyAlitalia();
}

function millemigliaUCardError() {
   $('.formPage__sectionBig').hide();
   $('.ucard-failure').show();
   disableLoaderMyAlitalia();
}

function validateMMcode() {
    var MMcode = $("#mm_code").val();
    $("#mmCodeError").addClass("hide");
    $("#mmCodeErrorChar").addClass("hide");
    if(MMcode.length > 0) {
        $("#mm_code").removeClass("isErrore");
        $("#mmCodeError").addClass("hide");
        $("#mmCodeErrorChar").addClass("hide");
        if (MMcode.length <= 10) {
            $("#mmCodeErrorChar").addClass("hide");
            return true;
        } else {
            $("#mm_code").addClass("isErrore");
            $("#mmCodeErrorChar").removeClass("hide");
            return false
        }
    } else {
        $("#mm_code").addClass("isErrore");
        $("#mmCodeError").removeClass("hide");
        return false;
    }
}

function validateNameIsFull() {
        var name = $("#appl_name").val();
        $("#nameError").addClass("hide");
        $("#nameErrorChar").addClass("hide");
    if (name.length > 0) {
        $("#appl_name").removeClass("isErrore");
        $("#nameError").addClass("hide");
        $("#nameErrorChar").addClass("hide");
        if (isAlphabeticWithAccentAndSpaces(name)) {
            $("#nameErrorChar").addClass("hide");
            return true;
        } else {
            $("#appl_name").addClass("isErrore");
            $("#nameErrorChar").removeClass("hide");
            return false
        }
    } else {
        $("#appl_name").addClass("isErrore");
        $("#nameError").removeClass("hide");
        return false;
    }
}

function validateSurnameIsFull() {
        var surname = $("#appl_surname").val();
        $("#surnameError").addClass("hide");
        $("#surnameErrorChar").addClass("hide");
    if (surname.length > 0) {
        $("#appl_surname").removeClass("isErrore");
        if (isAlphabeticWithAccentAndSpaces(surname)) {
        $("#surnameErrorChar").addClass("hide");
        return true;
        } else {
            $("#appl_surname").addClass("isErrore");
            $("#surnameErrorChar").removeClass("hide");
            return false
        }
    } else {
        $("#appl_surname").addClass("isErrore");
        $("#surnameError").removeClass("hide");
        return false;
    }
}

function validateUcode() {
    var pnr = $("#university_code").val();
    if (pnr.length > 0 && pnr.length <= 6) {
        $("#university_code").removeClass("isErrore");
        $("#uCodeErrorEmpty").addClass("hide");
        $("#uCodeErrorLong").addClass("hide");
        return true;
    } else if (pnr.length == 0){
        $("#university_code").addClass("isErrore");
        $("#uCodeErrorEmpty").removeClass("hide");
        $("#uCodeErrorLong").addClass("hide");
        return false;
    } else if (pnr.length > 6) {
        $("#university_code").addClass("isErrore");
        $("#uCodeErrorLong").removeClass("hide");
        $("#uCodeErrorEmpty").addClass("hide");
    }
}

function isEmailValid() {
  var email = $("#appl_email").val();
  if (email != "") {
      $("#appl_email").removeClass("isErrore");
      $("#emailError").addClass("hide");
      $("#emailErrorChar").addClass("hide");
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      if(regex.test(email)) {
        $("#appl_email").removeClass("isErrore");
        $("#emailErrorChar").addClass("hide");
        return true;
      } else {
        $("#appl_email").addClass("isErrore");
        $("#emailErrorChar").removeClass("hide");
        return false;
      }
  } else {
      $("#appl_email").addClass("isErrore");
      $("#emailError").removeClass("hide");
      return false;
  }
}

function validatePrivacyIsFull() {
    if ( $('input[name="auth-1"]').is(':checked') ) {
        $("#privacyError").addClass("hide");
        return true;
    } else {
        $("#privacyError").removeClass("hide");
        return false;
    }
}

function validateEmployeeIsFull() {
    if ( $('input[name="auth-2"]').is(':checked') ) {
        $("#employeeError").addClass("hide");
        return true;
    } else {
        $("#employeeError").removeClass("hide");
        return false;
    }
}