var newsletterOptions = {};
// document ready
$( document ).ready( function() {
    window.onload = function(){
        var text = $('.millemiglia__formFeedbackText').text();
        var comparingText = '';
        if(text !== comparingText){
            document.getElementById("newsletter-email").focus();
        }
    };
    $('#newsletter-email').on('focus',function(){
        $('.grecaptcha-badge').hide();
        if ($('.newsletter_FeedbackSuccess').html().trim()!="" || ($('.newsletter_feedback_error').html().trim()!="" && $('.newsletter_feedback_error').html().trim()!= $(".input-dati-error").html()) ){
            $('#newsletter-email').val("");
            $('.newsletter_FeedbackSuccess').html("");$('.newsletter_feedback_error').html("");
        }
    });
    $( "#checkbox1" ).change(function() {
            if($('#checkbox1').is(':checked')){$('.newsletter-error.newsletter-checkbox').hide();
        }
    });
    newsletterOptions.captcha();
} );

var newsletterSubscribeHandler=function newsletterSubscribeHandler(e) {
    grecaptcha.reset();
    //performSubmit('newsletter-subscribe', '#newsletter-subscribe', newsletterSubscribeSuccess, newsletterSubscribeError);
    performSubmit('newsletter-subscribe-crm', '#newsletter-subscribe', newsletterSubscribeSuccess, newsletterSubscribeError);

};
function newsletterSubscribeSuccess(data) {
    console.log("data",data);
    // $('.loader-wrap').removeClass('loading');
    if (data.isError){
        /*if (~data.errorMessage.indexOf("sendNewMailingListRegistrationMail")){
            $(".newsletter_feedback_message .newsletter_FeedbackSuccess").html($(".newsletter-subscription-ok").css("display", "block"));
            $(".newsletter_feedback_message .newsletter_feedback_error").html($('.newsletter-subscription-error-emailNotSent').html()).addClass('feedback-error').css("display", "block");
            $('.newsletter_feedback_message').show();
        }else{
            if (~data.errorMessage.indexOf("already exist")){
                $(".newsletter_feedback_message .newsletter_feedback_error").html($('.specialpage-subscribenewsletter-error-exists').html()).addClass('feedback-error').css("display", "block");
                $('.newsletter_feedback_message').show();

            }else{
                $(".newsletter_feedback_message .newsletter_feedback_error").html($(".newsletter-subscription-generic-error").html()).addClass('feedback-error').css("display", "block");
                $('.newsletter_feedback_message').show();
            }
        }*/
        //$(".newsletter_feedback_message .newsletter_feedback_error").html($(".newsletter-subscription-generic-error").html()).addClass('feedback-error').css("display", "block");
        $(".newsletter_feedback_message .newsletter_feedback_error").html(data.errorMessage).addClass('feedback-error').css("display", "block");
        $('.newsletter_feedback_message').show();
    }else{
        // $(".newsletter_feedback_message .newsletter_FeedbackSuccess").html($(".newsletter-subscription-ok ").css("display", "block"));
        // $('.newsletter_feedback_message').show();
        $('.grecaptcha-badge').hide();

        window.location.href = data.baseURL + "myalitalia/myalitalia-registrati.html?success=true&newsletter=true";
    }
    $('.grecaptcha-badge').hide();
}
function newsletterSubscribeError() {
    $('.grecaptcha-badge').hide();
    return newsletterSubscribeContinue(true);
}
function newsletterSubscribeContinue(stop) {
    removeErrors();
    $('#newsletter-submit').off('click');
    return stop || setAnalyticsPropertyAndSubmit();
}
function setAnalyticsPropertyAndSubmit(){
    var subscribedEmail = jQuery("#email").val();
    setLoginContextProfileProperty('subscribedEmail', subscribedEmail);
    $('#newslettersubSubmit').click();
}
newsletterOptions.captcha = function() {
    $('#newsletter-submit').click(function(e) {
        e.preventDefault();
        var el = $(this);
        enableLoader(el);
        $('#newsletter-subscribe .feedback-error').hide();
        $('.feedback-error.newsletter-checkbox').hide();
        $('.newsletter_FeedbackSuccess').html("");
        //$('.newsletter_feedback_error').html("");
        $('.newsletter_feedback_message').hide();
        $('#checkbox1').removeClass('is-invalid-input');
        $('#newsletter-email').removeClass('is-invalid-input');
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(String(document.getElementById('newsletter-email').value).toLowerCase()) || !$('#checkbox1').is(':checked')) {
            if (!$('#checkbox1').is(':checked')) {
                $('.newsletter-checkbox').addClass('feedback-error');
                $('.newsletter-error').addClass('feedback-error');
                $('.feedback-error.newsletter-checkbox').html($('.checked-error').css("display", "block"));
                $('.feedback-error.newsletter-checkbox').show();
            }
            if (!re.test(String(document.getElementById('newsletter-email').value).toLowerCase())) {
                $('#newsletter-email').addClass('is-invalid-input');
                $('.feedback-error:not(.newsletter-checkbox)').html($('.input-dati-error').html()).css("display", "block");
                $('.feedback-error:not(.newsletter-checkbox)').show();
            }
            disableLoader(el);
        }
        else { 
            // se ok validazione, recaptcha invisible e chiamata post
            try {
                var widgetRecaptcha = grecaptcha.render('submit-feedback-newsletter', {
                    'sitekey': "6Ldhxj0UAAAAACMfJpZyd7BKzU9ocGMd1W-d2bWw",
                    'callback': "newsletterSubscribeHandler",
                    'size': "invisible"
                });
            } catch (e) {  }
            grecaptcha.execute(widgetRecaptcha);
        }
    });
};