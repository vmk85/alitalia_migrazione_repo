// integrazione script vecchio
$( document ).ready( function() {
	$('.icon.icon--it').html('<span class="icon-icon-it"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>');
	$('.icon.icon--us').html('<span class="icon-icon-us"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>');
	$('.icon.icon--uk').html('<span class="icon-icon-uk"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>');
	$('.icon.icon--fr').html('<span class="icon-icon-fr"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>');
	$('.icon.icon--es').html('<span class="icon-icon-es"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>');

	if (typeof countries != 'undefined' && typeof urls != 'undefined' && countries && urls) {
		var selectedCountry;
		var selectedLanguage;

		// nuova logica impostazione etichetta menù
		// var $userMenuLanguage = $('.userMenu__language').find('span.userMenu__text');
		var $userMenuLanguage = $('.lang-selection');

		$('.languageMenu-country').empty();

		if (countries.countries && countries.countries.length > 0) {

			for (var i = 0; i < $(countries.countries).length; i++) {
				var selected = '';
				if (i === countries.countrySelected) {
					selected = 'selected';
					selectedCountry = countries.countries[i].name;
				}
				$('.languageMenu-country').append('<option ' + selected
					+ ' value="' + countries.countries[i].code + '">'
					+ countries.countries[i].name
					+ '</option>'
				);
			}
			var options = $('.languageMenu-country option');
			$('.languageMenu-country option').removeAttr('selected');
            var arr = options.map(function(_, o) { return { t: $(o).text(), v: o.value }; }).get();
            arr.sort(function(o1, o2) { return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0; });
            options.each(function(i, o) {
              o.value = arr[i].v;
              $(o).text(arr[i].t);

            });
            $('.languageMenu-country option:contains('+selectedCountry+')').prop('selected','selected');
			
			$('.languageMenu-country').bind('change', function() {
			function getIndex(val) {
				console.log("val ", val);
			    for (var i = 0; i < countries.countries.length; i++) {
			    	console.log("countries.countries[i] ",countries.countries[i]);
			        if (countries.countries[i].name === val) {
			            return i;
			        }
			    }
			}
			var indexC = getIndex($(".languageMenu-country option:selected").text());
				$('.languageMenu-language').empty();
				for (var i = 0; i < $(countries.countries[indexC].languages).length; i++) {
					$('.languageMenu-language').append('<option value="'
						+ countries.countries[indexC].languages[i].code + '">'
						+ countries.countries[indexC].languages[i].name
						+ '</option>');
				}
			});
		}

		$('.languageMenu-language').empty();

		if (countries.countries && countries.countries.length > 0) {

			for (var i = 0; i < $(countries.countries[countries.countrySelected].languages).length; i++) {
				var selected = '';
				if (i === countries.languageSelected) {
					selected = 'selected';
					selectedLanguage = countries.countries[countries.countrySelected].languages[i].name;
				}
				$('.languageMenu-language').append('<option ' + selected
					+ ' value="' + countries.countries[countries.countrySelected].languages[i].code + '">'
					+ countries.countries[countries.countrySelected].languages[i].name
					+ '</option>');
			}

			$('.confirmButton').bind('click', function() {
				countryCode = $('.languageMenu-country').find(':selected').val();
				languageCode = $('.languageMenu-language').find(':selected').val();
				countryLangCode = languageCode + '_' + countryCode;
				urlDirect = urls[countryLangCode];

				if ($('.rememberMeLang').is(':checked')) {
					try {
						if (typeof (Storage) !== "undefined"
								&& typeof (localStorage) !== "undefined") {
							var languageObject = {
								country: $('.languageMenu-country')
									.find(':selected').text(),
								language: $('.languageMenu-language')
									.find(':selected').text()
							}
							languageObject = JSON.stringify(languageObject);
							localStorage.setItem('languageObject', languageObject);
						}
					} catch (e) {}

					var expires = new Date();
			        expires.setTime(expires.getTime() + (10 * 24 * 60 * 60 * 365 *1000));
			        document.cookie= 'alitalia_consumer_locale' + "=" + countryCode+'_'+languageCode + '; expires='+ expires.toUTCString() + "; path=/";
				}

				window.location = urlDirect;
			});
		}

		// nuova logica impostazione etichetta menù
		// $userMenuLanguage.empty();
		// $userMenuLanguage.append(selectedCountry + ' - ' + selectedLanguage);
		$userMenuLanguage.find('.country, .lang').empty();
		$userMenuLanguage.find('.country').append(selectedCountry);
		$userMenuLanguage.find('.lang').append(selectedLanguage);

	}
} );