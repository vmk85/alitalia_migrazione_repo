var headerNotificationOptions = {};

$( document ).ready( function() {
    var numNotif;
    headerNotificationOptions.openPanel();
    headerNotificationOptions.closePanelDocument();
    headerNotificationOptions.closePanelhover();
    $('.notification-single-item.notification-unread .icon--close--black').on('click',function(e){
        headerNotificationOptions.deleteNotification($(this).parent());
    });
    $('.notification-single-item.notification-unread span').on('click',function(){
        headerNotificationOptions.readNotification($(this));
    });
    var notificationUnread = $('.notification-cta .notification-single-item.notification-unread').length;
    var notificationsId="notificationsId";
    var notifClosed = 0;
    $(".notification-cta .notification-single-item.notification-unread").each(function(index) {
        $(this).attr("id", this.id + notificationsId+index);
        var closed =localStorage.getItem(notificationsId+index);
        if (closed=="closed"){
            $(this).hide();
            $(this).addClass('closed');
        }
    });
    var countNotifications = $("li.notification-single-item:not(is-submenu-item.is-dropdown-submenu-item)").length - 1;
    if (typeof(Storage) !== "undefined") {
        var numNotif = 0;
        $('.notification-single-item.notification-unread:not(is-submenu-item.is-dropdown-submenu-item)').each(function(i, obj) {
            if( $(obj).parent().parent().hasClass('mobile-notify-wrap') == false ){
                numNotif++;
            }
        });
        /*Aggiunto try catch per gestire errore su Dynatrace: Access is denied*/
        try {
        	localStorage.notifications = numNotif;
        } catch (e) {
        	console.log('Cannot retrive notification from localStorage');
        }
        
    }
    function allStorage() {
        var archive = {}, // Notice change here
            keys = Object.keys(localStorage),
            i = keys.length;
        while ( i-- ) {
            archive[ keys[i] ] = localStorage.getItem( keys[i] );
        }
        return archive;
    }
    var locStorage=allStorage();
    $.each(locStorage, function(i, value) {
        if(i.substr(0,13)=="notificationsId" && value=="read"){
            //if(i.startsWith("notificationsId")==true && value=="read"){  //startsWith non funziona su IE
            var thisId="#"+i
            $(thisId).removeClass("notification-unread");
        }
        if(i.substr(0,13)=="notificationsId" && value=="closed"){
            //if(i.startsWith("notificationsId")==true && value=="closed"){  //startsWith non funziona su IE
            var thisId="#"+i;
            var index = parseInt(i.slice(15))+2;
            $('.notificationMenu__list.menu.device li:nth-child('+index+')').hide();
        }
    });
    //numNotif = $('.notification-single-item.notification-unread:not(.is-submenu-item):not(.closed)').length;
    numNotif = 0;
    $('.notification-single-item.notification-unread:not(.is-submenu-item):not(.closed)').each(function(i, obj) {
        if( $(obj).parent().parent().hasClass('mobile-notify-wrap') == false ){
            numNotif++;
        }
    });
    $("#header .notification-count").html(numNotif);
    if (numNotif <= 0 ){
        $(".notification-count").css('display','none');
        var notificationEmpty = 0;
        $('.notification-cta .notification-single-item').each(function(){
            if($(this).hasClass('closed')){
                notificationEmpty ++;
            }
        });
        if (notificationEmpty == $('.notification-cta .notification-single-item:not(.no-notification)').length){
            $(".no-notification").css('display','block');
        }else{
            $(".no-notification").css('display','none');
        }
    }
    headerNotificationOptions.initNotifications();
});
//var list1 =localStorage.getItem("notificationsId");

headerNotificationOptions.initNotifications = function(){
    var readNotifications = $('.notification-wrap .notification-single-item:not(.no-notification)');
    var arrayNotificationRead = [];
    $(readNotifications).each(function(){
        if ($(this).hasClass('notification-unread')){
            arrayNotificationRead.push(0);
        }else{
            arrayNotificationRead.push(1);
        }
    });
    $('.notificationMenu__list.menu.device .notification-single-item').each(function(i,el){
        if (arrayNotificationRead[i]){
            $(this).removeClass('notification-unread')
        }
    });
}
headerNotificationOptions.readNotification = function(elem){
    var notifID;
    notifID = $(elem).parent().prop("id");
    if (notifID == ""){
        var nid = $(elem).parent().index()-1;
        notifID = "notificationsId"+nid;
    }
    $(elem).parent().removeClass('notification-unread');
    localStorage.setItem(notifID,"read");
    window.open($(elem).parent().find('a.notification-text').attr('href'),'_blank');
}
headerNotificationOptions.cloneItems = function() {
    var el = $('.notification-cta .notification-wrap').find('ul').eq(0);
    el.clone().addClass('menu').addClass('device').appendTo('.mobile-notify-wrap');
};
headerNotificationOptions.deleteNotification = function(elem) {
    var notifID;
    $(elem).hide();
    $(elem).addClass('closed');
    notifID = $(elem).attr("id");
    if (!notifID){
        var nid = $(elem).index()-1;
        notifID = "notificationsId"+nid;
    }
    localStorage.setItem(notifID,"closed");
    numNotif = $('.notification-single-item.notification-unread:not(.is-submenu-item):not(.closed)').length;
    if (numNotif <= 0 ){
        $(".notification-count").css('display','none');
        var notificationEmpty = 0;
        $('.notification-cta .notification-single-item').each(function(){
            if($(this).hasClass('closed')){
                notificationEmpty ++;
            }
        });
        if (notificationEmpty == $('.notification-cta .notification-single-item:not(.no-notification)').length){
            $(".no-notification").css('display','block');
        }else{
            $(".no-notification").css('display','none');
        }
    }
    if (/^((?!chrome|android).)*safari/i.test(navigator.userAgent) && ((/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
        )){
        var n = $('.notification-single-item.notification-unread.is-submenu-item').length - $('.notification-single-item.notification-unread.is-submenu-item.closed').length;
        if (n==0){
            $(".no-notification").css('display','block');
        }else{
            $(".no-notification").css('display','none');
        }
    }
}
headerNotificationOptions.openPanel = function() {
    $('.is-drilldown').height("100%");
    // desktop version
    $('.notification-label').click(
        function(){
            var notificationWrap = $(this).closest('.notification-cta');
            if($('.notification-single-item', notificationWrap).length <= 1 ){
                $('.no-notification', notificationWrap).show();
            }

            $('.notification-count').hide();
            window.setTimeout(function() {
                $('.notification-cta').toggleClass('active');
            }, 10);
            if($('.notification-wrap').hasClass('closed')){
                $('.notification-wrap').slideUp(50).toggleClass('closed');
            } else {
                $('.notification-wrap').slideDown(50).toggleClass('closed');
            }
        }
    )
};
headerNotificationOptions.closePanelDocument = function() {
    // desktop version
    $(document).click(function(e) {
        if(!$(e.target).is(".notification-label") && !$(e.target).is(".notification-cta") && !$(e.target).is(".icon--close") && !$(e.target).is(".notification-single-item") && !$(e.target).is(".notification-count") && !$(e.target).is(".notification-label img")  ){
            //$(".toggle-all-destination-desk").addClass("hide");
            window.setTimeout(function() {
                $('.notification-cta').removeClass('active');
            }, 10);
            if ($('.notification-wrap').hasClass('closed')) {
                $('.notification-wrap').slideUp(50).removeClass('closed');
            }
        }
    });
};
headerNotificationOptions.closePanelhover = function() {
    // desktop version
    $(".is-dropdown-submenu-parent ").hover(function(e) {
        window.setTimeout(function() {
            $('.notification-cta').removeClass('active');
        }, 10);
        if ($('.notification-wrap').hasClass('closed')) {
            $('.notification-wrap').slideUp(50).removeClass('closed');
        }
    });
};
headerNotificationOptions.checkDrilldownOpen = function (event) {
    // mobile version
    var _target = $(event.target).find('.is-active');
    if(_target.closest('.mobile-notify-wrap').length) {
        $('.notification-count').hide();
    }
}
setTimeout(function() {localStorage.clear()}, 86400000);