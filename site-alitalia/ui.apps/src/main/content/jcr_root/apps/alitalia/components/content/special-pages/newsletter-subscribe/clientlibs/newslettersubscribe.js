jQuery(document).ready(function(){
	jQuery('#newslettersubSubmit').on('click', newsletterSubscribeHandler);
	$("#receievenewsletter").click(function(e){
		if ($(this).hasClass("checked")){
			$(this).removeClass("checked");
		} else {
			$(this).addClass("checked");
		}

	})
});
function newsletterSubscribeHandler(e) {
	e.preventDefault();
	$(".millemiglia__formFeedbackMessage").hide();
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if (re.test($("#email").val())){
		if ($("#receievenewsletter").hasClass("checked")){
			validation(e, 'newsletter-subscribe-crm-footer', '#specialpageform_newslettersubscribe', newsletterSubscribeSuccess, newsletterSubscribeError);
		} else {
			$(".millemiglia__formFeedbackMessage").show();
			showFormFeedbackError("specialpageform_newslettersubscribe",CQ.I18n.get('checked.error'));
		}

	} else {
		$(".millemiglia__formFeedbackMessage").show();
		showFormFeedbackError("specialpageform_newslettersubscribe",CQ.I18n.get("specialpage.email.error.notvalid"));
	}
}

function newsletterSubscribeSuccess(data) {
	if (data.result){
		enableLoaderMyAlitalia();
		performSubmit('newsletter-subscribe-crm-footer', '#specialpageform_newslettersubscribe', newsletterSubscribeSuccess2, newsletterSubscribeError);
	}
}

function newsletterSubscribeSuccess2(data) {
	console.log("data",data);
	if (data.isError){
		showFormFeedbackError("specialpageform_newslettersubscribe",data.errorMessage);
	}else{
		window.location.href = data.baseURL + "myalitalia/myalitalia-registrati.html?success=true&newsletter=true";
	}
}


function newsletterSubscribeError() {
	disableLoaderMyAlitalia();
	return newsletterSubscribeContinue(true);
}

function newsletterSubscribeContinue(stop) {
	removeErrors();
	$('#newslettersubSubmit').off('click');
	return stop || setAnalyticsPropertyAndSubmit();
}

function setAnalyticsPropertyAndSubmit(){
	var subscribedEmail = jQuery("#email").val();
	setLoginContextProfileProperty('subscribedEmail', subscribedEmail);
	$('#newslettersubSubmit').click();
}
