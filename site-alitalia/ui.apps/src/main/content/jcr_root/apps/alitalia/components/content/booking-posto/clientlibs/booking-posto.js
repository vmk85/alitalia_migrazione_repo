
function initSeatMapFunctions(){
	bookingSeat.init();
	//$("#confirmSeatMap").fastClick(confirmSeatSelection());
}

function bindingSectorSetmapFunction(){
	bookingSeat.initSeatsMap();
}

//resetSeatSelection
function be_cancelSeats(selector,done){
	$(".seatmap-form")[0].reset();
	$("input[name='action']").val("reset");
	
	var baseUrl = removeUrlSelector(); 
	$.ajax({
		url: baseUrl + ".seatmapselection.json",
		data: $(".seatmap-form").serialize(),
		context: document.body
	}).done(function(data) {
		refreshInfoBoxPartial();
		if(done) {
			done();
		}
	}).fail(function(){
		console.log("Failure saving seatmap");
		if(done) {
			done();
		}
	});
}

//confirmSeatSelection
function be_chooseSeats(e,btn,selector,done){
	$(".chooseSeat__container").each(function(i, flight){
		var indexFlight = i;
		var passengers = $(flight).find(".chooseSeat__passengerItem");

		$(passengers).each(function(i,pax){
			var passengerInfo = $(pax).data();
			var indexPax = passengerInfo.num;
			var seat = passengerInfo.takenSeat;
			var seatsPaxElement = $('input[name="seatPax_' + indexPax + '"]');
			var seatsPaxValue = seatsPaxElement.val();
			if (seatsPaxValue != '') {
				seatsPaxValue = seatsPaxValue + ";";
			}
			seatsPaxValue = seatsPaxValue + indexFlight + ":" + seat;
			seatsPaxElement.val(seatsPaxValue);
		});
	});
	$("input[name='action']").val("confirm");
	
	var baseUrl = removeUrlSelector(); 
	$.ajax({
		url: baseUrl + ".seatmapselection.json",
		data: $(".seatmap-form").serialize(),
		context: document.body
	}).done(function(data) {
		var baseUrl = removeUrlSelector(); 
		$.ajax({
			url: baseUrl + ".ancillary-seatmap-feedback-partial.html",
			context: document.body
		}).done(function(data) {
			$("#seatmap-feedback").html(data);
			refreshInfoBoxPartial();
			if(done) {
				done();
			}
		}).fail(function(){
			console.log("Failure saving seatmap");
			if(done) {
				done();
			}
		});
		
	}).fail(function(){
		console.log("Failure saving seatmap");
		if(done) {
			done();
		}
	});
}
