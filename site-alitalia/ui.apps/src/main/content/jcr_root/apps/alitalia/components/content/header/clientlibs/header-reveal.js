var headerRevealOptions = {};


// document ready
$( document ).ready( function() {
	$('body').on('click',function(e){
	$('.icon.icon--close').on('click',function(){
		$('#search').foundation('close');
		$("#search-bar__input").val("");
		$("#search-bar__input").keyup();
		//remove errors from reveal login
		$('#header-wave-login-errors').html("");
		$('input').removeClass('is-invalid-input');
		// $('.reveal--search .suggestion-box').empty();
	});
	if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
		$('.reveal-overlay .icon--close--black').on('click',function(){
			$('.reveal-overlay').hide();
			$( '.section--cerca-volo' ).removeClass( 'overlay--mobile' );
    		$('.reveal-overlay').css('display','none');
			$('html').removeClass('is-reveal-open');
			$('body').removeClass('is-reveal-open');

		});
	}

});
	headerRevealOptions.reveals = {};
	headerRevealOptions.reveals.login =  new Foundation.Reveal( $( '.reveal--login' ), {
		// animationIn: 'fade',
		// animationOut: 'fade',
	} );
} );

$(document).on('open.zf.reveal', function(){
    $('.menu.mainMenu').css("z-index",99);
    $('.reveal--login').foundation();
});

$(document).on('closed.zf.reveal', function(){
    $('.menu.mainMenu.posFixed').css("z-index","");
});