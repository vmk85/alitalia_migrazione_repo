setI18nTranslate('millemiglia.cartaCreditoRemove.error');
setI18nTranslate('millemiglia.cartaCreditoRemove.ajaxError');
setI18nTranslate('millemiglia.cartaCreditoRemove.success');

$('#cartaCreditoSubmit').bind('click', function(e) {
	validation(e, 'cartacreditosubmit', '#cartaCreditoForm',
			cartaCreditoSubmitSuccess, cartaCreditoSubmitError);
});

function cartaCreditoSubmitSuccess(data) {
	if (data.result) {
		removeErrors();
		$('#cartaCreditoSubmit').unbind('click');
		$('#cartaCreditoForm').submit();
	} else {
		showErrors(data);
		showFormFeedbackError('cartaCreditoForm',
				'millemiglia.cartaCreditoRemove.error');
	}
	return false;
}

function cartaCreditoSubmitError(data) {
	showFormFeedbackError('cartaCreditoForm',
			'millemiglia.cartaCreditoRemove.ajaxError');
	return false;
}
$(window).on("load", function (e) {

	var $accordionCartaCredito = $('#accordion1_tab7');
	if (resultCarteCreditoSubmit == "true") {
		$accordionCartaCredito.click();
		showFormFeedbackSuccess('cartaCreditoForm',
				'millemiglia.cartaCreditoRemove.success');
	} else if (resultCarteCreditoSubmit == "false") {
		$accordionCartaCredito.click();
		showFormFeedbackError('cartaCreditoForm',
				'millemiglia.cartaCreditoRemove.ajaxError');
	}
	if (resultCarteCreditoSubmit == "true"
		|| resultCarteCreditoSubmit == "false") {
		$accordionCartaCredito[0].scrollIntoView();
	}
	return false;
});


/*
$(window).load(function() {
	var $accordionCartaCredito = $('#accordion1_tab7');
	if (resultCarteCreditoSubmit == "true") {
		$accordionCartaCredito.click();
		showFormFeedbackSuccess('cartaCreditoForm',
				'millemiglia.cartaCreditoRemove.success');
	} else if (resultCarteCreditoSubmit == "false") {
		$accordionCartaCredito.click();
		showFormFeedbackError('cartaCreditoForm',
				'millemiglia.cartaCreditoRemove.ajaxError');
	}
	if (resultCarteCreditoSubmit == "true"
		|| resultCarteCreditoSubmit == "false") {
		$accordionCartaCredito[0].scrollIntoView();
	}
	return false;
});
*/