var infoVoliRequestData = {};
var infoVoliHandlerUrl;
var infoVoliListHandlerUrl;

$(document).ready(function() {
	infoVoliHandlerUrl = "http://booking.alitalia.com/FlightStatus/" +
		getLanguageCode() + '_' + getMarketCode() + "/FlightInfo";
	infoVoliListHandlerUrl = "http://booking.alitalia.com/FlightStatus/" +
		getLanguageCode() + '_' + getMarketCode() + "/FlightInfo/SceltaVoli";
	$('#infoVoliStatoButton').bind('click', infoVoliStatoHandler);
	$('#infoVoliOrariButton').bind('click', validateInfoVoliList);
});

function infoVoliStatoHandler(e) {
	widget_showOverlay();
	$('#infovoli__numero').val($('#infovoli__numero').val().trim());
	validation(e, "preparaviaggiosubmit", "#flight-info-submit",
			infoVoliCheckSuccess, infoVoliSubmitFail);
	return false;
}

function infoVoliCheckSuccess(data) {
	removeErrors();
	if (data.result) { 
		// FIXME: rimuovere fase 2
		/*var handler = infoVoliHandlerUrl + "?Brand=az&NumeroVolo="
			+ $('#infovoli__numero').val()
			+ "&DataCompleta=" + $('#flightStatusDatePicker').val();
		ajaxLoadIframeLogic(handler);
		setTimeout(function(){ $('.j-overlayClose, .j-overlayBg')
			.on('click', function(event) {
				event.preventDefault();
				$('.j-overlayBg, .j-overlayContainer, .j-overlayClose')
					.velocity('transition.fadeOut' , {
					complete: function() {
						$(this).remove();
						$('body').removeClass('overlayActive');
					}
				});
			});
		}, 150);
		return false;*/
		
		// FIXME: abilitare fase 2
		var data = {
			formType : $('#formType').val(),
			numeroVolo : $('#infovoli__numero').val(),
			dataPartenza : $('#flightStatusDatePicker').val()
		};
		performSubmit("preparaviaggiosubmit", data,
				infoVoliSubmitSuccess, infoVoliSubmitFail);
	} else {
		widget_hideOverlay();
		showErrors(data, true);
		
	}
	return false;
}

function infoVoliSubmitSuccess(data) {
	widget_hideOverlay();
	if (data.result && data.result == "OK") {
		var numeroVolo = $('#infovoli__numero').val();
		var dataPartenza = $('#flightStatusDatePicker').val();
		/*For avoiding caching*/
		$('#infoVoliStatoLoad').attr("data-link",
				$('#infoVoliStatoLoad').attr("href") 
				+ "?flight=AZ" + encodeURIComponent(numeroVolo)
				+ "&date=" + encodeURIComponent(dataPartenza.replace(/\//g,"-")));
		$('#infoVoliStatoLoad').trigger("click");
	} else {
		widget_hideOverlay();
		showErrors(data, true);
	}
}

function infoVoliSubmitFail(data) {
	widget_hideOverlay();
	return false;
}

function validateInfoVoliList(e) {
	e.preventDefault();
	widget_showOverlay();
	var $inputAeroportoPartenza = $('input#infovoli_departures').val();
	var $inputAeroportoArrivo = $('input#infovoli_arrival').val();
	
	// FIX per evitare che l'utente effettui ricerca con testo libero
	if ($('#infovoli_origin').val() != $inputAeroportoPartenza) {
		$("input#infovoli_departures").val("");
		$inputAeroportoPartenza = "";
	} else {
		$("input#infovoli_departures").val($inputAeroportoPartenza.substr($inputAeroportoPartenza.length - 3, 3));
	}
	
	if ($('#infovoli_destination').val() != $inputAeroportoArrivo) {
		$("input#infovoli_arrival").val("");
		$inputAeroportoArrivo = "";
	} else {
		$("input#infovoli_arrival").val($inputAeroportoArrivo.substr($inputAeroportoArrivo.length - 3, 3));
	}
	
	validation(e, "preparaviaggiosubmit", "#flight-info-list-submit",
			infoVoliListCheckSuccess, infoVoliListSubmitError);
	$("input#infovoli_departures").val($inputAeroportoPartenza);
	$("input#infovoli_arrival").val($inputAeroportoArrivo);
	return false;
}

function infoVoliListCheckSuccess(data) {
	removeErrors();
	if (data.result) {
		/*var $inputAeroportoPartenza = $('input#infovoli_departures');
		var $inputAeroportoArrivo = $('input#infovoli_arrival');
		$inputAeroportoPartenza = $inputAeroportoPartenza.val().substr(
				$inputAeroportoPartenza.val().length - 3, 3);
		$inputAeroportoArrivo = $inputAeroportoArrivo.val().substr(
				$inputAeroportoArrivo.val().length - 3, 3);
		var dataPartenza = $('input#flightStatusDatePicker2').val();*/
		
		// FIXME: rimuovere fase 2
		/*var handler = infoVoliListHandlerUrl + "?From_hidden="
			+ $inputAeroportoPartenza + "&To_hidden="
			+ $inputAeroportoArrivo + "&CompleteDate="
			+ dataPartenza;
		window.location.href = handler;
		return false;*/
		
		// FIXME: abilitare fase 2
		/*var infoVoliRequestData = {
			infovoli_departures : $inputAeroportoPartenza,
			infovoli_arrival : $inputAeroportoArrivo,
			flightStatusDatePicker2 : dataPartenza,
			formType : $("#formTypeList").val()
		}
		performSubmit('preparaviaggiosubmit', infoVoliRequestData,
				infoVoliListRedirect, infoVoliListSubmitError);*/
		$("#infoVoliOrariButton").off("click");
		$("#infoVoliOrariButton").click();
	} else {
		widget_hideOverlay();
		showErrors(data, true);
	}
}

function infoVoliListRedirect(data) {
	/*widget_hideOverlay();*/
	if (data.redirect) {
		window.location.href = data.redirect;
	} else {
		// FIXME: manage error
	}
}

function infoVoliListSubmitError(data) {
	widget_hideOverlay();
	return false;
}

function widget_showOverlay(){
	$("#widgetOverlay").show();
}

function widget_hideOverlay(){
	$("#widgetOverlay").hide();
}

function attachLightboxCloseEvt(){
	setTimeout(function(){ 
		$('.j-overlayClose, .j-overlayBg')
		.on('click', function(event) {
			event.preventDefault();
			$('.j-overlayBg, .j-overlayContainer, .j-overlayClose')
			.velocity('transition.fadeOut' , {
				complete: function() {
					$(this).remove();
					$('body').removeClass('overlayActive');
				}
			});
		});
	}, 150);
}