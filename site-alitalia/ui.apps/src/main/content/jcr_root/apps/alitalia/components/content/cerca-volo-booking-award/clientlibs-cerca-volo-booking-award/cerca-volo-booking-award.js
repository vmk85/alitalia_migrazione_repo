$(document).ready(function() {
	$('#cercaVoliAwardSubmit').on('click', function(e) {
		validation(e, 'bookingawardpreparesearch', '#cercaVoliAwardForm',
			bookingAwardPrepareSearchValidationSuccess,
			bookingAwardPrepareSearchValidationError);
	});
});

function bookingAwardPrepareSearchValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		$('#cercaVoliAwardSubmit').off('click');
		$('#cercaVoliAwardForm').submit();
	} else {
		showErrors(data);
	}
}

function bookingAwardPrepareSearchValidationError() {
	removeErrors();
	$('#cercaVoliAwardSubmit').off('click');
}