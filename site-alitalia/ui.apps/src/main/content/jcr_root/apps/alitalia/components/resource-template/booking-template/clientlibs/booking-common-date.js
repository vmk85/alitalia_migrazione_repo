/**
 * Gestione dei campi data composti da 3 select per selezionare, rispettivamente, giorno, mese e anno.
 * In particolare viene gestito il change sul mese e sull'anno, in modo da ricaricare i dati dei giorni per gestire i mesi con giorni da 28, 29, 30 o 31 giorni. 
 */
(function ($) {
	
	/**
	 * Definizione costanti
	 */
	var EMPTY_SELECTOR = '',
		CHILD_BIRTH_DATE_SELECTOR = '_dataNascitaBambino',
		INFANT_BIRTH_DATE_SELECTOR = '_dataNascitaNeonato';

	/**
	 * codice eseguito al caricamento completo della pagina
	 */
	 $( window ).on( "load",function() {
        /* Aggiunta event listner a tutti i campi data presenti in pagina */
		var dateIdSelectors = [EMPTY_SELECTOR, CHILD_BIRTH_DATE_SELECTOR, INFANT_BIRTH_DATE_SELECTOR];

		for (index in dateIdSelectors) {
			var selector = dateIdSelectors[index];
			setListner(selector);

			for (var i = 1; i <= 10; i++) {
				setListner(selector + "_" + i);
			}
		}
    });

	/**
	 * setting dei listner sulle select delle date
	 */
	function setListner(selector) {
		var $giorno = $('select#giorno' + selector),
		$mese = $('select#mese' + selector),
		$anno = $('select#anno' + selector);

		if ($giorno.length > 0 && $mese.length > 0 && $anno.length > 0) {
			$giorno.on('focus', handleFocusGiorno);
			$mese.on('change', refreshGiorni);
			$anno.on('change', refreshGiorni);

			fillGiorni(31, selector);

			if (selector.indexOf(CHILD_BIRTH_DATE_SELECTOR) != -1) {
				// setting del giusto range di anni per le date di nascita dei passeggeri bambini
				fillAnni(selector, maxYearChd, -minYearChd);
			} else if (selector.indexOf(INFANT_BIRTH_DATE_SELECTOR) != -1) {
				// setting del giusto range di anni per le date di nascita dei passeggeri neonati
				fillAnni(selector, maxYearInf, -minYearInf);
			} else {
				fillAnni(selector);
			}
		}
	}

	function handleFocusGiorno(e) {
		var selector = $(e.target).attr('id');
		selector = selector.substr(selector.indexOf('_'));
		if (selector == '0') {
			selector = '';
		}

		var sizeGiorni = $('select#giorno' + selector).children().length;
		var mese = $('select#mese' + selector).find(":selected").val();
		var anno = $('select#anno' + selector).find(":selected").val();
		var mesi31 = ['01','03','05','07','08','10','12'];
		var mesi30 = ['04','06','09','11'];

		if (jQuery.inArray(mese, mesi31) > -1 && sizeGiorni < 31) {
			fillGiorni(31, selector);
		} else if (jQuery.inArray(mese, mesi30) > -1 && (sizeGiorni > 30 || sizeGiorni <= 29)) {
			fillGiorni(30, selector);
		}

		if (mese == '02' && (anno % 4) == 0 && (sizeGiorni == 28 || sizeGiorni >= 30)) {
			fillGiorni(29, selector);
		} else if (mese == '02' && (anno % 4) > 0 && (sizeGiorni > 28)) {
			fillGiorni(28, selector);
		}
	}

	/**
	 * Funzioni per gestire la corretta popolazione delle select di gioni ed anni
	 */
	function fillGiorni(giorniMese, selector) {
		var $giorno = $('select#giorno' + selector);
		$giorno.empty();

		for (var i = 1; i <= giorniMese; i++) {
			if (i < 10) {
				$giorno.append('<option value="0' + i + '">0' + i + '</option>');
			} else {
				$giorno.append('<option value="' + i + '">' + i + '</option>');
			}
		}
		$giorno.prepend('<option value="" selected></option>');
	}

	function refreshGiorni(e) {
		var selector = $(e.target).attr('id');
		selector = selector.substr(selector.indexOf('_'));
		if (selector == '0') {
			selector = '';
		}

		var giorno = $('select#giorno' + selector).find(":selected").val();
		var mese = $('select#mese' + selector).find(":selected").val();
		var anno = $('select#anno' + selector).find(":selected").val();
		var mesi31 = ['01','03','05','07','08','10','12'];
		var mesi30 = ['04','06','09','11'];

		if (jQuery.inArray(mese, mesi30) > -1 && giorno > 30) {
			fillGiorni(30, selector);
		}

		if (mese == '02' && (anno % 4) == 0 && giorno > 29) {
			fillGiorni(29, selector);
		} else if (mese == '02' && (anno % 4) > 0 && giorno > 28) {
			fillGiorni(28, selector);
		}
	}

	function fillAnni(selector, oldestYearOffset, newestYearOffset) {
		var now = new Date();
		var anno = now.getFullYear();
		var $anno = $('select#anno' + selector);
		var oldestYear = anno - 100;
		var newestYear = anno;
		var $departureYear = $(departureDateSelected);

		var final_year;

		if($departureYear.val() != null && $departureYear.val() != "") {
		    final_year = $departureYear.val();
		    final_year = final_year.split("/");
		    final_year = final_year[final_year.length-1];
		    anno = parseInt(final_year);
		}

		if (oldestYearOffset != undefined) {
			oldestYear = anno - oldestYearOffset;
		}
		if (newestYearOffset != undefined) {
			newestYear = anno + newestYearOffset;
		}

		$anno.empty();

		for (var i = oldestYear; i <= newestYear; i++) {
			$anno.prepend('<option value="' + i + '">' + i + '</option>');
		}
		$anno.prepend('<option value="" selected></option>');
	}

	/**
	 * handle del listner sul giorno
	 */


}(jQuery));
