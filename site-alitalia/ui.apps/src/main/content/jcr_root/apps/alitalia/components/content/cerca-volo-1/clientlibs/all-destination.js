
function initAllDestination(airportList) {
    $(".initial").removeClass("disabled");
    initialsArray = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' ];
    prevAirport = { 'country' : 0, };
	prevDest = { 'city' : 0, };
    alphabetBg = ['а', 'б', 'ц', 'д', 'е', 'ф', 'г', 'х', 'и', 'л', 'н', 'м', 'п', 'к', 'р', 'с', 'т', 'у', 'в', 'з' ];
    alphabetRu = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я' ];
    alphabetGr = ['α', 'β', 'γ', 'δ', 'e', 'ζ', 'η', 'θ', 'ι', 'κ', 'λ', 'μ', 'ν', 'ξ', 'ο', 'π', 'ρ', 'σ', 'τ', 'υ', 'φ', 'χ', 'ψ', 'ω'];
    var text = "";
    var i;

    if (window.location.href.indexOf("bg_bg") != -1) {
       for (i = 0; i < alphabetBg.length; i++) {
         text +=  '<li><a class="initial" data-letter="'+alphabetBg[i]+'"href="javascript:;">'+alphabetBg[i]+'</a></li>' ;
       }
     document.getElementById("list-alphabet").innerHTML = text;
    }

   if (window.location.href.indexOf("ru_ru") != -1) {
      for (i = 0; i < alphabetRu.length; i++) {
        text +=  '<li><a class="initial" data-letter="'+alphabetRu[i]+'"href="javascript:;">'+alphabetRu[i]+'</a></li>' ;
      }
    document.getElementById("list-alphabet").innerHTML = text;
   }

    if (window.location.href.indexOf("el_gr") != -1) {
          for (i = 0; i < alphabetGr.length; i++) {
            text +=  '<li><a class="initial" data-letter="'+alphabetGr[i]+'"href="javascript:;">'+alphabetGr[i]+'</a></li>' ;
          }
        document.getElementById("list-alphabet").innerHTML = text;
    }

    // recupero tutti i paesi
    arrAllCountries = cercaVoloAllDestinationsOptions.getUniqueCountry(airportList);
    var old = oldGetUniqueCountry(airportList);
    // stampo lista paesi
    cercaVoloAllDestinationsOptions.printCountriesMarkup(function () {
        $( '.countries-wrap .country' ).on( 'click', function() {
            if(arrAvailableDest !== undefined) {
                cercaVoloAllDestinationsOptions.onSelectCountry( $( this ).text() );

                //  selected airport
                $( '.airports-wrap .airport' ).on( 'click', function() {
                    $( this ).closest( '.airports-wrap' ).find( '.selected' ).removeClass( 'selected' );
                    $( this ).addClass( 'selected' );
                } );

            }

            //  selected country
            if( !$(this).hasClass('disabled') )
            {
                $( this ).closest( '.countries-wrap' ).find( '.selected' ).removeClass( 'selected' );
                $( this ).addClass( 'selected' );
                     if (Foundation.MediaQuery.atLeast('large')) {
                     $( '.airports-list li:first-child .airports-group li:first-child .airport' ).focus();
            }
            }
        } );
    });

	// initials list controls
	cercaVoloAllDestinationsOptions.initialsListControls();

}



$( window ).on( 'changed.zf.mediaquery', function(event, newSize, oldSize ) {
	if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
		$( '.all-destination' ).addClass( 'hide' );
		if( $( '#departureAirportCode--prenota' ).val().length == 3 ) {
			$( '.toggle-all-destination-mobile' ).removeClass( 'hide' );
		}
	} else {
		$( '.toggle-all-destination-mobile' ).addClass( 'hide' );
	}
});

function oldGetUniqueCountry(inputArr) {
    return inputArr.sort( function( prev, next ) {
        if( prev.country < next.country ) return -1;
        if( prev.country > next.country ) return 1;
        return 0;
    } ).map( function( currAirport, i, allAirports ) {
        if( prevAirport.country !== currAirport.country ) {
            // currLetter = currAirport.country.slice(0,1);
            prevAirport.country = currAirport.country;
            return currAirport.country;
        }
    } ).filter( function(item, index, array) {
        if( item !== 'undefined' ) return item;
    } );
}

cercaVoloAllDestinationsOptions.getUniqueCountry = function(inputArr) {
    if(inputArr)
        {
            var airportByCountry = _.groupBy(inputArr, 'country');
            airportByCountry = _.pick(airportByCountry, function(value, key, object) {
                return value && _.isArray(value) && value.length>0;
            });
            return Object.keys(airportByCountry).sort();
        }
    else // TODO: get list from storage
        return [];
};

cercaVoloAllDestinationsOptions.printCountriesMarkup = function(callback) {
	$( '.countries-list' ).empty();

	// print all country
	for( var i = 0; i < arrAllCountries.length; i++ ) {
		currLetter = arrAllCountries[ i ].slice(0,1).toLowerCase();

		// open or close countries goups list
		if( currLetter !== prevLetter && i !== 0 && i !== arrAllCountries.length - 1 ) {
			// no countries with these intials
			initialsArray.splice( initialsArray.indexOf( currLetter ) , 1);
			$( '.countries-list' ).append('<li><span id="' + currLetter + '" class="initial-letter" data-letter="' + currLetter + '">' + currLetter + '</span><ul class="countries-group" data-letter="' + currLetter + '"></ul></li>');
			prevLetter = currLetter;
		}

		// print list item
		$( '.countries-group[data-letter="' + currLetter + '"]' ).append('<li><a href="javascript:;" class="country" data-country="' + arrAllCountries[ i ] + '" >' + arrAllCountries[ i ] + '</a></li>');
	}

	// disable initials with no country
	for( i in initialsArray ) {
		$( '.initials-wrap [data-letter="' + initialsArray[ i ] + '"]' ).addClass( 'disabled' );
	}

    callback();
};

cercaVoloAllDestinationsOptions.initialsListControls = function() {
	$( '.initials-wrap .initial' ).on( 'click', function(e) {
		e.preventDefault();
		if( !$(this).hasClass('disabled') )
		{
		    $( '.countries-list' ).scrollTop( 0 );
		    $( '.countries-list' ).scrollTop( $( '#' + $(this).data( 'letter' ) ).closest( 'li' ).position().top );
		}
	} );
};

cercaVoloAllDestinationsOptions.getAvailableAirport = function(iataCode) {

    	$.ajax( {
		type: "GET",
		//url: getServiceUrl('airportdestinationconsumer') + '?iata=' + iataCode,
		url: getServiceUrl_1('airportdestinationconsumer', iataCode),
		dataType: 'json',
        async: true,
		success: function( data ) {

			arrAvailableDest = data.airports;
			// Init desk version
            initAllDestination(arrAvailableDest);
            // Init mobile version
            cercaVoloAutocomplete.initAutocomplete(data, '#luogo-arrivo--prenota-desk', '#suggestion_ritorno--prenota-desk');
            cercaVoloAutocomplete.initAutocomplete(data, '#luogo-arrivo--prenota-mobile', '#suggestion_ritorno--prenota-mobile');
			//  array con destinazioni disponibili
			arrAvailableCountries = cercaVoloAllDestinationsOptions.getUniqueCountry( arrAvailableDest );
			// array con paesi non disponibili
			countriesToDisable = cercaVoloAllDestinationsOptions.getCountriesToDisable( arrAllCountries, arrAvailableCountries );

            for(var current in arrAvailableDest.country) {
				$( '.country[data-country="' + arrAvailableCountries[ current ] + '"]' ).toggleClass( 'disabled' );
        	}
			// disable no country dest
			for( var i in countriesToDisable ) {
				$( '.country[data-country="' + countriesToDisable[ i ] + '"]' ).addClass( 'disabled' );
			}
			$.each($('.countries-group'), function (i, el){
				var totalCountriesCheck = $('.country', $(el)).length;
				var disabledCountriesCheck = $('.disabled', $(el)).length;
				if ( disabledCountriesCheck == totalCountriesCheck ){
					$('#' + $(el).data('letter') + ', .initial[data-letter="' + $(el).data('letter') + '"]').addClass('disabled');
				}
			});

		},
		complete: function() {
			// if mobile or tablet, manage mobile vizualization
			if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
				// mostra bottone toggle
				$( '.all-destination-wrap, #panel-all-destination .toggle-all-destination-mobile' ).removeClass( 'hide' );
			}
		},
		error: function() {
			console.log( 'errore chiamata filter destination' );
		}
	} );

};

cercaVoloAllDestinationsOptions.getCountriesToDisable = function(allcountries, currCountries) {
  var countriesToDisable = [];
	for ( var i = 0; i < allcountries.length; i++ ) {
		if ( currCountries.indexOf( allcountries[ i ] ) === -1 ) {
			countriesToDisable.push( allcountries[ i ] );
		}
	}
  return countriesToDisable;
};

cercaVoloAllDestinationsOptions.onSelectCountry = function(selectedCountry) {

	$( '.airports-list' ).empty();

	arrAirportsToDisplay = arrAvailableDest.map( function( currDest, i, allDest ) {
		if( currDest.country == selectedCountry ) return currDest;
	} ).filter( function(item, index, array) {
		if( item !== 'undefined' ) return item;
	} ).sort( function( prev, next ) {
		if( prev.city < next.city ) return -1;
		if( prev.city > next.city ) return 1;
		return 0;
	} );

    var oneCity = true;
    arrAirportsToDisplay.forEach(function(el, i) { if(el.city != arrAirportsToDisplay[0].city) oneCity = false; });

    if(oneCity) {

        arrCityGroup = [];
        arrCityGroup.push(arrAirportsToDisplay[0].city);

    } else {

        arrCityGroup = arrAirportsToDisplay.map( function( currDest, i, allDest ) {
            if( currDest.city !== prevDest.city) {
                prevDest.city = currDest.city;
                return currDest.city;
            }
        } ).filter( function(item, index, array) {
            if( item !== 'undefined' ) return item;
        } );
    }
    // print all airports-list
    for( i = 0; i < arrCityGroup.length; i++ ) {
        // append ariports-group
        $( '.airports-list' ).append('<li><span class="city">' + arrCityGroup[ i ] + '</span><ul class="airports-group" data-city="' + arrCityGroup[ i ] + '"></ul></li>');
    }
    // print all airports city
    for( i = 0; i < arrAirportsToDisplay.length; i++ ) {
        $( '.airports-group[data-city="' + arrAirportsToDisplay[ i ].city + '"]' ).append('<li><a href="javascript:;" onclick="cercaVoloAutocomplete.setAirportInputValue(&quot;destination--prenota&quot;, &quot;' + arrAirportsToDisplay[ i ].city + '&quot;, &quot;' + arrAirportsToDisplay[ i ].code + '&quot;, &quot;' + arrAirportsToDisplay[ i ].country + '&quot;, &quot;' + arrAirportsToDisplay[ i ].type + '&quot;);" class="airport" data-airport="' + arrAirportsToDisplay[ i ].city + '" data-iata-code="' + arrAirportsToDisplay[ i ].code + '" >' + arrAirportsToDisplay[ i ].airport + '<strong class="iata-code">' + arrAirportsToDisplay[ i ].code + '</a></li>');
    }

	// if mobile or tablet, manage mobile vizualization
	if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
		$( '.initials-wrap, .countries-wrap' ).addClass( 'hide' );
		$( '.airports-wrap' ).removeClass( 'hide' );
	}

	// select airport
	$( '.airports-wrap .airport' ).on( 'click',  function() {
		cercaVoloAllDestinationsOptions.selectedAirport = $( this ).data('airport');
		cercaVoloAllDestinationsOptions.selectedIataCode = $( this ).data('iata-code');

		// update input val
		$('input[name="destination--prenota"]').val( cercaVoloAllDestinationsOptions.selectedAirport + ' ' + cercaVoloAllDestinationsOptions.selectedIataCode).trigger('change');
		// update hidden form iata
		$( '#arrivalAirportCode--prenota' ).val( cercaVoloAllDestinationsOptions.selectedIataCode );

		// split value to make IATA code bold
		$('input[name="destination--prenota"]').each( function(i, el) {
			$( el ).parent().find( '.input-placeholder' ).html( cercaVoloAllDestinationsOptions.selectedAirport + '<span style="color:#006643;"><strong class="iata-code">' + cercaVoloAllDestinationsOptions.selectedIataCode + '</strong></span>' );
		} );

		if( Foundation.MediaQuery.atLeast( 'large' ) ) {
			cercaVoloPrenotaOptions.closeAllCollapsablePanels();
			// autofocus destination desk
            // $( '#luogo-arrivo--prenota-desk' ).focus();
		}

		// open histogram
        cercaVoloOptions.handleHistogramStatus();

	} );
};

$("#luogo-partenza--prenota-desk").on("keyup", function(e) {
    if(e.originalEvent.key != 'Tab')
    	$(this).attr("data-type", "");
});

$("#luogo-arrivo--prenota-desk").on("keyup", function(e) {
    if(e.originalEvent.key != 'Tab')
	    $(this).attr("data-type", "");
});