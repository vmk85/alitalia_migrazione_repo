setI18nTranslate('millemiglia.richiediMiglia.error');
setI18nTranslate('millemiglia.richiediMiglia.ajaxError');
setI18nTranslate('millemiglia.richiediMiglia.success');

$('#richiediMigliaSubmit').bind('click', function(e) {
	clearRichiediMigliaMessages();
	validation(e, 'richiedimigliasubmit', '#richiediMigliaForm',
			richiediMigliaSuccess, richiediMigliaError);
	return false;
});

function richiediMigliaSuccess(data) {
	if (data.result) {
		removeErrors();
		$('#richiediMigliaSubmit').unbind('click');
		$('#richiediMigliaForm').submit();
	} else {
		showErrors(data);
		showFormFeedbackError('richiediMigliaForm',
				'millemiglia.richiediMiglia.error');
		if (typeof(data.fields["number"]) != "undefined") {
			if (typeof(window["analytics_num_biglietto_Error"]) === "function"){
				window["analytics_num_biglietto_Error"]();
			}
		}
	}
	return false;
}

function richiediMigliaError(data) {
	showFormFeedbackError('richiediMigliaForm',
			'millemiglia.richiediMiglia.ajaxError');
	return false;
}

function clearRichiediMigliaMessages(){
	$(".millemiglia__formFeedback.j-formFeedback").removeClass("isActive");
	$(".millemiglia__formFeedback.j-formFeedback p:gt(0)").remove();
	$(".millemiglia__formFeedback.j-formFeedback .millemiglia__formFeedbackText").empty();
}

function showMultipleFormFeedbackError(formId){
	var $formFeedback = $('form#' + formId).find('.millemiglia__formFeedback.j-formFeedback');
	$formFeedback.addClass('isActive');
}

$(window).on('load', function() {
	if (resultRichiediMigliaSubmit == "true") {
		clearRichiediMigliaMessages
		showFormFeedbackSuccess('richiediMigliaForm',
				'millemiglia.richiediMiglia.success');
	} else if (resultRichiediMigliaSubmit == "false" && $(".millemiglia__formFeedback.j-formFeedback p").size() === 1) {
		showFormFeedbackError('richiediMigliaForm', 
				'millemiglia.richiediMiglia.ajaxError');
	} else if (resultRichiediMigliaSubmit == "false") {
		showMultipleFormFeedbackError('richiediMigliaForm', 
		'millemiglia.richiediMiglia.ajaxError');
}
	return false;
	
});