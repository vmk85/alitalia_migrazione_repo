$(document).ready(function(){
	var target = $('a.j-lightbox-waiting');
	if(target.lenght != 0){
		target.each(function(){
			var hrefAttr = $(this).attr('href');
			$(this).attr('data-link',hrefAttr).removeAttr('target');				
		});
		
		target.unfastClick().fastClick(function(event) {
			event.preventDefault();
			startLoader('section.carnet.innerPage');
			
			var link = $(this).attr('data-link');
			var overlayClass = $(this).attr('data-overlayClass') || '';
			
			ajaxLoadPopupLogic(link, overlayClass, stopCarnetLoader);
		});
	}
});

function stopCarnetLoader(){
	stopLoader('section.carnet.innerPage');	
}

function errorRefreshPartial(id_container){
	$(id_container).find(".genericErrorMessage").show();
}

function startLoader(id_container) {
	$(id_container).addClass("carnetOverlayLoading");
}

function stopLoader(id_container) {
	$(id_container).removeClass("carnetOverlayLoading");
}

function refreshPartial(selector_partial, id_container, done, startAnalytics) {
	startLoader(id_container)
	$.ajax({
		url: removeUrlSelector() + "." + selector_partial,
		context : document.body
	})
	.done(function(data) {
		$(id_container).replaceWith(data);
		if (done) {
			done(data);
		}
		if(startAnalytics && typeof window["analytics_carnetPartialCallback"] == "function"){
			analytics_carnetPartialCallback();
		}
	})
	.fail(function() {
		errorRefreshPartial(id_container);
	})
	.always(function() {
		stopLoader(id_container);
	});
}