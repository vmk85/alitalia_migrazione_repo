
function initPartialMmbInsurance() {
}


/* add insurance */

function be_addInsurance(e, btn, section, callback) {
	performValidation('mmbancillarycartaddinsurance', '#form-mmb-insurance', 
			mmbAncillaryAddInsuranceValidationSuccess, mmbHandleInvokeError);
}

function mmbAncillaryAddInsuranceValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('mmbancillarycartaddinsurance', '#form-mmb-insurance', 
				mmbAncillaryAddInsuranceSubmitSuccess, mmbHandleInvokeError);
	} else {
		showErrors(data);
	}
}

function mmbAncillaryAddInsuranceSubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		if (data.isError) {
			mmbHandleServiceError(data);
		}
		refreshMmbPartialInsurance(true, true);
	}
}


/* clear insurance */

function be_cancelInsurance(e, btn, section, callback) {
	performSubmit('mmbancillarycartremoveinsurance', '#form-mmb-insurance',
			mmbAncillaryClearInsuranceValidationSuccess, mmbHandleInvokeError);
}

function mmbAncillaryClearInsuranceValidationSuccess(data) {
	if (data.result) {
		removeErrors();
		performSubmit('mmbancillarycartremoveinsurance', '#form-mmb-insurance', 
				mmbAncillaryClearInsuranceSubmitSuccess, mmbHandleInvokeError);
	} else {
		showErrors(data);
	}
}

function mmbAncillaryClearInsuranceSubmitSuccess(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	} else {
		if (data.isError) {
			mmbHandleServiceError(data);
		}
		refreshMmbPartialInsurance(true, true);
	}
}
