$(document).ready(function() {
	if (typeof (Storage) !== "undefined") {
		var codici = setId();
		removeOldNotifications(codici);
		var numero_notifiche = updateNotifications(codici);
		if (!isEmpty()) {
			$("#message-empty").hide();
		}
		$(".userMenu__notificationsCount").text(numero_notifiche);
	}
	$(".notificationMenu__itemClose").on('click', function(e) {
		var id = $(this).parent('li').attr('id');
		hideNotification(id);
		$(this).hide();
		if (isEmpty()) {
			$("#message-empty").show();
		}
		e.stopPropagation();
	});
});

function setId() {
	var number = 0;
	var codici = [];
	var k = 0;
	$(".notificationMenu__itemLink").each(function(i) {
		var descrizione = $(this).text();
		var id = $(this).parent().attr("id");
		if (id != "message-empty") {
			id = getHash(descrizione);
			codici[k] = id;
			k++;
			$(this).parent().attr("id", id.toString());
			$("#" + id.toString()).on('click', showNotification);
		}
	});
	$(".notificationMenu__itemLink").on('click', showNotification);
	return codici;
}

function showNotification(e) {
	readNotification($(this));
	e.stopPropagation();
}

function isEmpty() {
	if (typeof localStorage !== 'undefined') {
		try {
			var notifiche = JSON.parse(localStorage.getItem("Notifiche"));
			if (notifiche != null) {
				for (i = 0; i < notifiche.length; i++) {
					if (notifiche[i].stato != "eliminata") {
						return false;
					}
				}
			}
		} catch (e) {
		}
	}
	return true;
}

function readNotification(element) {
	if (typeof localStorage !== 'undefined'
			&& typeof (Storage) !== "undefined") {
		try {
			var id = element.attr('id');
			if (typeof (id) == "undefined") {
				id = element.parent().attr('id');
			}
			$('#' + id).addClass("isRead");
			var notifiche = JSON.parse(localStorage.getItem("Notifiche"));
			if (notifiche != null) {
				trovato = false;
				for (i = 0; i < notifiche.length && !trovato; i++) {
					if (notifiche[i].id == id && notifiche[i].stato == "nuova") {
						notifiche[i].stato = "visualizzata";
						trovato = true;
					}
				}
				if (trovato) {
					localStorage
							.setItem("Notifiche", JSON.stringify(notifiche));
				}
			}
		} catch (e) {
		}
	}
}

function hideNotification(id) {
	if (typeof localStorage !== 'undefined'
			&& typeof (Storage) !== "undefined") {
		try {
			$('#' + id).hide();
			var notifiche = JSON.parse(localStorage.getItem("Notifiche"));
			if (notifiche != null) {
				trovato = false;
				for (i = 0; i < notifiche.length && !trovato; i++) {
					if (notifiche[i].id == id
							&& notifiche[i].stato != "eliminata") {
						notifiche[i].stato = "eliminata";
						trovato = true;
					}
				}
				if (trovato) {
					localStorage
							.setItem("Notifiche", JSON.stringify(notifiche));
				}
			}
		} catch (e) {
		}
	}
}

function removeOldNotifications(codici) {
	if (typeof localStorage !== 'undefined') {
		try {
			var items = localStorage.getItem("Notifiche");
			var newItems = [];
			if (items) {
				items = JSON.parse(items);
				for (var i = 0; i < items.length; i++) {
					var id = items[i].id;
					for (var k = 0; k < codici.length; k++) {
						if (id == codici[k].toString()) {
							newItems.push(items[i]);
							break;
						}
					}
				}
				localStorage.setItem("Notifiche", JSON.stringify(newItems));
			}
		} catch (e) {
		}
	}
}

function updateNotifications(codici) {
	var number = 0;
	if (typeof localStorage !== 'undefined') {
		try {
			var items = localStorage.getItem("Notifiche");
			items = (items) ? JSON.parse(items) : [];
			var exist = false;
			for (var k = 0; k < codici.length; k++) {
				exist = false;
				for (var i = 0; i < items.length; i++) {
					var id = items[i].id;
					if (id == codici[k]) {
						exist = true;
						var stato = items[i].stato;
						if (stato != "eliminata") {
							$("#" + codici[k].toString()).show();
						}
						if (stato == "nuova") {
							number++;
						}
						if (stato == "visualizzata") {
							$('#' + codici[k].toString()).addClass("isRead");
						}
					}
				}
				if (!exist) {
					items.push({
						id : codici[k],
						stato : "nuova"
					});
					$("#" + codici[k].toString()).show();
					number++;
				}
			}
			localStorage.setItem("Notifiche", JSON.stringify(items));
		} catch (e) {
		}
	}
	return number;
}

function getHash(descrizione) {
	var hash = 0;
	if (descrizione.length == 0) {
		return hash;
	}
	for (i = 0; i < descrizione.length; i++) {
		c = descrizione.charCodeAt(i);
		hash = ((hash << 5) - hash) + c;
		hash = hash & hash; // Convert to 32bit integer
	}
	return hash;
}
