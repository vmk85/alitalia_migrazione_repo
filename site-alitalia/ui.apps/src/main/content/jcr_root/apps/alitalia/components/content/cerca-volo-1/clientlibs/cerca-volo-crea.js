var cercaVoloCrea = {};
var allAirports;
var cercaVoloAllDestinationsOptions = {};
var currLetter, prevLetter, initialsArray, arrAllCountries, countriesToDisable, arrAvailableDest, arrAirportsToDisplay, arrAvailableCountries, currAirport, arrCityGroup, prevDest;
var prevAirport = { 'country' : 0, };
var prevDest = { 'city' : 0, };

$(document).ready(function() {
    var ricercheRecenti;

    window.onload = function () {
        $('#switch-from--prenota-mobile').parent().removeClass('checked');

        //-----inizio FIX per ricerca volo ------------//
        var searchType=$("div.radio-travel-type.radio-group div.checked input").val();
        $('input#roundTrip--prenota').val(searchType)
        //----- fine FIX per ricerca volo ------------//
    }();

    cercaVoloCrea.showHistogram = false;
    var items = localStorage.getItem("ricercheRecenti");
    if (items == null || items == 'undefined' || items == '[]' || items.length == 0) {
        ricercheRecenti = [];
        $(".last__search").append("<div style='margin: 5px'>" + CQ.I18n.get('cercaVolo.nessuna.ricerca.recente') + "</div>");
    } else {
        ricercheRecenti = JSON.parse(items);
    }
    var i = 0;
    var today = new Date();
    today.setHours(0,0,0,0);
    ricercheRecenti.forEach(function(item) {
        var dateParts = item.dataPartenza.split("/");
        var dataPartenza = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        if(dataPartenza>=today){
            var pass = checkPassengers(item['nAdulti'], item['nBambini'], item['nNeonati']);
            var adate = setDate(item["dataPartenza"]);
            var rdate = "";
            var classe;
            if(item["classeViaggio"] == "cercaVolo.label.business"){
                classe = "Business";
            }
            else if(item["classeViaggio"] == "cercaVolo.label.economy"){
                classe = "Economy";
            }
            if(item["dataRitorno"] != ""){
                rdate = setDate(item["dataRitorno"]);
            }
            $('.last__search').append('<a  href="javascript:setSubmitLS(' + i + ');"><div class="ricerche-precedenti custom-row bg-main"><div class="column small-12"><div class="fly"></div><div class="custom-col-card"><div class="card"><div class="destination"> ' + item['cittaPartenza']['nomeT'] + ' <span class="sigla">' + item['cittaPartenza']['codiceAeroporto']  + ' </span> - '  + item['cittaArrivo']['nomeT']  + ' <span class="sigla"> ' + item['cittaArrivo']['codiceAeroporto'] + ' </span></div><div class="date leave">' + adate + '</div><div class="date back">' + rdate + '</div><div class="passengers"> ' + pass + '</div><div class="class"> ' + classe + '</div></div></div><div class="cta-wrap"><div class="cta cta--primary cta--only-icon"><span>' + CQ.I18n.get('dettaglioofferta.scopri.label') + '</span></div></div></div></div></a>');
        }

        i++;
    });

    (function(){
        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        var ratio = window.devicePixelRatio || 1;
        var screen = {
            width : window.screen.width * ratio,
            height : window.screen.height * ratio
        };
        // iPhone X Detection
        if (iOS && screen.width == 1125 && screen.height === 2436) {
            $('button#submitHidden--prenota.button').css('margin-bottom','50px');
        }
    })();

    getAirportsList();

    initialsArray = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' ];
    // toogle all destination panel
    $( '[class*="toggle-all-destination"]' ).on( 'click', function(e) {

        // hide toggle button
        $( this ).addClass( 'hide' );
        $( '.all-destination, .countries-wrap, .initials-wrap' ).removeClass( 'hide' );

        if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
            $( '.airports-wrap' ).addClass( 'hide' );
        } else {
            $( '.airports-wrap' ).removeClass( 'hide' );
        }
    } );

    // close airports panel
    $( '.airports-wrap [data-back]' ).on( 'click', function() {
        // if mobile or tablet
        if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
            $( '.initials-wrap, .countries-wrap' ).removeClass( 'hide' );
            $( '.airports-wrap' ).addClass( 'hide' );
        }
    } );

    // nascondi all destination if typing
    $( '[id*="luogo-arrivo--prenota"]' ).on( 'keyup', function(e) {
        // if mobile or tablet & campo vuoto
        if( $(this).val() !== '' ) {
            if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
                $( '[class*="toggle-all-destination"], .all-destination, .airports-wrap' ).addClass( 'hide' );
            } else {
                $( '[class*="toggle-all-destination"]' ).addClass( 'hide' );
                // cercaVoloPrenotaOptions.closeAllCollapsablePanels();
            }
        } else {
            $( '[class*="toggle-all-destination"]' ).removeClass( 'hide' );
            if( !Foundation.MediaQuery.atLeast( 'large' ) && (e.originalEvent.key == 'Backspace' || e.originalEvent.key == 'Delete')) {
                $( '.all-destination' ).addClass( 'hide' );
            }
        }
    } );

    // desk on input focus
    $( '#luogo-arrivo--prenota-desk' ).on( 'focus', function() {
        if(arrAvailableDest !== undefined && $( this ).val() === '') {
            $( '.toggle-all-destination-desk' ).removeClass( 'hide' );
        }
    } );

    // set up initial widget visibility
    // se mobile
    if( !Foundation.MediaQuery.atLeast( 'large' ) ) {
        $( '.airports-wrap' ).addClass( 'hide' );
    }

    //  selected initials
    $( '.initials-wrap .initial' ).on( 'click',  function(e) {
        e.preventDefault();
        if( !$(this).hasClass('disabled') )
        {
            $( this ).closest( '.initials-wrap' ).find( '.selected' ).removeClass( 'selected' );
            $( this ).addClass( 'selected' );
            if (Foundation.MediaQuery.atLeast('large')) {
                $( '.countries-group[data-letter="' + $(this).data('letter') + '"] li:first-child a' ).focus();
            }
        }
    } );

    // on input focus (tab cerca volo) chiudi all-destinations
    $( '.cerca-volo__content--prenota [data-ref]' ).on( 'focus', function(e) {
        if( $(e.target).attr( 'name' ) !== 'destination--prenota' ) {
            $( '[class*="toggle-all-destination"]' ).addClass( 'hide' );
        }
    } );

});



function getAirportsList() {
    var airportListUrl = getServiceUrl("airportslistconsumerrest");
    var geoUrl = getServiceUrl("airportslistconsumergeo");
    $.when(
        $.ajax({
            url : airportListUrl,
            context : document.body,
        }),
        $.ajax({
            url : geoUrl,
            context : document.body
        })
    ).done(function (airportLists, geo) {
        allAirports = airportLists[0];
        populateDepartureInput(geo[0].geoAirport);
        initAllDestination(allAirports.airports);
        initCercaVoloAutocomplete(allAirports);
    });
}


function checkPassengers(adult, kid, baby) {
    var string = "";
    if(adult > 0) {
        string += adult;
        if(adult == 1)
            string += " " + CQ.I18n.get("cercaVolo.label.adulto");
        else
            string += " " + CQ.I18n.get("cercaVolo.label.adulti");
    }
    if(kid > 0) {
        string += ", " + kid;
        if(kid == 1)
            string += " " + CQ.I18n.get("booking.carrello.bambino.label");
        else
            string += " " + CQ.I18n.get("booking.carrello.bambini.label");
    }
    if(baby > 0) {
        string += ", " + baby;
        if(baby == 1)
            string += " " + CQ.I18n.get("booking.carrello.neonato.label");
        else
            string += " " + CQ.I18n.get("booking.carrello.neonati.label");
    }
    return string;
}

function setDate(rawDate) {
	if(typeof(rawDate) != 'undefined') {
		date = rawDate.split("/");
	    var rawDate = new Date(date[2], date[1]-1, date[0]);
	    var week, day, month, year;
	    var weeks = [CQ.I18n.get("common.daysOfWeek.sunday.short"), CQ.I18n.get("common.daysOfWeek.monday.short"), CQ.I18n.get("common.daysOfWeek.tuesday.short"), CQ.I18n.get("common.daysOfWeek.wednesday.short"), CQ.I18n.get("common.daysOfWeek.thursday.short"), CQ.I18n.get("common.daysOfWeek.friday.short"), CQ.I18n.get("common.daysOfWeek.saturday.short")];
	    var months = ["months:", CQ.I18n.get("common.monthsOfYear.january.short"), CQ.I18n.get("common.monthsOfYear.february.short"), CQ.I18n.get("common.monthsOfYear.march.short"), CQ.I18n.get("common.monthsOfYear.april.short"), CQ.I18n.get("common.monthsOfYear.may.short"), CQ.I18n.get("common.monthsOfYear.june.short"), CQ.I18n.get("common.monthsOfYear.july.short"), CQ.I18n.get("common.monthsOfYear.august.short"), CQ.I18n.get("common.monthsOfYear.september.short"), CQ.I18n.get("common.monthsOfYear.october.short"), CQ.I18n.get("common.monthsOfYear.november.short"), CQ.I18n.get("common.monthsOfYear.december.short")];
	    week = weeks[rawDate.getDay()];
	    day = date[0];
	    month = months[date[1]*1];
	    year = date[2];
	    return week + " " + day + " " + month + "\n" + year;
	} else {
		return "";
	}
    
}

function setSubmitLS(index) {
    var items = JSON.parse(localStorage.getItem("ricercheRecenti"));
    $("#roundTrip--prenota").val(items[index]["tipoViaggio"]);
    $("#departureAirportCode--prenota").val(items[index]["cittaPartenza"]["codiceAeroporto"]);
    $("#departureAirport--prenota").val(items[index]["cittaPartenza"]["nomeT"]);
    $("#arrivalAirportCode--prenota").val(items[index]["cittaArrivo"]["codiceAeroporto"]);
    $("#arrivalAirport--prenota").val(items[index]["cittaArrivo"]["nomeT"]);
    $("#departureDate--prenota").val(items[index]["dataPartenza"]);
    $("#returnDate--prenota").val(items[index]["dataRitorno"]);
    $("#seatType--prenota").val(items[index]["classeViaggio"]);
    $("#adultPassenger--prenota").val(items[index]["nAdulti"]);
    //$("#youngAdultPassenger--prenota").val(items[index]["yAdultNum"]);
    $("#kidPassenger--prenota").val(items[index]["nBambini"]);
    $("#babyPassenger--prenota").val(items[index]["nNeonati"]);
    $("#hiddenForm--prenota").trigger("submit");
}



function removeUrlSelector() {
    var url = location.pathname;
    url = url.substr(0, location.pathname.indexOf('.'));
    return location.protocol + '//' + location.host + url;
}

function makeHistogram(data) {
    var structure = " ";

    if(data != false && data.result && data.result.international) {
        $("#panel-histogram").css("display", "block");
        var amounts = [], heights = [], bestPrices = [], departureDates = [], returnDates = [], months = [];
        var i18nroot = "common.monthsOfYear.";
        var extM = ["mesi", CQ.I18n.get(i18nroot + "january"), CQ.I18n.get(i18nroot + "february"), CQ.I18n.get(i18nroot + "march"), CQ.I18n.get(i18nroot + "april"), CQ.I18n.get(i18nroot + "may"), CQ.I18n.get(i18nroot + "june"), CQ.I18n.get(i18nroot + "july"), CQ.I18n.get(i18nroot + "august"), CQ.I18n.get(i18nroot + "september"), CQ.I18n.get(i18nroot + "octember"), CQ.I18n.get(i18nroot + "november"), CQ.I18n.get(i18nroot + "december")];
        var maxAmount, minAmount, heightMin, posMin, posMax, sizeMin;
        $.each(data.result.values, function(year, yearData) {
            $.each(yearData, function(month, monthData) {
                $.each(monthData, function(day, dayData) {
                    //amounts.push(parseInt(dayData.amount.replace(/\s/g, '')));
                    dayData.amount=dayData.amount.replace(/\s|,|\./g, '');

                    amounts.push(parseInt(dayData.amount));

                    //heights.push(dayData.height);
                    //bestPrices.push(parseInt(dayData.bestPrice.replace(/\s/g, '')));
                    departureDates.push(dayData.departureDate);
                    returnDates.push(dayData.returnDate);
                });
                months.push(month);
            });
        });
        sizeMin = 15;
        maxAmount = amounts[0];
        minAmount = amounts[0];
        posMin = 0;
        var i = 0;
        amounts.forEach(function(item) {
            if(item > maxAmount) {
                maxAmount = item;
                posMax = i;
            }
            i++;
        });
        i = 0;
        amounts.forEach(function(item) {
            if(item < minAmount) {
                minAmount = item;
                posMin = i;
            }
            i++;
        });
        var i = 0;/*
    amounts.forEach(function(item) {
        heights.push(item * 100 / maxAmount);
        i++;
    });*/
        amounts.forEach(function(item) {
            if(maxAmount != minAmount)
                heights.push((((item - minAmount)/(maxAmount-minAmount))*(100-sizeMin))+sizeMin);
            else
                heights.push(100);
        });
        structure += '<div class="panel__title">' + CQ.I18n.get("allofferts.miglioriofferte") + ' <strong>' + data.result.departure.city + " " + data.result.departure.code + "</strong> - <strong>" + data.result.arrival.city + " " + data.result.arrival.code + "</strong></div>";
        structure += '<div class="histogram_wrap">';
        structure += '  <div class="custom-row row--padded">';
        amounts.forEach(function(item) {
            structure += '<div class="col"><span class="histogram__price">' + currency + ' ' + item + '</span></div>';
        });
        structure += '  </div>';
        structure += '  <div class="custom-row row--histogram">';
        structure += '    <div class="col col--legend">';
        structure += '      <span class="histogram__priceline" style="height: 100%">';
        structure += '      <span class="price">' + currency + ' ' + maxAmount + '</span><span class="line"></span></span>';
        structure += '      <span class="histogram__priceline" style="height:' +sizeMin + '% ' + isVisible(maxAmount, minAmount) + '">';
        structure += '      <span class="price">' + currency + ' ' + minAmount + '</span><span class="line"></span></span>';
        structure += "    </div>";

        var i=0;
        heights.forEach(function(item) {
            structure += '<div class="col"><span class="histogram__block"><a class="histogram__bar" href="'+location.protocol + '//' + location.host + location.pathname.slice(0, 7) +'/offers/all-offers/offer-detail.html?from='+data.result.departure.code +'&to='+data.result.arrival.code+'&month='+months[i++]+'" style="height: ' + (item*1) + '%"></a></span></div>';
        });


        structure += "  </div>";
        structure += '  <div class="custom-row row--padded">';
        months.forEach(function(item) {
            structure += '<div class="col"><span class="histogram__month">' + extM[item] + '</span></div>';
        });
        structure += "</div></div>";
        $(".histo").html(structure);
        cercaVoloCrea.showHistogram = true;
        $("#panel-histogram").addClass("open");
    } else {
        $(".histo").html(" ");
        $(".histogram-panel").css("display", "none !important");
        $("#panel-histogram").removeClass("open");
        cercaVoloCrea.showHistogram = false;
    }
}

function isVisible(min, max) {
    if(min == max)
        return '; display: none;';
    else return "";
}

$("[aria-label='Close Cerca Volo Widget']").on("click", function(){
    cercaVoloPrenotaOptions.closeAllCollapsablePanels();
});