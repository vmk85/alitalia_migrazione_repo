var faqOptions = {};

// document ready
$( document ).ready( function() {

	faqOptions.initAccordion();
} );

faqOptions.initAccordion = function() {
	$( '.faq__accordion .accordion' ).foundation();
	faqOptions.accordion = new Foundation.Accordion( $( '.faq__accordion .accordion' ), {
		// slideSpeed: 0,
		multiExpand: false,
		allowAllClosed: true,
	} );
};