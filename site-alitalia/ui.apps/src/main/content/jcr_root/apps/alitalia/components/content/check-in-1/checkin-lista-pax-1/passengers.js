"use strict";
use(function () {

    var passenger = this.passenger;
    var loggedUser = this.loggedUser;
    var docRs = passenger.documentRequired;
    var docs = passenger.documents;
    var customerMaCrm = this.customerMaCrm;

    var check = {};
    var estaDocs = [];
    var estaDocsInf = [];

    var hasDoc = false;
    var hasDocInf = false;

    if (docRs != null ){

        check.documentsRequired = docRs.size() > 0 ? true : false;
        check.numDocumentsRequired = docRs.size();

    } else {

        check.documentsRequired = false;
        check.numDocumentsRequired = 0;

    }

    check.adultDocumentsSRequired = false;
    check.infantDocumentsSRequired = false;
    check.adultDocumentOtherRequired = false;
    check.infantDocumentOtherRequired = false;
    check.adultResidentDocumentRequired = false;
    check.infantResidentDocumentRequired = false;
    check.adultDestinationDocumentRequired = false;
    check.infantDestinationDocumentRequired = false;
    check.adultDocVRequired = false;
    check.infantDocVRequired = false;
    check.isLoggedUserChecked = false;

    check.adultNationalID = false;
    check.adultPassport = false;

    check.infantNationalID = false;
    check.infantPassport = false;
    check.nationalIDPermitted = passenger.nationalIDPermitted;

    // Dati documenti
    check.adultDocumentsS_type = "";
    check.adultDocumentsS_number = "";
    check.adultDocumentsS_dateOfBirth = "";
    check.adultDocumentsS_lastName = "";
    check.adultDocumentsS_firstName = "";
    check.adultDocumentsS_nationality = "";
    check.adultDocumentsS_gender = passenger.gender;
    check.adultDocumentsS_issueCountry = "";
    check.adultDocumentsS_expirationDate = "";

    check.infantDocumentsS_type = "";
    check.infantDocumentsS_number = "";
    check.infantDocumentsS_dateOfBirth = "";
    check.infantDocumentsS_lastName = "";
    check.infantDocumentsS_firstName = "";
    check.infantDocumentsS_nationality = "";
    check.infantDocumentsS_gender = "";
    check.infantDocumentsS_issueCountry = "";
    check.infantDocumentsS_expirationDate = "";

    check.adultDocumentOther_birthPlace = "";
    check.adultDocumentOther_number = "";
    check.adultDocumentOther_issueCity = "";
    check.adultDocumentOther_issueDate = "";
    check.adultDocumentOther_applicableCountryCode = "";

    check.infantDocumentOther_birthPlace = "";
    check.infantDocumentOther_number = "";
    check.infantDocumentOther_issueCity = "";
    check.infantDocumentOther_issueDate = "";
    check.infantDocumentOther_applicableCountryCode = "";

    check.adultResidentDocument_streetAddress = "";
    check.adultResidentDocument_city = "";
    check.adultResidentDocument_stateProvince = "";
    check.adultResidentDocument_countryCode = "";
    check.adultResidentDocument_zipCode = "";

    check.adultDestinationDocument_streetAddress = "";
    check.adultDestinationDocument_city = "";
    check.adultDestinationDocument_stateProvince = "";
    check.adultDestinationDocument_countryCode = "";
    check.adultDestinationDocument_zipCode = "";

    check.infantResidentDocument_streetAddress = "";
    check.infantResidentDocument_city = "";
    check.infantResidentDocument_stateProvince = "";
    check.infantResidentDocument_countryCode = "";
    check.infantResidentDocument_zipCode = "";

    check.infantDestinationDocument_streetAddress = "";
    check.infantDestinationDocument_city = "";
    check.infantDestinationDocument_stateProvince = "";
    check.infantDestinationDocument_countryCode = "";
    check.infantDestinationDocument_zipCode = "";

    check.CInumber = "";
    check.CICountry = "";
    check.CIexpirationDate = "";
    check.Passnumber = "";
    check.PassCountry = "";
    check.PassexpirationDate = "";
    check.greenCardRecapnumber = "";
    check.greenCardRecapCountry = "";
    check.greenCardRecapexpirationDate = "";
    check.vistoRecapnumber = "";
    check.vistoRecapCountry = "";
    check.vistoRecapexpirationDate = "";

    if(passenger.frequentFlyer != null){
        if(passenger.frequentFlyer.size()>0){
            check.ffCode = passenger.frequentFlyer.get(0).number;
        }
    }

    // Carico eventuali documenti esistenti
    if (docs != null){
        for (var y=0; y<docs.size(); y++)
        {

            var doc = docs.get(y);
            //gender adulto
            if(doc.type == "None" && doc.infant == false){
                check.adultDocumentsS_gender = doc.number.split("\\|")[1];
                check.adultDocumentsS_dateOfBirth = doc.birthDate;
            }
            if(doc.type == "None" && doc.infant == true){
                check.infantDocumentsS_gender = doc.number.split("\\|")[1].substring(0,1);
                check.infantDocumentsS_dateOfBirth = doc.birthDate;
            }
            // Passaporto/Carta identità bambino
            if ((doc.type == 'Passport' || doc.type == 'NationalID') && doc.infant == true)
            {
                hasDocInf = true;
                var n = doc.number.split("\\|");
                var number = "";
                var gender = "";
                var nationality = "";
                try
                {
                    number = n[0];
                    gender = n[1].substring(0,1);
                    nationality = n[2];
                }
                catch(z)
                {

                }

                var data = doc.number.split("\\|");
                check.infantDocumentOther_birthPlace = data[0];
                check.infantDocumentsSRequired = true;
                check.infantDocumentsS_type = doc.type.toUpperCase();
                check.infantDocumentsS_number = number; //doc.number;
                check.infantDocumentsS_dateOfBirth = doc.birthDate;
                check.infantDocumentsS_lastName = doc.lastName;
                check.infantDocumentsS_firstName = doc.firstName;
                check.infantDocumentsS_nationality = nationality;
                check.infantDocumentsS_gender = gender;//doc.gender;
                check.infantDocumentsS_issueCountry = doc.emissionCountry;
                check.infantDocumentsS_expirationDate = doc.expirationDate;
            }
            // Passaporto/Carta identità adulto
            if ((doc.type == 'Passport' || doc.type == 'NationalID') && doc.infant == false)
            {
                hasDoc = true;
                var n = doc.number.split("\\|");
                var number = "";
                var gender = "";
                var nationality = "";
                try
                {
                    number = n[0];
                    gender = n[1];
                    nationality = n[2];
                }
                catch(z)
                {
                }
                check.adultDocumentsSRequired = true;
                check.adultDocumentsS_type = doc.type.toUpperCase();
                check.adultDocumentsS_number = number;//doc.number;
                check.adultDocumentsS_dateOfBirth = doc.birthDate;
                check.adultDocumentsS_lastName = doc.lastName;
                check.adultDocumentsS_firstName = doc.firstName;
                check.adultDocumentsS_nationality = nationality;
                check.adultDocumentsS_gender = gender;//doc.gender;
                check.adultDocumentsS_issueCountry = doc.emissionCountry;
                check.adultDocumentsS_expirationDate = doc.expirationDate;
            }
            // GreenCard adulto
            if (doc.type == 'GreenCard' && doc.infant == false){
                var n = doc.number.split("\\|");
                var number = "";
                try
                {
                    number = n[0];

                    check.adultDocumentOtherRequired = true;
                    check.adultDocumentsOther_type = doc.type.toUpperCase();
                    check.adultDocumentsGreenCard_number = number;//doc.number;
                    check.adultDocumentsGreenCard_expirationDate = doc.expirationDate;
                }
                catch(z)
                {
                }
            }
            // GreenCard infant
            if (doc.type == 'GreenCard' && doc.infant == true){
                var n = doc.number.split("\\|");
                var number = "";
                try
                {
                    number = n[0];

                    check.infantDocumentOtherRequired = true;
                    check.infantDocumentsOther_type = doc.type.toUpperCase();
                    check.infantDocumentsGreenCard_number = number;//doc.number;
                    check.infantDocumentsGreenCard_expirationDate = doc.expirationDate;
                }
                catch(z)
                {
                }
            }
            if (doc.type == 'Esta' && doc.infant == false){
                estaDocs.push(doc);
            }
            if (doc.type == 'Esta' && doc.infant == true){
                estaDocsInf.push(doc);
            }

            // Residenza bambino
//        if (doc.type == 'DOCA-R' && check.adultResidentDocumentRequired && !check.infantResidentDocumentRequired)
//        {
//            try
//            {
//                check.infantResidentDocumentRequired = true;
//                var data = doc.number.split("\\|");
//                check.infantResidentDocument_streetAddress = data[0];
//                check.infantResidentDocument_city = data[1];
//                check.infantResidentDocument_stateProvince = data[2];
//                check.infantResidentDocument_countryCode = data[3];
//                check.infantResidentDocument_zipCode = data[4];
//            }
//            catch (e)
//            {
//            }
//        }
            // Residenza adulto
            if (doc.type == 'DOCA-R' && !check.adultResidentDocumentRequired)
            {
                try
                {
                    check.adultResidentDocumentRequired = true;
                    var data = doc.number.split("\\|");
                    check.adultResidentDocument_streetAddress = data[0];
                    check.adultResidentDocument_city = data[1];
                    check.adultResidentDocument_stateProvince = data[2];
                    check.adultResidentDocument_countryCode = data[3];
                    check.adultResidentDocument_zipCode = data[4];
                }
                catch (e)
                {
                }
            }
            // Destinazione bambino
//        if (doc.type == 'DOCA-D' && check.adultDestinationDocumentRequired && !check.infantDestinationDocumentRequired)
//        {
//            try
//            {
//                check.infantDestinationDocumentRequired = true;
//                var data = doc.number.split("\\|");
//                check.infantDestinationDocument_streetAddress = data[0];
//                check.infantDestinationDocument_city = data[1];
//                check.infantDestinationDocument_stateProvince = data[2];
//                check.infantDestinationDocument_countryCode = data[3];
//                check.infantDestinationDocument_zipCode = data[4];
//            }
//            catch (e)
//            {
//            }
//        }
            // Destinazione adulto
            if (doc.type == 'DOCA-D' && !check.adultDestinationDocumentRequired)
            {
                try
                {
                    check.adultDestinationDocumentRequired = true;
                    var data = doc.number.split("\\|");
                    check.adultDestinationDocument_streetAddress = data[0];
                    check.adultDestinationDocument_city = data[1];
                    check.adultDestinationDocument_stateProvince = data[2];
                    check.adultDestinationDocument_countryCode = data[3];
                    check.adultDestinationDocument_zipCode = data[4];
                }
                catch (e)
                {
                }
            }


        }
    }

    if(estaDocs.length>0){
        var lastEstaDoc = estaDocs[estaDocs.length-1];
        try
        {
            check.adultDocumentOtherRequired = true;
            check.adultDocumentsOther_type = "VISTO";


            var data = lastEstaDoc.number.split("\\|");
            check.adultDocumentOther_birthPlace = data[0];
            check.adultDocumentOther_number = data[1];
            check.adulDtocumentOther_issueCity = data[2];

            var id = data[3].split("-");
            var dt = "";
            try
            {
                dt = id[0] + "-" + id[1] + "-" + id[2] + "T00:00:01Z";
            }
            catch(z)
            {
            }
            check.adultDocumentOther_issueDate = dt;
            check.adultDocumentOther_applicableCountryCode = data[4];
        }
        catch (e)
        {
        }
    }
    if(estaDocsInf.length>0){
        var lastEstaDocInf = estaDocsInf[estaDocsInf.length-1];
        check.infantDocumentOtherRequired = true;
        check.infantDocumentsOther_type = "VISTO";

        var data = lastEstaDocInf.number.split("\\|");

        check.infantDocumentOther_birthPlace = data[0];
        check.infantDocumentOther_number = data[1];
        check.infantDocumentOther_issueCity = data[2];

        check.infantDocumentOther_issueDate = dt;
        check.infantDocumentOther_applicableCountryCode = data[4];
    }


    if (docRs != null){

        for (var x=0; x<docRs.size(); x++)
        {
            var doc = docRs.get(x);
            if (doc.code == "DOCS" || passenger.adultDocumentsSRequired == true)
                check.adultDocumentsSRequired = true;
            if (doc.code == "DOCS/INF" || passenger.infantDocumentsSRequired == true){
                check.infantDocumentsSRequired = true;
                if(hasDocInf){
                    check.numDocumentsRequired = check.numDocumentsRequired-1;
                }
            }
            if (doc.code == "DOCS-P"){
                check.adultPassport = true;
                if(hasDoc){
                    check.numDocumentsRequired = check.numDocumentsRequired-1;
                }
            }
            if (doc.code == "DOCS-C"){
                check.adultNationalID = true;
                if(hasDoc){
                    check.numDocumentsRequired = check.numDocumentsRequired-1;
                }
            }
            if (doc.code == "DOCS/INF" || passenger.infantDocumentsSRequired == true)
                check.infantDocumentsSRequired = true;
            if (doc.code == "ESTA" || passenger.adultDocumentOtherRequired == true)
                check.adultDocumentOtherRequired = true;
            if (doc.code == "DOCA/R" || passenger.adultResidentDocumentRequired == true)
                check.adultResidentDocumentRequired = true;
            if (doc.code == "DOCA/R/INF" || passenger.infantResidentDocumentRequired == true)
                check.infantResidentDocumentRequired = true;
            if (doc.code == "DOCA/D" || passenger.adultDestinationDocumentRequired == true)
                check.adultDestinationDocumentRequired = true;
            if (doc.code == "DOCA/D/INF" || passenger.infantDestinationDocumentRequired == true)
                check.infantDestinationDocumentRequired = true;
            if (doc.code == "DOCV" || passenger.adultDocVRequired == true){
                check.numDocumentsRequired = check.numDocumentsRequired-1;
                check.adultDocVRequired = true;
            }
            if(doc.code == "ESTA" && doc.freeText == "VERIFY U.S. VISA"){
                check.numDocumentsRequired = check.numDocumentsRequired-1;
            }
        }
    }

    if (check.adultDocumentOtherRequired == true && check.infantDocumentsSRequired == true)
        check.infantDocumentOtherRequired = true;

    if (check.infantDocumentOtherRequired == true)
    {
        if (check.adultPassport == true)
            check.infantPassport;
        if (check.adultNationalID == true)
            check.infantNationalID;
    }

    if (check.infantDocumentsSRequired == true && check.adultDocVRequired == true)
        check.infantDocVRequired = true;

    if (check.adultPassport == true)
        check.adultDocumentsSRequired = true;

    function getMonth(_m)
    {
        var m = "";

        if (_m == 'JAN')
            m = "01";
        if (_m == 'FEB')
            m = "02";
        if (_m == 'MAR')
            m = "03";
        if (_m == 'APR')
            m = "04";
        if (_m == 'MAY')
            m = "05";
        if (_m == 'JUN')
            m = "06";
        if (_m == 'JUL')
            m = "07";
        if (_m == 'AUG')
            m = "08";
        if (_m == 'SEP')
            m = "09";
        if (_m == 'OCT')
            m = "10";
        if (_m == 'NOV')
            m = "11";
        if (_m == 'DEC')
            m = "12";

        return m;
    }

    // SE C'è UN UTENTE LOGGATO SOVRASCRIVO I DATI CON QUELLI SALVATI NEL SUO PROFILO

    if(loggedUser != null) {
        var isLoggedUser = false;
        /** user millemiglia*/
        if(passenger.nome.toUpperCase() == loggedUser.nome.toUpperCase() && passenger.cognome.toUpperCase() == loggedUser.cognome.toUpperCase()){
            isLoggedUser = true;
        }
        if(loggedUser.tipo.toString().toUpperCase()!="NONE"){//    se utente è loggato
            if(isLoggedUser){ // se il passeggero è quello loggato
                check.isLoggedUserChecked = true;
                var hasPassport = false;
                var hasIC = false;
                var hasGC = false;
                var hasVisto = false;
                if(loggedUser.passportNumber != null){
                    hasPassport = true;
                }
                if(loggedUser.ICNumber != null){
                    hasIC = true;
                }
                if(loggedUser.numberGC != null){
                    hasGC = true;
                }
                if(loggedUser.numberVisto != null){
                    hasVisto = true;
                }

                //DATA DI NASCITA
                if(loggedUser.birthDate!=null){
                    if(loggedUser.birthDate!=""){
                        var formatedDateParts = loggedUser.birthDate.split("/");
                        var formatedDate = formatedDateParts[2]+"-"+formatedDateParts[1]+"-"+formatedDateParts[0];
                        check.adultDocumentsS_dateOfBirth = formatedDate;
                    }
                }

                //SESSO
                if(loggedUser.sesso == "M"){
                    check.adultDocumentsS_gender = "M";
                }
                if(loggedUser.sesso == "F"){
                    check.adultDocumentsS_gender = "F";
                }

                //PASSAPORTO
                if(hasPassport && hasIC){
                    var expDate = loggedUser.passportExpireDate.split("/");
                    check.adultDocumentsS_type = "PASSPORT";
                    check.adultDocumentsS_number = loggedUser.passportNumber;
                    check.adultDocumentsS_issueCountry = loggedUser.passportEmissionCountry.split("\\|")[0];
                    check.adultDocumentsS_expirationDate = expDate[2]+"-"+expDate[1]+"-"+expDate[0];
                }else if(hasPassport){
                    var expDate = loggedUser.passportExpireDate.split("/");
                    check.adultDocumentsS_type = "PASSPORT";
                    check.adultDocumentsS_number = loggedUser.passportNumber;
                    check.adultDocumentsS_issueCountry = loggedUser.passportEmissionCountry.split("\\|")[0];
                    check.adultDocumentsS_expirationDate = expDate[2]+"-"+expDate[1]+"-"+expDate[0];
                }else if(hasIC){
                    var expDate = loggedUser.ICExpireDate.split("/");
                    check.adultDocumentsS_type = "NATIONALID";
                    check.adultDocumentsS_number = loggedUser.ICNumber;
                    check.adultDocumentsS_issueCountry = loggedUser.ICEmissionCountry.split("\\|")[0];
                    check.adultDocumentsS_expirationDate = expDate[2]+"-"+expDate[1]+"-"+expDate[0];
                }

                //GREENCARD/VISTO
                if(hasVisto){
                    var issueDate = loggedUser.validFromVisto.split("/");
                    check.adultDocumentsOther_type = "VISTO";
                    check.adultDocumentOther_number = loggedUser.numberVisto;
                    check.adultDocumentOther_applicableCountryCode = loggedUser.countryCodeVisto.split("\\|")[0];
                    check.adultDocumentOther_issueDate = issueDate[2]+"-"+issueDate[1]+"-"+issueDate[0];
                }
                if(hasGC){
                    var expDate = loggedUser.expireOnGC.split("/");
                    check.adultDocumentsOther_type = "GREENCARD";
                    check.adultDocumentsGreenCard_number = loggedUser.numberGC;
                    check.adultDocumentsGreenCard_expirationDate = expDate[2]+"-"+expDate[1]+"-"+expDate[0];
                }
                if(hasGC && hasVisto){
                    check.adultDocumentsOther_type = "VISTO";
                }


                // INDIRIZZO DI RESIDENZA
                if(loggedUser.homeAddress!=null){
                    if(loggedUser.homeAddress.streetFreeText!=null){
                        check.adultResidentDocument_streetAddress = loggedUser.homeAddress.streetFreeText;
                    }
                    if(loggedUser.homeAddress.postalCode!=null){
                        check.adultResidentDocument_zipCode = loggedUser.homeAddress.postalCode;
                    }
                    if(loggedUser.homeAddress.stateCode!=null){
                        check.adultResidentDocument_stateProvince = loggedUser.homeAddress.stateCode;
                    }
                    if(loggedUser.homeAddress.country!=null){
                        check.adultResidentDocument_countryCode = loggedUser.homeAddress.country;
                    }
                    if(loggedUser.homeAddress.municipalityName!=null){
                        check.adultResidentDocument_city = loggedUser.homeAddress.municipalityName;
                    }
                }


            }
        }
    }

    /** user myalitalia crm*/
    if (customerMaCrm != null){
            if(passenger.nome.toUpperCase() == customerMaCrm.infoCliente.nome.toUpperCase() && passenger.cognome.toUpperCase() == customerMaCrm.infoCliente.cognome.toUpperCase()){
                check.isLoggedUserChecked = true;
                var hasPassport = false;
                var hasIC = false;
                var hasGC = false;
                var hasVisto = false;
                var expDate="";
                if (customerMaCrm.elencoDocumenti != null){
                    for(var i=0 ; i < customerMaCrm.elencoDocumenti.size();i++){


                        // switch (customerMaCrm.elencoDocumenti.get(i).tipo.toUpperCase()){
                        if ( customerMaCrm.elencoDocumenti.get(i).tipo.toUpperCase() == "CI") {
                            hasIC = true;
                            check.CInumber = customerMaCrm.elencoDocumenti.get(i).numero;
                            check.CICountry = customerMaCrm.elencoDocumenti.get(i).paeseEmissione;
                            check.CIexpirationDate = customerMaCrm.elencoDocumenti.get(i).dataScadenza;
                        } else if ( customerMaCrm.elencoDocumenti.get(i).tipo.toUpperCase() == "PA") {
                            hasPassport = true;
                            check.Passnumber = customerMaCrm.elencoDocumenti.get(i).numero;
                            check.PassCountry = customerMaCrm.elencoDocumenti.get(i).paeseEmissione;
                            check.PassexpirationDate = customerMaCrm.elencoDocumenti.get(i).dataScadenza;
                        } else if ( customerMaCrm.elencoDocumenti.get(i).tipo.toUpperCase() == "GC") {
                            hasGC = true;
                            check.greenCardRecapnumber = customerMaCrm.elencoDocumenti.get(i).numero;
                            check.greenCardRecapCountry = customerMaCrm.elencoDocumenti.get(i).paeseEmissione;
                            check.greenCardRecapexpirationDate = customerMaCrm.elencoDocumenti.get(i).dataScadenza;
                        } else if ( customerMaCrm.elencoDocumenti.get(i).tipo.toUpperCase() == "VS") {
                            hasVisto = true;
                            check.vistoRecapnumber = customerMaCrm.elencoDocumenti.get(i).numero;
                            check.vistoRecapCountry = customerMaCrm.elencoDocumenti.get(i).paeseEmissione;
                            check.vistoRecapexpirationDate = customerMaCrm.elencoDocumenti.get(i).dataScadenza;
                            check.adultDocumentOther_issueCity = customerMaCrm.elencoDocumenti.get(i).nazionalita;
                            check.adultDocumentOther_issueDate = customerMaCrm.elencoDocumenti.get(i).dataEmissione;
                        }
                        check.adultDocumentsS_dateOfBirth = hasIC;
                    }
                    //PASSAPORTO
                    if(hasPassport && hasIC){
                        check.adultDocumentsS_type = "PASSPORT";
                        check.adultDocumentsS_number = check.Passnumber;
                        check.adultDocumentsS_issueCountry = check.PassCountry;
                        check.adultDocumentsS_expirationDate = check.PassexpirationDate;
                    }else if(hasPassport){
                        check.adultDocumentsS_type = "PASSPORT";
                        check.adultDocumentsS_number = check.Passnumber;
                        check.adultDocumentsS_issueCountry = check.PassCountry;
                        check.adultDocumentsS_expirationDate = check.PassexpirationDate;
                    }else if(hasIC){
                        check.adultDocumentsS_type = "NATIONALID";
                        check.adultDocumentsS_number = check.CInumber;
                        check.adultDocumentsS_issueCountry = check.CICountry;
                        check.adultDocumentsS_expirationDate = check.CIexpirationDate;
                    }

                    //GREENCARD/VISTO
                    if(hasVisto){
                        check.adultDocumentsOther_type = "VISTO";
                        check.adultDocumentOther_number = check.vistoRecapnumber;
                        check.adultDocumentOther_applicableCountryCode = check.vistoRecapCountry;
                        // check.adultDocumentOther_issueDate = check.vistoRecapexpirationDate;
                    }
                    if(hasGC){
                        check.adultDocumentsOther_type = "GREENCARD";
                        check.adultDocumentsGreenCard_number = check.greenCardRecapnumber;
                        check.adultDocumentsGreenCard_expirationDate = check.greenCardRecapexpirationDate;
                    }
                    if(hasGC && hasVisto){
                        check.adultDocumentsOther_type = "VISTO";
                    }
                }

                // INDIRIZZO DI RESIDENZA
                if(customerMaCrm.infoCliente != null){
                        if(customerMaCrm.infoCliente.dataNascita!=null){
                            check.adultDocumentsS_dateOfBirth = customerMaCrm.infoCliente.dataNascita;
                        }

                        //SESSO
                        if(customerMaCrm.infoCliente.sesso == "M"){
                            check.adultDocumentsS_gender = "M";
                        }
                        if(customerMaCrm.infoCliente.sesso == "m"){
                            check.adultDocumentsS_gender = "F";
                        }
                        if(customerMaCrm.infoCliente.indirizzo!=null){
                            check.adultResidentDocument_streetAddress = customerMaCrm.infoCliente.indirizzo;
                        }
                        if(customerMaCrm.infoCliente.cap!=null){
                            check.adultResidentDocument_zipCode = customerMaCrm.infoCliente.cap;
                        }
                        if(customerMaCrm.infoCliente.nazione!=null){
                            check.adultResidentDocument_stateProvince = customerMaCrm.infoCliente.nazione;
                        }
                        if(customerMaCrm.infoCliente.stato!=null){
                            check.adultResidentDocument_countryCode = customerMaCrm.infoCliente.stato;
                        }
                        if(customerMaCrm.infoCliente.citta!=null){
                            check.adultResidentDocument_city = customerMaCrm.infoCliente.citta;
                        }
                    }

            }
        }


    return check;

});