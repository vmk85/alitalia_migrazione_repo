jQuery(document).ready(function(){
	getActivities();
	$('#estrattoContoSearch').bind('click', function(e) {
		activity = $('select#attivita').find(':selected').val();
		day = $('select#giorno').find(':selected').val();
		month = $('select#mese').find(':selected').val();
		year = $('select#anno').find(':selected').val();
		getActivities(activity, day, month, year);
	});
	return false;
});

function getActivities(activity, day, month, year){
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	params = {};

	var mmtransitionprogram = jQuery("#estratto_conto_list_activities").data("mmtransitionprogram");

	if (activity && isValidDate(day, month, year)) {
		params.activity = activity;
		params.day = day;
		params.month = month;
		params.year = year;
	}
    if(mmtransitionprogram) {
        params.mmtransitionprogram = "true";
    }
	$.ajax({
		url : url + ".estratto-conto-partial.html",
		context : document.body,
		data : params
	})
	.done(estrattoContoSearchSuccess)
	.fail(estrattoContoSearchError);
	return false;
}

function estrattoContoSearchSuccess(data) {
	jQuery("#estratto_conto_list_activities").children("div:gt(0)").remove();
	jQuery("#estratto_conto_list_activities").append(data);

	var mmTransitionRemainingPoints = jQuery("#estratto_conto_list_activities").children("#mm-transition-remaining-points");
	var mmTransitionEarnedTotalPoints = jQuery("#estratto_conto_list_activities").children("#mm-transition-earned-total-points");
	var mmTransitionSpentTotalPoints = jQuery("#estratto_conto_list_activities").children("#mm-transition-spent-total-points");
	var mmTransitionQualifyingSegment = jQuery("#estratto_conto_list_activities").children("#mm-transition-qualifying-segment");
	if(mmTransitionRemainingPoints.length > 0) {
        jQuery('.millemiglia__brief__block__info .binding-mm-remaining-miles').text(addThousandSeparator(mmTransitionRemainingPoints.text(),'.'));
        jQuery('.millemiglia__brief__block__info .binding-mm-total-miles').text(addThousandSeparator(mmTransitionEarnedTotalPoints.text(),'.'));
        jQuery('.millemiglia__brief__block__info .binding-mm-total-spent-miles').text(addThousandSeparator(mmTransitionSpentTotalPoints.text(),'.'));
        jQuery('.millemiglia__brief__block__info .binding-mm-total-qualified-miles').text("0");
        jQuery('.millemiglia__brief__block__info .binding-mm-qualified-flights').text(addThousandSeparator(mmTransitionQualifyingSegment.text(),'.'));
	}
	return false;
}

function estrattoContoSearchError(data) {
	return false;
}

function isValidDate(day, month, year) {
	if (typeof day === "undefined" || typeof month === "undefined" 
		|| typeof year === "undefined") {
		return false;
	}
	if (month > 12 || day > daysInMonth(month, year)) {
		return false;
	}
	return true;
}

function daysInMonth(m, y) {
	switch (m) {
		case 2:
			return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return 31;
		default:
			return 30
	}
}