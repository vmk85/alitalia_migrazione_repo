"use strict";
use(function() {
    var consenso = this.consensi;
    var response = {};
    response.com = "";
    response.prn = "";
    response.comFlag = "";
    response.prnFlag = "";
    if (consenso != null){
        for (var a = 0; a < consenso.size(); a++){
            if (consenso.get(a).tipoConsenso.toUpperCase() == "COM"){
                response.com = consenso.get(a).tipoConsenso;
                response.comFlag = consenso.get(a).flagConsenso;
            }if (consenso.get(a).tipoConsenso.toUpperCase() == "PRN"){
                response.prn = consenso.get(a).tipoConsenso;
                response.prnFlag = consenso.get(a).flagConsenso;
            }
        }
    }
    return response;
})