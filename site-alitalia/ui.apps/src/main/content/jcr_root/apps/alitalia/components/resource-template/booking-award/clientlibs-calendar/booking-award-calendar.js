function showErrorMessage(data) {
	
	$('#errorMessage').show();
	$('#errorMessage').addClass('isActive');
	$('#date_error').text(data.fields['dateChosen']);
}

function removeErrorMessage() {
	$('#date_error').text('');
	$('#errorMessage').hide();
	$('#errorMessage').removeClass('isActive');
}

function executeCalendarSearch(errorUrl) {
	performSubmit("bookingawardcalendarsearch", {}, function(data) {
		calendarSearchDoneCallback(data, errorUrl, true);
	}, function(){
		calendarSearchFailCallback(errorUrl);
	});
}



function calendarSearchDoneCallback(data, errorUrl, firstTime) {
	if (data.redirect) {
		window.location.replace(data.redirect);
	} else if (data.isError || !data.bAwardData) {
		window.location.replace(errorUrl);
	} else {
		displayCalendarResults(data);
		if (firstTime) {
			initCalendarBehaviours();
		}
	}
}

function calendarSearchFailCallback(errorUrl) {
	window.location.replace(errorUrl);
}

function modifySearchCall(step, flight) {
	var data = {
			step: step,
			flight: flight 
	};
	
	
	invokeService("bookingawardmodifysearch", "json", data, function(data) { modifySearchSuccess(data);}, modifySearchError);
}

function displayCalendarResults(result) {
	updateDatePicker();
	if ($('#bookingAward_andata').length > 0) {
		awardsDatePicker.init($('#bookingAward_andata'), result.bAwardData[0]);
	}
	if ($('#bookingAward_ritorno').length > 0) {
		awardsDatePicker.init($('#bookingAward_ritorno'), result.bAwardData[1]);
	}
	
	$("#booking-award-calendar").show();
	$(".waitingPage__loaderMobile").hide();
	$('.booking__informationBox').show();
	book.init();
	numberSelector();
	datePickerController.setSelectedDate("bookingAward_andata", result.bAwardData[0].selectedDate);
	datePickerController.setDisabledDates("bookingAward_andata", result.bAwardData[0].disabledDates);
	if (result.bAwardData[1]) {
		datePickerController.setSelectedDate("bookingAward_ritorno", result.bAwardData[1].selectedDate);
		datePickerController.setDisabledDates("bookingAward_ritorno", result.bAwardData[1].disabledDates);
	}
	
	
}

function checkDateFormat() {
	if (pageSettings.dateFormat == "%m/%d/%Y") {
		$('#dateFormat').val('USA');
	}
}

function initCalendarForm(modify) {
	
	$('#flightSearchButton, .bookInfoBoxBasketBtn').on('click', function(e) {
		removeErrorMessage();
		checkDateFormat();
		validation(e, 'bookingawardconfirmdate', '#flightSearchForm',
			confirmDateValidationSuccess,
			confirmDateValidationError);
	});
	
	
}

function initCalendarBehaviours() {
	
	// ANDATA
	
	$('#bookingAward_andata-prev-month-but').off('click');
	$('#bookingAward_andata-next-month-but').off('click');
	
			
	$('#bookingAward_andata-prev-month-but').on('click', function(e) {
		
		var step = 'P';
		var flight = 'A';
		modifySearchCall(step, flight);
	});
	
	$('#bookingAward_andata-next-month-but').on('click', function(e) {
		var step = 'N';
		var flight = 'A';
		modifySearchCall(step, flight);
	});
	
	// RITORNO
	
	if ($('#bookingAward_ritorno-prev-month-but') && $('#bookingAward_ritorno-next-month-but')) {
		
		$('#bookingAward_ritorno-prev-month-but').off('click');
		$('#bookingAward_ritorno-next-month-but').off('click');
		
		$('#bookingAward_ritorno-prev-month-but').on('click', function(e) {
			
			var step = 'P';
			var flight = 'R';
			modifySearchCall(step, flight);
		});
		
		$('#bookingAward_ritorno-next-month-but').on('click', function(e) {
			
			var step = 'N';
			var flight = 'R';
			modifySearchCall(step, flight);
		});
	}
	
	
}



function confirmDateValidationSuccess(data) {
	return (data.result == true ? confirmDateContinue() : showErrorMessage(data));
}

function confirmDateContinue() {
	$('#flightSearchButton').off('click');
	
	$('#flightSearchForm').submit();
}



function confirmDateValidationError() {
	console.log("Error : Validation");
}

function modifySearchSuccess(data) {
	if(data.result) {
		console.log("Success : Data Modified");
		refreshCalendar(function(){initCalendarForm();});
	}
}

function modifySearchError() {
	console.log("Error : Modify Search");
}
