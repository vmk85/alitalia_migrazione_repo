var headerSearchOptions = {};

var gsaHost,site,client,marketCode,languageCode;
var lastQuery="";


// document ready
$( document ).ready( function() {

    $('.icon--close--black').on('click',function(){console.log("x clicked");$('#search-bar__input').val("")});
} );



function myAutocomplete () {
    if($( '#search-bar__input' ).is(":focus")) {

        var q=$( '#search-bar__input' ).val();
        if (q == ""){
            $('.reveal--search .suggestion-box').empty();
            //console.log("svuoto");
        }else if(q!=lastQuery){
            lastQuery=q;
            mySearch (gsaHost,site,client,marketCode,languageCode);
        }

    }
}
 $('.search-bar__input').keyup(function() {
 if ($( '#search-bar__input' ).val() == ""){
 	$('.reveal--search .suggestion-box').empty();
 }else{
	populateGSAField ($( '#search-bar__input' ).val()); 	
 }
});


function mySearch (gsaHost, site, client, marketCode, languageCode) {
    console.log("eseguo ricerca: '" + lastQuery + "'" );

    $.ajax({
        url: gsaHost+"/suggest",
        dataType: 'jsonp',
        jsonpCallback: 'populateGSAField',
        jsonp: false,
        async: false,
        data: {
            max: "5",
            site: site+"_"+languageCode+"_"+marketCode,
            client: client+"_"+languageCode+"_"+marketCode,
            access: "p",
            format: "rich",
            callback: "populateGSAField",
            q: $( '#search-bar__input' ).val()
        }
    });
}



function populateGSAField (data) {
    $('.reveal--search .suggestion-box').empty().attr('role', 'listbox');
    $('.reveal--search .suggestion-box').append('<div class="suggestion-box__data"></div>');

    var currentinputvalue = $( '#search-bar__input' ).val();

    $.each(data.results, function(i, v) {
        var highlightedSuggestion = highlightSuggestion( v.name, currentinputvalue );
        $('.reveal--search .suggestion-box__data').append('<div class="suggestion-box__single-option" role="option" onClick="setSuggestion(\''+v.name+'\')">' + highlightedSuggestion + '</div>');
    });
}



function setSuggestion (value) {
    $('.mainMenu__navigationSearch .search-bar__input').val(value);
    $('.mainMenu__navigationSearch#suggestion_form').submit();
}



function highlightSuggestion( name, inputVal ) {
    var beforeHighlightedText = name.substr( 0, name.indexOf( inputVal ) );
    var highlightedText = name.substr( name.indexOf( inputVal ), inputVal.length );
    var afterHighlightedText = name.substr( name.indexOf( inputVal ) + inputVal.length );

    if( name.indexOf( inputVal ) >= 0 ) {
        return beforeHighlightedText + '<strong>' + highlightedText + '</strong>' + afterHighlightedText;
    } else {
        return name;
    }

}


/*

function bindAutocompleteGSA (gsaHost, site, client, marketCode, languageCode) {
    //var i = setInterval(myFunction(gsaHost, site, client, marketCode, languageCode), 1000,true);
    //myFunction(gsaHost, site, client, marketCode, languageCode);


        $( ".search-bar__input" ).autocomplete({
            lookup : function(query, done) {
                $.ajax({
                      url: gsaHost+"/suggest",
                      dataType: 'jsonp',
                      jsonpCallback: 'populateGSAField',
                      jsonp: false,
                      async: false,
                      data: {
                        max: "5",
                        site: site+"_"+languageCode+"_"+marketCode,
                        client: client+"_"+languageCode+"_"+marketCode,
                        access: "p",
                        format: "rich",
                        callback: "populateGSAField",
                        q: query
                      }
                    });
              },
              minLength: 1
          });
}

*/

