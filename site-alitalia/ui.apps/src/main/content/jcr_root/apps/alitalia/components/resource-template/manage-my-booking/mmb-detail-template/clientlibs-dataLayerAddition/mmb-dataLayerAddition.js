
function analytics_mmbPartialCallback(){
	addMmbAnalyticsData(window['analytics_mmb_step']);
	addMmbDataLayerVar();
	activateGTMOnce("mmb");
	activateWebtrendsOnce("mmb");
	if(window["DTMRule"]){
		activateDTMRule(window["DTMRule"]);
	}
}

function addMmbAnalyticsData(mmbStep){
		switch(mmbStep){
		case "1":
			if(!window["DTMRule"]){
				console.log("DTMRule empty");
				window["DTMRule"] = "Receipt";
			}
			break;
		case "2":
			window["DTMRule"] = "MMBPersonalizza";
			break;
		default:
			console.log("Invalid mmb step");
		}	
	}

function addMmbDataLayerVar(){
	if(typeof window["analytics_mmb_dl_var"] == "object"){
		addAnalyticsData(window["analytics_mmb_dl_var"]);
	}
}


function initMmb_tracking(){
	jQuery(".j-analytics-mmb-evt").off("mousedown", onMmbTrackingEvent);
	jQuery(".j-analytics-mmb-evt").on("mousedown", onMmbTrackingEvent);
	jQuery(".j-analytics-mmb-click").off("mousedown", onMmbTrackingClick);
	jQuery(".j-analytics-mmb-click").on("mousedown", onMmbTrackingClick);
	jQuery(".j-analytics-mmb-changeTab").off("mousedown", onMmbTrackingChangeTab);
	jQuery(".j-analytics-mmb-changeTab").on("mousedown", onMmbTrackingChangeTab);
	jQuery(".j-analytics-mmb-product").off("mousedown", onMmbTrackingProduct);
	jQuery(".j-analytics-mmb-product").on("mousedown", onMmbTrackingProduct);
}

/**
 * click sui tab, sui tasti modifica e sugli accordion
 */
function onMmbTrackingEvent(){
	var evtName = $(this).attr("data-evt");
	var stepName = $(this).attr("data-step");
	if(typeof window["mmb_dcs_multitrack_event"] != "undefined"){
		mmb_dcs_multitrack_event(evtName, stepName);
	}
}

/**
 * click sui tasti "scegli" dei box
 * e' possibile scatenare piu' di un evento separandoli con 
 * una virgola
 */
function onMmbTrackingClick(){
	var evtNames = $(this).attr("data-evt").split(",");
	var stepNames = $(this).attr("data-step").split(",");
	var boxNames = $(this).parents(".j-analytics-mmb-box").attr("data-box").split(",");
	for(ind in evtNames){
		var evtName = evtNames[ind];
		var stepName = stepNames[ind % stepNames.length];
		var boxName = boxNames[ind % boxNames.length];
		if(typeof window["mmb_dcs_multitrack_btn_click"] != "undefined"){
			mmb_dcs_multitrack_btn_click(evtName, stepName, boxName);
		}
	}
}

/**
 * richiamata quando i box degli ancillary sono caricati in pagina
 */
function onMmbTrackingBoxLoaded(){
	/* calcolo elenco dei box visualizzati */
	var visible_boxes = "";
	jQuery(".j-analytics-mmb-ancillary-box").each(function(ind, elem){
		visible_boxes = visible_boxes + $(elem).attr("data-box") + ";";
	});
	if(visible_boxes.length > 0){
		/*remove last ";" */
		visible_boxes = visible_boxes.slice(0, -1);
	}
	/*al cambio tab verrano settati i meta tag*/
	jQuery("#analytics-mmb-boxes").text(visible_boxes);
	
	/*tab cambiato prima che i box fossero caricati*/
	if(jQuery("#analytics-mmb-tab").text() == "1"){
		jQuery("meta[name='DCSext.mmb_v2_boxvisible']").attr("content", visible_boxes);
//		jQuery("meta[name='WT.pn_sku']").attr("content", visible_boxes);
	}
}

/**
 * adegua i valori dei tag meta quando vengono cambiati i tab
 */
function onMmbTrackingChangeTab(){
	var tab = $(this).attr("data-tab");
	jQuery("#analytics-mmb-tab").html(tab);
	if(tab == "0"){
		jQuery("meta[name='DCSext.mmb_v2_formstep']").attr("content", "CercaVolo");
		jQuery("meta[name='DCSext.mmb_v2_evento']").attr("content", "GestisciPrenotazione");
		jQuery("meta[name='DCSext.mmb_v2_boxvisible']").attr("content", "");
	} else{
		jQuery("meta[name='DCSext.mmb_v2_formstep']").attr("content", "MMBAncillari");
		jQuery("meta[name='DCSext.mmb_v2_evento']").attr("content", "PersonalizzaIlTuoVolo");
		var visible_boxes = jQuery("#analytics-mmb-boxes").text();
		jQuery("meta[name='DCSext.mmb_v2_boxvisible']").attr("content", visible_boxes);
//		jQuery("meta[name='WT.pn_sku']").attr("content", visible_boxes);
	}
}


/**
 * traccia prodotti visti/aggiunti/rimossi
 */
function onMmbTrackingProduct(){
	var prodName = $(this).attr("data-sku");
	var prodState = $(this).attr("data-state");
	var prodQt = $(this).attr("data-qt");
	if(prodQt == ""){
		/*calcola quantita'*/
		prodQt = computeMmbSelectedAncillaryQuantity(
				$(this).parents(".j-analytics-mmb-ancillary-box"));
	}
	jQuery("meta[name='WT.pn_sku']").attr("content", prodName);
	jQuery("meta[name='WT.tx_u']").attr("content", prodQt);
	jQuery("meta[name='WT.tx_e']").attr("content", prodState);
}

function computeMmbSelectedAncillaryQuantity(ancillary){
	var quantity = 1;
	var ancType = $(ancillary).attr("data-box");
	if(ancType == "assicurati_posto_migliore"){
		/*FIXME trovare numero di posti effettivamente selezionati*/
		quantity = 0;
		for (var i = 0; i < window.bookingSeatSelections.length; i++) {
			var seatSelection = window.bookingSeatSelections[i];
			quantity = 	quantity + seatSelection.passenger;
		}
	}else if(ancType == "risparmia_bagaglio"){
		quantity= $(ancillary).find(".extra-baggage-checkbox:checked").length;
	}else if(ancType == "menu_preferito"){
		quantity= $(ancillary).find(".foodOnBoard__select option:selected:not(:empty)").length;
	}else if(ancType == "sala_lounge"){
		quantity= $(ancillary).find(".lounge-checkbox:checked").length;
	}else if(ancType == "fast_track"){
		quantity= $(ancillary).find(".fast-track-checkbox:checked").length;
	}
	return quantity;
}