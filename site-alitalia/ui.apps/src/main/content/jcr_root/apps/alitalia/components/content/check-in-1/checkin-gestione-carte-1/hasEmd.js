"use strict";
use(function () {

    var segments = this.segments;
    var passenger = this.passenger;
    var hasEmd = false;


    for (var i = 0; i < segments.size(); i++) {
        var segment = segments.get((segments.size() > 1 ? i : 0));
        if(segment.passengers != null) {
            for (var x = 0; x < segment.passengers.size(); x++) {
                var pax = segment.passengers.get((segment.passengers.size() > 1 ? x : 0));
                if (pax.nome == passenger.nome && pax.cognome == passenger.cognome) {
                    if (pax.emd != null) {
                        hasEmd = true;
                        break;
                    }
                }
            }
        }
    }


    return hasEmd;

});