feedbackRow = $('.notification-row');
notification_type_succ_err = $('.feddbak-row');
var datePickers = ["PassDate","PassDateExp","docDate", "docDateExp","docDateExpGC","docBornGC","docDateVisto"];
var datePickersiD = ["","CI","PA", "GC","VS"];
var currentlyModifying = "";
var CI = "CI";
var Pass = "Pass";
var GC = "greenCardRecap";
var GreenCard = "GreenCard";
var vi = "vistoRecap";
var Visto = "Visto";
var typeOfDoc = "";
var tipo = "";
var crmRequest;
var actionDoc = {};
var saveBTN = $("#saveDatasave").clone();

$(document).ready(function() {
    globalInit.push(initDatiViaggio);
    initializeDatePickers();

    $("#addDocumentBtn").on("click", function(){
        $('html, body').animate({scrollTop:($(this).offset().top - ($(window).width() < 750 ? 150:30))}, 'slow');
        $("#addDocumentWrapper").removeClass("hide");
        $(".add-info").addClass("hide");
    });

    $("#cancelDocsBtn").on("click", function(){
        resetForm();
    });
});

function initializeDatePickers(){
    if(document.getElementById('CI') != null){
        $("select#docType option[value='carta-identita']").prop("disabled", true);
        $("#generalForm #cartaIdentita").remove();
    }
    if(document.getElementById('Pass') != null){
        $("select#docType option[value='passaporto']").prop("disabled", true);
        $("#generalForm #passaporto").remove();
    }
    if(document.getElementById('greenCardRecap') != null){
        $("select#docType option[value='green-card']").prop("disabled", true);
        $("#generalForm #greenCardDiv").remove();
    }
    if(document.getElementById('vistoRecap') != null){
        $("select#docType option[value='visto']").prop("disabled", true);
        $("#generalForm #vistoDiv").remove();
    }
    for(var i=0; i<datePickers.length; i++) {
        setDatePickers(datePickers[i],datePickersiD[a]);
    }

   for(var a=0; a<datePickersiD.length; a++) {
       addDataToWrapperCI(datePickersiD[a]);

    }
}

function resetForm(){
    $(".add-info").removeClass("hide");
    $("#addDocumentWrapper").addClass("hide").find(".docTypeWrapper").addClass("hide").end().find("#saveDoc").attr("disabled",true);
    $("#docType").val("")
}

function initDatiViaggio() {
    disableLoaderMyAlitalia();
    // performSubmit("GetMyAlitaliaCRMDataToSession","",Login.Session.success.getCrmDataDocument,Login.Session.error.getCrmData);
}

var TODAY = new Date();

$(".docInfo").on("change", function() {
    typeOfDoc = "";
    if ($(this).closest("#editCI").attr("id")=="editCI"){
        typeOfDoc = "CI"
    } if ($(this).closest("#editPass").attr("id")=="editPass"){
        typeOfDoc = "Pass"
    } if ($(this).closest("#editgreenCardRecap").attr("id")=="editgreenCardRecap"){
        typeOfDoc = "greenCardRecap"
    }
    if ($(this).closest("#editvistoRecap").attr("id")=="editvistoRecap"){
        typeOfDoc = "vistoRecap"
    }
    if(validateExpireDate()){
        validateForm();
    }

});

$(".docInfo").on("keyup", function() {
    validateForm();
});

$("#docType").on("change", function() {
    var val = $(this).val();
    currentlyModifying = val;
    $("#addDocumentWrapper .docTypeWrapper").addClass("hide");

    if(val == "carta-identita"){
        $("#addDocumentWrapper #cartaIdentita").removeClass("hide");
    }if(val == "passaporto"){
        $("#addDocumentWrapper #passaporto").removeClass("hide");
    }
    if(val == "green-card"){
        $("#addDocumentWrapper #greenCardDiv").removeClass("hide");
    }
    if(val == "visto"){
        $("#addDocumentWrapper #vistoDiv").removeClass("hide");
    }

    // clearFormOnDocChange(val);

});

function validateForm() {
    var save = "";
    switch(typeOfDoc){
        case "CI":
            docType = "carta-identita";
            save = $("#saveDocCI");
            break;
        case "Pass":
            docType = "passaporto";
            save = $("#saveDocPass");
            break;
        case "greenCardRecap":
            docType = "green-card";
            save = $("#saveDocgreenCardRecap");
            break;
        case "vistoRecap":
            docType = "vistoDiv";
            save = $("#saveDocvistoRecap");
            break;
        default:
            var docType = $("#docType").val();
            save = $("#saveDoc");
            break
    }

    var valid = true;
    if(docType == ""){
        valid = false;
    }
    if(checkDatesForm(docType) == false){
        valid = false;
    }
    if(docType == "carta-identita"){
        if( $("#docNum").val() == "" || $("#docTown").val() == ""){
            valid = false;
        }
    }
    if(docType =="passaporto"){
        if( $("#PassNum").val() == "" || $("#PassTown").val() == ""){
            valid = false;
        }
    }
    if(docType == "green-card"){
        if( $("#docNumGC").val() == "" || $("#emCountryGC").val() == "" || $("#nationalityGC").val() == "" ){
            valid = false;
        }
    }
    if(docType == "visto"){
        if( $("#docNumVisto").val() == "" || $("#docTownVisto").val() == ""){
            valid = false;
        }
    }

    if(valid){
        save.removeAttr("disabled");
    }else{
        save.attr("disabled", true);
    }
    return valid;
}

function checkDatesForm(type) {
    var dates = [];

    switch (type) {
        case "carta-identita":
            dates = ["docDate", "docDateExp"];
            break;
        case "passaporto":
            dates = ["PassDate", "PassDateExp"];
            break;
        case "green-card":
            dates = ["docDateExpGC","docBornGC"];
            break;
        case "visto":
            dates = ["docDateVisto"];
            break;
    }

    for(var i=0; i<dates.length; i++) {
        if($("#" + dates[i] + "Day").val() == "GG" || $("#" + dates[i] + "Month").val() == "MM" || $("#" + dates[i] + "Year").val() == "AAAA"  ) {
            return false;
        }
    }
    return true;
}

function checkCards(type,documento) {
    if(documento != null) {
        if(type == "init" || type=="CI") creaRiepilogoCI(documento);
    } else if(documento == null && type=="CI"){

        $("select#docType option[value='carta-identita']").prop("disabled", false);
        $("#saveDatasave").prepend($("#cartaIdentita").addClass("hide"));
        $("#cartaIdentita").find("#saveButt").remove()
    }

    if(documento != null) {
        if(type == "init" || getTipoCrmdData(type)=="Pass") creaRiepilogoPass(documento);
    } else if(documento == null && type=="Pass"){
        $("select#docType option[value='passaporto']").prop("disabled", false);
        $("#saveDatasave").prepend($("#passaporto").addClass("hide"));
        $("#passaporto").find("#saveButt").remove();
    }

    if(documento != null){
        if(type == "init" || getTipoCrmdData(type)=="greenCardRecap") creaRiepilogoGreenCard(documento);
    } else if(documento == null && type=="greenCardRecap"){
        $("select#docType option[value='green-card']").prop("disabled", false);
        $("#saveDatasave").prepend($("#greenCardDiv").addClass("hide"));
        $("#greenCardDiv").find("#saveButt").remove();
    }

    if(documento != null){
        if(type == "init" || getTipoCrmdData(type)=="vistoRecap") creaRiepilogoVisto(documento);
    } else if(documento == null && type=="vistoRecap"){
        $("select#docType option[value='visto']").prop("disabled", false);
        $("#saveDatasave").prepend($("#vistoDiv").addClass("hide"));
        $("#vistoDiv").find("#saveButt").remove();
    }

}

function creaRiepilogoCI(documento) {
    $("select#docType option[value='carta-identita']").attr("disabled", "disabled");
    var structure = "";
    var countryCode = documento.nazionalita;
    if($("#CI").length>0){
        $("#CI").remove();
    }
    structure += '<div class="widget-research mb-30 dati-viaggio__form" id="CI">';
    structure += '  <div class="inner-row">';
    structure += '    <div class="column small-12 medium-6">';
    structure += '      <h6>' + CQ.I18n.get('myalitalia.datiViaggio.riepologo.cartaIdentita') + '</h6>';
    structure += '      <h6 class="numerodocCI">' + documento.numero + '</h6>';
    if(countryCode!=null){
        structure += '      <p> '+CQ.I18n.get('myalitalia.datiViaggio.riepologo.paeseEmissione')+' <strong class="paesedocCI">' + documento.paeseEmissione  + '</strong></p>';
    }
    structure += '    </div>';
    structure += '    <div class="column small-12 medium-3 medium-offset-1 border-top mb-small-5 tar-medium editButton">';
    structure += '      <a href="#" onclick="editCI(CI)" class="edit-link db-small">';
    structure += '        <span class="pr-small-20">'+CQ.I18n.get('myalitalia.datiViaggio.riepologo.modificaDati')+'</span>';
    structure += '      </a>';
    structure += '    </div>';
    structure += '    <div class="column small-12 medium-2 border-top tar-medium">';
    structure += '      <a href="javascript:openConfirmWrap(CI);" class="delete-link db-small">';
    structure += '        <span class="pr-small-20">'+CQ.I18n.get('myalitalia.datiViaggio.riepologo.cancella')+'</span>';
    structure += '      </a>';
    structure += '    </div>';
    structure += '  </div>';
    structure += '  <div id="editCI" class=" wrapper" hidden>';
    structure += '  </div>';
    structure += '</div>';
    $(".riepilogo").append(structure);
    $("#docType").val("")
    resetForm();
    fillCard(documento);
}

function fillCard(documento){

    // var dtEmiss = documento.dataEmissione.split("-");
    // var dtExpEmiss = (documento.dataScadenza ? documento.dataScadenza.split("-"):"");
    // var dtNascitaPasseggero = (documento.dataNascitaPasseggero ? documento.dataNascitaPasseggero.split("-") : "");
    //
    // switch (getTipoCrmdData(documento.tipo)){
    //
    //     case "CI":
    //         $("#docNum").text(documento.numero).val(documento.numero);
    //         // $("#docTown").val(documento.paeseEmissione+"|"+documento.nazionalita.substring(0,1)+documento.nazionalita.substring(1,documento.nazionalita.length).toLowerCase());
    //         $("#docTown").val($("#docTown [value*="+documento.paeseEmissione+"]").val());
    //         $("#docDateDay").val(parseInt(dtEmiss[2]));
    //         $("#docDateMonth").val(parseInt(dtEmiss[1]));
    //         $("#docDateYear").val(dtEmiss[0]);
    //         $("#docDateExpDay").val(parseInt(dtExpEmiss[2]));
    //         $("#docDateExpMonth").val(parseInt(dtExpEmiss[1]));
    //         $("#docDateExpYear").val(dtExpEmiss[0]);
    //         break;
    //     case "Pass":
    //         $("#PassNum").text(documento.numero).val(documento.numero);
    //         // $("#PassTown").val(documento.paeseEmissione+"|"+documento.nazionalita.substring(0,1)+documento.nazionalita.substring(1,documento.nazionalita.length).toLowerCase());
    //         $("#PassTown").val($("#PassTown [value*="+documento.paeseEmissione+"]").val());
    //         $("#PassDateDay").val(parseInt(dtEmiss[2]));
    //         $("#PassDateMonth").val(parseInt(dtEmiss[1]));
    //         $("#PassDateYear").val(dtEmiss[0]);
    //         $("#PassDateExpDay").val(parseInt(dtExpEmiss[2]));
    //         $("#PassDateExpMonth").val(parseInt(dtExpEmiss[1]));
    //         $("#PassDateExpYear").val(dtExpEmiss[0]);
    //         break;
    //     case "greenCardRecap":
    //         $("#docNumGC").text(documento.numero).val(documento.numero);
    //         $("#emCountryGC").val($("#emCountryGC option[value*="+documento.paeseEmissione+"]").val());
    //         $("#nationalityGC").val($("#nationalityGC [value*="+documento.paeseEmissione+"]").val());
    //         $("#docBornGCDay").val(parseInt(dtNascitaPasseggero[2]));
    //         $("#docBornGCMonth").val(parseInt(dtNascitaPasseggero[1]));
    //         $("#docBornGCYear").val(dtNascitaPasseggero[0]);
    //         $("#docDateExpGCDay").val(parseInt(dtExpEmiss[2]));
    //         $("#docDateExpGCMonth").val(parseInt(dtExpEmiss[1]));
    //         $("#docDateExpGCYear").val(dtExpEmiss[0]);
    //         break;
    //     case "vistoRecap":
    //         $("#docNumVisto").val(documento.numero).text(documento.numero);
    //         $("#docTownVisto").val($("#docTownVisto option[value*="+documento.paeseEmissione+"]").val());
    //         $("#docDateVistoDay").val(parseInt(dtEmiss[2]));
    //         $("#docDateVistoMonth").val(parseInt(dtEmiss[1]));
    //         $("#docDateVistoYear").val(dtEmiss[0]);
    //         break;
    // }


}

function openConfirmWrap(el){
    $("#docDeleteMsg").text('');
    switch(el) {
        case 'Pass':
            $("#docDeleteMsg").text(CQ.I18n.get('myalitalia.datiViaggio.confirmDeletePassport.label'));
            break;
        case 'CI':
            $("#docDeleteMsg").text(CQ.I18n.get('myalitalia.datiViaggio.confirmDeleteCI.label'));
            break;
        case 'GreenCard':
            $("#docDeleteMsg").text(CQ.I18n.get('myalitalia.datiViaggio.confirmDeleteGreenCard.label'));
            break;
        case 'Visto':
            $("#docDeleteMsg").text(CQ.I18n.get('myalitalia.datiViaggio.confirmDeleteVisto.label'));
            break;
    }
    $('#reveal-offerte').foundation('open').find("#editLab").attr("onclick","delete"+el+"(true)");
}

function editCI(cas) {
    var offset = $("#"+cas).offset();
    // addModifyCI(cas);
    // addDataToWrapperCI(cas);
    // $("#"+cas+" #cartaIdentita").slideDown();
    $("#"+cas+" #saveDatasave").removeClass("hide");
    $("#"+cas+" .inner-row").first().addClass("del");
    $("#"+cas+" .editButton").hide();
    $("#"+cas+" #edit"+cas).slideDown();
    $('html, body').animate({scrollTop:(offset.top - 10)}, 'slow');
    $("[id*='Date']").trigger("change");
}

function retriveId(cas){
    switch (cas) {
        case "CI":
            cas = "cartaIdentita";
            break;
        case "Pass":
            cas = "passaporto";
            break;
        case "greenCardRecap":
            cas = "greenCardDiv";
            break;
        case "vistoRecap":
            cas = "vistoDiv";
            break;
    }
    return cas;
}

function deleteCI(mode) {
    enableLoaderMyAlitalia();
    $('#reveal-offerte').foundation('close');
    if(mode == false){
        $("#CI").remove();
    }else{
        setCRMData("CI","delete");
        // checkCards("delete");
    }
}

function creaRiepilogoPass(documento) {
    $("select#docType option[value='passaporto']").attr("disabled", "disabled");
    var structure = "";
    if($("#Pass").length>0){
        $("#Pass").remove();
    }
    structure += '<div class="widget-research mb-30 dati-viaggio__form" id="Pass">';
    structure += '  <div class="inner-row">';
    structure += '    <div class="column small-12 medium-6">';
    structure += '      <h6>' + CQ.I18n.get('myalitalia.datiViaggio.riepologo.passaporto') + '</h6>';
    structure += '      <h6 class="numerodocPass">' + documento.numero + '</h6>';
    structure += '      <p>' + CQ.I18n.get('myalitalia.datiViaggio.riepologo.paeseEmissione') + ' <strong class="paesedocPass">' + documento.paeseEmissione + '</strong></p>';
    structure += '    </div>';
    structure += '    <div class="column small-12 medium-3 medium-offset-1 border-top mb-small-5 tar-medium editButton">';
    structure += '      <a href="#"  onclick="editCI(Pass)" class="edit-link db-small">';
    structure += '        <span class="pr-small-20">' + CQ.I18n.get('myalitalia.datiViaggio.riepologo.modificaDati') + '</span>';
    structure += '      </a>';
    structure += '    </div>';
    structure += '    <div class="column small-12 medium-2 border-top tar-medium">';
    structure += '      <a href="javascript:openConfirmWrap(Pass);" class="delete-link db-small">';
    structure += '        <span class="pr-small-20">' + CQ.I18n.get('myalitalia.datiViaggio.riepologo.cancella') + '</span>';
    structure += '      </a>';
    structure += '    </div>';
    structure += '  </div>';
    structure += '  <div id="editPass" class=" wrapper" hidden>';
    structure += '  </div>';
    structure += '</div>';
    $(".riepilogo").append(structure);
    resetForm();
    fillCard(documento);
}

function deletePass(mode) {
    enableLoaderMyAlitalia();
    $('#reveal-offerte').foundation('close');
    if(mode == false){
        $("#Pass").remove();
    }else{
        setCRMData("Pass","delete");
        // checkCards("delete");
    }
}

function creaRiepilogoGreenCard(documento) {
    $("select#docType option[value='green-card']").attr("disabled", "disabled");
    var structure = "";
    if($("#greenCardRecap").length>0){
        $("#greenCardRecap").remove();
    }
    structure += '<div class="widget-research mb-30 dati-viaggio__form" id="greenCardRecap">';
    structure += '  <div class="inner-row">';
    structure += '    <div class="column small-12 medium-6">';
    structure += '      <h6>' + CQ.I18n.get('myalitalia.datiViaggio.selectOption.greenCard') + '</h6>';
    structure += '      <h6 class="numerodocgreenCardRecap">' + documento.numero + '</h6>';
    structure += '      <p> ' + CQ.I18n.get('myalitalia.datiViaggio.riepologo.paeseEmissione') + ' <strong class="paesedocgreenCardRecap">' + documento.paeseEmissione + '</strong></p>';
    structure += '    </div>';
    structure += '    <div class="column small-12 medium-3 medium-offset-1 border-top mb-small-5 tar-medium editButton">';
    structure += '      <a href="#" onclick="editCI(GC)" class="edit-link db-small">';
    structure += '        <span class="pr-small-20">'+CQ.I18n.get('myalitalia.datiViaggio.riepologo.modificaDati')+'</span>';
    structure += '      </a>';
    structure += '    </div>';
    structure += '    <div class="column small-12 medium-2 border-top tar-medium">';
    structure += '      <a href="javascript:openConfirmWrap(GreenCard);" class="delete-link db-small">';
    structure += '        <span class="pr-small-20">'+CQ.I18n.get('myalitalia.datiViaggio.riepologo.cancella')+'</span>';
    structure += '      </a>';
    structure += '    </div>';
    structure += '  </div>';
    structure += '  <div id="editgreenCardRecap" class=" wrapper" hidden>';
    structure += '  </div>';
    structure += '</div>';
    $(".riepilogo").append(structure);
    resetForm();
    fillCard(documento);
}

function deleteGreenCard(mode) {
    enableLoaderMyAlitalia();
    $('#reveal-offerte').foundation('close');
    if(mode == false){
        $("#greenCardRecap").remove();
    }else{
        setCRMData("greenCardRecap","delete");
        // checkCards("delete");
    }
}

function creaRiepilogoVisto(documento) {
    $("select#docType option[value='visto']").attr("disabled", "disabled");
    var structure = "";
    if($("#vistoRecap").length>0){
        $("#vistoRecap").remove();
    }
    structure += '<div class="widget-research mb-30 dati-viaggio__form" id="vistoRecap">';
    structure += '  <div class="inner-row">';
    structure += '    <div class="column small-12 medium-6">';
    structure += '      <h6>' + CQ.I18n.get('myalitalia.datiViaggio.selectOption.visa') + '</h6>';
    structure += '      <h6 class="numerodocvistoRecap">' + documento.numero + '</h6>';
    structure += '      <p> ' + CQ.I18n.get('myalitalia.datiViaggio.riepologo.paeseEmissione') + ' <strong class="paesedocvistoRecap">' + documento.paeseEmissione + '</strong></p>';
    structure += '    </div>';
    structure += '    <div class="column small-12 medium-3 medium-offset-1 border-top mb-small-5 tar-medium editButton">';
    structure +=  "<a href='#'  onclick='editCI(\"vistoRecap\")' class='edit-link db-small'>"
    structure += '        <span class="pr-small-20">'+CQ.I18n.get('myalitalia.datiViaggio.riepologo.modificaDati')+'</span>';
    structure += '      </a>';
    structure += '    </div>';
    structure += '    <div class="column small-12 medium-2 border-top tar-medium">';
    structure += '      <a href="javascript:openConfirmWrap(Visto);" class="delete-link db-small">';
    structure += '        <span class="pr-small-20">'+CQ.I18n.get('myalitalia.datiViaggio.riepologo.cancella')+'</span>';
    structure += '      </a>';
    structure += '    </div>';
    structure += '  </div>';
    structure += '  <div id="editvistoRecap" class=" wrapper" hidden>';
    structure += '  </div>';
    structure += '</div>';
    $(".riepilogo").append(structure);
    resetForm();
    fillCard(documento)
}

function deleteVisto(mode) {
    enableLoaderMyAlitalia();
    $('#reveal-offerte').foundation('close');
    if(mode == false){
        $("#vistoRecap").remove();
    }else{
        setCRMData("vistoRecap","delete");
        // checkCards("delete");
    }
}

function validateExpireDate(){
    try{
        var expireOn = (parseInt($("#docDateExpMonth").val())<=9?"0"+$("#docDateExpMonth").val():$("#docDateExpMonth").val()) + "/" + (parseInt($("#docDateExpDay").val())<=9?"0"+$("#docDateExpDay").val():$("#docDateExpDay").val()) + "/" + $("#docDateExpYear").val();
        expireOn = new Date(expireOn).getTime();
        if(expireOn < new Date().getTime()) {
            $(".errorField").css("display", "block");
            $("#docDateExpDay, #docDateExpMonth, #docDateExpYear").addClass("invalid-input");
            return false;
        }
        else {
            $(".errorField").css("display", "none");
            $("#docDateExpDay, #docDateExpMonth, #docDateExpYear").removeClass("invalid-input");
        }
    }
    catch(Exception){}

    return true;

}

function overWriteDataDoc(cas){
    enableLoaderMyAlitalia();
    setCRMData(cas,"edit");
    endEdit(cas);
}

function setCRMData(cas,edit){

    var docNumber;
    var dataEmissione;
    var dataScadenza;
    var paeseEmissione;
    var dataNascitaPasseggero;
    var nazionalita;
    var split;

    if (edit == "edit" || edit == "delete"){
        if (cas == "CI"){
            cas = "carta-identita";
        }
        if (cas == "Pass"){
            cas = "passaporto";
        }
        if (cas == "greenCardRecap"){
            cas = "green-card";
        }
        if (cas == "vistoRecap"){
            cas = "visto";
        }

    }

    switch (cas){
        case "carta-identita":
            tipo = "CI";
            docNumber = $("#docNum").val() || $("#docNumVisto").val() || $("#docNumGC").val();
            dataEmissione = $("#docDateYear").val() + "-" +(parseInt($("#docDateMonth").val())<=9?"0"+$("#docDateMonth").val():$("#docDateMonth").val()) + "-" + (parseInt($("#docDateDay").val())<=9?"0"+$("#docDateDay").val():$("#docDateDay").val());
            dataScadenza = $("#docDateExpYear").val() + "-" +(parseInt($("#docDateExpMonth").val())<=9?"0"+$("#docDateExpMonth").val():$("#docDateExpMonth").val()) + "-" + (parseInt($("#docDateExpDay").val())<=9?"0"+$("#docDateExpDay").val():$("#docDateExpDay").val());
            split = $("#docTown").val().split("|");
            nazionalita = split[1];
            paeseEmissione = split[0];

            break;
        case "passaporto":
            tipo = "PA";
            docNumber = $("#PassNum").val() || $("#PassNumVisto").val() || $("#PassNumGC").val();
            dataEmissione = $("#PassDateYear").val() + "-" +(parseInt($("#PassDateMonth").val())<=9?"0"+$("#PassDateMonth").val():$("#PassDateMonth").val()) + "-" + (parseInt($("#PassDateDay").val())<=9?"0"+$("#PassDateDay").val():$("#PassDateDay").val());
            dataScadenza = $("#PassDateExpYear").val() + "-" +(parseInt($("#PassDateExpMonth").val())<=9?"0"+$("#PassDateExpMonth").val():$("#PassDateExpMonth").val()) + "-" + (parseInt($("#PassDateExpDay").val())<=9?"0"+$("#PassDateExpDay").val():$("#PassDateExpDay").val());
            split = $("#PassTown").val().split("|");
            nazionalita = split[1];
            paeseEmissione = split[0];
            break;
        case "green-card":
            tipo = "GC";
            docNumber = $("#docNumGC").val();
            dataEmissione = $("#docBornGCYear").val() + "-" +(parseInt($("#docBornGCMonth").val())<=9?"0"+$("#docBornGCMonth").val():$("#docBornGCMonth").val()) + "-" + (parseInt($("#docBornGCDay").val())<=9?"0"+$("#docBornGCDay").val():$("#docBornGCDay").val());
            dataScadenza = $("#docDateExpGCYear").val() + "-" +(parseInt($("#docDateExpGCMonth").val())<=9?"0"+$("#docDateExpGCMonth").val():$("#docDateExpGCMonth").val()) + "-" + (parseInt($("#docDateExpGCDay").val())<=9?"0"+$("#docDateExpGCDay").val():$("#docDateExpGCDay").val());
            paeseEmissione = $("#emCountryGC").val().split("|");
            nazionalita = $("#nationalityGC").val().split("|");
            nazionalita = nazionalita[1];
            paeseEmissione = paeseEmissione[0];
            dataNascitaPasseggero = $("#docBornGCYear").val() + "-" + (parseInt($("#docBornGCMonth").val())<=9?"0"+$("#docBornGCMonth").val():$("#docBornGCMonth").val()) + "-" + (parseInt($("#docBornGCDay").val())<=9?"0"+$("#docBornGCDay").val():$("#docBornGCDay").val());
            break;
        case "visto":
            docNumber = $("#docNumVisto").val();
            paeseEmissione = $("#docTownVisto").val().split("|");
            split = $("#docTownVisto").val().split("|");
            nazionalita = split[1];
            paeseEmissione = split[0];
            dataEmissione = $("#docDateVistoYear").val() + "-" + (parseInt($("#docDateVistoMonth").val())<=9?"0"+$("#docDateVistoMonth").val():$("#docDateVistoMonth").val()) + "-" + (parseInt($("#docDateVistoDay").val())<=9?"0"+$("#docDateVistoDay").val():$("#docDateVistoDay").val());
            tipo = "VS";
            break;
    }
    if(edit == "delete" || documentValidation(docNumber)){
        crmRequest = { data:JSON.stringify({
                tipo: tipo,
                numero: docNumber,
                dataEmissione: dataEmissione,
                dataScadenza : dataScadenza,
                paeseEmissione: paeseEmissione != undefined ? paeseEmissione.toUpperCase():"",
                nazionalita: nazionalita != undefined ? nazionalita.toUpperCase():"",
                dataNascitaPasseggero : dataNascitaPasseggero
            })};
        if (edit == "edit") {
            performSubmit("SetMyAlitaliaCrmDataDocumenti", crmRequest, Login.Session.success.editDocumenti, Login.Session.error.setDocumenti);
            actionDoc.init = function () {
                switch (cas) {
                    case "carta-identita":
                        $(".paesedocCI").text(paeseEmissione);
                        $(".numerodocCI").text(docNumber);
                        closeWrap('CI')
                        break;
                    case "passaporto":
                        $(".paesedocPass").text(paeseEmissione);
                        $(".numerodocPass").text(docNumber);
                        closeWrap(Pass)
                        break;
                    case "green-card":
                        $(".paesedocgreenCardRecap").text(paeseEmissione);
                        $(".numerodocgreenCardRecap").text(docNumber);
                        closeWrap(GC)
                        break;
                    case "visto":
                        $(".paesedocvistoRecap").text(paeseEmissione);
                        $(".numerodocvistoRecap").text(docNumber);
                        closeWrap(vi)
                        break;
                }
                myAlitaliaOptions.feedbackSuccessWAnchor(getTipoCrmdData(tipo));
                $("#" + getTipoCrmdData(tipo)).before(notification_type_succ_err);
                performSubmit("CRMdataPercent","",Login.Session.success.dataPercent,Login.Session.error.dataPercent);
            }
        } else if(edit == "delete"){
            performSubmit("DeleteMyAlitaliaCrmDataDocumenti",crmRequest,Login.Session.success.deleteDocumenti,Login.Session.error.setDocumenti);
            actionDoc.init = function () {
                checkCards(getTipoCrmdData(tipo));
                $("#" + getTipoCrmdData(tipo)).remove();
                performSubmit("CRMdataPercent","",Login.Session.success.dataPercent,Login.Session.error.dataPercent);
            }

        } else {
            actionDoc.init = function () {
                var documento = JSON.parse(crmRequest.data);
                checkCards(getTipoCrmdData(tipo), documento);
                myAlitaliaOptions.feedbackSuccessWAnchor(getTipoCrmdData(tipo));
                $("#" + getTipoCrmdData(tipo)).before(notification_type_succ_err);
                // fillCard(documento);
                switch (tipo) {
                    case "CI":
                        $("#editCI").append($("#cartaIdentita").removeClass("hide"));
                        $("#editCI").append($(saveBTN).clone().find("#saveDoc").attr("onclick","saveData('carta-identita','edit')").removeAttr("disabled").closest("#saveDatasave").find("#cancelDocsBtn").attr("onclick","closeWrap('CI')").closest("#saveDatasave"));
                        break;
                    case "PA":
                        $("#editPass").append($("#passaporto").removeClass("hide"));
                        $("#editPass").append($(saveBTN).clone().find("#saveDoc").attr("onclick","saveData('passaporto','edit')").removeAttr("disabled").closest("#saveDatasave").find("#cancelDocsBtn").attr("onclick","closeWrap(Pass)").closest("#saveDatasave"));
                        break;
                    case "GC":
                        $("#editgreenCardRecap").append($("#greenCardDiv").removeClass("hide"));
                        $("#editgreenCardRecap").append($(saveBTN).clone().find("#saveDoc").attr("onclick","saveData('green-card','edit')").removeAttr("disabled").closest("#saveDatasave").find("#cancelDocsBtn").attr("onclick","closeWrap(GC)").closest("#saveDatasave"));
                        break;
                    case "VS":
                        $("#editvistoRecap").append($("#vistoDiv").removeClass("hide"));
                        $("#editvistoRecap").append($(saveBTN).clone().find("#saveDoc").attr("onclick","saveData('visto','edit')").removeAttr("disabled").closest("#saveDatasave").find("#cancelDocsBtn").attr("onclick","closeWrap(vi)").closest("#saveDatasave"));
                        break;
                }
                performSubmit("CRMdataPercent","",Login.Session.success.dataPercent,Login.Session.error.dataPercent);
            }
            performSubmit("SetMyAlitaliaCrmDataDocumenti",crmRequest,Login.Session.success.setDocumenti,Login.Session.error.setDocumenti);
        }
    } else {
        $("#docNum, #docNumVisto, #docNumGC").addClass("is-invalid-input");
        $(".feedback-error").css("display", "block");
        disableLoaderMyAlitalia();
    }



};

function documentValidation(docNumber){

    var reg = /^[0-9a-zA-Z]+$/;
    if(reg.test(docNumber) && docNumber.length >= 6 && docNumber.length <= 12 && validateForm() && validateExpireDate()){
        $("#docNum, #docNumVisto, #docNumGC").removeClass("is-invalid-input");
        $(".feedback-error").css("display", "none");
        return true;
    } else {
        return false;
    }

}

function closeWrap(cas){
    $("#"+cas+" #saveDatasave").addClass("hide");
    $("#"+cas+" .inner-row").first().removeClass("del");
    $("#"+cas+" .editButton").show();
    $("#edit"+cas).slideUp();
}

function saveData(id,edit) {
    enableLoaderMyAlitalia();
    setCRMData($("#docType").val()||id,edit||"");
}

$("select#docDateDay").on("change", function() {
    adaptDocDateExp($("#docDateDay").val(), $("#docDateMonth").val(), $("#docDateYear").val());
});

$("select#docDateMonth").on("change", function() {
    updateDocDate();
    adaptDocDateExp($("#docDateDay").val(), $("#docDateMonth").val(), $("#docDateYear").val());
});

$("select#docDateYear").on("change", function() {
    updateDocDate();
    adaptDocDateExp($("#docDateDay").val(), $("#docDateMonth").val(), $("#docDateYear").val());
});

$("select#docDateExpMonth").on("change", function() {
    adaptDocDateExp($("#docDateDay").val(), $("#docDateMonth").val(), $("#docDateYear").val());
});

$("select#docDateExpYear").on("change", function() {
    adaptDocDateExp($("#docDateDay").val(), $("#docDateMonth").val(), $("#docDateYear").val());
});


datePickers.forEach(function(item) {
    var day = "Day";
    var month = "Month";
    var year = "Year";

    $("select#"+item+month).on("change", function() {
        if (item.indexOf("Exp") >= 0){
            updateDocDateExp(item);
        }
        else
            updateDocDate(item);
    });

    $("select#"+item+year).on("change", function() {
        if (item.indexOf("Exp") >= 0){
            updateDocDateExp(item);
        }
        else
            updateDocDate(item);
    });
});

function clearForm(){
    checkCards("init");
    $("#docType").val("");
    $("#docType").trigger("change");

    $('.docInfo').each(function(i, obj) {
        if($(obj).is('input')){
            $(obj).val("");
        }
    });

    datePickers.forEach(function(item) {
        $("#" + item + "Day").val("GG");
        $("#" + item + "Month").val("MM");
        $("#" + item + "Year").val("AAAA");
    });

}

function clearFormOnDocChange(val){
    $('#'+val+' .docInfo').each(function(i, obj) {
        if($(obj).is('input')){
            $(obj).val("");
        }
    });

    $('#'+val+' #docTown').val("");
    $('#'+val+' #emCountryGC').val("");
    $('#'+val+' #nationalityGC').val("");
    $('#'+val+' #docTownVisto').val("");

    datePickers.forEach(function(item) {
        $('#'+val+" #" + item + "Day").val("GG");
        $('#'+val+" #" + item + "Month").val("MM");
        $('#'+val+" #" + item + "Year").val("AAAA");
    });

}

function setDatePickers(name,id) {

    // Clear options
    $("#" + name + "Day").find('option').remove();
    $("#" + name + "Day").append($("<option>", {
        value: "GG",
        text: CQ.I18n.get('myalitalia.datiViaggio.input.dataEmissione.gg')
    }));

    $("#" + name + "Month").find('option').remove();
    $("#" + name + "Month").append($("<option>", {
        value: "MM",
        text: CQ.I18n.get('myalitalia.datiViaggio.input.dataEmissione.mm')
    }));

    for(var i=1; i<=31; i++) {
        $("#" + name + "Day").append($("<option>", {
            value: i,
            text: (i<10?"0"+i.toString():i)
        }));
        if(i <= 12) {
            $("#" + name + "Month").append($("<option>", {
                value: i,
                text: (i<10?"0"+i.toString():i)
            }));
        }
    }
    var currentYear = new Date().getFullYear();
    var isExpirationDate = false;
    if (name.indexOf("Exp") >= 0){
        isExpirationDate = true;
    }
    if(name == "docDate") {
        if(isExpirationDate){
            $("#" + name + "Year").find('option').remove();
            $("#" + name + "Year").append($("<option>", {
                value: "AAAA",
                text: CQ.I18n.get('myalitalia.datiViaggio.input.dataScadenza.aaaa')
            }));
            for(var i=currentYear; i<currentYear+50; i++) {
                $("#" + name + "Year").append($("<option>", {
                    value: i,
                    text: i
                }));
            }
        }else{
            $("#" + name + "Year").find('option').remove();
            $("#" + name + "Year").append($("<option>", {
                value: "AAAA",
                text: CQ.I18n.get('myalitalia.datiViaggio.input.dataEmissione.aaaa')
            }));
            for(var i=currentYear-110; i<=currentYear; i++) {
                $("#" + name + "Year").append($("<option>", {
                    value: i,
                    text: i
                }));
            }
        }
    } else {
        if(isExpirationDate){
            $("#" + name + "Year").find('option').remove();
            $("#" + name + "Year").append($("<option>", {
                value: "AAAA",
                text: CQ.I18n.get('myalitalia.datiViaggio.input.dataEmissione.aaaa')
            }));
            for(var i = currentYear; i<currentYear+50; i++) {
                $("#" + name + "Year").append($("<option>", {
                    value: i,
                    text: i
                }));
            }
        }else{
            $("#" + name + "Year").find('option').remove();
            $("#" + name + "Year").append($("<option>", {
                value: "AAAA",
                text: CQ.I18n.get('myalitalia.datiViaggio.input.dataEmissione.aaaa')
            }));
            for(var i = currentYear-110; i<=currentYear; i++) {
                $("#" + name + "Year").append($("<option>", {
                    value: i,
                    text: i
                }));
            }
        }
    }
}

function updateDocDate(item) {
    var dayElem = $("#"+item+"Day");
    var monthElem = $("#"+item+"Month");
    var yearElem = $("#"+item+"Year");

    var day = $(dayElem).val();
    var month = $(monthElem).val();
    var year = $(yearElem).val();
    var maxDaysFeb;
    var maxDays = 31;
    var maxMonths = 12;
    if(year == "AAAA" || (parseInt(year)%4) != 0)
        maxDaysFeb = 28;
    else
        maxDaysFeb = 29;
    if(month != "MM") {
        switch(parseInt(month)) {
            case 4:
            case 6:
            case 9:
            case 11:
                maxDays = 30;
                break;
            case 2:
                maxDays = maxDaysFeb;
                break;
            default:
                maxDays = 31;
                break;
        }
    }
    if(year != "AAAA" && year==TODAY.getFullYear()) {
        maxMonths = TODAY.getMonth()+1;
        if(month!="MM" && month==TODAY.getMonth()+1)
            maxDays = TODAY.getDate();
    }

    $(dayElem).html($("<option>", {
        value: "GG",
        text: "GG"
    }));
    for(var i=1; i<=maxDays; i++) {
        $(dayElem).append($("<option>", {
            value: i,
            text: (i<=9?"0"+i.toString():i)
        }));
    }
    if($("#"+item+"Day option[value='" + day + "']").length != 0)
        $(dayElem).val(day);
    else
        $(dayElem).val("GG");

    $("#"+item+"Month").html($("<option>", {
        value: "MM",
        text: "MM"
    }));
    for(var i=1; i<=maxMonths; i++) {
        $(monthElem).append($("<option>", {
            value: i,
            text: (i<=9?"0"+i.toString():i)
        }));
    }
    if($("#"+item+"Month option[value='" + month + "']").length != 0)
        $(monthElem).val(month);
    else
        $(monthElem).val("MM");
}

function adaptDocDateExp(day, month, year) {
    var selDay = $("#docDateExpDay").val();
    var selMonth = $("#docDateExpMonth").val();
    var selYear = $("#docDateExpYear").val();
    if(year != "AAAA") {
        setData(year, parseInt(year)+100, "AAAA", "#docDateExpYear", false, selYear);
        if(selYear == year) {
            if(month != "MM") {
                setData(month, 12, "MM", "#docDateExpMonth", true, selMonth);
                if(day != "GG" && month == selMonth)
                    setData(day, getMaxDays(month, year), "GG", "#docDateExpDay", true, selDay);
                else
                    setData(1, getMaxDays(month, year), "GG", "#docDateExpDay", true, selDay);
            }
        } else {
            setData(1, 12, "MM", "#docDateExpMonth", true, selMonth);
            setData(1, getMaxDays(selMonth, selYear), "GG", "#docDateExpDay", true, selDay);
        }
    }
}

function setData(start, end, init, id, charCtrl, initVal) {
    if(start == "GG" || start == "MM" || start == "AAAA")
        start = 1;
    $(id).html($("<option>", {
        value: init,
        text: init
    }));
    if(start <= end) {
        for(var i=start; i<=end; i++) {
            if(charCtrl)
                var app = (i<=9?"0"+i.toString():i);
            $(id).append($("<option>", {
                value: i,
                text: (charCtrl?app:i)
            }));
        }
        if(initVal) {
            if($(id + " option[value='" + initVal + "']").length != 0)
                $(id).val(initVal);
            else
                $(id).val(init);
        }
    } else alert("Error");
}

function getMaxDays(month, year) {
    month = parseInt(month);
    year = parseInt(year);
    if(year != "AAAA") {
        if(month != "MM") {
            if(month == 2)
                return ((year%4)==0?29:28);
            else
                return getMaxDayFromMonth(month);
        } else return 31;
    } else if(month != "MM")
        return getMaxDayFromMonth(month);
    else return 31;

}

function getMaxDayFromMonth(month) {
    switch(parseInt(month)) {
        case 4:
        case 6:
        case 9:
        case 11:
            return 30;
        default:
            return 31;
    }
}

function updateDocDateExp() {
    var day = $("#docDateExpDay").val();
    var month = $("#docDateExpMonth").val();
    var year = $("#docDateExpYear").val();
    var maxDays = 31;
    var maxDaysFeb = 28;
    if(year != "AAAA" && (year%4) == 0)
        maxDaysFeb = 29;
    if(month != "MM") {
        switch(parseInt(month)) {
            case 4:
            case 6:
            case 9:
            case 11:
                maxDays = 30;
                break;
            case 2:
                maxDays = maxDaysFeb;
                break;
            default:
                maxDays = 31;
                break;
        }
    }
    $("#docDateExpDay").html($("<option>", {
        value: "GG",
        text: "GG"
    }));
    for(var i=1; i<=maxDays; i++) {
        $("#docDateExpDay").append($("<option>", {
            value: i,
            text: (i<=9?"0"+i.toString():i)
        }));
    }
    if($("#docDateExpDay option[value='" + day + "']").length == 0)
        $("#docDateExpDay").val("GG");
    else
        $("#docDateExpDay").val(day);
}

function AjaxRequest(service, data, method, otherData) {
    $.ajax({
        url: getServiceUrl(service),
        type: method,
        async: false,
        data: data,
        success: function(data){ console.log(data); },
        error: function() { console.log("error") }
    });
}
function success(data) {
    console.log(data);
}

function error() {
    alert("error");
}

function getServiceUrl(service, selector) {
    return cleanExtensioUrl() + "." + service + "."
        + ((selector) ? selector : "json");
}

function cleanExtensioUrl() {
    var url = location.protocol + '//' + location.host + location.pathname;
    return url.substr(0, url.lastIndexOf('.'));
}

function addModifyCI(cas){
    $("#edit"+cas).append($("#"+retriveId(cas)).removeClass("hide").append(function (){
        return ($("#saveDoc"+cas).length ? "":addSaveButton(cas))
    }));

}

function addSaveButton(cas){

    if (cas == "greenCardRecap"){
        return "<div id='saveButt' class='inner-row'>" +
            "<div class='column small-12'>" +
            "<div class='button-wrap'>" +
            "<input type='button' value="+CQ.I18n.get('myalitalia.common.salva')+" onclick='overWriteDataDoc(\"greenCardRecap\")' disabled class='button button--inline button--disabled' id='saveDoc"+cas+"'>" +
            "<input type='button' class='button button--inline button--back' onclick='endEdit(GC)' id='cancelDocsBtn"+cas+"' value="+CQ.I18n.get('myalitalia.common.annulla')+">" +
            "</div>" +
            "</div>" +
            "</div>";
    }else if(cas == "vistoRecap"){
        return "<div id='saveButt' class='inner-row'>" +
            "<div class='column small-12'>" +
            "<div class='button-wrap'>" +
            "<input type='button' value="+CQ.I18n.get('myalitalia.common.salva')+" onclick='overWriteDataDoc(\"vistoRecap\")' disabled class='button button--inline button--disabled' id='saveDoc"+cas+"'>" +
            "<input type='button' class='button button--inline button--back' onclick='endEdit(\"vistoRecap\")' id='cancelDocsBtn"+cas+"' value="+CQ.I18n.get('myalitalia.common.annulla')+">" +
            "</div>" +
            "</div>" +
            "</div>";

    } else {
        return "<div id='saveButt' class='inner-row'>" +
            "<div class='column small-12'>" +
            "<div class='button-wrap'>" +
            "<input type='button' value=" + CQ.I18n.get('myalitalia.common.salva') + " onclick='overWriteDataDoc(" + cas + ")' disabled class='button button--inline button--disabled' id='saveDoc" + cas + "'>" +
            "<input type='button' class='button button--inline button--back' onclick='endEdit("+cas+")' id='cancelDocsBtn" + cas + "' value=" + CQ.I18n.get('myalitalia.common.annulla') + ">" +
            "</div>" +
            "</div>" +
            "</div>";
    }
}

function addDataToWrapperCI(cas){

    if (cas == "PA"){
        if($("#"+cas+"DataEmissione").attr("data-content") != undefined){
            var ciDataEmissione = $("#"+cas+"DataEmissione").attr("data-content").split("-");
            $("#Pass select#PassDateYear").val(ciDataEmissione[0]);
            $("#Pass select#PassDateMonth").val(ciDataEmissione[1].substring(0,1) != "0"? ciDataEmissione[1]:ciDataEmissione[1].substring(1,2) );
            $("#Pass select#PassDateDay").val(ciDataEmissione[2].substring(0,1) != "0"? ciDataEmissione[2]:ciDataEmissione[2].substring(1,2) );
        }
        if($("#"+cas+"DataScadenza").attr("data-content") != undefined){
            var ciDataScadenza = $("#"+cas+"DataScadenza").attr("data-content").split("-");
            $("#Pass select#PassDateExpYear").val(ciDataScadenza[0]);
            $("#Pass select#PassDateExpMonth").val(ciDataScadenza[1].substring(0,1) != "0"? ciDataScadenza[1]:ciDataScadenza[1].substring(1,2) );
            $("#Pass select#PassDateExpDay").val(ciDataScadenza[2].substring(0,1) != "0"? ciDataScadenza[2]:ciDataScadenza[2].substring(1,2) );
        }
    }
    if (cas == "CI" ){
        var id = "doc";
        if($("#"+cas+"DataEmissione").attr("data-content") != undefined){
            var ciDataEmissione = $("#"+cas+"DataEmissione").attr("data-content").split("-");
            $("#CI select#"+id+"DateYear").val(ciDataEmissione[0]);
            $("#CI select#"+id+"DateMonth").val(ciDataEmissione[1].substring(0,1) != "0"? ciDataEmissione[1]:ciDataEmissione[1].substring(1,2) );
            $("#CI select#"+id+"DateDay").val(ciDataEmissione[2].substring(0,1) != "0"? ciDataEmissione[2]:ciDataEmissione[2].substring(1,2) );
        }
        if($("#"+cas+"DataScadenza").attr("data-content") != undefined){
            var ciDataScadenza = $("#"+cas+"DataScadenza").attr("data-content").split("-");
            $("#CI select#"+id+"DateExpYear").val(ciDataScadenza[0]);
            $("#CI select#"+id+"DateExpMonth").val(ciDataScadenza[1].substring(0,1) != "0"? ciDataScadenza[1]:ciDataScadenza[1].substring(1,2) );
            $("#CI select#"+id+"DateExpDay").val(ciDataScadenza[2].substring(0,1) != "0"? ciDataScadenza[2]:ciDataScadenza[2].substring(1,2) );
        }
    }
    if (cas == "GC"){
        if($("#"+cas+"DataEmissione").attr("data-content") != undefined){
            var ciDataEmissione = $("#"+cas+"DataEmissione").attr("data-content").split("-");
            $("#greenCardRecap select#docBornGCYear").val(ciDataEmissione[0]);
            $("#greenCardRecap select#docBornGCMonth").val(ciDataEmissione[1].substring(0,1) != "0"? ciDataEmissione[1]:ciDataEmissione[1].substring(1,2) );
            $("#greenCardRecap select#docBornGCDay").val(ciDataEmissione[2].substring(0,1) != "0"? ciDataEmissione[2]:ciDataEmissione[2].substring(1,2) );
        }
        if($("#"+cas+"DataScadenza").attr("data-content") != undefined){
            var ciDataScadenza = $("#"+cas+"DataScadenza").attr("data-content").split("-");
            $("#greenCardRecap select#docDateExpGCYear").val(ciDataScadenza[0]);
            $("#greenCardRecap select#docDateExpGCMonth").val(ciDataScadenza[1].substring(0,1) != "0"? ciDataScadenza[1]:ciDataScadenza[1].substring(1,2) );
            $("#greenCardRecap select#docDateExpGCDay").val(ciDataScadenza[2].substring(0,1) != "0"? ciDataScadenza[2]:ciDataScadenza[2].substring(1,2) );
        }
    } if (cas = "VS"){
        if($("#"+cas+"DataEmissione").attr("data-content") != undefined){
            var ciDataEmissione = $("#"+cas+"DataEmissione").attr("data-content").split("-");
            $("#vistoRecap select#docDateVistoYear").val(ciDataEmissione[0]);
            $("#vistoRecap select#docDateVistoMonth").val(ciDataEmissione[1].substring(0,1) != "0"? ciDataEmissione[1]:ciDataEmissione[1].substring(1,2) );
            $("#vistoRecap select#docDateVistoDay").val(ciDataEmissione[2].substring(0,1) != "0"? ciDataEmissione[2]:ciDataEmissione[2].substring(1,2) );
        }
    }
}

function endEdit(cas) {
    $("#edit"+cas).slideUp();
    $("#"+cas+" .editButton").show();
    $("#"+cas+" .inner-row").removeClass("del");
    $("#"+cas+" #saveDoc"+cas).attr("disabled",true);
}

