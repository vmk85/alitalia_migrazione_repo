
/**
 * Gestione della validazione e del submit della form
 */
$('#datiPasseggeroSubmit').on('click', passengerDataSubmit);
$('.bookInfoBoxBasketBtn').on('click', passengerDataSubmit);
$('#passengerDataForm').keypress(function(e){
	if(e.which == 13){
		return false;
	}
});



/**
 * Gestione della visualizzazione Regole tariffarie
 */
$("#aperturaregole").click(function(){
    $('html').scrollTop(0);
    setTimeout(function(){
        $('.booking__informationBox.j-bookInfoBox').addClass('opened');
        $("#basketAccordion").show();
        window.bookingHeaderStop=true;
    }, 0);
});



function scrollToAnchor(aid){
    var aTag = $("a[name='"+ aid +"']");
    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
}

function scrollDownBasketAccordion() {
     scrollToAnchor('regole');
}

function newsLetterDataSubmit(e) {

		if($('#receivenewsletter')[0].checked==true) {
            //validation(e,'newslettersubsubmit', '#passengerDataForm',null,null);
            //performSubmit('newslettersubsubmit', '#passengerDataForm',null,null);

            //debugger;
            var prefix = $( ".prefix > select option:selected" ).text();
            $("#prefix").val(prefix);
            performSubmit('newsletter-subscribe-crm-booking', '#passengerDataForm', null, null);
		}
}

function passengerDataSubmit(e) {

	$('#datiPasseggeroSubmit').off('click', passengerDataSubmit);
	$('.bookInfoBoxBasketBtn').off('click', passengerDataSubmit);
	$('#datiPasseggeroSubmit').addClass('isDisabled');
	$('.bookInfoBoxBasketBtn').addClass('isDisabled');

    // var cb=$('#receivenewsletter')[0];
    // if (cb!=null){
    // 	validation(e, 'bookingpassengersdataconsumer', '#passengerDataForm', null, passengerDataSubmitError);
    //     newsLetterDataSubmit(e);
    // }
	$("#prefix").val($( ".prefix > select option:selected" ).text());
    validation(e, 'bookingpassengersdataconsumer', '#passengerDataForm', passengerDataSubmitSuccess, passengerDataSubmitError);
}

function passengerDataSubmitSuccess(data) {

	if (data.result) {
		removeErrors();

        performSubmit('bookingpassengersdataconsumer', '#passengerDataForm',
                        function(data) {
                            window.location.replace(data.redirect);
                        },
                        passengerDataSubmitError);

		
	} else {
		$('#datiPasseggeroSubmit').on('click', passengerDataSubmit);
		$('#datiPasseggeroSubmit').removeClass('isDisabled');
		$('.bookInfoBoxBasketBtn').on('click', passengerDataSubmit);
		$('.bookInfoBoxBasketBtn').removeClass('isDisabled');
		showErrors(data);
		if(window["analytics_trackBookingErrors"] && typeof window["analytics_trackBookingErrors"] === "function"){
			window["analytics_trackBookingErrors"](data.fields, "editpassenger");
		}
		else{
			console.error("function analytics_trackBookingErrors not found");
		}
	}
}

function passengerDataSubmitError() {
	$('#datiPasseggeroSubmit').on('click', passengerDataSubmit);
	$('#datiPasseggeroSubmit').removeClass('isDisabled');
	$('.bookInfoBoxBasketBtn').on('click', passengerDataSubmit);
	$('.bookInfoBoxBasketBtn').removeClass('isDisabled');
	console.log('fail');
}


/**
 * Gestione della riga form relativa alla selezione del cuit e cuil
 */ 
$('#ARTaxInfoType').change(function(e) {
	var arTaxTypeSelected = $('#ARTaxInfoType').find(':selected').val();
	if (arTaxTypeSelected == 'NotAR') {
		$('#numeroCuitCuilFormElement').hide();
		$('#nazionalitaCuitCuilFormElement').show();
	} else {
		$('#numeroCuitCuilFormElement').show();
		$('#nazionalitaCuitCuilFormElement').hide();
	}
});

/**
 * Traccia spunta sulla checkbox newsletter
 */
function analytics_trackCheckboxCheck(){
	if($(this).prop('checked')){
		if(window['analytics_setBookingNewsletterCheckboxMeta'] && typeof window["analytics_setBookingNewsletterCheckboxMeta"] === "function"){
			window['analytics_setBookingNewsletterCheckboxMeta']();
		}
		else{
			console.error("function analytics_setBookingNewsletterCheckboxMeta not found");
		}
	}
	else{
		if(window['analytics_unsetBookingNewsletterCheckboxMeta'] && typeof window["analytics_unsetBookingNewsletterCheckboxMeta"] === "function"){
			window['analytics_unsetBookingNewsletterCheckboxMeta']();
		}
		else{
			console.error("function analytics_unsetBookingNewsletterCheckboxMeta not found");
		}
	}
}

/* Fix child and infants birth date */
$(document).ready(function() {
	$(window).trigger('load');
	$('#checkNewsletterAlitalia').on('change', analytics_trackCheckboxCheck);
	for(var i = 0; i < dataNascita.length; i++){
		if(dataNascita[i].length > 1){
			if (dataNascita[i].substring(0,1) == "0"){
				dataNascita[i] = dataNascita[i].substring(1,dataNascita[i].length)
			}
		}
	}

	$("input[data-filled='giorno_dataNascitaAdulto']").val(parseInt(dataNascita[2])!="00"? dataNascita[2]:"GG");
	$("input[data-filled='mese_dataNascitaAdulto']").val(parseInt(dataNascita[1])!="00"? dataNascita[1]:"MM");
	$("input[data-filled='anno_dataNascitaAdulto']").val(parseInt(dataNascita[0])!="0000"? dataNascita[0]:"AAAA");
});