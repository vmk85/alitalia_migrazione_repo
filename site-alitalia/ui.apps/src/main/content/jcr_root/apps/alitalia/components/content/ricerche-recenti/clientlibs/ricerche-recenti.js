$(document).ready(function() {
	try {
		if (typeof(localStorage) !== 'undefined' &&
				typeof(Storage) !== "undefined") {
			$(document).ready(function() {
	
				var items = localStorage.getItem("ricercheRecenti");		
				if (items != null && items != 'undefined' && items != '[]' &&
						items.length > 0) {
					items = JSON.parse(items);
					var length = (items.length > 3 ? 3 : items.length);
	
					for (var i = length-1; i >= 0; i--) {
						var dataRitorno = "";
						if (items[i].dataRitorno) {
							dataRitorno = " - " + items[i].dataRitorno;
						}
						
						var classeViaggio = "";
						if (pageSettings.labels[items[i].classeViaggio]) {
							classeViaggio = " - "
								+ pageSettings.labels[items[i].classeViaggio];
						}
	
						var urlSearch = '',
							items = localStorage.getItem("ricercheRecenti");		
						if (items != null && items != 'undefined' && items != '[]' && items.length > 0) {
							items = JSON.parse(items);
							var searchData = local = items[i];
						
							urlSearch =
								getServiceUrl('flightsearchconsumer') + '?' +
								'Origin=' + searchData.cittaPartenza.codiceAeroporto + '&' + 
								'Destination=' + searchData.cittaArrivo.codiceAeroporto + '&' +
								'SearchType=' + searchData.tipoViaggio + '&' +
								'DepartureDate=' + searchData.dataPartenza + '&' +
								'CUG=' + searchData.cug + '&' +
								'Adults=' + searchData.nAdulti + '&' +
								'Children=' + searchData.nBambini + '&' +
								'Infants=' + searchData.nNeonati + '&' +
								'Youths=' + searchData.nAdulti;
							
							if(searchData.dataRitorno != "") {
								urlSearch = urlSearch + '&ReturnDate=' + searchData.dataRitorno;
						    }
							if(searchData.fromSpecialOffer) {
								urlSearch = urlSearch + '&urlSearch=True';
						    }
						    if(searchData.tipoOfferteGiovani) {
						    	urlSearch = urlSearch + '&SearchType_Giovani=' + searchData.tipoOfferteGiovani;
						    }
						}
						
						var htmlRicercaRecente =
							'<li tabindex="-1">' +
								'<a href="' + urlSearch + '" id="ricercaRecente_' + i + '">' +
									'<div>' + 
										(items[i].cittaPartenza.nomeT || items[i].cittaPartenza.nome)
										+ ' - ' + 
										(items[i].cittaArrivo.nomeT || items[i].cittaArrivo.nome) + 
									'</div>' + 
									'<div>' + pageSettings.labels[items[i].tipoViaggioLabel] + '</div>' + 
									'<div>' + items[i].dataPartenza + dataRitorno + 
										classeViaggio + ' / ' + 
										items[i].nPasseggeri + ' ' + 
										(items[i].nPasseggeri == 1
												? pageSettings.labels['cercaVolo.label.passeggeri.single']
												: pageSettings.labels['cercaVolo.label.passeggeri'])
										+ '</div>' +
								'</a>' +
							 '</li>';
						$( "ul.recentMenu__list" ).prepend(htmlRicercaRecente);
					}
	
				} else {
					$( "[id='messaggioCortesia']" ).css("display", "block"); //.append('<li tabindex="-1"><div>Nessuna ricerca recente</div></li>');
				}
	
		    });
		}
		
		var recenti = $('div#recentMenu').find('div.recentMenu__container').find('ul.recentMenu__list').children();
		jQuery.each(recenti, function(i, element) {
			var $element = $(element);
			$element.bind('click', function() {
				var items = localStorage.getItem("ricercheRecenti");		
				if (items != null && items != 'undefined' && items != '[]' && items.length > 0) {
					items = JSON.parse(items);
					var searchData = local = items[i];

				    var form = document.createElement("form");
				    var origin = document.createElement("input");
				    var destination = document.createElement("input");
				    var searchType = document.createElement("input");
				    var departureDate = document.createElement("input");
				    var returnDate = document.createElement("input");
				    var cug = document.createElement("input");
				    var adults = document.createElement("input");
				    var children = document.createElement("input");
				    var infants = document.createElement("input");
				    var youths = document.createElement("input");
				    var fromSpecialOffers = document.createElement("input");
				    var searchTypeGiovani = document.createElement("input");
				    var childrenNumber = document.createElement("input");
				    var newbornNumber = document.createElement("input");

				    form.method = "POST";
				    form.action = "http://booking.alitalia.com/Booking/it_it/Flight/Search";

				    origin.value = searchData.cittaPartenza.codiceAeroporto;
				    origin.name = "Origin";
				    form.appendChild(origin);

				    destination.value = searchData.cittaArrivo.codiceAeroporto;
				    destination.name = "Destination";
				    form.appendChild(destination);

				    searchType.value = searchData.tipoViaggio;
				    searchType.name = "SearchType";
				    form.appendChild(searchType);

				    departureDate.value = searchData.dataPartenza;
				    departureDate.name = "DepartureDate";
				    form.appendChild(departureDate);

				    returnDate.value = searchData.dataRitorno;
				    returnDate.name = "ReturnDate";
				    form.appendChild(returnDate);

				    cug.value = searchData.cug;
				    cug.name = "CUG";
				    form.appendChild(cug);

				    adults.value = searchData.nAdulti;
				    adults.name = "Adults";
				    form.appendChild(adults);

				    children.value = searchData.nBambini;
				    children.name = "Children";
				    form.appendChild(children);

				    infants.value = searchData.nNeonati;
				    infants.name = "Infants";
				    form.appendChild(infants);

				    youths.value = searchData.nAdulti;
				    youths.name = "Youths";
				    form.appendChild(youths);

				    if(searchData.fromSpecialOffer) {
				    	fromSpecialOffers.value = "True";
					    fromSpecialOffers.name = "FromSpecialOffer";
					    form.appendChild(fromSpecialOffers);				    	
				    }

				    if(searchData.tipoOfferteGiovani) {
				    	searchTypeGiovani.value = searchData.tipoOfferteGiovani;
					    searchTypeGiovani.name = "SearchType_Giovani";
					    form.appendChild(searchTypeGiovani);			    	
				    }

				    childrenNumber.value = searchData.nBambini;
				    childrenNumber.name = "children_number";
				    form.appendChild(childrenNumber);

				    newbornNumber.value = searchData.nNeonati;
				    newbornNumber.name = "newborn_number";
				    form.appendChild(newbornNumber);

				    document.body.appendChild(form);
				    form.submit();
				}
			});
		});
		
	} catch(e) {}
});