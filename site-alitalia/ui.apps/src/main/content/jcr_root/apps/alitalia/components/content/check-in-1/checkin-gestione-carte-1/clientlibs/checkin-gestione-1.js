var manageRevealOptions = {};
var idx = 0;

var idxX = 0;

var isExtra = false;
var usaNazionalita = "";
var issueCountry = "";
var selectedPass = "";
var mailType = "";


var sendEmailLoading = false;
var sendSmsLoading = false;

var params = {}, queries, temp, i, l;
// Split into key/value pairs
queries = window.location.search.split("&");
// Convert the array of strings into an object
for ( i = 0, l = queries.length; i < l; i++ ) {
    temp = queries[i].split('=');
    params[temp[0]] = temp[1];
}
var flight = params["flight"];
$(document).ready(function(){

    $(".sendSingleRecap").click(function(){
        idx = $(this).attr('idx');
        var name_surname = $('.passenger_'+idx).html();
        $('#form-pdf-' + idx).append('<input type="hidden" name="name_surname" value="' + name_surname + '">');
        $('#form-pdf-' + idx).append('<input type="hidden" name="recap">');
        mailType = "Single";
    });

    $(".sendAllRecap").click(function(){
        idx = $(this).attr('idx');
        mailType = "All";
        $('#frmMultiBoards').append('<input type="hidden" name="recap">');
    });

    $(".linkEdit").each(function(k,v){
        $(this).attr("href",$(this).attr("href")+"&flight="+flight);
    });

    var is_mobile = navigator.userAgent.match(/iPhone/i)
    if (is_mobile){
    $('.icon--close').on('click', function() {
        $('.reveal-overlay').css('display','none');
        $('html').removeClass('is-reveal-open');
    });
    }

    $(".changePassengers").click(function(){

        var div = $(this).attr("data-div");
        $("#" + div).show();
        $(this).addClass("active");
        if (div == 'divComplete')
        {
            $("#divToComplete").hide();
            $(".changePassengers[data-div='divToComplete']").removeClass("active");
        }
        else
        {
            $("#divComplete").hide();
            $(".changePassengers[data-div='divComplete']").removeClass("active");
        }
        $(".manage-dropdown").click();

    });


	$(".manage-dropdown").on("click", function() {
		var dropP = $(this).attr('data-toggle');
		var $dropdown = new Foundation.Dropdown( $('#'+ dropP));
		$dropdown.close();
	});

	$(".manage-toggle").on("click", function() {
		var toggleP = $(this).attr('data-toggle');
		var $toggle = new Foundation.Toggler( $('#'+ toggleP));
	});


	$('.details__pane').on('on.zf.toggler',function(e){
		var trigRef= $(this).attr('id');
		$('a[data-toggle='+trigRef+']').closest('.row--heading').removeClass('details-expanded');
		$('a[data-toggle='+trigRef+']').removeClass('action__icon--arrow-up').addClass('action__icon--arrow-down');
		$('a[data-toggle='+trigRef+'] .toggle--open').hide();
		$('a[data-toggle='+trigRef+'] .toggle--closed').show();
	});
	$('.details__pane').on('off.zf.toggler',function(e){
		var trigRef= $(this).attr('id');
		$('a[data-toggle='+trigRef+']').closest('.row--heading').addClass('details-expanded');
		$('a[data-toggle='+trigRef+']').addClass('action__icon--arrow-up').removeClass('action__icon--arrow-down');
		$('a[data-toggle='+trigRef+'] .toggle--open').show();
		$('a[data-toggle='+trigRef+'] .toggle--closed').hide();
	});

	manageRevealOptions.reveals = [];
//	manageRevealOptions.reveals.sendAddress =  new Foundation.Reveal( $( '.reveal--manage' ), {
//		// animationIn: 'fade',
//		// animationOut: 'fade',
//	} );
//	manageRevealOptions.reveals.sendAddressdeleteCheckin =  new Foundation.Reveal( $( '.reveal--manage-delete-checkin' ), {
//		// animationIn: 'fade',
//		// animationOut: 'fade',
//	} );
	if ($( '#manage-send-address-all' ).length) {
		$( '#manage-send-address-all' ).each(function(i, el){
			var singleReveal = new Foundation.Reveal( $( el ), {
		// animationIn: 'fade',
		// animationOut: 'fade',
	} );
			manageRevealOptions.reveals.push(singleReveal);
	} );
	}

	var singleReveal = new Foundation.Reveal( $( "#reveal--manage-send-address-success" ), {
		// animationIn: 'fade',
		// animationOut: 'fade',
	} );
    manageRevealOptions.reveals.push(singleReveal);

	if ($( 'div[idx]' ).length) {
		$( 'div[idx]' ).each(function(i, el){
			var singleReveal = new Foundation.Reveal( $( el ), {
		// animationIn: 'fade',
		// animationOut: 'fade',
	} );
			manageRevealOptions.reveals.push(singleReveal);
	} );
	}

	$(".delete").click(function(){

            idx = $(this).attr("idx");

            deleteCheckin(idx);

    });

    $('.close-button').on('click',function(){
        $('.success-').attr('style','display: none;');
    });

    $("select[name='frequentFlyer']").change(function(){

        var form = $(this).closest('form');
        form.find("input[name='codeFFchanged']").val($(this).val());
        //console.log("Change FF");

    });

    $( "input[name='frequentFlyerNumber']" ).keyup(function() {
        var form = $(this).closest('form');
        form.find("input[name='numberFFchanged']").val($(this).val());
        //console.log("Change FF Number");
    });

	$(".update").click(function(){

            idx = $(this).attr("idx");

            updateFF(idx);

    });

    $("#btPrintAllPdfGestione").click(function(){

        enableLoaderCheckin();
        performSubmit('getmoreboardinigpassinfo','#frmMultiBoards', printMultiPdfSuccess, printMultiPdfFail);

    });

    $(".printGestione").click(function(){

        var idx = $(this).attr("idx");

        printPdf(idx);
    });

    $(".addMail").on("click", function(){
        var idx= $(this).attr("idx");
        addMail(idx);
        checkSendList(this);
    });

    $('.addTelGestioneKeyUp').keyup(function(e)
    {
        if(e.keyCode == 13)
        {
            var idx= $(this).attr("idx");
            addTel(idx);
            checkSendList(this);
        }
    });

    $(".addTelGestione").on("click", function(e){
        var idx= $(this).attr("idx");
        addTel(idx);
        checkSendList(this);
    });

    $('#txtEmail').keyup(function(e)
    {
        if(e.keyCode == 13)
            addMail();
    });


    $(".sendAllGestione").click(function(){
        idx = $(this).attr('idx');
        mailType = "All";
    });

    $(".sendSingleGestione").click(function(){
        idx = $(this).attr('idx');
        mailType = "Single";
    });

     $("#btSendMailGestione").on('click',function(){

        if (mailType == "Single") {
            performSubmit('getboardinigpassinfo','#form-pdf-' + idx, boardingPassSuccess(idx), boardingPassFail);
            enableLoaderSendAddress();
        }
        if (mailType == "All") {
            sendEmailLoading = true;
            performSubmit('getmoreboardinigpassinfo','#frmMultiBoards', boardingPassMultipleSuccess, boardingPassMultipleFail);
            var tels = [];
            $(".telList-all").find("span.telNumber").each(function(){
                tels.push($(this).html());
            });
            if(tels.length > 0)
            {
                $("#tel-all").val(tels.toString());
                sendSmsLoading = true;
                performSubmit('checkisendmoreboardyngpassbyphone','#frmSendSmsAll', sendTelSuccess, sendTelFail);
            }
            enableLoaderSendAddress();
        }

    });


    $('.button--back').click(function(){
        $('.close-button').trigger('click');
    });

    $('.icon.icon--reset').click(function() {
        idx=$(this).attr('idx');
        $('#ff-' + idx).click();
    });

    /* GESTIONE PASSEGGERI */

    $('.reveal-wrap').foundation();
    // giorni nelle dropdown giorno
    for(var i=1; i<=31; i++)
    {
        $(".days").append("<option value='" + ("0" + i).slice(-2) + "'>" + ("0" + i).slice(-2) + "</option>");
    }

    // mesi nelle dropdown mese
    //for(var i=1; i<=12; i++)
    //{
    //$(".months").append("<option value='" + ("0" + i).slice(-2) + "'>" + ("0" + i).slice(-2) + "</option>");
    //}
    //new mese

    var currentDate = new Date();

    var i18nroot = "common.monthsOfYear.";
    var months = ["months", CQ.I18n.get(i18nroot + "january"), CQ.I18n.get(i18nroot + "february"), CQ.I18n.get(i18nroot + "march"), CQ.I18n.get(i18nroot + "april"), CQ.I18n.get(i18nroot + "may"), CQ.I18n.get(i18nroot + "june"), CQ.I18n.get(i18nroot + "july"), CQ.I18n.get(i18nroot + "august"), CQ.I18n.get(i18nroot + "september"), CQ.I18n.get(i18nroot + "october"), CQ.I18n.get(i18nroot + "november"), CQ.I18n.get(i18nroot + "december")];

    var startMonth = 1;

    var iM;
    for (iM = startMonth ; iM <=  12; iM++) {
        $(".months").append('<option value="'  + ("0" + iM).slice(-2) + '">' + months[iM] + " "  + '</option>');
    }


    //new mese


    var year = (new Date()).getFullYear();

    // anni nel dropdown scadenza
    for(var i=year-10; i<=year+20; i++)
    {
        $(".years-scad").append("<option value='" + i + "'>" + i + "</option>");
    }

    // anni nel dropdown nascita
    for(var i=year - 110; i<=year; i++)
    {
        $(".years-born").append("<option value='" + i + "'>" + i + "</option>");
    }

    // In base ai vari input seleziono i valori dei dropdown delle date
    $(".dateToSplit").each(function()
    {

        try
        {
            var value = $(this).val();
            //console.log($(this).attr('id') + "=" + $(this).val());
            if (value != '')
            {
                var dt = value.split("T")[0].split("-");
                var fName = $(this).attr('idx');
                $("#day-" + fName).val(dt[2]);
                $("#month-" + fName).val(dt[1]);
                $("#year-" + fName).val(dt[0]);
            }
        }
        catch(err)
        {
        }

    });

    $('#btProcediSubmit').bind('click', savePassengers);

    $('.btConfirm').bind('click', confirmPassenger);

    $('#btAddPassenger').bind('click', addPassenger);

    $('.passengerLinkEdit').bind('click', showHideWrapContent);

    $( "#procediCotiPax" ).click(function() {
        enableLoaderCheckin();
        performSubmit('checkinselectedpassenger','#form-passengers',savePassengersSuccess,savePassengersFail);
    });

    // controlli sulle varie dati per validazione
    $(".checkDate").change(function(){

        var idx = $(this).attr("idx");
        var type = $(this).attr("type");
        var day = $("#day-" + type + "-" + idx).val();
        var month = $("#month-" + type + "-" + idx).val();
        var year = $("#year-" + type + "-" + idx).val();
        if (month != '' && year != '')
        {
            var old = 0;
            var days = getNumberOfDays(year, parseInt(month) - 1);
            if (days > 0)
            {
                old = $("#day-" + type + "-" + idx).val();
                $("#day-" + type + "-" + idx + ' option').each(function() {
                    if ( $(this).val() != '' ) {
                        $(this).remove();
                    }
                });
            }
            for(var i=1; i<=days; i++)
            {
                var sel = i == old ? 'selected' : '';
                $("#day-" + type + "-" + idx).append("<option value='" + ("0" + i).slice(-2) + "' " + sel + ">" + ("0" + i).slice(-2) + "</option>");
            }
        }

    });

    $("select[name='frequentFlyer']").change(function(){

        var form = $(this).closest('form');
        form.find("input[name='codeFFchanged']").val($(this).val());
        //console.log("Change FF");

    });

    $("select[name='gender']" ).change(function(e) {
        var gender = $(this).find('option:selected').val();
        $('.passengerSex select').val(gender).trigger('change');
    });




    $( "input[name='frequentFlyerNumber']" ).keyup(function() {
        var form = $(this).closest('form');
        form.find("input[name='numberFFchanged']").val($(this).val());
        //console.log("Change FF Number");
    });



    // Se al caricamento della pagina il passeggero è selezionato allora devo settare il campo hidden a true
    $(".chkPassenger").each(function(){

        var idx = $(this).attr("idx");

        var passengerType = $("#passenger_type_" + idx).val();
        var numDocsReq = $("#numDocumentsRequired-" + idx).val();

        //if(passengerType=="CHD"){
        if(numDocsReq>0){
            $(".passengerDataError"+idx).show();
            $(".passengerDataSuccess"+idx).addClass('hide');
            $("#passengerData" + idx).removeClass("passenger-wrap-completed");
        }else{
            $(".passengerDataError"+idx).hide();
            $(".passengerDataSuccess"+idx).removeClass('hide');
            $("#passengerData" + idx).addClass("passenger-wrap-completed");
        }
        /*}else{
            if(hasDocsReq>0){
                $("#passengerDataError"+idx).show();
            }else{
                $("#passengerDataError"+idx).hide();
            }
        }*/



        var checkBox = $("#cb_" + idx);

        if (checkBox.is(':checked')){
            $("#passenger_checked_" + idx).val("true");
            $("#passengerLinkEdit"+idx).removeClass('hide');
        }else{
                $("#passenger_checked_" + idx).val("");
            $("#passengerLinkEdit"+idx).addClass('hide');
        }

    });

    // setto il passeggero selezionato prima di fare il checkin
    $("input.chkPassenger").click(function(){ //modifica per revert

        var idx = $(this).attr("idx");

        //var isChecked = $(this).hasClass("checked");
        var isChecked = $("#passenger_checked_" + idx).val();

        //console.log("isChecked = " + isChecked);

        if (isChecked)
        {
            $(this).removeClass("checked");
            $("#passengerLinkEdit"+idx).addClass('hide');
            // $("[idx="+idx+"]").removeClass("checked");
            $("#cb_" + idx).attr("checked", false);
            $("#passenger_checked_" + idx).val("");
            $('#form-passenger-'+idx).find('.passenger-wrap-content').slideUp();
        }
        else
        {
            $(this).addClass("checked");
            $("#passengerLinkEdit"+idx).removeClass('hide');
            // $("[idx="+idx+"]").addClass("checked");
            $("#cb_" + idx).attr("checked", true);
            $("#passenger_checked_" + idx).val("true");
        }

        verifyCheckin();
    });

    //check modifica dati passengero
    $('.passenger-wrap-content a.close').on('click',function(){

        $(this).closest('.passenger-wrap-content').slideUp();
        event.stopPropagation();
        /*
        $('.field-wrap.checked').removeClass('checked');
        setTimeout(function(){
            $('.chkPassenger').trigger('click');
        }, 1000);*/
    });
    $('.interactive.passengerLinkEdit a.edit').on('click',function(){

        $(this).closest('.passenger-wrap-content').slideDown();
        $('html, body').animate({scrollTop:$(this).closest('.passenger-wrap').offset().top - 10}, 'slow');
        var relatedContent = $(this).closest('.passenger-wrap').find('.passenger-wrap-content');
        if (relatedContent.length > 0 && relatedContent.is(':visible')) {
            relatedContent.slideUp();
        } else {
            relatedContent.slideDown();
        }
        event.stopPropagation();

        /*
        if ($('.field-wrap').hasClass('checked')){
            $('.field-wrap').removeClass('checked');
            setTimeout(function(){
                $('.chkPassenger').trigger('click');
            }, 1000);
        }
        */
    });

    // Nascondo tutti i passeggeri che hanno già effettuato chekcin
    //$(".passenger-wrap-completed").hide(); -> aggiunto il controllo nel template

    getDropdownData({"frequentFlyerTypes": ""}, dropdownSuccess, dropdownFail);

    verifyCheckin();
    $(".destinationUs").each(function(){
        var idx = $(this).attr("idx");
        usaNazionalita = $("#nationalityUS-"+idx).val();
        issueCountry = $("#issueCountry-"+idx).val();
        checkUsaResidenzaNazionalita(idx);
    });


    $("select[name=nationality_0]").on("change", function() {
        var passID = $(this).attr("id").split('-')[1];
        usaNazionalita = $(this).val();
        issueCountry = $("#issueCountry-"+passID).val();
        $("#nationalityUSInf-"+passID).val(usaNazionalita);
        checkUsaResidenzaNazionalita(passID);
    });

    $("select[name=issueCountry_0]").on("change", function() {
        var passID = $(this).attr("id").split('-')[1];
        usaNazionalita = $("#nationalityUS-"+passID).val();
        issueCountry = $(this).val();
        checkUsaResidenzaNazionalita(passID);
    });

    $("select[name=documentTypeAdult]").on("change", function() {
        var passID = $(this).attr("idx");
        checkIfEsta($(this).val(), passID,"documentTypeAdult-wrapper");
    });

    $("select[name=documentTypeInfant]").on("change", function() {
        var passID = $(this).attr("idx");
        checkIfEsta($(this).val(), passID,"documentTypeInfant-wrapper");
    });

    fixEstaBox();

    $.each($("[name*='year-born-'][type='born']"), function(i, el) {
        $(el).change(function() {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "born")
        });
    });

    $.each($("[name*='month-born-'][type='born']"), function(i, el) {
        $(el).change(function() {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "born")
        });
    });

    $.each($("[name*='day-born-'][type='born']"), function(i, el) {
        $(el).change(function() {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "born")
        });
    });

    $.each($("[name*='year-scad-'][type='scad']"), function(i, el) {
        $(el).change(function() {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad")
        });
    });

    $.each($("[name*='month-scad-'][type='scad']"), function(i, el) {
        $(el).change(function() {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad")
        });
    });

    $.each($("[name*='day-scad-'][type='scad']"), function(i, el) {
        $(el).change(function() {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad")
        });
    });

    $.each($("[name*='year-scad-extra-'][type='scad-extra']"), function(i, el) {
       $(el).change(function() {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad-extra")
        });
    });

    $.each($("[name*='month-scad-extra'][type='scad-extra']"), function(i, el) {
        $(el).change(function() {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad-extra")
        });
    });

    $.each($("[name*='day-scad-extra-'][type='scad-extra']"), function(i, el) {
        $(el).change(function() {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad-extra")
        });
    });

    $.each($("[name*='year-scad-greencard-'][type='scad-greencard']"), function(i, el) {
        $(el).change(function() {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad-greencard")
        });
    });

    $.each($("[name*='month-scad-greencard'][type='scad-greencard']"), function(i, el) {
        $(el).change(function() {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad-greencard")
        });
    });

    $.each($("[name*='day-scad-greencard-'][type='scad-greencard']"), function(i, el) {
        $(el).change(function() {
            var idx = $(el).attr("idx");
            updateDateSelect(idx, "scad-greencard")
        });
    });

    setExpiredDocs();

});




var YEAR = new Date().getFullYear();
var MONTH = new Date().getMonth()+1;
var DAY = new Date().getDate();

function getMonthText(month) {
    var i18nroot = "common.monthsOfYear.";
    var months = ["months", CQ.I18n.get(i18nroot + "january"), CQ.I18n.get(i18nroot + "february"), CQ.I18n.get(i18nroot + "march"), CQ.I18n.get(i18nroot + "april"), CQ.I18n.get(i18nroot + "may"), CQ.I18n.get(i18nroot + "june"), CQ.I18n.get(i18nroot + "july"), CQ.I18n.get(i18nroot + "august"), CQ.I18n.get(i18nroot + "september"), CQ.I18n.get(i18nroot + "october"), CQ.I18n.get(i18nroot + "november"), CQ.I18n.get(i18nroot + "december")];
    return months[month];
}

function setDropDown(start, end, sel, ctrl, initValue, initText, isMonth, setStart) {
    var curr;
    if(setStart) {
        curr = $(sel).val();
    }
    $(sel).html($("<option>", {
        value: initValue,
        text: initText
    }));
    for(var i=start; i<=end; i++) {
        var val = (ctrl && i<=9 ? "0" + i : i);
        $(sel).append($("<option>", {
            value: val,
            text: (isMonth ? getMonthText(i) : val)
        }));
    }
    if(setStart && $(sel + " option[value='" + curr + "']").length > 0) $(sel).val(curr);
    //else $(sel).val(initValue);
}

function setExpiredDocs() {
    var types = ["scad", "scad-greencard", "scad-greencard-inf"];
    $.each(types, function(i, type) {
        $.each($("[name*='year-" + type + "-'][type='" + type + "']"), function(i, el) {
            var idx = $(el).attr("idx");
            setDropDown(YEAR, YEAR+20, "[name='year-" + type + "-" + idx + "']", false, "", CQ.I18n.get("specialpage.myexperience.year.label"), false, true);
            if($(el).val() > 0) {
                if($(el).val() == YEAR){
                    setDropDown(MONTH, 12, "[name='month-" + type + "-" + idx + "']", true, "", CQ.I18n.get("specialpage.myexperience.month.label"), true, true);
                    setDropDown(DAY, getDayFromMonth(MONTH), "[name='day-" + type + "-" + idx + "']", true, "", CQ.I18n.get("specialpage.myexperience.day.label"), false, true);
                }else{
                    setDropDown(1, 12, "[name='month-" + type + "-" + idx + "']", true, "", CQ.I18n.get("specialpage.myexperience.month.label"), true, true);
                    setDropDown(1, getDayFromMonth(1), "[name='day-" + type + "-" + idx + "']", true, "", CQ.I18n.get("specialpage.myexperience.day.label"), false, true);
                }
            } else {
                setDropDown(1, 12, "[name='month-" + type + "-" + idx + "']", true, "", CQ.I18n.get("specialpage.myexperience.month.label"), true, true);
                setDropDown(1, getDayFromMonth(1), "[name='day-" + type + "-" + idx + "']", true, "", CQ.I18n.get("specialpage.myexperience.day.label"), false, true);
                $("[name='month-" + type + "-" + idx + "']").val("");
                $("[name='day-" + type + "-" + idx + "']").val("");
            }
        });
    });
}

function updateDateSelect(index, type) {
    var day = "[name='day-" + type + "-" + index + "'][type='" + type + "']";
    var month = "[name='month-" + type + "-" + index + "'][type='" + type + "']";
    var year = "[name='year-" + type + "-" + index + "'][type='" + type + "']";
    if($(year).val() == YEAR) {
        var mv = $(month).val();
        if(type == "born") setDropDown(1, MONTH, month, true, "", CQ.I18n.get("specialpage.myexperience.month.label"), true);
        else if(type.substring(0, 4) == "scad") setDropDown(MONTH, 12, month, true, "", CQ.I18n.get("specialpage.myexperience.month.label"), true);
        if($(month + " option[value='" + mv + "']").length > 0) $(month).val(mv);
        if($(month).val() == MONTH) {
            var dv = $(day).val();
            if (type == "born") setDropDown(1, DAY-1, day, true, "", CQ.I18n.get("specialpage.myexperience.day.label"));
            else if(type.substring(0, 4) == "scad") setDropDown(DAY+1, getDayFromMonth($(month).val()), day, true, "", CQ.I18n.get("specialpage.myexperience.day.label"));
            if($(day + " option[value='" + dv + "']").length > 0) $(day).val(dv);
        } else {
            var dv = $(day).val();
            if(type == "born") setDropDown(1, getDayFromMonth($(month).val(), $(year).val()), day, true, "", CQ.I18n.get("specialpage.myexperience.day.label"));
            else if(type.substring(0, 4) == "scad") setDropDown(1, getDayFromMonth($(month).val(), $(year).val()), day, true, "", CQ.I18n.get("specialpage.myexperience.day.label"));
            if($(day + " option[value='" + dv + "']").length > 0) $(day).val(dv);
        }
    } else {
        var mv = $(month).val();
        setDropDown(1, 12, month, true, "", CQ.I18n.get("specialpage.myexperience.month.label"), true);
        if($(month + " option[value='" + mv + "']").length > 0) $(month).val(mv);
        var dv = $(day).val();
        setDropDown(1, getDayFromMonth($(month).val(), $(year).val()), day, true, "", CQ.I18n.get("specialpage.myexperience.day.label"));
        if($(day + " option[value='" + dv + "']").length > 0) $(day).val(dv);
    }
}

function getDayFromMonth(month, year) {
    try {
        switch(parseInt(month)) {
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                return (year!="" && parseInt(year)%4==0 ? 29 : 28);
            default: return 31;
        }
    } catch(e) {
        return 31;
    }
}

function fixEstaBox(){
    $("select[name=documentTypeInfant]").each(function(i,obj){
        var passID = $(this).attr("idx");
        checkIfEsta($(obj).val(),passID,"documentTypeInfant-wrapper");
    });

    $("select[name=documentTypeAdult]").each(function(i,obj){
        var passID = $(this).attr("idx");
        checkIfEsta($(obj).val(),passID,"documentTypeAdult-wrapper");
    });
}

function checkUsaResidenzaNazionalita(passID){
    if(passID!=""){
        if((usaNazionalita == "" && issueCountry=="") || (usaNazionalita == "US" && issueCountry=="US")){
            $("#form-passenger-"+passID+" .destination-wrapper").hide();
            $("#form-passenger-"+passID+" .destination-wrapper input").addClass('hide');
            $("#form-passenger-"+passID+" .destination-wrapper select").addClass('hide');

            $("#form-passenger-"+passID+" .residenza-wrapper").hide();
            $("#form-passenger-"+passID+" .residenza-wrapper input").addClass('hide');
            $("#form-passenger-"+passID+" .residenza-wrapper select").addClass('hide');

            $("#form-passenger-"+passID+" .otherDocuments-wrapper").hide();
            $("#form-passenger-"+passID+" .otherDocuments-wrapper input").addClass('hide');
            $("#form-passenger-"+passID+" .otherDocuments-wrapper select").addClass('hide');

        }else if(usaNazionalita != "" && issueCountry!=""){
            $("#form-passenger-"+passID+" .destination-wrapper").show();
            $("#form-passenger-"+passID+" .destination-wrapper input").removeClass('hide');
            $("#form-passenger-"+passID+" .destination-wrapper select").removeClass('hide');

            $("#form-passenger-"+passID+" .residenza-wrapper").show();
            $("#form-passenger-"+passID+" .residenza-wrapper input").removeClass('hide');
            $("#form-passenger-"+passID+" .residenza-wrapper select").removeClass('hide');

            $("#form-passenger-"+passID+" .otherDocuments-wrapper").show();
            $("#form-passenger-"+passID+" .otherDocuments-wrapper input").removeClass('hide');
            $("#form-passenger-"+passID+" .otherDocuments-wrapper select").removeClass('hide');
            fixEstaBox();
        }else{
            $("#form-passenger-"+passID+" .destination-wrapper").hide();
            $("#form-passenger-"+passID+" .destination-wrapper input").addClass('hide');
            $("#form-passenger-"+passID+" .destination-wrapper select").addClass('hide');

            $("#form-passenger-"+passID+" .residenza-wrapper").hide();
            $("#form-passenger-"+passID+" .residenza-wrapper input").addClass('hide');
            $("#form-passenger-"+passID+" .residenza-wrapper select").addClass('hide');

            $("#form-passenger-"+passID+" .otherDocuments-wrapper").hide();
            $("#form-passenger-"+passID+" .otherDocuments-wrapper input").addClass('hide');
            $("#form-passenger-"+passID+" .otherDocuments-wrapper select").addClass('hide');
        }
    }else{
        if((usaNazionalita == "" && issueCountry=="") || (usaNazionalita == "US" && issueCountry=="US")){
            $(".destination-wrapper").hide();
            $(".destination-wrapper input").addClass('hide');
            $(".destination-wrapper select").addClass('hide');

            $(".residenza-wrapper").hide();
            $(".residenza-wrapper input").addClass('hide');
            $(".residenza-wrapper select").addClass('hide');

            $(".otherDocuments-wrapper").hide();
            $(".otherDocuments-wrapper input").addClass('hide');
            $(".otherDocuments-wrapper select").addClass('hide');
        }else if(usaNazionalita != "" && issueCountry != ""){
            $(".destination-wrapper").show();
            $(".destination-wrapper input").removeClass('hide');
            $(".destination-wrapper select").removeClass('hide');

            $(".residenza-wrapper").show();
            $(".residenza-wrapper input").removeClass('hide');
            $(".residenza-wrapper select").removeClass('hide');

            $(".otherDocuments-wrapper").show();
            $(".otherDocuments-wrapper input").removeClass('hide');
            $(".otherDocuments-wrapper select").removeClass('hide');
            fixEstaBox();
        }else{
            $(".destination-wrapper").hide();
            $(".destination-wrapper input").addClass('hide');
            $(".destination-wrapper select").addClass('hide');

            $(".residenza-wrapper").hide();
            $(".residenza-wrapper input").addClass('hide');
            $(".residenza-wrapper select").addClass('hide');

            $(".otherDocuments-wrapper").hide();
            $(".otherDocuments-wrapper input").addClass('hide');
            $(".otherDocuments-wrapper select").addClass('hide');
        }
    }

}

function checkIfEsta(val,id,type){
    if(id!=""){
        if(val=="esta"){
            showEsta(true,id,type);
            showVisto(false,id,type);
            showGreenCard(false,id,type);
        }
        if(val=="visto"){
            showVisto(true,id,type);
            showEsta(false,id,type);
            showGreenCard(false,id,type);
        }
        if(val=="green"){
            showGreenCard(true,id,type);
            showEsta(false,id,type);
            showVisto(false,id,type);
        }
    }else{
        if(val=="esta"){
            showEsta(true,"",type);
            showVisto(false,"",type);
            showGreenCard(false,"",type);
        }
        if(val=="visto"){
            showVisto(true,"",type);
            showEsta(false,"",type);
            showGreenCard(false,"",type);
        }
        if(val=="green"){
            showGreenCard(true,"",type);
            showEsta(false,"",type);
            showVisto(false,"",type);
        }
    }

}

function showEsta(val,id,type){
    if(id!=""){
        if(val){
            $("#form-passenger-"+id+" ."+type+" .estaChkbox-wrapper").show();
            $("#form-passenger-"+id+" ."+type+" .estaChkbox-wrapper input").removeClass('hide');
            $("#form-passenger-"+id+" ."+type+" .estaChkbox-wrapper select").removeClass('hide');
        }else{
            $("#form-passenger-"+id+" ."+type+" .estaChkbox-wrapper").hide();
            $("#form-passenger-"+id+" ."+type+" .estaChkbox-wrapper input").addClass('hide');
            $("#form-passenger-"+id+" ."+type+" .estaChkbox-wrapper select").addClass('hide');
        }
    }else{
        if(val){
            $("."+type+" .estaChkbox-wrapper").show();
            $("."+type+" .estaChkbox-wrapper input").removeClass('hide');
            $("."+type+" .estaChkbox-wrapper select").removeClass('hide');
        }else{
            $("."+type+" .estaChkbox-wrapper").hide();
            $("."+type+" .estaChkbox-wrapper input").addClass('hide');
            $("."+type+" .estaChkbox-wrapper select").addClass('hide');
        }
    }
}
function showVisto(val,id,type){
    if(id!=""){
        if(val){
            $("#form-passenger-"+id+" ."+type+" .document-type-info-wrapper input").removeClass('hide');
            $("#form-passenger-"+id+" ."+type+" .document-type-info-wrapper select").removeClass('hide');
            $("#form-passenger-"+id+" ."+type+" .document-type-info-wrapper").show();
        }else{
            $("#form-passenger-"+id+" ."+type+" .document-type-info-wrapper input").addClass('hide');
            $("#form-passenger-"+id+" ."+type+" .document-type-info-wrapper select").addClass('hide');
            $("#form-passenger-"+id+" ."+type+" .document-type-info-wrapper").hide();
        }
    }else{
        if(val){
            $("."+type+" .document-type-info-wrapper").show();
            $("."+type+" .document-type-info-wrapper input").removeClass('hide');
            $("."+type+" .document-type-info-wrapper select").removeClass('hide');;
        }else{
            $("."+type+" .document-type-info-wrapper").hide();
            $("."+type+" .document-type-info-wrapper input").addClass('hide');
            $("."+type+" .document-type-info-wrapper select").addClass('hide');;
        }
    }
}
function showGreenCard(val,id,type){
    if(id!=""){
        if(val){
            $("#form-passenger-"+id+" ."+type+" .document-type-info-wrapper-greenCard input").removeClass('hide');
            $("#form-passenger-"+id+" ."+type+" .document-type-info-wrapper-greenCard select").removeClass('hide');
            $("#form-passenger-"+id+" ."+type+" .document-type-info-wrapper-greenCard").show();
        }else{
            $("#form-passenger-"+id+" ."+type+" .document-type-info-wrapper-greenCard input").addClass('hide');
            $("#form-passenger-"+id+" ."+type+" .document-type-info-wrapper-greenCard select").addClass('hide');
            $("#form-passenger-"+id+" ."+type+" .document-type-info-wrapper-greenCard").hide();
        }
    }else{
        if(val){
            $("."+type+" .document-type-info-wrapper-greenCard input").removeClass('hide');
            $("."+type+" .document-type-info-wrapper-greenCard select").removeClass('hide');
            $("."+type+" .document-type-info-wrapper-greenCard").show();
        }else{
            $("."+type+" .document-type-info-wrapper-greenCard input").addClass('hide');
            $("."+type+" .document-type-info-wrapper-greenCard select").addClass('hide');
            $("."+type+" .document-type-info-wrapper-greenCard").hide();
        }
    }
}

function verifyCheckin(){
    var disabled = false;
    var numCheckedPass = 0;
    var numDocsReq;

    $("input.chkPassenger").each(function(){
        var idx = $(this).attr("idx");

        var isChecked = $("#passenger_checked_" + idx).val();

        if(isChecked){
            numCheckedPass++;
            numDocsReq = $("#numDocumentsRequired-" + idx).val();
            if(numDocsReq>0){
                disabled = true;
            }
        }
    });

    if(numCheckedPass==0){
        disabled = true;
    }

    if(disabled){
        $('#btProcediSubmit').prop('disabled', true);
    }else{
        $('#btProcediSubmit').prop('disabled', false);
    }

}

function checkSendList(element) {
    var el = $(element).parents(".send");
    var telList = el.find(".telList");
    var mailList = el.find(".mailList");
    el.find("input#btSendMailGestione").prop("disabled", !(telList.children().length > 0 || mailList.children().length > 0));
}

function sendMailSingle(item){
    var emailList = [];
    var telList = [];
    var elList = $(item).parents(".send").find(".mailList").children();
    $.each(elList, function(i, el) {
        var mail = $(el).find(".mailAddress").text();
        emailList.push(mail);
    });
    elList = $(item).parents(".send").find(".telList").children();
    $.each(elList, function(i, el) {
        var tel = $(el).find(".telNumber").text();
        telList.push(tel);
    });
    $(".mails").html(emailList.join("; "));
    idx = $(this).attr('idx');
    var name_surname = $('.passenger_'+idx).html();
    $('#form-pdf-' + idx).append('<input type="hidden" name="name_surname" value="' + name_surname + '">');
    enableLoaderSendAddress();
    sendEmailLoading = true;
    performSubmit('getboardinigpassinfo','#form-pdf-' + idx, boardingPassSuccess, boardingPassFail);
    if(telList.length > 0)
    {
        $("#tel-"+idx).val(telList.toString());
        sendSmsLoading = true;
        performSubmit('checkisendmoreboardyngpassbyphone','#formSendSmd-' + idx, sendTelSuccess, sendTelFail);
    }

}

function sendTelSuccess(data)
{
    if(!sendEmailLoading)
    {
        disableLoaderSendAddress();
        $('.reveal--manage-send-address').parent().css('display','none');
        $("#send-address-success").trigger("click");
    }
    sendSmsLoading = false;
}

function sendTelFail()
{
    if(!sendEmailLoading) {
        disableLoaderSendAddress();
        $('.reveal--manage-send-address').parent().css('display','none');
        $("#send-address-success").trigger("click");
    }
    sendSmsLoading = false;
}

function dropdownSuccess(data) {
    $.each($("[name*='frequentFlyer-']"), function(i, el) {
        for(index in data.frequentFlyerTypes) {
            $(el).append($("<option>", {
                text: data.frequentFlyerTypes[index].description,
                value: data.frequentFlyerTypes[index].code
            }));
        }
    });
}

function dropdownFail() {
    // alert("fail");
}

function deleteCheckin(idx){
    enableLoaderCheckin();
    performSubmit('checkinoffloadrest','#delete-checkin-' + idx,deleteSuccess,deleteFail);

}

function deleteSuccess(data)
{
    countOffload();
    if(typeof(data) == 'string'){
        data=JSON.parse(data);
    }

    if(data.outcome != null){
        $('.success-'+idx).attr('style','display: block;');
        $('.message-'+idx).attr('style','display: none;');
        // TODO: tornare la pagina di redirect nella response
        // window.location.href = './check-in-flights-list.html';
        $("#form-cercaPnr-"+idx).find("#flightitinerary").val(flight);
        disableLoaderCheckin();
        setTimeout(function () {
            enableLoaderCheckin();
            if($("#form-cercaPnrFF").find("#frequentFlyer").val()){
                performSubmit('searchbyfrequentflyer', '#form-cercaPnrFF', cercaPnrSuccessFF, cercaPnrFail);
            }else {
                performSubmit('checkinpnrsearch', '#form-cercaPnr-' + idx, cercaPnrSuccess, cercaPnrFail);
            }
        }, 3000);
    }
}
function cercaPnrSuccessFF(data){

        if (!data.isError) {
            window.location.href = data.successPage;
        }else{
            disableLoaderCheckin();
            //alert("error");
        }
        //$('#cercaByFFSubmit').bind('click', cercaByFF);


}

function cercaPnrSuccess(data) {
    checkinStarted++;
    updateDataLayer('checkinStarted', checkinStarted);
    if (!data.isError) {
        // window.location.href = data.successPage;
        var params = retriveParam(data.successPage);
        if
        (params["repeate"] == "true") {
            window.location.href = data.successPage;
        }
        else {
            performSubmit('checkinpnrselected', '#form-cercaPnr-' + idx, selectPnrSuccessEdit, selectPnrFail);
        }
    } else {

        disableLoaderCheckin();
        // alert("Error redirect Flight list");
    }

}

function retriveParam(url) {
    queries = url.split("&");
// Convert the array of strings into an object
    for (i = 0, l = queries.length; i < l; i++) {
        temp = queries[i].split('=');
        params[temp[0]] = temp[1];
    }
    return params;
}

function selectPnrSuccessEdit(data) {
    if (!data.isError) {
        window.location.href = data.successPage+"?&flight="+flight;
    } else {
        //alert($('#form-selectPnr').data("genericerror"));
        disableLoaderCheckin();
    }
}

function selectPnrFail() {
    // alert($('#form-selectPnr').data("genericerror"));
    disableLoaderCheckin();
}

function cercaPnrFail() {
    // alert($('#form-selectPnr').data("genericerror"));
    disableLoaderCheckin();
}


function deleteFail(data)
{
}

function getServiceUrl(service, selector, secure) {
    if (selector == 'login') {
        return service;
    }
    selector = selector || "json";
    var protocol = location.protocol;
    if (jsonProtocol && (selector == "json" || secure)) {
        protocol = 'https:';
    }
    if(service == 'menu-millemiglia-header'){
        return protocol + '//' + location.host + internalUrl1 + "."
            + service + "." + selector;
    } else{
        return protocol + '//' + location.host + internalUrl1 + "/."
            + service + "." + selector;
    }
}

var globalIDX = null;

function updateFF(idx){

    if($("[name='frequentFlyer-" + idx).val().length > 0 && $("[name='frequentFlyerNumber-" + idx).val().length > 0) {
        var form = $('#form-passenger-' + idx);
    //    form.find("input[name='frequentFlyer']").val($("select[name='frequentFlyer-" + idx + "']").val());
    //    form.find("input[name='frequentFlyerNumber']").val($("input[name='frequentFlyerNumber-" + idx + "']").val());
        form.find("input[name='codeFFchanged']").val($("select[name='frequentFlyer-" + idx + "']").val());
        form.find("input[name='numberFFchanged']").val($("input[name='frequentFlyerNumber-" + idx + "']").val());
        globalIDX = idx;
        performSubmit('checkinupdatepassenger','#form-passenger-' + idx,updateSuccess,updateFail);
    } else {
        if($("[name='frequentFlyer-" + idx).val().length <= 0) $("[name='frequentFlyer-" + idx).addClass("is-invalid-input");
        else $("[name='frequentFlyer-" + idx).removeClass("is-invalid-input");
        if($("[name='frequentFlyerNumber-" + idx).val().length <= 0) $("[name='frequentFlyerNumber-" + idx).addClass("is-invalid-input");
        else $("[name='frequentFlyerNumber-" + idx).removeClass("is-invalid-input");
    }
}

function showErrorsAddFF(idx) {
    $("[name='frequentFlyer-" + idx).addClass("is-invalid-input");
    $("[name='frequentFlyerNumber-" + idx).addClass("is-invalid-input");
}

function resetErrorsAddFF(idx) {
    $("[name='frequentFlyer-" + idx).removeClass("is-invalid-input");
    $("[name='frequentFlyerNumber-" + idx).removeClass("is-invalid-input");
}

function updateSuccess(data){

    try {
        if(globalIDX != null && data.passengers[0].result != "KO") {
            resetErrorsAddFF(globalIDX);
            var tier = $("[name='frequentFlyer-" + globalIDX + "']").val();
            tier = $("[name='frequentFlyer-" + globalIDX + "'] option[value='" + tier + "']").text();
            var ffCode = $("[name='frequentFlyerNumber-" + globalIDX + "']").val();
            $(".icon--reset").trigger("click");
            $("#ff-" + globalIDX).text(tier + " " + ffCode);
            globalIDX = null;
        } else showErrorsAddFF(globalIDX);
    } catch (e) {
        showErrorsAddFF(globalIDX);
    }
}

function updateFail(data){

    //console.log("fail updatating passenger");
    showErrorsAddFF(globalIDX);
}

function printMultiPdfSuccess(data)
{
    disableLoaderCheckin();
    if(!data.isError) {
        countBpReprinted();
        var url = removeUrlSelector() + ".createpdf.json?q=checkin-1";
        window.location.assign(url);
    } else {
        $("#lnkErrorGetBoardingpass").trigger("click");
    }
}

function printMultiPdfFail()
{
    disableLoaderCheckin();
    $("#lnkErrorGetBoardingpass").trigger("click");
}

function removeUrlSelector() {
    var url = location.pathname;
    url = url.substr(0, location.pathname.indexOf('.'));
    return location.protocol + '//' + location.host + url;
}

function printPdf(idx){

    enableLoaderCheckin();
    performSubmit('getboardinigpassinfo','#form-pdf-' + idx,printPdfSuccess,printPdfFail);

}

function printPdfFail()
{
    disableLoaderCheckin();
    $("#lnkErrorGetBoardingpass").trigger("click");
}

function printPdfSuccess(data)
{
    disableLoaderCheckin();
    if(!data.isError) {
        countBpReprinted();
        var url = removeUrlSelector() + ".createpdf.json?q=checkin-1";
        window.location.assign(url);
    } else {
        $("#lnkErrorGetBoardingpass").trigger("click");
    }
}

function addMail(idx)
{
    var email = '';

    if (idx != null && idx != '' && idx != undefined){
        email = $("#txtEmail_"+idx).val();
    } else{
        email = $("#txtEmail").val();
    }

    if (email != ''){
        if(isEmail(email)){
            $('#warning-email').addClass('hide');
            if (idx != null && idx != ''){
                $('#txtEmail_'+idx).removeClass('is-invalid-input');
            }else{
                $('#txtEmail').removeClass('is-invalid-input');
            }
            var rnd = Math.random().toString().replace(".","");
            var str = '<button id="' + rnd + '" onclick="javascript:removeMailToken(\'' + rnd + '\');" class="removeMail">'
            str += '<span class="mailAddress">' + email + '</span>';
            str += '<span class="show-for-sr removeMail2">Elimina</span>';
            str += '</button>';
            if (idx != null && idx != ''){
                $(".mailList_"+idx).append(str);
                $("#txtEmail_"+idx).val('');
            }else{
                $(".mailListAll").append(str);
                $("#txtEmail").val('');
            }

            validateEmailOnKeyPress = false;
        } else{
            validateEmailOnKeyPress = true;
            if (idx != null && idx != ''){
                $('#txtEmail_'+idx).addClass('is-invalid-input');
                $('#warning-email').removeClass('hide');
            }else{
                $('#txtEmail').addClass('is-invalid-input');
                $('#warning-email').removeClass('hide');
            }

        }
    }
}


function addTel(idx){

    var tel = '';
    if (idx != null && idx != '' && idx != undefined)
    {
        tel = $("#txtTel-"+idx).val();
    }
    else
    {
        tel = $("#txtTel-all").val();
    }

    if (tel != '' && $.isNumeric(tel) && Math.floor(tel) == tel)
    {
        var str = '<span onclick="javascript:removeTel(this);" class="removeTel">';
        str += '<span class="telNumber">' + tel + '</span>';
        str += '<span class="show-for-sr removeTel">Elimina</span>';
        str += '</span>';
        if (idx != null && idx != '' && idx != undefined)
        {
            $("#txtTel-"+idx).val("");
            $(".telList-"+idx).append(str);
            $("#txtTel-"+idx).removeClass('is-invalid-input');
            $("#warning-tel-"+idx).addClass('hide');
        }
        else
        {
            $("#txtTel-all").val("");
            $(".telList-all").append(str);
            $('#txtTel-all').removeClass('is-invalid-input');
            $('#warning-tel-all').addClass('hide');
        }
    }
    else
    {
        if (idx != null && idx != '' && idx != undefined)
        {
            $("#txtTel-"+idx).addClass('is-invalid-input');
            $("#warning-tel-"+idx).removeClass('hide');
        }
        else
        {
            $('#txtTel-all').addClass('is-invalid-input');
            $('#warning-tel-all').removeClass('hide')
        }
    }
}

function enableLoaderSendAddress() {
    $(".loader-send-address").show();
}

function disableLoaderSendAddress() {
    $(".loader-send-address").hide();
}

function removeMailToken(el)
{
    var parent = $("#" + el).parent();
    $("#" + el).remove();
    checkSendList(parent);
}

function removeTel(el)
{
    var parent = $(el).parent();
    el.remove();
    checkSendList(parent);
}

function boardingPassSuccess(data)
{
    if(!data.isError) {
        var mails = [];
        $(".mailList_" + idx).find("span.mailAddress").each(function () {
            mails.push($(this).html());
        });
        $("#mailAddress-" + idx).val(mails.toString());
        performSubmit('checkisendboardyngpassbymail', '#form-pdf-' + idx, sendEmailSuccess, sendEmailFail);
    } else {
        disableLoaderCheckin();
        $("#lnkErrorGetBoardingpass").trigger("click");
    }
}

function boardingPassFail()
{
    disableLoaderCheckin();
    $("#lnkErrorGetBoardingpass").trigger("click");
}

function boardingPassMultipleSuccess(data)
{
    if(!data.isError) {
        var mails = [];
        $("#btSendMailGestione").closest(".row").find(".mailList").find("span.mailAddress").each(function(){
            mails.push($(this).html());
        });
        $("#mailAddress-" + idx).val(mails.toString());
        performSubmit('checkisendboardyngpassbymail','#frmMultiBoards', sendEmailSuccess, sendEmailFail);
    } else {
        disableLoaderCheckin();
        $("#lnkErrorGetBoardingpass").trigger("click");
    }
}

function boardingPassMultipleFail()
{
    disableLoaderSendAddress();
    $("#lnkErrorGetBoardingpass").trigger("click");
}

function sendEmailSuccess(data)
{
    if(!data.isError) {
        if (!sendSmsLoading) {
            disableLoaderSendAddress();
            $('.reveal--manage-send-address').parent().css('display', 'none');
            $("#send-address-success").trigger("click");
        }
        sendEmailLoading = false;
    } else {
        disableLoaderCheckin();
        $("#lnkErrorGetBoardingpass").trigger("click");
    }
}

function sendEmailFail(data)
{
    if(!data.isError) {
        if (!sendSmsLoading) {
            disableLoaderSendAddress();
            if (data == null || !data.isError) {
                $("#send-address-success").trigger("click");
                $('.reveal--manage-send-address').parent().css('display', 'none');
            }
        }
        sendEmailLoading = false;
    } else {
        disableLoaderCheckin();
        $("#lnkErrorGetBoardingpass").trigger("click");
    }
}


/* GESTIONE PASSEGGERI */

function savePassengers(){

    var cotiPaxNum = 0;
    var adultChecked = false;
    var childChecked = false;

    $("input.chkPassenger").each(function(){
        var idx = $(this).attr("idx");

        var isChecked = $("#passenger_checked_" + idx).val();
        var type = $("#passenger_type_" + idx).val();

        if(isChecked){
            if(type=="ADT"){
                adultChecked = true;
            }
            if(type=="CHD"){
                childChecked = true;
            }
            var cotiPax = $("#passenger_coti_pax_" + idx).val();
            if(cotiPax == "1"){
                cotiPaxNum++;
            }
        }
    });

    if(childChecked == true && adultChecked == false){
        $("#errorMessage").html(CQ.I18n.get('checkin.passengerList.onlyChild.label'));
        $("#lnkError").trigger("click");
    }else{
        if(cotiPaxNum>0){
            if(cotiPaxNum==1){
                $("#notifyMessage").html(CQ.I18n.get('checkin.passengerList.cotiPax.label'));
                $("#lnkNotify").trigger("click");
            }else{
                $("#notifyMessage").html(CQ.I18n.get('checkin.passengerList.cotiPaxMulti.label'));
                $("#lnkNotify").trigger("click");
            }

        }else{
            enableLoaderCheckin();
            performSubmit('checkinselectedpassenger','#form-passengers',savePassengersSuccess,savePassengersFail);
        }
    }

}


function savePassengersSuccess(data)
{

    if(data){
        if (!data.isError)
        {
            window.location.href = data.successPage;
        }else{
            disableLoaderCheckin();
            $("#lnkError").trigger("click");
            $("#errorMessage").html(data.errorMessage);
            dataLayer[1].checkinError = data.errorMessage;
            if(typeof window["_satellite"] !== "undefined"){
            	_satellite.track("CIError");
            } else {
            	console.error("Cannot activate CIError. _satellite object not found");
            }
            dataLayer[1].checkinError = "";
        }
    }
    else
    {
        disableLoaderCheckin()
        $("#lnkError").trigger("click");
        $("#errorMessage").html(data.errorMessage);
    }
    $('#btProcediSubmit').bind('click', savePassengers);
}

function savePassengersFail(data)
{
    disableLoaderCheckin()
    //$('#btProcediSubmit').bind('click', savePassengers);
}

function addPassenger(e){
    e.preventDefault();
    $('#btAddPassenger').unbind('click', addPassenger);
    performSubmit('checkinaddpassenger','#add-passenger',addPassengerSuccess,addPassengerFail);
}


function addPassengerSuccess(data)
{

    if (!data.isError)
    {
        window.location.href = data.successPage;
    }
    else
    {
        $("#lnkError").trigger("click");
        $("#errorMessage").html(data.errorMessage);
    }
    $('#btAddPassenger').bind('click', addPassenger);
}

function addPassengerFail() {
    $('#divError').show();
    $('#labelCheckinError').text('Errore (servlet FAIL) ');
    $('#btAddPassenger').bind('click', addPassenger);
}

// check dati singolo passeggero
function confirmPassenger()
{
    var idx = $(this).attr('idx');

    isExtra = false;
    if ($(this).data("extra") == "1")
        isExtra = true;

    var ok = true;

    $("#passengerData" + idx).removeClass("passenger-wrap-completed");
    $("#passengerLinkEdit" + idx).hide();
    $(".passengerDataError" + idx).hide();

    $('.btConfirm').unbind('click', confirmPassenger);

    $(".check-required-" + idx).each(function(){
        if($(this).closest('.hide').length>0 || $(this).hasClass('hide')){
            return true;
        }
        if($(this).is(':checkbox')){
            $(this).parent().find('label').removeClass("is-invalid-checkbox");
            if ($(this).prop('checked') == false){
                //console.log($(this).attr("name"));
                $(this).parent().find('label').addClass('is-invalid-checkbox');
                ok = false;
            }
        }else{
            $(this).removeClass("is-invalid-input");
            if ($(this).val() == ''){
                //console.log($(this).attr("name"));
                $(this).addClass("is-invalid-input");
                ok = false;
            }
        }

        $(this).change(function(){
            if($(this).closest('.hide').length>0 || $(this).hasClass('hide')){
                return false;
            }
            if($(this).is(':checkbox')){
                $(this).parent().find('label').removeClass("is-invalid-checkbox");
                if ($(this).prop('checked') == false){
                    //console.log($(this).attr("name"));
                    $(this).parent().find('label').addClass('is-invalid-checkbox');
                    ok = false;
                }
            }else{
                $(this).removeClass("is-invalid-input");
                if ($(this).val() == ''){
                    //console.log($(this).attr("name"));
                    $(this).addClass("is-invalid-input");
                    ok = false;
                }
            }

        });

    });


    // Validazione del numero di passaporto (Alfanumerico e caratteri compresi tra 6 e 12)
    var reg = /^[0-9a-zA-Z]+$/;
    var form = $("#passengerData" + idx);
    var passport = form.find("[name^='number_']");
    var type = form.find("[name^='type_']");
    if(passport.length > 0){
    if((!reg.test(passport.val()) || passport.val().length < 6 || passport.val().length > 12) && (type.val() == "PASSPORT" || type.val() == "NATIONALID")) {
        $(passport).addClass("is-invalid-input");
        ok = false;
    }
    }


    $("#passengerLinkEdit" + idx).show();

    if (ok)
    {
         // $("[idx="+idx+"]").addClass("checked"); //commentato per revert
         // $("#cb_" + idx).attr("checked", true);
         // $("#passenger_checked_" + idx).val("true");
        // Sistemo le date prima di passarle

        if ($("#dateOfBirth-" + idx).length )
            $("#dateOfBirth-" + idx).val($("#year-born-" + idx).val() + "-" + $("#month-born-" + idx).val() + "-" + $("#day-born-" + idx).val() + "T00:00:00.000Z");

        if ($("#dateOfBirthInfant-" + idx).length )
            $("#dateOfBirthInfant-" + idx).val($("#year-born-inf-" + idx).val() + "-" + $("#month-born-inf-" + idx).val() + "-" + $("#day-born-inf-" + idx).val() + "T00:00:00.000Z");

        if ($("#expirationDate-" + idx).length)
            $("#expirationDate-" + idx).val($("#year-scad-" + idx).val() + "-" + $("#month-scad-" + idx).val() + "-" + $("#day-scad-" + idx).val() + "T00:00:00.000Z");

        if ($("#expirationDateInfant-" + idx).length)
            $("#expirationDateInfant-" + idx).val($("#year-scad-inf-" + idx).val() + "-" + $("#month-scad-inf-" + idx).val() + "-" + $("#day-scad-inf-" + idx).val() + "T00:00:00.000Z");

        if ($("#issueDateAdult-" + idx).length)
            $("#issueDateAdult-" + idx).val($("#year-scad-extra-" + idx).val() + "-" + $("#month-scad-extra-" + idx).val() + "-" + $("#day-scad-extra-" + idx).val() + "T00:00:00.000Z");

        if ($("#issueDateInfant-" + idx).length)
            $("#issueDateInfant-" + idx).val($("#year-scad-extra-inf-" + idx).val() + "-" + $("#month-scad-extra-inf-" + idx).val() + "-" + $("#day-scad-extra-inf-" + idx).val() + "T00:00:00.000Z");

        if ($("#expireDateGreenCardAdult-" + idx).length)
            $("#expireDateGreenCardAdult-" + idx).val($("#year-scad-greencard-" + idx).val() + "-" + $("#month-scad-greencard-" + idx).val() + "-" + $("#day-scad-greencard-" + idx).val() + "T00:00:00.000Z");

        if ($("#expireDateGreenCardInfant-" + idx).length)
            $("#expireDateGreenCardInfant-" + idx).val($("#year-scad-greencard-inf-" + idx).val() + "-" + $("#month-scad-greencard-inf-" + idx).val() + "-" + $("#day-scad-greencard-inf-" + idx).val() + "T00:00:00.000Z");


        idxX = idx;

        // Chiamata alla servlet per salvare i dati
        if (isExtra){
            enableLoaderCheckin();
            performSubmit('checkinupdatepassengerextra','#form-passenger-' + idx,saveConfirmSuccess,saveConfirmFail);
        }else{
            enableLoaderCheckin();
            performSubmit('checkinupdatepassenger','#form-passenger-' + idx,saveConfirmSuccess,saveConfirmFail);
        }


        //$(this).parents("div.passenger-wrap-content").slideUp();
    }else{
        disableLoaderCheckin();
        $(".passengerDataError" + idx).show();
        $(".passengerDataSuccess" + idx).addClass('hide');
        $("#numDocumentsRequired-"+idx).val(1);
    }
    verifyCheckin();
    $('.btConfirm').bind('click', confirmPassenger);

    $(".check-required-" + idx).change(function(){
        if ($(this).val() == '')
            $(this).removeClass("is-invalid-input");
    });

    // Scroll della pagina per centrare la lista passeggeri

    //var offset = $("#lista_pass").offset();
    //$('html, body').animate({scrollTop:(offset.top)}, 'slow');


}

function saveConfirmSuccess(data)
{

    try
    {
        disableLoaderCheckin();
        // console.log(idxX);
        if (data.passengers && data.passengers[0].result == 'OK')
        {
            $("#numDocumentsRequired-"+idxX).val(0);
            $(".passengerDataSuccess" + idxX).removeClass('hide');
            $("#passengerData" + idxX).addClass("passenger-wrap-completed");
            if (!$("#cb_" + idxX).hasClass("checked")){
                $("#cb_" + idxX).trigger("click");
            }

            $('#form-passenger-'+idxX).find('.passenger-wrap-content').slideUp();
            var offset = $("#lista_pass").offset();
            $('html, body').animate({scrollTop:(offset.top)}, 'slow');
        }else{
             $(".passengerDataSuccess" + idx).addClass('hide');
        }

    }
    catch(err)
    {
        disableLoaderCheckin();
        //document.getElementById("demo").innerHTML = err.message;
    }
    verifyCheckin();
    // console.log(data);
}

function saveConfirmFail() {
    disableLoaderCheckin();
    verifyCheckin();
}


function getNumberOfDays(year, month) {
    var isLeap = ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0));
    return [31, (isLeap ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
}

function showHideWrapContent()
{

    $("#cb_" + $(this).attr("idx")).trigger("click");

}

function clickCheckBox(){
    if ( /^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {


    }else{

    }

}


function invokeGenericFormService(service, method, form, done, fail, always,
                                  selector, additionalParams) {
    var actualData;
    if ($.type(form) === "string" || !form) {
        var serializedData = $(form).serialize();
        if (serializedData != "") {
            serializedData += "&";
        }
        serializedData += "_isAjax=true";
        if (additionalParams) {
            for ( var paramName in additionalParams) {
                serializedData += "&" + paramName + "="
                    + encodeURIComponent(additionalParams[paramName]);
            }
        }
        actualData = serializedData;
    } else if ($.isPlainObject(form)) {
        actualData = {};
        if (form) {
            for (var fieldName in form) {
                actualData[fieldName] = form[fieldName];
            }
        }
        actualData['_isAjax'] = true;
        if (additionalParams) {
            for (var paramName in additionalParams) {
                actualData[paramName] = additionalParams[paramName];
            }
        }
    }
    return $.ajax({
        url : getServiceUrl(service, selector),
        method : method,
        data : actualData,
        context : document.body
    }).done(function(data) {
        if (done) {
            done(data);
        }
    }).fail(function() {
        if (fail) {
            fail();
        }
    }).always(function() {
        if (always) {
            always();
        }
    });
}

function performValidation(service, form, done, fail, always, selector) {
    var additionalParams = {
        '_action' : 'validate'
    };
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector, additionalParams);
}

function performSubmit(service, form, done, fail, always, selector) {
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector);
}

function validation(e, service, form, done, fail, always, selector) {
    e.preventDefault();
    return performValidation(service, form, done, fail, always, selector);
}

function validationFormFile(e, service, form, done, fail, always, selector,
                            additionalParams) {
    e.preventDefault();
    additionalParams._action = "validate";
    return invokeGenericFormService(service, 'POST', form, done, fail, always,
        selector, additionalParams);
}

function getDropdownData(requestData, done, fail, always) {
    return invokeGenericFormService('staticdatalistservlet', 'GET', '', done,
        fail, always, 'json', requestData);
}

function getServiceUrl(service, selector, secure) {
    if (selector == 'login') {
        return service;
    }
    selector = selector || "json";
    var protocol = location.protocol;
    if (jsonProtocol && (selector == "json" || secure)) {
        protocol = 'https:';
    }
    if(service == 'menu-millemiglia-header'){
        return protocol + '//' + location.host + internalUrl1 + "."
            + service + "." + selector;
    } else{
        return protocol + '//' + location.host + internalUrl1 + "/."
            + service + "." + selector;
    }
}

function removeUrlSelector() {
    var url = location.pathname;
    url = url.substr(0, location.pathname.indexOf('.'));
    return location.protocol + '//' + location.host + url;
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex
        .exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g,
        " "));
}

function showErrors(data, noanchor, checkboxnew) {
    removeErrors();
    for (var key in data.fields) {
        var selector = '[name="' + key + '"]';
        if ($(selector).css('display') == 'none' && $(selector).closest(".j-showCaptcha").lenght == 0) {
            selector = $(selector).next();
        }
        $(selector).attr("aria-invalid", "true");
        $(selector).attr("aria-describedby", key + '_error' );
        var $contElement = $(selector).parent();
        if ($(selector).prop("tagName") == 'SELECT' ||
            ($(selector).attr("type") == 'checkbox' && checkboxnew)) {
            $contElement = $(selector).parent().parent();
        }
        var linkClass = "form__errorIcon";
        var msgClass = "form__alert form__errorField";
        if ($(selector).attr("type") === "checkbox") {
            linkClass = linkClass + " left";
            msgClass = msgClass + " left";
        }
        $contElement.addClass("isError");
        $($contElement).append(
            '<a class="' + linkClass +
            ($(selector).attr("type") == 'checkbox' && checkboxnew ? '" style="display:none' : '') +
            '"></a><div id="' + key + '_error'
            + '" class="' + msgClass + '">'
            + data.fields[key] + '</div>');
    }

    // $('[aria-invalid="true"]:first').focus();
    removePopups();

    if (!noanchor) {
        if ($('.isError').length > 0) {
            $(".isError").first().animateAnchor(-32, 400);
        }
    }
    return false;
}
