$(document).ready(function() {
	initPage(CQ_Analytics.ProfileDataMgr.data);
	initECSelect();
	getEstrattoContoByDate();
});

function initPage(data) {
    var mmLevelInfo = getMMLevel(data.milleMigliaTierCode);
	//var cardImg = "/etc/designs/alitalia/clientlibs/images/cardMM_tier_" + mmLevelInfo.lvl + ".png";
    var rawDate = data.milleMigliaExpirationDate.split("T")[0].split("-");
    var expDate = rawDate[2] + " " + getMonthByNum(parseInt(rawDate[1])) + " " + rawDate[0];
    var remainMiles = parseInt(data.milleMigliaRemainingMiles).toLocaleString();
    var qualFlights = parseInt(e.milleMigliaQualifiedFlight).toLocaleString() == "NaN"? 0 : parseInt(e.milleMigliaQualifiedFlight).toLocaleString();
    var struRemain = "";
	var nextStep = getNextProfileStep(mmLevelInfo.lvl);
    //$(".badge-wrap").html("<img src=" + cardImg + '><p class="action-wrap action-wrap--right"><a class="action__icon action__icon--medium action__icon--stampa" href="#"><span>Stampa tessera</span></a></p>');
    //$("#mmStatus").html(mmLevelInfo.value);
    if(mmLevelInfo == 4)
        $(".progressbar").find("div.step").addClass("active");
    else {
    	$(".progressbar").find("div.step").removeClass("active");
    	for(var i=0; i<mmLevelInfo.lvl; i++)
        	$(".progressbar").find("div.step:eq(" + i + ")").addClass("active");
    }
    $("#QualifiedFlights").text(qualFlights);
    //$("#expDate").html(expDate);
    //$("#qualFlights").html(qualFlights);
//    if(nextStep != null) {
//		struRemain += "<strong> TI MANCANO " + remainMiles + " MIGLIA</strong>";
//    	struRemain += " PER ENTRARE NEL CLUB " + nextStep.toUpperCase();
//    	$("#remainingMiles").html(struRemain);
//    } else{
//        $("#remainingMiles").hide();
//    }


}

function getMonthByNum(num) {
    var i18nroot = "common.monthsOfYear.";
    var months = ["months", CQ.I18n.get(i18nroot + "january"), CQ.I18n.get(i18nroot + "february"), CQ.I18n.get(i18nroot + "march"), CQ.I18n.get(i18nroot + "april"), CQ.I18n.get(i18nroot + "may"), CQ.I18n.get(i18nroot + "june"), CQ.I18n.get(i18nroot + "july"), CQ.I18n.get(i18nroot + "august"), CQ.I18n.get(i18nroot + "september"), CQ.I18n.get(i18nroot + "octember"), CQ.I18n.get(i18nroot + "november"), CQ.I18n.get(i18nroot + "december")];
	return months[num];
}

function getNextProfileStep(level) {
    switch(level) {
        case 1:
            return "Ulisse";
        case 2:
            return "Freccia Alata";
        case 3:
            return "Plus";
        case 4:
            return null;
        default:
            return "Ulisse";
    }
}


