$(document).ready(function(){
	$("#login-carnet").on("click", startCarnetLogin);
});

function startCarnetLogin(e) {
	removeErrors();
	$("#login-carnet").off("click", startCarnetLogin);
	$("#login-carnet").addClass('isDisabled');
	validation(e, "carnetloginconsumer", "#carnet-login-form", function(data) { 
		successLoginCarnetValidation(data);
	}, failLoginCarnet);
}

function successLoginCarnetValidation(data) {
	if (data.result) {
		removeErrors();
		performSubmit('carnetloginconsumer', '#carnet-login-form', function(data) {
			successLoginCarnetSubmit(data);
		}, failLoginCarnet);
	} else {
		$("#login-carnet").on("click", startCarnetLogin);
		$("#login-carnet").removeClass('isDisabled');
		showErrors(data,true);
		$(".carnet__loginCover").anchor();
	}
}

function successLoginCarnetSubmit(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
	} else if (data.isError) {
		$("#login-carnet").on("click", startCarnetLogin);
		$("#login-carnet").removeClass('isDisabled');
		showErrors(data.isError,true);
		$(".carnet__loginCover").anchor();
	}
}

function failLoginCarnet(){
	$("#login-carnet").on("click", startCarnetLogin);
	$("#login-carnet").removeClass('isDisabled');
}