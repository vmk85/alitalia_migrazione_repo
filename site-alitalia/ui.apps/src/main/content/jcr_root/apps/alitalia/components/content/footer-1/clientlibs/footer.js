var footerOptions = {};

// document ready
$( document ).ready( function() {
    footerOptions.initAccordion();

    setTimeout(function(){
        if( Foundation.MediaQuery.atLeast( 'large' ) ) {
            footerOptions.setAccordionDesktop();
        } else {
            footerOptions.setAccordionMobile();
        }
    }, 100);

    if (getCookie("alitaliaCookieConfirmed") == ""){
        $('.thisCookie').css('display', 'block');
    } else {
        $('.thisCookie').css('display', 'none');
    }
} );

// change media query
$(window).on('changed.zf.mediaquery', function(event, name) {

    // If breakpoint is large and up, reinit the tabs
    if( Foundation.MediaQuery.atLeast( 'large' ) ) {
        // accordion
        footerOptions.setAccordionDesktop();
    } else {
        // accordion
        footerOptions.setAccordionMobile();
        // remove accordion equalizer
        $( '.footer__accordion .footer__item' ).removeAttr( 'style' );
    }

});


footerOptions.initAccordion = function() {
    $( '.footer__accordion .accordion' ).foundation();
    footerOptions.accordion = new Foundation.Accordion( $( '.footer__accordion .accordion' ), {
        // slideSpeed: 0,
        multiExpand: false,
        allowAllClosed: true,
    } );
};

footerOptions.setAccordionDesktop = function() {
    if($( '.footer__accordion .accordion-content' ).length>0)
        $( '.footer__accordion .accordion' ).foundation( 'down', $( '.footer__accordion .accordion-content' ) );
    $( '.footer__accordion .accordion' ).attr( 'disabled', true );
};

footerOptions.setAccordionMobile = function() {
    $( '.footer__accordion .accordion' ).removeAttr( 'disabled' );
    if($( '.footer__accordion .accordion-content' ).length>0)
        $( '.footer__accordion .accordion' ).foundation( 'up', $( '.footer__accordion .accordion-content' ) );
    // $( '.lista-offerte__accordion .accordion' ).foundation( 'down', $( '.lista-offerte__accordion .accordion-item:first-child .accordion-content' ) );
};

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";domain=.alitalia.com;path=/";
    $(".cookie-bar").animate({height: "0", 'padding' : 0}, 500);
}

function getCookie(cname) {
    var nameEQ = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return "";
}
