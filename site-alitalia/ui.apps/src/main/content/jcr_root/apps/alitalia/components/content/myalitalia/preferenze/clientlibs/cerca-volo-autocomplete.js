var cercaVoloAutocomplete = {};
var suggestionResults;

$( document ).ready( function() {

	cercaVoloAutocomplete.staticServiceUrl = getServiceUrl('airportslistconsumerrest');

	// get all airport json and init autocomplete
	$.ajax({
		url: cercaVoloAutocomplete.staticServiceUrl,
		dataType: 'json',
		async: true,
		context: document.body,
		success: function( data ) {
			suggestionResults = data;
		},
		complete: function() {
			cercaVoloAutocomplete.initAutocomplete( suggestionResults, '#preferenza-aeroporto', '#suggestion_airport' );
		}
	});

  // toggle visibility of clear autocomplete button
  $( '.preference-airport-form input' ).on( 'keyup focus', function() {
    if( $( this ).val() !== '' ) {
      $( this ).parent().find( '.clear-input' ).removeClass( 'hide' );
    } else {
      $( this ).parent().find( '.clear-input' ).addClass( 'hide' );
	}
  });
  // toggle visibility of clear autocomplete button
  $( '.preference-airport-form input' ).on( 'focus', function() {
    if( $( this ).val() !== '' ) {
		$( this ).select();
    }
  });

  // clear autocomplete input
  $( '.preference-airport-form .clear-input' ).on( 'click', function() {
	$( this ).parent().find( 'input' ).val('').focus();
  });

});

cercaVoloAutocomplete.initAutocomplete = function( suggestions, inputID, suggestionWrap ) {
	$( inputID ).autocomplete({
		minChars: 3,
		preserveInput : false,
		appendTo : $( suggestionWrap ),
		autoSelectFirst: true,
		lookup: function( query, done ) {
			// format json for autocomplete plugin & filter based on current query
			var resultsFiltered = {
				suggestions : suggestions.airports.filter( function( obj ) {
					return (
                        removeAccents(obj.code.toLowerCase()) == removeAccents(query.toLowerCase()) ||
                        removeAccents(obj.city.toLowerCase().substr(0, query.length)) == removeAccents(query.toLowerCase()) ||
                        removeAccents(obj.airport.toLowerCase().substr(0, query.length)) == removeAccents(query.toLowerCase())
					);
				} ).map( function(curr, inde, arr) {
					curr.value = curr.city + ' ' + curr.code;
					return curr;
				} )
			};

			// callback to pass results to the lookup
			done(resultsFiltered);

		},
		formatResult: function(suggestion, currentValue) {
			// format the returned suggestion string
			return suggestion.city + ' (' + suggestion.country + ') ' + suggestion.airport + ' <strong>' + suggestion.code + '</strong>';
		},
		beforeRender: function() {
			$( '.suggestion_new-box' ).addClass( 'hide' );

			// remove built-in inline style from the suggestion-wrap
			$( '.suggestion_new-box .autocomplete-suggestions' ).removeAttr( 'style' );
			// show current suggestion-wrap
			$( suggestionWrap ).removeClass( 'hide' );
		},
		onSelect: function (suggestion) {

			// hide suggestion-wrap
			$( '.suggestion_new-box' ).addClass( 'hide' );

			// function to pass selected data
			var thisName = $( inputID ).attr( 'name' );
			cercaVoloAutocomplete.setAriportInputValue(thisName, suggestion.city, suggestion.code);
		}
	});

};

cercaVoloAutocomplete.setAriportInputValue = function(inputName, currCity, currCode) {

	$('input[name="'+inputName+'"]').val(currCity + ' ' + currCode).trigger('change');

	cercaVoloAutocomplete.lastIataCode = currCode;
	cercaVoloAutocomplete.lastAirport = currCity;

	// set IATA code in hiddenForm
	$( '#preferenceAirports' ).val( currCode );

	// split value to make IATA code bold
	$('input[name="'+inputName+'"]').each( function(i, el) {
		$( el ).parent().find( '.input-placeholder' ).html( currCity + '<strong class="iata-code">' + currCode + '</strong>' );
	} );

};

function removeAccents(strAccents){
    strAccents = strAccents.split('');
    strAccentsOut = new Array();
    strAccentsLen = strAccents.length;
    var accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var accentsOut = ['A','A','A','A','A','A','a','a','a','a','a','a','O','O','O','O','O','O','O','o','o','o','o','o','o','E','E','E','E','e','e','e','e','e','C','c','D','I','I','I','I','i','i','i','i','U','U','U','U','u','u','u','u','N','n','S','s','Y','y','y','Z','z'];
    for (var y = 0; y < strAccentsLen; y++) {
        if (accents.indexOf(strAccents[y]) != -1) {
            strAccentsOut[y] = accentsOut[accents.indexOf(strAccents[y])];
        }
        else
            strAccentsOut[y] = strAccents[y];
    }
    strAccentsOut = strAccentsOut.join('');
    return strAccentsOut;
}