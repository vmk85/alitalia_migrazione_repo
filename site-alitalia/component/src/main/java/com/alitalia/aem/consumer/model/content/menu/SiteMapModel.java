package com.alitalia.aem.consumer.model.content.menu;


import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;

@Model(adaptables={SlingHttpServletRequest.class})
public class SiteMapModel extends GenericMenuModel{

	private Logger logger = LoggerFactory.getLogger(SiteMapModel.class);

    @Self
    private SlingHttpServletRequest request;
    
    @Inject
	private Page currentPage;
    
    @Inject
    @Default(values = AlitaliaConstants.DEFAULT_MENU_DEPTH)
    private String menuDepthString;

    // This object defines the whole menu
    private MenuItem[] menu;

    private String homepath;
    private Resource rootResource;
    private int menuDepth;

    @PostConstruct
    protected void initModel() {
    	
    	final Resource resource = request.getResource();
    	InheritanceValueMap vm = new HierarchyNodeInheritanceValueMap(
    			currentPage.getContentResource());
    	
        if(vm.getInherited("homepath", String.class)!=null || 
        		("").equals(vm.getInherited("homepath", String.class))){
        	
        	homepath = vm.getInherited("homepath", String.class);
        	
        } else {
        	homepath = AlitaliaUtils.findSiteBaseRepositoryPath(resource);
        }
        rootResource = resource.getResourceResolver().getResource(homepath);
        
        menuDepth = Integer.parseInt(menuDepthString);
    	menu = getPageList(rootResource.adaptTo(Page.class), menuDepth, request,
    			logger);
    }
    
	/**
     * Returns all the elements of the Alitalia menu
     * @return The menu
     */
    public MenuItem[] getMenu() {
    	return menu;
    }

}

