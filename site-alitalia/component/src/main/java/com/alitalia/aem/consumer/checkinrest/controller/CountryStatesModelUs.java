package com.alitalia.aem.consumer.checkinrest.controller;

import com.alitalia.aem.common.data.home.countrystates.CountryStates;
import com.alitalia.aem.common.messages.home.CountryStatesRequest;
import com.alitalia.aem.common.messages.home.CountryStatesResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.staticdata.delegaterest.StaticDataDelegateRest;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CountryStatesModelUs {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private volatile StaticDataDelegateRest staticDataDelegateRest;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private CountryStates countryStates;
	
	@PostConstruct
	protected void initModel() throws Exception {
		countryStates = new CountryStates();
		CountryStatesRequest countryStatesRequest = new CountryStatesRequest(IDFactory.getTid(), IDFactory.getSid(request));
		countryStatesRequest.setCountry("US");
		countryStatesRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
		countryStatesRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
		countryStatesRequest.setConversationID(IDFactory.getTid());
		
		CountryStatesResponse countryStatesResponse = staticDataDelegateRest.getCountryStates(countryStatesRequest);
		
		this.setCountryStates(countryStatesResponse.getCountryStates());
	}

	public CountryStates getCountryStates() {
		return countryStates;
	}

	public void setCountryStates(CountryStates countryStates) {
		this.countryStates = countryStates;
	}
}
