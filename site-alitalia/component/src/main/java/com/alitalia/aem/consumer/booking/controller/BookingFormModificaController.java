package com.alitalia.aem.consumer.booking.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.annotation.PostConstruct;

import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingFormModificaController extends BookingSessionGenericController{

	@Self
	private SlingHttpServletRequest request;

	private String departureAirport;
	private String arrivalAirport;
	private String departureCity;
	private String arrivalCity;
	private String outboundDate;
	/* repDate represents departure date in YYYYMMDD format*/
	private String retDate;
	/* repDateOld represents departure date in YYYY-MM-DD format*/
	private String retDateOld;
	private String returnDate;
	private String cug;
	private boolean isRoundtrip;
	private int numYoungs;
	private int numInfants;
	private int numChildren;
	private int numAdults;
	private String filterCabin;
	/* depDate represents departure date in YYYYMMDD format*/
	private String depDate;
	/* depDateOld represents departure date in YYYY-MM-DD format*/
	private String depDateOld;
	private String returnDateOld;
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@PostConstruct
	protected void initModel() throws Exception {

		try {
			initBaseModel(request);

			if (ctx != null) {

				//obtain cug
				cug = ctx.cug.value();

				//obtain dates
				String format = AlitaliaUtils.getDateFormatByMarket(request.getResource());
				if (format == null) {
					format = "dd/MM/yyyy";
				}
				SimpleDateFormat sdf = new SimpleDateFormat(format);
				outboundDate = sdf.format(ctx.searchElements.get(0)
						.getDepartureDate().getTime());
				Date date=sdf.parse(outboundDate);

				SimpleDateFormat oldFormatDepDate = new SimpleDateFormat("yyyy-MM-dd");
				depDateOld=oldFormatDepDate.format(date);

				SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
				depDate=newFormat.format(date).replace("-", "");

				if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP) {
					returnDate =  sdf.format(ctx.searchElements.get(1).getDepartureDate().getTime());
					Date returnDate=sdf.parse(outboundDate);
					Date dateRe=sdf.parse(this.returnDate);
					SimpleDateFormat oldFormatRetDate = new SimpleDateFormat("yyyy-MM-dd");
					retDateOld=oldFormatRetDate.format(dateRe);
					SimpleDateFormat newFormatRetDate = new SimpleDateFormat("yyyy-MM-dd");
					retDate=newFormatRetDate.format(returnDate).replace("-", "");
				} else {
					returnDateOld =  sdf.format(ctx.searchElements.get(0).getDepartureDate().getTime());
					Date dateRe=sdf.parse(returnDateOld);
					Calendar c = Calendar.getInstance();
					c.setTime(dateRe);
					c.add(Calendar.DATE, 1); //same with c.add(Calendar.DAY_OF_MONTH, 1);
					Date d = c.getTime();
					SimpleDateFormat oldFormatRetDate = new SimpleDateFormat("yyyy-MM-dd");
					returnDateOld=oldFormatRetDate.format(d);
				}

				//obtain airports
				departureAirport = ctx.searchElements.get(0).getFrom().getAirportCode();
				departureCity = i18n.get("airportsData." + departureAirport + ".city");
				arrivalAirport = ctx.searchElements.get(0).getTo().getAirportCode();
				arrivalCity = i18n.get("airportsData." + arrivalAirport + ".city");

				//obtain searchKind
				isRoundtrip = ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP || ctx.showRoundTrip;

				//obtain passengers
				numAdults = ctx.searchPassengersNumber.getNumAdults();
				numChildren = ctx.searchPassengersNumber.getNumChildren();
				numInfants = ctx.searchPassengersNumber.getNumInfants();
				numYoungs = ctx.searchPassengersNumber.getNumYoung();

				if(ctx.filterCabin != null){
					filterCabin = ctx.filterCabin.value();
				}
			} else {
				//in case of error the form will be compiled with empty field
				setDefaultConfiguration();
			}

		}catch(Exception e){
			logger.error("Unexpected error", e);

			//in case of error the form will be compiled with empty field
			setDefaultConfiguration();
		}
	}

	/**
	 * It sets the default configuration for the form modifica
	 * pre-compiling empty field
	 */
	private void setDefaultConfiguration() {
		this.departureAirport = "";
		this.arrivalAirport = "";
		this.departureCity = "";
		this.arrivalCity = "";
		this.outboundDate = "";
		this.returnDate = "";
		this.cug = "ADT";
		this.isRoundtrip = true;
		this.numYoungs = 0;
		this.numInfants = 0;
		this.numChildren = 0;
		this.numAdults = 0;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public String getDepartureCity() {
		return departureCity;
	}

	public String getArrivalCity() {
		return arrivalCity;
	}

	public String getOutboundDate() {
		return outboundDate;
	}
	public String getRetDate() {
		return retDate;
	}
	public String getReturnDate() {
		return returnDate;
	}

	public String getCug() {
		return cug;
	}

	public boolean isRoundtrip() {
		return isRoundtrip;
	}

	public String getNumYoungs() {
		return Integer.toString(numYoungs);
	}

	public String getNumInfants() {
		return Integer.toString(numInfants);
	}

	public String getNumChildren() {
		return Integer.toString(numChildren);
	}

	public String getNumAdults() {
		return Integer.toString(numAdults);
	}

	public String getFilterCabin(){
		return this.filterCabin;
	}
	public String getDepDate(){
		return depDate;
	}

	public String getRetDateOld() {
		return retDateOld;
	}

	public void setRetDateOld(String retDateOld) {
		this.retDateOld = retDateOld;
	}

	public String getDepDateOld() {
		return depDateOld;
	}

	public void setDepDateOld(String depDateOld) {
		this.depDateOld = depDateOld;
	}

}
