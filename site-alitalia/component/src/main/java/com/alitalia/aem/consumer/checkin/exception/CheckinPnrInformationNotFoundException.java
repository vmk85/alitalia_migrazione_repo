package com.alitalia.aem.consumer.checkin.exception;

public class CheckinPnrInformationNotFoundException extends Exception {

	private static final long serialVersionUID = -8388948758281010112L;

	public CheckinPnrInformationNotFoundException(String message) {
		super(message);
	}
	
	public CheckinPnrInformationNotFoundException(Throwable cause) {
		super(cause);
	}
	
}
