package com.alitalia.aem.consumer.millemiglia.servlet.profiloutente;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;
import com.alitalia.aem.common.data.home.SocialAccountData;
import com.alitalia.aem.common.messages.home.RemoveSocialAccountsRequest;
import com.alitalia.aem.common.messages.home.RemoveSocialAccountsResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "socialloginsubmit" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class SocialLoginServlet extends SlingAllMethodsServlet {

	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaCustomerProfileManager customerProfileManager;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		
		final String rimuoviDati = "rimuoviDati";
		final String rimuoviConnessione = "rimuoviConnessione";
		
		try {
			@SuppressWarnings("unchecked")
			Set<String> parameters = request.getParameterMap().keySet();
			int numParam = parameters.size() / 5;
			List<SocialAccountData> socialDaRimuovereList = new ArrayList<SocialAccountData>();
			logger.debug("[SocialLoginServlet] - Start");
			for(int i=0; i<numParam; i++){
				String provider = request.getParameter(i + "_provider");
				String email = request.getParameter(i + "_email");
				String gigyaId = request.getParameter(i + "_gigyaUID");
				String[] choice = request.getParameterValues(i + "_choice");
				SocialAccountData socialAccount = new SocialAccountData();
				socialAccount.setGigyaUID(gigyaId);
				socialAccount.setLoginProvider(provider);
				socialAccount.setEmail(email);
				if(choice != null){
					for(String c : choice){
						logger.debug("CHOICE: " + c);
						if(c.equals(rimuoviDati)){
							socialAccount.setLogicalDelete(true); // FIXME: Verificare quale parametro passare per effettuare la corretta operazione desiderata
							socialDaRimuovereList.add(socialAccount);
						} else if(c.equals(rimuoviConnessione)){
							socialAccount.setLogicalDelete(false); // FIXME: Verificare quale parametro passare per effettuare la corretta operazione desiderata
							socialDaRimuovereList.add(socialAccount);
						}
					}
				}
				logger.debug("[SocialLoginServlet] Request parameter: " + socialAccount.toString());
			}
			
			
			ResourceResolver resourceResolver = request.getResourceResolver();
			Session session = resourceResolver.adaptTo(Session.class);
			UserPropertiesManager userPropertiesManager = resourceResolver.adaptTo(UserPropertiesManager.class);
			UserProperties profileProperties = userPropertiesManager.getUserProperties(session.getUserID(), "profile");
			UserProperties privateProperties = userPropertiesManager.getUserProperties(session.getUserID(), "private");
			
			String customerNumber = profileProperties.getProperty("customerNumber");
			String customerPinCode = 
					customerProfileManager.decodeAndDecryptProperty(privateProperties.getProperty("customerPinCode"));
			//logger.debug("[SocialLoginServlet] customerNumber: " + customerNumber + ", customerPinCode: " + customerPinCode );
			RemoveSocialAccountsRequest removeSocialAccountsRequest = new RemoveSocialAccountsRequest();
			removeSocialAccountsRequest.setSid(IDFactory.getSid(request));
			removeSocialAccountsRequest.setTid(IDFactory.getTid());
			removeSocialAccountsRequest.setSocialAccounts(socialDaRimuovereList);
			removeSocialAccountsRequest.setUserCode(customerNumber);
			removeSocialAccountsRequest.setPin(customerPinCode);
			//logger.debug("[SocialLoginServlet] request: " + removeSocialAccountsRequest.toString());
			RemoveSocialAccountsResponse risp = consumerLoginDelegate.removeUidGigya(removeSocialAccountsRequest);
			if(risp != null){
				//logger.debug("[SocialLoginServlet] response: " + risp.toString());
			}
			
			// goBackSuccessfully
			StringBuffer stringBuffer = request.getRequestURL();
			String servletName = stringBuffer.substring(stringBuffer.lastIndexOf(".")+1, stringBuffer.length());
			String url = stringBuffer.delete(stringBuffer.lastIndexOf("."), stringBuffer.length())
					.append(".html?success=true&from=" + servletName).toString();
			logger.debug("[SocialLoginServlet] redirect back to URL [" + url + "] with success");
			response.sendRedirect(url);
			
		} catch(Exception e) {
			logger.error("[SocialLoginServlet]: exception", e );
			StringBuffer stringBuffer = request.getRequestURL();
			String url = stringBuffer.delete(stringBuffer.lastIndexOf("."), stringBuffer.length())
					.append(".html?success=false&from=socialloginsubmit").toString();
			logger.debug("[SocialLoginServlet] doPost - return URL: [" + url + "]");
			response.sendRedirect(url);
		}
		
	}
}
