package com.alitalia.aem.consumer.servlet.airportslist;

import java.util.Comparator;

public class AirportInfo {

	private String airportCode;
	private String keyAirportName;
	private String airportName;
	private String keyCityCode;
	private String cityCode;
	private String city;
	private String countryCode;
	private String keyCountryCode;
	private String country;
	//TODO aggiungere marketAreaCode per la costruzione del json?

	public static final Comparator<AirportInfo> COMPARE_BY_CITY_AND_AIRPORT =
			new Comparator<AirportInfo>() {

		@Override
		public int compare(AirportInfo one, AirportInfo other) {
			if (one.getCity().compareToIgnoreCase(other.getCity()) == 0) {
				int oneCityAllAirport = one.getCityCode()
						.compareToIgnoreCase(one.getAirportCode());
				int otherCityAllAirport = other.getCityCode()
						.compareToIgnoreCase(other.getAirportCode());
				// INTRODUCTION OF AIRPORT EXCEPTIONS - CR 14444
				int exceptionRule = findAirportExceptions(one, other);
				if (exceptionRule != 0){
					return exceptionRule;
				} else if (oneCityAllAirport == otherCityAllAirport) {
					return one.getAirportName()
							.compareToIgnoreCase(other.getAirportName());
				} else if (oneCityAllAirport < otherCityAllAirport) {
					return -1;
				} else {
					return 1;
				}
			} else if (one.getCity().compareToIgnoreCase(other.getCity()) < 0) {
				return -1;
			} else {
				return 1;
			}
		}
	};
	
	private static int findAirportExceptions(AirportInfo one, AirportInfo other){
		if ("VCP".equals(one.getAirportCode())){
			return 1;
		} else if ("VCP".equals(other.getAirportCode())){
			return -1;
		} else if ("MIL".equals(one.getAirportCode())) {
			return -1;
		} else if ("MIL".equals(other.getAirportCode())) {
			return 1;
		}
		return 0;
	}

	public AirportInfo(String airportCode, String keyAirportName,
			String airportName, String cityCode, String keyCityCode,
			String city, String countryCode, String keyCountryCode,
			String country) {
		super();
		this.airportCode = airportCode;
		this.keyAirportName = keyAirportName;
		this.airportName = airportName;
		this.cityCode = cityCode;
		this.keyCityCode = keyCityCode;
		this.city = city;
		this.countryCode = countryCode;
		this.keyCountryCode = keyCountryCode;
		this.country = country;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public String getKeyAirportName() {
		return keyAirportName;
	}

	public String getAirportName() {
		return airportName;
	}

	public String getCityCode() {
		return cityCode;
	}
	
	public String getKeyCityCode() {
		return keyCityCode;
	}

	public String getCity() {
		return city;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public String getKeyCountryCode() {
		return keyCountryCode;
	}

	public String getCountry() {
		return country;
	}

}