package com.alitalia.aem.consumer.checkin;

import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;

public class CheckinPassengerSeat {
	
	private String seat;
	private MmbCompartimentalClassEnum seatClass;
	private boolean comfortSeat;
	private MmbPriceRender price;
	
	public CheckinPassengerSeat(String seat, MmbCompartimentalClassEnum seatClass,
			boolean comfortSeat, MmbPriceRender price) {
		this(seat, seatClass);
		this.comfortSeat = comfortSeat;
		this.price = price;
	}
	
	public CheckinPassengerSeat(String seat, MmbCompartimentalClassEnum seatClass) {
		super();
		this.seat = seat;
		this.seatClass = seatClass;
		this.comfortSeat = false;
	}

	public String getSeat() {
		return seat;
	}
	
	public void setSeat(String seat) {
		this.seat = seat;
	}
	
	public MmbCompartimentalClassEnum getSeatClass() {
		return seatClass;
	}

	public void setSeatClass(MmbCompartimentalClassEnum seatClass) {
		this.seatClass = seatClass;
	}

	public boolean isComfortSeat() {
		return comfortSeat;
	}
	
	public void setComfortSeat(boolean comfortSeat) {
		this.comfortSeat = comfortSeat;
	}

	public MmbPriceRender getPrice() {
		return price;
	}
	
	public void setPrice(MmbPriceRender price) {
		this.price = price;
	}
	
	public String getSeatClassValue() {
		return seatClass == null ? "" : seatClass.toString();
	}

	@Override
	public String toString() {
		return "CheckinPassengerSeat [seat=" + seat + ", comfortSeat="
				+ comfortSeat + ", price=" + price + "]";
	}
}
