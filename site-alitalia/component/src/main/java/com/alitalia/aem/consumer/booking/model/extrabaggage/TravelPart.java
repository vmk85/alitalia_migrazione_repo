package com.alitalia.aem.consumer.booking.model.extrabaggage;


import java.util.*;

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
///#region References
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
///#endregion


/** 
 Sabre Request Model for TravelPart
*/
public class TravelPart
{
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [JsonProperty(PropertyName = "@ref")] public string _ref {get;set;}
	private String _ref;
	public final String get_ref()
	{
		return _ref;
	}
	public final void set_ref(String value)
	{
		_ref = value;
	}
	private String departure;
	public final String getdeparture()
	{
		return departure;
	}
	public final void setdeparture(String value)
	{
		departure = value;
	}
	private String arrival;
	public final String getarrival()
	{
		return arrival;
	}
	public final void setarrival(String value)
	{
		arrival = value;
	}
	private String destination;
	public final String getdestination()
	{
		return destination;
	}
	public final void setdestination(String value)
	{
		destination = value;
	}
	private String origin;
	public final String getorigin()
	{
		return origin;
	}
	public final void setorigin(String value)
	{
		origin = value;
	}
	private TravelPartType type;
	public final TravelPartType gettype()
	{
		return type;
	}
	public final void settype(TravelPartType value)
	{
		type = value;
	}
	private ArrayList<Segment> segments;
	public final ArrayList<Segment> getsegments()
	{
		return segments;
	}
	public final void setsegments(ArrayList<Segment> value)
	{
		segments = value;
	}
	private int stops;
	public final int getstops()
	{
		return stops;
	}
	public final void setstops(int value)
	{
		stops = value;
	}
	private int totalDuration;
	public final int gettotalDuration()
	{
		return totalDuration;
	}
	public final void settotalDuration(int value)
	{
		totalDuration = value;
	}
	private ArrayList<ConnectionInformation> connectionInformations;
	public final ArrayList<ConnectionInformation> getconnectionInformations()
	{
		return connectionInformations;
	}
	public final void setconnectionInformations(ArrayList<ConnectionInformation> value)
	{
		connectionInformations = value;
	}
	private String bookingClass;
	public final String getbookingClass()
	{
		return bookingClass;
	}
	public final void setbookingClass(String value)
	{
		bookingClass = value;
	}
	private ArrayList<Segment> cancelledSegments;
	public final ArrayList<Segment> getcancelledSegments()
	{
		return cancelledSegments;
	}
	public final void setcancelledSegments(ArrayList<Segment> value)
	{
		cancelledSegments = value;
	}
	private ArrayList<Advisory> advisories;
	public final ArrayList<Advisory> getadvisories()
	{
		return advisories;
	}
	public final void setadvisories(ArrayList<Advisory> value)
	{
		advisories = value;
	}
	private ArrayList<ItineraryPart> itineraryParts;
	public final ArrayList<ItineraryPart> getitineraryParts()
	{
		return itineraryParts;
	}
	public final void setitineraryParts(ArrayList<ItineraryPart> value)
	{
		itineraryParts = value;
	}
	private ArrayList<SegmentOfferInformation> segmentOfferInformation;
	public final ArrayList<SegmentOfferInformation> getsegmentOfferInformation()
	{
		return segmentOfferInformation;
	}
	public final void setsegmentOfferInformation(ArrayList<SegmentOfferInformation> value)
	{
		segmentOfferInformation = value;
	}
	private int duration;
	public final int getduration()
	{
		return duration;
	}
	public final void setduration(int value)
	{
		duration = value;
	}
	private String cabinClass;
	public final String getcabinClass()
	{
		return cabinClass;
	}
	public final void setcabinClass(String value)
	{
		cabinClass = value;
	}
	private String equipment;
	public final String getequipment()
	{
		return equipment;
	}
	public final void setequipment(String value)
	{
		equipment = value;
	}
	private String aircraftLeaseText;
	public final String getaircraftLeaseText()
	{
		return aircraftLeaseText;
	}
	public final void setaircraftLeaseText(String value)
	{
		aircraftLeaseText = value;
	}
	private Flight flight;
	public final Flight getflight()
	{
		return flight;
	}
	public final void setflight(Flight value)
	{
		flight = value;
	}
	private String segmentStatusCode;
	public final String getsegmentStatusCode()
	{
		return segmentStatusCode;
	}
	public final void setsegmentStatusCode(String value)
	{
		segmentStatusCode = value;
	}
	private int layoverDuration;
	public final int getlayoverDuration()
	{
		return layoverDuration;
	}
	public final void setlayoverDuration(int value)
	{
		layoverDuration = value;
	}
	private String previouslySelectedBookingClass;
	public final String getpreviouslySelectedBookingClass()
	{
		return previouslySelectedBookingClass;
	}
	public final void setpreviouslySelectedBookingClass(String value)
	{
		previouslySelectedBookingClass = value;
	}
	private String fareBasis;
	public final String getfareBasis()
	{
		return fareBasis;
	}
	public final void setfareBasis(String value)
	{
		fareBasis = value;
	}

}