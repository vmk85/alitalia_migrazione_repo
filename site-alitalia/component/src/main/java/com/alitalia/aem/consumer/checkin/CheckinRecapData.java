package com.alitalia.aem.consumer.checkin;

public class CheckinRecapData {
	private String passengerName;
	private String frequentFlyer;
	private String ticketNumber;
	private String pnr;
	private String seat;
	private String itinerary;
	private String flightNumber;
	private String departureTime;
	private String terminal;
	private String email;
	private String phone;
	
	
	public CheckinRecapData(String passengerName, String frequentFlyer, String ticketNumber, String pnr, String seat, String itinerary, String flightNumber, String departureTime, String terminal) {
		this.passengerName = passengerName;
		this.frequentFlyer = frequentFlyer;
		this.ticketNumber = ticketNumber;
		this.pnr = pnr;
		this.seat = seat;
		this.itinerary = itinerary;
		this.flightNumber = flightNumber;
		this.departureTime = departureTime;
		this.terminal = terminal;
	}
	
	
	public CheckinRecapData(String passengerName, String email, String phone) {
		this.passengerName = passengerName;
		this.email = email;
		this.phone = phone;
	}
	
	
	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	
	public String getFrequentFlyer() {
		return frequentFlyer;
	}

	public void setFrequentFlyer(String frequentFlyer) {
		this.frequentFlyer = frequentFlyer;
	}


	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}


	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}


	public String getSeat() {
		return seat;
	}
	
	public void setSeat(String seat) {
		this.seat = seat;
	}

	
	public String getItinerary() {
		return itinerary;
	}

	public void setItinerary(String itinerary) {
		this.itinerary = itinerary;
	}


	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}


	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}


	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	@Override
	public String toString() {
		return "CheckinRecapData [passengerName=" + passengerName
				+ ", frequentFlyer=" + frequentFlyer + ", ticketNumber="
				+ ticketNumber + ", pnr=" + pnr + ", seat=" + seat
				+ ", itinerary=" + itinerary + ", flightNumber=" + flightNumber
				+ ", departureTime=" + departureTime + ", terminal=" + terminal
				+ ", email=" + email + ", phone=" + phone + "]";
	}
	
	
}
