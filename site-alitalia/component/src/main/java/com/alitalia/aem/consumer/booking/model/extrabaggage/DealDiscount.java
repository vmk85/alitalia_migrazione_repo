package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Sabre Request Model for DealDiscount
*/
public class DealDiscount
{
	private String offerName;
	public final String getofferName()
	{
		return offerName;
	}
	public final void setofferName(String value)
	{
		offerName = value;
	}
	private ApiPrice totalDiscount;
	public final ApiPrice gettotalDiscount()
	{
		return totalDiscount;
	}
	public final void settotalDiscount(ApiPrice value)
	{
		totalDiscount = value;
	}
	private int percentage;
	public final int getpercentage()
	{
		return percentage;
	}
	public final void setpercentage(int value)
	{
		percentage = value;
	}
}