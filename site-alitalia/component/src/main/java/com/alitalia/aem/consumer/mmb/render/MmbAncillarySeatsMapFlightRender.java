package com.alitalia.aem.consumer.mmb.render;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alitalia.aem.common.data.home.SeatMapData;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;

public class MmbAncillarySeatsMapFlightRender {
	
	private MmbFlightDataRender flight;
	private MmbAncillarySeatsMapRender seatMap;
	private List<MmbAncillarySeatsMapPassengerRender> passengers;
	private boolean mappaPostiEnabled;
	
	public MmbAncillarySeatsMapFlightRender(MmbFlightData flightData) {
		this.flight = new MmbFlightDataRender(flightData);
		this.passengers = new ArrayList<MmbAncillarySeatsMapPassengerRender>();
	}
	
	public MmbFlightDataRender getFlight() {
		return this.flight;
	}
	
	public MmbAncillarySeatsMapRender getSeatMap() {
		return this.seatMap;
	}
	
	public List<MmbAncillarySeatsMapPassengerRender> getPassengers() {
		return this.passengers;
	}
	
	public void addPassenger(MmbAncillarySeatsMapPassengerRender passenger) {
		passengers.add(passenger);
	}
	
	public boolean getMappaPostiEnabled() {
		return this.mappaPostiEnabled;
	}
	
	public void SetMappaPostiEnabled(boolean mappaPosti) {
		this.mappaPostiEnabled=mappaPosti;
	}
	
	public void prepareSeatmapRenderUsingPassengersData(SeatMapData seatMap) {
		Map<String, Boolean> currentPnrAssignedSeats = new HashMap<String, Boolean>();
		for (MmbAncillarySeatsMapPassengerRender passenger : this.passengers) {
			currentPnrAssignedSeats.put(passenger.getCurrentValue(), passenger.isCurrentValueComfort());
			currentPnrAssignedSeats.put(passenger.getPreviousValue(), passenger.isPreviousValueComfort());
		}
		this.seatMap = new MmbAncillarySeatsMapRender(seatMap, currentPnrAssignedSeats);
	}
	
}
