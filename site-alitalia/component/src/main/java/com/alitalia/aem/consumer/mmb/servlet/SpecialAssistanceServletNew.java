package com.alitalia.aem.consumer.mmb.servlet;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.common.messages.home.MmbSendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbSendEmailResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeyComponents;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.managemybooking.delegate.ManageMyBookingDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "specialassistanceservletnew" }),
		@Property(name = "sling.servlet.methods", value = { "GET", "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "mail.from", description = "Mail address of the sender"),
		@Property(name = "mail.to", description = "Mail address of the recipient")
})
@SuppressWarnings("serial")
public class SpecialAssistanceServletNew extends GenericFormValidatorServlet {

	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ManageMyBookingDelegate mmbDelegate;
	
	private ComponentContext componentContext;
	
	private static final String KEYVALUE_SEPARATOR = ":";
	private static final String ELEMENT_SEPARATOR = System.getProperty("line.separator");
	private static final String KEY_PREFIX = "specialAssistance.";
	private static final String KEY_SEPARATOR = ",";
	private static final String MAIL_FROM = "AlitaliaSpecialServices@alitalia.com";
	private static final String MAIL_TO = "backoffice.alitalia@abramo.com";
	private static final String MAIL_SUBJECT_PREFIX = "Special Assistance + ";
	private static final String MAIL_SUBJECT_SEPARATOR = "&";
	private static final String MAIL_TO_SEPARATOR = ";";
	
	private static final String ATTR_PNR = "pnrCode";
	private static final String ATTR_TICKET= "etktNumber";
	private static final String ATTR_NAME = "firstName";
	private static final String ATTR_LASTNAME = "lastName";
	private static final String ATTR_FLIGHT = "flightNumber";
	private static final String ATTR_PHONE = "phoneNumber";
	private static final String ATTR_EMAIL = "email";
	private static final String ATTR_NOTES = "notes";
	private static final String ATTR_SPECIAL_ASSISTANCE = "specialAssistance";
	private static final String ATTR_HEIGHT = "wheelchairHeight";
	private static final String ATTR_WEIGHT = "wheelchairWeight";
	private static final String ATTR_LENGTH = "wheelchairLength";
	private static final String ATTR_WIDTH = "wheelchairWidth";
	
	private static final String KEY_PNR = "etkt number";
	private static final String KEY_TICKET = "pnr code";
	private static final String KEY_NAME = "Name and family nam";
	private static final String KEY_FLIGHT = "flight/date";
	private static final String KEY_CONTACTS = "contact email/telephone number";
	private static final String KEY_SPECIAL_ASSISTANCE = "type of assistance requested";
	private static final String KEY_LENGTH = "Lenght";
	private static final String KEY_WIDTH = "Width";
	private static final String KEY_HEIGHT = "Height";
	private static final String KEY_WEIGHT = "Weight";
	private static final String KEY_NOTES = "Notes";
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		
		logger.debug("validateForm");
		
		Validator validator = new Validator();
		
		String pnrCode = request.getParameter(ATTR_PNR);
		String etktNumber = request.getParameter(ATTR_TICKET);
		String name = request.getParameter(ATTR_NAME);
		String lastName = request.getParameter(ATTR_LASTNAME);
		String flightNumber = request.getParameter(ATTR_FLIGHT);
		
		String phoneNumber = request.getParameter(ATTR_PHONE);
		String email = request.getParameter(ATTR_EMAIL);
		String specialAssistance = request.getParameter(ATTR_SPECIAL_ASSISTANCE);
		
		// PNR
		validator.addDirectConditionMessagePattern(ATTR_PNR, pnrCode,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		
		// Ticket
		validator.addDirectConditionMessagePattern(ATTR_TICKET, etktNumber,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		
		// Name
		validator.addDirectConditionMessagePattern(ATTR_NAME, name,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		
		// Last name
		validator.addDirectConditionMessagePattern(ATTR_LASTNAME, lastName,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		
		// Flight
		validator.addDirectConditionMessagePattern(ATTR_FLIGHT, flightNumber,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
				
		// special assistance
		validator.addDirectConditionMessagePattern(ATTR_SPECIAL_ASSISTANCE, specialAssistance,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		
		// phone number
		validator.addDirectConditionMessagePattern(ATTR_PHONE, phoneNumber,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.PHONE_NUMBER_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern(ATTR_PHONE, phoneNumber,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.PHONE_NUMBER_ERROR_NOT_VALID, "isNumber");
		
		// e-mail
		validator.addDirectConditionMessagePattern(ATTR_EMAIL, email,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.EMAIL_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern(ATTR_EMAIL, email,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID, "isEmail");
		
		String[] wheelchairKeys = configuration.getSpecialAssistanceWheelchairKeys().split(KEY_SEPARATOR);
				
		boolean extended = isWheelChairAssistance(specialAssistance, wheelchairKeys);
		
		if (extended) {
			String height = request.getParameter(ATTR_HEIGHT);
			String weigth = request.getParameter(ATTR_WEIGHT);
			String lenght = request.getParameter(ATTR_LENGTH);
			String width = request.getParameter(ATTR_WIDTH);
					
			validator.addDirectConditionMessagePattern(ATTR_HEIGHT, height,
					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
					I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
			validator.addDirectConditionMessagePattern(ATTR_HEIGHT, height,
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					I18nKeyComponents.MMB_MANAGE_NUMBER_NOT_VALID, "isNumberWithDecimalPrecision");
					
			validator.addDirectConditionMessagePattern(ATTR_WEIGHT, weigth,
					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
					I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
			validator.addDirectConditionMessagePattern(ATTR_WEIGHT, weigth,
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					I18nKeyComponents.MMB_MANAGE_NUMBER_NOT_VALID, "isNumberWithDecimalPrecision");
					
			validator.addDirectConditionMessagePattern(ATTR_LENGTH, lenght,
					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
					I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
			validator.addDirectConditionMessagePattern(ATTR_LENGTH, lenght,
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					I18nKeyComponents.MMB_MANAGE_NUMBER_NOT_VALID, "isNumberWithDecimalPrecision");
					
			validator.addDirectConditionMessagePattern(ATTR_WIDTH, width,
					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
					I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
			validator.addDirectConditionMessagePattern(ATTR_WIDTH, width,
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					I18nKeyComponents.MMB_MANAGE_NUMBER_NOT_VALID, "isNumberWithDecimalPrecision");
		}
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
		
		logger.debug("performSubmit");
		
		String[] wheelchairKeys = configuration.getSpecialAssistanceWheelchairKeys().split(KEY_SEPARATOR);
		String key = request.getParameter(ATTR_SPECIAL_ASSISTANCE);
		
		boolean extended = isWheelChairAssistance(key, wheelchairKeys);
		
		boolean mailResult = sendMail(request, extended);
		logger.debug("Result: {0}", mailResult);
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		json.key("result").value(mailResult ? "OK" : "NOK");
		json.endObject();
	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception exception) {
		// TODO Auto-generated method stub
		
	}
	
	private boolean isWheelChairAssistance(String key, String[] keys) {
		for (String wheelchairKey : keys) {
			if (wheelchairKey.equals(key)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean sendMail(SlingHttpServletRequest request, boolean extended) {
		String name = request.getParameter(ATTR_NAME);
		String lastname = request.getParameter(ATTR_LASTNAME);
		String customerMail = request.getParameter(ATTR_EMAIL);
		
		String sender = PropertiesUtil.toString(componentContext.getProperties().get("mail.from"), MAIL_FROM);
		String recipient = PropertiesUtil.toString(componentContext.getProperties().get("mail.to"), MAIL_TO);
		
		MmbSendEmailRequest mailRequest = new MmbSendEmailRequest();
		mailRequest.setTid(IDFactory.getTid());
		mailRequest.setSid(IDFactory.getSid(request));
		mailRequest.setSender(sender);
		mailRequest.setRecipient(recipient + MAIL_TO_SEPARATOR + customerMail);
		mailRequest.setMailSubject(MAIL_SUBJECT_PREFIX + name + MAIL_SUBJECT_SEPARATOR + lastname);
		String emailText = generateBodyMail(request, extended);
		mailRequest.setMailBody(emailText);
		mailRequest.setHtmlBody(false);
		
		MmbSendEmailResponse mailResponse =  mmbDelegate.sendMail(mailRequest);

		return mailResponse.isMailSent();
	}
	
	private String generateBodyMail(SlingHttpServletRequest request, boolean extended) {
		boolean hasNotes = true;
		String[][] data;
		int dimForm;
		if (extended) {
			//dimForm = check_flights.length + 10;
			dimForm = 1 + 10;
		}
		else {
			//dimForm = check_flights.length + 6;
			dimForm = 1 + 6;
		}
		if (request.getParameter(ATTR_NOTES) == null || "".equals(request.getParameter(ATTR_NOTES))) {
			dimForm--;
			hasNotes = false;
		}
		
		data = new String[dimForm][2];
		
		// recupero le chiavi
		data[0][0] = KEY_TICKET;
		data[1][0] = KEY_PNR;
		data[2][0] = KEY_NAME;
		
		//for (k=0; k<check_flights.length; k++) {
		//	data[k+3][0] =  request.getParameter("KeyField3");
		//}
		data[3][0] = KEY_FLIGHT;
		
		//int index = check_flights.length + 3;
		int index = 4;
		
		data[index][0] = KEY_CONTACTS;
		data[index+1][0] = KEY_SPECIAL_ASSISTANCE;
		if (extended) {
			data[index+2][0] = KEY_LENGTH;
			data[index+3][0] = KEY_WIDTH;
			data[index+4][0] = KEY_HEIGHT;
			data[index+5][0] = KEY_WEIGHT;
			if (hasNotes) {
				data[index+6][0] = KEY_NOTES;
			}
			
		}
		else {
			if (hasNotes) {
				data[index+2][0] = KEY_NOTES;
			}
			
		}
		
		// associazione chiave valore
		
		data[0][1] = request.getParameter(ATTR_TICKET);
		data[1][1] = request.getParameter(ATTR_PNR);
		data[2][1] = request.getParameter(ATTR_NAME) + " " + request.getParameter(ATTR_LASTNAME);
		
		//for (k=0; k<check_flights.length; k++) {
		//	data[k+3][1] = check_flights[k];
		//}
		data[3][1] = request.getParameter(ATTR_FLIGHT);
		
		//index = check_flights.length + 3;
		index = 4;
		
		data[index][1] = request.getParameter(ATTR_EMAIL) + "/" + request.getParameter(ATTR_PHONE);
		
		I18n i18n = getI18n(request);
		String specialAssistance = i18n.get(KEY_PREFIX + request.getParameter(ATTR_SPECIAL_ASSISTANCE));
		data[index+1][1] = specialAssistance;
		
		if (extended) {
			data[index+2][1] = request.getParameter(ATTR_LENGTH);
			data[index+3][1] = request.getParameter(ATTR_WIDTH);
			data[index+4][1] = request.getParameter(ATTR_HEIGHT);
			data[index+5][1] = request.getParameter(ATTR_WEIGHT);
			if (hasNotes) {
				data[index+6][1] = request.getParameter(ATTR_NOTES);
			}
			
		}
		else {
			if (hasNotes) {
				data[index+2][1] = request.getParameter(ATTR_NOTES);
			}
			
		}
		
		//  genero il testo del messaggio
		String emailText = "";
		for (int k=0; k<dimForm; k++) {
			emailText = emailText + data[k][0] + " " + KEYVALUE_SEPARATOR + " " + data[k][1];
			if (k<dimForm-1) {
				emailText = emailText + ELEMENT_SEPARATOR;
			}
			
		}
		return emailText;
		
	}
	
	private I18n getI18n(SlingHttpServletRequest request) {
		
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		
		return new I18n(resourceBundle);
	}

}
