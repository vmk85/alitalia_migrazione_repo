package com.alitalia.aem.consumer.model.offerdetail;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.wcm.api.Page;

@Model(adaptables={SlingHttpServletRequest.class})
public class OfferDetailModel extends GenericBaseModel {
	
	@Self
	private SlingHttpServletRequest slingHttpServletRequest;
	
	private String imagePath;
	
	@PostConstruct
	protected void initModel() {
		
		Resource resource = slingHttpServletRequest.getResource();
		String[] selectors = slingHttpServletRequest.getRequestPathInfo().getSelectors();
		
		
		if (selectors.length > 1) {
			String destinationsPath = AlitaliaUtils.findSiteBaseRepositoryPath(resource) + 
					AlitaliaConstants.DESTINATIONS_PATH;
			Page page = findPageByName(slingHttpServletRequest, destinationsPath, selectors[1]);
			
			if (page != null) {
				imagePath = page.getPath() + "/jcr:content/image-dettaglio-offerta";
			} else {
				imagePath = destinationsPath + "/default/jcr:content/image-dettaglio-offerta";
			}
		}
	}
	
	public String getImagePath() {
		return imagePath;
	}
	
}
