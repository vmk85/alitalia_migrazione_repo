package com.alitalia.aem.consumer.utils;

import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.AncillaryOffer;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.ws.geospatial.service.xsd6.Offer;
import org.apache.sling.api.SlingHttpServletRequest;

public class AlitaliaMailUtils {


    private final static String FOLDER_PATH = "/etc/designs/alitalia/clientlibs-1/images/";
    private final static String FOLDER_PATH_2 = "/etc/designs/alitalia/clientlibs-1-rtl/images/";
    private final static String FOLDER_PATH_SEAT = "/etc/designs/alitalia/clientlibs/images/mail/";
    private final static String ICON_DEPARTURE = "icon-departure.png";
    private final static String ICON_ARRIVAL = "icon-arrival.png";
    private final static String CLOCK = "icon-clock.png";
    private final static String BAGGAGE = "ico-bagaglio.png";
    private final static String LOUNGE = "ico-lounge.png";
    private final static String ASSICURAZIONE = "ico-assicurazione.png";
    private final static String FAST_TRACK = "ico-fast-track.png";
    private final static String VISA = "icon_visa_new.png";
    private final static String VISA_ELECTRON = "icon_visa-electron.png";
    private final static String MASTERCARD = "icon_mastercard.png";
    private final static String AMERICAN_EXPRESS = "icon_american-express_new.png";
    private final static String CLUB = "icon_diners-club.png";
    private final static String SEAT = "icon-seat.png";
    private final static String FOLDER_ANCILLARY_PATH = "/etc/designs/alitalia/clientlibs-checkin-personalizza-volo-1-embed/images/";

    public String getIconPath(String icon, SlingHttpServletRequest request) {
        String prefix = "http://";
        String path = prefix + request.getServerName() + ":" + request.getServerPort();

        switch (icon) {
            case "departure":
                path += FOLDER_PATH_2 + ICON_DEPARTURE;
                break;

            case "arrival":
                path += FOLDER_PATH_2 + ICON_ARRIVAL;
                break;

            case "clock":
                path += FOLDER_PATH_2 + CLOCK;
                break;

            case "bagaglio":
                path += FOLDER_ANCILLARY_PATH + BAGGAGE;
                break;

            case "lounge":
                path += FOLDER_ANCILLARY_PATH + LOUNGE;
                break;

            case "assicurazione":
                path += FOLDER_ANCILLARY_PATH + ASSICURAZIONE;
                break;

            case "fast":
                path += FOLDER_ANCILLARY_PATH + FAST_TRACK;
                break;

            case "visa":
                path += FOLDER_PATH_2 + VISA;
                break;

            case "visaelectron":
                path += FOLDER_PATH + VISA_ELECTRON;
                break;

            case "mastercard":
                path += FOLDER_PATH_2 + MASTERCARD;
                break;

            case "americanexpress":
                path += FOLDER_PATH + AMERICAN_EXPRESS;
                break;

            case "diners_club_international":
                path += FOLDER_PATH_2 + CLUB;
                break;

            case "seat":
                path += FOLDER_PATH_SEAT + SEAT;
                break;
        }
        return path;
    }

    public String getAncillaryPrice(String caso, int qty, CheckinSessionContext ctx) {
        String price = "";
        AncillaryOffer offer;
        if (ctx.pnrListData.get(0).getFlights().get(0).getSegments().get(0).getPassengers().get(0).getOffers() != null) {
            offer = ctx.pnrListData.get(0).getFlights().get(0).getSegments().get(0).getPassengers().get(0).getOffers();
        } else {
            offer = ctx.pnrListData.get(0).getFlights().get(1).getSegments().get(0).getPassengers().get(0).getOffers();
        }
        if (qty != 0) {
            Float firstPricBag = offer.getFirstBagPrice();
            Float secondPriceBag = offer.getSecondBagPrice();
            Float priceOtherBaggage = offer.getMoreBagsPrice();
            Float loungePrice = offer.getFastTrackPrice();
            Float fastPrice = offer.getLoungePrice();
            switch (caso) {
                case "Baggage":
                    switch (qty) {
                        case 1:
                            price += firstPricBag;
                            break;
                        case 2:
                            price += (firstPricBag + secondPriceBag);
                            break;
                        default:
                            price += (firstPricBag + secondPriceBag) + (priceOtherBaggage * (qty - 2));
                            break;
                    }

                    break;
                case "Lounge":
                    price += (loungePrice * qty);
                    break;

                case "Fast":
                    price += (fastPrice * qty);
            }
        }
        return price;
    }

    public String getAncillaryLabel(String group) {
        String label = "";
        switch (group) {

            case "Baggage":
                label = "checkin.mail.ancillary.bagaglio";
                break;

            case "Lounge":
                label = "checkin.mail.ancillary.lounge";
                break;

            case "Pre-reserved Seat Assignments":
                label = "checkin.mail.ancillary.seat";
                break;

            case "Travel Services":
                label = "checkin.mail.ancillary.fastTrack";
                break;

            case "assicurazione":
                label = "checkin.mail.ancillary.insurance";
                break;

        }
        return label;
    }


}
