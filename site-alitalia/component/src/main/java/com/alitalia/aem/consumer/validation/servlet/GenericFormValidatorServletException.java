package com.alitalia.aem.consumer.validation.servlet;

public class GenericFormValidatorServletException extends RuntimeException {

	private static final long serialVersionUID = -9173564741881587180L;
	
	private boolean isRecoverable;
	
	public GenericFormValidatorServletException(boolean isRecoverable) {
		super();
		this.isRecoverable = isRecoverable;
	}
	
	public GenericFormValidatorServletException(boolean isRecoverable, String message) {
		super(message);
		this.isRecoverable = isRecoverable;
	}
	
	public GenericFormValidatorServletException(boolean isRecoverable, String message, Throwable cause) {
		super(message, cause);
		this.isRecoverable = isRecoverable;
	}

	public boolean isRecoverable() {
		return isRecoverable;
	}
	
}
