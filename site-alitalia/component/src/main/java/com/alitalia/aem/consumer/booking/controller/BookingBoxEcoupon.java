package com.alitalia.aem.consumer.booking.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.ECouponData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingPhaseEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingBoxEcoupon extends BookingSessionGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private Logger logger = LoggerFactory.getLogger(getClass()); 
	
	private boolean couponHidden = false;
	private boolean couponNotYetAvailable = false;
	private boolean couponAvailable = false;
	private boolean couponSpecified = false;
	private ECouponData couponData = null;
	private String couponCode = "";
	private boolean couponInvalid = false;
	private boolean couponNotApplicable = false;
	private boolean displayInvalidMessageAsDialog = false;
	private String couponInvalidErrorMessage = null;
	private boolean hideCouponCode = false; // (for FanPlayr codes)
	private boolean insertedNow;
	private boolean initiallyExpanded;
	private boolean showCCMessage;

	private String bookingHP;
	
	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			
			initBaseModel(request);
			
			couponHidden = false;
			couponNotYetAvailable = false;
			couponAvailable = false;
			couponSpecified = false;
			couponData = null;
			couponCode = "";
			couponInvalid = false;
			couponNotApplicable = false;
			couponInvalidErrorMessage = null;
			hideCouponCode = false;
			initiallyExpanded = false;
			
			insertedNow = "true".equals(request.getParameter("insertedNow"));
			
			if (ctx.phase == BookingPhaseEnum.FLIGHTS_SEARCH
					|| ctx.phase == BookingPhaseEnum.FLIGHTS_SELECTION 
					|| ctx.phase == BookingPhaseEnum.PASSENGERS_DATA) {
				/*Temporaneamente blocchiamo ecoupon in presenza di piu' di un passeggero*/
				if((ctx.searchPassengersNumber != null 
						&&  (ctx.searchPassengersNumber.getNumChildren() 
								+ ctx.searchPassengersNumber.getNumInfants()
								+ ctx.searchPassengersNumber.getNumAdults() 
								+ ctx.searchPassengersNumber.getNumYoung()) > 1)){
					this.couponHidden = false;
					this.couponAvailable = false;
					this.showCCMessage = true;
				}
				else if (!ctx.isCarnetProcess && 
						(ctx.searchKind == BookingSearchKindEnum.SIMPLE ||
						ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP)) {
					
					initiallyExpanded = insertedNow;
					
					if (ctx.coupon == null) {
						
						if (Boolean.FALSE.equals(ctx.isCouponValid)) {
							// not applied and not applicable, e.g. cash and miles already applied
							couponAvailable = true;
							couponSpecified = false;
							couponData = null;
							couponCode = "";
							couponInvalid = true;
							couponNotApplicable = false;
							couponInvalidErrorMessage = "E-Coupon non applicable.";
						
						} else {
							// not applied and possibly applicable, based on valid selections
							boolean flightSelectionsPerformed = true;
							for (FlightSelection selection : ctx.flightSelections) {
								if (selection == null) {
									flightSelectionsPerformed = false;
									break;
								}
							}
							if (flightSelectionsPerformed) {
								couponAvailable = true;
							} else {
								couponNotYetAvailable = true;
							}
							
						}
						
					} else {
						// coupon applied
						couponSpecified = true;
						couponData = ctx.coupon;
						couponCode = ctx.coupon.getCode();
						couponInvalid = !ctx.isCouponValid;
						hideCouponCode = ctx.isCouponFanPlayr;
						if (couponInvalid) {
							boolean isBrandLight = couponData.getIsBrandLight() != null ? 
									couponData.getIsBrandLight() : false;
							couponAvailable = true;

							String minNumPax = couponData.getAnyPaxMinNumPax();
							String maxNumPax = couponData.getAnyPaxMinNumPax();
							String minAmount = couponData.getMinAmount();
							String flightDateFrom = calendarToDateString(couponData.getDatVolFrm());
							String flightDateTo = calendarToDateString(couponData.getDatVolTo());
							String flightTimeFrom = calendarToTimeString(couponData.getDatVolFrm());
							String flightTimeTo = calendarToTimeString(couponData.getDatVolTo());
							String flightDateExclusion1From = calendarToDateString(couponData.getDatBlkFrm());
							String flightDateExclusion1To = calendarToDateString(couponData.getDatBlkTo());
							String flightDateExclusion2From = calendarToDateString(couponData.getDatBlkFrmDue());
							String flightDateExclusion2To = calendarToDateString(couponData.getDatBlkToDue());
							String flightDateExclusion3From = calendarToDateString(couponData.getDatBlkFrmTre());
							String flightDateExclusion3To = calendarToDateString(couponData.getDatBlkToTre());
							String flightWeekDays = couponData.getWeekTravellDay();
							String useDateFrom = calendarToDateString(couponData.getDatUseFrm());
							String useDateTo = calendarToDateString(couponData.getDatUseTo());
							String useWeekDays = couponData.getWeekTravellDay();
							
							int errorCode = couponData.getErrorCode().intValue();

							if (Boolean.TRUE.equals(ctx.isCouponFanPlayr)) {

								// il messaggio di errore e' generico per i codici FanPlayr
								displayInvalidMessageAsDialog = true;
								couponInvalidErrorMessage = i18n.get("booking.couponGenericError.message");

							} else if (ctx.isECouponWithTariffaLight && isBrandLight) {
								couponNotApplicable = true;
								displayInvalidMessageAsDialog = true;
								couponInvalidErrorMessage = i18n.get("booking.couponNotApplicableWithTariffaLight.message");							
							} else {
								switch (errorCode) {
									
									case 2000:
										couponInvalidErrorMessage = i18n.get("booking.couponAlreadyUsed.message");
										break;
									
									case 1100:
										couponNotApplicable = true;
										displayInvalidMessageAsDialog = true;
										couponInvalidErrorMessage = "<p>" + i18n.get("booking.couponNotApplicable-generic.message") + "</p>";
										break;
									
									case 1500:
										couponNotApplicable = true;
										displayInvalidMessageAsDialog = true;
										couponInvalidErrorMessage = "<p>" +i18n.get("booking.couponNotApplicable-detailHeader.message") + "</p>";
										couponInvalidErrorMessage += "<ul>";
										if (notNullOrEmpty(flightDateFrom, flightDateTo)) {
											couponInvalidErrorMessage += "<li>";
											couponInvalidErrorMessage += i18n.get(
													"booking.couponNotApplicable-detailFlightDate.message", "", 
													new Object[] { flightDateFrom, flightDateTo });
											if (notNullOrEmpty(flightDateExclusion1From, flightDateExclusion1To)) {
												couponInvalidErrorMessage += i18n.get(
														"booking.couponNotApplicable-detailFlightDateExclusion.message", "",
														new Object[] { flightDateExclusion1From, flightDateExclusion1To });
											}
											if (notNullOrEmpty(flightDateExclusion2From, flightDateExclusion2To)) {
												couponInvalidErrorMessage += i18n.get(
														"booking.couponNotApplicable-detailFlightDateExclusion.message", "",
														new Object[] { flightDateExclusion2From, flightDateExclusion2To });
											}
											if (notNullOrEmpty(flightDateExclusion3From, flightDateExclusion3To)) {
												couponInvalidErrorMessage += i18n.get(
														"booking.couponNotApplicable-detailFlightDateExclusion.message", "",
														new Object[] { flightDateExclusion3From, flightDateExclusion3To });
											}
											couponInvalidErrorMessage += "</li>";
										}
										if (notNullOrEmpty(useDateFrom, useDateTo)) {
											couponInvalidErrorMessage += "<li>";
											couponInvalidErrorMessage += i18n.get("booking.couponNotApplicable-detailUseDate.message",
													"", useDateFrom, useDateTo);
											couponInvalidErrorMessage += "</li>";
										}
										// TODO [booking/coupon] verificare msg errore: <li>viaggio in {0}</li>
										// TODO [booking/coupon] verificare msg errore: <li>voli {1}</li><li>voli operati da alitalia e gruppo cai</li>{0}
										couponInvalidErrorMessage += "</ul>";
										couponInvalidErrorMessage += "<p>" + i18n.get("booking.couponNotApplicable-detailFooter.message") + "</p>";
										break;
									
									case 1400:
										couponInvalidErrorMessage = i18n.get("booking.couponExpired.message");
										break;
									
									case 1000:
										couponInvalidErrorMessage = i18n.get("booking.couponIncorrect.message");
										break;
									
									case 1300:
										couponNotApplicable = true;
										displayInvalidMessageAsDialog = true;
										couponInvalidErrorMessage = "<p>" + i18n.get("booking.couponPaxNumberError.message", "", 
												new Object[] { minNumPax, maxNumPax }) + "</p>";
										break;
									
									case 1200:
										couponNotApplicable = true;
										displayInvalidMessageAsDialog = true;
										String currency = AlitaliaUtils.getInheritedProperty(request.getResource(), 
												"currencyCode", String.class);
										couponInvalidErrorMessage = "<p>" + i18n.get("booking.couponPriceIncorrect.message", "",
												new Object[] { minAmount, currency }) + "</p>";
										break;
									
									case 1600:
										couponNotApplicable = true;
										displayInvalidMessageAsDialog = true;
										couponInvalidErrorMessage = "<p>" +
												i18n.get("booking.couponNotApplicableFlight-flightDate", "", new Object[] { flightDateFrom, flightDateTo }) +
												i18n.get("booking.couponNotApplicableFlight-flightWeekDays", "", new Object[] { flightWeekDays }) +
												i18n.get("booking.couponNotApplicableFlight-flightTime", "", new Object[] { flightTimeFrom, flightTimeTo }) +
												i18n.get("booking.couponNotApplicableFlight-footer") + "</p>";
										break;
									
									case 1700:
										couponNotApplicable = true;
										displayInvalidMessageAsDialog = true;
										couponInvalidErrorMessage = "<p>" +
												i18n.get("booking.couponNotApplicableUse-useDate", "", new Object[] { useDateFrom, useDateTo }) +
												i18n.get("booking.couponNotApplicableUse-useWeekDays", "", new Object[] { useWeekDays }) +
												i18n.get("booking.couponNotApplicableUse-footer") + "</p>";
										break;
									
									case 1800:
										couponNotApplicable = true;
										displayInvalidMessageAsDialog = true;
										couponInvalidErrorMessage = "<p>" + i18n.get("booking.couponOnlyOutboundError.message") + "</p>";
										break;
										
									case 1900:
										couponNotApplicable = true;
										displayInvalidMessageAsDialog = true;
										couponInvalidErrorMessage = "<p>" + i18n.get("booking.couponOnlyRoundTripError.message") + "</p>";
										break;	
										
									case 3005:
										couponNotApplicable = true;
										displayInvalidMessageAsDialog = true;
										couponInvalidErrorMessage = "<p>" + i18n.get("booking.couponNotApplicable-flightWithStop.message") + "</p>";
										break;
									
									default:
										logger.error("Unrecognized error code received for coupon {0}: {1}", errorCode, 
												couponData.getErrorDescription());
										couponInvalidErrorMessage = i18n.get("booking.couponUnknownError.message", "",
												new Object[] { errorCode });
										break;
								}
							}
						}
					}
					
				} else {
					couponHidden = true;
				}
				
			} else {
				couponHidden = true;
			}
			if (configuration != null) {
				String baseUrl = request.getResource().getResourceResolver().map(
						AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
				bookingHP = baseUrl + configuration.getBookingHomePage();
			} else {
				logger.error("AlitaliaConfigurationHolder is null");
				bookingHP = "#";
			}
		} catch (Exception e) {
			logger.error("Unexpected error: " + e);
			throw e;
		}
	}
	
	public String getBookingHP() {
		return bookingHP;
	}

	public boolean getCouponHidden() {
		return couponHidden;
	}
	
	public boolean getCouponNotYetAvailable() {
		return couponNotYetAvailable;
	}

	public boolean getCouponAvailable() {
		return couponAvailable;
	}

	public boolean getCouponSpecified() {
		return couponSpecified;
	}
	
	public ECouponData getCouponData() {
		return couponData;
	}

	public boolean getCouponInvalid() {
		return couponInvalid;
	}
	
	public boolean getCouponNotApplicable() {
		return couponNotApplicable;
	}

	public boolean getDisplayInvalidMessageAsDialog() {
		return displayInvalidMessageAsDialog;
	}
	
	public String getCouponInvalidErrorMessage() {
		return couponInvalidErrorMessage;
	}

	public String getCouponCode() {
		return couponCode;
	}
	
	public boolean getHideCouponCode() {
		return hideCouponCode;
	}
	
	public boolean getInitiallyExpanded() {
		return initiallyExpanded;
	}
	
	public boolean getInsertedNow() {
		return insertedNow;
	}
	
	/* utility methods */
	
	private String calendarToDateString(Calendar source) {
		if (source == null || source.get(Calendar.YEAR) <= 1) {
			return "";
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(source.getTime());
		}
	}
	
	private String calendarToTimeString(Calendar source) {
		if (source == null || source.get(Calendar.YEAR) <= 1) {
			return "";
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
			return sdf.format(source.getTime());
		}
	}
	
	private boolean notNullOrEmpty(Object... arguments) {
		if (arguments != null) {
			for (Object object : arguments) {
				if (object == null || "".equals(object)) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean isShowCCMessage() {
		return showCCMessage;
	}
	
}