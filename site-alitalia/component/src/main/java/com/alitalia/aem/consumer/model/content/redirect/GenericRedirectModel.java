package com.alitalia.aem.consumer.model.content.redirect;

import com.adobe.cq.sightly.WCMUse;

public class GenericRedirectModel extends WCMUse {
	@Override
    public void activate() throws Exception {
    
		// redirect to specified URL
		String url = (String) 
				getCurrentPage().getProperties().get("redirectTarget");
        if (url != null && !"".equals(url)) {
        	if (url.startsWith("/") && !url.startsWith("//") &&
        			-1 == url.indexOf(".html")) {
        		url += ".html";
        	}
        	getResponse().sendRedirect(url);
        }
    }
}