package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Sabre Request Model for PassengerOffer
*/
public class PassengerOffer
{
	private AncillaryPassenger ancillaryPassenger;
	public final AncillaryPassenger getancillaryPassenger()
	{
		return ancillaryPassenger;
	}
	public final void setancillaryPassenger(AncillaryPassenger value)
	{
		ancillaryPassenger = value;
	}
	private AncillaryPrice ancillaryPrice;
	public final AncillaryPrice getancillaryPrice()
	{
		return ancillaryPrice;
	}
	public final void setancillaryPrice(AncillaryPrice value)
	{
		ancillaryPrice = value;
	}
	private int assignedQuantity;
	public final int getassignedQuantity()
	{
		return assignedQuantity;
	}
	public final void setassignedQuantity(int value)
	{
		assignedQuantity = value;
	}
	private boolean available;
	public final boolean getavailable()
	{
		return available;
	}
	public final void setavailable(boolean value)
	{
		available = value;
	}
}