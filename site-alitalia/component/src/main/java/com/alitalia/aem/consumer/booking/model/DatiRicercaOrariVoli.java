package com.alitalia.aem.consumer.booking.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.alitalia.aem.consumer.utils.AlitaliaUtils;


public class DatiRicercaOrariVoli {

	private String codiceAereoportoPartenza;
	private String cittaPartenza;
	private String statoPartenza;
	private String codiceAereoportoArrivo;
	private String cittaArrivo;
	private String statoArrivo;
	private String dataPartenza;
	private String fasciaOraria;
	private String giornoPartenza;
	private String mesePartenza;
	private String annoPartenza;
	
	public DatiRicercaOrariVoli(String codiceAereoportoPartenza, String codiceAereoportoArrivo
			, String dataPartenza, String fasciaOraria, String codiceStatoPartenza
			, String codiceStatoArrivo, String dateFormat) throws ParseException {
		this.codiceAereoportoPartenza = codiceAereoportoPartenza;
		this.codiceAereoportoArrivo = codiceAereoportoArrivo;
		SimpleDateFormat df = new SimpleDateFormat(dateFormat);
		df.setLenient(false);
		Date departureDate;
	
			departureDate = df.parse(dataPartenza);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(departureDate);
			
			String giornoWithZero= calendar.get(Calendar.DAY_OF_MONTH)< 10 ?"0"+calendar.get(Calendar.DAY_OF_MONTH) : calendar.get(Calendar.DAY_OF_MONTH)+"";
			this.giornoPartenza =giornoWithZero ; 
			String meseWithZero=calendar.get(Calendar.MONTH)+1< 10 ?"0"+(calendar.get(Calendar.MONTH)+1) : (calendar.get(Calendar.MONTH)+1)+"";
			this.mesePartenza = meseWithZero;
			this.annoPartenza = calendar.get(Calendar.YEAR)+"";
		
		
		this.dataPartenza = dataPartenza;
		this.fasciaOraria = fasciaOraria;
		this.cittaPartenza = "airportsData." + codiceAereoportoPartenza + ".city";
		this.cittaArrivo = "airportsData." + codiceAereoportoArrivo + ".city";
		this.statoPartenza = "airportsData." + codiceStatoPartenza + ".country";
		this.statoArrivo = "airportsData." + codiceStatoArrivo + ".country";
		
		
	}
	
	public String getCittaPartenza() {
		return cittaPartenza;
	}

	public void setCittaPartenza(String cittaPartenza) {
		this.cittaPartenza = cittaPartenza;
	}

	public String getCittaArrivo() {
		return cittaArrivo;
	}

	public void setCittaArrivo(String cittaArrivo) {
		this.cittaArrivo = cittaArrivo;
	}

	public String getDataPartenza() {
		return dataPartenza;
	}

	public void setDataPartenza(String dataPartenza) {
		this.dataPartenza = dataPartenza;
	}

	public String getFasciaOraria() {
		return fasciaOraria;
	}

	public void setFasciaOraria(String fasciaOraria) {
		this.fasciaOraria = fasciaOraria;
	}
	
	public String getCodiceAereoportoPartenza() {
		return codiceAereoportoPartenza;
	}

	public void setCodiceAereoportoPartenza(String codiceAereoportoPartenza) {
		this.codiceAereoportoPartenza = codiceAereoportoPartenza;
	}

	public String getCodiceAereoportoArrivo() {
		return codiceAereoportoArrivo;
	}

	public void setCodiceAereoportoArrivo(String codiceAereoportoArrivo) {
		this.codiceAereoportoArrivo = codiceAereoportoArrivo;
	}

	public String getGiornoPartenza() {
		return giornoPartenza;
	}

	public void setGiornoPartenza(String giornoPartenza) {
		this.giornoPartenza = giornoPartenza;
	}

	public String getMesePartenza() {
		return mesePartenza;
	}

	public void setMesePartenza(String mesePartenza) {
		this.mesePartenza = mesePartenza;
	}

	public String getAnnoPartenza() {
		return annoPartenza;
	}

	public void setAnnoPartenza(String annoPartenza) {
		this.annoPartenza = annoPartenza;
	}

	public String getStatoPartenza() {
		return statoPartenza;
	}

	public void setStatoPartenza(String statoPartenza) {
		this.statoPartenza = statoPartenza;
	}

	public String getStatoArrivo() {
		return statoArrivo;
	}

	public void setStatoArrivo(String statoArrivo) {
		this.statoArrivo = statoArrivo;
	}
	
}
