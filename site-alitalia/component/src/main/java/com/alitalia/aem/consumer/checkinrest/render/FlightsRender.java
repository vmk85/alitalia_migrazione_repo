package com.alitalia.aem.consumer.checkinrest.render;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.passengerdata.TotalAncillaryForPax;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Destination;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Origin;

public class FlightsRender {

	public List<TotalAncillaryForPax> getTotalAncillaryForPax() {
		return totalAncillaryForPax;
	}

	public void setTotalAncillaryForPax(List<TotalAncillaryForPax> totalAncillaryForPax) {
		this.totalAncillaryForPax = totalAncillaryForPax;
	}

	private List<TotalAncillaryForPax> totalAncillaryForPax;


	private Origin origin;

	private Destination destination;

	private String arrivalDate;

	private String departureDate;

	private String originalDepartureDate;

	private List<SegmentRender> segments = null;

	private Boolean isWebCheckinPermitted;

	private Boolean isBpPrintPermitted;

	private String webCheckInDeepLink;

	private String checkinOnExternalSite;
	
	private String arrivalTime;
	
	private String departureTime;
	
	private String totalNumberPassengers;

	private String numberCheckinExecuted;

	private String numberCheckinNotAllowed;

	private Boolean tooLate;

	private Boolean isVoloAlitalia;

	public Boolean getVoloAlitalia() {
		return isVoloAlitalia;
	}

	public void setVoloAlitalia(Boolean voloAlitalia) {
		isVoloAlitalia = voloAlitalia;
	}

	public Origin getOrigin() {
		return origin;
	}

	public void setOrigin(Origin origin) {
		this.origin = origin;
	}

	public Destination getDestination() {
		return destination;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getOriginalDepartureDate() {
		return originalDepartureDate;
	}

	public void setOriginalDepartureDate(String originalDepartureDate) {
		this.originalDepartureDate = originalDepartureDate;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public List<SegmentRender> getSegments() {
		return segments;
	}

	public void setSegments(List<SegmentRender> segments) {
		this.segments = segments;
	}

	public Boolean getIsWebCheckinPermitted() {
		return isWebCheckinPermitted;
	}

	public void setIsWebCheckinPermitted(Boolean isWebCheckinPermitted) {
		this.isWebCheckinPermitted = isWebCheckinPermitted;
	}

	public Boolean getIsBpPrintPermitted() {
		return isBpPrintPermitted;
	}

	public void setIsBpPrintPermitted(Boolean isBpPrintPermitted) {
		this.isBpPrintPermitted = isBpPrintPermitted;
	}

	public String getWebCheckInDeepLink() {
		return webCheckInDeepLink;
	}

	public void setWebCheckInDeepLink(String webCheckInDeepLink) {
		this.webCheckInDeepLink = webCheckInDeepLink;
	}


	public String getTotalNumberPassengers() {
		return totalNumberPassengers;
	}

	public void setTotalNumberPassengers(String totalNumberPassengers) {
		this.totalNumberPassengers = totalNumberPassengers;
	}

	public String getNumberCheckinExecuted() {
		return numberCheckinExecuted;
	}

	public void setNumberCheckinExecuted(String numberCheckinExecuted) {
		this.numberCheckinExecuted = numberCheckinExecuted;
	}

	public String getNumberCheckinNotAllowed() {
		return numberCheckinNotAllowed;
	}

	public void setNumberCheckinNotAllowed(String numberCheckinNotAllowed) {
		this.numberCheckinNotAllowed = numberCheckinNotAllowed;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	
	public Boolean getTooLate() {
		return tooLate;
	}

	public void setTooLate(Boolean tooLate) {
		this.tooLate = tooLate;
	}

	public String getCheckinOnExternalSite() {
		return checkinOnExternalSite;
	}

	public void setCheckinOnExternalSite(String checkinOnExternalSite) {
		this.checkinOnExternalSite = checkinOnExternalSite;
	}
}
