package com.alitalia.aem.consumer.carnet.data;

public class RecuperoPasswordData {
	public static final String NAME = "RecuperaPasswordData";
	
	private String mail;
	private String code;
	private String error;
	
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getError() {
		return error;
	}
	
	public void setError(String error) {
		this.error = error;
	}
	
	@Override
	public String toString() {
		return "RecuperaPinData [code=" + code + ", mail=" + mail +  "]";
	}
}
