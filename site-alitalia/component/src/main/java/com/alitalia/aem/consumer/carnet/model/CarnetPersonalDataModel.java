package com.alitalia.aem.consumer.carnet.model;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.StateData;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CarnetPersonalDataModel extends GenericCarnetModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	private List<CountryData> nations;
	private String canadaStates;
	private String usaStates;
	private String provinces;
	
	private static final String SEPARATOR = "#";

	
	@PostConstruct
	protected void initModel() {
		
		initBaseModel(request);
		
		nations = ctx.countries;
		
		
		// Init date controls
		provinces = "";
		List<CountryData> countries = ctx.provinces;
		for (CountryData country : countries) {
			 provinces = provinces + country.getStateCode() + "|" + i18n.get(country.getDescription()) + SEPARATOR;
		}
		provinces = provinces.substring(0, provinces.length()-1);
		
		usaStates = "";
		List<StateData> states = ctx.countriesUSA;
		for (StateData state : states) {
			usaStates = usaStates + state.getStateCode() + "|" + i18n.get(state.getStateDescription()) + SEPARATOR;
		}
		usaStates = usaStates.substring(0, usaStates.length()-1);
		
		canadaStates = "";
		states = ctx.countriesCAN;
		for (StateData state : states) {
			canadaStates = canadaStates + state.getStateCode() + "|" + i18n.get(state.getStateDescription()) + SEPARATOR;
		}
		canadaStates = canadaStates.substring(0, canadaStates.length()-1);

	}


	public List<CountryData> getNations() {
		return nations;
	}


	public String getCanadaStates() {
		return canadaStates;
	}


	public String getUsaStates() {
		return usaStates;
	}


	public String getProvinces() {
		return provinces;
	}


	
	
}


