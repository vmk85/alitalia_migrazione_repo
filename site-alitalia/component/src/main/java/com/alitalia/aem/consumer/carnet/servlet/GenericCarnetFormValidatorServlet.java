package com.alitalia.aem.consumer.carnet.servlet;

import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.carnet.CarnetConstants;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;

@SuppressWarnings("serial")
public abstract class GenericCarnetFormValidatorServlet
		extends GenericFormValidatorServlet {
	
	/**
	 * Utility method to access the CarnetSessionContext instance.
	 * 
	 * <p>If the instance is not available, an exception is thrown.</p>
	 * 
	 * @param request The related servlet request.
	 * @return The CarnetSessionContext instance.
	 */
	protected CarnetSessionContext getCarnetSessionContext(SlingHttpServletRequest request) {
		CarnetSessionContext ctx = (CarnetSessionContext) 
				request.getSession(true).getAttribute(CarnetConstants.CARNET_CONTEXT_ATTRIBUTE);
		if (ctx == null) {
			throw new GenericFormValidatorServletException(false, "Carnet session context not found.");
		} else {
			return ctx;
		}
	}
	
	protected BookingSessionContext getBookingSessionContext(SlingHttpServletRequest request) {
		BookingSessionContext ctx = (BookingSessionContext) 
				request.getSession(true).getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
		if (ctx == null) {
			throw new GenericFormValidatorServletException(false, "Booking session context not found (genericCarnetFormValidator).");
		} else {
			return ctx;
		}
	}
	
	/**
	 * Utility method to access the CarnetSessionContext instance to check if user isLogged.
	 * 
	 * <p>If the instance is not available, an exception is thrown.</p>
	 * 
	 * @param request The related servlet request.
	 * @return The CarnetSessionContext instance.
	 */
	protected CarnetSessionContext isUserLogged(SlingHttpServletRequest request) {
		CarnetSessionContext ctx = (CarnetSessionContext) 
				request.getSession(true).getAttribute(CarnetConstants.CARNET_CONTEXT_ATTRIBUTE);
		if (ctx == null) {
			throw new GenericFormValidatorServletException(false, "Carnet session context not found.");
		} else if(ctx.isLogged){
			return ctx;
		} else{
			throw new GenericFormValidatorServletException(false, "Carnet User not logged");
		}
	}
	
	/**
	 * Provide Carnet-specific unrecoverable error page.
	 */
	@Override
	protected String getUnrecoverableErrorRedirect(SlingHttpServletRequest request, 
			SlingHttpServletResponse response, GenericFormValidatorServletException exception) {
		return getUnrecoverableErrorRedirect(request);
	}
	
	private String getUnrecoverableErrorRedirect(SlingHttpServletRequest request) {
		return request.getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getCarnetLoginPage());
	}
	
	/**
	 * Provide custom management for Carnet exceptions.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		Map additionalParams = super.getAdditionalFailureData(request, exception);
		String url = getUnrecoverableErrorRedirect(request);
		additionalParams.put("redirect", url);
		return additionalParams;
	}
	
	/**
	 * Provide an empty implementation, should be never used as
	 * Carnet servlets should always be invoked with ajax requests
	 */
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		logger.warn("The emtpy saveDataIntoSession method was unexpectedly invoked.");
	}
	
	/**
	 * Abstract method to be implemented in subclasses in order to
	 * provide the configuration holder component instance.
	 * 
	 * @return The AlitaliaConfigurationHolder instance.
	 */
	protected abstract AlitaliaConfigurationHolder getConfiguration();
	
}
