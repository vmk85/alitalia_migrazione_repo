package com.alitalia.aem.consumer.carnet.servlet;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeRequest;
import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.carnet.CarnetConstants;
import com.alitalia.aem.consumer.carnet.CarnetSession;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.alitalia.aem.consumer.carnet.data.RecuperoPasswordData;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.RecuperaPinData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.alitalia.aem.web.component.carnet.delegate.CarnetDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "retrievepassword" }),
		@Property(name = "sling.servlet.methods", value = { "POST"}),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class RetrievePasswordCarnetServlet extends GenericCarnetFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile CarnetDelegate carnetDelegate;
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		Validator validator = new Validator();
		
		String mail = request.getParameter("carnetMail");
		String code = request.getParameter("carnetCode");
		
		validator.addDirectCondition("carnetMail",
				mail, CarnetConstants.MESSAGE_GENERIC_EMPTY_FIELD,
				"isNotEmpty");
		
		validator.addDirectCondition("carnetCode",
				code, CarnetConstants.MESSAGE_GENERIC_EMPTY_FIELD,
				"isNotEmpty");
		
		validator.addDirectCondition("carnetMail",
				mail, CarnetConstants.MESSAGE_GENERIC_INVALID_FIELD,
				"isEmail");
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
		
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
		
		Resource resource = request.getResource();
		String mail = request.getParameter("carnetMail");
		String code = request.getParameter("carnetCode");
		String marketCode = AlitaliaUtils.getRepositoryPathMarket(resource);
		String languageCode = AlitaliaUtils.getRepositoryPathLanguage(resource);
		
		CarnetRecoveryCodeRequest requestCode = new CarnetRecoveryCodeRequest();
		requestCode.setCarnetCode(code);
		requestCode.setEmail(mail);
		requestCode.setLanguageCode(languageCode);
		requestCode.setMarketCode(marketCode);
		
		CarnetRecoveryCodeResponse responseCode = carnetDelegate.recoveryCarnetCode(requestCode);
		
		goBackSuccessfully(request, response);
		
	}
	
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		logger.debug("[RetrievePasswordCarnetServlet] saveDataIntoSession.");
		logger.debug("[RetrievePasswordCarnetServlet] EXCEPTION RetrievePasswordCarnetServlet: " + e.getMessage());
		//TODO: gestire errori diversi dal generico?
		//String errorCarnetNotFound = configuration.getCustomerNotFoundRetrievePin();
		String errorCarnetNotFound = "CarnetCodeNotExist";
		logger.debug("[RetrievePasswordCarnetServlet] CARNET NOT FOUND ERROR: " + errorCarnetNotFound);
		if (e.getMessage().contains(errorCarnetNotFound)) {
			logger.debug("[RetrievePasswordCarnetServlet] ERROR DETECTED: carnet error not found: " + errorCarnetNotFound);
			RecuperoPasswordData recuperoPasswordData = getData(request, i18n.get(CarnetConstants.MESSAGE_CARNET_NOT_FOUND_ERROR_SERVICE));
			request.getSession().setAttribute(RecuperoPasswordData.NAME, recuperoPasswordData);
		}
		else {
			logger.debug("[RetrievePasswordCarnetServlet] ERROR DETECTED: generic error : " + errorCarnetNotFound);
			RecuperoPasswordData recuperoPasswordData = getData(request, CarnetConstants.MESSAGE_GENERIC_ERROR_SERVICE);
			request.getSession().setAttribute(RecuperoPasswordData.NAME, recuperoPasswordData);
		}
	}
	
	private RecuperoPasswordData getData(SlingHttpServletRequest request, String error) {
		RecuperoPasswordData recuperoPasswordData = new RecuperoPasswordData();
		recuperoPasswordData.setMail(request.getParameter("carnetMail"));
		recuperoPasswordData.setCode(request.getParameter("carnetCode"));
		recuperoPasswordData.setError(error);
		return recuperoPasswordData;
	}

}
