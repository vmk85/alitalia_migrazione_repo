package com.alitalia.aem.consumer.checkinrest.jobs.publisher;

import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadRequest;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import org.apache.sling.event.jobs.JobManager;

import java.util.Map;

/**
 * Created by ggadaleta on 06/02/2018.
 */
public interface JobPublisher {

    public Map<String, Object> createProperties(CheckinSessionContext ctx);

    public boolean publish(String sessionID, CheckinSessionContext ctx) throws IllegalArgumentException;

    public void setJobManager(JobManager jobManager);

}
