package com.alitalia.aem.consumer.global.i18n;

public class I18nKeyBooking {

	/*Messaggi errore cash and miles*/
	public static final String MESSAGE_ERROR_AUTHENTICATION_REQUIRED = "booking.cashAndMiles.authenticationRequired.error";
	public static final String MESSAGE_ERROR_TOO_MANY_MILES_USED = "booking.cashAndMiles.tooManyMilesUsed.error";
	public static final String MESSAGE_ERROR_FLIGHTS_CARRIER_NOT_AZ = "booking.cashAndMiles.notAZFlightsCarrier.error";
	public static final String MESSAGE_ERROR_FLIGHTS_BRAND_LIGHT = "booking.cashAndMiles.lightFlightsBrand.error";
	
	/*Messaggi di errore time table - non vengono mostrati a video*/
	public static final String MESSAGE_ERROR_FLIGHTNUMBER_NOT_VALID = "Numero volo non valido";
	public static final String MESSAGE_ERROR_FLIGHT_DATE_NOT_VALID = "Data volo non valida";
	public static final String MESSAGE_ERROR_APT_CODE_NOT_VALID = "Codice aeroporto non valido";
	
	/*Messaggi AirportDetalis*/
	public static final String CHECK_IN_TIME_DOM_FLIGHT_LABEL = "booking.airportDetail.checkInTimeDOM.label";
	public static final String CHECK_IN_TIME_INTZ_FLIGHT_LABEL = "booking.airportDetail.checkInTimeINTZ.label";
	public static final String CHECK_IN_TIME_INTC_FLIGHT_LABEL = "booking.airportDetail.checkInTimeINTC.label";
	public static final String MESSAGE_ERROR_AIRPORT_FROM_NOT_VALID = "booking.flightInfoList.airportFromNotValid.error";
	public static final String MESSAGE_ERROR_AIRPORT_TO_NOT_VALID = "booking.flightInfoList.airportToNotValid.error";
	public static final String MESSAGE_ERROR_DATE_NOT_VALID = "booking.flightInfoList.dateNotValid.error";
	public static final String MESSAGE_ERROR_DATE_PAST = "booking.flightInfoList.pastDate.error";
	
	/*Messaggi Stato voli*/
	public static final String FLIGHT_INFO_DELETED_FLIGHT = "booking.flightInfo.deletedFlight.label";
	public static final String FLIGHT_INFO_FLIGHT_NUMBER_NOT_VALID = "booking.flightInfo.flightNumbetNotValid.label";
	public static final String FLIGHT_INFO_FLIGHT_NOT_OPERATIVE = "booking.flightInfo.flightNotOperative.label";
	public static final String FLIGHT_INFO_FLIGHT_LANDED = "booking.flightInfo.flightLanded.label";
	public static final String FLIGHT_INFO_BEFORE_FLIGHT = "booking.flightInfo.beforeFlight.label";
	public static final String FLIGHT_INFO_IN_FLIGHT = "booking.flightInfo.inFlight.label";
	public static final String FLIGHT_INFO_ARRIVING = "booking.flightInfo.arriving.label";
	

}
