package com.alitalia.aem.consumer.booking.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingBreadcrumbController extends BookingSessionGenericController {

	@Inject
	AlitaliaConfigurationHolder configurationHolder;

	@Self
	private SlingHttpServletRequest request;

	private boolean carnet;
	
	
	@PostConstruct
	protected void initModel() {
		try {
			initBaseModel(request);
			
			if (ctx.phase != null) {
				carnet = ctx.isCarnetProcess;
			}
			
		} catch(Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}
	}
	
	public boolean isCarnet() {
		return carnet;
	}	
}
