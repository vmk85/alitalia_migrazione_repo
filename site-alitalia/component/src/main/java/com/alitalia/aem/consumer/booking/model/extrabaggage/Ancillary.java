package com.alitalia.aem.consumer.booking.model.extrabaggage;

import java.util.*;

/** 
 Sabre Request Model for Ancillary
*/
public class Ancillary
{
	private String code;
	public final String getcode()
	{
		return code;
	}
	public final void setcode(String value)
	{
		code = value;
	}
	private AncillarySpecialService ancillarySpecialService;
	public final AncillarySpecialService getancillarySpecialService()
	{
		return ancillarySpecialService;
	}
	public final void setancillarySpecialService(AncillarySpecialService value)
	{
		ancillarySpecialService = value;
	}
	private ArrayList<TravelPartOffer> travelPartOffers;
	public final ArrayList<TravelPartOffer> gettravelPartOffers()
	{
		return travelPartOffers;
	}
	public final void settravelPartOffers(ArrayList<TravelPartOffer> value)
	{
		travelPartOffers = value;
	}
	private AncillaryConfiguration ancillaryConfiguration;
	public final AncillaryConfiguration getancillaryConfiguration()
	{
		return ancillaryConfiguration;
	}
	public final void setancillaryConfiguration(AncillaryConfiguration value)
	{
		ancillaryConfiguration = value;
	}
	private ArrayList<BundleItem> bundleItems;
	public final ArrayList<BundleItem> getbundleItems()
	{
		return bundleItems;
	}
	public final void setbundleItems(ArrayList<BundleItem> value)
	{
		bundleItems = value;
	}
	private boolean refundable;
	public final boolean getrefundable()
	{
		return refundable;
	}
	public final void setrefundable(boolean value)
	{
		refundable = value;
	}

	//public string ancillaryCode { get; set; }
	//public int assignedQuantity { get; set; }
	//public SsrProperties ssrProperties { get; set; }
}