package com.alitalia.aem.consumer.checkin.analytics;

import java.util.Calendar;
import java.util.List;

public class CheckinAnalyticsInfo {
	public int step;
	
	/*Flight List (Step 1)*/
	public String travelType;
	
	public String Boapt;
	public String Bocity;
	public String Bocountry;
	public Calendar depDate;
	public String depBrand="";
	public String depFlightNumber="";
	public String depFareBasis="";
	
	public String Arapt;
	public String Arcity;
	public String Arcountry;
	public Calendar retDate;
	public String retBrand="";
	public String retFlightNumber="";
	public String retFareBasis="";
	
	public String Network;
	public Integer deltaBoAr;
	public String PNR;
	
	/*Passenger Data (Step 2)*/
	
	/*Ancillary (Step 3)*/
	public String emdTypeProposed;
	
	/*Payment  (Step 4)*/
	
	/*Ancillary Confirmation (Step 5)*/
	public String paymentType="";
	public String CCType="";
	public String tktNumber="";
	
	/*Boarding Pass (Step 6)*/
	
	/*Confirmation (Step 7)*/
	public String boardingPassType = "";
	
	/*Common*/
	public String flowType;
	public List<EMDInfo> ancillaryList;
	public Integer numAdults;
	public Integer numYoung;
	public Integer numChildren;
	public Integer numInfant;
	
}
