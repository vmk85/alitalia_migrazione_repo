package com.alitalia.aem.consumer.booking.model.extrabaggage;



/** 
 Sabre Request Model for AncillaryConfiguration
*/
public class AncillaryConfiguration
{
	private boolean allowQuantity;
	public final boolean getallowQuantity()
	{
		return allowQuantity;
	}
	public final void setallowQuantity(boolean value)
	{
		allowQuantity = value;
	}
	private int maxQuantityPerPax;
	public final int getmaxQuantityPerPax()
	{
		return maxQuantityPerPax;
	}
	public final void setmaxQuantityPerPax(int value)
	{
		maxQuantityPerPax = value;
	}
}