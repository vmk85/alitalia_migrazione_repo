package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbChannelsEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbAmericanStatesData;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbFrequentFlyerCarrierData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.MmbTaxData;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbFrequentFlyerCarriersResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.mmb.model.MmbFlightsGroup;
import com.alitalia.aem.consumer.mmb.render.MmbPassengerDataRender;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;
import com.alitalia.aem.consumer.mmb.render.MmbTaxDataListRender;
import com.alitalia.aem.consumer.mmb.render.MmbTaxDataRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.managemybooking.delegate.ManageMyBookingDelegate;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbManage extends MmbSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private ManageMyBookingDelegate manageMyBookingDelegate;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private List<MmbFlightsGroup> flightsGroupList;
	private MmbTaxDataListRender taxDataListRender;
	
	private List<MmbPassengerDataRender> passengerDataRenderList;
	private int numeroAdulti = 0;
	private int numeroBambini = 0;
	private int numeroNeonati = 0;
	
	private List<MmbFrequentFlyerCarrierData> frequentFlyerList;
	private List<CountryData> countryList;
	private List<MmbAmericanStatesData> americanStateList;
	
	private MmbApisTypeEnum apisTypeRequired;
	private String flightStatusUrl;
	private String email;
	private String tipoTelefono;
	private String prefissoInternazionale;
	private String telefono;
	private String myFlightsPageUrl;
	
	private boolean isPassengerDataEditEnabled;
	private boolean isPnrCreatedViaWeb;
	private boolean isInvoceRequestAvailable;
	private boolean isSecureFlightEsta;
	private boolean isLoggedUser;
	
	@PostConstruct
	protected void initModel() throws IOException {
		
		initBaseModel(request);
		this.isSecureFlightEsta = ctx.isSecureFlightEsta;
		
		flightsGroupList = MmbFlightsGroup.groupByRouteType(ctx.route.getFlights());
		
		MmbFlightData flightData = ctx.route.getFlights().get(0);
		passengerDataRenderList = new ArrayList<MmbPassengerDataRender>();
		for (MmbPassengerData passengerData : flightData.getPassengers()) {
			passengerDataRenderList.add(new MmbPassengerDataRender(passengerData));
		}

		taxDataListRender = new MmbTaxDataListRender();
		for (MmbTaxData taxData : ctx.route.getTaxes()) {
			if (!taxData.getCode().equals("TotalAmount")) {
				MmbTaxDataRender taxDataRender = new MmbTaxDataRender(taxData);
				taxDataRender.setPriceRender(
						new MmbPriceRender(taxData.getAmount(), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				taxDataListRender.getTaxDataRenderList().add(taxDataRender);	
			} else {
				taxDataListRender.setTotalAmountRender(
						new MmbPriceRender(taxData.getAmount(), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
			}
		}
		
		
		// Se il pnr è stato creato online e almeno un volo è disponibile per mmb, sarà possibile modificare i dati
		isPnrCreatedViaWeb = MmbChannelsEnum.WEB.equals(ctx.route.getChannel());
		
		isPassengerDataEditEnabled = false;
		if (isPnrCreatedViaWeb) {
			for (MmbFlightData mmbFlightData : ctx.route.getFlights()) {
				if (MmbFlightStatusEnum.AVAILABLE.equals(mmbFlightData.getStatus())) {
					isPassengerDataEditEnabled = true;
					break;
				}
			}
		}
		
		apisTypeRequired = ctx.route.getApisTypeRequired();
		
		// Url Flight Status
		String protocol = configuration.getHttpsEnabled() ? "https://" : "http://";
		flightStatusUrl = protocol + configuration.getExternalDomain() 
				+ request.getResourceResolver().map(
					AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
					+ configuration.getFlightInfoPage());
		
		
		/* BOX 2: DATI PASSEGGERI */
		for (MmbPassengerDataRender passengerDataRender : passengerDataRenderList) {
			MmbPassengerData passengerData = passengerDataRender.getPassengerData();
			if (passengerData.getType().equals(PassengerTypeEnum.ADULT)
					|| passengerData.getType().equals(PassengerTypeEnum.NORMAL)
					|| passengerData.getType().equals(PassengerTypeEnum.APPLICANT)) {
				numeroAdulti++;
			} else if (passengerData.getType().equals(PassengerTypeEnum.CHILD)) {
				numeroBambini++;
			} else if (passengerData.getType().equals(PassengerTypeEnum.INFANT)) {
				numeroNeonati++;
			}
		}
		

		/* BOX 3: CONTATTI */
		MmbPassengerData applicantPassenger = ctx.route.getApplicantPassenger();
		if (applicantPassenger != null) {
			email = applicantPassenger.getEmail1();
			if (email != null && !"".equals(email)) {
				email = email.substring(email.lastIndexOf("/") + 1);	
			} else {
				email = "";
			}
			
			telefono = applicantPassenger.getTelephone1();
			if (telefono != null && !"".equals(telefono)) {
				String[] telefonoSplit = telefono.split("/");
				tipoTelefono = telefonoSplit[0];
				prefissoInternazionale = telefonoSplit[1];
				telefono = telefonoSplit[2];
			} else {
				tipoTelefono = "";
				prefissoInternazionale = "";
				telefono = "";
			}
		
		} else {
			email = "";
			tipoTelefono = "";
			prefissoInternazionale = "";
			telefono = "";
		}

		
		/* BOX 4: RICHIEDI FATTURA */
		isInvoceRequestAvailable = false;
		if ("IT".equals(ctx.market.toUpperCase())
				&& ctx.route.isConfirmed()
				&& ctx.route.getCreateDateTime() != null) {
			LocalDate createDate = LocalDate.parse(
					ctx.route.getCreateDateTime().substring(0, 10), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			if (LocalDate.now().isBefore(createDate.plusDays(7))) {
				isInvoceRequestAvailable = true;
			}
		}
		
		
		/* CONTROLLO SE LOGGATO */
		MMCustomerProfileData customerProfileData = AlitaliaUtils.getAuthenticatedUser(request);
		isLoggedUser = customerProfileData != null;
		
		myFlightsPageUrl = protocol + configuration.getExternalDomain() 
				+ request.getResourceResolver().map(
					AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
					+ configuration.getMyFlightsPage());
		
		
		/* RECUPERO DATI STATICI */
		RetrieveFrequentFlayerRequest frequentFlayerRequest = new RetrieveFrequentFlayerRequest(tid, sid);
		frequentFlayerRequest.setLanguageCode(languageCode);
		frequentFlayerRequest.setMarket(marketCode);
		RetrieveMmbFrequentFlyerCarriersResponse frequentFlyerCarriersResponse = 
				manageMyBookingDelegate.getFrequentFlyers(frequentFlayerRequest);
		frequentFlyerList = frequentFlyerCarriersResponse.getFrequentFlyers();
		
		RetrieveCountriesRequest retrieveCountriesRequest = new RetrieveCountriesRequest(tid, sid);
		retrieveCountriesRequest.setLanguageCode(languageCode);
		retrieveCountriesRequest.setMarket(marketCode);
		RetrieveCountriesResponse retrieveCountriesResponse = 
				manageMyBookingDelegate.getCountries(retrieveCountriesRequest);
		countryList = retrieveCountriesResponse.getCountries();
		for (int i = 0; i < countryList.size(); i++) {
			CountryData country = countryList.get(i);
			String countryI18nKey = "countryData." + country.getCode();
			String countryI18nValue = i18n.get(countryI18nKey);
			if (!countryI18nKey.equals(countryI18nValue)) {
				country.setDescription(countryI18nValue);
			}
		}
		
		if (apisTypeRequired.equals(MmbApisTypeEnum.PLUS)) {
			RetrieveMmbAmericanStatesRequest retrieveMmbAmericanStatesRequest = 
					new RetrieveMmbAmericanStatesRequest(tid, sid);
			retrieveMmbAmericanStatesRequest.setLanguageCode(languageCode);
			retrieveMmbAmericanStatesRequest.setMarket(marketCode);
			RetrieveMmbAmericanStatesResponse retrieveMmbAmericanStatesResponse = 
					manageMyBookingDelegate.getAmericanStates(retrieveMmbAmericanStatesRequest);
			americanStateList = retrieveMmbAmericanStatesResponse.getAmericanStatesList();
			for (int i = 0; i < americanStateList.size(); i++) {
				MmbAmericanStatesData americanStateData = americanStateList.get(i);
				String countryI18nKey = "stateProvinceData.us." + americanStateData.getStateCode();
				String countryI18nValue = i18n.get(countryI18nKey);
				if (!countryI18nKey.equals(countryI18nValue)) {
					americanStateData.setState(countryI18nValue);
				}
			}
		}
		
	}

	public List<MmbFlightsGroup> getFlightsGroupList() {
		return flightsGroupList;
	}

	public List<MmbPassengerDataRender> getPassengerDataRenderList() {
		return passengerDataRenderList;
	}
	
	public MmbTaxDataListRender getTaxDataListRender() {
		return taxDataListRender;
	}

	public int getNumeroAdulti() {
		return numeroAdulti;
	}

	public int getNumeroBambini() {
		return numeroBambini;
	}

	public int getNumeroNeonati() {
		return numeroNeonati;
	}
	
	public List<MmbFrequentFlyerCarrierData> getFrequentFlyerList() {
		return frequentFlyerList;
	}
	
	public List<CountryData> getCountryList() {
		return countryList;
	}
	
	public List<MmbAmericanStatesData> getAmericanStateList() {
		return americanStateList;
	}
	
	public MmbApisTypeEnum getApisTypeRequired() {
		return apisTypeRequired;
	}

	public String getFlightStatusUrl() {
		return flightStatusUrl;
	}
	
	public String getEmail() {
		return email;
	}

	public String getTipoTelefono() {
		return tipoTelefono;
	}
	
	public String getPrefissoInternazionale() {
		return prefissoInternazionale;
	}

	public String getTelefono() {
		return telefono;
	}

	public String getMyFlightsPageUrl() {
		return myFlightsPageUrl;
	}

	public boolean isPassengerDataEditEnabled() {
		return isPassengerDataEditEnabled;
	}
	
	public boolean isPnrCreatedViaWeb() {
		return isPnrCreatedViaWeb;
	}

	public boolean isInvoceRequestAvailable() {
		return isInvoceRequestAvailable;
	}
	
	public boolean isSecureFlightEsta() {
		return isSecureFlightEsta;
	}
	
	public boolean isLoggedUser() {
		return isLoggedUser;
	}
}
