package com.alitalia.aem.consumer.validation.servlet;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

/**
 * This class can be extended to expose form submit handlers with support for
 * validation and generic exception handling.
 */
@SuppressWarnings("serial")
public abstract class GenericFormValidatorServlet extends SlingAllMethodsServlet {

	/**
	 * Class logger.
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * Entry point of the servlet (only POST is allowed for security reasons),
	 * look at the special request parameter and forward to the correct
	 * behavior.
	 */
	@Override
	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		long time_before = System.currentTimeMillis();
		String action = request.getParameter("_action");
		if ("validate".equals(action)) {
			performValidationBehavior(request, response);
		} else {
			performSubmitBehavior(request, response);
		}
		long time_after = System.currentTimeMillis();
		long execution_time = time_after - time_before;
		if (execution_time > 20000) {
			logger.info("------ ALERT EXECUTION TIME ------ Servlet execution time=" + execution_time);
		} else {
			logger.info("Servlet execution time=" + execution_time);
		}
	}

	/**
	 * 
	 */
	@Override
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		doPost(request, response);
	}

	/**
	 * When reached for validation, the servlet performs the validation of the
	 * received form (using the <code>validateForm</code> method) and returns
	 * the validation outcome as a JSON object in the response.
	 */
	protected void performValidationBehavior(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		ResultValidation resultValidation = validateForm(request);
		try {
			writeValidationResponse(request, response, resultValidation);
		} catch (Exception e) {
			logger.error("Error during validation.", e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * When reached for submit, the servlet performs the validation of the
	 * received form (using the <code>validateForm method</code> method) and, if
	 * valid, execute the requested behavior (using the
	 * <code>performSubmit</code> method).
	 */
	protected void performSubmitBehavior(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {

		// perform the validation again before trying to execute
		ResultValidation r = validateForm(request);

		boolean automaticallyWrapped = false;

		try {

			try {
				if (r.getResult()) {
					// validation passed, perform the submit
					performSubmit(request, response);
				} else {
					// unexpected invalid data
					throw new Exception("Unexpected invalid form on submit "
							+ "request: [" + r.toString() + "]");
				}
			} catch (GenericFormValidatorServletException e) {
				throw e;
			} catch (Exception e) {
				// generic catched exceptions are wrapped as a recoverable
				// custom exception
				logger.trace("Generic exception wrapped in a recoverable "
						+ "GenericFormValidatorServletException.", e);
				automaticallyWrapped = true;
				throw new GenericFormValidatorServletException(true,
						e.getMessage(), e);
			}

		} catch (GenericFormValidatorServletException e) {

			boolean isAjax = isAjaxRequest(request);

			logger.error("Exception caught, isRecoverable:" + e.isRecoverable() + ", isAjax: " + isAjax + ", "
					+ "response committed: " + response.isCommitted() + ". Exception details:", e);

			if (!response.isCommitted()) {
				if (isAjax) {

					// error detected in a ajax submit, send back error message
					// in JSON response
					try {
						TidyJSONWriter json = new TidyJSONWriter(
								response.getWriter());
						response.setContentType("application/json");
						json.object();
						json.key("isError").value(true);
						json.key("result").value("NOK");
						json.key("errorMessage").value(e.getMessage());
						if (!e.isRecoverable()) {
							// error is not recoverable, add redirecting
							// instruction
							json.key("redirect").value(
									getUnrecoverableErrorRedirect(request,
											response, e));
						}
						Map<String, String> additionalJsonFailureData = getAdditionalFailureData(request, e);
						for (Map.Entry<String, String> entry : additionalJsonFailureData.entrySet()) {
							json.key(entry.getKey()).value(entry.getValue());
						}
						json.endObject();

					} catch (JSONException jsonEx) {
						logger.error("A JSONException occured in exception "
								+ "management.", jsonEx);
					}

				} else {

					// called as a plain submit, always save submitted data
					// into HTTP session and send a redirect back to the
					// calling page, in order to show error details along
					// with the original form data
					try {
						Exception originalException = e;
						if (automaticallyWrapped) {
							originalException = (Exception) e.getCause();
						}
						saveDataIntoSession(request, originalException);
					} catch (Exception internalException) {
						logger.error("An internal exception was produced by "
								+ "saveDataIntoSession method.",
								internalException);
					}
					String url = getSelfPageUrl(request) + "?success=false";
					response.sendRedirect(url);

				}

			} else {
				logger.warn("Cannot manage the exception in the response, it "
						+ "was already committed.");
			}
		}
	}

	/**
	 * Base method used to write to the output stream the validation results.
	 * 
	 * This method could be overriden to completely modify the content of the
	 * validation response. To just add some additional return value, override
	 * the <code>writeAdditionalValidationResponseData</code> instead.
	 * 
	 * @param request
	 *            The request.
	 * @param response
	 *            The response.
	 * @param resultValidation
	 *            The validation result.
	 */
	protected void writeValidationResponse(SlingHttpServletRequest request,
			SlingHttpServletResponse response,
			ResultValidation resultValidation) throws Exception {

		response.setContentType("application/json");

		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);

		// set response message (JSON)
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		if (!resultValidation.getFields().isEmpty()) {
			json.key("fields");
			json.object();
			for (String k : resultValidation.getFields().keySet()) {
				json.key(k).value(i18n.get(resultValidation.getFields().get(k)));
			}
			json.endObject();
		}
		json.key("result").value(resultValidation.getResult());
		writeAdditionalValidationResponseData(request, response, json,
				resultValidation);
		json.endObject();

	}

	/**
	 * Hook method that can be used to provide additional data in the JSON
	 * output of the validation data.
	 * 
	 * @param request
	 *            The request.
	 * @param response
	 *            The response.
	 * @param json
	 *            The JSON writer.
	 * @param resultValidation
	 *            The validation result.
	 */
	protected void writeAdditionalValidationResponseData(
			SlingHttpServletRequest request, SlingHttpServletResponse response,
			TidyJSONWriter json, ResultValidation resultValidation)
					throws Exception {
	}

	/**
	 * Utility method to expose request form validation logic to external
	 * classes.
	 * 
	 * @param request
	 *            page request
	 * @return result of validation
	 */
	public final ResultValidation performValidateForm(
			SlingHttpServletRequest request) throws IOException {
		return validateForm(request);
	}

	/**
	 * Return the page on which the servlet selector was called (without
	 * selectors and with default html extension).
	 * 
	 * @param request
	 *            The request for which the error is produced.
	 * @return The URL.
	 */
	/*protected String getSelfPageUrl(SlingHttpServletRequest request) {
		// return AlitaliaUtils.findSiteResourceAbsoluteExternalUrl(request,
		// request.getResource());
		StringBuffer stringBuffer = request.getRequestURL();
		String url = stringBuffer.delete(stringBuffer.lastIndexOf("."),
				stringBuffer.length()).toString();
		return request.getResourceResolver().map(url) + ".html";
	}*/
	
	protected String getSelfPageUrl(SlingHttpServletRequest request) {
	    // return AlitaliaUtils.findSiteResourceAbsoluteExternalUrl(request,
	    // request.getResource());
	    String urlPath = "";
	    String urlMap = "";
	    Resource resource = request.getResource();
	    if(resource != null){
	      urlPath = resource.getPath();
	      urlMap = request.getResourceResolver().map(urlPath) + ".html";
	      if(logger.isDebugEnabled())
	    	  logger.debug("[SELF_PAGE_URL] urlMap = " + urlMap);
	    }
	  
	    
	    return urlMap;
	  }

	/**
	 * Allows to provide additional info to include in the JSON response
	 * after an exception.
	 * 
	 * <p>
	 * Default implementation returns an empy map.
	 * </p>
	 * 
	 * @param request
	 *            The request for which the error is produced.
	 * @param exception
	 *            The exception occurred.
	 * @return A map of key-value pairs to add.
	 */
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		return new HashMap<String, String>();
	}

	/**
	 * Provides the redirect address to send back in case of unrecoverable
	 * error on ajax submit requests.
	 * 
	 * <p>
	 * Default implementation simply delegates to <code>getPageUrl</code>.
	 * </p>
	 * 
	 * @param request
	 *            The request for which the error is produced.
	 * @param response
	 *            The related response.
	 * @param exception
	 *            The caught exception.
	 * @return The redirect value (as it must appears in response, unencoded).
	 */
	protected String getUnrecoverableErrorRedirect(
			SlingHttpServletRequest request, SlingHttpServletResponse response,
			GenericFormValidatorServletException exception) {
		return getSelfPageUrl(request);
	}
	
	/**
	 * Returns true if the request is an ajax one, based on the
	 * _isAjax request paramenter.
	 * 
	 * @param request The request parameter.
	 * @return True for requests marked as ajax, false otherwise.
	 */
	protected boolean isAjaxRequest(SlingHttpServletRequest request) {
		return "true".equals(request.getParameter("_isAjax"));
	}

	/**
	 * Form validation method.
	 * 
	 * @param request
	 *            page request
	 * @return result of validation
	 */
	protected abstract ResultValidation validateForm(
			SlingHttpServletRequest request) throws IOException;

	/**
	 * Form submit execution method.
	 * 
	 * @param request
	 *            page request
	 * @param response
	 *            page response
	 */
	protected abstract void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception;

	/**
	 * Method invoked when an error occurs in a non-ajax submit requests, in
	 * order to temporarly save the submitted data into a session object and
	 * allow display of the form values to the user.
	 * 
	 * @param request
	 *            The failed submit request.
	 * @param exception
	 *            The occurred exception.
	 */
	protected abstract void saveDataIntoSession(
			SlingHttpServletRequest request, Exception exception);

	/**
	 * Method that can be invoked for a successful non-ajax submit requests,
	 * in order to perform a redirect back to the page on which the servlet
	 * selector was called (obtained through <code>getSelfPageUrl</page>),
	 * with success query string parameters applied.
	 * 
	 * @param request
	 *            The request.
	 * @param response
	 *            The response.
	 */
	protected void goBackSuccessfully(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		String servletName = request.getRequestPathInfo().getExtension();
		String url = getSelfPageUrl(request) + "?success=true"
				+ ((null != servletName && !"".equals(servletName)) ? "&from="
						+ servletName : "");
		logger.debug("Redirect back to URL [" + url + "] with success");

		response.sendRedirect(url);
	}

}