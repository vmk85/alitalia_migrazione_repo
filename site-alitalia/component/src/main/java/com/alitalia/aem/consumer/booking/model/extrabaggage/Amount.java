package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Sabre Request Model for amount
*/
public class Amount
{
	private java.math.BigDecimal amount = new java.math.BigDecimal(0);
	public final java.math.BigDecimal getamount()
	{
		return amount;
	}
	public final void setamount(java.math.BigDecimal value)
	{
		amount = value;
	}
	private String currency;
	public final String getcurrency()
	{
		return currency;
	}
	public final void setcurrency(String value)
	{
		currency = value;
	}
}