package com.alitalia.aem.consumer.mmb.analytics;

import java.util.Calendar;
import java.util.List;

public class MmbAnalyticsInfo {
	public int step;
	
	public String ancillaryTypeProposed;
	public String ancillaryPage;
	public List<AncillaryInfo> ancillaryList;
	public String route;
	public String travelType;
	public Calendar depDate;
	public Calendar retDate;
	public String PNR;
	public String paymentType="";
	public String invoiceRequested;
	public String CCType="";
	public String tktNumber="";
	
	
}
