package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingflightdataconsumer" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class BookingFlightDataServlet extends SlingSafeMethodsServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private BookingSession bookingSession;
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		
		String baseUrl = request.getResource().getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
		
		boolean award = false;
			
		try {
			BookingSessionContext ctx = (BookingSessionContext) request.getSession(true).getAttribute(
					BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				throw new IllegalStateException("Booking session context not found.");
			}
			
			award = ctx.award;
			String solutionId = "";
			if(ctx.searchKind == BookingSearchKindEnum.MULTILEG && !ctx.readyToPassengersDataPhase) {
				solutionId = request.getParameter("solutionIdFirstSlice");
			}
			
			//INIZIO *** MODIFICA IP ***
			String ipAddress = "";
			ipAddress = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
			if (ipAddress != null && !ipAddress.isEmpty()) {
				ctx.ipAddress = ipAddress;
			}
			//FINE.
			
			bookingSession.confirmFlightSelections(ctx, solutionId, request);
			
			if (ctx.readyToPassengersDataPhase) {
				String successPage = configuration.getBookingPassengersDataPage();
				if (successPage != null && !"".equals(successPage)) {
					Resource resource = request.getResource();
					String protocol = "http://";
			    	if(configuration.getHttpsEnabled()){
			    		protocol = "https://";
			    	}
			    	String successUrl = protocol + configuration.getBookingExternalDomain() + 
							resource.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(resource, false)) + successPage;
					logger.debug("successUrl: " + successUrl);
					response.sendRedirect(successUrl);
				}
			}
			else if(ctx.searchKind == BookingSearchKindEnum.MULTILEG){
				String successPage = configuration.getBookingFlightSelectPage();
				if (successPage != null && !("").equals(successPage)) {
					String successUrl = AlitaliaUtils.findExternalRelativeUrlByPath(successPage, request.getResource());
					logger.debug("successUrl: " + successUrl);
					response.sendRedirect(response.encodeRedirectURL(successUrl));
				}
			}
			else{
				String failurePage;
				if (award) {
					failurePage = baseUrl + configuration.getBookingAwardFailurePage();
				}
				else {
					failurePage = baseUrl + configuration.getBookingFailurePage();
				}
				if (failurePage != null && !("").equals(failurePage)) {
//					String failureUrl = AlitaliaUtils.findExternalRelativeUrlByPath(failurePage, request.getResource());
					String failureUrl = failurePage;
					logger.debug("failureUrl: " + failureUrl);
					response.sendRedirect(response.encodeRedirectURL(failureUrl));
				}
			}
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			if (!response.isCommitted()) {
				String failurePage;
				if (award) {
					failurePage = configuration.getBookingAwardFailurePage();
				}
				else {
					failurePage = baseUrl + configuration.getBookingFailurePage();
				}
				if (failurePage != null && !"".equals(failurePage)) {
					response.sendRedirect(response.encodeRedirectURL(failurePage));
				} else {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
			}
		}
	}
	
}
