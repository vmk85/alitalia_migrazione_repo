package com.alitalia.aem.consumer.booking.model.extrabaggage;



/** 
 Sabre Request Model for AncillarySpecialService 
*/
public class AncillarySpecialService
{
	private String code;
	public final String getcode()
	{
		return code;
	}
	public final void setcode(String value)
	{
		code = value;
	}
	private AncillarySpecialServiceType type;
	public final AncillarySpecialServiceType gettype()
	{
		return type;
	}
	public final void settype(AncillarySpecialServiceType value)
	{
		type = value;
	}
	private RequiredProperties requiredProperties;
	public final RequiredProperties getrequiredProperties()
	{
		return requiredProperties;
	}
	public final void setrequiredProperties(RequiredProperties value)
	{
		requiredProperties = value;
	}
}