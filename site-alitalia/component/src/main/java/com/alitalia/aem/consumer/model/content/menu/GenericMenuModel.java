package com.alitalia.aem.consumer.model.content.menu;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;

import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;

public class GenericMenuModel extends GenericBaseModel {

    /**
     * Builds the menu elements to be used in the sightly model
     *
     * @param rootPage The root page from which you want to list elements
     * @param depth Integer indicating which depth the code must go to get
     * 		  the menu elements
     * @param request
     * @return The list of children of the root page
     */
    public MenuItem[] getPageList(Page rootPage, int depth,
    		SlingHttpServletRequest request, Logger logger) {
    	
    	Resource resource = request.getResource();
    	ResourceResolver resourceResolver = null;
    	if(resource != null)
    		resourceResolver = resource.getResourceResolver();
    	else
    		return null;
    	
    	if(resourceResolver == null)
    		return null;
    	
    	
    	
        // This object defines the whole menu
        MenuItem[] menu;
        final ArrayList<MenuItem> itemArrayList = new ArrayList<MenuItem>();

        // List children of the current page and exclude items with "hide in navigation" flag = true.
        final Iterator<Page> children = 
        		rootPage.listChildren(new PageFilter(request));
        
		// boolean isSecondLevel; FIXME: unused
		while (children.hasNext()) {

			final Page currentPage = children.next();
			final MenuItem currentItem;
			ValueMap prop = currentPage.getProperties();
			
			
			String hideInNav = prop.get("hideInNav", String.class);
			if (hideInNav == null || 
					!currentPage.getProperties("hideInNav").equals("true")) {
			
				// redirect
				String redirect = prop.get("redirectTarget", String.class);
				
				if (redirect != null) {
					String itemMapRedirect = resourceResolver.map(redirect);
					if(itemMapRedirect != null && !itemMapRedirect.contains(".html") && !itemMapRedirect.endsWith("/")){
						itemMapRedirect = itemMapRedirect + ".html";
					}
					//redirect = resourceResolver.map(redirect);
					if(!redirect.contains(".html") && !redirect.endsWith("/")){
						redirect = redirect + ".html";
					}
					currentItem = new MenuItem(currentPage.getTitle(), redirect,
							currentPage.getDescription(), MenuItemType.REDIRECT, itemMapRedirect);
				}
	
				// second lvl link
				else if (currentPage.getTemplate() != null &&
						currentPage.getTemplate().getPath().equals(
								AlitaliaConstants.TEMPLATE_SECOND_LEVEL)) {
					currentItem = new MenuItem(currentPage.getTitle(),
							 currentPage.getPath(), currentPage.getDescription(),
							 MenuItemType.SECOND_LEVEL, resourceResolver.map(currentPage.getPath())+ ".html");
				}
	
				// !second lvl link
				else {
					currentItem = new MenuItem(currentPage.getTitle(),
							currentPage.getPath(), currentPage.getDescription(),
							MenuItemType.LEAF, resourceResolver.map(currentPage.getPath())+".html");
					
				}
				logger.trace("currentItem: ", currentItem.toString());
				logger.trace("currentItem not resolved url: ", currentPage.getPath());
	
				if (depth > 0) {
					currentItem.setItemChildren(getPageList(currentPage, depth - 1,
							request, logger));
				}
				
				itemArrayList.add(currentItem);
				
			}
		}

        menu = new MenuItem[itemArrayList.size()];
        return itemArrayList.toArray(menu);
    }
}