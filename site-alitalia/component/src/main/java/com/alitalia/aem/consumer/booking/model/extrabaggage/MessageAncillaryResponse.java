package com.alitalia.aem.consumer.booking.model.extrabaggage;



public class MessageAncillaryResponse
{
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [JsonConverter(typeof(StringEnumConverter))] public AncillaryMessageLevel level {get;set;}
	private AncillaryMessageLevel level;
	public final AncillaryMessageLevel getlevel()
	{
		return level;
	}
	public final void setlevel(AncillaryMessageLevel value)
	{
		level = value;
	}
	private String code;
	public final String getcode()
	{
		return code;
	}
	public final void setcode(String value)
	{
		code = value;
	}
	private Object details;
	public final Object getdetails()
	{
		return details;
	}
	public final void setdetails(Object value)
	{
		details = value;
	}
}