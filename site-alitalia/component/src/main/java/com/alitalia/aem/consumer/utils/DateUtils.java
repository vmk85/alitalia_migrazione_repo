package com.alitalia.aem.consumer.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.alitalia.aem.consumer.global.AlitaliaConstants;

public class DateUtils {
	private static final float FROM_MILLIS_TO_DAYS = 1000 * 60 * 60 * 24;
	private static DateFormat dateFormat = new SimpleDateFormat(AlitaliaConstants.REQUEST_DATE_FORMAT);
	
	public static Calendar createCalendar(String day, String month, String year) throws ParseException {
		Calendar calendar  = Calendar.getInstance();
		calendar.setTime(dateFormat.parse(day + "/" + month + "/" + year));
		return calendar;
	}
	
	public static long daysBetween(Calendar day1, Calendar day2){
	    Calendar dayOne = (Calendar) day1.clone(),
	            dayTwo = (Calendar) day2.clone();

	    if (dayOne.get(Calendar.YEAR) == dayTwo.get(Calendar.YEAR)) {
	        return Math.abs(dayOne.get(Calendar.DAY_OF_YEAR) - dayTwo.get(Calendar.DAY_OF_YEAR));
	    } else {
	        if (dayTwo.get(Calendar.YEAR) > dayOne.get(Calendar.YEAR)) {
	            //swap them
	            Calendar temp = dayOne;
	            dayOne = dayTwo;
	            dayTwo = temp;
	        }
	        int extraDays = 0;

	        while (dayOne.get(Calendar.YEAR) > dayTwo.get(Calendar.YEAR)) {
	            dayOne.add(Calendar.YEAR, -1);
	            // getActualMaximum() important for leap years
	            extraDays += dayOne.getActualMaximum(Calendar.DAY_OF_YEAR);
	        }

	        return extraDays - dayTwo.get(Calendar.DAY_OF_YEAR) + dayOne.get(Calendar.DAY_OF_YEAR);
	    }
	}
	
	/**
	 * calcola il numero di weekend (sabato e domenica)
	 * presenti nell'intervallo fra due date (estremi esclusi)
	 * @param dep
	 * @param ret
	 * @return
	 */
	public static int computeWeekEndsNumber(Calendar dep, Calendar ret){
		int deltaBoAr = computeDiffDays(dep, ret);
		
		return (deltaBoAr / 7) 
				+ (((7 - computeRealDayOfWeek(dep.get(Calendar.DAY_OF_WEEK)) +1) + (deltaBoAr % 7)) >= 7 ? 1 : 0);
	}
	
	
	/**
	 * ottiene il numero del giorno della settimana in formato ITA
	 * 
	 * Es.
	 * Calendar.MONDAY = 1
	 * Calendar.SUNDAY = 7
	 * @param dayOfWeek
	 * @return
	 */
	public static int computeRealDayOfWeek(int dayOfWeek){
		return Math.floorMod(dayOfWeek -2, 7) +1;	
	}
	
	
	/**
	 * ottiene la differenza in giorni fra due date
	 * tenendo conto solo di giorno, mese e anno.
	 * 
	 * Es. dep: 14/01/2016 ret: 24/01/2016 return: 10
	 * @param dep
	 * @param ret
	 * @return
	 */
	public static int computeDiffDays(Calendar dep, Calendar ret){
		long diffInMillies = getDaysOnlyCalendar(ret).getTimeInMillis()
				- getDaysOnlyCalendar(dep).getTimeInMillis();
		return (int) (Math.ceil(diffInMillies / FROM_MILLIS_TO_DAYS));
	}
	
	private static Calendar getDaysOnlyCalendar(Calendar fullCalendar){
		Calendar daysOnlyCalendar = (Calendar) fullCalendar.clone();
		daysOnlyCalendar.set(Calendar.HOUR_OF_DAY, daysOnlyCalendar.getActualMinimum(Calendar.HOUR_OF_DAY));
		daysOnlyCalendar.set(Calendar.MINUTE, daysOnlyCalendar.getActualMinimum(Calendar.MINUTE));
		daysOnlyCalendar.set(Calendar.SECOND, daysOnlyCalendar.getActualMinimum(Calendar.SECOND));
		daysOnlyCalendar.set(Calendar.MILLISECOND, daysOnlyCalendar.getActualMinimum(Calendar.MILLISECOND));
		return daysOnlyCalendar;
	}
	
}
