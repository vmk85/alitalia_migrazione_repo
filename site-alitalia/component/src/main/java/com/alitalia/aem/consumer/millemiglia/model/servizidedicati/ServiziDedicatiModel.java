package com.alitalia.aem.consumer.millemiglia.model.servizidedicati;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.LabelUrl;
import com.day.cq.wcm.api.Page;

@Model(adaptables = { SlingHttpServletRequest.class })
public class ServiziDedicatiModel {

	static final private String BOX_SEMPLICE_RESOURCETYPE = "alitalia/components/content/box-servizi-dedicati-semplice";
	static final private String BOX_AQUISTA_MIGLIA_RESOURCETYPE = 
			"alitalia/components/content/box-servizi-dedicati-acquista-miglia";
	
	private Logger logger = LoggerFactory.getLogger(ServiziDedicatiModel.class);

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaCustomerProfileManager profileUser;
	
	private LabelUrl[] boxes;
	
	/**
	 * This method is used to retrieve title and description of parent page and all "brothers" page of current page
	 */
	@PostConstruct
	protected void initModel() {
		
		try {
			
			final Resource resource = request.getResource();
			ResourceResolver resourceResolver = resource.getResourceResolver();
			
			List<LabelUrl> boxList = new ArrayList<LabelUrl>();
			String tier;			  

			
			MMCustomerProfileData userdata= AlitaliaCustomerProfileManager.getAuthenticatedUserProfile(request);
			if(userdata==null){
			 tier="1";
			}else{
				Integer tierNumber= AlitaliaUtils.integerValuemmTierCode(userdata.getTierCode());
				tier = tierNumber.toString();
			}
			
			for (int i = 0; i < 4; i++) {
				String nomeRisorsa = (String) resource.getValueMap().get("tier" + tier + "box" + (i+1));
	            String pathNodoRisorsa = "";
				String resourceTypeRisorsa = "";

	            if (nomeRisorsa != null) {
	            	Page paginaRisorsa = resourceResolver.getResource(nomeRisorsa).adaptTo(Page.class);
		            Node nodoRisorsa = paginaRisorsa.getContentResource("box").adaptTo(Node.class);
		            pathNodoRisorsa = nodoRisorsa.getPath().toString();
		            resourceTypeRisorsa = (String) resourceResolver.getResource(pathNodoRisorsa).getValueMap()
							.get("sling:resourceType");
				}
				if (resourceTypeRisorsa.equals(BOX_SEMPLICE_RESOURCETYPE)) {
					boxList.add(new LabelUrl(pathNodoRisorsa, BOX_SEMPLICE_RESOURCETYPE));
				} else if (resourceTypeRisorsa.equals(BOX_AQUISTA_MIGLIA_RESOURCETYPE)) {
					boxList.add(new LabelUrl(pathNodoRisorsa, BOX_AQUISTA_MIGLIA_RESOURCETYPE));
				} 
			}
			if(boxList.size()>0){
			boxes = (LabelUrl[]) boxList.toArray(new LabelUrl[boxList.size()]);
			}
			
		} catch(Exception e) {
        	logger.error(e.toString());
        }
	}

	/**
	 * Returns title and url of editorial main two columns at the same level of current page
	 * 
	 * @return The boxes
	 */
	public LabelUrl[] getBoxes() {
		return boxes;
	}
}
