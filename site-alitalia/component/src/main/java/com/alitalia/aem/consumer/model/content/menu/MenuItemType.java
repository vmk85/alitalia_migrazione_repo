package com.alitalia.aem.consumer.model.content.menu;

public enum MenuItemType {

	SECOND_LEVEL("second_level"), LEAF("leaf"), REDIRECT("redirect");
	private final String value;

	MenuItemType(String v) {
		value = v;
	}

	public String value() {
		return this.value;
	}

}
