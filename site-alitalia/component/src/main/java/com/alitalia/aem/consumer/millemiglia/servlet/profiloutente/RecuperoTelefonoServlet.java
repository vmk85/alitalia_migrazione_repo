package com.alitalia.aem.consumer.millemiglia.servlet.profiloutente;

import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.gigya.socialize.GSRequest;
import com.gigya.socialize.GSResponse;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import javax.inject.Inject;
import javax.servlet.Servlet;
import java.io.IOException;

@Component
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = { "recuperoTelefonoServlet" }),
        @Property(name = "sling.servlet.methods", value = { "POST" }),
        @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) ,
        @Property(name = "sling.servlet.extensions", value = { "json" })
 })
@SuppressWarnings("serial")
public class RecuperoTelefonoServlet extends GenericFormValidatorServlet {

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile AlitaliaCustomerProfileManager customerProfileManager;

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile ConsumerLoginDelegate consumerLoginDelegate;

    @Inject
    private AlitaliaConfigurationHolder alitaliaConfigurationHolder;

    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
        logger.debug("[RecuperoTelefonoServlet] performSubmit");

        try
        {
            if(alitaliaConfigurationHolder==null)
                alitaliaConfigurationHolder = new AlitaliaConfigurationHolder();

            String apiKeyAppo = alitaliaConfigurationHolder.getMaApiKey(); // "3_FDF25EQ_XpOyo4xiZloa6QmF7QXYsb5r-rJaZj5ocvqW8B4bdCucFfqNdpaQPIec";
            String secretKeyAppo = alitaliaConfigurationHolder.getMaSecretKey();// "7XDjmCjYblwLmP+ejS79/RhqJPGapRbl";
            String userKeyAppo = alitaliaConfigurationHolder.getMaUserKey(); // "AI9W/RQHPi9r";


            if(userKeyAppo==null || userKeyAppo.length()==0) // provv
                userKeyAppo = "AI9W/RQHPi9r";
            String userKeyPar = request.getParameter("userKey");
            if(userKeyPar!=null && userKeyPar.length()>0)
                userKeyAppo = userKeyPar;  // presente il parametro sul form, prendo questo valore invece di quello di default

            if(apiKeyAppo==null || apiKeyAppo.length()==0) // provv
                apiKeyAppo = "3_6cxzgvUvDl5IM7jwQTtPhYYEjtUalJBu9cBG2wo_QIeAnwADfbItRNXs63nMqvvr";
            String apiKeyPar = request.getParameter("apiKey");
            if(apiKeyPar!=null && apiKeyPar.length()>0)
                apiKeyAppo = apiKeyPar;  // presente il parametro sul form, prendo questo valore invece di quello di default

            if(secretKeyAppo==null || secretKeyAppo.length()==0) // provv
                secretKeyAppo = "7XDjmCjYblwLmP+ejS79/RhqJPGapRbl";
            String secretKeyPar = request.getParameter("secretKey");
            if(secretKeyPar!=null && secretKeyPar.length()>0)
                secretKeyAppo = secretKeyPar;  // presente il parametro sul form, prendo questo valore invece di quello di default

            final String apiKey = apiKeyAppo;
            final String secretKey = secretKeyAppo;
            final String userKey = userKeyAppo;

            //String apiMethod = "https://accounts.eu1.gigya.com/accounts.deleteAccount";
            String email = request.getParameter("email");
            if(email==null || email.length()==0)
                email = "mforcinito@alpenite.com";
            String apiMethod = "accounts.search";

            boolean https = false;
            String suHttps = request.getParameter("suHttps");
            if(suHttps!=null && suHttps.length()>0 && suHttps.equals("true"))
                https = true;


            GSRequest req = new GSRequest(apiKey, secretKey, apiMethod, null, https, userKey);

            String query = "SELECT data.phone1Number FROM accounts WHERE profile.email = \""+email+"\" ";
            //req.setParam("query","SELECT profile.phones.number FROM accounts WHERE profile.email = '"+email+"' ");
            String queryPassata = request.getParameter("query");
            if(queryPassata!=null && queryPassata.length()>0 && queryPassata.toUpperCase().contains("SELECT")) {
                query = queryPassata;
                logger.debug("[RecuperoTelefonoServlet] queryPassata: " + queryPassata);
            }
            req.setParam("query",query);

            String uid = request.getParameter("uid");
            if(uid!=null && uid.length()>0) {
                req.setParam("UID", uid);
                logger.debug("[RecuperoTelefonoServlet] uid passato: " + uid);
            }

            String apiDomain = request.getParameter("apiDomain");
            if(apiDomain!=null && apiDomain.length()>0){
                if(apiDomain.equals("no"))
                    logger.debug("[RecuperoTelefonoServlet] non imposto eu1.gigya.com ");
                else
                    req.setAPIDomain(apiDomain);
            }
            else
                req.setAPIDomain("eu1.gigya.com");


            GSResponse res = req.send();


            String secondoTentativo = request.getParameter("secondoTentativo");
            if(res.getErrorCode()!=0 && secondoTentativo!=null && secondoTentativo.equalsIgnoreCase("s")){
                query = "SELECT * FROM accounts WHERE profile.email = \""+email+"\" ";
                //req.setParam("query","SELECT profile.phones.number FROM accounts WHERE profile.email = '"+email+"' ");

                req.setParam("query",query);

                res = req.send();

            }

            if(res.getErrorCode()==0)
            {
                logger.debug("[RecuperoTelefonoServlet] Recupero numero effettuato");
                logger.debug("[RecuperoTelefonoServlet] " + res.getResponseText());
        //        request.getSession().setAttribute("tel", );

                try{
                    if(res.hasData()){
                        logger.debug("[RecuperoTelefonoServlet] data.phone1Number: " + res.getData().getString("data.phone1Number"));
                    }
                }
                catch(Exception e){
                    logger.debug("[RecuperoTelefonoServlet] data.phone1Number - Exception: " + e );
                }

                String soloJson = request.getParameter("soloJson");
                if(soloJson!=null && soloJson.length()>0 && soloJson.equalsIgnoreCase("S")){
                    response.setContentType("text/plain");
                    response.getWriter().print(res.getData());
                }
                else {
                    response.setContentType("text/plain");
                    response.getWriter().append("[RecuperoTelefonoServlet] ResponseText: " + res.getResponseText());
                    response.getWriter().append("[RecuperoTelefonoServlet] ok - Telefono per MyAlitalia recuperato correttamente");
                }


            }
            else
            {
                logger.debug("[RecuperoTelefonoServlet] Errore: "+ res.getErrorCode() + " - " + res.getErrorMessage());
                logger.debug("[RecuperoTelefonoServlet] Dettagli Errore: "+ res.getErrorDetails());
                response.setContentType("text/plain");
                response.getWriter().append("[RecuperoTelefonoServlet] Errore: "+ res.getErrorCode() + " - " + res.getErrorMessage());

                try {
                    logger.debug("[RecuperoTelefonoServlet] Dettagli ResponseText: " + res.getResponseText());
                }catch (Exception e){
                    logger.debug("[RecuperoTelefonoServlet] ResponseText - Exception: " + e);
                }

            }



         //   req.setParam("UID", request.getParameter("id"));
         //   req.setAPIDomain("eu1.gigya.com");
         //   GSResponse resp = req.send();

             /*
            //Define the API-Key and Secret key (the keys can be obtained from your site setup page on Gigya's website).
            final String apiKey = "AI9W/RQHPi9r";
            final String secretKey = "7XDjmCjYblwLmP+ejS79/RhqJPGapRbl";

            String apiMethod = "accounts.deleteAccount";
            com.gigya.socialize.GSRequest req = new com.gigya.socialize.GSRequest(apiKey, secretKey, apiMethod);

            req.setParam("UID",request.getParameter("id"));

            com.gigya.socialize.GSResponse resp = req.send();

            if(resp.getErrorCode()==0)
            {
                //System.out.printIn("success!");
            }
            else
            {
                //System.out.printIn("Uh-oh, we got the following error: " + response.getLog());
            }*/

        } catch(Exception e) {
            logger.error("Error RecuperoTelefono per SMS non riuscito", e);
            throw new Exception();
        }

    }

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        logger.debug("[RecuperoTelefonoServlet] validateForm");
        Validator validator = new Validator();
        ResultValidation resultValidation = new ResultValidation();
        resultValidation.setResult(true);
        return resultValidation;
    }

    @Override
    protected void saveDataIntoSession(SlingHttpServletRequest request, Exception exception) {
        logger.debug("[RecuperoTelefonoServlet] saveDataIntoSession");

    }


}



