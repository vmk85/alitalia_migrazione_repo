package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.InvoiceData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "invoicesubmit" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class InvoiceServlet extends GenericFormValidatorServlet {
	
	private static final int DIM_FORM = 17;
	private static final String KEYVALUE_SEPARATOR = ":";
	private static final String ELEMENT_SEPARATOR = System.getProperty("line.separator");
	
	// Static Property For Mails
	private static final String COMPONENT_NAME = "invoice";
	private static final String PARSYS_NAME = "page-component";
	private static final String MAIL_FROM_PROPERTY_NAME = "mailFrom";
	private static final String MAIL_TO_PROPERTY_NAME = "mailTo";
	private static final String MAIL_SUBJECT_PROPERTY_NAME = "subject";
	private static final String MAIL_FROM_DEFAULT = "fatturazione.postvendita@alitalia.it";
	private static final String MAIL_TO_DEFAULT = "datiperfatture@alitalia.it";
	private static final String MAIL_SUBJECT_DEFAULT = "Richiesta Fatturazione";

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegate staticDataDelegate;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BusinessLoginService businessLoginService;
	
	private Locale locale;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[InvoiceServlet] validateForm");

		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			return validator.validate();

		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Error during parameters validation", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, 
			SlingHttpServletResponse response) throws IOException {
		logger.debug("[InvoiceServlet] performSubmit");
		boolean mailResult = sendMail(request);
		logger.debug("Mail Sended = " + mailResult);
		if (mailResult) {
			goBackSuccessfully(request, response);
		}
		else {
			throw new IOException("Service Error");
		}		
	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[InvoiceServlet] saveDataIntoSession");
		
		// create cookiesData object and save it into session
		InvoiceData invoiceData = new InvoiceData();
		invoiceData.setAddress(request.getParameter("address"));
		invoiceData.setCap(request.getParameter("cap"));
		invoiceData.setCity(request.getParameter("city"));
		invoiceData.setCode(request.getParameter("code"));
		invoiceData.setCompany(request.getParameter("company-ragsoc"));
		invoiceData.setEmail(request.getParameter("email"));
		invoiceData.setFare(request.getParameter("fare"));
		invoiceData.setJourney(request.getParameter("journey"));
		invoiceData.setName(request.getParameter("name"));
		invoiceData.setNation(request.getParameter("nation"));
		invoiceData.setPhone(request.getParameter("phone"));
		invoiceData.setPrice(request.getParameter("price"));
		invoiceData.setProvince(request.getParameter("province"));
		invoiceData.setSurname(request.getParameter("surname"));
		invoiceData.setTaxes(request.getParameter("taxes"));
		invoiceData.setType(request.getParameter("type"));
		invoiceData.setTicketnumber(request.getParameter("ticketnumber"));
		
		// go back to form with error
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		invoiceData.setError(i18n.get(I18nKeySpecialPage.ERROR_SERVICE));
		request.getSession().setAttribute(InvoiceData.NAME, invoiceData);
	}

	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {
		logger.debug("[ InvoiceServlet] setValidatorParameters");
		
		String type = request.getParameter("type");
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String code = request.getParameter("code");
		String address = request.getParameter("address");
		String cap = request.getParameter("cap");
		String city = request.getParameter("city");
		String nation = request.getParameter("nation");
		String province = request.getParameter("province");
		String company = request.getParameter("company-ragsoc");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String ticketnumber = request.getParameter("ticketnumber");
		String price = request.getParameter("price");
		String fare = request.getParameter("fare");
		String taxes = request.getParameter("taxes");
		
		// add validate conditions
		
		// type
		validator.addDirectConditionMessagePattern("type", type,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.INVOICE_TYPE_ERROR_EMPTY, "isNotEmpty");
		validator.setAllowedValuesMessagePattern("type", type,
				new String[]{"pg", "pf"}, I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_TYPE_ERROR_NOT_VALID);
		
		// name
		validator.addDirectConditionMessagePattern("name", name,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.INVOICE_NAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("name", name,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_NAME_ERROR_NOT_VALID,
				"isAlphabeticWithAccentAndSpaces");
		
		// surname
		validator.addDirectConditionMessagePattern("surname", surname,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.INVOICE_SURNAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("surname", surname,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_SURNAME_ERROR_NOT_VALID,
				"isAlphabeticWithAccentAndSpacesExtended");
		
		// code
		validator.addDirectConditionMessagePattern("code", code,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.INVOICE_CODE_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("code", code,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_CODE_ERROR_NOT_VALID,
				"isInvoiceCode");
		
		// address
		validator.addDirectConditionMessagePattern("address", address,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.INVOICE_ADDRESS_ERROR_EMPTY, "isNotEmpty");		
		// cap
		validator.addDirectConditionMessagePattern("cap", cap,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.INVOICE_CAP_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("cap", cap,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_CAP_ERROR_NOT_VALID,
				"isNumberWithSpace");
		
		// city
		validator.addDirectConditionMessagePattern("city", city,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.INVOICE_CITY_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("city", city,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_CITY_ERROR_NOT_VALID,
				"isAlphabeticWithAccentAndSpaces");
		
		// nation
		validator.addDirectConditionMessagePattern("nation", nation,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.INVOICE_NAME_ERROR_EMPTY, "isNotEmpty");
		ArrayList<String> countries = getCountries(request);
		validator.setAllowedValuesMessagePattern("nation", nation,
				countries.toArray(), I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_NATION_ERROR_NOT_VALID);
		
		// province
		validator.addDirectConditionMessagePattern("province", province,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.INVOICE_PROVINCE_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("province", province,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_PROVINCE_ERROR_NOT_VALID,
				"isAlphabeticWithAccentAndSpaces");
		
		// email
		validator.addDirectConditionMessagePattern("email", email,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.INVOICE_EMAIL_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("email", email,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_EMAIL_ERROR_NOT_VALID, "isEmail");
		
		// company
		validator.addDirectConditionMessagePattern("company-ragsoc", company,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.INVOICE_COMPANY_ERROR_EMPTY, "isNotEmpty");
				
		// phone
		validator.addDirectConditionMessagePattern("phone", phone,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.INVOICE_PHONE_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("phone", phone,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_PHONE_ERROR_NOT_VALID,
				"isNumberExtended");
		
		// ticket number
		validator.addDirectConditionMessagePattern("ticketnumber", ticketnumber,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.INVOICE_TICKETNUMBER_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("ticketnumber", ticketnumber,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_TICKETNUMBER_ERROR_NOT_VALID,
				"isNumberWithSpace");
		
		// price
		validator.addDirectConditionMessagePattern("price", price,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_PRICE_ERROR_NOT_VALID, "isAmount");
		
		// fare
		validator.addDirectConditionMessagePattern("fare", fare,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_FARE_ERROR_NOT_VALID, "isAmount");
		
		// taxes
		validator.addDirectConditionMessagePattern("taxes", taxes,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.INVOICE_TAXES_ERROR_NOT_VALID, "isAmount");
		
		return validator;

	}
	
	/*
	 * get countries
	 * 
	 */
	private ArrayList<String> getCountries(SlingHttpServletRequest request) {
		logger.debug("[ InvoiceServlet] getCountries");
		
		// set locale
		locale = AlitaliaUtils.getRepositoryPathLocale(request.getResource());
		
		// create request countries
		RetrieveCountriesRequest countriesServiceRequest = 
				new RetrieveCountriesRequest(IDFactory.getTid(),
						IDFactory.getSid(request));			
		countriesServiceRequest.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()));
		countriesServiceRequest.setMarket(locale.getCountry());
		
		// retrieve countries
		RetrieveCountriesResponse countriesServiceResponse = 
				staticDataDelegate.retrieveCountries(countriesServiceRequest);

		// return countries code
		ArrayList<String> countries = new ArrayList<String>();
		for (CountryData countryData : countriesServiceResponse.getCountries()) {
			countries.add(countryData.getCode());
		}
		
		return countries;
		
	}
	
	/*
	 * sendMail
	 * 
	 */
	private boolean sendMail(SlingHttpServletRequest request) {
		logger.debug("[ InvoiceServlet] sendMail");
		
		// current page
		Page currentPage = AlitaliaUtils.getPage(request.getResource());
		
		// email from
	    String mailFrom = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_FROM_PROPERTY_NAME);
	    if (mailFrom == null) {
	    	mailFrom = MAIL_FROM_DEFAULT;
	    }
	    
	    // email to
	    String mailTo = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_TO_PROPERTY_NAME);
	    if (mailTo == null) {
	    	mailTo = MAIL_TO_DEFAULT;
	    }
	    
	    // email subject
	    String subject = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_SUBJECT_PROPERTY_NAME);
	    if (subject == null) {
	    	subject = MAIL_SUBJECT_DEFAULT;
	    }
		
	    // email body
 		String messageText = generateBodyMail(request);
	    
 		logger.debug("[ MillemigliaKidsServlet] sendMail -- Email data: "
				+ "[From: {" + mailFrom + "}] - [To: {" + mailTo + "}] - "
				+ "[Subject: {" + subject + "}] - [Body: {" + messageText + "}]");
 		
 		// create email message object
		EmailMessage emailMessage = new EmailMessage();
		emailMessage.setFrom(mailFrom);
		emailMessage.setTo(mailTo);
		emailMessage.setSubject(subject);
		emailMessage.setMessageText(messageText);
		emailMessage.setIsBodyHtml(false);
		emailMessage.setDeliveryNotification(DeliveryNotificationOptionsEnum.NONE);
		emailMessage.setPriority(MailPriorityEnum.NORMAL);

		// create AgencySendMailRequest
		AgencySendMailRequest mailRequest = new AgencySendMailRequest();
		mailRequest.setTid(IDFactory.getTid());
		mailRequest.setSid(IDFactory.getSid(request));
		mailRequest.setEmailMessage(emailMessage);
		
		// get AgencySendMailResponse
		AgencySendMailResponse mailResponse =
				businessLoginService.sendMail(mailRequest);
		
		// return status
		return mailResponse.isSendMailSuccessful();

	}
	
	/*
	 * generateBodyMail
	 * 
	 */
	private String generateBodyMail(SlingHttpServletRequest request) {
		logger.debug("[ InvoiceServlet] generateBodyMail");
		
		String[][] data = new String[DIM_FORM][2];
		
		// creating Keys
		int k;
		for (k = 0; k < DIM_FORM; k++) {
			String name = "KeyField" + Integer.toString(k);
			data[k][0] = request.getParameter(name);
			logger.debug("key : " + data[k][0]);
		}
		
		// getting Values
		String type = request.getParameter("type");
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String code = request.getParameter("code");
		String company_ragsoc = request.getParameter("company-ragsoc");
		String address = request.getParameter("address");
		String cap = request.getParameter("cap");
		String city = request.getParameter("city");
		String nation = request.getParameter("nation");
		String province = request.getParameter("province");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String ticketnumber = request.getParameter("ticketnumber");
		String journey = request.getParameter("journey");
		String price = request.getParameter("price");
		String fare = request.getParameter("fare");
		String taxes = request.getParameter("taxes");
		
		// setting Values
		data[0][1] = (null != type) ? type : "";
		data[1][1] = (null != name) ? name : "";
		data[2][1] = (null != surname) ? surname : "";
		data[3][1] = (null != code) ? code : "";
		data[4][1] = (null != company_ragsoc) ? company_ragsoc : "";
		data[5][1] = (null != address) ? address : "";
		data[6][1] = (null != cap) ? cap : "";
		data[7][1] = (null != city) ? city : "";
		data[8][1] = (null != nation) ? nation : "";
		data[9][1] = (null != province) ? province : "";
		data[10][1] = (null != email) ? email : "";
		data[11][1] = (null != phone) ? phone : "";
		data[12][1] = (null != ticketnumber) ? ticketnumber : "";
		data[13][1] = (null != journey) ? journey : "";
		data[14][1] = (null != price) ? price : "";
		data[15][1] = (null != fare) ? fare : "";
		data[16][1] = (null != taxes) ? taxes : "";
		
		//  creating email message and return it
		String emailText = "";
		for (k = 0; k < DIM_FORM; k++) {
			emailText = emailText + data[k][0] + KEYVALUE_SEPARATOR + data[k][1];
			if (k < DIM_FORM - 1) {
				emailText = emailText + ELEMENT_SEPARATOR;
			}

		}
		return emailText;
		
	}
	
	/*
	 * getComponentProperty
	 * 
	 */
	private String getComponentProperty(Page ancestor, String parsysName,
			String compName, String propName) {
		logger.debug("[ InvoiceServlet] getComponentProperty");
		
		Resource pageResource = ancestor.getContentResource();	
		String prop = null;
		try {
			prop = pageResource.getChild(parsysName).getChild(compName).
					getValueMap().get(propName, String.class);
		} catch(Exception e) {}
		
		return prop;

	}
	
}