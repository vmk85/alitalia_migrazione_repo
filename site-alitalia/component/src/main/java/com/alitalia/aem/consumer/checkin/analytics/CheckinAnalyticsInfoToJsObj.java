package com.alitalia.aem.consumer.checkin.analytics;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.List;

import com.alitalia.aem.consumer.booking.analytics.FlightSegmentInfo;
import com.alitalia.aem.consumer.booking.analytics.filler.ToJsObj;


public class CheckinAnalyticsInfoToJsObj extends ToJsObj{
	
	private static final String HYPHEN_FLIGHT_DATE_FORMAT = "yyyy-MM-dd";
	private static final String HYPHEN_EU_FLIGHT_DATE_FORMAT = "dd-MM-yy";
	private static final String EU_FLIGHT_DATE_FORMAT = "dd/MM/yyyy";
	private static final String EN_FLIGHT_DATE_FORMAT = "MM/dd/yyyy";
	private static final String TIME_FORMAT_HH_MM_SS = "kk:mm:ss";
	
	private DecimalFormat format;
	private SimpleDateFormat dateFormat_eu;
	private SimpleDateFormat dateFormat_en;
	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateFormat_eu_hyphen;
	private SimpleDateFormat timeFormat;
	
	

	public CheckinAnalyticsInfoToJsObj() {
		initFormats();
	}
	
	protected void initFormats() {
		format = new DecimalFormat("#0.00");
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setCurrencySymbol(""); 
		format.setDecimalFormatSymbols(otherSymbols);
		format.setGroupingUsed(false);
		format.setPositivePrefix("");
		format.setNegativePrefix("");
		
		dateFormat_eu = new SimpleDateFormat(EU_FLIGHT_DATE_FORMAT);
		dateFormat_eu.setLenient(false);
		
		dateFormat_en = new SimpleDateFormat(EN_FLIGHT_DATE_FORMAT);
		dateFormat_en.setLenient(false);
		
		dateFormat = new SimpleDateFormat(HYPHEN_FLIGHT_DATE_FORMAT);
		dateFormat.setLenient(false);
		dateFormat_eu_hyphen = new SimpleDateFormat(HYPHEN_EU_FLIGHT_DATE_FORMAT);
		dateFormat_eu_hyphen.setLenient(false);
		
		timeFormat = new SimpleDateFormat(TIME_FORMAT_HH_MM_SS);
		timeFormat.setLenient(false);
	}
	
	public String toJSObj(CheckinAnalyticsInfo info, boolean trailingSemicolon) {
		String js = "{";
		
		/*Other info*/
		switch(info.step){
		case 1:
			js = addJsObjElem(js, "CIntravelType", info.travelType, true);
			js = addJsObjElem(js, "CInBoapt", info.Boapt, true);
			js = addJsObjElem(js, "CInBocountry", info.Bocountry, true);
			js = addJsObjElem(js, "CIndepDate", formatIfNotNull(info.depDate, dateFormat), true);
			js = addJsObjElem(js, "CIndepBrand", info.depBrand, true);
			js = addJsObjElem(js, "CIndepFlightNumber", info.depFlightNumber, true);
			js = addJsObjElem(js, "CInArapt", info.Arapt, true);
			js = addJsObjElem(js, "CInArcountry", info.Arcountry, true);
			js = addJsObjElem(js, "CInretDate", formatIfNotNull(info.retDate, dateFormat), true);
			js = addJsObjElem(js, "CInretFlightNumber", info.retFlightNumber, true);
			js = addJsObjElem(js, "CInretBrand", info.retBrand, true);
			js = addJsObjElem(js, "CIndeltaBoAr", info.deltaBoAr, true);
			js = addJsObjElem(js, "CInPNR", info.PNR, false);
			
			break;
		case 2:
			/*No data to track*/
			break;
		case 3:
			js = addJsObjElem(js, "CInFlow", info.flowType, true);
			js = addJsObjElem(js, "EMDTypeProposed", info.emdTypeProposed, true);
			js = addJsArrayObjElem(js, "EMD", convertAncillaryList(info.ancillaryList), false);
			break;
		case 4:
			js = addJsObjElem(js, "CInFlow", info.flowType, false);
			break;
		case 5:
			js = addJsObjElem(js, "CInFlow", info.flowType, true);
			js = addJsArrayObjElem(js, "EMD", convertAncillaryList(info.ancillaryList), true);
			js = addJsObjElem(js, "CInPaymentType", info.paymentType, true);
			js = addJsObjElem(js, "CInCCType", info.CCType, true);
			js = addJsObjElem(js, "PNR", info.PNR, true);
			js = addJsObjElem(js, "TktNumber", info.tktNumber, true);
			js = addJsObjElem(js, "MMBDepartureDate", formatIfNotNull(info.depDate, dateFormat), true);
			js = addJsObjElem(js, "MMBReturnDate", formatIfNotNull(info.retDate, dateFormat), false);
			break;
		case 6:
			js = addJsObjElem(js, "CInFlow", info.flowType, false);
			break;
		case 7:
			js = addJsObjElem(js, "CInFlow", info.flowType, true);
			js = addJsObjElem(js, "CInAdultsPNR", info.numAdults, true);
			js = addJsObjElem(js, "CInYoungPNR", info.numYoung, true);
			js = addJsObjElem(js, "CinChildrenPNR", info.numChildren, true);
			js = addJsObjElem(js, "CinInfantPNR", info.numInfant, true);
			js = addJsObjElem(js, "CInBoardinPass", info.boardingPassType, false);
		}
		
		js += (trailingSemicolon) ? "};" : "}";
		return js;
	}

	private String convertAncillaryList(List<EMDInfo> ancillaryList) {
		String js = "[";
		for(EMDInfo ancillary : ancillaryList){
			js += "{";
			if(ancillary.ancQuantity == null && ancillary.ancRevenue == null){
				js = addJsObjElem(js, "Name", ancillary.ancName, false);
			}
			else{
				js = addJsObjElem(js, "Name", ancillary.ancName, true);
				js = addJsObjElem(js, "Quantity", ancillary.ancQuantity, true);
				js = addJsObjElem(js, "Revenue", formatIfNotNull(ancillary.ancRevenue, format), false);
			}
			js += "},";
		}
		/*Remove last ","*/
		if(js.length() > 1){
			js = js.substring(0,js.length()-1);
		}
		js += "]";
		return js;
	}

}
