package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPassengerNoteEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryMealDetailData;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup.AncillariesGroupType;
import com.alitalia.aem.consumer.mmb.model.MmbFlightsGroup;
import com.alitalia.aem.consumer.mmb.render.MmbAncillaryMealRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbAncillaryMeal extends MmbSessionAncillaryGenericController {

	@Self
	private SlingHttpServletRequest request;
	private boolean showMmbBoxMeal=true;
	private List<MmbAncillaryMealRender> mealSelections;
	private List<MmbFlightsGroup> flightsGroupList;

	private int mealsNum;
	
	@PostConstruct
	protected void initModel() throws IOException {
		initBaseModel(request);
		
		this.mealSelections = new LinkedList<MmbAncillaryMealRender>();
		this.flightsGroupList = MmbFlightsGroup.groupByRouteType(ctx.route.getFlights());
		MmbFlightData firstRoute=ctx.route.getFlights().get(0);
		Date firstRouteDepartureTime=firstRoute.getDepartureDateTime().getTime();
	
		Date today = Calendar.getInstance().getTime();
		final int MILLI_TO_HOUR = 1000 * 60 * 60;
		long diffHours= (firstRouteDepartureTime.getTime()-today.getTime())/MILLI_TO_HOUR;
		if(diffHours<=48){
			showMmbBoxMeal=false;
		}

		// group by passenger
		List<MmbAncillariesGroup> groups = MmbAncillariesGroup.createGroups(AncillariesGroupType.BY_PASSENGER, 
				ctx.route, ctx.cart.getPassengers(), ctx.cart.getAncillaries(), MmbAncillaryTypeEnum.MEAL);
		for (MmbAncillariesGroup group : groups) {
			
			// skip passengers without any meal ancillaries
			if (group.getAncillaries().size() == 0) {
				continue;
			}
			
			// prepare passenger meal render
			MmbAncillaryMealRender mealRender = new MmbAncillaryMealRender(group);
			mealRender.setFullName(group.getPassengerData().getName() + " " + group.getPassengerData().getLastName());
			if (MmbPassengerNoteEnum.INFANT_ACCOMPANIST.equals(group.findMmbPassengerDataForFlight(0).getNote())) {
				mealRender.setHasInfant(true);
			}
			if (MmbPassengerNoteEnum.CHILD.equals(group.findMmbPassengerDataForFlight(0).getNote())) {
				mealRender.setChild(true);
			}
			mealRender.setDisplayInFeedback(false);
			mealRender.setIdentifier("(disabled)");
			mealRender.setCurrentValue("");
			mealRender.setCurrentValueLabel("");
			this.mealSelections.add(mealRender);
			
			List<MmbAncillaryData> issuedOrAvailableAncillaries = 
					group.getAncillariesByStatuses(new MmbAncillaryStatusEnum[] {
							MmbAncillaryStatusEnum.AVAILABLE,
							MmbAncillaryStatusEnum.ISSUED});
			MmbAncillaryData addedIssuedOrPreviouslyIssuedAncillary = 
					group.getAncillaryInStatuses(new MmbAncillaryStatusEnum[] {
							MmbAncillaryStatusEnum.TO_BE_ISSUED,
							MmbAncillaryStatusEnum.ISSUED,
							MmbAncillaryStatusEnum.PREVIOUSLY_ISSUED});
			
			if (addedIssuedOrPreviouslyIssuedAncillary != null) {
				// preselect current value from existing ancillaries
				MmbAncillaryMealDetailData detail = (MmbAncillaryMealDetailData) 
						addedIssuedOrPreviouslyIssuedAncillary.getAncillaryDetail();
				mealRender.setCurrentValue(detail.getMeal());
				for (MealData availableMeal : ctx.mealTypes) {
					if (availableMeal.getCode().equals(detail.getMeal())) {
						mealRender.setCurrentValueLabel(availableMeal.getDescription());
						break;
					}
				}
				if (MmbAncillaryStatusEnum.ISSUED.equals(addedIssuedOrPreviouslyIssuedAncillary.getAncillaryStatus())) {
					// issued ancillaries (i.e. meals selected in this MMB session) are displayed in feedback step
					mealRender.setDisplayInFeedback(true);
				}
			}
			
			if (issuedOrAvailableAncillaries.size() > 0) {
				// ancillaries are available or issued, thus enable modification (via add/update) and set identifier
				mealRender.setIdentifier(
						getAncillariesSelectionIdentifier(issuedOrAvailableAncillaries));
				mealRender.setAllowModify(true);
			}
			
		}
		
		mealsNum = mealSelections.size();
	}
	
	public List<MmbFlightsGroup> getFlightsGroupList() {
		return flightsGroupList;
	}
	
	public List<MealData> getMealTypes() {
		return ctx.mealTypes;
	}
	
	public List<MmbAncillaryMealRender> getMealSelections() {
		return mealSelections;
	}
	
	public int getMealsNum(){
		return mealsNum;
	}
	public boolean showMMBoxMeal(){
		return showMmbBoxMeal;
	}
}
