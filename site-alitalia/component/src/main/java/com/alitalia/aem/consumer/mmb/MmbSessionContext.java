package com.alitalia.aem.consumer.mmb;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.home.InsurancePolicyData;
import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.common.data.home.PaymentTypeItemData;
import com.alitalia.aem.common.data.home.SeatMapData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCartData;
import com.day.cq.i18n.I18n;

/**
 * The <code>MMBSessionContext</code> class contains the state of a manage
 * my booking session, in order to be stored in the proper way (e.g. in HTTP session).
 * 
 * </p>It is meant to be initialized by the <code>MMBSession</code>
 * component service, and then provided to it as requested for MMB
 * process management methods.</p>
 * 
 * <p>The values managed in the context shouldn't be modified
 * externally to the <code>MMBSession</code> component service.</p>
 */
public class MmbSessionContext {
	
	/* init data */
	
	public I18n i18n;
	
	public String serviceClient;
	
	public String machineName;
	
	public String sid;
	
	public String callerIp;
	
	public String market;
	
	public String site;

	public Locale locale;
	
	public NumberFormat currentNumberFormat;
	
	public String currencyCode;
	
	public String currencySymbol;
	
	public String tripId;
	
	public String passengerName;
	
	public String passengerSurname;
	
	public boolean isFromBooking;
	
	public String defaultCardType;
	
	public String defaultCardNumber;
	
	public Integer defaultCardExpireMonth;
	
	public Integer defaultCardExpireYear;
	
	public String defaultCardName;
	
	public String defaultCardSurname;
	
	public boolean mmbCartInitialized;
	
	
	/* trip data */
	
	public String pnr;

	public String eTicket;
	
	public MmbRouteData route;
	
	public RouteTypeEnum type;
	
	public AreaValueEnum area;
	
	public Map<RouteTypeEnum, List<MmbFlightData>> flightsByRouteType;
	
	public boolean isSecureFlightEsta;
	
	
	/* catalog and cart data */

	public MmbAncillaryCartData cart;

	public List<MmbAncillaryErrorData> cartErrors;
	
	public BigDecimal cartTotalAmount;
	
	public BigDecimal cartTotalAncillariesAmount;
	
	
	/* insurance data */
	
	public InsurancePolicyData insurancePolicy;
	
	public boolean insuranceAddedToCart;
	
	
	/* static data */
	
	public List<MealData> mealTypes;
	
	public List<PaymentTypeItemData> creditCards;

	
	/* seat maps data */
	
	public Map<MmbFlightData, SeatMapData> seatMapsByFlight;
	
	
	/* payment data */
	
	public String confirmationMailAddress;
	
	public boolean ancillaryPaymentSuccess;
	
	public boolean insurancePaymentSuccess;
	
	public String paymentErrorMessage;
	
	public String paymentCreditCardNumberHide;
	
	public String paymentCreditCardType;
	
	public boolean isBus;


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MmbSessionContext [i18n=").append(i18n).append(", serviceClient=").append(serviceClient)
				.append(", machineName=").append(machineName).append(", sid=").append(sid).append(", callerIp=")
				.append(callerIp).append(", market=").append(market).append(", site=").append(site).append(", locale=")
				.append(locale).append(", currencyCode=").append(currencyCode).append(", currencySymbol=")
				.append(currencySymbol).append(", tripId=").append(tripId).append(", passengerName=")
				.append(passengerName).append(", passengerSurname=").append(passengerSurname)
				.append(", isFromBooking=").append(isFromBooking).append(", defaultCardType=").append(defaultCardType)
				.append(", defaultCardNumber=").append(defaultCardNumber == null ? "null" : "[not-null-value]")
				.append(", defaultCardExpireMonth=").append(defaultCardExpireMonth == null ? "null" : "[not-null-value]")
				.append(", defaultCardExpireYear=").append(defaultCardExpireYear == null ? "null" : "[not-null-value]")
				.append(", defaultCardName=").append(defaultCardName).append(", defaultCardSurname=").append(defaultCardSurname).append(", pnr=")
				.append(pnr).append(", eTicket=").append(eTicket).append(", route=").append(route)
				.append(", type=").append(type).append(", area=")
				.append(area).append(", flightsByRouteType=").append(flightsByRouteType).append(", cart=").append(cart)
				.append(", cartErrors=").append(cartErrors).append(", cartTotalAmount=").append(cartTotalAmount)
				.append(", cartTotalAncillariesAmount=").append(cartTotalAncillariesAmount)
				.append(", insurancePolicy=").append(insurancePolicy).append(", insuranceAddedToCart=")
				.append(insuranceAddedToCart).append(", mealTypes=").append(mealTypes).append(", seatMapsByFlight=")
				.append(seatMapsByFlight).append(", ancillaryPaymentSuccess=").append(ancillaryPaymentSuccess)
				.append(", paymentErrorMessage=").append(paymentErrorMessage)
				.append(", mmbCartInitialized=").append(mmbCartInitialized)
				.append(", insurancePaymentSuccess=").append(insurancePaymentSuccess)
				.append(", paymentCreditCardNumberHide=").append(paymentCreditCardNumberHide)
				.append(", paymentCreditCardType=").append(paymentCreditCardType).append("]");
		return builder.toString();
	}
	
}