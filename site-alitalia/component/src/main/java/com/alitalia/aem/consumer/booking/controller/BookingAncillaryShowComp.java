package com.alitalia.aem.consumer.booking.controller;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/*
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
*/

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.PassengerApisSecureFlightInfoData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PassengerSecureFlightInfoData;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.AncillaryCodeAndPath;
import com.alitalia.aem.consumer.booking.model.AncillaryCompDataHolder;
import com.alitalia.aem.consumer.booking.model.AncillaryPageDoubleContainer;
import com.alitalia.aem.consumer.booking.model.AncillaryPassenger;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.model.Passenger;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.render.AvailableFlightsSelectionRender;
import com.alitalia.aem.consumer.booking.render.DirectFlightDataRender;
import com.alitalia.aem.consumer.booking.render.SelectedFlightInfoRender;


@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingAncillaryShowComp extends BookingSessionGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private BookingSession bookingSession;
	
	
	
	
	private List<AncillaryCodeAndPath> ancillaryComponentList;
	private List<AncillaryPageDoubleContainer> ancillaryContainer;
	private Boolean insuranceEnabled;
	private Boolean postoEnabled;
	private Boolean pastoEnabled;
	

	@PostConstruct
	protected void initModel() throws Exception {
	
		try {
			
			
			initBaseModel(request);
			if (ctx == null){
				return;
			}
			if (ctx != null) {
				
				insuranceEnabled = ctx.insuranceEnabled;
				pastoEnabled = ctx.mealsEnabled;
				
				postoEnabled = false;
				
				if (ctx.seatMapsByFlight != null && !ctx.seatMapsByFlight.entrySet().isEmpty()) {

					postoEnabled = true;
				}
				
				Map<String, AncillaryCompDataHolder> ancillaryData = new HashMap<String, AncillaryCompDataHolder>();
				
				ancillaryComponentList = new ArrayList<AncillaryCodeAndPath>();
				ancillaryContainer = new ArrayList<AncillaryPageDoubleContainer>();
				/*
				ancillaryComponentList.add("alitalia/components/content/booking-posto-new");
				ancillaryComponentList.add("alitalia/components/content/booking-extrabag");
				ancillaryComponentList.add("alitalia/components/content/booking-pasto-new");
				ancillaryComponentList.add("alitalia/components/content/booking-assicurazioni-new");
				*/
				//AncillaryCodeAndPath peppe = new AncillaryCodeAndPath();
				
				//ancillaryComponentList.add(peppe);
				
				if (postoEnabled==true){
					ancillaryComponentList.add(new AncillaryCodeAndPath("POST",
							"alitalia/components/content/booking-posto-new", "posto"));
				}
				
				ancillaryComponentList.add(new AncillaryCodeAndPath("BAG",
						"alitalia/components/content/booking-extrabag", "extrabag") );
				
				if (pastoEnabled==true){
					ancillaryComponentList.add(new AncillaryCodeAndPath("PAST",
							"alitalia/components/content/booking-pasto-new", "pasto"));
				}
				
				if (insuranceEnabled==true){
					ancillaryComponentList.add(new AncillaryCodeAndPath("ASS",
							"alitalia/components/content/booking-assicurazioni-new", "assicurazione"));
				}
				
				
				AncillaryPageDoubleContainer componentContainer=null ;
				Integer rowCount = 0;
				for (AncillaryCodeAndPath component : ancillaryComponentList) {
					
					AncillaryCompDataHolder componentData = new  AncillaryCompDataHolder();
					
					componentData.setAncillaryCode(component.getAncillaryCode());
					
					if (componentContainer == null){
						rowCount ++;
					}
					
					componentData.setVerticalIndex(rowCount);
					
					
					if (componentContainer == null){

						componentContainer=new AncillaryPageDoubleContainer();
						componentContainer.setFirstComponent(component.getAncillaryPath());
						componentContainer.setFirstComponentPath(component.getAncillaryCodePath());
						componentData.setHorizontalIndex(1);
						componentContainer.setVerticalIndex(rowCount);
						if (rowCount==1){
							componentData.setComponentClass("PCAD");
						}else{
							componentData.setComponentClass("SCAD");
						}
						
						
					}else{
						componentContainer.setSecondComponent(component.getAncillaryPath());
						componentContainer.setSecondComponentPath(component.getAncillaryCodePath());
						ancillaryContainer.add(componentContainer);
						componentContainer = null;
						componentData.setHorizontalIndex(2);
						
						if (rowCount==1){
							componentData.setComponentClass("PCAS");
						}else{
							componentData.setComponentClass("SCAS");
						}
					}
					
					ancillaryData.put(component.getAncillaryCode(),componentData);
				}
				
				if (componentContainer != null){
					ancillaryContainer.add(componentContainer);
				}
				
				request.getSession(true).setAttribute("AncillaryComponentData", ancillaryData);
				
			}
			
			
			
		} catch (Exception e) {
			logger.error("unexpected error: ", e);
			throw e;
		}
	}
	
	public SlingHttpServletRequest getRequest() {
		return request;
	}

	public BookingSession getBookingSession() {
		return bookingSession;
	}

	
	public List<AncillaryPageDoubleContainer> getAncillaryContainer() {
		return ancillaryContainer;
	}
	
	
	public boolean getInsuranceEnabled() {
		return insuranceEnabled;
	}
	
	public boolean getPostoEnabled() {
		return postoEnabled;
	}
	
	public boolean getPastoEnabled() {
		return pastoEnabled;
	}

	
}
