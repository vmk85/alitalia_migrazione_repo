package com.alitalia.aem.consumer.servlet.login;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.SocialLoginRequest;
import com.alitalia.aem.common.messages.home.SocialLoginResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;

/**
 * Servlet exposing a REST-based service to verify the validity of the
 * provided social UID credentials.
 * 
 * <p>The expected input includes the following request parameters:
 * <ul>
 * <li>gigyaId: the Gigyia UID.</li> 
 * <li>gigyaSignature: the Gigyia signature.</li>
 * <li>gigyaProvider: the name of the provider to display.</li>
 * <li>gigyaUserFirstName: the user first name to display.</li> 
 * <li>gigyaUserLastName: the user last name to display.</li>
 * <li>gigyaUserThumbnailURL: the user thumbnail URL.</li> 
 * </ul>
 * </p>
 * 
 * <p>The output is a JSON object with the following attributes:
 * <ul>
 * <li>successful: true for valid social credentials, false otherwise (e.g. invalid uid or signature).</li>
 * <li>mmCode: the associated MilleMiglia user code, if existing.</li>
 * <li>mmPin: the associated MilleMiglia user PIN, if existing.</li>
 * <li>errorCode: code related to the failed attempt, if applicable.</li> 
 * <li>errorDescription: a description related to the failed attempt, if applicable.</li>
 * <li>gigyaId: the Gigyia UID.</li> 
 * <li>gigyaSignature: the Gigyia signature.</li> 
 * <li>gigyaProvider: the name of the provider to display.</li>
 * <li>gigyaUserFirstName: the user first name to display.</li> 
 * <li>gigyaUserLastName: the user last name to display.</li> 
 * <li>gigyaUserThumbnailURL: the user thumbnail URL.</li> 
 * <li>setSocialAccountRedirectUrl: URL of the page to let user connect their MM account to the social account.</li> 
 * </ul>
 * </p>
 * 
 * <p>If a MilleMiglia account is not found for the social account, this servlet 
 * will also save the Gigya data in the <code>pendingSocialLoginData</code>
 * session object and return the URL of the user account link page.</p>
 */

@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checksociallogin" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "setsocialaccount.page")
})
@SuppressWarnings("serial")
public class CheckSocialLoginServlet extends SlingAllMethodsServlet {
	

	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	public static final String PENDING_SOCIAL_LOGIN_DATA_ATTRIBUTE = "pendingSocialLoginData";
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * Reference to the service to remote consumer login service.
	 */
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_UNARY)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;
	
	private ComponentContext componentContext;
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		
		logger.debug("Social credentials check requested.");
		
		String gigyaId = request.getParameter("gigyaId");
		String gigyaSignature = request.getParameter("gigyaSignature");
		String gigyaProvider = request.getParameter("gigyaProvider");
		String gigyaUserFirstName = request.getParameter("gigyaUserFirstName");
		String gigyaUserLastName = request.getParameter("gigyaUserLastName");
		String gigyaUserThumbnailURL = request.getParameter("gigyaUserThumbnailURL");
		
		boolean successful = false;
		String mmCode = ""; 
		String mmPin = ""; 
		String mmSsoToken = "";
		String setSocialAccountRedirectUrl = "";
		String errorCode = "";
		String errorDescription = "";
		
		if (consumerLoginDelegate == null) {
			logger.error("Cannot check social login credentials, delegate not available.");
			successful = false;
			errorCode = "unknown";
			errorDescription = "Login delegate currently unavailable";
		
		} else {
			
			try {
				String tid = IDFactory.getTid();
				String sid = IDFactory.getSid(request);
				
				// clear current pending social login data
				request.getSession().removeAttribute(PENDING_SOCIAL_LOGIN_DATA_ATTRIBUTE);
				
				SocialLoginRequest loginRequest = new SocialLoginRequest(tid, sid);
				loginRequest.setGigyaId(gigyaId);
				loginRequest.setGigyaSignature(gigyaSignature);
				SocialLoginResponse loginResponse = consumerLoginDelegate.socialLogin(loginRequest);
				
				successful = loginResponse.isLoginSuccessful();
				if (!successful) {
					errorCode = "unknown";
					errorDescription = "Invalid uid or signature";
				} else {
					mmCode = loginResponse.getUserCode();
					mmPin = loginResponse.getPin();
					
					logger.debug("SSOToken retrieved: "+loginResponse.getSsoToken());
					if (loginResponse.getSsoToken() != null && !loginResponse.getSsoToken().isEmpty()) {
						Cookie ssoCookie = new Cookie("ibeopentoken", loginResponse.getSsoToken());
						int ssoCookieExpiry = configuration.getSsoCookieTimeout();
						String ssoCookieDomain = configuration.getSsoCookieDomain();
						
						ssoCookie.setPath("/");
						ssoCookie.setSecure(configuration.getHttpsEnabled());
						if (ssoCookieExpiry != -1)
							ssoCookie.setMaxAge(ssoCookieExpiry);
						if (!"".equals(ssoCookieDomain))
							ssoCookie.setDomain(ssoCookieDomain);
						response.addCookie(ssoCookie);
					} else {
						logger.error("SSOToken is null or empty");
					}
					
					if (mmCode == null || "".equals(mmCode)) {
						// instruct to redirect to the social account link page, temporarly
						// saving in session data about the social account
						setSocialAccountRedirectUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), true) 
								+ (String) componentContext.getProperties().get("setsocialaccount.page");
						Map<String, String> pendingSocialLoginData = new HashMap<String, String>();
						pendingSocialLoginData.put("gigyaId", gigyaId);
						pendingSocialLoginData.put("gigyaSignature", gigyaSignature);
						pendingSocialLoginData.put("gigyaProvider", gigyaProvider);
						pendingSocialLoginData.put("gigyaUserFirstName", gigyaUserFirstName);
						pendingSocialLoginData.put("gigyaUserLastName", gigyaUserLastName);
						pendingSocialLoginData.put("gigyaUserThumbnailURL", gigyaUserThumbnailURL);
						request.getSession().setAttribute(PENDING_SOCIAL_LOGIN_DATA_ATTRIBUTE, pendingSocialLoginData);
					}
				}
				
			} catch (Exception e) {
				logger.error("Error trying to check social login.", e);
				successful = false;
				errorCode = "unknown";
				errorDescription = "Unexpected error";
				
			}
			
		}
		
		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		try {
			json.object();
			json.key("successful");
			json.value(successful);
			json.key("errorCode");
			json.value(errorCode);
			json.key("errorDescription");
			json.value(errorDescription);
			json.key("mmCode");
			json.value(mmCode);
			json.key("mmPin");
			json.value(mmPin);
			json.key("setSocialAccountRedirectUrl");
			json.value(setSocialAccountRedirectUrl);
			json.key("gigyaId");
			json.value(gigyaId);
			json.key("gigyaSignature");
			json.value(gigyaSignature);
			json.key("gigyaProvider");
			json.value(gigyaProvider);
			json.key("gigyaUserFirstName");
			json.value(gigyaUserFirstName);
			json.key("gigyaUserLastName");
			json.value(gigyaUserLastName);
			json.key("gigyaUserThumbnailURL");
			json.value(gigyaUserThumbnailURL);
			json.endObject();
		} catch (Exception e) {
			logger.error("Unexected error generating JSON response.", e);
		}
	}
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
}
