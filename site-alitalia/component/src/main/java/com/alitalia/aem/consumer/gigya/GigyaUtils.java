package com.alitalia.aem.consumer.gigya;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Flight;
import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.data.home.MyFlightsDataModel;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaUtils;
import com.alitalia.aem.consumer.myalitalia.model.MyFlightsPnr;
import com.alitalia.aem.consumer.myalitalia.model.MyFlightsTicket;
import com.gigya.socialize.GSResponse;
import com.google.gson.Gson;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class GigyaUtils {

    public static HashMap<String, String> mapJsonObjGigya(String jsonObject) throws JSONException {

        HashMap<String, String> map = new HashMap<>();
        JSONObject jsonObject1 = new JSONObject(jsonObject);
        map.put("data", jsonObject1.toString());
        return map;

    }

    public static HashMap<String, String> mapJsonDynamicObjGigya(String jsonObject) throws JSONException {

        HashMap<String, String> map = new HashMap<>();
        JSONObject jsonObject1 = new JSONObject(jsonObject);
        for (Iterator<String> it = jsonObject1.keys(); it.hasNext(); ) {
            String k = it.next();
            map.put(k, jsonObject1.getString(k));
        }
        return map;

    }


    public static MACustomer gigyaLogin(SlingHttpServletRequest request, GigyaSession gigyaSession) throws Exception {

        GSResponse gsResponse = null;

        /** Prendo l'utente gigya dalla sessione */
        MACustomer maCustomer = MyAlitaliaUtils.getMaCustomerBySession(request);

        /** Se l'utente non esiste, effettuo il login su gigya */
        if (maCustomer == null) {

            /** Prendo il token di registrazione dalla request ( Utilizzato per il social login ) */
            String authToken = request.getParameter("authToken");

            /** Prendo email e psw dalla request ( Utilizzato con login normale ) */
            String email = request.getParameter("email");
            String psw = request.getParameter("password");

            /** Controllo che si stia effettuando un login tramite credenziali */
            if (authToken == null || (email != null && psw != null)) {

                /** Chiamata al servizio per effettuare il login */
                gsResponse = gigyaSession.login(email, psw);

            } else {

                /** Se si sta effettuando il login tramite social, effettuo una finalize registration ( Unico modo per reperire i dati dell'utente in assenza di email e password) */
                gsResponse = gigyaSession.finalizeRegistration(authToken);

            }

            Gson gson = new Gson();

            /** Converto la response da Gigya per salvare l'utente in sessione ed effettuare il login su CRM  */
            maCustomer = gson.fromJson(gsResponse.getResponseText(), MACustomer.class);

            if (maCustomer.getErrorCode() == 0) {

                /** Se non è il primo accesso, salvo la variabile su gigya ( se valorizzata a true, mostro odale otp ma solo dal secondo accesso in poi ) */
                if (!maCustomer.getData().isFirstAccess()) {
                    try {

                        /** Inizializzo le variabili per caricare i dati su gigya */
                        org.json.JSONObject obj2 = new org.json.JSONObject();
                        obj2.put("isFirstAccess", true);

                        /** Salvo su gigya il parametro per l'otp */
                        gigyaSession.setAccount(maCustomer.getUID(), GigyaUtils.mapJsonObjGigya(obj2.toString()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return maCustomer;
    }

    public static int totalFlightsForManagePnr(List<MyFlightsPnr> flights) {

        int totalFlightsForManage = 0;

        if (flights != null && flights.size() > 0) {

            for (MyFlightsPnr myFlightsPnr : flights) {
                CheckinPnrSearchResponse checkinPnrSearchResponse = myFlightsPnr.getCheckinPnrSearchResponse();
                if (checkinPnrSearchResponse != null && checkinPnrSearchResponse.getPnrData() != null && checkinPnrSearchResponse.getPnrData().getPnr() != null) {
                    if (checkinPnrSearchResponse.getPnrData().getPnr().get(0).getRoundTrip()) {
                        if (checkinPnrSearchResponse.getPnrData().getPnr().get(0).getFlights().get(0).getIsWebCheckinPermitted()) {
                            if (!checkinPnrSearchResponse.getPnrData().getPnr().get(0).getFlights().get(0).getSegments().get(0).getOpenCI()) {
                                totalFlightsForManage++;
                            }
                        } else {
                            totalFlightsForManage++;
                        }
                    } else {
                        for (Flight flight : checkinPnrSearchResponse.getPnrData().getPnr().get(0).getFlights()) {
                            if (flight.getIsWebCheckinPermitted()) {
                                if (!flight.getSegments().get(0).getOpenCI()) {
                                    totalFlightsForManage++;
                                }
                            } else {
                                totalFlightsForManage++;
                            }
                        }
                    }
                }
            }
        }
        return totalFlightsForManage;
    }

    public static int totalFlightsAvailableForCheckinPnr(List<MyFlightsPnr> flights) {
        int totalFlightsAvailableForCheckin = 0;

        if (flights != null && flights.size() > 0) {

            for (MyFlightsPnr myFlightsPnr : flights) {
                CheckinPnrSearchResponse checkinPnrSearchResponse = myFlightsPnr.getCheckinPnrSearchResponse();
                if (checkinPnrSearchResponse != null && checkinPnrSearchResponse.getPnrData() != null && checkinPnrSearchResponse.getPnrData().getPnr() != null && checkinPnrSearchResponse.getPnrData().getPnr().size() > 0) {
                    if (checkinPnrSearchResponse.getPnrData().getPnr().get(0).getRoundTrip()) {
                        if (checkinPnrSearchResponse.getPnrData().getPnr().get(0).getFlights().get(0).getIsWebCheckinPermitted()) {
                            if (checkinPnrSearchResponse.getPnrData().getPnr().get(0).getFlights().get(0).getSegments().get(0).getOpenCI()) {
                                totalFlightsAvailableForCheckin++;
                            }
                        }
                    } else {
                        for (Flight flight : checkinPnrSearchResponse.getPnrData().getPnr().get(0).getFlights()) {
                            if (flight.getIsWebCheckinPermitted()) {
                                if (flight.getSegments().get(0).getOpenCI()) {
                                    totalFlightsAvailableForCheckin++;
                                }
                            }
                        }
                    }
                }
            }
        }
        return totalFlightsAvailableForCheckin;
    }

    public static int totalFlightsForManage(List<MyFlightsTicket> flights) {

        int totalFlightsForManage = 0;

        if (flights != null && flights.size() > 0) {

            for (MyFlightsTicket myFlightsTicket : flights) {
                CheckinTicketSearchResponse checkinPnrSearchResponse = myFlightsTicket.getCheckinTicketSearchResponse();
                if (checkinPnrSearchResponse != null && checkinPnrSearchResponse.getPnrData() != null && checkinPnrSearchResponse.getPnrData().getPnr() != null && checkinPnrSearchResponse.getPnrData().getPnr().size() > 0) {
                    if (checkinPnrSearchResponse.getPnrData().getPnr().get(0).getRoundTrip()) {
                        if (checkinPnrSearchResponse.getPnrData().getPnr().get(0).getFlights().get(0).getIsWebCheckinPermitted()) {
                            if (!checkinPnrSearchResponse.getPnrData().getPnr().get(0).getFlights().get(0).getSegments().get(0).getOpenCI()) {
                                totalFlightsForManage++;
                            }
                        } else {
                            totalFlightsForManage++;
                        }
                    } else {
                        for (Flight flight : checkinPnrSearchResponse.getPnrData().getPnr().get(0).getFlights()) {
                            if (flight.getIsWebCheckinPermitted()) {
                                if (!flight.getSegments().get(0).getOpenCI()) {
                                    totalFlightsForManage++;
                                }
                            } else {
                                totalFlightsForManage++;
                            }
                        }
                    }
                }
            }
        }
        return totalFlightsForManage;
    }

    public static int totalFlightsAvailableForCheckin(List<MyFlightsTicket> flights) {
        int totalFlightsAvailableForCheckin = 0;

        if (flights != null && flights.size() > 0) {

            for (MyFlightsTicket myFlightsTicket : flights) {
                CheckinTicketSearchResponse checkinPnrSearchResponse = myFlightsTicket.getCheckinTicketSearchResponse();
                if (checkinPnrSearchResponse != null && checkinPnrSearchResponse.getPnrData() != null && checkinPnrSearchResponse.getPnrData().getPnr() != null) {
                    if (checkinPnrSearchResponse.getPnrData().getPnr().get(0).getRoundTrip()) {
                        if (checkinPnrSearchResponse.getPnrData().getPnr().get(0).getFlights().get(0).getIsWebCheckinPermitted()) {
                            if (checkinPnrSearchResponse.getPnrData().getPnr().get(0).getFlights().get(0).getSegments().get(0).getOpenCI()) {
                                totalFlightsAvailableForCheckin++;
                            }
                        }
                    } else {
                        for (Flight flight : checkinPnrSearchResponse.getPnrData().getPnr().get(0).getFlights()) {
                            if (flight.getIsWebCheckinPermitted()) {
                                if (flight.getSegments().get(0).getOpenCI()) {
                                    totalFlightsAvailableForCheckin++;
                                }
                            }
                        }
                    }
                }
            }
        }
        return totalFlightsAvailableForCheckin;
    }
}
