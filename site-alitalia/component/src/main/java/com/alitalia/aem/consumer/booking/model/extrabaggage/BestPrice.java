package com.alitalia.aem.consumer.booking.model.extrabaggage;

import java.util.*;

public class BestPrice
{
	public BestPrice()
	{

	}

	private java.time.LocalDateTime Date = java.time.LocalDateTime.MIN;
	public final java.time.LocalDateTime getDate()
	{
		return Date;
	}
	public final void setDate(java.time.LocalDateTime value)
	{
		Date = value;
	}
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [System.Xml.Serialization.XmlElement(IsNullable = true)] public Nullable<DateTime> ReturnDate {get;set;}
	private java.time.LocalDateTime ReturnDate = null;
	public final java.time.LocalDateTime getReturnDate()
	{
		return ReturnDate;
	}
	public final void setReturnDate(java.time.LocalDateTime value)
	{
		ReturnDate = value;
	}
	private int Price;
	public final int getPrice()
	{
		return Price;
	}
	public final void setPrice(int value)
	{
		Price = value;
	}
	private String Type;
	public final String getType()
	{
		return Type;
	}
	public final void setType(String value)
	{
		Type = value;
	}
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [System.Xml.Serialization.XmlElement(IsNullable =true)] public List<DailyPrice> DailyPrices {get;set;}
	private ArrayList<DailyPrice> DailyPrices;
	public final ArrayList<DailyPrice> getDailyPrices()
	{
		return DailyPrices;
	}
	public final void setDailyPrices(ArrayList<DailyPrice> value)
	{
		DailyPrices = value;
	}
}