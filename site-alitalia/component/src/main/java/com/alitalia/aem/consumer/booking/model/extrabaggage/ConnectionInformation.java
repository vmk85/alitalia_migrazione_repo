package com.alitalia.aem.consumer.booking.model.extrabaggage;

public class ConnectionInformation
{
	private String id;
	public final String getid()
	{
		return id;
	}
	public final void setid(String value)
	{
		id = value;
	}
	private int duration;
	public final int getduration()
	{
		return duration;
	}
	public final void setduration(int value)
	{
		duration = value;
	}
	private boolean changeOfAirport;
	public final boolean getchangeOfAirport()
	{
		return changeOfAirport;
	}
	public final void setchangeOfAirport(boolean value)
	{
		changeOfAirport = value;
	}
}