package com.alitalia.aem.consumer.checkin.render;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;
import com.day.cq.i18n.I18n;

public class CheckinAncillaryExtraBaggageRender {

	private String identifier = "";
	private List<CheckinFlightRender> flightDataRenderList;
	private MmbPriceRender priceRender;
	private boolean allowModify = false;
	private boolean checked = false;
	private String value = "";
	private Integer baggageAllowNumber = 0;
	private Integer baggageWeight = null;
	private String routeTypeLabel = "";
	private String busLabel = "";
	
	private CheckinFlightRender firstFlight = null;
	private CheckinFlightRender lastFlight = null;
	
	private boolean currentValue = false;
	
	public CheckinAncillaryExtraBaggageRender(List<CheckinFlightData> flightDataList, CheckinSessionContext ctx) {
		super();
		
		flightDataRenderList = new ArrayList<CheckinFlightRender>();
		for (CheckinFlightData flightData : flightDataList) {
			flightDataRenderList.add(new CheckinFlightRender(flightData, ctx));
			busLabel = computeBusLabel(flightDataList, ctx.i18n);
			
			//FIXME valorizzate babbageAllowance
			
		}
	}
	
	private String computeBusLabel(List<CheckinFlightData> flightDataList, I18n i18n) {
		String labelBus = "";
		if (flightDataList != null) {
			for (CheckinFlightData flight : flightDataList) {
				if (flight.isBus()) {
					return "- " + i18n.get("booking.bus.label");
				}
			}
		}
		return labelBus;
	}
	
	public List<CheckinFlightRender> getFlightDataRenderList() {
		return flightDataRenderList;
	}

	public MmbPriceRender getPriceRender() {
		return priceRender;
	}

	public void setPriceRender(MmbPriceRender priceRender) {
		this.priceRender = priceRender;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getBaggageAllowNumber() {
		return baggageAllowNumber;
	}
	
	public void setBaggageAllowNumber(int baggageAllowNumber) {
		this.baggageAllowNumber = baggageAllowNumber;
	}

	public String getRouteTypeLabel() {
		return routeTypeLabel;
	}

	public void setRouteTypeLabel(String routeTypeLabel) {
		this.routeTypeLabel = routeTypeLabel;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public CheckinFlightRender getFirstFlight() {
		return firstFlight;
	}

	public void setFirstFlight(CheckinFlightRender firstFlight) {
		this.firstFlight = firstFlight;
	}

	public CheckinFlightRender getLastFlight() {
		return lastFlight;
	}

	public void setLastFlight(CheckinFlightRender lastFlight) {
		this.lastFlight = lastFlight;
	}
	
	public boolean isCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(boolean currentValue) {
		this.currentValue = currentValue;
	}

	public boolean isAllowModify() {
		return allowModify;
	}

	public void setAllowModify(boolean allowModify) {
		this.allowModify = allowModify;
	}

	public Integer getBaggageWeight() {
		return baggageWeight;
	}

	public void setBaggageWeight(Integer baggageWeight) {
		this.baggageWeight = baggageWeight;
	}
	
	public String getBusLabel() {
		return busLabel;
	}
	
}
