package com.alitalia.aem.consumer.mmb.render;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MmbAncillaryExtraBaggagePassengerRender {

	private Integer id;
	private String fullName = "";
	private boolean hasInfant = false;
	private boolean child = false;
	private String codiceMillemiglia = "";
	private int totalToBeIssuedQuantity = 0;
	private MmbPriceRender totalToBeIssuedPrice = null;
	private List<MmbAncillaryExtraBaggageRender> extraBaggageItemList = null;
	private boolean displayInFeedback = false;
	private Map<Integer, Integer> toBeIssuedQuantityByBaggageWeight = null;
	
	public MmbAncillaryExtraBaggagePassengerRender() {
		super();
		this.extraBaggageItemList = new ArrayList<MmbAncillaryExtraBaggageRender>();
		this.toBeIssuedQuantityByBaggageWeight = new HashMap<>();
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public boolean isHasInfant() {
		return hasInfant;
	}

	public void setHasInfant(boolean hasInfant) {
		this.hasInfant = hasInfant;
	}
	
	public boolean isChild() {
		return child;
	}

	public void setChild(boolean child) {
		this.child = child;
	}

	public String getCodiceMillemiglia() {
		return codiceMillemiglia;
	}

	public void setCodiceMillemiglia(String codiceMillemiglia) {
		this.codiceMillemiglia = codiceMillemiglia;
	}

	public List<MmbAncillaryExtraBaggageRender> getExtraBaggageItemList() {
		return extraBaggageItemList;
	}

	public boolean isDisplayInFeedback() {
		return displayInFeedback;
	}

	public void setDisplayInFeedback(boolean displayInFeedback) {
		this.displayInFeedback = displayInFeedback;
	}

	public int getTotalToBeIssuedQuantity() {
		return totalToBeIssuedQuantity;
	}

	public void setTotalToBeIssuedQuantity(int totalToBeIssuedQuantity) {
		this.totalToBeIssuedQuantity = totalToBeIssuedQuantity;
	}

	public MmbPriceRender getTotalToBeIssuedPrice() {
		return totalToBeIssuedPrice;
	}

	public void setTotalToBeIssuedPrice(MmbPriceRender totalToBeIssuedPrice) {
		this.totalToBeIssuedPrice = totalToBeIssuedPrice;
	}
	
	public List<Integer> getToBeIssuedBaggageWeights() {
		List<Integer> list = new ArrayList<Integer>(toBeIssuedQuantityByBaggageWeight.keySet());
		Collections.sort(list);
		return list;
	}
	
	public List<Integer> getToBeIssuedBaggageQuantityByWeights() {
		List<Integer> toBeIssuedBaggageWeights = getToBeIssuedBaggageWeights();
		List<Integer> toBeIssuedBaggageQuantityByWeights = new ArrayList<>(toBeIssuedBaggageWeights.size());
		for (Integer baggageWeight : toBeIssuedBaggageWeights) {
			toBeIssuedBaggageQuantityByWeights.add(this.toBeIssuedQuantityByBaggageWeight.get(baggageWeight));
		}
		return toBeIssuedBaggageQuantityByWeights;
	}

	public void increaseToBeIssuedQuantityForBaggageWeight(Integer baggageWeight) {
		Integer currentValue = this.toBeIssuedQuantityByBaggageWeight.get(baggageWeight);
		if (currentValue == null) {
			this.toBeIssuedQuantityByBaggageWeight.put(baggageWeight, 1);
		} else {
			this.toBeIssuedQuantityByBaggageWeight.put(baggageWeight, currentValue + 1);
		}
	}
	
}
