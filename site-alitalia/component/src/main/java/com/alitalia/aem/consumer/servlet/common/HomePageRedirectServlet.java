package com.alitalia.aem.consumer.servlet.common;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.AlitaliaGeolocalizationManager;
import com.alitalia.aem.consumer.AlitaliaGeolocalizationManager.AlitaliaGeolocalizationData;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.CookieUtils;

/**
 * Apply an automatic redirect to the home page of the correct language, using the
 * user locale preference cookie, geolocalization data or default-site configuration.
 */
@SuppressWarnings("serial")
@SlingServlet(resourceTypes = "cq/Page", selectors = "homeredirect", extensions = "html")
public class HomePageRedirectServlet extends SlingSafeMethodsServlet {
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private AlitaliaGeolocalizationManager geoManager;
	
	private final Logger log = LoggerFactory.getLogger(HomePageRedirectServlet.class);
	
	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		String targetUrl = null;
		
		try {
			log.debug("HomePageRedirectServlet activated.");
			targetUrl = getTargetUrl(request, response);
		
		} catch (Exception e) {
			log.error("Cannot redirect to home page, an unexpected error occurred.", e);
			response.sendError(500);
			return;
		}
		
		if(targetUrl.contains(configuration.getHomePage())){
			targetUrl = targetUrl.substring(0, targetUrl.lastIndexOf("/"));
		}
		log.debug("targetUrl: " + targetUrl);
		response.sendRedirect(targetUrl);
	}
	
	/**
	 * Returns the resolved and mapped URL of the home page for the correct locale,
	 * applying the preferred locale and geolocation cookies logic if required.
	 *
	 * @param request The Sling Http Request object
	 * @param response The Sling Http Response object
	 * @return The URL to the home page. If not found or in case of error, returns the default path.
	 */
	private String getTargetUrl(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
		
		String targetRepositoryPath = null;
		String targetUrl = null;
		
		// check if a user preferred locale exists as a persistent cookie
		String rememberMeCookieLocale = 
				CookieUtils.getCookieValue(request,
						AlitaliaConstants.COOKIE_REMEMBER_LOCALE);
		
		if (rememberMeCookieLocale != null) {
			
			// generate path based on preferred locale cookie
			log.debug("User locale preference cookie found: {}", rememberMeCookieLocale);
			targetRepositoryPath = buildRepositoryPath(rememberMeCookieLocale);
			targetUrl = buildTargetUrl(request, rememberMeCookieLocale);
			//log.debug("Generating path from locale cookie value, targetUrl is {}", targetUrl);
			log.debug("Generating path from locale cookie value, targetRepositoryPath is {}", targetRepositoryPath);
		
		} else {
			
			// generate path based on geolocalization (will read or set the geolocalization cookie)
			log.debug("User locale preference cookie not found.");
			AlitaliaGeolocalizationData geoData = geoManager.processFromRequest(request, response);
			if (geoData != null) {
				String geoLocale = (geoData.getCountryCode().toLowerCase() + "_" + geoData.getLanguageCode()).toLowerCase();
				targetRepositoryPath = buildRepositoryPath(geoLocale);
				targetUrl = buildTargetUrl(request, geoLocale);
				log.debug("Generating path from geolocalization data, targetRepositoryPath is {}", targetUrl);
			
			} else {
				log.info("Cannot determine repository path, geolocalization data not available.");
			}	
		}
			
		// check that a repository path was found and if it actually exists,
		// otherwise use a default site path to avoid a 404
		if(targetRepositoryPath != null && targetRepositoryPath.contains(".html")){
			targetRepositoryPath = targetRepositoryPath.substring(0, targetRepositoryPath.lastIndexOf("."));
		}
		log.debug("targetRepositoryPath: " + targetRepositoryPath);
		log.debug("targetRepositoryPath exists: " + request.getResourceResolver().getResource(targetRepositoryPath));

		if ((targetUrl == null ) || request.getResourceResolver().getResource(targetRepositoryPath) == null) {
			
			String defaultSite = "";
			if (configuration.getDefaultSite() != null && !configuration.getDefaultSite().equals("")) {
				defaultSite = configuration.getDefaultSite();
			}
			log.warn("Target repository path not found, fallback to default site {}.", defaultSite);
			targetUrl = buildTargetUrl(request, defaultSite);
		}
		
		return targetUrl;
	}
	
	/**
	 * Build an internal repository path to the home page for the specified locale.
	 *
	 * @param locale The locale, e.g. <code>en_us</code>.
	 * @return The repository path, e.g. <code>/content/alitalia-en/us/home-page.html</code>.
	 */
	private String buildRepositoryPath(String locale) {
		log.debug("BuildRepositoryPath - Locale: " + locale);
		String localePath = AlitaliaConstants.LOCAL_SITE_PREFIX + locale.replace("_", "/");
		log.debug("BuildRepositoryPath - localePath: " + localePath);
		String repositoryPath = "/" + configuration.getContext() + localePath + "/" + configuration.getHomePageWithExtension();
		log.debug("BuildRepositoryPath - repositoryPath: " + repositoryPath);
		return repositoryPath;
	}
	
	/**
	 * Build an external, resolver-mapped URL to the home page for the specified locale.
	 *
	 * @param request The request, used to determine protocol, server name and port.
	 * @param locale The locale, e.g. <code>en_us</code>.
	 * @return The mapped URL, e.g. <code>http://www.alitalia.com/en_us/home-page.html</code>.
	 */
	private String buildTargetUrl(SlingHttpServletRequest request, String locale) {
		Resource resource = request.getResourceResolver().resolve(buildRepositoryPath(locale));
		if(resource != null){
			log.debug("buildTargetUrl - resource: " + resource.getPath());
		} else{
			log.debug("buildTargetUrl - resource: NULL");
		}
		
		return AlitaliaUtils.findSiteResourceExternalUrl(request, resource);
	}
	
}
