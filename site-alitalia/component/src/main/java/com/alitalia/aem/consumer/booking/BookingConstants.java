package com.alitalia.aem.consumer.booking;

public class BookingConstants {
	
	// FIXME [booking/i18n] rivedere costanti di errore per usare le resource key
	
	public static final String BOOKING_CONTEXT_ATTRIBUTE = "alitaliaConsumerBookingSessionContext";
	
	public static final String MESSAGE_GENERIC_EMPTY_FIELD = "booking.common.genericEmptyField.label";
	public static final String MESSAGE_GENERIC_INVALID_FIELD = "booking.common.genericInvalidField.label";
	public static final String MESSAGE_GENERIC_CHECK_FIELD = "booking.common.genericUncheckedField.label";
	public static final String MESSAGE_SPECIFIC_PREFIX_NOT_VALID="booking.specific.prefix.empty";
	public static final String MESSAGE_SPECIFIC_VALORE_RECAPITO_NOT_VALID="booking.specific.recapito.empty";
	public static final String MESSAGE_SPECIFIC_MAIL_NOT_VALID="booking.specific.email.notValid";
	public static final String MESSAGE_AIRPORT_FROM_NOT_VALID = "booking.common.aeroportoPartenzaNonValido.label"; //Aeroporto di partenza non valido";
	public static final String MESSAGE_AIRPORT_TO_NOT_VALID = "booking.common.aeroportoArrivoNonValido.label"; //Aeroporto di arrivo non valido";
	public static final String MESSAGE_BABY_PER_ADULT_NOT_VALID = "Numero di bambini maggiore del numero di adulti";
	public static final String MESSAGE_CUG_NOT_VALID = "CUG non valido";
	public static final String MESSAGE_DATE_AFTER_NOT_VALID = "booking.common.dataPartenzaSuccessivaArrivo.label"; //Data di partenza maggiore della data di arrivo";
	public static final String MESSAGE_DATE_FUTURE = "booking.common.dataSuccessivaOggi.label"; //Data successiva ad oggi";
	public static final String MESSAGE_DATE_PAST = "booking.common.dataAntecedenteOggi.label"; //Data antecedente ad oggi";
	public static final String MESSAGE_EQUALS_NAMES = "booking.common.passeggeriUguali.label"; //Siamo spiacenti, ma non è possibile prenotare on line un biglietto per passeggeri con lo stesso nome e cognome";
	public static final String MESSAGE_FROM_TO_DIFF = "booking.common.aeroportoPartenzaArrivoUguali.label"; //Aeroporto di partenza e arrivo uguali";
	public static final String MESSAGE_NADULTS_NOT_VALID = "Numero di adulti non valido";
	public static final String MESSAGE_NBABIES_NOT_VALID = "Numero di nenonati non valido";
	public static final String MESSAGE_NKINDS_NOT_VALID = "Numero di bambini non valido";
	public static final String MESSAGE_NYOUNGS_NOT_VALID = "Numero di giovani non valido";
	public static final String MESSAGE_RESPONSIBLE_ADULTS_NOT_VALID = "booking.common.adultoResponsabile.label";//Ogni adulto può essere responsabile solamente di un neonato";
	public static final String MESSAGE_SUM_ADULTS_KIDS_NOT_VALID = "Il Numero di adulti e di bambini deve essere inferiore a 7";
	public static final String MESSAGE_FLIGHTS_NOT_FOUND = "booking.award.voliNonTrovati.label";
	
	public static final String FLIGHT_SELECT_OUTBOUND_TITLE = "booking.flightSelect.titoloAndata.label";
	public static final String FLIGHT_SELECT_OUTBOUND_TEXT = "booking.flightSelect.andata.label";
	public static final String FLIGHT_SELECT_OUTBOUND_DESCRIPTION = "booking.flightSelect.descrizioneAndata.label";
	public static final String FLIGHT_SELECT_RETURN_TITLE = "booking.flightSelect.titoloRitorno.label";
	public static final String FLIGHT_SELECT_RETURN_TEXT = "booking.flightSelect.ritorno.label";
	public static final String FLIGHT_SELECT_RETURN_DESCRIPTION = "booking.flightSelect.descrizioneRitorno.label";
	public static final String FLIGHT_SELECT_FIRST_SLICE_TITLE = "booking.flightSelect.titoloPrimaTratta.label";
	public static final String FLIGHT_SELECT_FIRST_SLICE_TEXT = "booking.flightSelect.primaTratta.label";
	public static final String FLIGHT_SELECT_SECOND_SLICE_TITLE = "booking.flightSelect.titoloSecondaTratta.label";
	public static final String FLIGHT_SELECT_SECOND_SLICE_TEXT = "booking.flightSelect.secondaTratta.label";
	public static final String FLIGHT_SELECT_THIRD_SLICE_TITLE = "booking.flightSelect.titoloTerzaTratta.label";
	public static final String FLIGHT_SELECT_THIRD_SLICE_TEXT = "booking.flightSelect.terzaTratta.label";
	public static final String FLIGHT_SELECT_FOURTH_SLICE_TITLE = "booking.flightSelect.titoloQuartaTratta.label";
	public static final String FLIGHT_SELECT_FOURTH_SLICE_TEXT = "booking.flightSelect.quartaTratta.label";
	public static final String FLIGHT_SELECT_MULTISLICE_DESCRIPTION = "booking.flightSelect.descrizioneMultislice.label";
	
	public static final String INPUT_SELECT_DEFAULT_VALUE = "booking.common.seleziona.label";
	
	public static final String MESSAGE_ERROR_INITIALIZE = "booking.payment.errorInitialize";
	public static final String MESSAGE_ERROR_INITIALIZE_INFANT_INVENTORY_UNAVAILABLE = "booking.payment.errorInitialize.infantInventoryUnavailable";
	public static final String MESSAGE_ERROR_AUTHORIZE = "booking.payment.errorAuthorize";
	
	public static final String ALITALIA_CARRIER = "AZ";
	public static final int LOAN_PRICE_NOT_SATISFIED = 1;
	public static final int LOAN_DATE_NOT_SATISFIED = 2;
	public static final int LOAN_MINIMUM_PRICE = 400;
	public static final int LOAN_DAY_OFFSET = 20;
	public static final String LOAN_MARKET = "IT";

	public static final String PAYMENT_DATA_ATTRIBUTE = "alitaliaConsumerPaymentData";
}
