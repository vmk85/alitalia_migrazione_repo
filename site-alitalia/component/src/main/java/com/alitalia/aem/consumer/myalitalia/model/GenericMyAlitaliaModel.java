/** Modello generico per controller MyAlitalia
 * ( Classe da estendere in caso di creazione controller ) [D.V.]*/

package com.alitalia.aem.consumer.myalitalia.model;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaConstants;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSessionContext;
import com.alitalia.aem.consumer.myalitalia.logger.MyAlitaliaSessionLog;
import org.apache.sling.api.SlingHttpServletRequest;

import javax.inject.Inject;

public class GenericMyAlitaliaModel extends GenericBaseModel {

    protected MyAlitaliaSessionContext mactx;

    protected MyAlitaliaSessionLog myAlitaliaSessionLog = new MyAlitaliaSessionLog();

    @Inject
    protected AlitaliaConfigurationHolder configuration;

    /** Funzione per inizializzare il contesto MyAlitalia*/
    @Override
    protected void initBaseModel(SlingHttpServletRequest request) {

        /** Inizializzo il costruttore */
        super.initBaseModel(request);

        myAlitaliaSessionLog.debug("GenericMyAlitaliaModel (initBaseModel) session accessed: [" + request.getSession().getId() + "]");

        /** Prendo dalla sessione il contesto */
        if(request.getSession(true).getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE) != null) {
            mactx = (MyAlitaliaSessionContext) request.getSession(true).getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);
        } else {
            mactx = null;
        }

        /* AMS Bugfix #5045 Utente non loggato visualizza nome di un altro utente MyAlitalia */
        /** Gestisco l'eccezione in caso di mancanza di contesto */
        if (mactx == null && !isWCMEnabled()) {
            //myAlitaliaSessionLog.error("MyAlitalia session context not found.");
            throw new RuntimeException("MyAlitalia session context not found.");
        }
        /* Fine Bugfix #5045 */

    }

    public MyAlitaliaSessionContext getMactx() {
        return mactx;
    }
}
