package com.alitalia.aem.consumer.mmb.render;

import java.math.BigDecimal;
import java.text.NumberFormat;

public class MmbPriceRender{

	public static final int DEFAULT_FRACTION_DIGITS = 2;
	public static final String DEFAULT_NULL_VALUE = "";
	
	private NumberFormat numberFormat;
	private BigDecimal amount;
	//private Locale locale;
	private String currencyCode;
	private String currencySymbol;
	//private int fractionDigits;
	private String nullValue;

	public MmbPriceRender(BigDecimal amount, String currencyCode, String currencySymbol, int fractionDigits, 
			String nullValue, NumberFormat numberFormat) {
		this.amount = amount;
		this.currencyCode = currencyCode;
		this.currencySymbol = currencySymbol;
		//this.fractionDigits = fractionDigits;
		this.nullValue = nullValue;
		this.numberFormat = numberFormat;
	}
	
	public MmbPriceRender(BigDecimal amount, String currencyCode, String currencySymbol,
			String nullValue, NumberFormat numberFormat) {
		this.amount = amount;
		this.currencyCode = currencyCode;
		this.currencySymbol = currencySymbol;
		//this.fractionDigits = DEFAULT_FRACTION_DIGITS;
		this.nullValue = nullValue;
		this.numberFormat = numberFormat;
	}
	
	public MmbPriceRender(BigDecimal amount, String currencyCode, String currencySymbol, NumberFormat numberFormat) {
		this.amount = amount;
		this.currencyCode = currencyCode;
		this.currencySymbol = currencySymbol;
		//this.fractionDigits = DEFAULT_FRACTION_DIGITS;
		this.nullValue = DEFAULT_NULL_VALUE;
		this.numberFormat = numberFormat;
	}
	
	public BigDecimal getAmount(){
		return this.amount;
	}
	
	public String getAmountAsString(){
		if (this.amount != null) {
			return this.amount.toString();
		} else {
			return "";
		}
	}
	
	public String getCurrencySymbol(){
		return this.currencySymbol;
	}
	
	public String getCurrencyCode(){
		return this.currencyCode;
	}
	
	public String getFormattedAmount() {
		if (this.amount == null) {
			return nullValue;
		} else {
			return getFormattedAmountInternal();
		}
	}
	
	public String getFormattedAmountWithCurrency() {
		if (this.amount == null) {
			return nullValue;
		} else {
			return getFormattedAmountInternal() + " " + getCurrencySymbol();
		}
	}
	
	public void increaseAmount(BigDecimal amount) {
		if (amount != null) {
			if (this.amount == null) {
				this.amount = amount;
			} else {
				this.amount = this.amount.add(amount);
			}
		}
	}
	
	public Boolean getIsAmountZero() {
		return this.amount == null || this.amount.equals(new BigDecimal(0));
	}
	
	public Boolean getIsAmountZeroOrNull() {
		return this.amount == null || this.amount.equals(new BigDecimal(0));
	}

	
	/* internal methods */

	/*private void initNumberFormat() {
		numberFormat = NumberFormat.getInstance(locale);
		numberFormat.setMinimumFractionDigits(fractionDigits);
		numberFormat.setMaximumFractionDigits(fractionDigits);
	}*/
	
	private String getFormattedAmountInternal() {
		return numberFormat.format(this.amount.doubleValue());
	}
	
}
