package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;

import com.alitalia.aem.common.data.checkinrest.model.cheangeseats.response.ComfortSeat;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.*;
import com.alitalia.aem.common.data.checkinrest.model.passengerdata.TotalAncillaryForPax;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.*;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Segment;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrListDataRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;
import com.alitalia.aem.consumer.utils.AlitaliaMailUtils;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.alitalia.aem.ws.common.exception.WSClientException;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.i18n.I18n;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.cheangeseats.response.ComfortSeat;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.request.AzAddressPayment;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.request.FraudNetParam;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.request.PaymentDetail;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.AuthorizationResult;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.AzAncillaryPayedTP;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.AzPaxPayedTP;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.AzSegmentPayedTP;
import com.alitalia.aem.common.data.checkinrest.model.passengerdata.TotalAncillaryForPax;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Emd;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Flight;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerSeat;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Segment;
import com.alitalia.aem.common.data.home.MACreditCardData;
import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.messages.checkinrest.CheckinInitPaymentResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.model.PaymentType;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrListDataRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaConstants;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaMailUtils;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.alitalia.aem.ws.common.exception.WSClientException;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.i18n.I18n;
import com.google.gson.Gson;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = {"checkininitpayment"}),
        @Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
        @Property(name = "sling.servlet.extensions", value = {"json"}),
        @Property(name = "sling.servlet.methods", value = {"POST"})})
@SuppressWarnings("serial")
public class CheckinInitPaymentServlet extends GenericCheckinFormValidatorServlet {

    // logger
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile ICheckinDelegate checkInDelegateRest;

    @Reference
    private AlitaliaConfigurationHolder configuration;

    @Reference
    private CheckinSession checkinSession;

    private final String FRAUDNET_IP_HEADER = "X-Forwarded-For";


    @Override
    protected AlitaliaConfigurationHolder getConfiguration() {
        return configuration;
    }

    private AlitaliaMailUtils alitaliaMailUtils = new AlitaliaMailUtils();

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        //TODO - c'e' solo validazione frontend
        ResultValidation resultValidation = new ResultValidation();
        resultValidation.setResult(true);
        return resultValidation;
    }


    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
        CheckinSessionContext ctx = getCheckInSessionContext(request);
        ctx.paymentErrorMsg = "";
        try {
			//Tolentino - Inizio
			String tipoPagamento = request.getParameter("tipoPagamento");
			if (PaymentType.OneClick.toString().equals(tipoPagamento)) {
				performOneClickPayment(request, response, ctx);
			} else {
			//Tolentino - Fine

            final I18n i18n = new I18n(request.getResourceBundle(
                    AlitaliaUtils.findResourceLocale(request.getResource())));

            String ipAddress = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
            String client = "Checkin";

            //INIZIO Valorizzazione PaymentDetail da form
            PaymentDetail paymentDetail = new PaymentDetail();
            paymentDetail.setCardCode(request.getParameter("cardCode"));                        // il nome della carta di credito per esteso
            paymentDetail.setCardNumber(request.getParameter("cardNumber"));                    //444433322221111
            paymentDetail.setExpireMonth(request.getParameter("expireMonth"));                    //08 (Agosto)
            paymentDetail.setExpireYear(request.getParameter("expireYear"));                    //2018
            paymentDetail.setCardSecurityCode(request.getParameter("cardSecurityCode"));        //CVC 737
            paymentDetail.setCardHolderFirstName(request.getParameter("cardHolderFirstName"));    //NOME
            paymentDetail.setCardHolderLastName(request.getParameter("cardHolderLastName"));    //COGNOME
            if(request.getParameter("cardBin") != null)
                paymentDetail.setCardBin(request.getParameter("cardBin"));

            /*** recupero informazioni da passare al managementSuccess per invio ricevuta
             *   via email in caso di  pagamento andato a buon fine ***/
            String[] values = new String[4];
            values[0] = request.getParameter("cardHolderFirstName") + " " + request.getParameter("cardHolderLastName");
            values[1] = ctx.pnr;
            values[2] = "";//getFlightsHtml(ctx, request);
            values[3] = "";//computeBusMessage(ctx);

            //recupero indirizzo email inserito dall'utente
            List<String> mails = new ArrayList<String>();
            String[] _mails = request.getParameter("email").split(",");
            for (String _mail : _mails) {
                mails.add(_mail);
            }

            /********* fine recupero info per invio ricevuta pagamento effettuato via mail ***************/

            paymentDetail.setAmount(ctx.totalPay);                        //prendo il valore salvato in sessione dal getCart()

            paymentDetail.setCurrencyCode(request.getParameter("currencyCode"));                //Valuta/Divisa --- EUR, GBP, USD
            paymentDetail.setApprovalCode("");                                                    //"" non viene valorizzato nella Init
            paymentDetail.setTransactionID("");                                                    //"" non viene valorizzato nella Init
            // Fine

            //Inizio Gestione URL :
            //1. paymentDetail.setApprovedURL;
            //2. paymentDetail.setErrorURL
            String protocol = request.getScheme();
            String serverName = request.getServerName();
            int port = request.getServerPort();
            logger.info("checkininitpaymentservlet: port = " + port);

//			if (port != -1) {
//				ctx.domain = protocol + "://" + serverName + ":" + port;
//				logger.info("checkininitpaymentservlet: domain = " + ctx.domain );
//			} else {
            ctx.domain = protocol + "://" + serverName;
            logger.info("checkininitpaymentservlet: domain = " + ctx.domain);
//			}

            String baseUrlForContext = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
            String baseUrlForRedirect = request.getResource().getResourceResolver().map(baseUrlForContext);

            String csrfToken = request.getHeader("CSRF-Token");

            //String errorUrl = "https://stage.alitalia.com/it_it/content/alitalia/alitalia-it/it/special-pages/newsletter-subscribe.newslettersubsubmit";
            //String approvedUrl =  "https://stage.alitalia.com/it_it/content/alitalia/alitalia-it/it/special-pages/newsletter-subscribe.newslettersubsubmit";
            String errorUrl = ctx.domain + baseUrlForRedirect + "/check-in-search/.checkinendpayment"; //"/booking/.redirectPaymentServlet"; //check-in-payment-complete.html"; //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment"
            String approvedUrl = ctx.domain + baseUrlForRedirect + "/check-in-search/.checkinendpayment"; //"/booking/.redirectPaymentServlet"; //check-in-payment-complete.html";//configuration.getBookingPaymentReturnPage(); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment"
            String redirectUrl = ctx.domain + baseUrlForRedirect + "/check-in-search/.checkinredirect3ds";
            ctx.redirectUrl = redirectUrl;
            logger.info("checkininitpaymentservlet: redirecturl = " + redirectUrl);

            if (!"undefined".equals(csrfToken)) {
                errorUrl += (errorUrl.contains("?") ? "&" : "?") + ":cq_csrf_token=" + csrfToken;
                approvedUrl += "?:cq_csrf_token=" + csrfToken;
            }

            //String approvedUrl = "https://www.google.it/";
            //String testUrl = "http://localhost:11111/content/alitalia/master-it/it/check-in-search/.checkinendpayment.json";
            paymentDetail.setApprovedURL(approvedUrl);
            paymentDetail.setErrorURL(errorUrl);

            //Fine Gestione URL

            //Inizio Gestione AzAddressPayment : valorizzato Obbligatoriamente solo in caso di pagamento con AMERICANEXPRESS
            if (paymentDetail.getCardCode().equalsIgnoreCase("AMERICANEXPRESS")) {
                AzAddressPayment azAddressPayment = new AzAddressPayment();

                azAddressPayment.setAddress(request.getParameter("address"));//Indirizzo
                azAddressPayment.setCityName(request.getParameter("cityName"));//Città
                azAddressPayment.setCountryName(request.getParameter("countryName"));//Stato
                azAddressPayment.setPostalCode(request.getParameter("postalCode"));//Codice Postale

                paymentDetail.setAzAddressPayment(azAddressPayment);
            } else {
                AzAddressPayment azAddressPayment = new AzAddressPayment();

                azAddressPayment.setAddress("");//Indirizzo
                azAddressPayment.setCityName("");//Città
                azAddressPayment.setCountryName("");//Stato
                azAddressPayment.setPostalCode("");//Codice Postale

                paymentDetail.setAzAddressPayment(azAddressPayment);
            }
            //Fine Gestione AzAddressPayment


            Map<String, String> httpHeaders = getHttpHeaders(request, ";");

            if (ipAddress != null && !ipAddress.isEmpty()) {
                httpHeaders.put(FRAUDNET_IP_HEADER, ipAddress);
            }
            //fin qui non c'e' ancora la proprieta' IPAddress
            String jscString = request.getParameter("beirutString");
            List<FraudNetParam> listFraudNetParam = new ArrayList<>();
            listFraudNetParam = addFraudnetInfo(httpHeaders, jscString);
            ctx.listFraudNetParam = listFraudNetParam;

            CheckinInitPaymentResponse result = checkinSession.getInitPayment(request, ctx, client, ipAddress, paymentDetail);


            if (result != null) {
                if (result.getInitPaymentResponse().getError() != null && !result.getInitPaymentResponse().getError().equals("")) {
                    managementError(request, response, result.getInitPaymentResponse().getError(), ctx);
                } else {

                    //rieseguo l'accettazione del passeggero che ha acquistato il posto comfort
                    boolean recheckin = checkinSession.checkinSelectedPassengerAfterPayment(request, ctx);

                    try {

                        if(result.getInitPaymentResponse().getaZPayedTPResponse() != null) {
                            SetEmdAfterPaymnt(result,ctx);
                            if (!result.getInitPaymentResponse().getaZPayedTPResponse().getAzPaxPayedTP().isEmpty()) {
                                for (AzPaxPayedTP azPaxPayedTP : result.getInitPaymentResponse().getaZPayedTPResponse().getAzPaxPayedTP()) {
                                    if (!azPaxPayedTP.getAzAncillaryPayedTP().isEmpty()) {
                                        for (AzAncillaryPayedTP azAncillaryPayedTP : azPaxPayedTP.getAzAncillaryPayedTP()) {
                                            if(ctx.comfortSeat != null) {
                                                setNewPassengerSeat(ctx.comfortSeat, ctx, request);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengersExtra().get(j).getPassenger().setNumeroPostoAssegnato(serviceResponce.getPnrData().getPnr().get(0));

                    } catch (Exception q) {
                        logger.error("Errore setEmd e setNewPassengerSeat" + q);
                        throw new WSClientException("Errore setEmd e setNewPassengerSeat", q);
                    }


                    int total = 0;
                    int executed = 0;
                    int notAllowed = 0;
                    //response.sendRedirect(successPage);
                    PnrListDataRender renderList = new PnrListDataRender();
                    List<PnrRender> pnrListDataRender = new ArrayList<>();
                    for (PnrRender pnrRender : renderList.getPnrListDataRender(ctx.pnrListData, ctx, total, executed, notAllowed)) {

                        for (FlightsRender flightsRender : pnrRender.getFlightsRender()) {
                            flightsRender.setTotalAncillaryForPax(new ArrayList<>());
                            if (flightsRender.getSegments().get(0).getPassengers() != null) {
                                for (Passenger passenger : flightsRender.getSegments().get(0).getPassengers()) {
                                    TotalAncillaryForPax totalAncillaryForPaxSet = new TotalAncillaryForPax();
                                    totalAncillaryForPaxSet.setPassenger(passenger);
                                    flightsRender.getTotalAncillaryForPax().add(totalAncillaryForPaxSet);
                                }
                            }
                        }
                        for (FlightsRender flightsRender : ctx.pnrSelectedListDataRender.get(0).getFlightsRender()) {
                            for (Passenger passenger : flightsRender.getSegments().get(0).getPassengers()) {
                                passenger.setNumberOfBaggage(0);
                                if(passenger.getEmd() != null) {
                                    for (Emd emd : passenger.getEmd()) {
                                        if (emd.getGroup().equals("BG")) {
                                            passenger.setNumberOfBaggage(passenger.getNumberOfBaggage() + 1);
                                        }
                                    }
                                }

                            }
                        }

                        for (FlightsRender flightsRender : pnrRender.getFlightsRender()) {
                            for (SegmentRender segmentRender : flightsRender.getSegments()) {
                                if (segmentRender.getPassengers() != null) {
                                    for (Passenger passenger : segmentRender.getPassengers()) {
                                        passenger.setNumberOfFT(0);
                                        passenger.setNumberOfBaggage(0);
                                        passenger.setNumberOfLouge(0);
                                        if(passenger.getEmd() != null) {
                                            for (Emd emd : passenger.getEmd()) {
                                                if (emd.getGroup().equals("TS")) {
                                                    passenger.setNumberOfFT(passenger.getNumberOfFT() + 1);
                                                }
                                                if (emd.getGroup().equals("LG")) {
                                                    passenger.setNumberOfLouge(passenger.getNumberOfLouge() + 1);
                                                }
                                                if (emd.getGroup().equals("BG")) {
                                                    passenger.setNumberOfBaggage(passenger.getNumberOfBaggage() + 1);
                                                }
                                            }
                                            for (TotalAncillaryForPax totalAncillaryForPax : flightsRender.getTotalAncillaryForPax()) {
                                                if (totalAncillaryForPax.getPassenger().getNome().equals(passenger.getNome()) && totalAncillaryForPax.getPassenger().getCognome().equals(passenger.getCognome())) {
                                                    totalAncillaryForPax.setTotalBaggage(passenger.getNumberOfBaggage());
                                                    totalAncillaryForPax.setTotalFast(totalAncillaryForPax.getTotalFast() + passenger.getNumberOfFT());
                                                    totalAncillaryForPax.setTotalLounge(totalAncillaryForPax.getTotalLounge() + passenger.getNumberOfLouge());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        pnrListDataRender.add(pnrRender);
                    }

                    //Tolentino - Inizio
                    //values[2] = getHTMLAncillary(request, ctx, pnrListDataRender, result, request.getParameter("cardCode"), new HierarchyNodeInheritanceValueMap(request.getResource().getParent()).getInherited("currencySymbol", String.class), i18n);
                    String cardNumber = request.getParameter("cardNumber").substring(0, request.getParameter("cardNumber").length() - 4).replaceAll("([0-9])", "*") + request.getParameter("cardNumber").substring(request.getParameter("cardNumber").length() - 4, request.getParameter("cardNumber").length());
                    values[2] = getHTMLAncillary(request, ctx, pnrListDataRender, result, request.getParameter("cardCode"), cardNumber, new HierarchyNodeInheritanceValueMap(request.getResource().getParent()).getInherited("currencySymbol", String.class), i18n);
                    //Tolentino - Fine
                    managementSuccess(request, response, result, redirectUrl, ctx, values, mails);
                }
            } else {
                logger.error("[CheckinInitPaymentServlet] - Errore durante l'invocazione del servizio.");
                managementError(request, response, "{\"isError\": true,\"errorMessage\": \"An error has occurred\",\"sabreStatusCode\": null,\"conversationID\": \"\",\"callerMethod\": null}", ctx);
            }

			//Tolentino - inizio
			}
			//Tolentino - Fine

        } catch (Exception e) {
            logger.error("Errore nel perfomSubmit di CheckinInitPaymentServlet, " + e.getMessage());
            throw new WSClientException("Error in CheckinInitPaymentServlet", e);
        }
    }

    public void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error, CheckinSessionContext ctx) {

        Gson gson = new Gson();
        TidyJSONWriter json;
        response.setContentType("application/json");

        try {
            json = new TidyJSONWriter(response.getWriter());
            json.object();
            json.key("isError").value(true);
            Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);
            final I18n i18n = new I18n(request.getResourceBundle(AlitaliaUtils.findResourceLocale(request.getResource())));

            String redirectPage = request.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) + getConfiguration().getCheckinThankYouPage());
            json.key("redirectPage").value("");

            if (errorObj.getErrorMessage().toLowerCase().contains("An error has occurred".toLowerCase())) {
                json.key("errorMessage").value(i18n.get("checkin.payment.error.generic")); //Ti invitiamo a riscrivere i dati della carta di credito
            } else if (errorObj.getErrorMessage().toLowerCase().contains("DECLINED".toLowerCase())) {
                json.key("errorMessage").value(i18n.get("checkin.payment.error.generic"));
            } else if (errorObj.getErrorMessage().toLowerCase().contains("DUPLICATE AE DATA".toLowerCase())) {
                json.key("errorMessage").value(i18n.get("checkin.payment.error.generic"));
            } else if (errorObj.getErrorMessage().toLowerCase().contains("INVALID CARD LENGTH".toLowerCase())) {
                json.key("errorMessage").value(i18n.get("checkin.payment.error.generic"));
            } else if (errorObj.getErrorMessage().toLowerCase().contains("SIMULTANEOUS CHANGES TO PNR".toLowerCase())) {
                json.key("errorMessage").value(i18n.get("checkin.payment.error.generic"));
            } else if (errorObj.getErrorMessage().toLowerCase().contains("Importo ancillaries cart vs cache non corrispondente".toLowerCase())) {
                json.key("errorMessage").value(i18n.get("checkin.payment.error.generic"));
            } else if (errorObj.getErrorMessage().toLowerCase().contains("Inserire dati CC corretti".toLowerCase())) {
                json.key("errorMessage").value(i18n.get("checkin.payment.error.generic"));
            } else if (errorObj.getErrorMessage().toLowerCase().contains("INSURANCE_ERROR".toLowerCase())) {
                ctx.paymentErrorMsg = i18n.get("checkin.payment.error.issue");
                // non viene inviata la mail e non verranno mostrati gli emd
                json.key("redirectPage").value(redirectPage);
                //rieseguo l'accettazione del passeggero che ha acquistato il posto comfort anche se non emesso - l'utente ha pagato
                boolean recheckin = checkinSession.checkinSelectedPassengerAfterPayment(request, ctx);
            } else if (errorObj.getErrorMessage().toLowerCase().contains("ISSUE_ERROR".toLowerCase())) {
                ctx.paymentErrorMsg = i18n.get("checkin.payment.error.issue");
                // non viene inviata la mail e non verranno mostrati gli emd
                json.key("redirectPage").value(redirectPage);
                //rieseguo l'accettazione del passeggero che ha acquistato il posto comfort anche se non emesso - l'utente ha pagato
                boolean recheckin = checkinSession.checkinSelectedPassengerAfterPayment(request, ctx);
            } else {
                json.key("errorMessage").value(i18n.get("checkin.payment.error.generic"));
            }

            json.key("originalErrorMessage").value(errorObj.getErrorMessage());
            json.endObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void managementSuccess(SlingHttpServletRequest request, SlingHttpServletResponse response, CheckinInitPaymentResponse result, String redirectUrl, CheckinSessionContext ctx, String[] values, List<String> mails) {

        logger.info("[CheckinInitPaymentServlet] [managementSuccess] ...");

        TidyJSONWriter json;
        try {
            json = new TidyJSONWriter(response.getWriter());
            response.setContentType("application/json");

            if (result.getInitPaymentResponse().getResult().getPaymentStatusDescription().equals("REDIRECT")) {
                //caso carta 3Ds
                if (result.getInitPaymentResponse().getRedirectInfoFor3D() != null) {
                    logger.info("[CheckinInitPaymentServlet] [managementSuccess 3D] redirectUrl = " + redirectUrl);
                    json.object();
                    json.key("isError").value(false);
                    json.key("redirectPage").value(redirectUrl);
                    json.endObject();
                }
            } else if (result.getInitPaymentResponse().getResult().getPaymentStatusDescription().equals("AUTHORIZED")) {

                ByteArrayOutputStream out = null;
                byte[] attachment = null;

                checkinSession.sendEmail(ctx, request.getResource(), mails, CheckinConstants.CHECKIN_TEMPLATE_EMAIL_ANCILLARY, CheckinConstants.CHECKIN_ANCILLARY_EMAIL_SUBJECT, attachment, CheckinConstants.CHECKIN_ANCILLARY_EMAIL_ATTACHMENT, CheckinConstants.PLACEHOLDER_BOARDING, values, CheckinConstants.CHECKIN_MAIL_REMINDER, ctx.selectedPassengers);

                String successPage = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) + getConfiguration().getCheckinThankYouPage();

                successPage = request.getResourceResolver().map(successPage);

                logger.info("[CheckinInitPaymentServlet] [managementSuccess 2D] successPage = " + successPage);

                json.object();
                json.key("isError").value(false);
                json.key("successPage").value(successPage);
                json.endObject();
            } else if (result.getInitPaymentResponse().getResult().getPaymentStatusDescription().equals("APPROVED")) {
                String successPage = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) + getConfiguration().getCheckinThankYouPage();

                successPage = request.getResourceResolver().map(successPage);

                logger.info("[CheckinInitPaymentServlet] [managementSuccess 2D] successPage = " + successPage);

                json.object();
                json.key("isError").value(false);
                json.key("successPage").value(successPage);
                json.endObject();
            } else if (result.getInitPaymentResponse().getResult().getPaymentStatusDescription().equals("DECLINED")) {
//                todo trovare nei log casi di questo tipo
//                restituire json che mostri errore in popup
            } else if (result.getInitPaymentResponse().getResult().getPaymentStatusDescription().equals("ERROR")) {
//                todo trovare nei log casi di questo tipo
//                restituire json che mostri errore in popup
            }

			//Tolentino - Inizio
			if (isMAUserMemorizzaCarta(request)) {
                logger.info("MAPayment - [CheckinInitPaymentServlet] [managementSuccess 2D] storing payment requested");
				checkinSession.registerCreditCard(request);
				checkinSession.reloadCreditCard(request);
			}
			//Tolentino - Fine

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private List<FraudNetParam> addFraudnetInfo(Map<String, String> httpHeaders, String jscString) {
        List<FraudNetParam> listFraudNetParam = new ArrayList<>();

        if (httpHeaders != null) {
            for (String key : httpHeaders.keySet()) {
                FraudNetParam param = new FraudNetParam();
                param.setName(key);
                param.setValue(httpHeaders.get(key));
                listFraudNetParam.add(param);
            }
        }
        /*Device fingerprint id*/
        FraudNetParam param = new FraudNetParam();
        param.setName("DEVICEFINGERPRINTID");
        param.setValue(jscString);
        listFraudNetParam.add(param);

        return listFraudNetParam;
    }

    /**
     * Return a map that contains all HTTP Headers present in the request.
     *
     * @param request
     * @param separator used to concat in case of headers with multiple values
     * @return
     */
    @SuppressWarnings("unchecked")
    private Map<String, String> getHttpHeaders(SlingHttpServletRequest request, String separator) {
        // TODO Auto-generated method stub
        Map<String, String> httpHeaders = new HashMap<String, String>();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            if (headerName != null && headerName.equals("Accept")) { //TODO eliminare queste assegnazioni
                String headerValue = "/";
                httpHeaders.put(headerName, headerValue);
            } else if (headerName != null && headerName.equals("Content-Type")) { //valori provvisori per test in locale
                String headerValue = "text/xml; charset=utf-8";
                httpHeaders.put(headerName, headerValue);
            } else if (headerName != null && headerName.length() > 0) {
                Enumeration<String> headerValues = request.getHeaders(headerName);
                String headerValue = concatEnumeration(headerValues, separator);
                /*Insert into map*/
                httpHeaders.put(headerName, headerValue);
            }
        }
        return httpHeaders;
    }

    private String concatEnumeration(Enumeration<String> enumeration, String separator) {
        String concat = "";
        while (enumeration.hasMoreElements()) {
            String value = enumeration.nextElement();
            concat += value + separator;
        }
        /*remove trailing separator*/
        if (concat.length() > 0) {
            concat = concat.substring(0, concat.lastIndexOf(separator));
        }
        return concat;
    }

    //Tolentino - Inizio
    //public String getHTMLAncillary(SlingHttpServletRequest request, CheckinSessionContext ctx, List<PnrRender> pnrRenders, CheckinInitPaymentResponse result, String card, String currency, I18n i18n) {
   	public String getHTMLAncillary(SlingHttpServletRequest request, CheckinSessionContext ctx, List<PnrRender> pnrRenders, CheckinInitPaymentResponse result, String card, String cardNumber, String currency, I18n i18n) {
    //Tolentino - Fine
        try {
            String stru = "";
            stru += getCssHead();
            //Tolentino - Inizio
            //stru += getBodyMailAncillary(request, ctx, pnrRenders, result, card, currency, i18n);
            stru += getBodyMailAncillary(request, ctx, pnrRenders, result, card, cardNumber, currency, i18n);
            //Tolentino - Fine
            return stru;
        } catch (Exception e) {
            return i18n.get("checkin.mail.notchecked.success.error");
        }
    }

    //Tolentino - Inizio
    //private String getBodyMailAncillary(SlingHttpServletRequest request, CheckinSessionContext ctx, List<PnrRender> pnrRenders, CheckinInitPaymentResponse result, String card, String currency, I18n i18n) {
   	private String getBodyMailAncillary(SlingHttpServletRequest request, CheckinSessionContext ctx, List<PnrRender> pnrRenders, CheckinInitPaymentResponse result, String card, String cardNumber, String currency, I18n i18n) {
    //Tolentino - Fine
        String stru = "";
        for (PnrRender pnrRender : pnrRenders) {
            if (pnrRender.getNumber().equals(ctx.pnrSelectedListDataRender.get(0).getNumber())) {

                stru += " <div style=\"height: 20px;background-color: #f8f8f8\"></div>\n" +
                        "\n" +
                        "    <table class=\"row\"\n" +
                        "           style=\"padding-top: 20px;background-color: #f8f8f8;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                        "        <tbody>\n" +
                        "        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "            <th class=\"small-12 large-12 columns first last\"\n" +
                        "                style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:20px;padding-right:20px;text-align:left;width:580px\">\n" +
                        "                <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\"> ";
                stru += getFlights(result.getInitPaymentResponse().getAuthorizationResult(), request, ctx, pnrRender, result, currency, i18n);
                stru += space();
                int insurancePrice = 0;

                if (result.getInitPaymentResponse().getAuthorizationResult().getInsurancePayment() != null || ctx.pnrListData.get(0).getInsurance()) {
                    insurancePrice = result.getInitPaymentResponse().getAuthorizationResult().getInsurancePayment().getInsuranceAmount();
                }

                //Tolentino - Inizio
                //stru += getPaymentMethod(result.getInitPaymentResponse().getAuthorizationResult(), i18n.get("checkin.mail.ancillary.totale"), currency, ctx.totalPay + insurancePrice, card, request, i18n);
                stru += getPaymentMethod(result.getInitPaymentResponse().getAuthorizationResult(), i18n.get("checkin.mail.ancillary.totale"), currency, ctx.totalPay + insurancePrice, card, cardNumber, request, i18n);
           	    //Tolentino - Fine
                stru +=
                        "            </th>\n" +
                                "        </tr>\n" +
                                "        </table>\n" +
                                "        </th>\n" +
                                "        </tr>\n" +
                                "        </tbody>\n" +
                                "    </table>";
            }

        }
        return stru;
    }

    private String getFlights(AuthorizationResult authorizationResult, SlingHttpServletRequest request, CheckinSessionContext ctx, PnrRender pnrRender, CheckinInitPaymentResponse result, String currency, I18n i18n) {
        String stru = "";
        for (FlightsRender flightsRender : pnrRender.getFlightsRender()) {

            stru += "<p class=\"info-title\" style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">";

            if (pnrRender.getFlightsRender().size() > 1) {
                if (flightsRender.equals(pnrRender.getFlightsRender().get(0))) {
                    stru += "<strong>" + i18n.get("checkin.mail.ancillary.service") + " - " + i18n.get("checkin.mail.ancillary.voloAndata") + "</strong>";
                } else {
                    stru += "<strong>" + i18n.get("checkin.mail.ancillary.service") + " - " + i18n.get("checkin.mail.ancillary.voloRitorno") + "</strong>";
                }
            } else {
                stru += "<strong>" + i18n.get("checkin.mail.ancillary.service") + " - " + i18n.get("checkin.mail.ancillary.voloAndata") + "</strong>";
            }

            stru += "</p>\n";
            stru += space();
            stru += "\n" +
                    "                                <!-- Inizio titolo -->\n" +
                    "\n" +
                    "                                <table class=\"wrapper\" align=\"center\" bgcolor=\"#006643\"\n" +
                    "                                       style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                    "                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                    "                                        <td class=\"wrapper-inner\"\n" +
                    "                                            style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
                    "                                            <p class=\"info-header\"\n" +
                    "                                               style=\"Margin:10px;Margin-bottom:10px;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin:10px;margin-bottom:10px;padding:0;text-align:left\">\n" +
                    "                                                " + flightsRender.getOrigin().getCity() + "\n" +
                    "                                                <strong>" + flightsRender.getOrigin().getCode() + "</strong> - " + flightsRender.getDestination().getCity() + "\n" +
                    "                                                <strong>" + flightsRender.getDestination().getCode() + "</strong>\n";
            if (flightsRender.getSegments().size() > 1) {
                stru += "<span class=\"small\" style=\"font-size:14px\">" + (flightsRender.getSegments().size() - 1);
                if (flightsRender.getSegments().size() == 1) {
                    stru += "\" scalo.label</span>\"";
                } else {
                    stru += "scali.label</span>";
                }
            }

            stru += "</p>\n" +
                    "                                        </td>\n" +
                    "                                    </tr>\n" +
                    "                                </table>\n" +
                    "\n" +
                    "                                <!-- Fine titolo -->\n" +
                    "\n" +
                    "                                <table class=\"wrapper\" align=\"center\" bgcolor=\"#ffffff\"\n" +
                    "                                       style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                    "                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                    "                                        <td class=\"wrapper-inner\"\n" +
                    "                                            style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
                    "                                            <div class=\"info\" style=\"Margin:10px;margin:10px\">\n" +
                    "\n" +
                    "                                                ";
            for (SegmentRender segment : flightsRender.getSegments()) {
                stru += "<!-- Inizio segmento -->";
                stru += space();
                stru += "<table class=\"row\"\n" +
                        "                                                           style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                        <tbody>\n" +
                        "                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                            <th class=\"small-12 large-6 columns first\"\n" +
                        "                                                                style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:50%\">\n" +
                        "                                                                <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                        "                                                                            <div class=\"arr-dep\"\n" +
                        "                                                                                 style=\"Margin:0 0 25px;margin:0 0 25px\">\n" +
                        "                                                                                <strong>" + i18n.get("checkin.mail.ancillary.partenza") + "</strong>\n" +
                        "                                                                            </div>\n" +
                        "                                                                            <table class=\"row no-padding m-10\"\n" +
                        "                                                                                   style=\"Margin:0 0 10px;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 10px;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                <tbody>\n" +
                        "                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                    <th class=\"small-12 large-12 columns first last\"\n" +
                        "                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%\">\n" +
                        "                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">\n" +
                        "                                                                          <span class=\"small\" style=\"font-size:14px\">" + segment.getOrigin().getCity() + "\n" +
                        "                                                                            <strong>" + segment.getOrigin().getCity() + "</strong>\n" +
                        "                                                                          </span>\n" +
                        "                                                                                                </th>\n" +
                        "                                                                                                <th class=\"expander\"\n" +
                        "                                                                                                    style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0\"></th>\n" +
                        "                                                                                            </tr>\n" +
                        "                                                                                        </table>\n" +
                        "                                                                                    </th>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                                </tbody>\n" +
                        "                                                                            </table>\n" +
                        "                                                                            <table class=\"row\"\n" +
                        "                                                                                   style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                <tbody>\n" +
                        "                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                    <th class=\"small-2 large-2 columns first\"\n" +
                        "                                                                                        valign=\"middle\"\n" +
                        "                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
                        "                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                        "                                                                                                    <img width=\"36\"\n" +
                        "                                                                                                         height=\"20\"\n" +
                        "                                                                                                         src=" + alitaliaMailUtils.getIconPath("departure", request) +
                        "                                                                                                         alt=\"departure\"\n" +
                        "                                                                                                         style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
                        "                                                                                                </th>\n" +
                        "                                                                                            </tr>\n" +
                        "                                                                                        </table>\n" +
                        "                                                                                    </th>\n" +
                        "                                                                                    <th class=\"small-10 large-10 columns last\"\n" +
                        "                                                                                        valign=\"middle\"\n" +
                        "                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
                        "                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                        "                                                                                                    " + segment.getDepartureDate() + "\n" +
                        "                                                                                                </th>\n" +
                        "                                                                                            </tr>\n" +
                        "                                                                                        </table>\n" +
                        "                                                                                    </th>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                                </tbody>\n" +
                        "                                                                            </table>\n" +
                        "                                                                            <table class=\"row\"\n" +
                        "                                                                                   style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                <tbody>\n" +
                        "                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                    <th class=\"small-2 large-2 columns first\"\n" +
                        "                                                                                        valign=\"middle\"\n" +
                        "                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
                        "                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                        "                                                                                                    <img width=\"29\"\n" +
                        "                                                                                                         height=\"20\"\n" +
                        "                                                                                                         src=" + alitaliaMailUtils.getIconPath("clock", request) +
                        "                                                                                                         alt=\"clock\"\n" +
                        "                                                                                                         style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
                        "                                                                                                </th>\n" +
                        "                                                                                            </tr>\n" +
                        "                                                                                        </table>\n" +
                        "                                                                                    </th>\n" +
                        "                                                                                    <th class=\"small-10 large-10 columns last\"\n" +
                        "                                                                                        valign=\"middle\"\n" +
                        "                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
                        "                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                        "                                                                                                    " + segment.getDepartureTime() + "\n" +
                        "                                                                                                </th>\n" +
                        "                                                                                            </tr>\n" +
                        "                                                                                        </table>\n" +
                        "                                                                                    </th>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                                </tbody>\n" +
                        "                                                                            </table>\n" +
                        "                                                                        </th>\n" +
                        "                                                                    </tr>\n" +
                        "                                                                </table>\n" +
                        "                                                            </th>\n" +
                        "                                                            <th class=\"small-12 large-6 columns last\"\n" +
                        "                                                                style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:50%\">\n" +
                        "                                                                <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                        "                                                                            <div class=\"arr-dep\"\n" +
                        "                                                                                 style=\"Margin:0 0 25px;margin:0 0 25px\">\n" +
                        "                                                                                <strong>" + i18n.get("checkin.mail.ancillary.arrivo") + "</strong>\n" +
                        "                                                                            </div>\n" +
                        "                                                                            <table class=\"row no-padding m-10\"\n" +
                        "                                                                                   style=\"Margin:0 0 10px;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 10px;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                <tbody>\n" +
                        "                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                    <th class=\"small-12 large-12 columns first last\"\n" +
                        "                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%\">\n" +
                        "                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">\n" +
                        "                                                                          <span class=\"small\" style=\"font-size:14px\">" + segment.getDestination().getCity() + "\n" +
                        "                                                                            <strong>" + segment.getDestination().getCode() + "</strong>\n" +
                        "                                                                          </span>\n" +
                        "                                                                                                </th>\n" +
                        "                                                                                                <th class=\"expander\"\n" +
                        "                                                                                                    style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0\"></th>\n" +
                        "                                                                                            </tr>\n" +
                        "                                                                                        </table>\n" +
                        "                                                                                    </th>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                                </tbody>\n" +
                        "                                                                            </table>\n" +
                        "                                                                            <table class=\"row\"\n" +
                        "                                                                                   style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                <tbody>\n" +
                        "                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                    <th class=\"small-2 large-2 columns first\"\n" +
                        "                                                                                        valign=\"middle\"\n" +
                        "                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
                        "                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                        "                                                                                                    <img width=\"36\"\n" +
                        "                                                                                                         height=\"20\"\n" +
                        "                                                                                                         src=" + alitaliaMailUtils.getIconPath("arrival", request) +
                        "                                                                                                         alt=\"arrival\"\n" +
                        "                                                                                                         style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
                        "                                                                                                </th>\n" +
                        "                                                                                            </tr>\n" +
                        "                                                                                        </table>\n" +
                        "                                                                                    </th>\n" +
                        "                                                                                    <th class=\"small-10 large-10 columns last\"\n" +
                        "                                                                                        valign=\"middle\"\n" +
                        "                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
                        "                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                        "                                                                                                    " + segment.getArrivalDate() + "                                                                                           </th>\n" +
                        "                                                                                            </tr>\n" +
                        "                                                                                        </table>\n" +
                        "                                                                                    </th>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                                </tbody>\n" +
                        "                                                                            </table>\n" +
                        "                                                                            <table class=\"row\"\n" +
                        "                                                                                   style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                <tbody>\n" +
                        "                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                    <th class=\"small-2 large-2 columns first\"\n" +
                        "                                                                                        valign=\"middle\"\n" +
                        "                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
                        "                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                        "                                                                                                    <img width=\"29\"\n" +
                        "                                                                                                         height=\"20\"\n" +
                        "                                                                                                         src=" + alitaliaMailUtils.getIconPath("clock", request) +
                        "                                                                                                         alt=\"clock\"\n" +
                        "                                                                                                         style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
                        "                                                                                                </th>\n" +
                        "                                                                                            </tr>\n" +
                        "                                                                                        </table>\n" +
                        "                                                                                    </th>\n" +
                        "                                                                                    <th class=\"small-10 large-10 columns last\"\n" +
                        "                                                                                        valign=\"middle\"\n" +
                        "                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
                        "                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                        "                                                                                                    " + segment.getArrivalTime() + "                                                                                            </th>\n" +
                        "                                                                                            </tr>\n" +
                        "                                                                                        </table>\n" +
                        "                                                                                    </th>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                                </tbody>\n" +
                        "                                                                            </table>\n" +
                        "                                                                        </th>\n" +
                        "                                                                    </tr>\n" +
                        "                                                                </table>\n" +
                        "                                                            </th>\n" +
                        "                                                        </tr>\n" +
                        "                                                        </tbody>\n" +
                        "                                                    </table>\n" +
                        "                                                    <table class=\"row no-padding\"\n" +
                        "                                                           style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                        <tbody>\n" +
                        "                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                            <th class=\"small-12 large-4 columns first\"\n" +
                        "                                                                style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
                        "                                                                <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">\n" +
                        "                                                                            <p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">\n" +
                        "                                                                                " + i18n.get("checkin.mail.ancillary.numeroVolo") +
                        "                                                                                <br>\n" +
                        "                                                                                <strong>" + segment.getAirline() + " " + segment.getFlight() + " </strong>\n" +
                        "                                                                            </p>\n" +
                        "                                                                        </th>\n" +
                        "                                                                    </tr>\n" +
                        "                                                                </table>\n" +
                        "                                                            </th>\n" +
                        "\n";
                String waiting = "", sostaLabel = "";
                if (!segment.getWaiting().equals("00:00:00")) {

                    waiting = segment.getWaiting();
                    sostaLabel = "ore di sosta label";

                }
                stru += "<th \n" +
                        "                                                                class=\"small-12 large-8 columns last\"\n" +
                        "                                                                style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:66.66667%\">\n" +
                        "                                                                <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                        "                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                        "                                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">\n" +
                        "                                                                            <p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">\n" +
                        "                                                                                " + sostaLabel +
                        "                                                                                <br>\n" +
                        "                                                                                <strong>" + waiting + "</strong>\n" +
                        "                                                                            </p>\n" +
                        "                                                                        </th>\n" +
                        "                                                                    </tr>\n" +
                        "                                                                </table>\n" +
                        "                                                            </th>\n" +
                        "                                                        </tr>\n" +
                        "                                                        </tbody>\n" +
                        "                                                    </table>\n" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "                                                <!-- Fine segmento -->";
                stru += row();
            }
            for (TotalAncillaryForPax totalAncillaryForPax : flightsRender.getTotalAncillaryForPax()) {
                for (SegmentRender seg : flightsRender.getSegments()) {
                    for (Passenger passenger : seg.getPassengers()) {
                        if (totalAncillaryForPax.getPassenger().getNome().equals(passenger.getNome()) && totalAncillaryForPax.getPassenger().getCognome().equals(passenger.getCognome())) {
                            if (passenger.getPassengerSeats() != null) {
                                totalAncillaryForPax.setConfortSeat(passenger.getPassengerSeats());
                            }
                        }
                    }
                }
                if (totalAncillaryForPax.getTotalBaggage() > 0 || totalAncillaryForPax.getTotalFast() > 0 || totalAncillaryForPax.getTotalLounge() > 0 || totalAncillaryForPax.getConfortSeat() != null) {
//                if (totalAncillaryForPax.getTotalBaggage() > 0 || totalAncillaryForPax.getTotalFast() > 0 || totalAncillaryForPax.getTotalLounge() > 0) {

                    stru += space();
                    stru += "<!-- Inizio nome pass -->\n" +
                            "\n" +
                            "                                                    <table class=\"row\"\n" +
                            "                                                           style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                            "                                                        <tbody>\n" +
                            "                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                            "                                                            <th class=\"small-12 large-12 columns first last\"\n" +
                            "                                                                style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%\">\n" +
                            "                                                                <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                            "                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                            "                                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                            "                                                                            <p class=\"name\"\n" +
                            "                                                                               style=\"Margin:0;Margin-bottom:10px;color:#006643;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">\n" +
                            "                                                                                <strong>" + totalAncillaryForPax.getPassenger().getNome() + " " + totalAncillaryForPax.getPassenger().getCognome() + "</strong>\n" +
                            "                                                                            </p>\n" +
                            "                                                                        </th>\n" +
                            "                                                                        <th class=\"expander\"\n" +
                            "                                                                            style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0\"></th>\n" +
                            "                                                                    </tr>\n" +
                            "                                                                </table>\n" +
                            "                                                            </th>\n" +
                            "                                                        </tr>\n" +
                            "                                                        </tbody>\n" +
                            "                                                    </table>" +
                            "<!-- Fine nome pass -->";
                    if (totalAncillaryForPax.getConfortSeat() != null) {
                        for (PassengerSeat passengerSeat : totalAncillaryForPax.getConfortSeat()) {
                            stru += " <table class=\"row extra-info no-padding\" style=\"Margin:0 0 20px!important;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 20px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                                    "                                                <tbody>\n" +
                                    "                                                  <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                    "                                                    <th class=\"small-6 large-5 columns first\" style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:41.66667%\">\n" +
                                    "                                                      <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                    "                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                    "                                                          <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                    "                                                            <table class=\"row\" style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                                    "                                                              <tbody>\n" +
                                    "                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                    "                                                                  <th class=\"small-2 large-2 columns first\" valign=\"middle\"\n" +
                                    "                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
                                    "                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                    "                                                                      <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                    "                                                                        <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                    "                                                                          <img width=\"38\" height=\"25\" src=" + alitaliaMailUtils.getIconPath("seat", request) +
                                    "                                                                            alt=\"seat\" style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
                                    "                                                                        </th>\n" +
                                    "                                                                      </tr>\n" +
                                    "                                                                    </table>\n" +
                                    "                                                                  </th>\n" +
                                    "                                                                  <th class=\"small-10 large-10 columns last\" valign=\"middle\"\n" +
                                    "                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
                                    "                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                    "                                                                      <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                    "                                                                        <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">" + i18n.get("checkin.mail.ancillary.seat") + "</th>\n" +
                                    "                                                                      </tr>\n" +
                                    "                                                                    </table>\n" +
                                    "                                                                  </th>\n" +
                                    "                                                                </tr>\n" +
                                    "                                                              </tbody>\n" +
                                    "                                                            </table>\n" +
                                    "                                                          </th>\n" +
                                    "                                                        </tr>\n" +
                                    "                                                      </table>\n" +
                                    "                                                    </th>\n" +
                                    "                                                    <th class=\"small-6 large-4 columns\" valign=\"middle\" style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
                                    "                                                      <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                    "                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                    "                                                          <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                    "                                                            <strong>" + passengerSeat.getNumeroPostoAssegnato() + " (" + passengerSeat.getOrigin() + ")</strong>\n" +
                                    "                                                          </th>\n" +
                                    "                                                        </tr>\n" +
                                    "                                                      </table>\n" +
                                    "                                                    </th>\n" +
                                    "                                                    <th class=\"small-12 large-3 columns last\" valign=\"middle\" style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:25%\">\n" +
                                    "                                                      <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                    "                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                    "                                                          <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">" + "</th>\n" +
                                    "                                                        </tr>\n" +
                                    "                                                      </table>\n" +
                                    "                                                    </th>\n" +
                                    "                                                  </tr>\n" +
                                    "                                                </tbody>\n" +
                                    "                                              </table>";
                        }
                    }
                    if (totalAncillaryForPax.getTotalBaggage() > 0) {
                        stru += "<!-- Inizio Bagaglio extra -->\n" +
                                "\n" +
                                "                                                        <table class=\"row extra-info no-padding\"\n" +
                                "                                                               style=\"Margin:0 0 20px!important;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 20px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                            <tbody>\n" +
                                "                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                <th class=\"small-6 large-5 columns first\"\n" +
                                "                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:41.66667%\">\n" +
                                "                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                <table class=\"row\"\n" +
                                "                                                                                       style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                                    <tbody>\n" +
                                "                                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                                        <th class=\"small-2 large-2 columns first\"\n" +
                                "                                                                                            valign=\"middle\"\n" +
                                "                                                                                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
                                "                                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                                        <img width=\"38\"\n" +
                                "                                                                                                             height=\"25\"\n" +
                                "                                                                                                             src=" + alitaliaMailUtils.getIconPath("bagaglio", request) +
                                "                                                                                                             alt=\"luggage\"\n" +
                                "                                                                                                             style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
                                "                                                                                                    </th>\n" +
                                "                                                                                                </tr>\n" +
                                "                                                                                            </table>\n" +
                                "                                                                                        </th>\n" +
                                "                                                                                        <th class=\"small-10 large-10 columns last\"\n" +
                                "                                                                                            valign=\"middle\"\n" +
                                "                                                                                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
                                "                                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                                        " + i18n.get("checkin.mail.ancillary.bagaglioExtra") +
                                "                                                                                                    </th>\n" +
                                "                                                                                                </tr>\n" +
                                "                                                                                            </table>\n" +
                                "                                                                                        </th>\n" +
                                "                                                                                    </tr>\n" +
                                "                                                                                    </tbody>\n" +
                                "                                                                                </table>\n" +
                                "                                                                            </th>\n" +
                                "                                                                        </tr>\n" +
                                "                                                                    </table>\n" +
                                "                                                                </th>\n" +
                                "                                                                <th class=\"small-6 large-4 columns\"\n" +
                                "                                                                    valign=\"middle\"\n" +
                                "                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
                                "                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                <strong>" + totalAncillaryForPax.getTotalBaggage() + " " + i18n.get("checkin.mail.ancillary.bagaglio") +
                                "                                                                                    </strong>\n" +
                                "                                                                            </th>\n" +
                                "                                                                        </tr>\n" +
                                "                                                                    </table>\n" +
                                "                                                                </th>\n" +
                                "                                                                <th class=\"small-12 large-3 columns last\"\n" +
                                "                                                                    valign=\"middle\"\n" +
                                "                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:25%\">\n" +
                                "                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                " +
                                "                                                                            </th>\n" +
                                "                                                                        </tr>\n" +
                                "                                                                    </table>\n" +
                                "                                                                </th>\n" +
                                "                                                            </tr>\n" +
                                "                                                            </tbody>\n" +
                                "                                                        </table>\n" +
                                "\n" +
                                "                                                        <!-- Fine Bagaglio extra -->";


                    }
                    if (totalAncillaryForPax.getTotalFast() > 0) {
                        stru += "<!-- Inizio fast track -->\n" +
                                "\n" +
                                "                                                        <table class=\"row extra-info no-padding\"\n" +
                                "                                                               style=\"Margin:0 0 20px!important;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 20px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                            <tbody>\n" +
                                "                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                <th class=\"small-6 large-5 columns first\"\n" +
                                "                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:41.66667%\">\n" +
                                "                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                <table class=\"row\"\n" +
                                "                                                                                       style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                                    <tbody>\n" +
                                "                                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                                        <th class=\"small-2 large-2 columns first\"\n" +
                                "                                                                                            valign=\"middle\"\n" +
                                "                                                                                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
                                "                                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                                        <img width=\"38\"\n" +
                                "                                                                                                             height=\"26\"\n" +
                                "                                                                                                             src=" + alitaliaMailUtils.getIconPath("fast", request) +
                                "                                                                                                             alt=\"fast-track\"\n" +
                                "                                                                                                             style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
                                "                                                                                                    </th>\n" +
                                "                                                                                                </tr>\n" +
                                "                                                                                            </table>\n" +
                                "                                                                                        </th>\n" +
                                "                                                                                        <th class=\"small-10 large-10 columns last\"\n" +
                                "                                                                                            valign=\"middle\"\n" +
                                "                                                                                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
                                "                                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                                        " + i18n.get("checkin.mail.ancillary.fastTrack") + "\n" +
                                "                                                                                                    </th>\n" +
                                "                                                                                                </tr>\n" +
                                "                                                                                            </table>\n" +
                                "                                                                                        </th>\n" +
                                "                                                                                    </tr>\n" +
                                "                                                                                    </tbody>\n" +
                                "                                                                                </table>\n" +
                                "                                                                            </th>\n" +
                                "                                                                        </tr>\n" +
                                "                                                                    </table>\n" +
                                "                                                                </th>\n" +
                                "                                                                <th class=\"small-6 large-4 columns\"\n" +
                                "                                                                    valign=\"middle\"\n" +
                                "                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
                                "                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                <strong>" + totalAncillaryForPax.getTotalFast() + " " + i18n.get("checkin.mail.ancillary.fastTrack.ingressi") + "</strong>\n" +
                                "                                                                            </th>\n" +
                                "                                                                        </tr>\n" +
                                "                                                                    </table>\n" +
                                "                                                                </th>\n" +
                                "                                                                <th class=\"small-12 large-3 columns last\"\n" +
                                "                                                                    valign=\"middle\"\n" +
                                "                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:25%\">\n" +
                                "                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                " +
                                "                                                                            </th>\n" +
                                "                                                                        </tr>\n" +
                                "                                                                    </table>\n" +
                                "                                                                </th>\n" +
                                "                                                            </tr>\n" +
                                "                                                            </tbody>\n" +
                                "                                                        </table>\n" +
                                "\n" +
                                "                                                        <!-- Fine fast track -->";
                    }
                    if (totalAncillaryForPax.getTotalLounge() > 0) {

                        stru += " <!-- Inizio lounge -->\n" +
                                "\n" +
                                "                                                        <table class=\"row extra-info no-padding\"\n" +
                                "                                                               style=\"Margin:0 0 20px!important;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 20px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                            <tbody>\n" +
                                "                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                <th class=\"small-6 large-5 columns first\"\n" +
                                "                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:41.66667%\">\n" +
                                "                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                <table class=\"row\"\n" +
                                "                                                                                       style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                                    <tbody>\n" +
                                "                                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                                        <th class=\"small-2 large-2 columns first\"\n" +
                                "                                                                                            valign=\"middle\"\n" +
                                "                                                                                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
                                "                                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                                        <img width=\"38\"\n" +
                                "                                                                                                             height=\"25\"\n" +
                                "                                                                                                             src=" + alitaliaMailUtils.getIconPath("lounge", request) +
                                "                                                                                                             alt=\"lounge\"\n" +
                                "                                                                                                             style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
                                "                                                                                                    </th>\n" +
                                "                                                                                                </tr>\n" +
                                "                                                                                            </table>\n" +
                                "                                                                                        </th>\n" +
                                "                                                                                        <th class=\"small-10 large-10 columns last\"\n" +
                                "                                                                                            valign=\"middle\"\n" +
                                "                                                                                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
                                "                                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                                        " + i18n.get("checkin.mail.ancillary.lounge") + "\n" +
                                "                                                                                                    </th>\n" +
                                "                                                                                                </tr>\n" +
                                "                                                                                            </table>\n" +
                                "                                                                                        </th>\n" +
                                "                                                                                    </tr>\n" +
                                "                                                                                    </tbody>\n" +
                                "                                                                                </table>\n" +
                                "                                                                            </th>\n" +
                                "                                                                        </tr>\n" +
                                "                                                                    </table>\n" +
                                "                                                                </th>\n" +
                                "                                                                <th class=\"small-6 large-4 columns\"\n" +
                                "                                                                    valign=\"middle\"\n" +
                                "                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
                                "                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                <strong>" + totalAncillaryForPax.getTotalLounge() + " " + i18n.get("checkin.mail.ancillary.lounge.acquistati") + " </strong>\n" +
                                "                                                                            </th>\n" +
                                "                                                                        </tr>\n" +
                                "                                                                    </table>\n" +
                                "                                                                </th>\n" +
                                "                                                                <th class=\"small-12 large-3 columns last\"\n" +
                                "                                                                    valign=\"middle\"\n" +
                                "                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:25%\">\n" +
                                "                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                                "                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                                "                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                                "                                                                                " +
                                "                                                                            </th>\n" +
                                "                                                                        </tr>\n" +
                                "                                                                    </table>\n" +
                                "                                                                </th>\n" +
                                "                                                            </tr>\n" +
                                "                                                            </tbody>\n" +
                                "                                                        </table>\n" +
                                "\n" +
                                "                                                        <!-- Fine lounge-->";
                    }
                }
            }
            stru += "\n" +
                    "\n" +
                    "\n" +
                    "                                                <!-- Fine pax e ancillary acquistati-->\n" +
                    "\n" +
                    "                                            </div>\n" +
                    "                                        </td>\n" +
                    "                                    </tr>\n" +
                    "</table>\n" +
                    "\n" +
                    "                            <!-- Fine volo andata/ritorno -->\n" +
                    "\n";
            stru += space();

        }
        if (authorizationResult.getInsurancePayment() != null || ctx.pnrListData.get(0).getInsurance()) {
            stru += getInsurance(authorizationResult, currency, request, i18n);

        }
        stru += space();
        stru += getLabel(i18n.get("checkin.mail.ancillary.cart"));
        for (AzPaxPayedTP azPaxPayedTP : result.getInitPaymentResponse().getaZPayedTPResponse().getAzPaxPayedTP()) {
            stru += getCart(azPaxPayedTP, authorizationResult, currency, i18n);
        }
        return stru;
    }

    private String getCssHead() {
        String stru = "<style>\n" +
                "   img {width : 20px; height : 20px;} " +
                "@media only screen {\n" +
                "      html {\n" +
                "        min-height: 100%;\n" +
                "        background: #f3f3f3\n" +
                "      }\n" +
                "    }\n" +
                "\n" +
                "    @media only screen and (max-width:620px) {\n" +
                "      table.body img {\n" +
                "        width: auto;\n" +
                "        height: auto\n" +
                "      }\n" +
                "      table.body center {\n" +
                "        min-width: 0 !important\n" +
                "      }\n" +
                "      table.body .container {\n" +
                "        width: 95% !important\n" +
                "      }\n" +
                "      table.body .columns {\n" +
                "        height: auto !important;\n" +
                "        -moz-box-sizing: border-box;\n" +
                "        -webkit-box-sizing: border-box;\n" +
                "        box-sizing: border-box;\n" +
                "        padding-left: 20px !important;\n" +
                "        padding-right: 20px !important\n" +
                "      }\n" +
                "      table.body .columns .columns {\n" +
                "        padding-left: 0 !important;\n" +
                "        padding-right: 0 !important\n" +
                "      }\n" +
                "      th.small-2 {\n" +
                "        display: inline-block !important;\n" +
                "        width: 16.66667% !important\n" +
                "      }\n" +
                "      th.small-6 {\n" +
                "        display: inline-block !important;\n" +
                "        width: 50% !important\n" +
                "      }\n" +
                "      th.small-10 {\n" +
                "        display: inline-block !important;\n" +
                "        width: 83.33333% !important\n" +
                "      }\n" +
                "      th.small-12 {\n" +
                "        display: inline-block !important;\n" +
                "        width: 100% !important\n" +
                "      }\n" +
                "      .columns th.small-12 {\n" +
                "        display: block !important;\n" +
                "        width: 100% !important\n" +
                "      }\n" +
                "      table.menu {\n" +
                "        width: 100% !important\n" +
                "      }\n" +
                "      table.menu td,\n" +
                "      table.menu th {\n" +
                "        width: auto !important;\n" +
                "        display: inline-block !important\n" +
                "      }\n" +
                "      table.menu.vertical td,\n" +
                "      table.menu.vertical th {\n" +
                "        display: block !important\n" +
                "      }\n" +
                "      table.menu[align=center] {\n" +
                "        width: auto !important\n" +
                "      }\n" +
                "    }\n" +
                "  </style>";
        return stru;
    }

    //Tolentino - Inizio
    //private String getPaymentMethod(AuthorizationResult authorizationResult, String totaleLabel, String currency, String totalePrice, String card, SlingHttpServletRequest request, I18n i18n) {
   	private String getPaymentMethod(AuthorizationResult authorizationResult, String totaleLabel, String currency, String totalePrice, String card, String cardNumber, SlingHttpServletRequest request, I18n i18n) {
    //Tolentino - Fine
        String[] price = new String[2];
        String decimalSeparator = new HierarchyNodeInheritanceValueMap(request.getResource().getParent()).getInherited("decimalSeparator", String.class);
        price = totalePrice.split(",");
        if(price.length > 1) {
            totalePrice = price[0] + decimalSeparator + price[1].substring(0, 2);
        } else {
            totalePrice = totalePrice + decimalSeparator + "00";
        }
        String stru = "<table class=\"row payment\"\n" +
                "                                                                       style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                "                                                                    <tbody>\n" +
                "                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                                                                        <th class=\"small-12 large-2 columns first\"\n" +
                "                                                                            style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
                "                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                "                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                                                                                    <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                "                                                                                        <center data-parsed=\"\"\n" +
                "                                                                                                style=\"min-width:none!important;width:100%\">\n" +
                "                                                                                            <img width='63' height='49'" +
                "                                                                                                 src=" + alitaliaMailUtils.getIconPath(card, request) +
                "                                                                                                 alt=\"mastercard\"\n" +
                "                                                                                                 align=\"center\"\n" +
                "                                                                                                 class=\"float-center\"\n" +
                "                                                                                                 style=\"-ms-interpolation-mode:bicubic;Margin:0 auto;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto\">\n" +
                "                                                                                        </center>\n" +
                "                                                                                    </th>\n" +
                "                                                                                </tr>\n" +
                "                                                                            </table>\n" +
                "                                                                        </th>\n" +
                "                                                                        <th class=\"small-12 large-6 columns\"\n" +
                "                                                                            style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:50%\">\n" +
                "                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                "                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                                                                                    <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                "                                                                                        <p class=\"text-center\"\n" +
                "                                                                                           style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:center\">" +
                "" + i18n.get("checkin.mail.ancillary.pagamento") +
                "                                                                                            <strong class=\"card\"\n" +
                "                                                                                                    style=\"font-size:20px\">" + card + "</strong>\n" +
                "                                                                                            <br>\n" +
                "                                                                                            <strong class=\"card\"\n" +
                //Tolentino - inizio
                //"                                                                                                     style=\"font-size:20px\">" + request.getParameter("cardNumber").substring(0, request.getParameter("cardNumber").length() - 4).replaceAll("([0-9])", "*") + request.getParameter("cardNumber").substring(request.getParameter("cardNumber").length() - 4, request.getParameter("cardNumber").length()) + "</strong>\n" +
                "                                                                                                     style=\"font-size:20px\">" + cardNumber + "</strong>\n" +
                //Tolentino - fine
                "                                                                                        </p>\n" +
                "                                                                                    </th>\n" +
                "                                                                                </tr>\n" +
                "                                                                            </table>\n" +
                "                                                                        </th>\n" +
                "                                                                        <th class=\"small-12 large-4 columns last\"\n" +
                "                                                                            style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
                "                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                "                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                                                                                    <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                "                                                                                        <table class=\"wrapper\"\n" +
                "                                                                                               align=\"center\"\n" +
                "                                                                                               bgcolor=\"#ffffff\"\n" +
                "                                                                                               style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                "                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                                                                                                <td class=\"wrapper-inner\"\n" +
                "                                                                                                    style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
                "                                                                                                    <div class=\"total\">\n" +
                "                                                                                                        <p class=\"text-center\"\n" +
                "                                                                                                           style=\"Margin:10px;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:10px;margin-bottom:10px;padding:0;text-align:center\">\n" +
                "                                                                                                            " + totaleLabel +
                "                                                                                                            <br>\n" +
                "                                                                                                            <strong style=\"color:#006643;font-size:20px\">" + currency +
                "                                                                                                                " + totalePrice + "</strong>\n" +
                "                                                                                                        </p>\n" +
                "                                                                                                    </div>\n" +
                "                                                                                                </td>\n" +
                "                                                                                            </tr>\n" +
                "                                                                                        </table>\n" +
                "                                                                                    </th>\n" +
                "                                                                                </tr>\n" +
                "                                                                            </table>\n" +
                "                                                                        </th>\n" +
                "                                                                    </tr>\n" +
                "                                                                    </tbody>\n" +
                "                                                                </table>";
        return stru;
    }

    private String space() {
        String stru = "<table class=\"spacer\"\n" +
                "       style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                "    <tbody>\n" +
                "    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "        <td height=\"30px\"\n" +
                "            style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:30px;font-weight:400;hyphens:auto;line-height:30px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
                "            &#xA0;\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    </tbody>\n" +
                "</table>";
        return stru;
    }

    private String getLabel(String string) {

        String stru = "<p class=\"info-title\"\n" +
                "   style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">\n" +
                "    <strong>" + string + "</strong>\n" +
                "</p>";
        return stru;

    }

    private String getCart(AzPaxPayedTP azPaxPayedTP, AuthorizationResult authorizationResult, String currency, I18n i18n) {
        Boolean insurance = false;
        if (authorizationResult.getInsurancePayment() != null) {
            insurance = true;
        }
        String stru = "<table class=\"wrapper\" align=\"center\" bgcolor=\"#ffffff\"\n" +
                "       style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                "    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "        <td class=\"wrapper-inner\"\n" +
                "            style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
                "            <div class=\"info\" style=\"Margin:10px;margin:10px\">";
        stru += getCartName(azPaxPayedTP.getFirstName(), azPaxPayedTP.getLastName());
        stru += row();
        stru += space();
        for (AzAncillaryPayedTP azAncillaryPayedTP : azPaxPayedTP.getAzAncillaryPayedTP()) {
            stru += getAncillary(azAncillaryPayedTP.getNumberOfItems(), azAncillaryPayedTP.getGroup(), azAncillaryPayedTP.getAzSegmentPayedTP().get(0).getEmdNumber(), currency, azAncillaryPayedTP.getPrice(), i18n);
        }
        if (insurance) {
            stru += getAncillary("", "assicurazione", "", currency, String.valueOf(authorizationResult.getInsurancePayment().getInsuranceAmount()), i18n);
        }
        stru += space();
        stru += "</div>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "</table>";


        return stru;
    }

    private String getCartName(String nome, String cognome) {
        String stru = "<table class=\"row no-padding\"\n" +
                "                       style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                "                    <tbody>\n" +
                "                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                        <th class=\"small-12 large-12 columns first last\"\n" +
                "                            style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%\">\n" +
                "                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                "                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                                    <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">\n" +
                "                                        <p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">\n" +
                "                                                              <span class=\"name\" style=\"color:#006643;font-size:20px\">\n" +
                "                                                                <strong>" + nome + " " + cognome + "</strong>\n" +
                "                                                              </span>\n" +
                "                                            <br>\n" +
                "                                            <span class=\"small\"\n" +
                "                                                  style=\"font-size:14px\"></span>\n" +
                "                                        </p>\n" +
                "                                    </th>\n" +
                "                                    <th class=\"expander\"\n" +
                "                                        style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0\"></th>\n" +
                "                                </tr>\n" +
                "                            </table>\n" +
                "                        </th>\n" +
                "                    </tr>\n" +
                "                    </tbody>\n" +
                "                </table>";
        return stru;
    }

    private String row() {
        String stru = "                <hr style=\"border:1px solid #e6e6e6\">\n";
        return stru;
    }

    private String getAncillary(String quantita, String desctizione, String codice, String currency, String prezzo, I18n i18n) {
        String stru = " <table class=\"row extra-info no-padding\"\n" +
                "                       style=\"Margin:0 0 20px!important;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 20px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
                "                    <tbody>\n" +
                "                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                        <th class=\"small-12 large-6 columns first\"\n" +
                "                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:50%\">\n" +
                "                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                "                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                "                                        <strong>" + quantita + "</strong> " + i18n.get(alitaliaMailUtils.getAncillaryLabel(desctizione)) +
                "                                    </th>\n" +
                "                                </tr>\n" +
                "                            </table>\n" +
                "                        </th>\n" +
                "                        <th class=\"small-6 large-4 columns\"\n" +
                "                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
                "                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                "                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n"
                + codice + "" +
                "</th>\n" +
                "                                </tr>\n" +
                "                            </table>\n" +
                "                        </th>\n" +
                "                        <th class=\"small-6 large-6 columns last\"\n" +
                "                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:50%\">\n" +
                "                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                "                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
                currency + " " + prezzo + "</th>\n" +
                "                                </tr>\n" +
                "                            </table>\n" +
                "                        </th>\n" +
                "                    </tr>\n" +
                "                    </tbody>\n" +
                "                </table>";
        return stru;
    }

    private String getInsurance(AuthorizationResult authorizationResult, String currency, SlingHttpServletRequest request, I18n i18n) {
        String stru = "<table class=\"row insurance\" style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;    background-color: rgb(255, 255, 255);\">\n" +
                "                                              <tbody>\n" +
                "                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                                                  <th class=\"small-12 large-1 columns first\" style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-top:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:8.33333%\">\n" +
                "                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                "                                                      <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                "                                                          <center data-parsed=\"\" style=\"min-width:none!important;width:100%\">\n" +
                "                                                            <img width=\"17\" height=\"22\" src='" + alitaliaMailUtils.getIconPath("assicurazione", request) + "' alt=\"insurance\"\n" +
                "                                                              align=\"center\" class=\"float-center\" style=\"-ms-interpolation-mode:bicubic;Margin:0 auto;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto\">\n" +
                "                                                          </center>\n" +
                "                                                        </th>\n" +
                "                                                      </tr>\n" +
                "                                                    </table>\n" +
                "                                                  </th>\n" +
                "                                                  <th class=\"small-12 large-7 columns\" valign=\"middle\" style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-top:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:58.33333%\">\n" +
                "                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                "                                                      <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                "                                                          <p class=\"text-center\" style=\"Margin:0!important;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0!important;margin-bottom:10px;padding:0;text-align:center\">\n" +
                "                                                            <strong style=\"color:#006643\">" + i18n.get("checkin.mail.ancillary.insurance") + "</strong> (" + i18n.get("checkin.mail.ancillary.insurance.forAllPax") + ")</p>\n" +
                "                                                        </th>\n" +
                "                                                      </tr>\n" +
                "                                                    </table>\n" +
                "                                                  </th>\n" +
                "                                                  <th class=\"small-12 large-4 columns last\" valign=\"middle\" style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-top:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
                "                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                "                                                      <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
                "                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
                "                                                          <p class=\"text-center\" style=\"Margin:0!important;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0!important;margin-bottom:10px;padding:0;text-align:center\">" + (authorizationResult.getInsurancePayment() != null ? currency + " " + authorizationResult.getInsurancePayment().getInsuranceAmount() : "") + "</p>\n" +
                "                                                        </th>\n" +
                "                                                      </tr>\n" +
                "                                                    </table>\n" +
                "                                                  </th>\n" +
                "                                                </tr>\n" +
                "                                              </tbody>\n" +
                "                                            </table>\n" +
                "                                          </td>\n" +
                "                                        </tr>\n" +
                "                                      </table>";
        return stru;
    }

    private void setNewPassengerSeat(List<ComfortSeat> comfortSeats, CheckinSessionContext ctx, SlingHttpServletRequest request) {
        for (Pnr pnrRender : ctx.pnrListData) {
            for (Flight flightsRender : pnrRender.getFlights()) {
                if (!comfortSeats.isEmpty() && pnrRender.getNumber().equals(ctx.pnrSelectedListDataRender.get(0).getNumber())) {
                    for (ComfortSeat comfortSeat : comfortSeats) {
                        for (Segment segmentRender : flightsRender.getSegments()) {
                            if (segmentRender.getFlight().equals(comfortSeat.getFlightNumber())) {
                                for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passenger : segmentRender.getPassengers()) {
                                    if (passenger.getNome().equals(comfortSeat.getNome()) && passenger.getCognome().equals(comfortSeat.getCognome())) {
                                        passenger.setNumeroPostoAssegnato(comfortSeat.getSeatNumber());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void SetEmdAfterPaymnt(CheckinInitPaymentResponse result, CheckinSessionContext ctx) {

        for (AzPaxPayedTP azPaxPayedTP : result.getInitPaymentResponse().getaZPayedTPResponse().getAzPaxPayedTP()){
            for(AzAncillaryPayedTP azAncillaryPayedTP : azPaxPayedTP.getAzAncillaryPayedTP()){
                for(AzSegmentPayedTP azSegmentPayedTP : azAncillaryPayedTP.getAzSegmentPayedTP()){
                    for(Pnr pnr : ctx.pnrListData){
                        if (pnr.getNumber().equals(ctx.pnrSelectedListDataRender.get(0).getNumber())){
                            for(Flight flight : pnr.getFlights()){
                                for(Segment segment : flight.getSegments()){
                                    if(segment.getFlight().equals(azSegmentPayedTP.getFlightNumber())){
                                        for(Passenger passenger : segment.getPassengers() ){
                                            if (passenger.getNome().toUpperCase().equals(azPaxPayedTP.getFirstName().toUpperCase()) && passenger.getCognome().toUpperCase().equals(azPaxPayedTP.getLastName().toUpperCase())){
                                                Emd emd = new Emd();
                                                emd.setNumber(azSegmentPayedTP.getEmdNumber());
                                                emd.setGroup(setGroupEmd(azAncillaryPayedTP.getGroup()));
                                                emd.setRficCode(azAncillaryPayedTP.getRficCode());
                                                emd.setRficSubcode(azAncillaryPayedTP.getRficSubcode());
                                                emd.setPdcSeat(ctx.comfortSeat==null ? "":ctx.comfortSeat);
                                                emd.setCommercialName(azAncillaryPayedTP.getCommercialName());
                                                if (passenger.getEmd() == null){
                                                    passenger.setEmd(new ArrayList<>());
                                                }
                                                passenger.getEmd().add(emd);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


    }

    private String setGroupEmd(String group) {
        String EmdGroup = "";

        switch (group){
            case "Baggage":
                EmdGroup = "BG";
                break;
            case "Lounge":
                EmdGroup = "LG";
                break;
            case "COMFORT SEAT":
                EmdGroup = "SA";
                break;
            case "FAST TRACK":
                EmdGroup = "TS";
                break;
        }

        return EmdGroup;
    }

	//Tolentino - Inizio
    private void performOneClickPayment(SlingHttpServletRequest request, SlingHttpServletResponse response, CheckinSessionContext ctx) throws Exception {
		logger.debug("MAPayment - performOneClickPayment");

		String idPagamento = request.getParameter("idPagamento");
		MACreditCardData selectedCreditCardData = null;
	    if (idPagamento!=null) {
			selectedCreditCardData = getSelectedCreditCardData(request, idPagamento);
			if (selectedCreditCardData==null) {
				logger.error("MAPayment - performOneClickPayment: selected credit card not found, idPagamento ["+idPagamento+"]");
	            throw new Exception("Errore durante il pagamento OneCLick.");
			}
		} else {
			logger.error("MAPayment - performOneClickPayment: credit card not selected, idPagamento ["+idPagamento+"]");
            throw new Exception("Errore durante il pagamento OneCLick.");
	    }

		MyAlitaliaSessionContext mactx = (MyAlitaliaSessionContext) request.getSession().getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);
		if (mactx == null) {
			logger.error("MAPayment - performOneClickPayment: user MA not logged");
			throw new Exception("Utente non loggato");
		}
		Object obj = mactx.getMaCustomer();
		if (!(obj instanceof MACustomer) || obj==null) {
			logger.error("MAPayment - performOneClickPayment: user MA not logged");
			throw new Exception("Utente non loggato");
		}
		MACustomer loggedMAUserProfile = (MACustomer) obj;

        final I18n i18n = new I18n(request.getResourceBundle(AlitaliaUtils.findResourceLocale(request.getResource())));

        String ipAddress = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
        String client = "Checkin";

        //INIZIO Valorizzazione PaymentDetail da form
        PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setCardCode(selectedCreditCardData.getCircuitCode());
        paymentDetail.setCardNumber(selectedCreditCardData.getAlias());                    //Adyen Alias H101509493822804
        paymentDetail.setExpireMonth(selectedCreditCardData.getExpireMonth());
        paymentDetail.setExpireYear(selectedCreditCardData.getExpireYear());
        paymentDetail.setCardSecurityCode(request.getParameter("maocCardSecurityCode"));
        paymentDetail.setCardHolderFirstName(selectedCreditCardData.getHolderFirstName());
        paymentDetail.setCardHolderLastName(selectedCreditCardData.getHolderLastName());
        paymentDetail.setCardBin(selectedCreditCardData.getCardBin());

        if(selectedCreditCardData.getCardBin() != null)
            paymentDetail.setCardBin(selectedCreditCardData.getCardBin());

        /*** recupero informazioni da passare al managementSuccess per invio ricevuta
         *   via email in caso di  pagamento andato a buon fine ***/
        String[] values = new String[4];
        values[0] = selectedCreditCardData.getHolderName();
        values[1] = ctx.pnr;
        values[2] = "";//getFlightsHtml(ctx, request);
        values[3] = "";//computeBusMessage(ctx);

        //recupero indirizzo email dal profilo utente
        List<String> mails = new ArrayList<String>();
        mails.add(loggedMAUserProfile.getProfile().getEmail());

        /********* fine recupero info per invio ricevuta pagamento effettuato via mail ***************/

        paymentDetail.setAmount(ctx.totalPay);                        //prendo il valore salvato in sessione dal getCart()

        paymentDetail.setCurrencyCode(request.getParameter("currencyCode"));                //Valuta/Divisa --- EUR, GBP, USD
        paymentDetail.setApprovalCode("");                                                    //"" non viene valorizzato nella Init
        paymentDetail.setTransactionID("");                                                    //"" non viene valorizzato nella Init
        // Fine

        //Inizio Gestione URL :
        //1. paymentDetail.setApprovedURL;
        //2. paymentDetail.setErrorURL
        String protocol = request.getScheme();
        String serverName = request.getServerName();
        int port = request.getServerPort();
        logger.info("MAPayment - performOneClickPayment: port = " + port);

        ctx.domain = protocol + "://" + serverName;
        logger.info("MAPayment - performOneClickPayment: domain = " + ctx.domain);

        String baseUrlForContext = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
        String baseUrlForRedirect = request.getResource().getResourceResolver().map(baseUrlForContext);

        String csrfToken = request.getHeader("CSRF-Token");

        String errorUrl = ctx.domain + baseUrlForRedirect + "/check-in-search/.checkinendpayment"; //"/booking/.redirectPaymentServlet"; //check-in-payment-complete.html"; //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment"
        String approvedUrl = ctx.domain + baseUrlForRedirect + "/check-in-search/.checkinendpayment"; //"/booking/.redirectPaymentServlet"; //check-in-payment-complete.html";//configuration.getBookingPaymentReturnPage(); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment"
        String redirectUrl = ctx.domain + baseUrlForRedirect + "/check-in-search/.checkinredirect3ds";
        ctx.redirectUrl = redirectUrl;
        logger.info("checkininitpaymentservlet: redirecturl = " + redirectUrl);

        if (!"undefined".equals(csrfToken)) {
            errorUrl += (errorUrl.contains("?") ? "&" : "?") + ":cq_csrf_token=" + csrfToken;
            approvedUrl += "?:cq_csrf_token=" + csrfToken;
        }

        paymentDetail.setApprovedURL(approvedUrl);
        paymentDetail.setErrorURL(errorUrl);
        //Fine Gestione URL

        //Inizio Gestione AzAddressPayment : da non valorizzare
        AzAddressPayment azAddressPayment = new AzAddressPayment();

        azAddressPayment.setAddress("");//Indirizzo
        azAddressPayment.setCityName("");//Città
        azAddressPayment.setCountryName("");//Stato
        azAddressPayment.setPostalCode("");//Codice Postale

        paymentDetail.setAzAddressPayment(azAddressPayment);
        //Fine Gestione AzAddressPayment

        Map<String, String> httpHeaders = getHttpHeaders(request, ";");

        if (ipAddress != null && !ipAddress.isEmpty()) {
            httpHeaders.put(FRAUDNET_IP_HEADER, ipAddress);
        }
        //fin qui non c'e' ancora la proprieta' IPAddress
        String jscString = request.getParameter("beirutString");
        List<FraudNetParam> listFraudNetParam = new ArrayList<>();
        listFraudNetParam = addFraudnetInfo(httpHeaders, jscString);
        ctx.listFraudNetParam = listFraudNetParam;

		logger.debug("MAPayment - performOneClickPayment: perform init payment recurring adyen");
        CheckinInitPaymentResponse result = checkinSession.getInitPaymentRecurringAdyen(request, ctx, client, ipAddress, loggedMAUserProfile.getProfile().getEmail(), paymentDetail);

        if (result != null) {
            if (result.getInitPaymentResponse().getError() != null && !result.getInitPaymentResponse().getError().equals("")) {
        		logger.warn("MAPayment - performOneClickPayment: init payment recurring adyen in error, "+result.getInitPaymentResponse().getError());
        		managementErrorOneClickPayment(request, response, result.getInitPaymentResponse().getError(), ctx);
            } else {

                //rieseguo l'accettazione del passeggero che ha acquistato il posto comfort
                boolean recheckin = checkinSession.checkinSelectedPassengerAfterPayment(request, ctx);

                try {

                    if(result.getInitPaymentResponse().getaZPayedTPResponse() != null) {
                        SetEmdAfterPaymnt(result,ctx);
                        if (!result.getInitPaymentResponse().getaZPayedTPResponse().getAzPaxPayedTP().isEmpty()) {
                            for (AzPaxPayedTP azPaxPayedTP : result.getInitPaymentResponse().getaZPayedTPResponse().getAzPaxPayedTP()) {
                                if (!azPaxPayedTP.getAzAncillaryPayedTP().isEmpty()) {
                                    for (AzAncillaryPayedTP azAncillaryPayedTP : azPaxPayedTP.getAzAncillaryPayedTP()) {
                                        if(ctx.comfortSeat != null) {
                                            setNewPassengerSeat(ctx.comfortSeat, ctx, request);
                                        }
                                    }
                                }
                            }
                        }
                    }

                } catch (Exception q) {
                    logger.error("MAPayment - performOneClickPayment: Errore setEmd e setNewPassengerSeat" + q);
                    throw new WSClientException("Errore setEmd e setNewPassengerSeat", q);
                }

                int total = 0;
                int executed = 0;
                int notAllowed = 0;
                PnrListDataRender renderList = new PnrListDataRender();
                List<PnrRender> pnrListDataRender = new ArrayList<>();
                for (PnrRender pnrRender : renderList.getPnrListDataRender(ctx.pnrListData, ctx, total, executed, notAllowed)) {

                    for (FlightsRender flightsRender : pnrRender.getFlightsRender()) {
                        flightsRender.setTotalAncillaryForPax(new ArrayList<>());
                        if (flightsRender.getSegments().get(0).getPassengers() != null) {
                            for (Passenger passenger : flightsRender.getSegments().get(0).getPassengers()) {
                                TotalAncillaryForPax totalAncillaryForPaxSet = new TotalAncillaryForPax();
                                totalAncillaryForPaxSet.setPassenger(passenger);
                                flightsRender.getTotalAncillaryForPax().add(totalAncillaryForPaxSet);
                            }
                        }
                    }
                    for (FlightsRender flightsRender : ctx.pnrSelectedListDataRender.get(0).getFlightsRender()) {
                        for (Passenger passenger : flightsRender.getSegments().get(0).getPassengers()) {
                            passenger.setNumberOfBaggage(0);
                            if(passenger.getEmd() != null) {
                                for (Emd emd : passenger.getEmd()) {
                                    if (emd.getGroup().equals("BG")) {
                                        passenger.setNumberOfBaggage(passenger.getNumberOfBaggage() + 1);
                                    }
                                }
                            }

                        }
                    }

                    for (FlightsRender flightsRender : pnrRender.getFlightsRender()) {
                        for (SegmentRender segmentRender : flightsRender.getSegments()) {
                            if (segmentRender.getPassengers() != null) {
                                for (Passenger passenger : segmentRender.getPassengers()) {
                                    passenger.setNumberOfFT(0);
                                    passenger.setNumberOfBaggage(0);
                                    passenger.setNumberOfLouge(0);
                                    if(passenger.getEmd() != null) {
                                        for (Emd emd : passenger.getEmd()) {
                                            if (emd.getGroup().equals("TS")) {
                                                passenger.setNumberOfFT(passenger.getNumberOfFT() + 1);
                                            }
                                            if (emd.getGroup().equals("LG")) {
                                                passenger.setNumberOfLouge(passenger.getNumberOfLouge() + 1);
                                            }
                                            if (emd.getGroup().equals("BG")) {
                                                passenger.setNumberOfBaggage(passenger.getNumberOfBaggage() + 1);
                                            }
                                        }
                                        for (TotalAncillaryForPax totalAncillaryForPax : flightsRender.getTotalAncillaryForPax()) {
                                            if (totalAncillaryForPax.getPassenger().getNome().equals(passenger.getNome()) && totalAncillaryForPax.getPassenger().getCognome().equals(passenger.getCognome())) {
                                                totalAncillaryForPax.setTotalBaggage(passenger.getNumberOfBaggage());
                                                totalAncillaryForPax.setTotalFast(totalAncillaryForPax.getTotalFast() + passenger.getNumberOfFT());
                                                totalAncillaryForPax.setTotalLounge(totalAncillaryForPax.getTotalLounge() + passenger.getNumberOfLouge());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    pnrListDataRender.add(pnrRender);
                }

                values[2] = getHTMLAncillary(request, ctx, pnrListDataRender, result, selectedCreditCardData.getCircuitCode(), selectedCreditCardData.getObfuscatedNumber(), new HierarchyNodeInheritanceValueMap(request.getResource().getParent()).getInherited("currencySymbol", String.class), i18n);
                managementSuccessOneClickPayment(request, response, result, redirectUrl, ctx, values, mails);
            }
        } else {
            logger.error("MAPayment - performOneClickPayment: Errore durante l'invocazione del servizio.");
            managementErrorOneClickPayment(request, response, "{\"isError\":true,\"errorMessage\":\"Errore nell'invocazione del servizio\"}", ctx);
        }
	}

    public void managementSuccessOneClickPayment(SlingHttpServletRequest request, SlingHttpServletResponse response, CheckinInitPaymentResponse result, String redirectUrl, CheckinSessionContext ctx, String[] values, List<String> mails) {

        logger.info("MAPayment - [CheckinInitPaymentServlet] [managementSuccessOneClickPayment] ...");

    	logger.info("MAPayment - Init Payment Response Status ["+result.getInitPaymentResponse().getResult().getPaymentStatusDescription()+"]");


        TidyJSONWriter json;
        try {
            json = new TidyJSONWriter(response.getWriter());
            response.setContentType("application/json");

            if (result.getInitPaymentResponse().getResult().getPaymentStatusDescription().equals("REDIRECT")) {
                //caso carta 3Ds
                if (result.getInitPaymentResponse().getRedirectInfoFor3D() != null) {
                    logger.info("MAPayment - [CheckinInitPaymentServlet] [managementSuccessOneClickPayment 3D] redirectUrl = " + redirectUrl);
                    json.object();
                    json.key("isError").value(false);
                    json.key("redirectPage").value(redirectUrl);
                    json.endObject();
                }
            } else if (result.getInitPaymentResponse().getResult().getPaymentStatusDescription().equals("AUTHORIZED")) {
                //il pagamento è avvenuto correttamente: inviare mail con ricevuta di acquisto ancillary
                /****** //TODO cosa fare con questo elemento?
                 ****/
                byte[] attachment = null;

                checkinSession.sendEmail(ctx, request.getResource(), mails, CheckinConstants.CHECKIN_TEMPLATE_EMAIL_ANCILLARY, CheckinConstants.CHECKIN_ANCILLARY_EMAIL_SUBJECT, attachment, CheckinConstants.CHECKIN_ANCILLARY_EMAIL_ATTACHMENT, CheckinConstants.PLACEHOLDER_BOARDING, values, CheckinConstants.CHECKIN_MAIL_REMINDER, ctx.selectedPassengers);

                String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
                        request.getResource(), false) + getConfiguration().getCheckinThankYouPage();

                successPage = request.getResourceResolver().map(successPage);

                logger.info("MAPayment - [CheckinInitPaymentServlet] [managementSuccessOneClickPayment 2D] successPage = " + successPage);

                json.object();
                json.key("isError").value(false);
                json.key("successPage").value(successPage);
                json.endObject();
            } else if (result.getInitPaymentResponse().getResult().getPaymentStatusDescription().equals("APPROVED")) {
                String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
                        request.getResource(), false) + getConfiguration().getCheckinThankYouPage();

                successPage = request.getResourceResolver().map(successPage);

                logger.info("MAPayment - [CheckinInitPaymentServlet] [managementSuccessOneClickPayment 2D] successPage = " + successPage);

                json.object();
                json.key("isError").value(false);
                json.key("successPage").value(successPage);
                json.endObject();
            } else if (result.getInitPaymentResponse().getResult().getPaymentStatusDescription().equals("DECLINED")) {

            } else if (result.getInitPaymentResponse().getResult().getPaymentStatusDescription().equals("ERROR")) {

            }

        } catch (IOException e) {
            logger.error("MAPayment - [CheckinInitPaymentServlet] [managementSuccessOneClickPayment] " + e.getMessage());
            e.printStackTrace();
        } catch (JSONException e) {
            logger.error("MAPayment - [CheckinInitPaymentServlet] [managementSuccessOneClickPayment] " + e.getMessage());
            e.printStackTrace();
        }

    }

	public void managementErrorOneClickPayment(SlingHttpServletRequest request, SlingHttpServletResponse response, String error, CheckinSessionContext ctx) {
        logger.info("MAPayment - [CheckinInitPaymentServlet] [managementErrorOneClickPayment] error =" + error);

        Gson gson = new Gson();
        TidyJSONWriter json;
        response.setContentType("application/json");

        try {
            json = new TidyJSONWriter(response.getWriter());
            json.object();
            json.key("isError").value(true);
            com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);
            final I18n i18n = new I18n(request.getResourceBundle(AlitaliaUtils.findResourceLocale(request.getResource())));

            String redirectPage = request.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) + getConfiguration().getCheckinThankYouPage());
            json.key("redirectPage").value("");

            if (errorObj.getSabreStatusCode()!=null && (errorObj.getSabreStatusCode().equals("IncorrectCCdata") || errorObj.getSabreStatusCode().equals("UpdateReservationError"))) {
                json.key("errorMessage").value(i18n.get("checkin.payment.error.generic"));
                logger.info("MAPayment - [CheckinInitPaymentServlet] [managementErrorOneClickPayment] no redirectPage");
            } else if (errorObj.getSabreStatusCode()!=null && errorObj.getSabreStatusCode().equals("ISSUE_ERROR")) {
                ctx.paymentErrorMsg = i18n.get("checkin.payment.error.issue");
                // non viene inviata la mail e non verranno mostrati gli emd
                json.key("redirectPage").value(redirectPage);
                //rieseguo l'accettazione del passeggero che ha acquistato il posto comfort anche se non emesso - l'utente ha pagato
                boolean recheckin = checkinSession.checkinSelectedPassengerAfterPayment(request, ctx);
                logger.info("MAPayment - [CheckinInitPaymentServlet] [managementErrorOneClickPayment] redirectPage = " + redirectPage);
            } else {
                json.key("errorMessage").value(i18n.get("checkin.payment.error.generic"));
                logger.info("MAPayment - [CheckinInitPaymentServlet] [managementErrorOneClickPayment] redirectPage = " + redirectPage);
            }

            json.key("originalErrorMessage").value(errorObj.getErrorMessage());
            json.endObject();

        } catch (IOException e) {
            logger.error("MAPayment - [CheckinInitPaymentServlet] [managementErrorOneClickPayment] " + e.getMessage());
            e.printStackTrace();
        } catch (JSONException e) {
            logger.error("MAPayment - [CheckinInitPaymentServlet] [managementErrorOneClickPayment] " + e.getMessage());
            e.printStackTrace();
        }
    }

	private MACreditCardData getSelectedCreditCardData(SlingHttpServletRequest request, String selectedCreditCardId) {
		MyAlitaliaSessionContext mactx = (MyAlitaliaSessionContext) request.getSession().getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);
		if (mactx != null) {
			Object obj = mactx.getMaCustomer();
			if (obj instanceof MACustomer && obj!=null) {
				MACustomer loggedMAUserProfile = (MACustomer) obj;
				if (loggedMAUserProfile!=null && loggedMAUserProfile.getData()!=null && loggedMAUserProfile.getData().getPayment()!=null &&
					loggedMAUserProfile.getData().getPayment().getCreditCardsData()!=null &&
					loggedMAUserProfile.getData().getPayment().getCreditCardsData().isEmpty()==false) {
					for (MACreditCardData creditCardData: loggedMAUserProfile.getData().getPayment().getCreditCardsData()) {
						if (creditCardData.getId()==Integer.valueOf(selectedCreditCardId)) {
							return creditCardData;
						}
					}
				}
			}
		}

		return null;
	}

	private boolean isMAUserMemorizzaCarta(SlingHttpServletRequest request) {
		MyAlitaliaSessionContext mactx = (MyAlitaliaSessionContext) request.getSession().getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);
		if (mactx != null) {
			Object obj = mactx.getMaCustomer();
			if (obj instanceof MACustomer && obj!=null) {
				if (request.getParameter("maUserMemorizzaCarta")!=null && !request.getParameter("maUserMemorizzaCarta").equals("")) {
					return true;
				}
			}
		}

		return false;
	}
	//Tolentino - Fine
}