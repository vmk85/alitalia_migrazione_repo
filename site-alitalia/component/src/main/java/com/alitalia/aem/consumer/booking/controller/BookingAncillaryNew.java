package com.alitalia.aem.consumer.booking.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.SeatMapData;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.model.AncillaryPageDoubleContainer;
import com.alitalia.aem.consumer.booking.render.DirectFlightRender;
import com.alitalia.aem.consumer.booking.render.SeatsMapRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingAncillaryNew extends BookingSessionGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private BookingSession bookingSession;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	private List<String> ancillaryComponentList;

	private List<AncillaryPageDoubleContainer> ancillaryContainer;

	private boolean isSeatEnabled;
	
	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			initBaseModel(request);
			
			if (ctx != null) {
				ancillaryComponentList = new ArrayList<String>();
				/*
				ancillaryComponentList.add("alitalia/components/content/booking-posto-new");
				ancillaryComponentList.add("alitalia/components/content/booking-extrabag");
				ancillaryComponentList.add("alitalia/components/content/booking-pasto-new");
				ancillaryComponentList.add("alitalia/components/content/booking-assicurazioni-new");
				*/
				
				ancillaryComponentList.add("alitalia/components/content/booking-extrabag");
				ancillaryComponentList.add("alitalia/components/content/booking-assicurazioni-new");
				ancillaryComponentList.add("alitalia/components/content/booking-posto-new");
				ancillaryComponentList.add("alitalia/components/content/booking-pasto-new");
				
				AncillaryPageDoubleContainer componentContainer=null ;
				
				for (String component : ancillaryComponentList) {
				    
					if (componentContainer == null){
						componentContainer=new AncillaryPageDoubleContainer();
						
						componentContainer.setFirstComponent(component);
					}else{
						componentContainer.setSecondComponent(component);
						ancillaryContainer.add(componentContainer);
						componentContainer = null;
					}
				}
				
				if (componentContainer != null){
					ancillaryContainer.add(componentContainer);
				}
			}
			
		} catch (Exception e) {
			logger.error("Unexpected exception:", e);
			throw (e);
		}
	}
	
	public List<AncillaryPageDoubleContainer> getAncillaryContainer() {
		return this.ancillaryContainer;
	}


	
}
