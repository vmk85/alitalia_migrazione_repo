package com.alitalia.aem.consumer.model.content.specialpages;

public class MillemigliaSocialLoginData {
	public static final String NAME = "MillemigliaSocialLoginData";
	
	private String code;
	private String pin;
	private String error;
	private String firstName;
	private String lastName;
	private String provider;
	private String id;
	private String signature;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}
	
	@Override
	public String toString() {
		return "RecuperaPinData [code=" + code + ", pin=" + pin + ", firstname=" + firstName + ", lastname=" + lastName + ", provider =" + provider + "]";
	}
}
