package com.alitalia.aem.consumer.booking.servlet;



import java.io.IOException;

import javax.servlet.Servlet;

import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AirportDetailsData;
import com.alitalia.aem.common.messages.home.RetrieveAirportDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportDetailsResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.global.i18n.I18nKeyBooking;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.alitalia.aem.service.impl.converter.home.commonservice.AirportDetailsToAirportDetailsData;
import com.alitalia.aem.web.component.common.delegate.CommonDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "airportdetailsconsumer" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
})
public class BookingAirportDetailsServlet extends GenericFormValidatorServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Reference
	private AlitaliaConfigurationHolder alitaliaConfiguration;
	
	@Reference
	private CommonDelegate commonDelegate;
	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		Validator validator = new Validator();
		
		String airportCode = request.getParameter("airportCode");
		
		validator.addDirectCondition("airportCode", airportCode, I18nKeyBooking.MESSAGE_ERROR_APT_CODE_NOT_VALID, "isNotEmpty");
		validator.addDirectCondition("airportCode", airportCode, I18nKeyBooking.MESSAGE_ERROR_APT_CODE_NOT_VALID, "isIataAptCode");
		
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		String airportCode = request.getParameter("airportCode");
		RetrieveAirportDetailsRequest airRequest = new RetrieveAirportDetailsRequest();
		airRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
		airRequest.setAirportCode(airportCode);
		RetrieveAirportDetailsResponse airResponse = commonDelegate.retrieveAirportDetail(airRequest);
		AirportDetailsData detail = airResponse.getAirportDetails();
		if(detail != null){
			request.getSession().setAttribute("airportDetailsData", detail);
			request.getSession().setAttribute("airportCode", airportCode);
			TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
			json.object();
			response.setContentType("application/json");
			json.key("result").value("OK");
			json.endObject();
		}
		else{
			throw new IllegalArgumentException("Response not valid");
		}
		
		
	}
	

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception exception) {
		// TODO Auto-generated method stub
		
	}

	
	
}
