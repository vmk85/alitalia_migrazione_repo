package com.alitalia.aem.consumer.checkin.servlet;


import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checkinancillarycartaddinsurance" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinAddInsuranceServlet extends GenericCheckinFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private CheckinSession checkinSession;	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		CheckinSessionContext ctx = this.getCheckInSessionContext(request);

		Validator validator = new Validator();
		
		
		String acceptConditions = request.getParameter("accept");
		
		validator.addDirectCondition("accept", acceptConditions, BookingConstants.MESSAGE_GENERIC_CHECK_FIELD,
				"isNotEmpty");
		
		
		return validator.validate();
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		checkinSession.addInsuranceToCart(getCheckInSessionContext(request));
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value("OK");
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}

