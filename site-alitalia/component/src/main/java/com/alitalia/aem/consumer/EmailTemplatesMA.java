package com.alitalia.aem.consumer;


import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.*;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.wcm.api.WCMMode;
import com.gigya.socialize.GSObject;
import com.gigya.socialize.GSRequest;
import com.gigya.socialize.GSResponse;
import org.apache.felix.scr.annotations.*;
import org.apache.felix.scr.annotations.Properties;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.engine.SlingRequestProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "setupemailtemplates" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
    @Property(name = "sling.servlet.extensions", value = { "json" })
@SuppressWarnings("serial")
public class EmailTemplatesMA extends SlingAllMethodsServlet {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Reference
	private volatile AlitaliaConfigurationHolder configuration;

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private RequestResponseFactory requestResponseFactory;

	@Reference
	private SlingRequestProcessor requestProcessor;


	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		logger.debug("[setupemailtemplates] inizio setup dei template email per utenti MA");

		String currentLanguage = AlitaliaUtils.getPageLocaleLanguage(request.getResource());

        String apiKey = configuration.getMaApiKey();
	    String secretKey = configuration.getMaSecretKey();
	    String userKey = configuration.getMaUserKey();

        if(userKey==null || userKey.length()==0)
                userKey = "AI9W/RQHPi9r";

        if(apiKey==null || apiKey.length()==0)
                apiKey = "3_6cxzgvUvDl5IM7jwQTtPhYYEjtUalJBu9cBG2wo_QIeAnwADfbItRNXs63nMqvvr";

        if(secretKey==null || secretKey.length()==0)
                secretKey = "7XDjmCjYblwLmP+ejS79/RhqJPGapRbl";

        boolean https = false;
        String suHttps = request.getParameter("suHttps");
        if(suHttps!=null && suHttps.length()>0 && suHttps.equals("true"))
            https = true;

        logger.debug("[setupemailtemplates] the api key is: "+apiKey);
        logger.debug("[setupemailtemplates] the userKey key is: "+userKey);
        logger.debug("[setupemailtemplates] the api secretKey is: "+secretKey);


        String apiMethod = "accounts.setPolicies";
        GSRequest grequest = new GSRequest(apiKey, secretKey, apiMethod, null, https, userKey);

        grequest.setAPIDomain("eu1.gigya.com");

        GSObject emailVerificationTemplates = new GSObject();
        GSObject emailNotifications = new GSObject();
        GSObject passwordReset = new GSObject();
        GSObject emailWelcomeTemplates = new GSObject();
		GSObject emailDeleteTemplates = new GSObject();
		GSObject emailTemplates = new GSObject();
        GSObject ev = new GSObject();

        GSObject accountOptions = new GSObject();

		String templateEV = getMailBody(
			request.getResource(), configuration.MAIL_CONFERMA_ISCRIZIONE_MA,
			true, resolverFactory, requestResponseFactory,
			requestProcessor);
		emailVerificationTemplates.put(currentLanguage,templateEV);

		String templateReg = getMailBody(
			request.getResource(), configuration.MAIL_REGISTRAZIONE_COMPLETATA_MA,
			true, resolverFactory, requestResponseFactory,
			requestProcessor);
		emailWelcomeTemplates.put(currentLanguage,templateReg);

		String templateDel = getMailBody(
				request.getResource(), configuration.MAIL_ELIMINAZIONE_COMPLETATA_MA,
				true, resolverFactory, requestResponseFactory,
				requestProcessor);
		emailDeleteTemplates.put(currentLanguage,templateDel);

		String templateReset = getMailBody(
				request.getResource(), configuration.MAIL_RESET_PASSWORD_MA,
				true, resolverFactory, requestResponseFactory,
				requestProcessor);
		emailTemplates.put(currentLanguage, templateReset);


        ev.put("emailTemplates",emailVerificationTemplates);
        emailNotifications.put("welcomeEmailTemplates",emailWelcomeTemplates);
		emailNotifications.put("accountDeletedEmailTemplates",emailDeleteTemplates);
		passwordReset.put("emailTemplates", emailTemplates);

        grequest.setParam("emailNotifications",emailNotifications);
        grequest.setParam("emailVerification",ev);
        grequest.setParam("passwordReset", "{'emailTemplates':{'" + currentLanguage + "': '" + templateReset + "'}}");

        //accountOptions.put("sendAccountDeletedEmail",true);
        //accountOptions.put("accountDeletedEmailTemplates",emailDeleteTemplates);
        grequest.setParam("accountOptions",accountOptions);

        GSResponse gresponse = grequest.send();

        logger.debug("[setupemailtemplates] the request is: ", grequest);
		logger.debug("[setupemailtemplates] the response is: ", gresponse);

        if(gresponse.getErrorCode()==0){
            logger.debug("[setupemailtemplates] template email myalitalia modificati con successo");

            response.setContentType("text/plain");
            response.getWriter().append("OK - email templates for myalitalia changed with success");
        }else{
            response.getWriter().append(gresponse.getErrorMessage());
            logger.debug("[setupemailtemplates] errore durante l'aggiornamento delle impsotazioni gigya: " + gresponse.getErrorMessage());
        }
	}

	public String getMailBody(Resource resource, String templateMail,
			boolean secure, ResourceResolverFactory resolverFactory,
			RequestResponseFactory requestResponseFactory,
			SlingRequestProcessor requestProcessor) {

		String result = null;
		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(resource, false);
		try {

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			/* Get resource resolver */
			@SuppressWarnings("deprecation")
			ResourceResolver resourceResolver =
				resolverFactory.getAdministrativeResourceResolver(null);
			String requestedUrl = baseUrl + AlitaliaConstants.BASE_CONFIG_PATH
					+ "/" + CheckinConstants.CHECKIN_TEMPLATE_EMAIL_FOLDER
					+ "/" + templateMail;

			/* Setup request */

			HttpServletRequest req =
					requestResponseFactory.createRequest("GET", requestedUrl);
			WCMMode.DISABLED.toRequest(req);

			out = new ByteArrayOutputStream();
			HttpServletResponse resp =
					requestResponseFactory.createResponse(out);

			/* Process request through Sling */
			requestProcessor.processRequest(req, resp, resourceResolver);

			String encoding = "UTF-8";

			result = new String(out.toByteArray(), Charset.forName(encoding));

		} catch (LoginException | ServletException | IOException e) {
			logger.error("Generic exception while retrieving email template "
					+ "from JCR repository: {}", e);
			result = "";
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Template retrieved and APIS/ESTA placeholders "
					+ "replaced: {} ", result);
		}

		return result;

	}

}