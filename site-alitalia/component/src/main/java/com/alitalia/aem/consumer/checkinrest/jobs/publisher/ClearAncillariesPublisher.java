package com.alitalia.aem.consumer.checkinrest.jobs.publisher;

import com.alitalia.aem.common.messages.checkinrest.CheckinClearAncillarySessionEndRequest;
import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceRequest;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkinrest.jobs.CheckinJobConstants;
import com.alitalia.aem.consumer.checkinrest.jobs.consumer.ClearAncillariesJob;
import com.alitalia.aem.consumer.checkinrest.jobs.consumer.OffloadPassengerAndClearAncillariesJob;
import com.google.gson.Gson;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ggadaleta on 06/02/2018.
 */
public class ClearAncillariesPublisher extends BaseJobPublisher {

    public ClearAncillariesPublisher() {
        this.setJobTopic(CheckinJobConstants.TOPIC_ANCILLARIES_CLEAR);
    }

    @Override
    public Map<String, Object> createProperties(CheckinSessionContext ctx) {
        Gson gson = new Gson();
        final Map<String, Object> props = new HashMap<>();

        props.put(ClearAncillariesJob.PROPERTY_CHECKIN_SESSION_ID, this.getSessionID());

        Boolean almenoUnAncillare = false;
        Boolean almenoUnInsurance = false;

        //TODO: refactor in caso di ancillari distribuiti su più di un PNR
        if (ctx.baggage != null) {
            if ("OK".equals(ctx.baggage.getOutcome())) {
                //c'è bagaglio
                logger.debug("Ancillary Baggage recognized {}", ctx.baggage.getOutcome());
                almenoUnAncillare = true;
            }
        }

        //TODO: refactor in caso di ancillari distribuiti su più di un PNR
        if (ctx.fastTrack != null) {
            if ("OK".equals(ctx.fastTrack.getOutcome())) {
                //c'è bagaglio
                logger.debug("Ancillary FastTrack recognized {}", ctx.fastTrack.getOutcome());
                almenoUnAncillare = true;
            }
        }

        //TODO: refactor in caso di ancillari distribuiti su più di un PNR
        if (ctx.lounge != null) {
            if ("OK".equals(ctx.lounge.getOutcome())) {
                //c'è bagaglio
                logger.debug("Ancillary Lounge recognized {}", ctx.lounge.getOutcome());
                almenoUnAncillare = true;
            }
        }

        if (almenoUnAncillare) {
            props.putAll(this.prepareClearAncillariesRequests(ClearAncillariesJob.PROPERTY_CHECKIN_CLEARANCILLARIES, ctx));
        }



        try {
            for (int pnr = 0; pnr < ctx.pnrSelectedListDataRender.size(); pnr++) {
                if (ctx.pnrSelectedListDataRender.get(pnr).getInsurance()) {
                    almenoUnInsurance = true;
                }
            }
        } catch (Exception e) {
            logger.info("eccezione in CheckinJobPublisher, " + e.getMessage());
            System.out.print(e.getStackTrace());
        } finally {
            if (almenoUnInsurance) {
                props.putAll(this.prepareClearInsuranceRequests(ClearAncillariesJob.PROPERTY_CHECKIN_CLEARINSURANCE, ctx));
            }
            return props;
        }
    }
}
