package com.alitalia.aem.consumer.booking.model.extrabaggage;

import java.util.ArrayList;
import java.util.List;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AncillariesExtraBagsPassenger implements Serializable
{

@SerializedName("customerNumber")
@Expose
private String customerNumber;
@SerializedName("surname")
@Expose
private String surname;
@SerializedName("name")
@Expose
private String name;
@SerializedName("idPassenger")
@Expose
private String idPassenger;
@SerializedName("countBaggageFree")
@Expose
private String countBaggageFree;
private final static long serialVersionUID = -7210492196723210856L;

/**
* No args constructor for use in serialization
* 
*/
public AncillariesExtraBagsPassenger() {
}

/**
* 
* @param name
* @param customerNumber
* @param surname
* @param countBaggageFree
* @param idPassenger
*/
public AncillariesExtraBagsPassenger(String customerNumber, String surname, String name, String idPassenger, String countBaggageFree) {
super();
this.customerNumber = customerNumber;
this.surname = surname;
this.name = name;
this.idPassenger = idPassenger;
this.countBaggageFree = countBaggageFree;
}

public String getCustomerNumber() {
return customerNumber;
}

public void setCustomerNumber(String customerNumber) {
this.customerNumber = customerNumber;
}

public String getSurname() {
return surname;
}

public void setSurname(String surname) {
this.surname = surname;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getIdPassenger() {
return idPassenger;
}

public void setIdPassenger(String idPassenger) {
this.idPassenger = idPassenger;
}

public String getCountBaggageFree() {
return countBaggageFree;
}

public void setCountBaggageFree(String countBaggageFree) {
this.countBaggageFree = countBaggageFree;
}

}
