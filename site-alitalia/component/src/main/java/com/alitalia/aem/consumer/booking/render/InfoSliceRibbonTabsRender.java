package com.alitalia.aem.consumer.booking.render;

import java.util.Calendar;

public class InfoSliceRibbonTabsRender {
	
	private String name;
	private DateRender date;
	private String fromCity;
	private String toCity;
	
	public InfoSliceRibbonTabsRender(int sliceIndex, Calendar departureDate,
			String fromCity, String toCity) {
		this.name = obtainNameBySliceIndex(sliceIndex);
		this.date = new DateRender(departureDate);
		this.fromCity = fromCity;
		this.toCity = toCity;
	}


	public String getName() {
		return name;
	}

	public DateRender getDate() {
		return date;
	}

	public String getFromCity() {
		return fromCity;
	}

	public String getToCity() {
		return toCity;
	}
	
	
	/* private methods */
	
	private String obtainNameBySliceIndex(int sliceIndex) {
		switch(sliceIndex){
			case 1:{
				return "PRIMA";
			}
			case 2:{
				return "SECONDA";		
			}
			case 3:{
				return "TERZA";
			}
			case 4:{
				return "QUARTA";
			}
		}
		return "";
	}
	
}
