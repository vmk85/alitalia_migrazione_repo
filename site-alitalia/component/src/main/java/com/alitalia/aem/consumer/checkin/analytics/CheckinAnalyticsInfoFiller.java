package com.alitalia.aem.consumer.checkin.analytics;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.alitalia.aem.common.data.home.checkin.CheckinCountryData;
import com.alitalia.aem.common.data.home.checkin.CheckinCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbRouteStatusEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillarySeatDetailData;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.utils.DateUtils;

public class CheckinAnalyticsInfoFiller{

	public CheckinAnalyticsInfoFiller() {
		initFormats();
	}
	
	private void initFormats() {
	}
	
	
	public CheckinAnalyticsInfo fillInfo(CheckinSessionContext ctx, int step){
		CheckinAnalyticsInfo info = new CheckinAnalyticsInfo();
		info.step = step;
		int[] numPax = {1,0,0,0};
		int firstRouteIndex = 0;
		switch (step){
		case 1:
			info.travelType = computeTravelType(ctx);
			/*FIXME nello step 1 conosciamo soltanto il 1° passeggero*/
			numPax = computePassengerNumbers(ctx);
			info.numAdults = numPax[0];
			info.numChildren = numPax[1];
			info.numInfant = numPax[2];
			info.numYoung = numPax[3];
			
			firstRouteIndex = 0;
			CheckinRouteData depRoute = ctx.routesList.get(firstRouteIndex);
			CheckinFlightData firstFlight = CheckinUtils.getFlightsList(depRoute).get(0);
			info.Boapt = firstFlight.getFrom().getCode();
			info.Bocity = firstFlight.getFrom().getCity();
			info.Bocountry = firstFlight.getFrom().getCountryCode();
			info.depDate = firstFlight.getDepartureDateTime();
			/*FIXME non e' possibile conoscere il brand
			 *  con cui il volo e' stato acquistato*/
			info.depBrand = CheckinUtils.getCompartimentalClass(firstFlight.getSeatClass(), firstFlight);
			info.depFlightNumber = firstFlight.getCarrier() + firstFlight.getFlightNumber();
			/*FIXME FAREBASIS*/
			info.depFareBasis = "";
			
			info.Arapt = firstFlight.getTo().getCode();
			info.Arcity = firstFlight.getTo().getCity();
			info.Arcountry = firstFlight.getTo().getCountryCode();
			info.Network = computeNetwork(firstFlight.getLegType());
			
			if(ctx.routesList.size()-1 >= firstRouteIndex+1 ){
				CheckinRouteData retRoute = ctx.routesList.get(firstRouteIndex+1);
				CheckinFlightData retFlight = retRoute.getFlights().get(0);
				info.retDate = retFlight.getDepartureDateTime();
				/*FIXME non e' possibile conoscere il brand
				 *  con cui il volo e' stato acquistato*/
				info.retBrand = CheckinUtils.getCompartimentalClass(retFlight.getSeatClass(), firstFlight);
				info.retFlightNumber = retFlight.getCarrier() + retFlight.getFlightNumber();
				/*FIXME FAREBASIS*/
				info.retFareBasis = "";
				info.deltaBoAr = DateUtils.computeDiffDays(firstFlight.getDepartureDateTime(), retFlight.getDepartureDateTime());
			}
			
			info.PNR = ctx.pnr;
			break;
		case 2:
			/*No data to track*/
			break;
		case 3:
			info.flowType = ctx.checkinDone ? "CheckIn" : "Manage";
			info.emdTypeProposed = computeEMDTypeProposed(ctx);
			info.ancillaryList = computeAncillaryList(ctx, true, MmbAncillaryStatusEnum.AVAILABLE);
			break;
		case 4:
			info.flowType = ctx.checkinDone ? "CheckIn" : "Manage";
			break;
		case 5:
			info.flowType = ctx.checkinDone ? "CheckIn" : "Manage";
			info.ancillaryList = computeAncillaryList(ctx, false, MmbAncillaryStatusEnum.ISSUED);
			info.paymentType = "Credit Card";
			info.CCType = ctx.paymentCreditCardType;
			info.PNR = ctx.pnr;
			info.tktNumber = computeTktNumberPurchased(ctx);
			
			CheckinRouteData route = ctx.selectedRoute;
			CheckinFlightData depFlight = CheckinUtils.getFlightsList(route).get(0);
			info.depDate = depFlight.getDepartureDateTime();
			/*FIXME trovare il volo di ritorno di un certo volo*/
			firstRouteIndex = getFirstCheckinOpenRouteIndex(ctx);
			if(ctx.routesList.size()-1 >= firstRouteIndex+1 ){
				CheckinFlightData retFlight = CheckinUtils.getFlightsList(ctx.routesList.get(firstRouteIndex+1)).get(0);
				info.retDate = retFlight.getDepartureDateTime();
			}
			break;
		case 6:
			info.flowType = ctx.checkinDone ? "CheckIn" : "Manage";
			break;
		case 7:
			info.flowType = ctx.checkinDone ? "CheckIn" : "Manage";
			numPax = computePassengerNumbers(ctx);
			info.numAdults = numPax[0];
			info.numChildren = numPax[1];
			info.numInfant = numPax[2];
			info.numYoung = numPax[3];
			info.boardingPassType = computeBoardingPassType(ctx);
			break;
		case 8:
			break;
		}
	return info;
}
	


	/*
	 * passenger type order: 
	 * [numAdult , numChildren , numInfant , numYoung]
	 */
	private int[] computePassengerNumbers(CheckinSessionContext ctx){
		int numAdult = 0;
		int numChildren = 0;
		int numInfant = 0;
		int numYoung = 0;
		List<CheckinPassengerData> paxList = ctx.passengers;
		if(paxList == null){
			paxList = new LinkedList<CheckinPassengerData>();
			paxList.add(ctx.passengersRouteList.get(0));
		}
		for(CheckinPassengerData pax : paxList){
			if(pax.getSpecialFare() != null && pax.getSpecialFare().booleanValue()){
				numYoung++;
			}
			else{
				switch(pax.getType()){
				case NORMAL:
					numAdult++;
					break;
				case CHILD:
					numChildren++;
					break;
				case INFANT:
					numInfant++;
					break;
				}
			}
		}
		int[] numPax = {numAdult, numChildren, numInfant, numYoung};
		return numPax;
	}
	
	private String computeNetwork(MmbLegTypeEnum searchCategory) {
		String network = "";
		if(searchCategory != null){
			switch(searchCategory){
			case INC:
				network = "INC";			
				break;
			case INZ:
				network = "INZ";			
				break;
			case DOM:
				network = "NAZ";			
				break;
			default:
				network = "NONE";
			}
		}
		return network;
	}

	
	private List<EMDInfo> computeAncillaryList(CheckinSessionContext ctx
			, boolean onlyNames, MmbAncillaryStatusEnum status) {
		List<EMDInfo> ancList = null;
		ancList = ctx.cart.getAncillaries().stream().filter( anc 
				-> status.equals(anc.getAncillaryStatus()))
				.map(anc -> convertAncillary(anc, onlyNames))
				.distinct()
				.collect(Collectors.toList());
		/*Adding Insurance*/
		if(ctx.insurancePolicyData != null &&
				((status.equals(MmbAncillaryStatusEnum.ISSUED) 
				&& ctx.insurancePaymentSuccess) 
				|| (!status.equals(MmbAncillaryStatusEnum.ISSUED) 
				&& ctx.insurancePolicyData.getBuy() != null 
				&& !ctx.insurancePolicyData.getBuy().booleanValue() ))){
			EMDInfo insurance = new EMDInfo();
			insurance.ancName = "INSURANCE";
			if(!onlyNames){
				insurance.ancQuantity = ctx.acceptedPassengers != null 
					? ctx.acceptedPassengers.size() : ctx.passengers.size();
				insurance.ancRevenue = ctx.insurancePolicyData.getTotalInsuranceCost();
			}
			ancList.add(insurance);
		}
		return ancList;
	}
	
	private EMDInfo convertAncillary(MmbAncillaryData anc, boolean onlyNames){
		EMDInfo ancillary = new EMDInfo();
		switch(anc.getAncillaryType()){
		case SEAT:
			ancillary.ancName = "SCEGLI IL POSTO";
			break;
		case UPGRADE:
			ancillary.ancName = "UPGRADE";
			break;
		case EXTRA_BAGGAGE:
			ancillary.ancName = "BAGAGLIO EXTRA";
			break;
		default:
		}
		if(!onlyNames){
			ancillary.ancQuantity = anc.getQuantity();
			ancillary.ancRevenue = anc.getAmount();
		}
		return ancillary;
	}

	private String computeEMDTypeProposed(CheckinSessionContext ctx) {
		String emdTypeProposed = "";
		boolean free = false;
		boolean pay = false;
		boolean isSeatPresent = false;
		boolean isSeatPayment = false;
		for(MmbAncillaryData anc : ctx.cart.getAncillaries()){
			if(MmbAncillaryTypeEnum.SEAT.equals(anc.getAncillaryType())){
				isSeatPresent = true;
				if(((MmbAncillarySeatDetailData) anc.getAncillaryDetail()) != null 
						&& ((MmbAncillarySeatDetailData) anc.getAncillaryDetail()).isPayment() != null
						&& ((MmbAncillarySeatDetailData) anc.getAncillaryDetail()).isPayment().booleanValue()){
					isSeatPayment = true;	
				}
			}
			if(anc.getQuotations() != null && anc.getQuotations().size() > 0){
				if(!BigDecimal.ZERO.equals(anc.getQuotations().get(0).getPrice())){
					pay = true;
				}
				else{
					free = true;
				}
			}
			else{
				free = true;
			}
		}
		if(ctx.insurancePolicyData != null && ctx.insurancePolicyData.getBuy() != null 
				&& !ctx.insurancePolicyData.getBuy().booleanValue()){
			pay = true;
		}
		if(isSeatPresent && !isSeatPayment){
			free = true;
		}
		emdTypeProposed = (free ? "Free" : "") + (pay ? "Pay" : "");
		return emdTypeProposed;
	}


	private String computeTravelType(CheckinSessionContext ctx) {
		String travelType = "";
		travelType = isRoundTrip(ctx.routesList) ? "RoundTrip": "OneWay";
		return travelType;
	}
	/*
	 * an heuristic for determine if an itinerary is Round Trip
	 */
	private boolean isRoundTrip(List<CheckinRouteData> routesList){
		boolean isRoundTrip = true;
		isRoundTrip = isRoundTrip && routesList.size() > 1;
		isRoundTrip = isRoundTrip && isConsecutiveTrip(routesList);
		isRoundTrip = isRoundTrip && hasSameOriginAndDestination(routesList);
		return isRoundTrip;
	}
	
	
	private boolean isConsecutiveTrip(List<CheckinRouteData> routesList) {
		boolean isConsecutive = true;
		String previousArr = null;
		String dep = null;
		for (CheckinRouteData route : routesList){
			List<CheckinFlightData> flights = CheckinUtils.getFlightsList(route);
			if(flights != null && flights.size() > 0){
				dep = flights.get(0).getFrom().getCode();
				if(previousArr != null){
					isConsecutive = isConsecutive && previousArr.equals(dep);
				}
				previousArr = flights.get(flights.size()-1).getTo().getCode();
			}
			else{
				isConsecutive = false;
			}
		}
		return isConsecutive;
	}
	
	
	private boolean hasSameOriginAndDestination(List<CheckinRouteData> routesList) {
		boolean hasOriginAndDestinationSame = false;
		CheckinRouteData firstRoute = routesList.get(0);
		CheckinRouteData lastRoute = routesList.get(routesList.size()-1);
		List<CheckinFlightData> firstFlights = CheckinUtils.getFlightsList(firstRoute);
		List<CheckinFlightData> lastFlights = CheckinUtils.getFlightsList(lastRoute);
		if(firstFlights != null && firstFlights.size() > 0
				&& lastFlights != null && lastFlights.size() > 0){
			hasOriginAndDestinationSame = firstFlights.get(0).getFrom().getCode().equals(
					lastFlights.get(lastFlights.size()-1).getTo().getCode());
		}
		return hasOriginAndDestinationSame;
	}

	private String computeTktNumberPurchased(CheckinSessionContext ctx) {
		String tktNumbers = "";
		String sep = ",";
		/*If insurance is purchased, all the ticket are added*/
		if(ctx.insurancePaymentSuccess){
			List<CheckinPassengerData> paxList = ctx.passengers;
			if(paxList == null){
				paxList = ctx.passengers;
			}
			tktNumbers = paxList.stream()
					.map(CheckinPassengerData::getCoupons)
					.flatMap(Collection::stream)
					.map(CheckinCouponData::getEticket)
					.map(eTkt -> eTkt.split("C")[0])
					.distinct()
					.collect(Collectors.joining(sep));
		}
		else if(ctx.cart != null && ctx.cart.getAncillaries() != null 
				&& ctx.cart.getAncillaries().size() > 0){
			/*Search for Issued Ancillary*/
			List<MmbAncillaryStatusEnum> statuses = new LinkedList<MmbAncillaryStatusEnum>();
			statuses.add(MmbAncillaryStatusEnum.ISSUED);
			tktNumbers = ctx.cart.getAncillaries().stream().filter( anc 
					-> statuses.contains(anc.getAncillaryStatus()))
					.map(MmbAncillaryData::getCouponNumbers)
					.flatMap(Collection::stream)
					.map(couponNum -> couponNum.split("C")[0])
					.distinct()
					.collect(Collectors.joining(sep));
			
		}
		
		return tktNumbers;
	}
	
	
	private String computeBoardingPassType(CheckinSessionContext ctx) {
		String type = "";
		/*Caso ritiro in aeroporto*/
		if(ctx.boardingPassTYP == 2){
			type = "Airport";
		}
		/*Caso ottieni adesso*/
		else if(ctx.boardingPassTYP == 1){
			type = (ctx.boardingPassPDFRequested ? "PDF;" : "")
					+ (ctx.boardingPassEmailRequested ? "Email;" : "")
					+ (ctx.boardingPassSMSRequested ? "SMS;" : "");
			/* Remove trailing ';' */
			if(type.length() > 0){
				type = type.substring(0, type.length()-1);
			}
		}
		return type;
	}
	
	private int getFirstCheckinOpenRouteIndex(CheckinSessionContext ctx){
		int ind = -1;
		for(int i = 0; i< ctx.routesList.size() && ind < 0; i++) {
			if (MmbRouteStatusEnum.REGULAR.equals(ctx.routesList.get(i).getStatus()) &&
					ctx.routesList.get(i).getCheckinEnabled()){
				ind = i;
			}
		}
		return ind;
	}
	
	
}
