package com.alitalia.aem.consumer.millemiglia.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;

import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.MMFlightData;
import com.alitalia.aem.common.messages.home.AddFlightActivityRequest;
import com.alitalia.aem.common.messages.home.AddFlightActivityResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.global.enumeration.RichiediMigliaNumberCode;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeyMillemiglia;
import com.alitalia.aem.consumer.millemiglia.model.richiedimiglia.RichiediMigliaData;
import com.alitalia.aem.consumer.millemiglia.model.richiedimiglia.RichiediMigliaMSG;
import com.alitalia.aem.consumer.millemiglia.model.richiedimiglia.exceptions.AddFlightActivityException;
import com.alitalia.aem.consumer.millemiglia.model.richiedimiglia.exceptions.MilesRequestFailedException;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
  @Property(name = "sling.servlet.selectors", value = { "richiedimigliasubmit" }),
  @Property(name = "sling.servlet.methods", value = { "POST" }),
  @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class RichiediMigliaServlet extends GenericFormValidatorServlet{

  @Reference(policy = ReferencePolicy.DYNAMIC)
  private volatile ConsumerLoginDelegate consumerLoginDelegate;
  
  @Reference(policy = ReferencePolicy.DYNAMIC)
  private volatile AlitaliaCustomerProfileManager alitaliaCustomerProfileManager;
  
  @Reference
  private AlitaliaConfigurationHolder configuration;
  
  @Override
  protected ResultValidation validateForm(SlingHttpServletRequest request)
      throws IOException {
    logger.debug("[RichiediMigliaServlet] validateForm");

    try {
      // validate parameters!
      Validator validator = setValidatorParameters(request, new Validator());
      ResultValidation res = validator.validate();
      logger.debug("[RichiediMigliaServlet] result validation: "
          + res.toString());
      return res;
    }
    // an error occurred...
    catch (Exception e) {
      logger.error("[RichiediMigliaServlet] Errore durante la procedura "
          + "di salvataggio dati booking", e);
      return null;
    }
  }

  @Override
  protected void performSubmit(SlingHttpServletRequest request,
      SlingHttpServletResponse response) throws Exception {
    logger.debug("[RichiediMigliaServlet] performSubmit");

    ResourceResolver resourceResolver = request.getResourceResolver();
    Session session = resourceResolver.adaptTo(Session.class);
    UserPropertiesManager userPropertiesManager =
        resourceResolver.adaptTo(UserPropertiesManager.class);

    UserProperties profileProperties = userPropertiesManager
    		.getUserProperties(session.getUserID(), "profile");

    AddFlightActivityRequest addFlightActivityRequest =
        new AddFlightActivityRequest();
    addFlightActivityRequest.setSid(IDFactory.getSid());
    addFlightActivityRequest.setTid(IDFactory.getTid());
    addFlightActivityRequest.setTicketNumber(request.getParameter("numberCode") 
    		+ request.getParameter("number"));
    MMCustomerProfileData customerProfileData = new MMCustomerProfileData();
    customerProfileData.setCustomerNumber(profileProperties.getProperty("customerNumber"));
    
    //TODO Controllare
    String dcryptedPin = alitaliaCustomerProfileManager
    		.decodeAndDecryptProperty(profileProperties.getProperty("customerPinCode"));
    customerProfileData.setCustomerPinCode(dcryptedPin);
    customerProfileData.setCustomerName(profileProperties.getProperty("customerName"));
    customerProfileData.setCustomerSurname(profileProperties.getProperty("customerSurname"));
    
    addFlightActivityRequest.setCustomerProfile(customerProfileData);

    AddFlightActivityResponse addFlightActivityResponse = consumerLoginDelegate
    		.addFlightActivity(addFlightActivityRequest);
//    AddFlightActivityResponse addFlightActivityResponse = createMockResponse();
    if (addFlightActivityResponse.getCustomerProfile() != null) {
    	/*Per ottenere messaggi di errore (o successo) distinti per tratta*/
    	List<MMFlightData> flights = addFlightActivityResponse
    			.getCustomerProfile().getFlight();
    	RichiediMigliaFlightOutcome[] outcomes = 
    			new RichiediMigliaFlightOutcome[flights.size()];
    	int i=0;
    	boolean errors = false;
    	for(MMFlightData flight : flights){
    		outcomes[i] = new RichiediMigliaFlightOutcome(
					flight.getVector() + flight.getFlightNumber());
    		if(flight.getResult() != 0){
    			errors = true;
    			outcomes[i].setError(true);
    			outcomes[i].setErrorCode(String.valueOf(flight.getResult()));
    		}
    		i++;
    	}
    	if(errors){
    		throw new MilesRequestFailedException(outcomes);
    	}
    	goBackSuccessfully(request, response);
    } else {
      Integer errorCode = addFlightActivityResponse.getReturnCode();
      logger.debug("[RichiediMigliaServlet] addFlightActivityResponse "
          + "errorCode: " + errorCode);
      
      if(errorCode != null){
    	  throw new AddFlightActivityException(String.valueOf(errorCode));
      }else{
    	  throw new Exception("-1");
      }
    }
  }

  private AddFlightActivityResponse createMockResponse() {
	  AddFlightActivityResponse response = new AddFlightActivityResponse();
	  MMCustomerProfileData customerProfile = new MMCustomerProfileData();
	  List<MMFlightData> flights = new ArrayList<MMFlightData>();
	  MMFlightData flight1 = new MMFlightData();
	  flight1.setVector("AZ");
	  flight1.setFlightNumber("610");
	  flight1.setResult(0);
	  flights.add(flight1);
	  MMFlightData flight2 = new MMFlightData();
	  flight2.setVector("AZ");
	  flight2.setFlightNumber("611");
	  flight2.setResult(11);
	  flights.add(flight2);
	  MMFlightData flight3 = new MMFlightData();
	  flight3.setVector("AZ");
	  flight3.setFlightNumber("612");
	  flight3.setResult(110);
	  flights.add(flight3);
	  customerProfile.setFlight(flights);
	  response.setCustomerProfile(customerProfile);
	return response;
}

@Override
  protected void saveDataIntoSession(SlingHttpServletRequest request,
      Exception e) {
	  I18n i18n = new I18n(request.getResourceBundle(
			  AlitaliaUtils.findResourceLocale(request.getResource())));
    logger.debug("[RichiediMigliaServlet] saveDataIntoSession");
    
    RichiediMigliaData richiediMigliaData = new RichiediMigliaData();
    richiediMigliaData.setNumber(request.getParameter("number"));
    richiediMigliaData.setNumberCode(request.getParameter("numberCode"));

    request.getSession().setAttribute(RichiediMigliaData.NAME,
        richiediMigliaData);

    // go back to form with error
    if(e instanceof MilesRequestFailedException){
    	RichiediMigliaFlightOutcome[] results = ((MilesRequestFailedException) e)
    			.getResults();
    	List<RichiediMigliaMSG> msgList = new ArrayList<RichiediMigliaMSG>(3);
    	for(RichiediMigliaFlightOutcome flightOutcome : results){
    		RichiediMigliaMSG msg = new RichiediMigliaMSG("", false);
    		msg.setSuccess(!flightOutcome.isError());
    		if(flightOutcome.isError()){
    			String errorCode = flightOutcome.getErrorCode();
    			String i18nLabel = "";
    			if (isSpecificError(i18n, errorCode)){
    				i18nLabel =  i18n.get(I18nKeyMillemiglia.ERROR_MESSAGE_MILES_REQUEST_FAILED + "." + errorCode);
    			}
    			else{
    				i18nLabel = i18n.get(I18nKeyMillemiglia.ERROR_MESSAGE_MILES_REQUEST_FAILED) + " (" + errorCode + ")";
    			}
    			msg.setText(i18nLabel
	    				.replaceFirst("\\{0\\}", flightOutcome.getFlightNum())
	    				.replaceFirst("\\{1\\}", errorCode)+ ". ");
    		}
    		else{
    			msg.setText(i18n.get(I18nKeyMillemiglia.ERROR_MESSAGE_MILES_REQUEST_SUCCESS)
    					.replaceFirst("\\{0\\}", flightOutcome.getFlightNum())+ ". ");
    		}
    		msgList.add(msg);
    	}
    	richiediMigliaData.setMessages(msgList);
    	
    }
    else if(e instanceof AddFlightActivityException){
    	String errorCode = ((AddFlightActivityException) e).getCode();
    	String i18nLabel = "";
		if (isSpecificError(i18n, errorCode)){
			i18nLabel =  i18n.get(I18nKeyMillemiglia.ERROR_MESSAGE_MILES_REQUEST_FAILED + "." + errorCode);
		}
		else {
			i18nLabel = i18n.get(I18nKeyMillemiglia.ERROR_MESSAGE_GENERIC) + " (" + errorCode + ")";
		}
		richiediMigliaData.setMessages(Arrays.asList(new RichiediMigliaMSG(i18nLabel, false)));
    }
    else {
    	richiediMigliaData.setMessages(Arrays.asList(new RichiediMigliaMSG(I18nKeyMillemiglia.ERROR_MESSAGE_GENERIC, false)));
    }
  }
  
  /**
   * Set Validator parameters for validation
   * 
   * @param request
   * @param validator
   */
  private Validator setValidatorParameters(SlingHttpServletRequest request,
      Validator validator) {

    // get parameters from request
    String numberCode = request.getParameter("numberCode");
    String number = request.getParameter("number");

    logger.debug("[RichiediMigliaServlet] setValidatorParameters - "
        + "numberCode: " + numberCode + ", number: " + number);
    
    ArrayList<String> listValues = RichiediMigliaNumberCode.listValues();
    Object[] allowedValues = listValues.toArray();
    logger.debug("[RichiediMigliaServlet] setValidatorParameters - "
        + "allowedValues size: " + allowedValues.length);

    validator.addDirectCondition("numberCode", numberCode, 
        I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
    
    validator.setAllowedValues("numberCode", numberCode, allowedValues,
        I18nKeyCommon.MESSAGE_INVALID_FIELD);
    
    validator.addDirectCondition("number", number, 
        I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
    
    validator.addDirectCondition("number", number,
        I18nKeyCommon.MESSAGE_INVALID_FIELD, "isNumber");
    
    validator.addCrossCondition("number", number, "10",
        I18nKeyCommon.MESSAGE_INVALID_FIELD, "sizeIsEqual");
    
    return validator;
  }
  
  private boolean isSpecificError(I18n i18n, String errorCode){
	  String i18nKey = I18nKeyMillemiglia.ERROR_MESSAGE_MILES_REQUEST_FAILED + "." + errorCode;
	  return !(i18n.get(i18nKey).equals(i18nKey));
  }
  
  
  

}

