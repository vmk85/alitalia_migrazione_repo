package com.alitalia.aem.consumer.booking.render;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.consumer.utils.DateUtils;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.utils.FlightDataUtils;
import com.day.cq.i18n.I18n;

public class AvailableFlightsSelectionRender {
	
	private FlightData flightData;
	private SearchElementDetailRender from;
	private SearchElementDetailRender to;
	private String duration;
	private Long nextDays;
	private List<DirectFlightDataRender> stops;
	private int stopsNumber;
	private String carriers;
	private ArrayList<String> classLogoCarrier;
	private List<AvailableFlightBrandSelectionRender> availableFlightBrandSelectionRenderList;
	private String multileg;
	private List<Integer> connectingFlightsWait;
	private GenericPriceRender bestPrice;
	private String bestAwardPrice;
	private int indexToShow;
	private String analyticsFlightNumber;
	private boolean hasBetter;

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public AvailableFlightsSelectionRender(FlightData flightData, Locale locale, I18n i18n, boolean refreshed, FlightData selectedFlightData, BrandData selectedBrandData, BookingSessionContext ctx, int indexToShow) {
		this(flightData, locale, i18n, refreshed, selectedFlightData, selectedBrandData, null, ctx, indexToShow);
	}
	
	public AvailableFlightsSelectionRender(FlightData flightData, Locale locale, I18n i18n, boolean refreshed, FlightData selectedFlightData, 
			BrandData selectedBrandData, String[] expectedBrands, BookingSessionContext ctx, int indexToShow) {
		this.flightData = flightData;
		this.duration = "";
		this.nextDays = null;
		this.stops = new ArrayList<DirectFlightDataRender>();
		this.stopsNumber = 0;
		this.carriers = "";
		this.classLogoCarrier = new ArrayList<String>();
		this.availableFlightBrandSelectionRenderList = new ArrayList<AvailableFlightBrandSelectionRender>();
		this.multileg = "";
		this.indexToShow = indexToShow;
		init(locale, i18n, refreshed, selectedFlightData, selectedBrandData, expectedBrands, ctx);
	}
	
	private void init(Locale locale, I18n i18n, boolean refreshed, FlightData selectedFlightData, BrandData selectedBrandData, String[] expectedBrands, BookingSessionContext ctx) {
		DirectFlightData directFlightData = null;
		stops = new ArrayList<DirectFlightDataRender>();
		connectingFlightsWait = FlightDataUtils.computeConnectingFlightsWait(this.flightData);
		analyticsFlightNumber = "";
		hasBetter = false;
		if (this.flightData.getFlightType() == FlightTypeEnum.CONNECTING) {
			int index = 0;
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) this.flightData;
			for (FlightData flightData : connectingFlightData.getFlights()) {
				directFlightData = (DirectFlightData) flightData;
				analyticsFlightNumber += directFlightData.getCarrier() + directFlightData.getFlightNumber() + ",";
				if ( index == 0 ) {
					this.from = new SearchElementDetailRender(new DateRender(directFlightData.getDepartureDate(), ctx.market),
							directFlightData.getFrom().getCode(), directFlightData.getFrom().getCityCode(),
							i18n.get("airportsData." + directFlightData.getFrom().getCode() + ".city"));
				}
				if ( index == connectingFlightData.getFlights().size() - 1 ) {
					this.to = new SearchElementDetailRender(new DateRender(directFlightData.getArrivalDate(), ctx.market),
							directFlightData.getTo().getCode(), directFlightData.getTo().getCityCode(),
							i18n.get("airportsData." + directFlightData.getTo().getCode() + ".city"));
				}

				Integer waiting = null;
				if (connectingFlightsWait != null) {
					waiting = connectingFlightsWait.get(index++);
				}
				stops.add(new DirectFlightDataRender(directFlightData, waiting, i18n, ctx.market));
			}
			if(analyticsFlightNumber.length() > 0){
				/*remove last comma (,)*/
				analyticsFlightNumber = analyticsFlightNumber.substring(0, analyticsFlightNumber.length() - 1);
			}
		} else {
			directFlightData = (DirectFlightData) this.flightData;
			analyticsFlightNumber = directFlightData.getCarrier()+ directFlightData.getFlightNumber();
			stops.add(new DirectFlightDataRender(directFlightData, null, i18n, ctx.market));
			this.from = new SearchElementDetailRender(new DateRender(directFlightData.getDepartureDate(), ctx.market),
					directFlightData.getFrom().getCode(), directFlightData.getFrom().getCityCode(),
					i18n.get("airportsData." + directFlightData.getFrom().getCode() + ".city"));
			this.to = new SearchElementDetailRender(new DateRender(directFlightData.getArrivalDate(), ctx.market),
					directFlightData.getTo().getCode(), directFlightData.getTo().getCityCode(),
					i18n.get("airportsData." + directFlightData.getTo().getCode() + ".city"));
		}
		
		duration = computeDurationString();
		stopsNumber = stops.size()-1;
		carriers = composeCarrierString(stops);
		classLogoCarrier = computeLogoCarrier(this.flightData);
		nextDays = computeNextDays(this.flightData);
		availableFlightBrandSelectionRenderList = addAvailableBrandSelection(locale,selectedFlightData,selectedBrandData,expectedBrands,ctx);
		setAvailabilityBrands(refreshed);
		multileg = obtainCssClassForMultileg();
	}
	
	public FlightData getFlightData() {
		return flightData;
	}

	public SearchElementDetailRender getFrom() {
		return from;
	}

	public SearchElementDetailRender getTo() {
		return to;
	}
	
	public String getDuration() {
		return duration;
	}

	public long getNextDays() {
		if(nextDays == null){
			return 0;
		}
		return nextDays.longValue();
	}

	public List<DirectFlightDataRender> getStops() {
		return stops;
	}
	
	public int getStopsNumber() {
		return stopsNumber;
	}

	public String getCarriers() {
		return carriers;
	}
	
	public ArrayList<String> getClassLogoCarrier() {
		return classLogoCarrier;
	}

	public List<AvailableFlightBrandSelectionRender> getAvailableFlightBrandSelectionRenderList() {
		return availableFlightBrandSelectionRenderList;
	}

	public String getMultileg() {
		return multileg;
	}
	
	public GenericPriceRender getBestPrice() {
		return bestPrice;
	}
	
	public String getBestAwardPrice() {
		return bestAwardPrice;
	}
	
	public int getIndexToShow() {
		return indexToShow;
	}
	
	public String getAnalyticsFlightNumber() {
		return analyticsFlightNumber;
	}
	
	public boolean isHasBetter() {
		return hasBetter;
	}

	/* private methods */
	
	private ArrayList<String> computeLogoCarrier(FlightData flight) {
		ArrayList<String> carrierLogoList = new ArrayList<String>();
		if (flight instanceof ConnectingFlightData) {
			for (FlightData flightData : ((ConnectingFlightData) flight).getFlights()) {
				DirectFlightData directFlightData = (DirectFlightData) flightData;
				carrierLogoList.add(directFlightData.getCarrier().toLowerCase() + "_small_logo");
			}
		} else {
			DirectFlightData directFlightData = (DirectFlightData) flight;
			carrierLogoList.add(directFlightData.getCarrier().toLowerCase() + "_small_logo");
		}
		return carrierLogoList;
	}

	private void setAvailabilityBrands(boolean refreshed) {
		if (refreshed) {
			for ( AvailableFlightBrandSelectionRender brandSelectionRender : this.availableFlightBrandSelectionRenderList ) {
				if ( brandSelectionRender.getBrandData().getRefreshSolutionId() != null ) {
					 if(brandSelectionRender.getBrandData().getRefreshSolutionId().equals("")) {
						 brandSelectionRender.setEnabled(false);
					 }
				}
			}
		}
	}
	
	private String computeDurationString() {
		String hours = this.flightData.getDurationHour().toString(); 
		String minutes = this.flightData.getDurationMinutes().toString();
		
		if(flightData.getDurationHour().intValue()<10){
			hours = "0" + hours;
		}
		if(flightData.getDurationMinutes().intValue()<10){
			minutes = "0" + minutes;
		}
		
		return hours + "H:" + minutes;
	}
	
	private String composeCarrierString(List<DirectFlightDataRender> stops) {
		String carriers = "";
		if (!stops.isEmpty()) {
			Iterator<DirectFlightDataRender> it = stops.iterator();
			do{
				DirectFlightDataRender stop = it.next();
				if (!carriers.contains(stop.getCarrier())) {
					if(!stop.getCarrier().equals("")){
						if (carriers.equals("")) {
							carriers = stop.getCarrier();
						} else {
							carriers = carriers + ", " + stop.getCarrier();
						}
					}
				}
			}while(it.hasNext());
		}
		return carriers;
	}
	
	private Long computeNextDays(FlightData flightData) {
		Calendar departureDate = null;
		Calendar arrivalDate = null;
		if (flightData instanceof DirectFlightData) {
			DirectFlightData directFlight = (DirectFlightData) flightData;
			departureDate = directFlight.getDepartureDate();
			arrivalDate = directFlight.getArrivalDate();
		} else {
			ConnectingFlightData connectingFlight = (ConnectingFlightData) flightData;
			ArrayList<FlightData> flights = connectingFlight.getFlights();
			departureDate = ((DirectFlightData)flights.get(0)).getDepartureDate();
			arrivalDate = ((DirectFlightData)flights.get(flights.size()-1)).getArrivalDate();
		}
		try {
			long daysBetween = DateUtils.daysBetween(departureDate, arrivalDate);
			return (daysBetween != 0 ? daysBetween : null);
		} catch (Exception e) {
			logger.error("Unexpected error in computeNextDays: ", e);
			throw e;
		}
	}
	
	private List<AvailableFlightBrandSelectionRender> addAvailableBrandSelection(Locale locale, FlightData selectedFilghtData, 
			BrandData selectedBrandData, String[] expectedBrands, BookingSessionContext ctx) {
		
		String selectedFlightNumber = "";
		String currentFlightNumber = "";
		if (selectedFilghtData!=null) {
			if (selectedFilghtData.getFlightType() == FlightTypeEnum.CONNECTING) {
				selectedFlightNumber = ((DirectFlightData) ((ConnectingFlightData)selectedFilghtData).getFlights().get(0)).getFlightNumber();
			} else {
				selectedFlightNumber = ((DirectFlightData)selectedFilghtData).getFlightNumber();
			}
			
			if (this.flightData.getFlightType() == FlightTypeEnum.CONNECTING) {
				currentFlightNumber = ((DirectFlightData) ((ConnectingFlightData)this.flightData).getFlights().get(0)).getFlightNumber();
			} else {
				currentFlightNumber = ((DirectFlightData)this.flightData).getFlightNumber();
			}
		}
		
		BigDecimal bestPriceValue = new BigDecimal(0);
		
		List<AvailableFlightBrandSelectionRender> resultList = new ArrayList<AvailableFlightBrandSelectionRender>();
		
		/*
		 * Gestione corretta dei brand: utilizzato solo se expectedBrands != null
		 * Al momento utilizzata in booking award
		 * TODO Da utilizzare anche in booking revenue dove attualmente invece viene passato il parametro expectedBrands a null
		 */
		Map<String, AvailableFlightBrandSelectionRender> receivedAvailableFlightBrandSelectionRender = 
				new HashMap<String, AvailableFlightBrandSelectionRender>();
		
		int index = 0;
		
		for(BrandData brandData : this.flightData.getBrands()){
			
			BrandPageData page = ctx.codeBrandMap.get(brandData.getCode().toLowerCase());
			
			if (page != null) {
				AvailableFlightBrandSelectionRender availableBrandSelectionRender = null;
				if ( selectedBrandData!=null && currentFlightNumber.equals(selectedFlightNumber) ) {
					
					if (brandData.getCode().equals(selectedBrandData.getCode())) {
						availableBrandSelectionRender = new AvailableFlightBrandSelectionRender(brandData,locale,true,index,ctx);
					} else {
						availableBrandSelectionRender = new AvailableFlightBrandSelectionRender(brandData,locale,false,index,ctx);
					}
					
				} else {
					availableBrandSelectionRender = new AvailableFlightBrandSelectionRender(brandData,locale,false,index,ctx);
				}
				
				if (brandData != null && brandData.isBestFare()) {
					this.hasBetter = true;
				}
				
				if (expectedBrands != null) {
					receivedAvailableFlightBrandSelectionRender.put(page.getId(), availableBrandSelectionRender);
				} else {
					resultList.add(availableBrandSelectionRender);
				}
				
				BigDecimal currentPrice = ctx.award ? brandData.getAwardPrice() : brandData.getGrossFare();
				
				if (availableBrandSelectionRender.isEnabled() 
						&& availableBrandSelectionRender.getSeatsAvailable() > 0 
						&& (bestPriceValue.compareTo(new BigDecimal(0)) == 0 
							|| currentPrice.compareTo(bestPriceValue) < 0)) {
					bestPriceValue = currentPrice;
				}
			}
			index++;
		}
		
		if (ctx.award) {
			bestAwardPrice = String.valueOf(bestPriceValue.intValue());
		} else {
			bestPrice = new GenericPriceRender(bestPriceValue, ctx.currency, ctx);
		}
		
		if (expectedBrands != null) {
			for (String brand : expectedBrands) {
				AvailableFlightBrandSelectionRender element = receivedAvailableFlightBrandSelectionRender.get(brand);
				if (element != null) {
					resultList.add(element);
				} else {
					BrandPageData page = ctx.brandMap.get(brand);
					resultList.add(new AvailableFlightBrandSelectionRender(page, locale, false, -1, ctx));
				}
			}
		}
		
		return resultList;
	}
	
	private String obtainCssClassForMultileg() {
		if(this.flightData.getFlightType() == FlightTypeEnum.CONNECTING){
			return "extra-tratte";
		}
		return "";
	}

}

