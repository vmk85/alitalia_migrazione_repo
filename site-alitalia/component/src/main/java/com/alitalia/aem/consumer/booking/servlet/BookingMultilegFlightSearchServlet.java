package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.model.SearchElementAirport;
import com.alitalia.aem.consumer.booking.model.SearchPassengersNumber;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.TypeUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;

@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingmultilegflightsearchconsumer" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class BookingMultilegFlightSearchServlet extends GenericBookingFormValidatorServlet {
	
private Logger logger = LoggerFactory.getLogger(getClass());
	
//	private ComponentContext componentContext;
	
	@Reference
	private AlitaliaConfigurationHolder alitaliaConfiguration;
	
	@Reference
	private BookingSession bookingSession;
	
	private static final String ATTR_CLASS_OF_FLIGHT = "classOfFlight";
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		try {
			/* -- RECUPERO DATI DALLA FORM -- */
			Validator validator = new Validator();
			
			String[] startMulti;
			
			String dateFormat = AlitaliaUtils.getDateFormatByMarket(request.getResource());
			String addParamDate = "";
			if (dateFormat != null) {
				addParamDate = "|" + dateFormat;
			}

			// MULTITRATTA
			logger.debug("nTratteHidden: " + request.getParameter("nTratteHidden"));
			int nTratte = Integer.parseInt(request.getParameter("nTratteHidden"));

			startMulti = new String[nTratte];

			for (int i = 0; i < nTratte; i++) {
				
				String from = request.getParameter("from[" + (i + 1) + "]");
				String to = request.getParameter("to[" + (i + 1) + "]");
				startMulti[i] = request.getParameter("start["+ (i + 1) + "]");
				
				if(logger.isDebugEnabled()){
					logger.debug("fromMulti["+i+"]: " + from);
					logger.debug("toMulti["+i+"]: " + to);		
				}
				
				// start date
				if(startMulti[i] != null && !("").equals(startMulti[i])) {
					validator.addDirectCondition("start[" + (i + 1) + "]", startMulti[i] + addParamDate, 
							BookingConstants.MESSAGE_DATE_PAST, "isDate", "isFuture");
				} else {
					validator.addDirectCondition("start[" + (i + 1) + "]", startMulti[i] + addParamDate, 
							BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				}
				
				if (i > 0) {
					if (startMulti[i-1] != null && !("").equals(startMulti[i-1])
							&& startMulti[i] != null && !("").equals(startMulti[i])) {
						validator.addCrossConditionMessagePattern("start[" + (i + 1) + "]", startMulti[i-1] + addParamDate, startMulti[i] + addParamDate, 
								BookingConstants.MESSAGE_DATE_AFTER_NOT_VALID, Integer.toString(i), "beforeThen");
					}
				}
				
				// check if departure airport and arrival airport are different
				if (from != null && !("").equals(from)
						&& to != null && !("").equals(to)) {
					validator.addCrossCondition("to[" + (i + 1) + "]", from, to, BookingConstants.MESSAGE_FROM_TO_DIFF, "areDifferent");
					// departure airport
					validator.addDirectCondition("from[" + (i + 1) + "]", from, BookingConstants.MESSAGE_AIRPORT_FROM_NOT_VALID, "isAirport");
					// arrival airport
					validator.addDirectCondition("to[" + (i + 1) + "]", to, BookingConstants.MESSAGE_AIRPORT_TO_NOT_VALID, "isAirport");
				} else {
					validator.addDirectCondition("to[" + (i + 1) + "]", to, BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
					validator.addDirectCondition("from[" + (i + 1) + "]", from, BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				}
				
			}
			
			// number of adults
			validator.addDirectCondition("nadults-multi",
					request.getParameter("nadults-multi"),
					BookingConstants.MESSAGE_NADULTS_NOT_VALID, "isRangeAdults");
	
			// number of kids
			String nkids = request.getParameter("nkids-multi");
			if (nkids != null && !nkids.isEmpty()) {
				validator.addDirectCondition("nkids-multi", nkids,
						BookingConstants.MESSAGE_NKINDS_NOT_VALID, "isRangeKids");
			}
	
			// number of babies
			String nbabies = request.getParameter("nbabies-multi");
			if (nbabies != null && !nbabies.isEmpty()) {
				validator.addDirectCondition("nbabies-multi", nbabies,
						BookingConstants.MESSAGE_NBABIES_NOT_VALID, "isRangeBabies");
			}
			
			// check sum of adults and kids
			validator.addCrossCondition("nadults-multi", request.getParameter("nadults-multi"),
					request.getParameter("nkids-multi"),
					BookingConstants.MESSAGE_SUM_ADULTS_KIDS_NOT_VALID,
					"underMaxSumAdultsKids");
			
			// check if number of babies is lower then number of adults
			if (nbabies != null && !nbabies.isEmpty()) {
				validator.addCrossCondition("nbabies-multi",
						request.getParameter("nadults-multi"),
						request.getParameter("nbabies-multi"),
						BookingConstants.MESSAGE_BABY_PER_ADULT_NOT_VALID, "babyPerAdult");
			}
			
			return validator.validate();
			
		} catch (Exception e) {
			logger.error("BookingMultilegFlightSearchServlet error: ", e);
			throw e;
		}
	}
	

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		
		try {
			
			logger.debug("Entro nella perform submit");
			
			int numberOfSlices = Integer.parseInt(request.getParameter("nTratteHidden"));
			
			String[] fromMulti;
			String[] toMulti;
			String[] startMulti;
			fromMulti = new String[numberOfSlices];
			toMulti = new String[numberOfSlices];
			startMulti = new String[numberOfSlices];
			
			for (int i = 0; i < numberOfSlices; i++) { 
				setSlices(fromMulti,toMulti,startMulti, i, request); 
			}
			
			int nadults = Integer.parseInt(request.getParameter("nadults-multi")); 
			int nchildren = Integer.parseInt(request.getParameter("nkids-multi"));
			int ninfants = Integer.parseInt(request.getParameter("nbabies-multi"));
			
			//prepare search parameters
			String format = AlitaliaUtils.getDateFormatByMarket(request.getResource());
			if (format == null) {
				format = "dd/MM/yyyy";
			}
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			dateFormat.setLenient(false);
			
			BookingSearchKindEnum searchKind = BookingSearchKindEnum.MULTILEG;
			List<SearchElement> searchElements = new ArrayList<SearchElement>();
			SearchPassengersNumber searchPassengersNumber = new SearchPassengersNumber();
			BookingSearchCUGEnum cug = BookingSearchCUGEnum.ADT;
			String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
			String site = AlitaliaUtils.getRepositoryPathMarket(request.getResource()); 
			Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
			InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(request.getResource());
			String decimalSeparator=ivm.getInherited("decimalSeparator", String.class);
			String groupingSeparator=ivm.getInherited("groupingSeparator", String.class);
			Integer numDecDigits=ivm.getInherited("decimalDigit", Integer.class);
			String currency = ivm.getInherited("currencyCode", String.class);
			if(numDecDigits == null){
				numDecDigits = 2;
			}
			NumberFormat currentNumberFormat = AlitaliaUtils.getNumberFormat(numDecDigits, decimalSeparator, groupingSeparator, market);
			
			//set search elements
			for (int i=0; i<numberOfSlices; i++) {
				
				SearchElementAirport fromElement = new SearchElementAirport(fromMulti[i]);
				SearchElementAirport toElement = new SearchElementAirport(toMulti[i]);
				logger.debug("fromMulti["+i+"]: " + fromMulti[i]);
				logger.debug("toMulti["+i+"]: " + toMulti[i]);
				logger.debug("startMulti["+i+"]: " + startMulti[i]);
				searchElements.add(new SearchElement(
						fromElement, toElement, TypeUtils.createCalendar(dateFormat.parse(startMulti[i]))));
				
			}
			
			//set the passengers
			searchPassengersNumber.setNumAdults(nadults);
			searchPassengersNumber.setNumChildren(nchildren);
			searchPassengersNumber.setNumInfants(ninfants);
			
			// create a new booking session context and save it in the session
			HttpSession sessionCTX = request.getSession(true);
			String clientIP = AlitaliaUtils.getRequestRemoteAddr(alitaliaConfiguration, request);
			String sessionId = sessionCTX.getId();
			BookingSessionContext ctx = bookingSession.initializeBookingSession("alitaliaConsumerBooking",
					clientIP,sessionId);
			sessionCTX.setAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE, ctx);
			
			ResourceBundle resourceBundle = request.getResourceBundle(locale);
			final I18n i18n = new I18n(resourceBundle);

			String codiceAgenzia = "";
			String codiceAccordo = "";
			ResourceResolver resourceResolver = request.getResourceResolver();
			Session session = resourceResolver.adaptTo(Session.class);
			UserManager userManager = resourceResolver.adaptTo(UserManager.class);
			String userID = session.getUserID();
		    Authorizable auth = userManager.getAuthorizable(userID);
		    
		    if (auth.getProperty("codiceAgenzia") != null 
		    		&& auth.getProperty("codiceAgenzia").length > 0) {
		    	codiceAgenzia = auth.getProperty("codiceAgenzia")[0].getString();
		    }
		    
		    if (auth.getProperty("codiceAccordo") != null 
		    		&& auth.getProperty("codiceAccordo").length > 0) {
		    	codiceAccordo = auth.getProperty("codiceAccordo")[0].getString();
		    }
		   
		    CabinEnum filterCabin = CabinEnum.ECONOMY;
			String filterClassFlight = request.getParameter(ATTR_CLASS_OF_FLIGHT);
			if (filterClassFlight != null) {
				if(("business").equals(filterClassFlight)) {
					filterCabin = CabinEnum.BUSINESS;
				}
			}
			
			// prepare the search data in the booking session
			bookingSession.prepareSearch(ctx, i18n, searchKind, searchElements,
					searchPassengersNumber, cug, market, site, locale, currentNumberFormat,
					currency, codiceAgenzia, codiceAccordo, filterCabin, false, false);

			//********************
			//MODIFICA DEL 04/05/2017
			//X aggiunta gestione risposta Kayak
			//INIZIO
			//********************
			request.getSession(true).removeAttribute("kayakclickid");
			//********************
			//MODIFICA DEL 04/05/2017
			//X aggiunta gestione risposta Kayak
			//FINE
			//********************
			
			String successPage = alitaliaConfiguration.getBookingFlightSelectPage();
			logger.debug("successPage: " + successPage);
			if (successPage != null && !("").equals(successPage)) {
				String successUrl = AlitaliaUtils.findExternalRelativeUrlByPath(successPage, request.getResource());
				logger.debug("successUrl: " + successUrl);
				response.sendRedirect(response.encodeRedirectURL(successUrl));
			}
			
		} catch(Exception e) {
			logger.error("Unexpected error. ", e);
			
			if (!response.isCommitted()) {
				String failurePage = alitaliaConfiguration.getBookingFailurePage();
				if (failurePage != null && !"".equals(failurePage)) {
					String failureUrl = AlitaliaUtils.findExternalRelativeUrlByPath(failurePage, request.getResource());
					response.sendRedirect(response.encodeRedirectURL(failureUrl));
				} else {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
			}
		}
		
	}
	
	private void setSlices(String[] fromMulti, String[] toMulti,
			String[] startMulti, int i, SlingHttpServletRequest request) {
		
		fromMulti[i] = request.getParameter("from[" + (i + 1) + "]");
		toMulti[i] = request.getParameter("to[" + (i + 1) + "]");
		startMulti[i] = request.getParameter("start["+ (i + 1) + "]");
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return alitaliaConfiguration;
	}
	
	@Override
	/*Send to Booking failure page if an error occurred*/
	protected String getSelfPageUrl(SlingHttpServletRequest request) {
		String selfPageUrl = "";
		String errorPage = alitaliaConfiguration.getBookingFailurePage();
		if(errorPage != null && errorPage.length() > 0){
			String baseUrlForContext = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
			String baseUrlForRedirect = request.getResource().getResourceResolver().map(baseUrlForContext);
			selfPageUrl = baseUrlForRedirect + errorPage;
		}
		else {
			selfPageUrl = super.getSelfPageUrl(request);
		}
		return selfPageUrl;
	}
}
