package com.alitalia.aem.consumer.booking.analytics.filler;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PaymentData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.PaymentProviderGlobalCollectData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.common.data.home.enumerations.GlobalCollectPaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSearchCategoryEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.analytics.AnalyticsInfo;
import com.alitalia.aem.consumer.booking.analytics.AncillaryInfo;
import com.alitalia.aem.consumer.booking.analytics.ConsumerInfo;
import com.alitalia.aem.consumer.booking.analytics.FlightSegmentInfo;
import com.alitalia.aem.consumer.booking.analytics.ItemsInfo;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.model.Passenger;
import com.alitalia.aem.consumer.booking.model.PassengersData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.DateUtils;

public class BookingAnalyticsInfoFiller implements AnalyticsInfoFiller{

	private static final float FROM_MILLIS_TO_DAYS = 1000 * 60 * 60 * 24;

	private Calendar now;

	public BookingAnalyticsInfoFiller() {
		initFormats();
	}
	
	private void initFormats() {
		now = Calendar.getInstance();
		now.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
		
	}
	
	
	public AnalyticsInfo fillInfo(BookingSessionContext ctx, int step, AlitaliaConfigurationHolder config){
		AnalyticsInfo info = new AnalyticsInfo();
		RoutesData routesData = ctx.selectionRoutes;
		FlightSelection[] flights = ctx.flightSelections;
		PassengersData passengers = ctx.passengersData;
		info.step = step;
		/*Fanplayr info*/
		switch(step){
		case 5:
		case 4:
		case 3:
			/*Passenger info*/
			if(ctx.passengersData != null) {
				info.email =  ctx.passengersData.getEmail();
				info.telefono = ctx.passengersData.getContact1() != null
						? ctx.passengersData.getContact1().getPhoneNumber() : "";
			}
			
		case 2:
			/*Flight Selection info*/
			/*Fare Basis Info*/
			String fareBasis = computeFareBasis(ctx.selectionRoutes, ctx.searchKind);
			info.depFareBasis = fareBasis.split(",")[0];
			if(fareBasis.split(",").length > 1)
				info.retFareBasis =  fareBasis.split(",")[1];
			info.eCoupon = (ctx.isCouponFanPlayr != null && ctx.isCouponFanPlayr.booleanValue()) 
					? "FANPLAYR" : (ctx.coupon == null ?  "NO" : "YES");
			info.totalPrice = ctx.grossAmount != null ? ctx.grossAmount : null;
			info.fare = ctx.netAmount != null ? ctx.netAmount : null;
			info.taxes = ctx.totalTaxes != null ? ctx.totalTaxes : null;
			info.surcharges = ctx.totalExtras != null ? ctx.totalExtras : null;
			if (ctx.flightSelections != null) {
				/*Outbound Flight Selection info*/
				FlightSelection departure = ctx.flightSelections[0];
				Calendar departureDateForDelta = Calendar.getInstance();
				if (departure != null) {
					DirectFlightData flight;
					if (departure.getFlightData() instanceof DirectFlightData) {
						flight = (DirectFlightData) departure.getFlightData();
						info.depFlightNumber = flight.getCarrier() + flight.getFlightNumber();

					} else {
						flight = (DirectFlightData) ((ConnectingFlightData) departure.getFlightData()).getFlights().get(0);
						for(FlightData fl : ((ConnectingFlightData) departure.getFlightData()).getFlights()){
							DirectFlightData dirFl = (DirectFlightData) fl;
							info.depFlightNumber += dirFl.getCarrier() + dirFl.getFlightNumber() + ",";
						}
						/*Remove last comma (,)*/
						if(info.depFlightNumber.length() > 0){
							info.depFlightNumber = info.depFlightNumber.substring(0, info.depFlightNumber.length() -1);
						}
					}
					info.depDate = flight.getDepartureDate();
					departureDateForDelta = flight.getDepartureDate();
					info.depHours = flight.getDepartureDate().get(Calendar.HOUR_OF_DAY);
					info.depMinutes = flight.getDepartureDate().get(Calendar.MINUTE);
					info.DayOfWeekA = DateUtils.computeRealDayOfWeek(
							flight.getDepartureDate().get(Calendar.DAY_OF_WEEK));
					info.deltaBoToday = DateUtils.computeDiffDays(now, flight.getDepartureDate());
					// search for selected brand
					BrandData brand = AlitaliaUtils.getBrandInfo((ctx.preSellupBrandData != null ? ctx.preSellupBrandData.subList(0, 1) : flight.getBrands())
							, departure.getSelectedBrandCode(), config.getMinifareSourceBrandCode(), config.getMinifareTargetBrandCode());
					if(brand != null){
						info.depcost = brand.getGrossFare();
						BrandPageData page =  ctx.codeBrandMap.get(departure.getSelectedBrandCode().toLowerCase());
						if(page != null){
							info.depBrand = page.getTitle();
						}
						else{
							info.depBrand = brand.getCode();
						}
					}
				}
				/*Return Flight Selection info*/
				if (ctx.flightSelections.length > 1) {
					FlightSelection back = ctx.flightSelections[1];
					if (back != null) {
						DirectFlightData flight;
						if (back.getFlightData() instanceof DirectFlightData) {
							flight = (DirectFlightData) back.getFlightData();
							info.retFlightNumber = flight.getCarrier() + flight.getFlightNumber();
						} else {
							flight = (DirectFlightData) ((ConnectingFlightData) back.getFlightData()).getFlights()
									.get(0);
							for(FlightData fl : ((ConnectingFlightData) back.getFlightData()).getFlights()){
								DirectFlightData dirFl = (DirectFlightData) fl;
								info.retFlightNumber += dirFl.getCarrier() + dirFl.getFlightNumber() + ",";
							}
							/*Remove last comma (,)*/
							if(info.retFlightNumber.length() > 0){
								info.retFlightNumber = info.retFlightNumber.substring(0, info.retFlightNumber.length() -1);
							}
						}
						info.retDate = flight.getDepartureDate();
						info.retHours = flight.getDepartureDate().get(Calendar.HOUR_OF_DAY);
						info.retMinutes = flight.getDepartureDate().get(Calendar.MINUTE);
						info.DayOfWeekR = DateUtils.computeRealDayOfWeek(
								flight.getDepartureDate().get(Calendar.DAY_OF_WEEK));
						info.deltaBoAr = DateUtils.computeDiffDays(departureDateForDelta, flight.getDepartureDate());
						// search for selected brand
						BrandData brand = AlitaliaUtils.getBrandInfo((ctx.preSellupBrandData != null ? ctx.preSellupBrandData.subList(1, 2) : flight.getBrands())
								, back.getSelectedBrandCode(), config.getMinifareSourceBrandCode(), config.getMinifareTargetBrandCode());
						if(brand != null){
							info.retcost = brand.getGrossFare();
							BrandPageData page =  ctx.codeBrandMap.get(back.getSelectedBrandCode().toLowerCase());
							if(page != null){
								info.retBrand = page.getTitle();
							}
							else{
								info.retBrand = brand.getCode();
							}
						}
					}
				}
			}
		case 1:
			/*Flight search info*/
			info.travelType = computeTravelType(ctx.searchKind);
			info.numAdults = ctx.searchPassengersNumber.getNumAdults();
			info.numYoung = ctx.searchPassengersNumber.getNumYoung();
			info.numChildren = ctx.searchPassengersNumber.getNumChildren();
			info.numInfant = ctx.searchPassengersNumber.getNumInfants();
			info.Network = computeNetwork(ctx.searchCategory);
			info.valuta = ctx.currency;
			if (ctx.searchElements != null && !ctx.searchElements.isEmpty()) {
				info.Boapt = ctx.searchElements.get(0).getFrom().getAirportCode();
				info.Bocity = ctx.searchElements.get(0).getFrom().getCityCode();
				info.Bocountry = ctx.searchElements.get(0).getFrom().getCountryCode();
				info.Arapt = ctx.searchElements.get(0).getTo().getAirportCode();
				info.Arcity = ctx.searchElements.get(0).getTo().getCityCode();
				info.Arcountry = ctx.searchElements.get(0).getTo().getCountryCode();
				if(step == 1){
					info.depDate = ctx.searchElements.get(0).getDepartureDate();
					info.DayOfWeekA = DateUtils.computeRealDayOfWeek(
							ctx.searchElements.get(0).getDepartureDate().get(Calendar.DAY_OF_WEEK));
					info.deltaBoToday = DateUtils.computeDiffDays(now, 
							ctx.searchElements.get(0).getDepartureDate());
					
					if (ctx.searchElements.size() > 1 ) {
						info.retDate = ctx.searchElements.get(1).getDepartureDate();
						info.DayOfWeekR = DateUtils.computeRealDayOfWeek(
								ctx.searchElements.get(1).getDepartureDate().get(Calendar.DAY_OF_WEEK));				
						info.deltaBoAr = DateUtils.computeDiffDays(ctx.searchElements.get(0).getDepartureDate()
								, ctx.searchElements.get(1).getDepartureDate());
					}
				}
			}
			
		}
		
		/*Other info*/
		switch (step){
		case 1:
			/*Flight Selection*/
			if(ctx.filterCabin != null){
				info.cabinData  = ctx.filterCabin.value();
			}
			break;
		case 2:
			/*PassengerData*/
			if(flights != null){
				info.flightSegments = Arrays.asList(flights).stream()
					.filter(fl -> fl != null)
					.flatMap(fl -> convFlightSelection(fl).stream()).collect(Collectors.toList());
			}
			if(ctx.filterCabin != null){
				info.cabinData  = ctx.filterCabin.value();
			}
			break;
		case 3:
			/*Ancillary*/
			if(passengers != null){
				info.consumers = passengers.getPassengersList().stream()
						.filter(pax -> pax != null)
						.map(pax -> convConsumer(pax)).collect(Collectors.toList());
			}
			if(ctx.filterCabin != null){
				info.cabinData  = ctx.filterCabin.value();
			}
			break;
		case 4:
			/*Payment*/
			if(routesData != null){
				info.ancillaries = routesData.getPassengers().stream()
						.filter( pax -> pax != null)
						.flatMap(pax -> convPaxToAncillaries(pax).stream()).collect(Collectors.toList());
				if (routesData.getInsurance() != null) {
					AncillaryInfo insurance = new AncillaryInfo();
					BigDecimal insuranceCost = routesData.getInsurance().getTotalInsuranceCost();
					insurance.ancType = insurance.ancName = "INSURANCE";
					insurance.ancPrice = insuranceCost;
					info.ancillaries.add(insurance);
				}
			}
			if(passengers != null){
				info.consumers = passengers.getPassengersList().stream()
						.filter(pax -> pax != null)
						.map(pax -> convConsumer(pax)).collect(Collectors.toList());
			}
			break;
		case 5:
			/*Receipt*/
			RoutesData prenotation = ctx.prenotation;
			BigDecimal couponPrice = ctx.totalCouponPrice;
			if(flights != null){
				info.items = new LinkedList<ItemsInfo>();
				ItemsInfo flightItem = new ItemsInfo();
				flightItem.type = "FLIGHT";
				flightItem.quantity = (int) Arrays.asList(flights).stream()
					.filter(fl -> fl != null)
					.flatMap(fl -> convFlightSelection(fl).stream()).count();
				info.items.add(flightItem);
				if(routesData != null){
					info.items.addAll(computeAncillaryItemsList(routesData));
				}
			}
			info.netRevenue = ctx.netAmount != null && ctx.totalExtras != null 
					? ctx.netAmount.add(ctx.totalExtras) : null;
			if (prenotation != null) {
				info.PNR = prenotation.getPnr();
				info.transactionDate = now;
				if (prenotation.getPayment() != null) {
					info.paymentType = computePaymentType(prenotation.getPayment());
					info.CCType = (prenotation.getPayment().getProvider() != null 
							&& prenotation.getPayment().getProvider() instanceof PaymentProviderCreditCardData) 
							? ((PaymentProviderCreditCardData)prenotation.getPayment().getProvider()).getType().value() : "";
				}
				if(prenotation.getPassengers() != null){
					if(prenotation.getPassengers().get(0).getTickets() != null){
						info.tktNumber = prenotation.getPassengers().stream()
							.flatMap(pass -> pass.getTickets().stream())
							.map(tkt -> tkt.getTicketNumber()).distinct()
							.collect(Collectors.joining(","));
						int tktPerPNR = 0;
						for(PassengerBaseData pax : prenotation.getPassengers()){
							tktPerPNR += pax.getTickets().stream().map( t -> t.getTicketNumber()).distinct().count();
						}
						info.tktPerPNR = tktPerPNR;
					}
				}
			}
			info.invoiceRequired = ctx.invoiceRequired != null  && ctx.invoiceRequired.booleanValue();
			info.discount = (couponPrice != null) ? couponPrice : null;
			info.userType = "Standard";
			if ( ctx.loggedUserProfileData != null ){
				if(ctx.award){
					info.userType = "Awarded";
				}
				else{
					info.userType = "MMPaid";
				}
			}
		}
	return info;
}
	

	private String computePaymentType(PaymentData payment) {
		String paymentType = payment.getType().value();
		if(PaymentTypeEnum.GLOBAL_COLLECT.equals(payment.getType()) 
				&& GlobalCollectPaymentTypeEnum.MAESTRO.equals(((PaymentProviderGlobalCollectData)payment.getProvider()).getType())){
			paymentType = "Maestro";
		}
		return paymentType;
	}

	private String computeNetwork(BookingSearchCategoryEnum searchCategory) {
		String network = "";
		if(searchCategory != null){
			switch(searchCategory){
			case INTC:
			case INTC_PROMO:
			case INTC_LIGHT:
				network = "INC";			
				break;
			case INTZ:
			case INTZ_PROMO:
			case INTZ_SAVER:
			case INTZ_PREMIUM:
				network = "INZ";
				break;
			default:
				network = "NAZ";
			}
		}
		return network;
	}

	private String computeTravelType(BookingSearchKindEnum searchKind) {
		String travelType = "";
		if(searchKind != null){
			switch(searchKind){
			case SIMPLE:
				travelType = "OneWay";
				break;
			case MULTILEG:
				travelType = "MultiLeg";
				break;
			case ROUNDTRIP:
				travelType = "RoundTrip";
				break;
			}
		}
		
		return travelType;
	}
	
	private String computeFareBasis(RoutesData selectionRoutes, BookingSearchKindEnum searchKind){
		String fareBasis = "";
		if(selectionRoutes != null){
			ResultBookingDetailsData details = ((ResultBookingDetailsData) selectionRoutes.getProperties().get("BookingDetails"));
			if(details != null){
				String depFareBasis = details.getSolutionField().get(0).getPricingField().get(0).getFareField().get(0).getExtendedFareCodeField();
				fareBasis += depFareBasis;
				if(!BookingSearchKindEnum.SIMPLE.equals(searchKind)){
					String retFareBasis = details.getSolutionField().get(0).getPricingField().get(0).getFareField().get(1).getExtendedFareCodeField();
					fareBasis += "," + retFareBasis;
				}
			}
		}
		return fareBasis;
	}
	

	private List<FlightSegmentInfo> convFlightSelection(FlightSelection flightSelection){
		List<FlightSegmentInfo> list = new LinkedList<FlightSegmentInfo>();
		if(flightSelection.getFlightData() instanceof DirectFlightData){
			FlightSegmentInfo flightSeg = new FlightSegmentInfo();
			DirectFlightData directFlight = (DirectFlightData) flightSelection.getFlightData();
			flightSeg.setSegBoapt(directFlight.getFrom().getCode());
			flightSeg.setSegArapt(directFlight.getTo().getCode());
			flightSeg.setSegdepDate(directFlight.getDepartureDate());
			flightSeg.setSegarrDate(directFlight.getArrivalDate());
			flightSeg.setSegdepHours(directFlight.getDepartureDate().get(Calendar.HOUR_OF_DAY));
			flightSeg.setSegarrHours(directFlight.getArrivalDate().get(Calendar.HOUR_OF_DAY));
			flightSeg.setSegdepMinutes(directFlight.getDepartureDate().get(Calendar.MINUTE));
			flightSeg.setSegarrMinutes(directFlight.getArrivalDate().get(Calendar.MINUTE));
			flightSeg.setSegFlightNumber(directFlight.getFlightNumber());
			flightSeg.setSegCarrier(directFlight.getCarrier());
			flightSeg.setSegCabin(directFlight.getCabin().value());
			list.add(flightSeg);
		}
		else if(flightSelection.getFlightData() instanceof ConnectingFlightData){
			ConnectingFlightData connFlight = (ConnectingFlightData) flightSelection.getFlightData();
			for (FlightData flight : connFlight.getFlights()) {
				DirectFlightData directFlight = (DirectFlightData) flight;
				FlightSegmentInfo flightSeg = new FlightSegmentInfo();
				flightSeg.setSegBoapt(directFlight.getFrom().getCode());
				flightSeg.setSegArapt(directFlight.getTo().getCode());
				flightSeg.setSegdepDate(directFlight.getDepartureDate());
				flightSeg.setSegarrDate(directFlight.getArrivalDate());
				flightSeg.setSegdepHours(directFlight.getDepartureDate().get(Calendar.HOUR_OF_DAY));
				flightSeg.setSegarrHours(directFlight.getArrivalDate().get(Calendar.HOUR_OF_DAY));
				flightSeg.setSegdepMinutes(directFlight.getDepartureDate().get(Calendar.MINUTE));
				flightSeg.setSegarrMinutes(directFlight.getArrivalDate().get(Calendar.MINUTE));
				flightSeg.setSegFlightNumber(directFlight.getFlightNumber());
				flightSeg.setSegCarrier(directFlight.getCarrier());
				flightSeg.setSegCabin(directFlight.getCabin().value());
				list.add(flightSeg);
			}

		}
		return list;
	}
	
	
	private ConsumerInfo convConsumer(Passenger passenger){
		ConsumerInfo consumer = new ConsumerInfo();
		String type = "";
		switch (passenger.getPassengerType()) {
			case ADULT:
				type = "ADT";
				break;
//			case YOUTH15:
//				type = "ADT";
//				break;
			case CHILD:
				type = "CHD";
				break;
			case INFANT:
				type = "INF";
				break;
		}
		consumer.type = type;
		consumer.name = passenger.getName();
		consumer.surname = passenger.getSurname();
		return consumer;
	}
	
	
	private List<AncillaryInfo> convPaxToAncillaries(PassengerBaseData passenger){
		List<AncillaryInfo> list = new LinkedList<AncillaryInfo>();
		MealData meal = null;
		List<SeatPreferencesData> prefs = null;
		if (passenger instanceof AdultPassengerData) {
			AdultPassengerData adult = (AdultPassengerData) passenger;
			if(adult.getPreferences() != null){
				meal = adult.getPreferences().getMealType();
				prefs = adult.getPreferences().getSeatPreferences();
			}
		} else if (passenger instanceof ChildPassengerData) {
			ChildPassengerData child = (ChildPassengerData) passenger;
			if (child.getPreferences() != null) {
				meal = child.getPreferences().getMealType();
				prefs = child.getPreferences().getSeatPreferences();
			}
		} else if (passenger instanceof InfantPassengerData) {
			InfantPassengerData infant = (InfantPassengerData) passenger;
			if (infant.getPreferences() != null) {
				meal = infant.getPreferences().getMealType();
				prefs = infant.getPreferences().getSeatPreferences();
			}
		}
		
		if (meal != null) {
			AncillaryInfo mealInfo = new AncillaryInfo();
			mealInfo.ancType = mealInfo.ancName = "MEAL";
			mealInfo.ancPrice = null;
			list.add(mealInfo);
		}
		if (prefs != null && !prefs.isEmpty()) {
			for (int j = 0; j < prefs.size(); j++) {
				AncillaryInfo seatInfo = new AncillaryInfo();
				seatInfo.ancType = seatInfo.ancName = "SEAT";
				seatInfo.ancPrice = null;
				list.add(seatInfo);
			}
		}
		return list;
	}
	
	
	private List<ItemsInfo> computeAncillaryItemsList(RoutesData routesData){
		List<ItemsInfo> list = new LinkedList<ItemsInfo>();
		int mealCount = 0;
		int seatCount = 0;
		for (PassengerBaseData pax : routesData.getPassengers()) {
			MealData meal = null;
			List<SeatPreferencesData> prefs = null;
			if (pax instanceof AdultPassengerData) {
				AdultPassengerData adult = (AdultPassengerData) pax;
				if(adult.getPreferences() != null){
					meal = adult.getPreferences().getMealType();
					prefs = adult.getPreferences().getSeatPreferences();
				}
			} else if (pax instanceof ChildPassengerData) {
				ChildPassengerData child = (ChildPassengerData) pax;
				if(child.getPreferences() != null){
					meal = child.getPreferences().getMealType();
					prefs = child.getPreferences().getSeatPreferences();
				}
			} else if (pax instanceof InfantPassengerData) {
				InfantPassengerData infant = (InfantPassengerData) pax;
				if(infant.getPreferences() != null){
					meal = infant.getPreferences().getMealType();
					prefs = infant.getPreferences().getSeatPreferences();
				}
			}
			if (meal != null) {
				mealCount++;
			}
			if (prefs != null && !prefs.isEmpty()) {
				for (int j = 0; j < prefs.size(); j++) {
					seatCount++;
				}
			}
		}
		if (mealCount > 0) {
			ItemsInfo mealInfo = new ItemsInfo();
			mealInfo.type= "MEAL";
			mealInfo.quantity = mealCount;
			list.add(mealInfo);
		}
		if (seatCount > 0) {
			ItemsInfo seatInfo = new ItemsInfo();
			seatInfo.type = "SEAT";
			seatInfo.quantity = seatCount;
			list.add(seatInfo);
		}
		if (routesData.getInsurance() != null) {
			ItemsInfo insuranceInfo = new ItemsInfo();
			insuranceInfo.type = "INSURANCE";
			insuranceInfo.quantity = 1;
			list.add(insuranceInfo);
		}
		return list;
	}
	

	}
