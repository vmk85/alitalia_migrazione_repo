package com.alitalia.aem.consumer.booking.model;

public enum PaymentType {
	CDC,
	BonificoOnLine,
	PagaDopo,
	OneClick,
	InstalmentsBrazil,
	ZeroPayment
}
