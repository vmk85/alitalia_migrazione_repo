package com.alitalia.aem.consumer.mmb.render;
import java.util.ArrayList;
import java.util.List;

public class MmbAncillaryLoungePassengerRender {
	
	private List<MmbAncillaryLoungeRender> loungeSelections = null;
	private String fullName = "";
	private boolean hasInfant = false;
	private boolean child = false;
	private int totalToBeIssuedQuantity = 0;
	private MmbPriceRender totalToBeIssuedPrice = null;
	private boolean displayInFeedback = false;
	
	public MmbAncillaryLoungePassengerRender() {
		super();
		loungeSelections = new ArrayList<MmbAncillaryLoungeRender>();
	}
	
	public List<MmbAncillaryLoungeRender> getLoungeSelections() {
		return loungeSelections;
	}
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public boolean isHasInfant() {
		return hasInfant;
	}

	public void setHasInfant(boolean hasInfant) {
		this.hasInfant = hasInfant;
	}
	
	public boolean isChild() {
		return child;
	}

	public void setChild(boolean child) {
		this.child = child;
	}

	public MmbPriceRender getTotalToBeIssuedPrice() {
		return totalToBeIssuedPrice;
	}

	public void setTotalToBeIssuedPrice(MmbPriceRender totalToBeIssuedPrice) {
		this.totalToBeIssuedPrice = totalToBeIssuedPrice;
	}


	public int getTotalToBeIssuedQuantity() {
		return totalToBeIssuedQuantity;
	}

	public void setTotalToBeIssuedQuantity(int totalToBeIssuedQuantity) {
		this.totalToBeIssuedQuantity = totalToBeIssuedQuantity;
	}

	public boolean isDisplayInFeedback() {
		return displayInFeedback;
	}

	public void setDisplayInFeedback(boolean displayInFeedback) {
		this.displayInFeedback = displayInFeedback;
	}
	
	
}
