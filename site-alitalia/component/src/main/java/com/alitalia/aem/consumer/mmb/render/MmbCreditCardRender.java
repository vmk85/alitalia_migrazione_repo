package com.alitalia.aem.consumer.mmb.render;

import com.alitalia.aem.common.data.home.DictionaryItemData;
import com.alitalia.aem.common.data.home.PaymentTypeItemData;

public class MmbCreditCardRender {

	private PaymentTypeItemData paymentTypeItemData;
	
	public MmbCreditCardRender(PaymentTypeItemData paymentTypeItemData) {
		this.paymentTypeItemData = paymentTypeItemData;
	}
	
	public PaymentTypeItemData getPaymentTypeItemData() {
		return this.paymentTypeItemData;
	}
	
	public Integer getCvvMaxLenght() {
		for (DictionaryItemData data : this.paymentTypeItemData.getOtherInfo()) {
			if (data.getKey().equals("MaxCVC")) {
				return (Integer) data.getValue();
			}
		}
		
		return 3;
	}
	
	public String getImgName() {
		switch (this.paymentTypeItemData.getCode()) {
			case "AX":
				return "AmericanExpress";
			case "DC":
				return "Diners";
			case "MC":
				return "MasterCard";
			case "TP":
				return "Uatp";
			case "VE":
				return "VisaElectron";
			case "CV":
				return "Visa";
			default:
				break;
		}
		
		return "";
	}
}
