package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "seatmapselection" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.")
})
@SuppressWarnings("serial")
public class BookingSeatMapSelectionServlet extends SlingSafeMethodsServlet {

	@Reference
	private BookingSession bookingSession;
	
	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private ComponentContext componentContext;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
		
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException { 
		
		String genericErrorPagePlain = "#";
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		try{
			response.setContentType("application/json");
			json.object();
			genericErrorPagePlain = (String) 
					componentContext.getProperties().get("failure.page");
			
			BookingSessionContext ctx = (BookingSessionContext) request.getSession(true).getAttribute(
					BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
			
			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				throw new IllegalStateException("Booking session context not found.");
			}
			
			String action = request.getParameter("action");
			if (action == null || ("").equals(action)) {
				throw new IllegalArgumentException("Parameter action not defined");
			}
			
			HashMap<Integer, String> seatPreferencesForAllPassengers[];
			if (action.equals("confirm")) {
				seatPreferencesForAllPassengers = bookingSession.confirmSeatSelection(ctx,request);
			} else if (action.equals("reset")) {
				seatPreferencesForAllPassengers = bookingSession.resetSeatSelection(ctx);
			} else {
				throw new IllegalArgumentException("Parameter action not defined");
			}
			boolean success = seatPreferencesForAllPassengers == null ? false : true;
			if (success) {
				bookingSession.updateAncillarySeatPreferences(ctx, seatPreferencesForAllPassengers);
			}
			json.key("success").value(success);
			json.endObject();
		}catch(Exception e){
			logger.error("Unexpected error: ", e);
			try {
				json.key("redirect").value(genericErrorPagePlain);
				json.endObject();
			} catch (JSONException e1) {
				logger.error("Error json creation: ", e1);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
	}
}

