package com.alitalia.aem.consumer.global.enumeration;

import java.util.ArrayList;

public enum  Genders {

	MALE("male"),
	FEMALE("female");
	

	private final String value;

	Genders(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	public String getI18nValue() {
		return "common.genders." + value;
	}
	
	public static ArrayList<String> listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (Genders c: Genders.values()) {
			result.add(c.value);
		}
		return result;
	}
	
	public static ArrayList<String> listI18nValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (Genders  c: Genders .values()) {
			result.add(c.getI18nValue());
		}
		return result;
	}

	public static Genders  fromValue(String v) {
		for (Genders  c: Genders .values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
