package com.alitalia.aem.consumer.checkin.model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.consumer.checkin.CheckinPassenger;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinUndo extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private CheckinSession checkinSession;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private List<CheckinPassenger> passengers;
	private int passengersSize;
	
	private String homePageUrl;
	
	@PostConstruct
	protected void initModel() {
		
		initBaseModel(request);
		passengers = new ArrayList<CheckinPassenger>();
		
		
		homePageUrl = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ configuration.getCheckinFlightListPage();
		homePageUrl = request.getResourceResolver().map(homePageUrl);
		
		if (ctx != null) {
			for (CheckinPassengerData passengerData : CheckinUtils.getCheckedInPassengers(ctx.passengers)) {
					CheckinPassenger passenger = checkinSession.toCheckinPassenger(passengerData);
					passengers.add(passenger);
			}
		}
		passengersSize = passengers.size();
	}
	
	public List<CheckinPassenger> getPassengers() {
		return passengers;
	}

	public int getPassengersSize() {
		return passengersSize;
	}

	public String getHomePageUrl() {
		return homePageUrl;
	}

}
