package com.alitalia.aem.consumer.mmb.render;

import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;

public class MmbAncillarySeatsMapPassengerRender {
	
	private Integer id = null;
	private String fullName = "";
	private boolean hasInfant = false;
	private boolean isChild = false;
	private String identifier = "";
	private MmbAncillariesGroup ancillariesGroup = null;
	private boolean allowModify = false;
	private String currentValue = "";
	private boolean currentValueComfort = false;
	private String previousValue = "";
	private boolean previousValueComfort = false;
	
	public MmbAncillarySeatsMapPassengerRender() {
		super();
	}
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public boolean getHasInfant() {
		return hasInfant;
	}

	public void setHasInfant(boolean hasInfant) {
		this.hasInfant = hasInfant;
	}
	
	public boolean isChild() {
		return isChild;
	}

	public void setChild(boolean isChild) {
		this.isChild = isChild;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getIdentifier() {
		return this.identifier;
	}
	
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	public MmbAncillariesGroup getAncillariesGroup() {
		return ancillariesGroup;
	}
	
	public boolean isAllowModify() {
		return allowModify;
	}

	public void setAllowModify(boolean allowModify) {
		this.allowModify = allowModify;
	}

	public String getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(String currentValue) {
		this.currentValue = currentValue;
	}

	public String getPreviousValue() {
		return previousValue;
	}

	public void setPreviousValue(String previousValue) {
		this.previousValue = previousValue;
	}

	public boolean isCurrentValueComfort() {
		return currentValueComfort;
	}

	public void setCurrentValueComfort(boolean currentValueComfort) {
		this.currentValueComfort = currentValueComfort;
	}

	public boolean isPreviousValueComfort() {
		return previousValueComfort;
	}

	public void setPreviousValueComfort(boolean previousValueComfort) {
		this.previousValueComfort = previousValueComfort;
	}
	
}
