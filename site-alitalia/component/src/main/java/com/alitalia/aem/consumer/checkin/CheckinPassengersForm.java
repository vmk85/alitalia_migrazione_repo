package com.alitalia.aem.consumer.checkin;

import java.util.List;

public class CheckinPassengersForm {
	
	private List<CheckinPassenger> passengers;
	private String checkAgreement;
	private boolean hasLigthboxInfantToBeShown;
	private boolean hasLigthboxSpecialFareToBeShown;
	
	public List<CheckinPassenger> getPassengers() {
		return passengers;
	}
	
	public void setPassengers(List<CheckinPassenger> passengers) {
		this.passengers = passengers;
	}
	
	public String getCheckAgreement() {
		return checkAgreement;
	}

	public void setCheckAgreement(String checkAgreement) {
		this.checkAgreement = checkAgreement;
	}

	public boolean hasPassengersToCheckIn() {
		if (passengers != null &&
				!passengers.isEmpty()) {
			for (CheckinPassenger passenger : passengers) {
				if (passenger.isCheckin()) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean getHasLigthboxInfantToBeShown() {
		return hasLigthboxInfantToBeShown;
	}

	public void setHasLigthboxInfantToBeShown(boolean hasLigthboxInfantToBeShown) {
		this.hasLigthboxInfantToBeShown = hasLigthboxInfantToBeShown;
	}

	public boolean getHasLigthboxSpecialFareToBeShown() {
		return hasLigthboxSpecialFareToBeShown;
	}

	public void setHasLigthboxSpecialFareToBeShown(boolean hasLigthboxSpecialFareToBeShown) {
		this.hasLigthboxSpecialFareToBeShown = hasLigthboxSpecialFareToBeShown;
	}

}
