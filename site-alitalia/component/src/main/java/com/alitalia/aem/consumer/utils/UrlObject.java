package com.alitalia.aem.consumer.utils;

public class UrlObject {
	private String countryLangCode;
	private String url;

	public UrlObject(String countryLangCode, String url) {
		setUrlObject(countryLangCode);
		setUrl(url);

	}

	public String getCountryCodeLanguageCode() {
		return countryLangCode;
	}

	public void setUrlObject(String countryCodeLanguageCode) {
		this.countryLangCode = countryCodeLanguageCode;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String toString() {

		String string = countryLangCode + ": '" + url + "'";
		return string;
	}

}
