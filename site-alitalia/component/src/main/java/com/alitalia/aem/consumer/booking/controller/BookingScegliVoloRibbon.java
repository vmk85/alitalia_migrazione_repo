package com.alitalia.aem.consumer.booking.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.FlightTabData;
import com.alitalia.aem.common.data.home.TabData;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.render.AvailableRibbonTabsRender;
import com.alitalia.aem.consumer.booking.render.GenericPriceRender;
import com.alitalia.aem.consumer.booking.render.InfoSliceRibbonTabsRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingScegliVoloRibbon extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	private List<AvailableRibbonTabsRender> ribbonTabsList;
	private List<InfoSliceRibbonTabsRender> multilegTabsList;
	private int routeIndex;
	private int indexActiveTab;
	private boolean multileg;
	private String title;
	private String description;

	private boolean showRibbon;

	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			
			initBaseModel(request);
			
			String[] selectors = request.getRequestPathInfo().getSelectors();
			
			if (selectors.length < 2 || !(selectors[1].equals("0") || selectors[1].equals("1"))) {
				logger.error("[BookingScegliVoloSelection] Direction of the route not found");
				throw new RuntimeException("direction of the route not found");
			}
			routeIndex = Integer.parseInt(selectors[1]);
			
			showRibbon = true;
			if (routeIndex == 1 && ctx.searchKind == BookingSearchKindEnum.SIMPLE) {
				showRibbon = false;
			}
			if (ctx.isCarnetProcess) {
				showRibbon = false;
			}
			
			if (showRibbon) {
				multileg=false;
				if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
					multileg = true;
					indexActiveTab = ctx.currentSliceIndex-1;
					multilegTabsList = prepareMultilegTabsList(routeIndex,ctx);
				} else {
					if (ctx.availableFlights.getTabs() != null && ctx.availableFlights.getTabs().size() > 0) {
						if(routeIndex == 1){
							if(ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP){
								ribbonTabsList = prepareRibbonTabsList(routeIndex,ctx,ctx.locale);
								indexActiveTab = ctx.selectedDepartureDateChoices[routeIndex];
							}
						}else{
							ribbonTabsList = prepareRibbonTabsList(routeIndex,ctx,ctx.locale);
							indexActiveTab = ctx.selectedDepartureDateChoices[routeIndex];
						}
						if (routeIndex == ctx.totalSlices-1) {
							addExtraChargeToFare(ctx.totalExtraCharges, ctx);
						}
					}
				}
				
				title = computeTitle(ctx);
				description = computeDescrizione(ctx);
			}
			
		}catch(Exception e){
			if (ctx == null) {
				logger.error("unexpected error - ctx is null");
			} else {
				logger.error("unexpected error: ", e);
			}
			throw e;
		}

	}
	
	public List<AvailableRibbonTabsRender> getRibbonTabsList(){
		return ribbonTabsList;
	}
	
	public int getRouteIndex(){
		return routeIndex;
	}
	
	public int getIndexActiveTab(){
		return indexActiveTab;
	}
	
	public List<InfoSliceRibbonTabsRender> getMultilegTabsList() {
		return multilegTabsList;
	}

	public boolean isMultileg() {
		return multileg;
	}
	
	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}
	
	public boolean getShowRibbon() {
		return showRibbon;
	}

	
	/* private methods */
	
	/**
	 * It sum the extra charge price to the tab fare
	 * @param totalExtraCharges
	 * @param ctx
	 */
	private void addExtraChargeToFare(BigDecimal totalExtraCharges, BookingSessionContext ctx) {
		for (AvailableRibbonTabsRender tabRender : this.ribbonTabsList) {
			GenericPriceRender currentPriceRender = tabRender.getPrice();
			BigDecimal newPrice = new BigDecimal(0);
			newPrice = newPrice.add(tabRender.getTabData().getPrice());
			newPrice = newPrice.add(totalExtraCharges);
			GenericPriceRender newPriceRender = new GenericPriceRender(
					newPrice, currentPriceRender.getCurrency(), ctx);
			tabRender.setPrice(newPriceRender);
		}
		
	}
	
	/**
	 * 
	 * @param routeIndex is the index of the route speficified by the call of this partial
	 * @param ctx 
	 * @return the tabs list
	 */
	private List<AvailableRibbonTabsRender> prepareRibbonTabsList(int routeIndex, BookingSessionContext ctx, Locale locale){
		List<AvailableRibbonTabsRender> ribbonTabsList = new ArrayList<AvailableRibbonTabsRender>();
		
		TabData routeTab = ctx.availableFlights.getTabs().get(routeIndex);
		if(routeIndex==0 && routeTab.getType()==RouteTypeEnum.RETURN){
			routeTab = ctx.availableFlights.getTabs().get(1);
		}
		if(routeIndex==1 && routeTab.getType()==RouteTypeEnum.OUTBOUND){
			routeTab = ctx.availableFlights.getTabs().get(0);
		}
		if(routeTab.getFlightTabs()==null || routeTab.getFlightTabs().size()==0){
			logger.error("Unexpected error - FlightTabs not found");
			throw new RuntimeException("Unexpected error - FlightTabs not found");
		}
		
		for(FlightTabData tabData : routeTab.getFlightTabs()){
			AvailableRibbonTabsRender tabRibbon = new AvailableRibbonTabsRender(tabData,ctx,i18n);
			ribbonTabsList.add(tabRibbon);
		}
		
		return ribbonTabsList;
	}
	
	/**
	 * 
	 * @param routeIndex is the index of the route speficified by the call of this partial
	 * @param ctx 
	 * @return the tabs list
	 */
	private List<InfoSliceRibbonTabsRender> prepareMultilegTabsList(
			int routeIndex, BookingSessionContext ctx) {
		List<InfoSliceRibbonTabsRender> multilegTabsList = new ArrayList<InfoSliceRibbonTabsRender>();
		
		int sliceIndex = 1;
		for (SearchElement searchElement : ctx.searchElements) {
			InfoSliceRibbonTabsRender infoSliceTab = 
					new InfoSliceRibbonTabsRender(sliceIndex, searchElement.getDepartureDate(), 
							searchElement.getFrom().getLocalizedCityName(), searchElement.getTo().getLocalizedCityName());
			multilegTabsList.add(infoSliceTab);
			sliceIndex++;
		}
		
		return multilegTabsList;
	}
	
	private String computeTitle(BookingSessionContext ctx) {
		String titolo = "";
		if(BookingSearchKindEnum.MULTILEG.equals(ctx.searchKind)){
			String label;
			switch(ctx.currentSliceIndex){
			case 1:
				label = BookingConstants.FLIGHT_SELECT_FIRST_SLICE_TITLE;
				break;
			case 2:
				label = BookingConstants.FLIGHT_SELECT_SECOND_SLICE_TITLE;
				break;
			case 3:
				label = BookingConstants.FLIGHT_SELECT_THIRD_SLICE_TITLE;
				break;
			case 4:
				label = BookingConstants.FLIGHT_SELECT_FOURTH_SLICE_TITLE;
				break;
			default:
				label = "";
			}
			titolo = i18n.get(label);
		} else if (routeIndex == 0) {
			titolo = i18n.get(BookingConstants.FLIGHT_SELECT_OUTBOUND_TITLE);
		} else {
			titolo = i18n.get(BookingConstants.FLIGHT_SELECT_RETURN_TITLE);
		}
		return titolo;
	}

	private String computeDescrizione(BookingSessionContext ctx) {
		String descrizione = "";
		if(BookingSearchKindEnum.MULTILEG.equals(ctx.searchKind)){
			descrizione = i18n.get(BookingConstants.FLIGHT_SELECT_MULTISLICE_DESCRIPTION);
		} else if (routeIndex == 0) {
			descrizione = i18n.get(BookingConstants.FLIGHT_SELECT_OUTBOUND_DESCRIPTION);
		} else {
			descrizione = i18n.get(BookingConstants.FLIGHT_SELECT_RETURN_DESCRIPTION);
		}
		return descrizione;
	}

	
}