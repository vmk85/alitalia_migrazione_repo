package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPassengerNoteEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup.AncillariesGroupType;
import com.alitalia.aem.consumer.mmb.model.MmbFlightsGroup;
import com.alitalia.aem.consumer.mmb.render.MmbAncillaryExtraBaggagePassengerRender;
import com.alitalia.aem.consumer.mmb.render.MmbAncillaryExtraBaggageRender;
import com.alitalia.aem.consumer.mmb.render.MmbFlightDataRender;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;
import com.alitalia.aem.consumer.utils.Lists;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbAncillaryExtraBaggage extends MmbSessionAncillaryGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private MmbSession mmbSession;
	
	private boolean allowClearCart = false;
	private List<MmbAncillaryExtraBaggagePassengerRender> extraBaggagePassengers;
	private List<MmbFlightsGroup> flightsGroupList;
	private Integer[] baggageWeightForFlightsGroups;
	
	private int purchasableExtraBaggageNum;
	private int selectedExtraBaggageNum;
	
	
	@PostConstruct
	protected void initModel() throws IOException {
		initBaseModel(request);
		
		this.allowClearCart = false;
		this.extraBaggagePassengers = new ArrayList<MmbAncillaryExtraBaggagePassengerRender>();
		this.flightsGroupList = MmbFlightsGroup.groupByRouteType(ctx.route.getFlights());
		this.baggageWeightForFlightsGroups = new Integer[this.flightsGroupList.size()];
		this.purchasableExtraBaggageNum = 0;
		this.selectedExtraBaggageNum = 0;
		// first, group by passenger
		List<MmbAncillariesGroup> groupsByPassenger = MmbAncillariesGroup.createGroups(
			AncillariesGroupType.BY_PASSENGER, ctx.route, ctx.cart.getPassengers(), 
			ctx.cart.getAncillaries(), MmbAncillaryTypeEnum.EXTRA_BAGGAGE);
		for (MmbAncillariesGroup passengerGroup : groupsByPassenger) {
			
			// skip passengers without any fast track ancillaries
			if (passengerGroup.getAncillaries().size() == 0) {
				continue;
			}
					
			// prepare passenger header render
			MmbAncillaryExtraBaggagePassengerRender extraBaggagePassenger = new MmbAncillaryExtraBaggagePassengerRender();
			extraBaggagePassenger.setFullName(passengerGroup.getPassengerData().getName() + 
					" " + passengerGroup.getPassengerData().getLastName());
			if (MmbPassengerNoteEnum.INFANT_ACCOMPANIST.equals(
					passengerGroup.findMmbPassengerDataForFlight(0).getNote())) {
				extraBaggagePassenger.setHasInfant(true);
			}
			if (MmbPassengerNoteEnum.CHILD.equals(
					passengerGroup.findMmbPassengerDataForFlight(0).getNote())) {
				extraBaggagePassenger.setChild(true);
			}
			extraBaggagePassenger.setTotalToBeIssuedPrice(
					new MmbPriceRender(BigDecimal.valueOf(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
			extraBaggagePassenger.setDisplayInFeedback(false);
			extraBaggagePassengers.add(extraBaggagePassenger);
			
			// for each passenger, group again by route
			List<MmbAncillariesGroup> groups = MmbAncillariesGroup.createGroups(
				AncillariesGroupType.BY_PASSENGER_AND_ROUTE, ctx.route, 
				Lists.asList(passengerGroup.getPassengerData()), passengerGroup.getAncillaries(), 
				MmbAncillaryTypeEnum.EXTRA_BAGGAGE);
			for (MmbAncillariesGroup group : groups) {
				
				MmbFlightData firstRouteFlight = Lists.getFirst(group.getFlights());
				MmbFlightData lastRouteFlight = Lists.getLast(group.getFlights());
				
				// determine the related flight group render
				MmbFlightsGroup relatedFlightsGroup = null;
				int relatedFlightsGroupIndex = -1;
				int i = 0;
				for (MmbFlightsGroup flightGroup : this.flightsGroupList) {
					if (firstRouteFlight.getType().equals(flightGroup.getType()) 
							&& firstRouteFlight.getRouteId().equals(flightGroup.getRouteId())) {
						relatedFlightsGroup = flightGroup;
						relatedFlightsGroupIndex = i;
						break;
					}
					i++;
				}
				
				MmbAncillaryExtraBaggageRender extraBaggage = 
						new MmbAncillaryExtraBaggageRender(group, group.getFlights());
				extraBaggagePassenger.getExtraBaggageItemList().add(extraBaggage);
				extraBaggage.setRouteTypeLabel(getRouteTypeLabel(group));
				extraBaggage.setFirstFlight(new MmbFlightDataRender(firstRouteFlight));
				extraBaggage.setLastFlight(new MmbFlightDataRender(lastRouteFlight));
				extraBaggage.setBaggageAllowNumber(firstRouteFlight.getBaggageAllow());
				if ("2".equals(passengerGroup.findMmbPassengerDataForFlight(0).getFrequentFlyerCustomerValue())) {
					extraBaggage.setBaggageAllowNumber(extraBaggage.getBaggageAllowNumber() + 1);
				}
				
				extraBaggage.setBaggageWeight(Integer.valueOf(23)); // TODO [mmb] CR per determinazione baggage weight
				this.baggageWeightForFlightsGroups[relatedFlightsGroupIndex] = extraBaggage.getBaggageWeight();
				
				if (group.getAncillaries().size() == 0) {
					
					// no ancillary for this passenger/route
					extraBaggage.setIdentifier("");
							
				} else {
							
					List<MmbAncillaryData> availableAncillaries = 
						group.getAncillariesByStatus(
								MmbAncillaryStatusEnum.AVAILABLE);
					List<MmbAncillaryData> addedAncillaries = 
						group.getAncillariesByStatus(
								MmbAncillaryStatusEnum.TO_BE_ISSUED);
					MmbAncillaryData issuedOrPreviouslyIssuedAncillary = 
						group.getAncillaryInStatuses(new MmbAncillaryStatusEnum[] {
								MmbAncillaryStatusEnum.ISSUED,
								MmbAncillaryStatusEnum.PENDING,
								MmbAncillaryStatusEnum.PREVIOUSLY_ISSUED});
					
					if (availableAncillaries.size() > 0) {
						// ancillaries available
						extraBaggage.setIdentifier(
							getAncillariesSelectionIdentifier(availableAncillaries));
						extraBaggage.setCurrentValue(false);
						extraBaggage.setPriceRender(
								mmbSession.getAncillaryDisplayedPriceInfo(ctx, availableAncillaries, 1));
						extraBaggage.setAllowModify(true);
						purchasableExtraBaggageNum += availableAncillaries.size();
								
					} else if (addedAncillaries.size() > 0) {
						// ancillaryies added to cart
						extraBaggage.setIdentifier(
							getAncillariesSelectionIdentifier(addedAncillaries));
						extraBaggage.setCurrentValue(true);
						extraBaggage.setPriceRender(
								mmbSession.getAncillaryDisplayedPriceInfo(ctx, addedAncillaries, null));
						extraBaggage.setAllowModify(true);
						relatedFlightsGroup.getRelatedAncillaries().addAll(addedAncillaries);
						this.allowClearCart = true;
						extraBaggagePassenger.setDisplayInFeedback(true);
						extraBaggagePassenger.setTotalToBeIssuedQuantity(
								extraBaggagePassenger.getTotalToBeIssuedQuantity() + 1);
						extraBaggagePassenger.getTotalToBeIssuedPrice().increaseAmount(
								extraBaggage.getPriceRender().getAmount());
						extraBaggagePassenger.increaseToBeIssuedQuantityForBaggageWeight(
								extraBaggage.getBaggageWeight());
						selectedExtraBaggageNum += addedAncillaries.size();
						
					} else if (issuedOrPreviouslyIssuedAncillary != null) {
						// ancillary already issued
						extraBaggage.setIdentifier(
								getAncillariesSelectionIdentifier(issuedOrPreviouslyIssuedAncillary));
						extraBaggage.setCurrentValue(true);
						extraBaggage.setAllowModify(false);
						extraBaggage.setPriceRender(null);
						relatedFlightsGroup.getRelatedAncillaries().add(issuedOrPreviouslyIssuedAncillary);
						
					}
							
				}
			}
					
		}
		
	}
	
	public List<MmbAncillaryExtraBaggagePassengerRender> getExtraBaggagePassengers() {
		return extraBaggagePassengers;
	}

	public List<MmbFlightsGroup> getFlightsGroupList() {
		return flightsGroupList;
	}
	
	public boolean isAllowClearCart() {
		return allowClearCart;
	}

	public Integer[] getBaggageWeightForFlightsGroups() {
		return baggageWeightForFlightsGroups;
	}

	public int getPurchasableExtraBaggageNum() {
		return purchasableExtraBaggageNum;
	}

	public int getSelectedExtraBaggageNum() {
		return selectedExtraBaggageNum;
	}
	
}
