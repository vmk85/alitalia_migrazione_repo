package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.SlingRequestProcessor;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.ValidationUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.i18n.I18n;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "millemiglialoginvalidation" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")

public class MillemigliaLoginValidationServlet extends GenericFormValidatorServlet {

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private RequestResponseFactory requestResponseFactory;

	@Reference
	private SlingRequestProcessor requestProcessor;

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[MilleMigliaLoginValidationServlet] validateForm");

		try {

			Validator validator = setValidatorParameters(request, new Validator());
			ResultValidation resultValidation = validator.validate();

			if (resultValidation.getResult()){ // Validazione Captcha
				String fieldPrefix = request.getParameter("fieldprefix");
				if (fieldPrefix == null) {
					fieldPrefix = "";
				}

				String recaptchaResponse = request.getParameter(fieldPrefix + "recaptchaResponse");
				logger.debug("recaptchaResponse: {}", recaptchaResponse);
				// Captcha validation
				boolean captchaAlreadyValidated = request.getSession(true).getAttribute("captchaAlreadyValidated") != null ? true : false;
				logger.debug("captchaAlreadyValidated: {}", captchaAlreadyValidated);

				boolean networkEnabled = configuration.getNetworkEnabled();
				if (!captchaAlreadyValidated){
					if (networkEnabled){
						Boolean isResponseCorrect = ValidationUtils.isRecaptchaLoginValid(
								configuration.getRecaptchaSecretKey(), recaptchaResponse, "");

						logger.debug("recaptcha isResponseCorrect: {}", isResponseCorrect);

						if (!isResponseCorrect){
							ResultValidation resultValidationCaptcha = new ResultValidation();
							resultValidationCaptcha.setResult(isResponseCorrect);

							Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
							ResourceBundle resourceBundle = request.getResourceBundle(locale);
							final I18n i18n = new I18n(resourceBundle);
							resultValidationCaptcha.addField(fieldPrefix + "recaptchaContainer", i18n.get("captcha.error.notvalid"));

							return resultValidationCaptcha;
						} else {
							request.getSession(true).setAttribute("captchaAlreadyValidated", true); // Per controllo sulla Check
							logger.debug("Captcha check passed");
						}
					}else {
						request.getSession(true).setAttribute("captchaAlreadyValidated", true); // Per controllo sulla Check
						logger.debug("Network unavailable: Captcha check passed");
					}
				}
			}

			return resultValidation;
		} catch (Exception e) {
			logger.error("Error during parameters validation", e);
			return null;
		}
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		// not required, no submit behaviour provided in this servlet (validation only)
	}

	private Validator setValidatorParameters(SlingHttpServletRequest request, Validator validator) {

		// get parameters from request
		String fieldPrefix = request.getParameter("fieldprefix");
		if (fieldPrefix == null) {
			fieldPrefix = "";
		}
		String code = request.getParameter(fieldPrefix + "code");
		String pin = request.getParameter(fieldPrefix + "pin");
		String recaptchaResponse = request.getParameter(fieldPrefix + "recaptchaResponse");
		logger.debug("recaptchaResponse: ["+recaptchaResponse+"]");

		// add validate conditions
		validator.addDirectConditionMessagePattern(fieldPrefix + "code", code,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "isNotEmpty");
		//Tolto controllo sul isNumber perchè può contenere anche lettere visto che può essere inserito un nickname.
		/*validator.addDirectConditionMessagePattern(fieldPrefix + "code", code,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "isNumber");*/
		validator.addCrossConditionMessagePattern(fieldPrefix + "code", code, "9",
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "sizeIsLess");
		validator.addDirectConditionMessagePattern(fieldPrefix + "pin", pin,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern(fieldPrefix + "pin", pin,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.PIN_ERROR_NOT_VALID, "isNumber");
		validator.addCrossConditionMessagePattern(fieldPrefix + "pin", pin, "4",
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.PIN_ERROR_NOT_VALID, "sizeIsEqual");

		validator.addDirectConditionMessagePattern(fieldPrefix + "recaptchaResponse", recaptchaResponse,
					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
					I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "isNotEmpty");

		return validator;

	}

}

