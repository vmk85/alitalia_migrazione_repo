package com.alitalia.aem.consumer.servlet.login;

import java.io.IOException;

import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginRequest;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;

/**
 * Servlet exposing a REST-based service to update the customer
 * profile data stored in server-side session for the currently
 * logged-in user.
 * 
 * <p>The profile data of a user are synchronized on each login,
 * this servlet should be used to trigger a refresh for an
 * active session when the data may have changed based on user
 * operation.</p>
 * 
 * <p>Note that the client context profile data are based on this
 * data, so usually the invocation of this servlet is paired with
 * a clearing of the client-side storage and a page refresh, in
 * order to retrieve the new data from the back-end web services
 * down to the client context local storage.</p>
 * 
 * <p>No input parameters are required, but an updated pin can
 * be optionally specified - if so, this will be used to retrieve
 * the credentials and also to update the stored encrypted PIN
 * along with the profile properties:
 * <ul>
 * <li>updatedMmPin: the updated PIN (optional).</li>
 * </ul>
 * </p>
 * 
 * <p>The output is a JSON object with the following attributes:
 * <ul>
 * <li>successful: true for a successful result, false otherwise.</li>
 * <li>errorCode: code related to the failed attempt, if applicable.</li> 
 * <li>errorDescription: a description related to the failed attempt, if applicable.</li>
 * </ul>
 * </p>
 */

@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "refreshmillemigliaprofile" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.extensions", value = { "json" })
})
@SuppressWarnings("serial")
public class RefreshMilleMigliaProfileServlet extends SlingAllMethodsServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * Reference to the service to remote consumer login service.
	 */
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_UNARY)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;
	
	/**
	 * Reference to the AlitaliaCustomerProfileManager service.
	 */
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_UNARY)
	private volatile AlitaliaCustomerProfileManager customerProfileManager;
	
	@SuppressWarnings("unused")
	private ComponentContext componentContext;
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		
		logger.debug("MilleMiglia profile refresh requested.");
		
		boolean successful = false;
		String updatedMmPin = request.getParameter("updatedMmPin");
		String errorCode = "";
		String errorDescription = "";
		
		if (consumerLoginDelegate == null) {
			logger.error("Cannot update MilleMiglia profile data, delegate not available.");
			successful = false;
			errorCode = "unknown";
			errorDescription = "Login delegate currently unavailable";
		
		} else if (customerProfileManager == null) {
			logger.error("Cannot update MilleMiglia profile data, customer profile manager not available.");
			successful = false;
			errorCode = "unknown";
			errorDescription = "Customer profile manager currently unavailable";
			
		} else {
			
			try {
				ResourceResolver resourceResolver = request.getResourceResolver();
				Session session = resourceResolver.adaptTo(Session.class);
				UserPropertiesManager userPropertiesManager = resourceResolver.adaptTo(UserPropertiesManager.class);
				
				UserProperties profileProperties = userPropertiesManager.getUserProperties(session.getUserID(), "profile");
				UserProperties privateProperties = userPropertiesManager.getUserProperties(session.getUserID(), "private");
				
				String customerNumber = profileProperties.getProperty("customerNumber");
				
				if (customerNumber == null || "".equals(customerNumber)) {
					
					errorCode = "notLoggedIn";
					errorDescription = "No user currently logged-in";
				
				} else {
				
					String customerPinCode = null;
					if (updatedMmPin != null && !"".equals(updatedMmPin)) {
						customerPinCode = updatedMmPin;
						
					} else {
						customerPinCode = customerProfileManager.decodeAndDecryptProperty(
								privateProperties.getProperty("customerPinCode"));
						
					}
					
					String tid = IDFactory.getTid();
					String sid = IDFactory.getSid(request);
					
					MilleMigliaLoginRequest loginRequest = new MilleMigliaLoginRequest(tid, sid);
					loginRequest.setCustomerCode(customerNumber);
					loginRequest.setCustomerPin(customerPinCode);
					MilleMigliaLoginResponse loginResponse = consumerLoginDelegate.milleMigliaLogin(loginRequest);
					
					successful = loginResponse.isLoginSuccessful();
					if (!successful) {
						errorCode = loginResponse.getErrorCode();
						errorDescription = loginResponse.getErrorDescription();
					
					} else {
						customerProfileManager.updateCustomerProfileProperties(
								loginResponse.getCustomerProfileData(), resourceResolver, updatedMmPin);
						
					}
					
				}
			
			} catch (Exception e) {
				logger.error("Error trying to check MilleMiglia login.", e);
				successful = false;
				errorCode = "unknown";
				errorDescription = "Unexpected error";
				
			}
			
		}
		
		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		try {
			json.object();
			json.key("successful");
			json.value(successful);
			json.key("errorCode");
			json.value(errorCode);
			json.key("errorDescription");
			json.value(errorDescription);
			json.endObject();
		} catch (Exception e) {
			logger.error("Unexected error generating JSON response.", e);
		}
	}
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
}
