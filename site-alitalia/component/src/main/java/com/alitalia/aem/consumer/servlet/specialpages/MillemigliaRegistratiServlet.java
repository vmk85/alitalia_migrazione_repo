package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.MillemigliaRegistratiData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.day.cq.i18n.I18n;

@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "millemigliaregistratisubmit" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")

public class MillemigliaRegistratiServlet extends GenericFormValidatorServlet {
	
	private ComponentContext componentContext;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[MillemigliaRegistratiServlet] validateForm");

		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			return validator.validate();

		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Error during parameters validation", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		logger.debug("[MillemigliaRegistratiServlet] performSubmit");
		
		MillemigliaRegistratiData millemigliaRegistratiData =
				getData(request, 2);
		
		request.getSession().setAttribute(MillemigliaRegistratiData.NAME,
				millemigliaRegistratiData);
		
		String selector = (String) componentContext.getProperties()
				.get("sling.servlet.selectors");
		String urlServlet = request.getRequestURL().toString();
		String url = urlServlet.substring(0,
				urlServlet.length() - selector.length());
		url = url + "html";
		
		response.sendRedirect(url);
	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[MillemigliaRegistratiServlet] saveDataIntoSession");
		
		MillemigliaRegistratiData millemigliaRegistratiData =
				getData(request, 1);
		
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		
		millemigliaRegistratiData.setError(i18n.get(
				I18nKeySpecialPage.ERROR_MESSAGE_MILLEMIGLIA_REGISTRATI));
		
		request.getSession().setAttribute(MillemigliaRegistratiData.NAME,
				millemigliaRegistratiData);
	}

	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {
		logger.debug("[MillemigliaRegistratiServlet] setValidatorParameters");
		
		final String ITALY = "IT";
		final String CANADA = "CA";

		// get parameters from request
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String day = request.getParameter("day");
		String month = request.getParameter("month");
		String year = request.getParameter("year");
		String gender = request.getParameter("gender");
		//String profession = request.getParameter("profession");
		String postalAddress = request.getParameter("postalAddress");
		String company = request.getParameter("company");
		String address = request.getParameter("address");
		String cap = request.getParameter("cap");
		String city = request.getParameter("city");
		String nation = request.getParameter("nation");
		String province = request.getParameter("province");
		String email = request.getParameter("email");
		String nationalPrefix = request.getParameter("nationalPrefix");
		String areaPrefix = request.getParameter("areaPrefix");
		String number = request.getParameter("number");
		String numberType = request.getParameter("numberType");
		String input_username = request.getParameter("input_username");
		String input_password = request.getParameter("input_password");
		String input_confpassword = request.getParameter("input_confpassword");
		String input_email = request.getParameter("input_email");
		String input_newdelivery=request.getParameter("input_newdelivery");
		String phonePrefix=request.getParameter("phonePrefix");
		// add validate conditions
		validator.addDirectConditionMessagePattern("name", name,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("name", name,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isAlphabeticWithSpaces");
		
		validator.addDirectConditionMessagePattern("surname", surname,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("surname", surname,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.SURNAME_ERROR_EMPTY,
				"isAlphabeticWithSpaces");
		
		validator.addDirectConditionMessagePattern("day", day,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern("month", month,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern("year", year,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern("gender", gender,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		validator.setAllowedValues("gender", gender,  new String[] { "M", "F" },
				I18nKeyCommon.MESSAGE_INVALID_FIELD);
		
		validator.addDirectConditionMessagePattern("postalAddress", postalAddress,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		validator.setAllowedValues("postalAddress", postalAddress,
				MillemigliaRegistratiData.ADDRESSES,
				I18nKeyCommon.MESSAGE_INVALID_FIELD);
		
		if (MillemigliaRegistratiData.ADDRESSES[1].equals(postalAddress)) {
			validator.addDirectConditionMessagePattern("company", company,
					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
					I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		}
		
		validator.addDirectConditionMessagePattern("address", address,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern("cap", cap,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");

		//Campi SA
		validator.addDirectConditionMessagePattern("input_username", input_username,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");

		//aggiungere messaggio specigico al posto di I18nKeyCommon.MESSAGE_INVALID_FIELD
		validator.addDirectConditionMessagePattern("input_username", input_username,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isMMUserName");



		validator.addDirectConditionMessagePattern("input_password", input_password,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");

		//Campi SA
		//TODO verificare validazione per input_password, input_confpassword e input_email

//		validator.addDirectConditionMessagePattern("input_password", input_password,
//				I18nKeyCommon.MESSAGE_INVALID_FIELD,
//				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isPassword");

		validator.addDirectConditionMessagePattern("input_confpassword", input_confpassword,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");



//		validator.addDirectConditionMessagePattern("input_confpassword", input_confpassword,
//				I18nKeyCommon.MESSAGE_INVALID_FIELD,
//				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isPassword");

		validator.addDirectConditionMessagePattern("input_email", input_email,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");

//		validator.addDirectConditionMessagePattern("input_email", input_email,
//				I18nKeyCommon.MESSAGE_INVALID_FIELD,
//				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isEmail");


		validator.addDirectConditionMessagePattern("cap", cap,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isAlphaNumeric");
		
		validator.addDirectConditionMessagePattern("city", city,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("city", city,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isAlphabeticWithSpaces");
		
		validator.addDirectConditionMessagePattern("nation", nation,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		
		if (ITALY.equalsIgnoreCase(nation) || CANADA.equalsIgnoreCase(nation)) {
			 validator.addDirectConditionMessagePattern("province", province,
						I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
						I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		}
		
		/*
		validator.addDirectConditionMessagePattern("phonePrefix", phonePrefix,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern("input_newdelivery", input_newdelivery,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.PHONE_NUMBER_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern("input_newdelivery", input_newdelivery,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.PHONE_NUMBER_ERROR_NOT_VALID, "isNumber");
		*/
		
//		validator.addDirectConditionMessagePattern("email", email,
//				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
//				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
//		validator.addDirectConditionMessagePattern("email", email,
//				I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID,
//				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isEmail");


		
//		if (!(isEmpty(areaPrefix) && isEmpty(number) && isEmpty(numberType))) {
//			validator.addDirectConditionMessagePattern("nationalPrefix",
//					nationalPrefix,
//					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
//					I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
//		}
		
//		validator.addDirectConditionMessagePattern("nationalPrefix",
//				nationalPrefix,
//				I18nKeyCommon.MESSAGE_INVALID_FIELD,
//				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNumber");
		
//		if (!(isEmpty(nationalPrefix) && isEmpty(number)
//				&& isEmpty(numberType))) {
//			validator.addDirectConditionMessagePattern("areaPrefix", areaPrefix,
//					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
//					I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
//		}
		
//		validator.addDirectConditionMessagePattern("areaPrefix",
//				areaPrefix,
//				I18nKeyCommon.MESSAGE_INVALID_FIELD,
//				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNumber");
		
//		if (!(isEmpty(nationalPrefix) && isEmpty(areaPrefix)
//				&& isEmpty(numberType))) {
//			validator.addDirectConditionMessagePattern("number", number,
//					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
//					I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
//		}
		
//		validator.addDirectConditionMessagePattern("number", number,
//				I18nKeyCommon.MESSAGE_INVALID_FIELD,
//				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNumber");
		
//		if (!(isEmpty(nationalPrefix) && isEmpty(areaPrefix)
//				&& isEmpty(number))) {
//			validator.addDirectConditionMessagePattern("numberType", numberType,
//					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
//					I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
//		}

		return validator;

	}
	
	/*
	 * isEmpty
	 * 
	 */
	private boolean isEmpty(String value) {
		if (null == value || 0 == value.length()) {
			return true;
		}
		return false;
	}
	
	/*
	 * getData
	 * 
	 */
	private MillemigliaRegistratiData getData(SlingHttpServletRequest request,
			int passo) {
		logger.debug("[MillemigliaRegistratiServlet] getData");
		
		MillemigliaRegistratiData millemigliaRegistratiData = (MillemigliaRegistratiData) 
				request.getSession()
				.getAttribute(MillemigliaRegistratiData.NAME);
		
		String trackCode = "0";
		if(millemigliaRegistratiData != null && millemigliaRegistratiData.getTrackingCod() != null && !"".equals(millemigliaRegistratiData.getTrackingCod())){
			trackCode = millemigliaRegistratiData.getTrackingCod();
			logger.debug("TrackingCode trovato in sessione: " + trackCode);
		} else{
			logger.debug("TrackingCode NON trovato in sessione");
		}
		
		millemigliaRegistratiData =
				new MillemigliaRegistratiData();
		
		millemigliaRegistratiData.setName(
				request.getParameter("name"));
		millemigliaRegistratiData.setSurname(
				request.getParameter("surname"));
		millemigliaRegistratiData.setDayOfBirth(
				request.getParameter("day")); // wsdl ok
		millemigliaRegistratiData.setMonthOfBirth(
				request.getParameter("month")); // wsdl ok
		millemigliaRegistratiData.setYearOfBirth(
				request.getParameter("year")); // wsdl ok
		millemigliaRegistratiData.setGender(
				request.getParameter("gender"));
		millemigliaRegistratiData.setProfession(
				request.getParameter("profession"));
		millemigliaRegistratiData.setPostalAddress(
				request.getParameter("postalAddress"));
		millemigliaRegistratiData.setCompany(
				request.getParameter("company"));
		millemigliaRegistratiData.setAddress(
				request.getParameter("address")); // wsdl ok
		millemigliaRegistratiData.setCap(
				request.getParameter("cap"));
		millemigliaRegistratiData.setUsername(
				request.getParameter("input_username"));
		millemigliaRegistratiData.setPassword(
				request.getParameter("input_password"));
		millemigliaRegistratiData.setCertifiedEmailAccount(
				request.getParameter("input_email"));
		millemigliaRegistratiData.setCity(
				request.getParameter("city"));
		millemigliaRegistratiData.setNation(
				request.getParameter("nation"));
		millemigliaRegistratiData.setProvince(
				request.getParameter("province"));
		millemigliaRegistratiData.setEmail(
				request.getParameter("email"));

		millemigliaRegistratiData.setNationalPrefix(
				request.getParameter("nationalPrefix"));

		millemigliaRegistratiData.setCountryNumber(
				request.getParameter("nationalPrefix"));

		millemigliaRegistratiData.setAreaPrefix(
				request.getParameter("areaPrefix"));
		millemigliaRegistratiData.setNumber(
				request.getParameter("number"));

		millemigliaRegistratiData.setTelephoneType(
				request.getParameter("numberType"));

		millemigliaRegistratiData.setTelephoneType(
				request.getParameter("typedelivery"));

//		millemigliaRegistratiData.setTelephoneType(
//				request.getParameter("nationalPrefix"));

		millemigliaRegistratiData.setTrackingCod(trackCode);
		millemigliaRegistratiData.setState(passo);

		millemigliaRegistratiData.setInputSecretQuestion(
				request.getParameter("secretQuestion"));

		millemigliaRegistratiData.setRispostaSegreta(
				request.getParameter("secureAnswer"));


		millemigliaRegistratiData.setCertifiedPhoneNumber(
				request.getParameter("input_newdelivery"));


		millemigliaRegistratiData.setPhonePrefix(
				request.getParameter("phonePrefix"));


		millemigliaRegistratiData.setFlagVerificaUserName_OK(
				Boolean.parseBoolean(request.getParameter("flagVerificaUserName_OK")));

		millemigliaRegistratiData.setFlagVerificaPassword_OK(
				Boolean.parseBoolean(request.getParameter("flagVerificaPassword_OK")));

		millemigliaRegistratiData.setFlagVerificaCellulare_OK(
				Boolean.parseBoolean(request.getParameter("flagVerificaCellulare_OK")));

		millemigliaRegistratiData.setFlagVerificaRisposta_OK(
				Boolean.parseBoolean(request.getParameter("flagVerificaRisposta_OK")));

		millemigliaRegistratiData.setFlagVerificaEmailUser_OK(
				Boolean.parseBoolean(request.getParameter("flagVerificaEmailUser_OK")));

//		if (request.getParameter("number") != null
//				&& !"".equals(request.getParameter("number"))) {
//			millemigliaRegistratiData.setViewSms(true);
//		} else {
//			millemigliaRegistratiData.setViewSms(false);
//		}
		
		return millemigliaRegistratiData;
	}
	
}