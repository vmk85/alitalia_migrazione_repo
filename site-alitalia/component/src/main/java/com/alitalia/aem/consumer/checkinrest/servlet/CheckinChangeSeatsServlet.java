package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;

import com.alitalia.aem.common.data.checkinrest.model.cheangeseats.response.ComfortSeat;
import com.alitalia.aem.common.data.checkinrest.model.getcart.AZCartAncillary;
import com.alitalia.aem.common.data.checkinrest.model.getcart.AZCartPassenger;
import com.alitalia.aem.common.data.checkinrest.model.getcart.AZFlightCart;
import com.alitalia.aem.common.data.checkinrest.model.getcart.AZSegmentCart;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Flight;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerSeat;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Segment;
import com.alitalia.aem.common.messages.checkinrest.*;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.PassengerRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.cheangeseats.request.SeatPassenger;
import com.alitalia.aem.common.data.checkinrest.model.passenger.Passenger;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "changeseatsrest"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinChangeSeatsServlet extends GenericCheckinFormValidatorServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ICheckinDelegate checkInDelegateRest;

	@Reference
	private CheckinSession checkinSession;

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}


	@SuppressWarnings("rawtypes")
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		Map<String,String> additionalParams = new HashMap<String, String>();
		return additionalParams;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		try {
			CheckinSessionContext ctx = getCheckInSessionContext(request);

			logger.info("Entrati in [CheckinChangeSeatsServlet - performSubmit]");

			CheckinChangeSeatsRequest changeSeatsRequest = new CheckinChangeSeatsRequest(IDFactory.getTid(), IDFactory.getSid(request));
			List<SeatPassenger> passengerList = new ArrayList<SeatPassenger>();
			SeatPassenger seatPassenger = new SeatPassenger();
			changeSeatsRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
			changeSeatsRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
			changeSeatsRequest.setConversationID(ctx.conversationId);
			if (request.getParameter("pnr").equals(""))
				changeSeatsRequest.setPnr(ctx.pnrSelectedListDataRender.get(0).getNumber());
			else
				changeSeatsRequest.setPnr(request.getParameter("pnr"));
			changeSeatsRequest.setFlight(request.getParameter("numeroVolo"));
			changeSeatsRequest.setOrigin(request.getParameter("origine"));
			changeSeatsRequest.setDestination(request.getParameter("destinazione"));
			changeSeatsRequest.setDepartureDate(request.getParameter("dataPartenza"));
			changeSeatsRequest.setBookingClass(request.getParameter("bookingClass"));
			seatPassenger.setNewSeat(request.getParameter("nuovoPosto"));
			seatPassenger.setNome(request.getParameter("nome"));
			seatPassenger.setCognome(request.getParameter("cognome"));
			changeSeatsRequest.setBookingClass(request.getParameter("bookingClass"));
			changeSeatsRequest.setAirline(request.getParameter("airline"));


			passengerList.add(seatPassenger);
			changeSeatsRequest.setSeatPassenger(passengerList);

			logger.info("[CheckinChangeSeatsServlet] invocando checkInDelegateRest.changeSeats...");
			CheckinChangeSeatsResponse checkinChangeSeatResponse = checkInDelegateRest.changeSeats(changeSeatsRequest);

			if (checkinChangeSeatResponse != null) {
				if((checkinChangeSeatResponse.getError() != null && !checkinChangeSeatResponse.getError().equals("")) || !checkinChangeSeatResponse.getEsitoCambioPosto().get(0).isEsito()){
					managementError(request, response, checkinChangeSeatResponse.getError());
				}else{
					if(checkinChangeSeatResponse.getConversationID() != null){
						ctx.conversationId = checkinChangeSeatResponse.getConversationID();
					}

					int x1 = 0;
					// Aggiorno il posto sul passeggero
					for (PnrRender pnrSelectedListDataRender : ctx.pnrSelectedListDataRender) {

						int x2 = 0;
						for (FlightsRender flight : pnrSelectedListDataRender.getFlightsRender()) {

                            int x3 = 0;
							for (SegmentRender segment : flight.getSegments()) {

								// Ciclo sui passeggeri
								int x4 = 0;
								for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passengerX : segment.getPassengers())
								{
									if (passengerX.getNome().equals(request.getParameter("nome")) && passengerX.getCognome().equals(request.getParameter("cognome"))) {
										if (passengerX.getPassengerSeats() != null)
										{
											List<PassengerSeat> seats = passengerX.getPassengerSeats();
											for(PassengerSeat seat : seats)
											{
												if (seat.getOrigin().equals(request.getParameter("origine")) && seat.getDepartureDate().equals(request.getParameter("dataPartenza")))
													seat.setNumeroPostoAssegnato(request.getParameter("nuovoPosto"));
											}
											ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengers().get(x4).setPassengerSeats(seats);
										}
									}
									x4 += 1;
								}

								if (segment.getPassengersExtra() != null)
								{
									// Ciclo sui passeggeri extra
									x4 = 0;
									for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra passengerX : segment.getPassengersExtra()) {
										if (passengerX.getPassenger().getNome().equals(request.getParameter("nome")) && passengerX.getPassenger().getCognome().equals(request.getParameter("cognome"))) {
											if (passengerX.getPassengerSeats() != null) {
												List<PassengerSeat> seats = passengerX.getPassengerSeats();
												for (PassengerSeat seat : seats) {
													if (seat.getOrigin().equals(request.getParameter("origine")) && seat.getDepartureDate().equals(request.getParameter("dataPartenza")))
														seat.setNumeroPostoAssegnato(request.getParameter("nuovoPosto"));
												}
												ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengersExtra().get(x4).setPassengerSeats(seats);
												ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengersExtra().get(x4).getPassenger().setPassengerSeats(seats);

											}
										}
										x4 += 1;
									}
								}

								x3 += 1;

							}
							x2 += 1;
						}
						x1 += 1;
					}

					//response.sendRedirect(successPage);
					managementSucces(request, response, checkinChangeSeatResponse);
				}
			}
			else {
				logger.error("[CheckinChangeSeatsServlet] - Errore durante l'invocazione del servizio." );
			}


		}
		catch (Exception e) {
			throw new RuntimeException("[CheckinChangeSeatsServlet] - Errore durante l'invocazione del servizio.", e);
		}
	}

	private void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error){

		logger.info("[CheckinChangeSeatsServlet] [managementError] error =" + error );

		Gson gson = new Gson();

		com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);

		TidyJSONWriter json;
		try {
			json = new TidyJSONWriter(response.getWriter());

			response.setContentType("application/json");

			json.object();
			json.key("isError").value(true);
			json.key("errorMessage").value(errorObj.getErrorMessage());
			json.key("sabreStatusCode").value(errorObj.getSabreStatusCode());
			json.key("conversationID").value(errorObj.getConversationID());
			json.endObject();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
	}

	private void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response, CheckinChangeSeatsResponse checkinChangeSeatResponse){

		logger.info("[CheckinChangeSeatsServlet] [managementSucces] ...");

		try {

			CheckinSessionContext ctx = getCheckInSessionContext(request);

			String comfortSeat = request.getParameter("postoComfort");
			if(comfortSeat.equals("false")){
				String priceComfortSeat = request.getParameter("pricePostoComfort");
				
				String pnr;
				String pnrExtra;
				Boolean clearAncillariesNeeded = true;
				if (request.getParameter("pnr").equals("")) {
					pnr = ctx.pnrSelectedListDataRender.get(0).getNumber();
				} else {
					pnr = request.getParameter("pnr");
				}

				if(ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().get(0).getPassengersExtra() != null){
					pnrExtra = ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().get(0).getPassengersExtra().get(0).getNumero();
					if(pnr.equals(pnrExtra)){
						clearAncillariesNeeded = false;
					}
				}
				
				if (priceComfortSeat.equals("") && clearAncillariesNeeded) {
//					pulisco il pnr da eventuali posti comfort precedentemente scelti ma non pagati
					CheckinClearAncillarySessionEndResponse checkinClearAncillarySessionEndResponse = new CheckinClearAncillarySessionEndResponse();
					CheckinClearAncillarySessionEndRequest checkinClearAncillarySessionEndRequest = new CheckinClearAncillarySessionEndRequest();
					checkinClearAncillarySessionEndRequest.setConversationID(ctx.conversationId);
					checkinClearAncillarySessionEndRequest.setPnr(ctx.pnr);
					checkinClearAncillarySessionEndRequest.setLanguage(ctx.language);
					checkinClearAncillarySessionEndRequest.setMarket(ctx.market);
					
					checkinClearAncillarySessionEndResponse = checkInDelegateRest.clearAncillaySessionEnd(checkinClearAncillarySessionEndRequest);
					
					if (checkinClearAncillarySessionEndResponse.getOutcome().get(0) != null) {
							checkinClearAncillarySessionEndResponse.getOutcome().get(0);
//							posto gratuito quindi riaccetto il passeggero
							checkinSession.checkinSelectedPassengerAfterPayment(request, ctx);
					}
				}
                setNewPassengerSeat(checkinChangeSeatResponse.getEsitoCambioPosto().get(0).getNewSeat(), ctx, request);
			} else {
				CheckinGetCartResponse result = checkinSession.getCart(request, ctx);
				if(comfortSeatToPay(result, request)){
					ctx.comfortSeat = new ArrayList<>();
					ComfortSeat comfortSeat1 = new ComfortSeat();

					comfortSeat1.setSeatNumber(checkinChangeSeatResponse.getEsitoCambioPosto().get(0).getNewSeat());
					comfortSeat1.setFlightNumber(request.getParameter("numeroVolo"));
					comfortSeat1.setNome(request.getParameter("nome"));
					comfortSeat1.setCognome(request.getParameter("cognome"));
					ctx.comfortSeat.add(comfortSeat1);

				} else {
					setNewPassengerSeat(checkinChangeSeatResponse.getEsitoCambioPosto().get(0).getNewSeat(), ctx, request);
				}
            }

			TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());
	
			jsonOutput.object();
			jsonOutput.key("esito").value(checkinChangeSeatResponse.getEsitoCambioPosto().get(0).isEsito());
			jsonOutput.key("newSeat").value(checkinChangeSeatResponse.getEsitoCambioPosto().get(0).getNewSeat());
			jsonOutput.key("cognome").value(checkinChangeSeatResponse.getEsitoCambioPosto().get(0).getCognome());
			jsonOutput.key("nome").value(checkinChangeSeatResponse.getEsitoCambioPosto().get(0).getNome());
			jsonOutput.endObject();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	

	}

	private boolean comfortSeatToPay(CheckinGetCartResponse result, SlingHttpServletRequest request) {
		boolean comfortSeatToPay = false;

		if (result.getCart().getCheckinCart() != null) {
			if (!result.getCart().getCheckinCart().getAZFlightCart().isEmpty()) {
				for (AZFlightCart azFlightCart : result.getCart().getCheckinCart().getAZFlightCart()){
					if(!azFlightCart.getAZSegmentCart().isEmpty()){
						for (AZSegmentCart azSegmentCart : azFlightCart.getAZSegmentCart()){
							if (azSegmentCart.getFlightNumber().equals(request.getParameter("numeroVolo")) && !azSegmentCart.getAZCartPassenger().isEmpty()){
								for(AZCartPassenger azCartPassenger : azSegmentCart.getAZCartPassenger()){
									if (azCartPassenger.getFirstName().equals(request.getParameter("nome")) && azCartPassenger.getFirstName().equals(request.getParameter("cognome")) && !azCartPassenger.getAZCartAncillary().isEmpty()){
										for (AZCartAncillary  azCartAncillary : azCartPassenger.getAZCartAncillary()){
											switch (azCartAncillary.getGroup()){
												case "SA":
													comfortSeatToPay = true;
													break;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return comfortSeatToPay;
	}

	private void setNewPassengerSeat(String newSeat, CheckinSessionContext ctx, SlingHttpServletRequest request) {
        for (PnrRender pnrRender: ctx.pnrSelectedListDataRender) {
            for (FlightsRender flightsRender : pnrRender.getFlightsRender()) {
                for (SegmentRender segmentRender : flightsRender.getSegments()){
                    if(segmentRender.getFlight().equals(request.getParameter("numeroVolo"))){
                        for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passenger: segmentRender.getPassengers()){
                            if (passenger.getNome().equals(request.getParameter("nome")) && passenger.getCognome().equals(request.getParameter("cognome"))){
                                passenger.setNumeroPostoAssegnato(newSeat);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}
