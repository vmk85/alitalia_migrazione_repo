package com.alitalia.aem.consumer.checkin.model;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryEticketData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryUpgradeDetailData;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillarySeatDetailData;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.render.MmbAncillaryCartPassengerRender;
import com.alitalia.aem.consumer.mmb.render.MmbAncillaryCartRender;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;
import com.alitalia.aem.consumer.mmb.render.MmbThankyouAnalyticsRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinAncillaryCart  extends GenericCheckinModel {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private CheckinSession checkinSession;
	
	private List<MmbAncillaryCartPassengerRender> passengerRenderList;
	private MmbPriceRender cartTotalPrice;
	private Integer cartTotalQuantity;
	private Boolean isInsuranceAddedToCart;
	private MmbPriceRender insurancePrice; 
	private String paymentCreditCardNumberHide;
	private String paymentCreditCardType;
	private MmbThankyouAnalyticsRender purchasedInfo;
	private String linkPayment;
	private boolean ancillaryPage;
	private boolean comfirmationPage;
	private boolean showButtonCart; 
	
	@PostConstruct
	protected void initModel() throws IOException {
		try{
			initBaseModel(request);
			
			this.cartTotalPrice = new MmbPriceRender(ctx.cartTotalAmount, ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat);
			this.cartTotalQuantity = 0;
			
			this.paymentCreditCardNumberHide = ctx.paymentCreditCardNumberHide == null ? "" 
					: ctx.paymentCreditCardNumberHide;
			this.paymentCreditCardType = ctx.paymentCreditCardType == null ? "" : ctx.paymentCreditCardType;
			
			/***/
			List <CheckinAncillaryPassengerData> ancillaryPassengersData = ctx.cart.getPassengers();
			
			List<MmbAncillaryStatusEnum> ancillaryStatusExtraBaggage = new ArrayList<MmbAncillaryStatusEnum>();
			List<MmbAncillaryStatusEnum> ancillaryStatusSeat = new ArrayList<MmbAncillaryStatusEnum>();
			
			String path = request.getRequestURL().toString();
			if (request.getRequestPathInfo().getSelectorString() != null &&
					!request.getRequestPathInfo().getSelectorString().isEmpty()) {
						path = request.getRequestURL().toString().replace(
								request.getRequestPathInfo().getSelectorString() + ".", "");
			}
			
			comfirmationPage = path.endsWith(configuration.getCheckinAncillaryConfirmationPage());
			ancillaryPage = path.endsWith(configuration.getCheckinAncillaryPage());
			
			if (!isWCMEnabled()) {
				if (comfirmationPage) {
					ancillaryStatusExtraBaggage.add(MmbAncillaryStatusEnum.ISSUED);
				}
				else {
					ancillaryStatusExtraBaggage.add(MmbAncillaryStatusEnum.TO_BE_ISSUED);
					ancillaryStatusSeat.add(MmbAncillaryStatusEnum.TO_BE_ISSUED);
				}
				ancillaryStatusSeat.add(MmbAncillaryStatusEnum.ISSUED);
			}
			
			this.passengerRenderList = new LinkedList<MmbAncillaryCartPassengerRender>(); 
			for (CheckinAncillaryPassengerData ancillaryPassengerData : ancillaryPassengersData) {
				MmbAncillaryCartPassengerRender passengerRender = new MmbAncillaryCartPassengerRender();
				passengerRender.setId(ancillaryPassengerData.getId());
				passengerRender.setFullName(ancillaryPassengerData.getName() + " " + ancillaryPassengerData.getLastName());
				passengerRender.setTicketNumber(
						(ancillaryPassengerData.getEtickets() == null || ancillaryPassengerData.getEtickets().size() == 0) ? "" :
							ancillaryPassengerData.getEtickets().get(0).getNumber());
				passengerRender.setQuantity(0);
				
				passengerRender.setSeatTotalPrice(
						new MmbPriceRender(new BigDecimal(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				passengerRender.setPaymentSeatTotalPrice(
						new MmbPriceRender(new BigDecimal(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				passengerRender.setExtraBaggageTotalPrice(
						new MmbPriceRender(new BigDecimal(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				passengerRender.setUpgradeTotalPrice(
						new MmbPriceRender(new BigDecimal(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				
				for (CheckinAncillaryEticketData ticket : ancillaryPassengerData.getEtickets()) {
					
					/* Seat */ 
					MmbAncillaryCartRender seatCartRender = new MmbAncillaryCartRender();
					seatCartRender.setStandardSeats(new ArrayList<String>());
					seatCartRender.setComfortSeats(new ArrayList<String>());
					seatCartRender.setEmds(new ArrayList<String>());
					BigDecimal comfortSeatPartialPrice = new BigDecimal(0);
					BigDecimal paymentSeatPartialPrice = new BigDecimal(0);
					Integer seatQuantity = 0;
					List<MmbAncillaryData> seatAncillaryList = checkinSession.getAncillaryByTypeStatusAndETicket(
							ctx, MmbAncillaryTypeEnum.SEAT, ancillaryStatusSeat, ticket.getNumber());
					
					for (MmbAncillaryData seatAncillary : seatAncillaryList) {
						
						boolean standardSeat = seatAncillary.getAncillaryDetail() == null ||
								!((MmbAncillarySeatDetailData) seatAncillary.getAncillaryDetail()).isComfort();
						
						if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(seatAncillary.getAncillaryStatus()) || standardSeat ||
								(MmbAncillaryStatusEnum.ISSUED.equals(seatAncillary.getAncillaryStatus()) && comfirmationPage)) {
							
							
							seatQuantity += seatAncillary.getQuantity();
							
							String seatValue = StringUtils.stripStart(((MmbAncillarySeatDetailData) seatAncillary.getAncillaryDetail()).getSeat(), "0");
							if (standardSeat) {
								seatCartRender.getStandardSeats().add(seatValue);
								paymentSeatPartialPrice = paymentSeatPartialPrice.add(seatAncillary.getAmount());
							} else {
								seatCartRender.getComfortSeats().add(seatValue);
								comfortSeatPartialPrice = comfortSeatPartialPrice.add(seatAncillary.getAmount());
							}
							seatCartRender.getEmds().add(seatAncillary.getEmd() == null ? "" : seatAncillary.getEmd());
							
						}
					}
					
					seatCartRender.setQuantity(seatQuantity);
					passengerRender.setSeatTotalQuantity(passengerRender.getSeatTotalQuantity() + seatQuantity);
					this.cartTotalQuantity += seatQuantity;
					passengerRender.setQuantity(passengerRender.getQuantity() + seatQuantity);
					passengerRender.getSeatItemList().add(seatCartRender);
					passengerRender.getSeatTotalPrice().increaseAmount(comfortSeatPartialPrice);
					passengerRender.getPaymentSeatTotalPrice().increaseAmount(paymentSeatPartialPrice);
					
					/* Extra Baggage */
					MmbAncillaryCartRender extraBaggageCartRender = new MmbAncillaryCartRender();
					BigDecimal extraBaggagePartialPrice = new BigDecimal(0);
					Integer extraBaggageQuantity = 0;
					List<MmbAncillaryData> extraBaggageAncillaryList = checkinSession.getAncillaryByTypeStatusAndETicket(
							ctx, MmbAncillaryTypeEnum.EXTRA_BAGGAGE, ancillaryStatusExtraBaggage, ticket.getNumber());
					for (MmbAncillaryData extraBaggageAncillary : extraBaggageAncillaryList) {
							extraBaggagePartialPrice = extraBaggagePartialPrice.add(extraBaggageAncillary.getAmount());
							extraBaggageQuantity += extraBaggageAncillary.getQuantity();
							extraBaggageCartRender.setEmd(extraBaggageAncillary.getEmd());
					}
					extraBaggageCartRender.setQuantity(extraBaggageQuantity);
					passengerRender.setExtraBaggageTotalQuantity(passengerRender.getExtraBaggageTotalQuantity() + extraBaggageQuantity);
					this.cartTotalQuantity += extraBaggageQuantity;
					passengerRender.setQuantity(passengerRender.getQuantity() + extraBaggageQuantity);
					extraBaggageCartRender.setI18nKeyDescription(extraBaggageQuantity == 1 ? 
							MmbConstants.CART_EXTRABAGGAGE_TYPE_LABEL : 
								MmbConstants.CART_EXTRABAGGAGE_TYPE_LABEL_PLURAL);
					passengerRender.getExtraBaggageItemList().add(extraBaggageCartRender);
					passengerRender.getExtraBaggageTotalPrice().increaseAmount(extraBaggagePartialPrice);
					
					/* Upgrade */ 
					MmbAncillaryCartRender upgradeCartRender = new MmbAncillaryCartRender();
					BigDecimal upgradePartialPrice = new BigDecimal(0);
					Integer upgradeQuantity = 0;
					List<MmbAncillaryData> upgradeAncillaries = checkinSession.getAncillaryByTypeStatusAndETicket(
							ctx, MmbAncillaryTypeEnum.UPGRADE, ancillaryStatusExtraBaggage, ticket.getNumber());
					
					for (MmbAncillaryData upgradeAncillary : upgradeAncillaries) {
						upgradePartialPrice = upgradePartialPrice.add(upgradeAncillary.getAmount());
						upgradeQuantity += upgradeAncillary.getQuantity();
						upgradeCartRender.setSeat(
								((CheckinAncillaryUpgradeDetailData) upgradeAncillary.getAncillaryDetail()).getSeat());
						upgradeCartRender.setEmd(upgradeAncillary.getEmd());
						MmbCompartimentalClassEnum upType = ((CheckinAncillaryUpgradeDetailData) 
								upgradeAncillary.getAncillaryDetail()).getCompartimentalClass();
						upgradeCartRender.setI18nKeyDescription(checkinSession.getBusinessCompClass(ctx).equals(upType) 
								? CheckinConstants.CHECKIN_UPGRADE_BUSINESS_CART_LABEL 
								: CheckinConstants.CHECKIN_UPGRADE_PREMIUM_ECONOMY_CART_LABEL);
					}
					upgradeCartRender.setQuantity(upgradeQuantity);
					passengerRender.setUpgradeTotalQuantity(passengerRender.getUpgradeTotalQuantity() + upgradeQuantity);
					this.cartTotalQuantity += upgradeQuantity;
					passengerRender.setQuantity(passengerRender.getQuantity() + upgradeQuantity);
					
					passengerRender.getUpgradeItemList().add(upgradeCartRender);
					passengerRender.getUpgradeTotalPrice().increaseAmount(upgradePartialPrice);
					
				}
				passengerRenderList.add(passengerRender);
			}
			
			/* INSURANCE */
			this.isInsuranceAddedToCart = ctx.insuranceAddedToCart;
			if (this.isInsuranceAddedToCart) {
				this.insurancePrice = new MmbPriceRender(ctx.insurancePolicyData.getTotalInsuranceCost(), ctx.currencyCode, 
						ctx.currencySymbol, ctx.currentNumberFormat);
				this.cartTotalQuantity += 1;
			}
			
			linkPayment = AlitaliaUtils.findSiteBaseExternalUrl(
					request.getResource(), false)
					+ configuration.getCheckinPaymentPage();
			
			/****/
			
			showButtonCart = false;
			if (request.getRequestPathInfo().getSelectorString() != null &&
					!request.getRequestPathInfo().getSelectorString().isEmpty()) {
						path = request.getRequestURL().toString().replace(
								request.getRequestPathInfo().getSelectorString() + ".", "");
			}
			if (!isWCMEnabled()) {
				if (path.endsWith(configuration.getCheckinAncillaryPage())) {
					showButtonCart = true;
				}
			}
			
			
		}
		catch(Exception e){
			logger.error("Unexpectd error", e);
		}
		
	}

	public List<MmbAncillaryCartPassengerRender> getPassengerRenderList() {
		return passengerRenderList;
	}

	public MmbPriceRender getCartTotalPrice() {
		return cartTotalPrice;
	}

	public Integer getCartTotalQuantity() {
		return cartTotalQuantity;
	}

	public Boolean getIsInsuranceAddedToCart() {
		return isInsuranceAddedToCart;
	}

	public MmbPriceRender getInsurancePrice() {
		return insurancePrice;
	}

	public String getPaymentCreditCardNumberHide() {
		return paymentCreditCardNumberHide;
	}

	public String getPaymentCreditCardType() {
		return paymentCreditCardType;
	}

	public MmbThankyouAnalyticsRender getPurchasedInfo() {
		return purchasedInfo;
	}
	
	public String getLinkPayment(){
		return linkPayment;
	}

	public boolean isAncillaryPage() {
		return ancillaryPage;
	}

	public boolean isComfirmationPage() {
		return comfirmationPage;
	}

	public boolean isShowButtonCart() {
		return showButtonCart;
	}

	public void setShowButtonCart(boolean showButtonCart) {
		this.showButtonCart = showButtonCart;
	}
	
}
