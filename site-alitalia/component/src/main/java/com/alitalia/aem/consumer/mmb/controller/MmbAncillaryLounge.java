package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPassengerNoteEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup.AncillariesGroupType;
import com.alitalia.aem.consumer.mmb.model.MmbFlightsGroup;
import com.alitalia.aem.consumer.mmb.render.MmbAncillaryLoungePassengerRender;
import com.alitalia.aem.consumer.mmb.render.MmbAncillaryLoungeRender;
import com.alitalia.aem.consumer.mmb.render.MmbFlightDataRender;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;
import com.alitalia.aem.consumer.utils.Lists;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbAncillaryLounge extends MmbSessionAncillaryGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private MmbSession mmbSession;
	
	private boolean allowClearCart = false;
	private List<MmbAncillaryLoungePassengerRender> loungePassengers;
	private List<MmbFlightsGroup> flightsGroupList;

	private int purchasableLoungeNum;
	private int selectedLoungeNum;
	
	@PostConstruct
	protected void initModel() throws IOException {
		initBaseModel(request);
		
		this.allowClearCart = false;
		this.flightsGroupList = (MmbFlightsGroup.groupByRouteType(ctx.route.getFlights()));
		this.loungePassengers = new ArrayList<MmbAncillaryLoungePassengerRender>();
		this.purchasableLoungeNum = 0;
		this.selectedLoungeNum = 0;
		// first, group by passenger
		List<MmbAncillariesGroup> groupsByPassenger = MmbAncillariesGroup.createGroups(
			AncillariesGroupType.BY_PASSENGER, ctx.route, ctx.cart.getPassengers(), 
			ctx.cart.getAncillaries(), MmbAncillaryTypeEnum.VIP_LOUNGE);
		for (MmbAncillariesGroup passengerGroup : groupsByPassenger) {
			
			// skip passengers without any lounge ancillaries
			if (passengerGroup.getAncillaries().size() == 0) {
				continue;
			}
			
			// prepare passenger header render
			MmbAncillaryLoungePassengerRender loungePassenger = new MmbAncillaryLoungePassengerRender();
			loungePassenger.setFullName(passengerGroup.getPassengerData().getName() + 
					" " + passengerGroup.getPassengerData().getLastName());
			if (MmbPassengerNoteEnum.INFANT_ACCOMPANIST.equals(
					passengerGroup.findMmbPassengerDataForFlight(0).getNote())) {
				loungePassenger.setHasInfant(true);
			}
			if (MmbPassengerNoteEnum.CHILD.equals(
					passengerGroup.findMmbPassengerDataForFlight(0).getNote())) {
				loungePassenger.setChild(true);
			}
			loungePassenger.setDisplayInFeedback(false);
			loungePassenger.setTotalToBeIssuedPrice(
					new MmbPriceRender(BigDecimal.valueOf(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
			loungePassenger.setTotalToBeIssuedQuantity(0);
			loungePassengers.add(loungePassenger);
			
			// for each passenger, group again by flight
			List<MmbAncillariesGroup> groups = MmbAncillariesGroup.createGroups(
					AncillariesGroupType.BY_PASSENGER_AND_FLIGHT, ctx.route, 
					Lists.asList(passengerGroup.getPassengerData()), passengerGroup.getAncillaries(), 
					MmbAncillaryTypeEnum.VIP_LOUNGE);
			for (MmbAncillariesGroup group : groups) {
			
				MmbFlightData flight = Lists.getFirst(group.getFlights());
				MmbAncillaryLoungeRender loungeRender = new MmbAncillaryLoungeRender(group);
				loungePassenger.getLoungeSelections().add(loungeRender);
				loungeRender.setRouteTypeLabel(getRouteTypeLabel(group));
				loungeRender.setFlight(new MmbFlightDataRender(Lists.getFirst(group.getFlights())));
				
				if (group.getAncillaries().size() == 0) {
					// no ancillary for this passenger/route
					loungeRender.setIdentifier("");
					
				} else {
					
					// determine the related flight group render
					MmbFlightsGroup relatedFlightsGroup = null;
					for (MmbFlightsGroup flightGroup : this.flightsGroupList) {
						if (flight.getType().equals(flightGroup.getType()) 
								&& flight.getRouteId().equals(flightGroup.getRouteId())) {
							relatedFlightsGroup = flightGroup;
							break;
						}
					}
					
					MmbAncillaryData availableAncillary = 
							group.getAncillaryInStatus(
									MmbAncillaryStatusEnum.AVAILABLE);
					MmbAncillaryData addedAncillary = 
							group.getAncillaryInStatus(
									MmbAncillaryStatusEnum.TO_BE_ISSUED);
					MmbAncillaryData issuedOrPreviouslyIssuedAncillary = 
							group.getAncillaryInStatuses(new MmbAncillaryStatusEnum[] {
									MmbAncillaryStatusEnum.ISSUED,
									MmbAncillaryStatusEnum.PREVIOUSLY_ISSUED});
					MmbFlightDataRender flightRender = loungeRender.getFlight();
					if (availableAncillary != null) {
						// ancillary available
						
						if(flightRender.isBus()){
							loungeRender.setAllowModify(false);
						} else{
							loungeRender.setAllowModify(true);
						}
						
						loungeRender.setIdentifier(
								getAncillariesSelectionIdentifier(availableAncillary));
						loungeRender.setCurrentValue(false);
						loungeRender.setPrice(
								mmbSession.getAncillaryDisplayedPriceInfo(ctx, availableAncillary, 1));
						
						
						
						purchasableLoungeNum += availableAncillary.getQuantity() > 0 
								? availableAncillary.getQuantity() : 1;
						
					} else if (addedAncillary != null) {
						// ancillary added to cart
						loungeRender.setIdentifier(
								getAncillariesSelectionIdentifier(addedAncillary));
						loungeRender.setCurrentValue(true);
						loungeRender.setPrice(
								mmbSession.getAncillaryDisplayedPriceInfo(ctx, addedAncillary, null));
						loungeRender.setAllowModify(true);

						this.allowClearCart = true;
						loungePassenger.setDisplayInFeedback(true);
						loungePassenger.setTotalToBeIssuedQuantity(
								loungePassenger.getTotalToBeIssuedQuantity() + 1);
						loungePassenger.getTotalToBeIssuedPrice().increaseAmount(
								loungeRender.getPrice().getAmount());
						relatedFlightsGroup.getRelatedAncillaries().add(addedAncillary);
						selectedLoungeNum += addedAncillary.getQuantity() > 0 
								? addedAncillary.getQuantity() : 1;
						
					} else if (issuedOrPreviouslyIssuedAncillary != null) {
						// ancillary already issued
						loungeRender.setIdentifier(
								getAncillariesSelectionIdentifier(issuedOrPreviouslyIssuedAncillary));
						loungeRender.setCurrentValue(true);
						loungeRender.setAllowModify(false);
						loungeRender.setPrice(null);
						relatedFlightsGroup.getRelatedAncillaries().add(issuedOrPreviouslyIssuedAncillary);
						
					}
					
				}
			}
			
		}
		
	}
	
	public boolean isAllowClearCart() {
		return allowClearCart;
	}
	
	public List<MmbFlightsGroup> getFlightsGroupList() {
		return flightsGroupList;
	}
	
	public List<MmbAncillaryLoungePassengerRender> getLoungePassengers() {
		return loungePassengers;
	}

	public int getPurchasableLoungeNum() {
		return purchasableLoungeNum;
	}

	public int getSelectedLoungeNum() {
		return selectedLoungeNum;
	}
	
}

