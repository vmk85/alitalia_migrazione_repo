package com.alitalia.aem.consumer.booking.model;

public class BrandPageData {

	private String title;
	private String path;
	private String id;
	
	public BrandPageData() {
		super();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "BrandPageData [title=" + title + ", path=" + path + ", id="
				+ id + "]";
	}
	
}
