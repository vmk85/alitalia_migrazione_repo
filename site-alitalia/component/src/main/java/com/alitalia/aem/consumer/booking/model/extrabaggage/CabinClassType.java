package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Enumerazione per la tipologia della classe di cabina. 
*/
public enum CabinClassType
{
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("Economy")] Economy = 0,
	Economy(0),
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("Business")] Business,
	Business(1),
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("PremiumEconomy")] PremiumEconomy
	PremiumEconomy(2);

	public static final int SIZE = java.lang.Integer.SIZE;

	private int intValue;
	private static java.util.HashMap<Integer, CabinClassType> mappings;
	private static java.util.HashMap<Integer, CabinClassType> getMappings()
	{
		if (mappings == null)
		{
			synchronized (CabinClassType.class)
			{
				if (mappings == null)
				{
					mappings = new java.util.HashMap<Integer, CabinClassType>();
				}
			}
		}
		return mappings;
	}

	private CabinClassType(int value)
	{
		intValue = value;
		getMappings().put(value, this);
	}

	public int getValue()
	{
		return intValue;
	}

	public static CabinClassType forValue(int value)
	{
		return getMappings().get(value);
	}
}