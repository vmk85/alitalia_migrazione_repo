package com.alitalia.aem.consumer.checkinrest.render;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Flight;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;

public class PnrListDataRender {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private List<PnrRender> pnrListDataRender = null;

	public List<PnrRender> getPnrListDataRender(List<Pnr> pnrList, CheckinSessionContext ctx, int total, int executed, int notAllowed){

        List<PnrRender> pnrListDataRender = new ArrayList<>();

        for(Pnr pnr : pnrList){
			PnrRender pnrRender = new PnrRender();
			pnrRender.setCheckinAllFlights("N");
			
            List<FlightsRender> flightsRender = new ArrayList<>();
			for(Flight flight : pnr.getFlights()){
                FlightsRender fr = new FlightsRender();
				fr = setFlightsRender(flight, pnr.getRoundTrip(), total, executed, notAllowed, ctx);
				flightsRender.add(fr);
            }
            pnrRender.setNumber(pnr.getNumber());
            pnrRender.setRoundTrip(pnr.getRoundTrip());
            pnrRender.setFlightsRender(flightsRender);
            pnrRender.setAllsegments();
            pnrRender.setVoloAlitalia(pnr.getVoloAlitalia());
            pnrListDataRender.add(pnrRender);
		}

		pnrListDataRender = dateRenderForPnrListDataRender(pnrListDataRender, ctx.language, ctx.market);
		return pnrListDataRender;

	}

	public List<PnrRender> getPnrSelectedListDataRender(List<Pnr> pnrListData, CheckinSessionContext ctx, String pnrSelected, int total, int executed, int notAllowed, String checkinAllFlights, int[] flightitinerary ){

		pnrListDataRender = new ArrayList<>();

		List<FlightsRender> flightsRender = new ArrayList<>();

		for(Pnr pnr : pnrListData){
			if(pnr.getNumber().equals(pnrSelected)){
				PnrRender pnrRender = new PnrRender();
				pnrRender.setCheckinAllFlights(checkinAllFlights);
				
				FlightsRender fr = null;
				if(flightitinerary.length > 1){
					for(int i = 0; i < flightitinerary.length; i++ ){
						fr = new FlightsRender();
						fr = setFlightsRender(pnr.getFlights().get(flightitinerary[i]), pnr.getRoundTrip(), total, executed, notAllowed, ctx);
						flightsRender.add(fr);
					}					
				}else{
					fr = new FlightsRender();
					fr = setFlightsRender(pnr.getFlights().get(flightitinerary[0]), pnr.getRoundTrip(), total, executed, notAllowed, ctx);
					flightsRender.add(fr);
				}
				
				pnrRender.setNumber(pnr.getNumber());
		        pnrRender.setRoundTrip(pnr.getRoundTrip());
		        pnrRender.setFlightsRender(flightsRender);
		        pnrRender.setAllsegments();
				pnrListDataRender.add(pnrRender);
			}
		}

		pnrListDataRender = dateRenderForPnrListDataRender(pnrListDataRender, ctx.language, ctx.market);
		return pnrListDataRender;

	}

	public List<PnrRender> dateRenderForPnrListDataRender(List<PnrRender> pnrListDataRender, String language, String market) {
		String dateFormatter = "yyyy-MM-dd'T'HH:mm:ss'Z'";

		for (PnrRender pnrRender : pnrListDataRender){
			for (FlightsRender flightRender : pnrRender.getFlightsRender()){

				if ((market.toLowerCase().equals("us") || market.toLowerCase().equals("ca"))) {
				CheckinDateRender dateRenderFlightsArrival = new CheckinDateRender(flightRender.getArrivalDate(), language, market, dateFormatter);
				String arrivalFlightsDateStr = dateRenderFlightsArrival.getAmericanStandardDateCheckin();
				String arrivalHoursDateStr = dateRenderFlightsArrival.getStandardHoursCheckin();
//				logger.info("[CheckinInPnrRenderModel] [dateRenderForPnrListDataRender] ArrivalDate : " + flightRender.getArrivalDate() + " ArrivalDateRender : " + arrivalFlightsDateStr);

				flightRender.setArrivalDate(arrivalFlightsDateStr);
				flightRender.setArrivalTime(arrivalHoursDateStr);

				// Data partenza originale non formattata
				//flightRender.setOriginalDepartureDate(flightRender.getDepartureDate().toString());

				CheckinDateRender dateRenderFlightsDeparture = new CheckinDateRender(flightRender.getDepartureDate(), language, market, dateFormatter);
				String departureFlightsDateStr = dateRenderFlightsDeparture.getAmericanStandardDateCheckin();
				String departureHoursDateStr = dateRenderFlightsDeparture.getStandardHoursCheckin();
//				logger.info("[CheckinInPnrRenderModel] [dateRenderForPnrListDataRender] DepartureDate : " + flightRender.getDepartureDate() + " DepartureDateRender : " + departureFlightsDateStr);

				flightRender.setDepartureDate(departureFlightsDateStr);
				flightRender.setDepartureTime(departureHoursDateStr);
				
				for (SegmentRender segment : flightRender.getSegments()){

					CheckinDateRender dateRenderSegmentArrival = new CheckinDateRender(segment.getArrivalDate(), language, market, dateFormatter);
					String arrivalSegmentDateStr = dateRenderSegmentArrival.getAmericanStandardDateCheckin();
					String arrivalHoursSegmentDateStr = dateRenderSegmentArrival.getStandardHoursCheckin();
//					logger.info("[CheckinInPnrRenderModel] [dateRenderForPnrListDataRender] ArrivalSegmentDate : " + segment.getArrivalDate() + " ArrivalSegmentDateRender : " + arrivalSegmentDateStr);

					segment.setArrivalDate(arrivalSegmentDateStr );
					segment.setArrivalTime(arrivalHoursSegmentDateStr);

					//segment.setOriginalDepartureDate(segment.getDepartureDate().toString());

					CheckinDateRender dateRenderSegmentDeparture = new CheckinDateRender(segment.getDepartureDate(), language, market, dateFormatter);
					String departureSegmentDateStr = dateRenderSegmentDeparture.getAmericanStandardDateCheckin();
					String departureHoursSegmentDateStr = dateRenderSegmentDeparture.getStandardHoursCheckin();
//					logger.info("[CheckinInPnrRenderModel] [dateRenderForPnrListDataRender] DepartureSegmentDate : " + segment.getDepartureDate() + " DepartureSegmentDateRender : " + departureSegmentDateStr);

					segment.setDepartureDate(departureSegmentDateStr);
					segment.setDepartureTime(departureHoursSegmentDateStr);
					}
				}
				else {
					CheckinDateRender dateRenderFlightsArrival = new CheckinDateRender(flightRender.getArrivalDate(), language, market, dateFormatter);
					String arrivalFlightsDateStr = dateRenderFlightsArrival.getStandardDateCheckin();
					String arrivalHoursDateStr = dateRenderFlightsArrival.getStandardHoursCheckin();
//					logger.info("[CheckinInPnrRenderModel] [dateRenderForPnrListDataRender] ArrivalDate : " + flightRender.getArrivalDate() + " ArrivalDateRender : " + arrivalFlightsDateStr);

					flightRender.setArrivalDate(arrivalFlightsDateStr);
					flightRender.setArrivalTime(arrivalHoursDateStr);

					// Data partenza originale non formattata
					//flightRender.setOriginalDepartureDate(flightRender.getDepartureDate().toString());

					CheckinDateRender dateRenderFlightsDeparture = new CheckinDateRender(flightRender.getDepartureDate(), language, market, dateFormatter);
					String departureFlightsDateStr = dateRenderFlightsDeparture.getStandardDateCheckin();
					String departureHoursDateStr = dateRenderFlightsDeparture.getStandardHoursCheckin();
//					logger.info("[CheckinInPnrRenderModel] [dateRenderForPnrListDataRender] DepartureDate : " + flightRender.getDepartureDate() + " DepartureDateRender : " + departureFlightsDateStr);

					flightRender.setDepartureDate(departureFlightsDateStr);
					flightRender.setDepartureTime(departureHoursDateStr);

					for (SegmentRender segment : flightRender.getSegments()){

						CheckinDateRender dateRenderSegmentArrival = new CheckinDateRender(segment.getArrivalDate(), language, market, dateFormatter);
						String arrivalSegmentDateStr = dateRenderSegmentArrival.getStandardDateCheckin();
						String arrivalHoursSegmentDateStr = dateRenderSegmentArrival.getStandardHoursCheckin();
//						logger.info("[CheckinInPnrRenderModel] [dateRenderForPnrListDataRender] ArrivalSegmentDate : " + segment.getArrivalDate() + " ArrivalSegmentDateRender : " + arrivalSegmentDateStr);

						segment.setArrivalDate(arrivalSegmentDateStr );
						segment.setArrivalTime(arrivalHoursSegmentDateStr);

						//segment.setOriginalDepartureDate(segment.getDepartureDate().toString());

						CheckinDateRender dateRenderSegmentDeparture = new CheckinDateRender(segment.getDepartureDate(), language, market, dateFormatter);
						String departureSegmentDateStr = dateRenderSegmentDeparture.getStandardDateCheckin();
						String departureHoursSegmentDateStr = dateRenderSegmentDeparture.getStandardHoursCheckin();
//						logger.info("[CheckinInPnrRenderModel] [dateRenderForPnrListDataRender] DepartureSegmentDate : " + segment.getDepartureDate() + " DepartureSegmentDateRender : " + departureSegmentDateStr);

						segment.setDepartureDate(departureSegmentDateStr);
						segment.setDepartureTime(departureHoursSegmentDateStr);
					}
				}
			}
		}

		return pnrListDataRender;
	}
	
	
	public FlightsRender setFlightsRender(Flight f, Boolean roundTrip, int total, int executed, int notAllowed, CheckinSessionContext ctx){
		FlightsRender fr = new FlightsRender();
		
		fr.setArrivalDate(f.getArrivalDate());
		fr.setDepartureDate(f.getDepartureDate());
		fr.setDestination(f.getDestination());
		fr.setOriginalDepartureDate(f.getDepartureDate());
		fr.setIsBpPrintPermitted(f.getIsBpPrintPermitted());
		fr.setIsWebCheckinPermitted(f.getIsWebCheckinPermitted());
		fr.setOrigin(f.getOrigin());
		fr.setTooLate(f.getTooLate());
		fr.setVoloAlitalia(f.getVoloAlitalia());
		
		//Checki-In **** Deep Link si prende come riferimento il primo passeggero del primo segmento
		if (f.getSegments().get(0).getPassengers() != null){
			if (f.getSegments().get(0).getPassengers().get(0).getWebCheckInDeepLink() != null){
				fr.setWebCheckInDeepLink(f.getSegments().get(0).getPassengers().get(0).getWebCheckInDeepLink());
			}else{
				fr.setWebCheckInDeepLink("");
			}
		}else{
			fr.setWebCheckInDeepLink("");
		}

		//Check-In **** checkinOnExternalSite si prende come riferimento il primo passeggero del primo segmento
		if (f.getSegments().get(0).getPassengers() != null){
			if (f.getSegments().get(0).getPassengers().get(0).getCheckinOnExternalSite() != null){
				fr.setCheckinOnExternalSite(f.getSegments().get(0).getPassengers().get(0).getCheckinOnExternalSite());
			}else{
				fr.setCheckinOnExternalSite("");
			}
		}else{
			fr.setCheckinOnExternalSite("");
		}
		
		if(f.getSegments().get(0).getOpenCI()){ //il primo segmento detiene le informazioni dei passeggeri per tutto l'itinerario/flight/volo
			if(f.getSegments().get(0).getPassengers() != null){//i passeggeri possono essere null nel caso in cui la tratta sia operata da un altro operatore
				total = f.getSegments().get(0).getPassengers().size();
				fr.setTotalNumberPassengers(String.valueOf(total));
				for(int p = 0; p < f.getSegments().get(0).getPassengers().size(); p++){
					if(f.getSegments().get(0).getPassengers().get(p).getCheckInComplete()){
						executed++;
					}
				}
				notAllowed = total - executed;
				fr.setNumberCheckinNotAllowed(String.valueOf(notAllowed));
				fr.setNumberCheckinExecuted(String.valueOf(executed));				
			}else{
				fr.setTotalNumberPassengers("0");
				fr.setNumberCheckinNotAllowed("0");
				fr.setNumberCheckinExecuted("0");
			}
		}else{
			fr.setTotalNumberPassengers("0");
			fr.setNumberCheckinNotAllowed("0");
			fr.setNumberCheckinExecuted("0");
		}
		
		
		List<SegmentRender> listSegmentRender = new ArrayList<>();
		for(int i = 0; i < f.getSegments().size(); i++ ){
			SegmentRender sr = new SegmentRender();
			sr.setI18n(ctx.i18n);
			sr.setAirline(f.getSegments().get(i).getAirline());
			sr.setFlight(f.getSegments().get(i).getFlight());
			sr.setOrigin(f.getSegments().get(i).getOrigin());
			sr.setDestination(f.getSegments().get(i).getDestination());
			sr.setArrivalDate(f.getSegments().get(i).getArrivalDate());
			sr.setDepartureDate(f.getSegments().get(i).getDepartureDate());
			sr.setOriginalDepartureDate(f.getSegments().get(i).getDepartureDate());
			sr.setOpenCI(f.getSegments().get(i).getOpenCI());
			sr.setCheckInGate(f.getSegments().get(i).getCheckInGate());
			sr.setPassengers(f.getSegments().get(i).getPassengers());
			sr.setAirlineName(f.getSegments().get(i).getAirlineName());
			sr.setTerminal(f.getSegments().get(i).getTerminal());
			sr.setDuration(f.getSegments().get(i).getDuration());
			sr.setWaiting(f.getSegments().get(i).getWaiting());
			sr.setAircraft(f.getSegments().get(i).getAircraft());
			sr.setAirlineCode(f.getSegments().get(i).getAirlineCode());
			sr.setFlightDelay(f.getSegments().get(i).getFlightDelay());
			sr.setFlightStatus(f.getSegments().get(i).getFlightStatus());
			listSegmentRender.add(sr);
		}
		
		fr.setSegments(listSegmentRender);
		
		return fr;
	}

}
