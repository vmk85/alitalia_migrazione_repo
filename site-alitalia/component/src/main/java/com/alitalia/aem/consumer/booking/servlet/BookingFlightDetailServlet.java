package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.common.data.home.FlightDetailsModelData;
import com.alitalia.aem.common.data.home.FlySegmentData;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsResponse;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.flightstatus.delegate.FlightStatusDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({ 
	@Property(name = "sling.servlet.selectors", value = { "flightdetailconsumer" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class BookingFlightDetailServlet extends GenericFormValidatorServlet {

	@Reference
	private FlightStatusDelegate flightStatusDelegate;
	
	private static final String STANDARD_REQUEST_DATE_FORMAT = "dd/MM/yyyy";

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		Validator validator = new Validator();
		String flightNumber = request.getParameter("flightnum");
		String flightVector = request.getParameter("flightvector");
		String depDate = request.getParameter("flightdate");
		/*TODO controlli di validazione*/
		
		return validator.validate();
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, 
			SlingHttpServletResponse response) throws Exception {

		response.setContentType("application/json");
		TidyJSONWriter out = new TidyJSONWriter(response.getWriter());

		out.object();
		String flightNumber = request.getParameter("flightnum");
		String flightVector = request.getParameter("flightvector");
		String dateFormat = AlitaliaUtils.getDateFormatByMarket(request.getResource());
		if (dateFormat==null){
			dateFormat=STANDARD_REQUEST_DATE_FORMAT;
		}
		SimpleDateFormat df = new SimpleDateFormat(dateFormat);
		df.setLenient(false);
		Date departureDate = df.parse(request.getParameter("flightdate"));
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(departureDate);

		RetrieveFlightDetailsResponse serviceResponseDetail = null;
		RetrieveFlightDetailsRequest serviceRequestDetail = new RetrieveFlightDetailsRequest();
		FlightDetailsModelData flightDetailsModelData = new FlightDetailsModelData();
		flightDetailsModelData.setFlightDate(calendar);
		flightDetailsModelData.setFlightNumber(flightNumber);
		flightDetailsModelData.setVector(flightVector);
		serviceRequestDetail.setFlightDetailsModelData(flightDetailsModelData);
		serviceResponseDetail = flightStatusDelegate.retrieveFlightDetails(serviceRequestDetail);

		if (serviceResponseDetail != null && serviceResponseDetail.getFlightDetailsModelData() != null) {
			String aircraft = "";
			String miles = "";
			String duration = "";
			String plusDays = "";
			if (serviceResponseDetail.getFlightDetailsModelData().getAirCraft() != null) {
				aircraft = serviceResponseDetail.getFlightDetailsModelData().getAirCraft();
				out.key("aircraft").value(aircraft);
			}

			List<FlySegmentData> flySegments = serviceResponseDetail.getFlightDetailsModelData()
					.getFlySegments();
			if (flySegments != null && !flySegments.isEmpty()) {

				if (flySegments.get(0).getMilesFromStart() != null) {
					miles = flySegments.get(0).getMilesFromStart().toString();
					out.key("miles").value(miles);
				}
				if (flySegments.get(0).getTimeFromStart() != null) {
					duration = flySegments.get(0).getTimeFromStart().toString();
					duration = (duration.length() > 2 ? duration.substring(0, duration.length()-2) : "0") + "h" + duration.substring(duration.length()-2, 
							duration.length()) + "&apos;";
					out.key("duration").value(duration);
				}
				if (flySegments.get(0).getDaysFromStart() != null
						&& !("").equals(flySegments.get(0).getDaysFromStart())) {
					plusDays = flySegments.get(0).getDaysFromStart().toString();
					out.key("plusDays").value(plusDays);
				}
			}

		} else {
			throw new IllegalStateException();
		}
		out.endObject();
	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception exception) {
		// TODO Auto-generated method stub

	}

}
