//package com.alitalia.aem.consumer.checkinrest.servlet;
//
//import java.io.IOException;
//
//import javax.servlet.Servlet;
//import javax.servlet.ServletException;
//
//import org.apache.felix.scr.annotations.Component;
//import org.apache.felix.scr.annotations.Properties;
//import org.apache.felix.scr.annotations.Property;
//import org.apache.felix.scr.annotations.Reference;
//import org.apache.felix.scr.annotations.Service;
//import org.apache.sling.api.SlingHttpServletRequest;
//import org.apache.sling.api.SlingHttpServletResponse;
//import org.apache.sling.api.servlets.SlingAllMethodsServlet;
//import org.apache.sling.commons.json.JSONException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
//import com.alitalia.aem.consumer.checkin.CheckinSession;
//import com.alitalia.aem.consumer.utils.AlitaliaUtils;
//import com.day.cq.commons.TidyJSONWriter;
//
//@Component(metatype = true)
//@Service(Servlet.class)
//@Properties({
//	@Property(name = "sling.servlet.selectors", value = { "checkinreturnredirect3ds"}),
//	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
//	@Property(name = "sling.servlet.methods", value = { "POST" }) })
//@SuppressWarnings("serial")
//public class CheckinReturnRedirect3DsServlet extends SlingAllMethodsServlet {
//	
//	protected Logger logger = LoggerFactory.getLogger(getClass());
//	
//	@Reference
//	private AlitaliaConfigurationHolder configuration;
//	
//	@Reference
//	private CheckinSession checkinSession;
//
//	@Override
//	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
//			throws ServletException, IOException {
//		
//		TidyJSONWriter json;
//		try {
//			json = new TidyJSONWriter(response.getWriter());
//			response.setContentType("application/json");
//
//			else {  //TODO possono esserci altri casi oltre REDIRECT o AUTHORIZED?
//				String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
//						request.getResource(), false) + getConfiguration().getCheckinThankYouPage();
//
//				successPage = request.getResourceResolver().map(successPage);
//
//				logger.info("[CheckinInitPaymentServlet] [managementSuccess 2D] successPage = " + successPage );
//
//				json.object();
//				json.key("isError").value(false);
//				json.key("successPage").value(successPage);
//				json.endObject();
//			}
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}	
//
//		
//	}
//
//
//}
