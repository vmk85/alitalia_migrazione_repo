package com.alitalia.aem.consumer.model.content.carosello;

import java.util.ArrayList;
import java.util.List;

import com.day.cq.wcm.api.Page;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
 
import javax.jcr.Node; 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables={Resource.class})
public class CaroselloModel {
	
	@Inject @Default(intValues={})
    private String[] banners;

    @Self
    private Resource resource;

    private Logger logger = LoggerFactory.getLogger(CaroselloModel.class);
    private String[] bannerPath;
    private String[] resourceType;
    private Integer[] numBanners;

    /**
     * E' l'equivalente di un costruttore di classe. In questo caso viene utilizzato per inizializzare
     * i valori di path e resourceType a cui deve puntare il componente.
     */
    @PostConstruct
    protected void initModel() {
    	
    	List<String> bannerPathList = new ArrayList<String>();
    	List<String> resourceTypeList = new ArrayList<String>();
    	
    	for (String banner : banners) {    		
    		if(banner != null && banner.length() > 0){
        		//logger.info("Initializing model << BlockElement >>");
            	try {
                    final ResourceResolver resolver = resource.getResourceResolver();
                    final Page targetPage = resolver.getResource(banner).adaptTo(Page.class);
                    final Node contentNode = targetPage.getContentResource("banner").adaptTo(Node.class);
                    if(contentNode != null){                    	
                    	bannerPathList.add(contentNode.getPath());
                    	resourceTypeList.add(contentNode.getProperty("sling:resourceType").getString());
                    } else{
                    	bannerPathList.add("banner non trovato");
                    	bannerPathList.add("res non trovato");
                    }
                } catch(Exception e) {
                    logger.error("Unable to get the values: \n" + e);
                    bannerPathList.add("banner errore");
                	bannerPathList.add("res errore");
                }
        	} else{
        		logger.info("Risorsa vuota");
        	}    		
    	}
    	
    	bannerPath = (String[]) bannerPathList.toArray(new String[bannerPathList.size()]);
    	resourceType = (String[]) resourceTypeList.toArray(new String[resourceTypeList.size()]);
    	
    	numBanners = new Integer[bannerPathList.size()];
    	for (int i = 0; i < numBanners.length; i++) {
    		numBanners[i] = i;
    	}
    }

    /**
     * Restituisce il path del componente
     */
    public String[] getBannerPath() {
        //logger.info("Returning the banenr path");
        return bannerPath;
    }

    /**
     * Restituisce il resource type del componente
     */
    public String[] getResourceType() {
        //logger.info("Returning the resource type");
        return resourceType;
    }
    
    /**
     * Restituisce un array di lunghezza pari al numero di banner inseriti
     */
    public Integer[] getNumBanners() {
    	return numBanners;
    }

    public String getResourcePath(){
        return resource.getPath();
    }
}

