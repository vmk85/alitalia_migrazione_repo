/** Modello per costanti/metodi Gigya [D.V.] */

package com.alitalia.aem.consumer.gigya;

public class GigyaConstans {

    public static final String WS_APIMETHOD_DELETEACCOUNT = "accounts.deleteAccount";

    public static final String WS_APIMETHOD_LOGIN = "accounts.login";

    public static final String WS_APIMETHOD_INIT_REGISTRATION = "accounts.initRegistration";

    public static final String WS_APIMETHOD_REGISTRER = "accounts.register";

    public static final String WS_APIMETHOD_FINALIZE_REGISTRATION = "accounts.finalizeRegistration";

    public static final String WS_APIMETHOD_IS_AVAILABLE_LOGIN_ID = "accounts.isAvailableLoginID";

    public static final String WS_APIMETHOD_SET_ACCOUNT = "accounts.setAccountInfo";

    public static final String WS_APIMETHOD_GET_ACCOUNT = "accounts.getAccountInfo";

    public static final String WS_APIMETHOD_LOGOUT = "accounts.logout";

    public static final String WS_APIMETHOD_REMOVE_CONNECTION= "socialize.removeConnection";

    public static final String WS_APIMETHOD_RESET_PASSWORD= "accounts.resetPassword";

    public static final String WS_APIMETHOD_SEARCH= "accounts.search";

}
