package com.alitalia.aem.consumer.booking.model;

import java.util.ArrayList;

public enum ContactType {
	MOBILE("Mobile"),
	HOME("Home"),
	OFFICE("Office"),
	HOTEL("Hotel");
	
	private final String value;

	ContactType(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	public String getI18nValue() {
		return "booking.contactType." + value + ".label";
	}
	
	public static ArrayList<String> listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (ContactType c: ContactType.values()) {
			result.add(c.value);
		}
		return result;
	}

	public static ContactType fromValue(String v) {
		for (ContactType c: ContactType.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
