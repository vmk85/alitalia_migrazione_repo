package com.alitalia.aem.consumer.millemiglia.model.header;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.enumerations.MMTierCodeEnum;
import com.alitalia.aem.consumer.global.i18n.I18nKeyMillemiglia;
import com.alitalia.aem.consumer.millemiglia.render.ProfiloUtenteRender;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables={SlingHttpServletRequest.class})
public class HeaderMillemigliaModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest request;
	
	private MMTierCodeEnum tierCode;
	private Long milleMigliaTotalMiles;
	private Long milleMigliaTotalQualifiedMiles;
	private Long milleMigliaRemainingMiles;
	private Long milleMigliaTotalSpentMiles;
	private Long milleMigliaQualifiedFlight;
	private String giornoMese;
	private String anno;
	private Long milesToNextTier;
	private String nextTierName;
	private Boolean isFrecciaAlataPlus;
	
	@PostConstruct
	protected void initModel(){
		try {
			super.initBaseModel(request);
			logger.debug("Init model HeaderMillemigliaModel");
			
			final Long checkpointClubUlisse = 20000L;
			final Long checkpointClubFrecciaAlata = 50000L;
			final Long checkpointClubFrecciaAlataPlus = 80000L;
			
			ProfiloUtenteRender profiloUtente = new ProfiloUtenteRender(AlitaliaUtils.getAuthenticatedUser(request));
			if (profiloUtente != null && profiloUtente.getCustomerData() != null) {
				logger.info("[HeaderMillemigliaModel] Get profiloUtente for AEM Autheticated user: {}", profiloUtente.getCustomerData().getCustomerNumber());
			} else {
				logger.info("[HeaderMillemigliaModel] profiloUtente is null");
			}
			
			tierCode = profiloUtente.getCustomerData().getTierCode();
			milleMigliaTotalMiles = profiloUtente.getCustomerData().getPointsEarnedTotal();
			milleMigliaTotalQualifiedMiles = profiloUtente.getCustomerData().getPointsEarnedTotalQualified();
			milleMigliaRemainingMiles = profiloUtente.getCustomerData().getPointsRemainingTotal();
			milleMigliaTotalSpentMiles = profiloUtente.getCustomerData().getPointsSpentTotal();
			milleMigliaQualifiedFlight = profiloUtente.getCustomerData().getQualifyingSegment();
			giornoMese = profiloUtente.getExpirationDate().substring(0, profiloUtente.getExpirationDate().length()-5);
			anno = profiloUtente.getExpirationDate().substring(
					profiloUtente.getExpirationDate().length()-4, profiloUtente.getExpirationDate().length());
			MMTierCodeEnum nextTier = AlitaliaUtils.getNextTier(tierCode);
			if(nextTier == null){
				//Caso utenza con MMTier PLUS
				isFrecciaAlataPlus = true;
			} else if(MMTierCodeEnum.PLUS.value().equals(nextTier.value())){
				milesToNextTier = ((checkpointClubFrecciaAlataPlus > milleMigliaTotalQualifiedMiles) ? (checkpointClubFrecciaAlataPlus - milleMigliaTotalQualifiedMiles) : 0);
				nextTierName = i18n.get(I18nKeyMillemiglia.CLUB_FRECCIA_ALATA_PLUS);
			} else if(MMTierCodeEnum.FRECCIA_ALATA.value().equals(nextTier.value())){
				milesToNextTier = ((checkpointClubFrecciaAlata > milleMigliaTotalQualifiedMiles) ? (checkpointClubFrecciaAlata - milleMigliaTotalQualifiedMiles) : 0);
				nextTierName = i18n.get(I18nKeyMillemiglia.CLUB_FRECCIA_ALATA);
			} else if(MMTierCodeEnum.ULISSE.value().equals(nextTier.value())){
				milesToNextTier = ((checkpointClubUlisse > milleMigliaTotalQualifiedMiles) ? (checkpointClubUlisse - milleMigliaTotalQualifiedMiles) : 0);
				nextTierName = i18n.get(I18nKeyMillemiglia.CLUB_ULISSE);
			}
		} catch(Exception e) {
			logger.error(e.toString());
		}
	}

	public MMTierCodeEnum getTierCode() {
		return tierCode;
	}

	public Long getMilleMigliaTotalMiles() {
		return milleMigliaTotalMiles;
	}

	public Long getMilleMigliaTotalQualifiedMiles() {
		return milleMigliaTotalQualifiedMiles;
	}
	
	public Long getMilleMigliaRemainingMiles() {
		return milleMigliaRemainingMiles;
	}

	public Long getMilleMigliaTotalSpentMiles() {
		return milleMigliaTotalSpentMiles;
	}

	public Long getMilleMigliaQualifiedFlight() {
		return milleMigliaQualifiedFlight;
	}

	public String getGiornoMese() {
		return giornoMese;
	}
	
	public String getAnno() {
		return anno;
	}

	public Long getMilesToNextTier() {
		return milesToNextTier;
	}
	
	public String getMilesToNextTierWithSeparator() {
		return AlitaliaUtils.setThousandsSeparatorLocalized(
				milesToNextTier, AlitaliaUtils.getPageLocale(
						request.getResource()));
	}

	public String getNextTierName() {
		return nextTierName;
	}
	
	public Boolean getIsFrecciaAlataPlus() {
		return isFrecciaAlataPlus;
	}
}
