/**
 * Componente di sessione MyAlitalia
 * ( Viene utilizzato per inizializzare la sessione MyAlitalia ) [D.V.]
 */

package com.alitalia.aem.consumer.myalitalia;

import com.alitalia.aem.common.data.crmdatarest.model.InfoCliente;
import com.alitalia.aem.common.data.crmdatarest.model.PreferenzePersonali;
import com.alitalia.aem.common.data.crmdatarest.model.PreferenzeViaggio;
import com.alitalia.aem.common.data.crmdatarest.model.RecapitiTelefonici;
import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.data.home.MyFlightsDataModel;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;
import com.alitalia.aem.common.messages.crmdatarest.GetCrmDataInfoRequest;
import com.alitalia.aem.common.messages.crmdatarest.GetCrmDataInfoResponse;
import com.alitalia.aem.common.messages.crmdatarest.SetCrmDataInfoRequest;
import com.alitalia.aem.common.messages.crmdatarest.SetCrmDataInfoResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.gigya.GigyaSession;
import com.alitalia.aem.consumer.gigya.GigyaUtils;
import com.alitalia.aem.consumer.myalitalia.logger.MyAlitaliaSessionLog;
import com.alitalia.aem.consumer.myalitalia.model.MyFlightsModel;
import com.alitalia.aem.consumer.myalitalia.model.MyFlightsPnr;
import com.alitalia.aem.consumer.myalitalia.model.MyFlightsTicket;
import com.alitalia.aem.consumer.myalitalia.model.ProfileComplite;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.crmdata.delegaterest.ICRMDataDelegate;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.alitalia.aem.web.component.webcheckin.delegaterest.IMyFlightsDelegate;
import com.alitalia.aem.ws.otp.service.OTPServiceClient;
import com.alitalia.aem.ws.otp.service.xsd1.GetOTPRequest;
import com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppRequest;
import com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.i18n.I18n;
import com.gigya.socialize.GSResponse;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.engine.SlingRequestProcessor;
import org.json.JSONObject;
import org.osgi.service.component.ComponentContext;

import java.util.ArrayList;
import java.util.List;

@Component(immediate = true)
@Service(value = MyAlitaliaSession.class)
public class MyAlitaliaSession {

    private MyAlitaliaSessionLog myAlitaliaSessionLog = new MyAlitaliaSessionLog();

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private RequestResponseFactory requestResponseFactory;

    @Reference
    private SlingRequestProcessor requestProcessor;

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile ICRMDataDelegate crmdataDelegate;

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile IMyFlightsDelegate myFlightsDelegateRest;

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile ICheckinDelegate checkInDelegateRest;

    private ComponentContext componentContext;

    @Reference
    private volatile GigyaSession gigyaSession;

    @Reference
    private OTPServiceClient otpServiceClient;

    public MyAlitaliaSession() {
        super();
    }

    /** Funzione per invio otp
     * @param prefisso
     * @param numero*/
    public SendOTPWithAppResponse SendVerificationOtp(String prefisso, String numero, String testo){

        prefisso = MyAlitaliaUtils.fixPrefix(prefisso);

        SendOTPWithAppRequest sendOTPRequest = new SendOTPWithAppRequest();
        sendOTPRequest.setMobileNumber(prefisso + numero);
        sendOTPRequest.setTextMessage(testo);
        sendOTPRequest.setApplicationName("Alitalia");
        SendOTPWithAppResponse sendOTPWithAppResponse = otpServiceClient.sendOTPWithApp(sendOTPRequest);


        return sendOTPWithAppResponse;

    }

    /**
     * Funzione per inizializzare la sessione ( Ritorna il contesto Myalitalia da salvare in sessione )
     */
    public MyAlitaliaSessionContext initializeSession(SlingHttpServletRequest request, MACustomer maCustomer) {

        myAlitaliaSessionLog.debug("initializeSession MyAlitalia");

        /** Inizializzo il contesto di sessione */
        MyAlitaliaSessionContext myAlitaliaSessionContext = setMyAlitaliaSessionContext(request, maCustomer, new MyAlitaliaSessionContext());

        myAlitaliaSessionLog.debug("Contesto inizzializato : " + myAlitaliaSessionContext.toString());

        return myAlitaliaSessionContext;
    }

    /**
     * Funzione per inizializzare il contesto MyAlitalia
     */
    private MyAlitaliaSessionContext setMyAlitaliaSessionContext(SlingHttpServletRequest request, MACustomer maCustomer, MyAlitaliaSessionContext mactx) {

        /** Effettuo la chiamata al CRM e prendo tutti i dati dell'utente */
        GetCrmDataInfoResponse getCrmDataInfoResponse = getCrmDataInfoResponse(MyAlitaliaUtils.prepareGetCrmDataInfoRequest(request, maCustomer));
        I18n i18n = new I18n(request.getResourceBundle(AlitaliaUtils.findResourceLocale(request.getResource())));

        /** Valorizzo la percentuale di completamento dei dati presenti per l'utente loggato */
        maCustomer.getProfile().setProfileComplete(AlitaliaUtils.getProfileComplete(maCustomer, i18n));
        mactx.setMaCustomer(maCustomer);
        mactx.setSid(IDFactory.getSid(request));
        mactx.setTid(IDFactory.getTid());
        mactx.setGetCrmDataInfoResponse(getCrmDataInfoResponse);

        /** Imposto la percentuale di completamento profilo */
        if (getCrmDataInfoResponse != null && !getCrmDataInfoResponse.getResult().getStatus().equals(MyAlitaliaConstants.MYALITALIA_SERVICE_ERROR)) {
            ProfileComplite profileComplite = new ProfileComplite();
            profileComplite.setPercent(MyAlitaliaUtils.getPercentProfileComplete(getCrmDataInfoResponse));
            profileComplite.setRedirectUrl(MyAlitaliaUtils.getUrlByPercent(profileComplite.getPercent(), getCrmDataInfoResponse));
            profileComplite.setTesto(MyAlitaliaUtils.getTextUrlByPercent(profileComplite.getPercent(), getCrmDataInfoResponse));
            mactx.setProfileComplite(new ProfileComplite());
            mactx.setProfileComplite(profileComplite);
        }

        /** Imposto i voli in sessione per l'utente loggato */
        mactx = setFlightsToMactx(request, maCustomer, mactx);

        return mactx;
    }


    public MyAlitaliaSessionContext setFlightsToMactx(SlingHttpServletRequest request, MACustomer maCustomer, MyAlitaliaSessionContext mactx) {
        if (maCustomer.getData().getFlights() != null) {
            List<MyFlightsPnr> myFlightsPnrs = new ArrayList<>();
            List<MyFlightsTicket> myFlightsTickets = new ArrayList<>();
            /** Inizzializzo la response */
            CheckinPnrSearchResponse checkinPnrSearchResponse = null;
            MyFlightsTicket myFlightsTicket = new MyFlightsTicket();
            List<MyFlightsDataModel> myFlightsDataModelList = new ArrayList<>();
            CheckinTicketSearchResponse checkinTicketSearchResponse = null;
            /** Effettuo la ricerca del PNR per ogni occorrenza */
            for (MyFlightsDataModel myFlightsDataModel : maCustomer.getData().getFlights()) {
                if (myFlightsDataModel.getFlight_pnr() != null && myFlightsDataModel.getFlight_firstName() != null && myFlightsDataModel.getFlight_lastName() != null) {
                    if (myFlightsDataModel.getFlight_pnr().length() == 6) {
                        /** Inizializzo la request */
                        CheckinPnrSearchRequest serviceRequest = new CheckinPnrSearchRequest(IDFactory.getTid(), IDFactory.getSid(request));
                        serviceRequest.setPnr(myFlightsDataModel.getFlight_pnr());
                        serviceRequest.setFirstName(myFlightsDataModel.getFlight_firstName());
                        serviceRequest.setLastName(myFlightsDataModel.getFlight_lastName());
                        serviceRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                        serviceRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                        serviceRequest.setConversationId(IDFactory.getTid());

                        boolean isEnable = false;

                        try {
                            isEnable = MyAlitaliaUtils.isChecked(AlitaliaUtils.getRepositoryPathMarket(request.getResource()), resolverFactory, "replicationDb");
                        } catch (Exception e) {

                        }
                        /** Richiamo il servizio */
                        if (isEnable) {
                            checkinPnrSearchResponse = myFlightsDelegateRest.retrieveCheckinPnr(serviceRequest);
                        } else {
                            checkinPnrSearchResponse = checkInDelegateRest.retrieveCheckinPnr(serviceRequest);
                        }
                        if (checkinPnrSearchResponse.getPnrData() != null && checkinPnrSearchResponse.getPnrData().getPnr() != null && checkinPnrSearchResponse.getPnrData().getPnr().size() > 0) {
                            if (checkinPnrSearchResponse.getPnrData().getError() == null) {
                                MyFlightsPnr myFlightsPnr = new MyFlightsPnr();
                                MyFlightsDataModel myFlightsDataModel2 = new MyFlightsDataModel();
                                myFlightsDataModel2 = myFlightsDataModel;
                                myFlightsDataModelList.add(myFlightsDataModel2);
                                myFlightsPnr.setCheckinPnrSearchResponse(checkinPnrSearchResponse);
                                myFlightsPnr.setNome(myFlightsDataModel.getFlight_firstName());
                                myFlightsPnr.setCognome(myFlightsDataModel.getFlight_lastName());
                                myFlightsPnrs.add(myFlightsPnr);
                            } else {
                                request.getSession().setAttribute("errorSearch", checkinPnrSearchResponse.getPnrData().getError());
                            }

                        }
                    } else if (myFlightsDataModel.getFlight_pnr().length() == 13) {
                        /** Inizializzo la request */
                        CheckinTicketSearchRequest serviceRequest = new CheckinTicketSearchRequest(IDFactory.getTid(), IDFactory.getSid(request));
                        serviceRequest.setTicket(myFlightsDataModel.getFlight_pnr());
                        serviceRequest.setLastName(myFlightsDataModel.getFlight_lastName());
                        serviceRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                        serviceRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                        serviceRequest.setConversationId(IDFactory.getTid());
                        /** Richiamo il servizio */
                        checkinTicketSearchResponse = checkInDelegateRest.retrieveCheckinTicket(serviceRequest);
                        if (checkinTicketSearchResponse.getPnrData() != null && checkinTicketSearchResponse.getPnrData().getError().equals("")) {
                            myFlightsTicket.setCheckinTicketSearchResponse(checkinTicketSearchResponse);
                            myFlightsTicket.setNome(myFlightsDataModel.getFlight_firstName());
                            myFlightsTicket.setCognome(myFlightsDataModel.getFlight_lastName());
                            myFlightsTickets.add(myFlightsTicket);
                        }
                    }

                }
            }
            /** Aggiungo i voli alla sessione */
            MyFlightsModel myFlightsModel = new MyFlightsModel();
            if (myFlightsPnrs != null && myFlightsPnrs.size() > 0) {
                myFlightsModel.setMyFlightsPnrs(myFlightsPnrs);
            }
            if (myFlightsTickets != null && myFlightsTickets.size() > 0) {
                myFlightsModel.setMyFlightsTickets(myFlightsTickets);
            }
            try {
                JSONObject obj2 = new JSONObject();
                obj2.put("flights", myFlightsDataModelList);
                GSResponse gsResponse = gigyaSession.setAccount(mactx.getMaCustomer().getUID(), GigyaUtils.mapJsonObjGigya(obj2.toString()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            mactx.setMyFlightsModel(myFlightsModel);

        }
        int pnrCountAvil = 0;
        int pnrCountManage = 0;
        int TktCountAvail = 0;
        int TktCountManage = 0;
        if (mactx.getMyFlightsModel() != null) {
            if (mactx.getMyFlightsModel().getMyFlightsPnrs() != null) {
                pnrCountAvil = GigyaUtils.totalFlightsAvailableForCheckinPnr(mactx.getMyFlightsModel().getMyFlightsPnrs());
                pnrCountManage = GigyaUtils.totalFlightsForManagePnr(mactx.getMyFlightsModel().getMyFlightsPnrs());
            }
            if (mactx.getMyFlightsModel().getMyFlightsTickets() != null) {
                TktCountAvail = GigyaUtils.totalFlightsAvailableForCheckin(mactx.getMyFlightsModel().getMyFlightsTickets());
                TktCountManage = GigyaUtils.totalFlightsForManage(mactx.getMyFlightsModel().getMyFlightsTickets());
            }

            mactx.getMyFlightsModel().setTotalFlightsAvailableForCheckin(TktCountAvail + pnrCountAvil);
            mactx.getMyFlightsModel().setTotalFlightsForManage(TktCountManage + pnrCountManage);
        }

        return mactx;
    }

    public MyAlitaliaSessionContext setSingleFlightToMactx(SlingHttpServletRequest request, MACustomer maCustomer, MyAlitaliaSessionContext mactx) {
        if (maCustomer.getData().getFlights() != null) {
            /** Inizzializzo la response */
            CheckinPnrSearchResponse checkinPnrSearchResponse = null;
            CheckinTicketSearchResponse checkinTicketSearchResponse = null;
            MyFlightsPnr myFlightsPnr = new MyFlightsPnr();
            MyFlightsTicket myFlightsTicket = new MyFlightsTicket();
            String pnr = request.getParameter("pnr");
            String nome = request.getParameter("firstName");
            String cognome = request.getParameter("lastName");
            /** Effettuo la ricerca del PNR per ogni occorrenza */
//            for (MyFlightsDataModel myFlightsDataModel : maCustomer.getData().getFlights()) {
            if (pnr != null && nome != null && cognome != null) {
                if (pnr.length() == 6) {
                    /** Inizializzo la request */
                    CheckinPnrSearchRequest serviceRequest = new CheckinPnrSearchRequest(IDFactory.getTid(), IDFactory.getSid(request));
                    serviceRequest.setPnr(pnr);
                    serviceRequest.setFirstName(nome);
                    serviceRequest.setLastName(cognome);
                    serviceRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                    serviceRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                    serviceRequest.setConversationId(IDFactory.getTid());

                    boolean isEnable = false;

                    try {
                        isEnable = MyAlitaliaUtils.isChecked(AlitaliaUtils.getRepositoryPathMarket(request.getResource()), resolverFactory, "replicationDb");
                    } catch (Exception e) {

                    }
                    /** Richiamo il servizio */
                    if (isEnable) {
                        checkinPnrSearchResponse = myFlightsDelegateRest.retrieveCheckinPnr(serviceRequest);
                    } else {
                        checkinPnrSearchResponse = checkInDelegateRest.retrieveCheckinPnr(serviceRequest);
                    }
                    if (checkinPnrSearchResponse.getPnrData() != null && checkinPnrSearchResponse.getPnrData().getPnr() != null && checkinPnrSearchResponse.getPnrData().getPnr().size() > 0) {
                        if (checkinPnrSearchResponse.getPnrData().getError() != null) {
                            request.getSession().setAttribute("errorSearch", checkinPnrSearchResponse.getPnrData().getError());
                        } else {
                            if (mactx.getMyFlightsModel() == null) {
                                mactx.setMyFlightsModel(new MyFlightsModel());
                            }
                            if (mactx.getMyFlightsModel().getMyFlightsPnrs() == null) {
                                mactx.getMyFlightsModel().setMyFlightsPnrs(new ArrayList<>());
                            }
                            myFlightsPnr.setCheckinPnrSearchResponse(checkinPnrSearchResponse);
                            myFlightsPnr.setNome(nome);
                            myFlightsPnr.setCognome(cognome);
                            mactx.getMyFlightsModel().getMyFlightsPnrs().add(myFlightsPnr);
                        }
                    }
                } else if (pnr.length() == 13) {
                    /** Inizializzo la request */
                    CheckinTicketSearchRequest serviceRequest2 = new CheckinTicketSearchRequest(IDFactory.getTid(), IDFactory.getSid(request));
                    serviceRequest2.setTicket(nome);
                    serviceRequest2.setLastName(cognome);
                    serviceRequest2.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                    serviceRequest2.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                    serviceRequest2.setConversationId(IDFactory.getTid());
                    /** Richiamo il servizio */
                    checkinTicketSearchResponse = checkInDelegateRest.retrieveCheckinTicket(serviceRequest2);
                    if (checkinTicketSearchResponse.getPnrData() != null) {
                        if (checkinPnrSearchResponse.getPnrData().getError() != null) {
                            request.getSession().setAttribute("errorSearch", checkinPnrSearchResponse.getPnrData().getError());
                        } else {
                            if (mactx.getMyFlightsModel() == null) {
                                mactx.setMyFlightsModel(new MyFlightsModel());
                            }
                            if (mactx.getMyFlightsModel().getMyFlightsTickets() == null) {
                                mactx.getMyFlightsModel().setMyFlightsTickets(new ArrayList<>());
                            }
                            myFlightsTicket.setCheckinTicketSearchResponse(checkinTicketSearchResponse);
                            myFlightsTicket.setNome(nome);
                            myFlightsTicket.setCognome(cognome);
                            mactx.getMyFlightsModel().getMyFlightsTickets().add(myFlightsTicket);
                        }
                    }
                }
                int pnrCountAvil = 0;
                int pnrCountManage = 0;
                int TktCountAvail = 0;
                int TktCountManage = 0;
                if (mactx.getMyFlightsModel() != null) {
                    if (mactx.getMyFlightsModel().getMyFlightsPnrs() != null) {
                        pnrCountAvil = GigyaUtils.totalFlightsAvailableForCheckinPnr(mactx.getMyFlightsModel().getMyFlightsPnrs());
                        pnrCountManage = GigyaUtils.totalFlightsForManagePnr(mactx.getMyFlightsModel().getMyFlightsPnrs());
                    }
                    if (mactx.getMyFlightsModel().getMyFlightsTickets() != null) {
                        TktCountAvail = GigyaUtils.totalFlightsAvailableForCheckin(mactx.getMyFlightsModel().getMyFlightsTickets());
                        TktCountManage = GigyaUtils.totalFlightsForManage(mactx.getMyFlightsModel().getMyFlightsTickets());
                    }

                    mactx.getMyFlightsModel().setTotalFlightsAvailableForCheckin(TktCountAvail + pnrCountAvil);
                    mactx.getMyFlightsModel().setTotalFlightsForManage(TktCountManage + pnrCountManage);
                }
//            }
            }
        }
        return mactx;

    }

    /**
     * Metodo per reperire i dati dell'utente MyAlitalia dal CRM
     */
    public GetCrmDataInfoResponse getCrmDataInfoResponse(GetCrmDataInfoRequest getCrmDataInfoRequest) {

        GetCrmDataInfoResponse getCrmDataInfoResponse = null;

        try {
            getCrmDataInfoResponse = crmdataDelegate.getAzCrmDataInfo(getCrmDataInfoRequest);
            myAlitaliaSessionLog.debug("Risposta getCrmDataInfoResponse (Dati utente " + getCrmDataInfoRequest.getIdMyAlitalia() + " MA dal CRM) = " + getCrmDataInfoResponse.toString());

        } catch (Exception e) {
            myAlitaliaSessionLog.error("Errore comunicazione CRM ( myAlitaliaSession.getCrmDataInfoResponse(getCrmDataInfoRequest)): " + e.toString());
        }

        return getCrmDataInfoResponse;
    }

    /**
     * Metodo per impostare i dati dell'utente MyAlitalia dal CRM
     */
    public SetCrmDataInfoResponse setCrmDataInfoResponse(SetCrmDataInfoRequest setCrmDataInfoRequest) {

        SetCrmDataInfoResponse setCrmDataInfoResponse = null;

        try {
            setCrmDataInfoResponse = crmdataDelegate.setAzCrmDataInfo(setCrmDataInfoRequest);
        } catch (Exception e) {
            myAlitaliaSessionLog.error("Errore comunicazione CRM ( myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest)): " + e.toString());
        }

        return setCrmDataInfoResponse;
    }

    /**
     * Funzione delete utente
     */
    public SetCrmDataInfoResponse deleteCrmAccount(String email, SlingHttpServletRequest request) {
        SetCrmDataInfoResponse setCrmDataInfoResponse = null;

        /** Preparo la request*/
        SetCrmDataInfoRequest setCrmDataInfoRequest = new SetCrmDataInfoRequest();
        setCrmDataInfoRequest.setCanale(MyAlitaliaConstants.MYALITALIA_SERVICE_UPDATE);
        setCrmDataInfoRequest.setInfoCliente(new InfoCliente());
        setCrmDataInfoRequest.setCanaleComunicazionePreferito(new ArrayList<>());
        setCrmDataInfoRequest.setListaConsensi(new ArrayList<>());
        setCrmDataInfoRequest.setRecapitiTelefonici(new RecapitiTelefonici());
        setCrmDataInfoRequest.setElencoDocumenti(new ArrayList<>());
        setCrmDataInfoRequest.setPreferenzeViaggio(new PreferenzeViaggio());
        setCrmDataInfoRequest.setPreferenzePersonali(new PreferenzePersonali());
        setCrmDataInfoRequest.setEmail(email);
        setCrmDataInfoRequest.setUnsubscribeNewsletter(true);
        setCrmDataInfoRequest.setIdMyAlitalia("0");
        setCrmDataInfoRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
        setCrmDataInfoRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
        setCrmDataInfoRequest.setTid(IDFactory.getTid());
        setCrmDataInfoRequest.setSid(IDFactory.getSid(request));
        setCrmDataInfoRequest.setConversationID(IDFactory.getTid());

        try {
            setCrmDataInfoResponse = crmdataDelegate.setAzCrmDataInfo(setCrmDataInfoRequest);
        } catch (Exception e) {
            myAlitaliaSessionLog.error("Errore comunicazione CRM ( myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest)): " + e.toString());
        }

        return setCrmDataInfoResponse;
    }


    @Activate
    private void activate(ComponentContext componentContext) {
        this.componentContext = componentContext;
    }

    @Modified
    private void modified(ComponentContext componentContext) {
        this.componentContext = componentContext;
    }

}
