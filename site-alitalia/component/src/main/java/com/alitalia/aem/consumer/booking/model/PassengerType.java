package com.alitalia.aem.consumer.booking.model;

public enum PassengerType {
	ADULT,
	CHILD,
	INFANT
}
