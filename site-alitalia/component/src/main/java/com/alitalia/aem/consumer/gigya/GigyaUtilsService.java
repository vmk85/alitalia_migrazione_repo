package com.alitalia.aem.consumer.gigya;

import com.gigya.socialize.GSObject;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class GigyaUtilsService {

    /** Metodo per inizializzare la request ( reset password )*/
    public static GSObject preparerESETpASSWORDGigyaObj(String email, String secret) throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("loginID",email);
        map.put("secretAnswer",secret);

        return new GSObject(mapObj(map));
    }

    /** Metodo per inizializzare la request ( reset password )*/
    public static GSObject preparerESETpASSWORDGigyaObj(String email, HashMap<String, String> map) throws Exception {

        map.put("loginID",email);

        return new GSObject(mapObj(map));
    }

    /** Metodo per inizializzare la request ( reset password )*/
    public static GSObject preparerESETpASSWORDGigyaObj(String psw, String pswRep, String resetToken) throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("newPassword",psw);
        map.put("secretAnswer","secretA");
        map.put("passwordResetToken",resetToken);

        return new GSObject(mapObj(map));
    }

    /** Metodo per inizializzare la request ( Controllo su gigya l'esistenza della mail )*/
    public static GSObject prepareRemoveConnectionGigyaObj(String uid, String provider) throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("UID",uid);
        map.put("provider",provider);

        return new GSObject(mapObj(map));
    }

    /** Metodo per inizializzare la request ( Controllo su gigya l'esistenza della mail )*/
    public static GSObject prepareLogoutGigyaObj(String uid) throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("UID",uid);

        return new GSObject(mapObj(map));
    }

    /** Metodo per inizializzare la request ( Controllo su gigya l'esistenza della mail )*/
    public static GSObject prepareAvailableIdGigyaObj(String email) throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("loginID",email);

        return new GSObject(mapObj(map));
    }

    /** Metodo per inizializzare la request ( Controllo su gigya l'esistenza della mail )*/
    public static GSObject prepareLoginGigyaObj(String email, String psw) throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("loginID",email);
        map.put("password",psw);
        map.put("include","profile,data,identities-all");

        return new GSObject(mapObj(map));
    }

    public static GSObject prepareQueryGigyaObj(String query) throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("query",query);

        return new GSObject(mapObj(map));
    }

    /** Metodo per inizializzare la request ( Finalizzazione della registrazione su gigya )*/
    public static GSObject prepareFinalizeRegistrationGigyaObj(String regToken) throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("regToken",regToken);
        map.put("include","profile,data,identities-all");

        return new GSObject(mapObj(map));
    }

    /** Metodo per inizializzare la request ( Set data )*/
    public static GSObject prepareSetAccountGigyaObj(String regToken, HashMap<String, String> map) throws Exception {

        map.put("UID",regToken);

        return new GSObject(mapObj(map));
    }

    /** Metodo per inizializzare la request ( Set data )*/
    public static GSObject prepareSetSocialAccountGigyaObj(String regToken, HashMap<String, String> map) throws Exception {

        map.put("regToken",regToken);

        return new GSObject(mapObj(map));
    }

    public static GSObject prepareGetAccountGigyaObj(String regToken) throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("UID",regToken);
        map.put("include","profile,data,identities-all");

        return new GSObject(mapObj(map));
    }

    /** Metodo per inizializzare la request ( Eliminazione account su gigya )*/
    public static GSObject prepareDeleteAccountGigyaObj(String uid) throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("UID",uid);

        return new GSObject(mapObj(map));
    }

    /** Metodo per inizializzare la request ( Regisrazione su gigya )*/
    public static GSObject prepareRegisterGigyaObj(String email, String psw, String regToken, String lang, boolean auth_com, boolean auth_profile) throws Exception {

        HashMap<String, String> map = new HashMap<>();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("auth_Profile",auth_profile);
        jsonObject.put("auth_Com",auth_com);
        map.put("email",email);
        map.put("password",psw);
        map.put("regToken",regToken);
        map.put("lang",lang);
        map.put("data",jsonObject.toString());

        return new GSObject(mapObj(map));
    }

    /** Metodo per mappare la request*/
    private static String mapObj (HashMap<String, String> map) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        for( Object key : map.keySet()){
            jsonObject.put(key.toString(),map.get(key));
        }
        return jsonObject.toString();

    }


}
