package com.alitalia.aem.consumer.checkin.servlet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinPassenger;
import com.alitalia.aem.consumer.checkin.CheckinPassengersForm;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checkinundo" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinUndoServlet extends GenericCheckinFormValidatorServlet {
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private CheckinSession checkinSession;
	
	public final static String CHECK_IN_FIELD = "checkin_";
	public final static String FREQUENT_FLYER_CODE_FIELD = "frequentFlyerCode_";
	public final static String FREQUENT_FLYER_TYPE_FIELD = "frequentFlyerType_";
	public final static String EXTRA_PASSENGER_NAME_FIELD = "addPassengerName_";
	public final static String EXTRA_PASSENGER_LAST_NAME_FIELD = "addPassengerLastName_";
	public final static String EXTRA_PASSENGER_TICKET_FIELD = "addPassengerTicket_";
	
	public final static String ERROR_NO_ADULTS_FIELD = "noAdult";
	public final static String FREQUENT_FLYER_CODE_NOT_VALID_MESSAGE = "checkin.passeggeri.codiceMillemigliaNonValido.label";
	public final static String TERMS_NOT_CHECKED = "checkin.passeggeri.necessariaAccettazioneCondizioni.label";
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		Validator validator = null;
		CheckinPassengersForm form = null;
		
		form = prepareForm(request, ctx.passengers.size());
		validator = validateFormBasic(form, ctx);
		
		/* Risultato della validazione */
		ResultValidation resultValidation = validator.validate();
		
		return resultValidation;
	}
	
	private CheckinPassengersForm prepareForm(SlingHttpServletRequest request, int size) {
		
		CheckinPassengersForm form = new CheckinPassengersForm();
		
		List<CheckinPassenger> passengers = new ArrayList<CheckinPassenger>();
		for (int i = 0; i < size; i++) {
			boolean checkin = Boolean.parseBoolean(request.getParameter(CHECK_IN_FIELD + i));
			if (checkin) {
				CheckinPassenger passenger = new CheckinPassenger();
				passenger.setIndex(i);
				passenger.setCheckin(checkin);
				passenger.setFrequentFlyerType(request.getParameter(FREQUENT_FLYER_TYPE_FIELD + i));
				passenger.setFrequentFlyerCode(request.getParameter(FREQUENT_FLYER_CODE_FIELD + i));
				passengers.add(passenger);
			}
		}
		form.setPassengers(passengers);
		
		return form;
	}
		
	private Validator validateFormBasic(CheckinPassengersForm form, CheckinSessionContext ctx) {
		
		Validator validator = new Validator();
		
		if (!form.hasPassengersToCheckIn()) {
			// Inserisco un errore sul primo passeggero
			validator.addDirectCondition(CHECK_IN_FIELD + "0", "", CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			return validator;
		}
		return validator;
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		CheckinPassengersForm form = prepareForm(request, ctx.passengers.size());
		
		ctx.selectedPassengers = new ArrayList<CheckinPassengerData>();
		
		for (int i = 0; i < form.getPassengers().size(); i++) {
			CheckinPassenger passenger = form.getPassengers().get(i);
			CheckinPassengerData passengerData = ctx.passengers.get(passenger.getIndex());
			ctx.selectedPassengers.add(passengerData);
		}
		
//		checkinSession.updatePassengersFirstStep(form, ctx);
		
		checkinSession.undoCheckin(ctx, false);
		
		String baseUrl = request.getResource().getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
		
		String nextPage = "/check-in.html";
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		json.key("result").value("OK");
		json.key("redirect").value(baseUrl + nextPage);
		json.endObject();
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}
