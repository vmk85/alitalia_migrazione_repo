package com.alitalia.aem.consumer.servlet.search;

import java.io.IOException;

import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.URLEncoder;
import java.io.UnsupportedEncodingException;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "searchsubmit" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class SearchServlet extends SlingAllMethodsServlet {
	
	private ComponentContext componentContext;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Reference
    private AlitaliaConfigurationHolder configuration;
	
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException {
		logger.debug("[SearchServlet] doGet");
		try {
			
			HttpSession session = request.getSession();
			session.setAttribute("searchedString", request.getParameter("mainMenu__search"));
			String parolaCercata = request.getParameter("mainMenu__search");
			Integer page = new Integer (1);
//			String pageString = "";
			if (request.getParameter("page")!= null && !"".equals(request.getParameter("page"))) {
//				pageString = request.getParameter("page");
				page = Integer.parseInt(request.getParameter("page"));
			}
			session.setAttribute("selectedPage", page); 
			logger.debug("[SearchServlet] parola cercata: "+parolaCercata);
			logger.debug("[SearchServlet] pagina cercata: "+page);

			String cluster = "";
			if (request.getParameter("cluster")!= null && !"".equals(request.getParameter("cluster"))) {
				cluster = request.getParameter("cluster");
			}
			session.setAttribute("selectedCluster", cluster); 
			logger.debug("[SearchServlet] cluster cercato: "+cluster);
			
			String xmlResponse = GSASearch (parolaCercata, page, cluster, request);
			
			session.setAttribute("xmlGSAResponse", xmlResponse);
			
			String successPage = (String) componentContext.getProperties().get("success.page");
			String succesUrl = AlitaliaUtils.findSiteBaseExternalUrl(
					request.getResource(), false) + successPage;
			if (successPage != null && !"".equals(successPage)) {
				response.sendRedirect(response.encodeRedirectURL(succesUrl));
			}
		}
		catch (Exception e) {
			logger.error("[SearchServlet] Unexpected error: ", e);
		}
	}
	
	private String GSASearch (String searchString, Integer page, String cluster, SlingHttpServletRequest request) {
	
	        //Create an HTTPClient object
			DefaultHttpClient httpClient = new DefaultHttpClient();
			Integer offset = Integer.parseInt(configuration.getGSAResultPerPage()); 
			Integer start = (page-1)*offset;
			
			String clusterQuery = "";
			String encodedSearchString = searchString;
			
			try {
				encodedSearchString = URLEncoder.encode(searchString, "UTF-8");
				if (!"".equals(cluster)) {
					clusterQuery = "&dnavs=inmeta:gsaentity_level1="+URLEncoder.encode(cluster, "UTF-8");
					encodedSearchString += "+inmeta:gsaentity_level1="+URLEncoder.encode(cluster, "UTF-8");
				}
				
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		        
			String baseUrl = configuration.getGSAHost() 
					+ "/search?q="+encodedSearchString
					+ "&access=p"
					+ "&client="+configuration.getGSAClient()+"_"+AlitaliaUtils.getRepositoryPathLanguage(request.getResource())+"_"+AlitaliaUtils.getRepositoryPathMarket(request.getResource())
					+ "&output=xml_no_dtd"
					+ "&oe=UTF-8"
					+ "&ie=UTF-8"
					+ "&site="+configuration.getGSASite()+"_"+AlitaliaUtils.getRepositoryPathLanguage(request.getResource())+"_"+AlitaliaUtils.getRepositoryPathMarket(request.getResource())
					+ "&start="+start
					+ "&num="+offset
					+ clusterQuery;
			
		HttpGet httpGet =  new HttpGet(baseUrl);

        logger.debug("[SearchServlet] httpGet: " + httpGet.toString());
                
	        HttpResponse httpResponse = null;
	        try {
	               httpResponse = httpClient.execute(httpGet);
	               logger.debug("[SearchServlet] httpResponse: " + httpResponse.toString());
	               
	        } catch (IOException e2) {
	               logger.error("[SearchServlet] errore execute httpGet ", e2);
	               e2.printStackTrace();
	        }
	        
	        InputStream streamIn = null;
	        try {
	               streamIn = httpResponse.getEntity().getContent();
	        } catch (IllegalStateException | IOException e1) {
	               e1.printStackTrace();
	               logger.error("[SearchServlet] errore getContent ", e1);
	        }
	        InputStreamReader streamInReader = new InputStreamReader(streamIn);
	        BufferedReader bufReader = new BufferedReader(streamInReader);
	        
	        String line;
	        StringBuilder response = new StringBuilder();
	        try {
	               while ((line = bufReader.readLine()) != null) {
	                      response.append(line);
	               }
	        } catch (IOException e) {
	               logger.error("[SearchServlet] errore leggo buffer ", e);
	               e.printStackTrace();
	               response = new StringBuilder();
	        }
	        
	        logger.debug("[SearchServlet] RISPOSTA: " + response.toString());
	        
	        return response.toString(); 
	}
}
