package com.alitalia.aem.consumer.myalitalia;

import com.alitalia.aem.common.data.crmdatarest.model.*;
import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.data.home.MyFlightsDataModel;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;
import com.alitalia.aem.common.messages.crmdatarest.GetCrmDataInfoResponse;
import com.alitalia.aem.common.messages.crmdatarest.SetCrmDataInfoRequest;
import com.alitalia.aem.common.messages.crmdatarest.SetCrmDataInfoResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.gigya.GigyaUtils;
import com.alitalia.aem.consumer.mapstruct.CrmMapper;
import com.alitalia.aem.consumer.myalitalia.logger.MyAlitaliaUtilsServiceLog;
import com.alitalia.aem.consumer.myalitalia.model.MyFlightsModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.alitalia.aem.web.component.webcheckin.delegaterest.IMyFlightsDelegate;
import com.google.gson.Gson;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;

import java.util.ArrayList;
import java.util.List;

import java.util.Locale;

public class MyAlitaliaUtilsService {


    /** Metodo per inviare al CRM i dati aggiornati dell'utente */
    static public SetCrmDataInfoResponse setMyAlitaliaCrmDataConsensi(SlingHttpServletRequest request, SlingHttpServletResponse response,MyAlitaliaSession myAlitaliaSession) {

        MyAlitaliaUtilsServiceLog myAlitaliaUtilsServiceLog = new MyAlitaliaUtilsServiceLog();

        SetCrmDataInfoResponse setCrmDataInfoResponse = null;

        String crmData = request.getParameter("data");

        myAlitaliaUtilsServiceLog.debug("Prendo i dati da inserire nella reguest per il servizio : " + crmData);

        /** Prendo i dati del CRM dalla sessione */
        GetCrmDataInfoResponse getCrmDataInfoResponse = MyAlitaliaUtils.getMyAlitaliaCrmDataBySession(request);

        myAlitaliaUtilsServiceLog.debug("Prendo i dati del crm dalla sessione : " + getCrmDataInfoResponse);

        /** Controllo che la response non sia nulla */
        if (getCrmDataInfoResponse != null && crmData != null){

            Gson gson = new Gson();
            try{

                /** Converto i parametri inviati alla servlet nell'oggetto per fare la request*/
                GetCrmDataInfoResponse jsonData = gson.fromJson(crmData, GetCrmDataInfoResponse.class);

                myAlitaliaUtilsServiceLog.debug("Converto i parametri inviati alla servlet : " + jsonData);

                /** Inserisco i nuovi dati nella response del CRM presente in sessione */
                getCrmDataInfoResponse = setConsensi(jsonData,getCrmDataInfoResponse);

                myAlitaliaUtilsServiceLog.debug("Inserisco i nuovi dati nella response del CRM presente in sessione : " + getCrmDataInfoResponse);

                /** Preparo la request per i dati da inviare al CRM
                 * ( La mia request setCrmDataInfoRequest è uguale alla risposta getCrmDataInfoResponse da parte del CRM ) */
                SetCrmDataInfoRequest setCrmDataInfoRequest = CrmMapper.MAPPER.setRequestFromGetResponse(getCrmDataInfoResponse);

                setCrmDataInfoRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                setCrmDataInfoRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                setCrmDataInfoRequest.setTid(IDFactory.getTid());
                setCrmDataInfoRequest.setSid(IDFactory.getSid(request));
                setCrmDataInfoRequest.setCanale(MyAlitaliaConstants.MYALITALIA_SERVICE_UPDATE);

                myAlitaliaUtilsServiceLog.debug("Preparo la request per i dati da inviare al CRM ( uso mapStruct): " + setCrmDataInfoRequest);

                try{
                    /** Invio i dati al CRM */
                    setCrmDataInfoResponse = myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest);
                    GetCrmDataInfoResponse getCrmDataInfoResponse1 = MyAlitaliaUtilsService.getCrmDataInfoFromCrm(request,myAlitaliaSession);
                    /** Salvo i nuovi dati del crm in sessione */
                    MyAlitaliaUtils.setCrmResponseDataToSession(request,getCrmDataInfoResponse1);

                    myAlitaliaUtilsServiceLog.debug("Response  myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest) : " + setCrmDataInfoResponse);

                }catch(Exception e){
                    myAlitaliaUtilsServiceLog.error("Errore comunicazione CRM ( myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest)): " + e.getMessage());
                }

                /** Invio i dati al CRM */

            }catch (Exception e){
                myAlitaliaUtilsServiceLog.error("impossibile associare i dati al modello per effettuare la chiamata" + e.getMessage());
            }

        }
        return setCrmDataInfoResponse;
    }

    /** Metodo utilizzato per mappare i consensi  */
    static public GetCrmDataInfoResponse setConsensi(GetCrmDataInfoResponse jsonData, GetCrmDataInfoResponse data) {

        if(jsonData != null){

            if(jsonData.getListaConsensi() != null){

                data.setListaConsensi(jsonData.getListaConsensi());

            }

            if(jsonData.getPreferenzePersonali() != null){

                data.getPreferenzePersonali().setLinguaComunicazioniCommerciali(MyAlitaliaUtils.getCodeLanguagefromLinguaCorrispondenza(jsonData.getPreferenzePersonali().getLinguaComunicazioniCommerciali()));
                data.getPreferenzePersonali().setLinguaComunicazioniServizio(MyAlitaliaUtils.getCodeLanguagefromLinguaCorrispondenza(jsonData.getPreferenzePersonali().getLinguaComunicazioniServizio()));
                data.setCanaleComunicazionePreferito(jsonData.getCanaleComunicazionePreferito());

            }

        }

        return data;

    }

    /** Metodo per reperire i dati dal CRM
     * @param request
     * @param myAlitaliaSession*/
    static public GetCrmDataInfoResponse getCrmDataInfoFromCrm(SlingHttpServletRequest request, MyAlitaliaSession myAlitaliaSession){

        MyAlitaliaUtilsServiceLog myAlitaliaUtilsServiceLog = new MyAlitaliaUtilsServiceLog();

        GetCrmDataInfoResponse getCrmDataInfoResponse = null;
        getCrmDataInfoResponse = myAlitaliaSession.getCrmDataInfoResponse(MyAlitaliaUtils.prepareGetCrmDataInfoRequest(request,MyAlitaliaUtils.getMaCustomerBySession(request)));
        myAlitaliaUtilsServiceLog.debug("Prendo i dati dal CRM per l'utente in sessione : " + getCrmDataInfoResponse);

        return getCrmDataInfoResponse;

    }

    /** Metodo utilizzato per registrare un nuovo utente */
    public static SetCrmDataInfoResponse setMyAlitaliaCrmDataRegistration(SlingHttpServletRequest request, SlingHttpServletResponse response, MyAlitaliaSession myAlitaliaSession, MACustomer maCustomer) {

        MyAlitaliaUtilsServiceLog myAlitaliaUtilsServiceLog = new MyAlitaliaUtilsServiceLog();

        SetCrmDataInfoResponse setCrmDataInfoResponse = null;

        /** Controllo che la response non sia nulla */
        if (maCustomer != null){

            Gson gson = new Gson();
            try{

                /** Inserisco i nuovi dati nella response del CRM presente in sessione */
                SetCrmDataInfoRequest setCrmDataInfoRequest = prepareRegistrationProfile(request,myAlitaliaSession,maCustomer);

                myAlitaliaUtilsServiceLog.debug("Preparo la request per i dati da inviare al CRM ( uso mapStruct ): " + setCrmDataInfoRequest);

                try{
                    /** Invio i dati al CRM */
                    setCrmDataInfoResponse = myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest);

                    myAlitaliaUtilsServiceLog.debug("Response  myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest) : " + setCrmDataInfoResponse.toString());

                }catch(Exception e){
                    myAlitaliaUtilsServiceLog.error("Errore comunicazione CRM ( myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest)): " + e.getMessage());
                }

            }catch (Exception e){
                myAlitaliaUtilsServiceLog.error("impossibile associare i dati al modello per effettuare la chiamata" + e.getMessage());
            }

        }

        return setCrmDataInfoResponse;

    }

    /** Metodo utilizzato per preparare la request dei dati del nuovo utente */
    private static SetCrmDataInfoRequest prepareRegistrationProfile(SlingHttpServletRequest request, MyAlitaliaSession myAlitaliaSession, MACustomer maCustomer) {

        MyAlitaliaUtilsServiceLog myAlitaliaUtilsServiceLog = new MyAlitaliaUtilsServiceLog();

        SetCrmDataInfoRequest setCrmDataInfoRequest = null;
        SetCrmDataInfoRequest setCrmDataInfoRequestJson = null;

        if(maCustomer != null ){
            Gson gson = new Gson();

            try{

                /** Inserisco i flag di accettazione nella request per la creazione dell'utenza */
                setCrmDataInfoRequestJson = gson.fromJson(request.getParameter("crmData"),SetCrmDataInfoRequest.class);
                myAlitaliaUtilsServiceLog.debug("Flag Accettazione request : " + setCrmDataInfoRequestJson);

            }catch (Exception e){
                myAlitaliaUtilsServiceLog.error(e.getMessage());
            }

            Locale locale;
            locale = AlitaliaUtils.findResourceLocale(request.getResource());


            PreferenzePersonali preferenzePersonali = new PreferenzePersonali();
            preferenzePersonali.setLinguaComunicazioniCommerciali(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
            preferenzePersonali.setLinguaComunicazioniServizio(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));

            /** Mappo il customer nella request*/
            setCrmDataInfoRequest = CrmMapper.MAPPER.setRequestFromMaCustomer(maCustomer);
            setCrmDataInfoRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
            setCrmDataInfoRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
            setCrmDataInfoRequest.setTid(IDFactory.getTid());
            setCrmDataInfoRequest.setSid(IDFactory.getSid(request));
            setCrmDataInfoRequest.setInfoCliente(new InfoCliente());
            setCrmDataInfoRequest.getInfoCliente().setNome(maCustomer.getProfile().getFirstName()!= null ? maCustomer.getProfile().getFirstName() : "");
            setCrmDataInfoRequest.getInfoCliente().setCognome(maCustomer.getProfile().getFirstName()!= null ? maCustomer.getProfile().getLastName() : "");
            setCrmDataInfoRequest.setCanaleComunicazionePreferito(new ArrayList<>());
            setCrmDataInfoRequest.setRecapitiTelefonici(new RecapitiTelefonici());
            setCrmDataInfoRequest.setElencoDocumenti(new ArrayList<>());
            setCrmDataInfoRequest.setPreferenzeViaggio(new PreferenzeViaggio());
            setCrmDataInfoRequest.setPreferenzePersonali(preferenzePersonali);
            setCrmDataInfoRequest.setCanale("web");
            setCrmDataInfoRequest.setListaConsensi(setCrmDataInfoRequestJson != null ? setCrmDataInfoRequestJson.getListaConsensi() : new ArrayList<>());
            setCrmDataInfoRequest.setCanaleComunicazionePreferito(setCrmDataInfoRequestJson != null ? setCrmDataInfoRequestJson.getCanaleComunicazionePreferito() : new ArrayList<>());
            setCrmDataInfoRequest.setConversationID(IDFactory.getTid());
            setCrmDataInfoRequest.setCanale(MyAlitaliaConstants.MYALITALIA_SERVICE_UPDATE);


            myAlitaliaUtilsServiceLog.debug("Mappatura per registrazione utente nel CRM : " + setCrmDataInfoRequest);

        }

        return setCrmDataInfoRequest;
    }

    /** Metodo utilizzato per inviare i dati personali dell'utente */
    public static SetCrmDataInfoResponse setMyAlitaliaCrmPersonalData(SlingHttpServletRequest request, SlingHttpServletResponse response, MyAlitaliaSession myAlitaliaSession) {

        MyAlitaliaUtilsServiceLog myAlitaliaUtilsServiceLog = new MyAlitaliaUtilsServiceLog();

        SetCrmDataInfoResponse setCrmDataInfoResponse = null;

        String crmData = request.getParameter("data");

        myAlitaliaUtilsServiceLog.debug("Prendo i dati da inserire nella reguest per il servizio ( setMyAlitaliaCrmPersonalData ) : " + crmData);

        /** Prendo i dati del CRM dalla sessione */
        GetCrmDataInfoResponse getCrmDataInfoResponse = MyAlitaliaUtils.getMyAlitaliaCrmDataBySession(request);

        myAlitaliaUtilsServiceLog.debug("Prendo i dati del crm dalla sessione ( setMyAlitaliaCrmPersonalData ) : " + getCrmDataInfoResponse);

        /** Controllo che la response non sia nulla */
        if (getCrmDataInfoResponse != null && crmData != null){

            Gson gson = new Gson();
            try{

                /** Converto i parametri inviati alla servlet nell'oggetto per fare la request */
                GetCrmDataInfoResponse jsonDataCustomer = gson.fromJson(crmData, GetCrmDataInfoResponse.class);

                String dataNascita = null;

//                if(jsonDataCustomer.getInfoCliente().get != null && jsonDataCustomer.getProfile().getBirthMonth() != null && jsonDataCustomer.getProfile().getBirthDay() != null ){
//
//                    dataNascita = (
//                            jsonDataCustomer.getProfile().getBirthYear() + "-" +
//                                    ( Integer.parseInt(jsonDataCustomer.getProfile().getBirthMonth()) < 10 ?
//                                            "0" + jsonDataCustomer.getProfile().getBirthMonth()
//                                            :
//                                            jsonDataCustomer.getProfile().getBirthMonth())
//                                    + "-" +
//                                    ( Integer.parseInt(jsonDataCustomer.getProfile().getBirthDay()) < 10 ?
//                                            "0" + jsonDataCustomer.getProfile().getBirthDay()
//                                            :
//                                            jsonDataCustomer.getProfile().getBirthDay()));
//                }

                myAlitaliaUtilsServiceLog.debug("Converto i parametri inviati alla servlet ( setMyAlitaliaCrmPersonalData ) : " + jsonDataCustomer);

//                GetCrmDataInfoResponse jsonData = CrmMapper.MAPPER.getRequestFromMaCustomer(jsonDataCustomer);

                /** Inserisco i nuovi dati nella response del CRM presente in sessione */
                getCrmDataInfoResponse = setDatiPersonali(jsonDataCustomer,getCrmDataInfoResponse);

                myAlitaliaUtilsServiceLog.debug("Inserisco i nuovi dati nella response del CRM presente in sessione ( setMyAlitaliaCrmPersonalData ) : " + getCrmDataInfoResponse);

                /** Preparo la request per i dati da inviare al CRM
                 * ( La mia request setCrmDataInfoRequest è uguale alla risposta getCrmDataInfoResponse da parte del CRM ) */
                SetCrmDataInfoRequest setCrmDataInfoRequest = CrmMapper.MAPPER.setRequestFromGetResponse(getCrmDataInfoResponse);
                setCrmDataInfoRequest.getInfoCliente().setDataNascita(jsonDataCustomer.getInfoCliente().getDataNascita());
                setCrmDataInfoRequest.getInfoCliente().setTipoIndirizzo(jsonDataCustomer.getInfoCliente().getTipoIndirizzo());
                setCrmDataInfoRequest.getInfoCliente().setNomeAzienda(jsonDataCustomer.getInfoCliente().getNomeAzienda());
                setCrmDataInfoRequest.getInfoCliente().setIndirizzo(jsonDataCustomer.getInfoCliente().getIndirizzo());
                setCrmDataInfoRequest.getInfoCliente().setCap(jsonDataCustomer.getInfoCliente().getCap());
                setCrmDataInfoRequest.getInfoCliente().setCitta(jsonDataCustomer.getInfoCliente().getCitta());
                setCrmDataInfoRequest.getInfoCliente().setNazione(jsonDataCustomer.getInfoCliente().getNazione());
                setCrmDataInfoRequest.getInfoCliente().setStato(jsonDataCustomer.getInfoCliente().getStato());
                setCrmDataInfoRequest.setRecapitiTelefonici(getCrmDataInfoResponse.getRecapitiTelefonici());
                setCrmDataInfoRequest.setCanale(MyAlitaliaConstants.MYALITALIA_SERVICE_UPDATE);

                setCrmDataInfoRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                setCrmDataInfoRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                setCrmDataInfoRequest.setTid(IDFactory.getTid());
                setCrmDataInfoRequest.setSid(IDFactory.getSid(request));

                myAlitaliaUtilsServiceLog.debug("Preparo la request per i dati da inviare al CRM ( uso mapStruct) ( setMyAlitaliaCrmPersonalData ) : " + setCrmDataInfoRequest);

                try{
                    /** Invio i dati al CRM */
                    setCrmDataInfoResponse = myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest);
                    GetCrmDataInfoResponse getCrmDataInfoResponse1 = MyAlitaliaUtilsService.getCrmDataInfoFromCrm(request,myAlitaliaSession);
                    /** Salvo i nuovi dati del crm in sessione */
                    MyAlitaliaUtils.setCrmResponseDataToSession(request,getCrmDataInfoResponse1);

                    myAlitaliaUtilsServiceLog.debug("Response  myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest)  ( setMyAlitaliaCrmPersonalData ) : " + setCrmDataInfoResponse);

                }catch(Exception e){
                    myAlitaliaUtilsServiceLog.error("Errore comunicazione CRM ( myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest)) ( setMyAlitaliaCrmPersonalData ) : " + e.getMessage());
                }

            }catch (Exception e){
                myAlitaliaUtilsServiceLog.error("impossibile associare i dati al modello per effettuare la chiamata" + e.getMessage());
            }

        }
        return setCrmDataInfoResponse;

    }

    /** Metodo per inserire documenti di viaggio*/
    public static SetCrmDataInfoResponse setMyAlitaliaCrmDocument(SlingHttpServletRequest request, SlingHttpServletResponse response, MyAlitaliaSession myAlitaliaSession) {

        MyAlitaliaUtilsServiceLog myAlitaliaUtilsServiceLog = new MyAlitaliaUtilsServiceLog();

        SetCrmDataInfoResponse setCrmDataInfoResponse = null;

        String crmData = request.getParameter("data");

        myAlitaliaUtilsServiceLog.debug("Prendo i dati da inserire nella reguest per il servizio ( setMyAlitaliaCrmPersonalData ) : " + crmData);

        /** Prendo i dati del CRM dalla sessione */
        GetCrmDataInfoResponse getCrmDataInfoResponse = MyAlitaliaUtils.getMyAlitaliaCrmDataBySession(request);

        myAlitaliaUtilsServiceLog.debug("Prendo i dati del crm dalla sessione ( setMyAlitaliaCrmPersonalData ) : " + getCrmDataInfoResponse);
        if (getCrmDataInfoResponse != null && crmData != null) {

            Gson gson = new Gson();

            try {

                Documento documento = gson.fromJson(crmData, Documento.class);

                SetCrmDataInfoRequest setCrmDataInfoRequest = CrmMapper.MAPPER.setRequestFromGetResponse(getCrmDataInfoResponse);

                boolean sostituito = false;

                List<Documento> listaDocumenti = new ArrayList<>();

                if (setCrmDataInfoRequest.getElencoDocumenti() != null){
                    for (Documento documentoRequest : setCrmDataInfoRequest.getElencoDocumenti()) {

                        if (!documentoRequest.getTipo().equals(documento.getTipo())) {
                            listaDocumenti.add(documentoRequest);
                        }
                    }
                }

                listaDocumenti.add(documento);
                setCrmDataInfoRequest.setElencoDocumenti(listaDocumenti);
                setCrmDataInfoRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                setCrmDataInfoRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                setCrmDataInfoRequest.setTid(IDFactory.getTid());
                setCrmDataInfoRequest.setSid(IDFactory.getSid(request));
                setCrmDataInfoRequest.setCanale(MyAlitaliaConstants.MYALITALIA_SERVICE_UPDATE);


                try {
                    /** Invio i dati al CRM */
                    setCrmDataInfoResponse = myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest);
                    GetCrmDataInfoResponse getCrmDataInfoResponse1 = MyAlitaliaUtilsService.getCrmDataInfoFromCrm(request,myAlitaliaSession);
                    /** Salvo i nuovi dati del crm in sessione */
                    MyAlitaliaUtils.setCrmResponseDataToSession(request,getCrmDataInfoResponse1);

                    myAlitaliaUtilsServiceLog.debug("Response  myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest)  ( setMyAlitaliaCrmDocument ) : " + setCrmDataInfoResponse);

                } catch (Exception e) {
                    myAlitaliaUtilsServiceLog.error("Errore comunicazione CRM ( myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest)) ( setMyAlitaliaCrmDocument ) : " + e.getMessage());
                }

            } catch (Exception e) {
                myAlitaliaUtilsServiceLog.error("impossibile inserire il documento per effettuare la chiamata" + e.getMessage());

            }
        }
        return setCrmDataInfoResponse;
    }

    /** Metodo utilizzato per mappare i dati personali */
    private static GetCrmDataInfoResponse setDatiPersonali(GetCrmDataInfoResponse jsonData, GetCrmDataInfoResponse data) {

        if(jsonData != null){

            if(jsonData.getInfoCliente() != null){

                data.setInfoCliente(jsonData.getInfoCliente());

            }

            if(jsonData.getRecapitiTelefonici() != null){

                data.setRecapitiTelefonici(jsonData.getRecapitiTelefonici());

            }

        }

        return data;

    }

    public static SetCrmDataInfoResponse deleteMyAlitaliaCrmDocument(SlingHttpServletRequest request, SlingHttpServletResponse response, MyAlitaliaSession myAlitaliaSession) {

        MyAlitaliaUtilsServiceLog myAlitaliaUtilsServiceLog = new MyAlitaliaUtilsServiceLog();

        SetCrmDataInfoResponse setCrmDataInfoResponse = null;

        String crmData = request.getParameter("data");

        myAlitaliaUtilsServiceLog.debug("Prendo i dati da inserire nella reguest per il servizio ( setMyAlitaliaCrmPersonalData ) : " + crmData);

        /** Prendo i dati del CRM dalla sessione */
        GetCrmDataInfoResponse getCrmDataInfoResponse = MyAlitaliaUtils.getMyAlitaliaCrmDataBySession(request);

        myAlitaliaUtilsServiceLog.debug("Prendo i dati del crm dalla sessione ( setMyAlitaliaCrmPersonalData ) : " + getCrmDataInfoResponse);
        if (getCrmDataInfoResponse != null && crmData != null) {

            Gson gson = new Gson();

            try {

                Documento documento = gson.fromJson(crmData, Documento.class);

                List<Documento> documentiRimanenti = new ArrayList<>();

                SetCrmDataInfoRequest setCrmDataInfoRequest = CrmMapper.MAPPER.setRequestFromGetResponse(getCrmDataInfoResponse);

                if (setCrmDataInfoRequest.getElencoDocumenti() != null){
                    for (Documento documentoRequest : setCrmDataInfoRequest.getElencoDocumenti()) {

                        if (!documentoRequest.getTipo().equals(documento.getTipo())) {
                            documentiRimanenti.add(documentoRequest);
                        }
                    }
                }

                setCrmDataInfoRequest.setElencoDocumenti(documentiRimanenti);
                setCrmDataInfoRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                setCrmDataInfoRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                setCrmDataInfoRequest.setTid(IDFactory.getTid());
                setCrmDataInfoRequest.setSid(IDFactory.getSid(request));
                setCrmDataInfoRequest.setCanale(MyAlitaliaConstants.MYALITALIA_SERVICE_UPDATE);


                try {
                    /** Invio i dati al CRM */
                    setCrmDataInfoResponse = myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest);
                    GetCrmDataInfoResponse getCrmDataInfoResponse1 = MyAlitaliaUtilsService.getCrmDataInfoFromCrm(request,myAlitaliaSession);
                    /** Salvo i nuovi dati del crm in sessione */
                    MyAlitaliaUtils.setCrmResponseDataToSession(request,getCrmDataInfoResponse1);

                    myAlitaliaUtilsServiceLog.debug("Response myAlitaliaSession.deleteCrmDataInfoResponse(setCrmDataInfoRequest)  ( setMyAlitaliaCrmDocument ) : " + setCrmDataInfoResponse);

                } catch (Exception e) {
                    myAlitaliaUtilsServiceLog.error("Errore comunicazione CRM ( myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest)) ( setMyAlitaliaCrmDocument ) : " + e.getMessage());
                }

            } catch (Exception e) {
                myAlitaliaUtilsServiceLog.error("impossibile eliminare il documento per effettuare la chiamata" + e.getMessage());

            }
        }
        return setCrmDataInfoResponse;
    }

    public static SetCrmDataInfoResponse setMyAlitaliaCrmPreferenze(SlingHttpServletRequest request, SlingHttpServletResponse response, MyAlitaliaSession myAlitaliaSession) {

        MyAlitaliaUtilsServiceLog myAlitaliaUtilsServiceLog = new MyAlitaliaUtilsServiceLog();

        SetCrmDataInfoResponse setCrmDataInfoResponse = null;

        String crmData = request.getParameter("data");
        String dataType = request.getParameter("type");

        myAlitaliaUtilsServiceLog.debug("Prendo i dati da inserire nella reguest per il servizio ( setMyAlitaliaCrmPersonalData ) : " + crmData);

        /** Prendo i dati del CRM dalla sessione */
        GetCrmDataInfoResponse getCrmDataInfoResponse = MyAlitaliaUtils.getMyAlitaliaCrmDataBySession(request);

        myAlitaliaUtilsServiceLog.debug("Prendo i dati del crm dalla sessione ( setMyAlitaliaCrmPersonalData ) : " + getCrmDataInfoResponse);
        if (getCrmDataInfoResponse != null) {

            if (getCrmDataInfoResponse.getPreferenzeViaggio() == null){
                getCrmDataInfoResponse.setPreferenzeViaggio(new PreferenzeViaggio());
            }

            SetCrmDataInfoRequest setCrmDataInfoRequest = CrmMapper.MAPPER.setRequestFromGetResponse(getCrmDataInfoResponse);

            try {

                switch (dataType){

                    case "posto":
                        setCrmDataInfoRequest.getPreferenzeViaggio().setPosto(crmData);
                        break;

                    case "pasto":
                        setCrmDataInfoRequest.getPreferenzeViaggio().setPasto(crmData);
                        break;

                    case "preferenzeAeroporto":
                        setCrmDataInfoRequest.getPreferenzeViaggio().setAeroportoPreferito(crmData);
                        break;

                    case "viaggiInFamiglia":
                        setCrmDataInfoRequest.getPreferenzeViaggio().setViaggiInFamiglia(Boolean.parseBoolean(crmData));
                        break;

//                    case "preferenzeAeroporto":
//                        setCrmDataInfoRequest.getPreferenzeViaggio().setAeroportoPreferito(crmData);
//                        break;

                }

                setCrmDataInfoRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                setCrmDataInfoRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                setCrmDataInfoRequest.setTid(IDFactory.getTid());
                setCrmDataInfoRequest.setSid(IDFactory.getSid(request));
                setCrmDataInfoRequest.setCanale(MyAlitaliaConstants.MYALITALIA_SERVICE_UPDATE);


                try {
                    /** Invio i dati al CRM */
                    setCrmDataInfoResponse = myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest);
                    GetCrmDataInfoResponse getCrmDataInfoResponse1 = MyAlitaliaUtilsService.getCrmDataInfoFromCrm(request,myAlitaliaSession);
                    /** Salvo i nuovi dati del crm in sessione */
                    MyAlitaliaUtils.setCrmResponseDataToSession(request,getCrmDataInfoResponse1);
                    myAlitaliaUtilsServiceLog.debug("Response myAlitaliaSession.deleteCrmDataInfoResponse(setCrmDataInfoRequest)  ( setMyAlitaliaCrmDocument ) : " + setCrmDataInfoResponse);

                } catch (Exception e) {
                    myAlitaliaUtilsServiceLog.error("Errore comunicazione CRM ( myAlitaliaSession.setCrmDataInfoResponse(setCrmDataInfoRequest)) ( setMyAlitaliaCrmDocument ) : " + e.getMessage());
                }

            } catch (Exception e) {
                myAlitaliaUtilsServiceLog.error("impossibile eliminare il documento per effettuare la chiamata" + e.getMessage());

            }
        }
        return setCrmDataInfoResponse;
    }
}
