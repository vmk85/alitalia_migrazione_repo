package com.alitalia.aem.consumer.model.content.boxcity;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.utils.AlitaliaUtils;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables={Resource.class})
public class BoxCityModel {

    @Self
    private Resource resource;
    
    private Map<String, Object> fieldList;
    
    private Logger logger = LoggerFactory.getLogger(BoxCityModel.class);
   
    @PostConstruct
    protected void initModel() {
		try {
			fieldList = AlitaliaUtils.fromJcrMultifieldToMapArrayObject(resource, "items");
		} catch (Exception e) {
			
			if (fieldList == null) {
				fieldList = new LinkedHashMap<String, Object>();
				Map<String, String> innerFieldList = new LinkedHashMap<String, String>();
				innerFieldList.put("label", "Clicca qui per editare");
				innerFieldList.put("url", "#");
				fieldList.put("0", innerFieldList);
			}
			
			logger.error("Errore BoxCity MultifieldModel: ", e);
		}
    }
    
    public Map<String, Object> getFieldList() {
        return fieldList;
    }
}
