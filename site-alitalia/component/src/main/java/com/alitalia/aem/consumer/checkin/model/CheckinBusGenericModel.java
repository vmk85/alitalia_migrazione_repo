package com.alitalia.aem.consumer.checkin.model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinBusGenericModel extends GenericCheckinModel {
	
	@Inject
	AlitaliaConfigurationHolder configurationHolder;
	
	@Self
	private SlingHttpServletRequest request;
	
	private boolean bus;
	private boolean selectedRouteBus;
	private boolean selectedRouteFromBusStation;
	private boolean selectedRouteToBusStation;
	private boolean selectedRouteFromToBusStation;
	private String[] messageFromStation;
	private String[] messageToStation;
	private String[] messageFromToStation;
	
	@PostConstruct
	protected void initModel() {
		
		try {
			initBaseModel(request);
			bus = ctx.isBus;
			selectedRouteBus = ctx.isSelectedRouteBus;
			selectedRouteFromBusStation = false;
			selectedRouteToBusStation = false;
			selectedRouteFromToBusStation = false;
			
			messageFromStation = CheckinUtils.getMessageFromBusToAirport(ctx,i18n);
			selectedRouteFromBusStation = messageFromStation.length > 0;
			
			messageToStation = CheckinUtils.getMessageFromAirportToBus(ctx,i18n);
			selectedRouteToBusStation = messageToStation.length > 0;
			
			messageFromToStation = CheckinUtils.getMessageFromBusToBus(ctx,i18n);
			selectedRouteFromToBusStation = messageFromToStation.length > 0;
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
		}
	}
	

	public boolean isBus() {
		return bus;
	}
	
	public boolean isSelectedRouteBus() {
		return selectedRouteBus;
	}
	
	public boolean isSelectedRouteFromBusStation() {
		return selectedRouteFromBusStation;
	}
	
	public boolean isSelectedRouteToBusStation() {
		return selectedRouteFromToBusStation;
	}
	
	public boolean isSelectedRouteFromToBusStation() {
		return selectedRouteToBusStation;
	}
	
	public String[] getMessageFromStation(){
		return messageFromStation;
	}
	
	public String[] getMessageToStation(){
		return messageToStation;
	}
	
	public String[] getMessageFromToStation(){
		return messageFromToStation;
	}

}