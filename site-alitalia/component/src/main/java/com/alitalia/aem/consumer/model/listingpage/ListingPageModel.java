package com.alitalia.aem.consumer.model.listingpage;

import com.alitalia.aem.consumer.utils.AlitaliaPage;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import java.util.ArrayList;
import java.util.Iterator;

@Model(adaptables={SlingHttpServletRequest.class})
public class ListingPageModel {

    @Self
    private SlingHttpServletRequest slingHttpServletRequest;

    @Inject
    private Page currentPage;

    private ArrayList<AlitaliaPage> childrenList;

    private final static String NO_DEF_PATH = "PATH-NOT-DEFINED";

    @PostConstruct
    protected void initModel() {
        childrenList = new ArrayList<AlitaliaPage>();
        setChildrenList(currentPage);
    }

    // Returns the list of children of the current page as "AlitaliaPage" objects.
    public AlitaliaPage[] getChildrenList() {
        AlitaliaPage[] children = new AlitaliaPage[childrenList.size()];

        return childrenList.toArray(children);
    }

    // Returns the current page title
    public String getTitle() {
        return currentPage.getTitle();
    }

    // Returns the current page description
    public String getDescription() {
        return currentPage.getDescription();
    }

    // Prepares the list of children
    private void setChildrenList(Page rootPage) {
        final Iterator<Page> sectionChildren = rootPage.listChildren(new PageFilter(slingHttpServletRequest));

        while(sectionChildren.hasNext()) {
            final Page child = sectionChildren.next();
            final String childPath = child.getPath();
            final String childTitle = child.getTitle();
            final String childDescription = child.getDescription();
            final String childImage = child.getProperties().get("image/fileReference", NO_DEF_PATH);;
            final String childRedirectPath = child.getProperties().get("redirectTarget", NO_DEF_PATH);

            final AlitaliaPage sectionPage = new AlitaliaPage(childPath, childTitle, childDescription, childImage, childRedirectPath, false);

            boolean isFirst = true;
            final Iterator<Page> subSectionChildren = child.listChildren(new PageFilter(slingHttpServletRequest));
            while(subSectionChildren.hasNext()) {
                final Page subChild = subSectionChildren.next();
                final String subChildPath = subChild.getPath();
                final String subChildTitle = subChild.getTitle();

                if(childRedirectPath.equalsIgnoreCase(NO_DEF_PATH) && isFirst) {
                    sectionPage.setRedirectPath(subChildPath);
                    isFirst = false;
                }

                final AlitaliaPage subSectionPage = new AlitaliaPage(subChildPath, subChildTitle, true);
                sectionPage.addChild(subSectionPage);
            }

            childrenList.add(sectionPage);
        }

    }
}
