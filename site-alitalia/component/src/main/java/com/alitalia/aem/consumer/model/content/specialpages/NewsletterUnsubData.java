package com.alitalia.aem.consumer.model.content.specialpages;

public class NewsletterUnsubData {
	
	public static final String NAME = "NewsletterUnsubData";
	
	private String mail;
	private String error;
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	@Override
	public String toString() {
		return "NewsletterUnsubData [ mail=" + mail + " error=" + error + "]";
	}
	
	
	
}
