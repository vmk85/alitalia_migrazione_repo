/** Modello per costanti MyAlitalia [D.V.] */

package com.alitalia.aem.consumer.myalitalia;

public class MyAlitaliaConstants {

    public static final String MYALITALIA_CONTEXT_ATTRIBUTE = "MyAlitaliaSessionContext";

    public static final String MYALITALIA_ERROR_MESSAGE_SESSION = "Errore caricamento utente in sessione";

    public static final String MYALITALIA_SUCCESS_MESSAGE_SESSION = "Utente caricato correttamente in sessione";

    public static final String MYALITALIA_ACTION_LOGIN = "LOGIN";

    public static final String MYALITALIA_ACTION_LOGOUT = "LOGOUT";

    public static final String MYALITALIA_SUCCESS_REFRESH = "Aggiornamento utente in sessione ";

    public static final String MYALITALIA_SERVICE_ERROR = "ERROR";

    public static final String MYALITALIA_SERVICE_REGISTRATION = "WEB";

    public static final String MYALITALIA_SERVICE_UPDATE = "MYALITALIA";

    public static final String MYALITALIA_SERVICE_NOT_RESPONSE = "myalitalia.service.not.response";

    public static final String MYALITALIA_SERVICE_MISSING_USER = "myalitalia.service.missing.user";
}
