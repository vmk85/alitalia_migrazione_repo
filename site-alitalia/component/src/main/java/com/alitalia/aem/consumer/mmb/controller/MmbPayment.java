package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.PaymentTypeItemData;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.consumer.mmb.render.MmbCreditCardRender;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbPayment extends MmbSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	private MmbPriceRender cartTotalAmount;
	private List<MmbCreditCardRender> creditCards;
	private String paymentErrorMessage;
	
	private Boolean defaultCardExists;
	private String defaultCardType;
	private String defaultCardTypeCssClass;
	private String defaultCardNumberHidden;
	private String defaultCardExpireMonth;
	private String defaultCardExpireYear;
	
	@PostConstruct
	protected void initModel() throws IOException {
		
		initBaseModel(request);
		
		this.cartTotalAmount = new MmbPriceRender(
				ctx.cartTotalAmount, ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat); 
		this.paymentErrorMessage = ctx.paymentErrorMessage;
		this.creditCards = new ArrayList<MmbCreditCardRender>();
		for (PaymentTypeItemData paymentTypeItemData : ctx.creditCards) {
			this.creditCards.add(new MmbCreditCardRender(paymentTypeItemData));
		}
		
		/* gestione visualizzazione carta di credito usata nel booking */
		this.defaultCardExists = false;
		if (ctx.defaultCardNumber != null && !"".equals(ctx.defaultCardNumber)) {
			this.defaultCardExists = true;
			this.defaultCardType = ctx.defaultCardType;
			
			this.defaultCardTypeCssClass = "";
			if (ctx.defaultCardType.equals(CreditCardTypeEnum.AMERICAN_EXPRESS.value())) {
				this.defaultCardTypeCssClass = "americanExpress";
			} else if (ctx.defaultCardType.equals(CreditCardTypeEnum.DINERS.value())) {
				this.defaultCardTypeCssClass = "diners";
			} else if (ctx.defaultCardType.equals(CreditCardTypeEnum.MASTER_CARD.value())) {
				this.defaultCardTypeCssClass = "masterCard";
			} else if (ctx.defaultCardType.equals(CreditCardTypeEnum.UATP.value())) {
				this.defaultCardTypeCssClass = "uatp";
			} else if (ctx.defaultCardType.equals(CreditCardTypeEnum.VISA_ELECTRON.value())) {
				this.defaultCardTypeCssClass = "visaElectron";
			} else if (ctx.defaultCardType.equals(CreditCardTypeEnum.VISA.value())) {
				this.defaultCardTypeCssClass = "visa";
			} else if (ctx.defaultCardType.equals(CreditCardTypeEnum.MAESTRO.value())) {
				this.defaultCardTypeCssClass = "maestro";
			}
			
			this.defaultCardNumberHidden = ""; 
			for (int i = 0; i < ctx.defaultCardNumber.length() - 4; i++) {
				this.defaultCardNumberHidden = this.defaultCardNumberHidden + "*";
			}
			this.defaultCardNumberHidden = this.defaultCardNumberHidden 
					+ ctx.defaultCardNumber.substring(ctx.defaultCardNumber.length() - 4);
			
			if (ctx.defaultCardExpireMonth < 10) {
				this.defaultCardExpireMonth = "0" + String.valueOf(ctx.defaultCardExpireMonth);	
			} else {
				this.defaultCardExpireMonth = String.valueOf(ctx.defaultCardExpireMonth);
			}
			
			if (ctx.defaultCardExpireYear < 10) {
				this.defaultCardExpireYear = "0" + String.valueOf(ctx.defaultCardExpireYear);	
			} else {
				this.defaultCardExpireYear = String.valueOf(ctx.defaultCardExpireYear);
			}
		}
	}

	public MmbPriceRender getCartTotalAmount() {
		return cartTotalAmount;
	}
	
	public String getPaymentErrorMessage() {
		return paymentErrorMessage;
	}
	
	public List<MmbCreditCardRender> getCreditCards() {
		return creditCards;
	}
	
	public Boolean getDefaultCardExists() {
		return defaultCardExists;
	}

	public String getDefaultCardType() {
		return defaultCardType;
	}
	
	public String getDefaultCardTypeCssClass() {
		return defaultCardTypeCssClass;
	}

	public String getDefaultCardNumberHidden() {
		return defaultCardNumberHidden;
	}

	public String getDefaultCardExpireMonth() {
		return defaultCardExpireMonth;
	}

	public String getDefaultCardExpireYear() {
		return defaultCardExpireYear;
	}

}
