package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.Servlet;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.MMGenderTypeEnum;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdatePassengerResponse;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSessionContext;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.checkinrest.render.PnrListDataRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkinpnrselected"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinPnrSelectedServlet extends GenericCheckinFormValidatorServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private AlitaliaConfigurationHolder configuration;


	@Reference
	private CheckinSession checkinSession;


	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
//		logger.info("[CheckinPnrSelectedServlet - performSubmit] prima di initSession....");
		
		
		CheckinSessionContext ctx = getCheckInSessionContext(request);

		if(ctx.pnr != null && ctx.pnr.isEmpty()){

			ctx.pnr = request.getParameter("pnr");

		}

		//ctx.pnrListData;

//		logger.info("[CheckinPnrSelectedServlet - performSubmit] recupero dati dal contesto.");

		try{
			String checkinAllFlights = "N";
			int [] flightitinerary = new int[1];//itinerario 0 è il primo di un andata o un volo singolo di un PNR;
												//itinerario 1 è il volo di ritorno di un PNR di tipo A/R;
												//itinerario 0_1 (split) è un volo Andata e Ritorno collegato con checkinAllFlights valorizzato come 'Y';
				
//			logger.info("[CheckinPnrSelectedServlet] [INIZIO] ");
					
				if (ctx.pnrListData != null){
					int total = 0;
					int executed = 0;
					int notAllowed = 0;
					
					PnrListDataRender renderList = new PnrListDataRender();
					List<PnrRender> pnrListDataRender = new ArrayList<>();
                    String pnr = request.getParameter("pnr");
                    
                    if(request.getParameter("flightitinerary") != null && !request.getParameter("flightitinerary").equals("")){
                    	
                    	if(request.getParameter("flightitinerary").equals("0") ) {
                    		flightitinerary [0] = Integer.parseInt(request.getParameter("flightitinerary"));
                    	}else if(request.getParameter("flightitinerary").equals("1")){
                    		flightitinerary [0] = Integer.parseInt(request.getParameter("flightitinerary"));
                    	}else if(request.getParameter("flightitinerary").equals("0_1")){
                    		String fi =  request.getParameter("flightitinerary");
                			String [] fiArr = fi.split("_");
							flightitinerary = new int[2];
                			flightitinerary[0] = Integer.parseInt(fiArr[0]);
                			flightitinerary[1] = Integer.parseInt(fiArr[1]);
                    	}  
                    	
                    }
                   
                    
                    //Viene Valorizzato ad "Y" solo se si preme il tasto "effettua il checkin per tutti i voli. Riferimento in checkin-lista-voli.js"
                    if(request.getParameter("checkinallflights") != null){
                    	checkinAllFlights = request.getParameter("checkinallflights");
                    }
                    
					pnrListDataRender = renderList.getPnrSelectedListDataRender(ctx.pnrListData, ctx, pnr, total, executed, notAllowed, checkinAllFlights, flightitinerary);

//					pnrListDataRender = removeExternalCompanyFlights(pnrListDataRender);

	                    ctx.pnrSelectedListDataRender = pnrListDataRender;

//					in caso di pnr sostituito da BL dopo lettura del replication db, lo risalvo nel ctx
					if(ctx.pnrSelectedListDataRender != null && !ctx.pnrSelectedListDataRender.get(0).getNumber().equals("")) {
						ctx.pnr = ctx.pnrSelectedListDataRender.get(0).getNumber();
					}

					if(request.getParameter("managecheckin").equals("manage") && request.getParameter("managecheckin") != null) {
						for(PnrRender thisPnr : pnrListDataRender){

							if(thisPnr.getNumber().equals(pnr)){
								List<FlightsRender> flights = thisPnr.getFlightsRender();
								for(FlightsRender f : flights) {
									ctx.selectedFlights.add(f);

									List<SegmentRender>segments =f.getSegments();

									for(SegmentRender seg : segments) {
										List<Passenger> passeengers = new ArrayList<>();
										List<Passenger> totalPassengers = new ArrayList<>();
										if(seg.getPassengers() != null){
										for (Passenger pass : seg.getPassengers()) {
											if (pass.getCheckInComplete() == true) {
												passeengers.add(pass);
											}
										}
										for (Passenger pass : seg.getPassengers()) {
											/*if(pass.getCheckInComplete() == true){*/
											totalPassengers.add(pass);
											/*}*/
										}
										ctx.checkinSelectedPassengers = passeengers;
										// Valorizzo la flag CheckinPerformed della sessione
										int idx = 0;
										boolean check_found = false;
//										for (int idx=0; idx < passeengers.size(); idx++){
										while (idx < passeengers.size() && !check_found) {

											boolean check_ff = false;

											boolean check_name = false;
//											confronto le prime due lettere del nome: quello inserito durante la search e quello restituito dal servizio.
// 											Confronto solo due lettere perche' e' il minimo numero di caratteri inseribili nel form di search.
// 											Inoltre, la BL fa anch'essa un confronto di tipo "start with"
											if(ctx.passengerName.length() != 0) {
												if (passeengers.get(idx).getNome().toLowerCase().replaceAll("\\s+", "").substring(0, 2).equals(ctx.passengerName.toLowerCase().replaceAll("\\s+", "").substring(0, 2))) {
													check_name = true;
												}
											}
											if (passeengers.get(idx).getFrequentFlyer().size() > 0) {
												if (ctx.frequentFlyer.equals(passeengers.get(idx).getFrequentFlyer().get(0).getNumber())) {
													check_ff = true;
												} else {
													check_ff = false;
												}
											}
											if ((check_ff || check_name) && passeengers.get(idx).getCognome().toLowerCase().replaceAll("\\s+","").equals(ctx.passengerSurname.toLowerCase().replaceAll("\\s+",""))) {
//												if (passeengers.get(idx).getCheckInComplete()) {
//													ctx.setPassengerCheckinSearch_CheckinPerformed(true);
////													ctx.passengerCheckinSearch_CheckinPerformed = true;
//												}
												ctx.setPassengerCheckinSearch_CheckinPerformed(passeengers.get(idx).getCheckInComplete());
												check_found = true;
											}
											idx++;
										}
										for (idx = 0; idx < passeengers.size(); idx++) {
											if (!totalPassengers.get(idx).getCheckInComplete()) {
												break;
											}
										}
										if (idx == totalPassengers.size()) {
											ctx.setallPassengersCheckinCompleted(true);
										}
									}
									}
								}

							}

						}
					}

					/** Update passenger per utenti loggati */

					/** Estraggo l'utente loggato */
					Object userLogged = getUserLogged(request);

					if (userLogged != null){

						HashMap<String,String> mapRequest = null;

						mapRequest = getUserDataForChechin(userLogged);

						if (mapRequest != null){

							if (userIsInPassengers(ctx,mapRequest)){

								if(!request.getParameter("flightitinerary").equals("0_1")){

									CheckinUpdatePassengerResponse checkinUpdatePassengerResponse = checkinSession.checkinUpdatePassengerUserLogged(mapRequest,ctx,request);

								}
							}
						}

					}

					managementSucces(request, response);
					
				}else{
					logger.info("[CheckinPnrSelectedServlet] - Errore durante la selezione del PNR." );
					managementError(request, response, "CONTESTO NON VALORIZZATO");		
				}

			}catch(Exception e){
				throw new RuntimeException("[CheckinPnrSelectedServlet] - Errore durante l'invocazione del servizio.", e);
			}
		
	}

//	private List<PnrRender> removeExternalCompanyFlights(List<PnrRender> pnrListDataRender) {
//
//		SegmentRender segmentToRemove = null;
//
//		for (PnrRender pnrRender : pnrListDataRender){
//			for (FlightsRender flightsRender : pnrRender.getFlightsRender()){
//				for (SegmentRender segmentRender : flightsRender.getSegments()){
//					if(segmentRender.getPassengers() == null){
//						segmentToRemove = segmentRender;
//					}
//				}
//				flightsRender.getSegments().remove(segmentToRemove);
//			}
//		}
//
//		return pnrListDataRender;
//	}


	private boolean userIsInPassengers(CheckinSessionContext ctx, HashMap<String, String> mapRequest) {

		boolean isInPassengers = false;

		if (ctx.pnrSelectedListDataRender != null && ctx.pnrSelectedListDataRender.size() > 0) {

			if (ctx.pnrSelectedListDataRender.get(0).getFlightsRender() != null && ctx.pnrSelectedListDataRender.get(0).getFlightsRender().size() > 0 ){

				if (ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments() != null && ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().size() > 0 ){

					/** controllo che l'utente loggato faccia parte dei passeggeri presenti nel volo selezionato */
					for (Passenger passenger : ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().get(0).getPassengers()) {

						if (passenger != null){

							if (passenger.getNome().equals(mapRequest.get("nome")) && passenger.getCognome().equals(mapRequest.get("cognome")) && passenger.getDocumentRequired() != null && passenger.getDocumentRequired().size() ==1 && passenger.getDocumentRequired().get(0).getCode().equalsIgnoreCase("gender") ) {

								isInPassengers = true;

							}
						}
					}
				}
			}
		}

		return isInPassengers;
	}

	private HashMap<String, String> getUserDataForChechin(Object userLogged) {

		HashMap<String,String> stringStringHashMap = null;

		if (userLogged instanceof MyAlitaliaSessionContext) {

			MyAlitaliaSessionContext mactx = (MyAlitaliaSessionContext) userLogged;

			/** Controllo se ho una risposta da parte del crm*/
			if (mactx.getGetCrmDataInfoResponse() != null && mactx.getGetCrmDataInfoResponse().getInfoCliente() != null){

				stringStringHashMap = new HashMap<>();

				/** Controllo l'esistenza dei campi necessari per effettuare l'updarePassenger */
				if (mactx.getGetCrmDataInfoResponse().getInfoCliente().getSesso() != null){

					stringStringHashMap.put("gender",mactx.getGetCrmDataInfoResponse().getInfoCliente().getSesso());

				}

				if (mactx.getGetCrmDataInfoResponse().getInfoCliente().getNome() != null){

					stringStringHashMap.put("nome",mactx.getGetCrmDataInfoResponse().getInfoCliente().getNome());

				}

				if (mactx.getGetCrmDataInfoResponse().getInfoCliente().getCognome() != null){

					stringStringHashMap.put("cognome",mactx.getGetCrmDataInfoResponse().getInfoCliente().getCognome());

				}
				//Todo : controllare se servono documenti

				if (!(stringStringHashMap.containsKey("gender") && stringStringHashMap.containsKey("nome") && stringStringHashMap.containsKey("cognome"))){

					stringStringHashMap = null;

				}
			}

		} else if (userLogged instanceof MMCustomerProfileData){

			MMCustomerProfileData mmCustomerProfileData = (MMCustomerProfileData) userLogged;
			stringStringHashMap = new HashMap<>();

			/** Controllo l'esistenza dei campi necessari per effettuare l'updarePassenger */
			if (mmCustomerProfileData.getGender() != null){

				stringStringHashMap.put("gender",mmCustomerProfileData.getGender().value().equals(MMGenderTypeEnum.MALE.value())? "M" : mmCustomerProfileData.getGender().value().equals(MMGenderTypeEnum.FEMALE.value())? "F" : null);

			}

			if (mmCustomerProfileData.getCustomerName() != null){

				stringStringHashMap.put("nome",mmCustomerProfileData.getCustomerName());

			}

			if (mmCustomerProfileData.getCustomerSurname() != null){

				stringStringHashMap.put("cognome",mmCustomerProfileData.getCustomerSurname());

			}
			//Todo : controllare se servono documenti

			if (!(stringStringHashMap.containsKey("gender") && stringStringHashMap.containsKey("nome") && stringStringHashMap.containsKey("cognome"))){

				stringStringHashMap = null;

			}
		}

		return stringStringHashMap;
	}

	private Object getUserLogged(SlingHttpServletRequest request) {
		Object userLogged = null;

		MyAlitaliaSessionContext mactx = MyAlitaliaUtils.getMactxBySession(request);
		MMCustomerProfileData customerProfileData = AlitaliaUtils.getAuthenticatedUser(request);

		if (customerProfileData != null){

			userLogged = customerProfileData;

		} else if (mactx != null){
			userLogged = mactx;

		}

		return userLogged;

	}


	public void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error){
		
		logger.info("[CheckinPnrSelectedServlet] [managementError] error =" + error );
		
		TidyJSONWriter json;
		try {
			json = new TidyJSONWriter(response.getWriter());
		
			response.setContentType("application/json");
			
			json.object();
			
			json.key("isError").value(true);
			json.key("errorMessage").value(error);
			json.key("pnr").value("");
			json.endObject();
		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
	}
	
	public void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response){
		
		logger.info("[CheckinPnrSelectedServlet] [managementSucces] ...");

		String successPage;

		if(request.getParameter("managecheckin").equals("manage") && request.getParameter("managecheckin") != null) {
			successPage = AlitaliaUtils.findSiteBaseExternalUrl(
					request.getResource(), false)
					+ getConfiguration().getCheckinManagePage();
		}
		else {
			successPage = AlitaliaUtils.findSiteBaseExternalUrl(
					request.getResource(), false)
					+ getConfiguration().getCheckinPassengersPage();
		}

		successPage = request.getResourceResolver().map(successPage);
		
		logger.info("[CheckinPnrSelectedServlet] [managementSucces] successPage = " + successPage );
		
		TidyJSONWriter json;
		try {
			
			json = new TidyJSONWriter(response.getWriter());
			response.setContentType("application/json");
			json.object();
			json.key("isError").value(false);
			json.key("successPage").value(successPage);
			json.endObject();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
		
	}

}
