package com.alitalia.aem.consumer.millemiglia.model.richiedimiglia.exceptions;

@SuppressWarnings("serial")
public class MissingTicketException extends Exception {

	public MissingTicketException() {
		// TODO Auto-generated constructor stub
	}

	public MissingTicketException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public MissingTicketException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public MissingTicketException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public MissingTicketException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
