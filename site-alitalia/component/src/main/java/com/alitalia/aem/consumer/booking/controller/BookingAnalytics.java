package com.alitalia.aem.consumer.booking.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.analytics.AnalyticsInfo;
import com.alitalia.aem.consumer.booking.analytics.MetaInfo;
import com.alitalia.aem.consumer.booking.analytics.filler.AnalyticsInfoFactory;
import com.alitalia.aem.consumer.booking.analytics.filler.AnalyticsInfoFiller;
import com.alitalia.aem.consumer.booking.analytics.filler.AnalyticsInfoToJsObj;
import com.alitalia.aem.consumer.booking.analytics.filler.MetaInfoFactory;
import com.alitalia.aem.consumer.booking.analytics.filler.MetaInfoFiller;
import com.alitalia.aem.consumer.booking.analytics.filler.MetaInfoToJsObj;
import com.alitalia.aem.consumer.booking.model.DatalayerMonitoring;
import com.google.gson.Gson;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingAnalytics extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;

	@Inject
	AlitaliaConfigurationHolder configurationHolder;
	
	private final static String PUBL1 = "10.50.3.28";
	private final static String PUBL2 ="10.50.3.54";
	private final static String PUBL3 ="10.50.3.93";
	private final static String PUBL4 ="10.50.3.117";
	private final static String PUBL5 ="10.50.3.62";
	private final static String PUBL6 ="10.50.3.69";
	private final static String PUBL7 ="10.50.3.31";
	private final static String PUBL8 ="10.50.3.89";
	private final static String PUBL9 ="10.50.3.52";
	private final static String PUBL10 ="10.50.3.56";
	private final static String PUBLISHER1 = "publisher1";
	private final static String PUBLISHER2 = "publisher2";
	private final static String PUBLISHER3 = "publisher3";
	private final static String PUBLISHER4 = "publisher4";
	private final static String PUBLISHER5 = "publisher5";
	private final static String PUBLISHER6 = "publisher6";
	private final static String PUBLISHER7 = "publisher7";
	private final static String PUBLISHER8 = "publisher8";
	private final static String PUBLISHER9 = "publisher9";
	private final static String PUBLISHER10 = "publisher10";

	private int step;
	
	private String dlVar="{}";
	private String dlVarMonitoring="{}";
	private String webtrendsMeta="{}";




	@PostConstruct
	protected void initModel() throws Exception {
		try {
			Gson gson = new Gson();
			DatalayerMonitoring dmObj = new DatalayerMonitoring();
			
			
			ctx = (BookingSessionContext) request.getSession(true).getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
			if(ctx != null){
				step = computeBookingStep();
				short key = MetaInfoFactory.BOOKING;
				if(ctx.award){
					key = MetaInfoFactory.BOOKING_AWARD;
					step = computeBookingAwardStep();
				}
				else if (ctx.isCarnetProcess){
					key = MetaInfoFactory.CARNET;
				}
				AnalyticsInfoFiller filler = AnalyticsInfoFactory.getFiller(key);
				AnalyticsInfo info = filler.fillInfo(ctx, step, configurationHolder);
				AnalyticsInfoToJsObj toJs = AnalyticsInfoFactory.getJsConverter(key);
				dlVar = toJs.toJSObj(info, false);
				
				dmObj.setUserID(ctx.sessionId);
				
				MetaInfoFiller metaFiller = MetaInfoFactory.getFiller(key);
				MetaInfo meta = metaFiller.fillInfo(ctx, step, configurationHolder);
				MetaInfoToJsObj metaToJs = MetaInfoFactory.getJsConverter(key);
				webtrendsMeta = metaToJs.toJSObj(meta, false);

			}
			dmObj.setServerID(mappingServerIP());
			dlVarMonitoring = gson.toJson(dmObj, DatalayerMonitoring.class);
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			dlVar="{}";
			webtrendsMeta = "{}";
		}
	}

	
	private int computeBookingStep(){
		String path = request.getRequestURL().toString();
		if (request.getRequestPathInfo().getSelectorString() != null &&
				!request.getRequestPathInfo().getSelectorString().isEmpty()) {
			path = request.getRequestURL().toString().replace(
					request.getRequestPathInfo().getSelectorString() + ".", "");
		}
		int step = 0;
		
		if(path.endsWith(configurationHolder.getBookingFlightSelectPage())){
			step = 1;
		}
		/*Passenger data*/
		else if(path.endsWith(configurationHolder.getBookingPassengersDataPage())){
			step = 2;
		}
		/*Ancillary*/
		else if(path.endsWith(configurationHolder.getBookingAncillaryPage())){
			step = 3;
		}
		/*Ancillary Payment*/
		else if(path.endsWith(configurationHolder.getBookingPaymentPage())){
			step = 4;
		}
		/*Ancillary Confirmation*/
		else if(path.endsWith(configurationHolder.getBookingConfirmationPage())){
			step = 5;
		}
		
		return step;
	}
	
	private int computeBookingAwardStep(){
		String path = request.getRequestURL().toString();
		if (request.getRequestPathInfo().getSelectorString() != null &&
				!request.getRequestPathInfo().getSelectorString().isEmpty()) {
			path = request.getRequestURL().toString().replace(
					request.getRequestPathInfo().getSelectorString() + ".", "");
	}
		int step = 0;
		/*Home*/
//		if(path.endsWith(configurationHolder.getBookingAwardHomePage())){
//			step = 1;
//		}
		/*Calendar*/
		if(path.endsWith(configurationHolder.getBookingAwardCalendarPage())){
			step = 1;
	}
		/*Flight select*/
		else if(path.endsWith(configurationHolder.getBookingAwardFlightSelectPage())){
			step = 1;
	}
		/*Passenger data*/
		else if(path.endsWith(configurationHolder.getBookingPassengersDataPage())){
			step = 2;
			}
		/*Ancillary*/
		else if(path.endsWith(configurationHolder.getBookingAncillaryPage())){
			step = 3;
		}
		/*Ancillary Payment*/
		else if(path.endsWith(configurationHolder.getBookingPaymentPage())){
			step = 4;
	}
		/*Ancillary Confirmation*/
		else if(path.endsWith(configurationHolder.getBookingConfirmationPage())){
			step = 5;
				}
				
		return step;
			}
		
	private String mappingServerIP() {
		String serverip = "unknown";
		InetAddress ip;
		try {
			ip = InetAddress.getLocalHost();
			if(ip != null)
				serverip =  ip.getHostAddress();

		} catch (UnknownHostException e) {
			logger.error("{Booking-Analytics mappingServerIP} Unexpected error: ", e);
		}
		if(!serverip.equals("unknown")) {
			switch (serverip) {
				case PUBL1 : serverip = PUBLISHER1;
							break;
				case PUBL2 : serverip = PUBLISHER2;
							break;
				case PUBL3 : serverip = PUBLISHER3;
							break;
				case PUBL4 : serverip = PUBLISHER4;
							break;
				case PUBL5 : serverip = PUBLISHER5;
							break;
				case PUBL6 : serverip = PUBLISHER6;
							break;
				case PUBL7 : serverip = PUBLISHER7;
							break;
				case PUBL8 : serverip = PUBLISHER8;
							break;
				case PUBL9 : serverip = PUBLISHER9;
							break;
				case PUBL10 : serverip = PUBLISHER10;
							break;
				default: serverip = serverip;
							break;
			}
		}
		return serverip;
	}
	
	public String getWebtrendsMeta() {
		return webtrendsMeta;
	}

	public String getDlVar() {
		return dlVar;
	}
	
	public String getDlVarMonitoring() {
		return dlVarMonitoring;
	}

	public int getStep() {
		return step;
	}


}