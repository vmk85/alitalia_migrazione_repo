package com.alitalia.aem.consumer.millemiglia.render;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateRender{
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	Calendar date;
	
	public DateRender(Calendar date) {
		this.date = date;
	}
	
	public String getDay() {
		String day = "";
		try {
			day = Integer.toString(date.get(Calendar.DATE));
			if(date.get(Calendar.DATE)<10){
				day = "0" + day;
			}
		} catch (Exception e) {
			logger.error("Errore format giorno", e);
		}
		return day;
	}
	
	public String getMonth() {
		String month = "";
		try {
			month = Integer.toString(date.get(Calendar.MONTH)+1);
			if((date.get(Calendar.MONTH)+1)<10)
				month = "0" + month;
		} catch (Exception e) {
			logger.error("Errore format mese", e);
		}
		return month;
	}
	
	public String getYear() {
		String year = "";
		try {
			year = Integer.toString(date.get(Calendar.YEAR));
			//year = year.substring(2);
		} catch (Exception e) {
			logger.error("Errore format anno", e);
		}
		return year;
	}
	
	public String getExtendedDate() {
		String extendedDate = "";
		try {
			extendedDate = getDay() + "/" + getMonth() + "/" + getYear();
		} catch (Exception e) {
			logger.error("Errore format data", e);
		}
		return extendedDate;
	}
	
	public String getHour() {
		String hour = "";
		try {
			int m = date.get(Calendar.MINUTE);
			int h = date.get(Calendar.HOUR_OF_DAY);
			String minutes = Integer.toString(m);
			String hours = Integer.toString(h);
			if(m<10){
				minutes = "0" + minutes;
			}
			if(h<10){
				hours = "0" + hours;
			}
			hour = hours + ":" +  minutes;
		} catch (Exception e) {
			logger.error("Errore format ora", e);
		}
		return hour;
	}
	
}
