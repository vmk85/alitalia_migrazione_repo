package com.alitalia.aem.consumer.checkin.servlet;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.enumerations.CheckinSearchReasonEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checkinpreparepassengerdata" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinPreparePassengersDataServlet extends GenericCheckinFormValidatorServlet {
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private CheckinSession checkinSession;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		// no input checked
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		
		String routeIndex = request.getParameter(CheckinConstants.CHECKIN_SELECTED_ROUTE_PARAM);
		String baseUrl = request.getResource().getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
		
		if (routeIndex == null || routeIndex.isEmpty()) {
			response.sendRedirect(baseUrl + configuration.getCheckinFailurePage());
		} else {
			checkinSession.prepareSelectedRoute(routeIndex, ctx);
			
			if (ctx.selectedRoute == null) {
				throw new RuntimeException("ctx.selectedRoute is null");
			}
			
			checkinSession.getPassengerList(ctx, CheckinSearchReasonEnum.CHECK_IN);
			response.sendRedirect(baseUrl + configuration.getCheckinPassengersPage());
		}
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}
