package com.alitalia.aem.consumer.checkin.render;

import java.util.LinkedHashMap;
import java.util.Map;

public class CheckinSeatMapFlight {
	
	private String flightNumber;
	private String nextFlightNumber;
	private boolean isBus;
	private Map<String, CheckinSeatMapRender> seatMaps;
	
	public CheckinSeatMapFlight(String flightNumber, String nextFlightNumber) {
		this.flightNumber = flightNumber;
		this.nextFlightNumber = nextFlightNumber;
		this.seatMaps = new LinkedHashMap<String, CheckinSeatMapRender>();
	}
	
	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getNextFlightNumber() {
		return nextFlightNumber;
	}

	public void setNextFlightNumber(String nextFlightNumber) {
		this.nextFlightNumber = nextFlightNumber;
	}

	public Map<String, CheckinSeatMapRender> getSeatMaps() {
		return seatMaps;
	}

	public void setSeatMaps(
			Map<String, CheckinSeatMapRender> seatMaps) {
		this.seatMaps = seatMaps;
	}

	public boolean isBus() {
		return isBus;
	}

	public void setBus(boolean isBus) {
		this.isBus = isBus;
	}
	
}
