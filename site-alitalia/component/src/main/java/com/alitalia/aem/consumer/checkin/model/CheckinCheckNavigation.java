package com.alitalia.aem.consumer.checkin.model;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinCheckNavigation extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private SlingHttpServletResponse response;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;

	@PostConstruct
	protected void initModel() throws IOException {
		
		try {
			initBaseModel(request);
		} catch (Exception e) {
			logger.error("", e);
		}
		
		String path = request.getRequestURL().toString();
		boolean notValid = !isWCMEnabled() && ctx == null;
		
		if (!isWCMEnabled() && !notValid) {
			if (path.endsWith(
					configuration.getCheckinFlightListPage())) {
				notValid = ctx.routesList == null ||
						ctx.routesList.isEmpty();
				
			} else if (path.endsWith(
					configuration.getCheckinPassengersPage())) {
				notValid = ctx.selectedRoute == null ||
						ctx.passengers == null ||
						ctx.passengers.isEmpty() ||
						CheckinUtils.getNotCheckedInPassengers(
								ctx.passengers).isEmpty();
				
			} else if (path.endsWith(
					configuration.getCheckinAncillaryPage())) {
				notValid = ctx.selectedRoute == null ||
						ctx.passengers == null ||
						ctx.passengers.isEmpty();
				
			} else if (path.endsWith(
					configuration.getCheckinBoardingPassPage())) {
				notValid = ctx.selectedRoute == null ||
						ctx.passengers == null ||
						ctx.passengers.isEmpty() ||
						CheckinUtils.getCheckedInPassengers(
								ctx.passengers).isEmpty();
				
			} else if (path.endsWith(
					configuration.getCheckinConfirmationPage())) {
				notValid = ctx.selectedRoute == null ||
						ctx.passengers == null ||
						ctx.passengers.isEmpty() ||
						CheckinUtils.getCheckedInPassengers(
								ctx.passengers).isEmpty();
			}
		}
		if (notValid) {
			logger.error(getCause(ctx));
			String baseUrl = request.getResource().getResourceResolver().map(
					AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
			response.sendRedirect(baseUrl + configuration.getCheckinFailurePage());
		}
	}
	
	private String getCause(CheckinSessionContext ctx) {
		String result = ctx == null ? "ctx = null" : "";
		if (ctx != null) {
			result += "{";
			result += ctx.selectedRoute == null ? " selectedRoute = null;" : "";
			result += ctx.passengers == null ? " passengers = null;" : "";
			if (ctx.passengers != null) {
				result += ctx.passengers.isEmpty() ? " passengers = {};" : "";
				
				List<CheckinPassengerData> checkedInPassengers = 
						CheckinUtils.getCheckedInPassengers(ctx.passengers);
				
				result += checkedInPassengers.isEmpty() ? " checkedInPassengers = {}" : "";
				
				List<CheckinPassengerData> notCheckedInPassengers = 
						CheckinUtils.getNotCheckedInPassengers(ctx.passengers);
				
				result += notCheckedInPassengers.isEmpty() ? " notCheckedInPassengers = {}" : "";
			}
			result += ctx.routesList == null ? " routeList = null;" : "";
			if (ctx.routesList != null) {
				result += ctx.routesList.isEmpty() ? " routeList = {};" : "";
			}
			result += ctx.acceptedPassengers == null ? " acceptedPassengers = null;" : "";
			if (ctx.acceptedPassengers != null) {
				result += ctx.acceptedPassengers.isEmpty() ? " acceptedPassengers = {};" : "";
			}
			result += "}";
		}
		return result;
	}
}
