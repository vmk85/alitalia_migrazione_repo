package com.alitalia.aem.consumer.utils;

public class Language {

	private String name;
	private String link;
	private boolean selected;
	private String code;

	public Language(String name, String link, String code) {
		setName(name);
		setLink(link);
		setSelected(false);
		setLanguageCode(code);
	}

	public Language(String name, String link, boolean selected, String code) {
		setName(name);
		setLink(link);
		setSelected(selected);
		setLanguageCode(code);
	}

	public String getName() {
		return name;
	}

	public String getLink() {
		return link;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getLanguageCode() {
		return code;
	}

	public void setLanguageCode(String code) {
		this.code = code;
	}

	public String toJson() {
		String jsonString = "{";

		jsonString += "name: \"" + name + "\", ";
		jsonString += "link: \"" + link + "\", ";
		jsonString += "code: \"" + code + "\"";
		jsonString += "}";

		return jsonString;
	}

}
