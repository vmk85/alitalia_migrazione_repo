package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingPhaseEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.mmb.MmbSession;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbAncillary extends MmbSessionAncillaryGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private MmbSession mmbSession;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;

	
	private boolean insuranceDisplayed;

	private boolean seatDisplayed;

	private boolean mealDisplayed;

	private boolean extrabaggageDisplayed;

	private boolean fasttrackDisplayed;

	private boolean loungeDisplayed;

	private boolean cartDisplayed;
	private boolean notCartDisplayedInBookingConfirmation;
	@PostConstruct
	protected void initModel() throws IOException {
		try {
			initBaseModel(request);
			
		    
		    
			if (!isWCMEnabled()) {
				mmbSession.initializeCartIfRequired(ctx);
			}
			insuranceDisplayed = isWCMEnabled() || mmbSession.isAncillaryBoxDisplayed(ctx, MmbAncillaryTypeEnum.INSURANCE);
			seatDisplayed = isWCMEnabled() || mmbSession.isAncillaryBoxDisplayed(ctx, MmbAncillaryTypeEnum.SEAT);
			mealDisplayed = isWCMEnabled() || mmbSession.isAncillaryBoxDisplayed(ctx, MmbAncillaryTypeEnum.MEAL);
			extrabaggageDisplayed = isWCMEnabled() || mmbSession.isAncillaryBoxDisplayed(ctx, MmbAncillaryTypeEnum.EXTRA_BAGGAGE);
			fasttrackDisplayed = isWCMEnabled() || mmbSession.isAncillaryBoxDisplayed(ctx, MmbAncillaryTypeEnum.FAST_TRACK);
			loungeDisplayed = isWCMEnabled() || mmbSession.isAncillaryBoxDisplayed(ctx, MmbAncillaryTypeEnum.VIP_LOUNGE);
			cartDisplayed = isWCMEnabled() || mmbSession.isAncillaryCartDisplayed(ctx);
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}
		String path = request.getRequestURL().toString();
	      if (request.getRequestPathInfo().getSelectorString() != null &&
	          !request.getRequestPathInfo().getSelectorString().isEmpty()) {
	            path = request.getRequestURL().toString().replace(
	                request.getRequestPathInfo().getSelectorString() + ".", "");
	      }
		 notCartDisplayedInBookingConfirmation = path.endsWith(configuration.getBookingConfirmationPage());

	}
	
	public boolean isInsuranceDisplayed() {
		return insuranceDisplayed;
	}
	
	public boolean isSeatDisplayed() {
		return seatDisplayed;
	}
	
	public boolean isMealDisplayed() {
		return mealDisplayed;
	}
	
	public boolean isExtrabaggageDisplayed() {
		return extrabaggageDisplayed;
	}
	
	public boolean isFasttrackDisplayed() {
		return fasttrackDisplayed;
	}
	
	public boolean isLoungeDisplayed() {
		return loungeDisplayed;
	}
	
	public boolean isCartDisplayed() {
		return cartDisplayed;
	}
	public boolean isNotCartDisplayedInConfirmationPage(){
		return notCartDisplayedInBookingConfirmation;
	}
	
}
