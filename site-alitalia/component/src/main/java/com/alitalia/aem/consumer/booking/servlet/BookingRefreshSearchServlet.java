package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "bookingrefreshsearchconsumer" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" })
})
@SuppressWarnings("serial")
public class BookingRefreshSearchServlet extends SlingSafeMethodsServlet {

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private BookingSession bookingSession;
	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException { 

		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
		BookingSessionContext ctx = null;

		try{

			response.setContentType("application/json");
			json.object();

			/* -- RECUPERO DATI DALLA FORM -- */
			String solutionId = request.getParameter("solutionId");
			String indexFlight = request.getParameter("indexFlight");
			String indexBrand = request.getParameter("indexBrand");
			String indexRoute = request.getParameter("indexRoute");

			if(indexFlight == null || ("").equals(indexFlight)){
				logger.error("indexFlight not found");
				throw new RuntimeException();
			}
			int solutionFlightIndex = Integer.parseInt(indexFlight);

			if(indexBrand == null || ("").equals(indexBrand)){
				logger.error("indexBrand not found");
				throw new RuntimeException();
			}
			int solutionBrandIndex = Integer.parseInt(indexBrand);

			if(("").equals(solutionId) || solutionId==null ){
				logger.error("solutionId not found");
				throw new RuntimeException();
			}

			int elementSelectionIndex = Integer.parseInt(indexRoute);
			if(indexRoute == null || ("").equals(indexRoute) || (elementSelectionIndex != 1 && elementSelectionIndex != 0) ){
				logger.error("indexRoute not found");
				throw new RuntimeException();
			}

			ctx = (BookingSessionContext) request.getSession().getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
			bookingSession.performFlightSelection(ctx, elementSelectionIndex, solutionId, solutionFlightIndex, solutionBrandIndex);
			int indexToRefresh = bookingSession.obtainRouteIndexToRefresh(ctx,elementSelectionIndex);
			json.key("indexToRefresh").value(indexToRefresh);
			json.key("refreshRibbon").value(1);
			if(!ctx.isCarnetProcess){
				writeAnalyticsInfo(ctx, json, indexRoute);
			}
			json.endObject();
		}catch(Exception e){
			if (ctx == null) {
				logger.error("Unexpected error - ctx is null");
			} else {
				logger.info("Unexpected error - details: {}", ctx.searchElements);
				logger.error("Unexpected error: ", e);
			}
			try {
				json.key("redirect").value(baseUrl + configuration.getBookingFailurePage());
				json.endObject();
			} catch (JSONException e1) {
				logger.error("Error json creation: ", e1);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
	}

	private void writeAnalyticsInfo(BookingSessionContext ctx, TidyJSONWriter json, String indexRoute) throws JSONException{
		try {
			json.key("analytics_flightInfo").object();
			DecimalFormat format = new DecimalFormat("#0.00");
			DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
			otherSymbols.setDecimalSeparator('.');
			otherSymbols.setCurrencySymbol(""); 
			format.setDecimalFormatSymbols(otherSymbols);
			format.setGroupingUsed(false);
			format.setPositivePrefix("");
			format.setNegativePrefix("");
			/*Fare Basis Info*/
			try{
				if(ctx.selectionRoutes != null){
					ResultBookingDetailsData details = ((ResultBookingDetailsData)ctx.selectionRoutes.getProperties().get("BookingDetails"));
					if(details != null){
						String depFareBasis = details.getSolutionField().get(0).getPricingField().get(0).getFareField().get(0).getExtendedFareCodeField();
						json.key("depFareBasis").value(depFareBasis);
						if(!BookingSearchKindEnum.SIMPLE.equals(ctx.searchKind)){
							String retFareBasis = details.getSolutionField().get(0).getPricingField().get(0).getFareField().get(1).getExtendedFareCodeField();
							json.key("retFareBasis").value(retFareBasis);
						}
					}
				}
			}
			catch(Exception e){
				if(logger.isDebugEnabled())
					logger.debug("Exception while retriving farebasis" + e);
			}
			/*Computing fare, surcharges, taxes, totalPrice*/
			List<TaxData> selectionTaxes = ctx.selectionTaxes;
			
			double fareDouble = 0;
			double taxesDouble = 0;
			double surchargesDouble = 0;
			double totalPriceDouble = 0;
			if(selectionTaxes != null 
					&& ((BookingSearchKindEnum.ROUNDTRIP.equals(ctx.searchKind) && "1".equals(indexRoute)) 
							|| (!BookingSearchKindEnum.ROUNDTRIP.equals(ctx.searchKind)))){
				for(TaxData taxData : selectionTaxes){
					totalPriceDouble += taxData.getAmount().doubleValue();
					if(taxData.getCode().toLowerCase().contains("fare")){
						fareDouble += taxData.getAmount().doubleValue();
					}
					else if(taxData.getCode().toLowerCase().contains("taxtotal")){
						taxesDouble += taxData.getAmount().doubleValue();
					}
					else if(taxData.getCode().toLowerCase().contains("yqtotal")){
						surchargesDouble += taxData.getAmount().doubleValue();
					}
				}
				/*add extra charges*/
				totalPriceDouble += ctx.totalExtraCharges.doubleValue();
			}
			
			BigDecimal fare = new BigDecimal(fareDouble);
			BigDecimal taxes = new BigDecimal(taxesDouble);
			BigDecimal surcharges = new BigDecimal(surchargesDouble);
			BigDecimal totalPrice = new BigDecimal(totalPriceDouble);
			
			json.key("fare").value(format.format(fare));
			json.key("taxes").value(format.format(taxes));
			json.key("surcharges").value(format.format(surcharges));
			json.key("totalPrice").value(format.format(totalPrice));
			/*TODO tasso di cambio?*/
			json.key("totalPriceEuro").value(format.format(totalPrice));

			if(ctx.flightSelections != null){
				int i= 0;
				String[] prefixes = {"dep", "ret"};
				for(String prefix : prefixes){
					if(ctx.preSellupBrandData != null 
							&& ctx.preSellupBrandData.size() > i 
							&& ctx.preSellupBrandData.get(i) != null){
						BrandData brand = ctx.preSellupBrandData.get(i);
						String brandName = ctx.codeBrandMap.get(brand.getCode().toLowerCase()).getTitle();
						String brandPrice = format.format(brand.getGrossFare());
						json.key(prefix + "Brand").value(brandName);
						json.key(prefix + "cost").value(brandPrice);
					}
					else if(ctx.flightSelections.length > i && ctx.flightSelections[i] != null){
						FlightData genericFlight = ctx.flightSelections[i].getFlightData();
						String brandCode = ctx.flightSelections[i].getSelectedBrandCode();
						List<BrandData> brands = new ArrayList<BrandData>(0);
						if(genericFlight instanceof DirectFlightData){
							brands = ((DirectFlightData) genericFlight).getBrands();
						}
						else if(genericFlight instanceof ConnectingFlightData){
							brands = ((ConnectingFlightData) genericFlight).getBrands();
						}
						for(BrandData br : brands){
							if(br.getCode().equals(brandCode)){
								String brandName = ctx.codeBrandMap.get(brandCode.toLowerCase()).getTitle();
								String brandPrice = format.format(br.getGrossFare());
								json.key(prefix + "Brand").value(brandName);
								json.key(prefix + "cost").value(brandPrice);
							}
						}

					}
					i++;
				}
			}
			json.endObject();
		} catch (Exception e) {
			json.endObject();
			throw e;
		}

	}
}
