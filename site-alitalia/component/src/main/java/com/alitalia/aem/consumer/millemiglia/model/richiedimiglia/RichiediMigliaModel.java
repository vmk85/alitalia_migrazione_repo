package com.alitalia.aem.consumer.millemiglia.model.richiedimiglia;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.global.enumeration.RichiediMigliaNumberCode;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables={SlingHttpServletRequest.class})
public class RichiediMigliaModel extends GenericBaseModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	private List<String> numberCodeList;
	private RichiediMigliaData richiediMigliaData;
	private Boolean resultRichiediMigliaSubmit;
	
	@PostConstruct
	protected void initModel(){
		try {
			super.initBaseModel(request);
			logger.debug("Init model RichiediMigliaModel");
			
			numberCodeList = RichiediMigliaNumberCode.listValues();

			//TODO: gestire i codici di errore
			
			richiediMigliaData = (RichiediMigliaData) request.getSession().getAttribute(RichiediMigliaData.NAME);
			request.getSession().removeAttribute(RichiediMigliaData.NAME);
			
			if (request.getParameter("success") != null && request.getParameter("success").equals("true")) {
				resultRichiediMigliaSubmit = true;
			} else if (request.getParameter("success") != null && request.getParameter("success").equals("false")) {
				resultRichiediMigliaSubmit = false;
			}
			
			
		} catch (Exception e) {
			logger.error(e.toString());
		}
	}
	
	public List<String> getNumberCodeList() {
		return numberCodeList;
	}

	public RichiediMigliaData getRichiediMigliaData() {
		return richiediMigliaData;
	}
	
	public Boolean getResultRichiediMigliaSubmit() {
		return resultRichiediMigliaSubmit;
	}
}
