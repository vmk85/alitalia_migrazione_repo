package com.alitalia.aem.consumer.checkinrest.jobs.consumer;

import com.alitalia.aem.common.data.checkinrest.model.offload.response.FlightPassR;
import com.alitalia.aem.common.data.checkinrest.model.offload.response.PnrFlightInfoR;
import com.alitalia.aem.common.data.checkinrest.model.offload.response.SeatListR;
import com.alitalia.aem.common.messages.checkinrest.*;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.checkinrest.jobs.CheckinJobConstants;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.alitalia.aem.web.component.webcheckin.delegaterest.IInsuranceDelegate;
import com.google.gson.Gson;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@Component(
        label = "Alitalia CheckinRestAPI - Offload Passenger and Clear Ancillaries Job Consumer",
        description = "Call Rest API (BL) for offload and clear ancillaries",

        // One of the few cases where immediate = true; this is so the Event Listener starts listening immediately
        immediate = true
)
@Properties({
        @Property(
                label = "Alitalia CheckinRestAPI - Offload Passenger and Clear Ancillaries Job Consumer",
                value = CheckinJobConstants.TOPIC_PASSENGER_OFFLOAD_AND_CLEAR_ANCILLARIES,
                description = "Call Rest API (BL) for offload and clear ancillaries",
                name = JobConsumer.PROPERTY_TOPICS,
                propertyPrivate = true
        )
})
@Service(value={JobConsumer.class})
@SuppressWarnings("unchecked")
public class OffloadPassengerAndClearAncillariesJob implements JobConsumer {

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile ICheckinDelegate checkInDelegateRest;

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile IInsuranceDelegate insuranceDelegateRest;

    private static final Logger logger = LoggerFactory.getLogger(OffloadPassengerAndClearAncillariesJob.class);

    final public static String PROPERTY_CHECKIN_SESSION_ID = "checkinrest-sessionid";

    //offload
    final public static String PROPERTY_CHECKIN_OFFLOAD = "checkinrest-offload";

    //clear ancillaries
    final public static String PROPERTY_CHECKIN_CLEARANCILLARIES = "checkinrest-clearancillaries";

    //clear insurance
    final public static String PROPERTY_CHECKIN_CLEARINSURANCE = "checkinrest-clearinsurance";

    public JobResult process(final Job job) {
        // process the job and return the result
        String sessionID = job.getProperty(PROPERTY_CHECKIN_SESSION_ID).toString();
        logger.debug("[{}] OffloadPassengerAndClearAncillariesJob starting", sessionID);

        Gson gson = new Gson();

        List<CheckinOffloadFlightsRequest> offloadRequestList = new ArrayList<>();
        List<CheckinClearAncillarySessionEndRequest> clearAncillariesRequestList = new ArrayList<>();
        List<DeleteInsuranceRequest> deleteInsuranceRequestList = new ArrayList<>();


        for(String propertyItemName : job.getPropertyNames()) {
            if(propertyItemName.startsWith(OffloadPassengerAndClearAncillariesJob.PROPERTY_CHECKIN_CLEARANCILLARIES) ||
                    propertyItemName.startsWith(OffloadPassengerAndClearAncillariesJob.PROPERTY_CHECKIN_OFFLOAD) ||
                    propertyItemName.startsWith(OffloadPassengerAndClearAncillariesJob.PROPERTY_CHECKIN_CLEARINSURANCE)) {

                String jsonProperty = job.getProperty(propertyItemName).toString();

                try {
                    if(propertyItemName.startsWith(OffloadPassengerAndClearAncillariesJob.PROPERTY_CHECKIN_CLEARANCILLARIES)) {
                        CheckinClearAncillarySessionEndRequest reqItem = gson.fromJson(jsonProperty, CheckinClearAncillarySessionEndRequest.class);
                        clearAncillariesRequestList.add(reqItem);
                    } else if(propertyItemName.startsWith(OffloadPassengerAndClearAncillariesJob.PROPERTY_CHECKIN_OFFLOAD)) {
                        CheckinOffloadFlightsRequest reqItem = gson.fromJson(jsonProperty, CheckinOffloadFlightsRequest.class);
                        offloadRequestList.add(reqItem);
                    } else if(propertyItemName.startsWith(OffloadPassengerAndClearAncillariesJob.PROPERTY_CHECKIN_CLEARINSURANCE)) {
                        DeleteInsuranceRequest deleteInsuranceRequest = gson.fromJson(jsonProperty, DeleteInsuranceRequest.class);
                        deleteInsuranceRequestList.add(deleteInsuranceRequest);
                    }
                } catch(Exception e) {
                    logger.error("[{}] cannot deserialize request {} " + e.toString(), sessionID,jsonProperty);
                }
            }
        }

        if(offloadRequestList.size() > 0) {
            Boolean status = true;
            //TODO: gestione errore 500 null
            for(CheckinOffloadFlightsRequest request : offloadRequestList) {
                request.setSid(sessionID);
                request.setTid(IDFactory.getTid());
                CheckinOffloadFlightsResponse response = checkInDelegateRest.offloadFlights(request);

                logger.debug("["+sessionID+"] OffloadCall request {} response {}", gson.toJson(request), gson.toJson(response));

                for(PnrFlightInfoR flightInfoR : response.getPnrFlightInfoRS()) {
                    for(FlightPassR flightPassR : flightInfoR.getFlightPassRS()) {
                        for(SeatListR seatListR : flightPassR.getSeatListRS()) {
                            if("OK".equals(seatListR.getEsito())) {
                                logger.info("["+sessionID+"] Offload eseguito per {} {}",
                                        seatListR.getFirstName() + " " + seatListR.getLastName(),
                                        "volo " + flightPassR.getAirline() +
                                                flightPassR.getFlight() +
                                                "da " + flightPassR.getOrigin() +
                                                "il " + flightPassR.getDepartureDate() +
                                                "pnr " +flightInfoR.getPnr());
                                status = status && true;
                            } else {
                                logger.info("["+sessionID+"] ERROR Offload per {} {}",
                                        seatListR.getFirstName() + " " + seatListR.getLastName(),
                                        "volo " + flightPassR.getAirline() +
                                                flightPassR.getFlight() +
                                                "da " + flightPassR.getOrigin() +
                                                "il " + flightPassR.getDepartureDate() +
                                                "pnr " +flightInfoR.getPnr());
                                status = status && false;
                            }
                        }
                    }
                }
            }

            // clear ancillaries
            logger.debug("[{}] Calling service rest clearAncillaySessionEnd",sessionID);
            if(clearAncillariesRequestList.size() > 0) {
                //TODO: gestione errore 500 null
                for (CheckinClearAncillarySessionEndRequest request : clearAncillariesRequestList) {
                    request.setSid(sessionID);
                    request.setTid(IDFactory.getTid());
                    CheckinClearAncillarySessionEndResponse checkinClearAncillarySessionEndResponse = checkInDelegateRest.clearAncillaySessionEnd(request);
                    if (checkinClearAncillarySessionEndResponse.getOutcome().get(0) != null) {
                        if (!checkinClearAncillarySessionEndResponse.getOutcome().get(0).equals("OK")) {
                            logger.warn("[{}] Si è verificato un errore nel clear ancillary e session end {}", sessionID, checkinClearAncillarySessionEndResponse.getOutcome().get(0));
                            status = status && false;
                        } else {
                            logger.info("[{}] Ancillay e sessione ripulite correttamente. Outcome: {}", sessionID, checkinClearAncillarySessionEndResponse.getOutcome().get(0));
                            status = status && true;
                        }
                    }
                }
            } else {
                logger.info("[{}] Clear Ancillaries nothing to do", sessionID);
            }

            if(deleteInsuranceRequestList.size() > 0) {
                for (DeleteInsuranceRequest request : deleteInsuranceRequestList) {
                    request.setSid(sessionID);
                    request.setTid(IDFactory.getTid());

                    DeleteInsuranceResponse deleteInsuranceResponse = insuranceDelegateRest.deleteInsurance(request);
                    if ("OK".equals(deleteInsuranceResponse.getSuccess())) {
                        logger.info("[{}] Clear insurance eseguito per {}", sessionID, request.getPnr());
                        status = status && true;
                    } else {
                        logger.error("[" + sessionID + "] ERRORE Clear insurance  per {}", gson.toJson(request), gson.toJson(deleteInsuranceResponse));
                        status = status && false;
                    }
                }
            } else {
                logger.info("[{}] Clear Insurance nothing to do", sessionID);
            }

            return status ? JobResult.OK : JobResult.CANCEL;
        } else {
            logger.warn("[{}] Offload nothing to do", sessionID);
            return JobResult.CANCEL;
        }
    }
}