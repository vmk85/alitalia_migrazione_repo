package com.alitalia.aem.consumer.carnet.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.StateData;
import com.alitalia.aem.common.data.home.carnet.CarnetPaymentTypeItemData;
import com.alitalia.aem.consumer.carnet.render.CarnetCreditCardRender;
import com.alitalia.aem.consumer.carnet.render.CarnetGenericPriceRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CarnetPayment extends GenericCarnetModel {

	@Self
	private SlingHttpServletRequest request;
	
	private String cartTotalAmount;
	private List<CarnetCreditCardRender> creditCards;
	private List<CountryData> countries;
	private List<StateData> countriesUSA;
	
	@PostConstruct
	protected void initModel() throws IOException {
		
		initBaseModel(request);
		
		this.countries = ctx.countries;
		this.countriesUSA = ctx.countriesUSA;
		
		CarnetGenericPriceRender priceRender = new CarnetGenericPriceRender(ctx);
		cartTotalAmount = priceRender.getFormattedPrice(ctx.carnetTotalAmount);
		
		this.creditCards = new ArrayList<CarnetCreditCardRender>();
		for (CarnetPaymentTypeItemData paymentTypeItemData : ctx.creditCards) {
			this.creditCards.add(new CarnetCreditCardRender(paymentTypeItemData));
		}
		
	}

	public String getCartTotalAmount() {
		return cartTotalAmount;
	}
	
	public List<CarnetCreditCardRender> getCreditCards() {
		return creditCards;
	}

	public List<CountryData> getCountries() {
		return countries;
	}

	public List<StateData> getCountriesUSA() {
		return countriesUSA;
	}
}
