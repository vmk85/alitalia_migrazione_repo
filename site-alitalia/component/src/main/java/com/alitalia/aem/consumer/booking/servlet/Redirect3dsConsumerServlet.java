package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "redirectPaymentConsumerServlet" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class Redirect3dsConsumerServlet extends SlingAllMethodsServlet {

	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaCustomerProfileManager customerProfileManager;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		
		logger.info("Executing redirectPaymentServlet");
		
		String baseUrl = request.getResource().getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
		
		String genericErrorPagePlain = baseUrl + configuration.getBookingFailurePage();
		String completePaymentPage = baseUrl + configuration.getBookingPaymentReturnPage();
		
		try {
			
			BookingSessionContext ctx = (BookingSessionContext) request.getSession(true).getAttribute(
					BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
			
			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				throw new IllegalStateException("Booking session context not found.");
			}
			
			String paRes = request.getParameter("PaRes");
			logger.debug("Parameters received from 3ds page: PaRes["+paRes+"]");
			ctx.paymentData.getProcess().setPaRes(paRes);
			
			response.sendRedirect(completePaymentPage);
		
		} catch(Exception e) {
			logger.error("Unexpected error: ", e);
			response.sendRedirect(genericErrorPagePlain);
		}
	}
}
