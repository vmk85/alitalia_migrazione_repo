package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;
import java.math.BigDecimal;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbThankyou extends MmbSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private Boolean ancillaryPaymentSuccess;
	private Boolean insurancePaymentSuccess;
	private String paymentErrorMessage;
	private Boolean insuranceAddedToCart;
	
	private boolean freeAncillaryOnly = false;
	private String confirmationMailAddress = null;
	private String tripId;
	private String passengerName;
	private String passengerSurname;
	
	private String specialAssistanceRedirect;
	
	
	@PostConstruct
	protected void initModel() throws IOException {
		
		initBaseModel(request);
		
		this.freeAncillaryOnly = ctx.cartTotalAmount.compareTo(new BigDecimal(0)) == 0;
		this.confirmationMailAddress = ctx.confirmationMailAddress;
		
		this.ancillaryPaymentSuccess = ctx.ancillaryPaymentSuccess || freeAncillaryOnly;
		this.insurancePaymentSuccess = ctx.insurancePaymentSuccess || freeAncillaryOnly;
		this.paymentErrorMessage = ctx.paymentErrorMessage;
		this.insuranceAddedToCart = ctx.insuranceAddedToCart || freeAncillaryOnly;
		
		this.tripId = ctx.tripId;
		this.passengerName = ctx.passengerName;
		this.passengerSurname = ctx.passengerSurname;
		
		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);

		this.specialAssistanceRedirect = baseUrl + configuration.getSpecialAssistancePage();
	}

	public Boolean getAncillaryPaymentSuccess() {
		return ancillaryPaymentSuccess;
	}

	public Boolean getInsurancePaymentSuccess() {
		return insurancePaymentSuccess;
	}

	public String getPaymentErrorMessage() {
		return paymentErrorMessage;
	}
	
	public Boolean getInsuranceAddedToCart() {
		return insuranceAddedToCart;
	}

	public String getTripId() {
		return tripId;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public String getPassengerSurname() {
		return passengerSurname;
	}

	public boolean isFreeAncillaryOnly() {
		return freeAncillaryOnly;
	}

	public String getConfirmationMailAddress() {
		return confirmationMailAddress;
	}
	
	public String getSpecialAssistanceRedirect() {
		return specialAssistanceRedirect;
	}
	
}
