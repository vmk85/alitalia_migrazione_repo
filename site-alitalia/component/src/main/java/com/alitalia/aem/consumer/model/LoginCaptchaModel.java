package com.alitalia.aem.consumer.model;
import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class LoginCaptchaModel extends GenericBaseModel{
	
		@Self
		private SlingHttpServletRequest request;
		
	    private boolean captchaEnabled;
	    private boolean widgetCheckin;
	    private boolean widgetMmb;
	    private boolean header;
	    private boolean loginSpacialPages;
	    private boolean loginSocial;
	    private boolean cashAndMiles;
	    private boolean script;
	    private String language;
	    
		@PostConstruct
		protected void initModel() {
			
			super.initBaseModel(request);
			try {
				widgetCheckin = false;
				widgetMmb = false;
				header = false;
				loginSpacialPages = false;
				loginSocial = false;
				cashAndMiles = false;
				captchaEnabled = true;
				script = false;
				language = AlitaliaUtils.getRepositoryPathLanguage(request.getResource());
				
				String[] selectors = request.getRequestPathInfo().getSelectors();
				
				if (selectors.length > 1) {
					if ("captcha_checkin".equals(selectors[1])) {
						widgetCheckin = true;
					}
					
					if ("captcha_mmb".equals(selectors[1])) {
						widgetMmb = true;
					}
					
					if ("captcha_header".equals(selectors[1])) {
						header = true;
					}
					
					if ("captcha_login_spacialpages".equals(selectors[1])) {
						loginSpacialPages = true;
					}
		
					if ("captcha_cash_miles".equals(selectors[1])) {
						cashAndMiles = true;
					}
					if ("script".equals(selectors[1])) {
						script = true;
					}
				}
			} catch (Exception e) {
				logger.error("Unexpected error", e);
			}
			
		}
		
		public boolean isCaptchaEnabled(){
			return captchaEnabled;
		}
		
		public boolean isWidgetCheckin(){
			return widgetCheckin;
		}
		
		public boolean isWidgetMmb(){
			return widgetMmb;
		}
		
		public boolean isHeader(){
			return header;
		}
		
		public boolean isLoginSpacialPages(){
			return loginSpacialPages;
		}
		
		public boolean isLoginSocial(){
			return loginSocial;
		}
		
		public boolean isCashAndMiles(){
			return cashAndMiles;
		}
		
		public boolean isScript(){
			return script;
		}
		
		public String getLanguage(){
			return language;
		}

}
