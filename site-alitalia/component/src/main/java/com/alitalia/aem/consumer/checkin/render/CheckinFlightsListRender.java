package com.alitalia.aem.consumer.checkin.render;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinStatus;
import com.alitalia.aem.consumer.checkin.CheckinUtils;

/**
 * CheckinFlightsListRender
 */
public class CheckinFlightsListRender {

	private List<CheckinFlightRender> flights;
	private CheckinStatus checkinStatus;
	private String manageSeatUrl;
	private String manageBoardingPassUrl;
	private String manageUndoUrl;
	private boolean checkinEnabled;
	
	public CheckinFlightsListRender(CheckinRouteData routeData,
			int flight_number, CheckinSessionContext ctx) {
		
		super();
		flights = new ArrayList<CheckinFlightRender>();
		
		List<CheckinFlightData> checkinFlights =
				CheckinUtils.getFlightsList(routeData);
		
		if (checkinFlights != null) {
			for (CheckinFlightData flight : checkinFlights) {
				flights.add(new CheckinFlightRender(flight, ctx));
			}
			checkinEnabled = CheckinUtils.checkValidCheckin(
					routeData, flight_number, "") != null;
		}
	}
	
	/**
	 * CheckinFlightsListRender
	 */
	public CheckinFlightsListRender(CheckinRouteData routeData,
			int flight_number, String passengersUrl, String manageSeatUrl,
			String manageBoardingPassUrl, String manageUndoUrl,
			CheckinSessionContext ctx) {
		
		this(routeData, flight_number, ctx);

		// set check-in status
		this.checkinStatus = CheckinUtils.verifyCheckinStatus(routeData,
				flight_number, passengersUrl, ctx);
		
		// set manage check-in URLs
		this.manageSeatUrl = manageSeatUrl + "?" +
				CheckinConstants.CHECKIN_SELECTED_ROUTE_PARAM
				+ "=" + flight_number;
		this.manageBoardingPassUrl = manageBoardingPassUrl + "?" +
				CheckinConstants.CHECKIN_SELECTED_ROUTE_PARAM
				+ "=" + flight_number;
		this.manageUndoUrl = Boolean.TRUE.equals(routeData.getUndoEnabled()) ?
				(manageUndoUrl + "?" +
				CheckinConstants.CHECKIN_SELECTED_ROUTE_PARAM
				+ "=" + flight_number) : "";

	}
	
	public List<CheckinFlightRender> getFlights() {
		return flights;
	}

	public CheckinStatus getCheckinStatus() {
		return checkinStatus;
	}

	public boolean isCheckinEnabled() {
		return checkinEnabled;
	}

	public String getManageSeatUrl() {
		return manageSeatUrl;
	}

	public String getManageBoardingPassUrl() {
		return manageBoardingPassUrl;
	}

	public String getManageUndoUrl() {
		return manageUndoUrl;
	}
	
}