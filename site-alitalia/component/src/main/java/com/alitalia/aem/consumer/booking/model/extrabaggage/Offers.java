package com.alitalia.aem.consumer.booking.model.extrabaggage;

import java.util.*;

public class Offers
{
	public Offers()
	{

	}

	private DepartureAirport DepartureAirport;
	public final DepartureAirport getDepartureAirport()
	{
		return DepartureAirport;
	}
	public final void setDepartureAirport(DepartureAirport value)
	{
		DepartureAirport = value;
	}
	private ArrivalAirport ArrivalAirport;
	public final ArrivalAirport getArrivalAirport()
	{
		return ArrivalAirport;
	}
	public final void setArrivalAirport(ArrivalAirport value)
	{
		ArrivalAirport = value;
	}
	private int BestPrice;
	public final int getBestPrice()
	{
		return BestPrice;
	}
	public final void setBestPrice(int value)
	{
		BestPrice = value;
	}
	private java.time.LocalDateTime ExpireOfferDate = java.time.LocalDateTime.MIN;
	public final java.time.LocalDateTime getExpireOfferDate()
	{
		return ExpireOfferDate;
	}
	public final void setExpireOfferDate(java.time.LocalDateTime value)
	{
		ExpireOfferDate = value;
	}
	private String Area;
	public final String getArea()
	{
		return Area;
	}
	public final void setArea(String value)
	{
		Area = value;
	}
	private ArrayList<BestPrice> BestPrices;
	public final ArrayList<BestPrice> getBestPrices()
	{
		return BestPrices;
	}
	public final void setBestPrices(ArrayList<BestPrice> value)
	{
		BestPrices = value;
	}
	private String Type;
	public final String getType()
	{
		return Type;
	}
	public final void setType(String value)
	{
		Type = value;
	}
	private String CabinType;
	public final String getCabinType()
	{
		return CabinType;
	}
	public final void setCabinType(String value)
	{
		CabinType = value;
	}
	private boolean IsHomePageOffer;
	public final boolean getIsHomePageOffer()
	{
		return IsHomePageOffer;
	}
	public final void setIsHomePageOffer(boolean value)
	{
		IsHomePageOffer = value;
	}
	private int NumberOfNights;
	public final int getNumberOfNights()
	{
		return NumberOfNights;
	}
	public final void setNumberOfNights(int value)
	{
		NumberOfNights = value;
	}
	private int ODpriority;
	public final int getODpriority()
	{
		return ODpriority;
	}
	public final void setODpriority(int value)
	{
		ODpriority = value;
	}
}