package com.alitalia.aem.consumer.checkin;

public enum CheckinAncillarySeatStatus {
	
	COMFORT_SEAT_AVAILABLE,
	SEAT_NOT_AVAILABLE,
	ONLY_STANDARD_SEAT_AVAILABLE
}
