package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.data.home.PaymentComunicationBancaIntesaData;
import com.alitalia.aem.common.data.home.PaymentData;
import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.StateData;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GlobalCollectPaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.InitializePaymentErrorEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingPaymentException;
import com.alitalia.aem.consumer.booking.BookingPhaseEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.InvoiceType;
import com.alitalia.aem.consumer.booking.model.PaymentType;
import com.alitalia.aem.consumer.carnet.CarnetSession;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.ValidationUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingpurchasedataconsumer" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })})
@SuppressWarnings("serial")
public class BookingPurchaseDataServlet extends GenericBookingFormValidatorServlet {

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private BookingSession bookingSession;

	@Reference
	private CarnetSession carnetSession;

	private final String FRAUDNET_IP_HEADER = "X-Forwarded-For";

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {

		try {

			BookingSessionContext ctx = (BookingSessionContext)
					request.getSession(true).getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);

			Validator validator = new Validator();

			// type of payment
			String tipoPagamento = request.getParameter("tipoPagamento");
			String[] countries = null;
			String[] countriesUSAValues = null;

			//prepare countries list for validation
			if (ctx.countries != null) {
				countries = new String[ctx.countries.size()];
				int i = 0;
				for(CountryData country : ctx.countries) {
					countries[i] = country.getCode();
					i++;
				}
			}

			//prepare countries list for validation
			if (ctx.countriesUSA != null) {
				countriesUSAValues = new String[ctx.countriesUSA.size()];
				int i = 0;
				for(StateData country : ctx.countriesUSA) {
					countriesUSAValues[i] = country.getStateCode();
					i++;
				}
			}

			// values
			String nomeBanca = request.getParameter("nomeBanca");

			logger.info("Executing Validation. tipoPagamento["+tipoPagamento+"], nomeBanca["+nomeBanca+"]");

			boolean captchaAlreadyValidated = (Boolean)(request.getSession(true).getAttribute("captchaAlreadyValidated") != null ? request.getSession(true).getAttribute("captchaAlreadyValidated") : Boolean.FALSE);

			logger.debug("captchaAlreadyValidated: " + captchaAlreadyValidated);

			// Captcha validation
			if (!captchaAlreadyValidated && nomeBanca!=null && (PaymentTypeEnum.LOTTOMATICA.value().equalsIgnoreCase(nomeBanca) || PaymentTypeEnum.FINDOMESTIC.value().equalsIgnoreCase(nomeBanca) || PaymentTypeEnum.PAY_AT_TO.value().equalsIgnoreCase(nomeBanca))){
				String jcaptchaResponse = request.getParameter("captcha");
				String captchaId = request.getSession().getId();
				Boolean isResponseCorrect = ValidationUtils.isCaptchaValid(jcaptchaResponse, captchaId);

				if (!isResponseCorrect){
					ResultValidation resultValidation = new ResultValidation();
					resultValidation.setResult(isResponseCorrect);
					resultValidation.addField("captcha", "Valore non valido");
					return resultValidation;
				} else {
					request.getSession(true).setAttribute("captchaAlreadyValidated", true);
					logger.debug("Captcha check passed");
				}
			}

			// One Click
			if (PaymentType.OneClick.toString().equals(tipoPagamento)) {
				MMCreditCardData cartaOneClick = ctx.loggedUserProfileData.getStoredCreditCard();

				String tipologiaCarta = cartaOneClick.getCircuitCode();
				String cvc = request.getParameter("cvc");
				if (!tipologiaCarta.equalsIgnoreCase(CreditCardTypeEnum.UATP.value())){
					validator.addDirectCondition("cvc", cvc, BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
					validator.addDirectCondition("cvc", cvc, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNumber");
					if (tipologiaCarta.equalsIgnoreCase(CreditCardTypeEnum.AMERICAN_EXPRESS.value())) {
						validator.addDirectCondition("cvc", cvc, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isCVCFourDigits");
					} else {
						validator.addDirectCondition("cvc", cvc, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isCVCThreeDigits");
					}
				}
			}
			// Zero Payment
			else if (PaymentType.ZeroPayment.toString().equals(tipoPagamento)) {

				String nome = request.getParameter("nome");
				String cognome = request.getParameter("cognome");

				validator.addDirectCondition("nome", nome,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.addDirectCondition("cognome", cognome,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD,
						"isNotEmpty");

			}
			// credit card
			else if (PaymentType.CDC.toString().equals(tipoPagamento)) {

				/* get Values */
				String tipologiaCarta = request.getParameter("tipologiaCarta");
				String numeroCarta = request.getParameter("numeroCarta");
				String cvc = request.getParameter("cvc");
				String meseScadenza = request.getParameter("meseScadenza");
				String annoScadenza = request.getParameter("annoScadenza");

				String nome = request.getParameter("nome");
				String cognome = request.getParameter("cognome");
				String indirizzo = request.getParameter("indirizzo");
				String cap = request.getParameter("cap");
				String citta = request.getParameter("citta");
				String paese = request.getParameter("paese");
				String countriesUSA = request.getParameter("countriesUSA");
				String isNumberValid = request.getParameter("isNumberValid");

				// validate
				validator.setAllowedValues("tipologiaCarta", tipologiaCarta,
						populateCreditCards().toArray(), BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD);
				validator.addDirectCondition("numeroCarta", numeroCarta,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

				validator.addDirectCondition("numeroCarta", numeroCarta,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNumber");

				validator.addDirectCondition("isNumberValid", isNumberValid,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNumber");

				if (!tipologiaCarta.equalsIgnoreCase(CreditCardTypeEnum.UATP.value())){
					validator.addDirectCondition("cvc", cvc,
							BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
					validator.addDirectCondition("cvc", cvc,
							BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNumber");

					if (tipologiaCarta.equalsIgnoreCase(CreditCardTypeEnum.AMERICAN_EXPRESS.value())) {
						validator.addDirectCondition("cvc", cvc,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isCVCFourDigits");
					} else {
						validator.addDirectCondition("cvc", cvc,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isCVCThreeDigits");
					}
				}

				validator.addDirectCondition("meseScadenza", meseScadenza,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

				String[] valoriMesi = new String[] { "01", "02", "03", "04", "05", "06", "07",
						"08", "09", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
				validator.setAllowedValues("meseScadenza", meseScadenza, valoriMesi,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD);
				validator.addDirectCondition("annoScadenza", annoScadenza,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.addDirectCondition("annoScadenza", annoScadenza,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isYear");

				if (meseScadenza != null && annoScadenza != null && !meseScadenza.isEmpty() &&
						!annoScadenza.isEmpty()) {
					try {
						int meseScad = Integer.parseInt(meseScadenza);
						int annoScad = Integer.parseInt(annoScadenza);
						if (meseScad == 12) {
							meseScad = 1;
							annoScad++;
						} else {
							meseScad++;
						}
						validator.addDirectCondition(
								"annoScadenza", "01/" + meseScad + "/" + annoScad,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isFuture");
					} catch (Exception e) {
						logger.warn("Errore valori meseScadenza, annoScadenza", e);
					}
				}

				validator.addDirectCondition("nome", nome,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.addDirectCondition("cognome", cognome,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD,
						"isNotEmpty");

				// check extra fields for American Express
				if (!tipologiaCarta.equals("") &&
						CreditCardTypeEnum.fromValue(tipologiaCarta) != null &&
						CreditCardTypeEnum.AMERICAN_EXPRESS.equals(
								CreditCardTypeEnum.fromValue(tipologiaCarta))) {

					validator.addDirectCondition("indirizzo", indirizzo,
							BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
					validator.addDirectCondition("cap", cap,
							BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
					validator.addDirectCondition("cap", cap,
							BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAlphaNumeric");
					validator.addDirectCondition("citta", citta,
							BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
					validator.addDirectCondition("paese", paese,
							BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
					validator.setAllowedValues("paese", paese,
							countries, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD);
					//Controllo campi extra di AMEX solo se il paese è US
					if (paese.equalsIgnoreCase("us")) {
						validator.addDirectCondition("countriesUSA", countriesUSA,
								BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
						validator.setAllowedValues("countriesUSA", countriesUSA,
								countriesUSAValues, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD);
					}
				}
			}
			else if (PaymentType.InstalmentsBrazil.toString().equals(tipoPagamento)) {

				// validate!
				String tipologiaCarta = request.getParameter("tipologiaCarta");
				String rate = request.getParameter("rate");
				logger.debug("tipologiaCarta["+tipologiaCarta+"] rate["+rate+"]");
				validator.setAllowedValues("tipologiaCarta", tipologiaCarta,
						populateBrCreditCards().toArray(), BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD);
				if ("br".equals(ctx.market)){
					validator.addDirectCondition("rate", rate,
							BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				}

			}
			// bank transfer
			else if (PaymentType.BonificoOnLine.toString().equals(tipoPagamento)) {

				// validate!
				validator.setAllowedValues("nomeBanca", nomeBanca,
						populateBanks().toArray(), BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD);
				//otherPayments
			} else {

				// validate!
				validator.setAllowedValues("nomeBanca", nomeBanca,
						populateOtherPayments().toArray(), BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD);

			}

			// invoice ?
			if ("it".equals(ctx.market) && request.getParameter("richiediFattura") != null &&
					!request.getParameter("richiediFattura").isEmpty()) {

				// values
				String tipoIntestatario = request.getParameter("tipoIntestatario");
				String nomeFattura = request.getParameter("nomeFattura");
				String cognomeFattura = request.getParameter("cognomeFattura");
				String codiceFiscaleFattura = request.getParameter("codiceFiscaleFattura");
				String intestatarioFattura = request.getParameter("intestatarioFattura");
				String indirizzoFattura = request.getParameter("indirizzoFattura");
				String capFattura = request.getParameter("capFattura");
				String cittaFattura = request.getParameter("cittaFattura");
				String provFattura = request.getParameter("provFattura");
				String paeseFattura = request.getParameter("paeseFattura");
				String emailFattura = request.getParameter("emailFattura");

				// validate!
				String  minSizeInvoiceName = "2";
				validator.setAllowedValues("tipoIntestatario", tipoIntestatario,
						InvoiceType.values(), BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD);

				validator.addDirectCondition("nomeFattura", nomeFattura,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.addDirectCondition("nomeFattura", nomeFattura,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAlphabeticWithAccentAndSpaces");
				validator.addCrossCondition("nomeFattura", nomeFattura, minSizeInvoiceName, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "sizeIsBiggerOrEquals");

				validator.addDirectCondition("cognomeFattura", cognomeFattura,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.addDirectCondition("cognomeFattura", cognomeFattura,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAlphabeticWithAccentAndSpaces");
				validator.addCrossCondition("cognomeFattura", cognomeFattura, minSizeInvoiceName, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "sizeIsBiggerOrEquals");

				validator.addDirectCondition("codiceFiscaleFattura",
						codiceFiscaleFattura, BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

				if (codiceFiscaleFattura.matches("^([0-9])*$")) {
					validator.addDirectCondition("codiceFiscaleFattura",
							codiceFiscaleFattura, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isPartitaIVA");
				} else {
					String validatorCF = nomeFattura + "|" + cognomeFattura + "|" +  codiceFiscaleFattura;
					validator.addDirectCondition("codiceFiscaleFattura",
							validatorCF, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isValidCodiceFiscaleByConcatNomeCognomeCF");
				}

				if (InvoiceType.fromValue(tipoIntestatario) != InvoiceType.PF) {
					validator.addDirectCondition("intestatarioFattura",
							intestatarioFattura, BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
					validator.addDirectCondition("intestatarioFattura",
							intestatarioFattura, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAlphanumericWithAccentAndSpacesExtended");

				}

				validator.addDirectCondition("indirizzoFattura", indirizzoFattura,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

				validator.addDirectCondition("capFattura", capFattura,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.addDirectCondition("capFattura", capFattura,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAlphaNumeric");

				validator.addDirectCondition("cittaFattura", cittaFattura,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.addDirectCondition("cittaFattura", cittaFattura,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAlphabeticWithAccentAndSpaces");

				validator.addDirectCondition("emailFattura", emailFattura,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.addDirectCondition("emailFattura", emailFattura,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isEmail");

				validator.setAllowedValues("paeseFattura", paeseFattura,
						countries, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD);
				if (paeseFattura.equals("IT")) {
					validator.addDirectCondition("provFattura", provFattura,
							BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				}
			}

			// validate!
			ResultValidation resultValidation = validator.validate();
			return resultValidation;

		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Error during validation form payment", e);
			throw e;
		}
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
								 SlingHttpServletResponse response) throws IOException {

		logger.info("BookingPurchaseDataServlet. performSubmit executing");

		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		String typeOfPayment = "";
		String nomeBanca = "";
		String baseUrlForContext = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
		String baseUrlForRedirect = request.getResource().getResourceResolver().map(baseUrlForContext);
		try {
			Map<String,String> httpHeaders = getHttpHeaders(request, ";");

			String ipAddress = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
			if (ipAddress != null && !ipAddress.isEmpty()) {
				httpHeaders.put(FRAUDNET_IP_HEADER, ipAddress);
			}

			String jscString = request.getParameter("beirutString");
			String csrfToken = request.getHeader("CSRF-Token");

			request.getSession(true).removeAttribute("captchaAlreadyValidated");

			BookingSessionContext ctx = (BookingSessionContext)
					request.getSession(true).getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				json.object();
				json.key("result").value(false);
				json.key("redirect").value(baseUrlForRedirect + configuration.getBookingFailurePage());
				json.endObject();
				return;
			}

			if(ctx.isCarnetProcess && ctx.isCarnetValid){
				int numPax = ctx.searchPassengersNumber.getNumAdults() + ctx.searchPassengersNumber.getNumChildren() + ctx.searchPassengersNumber.getNumInfants() + ctx.searchPassengersNumber.getNumYoung();
				boolean isValid = carnetSession.checkCarnetValidity(ctx, ctx.infoCarnet, numPax);
				// open JSON response
				if(!isValid){
					json.object();
					json.key("result").value(false);
					manageGenericError(ctx, json, baseUrlForRedirect);
					json.endObject();
					return;
				}
			}

			ctx.baseUrl = baseUrlForContext;

			ResourceBundle resourceBundle = request.getResourceBundle(ctx.locale);
			final I18n i18n = new I18n(resourceBundle);

			// invoice ?
			String billingName = null, billingSurname = null, billingFormaSocietaria = null,
					billingCfPax = null, billingIndSped = null, billingCAP = null,
					billingLocSped = null, billingPaese = null, billingEmailInt = null,
					billingProv = null, billingIntFattura = null;
			ctx.invoiceRequired = ("it".equals(ctx.market) && request.getParameter("richiediFattura") != null &&
					!request.getParameter("richiediFattura").isEmpty());
			if (ctx.invoiceRequired) {
				billingName = request.getParameter("nomeFattura");
				billingSurname = request.getParameter("cognomeFattura");
				billingCfPax = request.getParameter("codiceFiscaleFattura");
				billingIndSped = request.getParameter("indirizzoFattura");
				billingCAP = request.getParameter("capFattura");
				billingLocSped = request.getParameter("cittaFattura");
				billingPaese = request.getParameter("paeseFattura");
				billingProv = request.getParameter("provFattura");
				billingEmailInt = request.getParameter("emailFattura");
				billingFormaSocietaria = request.getParameter("tipoIntestatario");
				billingIntFattura = request.getParameter("intestatarioFattura");
			}

			// type of payment
			typeOfPayment = request.getParameter("tipoPagamento");
			nomeBanca = request.getParameter("nomeBanca");


			String protocol = request.getScheme();
			String serverName = request.getServerName();
//			int port = request.getServerPort();

//			if (port != -1) {
//				ctx.domain = protocol + "://" + serverName + ":" + port;
//			} else {
				ctx.domain = protocol + "://" + serverName;
//			}
			bookingSession.applyCashAndMiles(ctx);
			// credit card
			if (PaymentType.CDC.toString().equals(typeOfPayment)) {
				String creditCardType = request.getParameter("tipologiaCarta");
				logger.debug("creditCardType: ["+creditCardType+"]");
				String creditCardNumber = request.getParameter("numeroCarta");
				String creditCardCVV = request.getParameter("cvc");
				String creditCardName = request.getParameter("nome");
				String creditCardLastName = request.getParameter("cognome");
				Short creditCardExpiryMonth = Short.parseShort(request.getParameter("meseScadenza"));
				Short creditCardExpiryYear = Short.parseShort(request.getParameter("annoScadenza"));
				boolean memorizzaCarta = (request.getParameter("memorizzaCarta")!= null && !request.getParameter("memorizzaCarta").equals("")) ? true : false;
				String zip = null, city = null, country = null, address = null, state = null;
				if (!creditCardType.equals("") &&
						CreditCardTypeEnum.fromValue(creditCardType) != null &&
						CreditCardTypeEnum.AMERICAN_EXPRESS.equals(
								CreditCardTypeEnum.fromValue(creditCardType))) {
					zip = request.getParameter("cap");
					city = request.getParameter("citta");
					country = request.getParameter("paese");
					address = request.getParameter("indirizzo");

					//Recupera la provincia solo se il paese è USA
					String countriesUSA = request.getParameter("countriesUSA");
					if (country.equalsIgnoreCase("us")) {
						state = countriesUSA;
					}
				}


				String errorUrl = ctx.domain + baseUrlForRedirect + "/booking/.redirectPaymentServlet"; //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment"
				if (ctx.award) {
					errorUrl = ctx.domain + baseUrlForRedirect + configuration.getBookingAwardPaymentFailurePage(); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment"
				}
				String returnUrl = ctx.domain + baseUrlForRedirect + "/booking/.redirectPaymentServlet";//configuration.getBookingPaymentReturnPage(); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment"
				if(!"undefined".equals(csrfToken)){
					errorUrl += (errorUrl.contains("?") ? "&" : "?") + ":cq_csrf_token=" + csrfToken;
					returnUrl += "?:cq_csrf_token=" + csrfToken;
				}
				bookingSession.preparePaymentWithCreditCard(ctx, creditCardNumber,
						creditCardCVV, creditCardExpiryMonth, creditCardExpiryYear,
						creditCardType, creditCardName, creditCardLastName, memorizzaCarta, billingCAP,
						billingCfPax, billingEmailInt, billingFormaSocietaria, billingIndSped,
						billingLocSped, billingPaese, billingProv, billingName, billingSurname,
						billingIntFattura, zip, city, country, address, state,
						AlitaliaUtils.getRequestRemoteAddr(configuration, request), request.getHeader("User-Agent"),
						errorUrl, returnUrl, i18n);
			}
			// One click
			else if (PaymentType.OneClick.toString().equals(typeOfPayment)) {
				String creditCardCVV = request.getParameter("cvc");

				String errorUrl = ctx.domain + baseUrlForRedirect + configuration.getBookingPaymentFailurePage(); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment"
				if (ctx.award) {
					errorUrl = ctx.domain + baseUrlForRedirect + configuration.getBookingAwardPaymentFailurePage(); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment"
				}
				String returnUrl = ctx.domain + baseUrlForRedirect + configuration.getBookingPaymentReturnPage(); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment"

				bookingSession.preparePaymentWithOneClick(ctx, creditCardCVV,
						null, null, null, null, null, null, null, null, null,
						null, null, null, null, null, null, null, null, null,
						AlitaliaUtils.getRequestRemoteAddr(configuration, request), request.getHeader("User-Agent"),
						errorUrl, returnUrl, i18n);

			}
			// Zero Payment
			else if (PaymentType.ZeroPayment.toString().equals(typeOfPayment)) {

				String errorUrl = ctx.domain + baseUrlForRedirect + configuration.getBookingPaymentFailurePage(); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment"
				if (ctx.award) {
					errorUrl = ctx.domain + baseUrlForRedirect + configuration.getBookingAwardPaymentFailurePage(); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment"
				}
				String returnUrl = ctx.domain + baseUrlForRedirect + configuration.getBookingPaymentReturnPage(); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment"

				bookingSession.prepareZeroPayment(ctx, null,
						null, null, null, null, null, null, null, null, null,
						null, null, null, null, null, null, null, null, null,
						AlitaliaUtils.getRequestRemoteAddr(configuration, request), request.getHeader("User-Agent"),
						errorUrl, returnUrl, i18n);

			}
			// bank transfer && Instalments Brasil
			else if (PaymentType.BonificoOnLine.toString().equals(typeOfPayment) || PaymentType.InstalmentsBrazil.toString().equals(typeOfPayment)) {

				String errorUrl = ctx.domain + baseUrlForRedirect + configuration.getBookingPaymentPage() + "?error=true"; //booking.uat1.az/Booking/IT_IT/Checkout/RedirectErrorPayment"
				String returnUrl = ctx.domain + baseUrlForRedirect + configuration.getBookingPaymentReturnPage(); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment";
				String cancelUrl = ctx.domain + baseUrlForRedirect + configuration.getBookingPaymentPage(); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectCancelPayment";
				String globalCollectUrl = ctx.domain + baseUrlForRedirect + configuration.getBookingPaymentGlobalCollectReturnPage(); //"https://booking.uat1.az/Booking/IT_IT/GlobalCollect/Return";

				if (PaymentTypeEnum.fromValue(nomeBanca) == PaymentTypeEnum.PAY_PAL || PaymentTypeEnum.fromValue(nomeBanca) == PaymentTypeEnum.MAESTRO){
					returnUrl = ctx.domain + baseUrlForRedirect + "/booking/.redirectPaymentServlet";
					errorUrl = ctx.domain + baseUrlForRedirect + "/booking/.redirectPaymentServlet";
					cancelUrl = ctx.domain + baseUrlForRedirect + "/booking/.redirectPaymentServlet";
				}
				if(!"undefined".equals(csrfToken)){
					returnUrl += "?:cq_csrf_token=" + csrfToken;
					errorUrl += (errorUrl.contains("?") ? "&" : "?") + ":cq_csrf_token=" + csrfToken;
					cancelUrl += "?:cq_csrf_token=" + csrfToken;
				}

				if (PaymentType.InstalmentsBrazil.toString().equals(typeOfPayment))
					nomeBanca = PaymentType.InstalmentsBrazil.toString();

				String cardType = request.getParameter("tipologiaCarta");
				String numRate = request.getParameter("rate") != null ? request.getParameter("rate") : "0";
				logger.info("BookingPurchaseDataServlet. bank transfer payment performing. returnUrl["+returnUrl+"], errorUrl["+errorUrl+"]");
				bookingSession.preparePaymentWithBankTransfer(ctx, billingCAP,
						billingCfPax, billingEmailInt, billingFormaSocietaria, billingIndSped,
						billingLocSped, billingPaese, billingProv, billingName, billingSurname,
						billingIntFattura, nomeBanca, AlitaliaUtils.getRequestRemoteAddr(configuration, request),
						errorUrl, returnUrl, cancelUrl, globalCollectUrl,i18n, cardType, numRate);
				//PagaDopo
			} else if (PaymentType.PagaDopo.toString().equals(typeOfPayment)) {

				String returnUrl = ctx.domain + baseUrlForRedirect + configuration.getBookingPaymentReturnPage(); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment";
				List<String> emailForMail = null;
				if (PaymentTypeEnum.fromValue(nomeBanca) == PaymentTypeEnum.PAY_AT_TO){
					emailForMail = configuration.getBookingPaymentPTOMailingList(ctx.market);
				}

				logger.info("BookingPurchaseDataServlet preparePaymentWithPayLater performing. returnUrl: ["+returnUrl+"]");
				bookingSession.preparePaymentWithPayLater(ctx, billingCAP,
						billingCfPax, billingEmailInt, billingFormaSocietaria, billingIndSped,
						billingLocSped, billingPaese, billingProv, billingName, billingSurname,
						billingIntFattura, nomeBanca, AlitaliaUtils.getRequestRemoteAddr(configuration, request),
						returnUrl, i18n, emailForMail);

			}

			// perform payment
			try {
				bookingSession.performPayment(ctx, AlitaliaUtils.getRequestRemoteAddr(configuration, request),
						request.getHeader("User-Agent"), IDFactory.getTid(), typeOfPayment, nomeBanca, i18n, httpHeaders, jscString);
			} catch (BookingPaymentException e) {
				logger.error("Error during payment step: ", e);

				// open JSON response
				json.object();
				json.key("result").value(false);

				// evaluate specific error
				switch (e.getCode()) {
					case BookingPaymentException.BOOKING_ERROR_INITIALIZEPAYMENT :
						manageErrorInitializePayment(json, i18n, e.getInitializePaymentEnum());
						break;
					case BookingPaymentException.BOOKING_ERROR_AUTHORIZEPAYMENT :
						manageErrorAuthorizePayment(json, i18n);
						break;
					case BookingPaymentException.BOOKING_ERROR_CHECKPAYMENT :
						manageErrorCheckPayment(json, ctx, baseUrlForRedirect);
						break;
					case BookingPaymentException.BOOKING_ERROR_RETRIEVETICKETS :
						manageErrorRetrieveTickets(json, ctx, baseUrlForRedirect);
						break;
					default:
						manageGenericError(ctx, json, baseUrlForRedirect);
						break;
				}

				// close JSON response
				json.endObject();
				return;
			}

			// open JSON response
			json.object();
			json.key("result").value(true);

			// payment to be completed later?
			if (ctx.paymentData != null) {
				PaymentData paymentData = ctx.paymentData;
				PaymentProcessInfoData process = paymentData.getProcess();
				json.key("complete");
				json.object();
				json.key("redirect").value(process.getRedirectUrl());
				String method = computeSubmitMethod(nomeBanca);
				json.key("method").value(PaymentType.CDC.toString().equals(typeOfPayment) && process.getRedirectUrl()!=null ? "GET" : method);
				if (paymentData.getProvider() instanceof PaymentProviderCreditCardData) {
					json.key("params");
					json.object();
					if(process.getTokenId() != null && process.getTokenId().length() > 0){
						String[] parameters = process.getTokenId().split("&");
						for (String p : parameters){
							String name = p.substring(0, p.indexOf("="));
							String value = p.substring(1+p.indexOf("="));
							if (name.equals("PaRequest")){
								json.key("PaReq").value(value);
							} else {
								json.key(name).value(value);
							}
						}
					}
					json.key("TermUrl").value(paymentData.getProvider().getComunication().getReturnUrl());
					json.key("CartId").value(process.getTransactionId());
					json.endObject();
				}
				if (paymentData.getType() == PaymentTypeEnum.BANCA_INTESA) {
					PaymentComunicationBancaIntesaData comunicationBancaIntesa = (PaymentComunicationBancaIntesaData) paymentData.getProvider().getComunication();
					Map<String, String> requestData = comunicationBancaIntesa.getRequestData();
					if (requestData != null && !requestData.isEmpty()) {
						json.key("params");
						json.object();
						for (String key : requestData.keySet()) {
							String value = requestData.get(key);
							if (!key.equals("action")) {
								json.key(key).value(value);
							}
						}
						json.endObject();
					}
				}
				if (paymentData.getType() == PaymentTypeEnum.FINDOMESTIC) {
					if (process.getTokenId() != null) {
						logger.debug("Findomestic Token id: ["+process.getTokenId()+"]");
						json.key("params");
						json.object();
						String[] parameters = process.getTokenId().split("&");
						if (parameters!=null){
							for(String p : parameters){
								logger.debug("Findomestic Parametro i-esimo: ["+p+"]");
								String chiave = p.substring(0, p.indexOf("="));
								String valore = p.substring(p.indexOf("=")+1);
								//if (chiave!=null && (chiave.equals("urlRedirect") || chiave.equals("callBackUrl")))
								//	valore = URLDecoder.decode(valore, "UTF-8");
								json.key(chiave).value(valore);
							}
						}
						json.endObject();
					}
				}
				json.endObject();
			}
			// all done!
			else {
				json.key("redirect").value(baseUrlForRedirect + configuration.getBookingConfirmationPage());
			}

			// close JSON response
			json.endObject();

		} catch(Exception e) {
			logger.error("Unexpected", e);

			// open JSON response
			try {
				json.object();
				json.key("result").value(false);
				BookingSessionContext ctx = (BookingSessionContext)
						request.getSession(true).getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
				manageGenericError(ctx, json, baseUrlForRedirect);
				json.endObject();
			} catch (JSONException e1) {
				logger.error("Error creating JSON ", e1);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
	}


	/**
	 * Return a map that contains all HTTP Headers present in the request.
	 * @param request
	 * @param separator used to concat in case of headers with multiple values
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Map<String, String> getHttpHeaders(SlingHttpServletRequest request, String separator) {
		// TODO Auto-generated method stub
		Map<String, String> httpHeaders = new HashMap<String,String>();
		Enumeration<String> headerNames = request.getHeaderNames();
		while(headerNames.hasMoreElements()){
			String headerName = headerNames.nextElement();
			if(headerName != null && headerName.length() > 0){
				Enumeration<String> headerValues = request.getHeaders(headerName);
				String headerValue = concatEnumeration(headerValues, separator);
				/*Insert into map*/
				httpHeaders.put(headerName, headerValue);
			}
		}
		return httpHeaders;
	}

	private String concatEnumeration(Enumeration<String> enumeration, String separator){
		String concat = "";
		while(enumeration.hasMoreElements()){
			String value = enumeration.nextElement();
			concat += value + separator;
		}
		/*remove trailing separator*/
		if(concat.length() > 0){
			concat = concat.substring(0, concat.lastIndexOf(separator));
		}
		return concat;
	}

	private String computeSubmitMethod(String nomeBanca) {
		logger.debug("Executing computeSubmitMethod. nomeBanca:["+nomeBanca+"]");
		if (PaymentTypeEnum.MAESTRO.value().equals(nomeBanca) || PaymentTypeEnum.MASTER_PASS.value().equals(nomeBanca) || PaymentTypeEnum.DISCOVER.value().equals(nomeBanca)
				|| PaymentTypeEnum.JCB.value().equals(nomeBanca) || PaymentTypeEnum.CARTE_BLEUE.value().equals(nomeBanca)
				|| PaymentTypeEnum.INSTALMENTS_BRAZIL.value().equals(nomeBanca) || PaymentTypeEnum.PAY_PAL.value().equals(nomeBanca)) {
			return "GET";
		}
		return "POST";
	}

	/**
	 * Populate an ArrayList of supported credit cards
	 * @return ArrayList<String> credit cards
	 */
	private ArrayList<String> populateCreditCards() {
		ArrayList<String> creditCards = new ArrayList<String>();
		creditCards.add(CreditCardTypeEnum.AMERICAN_EXPRESS.value());
		creditCards.add(CreditCardTypeEnum.MASTER_CARD.value());
		creditCards.add(CreditCardTypeEnum.VISA.value());
		creditCards.add(CreditCardTypeEnum.DINERS.value());
		creditCards.add(CreditCardTypeEnum.VISA_ELECTRON.value());
		creditCards.add(CreditCardTypeEnum.UATP.value());
		creditCards.add(CreditCardTypeEnum.JCB.value());
		creditCards.add(CreditCardTypeEnum.Discover.value());
		creditCards.add(CreditCardTypeEnum.MAESTRO.value());
		return creditCards;
	}

	private ArrayList<String> populateBrCreditCards() {
		ArrayList<String> creditCards = new ArrayList<String>();
		creditCards.add(CreditCardTypeEnum.MASTER_CARD.value());
		creditCards.add(CreditCardTypeEnum.VISA.value());
		creditCards.add(CreditCardTypeEnum.AMERICAN_EXPRESS.value());
		return creditCards;
	}

	/**
	 * Populate an ArrayList of supported credit cards
	 * @return ArrayList<String> credit cards
	 */
	private ArrayList<String> populateBanks() {
		ArrayList<String> banks = new ArrayList<String>();
		banks.add(PaymentTypeEnum.BANCA_INTESA.value());
		banks.add(PaymentTypeEnum.BANCO_POSTA.value());
		banks.add(PaymentTypeEnum.UNICREDIT.value());
		banks.add(PaymentTypeEnum.GLOBAL_COLLECT.value());
		banks.add(PaymentTypeEnum.MASTER_PASS.value());
		banks.add(PaymentTypeEnum.PAY_PAL.value());
		banks.add(PaymentTypeEnum.FINDOMESTIC.value());
		banks.add(PaymentTypeEnum.POSTE_ID.value());
		banks.add(PaymentTypeEnum.MAESTRO.value());
		banks.add(GlobalCollectPaymentTypeEnum.CARTE_BLEUE.value());
		banks.add(GlobalCollectPaymentTypeEnum.YANDEX.value());
		banks.add(GlobalCollectPaymentTypeEnum.WEB_MONEY.value());
		banks.add(GlobalCollectPaymentTypeEnum.IDEAL.value());
		return banks;
	}

	/**
	 * Populate an ArrayList of supported other Payments
	 * @return ArrayList<String> other Payments
	 */
	private ArrayList<String> populateOtherPayments() {
		ArrayList<String> otherPayments = new ArrayList<String>();
		otherPayments.add(PaymentTypeEnum.LOTTOMATICA.value());
		otherPayments.add(PaymentTypeEnum.PAY_AT_TO.value());
		return otherPayments;
	}

	/**
	 * Manage error on initializePayment
	 * @param json
	 * @throws JSONException
	 */
	private void manageErrorInitializePayment(JSONWriter json, I18n i18n, InitializePaymentErrorEnum initPaymentErrorCode) throws JSONException {
		logger.error("manage error initialize. initPaymentErrorCode: " + initPaymentErrorCode);
		if (initPaymentErrorCode == null){
			json.key("message").value(i18n.get(BookingConstants.MESSAGE_ERROR_INITIALIZE));
		} else {
			switch (initPaymentErrorCode) {
				case INFANT_INVENTORY_UNAVAILABLE :
					logger.debug("manageErrorInitializePayment case INFANT_INVENTORY_UNAVAILABLE");
					json.key("message").value(i18n.get(BookingConstants.MESSAGE_ERROR_INITIALIZE_INFANT_INVENTORY_UNAVAILABLE));
					break;
				default :
					json.key("message").value(i18n.get(BookingConstants.MESSAGE_ERROR_INITIALIZE));
					break;
			}
		}
	}

	/**
	 * Manage error on authorizePayment
	 * @param json
	 * @throws JSONException
	 */
	private void manageErrorAuthorizePayment(JSONWriter json, I18n i18n) throws JSONException {
		logger.error("manage error authorize");
		json.key("message").value(i18n.get(BookingConstants.MESSAGE_ERROR_AUTHORIZE));
	}

	/**
	 * Manage error on checkPayment
	 * @param json
	 * @throws JSONException
	 */
	private void manageErrorCheckPayment(JSONWriter json, BookingSessionContext ctx, String baseUrl) throws JSONException {
		logger.error("manage error check");
		String redirect = baseUrl + configuration.getBookingConfirmationPage();
		redirect += "?showCallCenterMsg=true";
		json.key("redirect").value(redirect);
		ctx.phase = BookingPhaseEnum.DONE;
	}

	/**
	 * Manage error on retrieveTickets. Both for CDC and Bonifico 
	 * if an error occurs during this payment setp, the ticket was confirmed and the email is sent
	 * @param json
	 * @throws JSONException
	 */
	private void manageErrorRetrieveTickets(JSONWriter json, BookingSessionContext ctx, String baseUrl) throws JSONException {
		logger.error("manage error retriveTicket");
		String redirect = baseUrl + configuration.getBookingConfirmationPage();
		redirect += "?showCallCenterMsg=true";
		json.key("redirect").value(redirect);
		ctx.phase = BookingPhaseEnum.DONE;
	}

	/**
	 * Manage generic error
	 * @param json
	 * @throws JSONException
	 */
	private void manageGenericError(BookingSessionContext ctx,JSONWriter json, String baseUrl) throws JSONException {
		logger.error("manage generic error");
		String redirect;
		if (ctx!= null && ctx.award) {
			redirect = baseUrl + configuration.getBookingAwardPaymentFailurePage();
		}
		else {
			redirect = baseUrl + configuration.getBookingPaymentFailurePage();
		}

		json.key("redirect").value(redirect);
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}
