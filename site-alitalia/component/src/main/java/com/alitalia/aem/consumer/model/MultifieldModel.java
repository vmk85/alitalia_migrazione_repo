package com.alitalia.aem.consumer.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MultifieldModel extends GenericBaseModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject @Optional
	private String multifieldName = "items";
	
	@Inject @Optional
	private String propertyName = "voce";
	
	private Map<String, Object> allValues = new HashMap<>();
	
	@PostConstruct
	protected void initModel() throws IOException {
		initBaseModel(request);
		try {
			allValues = AlitaliaUtils.fromJcrMultifieldToMapArrayObject(request.getResource(), multifieldName);
		} catch (JSONException e) {
			logger.error("Error trying to access multifield values.", e);
		}
	}

	public Map<String, Object> getAllValues() {
		return allValues;
	}
	
	public List<Object> getPropertyValues() {
		List<Object> propertyValues = new ArrayList<>();
		if (propertyName != null && allValues != null) {
			for (Object multifieldValue : allValues.values()) {
				if (multifieldValue instanceof Map) {
					propertyValues.add(((Map) multifieldValue).get(propertyName));
				}
			}
		}
		return propertyValues;
	}
	
	
		
}
