package com.alitalia.aem.consumer.model.content.specialpages;

public class CookiesData {

	public static final String NAME = "CookiesData";
	
	private String error;

	private String marketingCookie;
	private String thirdpartyCookie;

	public String getMarketingCookie() {
		return marketingCookie;
	}

	public void setMarketingCookie(String marketingCookie) {
		this.marketingCookie = marketingCookie;
	}

	public String getThirdpartyCookie() {
		return thirdpartyCookie;
	}

	public void setThirdpartyCookie(String thirdpartyCookie) {
		this.thirdpartyCookie = thirdpartyCookie;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
