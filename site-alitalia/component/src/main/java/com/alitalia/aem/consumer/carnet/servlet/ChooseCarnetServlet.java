package com.alitalia.aem.consumer.carnet.servlet;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.carnet.CarnetConstants;
import com.alitalia.aem.consumer.carnet.CarnetSession;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "choosecarnet" }),
		@Property(name = "sling.servlet.methods", value = { "POST"}),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class ChooseCarnetServlet extends GenericCarnetFormValidatorServlet {

	@Reference
	private volatile CarnetSession carnetSession;
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		Validator validator = new Validator();
		
		String idCarnet1 = request.getParameter("idCarnet1");
		String idCarnet2 = request.getParameter("idCarnet2");
		String condizioniCarnet1 = request.getParameter("condizioniCarnet1");
		String condizioniCarnet2 = request.getParameter("condizioniCarnet2");
		String paramCondizioni = "condizioniCarnet1";
		
		String idCarnet = null;
		String condizioniCarnet = null;
		
		if(idCarnet1 != null){
			idCarnet = idCarnet1;
			condizioniCarnet = condizioniCarnet1;
		} else if(idCarnet2 != null){
			idCarnet = idCarnet2;
			condizioniCarnet = condizioniCarnet2;
			paramCondizioni = "condizioniCarnet2";
		}
		
		validator.addDirectCondition(paramCondizioni,
				condizioniCarnet, I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				"isNotEmpty");
		
		validator.addDirectCondition("idCarnet", idCarnet, 
				CarnetConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
		
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
		
		CarnetSessionContext ctx = getCarnetSessionContext(request);
		String idCarnet1 = request.getParameter("idCarnet1");
		String idCarnet2 = request.getParameter("idCarnet2");
		String idCarnet = null;
		
		if(idCarnet1 != null){
			idCarnet = idCarnet1;
		} else if(idCarnet2 != null){
			idCarnet = idCarnet2;
		} else{
			throw new GenericFormValidatorServletException(false, "idCarnet parameter mancante");
		}
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		
		String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCarnetPersonalDataPage();
		successPage = request.getResourceResolver().map(successPage);
		
		boolean result = carnetSession.selectCarnet(ctx, idCarnet);
		
		if (result) {
			json.key("redirect").value(successPage);
		} else {
			throw new GenericFormValidatorServletException(false, "Error save data into session");
		}
		
		json.endObject();
	}

}
