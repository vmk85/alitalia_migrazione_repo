package com.alitalia.aem.consumer.millemiglia.servlet.profiloutente;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.MMLifestyleData;
import com.alitalia.aem.common.data.home.MMPreferenceData;
import com.alitalia.aem.common.data.home.NewInformationData;
import com.alitalia.aem.common.data.home.enumerations.MMGenderTypeEnum;
import com.alitalia.aem.common.messages.home.ProfileRequest;
import com.alitalia.aem.common.messages.home.ProfileResponse;
import com.alitalia.aem.common.messages.home.UpdateUserProfileRequest;
import com.alitalia.aem.common.messages.home.UpdateUserProfileResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.global.enumeration.CompagnieAeree;
import com.alitalia.aem.consumer.global.i18n.I18nKeyMillemiglia;
import com.alitalia.aem.consumer.millemiglia.model.profiloutente.PreferenzeViaggioData;
import com.alitalia.aem.consumer.millemiglia.render.ProfiloUtenteRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "preferenzeviaggiosubmit" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class PreferenzeViaggioServlet extends GenericFormValidatorServlet {

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaCustomerProfileManager customerProfileManager;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		
		logger.debug("[PreferenzeViaggioServlet] validateForm");
		
		try {
			// validate parameters!
			Validator validator = setValidatorParameters(request, new Validator());
			return validator.validate();
		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Errore durante la procedura di salvataggio dati ", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, 
			SlingHttpServletResponse response) throws IOException {
		
		logger.debug("[PreferenzeViaggioServlet] performSubmit");
		
		try {
			
			// recupero dati da aggiornare dalla request
			PreferenzeViaggioData preferenzeViaggioRequestData = new PreferenzeViaggioData();

			preferenzeViaggioRequestData.setPreferenzePosto(request.getParameter("tipoPosto"));
			preferenzeViaggioRequestData.setPreferenzePasto(request.getParameter("tipoPasto"));
			
			List<MMLifestyleData> frequentFlyerCompaniesSelected = new ArrayList<MMLifestyleData>();
			for (String companyCode : CompagnieAeree.listValues()) {
				if (request.getParameter(companyCode) != null) {
					MMLifestyleData lifestyleData = new MMLifestyleData();
					lifestyleData.setCategory(PreferenzeViaggioData.FREQUENT_FLYER_PROGRAMS);
					lifestyleData.setDescription(companyCode);
					lifestyleData.setLifestyle(companyCode);
					frequentFlyerCompaniesSelected.add(lifestyleData);
				}
			}
			preferenzeViaggioRequestData.setFrequentFlyerCompaniesSelected(frequentFlyerCompaniesSelected);
			
			// recupero i dati dell'utente salvati nella clientcontext
			MMCustomerProfileData userProperties = AlitaliaUtils.getAuthenticatedUser(request);
			
			// recupero altri dati dell'utente loggato chiamando il servizio getProfile
			MMCustomerProfileData customerProfileData = new MMCustomerProfileData();
			customerProfileData.setCustomerNumber(userProperties.getCustomerNumber());
			customerProfileData.setCustomerPinCode(
					customerProfileManager.decodeAndDecryptProperty(userProperties.getCustomerPinCode()));
			customerProfileData.setGender(userProperties.getGender());
			customerProfileData.setCustomerNickName(userProperties.getCustomerNickName());
			ProfileRequest profileRequest = new ProfileRequest();
	    	profileRequest.setSid(IDFactory.getSid());
	    	profileRequest.setTid(IDFactory.getTid());
	    	profileRequest.setCustomerProfile(customerProfileData);
	    	
	    	ProfileResponse profileResponse = consumerLoginDelegate.getProfile(profileRequest);
	    	customerProfileData = profileResponse.getCustomerProfile();
	    	ProfiloUtenteRender profiloUtenteRender = new ProfiloUtenteRender(customerProfileData);

	    	// creo gli oggetti newInformation e customerProfileToUpdate contenenti le informazioni da aggiornare
			NewInformationData newInformation = new NewInformationData();
			MMCustomerProfileData customerProfileToUpdate = new MMCustomerProfileData();
			
			// imposto gli attributi obbligatori
			customerProfileToUpdate.setCustomerNumber(userProperties.getCustomerNumber());
			customerProfileToUpdate.setCustomerPinCode(
					customerProfileManager.decodeAndDecryptProperty(userProperties.getCustomerPinCode()));
			customerProfileToUpdate.setCustomerName(userProperties.getCustomerName());
			customerProfileToUpdate.setCustomerSurname(userProperties.getCustomerSurname());
			customerProfileToUpdate.setLanguage(userProperties.getLanguage());
			customerProfileToUpdate.setBirthDate(userProperties.getBirthDate());
			customerProfileToUpdate.setMailingType(userProperties.getMailingType());
			customerProfileToUpdate.setGender(userProperties.getGender());
			customerProfileToUpdate.setEmail(profiloUtenteRender.getEmail().getEmail());
			customerProfileToUpdate.setAddresses(customerProfileData.getAddresses());
			customerProfileToUpdate.setTelephones(customerProfileData.getTelephones());
			customerProfileToUpdate.setCustomerNickName(customerProfileData.getCustomerNickName());

			// setting modified seat and meal preferences
			List<MMPreferenceData> preferenceList = new ArrayList<MMPreferenceData>();
			MMPreferenceData preference; 
			
			if (!preferenzeViaggioRequestData.getPreferenzePasto().equals("QLSS")) {
				preference = new MMPreferenceData();
				preference.setShortNameCategory("MEAL");
				preference.setShortNamePreference(preferenzeViaggioRequestData.getPreferenzePasto());
				preferenceList.add(preference);	
			}
			
			if (!preferenzeViaggioRequestData.getPreferenzePosto().equals("QLSS")) {
				preference = new MMPreferenceData();
				preference.setShortNameCategory("SEAT");
				preference.setShortNamePreference(preferenzeViaggioRequestData.getPreferenzePosto());
				preferenceList.add(preference);
			}
			
			customerProfileToUpdate.setPreferences(preferenceList);
			
			// setting modified frequent flyer componanies selected
			customerProfileToUpdate.setLifestyles(customerProfileData.getLifestyles());
			Iterator<MMLifestyleData> lifestyleDataIterator = customerProfileToUpdate.getLifestyles().iterator();
			while (lifestyleDataIterator.hasNext()) {
				MMLifestyleData lifestyleData = lifestyleDataIterator.next();
				if (lifestyleData.getCategory().equals(PreferenzeViaggioData.FREQUENT_FLYER_PROGRAMS)) {
					lifestyleDataIterator.remove();
				}
			}
			for (MMLifestyleData lifestyleData : preferenzeViaggioRequestData.getFrequentFlyerCompaniesSelected()) {
				customerProfileToUpdate.getLifestyles().add(lifestyleData);
			}

			// perform request
			UpdateUserProfileRequest updateUserProfileRequest = new UpdateUserProfileRequest();
			updateUserProfileRequest.setTid(IDFactory.getTid());
			updateUserProfileRequest.setSid(IDFactory.getSid(request));
			updateUserProfileRequest.setCustomerProfile(customerProfileToUpdate);
			updateUserProfileRequest.setNewInformation(newInformation);
			
			UpdateUserProfileResponse updateUserProfileResponse = 
					consumerLoginDelegate.updateProfile(updateUserProfileRequest);
			
			List<String> errors = updateUserProfileResponse.getCustomerUpdateErrors();
			if ((errors != null && errors.size() > 0 && !errors.get(0).equals("None")) ||
					updateUserProfileResponse.getCustomerProfile() == null) {
				throw new IOException(); //TODO: Verificare se sollevare un'eccezione diversa
			}
			
			goBackSuccessfully(request, response);
			
		} catch(Exception e) {
			logger.error("Error Dati Personali Servlet", e);
			throw new IOException(); //TODO: Verificare se sollevare un'eccezione diversa
		}
		
	}
	
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		
		logger.debug("[PreferenzeViaggioServlet] saveDataIntoSession");
		
		PreferenzeViaggioData preferenzeViaggioData = new PreferenzeViaggioData();

		preferenzeViaggioData.setPreferenzePosto(request.getParameter("tipoPosto"));
		preferenzeViaggioData.setPreferenzePasto(request.getParameter("tipoPasto"));
		
		List<MMLifestyleData> frequentFlyerCompaniesSelected = new ArrayList<MMLifestyleData>();
		for (String companyCode : CompagnieAeree.listValues()) {
			if (request.getParameter(companyCode) != null) {
				MMLifestyleData lifestyleData = new MMLifestyleData();
				lifestyleData.setCategory(PreferenzeViaggioData.FREQUENT_FLYER_PROGRAMS);
				lifestyleData.setDescription(companyCode);
				lifestyleData.setLifestyle(companyCode);
				frequentFlyerCompaniesSelected.add(lifestyleData);
			}
		}
		preferenzeViaggioData.setFrequentFlyerCompaniesSelected(frequentFlyerCompaniesSelected);
		
		request.getSession().setAttribute("preferenzeViaggioData", preferenzeViaggioData);
	}
	
	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request, Validator validator) {

		// get parameters from request
		String tipoPosto = request.getParameter("tipoPosto");
		String tipoPasto = request.getParameter("tipoPasto");

		// add validate conditions
		validator.addDirectCondition("tipoPosto", tipoPosto,
				I18nKeyMillemiglia.TIPOPOSTO_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectCondition("tipoPasto", tipoPasto,
				I18nKeyMillemiglia.TIPOPASTO_ERROR_EMPTY, "isNotEmpty");

		
		return validator;
	}
}
