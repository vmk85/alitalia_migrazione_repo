package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.passenger.Passenger;
import com.alitalia.aem.common.messages.checkinrest.CheckinPassengerRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinPassengerResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkinpassenger"}),   //TODO - corretto?
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class CheckinPassengerServlet extends SlingSafeMethodsServlet {

		//logger
		private Logger logger = LoggerFactory.getLogger(this.getClass());

		//Check-in Passenger Delegate
		@Reference
		private ICheckinDelegate checkinDelegate;
		
		@Override
		protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
				throws ServletException, IOException {
			
			// content type
			response.setContentType("application/json");
			// JSON response
			TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());
			
			try{
				//TODO - gestire il language ?
				
				// invoco il delegate
				CheckinPassengerRequest serviceRequest = new CheckinPassengerRequest(IDFactory.getTid(), IDFactory.getSid(request));
				serviceRequest.setAirline(request.getParameter("airline"));
				serviceRequest.setFlight(request.getParameter("flight"));
				serviceRequest.setBookingClass(request.getParameter("bookingClass"));
				serviceRequest.setOrigin(request.getParameter("origin"));
				serviceRequest.setDepartureDate(request.getParameter("departureDate"));
				serviceRequest.setLastName(request.getParameter("lastName"));
				serviceRequest.setPassengerID(request.getParameter("passengerID"));
				CheckinPassengerResponse serviceResponse = checkinDelegate.retrieveCheckinPassenger(serviceRequest);	
				
				Passenger passenger = serviceResponse.getPassenger();
				
				//TODO - costruire il JSON
				
			}
			catch(Exception e){
				logger.error(e.getMessage());
				throw new RuntimeException("[CheckinPassengerServlet] - Errore durante la ricerca dei dati del passeggero.", e);
			}
		}

}
