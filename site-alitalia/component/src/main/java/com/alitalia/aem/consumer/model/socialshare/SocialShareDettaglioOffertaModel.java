package com.alitalia.aem.consumer.model.socialshare;

import javax.annotation.PostConstruct;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.wcm.api.Page;

@Model(adaptables={SlingHttpServletRequest.class})
public class SocialShareDettaglioOffertaModel extends BaseSocialShareModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@PostConstruct
	protected void initModel() {
		super.initBaseModel(request);
		try {
			Page page = AlitaliaUtils.getPage(request.getResource());
			String[] selectors = request.getRequestPathInfo().getSelectors();
			if (page != null && selectors.length > 3) {
				HierarchyNodeInheritanceValueMap hierarchyNodeInheritanceValueMap = 
						new HierarchyNodeInheritanceValueMap(page.getContentResource());
				String cityCode = selectors[1];
				String price = selectors[2];
				String roundTrip = selectors[3];
				String currencySymbol = hierarchyNodeInheritanceValueMap.getInherited("currencySymbol", String.class);
				String flightType = "";
				if ("true".equalsIgnoreCase(roundTrip)) {
					flightType = getI18n().get("dettaglioofferta.andataeritorno.label");
				} else {
					flightType = getI18n().get("dettaglioofferta.soloandata.label");
				}
				String cityName = getI18n().get("airportsData." + cityCode + ".city");
				title = cityName + " " + getI18n().get("dettaglioofferta.socialshare.title", "", 
						currencySymbol, price, flightType);
				description = cityName;
				
				// link to page itself
				linkBack = findLinkBack(request);
				
				// build the path of the image component in the destination box node
				imagePath = findDestinationImagePath(request, cityCode);
			}
			
		} catch (Exception e) {
			logger.error("Error while building social share data", e);
		}
	}
	
	private String findDestinationImagePath(SlingHttpServletRequest request, String cityCode) 
			throws RepositoryException, ValueFormatException, PathNotFoundException {
		String url = "";
		String destinationsPath = AlitaliaUtils.findSiteBaseRepositoryPath(request.getResource()) 
				+ AlitaliaConstants.DESTINATIONS_PATH;
		Page page = findPageByName(request, destinationsPath, cityCode);
		if (page != null) {
			url = page.getPath() + "/jcr:content/image-lista-offerte";
		} else {
			url = destinationsPath + "/default/jcr:content/image-lista-offerte";
		}
		return url;
	}
	
	private String findLinkBack(SlingHttpServletRequest request) {
		String url = request.getRequestURI();
		if (request.getQueryString() != null && request.getQueryString().length() > 0) {
			url += "?" + request.getQueryString();
		}
		return url;
	}
	
}
