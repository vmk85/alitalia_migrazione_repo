package com.alitalia.aem.consumer.model.content.specialpages;

import com.alitalia.aem.consumer.utils.FrequentFlyer;

import java.util.Objects;

public class MillemigliaRecuperaCredenzialiData {

	public static final String NAME = "MillemigliaRecuperaCredenzialiData";

	public static final String[] RETRIEVERESETCHOICE = new String[] { "retrieveUsername", "resetPassword" };

	private String surname;
	private String mmcode;
	private String alias;
	 // Strong Auth Fields
    private String  flagVerifica_OTP_DS_OK = "False";

    private String idProfilo;
	private String username;
	private String usernameForm;
    private boolean flagVerificaUserName_OK = false;
	private String password_MD5;
	private String confermaPassword_MD5;
    private boolean flagVerificaPassword_OK = false;
//	private String confPassword;
	private String certifiedEmailAccount;
    private boolean flagVerificaEmailUser_OK = false;
    private String certifiedCountryNumber;
	private String certifiedPhoneNumber;
    private boolean flagVerificaCellulare_OK = false;
//	private String inputSecretQuestion;
//	private int inputSecretQuestionID;
	private String secretQuestionText;
    private int secretQuestionID;
    private String secretQuestionCode;
	private String rispostaSegreta_MD5;
	private Boolean  flagVerificaRisposta_OK = false;
	private boolean isLockedOut = false;


	private String error;
	private Integer state = 1;
	private String retrieveResetChoice = "retrieveUsername";
	private String trackingCod;


	public static String getNAME() {
		return NAME;
	}

	public static String[] getRETRIEVERESETCHOICE() {
		return RETRIEVERESETCHOICE;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getMmcode() {
		return mmcode;
	}

	public void setMmcode(String mmcode) {
		this.mmcode = mmcode;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getFlagVerifica_OTP_DS_OK() {
		return flagVerifica_OTP_DS_OK;
	}

	public void setFlagVerifica_OTP_DS_OK(String flagVerifica_OTP_DS_OK) {
		this.flagVerifica_OTP_DS_OK = flagVerifica_OTP_DS_OK;
	}

	public String getIdProfilo() {
		return idProfilo;
	}

	public void setIdProfilo(String idProfilo) {
		this.idProfilo = idProfilo;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


	public String getUsernameForm() {
		return usernameForm;
	}

	public void setUsernameForm(String usernameForm) {
		this.usernameForm = usernameForm;
	}

	public boolean getFlagVerificaUserName_OK() {
		return flagVerificaUserName_OK;
	}

	public void setFlagVerificaUserName_OK(boolean flagVerificaUserName_OK) {
		this.flagVerificaUserName_OK = flagVerificaUserName_OK;
	}

	public String getPassword_MD5() {
		return password_MD5;
	}

	public void setPassword_MD5(String password_MD5) {
		this.password_MD5 = password_MD5;
	}

	public String getConfermaPassword_MD5() {
		return confermaPassword_MD5;
	}

	public void setConfermaPassword_MD5(String confermaPassword_MD5) {
		this.confermaPassword_MD5 = confermaPassword_MD5;
	}

	public boolean getFlagVerificaPassword_OK() {
		return flagVerificaPassword_OK;
	}

	public void setFlagVerificaPassword_OK(boolean flagVerificaPassword_OK) {
		this.flagVerificaPassword_OK = flagVerificaPassword_OK;
	}

	public String getCertifiedEmailAccount() {
		return certifiedEmailAccount;
	}

	public void setCertifiedEmailAccount(String certifiedEmailAccount) {
		this.certifiedEmailAccount = certifiedEmailAccount;
	}

	public boolean getFlagVerificaEmailUser_OK() {
		return flagVerificaEmailUser_OK;
	}

	public void setFlagVerificaEmailUser_OK(boolean flagVerificaEmailUser_OK) {
		this.flagVerificaEmailUser_OK = flagVerificaEmailUser_OK;
	}

	public String getCertifiedCountryNumber() {
		return certifiedCountryNumber;
	}

	public void setCertifiedCountryNumber(String certifiedCountryNumber) {
		this.certifiedCountryNumber = certifiedCountryNumber;
	}

	public String getCertifiedPhoneNumber() {
		return certifiedPhoneNumber;
	}

	public void setCertifiedPhoneNumber(String certifiedPhoneNumber) {
		this.certifiedPhoneNumber = certifiedPhoneNumber;
	}

	public boolean getFlagVerificaCellulare_OK() {
		return flagVerificaCellulare_OK;
	}

	public void setFlagVerificaCellulare_OK(boolean flagVerificaCellulare_OK) {
		this.flagVerificaCellulare_OK = flagVerificaCellulare_OK;
	}

	public String getSecretQuestionText() {
		return secretQuestionText;
	}

	public void setSecretQuestionText(String secretQuestionText) {
		this.secretQuestionText = secretQuestionText;
	}

	public int getSecretQuestionID() {
		return secretQuestionID;
	}

	public void setSecretQuestionID(int secretQuestionID) {
		this.secretQuestionID = secretQuestionID;
	}

	public String getRispostaSegreta_MD5() {
		return rispostaSegreta_MD5;
	}

	public void setRispostaSegreta_MD5(String rispostaSegreta_MD5) {
		this.rispostaSegreta_MD5 = rispostaSegreta_MD5;
	}

	public boolean getFlagVerificaRisposta_OK() {
		return flagVerificaRisposta_OK;
	}

	public void setFlagVerificaRisposta_OK(boolean flagVerificaRisposta_OK) {
		this.flagVerificaRisposta_OK = flagVerificaRisposta_OK;
	}

	public boolean getLockedOut() {
		return isLockedOut;
	}

	public void setLockedOut(boolean lockedOut) {
		isLockedOut = lockedOut;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getRetrieveResetChoice() {
		return retrieveResetChoice;
	}

	public void setRetrieveResetChoice(String retrieveResetChoice) {
		this.retrieveResetChoice = retrieveResetChoice;
	}

	public String getTrackingCod() {
		return trackingCod;
	}

	public void setTrackingCod(String trackingCod) {
		this.trackingCod = trackingCod;
	}

    public String getSecretQuestionCode() {
        return secretQuestionCode;
    }

    public void setSecretQuestionCode(String secretQuestionCode) {
        this.secretQuestionCode = secretQuestionCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MillemigliaRecuperaCredenzialiData that = (MillemigliaRecuperaCredenzialiData) o;
        return flagVerifica_OTP_DS_OK == that.flagVerifica_OTP_DS_OK &&
                flagVerificaUserName_OK == that.flagVerificaUserName_OK &&
                flagVerificaPassword_OK == that.flagVerificaPassword_OK &&
                flagVerificaEmailUser_OK == that.flagVerificaEmailUser_OK &&
                flagVerificaCellulare_OK == that.flagVerificaCellulare_OK &&
                secretQuestionID == that.secretQuestionID &&
                flagVerificaRisposta_OK == that.flagVerificaRisposta_OK &&
                isLockedOut == that.isLockedOut &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(mmcode, that.mmcode) &&
                Objects.equals(alias, that.alias) &&
                Objects.equals(idProfilo, that.idProfilo) &&
				Objects.equals(username, that.username) &&
				Objects.equals(usernameForm, that.usernameForm) &&
				Objects.equals(password_MD5, that.password_MD5) &&
                Objects.equals(confermaPassword_MD5, that.confermaPassword_MD5) &&
                Objects.equals(certifiedEmailAccount, that.certifiedEmailAccount) &&
                Objects.equals(certifiedCountryNumber, that.certifiedCountryNumber) &&
                Objects.equals(certifiedPhoneNumber, that.certifiedPhoneNumber) &&
                Objects.equals(secretQuestionText, that.secretQuestionText) &&
                Objects.equals(secretQuestionCode, that.secretQuestionCode) &&
                Objects.equals(rispostaSegreta_MD5, that.rispostaSegreta_MD5) &&
                Objects.equals(error, that.error) &&
                Objects.equals(state, that.state) &&
                Objects.equals(retrieveResetChoice, that.retrieveResetChoice) &&
                Objects.equals(trackingCod, that.trackingCod);
    }

    @Override
    public int hashCode() {

        return Objects.hash(surname, mmcode, alias, flagVerifica_OTP_DS_OK, idProfilo, username,usernameForm, flagVerificaUserName_OK, password_MD5, confermaPassword_MD5, flagVerificaPassword_OK, certifiedEmailAccount, flagVerificaEmailUser_OK, certifiedCountryNumber, certifiedPhoneNumber, flagVerificaCellulare_OK, secretQuestionText, secretQuestionID, secretQuestionCode, rispostaSegreta_MD5, flagVerificaRisposta_OK, isLockedOut, error, state, retrieveResetChoice, trackingCod);
    }

    @Override
    public String toString() {
        return "MillemigliaRecuperaCredenzialiData{" +
                "surname='" + surname + '\'' +
                ", mmcode='" + mmcode + '\'' +
                ", alias='" + alias + '\'' +
                ", flagVerifica_OTP_DS_OK=" + flagVerifica_OTP_DS_OK +
                ", idProfilo='" + idProfilo + '\'' +
				", username='" + usernameForm + '\'' +
				", usernameForm='" + username + '\'' +
				", flagVerificaUserName_OK=" + flagVerificaUserName_OK +
                ", password_MD5='" + password_MD5 + '\'' +
                ", confermaPassword_MD5='" + confermaPassword_MD5 + '\'' +
                ", flagVerificaPassword_OK=" + flagVerificaPassword_OK +
                ", certifiedEmailAccount='" + certifiedEmailAccount + '\'' +
                ", flagVerificaEmailUser_OK=" + flagVerificaEmailUser_OK +
                ", certifiedCountryNumber='" + certifiedCountryNumber + '\'' +
                ", certifiedPhoneNumber='" + certifiedPhoneNumber + '\'' +
                ", flagVerificaCellulare_OK=" + flagVerificaCellulare_OK +
                ", secretQuestionText='" + secretQuestionText + '\'' +
                ", secretQuestionID=" + secretQuestionID +
                ", secretQuestionCode='" + secretQuestionCode + '\'' +
                ", rispostaSegreta_MD5='" + rispostaSegreta_MD5 + '\'' +
                ", flagVerificaRisposta_OK=" + flagVerificaRisposta_OK +
                ", isLockedOut=" + isLockedOut +
                ", error='" + error + '\'' +
                ", state=" + state +
                ", retrieveResetChoice='" + retrieveResetChoice + '\'' +
                ", trackingCod='" + trackingCod + '\'' +
                '}';
    }
}

