package com.alitalia.aem.consumer.booking.servlet;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.utils.CaptchaLoginServiceSingleton;
import com.octo.captcha.CaptchaException;
import com.octo.captcha.service.CaptchaServiceException;


@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "jcaptchalogin" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class ImageLoginCaptchaServlet extends SlingAllMethodsServlet {
	
	protected Logger logger = LoggerFactory.getLogger(getClass());

	public void init(ServletConfig servletConfig) throws ServletException {
		super.init(servletConfig);
	}

	protected void doGet(SlingHttpServletRequest httpServletRequest, SlingHttpServletResponse httpServletResponse) 
					throws ServletException, IOException {
		
		logger.info("Executing ImageCaptchaServlet doGet method.");

		byte[] captchaChallengeAsJpeg = null;
		// L'output stream che ci serve per renderizzare l'immagine Captcha in una JPEG
		ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
		try {
			// Prendiamo l'id di sessione che utilizzeremo per generare l'immagine captcha. 
			// Lo stesso id sarà utilizzato per validare la risposta
			String captchaId = httpServletRequest.getSession().getId();
			
			
			BufferedImage challenge = null;
			int attempts = 0;
			
			while (challenge == null && attempts < 10) {
				attempts++;
				try {
					challenge = CaptchaLoginServiceSingleton.getInstance()
							.getImageChallengeForID(captchaId,
									httpServletRequest.getLocale());
				} catch (CaptchaException e) {
					logger.error("Image captcha error", e);
				}
				
			}

			// L'encoder JPEG		
			ImageWriter imageWriter = (ImageWriter)ImageIO.getImageWritersBySuffix("jpeg").next();
            ImageOutputStream ios = ImageIO.createImageOutputStream(jpegOutputStream);
            imageWriter.setOutput(ios);
            IIOMetadata imageMetaData = imageWriter.getDefaultImageMetadata(new ImageTypeSpecifier(challenge), null);
            imageWriter.write(imageMetaData, new IIOImage(challenge, null, null), null);

		} catch (IllegalArgumentException e) {
			httpServletResponse.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		} catch (CaptchaServiceException e) {
			httpServletResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}

		captchaChallengeAsJpeg = jpegOutputStream.toByteArray();

		// Mettiamo il risultato nella risposta
		httpServletResponse.setHeader("Cache-Control", "no-store");
		httpServletResponse.setHeader("Pragma", "no-cache");
		httpServletResponse.setDateHeader("Expires", 0);
		httpServletResponse.setContentType("image/jpeg");
		ServletOutputStream responseOutputStream =
				httpServletResponse.getOutputStream();
		responseOutputStream.write(captchaChallengeAsJpeg);
		responseOutputStream.flush();
		responseOutputStream.close();
	}
}
