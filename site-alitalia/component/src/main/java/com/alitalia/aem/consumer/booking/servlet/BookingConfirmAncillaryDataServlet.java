package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "confirmancillarydata" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
})
@SuppressWarnings("serial")
public class BookingConfirmAncillaryDataServlet extends SlingSafeMethodsServlet {
	
	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private BookingSession bookingSession;
	
	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException { 
		
		String baseUrl = request.getResource().getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
		
		String genericErrorPagePlain = baseUrl + configuration.getBookingFailurePage();;
		String successPage = baseUrl + configuration.getBookingPaymentPage();
		
		//TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		try{
			//response.setContentType("application/json");
			//json.object();
			
			BookingSessionContext ctx = (BookingSessionContext) request.getSession(true).getAttribute(
					BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
			
			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				throw new IllegalStateException("Booking session context not found.");
			}
			
			if (ctx.award) {
				genericErrorPagePlain = baseUrl + configuration.getBookingAwardFailurePage();
			}
			
			bookingSession.confirmAncillaryData(ctx);
			//bookingSession.loadStoredCreditCard(ctx);
			response.sendRedirect(successPage);
			//json.key("redirect").value(successPage);
			//json.endObject();
		}catch(Exception e){
			logger.error("Unexpected error: ", e);
			/*try {
				json.key("redirect").value(genericErrorPagePlain);
				json.endObject();
			} catch (JSONException e1) {*/
				//logger.error("Error json creation: ", e1);
				response.sendRedirect(genericErrorPagePlain);
			//}
		}
	}
}

