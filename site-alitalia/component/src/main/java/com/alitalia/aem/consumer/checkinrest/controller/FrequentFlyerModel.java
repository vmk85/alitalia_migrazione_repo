package com.alitalia.aem.consumer.checkinrest.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.frequentflyer.FrequentFlyers;
import com.alitalia.aem.common.messages.home.GetFrequentFlyerRequest;
import com.alitalia.aem.common.messages.home.GetFrequentFlyerResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.staticdata.delegaterest.StaticDataDelegateRest;

@Model(adaptables = { SlingHttpServletRequest.class })
public class FrequentFlyerModel {

	@Self
	private SlingHttpServletRequest request;

	@Inject
	private volatile StaticDataDelegateRest staticDataDelegateRest;

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private FrequentFlyers frequentFlyer;
	
	@PostConstruct
	protected void initModel() throws Exception {
		frequentFlyer = new FrequentFlyers();
		GetFrequentFlyerRequest getFrequentFlyerRequest = new GetFrequentFlyerRequest(IDFactory.getTid(), IDFactory.getSid(request));
		getFrequentFlyerRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
		getFrequentFlyerRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
		getFrequentFlyerRequest.setConversationID(IDFactory.getTid());
		
		GetFrequentFlyerResponse getFrequentFlyerResponse = staticDataDelegateRest.getFrequentFlyer(getFrequentFlyerRequest);
	
		this.setFrequentFlyer(getFrequentFlyerResponse.getFrequentFlyers());
	}
	
	

	public FrequentFlyers getFrequentFlyer() {
		return frequentFlyer;
	}

	public void setFrequentFlyer(FrequentFlyers frequentFlyer) {
		this.frequentFlyer = frequentFlyer;
	}
	
}
