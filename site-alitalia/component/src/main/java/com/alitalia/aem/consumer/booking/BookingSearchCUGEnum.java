package com.alitalia.aem.consumer.booking;

import java.util.ArrayList;

public enum BookingSearchCUGEnum {
	
	ADT("ADT"),
	MILITARY("MIL"),
    YOUTH("YTH"),
    FAMILY("FAM"),
	CARNET("WEB");
	
    private final String value;
    
    BookingSearchCUGEnum(String v) {
        value = v;
    }
    
    public String value() {
    	return this.value;
    }
    
    public static String[] listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (BookingSearchCUGEnum c: BookingSearchCUGEnum.values()) {
			result.add(c.value);
		}
		return result.toArray(new String[0]);
	}
    
    public static BookingSearchCUGEnum fromValue(String v) {
        for (BookingSearchCUGEnum c: BookingSearchCUGEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}