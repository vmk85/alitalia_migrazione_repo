package com.alitalia.aem.consumer.checkinrest.jobs.publisher;

import com.alitalia.aem.common.data.checkinrest.model.offload.request.FlightPas;
import com.alitalia.aem.common.data.checkinrest.model.offload.request.PnrFlightInfo;
import com.alitalia.aem.common.data.checkinrest.model.offload.request.SeatList;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Flight;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Segment;
import com.alitalia.aem.common.messages.checkinrest.*;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;
import com.google.gson.Gson;
import org.apache.sling.event.jobs.JobBuilder;
import org.apache.sling.event.jobs.JobManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import java.util.HashMap;
import java.util.*;

/**
 * Created by ggadaleta on 06/02/2018.
 */
public abstract class BaseJobPublisher implements JobPublisher {

    private String sessionID;

    private String jobTopic;

    private JobManager jobManager;

    protected static final Logger logger = LoggerFactory.getLogger(BaseJobPublisher.class);

    public boolean publish(String sessionID, CheckinSessionContext ctx) throws IllegalArgumentException {

        if(sessionID == null) {
            throw new IllegalArgumentException("sessionID cannot be null");
        }

        if(jobTopic == null) {
            throw new IllegalArgumentException("jobTopic cannot be null");
        }

        if(ctx == null) {
            throw new IllegalArgumentException("CheckinSessionContext cannot be null");
        }

        this.setSessionID(sessionID);

        Map<String, Object> props = this.createProperties(ctx);
        this.getJobManager().addJob(this.getJobTopic(), props);
        return true;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getJobTopic() {
        return jobTopic;
    }

    public void setJobTopic(String jobTopic) {
        this.jobTopic = jobTopic;
    }

    public void setJobManager(JobManager jobManager) {
        this.jobManager = jobManager;
    }

    protected JobManager getJobManager() {
        return this.jobManager;
    }

    protected Map<String, Object> prepareClearInsuranceRequests(String baseKey, CheckinSessionContext ctx) {
        Map<String, Object> props = new HashMap<>();

        Gson gson = new Gson();

        for(int pnr = 0; pnr < ctx.pnrSelectedListDataRender.size(); pnr++) {
            if (ctx.pnrSelectedListDataRender.get(pnr).getInsurance()) {
                DeleteInsuranceRequest deleteInsuranceRequest = new DeleteInsuranceRequest();
                deleteInsuranceRequest.setConversationID(ctx.conversationId);
                deleteInsuranceRequest.setPnr(ctx.pnrSelectedListDataRender.get(pnr).getNumber());
                deleteInsuranceRequest.setLanguage(ctx.language);
                deleteInsuranceRequest.setMarket(ctx.market);
                props.put(baseKey+pnr, gson.toJson(deleteInsuranceRequest));
            }
        }
        return props;
    }

    protected Map<String, Object> prepareClearAncillariesRequests(String baseKey, CheckinSessionContext ctx) {
        Map<String, Object> props = new HashMap<>();

        Gson gson = new Gson();

        CheckinClearAncillarySessionEndRequest requestClearEmd = new CheckinClearAncillarySessionEndRequest();
        requestClearEmd.setConversationID(ctx.conversationId);
        requestClearEmd.setPnr(ctx.pnr);
        requestClearEmd.setLanguage(ctx.language);
        requestClearEmd.setMarket(ctx.market);
        props.put(baseKey, gson.toJson(requestClearEmd));

        return props;
    }

    protected Map<String, Object> prepareRedoUpdatePassengersRequests(String baseKey, CheckinSessionContext ctx) {
        Map<String, Object> props = new HashMap<>();

        Gson gson = new Gson();

        CheckinSelectedPassengerRequest serviceRequest = new CheckinSelectedPassengerRequest();

        try {

            String pnrNumber = ctx.pnr;
            for (int f = 0; f < ctx.selectedFlights.size(); f++) {
                FlightsRender currentFlight = ctx.selectedFlights.get(f);
                String currentOrigin = currentFlight.getSegments().get(0).getOrigin().getCode();
                String currentDepartureDate = currentFlight.getSegments().get(0).getOriginalDepartureDate();
                String currentAirline = currentFlight.getSegments().get(0).getAirline();
                String currentFlightNumber = currentFlight.getSegments().get(0).getFlight();

                List<com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request.Passenger> selectedPassengers = new ArrayList<>();
                for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passenger : ctx.checkinSelectedPassengers) {
                    com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request.Passenger tempPassenger = new com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request.Passenger();
                    tempPassenger.setFirstName(passenger.getNome());
                    tempPassenger.setLastName(passenger.getCognome());
                    tempPassenger.setPassengerID(passenger.getPassengerID());
                    selectedPassengers.add(tempPassenger);
                }

                serviceRequest.setPnr(pnrNumber);
                serviceRequest.setAirline(currentAirline);
                serviceRequest.setOrigin(currentOrigin);
                serviceRequest.setDepartureDate(currentDepartureDate.split("T")[0]);
                serviceRequest.setFlight(currentFlightNumber);
                serviceRequest.setLanguage(ctx.language);
                serviceRequest.setMarket(ctx.market);
                serviceRequest.setConversationID(ctx.conversationId);
                serviceRequest.setPassengers(selectedPassengers);

            }
        } catch (Exception e) {
            logger.error("[prepareRedoUpdatePassengersRequests] valore chiave non trovato, " + e.getStackTrace());
            System.out.println(e.getStackTrace());
            //TODO gestire questo caso
        }

        props.put(baseKey, gson.toJson(serviceRequest));

        return props;
    }

    protected Map<String, Object> preparePrecheckinOffloadRequests(String baseKey, CheckinSessionContext ctx) {
        Map<String, Object> props = new HashMap<>();

        Gson gson = new Gson();

        CheckinOffloadRequest checkinOffloadRequest = new CheckinOffloadRequest();
        checkinOffloadRequest.setConversationID(ctx.conversationId);
        checkinOffloadRequest.setLanguage(ctx.language);
        checkinOffloadRequest.setMarket(ctx.market);

        List<PnrFlightInfo> pnrFlightInfoList = new ArrayList<>();

        //Stato passeggeri selezionati: controllo per precheckin
        for(int pnr = 0; pnr < ctx.pnrSelectedListDataRender.size(); pnr++){

            PnrFlightInfo pnrFlightInfo = new PnrFlightInfo();
            pnrFlightInfo.setPnr(ctx.pnrSelectedListDataRender.get(pnr).getNumber());

            List<FlightPas> flightPasList = new ArrayList<>();

            //---- Flights ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
            for(int f = 0; f < ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().size(); f++){
                //---- Segments	---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
                for(int s = 0; s < ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().size(); s++){
                    SegmentRender segmentRender = ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s);
                    if (segmentRender.getOpenCI()){
                        FlightPas flightPas = new FlightPas();
                        flightPas.setAirline(segmentRender.getAirline());
                        flightPas.setDepartureDate(segmentRender.getOriginalDepartureDate());
                        flightPas.setOrigin(segmentRender.getOrigin().getCode());
                        flightPas.setFlight(segmentRender.getFlight());

                        List<SeatList> seatListList = new ArrayList<>();

                        //---- Passengers ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
                        for(int p = 0; p < segmentRender.getPassengers().size(); p++){
                            //---- SelectedPassengers ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
                            for(int sp = 0; sp < ctx.checkinSelectedPassengers.size(); sp++) {
                                logger.debug(" - " + ctx.checkinSelectedPassengers.get(sp).getNome() + " - " + ctx.checkinSelectedPassengers.get(sp).getCognome() + " - " + ctx.checkinSelectedPassengers.get(sp).getPassengerID());
                                if (ctx.checkinSelectedPassengers.get(sp).getPassengerID().equals(ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().get(p).getPassengerID())) {
                                    if (ctx.listoriginalDataPassengers.get(sp).getPassengerID().equals(ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().get(p).getPassengerID())) {
                                        Passenger passenger = segmentRender.getPassengers().get(p);
                                        if (passenger.isPrecheckin() && !ctx.listoriginalDataPassengers.get(sp).getCheckInComplete()) {
                                            //precheckin effettuato per questo passeggero su questo volo su questa tratta
                                            logger.debug("Precheckin recognized for {}", passenger.getNome() + " " + passenger.getCognome() + " " + passenger.getPassengerID());

                                            SeatList seatList = new SeatList();
                                            seatList.setFirstName(passenger.getNome());
                                            seatList.setLastName(passenger.getCognome());

                                            seatListList.add(seatList);

                                        }
                                    }
                                }
                            }
                        }

                        if(seatListList.size() > 0) {
                            flightPas.setSeatList(seatListList);
                            flightPasList.add(flightPas);
                        }
                    }
                }
            }

            if(flightPasList.size() > 0) {
                pnrFlightInfo.setFlightPass(flightPasList);
                pnrFlightInfoList.add(pnrFlightInfo);
            }
        }

        if(pnrFlightInfoList.size() > 0) {
            checkinOffloadRequest.setPnrFlightInfo(pnrFlightInfoList);
            props.put(baseKey, gson.toJson(checkinOffloadRequest));
        }

        return props;
    }

    protected Map<String, Object> prepareComfortSeatOffloadRequests(String baseKey, CheckinSessionContext ctx) {
        Map<String, Object> props = new HashMap<>();

        Gson gson = new Gson();

        CheckinOffloadRequest checkinOffloadRequest = new CheckinOffloadRequest();
        checkinOffloadRequest.setConversationID(ctx.conversationId);
        checkinOffloadRequest.setLanguage(ctx.language);
        checkinOffloadRequest.setMarket(ctx.market);

        List<PnrFlightInfo> pnrFlightInfoList = new ArrayList<>();

        //Stato passeggeri selezionati: controllo per precheckin
        for(int pnr = 0; pnr < ctx.pnrSelectedListDataRender.size(); pnr++){

            PnrFlightInfo pnrFlightInfo = new PnrFlightInfo();
            pnrFlightInfo.setPnr(ctx.pnrSelectedListDataRender.get(pnr).getNumber());

            List<FlightPas> flightPasList = new ArrayList<>();

            //---- Flights ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
            for(int f = 0; f < ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().size(); f++){
                //---- Segments	---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
                for(int s = 0; s < ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().size(); s++){
                    SegmentRender segmentRender = ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s);
                    if (segmentRender.getOpenCI()){
                        FlightPas flightPas = new FlightPas();
                        flightPas.setAirline(segmentRender.getAirline());
                        flightPas.setDepartureDate(segmentRender.getOriginalDepartureDate());
                        flightPas.setOrigin(segmentRender.getOrigin().getCode());
                        flightPas.setFlight(segmentRender.getFlight());

                        List<SeatList> seatListList = new ArrayList<>();

                        //---- Passengers ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
                        for(int p = 0; p < segmentRender.getPassengers().size(); p++){
                            //---- SelectedPassengers ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
                            for(int sp = 0; sp < ctx.checkinSelectedPassengers.size(); sp++) {
                                logger.debug(" - " + ctx.checkinSelectedPassengers.get(sp).getNome() + " - " + ctx.checkinSelectedPassengers.get(sp).getCognome() + " - " + ctx.checkinSelectedPassengers.get(sp).getPassengerID());
                                if (ctx.checkinSelectedPassengers.get(sp).getPassengerID().equals(ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().get(p).getPassengerID())) {
                                    Passenger passenger = segmentRender.getPassengers().get(p);
                                    logger.debug("Precheckin recognized for {}", passenger.getNome() + " " + passenger.getCognome() + " " + passenger.getPassengerID());

                                    SeatList seatList = new SeatList();
                                    seatList.setFirstName(passenger.getNome());
                                    seatList.setLastName(passenger.getCognome());

                                    seatListList.add(seatList);

                                }
                            }
                        }

                        if(seatListList.size() > 0) {
                            flightPas.setSeatList(seatListList);
                            flightPasList.add(flightPas);
                        }
                    }
                }
            }

            if(flightPasList.size() > 0) {
                pnrFlightInfo.setFlightPass(flightPasList);
                pnrFlightInfoList.add(pnrFlightInfo);
            }
        }

        if(pnrFlightInfoList.size() > 0) {
            checkinOffloadRequest.setPnrFlightInfo(pnrFlightInfoList);
            props.put(baseKey, gson.toJson(checkinOffloadRequest));
        }

        return props;
    }

}
