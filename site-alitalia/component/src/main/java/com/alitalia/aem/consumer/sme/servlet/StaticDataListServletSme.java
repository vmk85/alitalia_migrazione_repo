package com.alitalia.aem.consumer.sme.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import com.alitalia.aem.common.data.home.*;
import com.alitalia.aem.common.messages.home.*;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.global.enumeration.ComfortSeatsClaimReasons;
import com.alitalia.aem.consumer.global.enumeration.LinguaCorrispondenza;
import com.alitalia.aem.consumer.global.enumeration.Professioni;
import com.alitalia.aem.consumer.global.enumeration.TipoTelefono;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "staticdatalistservletsme" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class StaticDataListServletSme extends SlingAllMethodsServlet {
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegate staticDataDelegate;
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	protected Logger logger = LoggerFactory.getLogger(getClass());

	private I18n i18n;
	private Locale locale;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		try {
			logger.info("StaticDataListServletSme");
			logger.debug("reuqest: " + request);
			response.setContentType("application/json");

			locale = AlitaliaUtils.findResourceLocale(request.getResource());
			ResourceBundle resourceBundle = request.getResourceBundle(locale);
			i18n = new I18n(resourceBundle);
			TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());

			jsonOutput.object();

			if (request.getParameter("countries") != null) {
				jsonOutput.key("countries");
				getCountries(request, jsonOutput);
			}

			if (request.getParameter("states") != null) {
				jsonOutput.key("states");
				getStates(request, jsonOutput, (String) request.getParameter("states"));
			}

			if (request.getParameter("professioni") != null) {
				jsonOutput.key("professioni");
				getProfessioni(request, jsonOutput);
			}

			if (request.getParameter("tipiTelefono") != null) {
				jsonOutput.key("tipiTelefono");
				getTipiTelefono(request, jsonOutput);
			}

			if (request.getParameter("prefissiNazionali") != null) {
				jsonOutput.key("prefissiNazionali");
				getPrefissiNazionali(request, jsonOutput);
			}

			if (request.getParameter("linguaCorrispondenza") != null) {
				jsonOutput.key("linguaCorrispondenza");
				getLinguaCorrispondenza(request, jsonOutput, (String) request.getParameter("states"));
			}

			if (request.getParameter("tipoPosto") != null) {
				jsonOutput.key("tipoPosto");
				getSeatTypes(request, jsonOutput);
			}

			if (request.getParameter("tipoPasto") != null) {
				jsonOutput.key("tipoPasto");
				getMealTypes(request, jsonOutput);
			}

			if (request.getParameter("ragioniReclamoPostoComfort") != null) {
				jsonOutput.key("ragioniReclamoPostoComfort");
				getComfortSeatsClaimReasons(request, jsonOutput);
			}
			
			if (request.getParameter("frequentFlyerTypes") != null) {
				jsonOutput.key("frequentFlyerTypes");
				getFrequentFlyerTypes(request, jsonOutput);
			}

			if (request.getParameter("secureQuestions") != null) {
				jsonOutput.key("secureQuestions");
				getSecureQuestions(request, jsonOutput);
			}

			jsonOutput.endObject();

		} catch (Exception e) {
			logger.error("Error during getting dropdown data.", e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}

	}

	private void getCountries(SlingHttpServletRequest request, TidyJSONWriter jsonOutput) throws JSONException {

		RetrieveCountriesRequest countriesServiceRequest = new RetrieveCountriesRequest(IDFactory.getTid(),
				IDFactory.getSid(request));
		countriesServiceRequest.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()));
		countriesServiceRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));

		jsonOutput.array();
		
		try {
			RetrieveCountriesResponse countriesServiceResponse = staticDataDelegate
					.retrieveCountries(countriesServiceRequest);
			List<CountryData> countryList = countriesServiceResponse.getCountries();
	
			for (CountryData country : countryList) {
				if(configuration.getSetProperty(AlitaliaConfigurationHolder.FORM_SME_COUNTRIES).contains(country.getCode())){
					String countryI18nKey = "countryData." + country.getCode();
					String countryI18nDescription = i18n.get(countryI18nKey);
					if (!countryI18nKey.equals(i18n.get(countryI18nKey))){
						jsonOutput.object();
						jsonOutput.key("code").value(country.getCode());
						jsonOutput.key("description").value(countryI18nDescription);
						jsonOutput.key("stateCode").value(country.getStateCode());
						jsonOutput.key("stateDescription").value(country.getStateDescription());
						jsonOutput.endObject();
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error during getCountries operation");
		}

		jsonOutput.endArray();
	}
	
	private void getStates(SlingHttpServletRequest request, TidyJSONWriter jsonOutput, String country)
			throws JSONException {

		RetrieveStateListRequest stateListRequest = 
				new RetrieveStateListRequest(IDFactory.getTid(), IDFactory.getSid(request));
		stateListRequest.setLanguageCode(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
		stateListRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
		stateListRequest.setItemCache(country);
		
		jsonOutput.array();
		
		try {
			RetrieveStateListResponse stateListResponse = staticDataDelegate.retrieveStateList(stateListRequest);
			List<StateData> stateList = stateListResponse.getStates();
			
			if (stateList != null && stateList.size() > 0) {
				for (StateData state : stateList) {
					String stateI18nKey = "stateProvinceData." + country.toLowerCase() + "." + state.getStateCode();
					String stateI18nName = i18n.get(stateI18nKey);
					if (!stateI18nKey.equals(stateI18nName)) {
						jsonOutput.object();
						jsonOutput.key("code").value(state.getStateCode());
						jsonOutput.key("name").value(stateI18nName);
						jsonOutput.endObject();
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error during getStates operation");
		}
		
		jsonOutput.endArray();
	}

	private void getProfessioni(SlingHttpServletRequest request, TidyJSONWriter jsonOutput) throws JSONException {
		jsonOutput.array();
		for (String value : Professioni.listValues()) {
			jsonOutput.object();
			jsonOutput.key("name").value(value);
			jsonOutput.key("value").value(i18n.get(Professioni.fromValue(value).getI18nValue()));
			jsonOutput.endObject();
		}
		jsonOutput.endArray();
	}

	private void getTipiTelefono(SlingHttpServletRequest request, TidyJSONWriter jsonOutput) throws JSONException {

		jsonOutput.array();
		for (String value : TipoTelefono.listValues()) {
			jsonOutput.object();
			jsonOutput.key("name").value(value);
			jsonOutput.key("value").value(i18n.get(TipoTelefono.fromValue(value).getI18nValue()));
			jsonOutput.endObject();
		}
		jsonOutput.endArray();
	}

	private void getPrefissiNazionali(SlingHttpServletRequest request, TidyJSONWriter jsonOutput) throws JSONException {

		RetrievePhonePrefixRequest phonePrefixServiceRequest = new RetrievePhonePrefixRequest(IDFactory.getTid(),
				IDFactory.getSid(request));
		phonePrefixServiceRequest.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()));
		phonePrefixServiceRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));

		jsonOutput.array();
		
		try {
			RetrievePhonePrefixResponse phonePrefixServiceResponse = staticDataDelegate
					.retrievePhonePrefixs(phonePrefixServiceRequest);
			List<PhonePrefixData> phonePrefixList = phonePrefixServiceResponse.getPhonePrefix();
	
			for (PhonePrefixData phonePrefix : phonePrefixList) {
				String phonePrefixI18nKey = "countryData." + phonePrefix.getCode();
				String phonePrefixI18nDescription = i18n.get(phonePrefixI18nKey);
				if (!phonePrefixI18nKey.equals(i18n.get(phonePrefixI18nKey))) {
					jsonOutput.object();
					jsonOutput.key("code").value(phonePrefix.getCode());
					jsonOutput.key("description").value(phonePrefixI18nDescription);
					jsonOutput.key("prefix").value(phonePrefix.getPrefix());
					jsonOutput.endObject();
				}
			}
		} catch (Exception e) {
			logger.error("Error during getPrefissiNazionali operation");
		}

		jsonOutput.endArray();
	}

	private void getLinguaCorrispondenza(SlingHttpServletRequest request, TidyJSONWriter jsonOutput, 
			String selectedNation) throws JSONException {

		jsonOutput.array();
		
		if (selectedNation != null && selectedNation.equals(AlitaliaConstants.COUNTRY_ITA)) {
			jsonOutput.object();
			jsonOutput.key("name").value(LinguaCorrispondenza.ITALIANO.value());
			jsonOutput.key("value").value(i18n.get(LinguaCorrispondenza.ITALIANO.getI18nValue()));
			jsonOutput.endObject();
		} else {
			for (String value : LinguaCorrispondenza.listValues()) {
				jsonOutput.object();
				jsonOutput.key("name").value(value);
				jsonOutput.key("value").value(i18n.get(LinguaCorrispondenza.fromValue(value).getI18nValue()));
				jsonOutput.endObject();
			}
		}
		
		jsonOutput.endArray();
	}

	private void getSeatTypes(SlingHttpServletRequest request, TidyJSONWriter jsonOutput) throws JSONException {

		RetrieveSeatTypeRequest retrieveSeatTypeRequest = new RetrieveSeatTypeRequest();
		retrieveSeatTypeRequest.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()));
		retrieveSeatTypeRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
		
		jsonOutput.array();

		try {
			RetrieveSeatTypeResponse retrieveSeatTypeResponse = staticDataDelegate
					.retrievesSeatTypes(retrieveSeatTypeRequest);
			List<SeatTypeData> seatTypeList = retrieveSeatTypeResponse.getSeatTypesData();
	
			jsonOutput.object();
			jsonOutput.key("code").value("QLSS");
			jsonOutput.key("description").value(i18n.get("seatTypeData.QLSS"));
			jsonOutput.endObject();
	
			for (SeatTypeData seatType : seatTypeList) {
				String seatTypeI18nKey = "seatTypeData." + seatType.getCode();
				String seatTypeI18nDescription = i18n.get(seatTypeI18nKey);
				if (!seatTypeI18nKey.equals(i18n.get(seatTypeI18nKey))) {
					jsonOutput.object();
					jsonOutput.key("code").value(seatType.getCode());
					jsonOutput.key("description").value(seatTypeI18nDescription);
					jsonOutput.endObject();
				}
			}
		} catch (Exception e) {
			logger.error("Error during getSeatTypes operation");
		}

		jsonOutput.endArray();
	}

	private void getMealTypes(SlingHttpServletRequest request, TidyJSONWriter jsonOutput) throws JSONException {

		RetrieveMealsRequest retrieveMealsRequest = new RetrieveMealsRequest(IDFactory.getTid(),
				IDFactory.getSid(request));
		retrieveMealsRequest.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()));
		retrieveMealsRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
		
		jsonOutput.array();
		
		try {
			RetrieveMealsResponse retrieveMealsResponse = staticDataDelegate.retrieveMeals(retrieveMealsRequest);
			List<MealData> mealTypeList = retrieveMealsResponse.getMeals();
	
			jsonOutput.object();
			jsonOutput.key("code").value("QLSS");
			jsonOutput.key("description").value(i18n.get("mealsData.QLSS"));
			jsonOutput.endObject();
	
			for (MealData mealType : mealTypeList) {
				String mealTypeI18nKey = "mealsData." + mealType.getCode();
				String mealTypeI18nDescription = i18n.get(mealTypeI18nKey);
				if (!mealTypeI18nKey.equals(i18n.get(mealTypeI18nKey))) {
					jsonOutput.object();
					jsonOutput.key("code").value(mealType.getCode());
					jsonOutput.key("description").value(mealTypeI18nDescription);
					jsonOutput.endObject();
				}
			}
		} catch (Exception e) {
			logger.error("Error during getMealTypes operation");
		}

		jsonOutput.endArray();
	}

	private void getComfortSeatsClaimReasons(SlingHttpServletRequest request, TidyJSONWriter jsonOutput)
			throws JSONException {

		jsonOutput.array();
		ComfortSeatsClaimReasons.listValues().stream().forEachOrdered(value -> {
			try {
				jsonOutput.object();
				jsonOutput.key("name").value(value);
				jsonOutput.key("value").value(i18n.get(ComfortSeatsClaimReasons.fromValue(value).getI18nValue()));
				jsonOutput.endObject();
			} catch (JSONException e) {
				throw new IllegalArgumentException();
			}
		});
		jsonOutput.endArray();
	}
	
	private void getFrequentFlyerTypes(SlingHttpServletRequest request, TidyJSONWriter jsonOutput) throws JSONException {
		
		RetrieveFrequentFlayerRequest frequentFlyerRequest = new RetrieveFrequentFlayerRequest(IDFactory.getTid(),
				IDFactory.getSid(request));
		frequentFlyerRequest.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()).toUpperCase());
		frequentFlyerRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
		
		jsonOutput.array();
		
		try {
			RetrieveFrequentFlyersResponse frequentFlyerResponse = 
					staticDataDelegate.retrieveFrequentFlyersType(frequentFlyerRequest);
	
			List<FrequentFlyerTypeData> frequentFlyerTypes = frequentFlyerResponse.getFrequentFlyers();
	
			for (FrequentFlyerTypeData frequentFlyerType : frequentFlyerTypes) {
				jsonOutput.object();
				jsonOutput.key("code").value(frequentFlyerType.getCode());
				jsonOutput.key("description").value(frequentFlyerType.getDescription());
				jsonOutput.endObject();
			}
		} catch (Exception e) {
			logger.error("Error during getFrequentFlyerTypes operation");
		}

		jsonOutput.endArray();
	}

	private void getSecureQuestions(SlingHttpServletRequest request, TidyJSONWriter jsonOutput) throws JSONException {

		RetrieveSecureQuestionListRequest secureQuestionRequest = new RetrieveSecureQuestionListRequest(IDFactory.getTid(),
				IDFactory.getSid(request));
		secureQuestionRequest.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()).toUpperCase());
		secureQuestionRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));

		jsonOutput.array();

		try {
			RetrieveSecureQuestionListResponse secureQuestionResponse =
					staticDataDelegate.retrieveSecureQuestionList(secureQuestionRequest);

			List<SecureQuestionData> secureQuestionList = secureQuestionResponse.getSecureQuestions();

			for (SecureQuestionData secureQuestion : secureQuestionList) {

				String secureQuestionI18nKey = "securequestion." + secureQuestion.getSecureQuestionCode();
				String countryI18nDescription = i18n.get(secureQuestionI18nKey);
				//if (!secureQuestionI18nKey.equals(i18n.get(secureQuestionI18nKey))) {
					jsonOutput.object();
					jsonOutput.key("id").value(secureQuestion.getSecureQuestionID());
					jsonOutput.key("code").value(secureQuestion.getSecureQuestionCode());
					jsonOutput.key("description").value(countryI18nDescription);
					jsonOutput.key("secureQuestionCode").value(secureQuestion.getSecureQuestionCode());
					jsonOutput.key("secureQuestionDescription").value(secureQuestion.getSecureQuestionDescription());
					jsonOutput.endObject();
				//}

			}
		} catch (Exception e) {
			logger.error("Error during getSecureQuestions operation");
		}

		jsonOutput.endArray();
	}
}