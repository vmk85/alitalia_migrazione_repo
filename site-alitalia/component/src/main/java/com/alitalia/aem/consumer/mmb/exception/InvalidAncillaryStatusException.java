package com.alitalia.aem.consumer.mmb.exception;

public class InvalidAncillaryStatusException extends Exception {

	private static final long serialVersionUID = -8388948758281010112L;
	
	public InvalidAncillaryStatusException(String message) {
		super(message);
	}
	
	public InvalidAncillaryStatusException(Throwable cause) {
		super(cause);
	}
	
}
