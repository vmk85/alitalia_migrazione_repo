package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.MMMemberIdentificationType;
import com.alitalia.aem.common.data.home.MMTicketNumberType;
import com.alitalia.aem.common.messages.home.MMPaybackAccrualRequest;
import com.alitalia.aem.common.messages.home.MMPaybackAccrualResponse;
import com.alitalia.aem.consumer.global.enumeration.RichiediMigliaNumberCode;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.PaybackAccrualData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "paybackaccrualsubmit" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class PaybackAccrualServlet extends GenericFormValidatorServlet{
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[PaybackAccrualServlet] validateForm");
		
		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			return validator.validate();
			
		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Errore durante la procedura di validazione dati "
					+ "richiesta accredito payback", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		logger.debug("[PaybackAccrualServlet] performSubmit");
		
		// get parameters from request
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String paybackCode = request.getParameter("paybackCode");
		String ticketCode = request.getParameter("ticketCode");
		String ticketNumber = request.getParameter("ticketNumber");
		
		MMPaybackAccrualRequest paybackRequest = new MMPaybackAccrualRequest();
		paybackRequest.setMemberVerificationFirstName(name);
		paybackRequest.setMemberVerificationLastName(surname);
		
		MMMemberIdentificationType identificationType = new MMMemberIdentificationType();
		identificationType.setAlias(paybackCode);
		identificationType.setAliasType(0);
		
		paybackRequest.setPaybackCardNumber(identificationType);
		
		MMTicketNumberType ticketNumberType = new MMTicketNumberType();
		ticketNumberType.setAirlineCode(ticketCode);
		ticketNumberType.setTicketNumberId(ticketNumber);
		
		paybackRequest.setTicketNumber(ticketNumberType);
		paybackRequest.setUsername("INTERNET");
		
		MMPaybackAccrualResponse paybackResponse =
				consumerLoginDelegate.paybackAccrual(paybackRequest);
		
		//TODO capire casistiche
		Long code = paybackResponse.getPaybackResponse();
		if(code == 0){
			goBackSuccessfully(request, response);			
		}
		else{
			throw new IOException("Service Error");
		}
		
		
	}
	
	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {

		// get parameters from request
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String paybackCode = request.getParameter("paybackCode");
		String ticketCode = request.getParameter("ticketCode");
		String ticketNumber = request.getParameter("ticketNumber");
		
	    ArrayList<String> tktCodeList = RichiediMigliaNumberCode.listValues();
	    Object[] tktCodeListObj = tktCodeList.toArray();
		// add validate conditions
		validator.addDirectCondition("name", name,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("name", name,
				I18nKeyCommon.MESSAGE_INVALID_FIELD, "isAlphabeticWithSpaces");
		
		validator.addDirectCondition("surname", surname,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("surname", surname,
				I18nKeyCommon.MESSAGE_INVALID_FIELD, "isAlphabeticWithSpaces");
		
		validator.addDirectCondition("paybackCode", paybackCode,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("paybackCode", paybackCode , 
				I18nKeyCommon.MESSAGE_INVALID_FIELD, "isAlphaNumeric");
		
		validator.addDirectCondition("ticketCode", ticketCode,
				 I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
	    validator.setAllowedValues("ticketCode", ticketCode, tktCodeListObj,
	            I18nKeyCommon.MESSAGE_INVALID_FIELD);
		
	    validator.addDirectCondition("ticketNumber", ticketNumber, 
	            I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
        validator.addDirectCondition("ticketNumber", ticketNumber,
	            I18nKeyCommon.MESSAGE_INVALID_FIELD, "isNumber");
        validator.addCrossCondition("ticketNumber", ticketNumber, "10",
	            I18nKeyCommon.MESSAGE_INVALID_FIELD, "sizeIsEqual");
		
		return validator;

	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[PaybackAccrualServlet] saveDataIntoSession");
		
		// create baggageClaimData object and save it into session
		PaybackAccrualData paybackAccrualData = new PaybackAccrualData();
		paybackAccrualData.setName(request.getParameter("name"));
		paybackAccrualData.setSurname(request.getParameter("surname"));
		paybackAccrualData.setPaybackCode(request.getParameter("paybackCode"));
		paybackAccrualData.setTicketCode(request.getParameter("ticketCode"));
		paybackAccrualData.setTicketNumber(request.getParameter("ticketNumber"));
		
		// go back to form with error
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		
		//TODO gestire errori specifici 
		paybackAccrualData.setError(i18n.get(I18nKeySpecialPage.ERROR_SERVICE));
		
		request.getSession().setAttribute(PaybackAccrualData.NAME,
				paybackAccrualData);
		
	}
	
	

}