package com.alitalia.aem.consumer.checkinrest.render;

import java.util.ArrayList;
import java.util.List;

public class PnrRender {
   
	private String number;
	
    private List<FlightsRender> flightsRender = null;

    private Boolean roundTrip;

    private Boolean isVoloAlitalia;

	private Boolean insurance;
    
    private String checkinAllFlights;

    private List<SegmentRender> allsegments = new ArrayList<>();

	public Boolean getVoloAlitalia() {
		return isVoloAlitalia;
	}

	public void setVoloAlitalia(Boolean voloAlitalia) {
		isVoloAlitalia = voloAlitalia;
	}

	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public List<FlightsRender> getFlightsRender() {
		return flightsRender;
	}
	public void setFlightsRender(List<FlightsRender> flightsRender) {
		this.flightsRender = flightsRender;
	}
    public Boolean getRoundTrip() {
        return roundTrip;
    }
    public void setRoundTrip(Boolean roundTrip) {
        this.roundTrip = roundTrip;
    }
	public Boolean getInsurance() {
		return insurance;
	}
	public void setInsurance(Boolean insurance) {
		this.insurance = insurance;
	}
    public String getCheckinAllFlights() {
		return checkinAllFlights;
	}
	public void setCheckinAllFlights(String checkinAllFlights) {
		this.checkinAllFlights = checkinAllFlights;
	}

	public List<SegmentRender> getAllsegments() {
		return allsegments;
	}

	public void setAllsegments() {
		for(int i = 0;i<this.flightsRender.size();i++){
			for (int j = 0; j < this.flightsRender.get(i).getSegments().size();j++){
				this.allsegments.add(this.flightsRender.get(i).getSegments().get(j));
			}
		}
	}
}
