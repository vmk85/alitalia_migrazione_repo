package com.alitalia.aem.consumer.booking.analytics;

import java.math.BigDecimal;

public class AncillaryInfo {
	public String ancType;
	public BigDecimal ancPrice;
	public String ancName;
	
}
