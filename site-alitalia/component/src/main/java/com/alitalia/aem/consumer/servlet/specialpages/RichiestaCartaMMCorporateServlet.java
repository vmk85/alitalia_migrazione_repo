package com.alitalia.aem.consumer.servlet.specialpages;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.utils.AlitaliaCommonUtils;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.global.i18n.I18nKeySubscriptionSme;
import com.alitalia.aem.consumer.model.content.specialpages.RichiestaCartaMMCorporateData;
import com.alitalia.aem.consumer.model.content.substriptionsme.AdministratorAccount;
import com.alitalia.aem.consumer.model.content.substriptionsme.Area;
import com.alitalia.aem.consumer.model.content.substriptionsme.Company;
import com.alitalia.aem.consumer.model.content.substriptionsme.Preferences;
import com.alitalia.aem.consumer.model.content.substriptionsme.TravelAgency;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.ValidationUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "mmcorporatecardsubmit" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class RichiestaCartaMMCorporateServlet extends GenericFormValidatorServlet{
	
	// Static Property For Mails
	private static final String COMPONENT_NAME = "richiesta_carta_mm_c";
	private static final String PARSYS_NAME = "page-component";
	private static final String MAIL_FROM_PROPERTY_NAME = "mailFrom";
	private static final String MAIL_TO_PROPERTY_NAME = "mailTo";
	private static final String MAIL_SUBJECT_PROPERTY_NAME = "subject";
	private static final String MAIL_FROM_DEFAULT = "azcorporate@dataar.it";
	private static final String MAIL_TO_DEFAULT = "azcorporate@dataar.it";
	private static final String MAIL_SUBJECT_DEFAULT = "ISCRIZIONE CORPORATE";
	
	
	private static final String REQ_ATTR_CODICE_MILLEMIGLIA = "mm_code";
	private static final String REQ_ATTR_NOME = "appl_name";
	private static final String REQ_ATTR_COGNOME = "appl_surname";
	private static final String REQ_ATTR_EMAIL = "appl_email";
	private static final String REQ_ATTR_CODICE_AZIENDA = "company_code";
	private static final String REQ_ATTR_NAZIONE = "nation";
	private static final String REQ_ATTR_CHECK_1 = "auth-1";
	private static final String REQ_ATTR_CHECK_2 = "auth-2";
	private static final String REQ_ATTR_CHECK_3 = "auth-3";
	private static final String REQ_ATTR_CHECK_4 = "auth-4";
	
	private static final String CHECKBOX_VALUE = "yes";
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BusinessLoginService businessLoginService;
	
	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[RichiestaCartaCorporateRequestServlet] validateForm");
		
		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());

			
			/*boolean captchaAlreadyValidated = (Boolean)(request.getSession(true).getAttribute("captchaAlreadyValidatedSME") != null ? request.getSession(true).getAttribute("captchaAlreadyValidatedSME") : Boolean.FALSE);
			if(!captchaAlreadyValidated){ 
				String jcaptchaResponse = request.getParameter("captcha");
				String captchaId = request.getSession().getId();
				Boolean isResponseCorrect = ValidationUtils.isCaptchaValid(jcaptchaResponse, captchaId);
				
				if (!isResponseCorrect){
					ResultValidation resultValidation = new ResultValidation();
					resultValidation.setResult(isResponseCorrect);
					resultValidation.addField("captcha", "Valore non valido");
					return resultValidation;
				} else {
					request.getSession(true).setAttribute("captchaAlreadyValidatedSME", true);
					logger.debug("Captcha check passed");
				}
			}*/
			return validator.validate();
			
		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Errore durante la procedura di validazione dati "
					+ " Adesione Sme ", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		logger.debug("[RichiestaCartaCorporateRequestServlet] performSubmit");
		
		boolean mailResult = sendMail(request);
		logger.debug("Mail Sended = " + mailResult);
		if (mailResult) {
			goBackSuccessfully(request, response);
		}
		else {
			throw new IOException("Service Error");
		}
		
	}
	
	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {

		RichiestaCartaMMCorporateData data = populateData(request);

		//### add validate conditions to Company
		validator.addDirectCondition(REQ_ATTR_CODICE_MILLEMIGLIA, data.getCodiceMillemiglia()
				, I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addCrossCondition(REQ_ATTR_CODICE_MILLEMIGLIA, data.getCodiceMillemiglia(), "9"
				, I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "sizeIsLess");
		
		validator.addDirectCondition(REQ_ATTR_NOME, data.getNome()
				, I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(REQ_ATTR_NOME, data.getNome()
				, I18nKeySpecialPage.NAME_ERROR_NOT_VALID, "isAlphabeticWithAccentAndSpaces");
		
		validator.addDirectCondition(REQ_ATTR_COGNOME, data.getCognome()
				, I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(REQ_ATTR_COGNOME, data.getCognome()
				, I18nKeySpecialPage.SURNAME_ERROR_NOT_VALID, "isAlphabeticWithAccentAndSpaces");
		
		validator.addDirectCondition(REQ_ATTR_EMAIL, data.getEmail()
				, I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(REQ_ATTR_EMAIL, data.getEmail()
				, I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID, "isEmail");
		
		validator.addDirectCondition(REQ_ATTR_CODICE_AZIENDA, data.getCodiceAzienda()
				, I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		String[] countryList = RichiestaCartaMMCorporateData.extractCountryList(
				AlitaliaUtils.getRepositoryPathMarket(request.getResource()).toUpperCase()
				, configuration.getStringProperty(AlitaliaConfigurationHolder.MM_CORPORATE_COUNTRIES_MAP));
		
		if(countryList != null && countryList.length > 0){
			validator.addDirectCondition(REQ_ATTR_NAZIONE, data.getNazione(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.setAllowedValues(REQ_ATTR_NAZIONE, data.getNazione(), countryList, I18nKeyCommon.MESSAGE_INVALID_FIELD);
		}
		
		validator.addCrossCondition(REQ_ATTR_CHECK_1, data.getCheck1(), CHECKBOX_VALUE
				, I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "areEqual");
		validator.addCrossCondition(REQ_ATTR_CHECK_2, data.getCheck2(), CHECKBOX_VALUE
				, I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "areEqual");
		validator.addCrossCondition(REQ_ATTR_CHECK_3, data.getCheck3(), CHECKBOX_VALUE
				, I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "areEqual");
		
		return validator;
	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[RichiestaCartaCorporateRequestServlet] saveDataIntoSession");
		
		// create baggageClaimData object and save it into session
		RichiestaCartaMMCorporateData richiestaCartaCorporateRequestData = populateData(request);
		
		// go back to form with error
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		richiestaCartaCorporateRequestData.setError(i18n.get(I18nKeySpecialPage.ERROR_SERVICE));
		
		request.getSession().setAttribute(RichiestaCartaMMCorporateData.NAME,
				richiestaCartaCorporateRequestData);
	}
	
	/*
	 * sendMail
	 * 
	 */
	private boolean sendMail(SlingHttpServletRequest request) {
		logger.debug("[RichiestaCartaCorporateRequestServlet] sendMail");
		
		// current page
		Page currentPage = AlitaliaUtils.getPage(request.getResource());
		
		// email from
	    String mailFrom = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_FROM_PROPERTY_NAME);
	    if (mailFrom == null) {
	    	mailFrom = MAIL_FROM_DEFAULT;
	    }
	    
	    // email to
	    String mailTo = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_TO_PROPERTY_NAME);
	    if (mailTo == null) {
	    	mailTo = MAIL_TO_DEFAULT;
	    }
	    
	    // email subject
	    String subject = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_SUBJECT_PROPERTY_NAME);
	    if (subject == null) {
	    	subject = MAIL_SUBJECT_DEFAULT;
	    }

	    // email body
	    String messageText = generateBodyMail(request);
	    
	    logger.debug("[ RichiestaCartaCorporate] sendMail -- Email data: "
				+ "[From: {" + mailFrom + "}] - [To: {" + mailTo + "}] - "
				+ "[Subject: {" + subject + "}] - [Body: {" + messageText + "}]");
		
	    // create email message object
		EmailMessage emailMessage = new EmailMessage();
		emailMessage.setFrom(mailFrom);
		emailMessage.setTo(mailTo);
		emailMessage.setSubject(subject);
		emailMessage.setMessageText(messageText);
		emailMessage.setIsBodyHtml(false);
		emailMessage.setDeliveryNotification(DeliveryNotificationOptionsEnum.NONE);
		emailMessage.setPriority(MailPriorityEnum.NORMAL);

		// create AgencySendMailRequest
	    AgencySendMailRequest mailRequest = new AgencySendMailRequest();
		mailRequest.setTid(IDFactory.getTid());
		mailRequest.setSid(IDFactory.getSid(request));
		mailRequest.setEmailMessage(emailMessage);

		// get AgencySendMailResponse
		AgencySendMailResponse mailResponse =
				businessLoginService.sendMail(mailRequest);
		
		// return status
		return mailResponse.isSendMailSuccessful();
		
	}
	


	/*
	 * generateBodyMail
	 * 
	 */
	private String generateBodyMail(SlingHttpServletRequest request)  {
		logger.debug("[RichiestaCartaCorporateRequestServlet] generateBodyMail");
		
		String msg = "";
		RichiestaCartaMMCorporateData mmcorpReq = populateData(request);
		try {
				Map<String, Object> mapChild = AlitaliaCommonUtils.introspect(mmcorpReq);
				
				for(String kC : mapChild.keySet()){
					if( kC.equalsIgnoreCase("error") || kC.equalsIgnoreCase("name") || kC.equalsIgnoreCase("class"))
						continue;
					if(mapChild.get(kC) != null && !mapChild.get(kC).toString().isEmpty())
						msg += kC + ": " + mapChild.get(kC) + "\n";
				}
		} catch (Exception e) {
			logger.error("Errore durante la creazione del corpo della mail", e);
		}
		
		return msg;
		
	}
	

	private RichiestaCartaMMCorporateData populateData(SlingHttpServletRequest request){
		RichiestaCartaMMCorporateData richiestaCartaCorporateData = new RichiestaCartaMMCorporateData();
		
		//### get parameters from request
		richiestaCartaCorporateData.setCodiceMillemiglia(request.getParameter(REQ_ATTR_CODICE_MILLEMIGLIA));
		richiestaCartaCorporateData.setNome(request.getParameter(REQ_ATTR_NOME));
		richiestaCartaCorporateData.setCognome(request.getParameter(REQ_ATTR_COGNOME));
		richiestaCartaCorporateData.setEmail(request.getParameter(REQ_ATTR_EMAIL));
		richiestaCartaCorporateData.setCodiceAzienda(request.getParameter(REQ_ATTR_CODICE_AZIENDA));
		richiestaCartaCorporateData.setNazione(request.getParameter(REQ_ATTR_NAZIONE));
		richiestaCartaCorporateData.setCheck1(request.getParameter(REQ_ATTR_CHECK_1));
		richiestaCartaCorporateData.setCheck2(request.getParameter(REQ_ATTR_CHECK_2));
		richiestaCartaCorporateData.setCheck3(request.getParameter(REQ_ATTR_CHECK_3));
		richiestaCartaCorporateData.setCheck4(request.getParameter(REQ_ATTR_CHECK_4));

		return richiestaCartaCorporateData;
				
	}
	
	
	/*
	 * getComponentProperty
	 * 
	 */
	private String getComponentProperty(Page ancestor, String parsysName,
			String compName, String propName) {
		logger.debug("[BaggageClaimServlet] getComponentProperty");
		
		Resource pageResource = ancestor.getContentResource();	
		String prop = null;
		try{
			prop = pageResource.getChild(parsysName).getChild(compName)
					.getValueMap().get(propName, String.class);
		}
		catch(Exception e) {}
		
		return prop;
	}

}