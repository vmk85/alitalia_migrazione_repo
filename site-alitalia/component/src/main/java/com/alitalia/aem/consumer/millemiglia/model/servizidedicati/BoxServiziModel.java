package com.alitalia.aem.consumer.millemiglia.model.servizidedicati;


import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { Resource.class })
public class BoxServiziModel extends GenericBaseModel {
	
	@Self
	private Resource resource;
	
	private String htmlID;
	private static final String HTML_ID_PREFIX = "mmServizi-Box-";
	
	
	@PostConstruct
	protected void initModel() {
		htmlID = HTML_ID_PREFIX + AlitaliaUtils.getPage(resource).getName();
	}


	public String getHtmlID() {
		return htmlID;
	}

}
