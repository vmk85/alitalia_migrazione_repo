package com.alitalia.aem.consumer.millemiglia.model.messaggimiglia;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.alitalia.aem.common.data.home.MMMessageData;

public class MessaggiMillemigliaData implements Comparable<MessaggiMillemigliaData> {

	private boolean alreadyRead;

	private String description;
	private String text;
	private String title;

	private int sequenceNumber;
	private int messageNumber;

	private String endValidatDate;
	private String readDate;
	private String startValidatDate;

	private Long dateTimestamp;

	public Long getDateTimestamp() {
		return dateTimestamp;
	}

	public void setDateTimestamp(Long dateTimestamp) {
		this.dateTimestamp = dateTimestamp;
	}

	public boolean isAlreadyRead() {
		return alreadyRead;
	}

	public void setAlreadyRead(boolean alreadyRead) {
		this.alreadyRead = alreadyRead;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public int getMessageNumber() {
		return messageNumber;
	}

	public void setMessageNumber(int messageNumber) {
		this.messageNumber = messageNumber;
	}

	public String getEndValidatDate() {
		return endValidatDate;
	}

	public void setEndValidatDate(String endValidatDate) {
		this.endValidatDate = endValidatDate;
	}

	public String getReadDate() {
		return readDate;
	}

	public void setReadDate(String readDate) {
		this.readDate = readDate;
	}

	public String getStartValidatDate() {
		return startValidatDate;
	}

	public void setStartValidatDate(String startValidatDate) {
		this.startValidatDate = startValidatDate;
	}

	@Override
	public String toString() {
		return "MessaggiMillemigliaData [alreadyRead=" + alreadyRead + ", description=" + description + ", text=" + text + ", title=" + title + ", sequenceNumber=" + sequenceNumber + ", messageNumber=" + messageNumber + ", endValidatDate=" + endValidatDate + ", readDate=" + readDate + ", startValidatDate=" + startValidatDate + ", dateTimestamp=" + dateTimestamp + ", getDateTimestamp()=" + getDateTimestamp() + ", isAlreadyRead()=" + isAlreadyRead() + ", getDescription()=" + getDescription() + ", getText()=" + getText() + ", getTitle()=" + getTitle() + ", getSequenceNumber()=" + getSequenceNumber() + ", getMessageNumber()=" + getMessageNumber() + ", getEndValidatDate()=" + getEndValidatDate() + ", getReadDate()=" + getReadDate() + ", getStartValidatDate()=" + getStartValidatDate() + ", hashCode()=" + hashCode() + ", getClass()=" + getClass() + ", toString()=" + super.toString() + "]";
	}

	public MessaggiMillemigliaData(MMMessageData message) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
		this.alreadyRead = (message.getAlreadyRead() != null && message.getAlreadyRead().booleanValue() ? true : false);
		this.description = message.getDescription();
		this.text = message.getText();
		this.title = message.getTitle();
		this.sequenceNumber = (message.getSequenceNumber() != null ? message.getSequenceNumber().intValue() : 0);
		this.messageNumber = (message.getMessageNumber() != null ? message.getMessageNumber().intValue() : 0);
		this.endValidatDate = (message.getEndValidatDate() != null ? sdf.format(message.getEndValidatDate().getTime()) : "");
		this.readDate = (message.getReadDate() != null ? sdf.format(message.getReadDate().getTime()) : "");
		this.startValidatDate = (message.getStartValidatDate() != null ? sdf.format(message.getStartValidatDate().getTime()) : "");
		this.dateTimestamp = (message.getEndValidatDate() != null ? message.getEndValidatDate().getTime().getTime() : 0);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (alreadyRead ? 1231 : 1237);
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((endValidatDate == null) ? 0 : endValidatDate.hashCode());
		result = prime * result + messageNumber;
		result = prime * result + ((readDate == null) ? 0 : readDate.hashCode());
		result = prime * result + sequenceNumber;
		result = prime * result + ((startValidatDate == null) ? 0 : startValidatDate.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessaggiMillemigliaData other = (MessaggiMillemigliaData) obj;
		if (alreadyRead != other.alreadyRead)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (endValidatDate == null) {
			if (other.endValidatDate != null)
				return false;
		} else if (!endValidatDate.equals(other.endValidatDate))
			return false;
		if (messageNumber != other.messageNumber)
			return false;
		if (readDate == null) {
			if (other.readDate != null)
				return false;
		} else if (!readDate.equals(other.readDate))
			return false;
		if (sequenceNumber != other.sequenceNumber)
			return false;
		if (startValidatDate == null) {
			if (other.startValidatDate != null)
				return false;
		} else if (!startValidatDate.equals(other.startValidatDate))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public int compareTo(MessaggiMillemigliaData m) {
		String dateMex1 = this.getStartValidatDate();
		String dateMex2 = m.getStartValidatDate();

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
		Date date1;
		Date date2;
		try {
			date1 = format.parse(dateMex1);
			date2 = format.parse(dateMex2);
		} catch (ParseException e) {
			return 0;
		}

		int result = 0;
		if (date1.before(date2)) {
			result = -1;
		} else if (date1.after(date2)) {
			result = 1;
		}

		return result;
	}
}
