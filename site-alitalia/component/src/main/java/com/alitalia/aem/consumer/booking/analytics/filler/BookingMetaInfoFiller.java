package com.alitalia.aem.consumer.booking.analytics.filler;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSearchCategoryEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.analytics.MetaInfo;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.model.SearchPassengersNumber;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.DateUtils;

public class BookingMetaInfoFiller implements MetaInfoFiller{

	private static final float FROM_MILLIS_TO_DAYS = 1000 * 60 * 60 * 24;
	
	private Calendar now;

	public BookingMetaInfoFiller() {
		initFormats();
	}
	
	private void initFormats() {
		now = Calendar.getInstance();
		now.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
		
	}
	
	
	public MetaInfo fillInfo(BookingSessionContext ctx, int step, AlitaliaConfigurationHolder config){
		MetaInfo info = new MetaInfo();
		if(ctx.availableFlights != null){
		info.bbxSessionId = ctx.availableFlights.getSessionId();
			info.bbxSolutionSet = ctx.availableFlights.getSolutionSet();
		}
		if(ctx.cookie != null){
			info.sabreSessionId = computeSabreSessionID(ctx.cookie);
		}
		String currencySymbol = ctx.currency;
		info.revenuePhase = step;
		info.step = step;
		switch(step){
		case 1:
			/*Need encoding*/
			info.Pn = computeProductName(ctx.searchElements, true);
			info.tx_u = computePaxQuantity(ctx.searchPassengersNumber);
			info.ItinType = computeItinType(ctx.searchKind);
			/*Need encoding*/
			info.tratta = computeTratta(ctx.searchElements);
			/*Need encoding*/
			info.boApt = computeBoapt(ctx.searchElements);
			/*Need encoding*/
			info.arApt = computeArapt(ctx.searchElements);
			info.NumWE = "0";
			if (ctx.searchElements != null && !ctx.searchElements.isEmpty()) {
				info.departureDate_eu = ctx.searchElements.get(0).getDepartureDate();
				info.departureDate = ctx.searchElements.get(0).getDepartureDate();
				if(ctx.award){
					info.DateOfTravel = ctx.searchElements.get(0).getDepartureDate();
				}
				if (ctx.searchElements.size() > 1) {
					info.returnDate_eu = ctx.searchElements.get(1).getDepartureDate();
					info.returnDate = ctx.searchElements.get(1).getDepartureDate();
					info.NumWE = String.valueOf(DateUtils.computeWeekEndsNumber(ctx.searchElements.get(0).getDepartureDate()
							, ctx.searchElements.get(1).getDepartureDate()));
				}
			}
			info.numAdults = ctx.searchPassengersNumber.getNumAdults();
			info.numYoung = ctx.searchPassengersNumber.getNumYoung();
			info.numChildren = ctx.searchPassengersNumber.getNumChildren();
			info.numInfant = ctx.searchPassengersNumber.getNumInfants();
			info.NetworkExt = computeNetworkExt(ctx.searchCategory);
			if (ctx.searchElements != null && !ctx.searchElements.isEmpty()) {
				info.DayOfWeekA = DateUtils.computeRealDayOfWeek(
							ctx.searchElements.get(0).getDepartureDate().get(Calendar.DAY_OF_WEEK));
				info.deltaBoToday = DateUtils.computeDiffDays(now, 
							ctx.searchElements.get(0).getDepartureDate());
				if (ctx.searchElements.size() > 1 && step == 1) {
					info.DayOfWeekR = DateUtils.computeRealDayOfWeek(
							ctx.searchElements.get(1).getDepartureDate().get(Calendar.DAY_OF_WEEK));				
					info.deltaBoAr = DateUtils.computeDiffDays(ctx.searchElements.get(0).getDepartureDate()
							, ctx.searchElements.get(1).getDepartureDate());
				}
			}
			info.tx_e = "V";
			info.z_sitecode = ctx.site;
			info.z_tx_cur = currencySymbol != null && currencySymbol.length() > 0
						? currencySymbol : "EUR";
			info.Time = computeTime(now); 
			info.pn_gr = "Alitalia";
			info.pn_fa = "Alitalia";
			info.pn_sc = "Biglietto";
			info.pn_sku = "PNR";
			info.ServClass = "ECO";
			info.si_n = "Booking2013";
			info.BFormItem = "selecflight";
			info.pc = "Booking";
			info.SearchType = "Date Fisse";
			break;
		case 2:
			info.ItinType = computeItinType(ctx.searchKind);
			info.si_n = "Booking2013";
			info.Pn = computeProductName(ctx.searchElements, true);
			info.departureFlight= computeDepFlightNumber(ctx.flightSelections);
			info.returnFlight = computeRetFlightNumber(ctx.flightSelections);
			info.tx_u = computePaxQuantity(ctx.searchPassengersNumber);
			info.tratta = computeTratta(ctx.searchElements);
			info.FareBasis = computeFareBasis(ctx.selectionRoutes, ctx.searchKind);
			if (ctx.flightSelections != null) {
				FlightSelection departure = ctx.flightSelections[0];
				if (departure != null) {
					DirectFlightData flight;
					DirectFlightData lastFlight;
					if (departure.getFlightData() instanceof DirectFlightData) {
						flight = (DirectFlightData) departure.getFlightData();
						lastFlight = (DirectFlightData) departure.getFlightData();
					} else {
						flight = (DirectFlightData) ((ConnectingFlightData) departure.getFlightData()).getFlights().get(0);
						lastFlight = (DirectFlightData) ((ConnectingFlightData) departure.getFlightData()).getFlights().get(
								((ConnectingFlightData) departure.getFlightData()).getFlights().size() -1);
					}
					info.departureHouro = computeTime(flight.getDepartureDate());
					info.departureHourd = computeTime(lastFlight.getArrivalDate());
					BrandData brand = AlitaliaUtils.getBrandInfo(flight.getBrands(), departure.getSelectedBrandCode(), config.getMinifareSourceBrandCode(), config.getMinifareTargetBrandCode());
					if(brand != null){
						BrandPageData page =  ctx.codeBrandMap.get(departure.getSelectedBrandCode().toLowerCase());
						if(page != null){
							info.SolutionBrandA = page.getTitle();
						}
						else{
							info.SolutionBrandA = brand.getCode();
						}
					}
				}
				if (ctx.flightSelections.length > 1) {
					FlightSelection back = ctx.flightSelections[1];
					if (back != null) {
						DirectFlightData flight;
						DirectFlightData lastFlight;
						if (back.getFlightData() instanceof DirectFlightData) {
							flight = (DirectFlightData) back.getFlightData();
							
							lastFlight = (DirectFlightData) back.getFlightData();
						} else {
							flight = (DirectFlightData) ((ConnectingFlightData) back.getFlightData()).getFlights()
									.get(0);
							lastFlight = (DirectFlightData) ((ConnectingFlightData) back.getFlightData()).getFlights().get(
									((ConnectingFlightData) back.getFlightData()).getFlights().size() -1);
						}
						info.returnHouro = computeTime(flight.getDepartureDate());
						info.returnHourd = computeTime(lastFlight.getArrivalDate());
						BrandData brand = AlitaliaUtils.getBrandInfo(flight.getBrands(), back.getSelectedBrandCode(), config.getMinifareSourceBrandCode(), config.getMinifareTargetBrandCode());
						if(brand != null){
							BrandPageData page =  ctx.codeBrandMap.get(back.getSelectedBrandCode().toLowerCase());
							if(page != null){
								info.SolutionBrandR = page.getTitle();
							}
							else{
								info.SolutionBrandR = brand.getCode();
							}
						}
					}
				}
			}
			info.tx_e = "A";
			info.BFormItem = "infopassenger";
			info.z_sitecode = ctx.site;
			info.z_tx_cur = currencySymbol != null && currencySymbol.length() > 0
						? currencySymbol : "EUR";
			info.pn_gr = "Alitalia";
			info.pn_fa = "Alitalia";
			info.pc = "Booking";
			info.pn_sc = "Biglietto";
			info.pn_sku = "PNR";
			info.checknewsletter = "0";
			break;
		case 3:
			info.BFormItem = "customizeyourtravel";
			info.si_n = "Booking2013";
			info.ItinType = computeItinType(ctx.searchKind);
			break;
		case 4:
			info.ItinType = computeItinType(ctx.searchKind);
			if (ctx.flightSelections != null) {
				FlightSelection departure = ctx.flightSelections[0];
				if (departure != null) {
					DirectFlightData flight;
					if (departure.getFlightData() instanceof DirectFlightData) {
						flight = (DirectFlightData) departure.getFlightData();
					} else {
						flight = (DirectFlightData) ((ConnectingFlightData) departure.getFlightData()).getFlights().get(0);
					}
					BrandData brand = AlitaliaUtils.getBrandInfo(flight.getBrands(), departure.getSelectedBrandCode(), config.getMinifareSourceBrandCode(), config.getMinifareTargetBrandCode());
					if(brand != null){
						BrandPageData page =  ctx.codeBrandMap.get(departure.getSelectedBrandCode().toLowerCase());
						if(page != null){
							info.SolutionBrandA = page.getTitle();
						}
						else{
							info.SolutionBrandA = brand.getCode();
						}
					}
				}
				if (ctx.flightSelections.length > 1) {
					FlightSelection back = ctx.flightSelections[1];
					if (back != null) {
						DirectFlightData flight;
						if (back.getFlightData() instanceof DirectFlightData) {
							flight = (DirectFlightData) back.getFlightData();
						} else {
							flight = (DirectFlightData) ((ConnectingFlightData) back.getFlightData()).getFlights()
									.get(0);
						}
						BrandData brand = AlitaliaUtils.getBrandInfo(flight.getBrands(), back.getSelectedBrandCode(), config.getMinifareSourceBrandCode(), config.getMinifareTargetBrandCode());
						if(brand != null){
							BrandPageData page = ctx.codeBrandMap.get(back.getSelectedBrandCode().toLowerCase());
							if(page != null){
								info.SolutionBrandR = page.getTitle();
							}
							else{
								info.SolutionBrandR = brand.getCode();
							}
						}
					}
				}
			}
			info.si_n = "Booking2013";
			info.BFormItem = "payment";
			if(ctx.passengersData != null) {
				info.phonetel =  ctx.passengersData.getContact1() != null
						? ctx.passengersData.getContact1().getPhoneNumber() : "";
				/*Need encoding*/
				info.contact_name = (ctx.passengersData.getPassengersList() != null && !ctx.passengersData.getPassengersList().isEmpty())
						?  ctx.passengersData.getPassengersList().get(0).getName() + ";" + ctx.passengersData.getPassengersList().get(0).getSurname() : "";
				/*Need encoding*/
				info.dcsvid = ctx.passengersData.getEmail();
			}
			break;
		case 5:
			RoutesData prenotation = ctx.prenotation;
			info.Pn = computeProductName(ctx.searchElements, true);
			info.departureFlight= computeDepFlightNumber(ctx.flightSelections);
			info.returnFlight = computeRetFlightNumber(ctx.flightSelections);
			info.tx_u = computePaxQuantity(ctx.searchPassengersNumber);
			info.tratta = computeTratta(ctx.searchElements);
			info.FareBasis = computeFareBasis(ctx.selectionRoutes, ctx.searchKind);
			info.NumWE = "0";
			info.ItinType = computeItinType(ctx.searchKind);
			info.numAdults = ctx.searchPassengersNumber.getNumAdults();
			info.numYoung = ctx.searchPassengersNumber.getNumYoung();
			info.numChildren = ctx.searchPassengersNumber.getNumChildren();
			info.numInfant = ctx.searchPassengersNumber.getNumInfants();
			info.NetworkExt = computeNetworkExt(ctx.searchCategory);
			if (ctx.flightSelections != null) {
				FlightSelection departure = ctx.flightSelections[0];
				Calendar departureDateForDelta = Calendar.getInstance();
				Calendar depDate = now;
				if (departure != null) {
					DirectFlightData flight;
					if (departure.getFlightData() instanceof DirectFlightData) {
						flight = (DirectFlightData) departure.getFlightData();
					} else {
						flight = (DirectFlightData) ((ConnectingFlightData) departure.getFlightData()).getFlights().get(0);
					}
					depDate = flight.getDepartureDate();
					departureDateForDelta = flight.getDepartureDate();
					info.departureDate = depDate;
					info.DateOfTravel = flight.getDepartureDate();
					info.departureDate_eu = flight.getDepartureDate();
					info.deltaBoToday = DateUtils.computeDiffDays(now, flight.getDepartureDate());
					BrandData brand = AlitaliaUtils.getBrandInfo(flight.getBrands(), departure.getSelectedBrandCode(), config.getMinifareSourceBrandCode(), config.getMinifareTargetBrandCode());
					if(brand != null){
						BrandPageData page =  ctx.codeBrandMap.get(departure.getSelectedBrandCode().toLowerCase());
						if(page != null){
							info.SolutionBrandA = page.getTitle();
						}
						else{
							info.SolutionBrandA = brand.getCode();
						}
						info.USellingA = info.SolutionBrandA;
					}
				}
				if (ctx.flightSelections.length > 1) {
					FlightSelection back = ctx.flightSelections[1];
					if (back != null) {
						DirectFlightData flight;
						if (back.getFlightData() instanceof DirectFlightData) {
							flight = (DirectFlightData) back.getFlightData();
						} else {
							flight = (DirectFlightData) ((ConnectingFlightData) back.getFlightData()).getFlights()
									.get(0);
						}
						info.returnDate = flight.getDepartureDate();
						info.returnDate_eu = flight.getDepartureDate();
						info.NumWE = String.valueOf(DateUtils.computeWeekEndsNumber(depDate , flight.getDepartureDate()));
						info.deltaBoAr = DateUtils.computeDiffDays(departureDateForDelta, flight.getDepartureDate());
						BrandData brand = AlitaliaUtils.getBrandInfo(flight.getBrands(), back.getSelectedBrandCode(), config.getMinifareSourceBrandCode(), config.getMinifareTargetBrandCode());
						if(brand != null){
							BrandPageData page =  ctx.codeBrandMap.get(back.getSelectedBrandCode().toLowerCase());
							if(page != null){
								info.SolutionBrandR = page.getTitle();
							}
							else{
								info.SolutionBrandR = brand.getCode();
							}
							info.USellingR = info.SolutionBrandR;
						}
					}
				}
			}
			info.tx_e = "P";
			info.z_sitecode = ctx.site;
			info.z_tx_cur = currencySymbol != null && currencySymbol.length() > 0
						? currencySymbol : "EUR";
			info.Time = computeTime(now); 
			info.pn_gr = "Alitalia";
			info.pn_fa = "Alitalia";
			info.pn_sc = "Biglietto";
			info.pn_sku = "PNR";
			info.tx_id = now;
			info.tx_it = now;
			info.ServClass = "ECO";
			info.Pax_leg = "0";
			info.si_n = "Booking2013";
			info.pc = "Booking";
			info.SearchType = "Date Fisse";
			info.seg_2 = info.tratta;
			info.acqtratta = info.tratta;
			info.EResponse = "without";
			if(ctx.passengersData != null) {
				info.emailreceipt =  ctx.passengersData.getEmail();
			}
			info.ServFeeAmn = ctx.totalExtras;
			info.FareYQ = info.ServFeeAmn;
			info.z_tx_eur = ctx.grossAmount != null ? ctx.grossAmount : BigDecimal.ZERO;
			info.z_tx_txn = ctx.grossAmount != null ? ctx.grossAmount : BigDecimal.ZERO;
			info.tx_s = ctx.grossAmount != null ? ctx.grossAmount : BigDecimal.ZERO;
			info.FareAmn = ctx.netAmount != null ? ctx.netAmount : BigDecimal.ZERO;
			info.TaxesAmn = ctx.totalTaxes != null ? ctx.totalTaxes : BigDecimal.ZERO;
			if (prenotation != null) {
				info.tx_i = prenotation.getPnr();
				if (prenotation.getPayment() != null) {
					info.CCType = (prenotation.getPayment().getProvider() != null 
							&& prenotation.getPayment().getProvider() instanceof PaymentProviderCreditCardData) 
							? ((PaymentProviderCreditCardData)prenotation.getPayment().getProvider()).getType().value() : "";
				}
			}
			info.OrdMMUserType = "Standard";
			info.awardUser = false;
			info.mmUser = false;
			info.standardUser = false;
			if ( ctx.loggedUserProfileData != null ){
				if(ctx.award){
					info.OrdMMUserType = "Awarded";
					info.awardUser = true;
				}
				else{
					info.OrdMMUserType = "MMPaid";
					info.mmUser = true;
				}
			} else{
				info.standardUser = true;
			}
		}
		return info;
	}
	
	

	private String computeBoapt(List<SearchElement> searchElements) {
		String boapt = "";
		if (searchElements != null && !searchElements.isEmpty()) {
			boapt = searchElements.get(0).getFrom().getAirportCode();
		}
		return boapt;
	}
	
	private String computeArapt(List<SearchElement> searchElements) {
		String arapt = "";
		if (searchElements != null && !searchElements.isEmpty()) {
			arapt = searchElements.get(0).getTo().getAirportCode();
		}
		return arapt;
	}
	
	private String computeTratta(List<SearchElement> searchElements){
		String tratta = "";
		if (searchElements != null && !searchElements.isEmpty()) {
			String Boapt = searchElements.get(0).getFrom().getAirportCode();
			String Arapt = searchElements.get(0).getTo().getAirportCode();
			tratta = Boapt + "-" + Arapt;
		}
		return tratta;
	}
	
	
	private String computeProductName(List<SearchElement> searchElements, boolean includeTravelType){
		String pName = "";
		if (searchElements != null && !searchElements.isEmpty()) {
			pName += searchElements.get(0).getFrom().getLocalizedCityName() 
					+ ", " + searchElements.get(0).getFrom().getLocalizedAirportName() 
					+ ", " + searchElements.get(0).getFrom().getLocalizedCountryName();
			pName += "-" + searchElements.get(0).getTo().getLocalizedCityName() 
					+ ", " + searchElements.get(0).getTo().getLocalizedAirportName() 
					+ ", " + searchElements.get(0).getTo().getLocalizedCountryName();
			if(includeTravelType){
				pName += (searchElements.size() > 1 ? "-RoundTrip" : "-OneWay");
			}
		}
		return pName;
	}
	
	private String computeDepFlightNumber(FlightSelection[] selections){
		String fNum = "";
		if (selections != null) {
			FlightSelection departure = selections[0];
			if (departure != null) {
				DirectFlightData flight;
				if (departure.getFlightData() instanceof DirectFlightData) {
					flight = (DirectFlightData) departure.getFlightData();
				} else {
					flight = (DirectFlightData) ((ConnectingFlightData) departure.getFlightData()).getFlights().get(0);
				}
				fNum = flight.getFlightNumber();
				
			}
		}
		return fNum;
		
	}
	
	private String computeRetFlightNumber(FlightSelection[] selections){
		String fNum = "";
		if (selections != null && selections.length > 1){
			FlightSelection back = selections[1];
			if (back != null) {
				DirectFlightData flight;
				if (back.getFlightData() instanceof DirectFlightData) {
					flight = (DirectFlightData) back.getFlightData();
				} else {
					flight = (DirectFlightData) ((ConnectingFlightData) back.getFlightData()).getFlights()
							.get(0);
				}
				fNum = flight.getFlightNumber();
			}
		}
		return fNum;
		
	}
	
	private String computePaxQuantity(SearchPassengersNumber paxNumbers){
		String paxQt = "";
		if (paxNumbers != null) {
			paxQt = String.valueOf(paxNumbers.getNumAdults()
					+ paxNumbers.getNumYoung()
					+paxNumbers.getNumChildren()
					+paxNumbers.getNumInfants());
		}
		return paxQt;
	}
	
	private String computeItinType(BookingSearchKindEnum searchKind) {
		String travelType = "";
		if (searchKind != null) {
			travelType = searchKind == BookingSearchKindEnum.SIMPLE ? "OneWay" : "RoundTrip";
		}
		return travelType;
	}
	
	private String computeFareBasis(RoutesData route, BookingSearchKindEnum searchKind){
		String fareBasis = "";
		if(route != null){
			ResultBookingDetailsData details = ((ResultBookingDetailsData) route.getProperties().get("BookingDetails"));
			if(details != null){
				String depFareBasis = details.getSolutionField().get(0).getPricingField().get(0).getFareField().get(0).getExtendedFareCodeField();
				fareBasis += depFareBasis;
				if(!BookingSearchKindEnum.SIMPLE.equals(searchKind)){
					String retFareBasis = details.getSolutionField().get(0).getPricingField().get(0).getFareField().get(1).getExtendedFareCodeField();
					fareBasis += "," + retFareBasis;
				}
			}
		}
		return fareBasis;
	}
	
	private String computeNetworkExt(BookingSearchCategoryEnum searchCategory) {
		String networkExt = "";
		if(searchCategory != null){
			switch(searchCategory){
			case INTC:
			case INTC_PROMO:
				networkExt = "Intercontinental";
				break;
			case INTZ:
			case INTZ_PROMO:
				networkExt = "International";
				break;
			default:
				networkExt = "Domestic";
			}
		}
		return networkExt;
	}
	
	private String computeTime(Calendar now){
		int hh = now.get(Calendar.HOUR_OF_DAY);
		int mm = now.get(Calendar.MINUTE);
		String hour = String.valueOf(hh);
		String min = String.valueOf(mm);
		return (hh > 9 ? hour : "0" + hour) + ":" 
				+ (mm > 9 ? min : "0" + min);
	}
	
	
	private String computeSabreSessionID(String cookie) {
		String jsession = "";
		int index = cookie.indexOf("JSESSIONID=");
		if(index >= 0 && cookie.length() >= index + 11 + 32){
			jsession = cookie.substring(index + 11, index + 11 + 32);
			/*JSESSIONID vuoto o non valido*/
			if(jsession.matches(".*[\",.;=].*")){
				jsession = "";
			}
		}
		return jsession;
	}

}
