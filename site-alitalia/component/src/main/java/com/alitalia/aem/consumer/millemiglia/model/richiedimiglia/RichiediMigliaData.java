package com.alitalia.aem.consumer.millemiglia.model.richiedimiglia;

import java.util.List;

public class RichiediMigliaData {
	
	public static final String NAME = "RichiediMigliaData";
	
	private String numberCode;
	private String number;
	
	private List<RichiediMigliaMSG> messages;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getNumberCode() {
		return numberCode;
	}
	public void setNumberCode(String numberCode) {
		this.numberCode = numberCode;
	}
	public List<RichiediMigliaMSG> getMessages() {
		return messages;
	}
	public void setMessages(List<RichiediMigliaMSG> messages) {
		this.messages = messages;
	}
	@Override
	public String toString() {
		return "RichiediMigliaData [numberCode=" + numberCode + ", number="
				+ number + ", messages=" + messages + "]";
	}
	
	
	
}
