/** Classe di log per Errori MyAlitalia */

package com.alitalia.aem.consumer.myalitalia.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyAlitaliaErrorLog {

    private static Logger logger = LoggerFactory.getLogger(MyAlitaliaErrorLog.class);

    public void error(String log) {
        logger.error(log);
    }
}
