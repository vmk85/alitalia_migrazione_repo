package com.alitalia.aem.consumer.checkinrest.render.ancillary;

import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.AncillaryOffer;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;

/**
 * Created by ggadaleta on 29/01/2018.
 */
public class PassengerAncillaryRender {

    private AncillaryOffer ancillaryPassenger;
    private Passenger passengerData;

    public PassengerAncillaryRender(AncillaryOffer ancillaryPassenger, Passenger selectedPassengerData){
        this.ancillaryPassenger = ancillaryPassenger;
        this.passengerData = selectedPassengerData;
    }

    public AncillaryOffer getAncillaryPassenger() {
        return ancillaryPassenger;
    }

    public Passenger getPassengerData() {
        return passengerData;
    }
}
