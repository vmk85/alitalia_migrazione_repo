package com.alitalia.aem.consumer.checkinrest.listener;

import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkinrest.jobs.CheckinJobPublisher;
import org.apache.sling.event.jobs.JobManager;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.felix.scr.annotations.*;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component(immediate = true, metatype = false)
@Service
public class CheckinSessionListener implements HttpSessionListener {

    @Reference
    private JobManager jobManager;

    private static final Logger logger = LoggerFactory.getLogger(CheckinSessionListener.class);

    @Activate
    protected void activate(final ComponentContext context) {
    }

    @Deactivate
    protected void deactivate() {
    }

    @Override
    public void sessionCreated(final HttpSessionEvent httpSessionEvent) {
        String sessionID = httpSessionEvent.getSession().getId();
        logger.info("Alitalia HTTP session listener [{}] (sessionCreated)", sessionID);
    }

    @Override
    public void sessionDestroyed(final HttpSessionEvent httpSessionEvent) {
        HttpSession session = httpSessionEvent.getSession();
        String sessionID = session.getId();
        long elapsedTime = System.currentTimeMillis() - session.getLastAccessedTime();

// aggiungere tempo passato fino alla distruzione. Se la distruzione della sessione avviene entro i 15 minuti sara' un errore
        logger.info("Alitalia HTTP session listener [{}] (sessionDestroyed) - elapsed in ms: {}", sessionID, elapsedTime);
// statistiche su quali sessioni sono passate per il checkin
        CheckinSessionContext ctx = (CheckinSessionContext) httpSessionEvent.getSession().getAttribute(CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE);
        if (ctx == null) {
            logger.info("Alitalia HTTP session listener [{}] (sessionDestroyed) but ctx null", sessionID);
//            numero di errori non gestiti (dovuti a navigazioni all'infuori del checkin)
        } else {
            //analyze use case and publish job on jobManager
            if (!ctx.checkinComplete) {
                logger.info("Alitalia HTTP session listener [{}] (sessionDestroyed) abandoned session managed", sessionID);
//                numero di errori gestiti
                CheckinJobPublisher.Publish(
                        jobManager,
                        sessionID,
                        ctx
                );
            }

        }
    }

}