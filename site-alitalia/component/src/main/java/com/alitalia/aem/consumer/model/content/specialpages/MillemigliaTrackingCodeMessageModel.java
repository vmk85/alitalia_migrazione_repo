package com.alitalia.aem.consumer.model.content.specialpages;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.wcm.api.Page;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MillemigliaTrackingCodeMessageModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private MillemigliaRegistratiData millemigliaRegistratiData;
	private final String propertyName="trackcod";
	private String trackingCode; 
	private boolean showRichCustomeTextMMIscriviti=true;
	private boolean showTrackingCodeMessage=false;
	private static final String NAME_COMPONENT="tracking-code-message";
	@PostConstruct
	protected void initModel() {
		logger.debug("[MillemigliaTrackingCodeMessageModel] initModel");

		super.initBaseModel(slingHttpServletRequest);
		Resource component=	request.getResource().getChild(NAME_COMPONENT);
		 

		String trackingCodeFromDialog= request.getResource().getValueMap().get(propertyName,String.class);
	
		trackingCode = slingHttpServletRequest.getParameter("cod");
		if(trackingCode==null || trackingCode.isEmpty()){
			millemigliaRegistratiData = (MillemigliaRegistratiData) 
					slingHttpServletRequest.getSession()
					.getAttribute(MillemigliaRegistratiData.NAME);
			if(millemigliaRegistratiData!=null){
				trackingCode=millemigliaRegistratiData.getTrackingCod();	
			}
		}
		if(trackingCode != null && !trackingCode.isEmpty()){
			if(trackingCode.equalsIgnoreCase(trackingCodeFromDialog)){
				showTrackingCodeMessage=true;
				showRichCustomeTextMMIscriviti=false;				
			}
			logger.debug("TrackingCode: " + trackingCode);
			logger.debug("TrackingCodeFromDialog: " + trackingCodeFromDialog);
			}
		
		if(trackingCode!=null && component!=null){
		for(Resource comp:component.getChildren()){
			if(comp!=null){
				String prop=comp.getValueMap().get(propertyName, String.class);
				if(trackingCode.equals(prop)){
					showRichCustomeTextMMIscriviti=false;
				}
			}
			
		}
		}
		
		
		
	}

	public boolean showTrackingCodeMessage(){
		return showTrackingCodeMessage;
	}
	public String getTrackingCode(){
		return trackingCode;
	}

	public boolean showRichCustomeTextMMIscriviti(){
		return showRichCustomeTextMMIscriviti;
	}




	

	

}

