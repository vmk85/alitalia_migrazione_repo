package com.alitalia.aem.consumer.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.consumer.booking.BookingFlightFilterTypeEnum;
import com.alitalia.aem.consumer.booking.render.AvailableFlightsSelectionRender;

public class FlightDataComparator implements Comparator<AvailableFlightsSelectionRender> {
	
	private static final int MINUTES_FOR_DEAFULT_ORDER = 20;
	

	BookingFlightFilterTypeEnum orderType;


	private Locale locale;

	public FlightDataComparator(BookingFlightFilterTypeEnum orderType, Locale locale) {
		this.orderType = orderType;
		this.locale = locale;
	}

	@Override
	public int compare(AvailableFlightsSelectionRender flight1, AvailableFlightsSelectionRender flight2) {
		switch(orderType){
			case PRICE:
				double price1 = getPrice(flight1.getFlightData());
				double price2 = getPrice(flight2.getFlightData());
				return Double.compare(price1, price2);
				
			case DEPARTURE_TIME:
				return getDepartureDate(flight1.getFlightData()).compareTo(getDepartureDate(flight2.getFlightData()));
			case ARRIVAL_TIME:
				return getArrivalDate(flight1.getFlightData()).compareTo(getArrivalDate(flight2.getFlightData()));
			default:
				return FlightData.COMPARE_BY_DURATION_AND_DATE.compare(flight1.getFlightData(), flight2.getFlightData());
				
		}
	}
	
	private double getPrice(FlightData flight){
		List<BrandData> brandList = flight.getBrands();
		/*Search for best price*/
		BigDecimal bestPriceValue = new BigDecimal(0);
		for(BrandData brand : brandList){
			if (brand.isEnabled() 
					&& brand.getSeatsAvailable() > 0 
					&& (bestPriceValue.compareTo(new BigDecimal(0)) == 0 
						|| brand.getGrossFare().compareTo(bestPriceValue) < 0)) {
				bestPriceValue = brand.getGrossFare();
			}
		}
		return bestPriceValue.doubleValue();
	}
	
	private Calendar getDepartureDate(FlightData flight){
		Calendar departureDate;
		if (flight.getFlightType() == FlightTypeEnum.CONNECTING) {
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) flight;
			DirectFlightData directFlightData = (DirectFlightData) connectingFlightData.getFlights().get(0);
			departureDate = directFlightData.getDepartureDate();
			
		} else {
			DirectFlightData directFlightData = (DirectFlightData) flight;
			departureDate = directFlightData.getDepartureDate();
		}
		return departureDate;
	}
	
	private Calendar getArrivalDate(FlightData flight){
		Calendar arrivalDate;
		if (flight.getFlightType() == FlightTypeEnum.CONNECTING) {
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) flight;
			List<FlightData> flightList = connectingFlightData.getFlights();
			DirectFlightData directFlightData = (DirectFlightData) flightList.get(flightList.size()-1);
			arrivalDate = directFlightData.getArrivalDate();
			
		} else {
			DirectFlightData directFlightData = (DirectFlightData) flight;
			arrivalDate = directFlightData.getArrivalDate();
		}
		return arrivalDate;
	}

}
