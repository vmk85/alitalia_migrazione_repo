package com.alitalia.aem.consumer.validation;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.commons.annotation.Parameter;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.global.enumeration.LinguaCorrispondenza;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

@SuppressWarnings("unused")
public class Validator {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public static final String MESSAGE_GENERIC_ERROR = "Errore esecuzione metodo";

	private Map<String, List<Element>> directParameters = new HashMap<String, List<Element>>();
	private Map<String, List<Element>> crossParameters = new HashMap<String, List<Element>>();

	/**
	 * Add a new direct element to validate
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param message
	 *            error message
	 * @param methods
	 *            element methods (to execute)
	 */
	public void addDirectCondition(String key, String value, String message,
			String... methods) {
		String[] values = { value };
		addParameter(key, values, null, message, null, null, methods);
	}

	public void addDirectConditionWithParameter(String key, String value,
			String message, String method, String... parameters) {
		List<String> values = Arrays.asList(parameters);
		values.add(0, value);
		addParameter(key, values.toArray(new String[values.size()]), null,
				message, null, null, method);
	}

	/**
	 * Add a new direct element to validate with message pattern
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param messagePattern
	 *            error message with a pattern
	 * @param messageArgument
	 *            argument for message
	 * @param methods
	 *            element methods (to execute)
	 */
	public void addDirectConditionMessagePattern(String key, String value,
			String messagePattern, String messageArgument, String... methods) {
		String[] values = { value };
		addParameter(key, values, null, messagePattern, messageArgument, null,
				methods);
	}

	/**
	 * Add a new cross element to validate
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param comparingValue
	 *            element to compare to
	 * @param message
	 *            error message
	 * @param methods
	 *            element methods (to execute)
	 */
	public void addCrossCondition(String key, String value,
			String comparingValue, String message, String... methods) {
		String[] values = { value };
		addParameter(key, values, comparingValue, message, null, null, methods);
	}

	/**
	 * Add a new cross element to validate with message pattern
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param comparingValue
	 *            element to compare to
	 * @param messagePattern
	 *            error message with a pattern
	 * @param messageArgument
	 *            argument for message
	 * @param methods
	 *            element methods (to execute)
	 */
	public void addCrossConditionMessagePattern(String key, String value,
			String comparingValue, String messagePattern,
			String messageArgument, String... methods) {
		String[] values = { value };
		addParameter(key, values, comparingValue, messagePattern,
				messageArgument, null, methods);
	}

	/**
	 * Add a new element to validate against a set of allowed values
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param allowedValues
	 *            allowed values
	 * @param message
	 *            error message
	 */
	public void setAllowedValues(String key, String value,
			Object[] allowedValues, String message) {
		String[] values = { value };
		addParameter(key, values, null, message, null, allowedValues,
				new String[] {});
	}

	/**
	 * Add a new element to validate against a set of allowed values with
	 * message pattern
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param allowedValues
	 *            allowed values
	 * @param messagePattern
	 *            error message with a pattern
	 * @param messageArgument
	 *            argument for message
	 */
	public void setAllowedValuesMessagePattern(String key, String value,
			Object[] allowedValues, String messagePattern,
			String messageArgument) {
		String[] values = { value };
		addParameter(key, values, null, messagePattern, messageArgument,
				allowedValues, new String[] {});
	}

	/**
	 * Add a new element to validate with cross validation
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param comparingValue
	 *            element comparing value
	 * @param methods
	 *            element methods (to execute)
	 */
	private void addParameter(String key, String[] values,
			String comparingValue, String messagePattern,
			String messageArgument, Object[] allowedValues, String... methods) {

		// create parameter element
		Element e = new Element();

		// set value
		e.setValues(values);

		// set allowedValues OR methods
		if (allowedValues != null) {
			e.setAllowedValues(allowedValues);
		} else {
			ArrayList<String> methodsList = new ArrayList<String>();
			for (String method : methods) {
				methodsList.add(method);
			}
			e.setMethods(methodsList);
		}


		// set message
		if (messageArgument != null) {
			messagePattern = MessageFormat.format(messagePattern,
					messageArgument);
		}
		e.setMessage(messagePattern);

		Map<String, List<Element>> parameters = directParameters;

		// set comparing value
		if (comparingValue != null && allowedValues == null) {
			e.setComparingValue(comparingValue);
			parameters = crossParameters;
		}

		// set key
		if (parameters.containsKey(key)) {
			// use existing element
			List<Element> elementList = parameters.get(key);
			elementList.add(e);
			parameters.put(key, elementList);

		} else {
			// put new element
			ArrayList<Element> elementList = new ArrayList<Element>();
			elementList.add(e);
			parameters.put(key, elementList);
		}
	}

	/**
	 * Validate parameters
	 * 
	 * @return result of validation
	 */
	public ResultValidation validate() {

		// result of validation
		ResultValidation r = new ResultValidation();
		boolean valid = true;

		// cycle on all available keys
		Set<String> keySet = new HashSet<String>(directParameters.keySet());
		keySet.addAll(crossParameters.keySet());

		for (String key : keySet) {

			// result of directValidation
			boolean directValidation = true;

			// validate direct parameters
			if (directParameters.containsKey(key)) {

				// cycle on direct parameters
				List<Element> elementList = directParameters.get(key);
				for (Element e : elementList) {

					// validate parameter
					try {

						// check allowed values or use specific methods
						if ((e.getAllowedValues() != null && !this.isIn(
								e.getValues()[0], e.getAllowedValues()))
								|| (e.getAllowedValues() == null && !this
										.isValid(e.getMethods(), e.getValues()))) {

							// on error add to result and block cross
							// comparisons
							r.addField(key, e.getMessage());
							directValidation = false;
							logger.info("DIRECT_PARAMETER - ERROR - " + key + " - " + e.getMessage());
							// show only one error message
							break;
						}
					}

					// an error occurred...
					catch (Exception ex) {
						r.addField(key, MESSAGE_GENERIC_ERROR);
						logger.info("Errore validazione direct", ex);

						// block cross comparisons
						directValidation = false;

						// show only one error message
						break;
					}
				}
			}

			// validate cross parameters
			if (directValidation && crossParameters.containsKey(key)) {

				// cycle on cross parameters
				List<Element> elementList = crossParameters.get(key);
				for (Element e : elementList) {

					// validate parameter
					try {

						// use specific methods
						if (!this.areValid(e.getMethods(), e.getValues()[0],
								e.getComparingValue())) {

							// on error add to result
							r.addField(key, e.getMessage());
							logger.info("CROSS_PARAMETER - ERROR - " + key + " - " + e.getMessage());
							// show only one error message
							break;
						}
					}

					// an error occurred...
					catch (Exception ex) {
						r.addField(key, MESSAGE_GENERIC_ERROR);
						logger.info("Errore validazione cross", ex);

						// show only one error message
						break;
					}
				}
			}
		}

		// if there are some errors, then validation is NOK
		if (!r.getFields().entrySet().isEmpty()) {
			logger.info("SONO STATI RISCONTRATI ERRORI DI VALIDAZIONE");
			valid = false;
		}
		r.setResult(valid);
		logger.info("Result validation - " + r.toString());
		// return result of validation
		return r;
	}

	/**
	 * Check if parameter is valid against a list of checker methods
	 * 
	 * @param methods
	 *            list of checker methods
	 * @param value
	 *            values to check
	 * @return boolean TRUE if valid, FALSE otherwise
	 * @throws Exception
	 */
	private boolean isValid(List<String> methods, String... values)
			throws Exception {

		Method method;
		for (String m : methods) {
			method = this.getClass().getDeclaredMethod(m, String.class);

			// reflection on method
			if (!(boolean) method.invoke(this, (Object[]) values)) {
				// stop checking on wrong validation
				return false;
			}
		}

		// validation OK
		return true;
	}

	/**
	 * Check if value is allowed
	 * 
	 * @param value
	 *            value to check
	 * @param allowedValues
	 *            set of allowed values
	 * @return
	 */
	private boolean isIn(String value, Object[] allowedValues) {

		for (Object val : allowedValues) {
			if (val.toString().equalsIgnoreCase(value)) {
				// if is in then OK
				return true;
			}
		}

		// value not allowed
		return false;
	}

	/**
	 * Check if values are valid
	 * 
	 * @param methods
	 *            list of checker methods
	 * @param value0
	 *            first value to compare
	 * @param value1
	 *            second value to compare
	 * @return boolean TRUE if valid, FALSE otherwise
	 * @throws Exception
	 */
	private boolean areValid(List<String> methods, String value0, String value1)
			throws Exception {

		Method method;
		for (String m : methods) {
			method = this.getClass().getDeclaredMethod(m, String.class,
					String.class);

			// reflection on method
			if (!(boolean) method.invoke(this, value0, value1)) {
				// stop checking on wrong validation
				return false;
			}
		}

		// validation OK
		return true;
	}

	/*************************************************************************************************
	 * COMMON METHODS
	 ************************************************************************************************/

	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean isNotEmpty(String string) {
		return string != null && !string.isEmpty();
	}
	

	/**
	 * 
	 * @param value1
	 *           
	 * @param value2
	 *            
	 * @return
	 */
	private boolean isLessThan(String value1, String value2) {
		return (Integer.parseInt(value1) < Integer.parseInt(value2));
	};
	
	/**
	 * 
	 * @param value1
	 * @param value2
	 * @return
	 */
	private boolean isMoreThan(String value1, String value2) {
		return (Integer.parseInt(value1) > Integer.parseInt(value2));
	};

	
	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isNumber(String value) {
		String regex = "^[0-9]*$";
		return value == null || value.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isNumberWithSpace(String value) {
		String regex = "^[0-9\\s]*$";
		return value == null || value.matches(regex);
	}
	
	
	/**
	 *
	 * @param value
	 * @return
	 */
	private boolean isNumberWithDecimalPrecision(String value) {
		String regex = "^[0-9]+|[0-9]+[.,][0-9]{1,2}";
		return value == null || value.matches(regex);
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean isNumberExtended(String value) {
		String regex = "^[0-9\\-]*$";
		return value != null && value.matches(regex);
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean isAmount(String value) {
		String regex = "^\\d+(\\,{0,1}\\d{2})$";
		return value == null || value.matches(regex);
	}
	
	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean isInvoiceCode(String value) {
		String regex = "^[a-zA-Z0-9?!()-]*$";
		return value == null || value.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isAlphabetic(String value) {
		String regex = "^[a-zA-Z]*$";
		return value == null || value.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isAlphabeticWithSpaces(String value) {
		String regex = "^[a-zA-Z\\s]*$";
		return value == null || value.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isAlphabeticWithAccents(String value) {
		String regex = "^[a-zA-ZàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝ]*$";
		return value == null || value.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isAlphabeticWithAccentAndSpaces(String value) {
		String regex = "^[a-zA-Z\\sàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝ]*$";
		return value == null || value.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isAlphabeticWithAccentAndSpacesExtended(String value) {
		String regex = "^[a-zA-Z\\sàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝ\\-!?\\(\\)\\'\\.]*$";
		return value == null || value.matches(regex);
	}

	private boolean isAlphanumericWithAccentAndSpacesExtended(String value) {
		String regex = "^[a-zA-Z0-9\\sàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝ\\-!?\\(\\)\\'\\.]*$";
		return value == null || value.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isAlphaNumeric(String value) {
		String regex = "^[a-zA-Z0-9]*$";
		return value == null || value.matches(regex);
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean isMMUserName(String string) {
		String regex = "^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[A-Za-z0-9]).{8,40}$";
		return string == null || string.matches(regex);
	}

	private boolean isPasswordStrongAuthentication(String string) {

		if (string.length() < 8)
			return false; //alert("bad password");

		if (string.length() > 40)
			return false; //alert("bad password");

		Pattern p = Pattern.compile("[A-Z]");
		Matcher m = p.matcher(string);

		boolean hasUpperCase =  m.find();

		p = Pattern.compile("[a-z]");
		m = p.matcher(string);

		boolean hasLowerCase = m.find();

		p = Pattern.compile("\\d");
		m = p.matcher(string);

		boolean hasNumbers = m.find();

		/* AMS Bugfix #4846 Underscore non accettato nella password */
		//p = Pattern.compile("\\W");
		p = Pattern.compile("(?:\\W|_)");
		/* Fine bugfix #4846 */

		m = p.matcher(string);

		boolean hasNonalphas = m.find();

		/* AMS Bugfix #4846 Underscore non accettato nella password */
		//boolean totalMatch = string.matches( "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!-/:-@])(?=.*?[A-Za-z0-9!-/:-@]).{8,40}$");
		boolean totalMatch = string.matches( "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!-/:-@_])(?=.*?[A-Za-z0-9!-/:-@_]).{8,40}$");
		/* Fine bugfix #4846 */

//		boolean hasUpperCase =  string.matches( "[A-Z]");
//		boolean hasLowerCase = string.matches( "[a-z]");
//		boolean hasNumbers = string.matches( "\\d");
//		boolean hasNonalphas = string.matches( "\\W");

		//^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!-/:-@])(?=.*?[A-Za-z0-9!-/:-@]).{8,40}$



		if ((hasUpperCase && hasLowerCase && hasNumbers && hasNonalphas) || totalMatch )
			return true;
		else
			return false;  //alert("bad password");
	}


	private boolean isEmail(String string) {
		String regex = "^[A-Za-z0-9._-]+@[A-Za-z0-9._-]+\\.[A-Za-z]{2,8}$";
		return string == null || string.matches(regex);
	}

	private boolean isPromoCode(String string) {
		String regex = "^[A-Za-z]{2}[0-9]{8}$";
		return string == null || string.matches(regex);
	}

	private boolean isIataAptCode(String code){
		String regex = "^[A-Z]{3}$";
		return code == null || code.matches(regex);
	}

	/**
	 * 
	 * @param value1
	 * @param value2
	 * @return
	 */
	private boolean areEqual(String value1, String value2) {
		return value1 != null && value2 != null && value1.equals(value2);
	}

	/**
	 * 
	 * @param value1
	 * @param value2
	 * @return
	 */
	private boolean areDifferent(String value1, String value2) {
		return !areEqual(value1, value2);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isBeforeNow(String param) {
		String dateFormat = "dd/MM/yyyy";
		String[] params = param.split("\\|");
		String value = params[0];
		if (params.length > 1) {
			dateFormat = params[1];
		}
		SimpleDateFormat df = new SimpleDateFormat(dateFormat);
		df.setLenient(false);
		Calendar calendar = Calendar.getInstance();
		Calendar today = Calendar.getInstance();
		today.add(Calendar.DAY_OF_MONTH, 0);

		try {
			calendar.setTime(df.parse(value));
			if (calendar.after(today)) {
				return false;
			}
		} catch (ParseException e) {
			logger.warn("Error during parse date", e);
			return false;
		}

		return true;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isTrue(String value) {
		return Boolean.parseBoolean(value);
	}

	/**
	 * 
	 * @param value1
	 * @param value2
	 * @return
	 */
	private boolean sizeIsLess(String value1, String value2) {
		return value1.length() < Integer.parseInt(value2);
	}

	/**
	 * 
	 * @param value1
	 * @param value2
	 * @return
	 */
	private boolean sizeIsEqual(String value1, String value2) {
		return value1.length() == Integer.parseInt(value2);
	}
	
	/**
	 * 
	 * @param value1
	 * @param value2
	 * @return
	 */
	private boolean sizeIsBiggerOrEquals(String value1, String value2) {
		return value1.length() >= Integer.parseInt(value2);
	}

	/**
	 * 
	 * @param linguaCorrispondenza
	 * @param nazioneSelezionata
	 * @return
	 */
	private boolean isLinguaCorrispondenzaValid(String linguaCorrispondenza,
			String nazioneSelezionata) {
		if (nazioneSelezionata.equals("IT")
				&& !linguaCorrispondenza.equals(LinguaCorrispondenza.ITALIANO
						.value())) {
			return false;
		}
		return true;
	}
	/**
	 * controlla se una data e' nel formato "dd/MM/yy"
	 * TODO considerare anche il caso di data last minute es. "Giovedì, 15 Ottobre 2015"
	 * @param string
	 * @return
	 */
	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean isDate(String param) {
		if(isNotEmpty(param)){
			String dateFormat = "dd/MM/yyyy";
			String[] params = param.split("\\|");
			String value = params[0];
			if (params.length > 1) {
				dateFormat = params[1];
			}
			SimpleDateFormat df = new SimpleDateFormat(dateFormat);
			try {
				df.setLenient(false);
				df.parse(value);
			} catch (ParseException e) {
				logger.warn("Error during parse date", e);
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean isNumeroVolo(String string) {
		if (string.length() <= 4 && isNumber(string)) {
			return true;
		}
		return false;
	}
	
	/**
	 * controlla se una data e' futura. 
	 * TODO da considerare il fuso orario
	 * @param value
	 * @return 
	 */
	protected boolean isFuture(String param) {
		if(isNotEmpty(param)){
			String dateFormat = "dd/MM/yyyy";
			String[] params = param.split("\\|");
			String value = params[0];
			if (params.length > 1) {
				dateFormat = params[1];
			}
			SimpleDateFormat df = new SimpleDateFormat(dateFormat);
			df.setLenient(false);
			Calendar cal  = Calendar.getInstance();
			Calendar yesterday  = Calendar.getInstance();
			yesterday.add(Calendar.DAY_OF_MONTH, -1);
			
			try {
				cal.setTime(df.parse(value));
				if (cal.before(yesterday)){
					return false;
				}
			} catch (ParseException e) {
				logger.warn("Error during parse date", e);
				return false;
			}
		}
		return true;
	}
	
	/**
	 * confronto date
	 * @param start
	 * @param end
	 * @return
	 */
	private boolean beforeThen(String startP, String endP) {
		String dateFormat = "dd/MM/yyyy";
		String[] sParams = startP.split("\\|");
		String start = sParams[0];
		String[] eParams = endP.split("\\|");
		String end = eParams[0];
		if (sParams.length > 1) {
			dateFormat = sParams[1];
		}
		SimpleDateFormat df = new SimpleDateFormat(dateFormat);
		df.setLenient(false);
		try {
			Date date0 = df.parse(start);
			Date date1 = df.parse(end);

			if (date0.after(date1))
				return false;
		} catch (ParseException e) {
			logger.warn("Error during parse date", e);
			return false;
		}
		return true;
	}
	
	/**
	 * @param day data in formato gg/mm/aa
	 * @return true se oggi <= data <= oggi+7gg
	 * @throws ParseException
	 */
	private boolean isBeforeTodayPlusSevenDay(String day) throws ParseException{
			
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(new Date());
			
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(new Date());
			cal2.add(Calendar.DATE, 8);
			
			String dateFormat = "dd/MM/yyyy";
			String[] params = day.split("\\|");
			day = params[0];
			if (params.length > 1) {
				dateFormat = params[1];
			}
			SimpleDateFormat df = new SimpleDateFormat(dateFormat);
			df.setLenient(false);
			Date date = df.parse(day);
			Calendar cal = Calendar.getInstance();
			
			cal.setTime(date);
			
			return cal.after(cal1) && cal.before(cal2);
			
		}
	
	/**
	 * @param value
	 * @return 
	 * 
	 * Checking if value is a valid pnr number or a valid ticket number
	 */
	private boolean isPnrOrTicketNumber(String value) {
		//Check pnr number
		if (isAlphaNumeric(value) && value.length() == 6) {
			return true;
		}
		//Check ticket number
		if (isNumber(value) && value.length() == 13) {
			return true;
		}
		return false;
	}
	
	/**
	 * @param value
	 * @return 
	 * 
	 * Checking if value is a valid ticket number
	 */
	private boolean isTicketNumber(String value) {
		//Check ticket number
		if (isNumber(value) && value.length() == 13) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param value, max
	 * @return
	 */
	private boolean isLessThanMaxFrequentFlyers(String value, String max) {
		int parsedValue = Integer.parseInt(value);
		int parsedMax = Integer.parseInt(max);
		return (parsedValue <= parsedMax);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isRangeAdults(String value) {
		String regex = "^[1-7]{1}$";
		return value != null && value.matches(regex);
	}
	
	/**
	 * 
	 * @param value, max
	 * @return
	 */
	private boolean isValidFlightDate(String value) {
		if(isNotEmpty(value)){
			String dateFormat = "dd/MM/yyyy";
			String[] params = value.split("\\|");
			value = params[0];
			if (params.length > 1) {
				dateFormat = params[1];
			}
			SimpleDateFormat df = new SimpleDateFormat(dateFormat);
			df.setLenient(false);
			Calendar calendar = Calendar.getInstance();

			try {
				calendar.setTime(df.parse(value));
				
				Calendar lastDay = Calendar.getInstance();
				lastDay.add(Calendar.DAY_OF_MONTH, 7);
				Calendar firstDay = Calendar.getInstance();
				firstDay.add(Calendar.DAY_OF_MONTH, -3);
				
				if (calendar.after(lastDay) || calendar.before(firstDay)) {
					return false;
				}
			} catch (ParseException e) {
				logger.warn("Error during parse date", e);
				return false;
			}
		}
		
		
		return true;
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isRangeKids(String value) {
		String regex = "^[0-6]{1}$";
		return value != null && value.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isRangeBabies(String value) {
		String regex = "^[0-7]{1}$";
		return value != null && value.matches(regex);
	}
	
	/**
	 * 
	 * @param nadults
	 * @param nbabies
	 * @return
	 */
	private boolean babyPerAdult(String nadults, String nbabies) {
		return Integer.parseInt(nadults) >= Integer.parseInt(nbabies);
	}
	
	/**
	 * It checks if the adults ar in the range [2-7]
	 * @param nadults
	 * @return
	 */
	private boolean isRangeAdultForFamily(String nadults) {
		String regex = "^[2-7]{1}$";
		return nadults != null && nadults.matches(regex);
	}
	
	/**
	 * It checks if at least one kids or one babies is selected
	 * @param value
	 * @return
	 */
	private boolean isRangeKidsAndBabiesForFamily(String nkids, String nbabies) {
		return Integer.parseInt(nkids) > 0 || Integer.parseInt(nbabies) > 0;
	}
	
	/**
	 * 
	 * @param nadults
	 * @param nkids
	 * @return
	 */
	private boolean underMaxSumAdultsKids(String nadults, String nkids) {
		return Integer.parseInt(nadults) + Integer.parseInt(nkids) <= 7;
	}
	
	/**
	 * @param inputNames
	 * @param concatPassengerNames
	 * @return
	 */
	private boolean isNotEqualsPassengerNames(String inputNames, String concatPassengerNames){
		inputNames = inputNames.replaceAll(" ", "");
		String[] passengersNames = concatPassengerNames.split("\\|");
		int cont = 0;
		for (String name : passengersNames) {
			if (name.equalsIgnoreCase(inputNames)) {
				cont++;
			}
		}
		return (cont == 1);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isNumeroTelefonoValido(String value) {
		return value.matches("^[0-9]{5,15}");
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isNomeValido(String value) {
		return value.length() >= 2 && 
				value.matches("^[a-zA-ZÇüâäàåçêëèéïîìÄÅÉæÆôöòûùÿÖÜ£Ø×?áíóúñÑªº®¡ÁÂÀ©¢¥ãÃÐÊËÈiÍÎÏÌÓÔÒõÕµÚÛÙýÝ§°¹³²žŽšŠčČćĆÁáÀàÂâÃãÄäĄąĆćÇçÈèÉéÊêËëĘęÌìÍíÎîÏïJ́j́ŁłŃńÑñŐőÔôÒòÓóÔôÖöÕõŚśÚúŰűÙùÛûÜüŸÿÝýŹźŻż'\\s]{0,27}$");
// Aggiunti altri caratteri per il Bug "CaratteriSeciali"
//		value.matches("^[a-zA-ZÇüéâäàåçêëèéïîìÄÅÉæÆôöòûùÿÖÜ£Ø×?áíóúñÑªº®¡ÁÂÀ©¢¥ãÃÐÊËÈiÍÎÏÌÓÔÒõÕµÚÛÙýÝ§°¹³²žŽšŠčČćĆ'\\s]{0,27}$");
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isBeforeToday(String value) {
		Date now = new Date();
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
		df.setLenient(false);
		try {
			Date date = df.parse(value);
			if (date.after(now)) {
				return false;
			}
		} catch (ParseException e) {
			logger.warn("Error during parse date", e);
			return false;
		}

		return true;
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isCodiceBluebizValido(String value) {
		return value.matches("^[0-9]{6}$");
	}
	
	private boolean isCodiceSkyBonusValido(String value) {
		return value.matches("^((US|CA)\\d{7,9}|(ES|ER|EQ)\\d{3}|SME\\d{7})$");
	}
	
	/**
	 * Il secondo parametro 'params' contiene 2 parametri separati dal carattere '|' (pipe)
	 * Il primo contiene l'età massima, il secondo la data alla quale il passeggero non deve aver compiuto l'età massima
	 * 
	 * @param dataNascita
	 * @param params
	 * @return
	 */
	private boolean isAllowedAge(String dataNascita, String params) {
		String[] parametri = params.split("\\|");
		
		Long maxAge = Long.parseLong(parametri[0]);
		LocalDate lastDate =
				LocalDate.parse(parametri[1], DateTimeFormatter.ofPattern(AlitaliaConstants.REQUEST_DATE_FORMAT));
		LocalDate birthDate =
				LocalDate.parse(dataNascita, DateTimeFormatter.ofPattern(AlitaliaConstants.REQUEST_DATE_FORMAT));
		
		if (lastDate.isBefore(birthDate.plusYears(maxAge))) {
			return true;
		}
		return false;
	}
	
	/**
	 * Il secondo parametro 'params' contiene 2 parametri separati dal carattere '|' (pipe)
	 * Il primo contiene l'età minima, il secondo la data alla quale il passeggero non deve aver compiuto l'età min
	 * 
	 * @param dataNascita
	 * @param params
	 * @return
	 */
	private boolean isAllowedMinAge(String dataNascita, String params) {
		String[] parametri = params.split("\\|");
		
		Long minAge = Long.parseLong(parametri[0]);
		LocalDate lastDate =
				LocalDate.parse(parametri[1], DateTimeFormatter.ofPattern(AlitaliaConstants.REQUEST_DATE_FORMAT));
		LocalDate birthDate =
				LocalDate.parse(dataNascita, DateTimeFormatter.ofPattern(AlitaliaConstants.REQUEST_DATE_FORMAT));
		
		if (lastDate.isEqual(birthDate.plusYears(minAge)) || lastDate.isAfter(birthDate.plusYears(minAge))) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param code
	 * @return
	 */
	private boolean isCuitCuilCode(String code) {
		
		if(code.length() != 11) {
		return false;
	}
	
			  Integer accumulator   = 0;
			  String digits[]   = code.split("");
			  Integer digit  = Integer.parseInt(digits[digits.length-1]);

			  
			  for(Integer i = 0; i < digits.length - 1; i++) {
			   accumulator += Integer.parseInt(digits[9 - i]) * (2 + (i % 6));
			  }
			  
			  Integer verif = 11 - (accumulator % 11);
			  if(verif == 11) {
			   verif = 0;
			  } else if(verif == 10) {
			   verif = 9;
			  }
			  
			  return digit == verif;
              
	}
		

	
	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isMonth(String value) {
		return value != null && value.matches("[0-9][0-9]");
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isYear(String value) {
		return value != null && value.matches("[0-9][0-9][0-9][0-9]");
	}
	
	/**
	 * 
	 * @param validatorCF
	 * @return
	 */
	private boolean isValidCodiceFiscaleByConcatNomeCognomeCF(String validatorCF) {
		
		validatorCF = validatorCF.replace(" ","");
		validatorCF = validatorCF.toUpperCase();
		String[] params = validatorCF.split("\\|");
		String nome = params[0];
		String cognome = params[1];
		String CF = params[2];
		
		final List<String> vocali = new ArrayList<String>();
		final String[] vocArray = {"A","E","I","O","U"};
		for (String v : vocArray ) {
			vocali.add(v);
		}
		String cogToCompare = "";
		int cont = 0;
		
		if (CF.length() < 6) {
			return false;
		}
		if (nome == null || ("").equals(nome) || 
				cognome == null || ("").equals(cognome)) {
			return false;
		}
		
		/*cognome minore di 3 lettere*/
		if (cognome.length()<3){
			cogToCompare += cognome;
			while (cogToCompare.length()<3) {
				cogToCompare += "X";
			}
		} else {
		
			/*caso normale - le prime tre consonanti*/
			for (int i=0; i<cognome.length(); i++) {
				if (cont==3) {
					break;
				}
				if (!vocali.contains(Character.toString(cognome.charAt(i)))) {
					cogToCompare += cognome.charAt(i);
					cont++;
				}
			}
		
			/* caso meno di 3 consonanti*/
			while (cont<3) {
				for (int i=0; i<cognome.length(); i++) {
					if (cont==3) {
						break;
					}
					if (vocali.contains(Character.toString(cognome.charAt(i)))) {
						cogToCompare += cognome.charAt(i);
						cont++;
					}
				}
			}
		}
		
		/*lettere nome*/
		cont = 0;
		String nomToCompare = "";
		/*caso nome minore di 3 lettere*/
		if (nome.length()<3) {
			nomToCompare += nome;
			while (nomToCompare.length()<3) {
				nomToCompare+= "X";
			}
			cont=3;
		}
		
		String nomeCom = nome;
		for (String car : vocArray) {
			nomeCom = nomeCom.replaceAll(car, "");
		}
		if (nomeCom.length() >= 4) {
			/*caso normale consonanti >= 4 -- 1^ 3^ 4^ consonante*/
			char[] conInName = new char[3];
			conInName[0] =  nomeCom.charAt(0);
			conInName[1] =  nomeCom.charAt(2);
			conInName[2] =  nomeCom.charAt(3);
			nomToCompare = new String(conInName);
		} else if (nomeCom.length() == 3) {
			nomToCompare = nomeCom;
		} else {
			/* caso meno di 3 consonanti*/
			nomToCompare = nomeCom;
			cont = nomToCompare.length();
			for (int i=0;i<nome.length();i++) {
				if (cont==3) {
					break;
				}
				if (vocali.contains(Character.toString(nome.charAt(i)))) {
					nomToCompare += nome.charAt(i);
					cont++;
				}
			}
		}
		String cFCog = CF.substring(0, 3);
		String cFNome = CF.substring(3, 6);
		
		return (cFCog.equals(cogToCompare) && cFNome.equals(nomToCompare));
	}
	
	/**
	 * 
	 * @param piva
	 * @return
	 */
	private boolean isPartitaIVA(String piva){
		return piva.matches("^([0-9]){11}$");
	}
	
	/**
	 * 
	 * @param cvc
	 * @return
	 */
	private boolean isCVCFourDigits(String cvc){
		return cvc.length() == 4;
	}
	
	/**
	 * 
	 * @param cvc
	 * @return
	 */
	private boolean isCVCThreeDigits(String cvc){
		return cvc.length() == 3;
	}
	
	/**
	 * 
	 * @param airport
	 * @return
	 */
	private boolean isAirport(String value) {
		if (isAlphabetic(value))
			return true;
		else
			return false;
	}
	
}
