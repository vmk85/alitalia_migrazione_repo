package com.alitalia.aem.consumer.booking.model.extrabaggage;


/** 
 Common response class for deserializing stop airports i.e airport name, arrival and departure time
*/
public class StopAirport
{
	private String airport;
	public final String getairport()
	{
		return airport;
	}
	public final void setairport(String value)
	{
		airport = value;
	}
	private String arrival;
	public final String getarrival()
	{
		return arrival;
	}
	public final void setarrival(String value)
	{
		arrival = value;
	}
	private String departure;
	public final String getdeparture()
	{
		return departure;
	}
	public final void setdeparture(String value)
	{
		departure = value;
	}
}