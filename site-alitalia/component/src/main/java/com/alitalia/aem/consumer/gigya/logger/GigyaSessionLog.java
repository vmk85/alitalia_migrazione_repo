package com.alitalia.aem.consumer.gigya.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GigyaSessionLog implements GigyaLogger{

    private static Logger logger = LoggerFactory.getLogger(GigyaSessionLog.class);

    GigyaErrorLog gigyaErrorLog = new GigyaErrorLog();

    @Override
    public void error(String log) {
        logger.error(log);
        gigyaErrorLog.error(log);
    }

    @Override
    public void debug(String log) {
        logger.debug(log);
    }

    @Override
    public void info(String log) {
        logger.info(log);
    }

    @Override
    public void trace(String log) {
        logger.trace(log);
    }

}
