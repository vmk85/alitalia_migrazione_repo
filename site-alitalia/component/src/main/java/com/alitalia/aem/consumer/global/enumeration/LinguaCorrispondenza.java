package com.alitalia.aem.consumer.global.enumeration;

import java.util.ArrayList;

public enum LinguaCorrispondenza {
	ITALIANO("Italiano"),
	FRANCESE("Francese"),
	GIAPPONESE("Giapponese"),
	INGLESE("Inglese"),
	PORTOGHESE("Portoghese"),
	SPAGNOLO("Spagnolo"),
	TEDESCO("Tedesco");
	
	private final String value;

	LinguaCorrispondenza(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	public String getI18nValue() {
		return "millemiglia.linguaCorrispondenza." + value + ".label";
	}
	
	public static ArrayList<String> listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (LinguaCorrispondenza c: LinguaCorrispondenza.values()) {
			result.add(c.value);
		}
		return result;
	}

	public static LinguaCorrispondenza fromValue(String v) {
		for (LinguaCorrispondenza c: LinguaCorrispondenza.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
