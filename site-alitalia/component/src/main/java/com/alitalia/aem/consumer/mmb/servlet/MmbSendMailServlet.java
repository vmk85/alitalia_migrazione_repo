package com.alitalia.aem.consumer.mmb.servlet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.SlingRequestProcessor;

import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;
import com.alitalia.aem.common.messages.home.MmbSendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbSendEmailResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.web.component.managemybooking.delegate.ManageMyBookingDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.wcm.api.WCMMode;

@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "mmbsendmail" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class MmbSendMailServlet extends GenericMmbFormValidatorServlet {

	private static final String ERROR = "MMB Send Recap Mail Error";
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile ManageMyBookingDelegate mmbDelegate;
	
	@Reference
	private ResourceResolverFactory resolverFactory;

    /** Service to create HTTP Servlet requests and responses */
    @Reference
    private RequestResponseFactory requestResponseFactory;
    
    /** Service to process requests through Sling */
    @Reference
    private SlingRequestProcessor requestProcessor;
    
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		
		/* Recupero dati dalla request */
		String recipientEmail = request.getParameter("recipientEmail");

		
		/* Validazione */
		Validator validator = new Validator();
		
		validator.addDirectCondition("recipientEmail", recipientEmail, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("recipientEmail", recipientEmail, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isEmail");
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		try {
			
			String recipientEmail = request.getParameter("recipientEmail");
			
			MmbSessionContext ctx = getMmbSessionContext(request);
			
			/* Create BaseInfoData */
			MmbRequestBaseInfoData baseInfoData = new MmbRequestBaseInfoData();
			baseInfoData.setClientIp(ctx.callerIp);
			baseInfoData.setSessionId(ctx.sid);
			baseInfoData.setSiteCode(ctx.market);
			
			/* Create Send Mail Request */
			MmbSendEmailRequest mmbSendEmailRequest = new MmbSendEmailRequest();
			mmbSendEmailRequest.setBaseInfo(baseInfoData);
			mmbSendEmailRequest.setClient(ctx.serviceClient);
			mmbSendEmailRequest.setSid(ctx.sid);
			mmbSendEmailRequest.setSid(IDFactory.getTid());
			mmbSendEmailRequest.setPnr(ctx.pnr);
			mmbSendEmailRequest.setHtmlBody(true);
			mmbSendEmailRequest.setSender(configuration.getMailNoreplyAlitalia());
			mmbSendEmailRequest.setMailSubject(ctx.i18n.get("mmb.mail.subject.label"));
			mmbSendEmailRequest.setRecipient(recipientEmail);
			
			String mailBody = getMailBody(request, ctx, configuration.getBodyMailMmbPrenotazione(), false);
			if (mailBody == null || (mailBody != null && mailBody.equals(""))) {
				throw new IOException(ERROR);
			}
			mmbSendEmailRequest.setMailBody(mailBody);

			MmbSendEmailResponse mmbSendEmailResponse = mmbDelegate.sendMail(mmbSendEmailRequest);
			
			if (!mmbSendEmailResponse.isMailSent()) {
				logger.error("[MmbSendMailServlet] ERRORE INVIO EMAIL MMB PRENOTAZIONE");
				throw new IOException(ERROR);
			}
			
		} catch (Exception e) {
			logger.error("[MmbSendMailServlet] Exception during send email " + e.getMessage(), e);
			throw new IOException(e.getMessage());
		}
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value("OK");
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
	
	/* Internal privates utility method */
	
	/**
	 * @param resource The resource of the 
	 * @param templateMail Path of email template
	 * @param secure (Unused)
	 * @return The email body
	 */
	private String getMailBody(SlingHttpServletRequest request, MmbSessionContext ctx, String templateMail, 
			boolean secure) {

		Resource resource = request.getResource();
		
		String result = null;
		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(resource, false);
		try {

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			/* Get resource resolver */
			ResourceResolver resourceResolver = request.getResourceResolver(); 
			String requestedUrl = baseUrl + AlitaliaConstants.BASE_TEMPLATE_EMAIL_PATH + "/" + templateMail;

			/* Setup request */
			HttpServletRequest req = requestResponseFactory.createRequest("GET", requestedUrl);
			req.setAttribute(MmbConstants.MMB_CONTEXT_ATTRIBUTE, ctx);
			WCMMode.DISABLED.toRequest(req);

			out = new ByteArrayOutputStream();
			HttpServletResponse resp = requestResponseFactory.createResponse(out);

			/* Process request through Sling */
			requestProcessor.processRequest(req, resp, resourceResolver);

			String encoding = "UTF-8";
			result = new String(out.toByteArray(), Charset.forName(encoding));

		} catch (ServletException | IOException e) {
			logger.error("[MmbSendMailServlet] Generic exception while retrieving email template from JCR repository: {}", e);
			result = "";
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("[MmbSendMailServlet] Template retrieved and placeholders replaced: {} ", result);
		}
		
		return result;
	}
	
}
