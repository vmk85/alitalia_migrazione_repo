package com.alitalia.aem.consumer.booking.controller;



import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.booking.render.GenericPriceRender;


@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingInsurance extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	
	private String insurancePrice;
	private String error;
	private Boolean enabled;

	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			
			initBaseModel(request);
			if (ctx == null){
				return;
			}
			
			enabled = ctx.insuranceEnabled;
			if (enabled) {
				if (ctx.insuranceProposalData == null 
						|| ctx.insuranceProposalData.getTotalInsuranceCost() == null) {
					logger.error("Check service 'getInsurance' response. insuranceProposalData or insuranceProposalData.getTotalInsuranceCost() is NULL");
					enabled = false;
					insurancePrice = "";
				} else {
					GenericPriceRender genericPriceRender = new GenericPriceRender(ctx.insuranceProposalData.getTotalInsuranceCost(), ctx.currency, ctx);
					insurancePrice = genericPriceRender.getFare();
				}
			}
			else {
				insurancePrice = "";
			}
			
			
		} catch (Exception e) {
			logger.error("Unexpected error", e);
			throw e;
		}
	}
	
	
	
	public String getInsurancePrice() {
		return insurancePrice;
	}
	
	public String getError() {
		return error;
	}
	
	public Boolean getEnabled() {
		return enabled;
	}
	
}
