package com.alitalia.aem.consumer.bookingaward.servlet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSearchTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.model.SearchElementAirport;
import com.alitalia.aem.consumer.booking.model.SearchPassengersNumber;
import com.alitalia.aem.consumer.bookingaward.BookingAwardSession;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.TypeUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingawardconfirmdate" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class BookingAwardConfirmDateServlet extends GenericBookingAwardFormValidatorServlet {
	
	@Reference
    private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private BookingAwardSession bookingAwardSession;
	
	private static final String STANDARD_REQUEST_DATE_FORMAT = "dd/MM/yyyy";
	
	private static final String DATE_FORMAT_USA = "USA";
	
	
	private static final String ATTR_DEPARTURE_DATE = "bookingAward_andata";
	private static final String ATTR_RETURN_DATE = "bookingAward_ritorno";
	private static final String ATTR_ANCHOR_ERRORS = "dateChosen";
	private static final String ATTR_SEARCH_TYPE = "SearchType";
	private static final String ATTR_DATE_FORMAT ="dateFormat";
	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {

		Validator validator = new Validator();
		BookingSessionContext ctx = getBookingSessionContext(request);
		Boolean exist;

		/*// start date
		String dateFormat = AlitaliaUtils.getDateFormatByMarket(request.getResource());
		String addParamDate = "";
		if (dateFormat != null) {
			addParamDate = "|" + dateFormat;
		}*/
		String format = request.getParameter(ATTR_DATE_FORMAT);
		String start = request.getParameter(ATTR_DEPARTURE_DATE);
		if (DATE_FORMAT_USA.equals(format) && start.length()>6) {
			start = start.substring(3,5) + "/" + start.substring(0,2) + "/" + start.substring(6);
		}
		validator.addDirectCondition(ATTR_ANCHOR_ERRORS,
				start,
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");		
		validator.addDirectCondition(ATTR_ANCHOR_ERRORS, start,
				BookingConstants.MESSAGE_DATE_PAST, "isDate", "isFuture");
		exist = bookingAwardSession.existFlights(ctx, start, 0);
		if (!exist) {
			validator.addDirectCondition(ATTR_ANCHOR_ERRORS,
					"false",
					BookingConstants.MESSAGE_FLIGHTS_NOT_FOUND, "isTrue");
		}


		// check if start date is before end date
		if (!BookingSearchTypeEnum.isOneWay(request.getParameter(ATTR_SEARCH_TYPE))) {
		
			// end date
			String end = request.getParameter(ATTR_RETURN_DATE);
			if (DATE_FORMAT_USA.equals(format) && end.length()>6) {
				end = end.substring(3,5) + "/" + end.substring(0,2) + "/" + end.substring(6);
			}
			
			validator.addDirectCondition(ATTR_ANCHOR_ERRORS, end,
					BookingConstants.MESSAGE_DATE_PAST, "isDate", "isFuture");
			validator.addCrossCondition(ATTR_ANCHOR_ERRORS, start,
					end, BookingConstants.MESSAGE_DATE_PAST,
					"beforeThen");
			exist = bookingAwardSession.existFlights(ctx, end, 1);
			if (!exist) {
				validator.addDirectCondition(ATTR_ANCHOR_ERRORS,
						"false",
						BookingConstants.MESSAGE_FLIGHTS_NOT_FOUND, "isTrue");
			}
		}
		
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		String start = request.getParameter(ATTR_DEPARTURE_DATE);
		String end = request.getParameter(ATTR_RETURN_DATE);
		String format = request.getParameter(ATTR_DATE_FORMAT);
		String departureDate = start;
		String returnDate = end;
		
		BookingSessionContext ctx = getBookingSessionContext(request);
		BookingSearchKindEnum searchKind = ctx.searchKind;
		
		if (DATE_FORMAT_USA.equals(format) && start.length()>6) {
			departureDate = start.substring(3,5) + "/" + start.substring(0,2) + "/" + start.substring(6);
		}
		
		if (searchKind == BookingSearchKindEnum.ROUNDTRIP && DATE_FORMAT_USA.equals(format) && end.length()>6) {
			returnDate = end.substring(3,5) + "/" + end.substring(0,2) + "/" + end.substring(6);
		}
		
		
	
		SimpleDateFormat dateFormat = new SimpleDateFormat(STANDARD_REQUEST_DATE_FORMAT);
		dateFormat.setLenient(false);
		
		List<SearchElement> searchElements = ctx.searchElements;
		List<SearchElement> newElements = new ArrayList<SearchElement>();
		
		SearchElement departureElement = searchElements.get(0);
		SearchElement newDepartureElement = new SearchElement(
				departureElement.getFrom(), departureElement.getTo(), TypeUtils.createCalendar(dateFormat.parse(departureDate)));
		
		newElements.add(newDepartureElement);
		
		if (searchKind == BookingSearchKindEnum.ROUNDTRIP) {
			SearchElement returnElement = searchElements.get(1);
			SearchElement newReturnElement = new SearchElement(
					returnElement.getFrom(), returnElement.getTo(), TypeUtils.createCalendar(dateFormat.parse(returnDate)));
			newElements.add(newReturnElement);
		}
		
		bookingAwardSession.updateSearch(ctx, newElements);
		
		// redirect to the correct page
		String successPage = configuration.getBookingAwardFlightSelectPage();
		if (successPage != null && !"".equals(successPage)) {
			Resource resource = request.getResource();
			String successUrl = resource.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(resource, false)) + successPage;
			response.sendRedirect(successUrl);
		}
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}
