package com.alitalia.aem.consumer.carnet.model;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;



import com.alitalia.aem.consumer.carnet.data.RecuperoPasswordData;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables={SlingHttpServletRequest.class})
public class CarnetRecuperoPasswordModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;
	
	private RecuperoPasswordData recuperoPasswordData;

	
	@PostConstruct
	protected void initModel() {
		logger.debug("[recuperoPasswordCarnetModel] initModel");
		super.initBaseModel(slingHttpServletRequest);
	
		recuperoPasswordData = (RecuperoPasswordData) slingHttpServletRequest.getSession().getAttribute(RecuperoPasswordData.NAME);
		slingHttpServletRequest.getSession().removeAttribute(RecuperoPasswordData.NAME);
	}

	public RecuperoPasswordData getRecuperoPasswordData() {
		return recuperoPasswordData;
	}
}
