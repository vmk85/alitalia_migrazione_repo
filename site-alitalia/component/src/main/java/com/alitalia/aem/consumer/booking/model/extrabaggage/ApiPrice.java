package com.alitalia.aem.consumer.booking.model.extrabaggage;


import java.util.*;

/** 
 Sabre Request Model for ApiPrice
*/
public class ApiPrice
{
	private ArrayList<ArrayList<Amount>> alternatives;
	public final ArrayList<ArrayList<Amount>> getalternatives()
	{
		return alternatives;
	}
	public final void setalternatives(ArrayList<ArrayList<Amount>> value)
	{
		alternatives = value;
	}
}