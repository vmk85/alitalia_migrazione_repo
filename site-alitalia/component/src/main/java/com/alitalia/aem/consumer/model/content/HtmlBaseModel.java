package com.alitalia.aem.consumer.model.content;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { Resource.class })
public class HtmlBaseModel {

	@Self
	private Resource resource;

	private Logger logger = LoggerFactory.getLogger(HtmlBaseModel.class);
	
	private String langCountry;
	
	@PostConstruct
	protected void initModel() {
		
		try{
						
			String country = AlitaliaUtils.getRepositoryPathMarket(resource);
			String language = AlitaliaUtils.getRepositoryPathLanguage(resource);
			
			langCountry = language + "-" + country;
			
		} catch (Exception e){
			logger.error("unexpected error", e);
		}
		
	}

	public String getLangCountry() {
		return langCountry;
	}

}