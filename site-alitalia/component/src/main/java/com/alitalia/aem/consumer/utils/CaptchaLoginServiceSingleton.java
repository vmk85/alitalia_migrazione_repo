package com.alitalia.aem.consumer.utils;

import java.awt.Color;
import java.awt.Font;

import com.octo.captcha.component.image.backgroundgenerator.BackgroundGenerator;
import com.octo.captcha.component.image.backgroundgenerator.UniColorBackgroundGenerator;
import com.octo.captcha.component.image.color.RandomRangeColorGenerator;
import com.octo.captcha.component.image.fontgenerator.FontGenerator;
import com.octo.captcha.component.image.fontgenerator.RandomFontGenerator;
import com.octo.captcha.component.image.textpaster.RandomTextPaster;
import com.octo.captcha.component.image.textpaster.TextPaster;
import com.octo.captcha.component.image.wordtoimage.ComposedWordToImage;
import com.octo.captcha.component.image.wordtoimage.WordToImage;
import com.octo.captcha.component.word.wordgenerator.RandomWordGenerator;
import com.octo.captcha.component.word.wordgenerator.WordGenerator;
import com.octo.captcha.engine.image.ListImageCaptchaEngine;
import com.octo.captcha.image.gimpy.GimpyFactory;
import com.octo.captcha.service.captchastore.FastHashMapCaptchaStore;
import com.octo.captcha.service.image.DefaultManageableImageCaptchaService;
import com.octo.captcha.service.image.ImageCaptchaService;

public class CaptchaLoginServiceSingleton {

	private static ImageCaptchaService instance = null;

	// Metodo della classe impiegato per accedere al singleton
	public static synchronized ImageCaptchaService getInstance() {
		if (instance == null) {
			instance = new DefaultManageableImageCaptchaService(
					new FastHashMapCaptchaStore(), new MyImageCaptchaEngine(), 180, 100000, 75000);
		}
		return instance;
	}

	private static class MyImageCaptchaEngine extends ListImageCaptchaEngine {
		
		@Override
		protected void buildInitialFactories() {
			WordGenerator wgen = new RandomWordGenerator("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789");
			RandomRangeColorGenerator cgen = new RandomRangeColorGenerator(new int[] { 0, 5 }, new int[] { 0, 5 }, new int[] { 0, 5 });

			TextPaster textPaster = new RandomTextPaster(new Integer(6), new Integer(6), cgen, Boolean.TRUE);

			BackgroundGenerator backgroundGenerator = new UniColorBackgroundGenerator(new Integer(115), new Integer(39), new Color(230,237,228));

			Font font1 = new Font("TimesRoman", Font.PLAIN, 17);
			Font font2 = new Font("Arial", Font.PLAIN, 17);
			Font font3 = new Font("Courier", Font.PLAIN, 17);
			FontGenerator fontGenerator = new RandomFontGenerator(new Integer(16), new Integer(18), new Font[]{font1, font2, font3});
			WordToImage wordToImage = new ComposedWordToImage(fontGenerator, backgroundGenerator, textPaster);
			this.addFactory(new GimpyFactory(wgen, wordToImage));
		}
	}
}