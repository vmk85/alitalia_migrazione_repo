package com.alitalia.aem.consumer.checkinrest.controller;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = {SlingHttpServletRequest.class})
public class CheckinMailImages extends GenericBaseModel {

    @Self
    private SlingHttpServletRequest request;

    @Inject
    private AlitaliaConfigurationHolder configuration;

    private final static String LOGO_NAME = "logoAZ_Skyteam.png";
    private final static String FB = "facebook.png";
    private final static String TWITTER = "twitter.png";
    private final static String INSTAGRAM = "instagram.png";
    private final static String LINKEDIN = "linkedin.png";
    private final static String YT = "youtube.png";

    private String imageLogo;
    private String imageFBLogo;
    private String imageTWITTERLogo;
    private String imageINSTAGRAMLogo;
    private String imageLINKEDINLogo;
    private String imageYTLogo;

    private String img;
    private String img1;
    private String img2;


    @PostConstruct
    protected void initModel() {

        initBaseModel(request);
        String prefix = "http://";
        if (configuration.getHttpsEnabled())
            prefix = "https://";

        String path = prefix + "www.alitalia.com" + configuration.getStaticImagesPath1();

        imageLogo = path + LOGO_NAME;
        imageFBLogo = path + FB;
        imageTWITTERLogo = path + TWITTER;
        imageINSTAGRAMLogo = path + INSTAGRAM;
        imageLINKEDINLogo = path + LINKEDIN;
        imageYTLogo = path + YT;

			if (request.getResource().getValueMap().get("fileReference") != null) {
            img = prefix + configuration.getExternalDomain() + request.getResource().getValueMap().get("fileReference").toString();
			}
			if (request.getResource().getValueMap().get("fileReference1") != null) {

            img1 = prefix + configuration.getExternalDomain() + request.getResource().getValueMap().get("fileReference1").toString();
			}
			if (request.getResource().getValueMap().get("fileReference2") != null) {

            img2 = prefix + configuration.getExternalDomain() + request.getResource().getValueMap().get("fileReference2").toString();
			}
		}

    public String getImageLogo() {
        return imageLogo;
    }

    public String getImageFBLogo() {
        return imageFBLogo;
    }

    public String getImageTWITTERLogo() {
        return imageTWITTERLogo;
    }

    public String getImageINSTAGRAMLogo() {
        return imageINSTAGRAMLogo;
    }

    public String getImageLINKEDINLogo() {
        return imageLINKEDINLogo;
    }

    public String getImageYTLogo() {
        return imageYTLogo;
    }

    public String getImg() {
        return img;
    }

    public String getImg1() {
        return img1;
    }

    public String getImg2() {
        return img2;
    }
}


