package com.alitalia.aem.consumer.checkin;

public class CheckinConstants {

	public static final String CHECKIN_CONTEXT_ATTRIBUTE = "alitaliaConsumerCheckInSessionContext";

	public static final String CHECKIN_SID = "alitaliaConsumerCheckin";
	public static final String CHECKIN_CLIENT = "alitaliaConsumerSiteCheckin";
	public static final String CHECKIN_SITE = "site";

	public static final String CHECKIN_TEMPLATE_EMAIL_ANCILLARY = "ancillary-email.html";
	public static final String CHECKIN_ANCILLARY_EMAIL_SUBJECT = "checkin.ancillary.email.subject";
	public static final String CHECKIN_ANCILLARY_EMAIL_ATTACHMENT = "checkin.ancillary.email.attachment";

	public static final String CHECKIN_TEMPLATE_EMAIL_FOLDER = "template-email";
	public static final String CHECKIN_TEMPLATE_EMAIL_BOARDING_PASS = "boarding-pass-email.html";
	public static final String CHECKIN_TEMPLATE_EMAIL_RECAP = "checkin_recap_1.html";
	public static final String CHECKIN_TEMPLATE_EMAIL_BOARDING_PASS_NEW = "checkin_bp_1.html";
	public static final String CHECKIN_TEMPLATE_EMAIL_CUMULATIVE = "checkin_recap.html";
	public static final String CHECKIN_TEMPLATE_EMAIL_REFUSED = "checkin-refused.html";
	public static final String CHECKIN_MAIL_REFUSED_TICKET = "checkin.mail.notchecked.ticket.label";
	public static final String CHECKIN_MAIL_REFUSED_ADULT = "checkin.mail.notchecked.passenger.adult";
	public static final String CHECKIN_MAIL_REFUSED_CHILD = "checkin.mail.notchecked.passenger.child";
	public static final String CHECKIN_MAIL_REFUSED_INFANT = "checkin.mail.notchecked.passenger.infant.accompanist";
	public static final String CHECKIN_CUMULATIVE_EMAIL_SUBJECT = "checkin.cumulative.email.subject";
	public static final String CHECKIN_CUMULATIVE_EMAIL_ATTACHMENT = "checkin.cumulative.email.attachment";
	public static final String CHECKIN_BOARDING_PASS_EMAIL_SUBJECT = "checkin.boardingpass.email.subject";
	public static final String CHECKIN_RECAP_EMAIL_SUBJECT = "checkin.recap.email.subject";
	public static final String CHECKIN_BOARDING_PASS_EMAIL_ATTACHMENT = "checkin.boardingpass.email.attachment";
	public static final String CHECKIN_REFUSED_EMAIL_SUBJECT = "checkin.refused.email.subject";
	
	public static final String MESSAGE_GENERIC_EMPTY_FIELD = "checkin.common.genericEmptyField.label";
	public static final String MESSAGE_GENERIC_INVALID_FIELD = "checkin.common.genericInvalidField.label";
	public static final String MESSAGE_GENERIC_CHECK_FIELD = "checkin.common.genericUncheckedField.label";
	public static final String MESSAGE_GENERIC_NOT_CHECKED_FIELDS = "checkin.common.genericUncheckedFields.label";
	
	public static final String MESSAGE_ERROR_EMPTY_NAME = "checkin.common.nameRequired.error";
	public static final String MESSAGE_ERROR_INVALID_NAME = "checkin.common.nameInvalid.error";
	public static final String MESSAGE_ERROR_EMPTY_LAST_NAME = "checkin.common.lastNameRequired.error";
	public static final String MESSAGE_ERROR_INVALID_LAST_NAME = "checkin.common.lastNameInvalid.error";
	public static final String MESSAGE_ERROR_EMPTY_PNR = "checkin.common.pnrRequired.error";
	public static final String MESSAGE_ERROR_INVALID_PNR = "checkin.common.pnrInvalid.error";
	
	public static final String MESSAGE_ERROR_MODIFY_PASSENGER_DATA = "checkin.common.modifyPassengerDataError.label";
	public static final String MESSAGE_ERROR_INVALID_ANCILLARY_ID = "checkin.common.invalidAncillaryId.label";
	public static final String MESSAGE_ERROR_INVALID_ANCILLARY_STATUS = "checkin.common.invalidAncillaryStatus.label";
	public static final String MESSAGE_ERROR_PAYMENT_WRONG_AMOUNT = "checkin.common.paymentWrongAmount.label";
	public static final String MESSAGE_ERROR_ANCILLARY_PAYMENT = "checkin.common.ancillaryPaymentFail.label";
	public static final String MESSAGE_ERROR_RECOVERABLE_ANCILLARY_PAYMENT = "checkin.common.ancillaryPaymentRecoverableFail.label";
	public static final String MESSAGE_ERROR_INSURANCE_PAYMENT = "checkin.common.insurancePaymentFail.label";
	public static final String MESSAGE_ERROR_INVALID_CREDENTIAL = "checkin.common.invalidCredential.label";
	public static final String MESSAGE_ERROR_INVALID_EMAIL = "checkin.common.invalidEmail.label";
	public static final String CHECKIN_DATA_EMAIL_ALERT="checkindatafromemail";
	
	public static final int MMCODE_TRIPID_LENGHT = 8;
	public static final String MMCODE_TRIPID_PAD_CHARACTER = "0";
	public static final String MMCODE_TRIPID_START_PATH = "Mm";
	
	public static final String CHECKIN_SEARCH_LOGIN = "login";
	public static final String CHECKIN_SEARCH_PNR = "code";
	
	public static final String CHECKIN_SELECTED_ROUTE_PARAM = "srid"; 
	public static final String CHECKIN_PASSENGER_BACK_PARAM = "back";
	
	public static final String PREPARE_PASSENGERS_SERVLET_SELECTOR = "checkinpreparepassengerdata";
	public static final String PREPARE_ANCILLARY_SERVLET_SELECTOR = "checkinprepareancillary";
	public static final String PREPARE_BOARDING_PASS_SERVLET_SELECTOR = "checkinprepareboardingpass";
	public static final String PREPARE_UNDO_SERVLET_SELECTOR = "checkinprepareundo";
	
	public static final String LABEL_TYPE_TRATTA = "checkin.common.tratta.label";
	public static final String LABEL_TYPE_ANDATA = "checkin.common.andata.label";
	public static final String LABEL_TYPE_RITORNO = "checkin.common.ritorno.label";
	
	public static final String CHECKIN_MAIL_IMAGES_FOLDER = "mail/checkin/";
	public static final String[] PLACEHOLDER_RECAP = {"#{T_NAME}#", "#{R_HTMLPASSENGERS}#", "#{R_HTMLFLIGHTS}#"};
	public static final String[] PLACEHOLDER_BOARDING = {"#{T_NAME}#", "#{R_PNR}#", "#{R_HTMLFLIGHTS}#", "#{PLACEHOLDER_BUS_INFO}#"};
	public static final String[] PLACEHOLDER_REFUSED = {"#{R_HTMLPASSENGERS}#", "#{R_HTMLFLIGHTS}#"};
	
	public static final String CHECKIN_MAIL_BP = "boardingPass";
	public static final String CHECKIN_MAIL_REMINDER = "reminder";
	
	public static final String CHECKIN_SMS_TEXT = "{0}, puoi ritirare la carta di imbarco al seguente url: {1}";
	
	public static final String CHECKIN_UPGRADE_BUSINESS_CART_LABEL = "checkin.ancillary.cart_business_upgrade.label";
	public static final String CHECKIN_UPGRADE_PREMIUM_ECONOMY_CART_LABEL = "checkin.ancillary.cart_premium_eco_upgrade.label";
	public static final String CHECKIN_UPGRADE_OTHER_CART_LABEL = "checkin.ancillary.cart_other_upgrade.label";
}
