package com.alitalia.aem.consumer.mmb.render;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.consumer.booking.render.DateRender;

public class MmbFlightDataRender {

	private MmbFlightData flightData;
	private DateRender departureDateRender;
	private DateRender arrivalDateRender;
	private long daysDifference;
	private Boolean isFlightStatusAvailable;
	private List<MmbAncillaryData> relatedAncillaries;
	private boolean bus;
	private boolean miniFare;
	
	public MmbFlightDataRender(MmbFlightData flightData) {
		this.flightData = flightData;
		this.departureDateRender = new DateRender(flightData.getDepartureDateTime());
		this.arrivalDateRender = new DateRender(flightData.getArrivalDateTime());
		this.daysDifference = Math.abs(ChronoUnit.DAYS.between(
				LocalDate.of(flightData.getDepartureDateTime().get(Calendar.YEAR), (flightData.getArrivalDateTime().get(Calendar.MONTH) + 1), flightData.getArrivalDateTime().get(Calendar.DAY_OF_MONTH)), 
				LocalDate.of(flightData.getArrivalDateTime().get(Calendar.YEAR), (flightData.getDepartureDateTime().get(Calendar.MONTH) + 1), flightData.getDepartureDateTime().get(Calendar.DAY_OF_MONTH))));;
		this.relatedAncillaries = new LinkedList<MmbAncillaryData>();
		this.isFlightStatusAvailable = computeIsFlightStatusAvailable();
		this.bus = flightData.isBus();
		this.miniFare = flightData.isMiniFare();
		
	}
	
	public MmbFlightData getFlightData() {
		return this.flightData;
	}
	
	public String getDepartureDay() {
		return departureDateRender.getDay();
	}
	
	public String getDepartureMonth() {
		return departureDateRender.getShortTextMonth();
	}
	
	public String getNumericDepartureMonth() {
		return departureDateRender.getMonth();
	}
	
	public String getDepartureYear() {
		return departureDateRender.getYear();
	}
	
	public String getDepartureHour() {
		return departureDateRender.getHour();
	}
	
	public String getArrivalHour() {
		return arrivalDateRender.getHour();
	}
	
	public long getDaysDifference() {
		return daysDifference;
	}

	public void setDaysDifference(long arrivesNextDay) {
		this.daysDifference = arrivesNextDay;
	}

	public String getDepartureCityI18nKey() {
		String code = flightData.getFrom().getCode() != null ? 
				flightData.getFrom().getCode() : flightData.getFrom().getCityCode();
		return "airportsData." + code + ".city";
	}
	
	public String getArrivalCityI18nKey() {
		String code = flightData.getTo().getCode() != null ? 
				flightData.getTo().getCode() : flightData.getTo().getCityCode();
		return "airportsData." + code + ".city";
	}
	
	public String getDepartureAirportI18nKey() {
		String code = flightData.getFrom().getCode() != null ? 
				flightData.getFrom().getCode() : flightData.getFrom().getCityCode();
		return "airportsData." + code + ".name";
	}
	
	public String getArrivalAirportI18nKey() {
		String code = flightData.getTo().getCode() != null ? 
				flightData.getTo().getCode() : flightData.getTo().getCityCode();
		return "airportsData." + code + ".name";
	}
	
	public List<MmbAncillaryData> getRelatedAncillaries() {
		return relatedAncillaries;
	}

	public Boolean getIsFlightStatusAvailable() {
		return isFlightStatusAvailable;
	}
	
	public boolean isBus(){
		return bus;
	}
	
	public boolean isMiniFare() {
		return miniFare;
	}
	
	
	/* internal private utility methods */
	
	private Boolean computeIsFlightStatusAvailable() {
		
		String departureDate = getDepartureDay() + "/"
				+ getNumericDepartureMonth() + "/"
				+ getDepartureYear();
		
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		df.setLenient(false);
		Calendar calendar = Calendar.getInstance();

		try {
			calendar.setTime(df.parse(departureDate));
			
			Calendar lastDay = Calendar.getInstance();
			lastDay.add(Calendar.DAY_OF_MONTH, 7);
			Calendar firstDay = Calendar.getInstance();
			firstDay.add(Calendar.DAY_OF_MONTH, -3);
			
			if (calendar.after(lastDay) || calendar.before(firstDay)) {
				return false;
			}
		} catch (ParseException e) {
			return false;
		}
		
		return true;
	}
	
}
