package com.alitalia.aem.consumer.booking.controller;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.PassengerApisSecureFlightInfoData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PassengerSecureFlightInfoData;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.AncillaryPassenger;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.model.Passenger;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.render.AvailableFlightsSelectionRender;
import com.alitalia.aem.consumer.booking.render.DirectFlightDataRender;
import com.alitalia.aem.consumer.booking.render.SelectedFlightInfoRender;


@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingMeals extends BookingSessionGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private BookingSession bookingSession;
	
	private List<AncillaryPassenger> passengers;
	private String[] sliceName;
	private SelectedFlightInfoRender[] flights;
	private boolean enabled;
	private boolean fam;
	private boolean showLimitedMessage;
	private static final String AIRPORT_CODE_TLV="TLV";
	private boolean showKosherMeal=true;
	private static final int MILLI_TO_HOUR = 1000 * 60 * 60;

	private boolean showBoxMeal=true;

	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			
			initBaseModel(request);
			if (ctx == null){
				return;
			}
			enabled = ctx.mealsEnabled;
			showLimitedMessage = ctx.isLimitedMeals;
			passengers = new ArrayList<AncillaryPassenger>();
			fam = false;
			if (ctx.cug == BookingSearchCUGEnum.FAMILY) {
				if (ctx.familySolutionFound) {
					fam=true;
				}
			}
			if (enabled) {
				List<Passenger> passengersList = ctx.passengersData.getPassengersList();
				
				String applicantMealPreference = bookingSession.retrieveLoggedUserMealPreferences(ctx);
				
				Passenger applicantPassenger = passengersList.get(0);
				if (applicantPassenger != null) {
					AncillaryPassenger ancillaryPassenger = new AncillaryPassenger();
					ancillaryPassenger.setName(applicantPassenger.getName());
					ancillaryPassenger.setSurname(applicantPassenger.getSurname());
					ancillaryPassenger.setSecondName(applicantPassenger.getSecondName());
					ancillaryPassenger.setMealPreference(applicantMealPreference);
					if (ctx.selectionRoutes.getPassengers().get(0).getType() == PassengerTypeEnum.CHILD) {
						ancillaryPassenger.setIsChild(true);
					}
					passengers.add(ancillaryPassenger);
					for (int j=0; j<passengersList.size(); j++) {
						Passenger referencePassenger = passengersList.get(j);
						if (referencePassenger.isInfant() && referencePassenger.getReferenceAdult() == 0) {
							ancillaryPassenger.setIsInfantReferenced(true);
						}
					}
				}
				for (int k=1; k < passengersList.size(); k++) {
				
					Passenger passenger = passengersList.get(k);
					if (!passenger.isInfant()) {
						AncillaryPassenger ancillaryPassenger = new AncillaryPassenger();
						ancillaryPassenger.setName(passenger.getName());
						if (passenger.getSecondName() != null) {
							ancillaryPassenger.setSecondName(passenger.getSecondName());
						}
						ancillaryPassenger.setSurname(passenger.getSurname());
						ancillaryPassenger.setMealPreference("");
						if (ctx.selectionRoutes.getPassengers().get(k).getType() == PassengerTypeEnum.CHILD) {
							ancillaryPassenger.setIsChild(true);
						}
						
						for (int j=0; j<passengersList.size(); j++) {
							Passenger referencePassenger = passengersList.get(j);
							if (referencePassenger.isInfant() && referencePassenger.getReferenceAdult() == k) {
								ancillaryPassenger.setIsInfantReferenced(true);
							}
						}
						
						passengers.add(ancillaryPassenger);
					}
					
				}
				sliceName = computeSliceName(ctx);
				flights = new SelectedFlightInfoRender[ctx.flightSelections.length];
				int i = 0;
				for (FlightSelection selectedFlight : ctx.flightSelections) {
					flights[i] = new SelectedFlightInfoRender(selectedFlight.getFlightData(), i18n, ctx.market);
					i++;
				} 
				/** Verifica se mostrare la scelta del box sulla base delle seguenti condizioni:
				 * 1)All'interno delle 24 ore non deve essere più mostrata.
				 * 2)Il pasto kosher può essere prenotato fino a 48 ore prima della partenza o 24 ore in partenza da TLV. 
				**/
				computeToShowBoxSeatandKosherMeal();
			} 
			
			
		} catch (Exception e) {
			logger.error("unexpected error: ", e);
			throw e;
		}
	}
	
	public SlingHttpServletRequest getRequest() {
		return request;
	}

	public BookingSession getBookingSession() {
		return bookingSession;
	}

	public List<AncillaryPassenger> getPassengers() {
		return passengers;
	}

	public String[] getSliceName() {
		return sliceName;
	}

	public SelectedFlightInfoRender[] getFlights() {
		return flights;
	}

	public boolean getEnabled() {
		return enabled;
	}
	
	public boolean isFam() {
		return fam;
	}
	
	public boolean getShowLimitedMessage() {
		return showLimitedMessage;
	}

	
	/* private methods */
	
	private String computePassengerName(List<PassengerBaseData> passengersList,
			int i) {
		PassengerBaseData passenger = passengersList.get(i);
		if (passenger instanceof AdultPassengerData) {
			AdultPassengerData typedPassenger = (AdultPassengerData) passenger;
			if (typedPassenger.getLastName() == null) {
				return null;
			}
			String secondName = "";
			if (typedPassenger.getInfo() instanceof PassengerApisSecureFlightInfoData) {
				PassengerApisSecureFlightInfoData apisSecureFlightInfo = (PassengerApisSecureFlightInfoData) typedPassenger.getInfo();
				if (apisSecureFlightInfo.getSecondName() != null) {
					secondName = " " + apisSecureFlightInfo.getSecondName() + " ";
				} 
			} else if (typedPassenger.getInfo() instanceof PassengerSecureFlightInfoData) {
				PassengerSecureFlightInfoData secureFlightInfo = (PassengerSecureFlightInfoData) typedPassenger.getInfo();
				if (secureFlightInfo.getSecondName() != null ){
					secondName = " " + secureFlightInfo.getSecondName() + " ";
				}
			}
			return typedPassenger.getLastName() + " " + typedPassenger.getName() + secondName;
		} else if (passenger instanceof ChildPassengerData) {
			ChildPassengerData typedPassenger = (ChildPassengerData) passenger;
			if (typedPassenger.getLastName() == null) {
				return null;
			}
			String secondName = "";
			if (typedPassenger.getInfo() instanceof PassengerApisSecureFlightInfoData) {
				PassengerApisSecureFlightInfoData apisSecureFlightInfo = (PassengerApisSecureFlightInfoData) typedPassenger.getInfo();
				if (apisSecureFlightInfo.getSecondName() != null) {
					secondName = " " + apisSecureFlightInfo.getSecondName() + " ";
				} 
			} else if (typedPassenger.getInfo() instanceof PassengerSecureFlightInfoData) {
				PassengerSecureFlightInfoData secureFlightInfo = (PassengerSecureFlightInfoData) typedPassenger.getInfo();
				if (secureFlightInfo.getSecondName() != null ){
					secondName = " " + secureFlightInfo.getSecondName() + " ";
				}
			}
			return typedPassenger.getLastName() + " " + typedPassenger.getName() + secondName;
		}
		return null;
	}
	
	
	private String[] computeSliceName(BookingSessionContext ctx) {
		String[] sliceName = new String[ctx.flightSelections.length];
		if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
			for ( int i = 0; i < ctx.flightSelections.length; i++) {
				sliceName[i] = obtainTitleBySliceIndex(i);
			}
		} else {
			for ( int i = 0; i < ctx.flightSelections.length; i++) {
				sliceName[i] = obtainARMessage(i);
			}
		}
		return sliceName;
	}
	
	private String obtainARMessage(int i) {
		switch(i){
			case 0:{
				return i18n.get("booking.carrello.andata.label"); //"andata"
			}
			case 1:{
				return i18n.get("booking.carrello.ritorno.label"); //"ritorno"
			}
		}
		return "";
	}
	
	private String obtainTitleBySliceIndex(int sliceIndex) {
		String msg = "";
		switch(sliceIndex){
			case 0:{
				msg = i18n.get("booking.common.multitratta.prima.label"); //"PRIMA"
			}break;
			case 1:{
				msg = i18n.get("booking.common.multitratta.seconda.label"); //"SECONDA"
			}break;
			case 2:{
				msg = i18n.get("booking.common.multitratta.terza.label"); //"TERZA"
			}break;
			case 3:{
				msg = i18n.get("booking.common.multitratta.quarta.label"); //"QUARTA"
			}break;
			default:{
				return "";
			}
		}
		return msg + " " + i18n.get("booking.common.multitratta.tratta.label"); //"TRATTA"
	}
	private void computeToShowBoxSeatandKosherMeal(){
		Date departureDate=new Date();
		Date today = Calendar.getInstance().getTime();
		String depAirportCode="";
		// Get the first route to extract the first departure Time to calculate is before 24hours or 48 hours from departure
		FlightData departureFlight=flights[0].getFlightData();
		if (departureFlight instanceof ConnectingFlightData){
			DirectFlightData connDepartureFlight=(DirectFlightData)((ConnectingFlightData)departureFlight).getFlights().get(0);
			departureDate=connDepartureFlight.getDepartureDate().getTime();
			depAirportCode=connDepartureFlight.getFrom().getCode();
		}else if(departureFlight instanceof DirectFlightData){				
			DirectFlightData directDepartureFlight=(DirectFlightData)departureFlight;
			departureDate=directDepartureFlight.getDepartureDate().getTime();
			depAirportCode=directDepartureFlight.getFrom().getCode();

		}
		
		long diffHours= (departureDate.getTime()-today.getTime())/MILLI_TO_HOUR;
		
		 if (diffHours<48){ 
				showKosherMeal=false;
				if(diffHours<=24 ){
					showBoxMeal=false;
				}else if(diffHours>24 && AIRPORT_CODE_TLV.equalsIgnoreCase(depAirportCode)){
					showKosherMeal=true;
			}
	}
	}
	public boolean showKosherMeal(){
		return showKosherMeal;
	}
	public boolean showBoxMeal(){
		return showBoxMeal;
	}
	public String getKosherMealCode(){
		return i18n.get("mealsData.KSML");
	}
}
