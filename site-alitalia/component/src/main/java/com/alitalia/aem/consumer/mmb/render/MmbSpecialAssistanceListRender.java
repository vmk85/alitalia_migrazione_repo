package com.alitalia.aem.consumer.mmb.render;

public class MmbSpecialAssistanceListRender {
	
	private String value;
	private String extraInfo;
	private String text;
	
	public MmbSpecialAssistanceListRender(String value, String text, String extraInfo) {
		this.text = text;
		this.value = value;
		this.extraInfo = extraInfo;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "MmbSpecialAssistance [text=" + text + ", value=" + value + "]";
	}

	public String getExtraInfo() {
		return extraInfo;
	}

	public void setExtraInfo(String extraInfo) {
		this.extraInfo = extraInfo;
	}
	
	
	
	

}
