/** Modello di risposta standard per servlet MyAlitalia
 * (Ricordarsi di svutare l'errorMessage in caso di success ) [D.V.]*/

package com.alitalia.aem.consumer.myalitalia.model;

import com.alitalia.aem.consumer.myalitalia.MyAlitaliaConstants;

public class StandardJsonResponse {

    boolean isError = true;

    String errorMessage = MyAlitaliaConstants.MYALITALIA_ERROR_MESSAGE_SESSION;

    String successMessage = "";

    String detailMessage = "";

    public String getDetailMessage() {
        return detailMessage;
    }

    public void setDetailMessage(String detailMessage) {
        this.detailMessage = detailMessage;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    @Override
    public String toString() {
        return "StandardJsonResponse{" +
                "isError=" + isError +
                ", errorMessage='" + errorMessage + '\'' +
                ", successMessage='" + successMessage + '\'' +
                '}';
    }
}
