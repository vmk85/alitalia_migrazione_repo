package com.alitalia.aem.consumer.booking.render;

public class SearchElementDetailRender {

	private DateRender date;
	private String airportCode;
	private String airportName;
	private String cityCode;
	private String cityName;
	
	public SearchElementDetailRender(DateRender date, String airportCode, String airportName, String cityCode, String cityName) {
		this.date = date;
		this.airportCode = airportCode;
		this.airportName = airportName;
		this.cityCode = cityCode;
		this.cityName = cityName;
	}
	
	public SearchElementDetailRender(DateRender date, String airportCode, String cityCode, String cityName) {
		this(date, airportCode, null, cityCode, cityName);
	}

	public DateRender getDate() {
		return date;
	}

	public String getAirportCode() {
		return airportCode;
	}
	
	public String getAirportName() {
		return airportName;
	}
	
	public String getCityCode() {
		return cityCode;
	}
	
	public String getCityName() {
		return cityName;
	}
	
}
