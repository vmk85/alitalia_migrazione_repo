package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.IOException;

import javax.servlet.Servlet;


import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.model.content.specialpages.MillemigliaRegistratiData;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;

@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "millemigliaremovedatasubmit" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")

public class MillemigliaRimuoviDatiServlet extends GenericFormValidatorServlet {
	
	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[MillemigliaRegistratiServlet] validateForm");
	
		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			return validator.validate();

		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Error during parameters validation", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		logger.debug("[MillemigliaRegistratiServlet] performSubmit");
		request.getSession().setAttribute(MillemigliaRegistratiData.NAME, null);
	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[MillemigliaRegistratiServlet] saveDataIntoSession");
		
		
		request.getSession().setAttribute(MillemigliaRegistratiData.NAME, null);
		
		// go back to form with error
	
	}

	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {
		
		return validator;


	}
	
	
	
}
