package com.alitalia.aem.consumer.booking.analytics.filler;

public class AnalyticsInfoFactory {
	
	public static final short BOOKING = 0;
	public static final short BOOKING_AWARD = 1;
	public static final short CARNET = 2;
	
	public static AnalyticsInfoFiller getFiller(short key){
		AnalyticsInfoFiller filler  = null;
		/*Only Booking at this moment*/
		if(AnalyticsInfoFactory.BOOKING==key){
		filler = new BookingAnalyticsInfoFiller();
		}
		else if(AnalyticsInfoFactory.BOOKING_AWARD==key){
			filler=new BookingAnalyticsInfoFiller();
		}
		else if(AnalyticsInfoFactory.CARNET==key){
			filler=new CarnetAnalytcsInfoFiller();
		}
		
		return filler;
	}
	
	public static AnalyticsInfoToJsObj getJsConverter(short key){
		AnalyticsInfoToJsObj conv = null;
		if(AnalyticsInfoFactory.BOOKING==key){
			conv = new BookingAnalyticsInfoToJsObj();
		}
		else if(AnalyticsInfoFactory.BOOKING_AWARD==key){
			conv = new BookingAwardAnalyticsInfoToJsObj();
		}
		else if(AnalyticsInfoFactory.CARNET==key){
			conv = new CarnetAnalyticsInfoToJsObj();

		}
		return conv;
	}

}
