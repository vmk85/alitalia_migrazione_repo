package com.alitalia.aem.consumer.model.content.specialpages;


import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;


import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables={SlingHttpServletRequest.class})
public class RecuperaPinModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;
	
	private RecuperaPinData recuperaPinData;

	
	@PostConstruct
	protected void initModel() {
		logger.debug("[recuperaPinModel] initModel");
		super.initBaseModel(slingHttpServletRequest);
		// retrieve and remove BaggageClaimData from session if any
	
		recuperaPinData = (RecuperaPinData) slingHttpServletRequest.getSession().getAttribute(RecuperaPinData.NAME);
		slingHttpServletRequest.getSession().removeAttribute(RecuperaPinData.NAME);
		
					
	}

	public RecuperaPinData getRecuperaPinData() {
		return recuperaPinData;
	}


}
