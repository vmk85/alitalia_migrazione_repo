package com.alitalia.aem.consumer;

import java.net.URLDecoder;
import java.net.URLEncoder;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.LocateRequest;
import com.alitalia.aem.common.messages.home.LocateResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoNearestAirportByCoordsResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.CookieUtils;
import com.alitalia.aem.web.component.egon.delegate.EgonDelegate;
import com.alitalia.aem.web.component.geospatial.delegate.GeoSpatialDelegate;

/**
 * Manager class that wraps the logic to retrieve geolocalization data using the
 * back-end services, to store it in a persistent cookie and to read it back
 * afterwards.
 */
@Component
@Service(value = AlitaliaGeolocalizationManager.class)
public class AlitaliaGeolocalizationManager {

	public static final String COOKIE_NAME = "alitalia-consumer-geo";
	//Bug 2840: ridotta la scadenza del cookie da 90 giorni a un solo giorno
	public static final int COOKIE_MAX_AGE = 86400;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private EgonDelegate egonDelegate;

	@Reference
	private GeoSpatialDelegate geoSpatialDelegate;

	/**
	 * The main entry point of the class, tries to extract the geolocalization
	 * data from a request cookie and, if not found, retrieve it using back-end
	 * services and store it as a response cookie. Either way, the
	 * geolocalization data is returned to the caller.
	 * 
	 * @param request
	 *            The Sling HTTP servlet request, also used to read the cookie.
	 * @param response
	 *            The Sling HTTP servlet response, also used to set the cookie.
	 * @return The geolocalization data found or produced, or null if an
	 *         unexpected error occurs.
	 */
	public AlitaliaGeolocalizationData processFromRequest(
			SlingHttpServletRequest request, SlingHttpServletResponse response) {
		logger.debug("[AlitaliaGeolocalizationData] Processing "
			+ "geolocalization for request.");

		AlitaliaGeolocalizationData geoData = null;
		String cookieValue = null;

		try {

			cookieValue = CookieUtils.getCookieValue(request, COOKIE_NAME);
			geoData = AlitaliaGeolocalizationData.fromCookieString(cookieValue);

			if (geoData == null && !configuration.getEgonDisabled()) {
				logger.debug("Geo cookie with name {} not found, invoking services...", COOKIE_NAME);

				String sid = "AlitaliaGeolocalizationManager";
				String tid = IDFactory.getTid();

				// 1st step - find IP to geolocalize
				if (logger.isTraceEnabled()) {
					// trace header details available for debugging purposes
					logger.trace("Printing request details.");
					AlitaliaUtils.logRequestHeaders(request);
				}
				String ipAddress = AlitaliaUtils.getRequestRemoteAddr(configuration, request);

				// 2nd step - geolocalize IP
				LocateRequest locateRequest = new LocateRequest();
				locateRequest.setSid(sid);
				locateRequest.setTid(tid);
				locateRequest.setWpuser(configuration.getEgonUser());
				locateRequest.setWppasw(configuration.getEgonPassword());
				locateRequest.setCdxipa(ipAddress);
				LocateResponse locateResponse = egonDelegate.locate(locateRequest);
				String latitude = locateResponse.getCoolat();
				String longitude = locateResponse.getCoolon();
				String countryCode = locateResponse.getCdxiso();
				logger.debug("Data from EGON for IP {}: latitude={}, longitude={}, countryCode={}", new Object[] { ipAddress, latitude, longitude, countryCode });

				if (countryCode == null || countryCode.length() == 0 || latitude == null || latitude.length() == 0 || longitude == null || latitude.length() == 0) {

					// invalid EGON data
					logger.warn("Invalid data received from EGON, error {} {}", locateResponse.getDsxerr(), locateResponse.getWp9STC());
					geoData = null;

				} else {

					// 3rd step - find nearest airport
					RetrieveGeoNearestAirportByCoordsRequest nearestAirportRequest = new RetrieveGeoNearestAirportByCoordsRequest();
					locateRequest.setSid(sid);
					locateRequest.setTid(tid);
					nearestAirportRequest.setCountryCode(countryCode);
					nearestAirportRequest.setLatitude(latitude);
					nearestAirportRequest.setLongitude(longitude);
					nearestAirportRequest.setMaxDistanceKM(250);
					RetrieveGeoNearestAirportByCoordsResponse retrieveGeoNearestAirportByCoords = geoSpatialDelegate.retrieveGeoNearestAirportByCoords(nearestAirportRequest);
					String airportCode = retrieveGeoNearestAirportByCoords.getAirportCode();
					String cityCode = retrieveGeoNearestAirportByCoords.getCityCode();
					if (cityCode == null) {
						logger.info("Could not find city code from retrieveGeoNearestAirportByCoords");
						return null;
					}
					logger.debug("Nearest airport {} {}", airportCode, cityCode);

					// 4th step - find language from market settings in JCR
					String languageCode = AlitaliaUtils.findResourceDefaultLanguage(request.getResourceResolver(), countryCode, cityCode);
					if (languageCode == null) {
						logger.info("Could not find default language code.");
						return null;
					}
					logger.debug("Default language found {}", languageCode);

					// finally, encode in geo cookie
					geoData = new AlitaliaGeolocalizationData(countryCode, languageCode, cityCode, latitude, longitude);
					cookieValue = geoData.toCookieString();
					if (response != null) {
						CookieUtils.setCookieValue(response, COOKIE_NAME, null,
								"/", COOKIE_MAX_AGE, cookieValue, false, false);
					}
				}
			}

		} catch (Exception e) {
			logger.error("Unexpected error in geolocalization logic.", e);
			geoData = null;

		}

		return geoData;
	}

	/**
	 * DTO class associated with cookie-stored data, with serialiation methods.
	 */
	public static class AlitaliaGeolocalizationData {

		/**
		 * Logger.
		 */
		private final static Logger logger = LoggerFactory.getLogger(AlitaliaGeolocalizationData.class);

		private String countryCode = null;
		private String languageCode = null;
		private String cityCode = null;
		private String latitude = null;
		private String longitude = null;

		public AlitaliaGeolocalizationData(String countryCode, String languageCode, String cityCode, String latitude, String longitude) {
			this.countryCode = countryCode;
			this.languageCode = languageCode;
			this.cityCode = cityCode;
			this.latitude = latitude;
			this.longitude = longitude;
		}

		public String getCountryCode() {
			return countryCode;
		}

		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}

		public String getLanguageCode() {
			return languageCode;
		}

		public void setLanguageCode(String languageCode) {
			this.languageCode = languageCode;
		}

		public String getCityCode() {
			return cityCode;
		}

		public void setCityCode(String cityCode) {
			this.cityCode = cityCode;
		}

		public String getLatitude() {
			return latitude;
		}

		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}

		public String getLongitude() {
			return longitude;
		}

		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}

		public static AlitaliaGeolocalizationData fromCookieString(String cookieString) {
			if (cookieString == null || cookieString.length() == 0) {
				return null;
			}
			try {
				String[] cookieStringTokens = cookieString.split("#");
				if (cookieStringTokens.length != 5) {
					logger.warn("Invalid geolocalization token found, ignoring.");
					return null;
				}
				String countryCode = URLDecoder.decode(cookieStringTokens[0], "UTF-8");
				String languageCode = URLDecoder.decode(cookieStringTokens[1], "UTF-8");
				String cityCode = URLDecoder.decode(cookieStringTokens[2], "UTF-8");
				String latitude = URLDecoder.decode(cookieStringTokens[3], "UTF-8");
				String longitude = URLDecoder.decode(cookieStringTokens[4], "UTF-8");
				return new AlitaliaGeolocalizationData(countryCode, languageCode, cityCode, latitude, longitude);
			} catch (Exception e) {
				logger.error("Error decoding geolocalization data, ignoring.", e);
				return null;
			}
		}

		public String toCookieString() {
			String cookieString = null;
			try {
				String[] cookieStringTokens = new String[5];
				cookieStringTokens[0] = URLEncoder.encode(this.countryCode, "UTF-8");
				cookieStringTokens[1] = URLEncoder.encode(this.languageCode, "UTF-8");
				cookieStringTokens[2] = URLEncoder.encode(this.cityCode, "UTF-8");
				cookieStringTokens[3] = URLEncoder.encode(this.latitude, "UTF-8");
				cookieStringTokens[4] = URLEncoder.encode(this.longitude, "UTF-8");
				cookieString = AlitaliaUtils.joinArray(cookieStringTokens, "#");
			} catch (Exception e) {
				logger.error("Error encoding geolocalization data.", e);
				return null;
			}
			return cookieString;
		}

	}

}
