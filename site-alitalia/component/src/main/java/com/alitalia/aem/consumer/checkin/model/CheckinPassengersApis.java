package com.alitalia.aem.consumer.checkin.model;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinCountryData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinPassenger;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinPassengersApis extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private CheckinSession checkinSession;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;

	private CheckinPassenger passenger;
	private String previousPassengerUrl;
	
	private String[] days;
	private Map<String, String> months;
	private String[] birthYears;
	private String[] expiryYears;
	private List<CheckinCountryData> countries;
	
	private boolean documentChoose;
	
	@PostConstruct
	protected void initModel() throws IOException {
		
		initBaseModel(request);
		prepareApisBasicData();
	}
	
	private void prepareApisBasicData() throws IOException {
		
		days = checkinSession.getDays();
		expiryYears = checkinSession.getExpiryYears();
		birthYears = checkinSession.getBirthYears();
		countries = populateCountriesLabel(checkinSession.getCountries(ctx));
				
		months = new LinkedHashMap<String, String>();
		Calendar date = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM", Locale.ENGLISH);
		for (int i = 0; i < 12; i++) {
			date.set(2016, i, 1);
			months.put(String.valueOf(i), i18n.get("common.monthsOfYear." + dateFormat.format(date.getTime()).toLowerCase()));
		}
		
		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
		
		CheckinPassengerData passengerData = ctx.selectedPassengers.get(ctx.passengerIndex);
			
		passenger = checkinSession.toCheckinPassenger(passengerData);
		
		documentChoose = checkinSession.getDocumentAllowedValues(ctx).length > 1;
		
		previousPassengerUrl = baseUrl + configuration.getCheckinPassengersPage();
	}
	
	private List<CheckinCountryData> populateCountriesLabel(List<CheckinCountryData> list){
		List<CheckinCountryData> newCountries= new ArrayList<CheckinCountryData>();
		for(int i = 0; i < list.size(); i++){
			CheckinCountryData country =  list.get(i).clone();
			country.setCountryName(ctx.i18n.get("countryData." + country.getCountryCode()));
			newCountries.add(country);
		}
		Collections.sort(newCountries);
		return newCountries;
	}
	
	
	public CheckinPassenger getPassenger() {
		return passenger;
	}

	public String getPreviousPassengerUrl() {
		return previousPassengerUrl;
	}

	public String[] getDays() {
		return days;
	}

	public Map<String, String> getMonths() {
		return months;
	}

	public String[] getBirthYears() {
		return birthYears;
	}

	public String[] getExpiryYears() {
		return expiryYears;
	}
	
	public List<CheckinCountryData> getCountries() {
		return countries;
	}


	public boolean isDocumentChoose() {
		return documentChoose;
	}
}
