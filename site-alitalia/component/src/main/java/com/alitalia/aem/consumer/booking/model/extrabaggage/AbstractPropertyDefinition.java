package com.alitalia.aem.consumer.booking.model.extrabaggage;


import java.util.ArrayList;

/** 
 Sabre Request Model for AbstractPropertyDefinition
*/
public class AbstractPropertyDefinition
{
	private String propertyName;
	public final String getpropertyName()
	{
		return propertyName;
	}
	public final void setpropertyName(String value)
	{
		propertyName = value;
	}
	private AbstractAncillaryPropertyType propertyType;
	public final AbstractAncillaryPropertyType getpropertyType()
	{
		return propertyType;
	}
	public final void setpropertyType(AbstractAncillaryPropertyType value)
	{
		propertyType = value;
	}
	private boolean optional;
	public final boolean getoptional()
	{
		return optional;
	}
	public final void setoptional(boolean value)
	{
		optional = value;
	}
	private ArrayList<String> allowedValueList;
	public final ArrayList<String> getallowedValueList()
	{
		return allowedValueList;
	}
	public final void setallowedValueList(ArrayList<String> value)
	{
		allowedValueList = value;
	}
	private int min;
	public final int getmin()
	{
		return min;
	}
	public final void setmin(int value)
	{
		min = value;
	}
	private int max;
	public final int getmax()
	{
		return max;
	}
	public final void setmax(int value)
	{
		max = value;
	}
	private String unit;
	public final String getunit()
	{
		return unit;
	}
	public final void setunit(String value)
	{
		unit = value;
	}
}