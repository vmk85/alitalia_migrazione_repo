package com.alitalia.aem.consumer.myalitalia.model;

import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;

public class MyFlightsPnr {
    private CheckinPnrSearchResponse checkinPnrSearchResponse;

    private String nome;

    private String cognome;

    public CheckinPnrSearchResponse getCheckinPnrSearchResponse() {
        return checkinPnrSearchResponse;
    }

    public void setCheckinPnrSearchResponse(CheckinPnrSearchResponse checkinPnrSearchResponse) {
        this.checkinPnrSearchResponse = checkinPnrSearchResponse;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    @Override
    public String toString() {
        return "MyFlightsPnr{" +
                "checkinPnrSearchResponse=" + checkinPnrSearchResponse +
                ", nome='" + nome + '\'' +
                ", cognome='" + cognome + '\'' +
                '}';
    }
}
