package com.alitalia.aem.consumer.servlet.login;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.enumerations.TipoProfiloEnum;
import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.millemiglia.exception.MMCodeMismatchException;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.alitalia.aem.ws.common.exception.WSClientException;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.MilleMigliaServiceClient;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.wsdl.IMilleMigliaServiceCheckCredenzialiServiceFaultFaultFaultMessage;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.CheckCredenzialiRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfile;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd2.CustomerProfileSA;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.*;
import org.apache.felix.scr.annotations.Properties;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.xml.bind.JAXBElement;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.ResourceBundle;

import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.*;

/**
 * Servlet exposing a REST-based service to verify the validity of the
 * provided MilleMiglia credentials.
 * 
 * <p>The expected input includes the following request parameters:
 * <ul>
 * <li>mmCode: the MilleMiglia code.</li> 
 * <li>mmPin: the MilleMiglia pin.</li> 
 * </ul>
 * </p>
 * 
 * <p>The output is a JSON object with the following attributes:
 * <ul>
 * <li>successful: true for valid credentials, false otherwise.</li>
 * <li>errorCode: code related to the failed attempt, if applicable.</li> 
 * <li>errorDescription: a description related to the failed attempt, if applicable.</li>
 * <li>mmCode: the MilleMiglia code.</li> 
 * <li>mmPin: the MilleMiglia pin.</li> 
 * </ul>
 * </p>
 */

@Component(metatype = false, immediate = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkCredenzialiMillemiglia" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" })
})
@SuppressWarnings("serial")
public class CheckMilleMigliaLoginSAServlet extends SlingAllMethodsServlet {
//public class CheckMilleMigliaLoginSAServlet extends GenericFormValidatorServlet {

	private Locale locale;
	private String itemCache;
	private String languageCode;
	private String market;

	private I18n i18n;

	private Logger logger = LoggerFactory.getLogger(getClass());



	/**
	 * Reference to the service to remote consumer login service.
	 */
	@Reference
	private MilleMigliaServiceClient wsClient;

	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_UNARY)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;

	@Reference
	private volatile AlitaliaConfigurationHolder configuration;

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		logger.debug("MilleMiglia Strong Auth Credentials Check Requested.");

		String userName = request.getParameter("mmCode");
		String password = request.getParameter("mmPin");

		String mmCode = null;
		String mmPin = null;
		Boolean rememberMe=false;

		//della password viene fatto un hash MD5 convertito in maiuscolo per passarla al webservice
		String md5password = DigestUtils.md5Hex(password).toUpperCase();

		//se userName e' tutto numerico allora e' stato ustato un codice millemiglia
		if(StringUtils.isNumeric(userName))
		{
			mmCode=userName;
			if (password.length()<8) {
				userName=null;
			}
		}


		boolean captchaValidated = (Boolean)(request.getSession(true).getAttribute("captchaAlreadyValidated") != null ? request.getSession(true).getAttribute("captchaAlreadyValidated") : Boolean.FALSE);


		request.getSession().removeAttribute("captchaAlreadyValidated");

		boolean successful = false;
		boolean showCaptcha = false;
		boolean isLocked = false;
		String errorCode = "";
		String errorDescription = "";
		// stron Auth
		String idProfilo = "";
		boolean flagCredenzialiOK = false;
		boolean isMailCertified = false;
		String certifiedMailAccount = "";
		String certifiedPhoneAccount = "";
		String accessFailedCount = "";
		boolean isKid=false;
		boolean saUpdateDone = false;

		if (consumerLoginDelegate == null) {
			logger.error("Cannot check MilleMiglia loginSA credentials, delegate not available.");
			successful = false;
			errorCode = "unknown";
			errorDescription = "LoginSA delegate currently unavailable";

		} else if (!captchaValidated) {
			logger.error("Cannot check MilleMiglia loginSA credentials, impossible to retrieve captcha validation from session.");
			successful = false;
			errorCode = "unknown";
			errorDescription = "Invalid Captcha";

		} else {

			logger.debug("Validating Credentials.");

			try {

//				GetDatiSicurezzaProfiloResponseLocal getDatiSicurezzaProfiloResponseLocal = new GetDatiSicurezzaProfiloResponseLocal();

				if (mmCode != null){
					GetDatiSicurezzaProfiloRequestLocal datiSicurezzaProfiloRequest = new GetDatiSicurezzaProfiloRequestLocal();

					datiSicurezzaProfiloRequest.setIdProfilo(mmCode);
					GetDatiSicurezzaProfiloResponseLocal datiBeforeUpdateSicurezzaProfilo =
							consumerLoginDelegate.GetDatiSicurezzaProfilo(datiSicurezzaProfiloRequest);
//					getDatiSicurezzaProfiloResponseLocal = datiBeforeUpdateSicurezzaProfilo;

					logger.debug("datiBeforeUpdateSicurezzaProfilo done");
					if (datiBeforeUpdateSicurezzaProfilo != null) {
						if(datiBeforeUpdateSicurezzaProfilo.getUsername() != null &&
								!datiBeforeUpdateSicurezzaProfilo.getUsername().isEmpty()){
							logger.debug("saUpdateDone done");
							saUpdateDone = true;
						}

						TipoProfiloEnum tipoProfiloUtente = datiBeforeUpdateSicurezzaProfilo.getTipoProfilo();

						if (tipoProfiloUtente != null){
							if (tipoProfiloUtente == TipoProfiloEnum.MM_KIDS){
								isKid=true;
							}
						}
					}
					logger.debug("CHECK SA LOGIN ----- userName: "+userName);
				}


				if (isKid) {
					//se questo utente ha già fatto l'adeguamento il login con millemiglia e pin deve restituire errore
					//per leggere questa informazione mi basta il codice millemiglia (che è quello della textbox)

					logger.debug("New Login KIDS");

					successful = false;
					errorCode = "unknown";
					errorDescription = "Login forbidden";

					if (locale == null){
						locale = AlitaliaUtils.findResourceLocale(request.getResource());
						ResourceBundle resourceBundle = request.getResourceBundle(locale);
						i18n = new I18n(resourceBundle);
						itemCache = AlitaliaUtils.getLanguage(locale.toLanguageTag()).toUpperCase();
						languageCode = AlitaliaUtils.getLanguage(locale.toLanguageTag()).toUpperCase();
						market = locale.getCountry();

						errorDescription = i18n.get("specialpage.login.error.isKid");

					}

				} else if (userName == null && password.length()<8) {
					//Errore: login nuovo con le vecchie credenziali 
					logger.debug("New Login Old Credential");
					successful = false;
					errorCode = "unknown";
					if (saUpdateDone) {
						logger.debug("New Login Old Credential Updated");
						errorDescription = "specialpage.login.error.oldCredential.updated";
					} else {
						logger.debug("New Login Old Credential NOT Updated");
						errorDescription = "specialpage.login.error.oldCredential.notupdated";
					}
					logger.debug("CHECK SA LOGIN ----- errorDescription 1: "+errorDescription);
				} else{
				
					String tid = IDFactory.getTid();
					String sid = IDFactory.getSid(request);

					MilleMigliaLoginSARequest loginSARequest = new MilleMigliaLoginSARequest(tid, sid);
					loginSARequest.setCodiceMM(mmCode);
					loginSARequest.setUsername(userName);
					loginSARequest.setPassword(md5password);
					MilleMigliaLoginSAResponse loginSAResponse = consumerLoginDelegate.milleMigliaLoginSA(loginSARequest);

					logger.debug("SSOToken retrieved: "+loginSAResponse.getSSO_token());
					if (loginSAResponse.getSSO_token() != null && !loginSAResponse.getSSO_token().isEmpty()) {
						Cookie ssoCookie = new Cookie("ibeopentoken", loginSAResponse.getSSO_token());
						int ssoCookieExpiry = configuration.getSsoCookieTimeout();
						String ssoCookieDomain = configuration.getSsoCookieDomain();

						ssoCookie.setPath("/");
						ssoCookie.setSecure(configuration.getHttpsEnabled());
						if (ssoCookieExpiry != -1)
							ssoCookie.setMaxAge(ssoCookieExpiry);
						if (!"".equals(ssoCookieDomain))
							ssoCookie.setDomain(ssoCookieDomain);
						response.addCookie(ssoCookie);
					} else {
						logger.error("SSOToken is null or empty");
					}

					successful = loginSAResponse.isLoginSuccessful();

					if (loginSAResponse.getCustomerSA() != null) {

						MMCustomerProfileData customerProfile = loginSAResponse.getCustomerSA().getMmCustomerProfileData();

						if(customerProfile !=null )
						{
							if(!customerProfile.getCustomerNumber().isEmpty()){
								mmCode=customerProfile.getCustomerNumber();
							}

							if(!customerProfile.getCustomerPinCode().isEmpty()){
								mmPin=customerProfile.getCustomerPinCode();
							}
						}

						rememberMe=loginSAResponse.getCustomerSA().isFlagRememberPassword();

					}

					if (!successful) {
						errorCode = loginSAResponse.getErrorCode();
						errorDescription = loginSAResponse.getErrorDescription();
						if (errorCode!=null){
							if (errorCode.equals("ErrorLoginSATempLock")){
								logger.debug("ErrorLoginSATempLock. isLocked true");
								isLocked = true;
							}
						}
					} else {
						/** Controllo j_security per mmCodeMismatchException */
						/** Va posizionato qui altrimenti se l'utente sbaglia la password entra nel controllo di sicurezza dato che loginSAResponse.getCustomerSA() == null*/
						if (loginSAResponse.getCustomerSA() != null && loginSAResponse.getCustomerSA().getMmCustomerProfileData() != null){
								String customerNumberResponse = loginSAResponse.getCustomerSA().getMmCustomerProfileData().getCustomerNumber();
								if (customerNumberResponse == null || (StringUtils.isNumeric(mmCode) && !customerNumberResponse.contains(mmCode))){
									// MM CODE MISMATCH
									MMCodeMismatchException mmCodeMismatchException = new MMCodeMismatchException("MilleMiglia Code mismatch exception");
									logger.error("MilleMiglia Code mismatch. Request Code: ["+mmCode+"], Response Code: ["+customerNumberResponse+"]", mmCodeMismatchException);
									throw mmCodeMismatchException;
								} else if(customerNumberResponse == null || (!StringUtils.isNumeric(userName) && !loginSAResponse.getCustomerSA().getUsername().toUpperCase().equals(userName.toUpperCase()))){

									MMCodeMismatchException mmCodeMismatchException = new MMCodeMismatchException("MilleMiglia Code mismatch exception");
									logger.error("MilleMiglia Username mismatch. Request Code: ["+userName+"], Response Code: ["+loginSAResponse.getCustomerSA().getUsername()+"]", mmCodeMismatchException);
									throw mmCodeMismatchException;

								} else {
									logger.info("MilleMiglia Code match. Request Code: [{}], Response Code: [{}]", mmCode, customerNumberResponse);
//									request.getSession().setAttribute("MilleMigliaProfileDataSicurezza",getDatiSicurezzaProfiloResponseLocal);
									ProfileRequest profileRequest = new ProfileRequest();
									profileRequest.setSid(IDFactory.getSid());
									profileRequest.setTid(IDFactory.getTid());
									profileRequest.setCustomerProfile(new MMCustomerProfileData());
									profileRequest.getCustomerProfile().setCustomerNumber(mmCode);
									profileRequest.getCustomerProfile().setCustomerPinCode(mmPin);
									ProfileResponse profileResponse = consumerLoginDelegate.getProfile(profileRequest);
									request.getSession().setAttribute("MilleMigliaProfileData",profileResponse);

								}

						} else {
							throw new Exception();
						}
					}
				}
				logger.debug("CHECK SA LOGIN ----- errorDescription 2: "+errorDescription);
			} catch (WSClientException wse){
				logger.error("WSClientException trying to check MilleMiglia loginSA.", wse);
				successful = false;
				errorCode = wse.getMessage();
				errorDescription = "Error To Initialize MilleMigliaServiceClient";

			} catch (Exception e) {
				logger.error("Error trying to check MilleMiglia loginSA.", e);
				successful = false;
				errorCode = "unknown";
				errorDescription = "Unexpected error";
			}
	}

		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		try {
			json.object();
			json.key("successful");
			json.value(successful);
			json.key("errorCode");
			json.value(errorCode);
			json.key("errorDescription");
			json.value(errorDescription);
			json.key("userName");
			json.value(userName);
			json.key("idProfilo");
			json.value(idProfilo);
			json.key("showCaptcha");
			json.value(showCaptcha);
			json.key("isLocked");
			json.value(isLocked);

			json.key("mmCode");
			json.value(mmCode);

			json.key("mmPin");
			json.value(mmPin);

			json.key("rememberMe");
			json.value(rememberMe);

			json.endObject();
		} catch (Exception e) {
			logger.error("Unexected error generating JSON response.", e);
		}

	}
}
