package com.alitalia.aem.consumer.booking.model.extrabaggage;

import java.util.*;

/** 
 Sabre Request Model for AncillaryGroup
*/
public class AncillaryGroup
{
	private String code;
	public final String getcode()
	{
		return code;
	}
	public final void setcode(String value)
	{
		code = value;
	}
	private int maxGroupedQuantity;
	public final int getmaxGroupedQuantity()
	{
		return maxGroupedQuantity;
	}
	public final void setmaxGroupedQuantity(int value)
	{
		maxGroupedQuantity = value;
	}
	private ArrayList<Ancillary> ancillaries;
	public final ArrayList<Ancillary> getancillaries()
	{
		return ancillaries;
	}
	public final void setancillaries(ArrayList<Ancillary> value)
	{
		ancillaries = value;
	}
}