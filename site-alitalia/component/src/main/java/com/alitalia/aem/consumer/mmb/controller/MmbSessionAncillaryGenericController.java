package com.alitalia.aem.consumer.mmb.controller;

import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;

import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;
import com.alitalia.aem.consumer.utils.Lists;

public class MmbSessionAncillaryGenericController extends MmbSessionGenericController {
	
	@Override
	protected void initBaseModel(SlingHttpServletRequest request) {
		super.initBaseModel(request);
	}
	
	public String getCurrencySymbol() {
		if (ctx != null) {
			return ctx.currencySymbol;
		} else {
			return "";
		}
	}
	
	public boolean getDisplayFeedbackStep() {
		if (request != null) {
			return "1".equals(request.getParameter("displayfeedbackstep"));
		} else {
			return false;
		}
	}
	
	/* utility methods for subclasses */
	
	protected String getAncillariesSelectionIdentifier(List<MmbAncillaryData> targetAncillaries) {
		return getAncillariesSelectionIdentifier(
				(MmbAncillaryData[]) targetAncillaries.toArray(new MmbAncillaryData[targetAncillaries.size()]));
	}
	
	protected String getAncillariesSelectionIdentifier(MmbAncillaryData[] targetAncillaries) {
		String selectionIdentifier = "";
		if (targetAncillaries == null || targetAncillaries.length == 0) {
			selectionIdentifier = "(disabled)";
		} else {
			for (MmbAncillaryData affectedAncillary : targetAncillaries) {
				if (selectionIdentifier.length() > 0) {
					selectionIdentifier += "-";
				}
				selectionIdentifier += affectedAncillary.getId();
			}
		}
		return selectionIdentifier;
	}
	
	protected String getAncillariesSelectionIdentifier(MmbAncillaryData targetAncillary) {
		MmbAncillaryData[] targetAncillaries = null;
		if (targetAncillary != null) {
			targetAncillaries = new MmbAncillaryData[] { targetAncillary };
		}
		return getAncillariesSelectionIdentifier(targetAncillaries);
	}
	
	protected String getRouteTypeLabel(MmbAncillariesGroup group) {
		MmbFlightData flight = Lists.getFirst(group.getFlights());
		if (group.isPartOfMultiLeg()) {
			int numTratta = flight.getRouteId().intValue() + 1;
			return i18n.get(MmbConstants.LABEL_TYPE_TRATTA) + " " + numTratta;
		} else {
			if (RouteTypeEnum.OUTBOUND.equals(flight.getType())) {
				return i18n.get(MmbConstants.LABEL_TYPE_ANDATA);
			} else {
				return i18n.get(MmbConstants.LABEL_TYPE_RITORNO);
			}
		}
	}
	
}
