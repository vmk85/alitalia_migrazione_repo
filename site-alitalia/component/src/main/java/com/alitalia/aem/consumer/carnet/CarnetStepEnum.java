package com.alitalia.aem.consumer.carnet;

public enum CarnetStepEnum {
	CHOOSE("0"),
	PERSONAL_INFO("1"),
    PAYMENT("2"),
    DONE("3");
	
    private final String value;
    
    CarnetStepEnum(String v) {
        value = v;
    }
    
    public String value() {
    	return this.value;
    }
    
    public static CarnetStepEnum fromValue(String v) {
        for (CarnetStepEnum c: CarnetStepEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
