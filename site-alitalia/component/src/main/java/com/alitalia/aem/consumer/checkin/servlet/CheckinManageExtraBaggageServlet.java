package com.alitalia.aem.consumer.checkin.servlet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checkinancillarycartextrabaggage" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinManageExtraBaggageServlet extends GenericCheckinFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile CheckinSession checkinSession;	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		CheckinSessionContext ctx = getCheckInSessionContext(request); 
		List<MmbAncillaryErrorData> errors = new ArrayList<MmbAncillaryErrorData>();
		
		if ("1".equals(request.getParameter("clear"))) {
			 errors = checkinSession.clearAncillariesFromCart(ctx, MmbAncillaryTypeEnum.EXTRA_BAGGAGE, null);
			
		} else {
			
			Map<Integer, Boolean> extraBaggageSelectionByAncillaryId = new HashMap<>();
			String[] split;
			String[] selections = request.getParameterValues("extrabaggage-selection");
			if (selections != null) {
				for (String selection : selections) {
					logger.debug("Processing extrabaggage-selection {}", selection);
					split = selection.split("#");
					String ancillaryId = split[0];
					String ancillaryValue = split[1];
					logger.debug("Evaluating ancillary id {} with value {}", ancillaryId, ancillaryValue);
					extraBaggageSelectionByAncillaryId.put(Integer.parseInt(ancillaryId), "1".equals(ancillaryValue));
				}
			}
			
			errors = checkinSession.addOrRemoveExtraBaggageAncillaryFormCart(ctx, extraBaggageSelectionByAncillaryId);
			
		}
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		//if (errors != null && !errors.isEmpty()) {
			json.key("result").value(true);
		/*} else {
			//FIXME
		}*/
		json.endObject();
		
		
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}

