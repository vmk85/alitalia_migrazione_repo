package com.alitalia.aem.consumer.booking.model.extrabaggage;

public final class EnumExtensions
{
	public static String CardCode(CardType enumValue)
	{
		switch (enumValue)
		{
			case Visa:
				return "VI";
			case MasterCard:
				return "CA";
			case Diners:
				return "DC";
			case AmericanExpress:
				return "AX";
			case Uatp:
				return "TP";
			case VisaElectron:
				return null;
			case JCB:
				return "JB";
			case Discover:
				return "DS";
			case Maestro:
				return "MA";
			default:
				return null;
		}
	}
}