package com.alitalia.aem.consumer.myalitalia.servlet;

import com.alitalia.aem.common.data.home.AdyenRecurringDetailData;
import com.alitalia.aem.common.data.home.AdyenRetrievalStoredPaymentData;
import com.alitalia.aem.common.data.home.MACreditCardData;
import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.data.home.MACustomerDataPayment;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.mapstruct.CrmMapper;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaConstants;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSession;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSessionContext;
import com.alitalia.aem.consumer.myalitalia.logger.MyAlitaliaIDLog;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.common.messages.crmdatarest.GetCrmDataInfoResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.gigya.GigyaSession;
import com.alitalia.aem.consumer.gigya.GigyaUtils;
import com.alitalia.aem.consumer.gigya.GigyaUtilsService;
import com.alitalia.aem.consumer.myalitalia.*;
import com.alitalia.aem.consumer.myalitalia.logger.MyAlitaliaLoginLog;
import com.alitalia.aem.consumer.myalitalia.logger.MyAlitaliaLogoutLog;
import com.alitalia.aem.consumer.myalitalia.logger.MyAlitaliaServletLog;
import com.alitalia.aem.consumer.myalitalia.model.MyAlitaliaOtpModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.web.component.myalitaliauser.delegaterest.IAdyenRecurringPaymentDelegate;
import com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.i18n.I18n;
import com.gigya.socialize.GSResponse;
import com.google.gson.Gson;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.Servlet;
import java.util.ArrayList;
import javax.servlet.http.Cookie;

@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = {"MyAlitaliaServlet-loginMa", "MyAlitaliaServlet-logoutMa", "MyAlitaliaServlet-UpdateSession", "MyAlitalia-isAvailableId", "MyAlitalia-resetPsw", "MyAlitaliaServlet-LinkAccount"}),
        @Property(name = "sling.servlet.methods", value = {"POST"}),
        @Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
        @Property(name = "sling.servlet.extensions", value = {"json"})
})
public class LoginMyAlitaliaServlet extends GenericMyAlitaliaFormValidatorServlet {

    @Reference
    private volatile MyAlitaliaSession myAlitaliaSession;

    @Reference
    private volatile AlitaliaConfigurationHolder configuration;

    @Reference
    private volatile GigyaSession gigyaSession;

    private MyAlitaliaLoginLog myAlitaliaLoginLog = new MyAlitaliaLoginLog();

    private MyAlitaliaLogoutLog myAlitaliaLogoutLog = new MyAlitaliaLogoutLog();

    private MyAlitaliaIDLog myAlitaliaIDLog = new MyAlitaliaIDLog();

    boolean associatoMM = false;

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) {

        Validator validator = new Validator();

        ResultValidation resultValidation = validator.validate();
        if (!resultValidation.getResult()) {
            myAlitaliaServletLog.error("Error in validateForm");

        }
        return resultValidation;

    }

    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

        final I18n i18n = new I18n(request.getResourceBundle(AlitaliaUtils.findResourceLocale(request.getResource())));

        String selectorString = request.getRequestPathInfo().getSelectorString();

        boolean isError = false, otpIsEnable = Boolean.parseBoolean(new HierarchyNodeInheritanceValueMap(request.getResource().getParent()).getInherited("maPaymentIsEnabled", String.class));
        String errorMessage = "";

        int errorCode = 0;

        GetCrmDataInfoResponse getCrmDataInfoResponse = null;

        String data = "";

        MyAlitaliaSessionContext mactx = null;

        if (selectorString.isEmpty()) {
            return;
        }

        switch (selectorString) {

            /** Reset password*/
            case "MyAlitalia-resetPsw":

                /** inizializzo le variabili per il reset/invio della mail */
                String emailReset = request.getParameter("email");

                String type = request.getParameter("type");

                String newPsw = request.getParameter("psw");

                String newPswConf = request.getParameter("pswConf");

                String regToken = request.getParameter("regToken");

                String retrieve = request.getParameter("retrieve");

                if (retrieve == null) {
                    retrieve = "false";
                }

                if (type == null) {

                    /** Controllo che la mail esista */
                    if (gigyaSession.isAvailableLoginID(emailReset)) {

                        isError = true;

                        errorMessage = "myalitalia.notValidEmail";

                    } else {

                        if (otpIsEnable) {

                            if (!retrieve.equals("true")) {

                                /** Preparo la query per i dati dell'otp relativi alla mail inserita */
                                String query = "SELECT data.resetPwdSms, data.phone1Number, data.phone1CountryCode, data.isStrong FROM accounts WHERE profile.email = \"" + emailReset + "\"";

                                /** Effettuo la chiamata al servizio*/
                                GSResponse gsResponse = gigyaSession.search(query);

                                /** Controllo che non ci siano errori in fase di retrive dati */
                                if (gsResponse.getErrorCode() == 0) {

                                    Gson gson = new Gson();

                                    /** Converto la risposta del servizio */
                                    MACustomer maCustomer = gson.fromJson(gsResponse.getResponseText(), MACustomer.class);

                                    /** Controllo che la query abbia avuto successo */
                                    if (maCustomer.getResults() != null && maCustomer.getResults()[0] != null) {

                                        /** Inizializzo le condizioni di recupero psw */
                                        String isStrong = maCustomer.getResults()[0].getData().getIsStrong();

                                        String numero = maCustomer.getResults()[0].getData().getPhone1Number();

                                        String prefisso = maCustomer.getResults()[0].getData().getPhone1CountryCode();

                                        /** Controllo se l'utente si è mai adeguato con otp*/
                                        if (isStrong != null && isStrong.equals("true")) {

                                            /** Controllo l'esistenza del prefisso */
                                            if (prefisso != null && prefisso.length() >= 2) {

                                                /** Controllo l'esistenza del numero */
                                                if (numero != null && numero.length() > 7) {

                                                    mactx = new MyAlitaliaSessionContext();

                                                    mactx.setMyAlitaliaOtpModel(new MyAlitaliaOtpModel());

                                                    /** Invio l'otp ( sendOtpWithApp )*/
                                                    SendOTPWithAppResponse sendOTPWithAppResponse = myAlitaliaSession.SendVerificationOtp(prefisso, numero, i18n.get("myalitalia.otp.smsdescription"));

                                                    /** Controllo che l'invio sia stato effettuato ( non si ha l'effettiva evidenza dell'invio, ma solo della corretta invocazione del servizio )*/
                                                    if (sendOTPWithAppResponse != null) {

                                                        /** Inserisco la response in sessione */
                                                        mactx.getMyAlitaliaOtpModel().setOTP(sendOTPWithAppResponse.getOTP().getValue());
                                                        mactx.getMyAlitaliaOtpModel().setStartDate(MyAlitaliaUtils.getActualDate());
                                                        mactx.getMyAlitaliaOtpModel().setExpMillisecondOtp(MyAlitaliaUtils.getDifferenceDate(sendOTPWithAppResponse.getExpiryDate().getValue(),sendOTPWithAppResponse.getSentDate().getValue()));
                                                        mactx.getMyAlitaliaOtpModel().setNumero(numero);
                                                        mactx.getMyAlitaliaOtpModel().setPrefisso(prefisso);
                                                        MyAlitaliaUtils.setMactxToSession(request, mactx);
                                                    } else {
                                                        /** Se la chiamata non ha avuto successo, mando in errore */
                                                        isError = true;
                                                    }
                                                    /** Nascondo il numero di telefono al quale è stato inviato l'otp ( verrà mostrato nella modale di conferma otp ) */
                                                    numero = blearNumber(numero);

                                                    JSONObject jsonObject = new JSONObject();

                                                    jsonObject.put("numero", numero);

                                                    data = jsonObject.toString();

                                                } else {

                                                    /** Se l'utente non è strong, mando in errore e mostro la modale per il contatto al callCenter*/
                                                    isError = true;

                                                    errorMessage = "myalitalia.user.notstrong";

                                                }
                                            } else {

                                                /** Se l'utente non è strong, mando in errore e mostro la modale per il contatto al callCenter*/
                                                isError = true;

                                                errorMessage = "myalitalia.user.notstrong";

                                            }

                                        } else {

                                            /** Se l'utente non è strong, mando in errore e mostro la modale per il contatto al callCenter*/
                                            isError = true;

                                            errorMessage = "myalitalia.user.notstrong";

                                        }

                                    } else {

                                        /** Se l'utente non è strong, mando in errore e mostro la modale per il contatto al callCenter*/
                                        isError = true;

                                        errorMessage = "myalitalia.user.notstrong";

                                    }


                                } else {

                                    /** Se gigya non risponde, mando in errore e mostro mesaggio di errore*/
                                    isError = true;

                                    errorMessage = "myalitalia.service.error";

                                }
                            } else {

                                /** Chiamata al servizio per l'invio della mail ( La gestione dell'invio della mail è in carico a gigya ) */
                                gigyaSession.resetPassword(emailReset, "secretA");

                                JSONObject jsonObject = new JSONObject();

                                jsonObject.put("message", "mail inviata");

                                data = jsonObject.toString();
                            }

                        } else {

                            /** Chiamata al servizio per l'invio della mail ( La gestione dell'invio della mail è in carico a gigya ) */
                            gigyaSession.resetPassword(emailReset, "secretA");

                            JSONObject jsonObject = new JSONObject();

                            jsonObject.put("message", "mail inviata");

                            data = jsonObject.toString();
                        }

                    }

                } else {

                    if (type.equals("reset") && newPsw.equals(newPswConf)) {

                        /** Chiamata la servizio per impostare la nuova password ( Il secondo parametro viene passato al metodo ma ignorato in fase di chiamata ) */
                        if (gigyaSession.resetPassword(newPsw, newPswConf, regToken)) {

                            /** Se non è stato possibile resettare la password, mando in errore */
                            isError = true;
                        }

                    } else {

                        /** Se le password non coincidono, mando in errore*/
                        isError = true;

                        errorMessage = "myalitalia.notValidPassword";
                    }
                }

                break;

            /** Controllo esistenza del loginId inserito ( Utilizzato in fase di login ) */
            case "MyAlitalia-isAvailableId":

                String email = request.getParameter("email");

                if (gigyaSession.isAvailableLoginID(email)) {

                    isError = true;

                    errorMessage = "myalitalia.notValidEmail";

                }

                break;

            /** Login a MyAlitalia (Gigya e CRM ) */
            case "MyAlitaliaServlet-loginMa":

                /** Effettuo il login su Gigya ( Prendo i dati del customer )*/
                MACustomer maCustomer = GigyaUtils.gigyaLogin(request, gigyaSession);

                /** Controllo che sia andato a buon fine il login su gigya */
                if (maCustomer != null && maCustomer.getErrorCode() == 0) {

                    myAlitaliaLoginLog.debug("Caricamento utente in sessione");

                    /** Effettuo il login su CRM ( Get dei dati personali, documenti di viaggio, preferenze e consensi ) */
                    mactx = actionUserMyAlitalia(request, MyAlitaliaConstants.MYALITALIA_ACTION_LOGIN, maCustomer);

                    /** Se non è stato possibile salvare l'utente in sessione, mando in errore */
                    if (mactx == null) {

                        isError = true;

                        errorMessage = MyAlitaliaConstants.MYALITALIA_ERROR_MESSAGE_SESSION;

                        myAlitaliaLoginLog.error("Utente non caricato correttamente in sessione");

                    }

                    /** Se il CRM non risponde, mando in errore */
                    else if (mactx.getGetCrmDataInfoResponse() == null) {

                        isError = true;

                        errorMessage = MyAlitaliaConstants.MYALITALIA_SERVICE_NOT_RESPONSE;

                        myAlitaliaLoginLog.error("Nessuna risposta dal servizio. Utente [" + mactx.getMaCustomer().getUID() + "]");

                    }

                    /** Se CRM restituisce errore, gestisco la casistica */
                    else if (mactx.getGetCrmDataInfoResponse().getResult() != null) {

                        if (mactx.getGetCrmDataInfoResponse().getResult().getStatus().equals(MyAlitaliaConstants.MYALITALIA_SERVICE_ERROR)) {

                            isError = true;

                            /** Seil CRM non è a conoscenza dell'utente gigya registrato, mando in errore */
                            if (mactx.getGetCrmDataInfoResponse().getResult().getDescription().contains("ID missing in DB")) {

                                errorMessage = MyAlitaliaConstants.MYALITALIA_SERVICE_MISSING_USER;

                            } else {

                                /** Gestisco eventuali errori restituiti */
                                errorMessage = MyAlitaliaConstants.MYALITALIA_SERVICE_NOT_RESPONSE;

                            }

                            myAlitaliaLoginLog.error("Errore getGetCrmDataInfoResponse per l'utente [" + mactx.getMaCustomer().getUID() + "] in fase di login : " + mactx.getGetCrmDataInfoResponse().getResult().toString());

                        } else {

                            JSONObject jsonObject = new JSONObject();

                            /** imposto il nome da mostrare */
                            if (!mactx.getGetCrmDataInfoResponse().getInfoCliente().getNome().equals("") && !mactx.getGetCrmDataInfoResponse().getInfoCliente().getCognome().equals("")) {

                                jsonObject.put("nome", mactx.getGetCrmDataInfoResponse().getInfoCliente().getNome());
                                jsonObject.put("cognome", mactx.getGetCrmDataInfoResponse().getInfoCliente().getCognome());

                            } else {
                                jsonObject.put("email", mactx.getGetCrmDataInfoResponse().getEmail());
                            }

                            /** Imposto i parametri per la percentuale */
                            jsonObject.put("percent", mactx.getProfileComplite().getPercent());
                            jsonObject.put("redirectUrl", mactx.getProfileComplite().getRedirectUrl());
                            jsonObject.put("text", mactx.getProfileComplite().getTesto());

                            /** Setto il cookie di sessione*/
                            jsonObject.put("cookieName", maCustomer.getSessionInfo().getCookieName());
                            jsonObject.put("cookieValue", maCustomer.getSessionInfo().getCookieValue());

                            /** Numero voli utente logato */
                            if (mactx.getMyFlightsModel() != null) {
                                jsonObject.put("totalFlightsForManage", mactx.getMyFlightsModel().getTotalFlightsForManage());
                                jsonObject.put("totalFlightsAvailableForCheckin", mactx.getMyFlightsModel().getTotalFlightsAvailableForCheckin());
                            }

                            /** Verifico se mostrare la modale di inserimento OTP*/
                            if (otpIsEnable) {
                                if (mactx.getMaCustomer().getData().getIsStrong() == null || mactx.getMaCustomer().getData().getPhone1CountryCode() == null || mactx.getMaCustomer().getData().getPhone1CountryCode() == null) {

                                    /** Setto la variabile di accesso per utenti pregressi alla data di rilascio dell'otp*/
                                    if (!mactx.getMaCustomer().getData().isFirstAccess()) {
                                        try {
                                            JSONObject obj2 = new JSONObject();
                                            obj2.put("isFirstAccess", true);
                                            gigyaSession.setAccount(mactx.getMaCustomer().getUID(), GigyaUtils.mapJsonObjGigya(obj2.toString()));
                                        } catch (org.apache.sling.commons.json.JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    jsonObject.put("showModal", true);
                                }
                            }

                            data = jsonObject.toString();

                        }

                    } else {

                        isError = true;

                        errorMessage = MyAlitaliaConstants.MYALITALIA_SERVICE_NOT_RESPONSE;

                        myAlitaliaLoginLog.error("Nessuna risposta dal servizio. Utente [" + mactx.getMaCustomer().getUID() + "]");

                    }

                    /** Controllo che non ci siano stati errori in fase di login */
                    if (!isError && otpIsEnable) {

                        /** Prendo da Adyen eventuali carte di credito associate al Customer loggato */
                        populateMACreditCardsData(request, mactx.getMaCustomer());
                    }

                } else {

                    isError = true;

                    if (maCustomer != null) {

                        if (maCustomer.getErrorMessage().equals("Invalid LoginID")) {

                            errorMessage = "myalitalia.invalid.credential";

                        } else {

                            errorCode = maCustomer.getErrorCode();


                            errorMessage = MyAlitaliaConstants.MYALITALIA_SERVICE_NOT_RESPONSE;

                        }

                    } else {

                        errorMessage = MyAlitaliaConstants.MYALITALIA_SERVICE_NOT_RESPONSE;

                    }

                    myAlitaliaLoginLog.error("IMPOSSIBILE effettuare il login a Gigya: errore = " + errorMessage);

                }

                if (isError) {

                    MyAlitaliaUtils.setMactxToSession(request, null);

                } else {

                    MyAlitaliaUtils.setMactxToSession(request, mactx);

                }
                break;

            /** Logout */
            case "MyAlitaliaServlet-logoutMa":

                maCustomer = MyAlitaliaUtils.getMaCustomerBySession(request);
                JSONObject jsonObject = new JSONObject();

                /** Restituisco il cookie di gigya da eliminare*/
                jsonObject.put("cookieValue", getCookieAuthSession(request));
                jsonObject.put("cookieName", getCookieNameByValue(request, getCookieAuthSession(request)));
                data = jsonObject.toString();
                actionUserMyAlitalia(request, MyAlitaliaConstants.MYALITALIA_ACTION_LOGOUT, new MACustomer());
                break;

            /** Refresh dela sessione ( Ogni pagina, serve per aggiornare continuamente il cookie di login ) */
            case "MyAlitaliaServlet-UpdateSession":

                /** Refresh della sessione */
                data = refeshSession(request, otpIsEnable);

                if (data.equals("{}")) {

                    isError = true;

                }

                break;

            case "MyAlitaliaServlet-LinkAccount":

                JSONObject jsonObjectLink = new JSONObject();

                String psw = request.getParameter("password");
                String token = request.getParameter("authToken");

                /** Getaccount*/
                GSResponse gsResponse = gigyaSession.getAccountLinkAccount(token);

                if (gsResponse.getErrorCode() == 0) {

                    Gson gson = new Gson();
                    maCustomer = gson.fromJson(gsResponse.getResponseText(), MACustomer.class);

                    if (psw != null && maCustomer.getProfile().getEmail() != null) {

                        gsResponse = gigyaSession.login(maCustomer.getProfile().getEmail(), psw);

                        if (gsResponse.getErrorCode() == 0) {

                            MACustomer maCustomerLogged = gson.fromJson(gsResponse.getResponseText(), MACustomer.class);

                            jsonObjectLink.put("cookieName", maCustomerLogged.getSessionInfo().getCookieName());
                            jsonObjectLink.put("cookieValue", maCustomerLogged.getSessionInfo().getCookieValue());

                            data = jsonObjectLink.toString();

                        } else {

                        }
                    }


                } else {


                }
                break;
        }

        try {

            response.setContentType("application/json");
            TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
            json.object();
            json.key("isError").value(isError);
            json.key("errorMessage").value(errorMessage);
            json.key("errorCode").value(errorCode);
            json.key("data").value(data);
            myAlitaliaServletLog.debug("Json di risposta LoginMyAlitaliaServlet " + json.toString());
            json.endObject();
        } catch (Exception e) {
            myAlitaliaServletLog.error("Unexected error generating JSON response." + e.getMessage());
        }
    }

    private String refeshSession(SlingHttpServletRequest request, boolean otpIsEnable) throws Exception {

        /** Inizializzio le variabili per il refresh */
        String authToken = "";
        String data = "";
        String requestauthToken = request.getParameter("authToken");
        MACustomer maCustomer = MyAlitaliaUtils.getMaCustomerBySession(request);
        JSONObject jsonObject = new JSONObject();
        String refresh = request.getParameter("refresh");

        /** Controllo se mi sto loggando tramite email di conferma registrazione */
        if (requestauthToken != null) {
            authToken = requestauthToken;
        }

        /** Se ho il token salvto in sessione, utilizzo quello per effettuare la chiamata e aggiornare i dati */
        else if (maCustomer != null && maCustomer.getSessionInfo() != null) {
            authToken = maCustomer.getSessionInfo().getCookieValue();
        }

        /** Se non ho il token in sessione, lo prendo dal cookie ( Tipicamente accade quando si effettua il social login ) */
        else {
            authToken = getCookieAuthSession(request);
        }

        if (authToken != ""  && refresh == null) {

            /** Chiamata al servizio per prendere tutti i dati dell'utente ( Con la finalizeRegistration viene restituito anche il cookie di sessione ) */
            GSResponse gsResponse = gigyaSession.finalizeRegistration(authToken);

            /** Controllo che non ci siano stati errori */
            if (gsResponse.getErrorCode() == 0) {
                Gson gson = new Gson();
                MACustomer userMA = gson.fromJson(gsResponse.getResponseText(), MACustomer.class);

                myAlitaliaIDLog.info("UID: [" + userMA.getUID().toUpperCase() + "], JSessionID: " + request.getSession().getId());

                if (maCustomer != null) {

                    /** Uso mapStruct per mappare il vecchio customer con il nuovo */
                    userMA.setData(CrmMapper.MAPPER.mapMaCustomerGigyaResponse(maCustomer.getData()));
                }

                /** Aggiorno i dati di pagamento per l'utente in sessione */
                if (userMA.getData().getPayment() == null && otpIsEnable) {
                    populateMACreditCardsData(request, userMA);
                }

                /** Setto i dati per il primo accesso */
                if (!userMA.getData().isFirstAccess()) {
                    try {
                        JSONObject obj2 = new JSONObject();
                        obj2.put("isFirstAccess", true);
                        gigyaSession.setAccount(userMA.getUID(), GigyaUtils.mapJsonObjGigya(obj2.toString()));
                    } catch (org.apache.sling.commons.json.JSONException e) {
                        e.printStackTrace();
                    }
                }

                /** Imposto il nuovo cookie di sessione */
                jsonObject.put("cookieName", userMA.getSessionInfo().getCookieName());
                jsonObject.put("cookieValue", userMA.getSessionInfo().getCookieValue());

                MyAlitaliaUtils.setMaCustomerToSession(request, myAlitaliaSession, userMA);

                if (MyAlitaliaUtils.getMyAlitaliaCrmDataBySession(request) == null) {

                    /** Aggiorno i dati dal CRM */
                    MyAlitaliaUtils.setCrmResponseDataToSession(request, myAlitaliaSession.getCrmDataInfoResponse(MyAlitaliaUtils.prepareGetCrmDataInfoRequest(request, userMA)));

                }
            }
        }

        MyAlitaliaSessionContext mactx = MyAlitaliaUtils.getMactxBySession(request);

        if (mactx != null && mactx.getGetCrmDataInfoResponse() != null && mactx.getMaCustomer() != null) {

            if (!mactx.getGetCrmDataInfoResponse().getInfoCliente().getNome().equals("") && !mactx.getGetCrmDataInfoResponse().getInfoCliente().getCognome().equals("")) {

                /** Dati per aggiornare l'header loggato */
                jsonObject.put("nome", mactx.getGetCrmDataInfoResponse().getInfoCliente().getNome());
                jsonObject.put("cognome", mactx.getGetCrmDataInfoResponse().getInfoCliente().getCognome());

            } else {
                jsonObject.put("email", mactx.getGetCrmDataInfoResponse().getEmail());
            }

            /** Imposto i parametri per la percentuale */
            jsonObject.put("percent", mactx.getProfileComplite().getPercent());
            jsonObject.put("redirectUrl", mactx.getProfileComplite().getRedirectUrl());
            jsonObject.put("text", mactx.getProfileComplite().getTesto());
            jsonObject.put("identities", mactx.getMaCustomer().getIdentities());

            /** Numero voli utente logato */
            if (mactx.getMyFlightsModel() != null) {
                jsonObject.put("totalFlightsForManage", mactx.getMyFlightsModel().getTotalFlightsForManage());
                jsonObject.put("totalFlightsAvailableForCheckin", mactx.getMyFlightsModel().getTotalFlightsAvailableForCheckin());
            }

            /** Verifico se mostrare la modale di inserimento OTP*/
            if (otpIsEnable) {
                if (mactx.getMaCustomer().getData().getIsStrong() == null || mactx.getMaCustomer().getData().getPhone1CountryCode() == null || mactx.getMaCustomer().getData().getPhone1CountryCode() == null) {
                    jsonObject.put("showModal", true);
                }
            }
        }

        data = jsonObject.toString();

        if (MyAlitaliaUtils.checkSession(request)) {
            myAlitaliaServletLog.debug("Utente UID: " + MyAlitaliaUtils.getMactxBySession(request).getGetCrmDataInfoResponse().getIdMyAlitalia() + " Ricaricato correttamente");
        }
        return data;
    }

    /**
     * Metodo per estrarre il valore del cookie di sessione dalla request
     */
    private String getCookieAuthSession(SlingHttpServletRequest request) {
        Cookie[] cookies = new Cookie[request.getCookies().length];
        String authToken = "";
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            if (cookies[i].getName().contains(configuration.getMaApiKey())) {
                authToken = cookies[i].getValue();
            }
        }

        return authToken;
    }

    /**
     * Metodo per estrarre il nome del cookie di sessione dalla request
     */
    private String getCookieNameByValue(SlingHttpServletRequest request, String value) {
        Cookie[] cookies = new Cookie[request.getCookies().length];
        String authToken = "";
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            if (cookies[i].getName().contains(configuration.getMaApiKey())) {
                if (cookies[i].getValue().equals(value)) {
                    authToken = cookies[i].getName();
                }
            }
        }

        return authToken;
    }

    /**
     * Metodo per effettuare il login/logout
     */
    private MyAlitaliaSessionContext actionUserMyAlitalia(SlingHttpServletRequest request, String action, MACustomer maCustomer) throws Exception {

        MyAlitaliaSessionContext mactx = null;

        switch (action) {

            case "LOGIN":
                myAlitaliaLoginLog.debug("Inizio Caricamento utente in sessione");
                /** Effettuo il login */
                mactx = doLogin(request, maCustomer);
                myAlitaliaLoginLog.debug("Fine Caricamento utente in sessione " + mactx.toString());
                break;

            case "LOGOUT":
                /** Effettuo il logout */
                associatoMM = doLogout(request);
                break;
        }
        return mactx;
    }

    /**
     * Metodo per effettuare il login
     */
    private MyAlitaliaSessionContext doLogin(SlingHttpServletRequest request, MACustomer userMA) {

        MyAlitaliaSessionContext mactx = null;

        /** Controllo che l'esistenza del Customer gigya in sessione ( Prvngo perdite di sessione ) */
        if (userMA != null && userMA.getUID() != null) {

            myAlitaliaLoginLog.debug("Inizializzo la sessione");

            /** Inizializzo la sessione con l'utente gigya e i dati dal CRM */
            mactx = myAlitaliaSession.initializeSession(request, userMA);

            myAlitaliaLoginLog.debug("Sessione MyAlitalia inizializzata: " + mactx.toString());

            /** Salvo utente loggato in sessione */
            boolean success = MyAlitaliaUtils.setMactxToSession(request, mactx);
            if (success) {
                myAlitaliaLoginLog.info("Utente UID: " + mactx.getGetCrmDataInfoResponse().getIdMyAlitalia() + " Loggato correttamente, JSessionID utente = [" + request.getSession().getId() + "]");
                myAlitaliaIDLog.info("UID: [" + mactx.getGetCrmDataInfoResponse().getIdMyAlitalia() + "], JSessionID: " + request.getSession().getId());
                myAlitaliaLoginLog.debug("Utente caricato correttamente in sessione " + mactx.toString());
            }

        }

        return mactx;
    }

    /**
     * Metodo per effettuare il logout
     */
    private boolean doLogout (SlingHttpServletRequest request) throws Exception{

        boolean associatoMM = false;

        /** Prendo l'utente dalla sessione */
        MyAlitaliaSessionContext myAlitaliaSessionContext = MyAlitaliaUtils.getMactxBySession(request);

        if (myAlitaliaSessionContext != null) {

            if(myAlitaliaSessionContext.getMaCustomer().getData().getId_mm() != null) {
                associatoMM = true;
            }

            myAlitaliaLogoutLog.info("Effettuo il logout dell'utenza: " + request.getSession().getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE).toString());
            myAlitaliaLogoutLog.info("Utente UID: " + myAlitaliaSessionContext.getGetCrmDataInfoResponse().getIdMyAlitalia() + " Logout correttamente");
            try {

                /** Chiamata a gigya per effettuare il logout */
                gigyaSession.logout(myAlitaliaSessionContext.getGetCrmDataInfoResponse().getIdMyAlitalia());
            } catch (Exception e) {
            }
        }

        /** Rimuovo l'utente dalla sessione ( Logout Terminato ) */
        request.getSession().removeAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);

        return associatoMM;
    }

//    private Map<String, String> setJsonResponse() {
//
//        Map<String, String> jsonResponse = setStandardResponse();
//
//        return jsonResponse;
//    }
//
//    private Map<String, String> setJsonResponse(Map<String, String> otherParamiterMap) {
//
//        otherParamiterMap.putAll(setStandardResponse());
//
//        return jsonResponse;
//    }

//    private Map<String, String> setStandardResponse() {
//        Map<String, String> standardResponse = new HashMap<>();
//        standardResponse.put("isError", String.valueOf(standardJsonResponse.isError()));
//        standardResponse.put("errorMessage", standardJsonResponse.getErrorMessage());
//        standardResponse.put("successMessage", standardJsonResponse.getSuccessMessage());
//        return standardResponse;
//    }

//    private void setBaseSuccessResponse() {
//        standardJsonResponse.setError(false);
//        standardJsonResponse.setErrorMessage("");
//        standardJsonResponse.setSuccessMessage(MyAlitaliaConstants.MYALITALIA_SUCCESS_MESSAGE_SESSION);
//    }

    //    private void successJsonServlet(SlingHttpServletResponse response, Map<String, String> jsonResponse) throws IOException, JSONException {
//        response.setContentType("application/json");
//        TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
//        json.object();
//        for (String key : jsonResponse.keySet()) {
//            for (String value : jsonResponse.values()) {
//                if (jsonResponse.get(key).equals(value)) {
//                    json.key(key);
//                    json.value(value);
//                }
//            }
//        }
//        logger.info("Json di risposta LoginMyAlitaliaServlet " + json.toString());
//        json.endObject();
//    }
//Tolentino - Inizio
    @Reference
    private volatile IAdyenRecurringPaymentDelegate adyenRecurringPaymentDelegate;

    private void populateMACreditCardsData(SlingHttpServletRequest request, MACustomer userMA) {
        logger.info("MAPayment - populateMACreditCardsData: Recupera Carte di Credito in Adyen");

        try {
            ArrayList<MACreditCardData> maCreditCardsData = null;

            AdyenRetrievalStoredPaymentRequest adyenRetrievalStoredPaymentRequest = new AdyenRetrievalStoredPaymentRequest();
            adyenRetrievalStoredPaymentRequest.setShopperReference(userMA.getUID());
            adyenRetrievalStoredPaymentRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
            adyenRetrievalStoredPaymentRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
            adyenRetrievalStoredPaymentRequest.setConversationID(IDFactory.getTid());

            logger.info("MAPayment - populateMACreditCardsData: invocando adyenRecurringPaymentDelegate.retrievalStoredPayment...");
            AdyenRetrievalStoredPaymentResponse adyenRetrievalStoredPaymentResponse = adyenRecurringPaymentDelegate.retrievalStoredPayment(adyenRetrievalStoredPaymentRequest);
            if (adyenRetrievalStoredPaymentResponse != null && adyenRetrievalStoredPaymentResponse.getAdyenRetrievalStoredPaymentList() != null &&
                    adyenRetrievalStoredPaymentResponse.getAdyenRetrievalStoredPaymentList().isEmpty() == false) {
                for (AdyenRetrievalStoredPaymentData adyenRetrievalStoredPayment : adyenRetrievalStoredPaymentResponse.getAdyenRetrievalStoredPaymentList()) {
                    if (adyenRetrievalStoredPayment != null && adyenRetrievalStoredPayment.getRecurringDetailList() != null && adyenRetrievalStoredPayment.getRecurringDetailList().isEmpty() == false) {
                        for (AdyenRecurringDetailData recurringDetail : adyenRetrievalStoredPayment.getRecurringDetailList()) {
                            if (recurringDetail != null && recurringDetail.getCard() != null) {
                                MACreditCardData maCreditCardData = new MACreditCardData(recurringDetail.getCard().getType(), recurringDetail.getPaymentMethodVariant(), recurringDetail.getCard().getNumber(),
                                        recurringDetail.getCard().getCvc(), recurringDetail.getCard().getHolderName(),recurringDetail.getCard().getExpiryMonth().length() < 2 ? "0" + recurringDetail.getCard().getExpiryMonth() : recurringDetail.getCard().getExpiryMonth(), recurringDetail.getCard().getExpiryYear(),
                                        recurringDetail.getAlias(), recurringDetail.getFirstPspReference(), recurringDetail.getRecurringDetailReference(), recurringDetail.getAdditionalDataList().get(0).getCardBin());

                                if (recurringDetail.getAddress() != null) {
                                    maCreditCardData.fillAddressData(recurringDetail.getAddress().getPostalCode(), recurringDetail.getAddress().getCity(), recurringDetail.getAddress().getCountry(), recurringDetail.getAddress().getStreet(),
                                            recurringDetail.getAddress().getProvince());
                                }

                                if (maCreditCardsData == null) maCreditCardsData = new ArrayList<MACreditCardData>();
                                maCreditCardsData.add(maCreditCardData);

                                logger.debug("MAPayment - populateMACreditCardsData: Recuperata carta di credito [{}]", maCreditCardData.print());
                            }
                        }
                    }
                }
            }

            if (maCreditCardsData != null) setCreditCardsData(userMA, maCreditCardsData);
        } catch (Exception e) {
            logger.error("MAPayment - populateMACreditCardsData: errore durante il recupero Carte di Credito in Adyen [{}]", e.getMessage());
        }
    }

    private void setCreditCardsData(MACustomer userMA, ArrayList<MACreditCardData> maCreditCardsData) {
        if (userMA != null && userMA.getData() != null) {
            if (userMA.getData().getPayment() == null) userMA.getData().setPayment(new MACustomerDataPayment());
            userMA.getData().getPayment().setCreditCardsData(maCreditCardsData);
        }
    }
    //Tolentino - Fine

    private String blearNumber(String number) {
        String firstNum = String.valueOf(number.charAt(0)).concat(String.valueOf(number.charAt(1)));
        String lastNum = number.substring(number.length() - 3);
        return firstNum.concat("*****").concat(lastNum);
    }

    protected AlitaliaConfigurationHolder getConfiguration() {
        return configuration;
    }

}
