package com.alitalia.aem.consumer.gigya.logger;

public interface GigyaLogger {

    public void error(String log);

    public void debug(String log);

    public void info(String log);

    public void trace(String log);

}
