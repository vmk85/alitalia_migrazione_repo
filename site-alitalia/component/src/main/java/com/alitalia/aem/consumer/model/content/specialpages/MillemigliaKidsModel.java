package com.alitalia.aem.consumer.model.content.specialpages;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MillemigliaKidsModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private MillemigliaKidsData millemigliaKidsData;

	private List<Integer> days = new ArrayList<Integer>();
	private List<String> months = new ArrayList<String>();
	private List<Integer> years = new ArrayList<Integer>();
	private List<String> genders= new  ArrayList<String>();
	
	@PostConstruct
	protected void initModel() {
		logger.debug("[MillemigliaKidsModel] initModel");

		super.initBaseModel(slingHttpServletRequest);

		// retrieve and remove ComfortSeatsClaimData from session if any
		millemigliaKidsData = (MillemigliaKidsData)
				slingHttpServletRequest.getSession().getAttribute(
						MillemigliaKidsData.NAME);
		slingHttpServletRequest.getSession().removeAttribute(
				MillemigliaKidsData.NAME);

		// Init date controls
		for (int i = 1; i <= 31; i++) { 
			days.add(i);
		}
		
		months = I18nKeyCommon.getMonths();
		
		Calendar c = new GregorianCalendar();
		int thisYear = c.get(Calendar.YEAR);
		for (int i = thisYear -14 ; i <= thisYear-2; i++) {
			years.add(i);
		}
		
		genders = I18nKeyCommon.getGenders();
		
	}

	public MillemigliaKidsData getMillemigliaKidsData() {
		return millemigliaKidsData;
	}

	public List<Integer> getDays() {
		return days;
	}

	public List<String> getMonths() {
		return months;
	}

	public List<Integer> getYears() {
		return years;
	}

	public List<String> getGenders(){
		return genders;
	}

}