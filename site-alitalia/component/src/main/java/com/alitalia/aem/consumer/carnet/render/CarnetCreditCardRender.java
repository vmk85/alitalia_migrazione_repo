package com.alitalia.aem.consumer.carnet.render;

import com.alitalia.aem.common.data.home.DictionaryItemData;
import com.alitalia.aem.common.data.home.carnet.CarnetPaymentTypeItemData;

public class CarnetCreditCardRender {
	
private CarnetPaymentTypeItemData paymentTypeItemData;
	
	public CarnetCreditCardRender(CarnetPaymentTypeItemData paymentTypeItemData) {
		this.paymentTypeItemData = paymentTypeItemData;
	}
	
	public CarnetPaymentTypeItemData getPaymentTypeItemData() {
		return this.paymentTypeItemData;
	}
	
	public Integer getCvvMaxLenght() {
		for (DictionaryItemData data : this.paymentTypeItemData.getOtherInfo()) {
			if (data.getKey().equals("MaxCVC")) {
				return (Integer) data.getValue();
			}
		}
		
		return 3;
	}
	
	public String getImgName() {
		switch (this.paymentTypeItemData.getCode()) {
			case "AX":
				return "AmericanExpress";
			case "DC":
				return "Diners";
			case "MC":
				return "MasterCard";
			case "TP":
				return "Uatp";
			case "VE":
				return "VisaElectron";
			case "CV":
				return "Visa";
			default:
				break;
		}
		
		return "";
	}

}
