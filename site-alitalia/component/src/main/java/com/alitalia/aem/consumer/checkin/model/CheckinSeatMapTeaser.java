package com.alitalia.aem.consumer.checkin.model;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryUpgradeDetailData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinSeatMapTeaser extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private CheckinSession checkinSession;
	
	private List<CheckinPassengerData> eligibleBusinessPassengers;
	private List<CheckinPassengerData> eligiblePremiumEconomyPassengers;
	private boolean showUpgradeBusiness;
	private boolean showUpgradePremiumEconomy;
	private MmbPriceRender businessUpgradePrice;
	private MmbPriceRender premiumEcoUpgradePrice;
	private boolean oneUpgradeOnly;
	
	private int purchasableSeatsNum;
	
	@PostConstruct
	protected void initModel() {
		
		try {
			initBaseModel(request);
			/*Numero di posti acquistabili*/
			purchasableSeatsNum = getPurchasableSeatNum();
			
			//Gestione UPGRADE
			showUpgradeBusiness = false;
			showUpgradePremiumEconomy = false;
			
			if (CheckinUtils.obtainBusFlight(ctx.selectedRoute).isEmpty()) {
				MmbCompartimentalClassEnum businessClass = checkinSession.getBusinessCompClass(ctx);
				MmbCompartimentalClassEnum premiumEcoClass = checkinSession.getPremiumEcoCompClass(ctx);
	
				eligibleBusinessPassengers = checkinSession.getUpgradeEligiblePassengers(ctx, businessClass, false);
				showUpgradeBusiness = !eligibleBusinessPassengers.isEmpty();
				
				if(premiumEcoClass != null){
					eligiblePremiumEconomyPassengers = checkinSession.getUpgradeEligiblePassengers(ctx, premiumEcoClass, false);
					showUpgradePremiumEconomy = !eligiblePremiumEconomyPassengers.isEmpty();
				}
				
				
				if(ctx.cart != null){
					List<MmbAncillaryData> upgrades = CheckinUtils.getAncillaryFromCartByType(
							ctx.cart, MmbAncillaryTypeEnum.UPGRADE);
					if(!upgrades.isEmpty()){
						if(showUpgradeBusiness){
							businessUpgradePrice = getUpgradePrice(upgrades, businessClass);
						}
						if(showUpgradePremiumEconomy){
							premiumEcoUpgradePrice = getUpgradePrice(upgrades, premiumEcoClass);
						}
						if(!showUpgradeBusiness && !showUpgradePremiumEconomy){
							logger.info("No ancillary of type upgrade in status available found in cart for PNR " + ctx.pnr);
						}
					}
					else{
						logger.info("No ancillary of type upgrade found in cart for PNR " + ctx.pnr);
					}
				}
			
			}
			
			oneUpgradeOnly = (showUpgradeBusiness && ! showUpgradePremiumEconomy) 
					|| (!showUpgradeBusiness && showUpgradePremiumEconomy);
			
		} catch (Exception e) {
			logger.error("Unexpected error:", e);
		}
	}
	
	private MmbPriceRender getUpgradePrice(List<MmbAncillaryData> ancillaries
			, MmbCompartimentalClassEnum seatClass){
		MmbPriceRender price = null;
		if(ancillaries.size() > 0){
			price = checkinSession.getAncillaryDisplayedPriceInfo(ctx
					, ancillaries.stream()
					.filter(anc -> MmbAncillaryStatusEnum.AVAILABLE.equals(anc.getAncillaryStatus()))
					.filter(anc ->(anc.getAncillaryDetail() == null ? false : 
						((CheckinAncillaryUpgradeDetailData) anc.getAncillaryDetail())
						.getCompartimentalClass().equals(seatClass)))
					.map( anc -> (anc == null ? new MmbAncillaryData() : anc ))
					.findFirst()
					.orElse(new MmbAncillaryData()), 1);
		}
		return price;
	}
	
	private int getPurchasableSeatNum(){
		int result = 0;
		MmbAncillaryStatusEnum[] statuses = {MmbAncillaryStatusEnum.AVAILABLE};
		if(ctx.cart != null && !ctx.cart.getAncillaries().isEmpty()){
			result = (int) ctx.cart.getAncillaries().stream().filter(anc 
					-> anc.getAncillaryType().equals(MmbAncillaryTypeEnum.SEAT) 
					&& Arrays.asList(statuses).contains(anc.getAncillaryStatus())
					&& (!anc.getAmount().equals(BigDecimal.ZERO) || (anc.getQuotations() != null 
					&& !anc.getQuotations().get(0).getPrice().equals(BigDecimal.ZERO)))
					).count();
		}
		return result ;
	}
	
	public boolean getShowUpgradeBusiness() {
		return showUpgradeBusiness;
	}
	
	public boolean getShowUpgradePremiumEconomy() {
		return showUpgradePremiumEconomy;
	}

	public List<CheckinPassengerData> getEligibleBusinessPassengers() {
		return eligibleBusinessPassengers;
	}

	public List<CheckinPassengerData> getEligiblePremiumEconomyPassengers() {
		return eligiblePremiumEconomyPassengers;
	}

	public boolean isOneUpgradeOnly() {
		return oneUpgradeOnly;
	}

	public MmbPriceRender getBusinessUpgradePrice() {
		return businessUpgradePrice;
	}

	public MmbPriceRender getPremiumEcoUpgradePrice() {
		return premiumEcoUpgradePrice;
	}

	public int getPurchasableSeatsNum() {
		return purchasableSeatsNum;
	}

}
