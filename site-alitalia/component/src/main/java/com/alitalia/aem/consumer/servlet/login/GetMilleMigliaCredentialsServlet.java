package com.alitalia.aem.consumer.servlet.login;

import java.io.IOException;

import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.day.cq.commons.TidyJSONWriter;

/**
 * Servlet exposing a REST-based service to retrieve the MilleMiglia
 * credentials of the currently logged-in user (if any).
 * 
 * <p>No input parameters are required.</p>
 * 
 * <p>The output is a JSON object with the following attributes:
 * <ul>
 * <li>successful: true for a successful operation, false otherwise (e.g. no user currently logged-in).</li>
 * <li>errorCode: code related to the failed attempt, if applicable.</li> 
 * <li>errorDescription: a description related to the failed attempt, if applicable.</li>
 * <li>mmCode: the MilleMiglia code of the currently logged-in user if available, or a blank string.</li> 
 * <li>mmPin: the MilleMiglia pin of the currently logged-in user if available, or a blank string.</li> 
 * </ul>
 * </p>
 */

@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "getmillemigliacredentials" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.extensions", value = { "json" })
})
@SuppressWarnings("serial")
public class GetMilleMigliaCredentialsServlet extends SlingAllMethodsServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaCustomerProfileManager customerProfileManager;
	
	@SuppressWarnings("unused")
	private ComponentContext componentContext;
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		
		logger.debug("MilleMiglia credentials get requested.");
		
		boolean successful = false;
		String mmCode = ""; 
		String mmPin = ""; 
		String errorCode = "";
		String errorDescription = "";
			
		try {
			
			ResourceResolver resourceResolver = request.getResourceResolver();
			Session session = resourceResolver.adaptTo(Session.class);
			UserPropertiesManager userPropertiesManager = resourceResolver.adaptTo(UserPropertiesManager.class);
			
			UserProperties profileProperties = userPropertiesManager.getUserProperties(session.getUserID(), "profile");
			UserProperties privateProperties = userPropertiesManager.getUserProperties(session.getUserID(), "private");
		   
			String customerNumber = profileProperties.getProperty("customerNumber");
			String customerPinCode = privateProperties.getProperty("customerPinCode");
			
			if (customerNumber == null || "".equals(customerNumber) || customerPinCode == null
					|| "".equals(customerPinCode)) {
				logger.error("No user currently logged-in.");
				successful = false;
				errorCode = "notAvailable";
				errorDescription = "No user currently logged-in.";
			}

		    String decryptedPin = customerProfileManager.decodeAndDecryptProperty(customerPinCode);
		    
		    successful = true;
			mmCode = customerNumber;
			mmPin = decryptedPin;
		
		} catch (Exception e) {
			logger.error("Error trying to obtain MilleMiglia credentials.", e);
			successful = false;
			errorCode = "unknown";
			errorDescription = "Unexpected error";
			
		}
		
		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		try {
			json.object();
			json.key("successful");
			json.value(successful);
			json.key("errorCode");
			json.value(errorCode);
			json.key("errorDescription");
			json.value(errorDescription);
			json.key("mmCode");
			json.value(mmCode);
			json.key("mmPin");
			json.value(mmPin);
			json.endObject();
		} catch (Exception e) {
			logger.error("Unexected error generating JSON response.", e);
		}
	}
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
}
