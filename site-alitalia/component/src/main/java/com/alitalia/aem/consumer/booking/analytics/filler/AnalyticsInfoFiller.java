package com.alitalia.aem.consumer.booking.analytics.filler;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.analytics.AnalyticsInfo;

public interface AnalyticsInfoFiller {
	
	public AnalyticsInfo fillInfo(BookingSessionContext ctx, int step, AlitaliaConfigurationHolder configurationHolder);
	
}
