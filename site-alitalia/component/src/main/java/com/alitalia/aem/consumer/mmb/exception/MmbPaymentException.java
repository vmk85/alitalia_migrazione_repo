package com.alitalia.aem.consumer.mmb.exception;

import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;

public class MmbPaymentException extends GenericFormValidatorServletException {

	private static final long serialVersionUID = -8388948758281010112L;
	
	public MmbPaymentException(boolean isRecoverable, String message) {
		super(isRecoverable, message);
	}
	
}
