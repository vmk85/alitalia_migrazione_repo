package com.alitalia.aem.consumer.model.content.myexperience;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlyersResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.day.cq.wcm.api.WCMMode;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MyExperienceModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	private Logger logger = LoggerFactory.getLogger(MyExperienceModel.class);
	
	private String selectValuesJSON;
	private List<String> titleList;
	private List<String> classList;
	private List<String> methodList;
	private List<String> profileList;
	private List<String> years;
	private String resourcePath;
	
	@SuppressWarnings("unchecked")
	@PostConstruct
	protected void initModel() {
		try {
			//Map<String, Map<String, List<String>>> selectValues = new LinkedHashMap<String, Map<String,List<String>>>();
			Map<String, Object> fieldListA = AlitaliaUtils.fromJcrMultifieldToMapArrayObject(request.getResource(), "items");
			
			selectValuesJSON = "{";
			int i = 0;
			for (String keyA : fieldListA.keySet()) {
				
				Map<String, Object> valuesA = (Map<String, Object>) fieldListA.get(keyA);
				
				String categoryNameA = (String) valuesA.get("nameA");
				Map<String, Object> fieldListB = (Map<String, Object>) valuesA.get("selectB");
				
				selectValuesJSON += "\"" + categoryNameA + "\"" + ":{";
				
				int j = 0;
				for (String keyB : fieldListB.keySet()) {
					
					Map<String, Object> valuesB = (Map<String, Object>) fieldListB.get(keyB);
					
					String categoryNameB = (String) valuesB.get("nameB");
					
					selectValuesJSON += "\"" + categoryNameB + "\"" + ":[";
					
					String[] valuesC = ((String) valuesB.get("selectC")).split(";");
					
					for (int k = 0; k < valuesC.length; k++) {
						selectValuesJSON += "\"" + valuesC[k] + "\"";
						if (k != (valuesC.length - 1)) {
							selectValuesJSON += ",";
						}
					}
					
					selectValuesJSON += "]";
					j++;
					if (j < fieldListB.keySet().size()) {
						selectValuesJSON += ",";
					}
				}
				selectValuesJSON += "}";
				i++;
				if (i < fieldListA.keySet().size()) {
					selectValuesJSON += ",";
				}
			}
			selectValuesJSON += "}";
			
			ValueMap properties = request.getResource().getValueMap();
			if (properties.get("titleValues") != null) {
				titleList = Arrays.asList(((String) properties.get("titleValues")).split(";"));
			} else {
				titleList = new ArrayList<String>();
			}
			
			if (properties.get("classValues") != null) {
				classList = Arrays.asList(((String) properties.get("classValues")).split(";"));
			} else {
				classList = new ArrayList<String>();
			}
			
			if (properties.get("methodsOfContact") != null) {
				methodList = Arrays.asList(((String) properties.get("methodsOfContact")).split(";"));
			} else {
				methodList = new ArrayList<String>();
			}
			
			if (properties.get("profileValues") != null) {
				profileList = Arrays.asList(((String) properties.get("profileValues")).split(";"));
			} else {
				profileList = new ArrayList<String>();
			}
			
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			
			years = new ArrayList<String>();
			for (int y = year; y > year - 10; y--) {
				years.add(String.valueOf(y));
			}
			
			resourcePath = request.getResource().getPath();
			
			
		} catch (Exception e) {
			logger.error("Error: ", e);
		}
	}

	public String getSelectValuesJSON() {
		return selectValuesJSON;
	}

	public List<String> getTitleList() {
		return titleList;
	}

	public List<String> getClassList() {
		return classList;
	}

	public List<String> getMethodList() {
		return methodList;
	}

	public List<String> getProfileList() {
		return profileList;
	}

	public List<String> getYears() {
		return years;
	}

	public String getResourcePath() {
		return resourcePath;
	}
}
