package com.alitalia.aem.consumer.mmb;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.home.InsurancePolicyData;
import com.alitalia.aem.common.data.home.SeatMapData;
import com.alitalia.aem.common.data.home.TicketData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPaymentGroupMaskEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbUpdateSSREnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.StatisticsDataTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.TypeSeatEnum;
import com.alitalia.aem.common.data.home.mmb.MmbApisInfoData;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.MmbPaymentData;
import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCartData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCartItemData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryMealDetailData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPassengerData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPaymentData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryQuotationData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillarySeatDetailData;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryRemoveFromCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryRemoveFromCartResponse;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryUpdateCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryUpdateCartResponse;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesResponse;
import com.alitalia.aem.common.messages.home.MmbInsuranceInfoRequest;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoRequest;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoResponse;
import com.alitalia.aem.common.messages.home.MmbPayInsuranceRequest;
import com.alitalia.aem.common.messages.home.RegisterPaymentBookingInsuranceStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapResponse;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationResponse;
import com.alitalia.aem.common.messages.home.SapInvoiceRequest;
import com.alitalia.aem.common.messages.home.SapInvoiceResponse;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsRequest;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsResponse;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerRequest;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerResponse;
import com.alitalia.aem.common.messages.home.enumeration.MmbUpdateReasonType;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.mmb.exception.InvalidAncillaryIdentifierException;
import com.alitalia.aem.consumer.mmb.exception.InvalidAncillaryStatusException;
import com.alitalia.aem.consumer.mmb.exception.MmbAncillaryServiceException;
import com.alitalia.aem.consumer.mmb.exception.MmbModifyPassengerDataException;
import com.alitalia.aem.consumer.mmb.exception.MmbPaymentException;
import com.alitalia.aem.consumer.mmb.exception.MmbPnrInformationNotFoundException;
import com.alitalia.aem.consumer.mmb.render.MmbAncillaryExtraBaggageRender;
import com.alitalia.aem.consumer.mmb.render.MmbFlightDataRender;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.Lists;
import com.alitalia.aem.web.component.managemybooking.delegate.ManageMyBookingDelegate;
import com.alitalia.aem.web.component.statistics.delegate.RegisterStatisticsDelegate;
import com.day.cq.i18n.I18n;

/**
 * The <code>MMBSession</code> component tracks the
 * behaviour of a manage my booking session and the related data,
 * including the prenotation management and ancillary services,
 * through specialized methods.
 * 
 * <p>Since the SCR component is stateless, the state of the
 * single MMB session is tracked in an external object
 * <code>MMBSessionContext</code>.</p>
 * 
 * @see MMBSessionContext
 */
@Component(immediate = true)
@Service(value = MmbSession.class)
public class MmbSession {
		
	/* static */
	
	private static Logger logger = LoggerFactory.getLogger(MmbSession.class);
	
	/* component references */
	
	@SuppressWarnings("unused")
	private ComponentContext componentContext;
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private ManageMyBookingDelegate mmbDelegate;
	
	@Reference
	private RegisterStatisticsDelegate registerStatisticsDelegate;
	
	@Reference
	private BookingSession bookingSession;
	
	/* constructor */
	
	public MmbSession() {
		super();
	}
	
	
	/* SCR lifecycle methods */
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Modified
	private void modified(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	
	/* interface methods */

	/**
	 * Restore an MMB session object
	 */
	
	/**
	 * Initialize the MMB session object, setting it ready to 
	 * start a new process for a given PNR number.
	 * 
	 * <p>This must be called when activating the MMB detail function
	 * on a prenotation, based on the provided tripId and
	 * additional environment data.</p>
	 * 
	 * @param currentCtx If provided, an attempt will be made to restore the previously active session.
	 * @param i18n The i18n instance to be used for translations.
	 * @param serviceClient A string identifying the client when requested by remote services.
	 * @param machineName A string identifying the calling machine name.
	 * @param sid The session identifier, used for tracking purposes.
	 * @param callerIp The IP address when requested by remote services.
	 * @param market The market code to be used when requested by remote services.
	 * @param site The site code to be used when requested by remote services.
	 * @param currencyCode The code of the currency.
	 * @param currencySymbol The symbol of the currency, encoded as HTML entity.
	 * @param locale The locale to use for the session.
	 * @param currentNumberFormat is used to compute the price in MMBPriceRender
	 * @param tripId The code of the trip (PNR code, ticket number or MilleMiglia customer number).
	 * @param passengerName The name of the passenger related to the tripId accessing the MMB.
	 * @param passengerSurname The surname of the passenger related to the tripId accessing the MMB.
	 * @param isFromBooking Set to true when the MMB session is initialized after a booking session.
	 * @param defaultCardType The type of the default credit card to be used for payment, or null.
	 * @param defaultCardNumber The number of the default credit card to be used for payment, or null.
	 * @param defaultCardExpireMonth The expire month of the default credit card to be used for payment, or null.
	 * @param defaultCardExpireYear The expire year of the default credit card to be used for payment, or null.
	 * @param defaultCardName The owner first name of the default credit card to be used for payment, or null.
	 * @param defaultCardSurname The owner last name of the default credit card to be used for payment, or null.
	 */
	public MmbSessionContext initializeSession(MmbSessionContext currentCtx, 
			I18n i18n, String serviceClient, String machineName,
			String sid, String site, String market, String currencyCode,
			String currencySymbol, Locale locale, NumberFormat currentNumberFormat, String callerIp, 
			String tripId, String passengerName, String passengerSurname,
			boolean isFromBooking, String defaultCardType, String defaultCardNumber,
			Integer defaultCardExpireMonth, Integer defaultCardExpireYear,
			String defaultCardName, String defaultCardSurname)
			throws MmbPnrInformationNotFoundException {
		logger.debug("initializeSession");
		String tid = IDFactory.getTid();
		MmbSessionContext ctx;
		
		if (currentCtx != null 
				&& tripId.equals(currentCtx.tripId)
				&& passengerName.equals(currentCtx.passengerName) 
				&& passengerSurname.equals(currentCtx.passengerSurname)) {
			// reuse the current MMB session, just refreshing cart
			ctx = currentCtx;
			refreshCart(ctx, tid);
		} else {
			// create a new MMB session
			ctx = new MmbSessionContext();
			ctx.i18n = i18n;
			ctx.serviceClient = serviceClient;
			ctx.machineName = machineName;
			ctx.sid = sid;
			ctx.callerIp = callerIp;
			ctx.market = "gb".equalsIgnoreCase(market) || "en".equalsIgnoreCase(market) ? market.toUpperCase() : market;
			ctx.site = site;
			ctx.currencyCode = currencyCode;
			ctx.currencySymbol = currencySymbol;
			ctx.locale = locale;
			ctx.currentNumberFormat = currentNumberFormat;
			ctx.tripId = tripId;
			ctx.passengerName = passengerName;
			ctx.passengerSurname = passengerSurname;
			ctx.cartTotalAmount = new BigDecimal(0);
			ctx.insuranceAddedToCart = false;
			ctx.ancillaryPaymentSuccess = false;
			ctx.insurancePaymentSuccess = false;
			ctx.paymentCreditCardNumberHide = "";
			ctx.paymentCreditCardType = "";
			refreshRouteData(ctx);
		}
		
		ctx.isFromBooking = isFromBooking;
		ctx.defaultCardType = defaultCardType;
		ctx.defaultCardNumber = defaultCardNumber;
		ctx.defaultCardExpireMonth = defaultCardExpireMonth;
		ctx.defaultCardExpireYear = defaultCardExpireYear;
		ctx.defaultCardName = defaultCardName;
		ctx.defaultCardSurname = defaultCardSurname;
		
		return ctx;
	}
	
	/**
	 * Initialize the MMB cart and related data if required,
	 * otherwise perform a refresh of the cart.
	 * 
	 * @param ctx The MmbSessionContext to use.
	 */
	public void initializeCartIfRequired(MmbSessionContext ctx) {
		logger.debug("initializeCartIfRequired");
		
		String tid = IDFactory.getTid();
		
		if (ctx.route.isConfirmed()) {
			if (!ctx.mmbCartInitialized) {
				logger.debug("Cart initialization requested on an uninitialized cart, "
						+ "performing a cart initialization.");
				initializeCart(ctx, tid);
				refreshStaticMealTypesData(ctx, tid);
				refreshInsurance(ctx, tid);
				refreshSeatMaps(ctx, tid);
				ctx.mmbCartInitialized = true;
				
			} else {
				logger.debug("Cart initialization requested on an already initialized cart, "
						+ "performing a cart refresh.");
				refreshCart(ctx, tid);
				
			}
		} else {
			logger.debug("PNR is not confirmed, skipping cart initialization.");
		}
	}
	
	/**
	 * Modifies data of a passenger.
	 * 
	 * @param ctx The MmbSessionContext to use.
	 * @param passengerIndex The index of the passenger.
	 * @param nationality The nationality to set.
	 * @param residency The residency to set.
	 * @param name The name to set.
	 * @param secondName The second name to set.
	 * @param lastName The last name to set.
	 * @param birthDate The birthdate to set.
	 * @param sex The sex to set.
	 * @param passportNumber The passport number to set.
	 * @param passportExpirationDate The passport expiration date to set.
	 * @param destinationAddress The address to set.
	 * @param destinationCountry The country to set.
	 * @param destinationCity The city to set.
	 * @param destinationZipCode The ZIP code to set.
	 * @param isApisDataEmpty Flag used to know if it have to do an update or an insert
	 */
	public void modifyPassengerInfo(MmbSessionContext ctx, int passengerIndex,
			String nationality, String residency, String name, String secondName, String lastName,
			Calendar birthDate, String sex, String passportNumber, Calendar passportExpirationDate,
			String destinationAddress, String destinationState, String destinationCity, String destinationZipCode,
			boolean isApisDataEmpty) 
			throws Exception {
		logger.debug("modifyPassengerInfo");
		
		String tid = IDFactory.getTid();
		
		List<MmbPassengerData> passengerDataList = ctx.route.getFlights().get(0).getPassengers();
		MmbPassengerData passengerToModify = new MmbPassengerData();
		if (passengerDataList != null
				&& passengerDataList.size() >= passengerIndex) {
			passengerToModify = passengerDataList.get(passengerIndex);	
		}

		passengerToModify.setApisDataToUpdate(new MmbApisInfoData(passengerToModify.getApisData()));
		
		passengerToModify.getApisData().setNationality(nationality);
		passengerToModify.getApisData().setResidenceCountry(residency);
		passengerToModify.getApisData().setName(passengerToModify.getApisDataToUpdate().getName());
		passengerToModify.getApisData().setSecondName(secondName);
		passengerToModify.getApisData().setLastname(passengerToModify.getApisDataToUpdate().getLastname());
		passengerToModify.getApisData().setBirthDate(birthDate);
		passengerToModify.getApisData().setGender(sex);
		passengerToModify.getApisData().setPassportNumber(passportNumber);
		passengerToModify.getApisData().setPassportExpirationDate(passportExpirationDate);
		passengerToModify.getApisData().setExtraDocumentExpirationDate(passportExpirationDate);
		passengerToModify.getApisData().setDestinationAddress(destinationAddress);
		passengerToModify.getApisData().setDestinationCity(destinationCity);
		passengerToModify.getApisData().setDestinationState(destinationState);
		passengerToModify.getApisData().setDestinationZipCode(destinationZipCode);
		passengerToModify.setUpdate(isApisDataEmpty ? MmbUpdateSSREnum.INSERT : MmbUpdateSSREnum.UPDATE);
		
		MmbRequestBaseInfoData baseInfoData = createBaseInfo(ctx);
		MmbModifyPnrApisInfoRequest modifyPnrApisInfoRequest = new MmbModifyPnrApisInfoRequest();
		modifyPnrApisInfoRequest.setTid(tid);
		modifyPnrApisInfoRequest.setSid(ctx.sid);
		modifyPnrApisInfoRequest.setPnr(ctx.pnr);
		modifyPnrApisInfoRequest.setBaseInfo(baseInfoData);
		modifyPnrApisInfoRequest.setPassenger(passengerToModify);
		
		MmbModifyPnrApisInfoResponse modifyPnrApisInfoResponse = 
				mmbDelegate.modifyPnrPassengerApisInfo(modifyPnrApisInfoRequest);
		
		if (modifyPnrApisInfoResponse != null 
				&& modifyPnrApisInfoResponse.getSuccessful() != null
				&& modifyPnrApisInfoResponse.getSuccessful()) {
			refreshRouteData(ctx);
		} else {
			throw new MmbModifyPassengerDataException(
					ctx.i18n.get(MmbConstants.MESSAGE_ERROR_MODIFY_PASSENGER_DATA));
		}
	}
	
	/**
	 * Modifies frequent flyer data of a passenger.
	 * 
	 * @param ctx The MmbSessionContext to use.
	 * @param passengerIndex The index of the passenger.
	 * @param frequentFlyerCarrier The frequent flyer carrier.
	 * @param frequentFlyerCode The frequent flyer code.
	 */
	public void modifyFrequentFlyerInfo(MmbSessionContext ctx, int passengerIndex, String frequentFlyerCarrier, 
			String frequentFlyerCode) throws Exception {
		logger.debug("modifyFrequentFlyerInfo");
		
		String tid = IDFactory.getTid();
		
		List<MmbPassengerData> passengerDataList = ctx.route.getFlights().get(0).getPassengers();
		MmbPassengerData passengerToModify = new MmbPassengerData();
		if (passengerDataList != null
				&& passengerDataList.size() >= passengerIndex) {
			passengerToModify = passengerDataList.get(passengerIndex);	
		}
		
		UpdateMmbPnrFrequentFlyerRequest updateMmbPnrFrequentFlyerRequest = new UpdateMmbPnrFrequentFlyerRequest();
		updateMmbPnrFrequentFlyerRequest.setTid(tid);
		updateMmbPnrFrequentFlyerRequest.setSid(ctx.sid);
		updateMmbPnrFrequentFlyerRequest.setBaseInfo(createBaseInfo(ctx));
		updateMmbPnrFrequentFlyerRequest.setClient(MmbConstants.MMB_CLIENT);
		updateMmbPnrFrequentFlyerRequest.setName(passengerToModify.getName());
		updateMmbPnrFrequentFlyerRequest.setLastName(passengerToModify.getLastName());
		updateMmbPnrFrequentFlyerRequest.setPnr(ctx.pnr);
		updateMmbPnrFrequentFlyerRequest.setTravelerRefNumber(passengerToModify.getTravelerRefNumber());
		updateMmbPnrFrequentFlyerRequest.setFrequentFlyerCarrier(frequentFlyerCarrier);
		updateMmbPnrFrequentFlyerRequest.setFrequentFlyerCode(frequentFlyerCode);
		
		UpdateMmbPnrFrequentFlyerResponse updateMmbPnrFrequentFlyerResponse = 
				mmbDelegate.updatePnrFrequentFlyer(updateMmbPnrFrequentFlyerRequest);
		
		if (updateMmbPnrFrequentFlyerResponse != null 
				&& updateMmbPnrFrequentFlyerResponse.getSuccessful() != null
				&& updateMmbPnrFrequentFlyerResponse.getSuccessful()) {
			refreshRouteData(ctx);
		} else {
			throw new MmbModifyPassengerDataException(
					ctx.i18n.get(MmbConstants.MESSAGE_ERROR_MODIFY_PASSENGER_FREQUENT_FLYER));
		}
	}
	
	/**
	 * Perform validation logic for the frequent flyer code of adult
	 * passenger data for which we are editing the frequent flyer info
	 * 
	 * @param ctx The mmb booking session context to use.
	 * @param passengerIndex The index of the passenger.
	 * @param frequentFlyerCode The frequent flyer code to validate
	 * @returns true if is valid, false if is not valid
	 */
	public boolean checkFrequentFlyerCodes(MmbSessionContext ctx, int passengerIndex, String frequentFlyerCode) {
		logger.debug("checkFrequentFlyerCode");
		
		String tid = IDFactory.getTid();
		
		// get the passenger info related of the frequent flyer edit form submitted 
		MmbPassengerData passenger = ctx.route.getFlights().get(0).getPassengers().get(passengerIndex);
		if (passenger != null
				&& !passenger.getType().equals(PassengerTypeEnum.CHILD)  
				&& !passenger.getType().equals(PassengerTypeEnum.INFANT)) {
		
			// perform a local check using available check pattern
			if (passenger.getFrequentFlyerCode() != null) {
				String validationRegExp = passenger.getFrequentFlyerType().getRegularExpressionValidation();
				if (!"".equals(validationRegExp) && validationRegExp != null) {
					if (!passenger.getFrequentFlyerCode().matches(validationRegExp)) {
						return false;
					}
				}
			}
			
			// perform a verification using the remote service - this may also perform an actual 
			// check on individual codes for certain frequent flyer types (such as MilleMiglia)
			Map<Integer, List<String>> ffCodesToCheck = new HashMap<Integer, List<String>>();
			List<String> ffPassengerDataList = new ArrayList<String>();
			ffPassengerDataList.add(frequentFlyerCode);
			ffPassengerDataList.add(passenger.getName());
			ffPassengerDataList.add(passenger.getLastName());
			ffCodesToCheck.put(0, ffPassengerDataList);
			
			MmbCheckFrequentFlyerCodesRequest checkFrequentFlyerCodesRequest = new MmbCheckFrequentFlyerCodesRequest();
			checkFrequentFlyerCodesRequest.setTid(tid);
			checkFrequentFlyerCodesRequest.setSid(ctx.sid);
			checkFrequentFlyerCodesRequest.setBaseInfo(createBaseInfo(ctx));
			checkFrequentFlyerCodesRequest.setClient(MmbConstants.MMB_CLIENT);
			checkFrequentFlyerCodesRequest.setFfCodesToCheck(ffCodesToCheck);
			
			MmbCheckFrequentFlyerCodesResponse checkFrequentFlyerCodesResponse = 
					mmbDelegate.checkFrequentFlyerCodes(checkFrequentFlyerCodesRequest); 
			
			if (checkFrequentFlyerCodesResponse.getInvalidFFCodes() != null &&
					checkFrequentFlyerCodesResponse.getInvalidFFCodes().contains(frequentFlyerCode)) {
				return false;
			}
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Modifies email and phone contacts provided during the booking process
	 * 
	 * @param ctx The MmbSessionContext to use.
	 * @param email The new email address.
	 * @param telefono The new telephone number.
	 */
	public void modifyContacts(MmbSessionContext ctx, String email, String tipoTelefono, String prefissoInternazionale,
			String telefono) throws Exception {
		logger.debug("modifyContacts");
		
		String tid = IDFactory.getTid();
		
		MmbPassengerData applicantPassenger = ctx.route.getApplicantPassenger();
		
		// Creazione della request
		UpdateMmbPnrContactsRequest updateMmbPnrContactsRequest = new UpdateMmbPnrContactsRequest();
		updateMmbPnrContactsRequest.setTid(tid);
		updateMmbPnrContactsRequest.setSid(ctx.sid);
		updateMmbPnrContactsRequest.setBaseInfo(createBaseInfo(ctx));
		updateMmbPnrContactsRequest.setClient(MmbConstants.MMB_CLIENT);
		updateMmbPnrContactsRequest.setPnr(ctx.pnr);
		
		String oldEmail = applicantPassenger.getEmail1();
		if (oldEmail != null && !"".equals(oldEmail)) {
			if (email.equals(oldEmail.substring(oldEmail.lastIndexOf("/") + 1))) {
				updateMmbPnrContactsRequest.setUpdateMailAction(MmbUpdateSSREnum.NONE);
			} else {
				updateMmbPnrContactsRequest.setMail(email);
				updateMmbPnrContactsRequest.setOldMail(oldEmail);
				updateMmbPnrContactsRequest.setUpdateMailAction(MmbUpdateSSREnum.UPDATE);	
			}
		} else {
			updateMmbPnrContactsRequest.setMail(email);
			updateMmbPnrContactsRequest.setUpdateMailAction(MmbUpdateSSREnum.INSERT);
		}
		
		telefono = tipoTelefono + "/" + prefissoInternazionale + "/" + telefono;
		String oldTelefono = applicantPassenger.getTelephone1();
		if (oldTelefono != null && !"".equals(oldTelefono)) {
			if (telefono.equals(oldTelefono)) {
				updateMmbPnrContactsRequest.setUpdateTelephoneAction(MmbUpdateSSREnum.NONE);
			} else {
				updateMmbPnrContactsRequest.setTelephone(telefono);
				updateMmbPnrContactsRequest.setOldTelephone(oldTelefono);
				updateMmbPnrContactsRequest.setUpdateTelephoneAction(MmbUpdateSSREnum.UPDATE);
			}
		} else {
			updateMmbPnrContactsRequest.setTelephone(telefono);
			updateMmbPnrContactsRequest.setUpdateTelephoneAction(MmbUpdateSSREnum.INSERT);
		}
		updateMmbPnrContactsRequest.setUpdateTelephone2Action(MmbUpdateSSREnum.NONE);
		
		updateMmbPnrContactsRequest.setReason(MmbUpdateReasonType.MANAGE_MY_BOOKING);		
		
		// controllo che ci siano contatti da modificare: altrimenti non chiamo il servizio
		if (!updateMmbPnrContactsRequest.getUpdateMailAction().equals(MmbUpdateSSREnum.NONE)
				|| !updateMmbPnrContactsRequest.getUpdateTelephoneAction().equals(MmbUpdateSSREnum.NONE)
				|| !updateMmbPnrContactsRequest.getUpdateTelephone2Action().equals(MmbUpdateSSREnum.NONE)) {
			UpdateMmbPnrContactsResponse updateMmbPnrContactsResponse = 
					mmbDelegate.updatePnrContacts(updateMmbPnrContactsRequest);
			
			if (updateMmbPnrContactsResponse != null 
					&& updateMmbPnrContactsResponse.getSuccessful() != null
					&& updateMmbPnrContactsResponse.getSuccessful()) {
				refreshRouteData(ctx);
			} else {
				throw new MmbModifyPassengerDataException(
						ctx.i18n.get(MmbConstants.MESSAGE_ERROR_MODIFY_CONTACTS));
			}
		}
	}

	/**
	 * Ask for booking invoice
	 * 
	 * @param ctx The MmbSessionContext to use.
	 * @param tipoFattura The invoice type
	 * @param nome The name
	 * @param cognome The last name
	 * @param codiceFiscalePIva The tax code
	 * @param indirizzo The address
	 * @param codicePostale The zip code
	 * @param citta The city
	 * @param provincia The province
	 * @param email The new email address.
	 */
	public void askForInvoice(MmbSessionContext ctx, String tipoFattura, String nome, String cognome, 
			String codiceFiscalePIva, String indirizzo, String codicePostale, String citta, String provincia, 
			String email, String intestatarioFattura) 
					throws Exception {
		
		SapInvoiceRequest sapInvoiceRequest = new SapInvoiceRequest();
		sapInvoiceRequest.setSid(ctx.sid);
		sapInvoiceRequest.setTid(IDFactory.getTid());
		
		List<TicketData> ticketDataList = new ArrayList<TicketData>();
		TicketData ticketData;
		
		MmbFlightData firstFlight = ctx.route.getFlights().get(0);
		for (MmbPassengerData passengerData : firstFlight.getPassengers()) {
			ticketData = new TicketData();
			
			DateRender dateRender = new DateRender(firstFlight.getDepartureDateTime());
			ticketData.setDataVolo(dateRender.getYear() + "-" + dateRender.getMonth() + "-" + dateRender.getDay());
			
			ticketData.setNumeroBiglietto(passengerData.getEticket());
			ticketData.setPnr(ctx.pnr);
			
			String tratta = firstFlight.getFrom().getCode() + "\\" + firstFlight.getTo().getCode();
			ticketData.setTratta(tratta);
			
			ticketDataList.add(ticketData);
		}
		sapInvoiceRequest.setBiglietti(ticketDataList);
		
		if ("PF".equals(tipoFattura)) {
			sapInvoiceRequest.setCodiceFiscale(codiceFiscalePIva.toUpperCase());
		} else if ("A".equals(tipoFattura)) {
			sapInvoiceRequest.setPartitaIva(codiceFiscalePIva);
			sapInvoiceRequest.setIntestatarioFattura(intestatarioFattura.toUpperCase());
		}
		
		sapInvoiceRequest.setNome((nome + " " + cognome).toUpperCase());
		sapInvoiceRequest.setIndirizzoSpedizione(indirizzo.toUpperCase());
		sapInvoiceRequest.setCap(codicePostale);
		sapInvoiceRequest.setLocalitaSpedizione(citta.toUpperCase());
		sapInvoiceRequest.setPaese("IT");
		sapInvoiceRequest.setProvincia(provincia.toUpperCase());
		sapInvoiceRequest.setEmailIntestatario(email.toUpperCase());
		
		SapInvoiceResponse sapInvoiceResponse = mmbDelegate.callSAPService(sapInvoiceRequest);
		
		if (sapInvoiceResponse != null 
				&& sapInvoiceResponse.getResult() != null
				&& sapInvoiceResponse.getResult().equals("OK")) {
			refreshRouteData(ctx);
		} else {
			throw new MmbModifyPassengerDataException(
					ctx.i18n.get(MmbConstants.MESSAGE_ERROR_ASK_FOR_INVOICE));
		}
	}
	
	/**
	 * Add Insurance to the cart
	 */
	public void addInsuranceToCart(MmbSessionContext ctx) {
		ctx.insuranceAddedToCart = true;
		refreshCart(ctx, IDFactory.getTid());
	}
	
	/**
	 * Remove Insurance from the cart
	 */
	
	public void removeInsuranceFromCart(MmbSessionContext ctx) {
		ctx.insuranceAddedToCart = false;
		refreshCart(ctx, IDFactory.getTid());
	}
	
	/**
	 * Add to the cart, or update, the meal ancillaries as specified.
	 * 
	 * @param ctx The MmbSessionContext to use.
	 * @param mealCodesByAncillaryId The meal codes, mapped by ancillary id, to add or update in cart.
	 */
	public void addOrUpdateMealsInCart(MmbSessionContext ctx, Map<Integer, String> mealCodesByAncillaryId) 
			throws Exception {
		logger.debug("addOrUpdateMealsInCart");
		
		String tid = IDFactory.getTid();
		
		// process ancillaries to add or update
		List<MmbAncillaryCartItemData> itemsToAdd = new ArrayList<>();
		List<MmbAncillaryCartItemData> itemsToUpdate = new ArrayList<>();
		for (Map.Entry<Integer, String> entry : mealCodesByAncillaryId.entrySet()) {
			Integer ancillaryId = entry.getKey();
			String mealCode = entry.getValue();
			if (mealCode != null && !"".equals(mealCode)) {
				MmbAncillaryData ancillary = findCartAncillaryById(ctx, ancillaryId);
				if (ancillary == null) {
					throw new InvalidAncillaryIdentifierException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_ID, 
									null, new Object[] { ancillaryId }));
				}
				MmbAncillaryCartItemData item = new MmbAncillaryCartItemData();
				item.setAncillaryId(ancillaryId);
				item.setQuantity(1);
				MmbAncillaryMealDetailData ancillaryDetail = new MmbAncillaryMealDetailData();
				ancillaryDetail.setMeal(mealCode);
				item.setAncillaryDetail(ancillaryDetail);
				if (MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus())) {
					itemsToAdd.add(item);
				} else if (MmbAncillaryStatusEnum.ISSUED.equals(ancillary.getAncillaryStatus())) {
					itemsToUpdate.add(item);
				} else {
					throw new InvalidAncillaryStatusException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_STATUS, 
									null, new Object[] { ancillaryId }));
				}
			}
		}
		logger.debug("{} meal ancillaries to be added in cart, {} to be updated.", 
				itemsToAdd.size(), itemsToUpdate.size());
		
		boolean refreshRequired = false;
		boolean detailedLogRequired = false;
		List<MmbAncillaryErrorData> errors = new ArrayList<>();
		try {
			
			if (itemsToAdd.size() > 0) {
				refreshRequired = true;
				MmbAncillaryAddToCartRequest addToCartRequest = 
						new MmbAncillaryAddToCartRequest();
				addToCartRequest.setTid(tid);
				addToCartRequest.setSid(ctx.sid);
				addToCartRequest.setSessionId(ctx.sid);
				addToCartRequest.setBaseInfo(createBaseInfo(ctx));
				addToCartRequest.setCartId(ctx.cart.getId());
				addToCartRequest.setClient(ctx.serviceClient);
				addToCartRequest.setMachineName(ctx.machineName);
				addToCartRequest.setItemsToAdd(itemsToAdd);
				
				MmbAncillaryAddToCartResponse addToCartResponse = 
						mmbDelegate.addToCart(addToCartRequest);
				
				if (addToCartResponse.getErrors() != null) {
					errors.addAll(addToCartResponse.getErrors());
				}
			}
			
			if (itemsToUpdate.size() > 0) {
				refreshRequired = true;
				MmbAncillaryUpdateCartRequest updateCartRequest = 
						new MmbAncillaryUpdateCartRequest();
				updateCartRequest.setTid(tid);
				updateCartRequest.setSid(ctx.sid);
				updateCartRequest.setSessionId(ctx.sid);
				updateCartRequest.setBaseInfo(createBaseInfo(ctx));
				updateCartRequest.setCartId(ctx.cart.getId());
				updateCartRequest.setClient(ctx.serviceClient);
				updateCartRequest.setMachineName(ctx.machineName);
				updateCartRequest.setItemsToUpdate(itemsToUpdate);
				
				MmbAncillaryUpdateCartResponse updateCartResponse = 
						mmbDelegate.updateCart(updateCartRequest);
				
				if (updateCartResponse.getErrors() != null) {
					errors.addAll(updateCartResponse.getErrors());
				}
			}
			
		} catch (Exception e) {
			detailedLogRequired = true;
			throw e;
			
		} finally {
			if (detailedLogRequired || logger.isTraceEnabled()) {
				logDetailedContextStatus(ctx, "Detailed log for MMB context status BEFORE cart refresh");
			}
			if (refreshRequired) {
				refreshCart(ctx, tid);
				if (detailedLogRequired || logger.isTraceEnabled()) {
					logDetailedContextStatus(ctx, "Detailed log for MMB context status AFTER cart refresh");
				}
			}
		}
		
		if (errors.size() > 0) {
			for (MmbAncillaryErrorData error : errors) {
				logger.error("An error occurred in add/update of ancillary meal cart item: " +
						"code {}, description {}, type: {}",
						new Object[] { error.getCode(), error.getDescription(), error.getType() });
			}
			throw new MmbAncillaryServiceException(errors);
		}
	}
	
	/**
	 * Add to the cart, or update, the seat ancillaries as specified.
	 * 
	 * <p>Seat selection values are made up of the seat code, with a "_COMFORT" suffix 
	 * applied only when the target seat is an extra comfort one.</p> 
	 * 
	 * @param ctx The MmbSessionContext to use.
	 * @param seatSelectionsByAncillaryId The seat selections, mapped by ancillary id, to add or update in cart.
	 */
	public void addOrUpdateSeatsInCart(MmbSessionContext ctx, Map<Integer, String> seatSelectionsByAncillaryId) 
			throws Exception {
		logger.debug("addOrUpdateSeatsInCart");
		
		String tid = IDFactory.getTid();
		
		// process ancillaries to add or update
		List<MmbAncillaryCartItemData> itemsToAdd = new ArrayList<>();
		List<MmbAncillaryCartItemData> itemsToUpdate = new ArrayList<>();
		for (Map.Entry<Integer, String> entry : seatSelectionsByAncillaryId.entrySet()) {
			Integer ancillaryId = entry.getKey();
			String seatCode = entry.getValue();
			if (seatCode != null && !"".equals(seatCode)) {
				MmbAncillaryData ancillary = findCartAncillaryById(ctx, ancillaryId);
				if (ancillary == null) {
					throw new InvalidAncillaryIdentifierException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_ID, 
									null, new Object[] { ancillaryId }));
				}
				Boolean isComfort = Boolean.FALSE;
				if (seatCode.endsWith("_COMFORT")) {
					isComfort = Boolean.TRUE;
					seatCode = seatCode.substring(0, seatCode.indexOf("_COMFORT"));
				}
				MmbAncillaryCartItemData item = new MmbAncillaryCartItemData();
				item.setAncillaryId(ancillaryId);
				item.setQuantity(1);
				MmbAncillarySeatDetailData ancillaryDetail = new MmbAncillarySeatDetailData();
				ancillaryDetail.setSeat(seatCode);
				ancillaryDetail.setComfort(isComfort);
				item.setAncillaryDetail(ancillaryDetail);
				if (MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus())) {
					itemsToAdd.add(item);
				} else if (MmbAncillaryStatusEnum.ISSUED.equals(ancillary.getAncillaryStatus())) {
					MmbAncillarySeatDetailData detail = 
							(MmbAncillarySeatDetailData) ancillary.getAncillaryDetail();
					if (!seatCode.equals(detail.getSeat())) {
						itemsToUpdate.add(item);
					}
				} else {
					throw new InvalidAncillaryStatusException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_STATUS, 
									null, new Object[] { ancillaryId }));
				}
			}
		}
		logger.debug("{} seat ancillaries to be added in cart, {} to be updated.", 
				itemsToAdd.size(), itemsToUpdate.size());
		
		boolean refreshRequired = false;
		boolean detailedLogRequired = false;
		List<MmbAncillaryErrorData> errors = new ArrayList<>();
		try {
			
			if (itemsToAdd.size() > 0) {
				refreshRequired = true;
				MmbAncillaryAddToCartRequest addToCartRequest = 
						new MmbAncillaryAddToCartRequest();
				addToCartRequest.setTid(tid);
				addToCartRequest.setSid(ctx.sid);
				addToCartRequest.setSessionId(ctx.sid);
				addToCartRequest.setBaseInfo(createBaseInfo(ctx));
				addToCartRequest.setCartId(ctx.cart.getId());
				addToCartRequest.setClient(ctx.serviceClient);
				addToCartRequest.setMachineName(ctx.machineName);
				addToCartRequest.setItemsToAdd(itemsToAdd);
				
				MmbAncillaryAddToCartResponse addToCartResponse = 
						mmbDelegate.addToCart(addToCartRequest);
				
				if (addToCartResponse.getErrors() != null) {
					errors.addAll(addToCartResponse.getErrors());
				}
			}
			
			if (itemsToUpdate.size() > 0) {
				refreshRequired = true;
				MmbAncillaryUpdateCartRequest updateCartRequest = 
						new MmbAncillaryUpdateCartRequest();
				updateCartRequest.setTid(tid);
				updateCartRequest.setSid(ctx.sid);
				updateCartRequest.setSessionId(ctx.sid);
				updateCartRequest.setBaseInfo(createBaseInfo(ctx));
				updateCartRequest.setCartId(ctx.cart.getId());
				updateCartRequest.setClient(ctx.serviceClient);
				updateCartRequest.setMachineName(ctx.machineName);
				updateCartRequest.setItemsToUpdate(itemsToUpdate);
				
				MmbAncillaryUpdateCartResponse updateCartResponse = 
						mmbDelegate.updateCart(updateCartRequest);
				
				if (updateCartResponse.getErrors() != null) {
					errors.addAll(updateCartResponse.getErrors());
				}
			}
		
		} catch (Exception e) {
			detailedLogRequired = true;
			throw e;
			
		} finally {
			if (detailedLogRequired || logger.isTraceEnabled()) {
				logDetailedContextStatus(ctx, "Detailed log for MMB context status BEFORE cart refresh");
			}
			if (refreshRequired) {
				refreshCart(ctx, tid);
				if (detailedLogRequired || logger.isTraceEnabled()) {
					logDetailedContextStatus(ctx, "Detailed log for MMB context status AFTER cart refresh");
				}
			}
		}
		
		if (errors.size() > 0) {
			for (MmbAncillaryErrorData error : errors) {
				logger.error("An error occurred in add/update of ancillary seat cart item: " +
						"code {}, description {}, type: {}",
						new Object[] { error.getCode(), error.getDescription(), error.getType() });
			}
			throw new MmbAncillaryServiceException(errors);
		}
	}
	
	/**
	 * Add to the cart, or remove, the fast track ancillaries as specified.
	 * 
	 * @param ctx The MmbSessionContext to use.
	 * @param fastTrackSelectionByAncillaryId The fast track selections, mapped by ancillary id, to add or remove in cart.
	 */
	public void addOrRemoveLoungeFromCart(MmbSessionContext ctx, Map<Integer, Boolean> loungeSelectionByAncillaryId) 
			throws Exception {
		logger.debug("addOrRemoveFastTrackFromCart");
		
		String tid = IDFactory.getTid();
		
		// process ancillaries to add or remove
		List<MmbAncillaryCartItemData> itemsToAdd = new ArrayList<>();
		List<Integer> itemIdsToRemove = new ArrayList<>();
		for (Map.Entry<Integer, Boolean> entry : loungeSelectionByAncillaryId.entrySet()) {
			Integer ancillaryId = entry.getKey();
			Boolean selected = entry.getValue();
			MmbAncillaryData ancillary = findCartAncillaryById(ctx, ancillaryId);
			if (ancillary == null) {
				throw new InvalidAncillaryIdentifierException(
						ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_ID, 
								null, new Object[] { ancillaryId }));
			}
			if (Boolean.TRUE.equals(selected)) {
				if (MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus())) {
					MmbAncillaryCartItemData item = new MmbAncillaryCartItemData();
					item.setAncillaryId(ancillaryId);
					item.setQuantity(1);
					itemsToAdd.add(item);
				} else if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
					logger.debug("Ancillary {} already to be issued, ignoring add to cart request.", ancillaryId);
				} else {
					throw new InvalidAncillaryStatusException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_STATUS, 
									null, new Object[] { ancillaryId }));
				}
			} else {
				if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
					itemIdsToRemove.add(ancillaryId);
				} else if ((MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus()))) {
					logger.debug("Ancillary {} already available, ignoring remove from cart request.", ancillaryId);
				} else {
					throw new InvalidAncillaryStatusException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_STATUS, 
									null, new Object[] { ancillaryId }));
				}
			}
		}
		logger.debug("{} fast track ancillaries to be added in cart, {} to be removed.", 
				itemsToAdd.size(), itemIdsToRemove.size());
		
		boolean refreshRequired = false;
		boolean detailedLogRequired = false;
		List<MmbAncillaryErrorData> errors = new ArrayList<>();
		try {
			
			if (itemsToAdd.size() > 0) {
				refreshRequired = true;
				MmbAncillaryAddToCartRequest addToCartRequest = 
						new MmbAncillaryAddToCartRequest();
				addToCartRequest.setTid(tid);
				addToCartRequest.setSid(ctx.sid);
				addToCartRequest.setSessionId(ctx.sid);
				addToCartRequest.setBaseInfo(createBaseInfo(ctx));
				addToCartRequest.setCartId(ctx.cart.getId());
				addToCartRequest.setClient(ctx.serviceClient);
				addToCartRequest.setMachineName(ctx.machineName);
				addToCartRequest.setItemsToAdd(itemsToAdd);
				
				MmbAncillaryAddToCartResponse addToCartResponse = 
						mmbDelegate.addToCart(addToCartRequest);
				
				if (addToCartResponse.getErrors() != null) {
					errors.addAll(addToCartResponse.getErrors());
				}
			}
			
			if (itemIdsToRemove.size() > 0) {
				refreshRequired = true;
				MmbAncillaryRemoveFromCartRequest removeFromCartRequest = 
						new MmbAncillaryRemoveFromCartRequest();
				removeFromCartRequest.setTid(tid);
				removeFromCartRequest.setSid(ctx.sid);
				removeFromCartRequest.setSessionId(ctx.sid);
				removeFromCartRequest.setBaseInfo(createBaseInfo(ctx));
				removeFromCartRequest.setCartId(ctx.cart.getId());
				removeFromCartRequest.setClient(ctx.serviceClient);
				removeFromCartRequest.setMachineName(ctx.machineName);
				removeFromCartRequest.setItemIdsToRemove(itemIdsToRemove);
				
				MmbAncillaryRemoveFromCartResponse removeFromCartResponse = 
						mmbDelegate.removeFromCart(removeFromCartRequest);
				
				if (removeFromCartResponse.getErrors() != null) {
					errors.addAll(removeFromCartResponse.getErrors());
				}
			}
		
		} catch (Exception e) {
			detailedLogRequired = true;
			throw e;
			
		} finally {
			if (detailedLogRequired || logger.isTraceEnabled()) {
				logDetailedContextStatus(ctx, "Detailed log for MMB context status BEFORE cart refresh");
			}
			if (refreshRequired) {
				refreshCart(ctx, tid);
				if (detailedLogRequired || logger.isTraceEnabled()) {
					logDetailedContextStatus(ctx, "Detailed log for MMB context status AFTER cart refresh");
				}
			}
		}
		
		if (errors.size() > 0) {
			for (MmbAncillaryErrorData error : errors) {
				logger.error("An error occurred in add/remove of ancillary fast track cart item: " +
						"code {}, description {}, type: {}",
						new Object[] { error.getCode(), error.getDescription(), error.getType() });
			}
			throw new MmbAncillaryServiceException(errors);
		}
	}
	
	/**
	 * Add to the cart, or remove, the extra baggage ancillaries as specified.
	 * 
	 * @param ctx The MmbSessionContext to use.
	 * @param extraBaggageSelectionByAncillaryId The fast track selections, mapped by ancillary id, to add or remove in cart.
	 */
	public void addOrRemoveExtraBaggageFromCart(MmbSessionContext ctx, Map<Integer, Boolean> extraBaggageSelectionByAncillaryId) 
			throws Exception {
		logger.debug("addOrRemoveExtraBaggageFromCart");
		
		String tid = IDFactory.getTid();
		
		// process ancillaries to add or update
		List<MmbAncillaryCartItemData> itemsToAdd = new ArrayList<>();
		List<Integer> itemIdsToRemove = new ArrayList<>();
		for (Map.Entry<Integer, Boolean> entry : extraBaggageSelectionByAncillaryId.entrySet()) {
			Integer ancillaryId = entry.getKey();
			Boolean selected = entry.getValue();
			MmbAncillaryData ancillary = findCartAncillaryById(ctx, ancillaryId);
			if (ancillary == null) {
				throw new InvalidAncillaryIdentifierException(
						ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_ID, 
								null, new Object[] { ancillaryId }));
			}
			if (Boolean.TRUE.equals(selected)) {
				if (MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus())) {
					MmbAncillaryCartItemData item = new MmbAncillaryCartItemData();
					item.setAncillaryId(ancillaryId);
					item.setQuantity(1);
					itemsToAdd.add(item);
				} else if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
					logger.debug("Ancillary {} already to be issued, ignoring add to cart request.", ancillaryId);
				} else {
					throw new InvalidAncillaryStatusException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_STATUS, 
									null, new Object[] { ancillaryId }));
				}
			} else {
				if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
					itemIdsToRemove.add(ancillaryId);
				} else if ((MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus()))) {
					logger.debug("Ancillary {} already available, ignoring remove from cart request.", ancillaryId);
				} else {
					throw new InvalidAncillaryStatusException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_STATUS, 
									null, new Object[] { ancillaryId }));
				}
			}
		}
		logger.debug("{} extra baggage ancillaries to be added in cart, {} to be removed.", 
				itemsToAdd.size(), itemIdsToRemove.size());
		
		boolean refreshRequired = false;
		boolean detailedLogRequired = false;
		List<MmbAncillaryErrorData> errors = new ArrayList<>();
		try {
			
			if (itemsToAdd.size() > 0) {
				refreshRequired = true;
				MmbAncillaryAddToCartRequest addToCartRequest = 
						new MmbAncillaryAddToCartRequest();
				addToCartRequest.setTid(tid);
				addToCartRequest.setSid(ctx.sid);
				addToCartRequest.setSessionId(ctx.sid);
				addToCartRequest.setBaseInfo(createBaseInfo(ctx));
				addToCartRequest.setCartId(ctx.cart.getId());
				addToCartRequest.setClient(ctx.serviceClient);
				addToCartRequest.setMachineName(ctx.machineName);
				addToCartRequest.setItemsToAdd(itemsToAdd);
				
				MmbAncillaryAddToCartResponse addToCartResponse = 
						mmbDelegate.addToCart(addToCartRequest);
				
				if (addToCartResponse.getErrors() != null) {
					errors.addAll(addToCartResponse.getErrors());
				}
			}
			
			if (itemIdsToRemove.size() > 0) {
				refreshRequired = true;
				MmbAncillaryRemoveFromCartRequest removeFromCartRequest = 
						new MmbAncillaryRemoveFromCartRequest();
				removeFromCartRequest.setTid(tid);
				removeFromCartRequest.setSid(ctx.sid);
				removeFromCartRequest.setSessionId(ctx.sid);
				removeFromCartRequest.setBaseInfo(createBaseInfo(ctx));
				removeFromCartRequest.setCartId(ctx.cart.getId());
				removeFromCartRequest.setClient(ctx.serviceClient);
				removeFromCartRequest.setMachineName(ctx.machineName);
				removeFromCartRequest.setItemIdsToRemove(itemIdsToRemove);
				
				MmbAncillaryRemoveFromCartResponse removeFromCartResponse = 
						mmbDelegate.removeFromCart(removeFromCartRequest);
				
				if (removeFromCartResponse.getErrors() != null) {
					errors.addAll(removeFromCartResponse.getErrors());
				}
			}
		
		} catch (Exception e) {
			detailedLogRequired = true;
			throw e;
			
		} finally {
			if (detailedLogRequired || logger.isTraceEnabled()) {
				logDetailedContextStatus(ctx, "Detailed log for MMB context status BEFORE cart refresh");
			}
			if (refreshRequired) {
				refreshCart(ctx, tid);
				if (detailedLogRequired || logger.isTraceEnabled()) {
					logDetailedContextStatus(ctx, "Detailed log for MMB context status AFTER cart refresh");
				}
			}
		}
		
		if (errors.size() > 0) {
			for (MmbAncillaryErrorData error : errors) {
				logger.error("An error occurred in add/remove of ancillary extra baggage cart item: " +
						"code {}, description {}, type: {}",
						new Object[] { error.getCode(), error.getDescription(), error.getType() });
			}
			throw new MmbAncillaryServiceException(errors);
		}
	}

	/**
	 * Add to the cart, or remove, the fast track ancillaries as specified.
	 * 
	 * @param ctx The MmbSessionContext to use.
	 * @param fastTrackSelectionByAncillaryId The fast track selections, mapped by ancillary id, to add or remove in cart.
	 */
	public void addOrRemoveFastTrackFromCart(MmbSessionContext ctx, Map<Integer, Boolean> fastTrackSelectionByAncillaryId) 
			throws Exception {
		logger.debug("addOrRemoveFastTrackFromCart");
		
		String tid = IDFactory.getTid();
		
		// process ancillaries to add or update
		List<MmbAncillaryCartItemData> itemsToAdd = new ArrayList<>();
		List<Integer> itemIdsToRemove = new ArrayList<>();
		for (Map.Entry<Integer, Boolean> entry : fastTrackSelectionByAncillaryId.entrySet()) {
			Integer ancillaryId = entry.getKey();
			Boolean selected = entry.getValue();
			MmbAncillaryData ancillary = findCartAncillaryById(ctx, ancillaryId);
			if (ancillary == null) {
				throw new InvalidAncillaryIdentifierException(
						ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_ID, 
								null, new Object[] { ancillaryId }));
			}
			if (Boolean.TRUE.equals(selected)) {
				if (MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus())) {
					MmbAncillaryCartItemData item = new MmbAncillaryCartItemData();
					item.setAncillaryId(ancillaryId);
					item.setQuantity(1);
					itemsToAdd.add(item);
				} else if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
					logger.debug("Ancillary {} already to be issued, ignoring add to cart request.", ancillaryId);
				} else {
					throw new InvalidAncillaryStatusException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_STATUS, 
									null, new Object[] { ancillaryId }));
				}
			} else {
				if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
					itemIdsToRemove.add(ancillaryId);
				} else if ((MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus()))) {
					logger.debug("Ancillary {} already available, ignoring remove from cart request.", ancillaryId);
				} else {
					throw new InvalidAncillaryStatusException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_STATUS, 
									null, new Object[] { ancillaryId }));
				}
			}
		}
		logger.debug("{} fast track ancillaries to be added in cart, {} to be removed.", 
				itemsToAdd.size(), itemIdsToRemove.size());
		
		boolean refreshRequired = false;
		boolean detailedLogRequired = false;
		List<MmbAncillaryErrorData> errors = new ArrayList<>();
		try {
			
			if (itemsToAdd.size() > 0) {
				refreshRequired = true;
				MmbAncillaryAddToCartRequest addToCartRequest = 
						new MmbAncillaryAddToCartRequest();
				addToCartRequest.setTid(tid);
				addToCartRequest.setSid(ctx.sid);
				addToCartRequest.setSessionId(ctx.sid);
				addToCartRequest.setBaseInfo(createBaseInfo(ctx));
				addToCartRequest.setCartId(ctx.cart.getId());
				addToCartRequest.setClient(ctx.serviceClient);
				addToCartRequest.setMachineName(ctx.machineName);
				addToCartRequest.setItemsToAdd(itemsToAdd);
				
				MmbAncillaryAddToCartResponse addToCartResponse = 
						mmbDelegate.addToCart(addToCartRequest);
				
				if (addToCartResponse.getErrors() != null) {
					errors.addAll(addToCartResponse.getErrors());
				}
			}
			
			if (itemIdsToRemove.size() > 0) {
				refreshRequired = true;
				MmbAncillaryRemoveFromCartRequest removeFromCartRequest = 
						new MmbAncillaryRemoveFromCartRequest();
				removeFromCartRequest.setTid(tid);
				removeFromCartRequest.setSid(ctx.sid);
				removeFromCartRequest.setSessionId(ctx.sid);
				removeFromCartRequest.setBaseInfo(createBaseInfo(ctx));
				removeFromCartRequest.setCartId(ctx.cart.getId());
				removeFromCartRequest.setClient(ctx.serviceClient);
				removeFromCartRequest.setMachineName(ctx.machineName);
				removeFromCartRequest.setItemIdsToRemove(itemIdsToRemove);
				
				MmbAncillaryRemoveFromCartResponse removeFromCartResponse = 
						mmbDelegate.removeFromCart(removeFromCartRequest);
				
				if (removeFromCartResponse.getErrors() != null) {
					errors.addAll(removeFromCartResponse.getErrors());
				}
			}
		
		} catch (Exception e) {
			detailedLogRequired = true;
			throw e;
			
		} finally {
			if (detailedLogRequired || logger.isTraceEnabled()) {
				logDetailedContextStatus(ctx, "Detailed log for MMB context status BEFORE cart refresh");
			}
			if (refreshRequired) {
				refreshCart(ctx, tid);
				if (detailedLogRequired || logger.isTraceEnabled()) {
					logDetailedContextStatus(ctx, "Detailed log for MMB context status AFTER cart refresh");
				}
			}
		}
		
		if (errors.size() > 0) {
			for (MmbAncillaryErrorData error : errors) {
				logger.error("An error occurred in add/remove of ancillary fast track cart item: " +
						"code {}, description {}, type: {}",
						new Object[] { error.getCode(), error.getDescription(), error.getType() });
			}
			throw new MmbAncillaryServiceException(errors);
		}
	}
	
	/**
	 * Remove all ancillaries in to be issued status from cart, optionally considering
	 * only the specified ancillary type and/or index.
	 * 
	 * @param ctx The MmbSessionContext to use.
	 * @param ancillaryType The ancillary type, or null.
	 * @param passengerIndex The passenger index, or null.
	 * @returns A list of errors produced by the operation.
	 */
	public void clearAncillariesFromCart(MmbSessionContext ctx, MmbAncillaryTypeEnum ancillaryType,
			Integer passengerIndex) throws Exception {
		logger.debug("clearAncillariesFromCart");
		
		String tid = IDFactory.getTid();
		List<MmbAncillaryErrorData> errors = null;
		
		boolean detailedLogRequired = false;
		try {
			
			if (ancillaryType == null || MmbAncillaryTypeEnum.INSURANCE.equals(ancillaryType)) {
				// insurance ancillary is not managed as a cart item, apply special logic
				if (passengerIndex != null) {
					logger.error("Insurance ancillary remove from cart requested for a single passenger - this is usupported.");
					throw new IllegalArgumentException("Insurance ancillary remove from cart requested for a single passenger - this is usupported.");
				}
				if (ctx.insurancePolicy != null && !StringUtils.isBlank(ctx.insurancePolicy.getPolicyNumber())) {
					logger.error("Insurance ancillary remove from cart requestes for an already bought insurance - this is usupported.");
					throw new IllegalArgumentException("Insurance ancillary remove from cart requestes for an already bought insurance - this is usupported.");
				}
				if (ctx.insuranceAddedToCart) {
					ctx.insuranceAddedToCart = false;
				}
				
			}
			
			// other ancillaries, use standard cart methods
			errors = removeAncillariesFromCart(ctx, tid, ancillaryType, passengerIndex);
		
		} catch (Exception e) {
			detailedLogRequired = true;
			throw e;
			
		} finally {
			if (detailedLogRequired || logger.isTraceEnabled()) {
				logDetailedContextStatus(ctx, "Detailed log for ancillary cart status BEFORE refresh");
			}
			refreshCart(ctx, tid);
			if (detailedLogRequired || logger.isTraceEnabled()) {
				logDetailedContextStatus(ctx, "Detailed log for ancillary cart status AFTER refresh");
			}
		}
		
		if (errors != null && errors.size() > 0) {
			for (MmbAncillaryErrorData error : errors) {
				logger.error("An error occurred in clearing of ancillary cart items: " +
						"code {}, description {}, type: {}",
						new Object[] { error.getCode(), error.getDescription(), error.getType() });
				throw new MmbAncillaryServiceException(errors);
			}
		}
	}

	/** 
	 * Perform the pre-payment check, sending the email if all the ancillaries in cart are free
	 * and providing info about the next step
	 * 
	 * @param ctx The MmbSessionContext to use.
	 * @return true if the payment step is required, false to skip to the thank-you page.
	 */
	public boolean performPrePayment(MmbSessionContext ctx) throws Exception {
		
		String tid = IDFactory.getTid();
		
		refreshCart(ctx, tid);
		loadCreditCards(ctx, tid);
		
		if (ctx.cartTotalAmount.compareTo(BigDecimal.valueOf(0)) == 0) {
			
			// trigger the confirmation e-mail when there is nothing to pay
			// (this is done automatically when a payment is performed)
			try {
				MmbAncillarySendEmailRequest sendMailRequest = new MmbAncillarySendEmailRequest();
				sendMailRequest.setSid(ctx.sid);
				sendMailRequest.setTid(tid);
				sendMailRequest.setClient(ctx.serviceClient);
				sendMailRequest.setMachineName(ctx.machineName);
				sendMailRequest.setSessionId(ctx.sid);
				sendMailRequest.setBaseInfo(createBaseInfo(ctx));
				sendMailRequest.setCartId(ctx.cart.getId());
				sendMailRequest.setRecipients(Lists.asList(ctx.confirmationMailAddress));
				sendMailRequest.setPnr(ctx.pnr);
				
				MmbAncillarySendEmailResponse sendMailResponse = 
						mmbDelegate.sendPurchasedAncillaryEmail(sendMailRequest);
				
				if (sendMailResponse.getErrors() != null 
						&& sendMailResponse.getErrors().size() > 0) {
					logger.error("One or more errors was found in MMB sendMail response, details follow.");
					for (String error : sendMailResponse.getErrors()) {
						logger.error("Send mail error: {}", error);
					}
				}
				
			} catch (Exception e) {
				logger.error("An exception was caught during MMB sendMail invocation", e);
				
			}
			
			// return false to skip the payment page (nothing to pay)
			return false;
			
		} else {
			// return true to land the user in payment page
			return true;
			
		}
	}

	/** 
	 * Perform payment of all ancillary in the cart with status TO_BE_ISSUED including insurance (if added to cart)
	 * 
	 * @param ctx The MmbSessionContext to use.
	 * @param totalDisplayedAmount The total amount to pay that was displayed to the user, or null to skip this check.
	 * @param cardNumber The credit card number
	 * @param creditCardTypeName The name of the circuit of credit card
	 * @param creditCardTypeCode The code of the circuit of credit card
	 * @param cvv The cvv/cvc/cid of credit card
	 * @param expirationMonth The month of expiration date of credit card
	 * @param expirationYear the year of expiration date of credit card
	 * @param ownerName The name of the credit card's owner
	 * @param ownerLastName The last name of the credit card's owner
	 * @param payWithDefaultCreditCard True if the user is paying with the same credit card used to complete payment in the booking session
	 */
	public void performPayment(MmbSessionContext ctx, BigDecimal totalDisplayedAmount, 
			String cardNumber, String creditCardTypeName, String creditCardTypeCode, String cvv, 
			Integer expirationMonth, Integer expirationYear, String ownerName, String ownerLastName, 
			boolean payWithDefaultCreditCard) throws Exception {
		logger.debug("performPayment (for cart id {})", ctx.cart.getId());
		
		String tid = IDFactory.getTid();
		
		// first, refresh the cart (this will also compute again the total amount to pay)
		refreshCart(ctx, tid);
		
		// if available, check if the total amount to pay is consistent with the value that was presented
		if (totalDisplayedAmount != null) {
			if (!totalDisplayedAmount.equals(ctx.cartTotalAmount)) {
				// recoverable error, go back to the payment page with an error message asking to check and retry
				ctx.ancillaryPaymentSuccess = false;
				ctx.insurancePaymentSuccess = false;
				ctx.paymentErrorMessage = ctx.i18n.get(MmbConstants.MESSAGE_ERROR_PAYMENT_WRONG_AMOUNT);
				throw new MmbPaymentException(true, ctx.paymentErrorMessage);
			}
		}
		
		// if default (i.e. booking) credit card is to be used, set the payment data accordingly
		if (payWithDefaultCreditCard) {
			cardNumber = ctx.defaultCardNumber;
			expirationMonth = ctx.defaultCardExpireMonth;
			expirationYear = ctx.defaultCardExpireYear;
			ownerName = ctx.defaultCardName;
			ownerLastName = ctx.defaultCardSurname;
		}
		
		boolean detailedLogRequired = false;
		try {
			
			if (ctx.cartTotalAncillariesAmount.compareTo(BigDecimal.valueOf(0)) == 0) {
				// skip the ancillary payment
				logger.debug("Skipping ancillary payment, nothing to pay.");
				
			} else {
				// perform the ancillary payment
				int numberOfAncillaryToBeIssued = 0;
				for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
					if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
						numberOfAncillaryToBeIssued++;
					}
				}
				logger.debug("Performing ancillary payment ({} ancillaries to be issued)", numberOfAncillaryToBeIssued);
				
				try {
					
					MmbAncillaryCheckOutInitRequest ancillaryCheckOutInitRequest = new MmbAncillaryCheckOutInitRequest();
					ancillaryCheckOutInitRequest.setSid(ctx.sid);
					ancillaryCheckOutInitRequest.setTid(tid);
					ancillaryCheckOutInitRequest.setClient(ctx.serviceClient);
					ancillaryCheckOutInitRequest.setMachineName(ctx.machineName);
					ancillaryCheckOutInitRequest.setSessionId(ctx.sid);
					ancillaryCheckOutInitRequest.setBaseInfo(createBaseInfo(ctx));
					ancillaryCheckOutInitRequest.setCartId(ctx.cart.getId());
					
					MmbAncillaryPaymentData ancillaryPaymentData = new MmbAncillaryPaymentData();
					ancillaryPaymentData.setCardNumber(cardNumber);
					ancillaryPaymentData.setCreditCardType(creditCardTypeCode);
					ancillaryPaymentData.setCvv(cvv);
					ancillaryPaymentData.setExpirationMonth(expirationMonth);
					ancillaryPaymentData.setExpirationYear(expirationYear);
					ancillaryPaymentData.setOwnerName(ownerName);
					ancillaryPaymentData.setOwnerLastName(ownerLastName);
					ancillaryPaymentData.setEmail(ctx.confirmationMailAddress);
					ancillaryPaymentData.setPaymentType(PaymentTypeEnum.CREDIT_CARD);
					ancillaryCheckOutInitRequest.setPayment(ancillaryPaymentData);
					
					MmbAncillaryCheckOutInitResponse ancillaryCheckOutInitResponse = 
							mmbDelegate.checkOutInit(ancillaryCheckOutInitRequest);
					
					if (ancillaryCheckOutInitResponse.getErrors() != null 
							&& ancillaryCheckOutInitResponse.getErrors().size() > 0) {
						detailedLogRequired = true;
						logger.error("One or more errors was found in MMB check-out INIT response, details follow. "
								+ "(a check-out status check will be performed anyway)");
						for (MmbAncillaryErrorData error : ancillaryCheckOutInitResponse.getErrors()) {
							logger.error("Ancillary error: {} {} {}", 
									new Object[] { error.getCode(), error.getDescription(), error.getType() });
						}
					}
				
				} catch (Exception e) {
					detailedLogRequired = true;
					logger.error("An exception was caught during MMB check-out INIT invocation "
							+ "(a check-out status check will be performed anyway)", e);
				}
				
				// perform the ancillary payment status check
				logger.debug("Performing ancillary payment status check");
				int ancillaryCheckCount = 0;
				final int ancillaryCheckMaxTries = 10;
				boolean ancillaryIssuingStarted = false;
				boolean ancillaryIssuingCompleted = false;
				while (!ancillaryIssuingCompleted && ancillaryCheckCount <= ancillaryCheckMaxTries) {
					
					ancillaryCheckCount++;
					logger.debug("Checking ancillary payment status, try {}/{}", 
							ancillaryCheckCount, ancillaryCheckMaxTries);
					
					try {
						if (ancillaryCheckCount > 1) {
							logger.debug("Applying thread sleep of 1 second before next try...");
							Thread.sleep(1000);
						}
					
						MmbAncillaryCheckOutStatusRequest ancillaryCheckOutStatusRequest = new MmbAncillaryCheckOutStatusRequest();
						ancillaryCheckOutStatusRequest.setSid(ctx.sid);
						ancillaryCheckOutStatusRequest.setTid(tid);
						ancillaryCheckOutStatusRequest.setClient(ctx.serviceClient);
						ancillaryCheckOutStatusRequest.setMachineName(ctx.machineName);
						ancillaryCheckOutStatusRequest.setSessionId(ctx.sid);
						ancillaryCheckOutStatusRequest.setBaseInfo(createBaseInfo(ctx));
						ancillaryCheckOutStatusRequest.setCartId(ctx.cart.getId());
						
						MmbAncillaryCheckOutStatusResponse ancillaryCheckOutStatusResponse = 
								mmbDelegate.getCheckOutStatus(ancillaryCheckOutStatusRequest);
						
						// count ancillaries still in to-be-issued or in error status
						int issuedAncillaries = 0;
						int stillToBeIssuedAncillaries = 0;
						int inErrorAncillaries = 0;
						if (ancillaryCheckOutStatusResponse.getAncillaries() != null) {
							for (MmbAncillaryData ancillary : ancillaryCheckOutStatusResponse.getAncillaries()) {
								if (MmbAncillaryStatusEnum.ISSUED.equals(ancillary.getAncillaryStatus())) {
									issuedAncillaries++;
									ancillaryIssuingStarted = true;
								}
								if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
									stillToBeIssuedAncillaries++;
								}
								if (MmbAncillaryStatusEnum.ERROR.equals(ancillary.getAncillaryStatus())) {
									inErrorAncillaries++;
									detailedLogRequired = true;
								}
							}
						}
						logger.debug("Found {} issued, {} still to-be-issued and {} in error ancillaries in response.", 
								new Object[] { issuedAncillaries, stillToBeIssuedAncillaries, inErrorAncillaries });
						
						// verify response errors
						if (ancillaryCheckOutStatusResponse.getErrors() != null 
								&& ancillaryCheckOutStatusResponse.getErrors().size() > 0) {
							detailedLogRequired = true;
							logger.error("One or more errors was found in MMB check-out STATUS response, details follow.");
							for (MmbAncillaryErrorData error : ancillaryCheckOutStatusResponse.getErrors()) {
								logger.error("Ancillary error: {} {} {}", 
										new Object[] { error.getCode(), error.getDescription(), error.getType() });
							}
						}
						
						if (stillToBeIssuedAncillaries == 0) {
							// no more ancillaries in to-be-issued status returned, stop status check
							logger.debug("No more ancillaries in to-be-issued status returned, stopping status check.");
							ancillaryIssuingCompleted = true;
						}
						
					} catch (Exception e) {
						detailedLogRequired = true;
						logger.error("An exception was caught during MMB check-out STATUS invocation", e);
					}
				}
				
				// Ancillary status check performed
				logger.debug("Ancillary status check performed.");
				if (!ancillaryIssuingCompleted) {
					if (ancillaryIssuingStarted) {
						// ancillary issuing was started but not completed, go to thank-you page
						logger.warn("Ancillary issuing was started but not completed, go to thank-you page.");
						ctx.ancillaryPaymentSuccess = false;
						ctx.insurancePaymentSuccess = false;
						ctx.paymentErrorMessage = 
								ctx.i18n.get(MmbConstants.MESSAGE_ERROR_ANCILLARY_PAYMENT);
						throw new MmbPaymentException(false, ctx.paymentErrorMessage);
						
					} else {
						// ancillary issuing seems to be not started at all, go back to payment page
						logger.warn("Ancillary issuing seems to be not started at all, go back to payment page.");
						ctx.ancillaryPaymentSuccess = false;
						ctx.insurancePaymentSuccess = false;
						ctx.paymentErrorMessage = 
								ctx.i18n.get(MmbConstants.MESSAGE_ERROR_RECOVERABLE_ANCILLARY_PAYMENT);
						throw new MmbPaymentException(true, ctx.paymentErrorMessage);
						
					}
				
				} else {
					// ancillary issuing completed correctly
					logger.debug("Ancillary issuing completed correctly.");
					ctx.ancillaryPaymentSuccess = true;
					ctx.paymentErrorMessage = null;
					
				}
				
			}
			
			if (!ctx.insuranceAddedToCart) {
				// skip the insurance payment
				logger.debug("Skipping insurance payment, not in cart.");
				
			} else {
				// perform the insurance payment
				logger.debug("Performing insurance payment");
				
				try {
					ctx.insurancePolicy.setBuy(true);
					ctx.insurancePolicy.setPolicyNumber(null);
					
					MmbPayInsuranceRequest payInsuranceRequest = new MmbPayInsuranceRequest();
					payInsuranceRequest.setTid(tid);
					payInsuranceRequest.setSid(ctx.sid);
					
					ctx.route.setPnr(ctx.tripId);
					payInsuranceRequest.setRoute(ctx.route);
					payInsuranceRequest.setInsurancePolicy(ctx.insurancePolicy);
					
					MmbPaymentData insurancePaymentData = new MmbPaymentData();
					insurancePaymentData.setCreditCardNumber(cardNumber);
					insurancePaymentData.setCreditCardType(
							MmbUtils.getCreditCardTypeEnumFromCode((creditCardTypeCode)));
					insurancePaymentData.setCvc(cvv);
					insurancePaymentData.setExpirationMonth(String.valueOf(expirationMonth));
					insurancePaymentData.setExpirationYear(String.valueOf(expirationYear));
					insurancePaymentData.setName(ownerName);
					insurancePaymentData.setLastName(ownerLastName);
					insurancePaymentData.setEmail(ctx.confirmationMailAddress);
					insurancePaymentData.setSelectedCountry(ctx.market);
					insurancePaymentData.setSelectedPaymentGroup(MmbPaymentGroupMaskEnum.CREDIT_CARDS);
					payInsuranceRequest.setPaymentData(insurancePaymentData);
					
					InsuranceResponse payInsuranceResponse = mmbDelegate.payInsurance(payInsuranceRequest);
				
					if (payInsuranceResponse.getPolicy() != null &&
							!StringUtils.isBlank(payInsuranceResponse.getPolicy().getPolicyNumber())) {
						logger.debug("Insurance payment executed and policy number obtained (success).");
						ctx.insurancePaymentSuccess = true;
						ctx.paymentErrorMessage = null;
						ctx.insurancePolicy = payInsuranceResponse.getPolicy();
						paymentInsuranceStatistic(ctx, creditCardTypeName, null, true);
						
					} else {
						if (payInsuranceResponse.getPolicy() == null) {
							logger.error("No policy received after insurance payment invocation (failure).");
						
						} else {
							logger.error("No policy number received after insurance payment invocation (failure), "
									+ "reported error description is {}, entire policy received is: {}",
									payInsuranceResponse.getPolicy().getErrorDescription(), 
									payInsuranceResponse.getPolicy());
							
						}
						
						detailedLogRequired = true;
						ctx.insurancePaymentSuccess = false;
						ctx.paymentErrorMessage = ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INSURANCE_PAYMENT);
						
						paymentInsuranceStatistic(ctx, creditCardTypeName, payInsuranceResponse.getPolicy(), false);
						
						throw new MmbPaymentException(false, ctx.paymentErrorMessage);
					}
				
				} catch (Exception e) {
					detailedLogRequired = true;
					logger.error("An exception was caught during MMB insurance pay invocation", e);
					
					ctx.insurancePolicy.setBuy(false);
					ctx.insurancePaymentSuccess = false;
					throw e;
				
				}
			}
				
			// when going to thank-you-page, save credit card info for payment feedback display
			for (int i = 0; i < cardNumber.length() - 4; i++) {
				ctx.paymentCreditCardNumberHide = ctx.paymentCreditCardNumberHide + "*";
			}
			ctx.paymentCreditCardNumberHide = 
					ctx.paymentCreditCardNumberHide + cardNumber.substring(cardNumber.length() - 4);
			ctx.paymentCreditCardType = creditCardTypeName;
			
		} finally {
			if (detailedLogRequired || logger.isTraceEnabled()) {
				logDetailedContextStatus(ctx, "Detailed log for ancillary cart status BEFORE refresh");
			}
			refreshCart(ctx, tid);
			if (detailedLogRequired || logger.isTraceEnabled()) {
				logDetailedContextStatus(ctx, "Detailed log for ancillary cart status AFTER refresh");
			}
		}
	}
	
	
	/* public utility methods (should not change context status) */
		
	/**
	 * Specify if the box for the ancillary cart should be displayed.
	 * 
	 * @param ctx The MMB session context.
	 * @return true if the cart should be displayed, false otherwise.
	 */
	public boolean isAncillaryCartDisplayed(MmbSessionContext ctx) {
		if (ctx == null || ctx.cart == null) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Specify if the box for an ancillary type should be displayed.
	 * 
	 * @param ctx The MMB session context.
	 * @param ancillaryType The ancillary type.
	 * @return true if the box should be displayed, false otherwise.
	 */
	public boolean isAncillaryBoxDisplayed(MmbSessionContext ctx, MmbAncillaryTypeEnum ancillaryType) {
		boolean isDirectAndata = isDirectFlight(ctx, RouteTypeEnum.OUTBOUND);
		boolean isDirectRitorno = isDirectFlight(ctx, RouteTypeEnum.RETURN);
		if (ctx == null || ctx.cart == null) {
			return false;
		} else {
			if (MmbAncillaryTypeEnum.INSURANCE.equals(ancillaryType)) {
				if(ctx.isBus){
					return false;
				} else{
					return "it".equalsIgnoreCase(ctx.market) &&
							ctx.insurancePolicy != null && 
							ctx.insurancePolicy.getTotalInsuranceCost() != null;
				}
			} else if(MmbAncillaryTypeEnum.EXTRA_BAGGAGE.equals(ancillaryType)) {
				return hasAncillaryType(ctx, ancillaryType);
			} else if (MmbAncillaryTypeEnum.SEAT.equals(ancillaryType)) {
				if (ctx.isBus && (isDirectAndata && isDirectRitorno)) {
					return false;
				}
				else {
					return !areAllMiniFare(ctx) && hasAncillaryType(ctx, ancillaryType);
				}
			} else {
				return (ctx.isBus && (isDirectAndata && isDirectRitorno)) ? false : hasAncillaryType(ctx, ancillaryType);
			}
		}
	}
	
	/**
	 * Verify if at least an ancillary of the specified type is available in the cart.
	 * 
	 * @param ctx The MMB session context.
	 * @param ancillaryType The ancillary type.
	 * @return true if at least one is found, false otherwise.
	 */
	public boolean hasAncillaryType(MmbSessionContext ctx, MmbAncillaryTypeEnum ancillaryType) {
		if (ctx != null && ctx.cart != null && ctx.cart.getAncillaries() != null) {
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
				if (ancillaryType.equals(ancillary.getAncillaryType())) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Given the id of a seatmap ancillary and a related ancillary value (i.e. a seat number),
	 * verify if that seat is marked as a comfort one, based on the current seatmap data
	 * in the context.
	 * 
	 * @param ctx The MMB session context.
	 * @param ancillaryId The ancillary id of the seat ancillary.
	 * @param ancillaryValue The ancillary value, i.e. the seat number to check.
	 * @return True when the seat is a comfort one, False if it is not a comfort one, null if the seat cannot be found.
	 */
	public Boolean verifyIsComfortSeat(MmbSessionContext ctx, Integer ancillaryId, String ancillaryValue) {
		MmbAncillaryData ancillary = findCartAncillaryById(ctx, ancillaryId);
		if (ancillary == null) {
			logger.warn("verifyIsComfortSeat could not find the ancillary id {}.", ancillaryId);
			return null;
		}
		MmbFlightData flight = MmbUtils.findAncillaryFlight(ancillary, ctx.route.getFlights());
		if (flight == null) {
			logger.warn("verifyIsComfortSeat could not find the flight related to ancillary id {}.", ancillaryId);
			return null;
		}
		SeatMapData seatMapData = ctx.seatMapsByFlight.get(flight);
		if (seatMapData == null) {
			logger.warn("verifyIsComfortSeat could not find the seatmap related to ancillary id {}.", ancillaryId);
			return null;
		}
		TypeSeatEnum seatType = MmbUtils.findSeatTypeInSeatMap(seatMapData, ancillaryValue);
		if (seatType == null) {
			logger.warn("verifyIsComfortSeat could not find the seat related to ancillary id {} in map.", ancillaryId);
			return null;
		}
		if (TypeSeatEnum.COMFORT_SEAT.equals(seatType) || TypeSeatEnum.EXIT_COMFORT_SEAT.equals(seatType)) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}
	
	/**
	 * Find an ancillary by identifier in current cart data.
	 * 
	 * @param ctx The MMB session context.
	 * @param ancillaryId The ancillary id.
	 * @return The MmbAncillaryData for the ancillary object, or null if not found.
	 */
	public MmbAncillaryData findCartAncillaryById(MmbSessionContext ctx, Integer ancillaryId) {
		if (ctx != null && ctx.cart != null && ancillaryId != null) {
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
				if (ancillaryId.equals(ancillary.getId())) {
					return ancillary;
				}
			}
		}
		return null;
	}
	
	/**
	 * Given an ancillary, returns the price value to be displayed.
	 * 
	 * @param ancillary The ancillary object.
	 * @param quantity The quantity to apply, or null to use the quantity of the ancillary.
	 * @return the amount value.
	 */
	public BigDecimal getAncillaryDisplayedPriceValue(MmbAncillaryData ancillary, Integer quantity) {
		List<MmbAncillaryQuotationData> quotations = ancillary.getQuotations();
		if (quantity == null) {
			quantity = ancillary.getQuantity();
		}
		if (quotations != null) {
			for (MmbAncillaryQuotationData quotation : quotations) {
				if (quantity.intValue() >= quotation.getMaxQuantity().intValue() &&
						quantity.intValue() <= quotation.getMinQuantity().intValue()) {
					return quotation.getPrice().multiply(new BigDecimal(quantity.intValue()));
				}
			}
		}
		// (no price data found or applicable)
		return null;
	}
	
	/**
	 * Given an ancillary, returns the price info to be displayed.
	 * 
	 * @param ctx The MMB session context.
	 * @param ancillary The ancillary object.
	 * @param quantity The quantity to apply, or null to use the quantity of the ancillary.
	 * @return an MmbPriceRender instance with the data.
	 */
	public MmbPriceRender getAncillaryDisplayedPriceInfo(MmbSessionContext ctx, MmbAncillaryData ancillary,
			Integer quantity) {
		BigDecimal value = getAncillaryDisplayedPriceValue(ancillary, quantity);
		return new MmbPriceRender(value, ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat);
	}
	
	/**
	 * Given a set of ancillary, returns the total price info to be displayed.
	 * 
	 * @param ctx The MMB session context.
	 * @param ancillaries The ancillaries.
	 * @param quantity The quantity to apply, or null to use the quantity of each ancillary.
	 * @return an MmbPriceRender instance with the data.
	 */
	public MmbPriceRender getAncillaryDisplayedPriceInfo(MmbSessionContext ctx, List<MmbAncillaryData> ancillaries,
			Integer quantity) {
		BigDecimal totalValue = null;
		for (MmbAncillaryData ancillary : ancillaries) {
			BigDecimal value = getAncillaryDisplayedPriceValue(ancillary, quantity);
			if (value != null) {
				if (totalValue == null) {
					totalValue = value;
				} else {
					totalValue = totalValue.add(value);
				}
			}
		}
		return new MmbPriceRender(totalValue, ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat);
	}
	
	
	/* internal methods */
	
	/**
	 * Print a detailed log about the current context status, with a custom message
	 * just before it. If trace level is enabled, the log is sent to trace or,
	 * if trace level is debug, debug level is used - otherwise no log is produced.
	 */
	private void logDetailedContextStatus(MmbSessionContext ctx, String message) {
		try {
			if (logger.isTraceEnabled()) {
				logger.trace(message);
				logger.trace(ctx.toString());
			} else if (logger.isDebugEnabled()) {
				logger.debug(message);
				logger.debug(ctx.toString());
			}
		} catch (Exception e) {
			logger.error("An error occurred during execution of logDetailedCartStatus method", e);
		}
	}
	
	/**
	 * Internal utility method to create the base info data for service calls.
	 */
	private MmbRequestBaseInfoData createBaseInfo(MmbSessionContext ctx) {
		MmbRequestBaseInfoData baseInfoData = new MmbRequestBaseInfoData();
		baseInfoData.setClientIp(ctx.callerIp);
		baseInfoData.setSessionId(ctx.sid);
		baseInfoData.setSiteCode(ctx.market);
		return baseInfoData;
	}
	
	/**
	 * Internal utility method to load the MMB route data into context.
	 */
	private void refreshRouteData(MmbSessionContext ctx) throws MmbPnrInformationNotFoundException {
		String tid = IDFactory.getTid();
		
		// prepare pnr info request
		RetriveMmbPnrInformationRequest pnrInformationRequest = new RetriveMmbPnrInformationRequest();
		pnrInformationRequest.setTid(tid);
		pnrInformationRequest.setSid(ctx.sid);
		pnrInformationRequest.setBaseInfo(createBaseInfo(ctx));
		pnrInformationRequest.setLocale(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()) + "_" + ctx.market);
		pnrInformationRequest.setClient(ctx.serviceClient);
		pnrInformationRequest.setTripId(ctx.tripId);
		pnrInformationRequest.setName(ctx.passengerName);
		pnrInformationRequest.setLastName(ctx.passengerSurname);
		
		// execute pnr info request
		RetriveMmbPnrInformationResponse pnrInformationResponse = null;
		try {
			pnrInformationResponse = mmbDelegate.retrivePnrInformation(pnrInformationRequest);
		} catch (Exception e) {
			logger.error("MMB Error in SERVICE retriving route for PNR: {} - NAME: {}", ctx.tripId, ctx.passengerName + " " + ctx.passengerSurname);
			throw new MmbPnrInformationNotFoundException(e);
		}
		if (pnrInformationResponse == null || 
				pnrInformationResponse.getPnr() == null || 
				"".equals(pnrInformationResponse.getPnr())) {
			logger.error("MMB Error RESPONSE retriving route for PNR: {} - NAME: {}", ctx.tripId, ctx.passengerName + " " + ctx.passengerSurname);
			throw new MmbPnrInformationNotFoundException(
					ctx.i18n.get(MmbConstants.MESSAGE_ERROR_PNR_NOT_FOUND));
		}
		if (pnrInformationResponse.getRoutesList() == null ||
				pnrInformationResponse.getRoutesList().size() == 0 ||
				pnrInformationResponse.getRoutesList().get(0) == null) {
			logger.error("MMB Error in ROUTE retriving route for PNR: {} - NAME: {}", ctx.tripId, ctx.passengerName + " " + ctx.passengerSurname);
			throw new MmbPnrInformationNotFoundException(
					ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_DATA));
		}
		
		// read pnr data into context
		ctx.pnr = pnrInformationResponse.getPnr();
		ctx.eTicket = pnrInformationResponse.geteTicket();
		ctx.passengerName = pnrInformationRequest.getName();
		ctx.passengerSurname = pnrInformationRequest.getLastName();
		ctx.route = pnrInformationResponse.getRoutesList().get(0);
		ctx.flightsByRouteType = new HashMap<RouteTypeEnum, List<MmbFlightData>>();
		ctx.isSecureFlightEsta = computeIsSecureFlightESTA(ctx);
		ctx.isBus = pnrInformationResponse.isBusCarrier();
		
		// evaluate e-mail for ancillary confirmation
		if (ctx.route.getApplicantPassenger() != null) {
			String email = null;
			email = ctx.route.getApplicantPassenger().getEmail1();
			if (StringUtils.isBlank(email)) {
				logger.debug("Applicant 1st e-mail contact null or blank, trying with 2nd one.");
				email = ctx.route.getApplicantPassenger().getEmail2();
			}
			if (email != null && email.startsWith("E/")) {
				email = email.substring(2);
			}
			ctx.confirmationMailAddress = email;
		} else {
			logger.warn("Cannot determine email for ancillary confirmation, applicat passenger is null.");
		}
		
		// evaluate additional flights info
		ctx.type = RouteTypeEnum.OUTBOUND;
		ctx.area = AreaValueEnum.DOM;
		List<MmbFlightData> flights = pnrInformationResponse.getRoutesList().get(0).getFlights();
		for (MmbFlightData flight : flights) {
			// add in flights-by-route map
			ctx.flightsByRouteType.putIfAbsent(flight.getType(), new ArrayList<MmbFlightData>());
			ctx.flightsByRouteType.get(flight.getType()).add(flight);
			// outbound / roundtrip
			if (RouteTypeEnum.RETURN.equals(flight.getType())) {
				ctx.type = RouteTypeEnum.RETURN;
			}
			// domestic / intz / intc
			if (MmbLegTypeEnum.INC.equals(flight.getLegType())) {
				ctx.area = AreaValueEnum.INTC;
			} else if (MmbLegTypeEnum.INZ.equals(flight.getLegType()) && !AreaValueEnum.INTC.equals(ctx.area)) {
				ctx.area = AreaValueEnum.INTZ;
			}
		}
	}
	
	/**
	 * Internal utility method to initialize the MMB cart into context.
	 */
	private void initializeCart(MmbSessionContext ctx, String tid) {
		// prepare request
		RetrieveMmbAncillaryCatalogRequest catalogRequest = 
				mmbDelegate.buildCatalogRequestFromMmbRouteData(
				createBaseInfo(ctx), ctx.serviceClient, AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()), 
				ctx.machineName, ctx.market, ctx.pnr, ctx.route);
		catalogRequest.setSid(ctx.sid);
		catalogRequest.setTid(tid);
		
		// invoke service
		RetrieveMmbAncillaryCatalogResponse catalogResponse = mmbDelegate.getCatalog(catalogRequest);
		
		// read catalog data into context
		ctx.cart = catalogResponse.getCart();
		ctx.cartErrors = catalogResponse.getErrors();
	}
	
	/**
	 * Internal utility method to refersh the MMB cart into context.
	 */
	private void refreshCart(MmbSessionContext ctx, String tid) {
		logger.debug("refreshCart");
		
		if (ctx.cart == null) {
			logger.debug("Refresh cart request ignored: no cart is currently available.");
			return;
		}
		
		// prepare request
		RetrieveMmbAncillaryCartRequest cartRequest = new RetrieveMmbAncillaryCartRequest();
		cartRequest.setSid(ctx.sid);
		cartRequest.setTid(tid);
		cartRequest.setBaseInfo(createBaseInfo(ctx));
		cartRequest.setCartId(ctx.cart.getId());
		cartRequest.setClient(ctx.serviceClient);
		cartRequest.setMachineName(ctx.machineName);
		cartRequest.setPnr(ctx.pnr);
		cartRequest.setSessionId(ctx.sid);
		
		// invoke service
		RetrieveMmbAncillaryCartResponse cartResponse = 
				mmbDelegate.getCart(cartRequest);
		
		// read cart data into context
		ctx.cart = cartResponse.getCart();
		ctx.cartErrors = cartResponse.getErrors();
		
		// compute cart total amount
		MmbAncillaryStatusEnum ancillaryStatus = !ctx.ancillaryPaymentSuccess ? MmbAncillaryStatusEnum.TO_BE_ISSUED 
				: MmbAncillaryStatusEnum.ISSUED;
		ctx.cartTotalAmount = new BigDecimal(0);
		for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
			if (ancillaryStatus.equals(ancillary.getAncillaryStatus())) {
				ctx.cartTotalAmount = ctx.cartTotalAmount.add(ancillary.getAmount());
			}
		}
		ctx.cartTotalAncillariesAmount = ctx.cartTotalAmount;
		if (ctx.insuranceAddedToCart && ctx.insurancePolicy != null && !ctx.insurancePolicy.getBuy()
				|| (ctx.insurancePolicy != null && ctx.insurancePolicy.getBuy() && ctx.insurancePaymentSuccess) ) {
			ctx.cartTotalAmount = ctx.cartTotalAmount.add(ctx.insurancePolicy.getTotalInsuranceCost());
		}
		
	}
	
	/**
	 * Internal utility method to refresh the MMB insurance data into context.
	 */
	private void refreshInsurance(MmbSessionContext ctx, String tid) {
		MmbFlightData firstFlight = Lists.getFirst(ctx.route.getFlights());
		MmbFlightData firstOutboundFlight = Lists.getFirst(ctx.flightsByRouteType.get(RouteTypeEnum.OUTBOUND));
		MmbFlightData lastOutboundFlight = Lists.getLast(ctx.flightsByRouteType.get(RouteTypeEnum.OUTBOUND));
		MmbFlightData lastFlight = Lists.getLast(ctx.route.getFlights());
		
		// prepare request
		MmbInsuranceInfoRequest insuranceRequest = new MmbInsuranceInfoRequest();
		insuranceRequest.setSid(ctx.sid);
		insuranceRequest.setTid(tid);
		insuranceRequest.setCountryCode(ctx.market);
		insuranceRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		insuranceRequest.setRouteType(ctx.type);
		insuranceRequest.setArea(ctx.area);
		insuranceRequest.setPnr(ctx.pnr);
		insuranceRequest.setDepartureAirport(firstOutboundFlight.getFrom().getCode());
		insuranceRequest.setDepartureDate(firstOutboundFlight.getDepartureDateTime());
		insuranceRequest.setArrivalAirport(lastOutboundFlight.getTo().getCode());
		insuranceRequest.setArrivalDate(lastFlight.getArrivalDateTime());
		insuranceRequest.setPassengersCount(firstFlight.getPassengers().size());
	
		// invoke service
		InsuranceResponse insuranceResponse = 
				mmbDelegate.getInsurance(insuranceRequest);
		
		// read insurance data into context
		ctx.insurancePolicy = insuranceResponse.getPolicy();
	}
	
	/**
	 * Internal utility method to refresh the MMB static data data into context.
	 */
	private void refreshStaticMealTypesData(MmbSessionContext ctx, String tid) {
		logger.debug("refreshStaticMealTypesData");
		
		// prepare request
		RetrieveMealsRequest mealsRequest = new RetrieveMealsRequest(ctx.sid, tid);
		mealsRequest.setMarket(ctx.market);
		mealsRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		
		// invoke service
		RetrieveMealsResponse mealsResponse = 
				mmbDelegate.getMeals(mealsRequest);
		
		// read insurance data into context
		ctx.mealTypes = mealsResponse.getMeals();
	}
	
	/**
	 * Internal utility method to refresh the MMB flights seat maps data into context.
	 */
	private void refreshSeatMaps(MmbSessionContext ctx, String tid) {
		logger.debug("refreshSeatMaps");
		
		Map<MmbFlightData, SeatMapData> seatMapsByFlight = new HashMap<>();
		
		if (ctx.cart != null) {
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
				if (MmbAncillaryTypeEnum.SEAT.equals(ancillary.getAncillaryType())) {
					MmbFlightData flight = MmbUtils.findAncillaryFlight(ancillary, ctx.route.getFlights());
					if (!seatMapsByFlight.containsKey(flight)) {
						
						// prepare request
						RetrieveMmbFlightSeatMapRequest seatMapRequest = 
								new RetrieveMmbFlightSeatMapRequest(ctx.sid, tid);
						seatMapRequest.setFlightInfo(flight);
						seatMapRequest.setCompartimentalClass(Lists.asList(flight.getFareClass()));
						
						// invoke service
						RetrieveMmbFlightSeatMapResponse seatMapResponse = 
								mmbDelegate.getFlightSeatMap(seatMapRequest);
						
						// read insurance data into context
						SeatMapData seatMapData = null;
						if (seatMapResponse.getSeatMapMatrix() != null && 
								seatMapResponse.getSeatMapMatrix().getFlight() != null &&
								seatMapResponse.getSeatMapMatrix().getSeatMaps() != null &&
								seatMapResponse.getSeatMapMatrix().getSeatMaps().size() > 0) {
							seatMapData = seatMapResponse.getSeatMapMatrix().getSeatMaps().get(0);
						}
						seatMapsByFlight.put(flight, seatMapData);
						
					}
				}
			}
		}
		
		ctx.seatMapsByFlight = seatMapsByFlight;
	}
	
	/**
	 * Internal utility method to prepare a list of to be issued ancillaries and remove them from cart.
	 */
	private List<MmbAncillaryErrorData> removeAncillariesFromCart(MmbSessionContext ctx, String tid, 
			MmbAncillaryTypeEnum ancillaryType, Integer passengerIndex) throws Exception {
		
		List<Integer> itemIdsToRemove = new ArrayList<>();
		for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
			
			// check type and passenger
			if (ancillaryType != null && !ancillaryType.equals(ancillary.getAncillaryType())) {
				// no type match
				continue;
			} else if (passengerIndex != null) {
				MmbAncillaryPassengerData ancillaryPassenger = 
						MmbUtils.findAncillaryPassenger(ancillary, ctx.cart.getPassengers());
				if (ancillaryPassenger == null) {
					logger.error("Cannot find the passenger related to an ancillary.");
					throw new Exception("Cannot find the passenger related to an ancillary.");
				}
				if (!passengerIndex.equals(ctx.cart.getPassengers().indexOf(ancillaryPassenger))) {
					// no passenger match
					continue;
				}
			}
			
			// check status
			boolean remove = false;
			if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
				remove = true;
			} else if (MmbAncillaryStatusEnum.ISSUED.equals(ancillary.getAncillaryStatus())) {
				if (MmbAncillaryTypeEnum.MEAL.equals(ancillary.getAncillaryType())) {
					// meal ancillaries are free, can be removed from cart even if ISSUED status
					remove = true;
				} else if (MmbAncillaryTypeEnum.SEAT.equals(ancillary.getAncillaryType())) {
					// seat ancillaries, when free, can be removed from cart even if ISSUED status
					MmbAncillarySeatDetailData ancillaryDetail = 
							(MmbAncillarySeatDetailData) ancillary.getAncillaryDetail();
					if (!ancillaryDetail.isComfort()) {
						remove = true;
					} else {
						BigDecimal price = getAncillaryDisplayedPriceValue(ancillary, null);
						if (price == null || price.equals(BigDecimal.valueOf(0))) {
							remove = true;
						}
					}
				}
			}
			
			if (remove) {
				itemIdsToRemove.add(ancillary.getId());
			}
		}
		logger.debug("{} ancillaries to be cleared in cart.", itemIdsToRemove.size());
		
		List<MmbAncillaryErrorData> errors = new ArrayList<>();
		if (itemIdsToRemove.size() > 0) {
			MmbAncillaryRemoveFromCartRequest removeFromCartRequest = 
					new MmbAncillaryRemoveFromCartRequest();
			removeFromCartRequest.setTid(tid);
			removeFromCartRequest.setSid(ctx.sid);
			removeFromCartRequest.setSessionId(ctx.sid);
			removeFromCartRequest.setBaseInfo(createBaseInfo(ctx));
			removeFromCartRequest.setCartId(ctx.cart.getId());
			removeFromCartRequest.setClient(ctx.serviceClient);
			removeFromCartRequest.setMachineName(ctx.machineName);
			removeFromCartRequest.setItemIdsToRemove(itemIdsToRemove);
			
			MmbAncillaryRemoveFromCartResponse removeFromCartResponse = 
					mmbDelegate.removeFromCart(removeFromCartRequest);
			
			if (removeFromCartResponse.getErrors() != null) {
				errors.addAll(removeFromCartResponse.getErrors());
			}
		}
		
		return errors;
	}
	
	/**
	 * Internal utility method for insurance payment statistics
	 */
	private void paymentInsuranceStatistic(MmbSessionContext ctx, String creditCardType, 
			InsurancePolicyData insurancePolicy, boolean success) 
			throws Exception {
		logger.debug("paymentInsuranceStatistic");
		
		try {
			RegisterPaymentBookingInsuranceStatisticRequest bookingInsuranceStatisticRequest = 
					new RegisterPaymentBookingInsuranceStatisticRequest(IDFactory.getTid(), ctx.sid);
			bookingInsuranceStatisticRequest.setClientIP(ctx.callerIp);
			bookingInsuranceStatisticRequest.setSessionId(ctx.sid);
			bookingInsuranceStatisticRequest.setSiteCode(ctx.market);
			bookingInsuranceStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_BOOKING_INSURANCE);
			bookingInsuranceStatisticRequest.setCardType(creditCardType);
			bookingInsuranceStatisticRequest.setSuccess(success);
			
			if (insurancePolicy != null) {
				bookingInsuranceStatisticRequest.setPolicyNumber(insurancePolicy.getPolicyNumber());
				bookingInsuranceStatisticRequest.setErrorDescr(insurancePolicy.getErrorDescription());
			} else {
				bookingInsuranceStatisticRequest.setPolicyNumber("(unknown - PNR is " + ctx.pnr + ")");
				bookingInsuranceStatisticRequest.setErrorDescr("Null policy data received.");
			}
			
			RegisterStatisticsResponse statisticsResponse = 
					registerStatisticsDelegate.registerStatistics(bookingInsuranceStatisticRequest);
			
			logger.debug("Insurance statistics invoked, result is {}", statisticsResponse.getResult());
			
		} catch (Exception e) {
			logger.debug("An exception occurred invoking insurance statistics.", e);
		}
	}
	
	/**
	 * Internal utility method for check if there is a secure flight esta in the ctx.route
	 */
	private Boolean computeIsSecureFlightESTA(MmbSessionContext ctx) {
		Set<String> countryCodesSecureFlightESTA = 
				bookingSession.createSetFromProperty(BookingSession.SECUREFLIGHT_ESTA_COUNTRY_CODES_PROPERTY);
		Set<String> countryCodesToCheck = new HashSet<String>();
		
		if (ctx.route != null) {
			for (MmbFlightData flightData : ctx.route.getFlights()) {
				countryCodesToCheck.add(flightData.getFrom().getCountryCode());
				countryCodesToCheck.add(flightData.getTo().getCountryCode());
			}
		}
		countryCodesToCheck.retainAll(countryCodesSecureFlightESTA);
		return (countryCodesToCheck.size() > 0);
	}
	
	/**
	 * Internal utility for get all credit cards available for payment in mmb
	 */
	private void loadCreditCards(MmbSessionContext ctx, String tid) {
		try {
			RetrievePaymentTypeItemRequest retrievePaymentTypeItemRequest = new RetrievePaymentTypeItemRequest();
			retrievePaymentTypeItemRequest.setSid(ctx.sid);
			retrievePaymentTypeItemRequest.setTid(tid);
			retrievePaymentTypeItemRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
			retrievePaymentTypeItemRequest.setMarket(ctx.locale.getCountry());
			
			RetrievePaymentTypeItemResponse retrievePaymentTypeItemResponse = 
					mmbDelegate.getPaymentTypeItem(retrievePaymentTypeItemRequest);
			
			ctx.creditCards = retrievePaymentTypeItemResponse.getPaymentTypeItemsData();
			
		} catch (Exception e) {
			logger.debug("[getCreditCards] exception: ", e);
		}
	}

	/**
	 * NOT USED
	 * It check if the baggage is available by carrier. It returns true 
	 * if and only if the carrier is AZ and the operational carrier is 
	 * empty or AZ or null for all flights.
	 * @param extraBaggage
	 * @return
	 */
	public boolean isExtraBaggageAvailableByCarrier(
			MmbAncillaryExtraBaggageRender extraBaggage, String carrier) {
		
		for (MmbFlightDataRender flightDataRender : extraBaggage.getFlightDataRenderList()) {
			MmbFlightData flightData = flightDataRender.getFlightData();
			if (!flightData.getCarrier().equalsIgnoreCase(carrier)) {
				return false;
			}
			if (flightData.getCarrier() != null && !flightData.getOperatingCarrier().equals("")) {
				if (!flightData.getOperatingCarrier().equalsIgnoreCase(carrier)) {
					return false;
				}
			} 
		}
		return true;
	}
	
	private boolean isDirectFlight(MmbSessionContext ctx, RouteTypeEnum routeType){
		List<MmbFlightData> listFlight = ctx.route.getFlights();
		int numberFlight = 0;
		for(MmbFlightData flightData : listFlight){
			if(flightData.getType() == routeType){
				numberFlight++;
			}
		}
		if(numberFlight > 1){
			return false;
		} else{
			return true;
		}
	}
	
	/*public void setCartForPdf(MmbSessionContext ctx, MmbAncillaryCartData cart) {
		ctx.cartForPdf = cart;
	}*/
	
	/**
	 * Verify if all flights are miniFare.
	 * 
	 * @param ctx The MMB session context.
	 * @return true if all flights are miniFare, false otherwise.
	 */
	public boolean areAllMiniFare(MmbSessionContext ctx) {
		
		List<MmbFlightData> flights = ctx.route.getFlights();
		for (MmbFlightData flight : flights) {
			if (!flight.isMiniFare()) {
				return false;
}
		}
		return true;
	}
	
}
