package com.alitalia.aem.consumer.model.content.preparaviaggio;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class PreparaViaggioModel extends GenericBaseModel {

	@Inject
	private AlitaliaConfigurationHolder configuration;

	private Logger logger = LoggerFactory.getLogger(PreparaViaggioModel.class);

	private String webCheckinBaseUrlCheckin;
	private String webCheckinBaseUrlCheckinRedirectHandler;
	private String webCheckinBaseUrlCheckinSearchHandler;
	private String webCheckinBaseUrlMyFlight;
	private String webCheckinBaseUrlMyFlightSearchHandler;
	private String webCheckinBaseUrlMyFlightRedirectHandler;
	private String flightInfoUrlRedirect;
	private String timeTableURL;
	private String boxLoginRedirect;
	
	@Self
	private SlingHttpServletRequest request;
	
	@PostConstruct
	protected void initModel() {
		
		super.initBaseModel(request);
		logger.debug("[PreparaViaggioModel] activate");
		String protocol = "http://";
    	if(configuration.getHttpsEnabled()){
    		protocol = "https://";
    	}
		webCheckinBaseUrlCheckin = configuration.getWebCheckinUrlCheckin();
		webCheckinBaseUrlMyFlight = configuration.getWebCheckinUrlMyFlight();
		webCheckinBaseUrlCheckinRedirectHandler =
				configuration.getWebCheckinUrlCheckinRedirectHandler();
		webCheckinBaseUrlCheckinSearchHandler =
				configuration.getWebCheckinUrlCheckinSearchHandler();
		webCheckinBaseUrlMyFlightSearchHandler =
				configuration.getWebCheckinUrlMyFlightRedirectHandler();
		webCheckinBaseUrlMyFlightRedirectHandler =
				configuration.getWebCheckinUrlMyFlightRedirectHandler();
		flightInfoUrlRedirect = protocol + configuration.getExternalDomain() 
				+ request.getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ configuration.getFlightInfoPage());
		boxLoginRedirect = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ configuration.getMyFlightsPage();
		timeTableURL = request.getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false)
				+ configuration.getBookingFlightInfoListPage());
	}

	public String getWebCheckinBaseUrlCheckin() {
		return webCheckinBaseUrlCheckin;
	}

	public String getWebCheckinBaseUrlMyFlight() {
		return webCheckinBaseUrlMyFlight;
	}

	public String getWebCheckinBaseUrlCheckinRedirectHandler() {
		return webCheckinBaseUrlCheckinRedirectHandler;
	}

	public String getWebCheckinBaseUrlCheckinSearchHandler() {
		return webCheckinBaseUrlCheckinSearchHandler;
	}
	
	public String getFlightInfoUrlRedirect() {
		return flightInfoUrlRedirect;
	}

	public String getWebCheckinBaseUrlMyFlightSearchHandler() {
		return webCheckinBaseUrlMyFlightSearchHandler;
	}

	public String getWebCheckinBaseUrlMyFlightRedirectHandler() {
		return webCheckinBaseUrlMyFlightRedirectHandler;
	}
	
	public String getBoxLoginRedirect() {
		return boxLoginRedirect;
	}

	public String getTimeTableURL() {
		return timeTableURL;
	}

}
