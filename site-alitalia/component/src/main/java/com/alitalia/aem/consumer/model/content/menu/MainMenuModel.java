package com.alitalia.aem.consumer.model.content.menu;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MainMenuModel extends GenericMenuModel {

	private Logger logger = LoggerFactory.getLogger(MainMenuModel.class);

	@Self
	private SlingHttpServletRequest request;

	@Inject
	@Default(values = AlitaliaConstants.DEFAULT_MENU_DEPTH)
	private String menuDepthString;

	@Inject
	private Page currentPage;

	// This object defines the whole menu
	private MenuItem[] menu;

	private String homepath;
	private Resource rootResource;
	private int menuDepth;

	@PostConstruct
	protected void initModel() {
		
		super.initBaseModel(request);
		final Resource resource = request.getResource();
		InheritanceValueMap vm = new HierarchyNodeInheritanceValueMap(
				currentPage.getContentResource());

		if (vm.getInherited("homepath", String.class) != null || 
				("").equals(vm.getInherited("homepath", String.class))) {
			
			homepath = vm.getInherited("homepath", String.class);
			logger.debug("HOMEPATH recuperata dalle property ereditate: " + homepath);
			
		} else {
			
			homepath = AlitaliaUtils.findSiteBaseRepositoryPath(resource);
			logger.debug("HOMEPATH recuperata dal path della richiesta: " + homepath);
			
		}
		rootResource = resource.getResourceResolver().getResource(homepath);

		if (rootResource != null) {
			if(logger.isDebugEnabled())
				logger.debug("RootResource recuperata correttamente: " +
					rootResource.getPath());
		} else {
			logger.debug("RootResource NULL");
		}

		menuDepth = Integer.parseInt(menuDepthString);
		if(logger.isDebugEnabled())
			logger.debug("Menu depth: " + menuDepth);
		menu = getPageList(rootResource.adaptTo(Page.class), menuDepth,
					request, logger);

		// Faccio il set dell'attributo menuClassNumber per tutti gli elementi
		// del menu di secondo livello
		for (MenuItem item : menu) {
			MenuItem[] children = item.getItemChildren();
			int childrenSize = children.length;
			for (int index = 0; index < childrenSize; index++) {
				children[index].setMenuClassNumber(calcolaMenuClassNumber(
						childrenSize, index));
			}
		}
		
	}

	/**
	 * @param itemNum
	 *            : Number of elements of menu
	 * @param colIndex
	 *            : Zero-based index of the menu item
	 * @return The class number attribute of the corresponding menu item to be
	 *         used in the j-colType class
	 */
	private int calcolaMenuClassNumber(int itemNum, int colIndex) {
		int classNumber = 0;
		// Se il numero di elementi è < 5 non metto alcuna classe
		if (itemNum <= 5)
			return classNumber;
		// Caso in cui il numero di elementi è diverso da 8+5*n
		if ((itemNum - 3) % 5 != 0) {
			classNumber = (colIndex % 5) + 1;
		} // Caso in cui il numero di elementi è pari a 8+5*n
		else if (colIndex > itemNum - 4) {
			classNumber = (colIndex % 5) + 2;
		} else {
			classNumber = (colIndex % 5) + 1;
		}
		return classNumber;
	}

	/**
	 * Returns all the elements of the Alitalia menu
	 * 
	 * @return The menu
	 */
	public MenuItem[] getMenu() {
		return menu;
	}

}
