package com.alitalia.aem.consumer.carnet.servlet;

import java.text.NumberFormat;
import java.util.Locale;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.carnet.CarnetConstants;
import com.alitalia.aem.consumer.carnet.CarnetSession;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "carnetloginconsumer" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CarnetLoginServlet extends GenericCarnetFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile CarnetSession carnetSession;	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		Validator validator = new Validator();
		
		// recupero dati dalla request
		String codiceCarnet = request.getParameter("codiceCarnet");
		String password = request.getParameter("password");

		//validazione
		validator.addDirectCondition("codiceCarnet", codiceCarnet, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("password", password, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		
		HttpSession session = request.getSession(true);
		
		String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		String currencyCode = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencyCode", String.class);
		String currencySymbol = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencySymbol", String.class);
		Locale locale = AlitaliaUtils.getRepositoryPathLocale(request.getResource());
		String callerIp = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
		I18n i18n = new I18n(request.getResourceBundle(locale));
		
		/*Currency format info*/
		InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(request.getResource());
		String decimalSeparator=ivm.getInherited("decimalSeparator", String.class);
		String groupingSeparator=ivm.getInherited("groupingSeparator", String.class);
		Integer numDecDigits=ivm.getInherited("decimalDigit", Integer.class);
		if(numDecDigits == null){
			numDecDigits = 2;
		}
		NumberFormat currentNumberFormat = AlitaliaUtils.getNumberFormat(numDecDigits, decimalSeparator, groupingSeparator, market);
		
		// Inizializzo la sessione
		CarnetSessionContext ctx = carnetSession.initializeSession(request, i18n, CarnetConstants.CARNET_SID,
				market, currencyCode, currencySymbol, locale, 
				callerIp, currentNumberFormat);
		
		session.setAttribute(CarnetConstants.CARNET_CONTEXT_ATTRIBUTE, ctx);
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		
		
		//execute login
		String codiceCarnet = request.getParameter("codiceCarnet");
		String password = request.getParameter("password");
		
		boolean isLogged = carnetSession.performLogin(ctx,codiceCarnet,password);
		
		json.object();
		
		String redirectUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getCarnetHomeLoggedPage();
		redirectUrl = request.getResourceResolver().map(redirectUrl);

		if(isLogged){
			json.key("redirect").value(redirectUrl);
		} else{
			json.key("isError");
				json.object();
				json.key("fields");
					json.object();
					json.key("codiceCarnet").value(i18n.get(CarnetConstants.MESSAGE_INVALID_LOGIN));
					json.endObject();
				json.key("result").value(false);
			json.endObject();
		}
			
			
		json.endObject();
		
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	
}

