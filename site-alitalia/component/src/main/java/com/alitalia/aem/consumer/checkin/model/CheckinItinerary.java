package com.alitalia.aem.consumer.checkin.model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.data.home.enumerations.MmbRouteStatusEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.render.CheckinFlightRender;
import com.alitalia.aem.consumer.checkin.render.CheckinFlightsListRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinItinerary extends GenericCheckinModel {
	
	@Inject
	AlitaliaConfigurationHolder configurationHolder;
	
	@Self
	private SlingHttpServletRequest request;
	
	// flights
	private List<CheckinFlightsListRender> flightsList;
	private List<CheckinFlightRender> itineraryFlights;
	private int flightsNumber;
	private boolean inPath;
	private String customerName;
	private boolean timeRestriction;
	
	@PostConstruct
	protected void initModel() {
		
		initBaseModel(request);
		
		flightsList = new ArrayList<CheckinFlightsListRender>();
		
		String path = request.getRequestURL().toString();
		if (request.getRequestPathInfo().getSelectorString() != null &&
				!request.getRequestPathInfo().getSelectorString().isEmpty()) {
			path = request.getRequestURL().toString().replace(
					request.getRequestPathInfo().getSelectorString() + ".", "");
		}
		
		// check if the page is not the first page (flights list)
		inPath = !path.endsWith(configurationHolder.getCheckinFlightListPage());
		
		if (ctx != null) {
			customerName =
					(ctx.searchType.equals(CheckinConstants.CHECKIN_SEARCH_LOGIN))
					? ctx.passengerName : null;
		
			if (inPath) {
				if (ctx.selectedRoute != null) {
					CheckinFlightsListRender flr = new CheckinFlightsListRender(
							ctx.selectedRoute, 0, ctx);
					flightsList.add(flr);
					itineraryFlights = flr.getFlights();
				}
			} else {
				
				ResourceResolver rs =
						request.getResource().getResourceResolver();
				
				String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(
						request.getResource(), false);
				
				// no map required because it is an anchor url
				String flightsListUrl = configurationHolder.getCheckinFlightListPage();
				String passengersUrl = baseUrl + flightsListUrl.substring(
						0, flightsListUrl.length() - 5) + "." + 
						CheckinConstants.PREPARE_PASSENGERS_SERVLET_SELECTOR;
				
				String manageSeatUrl = rs.map(
						baseUrl + flightsListUrl.substring(
								0, flightsListUrl.length() - 5) + "." + 
								CheckinConstants.PREPARE_ANCILLARY_SERVLET_SELECTOR);
				
				String manageBoardingPassUrl = rs.map(
						baseUrl + flightsListUrl.substring(
								0, flightsListUrl.length() - 5) + "." + 
								CheckinConstants.PREPARE_BOARDING_PASS_SERVLET_SELECTOR);
				
				String manageUndoUrl = rs.map(
						baseUrl + flightsListUrl.substring(
								0, flightsListUrl.length() - 5) + "." + 
								CheckinConstants.PREPARE_UNDO_SERVLET_SELECTOR);
			
				// extract flights from service response stored into session (ctx)
				int flight_number = -1;
				for (CheckinRouteData crd : ctx.routesList) {
					CheckinFlightsListRender flr = new CheckinFlightsListRender(
							crd, ++flight_number, passengersUrl, manageSeatUrl,
							manageBoardingPassUrl, manageUndoUrl, ctx);
					flightsList.add(flr);
		
					// set first flight with check-in enabled
					if (ctx.searchType.equals(CheckinConstants.CHECKIN_SEARCH_PNR)
							&& itineraryFlights == null && flr.isCheckinEnabled()) {
						itineraryFlights = flr.getFlights();
					}
					
					if (MmbRouteStatusEnum.SPECIAL_TIME_RESTRICTION.equals(crd.getStatus())) {
						timeRestriction = true;
					}
				}
			}
		}
		flightsNumber = itineraryFlights != null ? itineraryFlights.size() : 0;

	}
	
	public List<CheckinFlightsListRender> getFlightsList() {
		return flightsList;
	}

	public List<CheckinFlightRender> getItineraryFlights() {
		return itineraryFlights;
	}

	public int getFlightsNumber() {
		return flightsNumber;
	}
	
	public boolean isInPath() {
		return inPath;
	}

	public String getCustomerName() {
		return customerName;
	}

	public boolean isTimeRestriction() {
		return timeRestriction;
	}

}