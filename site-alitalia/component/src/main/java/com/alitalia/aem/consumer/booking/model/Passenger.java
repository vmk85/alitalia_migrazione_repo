package com.alitalia.aem.consumer.booking.model;

import java.util.Calendar;
import java.util.HashMap;

import com.alitalia.aem.consumer.booking.model.PassengerType;

public class Passenger {
	
	private PassengerType passengerType;
	private String name;
	private String secondName;
	private String surname;
	private Calendar birthDate;
	private Sex sex;
	private String frequentFlyerProgram;
	private String frequentFlyerCardNumber;
	private String blueBizCode;
	private String skyBonusCode;
	private String mealPreference;
	private String passportNumber;
	private String nationality;
	private Integer referenceAdult;
	private HashMap<Integer, String> seat;
	
	public PassengerType getPassengerType() {
		return passengerType;
	}

	public void setPassengerType(PassengerType passengerType) {
		this.passengerType = passengerType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Calendar getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Calendar birthDate) {
		this.birthDate = birthDate;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public String getFrequentFlyerProgram() {
		return frequentFlyerProgram;
	}

	public void setFrequentFlyerProgram(String frequentFlyerProgram) {
		this.frequentFlyerProgram = frequentFlyerProgram;
	}

	public String getFrequentFlyerCardNumber() {
		return frequentFlyerCardNumber;
	}

	public void setFrequentFlyerCardNumber(String frequentFlyerCardNumber) {
		this.frequentFlyerCardNumber = frequentFlyerCardNumber;
	}

	public String getBlueBizCode() {
		return blueBizCode;
	}

	public void setBlueBizCode(String blueBizCode) {
		this.blueBizCode = blueBizCode;
	}

	public String getMealPreference() {
		return mealPreference;
	}

	public void setMealPreference(String mealPreference) {
		this.mealPreference = mealPreference;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Integer getReferenceAdult() {
		return referenceAdult;
	}

	public void setReferenceAdult(Integer referenceAdult) {
		this.referenceAdult = referenceAdult;
	}
	
	public HashMap<Integer, String> getSeat() {
		return seat;
	}

	public void setSeat(HashMap<Integer, String> seat) {
		this.seat = seat;
	}
	public boolean isInfant(){
		return (this.passengerType == PassengerType.INFANT ? true : false);
	}

	public String getSkyBonusCode() {
		return skyBonusCode;
	}

	public void setSkyBonusCode(String skyBonusCode) {
		this.skyBonusCode = skyBonusCode;
	}
}
