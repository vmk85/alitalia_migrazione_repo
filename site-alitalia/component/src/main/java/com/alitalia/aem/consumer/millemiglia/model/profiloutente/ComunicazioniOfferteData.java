package com.alitalia.aem.consumer.millemiglia.model.profiloutente;

public class ComunicazioniOfferteData {

	private String mailingType;
	private Boolean smsAuth;
	private String language;
	private Boolean profiling;
	
	public String getMailingType() {
		return mailingType;
	}
	
	public void setMailingType(String mailingType) {
		this.mailingType = mailingType;
	}
	
	public Boolean getSmsAuth() {
		return smsAuth;
	}
	
	public void setSmsAuth(Boolean smsAuth) {
		this.smsAuth = smsAuth;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}
	
	public Boolean getProfiling() {
		return profiling;
	}
	
	public void setProfiling(Boolean profiling) {
		this.profiling = profiling;
	}
}
