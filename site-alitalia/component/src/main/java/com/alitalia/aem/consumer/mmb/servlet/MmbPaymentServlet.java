package com.alitalia.aem.consumer.mmb.servlet;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.DictionaryItemData;
import com.alitalia.aem.common.data.home.PaymentTypeItemData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.mmb.MmbUtils;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "mmbpaymentconsumer" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })})
@SuppressWarnings("serial")
public class MmbPaymentServlet extends GenericMmbFormValidatorServlet {

	private static final String DEFAULT_CARD_NAME = "defaultCard";
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile MmbSession mmbSession;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		
		MmbSessionContext ctx = getMmbSessionContext(request);
		
		String creditCardTypeCode = request.getParameter("creditCardType");
		boolean payWithDefaultCreditCard = DEFAULT_CARD_NAME.equals(creditCardTypeCode);
		if (payWithDefaultCreditCard) {
			if (ctx.defaultCardType != null || !"".equals(ctx.defaultCardType)) {
				creditCardTypeCode = MmbUtils.getCodeFromCreditCardTypeEnum(ctx.defaultCardType);
			}
		}
		
		//cerco il maxCvvLength con cui validare il cvv per la carta di credito selezionata
		Integer maxCvvLength = 0;
		for (PaymentTypeItemData creditCard : ctx.creditCards) {
			if (creditCardTypeCode.equals(creditCard.getCode())) {
				for (DictionaryItemData data : creditCard.getOtherInfo()) {
					if (data.getKey().equals("MaxCVC")) {
						maxCvvLength = (Integer) data.getValue();
						break;
					}
				}
				break;
			}
		}
		
		Validator validator = new Validator();
		
		if (payWithDefaultCreditCard) {
			// recupero dati dalla request
			String defaultCvv = request.getParameter("defaultCvv");
			
			validator.addDirectCondition("defaultCvv", defaultCvv, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("defaultCvv", defaultCvv, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD,
					maxCvvLength == 4 ? "isCVCFourDigits" : "isCVCThreeDigits");
			
		} else {
			// recupero dati dalla request
			String numeroCarta = request.getParameter("numeroCarta-" + creditCardTypeCode);
			String meseScadenzaCarta = request.getParameter("meseScadenzaCarta-" + creditCardTypeCode);
			String annoScadenzaCarta = request.getParameter("annoScadenzaCarta-" + creditCardTypeCode);
			String cvv = request.getParameter("cvv-" + creditCardTypeCode);
			String nomeTitolare = request.getParameter("nomeTitolare-" + creditCardTypeCode);
			String cognomeTitolare = request.getParameter("cognomeTitolare-" + creditCardTypeCode);
			
			
			//validazione
			validator.addDirectCondition("numeroCarta-" + creditCardTypeCode, numeroCarta, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			
			validator.addDirectCondition("meseScadenzaCarta-" + creditCardTypeCode, meseScadenzaCarta, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("meseScadenzaCarta-" + creditCardTypeCode, meseScadenzaCarta, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isMonth");
			
			validator.addDirectCondition("annoScadenzaCarta-" + creditCardTypeCode, annoScadenzaCarta, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("annoScadenzaCarta-" + creditCardTypeCode, annoScadenzaCarta, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isYear");
			
			validator.addDirectCondition("cvv-" + creditCardTypeCode, cvv, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("cvv-" + creditCardTypeCode, cvv, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, 
					maxCvvLength == 4 ? "isCVCFourDigits" : "isCVCThreeDigits");
			
			validator.addDirectCondition("nomeTitolare-" + creditCardTypeCode, nomeTitolare, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("nomeTitolare-" + creditCardTypeCode, nomeTitolare, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
			
			validator.addDirectCondition("cognomeTitolare-" + creditCardTypeCode, cognomeTitolare, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("cognomeTitolare-" + creditCardTypeCode, cognomeTitolare, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
		}
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

		MmbSessionContext ctx = getMmbSessionContext(request);
		
		String numeroCarta = "";
		Integer meseScadenzaCarta = null;
		Integer annoScadenzaCarta = null;
		String cvv = "";
		String nomeTitolare = "";
		String cognomeTitolare = "";
		String creditCardTypeName = "";
		
		// recupero dati dalla request
		
		BigDecimal displayedAmount = new BigDecimal(0);
		if (!StringUtils.isBlank(request.getParameter("displayedAmount"))) {
			displayedAmount = new BigDecimal(request.getParameter("displayedAmount"));
		}
		String creditCardTypeCode = request.getParameter("creditCardType");
		
		boolean payWithDefaultCreditCard = DEFAULT_CARD_NAME.equals(creditCardTypeCode);
		
		if (payWithDefaultCreditCard) {
			cvv = request.getParameter("defaultCvv");
			
			String defaultCardCode = MmbUtils.getCodeFromCreditCardTypeEnum(ctx.defaultCardType);
			for (PaymentTypeItemData creditCard : ctx.creditCards) {
				if (defaultCardCode.equals(creditCard.getCode())) {
					creditCardTypeCode = creditCard.getCode();
					creditCardTypeName = creditCard.getDescription();
					break;
				}
			}
			
		} else {
			numeroCarta = request.getParameter("numeroCarta-" + creditCardTypeCode);
			meseScadenzaCarta = Integer.parseInt(request.getParameter("meseScadenzaCarta-" + creditCardTypeCode));
			annoScadenzaCarta = Integer.parseInt(request.getParameter("annoScadenzaCarta-" + creditCardTypeCode));
			cvv = request.getParameter("cvv-" + creditCardTypeCode);
			nomeTitolare = request.getParameter("nomeTitolare-" + creditCardTypeCode);
			cognomeTitolare = request.getParameter("cognomeTitolare-" + creditCardTypeCode);
			
			for (PaymentTypeItemData creditCard : ctx.creditCards) {
				if (creditCardTypeCode.equals(creditCard.getCode())) {
					creditCardTypeName = creditCard.getDescription();
					break;
				}
			}
		}
		
 		mmbSession.performPayment(ctx, displayedAmount, numeroCarta, creditCardTypeName, creditCardTypeCode, cvv, meseScadenzaCarta, 
 				annoScadenzaCarta, nomeTitolare, cognomeTitolare, payWithDefaultCreditCard);
		
 		String redirect = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getMmbThankyouPage();
 		redirect = request.getResourceResolver().map(redirect);
 		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value("OK");
		json.key("redirect").value(redirect);
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected String getUnrecoverableErrorRedirect(
			SlingHttpServletRequest request, SlingHttpServletResponse response,
			GenericFormValidatorServletException exception) {
		return AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getMmbThankyouPage();
	}
	
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		Map additionalFailureData = super.getAdditionalFailureData(request, exception);
		if (exception.isRecoverable()) {
			additionalFailureData.put("redirect", 
					AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
					+ getConfiguration().getMmbPaymentPage());
		}
		return additionalFailureData;
	}
	
}
