package com.alitalia.aem.consumer.servlet.myflights;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightStatusEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.global.i18n.I18nKeyComponents;
import com.alitalia.aem.consumer.mmb.servlet.GenericMmbFormValidatorServlet;
import com.alitalia.aem.consumer.model.content.myflights.MyFlightsUtils;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.web.component.managemybooking.delegate.ManageMyBookingDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "mmbcheckmyflights" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckMyFlightsServlet extends GenericMmbFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile ManageMyBookingDelegate manageMyBookingDelegate;
	
	

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		MMCustomerProfileData loggedUserProfile = AlitaliaCustomerProfileManager.getAuthenticatedUserProfile(request);
		String customerNumber = loggedUserProfile.getCustomerNumber();
		String customerCode = "";
		if (customerNumber != null) {
			// Manage Customer Number to render it for the the service
			customerCode = MyFlightsUtils.getTripId(customerNumber);
		}
		String name = loggedUserProfile.getCustomerName();
		String lastname = loggedUserProfile.getCustomerSurname();
		Locale localRepository = AlitaliaUtils.getRepositoryPathLocale(
				request.getResource());
		String locale = AlitaliaUtils.getLanguage(AlitaliaUtils.getLanguage(localRepository.toLanguageTag())) + "_" + localRepository.getCountry();
		RetriveMmbPnrInformationRequest pnrRequest = new RetriveMmbPnrInformationRequest();
		pnrRequest.setTid(IDFactory.getTid());
		pnrRequest.setSid(IDFactory.getSid(request));
		pnrRequest.setTripId(customerCode);
		pnrRequest.setName(name);
		pnrRequest.setLastName(lastname);
		pnrRequest.setLocale(locale.toUpperCase());
		RetriveMmbPnrInformationResponse pnrResponse = manageMyBookingDelegate.retrivePnrInformation(pnrRequest);
		int mmbPrenotations = pnrResponse.getRoutesList().size();
		int checkInPrenotations = computeCheckinRoutesNumbers(pnrResponse.getRoutesList());
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value("OK");
		json.key("pnr").value(Integer.toString(mmbPrenotations));
		json.key("checkinPnr").value(Integer.toString(checkInPrenotations));
		String redirect = "";
		String message = "";
		String url;
		Locale localeFori18n = AlitaliaUtils.getPageLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(localeFori18n);
		final I18n i18n = new I18n(resourceBundle);
		if (mmbPrenotations > 0) {
			url = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
					+ getConfiguration().getMyFlightsPage();
			if (mmbPrenotations == 1) {
				message = i18n.get(I18nKeyComponents.PREPARAVIAGGIO_FLIGHTS_FOUND_PREFIX) + " " +  i18n.get(I18nKeyComponents.PREPARAVIAGGIO_ONEFLIGHT_FOUND_SUFFIX);
			}
			else {
				message = i18n.get(I18nKeyComponents.PREPARAVIAGGIO_FLIGHTS_FOUND_PREFIX) + " " + Integer.toString(mmbPrenotations) + " " + i18n.get(I18nKeyComponents.PREPARAVIAGGIO_FLIGHTS_FOUND_SUFFIX);
			}
		}
		else {
			url = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
					+ getConfiguration().getMmbHomePage();
		}
		String checkinRedirect = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCheckinFlightListPage();
		checkinRedirect = request.getResourceResolver().map(checkinRedirect);
		json.key("message").value(message);
		redirect = request.getResourceResolver().map(url);
		json.key("redirect").value(redirect);
		json.key("checkinRedirect").value(checkinRedirect);
		json.endObject();
	}

	

	private int computeCheckinRoutesNumbers(List<MmbRouteData> routesList) {
		return (int) routesList.stream().filter(route -> isCheckinOpen(route)).count();
	}
	
	private boolean isCheckinOpen(MmbRouteData route){
		boolean checkinOpen = false;
		for(MmbFlightData flight : route.getFlights()){
			checkinOpen = checkinOpen || flight.getStatus().equals(MmbFlightStatusEnum.INVALID);
		}
		return checkinOpen;
	}
	

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		// TODO Auto-generated method stub
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}

