package com.alitalia.aem.consumer.mmb.servlet;

import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.mmb.exception.InvalidAncillaryIdentifierException;
import com.alitalia.aem.consumer.mmb.exception.InvalidAncillaryStatusException;
import com.alitalia.aem.consumer.mmb.exception.MmbAncillaryServiceException;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;

@SuppressWarnings("serial")
public abstract class GenericMmbFormValidatorServlet
		extends GenericFormValidatorServlet {
	
	/**
	 * Utility method to access the MmbSessionContext instance.
	 * 
	 * <p>If the instance is not availble, an exception is thrown.</p>
	 * 
	 * @param request The related servlet request.
	 * @return The BookingSessionContext instance.
	 */
	protected MmbSessionContext getMmbSessionContext(SlingHttpServletRequest request) {
		MmbSessionContext ctx = (MmbSessionContext) 
				request.getSession(true).getAttribute(MmbConstants.MMB_CONTEXT_ATTRIBUTE);
		if (ctx == null) {
			throw new GenericFormValidatorServletException(false, "MMB session context not found.");
		} else {
			return ctx;
		}
	}
	
	/**
	 * Provide MMB-specific unrecoverable error page.
	 */
	@Override
	protected String getUnrecoverableErrorRedirect(SlingHttpServletRequest request, 
			SlingHttpServletResponse response, GenericFormValidatorServletException exception) {
		return AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getMmbFailurePage();
	}
	
	/**
	 * Provide custom management for MMB exceptions.
	 */
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		Map data = super.getAdditionalFailureData(request, exception);
		String errorMessage = "";
		try {
			errorMessage = getMmbSessionContext(request).i18n.get("mmb.common.serviceError.label");
		} catch (Exception e) {
			logger.debug("Could not translate error message header.");
		}
		String errorMessageDetail = "Unknown error";
		if (exception.getCause() != null) {
			if (exception.getCause() instanceof MmbAncillaryServiceException) {
				MmbAncillaryServiceException e = (MmbAncillaryServiceException) exception.getCause();
				errorMessageDetail = e.getMessage();
			} else if (exception.getCause() instanceof InvalidAncillaryIdentifierException) {
				errorMessageDetail = "Ancillary identifier not found";
			} else if (exception.getCause() instanceof InvalidAncillaryStatusException) {
				errorMessageDetail = "Ancillary status not applicable";
			}
		}
		data.put("errorMessage", errorMessage);
		data.put("errorMessageDetail", errorMessageDetail);
		return data;
	}
	
	/**
	 * Provide an empty implementation, should be never used as
	 * MMB servlets should always be invoked with ajax requests
	 */
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		logger.warn("The emtpy saveDataIntoSession method was unexpectedly invoked.");
	}
	
	/**
	 * Abstract method to be implemented in subclasses in order to
	 * provide the configuration holder component instance.
	 * 
	 * @return The AlitaliaConfigurationHolder instance.
	 */
	protected abstract AlitaliaConfigurationHolder getConfiguration();
	
}
