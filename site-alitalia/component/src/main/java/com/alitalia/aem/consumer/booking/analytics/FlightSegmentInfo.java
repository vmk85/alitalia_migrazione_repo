package com.alitalia.aem.consumer.booking.analytics;

import java.util.Calendar;

public class FlightSegmentInfo {
	private String SegBoapt;
	private String SegArapt;
	private Calendar SegdepDate;
	private Calendar SegarrDate;
	private int SegdepHours;
	private int SegarrHours;
	private int SegdepMinutes;
	private int SegarrMinutes;
	private String SegFlightNumber;
	private String SegCarrier;
	private String SegCabin;
	
	
	public String getSegBoapt() {
		return SegBoapt;
	}
	public void setSegBoapt(String segBoapt) {
		SegBoapt = segBoapt;
	}
	public String getSegArapt() {
		return SegArapt;
	}
	public void setSegArapt(String segArapt) {
		SegArapt = segArapt;
	}
	public Calendar getSegdepDate() {
		return SegdepDate;
	}
	public void setSegdepDate(Calendar segdepDate) {
		SegdepDate = segdepDate;
	}
	public Calendar getSegarrDate() {
		return SegarrDate;
	}
	public void setSegarrDate(Calendar segarrDate) {
		SegarrDate = segarrDate;
	}
	public int getSegdepHours() {
		return SegdepHours;
	}
	public void setSegdepHours(int i) {
		SegdepHours = i;
	}
	public int getSegarrHours() {
		return SegarrHours;
	}
	public void setSegarrHours(int segarrHours) {
		SegarrHours = segarrHours;
	}
	public int getSegdepMinutes() {
		return SegdepMinutes;
	}
	public void setSegdepMinutes(int segdepMinutes) {
		SegdepMinutes = segdepMinutes;
	}
	public int getSegarrMinutes() {
		return SegarrMinutes;
	}
	public void setSegarrMinutes(int segarrMinutes) {
		SegarrMinutes = segarrMinutes;
	}
	public String getSegFlightNumber() {
		return SegFlightNumber;
	}
	public void setSegFlightNumber(String segFlightNumber) {
		SegFlightNumber = segFlightNumber;
	}
	public String getSegCarrier() {
		return SegCarrier;
	}
	public void setSegCarrier(String segCarrier) {
		SegCarrier = segCarrier;
	}
	public String getSegCabin() {
		return SegCabin;
	}
	public void setSegCabin(String segCabin) {
		SegCabin = segCabin;
	}
}
