package com.alitalia.aem.consumer.mmb.analytics;

import java.math.BigDecimal;

public class AncillaryInfo {
	
	public static final int ONLY_NAME = 0;
	public static final int NAME_AND_QUANTITY = 1;
	public static final int NAME_QUANTITY_AND_REVENUE = 2;
	
	
	public String ancName;
	public Integer ancQuantity;
	public BigDecimal ancRevenue;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ancName == null) ? 0 : ancName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AncillaryInfo other = (AncillaryInfo) obj;
		if (ancName == null) {
			if (other.ancName != null)
				return false;
		} else if (!ancName.equals(other.ancName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AncillaryInfo [ancName=" + ancName + ", ancQuantity=" + ancQuantity + ", ancRevenue=" + ancRevenue
				+ "]";
	}
}
