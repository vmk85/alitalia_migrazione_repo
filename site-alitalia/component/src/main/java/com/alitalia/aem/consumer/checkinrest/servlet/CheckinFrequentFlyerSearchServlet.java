package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.Servlet;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Flight;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Segment;
import com.day.cq.i18n.I18n;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.checkinrest.CheckinFrequentFlyerSearchResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "searchbyfrequentflyer"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinFrequentFlyerSearchServlet extends GenericCheckinFormValidatorServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private CheckinSession checkinSession;

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		Validator validator = new Validator();

		String frequentFlyer = request.getParameter("frequentFlyer");
		String lastName = request.getParameter("lastName");

		validator.addDirectCondition("frequentFlyer", frequentFlyer, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		validator.addDirectCondition("lastName", lastName, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		validator.addDirectCondition("lastName", lastName, 
				CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");

		ResultValidation resultValidation = validator.validate();
		
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
//		logger.info("[CheckinFrequentFlyerSearchServlet - performSubmit] prima di initSession....");
		
		boolean resultSession = initSession(request, configuration, checkinSession);

		if (!resultSession){
			logger.error("Errore durante l'inizializzazione della sessione.");
			throw new Exception("Errore durante l'inizializzazione della sessione.");
		}else{
			
			CheckinSessionContext ctx = getCheckInSessionContext(request);
			
			if (ctx.pnrListData != null){
				ctx.pnrListData = new ArrayList<>();
			}	
			
			logger.info("[CheckinFrequentFlyerSearchServlet - performSubmit] recupero dati dal contesto.");

			try{
			
				logger.info("[CheckinFrequentFlyerSearchServlet] invoco il checkinSession");
				
				
				CheckinFrequentFlyerSearchResponse result = checkinSession.checkinFrequentFlyerSearch(request, ctx);
				
				logger.info("[CheckinFrequentFlyerSearchServlet] result=" + result);
				
				if (result != null) {
					if(result.getPnrData().getError() != null && !result.getPnrData().getError().equals("")){
						managementError(request, response, result.getPnrData().getError(),0);
					} else {
						//response.sendRedirect(successPage);
						request.getSession().setAttribute("searchbyfrequentflyerMM",result);
						int availableflights = 0;

						for(Pnr pnr : result.getPnrData().getPnr()){
							for(Flight flight : pnr.getFlights()){
								for(Segment segment : flight.getSegments()){
									if(segment.getOpenCI()){
										availableflights++;
									}
								}

							}
						}
						managementSucces(request, response,availableflights);
					}
				}
				else {
					logger.error("[CheckinFrequentFlyerSearchServlet] - Errore durante l'invocazione del servizio." );
				}
			}
			
			catch(Exception e){
				throw new RuntimeException("[CheckinFrequentFlyerSearchServlet] - Errore durante l'invocazione del servizio.", e);
			}
		}
	}

	public void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error, int availableflights){

		logger.info("[CheckinFrequentFlyerSearchServlet] [managementError] error =" + error );

		Gson gson = new Gson();

		com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);

		TidyJSONWriter json;
		try {
			final I18n i18n = new I18n(request.getResourceBundle(AlitaliaUtils.findResourceLocale(request.getResource())));
			json = new TidyJSONWriter(response.getWriter());

			response.setContentType("application/json");

			json.object();

			json.key("isError").value(true);
			json.key("errorMessage").value(i18n.get("checkin.search.input.error"));
			json.key("sabreStatusCode").value(errorObj.getSabreStatusCode());
			json.key("conversationID").value(errorObj.getConversationID());
			json.key("flights").value(availableflights);
			json.endObject();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
	}

	public void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response, int availableflights){

		logger.info("[CheckinFrequentFlyerSearchServlet] [managementSucces] ...");
		String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCheckinFlightListPage();

		successPage = request.getResourceResolver().map(successPage);

		logger.info("[CheckinFrequentFlyerSearchServlet] [managementSucces] successPage = " + successPage );

		TidyJSONWriter json;
		try {

			json = new TidyJSONWriter(response.getWriter());
			response.setContentType("application/json");
			json.object();
			json.key("isError").value(false);
			json.key("successPage").value(successPage);
			json.key("flights").value(availableflights);
			json.endObject();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
		
	}
	
}
