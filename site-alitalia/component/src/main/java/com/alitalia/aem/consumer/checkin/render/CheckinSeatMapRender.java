package com.alitalia.aem.consumer.checkin.render;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.alitalia.aem.common.data.home.checkin.CheckinSeatMapData;

public class CheckinSeatMapRender {
	
	private List<String> columns;
	private List<CheckinSeatMapSectorRender> sectors;
	private Set<String> comfortSeats;
	private String seatMapType;
	
	public CheckinSeatMapRender(List<CheckinSeatMapData> seatMapDataList, Set<String> assignedSeats) {
		
		if (seatMapDataList != null && !seatMapDataList.isEmpty()) {
			columns = seatMapDataList.get(0).getColumns();
			sectors = new ArrayList<CheckinSeatMapSectorRender>();
			comfortSeats = new HashSet<String>();
			
			int numSeats = 0;
			seatMapType = "";
			
			for (String column : columns) {
				if (column != null && !column.isEmpty()) {
					numSeats++;
				} else {
					seatMapType += numSeats + "_";
					numSeats = 0;
				}
			}
			seatMapType += numSeats;
			
			int i = 0;
			String sectorStart = "";
			String sectorEnd = "";
			
			List<CheckinSeatMapRowRender> rows = null;
			CheckinSeatMapSectorRender previousSector = null;
			
			for (CheckinSeatMapData seatMapData : seatMapDataList) {
				
				if (i % 14 == 0) {
					if (i > 0) {
						String label = sectorStart + "-" + sectorEnd;
						CheckinSeatMapSectorRender currentSector = new CheckinSeatMapSectorRender(label, rows);
						if (previousSector != null) {
							previousSector.setNext(label);
							currentSector.setPrevious(previousSector.getLabel());
						}
						previousSector = currentSector;
						
						sectors.add(currentSector);
					}
					rows = new ArrayList<CheckinSeatMapRowRender>();
					
					sectorStart = seatMapData.getNumber() != null ?
							seatMapData.getNumber().toString() : "";
				}
				
				sectorEnd = seatMapData.getNumber() != null ?
						seatMapData.getNumber().toString() : "";
				rows.add(new CheckinSeatMapRowRender(seatMapData, assignedSeats, comfortSeats));
				i++;
			}
			
			String label = sectorStart + "-" + sectorEnd;
			CheckinSeatMapSectorRender currentSector = new CheckinSeatMapSectorRender(label, rows);
			if (previousSector != null) {
				previousSector.setNext(label);
				currentSector.setPrevious(previousSector.getLabel());
			}
			sectors.add(currentSector);
		}
	}

	public List<String> getColumns() {
		return columns;
	}

	public void setColumns(List<String> columns) {
		this.columns = columns;
	}

	public List<CheckinSeatMapSectorRender> getSectors() {
		return sectors;
	}

	public void setSectors(List<CheckinSeatMapSectorRender> sectors) {
		this.sectors = sectors;
	}

	public Set<String> getComfortSeats() {
		return comfortSeats;
	}

	public void setComfortSeats(Set<String> comfortSeats) {
		this.comfortSeats = comfortSeats;
	}

	public String getSeatMapType() {
		return seatMapType;
	}

	public void setSeatMapType(String seatMapType) {
		this.seatMapType = seatMapType;
	}
}
