package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.MillemigliaKidsData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "millemigliakidssubmit" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class MillemigliaKidsServlet extends GenericFormValidatorServlet{
	
	private static final int DIM_FORM = 9;
	private static final String KEYVALUE_SEPARATOR = ":";
	private static final String ELEMENT_SEPARATOR =
			System.getProperty("line.separator");
	
	// Static Property For Mails
	private static final String COMPONENT_NAME = "millemiglia_kids";
	private static final String PARSYS_NAME = "page-component";
	private static final String MAIL_FROM_PROPERTY_NAME = "mailFrom";
	private static final String MAIL_TO_PROPERTY_NAME = "mailTo";
	private static final String MAIL_SUBJECT_PROPERTY_NAME = "subject";
	private static final String MAIL_FROM_DEFAULT = "millemigliakids@alitalia.it";
	private static final String MAIL_TO_DEFAULT = "family@dataar.it";
	private static final String MAIL_SUBJECT_DEFAULT = "Family - Italia";
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BusinessLoginService businessLoginService;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[MillemigliaKidsServlet] validateForm");
		
		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			return validator.validate();
			
		}
		catch (Exception e) {
			logger.error("Errore durante la procedura di validazione dati "
					+ "richiesta millemiglia kids", e);
			throw e;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		logger.debug("[MillemigliaKidsServlet] performSubmit");
		try{
			boolean mailResult = sendMail(request);
			logger.debug("Mail Sended = " + mailResult);
			if (mailResult) {
				goBackSuccessfully(request, response);
			}
			else {
				throw new IOException("Service Error");
			}
		} catch(Exception e){
			logger.error("Unexpected Errore", e);
			throw e;
		}
	}
	
	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {

		// get parameters from request
		String cod_millemiglia = request.getParameter("cod_millemiglia");
		String parent_name = request.getParameter("parent_name");
		String parent_surname = request.getParameter("parent_surname");
		String email = request.getParameter("email");
		String child_name = request.getParameter("child_name");
		String child_surname = request.getParameter("child_surname");
		String gender = request.getParameter("gender");
		String day = request.getParameter("day");
		String month = request.getParameter("month");
		String year = request.getParameter("year");
		String accettocond=request.getParameter("accettocond"); 
		
		String mmcode_size="9";
		
		// add validate conditions
		validator.addDirectConditionMessagePattern("cod_millemiglia",
				cod_millemiglia,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_EMPTY,
				"isNotEmpty");
		
		validator.addCrossConditionMessagePattern("cod_millemiglia",
				cod_millemiglia, mmcode_size,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID,
				"sizeIsLess");
		
		validator.addDirectConditionMessagePattern("cod_millemiglia", 
				cod_millemiglia,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID,
				"isNumberWithSpace");
		
		validator.addDirectConditionMessagePattern("parent_name",
				parent_name,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY,
				"isNotEmpty");
		
		validator.addDirectConditionMessagePattern("parent_name",
				parent_name,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.NAME_ERROR_NOT_VALID,
				"isAlphabeticWithSpaces");
		
		validator.addDirectConditionMessagePattern("parent_surname",
				parent_surname,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.SURNAME_ERROR_EMPTY,
				"isNotEmpty");
		
		validator.addDirectConditionMessagePattern("parent_surname",
				parent_surname,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.SURNAME_ERROR_NOT_VALID,
				"isAlphabeticWithSpaces");
		
		validator.addDirectConditionMessagePattern("email",
				email,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.EMAIL_ERROR_EMPTY,
				"isNotEmpty");
		validator.addDirectConditionMessagePattern("email",
				email,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID,
				"isEmail");
		
		validator.addDirectConditionMessagePattern("child_name",
				child_name,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY,
				"isNotEmpty");
		validator.addDirectConditionMessagePattern("child_name",
				child_name,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.NAME_ERROR_NOT_VALID,
				"isAlphabeticWithSpaces");
		
		validator.addDirectConditionMessagePattern("child_surname",
				child_surname,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.SURNAME_ERROR_EMPTY,
				"isNotEmpty");
		validator.addDirectConditionMessagePattern("child_surname",
				child_surname,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.SURNAME_ERROR_NOT_VALID,
				"isAlphabeticWithSpaces");
		
		validator.addDirectConditionMessagePattern("day",
				day,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.BIRTH_DAY_ERROR_NOT_EMPTY,
				"isNotEmpty");
		
		validator.addDirectConditionMessagePattern("month",
				month,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.BIRTH_DAY_ERROR_NOT_EMPTY,
				"isNotEmpty");
		
		validator.addDirectConditionMessagePattern("year",
				year,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.BIRTH_DAY_ERROR_NOT_EMPTY,
				"isNotEmpty");
		
		validator.addDirectConditionMessagePattern("gender",
				gender,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.GENDER_ERROR_EMPTY,
				"isNotEmpty");
		
		validator.addCrossConditionMessagePattern("accettocond",
				accettocond, "accepted",
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT,
				"areEqual");
		
		// validate date
		if (null != day && null != month && null != year) {
			String date = day + "/" + month + "/" + year;
			validator.addDirectConditionMessagePattern("day",
					date,
					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
					I18nKeySpecialPage.BIRTH_DAY_ERROR_NOT_EMPTY,
					"isBeforeNow");
		}
		
		return validator;

	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[ MillemigliaKidsServlet] saveDataIntoSession");
		
		// create baggageClaimData object and save it into session
		MillemigliaKidsData millemigliaKidsData = new MillemigliaKidsData();
		millemigliaKidsData.setParent_name(
				request.getParameter("parent_name"));
		millemigliaKidsData.setParent_surname(
				request.getParameter("parent_surname"));
		millemigliaKidsData.setCod_millemiglia(
				request.getParameter("cod_millemiglia"));
		millemigliaKidsData.setEmail(
				request.getParameter("email"));
		millemigliaKidsData.setAccettocond(
				request.getParameter("accettocond"));
		millemigliaKidsData.setChild_name(
				request.getParameter("child_name"));
		millemigliaKidsData.setChild_surname(
				request.getParameter("child_surname"));
		millemigliaKidsData.setGender(
				request.getParameter("gender"));
		
		try {
			millemigliaKidsData.setDay(Integer.parseInt(
					request.getParameter("day")));
		} catch (NumberFormatException n) { };
		
		try {
			millemigliaKidsData.setMonth(Integer.parseInt(
					request.getParameter("month")));
		} catch (NumberFormatException n) { };
		
		try{
			millemigliaKidsData.setYear(Integer.parseInt(
					request.getParameter("year")));
		} catch (NumberFormatException n) { };
		
		
		// go back to form with error
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		millemigliaKidsData.setError(i18n.get(I18nKeySpecialPage.ERROR_SERVICE));
		
		request.getSession().setAttribute(MillemigliaKidsData.NAME,
				millemigliaKidsData);
		
	}
	
	/*
	 * sendMail
	 * 
	 */
	private boolean sendMail(SlingHttpServletRequest request) {
		logger.debug("[ MillemigliaKidsServlet] sendMail");
		
		// current page
		Page currentPage = AlitaliaUtils.getPage(request.getResource());
		
		// email from
	    String mailFrom = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_FROM_PROPERTY_NAME);
	    if (mailFrom == null) {
	    	mailFrom = MAIL_FROM_DEFAULT;
	    }
	    
	    // email to
	    String mailTo = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_TO_PROPERTY_NAME);
	    if (mailTo == null) {
	    	mailTo = MAIL_TO_DEFAULT;
	    }
	    
	    // email subject
	    String subject = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_SUBJECT_PROPERTY_NAME);
	    if (subject == null) {
	    	subject = MAIL_SUBJECT_DEFAULT;
	    }
	    
	    // email body
		String messageText = generateBodyMail(request);
		
		logger.debug("[ MillemigliaKidsServlet] sendMail -- Email data: "
				+ "[From: {" + mailFrom + "}] - [To: {" + mailTo + "}] - "
				+ "[Subject: {" + subject + "}] - [Body: {" + messageText + "}]");
		
		// create email message object
		EmailMessage emailMessage = new EmailMessage();
		emailMessage.setFrom(mailFrom);
		emailMessage.setTo(mailTo);
		emailMessage.setSubject(subject);
		emailMessage.setMessageText(messageText);
		emailMessage.setIsBodyHtml(false);
		emailMessage.setDeliveryNotification(DeliveryNotificationOptionsEnum.NONE);
		emailMessage.setPriority(MailPriorityEnum.NORMAL);
		
		// create AgencySendMailRequest
		AgencySendMailRequest mailRequest = new AgencySendMailRequest();
		mailRequest.setTid(IDFactory.getTid());
		mailRequest.setSid(IDFactory.getSid(request));
		mailRequest.setEmailMessage(emailMessage);
		
		// get AgencySendMailResponse
		AgencySendMailResponse mailResponse =
				businessLoginService.sendMail(mailRequest);
		
		// return status
		return mailResponse.isSendMailSuccessful();

	}
	
	/*
	 * generateBodyMail
	 * 
	 */
	private String generateBodyMail(SlingHttpServletRequest request) {
		logger.debug("[ MillemigliaKidsServlet] generateBodyMail");
		
		String[][] data = new String[DIM_FORM][2];
		
		// creating Keys
		int k;
		for (k = 0; k < DIM_FORM; k++) {
			String name = "KeyField" + Integer.toString(k);
			data[k][0] = request.getParameter(name);
		}
		
		// getting Values
		String cod_millemiglia = request.getParameter("cod_millemiglia");
		String parent_name = request.getParameter("parent_name");
		String parent_surname = request.getParameter("parent_surname");
		String email = request.getParameter("email");
		String child_name = request.getParameter("child_name");
		String child_surname = request.getParameter("child_surname");
		String gender = request.getParameter("gender");
		String day = request.getParameter("day");
		String month = request.getParameter("month");
		String year = request.getParameter("year");
		String accettocond = request.getParameter("accettocond");
		
		// setting Values
		data[0][1] = (null != cod_millemiglia) ? cod_millemiglia : "";
		data[1][1] = (null != parent_name) ? parent_name : "";
		data[2][1] = (null != parent_surname) ? parent_surname : "";
		data[3][1] = (null != email) ? email : "";
		data[4][1] = (null != child_name) ? child_name : "";
		data[5][1] = (null != child_surname) ? child_surname : "";
		data[6][1] = (null != gender) ? gender.toUpperCase() : "";
		data[7][1] = ""; // (day of birth)
		data[8][1] = (null != accettocond) ? accettocond : "";
		
		// setting Day of Birth
		day = (null != day) ? day : ""; 
		month = (null != month) ? month : "";
		year = (null != year) ? year : "";
		if (!"".equals(day) && !"".equals(month) && !"".equals(year)) {
			try {
				List<String> months = new ArrayList<String>();
				months = I18nKeyCommon.getMonths();
				int numberMonth = Integer.parseInt(month);
				Locale locale = AlitaliaUtils.findResourceLocale(
						request.getResource());
				ResourceBundle resourceBundle = request.getResourceBundle(locale);
				I18n i18n = new I18n(resourceBundle);
				String keyMonth = months.get(numberMonth - 1);
				month = i18n.get(keyMonth);
				data[7][1] = day + "/" + numberMonth + "/" + year;
			} catch (NumberFormatException e) { 
				logger.error("Conversion date NumberFormatException ",e);
				throw e;
			}
		}
		
		//  creating email message and return it
		String emailText = "";
		for (k = 0; k < DIM_FORM; k++) {
			emailText = emailText + data[k][0] + KEYVALUE_SEPARATOR + data[k][1];
			if (k < DIM_FORM - 1) {
				emailText = emailText + ELEMENT_SEPARATOR;
			}
		}
		return emailText;
		
	}
	
	/*
	 * getComponentProperty
	 * 
	 */
	private String getComponentProperty(Page ancestor, String parsysName,
			String compName, String propName) {
		logger.debug("[ MillemigliaKidsServlet] getComponentProperty");
		
		Resource pageResource = ancestor.getContentResource();	
		String prop = null;
		try {
			prop = pageResource.getChild(parsysName).getChild(compName)
					.getValueMap().get(propName, String.class);
		} catch(Exception e) { }
		
		return prop;
	}

}