package com.alitalia.aem.consumer.checkin.model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.data.home.checkin.CheckinApisInfoData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.MmbAQQTypeEnum;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinPassenger;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinBoardingPass extends GenericCheckinModel {

	@Inject
	AlitaliaConfigurationHolder configurationHolder;

	@Inject
	CheckinSession session;

	@Self
	private SlingHttpServletRequest request;

	private boolean pdfEnabled = false;
	private boolean emailEnabled = false;
	private boolean smsEnabled = false;

	private boolean delayed = false;
	
	private boolean now = false;
	
	private List<PhonePrefixData> phonePrefixes;
	
	private List<CheckinPassenger> selectedPassengers = null;
	
	private String backUrl;
	
	private String defaultPrefix;

	@PostConstruct
	protected void initModel() {
		logger.debug("[CheckinBoardingPass] initModel");

		initBaseModel(request);

		// set boarding pass data
		session.setBoardingPass(ctx);

		// check print options
		pdfEnabled = ctx.boardingPassHasPDF;
		emailEnabled = ctx.boardingPassHasEmail;
		smsEnabled = ctx.boardingPassHasSMS;

		// show retire in airport
		delayed = ctx.boardingPassIsDelayed;
		
		// show download now or not
		now = pdfEnabled || emailEnabled || smsEnabled;
		
		// remove unauthorized users
		try {
			
			// retrieve already checked in passengers
			selectedPassengers = new ArrayList<CheckinPassenger>();
			
			
			boolean existsCleared = false;
			List<CheckinPassengerData> passengers =
					CheckinUtils.getCheckedInPassengers(ctx.passengers);
			for (CheckinPassengerData selectedPassenger : passengers) {
				if (selectedPassenger.getRouteId() == ctx.selectedRoute.getId()) {
					CheckinPassenger checkinPassenger = new CheckinPassenger();
					checkinPassenger.setName(selectedPassenger.getName());
					CheckinApisInfoData checkinApisInfoData = null;
					if (null != (checkinApisInfoData =
							selectedPassenger.getApisData())) {
						checkinPassenger.setMiddleName(
								checkinApisInfoData.getSecondName());
					}
					checkinPassenger.setLastName(selectedPassenger.getLastName());
					
					boolean cleared = MmbAQQTypeEnum.CLEARED.equals(
							selectedPassenger.getAuthorityPermission());
					existsCleared |= cleared;
					checkinPassenger.setCleared(cleared);
					selectedPassengers.add(checkinPassenger);
				}
			}
			
			pdfEnabled &= existsCleared;

			// retrieve phone prefixes
			RetrievePhonePrefixResponse response =
					session.retrievePhonePrefixes(ctx);
			phonePrefixes = response.getPhonePrefix();
			defaultPrefix = "";
			for (int i = 0; i < phonePrefixes.size(); i++) {
				PhonePrefixData phonePrefixData = phonePrefixes.get(i);
				if (phonePrefixData.getDescription().equalsIgnoreCase(marketCode)) {
					defaultPrefix = phonePrefixData.getPrefix();
				}
				phonePrefixData.setDescription(
						i18n.get("countryData." + phonePrefixData.getDescription()));
			}
			
			String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(
					request.getResource(), false);
			String flightsListUrl =
					configurationHolder.getCheckinFlightListPage();
			
			backUrl = baseUrl + flightsListUrl.substring(
					0, flightsListUrl.length() - 5) + "." + 
					CheckinConstants.PREPARE_ANCILLARY_SERVLET_SELECTOR;
			
		} catch(Exception e) {
			logger.error("[CheckinBoardingPass] initModel "
					+ "error: ", e.getMessage());
		}

	}

	public boolean isPdfEnabled() {
		return pdfEnabled;
	}

	public boolean isEmailEnabled() {
		return emailEnabled;
	}

	public boolean isSmsEnabled() {
		return smsEnabled;
	}

	public boolean isDelayed() {
		return delayed;
	}

	public List<CheckinPassenger> getSelectedPassengers() {
		return selectedPassengers;
	}

	public List<PhonePrefixData> getPhonePrefixes() {
		return phonePrefixes;
	}

	public boolean isNow() {
		return now;
	}

	public void setNow(boolean now) {
		this.now = now;
	}

	public String getBackUrl() {
		return backUrl;
	}

	public String getDefaultPrefix() {
		return defaultPrefix;
	}
	
}
