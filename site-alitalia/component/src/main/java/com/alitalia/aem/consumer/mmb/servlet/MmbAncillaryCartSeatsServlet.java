package com.alitalia.aem.consumer.mmb.servlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "mmbancillarycartseats" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class MmbAncillaryCartSeatsServlet extends GenericMmbFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile MmbSession mmbSession;	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		MmbSessionContext ctx = getMmbSessionContext(request);
		Map<Integer, String> seatsSelectionsByAncillaryId = new HashMap<>();
		String[] split;
		String[] selections = request.getParameterValues("seat-selection");
		if (selections != null) {
			for (String selection : selections) {
				logger.debug("Processing seat-selection {}", selection);
				split = selection.split("#", 2);
				String[] ancillaryIds = split[0].split("-");
				String ancillaryValue = split[1];
				for (String ancillaryIdString : ancillaryIds) {
					int ancillaryId = Integer.parseInt(ancillaryIdString);
					logger.debug("Evaluating ancillary id {} with value {}", ancillaryId, ancillaryValue);
					if (mmbSession.verifyIsComfortSeat(ctx, new Integer(ancillaryId), ancillaryValue)) {
						ancillaryValue += "_COMFORT";
					}
					seatsSelectionsByAncillaryId.put(ancillaryId, ancillaryValue);
				}
			}
		}
		
		mmbSession.addOrUpdateSeatsInCart(ctx, seatsSelectionsByAncillaryId);
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value("OK");
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}
