package com.alitalia.aem.consumer.checkinrest.render;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Destination_;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Origin_;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.day.cq.i18n.I18n;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SegmentRender {


	private String airline;

	private String flight;

	private Origin_ origin;

	private Destination_ destination;

	private String arrivalDate;

	private String departureDate;

	private String originalDepartureDate;

	private Boolean openCI;

	private String checkInGate;

	private List<Passenger> passengers = null;

	private List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra> passengersExtra = null;

	private String airlineName;

	private String terminal;

	private String duration;

	private String waiting;

    private String aircraft;

    private String airlineCode;

    private String flightStatus;

    private String flightDelay;
	
	private String arrivalTime;
	
	private String departureTime;

	public void setI18n(I18n i18n) {
		this.i18n = i18n;
	}

	private I18n i18n;

	public I18n getI18n() {
		return i18n;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getFlight() {
		return flight;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}

	public Origin_ getOrigin() {
		return origin;
	}

	public void setOrigin(Origin_ origin) {
		this.origin = origin;
	}

	public Destination_ getDestination() {
		return destination;
	}

	public void setDestination(Destination_ destination) {
		this.destination = destination;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getOriginalDepartureDate() {
		return originalDepartureDate;
	}

	public void setOriginalDepartureDate(String originalDepartureDate) {
		this.originalDepartureDate = originalDepartureDate;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public Boolean getOpenCI() {
		return openCI;
	}

	public void setOpenCI(Boolean openCI) {
		this.openCI = openCI;
	}

	public String getCheckInGate() {
		return checkInGate;
	}

	public void setCheckInGate(String checkInGate) {
		this.checkInGate = checkInGate;
	}

	public List<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}

	public List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra> getPassengersExtra() {
		return passengersExtra;
	}

	public void setPassengersExtra(List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra> passengersExtra) {
		this.passengersExtra = passengersExtra;
	}

	public void addPassengersExtra(com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra passengersExtra) {
		this.passengersExtra.add(passengersExtra);
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getWaiting() {
		return waiting;
	}

	public void setWaiting(String waiting) {
		this.waiting = waiting;
	}

	public String getAircraft() {
		return aircraft;
	}

	public void setAircraft(String aircraft) {
		this.aircraft = aircraft;
	}

	public String getFlightStatus() {
        return this.flightStatus;
    }

    public void setFlightStatus(String flightStatus) {
        this.flightStatus = flightStatus;
    }

    public String getFlightDelay() {
		if(this.flightDelay.equals("0")){
			return i18n.get("checkin.flighsList.statusOnTime.label");
		}else{
			return i18n.get("checkin.flighsList.statusDelay.label","",this.flightDelay);
		}
    }

    public void setFlightDelay(String flightDelay) {
        this.flightDelay = flightDelay;
    }

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

}
