package com.alitalia.aem.consumer.booking.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingDatiPrefooter extends BookingSessionGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private Boolean isSecureFlight;
	private Boolean isSecureFlightESTA;
	private Boolean isApis;
	private Boolean isApisCanada;
	
	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			
			initBaseModel(request);
			if (ctx == null) {
				//TODO: Gestire
				return;
			}
			
			if (ctx != null) {
				this.isSecureFlight = ctx.isSecureFlight;
				this.isSecureFlightESTA = ctx.isSecureFlightESTA;
				this.isApis = ctx.isApis;
			}
			
		} catch(Exception e) {
			logger.error("Unexpected error: ", e);
			throw (e);
		}

	}
	
	public Boolean getIsSecureFlight() {
		return isSecureFlight;
	}
	
	public Boolean getIsSecureFlightESTA() {
		return isSecureFlightESTA;
	}
	
	public Boolean getIsApis() {
		return isApis;
	}
	
	public Boolean getIsApisCanada() {
		return isApisCanada;
	}
	
}
