package com.alitalia.aem.consumer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.crypto.CryptoSupport;
import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.MMTelephoneData;
import com.alitalia.aem.common.data.home.enumerations.MMAddressTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMGenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMPhoneTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMTierCodeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMWorkPositionTypeEnum;
import com.alitalia.aem.common.utils.XsdConvertUtils;
import com.alitalia.aem.consumer.exception.NoUserLoggedInException;

@Component
@Service(value = AlitaliaCustomerProfileManager.class)
public class AlitaliaCustomerProfileManager {
	
	/**
	 * Logger.
	 */
    private static final Logger logger = LoggerFactory.getLogger(AlitaliaCustomerProfileManager.class);
	
    /**
	 * Reference to the service to AEM Granite Crypto API.
	 */
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_UNARY)
	private volatile CryptoSupport cryptoSupport;
    
	/**
	 * Given a customer profile data object, create a map of properties to be exposed in the user profile.
	 */
	public Map<String, String> getCustomerProfileProperties(MMCustomerProfileData profile) {
		Map<String, String> properties = new HashMap<String, String>();
		// prepare encrypted PIN
		String encryptedPin = encryptAndEncodeProperty(profile.getCustomerPinCode());
		// prepare MMTier code
		String mmTierCode = getTierCode(profile.getTierCode(), profile.getPointsEarnedTotalQualified());
		
		// set properties
		properties.put("customerNumber", profile.getCustomerNumber());
		properties.put("customerPinCode", encryptedPin);
		properties.put("customerName", profile.getCustomerName());
		properties.put("customerSurname", profile.getCustomerSurname());
		properties.put("customerNickName", profile.getCustomerNickName());
		properties.put("customerGender", profile.getGender() == null ? MMGenderTypeEnum.UNKNOWN.value() :
			profile.getGender().value());
		properties.put("customerBirthDate", profile.getBirthDate() == null ? "" :
			XsdConvertUtils.printDateTime(profile.getBirthDate()));
		properties.put("customerLanguage", profile.getLanguage());
		properties.put("customerContractType", profile.getContratcType());
		properties.put("milleMigliaTierCode", mmTierCode);
		properties.put("milleMigliaTotalMiles", profile.getPointsEarnedTotal() == null ? "" : 
			profile.getPointsEarnedTotal().toString());
		properties.put("milleMigliaTotalQualifiedMiles", profile.getPointsEarnedTotalQualified() == null ? "" : 
			profile.getPointsEarnedTotalQualified().toString());
		properties.put("milleMigliaRemainingMiles", profile.getPointsRemainingTotal() == null ? "" : 
			profile.getPointsRemainingTotal().toString());
		properties.put("milleMigliaTotalSpentMiles", profile.getPointsSpentTotal() == null ? "" : 
			profile.getPointsSpentTotal().toString());
		properties.put("milleMigliaQualifiedFlight", profile.getQualifyingSegment() == null ? "" : 
			profile.getQualifyingSegment().toString());
		properties.put("milleMigliaExpirationDate", profile.getTierDate() == null ? "" :
			XsdConvertUtils.printDateTime(profile.getTierDate()));
		properties.put("customerMailingType", profile.getMailingType());
		properties.put("customerProfiling", profile.getProfiling() == null ? "" : profile.getProfiling().toString());
		
		return properties;
	}
	
	/**
	 * Given a customer profile data object, updates the profile data of the currently
	 * logged-in user (associated to the provided ResourceResolver instance) by updating
	 * the associated JCR profile node properties.
	 * 
	 * <p>Optionally, if a value is provided for the <code>updatedCustomerPinCode</code>,
	 * the JCR-stored PIN value is also updated.</p>
	 */
	public void updateCustomerProfileProperties(MMCustomerProfileData profileData, 
			ResourceResolver resourceResolver, String updatedCustomerPinCode) throws RepositoryException {
		Session session = resourceResolver.adaptTo(Session.class);
		UserPropertiesManager userPropertiesManager = resourceResolver.adaptTo(UserPropertiesManager.class);
		UserProperties profileProperties = userPropertiesManager.getUserProperties(session.getUserID(), "profile");
		UserProperties privateProperties = userPropertiesManager.getUserProperties(session.getUserID(), "private");
		Map<String, String> updatedProfileProperties = getCustomerProfileProperties(profileData);
		Node profileNode = profileProperties.getNode();
		for (Map.Entry<String, String> profileValue : updatedProfileProperties.entrySet()) {
			if (profileValue.getValue() != null && 
					!profileValue.getValue().equals("") &&
					!profileValue.getValue().equals("UnKnown") && !profileValue.getValue().equals("Unknown") && //FIXME: si può migliorare questo controllo?
					!profileValue.getValue().equals(profileProperties.getProperty(profileValue.getKey()))) {
				profileNode.setProperty(profileValue.getKey(), profileValue.getValue());
			}
		}
		if (updatedCustomerPinCode != null && !"".equals(updatedCustomerPinCode)) {
			String encryptedCustomerPinCode = encryptAndEncodeProperty(updatedCustomerPinCode);
			Node privateNode = privateProperties.getNode();
			privateNode.setProperty("customerPinCode", encryptedCustomerPinCode);
		}
		session.save();
	}
	
	/**
	 * Static utility methods that read the data of the currently authenticated user, and
	 * create a MMCustomerProfileData object based on them.
	 * 
	 * <p>Note that only attributes that are synchronized in the user data authantication
	 * will be available in the returned profile object.</p>
	 * 
	 * @param request The request that will be used to identify the currently authenticated user.
	 * @return the customer profile data, or null if no user data are found.
	 */
	static public MMCustomerProfileData getAuthenticatedUserProfile(SlingHttpServletRequest request) {
		try {
			MMCustomerProfileData customerProfile = new MMCustomerProfileData();
			ResourceResolver resourceResolver = request.getResourceResolver();
			Session session = resourceResolver.adaptTo(Session.class);
			UserPropertiesManager userPropertiesManager = resourceResolver.adaptTo(UserPropertiesManager.class);
			UserProperties profileProperties = userPropertiesManager.getUserProperties(session.getUserID(), "profile");
			UserProperties privateProperties = userPropertiesManager.getUserProperties(session.getUserID(), "private");
			
			String customerNumber = profileProperties.getProperty("customerNumber");
			String encryptedCustomerPinCode = "";
			if(privateProperties != null){
				encryptedCustomerPinCode = privateProperties.getProperty("customerPinCode");
			}
			else{
				throw new NoUserLoggedInException("No user is logged in");
			}
			customerProfile.setCustomerNumber(customerNumber);
		    customerProfile.setCustomerPinCode(encryptedCustomerPinCode);
		    
		    customerProfile.setCustomerName(profileProperties.getProperty("customerName"));
		    customerProfile.setCustomerSurname(profileProperties.getProperty("customerSurname"));
		    customerProfile.setCustomerNickName(profileProperties.getProperty("customerNickName"));
		    customerProfile.setLanguage(profileProperties.getProperty("customerLanguage"));
		    customerProfile.setGender(MMGenderTypeEnum.fromValue(profileProperties.getProperty("customerGender")));
		    customerProfile.setContratcType(profileProperties.getProperty("customerContractType"));
		    customerProfile.setTierCode(profileProperties.getProperty("milleMigliaTierCode") != null ?
		    		MMTierCodeEnum.fromValue(profileProperties.getProperty("milleMigliaTierCode")) : null);
		    customerProfile.setPointsEarnedTotal(profileProperties.getProperty("milleMigliaTotalMiles") != null ?
		    		Long.parseLong(profileProperties.getProperty("milleMigliaTotalMiles")) : 0L);
		    customerProfile.setPointsEarnedTotalQualified(
		    		profileProperties.getProperty("milleMigliaTotalQualifiedMiles") != null ?
		    		Long.parseLong(profileProperties.getProperty("milleMigliaTotalQualifiedMiles")) : 0L);
		    customerProfile.setPointsRemainingTotal(
		    		profileProperties.getProperty("milleMigliaRemainingMiles") != null ?
		    		Long.parseLong(profileProperties.getProperty("milleMigliaRemainingMiles")) : 0L);
		    customerProfile.setPointsSpentTotal(
		    		profileProperties.getProperty("milleMigliaTotalSpentMiles") != null ?
		    		Long.parseLong(profileProperties.getProperty("milleMigliaTotalSpentMiles")) : 0L);
		    customerProfile.setQualifyingSegment(
		    		profileProperties.getProperty("milleMigliaQualifiedFlight") != null ?
		    		Long.parseLong(profileProperties.getProperty("milleMigliaQualifiedFlight")) : 0L);
		    customerProfile.setMailingType(profileProperties.getProperty("customerMailingType"));
		    customerProfile.setProfiling(Boolean.parseBoolean(profileProperties.getProperty("customerProfiling")));
		    customerProfile.setQualifiedFlights(profileProperties.getProperty("milleMigliaQualifiedFlight"));

		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		    simpleDateFormat.parse(profileProperties.getProperty("milleMigliaExpirationDate"));
		    customerProfile.setExpirationDate(simpleDateFormat.getCalendar());

		    simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		    simpleDateFormat.parse(profileProperties.getProperty("customerBirthDate"));
		    customerProfile.setBirthDate(simpleDateFormat.getCalendar());
		    simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		    simpleDateFormat.parse(profileProperties.getProperty("milleMigliaExpirationDate"));
		    customerProfile.setTierDate(simpleDateFormat.getCalendar());
		    return customerProfile;
		    
		} 
		catch(NoUserLoggedInException e){
			logger.debug(e.getMessage());
			return null;
		}catch(Exception e) {
			logger.error("Unexpected error in getAuthenticatedUser", e);
			return null;
		}
	}
	
	/**
	 * Utility method to obtain a mock MMCustomerProfileData instance.
	 */
	public MMCustomerProfileData getAuthenticatedUserMock() {
	
		MMCustomerProfileData user = new MMCustomerProfileData();
		
		String customerNumber = "00000000";
		String pinCode = encryptAndEncodeProperty("1234");
		
		user.setCustomerNumber(customerNumber);
		user.setCustomerPinCode(pinCode);
		
		user.setCustomerName("Martin");
		user.setCustomerSurname("Lewis");
		user.setCustomerNickName("MartinLewis81");
		user.setBirthDate(new GregorianCalendar(1981, 05, 11));
		user.setGender(MMGenderTypeEnum.MALE);
		user.setCustomerWorkPosition(MMWorkPositionTypeEnum.IMPIEGATO);
		user.setDefaultAddressType(MMAddressTypeEnum.BUSINESS);
		user.setDefaultAddressStreetFreeText("Via Nazionale");
		user.setDefaultAddressPostalCode("00100");
		user.setDefaultAddressMunicipalityName("Roma");
		user.setDefaultAddressCountry("IT");
		user.setDefaultAddressStateCode("RM");
		user.setEmail("martin.lewis31@example.com");
		
		user.setTierCode(MMTierCodeEnum.BASIC);
		user.setTierDate(new GregorianCalendar(2010, 01, 15));
		
		user.setPointsEarnedPartial(50L);
		user.setPointsEarnedTotal(100L);
		user.setPointsEarnedTotalQualified(200L);
		user.setPointsRemainingPartial(20L);
		user.setPointsRemainingTotal(100L);
		user.setPointsSpentPartial(35L);
		user.setPointsSpentTotal(70L);
		user.setQualifyingSegment(255L);
		
		List<MMTelephoneData> telephoneDataList = new ArrayList<MMTelephoneData>();
		MMTelephoneData telephoneData = new MMTelephoneData();
		telephoneData.setCountryNumber("1");
		telephoneData.setNumber("(899)624-9353");
		telephoneData.setTelephoneType(MMPhoneTypeEnum.BUSINESS_PHONE);
		telephoneDataList.add(telephoneData);
		user.setTelephones(telephoneDataList);
		
		return user;
	}
	
	/**
	 * Given a String, encrypt it using the AEM Granite Crypto API and
	 * encode it as a Base64 String.
	 */
	public String encryptAndEncodeProperty(String clearTextValue) {
		if (clearTextValue == null) {
			return "";
		}
		String encryptedAndEncodedValue;
		try {
			byte[] clearTextValueBytes = clearTextValue.getBytes("UTF-8");
			byte[] encryptedValueBytes = cryptoSupport.encrypt(clearTextValueBytes);
			encryptedAndEncodedValue = Base64.getEncoder().encodeToString(encryptedValueBytes);
		} catch (Exception e) {
			logger.error("Error performing property encryption", e);
			throw new RuntimeException("Error performing property encryption.", e);
		}
		return encryptedAndEncodedValue;
	}
	
	/**
	 * Decode and decrypt a value previously encrypted with the
	 * <code>encryptAndEncodeProperty>/code> method.
	 */
	public String decodeAndDecryptProperty(String encryptedValue) {
		if (encryptedValue == null) {
			return "";
		}
		if (cryptoSupport == null) {
			
		}
	    String decryptedValue;
	    try {
	    	byte[] encryptedValueBytes = Base64.getDecoder().decode(encryptedValue);
	    	byte[] decryptedValueBytes = cryptoSupport.decrypt(encryptedValueBytes);
	    	decryptedValue = new String(decryptedValueBytes, "UTF-8");
	    } catch (Exception e) {
	    	logger.error("Error performing property decryption.", e);
			throw new RuntimeException("Error performing property decryption.", e);
	    }
	    return decryptedValue;
	}
	
	private String getTierCode(MMTierCodeEnum mmTier, Long qualifiedMiles) {
		
		final Long checkpointClubUlisse = 20000L;
		final Long checkpointClubFrecciaAlata = 50000L;
		final Long checkpointClubFrecciaAlataPlus = 80000L;
		
		if(mmTier != null){
			logger.debug("QualifiedMiles Input: " + qualifiedMiles);
		} else{
			logger.warn("Tier Code NULL");
		}
		
		if (mmTier == null && qualifiedMiles == null) {
			return "";
		}
		
		if (MMTierCodeEnum.BASIC.value().equals(mmTier.value())) {
			return MMTierCodeEnum.BASIC.value();
		} else if (MMTierCodeEnum.ULISSE.value().equals(mmTier.value())) {
			return MMTierCodeEnum.ULISSE.value();
		} else if (MMTierCodeEnum.FRECCIA_ALATA.value().equals(mmTier.value())) {
			return MMTierCodeEnum.FRECCIA_ALATA.value();
		} else if (MMTierCodeEnum.PLUS.value().equals(mmTier.value())) {
			return MMTierCodeEnum.PLUS.value();
		} else{
			if(qualifiedMiles < checkpointClubUlisse){
				return MMTierCodeEnum.BASIC.value();
			} else if(qualifiedMiles >= checkpointClubUlisse && qualifiedMiles < checkpointClubFrecciaAlata){
				return MMTierCodeEnum.ULISSE.value();
			} else if((qualifiedMiles >= checkpointClubFrecciaAlata && qualifiedMiles < checkpointClubFrecciaAlataPlus)){
				return MMTierCodeEnum.FRECCIA_ALATA.value();
			} else if(qualifiedMiles >= checkpointClubFrecciaAlataPlus){
				return MMTierCodeEnum.PLUS.value();
			}
		}
		return null;
	}
	
}
