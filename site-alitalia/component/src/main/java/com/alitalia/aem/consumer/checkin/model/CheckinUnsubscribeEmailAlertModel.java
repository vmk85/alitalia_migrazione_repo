package com.alitalia.aem.consumer.checkin.model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinUnsubscribeEmailAlertModel extends GenericBaseModel{
	@Inject
	AlitaliaConfigurationHolder configuration;
	private final String SELECTOR_SERVLET=".checkinunsubscribe.json";
	private String redirectHP;
	private String errorMessage;
	private String redirectUrlUnsubServlet;
	private String successMessage;
	@Self
	private SlingHttpServletRequest request;
	
	@PostConstruct
	protected void initModel() {
		super.initBaseModel(request);		
		redirectHP=request.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), true)
			+ configuration.getHomePageWithExtension());
		if (request.getParameter("success")!=null &&request.getParameter("success").equals("false")){
			errorMessage=i18n.get("checkin.de.registration.error.label");
			
		}else if(request.getParameter("unsubscribeSuccess")!=null &&request.getParameter("unsubscribeSuccess").equals("true")){
			successMessage=i18n.get("checkin.de.registration.success");
		}
		redirectUrlUnsubServlet=request.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)+configuration.getCheckinDeregistrationPage(false))+SELECTOR_SERVLET;
		
		
	}
	
	public String getRedirectHP(){
		return redirectHP;
	}
	public String getErrorMessage(){
		return errorMessage;
	}
	public String getRedirectUrlUnsubServlet(){
		 return redirectUrlUnsubServlet;
	}
	public String getSuccessMessage(){
		 return successMessage;
	}
}
