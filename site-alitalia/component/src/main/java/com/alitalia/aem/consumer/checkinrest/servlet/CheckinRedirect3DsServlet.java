package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkinredirect3ds"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinRedirect3DsServlet extends SlingAllMethodsServlet {

	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		
		logger.info("Executing Checkinredirect3DsServlet.");
		
		String baseUrl = request.getResource().getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
		
		String paymentFormPage = baseUrl + configuration.getCheckinPaymentRedirectPage();

		//paymentFormPage = request.getResourceResolver().map(paymentFormPage);
		
		String genericErrorPage = ""; //TODO quale pagina?
		
		TidyJSONWriter json;
		
		try {
			//TODO get Session
			//System.out.println("REDIRECT");
			//response.sendRedirect(paymentFormPage);
			json = new TidyJSONWriter(response.getWriter());
			response.setContentType("application/json");
			
			json.object();
			json.key("isError").value(false);
			json.key("successPage").value(paymentFormPage);
			json.endObject();
		}
		catch(Exception e) {
			logger.error("Unexpected error: ", e);
			response.sendRedirect(genericErrorPage);
		}
		
	}
	
	
	
}
