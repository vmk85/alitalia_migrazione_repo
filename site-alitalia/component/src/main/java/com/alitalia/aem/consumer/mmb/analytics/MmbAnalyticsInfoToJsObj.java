package com.alitalia.aem.consumer.mmb.analytics;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.List;

import com.alitalia.aem.consumer.booking.analytics.FlightSegmentInfo;
import com.alitalia.aem.consumer.booking.analytics.filler.ToJsObj;


public class MmbAnalyticsInfoToJsObj extends ToJsObj{
	
	private static final String HYPHEN_FLIGHT_DATE_FORMAT = "yyyy-MM-dd";
	private static final String HYPHEN_EU_FLIGHT_DATE_FORMAT = "dd-MM-yy";
	private static final String EU_FLIGHT_DATE_FORMAT = "dd/MM/yyyy";
	private static final String EN_FLIGHT_DATE_FORMAT = "MM/dd/yyyy";
	private static final String TIME_FORMAT_HH_MM_SS = "kk:mm:ss";
	
	private DecimalFormat format;
	private SimpleDateFormat dateFormat_eu;
	private SimpleDateFormat dateFormat_en;
	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateFormat_eu_hyphen;
	private SimpleDateFormat timeFormat;
	
	

	public MmbAnalyticsInfoToJsObj() {
		initFormats();
	}
	
	protected void initFormats() {
		format = new DecimalFormat("#0.00");
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setCurrencySymbol(""); 
		format.setDecimalFormatSymbols(otherSymbols);
		format.setGroupingUsed(false);
		format.setPositivePrefix("");
		format.setNegativePrefix("");
		
		dateFormat_eu = new SimpleDateFormat(EU_FLIGHT_DATE_FORMAT);
		dateFormat_eu.setLenient(false);
		
		dateFormat_en = new SimpleDateFormat(EN_FLIGHT_DATE_FORMAT);
		dateFormat_en.setLenient(false);
		
		dateFormat = new SimpleDateFormat(HYPHEN_FLIGHT_DATE_FORMAT);
		dateFormat.setLenient(false);
		dateFormat_eu_hyphen = new SimpleDateFormat(HYPHEN_EU_FLIGHT_DATE_FORMAT);
		dateFormat_eu_hyphen.setLenient(false);
		
		timeFormat = new SimpleDateFormat(TIME_FORMAT_HH_MM_SS);
		timeFormat.setLenient(false);
	}
	
	public String toJSObj(MmbAnalyticsInfo info, boolean trailingSemicolon) {
		String js = "{";
		
		/*Other info*/
		switch(info.step){
		case 1:
			js = addJsObjElem(js, "AncillaryTypeProposed", info.ancillaryTypeProposed, true);
			js = addJsObjElem(js, "AncillaryPage", info.ancillaryPage, true);
			js = addJsArrayObjElem(js, "Ancillary", convertAncillaryList(info.ancillaryList), false);
			break;
		case 2:
			js = addJsObjElem(js, "AncillaryTypeProposed", info.ancillaryTypeProposed, true);
			js = addJsObjElem(js, "AncillaryPage", info.ancillaryPage, true);
			js = addJsArrayObjElem(js, "Ancillary", convertAncillaryList(info.ancillaryList), true);
			js = addJsObjElem(js, "AncillaryRoute", info.route, true);
			js = addJsObjElem(js, "AncillaryTravelType", info.travelType, false);
			break;
		case 3:
			js = addJsArrayObjElem(js, "Ancillary", convertAncillaryList(info.ancillaryList), false);
			break;
		case 4:
			js = addJsArrayObjElem(js, "Ancillary", convertAncillaryList(info.ancillaryList), true);
			js = addJsObjElem(js, "MMBPaymentType", info.paymentType, true);
			js = addJsObjElem(js, "MMBCCType", info.CCType, true);
			js = addJsObjElem(js, "MMBInvoice", info.invoiceRequested, true);
			js = addJsObjElem(js, "PNR", info.PNR, true);
			js = addJsObjElem(js, "TktNumber", info.tktNumber, true);
			js = addJsObjElem(js, "MMBDepartureDate", formatIfNotNull(info.depDate, dateFormat), true);
			js = addJsObjElem(js, "MMBReturnDate", formatIfNotNull(info.retDate, dateFormat), false);
			break;
		}
		
		js += (trailingSemicolon) ? "};" : "}";
		return js;
	}

	private String convertAncillaryList(List<AncillaryInfo> ancillaryList) {
		String js = "[";
		for(AncillaryInfo ancillary : ancillaryList){
			js += "{";
			if(ancillary.ancName != null){
				js = addJsObjElem(js, "Name", ancillary.ancName, true);
			}
			if(ancillary.ancQuantity != null){
				js = addJsObjElem(js, "Quantity", ancillary.ancQuantity, true);
			}
			if(ancillary.ancRevenue != null){
				js = addJsObjElem(js, "Revenue", formatIfNotNull(ancillary.ancRevenue, format), true);
			}
			/*Remove last ","*/
			if(js.length() > 1){
				js = js.substring(0,js.length()-1);
			}
			js += "},";
		}
		/*Remove last ","*/
		if(js.length() > 1){
			js = js.substring(0,js.length()-1);
		}
		js += "]";
		return js;
	}

}
