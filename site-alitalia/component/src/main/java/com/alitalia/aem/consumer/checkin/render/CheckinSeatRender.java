package com.alitalia.aem.consumer.checkin.render;

import com.alitalia.aem.common.data.home.checkin.CheckinSeatData;
import com.alitalia.aem.common.data.home.enumerations.CheckinAvailabilitySeatEnum;
import com.alitalia.aem.common.data.home.enumerations.CheckinTypeSeatEnum;

public class CheckinSeatRender {
	
	private boolean comfortSeat;
	private boolean occupied;
	private boolean available;
	private boolean aisle;
	private boolean nearEmergencyExit;
	private String column;
	private String cssClass;
	private String row;
	
	public CheckinSeatRender(CheckinSeatData seatData, String row, String column, boolean nearEmergencyExit) {
		this(seatData, row, column, nearEmergencyExit, false);
	}
	
	public CheckinSeatRender(CheckinSeatData seatData, String row, String column, boolean nearEmergencyExit, boolean showAsFree) {
		
		this.comfortSeat = Boolean.TRUE.equals(seatData.getIsComfortSeat());
		this.column = column;
		this.cssClass = "";
		this.nearEmergencyExit = nearEmergencyExit;
		
		// seatData.getAvailability(); // Free/Occupied/NotApplicable
		// seatData.getIsComfortSeat(); // true/false
		// seatData.getIsUpgradeSeat(); // true/false
		// seatData.getType(); // Normal/NotExist/Aisle
		
		this.row = row;
		
		if (CheckinTypeSeatEnum.AISLE.equals(seatData.getType())) {
			this.cssClass = "number";
			this.aisle = true;
		} else if (CheckinTypeSeatEnum.NOT_EXIST.equals(seatData.getType())) {
			this.cssClass = "";
		} else if (CheckinTypeSeatEnum.NORMAL.equals(seatData.getType())) {
			if (showAsFree || CheckinAvailabilitySeatEnum.FREE.equals(seatData.getAvailability())) {
				this.cssClass = this.comfortSeat ? "extraComfort" : "available";
				this.available = true;
			} else {
				this.cssClass = "occupied";
				this.occupied = true;
			}
		}
	}

	public boolean isComfortSeat() {
		return comfortSeat;
	}

	public void setComfortSeat(boolean comfortSeat) {
		this.comfortSeat = comfortSeat;
	}

	public boolean isOccupied() {
		return occupied;
	}

	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public boolean isAisle() {
		return aisle;
	}

	public void setAisle(boolean aisle) {
		this.aisle = aisle;
	}

	public boolean isNearEmergencyExit() {
		return nearEmergencyExit;
	}

	public void setNearEmergencyExit(boolean nearEmergencyExit) {
		this.nearEmergencyExit = nearEmergencyExit;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}
}
