/** Interfaccia di mappatura per modelli relativi alle chiamate CRM */

package com.alitalia.aem.consumer.mapstruct;

import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.data.home.MACustomerData;
import com.alitalia.aem.common.messages.crmdatarest.GetCrmDataInfoResponse;
import com.alitalia.aem.common.messages.crmdatarest.SetCrmDataInfoRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CrmMapper {

    CrmMapper   MAPPER = Mappers.getMapper( CrmMapper.class );

    @Mappings({
            @Mapping(source = "infoCliente", target = "infoCliente"),
            @Mapping(source = "canaleComunicazionePreferito", target = "canaleComunicazionePreferito"),
            @Mapping(source = "listaConsensi", target = "listaConsensi"),
            @Mapping(source = "email", target = "email"),
            @Mapping(source = "unsubscribeNewsletter", target = "unsubscribeNewsletter"),
            @Mapping(source = "frequenzaComunicazioni", target = "frequenzaComunicazioni"),
            @Mapping(source = "recapitiTelefonici", target = "recapitiTelefonici"),
            @Mapping(source = "elencoDocumenti", target = "elencoDocumenti"),
            @Mapping(source = "preferenzeViaggio", target = "preferenzeViaggio"),
            @Mapping(source = "preferenzePersonali", target = "preferenzePersonali"),
            @Mapping(source = "idMilleMiglia", target = "idMilleMiglia"),
            @Mapping(source = "idMyAlitalia", target = "idMyAlitalia"),
            @Mapping(source = "canale", target = "canale"),
            @Mapping(source = "conversationID", target = "conversationID"),
    })
    SetCrmDataInfoRequest setRequestFromGetResponse(GetCrmDataInfoResponse getCrmDataInfoResponse);

    @Mappings({
            @Mapping(source = "UID", target = "idMyAlitalia"),
            @Mapping(source = "profile.email", target = "email"),
    })
    SetCrmDataInfoRequest setRequestFromMaCustomer(MACustomer maCustomer);

    @Mappings({
            @Mapping(source = "data.address1_Address", target = "infoCliente.indirizzo"),
            @Mapping(source = "data.address1_CompanyName", target = "infoCliente.nomeAzienda"),
            @Mapping(source = "data.address1_Type", target = "infoCliente.tipoIndirizzo"),
            @Mapping(source = "data.address1_Zip", target = "infoCliente.cap"),
            @Mapping(source = "data.address1_CountryCode", target = "infoCliente.nazione"),
            @Mapping(source = "data.address1_StateCode", target = "infoCliente.stato"),
            @Mapping(source = "data.address1_City", target = "infoCliente.citta"),
            @Mapping(source = "data.phone1AreaCode", target = "recapitiTelefonici.prefissoArea"),
            @Mapping(source = "data.secondoName", target = "infoCliente.secondoNome"),
            @Mapping(source = "profile.firstName", target = "infoCliente.nome"),
            @Mapping(source = "profile.gender", target = "infoCliente.sesso"),
            @Mapping(source = "profile.lastName", target = "infoCliente.cognome"),
            @Mapping(source = "profile.professionalHeadline", target = "infoCliente.professione"),
            @Mapping(source = "data.mobilePhone", target = "recapitiTelefonici.cellulare"),
            @Mapping(source = "data.phone1CountryCode", target = "recapitiTelefonici.prefissioNazionale"),
            @Mapping(source = "data.phone1Number", target = "recapitiTelefonici.numero"),
            @Mapping(source = "data.phone1Type", target = "recapitiTelefonici.tipo"),
    })
    GetCrmDataInfoResponse getRequestFromMaCustomer(MACustomer maCustomer);


    @Mappings({})
    MACustomerData mapMaCustomerGigyaResponse(MACustomerData maCustomer);
}
