package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.enumerations.MailingListActionTypesEnum;
import com.alitalia.aem.common.messages.home.MailinglistRegistrationRequest;
import com.alitalia.aem.common.messages.home.MailinglistRegistrationResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.NewsletterUnsubData;
import com.alitalia.aem.consumer.servlet.specialpages.exceptions.NewsletterUserNotRegisteredException;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.i18n.I18n;

@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "newsletterunsubsubmit" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")

public class NewsletterUnsubServlet extends GenericFormValidatorServlet {
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[NewsletterUnsubServlet] validateForm");

		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			return validator.validate();

		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Error during parameters validation", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
		logger.debug("[NewsletterUnsubServlet] performSubmit");
	
		MMCustomerProfileData profile = new MMCustomerProfileData();
		profile.setEmail(request.getParameter("email"));
				
		MailinglistRegistrationRequest newsletterRequest = new MailinglistRegistrationRequest();
		newsletterRequest.setTid(IDFactory.getTid());
		newsletterRequest.setSid(IDFactory.getSid(request));
				
	    newsletterRequest.setAction(MailingListActionTypesEnum.DELETE);
	    newsletterRequest.setCustomerProfile(profile);
				
		//TODO Controllare la risposta del server e capire se l'operazione è andata a buon fine
		//Lanciare eccezione NewsletterUserNotRegisteredException nel caso l'utente non sia registrato
	    MailinglistRegistrationResponse mailingRegistrationResponse = consumerLoginDelegate.registerToMailinglist(newsletterRequest);
		boolean result = mailingRegistrationResponse.isRegistrationIsSuccessful();
		
		if (!result) {
//		    se disiscriviamo un'email gia' disiscritta: da BL riceviamo <ErrorCode>ErrorMailingListRegistration</ErrorCode> <Message>30; Mailing List read error : NF; </Message>
            if (mailingRegistrationResponse.getErrorMessage().contains("Mailing List read error")) {
		        throw new NewsletterUserNotRegisteredException();
            } else {
                throw new IOException("Service Error");
            }
		}
		
		goBackSuccessfully(request, response);

	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[NewsletterUnsubServlet] saveDataIntoSession");

		// create NewsletterSubscribeServlet object and save it into session
		NewsletterUnsubData newsletterUnsubData = new NewsletterUnsubData();
		newsletterUnsubData.setMail(request.getParameter("email"));
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);

		if(e instanceof NewsletterUserNotRegisteredException){
			newsletterUnsubData.setError(i18n.get(I18nKeySpecialPage.CUSTOMER_NOT_EXISTS));
		}
		else{
			newsletterUnsubData.setError(i18n.get(I18nKeySpecialPage.ERROR_SERVICE));
		}
				
	    request.getSession().setAttribute(NewsletterUnsubData.NAME, newsletterUnsubData);

	}

	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {

		// get parameters from request
		String email = request.getParameter("email");

		// add validate conditions
		validator.addDirectConditionMessagePattern("email", email,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("email", email,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID, "isEmail");

		return validator;

	}

}
