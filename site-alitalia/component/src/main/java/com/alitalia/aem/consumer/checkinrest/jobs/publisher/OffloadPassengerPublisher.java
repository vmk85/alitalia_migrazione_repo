package com.alitalia.aem.consumer.checkinrest.jobs.publisher;

import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkinrest.jobs.CheckinJobConstants;
import com.alitalia.aem.consumer.checkinrest.jobs.consumer.OffloadPassengerJob;
import com.google.gson.Gson;

import java.util.*;

/**
 * Created by ggadaleta on 06/02/2018.
 */
public class OffloadPassengerPublisher extends BaseJobPublisher {

    public OffloadPassengerPublisher() {
        this.setJobTopic(CheckinJobConstants.TOPIC_PASSENGER_OFFLOAD);
    }

    @Override
    public Map<String, Object> createProperties(CheckinSessionContext ctx) {

        Gson gson = new Gson();
        final Map<String, Object> props = new HashMap<>();

        //set sessionId
        props.put(OffloadPassengerJob.PROPERTY_CHECKIN_SESSION_ID, this.getSessionID());

        props.putAll(this.preparePrecheckinOffloadRequests(OffloadPassengerJob.PROPERTY_CHECKIN_OFFLOAD, ctx));

        return props;
    }
}
