package com.alitalia.aem.consumer.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Cookies management utility methods.
 */
public class CookieUtils {
	
	/**
	 * Utility method to read a cookie value.
	 *
	 * @param request The request to work with.
	 * @param cookieName The name of the cookie.
	 * @return The cookie value, or null if not found.
	 */
	public static String getCookieValue(HttpServletRequest request, 
			String cookieName) {
		
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookieName.equals(cookie.getName())) {
					return cookie.getValue();
				}
			}
		}
		return null;

	}
	
	/**
	 * Utility method to clear a cookie.
	 *
	 * @param request The request to work with.
	 * @param response The response to work with.
	 * @param cookieName The name of the cookie.
	 */
	public static void clearCookie(HttpServletRequest request,
			HttpServletResponse response, String cookieName) {
		
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookieName.equals(cookie.getName())) {
					cookie.setMaxAge(0);
					cookie.setValue(null);
	                response.addCookie(cookie);
	                return;
				}
			}
		}

	}
	
	/**
	 * Utility method to set a persistent cookie value.
	 *
	 * @param response The response to work with.
	 * @param cookieName The name of the cookie.
	 * @param domain The domain of the cookie to set.
	 * @param path The path of the cookie to set.
	 * @param maxAge The max age of the cookie.
	 * @param value The value to set.
	 * @param secure If true, set the cookie as secure (HTTPS-only).
	 * @param setExpires If true, the maxAge will be also set with expires (IE compatibility).
	 */
	public static void setCookieValue(HttpServletResponse response,
			String cookieName, String domain, String path, Integer maxAge,
			String value, boolean secure, boolean setExpires) {
		
		if (maxAge != null && setExpires) {
			// use alternative, manual way
			setCookieValueWithExpires(response, cookieName, domain, path,
					maxAge.intValue(), value, secure);
		} else {
			
			Cookie cookie = new Cookie(cookieName, value);
			if (domain != null && !"".equals(domain)) {
				cookie.setDomain(domain);
			}
			if (path != null) {
				cookie.setPath(path);
			}
			if (maxAge != null) {
				cookie.setMaxAge(maxAge);
			}
			cookie.setSecure(secure);
			response.addCookie(cookie);
			
		}
	}
	
	/**
	 * Manually set a persistent cookie setting the header response, also specifying the expires. 
	 * 
	 * @param response The response to work with.
	 * @param cookieName The name of the cookie.
	 * @param domain The domain of the cookie to set.
	 * @param path The path of the cookie to set.
	 * @param maxAge The max age of the cookie.
	 * @param value The value to set.
	 * @param secure If true, set the cookie as secure (HTTPS-only).
	 */
	private static void setCookieValueWithExpires(
			HttpServletResponse response, String cookieName, String domain,
			String path, int maxAge, String value, boolean secure) {
		
	    StringBuilder cookie =
	    		new StringBuilder(cookieName + "=" + value + "; ");
	    DateFormat df =
	    		new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss 'GMT'", Locale.US);
	    
	    Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.SECOND, maxAge);
	    cookie.append("Expires=" + df.format(cal.getTime()) + "; ");
	    cookie.append("Domain=; ");
	    cookie.append("Version=0; ");
	    cookie.append("Path=/; ");
	    cookie.append("Max-Age=" + maxAge + "; ");
	    if (secure) {
	    	cookie.append("Secure; ");
	    }
	    
	    response.setHeader("Set-Cookie", cookie.toString());
	}
	
}
