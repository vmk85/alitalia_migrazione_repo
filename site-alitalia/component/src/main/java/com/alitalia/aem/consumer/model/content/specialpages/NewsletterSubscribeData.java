package com.alitalia.aem.consumer.model.content.specialpages;

public class NewsletterSubscribeData {
public static final String NAME = "NewsletterSubscribeData";
	
	private String mail;
	private String error;
	private String newslettersubCond;
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getNewslettersubCond() {
		return newslettersubCond;
	}
	public void setNewslettersubCond(String newslettersubCond) {
		this.newslettersubCond = newslettersubCond;
	}
	
	@Override
	public String toString() {
		return "NewsletterSubscribeData [ mail=" + mail + " error=" + error +  " newslettersubCond="+newslettersubCond+ " ]";
	}

}
