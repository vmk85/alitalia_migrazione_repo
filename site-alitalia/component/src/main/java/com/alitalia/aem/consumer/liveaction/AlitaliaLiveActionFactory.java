package com.alitalia.aem.consumer.liveaction;

import java.util.Collections;
import java.util.Iterator;

import org.osgi.service.component.ComponentContext;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.apache.sling.commons.json.io.JSONWriter;
import org.apache.sling.commons.json.JSONException;

import org.apache.sling.commons.osgi.PropertiesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.PropertyIterator;
import javax.jcr.Session;
import javax.jcr.PropertyType;
import javax.jcr.Value;

import com.day.cq.wcm.msm.api.ActionConfig;
import com.day.cq.wcm.msm.api.LiveAction;
import com.day.cq.wcm.msm.api.LiveActionFactory;
import com.day.cq.wcm.msm.api.LiveRelationship;
import com.day.cq.wcm.api.WCMException;

@Component(metatype = false, immediate = true, name = "Alitalia Live Action Factory")
@Service
public class AlitaliaLiveActionFactory implements LiveActionFactory<LiveAction> {

    public AlitaliaLiveActionFactory() {}

    @org.apache.felix.scr.annotations.Property(value="alitaliaReferencesUpdateLiveAction")
    static final String actionName = LiveActionFactory.LIVE_ACTION_NAME;

    private String liveActionName;

    public LiveAction createAction(Resource configResource) {
        ValueMap config;

        /** Adapt the config resource to a ValueMap **/
        if (configResource == null || configResource.adaptTo(ValueMap.class) == null) {
            config = new ValueMapDecorator(Collections.<String, Object>emptyMap());
        } else {
            config = configResource.adaptTo(ValueMap.class);
        }

        return new AlitaliaReferencesUpdateLiveAction(liveActionName, config);
    }

    @Activate
    public void activate(ComponentContext ctx) {
        this.liveActionName = PropertiesUtil.toString(ctx.getProperties().get(actionName), "alitaliaReferencesUpdateLiveAction");
    }

    public String createsAction() {
        return actionName;
    }

    /********* Custom Live Action *********/
    private static class AlitaliaReferencesUpdateLiveAction implements LiveAction {

        private static final Logger logger = LoggerFactory.getLogger(AlitaliaReferencesUpdateLiveAction.class);
        private static final int ANCESTOR_LEVEL = 3;
        private static final String LIVE_SYNC_NODE_NAME = "cq:LiveSyncConfig";
        private static final String JCR_CONTENT_NODE_NAME = "jcr:content";
        private boolean propertyChanged;

        private String name;
        private ValueMap config;
        private String sourceBasePath;
        private String targetBasePath;

        public AlitaliaReferencesUpdateLiveAction(String name, ValueMap config) {
            this.name = name;
            this.config = config;
        }

        /** Execute the references update action **/
        public void execute(Resource source, Resource target, LiveRelationship liveRelationship, boolean autoSave, boolean isResetRollout) {

            if(source == null || target == null) {
                return;
            } else {
                try {
                    sourceBasePath = source.adaptTo(Node.class).getAncestor(ANCESTOR_LEVEL).getPath();
                    targetBasePath = target.adaptTo(Node.class).getAncestor(ANCESTOR_LEVEL).getPath();
                } catch(Exception e) {
                    logger.error("Unable to define string to be replaced\n" + e);
                }
            }

            logger.debug("\n\n *** Flags [" + autoSave + "] -- [" + isResetRollout + "]\n\n");
            logger.debug("\n\n Source base path: " + sourceBasePath +
                    "\n Target base path: " + targetBasePath);

            if(target.getName().equalsIgnoreCase(JCR_CONTENT_NODE_NAME)) {

                // Check also the current node - otherwise it is skipped.
                checkProperties(target);
                parseTree(target);
            }

            if(propertyChanged) {
                try {
                    final Session session = source.adaptTo(Session.class);
                    session.save();
                } catch(Exception e) {
                    logger.error("Unable to save session\n" + e);
                }
            }
        }

        private void parseTree(Resource resource) {
            propertyChanged = false;
            if(resource.hasChildren()) {
                final Iterator<Resource> resourceItrator = resource.listChildren();
                while(resourceItrator.hasNext()) {
                    final Resource currentResource = resourceItrator.next();
                    // Skip the live sync config node.
                    if(!currentResource.getName().equalsIgnoreCase(LIVE_SYNC_NODE_NAME)) {
                        checkProperties(currentResource);
                        parseTree(currentResource);
                    }
                }
            }
        }

        private void checkProperties(Resource resource) {

            try {
                final Node currentNode = resource.adaptTo(Node.class);
                final PropertyIterator properties = currentNode.getProperties();

                while(properties.hasNext()) {
                    final javax.jcr.Property property =  properties.nextProperty();
                    final String propertyName = property.getName();

                    if(property.getType() == PropertyType.STRING && !property.isMultiple()) {
                        logger.debug("\n\nChecking simple String property " + property.getName());

                        final String propertyValue = property.getString();
                        if(propertyValue.startsWith(sourceBasePath)) {
                            final String newPropertyValue = propertyValue.replaceAll(sourceBasePath, targetBasePath);
                            currentNode.setProperty(propertyName, newPropertyValue);

                            logger.debug("\n\n Node " + currentNode.getPath() +
                                    "\n Property " + propertyName +
                                    "\n Old Value " + propertyValue +
                                    "\n New value " + newPropertyValue);
                            propertyChanged = true;
                        }

                    } else if(property.getType() == PropertyType.STRING && property.isMultiple()) {
                        logger.debug("\n\nChecking String[] property " + property.getName());
                        final Value[] values = property.getValues();
                        final String[] newValues = new String[values.length];

                        for(int idx = 0; idx < values.length; idx++) {
                            final String oldValue = values[idx].getString();
                            if(oldValue.startsWith(sourceBasePath)) {
                                newValues[idx] = oldValue.replaceAll(sourceBasePath, targetBasePath);
                                logger.debug("\n\n Node " + currentNode.getPath() +
                                        "\n Property " + propertyName + "[" + idx + "]" +
                                        "\n Old Value " + oldValue +
                                        "\n New value " + newValues[idx]);

                                propertyChanged = true;
                            } else {
                                newValues[idx] = oldValue;
                            }
                            if(propertyChanged) {
                                currentNode.setProperty(propertyName, newValues);
                            }
                        }

                    }

                }
            } catch(Exception e) {
                logger.error("Unable to check properties for node " + resource.getPath() + "\n" + e);
            }
        }

        public String getName() {
            return name;
        }

        /** DEPRECATED METHODS **/
        @Deprecated
        public void execute(Resource source, Resource target) {
        }

        @Deprecated
        public void execute(ResourceResolver arg0, LiveRelationship arg1,
                            ActionConfig arg2, boolean arg3) throws WCMException {
        }
        @Deprecated
        public void execute(ResourceResolver arg0, LiveRelationship arg1,
                            ActionConfig arg2, boolean arg3, boolean arg4)
                throws WCMException {
        }
        @Deprecated
        public String getParameterName() {
            return null;
        }
        @Deprecated
        public String[] getPropertiesNames() {
            return null;
        }
        @Deprecated
        public int getRank() {
            return 0;
        }
        @Deprecated
        public String getTitle() {
            return null;
        }
        @Deprecated
        public void write(JSONWriter arg0) throws JSONException {
        }
    }
}

