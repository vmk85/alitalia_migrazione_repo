package com.alitalia.aem.consumer.checkin.servlet;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.data.home.checkin.CheckinCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.data.home.enumerations.CheckinPassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLinkTypeEnum;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.PDFManager;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.carnet.CarnetConstants;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinFlightDataMail;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.servlet.PDFCreatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkinrefusedmail" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinCheckinRefusedServlet extends GenericCheckinFormValidatorServlet {

	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private PDFManager pdfManager;
	
	@Reference
	private CheckinSession checkinSession;
	
	private static final String E_MAIL_FIELD = "mailRecipient";
	private static final String KEY_PREFIX = "airportsData.";
	private static final String CITY_SUFFIX = ".city";
	private static final String NAME_SUFFIX = ".name";
	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		logger.debug("[CheckinMailRefusedServlet] validateForm");
		
		Validator validator = new Validator();
		
		validator.addDirectCondition(E_MAIL_FIELD, request.getParameter(E_MAIL_FIELD),
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(E_MAIL_FIELD, request.getParameter(E_MAIL_FIELD),
				CheckinConstants.MESSAGE_ERROR_INVALID_EMAIL, "isEmail");
		
		return validator.validate();

	}
	
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
		logger.debug("[CheckinMailRefusedServlet] performSubmit");
		
		boolean isError = false;
		
		// get ctx context data
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		
		// send email with boarding pass to passengers
		String[] values = getValuesMailRefused(ctx, request);
		
		// get only checked in passengers
		List<CheckinPassengerData> passengers = ctx.notCheckedInPassengers;
			
		String receiverMail = request.getParameter(E_MAIL_FIELD);
		List<String> receivers = new ArrayList<String>();
		receivers.add(receiverMail);
		
		
		if (!Boolean.TRUE.equals(checkinSession.sendEmail(
				ctx, request.getResource(), receivers,
				CheckinConstants.CHECKIN_TEMPLATE_EMAIL_REFUSED,
				CheckinConstants.CHECKIN_REFUSED_EMAIL_SUBJECT,
				null,
				"",
				CheckinConstants.PLACEHOLDER_REFUSED, values,
				CheckinConstants.CHECKIN_MAIL_REMINDER,
				passengers))) {
			isError = true;
		}
		
		// send response to client
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		json.key("result").value("OK");
		json.key("isError").value(isError);
		json.endObject();
		
	}
	
	
	private String[] getValuesMailRefused(CheckinSessionContext ctx, SlingHttpServletRequest request) {
		String[] values = new String[2];
		values[0] = getPassengersHtml(ctx.notCheckedInPassengers, request);
		values[1] = getFlightsHtml(ctx.selectedRoute, ctx.notCheckedInPassengers, request);
		return values;
	}
	
	
	
	private String getPassengersHtml(List<CheckinPassengerData> passengers, SlingHttpServletRequest request) {
		String html = "";
		String ticketLabel = getI18nTicketLabel(request);
		for (CheckinPassengerData passenger : passengers) {
			String passengerType = getPassengerType(passenger, request);
			html = html + "<tr><td style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top;'><p style='color: #0d4722; font-family: Helvetica,Arial,sans-serif; font-size: 16px;  margin: 0; padding: 0; text-align: left; text-transform: uppercase; font-weight: bold;'>" + passenger.getName() + " " +  passenger.getLastName() +  "<br /><span style='text-transform: none;font-weight: normal; font-size: 13px; color:#000;'>" + passengerType + " - " + ticketLabel + " " +  passenger.getEticket() + "</span></p></td></tr><tr><td  width='100%' height='10' style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top;'><br/></td></tr>";
		}
		return html;
	}
	
	private String getFlightsHtml(CheckinRouteData route, List<CheckinPassengerData> passengers, SlingHttpServletRequest request) {
		String html = "";
		List<CheckinFlightDataMail> flights = toFlightData(route, passengers, request);
		for (CheckinFlightDataMail flight : flights) {
			html = html + "<tr><td style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top;'><table border='0' cellpadding='0' cellspacing='0' style='border: 0; margin:0; padding: 0; text-align: left; vertical-align: top; width: 100%; '><tr><td style='border: 0; margin: 0; padding: 0; vertical-align: top;' width='32%'><table border='0' cellpadding='0' cellspacing='0' style=' border: 0; margin: 0; padding: 0;  display: inline-block;padding: 0; text-align: left; vertical-align: top;'><tr><td style='border: 0; margin: 0; padding: 0; vertical-align: top;' width='50%'><span style='display: inline-block; font-size: 15px; text-transform: uppercase; vertical-align: top; font-family: Helvetica,Arial,sans-serif; '> " + flight.getFlightNumber() + "</span><br /><span style='display: inline-block; font-size: 12px; text-transform: uppercase; vertical-align: top; font-family: Helvetica,Arial,sans-serif; color: #73a13f;'>" + flight.getClasses() + "</span></td><td style='border: 0; margin: 0; padding: 0; vertical-align: top;' width='6%'>&nbsp;</td><td style='border: 0; margin: 0; padding: 0; vertical-align: top; text-align: center;' width='20%'><span style='display: inline-block; font-size: 28px; vertical-align: top; font-family: Helvetica,Arial,sans-serif;font-weight:100;'>" + flight.getDay() + "</span></td><td style='border: 0; margin: 0; padding: 0; vertical-align: top;' width='23%'><span style='display: inline-block; font-size: 12px; line-height: 14px; text-transform: uppercase; vertical-align: top; font-family: Helvetica,Arial,sans-serif;'><span style='font-weight:bold;'>" + flight.getMonth() + "</span><br>" + flight.getYear() + "</span></td></tr></table></td><td style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top;' width='1%'>&nbsp;</td><td style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top; ' width='33%'><table border='0' cellpadding='0' cellspacing='0' style=' padding: 0; text-align: left; vertical-align: top; width: 100%;'><tr><td style='border: 0; margin: 0; padding: 0; text-align: left; vertical-align: top;' width='30%'><span style='display: inline-block; font-size: 24px; vertical-align: top;font-family: Helvetica,Arial,sans-serif;font-weight:100; '>" + flight.getDepartureHour() + "&zwnj;</span></td><td style='border:0;margin: 0; padding: 0; text-align: left; vertical-align: top' width='65%'><span style='display: inline-block; font-size: 12px; line-height: 14px; text-transform: uppercase; vertical-align: top;font-family: Helvetica,Arial,sans-serif;font-weight:bold; '>" + flight.getDepartureCity() + "<br><span style='color: #73a13f;'>" + "(" + flight.getDepartureCode() + ")" + "</span></span></td></tr></table></td><td style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top;' width='1%'>&nbsp;</td><td style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top; ' width='33%'><table border='0' cellpadding='0' cellspacing='0' style=' padding: 0; text-align: left; vertical-align: top; width: 100%;'><tr><td style='border: 0; margin: 0; padding: 0; text-align: left; vertical-align: top;' width='30%'><span style='display: inline-block; font-size: 24px; vertical-align: top;font-family: Helvetica,Arial,sans-serif;font-weight:100; '>" + flight.getArrivalHour() + "&zwnj;</span></td><td style='border:0;margin: 0; padding: 0; text-align: left; vertical-align: top' width='65%'><span style='display: inline-block; font-size: 12px; line-height: 14px; text-transform: uppercase; vertical-align: top;font-family: Helvetica,Arial,sans-serif;font-weight:bold; '>" + flight.getArrivalCity() + "<br><span style='color: #73a13f;'>" + "(" + flight.getArrivalCode() + ")" + "</span></span></td></tr></table></td></tr></table></td></tr>";
		}
		return html;
	}
	
	private String getI18nTicketLabel(SlingHttpServletRequest request) {
		Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		return i18n.get(CheckinConstants.CHECKIN_MAIL_REFUSED_TICKET);
	}
	
	private String getPassengerType(CheckinPassengerData passenger, SlingHttpServletRequest request) {
		Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		String type = "";
		final I18n i18n = new I18n(resourceBundle);
		if (CheckinPassengerTypeEnum.NORMAL.equals(passenger.getType())) {
			type = i18n.get(CheckinConstants.CHECKIN_MAIL_REFUSED_ADULT);
			if (MmbLinkTypeEnum.INFANT_ACCOMPANIST.equals(passenger.getLinkType())) {
				type = type + " " + i18n.get(CheckinConstants.CHECKIN_MAIL_REFUSED_INFANT);
			}
		}
		if (CheckinPassengerTypeEnum.CHILD.equals(passenger.getType())) {
			type = i18n.get(CheckinConstants.CHECKIN_MAIL_REFUSED_CHILD);
		}
		return type;
	}
	
	private String getSeatClasses(List<CheckinPassengerData> passengers, CheckinRouteData route, Integer flightId ) {
		ArrayList<MmbCompartimentalClassEnum> classes = new ArrayList<MmbCompartimentalClassEnum>();
		// Get passengers classes
		for (CheckinPassengerData passenger : passengers) {
			for (CheckinCouponData coupon : passenger.getCoupons()) {
				if (coupon != null && coupon.getSeatClass() != null && flightId.equals(coupon.getFlightId()) && !classes.contains(coupon.getSeatClass())) {
					classes.add(coupon.getSeatClass());
				}
			}
		}
		
		// Build the string
		String output = "";
		CheckinFlightData flight = route.getFlights().stream().filter( fl -> fl.getId().equals(flightId)).findFirst().orElse(null);
		for (MmbCompartimentalClassEnum seatClass : classes) {
			output = output + CheckinUtils.getCompartimentalClass(seatClass, flight) + " / ";
		}
		if (output.length() > 2) {
			return output.substring(0, output.length()-2).toUpperCase();
		}
		else {
			return "";
		}
		
	}
	
	private List<CheckinFlightDataMail> toFlightData(CheckinRouteData route, List<CheckinPassengerData> passengers, SlingHttpServletRequest request) {
		Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		List<CheckinFlightDataMail> checkinFlightDataMail = new ArrayList<CheckinFlightDataMail>();
		List<CheckinFlightData> flightsData = route.getFlights();
		for (CheckinFlightData flightData : flightsData) {
			CheckinFlightDataMail flight = new CheckinFlightDataMail();
			DateRender dateTimeRenderDep = new DateRender(flightData.getDepartureDateTime());
			flight.setDay(dateTimeRenderDep.getDay());
			flight.setMonth(i18n.get(dateTimeRenderDep.getShortTextMonth()));
			flight.setYear(dateTimeRenderDep.getYear());
			flight.setDepartureHour(dateTimeRenderDep.getHour24());
			DateRender dateTimeRenderArr = new DateRender(flightData.getArrivalDateTime());
			flight.setArrivalHour(dateTimeRenderArr.getHour24());
			flight.setFlightNumber(flightData.getCarrier()+ flightData.getFlightNumber());
			String cityDeparture = i18n.get(KEY_PREFIX + flightData.getFrom().getCode() + CITY_SUFFIX ); 
			String cityArrival = i18n.get(KEY_PREFIX + flightData.getTo().getCode() + CITY_SUFFIX ); 
			flight.setDepartureAirport(i18n.get(KEY_PREFIX + flightData.getFrom().getCode() + NAME_SUFFIX));
			flight.setDepartureCity(cityDeparture);
			flight.setDepartureCode(flightData.getFrom().getCode());
			flight.setArrivalCity(cityArrival);
			flight.setArrivalCode(flightData.getTo().getCode());
			flight.setArrivalAirport(i18n.get(KEY_PREFIX + flightData.getTo().getCode() + NAME_SUFFIX));
			flight.setClasses(getSeatClasses(passengers, route, flightData.getId()));
			checkinFlightDataMail.add(flight);
			
		}
		return checkinFlightDataMail;
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}

