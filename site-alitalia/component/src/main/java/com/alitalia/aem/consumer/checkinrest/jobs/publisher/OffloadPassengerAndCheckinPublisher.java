package com.alitalia.aem.consumer.checkinrest.jobs.publisher;

import com.alitalia.aem.common.data.checkinrest.model.offload.FlightInfo;
import com.alitalia.aem.common.data.checkinrest.model.offload.request.SeatList;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSelectedPassengerRequest;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkinrest.jobs.CheckinJobConstants;
import com.alitalia.aem.consumer.checkinrest.jobs.consumer.OffloadPassengerCheckinAndClearAncillariesJob;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ggadaleta on 06/02/2018.
 */
@Deprecated
public class OffloadPassengerAndCheckinPublisher extends BaseJobPublisher {

    public OffloadPassengerAndCheckinPublisher() {
        this.setJobTopic(CheckinJobConstants.TOPIC_PASSENGER_OFFLOAD_AND_CHECKIN);
    }

    @Override
    public Map<String, Object> createProperties(CheckinSessionContext ctx) {

        Gson gson = new Gson();
        final Map<String, Object> props = new HashMap<>();

        //set sessionId
        props.put(OffloadPassengerCheckinAndClearAncillariesJob.PROPERTY_CHECKIN_SESSION_ID, this.getSessionID());

        List<FlightsRender> selectedFlights = ctx.selectedFlights;
        List<Passenger> selectedPassengers = ctx.checkinSelectedPassengers;

        if (selectedFlights.size() > 0 && selectedPassengers.size() > 0) {

            //offload request
            CheckinOffloadRequest offloadRequest = new CheckinOffloadRequest();
            offloadRequest.setConversationID(ctx.conversationId);
            offloadRequest.setLanguage(ctx.language);
            offloadRequest.setMarket(ctx.market);

            List<FlightInfo> flightInfoList = new ArrayList<>();

            int counterRequests = 0;
            for (FlightsRender flightsRender : selectedFlights) {
                if (flightsRender.getSegments().size() > 0) {
                    int segmentIndex = 0;
                    for (SegmentRender segmentRender : flightsRender.getSegments()) {

                        //offload flightinfo and seat
                        FlightInfo flighInfoRequest = new FlightInfo();
                        flighInfoRequest.setAirline(segmentRender.getAirline());
                        flighInfoRequest.setFlight(segmentRender.getFlight());
                        flighInfoRequest.setDepartureDate(segmentRender.getOriginalDepartureDate());
                        flighInfoRequest.setOrigin(segmentRender.getOrigin().getCode());

                        List<SeatList> seatRequestList = new ArrayList<>();
                        for (Passenger passengerData : selectedPassengers) {
                            SeatList seatRequest = new SeatList();
                            seatRequest.setFirstName(passengerData.getNome());
                            seatRequest.setLastName(passengerData.getCognome());
//                            seatRequest.setPassengerID(passengerData.getPassengerID());
                            seatRequestList.add(seatRequest);
                        }
                        flighInfoRequest.setSeatList(seatRequestList);
                        flightInfoList.add(flighInfoRequest);

                        //checkin ( solo su primo segmento, in quanto Sabre provvede a farlo sul resto del volo)
                        if (segmentIndex == 0) {
                            CheckinSelectedPassengerRequest checkinRequest = new CheckinSelectedPassengerRequest();
                            checkinRequest.setPnr(ctx.pnr);
                            checkinRequest.setAirline(segmentRender.getAirline());
                            checkinRequest.setFlight(segmentRender.getFlight());
                            checkinRequest.setOrigin(segmentRender.getOrigin().getCode());
                            checkinRequest.setDepartureDate(segmentRender.getOriginalDepartureDate());
                            checkinRequest.setLanguage(ctx.language);
                            checkinRequest.setMarket(ctx.market);
                            checkinRequest.setConversationID(ctx.conversationId);

                            List<com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request.Passenger> passengerDataRequestList = new ArrayList<>();
                            for (Passenger passengerData : selectedPassengers) {
                                com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request.Passenger passengerDataRequest =
                                        new com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request.Passenger();

                                passengerDataRequest.setFirstName(passengerData.getNome());
                                passengerDataRequest.setLastName(passengerData.getCognome());
                                passengerDataRequest.setPassengerID(passengerData.getPassengerID());
                                passengerDataRequestList.add(passengerDataRequest);
                            }
                            checkinRequest.setPassengers(passengerDataRequestList);

                            //richiesta checkin effettuata solo per segmentIndex==0 di ogni selectedFlight
                            props.put(OffloadPassengerCheckinAndClearAncillariesJob.PROPERTY_CHECKIN_CHECKINPASSENGER + counterRequests, gson.toJson(checkinRequest));
                            counterRequests++;
                        }
                        segmentIndex++;
                    }
                }
            }
            if(flightInfoList.size() > 0) {
//                offloadRequest.setFlightInfo(flightInfoList);
                props.put(OffloadPassengerCheckinAndClearAncillariesJob.PROPERTY_CHECKIN_OFFLOAD, gson.toJson(offloadRequest));
            }
        }
        return props;
    }
}
