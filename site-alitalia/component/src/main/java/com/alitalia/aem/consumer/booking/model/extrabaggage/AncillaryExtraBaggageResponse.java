package com.alitalia.aem.consumer.booking.model.extrabaggage;

import java.util.ArrayList;
import java.util.List;

public class AncillaryExtraBaggageResponse {
	private boolean extraBaggageAdmitted;
	private List<AncillariesExtraBagsPassenger> ancillariesExtraBagsPassengerList;

	
	public Boolean getExtraBaggageAdmitted() {
		return extraBaggageAdmitted;
	}

	public Boolean setExtraBaggageAdmitted(boolean isAdmitted){
		return this.extraBaggageAdmitted=isAdmitted;
	}
	public List<AncillariesExtraBagsPassenger> getAncillariesExtraBagsPassengerList() {
		return ancillariesExtraBagsPassengerList;
	}

	public void setAncillariesExtraBagsPassengerList(ArrayList<AncillariesExtraBagsPassenger> ancillariesExtraBagsPassengerList) {
		this.ancillariesExtraBagsPassengerList = ancillariesExtraBagsPassengerList;
	}
	
	public void addAncillariesExtraBagsPassengerList(AncillariesExtraBagsPassenger item) {
		this.ancillariesExtraBagsPassengerList.add(item);
	}
	

	@Override
	public String toString() {
		return "";
	//	return "AncillaryPassenger [bagaggeCode=" + bagaggeCode + ", bagaggeAmount=" + bagaggeAmount +"]";
	}
	
	

}
