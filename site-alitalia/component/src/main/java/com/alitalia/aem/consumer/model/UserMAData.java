package com.alitalia.aem.consumer.model;

public class UserMAData {

    private String address1_Address;
    private String address1_CompanyName;
    private String address1_Type ;
    private String address1_Zip;
    private boolean auth_Com;
    private String phone1AreaCode;

    public String getAddress1_Address() {
        return address1_Address;
    }

    public void setAddress1_Address(String address1_Address) {
        this.address1_Address = address1_Address;
    }

    public String getAddress1_CompanyName() {
        return address1_CompanyName;
    }

    public void setAddress1_CompanyName(String address1_CompanyName) {
        this.address1_CompanyName = address1_CompanyName;
    }

    public String getAddress1_Type() {
        return address1_Type;
    }

    public void setAddress1_Type(String address1_Type) {
        this.address1_Type = address1_Type;
    }

    public String getAddress1_Zip() {
        return address1_Zip;
    }

    public void setAddress1_Zip(String address1_Zip) {
        this.address1_Zip = address1_Zip;
    }

    public boolean isAuth_Com() {
        return auth_Com;
    }

    public void setAuth_Com(boolean auth_Com) {
        this.auth_Com = auth_Com;
    }

    public String getPhone1AreaCode() {
        return phone1AreaCode;
    }

    public void setPhone1AreaCode(String phone1AreaCode) {
        this.phone1AreaCode = phone1AreaCode;
    }
}
