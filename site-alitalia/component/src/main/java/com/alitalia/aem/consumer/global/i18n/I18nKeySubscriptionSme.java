package com.alitalia.aem.consumer.global.i18n;

public class I18nKeySubscriptionSme {

	//AREA
	public static final String AREA_NAME_ERROR_EMPTY = "specialpages.area_name.error.empty";
	public static final String AREA_NUM_EMPLOYERS_ERROR_EMPTY = "specialpages.area_num_employers.error.empty";
	public static final String AREA_NUM_FLYERS_ERROR_EMPTY = "specialpages.area_num_flyers.error.empty";
	public static final String AREA_FLEW_AVERAGE_ANNUAL_ERROR_EMPTY = "specialpages.area_flew_aver";
	
}
