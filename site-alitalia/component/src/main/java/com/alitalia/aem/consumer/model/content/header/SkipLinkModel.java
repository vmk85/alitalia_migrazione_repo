package com.alitalia.aem.consumer.model.content.header;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables={Resource.class})
public class SkipLinkModel {
       
       private Logger logger = LoggerFactory.getLogger(SkipLinkModel.class);
       
       @Inject @Default(values = "")
       private String urlStartPage;
       
       @Inject @Default(values = "")
       private String urlSiteNavigation;
       
       @Inject @Default(values = "")
       private String urlContentPage;
       
       @Inject @Default(values = "")
       private String urlContact;
       
       @Inject @Default(values = "")
       private String urlSiteMap;
       
       @Inject @Default(values = "")
       private String urlSearch;
       
       @Inject @Default(values = "")
       private String urlBooking;
       
       @Self @Default(values = "")
    private Resource resource;
       
       @Inject @Default(values = "")
    private AlitaliaConfigurationHolder configuration;
       
       @PostConstruct
    protected void initModel() {
             logger.debug("Initializing model << SkipLinkModel >>");
             logger.debug("Parameters << SkipLinkModel >>: [Url start page: " + urlStartPage + "], "
                           + "[Url site navigation: " + urlSiteNavigation + "], "
                                        + "[Url site map: " + urlSiteMap + "], "
                                                      + "[Url contatti: " + urlContact + "], "
                                                                   + "[Url content: " + urlContentPage + "], "
                                                                                 + "[Url search: " + urlSearch + "]"
                                                                                       + "[Url booking: " + urlBooking + "]");
             if(urlStartPage == null || (urlStartPage != null && urlStartPage.equals(""))){
                    //Se vuota la valorizzo con l'home-page del sito
                    urlStartPage = AlitaliaUtils.findSiteBaseExternalUrl(resource, true) 
             + configuration.getIndexPage();
             }
             
             if(urlSiteNavigation == null || 
                           (urlSiteNavigation != null && urlSiteNavigation.equals(""))){
                    urlSiteNavigation = "#site-navigation";
             }
             
             if(urlContentPage == null || 
                           (urlContentPage != null && urlContentPage.equals(""))){
                    urlContentPage = "#main-content";
             }
             
             if(urlContact == null || 
                           (urlContact != null && urlContact.equals(""))){
                    urlContact = "#";//TODO: quale deve essere il caso base?
             }
             
             if(urlSiteMap == null || 
                           (urlSiteMap != null && urlSiteMap.equals(""))){
                    urlSiteMap = "#"; // TODO: quale deve essere il caso base? Recuperare la url a partire dal template?
             }
             
             if(urlSearch == null || 
                           (urlSearch != null && urlSearch.equals(""))){
                    urlSearch = "#mainMenu__search";
             }
             
             if(urlBooking == null || 
                           (urlBooking != null && urlBooking.equals(""))){
                    urlBooking = "#departureInput";
             }
       }
       
       public String getUrlStartPage() {
             return urlStartPage;
       }
       
       public void setUrlStartPage(String urlStartPage) {
             this.urlStartPage = urlStartPage;
       }
       
       public String getUrlSiteNavigation() {
             return urlSiteNavigation;
       }
       
       public void setUrlSiteNavigation(String urlSiteNavigation) {
             this.urlSiteNavigation = urlSiteNavigation;
       }
       
       public String getUrlContentPage() {
             return urlContentPage;
       }
       
       public void setUrlContentPage(String urlContentPage) {
             this.urlContentPage = urlContentPage;
       }
       
       public String getUrlContact() {
             return urlContact;
       }
       
       public void setUrlContact(String urlContact) {
             this.urlContact = urlContact;
       }
       
       public String getUrlSiteMap() {
             return urlSiteMap;
       }
       
       public void setUrlSiteMap(String urlSiteMap) {
             this.urlSiteMap = urlSiteMap;
       }
       
       public String getUrlSearch() {
             return urlSearch;
       }
       
       public void setUrlSearch(String urlSearch) {
             this.urlSearch = urlSearch;
       }
       
       public String getUrlBooking() {
             return urlBooking;
       }
       
       public void setUrlBooking(String urlBooking) {
             this.urlBooking = urlBooking;
       }

}
