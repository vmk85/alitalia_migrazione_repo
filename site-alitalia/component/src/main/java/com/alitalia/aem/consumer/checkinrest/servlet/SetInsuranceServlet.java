package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.checkinrest.SetInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.SetInsuranceResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.web.component.webcheckin.delegaterest.IInsuranceDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;

@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "setinsurancerest"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class SetInsuranceServlet extends GenericCheckinFormValidatorServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile IInsuranceDelegate insuranceDelegate;

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		
		try {
			logger.info("Entrati in [SetInsuranceServlet - performSubmit]");

			SetInsuranceRequest setInsuranceRequest = new SetInsuranceRequest(IDFactory.getTid(), IDFactory.getSid(request));
			setInsuranceRequest.setConversationID(ctx.conversationId);
			setInsuranceRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
			setInsuranceRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
			setInsuranceRequest.setPnr(ctx.pnr);
			setInsuranceRequest.setPrice(ctx.insurancePolicy.getTotalInsuranceCost());
			
			logger.info("[SetInsuranceServlet] invocando insuranceDelegate.setInsurance...");
			SetInsuranceResponse setInsuranceResponse = insuranceDelegate.setInsurance(setInsuranceRequest);

			if (setInsuranceResponse != null) {

				// ctx.conversationId = setInsuranceResponse.getConversationID();

				if(setInsuranceResponse.getError() != null && !setInsuranceResponse.getError().equals("")){
					managementError(request, response, setInsuranceResponse.getError());
				}else{
					//response.sendRedirect(successPage);
					if("OK".equals(setInsuranceResponse.getOutcome()) && ctx.pnrSelectedListDataRender.size() > 0) {
						for(int pnr = 0; pnr < ctx.pnrSelectedListDataRender.size(); pnr++){
							if(ctx.pnrSelectedListDataRender.get(pnr).getNumber().equals(setInsuranceRequest.getPnr())) {
								ctx.pnrSelectedListDataRender.get(pnr).setInsurance(true);
							}
						}
					}
					managementSucces(request, response, setInsuranceResponse);
				}
			}
			else {
				logger.error("[SetInsuranceServlet] - Errore durante l'invocazione del servizio." );
			}
		}
		catch (Exception e) {
			throw new RuntimeException("[SetInsuranceServlet] - Errore durante l'invocazione del servizio.", e);
		}

	}
	
	private void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error){

		logger.info("[SetInsuranceServlet] [managementError] error =" + error );

		Gson gson = new Gson();

		com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);

		TidyJSONWriter json;
		try {
			json = new TidyJSONWriter(response.getWriter());

			response.setContentType("application/json");

			json.object();
			json.key("isError").value(true);
			json.key("errorMessage").value(errorObj.getErrorMessage());
			json.key("sabreStatusCode").value(errorObj.getSabreStatusCode());
			json.key("conversationID").value(errorObj.getConversationID());
			json.endObject();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
	}
	
	private void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response, SetInsuranceResponse setInsuranceResponse){

		logger.info("[SetInsuranceServlet] [managementSucces] ...");
		
		try {
			TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());
	
			jsonOutput.object();
			jsonOutput.key("outcome").value(setInsuranceResponse.getOutcome());
			jsonOutput.key("pnr").value(setInsuranceResponse.getPnr());
			jsonOutput.key("price").value(setInsuranceResponse.getPrice());
			jsonOutput.key("conversationID").value(setInsuranceResponse.getConversationID());
			jsonOutput.endObject();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
		
//		String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
//				request.getResource(), false)
//				+ getConfiguration().getCheckinFlightListPage();
//
//		successPage = request.getResourceResolver().map(successPage);
//
//		logger.info("[SetInsuranceServlet] [managementSucces] successPage = " + successPage );
//
//		TidyJSONWriter json;
//		try {
//
//			json = new TidyJSONWriter(response.getWriter());
//			response.setContentType("application/json");
//			json.object();
//			json.key("isError").value(false);
//			json.key("successPage").value(successPage);
//			json.endObject();
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}	

	}


}
