package com.alitalia.aem.consumer.checkin.servlet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.enumerations.PassportTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinPassenger;
import com.alitalia.aem.consumer.checkin.CheckinPassengersForm;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checkinpassengersdataapis" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinPassengersDataApisServlet extends GenericCheckinFormValidatorServlet {
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private CheckinSession checkinSession;
	
	/* APIS basic + APIS plus additional info */
	public final static String NATIONALITY_FIELD = "nationality";
	public final static String BIRTH_DAY_FIELD = "birthDay";
	public final static String BIRTH_MONTH_FIELD = "birthMonth";
	public final static String BIRTH_YEAR_FIELD = "birthYear";
	public final static String GENDER_FIELD = "gender";
	public final static String PASSPORT_EXPIRY_DAY_FIELD = "expiryDay";
	public final static String PASSPORT_EXPIRY_MONTH_FIELD = "expiryMonth";
	public final static String PASSPORT_EXPIRY_YEAR_FIELD = "expiryYear";
	public final static String DOCUMENT_TYPE_FIELD = "documentType";
	public final static String PASSPORT_FIELD = "passport";
	
	/* APIS plus */
	public final static String NAME_FIELD = "name";
	public final static String MIDDLE_NAME_FIELD = "middleName";
	public final static String LAST_NAME_FIELD = "lastName";
	public final static String RESIDENCY_FIELD = "residency";
	public final static String DESTINATION_STATE_FIELD = "destinationState";
	public final static String DESTINATION_ADDRESS_FIELD = "destinationAddress";
	public final static String DESTINATION_CITY_FIELD = "destinationCity";
	public final static String DESTINATION_ZIP_CODE_FIELD = "destinationZipCode";
	
	/* APIS plus additional info extra document */
	public final static String EXTRA_DOCUMENT_TYPE_FIELD = "extraDocumentType";
	public final static String VISTO_NUMBER_FIELD = "vistoNumber";
	public final static String VISTO_EXPIRY_DAY_FIELD = "vistoDateDay";
	public final static String VISTO_EXPIRY_MONTH_FIELD = "vistoDateMonth";
	public final static String VISTO_EXPIRY_YEAR_FIELD = "vistoDateYear";
	public final static String VISTO_DELIVERY_COUNTRY_FIELD = "vistoDeliveryCountry";
	public final static String GREENCARD_NUMBER_FIELD = "greenCardNumber";
	public final static String GREENCARD_EXPIRY_DAY_FIELD = "greenCardDateDay";
	public final static String GREENCARD_EXPIRY_MONTH_FIELD = "greenCardDateMonth";
	public final static String GREENCARD_EXPIRY_YEAR_FIELD = "greenCardDateYear";
	public final static String GREENCARD_DELIVERY_COUNTRY_FIELD = "greenCardDeliveryCountry";
	
	public final static String COUNTRY_USA_VALUE = "USA";
	public final static String ESTA_ENUM_VALUE = "None";
	public final static String VISTO_ENUM_VALUE = "Visa";
	public final static String GREENCARD_ENUM_VALUE = "GreenCard";
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		Validator validator = null;
		CheckinPassengersForm form = null;
		
		if (ctx.apisPlus) {
			if (ctx.additionalInfoPhase) {
				if (ctx.additionalInfoPhaseCase3 || ctx.additionalInfoPhaseCase4) {
					form = prepareFormApisPlusAdditionalInfoCase3And4(request);
					if (ctx.additionalInfoPhaseCase3) {
						validator = validateFormApisPlusAdditionalInfoCase3(form, ctx);
					} else {
						validator = validateFormApisPlusAdditionalInfoCase4(form, ctx);
					}
				} else {
					form = prepareFormApisPlusAdditionalInfo(request);
					if (ctx.additionalInfoPhaseCase2) {
						validator = validateFormApisPlusAdditionalInfoCase2(form, ctx);
					} else {
						validator = validateFormApisPlusAdditionalInfo(form, ctx);
					}
				}
			} else {
				form = prepareFormApisPlus(request);
				validator = validateFormApisPlus(form, ctx);
			}
		} else {
			form = prepareFormApisBasic(request);
			validator = validateFormApisBasic(form, ctx);
		}
		
		return validator.validate();
	}
	
	private CheckinPassengersForm prepareFormApisBasic(SlingHttpServletRequest request) {
		
		CheckinPassengersForm form = new CheckinPassengersForm();
		
		List<CheckinPassenger> passengers = new ArrayList<CheckinPassenger>();
		
		CheckinPassenger passenger = new CheckinPassenger();
		passenger.setNationality(request.getParameter(NATIONALITY_FIELD));
		passenger.setBirthDay(request.getParameter(BIRTH_DAY_FIELD));
		passenger.setBirthMonth(request.getParameter(BIRTH_MONTH_FIELD));
		passenger.setBirthYear(request.getParameter(BIRTH_YEAR_FIELD));
		passenger.setGender(request.getParameter(GENDER_FIELD));
		passenger.setDocumentType(request.getParameter(DOCUMENT_TYPE_FIELD));
		passenger.setPassport(request.getParameter(PASSPORT_FIELD));
		passenger.setPassportExpiryDay(request.getParameter(PASSPORT_EXPIRY_DAY_FIELD));
		passenger.setPassportExpiryMonth(request.getParameter(PASSPORT_EXPIRY_MONTH_FIELD));
		passenger.setPassportExpiryYear(request.getParameter(PASSPORT_EXPIRY_YEAR_FIELD));
		passengers.add(passenger);
		
		form.setPassengers(passengers);
		return form;
	}
	
	private CheckinPassengersForm prepareFormApisPlus(SlingHttpServletRequest request) {
		
		CheckinPassengersForm form = new CheckinPassengersForm();
		
		List<CheckinPassenger> passengers = new ArrayList<CheckinPassenger>();
		
		CheckinPassenger passenger = new CheckinPassenger();
		passenger.setName(request.getParameter(NAME_FIELD));
		passenger.setMiddleName(request.getParameter(MIDDLE_NAME_FIELD));
		passenger.setLastName(request.getParameter(LAST_NAME_FIELD));
		passenger.setResidency(request.getParameter(RESIDENCY_FIELD));
		passenger.setNationality(request.getParameter(NATIONALITY_FIELD));
		passengers.add(passenger);
		
		form.setPassengers(passengers);
		return form;
	}
	
	private CheckinPassengersForm prepareFormApisPlusAdditionalInfo(SlingHttpServletRequest request) {
		
		CheckinPassengersForm form = new CheckinPassengersForm();
		
		List<CheckinPassenger> passengers = new ArrayList<CheckinPassenger>();
		
		CheckinPassenger passenger = new CheckinPassenger();
		passenger.setBirthDay(request.getParameter(BIRTH_DAY_FIELD));
		passenger.setBirthMonth(request.getParameter(BIRTH_MONTH_FIELD));
		passenger.setBirthYear(request.getParameter(BIRTH_YEAR_FIELD));
		passenger.setGender(request.getParameter(GENDER_FIELD));
		passenger.setDocumentType(PassportTypeEnum.PASSPORT.value());//nel caso di APIS PLUS accetto solo Passaporto) request.getParameter(DOCUMENT_TYPE_FIELD));
		passenger.setPassport(request.getParameter(PASSPORT_FIELD));
		passenger.setPassportExpiryDay(request.getParameter(PASSPORT_EXPIRY_DAY_FIELD));
		passenger.setPassportExpiryMonth(request.getParameter(PASSPORT_EXPIRY_MONTH_FIELD));
		passenger.setPassportExpiryYear(request.getParameter(PASSPORT_EXPIRY_YEAR_FIELD));
		passenger.setDestinationState(request.getParameter(DESTINATION_STATE_FIELD));
		passenger.setDestinationAddress(request.getParameter(DESTINATION_ADDRESS_FIELD));
		passenger.setDestinationCity(request.getParameter(DESTINATION_CITY_FIELD));
		passenger.setDestinationZipCode(request.getParameter(DESTINATION_ZIP_CODE_FIELD));
		passengers.add(passenger);
		
		form.setPassengers(passengers);
		return form;
	}
	
	private CheckinPassengersForm prepareFormApisPlusAdditionalInfoCase3And4(SlingHttpServletRequest request) {

		CheckinPassengersForm form = prepareFormApisPlusAdditionalInfo(request);
		
		List<CheckinPassenger> passengers = new ArrayList<CheckinPassenger>();
		
		CheckinPassenger passenger = form.getPassengers().get(0);
		
		String extraDocumentType = request.getParameter(EXTRA_DOCUMENT_TYPE_FIELD);
		passenger.setExtraDocumentType(extraDocumentType);
		
		if(extraDocumentType != null && !extraDocumentType.isEmpty()){
			switch(extraDocumentType){
			case VISTO_ENUM_VALUE: 
				passenger.setExtraDocumentExpiryDay(request.getParameter(VISTO_EXPIRY_DAY_FIELD));
				passenger.setExtraDocumentExpiryMonth(request.getParameter(VISTO_EXPIRY_MONTH_FIELD));
				passenger.setExtraDocumentExpiryYear(request.getParameter(VISTO_EXPIRY_YEAR_FIELD));
				passenger.setExtraDocumentNumber(request.getParameter(VISTO_NUMBER_FIELD));
				passenger.setExtraDocumentDeliveryCountry(request.getParameter(VISTO_DELIVERY_COUNTRY_FIELD));
				break;
			case GREENCARD_ENUM_VALUE:
				passenger.setExtraDocumentExpiryDay(request.getParameter(GREENCARD_EXPIRY_DAY_FIELD));
				passenger.setExtraDocumentExpiryMonth(request.getParameter(GREENCARD_EXPIRY_MONTH_FIELD));
				passenger.setExtraDocumentExpiryYear(request.getParameter(GREENCARD_EXPIRY_YEAR_FIELD));
				passenger.setExtraDocumentNumber(request.getParameter(GREENCARD_NUMBER_FIELD));
				passenger.setExtraDocumentDeliveryCountry(request.getParameter(GREENCARD_DELIVERY_COUNTRY_FIELD));
				break;
			default:
				break;
			}
		}
		passengers.add(passenger);
		form.setPassengers(passengers);
		
		return form;
	}
	
	// Non necessario
//	private CheckinPassengersForm prepareFormApisPlusAdditionalInfoCase4(SlingHttpServletRequest request) {
//		return prepareFormApisPlusAdditionalInfoCase3And4(request);
//	}
	
	private Validator validateFormApisBasic(CheckinPassengersForm form, CheckinSessionContext ctx) {
		
		Validator validator = new Validator();
		
		List<CheckinPassenger> passengers = form.getPassengers();
		
		CheckinPassenger passenger = passengers.get(0);
		
		// Nationality
		validateNationality(validator, passenger, ctx);
		
		// Birth date
		validateBirthday(validator, passenger);
		
		// Gender
		validateGender(validator, passenger);
		
		//documentType
		validateDocumentType(validator, passenger, checkinSession.getDocumentAllowedValues(ctx));
		
		// Passport
		validatePassport(validator, passenger);
			
		return validator;
	}
	
	private void validateNationality(Validator validator, CheckinPassenger passenger, CheckinSessionContext ctx) {
		validator.addDirectCondition(NATIONALITY_FIELD, passenger.getNationality(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.setAllowedValues(NATIONALITY_FIELD, passenger.getNationality(),
				checkinSession.getCountriesISOCode(ctx), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
	}
	
	private void validateBirthday(Validator validator, CheckinPassenger passenger) {
		validator.addDirectCondition(BIRTH_DAY_FIELD, passenger.getBirthDay(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(BIRTH_MONTH_FIELD, passenger.getBirthMonth(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(BIRTH_YEAR_FIELD, passenger.getBirthYear(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.setAllowedValues(BIRTH_DAY_FIELD, passenger.getBirthDay(),
				checkinSession.getDays(), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
		validator.setAllowedValues(BIRTH_MONTH_FIELD, passenger.getBirthMonth(),
				checkinSession.getMonths(), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
		validator.setAllowedValues(BIRTH_YEAR_FIELD, passenger.getBirthYear(),
				checkinSession.getBirthYears(), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
	}
	
	private void validateGender(Validator validator, CheckinPassenger passenger) {
		validator.addDirectCondition(GENDER_FIELD, passenger.getGender(),
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.setAllowedValues(GENDER_FIELD, passenger.getGender(),
				new String[]{"M", "F"}, CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
	}
	
	private void validateDocumentType(Validator validator, CheckinPassenger passenger, String[] allowedValues){
		validator.addDirectCondition(DOCUMENT_TYPE_FIELD, passenger.getDocumentType(),
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.setAllowedValues(DOCUMENT_TYPE_FIELD, passenger.getDocumentType(),
				allowedValues, CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
	}
	
	private void validatePassport(Validator validator, CheckinPassenger passenger) {
		validator.addDirectCondition(PASSPORT_FIELD, passenger.getPassport(),
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addCrossConditionMessagePattern(PASSPORT_FIELD, passenger.getPassport(), "26",
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				"", "sizeIsLess");
		
		validator.addDirectCondition(PASSPORT_EXPIRY_DAY_FIELD, passenger.getPassportExpiryDay(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(PASSPORT_EXPIRY_MONTH_FIELD, passenger.getPassportExpiryMonth(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(PASSPORT_EXPIRY_YEAR_FIELD, passenger.getPassportExpiryYear(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.setAllowedValues(PASSPORT_EXPIRY_DAY_FIELD, passenger.getPassportExpiryDay(),
				checkinSession.getDays(), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
		validator.setAllowedValues(PASSPORT_EXPIRY_MONTH_FIELD, passenger.getPassportExpiryMonth(),
				checkinSession.getMonths(), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
		validator.setAllowedValues(PASSPORT_EXPIRY_YEAR_FIELD, passenger.getPassportExpiryYear(),
				checkinSession.getExpiryYears(), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
	}
	
	private Validator validateFormApisPlus(CheckinPassengersForm form, CheckinSessionContext ctx) {
		
		Validator validator = new Validator();
		
		List<CheckinPassenger> passengers = form.getPassengers();
		
		CheckinPassenger passenger = passengers.get(0);
		
		// Name and last name
		validator.addDirectCondition(NAME_FIELD, passenger.getName(),
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(LAST_NAME_FIELD, passenger.getLastName(),
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		// Nationality
		validateNationality(validator, passenger, ctx);
		
		// Residency
		validator.addDirectCondition(RESIDENCY_FIELD, passenger.getNationality(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.setAllowedValues(RESIDENCY_FIELD, passenger.getResidency(),
				checkinSession.getCountriesISOCode(ctx), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
		
		return validator;
	}
	
	private Validator validateFormApisPlusAdditionalInfo(CheckinPassengersForm form, CheckinSessionContext ctx) {
		
		Validator validator = new Validator();
		
		List<CheckinPassenger> passengers = form.getPassengers();
		
		CheckinPassenger passenger = passengers.get(0);
		
		// Birth date
		validateBirthday(validator, passenger);
		
		// Gender
		validateGender(validator, passenger);
		
		//documentType
		validateDocumentType(validator, passenger, checkinSession.getDocumentAllowedValues(ctx));
		
		// Passport
		validatePassport(validator, passenger);
		
		return validator;
	}
	
	private Validator validateFormApisPlusAdditionalInfoCase2(CheckinPassengersForm form, CheckinSessionContext ctx) {
		
		Validator validator = validateFormApisPlusAdditionalInfo(form, ctx);
		
		List<CheckinPassenger> passengers = form.getPassengers();
		
		CheckinPassenger passenger = passengers.get(0);
		
		// Destination
		validator.addDirectCondition(DESTINATION_ADDRESS_FIELD, passenger.getDestinationAddress(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addCrossConditionMessagePattern(DESTINATION_ADDRESS_FIELD, passenger.getDestinationAddress(), "36",
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				"", "sizeIsLess");
		validator.addDirectCondition(DESTINATION_CITY_FIELD, passenger.getDestinationCity(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addCrossConditionMessagePattern(DESTINATION_CITY_FIELD, passenger.getDestinationCity(), "26",
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				"", "sizeIsLess");
		validator.addDirectCondition(DESTINATION_STATE_FIELD, passenger.getDestinationState(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(DESTINATION_ZIP_CODE_FIELD, passenger.getDestinationZipCode(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addCrossConditionMessagePattern(DESTINATION_ZIP_CODE_FIELD, passenger.getDestinationZipCode(), "7",
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				"", "sizeIsLess");
		
		validator.setAllowedValues(DESTINATION_STATE_FIELD, passenger.getDestinationState(),
				checkinSession.getAmericanStates(ctx), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
		
		return validator;
	}
	
	private Validator validateFormApisPlusAdditionalInfoCase3(CheckinPassengersForm form, CheckinSessionContext ctx) {

		Validator validator = validateFormApisPlusAdditionalInfoCase2(form, ctx);
		
		List<CheckinPassenger> passengers = form.getPassengers();
		
		CheckinPassenger passenger = passengers.get(0);
		
		String extraDocumentType = passenger.getExtraDocumentType();
		
		validator.addDirectCondition(EXTRA_DOCUMENT_TYPE_FIELD, extraDocumentType,
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.setAllowedValues(EXTRA_DOCUMENT_TYPE_FIELD, extraDocumentType,
				new String[]{VISTO_ENUM_VALUE, GREENCARD_ENUM_VALUE}, CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);	
		
		switch(extraDocumentType){
		case VISTO_ENUM_VALUE: 
			// Visto number
			validator.addDirectCondition(VISTO_NUMBER_FIELD, passenger.getExtraDocumentNumber(),
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addCrossConditionMessagePattern(VISTO_NUMBER_FIELD, passenger.getExtraDocumentNumber(), "26",
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					"", "sizeIsLess");
			
			// Expiry date
			validateVistoExpiryDate(validator, passenger);
			
			// Delivery Country
			validator.addDirectCondition(VISTO_DELIVERY_COUNTRY_FIELD, passenger.getExtraDocumentDeliveryCountry(), 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.setAllowedValues(VISTO_DELIVERY_COUNTRY_FIELD, passenger.getExtraDocumentDeliveryCountry(),
					checkinSession.getCountriesISOCode(ctx), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
			break;
		case GREENCARD_ENUM_VALUE:
			// Visto number
			validator.addDirectCondition(GREENCARD_NUMBER_FIELD, passenger.getExtraDocumentNumber(),
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addCrossConditionMessagePattern(GREENCARD_NUMBER_FIELD, passenger.getExtraDocumentNumber(), "26",
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					"", "sizeIsLess");
			
			// Expiry date
			validateGreenCardExpiryDate(validator, passenger);
			
			// Delivery Country
			validator.addDirectCondition(GREENCARD_DELIVERY_COUNTRY_FIELD, passenger.getExtraDocumentDeliveryCountry(), 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.setAllowedValues(GREENCARD_DELIVERY_COUNTRY_FIELD, passenger.getExtraDocumentDeliveryCountry(),
					checkinSession.getAmericanStates(ctx), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);	
		}
		
		return validator;
	}
	
	private void validateVistoExpiryDate(Validator validator, CheckinPassenger passenger) {
		
		validator.addDirectCondition(VISTO_EXPIRY_DAY_FIELD, passenger.getExtraDocumentExpiryDay(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(VISTO_EXPIRY_MONTH_FIELD, passenger.getExtraDocumentExpiryMonth(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(VISTO_EXPIRY_YEAR_FIELD, passenger.getExtraDocumentExpiryYear(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.setAllowedValues(VISTO_EXPIRY_DAY_FIELD, passenger.getExtraDocumentExpiryDay(),
				checkinSession.getDays(), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
		validator.setAllowedValues(VISTO_EXPIRY_MONTH_FIELD, passenger.getExtraDocumentExpiryMonth(),
				checkinSession.getMonths(), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
		validator.setAllowedValues(VISTO_EXPIRY_YEAR_FIELD, passenger.getExtraDocumentExpiryYear(),
				checkinSession.getExpiryYears(), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
	}
	
	private void validateGreenCardExpiryDate(Validator validator, CheckinPassenger passenger) {
		
		validator.addDirectCondition(GREENCARD_EXPIRY_DAY_FIELD, passenger.getExtraDocumentExpiryDay(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(GREENCARD_EXPIRY_MONTH_FIELD, passenger.getExtraDocumentExpiryMonth(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(GREENCARD_EXPIRY_YEAR_FIELD, passenger.getExtraDocumentExpiryYear(), 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.setAllowedValues(GREENCARD_EXPIRY_DAY_FIELD, passenger.getExtraDocumentExpiryDay(),
				checkinSession.getDays(), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
		validator.setAllowedValues(GREENCARD_EXPIRY_MONTH_FIELD, passenger.getExtraDocumentExpiryMonth(),
				checkinSession.getMonths(), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
		validator.setAllowedValues(GREENCARD_EXPIRY_YEAR_FIELD, passenger.getExtraDocumentExpiryYear(),
				checkinSession.getExpiryYears(), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
	}
	
	private Validator validateFormApisPlusAdditionalInfoCase4(CheckinPassengersForm form, CheckinSessionContext ctx) {

		Validator validator = validateFormApisPlusAdditionalInfoCase2(form, ctx);

		List<CheckinPassenger> passengers = form.getPassengers();

		CheckinPassenger passenger = passengers.get(0);

		String extraDocumentType = passenger.getExtraDocumentType();

		validator.addDirectCondition(EXTRA_DOCUMENT_TYPE_FIELD, extraDocumentType,
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.setAllowedValues(EXTRA_DOCUMENT_TYPE_FIELD, extraDocumentType,
				new String[]{ESTA_ENUM_VALUE, VISTO_ENUM_VALUE, GREENCARD_ENUM_VALUE}, CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);

		switch(extraDocumentType){
		case VISTO_ENUM_VALUE: 
			// Visto number
			validator.addDirectCondition(VISTO_NUMBER_FIELD, passenger.getExtraDocumentNumber(),
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addCrossConditionMessagePattern(VISTO_NUMBER_FIELD, passenger.getExtraDocumentNumber(), "26",
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					"", "sizeIsLess");
			
			// Expiry date
						validateVistoExpiryDate(validator, passenger);
						
			// Delivery Country
			validator.addDirectCondition(VISTO_DELIVERY_COUNTRY_FIELD, passenger.getExtraDocumentDeliveryCountry(), 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.setAllowedValues(VISTO_DELIVERY_COUNTRY_FIELD, passenger.getExtraDocumentDeliveryCountry(),
					checkinSession.getCountriesISOCode(ctx), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
						
			break;
		case GREENCARD_ENUM_VALUE:
			// Visto number
			validator.addDirectCondition(GREENCARD_NUMBER_FIELD, passenger.getExtraDocumentNumber(),
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addCrossConditionMessagePattern(GREENCARD_NUMBER_FIELD, passenger.getExtraDocumentNumber(), "26",
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					"", "sizeIsLess");

			// Expiry date
			validateGreenCardExpiryDate(validator, passenger);

			// Delivery Country
			validator.addDirectCondition(GREENCARD_DELIVERY_COUNTRY_FIELD, passenger.getExtraDocumentDeliveryCountry(), 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.setAllowedValues(GREENCARD_DELIVERY_COUNTRY_FIELD, passenger.getExtraDocumentDeliveryCountry(),
					checkinSession.getAmericanStates(ctx), CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);	
		}

		return validator;
	}
	
	private boolean isAdditionalInfoCase2(CheckinPassengersForm form) {
		
		if (!form.getPassengers().isEmpty()) {
			CheckinPassenger checkinPassenger = form.getPassengers().get(0);
			if (checkinPassenger.getNationality().equals(COUNTRY_USA_VALUE) && 
					!checkinPassenger.getResidency().equals(COUNTRY_USA_VALUE)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean isAdditionalInfoCase3(CheckinPassengersForm form) {
		
		if (!form.getPassengers().isEmpty()) {
			CheckinPassenger checkinPassenger = form.getPassengers().get(0);
			if (!checkinPassenger.getNationality().equals(COUNTRY_USA_VALUE) &&
					checkinPassenger.getResidency().equals(COUNTRY_USA_VALUE)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean isAdditionalInfoCase4(CheckinPassengersForm form) {
		
		if (!form.getPassengers().isEmpty()) {
			CheckinPassenger checkinPassenger = form.getPassengers().get(0);
			if (!checkinPassenger.getNationality().equals(COUNTRY_USA_VALUE) && 
					!checkinPassenger.getResidency().equals(COUNTRY_USA_VALUE)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		CheckinPassengersForm form = null;
		
		if (ctx.apisPlus) {
			if (ctx.additionalInfoPhase) {
				if (ctx.additionalInfoPhaseCase3 || ctx.additionalInfoPhaseCase4){
					form = prepareFormApisPlusAdditionalInfoCase3And4(request);				 				
				} else {
					form = prepareFormApisPlusAdditionalInfo(request);
				}
			} else {
				form = prepareFormApisPlus(request);
			}
		} else {
			form = prepareFormApisBasic(request);
		}
		
		String baseUrl = request.getResource().getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
		
		// always TRUE if apis plus, FALSE otherwise (only if we are not already in the additional info phase)
		boolean additionalInfo = ctx.apisPlus && !ctx.additionalInfoPhase;
		
		boolean additionalInfoCase2 = additionalInfo && isAdditionalInfoCase2(form);
		boolean additionalInfoCase3 = additionalInfo && isAdditionalInfoCase3(form);
		boolean additionalInfoCase4 = additionalInfo && isAdditionalInfoCase4(form);
		
		CheckinPassenger passenger = form.getPassengers().get(0);
		
		if (ctx.apisPlus) {
			if (ctx.additionalInfoPhase) {
				if (ctx.additionalInfoPhaseCase3 || ctx.additionalInfoPhaseCase4 || ctx.additionalInfoPhaseCase2){
					if (ctx.additionalInfoPhaseCase2) {
						checkinSession.updatePassengerApisPlusAdditionalInfoCase2(passenger, ctx);
					} else if (ctx.additionalInfoPhaseCase3) {
						checkinSession.updatePassengerApisPlusAdditionalInfoCase3(passenger, ctx);
					} else {
						checkinSession.updatePassengerApisPlusAdditionalInfoCase4(passenger, ctx);
					}
				} else {
					checkinSession.updatePassengerApisPlusAdditionalInfo(passenger, ctx);
				}
			} else {
				checkinSession.updatePassengerApisPlus(passenger, ctx);
			}
		} else {
			checkinSession.updatePassengerApisBasic(passenger, ctx);
		}
		
		String nextPage = null;
		if (additionalInfo) {
			ctx.additionalInfoPhase = true;
			if (additionalInfoCase2) {
				ctx.additionalInfoPhaseCase2 = true;
				ctx.additionalInfoPhaseCase3 = false;
				ctx.additionalInfoPhaseCase4 = false;
			} else if (additionalInfoCase3) {
				ctx.additionalInfoPhaseCase2 = false;
				ctx.additionalInfoPhaseCase3 = true;
				ctx.additionalInfoPhaseCase4 = false;
			} else if (additionalInfoCase4) {
				ctx.additionalInfoPhaseCase2 = false;
				ctx.additionalInfoPhaseCase3 = false;
				ctx.additionalInfoPhaseCase4 = true;
			}
			nextPage = configuration.getCheckinPassengersPage();
			
		} else {
			
			ctx.additionalInfoPhase = false;
			if (++ctx.passengerIndex == ctx.selectedPassengers.size()) {
				checkinSession.performCheckin(ctx);
				nextPage = configuration.getCheckinAncillaryPage();
			} else {
				nextPage = configuration.getCheckinPassengersPage();
			}
		}
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		json.key("result").value("OK");
		json.key("redirect").value(baseUrl + nextPage);
		json.endObject();
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}
