package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.TabData;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.DateUtils;
import com.day.cq.commons.TidyJSONWriter;

@Component(immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "bookingchangetabconsumer" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class BookingChangeTabServlet extends SlingSafeMethodsServlet {
	
	private static final float FROM_MILLIS_TO_DAYS = 1000 * 60 * 60 * 24;


	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private BookingSession bookingSession;
	
	private static final String HYPHEN_FLIGHT_DATE_FORMAT = "yyyy-MM-dd";
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {

		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
		String genericErrorPagePlain = baseUrl + configuration.getBookingFailurePage();
		// FIXME Definire url corretta
		String errorFunctionalityPagePlain = baseUrl + configuration.getBookingFailurePage();
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		
		try {
			json.object();

			//retrive parameters form request
			if( ( request.getParameter("indexRoute")==null && ("").equals(request.getParameter("indexRoute")) ) 
					|| ( !("1").equals(request.getParameter("indexRoute")) && !("0").equals(request.getParameter("indexRoute")) ) ){
				logger.error("direction not found");
				throw new RuntimeException();
			}
			String indexRoute=request.getParameter("indexRoute");
			int searchElementIndex = Integer.parseInt(indexRoute);
			
			if( request.getParameter("selectedTab")==null && ("").equals(request.getParameter("selectedTab")) ){
				logger.error("selectedTab not found");
				throw new RuntimeException();
			}
			String selectedTab=request.getParameter("selectedTab");
			int dateChoiceIndex = Integer.parseInt(selectedTab);
			
			BookingSessionContext ctx =  (BookingSessionContext) request.getSession().getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
			String brandsPath = AlitaliaUtils.findSiteBaseRepositoryPath(request.getResource()) + AlitaliaConstants.BASE_BRAND_PATH;
			Resource rootResource = request.getResource().getResourceResolver().getResource(brandsPath);

			//INIZIO *** MODIFICA IP ***	
			String ipAddress = "";
			ipAddress = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
			if (ipAddress != null && !ipAddress.isEmpty()) {
				ctx.ipAddress = ipAddress;
			}
			//FINE.
			
			bookingSession.changeSearchElementDate(ctx, searchElementIndex, dateChoiceIndex,rootResource);
			
			response.setContentType("application/json");
			if (ctx.cug == BookingSearchCUGEnum.YOUTH) {
				if (!ctx.youthSolutionFound) {
					json.key("solutionNotFound").value("youth");
				}
			}
			if (ctx.cug == BookingSearchCUGEnum.FAMILY) {
				if (!ctx.familySolutionFound) {
					json.key("solutionNotFound").value("family");
				}
			}
			if (ctx.cug == BookingSearchCUGEnum.MILITARY) {
				if (!ctx.militarySolutionFound) {
					json.key("solutionNotFound").value("allSolutions");
					json.key("redirect").value(errorFunctionalityPagePlain);
				}
			}
			if (ctx.cug == BookingSearchCUGEnum.ADT) {
				if (ctx.availableFlights.getRoutes().isEmpty()) {
					json.key("solutionNotFound").value("allSolutions");
					json.key("redirect").value(errorFunctionalityPagePlain);
				}
			}
			writeAnalyticsInfo(ctx, json);
			json.endObject();
		}catch (Exception e) {
			logger.error("Unexpected error: ", e);
			if (!response.isCommitted()) {
				response.setContentType("application/json");
				try {
					json.key("redirect").value(genericErrorPagePlain);
					json.endObject();
				} catch (JSONException e1) {
					logger.error("Error building JSON");
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
				}
			}
		}
	}
	
	private void writeAnalyticsInfo(BookingSessionContext ctx, TidyJSONWriter json) throws JSONException{
		json.key("analytics_flightInfo").object();
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat(HYPHEN_FLIGHT_DATE_FORMAT);
			dateFormat.setLenient(false);
			DecimalFormat format = new DecimalFormat("#0.00");
			DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
			otherSymbols.setDecimalSeparator('.');
			otherSymbols.setCurrencySymbol(""); 
			format.setDecimalFormatSymbols(otherSymbols);
			format.setGroupingUsed(false);
			format.setPositivePrefix("");
			format.setNegativePrefix("");
			
			int searchElementIndex= 0;
			String[] prefixes = {"dep", "ret"};
			for(String prefix : prefixes){
				if(ctx.availableFlights.getRoutes().size() > searchElementIndex && ctx.availableFlights.getRoutes().get(searchElementIndex) != null){
					json.key(prefix + "Hours").value("");
					json.key(prefix + "Minutes").value("");
					json.key(prefix + "Brand").value("");
					json.key(prefix + "FlightNumber").value("");
					json.key(prefix + "cost").value("");
					json.key(prefix + "FareBasis").value("");
					String nuovaData = "";
					FlightData genericFlight = ctx.availableFlights.getRoutes().get(searchElementIndex).getFlights().get(0);
					if(genericFlight instanceof ConnectingFlightData){
						nuovaData = dateFormat.format(((DirectFlightData) ((ConnectingFlightData) genericFlight).getFlights().get(0)).getDepartureDate().getTime());
					}
					else if (genericFlight instanceof DirectFlightData){
						nuovaData = dateFormat.format(((DirectFlightData) genericFlight).getDepartureDate().getTime());
					}
					json.key(prefix + "Date").value(nuovaData);
					searchElementIndex++;
				}
			}
			BigDecimal fare = BigDecimal.ZERO;
			BigDecimal taxes = BigDecimal.ZERO;
			BigDecimal surcharges = BigDecimal.ZERO;
			BigDecimal totalPrice = BigDecimal.ZERO;
			json.key("fare").value(format.format(fare));
			json.key("taxes").value(format.format(taxes));
			json.key("surcharges").value(format.format(surcharges));
			json.key("totalPrice").value(format.format(totalPrice));
			/*TODO tasso di cambio?*/
			json.key("totalPriceEuro").value(format.format(totalPrice));
			/*delta giorni*/
			Calendar now = Calendar.getInstance();
			now.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
			String deltaBoToday = "";
			String deltaBoAr = "";
			String DayofWeekA = "";
			String DayofWeekR = "";
			if (ctx.availableFlights != null) {
				TabData outboundTab = getTabDataByRoute(ctx.availableFlights.getTabs(), RouteTypeEnum.OUTBOUND);
				int middleIndex = ((outboundTab.getFlightTabs().size() -1) / 2 );
				Calendar departure_date = outboundTab.getFlightTabs().get(middleIndex).getDate();
				DayofWeekA = String.valueOf(DateUtils.computeRealDayOfWeek(departure_date.get(Calendar.DAY_OF_WEEK)));
				deltaBoToday = String.valueOf(DateUtils.computeDiffDays(now, departure_date));
				if (ctx.availableFlights.getTabs().size() > 1) {
					TabData returnTab = getTabDataByRoute(ctx.availableFlights.getTabs(), RouteTypeEnum.RETURN);
					middleIndex = ((returnTab.getFlightTabs().size() -1) / 2 );
					Calendar return_date = returnTab.getFlightTabs().get(middleIndex).getDate();
					DayofWeekR = String.valueOf(DateUtils.computeRealDayOfWeek(return_date.get(Calendar.DAY_OF_WEEK)));
					deltaBoAr = String.valueOf(DateUtils.computeDiffDays(departure_date, return_date));
				}
			}
			json.key("deltaBoToday").value(deltaBoToday);
			json.key("deltaBoAr").value(deltaBoAr);
			json.key("DayofWeekA").value(DayofWeekA);
			json.key("DayofWeekR").value(DayofWeekR);
			json.endObject();
		}
		catch(Exception e){
			json.endObject();
			throw e;
		}
		
	}
	
	
	private TabData getTabDataByRoute(List<TabData> tabs, RouteTypeEnum type){
		return tabs.stream().filter( tab -> tab.getType().equals(type)).findFirst().orElse(null);
	}
}