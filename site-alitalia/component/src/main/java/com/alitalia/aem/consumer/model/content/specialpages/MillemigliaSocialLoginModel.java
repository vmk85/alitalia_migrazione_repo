package com.alitalia.aem.consumer.model.content.specialpages;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.servlet.login.CheckSocialLoginServlet;

@Model(adaptables={SlingHttpServletRequest.class})
public class MillemigliaSocialLoginModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;
	
	private MillemigliaSocialLoginData millemigliaSocialLoginData;
	
	@PostConstruct
	protected void initModel() {
		logger.info("[MillemigliaSocialLoginModel] initModel");
		
		super.initBaseModel(slingHttpServletRequest);
		
		HttpSession session = slingHttpServletRequest.getSession();
		
		@SuppressWarnings("unchecked")
		Map<String, String> pendingSocialLoginData = (Map<String, String>)
				session.getAttribute(CheckSocialLoginServlet.PENDING_SOCIAL_LOGIN_DATA_ATTRIBUTE);
		
		if (pendingSocialLoginData != null) {
			// initialize model from pending social login data, if found, and clear it
			millemigliaSocialLoginData = new MillemigliaSocialLoginData();
			millemigliaSocialLoginData.setFirstName(pendingSocialLoginData.get("gigyaUserFirstName"));
			millemigliaSocialLoginData.setLastName(pendingSocialLoginData.get("gigyaUserLastName"));
			millemigliaSocialLoginData.setProvider(pendingSocialLoginData.get("gigyaProvider").toUpperCase());
			millemigliaSocialLoginData.setId(pendingSocialLoginData.get("gigyaId"));
			millemigliaSocialLoginData.setSignature(pendingSocialLoginData.get("gigyaSignature"));
			session.removeAttribute(CheckSocialLoginServlet.PENDING_SOCIAL_LOGIN_DATA_ATTRIBUTE);
		} else {
			// otherwise, try to retrieve and remove SocialLoginData from session (if any)
			millemigliaSocialLoginData = (MillemigliaSocialLoginData) 
					session.getAttribute(MillemigliaSocialLoginData.NAME);
			session.removeAttribute(MillemigliaSocialLoginData.NAME);
		}
		
	}
	
	public MillemigliaSocialLoginData getMillemigliaSocialLoginData() {
		return millemigliaSocialLoginData;
	}
	
}
