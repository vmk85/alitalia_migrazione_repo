package com.alitalia.aem.consumer.myalitalia.servlet;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;
import com.gigya.socialize.GSObject;
import com.gigya.socialize.GSRequest;
import com.gigya.socialize.GSResponse;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;

import javax.inject.Inject;
import javax.servlet.Servlet;
import java.io.IOException;

@Component
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = { "gigyaSearchServlet" }),
        @Property(name = "sling.servlet.methods", value = { "POST" }),
        @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) ,
        @Property(name = "sling.servlet.extensions", value = { "json" })
})
//@SuppressWarnings("serial")
public class GigyaSearchServlet extends GenericMyAlitaliaFormValidatorServlet {

    @Reference
    private AlitaliaConfigurationHolder configuration;


    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

        logger.debug("[GigyaSearchServlet] performSubmit");

        TidyJSONWriter json = new TidyJSONWriter(response.getWriter());

        json.object();

        try {

            String email = request.getParameter("email");

            GSRequest gigyaRequest = new GSRequest(configuration.getMaApiKey(), configuration.getMaSecretKey(), "accounts.search", new GSObject(), true, configuration.getMaUserKey());

            gigyaRequest.setUseHTTPS(true);
            gigyaRequest.setAPIDomain("eu1.gigya.com");

            String query = "SELECT data.resetPwdSms, data.phone1Number, data.phone1CountryCode, data.isStrong FROM accounts WHERE profile.email = \"" + email + "\"";

            gigyaRequest.setParam("query", query);

            GSResponse gigyaResponse = gigyaRequest.send();

            logger.debug("[GigyaSearchServletquest with query = " + query);

            if (gigyaResponse.getErrorCode() == 0) {

                logger.debug("[GigyaSearchServlet] request sent, response OK");

                JSONObject obj = new JSONObject(gigyaResponse.getData().toJsonString());

                JSONArray results = (JSONArray) obj.get("results");
                JSONObject data = results.getJSONObject(0);
                data = (JSONObject) data.get("data");

                String isStrong = null, prefix = null, number = null;
                boolean isError;

                try {

                    isStrong = data.get("isStrong") != null ? data.get("isStrong").toString() : null;
                    prefix = data.get("phone1CountryCode") != null ? data.get("phone1CountryCode").toString() : null;
                    number = data.get("phone1Number") != null ? data.get("phone1Number").toString() : null;
                    isError = false;
//                    String resetType = data.get("resetPwdSms") != null ? data.get("resetPwdSms").toString() : null;
                } catch (Exception e) { isError = true; }

                prefix = fixPrefix(prefix);

                if(!isError) {
                    if (isStrong != null && isStrong.equals("true")) {
                        if (prefix != null && prefix.length() == 4) {
                            if (number != null && number.length() > 7) {
                                json.key("isError").value(false);
                                json.key("errorCode").value(0);
                                json.key("feedback").value("Ready for OTP");
                                json.key("blearNumber").value(blearNumber(number));

                                request.getSession().setAttribute("TelephoneNumber", prefix.concat(number));

                                request.getSession().removeAttribute("OTPSession");
                            } else {
                                json.key("isError").value(true);
                                json.key("errorCode").value(504);
                                json.key("feedback").value("Missing telephone number");
                            }
                        } else {
                            json.key("isError").value(true);
                            json.key("errorCode").value(504);
                            json.key("feedback").value("Missing prefix");
                        }
                    } else {
                        json.key("isError").value(true);
                        json.key("errorCode").value(5000);
                        json.key("feedback").value("User is not Strong");
                    }
                } else {
                    json.key("isError").value(true);
                    json.key("errorCode").value(5000);
                    json.key("feedback").value("User not strong");
                }

            } else {

                logger.error("[GigyaSearchServlet] request sent, error");

                json.key("isError").value(true);
                json.key("errorCode").value(gigyaResponse.getErrorCode());
                json.key("errorMessage").value(gigyaResponse.getErrorMessage());
                json.key("errorDetails").value(gigyaResponse.getErrorDetails());
            }

        } catch (Exception e) {

            json.key("isError").value(true);
            json.key("errorCode").value(500);
            json.key("errorMessage").value("internal server error");
            json.key("errorDetails").value(e.toString());

            logger.error("[GigyaSearchServlet] exception " + e.toString());

            e.printStackTrace();

        } finally {

            json.endObject();

            logger.error("[GigyaSearchServlet] finish");
        }

    }

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        logger.debug("[RecuperoTelefonoServlet] validateForm");
        Validator validator = new Validator();
        ResultValidation resultValidation = new ResultValidation();
        resultValidation.setResult(true);
        return resultValidation;
    }

    @Override
    protected void saveDataIntoSession(SlingHttpServletRequest request, Exception exception) {
        logger.debug("[RecuperoTelefonoServlet] saveDataIntoSession");

    }

    private String fixPrefix(String prefix) {
        String fix = "";
        if(prefix.length() < 4) {
            for(int i = prefix.length(); i < 4; i++) fix = fix.concat("0");
            return fix.concat(prefix);
        } else if(prefix.length() == 4) return prefix;
        else return null;
    }

    private String blearNumber(String number) {
        String firstNum = String.valueOf(number.charAt(0)).concat(String.valueOf(number.charAt(1)));
        String lastNum = number.substring(number.length() - 3);
        return firstNum.concat("*****").concat(lastNum);
    }

}



