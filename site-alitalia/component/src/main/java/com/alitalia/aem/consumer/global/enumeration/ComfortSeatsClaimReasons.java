package com.alitalia.aem.consumer.global.enumeration;

import java.util.ArrayList;

public enum ComfortSeatsClaimReasons {
	
	CANCELED_FLIGHT("canceledflight"),
	FLIGHT_TYPE("flighttype"),
	DELAYED_FLIGHT("delayedflight"),
	SEATS_TECNICAL_PROBLEM("seatstecnicalproblem"),
	SEATS_REASSIGNMENT("seatsreassignment"),
	OTHER_REASON("otherreason");
	
	private final String value;

	ComfortSeatsClaimReasons(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	public String getI18nValue() {
		return "specialpage.reason." + value + ".label";
	}
	
	public static ArrayList<String> listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (ComfortSeatsClaimReasons c: ComfortSeatsClaimReasons.values()) {
			result.add(c.value);
		}
		return result;
	}
	
	public static ArrayList<String> listI18nValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (ComfortSeatsClaimReasons c: ComfortSeatsClaimReasons.values()) {
			result.add(c.getI18nValue());
		}
		return result;
	}

	public static ComfortSeatsClaimReasons fromValue(String v) {
		for (ComfortSeatsClaimReasons c: ComfortSeatsClaimReasons.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
