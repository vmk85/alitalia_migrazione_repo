package com.alitalia.aem.consumer.booking.model.extrabaggage;



public class AncillariesExtraBagsPassengerRequest
{
	private String customerNumber;
	private String surname;
	private String name;
	private String Id;
	
	public final String getcustomerNumber()
	{
		return customerNumber;
	}
	
	public final void setcustomerNumber(String value)
	{
		customerNumber = value;
	}
	
	public final String getsurname()
	{
		return surname;
	}
	
	public final void setsurname(String value)
	{
		surname = value;
	}
	
	
	public final String getname()
	{
		return name;
	}
	
	public final void setname(String value)
	{
		name = value;
	}
	
	public final String getId()
	{
		return Id;
	}
	
	public final void setId(String value)
	{
		Id = value;
	}
	
	/*
	private ArrayList<TravelPart> TravelParts;
	public final ArrayList<TravelPart> getTravelParts()
	{
		return TravelParts;
	}
	public final void setTravelParts(ArrayList<TravelPart> value)
	{
		TravelParts = value;
	}
	*/
}