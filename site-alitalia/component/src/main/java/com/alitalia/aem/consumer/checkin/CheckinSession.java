package com.alitalia.aem.consumer.checkin;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

import com.alitalia.aem.common.data.home.*;
import com.alitalia.aem.common.messages.checkinrest.*;
import com.alitalia.aem.consumer.checkinrest.servlet.CheckinAuthorize3dServlet;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.engine.SlingRequestProcessor;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.exc.BehaviourException;
import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.request.FlightPassenger;
import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.AncillaryOffers;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPass;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPassInfo;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPassSms;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.Model;
import com.alitalia.aem.common.data.checkinrest.model.boardingpassprintable.BoardingPassPrintable;
import com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.response.Outcome;
import com.alitalia.aem.common.data.checkinrest.model.clearancillariescart.response.ClearAncillariesCart;
import com.alitalia.aem.common.data.checkinrest.model.endpayment.request.FraudNetParam;
import com.alitalia.aem.common.data.checkinrest.model.endpayment.response.EndPaymentResponse;
import com.alitalia.aem.common.data.checkinrest.model.fasttrack.request.FlightFastTrack;
import com.alitalia.aem.common.data.checkinrest.model.fasttrack.response.FastTrack;
import com.alitalia.aem.common.data.checkinrest.model.getcart.Cart;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.request.InsurancePayment;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.request.PaymentDetail;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.InitPaymentResponse;
import com.alitalia.aem.common.data.checkinrest.model.lounge.request.FlightLounge;
import com.alitalia.aem.common.data.checkinrest.model.lounge.response.Lounge;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Flight;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerSeat;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByAddPassenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByFrequentFlyerSearch;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByPnrSearch;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByTicketSearch;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Segment;
import com.alitalia.aem.common.data.checkinrest.model.reprintbp.FreeTextInfoList;
import com.alitalia.aem.common.data.checkinrest.model.setbaggage.request.FlightBaggage;
import com.alitalia.aem.common.data.checkinrest.model.setbaggage.response.Baggage;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.AdultDestinationDocument;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.AdultDocumentOther;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.AdultDocuments;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.AdultResidentDocument;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.Document;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.DocumentRequired;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.FrequentFlyer;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.InfantDestinationDocument;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.InfantDocumentOther;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.InfantDocuments;
import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.InfantResidentDocument;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryEticketData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryUpgradeDetailData;
import com.alitalia.aem.common.data.home.checkin.CheckinApisInfoData;
import com.alitalia.aem.common.data.home.checkin.CheckinCountryData;
import com.alitalia.aem.common.data.home.checkin.CheckinCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinCreditCardData;
import com.alitalia.aem.common.data.home.checkin.CheckinCreditCardHolderData;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinInsurancePolicyData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinPaymentData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.data.home.checkin.CheckinSeatMapData;
import com.alitalia.aem.common.data.home.checkin.UndoCheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.CanAddPassengerReasonEnum;
import com.alitalia.aem.common.data.home.enumerations.CheckinAncillaryServiceEnum;
import com.alitalia.aem.common.data.home.enumerations.CheckinPassengerStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.CheckinPassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.CheckinSaleChannelEnum;
import com.alitalia.aem.common.data.home.enumerations.CheckinSearchReasonEnum;
import com.alitalia.aem.common.data.home.enumerations.DocumentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbGdsTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLinkTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassportTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.StatisticsDataTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbAmericanStatesData;
import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCartItemData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryFlightData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryFrequentFlyerData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPaymentData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryQuotationData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillarySeatDetailData;
import com.alitalia.aem.common.messages.BaseRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinAddPassengerRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinAddPassengerResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinChangeSeatsResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinClearAncillariesCartRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinClearAncillariesCartResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinCreateBoardingPassRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinCreateBoardingPassResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinEndPaymentRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinEndPaymentResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinEnhancedSeatMapResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinFrequentFlyerSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinFrequentFlyerSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetBoardingPassInfoByNameRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetBoardingPassInfoByNameResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetBoardingPassInfoRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetBoardingPassInfoResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetCartRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetCartResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSelectedAncillaryOffersRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSelectedAncillaryOffersResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSmsDataRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSmsDataResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinInitPaymentRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinInitPaymentResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinIsBoardingPassPrintableRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinIsBoardingPassPrintableResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinReprintBPRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinReprintBPResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinSelectedPassengerRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSelectedPassengerResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetBaggageRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetBaggageResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetFastTrackRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetFastTrackResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetLoungeRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetLoungeResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetSmsDataRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetSmsDataResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdatePassengerRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdatePassengerResponse;
import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceResponse;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentResponse;
import com.alitalia.aem.common.messages.home.AdyenStoringPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenStoringPaymentResponse;
import com.alitalia.aem.common.messages.home.BoardingPassCheckinRequest;
import com.alitalia.aem.common.messages.home.BoardingPassCheckinResponse;
import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinRequest;
import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinResponse;
import com.alitalia.aem.common.messages.home.CheckinInsuranceResponse;
import com.alitalia.aem.common.messages.home.CheckinSendSmsRequest;
import com.alitalia.aem.common.messages.home.CheckinSendSmsResponse;
import com.alitalia.aem.common.messages.home.GetCountryRequest;
import com.alitalia.aem.common.messages.home.GetCountryResponse;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinRequest;
import com.alitalia.aem.common.messages.home.GetSeatMapCheckinResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusResponse;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesResponse;
import com.alitalia.aem.common.messages.home.PassengerListCheckinRequest;
import com.alitalia.aem.common.messages.home.PassengerListCheckinResponse;
import com.alitalia.aem.common.messages.home.RegisterPaymentBookingInsuranceStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrievePnrInformationCheckinResponse;
import com.alitalia.aem.common.messages.home.RetrieveStateListRequest;
import com.alitalia.aem.common.messages.home.RetrieveStateListResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinRequest;
import com.alitalia.aem.common.messages.home.SendBoardingPassCheckinResponse;
import com.alitalia.aem.common.messages.home.SendReminderCheckinRequest;
import com.alitalia.aem.common.messages.home.SendReminderCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAddToCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryCartResponse;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryResponse;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionRequest;
import com.alitalia.aem.common.messages.home.WebCheckinBPPermissionResponse;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinCancelCartResponse;
import com.alitalia.aem.common.messages.home.WebCheckinGetCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinGetCatalogRequest;
import com.alitalia.aem.common.messages.home.WebCheckinGetInsuranceRequest;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerRequest;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerResponse;
import com.alitalia.aem.common.messages.home.WebCheckinPayInsuranceRequest;
import com.alitalia.aem.common.messages.home.WebCheckinRemoveFromCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinResponse;
import com.alitalia.aem.common.messages.home.WebCheckinUpdateCartRequest;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.checkin.exception.CheckinFailedException;
import com.alitalia.aem.consumer.checkin.exception.CheckinPaymentException;
import com.alitalia.aem.consumer.checkin.exception.CheckinPnrInformationNotFoundException;
import com.alitalia.aem.consumer.checkin.render.CheckinSeatMapFlight;
import com.alitalia.aem.consumer.checkin.render.CheckinSeatMapRender;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.exception.InvalidAncillaryIdentifierException;
import com.alitalia.aem.consumer.mmb.exception.InvalidAncillaryStatusException;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaConstants;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.web.component.managemybooking.delegate.ManageMyBookingDelegate;
import com.alitalia.aem.web.component.myalitaliauser.delegaterest.IAdyenRecurringPaymentDelegate;
import com.alitalia.aem.web.component.myalitaliauser.delegaterest.IRegisterCreditCardDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.alitalia.aem.web.component.statistics.delegate.RegisterStatisticsDelegate;
import com.alitalia.aem.web.component.webcheckin.delegate.WebCheckinDelegate;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.alitalia.aem.web.component.webcheckin.delegaterest.IInsuranceDelegate;
import com.alitalia.aem.web.component.webcheckin.delegaterest.IPaymentDelegate;
import com.alitalia.aem.ws.common.exception.WSClientException;
//import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.i18n.I18n;
/**
 * The <code>CheckinSession</code> component tracks the behaviour of a Checkin
 * session and the related data through specialized methods.
 * 
 * <p>
 * Since the SCR component is stateless, the state of the single Checkin Session
 * is tracked in an external object <code>CheckinSessionContext</code>.
 * </p>
 * 
 * @see CheckinSessionContext
 */
@Component(immediate = true)
@Service(value = CheckinSession.class)
public class CheckinSession {

	public static final String APIS_PASSPORT_AIRPORT_CODES_PROPERTY = "apis.passport.airport.codes";

	private static Logger logger = LoggerFactory.getLogger(CheckinSession.class);

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ICheckinDelegate checkInDelegateRest;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile IInsuranceDelegate insuranceDelegateRest;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile WebCheckinDelegate checkInDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ManageMyBookingDelegate mmbDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile RegisterStatisticsDelegate registerStatisticsDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegate staticDataDelegate;

	@Reference
	private BookingSession bookingSession; //TODO togliere

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private RequestResponseFactory requestResponseFactory;

	@Reference
	private SlingRequestProcessor requestProcessor;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaCustomerProfileManager customerProfileManager;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile IPaymentDelegate paymentDelegateRest;

	//Tolentino - Inizio
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaConfigurationHolder configuration;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile IRegisterCreditCardDelegate registerCreditCardDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile IAdyenRecurringPaymentDelegate adyenRecurringPaymentDelegate;
	//Tolentino - Fine

	private ComponentContext componentContext;

	/* constructor */

	public CheckinSession() {
		super();
	}

	/* SCR lifecycle methods */

	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}


	/* interface methods */

	/**
	 * Restore an CheckIn session object
	 */

	/**
	 * Initialize the CheckIn session object, setting it ready to 
	 * start a new process for a given PNR number.
	 * 
	 * <p>This must be called when activating the CheckIn detail function
	 * on a prenotation, based on the provided tripId and
	 * additional environment data.</p>
	 * 
	 * @param i18n The i18n instance to be used for translations.
	 * @param serviceClient A string identifying the client when requested by
	 * 						remote services.
	 * @param machineName A string identifying the calling machine name.
	 * @param sid The session identifier, used for tracking purposes.
	 * @param callerIp The IP address when requested by remote services.
	 * @param market Market code to be used when requested by remote services.
	 * @param site Site code to be used when requested by remote services.
	 * @param currencyCode The code of the currency.
	 * @param currencySymbol The symbol of the currency, encoded as HTML entity.
	 * @param locale The locale to use for the session.
	 * @param tripId The code of the trip (PNR code, ticket number 
	 * 				 or MilleMiglia customer number).
	 * @param passengerName The name of the passenger related to the tripId
	 * 						accessing the CheckIn.
	 * @param passengerSurname The surname of the passenger related to the
	 * 						   tripId accessing the CheckIn.
	 */
	public CheckinSessionContext initializeSession(SlingHttpServletRequest request, I18n i18n,
			String serviceClient, String machineName, String sid, String site,
			String market, String currencyCode, String currencySymbol,
			Locale locale, String callerIp, String tripId, String passengerName,
			String passengerSurname, String MMcode, String pin,
			String searchType, NumberFormat currentNumberFormat, List<String> newSeats) throws CheckinPnrInformationNotFoundException {

		logger.debug("initializeSession");

		CheckinSessionContext ctx;

		// create a new CheckIn session
		ctx = new CheckinSessionContext();
		ctx.i18n = i18n;
		ctx.serviceClient = serviceClient;
		ctx.machineName = machineName;
		ctx.sid = sid;
		ctx.callerIp = callerIp;
		ctx.market = "gb".equalsIgnoreCase(market) ||
				"en".equalsIgnoreCase(market) ? market.toUpperCase() : market;
				ctx.site = site;
				ctx.currencyCode = currencyCode;
				ctx.currencySymbol = currencySymbol;
				ctx.locale = locale;
				ctx.searchType = searchType;
				ctx.tripId = tripId;
				ctx.passengerName = passengerName;
				ctx.passengerSurname = passengerSurname;
				ctx.currentNumberFormat = currentNumberFormat;
				ctx.pnrListData = new ArrayList<>();
				ctx.cartTotalAmount = new BigDecimal(0);
				ctx.insuranceAddedToCart = false;
				ctx.ancillaryPaymentSuccess = false;
				ctx.insurancePaymentSuccess = false;
				ctx.newSeats = newSeats;
				ctx.selectedFlights = new ArrayList<>();
				if (!CheckinConstants.CHECKIN_SEARCH_PNR.equals(ctx.searchType)) setMmcustomerData(request, ctx);
				//		ctx.MMcode = MMcode;
				//		ctx.pin = pin;
				refreshRouteData(ctx);

				return ctx;

	}

	/**
	 * Internal utility method to load the Checkin route data into context.
	 */
	public void refreshRouteData(CheckinSessionContext ctx)
			throws CheckinPnrInformationNotFoundException {
		String tid = IDFactory.getTid();

		// Rimossa chiamata validateMMUser, le info su nome e cognome vengono prese dal customer

		// prepare pnr info request
		RetriveMmbPnrInformationRequest pnrRequest =
				new RetriveMmbPnrInformationRequest();

		pnrRequest.setTid(tid);
		pnrRequest.setSid(ctx.sid);
		pnrRequest.setBaseInfo(createBaseInfo(ctx));
		pnrRequest.setLocale(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()) + "_" + ctx.market);
		pnrRequest.setClient(ctx.serviceClient);
		pnrRequest.setTripId(calculateTripId (ctx));
		pnrRequest.setName(ctx.passengerName);
		pnrRequest.setLastName(ctx.passengerSurname);

		// execute pnr info request
		RetrievePnrInformationCheckinResponse pnrResponse = null;
		try {
			pnrResponse = checkInDelegate.retrievePnrInformation(pnrRequest);
			ctx.isBus = pnrResponse.isBusCarrier();
		} catch (Exception e) {
			String reason = getReason(ctx, e);
			if (reason != null) {
				throw new CheckinPnrInformationNotFoundException(reason);
			} else {
				throw e;
			}
		}

		if (pnrResponse.getRoutesList() == null ||
				pnrResponse.getRoutesList().size() == 0 ||
				pnrResponse.getRoutesList().get(0) == null) {
			throw new CheckinPnrInformationNotFoundException("searchError");
		}

		// read pnr data into context
		ctx.pnr = pnrResponse.getPnr();
		ctx.eTicket = pnrResponse.geteTicket();
		ctx.passengerName = pnrResponse.getName();
		ctx.passengerSurname = pnrResponse.getLastname();
		ctx.routesList = pnrResponse.getRoutesList();

		// E' possibile che il servizio non restituisca il passeggero 
		// (caso in cui non ci sono voli per cui e' possibile effettuare il check-in) 
		/*if (pnrResponse.getPassengersList() == null ||
				pnrResponse.getPassengersList().isEmpty()) {
			throw new RuntimeException("passengers list is empty");
		}*/
		if (pnrResponse.getPassengersList() == null || pnrResponse.getPassengersList().isEmpty()) {
			logger.info("PNR {} - Lista passeggeri nella GetRouteList vuota", ctx.pnr);
		}

		// add only first passenger; other passengers will be retrieved with getPassengerList service
		ctx.passengersRouteList= new ArrayList<CheckinPassengerData>();
		ctx.passengersRouteList=pnrResponse.getPassengersList();
	}

	/**
	 * Internal utility method to create the base info data for service calls.
	 */
	private MmbRequestBaseInfoData createBaseInfo(CheckinSessionContext ctx) {
		MmbRequestBaseInfoData baseInfoData = new MmbRequestBaseInfoData();
		baseInfoData.setClientIp(ctx.callerIp);
		baseInfoData.setSessionId(ctx.sid);
		baseInfoData.setSiteCode(ctx.market);
		return baseInfoData;
	}

	private String calculateTripId (CheckinSessionContext ctx) {
		if (CheckinConstants.CHECKIN_SEARCH_PNR.equals(ctx.searchType)) {
			return ctx.tripId;
		} else {
			String moddedTrip = ctx.MMcode;
			while (moddedTrip.length() < CheckinConstants.MMCODE_TRIPID_LENGHT) {
				moddedTrip = CheckinConstants.MMCODE_TRIPID_PAD_CHARACTER +
						moddedTrip;
			}
			return CheckinConstants.MMCODE_TRIPID_START_PATH + moddedTrip;
		}
	}

	private void setMmcustomerData(SlingHttpServletRequest request, CheckinSessionContext ctx){
		MMCustomerProfileData customer =  AlitaliaUtils.getAuthenticatedUser(request);
		if(customer != null){
			String decryptedPin = customerProfileManager.decodeAndDecryptProperty(customer.getCustomerPinCode());
			ctx.MMcode = StringUtils.stripStart(customer.getCustomerNumber(), "0");
			ctx.pin = decryptedPin;
			ctx.passengerName = customer.getCustomerName();
			ctx.passengerSurname = customer.getCustomerSurname();
		}
	}

	private String getReason(CheckinSessionContext ctx, Exception e) {
		if (e instanceof BehaviourException &&
				e.getCause() != null && e.getCause() instanceof AlitaliaServiceException &&
				e.getCause().getCause() != null && e.getCause().getCause() instanceof WSClientException &&
				e.getCause().getCause().getCause() != null) {

			String reason = e.getCause().getCause().getCause().getMessage();
			if (reason != null) {
				String messageKey = "checkin.common." + reason + ".error";
				if (!ctx.i18n.get(messageKey).equals(messageKey)) {
					return reason;
				}
			}

		}
		return null;
	}

	public void getPassengerList(CheckinSessionContext ctx, CheckinSearchReasonEnum reason) {
		PassengerListCheckinRequest passengerRequest =
				new PassengerListCheckinRequest();

		String tid = IDFactory.getTid();

		passengerRequest.setTid(tid);
		passengerRequest.setSid(ctx.sid);
		passengerRequest.setBaseInfo(createBaseInfo(ctx));
		passengerRequest.setClient(ctx.serviceClient);
		passengerRequest.setPnr(ctx.selectedRoute.getPnr());
		passengerRequest.setName(ctx.passengerName);
		passengerRequest.setLastName(ctx.passengerSurname);
		passengerRequest.setSelectedRoute(ctx.selectedRoute);
		passengerRequest.setReason(reason);
		if(CheckinSearchReasonEnum.UNDO_CHECK_IN.equals(reason)){
			passengerRequest.setIsFullUndoList(true);
		}

		PassengerListCheckinResponse passengerResponse;
		passengerResponse = checkInDelegate.getPassengerList(passengerRequest);
		ctx.passengers=new ArrayList<CheckinPassengerData>();
		int passengersRouteIndex = computePassengersRouteIndex(ctx);

		if (ctx.passengersRouteList == null || ctx.passengersRouteList.isEmpty()) {
			logger.error("PNR {} - Lista passeggeri nella GetRouteList vuota", ctx.pnr);
		}

		ctx.pnr = passengerResponse.getPnr();

		ctx.passengers.add(0, ctx.passengersRouteList.get(passengersRouteIndex));

		// add passengers only if passengers size == 1
		if (passengerResponse.getPassengers() != null &&
				!passengerResponse.getPassengers().isEmpty()) {
			ctx.passengers.addAll(passengerResponse.getPassengers());
		}
		logger.debug("[GET PASSENGERS LIST] passengers = " + ctx.passengers.size());
	}

	private int computePassengersRouteIndex (CheckinSessionContext ctx) {
		int passengersRouteIndex = -1;
		int flight_number = -1;
		for (CheckinRouteData route : ctx.routesList) {
			if ( passengersRouteIndex < ctx.indexRoute && 
					(null != CheckinUtils.checkValidCheckin(route, ++flight_number, "") ||
					null != CheckinUtils.checkAlreadyCheckedIn (route))) {
				passengersRouteIndex++;
			}
		}
		return passengersRouteIndex;
	}

	public void prepareSelectedRoute(String routeIndex, CheckinSessionContext ctx) {
		if (routeIndex != null) {
			try {
				int index = Integer.parseInt(routeIndex);
				if (ctx.routesList != null && 
						ctx.routesList.size() > index) { 
					ctx.indexRoute=index;
					ctx.selectedRoute = ctx.routesList.get(index);
					ctx.isSelectedRouteBus = ctx.selectedRoute.isBus();
					ctx.apisBasic = computeIsApis(ctx);
					ctx.apisPlus = computeIsApisPlus(ctx);
					if (null == ctx.pnr || ctx.pnr.isEmpty()) {
						//se ho effettuato una ricerca con la login devo valorizzare ctx.pnr con il pnr della selectedroute
						ctx.pnr = ctx.selectedRoute.getPnr();
					}
				}
			} catch (NumberFormatException e) {
				logger.warn("Index route parameter not valid");
			}
		}
	}

	/**
	 * Perform validation logic for the frequent flyer codes of current adult
	 * passengers data. The validation is performed through the checkin service
	 * 
	 * @param passengers The list of passengers to verify
	 * @return A subset of the adult passengers data including only passengers with error.
	 */
	public List<String> checkFrequentFlyerCodes(CheckinSessionContext ctx, List<CheckinPassengerData> passengers) {

		List<String> passengersCodesWithWrongFrequentFlyerCode = new ArrayList<String>();

		//prepare the list of passenger with frequent flyer code to be validate
		List<CheckinPassengerData> passengersWithFrequentFlyerCode = new ArrayList<CheckinPassengerData>();
		for (CheckinPassengerData passenger : passengers) {
			if (passenger.getFrequentFlyerCarrier() != null && passenger.getFrequentFlyerCarrier().getCode() != null &&
					passenger.getFrequentFlyerCarrier().getCode().equals("AZ") &&
					passenger.getFrequentFlyerCode() != null && !passenger.getFrequentFlyerCode().isEmpty()) {
				passengersWithFrequentFlyerCode.add(passenger);
			}
		}

		//First check if a passenger has a regular expression for its own FFCode.
		/*Iterator<CheckinPassengerData> iteratorPassengersWithFFCode = passengersWithFrequentFlyerCode.iterator();
		while (iteratorPassengersWithFFCode.hasNext()) {
			CheckinPassengerData passengerWithFFCode = iteratorPassengersWithFFCode.next();
			if (passengerWithFFCode.getFrequentFlyerCarrier() != null) {
				String regularExpr = passengerWithFFCode.getFrequentFlyerCarrier().getRegularExpressionValidation();
				if (regularExpr != null && !regularExpr.isEmpty()) {
					if (!passengerWithFFCode.getFrequentFlyerCode().matches(regularExpr)) {
						passengersCodesWithWrongFrequentFlyerCode.add(passengerWithFFCode.getFrequentFlyerCode());
						iteratorPassengersWithFFCode.remove();
					}
				}
			}
		}*/

		if (passengersWithFrequentFlyerCode.size() > 0) {
			MmbCheckFrequentFlyerCodesRequest request = new MmbCheckFrequentFlyerCodesRequest ();
			request.setBaseInfo(createBaseInfo(ctx));
			request.setClient(ctx.serviceClient);
			request.setSid(ctx.sid);
			request.setTid(IDFactory.getTid());

			Map<Integer, List<String>> ffCodesToCheck = new HashMap<Integer, List<String>>  ();
			int i = 0;
			for (CheckinPassengerData passenger : passengersWithFrequentFlyerCode) {
				List <String> values = new ArrayList<String> ();
				values.add(passenger.getFrequentFlyerCode());
				values.add(passenger.getName());
				values.add(passenger.getLastName());
				ffCodesToCheck.	put(i, values);
				i++;
			}

			request.setFfCodesToCheck(ffCodesToCheck);

			MmbCheckFrequentFlyerCodesResponse response = checkInDelegate.checkFFCode(request);
			if (response != null && response.getInvalidFFCodes() != null && !response.getInvalidFFCodes().isEmpty()) {
				passengersCodesWithWrongFrequentFlyerCode.addAll(response.getInvalidFFCodes());
			}
		}

		return passengersCodesWithWrongFrequentFlyerCode;

	}

	public CheckinPassenger toCheckinPassenger(CheckinPassengerData passengerData) {
		CheckinPassenger passenger = new CheckinPassenger();

		passenger.setName(passengerData.getName());
		passenger.setLastName(passengerData.getLastName());

		if (passengerData.getApisData() != null) {

			passenger.setMiddleName(passengerData.getApisData().getSecondName());
			passenger.setGender(passengerData.getApisData().getGender());

			Calendar birthDay;
			if ((birthDay = passengerData.getApisData().getBirthDate()) != null &&
					birthDay.get(Calendar.YEAR) > 1) {
				passenger.setBirthDay(String.valueOf(birthDay.get(Calendar.DAY_OF_MONTH)));
				passenger.setBirthMonth(String.valueOf(birthDay.get(Calendar.MONTH)));
				passenger.setBirthYear(String.valueOf(birthDay.get(Calendar.YEAR)));
			}

			passenger.setPassport(passengerData.getApisData().getPassportNumber());

			Calendar passportExpirationDate;
			if ((passportExpirationDate = passengerData.getApisData().getPassportExpirationDate()) != null
					&& passportExpirationDate.get(Calendar.YEAR) > 1) {
				passenger.setPassportExpiryDay(String.valueOf(passportExpirationDate.get(Calendar.DAY_OF_MONTH)));
				passenger.setPassportExpiryMonth(String.valueOf(passportExpirationDate.get(Calendar.MONTH)));
				passenger.setPassportExpiryYear(String.valueOf(passportExpirationDate.get(Calendar.YEAR)));
			}

			passenger.setNationality(passengerData.getApisData().getNationality());
			passenger.setResidency(passengerData.getApisData().getResidenceCountry());
			passenger.setDestinationCity(passengerData.getApisData().getDestinationCity());
			passenger.setDestinationZipCode(passengerData.getApisData().getDestinationZipCode());
			passenger.setDestinationAddress(passengerData.getApisData().getDestinationAddress());
			passenger.setDestinationState(passengerData.getApisData().getDestinationState());
		}

		// child
		passenger.setChild(CheckinPassengerTypeEnum.CHILD.equals(passengerData.getType()));
		// has infant
		passenger.setHasInfant(MmbLinkTypeEnum.INFANT_ACCOMPANIST.equals(passengerData.getLinkType()));
		// already checked in
		passenger.setAlreadyCheckedIn(CheckinUtils.isCheckedIn(passengerData.getStatus()));

		return passenger;
	}

	public void updatePassengerApisBasic(CheckinPassenger passenger, CheckinSessionContext ctx) {
		CheckinPassengerData passengerData = ctx.selectedPassengers.get(ctx.passengerIndex);
		CheckinApisInfoData apisData = passengerData.getApisData() != null ?
				passengerData.getApisData() : new CheckinApisInfoData();

				apisData.setNationality(passenger.getNationality());
				if (passenger.getBirthDay() != null &&
						passenger.getBirthMonth() != null &&
						passenger.getBirthYear() != null) {

					Calendar birthDate = Calendar.getInstance();
					birthDate.setLenient(true);
					birthDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(passenger.getBirthDay()));
					birthDate.set(Calendar.MONTH, Integer.parseInt(passenger.getBirthMonth()));
					birthDate.set(Calendar.YEAR, Integer.parseInt(passenger.getBirthYear()));
					apisData.setBirthDate(birthDate);
				}
				apisData.setGender(passenger.getGender());
				apisData.setPassportType(PassportTypeEnum.fromValue(passenger.getDocumentType()));
				apisData.setPassportNumber(passenger.getPassport());
				if (passenger.getPassportExpiryDay() != null &&
						passenger.getPassportExpiryMonth() != null &&
						passenger.getPassportExpiryYear() != null) {
					Calendar expiryDate = Calendar.getInstance();
					expiryDate.setLenient(true);
					expiryDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(passenger.getPassportExpiryDay()));
					expiryDate.set(Calendar.MONTH, Integer.parseInt(passenger.getPassportExpiryMonth()));
					expiryDate.set(Calendar.YEAR, Integer.parseInt(passenger.getPassportExpiryYear()));
					apisData.setPassportExpirationDate(expiryDate);
				}
				passengerData.setApisData(apisData);
	}

	public void updatePassengerApisPlus(CheckinPassenger passenger, CheckinSessionContext ctx) {
		CheckinPassengerData passengerData = ctx.selectedPassengers.get(ctx.passengerIndex);
		CheckinApisInfoData apisData = passengerData.getApisData() != null ?
				passengerData.getApisData() : new CheckinApisInfoData();

				apisData.setNationality(passenger.getNationality());
				apisData.setResidenceCountry(passenger.getResidency());
				if (passenger.getName() != null && !passenger.getName().isEmpty()) {
					passengerData.setName(passenger.getName());
					apisData.setName(passenger.getName());
				}
				if (passenger.getLastName() != null && !passenger.getLastName().isEmpty()) {
					passengerData.setLastName(passenger.getLastName());
					apisData.setLastname(passenger.getLastName());
				}
				apisData.setSecondName(passenger.getMiddleName());
				passengerData.setApisData(apisData);
	}

	public void updatePassengerApisPlusAdditionalInfo(CheckinPassenger passenger, CheckinSessionContext ctx) {
		CheckinPassengerData passengerData = ctx.selectedPassengers.get(ctx.passengerIndex);
		CheckinApisInfoData apisData = passengerData.getApisData() != null ?
				passengerData.getApisData() : new CheckinApisInfoData();

				if (passenger.getBirthDay() != null &&
						passenger.getBirthMonth() != null &&
						passenger.getBirthYear() != null) {

					Calendar birthDate = Calendar.getInstance();
					birthDate.setLenient(true);
					birthDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(passenger.getBirthDay()));
					birthDate.set(Calendar.MONTH, Integer.parseInt(passenger.getBirthMonth()));
					birthDate.set(Calendar.YEAR, Integer.parseInt(passenger.getBirthYear()));
					apisData.setBirthDate(birthDate);
				}
				apisData.setGender(passenger.getGender());
				apisData.setPassportType(PassportTypeEnum.fromValue(passenger.getDocumentType()));
				apisData.setPassportNumber(passenger.getPassport());
				if (passenger.getPassportExpiryDay() != null &&
						passenger.getPassportExpiryMonth() != null &&
						passenger.getPassportExpiryYear() != null) {
					Calendar expiryDate = Calendar.getInstance();
					expiryDate.setLenient(true);
					expiryDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(passenger.getPassportExpiryDay()));
					expiryDate.set(Calendar.MONTH, Integer.parseInt(passenger.getPassportExpiryMonth()));
					expiryDate.set(Calendar.YEAR, Integer.parseInt(passenger.getPassportExpiryYear()));
					apisData.setPassportExpirationDate(expiryDate);
				}
				passengerData.setApisData(apisData);
	}

	public void updatePassengerApisPlusAdditionalInfoCase2(CheckinPassenger passenger, CheckinSessionContext ctx) {

		updatePassengerApisPlusAdditionalInfo(passenger, ctx);

		CheckinPassengerData passengerData = ctx.selectedPassengers.get(ctx.passengerIndex);
		CheckinApisInfoData apisData = passengerData.getApisData() != null ?
				passengerData.getApisData() : new CheckinApisInfoData();

				apisData.setDestinationCity(passenger.getDestinationCity());
				apisData.setDestinationZipCode(passenger.getDestinationZipCode());
				apisData.setDestinationAddress(passenger.getDestinationAddress());
				apisData.setDestinationState(passenger.getDestinationState());
	}

	public void updatePassengerApisPlusAdditionalInfoCase3(CheckinPassenger passenger, CheckinSessionContext ctx) {

		updatePassengerApisPlusAdditionalInfoCase2(passenger, ctx);

		CheckinPassengerData passengerData = ctx.selectedPassengers.get(ctx.passengerIndex);
		CheckinApisInfoData apisData = passengerData.getApisData() != null ?
				passengerData.getApisData() : new CheckinApisInfoData();

				apisData.setExtraDocumentType(DocumentTypeEnum.fromValue(passenger.getExtraDocumentType()));
				apisData.setExtraDocumentNumber(passenger.getExtraDocumentNumber());

				if (passenger.getExtraDocumentExpiryDay() != null &&
						passenger.getExtraDocumentExpiryMonth() != null &&
						passenger.getExtraDocumentExpiryYear() != null) {
					Calendar expiryDate = Calendar.getInstance();
					expiryDate.setLenient(true);
					expiryDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(passenger.getExtraDocumentExpiryDay()));
					expiryDate.set(Calendar.MONTH, Integer.parseInt(passenger.getExtraDocumentExpiryMonth()));
					expiryDate.set(Calendar.YEAR, Integer.parseInt(passenger.getExtraDocumentExpiryYear()));
					apisData.setExtraDocumentExpirationDate(expiryDate);
				}

				if(passenger.getExtraDocumentType().equals("Visa"))
					apisData.setExtraDocumentIssuingCountry(passenger.getExtraDocumentDeliveryCountry());
				else if(passenger.getExtraDocumentType().equals("GreenCard")){
					apisData.setExtraDocumentIssuingState(passenger.getExtraDocumentDeliveryCountry());
				}

	}

	public void updatePassengerApisPlusAdditionalInfoCase4(CheckinPassenger passenger, CheckinSessionContext ctx) {

		updatePassengerApisPlusAdditionalInfoCase3(passenger, ctx);

		CheckinPassengerData passengerData = ctx.selectedPassengers.get(ctx.passengerIndex);
		CheckinApisInfoData apisData = passengerData.getApisData() != null ?
				passengerData.getApisData() : new CheckinApisInfoData();

				apisData.setExtraDocumentType(DocumentTypeEnum.fromValue(passenger.getExtraDocumentType()));
				apisData.setExtraDocumentNumber(passenger.getExtraDocumentNumber());

				if (passenger.getExtraDocumentExpiryDay() != null &&
						passenger.getExtraDocumentExpiryMonth() != null &&
						passenger.getExtraDocumentExpiryYear() != null) {
					Calendar expiryDate = Calendar.getInstance();
					expiryDate.setLenient(true);
					expiryDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(passenger.getExtraDocumentExpiryDay()));
					expiryDate.set(Calendar.MONTH, Integer.parseInt(passenger.getExtraDocumentExpiryMonth()));
					expiryDate.set(Calendar.YEAR, Integer.parseInt(passenger.getExtraDocumentExpiryYear()));
					apisData.setExtraDocumentExpirationDate(expiryDate);
				}

				if(passenger.getExtraDocumentType().equals("GreenCard")){
					apisData.setExtraDocumentIssuingState(passenger.getExtraDocumentDeliveryCountry());
				}

	}

	//	public void updatePassengersFirstStep(CheckinPassengersForm form, CheckinSessionContext ctx) {
	//		
	//		for (CheckinPassenger passenger : form.getPassengers()) {
	//			int id = 0;
	//			if (passenger.getIndex() < ctx.selectedPassengers.size()) {
	//				CheckinPassengerData passengerData = ctx.selectedPassengers.get(passenger.getIndex());
	//				passengerData.setFrequentFlyerCode(passenger.getFrequentFlyerCode());
	//				
	//				if(passengerData.getFrequentFlyerCarrier() != null){
	//					MmbFrequentFlyerCarrierData frequentFlyerCarrier = new MmbFrequentFlyerCarrierData();
	//					frequentFlyerCarrier.setCode(passenger.getFrequentFlyerType());
	//					frequentFlyerCarrier.setId(id++);
	//					passengerData.setFrequentFlyerCarrier(frequentFlyerCarrier);
	//				}
	//			}
	//		}
	//	}

	public boolean computeIsApis(CheckinSessionContext ctx) {
		//		Set<String> countryCodesApis = bookingSession.createSetFromProperty(BookingSession.APIS_COUNTRY_CODES_PROPERTY);
		//		Set<String> countryCodesToCheck = new HashSet<String>();
		//		if (ctx.selectedRoute != null) {
		//			List<CheckinFlightData> flights = CheckinUtils.getFlightsList(ctx.selectedRoute);
		//			countryCodesToCheck.add(flights.get(0).getFrom().getCountryCode());
		//			countryCodesToCheck.add(flights.get(flights.size() - 1).getTo().getCountryCode());
		//		}
		//		countryCodesToCheck.retainAll(countryCodesApis);
		//		return (countryCodesToCheck.size() > 0);
		if (ctx.selectedRoute != null && ctx.selectedRoute.getApisTypeRequired() != null) {
			if (MmbApisTypeEnum.BASE.equals(ctx.selectedRoute.getApisTypeRequired())){
				return true;
			}
		}
		return false;
	}

	public boolean computeIsApisPlus(CheckinSessionContext ctx) {
		//		Set<String> countryCodesSecureFlight = bookingSession.createSetFromProperty(BookingSession.SECUREFLIGHT_COUNTRY_CODES_PROPERTY);
		//		Set<String> countryCodesToCheck = new HashSet<String>();
		//		if (ctx.selectedRoute != null) {
		//			List<CheckinFlightData> flights = CheckinUtils.getFlightsList(ctx.selectedRoute);
		//			countryCodesToCheck.add(flights.get(0).getFrom().getCountryCode());
		//			countryCodesToCheck.add(flights.get(flights.size() - 1).getTo().getCountryCode());
		//		}
		//		countryCodesToCheck.retainAll(countryCodesSecureFlight);
		//		return (countryCodesToCheck.size() > 0);
		if (ctx.selectedRoute != null && ctx.selectedRoute.getApisTypeRequired() != null) {
			if (MmbApisTypeEnum.PLUS.equals(ctx.selectedRoute.getApisTypeRequired())){
				return true;
			}
		}
		return false;
	}

	public String[] getDays() {
		String[] days = new String[31];
		for (int i = 0; i < 31; i++) {
			days[i] = String.valueOf(i + 1);
		}
		return days;
	}

	public String[] getMonths() {
		String[] months = new String[12];
		for (int i = 0; i < 12; i++) {
			months[i] = String.valueOf(i);
		}
		return months;
	}

	public String[] getBirthYears() {
		String[] years = new String[100];
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int k = 0;
		for (int i = currentYear; i > (currentYear - 100); i--) {
			years[k++] = String.valueOf(i);
		}
		return years;
	}

	public String[] getExpiryYears() {
		String[] years = new String[12];
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int k = 0;
		for (int i = currentYear; i <= (currentYear + 11); i++) {
			years[k++] = String.valueOf(i);
		}
		return years;
	}

	public List<CheckinCountryData> getCountries(CheckinSessionContext ctx) {

		String languageCode = AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag());
		String siteCode = ctx.site;

		GetCountryRequest getCountryRequest = new GetCountryRequest(IDFactory.getTid(), ctx.sid);

		getCountryRequest.setClient(ctx.serviceClient);
		getCountryRequest.setBaseInfo(createBaseInfo(ctx));

		getCountryRequest.setLanguageCode(languageCode);
		getCountryRequest.setSiteCode(siteCode);

		List<CheckinCountryData> countriesResponse = checkInDelegate.getCountries(getCountryRequest).getCountries();

		return countriesResponse;
	}

	public String[] getCountriesISOCode(CheckinSessionContext ctx) {

		String[] countriesISOCode = null;

		String languageCode = AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag());
		String siteCode = ctx.site;

		GetCountryRequest getCountryRequest = new GetCountryRequest(IDFactory.getTid(), ctx.sid);

		getCountryRequest.setClient(ctx.serviceClient);
		getCountryRequest.setBaseInfo(createBaseInfo(ctx));

		getCountryRequest.setLanguageCode(languageCode);
		getCountryRequest.setSiteCode(siteCode);

		List<CheckinCountryData> countriesResponse = checkInDelegate.getCountries(getCountryRequest).getCountries();
		countriesISOCode = new String[countriesResponse.size()];

		int i = 0;
		for(CheckinCountryData countryItem : countriesResponse){
			if(countryItem != null)
				countriesISOCode[i++] = countryItem.getCountryISOCode();
		}

		return countriesISOCode;
	}

	public List<CheckinSeatMapData> getSeatMap(CheckinFlightData flight, CheckinSessionContext ctx,
			MmbCompartimentalClassEnum compartimentalClass, boolean isComfort) {

		if (flight.isBus()) {
			return null;
		}
		if ("AZ".equals(flight.getOperatingCarrier())) {
			GetSeatMapCheckinRequest request = new GetSeatMapCheckinRequest();
			request.setSid(ctx.sid);
			request.setTid(IDFactory.getTid());
			request.setMarket(ctx.market);

			flight.setHasComfortSeat(isComfort);
			request.setFlight(flight);
			request.setBaseInfo(createBaseInfo(ctx));
			//request.setEticket();
			request.setClient(ctx.serviceClient);
			request.setCompartimentalClass(compartimentalClass);

			GetSeatMapCheckinResponse response = checkInDelegate.getSeatMap(request);
			return response.getSeatMap();
		}
		return null;
	}

	public Map<String, CheckinSeatMapFlight> getAllSeatMaps(CheckinSessionContext ctx, boolean isComfort) {

		List<CheckinPassengerData> checkedInPassengers = 
				CheckinUtils.getCheckedInPassengers(ctx.passengers);

		Map<String, CheckinSeatMapFlight> seatMaps = 
				getSeatMapsFiltered(ctx, checkedInPassengers
						, MmbCompartimentalClassEnum.UNKNOWN, isComfort);
		return seatMaps;
	}

	private Map<String, CheckinSeatMapFlight> getSeatMapsFiltered(CheckinSessionContext ctx
			, List<CheckinPassengerData> passengers
			, MmbCompartimentalClassEnum seatClassAdmitted
			, boolean isComfort) {

		Map<String, CheckinSeatMapFlight> seatMaps = new HashMap<String, CheckinSeatMapFlight>();

		List<CheckinFlightData> flights = CheckinUtils.getFlightsList(ctx.selectedRoute);

		String previousFlightNumber = null;

		Map<String, Set<String>> assignedSeats = getAssignedSeats(ctx, passengers);

		for (CheckinFlightData flight : flights) {
			CheckinSeatMapFlight seatMapWrapper = new CheckinSeatMapFlight(
					flight.getFlightNumber(), previousFlightNumber);
			seatMapWrapper.setBus(flight.isBus());

			for (CheckinPassengerData passengerData : passengers) {

				CheckinCouponData coupon = 
						CheckinUtils.getCouponByFlightNumber(passengerData.getCoupons()
								, flight.getFlightNumber());

				if (coupon != null)  {
					MmbCompartimentalClassEnum seatClass;
					if(!MmbCompartimentalClassEnum.UNKNOWN.equals(seatClassAdmitted)){
						seatClass = seatClassAdmitted;
					} else{
						seatClass = coupon.getSeatClass();
					}
					/*Only class admitted by flight are requested*/
					//					if(flight.getCabins().contains(seatClass)){
					if (seatMapWrapper.getSeatMaps().get(seatClass.toString()) == null) {
						List<CheckinSeatMapData> seatMap = getSeatMap(flight, ctx, seatClass, isComfort);
						if(seatMap != null && seatMap.size() > 0)
							seatMapWrapper.getSeatMaps().put(seatClass.toString()
									, new CheckinSeatMapRender(seatMap, assignedSeats.get(flight.getFlightNumber())));
					}
					//					}
				}
			}
			previousFlightNumber = flight.getFlightNumber();
			seatMaps.put(flight.getFlightNumber(), seatMapWrapper);
		}
		return seatMaps;
	}

	public String[] getAmericanStates(CheckinSessionContext ctx) {

		String[] americanStates = null;

		BaseRequest getAmericanStatesRequest = new BaseRequest(IDFactory.getTid(), ctx.sid);

		List<MmbAmericanStatesData> americanStatesResponse = checkInDelegate.getAmericanStates(getAmericanStatesRequest).getAmericanStates();
		americanStates = new String[americanStatesResponse.size()];

		int i = 0;
		for(MmbAmericanStatesData stateItem : americanStatesResponse){
			if(stateItem != null)
				americanStates[i++] = stateItem.getStateCode();
		}

		return americanStates;
	}

	/**
	 * Set Boarding Pass
	 * @param ctx
	 */
	public void setBoardingPass(CheckinSessionContext ctx) {
		logger.debug("[CheckinSession] setBoardingPass");

		List<String> aptCodes = new ArrayList<String>();

		CheckinRouteData checkinRouteData = ctx.selectedRoute;
		List<CheckinFlightData> flightsList =
				CheckinUtils.getFlightsList(checkinRouteData);

		if(flightsList != null && !flightsList.isEmpty()){

			if (CheckinUtils.isInterline(checkinRouteData)) {

				// In caso di voli interline vanno aggiunti solo quelli della prima tratta (requisito IT)
				aptCodes.add(flightsList.get(0).getFrom().getCode());
				aptCodes.add(flightsList.get(0).getTo().getCode());

			} else {
				aptCodes.add(flightsList.get(0).getFrom().getCode());
				aptCodes.add(flightsList.get(flightsList.size() - 1).getTo().getCode());
			}
		}
		// get permissions
		WebCheckinBPPermissionRequest webCheckinBPPermissionRequest = 
				new WebCheckinBPPermissionRequest();
		webCheckinBPPermissionRequest.setTid(IDFactory.getTid());
		webCheckinBPPermissionRequest.setSid(ctx.sid);
		webCheckinBPPermissionRequest.setClient(ctx.serviceClient);
		webCheckinBPPermissionRequest.setBaseInfo(createBaseInfo(ctx));
		webCheckinBPPermissionRequest.setAptCodes(aptCodes);
		WebCheckinBPPermissionResponse webCheckinBPPermissionResponse =
				checkInDelegate.getBoardingPassPermissions(
						webCheckinBPPermissionRequest);

		ctx.boardingPassHasPDF =
				webCheckinBPPermissionResponse.getIsBPPdfEnabled();
		ctx.boardingPassHasEmail =
				webCheckinBPPermissionResponse.getIsBPViaEmailEnabled();
		ctx.boardingPassHasSMS =
				webCheckinBPPermissionResponse.getIsBPViaSmsEnabled();
		ctx.boardingPassIsDelayed =
				webCheckinBPPermissionResponse.getIsDelayedDeliveryTypeEnabled();

	}

	/**
	 * Get Boarding Pass
	 * @param ctx
	 */
	public void getBoardingPass(CheckinSessionContext ctx) {
		logger.debug("[CheckinSession] getBoardingPass");

		// get boarding pass
		BoardingPassCheckinRequest boardingPassCheckinRequest =
				new BoardingPassCheckinRequest();
		boardingPassCheckinRequest.setBaseInfo(createBaseInfo(ctx));
		boardingPassCheckinRequest.setClient(ctx.serviceClient);
		boardingPassCheckinRequest.setPassengers(
				CheckinUtils.getCheckedInPassengers(ctx.passengers));
		boardingPassCheckinRequest.setSelectedRoute(ctx.selectedRoute);
		boardingPassCheckinRequest.setSid(ctx.sid);
		boardingPassCheckinRequest.setTid(IDFactory.getTid());
		BoardingPassCheckinResponse boardingPassCheckinResponse =
				checkInDelegate.getBoardingPass(boardingPassCheckinRequest);

		// update passengers into SESSION
		ctx.passengers = boardingPassCheckinResponse.getPassengers();
		logger.debug("[Passenger null]" + Boolean.toString(ctx.passengers == null));


	}

	/**
	 * Send Cumulative Email
	 * @param ctx
	 */
	private Boolean sendCumulativeEmail(CheckinSessionContext ctx,
			String emailBody, String emailSubject, List<String> receiverMails,
			byte[] attachment, String attachmentName) {

		// send cumulative email reminder
		SendReminderCheckinRequest sendReminderCheckinRequest =
				new SendReminderCheckinRequest();
		sendReminderCheckinRequest.setAttachment(attachment);
		sendReminderCheckinRequest.setAttachmentName(attachmentName);
		sendReminderCheckinRequest.setBaseInfo(createBaseInfo(ctx));
		sendReminderCheckinRequest.setClient(ctx.serviceClient);
		sendReminderCheckinRequest.setEmailBody(emailBody);
		sendReminderCheckinRequest.setEmailSubject(emailSubject);
		sendReminderCheckinRequest.setReceiverMails(receiverMails);
		sendReminderCheckinRequest.setSid(ctx.sid);
		sendReminderCheckinRequest.setTid(IDFactory.getTid());

		SendReminderCheckinResponse sendReminderCheckinResponse =
				checkInDelegate.sendReminder(sendReminderCheckinRequest);

		return sendReminderCheckinResponse.getSendReminderResult();

	}

	/**
	 * Send Cumulative Email
	 * @param ctx
	 */
	private Boolean sendBoardingPass(CheckinSessionContext ctx,
			String emailBody, String emailSubject, String smsBodyText,
			List<CheckinPassengerData> passengers, byte[] attachment, String attachmentName) {

		// send cumulative email reminder
		SendBoardingPassCheckinRequest sendBoardingPassCheckinRequest =
				new SendBoardingPassCheckinRequest();
		sendBoardingPassCheckinRequest.setAttachment(attachment);
		sendBoardingPassCheckinRequest.setAttachmentName(attachmentName);
		sendBoardingPassCheckinRequest.setBaseInfo(createBaseInfo(ctx));
		sendBoardingPassCheckinRequest.setClient(ctx.serviceClient);
		sendBoardingPassCheckinRequest.setEmailBody(emailBody);
		sendBoardingPassCheckinRequest.setEmailSubject(emailSubject);
		sendBoardingPassCheckinRequest.setSmsBodyText(smsBodyText);
		sendBoardingPassCheckinRequest.setPassengers(passengers);
		sendBoardingPassCheckinRequest.setSid(ctx.sid);
		sendBoardingPassCheckinRequest.setTid(IDFactory.getTid());

		SendBoardingPassCheckinResponse sendBoardingPassCheckinResponse =
				checkInDelegate.sendBoardingPass(sendBoardingPassCheckinRequest);

		return sendBoardingPassCheckinResponse.getSendBoardingPassResult();

	}



	/**
	 * Send email!
	 * @param ctx
	 * @param resource
	 * @param receiverMails
	 * @param emailBodyTemplate
	 * @param emailSubjectLabel
	 * @param attachment
	 * @param attachmentName
	 * @return
	 */
	public Boolean sendEmail(CheckinSessionContext ctx, Resource resource,
			List<String> receiverMails, String emailBodyTemplate,
			String emailSubjectLabel, byte[] attachment,
			String attachmentName, String[] placeholders, String[] values, 
			String type, List<CheckinPassengerData> passengers) {

		// email body
		String emailBody = CheckinUtils.getMailBody(
				resource, emailBodyTemplate,
				true, resolverFactory, requestResponseFactory,
				requestProcessor);

		emailBody = AlitaliaUtils.replacePlaceholderBodyMail(emailBody,
				placeholders, values);

		// email subject
		String emailSubject = ctx.i18n.get(emailSubjectLabel);

		// if any
		if (CheckinConstants.CHECKIN_MAIL_REMINDER.equals(type)
				&& 0 < receiverMails.size() ) {

			// send email reminder to all receiver
			return sendCumulativeEmail(ctx, emailBody, emailSubject,
					receiverMails, attachment,
					ctx.i18n.get(attachmentName) + ".pdf");
		}

		else if (CheckinConstants.CHECKIN_MAIL_BP.equals(type)
				&& 0 < passengers.size()) {

			// send boarding pass email
			String smsBodyText = CheckinConstants.CHECKIN_SMS_TEXT;
			return sendBoardingPass(ctx, emailBody, emailSubject,
					smsBodyText, passengers, attachment,
					ctx.i18n.get(attachmentName) + ".pdf");
		}

		return null;

	}

	/**
	 * Retrieve Phone Prefixes
	 * @param ctx
	 * @return RetrievePhonePrefixResponse
	 */
	public RetrievePhonePrefixResponse retrievePhonePrefixes(
			CheckinSessionContext ctx) {

		// prepare phone prefixes
		GetCountryRequest getCountryRequest =
				new GetCountryRequest(IDFactory.getTid(), ctx.sid);
		getCountryRequest.setBaseInfo(createBaseInfo(ctx));
		getCountryRequest.setClient(ctx.serviceClient);
		getCountryRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		getCountryRequest.setSiteCode(ctx.site);

		GetCountryResponse countryResponse =
				checkInDelegate.getCountries(getCountryRequest);

		RetrievePhonePrefixResponse phonePrefixesListResponse =
				checkInDelegate.getPhonePrefixes(getCountryRequest);
		for (PhonePrefixData phonePrefixData :
			phonePrefixesListResponse.getPhonePrefix()) {
			for (CheckinCountryData countryData : countryResponse.getCountries()) {
				if (countryData.getId().equals(
						Integer.parseInt(phonePrefixData.getCode()))) {
					phonePrefixData.setDescription(countryData.getCountryCode());
				}
			}
		}

		return phonePrefixesListResponse;

	}

	/**
	 * Perform Checkin
	 * @param ctx
	 */
	public void performCheckin(CheckinSessionContext ctx) throws CheckinFailedException{

		List<CheckinPassengerData> passengersList = ctx.passengers;
		List<CheckinPassengerData> selectedPassengersList = ctx.selectedPassengers;

		for(CheckinPassengerData passenger : passengersList){
			if(passenger != null && selectedPassengersList.contains(passenger)){
				passenger.setSelected(true);
			}
		}

		String pnr = ctx.pnr;
		CheckinRouteData selectedRoute = ctx.selectedRoute;

		WebCheckinRequest webCheckinRequest = 
				new WebCheckinRequest();
		webCheckinRequest.setTid(IDFactory.getTid());
		webCheckinRequest.setSid(ctx.sid);
		webCheckinRequest.setClient(ctx.serviceClient);
		webCheckinRequest.setBaseInfo(createBaseInfo(ctx));
		webCheckinRequest.setPassengersList(passengersList);
		webCheckinRequest.setPnr(pnr);
		webCheckinRequest.setSelectedRoute(selectedRoute);

		WebCheckinResponse webCheckinResponse =
				checkInDelegate.checkIn(webCheckinRequest);

		ctx.passengers = webCheckinResponse.getPassengers();
		logger.debug("[PERFORM CHECKIN] passengers = " + ctx.passengers.size());
		/* Controlla se ci sono passeggeri selezionati per cui il checkin non è andato a buon fine e li aggiunge
		   alla lista notCheckedInPassengers */
		for(CheckinPassengerData selectedPassenger : ctx.selectedPassengers){
			Integer idSelectedPassenger = selectedPassenger.getId();
			Integer idPassenger = null;
			CheckinPassengerData passenger = null;
			Iterator<CheckinPassengerData> it = ctx.passengers.iterator();
			while(it.hasNext()){
				passenger = it.next();
				idPassenger = passenger.getId();
				if(idPassenger != null && idPassenger.equals(idSelectedPassenger)){
					break;
				}
			}
			ctx.notCheckedInPassengers = new ArrayList<CheckinPassengerData>();
			if(!CheckinUtils.isCheckedIn(passenger.getStatus())){
				ctx.notCheckedInPassengers.add(passenger);
			}
		}
		if (ctx.selectedPassengers.size() != ctx.notCheckedInPassengers.size()) {
			prepareAncillaryData(ctx);
		}
		ctx.checkinDone = true;
	}

	/**
	 * Undo Checkin
	 * @param ctx
	 */
	public void undoCheckin(CheckinSessionContext ctx, boolean allPassengers) {

		WebCheckinUndoManyCheckinRequest webCheckinUndoManyRequest = 
				new WebCheckinUndoManyCheckinRequest();
		webCheckinUndoManyRequest.setTid(IDFactory.getTid());
		webCheckinUndoManyRequest.setSid(ctx.sid);
		webCheckinUndoManyRequest.setClient(ctx.serviceClient);
		webCheckinUndoManyRequest.setBaseInfo(createBaseInfo(ctx));
		webCheckinUndoManyRequest.setRoute(ctx.selectedRoute);

		List<UndoCheckinPassengerData> undoPassengers = new ArrayList<UndoCheckinPassengerData>();
		List<CheckinPassengerData> passengers = allPassengers ? ctx.passengers : ctx.selectedPassengers;
		for (CheckinPassengerData passenger : passengers) {

			UndoCheckinPassengerData undoPassenger = new UndoCheckinPassengerData ();
			undoPassenger.setEticket(passenger.getEticket());
			undoPassenger.setLastname(passenger.getLastName());
			undoPassenger.setName(passenger.getName());

			undoPassengers.add(undoPassenger);
		}

		webCheckinUndoManyRequest.setPassengers(undoPassengers);

		// TODO verificare response del servizio
		@SuppressWarnings("unused")
		WebCheckinUndoManyCheckinResponse webCheckinUndoManyResponse =
		checkInDelegate.undoManyCheckin(webCheckinUndoManyRequest);

	}

	/**
	 * Get Frequent Flyer List
	 * @param ctx
	 */
	public List<FrequentFlyerTypeData> getFrequentFlyersList(CheckinSessionContext ctx) {

		String languageCode = AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag());
		String siteCode = ctx.site;

		GetCountryRequest getCountryRequest = new GetCountryRequest(IDFactory.getTid(), ctx.sid);

		getCountryRequest.setClient(ctx.serviceClient);
		getCountryRequest.setBaseInfo(createBaseInfo(ctx));

		getCountryRequest.setLanguageCode(languageCode);
		getCountryRequest.setSiteCode(siteCode);

		return checkInDelegate.getFrequentFlyers(getCountryRequest).getFrequentFlyers();

	}

	/**
	 * It retrive the new routeList for the new passenger
	 * @param ctx
	 * @param passengerName
	 * @param passengerSurname
	 * @param eTicket
	 * @param tid 
	 * @throws CheckinPnrInformationNotFoundException 
	 */
	/*public NewPassengerInfo retriveRouteListForPassengerByEticket(
			CheckinSessionContext ctx, String passengerName,
			String passengerSurname, String eTicket, String tid) throws CheckinPnrInformationNotFoundException {
		RetriveMmbPnrInformationRequest pnrRequest =
				new RetriveMmbPnrInformationRequest(tid,ctx.sid);

		pnrRequest.setBaseInfo(createBaseInfo(ctx));
		pnrRequest.setLocale(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()) + "_" + ctx.market);
		pnrRequest.setClient(ctx.serviceClient);
		pnrRequest.setTripId(eTicket);
		pnrRequest.setName(passengerName);
		pnrRequest.setLastName(passengerSurname);

		// execute pnr info request
		RetrievePnrInformationCheckinResponse pnrResponse = null;
		try {
			pnrResponse = checkInDelegate.retrievePnrInformation(pnrRequest);
		} catch (Exception e) {
			String reason = getReason(ctx, e);
			if (reason != null) {
				throw new CheckinPnrInformationNotFoundException(reason);
			} else {
				throw e;
		}
		}

		if (pnrResponse.getRoutesList() == null ||
				pnrResponse.getRoutesList().size() == 0 ||
						pnrResponse.getRoutesList().get(0) == null) {
			throw new CheckinPnrInformationNotFoundException("searchError");
		}

		NewPassengerInfo newPassengerInfo = new NewPassengerInfo();
		newPassengerInfo.setPnr(pnrResponse.getPnr());
		newPassengerInfo.seteTicket(pnrResponse.geteTicket());
		newPassengerInfo.setLastname(pnrResponse.getLastname());
		newPassengerInfo.setName(pnrResponse.getName());
		newPassengerInfo.setPassengersList(pnrResponse.getPassengersList());
		newPassengerInfo.setRoutesList(pnrResponse.getRoutesList());
		return newPassengerInfo;
	}*/

	/**
	 * It check if the passenger can be added to this checkin Session
	 * @param tid 
	 * @return the specifc reason to add the passenger
	 */
	public CanAddPassengerReasonEnum canAddExtraPassenger(
			CheckinSessionContext ctx) {
		CanAddPassengerCheckinRequest addPassengerCheckinRequest = 
				new CanAddPassengerCheckinRequest();

		addPassengerCheckinRequest.setSid(ctx.sid);
		addPassengerCheckinRequest.setTid(IDFactory.getTid());
		addPassengerCheckinRequest.setBaseInfo(createBaseInfo(ctx));
		addPassengerCheckinRequest.setClient(ctx.callerIp);
		addPassengerCheckinRequest.setPassengers(ctx.passengers);
		addPassengerCheckinRequest.setPnr(ctx.pnr);

		CanAddPassengerCheckinResponse addPassengerCheckinResponse = 
				checkInDelegate.canAddPassenger(addPassengerCheckinRequest);

		if (addPassengerCheckinResponse != null) {
			return addPassengerCheckinResponse.getCanAddPassenger();
		} else {
			logger.error("addPassengerCheckinResponse is null");
		}
		return CanAddPassengerReasonEnum.MAX_GROUP_PASSENGER_REACHED;
	}


	/**
	 * It adds the newPassengerInfo to the passengers list of this checkin session
	 * @param tid 
	 * @param newPassengerInfo 
	 * @param passengerName
	 * @param passengerSurname
	 * @param eTicket
	 * @return true if the passenger was added
	 */
	public boolean addExtraPassenger(CheckinSessionContext ctx, String passengerName, 
			String passengerSurname, String eTicket) {

		WebCheckinNewPassengerRequest newPassengerRequest = 
				new WebCheckinNewPassengerRequest(IDFactory.getTid(), ctx.sid);

		newPassengerRequest.setBaseInfo(createBaseInfo(ctx));
		newPassengerRequest.setClient(ctx.callerIp);
		newPassengerRequest.setEticket(eTicket);
		newPassengerRequest.setGroupPnr(ctx.pnr);
		newPassengerRequest.setLastName(passengerSurname);
		newPassengerRequest.setName(passengerName);
		newPassengerRequest.setPassengers(ctx.passengers);
		newPassengerRequest.setSelectedRoute(ctx.selectedRoute);

		WebCheckinNewPassengerResponse checkinNewPassengerResponse = 
				checkInDelegate.getNewPassenger(newPassengerRequest);

		if (checkinNewPassengerResponse != null 
				&& checkinNewPassengerResponse.getPassenger() != null) {

			CheckinPassengerData newPassenger = checkinNewPassengerResponse.getPassenger();
			if (ctx.passengers != null) {
				ctx.passengers.add(newPassenger);
			}
			return true;
		}
		return false;
	}



	/**
	 * 
	 * @param ctx
	 * @param compartClass
	 * @return
	 */
	public List<CheckinPassengerData> getUpgradeEligiblePassengers(CheckinSessionContext ctx
			, MmbCompartimentalClassEnum CompClass, boolean includeIssued) {

		List<CheckinPassengerData> eligiblePassengers = 
				new ArrayList<CheckinPassengerData>();
		MmbAncillaryTypeEnum upgradeType = MmbAncillaryTypeEnum.UPGRADE;
		List<MmbAncillaryStatusEnum> status = new ArrayList<MmbAncillaryStatusEnum>();
		status.add(MmbAncillaryStatusEnum.AVAILABLE);
		if(includeIssued){
			status.add(MmbAncillaryStatusEnum.ISSUED);
			status.add(MmbAncillaryStatusEnum.PREVIOUSLY_ISSUED);
		}

		if(ctx.passengers != null){
			eligiblePassengers = ctx.passengers.stream().filter( pax -> /*true*/pax.getCoupons().stream()
					.filter(coupon -> coupon.getUpgradeEnabled() 
							&& !getAncillaryByTypeStatusAndETicket(ctx, upgradeType, status, coupon.getEticket()).isEmpty() 
							&& coupon.getUpgradeEnabledClasses().contains(CompClass)).count() > 0)

					.collect(Collectors.toList());
		}
		return eligiblePassengers;
	}

	/**
	 * 
	 * @param ctx
	 * @return i dati dell'assicurazione oppure null se
	 * 		   la chiamata a servizio è andata in errore
	 */
	public CheckinInsurancePolicyData getInsurancePolicyData(CheckinSessionContext ctx){
		String languageCode = AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag());
		CheckinFlightData firstFlight = ctx.selectedRoute.getFlights().get(0);
		CheckinFlightData lastFlight = ctx.selectedRoute.getFlights().get(
				ctx.selectedRoute.getFlights().size() -1);

		/*FIXME capire il criterio con cui popolare questo campo*/
		boolean hasReturn = computeHasReturn(ctx.routesList, ctx.selectedRoute);

		WebCheckinGetInsuranceRequest request = new WebCheckinGetInsuranceRequest(IDFactory.getTid(), ctx.sid);

		request.setArea(computeLegType(ctx.selectedRoute));
		request.setDepartureAirport(firstFlight.getFrom().getCode());
		request.setArrivalAirport(lastFlight.getTo().getCode());
		request.setDepartureDate(firstFlight.getDepartureDateTime());
		request.setArrivalDate(lastFlight.getArrivalDateTime());
		request.setCountryCode(ctx.market);
		request.setPnr(firstFlight.getPnr());
		Boolean usaCa = false;
		if("ca".equalsIgnoreCase(ctx.market) || "us".equalsIgnoreCase(ctx.market)){
			usaCa = true;
		}
		request.setFlgUsaCa(usaCa);
		request.setHasReturn(hasReturn);
		request.setPassengersCount(ctx.passengers.size());

		request.setLanguageCode(languageCode);
		request.setClient(ctx.serviceClient);
		request.setBaseInfo(createBaseInfo(ctx));
		CheckinInsuranceResponse response = null;
		try {
			response = checkInDelegate.getInsuranceInfo(request);
			if (response != null) {
				return response.getPolicy();
			}
		} catch (Exception e) {
			logger.error("Error retriving insurance: ", e);
			return null;
		}

		return null;
	}

	private MmbLegTypeEnum computeLegType(CheckinRouteData selectedRoute){
		MmbLegTypeEnum type= MmbLegTypeEnum.DOM;
		for(CheckinFlightData flight : selectedRoute.getFlights()){
			switch(flight.getLegType()){
			case INC:
				if(MmbLegTypeEnum.DOM.equals(type) || MmbLegTypeEnum.INZ.equals(type)){
					type = MmbLegTypeEnum.INC;
				}
				break;
			case INZ:
				if(MmbLegTypeEnum.DOM.equals(type)){
					type = MmbLegTypeEnum.INZ;
				}
			default:
			}
		}
		return type;
	}

	private boolean computeHasReturn(List<CheckinRouteData> routesList
			, CheckinRouteData selectedRoute){
		boolean hasReturn = false;
		if(routesList.size() > 1){
			/* FIXME vedere se necessario
			CheckinFlightData firstFlight = selectedRoute.getFlights().get(0);
			String from = firstFlight.getFrom().getCode();
			CheckinFlightData lastFlight = selectedRoute.getFlights().get(
					selectedRoute.getFlights().size() -1);
			String to = lastFlight.getTo().getCode();
			firstFlight = routesList.get(1).getFlights().get(0);
			lastFlight = routesList.get(1).getFlights().get(routesList.get(1).getFlights().size() -1);
			String routeFrom = firstFlight.getFrom().getCode();
			String routeTo = lastFlight.getTo().getCode();
			if(from.equals(routeTo) && to.equals(routeFrom)){
				hasReturn = true;
			}*/
			hasReturn = true;
		}
		return hasReturn;
	}

	/**
	 * 
	 * @param ctx
	 */
	public void addInsuranceToCart(CheckinSessionContext ctx) {
		ctx.insuranceAddedToCart = true;
		refreshCart(ctx, IDFactory.getTid());
	}

	/**
	 * 
	 * @param ctx
	 */
	public void removeInsuranceFromCart(CheckinSessionContext ctx) {
		ctx.insuranceAddedToCart = false;
		refreshCart(ctx, IDFactory.getTid());
	}

	/**
	 * Internal utility method to initialize the MMB cart into context.
	 */
	private void initializeCart(CheckinSessionContext ctx, String tid) {

		// prepare request
		WebCheckinGetCatalogRequest catalogRequest = 
				getCatalogRequestFromCheckinRouteData(ctx);

		catalogRequest.setSid(ctx.sid);
		catalogRequest.setTid(tid);

		// invoke service
		WebCheckinAncillaryCartResponse catalogResponse = checkInDelegate.getCatalog(catalogRequest);

		// read catalog data into context
		ctx.cart = catalogResponse.getCart();
		ctx.cartErrors = catalogResponse.getErrors();
		ctx.cartTotalAmount = new BigDecimal(0);
		ctx.cartTotalAncillariesAmount = new BigDecimal(0);
	}

	/**
	 * Initialize the MMB cart and related data if required,
	 * otherwise perform a refresh of the cart.
	 * 
	 * @param ctx The MmbSessionContext to use.
	 */
	public void initializeCartIfRequired(CheckinSessionContext ctx) {
		logger.debug("initializeCartIfRequired");

		String tid = IDFactory.getTid();

		if (!ctx.cartInitialized) {
			logger.debug("Cart initialization requested on an uninitialized cart, "
					+ "performing a cart initialization.");
			initializeCart(ctx, tid);
			ctx.cartInitialized = true;

		} else {
			logger.debug("Cart initialization requested on an already initialized cart, "
					+ "performing a cart refresh.");
			refreshCart(ctx, tid);
		}
	}

	/**
	 * Internal utility method to refersh the MMB cart into context.
	 */
	private void refreshCart(CheckinSessionContext ctx, String tid) {
		logger.debug("refreshCart");

		if (ctx.cart == null) {
			logger.debug("Refresh cart request ignored: no cart is currently available.");
			return;
		}

		// prepare request
		WebCheckinGetCartRequest cartRequest = new WebCheckinGetCartRequest();
		cartRequest.setSid(ctx.sid);
		cartRequest.setTid(tid);
		cartRequest.setBaseInfo(createBaseInfo(ctx));
		cartRequest.setCartId(ctx.cart.getId());
		cartRequest.setClient(ctx.serviceClient);
		cartRequest.setMachineName(ctx.machineName);
		cartRequest.setSessionId(ctx.sid);

		// invoke service
		WebCheckinAncillaryCartResponse cartResponse = checkInDelegate.getCart(cartRequest);

		// read cart data into context
		ctx.cart = cartResponse.getCart();
		ctx.cartErrors = cartResponse.getErrors();

		// compute cart total amount
		MmbAncillaryStatusEnum ancillaryStatus = !ctx.ancillaryPaymentSuccess ? MmbAncillaryStatusEnum.TO_BE_ISSUED 
				: MmbAncillaryStatusEnum.ISSUED;
		ctx.cartTotalAmount = new BigDecimal(0);
		for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
			if (ancillaryStatus.equals(ancillary.getAncillaryStatus())) {
				ctx.cartTotalAmount = ctx.cartTotalAmount.add(ancillary.getAmount());
			}
		}
		ctx.cartTotalAncillariesAmount = ctx.cartTotalAmount;
		if (ctx.insuranceAddedToCart && ctx.insurancePolicyData != null && !ctx.insurancePolicyData.getBuy()
				|| (ctx.insurancePolicyData != null && ctx.insurancePolicyData.getBuy() && ctx.insurancePaymentSuccess) ) {
			ctx.cartTotalAmount = ctx.cartTotalAmount.add(ctx.insurancePolicyData.getTotalInsuranceCost());
		}

	}

	/**
	 * Remove all ancillaries in to be issued status from cart, optionally considering
	 * only the specified ancillary type and/or index.
	 * 
	 * @param ctx The CheckinSessionContext to use.
	 * @param ancillaryType The ancillary type, or null.
	 * @param passengerIndex The passenger index, or null.
	 * @returns A list of errors produced by the operation.
	 */
	public List<MmbAncillaryErrorData> clearAncillariesFromCart(CheckinSessionContext ctx, MmbAncillaryTypeEnum ancillaryType,
			Integer passengerIndex) throws Exception {
		logger.debug("clearAncillariesFromCart");

		String tid = IDFactory.getTid();
		List<MmbAncillaryErrorData> errors = null;


		if (ancillaryType == null || MmbAncillaryTypeEnum.INSURANCE.equals(ancillaryType)) {
			// insurance ancillary is not managed as a cart item, apply special logic
			if (passengerIndex != null) {
				logger.error("Insurance ancillary remove from cart requested for a single passenger - this is usupported.");
				throw new IllegalArgumentException("Insurance ancillary remove from cart requested for a single passenger - this is usupported.");
			}
			if (ctx.insurancePolicyData != null && !StringUtils.isBlank(ctx.insurancePolicyData.getPolicyNumber())) {
				logger.error("Insurance ancillary remove from cart requestes for an already bought insurance - this is usupported.");
				throw new IllegalArgumentException("Insurance ancillary remove from cart requestes for an already bought insurance - this is usupported.");
			}
			if (ctx.insuranceAddedToCart) {
				ctx.insuranceAddedToCart = false;
			}

		}

		// other ancillaries, use standard cart methods
		errors = removeAncillariesFromCart(ctx, tid, ancillaryType, passengerIndex);

		refreshCart(ctx, tid);

		return errors;
	}

	/**
	 * Internal utility method to prepare a list of to be issued ancillaries and remove them from cart.
	 */
	private List<MmbAncillaryErrorData> removeAncillariesFromCart(CheckinSessionContext ctx, String tid, 
			MmbAncillaryTypeEnum ancillaryType, Integer passengerIndex) throws Exception {

		List<Integer> itemIdsToRemove = new ArrayList<>();
		for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {

			// check type and passenger
			if (ancillaryType != null && !ancillaryType.equals(ancillary.getAncillaryType())) {
				// no type match
				continue;
			} else if (passengerIndex != null) {
				CheckinAncillaryPassengerData ancillaryPassenger = 
						CheckinUtils.findAncillaryPassenger(ancillary, ctx.cart.getPassengers());
				if (ancillaryPassenger == null) {
					logger.error("Cannot find the passenger related to an ancillary.");
					throw new Exception("Cannot find the passenger related to an ancillary.");
				}
				// Verifico che sia il passeggero per cui e' stato richiesto di rimuovere l'ancillary
				if (!passengerIndex.equals(ancillaryPassenger.getId())) {
					// no passenger match
					continue;
				}
			}

			// check status
			boolean remove = false;
			if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
				remove = true;
			}

			// Non rimuovo gli item in stato ISSUED
			/* else if (MmbAncillaryStatusEnum.ISSUED.equals(ancillary.getAncillaryStatus())) {
				if (MmbAncillaryTypeEnum.MEAL.equals(ancillary.getAncillaryType())) {
					// meal ancillaries are free, can be removed from cart even if ISSUED status
					remove = true;
				} else if (MmbAncillaryTypeEnum.SEAT.equals(ancillary.getAncillaryType())) {
					// seat ancillaries, when free, can be removed from cart even if ISSUED status
					MmbAncillarySeatDetailData ancillaryDetail = 
							(MmbAncillarySeatDetailData) ancillary.getAncillaryDetail();
					if (!ancillaryDetail.isComfort()) {
						remove = true;
					} else {
						BigDecimal price = getAncillaryDisplayedPriceInfo(ctx, ancillary, null).getAmount();
						if (price == null || price.equals(BigDecimal.valueOf(0))) {
							remove = true;
						}
					}
				}
			}*/

			if (remove) {
				itemIdsToRemove.add(ancillary.getId());
			}
		}
		logger.debug("{} ancillaries to be cleared in cart.", itemIdsToRemove.size());

		List<MmbAncillaryErrorData> errors = new ArrayList<>();
		if (itemIdsToRemove.size() > 0) {
			WebCheckinRemoveFromCartRequest removeFromCartRequest = 
					new WebCheckinRemoveFromCartRequest();
			removeFromCartRequest.setTid(tid);
			removeFromCartRequest.setSid(ctx.sid);
			removeFromCartRequest.setSessionId(ctx.sid);
			removeFromCartRequest.setBaseInfo(createBaseInfo(ctx));
			removeFromCartRequest.setCartId(ctx.cart.getId());
			removeFromCartRequest.setClient(ctx.serviceClient);
			removeFromCartRequest.setMachineName(ctx.machineName);
			removeFromCartRequest.setAncillaryIDs(itemIdsToRemove);

			WebCheckinAncillaryResponse removeFromCartResponse = 
					checkInDelegate.removeFromCart(removeFromCartRequest);

			if (removeFromCartResponse.getErrors() != null) {
				errors.addAll(removeFromCartResponse.getErrors());
			}
		}

		return errors;
	}

	/**
	 * Find an ancillary by identifier in current cart data.
	 * 
	 * @param ctx The MMB session context.
	 * @param ancillaryId The ancillary id.
	 * @return The MmbAncillaryData for the ancillary object, or null if not found.
	 */
	public MmbAncillaryData findCartAncillaryById(CheckinSessionContext ctx, Integer ancillaryId) {
		if (ctx != null && ctx.cart != null && ancillaryId != null) {
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
				if (ancillaryId.equals(ancillary.getId())) {
					return ancillary;
				}
			}
		}
		return null;
	}

	public WebCheckinGetCatalogRequest getCatalogRequestFromCheckinRouteData(CheckinSessionContext ctx) {
		WebCheckinGetCatalogRequest ancillaryCatalogRequest = new WebCheckinGetCatalogRequest();

		MmbRequestBaseInfoData baseInfo = createBaseInfo(ctx);

		ancillaryCatalogRequest.setBaseInfo(baseInfo);
		ancillaryCatalogRequest.setClient(ctx.serviceClient);
		ancillaryCatalogRequest.setLanguage(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		ancillaryCatalogRequest.setMachineName(ctx.machineName);
		ancillaryCatalogRequest.setMarket(ctx.market);

		List<MmbAncillaryFlightData> ancillaryFlights = new ArrayList<MmbAncillaryFlightData>();
		List<CheckinAncillaryPassengerData> ancillaryPassengers = new ArrayList<CheckinAncillaryPassengerData>();
		//Map<Integer, String> lastCouponNumberByPassengerId = new HashMap<>();
		//int flightCounter = 0;
		//boolean isFirstRouteFlight = false;
		//CheckinFlightData previousMmbFlight = null;

		List<CheckinFlightData> flights = CheckinUtils.getFlightsList(ctx.selectedRoute);
		Map<Integer, Integer> flightIds = new HashMap<Integer, Integer>();

		int flightCounter = 1;
		for (CheckinFlightData checkinFlight : flights) {
			if (checkinFlight.getRph() != null) {
				MmbAncillaryFlightData ancillaryFlight = new MmbAncillaryFlightData();
				ancillaryFlight.setCarrier(checkinFlight.getCarrier());
				ancillaryFlight.setDepartureDate(checkinFlight.getDepartureDateTime());
				ancillaryFlight.setDestination(checkinFlight.getTo().getCode());

				switch (checkinFlight.getLegType()) {
				case DOM:
					ancillaryFlight.setFlightType(MmbFlightTypeEnum.DOMESTIC);
					break;
				case INZ:
					ancillaryFlight.setFlightType(MmbFlightTypeEnum.INTERNATIONAL);
					break;
				case INC:
					ancillaryFlight.setFlightType(MmbFlightTypeEnum.INTERCONTINENTAL);
					break;
				default:
					ancillaryFlight.setFlightType(MmbFlightTypeEnum.DOMESTIC);
				}

				ancillaryFlight.setId(flightCounter);
				ancillaryFlight.setNumber(checkinFlight.getFlightNumber());
				ancillaryFlight.setOperationalFlightCarrier(checkinFlight.getOperatingCarrier());
				ancillaryFlight.setOperationalFlightNumber(checkinFlight.getOperatingFlightNumber());
				ancillaryFlight.setOrigin(checkinFlight.getFrom().getCode());
				ancillaryFlight.setReferenceNumber(Integer.parseInt(checkinFlight.getRph()));
				ancillaryFlights.add(ancillaryFlight);
				flightIds.put(Integer.parseInt(checkinFlight.getFlightNumber()), flightCounter);
			}
			flightCounter++;
		}

		ancillaryCatalogRequest.setFlights(ancillaryFlights);

		List<CheckinPassengerData> passengers = CheckinUtils.getCheckedInPassengers(ctx.passengers);

		int passengerCount = 1;
		for (CheckinPassengerData passengerData : passengers) {
			CheckinAncillaryPassengerData ancillaryPassenger = new CheckinAncillaryPassengerData();
			ancillaryPassenger.setId(passengerCount);
			ancillaryPassenger.setLastName(passengerData.getLastName());
			ancillaryPassenger.setName(passengerData.getName());
			ancillaryPassenger.setReferenceNumber(Integer.parseInt(passengerData.getReservationRPH()));

			Map<String, List<CheckinAncillaryCouponData>> eticketMap = new HashMap<String, List<CheckinAncillaryCouponData>>();

			int couponCounter = 0;
			String previousCouponNumber = "";
			for (CheckinCouponData couponData : passengerData.getCoupons()) {
				CheckinAncillaryCouponData coupon = new CheckinAncillaryCouponData();
				coupon.setBaggageAllowance(passengerData.getBaggageAllowanceQuantity());
				coupon.setFareClass(couponData.getSeatClass().toString());
				coupon.setFlightId(flightIds.get(Integer.parseInt(couponData.getFlight().getFlightNumber())));
				coupon.setFlightNumber(Integer.parseInt(couponData.getFlight().getFlightNumber()));
				if (couponCounter>0 && !previousCouponNumber.isEmpty()) {
					coupon.setInboundCouponNumber(previousCouponNumber);
				} else {
					coupon.setInboundCouponNumber("");
					previousCouponNumber = couponData.getEticket() + "C" + couponData.getId();
				}
				coupon.setIsChild(CheckinPassengerTypeEnum.CHILD.equals(passengerData.getType()));
				coupon.setIsInfant(CheckinPassengerTypeEnum.INFANT.equals(passengerData.getType()));
				coupon.setIsInfantAccompanist(CheckinPassengerStatusEnum.INFANT_ACCOMPANIST.equals(passengerData.getStatus()));
				coupon.setIsInterline(couponData.getGdsCouponType() != MmbGdsTypeEnum.AZ);
				coupon.setIsSeatAssignDisabled(!couponData.getChangeSeatEnabled());
				coupon.setIsSeatAssignPayment(couponData.getChangeSeatPayment());
				coupon.setIsSpecialPassenger(CheckinPassengerStatusEnum.SPECIAL_PASSENGER.equals(passengerData.getStatus()));
				coupon.setNumber(couponData.getEticket() + "C" + Integer.toString(couponData.getId()));
				coupon.setPassengerNumber(couponData.getPassengerId());
				coupon.setSeatNumber(couponData.getSeat());
				if (couponData.getUpgradeEnabledClasses() != null) {
					coupon.setUpgradeCompartimentalClasses(couponData.getUpgradeEnabledClasses());
				}

				if (eticketMap.get(couponData.getEticket()) == null) {
					List<CheckinAncillaryCouponData> couponList = new ArrayList<CheckinAncillaryCouponData>();
					eticketMap.put(couponData.getEticket(), couponList);
				}

				eticketMap.get(couponData.getEticket()).add(coupon);
				couponCounter++;
			}

			List<CheckinAncillaryEticketData> eticketList = new ArrayList<CheckinAncillaryEticketData>();

			for (String eticketNumber : eticketMap.keySet()) {
				CheckinAncillaryEticketData eticket = new CheckinAncillaryEticketData();
				eticket.setNumber(eticketNumber);
				eticket.setPnr(ctx.pnr);
				eticket.setCoupons(eticketMap.get(eticketNumber));
				eticketList.add(eticket);
			}

			ancillaryPassenger.setEtickets(eticketList);

			if (passengerData.getFrequentFlyerCode() != null &&
					passengerData.getFrequentFlyerCarrier() != null &&
					passengerData.getFrequentFlyerType() != null &&
					("AZ").equals(passengerData.getFrequentFlyerCarrier().getCode()) ) {

				MmbAncillaryFrequentFlyerData frequentFlyerInfo = new MmbAncillaryFrequentFlyerData();
				frequentFlyerInfo.setCode(passengerData.getFrequentFlyerCode());
				frequentFlyerInfo.setFfType(passengerData.getFrequentFlyerCarrier().getCode());
				frequentFlyerInfo.setTier(passengerData.getFrequentFlyerType().toString());
				ancillaryPassenger.setFrequentFlyerInfo(frequentFlyerInfo);
			}

			ancillaryPassengers.add(ancillaryPassenger);
			passengerCount++;
		}

		ancillaryCatalogRequest.setPassengers(ancillaryPassengers);
		ancillaryCatalogRequest.setSessionId(baseInfo.getSessionId());

		return ancillaryCatalogRequest;
	}

	/**
	 * It adds or updates seats by using the ancillary in the cart 
	 * and the internal method addOrRemoveAncillaryFromCart. It returns the list of errors 
	 * @param ctx The CheckinSessionContext to use.
	 * @param seatSelectionsByAncillaryId The seat selections, mapped by ancillary id, to add or update in cart.
	 * @return The list of errors. The list if empty if the no errors occurred
	 * @throws InvalidAncillaryStatusException 
	 * @throws InvalidAncillaryIdentifierException 
	 */
	public List<MmbAncillaryErrorData> addOrUpdateSeatsInCart(CheckinSessionContext ctx, 
			Map<Integer, String> seatSelectionsByAncillaryId) 
					throws InvalidAncillaryStatusException, InvalidAncillaryIdentifierException {

		logger.debug("addOrUpdateSeatAncillaryFromCart");

		String tid = IDFactory.getTid();

		// process ancillaries to add or update
		List<MmbAncillaryCartItemData> itemsToAdd = new ArrayList<>();
		List<MmbAncillaryCartItemData> itemsToUpdate = new ArrayList<>();

		Map<String, MmbAncillaryData> ancillaryPaymentMap = new HashMap<String, MmbAncillaryData>();

		for (Map.Entry<Integer, String> entry : seatSelectionsByAncillaryId.entrySet()) {
			MmbAncillaryData ancillary = findCartAncillaryById(ctx, entry.getKey());
			if (ancillary.getAncillaryDetail() != null) {
				MmbAncillarySeatDetailData detail = (MmbAncillarySeatDetailData) ancillary.getAncillaryDetail();
				String seatCode = entry.getValue();
				if (Boolean.TRUE.equals(detail.isPayment())) {
					ancillaryPaymentMap.put(seatCode, ancillary);
				}
			}
		}

		for (Map.Entry<Integer, String> entry : seatSelectionsByAncillaryId.entrySet()) {
			Integer ancillaryId = entry.getKey();
			String seatCode = entry.getValue();
			if (seatCode != null && !"".equals(seatCode)) {
				MmbAncillaryData ancillary = findCartAncillaryById(ctx, ancillaryId);
				if (ancillary == null || ancillary.getCouponNumbers() == null || 
						ancillary.getCouponNumbers().isEmpty()) {
					throw new InvalidAncillaryIdentifierException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_ID, 
									null, new Object[] { ancillaryId }));
				}

				Boolean isComfortSeat = Boolean.FALSE;
				String flightNumber = getFlightNumberByAncillary(ctx, ancillary);

				if (ctx.comfortSeatAvailable && isComfortSeat(ctx, flightNumber, seatCode)) {
					isComfortSeat = Boolean.TRUE;
				}

				MmbAncillaryData ancillaryPayment = ancillaryPaymentMap.get(seatCode);
				Boolean existAncillaryPayment = ancillaryPayment != null;

				if ((!isComfortSeat && existAncillaryPayment && ancillary != ancillaryPayment) || 
						(isComfortSeat && existAncillaryPayment && ancillary == ancillaryPayment)) {
					// if we have an ancillary with attribute isPayment = true
					// the standard seat must be associated with that ancillary
					continue;
				}

				// In input non arriva mai il seat con il 0 iniziale nel caso di posti con fila a una cifra
				if (seatCode.length() == 2) {
					seatCode = "0" + seatCode;
				}

				MmbAncillaryCartItemData item = new MmbAncillaryCartItemData();
				item.setAncillaryId(ancillaryId);
				item.setQuantity(1);
				MmbAncillarySeatDetailData ancillaryDetail = new MmbAncillarySeatDetailData();
				ancillaryDetail.setSeat(seatCode);
				ancillaryDetail.setComfort(isComfortSeat);
				ancillaryDetail.setPayment(existAncillaryPayment);

				item.setAncillaryDetail(ancillaryDetail);
				if (MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus())) {
					itemsToAdd.add(item);
				} else if (MmbAncillaryStatusEnum.ISSUED.equals(ancillary.getAncillaryStatus()) ||
						MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
					MmbAncillarySeatDetailData detail = 
							(MmbAncillarySeatDetailData) ancillary.getAncillaryDetail();
					if (!seatCode.equals(detail.getSeat())) {
						itemsToUpdate.add(item);
					}
				} else {
					throw new InvalidAncillaryStatusException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_STATUS, 
									null, new Object[] { ancillaryId }));
				}
			}
		}
		logger.debug("{} seat ancillaries to be added in cart, {} to be updated.", 
				itemsToAdd.size(), itemsToUpdate.size());

		List<MmbAncillaryErrorData> errors = new ArrayList<MmbAncillaryErrorData>();

		if (itemsToAdd.size() > 0) {
			WebCheckinAddToCartRequest addToCartRequest = 
					new WebCheckinAddToCartRequest();
			addToCartRequest.setTid(tid);
			addToCartRequest.setSid(ctx.sid);
			addToCartRequest.setSessionId(ctx.sid);
			addToCartRequest.setBaseInfo(createBaseInfo(ctx));
			addToCartRequest.setCartId(ctx.cart.getId());
			addToCartRequest.setClient(ctx.serviceClient);
			addToCartRequest.setMachineName(ctx.machineName);
			addToCartRequest.setItems(itemsToAdd);

			WebCheckinAncillaryResponse addToCartResponse = 
					checkInDelegate.addToCart(addToCartRequest);

			if (addToCartResponse.getErrors() != null) {
				errors.addAll(addToCartResponse.getErrors());
			}
		}

		if (itemsToUpdate.size() > 0) {
			WebCheckinUpdateCartRequest updateCartRequest = 
					new WebCheckinUpdateCartRequest();
			updateCartRequest.setTid(tid);
			updateCartRequest.setSid(ctx.sid);
			updateCartRequest.setSessionId(ctx.sid);
			updateCartRequest.setBaseInfo(createBaseInfo(ctx));
			updateCartRequest.setCartId(ctx.cart.getId());
			updateCartRequest.setClient(ctx.serviceClient);
			updateCartRequest.setMachineName(ctx.machineName);
			updateCartRequest.setItems(itemsToUpdate);

			WebCheckinAncillaryResponse updateCartResponse = 
					checkInDelegate.updateCart(updateCartRequest);

			if (updateCartResponse.getErrors() != null) {
				errors.addAll(updateCartResponse.getErrors());
			}
		}

		if (itemsToAdd.size() > 0 || itemsToUpdate.size() > 0) {
			refreshCart(ctx, tid);
		}

		return errors;
	}

	/**
	 * It adds seats upgrades by using the ancillary in the cart 
	 * and the internal method addOrRemoveAncillaryFromCart. It returns the list of errors 
	 * @param ctx
	 * @return 
	 * @return The list of errors. The list if empty if the no errors occurred
	 * @throws InvalidAncillaryIdentifierException 
	 * @throws InvalidAncillaryStatusException 
	 */
	public List<MmbAncillaryErrorData> addSeatUpgradeInCart(CheckinSessionContext ctx, Map<Integer, String> upgradeByAncillaryId) 
			throws InvalidAncillaryIdentifierException, InvalidAncillaryStatusException {
		List<MmbAncillaryCartItemData> ancillaryUpgradeToAdd = new ArrayList<MmbAncillaryCartItemData>();
		List<Integer> ancillaryUpgradeIdsToRemove = new ArrayList<Integer>();


		// process ancillaries to add or update
		for (Entry<Integer, String> entry : upgradeByAncillaryId.entrySet()) {
			Integer ancillaryId = entry.getKey();
			String ancillaryValue = entry.getValue();
			MmbAncillaryData ancillary = findCartAncillaryById(ctx, ancillaryId);
			if (ancillary == null) {
				throw new InvalidAncillaryIdentifierException(
						ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_ID, 
								null, new Object[] { ancillaryId }));
			}

			CheckinAncillaryUpgradeDetailData detail = new CheckinAncillaryUpgradeDetailData();
			String[] detailSplit = ancillaryValue.split("-");
			String seatCode = detailSplit[0];

			if (seatCode.length() == 2) {
				seatCode = "0" + seatCode;
			}
			detail.setSeat(seatCode);
			detail.setCompartimentalClass(MmbCompartimentalClassEnum.fromValue(detailSplit[1]));
			ancillary.setAncillaryDetail(detail);
			if (MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus())) {
				MmbAncillaryCartItemData item = new MmbAncillaryCartItemData();
				item.setAncillaryId(ancillaryId);
				item.setQuantity(1);
				item.setAncillaryDetail(detail);
				ancillaryUpgradeToAdd.add(item);
			} else if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
				logger.debug("Ancillary {} already to be issued, ignoring add to cart request.", ancillaryId);
			} else {
				throw new InvalidAncillaryStatusException(
						ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_STATUS, 
								null, new Object[] { ancillaryId }));
			}
		}
		return addOrRemoveAncillaryFromCart(ctx,ancillaryUpgradeToAdd,ancillaryUpgradeIdsToRemove);
	}




	/**
	 * It adds or updates the extra baggages by using the ancillary in the cart 
	 * and the internal method addOrRemoveAncillaryFromCart. It returns the list of errors 
	 * @param ctx
	 * @return The list of errors. The list if empty if the no errors occurred
	 * @throws InvalidAncillaryIdentifierException 
	 * @throws InvalidAncillaryStatusException 
	 */
	public List<MmbAncillaryErrorData> addOrRemoveExtraBaggageAncillaryFormCart(
			CheckinSessionContext ctx, Map<Integer, Boolean> extraBaggageSelectionByAncillaryId) 
					throws InvalidAncillaryIdentifierException, InvalidAncillaryStatusException {

		List<MmbAncillaryCartItemData> ancillaryBaggageToAdd = new ArrayList<MmbAncillaryCartItemData>();
		List<Integer> ancillaryBaggageIdsToRemove = new ArrayList<Integer>();

		// process ancillaries to add or update
		for (Map.Entry<Integer, Boolean> entry : extraBaggageSelectionByAncillaryId.entrySet()) {
			Integer ancillaryId = entry.getKey();
			Boolean selected = entry.getValue();
			MmbAncillaryData ancillary = findCartAncillaryById(ctx, ancillaryId);
			if (ancillary == null) {
				throw new InvalidAncillaryIdentifierException(
						ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_ID, 
								null, new Object[] { ancillaryId }));
			}
			if (Boolean.TRUE.equals(selected)) {
				if (MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus())) {
					MmbAncillaryCartItemData item = new MmbAncillaryCartItemData();
					item.setAncillaryId(ancillaryId);
					item.setQuantity(1);
					ancillaryBaggageToAdd.add(item);
				} else if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
					logger.debug("Ancillary {} already to be issued, ignoring add to cart request.", ancillaryId);
				} else {
					throw new InvalidAncillaryStatusException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_STATUS, 
									null, new Object[] { ancillaryId }));
				}
			} else {
				if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
					ancillaryBaggageIdsToRemove.add(ancillaryId);
				} else if ((MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus()))) {
					logger.debug("Ancillary {} already available, ignoring remove from cart request.", ancillaryId);
				} else {
					throw new InvalidAncillaryStatusException(
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INVALID_ANCILLARY_STATUS, 
									null, new Object[] { ancillaryId }));
				}
			}
		}

		return addOrRemoveAncillaryFromCart(ctx,ancillaryBaggageToAdd,ancillaryBaggageIdsToRemove);
	}

	/**
	 * It add and remove the ancillary to and from cart. And return a list of errors
	 * @param ctx
	 * @param itemsToAdd The ancillaries to add to cart
	 * @param itemIdsToRemove The ancillaries ID to remove from cart
	 * @return a list of errors. The list is empty if no error occurred 
	 */
	private List<MmbAncillaryErrorData> addOrRemoveAncillaryFromCart(
			CheckinSessionContext ctx, List<MmbAncillaryCartItemData> itemsToAdd, 
			List<Integer> itemIdsToRemove) {

		String tid = IDFactory.getTid();

		List<MmbAncillaryErrorData> errors = new ArrayList<>();

		if (itemsToAdd != null && itemsToAdd.size() > 0) {
			WebCheckinAddToCartRequest addToCartRequest = 
					new WebCheckinAddToCartRequest();
			addToCartRequest.setTid(tid);
			addToCartRequest.setSid(ctx.sid);
			addToCartRequest.setSessionId(ctx.sid);
			addToCartRequest.setBaseInfo(createBaseInfo(ctx));
			addToCartRequest.setCartId(ctx.cart.getId());
			addToCartRequest.setClient(ctx.serviceClient);
			addToCartRequest.setMachineName(ctx.machineName);
			addToCartRequest.setItems(itemsToAdd);

			WebCheckinAncillaryResponse addToCartResponse = 
					checkInDelegate.addToCart(addToCartRequest);

			if (addToCartResponse.getErrors() != null) {
				errors.addAll(addToCartResponse.getErrors());
			}
		}

		if (itemIdsToRemove != null && itemIdsToRemove.size() > 0) {
			WebCheckinRemoveFromCartRequest removeFromCartRequest = 
					new WebCheckinRemoveFromCartRequest();
			removeFromCartRequest.setTid(tid);
			removeFromCartRequest.setSid(ctx.sid);
			removeFromCartRequest.setSessionId(ctx.sid);
			removeFromCartRequest.setBaseInfo(createBaseInfo(ctx));
			removeFromCartRequest.setCartId(ctx.cart.getId());
			removeFromCartRequest.setClient(ctx.serviceClient);
			removeFromCartRequest.setMachineName(ctx.machineName);
			removeFromCartRequest.setAncillaryIDs(itemIdsToRemove);

			WebCheckinAncillaryResponse removeFromCartResponse = 
					checkInDelegate.removeFromCart(removeFromCartRequest);

			if (removeFromCartResponse.getErrors() != null) {
				errors.addAll(removeFromCartResponse.getErrors());
			}
		}

		if ((itemsToAdd != null && itemsToAdd.size() > 0) ||
				(itemIdsToRemove != null && itemIdsToRemove.size() > 0)) {
			refreshCart(ctx, tid);
		}

		return errors;
	}

	/**
	 * 
	 * @param ctx
	 * @param ancillary
	 * @return
	 */
	public String getFlightNumberByAncillary(CheckinSessionContext ctx, MmbAncillaryData ancillary) {

		List<CheckinPassengerData> passengers = CheckinUtils.getCheckedInPassengers(ctx.passengers);

		for (CheckinPassengerData passenger : passengers) {
			for (CheckinCouponData coupon : passenger.getCoupons()) {
				for (String couponNumber : ancillary.getCouponNumbers()) {
					if ((coupon.getEticket() + "C" + coupon.getId()).equals(couponNumber)) {
						return coupon.getFlight().getFlightNumber();
					}
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * @param ctx
	 * @param ancillary
	 * @return
	 */
	public CheckinCouponData getCouponByAncillary(CheckinSessionContext ctx, MmbAncillaryData ancillary) {

		List<CheckinPassengerData> passengers = CheckinUtils.getCheckedInPassengers(ctx.passengers);

		for (CheckinPassengerData passenger : passengers) {
			for (CheckinCouponData coupon : passenger.getCoupons()) {
				for (String couponNumber : ancillary.getCouponNumbers()) {
					if ((coupon.getEticket() + "C" + coupon.getId()).equals(couponNumber)) {
						return coupon;
					}
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * @param ctx
	 * @param ancillaryType
	 * @param ancillaryStatus
	 * @param eticket
	 * @return
	 */
	public List<MmbAncillaryData> getAncillaryByTypeStatusAndETicket(CheckinSessionContext ctx,
			MmbAncillaryTypeEnum ancillaryType, List<MmbAncillaryStatusEnum> ancillaryStatus, String eticket) {

		List<MmbAncillaryData> ancillaries = new ArrayList<MmbAncillaryData>();
		if (ctx.cart != null && ctx.cart.getAncillaries() != null) {
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
				if (ancillary.getAncillaryType() == ancillaryType && ancillaryStatus.contains(ancillary.getAncillaryStatus())) {
					if (ancillary.getCouponNumbers() != null) {
						String coupon = ancillary.getCouponNumbers().get(0);
						String couponTicket = coupon.split("C")[0];
						if (eticket.equals(couponTicket)) {
							ancillaries.add(ancillary);
						}
					}
				}
			}
		}
		return ancillaries;
	}

	private CheckinAncillarySeatStatus getAncillarySeatStatus(CheckinSessionContext ctx) {
		if (ctx.cart != null && ctx.cart.getAncillaries() != null) {
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {

				// only seat available with isPayment != true
				if (MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus()) && 
						MmbAncillaryTypeEnum.SEAT.equals(ancillary.getAncillaryType()) &&
						(ancillary.getAncillaryDetail() == null || 
						Boolean.FALSE.equals(((MmbAncillarySeatDetailData) ancillary.getAncillaryDetail()).isPayment()))) {

					if (ancillary.getQuotations() == null || ancillary.getQuotations().isEmpty()) {
						return CheckinAncillarySeatStatus.ONLY_STANDARD_SEAT_AVAILABLE;
					} else {
						return CheckinAncillarySeatStatus.COMFORT_SEAT_AVAILABLE;
					}
				}
			}
		}
		return CheckinAncillarySeatStatus.SEAT_NOT_AVAILABLE;
	}


	/**
	 * Retrieve available upgrade ancillary associated to a specific passenger 
	 * @param ctx
	 * @param eticketAndCouponId E-ticket number + "C" + coupon id
	 * @return all upgrades in map form
	 */
	public Map<MmbCompartimentalClassEnum, MmbAncillaryData> getAvailableUpdateForEticket(CheckinSessionContext ctx, String eticketAndCouponId){
		Map<MmbCompartimentalClassEnum, MmbAncillaryData> map = new HashMap<MmbCompartimentalClassEnum, MmbAncillaryData>();
		if (ctx.cart != null && ctx.cart.getAncillaries() != null) {
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
				if (MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus()) && 
						MmbAncillaryTypeEnum.UPGRADE.equals(ancillary.getAncillaryType()) &&
						ancillary.getCouponNumbers() != null) {
					for (String coupon : ancillary.getCouponNumbers()) {
						if (eticketAndCouponId.equals(coupon)) {
							MmbCompartimentalClassEnum compClass = ((CheckinAncillaryUpgradeDetailData) 
									ancillary.getAncillaryDetail()).getCompartimentalClass();
							map.put(compClass, ancillary);
						}
					}
				}
			}
		}
		return map;
	}

	/**
	 * Retrieve assigned upgrade ancillary associated to a specific passenger 
	 * @param ctx
	 * @param eticket E-ticket number 
	 * @return all upgrades in map form
	 */
	public MmbAncillaryData getAssignedUpdateForEticket(CheckinSessionContext ctx, String eticket){
		MmbAncillaryData assignedUpgrade = null;
		List<MmbAncillaryStatusEnum> status = new LinkedList<MmbAncillaryStatusEnum>();
		status.add(MmbAncillaryStatusEnum.TO_BE_ISSUED);
		status.add(MmbAncillaryStatusEnum.ISSUED);
		status.add(MmbAncillaryStatusEnum.PREVIOUSLY_ISSUED);
		List<MmbAncillaryData> upgrade = getAncillaryByTypeStatusAndETicket(ctx, MmbAncillaryTypeEnum.UPGRADE, status, eticket);
		/*We expect only 1 upgrade in this state*/
		if(upgrade != null && upgrade.size() > 0)
			assignedUpgrade = upgrade.get(0);
		return assignedUpgrade;
	}

	/**
	 * Utility method to retrieve assigned seats in a map <E-ticket number + "C" + coupon id, Ancillary>
	 * @param ctx
	 * @return
	 */
	public Map<String, MmbAncillaryData> getAllAncillariesIssuedUpgrade(CheckinSessionContext ctx, boolean includePreviuslyIssued) {

		Map<String, MmbAncillaryData> ancillaries = new HashMap<String, MmbAncillaryData>();
		List<MmbAncillaryStatusEnum> status = new LinkedList<MmbAncillaryStatusEnum>();
		status.add(MmbAncillaryStatusEnum.TO_BE_ISSUED);
		status.add(MmbAncillaryStatusEnum.ISSUED);
		if(includePreviuslyIssued){
			status.add(MmbAncillaryStatusEnum.PREVIOUSLY_ISSUED);	
		}
		if (ctx.cart != null && ctx.cart.getAncillaries() != null) {
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
				if (MmbAncillaryTypeEnum.UPGRADE.equals(ancillary.getAncillaryType()) && 
						status.contains(ancillary.getAncillaryStatus())) {
					ancillaries.put(ancillary.getCouponNumbers().get(0), ancillary);
				}
			}
		}
		return ancillaries;
	}


	/**
	 * Retrieve available ancillary of given type by passenger's e-ticket number
	 * @param ctx
	 * @param eticketAndCouponId E-ticket number + "C" + coupon id
	 * @param type the type of wanted ancillary
	 * @return
	 */
	public MmbAncillaryData getAvailableAncillaryForType(CheckinSessionContext ctx, String eticketAndCouponId, MmbAncillaryTypeEnum type) {

		if (type != null && ctx.cart != null && ctx.cart.getAncillaries() != null) {
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
				if (MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus()) && 
						type.equals(ancillary.getAncillaryType())&&
						ancillary.getCouponNumbers() != null) {
					for (String coupon : ancillary.getCouponNumbers()) {
						if (eticketAndCouponId.equals(coupon)) {
							// We expect only 1 available ancillary of type
							return ancillary;
						}
					}
				}
			}
		}
		return null;
	}


	/**
	 * Retrieve available ancillary of type seat by passenger's e-ticket number
	 * @param ctx
	 * @param eticketAndCouponId
	 * @param isComfort
	 * @param isPayment
	 * @return
	 */
	public MmbAncillaryData getAvailableOrCurrentAncillarySeat(CheckinSessionContext ctx, String eticketAndCouponId, boolean isComfort, boolean isPayment) {

		MmbAncillaryData result = null;

		if (ctx.cart != null && ctx.cart.getAncillaries() != null) {
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
				if (MmbAncillaryTypeEnum.SEAT.equals(ancillary.getAncillaryType()) &&
						(MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus()) || 
								MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus()) ||
								MmbAncillaryStatusEnum.ISSUED.equals(ancillary.getAncillaryStatus())) &&
						ancillary.getCouponNumbers() != null) {
					for (String coupon : ancillary.getCouponNumbers()) {
						if (eticketAndCouponId.equals(coupon)) {
							// We expect only 1 available ancillary of type
							MmbAncillarySeatDetailData ancillaryDetail = (MmbAncillarySeatDetailData) ancillary.getAncillaryDetail();
							if ((isComfort && ancillaryDetail == null) ||
									(ancillaryDetail != null &&
									(new Boolean(isComfort)).equals(ancillaryDetail.isComfort()) &&
									(new Boolean(isPayment)).equals(ancillaryDetail.isPayment()))) {
								if (result == null || MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())
										|| MmbAncillaryStatusEnum.ISSUED.equals(ancillary.getAncillaryStatus())) {
									result = ancillary;
								}
							}
						}
					}
				}
			}
		}
		return result;
	}

	/**
	 * Utility method to retrieve assigned seats in a map <E-ticket number + "C" + coupon id, Ancillary>
	 * @param ctx
	 * @return
	 */
	public Map<String, MmbAncillaryData> getAncillariesAssignedSeats(CheckinSessionContext ctx) {

		Map<String, MmbAncillaryData> ancillaries = new HashMap<String, MmbAncillaryData>();

		if (ctx.cart != null && ctx.cart.getAncillaries() != null) {
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {

				if (MmbAncillaryTypeEnum.SEAT.equals(ancillary.getAncillaryType()) && 
						(MmbAncillaryStatusEnum.PREVIOUSLY_ISSUED.equals(ancillary.getAncillaryStatus()) ||
								MmbAncillaryStatusEnum.ISSUED.equals(ancillary.getAncillaryStatus()) ||
								MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) &&
						ancillary.getCouponNumbers() != null && !ancillary.getCouponNumbers().isEmpty()) {

					MmbAncillaryData previousAncillary = ancillaries.get(ancillary.getCouponNumbers().get(0));

					// Assigned seat is
					// 1. Ancillary in state ISSUED or TO_BE_ISSUED, otherwise
					// 2. Ancillary in state PREVIOUSLY_ISSUED

					if (previousAncillary == null || 
							(MmbAncillaryStatusEnum.ISSUED.equals(ancillary.getAncillaryStatus())) ||
							(MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus()))) {
						ancillaries.put(ancillary.getCouponNumbers().get(0), ancillary);
					}
				}
			}
		}
		return ancillaries;
	}

	/**
	 * Retrieve assigned seats in a map <Flight number, Set<Seat number>> from ancillaries data
	 * @param ctx
	 * @param passengers
	 * @return
	 */
	public Map<String, Set<String>> getAssignedSeats(CheckinSessionContext ctx, List<CheckinPassengerData> passengers) {

		Map<String, Set<String>> assignedSeats = new HashMap<String, Set<String>>();
		Map<String, MmbAncillaryData> ancillariesAssignedSeats = getAncillariesAssignedSeats(ctx);

		for (CheckinPassengerData passenger : passengers) {
			for (CheckinCouponData coupon : passenger.getCoupons()) {

				MmbAncillaryData ancillary;
				MmbAncillarySeatDetailData ancillaryDetail;
				if ((ancillary = ancillariesAssignedSeats.get(coupon.getEticket() + "C" + coupon.getId())) != null &&
						(ancillaryDetail = (MmbAncillarySeatDetailData) ancillary.getAncillaryDetail()) != null) {

					String flightNumber = coupon.getFlight().getFlightNumber();
					if (assignedSeats.get(flightNumber) == null) {
						assignedSeats.put(flightNumber, new HashSet<String>());
					}
					assignedSeats.get(flightNumber).add(StringUtils.stripStart(ancillaryDetail.getSeat(), "0"));
				}
			}
		}
		return assignedSeats;
	}

	/**
	 * Retrieve assigned seats in a map <Flight number, Map<Seat number, is comfort>> from ancillaries data
	 * @param ctx
	 * @param passengers
	 * @return
	 */
	public Map<String, Map<String, Boolean>> getAssignedSeatsWithComfortInfo(CheckinSessionContext ctx, List<CheckinPassengerData> passengers) {

		Map<String, Map<String, Boolean>> assignedSeats = new HashMap<String, Map<String, Boolean>>();
		Map<String, MmbAncillaryData> ancillariesAssignedSeats = getAncillariesAssignedSeats(ctx);

		for (CheckinPassengerData passenger : passengers) {
			for (CheckinCouponData coupon : passenger.getCoupons()) {

				MmbAncillaryData ancillary;
				MmbAncillarySeatDetailData ancillaryDetail;
				if ((ancillary = ancillariesAssignedSeats.get(coupon.getEticket() + "C" + coupon.getId())) != null &&
						(ancillaryDetail = (MmbAncillarySeatDetailData) ancillary.getAncillaryDetail()) != null) {

					String flightNumber = coupon.getFlight().getFlightNumber();
					String seatNumber = StringUtils.stripStart(ancillaryDetail.getSeat(), "0");
					if (assignedSeats.get(flightNumber) == null) {
						assignedSeats.put(flightNumber, new HashMap<String, Boolean>());
					}
					if (Boolean.TRUE.equals(ancillaryDetail.isComfort()) || isComfortSeat(ctx, coupon.getFlight().getFlightNumber(), seatNumber)) {
						assignedSeats.get(flightNumber).put(StringUtils.stripStart(ancillaryDetail.getSeat(), "0"), Boolean.TRUE);
					} else {
						assignedSeats.get(flightNumber).put(StringUtils.stripStart(ancillaryDetail.getSeat(), "0"), Boolean.FALSE);
					}
				}
			}
		}
		return assignedSeats;
	}

	/**
	 * Retrieve assigned seats of a passenger in a map <Flight number, Seat> from passenger data
	 * @param ctx
	 * @param passengerData
	 * @param ancillariesAssignedSeats
	 * @return
	 */
	public Map<String, CheckinPassengerSeat> getPassengerSeats(CheckinSessionContext ctx,
			CheckinPassengerData passengerData, Map<String, MmbAncillaryData> ancillariesAssignedSeats) {

		Map<String, CheckinPassengerSeat> seats = new LinkedHashMap<String, CheckinPassengerSeat>();

		for (CheckinCouponData coupon : passengerData.getCoupons()) {

			// N.B.! Se bus non preso in considerazione come richiesto
			if (!coupon.getFlight().isBus()) {
				MmbAncillaryData ancillary;
				MmbAncillarySeatDetailData ancillaryDetail;
				if ((ancillary = ancillariesAssignedSeats.get(coupon.getEticket() + "C" + coupon.getId())) != null &&
						(ancillaryDetail = (MmbAncillarySeatDetailData) ancillary.getAncillaryDetail()) != null) {

					CheckinPassengerSeat seat;
					String seatNumber = StringUtils.stripStart(ancillaryDetail.getSeat(), "0");
					if (Boolean.TRUE.equals(ancillaryDetail.isComfort()) || isComfortSeat(ctx, coupon.getFlight().getFlightNumber(), seatNumber)) {
						seat = new CheckinPassengerSeat(seatNumber, coupon.getSeatClass(), true,
								getAncillaryDisplayedPriceInfo(ctx, ancillary, 1));
					} else {
						if (Boolean.TRUE.equals(ancillaryDetail.isPayment())) {
							seat = new CheckinPassengerSeat(seatNumber, coupon.getSeatClass(), false, 
									getAncillaryDisplayedPriceInfo(ctx, ancillary, 1));
						} else {
							seat = new CheckinPassengerSeat(seatNumber, coupon.getSeatClass());
						}
					}

					seats.put(coupon.getFlight().getFlightNumber(), seat);
				}
			}
		}
		return seats;
	}

	public boolean isComfortSeat(CheckinSessionContext ctx, String flightNumber, String seat) {

		CheckinSeatMapFlight seatMapFlight;
		CheckinSeatMapRender seatMap;
		if ((seatMapFlight = ctx.seatMaps.get(flightNumber)) != null &&
				(seatMap = seatMapFlight.getSeatMaps().get(MmbCompartimentalClassEnum.Y.toString())) != null &&
				seatMap.getComfortSeats() != null) {

			logger.info("PNR/Flight {} - Posti comfort: {}", ctx.pnr + "/" + flightNumber, 
					seatMap.getComfortSeats().toString());

			if (!seatMap.getComfortSeats().isEmpty()) {
				return seatMap.getComfortSeats().contains(seat);
			}
		}
		return false;
	}

	/**
	 * Given a set of ancillary, returns the total price info to be displayed.
	 * 
	 * @param ctx The checkin session context.
	 * @param ancillaries The ancillaries.
	 * @param quantity The quantity to apply, or null to use the quantity of each ancillary.
	 * @return an MmbPriceRender instance with the data.
	 */
	public MmbPriceRender getAncillaryDisplayedPriceInfo(CheckinSessionContext ctx, MmbAncillaryData ancillary,
			Integer quantity) {
		BigDecimal totalValue = null;
		List<MmbAncillaryQuotationData> quotations = ancillary.getQuotations();
		if (quantity == null) {
			quantity = ancillary.getQuantity();
		}
		if (quotations != null) {
			for (MmbAncillaryQuotationData quotation : quotations) {
				if (quantity.intValue() >= quotation.getMaxQuantity().intValue() &&
						quantity.intValue() <= quotation.getMinQuantity().intValue()) {
					totalValue = quotation.getPrice().multiply(new BigDecimal(quantity.intValue()));
				}
			}
		}
		return new MmbPriceRender(totalValue, ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat);
	}

	public List<MmbAncillaryErrorData> removeSeatsInCart(CheckinSessionContext ctx) {

		List<Integer> itemIdsToRemove = new ArrayList<Integer>();
		List<MmbAncillaryErrorData> errors = new ArrayList<MmbAncillaryErrorData>();
		if (ctx.cart != null && ctx.cart.getAncillaries() != null) {
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
				if ((MmbAncillaryTypeEnum.SEAT.equals(ancillary.getAncillaryType()) 
						|| MmbAncillaryTypeEnum.UPGRADE.equals(ancillary.getAncillaryType())) &&
						(MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus()))) {
					itemIdsToRemove.add(new Integer(ancillary.getId()));
				}
			}
		}
		if (itemIdsToRemove.size() > 0) {
			errors = addOrRemoveAncillaryFromCart(ctx, null, itemIdsToRemove);
		}
		return errors;
	}

	/**
	 * Prepare data for ancillary page
	 * @param ctx
	 */
	public void prepareAncillaryData(CheckinSessionContext ctx) {
		initializeCartIfRequired(ctx);

		CheckinAncillarySeatStatus seatStatus = getAncillarySeatStatus(ctx);

		if (!seatStatus.equals(CheckinAncillarySeatStatus.SEAT_NOT_AVAILABLE)) {

			boolean isComfort = CheckinAncillarySeatStatus.COMFORT_SEAT_AVAILABLE.equals(seatStatus);
			ctx.seatMaps = getAllSeatMaps(ctx, isComfort);
			ctx.comfortSeatAvailable = isComfort;

			/*Add eventually Business upgraded seat*/
			MmbCompartimentalClassEnum businessClass = getBusinessCompClass(ctx);
			addSpecificClassSeatMaps(ctx, businessClass);
			/*Add eventually Premium Economy upgraded seat*/
			MmbCompartimentalClassEnum premiumEcoClass = getPremiumEcoCompClass(ctx);
			if(premiumEcoClass != null)
				addSpecificClassSeatMaps(ctx, premiumEcoClass);

			rearrangeSeatmaps(ctx);
			restorePendingAncillaries(ctx);
		}


		/*get insurance data*/
		ctx.insurancePolicyData = getInsurancePolicyData(ctx);
	}

	/**
	 * Mette per prima la seatmap economy.
	 * Per ovviare a bug in base a quale la seatmap Economy non si vede
	 * se ci sono altre seatmap prima nell'html
	 * @param ctx
	 */
	private void rearrangeSeatmaps(CheckinSessionContext ctx){
		Map<String, CheckinSeatMapFlight> seatmaps = ctx.seatMaps;
		if(seatmaps != null){
			for(String flight : seatmaps.keySet()){
				CheckinSeatMapFlight seatsFlight = seatmaps.get(flight);
				Map<String, CheckinSeatMapRender> seatMaps = seatsFlight.getSeatMaps();
				/*for(String compClass : seatMaps.keySet()){
					if(!MmbCompartimentalClassEnum.Y.value().equals(compClass)){
						CheckinSeatMapRender notEconomySeatMap = seatMaps.remove(compClass);
						if (notEconomySeatMap != null){
							seatMaps.put(compClass, notEconomySeatMap);
						}
					}
				}*/
				/*ConcurrentModificationException if doing this with a for*/
				String compClass = MmbCompartimentalClassEnum.C.value();
				CheckinSeatMapRender notEconomySeatMap = seatMaps.remove(compClass);
				if (notEconomySeatMap != null){
					seatMaps.put(compClass, notEconomySeatMap);
				}
				compClass = MmbCompartimentalClassEnum.F.value();
				notEconomySeatMap = seatMaps.remove(compClass);
				if (notEconomySeatMap != null){
					seatMaps.put(compClass, notEconomySeatMap);
				}

			}
		}
	}

	/**
	 * Vengono riaggiunti a carrello eventuali ancillary che sono stati
	 * aggiunti in sessioni precedenti e non sono stati pagati
	 * @param ctx
	 */
	private void restorePendingAncillaries(CheckinSessionContext ctx) {

		Map<String, MmbAncillaryData> assignedSeats = getAncillariesAssignedSeats(ctx);

		if (ctx.cart.getAncillaries() != null) {
			Map<Integer, String> ancillaryIds = new HashMap<Integer, String>();
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
				if (MmbAncillaryTypeEnum.SEAT.equals(ancillary.getAncillaryType()) &&
						MmbAncillaryStatusEnum.AVAILABLE.equals(ancillary.getAncillaryStatus())) {

					MmbAncillaryData ancillaryAssignedSeat = assignedSeats.get(ancillary.getCouponNumbers().get(0));

					if (ancillaryAssignedSeat != null) {
						MmbAncillarySeatDetailData ancillaryDetail = 
								(MmbAncillarySeatDetailData) ancillaryAssignedSeat.getAncillaryDetail();

						if (ancillaryDetail.getSeat() != null) {
							String flightNumber = getFlightNumberByAncillary(ctx, ancillary);
							if (ctx.comfortSeatAvailable && isComfortSeat(ctx, flightNumber, StringUtils.stripStart(ancillaryDetail.getSeat(), "0"))) {

								CheckinCouponData coupon = getCouponByAncillary(ctx, ancillaryAssignedSeat);

								if (!Boolean.TRUE.equals(coupon.getComfortSeatPaid())) {
									ancillaryIds.put(ancillary.getId(), StringUtils.stripStart(ancillaryDetail.getSeat(), "0"));
								}
							}
						}
					}
				}
			}
			if (!ancillaryIds.isEmpty()) {
				try {
					addOrUpdateSeatsInCart(ctx, ancillaryIds);
				} catch (InvalidAncillaryStatusException
						| InvalidAncillaryIdentifierException e) {
					logger.error("Error restorePendingAncillaries", e);
				}
			}
		}
	}

	private void addSpecificClassSeatMaps(CheckinSessionContext ctx, MmbCompartimentalClassEnum compClass){
		List<CheckinPassengerData> passengers = getUpgradeEligiblePassengers(ctx, compClass, true);
		if(!passengers.isEmpty()){
			Map<String, CheckinSeatMapFlight> sm = getSeatMapsFiltered(ctx, passengers, compClass, false);
			for(String flightNumber : sm.keySet()){
				if(ctx.seatMaps.get(flightNumber) != null && 
						!ctx.seatMaps.get(flightNumber).getSeatMaps().keySet().contains(compClass.value())){
					ctx.seatMaps.get(flightNumber).getSeatMaps().put(
							compClass.value(), sm.get(flightNumber).getSeatMaps().get(compClass.value()));
				}
			}
		}
	}


	public MmbCompartimentalClassEnum getBusinessCompClass(CheckinSessionContext ctx){
		MmbCompartimentalClassEnum businessClass = null;
		MmbLegTypeEnum legType = ctx.selectedRoute.getFlights().get(0).getLegType();
		List<MmbCompartimentalClassEnum> cabins = ctx.selectedRoute.getFlights().get(0).getCabins();
		if(!MmbLegTypeEnum.NONE.equals(legType)){
			if(MmbLegTypeEnum.INC.equals(legType) && cabins.contains(MmbCompartimentalClassEnum.F)){
				/*Volo intercontinentale a 3 classi*/
				businessClass = MmbCompartimentalClassEnum.F;
			}
			else{
				businessClass = MmbCompartimentalClassEnum.C;
			}
		}
		else{
			/*FIXME da rimuovere quando avranno fatto fix del legtype*/
			if(cabins.size() == 3){
				/*Volo intercontinentale a 3 classi*/
				businessClass = MmbCompartimentalClassEnum.F;
			}
			else{
				businessClass = MmbCompartimentalClassEnum.C;
			}
		}
		return businessClass;
	}

	public MmbCompartimentalClassEnum getPremiumEcoCompClass(CheckinSessionContext ctx){
		MmbCompartimentalClassEnum premiumEcoClass = null;
		MmbLegTypeEnum legType = ctx.selectedRoute.getFlights().get(0).getLegType();
		List<MmbCompartimentalClassEnum> cabins = ctx.selectedRoute.getFlights().get(0).getCabins();
		if(!MmbLegTypeEnum.NONE.equals(legType)){
			if(MmbLegTypeEnum.INC.equals(legType) && cabins.contains(MmbCompartimentalClassEnum.F)){
				/*Volo intercontinentale a 3 classi*/
				premiumEcoClass = MmbCompartimentalClassEnum.C;
			}
			else{
				/*No premium Economy*/
			}
		}
		else{
			/*FIXME da rimuovere quando avranno fatto fix del legtype*/
			if(cabins.size() == 3){
				/*Volo intercontinentale a 3 classi*/
				premiumEcoClass = MmbCompartimentalClassEnum.C;			}
			else{
				/*No premium Economy*/
			}
		}
		return premiumEcoClass;
	}



	/**
	 * 
	 * @param ctx
	 * @return
	 */
	public boolean performPrePayment(CheckinSessionContext ctx) {
		String tid = IDFactory.getTid();

		refreshCart(ctx, tid);

		if (ctx.cartTotalAmount.compareTo(BigDecimal.valueOf(0)) == 0) {

			// return false to skip the payment page (nothing to pay)
			return false;

		} else {
			// return true to land the user in payment page
			RetrievePaymentTypeItemRequest retrievePaymentTypeItemRequest = new RetrievePaymentTypeItemRequest();
			retrievePaymentTypeItemRequest.setSid(ctx.sid);
			retrievePaymentTypeItemRequest.setTid(tid);
			retrievePaymentTypeItemRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
			retrievePaymentTypeItemRequest.setMarket(ctx.market);

			RetrievePaymentTypeItemResponse retrievePaymentTypeItemResponse = 
					mmbDelegate.getPaymentTypeItem(retrievePaymentTypeItemRequest);

			ctx.creditCards = retrievePaymentTypeItemResponse.getPaymentTypeItemsData();

			if (ctx.creditCards != null) {
				for (PaymentTypeItemData card : ctx.creditCards) {
					if (card.getCode().equalsIgnoreCase("TP")) {
						ctx.creditCards.remove(card);
						break;
					}
				}
			}

			// prepare countries for nationality
			RetrieveCountriesResponse countriesResponse = null;
			try {
				RetrieveCountriesRequest countriesRequest = new RetrieveCountriesRequest(ctx.sid, tid);
				countriesRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
				countriesRequest.setMarket(ctx.market);
				countriesResponse = staticDataDelegate.retrieveCountries(countriesRequest);
			} catch (Exception e) {
				logger.error("Error by retriving countries");
			}


			// prepare countries for USA
			RetrieveStateListResponse stateListResponse = null;
			try {
				RetrieveStateListRequest stateListRequest = new RetrieveStateListRequest(ctx.sid, tid);
				stateListRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
				stateListRequest.setMarket(ctx.market);
				stateListRequest.setItemCache("US");
				stateListResponse = staticDataDelegate.retrieveStateList(stateListRequest);
			} catch (Exception e) {
				logger.error("Error by retriving american states");
			}

			ctx.countries = countriesResponse != null ? countriesResponse.getCountries() : new ArrayList<CountryData>();
			if (ctx.countries != null) {
				for (CountryData countryData : ctx.countries) {
					countryData.setDescription("countryData." + countryData.getCode());
				}
			}

			ctx.countriesUSA = stateListResponse != null ? stateListResponse.getStates() : new ArrayList<StateData>();
			if (ctx.countriesUSA != null) {
				for (StateData stateData : ctx.countriesUSA) {
					stateData.setStateDescription("stateProvinceData.us." + stateData.getStateCode());
				}
			}
			return true;
		}
	}

	/**
	 * 
	 * @param ctx
	 * @param totalDisplayedAmount
	 * @param cardNumber
	 * @param creditCardTypeName
	 * @param creditCardTypeCode
	 * @param cvv
	 * @param expirationMonth
	 * @param expirationYear
	 * @param ownerName
	 * @param ownerLastName
	 * @param countriesUSA 
	 * @param paese 
	 * @param citta 
	 * @param cap 
	 * @param indirizzo 
	 * @throws Exception 
	 */
	public void performPayment(CheckinSessionContext ctx,
			BigDecimal totalDisplayedAmount, String cardNumber, 
			String creditCardTypeName, String creditCardTypeCode, 
			String cvv, Integer expirationMonth, Integer expirationYear, 
			String ownerName, String ownerLastName, String indirizzo, 
			String cap, String citta, String paese, String countriesUSA) throws Exception {

		logger.debug("performPayment (for cart id {})", ctx.cart.getId());

		String tid = IDFactory.getTid();

		// first, refresh the cart (this will also compute again the total amount to pay)
		refreshCart(ctx, tid);

		// if available, check if the total amount to pay is consistent with the value that was presented
		if (totalDisplayedAmount != null) {
			if (!totalDisplayedAmount.equals(ctx.cartTotalAmount)) {
				// recoverable error, go back to the payment page with an error message asking to check and retry
				ctx.ancillaryPaymentSuccess = false;
				ctx.insurancePaymentSuccess = false;
				ctx.paymentErrorMessage = ctx.i18n.get(CheckinConstants.MESSAGE_ERROR_PAYMENT_WRONG_AMOUNT);
				throw new CheckinPaymentException(true, ctx.paymentErrorMessage);
			}
		}


		if (ctx.cartTotalAncillariesAmount.compareTo(BigDecimal.valueOf(0)) == 0) {
			// skip the ancillary payment
			logger.debug("Skipping ancillary payment, nothing to pay.");

		} else {
			//payment
			int numberOfAncillaryToBeIssued = 0;
			for (MmbAncillaryData ancillary : ctx.cart.getAncillaries()) {
				if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
					numberOfAncillaryToBeIssued++;
				}
			}
			logger.debug("Performing ancillary payment ({} ancillaries to be issued)", numberOfAncillaryToBeIssued);

			try {

				MmbAncillaryCheckOutInitRequest ancillaryCheckOutInitRequest = new MmbAncillaryCheckOutInitRequest();
				ancillaryCheckOutInitRequest.setSid(ctx.sid);
				ancillaryCheckOutInitRequest.setTid(tid);
				ancillaryCheckOutInitRequest.setClient(ctx.serviceClient);
				ancillaryCheckOutInitRequest.setMachineName(ctx.machineName);
				ancillaryCheckOutInitRequest.setSessionId(ctx.sid);
				ancillaryCheckOutInitRequest.setBaseInfo(createBaseInfo(ctx));
				ancillaryCheckOutInitRequest.setCartId(ctx.cart.getId());

				MmbAncillaryPaymentData ancillaryPaymentData = new MmbAncillaryPaymentData();
				ancillaryPaymentData.setCardNumber(cardNumber);
				ancillaryPaymentData.setCreditCardType(creditCardTypeCode);
				ancillaryPaymentData.setCvv(cvv);
				ancillaryPaymentData.setExpirationMonth(expirationMonth);
				ancillaryPaymentData.setExpirationYear(expirationYear);
				ancillaryPaymentData.setOwnerName(ownerName);
				ancillaryPaymentData.setOwnerLastName(ownerLastName);
				ancillaryPaymentData.setEmail(ctx.confirmationMailAddress);
				ancillaryPaymentData.setPaymentType(PaymentTypeEnum.CREDIT_CARD);

				//extra fields for AMEX
				if (creditCardTypeCode.equalsIgnoreCase("AX")) {
					ancillaryPaymentData.setAddress(indirizzo);
					ancillaryPaymentData.setCity(citta);
					ancillaryPaymentData.setCountry(paese);
					ancillaryPaymentData.setZipCode(cap);
					ancillaryPaymentData.setStare(countriesUSA);
				}

				ancillaryCheckOutInitRequest.setPayment(ancillaryPaymentData);

				MmbAncillaryCheckOutInitResponse ancillaryCheckOutInitResponse = 
						checkInDelegate.checkOutInit(ancillaryCheckOutInitRequest);

				if (ancillaryCheckOutInitResponse.getErrors() != null 
						&& ancillaryCheckOutInitResponse.getErrors().size() > 0) {
					logger.error("One or more errors was found in MMB check-out INIT response, details follow. "
							+ "(a check-out status check will be performed anyway)");
					for (MmbAncillaryErrorData error : ancillaryCheckOutInitResponse.getErrors()) {
						logger.error("Ancillary error: {} {} {}", 
								new Object[] { error.getCode(), error.getDescription(), error.getType() });
					}
				}

			} catch (Exception e) {
				logger.error("An exception was caught during MMB check-out INIT invocation "
						+ "(a check-out status check will be performed anyway)", e);
			}

			// perform the ancillary payment status check
			logger.debug("Performing ancillary payment status check");
			int ancillaryCheckCount = 0;
			final int ancillaryCheckMaxTries = 10;
			boolean ancillaryIssuingStarted = false;
			boolean ancillaryIssuingCompleted = false;
			while (!ancillaryIssuingCompleted && ancillaryCheckCount <= ancillaryCheckMaxTries) {

				ancillaryCheckCount++;
				logger.debug("Checking ancillary payment status, try {}/{}", 
						ancillaryCheckCount, ancillaryCheckMaxTries);

				try {
					if (ancillaryCheckCount > 1) {
						logger.debug("Applying thread sleep of 1 second before next try...");
						Thread.sleep(1000);
					}

					MmbAncillaryCheckOutStatusRequest ancillaryCheckOutStatusRequest = new MmbAncillaryCheckOutStatusRequest();
					ancillaryCheckOutStatusRequest.setSid(ctx.sid);
					ancillaryCheckOutStatusRequest.setTid(tid);
					ancillaryCheckOutStatusRequest.setClient(ctx.serviceClient);
					ancillaryCheckOutStatusRequest.setMachineName(ctx.machineName);
					ancillaryCheckOutStatusRequest.setSessionId(ctx.sid);
					ancillaryCheckOutStatusRequest.setBaseInfo(createBaseInfo(ctx));
					ancillaryCheckOutStatusRequest.setCartId(ctx.cart.getId());

					MmbAncillaryCheckOutStatusResponse ancillaryCheckOutStatusResponse = 
							checkInDelegate.getCheckOutStatus(ancillaryCheckOutStatusRequest);

					// count ancillaries still in to-be-issued or in error status
					int issuedAncillaries = 0;
					int stillToBeIssuedAncillaries = 0;
					int inErrorAncillaries = 0;
					if (ancillaryCheckOutStatusResponse.getAncillaries() != null) {
						for (MmbAncillaryData ancillary : ancillaryCheckOutStatusResponse.getAncillaries()) {
							if (MmbAncillaryStatusEnum.ISSUED.equals(ancillary.getAncillaryStatus())) {
								issuedAncillaries++;
								ancillaryIssuingStarted = true;
							}
							if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(ancillary.getAncillaryStatus())) {
								stillToBeIssuedAncillaries++;
							}
							if (MmbAncillaryStatusEnum.ERROR.equals(ancillary.getAncillaryStatus())) {
								inErrorAncillaries++;
							}
						}
					}
					logger.debug("Found {} issued, {} still to-be-issued and {} in error ancillaries in response.", 
							new Object[] { issuedAncillaries, stillToBeIssuedAncillaries, inErrorAncillaries });

					// verify response errors
					if (ancillaryCheckOutStatusResponse.getErrors() != null 
							&& ancillaryCheckOutStatusResponse.getErrors().size() > 0) {
						logger.error("One or more errors was found in MMB check-out STATUS response, details follow.");
						for (MmbAncillaryErrorData error : ancillaryCheckOutStatusResponse.getErrors()) {
							logger.error("Ancillary error: {} {} {}", 
									new Object[] { error.getCode(), error.getDescription(), error.getType() });
						}
					}

					if (stillToBeIssuedAncillaries == 0) {
						// no more ancillaries in to-be-issued status returned, stop status check
						logger.debug("No more ancillaries in to-be-issued status returned, stopping status check.");
						ancillaryIssuingCompleted = true;
					}

				} catch (Exception e) {
					logger.error("An exception was caught during MMB check-out STATUS invocation", e);
				}
			}

			// Ancillary status check performed
			logger.debug("Ancillary status check performed.");
			if (!ancillaryIssuingCompleted) {
				if (ancillaryIssuingStarted) {
					// ancillary issuing was started but not completed, go to thank-you page
					logger.warn("Ancillary issuing was started but not completed, go to thank-you page.");
					ctx.ancillaryPaymentSuccess = false;
					ctx.insurancePaymentSuccess = false;
					ctx.paymentErrorMessage = 
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_ANCILLARY_PAYMENT);
					throw new CheckinPaymentException(false, ctx.paymentErrorMessage);

				} else {
					// ancillary issuing seems to be not started at all, go back to payment page
					logger.warn("Ancillary issuing seems to be not started at all, go back to payment page.");
					ctx.ancillaryPaymentSuccess = false;
					ctx.insurancePaymentSuccess = false;
					ctx.paymentErrorMessage = 
							ctx.i18n.get(MmbConstants.MESSAGE_ERROR_RECOVERABLE_ANCILLARY_PAYMENT);
					throw new CheckinPaymentException(true, ctx.paymentErrorMessage);

				}

			} else {
				// ancillary issuing completed correctly
				logger.debug("Ancillary issuing completed correctly.");
				ctx.ancillaryPaymentSuccess = true;
				ctx.paymentErrorMessage = null;

			}

		}

		//Pago assicurazione
		if (!ctx.insuranceAddedToCart) {
			// skip the insurance payment
			logger.debug("Skipping insurance payment, not in cart.");

		} else {
			// perform the insurance payment
			logger.debug("Performing insurance payment");

			try {

				CheckinCreditCardHolderData creditCardHolderData = new CheckinCreditCardHolderData();
				creditCardHolderData.setFirstName(ownerName);
				creditCardHolderData.setLastName(ownerLastName);
				if (creditCardTypeCode.equalsIgnoreCase("AX")) {
					creditCardHolderData.setAddress(indirizzo);
					creditCardHolderData.setCity(citta);
					creditCardHolderData.setCountry(paese);
					creditCardHolderData.setZip(cap);
					creditCardHolderData.setState(countriesUSA);
				}

				CheckinCreditCardData creditCardData = new CheckinCreditCardData();
				creditCardData.setCreditCardHolderData(creditCardHolderData);
				creditCardData.setCvv(cvv);
				creditCardData.setExpireMonth(expirationMonth);
				creditCardData.setExpireYear(expirationYear);
				creditCardData.setNumber(cardNumber);
				creditCardData.setType(creditCardTypeCode);

				CheckinPaymentData paymentData = new CheckinPaymentData();
				paymentData.setCreditCardData(creditCardData);
				paymentData.setEmail(ctx.confirmationMailAddress);
				paymentData.setAmount(ctx.insurancePolicyData.getTotalInsuranceCost());
				paymentData.setSaleChannel(CheckinSaleChannelEnum.WEB);
				/*FIXME INSURANCE non prevista nell'enum*/
				paymentData.setSoldGood(CheckinAncillaryServiceEnum.EXTRA_BAGGAGE);
				Calendar transactionDate = Calendar.getInstance();
				transactionDate.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
				paymentData.setTransactionDate(transactionDate);

				ctx.insurancePolicyData.setBuy(true);
				ctx.insurancePolicyData.setPolicyNumber(null);
				ctx.insurancePolicyData.setPaymentData(paymentData);

				WebCheckinPayInsuranceRequest payInsuranceRequest = new WebCheckinPayInsuranceRequest();
				payInsuranceRequest.setTid(tid);
				payInsuranceRequest.setSid(ctx.sid);
				payInsuranceRequest.setSelectedRoute(ctx.selectedRoute);
				payInsuranceRequest.setInsurancePolicy(ctx.insurancePolicyData);
				payInsuranceRequest.setBaseInfo(createBaseInfo(ctx));
				payInsuranceRequest.setArea(computeLegType(ctx.selectedRoute));
				payInsuranceRequest.setClient(ctx.callerIp);
				payInsuranceRequest.setHasReturn(computeHasReturn(ctx.routesList,ctx.selectedRoute));
				payInsuranceRequest.setPassengersList(
						CheckinUtils.getCheckedInPassengers(ctx.passengers));

				CheckinInsuranceResponse payInsuranceResponse = checkInDelegate.payInsurance(payInsuranceRequest);

				if (payInsuranceResponse.getPolicy() != null &&
						!StringUtils.isBlank(payInsuranceResponse.getPolicy().getPolicyNumber())) {
					logger.debug("Insurance payment executed and policy number obtained (success).");
					ctx.insurancePaymentSuccess = true;
					ctx.paymentErrorMessage = null;
					ctx.insurancePolicyData = payInsuranceResponse.getPolicy();
					paymentInsuranceStatistic(ctx, creditCardTypeName, null, true);

				} else {
					if (payInsuranceResponse.getPolicy() == null) {
						logger.error("No policy received after insurance payment invocation (failure).");

					} else {
						logger.error("No policy number received after insurance payment invocation (failure), "
								+ "reported error description is {}, entire policy received is: {}",
								payInsuranceResponse.getPolicy().getErrorDescription(), 
								payInsuranceResponse.getPolicy());

					}
					ctx.insurancePaymentSuccess = false;
					ctx.paymentErrorMessage = ctx.i18n.get(MmbConstants.MESSAGE_ERROR_INSURANCE_PAYMENT);

					paymentInsuranceStatistic(ctx, creditCardTypeName, payInsuranceResponse.getPolicy(), false);

					throw new CheckinPaymentException(false, ctx.paymentErrorMessage);
				}

			} catch (Exception e) {
				logger.error("An exception was caught during MMB insurance pay invocation", e);
				ctx.insurancePolicyData.setBuy(false);
				ctx.insurancePaymentSuccess = false;
				throw e;
			}
		}

		// when going to thank-you-page, save credit card info for payment feedback display
		for (int i = 0; i < cardNumber.length() - 4; i++) {
			ctx.paymentCreditCardNumberHide = ctx.paymentCreditCardNumberHide + "*";
		}
		ctx.paymentCreditCardNumberHide = 
				ctx.paymentCreditCardNumberHide + cardNumber.substring(cardNumber.length() - 4);
		ctx.paymentCreditCardType = creditCardTypeName;

		//Refresh cart to show correct Ancillary in ancillary-cnfirmation
		refreshCart(ctx, tid);

		/*close the current cart*/
		ctx.cartInitialized = false;
	}


	/**
	 * Internal utility method for insurance payment statistics
	 */
	private void paymentInsuranceStatistic(CheckinSessionContext ctx, String creditCardType, 
			InsurancePolicyData insurancePolicy, boolean success) 
					throws Exception {
		logger.debug("paymentInsuranceStatistic");

		try {
			RegisterPaymentBookingInsuranceStatisticRequest bookingInsuranceStatisticRequest = 
					new RegisterPaymentBookingInsuranceStatisticRequest(IDFactory.getTid(), ctx.sid);
			bookingInsuranceStatisticRequest.setClientIP(ctx.callerIp);
			bookingInsuranceStatisticRequest.setSessionId(ctx.sid);
			bookingInsuranceStatisticRequest.setSiteCode(ctx.market);
			bookingInsuranceStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_BOOKING_INSURANCE);
			bookingInsuranceStatisticRequest.setCardType(creditCardType);
			bookingInsuranceStatisticRequest.setSuccess(success);

			if (insurancePolicy != null) {
				bookingInsuranceStatisticRequest.setPolicyNumber(insurancePolicy.getPolicyNumber());
				bookingInsuranceStatisticRequest.setErrorDescr(insurancePolicy.getErrorDescription());
			} else {
				bookingInsuranceStatisticRequest.setPolicyNumber("(unknown - PNR is " + ctx.pnr + ")");
				bookingInsuranceStatisticRequest.setErrorDescr("Null policy data received.");
			}

			RegisterStatisticsResponse statisticsResponse = 
					registerStatisticsDelegate.registerStatistics(bookingInsuranceStatisticRequest);

			logger.debug("Insurance statistics invoked, result is {}", statisticsResponse.getResult());

		} catch (Exception e) {
			logger.debug("An exception occurred invoking insurance statistics.", e);
		}
	}

	public Set<String> createSetFromProperty(String propertyName) {
		String propertyValue = PropertiesUtil.toString(componentContext.getProperties().get(propertyName), "");
		String[] setElements = propertyValue.split(",");
		Set<String> set = new HashSet<String>();
		for (String setElement : setElements) {
			set.add(setElement);
		}
		return set;
	}

	public String[] getDocumentAllowedValues(CheckinSessionContext ctx) {
		Set<String> airportList = createSetFromProperty(CheckinSession.APIS_PASSPORT_AIRPORT_CODES_PROPERTY);
		List<CheckinFlightData> flights = CheckinUtils.getFlightsList(ctx.selectedRoute);
		if (airportList != null && !airportList.isEmpty() && 
				(airportList.contains(flights.get(0).getFrom().getCode()) ||
						airportList.contains(flights.get(flights.size() - 1).getTo().getCode()))) {
			return new String[]{PassportTypeEnum.PASSPORT.value(), PassportTypeEnum.ID.value()};
		}
		return new String[]{PassportTypeEnum.PASSPORT.value()};
	}


	public List<MmbAncillaryErrorData> cancelCart(CheckinSessionContext ctx){
		String tid = IDFactory.getTid();
		WebCheckinCancelCartRequest request = new WebCheckinCancelCartRequest();
		request.setBaseInfo(createBaseInfo(ctx));
		request.setCartId(ctx.cart.getId());
		request.setClient(ctx.serviceClient);
		request.setMachineName(ctx.machineName);
		request.setSessionId(ctx.sid);
		request.setSid(ctx.sid);
		request.setTid(tid);

		WebCheckinCancelCartResponse response = checkInDelegate.cancelCart(request);

		return response.getErrors();
	}

	public void inizializeNewSeats(CheckinSessionContext ctx) {
		ctx.newSeats = new ArrayList<String>();
	}

	public void addNewSeat(CheckinSessionContext ctx, String seat) {
		ctx.newSeats.add(seat);
	}



	/******************* NUOVO SVILUPPO PER SERVIZI REST CHECKIN *******************/

	/**
	 * Inizializzazione del session context per il checkin
	 * 
	 */
	public CheckinSessionContext initializeSessionRest(SlingHttpServletRequest request, I18n i18n, String serviceClient,
			String machineName, String sid, String site, String market, String language, String conversationId, String currencyCode,
			String currencySymbol, Locale locale, String callerIp, String pnr, String ticket, String frequentFlyer, String passengerName,
			String passengerSurname, String searchType, NumberFormat currentNumberFormat,  List<String> newSeats)  throws CheckinPnrInformationNotFoundException {
		logger.debug("initializeSession");

		CheckinSessionContext ctx;
		if(!pnr.equals("") && frequentFlyer.equals("")) {
	//		rimossa clear perche' per il frontend e' impossibile fare un check preventivo sul replication db (in caso di pnr di compagnie aeree diverse)
	
	//		CheckinClearAncillarySessionEndResponse checkinClearAncillarySessionEndResponse = new CheckinClearAncillarySessionEndResponse();
	//		CheckinClearAncillarySessionEndRequest checkinClearAncillarySessionEndRequest = new CheckinClearAncillarySessionEndRequest();
	//		checkinClearAncillarySessionEndRequest.setConversationID(conversationId);
	//		checkinClearAncillarySessionEndRequest.setPnr(pnr);
	//		checkinClearAncillarySessionEndRequest.setLanguage(language);
	//		checkinClearAncillarySessionEndRequest.setMarket(market);

			if("it".equalsIgnoreCase(market) && pnr.length() == 6){
				DeleteInsuranceResponse deleteInsuranceResponse = new DeleteInsuranceResponse();
				DeleteInsuranceRequest deleteInsuranceRequest = new DeleteInsuranceRequest();
				deleteInsuranceRequest.setConversationID(conversationId);
				deleteInsuranceRequest.setPnr(pnr);
				deleteInsuranceRequest.setLanguage(language);
				deleteInsuranceRequest.setMarket(market);
	
				try {
		//			checkinClearAncillarySessionEndResponse = checkInDelegateRest.clearAncillaySessionEnd(checkinClearAncillarySessionEndRequest);
		//			if (checkinClearAncillarySessionEndResponse.getOutcome().get(0) != null) {
		//				if (!checkinClearAncillarySessionEndResponse.getOutcome().get(0).equals("OK")) {
		//					logger.warn("Si è verificato un errore nel clear ancillary e session end " + checkinClearAncillarySessionEndResponse.getOutcome().get(0));
		//				}
		//				else {
		//					logger.info("Ancillay e sessione ripulite correttamente. Outcome: " + checkinClearAncillarySessionEndResponse.getOutcome().get(0));
		//				}
		//			}
		
					deleteInsuranceResponse = insuranceDelegateRest.deleteInsurance(deleteInsuranceRequest);
					if (deleteInsuranceResponse.getSuccess() != null) {
						if (!deleteInsuranceResponse.getSuccess().equals("OK")) {
							logger.warn("Si è verificato un errore nel delete dell'assicurazione. ErroreDescription: " + deleteInsuranceResponse.getErrorDescription());
						}
						else {
							logger.info("Assicurazione eliminata correttamente.");
						}
					}
				}
				catch (Exception e) {
					logger.error("[CheckinSession] - initializeSessionRest: errore durante il clear delle informazioni della sessione " + e.getMessage());
				}
			}
		}
		// create a new CheckIn session
		ctx = new CheckinSessionContext();
		ctx.i18n = i18n;
		ctx.serviceClient = serviceClient;
		ctx.machineName = machineName;
		ctx.sid = sid;
		ctx.callerIp = callerIp;
		ctx.market = "gb".equalsIgnoreCase(market) ||
				"en".equalsIgnoreCase(market) ? market.toUpperCase() : market;
		ctx.language = language;
		ctx.conversationId = conversationId;
		ctx.site = site;
		ctx.currencyCode = currencyCode;
		ctx.currencySymbol = currencySymbol;
		ctx.locale = locale;
		ctx.searchType = searchType;
		ctx.pnr = pnr;
		ctx.eTicket = ticket;
		ctx.frequentFlyer = frequentFlyer;
		ctx.passengerName = passengerName;
		ctx.passengerSurname = passengerSurname;
		ctx.currentNumberFormat = currentNumberFormat;
		ctx.cartTotalAmount = new BigDecimal(0);
		ctx.insuranceAddedToCart = false;
		ctx.ancillaryPaymentSuccess = false;
		ctx.insurancePaymentSuccess = false;
		ctx.newSeats = newSeats;
		ctx.pnrListData = new ArrayList<>();
		ctx.checkinSelectedPassengers = new ArrayList<>();
		ctx.originalDataPassengers = new HashMap<>();
		ctx.listoriginalDataPassengers = new ArrayList<>();
		ctx.checkinSelectedPassengersExtra = new ArrayList<>();
		ctx.selectedFlights = new ArrayList<>();
		ctx.pnrSelectedListDataRender = new ArrayList<>();
		ctx.pnrListDataRender = new ArrayList<>();
		if (!CheckinConstants.CHECKIN_SEARCH_PNR.equals(ctx.searchType)) setMmcustomerData(request, ctx);

				return ctx;
	}

	/**
	 * checkinpnrsearch service
	 * 
	 */
	public CheckinPnrSearchResponse checkInPnrSearch(SlingHttpServletRequest request, CheckinSessionContext ctx) {

		CheckinPnrSearchResponse serviceResponce = null;

		try{
			logger.info("[CheckinSession] costruzione Request...");
			CheckinPnrSearchRequest serviceRequest = new CheckinPnrSearchRequest(IDFactory.getTid(), IDFactory.getSid(request));
			serviceRequest.setPnr(ctx.pnr);
			serviceRequest.setFirstName(ctx.passengerName);
			serviceRequest.setLastName(ctx.passengerSurname);
			serviceRequest.setMarket(ctx.market);
			serviceRequest.setLanguage(ctx.language);
			serviceRequest.setConversationId(ctx.conversationId);

			logger.info("[CheckinSession] serviceRequest parametri [PNR=" + ctx.pnr + "]"
					+ "[FIRSTNAME=" + ctx.passengerName + "][LASTNAME=" + ctx.passengerSurname +"]" +
					"[MARKET=" + ctx.market + "][LANGUAGE=" + ctx.language + "][CONVERSATIONID=" + ctx.conversationId + "]" );

			logger.info("[CheckinSession] Chiamata del servizio retrieveCheckinPnr...");
			serviceResponce = checkInDelegateRest.retrieveCheckinPnr(serviceRequest);
			logger.info("[CheckinSession] Ritorno del servizio retrieveCheckinPnr...");

			//Oggetto Data dopo la chiamata al servizio
			PnrListInfoByPnrSearch pnrInfoResult = serviceResponce.getPnrData();

			if(pnrInfoResult != null && pnrInfoResult.getPnr() != null && pnrInfoResult.getPnr().size() > 0){				
				//Bisogna utilizzare l'oggetto per inserirlo in un model, controller o nel contesto di sessione
				logger.info("[CheckinSession] Ritorno del servizio retrieveCheckinPnr PNR : " +  pnrInfoResult.getPnr().get(0).getNumber());
				ctx.pnrListData = pnrInfoResult.getPnr();
				if(pnrInfoResult.getConversationID() != null) {
					ctx.conversationId = pnrInfoResult.getConversationID();
				}



				// Inserisco i dettagli del volo in un parametro di sessione (flightsDetail)
				//createFlightMap(ctx, pnrInfoResult.getPnr());
			}

		}catch(Exception e){
			logger.error("[CheckinSession][checkInPnrSearch] : " + e.getMessage());
		}
		return serviceResponce;
	}

	/**
	 * checkinticketsearch service
	 * 
	 */
	public CheckinTicketSearchResponse checkinTicketSearch(SlingHttpServletRequest request, CheckinSessionContext ctx) {
		CheckinTicketSearchResponse serviceResponce = null;
		try{
			CheckinTicketSearchRequest serviceRequest = new CheckinTicketSearchRequest(IDFactory.getTid(), IDFactory.getSid(request));
			serviceRequest.setTicket(ctx.eTicket);
			serviceRequest.setLastName(ctx.passengerSurname);
			serviceRequest.setMarket(ctx.market);
			serviceRequest.setLanguage(ctx.language);
			serviceRequest.setConversationId(ctx.conversationId);

			logger.info("[CheckinSession] Chiamata del servizio retrieveCheckinTicket...");
			serviceResponce = checkInDelegateRest.retrieveCheckinTicket(serviceRequest);
			logger.info("[CheckinSession] Ritorno del servizio retrieveCheckinTicket...");

			//Oggetto Data dopo la chiamata al servizio
			PnrListInfoByTicketSearch pnrInfoResult = serviceResponce.getPnrData();

			if(pnrInfoResult != null && pnrInfoResult.getPnr() != null && pnrInfoResult.getPnr().size() > 0){			
				//Bisogna utilizzare l'oggetto per inserirlo in un model, controller o nel contesto di sessione
				logger.info("[CheckinSession] Ritorno del servizio retrieveCheckinTicket PNR : " +  pnrInfoResult.getPnr().get(0).getNumber());
				ctx.pnr = pnrInfoResult.getPnr().get(0).getNumber();
				ctx.pnrListData = pnrInfoResult.getPnr();
				if(pnrInfoResult.getConversationID() != null){
					ctx.conversationId = pnrInfoResult.getConversationID();
				}
				//				createFlightMap(ctx, pnrInfoResult.getPnr());
			}

		}catch(Exception e){
			logger.error("[CheckinSession][checkinTicketSearch] - " + e.getMessage());

		}
		return serviceResponce;
	}

	/**
	 * checkinFrequentflyersearch service
	 * 
	 */
	public CheckinFrequentFlyerSearchResponse checkinFrequentFlyerSearch(SlingHttpServletRequest request, CheckinSessionContext ctx) {
		CheckinFrequentFlyerSearchResponse serviceResponce = null;
		try{
			CheckinFrequentFlyerSearchRequest serviceRequest = new CheckinFrequentFlyerSearchRequest(IDFactory.getTid(), IDFactory.getSid(request));
			serviceRequest.setFrequentFlyer(ctx.frequentFlyer);
			serviceRequest.setLastName(ctx.passengerSurname);
			serviceRequest.setMarket(ctx.market);
			serviceRequest.setLanguage(ctx.language);
			serviceRequest.setConversationId(ctx.conversationId);

			serviceResponce = checkInDelegateRest.retrieveCheckinFrequentFlyer(serviceRequest);

			//Oggetto Data dopo la chiamata al servizio
			PnrListInfoByFrequentFlyerSearch pnrListFFInfo = serviceResponce.getPnrData();

			//Bisogna utilizzare l'oggetto per inserirlo in un model, controller o nel contesto di sessione
			logger.info("[CheckinSession] serviceRequest parametri [FREQUENT FLYER=" + ctx.pnr + "]"
					+ "[FIRSTNAME=" + ctx.passengerName + "][LASTNAME=" + ctx.passengerSurname +"]" +
					"[MARKET=" + ctx.market + "][LANGUAGE=" + ctx.language + "][CONVERSATIONID=" + ctx.conversationId + "]" );

			logger.info("[CheckinSession] Chiamata del servizio retrieveCheckinFrequentFlyerSearch...");

			if(pnrListFFInfo != null && pnrListFFInfo.getPnr() != null && pnrListFFInfo.getPnr().size() > 0){
				if(pnrListFFInfo.getConversationID() != null){
					ctx.conversationId = pnrListFFInfo.getConversationID();
				}
				ctx.pnrListData = pnrListFFInfo.getPnr();
			}
		}catch(Exception e){
			logger.error("[CheckinSession][checkinFrequentFlyerSearch] - " + e, e);
		}
		return serviceResponce;
	}

	/**
	 * checkinreprintbp service
	 * 
	 */
	public boolean checkinReprintBP(SlingHttpServletRequest request, CheckinSessionContext ctx) {
		boolean res = true;
		try{
			CheckinReprintBPRequest serviceRequest = new CheckinReprintBPRequest(IDFactory.getTid(), IDFactory.getSid(request));
			serviceRequest.setAirline(request.getParameter("airline"));
			serviceRequest.setFlight("flight");
			serviceRequest.setOrigin("origin");
			serviceRequest.setPassengerID("passengerID");
			serviceRequest.setDepartureDate("departureDate");
			serviceRequest.setLastName("lastName");

			CheckinReprintBPResponse serviceResponse = checkInDelegateRest.retrieveNewBP(serviceRequest);
			FreeTextInfoList newBP = serviceResponse.getBpData();
			ctx.freeTextInfoList = newBP;
		}
		catch(Exception e){
			res = false;
			logger.error("[CheckinSession][checkinReprintBP] - " + e.getMessage());
		}
		return res;
	}

	/**
	 * checkinisboardingpassprintable service
	 * 
	 */
	public boolean checkinIsBoardingPassPrintable(SlingHttpServletRequest request, CheckinSessionContext ctx) {
		boolean res = true;

		try{
			CheckinIsBoardingPassPrintableRequest serviceRequest = new CheckinIsBoardingPassPrintableRequest(IDFactory.getTid(), IDFactory.getSid(request));
			//TODO - prelevare i parametri di richiesta dal context
			serviceRequest.setPnr(request.getParameter("pnr"));
			serviceRequest.setFirstName(request.getParameter("firstName"));
			serviceRequest.setLastName(request.getParameter("lastName"));
			serviceRequest.setAirline(request.getParameter("airline"));
			serviceRequest.setFlight(request.getParameter("flight"));
			serviceRequest.setDepartureDate(request.getParameter("departureDate"));
			serviceRequest.setOrigin(request.getParameter("origin"));

			CheckinIsBoardingPassPrintableResponse serviceResponce = checkInDelegateRest.retrieveBoardingPassPrintable(serviceRequest);

			//Oggetto Data dopo la chiamata al servizio
			BoardingPassPrintable isbpres = serviceResponce.getIsPrintable();

			//Bisogna utilizzare l'oggetto per inserirlo in un model, controller o nel contesto di sessione
			ctx.isbpprintable = isbpres;

		}catch(Exception e){
			logger.error("[CheckinSession][checkinIsBoardingPassPrintable] - " + e.getMessage());
			res = false;
		}
		return res;
	}

	/**
	 * checkincreateboardingpass service
	 * 
	 */
	public boolean checkinCreateBoardingPass(SlingHttpServletRequest request, CheckinSessionContext ctx) {
		boolean res = true;

		try{
			CheckinCreateBoardingPassRequest serviceRequest = new CheckinCreateBoardingPassRequest(IDFactory.getTid(), IDFactory.getSid(request));
			//TODO - prelevare i parametri di richiesta dal context
			serviceRequest.setPassengerID(request.getParameter("passengerID"));
			serviceRequest.setLastName(request.getParameter("lastName"));
			serviceRequest.setAirline(request.getParameter("airline"));
			serviceRequest.setFlight(request.getParameter("flight"));
			serviceRequest.setDepartureDate(request.getParameter("departureDate"));
			serviceRequest.setOrigin(request.getParameter("origin"));

			CheckinCreateBoardingPassResponse serviceResponce = checkInDelegateRest.retrieveBoardingPass(serviceRequest);

			//Oggetto Data dopo la chiamata al servizio
			BoardingPass boardingPass = serviceResponce.getBoardingPass();

			//Bisogna utilizzare l'oggetto per inserirlo in un model, controller o nel contesto di sessione
			ctx.boardingPass = boardingPass;

		}catch(Exception e){
			logger.error("[CheckinSession][checkinCreateBoardingPass] - " + e.getMessage());
			res = false;
		}
		return res;
	}

	/**
	 * getselectedancillaryoffers service
	 * 
	 */
	public boolean checkinGetSelectedAncillaryOffers(SlingHttpServletRequest request, CheckinSessionContext ctx) {
		boolean res = true;

		try{
			CheckinGetSelectedAncillaryOffersRequest serviceRequest = new CheckinGetSelectedAncillaryOffersRequest(IDFactory.getTid(), IDFactory.getSid(request));

			serviceRequest.setConversationID(ctx.conversationId);
			serviceRequest.setLanguage(ctx.language);
			serviceRequest.setMarket(ctx.market);
			serviceRequest.setPnr(ctx.pnr);
			List<FlightPassenger> flightPassengers = new ArrayList<>();
			List<com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.request.Passenger> passengersList = new ArrayList<>();

			FlightPassenger flightPassenger;
			com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.request.Passenger passenger;

			if(ctx.selectedFlights != null && ctx.selectedFlights.size() > 0){
				for(int i=0; i<ctx.selectedFlights.size(); i++){
					flightPassenger = new FlightPassenger();
					flightPassenger.setFlight(ctx.selectedFlights.get(i).getSegments().get(0).getFlight()); //TODO - farselo passare da pagina (parametro flight).
					passenger = new com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.request.Passenger();

					if(ctx.checkinSelectedPassengers != null && ctx.checkinSelectedPassengers.size() > 0){
						for(int j=0; j<ctx.checkinSelectedPassengers.size(); j++){
							passenger.setFirstName(ctx.checkinSelectedPassengers.get(j).getNome());
							passenger.setLastName(ctx.checkinSelectedPassengers.get(j).getCognome());
							passenger.setPassengerID(ctx.checkinSelectedPassengers.get(j).getPassengerID());
							passengersList.add(passenger);
						}
						flightPassenger.setPassengers(passengersList);
					}
					else{
						logger.warn("[CheckinSession][checkinGetSelectedAncillaryOffers] - Non sono presenti passeggeri selezionati in sessione.");
						break;
					}

					flightPassengers.add(flightPassenger);
				}

				serviceRequest.setFlightPassengers(flightPassengers);
				CheckinGetSelectedAncillaryOffersResponse serviceResponce = checkInDelegateRest.retrieveSelectedAncillaryOffers(serviceRequest);

				//Oggetto Data dopo la chiamata al servizio
				AncillaryOffers ancillaryOffers = serviceResponce.getAncillaryOffers();

				//Bisogna utilizzare l'oggetto per inserirlo in un model, controller o nel contesto di sessione
				ctx.ancillaryOffers = ancillaryOffers;
				if(ancillaryOffers.getConversationID() != null){
					ctx.conversationId = ancillaryOffers.getConversationID();
				}
			}
			else{
				logger.warn("[CheckinSession][checkinGetSelectedAncillaryOffers] - Non sono presenti voli selezionati in sessione.");
			}
		}catch(Exception e){
			logger.error("[CheckinSession][checkinGetSelectedAncillaryOffers] - " + e.getMessage());
			res = false;
		}
		return res;
	}

	/**
	 * setfasttrack service
	 * 
	 */
	public FastTrack checkinSetFastTrack(SlingHttpServletRequest request, CheckinSessionContext ctx) {
		FastTrack fastTrack = new FastTrack();

		try{
			CheckinSetFastTrackRequest serviceRequest = new CheckinSetFastTrackRequest(IDFactory.getTid(), IDFactory.getSid(request));

			serviceRequest.setConversationID(ctx.conversationId);
			serviceRequest.setLanguage(ctx.language);
			serviceRequest.setMarket(ctx.market);
			serviceRequest.setPnr(ctx.pnrSelectedListDataRender.get(0).getNumber());
			List<FlightFastTrack> flightFastTracks = new ArrayList<>();
			FlightFastTrack flightFastTrack = new FlightFastTrack();

			String numero = request.getParameter("numeroPasseggeri");
			int numeroPasseggeri = Integer.parseInt(numero);
			com.alitalia.aem.common.data.checkinrest.model.fasttrack.request.Passenger fastPassenger;

			// Ciclo voli
			for(int k=0; k<ctx.selectedFlights.size(); k++)
			{

				// Coclo segmenti
				for(int z=0; z<ctx.selectedFlights.get(k).getSegments().size(); z++) {

					SegmentRender sr = ctx.selectedFlights.get(k).getSegments().get(z);

					flightFastTrack = new FlightFastTrack();
					flightFastTrack.setFlight(sr.getFlight());

					List<com.alitalia.aem.common.data.checkinrest.model.fasttrack.request.Passenger> fastPassengers = new ArrayList<>();

					for (int i = 0; i < sr.getPassengers().size(); i++)
					{
					//for (int i = 0; i < numeroPasseggeri; i++)
					//{

						if (request.getParameter("fasttrack_"  + sr.getOrigin().getCode() + "_"  + sr.getFlight() + "_" + i) != null) {
							String loungeBoolean = request.getParameter("fasttrack_" + sr.getOrigin().getCode() + "_" + sr.getFlight() + "_" + i);
							if (Boolean.valueOf(loungeBoolean)) {
								fastPassenger = new com.alitalia.aem.common.data.checkinrest.model.fasttrack.request.Passenger();
								fastPassenger.setFastTrack(true);
								fastPassenger.setLastName(request.getParameter("cognomePasseggero_" + sr.getOrigin().getCode() + "_" + sr.getFlight() + "_" + i));
								fastPassenger.setFirstName(request.getParameter("nomePasseggero_" + sr.getOrigin().getCode() + "_" + sr.getFlight() + "_" + i));
								if (ctx.checkinSelectedPassengers != null && ctx.checkinSelectedPassengers.size() > 0) {
									for (int j = 0; j < ctx.checkinSelectedPassengers.size(); j++) {
										if (request.getParameter("cognomePasseggero_" + sr.getOrigin().getCode() + "_" + sr.getFlight() + "_" + i).equals(ctx.checkinSelectedPassengers.get(j).getCognome()) &&
												request.getParameter("nomePasseggero_" + sr.getOrigin().getCode() + "_" + sr.getFlight() + "_" + i).equals(ctx.checkinSelectedPassengers.get(j).getNome()))
											//fastPassenger.setPassengerID(ctx.checkinSelectedPassengers.get(j).getPassengerID());
											fastPassenger.setPassengerID(sr.getPassengers().get(i).getPassengerID());
									}
								}
								fastPassengers.add(fastPassenger);
							}
						}

					}
					flightFastTrack.setPassengers(fastPassengers);
					if (fastPassengers.size() > 0)
						flightFastTracks.add(flightFastTrack);

				}

			}

			serviceRequest.setFlightFastTrack(flightFastTracks);

			CheckinSetFastTrackResponse serviceResponce = checkInDelegateRest.setFastTrack(serviceRequest);

			//Oggetto Data dopo la chiamata al servizio
			fastTrack = serviceResponce.getFastTrack();

			//Bisogna utilizzare l'oggetto per inserirlo in un model, controller o nel contesto di sessione
			//TODO: verificare se ci sono più fasttrack distribuiti su più pnr
			ctx.fastTrack = fastTrack;
			if(fastTrack.getConversationID() != null){
				ctx.conversationId = fastTrack.getConversationID();
			}

		}catch(Exception e){
			logger.error("[CheckinSession][checkinSetFastTrack] - " + e.getMessage());
		}
		return fastTrack;
	}

	public ClearAncillariesCart checkinClearAncillariesCart(SlingHttpServletRequest request, CheckinSessionContext ctx) {

		ClearAncillariesCart clearAncillariesCart = new ClearAncillariesCart();

		try
		{
			CheckinClearAncillariesCartRequest serviceRequest = new CheckinClearAncillariesCartRequest(IDFactory.getTid(), IDFactory.getSid(request));

			serviceRequest.setConversationID(ctx.conversationId);
			serviceRequest.setLanguage(ctx.language);
			serviceRequest.setMarket(ctx.market);
			serviceRequest.setPnr(ctx.pnrSelectedListDataRender.get(0).getNumber());

			List<String> listAncillaryId = new ArrayList<String>();

			listAncillaryId.add(request.getParameter("ancillaryId"));

			serviceRequest.setListAncillaryId(listAncillaryId);

			CheckinClearAncillariesCartResponse serviceResponce = checkInDelegateRest.clearAncillariesCart(serviceRequest);

			//Oggetto Data dopo la chiamata al servizio
			clearAncillariesCart = serviceResponce.getClearAncillariesCart();

			//Bisogna utilizzare l'oggetto per inserirlo in un model, controller o nel contesto di sessione
			// ctx.clearAncillariesCart = clearAncillariesCart;
			if (clearAncillariesCart.getConversationID() != null) {
				ctx.conversationId = clearAncillariesCart.getConversationID();
			}

		}catch(Exception e){
			logger.error("[CheckinSession][checkinSetFastTrack] - " + e.getMessage());
		}
		return clearAncillariesCart;
	}

	/**
	 * checkinselectedpassenger service
	 * 
	 */
	public CheckinSelectedPassengerResponse checkinSelectedPassenger(SlingHttpServletRequest request, CheckinSessionContext ctx) {

		//Oggetto Data utilizzato per la response del servizio
		Outcome outcome = new Outcome();
		CheckinSelectedPassengerResponse serviceResponse = new CheckinSelectedPassengerResponse();

		try{


			CheckinSelectedPassengerRequest serviceRequest = new CheckinSelectedPassengerRequest(IDFactory.getTid(), IDFactory.getSid(request));

			String pnrNumber = request.getParameter("pnr");
			String departureDate = request.getParameter("departureDate");
			String airline = request.getParameter("airline");
			String origin = request.getParameter("origin");

			List<Passenger> selectedPassengers = new ArrayList<Passenger>();
			List<PnrRender> pnrs = ctx.pnrSelectedListDataRender;
			List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger> passengersTemp = null;
			String checkinAllFlights = "";

			ctx.selectedFlights = new ArrayList<>();

			if(pnrs != null && !pnrs.isEmpty()){
				passengersTemp =  new ArrayList<>();
				for(PnrRender thisPnr : pnrs){

					if(thisPnr.getNumber().equals(pnrNumber)){

						checkinAllFlights = thisPnr.getCheckinAllFlights();

						if(thisPnr.getCheckinAllFlights() != null && checkinAllFlights.equals("N")){
							List<FlightsRender> flights = thisPnr.getFlightsRender();
							logger.info("[CheckinSession] [checkinSelectedPassenger]");
							for(FlightsRender f : flights){
								if(f.getOriginalDepartureDate().equals(departureDate)){ //La departure date è stata riabilitata per gestire i voli e i passeggeri. 
									ctx.selectedFlights.add(f);
									for(int z = 0; z < f.getSegments().size(); z++){
										logger.info("[CheckinSession] [checkinSelectedPassenger] [flight= " + f.getSegments().get(z).getFlight()+"] [openCI= " + f.getSegments().get(z).getOpenCI()+"]");
										if(f.getSegments().get(z).getOpenCI()){
											logger.info("[CheckinSession] [checkinSelectedPassenger] [flight= " + f.getSegments().get(z).getFlight()+"] [origin= " + f.getSegments().get(z).getOrigin()+"]");
											if (f.getSegments().get(z).getPassengers() != null ){
												for(int p = 0; p < f.getSegments().get(z).getPassengers().size(); p++){
													logger.info("[CheckinSession] [checkinSelectedPassenger] [passengerTemp_" + p + "= "+ f.getSegments().get(z).getPassengers().get(p).getPassengerID() + "]");
													passengersTemp.add(f.getSegments().get(z).getPassengers().get(p));
												}
											}
										}
									}
								}	
							}
						}else if(thisPnr.getCheckinAllFlights() != null && checkinAllFlights.equals("Y")){
							List<FlightsRender> flights = thisPnr.getFlightsRender();
							logger.info("[CheckinSession] [checkinSelectedPassenger]");
							for(FlightsRender f : flights){
								ctx.selectedFlights.add(f);
								for(int z = 0; z < f.getSegments().size(); z++){
									logger.info("[CheckinSession] [checkinSelectedPassenger] [flight= " + f.getSegments().get(z).getFlight()+"] [openCI= " + f.getSegments().get(z).getOpenCI()+"]");
									if(f.getSegments().get(z).getOpenCI()){
										logger.info("[CheckinSession] [checkinSelectedPassenger] [flight= " + f.getSegments().get(z).getFlight()+"] [origin= " + f.getSegments().get(z).getOrigin()+"]");
										if (f.getSegments().get(z).getPassengers() != null ) {
											for (int p = 0; p < f.getSegments().get(z).getPassengers().size(); p++) {
												logger.info("[CheckinSession] [checkinSelectedPassenger] [passengerTemp_" + p + "= " + f.getSegments().get(z).getPassengers().get(p).getPassengerID() + "]");
												passengersTemp.add(f.getSegments().get(z).getPassengers().get(p));
											}
										}
									}
								}	
							}
						}
					}
				}
			}

			Map<String, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger> thePassengers = new HashMap<String, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger>();

			if(passengersTemp != null && passengersTemp.size() > 0){

				List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger> passeggeriInSessione = new ArrayList<>();
				
				for(int i=0; i<passengersTemp.size(); i++){

					boolean selectedFlag = (request.getParameter("passenger_checked_" + i ) != null) &&
							(request.getParameter("passenger_checked_" + i ).contentEquals("true")) ? true : false;

					if (selectedFlag){
						
						com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passengerInSession = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger();
						if(checkinAllFlights.equals("N")){
							
							String name = request.getParameter("passenger_name_" + i);
							String surname = request.getParameter("passenger_surname_" + i);
							String passengerId = request.getParameter("passenger_id_" + i);
							
							logger.info("[CheckinSession] [checkinSelectedPassenger] passenger_id_" + i + " passengerID");
							if (!passengersTemp.get(i).getCheckInComplete() && passengersTemp.get(i).getNome().equals(name) && passengersTemp.get(i).getCognome().equals(surname) && passengersTemp.get(i).getPassengerID().equals(passengerId)){
								
								// ***** Passeggeri che devono effettuare il checkin ***** //
								// ***** Aggiungo alla lista solo i Passeggeri che hanno il flag 'checkinComplete' a 'false' ***** //
								Passenger myPassenger = new Passenger();
								myPassenger.setFirstName(passengersTemp.get(i).getNome());
								myPassenger.setLastName(passengersTemp.get(i).getCognome());
								myPassenger.setPassengerID(passengersTemp.get(i).getPassengerID());
								
								selectedPassengers.add(myPassenger);
								
								// ***** Passeggeri selezionati e che possono visualizzare la Seat-Map ***** //
								passengerInSession = passengersTemp.get(i);

								passeggeriInSessione.add(passengerInSession);
								
							}else{
								
								// ***** Passeggeri selezionati e che possono visualizzare la Seat-Map ***** //
								passengerInSession = passengersTemp.get(i);

								passeggeriInSessione.add(passengerInSession);
								
							}
							
						}else if(checkinAllFlights.equals("Y")){
							String name = request.getParameter("passenger_name_" + i);
							String surname = request.getParameter("passenger_surname_" + i);
							
							if (!passengersTemp.get(i).getCheckInComplete() && passengersTemp.get(i).getNome().equals(name) && passengersTemp.get(i).getCognome().equals(surname)){
								
								// ***** Passeggeri che devono effettuare il checkin ***** //
								// ***** Aggiungo alla lista solo i Passeggeri che hanno il flag 'checkinComplete' a 'false' ***** //
								Passenger myPassenger = new Passenger();
								myPassenger.setFirstName(passengersTemp.get(i).getNome());
								myPassenger.setLastName(passengersTemp.get(i).getCognome());
								myPassenger.setPassengerID(passengersTemp.get(i).getPassengerID());
								
								selectedPassengers.add(myPassenger);
								
								// ***** Passeggeri selezionati e che possono visualizzare la Seat-Map ***** //
								passengerInSession = passengersTemp.get(i);

								passeggeriInSessione.add(passengerInSession);
								
							}else{
								
								// ***** Passeggeri selezionati e che possono visualizzare la Seat-Map ***** //
								passengerInSession = passengersTemp.get(i);

								passeggeriInSessione.add(passengerInSession);
								
							}
							
							
						}

						}
					}
					ctx.checkinSelectedPassengers = passeggeriInSessione;

				if (ctx.listoriginalDataPassengers.isEmpty()) {
					for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger pas : passeggeriInSessione) {
						com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passengercopy = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger();
						passengercopy.setNome(pas.getNome());
						passengercopy.setCognome(pas.getCognome());
						passengercopy.setPassengerID(pas.getPassengerID());
						passengercopy.setCheckInComplete(pas.getCheckInComplete());

						ctx.listoriginalDataPassengers.add(passengercopy);
					}
				}

					try {



                        for(int f=0;f<ctx.selectedFlights.size();f++) {
                            // Dovranno essere inviati al servizio solo i passeggeri che sono stati selezionati e
                            // che non hanno effettuato ancora il checkin.
                            FlightsRender currentFlight= ctx.selectedFlights.get(f);
                            if (selectedPassengers.size() > 0){

                                if (f>0){  //aggiorna i passengerID al fine di effettuare il checkin per i voli successivi al primo
                                    List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger> currentPassengers = currentFlight.getSegments().get(0).getPassengers();
                                    currentPassengers=currentFlight.getSegments().get(0).getPassengers();

                                    for(int i=0;i<selectedPassengers.size();i++){
                                        com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request.Passenger p = selectedPassengers.get(i);
                                        for(int k=0;k<currentPassengers.size();k++){
                                            com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger pp =currentPassengers.get(k);
                                            if(p.getFirstName().equals(pp.getNome()) && p.getLastName().equals(pp.getCognome())){
                                                p.setPassengerID(pp.getPassengerID());
                                            }
                                        }
                                    }
                                }

                                String currentOrigin=currentFlight.getSegments().get(0).getOrigin().getCode();
                                String currentDepartureDate=currentFlight.getSegments().get(0).getOriginalDepartureDate();
                                String currentAirline=currentFlight.getSegments().get(0).getAirline();
                                String currentFlightNumber=currentFlight.getSegments().get(0).getFlight();

                                serviceRequest.setPnr(pnrNumber);

                                //serviceRequest.setAirline(airline);
                                serviceRequest.setAirline(currentAirline);

                                //serviceRequest.setOrigin(origin);
                                serviceRequest.setOrigin(currentOrigin);

                                //serviceRequest.setDepartureDate(departureDate.split("T")[0]);
                                serviceRequest.setDepartureDate(currentDepartureDate.split("T")[0]);

                                //serviceRequest.setFlight(request.getParameter("flight"));
                                serviceRequest.setFlight(currentFlightNumber);

                                serviceRequest.setLanguage(ctx.language);
                                serviceRequest.setMarket(ctx.market);
                                serviceRequest.setConversationID(ctx.conversationId);

                                // Costruzione request

                                serviceRequest.setPassengers(selectedPassengers);

                                serviceResponse = checkInDelegateRest.executePassengerCheckin(serviceRequest);
								String outcomeErrorMsg = "";

                                if (serviceResponse != null && serviceResponse.getOutcome() != null){
                                	if(serviceResponse.getOutcome().getConversationID() != null){
                                		ctx.conversationId = serviceResponse.getOutcome().getConversationID();
									}
                                    outcome = serviceResponse.getOutcome();
                                	if(outcome.getError() != null){
										outcomeErrorMsg = outcome.getError();
									}

									if (outcome != null && outcome.getOutcome() != null) {
										if (outcome.getOutcome().equalsIgnoreCase("OK")) {

											//Del passeggero il cui checkin è andato a buon fine viene aggiornato il flag checkinComplete
											updateCheckinComplete(selectedPassengers, ctx);
										}
										else if (outcome.getOutcome().equalsIgnoreCase("OK-PRIORITY")) {

											//Del passeggero il cui checkin è andato a buon fine viene aggiornato il flag checkinComplete
											updateCheckinCompletePriority(selectedPassengers, ctx);
										}
										else {
											//Ricostruisco la Stringa Json di ERRORE per poterla utilizzare nel metodo managementError della servlet CheckinSelectedPassengerServlet.
											//outcome.setError("{\"isError\":true,\"errorMessage\":\"ERRORE GENERICO\",\"sabreStatusCode\":null,\"conversationID\":null,\"callerMethod\":null}");
											outcome.setError(outcomeErrorMsg);
											outcome.setOutcome("KO");
											serviceResponse.setOutcome(outcome);
										}
									}else{
										outcome.setError(outcomeErrorMsg);
										outcome.setOutcome("KO");
										serviceResponse.setOutcome(outcome);
									}
                                }else{
                                    //Ricostruisco la Stringa Json di ERRORE per poterla utilizzare nel metodo managementError della servlet CheckinSelectedPassengerServlet.
                                    //outcome.setError("{\"isError\":true,\"errorMessage\":\"ERRORE GENERICO\",\"sabreStatusCode\":null,\"conversationID\":null,\"callerMethod\":null}");

									outcome.setError("{\"isError\":true,\"errorMessage\":\"ERRORE GENERICO\",\"sabreStatusCode\":null,\"conversationID\":null,\"callerMethod\":null}");
                                    outcome.setOutcome("KO");
                                    serviceResponse.setOutcome(outcome);
                                }

                            }else{
                                outcome.setOutcome("OK");	//I passeggeri hanno già effettuato il check-in.
                                serviceResponse.setOutcome(outcome);
                            }
                        }
					} catch (Exception e){
						logger.error("[CheckinSession][checkinSelectedPassenger] - Errore durante l'invocazione del servizio." + e.getMessage());
						//Ricostruisco la Stringa Json di ERRORE per poterla utilizzare nel metodo managementError della servlet CheckinSelectedPassengerServlet.
						//outcome.setError("{\"isError\":true,\"errorMessage\":" + outcome.getError().toString() + ",\"sabreStatusCode\":null,\"conversationID\":null,\"callerMethod\":null}");
						outcome.setError("{\"isError\":true,\"errorMessage\":\"ERRORE GENERICO\",\"sabreStatusCode\":null,\"conversationID\":null,\"callerMethod\":null}");
						outcome.setOutcome("KO");
						serviceResponse.setOutcome(outcome);
					}
				}
				else{
					logger.warn("[CheckinSession][checkinSelectedPassenger] - Nessun dato presente.");
					//Ricostruisco la Stringa Json di ERRORE per poterla utilizzare nel metodo managementError della servlet CheckinSelectedPassengerServlet.
					outcome.setError("{\"isError\":true,\"errorMessage\":\"Nessun dato presente.\",\"sabreStatusCode\":null,\"conversationID\":null,\"callerMethod\":null}");
					outcome.setOutcome("KO");
					serviceResponse.setOutcome(outcome);
				}

			}catch(Exception e){
				logger.error("[CheckinSession][checkinSelectedPassenger] - " + e.getMessage());
				//Ricostruisco la Stringa Json di ERRORE per poterla utilizzare nel metodo managementError della servlet CheckinSelectedPassengerServlet.
				outcome.setError("{\"isError\":true,\"errorMessage\":\"ERRORE GENERICO\",\"sabreStatusCode\":null,\"conversationID\":null,\"callerMethod\":null}");
				outcome.setOutcome("KO");
				serviceResponse.setOutcome(outcome);
			}

			// Svuoto array passeggeri extra selezionati
			ctx.checkinSelectedPassengersExtra = new ArrayList<>();

			// Faccio il checkin dei passeggeri "extra" selezionati

			int x1 = 0;
			int x2 = 0;
			int x3 = 0;

			for (PnrRender pnrSelectedListDataRender : ctx.pnrSelectedListDataRender)
			{
				x2 = 0;
				for (FlightsRender flight : pnrSelectedListDataRender.getFlightsRender()) {
					x3 = 0;
					for (SegmentRender segment : flight.getSegments()) {
						int j = 100;
						if (segment.getPassengersExtra() != null) {
							for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra pExtra : segment.getPassengersExtra()) {
								String ticket = request.getParameter("passenger_ticket_" + j);
								if (pExtra.getPassenger().getTicket().equals(ticket)) {

									boolean cercaPosto = false;

									if (!pExtra.getPassenger().getCheckInComplete()) {

										CheckinSelectedPassengerRequest serviceRequestExtra = new CheckinSelectedPassengerRequest(IDFactory.getTid(), IDFactory.getSid(request));

										serviceRequestExtra.setPnr(pExtra.getPrn());

										//serviceRequest.setAirline(airline);
										serviceRequestExtra.setAirline(pExtra.getAirline());

										//serviceRequest.setOrigin(origin);
										serviceRequestExtra.setOrigin(pExtra.getOriginCode());

										//serviceRequest.setDepartureDate(departureDate.split("T")[0]);
										serviceRequestExtra.setDepartureDate(pExtra.getDepartureDate().split("T")[0]);

										//serviceRequest.setFlight(request.getParameter("flight"));
										serviceRequestExtra.setFlight(pExtra.getFlightNumber());

										serviceRequestExtra.setLanguage(ctx.language);
										serviceRequestExtra.setMarket(ctx.market);
										serviceRequestExtra.setConversationID(ctx.conversationId);

										// Costruzione request

										List<Passenger> _p = new ArrayList<Passenger>();

										Passenger myPassenger = new Passenger();
										myPassenger.setFirstName(pExtra.getPassenger().getNome());
										myPassenger.setLastName(pExtra.getPassenger().getCognome());
										myPassenger.setPassengerID(pExtra.getPassenger().getPassengerID());

										_p.add(myPassenger);

										serviceRequestExtra.setPassengers(_p);

										serviceResponse = checkInDelegateRest.executePassengerCheckin(serviceRequestExtra);


										if (serviceResponse != null && serviceResponse.getOutcome() != null) {

											if(serviceResponse.getOutcome().getConversationID() != null)
											{
												ctx.conversationId = serviceResponse.getOutcome().getConversationID();
											}

											outcome = serviceResponse.getOutcome();

											if (outcome.getOutcome().equalsIgnoreCase("OK") && ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengersExtra().get(j - 100).getPassenger() != null) {

												ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengersExtra().get(j - 100).getPassenger().setCheckInComplete(true);

												cercaPosto = true;

												// * * * ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengersExtra().get(x4).getPassenger().setPostoAssegnato(true);

												//Del passeggero il cui checkin è andato a buon fine viene aggiornato il flag checkinComplete
												// updateCheckinComplete(selectedPassengers, ctx);

											} else {
												//Ricostruisco la Stringa Json di ERRORE per poterla utilizzare nel metodo managementError della servlet CheckinSelectedPassengerServlet.
												outcome.setError("{\"isError\":true,\"errorMessage\":\"ERRORE GENERICO\",\"sabreStatusCode\":null,\"conversationID\":null,\"callerMethod\":null}");
												outcome.setOutcome("KO");
												serviceResponse.setOutcome(outcome);
											}
										} else {
											//Ricostruisco la Stringa Json di ERRORE per poterla utilizzare nel metodo managementError della servlet CheckinSelectedPassengerServlet.
											outcome.setError("{\"isError\":true,\"errorMessage\":\"ERRORE GENERICO\",\"sabreStatusCode\":null,\"conversationID\":null,\"callerMethod\":null}");
											outcome.setOutcome("KO");
											serviceResponse.setOutcome(outcome);
										}
									}
									else
										cercaPosto = true;

									if (cercaPosto) {

										// Chiamata per avere il posto
										CheckinPnrSearchResponse serviceResponce = null;

										try {

											CheckinPnrSearchRequest serviceRequest = new CheckinPnrSearchRequest(IDFactory.getTid(), IDFactory.getSid(request));
											serviceRequest.setPnr(pExtra.getPrn());
											serviceRequest.setFirstName(pExtra.getPassenger().getNome());
											serviceRequest.setLastName(pExtra.getPassenger().getCognome());
											serviceRequest.setMarket(ctx.market);
											serviceRequest.setLanguage(ctx.language);
											serviceRequest.setConversationId(ctx.conversationId);

											serviceResponce = checkInDelegateRest.retrieveCheckinPnr(serviceRequest);

											// Aggiungo tutti i posti sul passeggero

											List<PassengerSeat> ps = new ArrayList<PassengerSeat>();
											for (Pnr _pnr : serviceResponce.getPnrData().getPnr()) {
												if (_pnr.getNumber().equals(pExtra.getPrn())) {
													for (Flight _flight : _pnr.getFlights()) {
														for (Segment _segment : _flight.getSegments()) {
															for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger _pass : _segment.getPassengers()) {
																PassengerSeat _ps = new PassengerSeat();
																_ps.setNumeroPostoAssegnato(_pass.getNumeroPostoAssegnato());
																_ps.setOrigin(_segment.getOrigin().getCode());
																_ps.setDepartureDate(_segment.getDepartureDate());
																_ps.setPostoAssegnato(true);
																ps.add(_ps);
															}
														}
													}
												}
											}

											//pExtra.setPassengerSeats(ps);
											pExtra.getPassenger().setPassengerSeats(ps);

											//ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengersExtra().get(j - 100).getPassenger().setNumeroPostoAssegnato(serviceResponce.getPnrData().getPnr().get(0));


										} catch (Exception q) {

										}

									}

									// Aggiungo il passeggero extra selezionato
									boolean justInsert = false;
									for(com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra _pe : ctx.checkinSelectedPassengersExtra)
									{
										if (_pe.getPassenger().getNome().equals(pExtra.getPassenger().getNome()) && _pe.getPassenger().getCognome().equals(pExtra.getPassenger().getCognome()))
											justInsert = true;
									}
									if (!justInsert)
										ctx.checkinSelectedPassengersExtra.add(pExtra);//.getPassenger());

								}
								j++;
							}
						}
						x3 += 1;
					}
					x2 += 1;
				}
				x1 += 1;
			}

			return serviceResponse;
		}

	public boolean checkinSelectedPassengerAfterPayment(SlingHttpServletRequest request, CheckinSessionContext ctx) {
		CheckinSelectedPassengerResponse serviceResponse = new CheckinSelectedPassengerResponse();
		boolean ret = true;
		try {
			CheckinSelectedPassengerRequest serviceRequest = new CheckinSelectedPassengerRequest(IDFactory.getTid(), IDFactory.getSid(request));
			String pnrNumber = ctx.pnr;
			for (int f = 0; f < ctx.selectedFlights.size(); f++) {
				FlightsRender currentFlight = ctx.selectedFlights.get(f);
				String currentOrigin = currentFlight.getSegments().get(0).getOrigin().getCode();
				String currentDepartureDate = currentFlight.getSegments().get(0).getOriginalDepartureDate();
				String currentAirline = currentFlight.getSegments().get(0).getAirline();
				String currentFlightNumber = currentFlight.getSegments().get(0).getFlight();
				
				List<com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request.Passenger> selectedPassengers = new ArrayList<>();
				for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passenger : ctx.checkinSelectedPassengers) {
					com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request.Passenger tempPassenger = new com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request.Passenger();
					tempPassenger.setFirstName(passenger.getNome());
					tempPassenger.setLastName(passenger.getCognome());
					tempPassenger.setPassengerID(passenger.getPassengerID());
					selectedPassengers.add(tempPassenger);
				}
				
				serviceRequest.setPnr(pnrNumber);
				serviceRequest.setAirline(currentAirline);
				serviceRequest.setOrigin(currentOrigin);
				serviceRequest.setDepartureDate(currentDepartureDate.split("T")[0]);
				serviceRequest.setFlight(currentFlightNumber);
				serviceRequest.setLanguage(ctx.language);
				serviceRequest.setMarket(ctx.market);
				serviceRequest.setConversationID(ctx.conversationId);
				serviceRequest.setPassengers(selectedPassengers);
				serviceResponse = checkInDelegateRest.executePassengerCheckin(serviceRequest);
				if (serviceResponse != null && serviceResponse.getOutcome() != null) {
					if (serviceResponse.getOutcome().getOutcome().equalsIgnoreCase("OK")) {
//						todo aggiornare numero posto assegnato in ctx passengers
						ret = true;
					} else {
						ret = false;
					}
				} else {
					ret = false;
				}
			}
		} catch (Exception e) {
			ret = false;
		}
		return ret;
	}

		private void updateCheckinCompletePriority(List<Passenger> selectedPassengers, CheckinSessionContext ctx) {

			for(int pnr = 0; pnr < ctx.pnrSelectedListDataRender.size(); pnr++){
				//---- Flights ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
				for(int f = 0; f < ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().size(); f++){
					//---- Segments	---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
					for(int s = 0; s < ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().size(); s++){
						//---- Passengers ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----		
						if (ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getOpenCI()){
							if (ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers() != null) {
								for(int p = 0; p < ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().size(); p++){
									//---- SelectedPassengers ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
									for(int sp = 0; sp < selectedPassengers.size(); sp++){
										logger.info("[CheckinSession] [updateCheckinComplete] [Passenger] - " + selectedPassengers.get(sp).getFirstName() + " - " + selectedPassengers.get(sp).getLastName() + " - " +selectedPassengers.get(sp).getPassengerID());
										if(selectedPassengers.get(sp).getFirstName().equals(ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().get(p).getNome()) && selectedPassengers.get(sp).getLastName().equals(ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().get(p).getCognome())){
											ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().get(p).setCheckInComplete(false);
										}
										ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().get(p).setPrecheckin(true);
									}
								}
							}
						}
					}	
				}
			}

		}

	private void updateCheckinComplete(List<Passenger> selectedPassengers, CheckinSessionContext ctx) {

		for(int pnr = 0; pnr < ctx.pnrSelectedListDataRender.size(); pnr++){
			//---- Flights ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
			for(int f = 0; f < ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().size(); f++){
				//---- Segments	---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
				for(int s = 0; s < ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().size(); s++){
					//---- Passengers ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
					if (ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getOpenCI()){
						if (ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers() != null){
							for(int p = 0; p < ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().size(); p++){
								//---- SelectedPassengers ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
								for(int sp = 0; sp < selectedPassengers.size(); sp++){
									logger.info("[CheckinSession] [updateCheckinComplete] [Passenger] - " + selectedPassengers.get(sp).getFirstName() + " - " + selectedPassengers.get(sp).getLastName() + " - " +selectedPassengers.get(sp).getPassengerID());
									if(selectedPassengers.get(sp).getFirstName().equals(ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().get(p).getNome()) && selectedPassengers.get(sp).getLastName().equals(ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().get(p).getCognome())){
										ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().get(p).setCheckInComplete(true);
									}
								}
							}
						}
					}
				}
			}
		}

	}

		/**
		 * getboardinigpassinfo service
		 * 
		 */
		public CheckinGetBoardingPassInfoResponse getBoardingPassInfo(SlingHttpServletRequest request, CheckinSessionContext ctx) {
			CheckinGetBoardingPassInfoResponse serviceResponse = new CheckinGetBoardingPassInfoResponse();
			ctx.boardingPassInfo = null;

			try {

				// Ciclo sulle varie carte d'imbarco
				for(int z=0; z< ctx.pnrSelectedListDataRender.get(0).getFlightsRender().size(); z++) {
					for (int x = 0; x < ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(z).getSegments().size(); x++) {

						CheckinGetBoardingPassInfoRequest serviceRequest = new CheckinGetBoardingPassInfoRequest(IDFactory.getTid(), IDFactory.getSid(request));
						serviceRequest.setAirline(request.getParameter("airline_" + z + x + "0"));
						serviceRequest.setConversationID(ctx.conversationId);
						serviceRequest.setDepartureDate(request.getParameter("departureDate_" + z + x + "0"));
						serviceRequest.setMarket(ctx.market);
						serviceRequest.setLanguage(ctx.language);
						serviceRequest.setFlight(request.getParameter("flight_" + z + x + "0"));
						serviceRequest.setOrigin(request.getParameter("origin_" + z + x + "0"));
						serviceRequest.setDestination(request.getParameter("destination_" + z + x + "0"));
						serviceRequest.setPnr(request.getParameter("pnr_" + z + x + "0"));
						List<String> seats = new ArrayList<String>();
						seats.add(request.getParameter("seat_" + z + x + "0"));
						serviceRequest.setSeats(seats);

						serviceResponse = checkInDelegateRest.getBoardingPassInfo(serviceRequest);

						if(serviceResponse.getBoardingPassInfo().getError() != null && !serviceResponse.getBoardingPassInfo().getError().equals("")){
							return serviceResponse;
						} else {
							BoardingPassInfo boardingPassInfo = serviceResponse.getBoardingPassInfo();

							if (ctx.boardingPassInfo == null) {
								ctx.boardingPassInfo = boardingPassInfo;
							} else {
								for (Model model : boardingPassInfo.getModels()) {
									ctx.boardingPassInfo.addModel(model);
								}
							}
						}
					}
				}

			}catch(Exception e){
				logger.error("[CheckinSession][getBoardingPassInfo] - " + e.getMessage());
			}
			return serviceResponse;
		}

	/**
	 * getboardinigpassinfobyname service
	 *
	 */
	public CheckinGetBoardingPassInfoByNameResponse getBoardingPassInfoByName (SlingHttpServletRequest request, CheckinSessionContext ctx) {
		CheckinGetBoardingPassInfoByNameResponse serviceResponse = new CheckinGetBoardingPassInfoByNameResponse();
		ctx.boardingPassInfo = null;

		try {

			// Ciclo sulle varie carte d'imbarco
			for(int z=0; z< ctx.pnrSelectedListDataRender.get(0).getFlightsRender().size(); z++) {
				for (int x = 0; x < ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(z).getSegments().size(); x++) {

					CheckinGetBoardingPassInfoByNameRequest serviceRequest = new CheckinGetBoardingPassInfoByNameRequest(IDFactory.getTid(), IDFactory.getSid(request));
					serviceRequest.setAirline(request.getParameter("airline_" + z + x + "0"));
					serviceRequest.setFlight(request.getParameter("flight_" + z + x + "0"));
					serviceRequest.setOrigin(request.getParameter("origin_" + z + x + "0"));
					serviceRequest.setDestination(request.getParameter("destination_" + z + x + "0"));
					serviceRequest.setDepartureDate(request.getParameter("departureDate_" + z + x + "0"));
					serviceRequest.setPnr(ctx.pnr);
					serviceRequest.setPassengerID(request.getParameter("passengerID_"+ z + x + "0"));
					serviceRequest.setSurname(request.getParameter("surname_"+ z + x + "0"));
					serviceRequest.setConversationID(ctx.conversationId);
					serviceRequest.setMarket(ctx.market);
					serviceRequest.setLanguage(ctx.language);

					serviceResponse = checkInDelegateRest.getBoardingPassInfoByName(serviceRequest);

					if(serviceResponse.getBoardingPassInfo().getError() != null && !serviceResponse.getBoardingPassInfo().getError().equals("")){
						return serviceResponse;
					} else {
						BoardingPassInfo boardingPassInfo = serviceResponse.getBoardingPassInfo();

						if (ctx.boardingPassInfo == null) {
							ctx.boardingPassInfo = boardingPassInfo;
						} else {
							for (Model model : boardingPassInfo.getModels()) {
								ctx.boardingPassInfo.addModel(model);
							}
						}
					}
				}
			}
		} catch(Exception e) {
			logger.error("[CheckinSession][getBoardingPassInfoByName] - " + e.getMessage());
		}
		return serviceResponse;
	}

		public boolean getMoreBoardingPassInfo(SlingHttpServletRequest request, CheckinSessionContext ctx) {
			boolean res = true;

			try
			{

				ctx.boardingPassInfo = null;

				// Ciclo sulle varie carte d'imbarco

				for (int x = 0; x < Integer.parseInt(request.getParameter("boards")); x++)
				{

					for(int z=0; z< ctx.pnrSelectedListDataRender.get(0).getFlightsRender().size(); z++) {

						for (int y = 0; y < ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(z).getSegments().size(); y++)
						{

							if (request.getParameter("airline_" + z + "_" + y + "_" + x) != null)
							{

								CheckinGetBoardingPassInfoRequest serviceRequest = new CheckinGetBoardingPassInfoRequest(IDFactory.getTid(), IDFactory.getSid(request));
								serviceRequest.setAirline(request.getParameter("airline_" + z + "_" + y + "_" + x));
								serviceRequest.setConversationID(ctx.conversationId);
								serviceRequest.setDepartureDate(request.getParameter("departureDate_" + z + "_" + y + "_" + x));
								serviceRequest.setMarket(ctx.market);
								serviceRequest.setLanguage(ctx.language);

								serviceRequest.setFlight(request.getParameter("flight_" + z + "_" + y + "_" + x));

								serviceRequest.setOrigin(request.getParameter("origin_" + z + "_" + y + "_" + x));

								serviceRequest.setDestination(request.getParameter("destination_" + z + "_" + y + "_" + x));
								serviceRequest.setPnr(request.getParameter("pnr_" + z + "_" + y + "_" + x));
								List<String> seats = new ArrayList<String>();
								seats.add(request.getParameter("seat_" + z + "_" + y + "_" + x));
								serviceRequest.setSeats(seats);

								for(String seat : seats){
									if(seat == null){
										break;
									}
								}
								CheckinGetBoardingPassInfoResponse serviceResponse = checkInDelegateRest.getBoardingPassInfo(serviceRequest);

								if(serviceResponse.getBoardingPassInfo().getError() != null && !serviceResponse.getBoardingPassInfo().getError().equals("")){
									continue;
								} else {
									BoardingPassInfo boardingPassInfo = serviceResponse.getBoardingPassInfo();
									if (ctx.boardingPassInfo == null) {
										ctx.boardingPassInfo = boardingPassInfo;
									} else {
										for (Model model : boardingPassInfo.getModels()) {
											ctx.boardingPassInfo.addModel(model);
										}
									}
								}
							}
						}
					}
				}

				if (ctx.boardingPassInfo == null){
					res = false;
				}

			}catch(Exception e){
				logger.error("[CheckinSession][getBoardingPassInfo] - " + e.getMessage());
				res = false;
			}
			return res;
		}

		public boolean getMoreBoardingPassInfoByName(SlingHttpServletRequest request, CheckinSessionContext ctx) {
			boolean res = true;

			try
			{

				ctx.boardingPassInfo = null;

				// Ciclo sulle varie carte d'imbarco

				for (int x = 0; x < Integer.parseInt(request.getParameter("boards")); x++)
				{

					for(int z=0; z< ctx.pnrSelectedListDataRender.get(0).getFlightsRender().size(); z++) {

						for (int y = 0; y < ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(z).getSegments().size(); y++) {

							CheckinGetBoardingPassInfoByNameRequest serviceRequest = new CheckinGetBoardingPassInfoByNameRequest(IDFactory.getTid(), IDFactory.getSid(request));
							serviceRequest.setAirline(request.getParameter("airline_" + z + "_" + y + "_" + x));
							serviceRequest.setConversationID(ctx.conversationId);
							serviceRequest.setDepartureDate(request.getParameter("departureDate_" + z + "_" + y + "_" + x));
							serviceRequest.setMarket(ctx.market);
							serviceRequest.setLanguage(ctx.language);
							serviceRequest.setSurname(request.getParameter("surname_" + z + "_" + y + "_" + x));
							serviceRequest.setFlight(request.getParameter("flight_" + z + "_" + y + "_" + x));
							serviceRequest.setPassengerID(request.getParameter("passengerID_" + z + "_" + y + "_" + x));
							serviceRequest.setOrigin(request.getParameter("origin_" + z + "_" + y + "_" + x));

							serviceRequest.setDestination(request.getParameter("destination_" + z + "_" + y + "_" + x));
							serviceRequest.setPnr(request.getParameter("pnr_" + z + "_" + y + "_" + x));

							if(serviceRequest.getPassengerID() == null || serviceRequest.getFlight() == null){
								break;
							}
							CheckinGetBoardingPassInfoByNameResponse serviceResponse = checkInDelegateRest.getBoardingPassInfoByName(serviceRequest);

							if(serviceResponse.getBoardingPassInfo().getError() != null && !serviceResponse.getBoardingPassInfo().getError().equals("")){
								continue;
							} else {
								BoardingPassInfo boardingPassInfo = serviceResponse.getBoardingPassInfo();
								if (ctx.boardingPassInfo == null) {
									ctx.boardingPassInfo = boardingPassInfo;
								} else {
									for (Model model : boardingPassInfo.getModels()) {
										ctx.boardingPassInfo.addModel(model);
										//models.add(model);
									}
								}
							}
						}

					}

				}

				if(!request.getParameter("e_boards").equals("")) {
					for (int x = 0; x < Integer.parseInt(request.getParameter("e_boards")); x++) {

						for (int z = 0; z < ctx.pnrSelectedListDataRender.get(0).getFlightsRender().size(); z++) {

							for (int y = 0; y < ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(z).getSegments().size(); y++) {

								CheckinGetBoardingPassInfoByNameRequest serviceRequest = new CheckinGetBoardingPassInfoByNameRequest(IDFactory.getTid(), IDFactory.getSid(request));
								serviceRequest.setAirline(request.getParameter("e_airline_" + z + "_" + y + "_" + x));
								serviceRequest.setConversationID(ctx.conversationId);
								serviceRequest.setDepartureDate(request.getParameter("e_departureDate_" + z + "_" + y + "_" + x));
								serviceRequest.setMarket(ctx.market);
								serviceRequest.setLanguage(ctx.language);
								serviceRequest.setSurname(request.getParameter("e_surname_" + z + "_" + y + "_" + x));
								serviceRequest.setFlight(request.getParameter("e_flight_" + z + "_" + y + "_" + x));
								serviceRequest.setPassengerID(request.getParameter("e_passengerID_" + z + "_" + y + "_" + x));
								serviceRequest.setOrigin(request.getParameter("e_origin_" + z + "_" + y + "_" + x));

								serviceRequest.setDestination(request.getParameter("e_destination_" + z + "_" + y + "_" + x));
								serviceRequest.setPnr(request.getParameter("e_pnr_" + z + "_" + y + "_" + x));

								if(serviceRequest.getPassengerID() == null || serviceRequest.getFlight() == null){
									break;
								}

								CheckinGetBoardingPassInfoByNameResponse serviceResponse = checkInDelegateRest.getBoardingPassInfoByName(serviceRequest);

								//Oggetto Data dopo la chiamata al servizio
								if(serviceResponse.getBoardingPassInfo().getError() != null && !serviceResponse.getBoardingPassInfo().getError().equals("")){
									continue;
								} else {
									BoardingPassInfo boardingPassInfo = serviceResponse.getBoardingPassInfo();
									if (ctx.boardingPassInfo == null) {
										ctx.boardingPassInfo = boardingPassInfo;
									} else {
										for (Model model : boardingPassInfo.getModels()) {
											ctx.boardingPassInfo.addModel(model);
											//models.add(model);
										}
									}
								}
							}
						}
					}
				}
			}catch(Exception e){
				logger.error("[CheckinSession][getBoardingPassInfo] - " + e.getMessage());
				res = false;
			}
			return res;
		}

		/**
		 * setlounge service
		 * 
		 */
		public Lounge checkinSetLounge(SlingHttpServletRequest request, CheckinSessionContext ctx) {
			Lounge lounge = null;

			try{
				CheckinSetLoungeRequest serviceRequest = new CheckinSetLoungeRequest(IDFactory.getTid(), IDFactory.getSid(request));

				serviceRequest.setConversationID(ctx.conversationId);
				serviceRequest.setLanguage(ctx.language);
				serviceRequest.setMarket(ctx.market);
				serviceRequest.setPnr(ctx.pnrSelectedListDataRender.get(0).getNumber());
				List<FlightLounge> flightLoungeList = new ArrayList<>();
				String numero = request.getParameter("numeroPasseggeri");

				int numeroPasseggeri = Integer.parseInt(numero);
				com.alitalia.aem.common.data.checkinrest.model.lounge.request.Passenger loungePassenger;
				Boolean hasOnlyFF = false;

				for(int k=0; k<ctx.selectedFlights.size(); k++){
					for(int y = 0; y<ctx.selectedFlights.get(k).getSegments().size();y++) {
						FlightLounge flightLounge = new FlightLounge();
						SegmentRender sr = ctx.selectedFlights.get(k).getSegments().get(y);
						flightLounge.setFlight(sr.getFlight());
						List<com.alitalia.aem.common.data.checkinrest.model.lounge.request.Passenger> loungePassengers = new ArrayList<>();

						//for (int i = 0; i < numeroPasseggeri; i++) {
						for (int i = 0; i < sr.getPassengers().size(); i++)
						{
							loungePassenger = new com.alitalia.aem.common.data.checkinrest.model.lounge.request.Passenger();
							if (request.getParameter("lounge_"  + sr.getOrigin().getCode() + "_" + sr.getFlight() + "_" + i) != null) {
								String loungeBoolean = request.getParameter("lounge_" + sr.getOrigin().getCode() + "_" + sr.getFlight() + "_" + i);
								if (Boolean.valueOf(loungeBoolean)) {
									loungePassenger.setLounge(Boolean.valueOf(loungeBoolean));
									loungePassenger.setLastName(request.getParameter("cognomePasseggero_" + sr.getOrigin().getCode() + "_" + sr.getFlight() + "_" + i));
									loungePassenger.setFirstName(request.getParameter("nomePasseggero_" + sr.getOrigin().getCode() + "_" + sr.getFlight() + "_" + i));
									if (Boolean.valueOf(request.getParameter("frequentFlyer_" + sr.getOrigin().getCode() + "_" + sr.getFlight() + "_" + i))) {
										hasOnlyFF = true;
									}
									if (!Boolean.valueOf(request.getParameter("frequentFlyer_" + sr.getOrigin().getCode() + "_" + sr.getFlight() + "_" + i))) {
										hasOnlyFF = false;
									}
									if (ctx.checkinSelectedPassengers != null && ctx.checkinSelectedPassengers.size() > 0) {
										for (int j = 0; j < ctx.checkinSelectedPassengers.size(); j++) {
											if (request.getParameter("cognomePasseggero_" + sr.getOrigin().getCode() + "_" + sr.getFlight() + "_" + i).equals(ctx.checkinSelectedPassengers.get(j).getCognome()) &&
													request.getParameter("nomePasseggero_" + sr.getOrigin().getCode() + "_" + sr.getFlight() + "_" + i).equals(ctx.checkinSelectedPassengers.get(j).getNome()))
												//loungePassenger.setPassengerID(ctx.checkinSelectedPassengers.get(j).getPassengerID());
												loungePassenger.setPassengerID(sr.getPassengers().get(i).getPassengerID());
										}
									}
									loungePassengers.add(loungePassenger);
								}
							}
						}
						flightLounge.setPassengers(loungePassengers);
						if(!hasOnlyFF && loungePassengers.size()>0) {
							flightLoungeList.add(flightLounge);
						}
					}
				}

				serviceRequest.setFlightLounge(flightLoungeList);

				CheckinSetLoungeResponse serviceResponce = checkInDelegateRest.setLounge(serviceRequest);

				//Oggetto Data dopo la chiamata al servizio
				lounge = serviceResponce.getLounge();

				//Bisogna utilizzare l'oggetto per inserirlo in un model, controller o nel contesto di sessione
				ctx.lounge = lounge;
				if(lounge.getConversationID() != null){
					ctx.conversationId = lounge.getConversationID();
				}

			}catch(Exception e){
				logger.error("[CheckinSession][checkinSetLounge] - " + e.getMessage());
			}
			return lounge;
		}

		/**
		 * setbaggage service
		 * 
		 */
		public Baggage checkinSetBaggage(SlingHttpServletRequest request, CheckinSessionContext ctx) {
			Baggage baggage = null;

			try{
				CheckinSetBaggageRequest serviceRequest = new CheckinSetBaggageRequest(IDFactory.getTid(), IDFactory.getSid(request));

				serviceRequest.setConversationID(ctx.conversationId);
				serviceRequest.setLanguage(ctx.language);
				serviceRequest.setMarket(ctx.market);
				serviceRequest.setPnr(ctx.pnrSelectedListDataRender.get(0).getNumber());
				List<FlightBaggage> flightBaggageList = new ArrayList<>();
				String numero = request.getParameter("numeroPasseggeri");

				int numeroPasseggeri = Integer.parseInt(numero);
				com.alitalia.aem.common.data.checkinrest.model.setbaggage.request.Passenger baggagePassenger;

				for(int k=0; k<ctx.selectedFlights.size(); k++) {
						FlightBaggage flightBaggage = new FlightBaggage();
						flightBaggage.setFlight(ctx.selectedFlights.get(k).getSegments().get(0).getFlight());
						List<com.alitalia.aem.common.data.checkinrest.model.setbaggage.request.Passenger> baggagePassengers = new ArrayList<>();

						for (int i = 0; i < numeroPasseggeri; i++) {
							baggagePassenger = new com.alitalia.aem.common.data.checkinrest.model.setbaggage.request.Passenger();
							String[] numeroBaggages = request.getParameter("baggage_" + i).split(",");
							Long numeroBagagli = Long.parseLong(numeroBaggages[k]);
							baggagePassenger.setBaggage(numeroBagagli);
							baggagePassenger.setLastName(request.getParameter("cognomePasseggero_" + i));
							baggagePassenger.setFirstName(request.getParameter("nomePasseggero_" + i));
							if (ctx.checkinSelectedPassengers != null && ctx.checkinSelectedPassengers.size() > 0) {
								for (int j = 0; j < ctx.checkinSelectedPassengers.size(); j++) {
									if (request.getParameter("cognomePasseggero_" + i).equals(ctx.checkinSelectedPassengers.get(j).getCognome()) &&
											request.getParameter("nomePasseggero_" + i).equals(ctx.checkinSelectedPassengers.get(j).getNome()))
										baggagePassenger.setPassengerID(ctx.checkinSelectedPassengers.get(j).getPassengerID());
								}
							}
							baggagePassengers.add(baggagePassenger);
						}
						flightBaggage.setPassengers(baggagePassengers);
						flightBaggageList.add(flightBaggage);
				}
				serviceRequest.setFlightBaggage(flightBaggageList);

				CheckinSetBaggageResponse serviceResponce = checkInDelegateRest.setBaggage(serviceRequest);

				//Oggetto Data dopo la chiamata al servizio
				baggage = serviceResponce.getBaggage();

				//Bisogna utilizzare l'oggetto per inserirlo in un model, controller o nel contesto di sessione
				ctx.baggage = baggage;
				if(baggage.getConversationID() != null){
					ctx.conversationId = baggage.getConversationID();
				}

			}catch(Exception e){
				logger.error("[CheckinSession][checkinSetBaggage] - " + e.getMessage());
			}
			return baggage;
		}

		public CheckinEnhancedSeatMapResponse checkinEnhancedSeatMap(SlingHttpServletRequest request, CheckinSessionContext ctx) {
			return null;
			//		CheckinEnhancedSeatMapRequest enhancedSeatMapRequest = new CheckinEnhancedSeatMapRequest(IDFactory.getTid(), IDFactory.getSid(request));
			//		EnhancedSeatMapReq enhancedSMR = new EnhancedSeatMapReq();
			//		Gson gson = new Gson();
			//		InputStream is = null;
			//		try {
			//			is = request.getInputStream();
			//		} catch (IOException e) {
			//			logger.info("checkinEnhancedSeatMap -- errore nella lettura della request da CheckinSession");
			//			e.printStackTrace();
			//		}
			//		String stringRequest = ResponseReader.retrieveResponse(is);
			//		enhancedSMR = gson.fromJson(stringRequest, EnhancedSeatMapReq.class);
			//		enhancedSMR.setConversationID(IDFactory.getTid());
			//		
			//		enhancedSeatMapRequest.set_enhancedseatmapReq(enhancedSMR);
			//		
			//		CheckinEnhancedSeatMapResponse checkinEnhancedSeatMapresponse = new CheckinEnhancedSeatMapResponse();
			//		checkinEnhancedSeatMapresponse = checkInDelegateRest.enhancedSeatMap(enhancedSeatMapRequest);
			//		
			//		return checkinEnhancedSeatMapresponse;

			//		CheckinEnhancedSeatMapRequest enhancedSeatMapRequest = new CheckinEnhancedSeatMapRequest();
			//		CheckinEnhancedSeatMapResponse checkinEnhancedSeatMapresponse = new CheckinEnhancedSeatMapResponse();
			//		try {
			//			
			//			List<ListItemsFareAvailQualifier> listOfPassengerNames = enhancedSeatMapRequest.get_enhancedseatmapReq().getListItemsFareAvailQualifiers();
			//			List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr> pnrList = ctx.pnrListData;
			//			
			//			//costruisco la request attraverso i dati che ho a disposizione nella sessione
			//			enhancedSeatMapRequest.get_enhancedseatmapReq().setLanguage(ctx.language);
			//			enhancedSeatMapRequest.get_enhancedseatmapReq().setMarket(ctx.market);
			//			enhancedSeatMapRequest.get_enhancedseatmapReq().setConversationID(ctx.conversationId);
			//			for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr pnr : pnrList) {
			//				for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Flight flight : pnr.getFlights()) {
			//					enhancedSeatMapRequest.get_enhancedseatmapReq().setOrigin(flight.getOrigin().getCityCode());
			//					enhancedSeatMapRequest.get_enhancedseatmapReq().setDestination(flight.getDestination().getCityCode());
			//					for (Segment segment : flight.getSegments()) {
			//						enhancedSeatMapRequest.get_enhancedseatmapReq().setAirline(segment.getAirline());
			//						enhancedSeatMapRequest.get_enhancedseatmapReq().setFlight(segment.getFlight());
			//						enhancedSeatMapRequest.get_enhancedseatmapReq().setDepartureDate(segment.getDepartureDate());
			//						for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passengers : segment.getPassengers()) {
			//							enhancedSeatMapRequest.get_enhancedseatmapReq().setBookingClass(passengers.getBookingClass());
			//							for (ListItemsFareAvailQualifiNames) {
			//								listIFAQ.setGivenName(passengers.getNome());
			//								listIFAQ.setSurname(passengers.getCognome());
			//							}
			//						}
			//					}
			//				}
			//			}
			//			
			//			logger.info("[CheckinSession - checkinEnhancedSeatMap] - Dati per request recuperati dalla sessione. La request è: " + enhancedSeatMapRequest.get_enhancedseatmapReq());
			//			
			//			checkinEnhancedSeatMapresponse = checkInDelegateRest.enhancedSeatMap(enhancedSeatMapRequest);
			//			
			//		}
			//		catch (Exception e) {
			//			logger.error("[CheckinSession][checkinEnhancedSeatMap] - errore nella chiamata al Delegate da CheckinSession");
			//			e.getMessage();
			//		}
			//		return checkinEnhancedSeatMapresponse;
		}

		public CheckinChangeSeatsResponse checkInChangeSeats(SlingHttpServletRequest request, CheckinSessionContext ctx) {
			return null;
		} 

		public boolean checkinUpdateSecurityInfo(SlingHttpServletRequest request, CheckinSessionContext ctx) {
			// TODO Auto-generated method stub
			return false;
		}

		public boolean checkinUpdatePassengerDetails(SlingHttpServletRequest request, CheckinSessionContext ctx) {
			// TODO Auto-generated method stub
			return false;
		}

		public CheckinUpdatePassengerResponse checkinUpdatePassenger(SlingHttpServletRequest request, CheckinSessionContext ctx) {

			CheckinUpdatePassengerResponse response = null;
//			logger.info("[CheckinSession][checkinUpdatePassenger] Entro nel metodo.");

			try{
				CheckinUpdatePassengerRequest serviceRequest = new CheckinUpdatePassengerRequest(IDFactory.getTid(), IDFactory.getSid(request));
				com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.Passenger passengerToUpdate = new com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.Passenger();

				List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger> passengersFromSession = null;
				String departureDate = request.getParameter("departureDate");
//				logger.info("[CheckinSession][checkinUpdatePassenger][departureDate=" + departureDate + "]");
				String airline = "";
				String origin = "";
				String mflight = "";
				// String passengerID = request.getParameter("passengerID");
				String name = request.getParameter("name");
				String surname = request.getParameter("surname");
				String check = name + "_" + surname;
				boolean isDestinationUS = false;

				for(Pnr pnr : ctx.pnrListData){
//					logger.info("[CheckinSession][checkinUpdatePassenger] PNR trovato: " + pnr.getNumber());

					if(pnr.getNumber().equals(ctx.pnr) || 
							((ctx.pnrSelectedListDataRender != null 
							&& ctx.pnrSelectedListDataRender.get(0) != null) &&
									(ctx.pnrSelectedListDataRender.get(0).getNumber().equals(pnr.getNumber()))))
					{
						for(Flight flight : pnr.getFlights()){
//							logger.info("[CheckinSession][checkinUpdatePassenger] FLIGHT trovato.");
							if(departureDate.equals(flight.getDepartureDate())){
//								logger.info("[CheckinSession][checkinUpdatePassenger]departureDate: " + departureDate);
								airline = flight.getSegments().get(0).getAirline();
								origin = flight.getSegments().get(0).getOrigin().getCode();
								mflight = flight.getSegments().get(0).getFlight();
								passengersFromSession = flight.getSegments().get(0).getPassengers();
								if(flight.getDestination().getCountryCode().equals("US")){
									isDestinationUS = true;
								}
//								logger.info("[CheckinSession][checkinUpdatePassenger] PASSEGGERO trovato: " + passengersFromSession.size());
								break;
							}
						}
					}
				}

				if(passengersFromSession != null){
					com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger myPassenger = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger();
					//				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger updatedPassenger = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger();


					for(int i=0; i<passengersFromSession.size(); i++)
					{

						if(check.equals(passengersFromSession.get(i).getNome() + "_" + passengersFromSession.get(i).getCognome())){
//							logger.info("[CheckinSession][checkinUpdatePassenger] PASSEGGERO [passengerID = " +
//									passengersFromSession.get(i).getPassengerID() + "] TROVATO.");
							myPassenger = passengersFromSession.get(i);
							//						updatedPassenger = passengersFromSession.get(i);
							break;
						}
					}

					String []dateSplitted = departureDate.split("T");
					serviceRequest.setDepartureDate(dateSplitted[0]);
					serviceRequest.setAirline(airline);
					serviceRequest.setFlight(mflight);
					serviceRequest.setOrigin(origin);
					serviceRequest.setLanguage(ctx.language);
					serviceRequest.setMarket(ctx.market);
					serviceRequest.setConversationID(ctx.conversationId);

					passengerToUpdate.setPassengerID(myPassenger.getPassengerID() == null ? "" : myPassenger.getPassengerID());
					passengerToUpdate.setCheckInLimited(myPassenger.getCheckInLimited());
					passengerToUpdate.setCheckInComplete(myPassenger.getCheckInComplete());
					passengerToUpdate.setNome(myPassenger.getNome());
					passengerToUpdate.setCognome(myPassenger.getCognome());
					passengerToUpdate.setBookingClass(myPassenger.getBookingClass());
					passengerToUpdate.setPostoAssegnato(myPassenger.getPostoAssegnato());
					passengerToUpdate.setNumeroPostoAssegnato(myPassenger.getNumeroPostoAssegnato());
					passengerToUpdate.setGender(request.getParameter("gender"));

					myPassenger.setGender(request.getParameter("gender"));

					List<FrequentFlyer> ffList = new ArrayList<>();
					List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.FrequentFlyer> ffListUpdated = myPassenger.getFrequentFlyer();
					FrequentFlyer ff;
					com.alitalia.aem.common.data.checkinrest.model.pnrinfo.FrequentFlyer ffUpdated;

					if(myPassenger.getFrequentFlyer() != null && myPassenger.getFrequentFlyer().size() > 0){
						for(int i=0; i<myPassenger.getFrequentFlyer().size(); i++){
							ff = new FrequentFlyer();
							ff.setCode(myPassenger.getFrequentFlyer().get(i).getCode());
							ff.setDescription(myPassenger.getFrequentFlyer().get(i).getDescription());
							ff.setNumber(myPassenger.getFrequentFlyer().get(i).getNumber());
							ff.setLenght(myPassenger.getFrequentFlyer().get(i).getLenght());
							ff.setPreFillChar(myPassenger.getFrequentFlyer().get(i).getPreFillChar());
							ff.setRegularExpressionValidation(myPassenger.getFrequentFlyer().get(i).getRegularExpressionValidation());
							ffList.add(ff);
						}
					}

					if((request.getParameter("frequentFlyer") != null) && 
							!(request.getParameter("frequentFlyer").equals(""))){
						ff = new FrequentFlyer();
						ffUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.FrequentFlyer();
						ff.setCode(request.getParameter("frequentFlyer"));
						ff.setDescription(request.getParameter("frequentFlyerDescription"));
						ff.setNumber(request.getParameter("frequentFlyerNumber"));
						ffUpdated.setCode(request.getParameter("frequentFlyer"));
						ffUpdated.setDescription(request.getParameter("frequentFlyerDescription"));
						ffUpdated.setNumber(request.getParameter("frequentFlyerNumber"));
						ffList.add(ff);
						ffListUpdated.add(ffUpdated);
					}

					passengerToUpdate.setFrequentFlyer(ffList);
					myPassenger.setFrequentFlyer(ffListUpdated);

					if (myPassenger.getAirline() == null)
						passengerToUpdate.setAirline("");
					else
						passengerToUpdate.setAirline(myPassenger.getAirline());

					if (myPassenger.getNationality() == null)
						passengerToUpdate.setNationality("");
					else
						passengerToUpdate.setNationality(myPassenger.getNationality());

					if (myPassenger.getResidence() == null)
						passengerToUpdate.setResidence("");
					else
						passengerToUpdate.setResidence(myPassenger.getResidence());

					if (myPassenger.getAddress() == null)
						passengerToUpdate.setAddress("");
					else
						passengerToUpdate.setAddress(myPassenger.getAddress());

					if (myPassenger.getCity() == null)
						passengerToUpdate.setCity("");
					else
						passengerToUpdate.setCity(myPassenger.getCity());

					if (myPassenger.getState() == null)
						passengerToUpdate.setState("");
					else
						passengerToUpdate.setState(myPassenger.getState());

					if (myPassenger.getZipCode() == null)
						passengerToUpdate.setZipCode("");
					else
						passengerToUpdate.setZipCode(myPassenger.getZipCode());

					if(myPassenger.getCodeFFchanged() != null && myPassenger.getCodeFFchanged().equals(request.getParameter("codeFFchanged"))){
						passengerToUpdate.setCodeFFchanged("");
					}
					else{
						passengerToUpdate.setCodeFFchanged(request.getParameter("codeFFchanged"));
						myPassenger.setCodeFFchanged(request.getParameter("codeFFchanged"));
					}

					if(myPassenger.getNumberFFchanged() != null && myPassenger.getCodeFFchanged().equals(request.getParameter("numberFFchanged"))){
						passengerToUpdate.setNumberFFchanged("");
					}
					else{
						passengerToUpdate.setNumberFFchanged(request.getParameter("numberFFchanged"));
						myPassenger.setNumberFFchanged(request.getParameter("numberFFchanged"));
					}

					if (myPassenger.getIsChanged() == null)
						passengerToUpdate.setIsChanged(false);
					else
						passengerToUpdate.setIsChanged(myPassenger.getIsChanged());

					if (myPassenger.getTicketStatus() == null)
						passengerToUpdate.setTicketStatus("");
					else
						passengerToUpdate.setTicketStatus(myPassenger.getTicketStatus());

					if (myPassenger.getTicket() == null)
						passengerToUpdate.setTicket("");
					else
						passengerToUpdate.setTicket(myPassenger.getTicket());

					if (myPassenger.getPassengerType() == null)
						passengerToUpdate.setPassengerType("");
					else
						passengerToUpdate.setPassengerType(myPassenger.getPassengerType());
//					evita aggiunta di piu' documenti di tipo uguale: fix errore request BL "Duplicate Types are not allowed"
					List<Document> documents = new ArrayList<>();
					for(com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Document doc : myPassenger.getDocuments()){
						boolean addDoc = true;
						if(documents.size() > 0) {
							for (Document checkDoc : documents) {
								if(checkDoc.getType().equals(doc.getType())){
									addDoc = false;
									break;
								}
							}
						}
						Document myDoc = new Document();
						myDoc.setBirthDate(doc.getBirthDate());
						myDoc.setEmissionCountry(doc.getEmissionCountry());
						myDoc.setExpirationDate(doc.getExpirationDate());
						myDoc.setFirstName(doc.getFirstName());
						myDoc.setInfant(doc.getInfant());
						myDoc.setLastName(doc.getLastName());
						myDoc.setNumber(doc.getNumber());
						myDoc.setSecondName(doc.getSecondName());
						myDoc.setType(doc.getType());
						if (addDoc) {
							documents.add(myDoc);
						}
					}
					passengerToUpdate.setDocuments(documents);

					List<DocumentRequired> documentRequiredList = new ArrayList<>();
					List<AdultDocuments> adultDocumentsList = new ArrayList<>();
					List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments> adultDocumentsListUpdated = new ArrayList<>();
					List<InfantDocuments> infantDocumentsList = new ArrayList<>();
					List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments> infantDocumentsListUpdated = new ArrayList<>();
					AdultDocumentOther adultDocumentOther = new AdultDocumentOther();
					com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocumentOther adultDocumentOtherUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocumentOther();
					InfantDocumentOther infantDocumentOther = new InfantDocumentOther();
					com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocumentOther infantDocumentOtherUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocumentOther();
					AdultResidentDocument adultResidentDocument = new AdultResidentDocument();
					com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultResidentDocument adultResidentDocumentUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultResidentDocument();
					InfantResidentDocument infantResidentDocument = new InfantResidentDocument();
					com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantResidentDocument infantResidentDocumentUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantResidentDocument();
					AdultDestinationDocument adultDestinationDocument = new AdultDestinationDocument();
					com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDestinationDocument adultDestinationDocumentUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDestinationDocument();
					InfantDestinationDocument infantDestinationDocument = new InfantDestinationDocument();
					com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDestinationDocument infantDestinationDocumentUpdated =  new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDestinationDocument();

					passengerToUpdate.setAdultDocumentsSRequired(false);
					passengerToUpdate.setInfantDocumentsSRequired(false);
					passengerToUpdate.setAdultDocumentOtherRequired(false);
					passengerToUpdate.setInfantDocumentOtherRequired(false);
					passengerToUpdate.setAdultResidentDocumentRequired(false);
					passengerToUpdate.setInfantResidentDocumentRequired(false);
					passengerToUpdate.setAdultDestinationDocumentRequired(false);
					passengerToUpdate.setInfantDestinationDocumentRequired(false);
					passengerToUpdate.setAdultDocVRequired(false);
					passengerToUpdate.setInfantDocVRequired(false);

					myPassenger.setAdultDocumentsSRequired(false);
					myPassenger.setInfantDocumentsSRequired(false);
					myPassenger.setAdultDocumentOtherRequired(false);
					myPassenger.setInfantDocumentOtherRequired(false);
					myPassenger.setAdultResidentDocumentRequired(false);
					myPassenger.setInfantResidentDocumentRequired(false);
					myPassenger.setAdultDestinationDocumentRequired(false);
					myPassenger.setInfantDestinationDocumentRequired(false);
					myPassenger.setAdultDocVRequired(false);
					myPassenger.setInfantDocVRequired(false);

					boolean docsAdded = false;
					boolean docsAddedInf = false;
					boolean docsEstaAdded = false;
					boolean docsEstaAddedInf = false;

					if(myPassenger.getDocumentRequired() != null && myPassenger.getDocumentRequired().size() > 0){

						boolean adInsert = false;
						boolean estaInfant = false;

						for(int k=0; k<myPassenger.getDocumentRequired().size(); k++) {
							DocumentRequired docr = new DocumentRequired();
							docr.setCode(myPassenger.getDocumentRequired().get(k).getCode());
							if(docr.getCode().equals("DOCS/INF")){
								estaInfant = true;
							}
						}

						for(int k=0; k<myPassenger.getDocumentRequired().size(); k++){
							DocumentRequired documentRequired = new DocumentRequired();
							documentRequired.setCode(myPassenger.getDocumentRequired().get(k).getCode());
							documentRequired.setDetailStatus(myPassenger.getDocumentRequired().get(k).getDetailStatus());
							documentRequired.setFreeText(myPassenger.getDocumentRequired().get(0).getFreeText());
							documentRequiredList.add(documentRequired);

							if(documentRequired.getCode().equals("DOCS-P") || documentRequired.getCode().equals("DOCS-C") )
							{	//TODO - cablatura!
								if (!adInsert)
								{
									passengerToUpdate.setAdultDocumentsSRequired(true);
									myPassenger.setAdultDocumentsSRequired(true);
									AdultDocuments adultDocuments = new AdultDocuments();
									com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments adultDocumentsToUpdate = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments();
									String adultDocs = (request.getParameter("numberOfAdultDocuments") == "" || request.getParameter("numberOfAdultDocuments") == null) ? "0" : request.getParameter("numberOfAdultDocuments");
									int numberOfAdultDocuments = Integer.parseInt(adultDocs);

									for (int p = 0; p < numberOfAdultDocuments; p++) {
										adultDocuments.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
										adultDocumentsToUpdate.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
										adultDocuments.setExpirationDate(request.getParameter("expirationDate_" + p));
										adultDocumentsToUpdate.setExpirationDate(request.getParameter("expirationDate_" + p));
										adultDocuments.setFirstName(request.getParameter("firstName_" + p));
										adultDocumentsToUpdate.setFirstName(request.getParameter("firstName_" + p));
										adultDocuments.setGender(request.getParameter("gender_" + p));
										adultDocumentsToUpdate.setGender(request.getParameter("gender_" + p));
										adultDocuments.setIssueCountry(request.getParameter("issueCountry_" + p));
										adultDocumentsToUpdate.setIssueCountry(request.getParameter("issueCountry_" + p));
										adultDocuments.setLastName(request.getParameter("lastName_" + p));
										adultDocumentsToUpdate.setLastName(request.getParameter("lastName_" + p));
										adultDocuments.setNationality(request.getParameter("nationality_" + p));
										adultDocumentsToUpdate.setNationality(request.getParameter("nationality_" + p));
										adultDocuments.setNumber(request.getParameter("number_" + p));
										adultDocumentsToUpdate.setNumber(request.getParameter("number_" + p));
										adultDocuments.setType(request.getParameter("type_" + p));
										adultDocumentsToUpdate.setType(request.getParameter("type_" + p));
										adultDocumentsList.add(adultDocuments);
										adultDocumentsListUpdated.add(adultDocumentsToUpdate);
										adInsert = true;
										docsAdded = true;
									}
								}
							}
							if(documentRequired.getCode().equals("DOCS/INF")){ 				//TODO - cablatura!
								passengerToUpdate.setInfantDocumentsSRequired(true);
								myPassenger.setInfantDocumentsSRequired(true);
								InfantDocuments infantDocuments = new InfantDocuments();
								com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments infantDocumentsToUpdate = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments();
								String infantDocs = (request.getParameter("numberOfInfantDocuments") == "" || request.getParameter("numberOfInfantDocuments") == null) ? "0" :  request.getParameter("numberOfInfantDocuments");
								int numberOfInfantDocuments = Integer.parseInt(infantDocs);
								for(int t=0; t<numberOfInfantDocuments; t++){
									infantDocuments.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
									infantDocumentsToUpdate.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
									infantDocuments.setExpirationDate(request.getParameter("expirationDateInfant_" + t));
									infantDocumentsToUpdate.setExpirationDate(request.getParameter("expirationDateInfant_" + t));
									infantDocuments.setFirstName(request.getParameter("firstNameInfant_" + t));
									infantDocumentsToUpdate.setFirstName(request.getParameter("firstNameInfant_" + t));
									infantDocuments.setGender(request.getParameter("genderInfant_" + t));
									infantDocumentsToUpdate.setGender(request.getParameter("genderInfant_" + t));
									infantDocuments.setIssueCountry(request.getParameter("issueCountryInfant_" + t));
									infantDocumentsToUpdate.setIssueCountry(request.getParameter("issueCountryInfant_" + t));
									infantDocuments.setLastName(request.getParameter("lastNameInfant_" + t));
									infantDocumentsToUpdate.setLastName(request.getParameter("lastNameInfant_" + t));
									infantDocuments.setNationality(request.getParameter("nationalityInfant_" + t));
									infantDocumentsToUpdate.setNationality(request.getParameter("nationalityInfant_" + t));
									infantDocuments.setNumber(request.getParameter("numberInfant_" + t));
									infantDocumentsToUpdate.setNumber(request.getParameter("numberInfant_" + t));
									infantDocuments.setType(request.getParameter("typeInfant_" + t));
									infantDocumentsToUpdate.setType(request.getParameter("typeInfant_" + t));
									infantDocumentsList.add(infantDocuments);
									infantDocumentsListUpdated.add(infantDocumentsToUpdate);
									docsAddedInf = true;
								}
							}
							if(documentRequired.getCode().equals("ESTA")){ 					//TODO - cablatura!
								String documentType = request.getParameter("documentTypeAdult");
								if(documentType.equals("esta")){
									passengerToUpdate.setAdultDocumentOtherRequired(false);
									myPassenger.setAdultDocumentOtherRequired(false);
								}
								if(documentType.equals("visto")){
									passengerToUpdate.setAdultDocumentOtherRequired(true);
									myPassenger.setAdultDocumentOtherRequired(true);
									adultDocumentOther.setApplicableCountryCode(request.getParameter("applicableCountryCodeAdult"));
									adultDocumentOtherUpdated.setApplicableCountryCode(request.getParameter("applicableCountryCodeAdult"));
									adultDocumentOther.setBirthPlace("");
									adultDocumentOtherUpdated.setBirthPlace("");
									adultDocumentOther.setIssueCity(request.getParameter("issueCityAdult"));
									adultDocumentOtherUpdated.setIssueCity(request.getParameter("issueCityAdult"));
									adultDocumentOther.setIssueDate(request.getParameter("issueDateAdult"));
									adultDocumentOtherUpdated.setIssueDate(request.getParameter("issueDateAdult"));
									adultDocumentOther.setNumber(request.getParameter("numberOtherDocumentAdult"));
									adultDocumentOtherUpdated.setNumber(request.getParameter("numberOtherDocumentAdult"));
								}
								if(documentType.equals("green")){
									AdultDocuments adultDocumentsGreenCard = new AdultDocuments();
									com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments adultDocumentsToUpdateGreenCard = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments();
									String adultDocs = (request.getParameter("numberOfAdultDocuments") == "" || request.getParameter("numberOfAdultDocuments") == null) ? "0" :  request.getParameter("numberOfAdultDocuments");
									int numberOfAdultDocuments = Integer.parseInt(adultDocs);

									for(int p=0; p<numberOfAdultDocuments; p++){
										adultDocumentsGreenCard.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
										adultDocumentsToUpdateGreenCard.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
										adultDocumentsGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardAdult_" + p));
										adultDocumentsToUpdateGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardAdult_" + p));
										adultDocumentsGreenCard.setFirstName(request.getParameter("firstName_" + p));
										adultDocumentsToUpdateGreenCard.setFirstName(request.getParameter("firstName_" + p));
										adultDocumentsGreenCard.setGender(request.getParameter("gender_" + p));
										adultDocumentsToUpdateGreenCard.setGender(request.getParameter("gender_" + p));
										adultDocumentsGreenCard.setIssueCountry("US");
										adultDocumentsToUpdateGreenCard.setIssueCountry("US");
										adultDocumentsGreenCard.setLastName(request.getParameter("lastName_" + p));
										adultDocumentsToUpdateGreenCard.setLastName(request.getParameter("lastName_" + p));
										adultDocumentsGreenCard.setNationality("US");
										adultDocumentsToUpdateGreenCard.setNationality("US");
										adultDocumentsGreenCard.setNumber(request.getParameter("numberGreenCardAdult_" + p));
										adultDocumentsToUpdateGreenCard.setNumber(request.getParameter("numberGreenCardAdult_" + p));
										adultDocumentsGreenCard.setType("GREENCARD");
										adultDocumentsToUpdateGreenCard.setType("GREENCARD");
										adultDocumentsList.add(adultDocumentsGreenCard);
										adultDocumentsListUpdated.add(adultDocumentsToUpdateGreenCard);
									}
								}
								docsEstaAdded = true;
							}
							if(documentRequired.getCode().equals("ESTA") && estaInfant == true){				//TODO - cablatura!
								String documentTypeInfant = request.getParameter("documentTypeInfant");
								if(documentTypeInfant.equals("esta")){
									passengerToUpdate.setInfantDocumentOtherRequired(false);
									myPassenger.setInfantDocumentOtherRequired(false);
								}
								if(documentTypeInfant.equals("visto")){
									passengerToUpdate.setInfantDocumentOtherRequired(true);
									myPassenger.setInfantDocumentOtherRequired(true);
									infantDocumentOther.setApplicableCountryCode(request.getParameter("applicableCountryCodeInfant"));
									infantDocumentOtherUpdated.setApplicableCountryCode(request.getParameter("applicableCountryCodeInfant"));
									infantDocumentOther.setBirthPlace("");
									infantDocumentOtherUpdated.setBirthPlace("");
									infantDocumentOther.setIssueCity(request.getParameter("issueCityInfant"));
									infantDocumentOtherUpdated.setIssueCity(request.getParameter("issueCityInfant"));
									infantDocumentOther.setIssueDate(request.getParameter("issueDateInfant"));
									infantDocumentOtherUpdated.setIssueDate(request.getParameter("issueDateInfant"));
									infantDocumentOther.setNumber(request.getParameter("numberOtherDocumentInfant"));
									infantDocumentOtherUpdated.setNumber(request.getParameter("numberOtherDocumentInfant"));
								}
								if(documentTypeInfant.equals("green")){
									InfantDocuments infantDocumentsGreenCard = new InfantDocuments();
									com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments infantDocumentsToUpdateGreenCard = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments();
									String infantDocs = (request.getParameter("numberOfInfantDocuments") == "" || request.getParameter("numberOfInfantDocuments") == null) ? "0" :  request.getParameter("numberOfInfantDocuments");
									int numberOfInfantDocuments = Integer.parseInt(infantDocs);
									for(int t=0; t<numberOfInfantDocuments; t++){
										infantDocumentsGreenCard.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
										infantDocumentsToUpdateGreenCard.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
										infantDocumentsGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardInfant_" + t));
										infantDocumentsToUpdateGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardInfant_" + t));
										infantDocumentsGreenCard.setFirstName(request.getParameter("firstNameInfant_" + t));
										infantDocumentsToUpdateGreenCard.setFirstName(request.getParameter("firstNameInfant_" + t));
										infantDocumentsGreenCard.setGender(request.getParameter("genderInfant_" + t));
										infantDocumentsToUpdateGreenCard.setGender(request.getParameter("genderInfant_" + t));
										infantDocumentsGreenCard.setIssueCountry("US");
										infantDocumentsToUpdateGreenCard.setIssueCountry("US");
										infantDocumentsGreenCard.setLastName(request.getParameter("lastNameInfant_" + t));
										infantDocumentsToUpdateGreenCard.setLastName(request.getParameter("lastNameInfant_" + t));
										infantDocumentsGreenCard.setNationality("US");
										infantDocumentsToUpdateGreenCard.setNationality("US");
										infantDocumentsGreenCard.setNumber(request.getParameter("numberGreenCardInfant_" + t));
										infantDocumentsToUpdateGreenCard.setNumber(request.getParameter("numberGreenCardInfant" + t));
										infantDocumentsGreenCard.setType("GREENCARD");
										infantDocumentsToUpdateGreenCard.setType("GREENCARD");
										infantDocumentsList.add(infantDocumentsGreenCard);
										infantDocumentsListUpdated.add(infantDocumentsToUpdateGreenCard);
									}
								}
								docsEstaAddedInf = true;
							}

							if(documentRequired.getCode().equals("DOCA/R")){
								if(request.getParameter("issueCountry_0").equals("US") && request.getParameter("nationality_0").equals("US")){
									passengerToUpdate.setAdultResidentDocumentRequired(false);
									myPassenger.setAdultResidentDocumentRequired(false);
								}else{
									passengerToUpdate.setAdultResidentDocumentRequired(true);
									myPassenger.setAdultResidentDocumentRequired(true);
									adultResidentDocument.setCity(request.getParameter("cityResident"));
									adultResidentDocumentUpdated.setCity(request.getParameter("cityResident"));
									adultResidentDocument.setCountryCode(request.getParameter("countryCodeResident"));
									adultResidentDocumentUpdated.setCountryCode(request.getParameter("countryCodeResident"));
									adultResidentDocument.setStateProvince(request.getParameter("stateProvinceResident"));
									adultResidentDocumentUpdated.setStateProvince(request.getParameter("stateProvinceResident"));
									adultResidentDocument.setStreetAddress(request.getParameter("streetAddressResident"));
									adultResidentDocumentUpdated.setStreetAddress(request.getParameter("streetAddressResident"));
									adultResidentDocument.setZipCode(request.getParameter("zipCodeAdultResident"));
									adultResidentDocumentUpdated.setZipCode(request.getParameter("zipCodeAdultResident"));
								}
							}
							if(documentRequired.getCode().equals("DOCA/R/INF")){
								if(request.getParameter("issueCountryInfant_0").equals("US") && request.getParameter("nationalityInfant_0").equals("US")){
									passengerToUpdate.setInfantResidentDocumentRequired(false);
									myPassenger.setInfantResidentDocumentRequired(false);
								}else{
									passengerToUpdate.setInfantResidentDocumentRequired(true);
									myPassenger.setInfantResidentDocumentRequired(true);
									infantResidentDocument.setCity(request.getParameter("cityInfantResident"));
									infantResidentDocumentUpdated.setCity(request.getParameter("cityInfantResident"));
									infantResidentDocument.setCountryCode(request.getParameter("countryCodeInfantResident"));
									infantResidentDocumentUpdated.setCountryCode(request.getParameter("countryCodeInfantResident"));
									infantResidentDocument.setStateProvince(request.getParameter("stateProvinceInfantResident"));
									infantResidentDocumentUpdated.setStateProvince(request.getParameter("stateProvinceInfantResident"));
									infantResidentDocument.setStreetAddress(request.getParameter("streetAddressInfantResident"));
									infantResidentDocumentUpdated.setStreetAddress(request.getParameter("streetAddressInfantResident"));
									infantResidentDocument.setZipCode(request.getParameter("zipCodeInfantResident"));
									infantResidentDocumentUpdated.setZipCode(request.getParameter("zipCodeInfantResident"));
								}

							}
							if(documentRequired.getCode().equals("DOCA/D")){
								if(request.getParameter("issueCountry_0").equals("US")
										&& request.getParameter("nationality_0").equals("US")){
									passengerToUpdate.setAdultDestinationDocumentRequired(false);
									myPassenger.setAdultDestinationDocumentRequired(false);
								}else{
									passengerToUpdate.setAdultDestinationDocumentRequired(true);
									myPassenger.setAdultDestinationDocumentRequired(true);
									adultDestinationDocument.setCity(request.getParameter("cityDestination"));
									adultDestinationDocument.setCountryCode(request.getParameter("countryCodeDestination"));
									adultDestinationDocument.setStateProvince(request.getParameter("stateProvinceDestination"));
									adultDestinationDocument.setStreetAddress(request.getParameter("streetAddressDestination"));
									adultDestinationDocument.setZipCode(request.getParameter("zipCodeAdultDestination"));
									adultDestinationDocumentUpdated.setCity(request.getParameter("cityDestination"));
									adultDestinationDocumentUpdated.setCountryCode(request.getParameter("countryCodeDestination"));
									adultDestinationDocumentUpdated.setStateProvince(request.getParameter("stateProvinceDestination"));
									adultDestinationDocumentUpdated.setStreetAddress(request.getParameter("streetAddressDestination"));
									adultDestinationDocumentUpdated.setZipCode(request.getParameter("zipCodeAdultDestination"));
								}

							}
							if(documentRequired.getCode().equals("DOCA/D/INF")){
								if(request.getParameter("issueCountryInfant_0").equals("US")
										&& request.getParameter("nationalityInfant_0").equals("US")){
									passengerToUpdate.setInfantDestinationDocumentRequired(false);
									myPassenger.setInfantDestinationDocumentRequired(false);
								}else{
									passengerToUpdate.setInfantDestinationDocumentRequired(true);
									myPassenger.setInfantDestinationDocumentRequired(true);
									infantDestinationDocument.setCity(request.getParameter("cityInfantDestination"));
									infantDestinationDocument.setCountryCode(request.getParameter("countryCodeInfantDestination"));
									infantDestinationDocument.setStateProvince(request.getParameter("stateProvinceInfantDestination"));
									infantDestinationDocument.setStreetAddress(request.getParameter("streetAddressInfantDestination"));
									infantDestinationDocument.setZipCode(request.getParameter("zipCodeInfantDestination"));
									infantDestinationDocumentUpdated.setCity(request.getParameter("cityInfantDestination"));
									infantDestinationDocumentUpdated.setCountryCode(request.getParameter("countryCodeInfantDestination"));
									infantDestinationDocumentUpdated.setStateProvince(request.getParameter("stateProvinceInfantDestination"));
									infantDestinationDocumentUpdated.setStreetAddress(request.getParameter("streetAddressInfantDestination"));
									infantDestinationDocumentUpdated.setZipCode(request.getParameter("zipCodeInfantDestination"));
								}

							}
							if(documentRequired.getCode().equals("DOCV")){
								passengerToUpdate.setAdultDocVRequired(false);
								passengerToUpdate.setInfantDocVRequired(false);
								myPassenger.setAdultDocVRequired(false);
								myPassenger.setInfantDocVRequired(false);
							}
						}
					}else{
						passengerToUpdate.setAdultDocumentsSRequired(false);
						passengerToUpdate.setInfantDocumentsSRequired(false);
						passengerToUpdate.setAdultDocumentOtherRequired(false);
						passengerToUpdate.setInfantDocumentOtherRequired(false);
						passengerToUpdate.setAdultResidentDocumentRequired(false);
						passengerToUpdate.setInfantResidentDocumentRequired(false);
						passengerToUpdate.setAdultDestinationDocumentRequired(false);
						passengerToUpdate.setInfantDestinationDocumentRequired(false);
						passengerToUpdate.setAdultDocVRequired(false);
						passengerToUpdate.setInfantDocVRequired(false);
						myPassenger.setAdultDocumentsSRequired(false);
						myPassenger.setInfantDocumentsSRequired(false);
						myPassenger.setAdultDocumentOtherRequired(false);
						myPassenger.setInfantDocumentOtherRequired(false);
						myPassenger.setAdultResidentDocumentRequired(false);
						myPassenger.setInfantResidentDocumentRequired(false);
						myPassenger.setAdultDestinationDocumentRequired(false);
						myPassenger.setInfantDestinationDocumentRequired(false);
						myPassenger.setAdultDocVRequired(false);
						myPassenger.setInfantDocVRequired(false);
					}
					if(myPassenger.getDocuments() != null && myPassenger.getDocuments().size() > 0){
						for(int k=0; k<myPassenger.getDocuments().size(); k++){
							Document documentPass = new Document();
							documentPass.setType(myPassenger.getDocuments().get(k).getType());
							documentPass.setInfant(myPassenger.getDocuments().get(k).getInfant());
							//documentRequiredList.add(documentRequired);

							if(documentPass.getType().equals("Passport") || documentPass.getType().equals("NationalID"))
							{	//TODO - cablatura!
								if(documentPass.getInfant() && docsAddedInf == false){
									passengerToUpdate.setInfantDocumentsSRequired(true);
									myPassenger.setInfantDocumentsSRequired(true);
									InfantDocuments infantDocuments = new InfantDocuments();
									com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments infantDocumentsToUpdate = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments();
									String infantDocs = (request.getParameter("numberOfInfantDocuments") == "" || request.getParameter("numberOfInfantDocuments") == null) ? "0" :  request.getParameter("numberOfInfantDocuments");
									int numberOfInfantDocuments = Integer.parseInt(infantDocs);
									for(int t=0; t<numberOfInfantDocuments; t++){
										infantDocuments.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
										infantDocumentsToUpdate.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
										infantDocuments.setExpirationDate(request.getParameter("expirationDateInfant_" + t));
										infantDocumentsToUpdate.setExpirationDate(request.getParameter("expirationDateInfant_" + t));
										infantDocuments.setFirstName(request.getParameter("firstNameInfant_" + t));
										infantDocumentsToUpdate.setFirstName(request.getParameter("firstNameInfant_" + t));
										infantDocuments.setGender(request.getParameter("genderInfant_" + t));
										infantDocumentsToUpdate.setGender(request.getParameter("genderInfant_" + t));
										infantDocuments.setIssueCountry(request.getParameter("issueCountryInfant_" + t));
										infantDocumentsToUpdate.setIssueCountry(request.getParameter("issueCountryInfant_" + t));
										infantDocuments.setLastName(request.getParameter("lastNameInfant_" + t));
										infantDocumentsToUpdate.setLastName(request.getParameter("lastNameInfant_" + t));
										infantDocuments.setNationality(request.getParameter("nationalityInfant_" + t));
										infantDocumentsToUpdate.setNationality(request.getParameter("nationalityInfant_" + t));
										infantDocuments.setNumber(request.getParameter("numberInfant_" + t));
										infantDocumentsToUpdate.setNumber(request.getParameter("numberInfant_" + t));
										infantDocuments.setType(request.getParameter("typeInfant_" + t));
										infantDocumentsToUpdate.setType(request.getParameter("typeInfant_" + t));
										infantDocumentsList.add(infantDocuments);
										infantDocumentsListUpdated.add(infantDocumentsToUpdate);
									}
								}else{
									if(docsAdded == false) {
										passengerToUpdate.setAdultDocumentsSRequired(true);
										myPassenger.setAdultDocumentsSRequired(true);
										AdultDocuments adultDocuments = new AdultDocuments();
										com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments adultDocumentsToUpdate = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments();
										String adultDocs = (request.getParameter("numberOfAdultDocuments") == "" || request.getParameter("numberOfAdultDocuments") == null) ? "0" : request.getParameter("numberOfAdultDocuments");
										int numberOfAdultDocuments = Integer.parseInt(adultDocs);

										for (int p = 0; p < numberOfAdultDocuments; p++) {
											adultDocuments.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
											adultDocumentsToUpdate.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
											adultDocuments.setExpirationDate(request.getParameter("expirationDate_" + p));
											adultDocumentsToUpdate.setExpirationDate(request.getParameter("expirationDate_" + p));
											adultDocuments.setFirstName(request.getParameter("firstName_" + p));
											adultDocumentsToUpdate.setFirstName(request.getParameter("firstName_" + p));
											adultDocuments.setGender(request.getParameter("gender_" + p));
											adultDocumentsToUpdate.setGender(request.getParameter("gender_" + p));
											adultDocuments.setIssueCountry(request.getParameter("issueCountry_" + p));
											adultDocumentsToUpdate.setIssueCountry(request.getParameter("issueCountry_" + p));
											adultDocuments.setLastName(request.getParameter("lastName_" + p));
											adultDocumentsToUpdate.setLastName(request.getParameter("lastName_" + p));
											adultDocuments.setNationality(request.getParameter("nationality_" + p));
											adultDocumentsToUpdate.setNationality(request.getParameter("nationality_" + p));
											adultDocuments.setNumber(request.getParameter("number_" + p));
											adultDocumentsToUpdate.setNumber(request.getParameter("number_" + p));
											adultDocuments.setType(request.getParameter("type_" + p));
											adultDocumentsToUpdate.setType(request.getParameter("type_" + p));

//											evita aggiunta di piu' documenti di tipo uguale: fix errore request BL "Duplicate Types are not allowed"
											if(!adultDocumentsList.contains(adultDocuments)) {
												adultDocumentsList.add(adultDocuments);
											}
											if(!adultDocumentsListUpdated.contains(adultDocumentsToUpdate)) {
												adultDocumentsListUpdated.add(adultDocumentsToUpdate);
											}
										}
									}
								}

							}
							if(documentPass.getType().equals("DOCA-R")){
								if(request.getParameter("issueCountry_0").equals("US") && request.getParameter("nationality_0").equals("US")){
									passengerToUpdate.setAdultResidentDocumentRequired(false);
									myPassenger.setAdultResidentDocumentRequired(false);
								}else{
									passengerToUpdate.setAdultResidentDocumentRequired(true);
									myPassenger.setAdultResidentDocumentRequired(true);
									adultResidentDocument.setCity(request.getParameter("cityResident"));
									adultResidentDocumentUpdated.setCity(request.getParameter("cityResident"));
									adultResidentDocument.setCountryCode(request.getParameter("countryCodeResident"));
									adultResidentDocumentUpdated.setCountryCode(request.getParameter("countryCodeResident"));
									adultResidentDocument.setStateProvince(request.getParameter("stateProvinceResident"));
									adultResidentDocumentUpdated.setStateProvince(request.getParameter("stateProvinceResident"));
									adultResidentDocument.setStreetAddress(request.getParameter("streetAddressResident"));
									adultResidentDocumentUpdated.setStreetAddress(request.getParameter("streetAddressResident"));
									adultResidentDocument.setZipCode(request.getParameter("zipCodeAdultResident"));
									adultResidentDocumentUpdated.setZipCode(request.getParameter("zipCodeAdultResident"));
								}
							}
							if(documentPass.getType().equals("DOCA-D")){
								if(request.getParameter("issueCountry_0").equals("US") && request.getParameter("nationality_0").equals("US")){
									passengerToUpdate.setAdultDestinationDocumentRequired(false);
									myPassenger.setAdultDestinationDocumentRequired(false);
								}else{
									passengerToUpdate.setAdultDestinationDocumentRequired(true);
									myPassenger.setAdultDestinationDocumentRequired(true);
									adultDestinationDocument.setCity(request.getParameter("cityDestination"));
									adultDestinationDocument.setCountryCode(request.getParameter("countryCodeDestination"));
									adultDestinationDocument.setStateProvince(request.getParameter("stateProvinceDestination"));
									adultDestinationDocument.setStreetAddress(request.getParameter("streetAddressDestination"));
									adultDestinationDocument.setZipCode(request.getParameter("zipCodeAdultDestination"));
									adultDestinationDocumentUpdated.setCity(request.getParameter("cityDestination"));
									adultDestinationDocumentUpdated.setCountryCode(request.getParameter("countryCodeDestination"));
									adultDestinationDocumentUpdated.setStateProvince(request.getParameter("stateProvinceDestination"));
									adultDestinationDocumentUpdated.setStreetAddress(request.getParameter("streetAddressDestination"));
									adultDestinationDocumentUpdated.setZipCode(request.getParameter("zipCodeAdultDestination"));
								}

							}
						}
						boolean hasDocsInf = false;
						boolean hasEsta = false;
						boolean hasGreenCard = false;
						boolean hasGreenCardInfant = false;
						for(int k=0; k<myPassenger.getDocuments().size(); k++) {
							Document docsPass = new Document();
							docsPass.setType(myPassenger.getDocuments().get(k).getType());
							docsPass.setInfant(myPassenger.getDocuments().get(k).getInfant());
							if(docsPass.getType().equals("Passport")){
								if(docsPass.getInfant()){
									hasDocsInf = true;
								}
							}
							if(docsPass.getType().equals("GreenCard")){
								if(docsPass.getInfant()){
									hasGreenCardInfant = true;
								}else{
									hasGreenCard = true;
								}
							}
							if(docsPass.getType().equals("Esta")){
								hasEsta = true;
							}
						}
						if(docsEstaAdded == false){
						    if((hasEsta && isDestinationUS)  || (hasGreenCard && isDestinationUS)){ // Esta/visto/greencard adulto
                                String documentType = request.getParameter("documentTypeAdult");
                                if(documentType.equals("esta")){
                                    passengerToUpdate.setAdultDocumentOtherRequired(false);
                                    myPassenger.setAdultDocumentOtherRequired(false);
                                }
                                if(documentType.equals("visto")){
                                    passengerToUpdate.setAdultDocumentOtherRequired(true);
                                    myPassenger.setAdultDocumentOtherRequired(true);
                                    adultDocumentOther.setApplicableCountryCode(request.getParameter("applicableCountryCodeAdult"));
                                    adultDocumentOtherUpdated.setApplicableCountryCode(request.getParameter("applicableCountryCodeAdult"));
                                    adultDocumentOther.setBirthPlace("");
                                    adultDocumentOtherUpdated.setBirthPlace("");
                                    adultDocumentOther.setIssueCity(request.getParameter("issueCityAdult"));
                                    adultDocumentOtherUpdated.setIssueCity(request.getParameter("issueCityAdult"));
                                    adultDocumentOther.setIssueDate(request.getParameter("issueDateAdult"));
                                    adultDocumentOtherUpdated.setIssueDate(request.getParameter("issueDateAdult"));
                                    adultDocumentOther.setNumber(request.getParameter("numberOtherDocumentAdult"));
                                    adultDocumentOtherUpdated.setNumber(request.getParameter("numberOtherDocumentAdult"));
                                }
                                if(documentType.equals("green")){
                                    AdultDocuments adultDocumentsGreenCard = new AdultDocuments();
                                    com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments adultDocumentsToUpdateGreenCard = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments();
                                    String adultDocs = (request.getParameter("numberOfAdultDocuments") == "" || request.getParameter("numberOfAdultDocuments") == null) ? "0" :  request.getParameter("numberOfAdultDocuments");
                                    int numberOfAdultDocuments = Integer.parseInt(adultDocs);

                                    for(int p=0; p<numberOfAdultDocuments; p++){
                                        adultDocumentsGreenCard.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
                                        adultDocumentsToUpdateGreenCard.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
                                        adultDocumentsGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardAdult_" + p));
                                        adultDocumentsToUpdateGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardAdult_" + p));
                                        adultDocumentsGreenCard.setFirstName(request.getParameter("firstName_" + p));
                                        adultDocumentsToUpdateGreenCard.setFirstName(request.getParameter("firstName_" + p));
                                        adultDocumentsGreenCard.setGender(request.getParameter("gender_" + p));
                                        adultDocumentsToUpdateGreenCard.setGender(request.getParameter("gender_" + p));
                                        adultDocumentsGreenCard.setIssueCountry("US");
                                        adultDocumentsToUpdateGreenCard.setIssueCountry("US");
                                        adultDocumentsGreenCard.setLastName(request.getParameter("lastName_" + p));
                                        adultDocumentsToUpdateGreenCard.setLastName(request.getParameter("lastName_" + p));
                                        adultDocumentsGreenCard.setNationality("US");
                                        adultDocumentsToUpdateGreenCard.setNationality("US");
                                        adultDocumentsGreenCard.setNumber(request.getParameter("numberGreenCardAdult_" + p));
                                        adultDocumentsToUpdateGreenCard.setNumber(request.getParameter("numberGreenCardAdult_" + p));
                                        adultDocumentsGreenCard.setType("GREENCARD");
                                        adultDocumentsToUpdateGreenCard.setType("GREENCARD");
                                        adultDocumentsList.add(adultDocumentsGreenCard);
                                        adultDocumentsListUpdated.add(adultDocumentsToUpdateGreenCard);
                                    }
                                }
                            }
                        }
                        if(docsEstaAddedInf == false){
						    if((hasEsta && hasDocsInf && isDestinationUS) || (hasGreenCard && hasGreenCardInfant && isDestinationUS)){ // Esta/visto/greenCard bambino
                                String documentTypeInfant = request.getParameter("documentTypeInfant");
                                if(documentTypeInfant.equals("esta")){
                                    passengerToUpdate.setInfantDocumentOtherRequired(false);
                                    myPassenger.setInfantDocumentOtherRequired(false);
                                }
                                if(documentTypeInfant.equals("visto")){
                                    passengerToUpdate.setInfantDocumentOtherRequired(true);
                                    myPassenger.setInfantDocumentOtherRequired(true);
                                    infantDocumentOther.setApplicableCountryCode(request.getParameter("applicableCountryCodeInfant"));
                                    infantDocumentOtherUpdated.setApplicableCountryCode(request.getParameter("applicableCountryCodeInfant"));
                                    infantDocumentOther.setBirthPlace("");
                                    infantDocumentOtherUpdated.setBirthPlace("");
                                    infantDocumentOther.setIssueCity(request.getParameter("issueCityInfant"));
                                    infantDocumentOtherUpdated.setIssueCity(request.getParameter("issueCityInfant"));
                                    infantDocumentOther.setIssueDate(request.getParameter("issueDateInfant"));
                                    infantDocumentOtherUpdated.setIssueDate(request.getParameter("issueDateInfant"));
                                    infantDocumentOther.setNumber(request.getParameter("numberOtherDocumentInfant"));
                                    infantDocumentOtherUpdated.setNumber(request.getParameter("numberOtherDocumentInfant"));
                                }
                                if(documentTypeInfant.equals("green")){
                                    InfantDocuments infantDocumentsGreenCard = new InfantDocuments();
                                    com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments infantDocumentsToUpdateGreenCard = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments();
                                    String infantDocs = (request.getParameter("numberOfInfantDocuments") == "" || request.getParameter("numberOfInfantDocuments") == null) ? "0" :  request.getParameter("numberOfInfantDocuments");
                                    int numberOfInfantDocuments = Integer.parseInt(infantDocs);
                                    for(int t=0; t<numberOfInfantDocuments; t++){
                                        infantDocumentsGreenCard.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
                                        infantDocumentsToUpdateGreenCard.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
                                        infantDocumentsGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardInfant_" + t));
                                        infantDocumentsToUpdateGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardInfant_" + t));
                                        infantDocumentsGreenCard.setFirstName(request.getParameter("firstNameInfant_" + t));
                                        infantDocumentsToUpdateGreenCard.setFirstName(request.getParameter("firstNameInfant_" + t));
                                        infantDocumentsGreenCard.setGender(request.getParameter("genderInfant_" + t));
                                        infantDocumentsToUpdateGreenCard.setGender(request.getParameter("genderInfant_" + t));
                                        infantDocumentsGreenCard.setIssueCountry("US");
                                        infantDocumentsToUpdateGreenCard.setIssueCountry("US");
                                        infantDocumentsGreenCard.setLastName(request.getParameter("lastNameInfant_" + t));
                                        infantDocumentsToUpdateGreenCard.setLastName(request.getParameter("lastNameInfant_" + t));
                                        infantDocumentsGreenCard.setNationality("US");
                                        infantDocumentsToUpdateGreenCard.setNationality("US");
                                        infantDocumentsGreenCard.setNumber(request.getParameter("numberGreenCardInfant_" + t));
                                        infantDocumentsToUpdateGreenCard.setNumber(request.getParameter("numberGreenCardInfant" + t));
                                        infantDocumentsGreenCard.setType("GREENCARD");
                                        infantDocumentsToUpdateGreenCard.setType("GREENCARD");
                                        infantDocumentsList.add(infantDocumentsGreenCard);
                                        infantDocumentsListUpdated.add(infantDocumentsToUpdateGreenCard);
                                    }
                                }
                            }
                        }
					}


					passengerToUpdate.setDocumentRequired(documentRequiredList);
					passengerToUpdate.setAdultDocumentsS(adultDocumentsList);
					passengerToUpdate.setInfantDocumentsS(infantDocumentsList);
					passengerToUpdate.setAdultDocumentOther(adultDocumentOther);
					passengerToUpdate.setInfantDocumentOther(infantDocumentOther);
					passengerToUpdate.setAdultResidentDocument(adultResidentDocument);
					passengerToUpdate.setInfantResidentDocument(infantResidentDocument);
					passengerToUpdate.setAdultDestinationDocument(adultDestinationDocument);
					passengerToUpdate.setInfantDestinationDocument(infantDestinationDocument);

					myPassenger.setAdultDocumentsS(adultDocumentsListUpdated);
					myPassenger.setInfantDocumentsS(infantDocumentsListUpdated);
					myPassenger.setAdultDocumentOther(adultDocumentOtherUpdated);
					myPassenger.setInfantDocumentOther(infantDocumentOtherUpdated);
					myPassenger.setAdultResidentDocument(adultResidentDocumentUpdated);
					myPassenger.setInfantResidentDocument(infantResidentDocumentUpdated);
					myPassenger.setAdultDestinationDocument(adultDestinationDocumentUpdated);
					myPassenger.setInfantDestinationDocument(infantDestinationDocumentUpdated);

					List<com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.Passenger> updatedPassengerList = new ArrayList<>();
					updatedPassengerList.add(passengerToUpdate);
					serviceRequest.setPassengers(updatedPassengerList);

					response = checkInDelegateRest.updatePassenger(serviceRequest);

					if(response != null && response.get_updatepassengerResp().getPassengers().get(0).getResult().equals("OK")){
						if(response.get_updatepassengerResp().getConversationID() != null){
							ctx.conversationId = response.get_updatepassengerResp().getConversationID();
						}
						List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.DocumentRequired> xList = new ArrayList<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.DocumentRequired>();
						myPassenger.setDocumentRequired(xList);
						for(Pnr pnr : ctx.pnrListData){
							if(pnr.getNumber().equals(ctx.pnr)){
								for(Flight flight : pnr.getFlights()){
									if(departureDate.equals(flight.getDepartureDate())){
										passengersFromSession = flight.getSegments().get(0).getPassengers();
										for(int i=0; i<passengersFromSession.size(); i++){
											if(check.equals(passengersFromSession.get(i).getNome() + "_" + passengersFromSession.get(i).getCognome())){
												flight.getSegments().get(0).getPassengers().set(i, myPassenger);
//												logger.info("[CheckinSession][checkinUpdatePassenger] - Le informazioni del passeggero sono modificate e inserite nel Context.");
												break;
											}
										}
										break;
									}
								}
							}
						}
					}
				}
				else{
					logger.warn("[CheckinSession][checkinUpdatePassenger] - Il passeggero selezionato non è stato trovato.");
				}
			}
			catch(Exception e){
				logger.error("[CheckinSession][checkinUpdatePassenger] - Errore durante l'invocazione del servizio.", e);
			}

			return response;
		}

	public CheckinUpdatePassengerResponse checkinUpdatePassengerUserLogged(HashMap<String,String> map, CheckinSessionContext ctx,SlingHttpServletRequest request) {

		CheckinUpdatePassengerResponse response = null;

		try{
			CheckinUpdatePassengerRequest serviceRequest = new CheckinUpdatePassengerRequest(IDFactory.getTid(), IDFactory.getSid(request));
			com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.Passenger passengerToUpdate = new com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.Passenger();

			List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger> passengersFromSession = null;
			String departureDate = ctx.pnrListData.get(0).getFlights().get(Integer.parseInt(request.getParameter("flightitinerary"))).getDepartureDate();
			String airline = "";
			String origin = "";
			String mflight = "";
			// String passengerID = request.getParameter("passengerID");
			String name = map.get("nome");
			String surname = map.get("cognome");
			String check = name + "_" + surname;
			boolean isDestinationUS = false;

			for(Pnr pnr : ctx.pnrListData){
//					logger.info("[CheckinSession][checkinUpdatePassenger] PNR trovato: " + pnr.getNumber());

				if(pnr.getNumber().equals(ctx.pnr) ||
						((ctx.pnrSelectedListDataRender != null
								&& ctx.pnrSelectedListDataRender.get(0) != null) &&
								(ctx.pnrSelectedListDataRender.get(0).getNumber().equals(pnr.getNumber()))))
				{
					for(Flight flight : pnr.getFlights()){
//							logger.info("[CheckinSession][checkinUpdatePassenger] FLIGHT trovato.");
						if(departureDate.equals(flight.getDepartureDate())){
//								logger.info("[CheckinSession][checkinUpdatePassenger]departureDate: " + departureDate);
							airline = flight.getSegments().get(0).getAirline();
							origin = flight.getSegments().get(0).getOrigin().getCode();
							mflight = flight.getSegments().get(0).getFlight();
							passengersFromSession = flight.getSegments().get(0).getPassengers();
							if(flight.getDestination().getCountryCode().equals("US")){
								isDestinationUS = true;
							}
//								logger.info("[CheckinSession][checkinUpdatePassenger] PASSEGGERO trovato: " + passengersFromSession.size());
							break;
						}
					}
				}
			}

			if(passengersFromSession != null){
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger myPassenger = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger();
				//				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger updatedPassenger = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger();


				for(int i=0; i<passengersFromSession.size(); i++)
				{

					if(check.equals(passengersFromSession.get(i).getNome() + "_" + passengersFromSession.get(i).getCognome())){
//							logger.info("[CheckinSession][checkinUpdatePassenger] PASSEGGERO [passengerID = " +
//									passengersFromSession.get(i).getPassengerID() + "] TROVATO.");
						myPassenger = passengersFromSession.get(i);
						//						updatedPassenger = passengersFromSession.get(i);
						break;
					}
				}

				String []dateSplitted = departureDate.split("T");
				serviceRequest.setDepartureDate(dateSplitted[0]);
				serviceRequest.setAirline(airline);
				serviceRequest.setFlight(mflight);
				serviceRequest.setOrigin(origin);
				serviceRequest.setLanguage(ctx.language);
				serviceRequest.setMarket(ctx.market);
				serviceRequest.setConversationID(ctx.conversationId);

				passengerToUpdate.setPassengerID(myPassenger.getPassengerID() == null ? "" : myPassenger.getPassengerID());
				passengerToUpdate.setCheckInLimited(myPassenger.getCheckInLimited());
				passengerToUpdate.setCheckInComplete(myPassenger.getCheckInComplete());
				passengerToUpdate.setNome(myPassenger.getNome());
				passengerToUpdate.setCognome(myPassenger.getCognome());
				passengerToUpdate.setBookingClass(myPassenger.getBookingClass());
				passengerToUpdate.setPostoAssegnato(myPassenger.getPostoAssegnato());
				passengerToUpdate.setNumeroPostoAssegnato(myPassenger.getNumeroPostoAssegnato());
				passengerToUpdate.setGender(map.get("gender"));

				myPassenger.setGender(map.get("gender"));

				List<FrequentFlyer> ffList = new ArrayList<>();
				List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.FrequentFlyer> ffListUpdated = myPassenger.getFrequentFlyer();
				FrequentFlyer ff;
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.FrequentFlyer ffUpdated;

				passengerToUpdate.setFrequentFlyer(ffList);
				myPassenger.setFrequentFlyer(ffListUpdated);

				if (myPassenger.getAirline() == null)
					passengerToUpdate.setAirline("");
				else
					passengerToUpdate.setAirline(myPassenger.getAirline());

				if (myPassenger.getNationality() == null)
					passengerToUpdate.setNationality("");
				else
					passengerToUpdate.setNationality(myPassenger.getNationality());

				if (myPassenger.getResidence() == null)
					passengerToUpdate.setResidence("");
				else
					passengerToUpdate.setResidence(myPassenger.getResidence());

				if (myPassenger.getAddress() == null)
					passengerToUpdate.setAddress("");
				else
					passengerToUpdate.setAddress(myPassenger.getAddress());

				if (myPassenger.getCity() == null)
					passengerToUpdate.setCity("");
				else
					passengerToUpdate.setCity(myPassenger.getCity());

				if (myPassenger.getState() == null)
					passengerToUpdate.setState("");
				else
					passengerToUpdate.setState(myPassenger.getState());

				if (myPassenger.getZipCode() == null)
					passengerToUpdate.setZipCode("");
				else
					passengerToUpdate.setZipCode(myPassenger.getZipCode());

				if(myPassenger.getCodeFFchanged() != null && myPassenger.getCodeFFchanged().equals(request.getParameter("codeFFchanged"))){
					passengerToUpdate.setCodeFFchanged("");
				}
				else{
					if (map.get("codeFFchanged") != null){
						passengerToUpdate.setCodeFFchanged(map.get("codeFFchanged"));
						myPassenger.setCodeFFchanged(map.get("codeFFchanged"));
					} else {
						passengerToUpdate.setCodeFFchanged("");
						myPassenger.setCodeFFchanged("");

					}

				}

				if(myPassenger.getNumberFFchanged() != null && myPassenger.getCodeFFchanged().equals(request.getParameter("numberFFchanged"))){
					passengerToUpdate.setNumberFFchanged("");
				}
				else {
					if (map.get("numberFFchanged") != null){
						passengerToUpdate.setNumberFFchanged(map.get("numberFFchanged"));
						myPassenger.setNumberFFchanged(map.get("numberFFchanged"));
					} else {
						passengerToUpdate.setNumberFFchanged("");
						myPassenger.setNumberFFchanged("");

					}
				}

				if (myPassenger.getIsChanged() == null)
					passengerToUpdate.setIsChanged(false);
				else
					passengerToUpdate.setIsChanged(myPassenger.getIsChanged());

				if (myPassenger.getTicketStatus() == null)
					passengerToUpdate.setTicketStatus("");
				else
					passengerToUpdate.setTicketStatus(myPassenger.getTicketStatus());

				if (myPassenger.getTicket() == null)
					passengerToUpdate.setTicket("");
				else
					passengerToUpdate.setTicket(myPassenger.getTicket());

				if (myPassenger.getPassengerType() == null)
					passengerToUpdate.setPassengerType("");
				else
					passengerToUpdate.setPassengerType(myPassenger.getPassengerType());
//					evita aggiunta di piu' documenti di tipo uguale: fix errore request BL "Duplicate Types are not allowed"
				List<Document> documents = new ArrayList<>();
				for(com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Document doc : myPassenger.getDocuments()){
					boolean addDoc = true;
					if(documents.size() > 0) {
						for (Document checkDoc : documents) {
							if(checkDoc.getType().equals(doc.getType())){
								addDoc = false;
								break;
							}
						}
					}
					Document myDoc = new Document();
					myDoc.setBirthDate(doc.getBirthDate());
					myDoc.setEmissionCountry(doc.getEmissionCountry());
					myDoc.setExpirationDate(doc.getExpirationDate());
					myDoc.setFirstName(doc.getFirstName());
					myDoc.setInfant(doc.getInfant());
					myDoc.setLastName(doc.getLastName());
					myDoc.setNumber(doc.getNumber());
					myDoc.setSecondName(doc.getSecondName());
					myDoc.setType(doc.getType());
					if (addDoc) {
						documents.add(myDoc);
					}
				}
				passengerToUpdate.setDocuments(documents);

				List<DocumentRequired> documentRequiredList = new ArrayList<>();
				List<AdultDocuments> adultDocumentsList = new ArrayList<>();
				List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments> adultDocumentsListUpdated = new ArrayList<>();
				List<InfantDocuments> infantDocumentsList = new ArrayList<>();
				List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments> infantDocumentsListUpdated = new ArrayList<>();
				AdultDocumentOther adultDocumentOther = new AdultDocumentOther();
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocumentOther adultDocumentOtherUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocumentOther();
				InfantDocumentOther infantDocumentOther = new InfantDocumentOther();
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocumentOther infantDocumentOtherUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocumentOther();
				AdultResidentDocument adultResidentDocument = new AdultResidentDocument();
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultResidentDocument adultResidentDocumentUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultResidentDocument();
				InfantResidentDocument infantResidentDocument = new InfantResidentDocument();
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantResidentDocument infantResidentDocumentUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantResidentDocument();
				AdultDestinationDocument adultDestinationDocument = new AdultDestinationDocument();
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDestinationDocument adultDestinationDocumentUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDestinationDocument();
				InfantDestinationDocument infantDestinationDocument = new InfantDestinationDocument();
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDestinationDocument infantDestinationDocumentUpdated =  new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDestinationDocument();

					passengerToUpdate.setAdultDocumentsSRequired(false);
					passengerToUpdate.setInfantDocumentsSRequired(false);
					passengerToUpdate.setAdultDocumentOtherRequired(false);
					passengerToUpdate.setInfantDocumentOtherRequired(false);
					passengerToUpdate.setAdultResidentDocumentRequired(false);
					passengerToUpdate.setInfantResidentDocumentRequired(false);
					passengerToUpdate.setAdultDestinationDocumentRequired(false);
					passengerToUpdate.setInfantDestinationDocumentRequired(false);
					passengerToUpdate.setAdultDocVRequired(false);
					passengerToUpdate.setInfantDocVRequired(false);

				myPassenger.setAdultDocumentsSRequired(false);
				myPassenger.setInfantDocumentsSRequired(false);
				myPassenger.setAdultDocumentOtherRequired(false);
				myPassenger.setInfantDocumentOtherRequired(false);
				myPassenger.setAdultResidentDocumentRequired(false);
				myPassenger.setInfantResidentDocumentRequired(false);
				myPassenger.setAdultDestinationDocumentRequired(false);
				myPassenger.setInfantDestinationDocumentRequired(false);
				myPassenger.setAdultDocVRequired(false);
				myPassenger.setInfantDocVRequired(false);

				boolean docsAdded = false;
				boolean docsAddedInf = false;
				boolean docsEstaAdded = false;
				boolean docsEstaAddedInf = false;

//				if(myPassenger.getDocumentRequired() != null && myPassenger.getDocumentRequired().size() > 0){
//
//					boolean adInsert = false;
//					boolean estaInfant = false;
//
//					for(int k=0; k<myPassenger.getDocumentRequired().size(); k++) {
//						DocumentRequired docr = new DocumentRequired();
//						docr.setCode(myPassenger.getDocumentRequired().get(k).getCode());
//						if(docr.getCode().equals("DOCS/INF")){
//							estaInfant = true;
//						}
//					}
//
//					for(int k=0; k<myPassenger.getDocumentRequired().size(); k++){
//						DocumentRequired documentRequired = new DocumentRequired();
//						documentRequired.setCode(myPassenger.getDocumentRequired().get(k).getCode());
//						documentRequired.setDetailStatus(myPassenger.getDocumentRequired().get(k).getDetailStatus());
//						documentRequired.setFreeText(myPassenger.getDocumentRequired().get(0).getFreeText());
//						documentRequiredList.add(documentRequired);
//
//						if(documentRequired.getCode().equals("DOCS-P") || documentRequired.getCode().equals("DOCS-C") )
//						{	//TODO - cablatura!
//							if (!adInsert)
//							{
//								passengerToUpdate.setAdultDocumentsSRequired(true);
//								myPassenger.setAdultDocumentsSRequired(true);
//								AdultDocuments adultDocuments = new AdultDocuments();
//								com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments adultDocumentsToUpdate = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments();
//								String adultDocs = (request.getParameter("numberOfAdultDocuments") == "" || request.getParameter("numberOfAdultDocuments") == null) ? "0" : request.getParameter("numberOfAdultDocuments");
//								int numberOfAdultDocuments = Integer.parseInt(adultDocs);
//
//								for (int p = 0; p < numberOfAdultDocuments; p++) {
//									adultDocuments.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
//									adultDocumentsToUpdate.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
//									adultDocuments.setExpirationDate(request.getParameter("expirationDate_" + p));
//									adultDocumentsToUpdate.setExpirationDate(request.getParameter("expirationDate_" + p));
//									adultDocuments.setFirstName(request.getParameter("firstName_" + p));
//									adultDocumentsToUpdate.setFirstName(request.getParameter("firstName_" + p));
//									adultDocuments.setGender(request.getParameter("gender_" + p));
//									adultDocumentsToUpdate.setGender(request.getParameter("gender_" + p));
//									adultDocuments.setIssueCountry(request.getParameter("issueCountry_" + p));
//									adultDocumentsToUpdate.setIssueCountry(request.getParameter("issueCountry_" + p));
//									adultDocuments.setLastName(request.getParameter("lastName_" + p));
//									adultDocumentsToUpdate.setLastName(request.getParameter("lastName_" + p));
//									adultDocuments.setNationality(request.getParameter("nationality_" + p));
//									adultDocumentsToUpdate.setNationality(request.getParameter("nationality_" + p));
//									adultDocuments.setNumber(request.getParameter("number_" + p));
//									adultDocumentsToUpdate.setNumber(request.getParameter("number_" + p));
//									adultDocuments.setType(request.getParameter("type_" + p));
//									adultDocumentsToUpdate.setType(request.getParameter("type_" + p));
//									adultDocumentsList.add(adultDocuments);
//									adultDocumentsListUpdated.add(adultDocumentsToUpdate);
//									adInsert = true;
//									docsAdded = true;
//								}
//							}
//						}
//						if(documentRequired.getCode().equals("DOCS/INF")){ 				//TODO - cablatura!
//							passengerToUpdate.setInfantDocumentsSRequired(true);
//							myPassenger.setInfantDocumentsSRequired(true);
//							InfantDocuments infantDocuments = new InfantDocuments();
//							com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments infantDocumentsToUpdate = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments();
//							String infantDocs = (request.getParameter("numberOfInfantDocuments") == "" || request.getParameter("numberOfInfantDocuments") == null) ? "0" :  request.getParameter("numberOfInfantDocuments");
//							int numberOfInfantDocuments = Integer.parseInt(infantDocs);
//							for(int t=0; t<numberOfInfantDocuments; t++){
//								infantDocuments.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
//								infantDocumentsToUpdate.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
//								infantDocuments.setExpirationDate(request.getParameter("expirationDateInfant_" + t));
//								infantDocumentsToUpdate.setExpirationDate(request.getParameter("expirationDateInfant_" + t));
//								infantDocuments.setFirstName(request.getParameter("firstNameInfant_" + t));
//								infantDocumentsToUpdate.setFirstName(request.getParameter("firstNameInfant_" + t));
//								infantDocuments.setGender(request.getParameter("genderInfant_" + t));
//								infantDocumentsToUpdate.setGender(request.getParameter("genderInfant_" + t));
//								infantDocuments.setIssueCountry(request.getParameter("issueCountryInfant_" + t));
//								infantDocumentsToUpdate.setIssueCountry(request.getParameter("issueCountryInfant_" + t));
//								infantDocuments.setLastName(request.getParameter("lastNameInfant_" + t));
//								infantDocumentsToUpdate.setLastName(request.getParameter("lastNameInfant_" + t));
//								infantDocuments.setNationality(request.getParameter("nationalityInfant_" + t));
//								infantDocumentsToUpdate.setNationality(request.getParameter("nationalityInfant_" + t));
//								infantDocuments.setNumber(request.getParameter("numberInfant_" + t));
//								infantDocumentsToUpdate.setNumber(request.getParameter("numberInfant_" + t));
//								infantDocuments.setType(request.getParameter("typeInfant_" + t));
//								infantDocumentsToUpdate.setType(request.getParameter("typeInfant_" + t));
//								infantDocumentsList.add(infantDocuments);
//								infantDocumentsListUpdated.add(infantDocumentsToUpdate);
//								docsAddedInf = true;
//							}
//						}
//						if(documentRequired.getCode().equals("ESTA")){ 					//TODO - cablatura!
//							String documentType = request.getParameter("documentTypeAdult");
//							if(documentType.equals("esta")){
//								passengerToUpdate.setAdultDocumentOtherRequired(false);
//								myPassenger.setAdultDocumentOtherRequired(false);
//							}
//							if(documentType.equals("visto")){
//								passengerToUpdate.setAdultDocumentOtherRequired(true);
//								myPassenger.setAdultDocumentOtherRequired(true);
//								adultDocumentOther.setApplicableCountryCode(request.getParameter("applicableCountryCodeAdult"));
//								adultDocumentOtherUpdated.setApplicableCountryCode(request.getParameter("applicableCountryCodeAdult"));
//								adultDocumentOther.setBirthPlace("");
//								adultDocumentOtherUpdated.setBirthPlace("");
//								adultDocumentOther.setIssueCity(request.getParameter("issueCityAdult"));
//								adultDocumentOtherUpdated.setIssueCity(request.getParameter("issueCityAdult"));
//								adultDocumentOther.setIssueDate(request.getParameter("issueDateAdult"));
//								adultDocumentOtherUpdated.setIssueDate(request.getParameter("issueDateAdult"));
//								adultDocumentOther.setNumber(request.getParameter("numberOtherDocumentAdult"));
//								adultDocumentOtherUpdated.setNumber(request.getParameter("numberOtherDocumentAdult"));
//							}
//							if(documentType.equals("green")){
//								AdultDocuments adultDocumentsGreenCard = new AdultDocuments();
//								com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments adultDocumentsToUpdateGreenCard = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments();
//								String adultDocs = (request.getParameter("numberOfAdultDocuments") == "" || request.getParameter("numberOfAdultDocuments") == null) ? "0" :  request.getParameter("numberOfAdultDocuments");
//								int numberOfAdultDocuments = Integer.parseInt(adultDocs);
//
//									for(int p=0; p<numberOfAdultDocuments; p++){
//										adultDocumentsGreenCard.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
//										adultDocumentsToUpdateGreenCard.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
//										adultDocumentsGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardAdult_" + p));
//										adultDocumentsToUpdateGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardAdult_" + p));
//										adultDocumentsGreenCard.setFirstName(request.getParameter("firstName_" + p));
//										adultDocumentsToUpdateGreenCard.setFirstName(request.getParameter("firstName_" + p));
//										adultDocumentsGreenCard.setGender(request.getParameter("gender_" + p));
//										adultDocumentsToUpdateGreenCard.setGender(request.getParameter("gender_" + p));
//										adultDocumentsGreenCard.setIssueCountry("US");
//										adultDocumentsToUpdateGreenCard.setIssueCountry("US");
//										adultDocumentsGreenCard.setLastName(request.getParameter("lastName_" + p));
//										adultDocumentsToUpdateGreenCard.setLastName(request.getParameter("lastName_" + p));
//										adultDocumentsGreenCard.setNationality("US");
//										adultDocumentsToUpdateGreenCard.setNationality("US");
//										adultDocumentsGreenCard.setNumber(request.getParameter("numberGreenCardAdult_" + p));
//										adultDocumentsToUpdateGreenCard.setNumber(request.getParameter("numberGreenCardAdult_" + p));
//										adultDocumentsGreenCard.setType("GREENCARD");
//										adultDocumentsToUpdateGreenCard.setType("GREENCARD");
//										adultDocumentsList.add(adultDocumentsGreenCard);
//										adultDocumentsListUpdated.add(adultDocumentsToUpdateGreenCard);
//									}
//								}
//								docsEstaAdded = true;
//							}
//							if(documentRequired.getCode().equals("ESTA") && estaInfant == true){				//TODO - cablatura!
//								String documentTypeInfant = request.getParameter("documentTypeInfant");
//								if(documentTypeInfant.equals("esta")){
//									passengerToUpdate.setInfantDocumentOtherRequired(false);
//									myPassenger.setInfantDocumentOtherRequired(false);
//								}
//								if(documentTypeInfant.equals("visto")){
//									passengerToUpdate.setInfantDocumentOtherRequired(true);
//									myPassenger.setInfantDocumentOtherRequired(true);
//									infantDocumentOther.setApplicableCountryCode(request.getParameter("applicableCountryCodeInfant"));
//									infantDocumentOtherUpdated.setApplicableCountryCode(request.getParameter("applicableCountryCodeInfant"));
//									infantDocumentOther.setBirthPlace("");
//									infantDocumentOtherUpdated.setBirthPlace("");
//									infantDocumentOther.setIssueCity(request.getParameter("issueCityInfant"));
//									infantDocumentOtherUpdated.setIssueCity(request.getParameter("issueCityInfant"));
//									infantDocumentOther.setIssueDate(request.getParameter("issueDateInfant"));
//									infantDocumentOtherUpdated.setIssueDate(request.getParameter("issueDateInfant"));
//									infantDocumentOther.setNumber(request.getParameter("numberOtherDocumentInfant"));
//									infantDocumentOtherUpdated.setNumber(request.getParameter("numberOtherDocumentInfant"));
//								}
//								if(documentTypeInfant.equals("green")){
//									InfantDocuments infantDocumentsGreenCard = new InfantDocuments();
//									com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments infantDocumentsToUpdateGreenCard = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments();
//									String infantDocs = (request.getParameter("numberOfInfantDocuments") == "" || request.getParameter("numberOfInfantDocuments") == null) ? "0" :  request.getParameter("numberOfInfantDocuments");
//									int numberOfInfantDocuments = Integer.parseInt(infantDocs);
//									for(int t=0; t<numberOfInfantDocuments; t++){
//										infantDocumentsGreenCard.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
//										infantDocumentsToUpdateGreenCard.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
//										infantDocumentsGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardInfant_" + t));
//										infantDocumentsToUpdateGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardInfant_" + t));
//										infantDocumentsGreenCard.setFirstName(request.getParameter("firstNameInfant_" + t));
//										infantDocumentsToUpdateGreenCard.setFirstName(request.getParameter("firstNameInfant_" + t));
//										infantDocumentsGreenCard.setGender(request.getParameter("genderInfant_" + t));
//										infantDocumentsToUpdateGreenCard.setGender(request.getParameter("genderInfant_" + t));
//										infantDocumentsGreenCard.setIssueCountry("US");
//										infantDocumentsToUpdateGreenCard.setIssueCountry("US");
//										infantDocumentsGreenCard.setLastName(request.getParameter("lastNameInfant_" + t));
//										infantDocumentsToUpdateGreenCard.setLastName(request.getParameter("lastNameInfant_" + t));
//										infantDocumentsGreenCard.setNationality("US");
//										infantDocumentsToUpdateGreenCard.setNationality("US");
//										infantDocumentsGreenCard.setNumber(request.getParameter("numberGreenCardInfant_" + t));
//										infantDocumentsToUpdateGreenCard.setNumber(request.getParameter("numberGreenCardInfant" + t));
//										infantDocumentsGreenCard.setType("GREENCARD");
//										infantDocumentsToUpdateGreenCard.setType("GREENCARD");
//										infantDocumentsList.add(infantDocumentsGreenCard);
//										infantDocumentsListUpdated.add(infantDocumentsToUpdateGreenCard);
//									}
//								}
//								docsEstaAddedInf = true;
//							}
//
//							if(documentRequired.getCode().equals("DOCA/R")){
//								if(request.getParameter("issueCountry_0").equals("US") && request.getParameter("nationality_0").equals("US")){
//									passengerToUpdate.setAdultResidentDocumentRequired(false);
//									myPassenger.setAdultResidentDocumentRequired(false);
//								}else{
//									passengerToUpdate.setAdultResidentDocumentRequired(true);
//									myPassenger.setAdultResidentDocumentRequired(true);
//									adultResidentDocument.setCity(request.getParameter("cityResident"));
//									adultResidentDocumentUpdated.setCity(request.getParameter("cityResident"));
//									adultResidentDocument.setCountryCode(request.getParameter("countryCodeResident"));
//									adultResidentDocumentUpdated.setCountryCode(request.getParameter("countryCodeResident"));
//									adultResidentDocument.setStateProvince(request.getParameter("stateProvinceResident"));
//									adultResidentDocumentUpdated.setStateProvince(request.getParameter("stateProvinceResident"));
//									adultResidentDocument.setStreetAddress(request.getParameter("streetAddressResident"));
//									adultResidentDocumentUpdated.setStreetAddress(request.getParameter("streetAddressResident"));
//									adultResidentDocument.setZipCode(request.getParameter("zipCodeAdultResident"));
//									adultResidentDocumentUpdated.setZipCode(request.getParameter("zipCodeAdultResident"));
//								}
//							}
//							if(documentRequired.getCode().equals("DOCA/R/INF")){
//								if(request.getParameter("issueCountryInfant_0").equals("US") && request.getParameter("nationalityInfant_0").equals("US")){
//									passengerToUpdate.setInfantResidentDocumentRequired(false);
//									myPassenger.setInfantResidentDocumentRequired(false);
//								}else{
//									passengerToUpdate.setInfantResidentDocumentRequired(true);
//									myPassenger.setInfantResidentDocumentRequired(true);
//									infantResidentDocument.setCity(request.getParameter("cityInfantResident"));
//									infantResidentDocumentUpdated.setCity(request.getParameter("cityInfantResident"));
//									infantResidentDocument.setCountryCode(request.getParameter("countryCodeInfantResident"));
//									infantResidentDocumentUpdated.setCountryCode(request.getParameter("countryCodeInfantResident"));
//									infantResidentDocument.setStateProvince(request.getParameter("stateProvinceInfantResident"));
//									infantResidentDocumentUpdated.setStateProvince(request.getParameter("stateProvinceInfantResident"));
//									infantResidentDocument.setStreetAddress(request.getParameter("streetAddressInfantResident"));
//									infantResidentDocumentUpdated.setStreetAddress(request.getParameter("streetAddressInfantResident"));
//									infantResidentDocument.setZipCode(request.getParameter("zipCodeInfantResident"));
//									infantResidentDocumentUpdated.setZipCode(request.getParameter("zipCodeInfantResident"));
//								}
//
//							}
//							if(documentRequired.getCode().equals("DOCA/D")){
//								if(request.getParameter("issueCountry_0").equals("US")
//										&& request.getParameter("nationality_0").equals("US")){
//									passengerToUpdate.setAdultDestinationDocumentRequired(false);
//									myPassenger.setAdultDestinationDocumentRequired(false);
//								}else{
//									passengerToUpdate.setAdultDestinationDocumentRequired(true);
//									myPassenger.setAdultDestinationDocumentRequired(true);
//									adultDestinationDocument.setCity(request.getParameter("cityDestination"));
//									adultDestinationDocument.setCountryCode(request.getParameter("countryCodeDestination"));
//									adultDestinationDocument.setStateProvince(request.getParameter("stateProvinceDestination"));
//									adultDestinationDocument.setStreetAddress(request.getParameter("streetAddressDestination"));
//									adultDestinationDocument.setZipCode(request.getParameter("zipCodeAdultDestination"));
//									adultDestinationDocumentUpdated.setCity(request.getParameter("cityDestination"));
//									adultDestinationDocumentUpdated.setCountryCode(request.getParameter("countryCodeDestination"));
//									adultDestinationDocumentUpdated.setStateProvince(request.getParameter("stateProvinceDestination"));
//									adultDestinationDocumentUpdated.setStreetAddress(request.getParameter("streetAddressDestination"));
//									adultDestinationDocumentUpdated.setZipCode(request.getParameter("zipCodeAdultDestination"));
//								}
//
//							}
//							if(documentRequired.getCode().equals("DOCA/D/INF")){
//								if(request.getParameter("issueCountryInfant_0").equals("US")
//										&& request.getParameter("nationalityInfant_0").equals("US")){
//									passengerToUpdate.setInfantDestinationDocumentRequired(false);
//									myPassenger.setInfantDestinationDocumentRequired(false);
//								}else{
//									passengerToUpdate.setInfantDestinationDocumentRequired(true);
//									myPassenger.setInfantDestinationDocumentRequired(true);
//									infantDestinationDocument.setCity(request.getParameter("cityInfantDestination"));
//									infantDestinationDocument.setCountryCode(request.getParameter("countryCodeInfantDestination"));
//									infantDestinationDocument.setStateProvince(request.getParameter("stateProvinceInfantDestination"));
//									infantDestinationDocument.setStreetAddress(request.getParameter("streetAddressInfantDestination"));
//									infantDestinationDocument.setZipCode(request.getParameter("zipCodeInfantDestination"));
//									infantDestinationDocumentUpdated.setCity(request.getParameter("cityInfantDestination"));
//									infantDestinationDocumentUpdated.setCountryCode(request.getParameter("countryCodeInfantDestination"));
//									infantDestinationDocumentUpdated.setStateProvince(request.getParameter("stateProvinceInfantDestination"));
//									infantDestinationDocumentUpdated.setStreetAddress(request.getParameter("streetAddressInfantDestination"));
//									infantDestinationDocumentUpdated.setZipCode(request.getParameter("zipCodeInfantDestination"));
//								}
//
//						}
//						if(documentRequired.getCode().equals("DOCV")){
//							passengerToUpdate.setAdultDocVRequired(false);
//							passengerToUpdate.setInfantDocVRequired(false);
//							myPassenger.setAdultDocVRequired(false);
//							myPassenger.setInfantDocVRequired(false);
//						}
//					}
//				}else{
					passengerToUpdate.setAdultDocumentsSRequired(false);
					passengerToUpdate.setInfantDocumentsSRequired(false);
					passengerToUpdate.setAdultDocumentOtherRequired(false);
					passengerToUpdate.setInfantDocumentOtherRequired(false);
					passengerToUpdate.setAdultResidentDocumentRequired(false);
					passengerToUpdate.setInfantResidentDocumentRequired(false);
					passengerToUpdate.setAdultDestinationDocumentRequired(false);
					passengerToUpdate.setInfantDestinationDocumentRequired(false);
					passengerToUpdate.setAdultDocVRequired(false);
					passengerToUpdate.setInfantDocVRequired(false);
					myPassenger.setAdultDocumentsSRequired(false);
					myPassenger.setInfantDocumentsSRequired(false);
					myPassenger.setAdultDocumentOtherRequired(false);
					myPassenger.setInfantDocumentOtherRequired(false);
					myPassenger.setAdultResidentDocumentRequired(false);
					myPassenger.setInfantResidentDocumentRequired(false);
					myPassenger.setAdultDestinationDocumentRequired(false);
					myPassenger.setInfantDestinationDocumentRequired(false);
					myPassenger.setAdultDocVRequired(false);
					myPassenger.setInfantDocVRequired(false);
//				}
//				if(myPassenger.getDocuments() != null && myPassenger.getDocuments().size() > 0){
//					for(int k=0; k<myPassenger.getDocuments().size(); k++){
//						Document documentPass = new Document();
//						documentPass.setType(myPassenger.getDocuments().get(k).getType());
//						documentPass.setInfant(myPassenger.getDocuments().get(k).getInfant());
//						//documentRequiredList.add(documentRequired);
//
//							if(documentPass.getType().equals("Passport") || documentPass.getType().equals("NationalID"))
//							{	//TODO - cablatura!
//								if(documentPass.getInfant() && docsAddedInf == false){
//									passengerToUpdate.setInfantDocumentsSRequired(true);
//									myPassenger.setInfantDocumentsSRequired(true);
//									InfantDocuments infantDocuments = new InfantDocuments();
//									com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments infantDocumentsToUpdate = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments();
//									String infantDocs = (request.getParameter("numberOfInfantDocuments") == "" || request.getParameter("numberOfInfantDocuments") == null) ? "0" :  request.getParameter("numberOfInfantDocuments");
//									int numberOfInfantDocuments = Integer.parseInt(infantDocs);
//									for(int t=0; t<numberOfInfantDocuments; t++){
//										infantDocuments.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
//										infantDocumentsToUpdate.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
//										infantDocuments.setExpirationDate(request.getParameter("expirationDateInfant_" + t));
//										infantDocumentsToUpdate.setExpirationDate(request.getParameter("expirationDateInfant_" + t));
//										infantDocuments.setFirstName(request.getParameter("firstNameInfant_" + t));
//										infantDocumentsToUpdate.setFirstName(request.getParameter("firstNameInfant_" + t));
//										infantDocuments.setGender(request.getParameter("genderInfant_" + t));
//										infantDocumentsToUpdate.setGender(request.getParameter("genderInfant_" + t));
//										infantDocuments.setIssueCountry(request.getParameter("issueCountryInfant_" + t));
//										infantDocumentsToUpdate.setIssueCountry(request.getParameter("issueCountryInfant_" + t));
//										infantDocuments.setLastName(request.getParameter("lastNameInfant_" + t));
//										infantDocumentsToUpdate.setLastName(request.getParameter("lastNameInfant_" + t));
//										infantDocuments.setNationality(request.getParameter("nationalityInfant_" + t));
//										infantDocumentsToUpdate.setNationality(request.getParameter("nationalityInfant_" + t));
//										infantDocuments.setNumber(request.getParameter("numberInfant_" + t));
//										infantDocumentsToUpdate.setNumber(request.getParameter("numberInfant_" + t));
//										infantDocuments.setType(request.getParameter("typeInfant_" + t));
//										infantDocumentsToUpdate.setType(request.getParameter("typeInfant_" + t));
//										infantDocumentsList.add(infantDocuments);
//										infantDocumentsListUpdated.add(infantDocumentsToUpdate);
//									}
//								}else{
//									if(docsAdded == false) {
//										passengerToUpdate.setAdultDocumentsSRequired(true);
//										myPassenger.setAdultDocumentsSRequired(true);
//										AdultDocuments adultDocuments = new AdultDocuments();
//										com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments adultDocumentsToUpdate = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments();
//										String adultDocs = (request.getParameter("numberOfAdultDocuments") == "" || request.getParameter("numberOfAdultDocuments") == null) ? "0" : request.getParameter("numberOfAdultDocuments");
//										int numberOfAdultDocuments = Integer.parseInt(adultDocs);
//
//									for (int p = 0; p < numberOfAdultDocuments; p++) {
//										adultDocuments.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
//										adultDocumentsToUpdate.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
//										adultDocuments.setExpirationDate(request.getParameter("expirationDate_" + p));
//										adultDocumentsToUpdate.setExpirationDate(request.getParameter("expirationDate_" + p));
//										adultDocuments.setFirstName(request.getParameter("firstName_" + p));
//										adultDocumentsToUpdate.setFirstName(request.getParameter("firstName_" + p));
//										adultDocuments.setGender(request.getParameter("gender_" + p));
//										adultDocumentsToUpdate.setGender(request.getParameter("gender_" + p));
//										adultDocuments.setIssueCountry(request.getParameter("issueCountry_" + p));
//										adultDocumentsToUpdate.setIssueCountry(request.getParameter("issueCountry_" + p));
//										adultDocuments.setLastName(request.getParameter("lastName_" + p));
//										adultDocumentsToUpdate.setLastName(request.getParameter("lastName_" + p));
//										adultDocuments.setNationality(request.getParameter("nationality_" + p));
//										adultDocumentsToUpdate.setNationality(request.getParameter("nationality_" + p));
//										adultDocuments.setNumber(request.getParameter("number_" + p));
//										adultDocumentsToUpdate.setNumber(request.getParameter("number_" + p));
//										adultDocuments.setType(request.getParameter("type_" + p));
//										adultDocumentsToUpdate.setType(request.getParameter("type_" + p));
//
////											evita aggiunta di piu' documenti di tipo uguale: fix errore request BL "Duplicate Types are not allowed"
//											if(!adultDocumentsList.contains(adultDocuments)) {
//												adultDocumentsList.add(adultDocuments);
//											}
//											if(!adultDocumentsListUpdated.contains(adultDocumentsToUpdate)) {
//												adultDocumentsListUpdated.add(adultDocumentsToUpdate);
//											}
//										}
//									}
//								}
//
//							}
//							if(documentPass.getType().equals("DOCA-R")){
//								if(request.getParameter("issueCountry_0").equals("US") && request.getParameter("nationality_0").equals("US")){
//									passengerToUpdate.setAdultResidentDocumentRequired(false);
//									myPassenger.setAdultResidentDocumentRequired(false);
//								}else{
//									passengerToUpdate.setAdultResidentDocumentRequired(true);
//									myPassenger.setAdultResidentDocumentRequired(true);
//									adultResidentDocument.setCity(request.getParameter("cityResident"));
//									adultResidentDocumentUpdated.setCity(request.getParameter("cityResident"));
//									adultResidentDocument.setCountryCode(request.getParameter("countryCodeResident"));
//									adultResidentDocumentUpdated.setCountryCode(request.getParameter("countryCodeResident"));
//									adultResidentDocument.setStateProvince(request.getParameter("stateProvinceResident"));
//									adultResidentDocumentUpdated.setStateProvince(request.getParameter("stateProvinceResident"));
//									adultResidentDocument.setStreetAddress(request.getParameter("streetAddressResident"));
//									adultResidentDocumentUpdated.setStreetAddress(request.getParameter("streetAddressResident"));
//									adultResidentDocument.setZipCode(request.getParameter("zipCodeAdultResident"));
//									adultResidentDocumentUpdated.setZipCode(request.getParameter("zipCodeAdultResident"));
//								}
//							}
//							if(documentPass.getType().equals("DOCA-D")){
//								if(request.getParameter("issueCountry_0").equals("US") && request.getParameter("nationality_0").equals("US")){
//									passengerToUpdate.setAdultDestinationDocumentRequired(false);
//									myPassenger.setAdultDestinationDocumentRequired(false);
//								}else{
//									passengerToUpdate.setAdultDestinationDocumentRequired(true);
//									myPassenger.setAdultDestinationDocumentRequired(true);
//									adultDestinationDocument.setCity(request.getParameter("cityDestination"));
//									adultDestinationDocument.setCountryCode(request.getParameter("countryCodeDestination"));
//									adultDestinationDocument.setStateProvince(request.getParameter("stateProvinceDestination"));
//									adultDestinationDocument.setStreetAddress(request.getParameter("streetAddressDestination"));
//									adultDestinationDocument.setZipCode(request.getParameter("zipCodeAdultDestination"));
//									adultDestinationDocumentUpdated.setCity(request.getParameter("cityDestination"));
//									adultDestinationDocumentUpdated.setCountryCode(request.getParameter("countryCodeDestination"));
//									adultDestinationDocumentUpdated.setStateProvince(request.getParameter("stateProvinceDestination"));
//									adultDestinationDocumentUpdated.setStreetAddress(request.getParameter("streetAddressDestination"));
//									adultDestinationDocumentUpdated.setZipCode(request.getParameter("zipCodeAdultDestination"));
//								}
//
//						}
//					}
//					boolean hasDocsInf = false;
//					boolean hasEsta = false;
//					boolean hasGreenCard = false;
//					boolean hasGreenCardInfant = false;
//					for(int k=0; k<myPassenger.getDocuments().size(); k++) {
//						Document docsPass = new Document();
//						docsPass.setType(myPassenger.getDocuments().get(k).getType());
//						docsPass.setInfant(myPassenger.getDocuments().get(k).getInfant());
//						if(docsPass.getType().equals("Passport")){
//							if(docsPass.getInfant()){
//								hasDocsInf = true;
//							}
//						}
//						if(docsPass.getType().equals("GreenCard")){
//							if(docsPass.getInfant()){
//								hasGreenCardInfant = true;
//							}else{
//								hasGreenCard = true;
//							}
//						}
//						if(docsPass.getType().equals("Esta")){
//							hasEsta = true;
//						}
//					}
//					if(docsEstaAdded == false){
//						if((hasEsta && isDestinationUS)  || (hasGreenCard && isDestinationUS)){ // Esta/visto/greencard adulto
//							String documentType = request.getParameter("documentTypeAdult");
//							if(documentType.equals("esta")){
//								passengerToUpdate.setAdultDocumentOtherRequired(false);
//								myPassenger.setAdultDocumentOtherRequired(false);
//							}
//							if(documentType.equals("visto")){
//								passengerToUpdate.setAdultDocumentOtherRequired(true);
//								myPassenger.setAdultDocumentOtherRequired(true);
//								adultDocumentOther.setApplicableCountryCode(request.getParameter("applicableCountryCodeAdult"));
//								adultDocumentOtherUpdated.setApplicableCountryCode(request.getParameter("applicableCountryCodeAdult"));
//								adultDocumentOther.setBirthPlace("");
//								adultDocumentOtherUpdated.setBirthPlace("");
//								adultDocumentOther.setIssueCity(request.getParameter("issueCityAdult"));
//								adultDocumentOtherUpdated.setIssueCity(request.getParameter("issueCityAdult"));
//								adultDocumentOther.setIssueDate(request.getParameter("issueDateAdult"));
//								adultDocumentOtherUpdated.setIssueDate(request.getParameter("issueDateAdult"));
//								adultDocumentOther.setNumber(request.getParameter("numberOtherDocumentAdult"));
//								adultDocumentOtherUpdated.setNumber(request.getParameter("numberOtherDocumentAdult"));
//							}
//							if(documentType.equals("green")){
//								AdultDocuments adultDocumentsGreenCard = new AdultDocuments();
//								com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments adultDocumentsToUpdateGreenCard = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments();
//								String adultDocs = (request.getParameter("numberOfAdultDocuments") == "" || request.getParameter("numberOfAdultDocuments") == null) ? "0" :  request.getParameter("numberOfAdultDocuments");
//								int numberOfAdultDocuments = Integer.parseInt(adultDocs);
//
//                                    for(int p=0; p<numberOfAdultDocuments; p++){
//                                        adultDocumentsGreenCard.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
//                                        adultDocumentsToUpdateGreenCard.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
//                                        adultDocumentsGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardAdult_" + p));
//                                        adultDocumentsToUpdateGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardAdult_" + p));
//                                        adultDocumentsGreenCard.setFirstName(request.getParameter("firstName_" + p));
//                                        adultDocumentsToUpdateGreenCard.setFirstName(request.getParameter("firstName_" + p));
//                                        adultDocumentsGreenCard.setGender(request.getParameter("gender_" + p));
//                                        adultDocumentsToUpdateGreenCard.setGender(request.getParameter("gender_" + p));
//                                        adultDocumentsGreenCard.setIssueCountry("US");
//                                        adultDocumentsToUpdateGreenCard.setIssueCountry("US");
//                                        adultDocumentsGreenCard.setLastName(request.getParameter("lastName_" + p));
//                                        adultDocumentsToUpdateGreenCard.setLastName(request.getParameter("lastName_" + p));
//                                        adultDocumentsGreenCard.setNationality("US");
//                                        adultDocumentsToUpdateGreenCard.setNationality("US");
//                                        adultDocumentsGreenCard.setNumber(request.getParameter("numberGreenCardAdult_" + p));
//                                        adultDocumentsToUpdateGreenCard.setNumber(request.getParameter("numberGreenCardAdult_" + p));
//                                        adultDocumentsGreenCard.setType("GREENCARD");
//                                        adultDocumentsToUpdateGreenCard.setType("GREENCARD");
//                                        adultDocumentsList.add(adultDocumentsGreenCard);
//                                        adultDocumentsListUpdated.add(adultDocumentsToUpdateGreenCard);
//                                    }
//                                }
//                            }
//                        }
//                        if(docsEstaAddedInf == false){
//						    if((hasEsta && hasDocsInf && isDestinationUS) || (hasGreenCard && hasGreenCardInfant && isDestinationUS)){ // Esta/visto/greenCard bambino
//                                String documentTypeInfant = request.getParameter("documentTypeInfant");
//                                if(documentTypeInfant.equals("esta")){
//                                    passengerToUpdate.setInfantDocumentOtherRequired(false);
//                                    myPassenger.setInfantDocumentOtherRequired(false);
//                                }
//                                if(documentTypeInfant.equals("visto")){
//                                    passengerToUpdate.setInfantDocumentOtherRequired(true);
//                                    myPassenger.setInfantDocumentOtherRequired(true);
//                                    infantDocumentOther.setApplicableCountryCode(request.getParameter("applicableCountryCodeInfant"));
//                                    infantDocumentOtherUpdated.setApplicableCountryCode(request.getParameter("applicableCountryCodeInfant"));
//                                    infantDocumentOther.setBirthPlace("");
//                                    infantDocumentOtherUpdated.setBirthPlace("");
//                                    infantDocumentOther.setIssueCity(request.getParameter("issueCityInfant"));
//                                    infantDocumentOtherUpdated.setIssueCity(request.getParameter("issueCityInfant"));
//                                    infantDocumentOther.setIssueDate(request.getParameter("issueDateInfant"));
//                                    infantDocumentOtherUpdated.setIssueDate(request.getParameter("issueDateInfant"));
//                                    infantDocumentOther.setNumber(request.getParameter("numberOtherDocumentInfant"));
//                                    infantDocumentOtherUpdated.setNumber(request.getParameter("numberOtherDocumentInfant"));
//                                }
//                                if(documentTypeInfant.equals("green")){
//                                    InfantDocuments infantDocumentsGreenCard = new InfantDocuments();
//                                    com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments infantDocumentsToUpdateGreenCard = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments();
//                                    String infantDocs = (request.getParameter("numberOfInfantDocuments") == "" || request.getParameter("numberOfInfantDocuments") == null) ? "0" :  request.getParameter("numberOfInfantDocuments");
//                                    int numberOfInfantDocuments = Integer.parseInt(infantDocs);
//                                    for(int t=0; t<numberOfInfantDocuments; t++){
//                                        infantDocumentsGreenCard.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
//                                        infantDocumentsToUpdateGreenCard.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
//                                        infantDocumentsGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardInfant_" + t));
//                                        infantDocumentsToUpdateGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardInfant_" + t));
//                                        infantDocumentsGreenCard.setFirstName(request.getParameter("firstNameInfant_" + t));
//                                        infantDocumentsToUpdateGreenCard.setFirstName(request.getParameter("firstNameInfant_" + t));
//                                        infantDocumentsGreenCard.setGender(request.getParameter("genderInfant_" + t));
//                                        infantDocumentsToUpdateGreenCard.setGender(request.getParameter("genderInfant_" + t));
//                                        infantDocumentsGreenCard.setIssueCountry("US");
//                                        infantDocumentsToUpdateGreenCard.setIssueCountry("US");
//                                        infantDocumentsGreenCard.setLastName(request.getParameter("lastNameInfant_" + t));
//                                        infantDocumentsToUpdateGreenCard.setLastName(request.getParameter("lastNameInfant_" + t));
//                                        infantDocumentsGreenCard.setNationality("US");
//                                        infantDocumentsToUpdateGreenCard.setNationality("US");
//                                        infantDocumentsGreenCard.setNumber(request.getParameter("numberGreenCardInfant_" + t));
//                                        infantDocumentsToUpdateGreenCard.setNumber(request.getParameter("numberGreenCardInfant" + t));
//                                        infantDocumentsGreenCard.setType("GREENCARD");
//                                        infantDocumentsToUpdateGreenCard.setType("GREENCARD");
//                                        infantDocumentsList.add(infantDocumentsGreenCard);
//                                        infantDocumentsListUpdated.add(infantDocumentsToUpdateGreenCard);
//                                    }
//                                }
//                            }
//                        }
//					}


				passengerToUpdate.setDocumentRequired(documentRequiredList);
				passengerToUpdate.setAdultDocumentsS(adultDocumentsList);
				passengerToUpdate.setInfantDocumentsS(infantDocumentsList);
				passengerToUpdate.setAdultDocumentOther(adultDocumentOther);
				passengerToUpdate.setInfantDocumentOther(infantDocumentOther);
				passengerToUpdate.setAdultResidentDocument(adultResidentDocument);
				passengerToUpdate.setInfantResidentDocument(infantResidentDocument);
				passengerToUpdate.setAdultDestinationDocument(adultDestinationDocument);
				passengerToUpdate.setInfantDestinationDocument(infantDestinationDocument);

				myPassenger.setAdultDocumentsS(adultDocumentsListUpdated);
				myPassenger.setInfantDocumentsS(infantDocumentsListUpdated);
				myPassenger.setAdultDocumentOther(adultDocumentOtherUpdated);
				myPassenger.setInfantDocumentOther(infantDocumentOtherUpdated);
				myPassenger.setAdultResidentDocument(adultResidentDocumentUpdated);
				myPassenger.setInfantResidentDocument(infantResidentDocumentUpdated);
				myPassenger.setAdultDestinationDocument(adultDestinationDocumentUpdated);
				myPassenger.setInfantDestinationDocument(infantDestinationDocumentUpdated);

				List<com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.Passenger> updatedPassengerList = new ArrayList<>();
				updatedPassengerList.add(passengerToUpdate);
				serviceRequest.setPassengers(updatedPassengerList);

				response = checkInDelegateRest.updatePassenger(serviceRequest);

				if(response != null && response.get_updatepassengerResp().getPassengers().get(0).getResult().equals("OK")){
					if(response.get_updatepassengerResp().getConversationID() != null){
						ctx.conversationId = response.get_updatepassengerResp().getConversationID();
					}
					List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.DocumentRequired> xList = new ArrayList<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.DocumentRequired>();
					myPassenger.setDocumentRequired(xList);
					for(Pnr pnr : ctx.pnrListData){
						if(pnr.getNumber().equals(ctx.pnr)){
							for(Flight flight : pnr.getFlights()){
								if(departureDate.equals(flight.getDepartureDate())){
									passengersFromSession = flight.getSegments().get(0).getPassengers();
									for(int i=0; i<passengersFromSession.size(); i++){
										if(check.equals(passengersFromSession.get(i).getNome() + "_" + passengersFromSession.get(i).getCognome())){
											flight.getSegments().get(0).getPassengers().set(i, myPassenger);
//												logger.info("[CheckinSession][checkinUpdatePassenger] - Le informazioni del passeggero sono modificate e inserite nel Context.");
											break;
										}
									}
									break;
								}
							}
						}
					}
				}
			}
			else{
				logger.warn("[CheckinSession][checkinUpdatePassenger] - Il passeggero selezionato non è stato trovato.");
			}
		}
		catch(Exception e){
			logger.error("[CheckinSession][checkinUpdatePassenger] - Errore durante l'invocazione del servizio.", e);
		}

		return response;
	}

	public CheckinUpdatePassengerResponse checkinUpdatePassengerExtra(SlingHttpServletRequest request, CheckinSessionContext ctx) {

		CheckinUpdatePassengerResponse response = null;
//		logger.info("[CheckinSession][checkinUpdatePassengerExtra] Entro nel metodo.");

		try{
			CheckinUpdatePassengerRequest serviceRequest = new CheckinUpdatePassengerRequest(IDFactory.getTid(), IDFactory.getSid(request));
			com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.Passenger passengerToUpdate = new com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.Passenger();

			String departureDate = request.getParameter("departureDate");
			String ticketNumber =  request.getParameter("ticketNumber");
//			logger.info("[CheckinSession][checkinUpdatePassengerExtra][departureDate=" + departureDate + "]");
			String airline = "";
			String origin = "";
			String mflight = "";
			String name = request.getParameter("name");
			String surname = request.getParameter("surname");
			String check = name + "_" + surname;
			boolean isDestinationUS = false;

			com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger myPassenger = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger();

			boolean found = false;

			// Ciclo sui passeggeri extra e prendo quello selezionato
			for (PnrRender pnrSelectedListDataRender : ctx.pnrSelectedListDataRender) {
				for (FlightsRender flight : pnrSelectedListDataRender.getFlightsRender())
				{

					if(departureDate.equals(flight.getOriginalDepartureDate())) {

						for (SegmentRender segment : flight.getSegments()) {
							for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra pExtra : segment.getPassengersExtra()) {
								if (pExtra.getPassenger().getTicket().equals(ticketNumber) && !found)
								{
									myPassenger = pExtra.getPassenger();
									airline = pExtra.getAirline();
									origin = pExtra.getOriginCode();
									mflight = pExtra.getFlightNumber();
									found = true;
								}
							}
						}
						if(flight.getDestination().getCountryCode().equals("US")){
							isDestinationUS = true;
						}
					}
				}
			}

			if (myPassenger != null)
			{
				String []dateSplitted = departureDate.split("T");
				serviceRequest.setDepartureDate(dateSplitted[0]);
				serviceRequest.setAirline(airline);
				serviceRequest.setFlight(mflight);
				serviceRequest.setOrigin(origin);
				serviceRequest.setLanguage(ctx.language);
				serviceRequest.setMarket(ctx.market);
				serviceRequest.setConversationID(ctx.conversationId);

				passengerToUpdate.setPassengerID(myPassenger.getPassengerID() == null ? "" : myPassenger.getPassengerID());
				passengerToUpdate.setCheckInLimited(myPassenger.getCheckInLimited());
				passengerToUpdate.setCheckInComplete(myPassenger.getCheckInComplete());
				passengerToUpdate.setNome(myPassenger.getNome());
				passengerToUpdate.setCognome(myPassenger.getCognome());
				passengerToUpdate.setBookingClass(myPassenger.getBookingClass());
				passengerToUpdate.setPostoAssegnato(myPassenger.getPostoAssegnato());
				passengerToUpdate.setNumeroPostoAssegnato(myPassenger.getNumeroPostoAssegnato());
				passengerToUpdate.setGender(request.getParameter("gender"));

				myPassenger.setGender(request.getParameter("gender"));

				List<FrequentFlyer> ffList = new ArrayList<>();
				List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.FrequentFlyer> ffListUpdated = myPassenger.getFrequentFlyer();
				FrequentFlyer ff;
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.FrequentFlyer ffUpdated;

				if(myPassenger.getFrequentFlyer() != null && myPassenger.getFrequentFlyer().size() > 0){
					for(int i=0; i<myPassenger.getFrequentFlyer().size(); i++){
						ff = new FrequentFlyer();
						ff.setCode(myPassenger.getFrequentFlyer().get(i).getCode());
						ff.setDescription(myPassenger.getFrequentFlyer().get(i).getDescription());
						ff.setNumber(myPassenger.getFrequentFlyer().get(i).getNumber());
						ff.setLenght(myPassenger.getFrequentFlyer().get(i).getLenght());
						ff.setPreFillChar(myPassenger.getFrequentFlyer().get(i).getPreFillChar());
						ff.setRegularExpressionValidation(myPassenger.getFrequentFlyer().get(i).getRegularExpressionValidation());
						ffList.add(ff);
					}
				}

				if((request.getParameter("frequentFlyer") != null) &&
						!(request.getParameter("frequentFlyer").equals(""))){
					ff = new FrequentFlyer();
					ffUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.FrequentFlyer();
					ff.setCode(request.getParameter("frequentFlyer"));
					ff.setDescription(request.getParameter("frequentFlyerDescription"));
					ff.setNumber(request.getParameter("frequentFlyerNumber"));
					ffUpdated.setCode(request.getParameter("frequentFlyer"));
					ffUpdated.setDescription(request.getParameter("frequentFlyerDescription"));
					ffUpdated.setNumber(request.getParameter("frequentFlyerNumber"));
					ffList.add(ff);
					ffListUpdated.add(ffUpdated);
				}

				passengerToUpdate.setFrequentFlyer(ffList);
				myPassenger.setFrequentFlyer(ffListUpdated);

				if (myPassenger.getAirline() == null)
					passengerToUpdate.setAirline("");
				else
					passengerToUpdate.setAirline(myPassenger.getAirline());

				if (myPassenger.getNationality() == null)
					passengerToUpdate.setNationality("");
				else
					passengerToUpdate.setNationality(myPassenger.getNationality());

				if (myPassenger.getResidence() == null)
					passengerToUpdate.setResidence("");
				else
					passengerToUpdate.setResidence(myPassenger.getResidence());

				if (myPassenger.getAddress() == null)
					passengerToUpdate.setAddress("");
				else
					passengerToUpdate.setAddress(myPassenger.getAddress());

				if (myPassenger.getCity() == null)
					passengerToUpdate.setCity("");
				else
					passengerToUpdate.setCity(myPassenger.getCity());

				if (myPassenger.getState() == null)
					passengerToUpdate.setState("");
				else
					passengerToUpdate.setState(myPassenger.getState());

				if (myPassenger.getZipCode() == null)
					passengerToUpdate.setZipCode("");
				else
					passengerToUpdate.setZipCode(myPassenger.getZipCode());

				if(myPassenger.getCodeFFchanged() != null && myPassenger.getCodeFFchanged().equals(request.getParameter("codeFFchanged"))){
					passengerToUpdate.setCodeFFchanged("");
				}
				else{
					passengerToUpdate.setCodeFFchanged(request.getParameter("codeFFchanged"));
					myPassenger.setCodeFFchanged(request.getParameter("codeFFchanged"));
				}

				if(myPassenger.getNumberFFchanged() != null && myPassenger.getCodeFFchanged().equals(request.getParameter("numberFFchanged"))){
					passengerToUpdate.setNumberFFchanged("");
				}
				else{
					passengerToUpdate.setNumberFFchanged(request.getParameter("numberFFchanged"));
					myPassenger.setNumberFFchanged(request.getParameter("numberFFchanged"));
				}

				if (myPassenger.getIsChanged() == null)
					passengerToUpdate.setIsChanged(false);
				else
					passengerToUpdate.setIsChanged(myPassenger.getIsChanged());

				if (myPassenger.getTicketStatus() == null)
					passengerToUpdate.setTicketStatus("");
				else
					passengerToUpdate.setTicketStatus(myPassenger.getTicketStatus());

				if (myPassenger.getTicket() == null)
					passengerToUpdate.setTicket("");
				else
					passengerToUpdate.setTicket(myPassenger.getTicket());

				if (myPassenger.getPassengerType() == null)
					passengerToUpdate.setPassengerType("");
				else
					passengerToUpdate.setPassengerType(myPassenger.getPassengerType());

				List<Document> documents = new ArrayList<>();
				for(com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Document doc : myPassenger.getDocuments()){
					Document myDoc = new Document();
					myDoc.setBirthDate(doc.getBirthDate());
					myDoc.setEmissionCountry(doc.getEmissionCountry());
					myDoc.setExpirationDate(doc.getExpirationDate());
					myDoc.setFirstName(doc.getFirstName());
					myDoc.setInfant(doc.getInfant());
					myDoc.setLastName(doc.getLastName());
					myDoc.setNumber(doc.getNumber());
					myDoc.setSecondName(doc.getSecondName());
					myDoc.setType(doc.getType());
					documents.add(myDoc);
				}
				passengerToUpdate.setDocuments(documents);

				List<DocumentRequired> documentRequiredList = new ArrayList<>();
				List<AdultDocuments> adultDocumentsList = new ArrayList<>();
				List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments> adultDocumentsListUpdated = new ArrayList<>();
				List<InfantDocuments> infantDocumentsList = new ArrayList<>();
				List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments> infantDocumentsListUpdated = new ArrayList<>();
				AdultDocumentOther adultDocumentOther = new AdultDocumentOther();
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocumentOther adultDocumentOtherUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocumentOther();
				InfantDocumentOther infantDocumentOther = new InfantDocumentOther();
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocumentOther infantDocumentOtherUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocumentOther();
				AdultResidentDocument adultResidentDocument = new AdultResidentDocument();
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultResidentDocument adultResidentDocumentUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultResidentDocument();
				InfantResidentDocument infantResidentDocument = new InfantResidentDocument();
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantResidentDocument infantResidentDocumentUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantResidentDocument();
				AdultDestinationDocument adultDestinationDocument = new AdultDestinationDocument();
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDestinationDocument adultDestinationDocumentUpdated = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDestinationDocument();
				InfantDestinationDocument infantDestinationDocument = new InfantDestinationDocument();
				com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDestinationDocument infantDestinationDocumentUpdated =  new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDestinationDocument();

				passengerToUpdate.setAdultDocumentsSRequired(false);
				passengerToUpdate.setInfantDocumentsSRequired(false);
				passengerToUpdate.setAdultDocumentOtherRequired(false);
				passengerToUpdate.setInfantDocumentOtherRequired(false);
				passengerToUpdate.setAdultResidentDocumentRequired(false);
				passengerToUpdate.setInfantResidentDocumentRequired(false);
				passengerToUpdate.setAdultDestinationDocumentRequired(false);
				passengerToUpdate.setInfantDestinationDocumentRequired(false);
				passengerToUpdate.setAdultDocVRequired(false);
				passengerToUpdate.setInfantDocVRequired(false);

				myPassenger.setAdultDocumentsSRequired(false);
				myPassenger.setInfantDocumentsSRequired(false);
				myPassenger.setAdultDocumentOtherRequired(false);
				myPassenger.setInfantDocumentOtherRequired(false);
				myPassenger.setAdultResidentDocumentRequired(false);
				myPassenger.setInfantResidentDocumentRequired(false);
				myPassenger.setAdultDestinationDocumentRequired(false);
				myPassenger.setInfantDestinationDocumentRequired(false);
				myPassenger.setAdultDocVRequired(false);
				myPassenger.setInfantDocVRequired(false);

				boolean docsAdded = false;
				boolean docsAddedInf = false;
				boolean docsEstaAdded = false;
				boolean docsEstaAddedInf = false;

				if(myPassenger.getDocumentRequired() != null && myPassenger.getDocumentRequired().size() > 0){

					boolean adInsert = false;
					boolean estaInfant = false;

					for(int k=0; k<myPassenger.getDocumentRequired().size(); k++) {
						DocumentRequired docr = new DocumentRequired();
						docr.setCode(myPassenger.getDocumentRequired().get(k).getCode());
						if(docr.getCode().equals("DOCS/INF")){
							estaInfant = true;
						}
					}

					for(int k=0; k<myPassenger.getDocumentRequired().size(); k++){
						DocumentRequired documentRequired = new DocumentRequired();
						documentRequired.setCode(myPassenger.getDocumentRequired().get(k).getCode());
						documentRequired.setDetailStatus(myPassenger.getDocumentRequired().get(k).getDetailStatus());
						documentRequired.setFreeText(myPassenger.getDocumentRequired().get(0).getFreeText());
						documentRequiredList.add(documentRequired);

						if(documentRequired.getCode().equals("DOCS-P") || documentRequired.getCode().equals("DOCS-C") )
						{	//TODO - cablatura!
							if (!adInsert)
							{
								passengerToUpdate.setAdultDocumentsSRequired(true);
								myPassenger.setAdultDocumentsSRequired(true);
								AdultDocuments adultDocuments = new AdultDocuments();
								com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments adultDocumentsToUpdate = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments();
								String adultDocs = (request.getParameter("numberOfAdultDocuments") == "" || request.getParameter("numberOfAdultDocuments") == null) ? "0" : request.getParameter("numberOfAdultDocuments");
								int numberOfAdultDocuments = Integer.parseInt(adultDocs);

								for (int p = 0; p < numberOfAdultDocuments; p++) {
									adultDocuments.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
									adultDocumentsToUpdate.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
									adultDocuments.setExpirationDate(request.getParameter("expirationDate_" + p));
									adultDocumentsToUpdate.setExpirationDate(request.getParameter("expirationDate_" + p));
									adultDocuments.setFirstName(request.getParameter("firstName_" + p));
									adultDocumentsToUpdate.setFirstName(request.getParameter("firstName_" + p));
									adultDocuments.setGender(request.getParameter("gender_" + p));
									adultDocumentsToUpdate.setGender(request.getParameter("gender_" + p));
									adultDocuments.setIssueCountry(request.getParameter("issueCountry_" + p));
									adultDocumentsToUpdate.setIssueCountry(request.getParameter("issueCountry_" + p));
									adultDocuments.setLastName(request.getParameter("lastName_" + p));
									adultDocumentsToUpdate.setLastName(request.getParameter("lastName_" + p));
									adultDocuments.setNationality(request.getParameter("nationality_" + p));
									adultDocumentsToUpdate.setNationality(request.getParameter("nationality_" + p));
									adultDocuments.setNumber(request.getParameter("number_" + p));
									adultDocumentsToUpdate.setNumber(request.getParameter("number_" + p));
									adultDocuments.setType(request.getParameter("type_" + p));
									adultDocumentsToUpdate.setType(request.getParameter("type_" + p));
									adultDocumentsList.add(adultDocuments);
									adultDocumentsListUpdated.add(adultDocumentsToUpdate);
									adInsert = true;
									docsAdded = true;
								}
							}
						}
						if(documentRequired.getCode().equals("DOCS/INF")){ 				//TODO - cablatura!
							passengerToUpdate.setInfantDocumentsSRequired(true);
							myPassenger.setInfantDocumentsSRequired(true);
							InfantDocuments infantDocuments = new InfantDocuments();
							com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments infantDocumentsToUpdate = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments();
							String infantDocs = (request.getParameter("numberOfInfantDocuments") == "" || request.getParameter("numberOfInfantDocuments") == null) ? "0" :  request.getParameter("numberOfInfantDocuments");
							int numberOfInfantDocuments = Integer.parseInt(infantDocs);
							for(int t=0; t<numberOfInfantDocuments; t++){
								infantDocuments.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
								infantDocumentsToUpdate.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
								infantDocuments.setExpirationDate(request.getParameter("expirationDateInfant_" + t));
								infantDocumentsToUpdate.setExpirationDate(request.getParameter("expirationDateInfant_" + t));
								infantDocuments.setFirstName(request.getParameter("firstNameInfant_" + t));
								infantDocumentsToUpdate.setFirstName(request.getParameter("firstNameInfant_" + t));
								infantDocuments.setGender(request.getParameter("genderInfant_" + t));
								infantDocumentsToUpdate.setGender(request.getParameter("genderInfant_" + t));
								infantDocuments.setIssueCountry(request.getParameter("issueCountryInfant_" + t));
								infantDocumentsToUpdate.setIssueCountry(request.getParameter("issueCountryInfant_" + t));
								infantDocuments.setLastName(request.getParameter("lastNameInfant_" + t));
								infantDocumentsToUpdate.setLastName(request.getParameter("lastNameInfant_" + t));
								infantDocuments.setNationality(request.getParameter("nationalityInfant_" + t));
								infantDocumentsToUpdate.setNationality(request.getParameter("nationalityInfant_" + t));
								infantDocuments.setNumber(request.getParameter("numberInfant_" + t));
								infantDocumentsToUpdate.setNumber(request.getParameter("numberInfant_" + t));
								infantDocuments.setType(request.getParameter("typeInfant_" + t));
								infantDocumentsToUpdate.setType(request.getParameter("typeInfant_" + t));
								infantDocumentsList.add(infantDocuments);
								infantDocumentsListUpdated.add(infantDocumentsToUpdate);
								docsAddedInf = true;
							}
						}
						if(documentRequired.getCode().equals("ESTA")){ 					//TODO - cablatura!
							String documentType = request.getParameter("documentTypeAdult");
							if(documentType.equals("esta")){
								passengerToUpdate.setAdultDocumentOtherRequired(false);
								myPassenger.setAdultDocumentOtherRequired(false);
							}
							if(documentType.equals("visto")){
								passengerToUpdate.setAdultDocumentOtherRequired(true);
								myPassenger.setAdultDocumentOtherRequired(true);
								adultDocumentOther.setApplicableCountryCode(request.getParameter("applicableCountryCodeAdult"));
								adultDocumentOtherUpdated.setApplicableCountryCode(request.getParameter("applicableCountryCodeAdult"));
								adultDocumentOther.setBirthPlace("");
								adultDocumentOtherUpdated.setBirthPlace("");
								adultDocumentOther.setIssueCity(request.getParameter("issueCityAdult"));
								adultDocumentOtherUpdated.setIssueCity(request.getParameter("issueCityAdult"));
								adultDocumentOther.setIssueDate(request.getParameter("issueDateAdult"));
								adultDocumentOtherUpdated.setIssueDate(request.getParameter("issueDateAdult"));
								adultDocumentOther.setNumber(request.getParameter("numberOtherDocumentAdult"));
								adultDocumentOtherUpdated.setNumber(request.getParameter("numberOtherDocumentAdult"));
							}
							if(documentType.equals("green")){
								AdultDocuments adultDocumentsGreenCard = new AdultDocuments();
								com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments adultDocumentsToUpdateGreenCard = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments();
								String adultDocs = (request.getParameter("numberOfAdultDocuments") == "" || request.getParameter("numberOfAdultDocuments") == null) ? "0" :  request.getParameter("numberOfAdultDocuments");
								int numberOfAdultDocuments = Integer.parseInt(adultDocs);

								for(int p=0; p<numberOfAdultDocuments; p++){
									adultDocumentsGreenCard.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
									adultDocumentsToUpdateGreenCard.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
									adultDocumentsGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardAdult_" + p));
									adultDocumentsToUpdateGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardAdult_" + p));
									adultDocumentsGreenCard.setFirstName(request.getParameter("firstName_" + p));
									adultDocumentsToUpdateGreenCard.setFirstName(request.getParameter("firstName_" + p));
									adultDocumentsGreenCard.setGender(request.getParameter("gender_" + p));
									adultDocumentsToUpdateGreenCard.setGender(request.getParameter("gender_" + p));
									adultDocumentsGreenCard.setIssueCountry("US");
									adultDocumentsToUpdateGreenCard.setIssueCountry("US");
									adultDocumentsGreenCard.setLastName(request.getParameter("lastName_" + p));
									adultDocumentsToUpdateGreenCard.setLastName(request.getParameter("lastName_" + p));
									adultDocumentsGreenCard.setNationality("US");
									adultDocumentsToUpdateGreenCard.setNationality("US");
									adultDocumentsGreenCard.setNumber(request.getParameter("numberGreenCardAdult_" + p));
									adultDocumentsToUpdateGreenCard.setNumber(request.getParameter("numberGreenCardAdult_" + p));
									adultDocumentsGreenCard.setType("GREENCARD");
									adultDocumentsToUpdateGreenCard.setType("GREENCARD");
									adultDocumentsList.add(adultDocumentsGreenCard);
									adultDocumentsListUpdated.add(adultDocumentsToUpdateGreenCard);
								}
							}
							docsEstaAdded = true;
						}

						if(documentRequired.getCode().equals("DOCA/R")){
							if(request.getParameter("issueCountry_0").equals("US") && request.getParameter("nationality_0").equals("US")){
								passengerToUpdate.setAdultResidentDocumentRequired(false);
								myPassenger.setAdultResidentDocumentRequired(false);
							}else{
								passengerToUpdate.setAdultResidentDocumentRequired(true);
								myPassenger.setAdultResidentDocumentRequired(true);
								adultResidentDocument.setCity(request.getParameter("cityResident"));
								adultResidentDocumentUpdated.setCity(request.getParameter("cityResident"));
								adultResidentDocument.setCountryCode(request.getParameter("countryCodeResident"));
								adultResidentDocumentUpdated.setCountryCode(request.getParameter("countryCodeResident"));
								adultResidentDocument.setStateProvince(request.getParameter("stateProvinceResident"));
								adultResidentDocumentUpdated.setStateProvince(request.getParameter("stateProvinceResident"));
								adultResidentDocument.setStreetAddress(request.getParameter("streetAddressResident"));
								adultResidentDocumentUpdated.setStreetAddress(request.getParameter("streetAddressResident"));
								adultResidentDocument.setZipCode(request.getParameter("zipCodeAdultResident"));
								adultResidentDocumentUpdated.setZipCode(request.getParameter("zipCodeAdultResident"));
							}
						}
						if(documentRequired.getCode().equals("DOCA/R/INF")){
							if(request.getParameter("issueCountryInfant_0").equals("US") && request.getParameter("nationalityInfant_0").equals("US")){
								passengerToUpdate.setInfantResidentDocumentRequired(false);
								myPassenger.setInfantResidentDocumentRequired(false);
							}else{
								passengerToUpdate.setInfantResidentDocumentRequired(true);
								myPassenger.setInfantResidentDocumentRequired(true);
								infantResidentDocument.setCity(request.getParameter("cityInfantResident"));
								infantResidentDocumentUpdated.setCity(request.getParameter("cityInfantResident"));
								infantResidentDocument.setCountryCode(request.getParameter("countryCodeInfantResident"));
								infantResidentDocumentUpdated.setCountryCode(request.getParameter("countryCodeInfantResident"));
								infantResidentDocument.setStateProvince(request.getParameter("stateProvinceInfantResident"));
								infantResidentDocumentUpdated.setStateProvince(request.getParameter("stateProvinceInfantResident"));
								infantResidentDocument.setStreetAddress(request.getParameter("streetAddressInfantResident"));
								infantResidentDocumentUpdated.setStreetAddress(request.getParameter("streetAddressInfantResident"));
								infantResidentDocument.setZipCode(request.getParameter("zipCodeInfantResident"));
								infantResidentDocumentUpdated.setZipCode(request.getParameter("zipCodeInfantResident"));
							}

						}
						if(documentRequired.getCode().equals("DOCA/D")){
							if(request.getParameter("issueCountry_0").equals("US")
									&& request.getParameter("nationality_0").equals("US")){
								passengerToUpdate.setAdultDestinationDocumentRequired(false);
								myPassenger.setAdultDestinationDocumentRequired(false);
							}else{
								passengerToUpdate.setAdultDestinationDocumentRequired(true);
								myPassenger.setAdultDestinationDocumentRequired(true);
								adultDestinationDocument.setCity(request.getParameter("cityDestination"));
								adultDestinationDocument.setCountryCode(request.getParameter("countryCodeDestination"));
								adultDestinationDocument.setStateProvince(request.getParameter("stateProvinceDestination"));
								adultDestinationDocument.setStreetAddress(request.getParameter("streetAddressDestination"));
								adultDestinationDocument.setZipCode(request.getParameter("zipCodeAdultDestination"));
								adultDestinationDocumentUpdated.setCity(request.getParameter("cityDestination"));
								adultDestinationDocumentUpdated.setCountryCode(request.getParameter("countryCodeDestination"));
								adultDestinationDocumentUpdated.setStateProvince(request.getParameter("stateProvinceDestination"));
								adultDestinationDocumentUpdated.setStreetAddress(request.getParameter("streetAddressDestination"));
								adultDestinationDocumentUpdated.setZipCode(request.getParameter("zipCodeAdultDestination"));
							}

						}
						if(documentRequired.getCode().equals("DOCA/D/INF")){
							if(request.getParameter("issueCountryInfant_0").equals("US")
									&& request.getParameter("nationalityInfant_0").equals("US")){
								passengerToUpdate.setInfantDestinationDocumentRequired(false);
								myPassenger.setInfantDestinationDocumentRequired(false);
							}else{
								passengerToUpdate.setInfantDestinationDocumentRequired(true);
								myPassenger.setInfantDestinationDocumentRequired(true);
								infantDestinationDocument.setCity(request.getParameter("cityInfantDestination"));
								infantDestinationDocument.setCountryCode(request.getParameter("countryCodeInfantDestination"));
								infantDestinationDocument.setStateProvince(request.getParameter("stateProvinceInfantDestination"));
								infantDestinationDocument.setStreetAddress(request.getParameter("streetAddressInfantDestination"));
								infantDestinationDocument.setZipCode(request.getParameter("zipCodeInfantDestination"));
								infantDestinationDocumentUpdated.setCity(request.getParameter("cityInfantDestination"));
								infantDestinationDocumentUpdated.setCountryCode(request.getParameter("countryCodeInfantDestination"));
								infantDestinationDocumentUpdated.setStateProvince(request.getParameter("stateProvinceInfantDestination"));
								infantDestinationDocumentUpdated.setStreetAddress(request.getParameter("streetAddressInfantDestination"));
								infantDestinationDocumentUpdated.setZipCode(request.getParameter("zipCodeInfantDestination"));
							}

						}
						if(documentRequired.getCode().equals("DOCV")){
							passengerToUpdate.setAdultDocVRequired(false);
							passengerToUpdate.setInfantDocVRequired(false);
							myPassenger.setAdultDocVRequired(false);
							myPassenger.setInfantDocVRequired(false);
						}
					}
				}else{
					passengerToUpdate.setAdultDocumentsSRequired(false);
					passengerToUpdate.setInfantDocumentsSRequired(false);
					passengerToUpdate.setAdultDocumentOtherRequired(false);
					passengerToUpdate.setInfantDocumentOtherRequired(false);
					passengerToUpdate.setAdultResidentDocumentRequired(false);
					passengerToUpdate.setInfantResidentDocumentRequired(false);
					passengerToUpdate.setAdultDestinationDocumentRequired(false);
					passengerToUpdate.setInfantDestinationDocumentRequired(false);
					passengerToUpdate.setAdultDocVRequired(false);
					passengerToUpdate.setInfantDocVRequired(false);
					myPassenger.setAdultDocumentsSRequired(false);
					myPassenger.setInfantDocumentsSRequired(false);
					myPassenger.setAdultDocumentOtherRequired(false);
					myPassenger.setInfantDocumentOtherRequired(false);
					myPassenger.setAdultResidentDocumentRequired(false);
					myPassenger.setInfantResidentDocumentRequired(false);
					myPassenger.setAdultDestinationDocumentRequired(false);
					myPassenger.setInfantDestinationDocumentRequired(false);
					myPassenger.setAdultDocVRequired(false);
					myPassenger.setInfantDocVRequired(false);
				}
				if(myPassenger.getDocuments() != null && myPassenger.getDocuments().size() > 0) {
					for (int k = 0; k < myPassenger.getDocuments().size(); k++) {
						Document documentPass = new Document();
						documentPass.setType(myPassenger.getDocuments().get(k).getType());
						documentPass.setInfant(myPassenger.getDocuments().get(k).getInfant());
						//documentRequiredList.add(documentRequired);

						if (documentPass.getType().equals("Passport") || documentPass.getType().equals("NationalID")) {    //TODO - cablatura!
							if (documentPass.getInfant() && docsAddedInf == false) {
								passengerToUpdate.setInfantDocumentsSRequired(true);
								myPassenger.setInfantDocumentsSRequired(true);
								InfantDocuments infantDocuments = new InfantDocuments();
								com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments infantDocumentsToUpdate = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments();
								String infantDocs = (request.getParameter("numberOfInfantDocuments") == "" || request.getParameter("numberOfInfantDocuments") == null) ? "0" : request.getParameter("numberOfInfantDocuments");
								int numberOfInfantDocuments = Integer.parseInt(infantDocs);
								for (int t = 0; t < numberOfInfantDocuments; t++) {
									infantDocuments.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
									infantDocumentsToUpdate.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
									infantDocuments.setExpirationDate(request.getParameter("expirationDateInfant_" + t));
									infantDocumentsToUpdate.setExpirationDate(request.getParameter("expirationDateInfant_" + t));
									infantDocuments.setFirstName(request.getParameter("firstNameInfant_" + t));
									infantDocumentsToUpdate.setFirstName(request.getParameter("firstNameInfant_" + t));
									infantDocuments.setGender(request.getParameter("genderInfant_" + t));
									infantDocumentsToUpdate.setGender(request.getParameter("genderInfant_" + t));
									infantDocuments.setIssueCountry(request.getParameter("issueCountryInfant_" + t));
									infantDocumentsToUpdate.setIssueCountry(request.getParameter("issueCountryInfant_" + t));
									infantDocuments.setLastName(request.getParameter("lastNameInfant_" + t));
									infantDocumentsToUpdate.setLastName(request.getParameter("lastNameInfant_" + t));
									infantDocuments.setNationality(request.getParameter("nationalityInfant_" + t));
									infantDocumentsToUpdate.setNationality(request.getParameter("nationalityInfant_" + t));
									infantDocuments.setNumber(request.getParameter("numberInfant_" + t));
									infantDocumentsToUpdate.setNumber(request.getParameter("numberInfant_" + t));
									infantDocuments.setType(request.getParameter("typeInfant_" + t));
									infantDocumentsToUpdate.setType(request.getParameter("typeInfant_" + t));
									infantDocumentsList.add(infantDocuments);
									infantDocumentsListUpdated.add(infantDocumentsToUpdate);
								}
							} else {
								if (docsAdded == false) {
									passengerToUpdate.setAdultDocumentsSRequired(true);
									myPassenger.setAdultDocumentsSRequired(true);
									AdultDocuments adultDocuments = new AdultDocuments();
									com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments adultDocumentsToUpdate = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments();
									String adultDocs = (request.getParameter("numberOfAdultDocuments") == "" || request.getParameter("numberOfAdultDocuments") == null) ? "0" : request.getParameter("numberOfAdultDocuments");
									int numberOfAdultDocuments = Integer.parseInt(adultDocs);

									for (int p = 0; p < numberOfAdultDocuments; p++) {
										adultDocuments.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
										adultDocumentsToUpdate.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
										adultDocuments.setExpirationDate(request.getParameter("expirationDate_" + p));
										adultDocumentsToUpdate.setExpirationDate(request.getParameter("expirationDate_" + p));
										adultDocuments.setFirstName(request.getParameter("firstName_" + p));
										adultDocumentsToUpdate.setFirstName(request.getParameter("firstName_" + p));
										adultDocuments.setGender(request.getParameter("gender_" + p));
										adultDocumentsToUpdate.setGender(request.getParameter("gender_" + p));
										adultDocuments.setIssueCountry(request.getParameter("issueCountry_" + p));
										adultDocumentsToUpdate.setIssueCountry(request.getParameter("issueCountry_" + p));
										adultDocuments.setLastName(request.getParameter("lastName_" + p));
										adultDocumentsToUpdate.setLastName(request.getParameter("lastName_" + p));
										adultDocuments.setNationality(request.getParameter("nationality_" + p));
										adultDocumentsToUpdate.setNationality(request.getParameter("nationality_" + p));
										adultDocuments.setNumber(request.getParameter("number_" + p));
										adultDocumentsToUpdate.setNumber(request.getParameter("number_" + p));
										adultDocuments.setType(request.getParameter("type_" + p));
										adultDocumentsToUpdate.setType(request.getParameter("type_" + p));
										adultDocumentsList.add(adultDocuments);
										adultDocumentsListUpdated.add(adultDocumentsToUpdate);
									}
								}
							}

						}
						if (documentPass.getType().equals("DOCA-R")) {
							if (request.getParameter("issueCountry_0").equals("US") && request.getParameter("nationality_0").equals("US")) {
								passengerToUpdate.setAdultResidentDocumentRequired(false);
								myPassenger.setAdultResidentDocumentRequired(false);
							} else {
								passengerToUpdate.setAdultResidentDocumentRequired(true);
								myPassenger.setAdultResidentDocumentRequired(true);
								adultResidentDocument.setCity(request.getParameter("cityResident"));
								adultResidentDocumentUpdated.setCity(request.getParameter("cityResident"));
								adultResidentDocument.setCountryCode(request.getParameter("countryCodeResident"));
								adultResidentDocumentUpdated.setCountryCode(request.getParameter("countryCodeResident"));
								adultResidentDocument.setStateProvince(request.getParameter("stateProvinceResident"));
								adultResidentDocumentUpdated.setStateProvince(request.getParameter("stateProvinceResident"));
								adultResidentDocument.setStreetAddress(request.getParameter("streetAddressResident"));
								adultResidentDocumentUpdated.setStreetAddress(request.getParameter("streetAddressResident"));
								adultResidentDocument.setZipCode(request.getParameter("zipCodeAdultResident"));
								adultResidentDocumentUpdated.setZipCode(request.getParameter("zipCodeAdultResident"));
							}
						}
						if (documentPass.getType().equals("DOCA-D")) {
							if (request.getParameter("issueCountry_0").equals("US") && request.getParameter("nationality_0").equals("US")) {
								passengerToUpdate.setAdultDestinationDocumentRequired(false);
								myPassenger.setAdultDestinationDocumentRequired(false);
							} else {
								passengerToUpdate.setAdultDestinationDocumentRequired(true);
								myPassenger.setAdultDestinationDocumentRequired(true);
								adultDestinationDocument.setCity(request.getParameter("cityDestination"));
								adultDestinationDocument.setCountryCode(request.getParameter("countryCodeDestination"));
								adultDestinationDocument.setStateProvince(request.getParameter("stateProvinceDestination"));
								adultDestinationDocument.setStreetAddress(request.getParameter("streetAddressDestination"));
								adultDestinationDocument.setZipCode(request.getParameter("zipCodeAdultDestination"));
								adultDestinationDocumentUpdated.setCity(request.getParameter("cityDestination"));
								adultDestinationDocumentUpdated.setCountryCode(request.getParameter("countryCodeDestination"));
								adultDestinationDocumentUpdated.setStateProvince(request.getParameter("stateProvinceDestination"));
								adultDestinationDocumentUpdated.setStreetAddress(request.getParameter("streetAddressDestination"));
								adultDestinationDocumentUpdated.setZipCode(request.getParameter("zipCodeAdultDestination"));
							}

						}
					}
					boolean hasDocsInf = false;
					boolean hasEsta = false;
					boolean hasGreenCard = false;
					boolean hasGreenCardInfant = false;
					for (int k = 0; k < myPassenger.getDocuments().size(); k++) {
						Document docsPass = new Document();
						docsPass.setType(myPassenger.getDocuments().get(k).getType());
						docsPass.setInfant(myPassenger.getDocuments().get(k).getInfant());
						if (docsPass.getType().equals("Passport")) {
							if (docsPass.getInfant()) {
								hasDocsInf = true;
							}
						}
						if (docsPass.getType().equals("GreenCard")) {
							if (docsPass.getInfant()) {
								hasGreenCardInfant = true;
							} else {
								hasGreenCard = true;
							}
						}
						if (docsPass.getType().equals("Esta")) {
							hasEsta = true;
						}
					}
					if (docsEstaAdded == false) {
						if ((hasEsta && isDestinationUS) || (hasGreenCard && isDestinationUS)) { // Esta/visto/greencard adulto
							String documentType = request.getParameter("documentTypeAdult");
							if (documentType.equals("esta")) {
								passengerToUpdate.setAdultDocumentOtherRequired(false);
								myPassenger.setAdultDocumentOtherRequired(false);
							}
							if (documentType.equals("visto")) {
								passengerToUpdate.setAdultDocumentOtherRequired(true);
								myPassenger.setAdultDocumentOtherRequired(true);
								adultDocumentOther.setApplicableCountryCode(request.getParameter("applicableCountryCodeAdult"));
								adultDocumentOtherUpdated.setApplicableCountryCode(request.getParameter("applicableCountryCodeAdult"));
								adultDocumentOther.setBirthPlace("");
								adultDocumentOtherUpdated.setBirthPlace("");
								adultDocumentOther.setIssueCity(request.getParameter("issueCityAdult"));
								adultDocumentOtherUpdated.setIssueCity(request.getParameter("issueCityAdult"));
								adultDocumentOther.setIssueDate(request.getParameter("issueDateAdult"));
								adultDocumentOtherUpdated.setIssueDate(request.getParameter("issueDateAdult"));
								adultDocumentOther.setNumber(request.getParameter("numberOtherDocumentAdult"));
								adultDocumentOtherUpdated.setNumber(request.getParameter("numberOtherDocumentAdult"));
							}
							if (documentType.equals("green")) {
								AdultDocuments adultDocumentsGreenCard = new AdultDocuments();
								com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments adultDocumentsToUpdateGreenCard = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.AdultDocuments();
								String adultDocs = (request.getParameter("numberOfAdultDocuments") == "" || request.getParameter("numberOfAdultDocuments") == null) ? "0" : request.getParameter("numberOfAdultDocuments");
								int numberOfAdultDocuments = Integer.parseInt(adultDocs);

								for (int p = 0; p < numberOfAdultDocuments; p++) {
									adultDocumentsGreenCard.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
									adultDocumentsToUpdateGreenCard.setDateOfBirth(request.getParameter("dateOfBirth_" + p));
									adultDocumentsGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardAdult_" + p));
									adultDocumentsToUpdateGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardAdult_" + p));
									adultDocumentsGreenCard.setFirstName(request.getParameter("firstName_" + p));
									adultDocumentsToUpdateGreenCard.setFirstName(request.getParameter("firstName_" + p));
									adultDocumentsGreenCard.setGender(request.getParameter("gender_" + p));
									adultDocumentsToUpdateGreenCard.setGender(request.getParameter("gender_" + p));
									adultDocumentsGreenCard.setIssueCountry("US");
									adultDocumentsToUpdateGreenCard.setIssueCountry("US");
									adultDocumentsGreenCard.setLastName(request.getParameter("lastName_" + p));
									adultDocumentsToUpdateGreenCard.setLastName(request.getParameter("lastName_" + p));
									adultDocumentsGreenCard.setNationality("US");
									adultDocumentsToUpdateGreenCard.setNationality("US");
									adultDocumentsGreenCard.setNumber(request.getParameter("numberGreenCardAdult_" + p));
									adultDocumentsToUpdateGreenCard.setNumber(request.getParameter("numberGreenCardAdult_" + p));
									adultDocumentsGreenCard.setType("GREENCARD");
									adultDocumentsToUpdateGreenCard.setType("GREENCARD");
									adultDocumentsList.add(adultDocumentsGreenCard);
									adultDocumentsListUpdated.add(adultDocumentsToUpdateGreenCard);
								}
							}
						}
					}
					if (docsEstaAddedInf == false) {
						if ((hasEsta && hasDocsInf && isDestinationUS) || (hasGreenCard && hasGreenCardInfant && isDestinationUS)) { // Esta/visto/greenCard bambino
							String documentTypeInfant = request.getParameter("documentTypeInfant");
							if (documentTypeInfant.equals("esta")) {
								passengerToUpdate.setInfantDocumentOtherRequired(false);
								myPassenger.setInfantDocumentOtherRequired(false);
							}
							if (documentTypeInfant.equals("visto")) {
								passengerToUpdate.setInfantDocumentOtherRequired(true);
								myPassenger.setInfantDocumentOtherRequired(true);
								infantDocumentOther.setApplicableCountryCode(request.getParameter("applicableCountryCodeInfant"));
								infantDocumentOtherUpdated.setApplicableCountryCode(request.getParameter("applicableCountryCodeInfant"));
								infantDocumentOther.setBirthPlace("");
								infantDocumentOtherUpdated.setBirthPlace("");
								infantDocumentOther.setIssueCity(request.getParameter("issueCityInfant"));
								infantDocumentOtherUpdated.setIssueCity(request.getParameter("issueCityInfant"));
								infantDocumentOther.setIssueDate(request.getParameter("issueDateInfant"));
								infantDocumentOtherUpdated.setIssueDate(request.getParameter("issueDateInfant"));
								infantDocumentOther.setNumber(request.getParameter("numberOtherDocumentInfant"));
								infantDocumentOtherUpdated.setNumber(request.getParameter("numberOtherDocumentInfant"));
							}
							if (documentTypeInfant.equals("green")) {
								InfantDocuments infantDocumentsGreenCard = new InfantDocuments();
								com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments infantDocumentsToUpdateGreenCard = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.InfantDocuments();
								String infantDocs = (request.getParameter("numberOfInfantDocuments") == "" || request.getParameter("numberOfInfantDocuments") == null) ? "0" : request.getParameter("numberOfInfantDocuments");
								int numberOfInfantDocuments = Integer.parseInt(infantDocs);
								for (int t = 0; t < numberOfInfantDocuments; t++) {
									infantDocumentsGreenCard.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
									infantDocumentsToUpdateGreenCard.setDateOfBirth(request.getParameter("dateOfBirthInfant_" + t));
									infantDocumentsGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardInfant_" + t));
									infantDocumentsToUpdateGreenCard.setExpirationDate(request.getParameter("expireDateGreenCardInfant_" + t));
									infantDocumentsGreenCard.setFirstName(request.getParameter("firstNameInfant_" + t));
									infantDocumentsToUpdateGreenCard.setFirstName(request.getParameter("firstNameInfant_" + t));
									infantDocumentsGreenCard.setGender(request.getParameter("genderInfant_" + t));
									infantDocumentsToUpdateGreenCard.setGender(request.getParameter("genderInfant_" + t));
									infantDocumentsGreenCard.setIssueCountry("US");
									infantDocumentsToUpdateGreenCard.setIssueCountry("US");
									infantDocumentsGreenCard.setLastName(request.getParameter("lastNameInfant_" + t));
									infantDocumentsToUpdateGreenCard.setLastName(request.getParameter("lastNameInfant_" + t));
									infantDocumentsGreenCard.setNationality("US");
									infantDocumentsToUpdateGreenCard.setNationality("US");
									infantDocumentsGreenCard.setNumber(request.getParameter("numberGreenCardInfant_" + t));
									infantDocumentsToUpdateGreenCard.setNumber(request.getParameter("numberGreenCardInfant" + t));
									infantDocumentsGreenCard.setType("GREENCARD");
									infantDocumentsToUpdateGreenCard.setType("GREENCARD");
									infantDocumentsList.add(infantDocumentsGreenCard);
									infantDocumentsListUpdated.add(infantDocumentsToUpdateGreenCard);
								}
							}
						}
					}
				}


				passengerToUpdate.setDocumentRequired(documentRequiredList);
				passengerToUpdate.setAdultDocumentsS(adultDocumentsList);
				passengerToUpdate.setInfantDocumentsS(infantDocumentsList);
				passengerToUpdate.setAdultDocumentOther(adultDocumentOther);
				passengerToUpdate.setInfantDocumentOther(infantDocumentOther);
				passengerToUpdate.setAdultResidentDocument(adultResidentDocument);
				passengerToUpdate.setInfantResidentDocument(infantResidentDocument);
				passengerToUpdate.setAdultDestinationDocument(adultDestinationDocument);
				passengerToUpdate.setInfantDestinationDocument(infantDestinationDocument);

				myPassenger.setAdultDocumentsS(adultDocumentsListUpdated);
				myPassenger.setInfantDocumentsS(infantDocumentsListUpdated);
				myPassenger.setAdultDocumentOther(adultDocumentOtherUpdated);
				myPassenger.setInfantDocumentOther(infantDocumentOtherUpdated);
				myPassenger.setAdultResidentDocument(adultResidentDocumentUpdated);
				myPassenger.setInfantResidentDocument(infantResidentDocumentUpdated);
				myPassenger.setAdultDestinationDocument(adultDestinationDocumentUpdated);
				myPassenger.setInfantDestinationDocument(infantDestinationDocumentUpdated);

				List<com.alitalia.aem.common.data.checkinrest.model.updatepassenger.request.Passenger> updatedPassengerList = new ArrayList<>();
				updatedPassengerList.add(passengerToUpdate);
				serviceRequest.setPassengers(updatedPassengerList);

				response = checkInDelegateRest.updatePassenger(serviceRequest);

				if(response != null && response.get_updatepassengerResp().getPassengers().get(0).getResult().equals("OK")){
					if(response.get_updatepassengerResp().getConversationID() != null){
						ctx.conversationId = response.get_updatepassengerResp().getConversationID();
					}
					List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.DocumentRequired> xList = new ArrayList<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.DocumentRequired>();
					myPassenger.setDocumentRequired(xList);
					for(PnrRender pnr : ctx.pnrSelectedListDataRender){
						if(pnr.getNumber().equals(ctx.pnr)){
							for(FlightsRender flight : pnr.getFlightsRender()){
								if(departureDate.equals(flight.getOriginalDepartureDate())) {
									List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger> passengersExtraFromSession = new ArrayList<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger>();
									for (PassengerExtra passenger : flight.getSegments().get(0).getPassengersExtra()) {
										passengersExtraFromSession.add(passenger.getPassenger());
									}
									for(int i=0; i<passengersExtraFromSession.size(); i++){
										if(check.equals(passengersExtraFromSession.get(i).getNome() + "_" + passengersExtraFromSession.get(i).getCognome())){
											flight.getSegments().get(0).getPassengersExtra().get(i).setPassenger(myPassenger);
											logger.info("[CheckinSession][checkinUpdatePassenger] - Le informazioni del passeggero sono modificate e inserite nel Context.");
											break;
										}
									}
									break;
								}
							}
						}
					}
				}
			}
			else{
				logger.warn("[CheckinSession][checkinUpdatePassengerExtra] - Il passeggero selezionato non è stato trovato.");
			}
		}
		catch(Exception e){
			logger.error("[CheckinSession][checkinUpdatePassengerExtra] - Errore durante l'invocazione del servizio.", e);
		}

		return response;
	}

		/**
		 * Metodo d'utilità per la creazione degli oggetti Flight data una lista di PNR
		 * @param ctx
		 * @param pnrDataList
		 */
		//	private void createFlightMap (CheckinSessionContext ctx, List <Pnr> pnrDataList){
		//		Map<Integer, Map<String, Flight>> myMap = new HashMap<Integer, Map<String, Flight>>();
		//		Map<String, Flight> myFlights = null;
		//
		//		if(pnrDataList != null){
		//			for(int i=0; i< pnrDataList.size(); i++){
		//				myFlights = new HashMap<String, Flight>();
		//				String number = "";
		//				number = pnrDataList.get(i).getNumber();
		//				Flight flight = new Flight();
		//				for(Flight f : pnrDataList.get(i).getFlights()){
		//					flight.setArrivalDate(f.getArrivalDate());
		//					flight.setDepartureDate(f.getDepartureDate());
		//					flight.setDestination(f.getDestination());
		//					flight.setOrigin(f.getOrigin());
		//					flight.setSegments(f.getSegments());
		//					flight.setIsBpPrintPermitted(f.getIsBpPrintPermitted());
		//					flight.setIsWebCheckinPermitted(f.getIsWebCheckinPermitted());
		//					flight.setWebCheckInDeepLink(f.getWebCheckInDeepLink());
		//					myFlights.put(number, flight);
		//				}
		//				myMap.put(i, myFlights);
		//			}
		//		}
		//		ctx.flightsDetail = myMap;
		//	}

    /*
     * 		Service Payment : getCart()
     */
    public CheckinGetCartResponse getCart(SlingHttpServletRequest request, CheckinSessionContext ctx) {
        CheckinGetCartResponse serviceResponse = null;
        try {
            String decimalSeparator = new HierarchyNodeInheritanceValueMap(request.getResource().getParent()).getInherited("decimalSeparator", String.class);
            String decimalDigit = new HierarchyNodeInheritanceValueMap(request.getResource().getParent()).getInherited("decimalDigit", String.class);

//            logger.info("[CheckinSession] [getCart] costruzione Request...");
            CheckinGetCartRequest serviceRequest = new CheckinGetCartRequest(IDFactory.getTid(), IDFactory.getSid(request));
            serviceRequest.setPnr(ctx.pnr);
            serviceRequest.setMarket(ctx.market);
            serviceRequest.setLanguage(ctx.language);
            serviceRequest.setConversationID(ctx.conversationId);
            serviceRequest.setSeparatore(decimalSeparator);

//				logger.info("[CheckinSession] serviceRequest parametri [PNR=" + ctx.pnr + "]"
//						+ "[MARKET=" + ctx.market + "][LANGUAGE=" + ctx.language + "][CONVERSATIONID=" + ctx.conversationId + "]" );

//				logger.info("[CheckinSession] Chiamata del servizio getCart...");
				serviceResponse = paymentDelegateRest.getCart(serviceRequest);
//				logger.info("[CheckinSession] Ritorno del servizio getCart...");

				//Oggetto Data dopo la chiamata al servizio
				Cart cartResult = serviceResponse.getCart();	

				if(cartResult != null ){				
					//verificare se deve essere costruita una sessione
					if(cartResult.getCheckinCart() != null) {
						ctx.cartTotalAmount = BigDecimal.valueOf(cartResult.getCheckinCart().getCartPriceToBeIssued());
						ctx.totalPay = cartResult.getTotalPrice();
					}
					else
						ctx.cartTotalAmount = BigDecimal.valueOf(0);

//					logger.info("[CheckinSession] [getCart] Ritorno del servizio getCart");
				}


			}catch(Exception e){
				logger.error("[CheckinSession][getCart] - Errore durante l'invocazione del servizio.", e);

			}
			return serviceResponse;
		}

		public CheckinInitPaymentResponse getInitPayment(SlingHttpServletRequest request, CheckinSessionContext ctx, String client, String ipAddress, PaymentDetail paymentDetail) {
			CheckinInitPaymentResponse serviceResponse = null;
			ctx.checkinInitPaymentRequest = null;
			ctx.checkinInitPaymentResponse = null;
			try{
//				logger.info("[CheckinSession] [getInitPayment] costruzione Request...");
				CheckinInitPaymentRequest serviceRequest = new CheckinInitPaymentRequest(IDFactory.getTid(), IDFactory.getSid(request));
				serviceRequest.setPnr(ctx.pnr);
				serviceRequest.setClient(client);
				serviceRequest.setiP_Address(ipAddress);
				serviceRequest.setEmail(request.getParameter("email"));
				serviceRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
				serviceRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
				serviceRequest.setConversationID(ctx.conversationId);
				serviceRequest.setPaymentStatus(""); //Per il pagamento nel Init stringa VUOTA. Nel metodo End dovrà essere valorizzato ad "AUTHORIZED".
				serviceRequest.setPaymentDetail(paymentDetail);

//				aggiungo l'oggetto insurance payment se inserito a carrello
				InsurancePayment insurancePayment = new InsurancePayment();
				if(request.getParameter("insuranceAmount") != null && !request.getParameter("insuranceAmount").isEmpty() && Double.parseDouble(request.getParameter("insuranceAmount")) > 0){
					insurancePayment.setBuyInsurance(true);// di base a false valorizzato a true in caso di scelta da ancillary
					insurancePayment.setInsuranceAmount(request.getParameter("insuranceAmount"));//costo dell'assicurazione
				} else{
					insurancePayment.setBuyInsurance(false);// di base a false valorizzato a true in caso di scelta da ancillary
					insurancePayment.setInsuranceAmount("");//costo dell'assicurazione
				}
				insurancePayment.setPolicyNumber("");//polizza assicurativa

				serviceRequest.setInsurancePayment(insurancePayment);
				serviceRequest.setFraudNetParam(ctx.listFraudNetParam);
				ctx.checkinInitPaymentRequest = serviceRequest;

				serviceResponse = paymentDelegateRest.initPayment(serviceRequest);

				if(serviceResponse != null) {
                    if (serviceResponse.getInitPaymentResponse().getError() != null && !serviceResponse.getInitPaymentResponse().getError().equals("")) {
                        return serviceResponse;
                    } else {
                        if (serviceResponse.getInitPaymentResponse() != null) {
                            ctx.checkinInitPaymentResponse = serviceResponse;
                        }
						//	non aggiorniamo il conversation id perche' uguale a quello della request
                    }
                }

			}catch(Exception e){
				logger.error("[CheckinSession][initPayment] - Errore durante l'invocazione del servizio.", e);
			}
			return serviceResponse;
		}

		public CheckinEndPaymentResponse endPayment(SlingHttpServletRequest request, CheckinSessionContext ctx, String approvalCode) {
			CheckinEndPaymentResponse serviceResponse = null;
			try{
				CheckinEndPaymentRequest endPaymentRequest = new CheckinEndPaymentRequest(IDFactory.getTid(), IDFactory.getSid(request));

				endPaymentRequest.setPnr(ctx.checkinInitPaymentRequest.getPnr());
				endPaymentRequest.setClient(ctx.checkinInitPaymentRequest.getClient());
				endPaymentRequest.setIP_Address(ctx.checkinInitPaymentRequest.getiP_Address());
				endPaymentRequest.setPaymentStatus("AUTHORIZED");
				endPaymentRequest.setLanguage(ctx.checkinInitPaymentRequest.getLanguage());
				endPaymentRequest.setMarket(ctx.checkinInitPaymentRequest.getMarket());
				endPaymentRequest.setConversationID(ctx.checkinInitPaymentResponse.getInitPaymentResponse().getConversationID());

				com.alitalia.aem.common.data.checkinrest.model.endpayment.request.PaymentDetail paymentDetail = new com.alitalia.aem.common.data.checkinrest.model.endpayment.request.PaymentDetail();
				paymentDetail.setCardCode(ctx.checkinInitPaymentRequest.getPaymentDetail().getCardCode());
				paymentDetail.setCardNumber(ctx.checkinInitPaymentRequest.getPaymentDetail().getCardNumber());
				paymentDetail.setExpireMonth(ctx.checkinInitPaymentRequest.getPaymentDetail().getExpireMonth());
				paymentDetail.setExpireYear(ctx.checkinInitPaymentRequest.getPaymentDetail().getExpireYear());
				paymentDetail.setCardSecurityCode(""); //non bisogna valorizzare cvc nella request di endPayment
				paymentDetail.setCardHolderFirstName(ctx.checkinInitPaymentRequest.getPaymentDetail().getCardHolderFirstName());
				paymentDetail.setCardHolderLastName(ctx.checkinInitPaymentRequest.getPaymentDetail().getCardHolderLastName());
				paymentDetail.setAmount(ctx.checkinInitPaymentRequest.getPaymentDetail().getAmount());
				paymentDetail.setCurrencyCode(ctx.checkinInitPaymentRequest.getPaymentDetail().getCurrencyCode());
				paymentDetail.setApprovalCode(approvalCode);
				//paymentDetail.setApprovalCode(ctx.checkinInitPaymentResponse.getInitPaymentResponse().getAuthorizationResult().getApprovalCode());
				paymentDetail.setTransactionID(ctx.checkinInitPaymentResponse.getInitPaymentResponse().getSabreTransactionID());
				paymentDetail.setApprovedURL(ctx.redirectUrl); 
				paymentDetail.setErrorURL(ctx.redirectUrl); 

				com.alitalia.aem.common.data.checkinrest.model.endpayment.request.AzAddressPayment azAddressPayment = new com.alitalia.aem.common.data.checkinrest.model.endpayment.request.AzAddressPayment();
				azAddressPayment.setAddress(ctx.checkinInitPaymentRequest.getPaymentDetail().getAzAddressPayment().getAddress());
				azAddressPayment.setPostalCode(ctx.checkinInitPaymentRequest.getPaymentDetail().getAzAddressPayment().getPostalCode());
				azAddressPayment.setCityName(ctx.checkinInitPaymentRequest.getPaymentDetail().getAzAddressPayment().getCityName());
				azAddressPayment.setCountryName(ctx.checkinInitPaymentRequest.getPaymentDetail().getAzAddressPayment().getCountryName());

				paymentDetail.setAzAddressPayment(azAddressPayment);
				endPaymentRequest.setPaymentDetail(paymentDetail);

				com.alitalia.aem.common.data.checkinrest.model.endpayment.request.InsurancePayment insurancePayment = new com.alitalia.aem.common.data.checkinrest.model.endpayment.request.InsurancePayment();
				insurancePayment.setBuyInsurance(ctx.checkinInitPaymentRequest.getInsurancePayment().getBuyInsurance());
				insurancePayment.setInsuranceAmount(ctx.checkinInitPaymentRequest.getInsurancePayment().getInsuranceAmount());
				insurancePayment.setPolicyNumber(ctx.checkinInitPaymentRequest.getInsurancePayment().getPolicyNumber());

				endPaymentRequest.setInsurancePayment(insurancePayment);

				List<FraudNetParam> fraudNet = new ArrayList<FraudNetParam>();
				for (com.alitalia.aem.common.data.checkinrest.model.initpayment.request.FraudNetParam fraudNetParam : ctx.listFraudNetParam) {
					FraudNetParam param = new FraudNetParam();
					param.setName(fraudNetParam.getName());
					param.setValue(fraudNetParam.getValue());
					fraudNet.add(param);
				}
				endPaymentRequest.setFraudNetParam(fraudNet);

				serviceResponse = paymentDelegateRest.endPayment(endPaymentRequest);
				//	non aggiorniamo il conversation id perche' uguale a quello della request

			}catch(Exception e){
				logger.error("[CheckinSession][endPayment] - Errore durante l'invocazione del servizio.", e);
			}
			return serviceResponse;
		}

	public CheckinAddPassengerResponse checkinAddPassenger(SlingHttpServletRequest request, CheckinSessionContext ctx) {
		CheckinAddPassengerResponse serviceResponce = null;
		try{

			CheckinAddPassengerRequest serviceRequest = new CheckinAddPassengerRequest(IDFactory.getTid(), IDFactory.getSid(request));
			serviceRequest.setPnr(ctx.pnr); // PNR volo principale
			serviceRequest.setTicket(request.getParameter("ticketNumber"));
			serviceRequest.setLastName(request.getParameter("lastName"));
			serviceRequest.setMarket(ctx.market);
			serviceRequest.setLanguage(ctx.language);
			serviceRequest.setConversationId(ctx.conversationId);

//			logger.info("[CheckinSession] Chiamata del servizio retrieveAddPassenger...");
			serviceResponce = checkInDelegateRest.retrieveCheckinAddPassenger(serviceRequest);
//			logger.info("[CheckinSession] Ritorno del servizio retrieveAddPassenger...");

			//Oggetto Data dopo la chiamata al servizio
			PnrListInfoByAddPassenger pnrInfoResult = serviceResponce.getPnrData();

			// Metto in sessione il nuovo conversationID
			if(pnrInfoResult != null && pnrInfoResult.getPnr() != null && pnrInfoResult.getPnr().size() > 0)
				if(pnrInfoResult.getConversationID() != null) {
					ctx.conversationId = pnrInfoResult.getConversationID();
				}

		}catch(Exception e){
			logger.error("[CheckinSession][checkinAddPassenger] - " + e.getMessage());

		}
		return serviceResponce;
	}


	public CheckinSetSmsDataResponse setMoreSmsData(SlingHttpServletRequest request, CheckinSessionContext ctx) {

		CheckinSetSmsDataRequest serviceRequest = new CheckinSetSmsDataRequest(IDFactory.getTid(), IDFactory.getSid(request));
		BoardingPassSms boradingPassSms = new BoardingPassSms();

		boradingPassSms.setAirline(ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().get(0).getAirline());
		boradingPassSms.setDepartureDate(ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().get(0).getOriginalDepartureDate().split("T")[0]);
		boradingPassSms.setFlight(ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().get(0).getFlight());
		boradingPassSms.setOrigin(ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().get(0).getOrigin().getCode());
		boradingPassSms.setDestination(ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().get(0).getDestination().getCode());
		boradingPassSms.setPnr(ctx.pnr);
		boradingPassSms.setPassengerID(ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().get(0).getPassengers().get(0).getPassengerID());
		boradingPassSms.setSurname(ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().get(0).getPassengers().get(0).getCognome());
		boradingPassSms.setSeats(Arrays.asList(request.getParameterValues("seats_sms[]")));

		serviceRequest.setMarket(ctx.market);
		serviceRequest.setLanguage(ctx.language);
		serviceRequest.setConversationID(ctx.conversationId);
		serviceRequest.setBoardingPassData(boradingPassSms);

		CheckinSetSmsDataResponse serviceResponce = checkInDelegateRest.setSmsData(serviceRequest);

		return serviceResponce;
	}


	public CheckinGetSmsDataResponse getMoreSmsData(SlingHttpServletRequest request) {

		CheckinGetSmsDataRequest serviceRequest = new CheckinGetSmsDataRequest(IDFactory.getTid(), IDFactory.getSid(request));

		serviceRequest.setKey(request.getParameter("key"));
		serviceRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
		serviceRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
		serviceRequest.setConversationID(IDFactory.getTid());

		CheckinGetSmsDataResponse serviceResponce = checkInDelegateRest.getSmsData(serviceRequest);

		return serviceResponce;
	}


	public CheckinSendSmsResponse sendSms(CheckinSendSmsRequest request){
		try {
			return checkInDelegate.sendSms(request);
		} catch (Exception e) {
			logger.error("Error sendSms: ", e);
			return null;
		}
	}

	public CheckinAuthorize3dResponse authorize3d(SlingHttpServletRequest request, CheckinSessionContext ctx, String ipAddress, String merchantData, String paymentResponse) {
    	logger.debug("MAPayment - authorize3D");

		CheckinAuthorize3dResponse serviceResponse = null;
		try {
			CheckinAuthorize3dRequest serviceRequest = new CheckinAuthorize3dRequest(IDFactory.getTid(), IDFactory.getSid(request));
			serviceRequest.setMarket(ctx.market);
			serviceRequest.setLanguage(ctx.language);
			serviceRequest.setConversationID(ctx.conversationId);
			serviceRequest.setCaller("WebCheckin");
			serviceRequest.setiP_Address(ipAddress);
			serviceRequest.setMerchantData(merchantData);
			serviceRequest.setPaymentResponse(paymentResponse);

			serviceResponse = paymentDelegateRest.authorize3d(serviceRequest);
		} catch (Exception e) {
			logger.error("MAPayment - authorize3d: Errore durante l'invocazione del servizio.", e);
		}
		return serviceResponse;
	}

	//Tolentino - Inizio
	public CheckinInitPaymentResponse getInitPaymentRecurringAdyen(SlingHttpServletRequest request, CheckinSessionContext ctx, String client, String ipAddress, String email, PaymentDetail paymentDetail) {
		logger.debug("MAPayment - getInitPaymentRecurringAdyen");
		//TODO controllare che i dati vengano eliminati dalla sessione all'inizio e alla fine del procedimento

		CheckinInitPaymentResponse serviceResponse = null;
		try{
			CheckinInitPaymentRequest serviceRequest = new CheckinInitPaymentRequest(IDFactory.getTid(), IDFactory.getSid(request));
			serviceRequest.setPnr(ctx.pnr);
			serviceRequest.setClient(client);
			serviceRequest.setiP_Address(ipAddress);
			serviceRequest.setEmail(email);
			serviceRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
			serviceRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
			serviceRequest.setConversationID(ctx.conversationId);
			serviceRequest.setPaymentStatus(""); //Per il pagamento nel Init stringa VUOTA. Nel metodo End dovrà essere valorizzato ad "AUTHORIZED".
			serviceRequest.setPaymentDetail(paymentDetail);

			//Inizio Gestione InsurancePayment
			InsurancePayment insurancePayment = new InsurancePayment();

			if(request.getParameter("insuranceAmount") != null && !request.getParameter("insuranceAmount").isEmpty() && Double.parseDouble(request.getParameter("insuranceAmount")) > 0){
				insurancePayment.setBuyInsurance(true);// di base a false valorizzato a true in caso di scelta da ancillary
				insurancePayment.setInsuranceAmount(request.getParameter("insuranceAmount"));//costo dell'assicurazione
				insurancePayment.setPolicyNumber("");//polizza assicurativa
			}
			else{
				insurancePayment.setBuyInsurance(false);// di base a false valorizzato a true in caso di scelta da ancillary
				insurancePayment.setInsuranceAmount("");//costo dell'assicurazione
				insurancePayment.setPolicyNumber("");//polizza assicurativa
			}

			serviceRequest.setInsurancePayment(insurancePayment);
			//Fine Gestione InsurancePayment

			//Inizio Gestione List<FraudNetParam> fraudNetParam
			serviceRequest.setFraudNetParam(ctx.listFraudNetParam);
			//Fine Gestione List<FraudNetParam> fraudNetParam

			ctx.checkinInitPaymentRequest = serviceRequest;

			logger.debug("MAPayment - getInitPaymentRecurringAdyen: Chiamata del servizio initPaymentRecurringAdyen...");
			serviceResponse = paymentDelegateRest.initPaymentRecurringAdyen(serviceRequest);
			logger.info("MAPayment - [CheckinSession] Ritorno del servizio initPaymentRecurringAdyen...");

			//Oggetto Data dopo la chiamata al servizio
			InitPaymentResponse initPaymentResponse = serviceResponse.getInitPaymentResponse();

			if(initPaymentResponse != null ){
				ctx.checkinInitPaymentResponse = serviceResponse;
			}

		}catch(Exception e){
			logger.error("MAPayment - getInitPaymentRecurringAdyen: Errore durante l'invocazione del servizio.", e);
		}
		return serviceResponse;
	}

	public void registerCreditCard(SlingHttpServletRequest request) {
		logger.info("MAPayment - registerCreditCard: Memorizzazione Carta di Credito in Adyen");
		try {
			MyAlitaliaSessionContext mactx = (MyAlitaliaSessionContext) request.getSession().getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);
			if (mactx != null) {
				Object obj = mactx.getMaCustomer();
				if (obj instanceof MACustomer && obj!=null) {
					MACustomer loggedMAUserProfile = (MACustomer) obj;
					String UID = loggedMAUserProfile.getUID();
					if (UID!=null && UID.trim().isEmpty()==false) {
						logger.info("MAPayment - registerCreditCard: recupero i dati della Carta di Credito dalla request");
//						MACreditCardData creditCardData = new MACreditCardData(request.getParameter("cardCode"), request.getParameter("cardNumber"), request.getParameter("cardSecurityCode"), request.getParameter("cardHolderFirstName"),
//								request.getParameter("cardHolderLastName"), request.getParameter("expireMonth"), request.getParameter("expireYear"), request.getParameter("cardBin"));

						MACreditCardData creditCardData = new MACreditCardData(request.getParameter("cardCode"));
						AdyenAdditionalData additionalData = new AdyenAdditionalData("card.encrypted.json",request.getParameter("adyen-encrypted-data"));

						AdyenStoringPaymentRequest storingPaymentRequest = new AdyenStoringPaymentRequest();
//						AdyenCardData card = new AdyenCardData(creditCardData.getCvv(), creditCardData.getExpireMonth(), creditCardData.getExpireYear(), creditCardData.getHolderName(), creditCardData.getNumber(), creditCardData.getType());
//						storingPaymentRequest.setCard(card);
						storingPaymentRequest.setType(creditCardData.getType());
						storingPaymentRequest.setAdditionalData(additionalData);

						if (creditCardData.getCircuitCode()!=null && creditCardData.getCircuitCode().trim().isEmpty()==false && creditCardData.getCircuitCode().equals("americanexpress")) {
							creditCardData.fillAddressData(request.getParameter("postalCode"), request.getParameter("cityName"), request.getParameter("countryName"), request.getParameter("address"));
							AdyenAddressData address = new AdyenAddressData(creditCardData.getCity(), creditCardData.getCountry(), creditCardData.getZip(), creditCardData.getState(), creditCardData.getAddress());
							storingPaymentRequest.setAddress(address);
						}

						storingPaymentRequest.setShopperEmail(request.getParameter("email"));
						storingPaymentRequest.setShopperReference(UID);
						storingPaymentRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
						storingPaymentRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
						storingPaymentRequest.setConversationID(IDFactory.getTid());
						storingPaymentRequest.setiP_Address(AlitaliaUtils.getRequestRemoteAddr(configuration, request));

						logger.info("MAPayment - registerCreditCard: invocando adyenRecurringPaymentDelegate.storingPayment...");
						AdyenStoringPaymentResponse storingPaymentResponse = adyenRecurringPaymentDelegate.storingPayment(storingPaymentRequest);
						if (storingPaymentResponse!=null) {
							if (storingPaymentResponse.getResultCode()!=null && storingPaymentResponse.getResultCode().equalsIgnoreCase("Authorised")) {
								logger.debug("MAPayment - registerCreditCard: Carta di credito [{}] memorizzata", creditCardData.print());
							} else {
								logger.error("MAPayment - registerCreditCard: Errore durante la memorizzazione della carta di credito in Adyen, resultCode [{}]", storingPaymentResponse.getResultCode());
							}
						} else {
							logger.error("MAPayment - registerCreditCard: Errore durante la memorizzazione della carta di credito in Adyen, response is null");
						}
					} else {
						logger.error("MAPayment - registerCreditCard: UID user MyAlitalia non presente");
					}
				} else {
					logger.error("MAPayment - registerCreditCard: User MyAlitalia non trovato nel MyAlitaliaSessionContext");
				}
			} else {
				logger.error("MAPayment - registerCreditCard: MyAlitaliaSessionContext non trovto in sessione");
			}
		} catch (Exception e) {
			logger.error("MAPayment - registerCreditCard: Errore durante la memorizzazione della carta di credito in Adyen", e);
		}
		logger.info("MAPayment - registerCreditCard: Carta di Credito memorizzata in Adyen");
	}

	public void reloadCreditCard(SlingHttpServletRequest request) {
		logger.info("MAPayment - reloadCreditCard: Recupera Carte di Credito in Adyen");
		try {
			MyAlitaliaSessionContext mactx = (MyAlitaliaSessionContext) request.getSession().getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);
			if (mactx != null) {
				Object obj = mactx.getMaCustomer();
				if (obj instanceof MACustomer && obj!=null) {
					MACustomer loggedMAUserProfile = (MACustomer) obj;
					setCreditCardsData(loggedMAUserProfile, null);
					String UID = loggedMAUserProfile.getUID();
					if (UID!=null && UID.trim().isEmpty()==false) {
						ArrayList<MACreditCardData> maCreditCardsData = null;

						AdyenRetrievalStoredPaymentRequest adyenRetrievalStoredPaymentRequest = new AdyenRetrievalStoredPaymentRequest();
						adyenRetrievalStoredPaymentRequest.setShopperReference(UID);
						adyenRetrievalStoredPaymentRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
						adyenRetrievalStoredPaymentRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
						adyenRetrievalStoredPaymentRequest.setConversationID(IDFactory.getTid());

						logger.info("MAPayment - reloadCreditCard: invocando adyenRecurringPaymentDelegate.retrievalStoredPayment...");
						AdyenRetrievalStoredPaymentResponse adyenRetrievalStoredPaymentResponse = adyenRecurringPaymentDelegate.retrievalStoredPayment(adyenRetrievalStoredPaymentRequest);
						if (adyenRetrievalStoredPaymentResponse!=null && adyenRetrievalStoredPaymentResponse.getAdyenRetrievalStoredPaymentList()!=null &&
							adyenRetrievalStoredPaymentResponse.getAdyenRetrievalStoredPaymentList().isEmpty()==false) {
							for (AdyenRetrievalStoredPaymentData adyenRetrievalStoredPayment: adyenRetrievalStoredPaymentResponse.getAdyenRetrievalStoredPaymentList()) {
								if (adyenRetrievalStoredPayment!=null && adyenRetrievalStoredPayment.getRecurringDetailList()!=null && adyenRetrievalStoredPayment.getRecurringDetailList().isEmpty()==false) {
									for (AdyenRecurringDetailData recurringDetail: adyenRetrievalStoredPayment.getRecurringDetailList()) {
										if (recurringDetail!=null && recurringDetail.getCard()!=null) {
											MACreditCardData maCreditCardData = new MACreditCardData(recurringDetail.getCard().getType(), recurringDetail.getPaymentMethodVariant(), recurringDetail.getCard().getNumber(),
													recurringDetail.getCard().getCvc(), recurringDetail.getCard().getHolderName(), recurringDetail.getCard().getExpiryMonth().length() < 2 ? "0" + recurringDetail.getCard().getExpiryMonth() : recurringDetail.getCard().getExpiryMonth(), recurringDetail.getCard().getExpiryYear(),
													recurringDetail.getAlias(), recurringDetail.getFirstPspReference(), recurringDetail.getRecurringDetailReference(), recurringDetail.getAdditionalDataList().get(0).getCardBin());

											if (recurringDetail.getAddress()!=null) {
												maCreditCardData.fillAddressData(recurringDetail.getAddress().getPostalCode(), recurringDetail.getAddress().getCity(), recurringDetail.getAddress().getCountry(), recurringDetail.getAddress().getStreet(),
														recurringDetail.getAddress().getProvince());
											}

											if (maCreditCardsData==null) maCreditCardsData = new ArrayList<MACreditCardData>();
											maCreditCardsData.add(maCreditCardData);

											logger.debug("MAPayment - reloadCreditCard: Recuperata carta di credito [{}]", maCreditCardData.print());
										}
									}
								}
							}
						}

						if (maCreditCardsData!=null) setCreditCardsData(loggedMAUserProfile, maCreditCardsData);
					} else {
						logger.error("MAPayment - reloadCreditCard: UID user MyAlitalia non presente");
					}
				} else {
					logger.error("MAPayment - reloadCreditCard: User MyAlitalia non trovato nel MyAlitaliaSessionContext");
				}
			} else {
				logger.error("MAPayment - reloadCreditCard: MyAlitaliaSessionContext non trovto in sessione");
			}
		} catch (Exception e) {
			logger.error("MAPayment - reloadCreditCard: Errore durante il recupero delle carte di credito in Adyen", e);
		}
		logger.info("MAPayment - reloadCreditCard: Carte di Credito recuperate");
	}

	private void setCreditCardsData(MACustomer userMA, ArrayList<MACreditCardData> maCreditCardsData) {
		if (userMA!=null && userMA.getData()!=null) {
			if (userMA.getData().getPayment()==null) userMA.getData().setPayment(new MACustomerDataPayment());
			userMA.getData().getPayment().setCreditCardsData(maCreditCardsData);
		}
	}
	//Tolentino - Fine

}