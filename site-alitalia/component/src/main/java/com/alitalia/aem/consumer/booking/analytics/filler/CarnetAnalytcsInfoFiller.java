package com.alitalia.aem.consumer.booking.analytics.filler;

import java.util.Calendar;
import java.util.TimeZone;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.analytics.AnalyticsInfo;
public class CarnetAnalytcsInfoFiller implements AnalyticsInfoFiller {
	
	private static final float FROM_MILLIS_TO_DAYS = 1000 * 60 * 60 * 24;
	private Calendar now;
	
	private void initFormats() {
		now = Calendar.getInstance();
		now.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
		
	}
	@Override
	public AnalyticsInfo fillInfo(BookingSessionContext ctx, int step, AlitaliaConfigurationHolder configurationHolder) {
		AnalyticsInfo info = new AnalyticsInfo();
		info.step=step;
		/*Fanplayr info*/
		if(step>0 &&step<6){
		
			info.carnetUseStep=step;
			if (ctx.searchElements != null && !ctx.searchElements.isEmpty()) {
				info.carnetTratta = ctx.searchElements.get(0).getFrom().getCityCode()+"-"+ctx.searchElements.get(0).getTo().getCityCode();		
			}
			info.carnetItinType=computeTravelType(ctx.searchKind);
		}
		
		
	return info;
		// TODO Auto-generated method stub
		
	}
	
	private String computeTravelType(BookingSearchKindEnum searchKind) {
		String travelType = "";
		if(searchKind != null){
			switch(searchKind){
			case SIMPLE:
				travelType = "OneWay";
				break;
			case MULTILEG:
				travelType = "MultiLeg";
				break;
			case ROUNDTRIP:
				travelType = "RoundTrip";
				break;
			}
		}
		
		return travelType;
	}
}
