package com.alitalia.aem.consumer.booking.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.alitalia.aem.common.data.home.FlightData;

public class FlightSelection {
	
	private FlightData flightData;
	
	private String selectedBrandCode;

	private Map<String, String> fareRules;
	
	private List<Integer> flightsMileage;
	
	private List<String> carriers; // as list of unique carriers

	private boolean isTariffaLight;
	
	private boolean bus;

	private int indexFlight;
	
	private BigDecimal awardPrice;
	
	private boolean onlyHandBaggage;
	
	private String solutionId;

	public FlightSelection(FlightData flightData, String selectedBrandCode, List<Integer> flightsMileage,
			Map<String, String> fareRules, List<String> carriers,
			boolean isTariffaLight, int solutionFlightIndex, String solutionId) {
		super();
		this.flightData = flightData;
		this.selectedBrandCode = selectedBrandCode;
		this.flightsMileage = flightsMileage;
		this.fareRules = fareRules;
		this.carriers = carriers;
		this.isTariffaLight = isTariffaLight;
		this.indexFlight = solutionFlightIndex;
		this.solutionId = solutionId;
	}
	
	public FlightSelection(FlightData flightData, String selectedBrandCode, List<Integer> flightsMileage,
			Map<String, String> fareRules, List<String> carriers,
			boolean isTariffaLight, int solutionFlightIndex, BigDecimal awardPrice) {
		super();
		this.flightData = flightData;
		this.selectedBrandCode = selectedBrandCode;
		this.flightsMileage = flightsMileage;
		this.fareRules = fareRules;
		this.carriers = carriers;
		this.isTariffaLight = isTariffaLight;
		this.indexFlight = solutionFlightIndex;
		this.awardPrice = awardPrice;
	}
	
	public FlightData getFlightData() {
		return flightData;
	}
	
	public void setFlightData(FlightData flightData) {
		this.flightData = flightData;
	}

	public String getSelectedBrandCode() {
		return selectedBrandCode;
	}
	
	public List<Integer> getFlightsMileage() {
		return flightsMileage;
	}
	
	public void setFlightsMileage(List<Integer> flightsMileage) {
		this.flightsMileage = flightsMileage;
	}
	
	public Map<String, String> getFareRules() {
		return fareRules;
	}
	
	public void setFareRules(Map<String, String> fareRules) {
		this.fareRules = fareRules;
	}

	public List<String> getCarriers() {
		return carriers;
	}

	public boolean isTariffaLight() {
		return isTariffaLight;
	}

	public void setTariffaLight(boolean isTariffaLight) {
		this.isTariffaLight = isTariffaLight;
	}
	
	public boolean isBus() {
		return bus;
	}

	public void setBus(boolean bus) {
		this.bus = bus;
	}

	public int getIndexFlight() {
		return indexFlight;
	}

	public BigDecimal getAwardPrice() {
		return awardPrice;
	}

	public void setAwardPrice(BigDecimal awardPrice) {
		this.awardPrice = awardPrice;
	}

	public String getSolutionId() {
		return solutionId;
	}

	public void setSolutionId(String solutionId) {
		this.solutionId = solutionId;
	}
	
	public boolean isOnlyHandBaggage() {
		return onlyHandBaggage;
	}

	public void setOnlyHandBaggage(boolean onlyHandBaggage) {
		this.onlyHandBaggage = onlyHandBaggage;
	}
	
}
