package com.alitalia.aem.consumer.booking.model.extrabaggage;

public class DailyPrice
{
	public DailyPrice()
	{

	}

	private java.time.LocalDateTime DepartureDate = java.time.LocalDateTime.MIN;
	public final java.time.LocalDateTime getDepartureDate()
	{
		return DepartureDate;
	}
	public final void setDepartureDate(java.time.LocalDateTime value)
	{
		DepartureDate = value;
	}
	private java.time.LocalDateTime ReturnDate = java.time.LocalDateTime.MIN;
	public final java.time.LocalDateTime getReturnDate()
	{
		return ReturnDate;
	}
	public final void setReturnDate(java.time.LocalDateTime value)
	{
		ReturnDate = value;
	}
	private int Price;
	public final int getPrice()
	{
		return Price;
	}
	public final void setPrice(int value)
	{
		Price = value;
	}
}