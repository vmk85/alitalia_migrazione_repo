package com.alitalia.aem.consumer.checkin.model;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinBase extends GenericBaseModel{
	
		@Self
		private SlingHttpServletRequest slingHttpServletRequest;
	    private boolean isLog=false;
	    
	    private String fullMarketPath;
	    
	    private String errorMessage;

		@PostConstruct
		protected void initModel() {
			logger.debug("[EmptyModel] initModel");
			super.initBaseModel(slingHttpServletRequest);
			if(AlitaliaUtils.getAuthenticatedUser(slingHttpServletRequest)!=null){
		        isLog=true;
		      }else{
		        isLog=false;
		      }
			String faqPage = "/faq/jcr:content";
			fullMarketPath = AlitaliaUtils.findSiteBaseExternalUrl(
					slingHttpServletRequest.getResource(), false)+AlitaliaConstants.BASE_CONFIG_PATH+faqPage ;
			
			errorMessage = null;
			if ("false".equals(request.getParameter("success"))) {
				String reason = request.getParameter("reason");
				if (reason != null) {
					String message = i18n.get("checkin.common." + reason + ".error");
					if (!message.equals("checkin.common." + reason + ".error")) {
						errorMessage = message;
					} else {
						errorMessage = i18n.get("checkin.common.searchError.error");
					}
				} else {
					errorMessage = i18n.get("checkin.common.searchError.error");
				}
			}
		}
	   public boolean getIsLog(){
		      return isLog;
		   }
	public String getFullMarketPath() {
		return fullMarketPath;
	}
	public void setFullMarketPath(String fullMarketPath) {
		this.fullMarketPath = fullMarketPath;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
}
