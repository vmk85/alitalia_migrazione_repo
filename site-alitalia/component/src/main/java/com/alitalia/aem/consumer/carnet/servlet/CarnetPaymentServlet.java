package com.alitalia.aem.consumer.carnet.servlet;

import java.math.BigDecimal;
import java.text.DecimalFormatSymbols;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.DictionaryItemData;
import com.alitalia.aem.common.data.home.StateData;
import com.alitalia.aem.common.data.home.carnet.CarnetCreditCardProviderData;
import com.alitalia.aem.common.data.home.carnet.CarnetPaymentTypeItemData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.carnet.CarnetConstants;
import com.alitalia.aem.consumer.carnet.CarnetSession;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.alitalia.aem.consumer.carnet.CarnetUtils;
import com.alitalia.aem.consumer.carnet.render.CarnetGenericPriceRender;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "carnetpaymentconsumer" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CarnetPaymentServlet extends GenericCarnetFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile CarnetSession carnetSession;	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		CarnetSessionContext ctx = getCarnetSessionContext(request);
		
		String tipologiaCarta = request.getParameter("tipologiaCarta");
		
		// type of payment
		String[] countries = null;
		String[] countriesUSAValues = null;

		//prepare countries list for validation
		if (ctx.countries != null) {
			countries = new String[ctx.countries.size()];
			int i = 0;
			for(CountryData country : ctx.countries) {
				countries[i] = country.getCode();
				i++;
			}
		}
		
		//prepare countries list for validation
		if (ctx.countriesUSA != null) {
			countriesUSAValues = new String[ctx.countriesUSA.size()];
			int i = 0;
			for(StateData country : ctx.countriesUSA) {
				countriesUSAValues[i] = country.getStateCode();
				i++;
			}
		}
		
		//cerco il maxCvvLength con cui validare il cvv per la carta di credito selezionata
		Integer maxCvvLength = 0;
		for (CarnetPaymentTypeItemData creditCard : ctx.creditCards) {
			if (tipologiaCarta.equals(creditCard.getCode())) {
				for (DictionaryItemData data : creditCard.getOtherInfo()) {
					if (data.getKey().equals("MaxCVC")) {
						maxCvvLength = (Integer) data.getValue();
						break;
					}
				}
				break;
			}
		}
		
		Validator validator = new Validator();
		
		// recupero dati dalla request
		String numeroCarta = request.getParameter("numeroCarta");
		String meseScadenzaCarta = request.getParameter("meseScadenza");
		String annoScadenzaCarta = request.getParameter("annoScadenza");
		String cvc = request.getParameter("cvc");
		String nomeTitolare = request.getParameter("nome");
		String cognomeTitolare = request.getParameter("cognome");
		
		String indirizzo = request.getParameter("indirizzo");
		String cap = request.getParameter("cap");
		String citta = request.getParameter("citta");
		String paese = request.getParameter("paese");
		String countriesUSA = request.getParameter("countriesUSA");
		
		
		//validazione
		validator.addDirectCondition("numeroCarta", numeroCarta, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("meseScadenza", meseScadenzaCarta, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("meseScadenza", meseScadenzaCarta, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isMonth");
		
		validator.addDirectCondition("annoScadenza" , annoScadenzaCarta, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("annoScadenza", annoScadenzaCarta, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isYear");
		
		validator.addDirectCondition("cvc", cvc, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("cvc", cvc, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, 
				maxCvvLength == 4 ? "isCVCFourDigits" : "isCVCThreeDigits");
		
		validator.addDirectCondition("nome", nomeTitolare, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("nome", nomeTitolare, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
		
		validator.addDirectCondition("cognome", cognomeTitolare, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("cognome", cognomeTitolare, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
		
		// check extra fields for American Express
		if (!tipologiaCarta.equals("") && tipologiaCarta != null &&
				tipologiaCarta.equals("AX")) {

			validator.addDirectCondition("indirizzo", indirizzo,
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("cap", cap,
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("cap", cap,
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNumber");
			validator.addDirectCondition("citta", citta,
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("paese", paese,
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.setAllowedValues("paese", paese,
					countries, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD);
			//Controllo campi extra di AMEX solo se il paese è US
			if (paese.equalsIgnoreCase("us")) {
				validator.addDirectCondition("countriesUSA", countriesUSA,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.setAllowedValues("countriesUSA", countriesUSA,
						countriesUSAValues, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD);
			}
		}
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		CarnetSessionContext ctx = getCarnetSessionContext(request); 
		
		// recupero dati dalla request
		String numeroCarta = request.getParameter("numeroCarta");
		Short meseScadenzaCarta = Short.parseShort(request.getParameter("meseScadenza"));
		Short annoScadenzaCarta = Short.parseShort(request.getParameter("annoScadenza"));
		String cvc = request.getParameter("cvc");
		String nomeTitolare = request.getParameter("nome");
		String cognomeTitolare = request.getParameter("cognome");
		//Extra fields AMEX
		String indirizzo = request.getParameter("indirizzo");
		String cap = request.getParameter("cap");
		String citta = request.getParameter("citta");
		String paese = request.getParameter("paese");
		String countriesUSA = request.getParameter("countriesUSA");
		String tipologiaCarta = request.getParameter("tipologiaCarta");
		String creditCardTypeName = "";
		String state = null;
		if (paese.equalsIgnoreCase("us")) {
			state = countriesUSA;
		}
		
		for (CarnetPaymentTypeItemData creditCard : ctx.creditCards) {
			if (tipologiaCarta.equals(creditCard.getCode())) {
				creditCardTypeName = creditCard.getDescription();
				break;
			}
		}
		creditCardTypeName = creditCardTypeName.replaceAll(" ", "").toLowerCase();
		
		String redirectUrl = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCarnetConfirmationPage();
		redirectUrl = request.getResourceResolver().map(redirectUrl);
		
		String returnUrl = ctx.domain + redirectUrl;
		String errorUrl = ctx.domain + AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) + 
				getConfiguration().getCarnetPaymentPage() + "?error=true";

		boolean paymentPerformed = false;

		paymentPerformed = carnetSession.performPaymentCarnet(ctx, numeroCarta, creditCardTypeName, cvc, meseScadenzaCarta, 
				annoScadenzaCarta, nomeTitolare, cognomeTitolare, indirizzo,
				cap, citta, paese, state, request.getHeader("User-Agent"), errorUrl, returnUrl);
		
		if(paymentPerformed){
			String[] values = getValuesToReplace(ctx, request);
			boolean mailSent = carnetSession.sendEmail(ctx, request.getResource(), 
					getConfiguration().getCarnetMailRicevutaAcquistoCarnetPage(),
					CarnetConstants.PLACEHOLDER_RICEVUTA_ACQUISTO_CARNET, values);
			ctx.hasBeenMailSent = mailSent;
		}
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		
		if(!paymentPerformed){
			json.key("isError").value("true");
		} else {
			json.key("result").value("OK");
			json.key("redirect").value(redirectUrl);
		}
		
		json.endObject();
		
		
	}
	
	private String[] getValuesToReplace(CarnetSessionContext ctx, SlingHttpServletRequest request) {
		String[] values = new String[13];
		values[0] = ctx.infoCarnet.getCarnetCode();
		values[1] = ctx.customerInfo.getName() + " " + ctx.customerInfo.getLastName();
		values[2] = ctx.customerInfo.getMobileNumber();
		values[3] = ctx.infoCarnet.getPassword();
		values[4] = Short.toString(ctx.infoCarnet.getTotalRoutes());
		
		BigDecimal price = ctx.selectedCarnet.getPrice();
		CarnetGenericPriceRender  priceRender = new CarnetGenericPriceRender(ctx);
		values[5] = ctx.currencySymbol + " " + priceRender.getFormattedPrice(price);
				
		values[6] = CarnetUtils.computeCreditCardName(((CarnetCreditCardProviderData)ctx.paymentData.getProvider()).getType());
		values[7] = ((CarnetCreditCardProviderData)ctx.paymentData.getProvider()).getCreditCardNumber();
		
		DecimalFormatSymbols dfs = new DecimalFormatSymbols(ctx.locale);
		int decimalSeparatorIndex = values[5].lastIndexOf(dfs.getDecimalSeparator());
		if (decimalSeparatorIndex > 0) {
			values[8] = values[5].substring(0, decimalSeparatorIndex + 1);
			values[9] = values[5].substring(decimalSeparatorIndex + 1);
		} else {
			values[8] = values[5];
			values[9] = "";
		}
		values[10] = getBaseCCImagePath() + CarnetUtils.computeCreditCardImageName(((CarnetCreditCardProviderData)ctx.paymentData.getProvider()).getType());	
		values[11] = ctx.infoCarnet.getEmdCode();
		
		String protocol = "http://";
    	if(configuration.getHttpsEnabled()){
    		protocol = "https://";
    	}
    	String domain = getConfiguration().getExternalDomain();
		String linkEmettiBiglietti = protocol + domain + AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) + 
				getConfiguration().getCarnetLoginPage() + "?codiceCarnet=" + ctx.infoCarnet.getCarnetCode();
		values[12] = "<a href='" + linkEmettiBiglietti + "' style='text-align: center; text-transform: uppercase; font-weight: normal; text-decoration: none; color: white; white-space: nowrap; padding-top: 5px;padding-bottom: 5px;padding-left: 10px;padding-right: 10px; display: block;'>" 
				+ ctx.i18n.get("carnet.riepilogoAcquisto.emettiBiglietti.label") + "</a>";
		
		return values;
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
	
	private String getBaseCCImagePath() {
		String prefix = "http://";
		if (configuration.getHttpsEnabled())
			prefix = "https://";
		String hostName = prefix + configuration.getExternalDomain();
		return hostName + configuration.getStaticImagesPath() + "loghi/carte/";
	}

	
}

