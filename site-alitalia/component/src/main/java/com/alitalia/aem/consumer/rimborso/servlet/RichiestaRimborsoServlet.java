package com.alitalia.aem.consumer.rimborso.servlet;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Segment;
import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;
import com.alitalia.aem.common.messages.home.SendReminderCheckinRequest;
import com.alitalia.aem.common.messages.home.SendReminderCheckinResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.rimborso.model.RimborsoConstans;
import com.alitalia.aem.consumer.rimborso.model.RimborsoSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.webcheckin.delegate.WebCheckinDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.wcm.api.WCMMode;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.SlingRequestProcessor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

import com.day.cq.i18n.I18n;

import static aQute.lib.osgi.Constants.COMPONENT_NAME;

@Component
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = {"richiesta-rimborso-submit"}),
        @Property(name = "sling.servlet.methods", value = {"POST"}),
        @Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"})})
@SuppressWarnings("serial")
public class RichiestaRimborsoServlet extends GenericCheckinFormValidatorServlet {

    private static final int DIM_FORM = 17;
    private static final String KEYVALUE_SEPARATOR = ":";
    private static final String ELEMENT_SEPARATOR = System.getProperty("line.separator");

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile WebCheckinDelegate checkInDelegate;

    @Reference
    private volatile AlitaliaConfigurationHolder configuration;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private RequestResponseFactory requestResponseFactory;

    @Reference
    private SlingRequestProcessor requestProcessor;

    @Reference
    private CheckinSession checkinSession;

    private Locale locale;

    private I18n i18n;

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request)
            throws IOException {
        logger.debug("[InvoiceServlet] validateForm");
        i18n = new I18n(request.getResourceBundle(AlitaliaUtils.findResourceLocale(request.getResource())));

        try {

            ResultValidation resultValidation = null;

            boolean validateSuccess =  false;

            // validate parameters!
            Validator validator = setValidatorParameters(request, new Validator());

            /** Validazione PNR */
//            if (request.getSession().getAttribute("richiesta-rimborso-pnr") != null && request.getSession().getAttribute("richiesta-rimborso-pnr").equals(request.getParameter("pnr"))){
//
//                validateSuccess = true;
//
//            } else if (validator.validate().getResult()){
//
//                validateSuccess = validateRequestPnr(request);
//
//                if (!validateSuccess){
//                    resultValidation = new ResultValidation();
//                    resultValidation.setResult(false);
//                    Map<String, String> fields = new LinkedHashMap<String, String>();
//                    fields.put("pnr",i18n.get("richiesta.invalid.pnr"));
//                    fields.put("name",i18n.get("richiesta.invalid.name"));
//                    fields.put("cognome",i18n.get("richiesta.invalid.cognome"));
//                    resultValidation.setFields(new LinkedHashMap<String, String>());
//                    resultValidation.setFields(fields);
//                }
//
//            }

            return resultValidation == null ? validator.validate() : resultValidation;

        }

        catch (Exception e) {
            logger.error("Error during parameters validation", e);
            return null;
        }

    }

    /** Funzione per validazione PNR */
//    private boolean validateRequestPnr(SlingHttpServletRequest request) throws Exception {
//
//        boolean success = false;
//
//        boolean resultSession = initSession(request, configuration, checkinSession);
//
//        /** Controlllo se è andato tutto bene */
//        if (!resultSession) {
//            logger.error("Errore durante l'inizializzazione della sessione.");
//            throw new Exception("Errore durante l'inizializzazione della sessione.");
//        }
//
//        /** Estraggo il contesto dalla sessione */
//        CheckinSessionContext ctx = getCheckInSessionContext(request);
//        String pnr = request.getParameter("pnr");
//        String name = request.getParameter("name");
//        String cognome = request.getParameter("cognome");
//
//        ctx.passengerName = name;
//        ctx.passengerSurname = cognome;
//
//        logger.debug("[CheckinPnrSearchServlet - performSubmit] recupero dati dal contesto.");
//
//        try {
//
//            /** Controllo che sia un pnr da 6 */
//            if (pnr.length() == 6) {
//
//                /** Efettuo la chiamata */
//                logger.info("[CheckinPnrSearchServlet] invoco il checkinSession.checkInPnrSearch");
//                CheckinPnrSearchResponse result = checkinSession.checkInPnrSearch(request, ctx);
//                logger.info("[CheckinPnrSearchServlet] result=" + result);
//
//                /** Controllo la risposta del servizio */
//                if (result != null && result.getPnrData() != null) {
//                    /** Se non ci sono errori controllo la validitàdel volo associato al pnr */
//
//                    for (Pnr pnrList : result.getPnrData().getPnr()) {
//
//                        if (pnrList.getNumber().toUpperCase().equals(pnr.toUpperCase())) {
//                                success = true;
//                                break;
//                        }
//                    }
//
//                } else {
//                    logger.error("[CheckinPnrSearchServlet] - Errore durante l'invocazione del servizio.");
//                }
//            }
//            /** Controllo che sia un pnr da 13 */
//            if (pnr.length() == 13) {
//
//                /** Efettuo la chiamata */
//                logger.info("[CheckinPnrSearchServlet] invoco il checkinSession.checkinTicketSearch");
//                CheckinTicketSearchResponse result = checkinSession.checkinTicketSearch(request, ctx);
//                logger.info("[CheckinPnrSearchServlet] result=" + result);
//
//                /** Controllo la risposta del servizio */
//                if (result != null && result.getPnrData() != null) {
//                    /** Se non ci sono errori controllo la validitàdel volo associato al pnr */
//
//                    for (Pnr pnrList : result.getPnrData().getPnr()) {
//
//                        if (pnrList.getNumber().toUpperCase().equals(pnr.toUpperCase())) {
//                            success = true;
//                            break;
//                        }
//                    }
//
//                } else {
//                    logger.error("[CheckinPnrSearchServlet] - Errore durante l'invocazione del servizio.");
//                }
//            }
//
//        } catch (Exception e) {
//            throw new RuntimeException("[CheckinPnrSearchServlet] - Errore durante l'invocazione del servizio.", e);
//        }
//
//        if (success){
//            request.getSession().setAttribute("richiesta-rimborso-pnr",pnr);
//        }
//
//        return success;
//    }

    @Override
    protected void performSubmit(SlingHttpServletRequest request,
                                 SlingHttpServletResponse response) throws Exception {

        String pnr = request.getParameter("pnr");
        String name = request.getParameter("name");
        String cognome = request.getParameter("cognome");
        String numeroBiglietto = request.getParameter("numeroBiglietto");
        String motivo = request.getParameter("motivo");
        String tipo = request.getParameter("tipo");
        String checkAgreement = request.getParameter("checkAgreement");

        RimborsoSessionContext rimborsoSessionContext = new RimborsoSessionContext(name, cognome, pnr, numeroBiglietto, motivo, tipo);

        logger.debug("[InvoiceServlet] performSubmit");
        boolean mailResult = sendMail(request, rimborsoSessionContext);
        request.getSession().removeAttribute("richiesta-rimborso-pnr");
        logger.debug("Mail Sended = " + mailResult);

        TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
        response.setContentType("application/json");
        json.object();
        json.key("isError").value(!mailResult);
        json.endObject();
    }

    @Override
    protected void saveDataIntoSession(SlingHttpServletRequest request,
                                       Exception e) {

    }

    @Override
    protected AlitaliaConfigurationHolder getConfiguration() {
        return this.configuration;
    }

    /**
     * Set Validator parameters for validation
     *
     * @param request
     * @param validator
     */
    private Validator setValidatorParameters(SlingHttpServletRequest request,
                                             Validator validator) {
        logger.debug("[ InvoiceServlet] setValidatorParameters");

        String name = request.getParameter("name");
        String cognome = request.getParameter("cognome");
        String numeroBiglietto = request.getParameter("numeroBiglietto");
        String pnr = request.getParameter("pnr");
        String motivo = request.getParameter("motivo");
        String tipo = request.getParameter("tipo");
        String checkAgreement = request.getParameter("checkAgreement");

        // type
        validator.addDirectConditionMessagePattern("tipo", tipo,
                I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
                I18nKeySpecialPage.INVOICE_TYPE_ERROR_EMPTY, "isNotEmpty");
        validator.setAllowedValuesMessagePattern("tipo", tipo,
                new String[]{"totale", "parziale"}, I18nKeyCommon.MESSAGE_INVALID_FIELD,
                I18nKeySpecialPage.INVOICE_TYPE_ERROR_NOT_VALID);

        validator.addDirectConditionMessagePattern("motivo", motivo,
                I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
                I18nKeySpecialPage.INVOICE_TYPE_ERROR_EMPTY, "isNotEmpty");
        validator.setAllowedValuesMessagePattern("motivo", motivo,
                new String[]{"rimborsoVolontario", "rimborsoMalattia", "rimborsoRitardo"}, I18nKeyCommon.MESSAGE_INVALID_FIELD,
                I18nKeySpecialPage.INVOICE_TYPE_ERROR_NOT_VALID);

        // name
        validator.addDirectConditionMessagePattern("name", name,
                I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
                I18nKeySpecialPage.INVOICE_NAME_ERROR_EMPTY, "isNotEmpty");
        validator.addDirectConditionMessagePattern("name", name,
                I18nKeyCommon.MESSAGE_INVALID_FIELD,
                I18nKeySpecialPage.INVOICE_NAME_ERROR_NOT_VALID,
                "isAlphabeticWithAccentAndSpaces");

        // surname
        validator.addDirectConditionMessagePattern("cognome", cognome,
                I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
                I18nKeySpecialPage.INVOICE_SURNAME_ERROR_EMPTY, "isNotEmpty");
        validator.addDirectConditionMessagePattern("cognome", cognome,
                I18nKeyCommon.MESSAGE_INVALID_FIELD,
                I18nKeySpecialPage.INVOICE_SURNAME_ERROR_NOT_VALID,
                "isAlphabeticWithAccentAndSpaces");

        // ticket code
        validator.addDirectConditionMessagePattern("numeroBiglietto", numeroBiglietto,
                I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
                I18nKeySpecialPage.INVOICE_CODE_ERROR_EMPTY, "isNotEmpty");

        // pnr
        validator.addDirectConditionMessagePattern("pnr", pnr,
                I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
                I18nKeySpecialPage.INVOICE_ADDRESS_ERROR_EMPTY, "isNotEmpty");

        // checkbox accettazione privacy
        validator.addDirectConditionMessagePattern("checkAgreement", checkAgreement,
                I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
                I18nKeySpecialPage.INVOICE_TYPE_ERROR_EMPTY, "isNotEmpty");

        return validator;

    }

    private boolean sendMail(SlingHttpServletRequest request, RimborsoSessionContext rimborsoSessionContext) {

        SendReminderCheckinRequest sendReminderCheckinRequest = setRequestMail("backoffice.alitalia@abramo.com", "", configuration.MAIL_RICHIESTA_RIMBORSO, request, rimborsoSessionContext);
        SendReminderCheckinResponse sendReminderCheckinResponse =
                checkInDelegate.sendReminder(sendReminderCheckinRequest);

        return new Boolean(true);
    }

    private SendReminderCheckinRequest setRequestMail(String mails, String mailUtente, String templateMail, SlingHttpServletRequest request, RimborsoSessionContext rimborsoSessionContext) {

        SendReminderCheckinRequest sendReminderCheckinRequest = new SendReminderCheckinRequest();
        sendReminderCheckinRequest.setAttachment(null);
        sendReminderCheckinRequest.setAttachmentName(null);
        sendReminderCheckinRequest.setEmailBody(getMailBody(
                request.getResource(), templateMail,
                true, resolverFactory, requestResponseFactory,
                requestProcessor, rimborsoSessionContext));
        sendReminderCheckinRequest.setEmailSubject(i18n.get("richiediRimborso.subject"));
        List<String> mailsList = new ArrayList<>();
        String[] mailsArr = mails.split(";");
        for (int i = 0; i < mailsArr.length; i++) {
            mailsList.add(mailsArr[i]);
        }
        sendReminderCheckinRequest.setReceiverMails(mailsList);
        sendReminderCheckinRequest.setTid(IDFactory.getTid());
        sendReminderCheckinRequest.setSid(IDFactory.getSid());
        return sendReminderCheckinRequest;

    }

    public String getMailBody(Resource resource, String templateMail,
                              boolean secure, ResourceResolverFactory resolverFactory,
                              RequestResponseFactory requestResponseFactory,
                              SlingRequestProcessor requestProcessor, RimborsoSessionContext rimborsoSessionContext) {

        String result = null;
        String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(resource, false);
        try {

            ByteArrayOutputStream out = new ByteArrayOutputStream();

            /* Get resource resolver */
            @SuppressWarnings("deprecation")
            ResourceResolver resourceResolver =
                    resolverFactory.getAdministrativeResourceResolver(null);
            String requestedUrl = baseUrl + AlitaliaConstants.BASE_CONFIG_PATH
                    + "/" + CheckinConstants.CHECKIN_TEMPLATE_EMAIL_FOLDER
                    + "/" + templateMail;

            /* Setup request */

            HttpServletRequest req =
                    requestResponseFactory.createRequest("GET", requestedUrl);
            WCMMode.DISABLED.toRequest(req);

            out = new ByteArrayOutputStream();
            HttpServletResponse resp =
                    requestResponseFactory.createResponse(out);

            /* Process request through Sling */
            requestProcessor.processRequest(req, resp, resourceResolver);

            String encoding = "UTF-8";

            result = new String(out.toByteArray(), Charset.forName(encoding))
                    .replace("{{nome}}", rimborsoSessionContext.getNome())
                    .replace("{{cognome}}", rimborsoSessionContext.getCognome())
                    .replace("{{codicebiglietto}}", rimborsoSessionContext.getCodiceBiglietto())
                    .replace("{{motivorichiesta}}", rimborsoSessionContext.getMotivoRichiesta())
                    .replace("{{quantitarimborso}}", rimborsoSessionContext.getQuantitàRimborso())
                    .replace("{{ticketcode}}", rimborsoSessionContext.getTicketCode());


        } catch (Exception e) {
            logger.error("Generic exception while retrieving email template "
                    + "from JCR repository: {}", e);
            result = "";
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Template retrieved and APIS/ESTA placeholders "
                    + "replaced: {} ", result);
        }

        return result;

    }

}

