package com.alitalia.aem.consumer.millemiglia.model.estrattoconto;

import com.alitalia.aem.common.data.home.MMActivityData;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.enumerations.ActivityTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMActivityTypeEnum;
import com.alitalia.aem.common.messages.home.StatementByDateRequest;
import com.alitalia.aem.common.messages.home.StatementByDateResponse;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.millemiglia.render.EstrattoContoRender;
import com.alitalia.aem.consumer.millemiglia.render.EstrattoContoRenderComparator;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Model(adaptables = { SlingHttpServletRequest.class })
public class EstrattoContoModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest request;

	@Inject
	private AlitaliaCustomerProfileManager customerProfileManager;

	@Inject
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	private List<EstrattoContoRender> activities;
	private Map<String, Object> links;

	private String mmRemainingPoints = ""; // only for transitionmode, printed in estratto-conto-partial.html.html
	private String mmEarnedTotalPoints = ""; // only for transitionmode, printed in estratto-conto-partial.html.html
	private String mmSpentTotalPoints = ""; // only for transitionmode, printed in estratto-conto-partial.html.html
	private String mmQualifyingSegment = ""; // only for transitionmode, printed in estratto-conto-partial.html.html

	private static final String ACTIVITIES_DATE_FORMAT = "dd/MM/yyyy";
	private static final String ACTIVITY_TYPE_OTHER = "Other";
	private static final int MAX_RESULT_LENGTH = 20;

	@PostConstruct
	protected void initModel() {
		try {
			initBaseModel(request);

			MMCustomerProfileData user = AlitaliaUtils.getAuthenticatedUser(request);

			if (user != null) {
				user.setCustomerPinCode(customerProfileManager.decodeAndDecryptProperty(user.getCustomerPinCode()));
				StatementByDateRequest statementRequest = new StatementByDateRequest();
				String activity = request.getParameter("activity");
				String day = request.getParameter("day");
				String month = request.getParameter("month");
				String year = request.getParameter("year");

				String transitionmode = request.getParameter("mmtransitionprogram");
				logger.debug("TransitionMode:" + transitionmode);
				Boolean mmTransition = false;
				if("true".equals(transitionmode)){
					mmTransition = true;
				}

				String dayto = "31";
				String monthto = "12";
				String yearto = "2017";

				try {
					statementRequest.setActivityType(activity != ACTIVITY_TYPE_OTHER
							? ActivityTypeEnum.fromValue(activity) : ActivityTypeEnum.NOT_SPECIFIED);
				} catch (Exception e) {
					logger.debug(e.toString());
					statementRequest.setActivityType(ActivityTypeEnum.NOT_SPECIFIED);
				}
				try {
					Calendar filterDate = Calendar.getInstance();
					filterDate.setTime(
							new SimpleDateFormat(ACTIVITIES_DATE_FORMAT).parse(day + "/" + month + "/" + year));
					statementRequest.setDateFrom(filterDate);

					if(mmTransition) {
						Calendar dateMinTransitionProgram = Calendar.getInstance();
						dateMinTransitionProgram.setTime(
								new SimpleDateFormat(ACTIVITIES_DATE_FORMAT).parse("01/01/2013")
						);
						if(filterDate.before(dateMinTransitionProgram)){
							statementRequest.setDateFrom(dateMinTransitionProgram);
						}
					}

				} catch (Exception e) {
					logger.debug(e.toString());
				}
				if(mmTransition) {
					try {
						Calendar filterDateTo = Calendar.getInstance();
						filterDateTo.setTime(
								new SimpleDateFormat(ACTIVITIES_DATE_FORMAT).parse(dayto + "/" + monthto + "/" + yearto));
						statementRequest.setDateTo(filterDateTo);
					} catch (Exception e) {
						logger.debug(e.toString());
					}
				}
				statementRequest.setCustomerProfile(user);
				statementRequest.setMaxResults(activity != ACTIVITY_TYPE_OTHER ? MAX_RESULT_LENGTH : null);
				StatementByDateResponse statementResponse = personalAreaMilleMigliaService
						.getStatementByDate(statementRequest);

				activities = activity != ACTIVITY_TYPE_OTHER
						? AlitaliaUtils.convertActivities(statementResponse.getCustomerProfile().getActivities())
						: filterOtherActivities(statementResponse.getCustomerProfile().getActivities());

				logger.debug("statementResponse: " + statementResponse.toString());

				if(mmTransition) {
					MMCustomerProfileData customerProfileTransitionMode = statementResponse.getCustomerProfile();
					mmRemainingPoints = customerProfileTransitionMode.getPointsRemainingTotal().toString();
                    mmEarnedTotalPoints = customerProfileTransitionMode.getPointsEarnedTotal().toString();
                    mmSpentTotalPoints = customerProfileTransitionMode.getPointsSpentTotal().toString();
                    mmQualifyingSegment = customerProfileTransitionMode.getQualifyingSegment().toString();
					logger.debug("mmRemainingPoints="+ mmRemainingPoints);
				}
				// links =
				// AlitaliaUtils.fromJcrMultifieldToMapArrayObject(request.getResource(),
				// "items");

			} else {
				// Utente non autenticato che prova ad accedere al partial.
				logger.info("Utente non autenticato.");
				// TODO: Gestire
			}
		} catch (Exception e) {
			logger.error(e.toString() + " " + e.getCause().toString());
		}
	}

	private List<EstrattoContoRender> filterOtherActivities(List<MMActivityData> activitiesToFilter) {
		MMActivityTypeEnum[] otherTypes = { MMActivityTypeEnum.CATALOGUE, MMActivityTypeEnum.PROMOTIONAL,
				MMActivityTypeEnum.CDS, MMActivityTypeEnum.CONVERSION };

		return activitiesToFilter.stream().filter(act -> Arrays.asList(otherTypes).contains(act.getActivityType()))
				.limit(MAX_RESULT_LENGTH).map(actToConv -> new EstrattoContoRender(actToConv))
				.sorted(EstrattoContoRenderComparator.getInstance(false)).collect(Collectors.toList());
	}

	public List<EstrattoContoRender> getActivities() {
		return activities;
	}

	/**
	 * only for transitionmode, printed in estratto-conto-partial.html.html
	 * @return String
	 */
	public String getMmRemainingPoints() {return mmRemainingPoints;}
	/**
	 * only for transitionmode, printed in estratto-conto-partial.html.html
	 * @return String
	 */
	public String getMmEarnedTotalPoints() {return mmEarnedTotalPoints;}
	/**
	 * only for transitionmode, printed in estratto-conto-partial.html.html
	 * @return String
	 */
	public String getMmSpentTotalPoints() {return mmSpentTotalPoints;}
	/**
	 * only for transitionmode, printed in estratto-conto-partial.html.html
	 * @return String
	 */
	public String getMmQualifyingSegment() {return mmQualifyingSegment;}

	public Map<String, Object> getLinks() {
		return links;
	}

}
