package com.alitalia.aem.consumer.rimborso.model;

public class RimborsoSessionContext {

    String nome;

    String cognome;

    String ticketCode;

    String codiceBiglietto;

    String motivoRichiesta;

    String quantitàRimborso;

    public RimborsoSessionContext() {}

    public RimborsoSessionContext(String nome, String cognome, String ticketCode, String codiceBiglietto, String motivoRichiesta, String quantitàRimborso) {
        this.nome = nome;
        this.cognome = cognome;
        this.ticketCode = ticketCode;
        this.codiceBiglietto = codiceBiglietto;
        this.motivoRichiesta = motivoRichiesta;
        this.quantitàRimborso = quantitàRimborso;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodiceBiglietto() {
        return codiceBiglietto;
    }

    public void setCodiceBiglietto(String codiceBiglietto) {
        this.codiceBiglietto = codiceBiglietto;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(String ticketCode) {
        this.ticketCode = ticketCode;
    }

    public String getMotivoRichiesta() {
        return motivoRichiesta;
    }

    public void setMotivoRichiesta(String motivoRichiesta) {
        this.motivoRichiesta = motivoRichiesta;
    }

    public String getQuantitàRimborso() {
        return quantitàRimborso;
    }

    public void setQuantitàRimborso(String quantitàRimborso) {
        this.quantitàRimborso = quantitàRimborso;
    }

    @Override
    public String toString() {
        return "RimborsoSessionContext{" +
                "nome='" + nome + '\'' +
                ", cognome='" + cognome + '\'' +
                ", ticketCode=" + ticketCode +
                ", codicePrenotazione='" + codiceBiglietto + '\'' +
                ", motivoRichiesta='" + motivoRichiesta + '\'' +
                ", quantitàRimborso='" + quantitàRimborso + '\'' +
                '}';
    }

}
