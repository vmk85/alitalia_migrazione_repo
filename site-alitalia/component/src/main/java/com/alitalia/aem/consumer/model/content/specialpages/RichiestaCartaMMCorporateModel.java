package com.alitalia.aem.consumer.model.content.specialpages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class RichiestaCartaMMCorporateModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;

	private RichiestaCartaMMCorporateData data;
	private boolean showCountry;
	private boolean multipleCountries;
	private List<String> countryList;

	@PostConstruct
	protected void initModel() {
		logger.debug("[RichiestaCartaMMCorporateModel] initModel");

		try {
			super.initBaseModel(slingHttpServletRequest);
			
			// retrieve and remove data from session if any
			data = (RichiestaCartaMMCorporateData) slingHttpServletRequest.getSession().getAttribute(RichiestaCartaMMCorporateData.NAME);
			slingHttpServletRequest.getSession().removeAttribute(RichiestaCartaMMCorporateData.NAME);
			
			String[] countryArray = RichiestaCartaMMCorporateData.extractCountryList(
					marketCode.toUpperCase()
					, configuration.getStringProperty(AlitaliaConfigurationHolder.MM_CORPORATE_COUNTRIES_MAP));
			showCountry = countryArray != null && countryArray.length > 0;
			multipleCountries = countryArray != null && countryArray.length > 1;
			if(countryArray != null)
				countryList = Arrays.asList(countryArray);
			
		} catch (Exception e) {
			logger.error("Unexpected error: {}", e);
		}
	}

	public RichiestaCartaMMCorporateData getData() {
		return data;
	}

	public void setData(RichiestaCartaMMCorporateData data) {
		this.data = data;
	}

	public boolean isShowCountry() {
		return showCountry;
	}

	public boolean isMultipleCountries() {
		return multipleCountries;
	}

	public List<String> getCountryList() {
		return countryList;
	}

}
