package com.alitalia.aem.consumer.millemiglia.model.servizidedicati;

import java.util.Base64;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import javax.jcr.Session;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class ServiziBuyPointsModel {
	
	private String url;
	private String customerNumber;
	private String name;
	private String surname;
	private String totalPoints;
	private String cdLng;
	private String tier;
	private String tipContr;
	
	private Logger logger = LoggerFactory.getLogger(ServiziDedicatiModel.class);
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
    private AlitaliaConfigurationHolder configuration;
	
	@PostConstruct
	protected void initModel() {
		
		try {
			ResourceResolver resourceResolver = request.getResourceResolver();
			Session session = resourceResolver.adaptTo(Session.class);
			UserPropertiesManager userPropertiesManager = resourceResolver.adaptTo(UserPropertiesManager.class);
			
			UserProperties profileProperties = userPropertiesManager.getUserProperties(session.getUserID(), "profile");
			
			customerNumber = encryptValue(profileProperties.getProperty("customerNumber"));
			name = encryptValue(profileProperties.getProperty("customerName"));
			surname = encryptValue(profileProperties.getProperty("customerSurname"));
			totalPoints = encryptValue(profileProperties.getProperty("milleMigliaTotalMiles"));
			cdLng = encryptValue(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
			tier = encryptValue(profileProperties.getProperty("milleMigliaTierCode"));
			tipContr = encryptValue(profileProperties.getProperty("customerContractType"));
			
			url = configuration.getBuyPointsEndpoint();
		}
		catch (Exception e) {
			logger.error("[ServiziMillemigliaModel] Error", e);
		}
	}
	
	private String encryptValue(String value) {
		
		if (value == null || value.isEmpty()) {
			return "";
		}
		try {
			
			String keyValue = configuration.getBuyPointsKey();
			String ivValue = configuration.getBuyPointsIV();
			String keySpecAlgorithm = configuration.getBuyPointsKeyAlgorithm();
			String cipherConf = configuration.getBuyPointsCipherConf();
			
			byte key[] = hexToByteArray(keyValue);
			byte iv[] = hexToByteArray(ivValue);
			IvParameterSpec ivParamSpec = new IvParameterSpec(iv);
			SecretKeySpec skeySpec = new SecretKeySpec(key, keySpecAlgorithm);
			Cipher cipher = Cipher.getInstance(cipherConf);
			cipher.init(1, skeySpec, ivParamSpec);
			byte encrypted[] = cipher.doFinal(value.getBytes());
			return Base64.getEncoder().encodeToString(encrypted);
		}
		catch(Exception e) {
			logger.error("[ServiziMillemigliaModel] encryptValue error", e);
		}
		return "";
	}

	private byte[] hexToByteArray(String s) {
		byte b[] = new byte[s.length() / 2];
		for (int i = 0; i < b.length; i++) {
			int index = i * 2;
			int v = Integer.parseInt(s.substring(index, index + 2), 16);
			b[i] = (byte) v;
		}

		return b;
	}
	
	public String getUrl() {
		return url;
	}
	
	public String getCustomerNumber() {
		return customerNumber;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getTotalPoints() {
		return totalPoints;
	}

	public String getCdLng() {
		return cdLng;
	}

	public String getTier() {
		return tier;
	}

	public String getTipContr() {
		return tipContr;
	}
}
