package com.alitalia.aem.consumer.booking.controller;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingCartRecovery extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	private String params;
	

	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			initBaseModel(request);
			params = ctx.cartRecoveryParams != null ? "&" + ctx.cartRecoveryParams : "";
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			params = "";
		}
	}


	public String getParams() {
		return params;
	}
	
}