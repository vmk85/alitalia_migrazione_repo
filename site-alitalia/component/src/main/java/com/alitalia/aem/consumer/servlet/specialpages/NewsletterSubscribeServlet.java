package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.SlingRequestProcessor;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.enumerations.MailingListActionTypesEnum;
import com.alitalia.aem.common.messages.home.MailinglistRegistrationRequest;
import com.alitalia.aem.common.messages.home.MailinglistRegistrationResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.NewsletterSubscribeData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.WCMMode;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "newslettersubsubmit" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class NewsletterSubscribeServlet extends GenericFormValidatorServlet {
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;
	
	@Reference
    private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private ResourceResolverFactory resolverFactory;

    /** Service to create HTTP Servlet requests and responses */
    @Reference
    private RequestResponseFactory requestResponseFactory;

    /** Service to process requests through Sling */
    @Reference
    private SlingRequestProcessor requestProcessor;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[NewsletterSubscribeServlet] validateForm");

		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			return validator.validate();

		}

		// an error occurred...
		catch (Exception e) {
			logger.error(
					"Errore durante la procedura di iscrizione newsletter", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		
		logger.debug("[NewsletterSubscribeServlet] performSubmit");
		
		final I18n i18n = new I18n(request.getResourceBundle(
		        AlitaliaUtils.findResourceLocale(request.getResource())));
		
		MMCustomerProfileData profile = new MMCustomerProfileData();
		profile.setEmail(request.getParameter("email"));
		String bodyMail = getEmailTemplate(request.getResource(), configuration.getTemplateMailNewsletter());
		if(bodyMail == null){
			throw new IOException("Errore nel recupero mail");
		}
		
		MailinglistRegistrationRequest newsletterRequest = new MailinglistRegistrationRequest();
		newsletterRequest.setTid(IDFactory.getTid());
		newsletterRequest.setSid(IDFactory.getSid(request));
		newsletterRequest.setBodyWelcomeMail(bodyMail);
		newsletterRequest.setSubjectMail(i18n.get(I18nKeySpecialPage.SUBJECT_MAIL_NEWSLETTER));
		newsletterRequest.setAction(MailingListActionTypesEnum.REGISTER);
		newsletterRequest.setCustomerProfile(profile);
		logger.debug("[NewsletterSubscribeServlet] calling delegate");
		MailinglistRegistrationResponse mailingRegistrationResponse = consumerLoginDelegate.registerToMailinglist(newsletterRequest);
		logger.debug("[NewsletterSubscribeServlet] called delegate {}", mailingRegistrationResponse);
		boolean result = mailingRegistrationResponse.isRegistrationIsSuccessful();
		logger.debug("[NewsletterSubscribeServlet] result mailingRegistrationResponse {}", mailingRegistrationResponse.toString() + " result: " + result);
		if (!result) {
			logger.debug("[NewsletterSubscribeServlet] Error registrazione newsletter: " + mailingRegistrationResponse.getErrorMessage() + ", mailingRegistrationResponse");
			throw new IOException(mailingRegistrationResponse.getErrorMessage());
		}
		response.sendRedirect(request.getResource().getParent().getParent().getPath()+"/myalitalia/myalitalia-registrati.html?a="+request.getParameter("email"));
	}

	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {

		// get parameters from request
		String email = request.getParameter("email");
		String newsletterCond = request.getParameter("receivenewsletter");

		// add validate conditions
		validator.addDirectConditionMessagePattern("email", email,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID, "isNotEmpty");

		//validator.addDirectCondition("receivenewsletter", newsletterCond,
				//I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "isNotEmpty");
		validator.addCrossCondition("receivenewsletter", newsletterCond, "accepted", 
				I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "areEqual");
		
		validator.addDirectConditionMessagePattern("email",email ,I18nKeyCommon.MESSAGE_INVALID_FIELD, 
				I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID, "isEmail");
		
		return validator;

	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[NewsletterSubscribeServlet] saveDataIntoSession");
		logger.debug("[NewsletterSubscribeServlet] EXCEPTION saveDataIntoSession: " + e.getMessage());
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		String error = i18n.get(I18nKeySpecialPage.ERROR_SERVICE);
		logger.error("[NewsletterSubscribeServlet] Error message "+error);
		//TODO:inserire property corretta
		String errorCustomerAlreadyExists = "already exist";
		if (e.getMessage().contains(errorCustomerAlreadyExists)) {
			logger.debug("[NewsletterSubscribeServlet] ERROR DETECTED: customer error already exists");
			error = i18n.get(I18nKeySpecialPage.CUSTOMER_ALREADY_EXISTS);
		}
		// create NewsletterSubscribeServlet object and save it into session
		NewsletterSubscribeData newslettersubscribeData = new NewsletterSubscribeData();		
		newslettersubscribeData.setMail(request.getParameter("email"));
		newslettersubscribeData.setNewslettersubCond(request.getParameter("receivenewsletter"));
		newslettersubscribeData.setError(error);
		request.getSession().setAttribute(NewsletterSubscribeData.NAME, newslettersubscribeData);
	}
	
	@SuppressWarnings("deprecation")
	private String getEmailTemplate(Resource resource, String templateMail) {
        
		/* Setup response */
		String result = "";
		try {

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			/* Get resource resolver */
			ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			String requestedUrl = AlitaliaUtils.findSiteBaseExternalUrl(resource, true) 
					+ "alitalia-config/" + templateMail;

			/* Setup request */
			HttpServletRequest req = requestResponseFactory.createRequest("GET", requestedUrl);
			WCMMode.DISABLED.toRequest(req);

			out = new ByteArrayOutputStream();
			HttpServletResponse resp = requestResponseFactory.createResponse(out);

			/* Process request through Sling */
			requestProcessor.processRequest(req, resp, resourceResolver);

			String encoding = "UTF-8";

			result = new String(out.toByteArray(), Charset.forName(encoding));

		} catch (LoginException | ServletException | IOException e) {
			logger.error("Generic exception while retrieving email template from JCR repository: {}", e);
			result = "";
		}
		if (logger.isDebugEnabled())
			logger.debug("Template retrieved: ", result);

		return result;
	}
}
