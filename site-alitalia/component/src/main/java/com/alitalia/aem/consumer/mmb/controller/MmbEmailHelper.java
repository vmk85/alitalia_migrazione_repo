package com.alitalia.aem.consumer.mmb.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbAirportData;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.mmb.model.MmbFlightsGroup;
import com.alitalia.aem.consumer.mmb.render.MmbPassengerDataRender;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables={SlingHttpServletRequest.class})
public class MmbEmailHelper extends GenericBaseModel {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	private String hostName = "";
	private String mailImageUrl = "";
	private String marketSiteUrl = "";

	private List<MmbFlightsGroup> flightsGroupList;
	private List<MmbPassengerDataRender> passengerDataRenderList;

	private String email;
	private String tipoTelefono;
	private String prefissoInternazionale;
	private String telefono;
	private String pnr;

	protected MmbSessionContext ctx;
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
    private AlitaliaConfigurationHolder configuration;
	
	@Override
	protected void initBaseModel(SlingHttpServletRequest request) {
	    logger.debug("[MmbEmailHelper] initBaseModel");
	    
	    super.initBaseModel(request);

	    try {
	      HttpSession session = request.getSession(true);
	      if (session != null) {
	        ctx = (MmbSessionContext) session.getAttribute(MmbConstants.MMB_CONTEXT_ATTRIBUTE);
		    logger.debug("[MmbEmailHelper] ctx retrieved from session.  ctx==null: " + (ctx == null));
	      }
	      else {
	        ctx = (MmbSessionContext) request.getAttribute(MmbConstants.MMB_CONTEXT_ATTRIBUTE);
	      }
	    } catch (Exception e) {
	      ctx = (MmbSessionContext) request.getAttribute(MmbConstants.MMB_CONTEXT_ATTRIBUTE);
	      logger.debug("[MmbEmailHelper] ctx retrieved from request.  ctx==null: " + (ctx == null));
	    }

	    if ((ctx == null) && !isWCMEnabled()) {
	      logger.error("Cannot find MMB session context in session or request.");
	      throw new RuntimeException("Cannot find MMB session context in session or request.");
	    }

	  }
	
	@PostConstruct
	protected void initModel() {
		try{
			logger.debug("[MmbEmailHelper] initModel. Hashcode: " + this.hashCode());
	
			this.initBaseModel(request);
	
			String prefix = "http://";
			if (!isWCMEnabled() && configuration.getHttpsEnabled())
				prefix = "https://";
			hostName = prefix + configuration.getExternalDomain();
			logger.debug("hostName: " + hostName);
	//		mailImageUrl = hostName + "/etc/designs/alitalia/clientlibs/images/";
			mailImageUrl = hostName + configuration.getStaticImagesPath();
			logger.debug("mailImageUrl: " + mailImageUrl);
			this.marketSiteUrl = hostName + AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
			
			if (isWCMEnabled()) {
				ArrayList<MmbFlightData> list = new ArrayList<MmbFlightData>();
				for (int i = 0; i < 2; i++) {
					MmbAirportData depAirportData = new MmbAirportData();
					depAirportData.setCode("FCO");
					MmbAirportData arrAirportData = new MmbAirportData();
					arrAirportData.setCode("NYC");
					MmbFlightData flightData = new MmbFlightData();
					flightData.setType(RouteTypeEnum.OUTBOUND);
					flightData.setMultitratta(null);
					flightData.setRouteId(i);
					flightData.setCarrier("AZ");
					flightData.setFlightNumber("0609");
					Calendar depDateTime = Calendar.getInstance();
					Calendar arrDateTime = Calendar.getInstance();
					arrDateTime.add(Calendar.HOUR, 24);
					flightData.setArrivalDateTime(arrDateTime);
					flightData.setDepartureDateTime(depDateTime);
					flightData.setFrom(depAirportData);
					flightData.setFromTerminal("1");
					flightData.setTo(arrAirportData);
					flightData.setToTerminal("C");
					list.add(flightData);
				}
				for (int i = 0; i < 1; i++) {
					MmbAirportData depAirportData = new MmbAirportData();
					depAirportData.setCode("FCO");
					MmbAirportData arrAirportData = new MmbAirportData();
					arrAirportData.setCode("NYC");
					MmbFlightData flightData = new MmbFlightData();
					flightData.setType(RouteTypeEnum.RETURN);
					flightData.setMultitratta(null);
					flightData.setRouteId(i);
					flightData.setCarrier("AZ");
					flightData.setFlightNumber("0609");
					Calendar depDateTime = Calendar.getInstance();
					Calendar arrDateTime = Calendar.getInstance();
					arrDateTime.add(Calendar.HOUR, 24);
					flightData.setArrivalDateTime(arrDateTime);
					flightData.setDepartureDateTime(depDateTime);
					flightData.setFrom(depAirportData);
					flightData.setFromTerminal("1");
					flightData.setTo(arrAirportData);
					flightData.setToTerminal("C");
					list.add(flightData);
				}
				flightsGroupList = MmbFlightsGroup.groupByRouteType(list);
	
				MmbPassengerData passengerData = new MmbPassengerData();
				passengerData.setName("Name");
				passengerData.setLastName("Surname");
				passengerData.setEticket("1234567890");
				passengerData.setFrequentFlyerCode("123456");
				passengerDataRenderList = new ArrayList<MmbPassengerDataRender>();
				passengerDataRenderList.add(new MmbPassengerDataRender(passengerData));
	
				email = "name.surname@mail.com";
				tipoTelefono = "M";
				prefissoInternazionale = "11";
				telefono = "011111111111";
				pnr = "PNR123";
			} 
			else
			{
				flightsGroupList = MmbFlightsGroup.groupByRouteType(ctx.route.getFlights());
	
				MmbFlightData flightData = ctx.route.getFlights().get(0);
				passengerDataRenderList = new ArrayList<MmbPassengerDataRender>();
				for (MmbPassengerData passengerData : flightData.getPassengers()) {
					passengerDataRenderList.add(new MmbPassengerDataRender(passengerData));
				}
	
				MmbPassengerData applicantPassenger = ctx.route.getApplicantPassenger();
				email = applicantPassenger.getEmail1();
				email = email.substring(email.lastIndexOf("/") + 1);
				String[] telefonoSplit = applicantPassenger.getTelephone1().split(
						"/");
				tipoTelefono = telefonoSplit[0];
				prefissoInternazionale = telefonoSplit[1];
				telefono = telefonoSplit[2];
				pnr = ctx.pnr; 
				if(logger.isDebugEnabled()){
					logger.debug("[MmbEmailHelper] pnr is: " + this.ctx.pnr);
				}
			}
			
		}
		catch(Exception e){
			logger.error("[MmbEmailHelper] unexpected error. " , e);
		}
	}
	
	public String getHostName() {
		return hostName;
	}

	public String getMailImageUrl() {
		return mailImageUrl;
	}

	public String getMarketSiteUrl() {
		return marketSiteUrl;
	}

	public String getPnr() {
		logger.debug("[MmbEmailHelper] getPnr. Hashcode: " + this.hashCode() + " .  this.ctx==null: " + (this.ctx == null));
		return pnr;
	}

	public String getEmail() {
		return email;
	}

	public String getTipoTelefono() {
		return tipoTelefono;
	}

	public String getPrefissoInternazionale() {
		return prefissoInternazionale;
	}

	public String getTelefono() {
		return telefono;
	}

	public List<MmbFlightsGroup> getFlightsGroupList() {
		return flightsGroupList;
	}

	public List<MmbPassengerDataRender> getPassengerDataRenderList() {
		return passengerDataRenderList;
	}

}
