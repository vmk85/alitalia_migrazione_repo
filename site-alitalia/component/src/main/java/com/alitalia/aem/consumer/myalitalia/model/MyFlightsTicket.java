package com.alitalia.aem.consumer.myalitalia.model;

import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;

public class MyFlightsTicket {

    private CheckinTicketSearchResponse checkinTicketSearchResponse;

    private String nome;

    private String cognome;

    public CheckinTicketSearchResponse getCheckinTicketSearchResponse() {
        return checkinTicketSearchResponse;
    }

    public void setCheckinTicketSearchResponse(CheckinTicketSearchResponse checkinTicketSearchResponse) {
        this.checkinTicketSearchResponse = checkinTicketSearchResponse;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    @Override
    public String toString() {
        return "MyFlightsTicket{" +
                "checkinTicketSearchResponse=" + checkinTicketSearchResponse +
                ", nome='" + nome + '\'' +
                ", cognome='" + cognome + '\'' +
                '}';
    }
}
