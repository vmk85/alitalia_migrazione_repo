package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;

import javax.servlet.Servlet;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.checkinrest.CheckinGetCartResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = {"checkingetcart"}),
        @Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
        @Property(name = "sling.servlet.extensions", value = {"json"}),
        @Property(name = "sling.servlet.methods", value = {"POST"})})
@SuppressWarnings("serial")
public class CheckinGetCartServlet extends GenericCheckinFormValidatorServlet {

    // logger
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private AlitaliaUtils utils;

    @Reference
    private AlitaliaConfigurationHolder configuration;

    @Reference
    private CheckinSession checkinSession;

    @Override
    protected AlitaliaConfigurationHolder getConfiguration() {
        return configuration;
    }

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        //TODO - da verificare
        ResultValidation resultValidation = new ResultValidation();
        resultValidation.setResult(true);
        return resultValidation;
    }

    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
        CheckinSessionContext ctx = getCheckInSessionContext(request);
        try {

            CheckinGetCartResponse result = checkinSession.getCart(request, ctx);

			if (result != null) {
				if(result.getCart().getError() != null && !result.getCart().getError().equals("")){
					managementError(request, response, result.getCart().getError());
				}else{
					if (!ctx.comfortSeatInCart) {
						if (result.getCart().getCheckinCart() != null) {
							if (!result.getCart().getCheckinCart().getAZFlightCart().isEmpty()) {
								if (!result.getCart().getCheckinCart().getAZFlightCart().get(0).getAZSegmentCart().isEmpty()) {
									if (!result.getCart().getCheckinCart().getAZFlightCart().get(0).getAZSegmentCart().get(0).getAZCartPassenger().isEmpty()) {
										if (!result.getCart().getCheckinCart().getAZFlightCart().get(0).getAZSegmentCart().get(0).getAZCartPassenger().get(0).getAZCartAncillary().isEmpty()) {
											if (result.getCart().getCheckinCart().getAZFlightCart().get(0).getAZSegmentCart().get(0).getAZCartPassenger().get(0).getAZCartAncillary().get(0).getPrice() != 0 && result.getCart().getCheckinCart().getAZFlightCart().get(0).getAZSegmentCart().get(0).getAZCartPassenger().get(0).getAZCartAncillary().get(0).getRficSubcode().equalsIgnoreCase("0B5")) {
												ctx.comfortSeatInCart = true;
											}
										}
									}
								}
							}
						}
					}
					//response.sendRedirect(successPage);
					managementSucces(request, response, result);
				}
			} else {
				logger.error("[CheckinGetCartServlet] - Errore durante l'invocazione del servizio." );
				managementError(request, response, "Errore nell'invocazione del servizio.");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("errore in CheckinGetCartServlet, " + e.getMessage());
			logger.info("errore in CheckinGetCartServlet, " + e.getMessage());
		}
	}
	
	public void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error){
		
		logger.info("[CheckinGetCartServlet] [managementError] error =" + error );
		
		Gson gson = new Gson();
		
		com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);

        TidyJSONWriter json;
        try {
            json = new TidyJSONWriter(response.getWriter());

            response.setContentType("application/json");

            json.object();
            json.key("isError").value(true);
            json.key("errorMessage").value(errorObj.getErrorMessage());
            json.key("sabreStatusCode").value(errorObj.getSabreStatusCode());
            json.key("conversationID").value(errorObj.getConversationID());
            json.endObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response, CheckinGetCartResponse result) {

//        logger.info("[CheckinGetCartServlet] [managementSucces] ...");
        String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
                request.getResource(), false);
        //+ getConfiguration().getCheckinFlightListPage();//Gestire la pagina di success

        successPage = request.getResourceResolver().map(successPage);

//        logger.info("[CheckinGetCartServlet] [managementSucces] successPage = " + successPage);

        Gson gson = new Gson();
        String jsonInString = gson.toJson(result);

        TidyJSONWriter json;
        try {

            String decimalSeparator = new HierarchyNodeInheritanceValueMap(request.getResource().getParent()).getInherited("decimalSeparator", String.class);
            String decimalDigit = new HierarchyNodeInheritanceValueMap(request.getResource().getParent()).getInherited("decimalDigit", String.class);

            json = new TidyJSONWriter(response.getWriter());
            response.setContentType("application/json");
            json.object();
            json.key("isError").value(false);
            json.key("data").value(jsonInString);

            if (decimalSeparator != null && decimalDigit != null) {
                json.key("decimalDigit").value(decimalDigit);
                json.key("decimalSeparator").value(decimalSeparator);
            }
            // json.key("successPage").value(successPage);
            json.endObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
