package com.alitalia.aem.consumer.mmb.controller;

import javax.servlet.http.HttpSession;

import org.apache.sling.api.SlingHttpServletRequest;

import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;

public class MmbSessionGenericController extends GenericBaseModel {
	
	protected MmbSessionContext ctx;
	
	@Override
	protected void initBaseModel(SlingHttpServletRequest request) {
		super.initBaseModel(request);
		if (!isWCMEnabled()) {
			HttpSession session = request.getSession(true);
			Object sessionCtx = session.getAttribute(MmbConstants.MMB_CONTEXT_ATTRIBUTE);
			if (sessionCtx == null) {
				logger.error("Cannot find MMB session context in session.");
				throw new RuntimeException("Cannot find MMB session context in session.");
			} else if (!(sessionCtx instanceof MmbSessionContext)) {
				logger.error("Invalid MMB session context in session (wrong class).");
				throw new RuntimeException("Invalid MMB session context in session (wrong class).");
			} else {
				this.ctx = (MmbSessionContext) sessionCtx;
			}
		}
	}

	public MmbSessionContext getCtx() {
		return ctx;
	}
	
}
