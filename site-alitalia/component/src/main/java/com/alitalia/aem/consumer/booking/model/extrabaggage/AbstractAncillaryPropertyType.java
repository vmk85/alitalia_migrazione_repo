package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Enumerazione per il tipo di proprietà per gli ancillari. 
   ['INT', 'DECIMAL', 'TEXT', 'ENUM']
*/
public enum AbstractAncillaryPropertyType
{
	INT,
	DECIMAL,
	TEXT,
	ENUM;

	public static final int SIZE = java.lang.Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static AbstractAncillaryPropertyType forValue(int value)
	{
		return values()[value];
	}
}