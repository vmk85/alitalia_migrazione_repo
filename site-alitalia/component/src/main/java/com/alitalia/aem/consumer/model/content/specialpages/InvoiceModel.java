package com.alitalia.aem.consumer.model.content.specialpages;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class InvoiceModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private InvoiceData invoiceData;

	@PostConstruct
	protected void initModel() {
		logger.debug("[InvoiceModel] initModel");

		super.initBaseModel(slingHttpServletRequest);

		// retrieve and remove CookiesData from session if any
		invoiceData = (InvoiceData) slingHttpServletRequest.getSession().getAttribute(InvoiceData.NAME);
		slingHttpServletRequest.getSession().removeAttribute(InvoiceData.NAME);

	}

	public InvoiceData getInvoiceData() {
		return invoiceData;
	}

}
