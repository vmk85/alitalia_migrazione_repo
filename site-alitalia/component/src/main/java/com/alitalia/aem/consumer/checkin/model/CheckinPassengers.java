package com.alitalia.aem.consumer.checkin.model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.CanAddPassengerReasonEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinPassenger;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinPassengers extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private CheckinSession checkinSession;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private List<CheckinPassenger> passengers;
	private int passengersSize;
	private List<FrequentFlyerTypeData> frequentFlyerTypes;
	private boolean canAddPassenger;
	
	private String infantLightboxPage;
	private String specialFareLightboxPage;
	private String flightListUrl;
	
	@PostConstruct
	protected void initModel() {
		
		try {
			initBaseModel(request);
			passengers = new ArrayList<CheckinPassenger>();
		
			if (ctx != null) {
				
				List<CheckinPassengerData> allPassengers = ctx.passengers;
			
				for (CheckinPassengerData passengerData : allPassengers) {
					CheckinPassenger passenger = checkinSession.toCheckinPassenger(passengerData);
					
					if (!CheckinUtils.isCheckedIn(passengerData.getStatus()) &&
							passengerData.getFrequentFlyerCode() != null && 
							!passengerData.getFrequentFlyerCode().isEmpty() &&
							passengerData.getFrequentFlyerCarrier() != null &&
							passengerData.getFrequentFlyerCarrier().getCode() != null &&
							!passengerData.getFrequentFlyerCarrier().getCode().isEmpty()) {
					
						passenger.setFrequentFlyerCode(StringUtils.stripStart(passengerData.getFrequentFlyerCode(), "0"));
						passenger.setFrequentFlyerType(passengerData.getFrequentFlyerCarrier().getCode());
					}
					passengers.add(passenger);
				}
				frequentFlyerTypes = checkinSession.getFrequentFlyersList(ctx);
				ctx.frequentFlyerTypes = frequentFlyerTypes;
				canAddPassenger = checkinSession.canAddExtraPassenger(ctx) == CanAddPassengerReasonEnum.CAN_ADD;
			}
			passengersSize = passengers.size();
			
			String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
			
			this.infantLightboxPage = baseUrl + configuration.getCheckinInfantLightboxPage();
			this.specialFareLightboxPage = baseUrl + configuration.getCheckinSpeciaFareLightboxPage();
			this.flightListUrl = baseUrl + configuration.getCheckinFlightListPage();
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}
	}
	
	public List<CheckinPassenger> getPassengers() {
		return passengers;
	}

	public int getPassengersSize() {
		return passengersSize;
	}

	public List<FrequentFlyerTypeData> getFrequentFlyerTypes() {
		return frequentFlyerTypes;
	}
	
	public boolean getCanAddPassenger() {
		return canAddPassenger;
	}

	public String getInfantLightboxPage() {
		return infantLightboxPage;
	}

	public String getSpecialFareLightboxPage() {
		return specialFareLightboxPage;
	}

	public String getFlightListUrl() {
		return flightListUrl;
	}
	
}
