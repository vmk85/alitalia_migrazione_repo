package com.alitalia.aem.consumer.booking.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.CashAndMilesData;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSearchTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.render.GenericPriceRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.CashAndMilesCountryTable;
import com.alitalia.aem.consumer.utils.ValidationUtils;
import com.day.io.file.StreamUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingBoxCashAndMiles extends BookingSessionGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	private Logger logger = LoggerFactory.getLogger(getClass()); 
	

	private static final Long MIN_MILES_AMOUNT = 5000l;
//	private static final String AZ_CARRIER = "AZ";
	private static final List<String> AZ_CARRIER = Arrays.asList("AZ","CT","XM","VE");
	
	private Long milesStep;
	private Long minMiles;
	private Long maxMiles;
	private Double milesPerCashFactor;
	private Long userMiles;
	private String messageSelezionaMiglia1;
	private String messageSelezionaMiglia2;
	private String prezzoBasso;
	
	private boolean cashAndMilesHidden = false;
	private boolean loginDone = false;
	private boolean milesTooLow = true;
	private boolean flightNotSelected = true;
	private boolean ticketPriceTooLow = true;
	private boolean flightInvalid = true;
	private boolean cashAndMilesUnavailable = false;

	
	@PostConstruct
	protected void initModel() throws Exception {
		
		try{
			super.initBaseModel(request);
			cashAndMilesHidden = ctx.isCarnetProcess;
			CashAndMilesCountryTable table = CashAndMilesCountryTable.getInstance();
			milesStep = Integer.toUnsignedLong(table.getMilesStep(marketCode));
			milesPerCashFactor = table.getCashMilesRatio(marketCode);
			MMCustomerProfileData user = AlitaliaUtils.getAuthenticatedUser(request);
			String currencySymbol = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencySymbol", String.class);
			messageSelezionaMiglia1 = i18n.get("booking.cashAndMiles.selezionaMiglia1.label", "", new Object[]{currencySymbol});
			String minPrice = "";
			if (milesPerCashFactor != null) {
				minPrice = (new GenericPriceRender(new BigDecimal(MIN_MILES_AMOUNT * milesPerCashFactor), ctx.currency, ctx)).getFare();
			}
			String decimalSeparator = AlitaliaUtils.getInheritedProperty(request.getResource(), "decimalSeparator", String.class);
			if (decimalSeparator == null) {
				decimalSeparator = ".,";
			}
			messageSelezionaMiglia2 = i18n.get("booking.cashAndMiles.selezionaMiglia2.label", "", new Object[]{currencySymbol + " " + minPrice.replaceAll("[" + decimalSeparator + ".][0-9]*$", "")});
			if(user != null){
				//autenticazione effettuata
				loginDone = true;
				Long milesAmount = user.getPointsRemainingTotal();
				if (milesAmount != null && milesAmount >= MIN_MILES_AMOUNT){
					//l'utente ha abbastanza miglia
					milesTooLow = false;
					if(ctx != null && ctx.flightSelections != null  && 
							ValidationUtils.checkAllFlights(ctx, ValidationUtils.isFlightSelected())){
						//sono stati selezionati tutti i voli
						flightNotSelected = false;	
						if(ValidationUtils.checkAllFlights(ctx, ValidationUtils.isNotLight()) && ValidationUtils.checkAllFlights(ctx, ValidationUtils.isSpecificCarrier(AZ_CARRIER))){
							//i voli selezionati sono tutti adatti per cash and miles
							flightInvalid = false;
							BigDecimal minTicketPrice = new BigDecimal(MIN_MILES_AMOUNT * milesPerCashFactor * 4);
							String minTicketPriceValue = (new GenericPriceRender(minTicketPrice, ctx.currency, ctx)).getFare();
							prezzoBasso = i18n.get("booking.cashAndMiles.prezzoBasso.label", "", new Object[]{currencySymbol + " " + minTicketPriceValue});
							BigDecimal grossFare = getFlightSelectionsTotalGrossFare(ctx);
							if(grossFare.compareTo(minTicketPrice) >=0){
								//i voli selezionati hanno un prezzo sufficentemente alto
								BigDecimal maxMilesTicket= grossFare.divide(new BigDecimal(4), 2 , RoundingMode.HALF_UP)
										.divide(new BigDecimal(milesPerCashFactor), 2 , RoundingMode.HALF_UP);
								Long maxMilesTicketRounded = maxMilesTicket
										.subtract(maxMilesTicket.remainder(new BigDecimal(milesStep))).longValue();
								Long milesAmountRounded = milesAmount- (milesAmount % milesStep);
								userMiles = user.getPointsRemainingTotal();
								maxMiles = Math.min(milesAmountRounded, maxMilesTicketRounded);
								minMiles = MIN_MILES_AMOUNT;
								ticketPriceTooLow = false;
							}
						}
					}
				}
				
			}
			
		} catch(Exception e) {
			cashAndMilesUnavailable = true;
			logger.error("Unexpected Error", e);
			throw e;
		}
		
	}
	
	private BigDecimal getFlightSelectionsTotalGrossFare(BookingSessionContext ctx){
		BigDecimal sum = BigDecimal.ZERO;
		for(FlightSelection sel : ctx.flightSelections){
			String selBrand = sel.getSelectedBrandCode();
			for(BrandData brand : sel.getFlightData().getBrands()){
				if(brand.getCode().equals(selBrand)){
					sum = sum.add(brand.getGrossFare());
				}
			}
		}
		return sum;
	}

	public String getMilesStep() {
		return String.valueOf(milesStep);
	}
	
	public String getMinMiles() {
		return String.valueOf(minMiles);
	}

	public String getMaxMiles() {
		return String.valueOf(maxMiles);
	}

	public String getMilesPerCashFactor() {
		return String.valueOf(milesPerCashFactor);
	}

	public boolean isCashAndMilesHidden() {
		return cashAndMilesHidden;
	}

	public boolean isLoginDone() {
		return loginDone;
	}

	public boolean isMilesTooLow() {
		return milesTooLow;
	}

	public boolean isFlightNotSelected() {
		return flightNotSelected;
	}

	public boolean isTicketPriceTooLow() {
		return ticketPriceTooLow;
	}

	public boolean isCashAndMilesUnavailable() {
		return cashAndMilesUnavailable;
	}

	public boolean isFlightInvalid() {
		return flightInvalid;
	}
	
	public Long getUserMiles() {
		return userMiles;
	}

	public String getMessageSelezionaMiglia1() {
		return messageSelezionaMiglia1;
	}

	public String getMessageSelezionaMiglia2() {
		return messageSelezionaMiglia2;
	}

	public String getPrezzoBasso() {
		return prezzoBasso;
	}
}