package com.alitalia.aem.consumer.mmb.render;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;

public class MmbAncillaryExtraBaggageRender {

	private String identifier = "";
	private MmbAncillariesGroup ancillariesGroup = null;
	private List<MmbFlightDataRender> flightDataRenderList;
	private MmbPriceRender priceRender;
	private boolean allowModify = false;
	private boolean checked = false;
	private String value = "";
	private Integer baggageAllowNumber = 0;
	private Integer baggageWeight = null;
	private String routeTypeLabel = "";
	
	private MmbFlightDataRender firstFlight = null;
	private MmbFlightDataRender lastFlight = null;
	
	private boolean currentValue = false;
	
	public MmbAncillaryExtraBaggageRender(MmbAncillariesGroup ancillariesGroup, List<MmbFlightData> flightDataList) {
		super();
		this.ancillariesGroup = ancillariesGroup;
		
		flightDataRenderList = new ArrayList<MmbFlightDataRender>();
		for (MmbFlightData flightData : flightDataList) {
			flightDataRenderList.add(new MmbFlightDataRender(flightData));
			baggageAllowNumber = flightData.getBaggageAllow();
		}
	}
	
	public MmbAncillariesGroup getAncillaryGroup() {
		return ancillariesGroup;
	}

	public List<MmbFlightDataRender> getFlightDataRenderList() {
		return flightDataRenderList;
	}

	public MmbPriceRender getPriceRender() {
		return priceRender;
	}

	public void setPriceRender(MmbPriceRender priceRender) {
		this.priceRender = priceRender;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getBaggageAllowNumber() {
		return baggageAllowNumber;
	}
	
	public void setBaggageAllowNumber(int baggageAllowNumber) {
		this.baggageAllowNumber = baggageAllowNumber;
	}

	public String getRouteTypeLabel() {
		return routeTypeLabel;
	}

	public void setRouteTypeLabel(String routeTypeLabel) {
		this.routeTypeLabel = routeTypeLabel;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public MmbFlightDataRender getFirstFlight() {
		return firstFlight;
	}

	public void setFirstFlight(MmbFlightDataRender firstFlight) {
		this.firstFlight = firstFlight;
	}

	public MmbFlightDataRender getLastFlight() {
		return lastFlight;
	}

	public void setLastFlight(MmbFlightDataRender lastFlight) {
		this.lastFlight = lastFlight;
	}
	
	public boolean isCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(boolean currentValue) {
		this.currentValue = currentValue;
	}

	public boolean isAllowModify() {
		return allowModify;
	}

	public void setAllowModify(boolean allowModify) {
		this.allowModify = allowModify;
	}

	public Integer getBaggageWeight() {
		return baggageWeight;
	}

	public void setBaggageWeight(Integer baggageWeight) {
		this.baggageWeight = baggageWeight;
	}
	
}
