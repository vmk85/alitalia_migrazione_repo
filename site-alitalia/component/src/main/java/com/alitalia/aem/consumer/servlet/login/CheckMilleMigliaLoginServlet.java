package com.alitalia.aem.consumer.servlet.login;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.millemiglia.exception.MMCodeMismatchException;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.alitalia.aem.common.data.home.enumerations.TipoProfiloEnum;
import com.day.cq.i18n.I18n;

/**
 * Servlet exposing a REST-based service to verify the validity of the
 * provided MilleMiglia credentials.
 * 
 * <p>The expected input includes the following request parameters:
 * <ul>
 * <li>mmCode: the MilleMiglia code.</li> 
 * <li>mmPin: the MilleMiglia pin.</li> 
 * </ul>
 * </p>
 * 
 * <p>The output is a JSON object with the following attributes:
 * <ul>
 * <li>successful: true for valid credentials, false otherwise.</li>
 * <li>errorCode: code related to the failed attempt, if applicable.</li> 
 * <li>errorDescription: a description related to the failed attempt, if applicable.</li>
 * <li>mmCode: the MilleMiglia code.</li> 
 * <li>mmPin: the MilleMiglia pin.</li> 
 * </ul>
 * </p>
 */

@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkmillemiglialogin" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" })
})
@SuppressWarnings("serial")
public class CheckMilleMigliaLoginServlet extends SlingAllMethodsServlet {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private Locale locale;
	private String itemCache;
	private String languageCode;
	private String market;

	private I18n i18n;

	/**
	 * Reference to the service to remote consumer login service.
	 */
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_UNARY)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;

	@Reference
	private volatile AlitaliaConfigurationHolder configuration;

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {

		logger.debug("MilleMiglia credentials check requested.");

		String mmCode = request.getParameter("mmCode");
		if (mmCode != null){
			mmCode = mmCode.trim();
		}
		String mmPin = request.getParameter("mmPin");
		boolean captchaValidated = (Boolean)(request.getSession(true).getAttribute("captchaAlreadyValidated") != null ? request.getSession(true).getAttribute("captchaAlreadyValidated") : Boolean.FALSE);
		request.getSession().removeAttribute("captchaAlreadyValidated");


		boolean successful = false;
		boolean saUpdateDone = false;
		boolean showCaptcha = false;
		boolean isLocked = false;
		boolean isKid=false;
		String errorCode = "";
		String errorDescription = "";

		if (consumerLoginDelegate == null) {
			logger.error("Cannot check MilleMiglia login credentials, delegate not available.");
			successful = false;
			errorCode = "unknown";
			errorDescription = "Login delegate currently unavailable";

		} else if (!captchaValidated) {
			logger.error("Cannot check MilleMiglia login credentials, impossible to retrieve captcha validation from session.");
			successful = false;
			errorCode = "unknown";
			errorDescription = "Invalid Captcha";

		} else {

			logger.debug("Validating Credentials.");

			try {

				GetDatiSicurezzaProfiloRequestLocal datiSicurezzaProfiloRequest = new GetDatiSicurezzaProfiloRequestLocal();

				datiSicurezzaProfiloRequest.setIdProfilo(mmCode);
				GetDatiSicurezzaProfiloResponseLocal datiBeforeUpdateSicurezzaProfilo =
						consumerLoginDelegate.GetDatiSicurezzaProfilo(datiSicurezzaProfiloRequest);

				if (datiBeforeUpdateSicurezzaProfilo != null) {
					if(datiBeforeUpdateSicurezzaProfilo.getUsername() != null &&
							!datiBeforeUpdateSicurezzaProfilo.getUsername().isEmpty()){
						saUpdateDone = true;
					}

					TipoProfiloEnum tipoProfiloUtente = datiBeforeUpdateSicurezzaProfilo.getTipoProfilo();

					if (tipoProfiloUtente != null){
						if (tipoProfiloUtente == TipoProfiloEnum.MM_KIDS){
							isKid=true;
						}
					}
				}
				//Modifica temporanea per eliminazione di adeguamento
				saUpdateDone = false;
				//Fine modifica temporanea per eliminazione di adeguamento
				if (saUpdateDone){

					successful = false;
					errorCode = "unknown";
					errorDescription = "Login forbidden";
				} else if (isKid) {

					successful = false;
					errorCode = "unknown";
					errorDescription = "Login forbidden";

					if (locale == null){
						locale = AlitaliaUtils.findResourceLocale(request.getResource());
					}

					if (locale != null) {
						ResourceBundle resourceBundle = request.getResourceBundle(locale);
						i18n = new I18n(resourceBundle);
						itemCache = AlitaliaUtils.getLanguage(locale.toLanguageTag()).toUpperCase();
						languageCode = AlitaliaUtils.getLanguage(locale.toLanguageTag()).toUpperCase();
						market = locale.getCountry();

						errorDescription = i18n.get("specialpage.login.error.isKid");
					}



				}else{
					String tid = IDFactory.getTid();
					String sid = IDFactory.getSid(request);

					MilleMigliaLoginRequest loginRequest = new MilleMigliaLoginRequest(tid, sid);
					loginRequest.setCustomerCode(mmCode);
					loginRequest.setCustomerPin(mmPin);
					MilleMigliaLoginResponse loginResponse = consumerLoginDelegate.milleMigliaLogin(loginRequest);

					if (loginResponse.getCustomerProfileData() != null){
						String customerNumberResponse = loginResponse.getCustomerProfileData().getCustomerNumber();
						if (customerNumberResponse == null || (StringUtils.isNumeric(mmCode) && !mmCode.contains(customerNumberResponse))){
							// MM CODE MISMATCH
							MMCodeMismatchException mmCodeMismatchException = new MMCodeMismatchException("MilleMiglia Code mismatch exception");
							logger.error("MilleMiglia Code mismatch. Request Code: ["+mmCode+"], Response Code: ["+customerNumberResponse+"]", mmCodeMismatchException);
							throw mmCodeMismatchException;
						} else {
							logger.info("MilleMiglia Code match. Request Code: [{}], Response Code: [{}]", mmCode, customerNumberResponse);

//							request.getSession().setAttribute("MilleMigliaProfileDataSicurezza",datiBeforeUpdateSicurezzaProfilo);
							ProfileRequest profileRequest = new ProfileRequest();
							profileRequest.setSid(IDFactory.getSid());
							profileRequest.setTid(IDFactory.getTid());
							profileRequest.setCustomerProfile(new MMCustomerProfileData());
							profileRequest.getCustomerProfile().setCustomerNumber(mmCode);
							profileRequest.getCustomerProfile().setCustomerPinCode(mmPin);
							ProfileResponse profileResponse = consumerLoginDelegate.getProfile(profileRequest);
							request.getSession().setAttribute("MilleMigliaProfileData",profileResponse);
						}
					}

					logger.debug("SSOToken retrieved: "+loginResponse.getSsoToken());
					if (loginResponse.getSsoToken() != null && !loginResponse.getSsoToken().isEmpty()) {
						Cookie ssoCookie = new Cookie("ibeopentoken", loginResponse.getSsoToken());
						int ssoCookieExpiry = configuration.getSsoCookieTimeout();
						String ssoCookieDomain = configuration.getSsoCookieDomain();

						ssoCookie.setPath("/");
						ssoCookie.setSecure(configuration.getHttpsEnabled());
						if (ssoCookieExpiry != -1)
							ssoCookie.setMaxAge(ssoCookieExpiry);
						if (!"".equals(ssoCookieDomain))
							ssoCookie.setDomain(ssoCookieDomain);
						response.addCookie(ssoCookie);
					} else {
						logger.error("SSOToken is null or empty");
					}

					successful = loginResponse.isLoginSuccessful();

					if (!successful) {
						errorCode = loginResponse.getErrorCode();
						errorDescription = loginResponse.getErrorDescription();
						if (errorCode!=null){
							if (errorCode.equals("ErrorLoginTempLock")){
								logger.debug("ErrorLoginTempLock. isLocked true");
								isLocked = true;
							}
						}
					} else{


//					GetDatiSicurezzaProfiloRequestLocal datiSicurezzaProfiloRequest = new GetDatiSicurezzaProfiloRequestLocal();
//
//					datiSicurezzaProfiloRequest.setIdProfilo(mmCode);
//					GetDatiSicurezzaProfiloResponseLocal datiBeforeUpdateSicurezzaProfilo =
//							consumerLoginDelegate.GetDatiSicurezzaProfilo(datiSicurezzaProfiloRequest);
//
//					if (datiBeforeUpdateSicurezzaProfilo != null) {
//						if(datiBeforeUpdateSicurezzaProfilo.getUsername() != null &&
//								!datiBeforeUpdateSicurezzaProfilo.getUsername().isEmpty()){
//							saUpdateDone = true;
//						}
//					}

					}
				}


			} catch (Exception e) {
				logger.error("Error trying to check MilleMiglia login.", e);
				successful = false;
				errorCode = "unknown";
				errorDescription = "Unexpected error";
			}
		}

		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		try {
			json.object();
			json.key("successful");
			json.value(successful);
			json.key("errorCode");
			json.value(errorCode);
			json.key("errorDescription");
			json.value(errorDescription);
			json.key("mmCode");
			json.value(mmCode);
			json.key("mmPin");
			json.value(mmPin);
			json.key("showCaptcha");
			json.value(showCaptcha);
			json.key("isLocked");
			json.value(isLocked);
			json.key("saUpdateDone");
			json.value(saUpdateDone);
			json.endObject();
		} catch (Exception e) {
			logger.error("Unexected error generating JSON response.", e);
		}
	}
}
