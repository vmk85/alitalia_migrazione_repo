package com.alitalia.aem.consumer.booking.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.CrossSellingBaseData;
import com.alitalia.aem.common.data.home.CrossSellingData;
import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.web.component.managemybooking.delegate.ManageMyBookingDelegate;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingFullTextFareRules extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private ArrayList<String> fullTextFareRules;
	

	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			initBaseModel(request);
			
			fullTextFareRules = null; 
			if (ctx.fullTextFareRules != null) {
				fullTextFareRules = ctx.fullTextFareRules;
			}
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
		}
	}


	public List<String> getFullTextFareRules() {
		return fullTextFareRules;
	}
	
}