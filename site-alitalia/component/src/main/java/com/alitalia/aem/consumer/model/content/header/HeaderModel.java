package com.alitalia.aem.consumer.model.content.header;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;

@Model(adaptables = { SlingHttpServletRequest.class })
public class HeaderModel {
	
	private Logger logger = LoggerFactory.getLogger(HeaderModel.class);
	
	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	@Inject
	private Page currentPage;

	private String resourcePathLogo1;
	private String resourcePathLogo2;
	private String resourcePath;
	
	@PostConstruct
    protected void initModel() {
    	logger.debug("[HeaderModel] Initializing model");
    	
    	Resource resource = currentPage.getContentResource();
        InheritanceValueMap inheritanceValueMap =
        		new HierarchyNodeInheritanceValueMap(resource);
        
        String definedHeaderResourcePath =
        		inheritanceValueMap.getInherited("headerpath", String.class);
        if (definedHeaderResourcePath != null &&
        		!"".equals(definedHeaderResourcePath)) {
	        Page headerPage = resource.getResourceResolver()
	        		.getResource(definedHeaderResourcePath).adaptTo(Page.class);
	        if (headerPage != null) {
	        	resourcePathLogo1 = headerPage.getContentResource().getPath();
	        	resourcePath = headerPage.getContentResource().getPath();
	        }
	        
	        resourcePathLogo2 = resourcePathLogo1 + "/header/logo2";
	        resourcePathLogo1 = resourcePathLogo1 + "/header/logo1";
	        
        }
    }

	public String getResourcePathLogo1() {
		return resourcePathLogo1;
	}
	
	public String getResourcePathLogo2() {
		return resourcePathLogo2;
	}
	
	public String getResourcePath() {
		return resourcePath;
	}

}