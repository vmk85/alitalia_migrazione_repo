package com.alitalia.aem.consumer.checkinrest.render;

import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.AncillaryOffer;
import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.AncillaryOffers;
import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.FlightAncillaryOffer;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.*;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinSelectedPassengerRequest;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkinrest.render.ancillary.FlightAncillaryRender;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ggadaleta on 29/01/2018.
 */
public class AncillaryRender {

//    private List<Passenger> passengers;
    private AncillaryOffers ancillaryOffers;

    private List<FlightAncillaryRender> ancillaries;

    private boolean isFastTrackEnable;

    private boolean isLoungeEnable;

    private List<PnrRender> pnrSelectedListDataRender;

    public AncillaryRender(CheckinSessionContext ctx, AncillaryOffers inputAncillaryOffers) throws Exception {

        if (ctx == null) {
            throw new Exception("Cannot render AncillaryRender: no CheckinSessionContext");
        }
        if(inputAncillaryOffers == null) {
            throw new Exception("Cannot render AncillaryRender: AncillaryOffers response null");
        }

        this.ancillaryOffers = inputAncillaryOffers;

        if(ctx.selectedFlights != null && ctx.selectedFlights.size() > 0) {


            // Ciclo per aggiungere gli ancillari al contesto

            int x1 = 0;
            int x2 = 0;
            int x3 = 0;

            boolean _isFastTrackEnable = false;
            boolean _isLoungeEnable = false;

            for (PnrRender pnrSelectedListDataRender : ctx.pnrSelectedListDataRender)
            {
                x2 = 0;
                for (FlightsRender flight : pnrSelectedListDataRender.getFlightsRender()) {
                    x3 = 0;
                    for (SegmentRender segment : flight.getSegments()) {
                        if (segment.getPassengers() != null) {

                            int j = 0;
                            for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger pax : segment.getPassengers()) {

                                boolean selected = false;
                                for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger paxSel : ctx.checkinSelectedPassengers) {

                                    if (paxSel.getNome().equals(pax.getNome()) && paxSel.getCognome().equals(pax.getCognome()))
                                        selected = true;

                                }

                                if (selected) {
                                    for (int z = 0; z < inputAncillaryOffers.getFlightAncillaryOffers().size(); z++) {

                                        if (inputAncillaryOffers.getFlightAncillaryOffers().get(z).getLoungeEnabled())
                                            _isLoungeEnable = true;

                                        if (inputAncillaryOffers.getFlightAncillaryOffers().get(z).getFastTrackEnabled())
                                            _isFastTrackEnable = true;

                                        if (inputAncillaryOffers.getFlightAncillaryOffers().get(z).getFlightNumber().equals(segment.getFlight())) {
                                            AncillaryOffer ao;// = new AncillaryOffer();
                                            ao = inputAncillaryOffers.getFlightAncillaryOffers().get(z).getAncillaryOffers().get(j);
                                            ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengers().get(j).setOffers(ao);
                                        }
                                    }

                                    j++;
                                }

                            }

                            x3 += 1;
                        }
                    }
                    x2 += 1;
                }
                x1 += 1;
            }

            pnrSelectedListDataRender = ctx.pnrSelectedListDataRender;
            /*
            List<FlightAncillaryRender> result = new ArrayList<>();

            //todo: matching segment with flights with roundTrip (selectedFlights contains only one flight)
            //for(Flight flight : ctx.selectedFlights) {
            for(int i = 0;i<ctx.selectedFlights.size();i++) {
                List<SegmentRender> segmentRenderList = new ArrayList();
                for (SegmentRender segment : ctx.selectedFlights.get(i).getSegments()) {
                    segmentRenderList.add(segment);
                }
                FlightAncillaryRender flightAncillaryRender = new FlightAncillaryRender(inputAncillaryOffers.getFlightAncillaryOffers().get(i), segmentRenderList, ctx.checkinSelectedPassengers);
                result.add(flightAncillaryRender);
            }
            */
            //}

//            for(FlightAncillaryOffer flight : inputAncillaryOffers.getFlightAncillaryOffers()) {
//                result.add(new FlightAncillaryRender(flight,ctx.selectedFlights.get(0).getSegments(),ctx.checkinSelectedPassengers));
//                counter++;
//            }
            //this.ancillaries = result;

/*
            for (int x=0; x<result.size(); x++)
            {
                FlightAncillaryRender fr = result.get(x);
                if (fr.getFlightAncillary().getFastTrackEnabled())
                    _isFastTrackEnable = true;
                if (fr.getFlightAncillary().getLoungeEnabled())
                    _isLoungeEnable = true;
            }
*/
            this.isFastTrackEnable = _isFastTrackEnable;
            this.isLoungeEnable = _isLoungeEnable;

        }


    }

    public boolean isFastTrackEnable() {
        return isFastTrackEnable;
    }

    public boolean isLoungeEnable() {
        return isLoungeEnable;
    }

    public List<FlightAncillaryRender> getAncillaries() {
        return ancillaries;
    }

    public List<PnrRender> getPnrSelectedListDataRender() {
        return pnrSelectedListDataRender;
    }

    public AncillaryOffers getAncillaryOffers() {
        return ancillaryOffers;
    }

//    public List<FlightAncillaryRender> getAncillaries

}
