package com.alitalia.aem.consumer.checkin.exception;

public class CheckinFailedException extends Exception {

	private static final long serialVersionUID = -8388948758281010112L;

	public CheckinFailedException(String message) {
		super(message);
	}
	
	public CheckinFailedException(Throwable cause) {
		super(cause);
	}
	
}
