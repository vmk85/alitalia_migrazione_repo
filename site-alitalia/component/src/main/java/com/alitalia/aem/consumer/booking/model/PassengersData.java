package com.alitalia.aem.consumer.booking.model;

import java.util.ArrayList;
import java.util.List;

public class PassengersData {

	private List<Passenger> passengersList;
	private Contact contact1;
	private Contact contact2;
	private String email;
	
	private String arTaxInfoType;
	private String numeroCuitCuil;
	private String nazionalitaCuitCuil;
	
	public PassengersData() {
		passengersList = new ArrayList<Passenger>();
	}

	public List<Passenger> getPassengersList() {
		return passengersList;
	}

	public void setPassengersList(ArrayList<Passenger> passengersList) {
		this.passengersList = passengersList;
	}
	
	public void addPassenger(Passenger passenger) {
		this.passengersList.add(passenger);
	}
	
	public void setContact1(ContactType contactType, String internationalPrefix, String phoneNumber) {
		this.contact1 = new Contact(contactType, internationalPrefix, phoneNumber);
	}
	
	public Contact getContact1() {
		return contact1;
	}
	
	public void setContact2(ContactType contactType, String internationalPrefix, String phoneNumber) {
		this.contact2 = new Contact(contactType, internationalPrefix, phoneNumber);
	}
	
	public Contact getContact2() {
		return contact2;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}

	public String getArTaxInfoType() {
		return arTaxInfoType;
	}

	public void setArTaxInfoType(String arTaxInfoType) {
		this.arTaxInfoType = arTaxInfoType;
	}

	public String getNumeroCuitCuil() {
		return numeroCuitCuil;
	}

	public void setNumeroCuitCuil(String numeroCuitCuil) {
		this.numeroCuitCuil = numeroCuitCuil;
	}

	public String getNazionalitaCuitCuil() {
		return nazionalitaCuitCuil;
	}

	public void setNazionalitaCuitCuil(String nazionalitaCuitCuil) {
		this.nazionalitaCuitCuil = nazionalitaCuitCuil;
	}

	@Override
	public String toString() {
		return "PassengersData [passengersList=" + passengersList
				+ ", contact1=" + contact1 + ", contact2=" + contact2
				+ ", email=" + email + ", arTaxInfoType=" + arTaxInfoType
				+ ", numeroCuitCuil=" + numeroCuitCuil
				+ ", nazionalitaCuitCuil=" + nazionalitaCuitCuil + "]";
	}
	
}
