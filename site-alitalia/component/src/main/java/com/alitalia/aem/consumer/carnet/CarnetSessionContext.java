package com.alitalia.aem.consumer.carnet;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.StateData;
import com.alitalia.aem.common.data.home.carnet.CarnetBuyerData;
import com.alitalia.aem.common.data.home.carnet.CarnetData;
import com.alitalia.aem.common.data.home.carnet.CarnetInfoCarnet;
import com.alitalia.aem.common.data.home.carnet.CarnetPaymentData;
import com.alitalia.aem.common.data.home.carnet.CarnetPaymentTypeItemData;
import com.day.cq.i18n.I18n;

/**
 * The <code>CarnetSessionContext</code> class contains the state of a carnet
 * session, in order to be stored in the proper way (e.g. in HTTP session).
 * 
 * </p>It is meant to be initialized by the <code>CarnetSession</code>
 * component service, and then provided to it as requested for CarnetSession
 * process management methods.</p>
 * 
 * <p>The values managed in the context shouldn't be modified
 * externally to the <code>CarnetSession</code> component service.</p>
 */

public class CarnetSessionContext {

	public I18n i18n;
	
	public String machineName;
	
	public String sid;
	
	public String callerIp;
	
	public String serviceClient;
	
	public String market;
	
	public String site;
	
	public String currencyCode;
	
	public String currencySymbol;

	public NumberFormat currentNumberFormat;
	
	public Locale locale;
	
	public CarnetStepEnum phase;
	
	public String domain;

	public CarnetData selectedCarnet;
	
	public List<CarnetData> availableCarnet;

	public List<StateData> countriesUSA;
	
	public List<StateData> countriesCAN;

	public List<CountryData> countries;

	public List<CarnetPaymentTypeItemData> creditCards;

	public CarnetBuyerData customerInfo;

	public List<CountryData> provinces;
	
	public int numberAvailableCarnet;

	public String confirmationMailAddress;
	
	public BigDecimal carnetTotalAmount;
	
	public CarnetInfoCarnet infoCarnet;
	
	public CarnetPaymentData paymentData;
	
	public boolean enableNewsletter;
	
	public boolean isLogged;
	
	public boolean hasBeenMailSent;
	
}
