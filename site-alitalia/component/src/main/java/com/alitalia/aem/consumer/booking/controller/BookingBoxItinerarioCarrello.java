package com.alitalia.aem.consumer.booking.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.alitalia.aem.consumer.booking.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.FlightTabData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.PassengerApisSecureFlightInfoData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PassengerSecureFlightInfoData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.PaymentProviderGlobalCollectData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.common.data.home.TabData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.common.data.home.enumerations.ARTaxInfoTypesEnum;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GlobalCollectPaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingPhaseEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.model.SearchPassengersNumber;
import com.alitalia.aem.consumer.booking.render.AvailableFlightsSelectionRender;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.booking.render.GenericPriceRender;
import com.alitalia.aem.consumer.booking.render.RegoleTariffarieTrattaRender;
import com.alitalia.aem.consumer.booking.render.SearchElementDetailRender;
import com.alitalia.aem.consumer.booking.render.SearchElementRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingBoxItinerarioCarrello extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;

	@Inject
	private BookingSession bookingSession;

	@Inject
	private AlitaliaConfigurationHolder configuration;

	/* Presi da BoxBookingRiepilogoVoli */
	private BookingSearchKindEnum searchKind;
	private List<AvailableFlightsSelectionRender> flightSelectionsDetails;

	/* Presi dalla classe BoxBookingLaTuaSelezione */
	private boolean isRoundtrip;
	private boolean showFareDetails;
	private boolean showFullFareRules;
	private boolean showButton;
	private boolean openSearch;
	private boolean fareRulesStatic;
	private String pnr;
	private GenericPriceRender price;
	private GenericPriceRender ecouponPrice;
	private SearchPassengersNumber searchPassengersNumber;
	private List<SearchElementRender> searchElementRenderList;
	private String listaVoliSoggettiApprovazioneGovernativa;

	/* Presi dalla classe BookingScegliVolo */
	private SearchElement outboundSearchElement;
	private SearchElement returnSearchElement;

	/* Variabili per informazioni sui passeggeri */
	private String[] passengersInfo;
	private String[][] seatsInfo;
	private String[][] mealsInfo;
	
	//INIZIO modifica sprint 1
	private String[][] extraBagInfo;
	//FINE modifica sprint 1

	/* Variabili per informazioni sulle regole tariffarie */
	private ArrayList<RegoleTariffarieTrattaRender> fareRules;

	private String paymentType;

	private boolean isPaymentTypeCDC;

	private String formattedCardNumber;

	private String iconCreditCard;

	private String otherPaymentIconSource;

	private String[] fareForSlice;

	private boolean modifica;

	private String[] flightNumbers;

	private boolean showPassengerInfo;

	private boolean loanEnabled;

	private String bookInfoItinerary;

	private String linAirportMessage;

	private BigDecimal totalPrice;

	private GenericPriceRender priceWithFee;

	private String awardPrice;

	private String awardAdult;

	private String awardChildren;

	private GenericPriceRender cashAndMilesPrice;

	private GenericPriceRender insurancePrice;
	
	private String searchCategory;

	/* Indice tratta corrente */
	private int currentSliceIndex;

	/* Indice tratta successiva */
	private int previousSliceIndex;

	/* Numero tolate tratte */
	private int totalSlices;

	private boolean isMultileg;

	private boolean award;

	private boolean carnet;

	private boolean readyToPassengersDataPhase;

	private String urlModifica;

	private boolean openDetail;

	private int totalPax;

	private int residuoCarnet;

	private String codiceCarnet;

	private boolean readyEmettiBigliettiCarnet;

	private boolean recapPaymentToBeHidden;

	// ccFee
	private boolean ccFeeShown;

	private GenericPriceRender ccFeePrice;

	private boolean ccFeeTyp;

	private boolean firstLabel = false;

	private boolean secondLabel = false;

	private boolean genericLabel = false;

	@PostConstruct
	protected void initModel() {
		logger.debug("initModel: <<<BookingBoxItinerarioCarrello>>>");
		try {
			BigDecimal ccFeePriceAmount = new BigDecimal(0);
			Calendar departureDate = null;
			// Da verificare se l'if va bene
			initBaseModel(request);
			fareRulesStatic = configuration.isFareRulesStaticOn();
			listaVoliSoggettiApprovazioneGovernativa=configuration.getListaVoliSoggettiApprovazioneGovernativa();
			this.searchKind = ctx.searchKind;
			this.modifica = (ctx.phase == BookingPhaseEnum.FLIGHTS_SEARCH
					|| ctx.phase == BookingPhaseEnum.FLIGHTS_SELECTION);
			this.flightSelectionsDetails = new ArrayList<AvailableFlightsSelectionRender>();
			flightNumbers = new String[ctx.flightSelections.length];
			if (ctx.flightSelections != null) {
				int i = 0;
				for (FlightSelection flightSelection : ctx.flightSelections) {
					if (flightSelection != null) {

						Boolean isSoggettoAppGov=computeIsSoggettoAppGov(obtainFlightNumbers(flightSelection));
						flightSelection.getFlightData().setIsSoggettoApprovazioneGovernativa(isSoggettoAppGov);

						AvailableFlightsSelectionRender availableFlightsSelectionRender = new AvailableFlightsSelectionRender(
								flightSelection.getFlightData(), ctx.locale, i18n, ctx.isRefreshed, null, null, ctx, i);
						this.flightSelectionsDetails.add(availableFlightsSelectionRender);
						if (availableFlightsSelectionRender.getStopsNumber() > 1) {
							flightNumbers[i] = i18n.get("booking.carrello.numeriVoli.label") + " "
									+ obtainFlightNumbers(flightSelection);
						} else {
							flightNumbers[i] = i18n.get("booking.carrello.numeroVolo.label") + " "
									+ obtainFlightNumbers(flightSelection);
						}
					}
					i++;
				}
			}

			/* Codice di BookingBoxLaTuaSelezione */
			// aggiungere ctx.searchCategory in modo da nascondere il link "vedi regole tariffarie" non solo per us e canada ma anche per tutti i voli DOM e FCO_LIN
			if(ctx.searchCategory == null){
				String areaFrom=null;
				String areaTo=null;
				if (ctx.selectionRoutes != null) {
					if (ctx.selectionRoutes.getRoutesList().get(0).getFlights().get(0).getFlightType().value().equals("Connecting")) {
						int size=((ConnectingFlightData) ctx.selectionRoutes.getRoutesList().get(0).getFlights().get(0)).getFlights().size() - 1;
						areaFrom=((DirectFlightData) ((ConnectingFlightData) ctx.selectionRoutes.getRoutesList().get(0).getFlights().get(0)).getFlights().get(0)).getFrom().getArea().name();
						areaTo=((DirectFlightData) ((ConnectingFlightData) ctx.selectionRoutes.getRoutesList().get(0).getFlights().get(0)).getFlights().get(size)).getFrom().getArea().name();
					}else{
						areaFrom=(((DirectFlightData) ctx.selectionRoutes.getRoutesList().get(0).getFlights().get(0)).getFrom().getArea().name());
						areaTo=(((DirectFlightData) ctx.selectionRoutes.getRoutesList().get(0).getFlights().get(0)).getTo().getArea().name());
					}
					searchCategory = (areaTo.equals(areaFrom) && areaFrom.equals("DOM") ? "DOM" : "NOT_DOM");
				}
			}else{
				searchCategory = ctx.searchCategory.name().equals("FCO_LIN") ? "DOM" : (ctx.searchCategory.name().startsWith("DOM") ? "DOM" : "NOT_DOM");
			}
			
			
			
			
			pnr = null;
			isRoundtrip = (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP ? true : false);
			showButton = false;
			searchPassengersNumber = ctx.searchPassengersNumber;
			searchElementRenderList = new ArrayList<SearchElementRender>();
			showFareDetails = false;
			paymentType = "";
			isPaymentTypeCDC = false;
			iconCreditCard = "";
			otherPaymentIconSource = "";
			fareForSlice = null;
			isMultileg = (ctx.searchKind == BookingSearchKindEnum.MULTILEG);
			readyToPassengersDataPhase = ctx.readyToPassengersDataPhase;
			openSearch = ctx.showRoundTrip;
			award = ctx.award;
			carnet = ctx.isCarnetProcess;
			recapPaymentToBeHidden = false;

			if (carnet && ctx.phase == BookingPhaseEnum.PAYMENT) {
				readyEmettiBigliettiCarnet = true;
			} else {
				readyEmettiBigliettiCarnet = false;
			}

			/*
			 * Valorizzazione oggetti contenenti informazioni su città Andata e
			 * Ritorno
			 */
			if (ctx.searchElements != null) {
				outboundSearchElement = ctx.searchElements.get(0);
				outboundSearchElement.getFrom().setLocalizedCityName(
						i18n.get("airportsData." + outboundSearchElement.getFrom().getAirportCode() + ".name"));
				outboundSearchElement.getTo().setLocalizedCityName(
						i18n.get("airportsData." + outboundSearchElement.getTo().getAirportCode() + ".name"));
				if (isRoundtrip) {
					returnSearchElement = ctx.searchElements.get(1);
				}
				SearchElementRender searchElementRender;
				SearchElementDetailRender departureSearchElement;
				SearchElementDetailRender arrivalSearchElement;
				if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
					int i = 0;
					for (SearchElement searchElement : ctx.searchElements) {
						departureSearchElement = new SearchElementDetailRender(null,
								searchElement.getFrom().getAirportCode(), searchElement.getFrom().getCityCode(),
								searchElement.getFrom().getCityName());
						arrivalSearchElement = new SearchElementDetailRender(null,
								searchElement.getTo().getAirportCode(), searchElement.getTo().getCityCode(),
								searchElement.getTo().getCityName());
						searchElementRender = new SearchElementRender(new DateRender(searchElement.getDepartureDate()),
								departureSearchElement, arrivalSearchElement, null, ctx, i);
						searchElementRenderList.add(searchElementRender);
						i++;
					}
				}
			}

			/* Valorizzazione oggetto contenente dati sui passeggeri */
			showPassengerInfo = false;
			if (ctx.selectionRoutes != null) {
				List<PassengerBaseData> passengers = null;
				if (ctx.prenotation == null) {
					passengers = ctx.selectionRoutes.getPassengers();
				} else {
					passengers = ctx.prenotation.getPassengers();
				}
				if (passengers != null) {
					passengersInfo = new String[passengers.size()];
					seatsInfo = new String[ctx.flightSelections.length][];
					mealsInfo = new String[ctx.flightSelections.length][];
					
					// INIZIO modifica sprint 1
					extraBagInfo = new String[ctx.flightSelections.length][];
					// FINE modifica sprint 1
					
					for (int j = 0; j < ctx.flightSelections.length; j++) {
						seatsInfo[j] = new String[passengers.size()];
						mealsInfo[j] = new String[passengers.size()];
						
						// INIZIO modifica sprint 1
						extraBagInfo[j] = new String[passengers.size()];
						// FINE modifica sprint 1
						
						for(int i = 0; i < ctx.selectionRoutes.getPassengers().size(); i++) {
							passengersInfo[i] = computeInfoPassengers(passengers,i);
							seatsInfo[j][i] = computeInfoSeats(passengers,i,j);
							mealsInfo[j][i] = computeInfoMeals(passengers,i,j);
							
							// INIZIO modifica sprint 1
							extraBagInfo[j][i] = computeInfoMeals(passengers,i,j);
							// FINE modifica sprint 1
							
							if (passengersInfo[i] != null){
								showPassengerInfo = true;
							}
						}
					}
				}
			}

			if (!ctx.award) {
				/*
				 * Valorizzazione oggetto contenente informazioni sulle regole
				 * tariffarie
				 */
				showFullFareRules = false;

				if(ctx.market.equalsIgnoreCase("us") || ctx.market.equalsIgnoreCase("ca")){
					showFullFareRules = true;
				}

				this.fareRules = null;

				if (ctx.flightSelections != null) {
					this.fareRules = new ArrayList<RegoleTariffarieTrattaRender>();
					for (FlightSelection flightSelection : ctx.flightSelections) {
						if (flightSelection != null && flightSelection.getFareRules() != null) {
							this.fareRules.add(new RegoleTariffarieTrattaRender(flightSelection.getFareRules(),
									flightSelection.isTariffaLight(), i18n, ctx, flightSelection,
									flightSelection.getSelectedBrandCode()));
						}
					}
				}
				if(this.fareRules.size() > 0) {
					setNotesForPopup();
				}

				// Obtain fare for each slice
				if (ctx.searchKind != BookingSearchKindEnum.MULTILEG) {
					if (ctx.flightSelections != null) {
						fareForSlice = new String[ctx.flightSelections.length];
						for (int i = 0; i < ctx.flightSelections.length; i++) {
							if (ctx.flightSelections[i] != null) {
								BigDecimal grossFare = obtainGrossFareForSelectedFlight(ctx.flightSelections[i]);
								GenericPriceRender priceRender = new GenericPriceRender(grossFare, ctx.currency, ctx);
								fareForSlice[i] = priceRender.getFare();
							}
						}
					}
				}
				previousSliceIndex = ctx.currentSliceIndex - 1;
				currentSliceIndex = ctx.currentSliceIndex;
				totalSlices = ctx.totalSlices;
			}

			// compute the visibility of the button
			if (ctx.phase == BookingPhaseEnum.FLIGHTS_SELECTION) {
				showButton = true;
			}
			boolean initialState = ctx.newSearch;

			// Obtaining selected dates and info selected flights
			if (ctx.searchKind != BookingSearchKindEnum.MULTILEG) {
				if (initialState) { // at the first access to the page
					int i = 0;
					for (SearchElement searchElement : ctx.searchElements) {
						if (logger.isDebugEnabled()) {
							logger.debug("ctx.searchElements: " + searchElement);
						}
						SearchElementRender searchElementRender = new SearchElementRender(
								new DateRender(searchElement.getDepartureDate()), null, null, null, ctx, i);
						i++;
						if (logger.isDebugEnabled()) {
							logger.debug("searchElementRender: " + searchElementRender);
						}
						searchElementRenderList.add(searchElementRender);
					}
				} else { // after the selection
					for (int i = 0; i < ctx.flightSelections.length; i++) {
						SearchElementRender searchElementRender = null;

						logger.debug("Is meta search: " + ctx.metaSearch);

						if (!ctx.award) {
							// set the new date in case of change tab
							if (ctx.searchKind != BookingSearchKindEnum.MULTILEG && !ctx.metaSearch
									&& !ctx.isCarnetProcess) {
								TabData tab = ctx.availableFlights.getTabs().get(i);
								if (i == 0 && tab.getType() == RouteTypeEnum.RETURN) {
									tab = ctx.availableFlights.getTabs().get(1);
								}
								if (i == 1 && tab.getType() == RouteTypeEnum.OUTBOUND) {
									tab = ctx.availableFlights.getTabs().get(0);
								}
								int indexDateSelectionReturn = ctx.selectedDepartureDateChoices[i];
								departureDate = tab.getFlightTabs().get(indexDateSelectionReturn).getDate();
							} else { // multileg
								departureDate = ctx.searchElements.get(i).getDepartureDate();
							}
						}

						// show the possible selections
						if (ctx.flightSelections[i] != null) { // the flight was
																// selected
							FlightData selectedFlightData = ctx.flightSelections[i].getFlightData();
							DirectFlightData firstFlight;
							DirectFlightData lastFlight;
							if (selectedFlightData.getFlightType() == FlightTypeEnum.CONNECTING) {
								ConnectingFlightData connectingFlights = (ConnectingFlightData) selectedFlightData;
								firstFlight = (DirectFlightData) connectingFlights.getFlights().get(0);
								lastFlight = (DirectFlightData) connectingFlights.getFlights()
										.get(connectingFlights.getFlights().size() - 1);
							} else {
								DirectFlightData directFlight = (DirectFlightData) selectedFlightData;
								firstFlight = directFlight;
								lastFlight = directFlight;
							}

							SearchElementDetailRender from = new SearchElementDetailRender(
									new DateRender(firstFlight.getDepartureDate()), firstFlight.getFrom().getCode(),
									firstFlight.getFrom().getName(), firstFlight.getFrom().getCityCode(),
									i18n.get("airportsData." + firstFlight.getFrom().getCode() + ".city"));

							SearchElementDetailRender to = new SearchElementDetailRender(
									new DateRender(lastFlight.getArrivalDate()), lastFlight.getTo().getCode(),
									lastFlight.getTo().getName(), lastFlight.getTo().getCityCode(),
									i18n.get("airportsData." + lastFlight.getTo().getCode() + ".city"));

							searchElementRender = new SearchElementRender(new DateRender(departureDate), from, to,
									selectedFlightData, ctx, i);
						} else { // no flights are selected
							searchElementRender = new SearchElementRender(new DateRender(departureDate), null, null,
									null, ctx, i);
						}

						searchElementRenderList.add(searchElementRender);

					}

				}
			}

			if (ctx.searchKind != BookingSearchKindEnum.MULTILEG) {
				bookInfoItinerary = computeBookInfoItinerary(ctx);
			}

				linAirportMessage = bookingLinAirportMessage(ctx);

			// compute if ccFee has to be shown
			if (carnet) {
				ccFeeShown = false;
			} else {
				ccFeeShown = ctx.ccFeeApplied;
				if (ccFeeShown) {
					ccFeePriceAmount = ctx.ccFeeTotalAmount;
					ccFeePrice = new GenericPriceRender(ccFeePriceAmount, ctx.currency, ctx);
				}

			}

			// compute if fee is applied in typ
			if (!carnet && ctx.phase == BookingPhaseEnum.DONE) {
				ccFeeTyp = ctx.ccFeeApplied;
			} else {
				ccFeeTyp = false;
			}

			// Obtaining price
			if (ctx.readyToPassengersDataPhase) {

				if (ctx.award) {
					awardPrice = ctx.totalAwardPrice.toPlainString();
				}
				if (carnet) {
					totalPax = ctx.searchPassengersNumber.getNumAdults() + ctx.searchPassengersNumber.getNumYoung()
							+ ctx.searchPassengersNumber.getNumChildren() + ctx.searchPassengersNumber.getNumInfants();

					if (ctx.updateCarnetSuccess) {
						residuoCarnet = ctx.infoCarnet.getResidualRoutes();
					} else {
						residuoCarnet = ctx.infoCarnet.getResidualRoutes() - (ctx.totalSlices * totalPax);
					}
					codiceCarnet = ctx.infoCarnet.getCarnetCode();
				}
				obtainingPriceForSelectedFlight(ctx, ccFeePriceAmount);

			} else if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
				showSelectedMultilegFare(ctx);
			} else if (ctx.flightSelections != null) {
				int index = ctx.flightSelections[0] != null ? 0
						: (ctx.flightSelections.length == 2 && ctx.flightSelections[1] != null ? 1 : -1);
				if (index != -1) {
					BigDecimal totalFare = new BigDecimal(0);
					String selectedBrand = ctx.flightSelections[index].getSelectedBrandCode();
					BrandData brand = AlitaliaUtils.getBrandInfo(
							ctx.flightSelections[index].getFlightData().getBrands(), selectedBrand,
							configuration.getMinifareSourceBrandCode(), configuration.getMinifareTargetBrandCode());
					if (brand != null) {

						totalFare = brand.getGrossFare();
					}
					price = new GenericPriceRender(new BigDecimal(0), new BigDecimal(0), new BigDecimal(0),
							new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), totalFare, ctx.currency, ctx);
					recapPaymentToBeHidden = true;
				}
			}

			// obtain pnr
			if (ctx.phase == BookingPhaseEnum.DONE) {
				pnr = ctx.prenotation.getPnr();
			}

			// obtain payment info
			if (ctx.phase == BookingPhaseEnum.DONE) {
				if (ctx.prenotation.getPayment().getType() == PaymentTypeEnum.CREDIT_CARD
						&& ctx.prenotation.getPayment().getProvider() instanceof PaymentProviderCreditCardData) {
					isPaymentTypeCDC = true;
					PaymentProviderCreditCardData provider = (PaymentProviderCreditCardData) ctx.prenotation
							.getPayment().getProvider();
					paymentType = provider.getType().value().toLowerCase();
					String cardNumber = provider.getCreditCardNumber();
					int starSize = cardNumber.length() - 4;
					formattedCardNumber = cardNumber.substring(0, starSize).replaceAll("[0-9]", "*");
					formattedCardNumber += cardNumber.substring(starSize);
					iconCreditCard = computecreditCardIcon(provider.getType());
				} else {
					if (ctx.paymentData != null) {
						isPaymentTypeCDC = false;
						paymentType = ctx.paymentData.getType().value();
						if ("GlobalCollect".equals(paymentType)
								&& ((PaymentProviderGlobalCollectData) ctx.paymentData.getProvider())
										.getType() == GlobalCollectPaymentTypeEnum.MAESTRO)
							paymentType = ((PaymentProviderGlobalCollectData) ctx.paymentData.getProvider()).getType()
									.value();
						otherPaymentIconSource = computeOtherPaymentIconSource(paymentType);
						paymentType = paymentType.toLowerCase();
					}
				}

			}

			String market = ctx.market.toUpperCase();
			// compute if Loan is enabled for this flight
			if (totalPrice == null || departureDate == null || !market.equals(BookingConstants.LOAN_MARKET)) {
				loanEnabled = false;
			} else {

				if (totalPrice.intValue() < BookingConstants.LOAN_MINIMUM_PRICE) {

					loanEnabled = false;
				} else {
					Calendar min_day = Calendar.getInstance();
					min_day.add(Calendar.DAY_OF_YEAR, BookingConstants.LOAN_DAY_OFFSET);
					if (departureDate.compareTo(min_day) < 0) {
						loanEnabled = false;
					} else {
						loanEnabled = true;
					}
				}
			}
			openDetail = (ctx.metaSearch && ctx.phase == BookingPhaseEnum.PASSENGERS_DATA);

		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}

	}

	private Boolean computeIsSoggettoAppGov(String numeroVolo) {
		//String a=listaVoliSoggettiApprovazioneGovernativa;
		String[] arrayVolo =listaVoliSoggettiApprovazioneGovernativa.split(";");
		for (int i=0; i<arrayVolo.length; i++){
			if (arrayVolo[i].equals(numeroVolo)) {return true;}
		}
		return false;
	}

	private String computeBookInfoItinerary(BookingSessionContext ctx) {
		String info = "";
		List<FlightTabData> outboundTabList = null;
		List<FlightTabData> returnTabList = null;
		if (ctx.availableFlights != null && ctx.availableFlights.getTabs() != null) {
			for (TabData tabs : ctx.availableFlights.getTabs()) {
				if (tabs.getType() == RouteTypeEnum.OUTBOUND) {
					outboundTabList = tabs.getFlightTabs();
				} else {
					returnTabList = tabs.getFlightTabs();
				}
			}
		}

		if (outboundTabList != null) {
			FlightTabData selectedTabOutbound = outboundTabList.get(ctx.selectedDepartureDateChoices[0]);
			if ("us".equalsIgnoreCase(ctx.market) || "ca".equalsIgnoreCase(ctx.market)) {
				info = i18n.get((new DateRender(selectedTabOutbound.getDate())).getShortTextMonth());
				info += " <strong>" + (new DateRender(selectedTabOutbound.getDate())).getDay() + "</strong>";
			} else {
				info = "<strong>" + (new DateRender(selectedTabOutbound.getDate())).getDay() + "</strong>";
				info += " " + i18n.get((new DateRender(selectedTabOutbound.getDate())).getShortTextMonth());
			}
			if (returnTabList != null) {
				FlightTabData selectedTabReturn = returnTabList.get(ctx.selectedDepartureDateChoices[1]);
				if ("us".equalsIgnoreCase(ctx.market) || "ca".equalsIgnoreCase(ctx.market)) {
					info += " - " + i18n.get((new DateRender(selectedTabReturn.getDate())).getShortTextMonth());
					info += " <strong>" + (new DateRender(selectedTabReturn.getDate())).getDay() + "</strong>";
				} else {
					info += " - <strong>" + (new DateRender(selectedTabReturn.getDate())).getDay() + "</strong>";
					info += " " + i18n.get((new DateRender(selectedTabReturn.getDate())).getShortTextMonth());
				}
			}
		} else {
			if ("us".equalsIgnoreCase(ctx.market) || "ca".equalsIgnoreCase(ctx.market)) {
				info = ""
						+ i18n.get((new DateRender(ctx.searchElements.get(0).getDepartureDate())).getShortTextMonth());
				info += " <strong>" + (new DateRender(ctx.searchElements.get(0).getDepartureDate())).getDay()
						+ "</strong>";
			} else {
				info = "<strong>" + (new DateRender(ctx.searchElements.get(0).getDepartureDate())).getDay()
						+ "</strong>";
				info += " "
						+ i18n.get((new DateRender(ctx.searchElements.get(0).getDepartureDate())).getShortTextMonth());
			}
			// check if andata/return
			if (ctx.searchElements.size() > 1 && ctx.searchElements.get(1) != null) {
				if ("us".equalsIgnoreCase(ctx.market) || "ca".equalsIgnoreCase(ctx.market)) {
					info += " " + i18n
							.get((new DateRender(ctx.searchElements.get(1).getDepartureDate())).getShortTextMonth());
					info += " - <strong>" + (new DateRender(ctx.searchElements.get(1).getDepartureDate())).getDay()
							+ "</strong>";
				} else {
					info += " - <strong>" + (new DateRender(ctx.searchElements.get(1).getDepartureDate())).getDay()
							+ "</strong>";
					info += " " + i18n
							.get((new DateRender(ctx.searchElements.get(1).getDepartureDate())).getShortTextMonth());
				}

			}
		}
		return info;
	}

	// genero l'html che mostrerà l'alert nella flight-select se isLinAirportClosedMessage = true
	private String bookingLinAirportMessage(BookingSessionContext ctx) {
		String linMessage = "";
		if (isLinAirportClosedMessage(ctx)) {
			linMessage += "<div id=\"alertLinateAPT\" class=\"genericErrorMessage\">\n" +
					"\t<p class=\"genericErrorMessage__text\">\n" +
					i18n.get("booking.flightSelect.linate.chisura.label") +
					"\t</p>\n" +
					"</div>";
		}
		return linMessage;
	}

	private String obtainFlightNumbers(FlightSelection flightSelection) {
		String result = "";
		FlightData flightData = flightSelection.getFlightData();
		if (flightData instanceof DirectFlightData) {
			DirectFlightData directFlightData = (DirectFlightData) flightData;
			return directFlightData.getCarrier() + directFlightData.getFlightNumber();
		} else {
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
			for (FlightData flight : connectingFlightData.getFlights()) {
				DirectFlightData directFlightData = (DirectFlightData) flight;
				String number = directFlightData.getCarrier() + directFlightData.getFlightNumber();
				if (!result.equals("")) {
					result += ", " + number;
				} else {
					result = number;
				}
			}
		}
		return result;
	}

	private String computeInfoMeals(List<PassengerBaseData> passengers, int i, int slice) {
		PassengerBaseData passenger = passengers.get(i);
		if (passenger instanceof AdultPassengerData) {
			AdultPassengerData typedPassenger = (AdultPassengerData) passenger;
			if (typedPassenger.getPreferences() == null) {
				return "-";
			} else {
				List<DirectFlightData> directFlightsList = ctx.selectionSearchElementDirectFlights[slice];
				boolean isAZ = false;
				for (DirectFlightData directFlight : directFlightsList) {
					if (directFlight.getCarrier().equals("AZ")) {
						isAZ = true;
						break;
					}
				}
				if (typedPassenger.getPreferences().getMealType() != null) {
					if (isAZ) {
						return i18n.get("mealsData." + typedPassenger.getPreferences().getMealType().getCode());
					} else {
						return "-";
					}
				} else {
					return "-";
				}
			}
		} else if (passenger instanceof ChildPassengerData) {
			ChildPassengerData typedPassenger = (ChildPassengerData) passenger;
			if (typedPassenger.getPreferences() == null) {
				return "-";
			} else {
				List<DirectFlightData> directFlightsList = ctx.selectionSearchElementDirectFlights[slice];
				boolean isAZ = false;
				for (DirectFlightData directFlight : directFlightsList) {
					if (directFlight.getCarrier().equals("AZ")) {
						isAZ = true;
						break;
					}
				}
				if (typedPassenger.getPreferences().getMealType() != null) {
					if (isAZ) {
						return i18n.get("mealsData." + typedPassenger.getPreferences().getMealType().getCode());
					} else {
						return "-";
					}
				} else {
					return "-";
				}
			}
		}
		return "-";
	}

	private String computeInfoSeats(List<PassengerBaseData> passengers, int i, int slice) {
		PassengerBaseData passenger = passengers.get(i);
		if (passenger instanceof AdultPassengerData) {
			AdultPassengerData typedPassenger = (AdultPassengerData) passenger;
			if (typedPassenger.getPreferences() == null) {
				return "-";
			} else {

				if (typedPassenger.getPreferences().getSeatPreferences() != null
						&& !typedPassenger.getPreferences().getSeatPreferences().isEmpty()) {

					String result = "";
					List<DirectFlightData> directFlightsList = ctx.selectionSearchElementDirectFlights[slice];
					for (DirectFlightData directFlight : directFlightsList) {
						String fligthNumber = directFlight.getFlightNumber();
						for (SeatPreferencesData seatPreferenceData : typedPassenger.getPreferences()
								.getSeatPreferences()) {
							boolean hasSeat = false;
							try {
								int numberA = Integer.parseInt(seatPreferenceData.getFlightNumber());
								int numberB = Integer.parseInt(fligthNumber);
								hasSeat = numberA == numberB;
							} catch (NumberFormatException e) {
								logger.debug(
										"Error casting flight Numebr in Integer {} and {}. Now Using string for equals",
										seatPreferenceData.getFlightNumber(), fligthNumber);
								hasSeat = fligthNumber != null
										&& fligthNumber.equals(seatPreferenceData.getFlightNumber());
							}
							if (hasSeat) {
								String number = "";
								if (seatPreferenceData.getNumber() != null) {
									number = seatPreferenceData.getNumber();
								}
								if (!result.equals("")) {
									result += " / " + seatPreferenceData.getRow() + number;
								} else {
									result += seatPreferenceData.getRow() + number;
								}
							}
						}
					}
					if (result.equals("")) {
						return "-";
					} else {
						return result;
					}

				} else {
					return "-";
				}
			}
		} else if (passenger instanceof ChildPassengerData) {
			ChildPassengerData typedPassenger = (ChildPassengerData) passenger;
			if (typedPassenger.getPreferences() == null) {
				return "-";
			} else {

				if (typedPassenger.getPreferences().getSeatPreferences() != null
						&& !typedPassenger.getPreferences().getSeatPreferences().isEmpty()) {

					String result = "";
					List<DirectFlightData> directFlightsList = ctx.selectionSearchElementDirectFlights[slice];
					for (DirectFlightData directFlight : directFlightsList) {
						String fligthNumber = directFlight.getFlightNumber();
						for (SeatPreferencesData seatPreferenceData : typedPassenger.getPreferences()
								.getSeatPreferences()) {
							boolean hasSeat = false;
							try {
								int numberA = Integer.parseInt(seatPreferenceData.getFlightNumber());
								int numberB = Integer.parseInt(fligthNumber);
								hasSeat = numberA == numberB;
							} catch (NumberFormatException e) {
								logger.debug(
										"Error casting flight Numebr in Integer {} and {}. Now Using string for equals",
										seatPreferenceData.getFlightNumber(), fligthNumber);
								hasSeat = seatPreferenceData.getFlightNumber() != null
										&& seatPreferenceData.getFlightNumber().equals(fligthNumber);
							}
							if (hasSeat) {
								String number = "";
								if (seatPreferenceData.getNumber() != null) {
									number = seatPreferenceData.getNumber();
								}
								if (!result.equals("")) {
									result += " / " + seatPreferenceData.getRow() + number;
								} else {
									result += seatPreferenceData.getRow() + number;
								}
							}
						}
					}
					if (result.equals("")) {
						return "-";
					} else {
						return result;
					}

				} else {
					return "-";
				}
			}
		}
		return "-";
	}

	private String computeInfoPassengers(List<PassengerBaseData> passengers, int i) {
		PassengerBaseData passenger = passengers.get(i);
		if (passenger instanceof AdultPassengerData) {
			AdultPassengerData typedPassenger = (AdultPassengerData) passenger;
			String referedInfant = computeReferedInfant(passengers, i);
			String ticket = computeTicket(passengers, i);
			String cuit = "";
			if (passenger instanceof ApplicantPassengerData) {
				ApplicantPassengerData applicantPassenger = (ApplicantPassengerData) passenger;
				cuit = computeCuit(applicantPassenger);
			}
			if (typedPassenger.getLastName() == null) {
				return null;
			}
			String secondName = "";
			if (typedPassenger.getInfo() instanceof PassengerApisSecureFlightInfoData) {
				PassengerApisSecureFlightInfoData apisSecureFlightInfo = (PassengerApisSecureFlightInfoData) typedPassenger
						.getInfo();
				if (apisSecureFlightInfo.getSecondName() != null) {
					secondName = " " + apisSecureFlightInfo.getSecondName() + " ";
				}
			} else if (typedPassenger.getInfo() instanceof PassengerSecureFlightInfoData) {
				PassengerSecureFlightInfoData secureFlightInfo = (PassengerSecureFlightInfoData) typedPassenger
						.getInfo();
				if (secureFlightInfo.getSecondName() != null) {
					secondName = " " + secureFlightInfo.getSecondName() + " ";
				}
			}
			return "<div><strong>" + typedPassenger.getLastName() + " " + typedPassenger.getName() + secondName
					+ "</strong></div>" + cuit + ticket + referedInfant;
		} else if (passenger instanceof ChildPassengerData) {
			ChildPassengerData typedPassenger = (ChildPassengerData) passenger;
			String referedInfant = computeReferedInfant(passengers, i);
			String ticket = computeTicket(passengers, i);
			if (typedPassenger.getLastName() == null) {
				return null;
			}
			String secondName = "";
			if (typedPassenger.getInfo() instanceof PassengerApisSecureFlightInfoData) {
				PassengerApisSecureFlightInfoData apisSecureFlightInfo = (PassengerApisSecureFlightInfoData) typedPassenger
						.getInfo();
				if (apisSecureFlightInfo.getSecondName() != null) {
					secondName = " " + apisSecureFlightInfo.getSecondName() + " ";
				}
			} else if (typedPassenger.getInfo() instanceof PassengerSecureFlightInfoData) {
				PassengerSecureFlightInfoData secureFlightInfo = (PassengerSecureFlightInfoData) typedPassenger
						.getInfo();
				if (secureFlightInfo.getSecondName() != null) {
					secondName = " " + secureFlightInfo.getSecondName() + " ";
				}
			}
			return "<div><strong>" + typedPassenger.getLastName() + " " + typedPassenger.getName() + secondName
					+ "</strong></div>" + ticket + referedInfant;
		}
		return null;
	}

	private String computeCuit(ApplicantPassengerData applicantPassenger) {
		String cuit = "";
		String cuitValue = ctx.cuit;
		if (cuitValue != null) {
			switch (applicantPassenger.getArTaxInfoType()) {
			case CUIL:
				cuit = "<div class=\"ticketNumber\">" + i18n.get("booking.passengersData.cuil.label") + "<strong> "
						+ cuitValue + "<string></div>";
				break;
			case CUIT:
				cuit = "<div class=\"ticketNumber\">" + i18n.get("booking.passengersData.cuit.label") + "<strong> "
						+ cuitValue + "<string></div>";
				break;
			case NONE:
				cuit = "";
				break;
			case NOT_AR:
				cuit = "<div class=\"ticketNumber\">" + i18n.get("booking.passengersData.altraNazionalita.label")
						+ "<strong> " + i18n.get("countryData." + cuitValue) + "<string></div>";
				break;
			default:
				cuit = "";
				break;

			}
		}
		return cuit;
	}

	private String computeTicket(List<PassengerBaseData> passengers, int i) {
		String ticket = "";
		PassengerBaseData pax = passengers.get(i);
		if (pax instanceof AdultPassengerData) {
			AdultPassengerData passenger = (AdultPassengerData) pax;
			int j = 0;
			HashMap<String, Integer> ticketMap = new HashMap<String, Integer>();
			if (passenger.getTickets() != null) {
				for (TicketInfoData tkt : passenger.getTickets()) {
					Integer ticketIndex = ticketMap.get(tkt.getTicketNumber());
					if (ticketIndex == null) {
						ticketMap.put(tkt.getTicketNumber(), j);
						j++;
					}
				}
				int k = 0;
				for (String ticketNumber : ticketMap.keySet()) {
					if (k == 0) {
						ticket = "<div class=\"ticketNumber\">" + i18n.get("booking.carrello.numeroBiglietto")
								+ " <strong>" + ticketNumber + "</strong>";
					} else {
						ticket += " - " + "<strong>" + ticketNumber + "</strong>";
					}
					k++;
				}
				ticket += "</div>";
			}

		} else if (pax instanceof ChildPassengerData) {
			ChildPassengerData passenger = (ChildPassengerData) pax;
			int j = 0;
			HashMap<String, Integer> ticketMap = new HashMap<String, Integer>();
			if (passenger.getTickets() != null) {
				for (TicketInfoData tkt : passenger.getTickets()) {
					Integer ticketIndex = ticketMap.get(tkt.getTicketNumber());
					if (ticketIndex == null) {
						ticketMap.put(tkt.getTicketNumber(), j);
						j++;
					}
				}
				int k = 0;
				for (String ticketNumber : ticketMap.keySet()) {
					if (k == 0) {
						ticket = "<div class=\"ticketNumber\">" + i18n.get("booking.carrello.numeroBiglietto")
								+ " <strong>" + ticketNumber + "</strong>";
					} else {
						ticket += " - " + "<strong>" + ticketNumber + "</strong>";
					}
					k++;
				}
				ticket += "</div>";
			}

		} else if (pax instanceof InfantPassengerData) { // infant
			InfantPassengerData passenger = (InfantPassengerData) pax;
			int j = 0;
			HashMap<String, Integer> ticketMap = new HashMap<String, Integer>();
			if (passenger.getTickets() != null) {
				for (TicketInfoData tkt : passenger.getTickets()) {
					Integer ticketIndex = ticketMap.get(tkt.getTicketNumber());
					if (ticketIndex == null) {
						ticketMap.put(tkt.getTicketNumber(), j);
						j++;
					}
				}
				int k = 0;
				for (String ticketNumber : ticketMap.keySet()) {
					if (k == 0) {
						ticket = "<div class=\"ticketNumber\">" + i18n.get("booking.carrello.numeroBiglietto")
								+ " <strong>" + ticketNumber + "</strong>";
					} else {
						ticket += " - " + "<strong>" + ticketNumber + "</strong>";
					}
					k++;
				}
				ticket += "</div>";
			}
		}
		return ticket;
	}

	private String computeReferedInfant(List<PassengerBaseData> passengers, int i) {
		String infant = "";
		int indexInfant = bookingSession.computeReferedInfant(passengers, i);

		if (indexInfant > -1) {
			InfantPassengerData infantData = (InfantPassengerData) passengers.get(indexInfant);

			infant = "<strong> +" + infantData.getName() + " " + infantData.getLastName() + "</strong>";
			infant += computeTicket(passengers, indexInfant);
		}
		return infant;
	}

	private BigDecimal obtainGrossFareForSelectedFlight(FlightSelection flightSelections) {
		BrandData selectedBrand = AlitaliaUtils.getBrandInfo(flightSelections.getFlightData().getBrands(),
				flightSelections.getSelectedBrandCode(), configuration.getMinifareSourceBrandCode(),
				configuration.getMinifareTargetBrandCode());
		if (selectedBrand != null) {
			return selectedBrand.getGrossFare();
		} else {
			return new BigDecimal(0);
		}
	}

	private String computecreditCardIcon(CreditCardTypeEnum creditCardType) {
		String icon = "";
		switch (creditCardType) {
		case AMERICAN_EXPRESS:
			icon = "americanExpress";
			break;
		case DINERS:
			icon = "diners";
			break;
		case MASTER_CARD:
			icon = "masterCard";
			break;
		case UATP:
			icon = "uatp";
			break;
		case VISA:
			icon = "visa";
			break;
		case VISA_ELECTRON:
			icon = "visaElectron";
			break;
		case JCB:
			icon = "jcb";
			break;
		case MAESTRO:
			icon = "maestro";
			break;
		default:
			icon = "";
			break;
		}
		return icon;
	}

	private String computeOtherPaymentIconSource(String paymentType) {
		// String paymentIconSource =
		// "/etc/designs/alitalia/clientlibs/images/loghi/";
		String paymentIconSource = configuration.getStaticImagesPath() + "loghi/";
		switch (paymentType) {
		case "Unicredit":
			paymentIconSource = paymentIconSource + "bonifici/Pagonline.png";
			break;
		case "GlobalCollect":
			paymentIconSource = paymentIconSource + "bonifici/Sofort.png";
			break;
		case "BancaIntesa":
		case "BancoPosta":
		//case "Maestro":
			paymentIconSource = paymentIconSource + "bonifici/" + paymentType + ".png";
			break;
		default:
			paymentIconSource = paymentIconSource + "altrimetodi/" + paymentType + ".png";
		}
		return paymentIconSource;
	}

	/* private methods */

	private void showSelectedMultilegFare(BookingSessionContext ctx) {

		boolean isSelectedMultileg = false;
		BigDecimal totalFare = new BigDecimal(0);
		if (ctx.searchKind == BookingSearchKindEnum.MULTILEG && ctx.selectionTaxes == null) {
			/* Sum the fare of all previous slices */
			for (int i = ctx.currentSliceIndex - 1; i >= 0; i--) {
				if (ctx.flightSelections[i] != null) {
					String selectedBrand = ctx.flightSelections[i].getSelectedBrandCode();
					BrandData brandData = AlitaliaUtils.getBrandInfo(
							ctx.flightSelections[i].getFlightData().getBrands(), selectedBrand,
							configuration.getMinifareSourceBrandCode(), configuration.getMinifareTargetBrandCode());
					if (brandData != null) {
						totalFare = totalFare.add(brandData.getGrossFare());
						isSelectedMultileg = true;
					}
				}
			}
		}
		if (isSelectedMultileg) {
			price = new GenericPriceRender(new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), new BigDecimal(0),
					totalFare, ctx.currency, ctx);
		}
	}

	private void obtainingPriceForSelectedFlight(BookingSessionContext ctx, BigDecimal ccFeePrice) {

		// Obtaining Taxes
		BigDecimal totalFareAdult = new BigDecimal(0);
		BigDecimal totalFareChildren = new BigDecimal(0);
		BigDecimal totalFareInfant = new BigDecimal(0);
		BigDecimal totalTaxes = new BigDecimal(0);
		BigDecimal totalExtra = new BigDecimal(0);

		List<TaxData> selectionTaxes = ctx.selectionTaxes;
		if (ctx.award) {
			double adultFraction = (double) searchPassengersNumber.getNumAdults()
					/ ((double) searchPassengersNumber.getNumAdults()
							+ (double) searchPassengersNumber.getNumChildren());
			double adultQuantity = ctx.totalAwardPrice.intValue() * adultFraction;
			int totalAdult = (int) (adultQuantity);
			awardAdult = Integer.toString(totalAdult);

			if (searchPassengersNumber.getNumChildren() > 0) {
				double childrenFraction = (double) searchPassengersNumber.getNumChildren()
						/ ((double) searchPassengersNumber.getNumAdults()
								+ (double) searchPassengersNumber.getNumChildren());
				double childrenQuantity = ctx.totalAwardPrice.doubleValue() * childrenFraction;
				int totalChildren = (int) (childrenQuantity);
				awardChildren = Integer.toString(totalChildren);
			}

			if (selectionTaxes != null) {
				for (TaxData taxData : selectionTaxes) {
					if (taxData.getCode().toLowerCase().contains("taxtotal")) {
						totalTaxes = totalTaxes.add(taxData.getAmount());
					}
					if (taxData.getCode().toLowerCase().contains("yqtotal")) {
						totalExtra = totalExtra.add(taxData.getAmount());
					}
				}

			}

		} else {

			if (selectionTaxes != null) {
				for (TaxData taxData : selectionTaxes) {
					if (taxData.getCode().toLowerCase().contains("fare adult")
							|| taxData.getCode().toLowerCase().contains("fare youth")) {
						totalFareAdult = totalFareAdult.add(taxData.getAmount());
					}
					if (taxData.getCode().toLowerCase().contains("fare children")) {
						totalFareChildren = totalFareChildren.add(taxData.getAmount());
					}
					if (taxData.getCode().toLowerCase().contains("fare infant")) {
						totalFareInfant = totalFareInfant.add(taxData.getAmount());
					}
					if (taxData.getCode().toLowerCase().contains("taxtotal")) {
						totalTaxes = totalTaxes.add(taxData.getAmount());
					}
					if (taxData.getCode().toLowerCase().contains("yqtotal")) {
						totalExtra = totalExtra.add(taxData.getAmount());
					}

				}
			}
		}

		// Obtaining Extracharge
		BigDecimal totalExtraCharge = ctx.totalExtraCharges;

		// Obtaining total price
		// BigDecimal totalPrice = ctx.grossAmount;

		/*
		 * if (ctx.searchKind == BookingSearchKindEnum.MULTILEG &&
		 * ctx.selectionTaxes == null) { // check the selection for this slice
		 * if(ctx.flightSelections[ctx.currentSliceIndex - 1] != null ) { String
		 * selectedBrand = ctx.flightSelections[ctx.currentSliceIndex -
		 * 1].getSelectedBrandCode(); for (BrandData brandData :
		 * ctx.flightSelections[ctx.currentSliceIndex -
		 * 1].getFlightData().getBrands()) { if
		 * (brandData.getCode().equals(selectedBrand)) { totalFare =
		 * totalFare.add(brandData.getGrossFare()); break; } } } }
		 */

		totalPrice = new BigDecimal(0);

		totalPrice = totalPrice.add(totalFareAdult);
		totalPrice = totalPrice.add(totalFareChildren);
		totalPrice = totalPrice.add(totalFareInfant);
		totalPrice = totalPrice.add(totalTaxes);
		totalPrice = totalPrice.add(totalExtra);
		totalPrice = totalPrice.add(totalExtraCharge);

		if (ctx.coupon != null && ctx.coupon.isValid()) {
			ecouponPrice = new GenericPriceRender(ctx.totalCouponPrice, ctx.currency, ctx);
			totalPrice = totalPrice.subtract(ctx.totalCouponPrice);
		} else {
			ecouponPrice = null;
		}
		if (ctx.cashAndMiles != null) {
			cashAndMilesPrice = new GenericPriceRender(ctx.cashAndMiles.getDiscountAmount(), ctx.currency, ctx);
			totalPrice = totalPrice.subtract(ctx.cashAndMiles.getDiscountAmount());
		} else {
			cashAndMilesPrice = null;
		}
		if (ctx.insuranceProposalData != null && ctx.isInsuranceApplied) {
			insurancePrice = new GenericPriceRender(ctx.insuranceAmount, ctx.currency, ctx);
			totalPrice = totalPrice.add(ctx.insuranceAmount);
		} else {
			insurancePrice = null;
		}

		if (totalPrice.intValue() < 0)
			totalPrice = new BigDecimal(0);

		if (ctx.selectionRoutes != null) {
			if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
				if (ctx.flightSelections[ctx.currentSliceIndex - 1] != null) {
					price = new GenericPriceRender(totalFareAdult, totalFareChildren, totalFareInfant, totalTaxes,
							totalExtra, totalExtraCharge, totalPrice, ctx.currency, ctx);
					if (ccFeeShown) {

						BigDecimal totalPriceWithFee = totalPrice.add(ccFeePrice);
						priceWithFee = new GenericPriceRender(totalFareAdult, totalFareChildren, totalFareInfant,
								totalTaxes, totalExtra, totalExtraCharge, totalPriceWithFee, ctx.currency, ctx);
					} else {
						priceWithFee = price;
					}
				}
			} else {
				price = new GenericPriceRender(totalFareAdult, totalFareChildren, totalFareInfant, totalTaxes,
						totalExtra, totalExtraCharge, totalPrice, ctx.currency, ctx);
				if (ccFeeShown) {
					BigDecimal totalPriceWithFee = totalPrice.add(ccFeePrice);
					priceWithFee = new GenericPriceRender(totalFareAdult, totalFareChildren, totalFareInfant,
							totalTaxes, totalExtra, totalExtraCharge, totalPriceWithFee, ctx.currency, ctx);
				} else {
					priceWithFee = price;
				}
			}
		}
	}

	private void setNotesForPopup(){
		for(int i = 0; i < this.fareRules.size();i++) {
			Map<String, String> fareRulesSecondColumn = this.fareRules.get(i).getFareRulesSecondColumn();
			for (String a : fareRulesSecondColumn.keySet()) {
				if (fareRulesSecondColumn.get(a).contains("*")) {
					if (fareRulesSecondColumn.get(a).equals(ctx.i18n.get("fareRules.fa.change.label") + "*") || fareRulesSecondColumn.get(a).equals(ctx.i18n.get("fareRules.zz.change.label") + "*")) {
						genericLabel = true;
					} else {
						firstLabel = true;
					}
				}
				if (fareRulesSecondColumn.get(a).equals(ctx.i18n.get("fareRules.fa.change.label") + "**")) {
					secondLabel = true;
				}
			}
		}
	}

	//in caso di ricerca "MIL" e periodo chiusura aeroporto Linate, ritorno true per mostrare un alert nella flight-select (vedi bookingLinAirportMessage)
	private boolean isLinAirportClosedMessage(BookingSessionContext ctx) {
		boolean retval = false;

		String date_1 = "27-07-2019";
		String date_2 = "27-10-2019";
		long milliseconds = 0;
		long milliseconds_2 = 0;
		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		try {
			Date d_1 = f.parse(date_1);
			milliseconds = d_1.getTime();
			Date d_2 = f.parse(date_2);
			milliseconds_2 = d_2.getTime();

		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (ctx.searchElements != null) {
			int i = 0;
			for (SearchElement searchElement : ctx.searchElements) {
				i++;
				long depDate = searchElement.getDepartureDate().getTimeInMillis();
				if (("MIL".equals(searchElement.getFrom().getAirportCode()) || "MIL".equals(searchElement.getTo().getAirportCode())) && (depDate >= milliseconds) && (depDate <= milliseconds_2)) {
					return true;
				}
			}
		}
		return retval;
	}

	/* Get and set method */
	public boolean isRoundtrip() {
		return isRoundtrip;
	}

	public boolean isShowFareDetails() {
		return showFareDetails;
	}

	public boolean getModifica() {
		return modifica;
	}

	public boolean isShowButton() {
		return showButton;
	}

	public boolean showFullFareRules() {
		return showFullFareRules;
	}

	public boolean showPassengerInfo() {
		return showPassengerInfo;
	}

	public String getPnr() {
		return pnr;
	}
	public GenericPriceRender getPrice() {
		return price;
	}

	public GenericPriceRender getEcouponPrice() {
		return ecouponPrice;
	}

	public GenericPriceRender getCashAndMilesPrice() {
		return cashAndMilesPrice;
	}

	public GenericPriceRender getInsurancePrice() {
		return insurancePrice;
	}

	public SearchPassengersNumber getSearchPassengersNumber() {
		return searchPassengersNumber;
	}

	public List<SearchElementRender> getSearchElementRenderList() {
		return searchElementRenderList;
	}

	public BookingSearchKindEnum getSearchKind() {
		return searchKind;
	}

	public List<AvailableFlightsSelectionRender> getFlightSelectionsDetails() {
		return flightSelectionsDetails;
	}

	public SearchElement getOutboundSearchElement() {
		return outboundSearchElement;
	}

	public SearchElement getReturnSearchElement() {
		return returnSearchElement;
	}

	public ArrayList<RegoleTariffarieTrattaRender> getFareRules() {
		return fareRules;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public String getFormattedCardNumber() {
		return formattedCardNumber;
	}

	public String getIconCreditCard() {
		return iconCreditCard;
	}

	public String[] getFareForSlice() {
		return fareForSlice;
	}

	public String[] getFlightNumbers() {
		return flightNumbers;
	}

	public String[] getPassengersInfo() {
		return passengersInfo;
	}

	public String[][] getSeatsInfo() {
		return seatsInfo;
	}

	public String[][] getMealsInfo() {
		return mealsInfo;
	}

	// INIZIO modifica sprint 1
	public String[][] getExtraBagInfo() {
		return extraBagInfo;
	}
	// FINE modifica sprint 1
	
	public boolean getLoanEnabled() {
		return loanEnabled;
	}

	public String getBookInfoItinerary() {
		return bookInfoItinerary;
	}

	public String getLinAirportMessage() { return linAirportMessage; }

	public int getCurrentSliceIndex() {
		return currentSliceIndex;
	}

	public int getPreviousSliceIndex() {
		return previousSliceIndex;
	}

	public int getTotalSlices() {
		return totalSlices;
	}

	public boolean isMultileg() {
		return isMultileg;
	}

	public boolean isAward() {
		return award;
	}

	public boolean isCarnet() {
		return carnet;
	}

	public boolean isReadyToPassengersDataPhase() {
		return readyToPassengersDataPhase;
	}

	public String getUrlModifica() {
		return urlModifica;
	}

	public boolean isOpenSearch() {
		return openSearch;
	}

	public boolean isOpenDetail() {
		return openDetail;
	}

	public String getOtherPaymentIconSource() {
		return otherPaymentIconSource;
	}

	public boolean isPaymentTypeCDC() {
		return isPaymentTypeCDC;
	}

	public String getAwardPrice() {
		return awardPrice;
	}

	public String getAwardAdult() {
		return awardAdult;
	}

	public String getAwardChildren() {
		return awardChildren;
	}

	public String getCodiceCarnet() {
		return codiceCarnet;
	}

	public int getResiduoCarnet() {
		return residuoCarnet;
	}

	public boolean isReadyEmettiBigliettiCarnet() {
		return readyEmettiBigliettiCarnet;
	}

	public boolean isRecapPaymentToBeHidden() {
		return recapPaymentToBeHidden;
	}

	public boolean isCcFeeShown() {
		return ccFeeShown;
	}

	public GenericPriceRender getCcFeePrice() {
		return ccFeePrice;
	}

	public GenericPriceRender getPriceWithFee() {
		return priceWithFee;
	}

	public boolean isCcFeeTyp() {
		return ccFeeTyp;
	}

	public boolean isFareRulesStatic() {
		return fareRulesStatic;
	}
	
	public String getSearchCategory() { return searchCategory; }

	public boolean isFirstLabel() {
		return firstLabel;
	}

	public boolean isSecondLabel() {
		return secondLabel;
	}

	public boolean isGenericLabel() {
		return genericLabel;
	}
}
