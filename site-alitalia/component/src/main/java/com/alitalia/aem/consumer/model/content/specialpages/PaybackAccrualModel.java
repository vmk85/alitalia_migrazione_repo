package com.alitalia.aem.consumer.model.content.specialpages;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.global.enumeration.RichiediMigliaNumberCode;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class PaybackAccrualModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private PaybackAccrualData paybackAccrualData;
	private List<String> numberCodeList;


	@PostConstruct
	protected void initModel() {
		logger.debug("[PaybackAccrualModel] initModel");
		try{
		super.initBaseModel(slingHttpServletRequest);

		// retrieve and remove PaybackAccrualData from session if any

		paybackAccrualData = (PaybackAccrualData)  slingHttpServletRequest.getSession().getAttribute(PaybackAccrualData.NAME);
		slingHttpServletRequest.getSession().removeAttribute(PaybackAccrualData.NAME);
		
		numberCodeList = RichiediMigliaNumberCode.listValues();
		}
		catch(Exception e){
			logger.error("Unexpected error:", e);
		}

	}

	public PaybackAccrualData getPaybackAccrualData() {
		return paybackAccrualData;
	}
	

	public List<String> getNumberCodeList() {
		return numberCodeList;
	}

}
