package com.alitalia.aem.consumer.carnet.model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.carnet.CarnetCreditCardProviderData;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.carnet.CarnetUtils;
import com.alitalia.aem.consumer.carnet.render.CarnetGenericPriceRender;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CarnetConfirmationModel extends GenericCarnetModel {

	@Inject
	AlitaliaConfigurationHolder configurationHolder;

	@Inject
	CheckinSession session;

	@Self
	private SlingHttpServletRequest request;
	
	private String name;
	private String email;
	private String mobile;
	private String home;
	private String codiceCarnet;
	private String password;
	private String carnetLabel;
	private String creditCard;
	private String paymentTypeClass;
	private String paymentType;
	private String totalAmount;
	private String linkEmettiBiglietti;
	private boolean hasBeenMailSent;

	@PostConstruct
	protected void initModel() {
		try {
			initBaseModel(request);
			
			email = ctx.confirmationMailAddress;
			if (ctx.infoCarnet != null) {
				codiceCarnet = ctx.infoCarnet.getCarnetCode();
				password = ctx.infoCarnet.getPassword();
			}
			
			if (ctx.selectedCarnet != null) {
				carnetLabel = i18n.get("carnet.riepilogoAcquisto.carnet.label", "", ctx.selectedCarnet.getQuantity());
			}
			
			if (ctx.customerInfo!= null) {
				name = ctx.customerInfo.getLastName() + " " + ctx.customerInfo.getName();
				mobile = ctx.customerInfo.getMobileNumber();
				home = ctx.customerInfo.getTelephoneNumber();
			}
			
			CarnetCreditCardProviderData creditCardInfo = null;
			
			if (ctx.paymentData!= null) {
				if (ctx.paymentData.getProvider() instanceof CarnetCreditCardProviderData) {
					creditCardInfo = (CarnetCreditCardProviderData) ctx.paymentData.getProvider(); 
				}
				//the credit card number was hidden after payment step in CarnetSession
				creditCard = creditCardInfo.getCreditCardNumber();
				
				CarnetGenericPriceRender priceRender = new CarnetGenericPriceRender(ctx);
				totalAmount = priceRender.getFormattedPrice(ctx.carnetTotalAmount);
				paymentTypeClass = CarnetUtils.computecreditCardIcon(creditCardInfo.getType());
				paymentType = CarnetUtils.computeCreditCardName(creditCardInfo.getType());
			}
			
			String baseUrl  = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), true);
			linkEmettiBiglietti = baseUrl + getConfigurationHolder().getCarnetLoginPage() + "?codiceCarnet=" + codiceCarnet;
			
			this.hasBeenMailSent = ctx.hasBeenMailSent;
			
		} catch(Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}
	}
	
	public AlitaliaConfigurationHolder getConfigurationHolder() {
		return configurationHolder;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getMobile() {
		return mobile;
	}
	
	public String getHome() {
		return home;
	}

	public String getCodiceCarnet() {
		return codiceCarnet;
	}

	public String getPassword() {
		return password;
	}

	public String getCarnetLabel() {
		return carnetLabel;
	}

	public String getCreditCard() {
		return creditCard;
	}

	public String getPaymentTypeClass() {
		return paymentTypeClass;
	}
	
	public String getPaymenType() {
		return paymentType;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public String getLinkEmettiBiglietti() {
		return linkEmettiBiglietti;
	}

	public boolean getHasBeenMailSent() {
		return hasBeenMailSent;
	}
	
}
