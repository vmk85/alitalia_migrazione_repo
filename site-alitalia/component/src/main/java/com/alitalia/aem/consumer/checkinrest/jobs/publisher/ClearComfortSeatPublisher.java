package com.alitalia.aem.consumer.checkinrest.jobs.publisher;

import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkinrest.jobs.CheckinJobConstants;
import com.alitalia.aem.consumer.checkinrest.jobs.consumer.ClearComfortSeatJob;
import com.alitalia.aem.consumer.checkinrest.jobs.consumer.OffloadPassengerAndClearAncillariesJob;

import java.util.HashMap;
import java.util.Map;

public class ClearComfortSeatPublisher extends BaseJobPublisher {

    public ClearComfortSeatPublisher() {
        this.setJobTopic(CheckinJobConstants.TOPIC_ANCILLARIES_COMFORT_SEAT_CLEAR_AND_CHECKIN);
    }

    @Override
    public Map<String, Object> createProperties(CheckinSessionContext ctx) {

        final Map<String, Object> props = new HashMap<>();

        //set sessionId
        props.put(ClearComfortSeatJob.PROPERTY_CHECKIN_SESSION_ID, this.getSessionID());

        Boolean almenoUnAncillare = false;
        Boolean almenoUnInsurance = false;

        //TODO: refactor in caso di ancillari distribuiti su più di un PNR
        if (ctx.baggage != null) {
            if ("OK".equals(ctx.baggage.getOutcome())) {
                //c'è bagaglio
                logger.debug("Ancillary Baggage recognized {}", ctx.baggage.getOutcome());
                almenoUnAncillare = true;
            }
        }

        //TODO: refactor in caso di ancillari distribuiti su più di un PNR
        if (ctx.fastTrack != null) {
            if ("OK".equals(ctx.fastTrack.getOutcome())) {
                //c'è bagaglio
                logger.debug("Ancillary FastTrack recognized {}", ctx.fastTrack.getOutcome());
                almenoUnAncillare = true;
            }
        }

        //TODO: refactor in caso di ancillari distribuiti su più di un PNR
        if (ctx.lounge != null) {
            if ("OK".equals(ctx.lounge.getOutcome())) {
                //c'è bagaglio
                logger.debug("Ancillary Lounge recognized {}", ctx.lounge.getOutcome());
                almenoUnAncillare = true;
            }
        }

        if (ctx.comfortSeatInCart) {
            //c'è almeno un posto comfort nel carrello
            almenoUnAncillare = true;
        }

        if (almenoUnAncillare) {
            props.putAll(this.prepareClearAncillariesRequests(ClearComfortSeatJob.PROPERTY_CHECKIN_CLEARANCILLARIES, ctx));
        }

        props.putAll(this.prepareComfortSeatOffloadRequests(ClearComfortSeatJob.PROPERTY_CHECKIN_OFFLOAD, ctx));

        if (ctx.comfortSeatInCart) {
            //c'è almeno un posto comfort nel carrello
            logger.debug("Ancillary ComfortSeat recognized {}", ctx.comfortSeatInCart);
            props.putAll(this.prepareRedoUpdatePassengersRequests(ClearComfortSeatJob.PROPERTY_CHECKIN_COMFORTSEATCLEAR, ctx));
        }

        try {
            for (int pnr = 0; pnr < ctx.pnrSelectedListDataRender.size(); pnr++) {
                if (ctx.pnrSelectedListDataRender.get(pnr).getInsurance()  != null) {
                    almenoUnInsurance = ctx.pnrSelectedListDataRender.get(pnr).getInsurance();
                }
            }
        } catch (Exception e) {
            logger.info("eccezione in CheckinJobPublisher, " + e.getMessage());
            System.out.print(e.getStackTrace());
        } finally {
            if (almenoUnInsurance) {
                props.putAll(this.prepareClearInsuranceRequests(ClearComfortSeatJob.PROPERTY_CHECKIN_CLEARINSURANCE, ctx));
            }
            return props;
        }
    }
}
