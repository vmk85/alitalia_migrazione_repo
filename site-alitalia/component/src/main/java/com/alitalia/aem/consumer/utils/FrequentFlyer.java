package com.alitalia.aem.consumer.utils;

public class FrequentFlyer {
	private String airFrance;
	private String americanAirlines;
	private String britishAirways;
	private String delta;
	private String easyjet;
	private String emirates;
	private String iberia;
	private String lufthansa;
	private String meridiana;
	private String turkishAirlines;
	private String other;
	
	public FrequentFlyer(String france, String american, String british, String delta, String easyjet, String emirates, String iberia, String lufthansa, String meridiana, String turkish, String other) {
		this.airFrance = france;
		this.americanAirlines = american;
		this.britishAirways = british;
		this.delta = delta;
		this.easyjet = easyjet;
		this.emirates = emirates;
		this.iberia = iberia;
		this.lufthansa = lufthansa;
		this.meridiana = meridiana;
		this.turkishAirlines = turkish;
		this.other = other;
	}
	
	public String getAirFrance() {
		return this.airFrance;
	}
	
	public String getAmericanAirlines() {
		return this.americanAirlines;
	}
	
	public String getBritishAirways() {
		return this.britishAirways;
	}
	
	public String getDelta() {
		return this.delta;
	}
	
	public String getEasyjet() {
		return this.easyjet;
	}
	
	public String getEmirates() {
		return this.emirates;
	}
	
	public String getIberia() {
		return this.iberia;
	}
	
	public String getLufthansa() {
		return this.lufthansa;
	}
	
	public String getMeridiana() {
		return this.meridiana;
	}
	
	public String getTurkishAirlines() {
		return this.turkishAirlines;
	}
	
	public String getOther() {
		return this.other;
	}
	
	public String toString() {
		return " FrequentFlyer [Air France = " + airFrance + ", American Airlines = " + americanAirlines + ", British Airways = " + britishAirways + ", Delta = " + delta + ", Easyjet = " + easyjet + ", Emirates = " + emirates + ", Iberia = " + iberia + ", Lufthansa = " + lufthansa + ", Meridiana = " + meridiana + ", Turkish Airlines = " + turkishAirlines + ", other = " +  other + "] ";
 	}
}
