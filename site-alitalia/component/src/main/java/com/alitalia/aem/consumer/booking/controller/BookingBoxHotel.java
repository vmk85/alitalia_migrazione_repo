package com.alitalia.aem.consumer.booking.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingBoxHotel extends BookingSessionGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	private final static String EXPEDIA_DATE_FORMAT = "yyyy-MM-dd";
	
	
	
	private String expediaHotelURL;
	
	@PostConstruct
	protected void initModel() throws Exception {
		expediaHotelURL = "https://extras.expedia.com/Hotels/Delivery/EAN/EANXSell.aspx";
		String parameters = "?";
		SimpleDateFormat dateFormat = new SimpleDateFormat(EXPEDIA_DATE_FORMAT);
		String arrivaldate = dateFormat.format(Calendar.getInstance().getTime());
		String arrivalApt = "FCO";
		int numAd = 1;
		int numChild = 0;
		int numInfant = 0;
		int numYoung = 0;
		try {
			initBaseModel(request);
			
			if (ctx != null) {
				if (ctx.prenotation != null) {
					FlightData genericFlight = ctx.prenotation.getRoutesList().get(0).getFlights().get(0);
					if (genericFlight instanceof DirectFlightData) {
						arrivaldate = dateFormat
								.format(((DirectFlightData) genericFlight).getDepartureDate().getTime());
						arrivalApt = ((DirectFlightData) genericFlight).getTo().getCode();
					} else if (genericFlight instanceof ConnectingFlightData) {
						arrivaldate = dateFormat
								.format(((DirectFlightData) ((ConnectingFlightData) genericFlight)
										.getFlights().get(0)).getDepartureDate().getTime());
						int numFlight = ((ConnectingFlightData) genericFlight).getFlights().size();
						arrivalApt = ((DirectFlightData) ((ConnectingFlightData) genericFlight)
								.getFlights().get(numFlight-1)).getTo().getCode();
					}
					
					if (ctx.prenotation.getRoutesList().size() > 1) {
						String departureDate = "";
						genericFlight = ctx.prenotation.getRoutesList().get(1).getFlights().get(0);
						if (genericFlight instanceof DirectFlightData) {
							departureDate = dateFormat
									.format(((DirectFlightData) genericFlight).getDepartureDate().getTime());
						} else if (genericFlight instanceof ConnectingFlightData) {
							departureDate = dateFormat.format(
									((DirectFlightData) ((ConnectingFlightData) genericFlight).getFlights().get(0))
											.getDepartureDate().getTime());
						}
						parameters += "DEPARTUREDATE=" + departureDate + "&";
					}
				}
				if (ctx.searchPassengersNumber != null) {
					numAd = ctx.searchPassengersNumber.getNumAdults();
					numChild = ctx.searchPassengersNumber.getNumChildren();
					numInfant = ctx.searchPassengersNumber.getNumInfants();
					numYoung = ctx.searchPassengersNumber.getNumYoung();
					

				} 
			}
			parameters += "HLAID=246&";
			parameters += "TLA=" + arrivalApt + "&";
			parameters += "ARRIVALDATE=" + arrivaldate + "&";
			parameters += "NUMADULTS=" + (numAd + numYoung) + "&";
			parameters += "NUMCHILDREN=" + numChild + "&";
			parameters += "NUMTRAVELERS=" + (numAd + numChild + numInfant + numYoung) + "&";
			parameters += "TICKETTYPE=STANDARD&";
			parameters += "lang=" + languageCode + "_" + marketCode.toUpperCase();
			
			expediaHotelURL += parameters;
			
		}catch (Exception e) {
			logger.error("Unexpected error: ", e);
		}
	}

	public String getExpediaHotelURL() {
		return expediaHotelURL;
	}

	
}