package com.alitalia.aem.consumer.checkin.servlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.render.NewPassengerInfo;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checkinaddextrapassenger" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinAddExtraPassengerServlet extends GenericCheckinFormValidatorServlet {
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private CheckinSession checkinSession;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		Validator validator = new Validator();
		
		String passengerName = request.getParameter("addPassengerName");
		String passengerSurname = request.getParameter("addPassengerLastName");
		String eTicket = request.getParameter("addPassengerTicket");
		
		validator.addDirectCondition("addPassengerName", passengerName, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("addPassengerName", passengerName, 
				CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
		
		validator.addDirectCondition("addPassengerLastName", passengerSurname, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("addPassengerLastName", passengerSurname, 
				CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
		
		validator.addDirectCondition("addPassengerTicket", eTicket, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("addPassengerTicket", eTicket, 
				CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isTicketNumber");
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
		
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		String passengerName = request.getParameter("addPassengerName");
		String passengerSurname = request.getParameter("addPassengerLastName");
		String eTicket = request.getParameter("addPassengerTicket");
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		
		String errorUrl = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCheckinFailurePage();
		errorUrl = request.getResourceResolver().map(errorUrl);
		
		String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCheckinPassengersPage();
		successPage = request.getResourceResolver().map(successPage);
		
		/*NewPassengerInfo newPassengerInfo = 
				checkinSession.retriveRouteListForPassengerByEticket(
						ctx,passengerName,passengerSurname,eTicket,tid);*/
		
		boolean result = checkinSession.addExtraPassenger(
				ctx,passengerName,passengerSurname,eTicket);
		
		if (result) {
			json.key("redirect").value(successPage);
		} else {
			json.key("isError").value(true);
		}
		
		json.endObject();
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		Map<String,String> additionalParams = new HashMap<String, String>();
		return additionalParams;
	}
}
