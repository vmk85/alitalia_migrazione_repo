package com.alitalia.aem.consumer.servlet.getcountrystates;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.CountryStatesRequest;
import com.alitalia.aem.common.messages.home.CountryStatesResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.staticdata.delegaterest.StaticDataDelegateRest;
import com.day.cq.commons.TidyJSONWriter;


@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "getcountrystates" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class GetCountryStatesServlet extends SlingSafeMethodsServlet {
	
	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	//Search Country States
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegateRest staticDataDelegateRest;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		
		CountryStatesRequest countryStatesRequest = new CountryStatesRequest(IDFactory.getTid(), IDFactory.getSid(request));
		countryStatesRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
		countryStatesRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
		countryStatesRequest.setConversationID(IDFactory.getTid());
		
		CountryStatesResponse countryStatesResponse = staticDataDelegateRest.getCountryStates(countryStatesRequest);
		
		TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());
		try {
			jsonOutput.object();
			jsonOutput.key("result").value("OK");
			jsonOutput.endObject();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}



}
