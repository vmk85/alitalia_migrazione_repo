package com.alitalia.aem.consumer.model.content.myflights;

import java.util.List;

public class MyRouteRender {
	private String pnr;
	private List<MyFlightRender> outboundFlights;
	private List<MyFlightRender> returnFlights;
	
	
	public List<MyFlightRender> getOutboundFlights() {
		return outboundFlights;
	}


	public void setOutboundFlights(List<MyFlightRender> outboundFlights) {
		this.outboundFlights = outboundFlights;
	}


	public List<MyFlightRender> getReturnFlights() {
		return returnFlights;
	}


	public void setReturnFlights(List<MyFlightRender> returnFlights) {
		this.returnFlights = returnFlights;
	}


	public String getPnr() {
		return pnr;
	}


	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	
	@Override
	public String toString() {
		return "MyRouteRender [pnr=" + pnr + ", outboundFlights="
				+ outboundFlights + ", returnFlights=" + returnFlights + "]";
	}
	
	
}
