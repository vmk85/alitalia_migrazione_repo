package com.alitalia.aem.consumer.checkin.model;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinStatus;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinConfirmation extends GenericCheckinModel {

	@Self
	private SlingHttpServletRequest request;

	@Inject
	AlitaliaConfigurationHolder configurationHolder;
	
	@Inject
	private CheckinSession checkinSession;
	
	private boolean affinion;
	private int boardingPassTYP;
	private String nextFlightCheckin = null;
	private String backUrl;
	private boolean selectedRouteFromBusStation;
	private boolean selectedRouteToBusStation;
	private boolean selectedRouteFromToBusStation;
	private String[] messageFromStation;
	private String[] messageToStation;
	private String[] messageFromToStation;

	static final String MARKET_AFFLINION = "IT,GB,FR,ES";

	@PostConstruct
	protected void initModel() {

		initBaseModel(request);
		boardingPassTYP = ctx.boardingPassTYP;
		String[] markets = MARKET_AFFLINION.split(",");
		String market = ctx.market.toUpperCase();
		affinion = false;
		int k;
		for (k = 0; k < markets.length; k++) {
			if (markets[k].equals(market)) {
				affinion = true;
			}
		}

		// verify if next flight is available to check-in
		try {
			
			// re-intialize check-in session context
			List<CheckinPassengerData> passengers = CheckinUtils.getCheckedInPassengers(ctx.passengers);
			int boardingPassTYP = ctx.boardingPassTYP;
			int indexRoute = ctx.indexRoute;
			selectedRouteFromBusStation = false;
			selectedRouteToBusStation = false;
			selectedRouteFromToBusStation = false;
			
			messageFromStation = CheckinUtils.getMessageFromBusToAirport(ctx,i18n);
			selectedRouteFromBusStation = messageFromStation.length > 0;
			
			messageToStation = CheckinUtils.getMessageFromAirportToBus(ctx,i18n);
			selectedRouteToBusStation = messageToStation.length > 0;
			
			messageFromToStation = CheckinUtils.getMessageFromBusToBus(ctx,i18n);
			selectedRouteFromToBusStation = messageFromToStation.length > 0;
			
			ctx = checkinSession.initializeSession(request, ctx.i18n,
					ctx.serviceClient, ctx.machineName, ctx.sid, ctx.site,
					ctx.market, ctx.currencyCode, ctx.currencySymbol,
					ctx.locale, ctx.callerIp, ctx.tripId, ctx.passengerName,
					ctx.passengerSurname, ctx.MMcode, ctx.pin, ctx.searchType,
					ctx.currentNumberFormat, ctx.newSeats);
			ctx.acceptedPassengers = passengers;
			ctx.boardingPassTYP = boardingPassTYP;
			request.getSession(true).setAttribute(
					CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE, ctx);
			
			String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(
					request.getResource(), false);
			String flightsListUrl =
					configurationHolder.getCheckinFlightListPage();
			String passengersUrl = baseUrl + flightsListUrl.substring(
					0, flightsListUrl.length() - 5) + "." + 
					CheckinConstants.PREPARE_PASSENGERS_SERVLET_SELECTOR;
			
			int flight_number = -1;
			CheckinStatus status = null;
			for (CheckinRouteData crd : ctx.routesList) {
				if (null != (status = CheckinUtils.checkValidCheckin(
						crd, ++flight_number, passengersUrl))) {
					nextFlightCheckin = status.getUrl();
					break;
				}
			}
			
			backUrl = baseUrl + flightsListUrl.substring(
					0, flightsListUrl.length() - 5) + "." + 
					CheckinConstants.PREPARE_BOARDING_PASS_SERVLET_SELECTOR + "?"
							+ CheckinConstants.CHECKIN_SELECTED_ROUTE_PARAM
							+ "=" + indexRoute;
			
		} catch (Exception e) { }
		
	}

	public String getNextFlightCheckin() {
		return nextFlightCheckin;
	}

	public boolean isAffinion() {
		return affinion;
	}

	public int getBoardingPassTYP() {
		return boardingPassTYP;
	}

	public String getBackUrl() {
		return backUrl;
	}
	
	public boolean isSelectedRouteFromBusStation() {
		return selectedRouteFromBusStation;
	}
	
	public boolean isSelectedRouteToBusStation() {
		return selectedRouteFromToBusStation;
	}
	
	public boolean isSelectedRouteFromToBusStation() {
		return selectedRouteToBusStation;
	}
	
	public String[] getMessageFromStation(){
		return messageFromStation;
	}
	
	public String[] getMessageToStation(){
		return messageToStation;
	}
	
	public String[] getMessageFromToStation(){
		return messageFromToStation;
	}
}
