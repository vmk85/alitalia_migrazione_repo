package com.alitalia.aem.consumer.booking.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.TabData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingPhaseEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.model.SearchPassengersNumber;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.booking.render.GenericPriceRender;
import com.alitalia.aem.consumer.booking.render.SearchElementDetailRender;
import com.alitalia.aem.consumer.booking.render.SearchElementRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.LocalizationUtils;
import com.day.cq.i18n.I18n;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingBoxLaTuaSelezione extends BookingSessionGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private boolean isRoundtrip;
	private boolean showFareDetails;
	private boolean showButton;
	private String pnr;
	private GenericPriceRender price;
	private GenericPriceRender ecouponPrice;
	private SearchPassengersNumber searchPassengersNumber;
	private List<SearchElementRender> searchElementRenderList;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			
			initBaseModel(request);
			if (ctx == null) {
				//TODO: Gestire
				return;
			}
			
			ResourceBundle resourceBundle = request.getResourceBundle(
					LocalizationUtils.getLocaleByResource(request.getResource(), AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag())));
			final I18n i18n = new I18n(resourceBundle);
			
			pnr = null;
			isRoundtrip = false;
			showButton = false;
			searchPassengersNumber = ctx.searchPassengersNumber;
			searchElementRenderList = new ArrayList<SearchElementRender>();
			showFareDetails = false;
			
			//compute the visibility of the button
			if (ctx.phase == BookingPhaseEnum.FLIGHTS_SELECTION) {
				showButton = true;
			}
			boolean initialState = ctx.newSearch;
					
			//Obtaining selected dates and info selected flights
			if (initialState) { //at the first access to the page
				int i = 0;
				for (SearchElement searchElement : ctx.searchElements) {
					SearchElementRender searchElementRender = new SearchElementRender(
							new DateRender(searchElement.getDepartureDate()), null, 
							null, null, ctx, i);
					i++;
					searchElementRenderList.add(searchElementRender);
				}
			} else { //after the selection
				for (int i=0; i < ctx.flightSelections.length; i++){
					SearchElementRender searchElementRender = null;
					Calendar departureDate = null;
					
					//set the new date in case of change tab
					if (ctx.searchKind != BookingSearchKindEnum.MULTILEG) {
						TabData tab = ctx.availableFlights.getTabs().get(i);
						if (i == 0 && tab.getType() == RouteTypeEnum.RETURN) {
							tab = ctx.availableFlights.getTabs().get(1);
						}
						if (i == 1 && tab.getType() == RouteTypeEnum.OUTBOUND) {
							tab = ctx.availableFlights.getTabs().get(0);
						}
						int indexDateSelectionReturn = ctx.selectedDepartureDateChoices[i];
						departureDate = tab.getFlightTabs().get(indexDateSelectionReturn).getDate();
					} else { //multileg
						departureDate = ctx.searchElements.get(i).getDepartureDate();
					}
					
					//show the possible selections
					if (ctx.flightSelections[i] != null) { //the flight was selected
						FlightData selectedFlightData = ctx.flightSelections[i].getFlightData();
						DirectFlightData firstFlight;
						DirectFlightData lastFlight;
						if (selectedFlightData.getFlightType() == FlightTypeEnum.CONNECTING) {
							ConnectingFlightData connectingFlights = (ConnectingFlightData) selectedFlightData;
							firstFlight = (DirectFlightData) connectingFlights.getFlights().get(0);
							lastFlight = (DirectFlightData) connectingFlights.getFlights().get(connectingFlights.getFlights().size()-1);
						} else {
							DirectFlightData directFlight = (DirectFlightData) selectedFlightData;
							firstFlight = directFlight;
							lastFlight = directFlight;
						}
						
						SearchElementDetailRender from = new SearchElementDetailRender(
								new DateRender(firstFlight.getDepartureDate()),
								firstFlight.getFrom().getCode(),
								firstFlight.getFrom().getCityCode(),
								i18n.get("airportsData." + firstFlight.getFrom().getCode() + ".city"));
						
						SearchElementDetailRender to = new SearchElementDetailRender(
								new DateRender(lastFlight.getArrivalDate()),
								lastFlight.getTo().getCode(),
								lastFlight.getTo().getCityCode(),
								i18n.get("airportsData." + lastFlight.getTo().getCode() + ".city"));
						
						searchElementRender = new SearchElementRender(
								new DateRender(departureDate), from, 
								to, selectedFlightData, ctx , i);
					} else { //no flights are selected
						searchElementRender = new SearchElementRender(
								new DateRender(departureDate), null, 
								null, null, ctx, i);
					}
					
					searchElementRenderList.add(searchElementRender); 
					
				}
				
			}
			
				
			//Obtaining price
			if(ctx.readyToPassengersDataPhase){
				obtainingPriceForSelectedFlight(ctx);
			} else if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
				showSelectedMultilegFare(ctx);
			}
			
			//obtain pnr
			if(ctx.phase == BookingPhaseEnum.DONE){
				pnr = ctx.prenotation.getPnr();
			}
			
    	}catch(Exception e){
			logger.error("Unexpected error ", e);
			throw e;
    	}
    	
	}
	
	
	private void showSelectedMultilegFare(BookingSessionContext ctx) {
		
		boolean isSelectedMultileg = false;
		BigDecimal totalFare = ctx.totalExtraCharges;
		if (ctx.searchKind == BookingSearchKindEnum.MULTILEG && ctx.selectionTaxes == null) {
			// check the selection for this slice
			if(ctx.flightSelections[ctx.currentSliceIndex -1] != null ) {
				String selectedBrand = ctx.flightSelections[ctx.currentSliceIndex -1].getSelectedBrandCode();
				for (BrandData brandData : ctx.flightSelections[ctx.currentSliceIndex -1].getFlightData().getBrands()) {
					if (brandData.getCode().equals(selectedBrand)) {
						totalFare  = totalFare.add(brandData.getGrossFare());
						isSelectedMultileg = true;
						break;
					}
				}
			}
		}
		if (isSelectedMultileg) {
			price = new GenericPriceRender(new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), 
					totalFare, ctx.currency, ctx);
		}
	}


	public boolean isRoundtrip() {
		return isRoundtrip;
	}

	public boolean isShowFareDetails() {
		return showFareDetails;
	}

	public boolean isShowButton() {
		return showButton;
	}

	public String getPnr() {
		return pnr;
	}

	public GenericPriceRender getPrice() {
		return price;
	}
	
	public GenericPriceRender getEcouponPrice() {
		return ecouponPrice;
	}

	public SearchPassengersNumber getSearchPassengersNumber() {
		return searchPassengersNumber;
	}

	public List<SearchElementRender> getSearchElementRenderList() {
		return searchElementRenderList;
	}

	
	/* private methods*/
	
	private void obtainingPriceForSelectedFlight(BookingSessionContext ctx) {
		
		BigDecimal totalFare = new BigDecimal(0);
		BigDecimal totalTaxes = new BigDecimal(0);
		BigDecimal totalExtra = new BigDecimal(0);
		List<TaxData> selectionTaxes = ctx.selectionTaxes;
		if (selectionTaxes != null) {
			for (TaxData taxData : selectionTaxes) {
		    	if(taxData.getCode().toLowerCase().contains("fare")){
		    		totalFare = totalFare.add(taxData.getAmount());
		    	}
		    	if(taxData.getCode().toLowerCase().contains("taxtotal")){
		    		totalTaxes = totalTaxes.add(taxData.getAmount());
		    	}
		    	if(taxData.getCode().toLowerCase().contains("yqtotal")){
		    		totalExtra = totalExtra.add(taxData.getAmount());
		    	}

			}
			showFareDetails = true;
		}
		
		//Obtaining Extracharge
		BigDecimal totalExtraCharge = ctx.totalExtraCharges;
		totalFare = totalFare.add(totalExtraCharge);
		if (ctx.searchKind == BookingSearchKindEnum.MULTILEG && ctx.selectionTaxes == null) {
			// check the selection for this slice
			if(ctx.flightSelections[ctx.currentSliceIndex - 1] != null ) {
				String selectedBrand = ctx.flightSelections[ctx.currentSliceIndex - 1].getSelectedBrandCode();
				for (BrandData brandData : ctx.flightSelections[ctx.currentSliceIndex - 1].getFlightData().getBrands()) {
					if (brandData.getCode().equals(selectedBrand)) {
						totalFare = totalFare.add(brandData.getGrossFare());
						break;
					}
				}
			}
		}
		
		
		BigDecimal totalPrice = new BigDecimal(0);
		
		//Obtaining Ecoupon Discount is the coupon is applied
		BigDecimal couponPrice = new BigDecimal(0);
		if (ctx.coupon != null && ctx.coupon.isValid() != null) {
			if (ctx.coupon.isValid()) {
				couponPrice = ctx.totalCouponPrice;
				totalFare = ctx.netAmount;
				totalTaxes = ctx.totalTaxes;
				totalExtra = ctx.totalExtras;
				totalExtraCharge = ctx.totalExtraCharges;
				totalPrice = ctx.grossAmount;
				ecouponPrice = new GenericPriceRender(couponPrice, ctx.currency, ctx);
			} else {
				//Obtaining total price
				totalPrice = totalPrice.add(totalFare);
				totalPrice = totalPrice.add(totalTaxes);
				totalPrice = totalPrice.add(totalExtra);
				//totalPrice = totalPrice.add(totalExtraCharge);
			}
		} else {
			//Obtaining total price
			totalPrice = totalPrice.add(totalFare);
			totalPrice = totalPrice.add(totalTaxes);
			totalPrice = totalPrice.add(totalExtra);
			//totalPrice = totalPrice.add(totalExtraCharge);
		}
		
		if (ctx.selectionRoutes!=null) {
			if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
				if (ctx.flightSelections[ctx.currentSliceIndex - 1] != null) {
					price = new GenericPriceRender(totalFare, totalTaxes, totalExtra, totalExtraCharge, 
							totalPrice, ctx.currency, ctx);
				}
			} else {
				price = new GenericPriceRender(totalFare, totalTaxes, totalExtra, totalExtraCharge, 
						totalPrice, ctx.currency, ctx);
			}
		} 
	}
}