package com.alitalia.aem.consumer.myalitalia.model;

import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;


import java.util.List;

public class MyFlightsModel {

    private List<MyFlightsPnr> myFlightsPnrs;

    private List<MyFlightsTicket> myFlightsTickets;

    private PassengerSelectedCheckin passengerSelectedCheckin;

    private int totalFlightsAvailableForCheckin;

    private int totalFlightsForManage;

    public int getTotalFlightsAvailableForCheckin() {
        return totalFlightsAvailableForCheckin;
    }

    public void setTotalFlightsAvailableForCheckin(int totalFlightsAvailableForCheckin) {
        this.totalFlightsAvailableForCheckin = totalFlightsAvailableForCheckin;
    }

    public int getTotalFlightsForManage() {
        return totalFlightsForManage;
    }

    public void setTotalFlightsForManage(int totalFlightsForManage) {
        this.totalFlightsForManage = totalFlightsForManage;
    }

    public PassengerSelectedCheckin getPassengerSelectedCheckin() {
        return passengerSelectedCheckin;
    }

    public void setPassengerSelectedCheckin(PassengerSelectedCheckin passengerSelectedCheckin) {
        this.passengerSelectedCheckin = passengerSelectedCheckin;
    }

    public List<MyFlightsTicket> getMyFlightsTickets() {
        return myFlightsTickets;
    }

    public void setMyFlightsTickets(List<MyFlightsTicket> myFlightsTickets) {
        this.myFlightsTickets = myFlightsTickets;
    }

    public List<MyFlightsPnr> getMyFlightsPnrs() {
        return myFlightsPnrs;
    }

    public void setMyFlightsPnrs(List<MyFlightsPnr> myFlightsPnrs) {
        this.myFlightsPnrs = myFlightsPnrs;
    }

    @Override
    public String toString() {
        return "MyFlightsModel{" +
                "myFlightsPnrs=" + myFlightsPnrs +
                ", myFlightsTickets=" + myFlightsTickets +
                ", passengerSelectedCheckin=" + passengerSelectedCheckin +
                ", totalFlightsAvailableForCheckin=" + totalFlightsAvailableForCheckin +
                ", totalFlightsForManage=" + totalFlightsForManage +
                '}';
    }
}
