package com.alitalia.aem.consumer.checkinrest.render;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckinDateRender {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	String market;
	String language;
	String date;
	String dateFormatter;
	
	public CheckinDateRender(String date, String language, String market, String dateFormatter) {
		this.date = date;
		this.language = language;
		this.market = market;
		this.dateFormatter = dateFormatter;
	}
	
	public String getStandardDateCheckin() {
		String standardDateCheckin = "";
		
		try {
			LocalDate parsedDate = LocalDate.parse(date, DateTimeFormatter.ofPattern(dateFormatter));
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE dd MMM uuuu", new Locale(language, market.toUpperCase()));
//			logger.info("[DateRender] [getStandardDateCheckin] New Date :");
			standardDateCheckin = parsedDate.format(formatter);
			
		} catch (Exception e) {
			logger.error("Errore format data", e);
		}
		return standardDateCheckin;
	}

	public String getAmericanStandardDateCheckin() {
		String americanStandardDateCheckin = "";

		try {
			LocalDate parsedDate = LocalDate.parse(date, DateTimeFormatter.ofPattern(dateFormatter));
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE MMM dd uuuu", new Locale(language, market.toUpperCase()));
//			logger.info("[DateRender] [getStandardDateCheckin] New Date :");
			americanStandardDateCheckin = parsedDate.format(formatter);

		} catch (Exception e) {
			logger.error("Errore format data", e);
		}
		return americanStandardDateCheckin;
	}
	
	public String getStandardHoursCheckin() {
		String standardHoursCheckin = "";
		try {
			LocalDateTime parseTime = LocalDateTime.parse(date, DateTimeFormatter.ofPattern(dateFormatter));
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("H:mm", new Locale(language, market.toUpperCase()));
//			logger.info("[DateRender] [getStandardHoursCheckin] New Hours :");
			standardHoursCheckin = parseTime.format(formatter);
			
		} catch (Exception e) {
			logger.error("Errore format data", e);
		}
		return standardHoursCheckin;
	}
	

}
