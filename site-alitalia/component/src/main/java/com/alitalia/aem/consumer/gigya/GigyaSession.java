/** Componente di sessione Gigya
 * ( Viene utilizzato per inizializzare e richiamare i servizi Gigya ) [D.V.]*/

package com.alitalia.aem.consumer.gigya;

import com.alitalia.aem.consumer.gigya.logger.GigyaClientLog;
import com.alitalia.aem.consumer.gigya.logger.GigyaSessionLog;
import com.alitalia.aem.ws.gigyaService.GigyaClient;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.gigya.socialize.GSObject;
import com.gigya.socialize.GSResponse;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.SlingRequestProcessor;
import org.osgi.service.component.ComponentContext;
import org.apache.felix.scr.annotations.*;

import java.util.HashMap;

@Component(immediate = true)
@Service(value = GigyaSession.class)
public class GigyaSession {

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private RequestResponseFactory requestResponseFactory;

    @Reference
    private SlingRequestProcessor requestProcessor;

    private ComponentContext componentContext;

    @Reference
    private volatile GigyaClient gigyaClient;

    private GigyaClientLog gigyaClientLog = new GigyaClientLog();

    private GigyaSessionLog gigyaSessionLog = new GigyaSessionLog();

    /** Metodo per inizializzare la reegistrazione di un nuovo utente */
    public String initRegistration() throws Exception {

        GSResponse gsResponse = null;

        String regToken = "";

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_INIT_REGISTRATION );

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_INIT_REGISTRATION,null);

            if (gsResponse != null && gsResponse.getData() != null && gsResponse.getData().get("regToken") != null){

                regToken = gsResponse.getData().get("regToken").toString();

            } else {

                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_INIT_REGISTRATION + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());

                throw new Exception("No response token from GIGYA ");
            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_INIT_REGISTRATION + " , Exception : " + e.getMessage());

            throw e;
        }

        return regToken;
    }

    /** Metodo per controllare l'esistenza del loginId*/
    public boolean isAvailableLoginID(String email) throws Exception {

        GSResponse gsResponse = null;

        Boolean isAvailable = false;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_IS_AVAILABLE_LOGIN_ID + " , email : " + email );

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_IS_AVAILABLE_LOGIN_ID, GigyaUtilsService.prepareAvailableIdGigyaObj(email));

            if (gsResponse != null && gsResponse.getErrorCode() == 0 && gsResponse.getData() != null && gsResponse.getData().get("isAvailable") != null){

                isAvailable = Boolean.parseBoolean(gsResponse.getData().get("isAvailable").toString());

            } else {

                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_IS_AVAILABLE_LOGIN_ID + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());

                throw new Exception("No response token from GIGYA ");
            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_IS_AVAILABLE_LOGIN_ID + " , Exception : " + e.getMessage());

            throw e;
        }

        return isAvailable;

    }

    /** Metodo per effettuare il logout */
    public boolean logout(String uid) throws Exception {

        GSResponse gsResponse = null;

        Boolean isAvailable = false;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_LOGOUT + " , UID : " + uid);

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_LOGOUT, GigyaUtilsService.prepareLogoutGigyaObj(uid));

            if (gsResponse != null && gsResponse.getErrorCode() == 0){

                isAvailable = true;

            } else {

                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_LOGOUT + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());

                throw new Exception("No GIGYA Logout: " + uid);
            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_LOGOUT + " , Exception : " + e.getMessage());

            throw e;
        }

        return isAvailable;

    }

    /** Metodo per finalizzare la registrazione */
    public GSResponse finalizeRegistration(String regToken) throws Exception {

        GSResponse gsResponse = null;

        Boolean success = false;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_FINALIZE_REGISTRATION );

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_FINALIZE_REGISTRATION, GigyaUtilsService.prepareFinalizeRegistrationGigyaObj(regToken));

            if (gsResponse != null && gsResponse.getErrorCode() == 0){

                success = true;

            } else {

                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_FINALIZE_REGISTRATION + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());
            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_FINALIZE_REGISTRATION + " , Exception : " + e.getMessage());

            throw e;
        }

        return gsResponse;

    }

    /** Metodo per cancellare l'utente da gigya ( Passare lo UID ) */
    public GSResponse deleteAccount(String uid) throws Exception {

        GSResponse gsResponse = null;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_DELETEACCOUNT + " , UID : " + uid );

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_DELETEACCOUNT, GigyaUtilsService.prepareDeleteAccountGigyaObj(uid));

            if (gsResponse.getErrorCode() != 0){

                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_DELETEACCOUNT + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());
            }


        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_DELETEACCOUNT + " , Exception : " + e.getMessage());

            throw e;
        }

        return gsResponse;

    }

    /** Metodo per effettuare la reegistrazione di un nuovo utente */
    public GSResponse register(String email, String psw, String regToken, String lang, boolean auth_com, boolean auth_profile) throws Exception {

        GSResponse gsResponse = null;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_REGISTRER + " , email : " + email );

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_REGISTRER,GigyaUtilsService.prepareRegisterGigyaObj(email, psw, regToken, lang, auth_com, auth_profile));

            if (gsResponse == null && gsResponse.getData() == null && gsResponse.getData().get("regToken") == null){

                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_REGISTRER + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());

                throw new Exception("No response token from GIGYA registration ");

            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_REGISTRER + " , Exception : " + e.getMessage());

            throw e;
        }

        return gsResponse;

    }

    /** Metodo per caricare dati su gigya ( con UID ) */
    public GSResponse setAccount(String regToken, HashMap<String, String> map) throws Exception {

        GSResponse gsResponse = null;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_SET_ACCOUNT + " , UID : " + regToken);

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_SET_ACCOUNT, GigyaUtilsService.prepareSetAccountGigyaObj(regToken,map));

            if (gsResponse.getErrorCode() != 0){

                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_SET_ACCOUNT + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());

            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_SET_ACCOUNT + " , Exception : " + e.getMessage());

            throw e;
        }

        return gsResponse;

    }

    /** Metodo per caricare dati su gigya ( con regToken ) */
    public GSResponse setSocialAccount(String regToken, HashMap<String, String> map) throws Exception {

        GSResponse gsResponse = null;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_SET_ACCOUNT );

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_SET_ACCOUNT, GigyaUtilsService.prepareSetSocialAccountGigyaObj(regToken,map));

            if (gsResponse.getErrorCode() != 0){

                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_SET_ACCOUNT + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());

            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_SET_ACCOUNT + " , Exception : " + e.getMessage());

            throw e;
        }

        return gsResponse;

    }

    /** Metodo prendere i dati dell'utente da gigya ( Passare UID ) */
    public GSResponse getAccount(String regToken) throws Exception {

        GSResponse gsResponse = null;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_GET_ACCOUNT + " , UID " + regToken );

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_GET_ACCOUNT, GigyaUtilsService.prepareGetAccountGigyaObj(regToken));

            if (gsResponse.getErrorCode() != 0){

                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_GET_ACCOUNT + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());

            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_GET_ACCOUNT + " , Exception : " + e.getMessage());

            throw e;
        }

        return gsResponse;

    }

    /** Metodo prendere i dati dell'utente da gigya ( Passare regToken ) */
    public GSResponse getAccountLinkAccount(String regToken) throws Exception {

        GSResponse gsResponse = null;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_GET_ACCOUNT );

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_GET_ACCOUNT, GigyaUtilsService.prepareFinalizeRegistrationGigyaObj(regToken));

            if (gsResponse.getErrorCode() != 0){

                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_GET_ACCOUNT + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());

            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_GET_ACCOUNT + " , Exception : " + e.getMessage());

            throw e;
        }

        return gsResponse;

    }

    /** Metodo per effettuare il login su gigya ( Username o Email )*/
    public GSResponse login(String email, String psw) throws Exception {

        GSResponse gsResponse = null;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_LOGIN + " , email : " + email);

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_LOGIN, GigyaUtilsService.prepareLoginGigyaObj(email,psw));

            if (gsResponse.getErrorCode() != 0){

                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_LOGIN + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());

            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_LOGIN + " , Exception : " + e.getMessage());

            throw e;
        }

        return gsResponse;

    }

    /** Metodo per effettuare query su DB di gigya ( Costruire la query prima di interrogare il metodo) */
    public GSResponse search (String query) throws Exception {

        GSResponse gsResponse = null;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_SEARCH );

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_SEARCH, GigyaUtilsService.prepareQueryGigyaObj(query));


        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_SEARCH + " , Exception : " + e.getMessage());

            throw e;
        }

        return gsResponse;

    }

    /** Metodo per cancellare l'account social associato */
    public boolean removeConnection (String uid, String provider) throws Exception {

        boolean isError = true;

        GSResponse gsResponse = null;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_REMOVE_CONNECTION + " , UID " + uid);

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_REMOVE_CONNECTION, GigyaUtilsService.prepareRemoveConnectionGigyaObj(uid, provider));

            if (gsResponse.getErrorCode() == 0){
                isError = false;
            } else {
                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_REMOVE_CONNECTION + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());
            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_REMOVE_CONNECTION + " , Exception : " + e.getMessage());

            throw e;
        }

        return isError;
    }

    /** Metodo per reset della password ( Ivia email di reset ) */
    public boolean resetPassword (String email, String secret) throws Exception {

        boolean isError = true;

        GSResponse gsResponse = null;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_RESET_PASSWORD + " , Email : " + email );

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_RESET_PASSWORD, GigyaUtilsService.preparerESETpASSWORDGigyaObj(email, secret));

            if (gsResponse.getErrorCode() == 0){
                isError = false;
            } else {
                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_RESET_PASSWORD + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());
            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_RESET_PASSWORD + " , Exception : " + e.getMessage());

            throw e;
        }

        return isError;
    }

    /** Metodo per reset della password (  ) */
    public boolean resetPassword (String email, HashMap<String, String> map) throws Exception {

        boolean isError = true;

        GSResponse gsResponse = null;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_RESET_PASSWORD + " , Email : " + email );

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_RESET_PASSWORD, GigyaUtilsService.preparerESETpASSWORDGigyaObj(email, map));

            if (gsResponse.getErrorCode() == 0){
                isError = false;
            } else {
                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_RESET_PASSWORD + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());
            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_RESET_PASSWORD + " , Exception : " + e.getMessage());

            throw e;
        }

        return isError;
    }

    /** Metodo per reset della password ( resetta la psw ) */
    public boolean resetPassword (String psw, String pswRep, String resetToken) throws Exception {

        boolean isError = true;

        GSResponse gsResponse = null;

        try{

            gigyaSessionLog.info(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_RESET_PASSWORD );

            gsResponse = callGigya(GigyaConstans.WS_APIMETHOD_RESET_PASSWORD, GigyaUtilsService.preparerESETpASSWORDGigyaObj(psw, pswRep,resetToken));

            if (gsResponse.getErrorCode() == 0){
                isError = false;
            } else {
                gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_RESET_PASSWORD + ", codice errore " + gsResponse.getErrorCode() + " : " + gsResponse.getErrorDetails());
            }

        } catch (Exception e){

            gigyaSessionLog.error(" Chiamata a Gigya : " + GigyaConstans.WS_APIMETHOD_RESET_PASSWORD + " , Exception : " + e.getMessage());

            throw e;
        }

        return isError;
    }

    /** Metodo per effettuare le chiamate a gigya */
    private GSResponse callGigya (String method, GSObject gsObject){

        gigyaClientLog.info("Chiamata a Gigya, metodo " + method);

        return gigyaClient.execute(method,gsObject);

    }

    @Activate
    private void activate(ComponentContext componentContext) {
        this.componentContext = componentContext;
    }

    @Modified
    private void modified(ComponentContext componentContext) {
        this.componentContext = componentContext;
    }

}
