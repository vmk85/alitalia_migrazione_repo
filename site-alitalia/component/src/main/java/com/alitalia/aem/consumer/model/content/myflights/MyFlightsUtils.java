package com.alitalia.aem.consumer.model.content.myflights;

public class MyFlightsUtils {
	private static String ID_PREFIX = "Mm";
	private static int CUSTOMER_NUMBER_DIMENSION = 8;
	
	public static String getTripId(String customerNumber) {
		// if customerNumber contains too much 0's the method cut them.
		// The method uses string "Mm" as prefix
		if (customerNumber.length() > CUSTOMER_NUMBER_DIMENSION) {
			int start = customerNumber.length() - CUSTOMER_NUMBER_DIMENSION;
			String tripId = ID_PREFIX + customerNumber.substring(start);
			return tripId;
			
		}
		else {
			return ID_PREFIX + customerNumber;
		}
	}

}
