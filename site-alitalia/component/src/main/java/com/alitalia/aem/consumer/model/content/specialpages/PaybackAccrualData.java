package com.alitalia.aem.consumer.model.content.specialpages;

public class PaybackAccrualData {

	public static final String NAME = "PaybackAccrualData";

	private String error;

	private String name;
	private String surname;
	private String paybackCode;
	private String ticketCode;
	private String ticketNumber;
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPaybackCode() {
		return paybackCode;
	}
	public void setPaybackCode(String paybackCode) {
		this.paybackCode = paybackCode;
	}
	public String getTicketCode() {
		return ticketCode;
	}
	public void setTicketCode(String ticketCode) {
		this.ticketCode = ticketCode;
	}
	public String getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	@Override
	public String toString() {
		return "PaybackAccrualData [error=" + error + ", name=" + name + ", surname=" + surname + ", paybackCode="
				+ paybackCode + ", ticketCode=" + ticketCode + ", ticketNumber=" + ticketNumber + "]";
	}


}