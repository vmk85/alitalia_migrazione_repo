package com.alitalia.aem.consumer.booking;

import java.util.ArrayList;

public enum BookingSearchTypeEnum {
	BRANDSEARCH("BrandSearch"),
	BRANDSEARCH_ONEWAY("BrandSearchOneWay"),
	YOUTH("youth"),
	YOUTH_ONEWAY("youthOneWay"),
	JUMPUP("jumpup"),
	JUMPUP_ONEWAY("jumpupOneWay");
	
	private final String value;

	BookingSearchTypeEnum(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	
	public static String[] listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (BookingSearchTypeEnum c: BookingSearchTypeEnum.values()) {
			result.add(c.value);
		}
		return result.toArray(new String[0]);
	}
	

	public static BookingSearchTypeEnum fromValue(String v) {
		for (BookingSearchTypeEnum c: BookingSearchTypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
	
	public static boolean isOneWay(String value){
		return "a".equals(value);
	}
	
	public static boolean isYoung(String value){
		return value.contains("youth");
	}

	
	public static boolean isLastMinute(String value){
		return value.contains("jumpup");
	}

}
