package com.alitalia.aem.consumer.global.enumeration;

import java.util.ArrayList;

public enum CompagnieAeree {
	AMERICAN_AIRLINES("AA"),
	AIR_FRANCE_KLM("AK"),
	BRITISH_AIRWAYS("BA"),
	DELTA("DL"),
	EMIRATES("EK"),
	RYANAIR("FR"),
	IBERIA_VUELING("IB"),
	MERIDIANA("IG"),
	LUFTHANSA("LH"),
	TURKISH_AIRLINES("TK"),
	EASYJET("U2"),
	OTHER("OTHER");
	
	
	private final String value;
		
	CompagnieAeree(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	public static ArrayList<String> listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (CompagnieAeree c: CompagnieAeree.values()) {
			result.add(c.value);
		}
		return result;
	}

	public static CompagnieAeree fromValue(String v) {
		for (CompagnieAeree c: CompagnieAeree.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
