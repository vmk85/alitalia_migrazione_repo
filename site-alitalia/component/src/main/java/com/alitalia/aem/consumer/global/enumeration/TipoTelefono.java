package com.alitalia.aem.consumer.global.enumeration;

import java.util.ArrayList;

public enum TipoTelefono {
	AZIENDALE("BusinessPhone"),
	PRIVATO("HomePhone"),
	CELLULARE_AZIENDALE("MobileBusiness"),
	CELLULARE_PRIVATO("MobileHome");
	
	private final String value;

	TipoTelefono(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	public String getI18nValue() {
		return "millemiglia.tipoTelefono." + value + ".label";
	}
	
	public static ArrayList<String> listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (TipoTelefono c: TipoTelefono.values()) {
			result.add(c.value);
		}
		return result;
	}

	public static TipoTelefono fromValue(String v) {
		for (TipoTelefono c: TipoTelefono.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
