package com.alitalia.aem.consumer.mmb.render;

public class MmbSpecialAssistanceFlightRender {
	private String departureCityI18nKey;
	private String departureCityCode;
	private String departureAirportCode;
	private String arrivalCityI18nKey;
	private String arrivalCityCode;
	private String arrivalAirportCode;
	private String flightDate;
	private String flightNumber;
	
	public String getDepartureCityI18nKey() {
		return departureCityI18nKey;
	}
	public void setDepartureCityI18nKey(String departureCityI18nKey) {
		this.departureCityI18nKey = departureCityI18nKey;
	}
	public String getDepartureCityCode() {
		return departureCityCode;
	}
	public void setDepartureCityCode(String departureCityCode) {
		this.departureCityCode = departureCityCode;
	}
	public String getDepartureAirportCode() {
		return departureAirportCode;
	}
	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}
	public String getArrivalCityI18nKey() {
		return arrivalCityI18nKey;
	}
	public void setArrivalCityI18nKey(String arrivalCityI18nKey) {
		this.arrivalCityI18nKey = arrivalCityI18nKey;
	}
	public String getArrivalCityCode() {
		return arrivalCityCode;
	}
	public void setArrivalCityCode(String arrivalCityCode) {
		this.arrivalCityCode = arrivalCityCode;
	}
	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}
	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}
	
	
	@Override
	public String toString() {
		return "MmbSpecialAssistanceFlightRender [departureCityI18nKey="
				+ departureCityI18nKey + ", departureCityCode="
				+ departureCityCode + ", departureAirportCode="
				+ departureAirportCode + ", arrivalCityI18nKey="
				+ arrivalCityI18nKey + ", arrivalCityCode=" + arrivalCityCode
				+ ", arrivalAirportCode=" + arrivalAirportCode + "]";
	}
	public String getFlightDate() {
		return flightDate;
	}
	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	
	
	
	
}
