package com.alitalia.aem.consumer.model.content.specialpages;

public class ComfortSeatsClaimData {

	public static final String NAME = "ComfortSeatsClaimData";

	private String error;
	
	private String name;
	private String surname;
	private String phonenumber;
	private String email;
	private String ticketnumber;
	private String flightcarrier;
	private String flightnumber;
	private Integer day;
	private Integer month;
	private Integer year;
	private String reason;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFlightcarrier() {
		return flightcarrier;
	}

	public void setFlightcarrier(String flightcarrier) {
		this.flightcarrier = flightcarrier;
	}

	public String getFlightnumber() {
		return flightnumber;
	}

	public void setFlightnumber(String flightnumber) {
		this.flightnumber = flightnumber;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}


	public String getTicketnumber() {
		return ticketnumber;
	}

	public void setTicketnumber(String ticketnumber) {
		this.ticketnumber = ticketnumber;
	}
	
	@Override
	public String toString() {
		return "ComfortSeatsClaimData [name=" + name + ", surname=" + surname +
				", phonenumber=" + phonenumber + ", email=" + email +
				", ticketnumber="+ticketnumber+"flightcarrier=" + flightcarrier + ", flightnumber=" + flightnumber +
				", day=" + day + ", month=" + month + ", year=" + year + ", reason=" + reason + "]";
	}

}