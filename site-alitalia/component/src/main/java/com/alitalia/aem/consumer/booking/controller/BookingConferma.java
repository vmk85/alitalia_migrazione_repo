package com.alitalia.aem.consumer.booking.controller;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSearchCategoryEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.booking.render.GenericPriceRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingConferma extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;

	@Inject
	private AlitaliaConfigurationHolder configuration;

	private boolean showFareRules;
	private boolean carnet;
	private boolean showFullFareRules;
	private boolean showCallCenterMsg;
	private String pnr;
	private String mail;
	private String grossAmount;
	private String name;
	private String surname;
	private String encodedSurname;
	private String webCheckinBaseUrlMyFlight;
	private String market;
	private String locale;
	private Boolean isApis;
	private String paymentType;
	private String lastDateToPay;
	private String ticketOfficeRTEContentWithDate;
	private boolean bus;
	private String linkSSWMTO;
	private String langParam;

	private boolean award;
	private String awardPrice;

	private boolean sessionResult;

	private static final String defaultLanguage = "it";
	private static final String defaultMarket = "IT";

	private static final String ENCODING = "UTF-8";
	// ********************
	// MODIFICA DEL 03/05/2017
	// X aggiunta gestione risposta Kayak
	// INIZIO
	// ********************
	private String kayakclickid;
	// ********************
	// MODIFICA DEL 03/05/2017
	// X aggiunta gestione risposta Kayak
	// FINE
	// ********************
	
	// ********************
	// MODIFICA DEL 04/05/2017
	// X aggiunta gestione risposta Kayak
	// INIZIO
	// ********************

	private String partner_code;
	private String currency;
	private String random;

	// ********************
	// MODIFICA DEL 04/05/2017
	// X aggiunta gestione risposta Kayak
	// FINE
	
	// ********************
	// MODIFICA DEL 16/06/2017
	// X mettere l'importo formattato con il punto per i decimali
	// INIZIO
	// ********************

	private String prezzoKayak;

	// ********************
	// MODIFICA DEL 16/06/2017
	//X mettere l'importo formattato con il punto per i decimali
	// FINE

	private String srcImgKayak;

    // ********************
    // MODIFICA DEL 23/04/2018
    // Banner trenitalia
    // INIZIO
    // ********************
	private String trenitaliaQueryUrl;

	private String selectedLanguage;

    // ********************
    // MODIFICA DEL 23/04/2018
    // Banner trenitalia
    // FINE
    // ********************

	//Fix ctx in get
	private boolean secureFlightESTA;
	private String departureDate;
	private String destinationNation;
	private boolean isSingleFlight;
	//ADRWidget field
	private boolean showWidgetADR = false;
	private String dataPartenza ="";
	private String returnDate ="";

	


	public String getReturnDate() {
		return returnDate;
	}

	public String getDataPartenza() {
		return dataPartenza;
	}

	public boolean isShowWidgetADR() {
		return showWidgetADR;
	}


	public void setShowWidgetADR(boolean showWidgetADR) {
		this.showWidgetADR = showWidgetADR;
	}


	@PostConstruct
	protected void initModel() throws Exception {
		sessionResult = false;
		isSingleFlight = true;
		try {
			initBaseModel(request);

			logger.trace("BOOKINGCONFERMA----{executing initModel()}-----InitBaseModel end");
            // ********************
			// MODIFICA DEL 03/05/2017
			// X aggiunta gestione risposta Kayak
			// INIZIO
			// ********************
			String currencyCode = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencyCode", String.class);
			if(request.getSession(true).getAttribute("kayakclickid") != null ){
				kayakclickid = request.getSession(true).getAttribute("kayakclickid").toString();
				logger.info("BookingConferma - kayakclickid = " + kayakclickid);
				request.getSession(true).removeAttribute("kayakclickid");
			}
			
			// ********************
			// MODIFICA DEL 03/05/2017
			// X aggiunta gestione risposta Kayak
			// FINE
			// ********************
			// ********************
			// MODIFICA DEL 04/05/2017
			// X aggiunta gestione risposta Kayak
			// INIZIO
			// ********************

			long unixTime = System.currentTimeMillis() / 1000L;
			
			partner_code = "AZ";
			currency = ctx.currency;
			random =Long.toString(unixTime);

			// ********************
			// MODIFICA DEL 04/05/2017
			// X aggiunta gestione risposta Kayak
			// FINE			

			this.isApis = ctx.isApis;
			showFullFareRules = false;

			//eliminazione al link Vedi regole tariffarie solo per DOM & FCO-LIN ad eccezione mercati US e CA
			if((ctx.searchCategory == BookingSearchCategoryEnum.DOM) || (ctx.searchCategory == BookingSearchCategoryEnum.FCO_LIN)){
				showFullFareRules = false;
			}
			if(ctx.market.equalsIgnoreCase("us") || ctx.market.equalsIgnoreCase("ca")){
				showFullFareRules = true;
			}

			showFareRules = ctx.searchKind != BookingSearchKindEnum.MULTILEG;
			carnet = ctx.isCarnetProcess;
			bus = ctx.busIsSelected;
			String showCallCenterMsgString = request.getParameter("showCallCenterMsg");
			if (showCallCenterMsgString != null && !showCallCenterMsgString.equals("")) {
				showCallCenterMsg = Boolean.parseBoolean(showCallCenterMsgString);
			} else {
				showCallCenterMsg = false;
			}
			if (ctx.prenotation != null) {
				pnr = ctx.prenotation.getPnr();
			}
			for (PassengerBaseData passenger : ctx.selectionRoutes.getPassengers()) {
				if (passenger instanceof ApplicantPassengerData) {
					ApplicantPassengerData applicantPassengerData = (ApplicantPassengerData) passenger;
					mail = applicantPassengerData.getEmail();
					break;
				}
			}
			BigDecimal insurance;
			if (ctx.insuranceAmount == null) {
				insurance = BigDecimal.ZERO;

			} else {
				insurance = ctx.insuranceAmount;
			}

			GenericPriceRender price = new GenericPriceRender(ctx.grossAmount.add(insurance), ctx.currency, ctx);
			grossAmount = price.getFare();
			
			BigDecimal fare = ctx.grossAmount.add(insurance);
			
			DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
			otherSymbols.setDecimalSeparator('.');
			
			NumberFormat formatter = new DecimalFormat("#0.00",otherSymbols);     
			prezzoKayak =  formatter.format(fare);
			

			for (PassengerBaseData passenger : ctx.selectionRoutes.getPassengers()) {
				if (passenger instanceof ApplicantPassengerData) {
					name = passenger.getName();
					surname = passenger.getLastName();
					break;
				}
			}
			webCheckinBaseUrlMyFlight = configuration.getWebCheckinUrlMyFlight();
			market = ctx.market.toLowerCase();
			locale = ctx.locale.toString().toLowerCase();
			paymentType = ctx.prenotation.getPayment().getType().value();
			Calendar cal = Calendar.getInstance(ctx.locale);
			int dayIncrement = 1;
			if (PaymentTypeEnum.PAY_AT_TO.value().equals(paymentType)) {
				dayIncrement = configuration.getTicketOfficePaymentDayLimit();
			}
			cal.add(Calendar.DATE, dayIncrement);
			DateRender dateRender = new DateRender(cal);
			lastDateToPay = dateRender.getDay() + " " + i18n.get(dateRender.getTextMonth()) + " "
					+ dateRender.getYear();

			/*
			 * Per ticketOffice sostituisce il placeholder {} nel RTE con la
			 * data
			 */
			this.ticketOfficeRTEContentWithDate = "";
			if (PaymentTypeEnum.PAY_AT_TO.value().equals(paymentType)) {
				String rteContent = AlitaliaUtils.getComponentProperty(AlitaliaUtils.getPage(request.getResource()),
						"rich-text-ticketoffice", "text");
				if (rteContent != null) {
					String data = dateRender.getExtendedDate();
					this.ticketOfficeRTEContentWithDate = rteContent.replaceAll("\\{\\}", data);
				}
			}
			award = ctx.award;
			if (award) {
				awardPrice = ctx.totalAwardPrice.toPlainString();
			}
			linkSSWMTO = configuration.getSSWMTODeepLink();
			encodedSurname = surname != null ? URLEncoder.encode(surname, ENCODING).replace("+", "%20") : "";

			String lang = this.languageCode != null ? this.languageCode.toLowerCase() : defaultLanguage;
			String country = this.marketCode != null ? this.marketCode.toUpperCase() : defaultMarket;

            // ********************
            // MODIFICA DEL 23/04/2018
            // Banner trenitalia
            // INIZIO
            // ********************

			selectedLanguage = lang.toLowerCase();

			logger.trace("BOOKINGCONFERMA----{executing initModel()}-----Start try inside");
			try {
				if (country.equalsIgnoreCase("it") && (lang.equalsIgnoreCase("it")||lang.equalsIgnoreCase("en"))){
					//ctx.flightSelections[0].flightData.from.code
					String searchTye = ctx.searchKind.toString() ;

					if (searchTye.equalsIgnoreCase("simple") || searchTye.equalsIgnoreCase("ROUNDTRIP")){



						String querylang = lang.toLowerCase();


						Boolean trovatto = false;

						Calendar flightDate = Calendar.getInstance();

						if (searchTye.equalsIgnoreCase("simple") ){
							if (ctx.flightSelections.length==1){

								GetFlightDataForTrenitalia leggiVolo = new GetFlightDataForTrenitalia(ctx,0);

								if (leggiVolo.thereIsFCO==true){

									String tempQueryUrl = "&isRoundTrip=false&departureStation=" + leggiVolo.departureStation + "&arrivalStation=" + leggiVolo.arrivalStation
											+ "&departureDate=" + leggiVolo.formattedReferenceDate() + "&departureTime=" + leggiVolo.formattedReferenceTime() + "&noOfAdults=" + leggiVolo.formattedAduld()
											+ "&noOfChildren=" +  leggiVolo.formattedChildren() + "&lang="  + querylang + "";

									trenitaliaQueryUrl = tempQueryUrl;
								}





							}
						}else if (searchTye.equalsIgnoreCase("ROUNDTRIP") ){
							if (ctx.flightSelections.length==2){

								//è quello ce identifica tutti i parametri del querystring salvo dataritorno e oraritorno
								GetFlightDataForTrenitalia leggiVoloPrimario = null;
								//praticamente viene valorizato o comunque è interessante solo se roundtrip e ne leggerò solo data e ora
								GetFlightDataForTrenitalia leggiVoloSecondario = null;

								GetFlightDataForTrenitalia leggiVoloAndata = new GetFlightDataForTrenitalia(ctx,0);
								GetFlightDataForTrenitalia leggiVoloRitorno = new GetFlightDataForTrenitalia(ctx,1);

								boolean trovatoFCO = false;
								boolean roundtrip = false;

								if (leggiVoloAndata.thereIsFCO==true){
									leggiVoloPrimario =leggiVoloAndata;
									trovatoFCO = true;
								}

								if (leggiVoloRitorno.thereIsFCO==true){
									if (leggiVoloAndata.thereIsFCO==true) {
										leggiVoloSecondario =leggiVoloRitorno;
										roundtrip = true;
									}else{
										leggiVoloPrimario =leggiVoloRitorno;
									}

									trovatoFCO = true;
								}



								if (trovatoFCO==true){

									String tempQueryUrl = "&departureStation=" + leggiVoloPrimario.departureStation + "&arrivalStation=" + leggiVoloPrimario.arrivalStation
											+ "&departureDate=" + leggiVoloPrimario.formattedReferenceDate() + "&departureTime=" + leggiVoloPrimario.formattedReferenceTime() + "&noOfAdults=" + leggiVoloPrimario.formattedAduld()
											+ "&noOfChildren=" +  leggiVoloPrimario.formattedChildren() + "&lang="  + querylang + "";
									if(roundtrip==false) {
										tempQueryUrl = tempQueryUrl + "&isRoundTrip=false" ;
									}else{
										tempQueryUrl = tempQueryUrl + "&isRoundTrip=true"
												+ "&returnDate=" + leggiVoloSecondario.formattedReferenceDate()
												+ "&returnTime="  + leggiVoloSecondario.formattedReferenceTime();
									}

									trenitaliaQueryUrl = tempQueryUrl;
									returnDate = leggiVoloSecondario.formattedReferenceDate().replace("-", "/").substring(0, 10);
									
								}
							}
						}
					}
				}

			} catch (Exception e) {
				logger.error("Unexpected error try inside: "+ e);

			}
			logger.trace("BOOKINGCONFERMA----{executing initModel()}-----End try inside");

            // ********************
            // MODIFICA DEL 23/04/2018
            // Banner trenitalia
            // INIZIO
            // ********************

			langParam = lang + "_" + country;
			Map<String, String> sswLang = configuration
					.getMapProperty(AlitaliaConfigurationHolder.SSW_LANGUAGE_MAPPING);
			if (sswLang.get(lang) != null) {
				langParam = sswLang.get(lang);
			} else if (configuration.getSetProperty(AlitaliaConfigurationHolder.SSW_NOT_SUPPORTED_LANGUAGES)
					.contains(lang)) {
				lang = configuration.getStringProperty(AlitaliaConfigurationHolder.SSW_DEFAULT_LANGUAGE);
				country = configuration.getStringProperty(AlitaliaConfigurationHolder.SSW_DEFAULT_COUNTRY);
				langParam = lang + "_" + country;
			}

			if ((kayakclickid!=null)&&(!(kayakclickid.isEmpty()))){
				kayakclickid=URLEncoder.encode(kayakclickid, "UTF-8");
				srcImgKayak="https://www.kayak.com/s/kayakpixel/confirm/"+partner_code+"?kayakclickid="+kayakclickid+"&price="+prezzoKayak+"&currency="+currency+"&confirmation="+pnr+"&rand"+random;
				logger.info("BookingConferma - srcImgKayak = " + srcImgKayak);
			}
			logger.trace("BOOKINGCONFERMA----{executing initModel()}-----Start block fix ctx in get");
			// Fix ctx in get
			if(ctx!=null) {
				secureFlightESTA = ctx.isSecureFlightESTA;
				logger.trace("BOOKINGCONFERMA----{executing initModel()}-----secureFlightESTA Execute");
			}

			retriveDepartureDateDestinationNation();

			logger.trace("BOOKINGCONFERMA----{executing initModel()}-----End block fix ctx in get");
			sessionResult =true;
		} catch (Exception e) {
			logger.error("Unexpected error try outside: "+ e);
			sessionResult = false;
			//throw e;
		}
		// Mostra Widget-Alitalia se Roundtrip e partenza da"FCO" 
		if(ctx.flightSelections[0].getFlightData() instanceof  DirectFlightData){

            if(((DirectFlightData) ctx.flightSelections[0].getFlightData()).getFrom().getCode().equalsIgnoreCase("FCO") && ctx.searchKind.toString().equalsIgnoreCase("ROUNDTRIP")){
                showWidgetADR = true;
            }

        }else if (ctx.flightSelections[0].getFlightData() instanceof  ConnectingFlightData){

		    if(((DirectFlightData) ((ConnectingFlightData) ctx.flightSelections[0].getFlightData()).getFlights().get(0)).getFrom().getCode().equalsIgnoreCase("FCO") && ctx.searchKind.toString().equalsIgnoreCase("ROUNDTRIP")){
		    	logger.debug("MArket Code"+ getMarketCode()+"MArket"+getMarket());
                showWidgetADR = true;
            }
        }
		// Fine mostra Widget-Alitalia
        String dateTemp = departureDate.split("T")[0];
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("dd-MM-yyyy");
        Date date = format1.parse(dateTemp);
        dataPartenza = format2.format(date).replace("-","/");
	}


	public boolean isSessionResult() {
		return sessionResult;
	}

	public boolean getShowFareRules() {
		return showFareRules;
	}

	public boolean getShowFullFareRules() {
		return showFullFareRules;
	}

	public boolean getShowCallCenterMsg() {
		return showCallCenterMsg;
	}

	public String getPnr() {
		return pnr;
	}

	public String getMail() {
		return mail;
	}

	public String getGrossAmount() {
		return grossAmount;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getWebCheckinBaseUrlMyFlight() {
		return webCheckinBaseUrlMyFlight;
	}

	public String getMarket() {
		return market;
	}

	public String getLocale() {
		return locale;
	}

	public Boolean getIsApis() {
		return isApis;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public String getLastDateToPay() {
		return lastDateToPay;
	}

	public boolean isAward() {
		return award;
	}

	public boolean isCarnet() {
		return carnet;
	}

	public String getAwardPrice() {
		return awardPrice;
	}

	public boolean isSecureFlightESTA() {
		return secureFlightESTA;
	}

	public boolean isBus() {
		return bus;
	}

	public String getTicketOfficeRTEContentWithDate() {
		return ticketOfficeRTEContentWithDate;
	}

	public String getLinkSSWMTO() {
		return linkSSWMTO;
	}

	public String getLangParam() {
		return langParam;
	}

	public String getEncodedSurname() {
		return encodedSurname;
	}

	// ********************
	// MODIFICA DEL 03/05/2017
	// X aggiunta gestione risposta Kayak
	// INIZIO
	// ********************

	public String getKayakClickId() {
		return kayakclickid;
	}

	// ********************
	// MODIFICA DEL 03/05/2017
	// X aggiunta gestione risposta Kayak
	// FINE
	// ********************

	// ********************
	// MODIFICA DEL 04/05/2017
	// X aggiunta gestione risposta Kayak
	// INIZIO
	// ********************

	public String getPartner_code() {
		return partner_code;
	}

	public String getCurrency() {
		return currency;
	}

	public String getRandom() {
		return random;
	}

	// ********************
	// MODIFICA DEL 04/05/2017
	// X aggiunta gestione risposta Kayak
	// FINE
	// ********************
	
	
	// ********************
	// MODIFICA DEL 16/06/2017
	// X mettere l'importo formattato con il punto per i decimali
	// INIZIO
	// ********************

	public String getPrezzoKayak() { return prezzoKayak; }

	// ********************
	// MODIFICA DEL 16/06/2017
	//X mettere l'importo formattato con il punto per i decimali
	// FINE


	public String getSrcImgKayak() { return srcImgKayak; }

    // ********************
    // MODIFICA DEL 23/04/2018
    // Banner trenitalia
    // INIZIO
    // ********************

	public String getTrenitaliaQueryUrl() { return trenitaliaQueryUrl; }
	public String getSelectedLanguage() { return selectedLanguage; }

	public String getDepartureDate() {
		return departureDate;
	}
	public String getDestinationNation() {
		return destinationNation;
	}
	public boolean isSingleFlight() {
		return isSingleFlight;
	}
	public void setSingleFlight(boolean isSingleFlight) {
		this.isSingleFlight = isSingleFlight;
	}


	private class GetFlightDataForTrenitalia {
        /*com.alitalia.aem.consumer.booking.BookingSessionContext peppe;*/

        public boolean thereIsFCO = false;
        public  Calendar referenceDate = Calendar.getInstance();
        //public  int referenceTime = 0;
        public  AreaValueEnum flightArea = AreaValueEnum.DOM;
        public String departureStation = "";
        public String arrivalStation = "";
        public int aduld = 0;
        public int children = 0;


        private int flightIndex;
        private com.alitalia.aem.consumer.booking.BookingSessionContext bookingContext;





        public String formattedReferenceTime() {

            return String.valueOf(referenceDate.get(Calendar.HOUR_OF_DAY));
        }
		public int referenceTime() {

			return referenceDate.get(Calendar.HOUR_OF_DAY);
		}

        public String formattedAduld() {

            return String.valueOf(this.aduld);
        }

        public String formattedChildren() {

            return String.valueOf(this.children);
        }

        public String formatDate(Calendar date) {
			java.util.Date dateInDateForm = (java.util.Date)date.getTime();
            SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
            String formattedDate = format1.format(dateInDateForm);

            return String.valueOf(formattedDate);
        }

        public Calendar recalculateReferenceDate(Calendar date , boolean isFromFco,
                                                 AreaValueEnum recalculteflightArea) {
            Calendar retDate = (Calendar)date.clone();
			if (isFromFco==true){
				switch (recalculteflightArea) {
					case DOM:  retDate.add(Calendar.HOUR_OF_DAY, -2);
						break;
					case INTZ:  retDate.add(Calendar.HOUR_OF_DAY, -3);
						break;
					case INTC:  retDate.add(Calendar.HOUR_OF_DAY, -4);
						break;
				}
			}else{
				retDate.add(Calendar.HOUR_OF_DAY, 2);
			}
            //qui i calcoli se aggiungere (in caso di arrifo a fco)o togliere (in caso di partenza da fco)
            //anche in base a internazionsle , domestico o intercontinentale (se si parteda fco togliere più ore se internazionale o intercontinentale)
            //retDate.add(Calendar.HOUR_OF_DAY, 1); // adds one hour


            return retDate;
        }

        public String formattedReferenceDate() {

            return formatDate(referenceDate);
        }



        public GetFlightDataForTrenitalia( com.alitalia.aem.consumer.booking.BookingSessionContext bookingContext, int flightIndex) {

            this.bookingContext = bookingContext;
            this.flightIndex = flightIndex;
            init();
        }


        private void init() {
            com.alitalia.aem.common.data.home.FlightData flightData= bookingContext.flightSelections[flightIndex].getFlightData();

            DirectFlightData flifhtInfoFrom = null;
            DirectFlightData flifhtInfoTo = null;

            boolean isFromFco = true;

            if (flightData.getFlightType() == FlightTypeEnum.DIRECT) {

                thereIsFCO = false;
                DirectFlightData flightInfo = (DirectFlightData)bookingContext.flightSelections[flightIndex].getFlightData();

                flightArea = AreaValueEnum.DOM;

                if (flightInfo.getFrom().getArea() == AreaValueEnum.INTZ|| flightInfo.getTo().getArea() == AreaValueEnum.INTZ){
                    flightArea = AreaValueEnum.INTZ;
                }

                if (flightInfo.getFrom().getArea() == AreaValueEnum.INTC || flightInfo.getTo().getArea() == AreaValueEnum.INTC){
                    flightArea = AreaValueEnum.INTC;
                }

                flifhtInfoFrom = flightInfo;
                flifhtInfoTo = flightInfo;
            }else{
                //ora CONNECTING

                //com.alitalia.aem.common.data.home.FlightData flightInfo = bookingContext.flightSelections[flightIndex].getFlightData();
                ConnectingFlightData connectingFlightData = (ConnectingFlightData)bookingContext.flightSelections[flightIndex].getFlightData();

                //DirectFlightData connectFlightInfoFrom = (DirectFlightData)flightInfo.getFlights().get(0);
                //DirectFlightData connectFlightInfoTo = (DirectFlightData)flightInfo.getFlights().get(flightInfo.getFlights().size()-1);

                flightArea = AreaValueEnum.DOM;



				for (int i = 0; i < connectingFlightData.getFlights().size(); i++){

					DirectFlightData directFlightData = (DirectFlightData)connectingFlightData.getFlights().get(i);
					if (directFlightData.getFrom().getArea() == AreaValueEnum.INTZ|| directFlightData.getTo().getArea() == AreaValueEnum.INTZ){
						flightArea = AreaValueEnum.INTZ;
					}

					if (directFlightData.getFrom().getArea() == AreaValueEnum.INTC || directFlightData.getTo().getArea() == AreaValueEnum.INTC){
						flightArea = AreaValueEnum.INTC;
						//appena trovo un intercontinentale (che è la misura più grande possibile)
						//esco
						break;
					}
				}
				/*
                for (com.alitalia.aem.common.data.home.FlightData connectFlightData : connectingFlightData.getFlights()) {
                	DirectFlightData pippo = (DirectFlightData)connectingFlightData.getFlights().get(0);
                    DirectFlightData directFlightData = (DirectFlightData) flightData;
                    if (directFlightData.getFrom().getArea() == AreaValueEnum.INTZ|| directFlightData.getTo().getArea() == AreaValueEnum.INTZ){
                        flightArea = AreaValueEnum.INTZ;
                    }

                    if (directFlightData.getFrom().getArea() == AreaValueEnum.INTC || directFlightData.getTo().getArea() == AreaValueEnum.INTC){
                        flightArea = AreaValueEnum.INTC;
                        //appena trovo un intercontinentale (che è la misura più grande possibile)
                        //esco
                        break;
                    }
                }
                */

                ArrayList<com.alitalia.aem.common.data.home.FlightData> itinerario = connectingFlightData.getFlights();
                flifhtInfoFrom = (DirectFlightData)itinerario.get(0);
                flifhtInfoTo = (DirectFlightData)itinerario.get(itinerario.size()-1);

            }

            if (flifhtInfoFrom.getFrom().getCode().equalsIgnoreCase("FCO") || flifhtInfoTo.getTo().getCode().equalsIgnoreCase("FCO") ){

                thereIsFCO = true;

                if (flifhtInfoFrom.getFrom().getCode().equalsIgnoreCase("FCO") ){
                    referenceDate = (Calendar)flifhtInfoFrom.getDepartureDate().clone();
                    departureStation = "Roma+Termini";
                    arrivalStation = "Fiumicino+Aeroporto";
                    isFromFco = true;
                }

                if (flifhtInfoTo.getTo().getCode().equalsIgnoreCase("FCO") ){
                    referenceDate = (Calendar)flifhtInfoTo.getArrivalDate().clone();
                    departureStation = "Fiumicino+Aeroporto";
                    arrivalStation = "Roma+Termini";
                    isFromFco = false;
                }
                referenceDate   = recalculateReferenceDate(referenceDate ,isFromFco,flightArea);

                if(ctx != null) {
	                this.aduld = ctx.searchPassengersNumber.getNumAdults();
	                this.children = ctx.searchPassengersNumber.getNumChildren();
                }
            }



        }
    }

    private void retriveDepartureDateDestinationNation() throws Exception{
    	logger.trace("BOOKINGCONFERMA----{executing retriveDepartureDateDestinationNation()}-----Start Method");
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		if(ctx != null && ctx.flightSelections!=null && ctx.flightSelections.length>0 ) {

			if (ctx.flightSelections[0].getFlightData() instanceof DirectFlightData) {
				logger.trace("BOOKINGCONFERMA----{executing retriveDepartureDateDestinationNation()}-----Try get departure date Single Flight");
				DirectFlightData dfd = (DirectFlightData) ctx.flightSelections[0].getFlightData();
				if(dfd!=null) {
					logger.trace("BOOKINGCONFERMA----{executing retriveDepartureDateDestinationNation()}-----DirectFlightData is not null");
					Calendar departureDateCal = dfd.getDepartureDate();
					logger.trace("BOOKINGCONFERMA----{executing retriveDepartureDateDestinationNation()}-----getDepartureDate Executed");
					departureDate = dateFormatter.format(departureDateCal.getTime());
					if(dfd.getTo()!=null) {
						logger.trace("BOOKINGCONFERMA----{executing retriveDepartureDateDestinationNation()}-----Try get destination nation getTo is not null");
						destinationNation = dfd.getTo().getCountryCode();
					}
				}
			} else if (ctx.flightSelections[0].getFlightData() instanceof ConnectingFlightData) {
				logger.trace("BOOKINGCONFERMA----{executing retriveDepartureDateDestinationNation()}-----Try get departure date Multiple Flight");
				isSingleFlight = false;
				ConnectingFlightData dfd = (ConnectingFlightData) ctx.flightSelections[0].getFlightData();
				if(dfd!=null && dfd.getFlights().size()>0) {
					logger.trace("BOOKINGCONFERMA----{executing retriveDepartureDateDestinationNation()}-----getFlights.size >0");
					DirectFlightData dfdFinal = ((DirectFlightData) dfd.getFlights().get(dfd.getFlights().size()-1));
					logger.trace("BOOKINGCONFERMA----{executing retriveDepartureDateDestinationNation()}-----GeDdfdFinal done");
					DirectFlightData dfdFirst = ((DirectFlightData) dfd.getFlights().get(0));
					logger.trace("BOOKINGCONFERMA----{executing retriveDepartureDateDestinationNation()}-----DirectFlightData is not null");
					Calendar departureDateCal = dfdFirst.getDepartureDate();
					logger.trace("BOOKINGCONFERMA----{executing retriveDepartureDateDestinationNation()}-----getDepartureDate Executed");
					departureDate = dateFormatter.format(departureDateCal.getTime());
					if(dfdFinal.getTo()!=null) {
						logger.trace("BOOKINGCONFERMA----{executing retriveDepartureDateDestinationNation()}-----Try get destination nation getTo is not null");
						destinationNation = dfdFinal.getTo().getCountryCode();
					}
				}
			}
		}
    }
    // ********************
    // MODIFICA DEL 23/04/2018
    // Banner trenitalia
    // FINE
    // ********************
}