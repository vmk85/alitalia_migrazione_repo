package com.alitalia.aem.consumer.checkinrest.controller;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;
import com.alitalia.aem.consumer.checkinrest.render.PnrListDataRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrRender;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckInThankYouModel extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private SlingHttpServletResponse response;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	@Inject
	private CheckinSession checkinSession;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	//Oggetti JAVA
	//Variabili Classe principale
	private List<Pnr> pnrListData;
	
	private List<PnrRender> pnrListDataRender;	
	
	private List<PnrRender> pnrSelectedListDataRender;
	
	@PostConstruct
	protected void initModel() throws Exception {
		
		//Estendendo la classe GenericCheckInModel abbiamo a disposizione anche il contesto
		//Qui posso controllare i dati di risposta e renderli utilizzabili al FrontEnd AEM
		try{
			super.initBaseModel(request);
			
			this.pnrListData = new ArrayList<>();
			
			if (ctx == null) {
				logger.debug("[CheckInPnr] - contesto assente.");
				return;
			}
			
			if (ctx != null) {
				this.setPnrListData(new ArrayList<>());
				this.setPnrListData(ctx.pnrListData); 
				
				int total = 0;
				int executed = 0;
				int notAllowed = 0;
				
				PnrListDataRender renderList = new PnrListDataRender();
				this.setPnrListDataRender(new ArrayList<>());
				this.setPnrListDataRender(renderList.getPnrListDataRender(ctx.pnrListData, ctx, total, executed, notAllowed));
				
				if(ctx.pnrSelectedListDataRender != null){
					this.setPnrSelectedListDataRender(new ArrayList<>());
					this.setPnrSelectedListDataRender(ctx.pnrSelectedListDataRender);
				}
			}
			
		}catch (Exception e) {
			logger.error("Unexpected error: ", e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	public List<Pnr> getPnrListData() {
		return pnrListData;
	}

	public void setPnrListData(List<Pnr> pnrListData) {
		this.pnrListData = pnrListData;
	}

	public List<PnrRender> getPnrSelectedListDataRender() {
		return pnrSelectedListDataRender;
	}

	public void setPnrSelectedListDataRender(List<PnrRender> pnrSelectedListDataRender) {
		this.pnrSelectedListDataRender = pnrSelectedListDataRender;
	}

	public List<PnrRender> getPnrListDataRender() {
		return pnrListDataRender;
	}

	public void setPnrListDataRender(List<PnrRender> pnrListDataRender) {
	    this.pnrListDataRender = pnrListDataRender;

	}

}
