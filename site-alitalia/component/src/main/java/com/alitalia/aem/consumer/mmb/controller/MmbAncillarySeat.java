package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPassengerNoteEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillarySeatDetailData;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup.AncillariesGroupType;
import com.alitalia.aem.consumer.mmb.model.MmbFlightsGroup;
import com.alitalia.aem.consumer.mmb.render.MmbAncillarySeatsMapFlightRender;
import com.alitalia.aem.consumer.mmb.render.MmbAncillarySeatsMapPassengerRender;
import com.alitalia.aem.consumer.mmb.render.MmbAncillarySeatsMapPassengerSummaryRender;
import com.alitalia.aem.consumer.mmb.render.MmbFlightDataRender;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;
import com.alitalia.aem.consumer.utils.Lists;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbAncillarySeat extends MmbSessionAncillaryGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private MmbSession mmbSession;
	
	private List<MmbAncillarySeatsMapFlightRender> seatMapFlights;
	private List<MmbAncillarySeatsMapPassengerSummaryRender> passengersSummary;
	private List<MmbFlightsGroup> flightsGroupList;
	private boolean allowClearCart;
	private String bookingSeatSelectionsJS;
	private boolean mappaPostiEnabled;

	private int purchasableSeatsNum;
	private int selectedSeatsNum;
	
	@PostConstruct
	protected void initModel() throws IOException {
		try{
			initBaseModel(request);
			
			this.seatMapFlights = new ArrayList<MmbAncillarySeatsMapFlightRender>();
			this.passengersSummary = new ArrayList<MmbAncillarySeatsMapPassengerSummaryRender>();
			this.flightsGroupList = MmbFlightsGroup.groupByRouteType(ctx.route.getFlights());
			this.allowClearCart = false;
			this.bookingSeatSelectionsJS = "[";
			
			Map<Integer, MmbAncillarySeatsMapPassengerSummaryRender> passengersSummaryMap =
					new HashMap<Integer, MmbAncillarySeatsMapPassengerSummaryRender>();
			
			// perform a first grouping by passenger to initialize summary renderers,
			// used to provide a summary of seats organized by passenger in feedback step
			List<MmbAncillariesGroup> passengersGroups = MmbAncillariesGroup.createGroups(
					AncillariesGroupType.BY_PASSENGER, ctx.route, 
					ctx.cart.getPassengers(), ctx.cart.getAncillaries(), 
					MmbAncillaryTypeEnum.SEAT);
			for (MmbAncillariesGroup passengerGroup : passengersGroups) {
				MmbAncillarySeatsMapPassengerSummaryRender passengerSummary =
						new MmbAncillarySeatsMapPassengerSummaryRender();
				passengerSummary.setFullName(passengerGroup.getPassengerData().getName() + 
						" " + passengerGroup.getPassengerData().getLastName());
				if (MmbPassengerNoteEnum.INFANT_ACCOMPANIST.equals(
						passengerGroup.findMmbPassengerDataForFlight(0).getNote())) {
					passengerSummary.setHasInfant(true);
				}
				if (MmbPassengerNoteEnum.CHILD.equals(
						passengerGroup.findMmbPassengerDataForFlight(0).getNote())) {
					passengerSummary.setChild(true);
				}
				passengerSummary.setDisplayInFeedback(false);
				passengerSummary.setTotalToBeIssuedPrice(
						new MmbPriceRender(BigDecimal.valueOf(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				passengerSummary.setTotalToBeIssuedStandardQuantity(0);
				passengerSummary.setTotalToBeIssuedComfortQuantity(0);
				this.passengersSummary.add(passengerSummary);
				passengersSummaryMap.put(passengerGroup.getPassengerData().getId(), passengerSummary);
			}
			
			// then, perform a new external grouping by flight
			int seatMapFlightsIndex = 0;
			List<MmbAncillariesGroup> flightGroups = MmbAncillariesGroup.createGroups(AncillariesGroupType.BY_FLIGHT, 
					ctx.route, ctx.cart.getPassengers(), ctx.cart.getAncillaries(), MmbAncillaryTypeEnum.SEAT);
			for (MmbAncillariesGroup flightGroup : flightGroups) {
				// prepare seatmap header render for the flight
				MmbFlightData flight = Lists.getFirst(flightGroup.getFlights());
				MmbAncillarySeatsMapFlightRender seatMapFlightRender = 
						new MmbAncillarySeatsMapFlightRender(flight);
				
				// skip flights without any seat ancillaries
				if (flightGroup.getAncillaries().size() == 0 || seatMapFlightRender.getFlight().isMiniFare()) {
					this.mappaPostiEnabled=false;
				}else{
					if(!seatMapFlightRender.getFlight().isBus()){
						this.mappaPostiEnabled=true;
					}else{
						List<MmbAncillaryData> seatMap=new ArrayList<MmbAncillaryData>();
						flightGroup.setAncillaries(seatMap);
						this.mappaPostiEnabled=false;
					}
				}
		
				seatMapFlightRender.SetMappaPostiEnabled(mappaPostiEnabled);
				
				if (!seatMapFlightRender.getFlight().isMiniFare()) {
					this.seatMapFlights.add(seatMapFlightRender);
				}
				
				// for each flight, group again by passenger
				List<MmbAncillariesGroup> groups = MmbAncillariesGroup.createGroups(
						AncillariesGroupType.BY_PASSENGER, ctx.route, 
						ctx.cart.getPassengers(), flightGroup.getAncillaries(), 
						MmbAncillaryTypeEnum.SEAT);
				for (MmbAncillariesGroup group : groups) {
					
					// skip passengers without any seat ancillaries
					if (group.getAncillaries().size() == 0) {
						continue;
					}
					
					// data for JS initialization object
					String jsDataPassenger = "";
					String jsDataFlight = "";
					String jsDataSeatNumber = "";
					String jsDataPreviousSeatNumber = "";
					String jsDataIsChangeable = "";
					String jsDataHasInfant = "";
					String jsDataIsChild = "";
					String jsDataComfortSeatPrice = "";
					String jsDataComfortSeatPriceCurrency = "";
					String jsDataHasFreeComfortSeat = "";
					String jsDataAncillaryIds = "";
					
					MmbAncillarySeatsMapPassengerRender flightPassengerRender = 
							new MmbAncillarySeatsMapPassengerRender();
					seatMapFlightRender.addPassenger(flightPassengerRender);
					flightPassengerRender.setId(group.getPassengerData().getId());
					flightPassengerRender.setFullName(group.getPassengerData().getName() + 
							" " + group.getPassengerData().getLastName());
					if (MmbPassengerNoteEnum.INFANT_ACCOMPANIST.equals(group.findMmbPassengerDataForFlight(0).getNote())) {
						flightPassengerRender.setHasInfant(true);
					}
					if (MmbPassengerNoteEnum.CHILD.equals(group.findMmbPassengerDataForFlight(0).getNote())) {
						flightPassengerRender.setChild(true);
					}
					MmbPriceRender availablePriceInfo = null;
					
					if (group.getAncillaries().size() == 0) {
						
						// no ancillary for this passenger/flight
						flightPassengerRender.setIdentifier("");
						
						jsDataPassenger = String.valueOf(group.getPassengerData().getId());
						jsDataFlight = String.valueOf(seatMapFlightsIndex);
						jsDataSeatNumber = "";
						jsDataPreviousSeatNumber = "";
						jsDataIsChangeable = "false";
						jsDataHasInfant = "false";
						jsDataIsChild = "false";
						jsDataComfortSeatPrice = "";
						jsDataHasFreeComfortSeat = "false";
						jsDataAncillaryIds = "";
						
					} else {
						
						// determine the related flight group render
						MmbFlightsGroup relatedHeaderFlightsGroup = null;
						MmbFlightDataRender relatedHeaderFlight = null;
						for (MmbFlightsGroup flightHeaderGroup : this.flightsGroupList) {
							if (flight.getType().equals(flightHeaderGroup.getType()) 
									&& flight.getRouteId().equals(flightHeaderGroup.getRouteId())) {
								relatedHeaderFlightsGroup = flightHeaderGroup;
								for (MmbFlightDataRender currentHeaderFlight : 
											relatedHeaderFlightsGroup.getFlightDataRender()) {
									if (flight.getFlightNumber().equals(currentHeaderFlight.getFlightData().getFlightNumber())
											&& flight.getCarrier().equals(currentHeaderFlight.getFlightData().getCarrier())) {
										relatedHeaderFlight = currentHeaderFlight;
										break;
									}
								}
								break;
							}
						}
						
						// determine the related passenger summary render group render
						MmbAncillarySeatsMapPassengerSummaryRender relatedPassengerSummary =
								passengersSummaryMap.get(group.getPassengerData().getId());
						
						// determine currently selected seat
						MmbAncillaryData ancillaryForPreselection = 
								group.getAncillaryInStatuses(new MmbAncillaryStatusEnum[] {
										MmbAncillaryStatusEnum.PENDING,
										MmbAncillaryStatusEnum.TO_BE_ISSUED,
										MmbAncillaryStatusEnum.ISSUED,
										MmbAncillaryStatusEnum.PREVIOUSLY_ISSUED});
						if (ancillaryForPreselection != null) {
							MmbAncillarySeatDetailData ancillaryDetail = 
									(MmbAncillarySeatDetailData) ancillaryForPreselection.getAncillaryDetail();
							flightPassengerRender.setCurrentValue(ancillaryDetail.getSeat());
							flightPassengerRender.setCurrentValueComfort(ancillaryDetail.isComfort());
							if (relatedHeaderFlightsGroup != null) {
								relatedHeaderFlightsGroup.getRelatedAncillaries().add(ancillaryForPreselection);
								relatedHeaderFlight.getRelatedAncillaries().add(ancillaryForPreselection);
							}
						}
						
						// determine previously selected seat
						MmbAncillaryData ancillaryForPreviousSelection = 
								group.getAncillaryInStatuses(new MmbAncillaryStatusEnum[] {
										MmbAncillaryStatusEnum.PENDING,
										MmbAncillaryStatusEnum.PREVIOUSLY_ISSUED});
						if (ancillaryForPreviousSelection != null) {
							MmbAncillarySeatDetailData ancillaryDetail = 
									(MmbAncillarySeatDetailData) ancillaryForPreselection.getAncillaryDetail();
							// we make sure to also save the previously selected seat, in case we have an issued one,
							// in order to mark it as free in the map when changing the selection for that passenger
							flightPassengerRender.setPreviousValue(ancillaryDetail.getSeat());
							flightPassengerRender.setPreviousValueComfort(ancillaryDetail.isComfort());
							selectedSeatsNum += ancillaryForPreviousSelection.getQuantity() > 0 
									? ancillaryForPreviousSelection.getQuantity() : 1;
						}
						
						// determine if a new selection is available
						MmbAncillaryData ancillaryForSelection = 
								group.getAncillaryInStatuses(new MmbAncillaryStatusEnum[] {
										MmbAncillaryStatusEnum.AVAILABLE,
										MmbAncillaryStatusEnum.TO_BE_ISSUED,
										MmbAncillaryStatusEnum.ISSUED});
						if (ancillaryForSelection != null) {
							flightPassengerRender.setIdentifier(
									getAncillariesSelectionIdentifier(ancillaryForSelection));
							flightPassengerRender.setAllowModify(true);
							availablePriceInfo = mmbSession.getAncillaryDisplayedPriceInfo(ctx, ancillaryForSelection, 1);
							purchasableSeatsNum += ancillaryForSelection.getQuantity() > 0 
									? ancillaryForSelection.getQuantity() : 1;
						} else {
							flightPassengerRender.setIdentifier("");
						}
						
						// determine if clearing cart is available
						MmbAncillaryData ancillaryForCartClear = 
								group.getAncillaryInStatus(MmbAncillaryStatusEnum.TO_BE_ISSUED);
						if (ancillaryForCartClear != null) {
							this.allowClearCart = true;
						}
						
						// update renderers for the seat ancillary to be issued or just issued
						MmbAncillaryData ancillaryForSummary = 
								group.getAncillaryInStatuses(new MmbAncillaryStatusEnum[] {
										MmbAncillaryStatusEnum.TO_BE_ISSUED,
										MmbAncillaryStatusEnum.ISSUED});
						if (ancillaryForSummary != null) {
							relatedPassengerSummary.setDisplayInFeedback(true);
							MmbAncillarySeatDetailData ancillaryDetail = 
									(MmbAncillarySeatDetailData) ancillaryForPreselection.getAncillaryDetail();
							if (ancillaryDetail.isComfort()) {
								relatedPassengerSummary.setTotalToBeIssuedComfortQuantity(
										relatedPassengerSummary.getTotalToBeIssuedComfortQuantity() + 1);
								MmbPriceRender priceInfo = 
										mmbSession.getAncillaryDisplayedPriceInfo(ctx, ancillaryForSummary, null);
								relatedPassengerSummary.getTotalToBeIssuedPrice().increaseAmount(
										priceInfo.getAmount());
							} else {
								relatedPassengerSummary.setTotalToBeIssuedStandardQuantity(
										relatedPassengerSummary.getTotalToBeIssuedStandardQuantity() + 1);
							}
						}
						
						jsDataPassenger = String.valueOf(group.getPassengerData().getId());
						jsDataFlight = String.valueOf(seatMapFlightsIndex);
						jsDataSeatNumber = flightPassengerRender.getCurrentValue();
						jsDataPreviousSeatNumber = flightPassengerRender.getPreviousValue();
						jsDataIsChangeable = flightPassengerRender.isAllowModify() ? "true" : "false";
						jsDataHasInfant = flightPassengerRender.getHasInfant() ? "true" : "false";
						jsDataIsChild = flightPassengerRender.isChild() ? "true" : "false";
						if (availablePriceInfo != null) {
							jsDataComfortSeatPrice = availablePriceInfo.getFormattedAmount();
							jsDataComfortSeatPriceCurrency = availablePriceInfo.getCurrencySymbol();
							jsDataHasFreeComfortSeat = availablePriceInfo.getIsAmountZeroOrNull() ? "true" : "false";
						} else {
							jsDataComfortSeatPrice = "";
							jsDataComfortSeatPriceCurrency = "";
							jsDataHasFreeComfortSeat = "false";
						}
						jsDataAncillaryIds = flightPassengerRender.getIdentifier();
						
					}
					
					this.bookingSeatSelectionsJS += "{" +
							"passenger: '" + jsDataPassenger + "'," +
							"flight: '" + jsDataFlight + "'," +
							"seatNumber: '" + jsDataSeatNumber + "'," +
							"previousSeatNumber: '" + jsDataPreviousSeatNumber + "'," +
							"isChangeable: '" + jsDataIsChangeable + "', " +
							"hasInfant: '" + jsDataHasInfant + "', " +
							"isChild: '" + jsDataIsChild + "', " +
							"comfortSeatPrice: '" + jsDataComfortSeatPrice + "', " +
							"comfortSeatPriceCurrency: '" + jsDataComfortSeatPriceCurrency + "', " +
							"hasFreeComfortSeat: '" + jsDataHasFreeComfortSeat + "', " +
							"ancillaryIds: '" + jsDataAncillaryIds + "' " +
							"},\n";
				}
				
				// prepare seat map render when current seats for passengers have been determined, because
				// we want to mark as free the seats occupied by the passengers of our PNR
				seatMapFlightRender.prepareSeatmapRenderUsingPassengersData(ctx.seatMapsByFlight.get(flight));
				
				seatMapFlightsIndex++;
			}
			
			this.bookingSeatSelectionsJS += "]";
		} catch (Exception e) {
			logger.error("Unexpected error" , e);
			throw e;
		}
		
	}
		
	public List<MmbAncillarySeatsMapFlightRender> getSeatMapFlights() {
		return this.seatMapFlights;
	}
	
	public List<MmbAncillarySeatsMapPassengerSummaryRender> getPassengersSummary() {
		return this.passengersSummary;
	}
	
	public List<MmbFlightsGroup> getFlightsGroupList() {
		return this.flightsGroupList;
	}
	
	public boolean isAllowClearCart() {
		return this.allowClearCart;
	}
	
	public boolean getMappaPostiEnabled() {
		return this.mappaPostiEnabled;
	}
	
	public String getBookingSeatSelectionsJS() {
		return this.bookingSeatSelectionsJS;
	}
	
	public int getPurchasableSeatsNum() {
		return purchasableSeatsNum;
	}

	public int getSelectedSeatsNum() {
		return selectedSeatsNum;
	}
	
}