package com.alitalia.aem.consumer.servlet.myalitalia;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.MACreditCardData;
import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.messages.home.AdyenDisableStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenDisableStoredPaymentResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaConstants;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.myalitaliauser.delegaterest.IAdyenRecurringPaymentDelegate;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "madeletemycreditcard" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class DeleteMyCreditCardServlet extends MyAlitaliaCreditCardServlet {
	
	@Reference
	protected volatile IAdyenRecurringPaymentDelegate adyenRecurringPaymentDelegate;
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		logger.info("MAPayment - DeleteMyCreditCardServlet - performSubmit: Cancellazione Carta di Credito in Adyen");
		try {
			MyAlitaliaSessionContext mactx = (MyAlitaliaSessionContext) request.getSession().getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);
			if (mactx != null) {
				Object obj = mactx.getMaCustomer();
				if (obj instanceof MACustomer && obj!=null) {
					MACustomer loggedMAUserProfile = (MACustomer) obj;
					String UID = loggedMAUserProfile.getUID();
					if (UID!=null && UID.trim().isEmpty()==false) {
						logger.debug("MAPayment - DeleteMyCreditCardServlet - performSubmit: UID [{}]", UID);
	
						String selectedCreditCardId = request.getParameter("selectedCreditCardId");
						logger.debug("MAPayment - DeleteMyCreditCardServlet - performSubmit: selectedCreditCardId [{}]", selectedCreditCardId);
						
						if (selectedCreditCardId!=null) {
							MACreditCardData selectedCreditCardData = getSelectedCreditCardData(loggedMAUserProfile, selectedCreditCardId);
							if (selectedCreditCardData!=null) {
								logger.debug("MAPayment - DeleteMyCreditCardServlet - performSubmit: selectedCreditCard [{}]", selectedCreditCardData.print());
								
								AdyenDisableStoredPaymentRequest disableStoredPaymentRequest = new AdyenDisableStoredPaymentRequest();
								disableStoredPaymentRequest.setShopperReference(UID);
								disableStoredPaymentRequest.setRecurringDetailReference(selectedCreditCardData.getRecurringDetailReference());
								disableStoredPaymentRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
								disableStoredPaymentRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
								disableStoredPaymentRequest.setConversationID(IDFactory.getTid());
								
								logger.info("MAPayment - DeleteMyCreditCardServlet - performSubmit: invocando adyenRecurringPaymentDelegate.disableStoredPayment...");
								AdyenDisableStoredPaymentResponse disableStoredPaymentResponse = adyenRecurringPaymentDelegate.disableStoredPayment(disableStoredPaymentRequest);
								if (disableStoredPaymentResponse!=null) {
									if (disableStoredPaymentResponse.getResponse()!=null && disableStoredPaymentResponse.getResponse().equalsIgnoreCase("[detail-successfully-disabled]")) {
										logger.info("MAPayment - DeleteMyCreditCardServlet - performSubmit: Carta di credito [{}] cancellata", selectedCreditCardData.print());
										try {
											retrieveMyCreditCard(loggedMAUserProfile, AlitaliaUtils.getRepositoryPathMarket(request.getResource()), AlitaliaUtils.getRepositoryPathLanguage(request.getResource()), adyenRecurringPaymentDelegate);
										} catch (Exception e) {
										} finally {
											prepareResponseOK(response, "");
										}
									} else {
										logger.error("MAPayment - DeleteMyCreditCardServlet - performSubmit: Errore durante la cancellazione della carta di credito in Adyen, resultCode [{}]", disableStoredPaymentResponse.getResponse());
										prepareResponseNOK(response, "");
/*RDT TOLENTINO Rimuover Dopo Test - I servizi Adyen non rispondono sempre perchè non sono dietro Data Power*/		
/*RDT									logger.info("MAPayment - DeleteMyCreditCardServlet - performSubmit: Carta di credito [{}] cancellata", selectedCreditCardData.print());*/
/*RDT									try {*/
/*RDT										retrieveMyCreditCard(loggedMAUserProfile, AlitaliaUtils.getRepositoryPathMarket(request.getResource()), AlitaliaUtils.getRepositoryPathLanguage(request.getResource()), adyenRecurringPaymentDelegate);*/
/*RDT									} catch (Exception e) {*/
/*RDT									} finally {*/
/*RDT										prepareResponseOK(response, "");*/
/*RDT									}*/
									}
								} else {
									logger.error("MAPayment - DeleteMyCreditCardServlet - performSubmit: Errore durante la cancellazione della carta di credito in Adyen, response is null");
									prepareResponseNOK(response, "");
								}
							} else {
								logger.error("MAPayment - DeleteMyCreditCardServlet - performSubmit: selected credit card not found, selectedCreditCardId ["+selectedCreditCardId+"]");
					            throw new Exception("Carta di credito con ID ["+selectedCreditCardId+"] non trovata");
							}
						} else {
							logger.error("MAPayment - DeleteMyCreditCardServlet - performSubmit: ID Carta di credito non ricevuto");
							throw new Exception("ID Carta di credito non ricevuto");
						}
					} else {
						logger.error("MAPayment - DeleteMyCreditCardServlet - performSubmit: UID utente MyAlitalia non presente");
						throw new Exception("UID utente MyAlitalia non presente");
					}
				} else {
					logger.error("MAPayment - DeleteMyCreditCardServlet - performSubmit: Utente MyAlitalia non loggato");
					throw new Exception("Utente MyAlitalia non loggato");
				}
			} else {
				logger.error("MAPayment - DeleteMyCreditCardServlet - performSubmit: Utente MyAlitalia non loggato");
				throw new Exception("Utente MyAlitalia non loggato");
			}
		} catch (Exception e) {
			logger.error("MAPayment - DeleteMyCreditCardServlet - performSubmit: Errore durante la cancellazione della carta di credito in Adyen, Exception [{}]", e.getMessage());
			throw e;
		}
	}

	private MACreditCardData getSelectedCreditCardData(MACustomer loggedMAUserProfile, String selectedCreditCardId) {
		if (loggedMAUserProfile!=null && loggedMAUserProfile.getData()!=null && loggedMAUserProfile.getData().getPayment()!=null &&
			loggedMAUserProfile.getData().getPayment().getCreditCardsData()!=null && 
			loggedMAUserProfile.getData().getPayment().getCreditCardsData().isEmpty()==false) {
			for (MACreditCardData creditCardData: loggedMAUserProfile.getData().getPayment().getCreditCardsData()) {
				if (creditCardData.getId()==Integer.valueOf(selectedCreditCardId)) {
					return creditCardData;
				}
			}
		}

		return null;
	}

	/**
	 * Provide an empty implementation, should be never used as
	 * MMB servlets should always be invoked with ajax requests
	 */
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		logger.warn("The emtpy saveDataIntoSession method was unexpectedly invoked.");
	}
}

