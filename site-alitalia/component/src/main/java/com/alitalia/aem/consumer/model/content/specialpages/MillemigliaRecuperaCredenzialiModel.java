package com.alitalia.aem.consumer.model.content.specialpages;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

//
//public static final String NAME = "MillemigliaRecuperaCredenzialiData";
//
//public static final String[] RETRIEVERESETCHOICE = new String[] { "retrieveUsername", "resetPassword" };
//
//private String surname;
//private String mmcode;
//private String alias;
//// Strong Auth Fields
//private boolean  flagVerifica_OTP_DS_OK = false;
//
//private String idProfilo;
//private String username;
//private boolean flagVerificaUserName_OK = false;
//private String password;
//private boolean flagVerificaPassword_OK = false;
//private String confPassword;
//private boolean flagVerificaPasswordConfirmed_OK = false;
//private String certifiedEmailAccount;
//private boolean flagVerificaEmailUser_OK = false;
//private String certifiedCountryNumber; // Prefisso Internazionale. Es : 0039
//private String certifiedPhoneNumber;   //
//private boolean flagVerificaCellulare_OK = false;
//private String inputSecretQuestion;
//private int inputSecretQuestionID;
//private boolean flagVerificaDomanda_OK = false;
//private String rispostaSegreta;
//private boolean flagVerificaRisposta_OK = false;
//
//
//private String error;
//private Integer state;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MillemigliaRecuperaCredenzialiModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private MillemigliaRecuperaCredenzialiData millemigliaRecuperaCredenzialiData;

	@PostConstruct
	protected void initModel() {
		logger.debug("[MillemigliaRecuperaCredenzialiModel] initModel");

		super.initBaseModel(slingHttpServletRequest);
		
		try {
			// retrieve and remove BaggageClaimData from session if any
			millemigliaRecuperaCredenzialiData = (MillemigliaRecuperaCredenzialiData)
				slingHttpServletRequest.getSession()
				.getAttribute(MillemigliaRecuperaCredenzialiData.NAME);
	
			if (millemigliaRecuperaCredenzialiData == null) {
				millemigliaRecuperaCredenzialiData = new MillemigliaRecuperaCredenzialiData();
			}

			
			slingHttpServletRequest.getSession().setAttribute(
					MillemigliaRecuperaCredenzialiData.NAME,
					millemigliaRecuperaCredenzialiData);
		} catch (Exception e) {
			logger.error("Unexpected error: {}", e);
			throw e;
		}
	}


	public MillemigliaRecuperaCredenzialiData getMillemigliaRecuperaCredenzialiData() {
		return millemigliaRecuperaCredenzialiData;
	}



}
