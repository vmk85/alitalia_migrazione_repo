package com.alitalia.aem.consumer.model.content;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

@Model(adaptables = { SlingHttpServletRequest.class })
public class FakeTitleModel extends GenericBaseModel{
	
		@Self
		private SlingHttpServletRequest slingHttpServletRequest;
	    private boolean showFakeTitle;
	    
	    private static final String[] FAKE_TITLE_EXLUSIONS = {
	    		"pagine-primo-livello",
	    		"redazionale-landing",
	    		"redazionale-principale-2-colonne",
	    		"redazionale-secondaria-2-colonne",
	    		"redazionale-tutta-pagina",
	    		"redazionale-form-booking",
	    		"pagine-secondo-livello",
	    		"redazionale-lightbox",
	    		"mmb-special-assistance-page"
	    		};
	    
		@PostConstruct
		protected void initModel() {
			showFakeTitle = true;
			try{
				super.initBaseModel(slingHttpServletRequest);
				String templatePath = slingHttpServletRequest.getResource().getValueMap().get("cq:template", "");
				String templateName = templatePath.substring(templatePath.lastIndexOf("/")+1);
				if(Arrays.asList(FAKE_TITLE_EXLUSIONS).contains(templateName)){
					showFakeTitle = false;
				}
			}
			catch(Exception e){
				
			}
		}
		
		public boolean isShowFakeTitle() {
			return showFakeTitle;
		}
			   
}
