package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Enumerazione per la tipologia del passeggero per gli ancillari. 
 ['ADT', 'CHD', 'INF', 'YTH', 'C04', 'C11', 'INS', 'YCD', 'YCB', 'STU', 'MIL', 'SRC', 'CMP', 'CMA', 'NRF']
*/
public enum AncillaryPassengerType
{
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("ADULT")] ADT,
	ADT,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("CHILD")] CHD,
	CHD,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("INFANT WITHOUT A SEAT")] INF,
	INF,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("YOUTH CONFIRMED")] YTH,
	YTH,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("ACCOMPANIED CHILD 4 YEARS")] C04,
	C04,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("ACCOMPANIED CHILD 11 YEARS")] C11,
	C11,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("INFANT WITH A SEAT")] INS,
	INS,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("SENIOR CITIZEN")] YCD,
	YCD,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("SENIOR CITIZEN STANDBY")] YCB,
	YCB,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("STUDENT")] STU,
	STU,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("MILITARY CONFIRMED")] MIL,
	MIL,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("SENIOR CITIZEN")] SRC,
	SRC,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("COMPANION")] CMP,
	CMP,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("ADULT WITH COMPANION")] CMA,
	CMA,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("---")] NRF
	NRF;

	public static final int SIZE = java.lang.Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static AncillaryPassengerType forValue(int value)
	{
		return values()[value];
	}
}