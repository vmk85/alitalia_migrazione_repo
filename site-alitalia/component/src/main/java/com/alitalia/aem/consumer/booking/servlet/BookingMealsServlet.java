package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.Passenger;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingmealsconsumer" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.") })
@SuppressWarnings("serial")
public class BookingMealsServlet extends GenericBookingFormValidatorServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	
	
	@Reference
	private AlitaliaConfigurationHolder alitaliaConfiguration;
	
	@Reference
	private BookingSession bookingSession;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
			
		BookingSessionContext ctx = getBookingSessionContext(request);
		
	
		Validator validator = new Validator();
		
		if (ctx == null) {
			logger.error("Cannot find booking session context in session.");
			throw new IllegalStateException("Booking session context not found.");
		}
		else {
			int numberPassengers = 0;
			for (Passenger passenger : ctx.passengersData.getPassengersList()) {
				if (!passenger.isInfant()) {
					numberPassengers++;
				}
			}
			
			
			int k;
			for (k=0; k<numberPassengers; k++) {
				String key = "food" + Integer.toString(k+1);
				String meal = request.getParameter(key);
				validator.addDirectConditionMessagePattern(key, meal,
						I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				
			}
		}
		
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		BookingSessionContext ctx = getBookingSessionContext(request);
		if (ctx == null) {
			throw new GenericFormValidatorServletException(false);
		}
		
		int numberPassengers = 0;
		for (Passenger passenger : ctx.passengersData.getPassengersList()) {
			if (!passenger.isInfant()) {
				numberPassengers++;
			}
		}
		String[] mealPreferences = new String[numberPassengers];
		int k;
		for (k=0; k<numberPassengers; k++) {
			String key = "food" + Integer.toString(k+1);
			String meal = request.getParameter(key);
			if (meal == null) {
				throw new GenericFormValidatorServletException(false);
			}
			else {
				mealPreferences[k] = meal;
			}
		}
		bookingSession.updateAncillaryMealPreferences(ctx, mealPreferences);
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		json.key("result").value("OK");
		json.endObject();
		
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return alitaliaConfiguration;
	}
	
}
