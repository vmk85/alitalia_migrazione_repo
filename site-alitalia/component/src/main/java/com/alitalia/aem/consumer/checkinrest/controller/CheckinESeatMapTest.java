package com.alitalia.aem.consumer.checkinrest.controller;

import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;

//package com.alitalia.aem.consumer.checkinrest.controller;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.annotation.PostConstruct;
//import javax.inject.Inject;
//
//import org.apache.sling.api.SlingHttpServletRequest;
//import org.apache.sling.models.annotations.Model;
//import org.apache.sling.models.annotations.injectorspecific.Self;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.request.ListItemsFareAvailQualifier;
//import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.EnhancedSeatMapResp;
//import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.Seat;
//import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.SeatMapColumn;
//import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Flight;
//import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
//import com.alitalia.aem.common.messages.checkinrest.CheckinEnhancedSeatMapRequest;
//import com.alitalia.aem.common.messages.checkinrest.CheckinEnhancedSeatMapResponse;
//import com.alitalia.aem.common.utils.IDFactory;
//import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
//import com.alitalia.aem.consumer.checkin.CheckinSession;
//import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;
//import com.alitalia.aem.consumer.checkinrest.enumeration.CheckinRestTypeSeatEnum;
//import com.alitalia.aem.consumer.checkinrest.render.FlightsSectorRender;
//import com.alitalia.aem.consumer.checkinrest.render.PassengerRender;
//import com.alitalia.aem.consumer.checkinrest.render.SeatRender;
//import com.alitalia.aem.consumer.checkinrest.render.SectorRender;
//import com.alitalia.aem.consumer.checkinrest.render.THead;
//import com.alitalia.aem.service.impl.checkinrest.utils.CheckinMessageUnmarshal;
//import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
//
//@Model(adaptables = { SlingHttpServletRequest.class})
public class CheckinESeatMapTest extends GenericCheckinModel {
//
//	@Self
//	private SlingHttpServletRequest request;
//
//	@Inject
//	private AlitaliaConfigurationHolder configuration;
//
//	@Inject
//	private CheckinSession checkinSession;
//
//	@Inject 
//	private volatile ICheckinDelegate checkInDelegateRest;
//
//	private Logger logger = LoggerFactory.getLogger(this.getClass());
//
//	private List<EnhancedSeatMapResp> enhancedSeatMapResp;
//
//	private Flight selectedFlight;
//
//	private ArrayList<ArrayList> matrixFlightsSectorRender = null;	
//
//
//	@PostConstruct
//	protected void initModel() throws Exception {
//
//		enhancedSeatMapResp = new ArrayList<EnhancedSeatMapResp>();
//
//		try {
//			super.initBaseModel(request);
//			//contatore chiamate al delegate
//			int d = 0;
//
//			//******************************
//			CheckinEnhancedSeatMapResponse enhancedSeatMapObj = new CheckinEnhancedSeatMapResponse();
//			CheckinEnhancedSeatMapRequest checkinEnhancedSeatMapRequest = new CheckinEnhancedSeatMapRequest(IDFactory.getTid(), IDFactory.getSid(request));
//			CheckinEnhancedSeatMapResponse checkinEnhancedSeatMapresponse = new CheckinEnhancedSeatMapResponse(IDFactory.getTid(), IDFactory.getSid(request));
//
//			List<ListItemsFareAvailQualifier> listOfPassengerNames = new ArrayList<ListItemsFareAvailQualifier>(); 
//
//			//			List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr> pnrList = ctx.pnrListData;  COMMENTATO PER TEST
//
//			//costruisco la request attraverso i dati che ho a disposizione nella sessione
//
//			//----------------------- TEST SEAT MAP ----------------------------------
//
//			ListItemsFareAvailQualifier listItemsFareAvailQualifier = new ListItemsFareAvailQualifier();
//			listItemsFareAvailQualifier.setGivenName("giancarlo");
//			listItemsFareAvailQualifier.setSurname("negrin");
//			listOfPassengerNames.add(listItemsFareAvailQualifier);
//			ListItemsFareAvailQualifier listItemsFareAvailQualifier1 = new ListItemsFareAvailQualifier();
//			listItemsFareAvailQualifier1.setGivenName("guglielmo");
//			listItemsFareAvailQualifier1.setSurname("kristiano");
//			listOfPassengerNames.add(listItemsFareAvailQualifier1);
//			ListItemsFareAvailQualifier listItemsFareAvailQualifier2 = new ListItemsFareAvailQualifier();
//			listItemsFareAvailQualifier2.setGivenName("bianca");
//			listItemsFareAvailQualifier2.setSurname("bianchini");
//			listOfPassengerNames.add(listItemsFareAvailQualifier2);
//			checkinEnhancedSeatMapRequest.setListItemsFareAvailQualifiers(listOfPassengerNames);
//			checkinEnhancedSeatMapRequest.setFlight("1435");
//			checkinEnhancedSeatMapRequest.setBookingClass("Y");
//			checkinEnhancedSeatMapRequest.setOrigin("FCO");
//			checkinEnhancedSeatMapRequest.setDestination("TRN");
//			checkinEnhancedSeatMapRequest.setDepartureDate("2018-01-20");
//			checkinEnhancedSeatMapRequest.setAirline("AZ");
//			checkinEnhancedSeatMapRequest.setPnr("HSTTID");
//			checkinEnhancedSeatMapRequest.setLanguage("IT");
//			checkinEnhancedSeatMapRequest.setMarket("IT");
//			checkinEnhancedSeatMapRequest.setConversationID("FR567YUH");
//
//			//----------------------- TEST SEAT MAP ----------------------------------
//
//			/*	if (ctx.selectedFlight == null) {
//				enhancedSeatMapReq.setLanguage(ctx.language);
//				enhancedSeatMapReq.setMarket(ctx.market);
//				enhancedSeatMapReq.setConversationID(ctx.conversationId);
//			//for each pnr in pnrListData, do
//			 * for each flight in pnr, do
//			 * for each segment in fight, do
//				enhancedSeatMapReq.setFlight(ctx.pnrListData.get(0).getFlights().get(0).getSegments().get(0).getFlight()); //numero del volo del primo segmento
//				enhancedSeatMapReq.setOrigin(ctx.pnrListData.get(0).getFlights().get(0).getSegments().get(0).getOrigin().getCode());
//				enhancedSeatMapReq.setDestination(ctx.pnrListData.get(0).getFlights().get(0).getSegments().get(0).getDestination().getCode());
//				enhancedSeatMapReq.setDepartureDate(ctx.pnrListData.get(0).getFlights().get(0).getDepartureDate());
//				enhancedSeatMapReq.setAirline(ctx.pnrListData.get(0).getFlights().get(0).getSegments().get(0).getAirline());
//			//only get bookingClass of first passenger
//				enhancedSeatMapReq.setBookingClass(ctx.pnrListData.get(0).getFlights().get(0).getSegments().get(0).getPassengers().get(0).getBookingClass());
//
//				for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passengers : ctx.pnrListData.get(0).getFlights().get(0).getSegments().get(0).getPassengers()) {
//					ListItemsFareAvailQualifier listItemsFareAvailQualifier = new ListItemsFareAvailQualifier();
//					listItemsFareAvailQualifier.setGivenName(passengers.getNome());
//					listItemsFareAvailQualifier.setSurname(passengers.getCognome());
//					listOfPassengerNames.add(listItemsFareAvailQualifier);
//				}
//				enhancedSeatMapReq.setListItemsFareAvailQualifiers(listOfPassengerNames);
//				checkinEnhancedSeatMapRequest.set_enhancedseatmapReq(enhancedSeatMapReq);
//			}
//			else {
//				enhancedSeatMapReq.setLanguage(ctx.language);
//				enhancedSeatMapReq.setMarket(ctx.market);
//				enhancedSeatMapReq.setConversationID(ctx.conversationId);
//			//for each segment in selectedFlight, do
//				enhancedSeatMapReq.setFlight(ctx.selectedFlight.getSegments().get(0).getFlight()); //numero del volo del primo segmento
//				enhancedSeatMapReq.setOrigin(ctx.selectedFlight.getSegments().get(0).getOrigin().getCityCode());
//				enhancedSeatMapReq.setDestination(ctx.selectedFlight.getSegments().get(0).getDestination().getCityCode());
//				enhancedSeatMapReq.setDepartureDate(ctx.selectedFlight.getDepartureDate());
//				enhancedSeatMapReq.setAirline(ctx.selectedFlight.getSegments().get(0).getAirline());
//			//only get bookingClass of first passenger
//				enhancedSeatMapReq.setBookingClass(ctx.selectedFlight.getSegments().get(0).getPassengers().get(0).getBookingClass());
//
//				for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passengers : ctx.selectedFlight.getSegments().get(0).getPassengers()) {
//					ListItemsFareAvailQualifier listItemsFareAvailQualifier = new ListItemsFareAvailQualifier();
//					listItemsFareAvailQualifier.setGivenName(passengers.getNome());
//					listItemsFareAvailQualifier.setSurname(passengers.getCognome());
//					listOfPassengerNames.add(listItemsFareAvailQualifier);
//				}
//				enhancedSeatMapReq.setListItemsFareAvailQualifiers(listOfPassengerNames);
//				checkinEnhancedSeatMapRequest.set_enhancedseatmapReq(enhancedSeatMapReq);
//
//			}*/
//
//			//primo volo -- primo segmento --- lista pass primo seg
//			//			for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr pnr : pnrList) {
//			//				for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Flight flight : pnr.getFlights()) {
//			//					enhancedSeatMapRequest.get_enhancedseatmapReq().setOrigin(flight.getOrigin().getCityCode());
//			//					enhancedSeatMapRequest.get_enhancedseatmapReq().setDestination(flight.getDestination().getCityCode());
//			//					for (Segment segment : flight.getSegments()) {
//			//						enhancedSeatMapRequest.get_enhancedseatmapReq().setAirline(segment.getAirline());
//			//						enhancedSeatMapRequest.get_enhancedseatmapReq().setFlight(segment.getFlight());
//			//						enhancedSeatMapRequest.get_enhancedseatmapReq().setDepartureDate(segment.getDepartureDate());
//			//						for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passengers : segment.getPassengers()) {
//			//							enhancedSeatMapRequest.get_enhancedseatmapReq().setBookingClass(passengers.getBookingClass());
//			//							for (ListItemsFareAvailQualifier listIFAQ : listOfPassengerNames) {
//			//								listIFAQ.setGivenName(passengers.getNome());
//			//								listIFAQ.setSurname(passengers.getCognome());
//			//							}
//			//						}
//			//					}
//			//				}
//			//			}
//
//			logger.info("[CheckinESeatMap - initModel] - Dati per request recuperati dalla sessione.");
//
//
//			//checkinEnhancedSeatMapresponse = checkInDelegateRest.enhancedSeatMap(checkinEnhancedSeatMapRequest);
//			CheckinMessageUnmarshal checkinMessageUnmarshal = new CheckinMessageUnmarshal();
//			String jsonString = "{ \"seatMAp\": { \"segmentSeat\": { \"airline\": \"AZ\", \"flight\": \"1268\", \"origin\": { \"city\": \"Napoli\", \"cityUrl\": null, \"cityCode\": \"NAP\", \"code\": \"NAP\", \"country\": null, \"countryCode\": \"IT\", \"state\": \"  \", \"stateCode\": null, \"name\": \"Napoli\", \"marketAreaCode\": \"ITA\", \"checkInEligibility\": false }, \"destination\": { \"city\": \"Roma\", \"cityUrl\": null, \"cityCode\": \"ROM\", \"code\": \"FCO\", \"country\": null, \"countryCode\": \"IT\", \"state\": \"  \", \"stateCode\": null, \"name\": \"Roma, Fiumicino\", \"marketAreaCode\": \"ITA\", \"checkInEligibility\": false }, \"arrivalDate\": \"2018-01-26T07:35:00Z\", \"departureDate\": \"2018-01-26T06:50:00Z\", \"openCI\": false, \"checkInGate\": \"GATE\", \"airlineName\": null, \"terminal\": null, \"duration\": \"00:00:00\", \"waiting\": \"00:00:00\" }, \"seatMaps\": { \"cabinType\": 0, \"startingRow\": \"1\", \"endingRow\": \"31\", \"seatMapSectors\": [ { \"startingRow\": \"1\", \"endingRow\": \"12\", \"seatMapRows\": [ { \"rowNumber\": \"1\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"SeatDenied\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"SeatDenied\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"NotApplicable\", \"type\": \"NotExist\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"NotApplicable\", \"type\": \"NotExist\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"NotApplicable\", \"type\": \"NotExist\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"NotApplicable\", \"type\": \"NotExist\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"NotApplicable\", \"type\": \"NotExist\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"NotApplicable\", \"type\": \"NotExist\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"2\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"3\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"4\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"5\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"6\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"7\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": true, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"8\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Occupied\", \"type\": null, \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": \"FE788F000001\" }, { \"number\": \"A\", \"availability\": \"Occupied\", \"type\": null, \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": \"FE788F000001\" }, { \"number\": \"B\", \"availability\": \"Occupied\", \"type\": null, \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": \"FE788F000002\" }, { \"number\": \"B\", \"availability\": \"Occupied\", \"type\": null, \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": \"FE788F000002\" }, { \"number\": \"C\", \"availability\": \"SeatDenied\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"SeatDenied\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"9\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"10\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"11\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"12\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"ExitRowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] } ] }, { \"startingRow\": \"13\", \"endingRow\": \"26\", \"seatMapRows\": [ { \"rowNumber\": \"13\", \"seats\": [ { \"number\": null, \"availability\": \"NotApplicable\", \"type\": null, \"price\": 0, \"confortSeat\": false, \"passengerId\": null, \"occupatoDa\": null } ] }, { \"rowNumber\": \"14\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"15\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"16\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"17\", \"seats\": [ { \"number\": null, \"availability\": \"NotApplicable\", \"type\": null, \"price\": 0, \"confortSeat\": false, \"passengerId\": null, \"occupatoDa\": null } ] }, { \"rowNumber\": \"18\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"19\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"20\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"21\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"22\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"23\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"24\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"25\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"26\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] } ] }, { \"startingRow\": \"27\", \"endingRow\": \"31\", \"seatMapRows\": [ { \"rowNumber\": \"27\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"28\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"29\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"30\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"SeatDenied\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"SeatDenied\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"SeatDenied\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"SeatDenied\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"SeatDenied\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"SeatDenied\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] }, { \"rowNumber\": \"31\", \"seats\": [ { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"A\", \"availability\": \"Free\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"B\", \"availability\": \"Free\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"C\", \"availability\": \"Free\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"SeatDenied\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"D\", \"availability\": \"SeatDenied\", \"type\": \"AisleSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"SeatDenied\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"E\", \"availability\": \"SeatDenied\", \"type\": \"CenterSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"SeatDenied\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000002\", \"occupatoDa\": null }, { \"number\": \"F\", \"availability\": \"SeatDenied\", \"type\": \"WindowSeat\", \"price\": 0, \"confortSeat\": false, \"passengerId\": \"FE788F000001\", \"occupatoDa\": null } ] } ] } ], \"seatMapColumns\": [ { \"column\": \"A\", \"typeCharacteristics\": \"Window\" }, { \"column\": \"B\", \"typeCharacteristics\": \"\" }, { \"column\": \"C\", \"typeCharacteristics\": \"Aisle\" }, { \"column\": \"D\", \"typeCharacteristics\": \"Aisle\" }, { \"column\": \"E\", \"typeCharacteristics\": \"\" }, { \"column\": \"F\", \"typeCharacteristics\": \"Window\" } ] } }, \"conversationID\": \"1\" }";
//			checkinEnhancedSeatMapresponse = checkinMessageUnmarshal.CheckinEnhancedSeatMapUnmarshal(jsonString.trim());
//
//
//			//d++; //questo contatore indica quante volte viene invocato il delegate a seconda del numero dei segements 
//			//per il momento commentato
//			
//
//			//Viene Costruita una matrice di settori riguardante il volo ed i posti
//			matrixFlightsSectorRender = new ArrayList<>();
//
//			ArrayList<FlightsSectorRender> listFlightsSectorRender  =  new ArrayList<>();
//
//			FlightsSectorRender flightsSectorRender = new FlightsSectorRender();
//
//			flightsSectorRender.setAirline(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSegmentSeat().getAirline());
//			flightsSectorRender.setFlight(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSegmentSeat().getFlight());
//			flightsSectorRender.setFlightStatus(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSegmentSeat().getFlightStatus());
//			flightsSectorRender.setCheckInGate(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSegmentSeat().getCheckInGate());
//			flightsSectorRender.setOrigin(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSegmentSeat().getOrigin().getCode());
//			flightsSectorRender.setDestination(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSegmentSeat().getDestination().getCode());
//			flightsSectorRender.setDepartureDate(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSegmentSeat().getDepartureDate());
//			flightsSectorRender.setDescriptionAirBus("AIRBUS 666");
//
//			String structureNumericMap = structureMapSeats(checkinEnhancedSeatMapresponse);
//
//			ArrayList<PassengerRender> listPassengerRender = new ArrayList<>();	
//
//			boolean result = true;
//
//			//if (ctx.checkinSelectedPassengers != null){commentato per test
//			if(result){
//				for (int p = 0; p < 2; p++){
//					//for (int p = 0; p < ctx. checkinSelectedPassengers.size(); p++){commentato per test
//					PassengerRender passengerRender = new PassengerRender();
//                    if(p==0){
//                        passengerRender.setNome("marcob");
//                        passengerRender.setCognome("marcob");
//                        passengerRender.setPassengerId("FE788F000002");
//                        passengerRender.setFrequentFlyer("MyAlitalia");
//                        passengerRender.setCodeFrequentFlyer("MM");
//                        passengerRender.setPostoAssegnato(true);
//                        passengerRender.setSeatCI("A30");
//                        passengerRender.setNewSeat("");
//                        passengerRender.setNumberFrequentFlyer("1234567890");
//                    }else{
//                        passengerRender.setNome("francesco");
//                        passengerRender.setCognome("fra");
//                        passengerRender.setPassengerId("FE788F000001");
//                        passengerRender.setFrequentFlyer("MyAlitalia");
//                        passengerRender.setCodeFrequentFlyer("MM");
//                        passengerRender.setPostoAssegnato(true);
//                        passengerRender.setSeatCI("B30");
//                        passengerRender.setNewSeat("");
//                        passengerRender.setNumberFrequentFlyer("0987654321");
//                    }
//
//
//
//					//					passengerRender.setNome(ctx.checkinSelectedPassengers.get(p).getNome());
//					//					passengerRender.setCognome(ctx.checkinSelectedPassengers.get(p).getCognome());
//					//					passengerRender.setPassengerId(ctx.checkinSelectedPassengers.get(p).getPassengerID());
//					//					passengerRender.setFrequentFlyer(ctx.checkinSelectedPassengers.get(p).getFrequentFlyer().get(0).getDescription());
//					//					passengerRender.setCodeFrequentFlyer(ctx.checkinSelectedPassengers.get(p).getFrequentFlyer().get(0).getCode());
//					//					passengerRender.setPostoAssegnato(ctx.checkinSelectedPassengers.get(p).getPostoAssegnato());
//					//					passengerRender.setSeatCI(ctx.checkinSelectedPassengers.get(p).getNumeroPostoAssegnato());
//					//					passengerRender.setNumberFrequentFlyer(ctx.checkinSelectedPassengers.get(p).getFrequentFlyer().get(0).getNumber());
//					//					passengerRender.setNewSeat("");
//
//
//					int sectors = checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().size();
//
//					ArrayList<SectorRender> listSectorsRender = new ArrayList<>(); 
//					for (int i = 0; i < sectors; i++){
//						SectorRender sectorRender = new SectorRender();
//						int sectorsStartRow = Integer.parseInt(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getStartingRow());
//						int sectorsEndingRow = Integer.parseInt(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getEndingRow());
//
//						sectorRender.setStartRow(String.valueOf(sectorsStartRow));
//						sectorRender.setEndRow(String.valueOf(sectorsEndingRow));
//						sectorRender.setStructureMap(structureNumericMap);
//
//						//for (int s = sectorsStartRow; s <= sectorsEndingRow; s++){
//						// ***** START : THEAD per AEM Seat Map *****
//						ArrayList<THead> listTHead = new ArrayList<>(); 
//						String[] items = structureNumericMap.split("_");
//						int aisles = items.length - 1; //corridoi
//
//						//exitEmergency1
//						THead theadEmergency1 = new THead();
//						theadEmergency1.setColumn("SC");
//						theadEmergency1.setTypeCharacteristics("Exit Emergency");
//						listTHead.add(theadEmergency1);
//
//						//corpo
//						int cont1 = 0;
//						int cont2 = 0;
//						for (int u = 0; u < items.length; u++) {
//							cont1 = cont1 + Integer.parseInt(items[u]);
//
//							for(int c = cont2; c < cont1; c++){
//								THead theadCorpo = new THead();
//								theadCorpo.setColumn(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapColumns().get(c).getColumn());
//								theadCorpo.setTypeCharacteristics(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapColumns().get(c).getTypeCharacteristics());
//								listTHead.add(theadCorpo);
//								cont2 = cont1;
//							}
//
//							if(u < aisles){
//								THead theadAisles = new THead();
//								theadAisles.setColumn("SC");
//								theadAisles.setTypeCharacteristics("");
//								listTHead.add(theadAisles);
//							}
//						}
//
//						//exitEmergency1
//						THead theadEmergency2 = new THead();
//						theadEmergency2.setColumn("SC");
//						theadEmergency2.setTypeCharacteristics("Exit Emergency");
//						listTHead.add(theadEmergency2);
//
//						sectorRender.setThead(listTHead);
//
//						// ***** END : THEAD per AEM Seat Map *****
//
//						// ***** START : TBODY per AEM Seat Map *****
//						ArrayList<ArrayList> listTBody = new ArrayList<>(); 
//						
//
//						int struttura= checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapColumns().size();
//						for (int s= 0; s < checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().size(); s++){
//							ArrayList<SeatRender> listSeatsRender = new ArrayList<>(); //questo array deve essere inizializzato ad ogni giro//
//							String rowNumber = checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getRowNumber();
//							if (struttura == checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().size()){
//									
//								//exitEmergency1
//								SeatRender seatEmergency1 = new SeatRender();
//								seatEmergency1.setNumber("");
//								seatEmergency1.setPassengerId(""); 
//								seatEmergency1.setOccupatoDa("");
//								seatEmergency1.setPrice("");
//								seatEmergency1.setConfortSeat(false); 
//								seatEmergency1.setType("SC");
//								seatEmergency1.setAvailability("");
//								listSeatsRender.add(seatEmergency1);
//
//									//corpo
//									int c1 = 0;
//									int c2 = 0;
//									for (int u = 0; u < items.length; u++) {
//										c1 = c1 + Integer.parseInt(items[u]);
//
//										for(int c = c2; c < c1; c++){
//											String passengerId = "";
//											passengerId = checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(c).getPassengerId().trim();
//											if(passengerId.equals(passengerRender.getPassengerId())){
//												SeatRender seatCorpo = new SeatRender();
//												seatCorpo.setNumber(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(c).getNumber() + rowNumber);
//                                                seatCorpo.setRow(rowNumber);
//                                                seatCorpo.setColumn(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(c).getNumber());
//
//												seatCorpo.setPassengerId(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(c).getPassengerId());
//												seatCorpo.setOccupatoDa(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(c).getOccupatoDa());
//												seatCorpo.setPrice(String.valueOf(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(c).getPrice()));
//												seatCorpo.setConfortSeat(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(c).getConfortSeat());
//												seatCorpo.setType(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(c).getType());
//												seatCorpo.setAvailability(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(c).getAvailability());
//												listSeatsRender.add(seatCorpo);
//												c2 = c1;
//											}
//										}
//
//										if(u < aisles){
//											SeatRender seatAisles = new SeatRender();
//											seatAisles.setNumber("");
//											seatAisles.setPassengerId(""); 
//											seatAisles.setOccupatoDa("");
//											seatAisles.setPrice("");
//											seatAisles.setConfortSeat(false); 
//											seatAisles.setType("SC");
//											seatAisles.setAvailability("");
//											listSeatsRender.add(seatAisles);
//										}
//									}
//								//exitEmergency1
//								SeatRender seatEmergency2 = new SeatRender();
//								seatEmergency1.setNumber("");
//								seatEmergency1.setPassengerId(""); 
//								seatEmergency1.setOccupatoDa("");
//								seatEmergency1.setPrice("");
//								seatEmergency1.setConfortSeat(false); 
//								seatEmergency1.setType("SC");
//								seatEmergency1.setAvailability("");
//								listSeatsRender.add(seatEmergency2);
//							//E' possibile che la seatMapRow sia formata solo dal rowNumber senza alcun parmaetro valorizzato	
//							}else if(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().size() == 1) {
//								//exitEmergency1
//								SeatRender seatEmergency1 = new SeatRender();
//								seatEmergency1.setNumber("");
//								seatEmergency1.setPassengerId(""); 
//								seatEmergency1.setOccupatoDa("");
//								seatEmergency1.setPrice("");
//								seatEmergency1.setConfortSeat(false); 
//								seatEmergency1.setType("SC");
//								seatEmergency1.setAvailability("");
//								listSeatsRender.add(seatEmergency1);
//
//									//corpo
//									int c1 = 0;
//									int c2 = 0;
//									for (int u = 0; u < items.length; u++) {
//										c1 = c1 + Integer.parseInt(items[u]);
//
//										for(int c = c2; c < c1; c++){
//												SeatRender seatCorpo = new SeatRender();
//												seatCorpo.setNumber(rowNumber);
//												seatCorpo.setPassengerId("");
//												seatCorpo.setOccupatoDa("");
//												seatCorpo.setPrice("");
//												seatCorpo.setConfortSeat(false);
//												seatCorpo.setType("SC");
//												seatCorpo.setAvailability("");
//												listSeatsRender.add(seatCorpo);
//												c2 = c1;
//										}
//
//										if(u < aisles){
//											SeatRender seatAisles = new SeatRender();
//											seatAisles.setNumber("");
//											seatAisles.setPassengerId(""); 
//											seatAisles.setOccupatoDa("");
//											seatAisles.setPrice("");
//											seatAisles.setConfortSeat(false); 
//											seatAisles.setType("SC");
//											seatAisles.setAvailability("");
//											listSeatsRender.add(seatAisles);
//										}
//									}
//									
//								//exitEmergency1
//								SeatRender seatEmergency2 = new SeatRender();
//								seatEmergency1.setNumber("");
//								seatEmergency1.setPassengerId(""); 
//								seatEmergency1.setOccupatoDa("");
//								seatEmergency1.setPrice("");
//								seatEmergency1.setConfortSeat(false); 
//								seatEmergency1.setType("SC");
//								seatEmergency1.setAvailability("");
//								listSeatsRender.add(seatEmergency2);
//								
//							}else{
//
//
//								List<SeatRender> listSeatRenderOccupiedForPassenger= new ArrayList<>() ; 
//								listSeatRenderOccupiedForPassenger = seatOccupiedForPassenger(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats(),passengerRender.getPassengerId());
//
//
//									//exitEmergency1
//									SeatRender seatEmergency1 = new SeatRender();
//									seatEmergency1.setNumber("");
//									seatEmergency1.setPassengerId(""); 
//									seatEmergency1.setOccupatoDa("");
//									seatEmergency1.setPrice("");
//									seatEmergency1.setConfortSeat(false); 
//									seatEmergency1.setType("SC");
//									seatEmergency1.setAvailability("");
//									listSeatsRender.add(seatEmergency1);
//
//
//									//corpo
//									int c1 = 0;
//									int c2 = 0;
//									for (int u = 0; u < items.length; u++) {
//										c1 = c1 + Integer.parseInt(items[u]);
//
//										for(int c = c2; c < c1; c++){
//											String passengerId = "";
//											passengerId = listSeatRenderOccupiedForPassenger.get(c).getPassengerId().trim();
//											if(passengerId.equals(passengerRender.getPassengerId())){
//												SeatRender seatCorpo = new SeatRender();
//                                                seatCorpo.setRow(rowNumber);
//                                                seatCorpo.setColumn(listSeatRenderOccupiedForPassenger.get(c).getNumber());
//												seatCorpo.setNumber(listSeatRenderOccupiedForPassenger.get(c).getNumber() + rowNumber);
//												seatCorpo.setPassengerId(listSeatRenderOccupiedForPassenger.get(c).getPassengerId());
//												seatCorpo.setOccupatoDa(listSeatRenderOccupiedForPassenger.get(c).getOccupatoDa());
//												seatCorpo.setPrice(String.valueOf(listSeatRenderOccupiedForPassenger.get(c).getPrice()));
//												seatCorpo.setConfortSeat(listSeatRenderOccupiedForPassenger.get(c).getConfortSeat());
//												seatCorpo.setType(listSeatRenderOccupiedForPassenger.get(c).getType());
//												seatCorpo.setAvailability(listSeatRenderOccupiedForPassenger.get(c).getAvailability());
//												seatCorpo.setCssClass(setCssClass(seatCorpo, passengerRender));
//												listSeatsRender.add(seatCorpo);
//												c2 = c1;
//											}	
//										}
//
//										if(u < aisles){
//											SeatRender seatAisles = new SeatRender();
//											seatAisles.setNumber("");
//											seatAisles.setPassengerId(""); 
//											seatAisles.setOccupatoDa("");
//											seatAisles.setPrice("");
//											seatAisles.setConfortSeat(false); 
//											seatAisles.setType("SC");
//											seatAisles.setAvailability("");
//											listSeatsRender.add(seatAisles);
//										}
//									}
//
//									//exitEmergency1
//									SeatRender seatEmergency2 = new SeatRender();
//									seatEmergency1.setNumber("");
//									seatEmergency1.setPassengerId(""); 
//									seatEmergency1.setOccupatoDa("");
//									seatEmergency1.setPrice("");
//									seatEmergency1.setConfortSeat(false); 
//									seatEmergency1.setType("SC");
//									seatEmergency1.setAvailability("");
//									listSeatsRender.add(seatEmergency2);
//							}
//
//							listTBody.add(s, listSeatsRender);
//						}
//
//						// ***** END : TBODY per AEM Seat Map *****
//						sectorRender.setTBody(listTBody);
//						//}		
//						listSectorsRender.add(sectorRender);
//					}
//
//					passengerRender.setListSectorRender(listSectorsRender);
//					listPassengerRender.add(passengerRender);
//				}
//			}
//
//
//			flightsSectorRender.setPassengerRender(listPassengerRender);
//			listFlightsSectorRender.add(flightsSectorRender);
//			this.matrixFlightsSectorRender.add(d, listFlightsSectorRender);
//
//
//			enhancedSeatMapObj.set_enhancedseatmapResp(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp());
//			this.getEnhancedSeatMapResp().add(enhancedSeatMapObj.get_enhancedseatmapResp());
//			//checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMAp()
//
//		}catch (Exception e) {
//			logger.error("[CheckinESeatMap][initModel] - errore nella chiamata al Delegate da CheckinESeatMap");
//			e.getMessage();
//		}
//	}
//
//    private String setCssClass(SeatRender s, PassengerRender p){
//	    String cssClass="";
//	    if(p.getSeatCI().equals(s.getNumber())){
//            return "seatAssigned";
//        }
//	    if(s.getAvailability().equals("Occupied")){
//	        return "occupied";
//        }
//        if(s.getAvailability().equals("SeatDenied")){
//            return "occupied";
//        }
//        if(s.getAvailability().equals("NotApplicable")){
//            return "occupied";
//        }
//        if(s.getAvailability().equals("Free")){
//            if(s.getConfortSeat()){
//                return "extraComfort";
//            }else{return "available";}
//        }
//	    return cssClass;
//    }
//
//	private List<SeatRender> seatOccupiedForPassenger(List<Seat> seats, String passengerID) {
//		List<SeatRender> listSeatRenderOccupiedForPassenger= new ArrayList<>(); 
//
//        for (int s = 0; s < seats.size(); s++){
//            SeatRender seatRender = new SeatRender();
//
//            if(seats.get(s).getPassengerId().equals(passengerID)){
//                seatRender.setNumber(seats.get(s).getNumber());
//                seatRender.setPassengerId(seats.get(s).getPassengerId());
//                seatRender.setOccupatoDa(seats.get(s).getOccupatoDa());
//                seatRender.setPrice(String.valueOf(seats.get(s).getPrice()));
//                seatRender.setConfortSeat(seats.get(s).getConfortSeat());
//                seatRender.setType(seats.get(s).getType());
//                seatRender.setAvailability(seats.get(s).getAvailability());
//                listSeatRenderOccupiedForPassenger.add(seatRender);
//            }
//        }
//		return listSeatRenderOccupiedForPassenger;
//	}
//
//
//
//	private String structureMapSeats(CheckinEnhancedSeatMapResponse checkinEnhancedSeatMapresponse) {
//		List<SeatMapColumn> posti = checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getSeatMaps().getSeatMapColumns();
//		String structureMapSeats = "";
//
//		String rigaPosti = "";
//
//		int numPosti = 1;
//		boolean isCorridoio;
//		boolean newCorridoio = false;
//		//      int postiTemp = 1;
//
//		for(int i=0; i<posti.size(); i++){
//			isCorridoio = posti.get(i).getTypeCharacteristics().equals(CheckinRestTypeSeatEnum.AISLE.value()) ? true : false;
//
//			if(isCorridoio){
//				if(rigaPosti.equals("") || !newCorridoio){
//					rigaPosti += numPosti + "_";
//					newCorridoio = true;
//				}
//				else if(newCorridoio){
//					numPosti = 1;
//					newCorridoio = false;
//				}
//				else{
//					//                           rigaPosti += numPosti;
//					newCorridoio = false;
//				}
//			}
//			if(i == posti.size() - 1){
//				rigaPosti += numPosti;
//			}
//			numPosti++;
//		}
//		System.out.println("RIGA:" + rigaPosti);
//		structureMapSeats = rigaPosti;
//		return structureMapSeats;
//	}
//
//	//	private String[] StringSplit (String input) {
//	//
//	//		String[] items = input.split("_");
//	//
//	//		for (String item : items) {
//	//
//	//
//	//
//	//		}
//	//		return items;
//	//
//	//	}
//
//
//
//
//	public List<EnhancedSeatMapResp> getEnhancedSeatMapResp() {
//		return enhancedSeatMapResp;
//	}
//
//	public void setEnhancedSeatMapResp(List<EnhancedSeatMapResp> enhancedSeatMapResp) {
//		this.enhancedSeatMapResp = enhancedSeatMapResp;
//	}
//
//	public Flight getSelectedFlight() {
//		return selectedFlight;
//	}
//
//	public void setSelectedFlight(Flight selectedFlight) {
//		this.selectedFlight = selectedFlight;
//	}
//
//	//	public void MatriceSeatMap(){
//	//		
//	//		ArrayList<ArrayList> matriceSeatMap = new ArrayList<>();
//	//
//	//		ArrayList<String> firstRow = new ArrayList<>();
//	//		firstRow.add(0, "");
//	//		firstRow.add(1, "");
//	//		firstRow.add(2, "");
//	//		//[0] [1]
//	//		//...
//	//		//[0] [n]
//	//		//**0
//	//		matriceSeatMap.add(0, firstRow);
//	//		
//	//		ArrayList<String> secondRow = new ArrayList<>();
//	//		secondRow.add(0, "");
//	//		secondRow.add(1, "");
//	//		secondRow.add(2, "");
//	//		
//	//		//**1
//	//		matriceSeatMap.add(1, secondRow);
//	//		
//	//		ArrayList<String> thirdRow = new ArrayList<>();
//	//		thirdRow.add(0, "");
//	//		thirdRow.add(1, "");
//	//		thirdRow.add(2, "");
//	//		
//	//		//**2
//	//		matriceSeatMap.add(2, thirdRow);
//	//		
//	//	}
//
//
//
//	public ArrayList<ArrayList> getMatrixFlightsSectorRender() {
//		return matrixFlightsSectorRender;
//	}
//
//	public void setMatrixFlightsSectorRender(ArrayList<ArrayList> matrixFlightsSectorRender) {
//		this.matrixFlightsSectorRender = matrixFlightsSectorRender;
//	}
//
//
//
}
