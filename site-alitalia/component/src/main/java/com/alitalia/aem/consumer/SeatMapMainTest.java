package com.alitalia.aem.consumer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SeatMapMainTest {

	public static void main(String[] args) {

		try {
			
			String queryString =  "{\"listItemsFareAvailQualifiers\": [ {\"givenName\": \"string\", \"surname\": "
					+ "\"string\"} ], \"flight\": \"string\", \"bookingClass\": \"string\", "
					+ "\"origin\": \"string\", \"destination\": \"string\", \"departureDate\": \"string\", "
					+ "\"airline\": \"string\", \"language\": \"string\", \"market\": \"string\", \"conversationID\": \"string\"}";
			
			URL url = new URL("http://172.31.251.138:9002/api/v0.1/checkin/enhancedseatmap");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			byte[] outputInBytes = queryString.getBytes("UTF-8");
			
			OutputStream os = conn.getOutputStream();
			os.write( outputInBytes );    
			os.close();

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String output;
			
			System.out.println("Output from Server .... \n");
			
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			conn.disconnect();

		} catch (MalformedURLException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

} 