package com.alitalia.aem.consumer.mmb.render;

import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;

public class MmbAncillaryMealRender {
	
	private String identifier = "";
	private boolean displayInFeedback = false;
	private MmbAncillariesGroup ancillariesGroup = null;
	private String fullName = "";
	private boolean hasInfant = false;
	private boolean child = false;
	private boolean allowModify = false;
	private String currentValue = null;
	private String currentValueLabel = null;
	
	public MmbAncillaryMealRender(MmbAncillariesGroup ancillariesGroup) {
		super();
		this.ancillariesGroup = ancillariesGroup;
	}

	public String getIdentifier() {
		return this.identifier;
	}
	
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	public boolean isDisplayInFeedback() {
		return displayInFeedback;
	}

	public void setDisplayInFeedback(boolean displayInFeedback) {
		this.displayInFeedback = displayInFeedback;
	}
	
	public MmbAncillariesGroup getAncillariesGroup() {
		return ancillariesGroup;
	}
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public boolean isHasInfant() {
		return hasInfant;
	}

	public void setHasInfant(boolean hasInfant) {
		this.hasInfant = hasInfant;
	}
	
	public boolean isChild() {
		return child;
	}

	public void setChild(boolean child) {
		this.child = child;
	}

	public boolean isAllowModify() {
		return allowModify;
	}

	public void setAllowModify(boolean allowModify) {
		this.allowModify = allowModify;
	}

	public String getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(String currentValue) {
		this.currentValue = currentValue;
	}
	
	public String getCurrentValueLabel() {
		return currentValueLabel;
	}

	public void setCurrentValueLabel(String currentValueLabel) {
		this.currentValueLabel = currentValueLabel;
	}
	
}
