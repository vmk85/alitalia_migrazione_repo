/** Controller MyAlitali per Customer Loggato
 * ( Questo controller viene richiamato in pagina per compilare i campi relativi ai dati dell'utente )
 * [D.V.] */

package com.alitalia.aem.consumer.myalitalia.controller;

import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.messages.crmdatarest.GetCrmDataInfoResponse;
import com.alitalia.aem.common.utils.StringUtils;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.myalitalia.model.GenericMyAlitaliaModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = {SlingHttpServletRequest.class})
public class MaCustomerProfileController extends GenericMyAlitaliaModel {

    @Self
    private SlingHttpServletRequest request;

    @Inject
    private SlingHttpServletResponse response;

    @Inject
    protected AlitaliaConfigurationHolder configuration;

    private MACustomer maCustomer;

    private GetCrmDataInfoResponse getCrmDataInfoResponse;

    @PostConstruct
    protected void initModel() throws Exception {

        try {

            /** Inizializzo il contesto tramite la classe GenericMyAlitaliaModel */
            super.initBaseModel(request);

            /** Setto localmente la variabile maCustomer contenente i dati utente loggato  */
            if(mactx != null){
                this.maCustomer = mactx.getMaCustomer();
                this.getCrmDataInfoResponse = mactx.getGetCrmDataInfoResponse();
                this.maCustomer.getProfile().setFirstName(StringUtils.capitalize(this.getCrmDataInfoResponse.getInfoCliente().getNome()));
                this.maCustomer.getProfile().setLastName(StringUtils.capitalize(this.getCrmDataInfoResponse.getInfoCliente().getCognome()));
                this.maCustomer.getProfile().setEmail(StringUtils.capitalize(this.getCrmDataInfoResponse.getEmail()));
            }

            if(maCustomer == null && !isWCMEnabled()) {
                String protocol = "http://";
                String logoutLandingPage = protocol + getExternalDomain() + request.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
/*
                response.sendRedirect(logoutLandingPage);
*/
            }


        } catch (Exception e) {

            /* AMS Bugfix #5045 Utente non loggato visualizza nome di un altro utente MyAlitalia */
            String requestQueryString = request.getResource().getPath();
            myAlitaliaSessionLog.info(": " + requestQueryString);
            if(!isWCMEnabled() && requestQueryString.contains("myalitalia/myalitalia-")){
                String baseUrl = request.getResource().getResourceResolver().map(
                        AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
                response.sendRedirect(baseUrl);
                myAlitaliaSessionLog.error("Errore Caricamento MaCustomerProfileController" + e.toString());
            }
            /* Fine Bugfix #5045 */

        }
    }

    public MACustomer getMaCustomer() {
        return maCustomer;
    }

    public void setMaCustomer(MACustomer maCustomer) {
        this.maCustomer = maCustomer;
    }

    public String getExternalDomain() {
        return configuration.getExternalDomain();
    }

    public GetCrmDataInfoResponse getGetCrmDataInfoResponse() {
        return getCrmDataInfoResponse;
    }

    public void setGetCrmDataInfoResponse(GetCrmDataInfoResponse getCrmDataInfoResponse) {
        this.getCrmDataInfoResponse = getCrmDataInfoResponse;
    }
}
