package com.alitalia.aem.consumer.booking.render;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.DirectFlightData;

public class DirectFlightRender{
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private String durata;
	private DateRender dataPartenza;
	private DateRender dataArrivo;
	private SeatsMapRender mappaPosti;
	private boolean mappaPostiEnabled;
	private long miglia;
	private DirectFlightData directFlightData;
	
	public DirectFlightRender(DirectFlightData directFlightData) {
		this.directFlightData = directFlightData;
		init();
	}

	private void init() {
		
		//SET DURATA
		int durationHours = directFlightData.getDurationHour();
		int durationMinutes = directFlightData.getDurationMinutes();
		String minutes = Integer.toString(durationMinutes);
		String hours = Integer.toString(durationHours);
		if(durationMinutes<10){
			minutes = "0" + minutes;
		}
		durata = hours + "H:" + minutes;
		
		//SET DateRender
		this.dataPartenza = new DateRender(directFlightData.getDepartureDate());
		this.dataArrivo = new DateRender(directFlightData.getArrivalDate());
		
		//SET mappaPosti
		this.mappaPosti = new SeatsMapRender();
		
		miglia = 0; //TODO chiamare altro servizio
		
	}
	
	public String getClasse() {
		if (directFlightData != null && directFlightData.getCabin() != null) {
			return directFlightData.getCabin().value();
		} else {
			return "";
		}
	}

	public long getGiorniDopo() {
		try {
			long partenza = directFlightData.getDepartureDate().getTimeInMillis();
			long arrivo = directFlightData.getArrivalDate().getTimeInMillis();
			return (arrivo - partenza)/(1000*3600*24);
		} catch (Exception e) {
			logger.error("Errore calcolo giorni dopo", e);
		}
		return 0;
	}
	
	public DirectFlightData getDirectFlightData() {
		return directFlightData;
	}

	public String getDurata() {
		return durata;
	}

	public DateRender getDataPartenza() {
		return dataPartenza;
	}

	public void setDataPartenza(DateRender dataPartenza) {
		this.dataPartenza = dataPartenza;
	}

	public DateRender getDataArrivo() {
		return dataArrivo;
	}

	public void setDataArrivo(DateRender dataArrivo) {
		this.dataArrivo = dataArrivo;
	}
	
	public SeatsMapRender getMappaPosti() {
		return mappaPosti;
	}
	
	public void setMappaPosti(SeatsMapRender mappaPosti) {
		this.mappaPosti = mappaPosti;
	}

	public long getMiglia() {
		return miglia;
	}

	public void setMiglia(long miglia) {
		this.miglia = miglia;
	}
	
	
	public boolean getMappaPostiEnabled() {
		return mappaPostiEnabled;
	}

	public void setMappaPostiEnabled(boolean mappaPostiEnabled) {
		this.mappaPostiEnabled = mappaPostiEnabled;
	}
	
}
