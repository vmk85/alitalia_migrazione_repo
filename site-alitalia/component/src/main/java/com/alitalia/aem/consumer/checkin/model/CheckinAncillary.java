package com.alitalia.aem.consumer.checkin.model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinAncillary extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	private List<CheckinPassengerData> notCheckedInPassengers;
	private boolean nonePassengerAccepted;
	private boolean somePassengerNotAccepted;
	private String linkPayment;
	private boolean showSeatMap;
	private boolean sendMailEnabled;
	private boolean checkinDone;
	private String flightListUrl;
	
	private static final String LIGHT = "PC0";
	
	@PostConstruct
	protected void initModel() {
		
		try {
			initBaseModel(request);
			this.notCheckedInPassengers = ctx.notCheckedInPassengers != null ? ctx.notCheckedInPassengers : 
				new ArrayList<CheckinPassengerData>();
			if(ctx.selectedPassengers != null){
				nonePassengerAccepted = (this.notCheckedInPassengers.size() == ctx.selectedPassengers.size() ? true : false);
			}

			somePassengerNotAccepted = this.notCheckedInPassengers.size() != 0 ? true : false;
			linkPayment = AlitaliaUtils.findSiteBaseExternalUrl(
					request.getResource(), false)
					+ configuration.getCheckinPaymentPage();
			
			showSeatMap = ctx.seatMaps != null && ! ctx.seatMaps.isEmpty();
			
			// CHECK IF THERE IS AT LEAST ONE PASSENGER WITH LIGHT FARE (IN THAT CASE ENABLE THE SEND MAIL BUTTON)
			sendMailEnabled = false;
			for (CheckinPassengerData passenger : this.notCheckedInPassengers) {
				if (passenger.getBaggageAllowance() != null) {
					String fare = passenger.getBaggageAllowance().toUpperCase();
					if (LIGHT.equals(fare)) {
						sendMailEnabled = true;
						break;
					}
				}
			}
			
			checkinDone = ctx.checkinDone;
			String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
			this.flightListUrl = baseUrl + configuration.getCheckinFlightListPage();
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}
	}


	public List<CheckinPassengerData> getNotCheckedInPassengers() {
		return notCheckedInPassengers;
	}

	public boolean isNonePassengerAccepted() {
		return nonePassengerAccepted;
	}

	public boolean isSomePassengerNotAccepted() {
		return somePassengerNotAccepted;
	}
	
	public String getLinkPayment(){
		return linkPayment;
	}

	public boolean isShowSeatMap() {
		return showSeatMap;
	}

	public boolean isSendMailEnabled() {
		return sendMailEnabled;
	}

	public boolean isCheckinDone() {
		return checkinDone;
	}

	public String getFlightListUrl() {
		return flightListUrl;
	}
}
