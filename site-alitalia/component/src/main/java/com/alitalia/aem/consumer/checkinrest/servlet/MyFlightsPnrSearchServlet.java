package com.alitalia.aem.consumer.checkinrest.servlet;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PnrListInfoByPnrSearch;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Segment;
import com.alitalia.aem.common.data.home.MyFlightsDataModel;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.gigya.GigyaSession;
import com.alitalia.aem.consumer.gigya.GigyaUtils;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSession;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSessionContext;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaUtils;
import com.alitalia.aem.consumer.myalitalia.model.MyFlightsModel;
import com.alitalia.aem.consumer.myalitalia.model.MyFlightsPnr;
import com.alitalia.aem.consumer.myalitalia.model.MyFlightsTicket;
import com.alitalia.aem.consumer.myalitalia.model.PassengerSelectedCheckin;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.alitalia.aem.web.component.webcheckin.delegaterest.IMyFlightsDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;
import com.gigya.socialize.GSResponse;
import com.google.gson.Gson;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = {"myflightspnrsearch", "myflightspnrsearchrefresh", "updatePassengerMyFlights","myflightsGetAllPnr"}),
        @Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
        @Property(name = "sling.servlet.extensions", value = {"json"}),
        @Property(name = "sling.servlet.methods", value = {"POST"})})
@SuppressWarnings("serial")
public class MyFlightsPnrSearchServlet extends GenericCheckinFormValidatorServlet {


    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile IMyFlightsDelegate myFlightsDelegateRest;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private volatile MyAlitaliaSession myAlitaliaSession;

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile ICheckinDelegate checkInDelegateRest;

    // logger
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    private AlitaliaConfigurationHolder configuration;

    @Reference
    private volatile GigyaSession gigyaSession;

    @Reference
    private CheckinSession checkinSession;

    private boolean repeate = true;

    @Override
    protected AlitaliaConfigurationHolder getConfiguration() {
        return configuration;
    }

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        Validator validator = new Validator();

//        String selectorString = request.getRequestPathInfo().getSelectorString();
//        if (selectorString.equals("myflightspnrsearch")) {
//
//            String pnr = request.getParameter("pnr");
//            String firstName = request.getParameter("firstName");
//            String lastName = request.getParameter("lastName");
//
//            validator.addDirectCondition("pnr", pnr,
//                    CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
//
//            validator.addDirectCondition("pnr", pnr,
//                    CheckinConstants.MESSAGE_ERROR_INVALID_PNR, "isPnrOrTicketNumber");
//
//            validator.addDirectCondition("firstName", firstName,
//                    CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
//
//            validator.addDirectCondition("firstName", firstName,
//                    CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
//
//            validator.addDirectCondition("lastName", lastName,
//                    CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
//
//            validator.addDirectCondition("lastName", lastName,
//                    CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
//        }


        ResultValidation resultValidation = validator.validate();
        return resultValidation;

    }

    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

        String selectorString = request.getRequestPathInfo().getSelectorString();

        logger.info("[CheckinPnrSearchServlet - performSubmit] recupero dati dal contesto.");

        String pnr = request.getParameter("pnr");
        String nome = request.getParameter("firstName");
        String cognome = request.getParameter("lastName");

        boolean inSession = false;

        MyAlitaliaSessionContext mactx = MyAlitaliaUtils.getMactxBySession(request);
        CheckinPnrSearchResponse serviceResponce = null;
        CheckinTicketSearchResponse checkinTicketSearchResponse = null;
        boolean add = true;

        switch (selectorString) {

            case "myflightsGetAllPnr":
                Gson gson2 = new Gson();
                String jsonInString2 = "";
                boolean isError = true;

                if (mactx.getMyFlightsModel() != null && mactx.getMyFlightsModel().getMyFlightsPnrs() != null && mactx.getMyFlightsModel().getMyFlightsPnrs().size() > 0){
                    jsonInString2 = gson2.toJson(mactx.getMyFlightsModel().getMyFlightsPnrs());
                    isError = false;
                }
                TidyJSONWriter json3;
                try {


                    json3 = new TidyJSONWriter(response.getWriter());

                    response.setContentType("application/json");

                    json3.object();
                    json3.key("data").value(jsonInString2);
                    json3.key("isError").value(isError);
                    json3.endObject();

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;

            case "myflightspnrsearch":

                try {

                    if (request.getParameter("pnr").length() == 6) {

                        if (mactx != null) {

                            if (mactx.getMyFlightsModel() == null) {

                                mactx.setMyFlightsModel(new MyFlightsModel());

                                if (mactx.getMyFlightsModel().getMyFlightsPnrs() == null) {

                                    mactx.getMyFlightsModel().setMyFlightsPnrs(new ArrayList<>());

                                }

                            } else {
                                if (mactx.getMyFlightsModel().getMyFlightsPnrs() != null) {
                                    for (MyFlightsPnr myFlightsPnr : mactx.getMyFlightsModel().getMyFlightsPnrs()) {
                                        CheckinPnrSearchResponse checkinPnrSearchResponse = myFlightsPnr.getCheckinPnrSearchResponse();
                                        if (checkinPnrSearchResponse.getPnrData() != null) {
                                            for (Pnr pnrRender : checkinPnrSearchResponse.getPnrData().getPnr()) {
                                                if (pnrRender.getNumber().equals(pnr)) {
                                                    inSession = true;
                                                    serviceResponce = checkinPnrSearchResponse;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (mactx.getMaCustomer().getData() != null){
                                if (mactx.getMaCustomer().getData().getFlights() != null){
                                    for(MyFlightsDataModel myFlightsDataModel : mactx.getMaCustomer().getData().getFlights()){
                                        if (myFlightsDataModel.getFlight_pnr().toUpperCase().equals(pnr.toUpperCase())){
                                            add = false;
                                            break;
                                        }
                                    }
                                } else {
                                    mactx.getMaCustomer().getData().setFlights(new ArrayList<>());
                                }
                            }

                            if (add){
                                MyFlightsDataModel myFlightsDataModel = new MyFlightsDataModel();
                                myFlightsDataModel.setFlight_pnr(pnr);
                                myFlightsDataModel.setFlight_lastName(cognome);
                                myFlightsDataModel.setFlight_firstName(nome);
                                mactx.getMaCustomer().getData().getFlights().add(myFlightsDataModel);
                             }
                        }

                        if (!inSession) {
                            try {
                                logger.info("[CheckinSession] costruzione Request...");
                                mactx = myAlitaliaSession.setSingleFlightToMactx(request, mactx.getMaCustomer(), mactx);
                                MyAlitaliaUtils.setMactxToSession(request, mactx );
                                for (MyFlightsPnr myFlightsPnr: mactx.getMyFlightsModel().getMyFlightsPnrs()){
                                    CheckinPnrSearchResponse checkinPnrSearchResponse = myFlightsPnr.getCheckinPnrSearchResponse();
                                    if (checkinPnrSearchResponse.getPnrData() != null && checkinPnrSearchResponse.getPnrData().getError() == null){
                                        if (checkinPnrSearchResponse.getPnrData().getPnr().get(0).getNumber().toUpperCase().equals(pnr.toUpperCase())){
                                            serviceResponce = checkinPnrSearchResponse;
                                        }
                                    }
                                }

                            } catch (Exception e) {
                                logger.error("[CheckinSession][checkInPnrSearch] : " + e.getMessage());
                            }
                        }


                        try {
                            this.repeate = true;

                            for (Segment segment : serviceResponce.getPnrData().getPnr().get(0).getFlights().get(Integer.parseInt(request.getParameter("flightitinerary"))).getSegments()) {
                                for (Passenger passenger : segment.getPassengers()) {
                                    if (passenger.getCheckInComplete() == true) {
                                        this.repeate = false;
                                    }
                                }
                            }
                        } catch (Exception e) {

                        }
                        if (serviceResponce != null) {
                            if (add){
                                JSONObject obj2 = new JSONObject();
                                obj2.put("flights", mactx.getMaCustomer().getData().getFlights());
                                GSResponse gsResponse = gigyaSession.setAccount(mactx.getMaCustomer().getUID(), GigyaUtils.mapJsonObjGigya(obj2.toString()));
                            }
                            if (serviceResponce.getPnrData().getError() != null && !serviceResponce.getPnrData().getError().equals("")) {
                                managementError(request, response, serviceResponce.getPnrData().getError());
                            } else {

                                // Se request format=json allora torno il json
                                if (request.getParameter("format") != null && request.getParameter("format").equals("json")) {
                                    TidyJSONWriter json;
                                    try {

                                        MyFlightsPnr myFlightsPnr = new MyFlightsPnr();
                                        myFlightsPnr.setCheckinPnrSearchResponse(serviceResponce);
                                        myFlightsPnr.setNome(nome);
                                        myFlightsPnr.setCognome(cognome);

                                        Gson gson = new Gson();
                                        String jsonInString = gson.toJson(myFlightsPnr);

                                        json = new TidyJSONWriter(response.getWriter());

                                        response.setContentType("application/json");

                                        json.object();
                                        json.key("data").value(jsonInString);
                                        json.key("inSession").value(inSession);
                                        json.endObject();

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    //response.sendRedirect(successPage);
                                    managementSucces(request, response);
                                }

                            }
                        } else {
                                //response.sendRedirect(successPage);
                                managementError(request, response,request.getSession().getAttribute("errorSearch").toString());
                        }

                    }

                    if (request.getParameter("pnr").length() == 13) {

                        CheckinTicketSearchResponse result = new CheckinTicketSearchResponse();


                        if (mactx != null) {

                            if (mactx.getMyFlightsModel() == null) {

                                mactx.setMyFlightsModel(new MyFlightsModel());

                                if (mactx.getMyFlightsModel().getMyFlightsTickets() == null) {

                                    mactx.getMyFlightsModel().setMyFlightsTickets(new ArrayList<>());

                                }

                            } else {
                                if (mactx.getMyFlightsModel().getMyFlightsTickets() != null) {
                                    for (MyFlightsTicket myFlightsTicket : mactx.getMyFlightsModel().getMyFlightsTickets()) {
                                        CheckinTicketSearchResponse checkinPnrSearchResponse = myFlightsTicket.getCheckinTicketSearchResponse();
                                        if (checkinPnrSearchResponse.getPnrData() != null) {

                                            for (Pnr pnrRender : checkinPnrSearchResponse.getPnrData().getPnr()) {
                                                if (pnrRender.getNumber().equals(pnr)) {
                                                    inSession = true;
                                                    result = checkinPnrSearchResponse;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (mactx.getMaCustomer().getData() != null){
                                if (mactx.getMaCustomer().getData().getFlights() != null){
                                    for(MyFlightsDataModel myFlightsDataModel : mactx.getMaCustomer().getData().getFlights()){
                                        if (myFlightsDataModel.getFlight_pnr().toUpperCase().equals(pnr.toUpperCase())){
                                            add = false;
                                        }
                                    }
                                } else {
                                    mactx.getMaCustomer().getData().setFlights(new ArrayList<>());
                                }
                            }

                            if (add){
                                MyFlightsDataModel myFlightsDataModel = new MyFlightsDataModel();
                                myFlightsDataModel.setFlight_pnr(pnr);
                                myFlightsDataModel.setFlight_lastName(cognome);
                                myFlightsDataModel.setFlight_firstName(nome);
                                mactx.getMaCustomer().getData().getFlights().add(myFlightsDataModel);
                            }
                        }

                        if (!inSession) {

                            boolean resultSession = initSession(request, configuration, checkinSession);
                            CheckinSessionContext ctx = getCheckInSessionContext(request);
                            logger.info("[CheckinPnrSearchServlet] invoco il checkinSession.checkinTicketSearch");
                            result = checkinSession.checkinTicketSearch(request, ctx);
                            logger.info("[CheckinPnrSearchServlet] result=" + result);
                            MyFlightsTicket myFlightsTicket = new MyFlightsTicket();
                            myFlightsTicket.setCheckinTicketSearchResponse(result);
                            myFlightsTicket.setNome(nome);
                            myFlightsTicket.setCognome(cognome);
                            mactx.getMyFlightsModel().getMyFlightsTickets().add(myFlightsTicket);
                            MyAlitaliaUtils.setMactxToSession(request, mactx);

                        }
                        try {
                            this.repeate = true;

                            for (Segment segment : result.getPnrData().getPnr().get(0).getFlights().get(Integer.parseInt(request.getParameter("flightitinerary"))).getSegments()) {
                                for (Passenger passenger : segment.getPassengers()) {
                                    if (passenger.getCheckInComplete() == true) {
                                        this.repeate = false;
                                    }
                                }
                            }
                        } catch (Exception e) {
                        }

                        if (result != null) {
                            if (result.getPnrData().getError() != null && !result.getPnrData().getError().equals("")) {
                                managementError(request, response, result.getPnrData().getError());
                            } else {
                                //response.sendRedirect(successPage);
                                managementSucces(request, response);
                            }
                        } else {
                            logger.error("[CheckinPnrSearchServlet] - Errore durante l'invocazione del servizio.");
                        }
                    }


                } catch (Exception e) {
                    throw new RuntimeException("[CheckinPnrSearchServlet] - Errore durante l'invocazione del servizio.", e);
                }

                break;
            case "myflightspnrsearchrefresh":

                PassengerSelectedCheckin passengerSelectedCheckin = mactx.getMyFlightsModel().getPassengerSelectedCheckin();

                try {

                    if (passengerSelectedCheckin.getPnr().length() == 6) {

                        List<MyFlightsPnr> myFlightsPnrs = new ArrayList<>();
                        CheckinPnrSearchRequest checkinPnrSearchRequest = new CheckinPnrSearchRequest(IDFactory.getTid(), IDFactory.getSid(request));

                        if (mactx != null) {

                            if (mactx.getMyFlightsModel() == null) {

                                mactx.setMyFlightsModel(new MyFlightsModel());

                                if (mactx.getMyFlightsModel().getMyFlightsPnrs() == null) {

                                    mactx.getMyFlightsModel().setMyFlightsPnrs(new ArrayList<>());
                                }

                            } else {
                                if (mactx.getMyFlightsModel().getMyFlightsPnrs() != null) {
                                    for (MyFlightsPnr myFlightsPnr : mactx.getMyFlightsModel().getMyFlightsPnrs()) {
                                        CheckinPnrSearchResponse checkinPnrSearchResponse = myFlightsPnr.getCheckinPnrSearchResponse();
                                        if (checkinPnrSearchResponse.getPnrData() != null) {
                                            for (Pnr pnrRender : checkinPnrSearchResponse.getPnrData().getPnr()) {
                                                if (!pnrRender.getNumber().equals(passengerSelectedCheckin.getPnr())) {
                                                    MyFlightsPnr myFlightsPnr1 = new MyFlightsPnr();
                                                    myFlightsPnr1.setCheckinPnrSearchResponse(checkinPnrSearchResponse);
                                                    myFlightsPnr1.setNome(passengerSelectedCheckin.getNome());
                                                    myFlightsPnr1.setCognome(passengerSelectedCheckin.getCognome());
                                                    myFlightsPnrs.add(myFlightsPnr1);
                                                } else {
                                                    checkinPnrSearchRequest.setPnr(passengerSelectedCheckin.getPnr());
                                                    checkinPnrSearchRequest.setFirstName(passengerSelectedCheckin.getNome());
                                                    checkinPnrSearchRequest.setLastName(passengerSelectedCheckin.getCognome());
                                                    checkinPnrSearchRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                                                    checkinPnrSearchRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                                                    checkinPnrSearchRequest.setConversationId(IDFactory.getTid());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        logger.info("[CheckinSession] Chiamata del servizio retrieveCheckinPnr...");
                        boolean isEnable = false;

                        try {
                            isEnable = MyAlitaliaUtils.isChecked(AlitaliaUtils.getRepositoryPathMarket(request.getResource()), resolverFactory, "replicationDb");
                        } catch (Exception e) {

                        }
                        /** Richiamo il servizio */
                        if (isEnable) {
                            serviceResponce = myFlightsDelegateRest.retrieveCheckinPnr(checkinPnrSearchRequest);
                        } else {
                            serviceResponce = checkInDelegateRest.retrieveCheckinPnr(checkinPnrSearchRequest);
                        }                        logger.info("[CheckinSession] Ritorno del servizio retrieveCheckinPnr...");

                        if (serviceResponce != null) {
                            MyFlightsPnr myFlightsPnr1 = new MyFlightsPnr();
                            myFlightsPnr1.setCheckinPnrSearchResponse(serviceResponce);
                            myFlightsPnr1.setNome(passengerSelectedCheckin.getNome());
                            myFlightsPnr1.setCognome(passengerSelectedCheckin.getCognome());

                            myFlightsPnrs.add(myFlightsPnr1);
                        }
                        mactx.getMyFlightsModel().setMyFlightsPnrs(myFlightsPnrs);
                        MyAlitaliaUtils.setMactxToSession(request, mactx);

                        TidyJSONWriter json;
                        try {

                            Gson gson = new Gson();
                            String jsonInString = gson.toJson(serviceResponce);

                            json = new TidyJSONWriter(response.getWriter());

                            response.setContentType("application/json");

                            json.object();
                            json.key("data").value(jsonInString);
                            json.endObject();

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {

                        List<MyFlightsTicket> myFlightsTickets = new ArrayList<>();

                        CheckinTicketSearchResponse result = new CheckinTicketSearchResponse();

                        if (mactx != null) {

                            if (mactx.getMyFlightsModel() == null) {

                                mactx.setMyFlightsModel(new MyFlightsModel());

                                if (mactx.getMyFlightsModel().getMyFlightsTickets() == null) {

                                    mactx.getMyFlightsModel().setMyFlightsTickets(new ArrayList<>());

                                }

                            } else {
                                if (mactx.getMyFlightsModel().getMyFlightsTickets() != null) {
                                    for (MyFlightsTicket myFlightsTicket : mactx.getMyFlightsModel().getMyFlightsTickets()) {
                                        CheckinTicketSearchResponse checkinPnrSearchResponse = myFlightsTicket.getCheckinTicketSearchResponse();
                                        if (checkinPnrSearchResponse.getPnrData() != null) {

                                            for (Pnr pnrRender : checkinPnrSearchResponse.getPnrData().getPnr()) {
                                                if (!pnrRender.getNumber().equals(pnr)) {
                                                    MyFlightsTicket myFlightsTicket1 = new MyFlightsTicket();
                                                    myFlightsTicket1.setCognome(passengerSelectedCheckin.getCognome());
                                                    myFlightsTicket1.setNome(passengerSelectedCheckin.getNome());
                                                    myFlightsTicket1.setCheckinTicketSearchResponse(checkinPnrSearchResponse);
                                                    myFlightsTickets.add(myFlightsTicket1);
                                                } else {
                                                    boolean resultSession = initSession(request, configuration, checkinSession);
                                                    CheckinSessionContext ctx = getCheckInSessionContext(request);
                                                    ctx.pnr = passengerSelectedCheckin.getPnr();
                                                    ctx.passengerName = passengerSelectedCheckin.getNome();
                                                    ctx.passengerSurname = passengerSelectedCheckin.getCognome();
                                                    logger.info("[CheckinPnrSearchServlet] invoco il checkinSession.checkinTicketSearch");
                                                    result = checkinSession.checkinTicketSearch(request, ctx);
                                                    logger.info("[CheckinPnrSearchServlet] result=" + result);
                                                    if (result != null) {
                                                        MyFlightsTicket myFlightsTicket1 = new MyFlightsTicket();
                                                        myFlightsTicket1.setCognome(passengerSelectedCheckin.getCognome());
                                                        myFlightsTicket1.setNome(passengerSelectedCheckin.getNome());
                                                        myFlightsTicket1.setCheckinTicketSearchResponse(result);
                                                        myFlightsTickets.add(myFlightsTicket1);

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        mactx.getMyFlightsModel().setMyFlightsTickets(myFlightsTickets);
                        MyAlitaliaUtils.setMactxToSession(request, mactx);

                        TidyJSONWriter json;
                        try {

                            Gson gson = new Gson();
                            String jsonInString = gson.toJson(result);

                            json = new TidyJSONWriter(response.getWriter());

                            response.setContentType("application/json");

                            json.object();
                            json.key("data").value(jsonInString);
                            json.endObject();

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                } catch (Exception e) {
                    logger.error("Errore aggiornamento voli myFlights in sessione : " + e.getMessage());
                }


                break;

            case "updatePassengerMyFlights":

                mactx.getMyFlightsModel().setPassengerSelectedCheckin(new PassengerSelectedCheckin());
                mactx.getMyFlightsModel().getPassengerSelectedCheckin().setPnr(pnr);
                mactx.getMyFlightsModel().getPassengerSelectedCheckin().setNome(nome);
                mactx.getMyFlightsModel().getPassengerSelectedCheckin().setCognome(cognome);
                MyAlitaliaUtils.setMactxToSession(request, mactx);

                TidyJSONWriter json2;
                try {

                    Gson gson = new Gson();
                    String jsonInString = gson.toJson(mactx.getMyFlightsModel().getMyFlightsPnrs());

                    json2 = new TidyJSONWriter(response.getWriter());

                    response.setContentType("application/json");

                    json2.object();
                    json2.key("data").value(jsonInString);
                    json2.endObject();

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
        }

    }


    @SuppressWarnings("rawtypes")
    @Override
    protected Map getAdditionalFailureData(SlingHttpServletRequest request,
                                           GenericFormValidatorServletException exception) {
        Map<String, String> additionalParams = new HashMap<String, String>();
        return additionalParams;
    }

    public void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error) {

        logger.info("[CheckinPnrSearchServlet] [managementError] error =" + error);

        Gson gson = new Gson();

        com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);

        TidyJSONWriter json;
        try {
            final I18n i18n = new I18n(request.getResourceBundle(AlitaliaUtils.findResourceLocale(request.getResource())));
            json = new TidyJSONWriter(response.getWriter());

            response.setContentType("application/json");
            String errorMessageFromBL = "";
            switch (errorObj.getErrorMessage()) {
                case "Volo decollato":
                    errorMessageFromBL = i18n.get("checkin.flightList.tooLate.label");
                    break;
                case "Nessun volo trovato":
                    errorMessageFromBL = i18n.get("checkin.flightList.notFound.label");
                default:
                    errorMessageFromBL = i18n.get("checkin.search.input.error");
                    break;
            }

            json.object();
            json.key("isError").value(true);
            json.key("errorMessage").value(errorMessageFromBL);
            json.key("sabreStatusCode").value(errorObj.getSabreStatusCode());
            json.key("conversationID").value(errorObj.getConversationID());
            json.endObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response) {

        logger.info("[CheckinPnrSearchServlet] [managementSucces] ...");
        String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
                request.getResource(), false)
                + getConfiguration().getCheckinFlightListPage();

        successPage = request.getResourceResolver().map(successPage);

        if (repeate) {
            successPage = successPage + "?&repeate=true";
        }


        logger.info("[CheckinPnrSearchServlet] [managementSucces] successPage = " + successPage);

        TidyJSONWriter json;
        try {

            json = new TidyJSONWriter(response.getWriter());
            response.setContentType("application/json");
            json.object();
            json.key("isError").value(false);
            json.key("successPage").value(successPage);
            json.endObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
