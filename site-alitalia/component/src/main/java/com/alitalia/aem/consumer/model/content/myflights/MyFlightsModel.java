package com.alitalia.aem.consumer.model.content.myflights;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.managemybooking.delegate.ManageMyBookingDelegate;

@Model(adaptables = { SlingHttpServletRequest.class })

public class MyFlightsModel extends GenericBaseModel {
	
	//ERROR SECTION
	private static int LOGGED_USER_NOT_FOUND = 1;
	private static int SERVICE_ERROR = 2;
	
	//CONSTANT SECTION
	private static String TIME_SEPARATOR = ":";
	
	
	private static String[] MONTHS = {
		"common.monthsOfYear.january.short", 
		"common.monthsOfYear.february.short", 
		"common.monthsOfYear.march.short", 
		"common.monthsOfYear.april.short", 
		"common.monthsOfYear.may.short", 
		"common.monthsOfYear.june.short", 
		"common.monthsOfYear.july.short", 
		"common.monthsOfYear.august.short", 
		"common.monthsOfYear.september.short", 
		"common.monthsOfYear.october.short", 
		"common.monthsOfYear.november.short", 
		"common.monthsOfYear.december.short"
	};
	
	//@Reference(policy = ReferencePolicy.DYNAMIC)
	// private volatile ManageMyBookingDelegate manageMyBookingDelegate;
	
	@Inject
	private ManageMyBookingDelegate manageMyBookingDelegate;
	
	@Self
	private SlingHttpServletRequest request;
	
	private MyFlightsData flightList;
	
	
	@PostConstruct
	protected void initModel() {
		logger.debug("[MyFlightsModel] initModel");

		super.initBaseModel(request);
		
		flightList = new MyFlightsData();
		MMCustomerProfileData loggedUserProfile = AlitaliaCustomerProfileManager.getAuthenticatedUserProfile(request);
		if (loggedUserProfile == null) {
			flightList.setError(LOGGED_USER_NOT_FOUND);
			logger.debug("[MyFlightsModel] User Not Found");
			
		}
		else {
			
			// Retrieve user data and call the service to get pnr's
			String customerNumber = loggedUserProfile.getCustomerNumber();
			String customerCode = "";
			if (customerNumber != null) {
				// Manage Customer Number to render it for the the service
				customerCode = MyFlightsUtils.getTripId(customerNumber);
			}
			
			String name = loggedUserProfile.getCustomerName();
			String lastname = loggedUserProfile.getCustomerSurname();
			flightList.setName(name);
			flightList.setLastname(lastname);
			Locale localRepository = AlitaliaUtils.getRepositoryPathLocale(
					request.getResource());
			String locale = AlitaliaUtils.getLanguage(localRepository.toLanguageTag()) + "_" + localRepository.getCountry();
			RetriveMmbPnrInformationRequest pnrRequest = new RetriveMmbPnrInformationRequest();
			pnrRequest.setTid(IDFactory.getTid());
			pnrRequest.setSid(IDFactory.getSid(request));
			pnrRequest.setTripId(customerCode);
			pnrRequest.setName(name);
			pnrRequest.setLastName(lastname);
			pnrRequest.setLocale(locale.toUpperCase());
			
			try {
				
				//call to the service
				List<MyRouteRender> myRoutes = new ArrayList<MyRouteRender>(); 
				RetriveMmbPnrInformationResponse pnrResponse = manageMyBookingDelegate.retrivePnrInformation(pnrRequest);
				List<MmbRouteData> responseRoutes = pnrResponse.getRoutesList();
				
				// manipulate data
				if (responseRoutes != null) {
					for (MmbRouteData responseRoute : responseRoutes) {
						List<MyFlightRender> outboundFlights = new ArrayList<MyFlightRender>();
						List<MyFlightRender> returnFlights = new ArrayList<MyFlightRender>();
						for (MmbFlightData flightData : responseRoute.getFlights()) {
							MyFlightRender flight = new MyFlightRender();
							String aCode = flightData.getTo().getCode();
							flight.setArrivalAirportCode(aCode);
							flight.setArrivalAirportTerminal(flightData.getToTerminal());
							flight.setArrivalCity(I18nKeyCommon.AIRPORT_PREFIX + aCode + I18nKeyCommon.AIRPORT_CITY);
							Calendar arrivalDateTime = flightData.getArrivalDateTime();
							if (arrivalDateTime.get(Calendar.DAY_OF_MONTH) < 10) {
								flight.setArrivalDay("0" + Integer.toString(arrivalDateTime.get(Calendar.DAY_OF_MONTH)));
							}
							else {
								flight.setArrivalDay(Integer.toString(arrivalDateTime.get(Calendar.DAY_OF_MONTH)));
							}
							
							int arrivalMonth = arrivalDateTime.get(Calendar.MONTH);
							flight.setArrivalMonth(MONTHS[arrivalMonth]);  
							flight.setArrivalYear(Integer.toString(arrivalDateTime.get(Calendar.YEAR)));
							
							String arrivalHour;
							if (arrivalDateTime.get(Calendar.HOUR_OF_DAY) < 10) {
								arrivalHour = "0" + Integer.toString(arrivalDateTime.get(Calendar.HOUR_OF_DAY));
							}
							else {
								arrivalHour = Integer.toString(arrivalDateTime.get(Calendar.HOUR_OF_DAY));
							}
							
							String arrivalMinute;
							if (arrivalDateTime.get(Calendar.MINUTE)< 10) {
								arrivalMinute = "0" + Integer.toString(arrivalDateTime.get(Calendar.MINUTE));
							}
							else {
								arrivalMinute = Integer.toString(arrivalDateTime.get(Calendar.MINUTE));
							}
							
							String arrivalTime = arrivalHour + TIME_SEPARATOR + arrivalMinute;
							flight.setArrivalTime(arrivalTime);
							flight.setCabin(flightData.getCabin().value());
							flight.setCarrier(flightData.getCarrier());
							String dCode = flightData.getFrom().getCode();
							flight.setDepartureAirportCode(dCode);
						
							flight.setDepartureAirportTerminal(flightData.getFromTerminal());
							
							flight.setDepartureCity(I18nKeyCommon.AIRPORT_PREFIX + dCode + I18nKeyCommon.AIRPORT_CITY);
							Calendar departureDateTime = flightData.getDepartureDateTime();
							if (departureDateTime.get(Calendar.DAY_OF_MONTH) < 10) {
								flight.setDepartureDay("0" + Integer.toString(departureDateTime.get(Calendar.DAY_OF_MONTH)));
							}
							else {
								flight.setDepartureDay(Integer.toString(departureDateTime.get(Calendar.DAY_OF_MONTH)));
							}
							int departureMonth = departureDateTime.get(Calendar.MONTH);
							flight.setDepartureMonth(MONTHS[departureMonth]);  // january = 0
							flight.setDepartureYear(Integer.toString(departureDateTime.get(Calendar.YEAR)));
							
							String departureHour;
							if (departureDateTime.get(Calendar.HOUR_OF_DAY) < 10) {
								departureHour = "0" + Integer.toString(departureDateTime.get(Calendar.HOUR_OF_DAY));
							}
							else {
								departureHour = Integer.toString(departureDateTime.get(Calendar.HOUR_OF_DAY));
							}
							
							String departureMinute;
							if (departureDateTime.get(Calendar.MINUTE)< 10) {
								departureMinute = "0" + Integer.toString(departureDateTime.get(Calendar.MINUTE));
							}
							else {
								departureMinute = Integer.toString(departureDateTime.get(Calendar.MINUTE));
							}
							
							String departureTime = departureHour + TIME_SEPARATOR + departureMinute;
							flight.setDepartureTime(departureTime);
							flight.setFlightNumber(flightData.getFlightNumber());
							
							if (flightData.getType() == RouteTypeEnum.OUTBOUND) {
								outboundFlights.add(flight);
							}
							else {
								returnFlights.add(flight);
							}
							
						}
						MyRouteRender route = new MyRouteRender();
						route.setPnr(responseRoute.getFlights().get(0).getPnr());
						route.setOutboundFlights(outboundFlights);
						route.setReturnFlights(returnFlights);
						myRoutes.add(route);
					}
					flightList.setMyRoutes(myRoutes);
				}
				else {
					logger.debug("[MyFlights] No Flights available");
				}
				
				
			}
			catch(Exception e) {
				flightList.setError(SERVICE_ERROR);
			}
			
			
		}
		
		request.getSession().setAttribute(MyFlightsData.NAME,
				flightList);
		
	}

	public MyFlightsData getFlightList() {
		return flightList;
	}
	
	
	
	
}
