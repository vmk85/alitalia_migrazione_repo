package com.alitalia.aem.consumer.myalitalia.model;

public class ProfileComplite {

    private int percent;

    private String redirectUrl;

    private String testo;

    public String getTesto() {
        return testo;
    }

    public void setTesto(String testo) {
        this.testo = testo;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    @Override
    public String toString() {
        return "ProfileComplite{" +
                "percent=" + percent +
                ", redirectUrl='" + redirectUrl + '\'' +
                ", testo='" + testo + '\'' +
                '}';
    }
}
