package com.alitalia.aem.consumer.checkin.render;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.alitalia.aem.common.data.home.checkin.CheckinSeatData;
import com.alitalia.aem.common.data.home.checkin.CheckinSeatMapData;

public class CheckinSeatMapRowRender {
	
	private boolean hasExit;
	private String number;
	private List<CheckinSeatRender> seats;
	private boolean secondLevel;
	
	public CheckinSeatMapRowRender(CheckinSeatMapData rowData, Set<String> assignedSeats, Set<String> comfortSeats) {
		
		this.hasExit = Boolean.TRUE.equals(rowData.getHasExit());
		this.secondLevel = Boolean.TRUE.equals(rowData.getSecondLevel());
		this.number = rowData.getNumber() == null ? "" : rowData.getNumber().toString();
		
		if (rowData.getSeats() != null &&
				!rowData.getSeats().isEmpty()) {
			
			int i = 0;
			seats = new ArrayList<CheckinSeatRender>();
			for (CheckinSeatData seat : rowData.getSeats()) {
				String column = "";
				if (i < rowData.getColumns().size()) {
					column = rowData.getColumns().get(i);
				}
				String seatValue = this.number + column;
				if (assignedSeats != null && assignedSeats.contains(seatValue)) {
					seats.add(new CheckinSeatRender(seat, this.number, column,
							this.hasExit, true));
				} else {
					seats.add(new CheckinSeatRender(seat, this.number, column, 
							this.hasExit));
				}
				if (Boolean.TRUE.equals(seat.getIsComfortSeat())) {
					comfortSeats.add(seatValue);
				}
				i++;
			}
		}
	}

	public boolean isHasExit() {
		return hasExit;
	}

	public void setHasExit(boolean hasExit) {
		this.hasExit = hasExit;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public List<CheckinSeatRender> getSeats() {
		return seats;
	}

	public void setSeats(List<CheckinSeatRender> seats) {
		this.seats = seats;
	}

	public boolean isSecondLevel() {
		return secondLevel;
	}

	public void setSecondLevel(boolean secondLevel) {
		this.secondLevel = secondLevel;
	}
}
