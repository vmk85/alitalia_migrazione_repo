package com.alitalia.aem.consumer.checkin.servlet;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.enumerations.CheckinSearchReasonEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;

	@Component(metatype = true)
	@Service(Servlet.class)
	@Properties({
			@Property(name = "sling.servlet.selectors", value = { "checkinundoall" }),
			@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
			@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
	})
	@SuppressWarnings("serial")
	public class CheckinUndoAllServlet extends GenericCheckinFormValidatorServlet {
		
		@Reference
		private AlitaliaConfigurationHolder configuration;
		
		@Reference
		private CheckinSession checkinSession;
		
		@Override
		protected ResultValidation validateForm(SlingHttpServletRequest request) {
			
			/* Risultato della validazione */
			ResultValidation resultValidation = new ResultValidation();
			resultValidation.setResult(true);
			
			return resultValidation;
		}
		
		
		@Override
		protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
			
			CheckinSessionContext ctx = getCheckInSessionContext(request);
			
			checkinSession.getPassengerList(ctx, CheckinSearchReasonEnum.UNDO_CHECK_IN);
			checkinSession.undoCheckin(ctx, true);
			checkinSession.refreshRouteData(ctx);
			
			String baseUrl = request.getResource().getResourceResolver().map(
					AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
			
			TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
			json.object();
			response.setContentType("application/json");
			json.key("result").value("OK");
			json.key("redirect").value(baseUrl + configuration.getCheckinFlightListPage());
			json.endObject();
		}
		
		@Override
		protected AlitaliaConfigurationHolder getConfiguration() {
			return configuration;
		}
	}

