package com.alitalia.aem.consumer.mmb.servlet;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "mmbmodifypnrpassengerfrequentflyer" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class MmbModifyPnrPassengerFrequentFlyerServlet extends GenericMmbFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile MmbSession mmbSession;	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		MmbSessionContext ctx = getMmbSessionContext(request);
		
		/* Recupero dati dalla request */		
		String programmaFrequentFlyer = request.getParameter("programmaFrequentFlyer");
		String codiceFrequentFlyer = request.getParameter("codiceFrequentFlyer");
		int passengerIndex = Integer.valueOf(request.getParameter("passengerIndex"));
		

		/* Validazione */
		Validator validator = new Validator();
		
		validator.addDirectCondition("programmaFrequentFlyer", programmaFrequentFlyer, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("codiceFrequentFlyer", codiceFrequentFlyer, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		ResultValidation resultValidation = validator.validate();
		
		if (resultValidation.getResult() && "AZ".equals(programmaFrequentFlyer)) {
			// perform the frequent flyer codes validation using the service - only for millemiglia program
			// perform the frequent flyer codes validation using the service
			if (!mmbSession.checkFrequentFlyerCodes(ctx, passengerIndex, codiceFrequentFlyer)) {
				resultValidation.setResult(false);
				resultValidation.getFields().put("codiceFrequentFlyer", BookingConstants.MESSAGE_GENERIC_INVALID_FIELD);
			}
		}
		
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		MmbSessionContext ctx = getMmbSessionContext(request);
		
		/* Recupero dati dalla request */		
		String programmaFrequentFlyer = request.getParameter("programmaFrequentFlyer");
		String codiceFrequentFlyer = request.getParameter("codiceFrequentFlyer");
		int passengerIndex = Integer.valueOf(request.getParameter("passengerIndex"));
		
		mmbSession.modifyFrequentFlyerInfo(getMmbSessionContext(request), passengerIndex, programmaFrequentFlyer, 
				codiceFrequentFlyer);
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value("OK");
		json.key("message").value(ctx.i18n.get("mmb.gestisciPrenotazione.codiceFrequentFlyer.label"));
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}
