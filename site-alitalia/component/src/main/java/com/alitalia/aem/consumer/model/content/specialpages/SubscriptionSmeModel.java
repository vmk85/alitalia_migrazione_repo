package com.alitalia.aem.consumer.model.content.specialpages;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.model.content.substriptionsme.SubscriptionSmeRequestData;

@Model(adaptables = { SlingHttpServletRequest.class })
public class SubscriptionSmeModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private SubscriptionSmeRequestData subscriptionSmeRequestData;
	private List<String> days = new ArrayList<String>();
	private List<String> months = new ArrayList<String>();
	private List<String> years = new ArrayList<String>();

	@PostConstruct
	protected void initModel() {
		logger.debug("[SubscriptionSmeModel] initModel");

		super.initBaseModel(slingHttpServletRequest);
		
		try {
			// retrieve and remove BaggageClaimData from session if any
			setSubscriptionSmeRequestData((SubscriptionSmeRequestData) 
				slingHttpServletRequest.getSession()
				.getAttribute(SubscriptionSmeRequestData.NAME));
			slingHttpServletRequest.getSession().removeAttribute(SubscriptionSmeRequestData.NAME);
	
			// Init date controls
			for (int i = 1; i <= 31; i++) {
				days.add(String.valueOf(i));
			}
	
			for (int i = 1; i <= 12; i++) {
				months.add(String.valueOf(i));
			}
	
			Calendar c = new GregorianCalendar();
			int thisYear = c.get(Calendar.YEAR);
			for (int i = thisYear - 100; i <= thisYear - 18; i++) {
				years.add(String.valueOf(i));
			}
	
			
		} catch (Exception e) {
			logger.error("Unexpected error: {}", e);
			throw e;
		}
	}

	public SubscriptionSmeRequestData getSubscriptionSmeRequestData() {
		return subscriptionSmeRequestData;
	}

	public void setSubscriptionSmeRequestData(SubscriptionSmeRequestData subscriptionSmeRequestData) {
		this.subscriptionSmeRequestData = subscriptionSmeRequestData;
	}

	public List<String> getDays() {
		return days;
	}

	public List<String> getMonths() {
		return months;
	}

	public List<String> getYears() {
		return years;
	}

	
	

}
