package com.alitalia.aem.consumer.carnet.servlet;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.carnet.CarnetData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.carnet.CarnetConstants;
import com.alitalia.aem.consumer.carnet.CarnetSession;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.alitalia.aem.consumer.carnet.render.CarnetGenericPriceRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.web.component.carnet.delegate.CarnetDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "prezzicarnet" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CarnetGetPriceServlet extends GenericCarnetFormValidatorServlet {

	
	// Search Flight Delegate
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile CarnetDelegate carnetDelegate;
	
	@Reference
	private volatile CarnetSession carnetSession;
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
		
		// content type
		response.setContentType("application/json");
				
		// JSON response
		TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());
				
		HttpSession session = request.getSession(true);
		
		String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		String currencyCode = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencyCode", String.class);
		String currencySymbol = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencySymbol", String.class);
		Locale locale = AlitaliaUtils.getRepositoryPathLocale(request.getResource());
		String callerIp = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
		I18n i18n = new I18n(request.getResourceBundle(locale));
		
		/*Currency format info*/
		InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(request.getResource());
		String decimalSeparator=ivm.getInherited("decimalSeparator", String.class);
		String groupingSeparator=ivm.getInherited("groupingSeparator", String.class);
		Integer numDecDigits=ivm.getInherited("decimalDigit", Integer.class);
		if(numDecDigits == null){
			numDecDigits = 2;
		}
		NumberFormat currentNumberFormat = AlitaliaUtils.getNumberFormat(numDecDigits, decimalSeparator, groupingSeparator, market);
		
		// Inizializzo la sessione
		CarnetSessionContext ctx = carnetSession.initializeSession(request, i18n, CarnetConstants.CARNET_SID,
				market, currencyCode, currencySymbol, locale, 
				callerIp, currentNumberFormat);
		
		session.setAttribute(CarnetConstants.CARNET_CONTEXT_ATTRIBUTE, ctx);
		
		logger.debug("Chiamo getPrice carnet");
		
		jsonOutput.object();
		
		/* Lista degli areoporti presi dal servizio */
		jsonOutput.key("carnets");
		jsonOutput.array();
		
		List<CarnetData> listCarnet = carnetSession.retrieveCarnetList(ctx);
		
		logger.debug("Risposta del servizio: " + listCarnet.toString());
		CarnetGenericPriceRender priceRender = new CarnetGenericPriceRender(ctx);
		for(CarnetData carnet : listCarnet){
			Short quantity = carnet.getQuantity();
			BigDecimal price = carnet.getPrice();
			if(quantity.shortValue() == CarnetConstants.NUM_CARNET_6){
				jsonOutput.object();
				jsonOutput.key("firstCarnet").value(priceRender.getFormattedPrice(price));
				jsonOutput.key("code").value(carnet.getCode());
				jsonOutput.endObject();
			} else if(quantity.shortValue() == CarnetConstants.NUM_CARNET_12){
				jsonOutput.object();
				jsonOutput.key("secondCarnet").value(priceRender.getFormattedPrice(price));
				jsonOutput.key("code").value(carnet.getCode());
				jsonOutput.endObject();
			}
		}
		jsonOutput.endArray();
		jsonOutput.endObject();
		logger.debug("JSON del servizio: " + jsonOutput.toString());
					
		
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}
