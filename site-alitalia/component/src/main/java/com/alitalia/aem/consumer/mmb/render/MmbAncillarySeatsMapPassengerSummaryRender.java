package com.alitalia.aem.consumer.mmb.render;

import java.util.ArrayList;
import java.util.List;

public class MmbAncillarySeatsMapPassengerSummaryRender {
	
	private String fullName = "";
	private boolean hasInfant = false;
	private boolean child = false;
	private boolean displayInFeedback = false;
	private int totalToBeIssuedStandardQuantity = 0;
	private int totalToBeIssuedComfortQuantity = 0;
	private MmbPriceRender totalToBeIssuedPrice = null;
	private List<MmbAncillaryFastTrackRender> fastTrackSelections = null;
	
	public MmbAncillarySeatsMapPassengerSummaryRender() {
		super();
		fastTrackSelections = new ArrayList<MmbAncillaryFastTrackRender>();
	}
	
	public List<MmbAncillaryFastTrackRender> getFastTrackSelections() {
		return fastTrackSelections;
	}
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public boolean isHasInfant() {
		return hasInfant;
	}

	public void setHasInfant(boolean hasInfant) {
		this.hasInfant = hasInfant;
	}
	
	public boolean isChild() {
		return child;
	}

	public void setChild(boolean child) {
		this.child = child;
	}
	
	public boolean isDisplayInFeedback() {
		return displayInFeedback;
	}

	public void setDisplayInFeedback(boolean displayInFeedback) {
		this.displayInFeedback = displayInFeedback;
	}
	
	public int getTotalToBeIssuedStandardQuantity() {
		return totalToBeIssuedStandardQuantity;
	}

	public void setTotalToBeIssuedStandardQuantity(int totalToBeIssuedStandardQuantity) {
		this.totalToBeIssuedStandardQuantity = totalToBeIssuedStandardQuantity;
	}
	
	public int getTotalToBeIssuedComfortQuantity() {
		return totalToBeIssuedComfortQuantity;
	}

	public void setTotalToBeIssuedComfortQuantity(int totalToBeIssuedComfortQuantity) {
		this.totalToBeIssuedComfortQuantity = totalToBeIssuedComfortQuantity;
	}

	public MmbPriceRender getTotalToBeIssuedPrice() {
		return totalToBeIssuedPrice;
	}

	public void setTotalToBeIssuedPrice(MmbPriceRender totalToBeIssuedPrice) {
		this.totalToBeIssuedPrice = totalToBeIssuedPrice;
	}
	
}
