package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPassengerNoteEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup.AncillariesGroupType;
import com.alitalia.aem.consumer.mmb.model.MmbFlightsGroup;
import com.alitalia.aem.consumer.mmb.render.MmbAncillaryFastTrackPassengerRender;
import com.alitalia.aem.consumer.mmb.render.MmbAncillaryFastTrackRender;
import com.alitalia.aem.consumer.mmb.render.MmbFlightDataRender;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;
import com.alitalia.aem.consumer.utils.Lists;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbAncillaryFastTrack extends MmbSessionAncillaryGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private MmbSession mmbSession;
	
	private boolean allowClearCart = false;
	private List<MmbAncillaryFastTrackPassengerRender> fastTrackPassengers;
	private List<MmbFlightsGroup> flightsGroupList;

	private int purchasableFastTrackNum;
	private int selectedFastTrackNum;
	
	@PostConstruct
	protected void initModel() throws IOException {
		initBaseModel(request);
		
		this.allowClearCart = false;
		this.fastTrackPassengers = new ArrayList<MmbAncillaryFastTrackPassengerRender>();
		this.flightsGroupList = MmbFlightsGroup.groupByRouteType(ctx.route.getFlights());
		this.purchasableFastTrackNum = 0;
		this.selectedFastTrackNum = 0;
		// first, group by passenger
		List<MmbAncillariesGroup> groupsByPassenger = MmbAncillariesGroup.createGroups(
				AncillariesGroupType.BY_PASSENGER, ctx.route, ctx.cart.getPassengers(), 
				ctx.cart.getAncillaries(), MmbAncillaryTypeEnum.FAST_TRACK);
		for (MmbAncillariesGroup passengerGroup : groupsByPassenger) {
			
			// skip passengers without any fast track ancillaries
			if (passengerGroup.getAncillaries().size() == 0) {
				continue;
			}
			
			// prepare passenger header render
			MmbAncillaryFastTrackPassengerRender fastTrackPassenger = new MmbAncillaryFastTrackPassengerRender();
			fastTrackPassenger.setFullName(passengerGroup.getPassengerData().getName() + 
					" " + passengerGroup.getPassengerData().getLastName());
			if (MmbPassengerNoteEnum.INFANT_ACCOMPANIST.equals(
					passengerGroup.findMmbPassengerDataForFlight(0).getNote())) {
				fastTrackPassenger.setHasInfant(true);
			}
			if (MmbPassengerNoteEnum.CHILD.equals(
					passengerGroup.findMmbPassengerDataForFlight(0).getNote())) {
				fastTrackPassenger.setChild(true);
			}
			fastTrackPassenger.setDisplayInFeedback(false);
			fastTrackPassenger.setTotalToBeIssuedPrice(
					new MmbPriceRender(BigDecimal.valueOf(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
			fastTrackPassenger.setTotalToBeIssuedQuantity(0);
			fastTrackPassengers.add(fastTrackPassenger);
			
			// for each passenger, group again by flight
			List<MmbAncillariesGroup> groups = MmbAncillariesGroup.createGroups(
					AncillariesGroupType.BY_PASSENGER_AND_FLIGHT, ctx.route, 
					Lists.asList(passengerGroup.getPassengerData()), passengerGroup.getAncillaries(), 
					MmbAncillaryTypeEnum.FAST_TRACK);
			for (MmbAncillariesGroup group : groups) {
				
				MmbFlightData flight = Lists.getFirst(group.getFlights());
				MmbAncillaryFastTrackRender fastTrackRender = new MmbAncillaryFastTrackRender(group);
				fastTrackPassenger.getFastTrackSelections().add(fastTrackRender);
				fastTrackRender.setRouteTypeLabel(getRouteTypeLabel(group));
				fastTrackRender.setFlight(new MmbFlightDataRender(flight));
				
				if (group.getAncillaries().size() == 0) {
					
					// no ancillary for this passenger/route
					fastTrackRender.setIdentifier("");
					
				} else {
					
					// determine the related flight group render
					MmbFlightsGroup relatedFlightsGroup = null;
					for (MmbFlightsGroup flightGroup : this.flightsGroupList) {
						if (flight.getType().equals(flightGroup.getType()) 
								&& flight.getRouteId().equals(flightGroup.getRouteId())) {
							relatedFlightsGroup = flightGroup;
							break;
						}
					}
					
					MmbAncillaryData availableAncillary = 
							group.getAncillaryInStatus(
									MmbAncillaryStatusEnum.AVAILABLE);
					MmbAncillaryData addedAncillary = 
							group.getAncillaryInStatus(
									MmbAncillaryStatusEnum.TO_BE_ISSUED);
					MmbAncillaryData issuedOrPreviouslyIssuedAncillary = 
							group.getAncillaryInStatuses(new MmbAncillaryStatusEnum[] {
									MmbAncillaryStatusEnum.ISSUED,
									MmbAncillaryStatusEnum.PREVIOUSLY_ISSUED});
					
					MmbFlightDataRender flightData = fastTrackRender.getFlight();
					
					if (availableAncillary != null) {
						
						if(flightData.isBus()){
							fastTrackRender.setAllowModify(false);
						} else{
							fastTrackRender.setAllowModify(true);
						}
						
						// ancillary available
						fastTrackRender.setIdentifier(
								getAncillariesSelectionIdentifier(availableAncillary));
						fastTrackRender.setCurrentValue(false);
						fastTrackRender.setPrice(
								mmbSession.getAncillaryDisplayedPriceInfo(ctx, availableAncillary, 1));
						purchasableFastTrackNum += availableAncillary.getQuantity() > 0 
								? availableAncillary.getQuantity() : 1;
						
					} else if (addedAncillary != null) {
						// ancillary added to cart
						fastTrackRender.setIdentifier(
								getAncillariesSelectionIdentifier(addedAncillary));
						fastTrackRender.setCurrentValue(true);
						fastTrackRender.setPrice(
								mmbSession.getAncillaryDisplayedPriceInfo(ctx, addedAncillary, null));
						fastTrackRender.setAllowModify(true);
						relatedFlightsGroup.getRelatedAncillaries().add(addedAncillary);
						this.allowClearCart = true;

						fastTrackPassenger.setDisplayInFeedback(true);
						fastTrackPassenger.setTotalToBeIssuedQuantity(
								fastTrackPassenger.getTotalToBeIssuedQuantity() + 1);
						fastTrackPassenger.getTotalToBeIssuedPrice().increaseAmount(
								fastTrackRender.getPrice().getAmount());
						selectedFastTrackNum += addedAncillary.getQuantity() > 0 
								? addedAncillary.getQuantity() : 1;
						
					} else if (issuedOrPreviouslyIssuedAncillary != null) {
						// ancillary already issued
						fastTrackRender.setIdentifier(
								getAncillariesSelectionIdentifier(issuedOrPreviouslyIssuedAncillary));
						fastTrackRender.setCurrentValue(true);
						fastTrackRender.setAllowModify(false);
						fastTrackRender.setPrice(null);
						relatedFlightsGroup.getRelatedAncillaries().add(issuedOrPreviouslyIssuedAncillary);
						
					}
					
				}
			}
			
		}
	}
	
	public List<MmbFlightsGroup> getFlightsGroupList() {
		return flightsGroupList;
	}
	
	public List<MmbAncillaryFastTrackPassengerRender> getFastTrackPassengers() {
		return fastTrackPassengers;
	}

	public boolean isAllowClearCart() {
		return allowClearCart;
	}

	public int getPurchasableFastTrackNum() {
		return purchasableFastTrackNum;
	}

	public int getSelectedFastTrackNum() {
		return selectedFastTrackNum;
	}

}
