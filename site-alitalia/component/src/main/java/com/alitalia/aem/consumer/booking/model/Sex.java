package com.alitalia.aem.consumer.booking.model;

import java.util.ArrayList;

public enum Sex {
	MALE("M"),
	FEMALE("F");
	
	private final String value;

	Sex(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	public static ArrayList<String> listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (Sex s: Sex.values()) {
			result.add(s.value);
		}
		return result;
	}

	public static Sex fromValue(String v) {
		for (Sex s: Sex.values()) {
			if (s.value.equals(v)) {
				return s;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
