package com.alitalia.aem.consumer.carnet.analytics;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CarnetAnalyticsInfoToJSObject{
	private static final String EN_FLIGHT_DATE_FORMAT = "MM/dd/yyyy";
	private static final String TIME_FORMAT_HH_MM_SS = "kk:mm:ss";
	private DecimalFormat format;
	private SimpleDateFormat dateFormat_en;
	private SimpleDateFormat timeFormat;
	private CarnetAnalyticsInfo info;
	
	public CarnetAnalyticsInfoToJSObject(CarnetAnalyticsInfo info){
		this.info = info;
		initFormats();
	}
	
	private void initFormats() {
		format = new DecimalFormat("#0.00");
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setCurrencySymbol(""); 
		format.setDecimalFormatSymbols(otherSymbols);
		format.setGroupingUsed(false);
		format.setPositivePrefix("");
		format.setNegativePrefix("");
		dateFormat_en = new SimpleDateFormat(EN_FLIGHT_DATE_FORMAT);
		dateFormat_en.setLenient(false);
		timeFormat = new SimpleDateFormat(TIME_FORMAT_HH_MM_SS);
		timeFormat.setLenient(false);
		
	}
	
	public String toJSObj(boolean trailingSemicolon){
		String js = "{";
		switch(info.step){
		case 3:
			js = addJsObjElem(js, "CarnetRevenue", formatIfNotNull(info.carnetRevenue, format), true);
			js = addJsObjElem(js, "IdOrd", formatIfNotNull(info.ordDate, dateFormat_en) + info.carnetCode, true);
			js = addJsObjElem(js, "CodiceCarnet", info.carnetCode, true);
			js = addJsObjElem(js, "CarnetPaymentType", info.carnetPaymentType, true);
			js = addJsObjElem(js, "CarnetCCType", info.carnetCCType, true);
		case 1:
		case 2: 
			js = addJsObjElem(js, "CarnetBuyStep", String.valueOf(info.step), true);
			js = addJsObjElem(js, "CarnetType", String.valueOf(info.carnetType), false);
			break;
		}
		js += (trailingSemicolon) ? "};" : "}";
		return js;
	}	
	
	private String addJsObjElem(String jsObj, String ElemName
			, String ElemValue, boolean trailingComma){
		return jsObj + "'"+ElemName+"':'"+ElemValue+"'" 
			+ (trailingComma ? "," : "");
	}
	
	private String formatIfNotNull(Calendar date, SimpleDateFormat formatter){
		
		return date != null ? formatter.format(date.getTime()) : "";
	}
	
	private String formatIfNotNull(BigDecimal price, DecimalFormat formatter){
		return price != null ? formatter.format(price) : "";
	}
}
