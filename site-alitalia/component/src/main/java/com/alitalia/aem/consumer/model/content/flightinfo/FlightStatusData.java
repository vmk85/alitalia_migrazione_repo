package com.alitalia.aem.consumer.model.content.flightinfo;

public class FlightStatusData {
	public static final String NAME = "FlightStatusData";
	
	
	private String flightNumber;
	private String totalDuration;
	private String departureCity;
	private String departureAirport;
	private String departureAirportCode;
	private String arrivalCity;
	private String arrivalAirport;
	private String arrivalAirportCode;
	private String scheduledDepartureTime;
	private String realDepartureTime;
	private String scheduledArrivalTime;
	private String realArrivalTime;
	private String state;
	private String error;
	
	
	
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getTotalDuration() {
		return totalDuration;
	}
	public void setTotalDuration(String totalDuration) {
		this.totalDuration = totalDuration;
	}
	public String getDepartureCity() {
		return departureCity;
	}
	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}
	public String getDepartureAirport() {
		return departureAirport;
	}
	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}
	public String getDepartureAirportCode() {
		return departureAirportCode;
	}
	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}
	public String getArrivalCity() {
		return arrivalCity;
	}
	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}
	public String getArrivalAirport() {
		return arrivalAirport;
	}
	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}
	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}
	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}
	public String getScheduledDepartureTime() {
		return scheduledDepartureTime;
	}
	public void setScheduledDepartureTime(String scheduledDepartureTime) {
		this.scheduledDepartureTime = scheduledDepartureTime;
	}
	public String getRealDepartureTime() {
		return realDepartureTime;
	}
	public void setRealDepartureTime(String realDepartureTime) {
		this.realDepartureTime = realDepartureTime;
	}
	public String getScheduledArrivalTime() {
		return scheduledArrivalTime;
	}
	public void setScheduledArrivalTime(String scheduledArrivalTime) {
		this.scheduledArrivalTime = scheduledArrivalTime;
	}
	public String getRealArrivalTime() {
		return realArrivalTime;
	}
	public void setRealArrivalTime(String realArrivalTime) {
		this.realArrivalTime = realArrivalTime;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	@Override
	public String toString() {
		return "FlightStatusData [flightNumber=" + flightNumber
				+ ", totalDuration=" + totalDuration + ", departureCity="
				+ departureCity + ", departureAirport=" + departureAirport
				+ ", departureAirportCode=" + departureAirportCode
				+ ", arrivalCity=" + arrivalCity + ", arrivalAirport="
				+ arrivalAirport + ", arrivalAirportCode=" + arrivalAirportCode
				+ ", scheduledDepartureTime=" + scheduledDepartureTime
				+ ", realDepartureTime=" + realDepartureTime
				+ ", scheduledArrivalTime=" + scheduledArrivalTime
				+ ", realArrivalTime=" + realArrivalTime + ", state=" + state
				+ "]";
	}

}
