package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import com.alitalia.aem.common.data.checkinrest.model.cheangeseats.response.ComfortSeat;
import com.alitalia.aem.common.data.checkinrest.model.endpayment.response.AzSegmentPayedTP;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.AuthorizationResult;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.AzAncillaryPayedTP;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.AzPaxPayedTP;
import com.alitalia.aem.common.data.checkinrest.model.passengerdata.TotalAncillaryForPax;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.*;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.messages.checkinrest.*;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrListDataRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;
import com.alitalia.aem.consumer.utils.AlitaliaMailUtils;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.i18n.I18n;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;

@Component //(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkinendpayment"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	//@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinEndPaymentServlet extends SlingAllMethodsServlet { //extends GenericCheckinFormValidatorServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ICheckinDelegate checkInDelegateRest;

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private CheckinSession checkinSession;
	
	protected CheckinSessionContext ctx;

	private AlitaliaMailUtils alitaliaMailUtils = new AlitaliaMailUtils();


	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException{
		this.doPost(request, response);
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		
		logger.info("CheckinEndPaymentServlet - executing doPost");

		final I18n i18n = new I18n(request.getResourceBundle(
				AlitaliaUtils.findResourceLocale(request.getResource())));

		String approvalCode = (request.getSession().getAttribute("ApprovalCode") == null ? request.getParameter("ApprovalCode") : request.getSession().getAttribute("ApprovalCode").toString());
		ctx = (CheckinSessionContext) request.getSession(true).getAttribute(CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE);
		CheckinEndPaymentResponse result = checkinSession.endPayment(request, ctx, approvalCode);
		
		if (result != null) {
			if(result.getEndPaymentResponse().getError() != null && !result.getEndPaymentResponse().getError().equals("")){
				managementError(request, response, result.getEndPaymentResponse().getError());
			}else{

				//rieseguo l'accettazione del passeggero che ha acquistato il posto comfort
				boolean recheckin = checkinSession.checkinSelectedPassengerAfterPayment(request, ctx);

				try {

					SetEmdAfterPaymnt(result,ctx);

					if (!result.getEndPaymentResponse().getaZPayedTPResponse().getAzPaxPayedTP().isEmpty()) {
						for (com.alitalia.aem.common.data.checkinrest.model.endpayment.response.AzPaxPayedTP azPaxPayedTP : result.getEndPaymentResponse().getaZPayedTPResponse().getAzPaxPayedTP()) {
							if (!azPaxPayedTP.getAzAncillaryPayedTP().isEmpty()) {
								for (com.alitalia.aem.common.data.checkinrest.model.endpayment.response.AzAncillaryPayedTP azAncillaryPayedTP : azPaxPayedTP.getAzAncillaryPayedTP()) {
									setNewPassengerSeat(ctx.comfortSeat, ctx, request);

								}
							}
						}
					}

					//ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengersExtra().get(j).getPassenger().setNumeroPostoAssegnato(serviceResponce.getPnrData().getPnr().get(0));


				} catch (Exception q)
				{

				}

				//response.sendRedirect(successPage);
				managementSucces(request, response, ctx, result,i18n);
				ctx.checkinPaymentDone = true;
			}
		} else {
			logger.error("CheckinEndPaymentServlet - Errore durante l'invocazione del servizio." );
			managementError(request, response, "Errore nell'invocazione del servizio.");
		}
	
	
	}

	public void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error){

		logger.info("[CheckinEndPaymentServlet] [managementError] error =" + error );

		Gson gson = new Gson();
		String redirectPage = request.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) + configuration.getCheckinThankYouPage());

		try {
			Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);
			final I18n i18n = new I18n(request.getResourceBundle(AlitaliaUtils.findResourceLocale(request.getResource())));

			if (errorObj.getErrorMessage().toLowerCase().contains("ISSUE_ERROR".toLowerCase())) {
				ctx.paymentErrorMsg = i18n.get("checkin.payment.error.issue");
			} else if (errorObj.getErrorMessage().toLowerCase().contains("An error has occurred".toLowerCase())) {
				ctx.paymentErrorMsg = i18n.get("checkin.payment.error.issue");
			} else {
				ctx.paymentErrorMsg = i18n.get("checkin.payment.error.issue");
			}

			//rieseguo l'accettazione del passeggero che ha acquistato il posto comfort anche se non emesso - l'utente ha pagato
			boolean recheckin = checkinSession.checkinSelectedPassengerAfterPayment(request, ctx);

			response.sendRedirect(redirectPage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response, CheckinSessionContext ctx,CheckinEndPaymentResponse result,  I18n i18n){

		logger.info("[CheckinEndPaymentServlet] [managementSucces] ...");

		try {

			byte[] attachment = null;

			//recupero indirizzo email inserito dall'utente
			List<String> mails = new ArrayList<String>();
			String[] _mails = ctx.checkinInitPaymentRequest.getEmail().split(",");
			for(String _mail:_mails)
			{
				mails.add(_mail);
			}

			/* recupero informazioni da passare al managementSuccess per invio ricevuta
			 *   via email in caso di  pagamento andato a buon fine */
			String[] values = new String[4];
			values[0] = ctx.checkinInitPaymentRequest.getPaymentDetail().getCardHolderFirstName() + " " + ctx.checkinInitPaymentRequest.getPaymentDetail().getCardHolderLastName();
			values[1] = ctx.pnr;
			values[2] = "";
			values[3] = "";//computeBusMessage(ctx);

			int total = 0;
			int executed = 0;
			int notAllowed = 0;
			//response.sendRedirect(successPage);
			PnrListDataRender renderList = new PnrListDataRender();
			List<PnrRender> pnrListDataRender = new ArrayList<>();
			for (PnrRender pnrRender : renderList.getPnrListDataRender(ctx.pnrListData, ctx, total, executed, notAllowed)) {

				for (FlightsRender flightsRender : pnrRender.getFlightsRender()) {
					flightsRender.setTotalAncillaryForPax(new ArrayList<>());
					if (flightsRender.getSegments().get(0).getPassengers() != null) {
						for (Passenger passenger : flightsRender.getSegments().get(0).getPassengers()) {
							TotalAncillaryForPax totalAncillaryForPaxSet = new TotalAncillaryForPax();
							totalAncillaryForPaxSet.setPassenger(passenger);
							flightsRender.getTotalAncillaryForPax().add(totalAncillaryForPaxSet);
						}
					}
				}
				for (FlightsRender flightsRender : ctx.pnrSelectedListDataRender.get(0).getFlightsRender()) {
					for (Passenger passenger : flightsRender.getSegments().get(0).getPassengers()) {
						passenger.setNumberOfBaggage(0);
						for (Emd emd : passenger.getEmd()) {
							if (emd.getGroup().equals("BG")) {
								passenger.setNumberOfBaggage(passenger.getNumberOfBaggage() + 1);
							}
						}

					}
				}

				for (FlightsRender flightsRender : pnrRender.getFlightsRender()) {
					for (SegmentRender segmentRender : flightsRender.getSegments()) {
						if (segmentRender.getPassengers() != null) {
							for (Passenger passenger : segmentRender.getPassengers()) {
								passenger.setNumberOfFT(0);
								passenger.setNumberOfBaggage(0);
								passenger.setNumberOfLouge(0);
								for (Emd emd : passenger.getEmd()) {
									if (emd.getGroup().equals("TS")) {
										passenger.setNumberOfFT(passenger.getNumberOfFT() + 1);
									}
									if (emd.getGroup().equals("LG")) {
										passenger.setNumberOfLouge(passenger.getNumberOfLouge() + 1);
									}
									if (emd.getGroup().equals("BG")) {
										passenger.setNumberOfBaggage(passenger.getNumberOfBaggage() + 1);
									}
								}
								for (TotalAncillaryForPax totalAncillaryForPax : flightsRender.getTotalAncillaryForPax()) {
									if (totalAncillaryForPax.getPassenger().getNome().equals(passenger.getNome()) && totalAncillaryForPax.getPassenger().getCognome().equals(passenger.getCognome())) {
										totalAncillaryForPax.setTotalBaggage(passenger.getNumberOfBaggage());
										totalAncillaryForPax.setTotalFast(totalAncillaryForPax.getTotalFast() + passenger.getNumberOfFT());
										totalAncillaryForPax.setTotalLounge(totalAncillaryForPax.getTotalLounge() + passenger.getNumberOfLouge());
									}
								}
							}
						}
					}
				}
				pnrListDataRender.add(pnrRender);
			}

			values[2] = getBodyMailAncillary(request, ctx, pnrListDataRender, result, ctx.checkinInitPaymentRequest.getPaymentDetail().getCardCode(), new HierarchyNodeInheritanceValueMap(request.getResource().getParent()).getInherited("currencySymbol", String.class), i18n);


			checkinSession.sendEmail(ctx, request.getResource(), mails, CheckinConstants.CHECKIN_TEMPLATE_EMAIL_ANCILLARY, CheckinConstants.CHECKIN_ANCILLARY_EMAIL_SUBJECT, attachment, CheckinConstants.CHECKIN_ANCILLARY_EMAIL_ATTACHMENT, CheckinConstants.PLACEHOLDER_BOARDING, values, CheckinConstants.CHECKIN_MAIL_REMINDER, ctx.selectedPassengers);


			String successPage = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) + configuration.getCheckinThankYouPage();

			successPage = request.getResourceResolver().map(successPage);
			
			response.sendRedirect(successPage);

			logger.info("[CheckinInitPaymentServlet] [managementSuccess 3D] successPage = " + successPage );

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getBodyMailAncillary(SlingHttpServletRequest request, CheckinSessionContext ctx, List<PnrRender> pnrRenders, CheckinEndPaymentResponse result, String card, String currency, I18n i18n) {
		String stru = "";


		for (PnrRender pnrRender : pnrRenders) {
			if (pnrRender.getNumber().equals(ctx.pnrSelectedListDataRender.get(0).getNumber())) {

				stru += " <div style=\"height: 20px;background-color: #f8f8f8\"></div>\n" +
						"\n" +
						"    <table class=\"row\"\n" +
						"           style=\"padding-top: 20px;background-color: #f8f8f8;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
						"        <tbody>\n" +
						"        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"            <th class=\"small-12 large-12 columns first last\"\n" +
						"                style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:20px;padding-right:20px;text-align:left;width:580px\">\n" +
						"                <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\"> ";
				stru += getFlights(result.getEndPaymentResponse().getAuthorizationResult(), request, ctx, pnrRender, result, currency, i18n);
				stru += space();
				stru += getPaymentMethod(result.getEndPaymentResponse().getAuthorizationResult(), i18n.get("checkin.mail.ancillary.totale"), currency, ctx.totalPay, card, request, i18n);
				stru +=
						"            </th>\n" +
								"        </tr>\n" +
								"        </table>\n" +
								"        </th>\n" +
								"        </tr>\n" +
								"        </tbody>\n" +
								"    </table>";
			}

		}
		return stru;
	}

	private String getFlights(com.alitalia.aem.common.data.checkinrest.model.endpayment.response.AuthorizationResult authorizationResult, SlingHttpServletRequest request, CheckinSessionContext ctx, PnrRender pnrRender, CheckinEndPaymentResponse result, String currency, I18n i18n) {
		String stru = "";
		for (FlightsRender flightsRender : pnrRender.getFlightsRender()) {

			stru += "<p class=\"info-title\" style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">";

			if (pnrRender.getFlightsRender().size() > 1) {
				if (flightsRender.equals(pnrRender.getFlightsRender().get(0))) {
					stru += "<strong>" + i18n.get("checkin.mail.ancillary.service") + " - " + i18n.get("checkin.mail.ancillary.voloAndata") + "</strong>";
				} else {
					stru += "<strong>" + i18n.get("checkin.mail.ancillary.service") + " - " + i18n.get("checkin.mail.ancillary.voloRitorno") + "</strong>";
				}
			} else {
				stru += "<strong>" + i18n.get("checkin.mail.ancillary.service") + " - " + i18n.get("checkin.mail.ancillary.voloAndata") + "</strong>";
			}

			stru += "</p>\n";
			stru += space();
			stru += "\n" +
					"                                <!-- Inizio titolo -->\n" +
					"\n" +
					"                                <table class=\"wrapper\" align=\"center\" bgcolor=\"#006643\"\n" +
					"                                       style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
					"                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
					"                                        <td class=\"wrapper-inner\"\n" +
					"                                            style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
					"                                            <p class=\"info-header\"\n" +
					"                                               style=\"Margin:10px;Margin-bottom:10px;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin:10px;margin-bottom:10px;padding:0;text-align:left\">\n" +
					"                                                " + flightsRender.getOrigin().getCity() + "\n" +
					"                                                <strong>" + flightsRender.getOrigin().getCode() + "</strong> - " + flightsRender.getDestination().getCity() + "\n" +
					"                                                <strong>" + flightsRender.getDestination().getCode() + "</strong>\n";
			if (flightsRender.getSegments().size() > 1) {
				stru += "<span class=\"small\" style=\"font-size:14px\">" + (flightsRender.getSegments().size() - 1);
				if (flightsRender.getSegments().size() == 1) {
					stru += "\" scalo.label</span>\"";
				} else {
					stru += "scali.label</span>";
				}
			}

			stru += "</p>\n" +
					"                                        </td>\n" +
					"                                    </tr>\n" +
					"                                </table>\n" +
					"\n" +
					"                                <!-- Fine titolo -->\n" +
					"\n" +
					"                                <table class=\"wrapper\" align=\"center\" bgcolor=\"#ffffff\"\n" +
					"                                       style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
					"                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
					"                                        <td class=\"wrapper-inner\"\n" +
					"                                            style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
					"                                            <div class=\"info\" style=\"Margin:10px;margin:10px\">\n" +
					"\n" +
					"                                                ";
			for (SegmentRender segment : flightsRender.getSegments()) {
				stru += "<!-- Inizio segmento -->";
				stru += space();
				stru += "<table class=\"row\"\n" +
						"                                                           style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                        <tbody>\n" +
						"                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                            <th class=\"small-12 large-6 columns first\"\n" +
						"                                                                style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:50%\">\n" +
						"                                                                <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
						"                                                                            <div class=\"arr-dep\"\n" +
						"                                                                                 style=\"Margin:0 0 25px;margin:0 0 25px\">\n" +
						"                                                                                <strong>" + i18n.get("checkin.mail.ancillary.partenza") + "</strong>\n" +
						"                                                                            </div>\n" +
						"                                                                            <table class=\"row no-padding m-10\"\n" +
						"                                                                                   style=\"Margin:0 0 10px;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 10px;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                <tbody>\n" +
						"                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                    <th class=\"small-12 large-12 columns first last\"\n" +
						"                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%\">\n" +
						"                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">\n" +
						"                                                                          <span class=\"small\" style=\"font-size:14px\">" + segment.getOrigin().getCity() + "\n" +
						"                                                                            <strong>" + segment.getOrigin().getCity() + "</strong>\n" +
						"                                                                          </span>\n" +
						"                                                                                                </th>\n" +
						"                                                                                                <th class=\"expander\"\n" +
						"                                                                                                    style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0\"></th>\n" +
						"                                                                                            </tr>\n" +
						"                                                                                        </table>\n" +
						"                                                                                    </th>\n" +
						"                                                                                </tr>\n" +
						"                                                                                </tbody>\n" +
						"                                                                            </table>\n" +
						"                                                                            <table class=\"row\"\n" +
						"                                                                                   style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                <tbody>\n" +
						"                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                    <th class=\"small-2 large-2 columns first\"\n" +
						"                                                                                        valign=\"middle\"\n" +
						"                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
						"                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
						"                                                                                                    <img width=\"36\"\n" +
						"                                                                                                         height=\"20\"\n" +
						"                                                                                                         src=" + alitaliaMailUtils.getIconPath("departure", request) +
						"                                                                                                         alt=\"departure\"\n" +
						"                                                                                                         style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
						"                                                                                                </th>\n" +
						"                                                                                            </tr>\n" +
						"                                                                                        </table>\n" +
						"                                                                                    </th>\n" +
						"                                                                                    <th class=\"small-10 large-10 columns last\"\n" +
						"                                                                                        valign=\"middle\"\n" +
						"                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
						"                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
						"                                                                                                    " + segment.getDepartureDate() + "\n" +
						"                                                                                                </th>\n" +
						"                                                                                            </tr>\n" +
						"                                                                                        </table>\n" +
						"                                                                                    </th>\n" +
						"                                                                                </tr>\n" +
						"                                                                                </tbody>\n" +
						"                                                                            </table>\n" +
						"                                                                            <table class=\"row\"\n" +
						"                                                                                   style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                <tbody>\n" +
						"                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                    <th class=\"small-2 large-2 columns first\"\n" +
						"                                                                                        valign=\"middle\"\n" +
						"                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
						"                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
						"                                                                                                    <img width=\"29\"\n" +
						"                                                                                                         height=\"20\"\n" +
						"                                                                                                         src=" + alitaliaMailUtils.getIconPath("clock", request) +
						"                                                                                                         alt=\"clock\"\n" +
						"                                                                                                         style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
						"                                                                                                </th>\n" +
						"                                                                                            </tr>\n" +
						"                                                                                        </table>\n" +
						"                                                                                    </th>\n" +
						"                                                                                    <th class=\"small-10 large-10 columns last\"\n" +
						"                                                                                        valign=\"middle\"\n" +
						"                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
						"                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
						"                                                                                                    " + segment.getDepartureTime() + "\n" +
						"                                                                                                </th>\n" +
						"                                                                                            </tr>\n" +
						"                                                                                        </table>\n" +
						"                                                                                    </th>\n" +
						"                                                                                </tr>\n" +
						"                                                                                </tbody>\n" +
						"                                                                            </table>\n" +
						"                                                                        </th>\n" +
						"                                                                    </tr>\n" +
						"                                                                </table>\n" +
						"                                                            </th>\n" +
						"                                                            <th class=\"small-12 large-6 columns last\"\n" +
						"                                                                style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:50%\">\n" +
						"                                                                <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
						"                                                                            <div class=\"arr-dep\"\n" +
						"                                                                                 style=\"Margin:0 0 25px;margin:0 0 25px\">\n" +
						"                                                                                <strong>" + i18n.get("checkin.mail.ancillary.arrivo") + "</strong>\n" +
						"                                                                            </div>\n" +
						"                                                                            <table class=\"row no-padding m-10\"\n" +
						"                                                                                   style=\"Margin:0 0 10px;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 10px;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                <tbody>\n" +
						"                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                    <th class=\"small-12 large-12 columns first last\"\n" +
						"                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%\">\n" +
						"                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">\n" +
						"                                                                          <span class=\"small\" style=\"font-size:14px\">" + segment.getDestination().getCity() + "\n" +
						"                                                                            <strong>" + segment.getDestination().getCode() + "</strong>\n" +
						"                                                                          </span>\n" +
						"                                                                                                </th>\n" +
						"                                                                                                <th class=\"expander\"\n" +
						"                                                                                                    style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0\"></th>\n" +
						"                                                                                            </tr>\n" +
						"                                                                                        </table>\n" +
						"                                                                                    </th>\n" +
						"                                                                                </tr>\n" +
						"                                                                                </tbody>\n" +
						"                                                                            </table>\n" +
						"                                                                            <table class=\"row\"\n" +
						"                                                                                   style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                <tbody>\n" +
						"                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                    <th class=\"small-2 large-2 columns first\"\n" +
						"                                                                                        valign=\"middle\"\n" +
						"                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
						"                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
						"                                                                                                    <img width=\"36\"\n" +
						"                                                                                                         height=\"20\"\n" +
						"                                                                                                         src=" + alitaliaMailUtils.getIconPath("arrival", request) +
						"                                                                                                         alt=\"arrival\"\n" +
						"                                                                                                         style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
						"                                                                                                </th>\n" +
						"                                                                                            </tr>\n" +
						"                                                                                        </table>\n" +
						"                                                                                    </th>\n" +
						"                                                                                    <th class=\"small-10 large-10 columns last\"\n" +
						"                                                                                        valign=\"middle\"\n" +
						"                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
						"                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
						"                                                                                                    " + segment.getArrivalDate() + "                                                                                           </th>\n" +
						"                                                                                            </tr>\n" +
						"                                                                                        </table>\n" +
						"                                                                                    </th>\n" +
						"                                                                                </tr>\n" +
						"                                                                                </tbody>\n" +
						"                                                                            </table>\n" +
						"                                                                            <table class=\"row\"\n" +
						"                                                                                   style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                <tbody>\n" +
						"                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                    <th class=\"small-2 large-2 columns first\"\n" +
						"                                                                                        valign=\"middle\"\n" +
						"                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
						"                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
						"                                                                                                    <img width=\"29\"\n" +
						"                                                                                                         height=\"20\"\n" +
						"                                                                                                         src=" + alitaliaMailUtils.getIconPath("clock", request) +
						"                                                                                                         alt=\"clock\"\n" +
						"                                                                                                         style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
						"                                                                                                </th>\n" +
						"                                                                                            </tr>\n" +
						"                                                                                        </table>\n" +
						"                                                                                    </th>\n" +
						"                                                                                    <th class=\"small-10 large-10 columns last\"\n" +
						"                                                                                        valign=\"middle\"\n" +
						"                                                                                        style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
						"                                                                                        <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                                                <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
						"                                                                                                    " + segment.getArrivalTime() + "                                                                                            </th>\n" +
						"                                                                                            </tr>\n" +
						"                                                                                        </table>\n" +
						"                                                                                    </th>\n" +
						"                                                                                </tr>\n" +
						"                                                                                </tbody>\n" +
						"                                                                            </table>\n" +
						"                                                                        </th>\n" +
						"                                                                    </tr>\n" +
						"                                                                </table>\n" +
						"                                                            </th>\n" +
						"                                                        </tr>\n" +
						"                                                        </tbody>\n" +
						"                                                    </table>\n" +
						"                                                    <table class=\"row no-padding\"\n" +
						"                                                           style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                        <tbody>\n" +
						"                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                            <th class=\"small-12 large-4 columns first\"\n" +
						"                                                                style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
						"                                                                <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">\n" +
						"                                                                            <p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">\n" +
						"                                                                                " + i18n.get("checkin.mail.ancillary.numeroVolo") +
						"                                                                                <br>\n" +
						"                                                                                <strong>" + segment.getAirline() + " " + segment.getFlight() + " </strong>\n" +
						"                                                                            </p>\n" +
						"                                                                        </th>\n" +
						"                                                                    </tr>\n" +
						"                                                                </table>\n" +
						"                                                            </th>\n" +
						"\n";
				String waiting = "", sostaLabel = "";
				if (!segment.getWaiting().equals("00:00:00")) {

					waiting = segment.getWaiting();
					sostaLabel = "ore di sosta label";

				}
				stru += "<th \n" +
						"                                                                class=\"small-12 large-8 columns last\"\n" +
						"                                                                style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:66.66667%\">\n" +
						"                                                                <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
						"                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
						"                                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">\n" +
						"                                                                            <p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">\n" +
						"                                                                                " + sostaLabel +
						"                                                                                <br>\n" +
						"                                                                                <strong>" + waiting + "</strong>\n" +
						"                                                                            </p>\n" +
						"                                                                        </th>\n" +
						"                                                                    </tr>\n" +
						"                                                                </table>\n" +
						"                                                            </th>\n" +
						"                                                        </tr>\n" +
						"                                                        </tbody>\n" +
						"                                                    </table>\n" +
						"\n" +
						"\n" +
						"\n" +
						"                                                <!-- Fine segmento -->";
				stru += row();
			}
			for (TotalAncillaryForPax totalAncillaryForPax : flightsRender.getTotalAncillaryForPax()) {
				for (SegmentRender seg : flightsRender.getSegments()) {
					for (Passenger passenger : seg.getPassengers()) {
						if (totalAncillaryForPax.getPassenger().getNome().equals(passenger.getNome()) && totalAncillaryForPax.getPassenger().getCognome().equals(passenger.getCognome())) {
							if (passenger.getPassengerSeats() != null) {
								totalAncillaryForPax.setConfortSeat(passenger.getPassengerSeats());
							}
						}
					}
				}
				if (totalAncillaryForPax.getTotalBaggage() > 0 || totalAncillaryForPax.getTotalFast() > 0 || totalAncillaryForPax.getTotalLounge() > 0 || totalAncillaryForPax.getConfortSeat() != null) {
//                if (totalAncillaryForPax.getTotalBaggage() > 0 || totalAncillaryForPax.getTotalFast() > 0 || totalAncillaryForPax.getTotalLounge() > 0) {

					stru += space();
					stru += "<!-- Inizio nome pass -->\n" +
							"\n" +
							"                                                    <table class=\"row\"\n" +
							"                                                           style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
							"                                                        <tbody>\n" +
							"                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
							"                                                            <th class=\"small-12 large-12 columns first last\"\n" +
							"                                                                style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%\">\n" +
							"                                                                <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
							"                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
							"                                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
							"                                                                            <p class=\"name\"\n" +
							"                                                                               style=\"Margin:0;Margin-bottom:10px;color:#006643;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">\n" +
							"                                                                                <strong>" + totalAncillaryForPax.getPassenger().getNome() + " " + totalAncillaryForPax.getPassenger().getCognome() + "</strong>\n" +
							"                                                                            </p>\n" +
							"                                                                        </th>\n" +
							"                                                                        <th class=\"expander\"\n" +
							"                                                                            style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0\"></th>\n" +
							"                                                                    </tr>\n" +
							"                                                                </table>\n" +
							"                                                            </th>\n" +
							"                                                        </tr>\n" +
							"                                                        </tbody>\n" +
							"                                                    </table>" +
							"<!-- Fine nome pass -->";
					if (totalAncillaryForPax.getConfortSeat() != null) {
						for (PassengerSeat passengerSeat : totalAncillaryForPax.getConfortSeat()) {
							stru += " <table class=\"row extra-info no-padding\" style=\"Margin:0 0 20px!important;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 20px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
									"                                                <tbody>\n" +
									"                                                  <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
									"                                                    <th class=\"small-6 large-5 columns first\" style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:41.66667%\">\n" +
									"                                                      <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
									"                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
									"                                                          <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
									"                                                            <table class=\"row\" style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
									"                                                              <tbody>\n" +
									"                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
									"                                                                  <th class=\"small-2 large-2 columns first\" valign=\"middle\"\n" +
									"                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
									"                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
									"                                                                      <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
									"                                                                        <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
									"                                                                          <img width=\"38\" height=\"25\" src=" + alitaliaMailUtils.getIconPath("seat", request) +
									"                                                                            alt=\"seat\" style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
									"                                                                        </th>\n" +
									"                                                                      </tr>\n" +
									"                                                                    </table>\n" +
									"                                                                  </th>\n" +
									"                                                                  <th class=\"small-10 large-10 columns last\" valign=\"middle\"\n" +
									"                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
									"                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
									"                                                                      <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
									"                                                                        <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">" + i18n.get("checkin.mail.ancillary.seat") + "</th>\n" +
									"                                                                      </tr>\n" +
									"                                                                    </table>\n" +
									"                                                                  </th>\n" +
									"                                                                </tr>\n" +
									"                                                              </tbody>\n" +
									"                                                            </table>\n" +
									"                                                          </th>\n" +
									"                                                        </tr>\n" +
									"                                                      </table>\n" +
									"                                                    </th>\n" +
									"                                                    <th class=\"small-6 large-4 columns\" valign=\"middle\" style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
									"                                                      <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
									"                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
									"                                                          <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
									"                                                            <strong>" + passengerSeat.getNumeroPostoAssegnato() + " (" + passengerSeat.getOrigin() + ")</strong>\n" +
									"                                                          </th>\n" +
									"                                                        </tr>\n" +
									"                                                      </table>\n" +
									"                                                    </th>\n" +
									"                                                    <th class=\"small-12 large-3 columns last\" valign=\"middle\" style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:25%\">\n" +
									"                                                      <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
									"                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
									"                                                          <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">" + "</th>\n" +
									"                                                        </tr>\n" +
									"                                                      </table>\n" +
									"                                                    </th>\n" +
									"                                                  </tr>\n" +
									"                                                </tbody>\n" +
									"                                              </table>";
						}
					}
					if (totalAncillaryForPax.getTotalBaggage() > 0) {
						stru += "<!-- Inizio Bagaglio extra -->\n" +
								"\n" +
								"                                                        <table class=\"row extra-info no-padding\"\n" +
								"                                                               style=\"Margin:0 0 20px!important;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 20px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                            <tbody>\n" +
								"                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                <th class=\"small-6 large-5 columns first\"\n" +
								"                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:41.66667%\">\n" +
								"                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                <table class=\"row\"\n" +
								"                                                                                       style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                                    <tbody>\n" +
								"                                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                                        <th class=\"small-2 large-2 columns first\"\n" +
								"                                                                                            valign=\"middle\"\n" +
								"                                                                                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
								"                                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                                        <img width=\"38\"\n" +
								"                                                                                                             height=\"25\"\n" +
								"                                                                                                             src=" + alitaliaMailUtils.getIconPath("bagaglio", request) +
								"                                                                                                             alt=\"luggage\"\n" +
								"                                                                                                             style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
								"                                                                                                    </th>\n" +
								"                                                                                                </tr>\n" +
								"                                                                                            </table>\n" +
								"                                                                                        </th>\n" +
								"                                                                                        <th class=\"small-10 large-10 columns last\"\n" +
								"                                                                                            valign=\"middle\"\n" +
								"                                                                                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
								"                                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                                        " + i18n.get("checkin.mail.ancillary.bagaglioExtra") +
								"                                                                                                    </th>\n" +
								"                                                                                                </tr>\n" +
								"                                                                                            </table>\n" +
								"                                                                                        </th>\n" +
								"                                                                                    </tr>\n" +
								"                                                                                    </tbody>\n" +
								"                                                                                </table>\n" +
								"                                                                            </th>\n" +
								"                                                                        </tr>\n" +
								"                                                                    </table>\n" +
								"                                                                </th>\n" +
								"                                                                <th class=\"small-6 large-4 columns\"\n" +
								"                                                                    valign=\"middle\"\n" +
								"                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
								"                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                <strong>" + totalAncillaryForPax.getTotalBaggage() + " " + i18n.get("checkin.mail.ancillary.bagaglio") +
								"                                                                                    </strong>\n" +
								"                                                                            </th>\n" +
								"                                                                        </tr>\n" +
								"                                                                    </table>\n" +
								"                                                                </th>\n" +
								"                                                                <th class=\"small-12 large-3 columns last\"\n" +
								"                                                                    valign=\"middle\"\n" +
								"                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:25%\">\n" +
								"                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                " +
								"                                                                            </th>\n" +
								"                                                                        </tr>\n" +
								"                                                                    </table>\n" +
								"                                                                </th>\n" +
								"                                                            </tr>\n" +
								"                                                            </tbody>\n" +
								"                                                        </table>\n" +
								"\n" +
								"                                                        <!-- Fine Bagaglio extra -->";


					}
					if (totalAncillaryForPax.getTotalFast() > 0) {
						stru += "<!-- Inizio fast track -->\n" +
								"\n" +
								"                                                        <table class=\"row extra-info no-padding\"\n" +
								"                                                               style=\"Margin:0 0 20px!important;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 20px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                            <tbody>\n" +
								"                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                <th class=\"small-6 large-5 columns first\"\n" +
								"                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:41.66667%\">\n" +
								"                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                <table class=\"row\"\n" +
								"                                                                                       style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                                    <tbody>\n" +
								"                                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                                        <th class=\"small-2 large-2 columns first\"\n" +
								"                                                                                            valign=\"middle\"\n" +
								"                                                                                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
								"                                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                                        <img width=\"38\"\n" +
								"                                                                                                             height=\"26\"\n" +
								"                                                                                                             src=" + alitaliaMailUtils.getIconPath("fast", request) +
								"                                                                                                             alt=\"fast-track\"\n" +
								"                                                                                                             style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
								"                                                                                                    </th>\n" +
								"                                                                                                </tr>\n" +
								"                                                                                            </table>\n" +
								"                                                                                        </th>\n" +
								"                                                                                        <th class=\"small-10 large-10 columns last\"\n" +
								"                                                                                            valign=\"middle\"\n" +
								"                                                                                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
								"                                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                                        " + i18n.get("checkin.mail.ancillary.fastTrack") + "\n" +
								"                                                                                                    </th>\n" +
								"                                                                                                </tr>\n" +
								"                                                                                            </table>\n" +
								"                                                                                        </th>\n" +
								"                                                                                    </tr>\n" +
								"                                                                                    </tbody>\n" +
								"                                                                                </table>\n" +
								"                                                                            </th>\n" +
								"                                                                        </tr>\n" +
								"                                                                    </table>\n" +
								"                                                                </th>\n" +
								"                                                                <th class=\"small-6 large-4 columns\"\n" +
								"                                                                    valign=\"middle\"\n" +
								"                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
								"                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                <strong>" + totalAncillaryForPax.getTotalFast() + " " + i18n.get("checkin.mail.ancillary.fastTrack.ingressi") + "</strong>\n" +
								"                                                                            </th>\n" +
								"                                                                        </tr>\n" +
								"                                                                    </table>\n" +
								"                                                                </th>\n" +
								"                                                                <th class=\"small-12 large-3 columns last\"\n" +
								"                                                                    valign=\"middle\"\n" +
								"                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:25%\">\n" +
								"                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                " +
								"                                                                            </th>\n" +
								"                                                                        </tr>\n" +
								"                                                                    </table>\n" +
								"                                                                </th>\n" +
								"                                                            </tr>\n" +
								"                                                            </tbody>\n" +
								"                                                        </table>\n" +
								"\n" +
								"                                                        <!-- Fine fast track -->";
					}
					if (totalAncillaryForPax.getTotalLounge() > 0) {

						stru += " <!-- Inizio lounge -->\n" +
								"\n" +
								"                                                        <table class=\"row extra-info no-padding\"\n" +
								"                                                               style=\"Margin:0 0 20px!important;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 20px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                            <tbody>\n" +
								"                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                <th class=\"small-6 large-5 columns first\"\n" +
								"                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:41.66667%\">\n" +
								"                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                <table class=\"row\"\n" +
								"                                                                                       style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                                    <tbody>\n" +
								"                                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                                        <th class=\"small-2 large-2 columns first\"\n" +
								"                                                                                            valign=\"middle\"\n" +
								"                                                                                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
								"                                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                                        <img width=\"38\"\n" +
								"                                                                                                             height=\"25\"\n" +
								"                                                                                                             src=" + alitaliaMailUtils.getIconPath("lounge", request) +
								"                                                                                                             alt=\"lounge\"\n" +
								"                                                                                                             style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">\n" +
								"                                                                                                    </th>\n" +
								"                                                                                                </tr>\n" +
								"                                                                                            </table>\n" +
								"                                                                                        </th>\n" +
								"                                                                                        <th class=\"small-10 large-10 columns last\"\n" +
								"                                                                                            valign=\"middle\"\n" +
								"                                                                                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">\n" +
								"                                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                                        " + i18n.get("checkin.mail.ancillary.lounge") + "\n" +
								"                                                                                                    </th>\n" +
								"                                                                                                </tr>\n" +
								"                                                                                            </table>\n" +
								"                                                                                        </th>\n" +
								"                                                                                    </tr>\n" +
								"                                                                                    </tbody>\n" +
								"                                                                                </table>\n" +
								"                                                                            </th>\n" +
								"                                                                        </tr>\n" +
								"                                                                    </table>\n" +
								"                                                                </th>\n" +
								"                                                                <th class=\"small-6 large-4 columns\"\n" +
								"                                                                    valign=\"middle\"\n" +
								"                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
								"                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                <strong>" + totalAncillaryForPax.getTotalLounge() + " " + i18n.get("checkin.mail.ancillary.lounge.acquistati") + " </strong>\n" +
								"                                                                            </th>\n" +
								"                                                                        </tr>\n" +
								"                                                                    </table>\n" +
								"                                                                </th>\n" +
								"                                                                <th class=\"small-12 large-3 columns last\"\n" +
								"                                                                    valign=\"middle\"\n" +
								"                                                                    style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:25%\">\n" +
								"                                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
								"                                                                        <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
								"                                                                            <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
								"                                                                                " +
								"                                                                            </th>\n" +
								"                                                                        </tr>\n" +
								"                                                                    </table>\n" +
								"                                                                </th>\n" +
								"                                                            </tr>\n" +
								"                                                            </tbody>\n" +
								"                                                        </table>\n" +
								"\n" +
								"                                                        <!-- Fine lounge-->";
					}
				}
			}
			stru += "\n" +
					"\n" +
					"\n" +
					"                                                <!-- Fine pax e ancillary acquistati-->\n" +
					"\n" +
					"                                            </div>\n" +
					"                                        </td>\n" +
					"                                    </tr>\n" +
					"</table>\n" +
					"\n" +
					"                            <!-- Fine volo andata/ritorno -->\n" +
					"\n";
			stru += space();

		}
		if(authorizationResult != null) {
			if (authorizationResult.getInsurancePayment() != null || ctx.pnrListData.get(0).getInsurance()) {
				stru += getInsurance(authorizationResult, currency, request, i18n);

			}
		}
		stru += space();
		stru += getLabel(i18n.get("checkin.mail.ancillary.cart"));
		for (com.alitalia.aem.common.data.checkinrest.model.endpayment.response.AzPaxPayedTP azPaxPayedTP : result.getEndPaymentResponse().getaZPayedTPResponse().getAzPaxPayedTP()) {
			stru += getCart(azPaxPayedTP, authorizationResult, currency,i18n);
		}
		return stru;
	}

	private String getCssHead() {
		String stru = "<style>\n" +
				"   img {width : 20px; height : 20px;} " +
				"@media only screen {\n" +
				"      html {\n" +
				"        min-height: 100%;\n" +
				"        background: #f3f3f3\n" +
				"      }\n" +
				"    }\n" +
				"\n" +
				"    @media only screen and (max-width:620px) {\n" +
				"      table.body img {\n" +
				"        width: auto;\n" +
				"        height: auto\n" +
				"      }\n" +
				"      table.body center {\n" +
				"        min-width: 0 !important\n" +
				"      }\n" +
				"      table.body .container {\n" +
				"        width: 95% !important\n" +
				"      }\n" +
				"      table.body .columns {\n" +
				"        height: auto !important;\n" +
				"        -moz-box-sizing: border-box;\n" +
				"        -webkit-box-sizing: border-box;\n" +
				"        box-sizing: border-box;\n" +
				"        padding-left: 20px !important;\n" +
				"        padding-right: 20px !important\n" +
				"      }\n" +
				"      table.body .columns .columns {\n" +
				"        padding-left: 0 !important;\n" +
				"        padding-right: 0 !important\n" +
				"      }\n" +
				"      th.small-2 {\n" +
				"        display: inline-block !important;\n" +
				"        width: 16.66667% !important\n" +
				"      }\n" +
				"      th.small-6 {\n" +
				"        display: inline-block !important;\n" +
				"        width: 50% !important\n" +
				"      }\n" +
				"      th.small-10 {\n" +
				"        display: inline-block !important;\n" +
				"        width: 83.33333% !important\n" +
				"      }\n" +
				"      th.small-12 {\n" +
				"        display: inline-block !important;\n" +
				"        width: 100% !important\n" +
				"      }\n" +
				"      .columns th.small-12 {\n" +
				"        display: block !important;\n" +
				"        width: 100% !important\n" +
				"      }\n" +
				"      table.menu {\n" +
				"        width: 100% !important\n" +
				"      }\n" +
				"      table.menu td,\n" +
				"      table.menu th {\n" +
				"        width: auto !important;\n" +
				"        display: inline-block !important\n" +
				"      }\n" +
				"      table.menu.vertical td,\n" +
				"      table.menu.vertical th {\n" +
				"        display: block !important\n" +
				"      }\n" +
				"      table.menu[align=center] {\n" +
				"        width: auto !important\n" +
				"      }\n" +
				"    }\n" +
				"  </style>";
		return stru;
	}

	private String getPaymentMethod(com.alitalia.aem.common.data.checkinrest.model.endpayment.response.AuthorizationResult authorizationResult, String totaleLabel, String currency, String totalePrice, String card, SlingHttpServletRequest request, I18n i18n) {
		String[] price = new String[2];
		String decimalSeparator = new HierarchyNodeInheritanceValueMap(request.getResource().getParent()).getInherited("decimalSeparator", String.class);
		price = totalePrice.split(",");
		if(price.length > 1) {
			totalePrice = price[0] + decimalSeparator + price[1].substring(0, 2);
		} else {
			totalePrice = totalePrice + decimalSeparator + "00";
		}
		String stru = "<table class=\"row payment\"\n" +
				"                                                                       style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
				"                                                                    <tbody>\n" +
				"                                                                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                                                                        <th class=\"small-12 large-2 columns first\"\n" +
				"                                                                            style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">\n" +
				"                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
				"                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                                                                                    <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
				"                                                                                        <center data-parsed=\"\"\n" +
				"                                                                                                style=\"min-width:none!important;width:100%\">\n" +
				"                                                                                            <img width='63' height='49'" +
				"                                                                                                 src=" + alitaliaMailUtils.getIconPath(card, request) +
				"                                                                                                 alt=\"mastercard\"\n" +
				"                                                                                                 align=\"center\"\n" +
				"                                                                                                 class=\"float-center\"\n" +
				"                                                                                                 style=\"-ms-interpolation-mode:bicubic;Margin:0 auto;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto\">\n" +
				"                                                                                        </center>\n" +
				"                                                                                    </th>\n" +
				"                                                                                </tr>\n" +
				"                                                                            </table>\n" +
				"                                                                        </th>\n" +
				"                                                                        <th class=\"small-12 large-6 columns\"\n" +
				"                                                                            style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:50%\">\n" +
				"                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
				"                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                                                                                    <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
				"                                                                                        <p class=\"text-center\"\n" +
				"                                                                                           style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:center\">" +
				"" + i18n.get("checkin.mail.ancillary.pagamento") +
				"                                                                                            <strong class=\"card\"\n" +
				"                                                                                                    style=\"font-size:20px\">" + card + "</strong>\n" +
				"                                                                                            <br>\n" +
				"                                                                                            <strong class=\"card\"\n" +
				"                                                                                                    style=\"font-size:20px\">" + ctx.checkinInitPaymentRequest.getPaymentDetail().getCardNumber().substring(0, ctx.checkinInitPaymentRequest.getPaymentDetail().getCardNumber().length() - 4).replaceAll("([0-9])", "*") + ctx.checkinInitPaymentRequest.getPaymentDetail().getCardNumber().substring(ctx.checkinInitPaymentRequest.getPaymentDetail().getCardNumber().length() - 4, ctx.checkinInitPaymentRequest.getPaymentDetail().getCardNumber().length()) + "</strong>\n" +
				"                                                                                        </p>\n" +
				"                                                                                    </th>\n" +
				"                                                                                </tr>\n" +
				"                                                                            </table>\n" +
				"                                                                        </th>\n" +
				"                                                                        <th class=\"small-12 large-4 columns last\"\n" +
				"                                                                            style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
				"                                                                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
				"                                                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                                                                                    <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
				"                                                                                        <table class=\"wrapper\"\n" +
				"                                                                                               align=\"center\"\n" +
				"                                                                                               bgcolor=\"#ffffff\"\n" +
				"                                                                                               style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
				"                                                                                            <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                                                                                                <td class=\"wrapper-inner\"\n" +
				"                                                                                                    style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
				"                                                                                                    <div class=\"total\">\n" +
				"                                                                                                        <p class=\"text-center\"\n" +
				"                                                                                                           style=\"Margin:10px;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:10px;margin-bottom:10px;padding:0;text-align:center\">\n" +
				"                                                                                                            " + totaleLabel +
				"                                                                                                            <br>\n" +
				"                                                                                                            <strong style=\"color:#006643;font-size:20px\">" + currency +
				"                                                                                                                " + totalePrice + "</strong>\n" +
				"                                                                                                        </p>\n" +
				"                                                                                                    </div>\n" +
				"                                                                                                </td>\n" +
				"                                                                                            </tr>\n" +
				"                                                                                        </table>\n" +
				"                                                                                    </th>\n" +
				"                                                                                </tr>\n" +
				"                                                                            </table>\n" +
				"                                                                        </th>\n" +
				"                                                                    </tr>\n" +
				"                                                                    </tbody>\n" +
				"                                                                </table>";
		return stru;
	}

	private String space() {
		String stru = "<table class=\"spacer\"\n" +
				"       style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
				"    <tbody>\n" +
				"    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"        <td height=\"30px\"\n" +
				"            style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:30px;font-weight:400;hyphens:auto;line-height:30px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
				"            &#xA0;\n" +
				"        </td>\n" +
				"    </tr>\n" +
				"    </tbody>\n" +
				"</table>";
		return stru;
	}

	private String getLabel(String string) {

		String stru = "<p class=\"info-title\"\n" +
				"   style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">\n" +
				"    <strong>" + string + "</strong>\n" +
				"</p>";
		return stru;

	}

	private String getCart(com.alitalia.aem.common.data.checkinrest.model.endpayment.response.AzPaxPayedTP azPaxPayedTP, com.alitalia.aem.common.data.checkinrest.model.endpayment.response.AuthorizationResult authorizationResult, String currency,I18n i18n) {
		Boolean insurance = false;
		if(authorizationResult != null) {
			if (authorizationResult.getInsurancePayment() != null) {
				insurance = true;
			}
		}
		String stru = "<table class=\"wrapper\" align=\"center\" bgcolor=\"#ffffff\"\n" +
				"       style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
				"    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"        <td class=\"wrapper-inner\"\n" +
				"            style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
				"            <div class=\"info\" style=\"Margin:10px;margin:10px\">";
		stru += getCartName(azPaxPayedTP.getFirstName(), azPaxPayedTP.getLastName());
		stru += row();
		stru += space();
		for (com.alitalia.aem.common.data.checkinrest.model.endpayment.response.AzAncillaryPayedTP azAncillaryPayedTP : azPaxPayedTP.getAzAncillaryPayedTP()) {
			stru += getAncillary(azAncillaryPayedTP.getNumberOfItems(), azAncillaryPayedTP.getGroup(), azAncillaryPayedTP.getAzSegmentPayedTP().get(0).getEmdNumber(), currency, azAncillaryPayedTP.getPrice(),i18n);
		}
		if (insurance) {
			stru += getAncillary("", "assicurazione", "", currency, String.valueOf(authorizationResult.getInsurancePayment().getInsuranceAmount()),i18n);
		}
		stru += space();
		stru += "</div>\n" +
				"        </td>\n" +
				"    </tr>\n" +
				"</table>";


		return stru;
	}

	private String getCartName(String nome, String cognome) {
		String stru = "<table class=\"row no-padding\"\n" +
				"                       style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
				"                    <tbody>\n" +
				"                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                        <th class=\"small-12 large-12 columns first last\"\n" +
				"                            style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%\">\n" +
				"                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
				"                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                                    <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">\n" +
				"                                        <p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">\n" +
				"                                                              <span class=\"name\" style=\"color:#006643;font-size:20px\">\n" +
				"                                                                <strong>" + nome + " " + cognome + "</strong>\n" +
				"                                                              </span>\n" +
				"                                            <br>\n" +
				"                                            <span class=\"small\"\n" +
				"                                                  style=\"font-size:14px\"></span>\n" +
				"                                        </p>\n" +
				"                                    </th>\n" +
				"                                    <th class=\"expander\"\n" +
				"                                        style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0\"></th>\n" +
				"                                </tr>\n" +
				"                            </table>\n" +
				"                        </th>\n" +
				"                    </tr>\n" +
				"                    </tbody>\n" +
				"                </table>";
		return stru;
	}

	private String row() {
		String stru = "                <hr style=\"border:1px solid #e6e6e6\">\n";
		return stru;
	}

	private String getAncillary(String quantita, String desctizione, String codice, String currency, String prezzo,I18n i18n) {
		String stru = " <table class=\"row extra-info no-padding\"\n" +
				"                       style=\"Margin:0 0 20px!important;border-collapse:collapse;border-spacing:0;display:table;margin:0 0 20px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">\n" +
				"                    <tbody>\n" +
				"                    <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                        <th class=\"small-12 large-6 columns first\"\n" +
				"                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:50%\">\n" +
				"                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
				"                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
				"                                        <strong>" + quantita + "</strong> " + i18n.get(alitaliaMailUtils.getAncillaryLabel(desctizione)) +
				"                                    </th>\n" +
				"                                </tr>\n" +
				"                            </table>\n" +
				"                        </th>\n" +
				"                        <th class=\"small-6 large-4 columns\"\n" +
				"                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
				"                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
				"                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n"
				+ codice + "" +
				"</th>\n" +
				"                                </tr>\n" +
				"                            </table>\n" +
				"                        </th>\n" +
				"                        <th class=\"small-6 large-6 columns last\"\n" +
				"                            style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:50%\">\n" +
				"                            <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
				"                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                                    <th style=\"Margin:0 0 5px!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 0 5px!important;padding:0!important;text-align:left\">\n" +
				currency + " " + prezzo + "</th>\n" +
				"                                </tr>\n" +
				"                            </table>\n" +
				"                        </th>\n" +
				"                    </tr>\n" +
				"                    </tbody>\n" +
				"                </table>";
		return stru;
	}

	private String getInsurance(com.alitalia.aem.common.data.checkinrest.model.endpayment.response.AuthorizationResult authorizationResult, String currency, SlingHttpServletRequest request, I18n i18n) {
		String stru = "<table class=\"row insurance\" style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;    background-color: rgb(255, 255, 255);\">\n" +
				"                                              <tbody>\n" +
				"                                                <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                                                  <th class=\"small-12 large-1 columns first\" style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-top:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:8.33333%\">\n" +
				"                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
				"                                                      <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
				"                                                          <center data-parsed=\"\" style=\"min-width:none!important;width:100%\">\n" +
				"                                                            <img width=\"17\" height=\"22\" src='" + alitaliaMailUtils.getIconPath("assicurazione", request) + "' alt=\"insurance\"\n" +
				"                                                              align=\"center\" class=\"float-center\" style=\"-ms-interpolation-mode:bicubic;Margin:0 auto;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto\">\n" +
				"                                                          </center>\n" +
				"                                                        </th>\n" +
				"                                                      </tr>\n" +
				"                                                    </table>\n" +
				"                                                  </th>\n" +
				"                                                  <th class=\"small-12 large-7 columns\" valign=\"middle\" style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-top:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:58.33333%\">\n" +
				"                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
				"                                                      <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
				"                                                          <p class=\"text-center\" style=\"Margin:0!important;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0!important;margin-bottom:10px;padding:0;text-align:center\">\n" +
				"                                                            <strong style=\"color:#006643\">" + i18n.get("checkin.mail.ancillary.insurance") + "</strong> (" + i18n.get("checkin.mail.ancillary.insurance.forAllPax") + ")</p>\n" +
				"                                                        </th>\n" +
				"                                                      </tr>\n" +
				"                                                    </table>\n" +
				"                                                  </th>\n" +
				"                                                  <th class=\"small-12 large-4 columns last\" valign=\"middle\" style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-top:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">\n" +
				"                                                    <table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
				"                                                      <tr style=\"padding:0;text-align:left;vertical-align:top\">\n" +
				"                                                        <th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">\n" +
				"                                                          <p class=\"text-center\" style=\"Margin:0!important;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0!important;margin-bottom:10px;padding:0;text-align:center\">" + (authorizationResult.getInsurancePayment() != null ? currency + " " + authorizationResult.getInsurancePayment().getInsuranceAmount() : "") + "</p>\n" +
				"                                                        </th>\n" +
				"                                                      </tr>\n" +
				"                                                    </table>\n" +
				"                                                  </th>\n" +
				"                                                </tr>\n" +
				"                                              </tbody>\n" +
				"                                            </table>\n" +
				"                                          </td>\n" +
				"                                        </tr>\n" +
				"                                      </table>";
		return stru;
	}

	private void setNewPassengerSeat(List<ComfortSeat> comfortSeats, CheckinSessionContext ctx, SlingHttpServletRequest request) {
		for (PnrRender pnrRender : ctx.pnrSelectedListDataRender) {
			for (FlightsRender flightsRender : pnrRender.getFlightsRender()) {
				if (!comfortSeats.isEmpty()) {
					for (ComfortSeat comfortSeat : comfortSeats) {
						for (SegmentRender segmentRender : flightsRender.getSegments()) {
							if (segmentRender.getFlight().equals(comfortSeat.getFlightNumber())) {
								for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passenger : segmentRender.getPassengers()) {
									if (passenger.getNome().equals(comfortSeat.getNome()) && passenger.getCognome().equals(comfortSeat.getCognome())) {
										passenger.setNumeroPostoAssegnato(comfortSeat.getSeatNumber());
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private void SetEmdAfterPaymnt(CheckinEndPaymentResponse result, CheckinSessionContext ctx) {

		for (com.alitalia.aem.common.data.checkinrest.model.endpayment.response.AzPaxPayedTP azPaxPayedTP : result.getEndPaymentResponse().getaZPayedTPResponse().getAzPaxPayedTP()){
			for(com.alitalia.aem.common.data.checkinrest.model.endpayment.response.AzAncillaryPayedTP azAncillaryPayedTP : azPaxPayedTP.getAzAncillaryPayedTP()){
				for(AzSegmentPayedTP azSegmentPayedTP : azAncillaryPayedTP.getAzSegmentPayedTP()){
					for(Pnr pnr : ctx.pnrListData){
						if (pnr.getNumber().equals(ctx.pnrSelectedListDataRender.get(0).getNumber())){
							for(Flight flight : pnr.getFlights()){
								for(Segment segment : flight.getSegments()){
									if(segment.getFlight().equals(azSegmentPayedTP.getFlightNumber())){
										for(Passenger passenger : segment.getPassengers() ){
											if (passenger.getNome().toUpperCase().equals(azPaxPayedTP.getFirstName().toUpperCase()) && passenger.getCognome().toUpperCase().equals(azPaxPayedTP.getLastName().toUpperCase())){
												Emd emd = new Emd();
												emd.setNumber(azSegmentPayedTP.getEmdNumber());
												emd.setGroup(setGroupEmd(azAncillaryPayedTP.getGroup()));
												emd.setRficCode(azAncillaryPayedTP.getRficCode());
												emd.setRficSubcode(azAncillaryPayedTP.getRficSubcode());
												emd.setPdcSeat(ctx.comfortSeat==null ? "":ctx.comfortSeat);
												emd.setCommercialName(azAncillaryPayedTP.getCommercialName());
												if (passenger.getEmd() == null){
													passenger.setEmd(new ArrayList<>());
												}
												passenger.getEmd().add(emd);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private String setGroupEmd(String group) {
		String EmdGroup = "";

		switch (group){
			case "Baggage":
				EmdGroup = "BG";
				break;
			case "Lounge":
				EmdGroup = "LG";
				break;
			case "COMFORT SEAT":
				EmdGroup = "SA";
				break;
			case "FAST TRACK":
				EmdGroup = "TS";
				break;
		}

		return EmdGroup;
	}

}