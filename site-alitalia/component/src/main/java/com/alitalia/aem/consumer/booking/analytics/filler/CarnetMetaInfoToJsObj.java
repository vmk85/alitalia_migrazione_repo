package com.alitalia.aem.consumer.booking.analytics.filler;

import com.alitalia.aem.consumer.booking.analytics.MetaInfo;

public class CarnetMetaInfoToJsObj extends ToJsObj implements MetaInfoToJsObj{
	

	public CarnetMetaInfoToJsObj() {
		initFormats();
	}
	
	private void initFormats() {
	}
	
	
	@Override
	public String toJSObj(MetaInfo info, boolean trailingSemicolon) {
		String js = "{";
//		js = addJsObjElem(js, "BbxSessionId", info.bbxSessionId, false);
		js += (trailingSemicolon) ? "};" : "}";
		return js;
	}
	
	

}
