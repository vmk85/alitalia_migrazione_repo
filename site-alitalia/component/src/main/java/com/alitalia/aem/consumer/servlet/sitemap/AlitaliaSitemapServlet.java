package com.alitalia.aem.consumer.servlet.sitemap;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.osgi.PropertiesUtil;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;
import com.day.cq.wcm.api.PageManager;

/**
 * Servlet that dinamically creates sitemap.xml for a localized site or part of it,
 * and that generates a sitemap index if invoked on the global root path.
 * 
 * <p>Each generator must be configured with a configuration entry, explicitly
 * specifying the site map root path(s) (tipically the home page). Note that 
 * these paths must be specified as relative to site root, in order to avoid
 * having to specify an entry for each localized site.</p>
 * 
 * <p>When one of the site map root paths are hit for a localized site with
 * the .sitemap.xml selector and extension, then a sitemap is produced based on 
 * actual content, also generating hreflang links for pages available in other
 * languages / countries at the same path.<p>
 * 
 * <p>When the root content path is hit with the same selector and extension,
 * a sitemap index is generated with an entry for each site map root path
 * specified, for each found market / language found.</p>
 */
@SuppressWarnings("serial")
@Component(metatype = true, label = "Alitalia - Site Map Servlet", description = "Site Map Servlet", configurationFactory = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.resourceTypes", unbounded = PropertyUnbounded.ARRAY, label = "Sling Resource Type", description = "Sling resource type for the home page component(s)."),
		@Property(name = "sling.servlet.selectors", value = "sitemap", propertyPrivate = true),
		@Property(name = "sling.servlet.extensions", value = "xml", propertyPrivate = true),
		@Property(name = "sling.servlet.methods", value = "GET", propertyPrivate = true),
		@Property(name = AlitaliaSitemapServlet.PROP_SITEMAP_ROOT_PATHS, value = {}, unbounded = PropertyUnbounded.ARRAY, label = "Site map root paths", description = "The site maps root paths, relative to localized site root (e.g. 'home-page')."),
		@Property(name = AlitaliaSitemapServlet.PROP_SITEMAP_EXCLUDED_PATHS, value = {}, unbounded = PropertyUnbounded.ARRAY, label = "Site map excluded paths", description = "The path excluded from site maps (e.g. '/alitalia-config')."),
		@Property(name = AlitaliaSitemapServlet.PROP_INHERIT_HIDE_FLAG, boolValue = AlitaliaSitemapServlet.PROP_INHERIT_HIDE_FLAG_DEFAULT, label = "Inherit Hide Flag", description = "Inherit hide in sitemap flag from parent.")
})
public class AlitaliaSitemapServlet extends SlingSafeMethodsServlet {
	
	public static final String PROP_SITEMAP_ROOT_PATHS = "sitemap.root.paths";
	public static final String PROP_INHERIT_HIDE_FLAG = "inherit.hide.flag";
	public static final boolean PROP_INHERIT_HIDE_FLAG_DEFAULT = false;
	public static final String PROP_SITEMAP_EXCLUDED_PATHS = "sitemap.excluded.paths";
	
	private static final String DEFAULT_CHANGE_FREQUENCY = "daily";
	private static final String DEFAULT_PRIORITY = "0.5";
	private static final String XML_DOC_VERSION = "1.0";
	private static final String NAMESPACE = "http://www.sitemaps.org/schemas/sitemap/0.9";
	private static final String URLSET_TAG = "urlset";
	private static final String URL_TAG = "url";
	private static final String LOCATION_TAG = "loc";
	private static final String LAST_MODIFIED_DATE_TAG = "lastmod";
	private static final String CHANGE_FREQUENCY_TAG = "changefreq";
	private static final String PRIORITY_TAG = "priority";
	
	private static final String SITEMAPINDEX_TAG = "sitemapindex";
	private static final String SITEMAP_TAG = "sitemap";
	
	private static final String XHTML_NAMESPACE = "http://www.w3.org/1999/xhtml";
	private static final String LINK_TAG = "link";
	private static final String REL_ATTRIBUTE = "rel";
	private static final String REL_ATTRIBUTE_VALUE = "alternate";
	private static final String HREF_LANG_ATTRIBUTE = "hreflang";
	private static final String HREF_ATTRIBUTE = "href";
	
	private static final String PAGE_PROP_HIDE_IN_SITE_MAP = "hideInSiteMap";
	private static final String SITE_PROP_CHANGE_FREQUENCY = "siteMapChangefrequency";
	private static final String SITE_PROP_CHANGE_PRIORITY = "siteMapPriority";
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
	private static final SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mmz");
	
	private String[] allowedPaths;
	private String[] excludedPaths;
	private boolean inheritHideFlag;
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Activate
	protected void activate(Map<String, Object> properties) {
		readProperties(properties);
	}
	
	@Modified
	protected void modified(Map<String, Object> properties) {
		readProperties(properties);
	}
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		if (AlitaliaConstants.CONTENT_PATH.equals(request.getResource().getPath())) {
			// We are on the global root content
			if (allowedPaths != null) {
				// Handle request as a sitemap index
				doGetSitemapIndex(request, response);
				return;
			}
		} else {
			// We are not on the global root, check that path is allowed by configuration for sitemap
			if (allowedPaths != null) {
				String requestedPath = AlitaliaUtils.getRepositoryPathSiteRelative(request.getResource());
				for (String allowedPath : allowedPaths) {
					if (requestedPath.equals(allowedPath)) {
						// Handle request as a plain sitemap
						doGetSitemap(request, response);
						return;
					}
				}
			}
		}
		// Otherwise..
		response.sendError(404);
		return;
	}
	
	/**
	 * Perform a sitemap index request.
	 */
	private void doGetSitemapIndex(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		
		// Check that path is allowed (only root path is enabled)
		if (!AlitaliaConstants.CONTENT_PATH.equals(request.getResource().getPath())) {
			response.sendError(404);
			return;
		}
		
		response.setContentType(request.getResponseContentType());
		final ResourceResolver resourceResolver = request.getResourceResolver();
		XMLOutputFactory outputFactory = XMLOutputFactory.newFactory();
		
		try {
			XMLStreamWriter stream = outputFactory.createXMLStreamWriter(response.getWriter());
			stream.writeStartDocument(XML_DOC_VERSION);
			
			stream.writeStartElement("", SITEMAPINDEX_TAG, NAMESPACE);
			stream.writeNamespace("", NAMESPACE);
			
			// enumerate all language/countries
			String[] selectors = new String[] { "sitemap" };
			List<String> countryCodes = AlitaliaUtils.getListMarkets(resourceResolver);
			for (String countryCode : countryCodes) {
				List<String> languageCodes = AlitaliaUtils.getListMarketLanguages(resourceResolver, countryCode);
				for (String languageCode : languageCodes) {
					String siteBasePath = AlitaliaUtils.findSiteBaseRepositoryPathByCodes(
							AlitaliaConstants.LOCAL_SITE_PREFIX, countryCode, languageCode);
					String protocol = configuration.getHttpsEnabled() ? "https" : "http";
					String domain = configuration.getExternalDomain();
					// enumerate all sitemap paths
					for (String sitemapPath : allowedPaths) {
						String siteMapPath = siteBasePath + sitemapPath;
						Resource siteMapResource = resourceResolver.getResource(siteMapPath);
						if (siteMapResource != null) {
							// per la sitemap index non effettuo la mappatura, in quanto la regexp di mapping
							// non gestisce bene la trasformazione dei percorsi della language page o superiore
							String loc = AlitaliaUtils.findSiteResourceAbsoluteExternalUrl(
									request, siteMapResource, selectors, "xml", protocol, domain, false);
							stream.writeStartElement(NAMESPACE, SITEMAP_TAG);
							stream.writeStartElement(NAMESPACE, LOCATION_TAG);
							stream.writeCharacters(loc);
							stream.writeEndElement();
							stream.writeEndElement();
						}
					}
				}
			}
			
			stream.writeEndElement();
			stream.writeEndDocument();
			
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}
	
	/**
	 * Perform a standard sitemap request.
	 */
	private void doGetSitemap(SlingHttpServletRequest request, SlingHttpServletResponse response) 
		throws ServletException, IOException {
		
		response.setContentType(request.getResponseContentType());
		
		final ResourceResolver resourceResolver = request.getResourceResolver();
		final PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
		final Page page = pageManager.getContainingPage(request.getResource());
		
			XMLOutputFactory outputFactory = XMLOutputFactory.newFactory();
			
			try {
				XMLStreamWriter stream = outputFactory.createXMLStreamWriter(response.getWriter());
				stream.writeStartDocument(XML_DOC_VERSION);
				
				stream.writeStartElement("", URLSET_TAG, NAMESPACE);
				stream.writeNamespace("", NAMESPACE);
				stream.writeNamespace("xhtml", XHTML_NAMESPACE);
				
				if (inheritHideFlag) {
					// Parse the tree and skip all the sub-trees where the parent has the hide in sitemap flag to true.
					parseTree(page, stream, resourceResolver, request);
				} else {
					// Display all the pages, just skip the ones that have the hide in sitemap flag to true.
					displayTree(page, stream, resourceResolver, request);
				}
				
				stream.writeEndElement();
				
				stream.writeEndDocument();
				
			} catch (XMLStreamException e) {
				throw new IOException(e);
			}
	}
	
	/**
	 * Writes the XML document to the output stream.
	 *
	 * @param page The current page.
	 * @param stream The output stream for the XML document.
	 * @param resourceResolver The ResourceResolver.
	 * @param request The SlingHttpServletRequest.
	 * @throws XMLStreamException
	 */
	private void processPage(Page page, XMLStreamWriter stream, 
			ResourceResolver resourceResolver, SlingHttpServletRequest request) throws XMLStreamException {
		
		for (String excludedPath : excludedPaths) {
			if (!AlitaliaUtils.getRepositoryPathSiteRelative(page).startsWith(excludedPath)) {
				stream.writeStartElement(NAMESPACE, URL_TAG);
				
				String protocol = configuration.getHttpsEnabled() ? "https" : "http";
				String domain = configuration.getExternalDomain();
				
				// Mandatory field with the page URL.
				stream.writeStartElement(NAMESPACE, LOCATION_TAG);
				final String loc = AlitaliaUtils.findSiteResourceAbsoluteExternalUrl(
						request, page.adaptTo(Resource.class), protocol, domain, true);
				stream.writeCharacters(loc);
				stream.writeEndElement();
				
				// Optional field with the last modified date value.
				if(page.getLastModified() != null){
					stream.writeStartElement(NAMESPACE, LAST_MODIFIED_DATE_TAG);
					final String lastModifiedDate = dateFormat.format(page.getLastModified().getTime());
					stream.writeCharacters(lastModifiedDate);
					stream.writeEndElement();
				}
				
				// Optional field with the change frequency - by default this is set to "daily".
				stream.writeStartElement(NAMESPACE, CHANGE_FREQUENCY_TAG);
				final String changeFrequency = page.getProperties().get(SITE_PROP_CHANGE_FREQUENCY, DEFAULT_CHANGE_FREQUENCY);
				stream.writeCharacters(changeFrequency);
				stream.writeEndElement();
				
				// Optional field with the priority - by default this is set to "0.5".
				stream.writeStartElement(NAMESPACE, PRIORITY_TAG);
				final String priority = page.getProperties().get(SITE_PROP_CHANGE_PRIORITY, DEFAULT_PRIORITY);
				stream.writeCharacters(priority);
				stream.writeEndElement();
				
				// verify if a page with the same relative path exists for other countries/languages
				String pageRelativePath = AlitaliaUtils.getRepositoryPathSiteRelative(page);
				Resource pageResource = page.adaptTo(Resource.class);
				String currentCountryCode = AlitaliaUtils.getRepositoryPathMarket(pageResource);
				String currentLanguageCode = AlitaliaUtils.getRepositoryPathLanguage(pageResource);
				List<String> countryCodes = AlitaliaUtils.getListMarkets(resourceResolver);
				for (String countryCode : countryCodes) {
					List<String> languageCodes = AlitaliaUtils.getListMarketLanguages(resourceResolver, countryCode);
					for (String languageCode : languageCodes) {
						if (!countryCode.equals(currentCountryCode) || (!languageCode.equals(currentLanguageCode))) {
							String siteBasePath = AlitaliaUtils.findSiteBaseRepositoryPathByCodes(
									AlitaliaConstants.LOCAL_SITE_PREFIX, countryCode, languageCode);
							String candidatePagePath = siteBasePath + pageRelativePath;
							Resource hrefLangResource = resourceResolver.getResource(candidatePagePath);
							if (hrefLangResource != null) {
								// existing alternative page found, generate xhtml:link
								String hrefLang = languageCode + "-" + countryCode;
								String href = AlitaliaUtils.findSiteResourceAbsoluteExternalUrl(
										request, hrefLangResource, protocol, domain, true);
								stream.writeStartElement(XHTML_NAMESPACE, LINK_TAG);
								stream.writeAttribute(REL_ATTRIBUTE, REL_ATTRIBUTE_VALUE);
								stream.writeAttribute(HREF_LANG_ATTRIBUTE, hrefLang);
								stream.writeAttribute(HREF_ATTRIBUTE, href);
								stream.writeEndElement();
							}
						}
					}
				}
				
				stream.writeEndElement();
			}
		}
	}
	
	/**
	 * Parses the website tree and builds the sitemap.xml file.
	 *
	 * @param page The current page.
	 * @param stream The output stream for the XML document.
	 * @param resourceResolver The ResourceResolver.
	 * @para, request The SlingHttpServletRequest.
	 * @throws XMLStreamException
	 */
	private void parseTree(Page page, XMLStreamWriter stream, 
			ResourceResolver resourceResolver, SlingHttpServletRequest request)
			throws XMLStreamException {
		
		// Get the hide in map flag
		final boolean hidePageInMap = page.getProperties().get(PAGE_PROP_HIDE_IN_SITE_MAP, false);
		if (!hidePageInMap) {
			// Add the current page to the map and parse its children.
			processPage(page, stream, resourceResolver, request);
			Iterator<Page> children = page.listChildren(new PageFilter(), false);
			while (children.hasNext()) {
				final Page child = children.next();
				
				// Recursively parse the sub-tree.
				parseTree(child, stream, resourceResolver, request);
			}
		}
	}
	
	/**
	 * Display the whole tree in sitempa.xml (just exclude the pages with
	 * "hide in sitemap" to true.
	 *
	 * @param page The current page.
	 * @param stream The output stream for the XML document.
	 * @param resourceResolver The ResourceResolver.
	 * @param request The SlingHttpServletRequest.
	 * @throws XMLStreamException
	 */
	private void displayTree(Page page, XMLStreamWriter stream, 
			ResourceResolver resourceResolver, SlingHttpServletRequest request)
			throws XMLStreamException {
		for (Iterator<Page> children = page.listChildren(new PageFilter(), true); children.hasNext();) {
			final Page child = children.next();
			final boolean hidePageInMap = child.getProperties().get(PAGE_PROP_HIDE_IN_SITE_MAP, false);
			if (!hidePageInMap) {
				processPage(child, stream, resourceResolver, request);
			}
		}
	}
	
	/**
	 * Update the component instance reading the specified configuration properties object. 
	 * 
	 * @param properties The configuration properties.
	 */
	private void readProperties(Map<String, Object> properties) {
		this.allowedPaths = PropertiesUtil.toStringArray(
				properties.get(PROP_SITEMAP_ROOT_PATHS), new String[0]);
		this.inheritHideFlag = PropertiesUtil.toBoolean(
				properties.get(PROP_INHERIT_HIDE_FLAG), PROP_INHERIT_HIDE_FLAG_DEFAULT);
		this.excludedPaths = PropertiesUtil.toStringArray(
				properties.get(PROP_SITEMAP_EXCLUDED_PATHS), new String[0]);
	}
	
}
