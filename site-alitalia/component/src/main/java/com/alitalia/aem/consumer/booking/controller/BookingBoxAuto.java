package com.alitalia.aem.consumer.booking.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.CrossSellingBaseData;
import com.alitalia.aem.common.data.home.CrossSellingData;
import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.managemybooking.delegate.ManageMyBookingDelegate;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingBoxAuto extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	@Inject
	private ManageMyBookingDelegate manageMyBookingDelegate;

	private List<CrossSellingData> boxAuto;
	

	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			initBaseModel(request);
			
			CrossSellingsRequest crossSellingsRequest = new CrossSellingsRequest();
			crossSellingsRequest.setTid(IDFactory.getTid());
			crossSellingsRequest.setSid(ctx.sid);
			crossSellingsRequest.setCookie(ctx.cookie);
			crossSellingsRequest.setExecution(ctx.execution);
			crossSellingsRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
			crossSellingsRequest.setIpAddress(ctx.clientIP);
			crossSellingsRequest.setLanguageCode(AlitaliaUtils.getLanguage(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag())));
			crossSellingsRequest.setMarketCode(ctx.market);
			crossSellingsRequest.setPrenotation(ctx.prenotation);
			
			CrossSellingsResponse crossSellingsResponse = 
					manageMyBookingDelegate.getCrossSellingsByPrenotation(crossSellingsRequest);
			if (crossSellingsResponse != null &&
			             crossSellingsResponse.getCrossSellingsData() != null &&
			             crossSellingsResponse.getCrossSellingsData().getItems() != null &&
			             !crossSellingsResponse.getCrossSellingsData().getItems().isEmpty()) {
				
				boxAuto = new ArrayList<CrossSellingData>();
				for (CrossSellingBaseData item : crossSellingsResponse.getCrossSellingsData().getItems()) {
					if (item instanceof CrossSellingData) {
						CrossSellingData auto = (CrossSellingData) item;
						boxAuto.add(auto);
					}
				}
				
			}
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
		}
	}


	public List<CrossSellingData> getBoxAuto() {
		return boxAuto;
	}
	
	
}