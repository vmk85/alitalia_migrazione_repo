package com.alitalia.aem.consumer.mmb.render;

import com.alitalia.aem.common.data.home.mmb.MmbTaxData;

public class MmbTaxDataRender {
	
	private static final String NORMAL = "Normal";
	private static final String CHILD = "Child";
	private static final String INFANT = "Infant";
	private static final String TAX = "Tax";
	private static final String FUEL = "Fuel";
	
	private MmbTaxData taxData;
	private MmbPriceRender priceRender;
	
	public MmbTaxDataRender(MmbTaxData taxData) {
		this.taxData = taxData;
	}
	
	public MmbTaxData getTaxData() {
		return taxData;
	}
	
	public MmbPriceRender getPriceRender() {
		return priceRender;
	}

	public void setPriceRender(MmbPriceRender priceRender) {
		this.priceRender = priceRender;
	}
	
	public String getCodeI18nKey() {
		String codeI18nKey = "";
		if (this.taxData.getCode().equals(NORMAL)) {
			codeI18nKey = "mmb.gestisciPrenotazione.adulto.label";
		} 
		if (this.taxData.getCode().equals(CHILD)) {
			codeI18nKey = "mmb.gestisciPrenotazione.bambino.label";
		}
		if (this.taxData.getCode().equals(INFANT)) {
			codeI18nKey = "mmb.gestisciPrenotazione.neonato.label";
		}
		if (this.taxData.getPassengerTypeQuantity() != null) {
			if (Integer.parseInt(this.taxData.getPassengerTypeQuantity()) > 1) {
				return codeI18nKey + ".plural";
			} else {
				return codeI18nKey;
			}
		} 
		
		if (this.taxData.getCode().equals(TAX)) {
			return "mmb.gestisciPrenotazione.tasse.label";
		}
		if (this.taxData.getCode().equals(FUEL)) {
			return "mmb.gestisciPrenotazione.supplementi.label";
		}
		return "";
	}

}
