package com.alitalia.aem.consumer.utils;

import static java.util.stream.Collectors.joining;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import com.alitalia.aem.common.data.home.*;
import com.alitalia.aem.common.data.home.mmb.ProfileComplete;
import com.day.cq.i18n.I18n;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.enumerations.MMTierCodeEnum;
import com.alitalia.aem.common.messages.home.MessagesRequest;
import com.alitalia.aem.common.messages.home.MessagesResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.global.enumeration.LinguaCorrispondenza;
import com.alitalia.aem.consumer.millemiglia.model.messaggimiglia.MessaggiMillemigliaData;
import com.alitalia.aem.consumer.millemiglia.render.EstrattoContoRender;
import com.alitalia.aem.consumer.millemiglia.render.EstrattoContoRenderComparator;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;

// security check - Begin
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
// security check - End

public class AlitaliaUtils {

	protected static Logger logger = LoggerFactory.getLogger(AlitaliaUtils.class);
	
	/* metodi di utilita' a livello di pagina */
	
	/**
	 * Restituisce l'oggetto Page di riferimento per la risorsa
	 * specificata, individuata risalendo ricorsivamente la tree
	 * delle risorse fino alla prima di tipo cq:Page.
	 * 
	 * @param resource La risorsa di riferimento
	 * @return la pagina individuata, o null.
	 */
	static public Page getPage(Resource resource) {
		boolean notFound = true;
		Resource currentResource = resource;
		while (notFound) {
			if (currentResource.getResourceType().equalsIgnoreCase("cq:Page")) {
				Page page = currentResource.adaptTo(Page.class);
				return page;
			} else {
				currentResource = currentResource.getParent();
				notFound = !(currentResource == null);
			}
		}
		return null;
	}
	
	/**
	 * Restituisce il valore di una propriet&agrave; di un componente incluso in una pagina
	 * @param ancestor la pagina che contiene il componente
	 * @param compName il nome del componente ("path" specificato 
	 *        in sightly con "data-sly-resource")
	 * @param propName il nome della propriet&agrave; del componente
	 * @return la propriet&agrave; oppure null nel caso la propriet&agrave; o il componente non viene trovata
	 */
	public static String getComponentProperty(Page ancestor, String compName, String propName){
		Resource pageResource = ancestor.getContentResource();	
		String prop = null;
		try{
			prop = pageResource.getChild(compName).getValueMap().get(propName, String.class);
		}
		catch(Exception e){
			logger.error("AliataliaUtils.java - getComponentProperty: <<prop = pageResource.getChild(compName).getValueMap().get(propName, String.class);>>");
		}
		return prop;
	}
	
	/**
	 * Restituisce un oggetto Locale di una risorsa,
	 * determinando la page corrispondente e
	 * usando le Page API.
	 * 
	 * @param resource La risorsa di riferimento.
	 * @return il locale di pagina, o null.
	 */
	static public Locale getPageLocale(Resource resource) {
		Page page = getPage(resource);
		if (page != null) {
			return page.getLanguage(false);
		}
		return null;
	}
	
	/**
	 * Restituisce un oggetto Locale di una pagina,
	 * usando le Page API.
	 * 
	 * @param page La pagina di riferimento.
	 * @return il locale di pagina, o null.
	 */
	static public Locale getPageLocale(Page page) {
		if (page != null) {
			return page.getLanguage(false);
		}
		return null;
	}
	
	/**
	 * Individua il language di una risorsa, determinando la
	 * page corrispondente e usando le Page API.
	 * 
	 * @param resource La risorsa di riferimento.
	 * @return il language individuato, o null.
	 */
	static public String getPageLocaleLanguage(Resource resource) {
		Locale pageLocale = getPageLocale(resource);
		if (pageLocale != null) {
			return getLanguage(pageLocale.toLanguageTag());
		}
		return null;
	}


	static public ProfileComplete getProfileComplete(MACustomer maCustomer, I18n i18n) {

		try {
			int step = 0;
			String text = "";
			String URL = "";
			int errorCode = 0;
			Boolean skip = false;
			ProfileComplete response = new ProfileComplete();
				if(response.checkDatiPersonaliMA(maCustomer)>0){
					step += response.checkDatiPersonaliMA(maCustomer);
					if(response.checkDatiPersonaliMA(maCustomer)<20){
						text = i18n.get("myalitalia.info.completaDatiPersonali");
						URL = "./myalitalia-dati-personali.html?page=personalData";
						skip = true;
					}
				}else{
					text = i18n.get("myalitalia.info.completaDatiPersonali");
					URL = "./myalitalia-dati-personali.html?page=personalData";
					skip = true;
				}

				if(response.checkDatiViaggioMA(maCustomer)>0){
					step += response.checkDatiViaggioMA(maCustomer);
					if(response.checkDatiViaggioMA(maCustomer)<20){
						if(!skip){
							text = i18n.get("myalitalia.info.completaDatiViaggio");
							URL = "./myalitalia-dati-di-viaggio.html?page=dataTravel";
							skip = true;
						}
					}
				}else{
					if(!skip){
						text = i18n.get("myalitalia.info.completaDatiViaggio");
						URL = "./myalitalia-dati-di-viaggio.html?page=dataTravel";
						skip = true;
					}
				}

				if(response.checkPreferenzeViaggioMA(maCustomer)>0){
					step += response.checkPreferenzeViaggioMA(maCustomer);
					if(response.checkPreferenzeViaggioMA(maCustomer)<30){
						if(!skip){
							text = i18n.get("myalitalia.info.completaPreferenza");
							URL = "./myalitalia-millemiglia-preferenze-viaggio.html?page=preferences";
							skip = true;
						}
					}
				}else{
					if(!skip){
						text = i18n.get("myalitalia.info.completaPreferenza");
						URL = "./myalitalia-millemiglia-preferenze-viaggio.html?page=preferences";
						skip = true;
					}
				}

				if(response.checkDatiPagamentoFatturazione(maCustomer)>0){
					step += response.checkDatiPagamentoFatturazione(maCustomer);
				}else{
					if(!skip){
						text = "";
						URL = "#";
						//text = i18n.get("myalitalia.info.completaDatiPagamento");
						//URL = "./myalitalia-dati-di-pagamento.html";
					}
				}

				//DA TOGLIERE UNA VOLTA IMPLEMENTATA LA PARTE DEI DATI DI PAGAMENTO E FATTURAZIONE
				if(step == 70){
					step = 100;
				}

				if(step==100){
					text = i18n.get("myalitalia.info.allCompleted");
					URL = null;
				}


//                if(response.checkDatiPersonaliMA(maCustomer)) {
//                    step += 20;
//                    if(response.checkDatiViaggioMA(maCustomer)) {
//                        step += 20;
//                        if(response.checkDatiPagamentoFatturazione(maCustomer)) {
//                            step += 20;
//                            if (response.checkPreferenzeViaggioMA(maCustomer)) {
//                                step += 20;
//                                if(response.checkMMConnection(maCustomer)) {
//                                    step += 20;
//                                    text = i18n.get("myalitalia.info.allCompleted");
//                                    URL = null;
//                                } else {
//                                    text = i18n.get("myalitalia.info.iscrizioneMillemiglia");
//                                    URL = "#";
//                                }
//                            } else {
//                                text = i18n.get("myalitalia.info.completaPreferenza");
//                                URL = "./myalitalia-millemiglia-preferenze-viaggio.html?page=preferences";
//                            }
//                        } else {
//                            text = i18n.get("myalitalia.info.completaDatiPagamento");
//                            URL = "./myalitalia-dati-di-pagamento.html";
//                        }
//                    } else {
//                        text = i18n.get("myalitalia.info.completaDatiViaggio");
//                        URL = "./myalitalia-dati-di-viaggio.html?page=dataTravel";
//                    }
//                } else {
//                    text = i18n.get("myalitalia.info.completaDatiPersonali");
//                    URL = "./myalitalia-dati-personali.html?page=personalData";
//                }
				response.setErrorCode(errorCode);
				response.setText(text);
				response.setURL(URL);
				response.setCompleted(step);
				return response;

		} catch (Exception e) {
			return new ProfileComplete("#", "-", 0, 1, "Eccezione", e.getStackTrace().toString());
		}
	}

	/**
	 * Individua il language di una pagina, usando le Page API.
	 * 
	 * @param page La pagina di riferimento.
	 * @return il language individato, o null.
	 */
	static public String getPageLocaleLanguage(Page page) {
		Locale pageLocale = getPageLocale(page);
		if (pageLocale != null) {
			return getLanguage(pageLocale.toLanguageTag());
		}
		return null;
	}
	
	/**
	 * Individua il country di una risorsa, determinando la
	 * page corrispondente e usando le Page API.
	 * 
	 * @param resource La risorsa di riferimento.
	 * @return il country individuato, o null.
	 */
	static public String getPageLocaleCountry(Resource resource) {
		Locale pageLocale = getPageLocale(resource);
		if (pageLocale != null) {
			return pageLocale.getCountry();
		}
		return null;
	}
	
	/**
	 * Individua il country di una pagina, usando le Page API.
	 * 
	 * @param page La pagina di riferimento.
	 * @return il country individato, o null.
	 */
	static public String getPageLocaleCountry(Page page) {
		Locale pageLocale = getPageLocale(page);
		if (pageLocale != null) {
			return pageLocale.getCountry();
		}
		return null;
	}
	
	
	/* metodi di utilita' a livello di repository path */
	
	/**
	 * Restituisce il path per il repository JCR della root di un sito localizzato,
	 * in base a prefisso, country e lingua specificati.
	 * 
	 * @param resource La risorsa di riferimento da utilizzare.
	 * @return il path individuato.
	 */
	static public String findSiteBaseRepositoryPathByCodes(String prefix, String countryCode, String languageCode) {
		return AlitaliaConstants.CONTENT_PATH + "/" + prefix + countryCode + "/" + languageCode;
	}
	
	/**
	 * Restituisce il path per il repository JCR della root di un sito localizzato,
	 * determinando country e lingua in base alla risorsa di riferimento specificata.
	 * 
	 * @param resource La risorsa di riferimento da utilizzare.
	 * @return il path individuato.
	 */
	static public String findSiteBaseRepositoryPath(Resource resource) {
		return findSiteBaseRepositoryPathByCodes(getRepositoryPathPrefix(resource),
				getRepositoryPathMarket(resource), getRepositoryPathLanguage(resource));
	}
	
	/**
	 * Restituisce, in base alla risorsa specificata, il prefisso
	 * applicato al country code (i.e. "master" o "alitalia").
	 * 
	 * @param resource La risorsa di riferimento.
	 * @return il prefisso individuato.
	 */
	static public String getRepositoryPathPrefix(Resource resource) {
		String s = resource.getPath().split("\\/")[AlitaliaConstants.REPOSITORY_PATH_COUNTRY_DEPTH];
		return s.substring(0, s.lastIndexOf("-") + 1);
	}
	
	/**
	 * Restituisce il codice country del mercato a partire dal
	 * repository path della risorsa specificata.
	 * 
	 * @param resource la risorsa di riferimento.
	 * @return il codice country individuato.
	 */
	static public String getRepositoryPathMarket(Resource resource) {
		String s = resource.getPath().split("\\/")[AlitaliaConstants.REPOSITORY_PATH_COUNTRY_DEPTH];
		return s.substring(s.lastIndexOf("-") + 1);
	}
	
	/**
	 * Restituisce il codice language del sito a partire dal
	 * repository path della risorsa specificata.
	 * 
	 * @param resource la risorsa di riferimento.
	 * @return il codice language individuato.
	 */
	static public String getRepositoryPathLanguage(Resource resource) {
		String s = resource.getPath().split("\\/")[AlitaliaConstants.REPOSITORY_PATH_LANGUAGE_DEPTH];
		return s;
	}
	
	/**
	 * Restituisce un oggetto Locale costruito in base al country e 
	 * alla language determinata da repository path della risorsa 
	 * specificata.
	 * 
	 * @param resource la risorsa di riferimento.
	 * @return il Locale costruito.
	 */
	static public Locale getRepositoryPathLocale(Resource resource) {
		Locale locale = new Locale(getRepositoryPathLanguage(resource), getRepositoryPathMarket(resource));
		return locale;
	}
	
	/**
	 * Restituisce il path di un risorsa relativo rispetto alla site root.
	 * 
	 * @param resource la risorsa.
	 * @return il path risultante.
	 */
	public static String getRepositoryPathSiteRelative(Resource resource) {
		String siteRelativePath = "";
		Page page = getPage(resource);
		if (page != null) {
			siteRelativePath = getRepositoryPathSiteRelative(page);
		}
		return siteRelativePath;
	}
	
	/**
	 * Restituisce il path di una pagina relativo rispetto alla root del
	 * sito localizzato.
	 * 
	 * @param page la pagina.
	 * @return il path risultante, o stringa vuota.
	 */
	public static String getRepositoryPathSiteRelative(Page page) {
		String relativePath = "";
		String pagePath = page.getPath();
		String rootPath = page.getAbsoluteParent(AlitaliaConstants.SITE_HOME_LEVEL).getPath();
		if (pagePath.startsWith(rootPath)) {
			relativePath = pagePath.substring(rootPath.length());
		}
		return relativePath;
	}
	
	
	/* metodi di utilita' a livello di URL esterne */
	
	/**
	 * Restituisce la URL relativa esterna risolta per la root di un
	 * sito localizzato, determinando country e language in base alla risorsa specificata.
	 * 
	 * @param resource La risorsa di riferimento.
	 * @param initialSlash Indica se includere lo slash finale alla fine dell'URL.
	 * @returns l'URL esterna di base.
	 */
	static public String findSiteBaseExternalUrl(Resource resource, boolean trailingSlash) {
		String url = "";
		Page page = getPage(resource);
		if (page != null && 
				null != page.getAbsoluteParent(AlitaliaConstants.SITE_HOME_LEVEL)) {
			logger.trace("findSiteBaseExternalUrl - getAbsoluteParent: " + page.getAbsoluteParent(AlitaliaConstants.SITE_HOME_LEVEL));
			url = page.getAbsoluteParent(AlitaliaConstants.SITE_HOME_LEVEL).getPath();
			logger.trace("findSiteBaseExternalUrl - URL: " + url);
		} else{
			logger.trace("findSiteBaseExternalUrl - Page null");
		}
		if (trailingSlash) {
			url += "/";
		}
		return url;
	}
	
	/**
	 * Restituisce il percorso, risolto e mappato, di base per una country e un language
	 * determinate in base alla risorsa specificata.
	 * 
	 * @param resource La risorsa di riferimento.
	 * @param initialSlash Indica se includere lo slash finale alla fine dell'URL.
	 * @returns l'URL esterna di base.
	 */
	static public String findSiteExternalUrlWithDomain(Resource resource, boolean trailingSlash) {
		String url = "";
		Page page = getPage(resource);
		if (page != null) {
			url = page.getAbsoluteParent(AlitaliaConstants.SITE_HOME_LEVEL).getPath();
		}
		if (trailingSlash) {
			url += "/";
		}
		return url;
	}
	
	/**
	 * Restituisce per una risorsa l'URL esterno, assoluto e (opzionalmente) mappato con il ResourceResolver,
	 * senza applicare selettori, usando l'estensione predefinita (html) e applicando il protocollo
	 * e il dominio esterno in base ai parametri di configurazione.
	 *
	 * @param request La richiesta, usata solo per recuperare il resource resolver per la mappatura.
	 * @param resource La risorsa di cui costruire l'URL.
	 * @param protocol Il protocollo da applicare (obbligatorio).
	 * @param domain Il dominio da applicare, inclusa la porta se necessario (obbligatorio).
	 * @param map Flag per effettua o meno la mappatura della URL. 
	 * @return L'URL, ad es. <code>http://www.alitalia.com/en_us/path/to/page.html</code>.
	 */
	static public String findSiteResourceAbsoluteExternalUrl(SlingHttpServletRequest request, Resource resource, 
			String protocol, String domain, boolean map) {
		return findSiteResourceAbsoluteExternalUrl(request, resource, null, "html", protocol, domain, map);
	}
	
	/**
	 * Restituisce per una risorsa l'URL esterno, assoluto e (opzionalmente) mappato con il ResourceResolver.
	 *
	 * @param request La richiesta, usata solo per recuperare il resource resolver per la mappatura.
	 * @param resource La risorsa di cui costruire l'URL.
	 * @param selectors I selettori da applicare, o null.
	 * @param extension L'estensione da applicare, o null.
	 * @param protocol Il protocollo da applicare (obbligatorio).
	 * @param domain Il dominio da applicare, inclusa la porta se necessario (obbligatorio).
	 * @param map Flag per effettua o meno la mappatura della URL. 
	 * @return L'URL, ad es. <code>http://www.alitalia.com/en_us/path/to/page.selector.selector.extension</code>.
	 */
	static public String findSiteResourceAbsoluteExternalUrl(
			SlingHttpServletRequest request, Resource resource,
			String[] selectors, String extension, String protocol, String domain, boolean map) {
		String targetUrl = findSiteBaseExternalUrl(resource, false) + getRepositoryPathSiteRelative(resource);
		if (selectors != null) {
			for (String selector : selectors) {
				targetUrl += "." + selector;
			}
		}
		if (extension != null) {
			targetUrl += "." + extension;
		}
		if (map) {
			targetUrl = request.getResourceResolver().map(targetUrl);
		}
		targetUrl = protocol + "://" + domain + targetUrl;
		return targetUrl;
	}
	
	/**
	 * Restituisce la URL relativa esterna, risolta e mappata, per il path
	 * specificato (in forma relativa rispetto al sito localizzato),
	 * determinando country e language in base alla risorsa specificata.
	 * 
	 * @param siteBaseRelativePath Il path della risorsa, relativo rispetto allla root dei siti localizzati.
	 * @param resource Una risorsa da utilizzare per determinare country e language.
	 * @return L'URL esterna risolta e mappata.
	 */
	static public String findExternalRelativeUrlByPath(String siteBaseRelativePath, Resource resource) {
		String targetUrl = findSiteBaseExternalUrl(resource, false) + siteBaseRelativePath;
		String resolvedTargetUrl = resource.getResourceResolver().map(targetUrl);	
		return resolvedTargetUrl;
	}
	/**
	 * Restituisce per una risorsa l'URL esterno e mappato con Resource Resolver
	 * 
	 * @param request la richiesta da cui ricavare il resource resolver
	 * @param resource la risorsa di cui costruire l'URL
	 * @return String l'URL
	 */
	static public String findSiteResourceExternalUrl(
			SlingHttpServletRequest request, Resource resource) {
		
		return request.getResourceResolver().map(
				findSiteBaseExternalUrl(resource, false) + 
				getRepositoryPathSiteRelative(resource));
	}
	
	
	/* metodi di utilita' relativi ai mercati e alle lingue */
	
	/**
	 * Usando il ResourceResolver fornito, individua la risorsa
	 * corrispondente al nodo root dei mercati e restituisce la 
	 * relativa risorsa.
	 * 
	 * @param resolver il ResourceResolver da usare per la risoluzione del path.
	 * @param jcrContent indica se individuare il nodo jcr:content o il nodo stesso.
	 * @return la risorsa risolta, o null.
	 */
	static public Resource getMarketsRootResource(ResourceResolver resolver, boolean jcrContent) {
		Resource resource = null;
		try {
			String path = AlitaliaConstants.CONTENT_PATH;
			if (jcrContent) {
				path += "/jcr:content";
			}
			resource = resolver.getResource(path);
		} catch (Exception e) {
			logger.debug("Error resolving the market root node", e);
		}
		return resource;
	}
	
	/**
	 * Usando il ResourceResolver fornito, individua la risorsa
	 * corrispondente al mercato per il country code specificato
	 * e restituisce la relativa risorsa.
	 * 
	 * @param resolver il ResourceResolver da usare per la risoluzione del path.
	 * @param countryCode il codice country da usare per la costruzione del path.
	 * @param jcrContent indica se individuare il nodo jcr:content o il nodo market stesso.
	 * @return la risorsa risolta, o null.
	 */
	static public Resource getMarketResource(ResourceResolver resolver, String countryCode, boolean jcrContent) {
		Resource resource = null;
		try {
			String path = AlitaliaConstants.CONTENT_PATH + "/" + 
					AlitaliaConstants.LOCAL_SITE_PREFIX + countryCode.toLowerCase();
			if (jcrContent) {
				path += "/jcr:content";
			}
			resource = resolver.getResource(path);
		} catch (Exception e) {
			logger.debug("Error resolving the market node", e);
		}
		return resource;
	}
	
	/**
	 * Usando il ResourceResolver fornito, individua l'elenco delle risorse
	 * corrispondenti ai mercati, le enumera e restituisce l'elenco dei relativi
	 * country code.
	 * 
	 * @param resolver il ResourceResolver da usare per la risoluzione dei path.
	 * @return la lista dei country code supportati.
	 */
	static public List<String> getListMarkets(ResourceResolver resolver) {
		List<String> countries = new ArrayList<String>();
		try {
			Resource marketsRootResource = AlitaliaUtils.getMarketsRootResource(resolver, false);
			Iterator<Resource> marketsIterator = marketsRootResource.getChildren().iterator();
			while (marketsIterator.hasNext()) {
				Resource market = marketsIterator.next();
				if (market != null && market.getName() != "jcr:content" 
						&& !market.getName().startsWith(AlitaliaConstants.MASTER_SITE_PREFIX)) {
					countries.add(getRepositoryPathMarket(market));
				}
			}
		} catch (Exception e) {
			logger.debug("Error resolving the available markets", e);
		}
		return countries;
	}
	
	/**
	 * Usando il ResourceResolver fornito, individua la risosra
	 * corrispondente al mercato per il country code specificato,
	 * enumera i nodi sottostanti corrispondenti ai siti localizzati
	 * disponibili e restituisce l'elenco dei relativi language code.
	 * 
	 * @param resolver il ResourceResolver da usare per la risoluzione del path.
	 * @param countryCode il codice country da usare per la costruzione del path.
	 * @return la lista dei language code supportati.
	 */
	static public List<String> getListMarketLanguages(ResourceResolver resolver, String countryCode) {
		List<String> languages = new ArrayList<String>();
		try {
			Resource marketResource = AlitaliaUtils.getMarketResource(resolver, countryCode, false);
			Iterator<Resource> marketLanguagesIterator = marketResource.getChildren().iterator();
			while (marketLanguagesIterator.hasNext()) {
				Resource marketLanguage = marketLanguagesIterator.next();
				if (marketLanguage != null && !"jcr:content".equals(marketLanguage.getName())) {
					languages.add(getRepositoryPathLanguage(marketLanguage));
				}
			}
		} catch (Exception e) {
			logger.debug("Error resolving the market available languages", e);
		}
		return languages;
	}
	
	/**
	 * Usando il ResourceResolver fornito, individua il mercato relativo al country code
	 * specificato e determina il language code della lingua di default per il mercato,
	 * opzionalmente applicando anche le preferenze di lingua specificate a livello
	 * di singole citta'.
	 * 
	 * @param resolver il ResourceResolver da usare per la risoluzione dei path.
	 * @param countryCode il country code del mercato.
	 * @param cityCode il city code della citta' specifica, o null.
	 * @return il language code determinato, o null in caso di errori.
	 */
	public static String findResourceDefaultLanguage(ResourceResolver resourceResolver, String countryCode,
			String cityCode) {
		Resource marketContentResource = getMarketResource(resourceResolver, countryCode, true);
		if (marketContentResource == null) {
			logger.info("Cannot find market resource for country code {}", countryCode);
			return null;
		}
		String languageCode = null;
		try {
			ValueMap marketProperties = marketContentResource.getValueMap();
			languageCode = (String) marketProperties.getOrDefault("defaultLanguage", null);
			if (languageCode == null) {
				logger.info("defaultLanguage is null for market {}", countryCode);
				List<String> marketLanguages = getListMarketLanguages(resourceResolver, countryCode);
				if (marketLanguages != null && marketLanguages.size() > 0) {
					languageCode = marketLanguages.get(0);
					logger.info("Language set to the first available found: {}", languageCode);
				} else {
					throw new Exception("No default language set and no available languages found.");
				}
			}
			if (cityCode != null) {
				// process city-specific market default language settings
				Resource marketCitiesLanguage = marketContentResource.getChild("market-cities-language");
				if (marketCitiesLanguage != null) {
					Map<String, Object> defaultCityLanguage = 
							AlitaliaUtils.fromJcrMultifieldToMapArrayObject(marketCitiesLanguage, "items");
					if (defaultCityLanguage != null) {
						for (Map.Entry<String, Object> setting : defaultCityLanguage.entrySet()) {
							String settingCity = (String) ((Map) setting.getValue()).get("city");
							String settingLanguage = (String) ((Map) setting.getValue()).get("language");
							if (cityCode.equalsIgnoreCase(settingCity)) {
								languageCode = settingLanguage;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error finding default language code.", e);
			return null;
		}
		return languageCode;
	}
	
	
	/* metodi di utilita' per i multifield */

	/**
	 * Restituisce l'elenco dei valori di un multifield in forma di mappa di mappe,
	 * in cui la mappa esterna corrisponde al nome del nodo valore (tipicamente un
	 * numero progressivo) e ciasun valore corrisponde a sua volta ad una mappa
	 * con il nome e il valore di ciascuna proprieta'.
	 * 
	 * <p>Gestisce inoltre i multifield innestati decodificando i valori JSON dal repository.</p>
	 * 
	 * @param componentResource la risorsa relativa al nodo del componente  valore del multifield.
	 * @param propertyName il nome della proprieta' corrispondente al multifield.
	 * @return Una mappa con i valori del multifield.
	 * @throws JSONException in caso di errori nella decodifica di multifield innestati.
	 */
	static public Map<String, Object> fromJcrMultifieldToMapArrayObject(Resource componentResource, String propertyName ) 
			throws JSONException {
		
		Resource item = componentResource.getChild(propertyName );
		
		Map<String, Object> multifield = new LinkedHashMap<String, Object>();
		Map<String, Object> field;
		int i = 0;
		
		if(item != null){
			for (Resource multifieldResource : item.getChildren()) {
				ValueMap multifieldMap = multifieldResource.getValueMap();
				String[] multifieldMapKeys = 
						(String[]) multifieldMap.keySet().toArray(new String[multifieldMap.keySet().size()]); 
				String fieldValue = "";
				
				field = new LinkedHashMap<String, Object>();
	
				for (String valueMapKey : multifieldMapKeys) {
					if (!valueMapKey.equals("jcr:primaryType")) {
						fieldValue = multifieldMap.get(valueMapKey).toString();
						
						Map<String, Object> nestedMultifield = null;
						
						if (Pattern.matches("^\\[(\\{(\"[A-z0-9\\-\\.]*\"\\:\".*\"\\,?)*\\}\\,?)*\\]", fieldValue)) {
							//Se è un multifield nested
							JSONArray jsonArray = new JSONArray(fieldValue);
	
							nestedMultifield = new LinkedHashMap<String, Object>();
							Map<String, String> nestedFieldValue;
							
							for (int j = 0; j < jsonArray.length(); j++) {
								JSONObject jsonObject = jsonArray.getJSONObject(j);
								Iterator<String> keysIterator = jsonObject.keys();
								
								nestedFieldValue = new LinkedHashMap<String, String>();
								
								while (keysIterator.hasNext()) {
									String nextNestedKey = keysIterator.next();
									nestedFieldValue.put(nextNestedKey, jsonObject.getString(nextNestedKey));
								}
								
								nestedMultifield.put(Integer.toString(j), nestedFieldValue);
							}
							
							field.put(valueMapKey, nestedMultifield);
							
							
						} else {
							field.put(valueMapKey, fieldValue);
						}
					}
				}
				
				multifield.put(Integer.toString(i++), field);
			}
		}
		
		return multifield;
	}
	
	
	/* metodi di utilita' per i dati di autenticazione utente */
	
	/**
	 * @see AlitaliaCustomerProfileManager#getAuthenticatedUserProfile(SlingHttpServletRequest)
	 */
	static public MMCustomerProfileData getAuthenticatedUser(SlingHttpServletRequest request) {
		return AlitaliaCustomerProfileManager.getAuthenticatedUserProfile(request);
	}
	
	
	/* metodi di utilita' relativi a IP e nomi host */
	
	/**
	 * Restituisce l'IP chiamate relativo ad una richiesta HTTP, tentando di recuperarlo
	 * dagli header specificati in configurazione (in caso di load balancer, dispatcher
	 * o simili) o, in assenza, dall'indirizzo remoto indicato nella request stessa.
	 * 
	 * @param configuration La configurazione da usare per reperire gli header da cercare.
	 * @param request La richiesta da analizzare.
	 * @return L'IP individuato.
	 */
	public static String getRequestRemoteAddr(AlitaliaConfigurationHolder configuration, HttpServletRequest request) {
		String ipAddress = null;
		String[] clientIpHeaders = configuration.getClientIpHeaders();
		if (clientIpHeaders != null) {
			logger.debug("clientIpHeaders diverso da null: " + clientIpHeaders.toString());
			for (String headerName : clientIpHeaders) {
				String headerValue = request.getHeader(headerName);
				if (headerValue != null && headerValue.length() > 0) {
					logger.debug("Found client ip in header {}: {}", headerName, headerValue);
					logger.debug("headerValue: " + headerValue);
					if(headerValue.contains(",")){
						String[] ipValues = headerValue.split(",");
						if(ipValues.length > 1){
							headerValue = ipValues[ipValues.length-2];
							logger.debug("More than one ip: " + headerValue);
						} else{
							headerValue = ipValues[0];
							logger.debug("Only one ip: " + headerValue);
						}
					}
					ipAddress = headerValue.trim();
					break;
				}
			}
		}
		
		if (ipAddress == null) {
			logger.debug("Client ip header not found, fallback to request remote address: {}", 
					request.getRemoteAddr());
			ipAddress = request.getRemoteAddr();
		}
		return ipAddress;
	}
	
	/**
	 * Tenta di determinare il nome dell'host locale.
	 * 
	 * @param defaultValue Il valore di default da usare come fallback.
	 * @return Il nome individuato, o il valore di defaultValue.
	 */
	public static String getLocalHostName(String defaultValue) {
		String hostname = null;
		hostname = System.getenv("HOSTNAME"); // linux systems
		if (hostname == null || "".equals(hostname)) {
			hostname = System.getenv("COMPUTERNAME"); // windows systems
		}
		if (hostname == null || "".equals(hostname)) {
			try {
				hostname = InetAddress.getLocalHost().getHostName();
			} catch (Exception e) {
				logger.trace("Could not be able to determine local hostname from InetAddress", e);
			}
		}
		if (hostname == null || "".equals(hostname)) {
			hostname = defaultValue;
		}
		return hostname;
	}
	
	
	/* altri metodi di utilita' */
	
	/**
	 * Restituisce un oggetto Locale in base alla risorsa, tentando di risolvere
	 * il locale di pagina usando le Page API e, se non disponibile, costruendolo
	 * in base al repository path della risorsa.
	 * 
	 * @param resource la risora di riferimento.
	 * @return il Locale individuato.
	 */
	static public Locale findResourceLocale(Resource resource) {
		Locale locale = AlitaliaUtils.getPageLocale(resource);
		if (locale == null) {
			locale = AlitaliaUtils.getRepositoryPathLocale(resource);
		}
		return locale;
	}
	
	/**
	 * Restituisce il valore di una proprieta' di pagina ereditata.
	 * 
	 * @param resource Riferimento alla risorsa da cui risalire alla pagine.
 	 * @param propertyName Nome della proprieta'.
 	 * @param type Il tipo restituito desiderato.
	 * @return Il valore trovato per la proprieta'.
	 */
	public static <T> T getInheritedProperty(Resource resource, String propertyName, Class<T> type) {
		Page page = getPage(resource);
		if (page != null) {
			InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(page.getContentResource());
			return ivm.getInherited(propertyName, type);
		} else {
			return null;
		}
	}
	
	/**
	 * Create a string joining the values of the provider array.
	 * 
	 * @param source The source array.
	 * @param delimiter The delimeter to apply.
	 * @return a String with the joint values.
	 */
	public static String joinArray(Object[] source, String delimiter) {
		if (source == null || source.length == 0) { 
			return "";
		}
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < source.length; i++) {
			if (i > 0) {
				builder.append(delimiter);
			}
			builder.append(source[i]);
		}
	    return builder.toString();
	}
	
	/**
	 * Log the complete list of specified request headers.
	 * 
	 * @param request The request.
	 */
	static public void logRequestHeaders(SlingHttpServletRequest request) {
		if (request != null) {
			Enumeration headerNames = request.getHeaderNames();
			while (headerNames.hasMoreElements()) {
				String headerName = (String) headerNames.nextElement();
				Enumeration headerValues = request.getHeaders(headerName);
				while (headerValues.hasMoreElements()) {
					String headerValue = (String) headerValues.nextElement();
					logger.trace("logRequestHeaders - {}: {}", headerName, headerValue);
				}
			}
		} else {
			logger.trace("logRequestHeaders - Request is null.");
		}
	}
	
	
	/* metodi di utilita' relativi alla gestione dei messaggi millemiglia */
	
	/**
	 * Ottiene una lista di messaggi utente per un utente.
	 * 
	 * @param user il profilo dell'utente.
	 * @param consumerDataDelegate il delegate da usare per la chiamata.
	 * @return L'elenco di messaggi risultante.
	 */
	static public List<MessaggiMillemigliaData> getListaMessaggi(
			MMCustomerProfileData user, ConsumerLoginDelegate consumerDataDelegate) {
		logger.debug("Entro getListaMessaggi");
		
		List<MessaggiMillemigliaData> listMessages = null;
		
		// Create getMessagesRequest
		MessagesRequest messRequest = new MessagesRequest();
		messRequest.setCustomerProfile(user);
		logger.debug("Request pronta: " + messRequest.toString());
		MessagesResponse messResponse = 
				consumerDataDelegate.getMessages(messRequest);

		if (messResponse != null) {
			MMCustomerProfileData userResponse = messResponse.getCustomerProfile();
			logger.debug("Response pronta: " + messResponse.toString());
			if (userResponse != null) {
				logger.debug("userResponse: " + userResponse.toString());
				ArrayList<MMMessageData> mexList = 
						(ArrayList<MMMessageData>) userResponse.getMessages();
				if (mexList != null && mexList.size() > 0) {
					logger.debug("mexList: " + mexList.toString());
					listMessages = createMessageList(mexList);
				}
			}
		}

		return listMessages;
	}

	/**
	 * Crea una lista ordinata di MessaggiMillemigliaData a partire dalla lista di MMMessageData fornita.
	 * 
	 * @param messages la lista di MMMessageData.
	 * @return la lista di MessaggiMillemigliaData risultante.
	 */
	static public List<MessaggiMillemigliaData> createMessageList(List<MMMessageData> messages) {
		
		return messages.stream().map(MMmsgData -> new MessaggiMillemigliaData(MMmsgData))
				.sorted(new Comparator<MessaggiMillemigliaData>() {
			
			@Override
			public int compare(MessaggiMillemigliaData a, MessaggiMillemigliaData b) {
				
				Calendar aDate = Calendar.getInstance();
				Calendar bDate = Calendar.getInstance();
				try {
					
					aDate.setTime(
							new SimpleDateFormat(AlitaliaConstants.MESSAGES_DATE_FORMAT)
						.parse(a.getEndValidatDate()));
					bDate.setTime(
							new SimpleDateFormat(AlitaliaConstants.MESSAGES_DATE_FORMAT)
						.parse(b.getEndValidatDate()));
					
				} catch (ParseException e) {
					throw new RuntimeException();
				}
				
				return aDate.compareTo(bDate);
				
			}
		}).collect(Collectors.toList());
	}
	
	/**
	 * Converte una lista di attivita' in una lista di oggetti render corrispondenti.
	 * 
	 * @param activitiesToConvert la lista di attivita'.
	 * @return la lista di render risultante.
	 */
	static public List<EstrattoContoRender> convertActivities(List<MMActivityData> activitiesToConvert) {
		return activitiesToConvert.stream().map(
				actToConv -> new EstrattoContoRender(actToConv))
				.sorted(EstrattoContoRenderComparator.getInstance(false))
				.collect(Collectors.toList());
	}
	
	/**
	 * Restituisce il valore in input arrotondato con il numero di cifre decimali 
	 * acquisito dalla properties contenuta nei template Market e Language.
	 * 
	 * Inoltre, formatta il valore con i separatori dei decimali e migliaia specificati 
	 * dall'utente nei precedenti suddetti template.
	 * 
	 * Nel caso in cui il countrycode è Korea restituisce il valore senza decimali
	 *   
	 * @param value
	 * @param res risorsa da cui prendere country code, numero di cifre decimali e separatori
	 * @return
	 */
	static public String round(double value, Resource res) {

		InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(res);
		
		String decimalSeparator=ivm.getInherited("decimalSeparator", String.class);
		String groupingSeparator=ivm.getInherited("groupingSeparator", String.class);
		String countryCode= getRepositoryPathMarket(res);
		Integer numDecDigits=ivm.getInherited("decimalDigit", Integer.class);
		if(numDecDigits == null){
			numDecDigits = 2;
		}
		DecimalFormat form = (DecimalFormat) getNumberFormat(numDecDigits,decimalSeparator,groupingSeparator,countryCode);
		
		return (form.format(value).replaceFirst("[.,]0+$", ""));
	}

    static public String round_new(double value, Resource res) {
        String countryCode= getRepositoryPathMarket(res);
        InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(res);

        String decimalSeparator =  new HierarchyNodeInheritanceValueMap(res.getParent().getParent()).getInherited("decimalSeparator", String.class);
        String groupingSeparator = new HierarchyNodeInheritanceValueMap(res.getParent().getParent()).getInherited("groupingSeparator", String.class);
        //Integer numDecDigits  =    new HierarchyNodeInheritanceValueMap(res.getParent().getParent()).getInherited("decimalDigit", Integer.class);
        Integer numDecDigits  =    0;


        /*if(countryCode == "kr"){
            groupingSeparator=" ";
        }*/

        /*if(numDecDigits == null){
            numDecDigits = 0;
        }*/

        DecimalFormat form = (DecimalFormat) getNumberFormat(numDecDigits,decimalSeparator,groupingSeparator,countryCode);

        return (form.format(value));

	}

	/**
	 * 
	 * 
	 * @param numDecDigits 
	 * @param decimalSeparator 
	 * @param groupingSeparator 
	 * @param countryCode 
	 * @return
	 */
	public static NumberFormat getNumberFormat(int numDecDigits, String decimalSeparator, 
			String groupingSeparator, String countryCode) {
		
		String decDigitsFormat = Stream.generate(() -> "0").limit(numDecDigits).collect(joining());
		DecimalFormat form = (DecimalFormat) NumberFormat.getInstance();
		DecimalFormatSymbols custom=new DecimalFormatSymbols();
		if (decimalSeparator != null && !decimalSeparator.isEmpty()) { 
			custom.setDecimalSeparator(decimalSeparator.charAt(0));
		}
		if (groupingSeparator != null  && !decimalSeparator.isEmpty()) { 
			custom.setGroupingSeparator(groupingSeparator.charAt(0));
		}
		
		form.setDecimalFormatSymbols(custom);
		
		String formatString = ((DecimalFormat) NumberFormat
				.getIntegerInstance()).toPattern() + "." + decDigitsFormat;	
		if (numDecDigits == 0 || countryCode.equalsIgnoreCase("kr") || countryCode.equalsIgnoreCase("ru")) {
			formatString = ((DecimalFormat) NumberFormat
					.getIntegerInstance()).toPattern();
		}
		form.applyPattern(formatString);
		return form;
	} 
	
	public static String setThousandsSeparator(Long value){
		return setThousandsSeparatorLocalized(value, Locale.ITALY);
	}
	
	public static String setThousandsSeparatorLocalized(Long value, Locale l){
		String computedValue = "0";
		if (null != value) {
			NumberFormat formatter = NumberFormat.getInstance(l);
			computedValue = formatter.format(value);
		}
		return computedValue;
	}
	
	/*
	 * Return null se non  è possibile recuperare il body della mail
	 */
	@Deprecated
	public static String getMailBody(Resource resource, String templateMail, boolean secure){
        
        //Create an HTTPClient object
		DefaultHttpClient httpClient = new DefaultHttpClient();
		String localDomain = "127.0.0.1:11111";
		String protocol = "http://";
        if(secure){
        	protocol = "https://";
        }
        //TODO: use configuration osgi
		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(resource, false);
        HttpGet httpGet =  new HttpGet(protocol + localDomain + baseUrl + AlitaliaConstants.BASE_CONFIG_PATH + "/" + templateMail);
        logger.debug("httpGet: " + httpGet.toString());
        HttpResponse httpResponse = null;
        try {
               httpResponse = httpClient.execute(httpGet);
               logger.debug("httpResp: " + httpResponse.toString());
               
        } catch (Exception e2) {
               // TODO Auto-generated catch block
               logger.error("errore execute httpGet ", e2);
        }
        
        InputStream streamIn = null;
        InputStreamReader streamInReader = null;
        BufferedReader bufReader = null;
        StringBuilder response = null;
        try {
               streamIn = httpResponse.getEntity().getContent();
               streamInReader = new InputStreamReader(streamIn);
               bufReader = new BufferedReader(streamInReader);
               
               String line;
               response = new StringBuilder();
              
               while ((line = bufReader.readLine()) != null) {
                   response.append(line);
               }
        } catch (Exception e1) {
               // TODO Auto-generated catch block
               logger.error("errore getContent ", e1);
        } finally{
        	try {
        		if(streamInReader != null){
        			streamInReader.close();
        		}
				if(bufReader != null){
					bufReader.close();
				}
				if(streamIn !=  null){
					streamIn.close();
				}
			} catch (IOException e) {
				logger.error("Exception close stream: ", e );
			}
        }
        String responseMail = null;
        if(response != null){
        	logger.debug("EMAIL: " + response.toString());
        	responseMail = response.toString();
        }
        
        return responseMail; 
  }
	
	public static MMTierCodeEnum getNextTier(MMTierCodeEnum mmTier){
		if (MMTierCodeEnum.BASIC.value().equals(mmTier.value())) {
			return MMTierCodeEnum.ULISSE;
		} else if (MMTierCodeEnum.ULISSE.value().equals(mmTier.value())) {
			return MMTierCodeEnum.FRECCIA_ALATA;
		} else if (MMTierCodeEnum.FRECCIA_ALATA.value().equals(mmTier.value())) {
			return MMTierCodeEnum.PLUS;
		} else {
			return null;
		}
	}
	/*
	 * This method returns  a corrispondent number for each type of MMTier.
	 * The range starts from 1 to 4.
	 * The order utilized is the same of tiers. 
	 */
	public static Integer integerValuemmTierCode(MMTierCodeEnum tier){
		if (MMTierCodeEnum.BASIC.equals(tier)) {
			return 1;
		} else if (MMTierCodeEnum.ULISSE.equals(tier)) {
			return 2;
		} else if (MMTierCodeEnum.FRECCIA_ALATA.equals(tier)) {
			return 3;
		} else if (MMTierCodeEnum.PLUS.equals(tier)) {
				return 4;
		}
		return 1;
		
	}
	
	/**
	 * This method return the spicific date format according to the current market.
	 * If the market is US or CA it returns the english format "MM/dd/YY", 
	 * otherwise it return null and the caller use the standard format dd/MM/YY.
	 * @param resource
	 * @return
	 */
	public static String getDateFormatByMarket(Resource resource){
		String market = getRepositoryPathMarket(resource);
		if (market.equalsIgnoreCase("us") || market.equalsIgnoreCase("ca")) {
			return  "MM/dd/yyyy";
		} 
		return null;
	}
	
	public static String getCodeLanguagefromLinguaCorrispondenza(LinguaCorrispondenza linguaCorrispondenza){
		String langCode;
		
		switch (linguaCorrispondenza) {
        	case ITALIANO:   
        		langCode= "IT";
        		break;
        	case INGLESE:  langCode = "EN";
                break;
        	case GIAPPONESE:  langCode = "JA";
                 break;
        	case FRANCESE:  langCode = "FR";
                 break;
        	case SPAGNOLO:  langCode = "SP";
                 break;
        	case PORTOGHESE:  langCode = "PO";
                 break;
        	default: langCode = "IT";
                 break;
    
		}
		return langCode;
	}
	
	public static String replacePlaceholderBodyMail(String body, String[] placeholders, String[] values) {
		if (placeholders == null || values == null) {
			return body;
		}
		body = StringUtils.replaceEach(body, placeholders, values);
		return body;
	}
	
	public static String getLanguage(String languageTag) {
		String[] elements = languageTag.split("-");
		String language = elements[0];
		if ("und".equals(language)) {
			logger.error("[GET LANGUAGE] language not found");
			return "";
		}
		else {
			return language;
		}
	}
	
	/**
	 * Search for a brand in a list, accounting to rewriting of brand code. 
	 * If brandCode == brandCodeTarget, than a second search is performed with brandCodeSource
	 * Example: EconomyBasic --> EconomyLight. 
	 * If you search EconomyLight in list and cannot find it, it will search for EconomyBasic
	 * @param list
	 * @param brandCode
	 * @param brandCodeSource source brandCode for mapping
	 * @param brandCodeTarget target brandCode for mapping. 
	 * @return BrandData if found, null otherwise
	 */
	public static BrandData getBrandInfo(List<BrandData> list, String brandCode, String brandCodeSource, String brandCodeTarget){
		BrandData info = null;
		if(list != null && list.size() > 0){
			for (BrandData brand : list) {
				if (brand.getCode().equals(brandCode)) {
					info = brand;
				}
			}
			if(info == null && brandCode.equals(brandCodeTarget)){
				/*Piano B cerca EconomyBasic*/
				for (BrandData brand : list) {
					if (brand.getCode().equals(brandCodeSource)) {
						info = brand;
					}
				}
			}
		}
		return info;
	}
	
	/**
	 * Some market require an extraFee for the price of SpecialOffer. If the marketCode is one of these market 
	 * return the value configured for extraFee, otherwhise return 0. 
	 * @param alitaliaConfigurationHolder
	 * @param marketCode the current market
	 * @return extraFee if the marketCode require an extraFee, 0 otherwhise
	 */
	public static double getExtraFee (AlitaliaConfigurationHolder alitaliaConfigurationHolder, String marketCode, boolean isSliceSardiniaTerritorialContinuity, boolean isSliceSicilyTerritorialContinuity ) {
		double extraFee = 0;
		for (String marketFee : alitaliaConfigurationHolder.getFeeSpecialOffersMarket()) {
			if(!isSliceSardiniaTerritorialContinuity 
					&& !isSliceSicilyTerritorialContinuity
						&& marketFee.equalsIgnoreCase(marketCode)){
				extraFee = Double.parseDouble(alitaliaConfigurationHolder.getFeeSpecialOffers());
				break;
			}
		}
		return extraFee;
	}
	
	public static String trimIfNotNull(String str){
		return str != null ? str.trim() : null;
	}
	
	public static JSONObject invokeHttpPostClient(String hostname, String formRequst) 
			throws  JSONException, IOException {

		HttpClient httpClient = new DefaultHttpClient();
		JSONObject jsonResponse = null;
        try {
            HttpPost request = new HttpPost(hostname);

            if(formRequst != null){
    			ArrayList<BasicNameValuePair> postParameters = new ArrayList<BasicNameValuePair>();
    		    for(String s : formRequst.split("&")){
    		    	String[] param = s.split("=");
    		    	if(param.length > 0)
    		    		postParameters.add(new BasicNameValuePair(URLEncoder.encode(param[0],"UTF-8"), URLEncoder.encode(param[1],"UTF-8")));
    		    }
    		    request.setEntity(new UrlEncodedFormEntity(postParameters));
    		}
            
            request.addHeader("Content-Type", "application/x-www-form-urlencoded");
            
            logger.debug("Executing request: {}", EntityUtils.toString(request.getEntity()));
            
            HttpResponse response = httpClient.execute(request);
            
            if (response != null && response.getStatusLine().getStatusCode() == 200) {
            	String resoponseString = EntityUtils.toString(response.getEntity());
            	jsonResponse = new JSONObject(resoponseString);
            	logger.debug("Recaptcha response: {}", resoponseString);
            }
        } catch (IOException e) {
        	logger.error("Error invoking client");
        	httpClient.getConnectionManager().shutdown();
        	throw e;
        }
        httpClient.getConnectionManager().shutdown();
		return jsonResponse;
    }
	/**
	 * Some market require the Time Format AM/PM  If the marketCode is one of these market
	 * return the time value reformatted, otherwhise return _origTime.
	 * @param _origTime  the original Time Format
	 * @param _marketCode the current market
	 * @return newTime if the marketCode is known , otherwise left
	 */
	public static String getTimeForMarket(String _marketCode, String _origTime){
		String newTime = null;
		final int PM_OFSET_TIME = 12;
		if (_origTime != null) {
			// Al momento non ho altro modo di controllare se la conversione e' gia' stata effettuata
			if (_origTime.contains("AM") || _origTime.contains("PM")){
				newTime = _origTime;
			}
			else {
				if (_marketCode.toLowerCase().equals("us") || _marketCode.toLowerCase().equals("ca")) {
					String[] timeSplit = _origTime.split(":");

					if (Integer.parseInt(timeSplit[0]) == 0) {
						newTime = Math.abs(Integer.parseInt(timeSplit[0]) - PM_OFSET_TIME) + ":" + timeSplit[1] + "AM";
					} else if ((Integer.parseInt(timeSplit[0]) > 0) && (Integer.parseInt(timeSplit[0]) < 12)) {
						newTime = timeSplit[0] + ":" + timeSplit[1] + "AM";
					} else if (Integer.parseInt(timeSplit[0]) == PM_OFSET_TIME) {
						newTime = Integer.toString(PM_OFSET_TIME) + ":" + timeSplit[1] + "PM";
					} else if ((Integer.parseInt(timeSplit[0]) > 12) && (Integer.parseInt(timeSplit[0]) <= 23)) {
						newTime = Integer.toString(Integer.parseInt(timeSplit[0]) - PM_OFSET_TIME) + ":" + timeSplit[1] + "PM";
					} else { /* Non dovrei mai entrare in questo ramo ....*/
						//newTime = _origTime;
					}
				}
				else {
					newTime = _origTime;
				}
			}
		}
		return newTime;
	}

	/**
	 * Utility Special Characters Replacement
	 * return the input string with replacements if required
	 * @param input  the original input String
	 * @return input string with replacement if needed
	 */
	public static String replaceUnwantedCharacters(String input){
		return input.replaceAll("à", "a").replaceAll("á", "a")
				.replaceAll("À", "A").replaceAll("Á", "A")
				.replaceAll("è", "e").replaceAll("é", "e")
				.replaceAll("È", "E").replaceAll("É", "E")
				.replaceAll("ì", "i").replaceAll("í", "i")
				.replaceAll("Ì", "I").replaceAll("Í", "I")
				.replaceAll("ò", "o").replaceAll("ó", "o")
				.replaceAll("Ò", "O").replaceAll("Ó", "O")
				.replaceAll("ù", "u").replaceAll("ú", "u")
				.replaceAll("Ù", "U").replaceAll("Ú", "U")
//  Aggiunti altri caratteri per il Bug "CaratteriSeciali"
//  ÁáÀàÂâÃãÄäĄąĆćÇçÈèÉéÊêËëĘęÌìÍíÎîÏïJ́j́ŁłŃńÑñŐőÔôÒòÓóÔôÖöÕõŚśÚúŰűÙùÛûÜüŸÿÝýŹźŻż
				.replaceAll("Á", "A").replaceAll("á", "a")
				.replaceAll("À", "A").replaceAll("à", "a")
				.replaceAll("Â", "A").replaceAll("â", "a")
				.replaceAll("Ã", "A").replaceAll("ã", "a")
				.replaceAll("Ä", "A").replaceAll("ä", "a")
				.replaceAll("Ą", "A").replaceAll("ą", "a")
				.replaceAll("Ć", "C").replaceAll("ć", "c")
				.replaceAll("Ç", "C").replaceAll("ç", "c")
				.replaceAll("È", "E").replaceAll("è", "e")
				.replaceAll("É", "E").replaceAll("é", "e")
				.replaceAll("Ê", "E").replaceAll("ê", "e")
				.replaceAll("Ë", "E").replaceAll("ë", "e")
				.replaceAll("Ę", "E").replaceAll("ę", "e")
				.replaceAll("Ì", "I").replaceAll("ì", "i")
				.replaceAll("Í", "I").replaceAll("í", "i")
				.replaceAll("Î", "I").replaceAll("î", "i")
				.replaceAll("Ï", "I").replaceAll("ï", "i")
				.replaceAll("J́", "J").replaceAll("j́", "j")
				.replaceAll("Ł", "L").replaceAll("ł", "l")
				.replaceAll("Ń", "N").replaceAll("ń", "n")
				.replaceAll("Ñ", "N").replaceAll("ñ", "n")
				.replaceAll("Ő", "O").replaceAll("ő", "o")
				.replaceAll("Ô", "O").replaceAll("ô", "o")
				.replaceAll("Ò", "O").replaceAll("ò", "o")
				.replaceAll("Ó", "O").replaceAll("ó", "o")
				.replaceAll("Ô", "O").replaceAll("ô", "o")
				.replaceAll("Ö", "O").replaceAll("ö", "o")
				.replaceAll("Õ", "O").replaceAll("õ", "o")
				.replaceAll("Ś", "S").replaceAll("ś", "s")
				.replaceAll("Ú", "U").replaceAll("ú", "u")
				.replaceAll("Ű", "U").replaceAll("ű", "u")
				.replaceAll("Ù", "U").replaceAll("ù", "u")
				.replaceAll("Û", "U").replaceAll("û", "u")
				.replaceAll("Ü", "U").replaceAll("ü", "u")
				.replaceAll("Ÿ", "Y").replaceAll("ÿ", "y")
				.replaceAll("Ý", "Y").replaceAll("ý", "y")
				.replaceAll("Ź", "Z").replaceAll("ź", "z")
				.replaceAll("Ż", "Z").replaceAll("ż", "z");

	}


	// security check - Begin
	static Cipher ecipher;
	static Cipher dcipher;
	// 8-byte Salt
	static byte[] salt = {
			(byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
			(byte) 0x56, (byte) 0x35, (byte) 0xE3, (byte) 0x03
	};
	// Iteration count
	static int iterationCount = 19;

	public static String getSecretKey() {
		return secretKey;
	}

	private static String secretKey="Secret12345Secret12345";
	/* Esempio
	 *	String enc=this.encrypt(secretKey, "TestoInChiaroDaCriptare");
	 */
	// security check - End

	/**
	 *
	 * @param secretKey Key used to encrypt data
	 * @param plainText Text input to be encrypted
	 * @return Returns encrypted text
	 * @throws java.security.NoSuchAlgorithmException
	 * @throws java.security.spec.InvalidKeySpecException
	 * @throws javax.crypto.NoSuchPaddingException
	 * @throws java.security.InvalidKeyException
	 * @throws java.security.InvalidAlgorithmParameterException
	 * @throws java.io.UnsupportedEncodingException
	 * @throws javax.crypto.IllegalBlockSizeException
	 * @throws javax.crypto.BadPaddingException
	 *
	 */
	public static String encrypt(String secretKey, String plainText)
			throws NoSuchAlgorithmException,
			InvalidKeySpecException,
			NoSuchPaddingException,
			InvalidKeyException,
			InvalidAlgorithmParameterException,
			UnsupportedEncodingException,
			IllegalBlockSizeException,
			BadPaddingException {


		//Key generation for enc and desc
		KeySpec keySpec = new PBEKeySpec(secretKey.toCharArray(), salt, iterationCount);
		SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
		// Prepare the parameter to the ciphers
		AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

		//Enc process
		ecipher = Cipher.getInstance(key.getAlgorithm());
		ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
		String charSet = "UTF-8";
		byte[] in = plainText.getBytes(charSet);
		byte[] out = ecipher.doFinal(in);
		String encStr = new String(Base64.getEncoder().encode(out));
		return encStr;
	}

	/**
	 * @param secretKey Key used to decrypt data
	 * @param encryptedText encrypted text input to decrypt
	 * @return Returns plain text after decryption
	 * @throws java.security.NoSuchAlgorithmException
	 * @throws java.security.spec.InvalidKeySpecException
	 * @throws javax.crypto.NoSuchPaddingException
	 * @throws java.security.InvalidKeyException
	 * @throws java.security.InvalidAlgorithmParameterException
	 * @throws java.io.UnsupportedEncodingException
	 * @throws javax.crypto.IllegalBlockSizeException
	 * @throws javax.crypto.BadPaddingException
	 */
	public static String decrypt(String secretKey, String encryptedText)
			throws NoSuchAlgorithmException,
			InvalidKeySpecException,
			NoSuchPaddingException,
			InvalidKeyException,
			InvalidAlgorithmParameterException,
			UnsupportedEncodingException,
			IllegalBlockSizeException,
			BadPaddingException,
			IOException {

		//Key generation for enc and desc
		KeySpec keySpec = new PBEKeySpec(secretKey.toCharArray(), salt, iterationCount);
		SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
		// Prepare the parameter to the ciphers
		AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);
		//Decryption process; same key will be used for decr
		dcipher = Cipher.getInstance(key.getAlgorithm());
		dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
		byte[] enc = Base64.getDecoder().decode(encryptedText);
		byte[] utf8 = dcipher.doFinal(enc);
		String charSet = "UTF-8";
		String plainStr = new String(utf8, charSet);
		return plainStr;
	}

	public static String generateLockAccountUrl(AlitaliaConfigurationHolder configuration,
												SlingHttpServletRequest request,
												String IdProfilo
												){

		String baseUrl = request.getResource().getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
		String lockPage = baseUrl + configuration.getMMSALockAccountPage();

		String parametersToCrypt="";

		String dataScadenza="";

		parametersToCrypt = IdProfilo + ";" + dataScadenza;

		String parametersCrypted="";
		try {
			parametersCrypted=encrypt(getSecretKey(),parametersToCrypt);
		}catch (Exception ex){
			logger.error("AlitaliaUtils.generateLockAccountUrl - impossibile criptare",ex);
		}


		String lockAccountUrl=lockPage+"?"+parametersCrypted;

		return  lockAccountUrl;
	}

}

