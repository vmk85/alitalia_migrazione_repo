package com.alitalia.aem.consumer.carnet;

public class CarnetConstants {

	public static final String CARNET_CONTEXT_ATTRIBUTE = "alitaliaConsumerCarnetSessionContext";
	public static final String CARNET_SID = "alitaliaConsumerCarnet";
	
	public static final short NUM_CARNET_6 = 6;
	public static final short NUM_CARNET_12 = 12;
	
	public static final String MESSAGE_GENERIC_EMPTY_FIELD = "carnet.common.genericEmptyField.label";
	public static final String MESSAGE_GENERIC_INVALID_FIELD = "carnet.common.genericInvalidField.label";
	public static final String MESSAGE_GENERIC_CHECK_FIELD = "carnet.common.genericUncheckedField.label";
	public static final String MESSAGE_SPECIFIC_EMAIL_FIELD = "carnet.specific.email.notValid";
	public static final String MESSAGE_SPECIFIC_CF_FIELD = "carnet.specific.cf.notValid";
	public static final String MESSAGE_INVALID_LOGIN = "carnet.invalidLogin.label";
	public static final String[] PLACEHOLDER_RICEVUTA_ACQUISTO_CARNET = {"#{R_CODCARNET}#", "#{R_NAMEANDSURNAME}#", "#{R_PHONE}#", 
		"#{R_PASSWD}#", "#{R_NUMVOLICARNET}#", "#{R_PREZZO}#", "#{R_TIPOCC}#", "#{R_CEILEDNUMCC}#", 
		"#{R_PREZZOINT}#", "#{R_PREZZODEC}#", "#{R_CCICONPATH}#", "#{R_COD_SEQ}#", "#{R_LINKEMETTIBIGLIETTI}#"};	
	public static final String MESSAGE_CARNET_NOT_FOUND_ERROR_SERVICE = "carnet.service.error.carnetnotfound";
	public static final String MESSAGE_GENERIC_ERROR_SERVICE = "carnet.service.error.generic";
	
}
