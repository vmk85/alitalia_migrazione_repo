package com.alitalia.aem.consumer.millemiglia.model.richiedimiglia;

public class RichiediMigliaMSG {
	
	private String text;
	private boolean success;
	
	public RichiediMigliaMSG(String text, boolean success) {
		this.text = text;
		this.success = success;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	@Override
	public String toString() {
		return "RichiediMigliaMSG [text=" + text + ", success=" + success + "]";
	}

}
