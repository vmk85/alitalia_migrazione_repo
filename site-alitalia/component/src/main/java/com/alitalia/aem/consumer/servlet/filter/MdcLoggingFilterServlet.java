package com.alitalia.aem.consumer.servlet.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingFilter;
import org.apache.felix.scr.annotations.sling.SlingFilterScope;
import org.apache.http.HttpStatus;

import javax.servlet.http.Cookie;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.alitalia.aem.common.component.behaviour.exc.BehaviourException;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

import org.apache.sling.api.SlingException;
import org.apache.sling.api.resource.ResourceNotFoundException;
import org.apache.sling.api.scripting.ScriptEvaluationException;

@SlingFilter(scope = SlingFilterScope.REQUEST, order = 100001)

public class MdcLoggingFilterServlet implements javax.servlet.Filter{
	
	/**
	 * Class configuration.
	 */
	@Reference
    private AlitaliaConfigurationHolder configuration;
	
	/**
	 * Class logger.
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
	private boolean insertIpInMdc( HttpServletRequest request ) {
		
		try {
			Boolean clientIPenabled = configuration.getMdcClientIpEnabled();
			/*Inserimento dell'IP del client nei log attraverso Mapped Diagnostic Context */	
			if (clientIPenabled) {
				
				String clientIP = (String)request.getSession().getAttribute("clientIP");
	
				if (clientIP == null) {
					
					clientIP = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
					logger.trace("Optimization: putting clientIP in session");
					request.getSession().setAttribute("clientIP",clientIP);
				}
				
				MDC.put("clientIP", "[" + clientIP + "]" );
				return true;
			}
			
			return false;
			
		} catch ( UnsupportedOperationException e ) {

			logger.debug( "MDC Filter: Request doesn't allow operation on session in method insertIpInMdc");
			return false;
			
		} catch (ResourceNotFoundException e) {

			logger.debug( "MDC Filter: Resource not found in method insertIpInMdc");
			throw e;
		} catch (Exception e) {

			logger.warn( "MDC Filter: Error while inizialing MDC in method insertIpInMdc", e );
			return false;
		}
	}
	
	private boolean insertJsessionInMdc(HttpServletRequest request) {
		Boolean jsessionidEnabled = configuration.getMdcJsessionIdEnabled();
		try{
			/*Inserimento dell'ID di sessione nei log attraverso Mapped Diagnostic Context */
			if (jsessionidEnabled && request.getSession() != null) {
				
				String jsessionId = request.getSession().getId();			
				MDC.put("jsessionId", "[" + jsessionId + "]" );
				return true;
			}
			return false;
		} catch ( UnsupportedOperationException e ) {

			logger.debug( "MDC Filter: Request doesn't allow operation on session in method insertJsessionInMdc");
			return false;
		} catch (ResourceNotFoundException e) {

			logger.debug( "MDC Filter: Resource not found in method insertJsessionInMdc");
			throw e;
		}catch (Exception e) {

			logger.warn( "MDC Filter: Error while inizialing MDC in method insertJsessionInMdc", e );
			return false;
		}
	}
	
	private boolean insertAWSCookieInMDC(HttpServletRequest request){
		Boolean awselbEnabled = configuration.getMdcAwsElbEnabled();
		
		try{
			if (awselbEnabled) {
				
				Cookie[] cookies = request.getCookies();
				
				if (cookies != null) {
					 for (Cookie cookie : cookies) {
					   if ("AWSELB".equals(cookie.getName())) {
					     String awselb = cookie.getValue();
					     MDC.put("awselb", "[" + awselb + "]" );
					     break;
					    }
					  }
					}
				return true;
			}
			return false;
		} catch (ResourceNotFoundException e) {
			logger.debug( "MDC Filter: Resource not found in method insertAWSCookieInMDC");
			throw e;
		}catch(Exception e){
			logger.warn( "MDC Filter: Error while inizialing MDC in method insertAWSCookieInMDC", e );
			return false;
		}
		
	}
	
	private boolean insertUserAgentInMDC(HttpServletRequest request){
		Boolean userAgentEnabled = configuration.getMdcUserAgentEnabled();
		
		try{
			if (userAgentEnabled) {
				
				String userAgent = request.getHeader("User-Agent");
				MDC.put("User-Agent", "[" + userAgent + "]" );
				return true;
			}
			return false;
		} catch (ResourceNotFoundException e) {
			logger.debug( "MDC Filter: Resource not found in method insertUserAgentInMDC");
			throw e;
		}catch(Exception e){
			logger.warn( "MDC Filter: Error while inizialing MDC in method insertUserAgentInMDC", e );
			return false;
		}
		
	}
	
	private boolean insertReferrerInMDC(HttpServletRequest request){
		Boolean referrerEnabled = configuration.getMdcReferrerEnabled();
		
		try{
			if (referrerEnabled) {
				
				String referrer = request.getHeader("Referer");
				MDC.put("Referrer", "[" + referrer + "]" );
				return true;
			}
			return false;
		} catch (ResourceNotFoundException e) {
			logger.debug( "MDC Filter: Resource not found in method insertReferrerInMDC");
			throw e;
		}catch(Exception e){
			logger.warn( "MDC Filter: Error while inizialing MDC in method insertReferrerInMDC", e );
			return false;
		}
		
	}
	
	@Override
	public void doFilter(ServletRequest pRequest, ServletResponse pResponse, FilterChain pChain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest)pRequest;
		HttpServletResponse response = (HttpServletResponse)pResponse;
		
		
		

		boolean mdcSettedWithIP=false;
		boolean mdcSettedWithJSession=false;
		boolean mdcSettedWithAWS=false;
		boolean mdcSettedWithUserAgent=false;
		boolean mdcSettedWithReferrer=false;
		
		try {
			
			/*Inserimento dell'IP del client nei log attraverso Mapped Diagnostic Context */	
			mdcSettedWithIP = insertIpInMdc( request );
			
			/*Inserimento dell'ID di sessione nei log attraverso Mapped Diagnostic Context */
			mdcSettedWithJSession= insertJsessionInMdc(request);
			
			/*Inserimento dell'ID del Load Balancer nei log attraverso Mapped Diagnostic Context */
			mdcSettedWithAWS=insertAWSCookieInMDC(request);
			
			/*Inserimento dello User-Agent nei log attraverso Mapped Diagnostic Context */
			mdcSettedWithUserAgent=insertUserAgentInMDC(request);
			
			/*Inserimento del Referrer nei log attraverso Mapped Diagnostic Context */
			mdcSettedWithReferrer=insertReferrerInMDC(request);
					
			pChain.doFilter(pRequest, pResponse);
		
		}catch(ResourceNotFoundException e){
			logger.debug( "MDC Filter: Resource not found");
			throw e;
		}catch (ScriptEvaluationException e) {
			logger.warn( "MDC Filter: ScriptEvaluationException Error", e );
			response.setStatus(500);
		} catch (BehaviourException e) {
			logger.warn( "MDC Filter: BehaviourException Error", e );
			response.setStatus(500);
		} catch (SlingException e) {
			logger.warn( "MDC Filter: SlingException Error", e );
			response.setStatus(500);
		} catch (Exception e) {
			logger.error( "MDC Filter: Generic Error", e );
			response.setStatus(500);
		}finally {
			
			if (mdcSettedWithIP) MDC.remove("clientIP");
			if (mdcSettedWithJSession) MDC.remove("jsessionId");
			if (mdcSettedWithAWS) MDC.remove("awselb");
			if (mdcSettedWithUserAgent) MDC.remove("User-Agent");
			if (mdcSettedWithReferrer) MDC.remove("Referrer");

		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
