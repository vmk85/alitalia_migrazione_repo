package com.alitalia.aem.consumer.mmb.servlet;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingPhaseEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

/**
 * REST service to initialize the MmbSession object in session for
 * the provided passenger name, surname and trip id.
 * 
 * Returns a JSON with outcome and redirect instructions.
 */

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "mmbsessioninit" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class MmbSessionInitServlet extends GenericMmbFormValidatorServlet {
	
	private static String ERROR_QSTRING = "?success=false";
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile MmbSession mmbSession;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		String passengerName = request.getParameter("mieivoli__name");
		String passengerSurname = request.getParameter("mieivoli__surname");
		String tripId = request.getParameter("mieivoli__code");
		String fromBooking = request.getParameter("from__booking");
		
		Validator validator = new Validator();
		
		if (!"1".equals(fromBooking)) {
		
			validator.addDirectCondition("mieivoli__name", passengerName, 
					MmbConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("mieivoli__name", passengerName, 
					MmbConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
			
			validator.addDirectCondition("mieivoli__surname", passengerSurname, 
					MmbConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("mieivoli__surname", passengerSurname, 
					MmbConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
			
			validator.addDirectCondition("mieivoli__code", tripId, 
					MmbConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("mieivoli__code", tripId, 
					MmbConstants.MESSAGE_GENERIC_INVALID_FIELD, "isPnrOrTicketNumber");
			
		}
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		String passengerName = request.getParameter("mieivoli__name");
		String passengerSurname = request.getParameter("mieivoli__surname");
		String tripId = request.getParameter("mieivoli__code");
		String forceClear = request.getParameter("force__clear");
		String fromBooking = request.getParameter("from__booking");
		String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		String currencyCode = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencyCode", String.class);
		String currencySymbol = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencySymbol", String.class);
		Locale locale = AlitaliaUtils.getRepositoryPathLocale(request.getResource());
		String decimalSeparator = AlitaliaUtils.getInheritedProperty(request.getResource(), "decimalSeparator", String.class);
		String groupingSeparator =  AlitaliaUtils.getInheritedProperty(request.getResource(), "groupingSeparator", String.class);
		Integer numDecDigits = AlitaliaUtils.getInheritedProperty(request.getResource(), "decimalDigit", Integer.class);
		if(numDecDigits == null){
			numDecDigits = 2;
		}
		NumberFormat currentNumberFormat = AlitaliaUtils.getNumberFormat(numDecDigits, decimalSeparator, groupingSeparator, market);
		String callerIp = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
		String machineName = AlitaliaUtils.getLocalHostName("unknown");
		I18n i18n = new I18n(request.getResourceBundle(locale));
		
		HttpSession session = request.getSession(true);
		
		MmbSessionContext currentCtx = null;
		if (!"1".equals(forceClear) 
				&& session.getAttribute(MmbConstants.MMB_CONTEXT_ATTRIBUTE) instanceof MmbSessionContext) {
			currentCtx = (MmbSessionContext) session.getAttribute(MmbConstants.MMB_CONTEXT_ATTRIBUTE);
		}
		session.removeAttribute(MmbConstants.MMB_CONTEXT_ATTRIBUTE);
		
		// recupero dati dalla sessione di booking, se presente
		boolean isFromBooking = false;
		String defaultCardType = null;
		String defaultCardNumber = null;
		Integer defaultCardExpireMonth = null;
		Integer defaultCardExpireYear = null;
		String defaultCardName = null;
		String defaultCardSurname = null;
		if ("1".equals(fromBooking) 
				&& session.getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE) instanceof BookingSessionContext) {
			try {
				BookingSessionContext bookingCtx = 
						(BookingSessionContext) session.getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
				if (bookingCtx.phase == BookingPhaseEnum.DONE &&
						bookingCtx.prenotation != null &&
						bookingCtx.paymentDataForMmb != null &&
						bookingCtx.paymentDataForMmb.getType() == PaymentTypeEnum.CREDIT_CARD &&
						bookingCtx.paymentDataForMmb.getProvider() instanceof PaymentProviderCreditCardData) {
					isFromBooking = true;
					tripId = bookingCtx.prenotation.getPnr();
					passengerName = bookingCtx.prenotation.getPassengers().get(0).getName();
					passengerSurname = bookingCtx.prenotation.getPassengers().get(0).getLastName();
					PaymentProviderCreditCardData creditCardProviderData =
							(PaymentProviderCreditCardData) bookingCtx.paymentDataForMmb.getProvider(); 
					defaultCardType = creditCardProviderData.getType().value();
					defaultCardNumber = creditCardProviderData.getCreditCardNumber();
					defaultCardExpireMonth = Integer.valueOf(creditCardProviderData.getExpiryMonth());
					defaultCardExpireYear = Integer.valueOf(creditCardProviderData.getExpiryYear());
					defaultCardName = creditCardProviderData.getUserInfo().getName();
					defaultCardSurname = creditCardProviderData.getUserInfo().getLastname();
				}
			} catch (Exception e) {
				logger.error("Could not preset payment data for MMB from Booking, an unexpected exception occurred.", e);
			}
		}
		
		MmbSessionContext ctx = mmbSession.initializeSession(currentCtx, i18n, MmbConstants.MMB_CLIENT, machineName, 
				MmbConstants.MMB_SID, MmbConstants.MMB_SITE, market, currencyCode, currencySymbol, locale, currentNumberFormat,
				callerIp, tripId, passengerName, passengerSurname, isFromBooking, defaultCardType, defaultCardNumber, 
				defaultCardExpireMonth, defaultCardExpireYear, defaultCardName, defaultCardSurname);
		
		session.setAttribute(MmbConstants.MMB_CONTEXT_ATTRIBUTE, ctx);
		
		String protocol = configuration.getHttpsEnabled() ? "https://": "http://";
		String redirectUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getMmbDetailPage();
		redirectUrl = request.getResourceResolver().map(redirectUrl);
		redirectUrl = protocol + configuration.getExternalDomain() + redirectUrl;
		
		if (isAjaxRequest(request)) {
			// for normal ajax requests, perform a standard JSON based redirect on successful search
			TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
			response.setContentType("application/json");
			json.object();
			json.key("result").value("OK");
			json.key("redirect").value(redirectUrl);
			json.endObject();
		} else {
			// for non-ajax requests, perform an HTTP redirection on successful search
			response.sendRedirect(redirectUrl);
		}
	}
	
	/**
	 * For this servlet, hardcode the self page URL to the PNR search page.
	 * This provide correct user redirection when a wrong search is performed
	 * with a non-ajax request (e.g. from confirmation email link).
	 */
	@Override
	protected String getSelfPageUrl(SlingHttpServletRequest request) {
		return AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getMmbHomePage();
	}
	
	/**
	 * For this servlet, overwrite the default MMB error page with the PNR search page.
	 * This provide correct user redirection when a wrong search is performed
	 * with an ajax request (e.g. from PNR search page itself).
	 */
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		HashMap<String, String> additionalParams =  new HashMap<String, String>();
		String url =  AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getMmbHomePage() + ERROR_QSTRING;
		additionalParams.put("redirect", request.getResourceResolver().map(url));
		return additionalParams;
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}
