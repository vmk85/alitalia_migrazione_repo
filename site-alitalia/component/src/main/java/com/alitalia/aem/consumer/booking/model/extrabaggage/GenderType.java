package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Enumerazione su Gender
*/
public enum GenderType
{
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("MALE")] MALE,
	MALE,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("FEMALE")] FEMALE,
	FEMALE,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("UNKNOWN")] UNKNOWN
	UNKNOWN;

	public static final int SIZE = java.lang.Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static GenderType forValue(int value)
	{
		return values()[value];
	}
}