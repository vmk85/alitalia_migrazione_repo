package com.alitalia.aem.consumer.booking.model.extrabaggage;

public abstract class BaseAirport
{
	private String CityCode;
	public final String getCityCode()
	{
		return CityCode;
	}
	public final void setCityCode(String value)
	{
		CityCode = value;
	}
	private String Code;
	public final String getCode()
	{
		return Code;
	}
	public final void setCode(String value)
	{
		Code = value;
	}
	private String Description;
	public final String getDescription()
	{
		return Description;
	}
	public final void setDescription(String value)
	{
		Description = value;
	}
	private String Area;
	public final String getArea()
	{
		return Area;
	}
	public final void setArea(String value)
	{
		Area = value;
	}
	private boolean AZDeserves;
	public final boolean getAZDeserves()
	{
		return AZDeserves;
	}
	public final void setAZDeserves(boolean value)
	{
		AZDeserves = value;
	}
	private boolean Eligibility;
	public final boolean getEligibility()
	{
		return Eligibility;
	}
	public final void setEligibility(boolean value)
	{
		Eligibility = value;
	}
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] public object EtktAvlblFrom {get;set;}
	private Object EtktAvlblFrom;
	public final Object getEtktAvlblFrom()
	{
		return EtktAvlblFrom;
	}
	public final void setEtktAvlblFrom(Object value)
	{
		EtktAvlblFrom = value;
	}
	private boolean HiddenInFirstDeparture;
	public final boolean getHiddenInFirstDeparture()
	{
		return HiddenInFirstDeparture;
	}
	public final void setHiddenInFirstDeparture(boolean value)
	{
		HiddenInFirstDeparture = value;
	}
	private int IdMsg;
	public final int getIdMsg()
	{
		return IdMsg;
	}
	public final void setIdMsg(int value)
	{
		IdMsg = value;
	}
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] public object UnavlblFrom {get;set;}
	private Object UnavlblFrom;
	public final Object getUnavlblFrom()
	{
		return UnavlblFrom;
	}
	public final void setUnavlblFrom(Object value)
	{
		UnavlblFrom = value;
	}
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] public object UnavlblUntil {get;set;}
	private Object UnavlblUntil;
	public final Object getUnavlblUntil()
	{
		return UnavlblUntil;
	}
	public final void setUnavlblUntil(Object value)
	{
		UnavlblUntil = value;
	}
}