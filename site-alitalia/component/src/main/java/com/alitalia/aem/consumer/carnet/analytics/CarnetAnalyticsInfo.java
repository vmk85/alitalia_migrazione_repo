package com.alitalia.aem.consumer.carnet.analytics;

import java.math.BigDecimal;
import java.util.Calendar;

public class CarnetAnalyticsInfo{
	int step;
	int carnetType;
	BigDecimal carnetRevenue;
	Calendar ordDate;
	String carnetCode;
	String carnetPaymentType;
	String carnetCCType;
}

