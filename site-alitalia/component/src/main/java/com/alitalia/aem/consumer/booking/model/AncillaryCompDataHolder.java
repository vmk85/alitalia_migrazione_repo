package com.alitalia.aem.consumer.booking.model;


public class AncillaryCompDataHolder {
	
	private String ancillaryCode;
	private Integer horizontalIndex;
	private Integer verticalIndex;
	private String componentClass;
	
	public AncillaryCompDataHolder() {
    }
	
	public AncillaryCompDataHolder(String code , Integer hIndex , Integer vIndex  , String compClass) {
        this.ancillaryCode = code;
        this.horizontalIndex = hIndex;
        this.verticalIndex = vIndex ;
        this.componentClass = compClass;
    }
	
	public String getAncillaryCode() {
		return ancillaryCode;
	}	

	public void setAncillaryCode(String name) {
		this.ancillaryCode = name;
	}
	
	public Integer getHorizontalIndex() {
		return horizontalIndex;
	}	

	public void setHorizontalIndex(Integer value) {
		this.horizontalIndex = value;
	}
	
	public Integer getVerticalIndex() {
		return verticalIndex;
	}	

	public void setVerticalIndex(Integer value) {
		this.verticalIndex = value;
	}

	public String getComponentClass() {
		return componentClass;
	}	

	public void setComponentClass(String name) {
		this.componentClass = name;
	}


}
