package com.alitalia.aem.consumer.booking;

public enum BookingSearchCategoryEnum {
	
	DOM("domestic"),
	DOM_PROMO("domesticPromo"),
	FCO_LIN("fcoLin"),
	INTZ("international"),
	INTZ_PROMO("internationalPromo"),
	INTZ_SAVER("internationalSaver"),
	INTZ_PREMIUM("internationalPremium"),
	INTC("intercontinental"),
	INTC_PROMO("intercontinentalPromo"),
	INTC_PROMO_SEMIFLEX("intercontinentalPromoSemiFlex"),
	INTC_LIGHT("intercontinentalLight"),
	INTC_LIGHT_SEMIFLEX("intercontinentalLightSemiFlex"),
	CONTINUITY("continuity"),
	CONTINUITY_OUT_SARDINIA("continuityOutSardinia"),
	CONTINUITY_REFUSE_SARDINIA("continuityRefuseSardinia"),
	CONTINUITY_REFUSE_SICILY("continuityRefuseSicily"),
	MIL("military"),
	FAM("family"),
	YTH("youth"),
	CARNET("carnet");
	
	private String value;

	private BookingSearchCategoryEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
