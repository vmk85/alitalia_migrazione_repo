package com.alitalia.aem.consumer.model.content.substriptionsme;

public class Area {
	private String sectorName;
	private String numEmployees;
	private String numFlyers;
	private String flewAverageAnnual;
	
	public String getSectorName() {
		return sectorName;
	}
	public void setSectorName(String sectorName) {
		this.sectorName = sectorName;
	}
	public String getNumEmployees() {
		return numEmployees;
	}
	public void setNumEmployees(String numbEmployers) {
		this.numEmployees = numbEmployers;
	}
	public String getNumFlyers() {
		return numFlyers;
	}
	public void setNumFlyers(String numFlyers) {
		this.numFlyers = numFlyers;
	}
	public String getFlewAverageAnnual() {
		return flewAverageAnnual;
	}
	public void setFlewAverageAnnual(String flewAverageAnnual) {
		this.flewAverageAnnual = flewAverageAnnual;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((flewAverageAnnual == null) ? 0 : flewAverageAnnual.hashCode());
		result = prime * result + ((numFlyers == null) ? 0 : numFlyers.hashCode());
		result = prime * result + ((numEmployees == null) ? 0 : numEmployees.hashCode());
		result = prime * result + ((sectorName == null) ? 0 : sectorName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Area other = (Area) obj;
		if (flewAverageAnnual == null) {
			if (other.flewAverageAnnual != null)
				return false;
		} else if (!flewAverageAnnual.equals(other.flewAverageAnnual))
			return false;
		if (numFlyers == null) {
			if (other.numFlyers != null)
				return false;
		} else if (!numFlyers.equals(other.numFlyers))
			return false;
		if (numEmployees == null) {
			if (other.numEmployees != null)
				return false;
		} else if (!numEmployees.equals(other.numEmployees))
			return false;
		if (sectorName == null) {
			if (other.sectorName != null)
				return false;
		} else if (!sectorName.equals(other.sectorName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Area [sectorName=" + sectorName + ", numbEmployers=" + numEmployees + ", numFlyers=" + numFlyers
				+ ", flewAverageAnnual=" + flewAverageAnnual + "]";
	}
	
}
