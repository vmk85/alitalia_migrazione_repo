package com.alitalia.aem.consumer.model.content.specialpages;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;



@Model(adaptables = { SlingHttpServletRequest.class })
public class MillemigliaComplainModel extends GenericBaseModel{

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private MillemigliaComplainData mmComplainData;
	private List<String> months;
	private List<Integer> days = new ArrayList<Integer>();
	private List<Integer> years = new ArrayList<Integer>();

	@PostConstruct
	protected void initModel() {
		logger.debug("[MillemigliaComplainModel] initModel");

		super.initBaseModel(slingHttpServletRequest);

		// retrieve and remove MillemigliaComplainData from session if any
		mmComplainData= (MillemigliaComplainData) 
				slingHttpServletRequest.getSession().getAttribute(MillemigliaComplainData.NAME);
		slingHttpServletRequest.getSession().removeAttribute(MillemigliaComplainData.NAME);

		for (int i = 1; i < 31; i++) {
			days.add(i);
		}

		months = I18nKeyCommon.getMonths();

		for (int i = 0; i < 10; i++) {
			years.add((Calendar.getInstance().get(Calendar.YEAR) - i));
		}


	}

	public MillemigliaComplainData getMmComplainData() {
		return mmComplainData;
	}

	public List<String> getMonths() {
		return months;
	}

	public List<Integer> getDays() {
		return days;
	}

	public List<Integer> getYears() {
		return years;
	}


	
	
	
	
}
