package com.alitalia.aem.consumer.booking.render;

import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.BrandPageData;

public class BrandRender{

	private BrandData brandData;
	private String name;
	private String id;
	private String path;

	public BrandRender(BrandData brandData, BookingSessionContext ctx){
		this.brandData = brandData;
		BrandPageData page = obtainBrandPageByCode(ctx);
		this.name = page != null ? page.getTitle() : "";
		this.id = page != null ? page.getId() : "";
		this.path = page != null ? page.getPath() : "";
	}
	
	public BrandRender(BrandPageData page, BookingSessionContext ctx) {
		this.brandData = new BrandData();
		brandData.setCode(page.getId());
		this.name = page.getTitle() != null ? page.getTitle() : "";
		this.id = page.getId() != null ? page.getId() : "";
		this.path = page.getPath() != null ? page.getPath() : "";
	}
	
	public BrandData getBrandData() {
		return brandData;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}
	
	public String getPath() {
		return path;
	}

	/* private methods */

	private BrandPageData obtainBrandPageByCode(BookingSessionContext ctx) {
		String brandCode = brandData.getCode().toLowerCase();
		
		BrandPageData brandPage = null;
		brandPage = ctx.codeBrandMap.get(brandCode);
		return brandPage;
	}
	
	@Override
	public String toString() {
		return "BrandRender [brandData=" + brandData + ", name=" + name
				+ ", id=" + id + ", path=" + path + "]";
	}
}