package com.alitalia.aem.consumer.checkinrest.jobs.publisher;

import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkinrest.jobs.CheckinJobConstants;
import com.alitalia.aem.consumer.checkinrest.jobs.consumer.ClearAncillariesJob;
import com.alitalia.aem.consumer.checkinrest.jobs.consumer.OffloadPassengerAndClearAncillariesJob;

import java.util.*;

/**
 * Created by ggadaleta on 06/02/2018.
 */
public class OffloadPassengerAndClearAncillariesPublisher extends BaseJobPublisher {

    public OffloadPassengerAndClearAncillariesPublisher() {
        this.setJobTopic(CheckinJobConstants.TOPIC_PASSENGER_OFFLOAD_AND_CLEAR_ANCILLARIES);
    }

    @Override
    public Map<String, Object> createProperties(CheckinSessionContext ctx) {

        final Map<String, Object> props = new HashMap<>();

        //set sessionId
        props.put(OffloadPassengerAndClearAncillariesJob.PROPERTY_CHECKIN_SESSION_ID, this.getSessionID());

        props.putAll(this.preparePrecheckinOffloadRequests(OffloadPassengerAndClearAncillariesJob.PROPERTY_CHECKIN_OFFLOAD, ctx));

        Boolean almenoUnAncillare = false;
        Boolean almenoUnInsurance = false;

        //TODO: refactor in caso di ancillari distribuiti su più di un PNR
        if(ctx.baggage != null) {
            if ("OK".equals(ctx.baggage.getOutcome())) {
                //c'è bagaglio
                logger.debug("Ancillary Baggage recognized {}", ctx.baggage.getOutcome());
                almenoUnAncillare = true;
            }
        }

        //TODO: refactor in caso di ancillari distribuiti su più di un PNR
        if (ctx.fastTrack != null) {
            if ("OK".equals(ctx.fastTrack.getOutcome())) {
                //c'è bagaglio
                logger.debug("Ancillary FastTrack recognized {}", ctx.fastTrack.getOutcome());
                almenoUnAncillare = true;
            }
        }

        //TODO: refactor in caso di ancillari distribuiti su più di un PNR
        if (ctx.lounge != null) {
            if ("OK".equals(ctx.lounge.getOutcome())) {
                //c'è bagaglio
                logger.debug("Ancillary Lounge recognized {}", ctx.lounge.getOutcome());
                almenoUnAncillare = true;
            }
        }

        if (ctx.comfortSeatInCart) {
            //c'è almeno un posto comfort nel carrello
            logger.debug("Ancillary ComfortSeat recognized {}", ctx.comfortSeatInCart);
            almenoUnAncillare = true;
        }

        if(almenoUnAncillare) {
            props.putAll(this.prepareClearAncillariesRequests(OffloadPassengerAndClearAncillariesJob.PROPERTY_CHECKIN_CLEARANCILLARIES, ctx));
        }

        for(int pnr = 0; pnr < ctx.pnrSelectedListDataRender.size(); pnr++) {
            if (ctx.pnrSelectedListDataRender.get(pnr).getInsurance() != null) {
                almenoUnInsurance = ctx.pnrSelectedListDataRender.get(pnr).getInsurance();
            }
        }
        if(almenoUnInsurance) {
            props.putAll(this.prepareClearInsuranceRequests(OffloadPassengerAndClearAncillariesJob.PROPERTY_CHECKIN_CLEARINSURANCE, ctx));
        }

        return props;
    }


}
