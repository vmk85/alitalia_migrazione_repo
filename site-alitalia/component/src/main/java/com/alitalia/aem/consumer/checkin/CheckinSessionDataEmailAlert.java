package com.alitalia.aem.consumer.checkin;

public class CheckinSessionDataEmailAlert {
private String name;
private String surname;
private String email;
private String mmcode;
private boolean enable;

public CheckinSessionDataEmailAlert(String name, String surname, String email, String mmcode, boolean enable){
	setName(name);
	setSurname(surname);
	setEmail(email);
	setMmcode(mmcode);
	setEnable(enable);
}

public String getName() {
	return name;
}

private void setName(String name) {
	this.name = name;
}

public String getSurname() {
	return surname;
}

private void setSurname(String surname) {
	this.surname = surname;
}

public String getEmail() {
	return email;
}

private void setEmail(String email) {
	this.email = email;
}

public String getMmcode() {
	return mmcode;
}

private void setMmcode(String mmcode) {
	this.mmcode = mmcode;
}

public boolean isEnable() {
	return enable;
}

private void setEnable(boolean enable) {
	this.enable = enable;
}

public String toString(){
	return "CheckinSessionDataEmailAlert name: " +name+
			" surname: "+surname+
			" email: "+ email+
			" MMcode: " +mmcode+
			" enable:"+ enable;
}

}
