package com.alitalia.aem.consumer.servlet.flyalitalia.substribtionsme;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import com.day.cq.commons.TidyJSONWriter;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.commons.osgi.PropertiesUtil;

import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.utils.AlitaliaCommonUtils;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.global.i18n.I18nKeySubscriptionSme;
import com.alitalia.aem.consumer.model.content.substriptionsme.AdministratorAccount;
import com.alitalia.aem.consumer.model.content.substriptionsme.Area;
import com.alitalia.aem.consumer.model.content.substriptionsme.Company;
import com.alitalia.aem.consumer.model.content.substriptionsme.Preferences;
import com.alitalia.aem.consumer.model.content.substriptionsme.SubscriptionSmeRequestData;
import com.alitalia.aem.consumer.model.content.substriptionsme.TravelAgency;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.ValidationUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "subscriptionsme" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class SubscriptionSme extends GenericFormValidatorServlet{

	// Static Property For Mails
	private static final String COMPONENT_NAME = "subscribe_sme";
	private static final String PARSYS_NAME = "page-component";
	private static final String MAIL_FROM_PROPERTY_NAME = "mailFrom";
	private static final String MAIL_TO_PROPERTY_NAME = "mailTo";
	private static final String MAIL_SUBJECT_PROPERTY_NAME = "subject";
	/*
	private static final String MAIL_FROM_DEFAULT = "abc@alitalia.com";
	private static final String MAIL_TO_DEFAULT = "abc@dataar.it";
	private static final String MAIL_SUBJECT_DEFAULT = "BusinessConnect";
	*/

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BusinessLoginService businessLoginService;
	
	@Reference
	private AlitaliaConfigurationHolder config;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[SubscriptionSmeRequestServlet] validateForm");
			
		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			/*
			String recaptchaResponse = request.getParameter("recaptchaResponse");
			boolean captchaAlreadyValidated = (Boolean)(request.getSession(true).getAttribute("captchaAlreadyValidatedSME") != null ? request.getSession(true).getAttribute("captchaAlreadyValidatedSME") : Boolean.FALSE);
			boolean networkEnabled = config.getNetworkEnabled();

			if (!captchaAlreadyValidated){
				if (networkEnabled){
					Boolean isResponseCorrect = ValidationUtils.isRecaptchaLoginValid(
							config.getRecaptchaSecretKey(), recaptchaResponse, "");

				if (!isResponseCorrect){
					ResultValidation resultValidationCaptcha = new ResultValidation();
					resultValidationCaptcha.setResult(isResponseCorrect);
					
					
					Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
					ResourceBundle resourceBundle = request.getResourceBundle(locale);
					final I18n i18n = new I18n(resourceBundle);
					resultValidationCaptcha.addField("recaptchaResponse", i18n.get("captcha.error.notvalid"));

					return resultValidationCaptcha;
				} else {
					request.getSession(true).setAttribute("captchaAlreadyValidatedSME", true);
					logger.debug("Captcha check passed");
				}
			}
			

			}else {
				request.getSession(true).setAttribute("captchaAlreadyValidatedSME", true); // Per controllo sulla Check
				logger.debug("Network unavailable: Captcha check passed");
			}*/
			boolean captchaAlreadyValidated = (Boolean)(request.getSession(true).getAttribute("captchaAlreadyValidated") != null ? request.getSession(true).getAttribute("captchaAlreadyValidated") : Boolean.FALSE);
			logger.debug("captchaAlreadyValidated: " + captchaAlreadyValidated);
			if (!captchaAlreadyValidated){
				String jcaptchaResponse = request.getParameter("captcha");
				String captchaId = request.getSession().getId();
				Boolean isResponseCorrect = ValidationUtils.isCaptchaValid(jcaptchaResponse, captchaId);

				if (!isResponseCorrect){
					ResultValidation resultValidation = new ResultValidation();
					resultValidation.setResult(isResponseCorrect);
					resultValidation.addField("captcha", "Valore non valido");
					return resultValidation;
				} else {
					request.getSession(true).setAttribute("captchaAlreadyValidated", true);
					logger.debug("Captcha check passed");
				}
			}

			return validator.validate();

		// an error occurred...
		}catch (Exception e) {
			logger.error("Errore durante la procedura di validazione dati "
					+ " Adesione Sme ", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		logger.debug("[SubscriptionSmeRequestServlet] performSubmit");
		
		boolean mailResult = sendMail(request);
		logger.debug("Mail Sended = " + mailResult);
		if (mailResult) {
            try {
                writeSubmitResponse(response);
            } catch (Exception e) {
                logger.error("Error during validation.", e);
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
		}
		else {
			throw new IOException("Service Error");
		}
		
	}

    protected void writeSubmitResponse(SlingHttpServletResponse response) throws Exception {
        response.setContentType("application/json");

        // set response message (JSON)
        TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
        json.object();
        json.key("result").value("true");
        json.endObject();
    }

	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {

		SubscriptionSmeRequestData data = populateData(request);

		//### add validate conditions to Company
		validator.addDirectCondition("company_name", data.getCompany().getCompanyName(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addCrossConditionMessagePattern("company_name", data.getCompany().getCompanyName(), "26",
				I18nKeySpecialPage.COMPANYNAME_TOO_LONG,
				"", "sizeIsLess");


		if(AlitaliaUtils.getRepositoryPathMarket(request.getResource()).equalsIgnoreCase("it")){
			validator.addDirectCondition("company_piva", data.getCompany().getPiva(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("company_piva", data.getCompany().getPiva(), I18nKeyCommon.MESSAGE_INVALID_FIELD, "isPartitaIVA");
		}

		validator.addDirectCondition("company_address", data.getCompany().getAddress(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		validator.addDirectCondition("company_country", data.getCompany().getCountry(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		if( data.getCompany().getCountry().equalsIgnoreCase("IT") || data.getCompany().getCountry().equalsIgnoreCase("US")){
			validator.addDirectCondition("company_state_prov", data.getCompany().getState_prov(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		}

		validator.addDirectCondition("company_city", data.getCompany().getCity(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("company_city", data.getCompany().getCity(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isAlphabeticWithAccentAndSpaces");

		validator.addDirectCondition("company_zipcode", data.getCompany().getZipCode(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		logger.debug("Countries config: [" + config.getSmeZipAlphanumericCountries() + "]");
		logger.debug("Country selected: [" + data.getCompany().getCountry() + "]");
		
		if(config.getSetProperty(AlitaliaConfigurationHolder.SME_ZIP_ALPHANUMERIC_COUNTRIES).contains(data.getCompany().getCountry())){
			validator.addDirectCondition("company_zipcode", data.getCompany().getZipCode(), I18nKeyCommon.MESSAGE_INVALID_FIELD, "isAlphaNumeric");
		}else{
			validator.addDirectCondition("company_zipcode", data.getCompany().getZipCode(), I18nKeyCommon.MESSAGE_INVALID_FIELD, "isNumber");
		}


		validator.addDirectCondition("company_area_prefix", data.getCompany().getCompanyAreaPrefix(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		validator.addDirectCondition("company_phone_number", data.getCompany().getCompanyPhoneNumber(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		validator.addDirectCondition("company_phone_nationalprefix", data.getCompany().getCompanyNationalPrefix(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		validator.addDirectCondition("company_email", data.getCompany().getCompanyEmail(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("company_email", data.getCompany().getCompanyEmail(), I18nKeyCommon.MESSAGE_INVALID_FIELD, "isEmail");

		//### add validate conditions to AdminAccountData
		validator.addDirectCondition("admin_account_name",	data.getAdminAccount().getName(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("admin_account_name",	data.getAdminAccount().getName(), I18nKeyCommon.MESSAGE_INVALID_FIELD, "isAlphabeticWithSpaces");

		validator.addDirectCondition("admin_account_surname",	data.getAdminAccount().getSurname(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("admin_account_surname",	data.getAdminAccount().getSurname(), I18nKeyCommon.MESSAGE_INVALID_FIELD, "isAlphabeticWithSpaces");

		validator.addDirectCondition("admin_account_day", data.getAdminAccount().getDay(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("admin_account_day",	data.getAdminAccount().getDay(), I18nKeyCommon.MESSAGE_INVALID_FIELD, "isNumber");

		validator.addDirectCondition("admin_account_month",	data.getAdminAccount().getMonth(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("admin_account_month",	data.getAdminAccount().getMonth(), I18nKeyCommon.MESSAGE_INVALID_FIELD, "isNumber");

		validator.addDirectCondition("admin_account_year",	data.getAdminAccount().getYear(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("admin_account_year",	data.getAdminAccount().getYear(), I18nKeyCommon.MESSAGE_INVALID_FIELD, "isNumber");

		//validator.addDirectCondition("admin_account_qualification",	data.getAdminAccount().getQualification( I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isAlphabeticWithSpaces");

		validator.addDirectCondition("admin_account_area_prefix",	data.getAdminAccount().getAdministratorAreaPrefix(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		validator.addDirectCondition("admin_account_phone_nationalprefix",	data.getAdminAccount().getAdministratorNationalPrefix(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		validator.addDirectCondition("admin_account_phone_number",	data.getAdminAccount().getAdministratorPhoneNumber(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		validator.addDirectCondition("admin_account_email",	data.getAdminAccount().getAdministratorEmail(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("admin_account_email",	data.getAdminAccount().getAdministratorEmail(), I18nKeyCommon.MESSAGE_INVALID_FIELD, "isEmail");

		//validator.addDirectConditionMessagePattern("admin_account_fax", data.getAdminAccount().getFax(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");


		//### Any validate conditions to Area

		//### add validate conditions to Preferences
		validator.addCrossCondition("preferences_accepted", data.getPreferences().getAccepted(), "yes",I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "areEqual");

		validator.addDirectCondition("preferences_comunication", data.getPreferences().getAlitaliaOffers(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		if(AlitaliaUtils.getRepositoryPathMarket(request.getResource()).equalsIgnoreCase("it")){
			validator.addDirectCondition("preferences_language", data.getPreferences().getLanguage(), I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		}

		/*
		validator.addDirectConditionMessagePattern("recaptchaResponse", request.getParameter("recaptchaResponse"),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "isNotEmpty");
				*/
		return validator;
	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[SubscriptionSmeRequestServlet] saveDataIntoSession");

		// create baggageClaimData object and save it into session
		SubscriptionSmeRequestData subscriptionSmeRequestData = populateData(request);

		// go back to form with error
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);

		request.getSession().setAttribute(SubscriptionSmeRequestData.NAME,
				subscriptionSmeRequestData);

	}

	private Page getCurrentPage(Resource resource) {
		boolean notFound = true;
		Resource currentResource = resource;
		while (notFound) {
			if (currentResource.getResourceType().equalsIgnoreCase("cq:Page")) {
				Page page = currentResource.adaptTo(Page.class);
				return page;
			} else {
				currentResource = currentResource.getParent();
				notFound = !(currentResource == null);
			}
		}
		return null;
	}

	/*
	 * isItalianMarket
	 *
	 */
	private boolean isItalianMarket(Page currentPage) {
		//market
		String path=currentPage.getPath().toString();
		if (path.toLowerCase().contains(("Alitalia-it").toLowerCase())){
			return true;
		}else{return false;}
	}

	/*
	 * sendMail
	 * 
	 */
	private boolean sendMail(SlingHttpServletRequest request) {
		logger.debug("[SubscriptionSmeRequestServlet] sendMail");

		// current page
		Page currentPage = getCurrentPage(request.getResource());

		// email from
		String mailFrom = getComponentProperty(currentPage, PARSYS_NAME, COMPONENT_NAME, MAIL_FROM_PROPERTY_NAME);

		if (mailFrom == null) {
			mailFrom=config.getSpecialpageBusinessConnectEmailFrom();
		}

		// email to
		String mailTo = getComponentProperty(currentPage, PARSYS_NAME, COMPONENT_NAME, MAIL_TO_PROPERTY_NAME);

		if (mailTo == null) {
			mailTo = config.getSpecialpageBusinessConnectEmailTo();
		}

		// email subject
		String subject = getComponentProperty(currentPage, PARSYS_NAME, COMPONENT_NAME, MAIL_SUBJECT_PROPERTY_NAME);

		if (subject == null) {
			subject=config.getSpecialpageBusinessConnectEmailSubject();
			if (isItalianMarket(currentPage)) {
				subject=subject + " Italia";
			}else{
				subject=subject + " Estero";
			}
		}

		// email body
		String messageText = generateBodyMail(request);

		logger.debug("[ SubscriptionSme] sendMail -- Email data: "
				+ "[From: {" + mailFrom + "}] - [To: {" + mailTo + "}] - "
				+ "[Subject: {" + subject + "}] - [Body: {" + messageText + "}]");

		// create email message object
		EmailMessage emailMessage = new EmailMessage();
		emailMessage.setFrom(mailFrom);
		emailMessage.setTo(mailTo);
		emailMessage.setSubject(subject);
		emailMessage.setMessageText(messageText);
		emailMessage.setIsBodyHtml(false);
		emailMessage.setDeliveryNotification(DeliveryNotificationOptionsEnum.NONE);
		emailMessage.setPriority(MailPriorityEnum.NORMAL);

		// create AgencySendMailRequest
		AgencySendMailRequest mailRequest = new AgencySendMailRequest();
		mailRequest.setTid(IDFactory.getTid());
		mailRequest.setSid(IDFactory.getSid(request));
		mailRequest.setEmailMessage(emailMessage);

		// get AgencySendMailResponse
		AgencySendMailResponse mailResponse =
				businessLoginService.sendMail(mailRequest);

		// return status
		return mailResponse.isSendMailSuccessful();

	}



	/*
	 * generateBodyMail
	 * 
	 */
	private String generateBodyMail(SlingHttpServletRequest request)  {
		logger.debug("[SubscriptionSmeRequestServlet] generateBodyMail");

		String msg = "";
		SubscriptionSmeRequestData smeReq = populateData(request);
		try {
			Map<String, Object> map = AlitaliaCommonUtils.introspect(smeReq);
			for(String k : map.keySet()){
				if(k.equalsIgnoreCase("error") || k.equalsIgnoreCase("name") || k.equalsIgnoreCase("class"))
					continue;
				msg += "\n"+k.toUpperCase()+"\n";

				Map<String, Object> mapChild;

				mapChild = AlitaliaCommonUtils.introspect(map.get(k));

				for(String kC : mapChild.keySet()){
					if( kC.equalsIgnoreCase("class"))
						continue;
					if(mapChild.get(kC) != null && !mapChild.get(kC).toString().isEmpty())
						msg += kC + ": " + mapChild.get(kC) + "\n";
				}
			}
		} catch (Exception e) {
			logger.error("Errore durante la creazione del corpo della mail", e);
		}

		return msg;

	}


	private SubscriptionSmeRequestData populateData(SlingHttpServletRequest request){
		SubscriptionSmeRequestData subscriptionSmeRequestData = new SubscriptionSmeRequestData();

		//### get parameters from request
		AdministratorAccount adminAccount = new AdministratorAccount();
		adminAccount.setDay(request.getParameter("admin_account_day"));
		adminAccount.setMonth(request.getParameter("admin_account_month"));
		adminAccount.setYear(request.getParameter("admin_account_year"));
		adminAccount.setAdministratorNationalPrefix(request.getParameter("admin_account_phone_nationalprefix"));
		adminAccount.setAdministratorAreaPrefix(request.getParameter("admin_account_area_prefix"));
		adminAccount.setAdministratorPhoneNumber(request.getParameter("admin_account_phone_number"));
		adminAccount.setAdministratorEmail(request.getParameter("admin_account_email"));
		adminAccount.setAdministratorFax(request.getParameter("admin_account_fax"));
		adminAccount.setName(request.getParameter("admin_account_name"));
		adminAccount.setSurname(request.getParameter("admin_account_surname"));
		adminAccount.setQualification(request.getParameter("admin_account_qualification"));

		Area area = new Area();
		area.setFlewAverageAnnual(request.getParameter("area_flew_average_annual"));
		area.setNumEmployees(request.getParameter("area_num_employers"));
		area.setNumFlyers(request.getParameter("area_num_flyers"));
		area.setSectorName(request.getParameter("area_name"));

		Company company = new Company();
		company.setAddress(request.getParameter("company_address"));
		company.setCity(request.getParameter("company_city"));
		company.setCompanyName(request.getParameter("company_name"));
		company.setCompanyNationalPrefix(request.getParameter("company_phone_nationalprefix"));
		company.setCompanyAreaPrefix(request.getParameter("company_area_prefix"));
		company.setCompanyPhoneNumber(request.getParameter("company_phone_number"));
		company.setState_prov(request.getParameter("company_state_prov"));
		company.setCompanyEmail(request.getParameter("company_email"));
		company.setCompanyFax(request.getParameter("company_fax"));
		company.setPiva(request.getParameter("company_piva"));
		company.setWebsiteUrl(request.getParameter("company_website"));
		company.setZipCode(request.getParameter("company_zipcode"));
		company.setCountry(request.getParameter("company_country"));

		Preferences preferences = new Preferences();
		preferences.setAccepted(request.getParameter("preferences_accepted"));
		preferences.setAlitaliaOffers(request.getParameter("preferences_comunication"));
		preferences.setCaptchaString(request.getParameter("captcha"));
		preferences.setLanguage(request.getParameter("preferences_language"));
		preferences.setPromotionalCode(request.getParameter("preferences_codice_promo"));

		subscriptionSmeRequestData.setCompany(company);
		subscriptionSmeRequestData.setArea(area);
		subscriptionSmeRequestData.setAdminAccount(adminAccount);
		subscriptionSmeRequestData.setPreferences(preferences);

		return subscriptionSmeRequestData;

	}


	/*
	 * getComponentProperty
	 * 
	 */
	private String getComponentProperty(Page ancestor, String parsysName, String compName, String propName) {
		logger.debug("[BaggageClaimServlet] getComponentProperty");

		Resource pageResource = ancestor.getContentResource();	
		String prop = null;
		try{
			prop = pageResource.getChild(parsysName).getChild(compName)
					.getValueMap().get(propName, String.class);
		}
		catch(Exception e) {
			logger.error("SubscriptionSme.java - getComponentProperty: impossibile recuperare la propietà " + propName);
		}

		return prop;
	}

}