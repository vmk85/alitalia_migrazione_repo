package com.alitalia.aem.consumer.mmb.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables={SlingHttpServletRequest.class})
public class MmbAncillaryBoxPathModel extends GenericBaseModel {
	
	@Self
	private SlingHttpServletRequest slingHttpServletRequest;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private String mmbDetailPath;
	
	@PostConstruct
	protected void initModel() {
		Resource resource = slingHttpServletRequest.getResource();
		String path = AlitaliaUtils.findSiteBaseRepositoryPath(resource) + configuration.getMmbDetailPage();
		if (path.endsWith(".html")) {
			path = path.substring(0, path.length() - 5);
		}
		path += "/jcr:content/mmb-ancillary/";
		this.mmbDetailPath = path;
	}
	
	public String getMmbDetailPath() {
		return mmbDetailPath;
	}
	
	public String getMmbDetailMealPath() {
		return mmbDetailPath + "meal";
	}
	
	public String getMmbDetailSeatPath() {
		return mmbDetailPath + "seat";
	}
	
	public String getMmbDetailExtraBaggagePath() {
		return mmbDetailPath + "extrabaggage";
	}
	
	public String getMmbDetailFastTrackPath() {
		return mmbDetailPath + "fasttrack";
	}
	
	public String getMmbDetailLoungePath() {
		return mmbDetailPath + "lounge";
	}
	
	public String getMmbDetailInsurancePath() {
		return mmbDetailPath + "insurance";
	}
	
}
