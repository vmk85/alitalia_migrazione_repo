package com.alitalia.aem.consumer.booking.model;


public class AncillaryPageDoubleContainer {
	
	private String firstComponent;
	private String firstComponentPath;	
	private String secondComponent;
	private String secondComponentPath;
	
	private Integer verticalIndex;
	
	public String getFirstComponent() {
		return firstComponent;
	}	

	public void setFirstComponent(String name) {
		this.firstComponent = name;
	}
	
	public String getFirstComponentPath() {
		return firstComponentPath;
	}	

	public void setFirstComponentPath(String name) {
		this.firstComponentPath = name;
	}
	
	public String getSecondComponent() {
		return secondComponent;
	}	

	public void setSecondComponent(String name) {
		this.secondComponent = name;
	}
	
	public String getSecondComponentPath() {
		return secondComponentPath;
	}	

	public void setSecondComponentPath(String name) {
		this.secondComponentPath = name;
	}
	
	public Integer getVerticalIndex() {
		return verticalIndex;
	}	

	public void setVerticalIndex(Integer value) {
		this.verticalIndex = value;
	}



}
