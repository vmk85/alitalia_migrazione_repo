package com.alitalia.aem.consumer.servlet.specialpages.exceptions;

@SuppressWarnings("serial")
public class NewsletterUserNotRegisteredException extends Exception {

	public NewsletterUserNotRegisteredException() {
		// TODO Auto-generated constructor stub
	}

	public NewsletterUserNotRegisteredException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public NewsletterUserNotRegisteredException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public NewsletterUserNotRegisteredException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public NewsletterUserNotRegisteredException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
