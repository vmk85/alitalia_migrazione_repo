package com.alitalia.aem.consumer.model.content.specialpages;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class NewsletterSubscribeModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private NewsletterSubscribeData newsletterSubscribeData;

	

	@PostConstruct
	protected void initModel() {
		logger.debug("[NewsletterSubscribeModel] initModel");

		super.initBaseModel(slingHttpServletRequest);

	
		newsletterSubscribeData = (NewsletterSubscribeData) slingHttpServletRequest.getSession()
				.getAttribute(NewsletterSubscribeData.NAME);
		slingHttpServletRequest.getSession().removeAttribute(NewsletterSubscribeData.NAME);

	
	}

	public NewsletterSubscribeData getNewsletterSubscribeData() {
		return newsletterSubscribeData;
	}



}


