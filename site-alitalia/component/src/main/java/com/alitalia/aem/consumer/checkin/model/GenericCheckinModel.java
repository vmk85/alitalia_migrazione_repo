package com.alitalia.aem.consumer.checkin.model;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;

public class GenericCheckinModel extends GenericBaseModel {
	
	protected CheckinSessionContext ctx;
	
	@Inject
	protected AlitaliaConfigurationHolder configuration;
	
	@Override
	protected void initBaseModel(SlingHttpServletRequest request) {
		
		super.initBaseModel(request);

		logger.info("GenericCheckinModel (initBaseModel) session accessed: {}", request.getSession().getId());


		if(request.getSession(true).getAttribute(CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE) != null) {
			ctx = (CheckinSessionContext) request.getSession(true).getAttribute(CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE);
		} else {
			ctx = null;
		}

		if (ctx == null && !isWCMEnabled()) {
			logger.error("CheckIn session context not found. Referer: "+ request.getHeader("referer"));
			throw new RuntimeException("CheckIn session context not found.");
		}
		
	}
	
	public CheckinSessionContext getCtx() {
		return ctx;
	}
}
