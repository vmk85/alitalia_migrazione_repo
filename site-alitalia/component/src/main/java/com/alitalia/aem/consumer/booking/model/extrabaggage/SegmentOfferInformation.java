package com.alitalia.aem.consumer.booking.model.extrabaggage;



/** 
 Class for segment offer information
*/
public class SegmentOfferInformation
{
	private int flightsMiles;
	public final int getflightsMiles()
	{
		return flightsMiles;
	}
	public final void setflightsMiles(int value)
	{
		flightsMiles = value;
	}
	private boolean awardFare;
	public final boolean getawardFare()
	{
		return awardFare;
	}
	public final void setawardFare(boolean value)
	{
		awardFare = value;
	}
}