package com.alitalia.aem.consumer.checkinrest.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.*;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;
import com.alitalia.aem.consumer.checkinrest.render.PnrListDataRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrRender;



@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckInPnr extends GenericCheckinModel{
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private SlingHttpServletResponse response;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	@Inject
	private CheckinSession checkinSession;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	//Oggetti JAVA
	//Variabili Classe principale
	private List<Pnr> pnrListData;
	
	private List<PnrRender> pnrListDataRender;	
	
	private List<PnrRender> pnrSelectedListDataRender;

    private Passenger passenger;

    private String FFcode;
    static private boolean timeConversionAlreadyPerformed = false;
	@PostConstruct
	protected void initModel() throws Exception {
		
		//Estendendo la classe GenericCheckInModel abbiamo a disposizione anche il contesto
		//Qui posso controllare i dati di risposta e renderli utilizzabili al FrontEnd AEM
		try{
			super.initBaseModel(request);
			
			this.pnrListData = new ArrayList<>();
			
			if (ctx == null && !isWCMEnabled()) {
			    //fixme non entra mai qui perche' nel metodo super.initBaseModel lancia eccezione in caso di ctx==null
				logger.debug("[CheckInPnr] - contesto assente.");

				String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
						request.getResource(), false) + configuration.getCheckinSearch();

				String redirectPage = request.getResourceResolver().map(successPage);

				response.sendRedirect(redirectPage);

				return;
			}
			
			if (ctx != null)
			{

                // Faccio una nuova ricerca del pnr per aggiornare gli emd


                this.setPnrListData(new ArrayList<>());
                this.FFcode = ctx.frequentFlyer;

                // Start - Modifiche Orari Mercato Americano e Canadese
                String market = getCtx().market;
                String language = getCtx().language;

                // End - Modifiche Orari Mercato Americano

//                try {
//                    for (Pnr pnrlistDataTime : ctx.pnrListData) {
//                        for (Flight flightTime : pnrlistDataTime.getFlights()) {
//                            for (Segment segmentTime : flightTime.getSegments()) {
//                                segmentTime.setDuration(segmentTime.getDuration().substring(0, 5));
//                                if (segmentTime.getDuration().substring(3,5) == "00" ) {
//                                    segmentTime.setDuration(segmentTime.getDuration().substring(0, 2)+" H");
//                                    if (segmentTime.getDuration().substring(0,1) == "0" ){
//                                        segmentTime.setDuration(segmentTime.getDuration().substring(1, 2)+" H");
//                                    }
//                                }
//                                segmentTime.setWaiting(segmentTime.getWaiting().substring(0, 5));
//                                if (segmentTime.getWaiting().substring(3,5) == "00" ) {
//                                    segmentTime.setWaiting(segmentTime.getWaiting().substring(0, 2)+" H");
//                                    if (segmentTime.getWaiting().substring(0,1) == "0" ){
//                                        segmentTime.setWaiting(segmentTime.getWaiting().substring(1, 2)+" H");
//                                    }
//                                }
//                            }
//
//                        }
//                    }
//                } catch (Exception e) {
//                }


                this.setPnrListData(ctx.pnrListData);


//				this.setSegmentIndex();


                int total = 0;
                int executed = 0;
                int notAllowed = 0;

                PnrListDataRender renderList = new PnrListDataRender();
                this.setPnrListDataRender(new ArrayList<>());
                this.setPnrListDataRender(renderList.getPnrListDataRender(ctx.pnrListData, ctx, total, executed, notAllowed));


                if (ctx.pnrSelectedListDataRender != null) {
                    this.setPnrSelectedListDataRender(new ArrayList<>());
                    this.setPnrSelectedListDataRender(ctx.pnrSelectedListDataRender);

                    if (this.getPnrSelectedListDataRender().size() != 0 && this.getPnrListDataRender().size() != 0 && this.getPnrListDataRender().get(0).getFlightsRender().size() > 1) {
                        this.setFlightType((
                                this.getPnrSelectedListDataRender().get(0).getFlightsRender().get(0).getOrigin().equals(
                                        this.getPnrListDataRender().get(0).getFlightsRender().get(1).getOrigin())
                                        &&
                                        this.getPnrSelectedListDataRender().get(0).getFlightsRender().get(0).getDestination().equals(
                                                this.getPnrListDataRender().get(0).getFlightsRender().get(1).getDestination()) ?
                                        true
                                        :
                                        false
                        ));
                    }
                    /*Begin 21-06-2018 per il momento e' richiesto il Formato Orario AM/PM solo per i mercati Americano e Canadese */
//                    if ((market.toLowerCase().equals("us") || market.toLowerCase().equals("ca")) && !this.timeConversionAlreadyPerformed) {
                    if (this.isAmericanCountry()) {
                        if (this.getPnrSelectedListDataRender().size() != 0) {
                            //la sezione pnrSelectedListDataRender risulta valorizzata a valle dell'evento "Manage Check-in" in check-in-flights-list.html (Gestisci Check-in)
                            for (int idx=0; idx < this.getPnrSelectedListDataRender().get(0).getFlightsRender().get(0).getSegments().size(); idx++){
                                String newArrivalTime;
                                String newDepartureTime;
                                String prevArrivalTime = this.getPnrSelectedListDataRender().get(0).getFlightsRender().get(0).getSegments().get(idx).getArrivalTime();
                                String prevDepartureTime = this.getPnrSelectedListDataRender().get(0).getFlightsRender().get(0).getSegments().get(idx).getDepartureTime();

                                newArrivalTime = AlitaliaUtils.getTimeForMarket(market, prevArrivalTime);
                                newDepartureTime = AlitaliaUtils.getTimeForMarket(market, prevDepartureTime);
                                this.getPnrSelectedListDataRender().get(0).getFlightsRender().get(0).getSegments().get(idx).setArrivalTime(newArrivalTime);
                                this.getPnrSelectedListDataRender().get(0).getFlightsRender().get(0).getSegments().get(idx).setDepartureTime(newDepartureTime);
                            }
                            // La sezione che segue e' coinvolta nella presentazione dei dati (departureTime e arrivalTime) nel modulo checkin-recap-volo-1
                            for (int idx=0; idx < this.getPnrSelectedListDataRender().get(0).getFlightsRender().size(); idx++){
                                String prevDepTime = this.getPnrSelectedListDataRender().get(0).getFlightsRender().get(idx).getDepartureTime();
                                String prevArrTime = this.getPnrSelectedListDataRender().get(0).getFlightsRender().get(idx).getArrivalTime();
                                String newArrivalTime = AlitaliaUtils.getTimeForMarket(market, prevArrTime);
                                String newDepartureTime = AlitaliaUtils.getTimeForMarket(market, prevDepTime);
                                this.getPnrSelectedListDataRender().get(0).getFlightsRender().get(idx).setDepartureTime(newDepartureTime);
                                this.getPnrSelectedListDataRender().get(0).getFlightsRender().get(idx).setArrivalTime(newArrivalTime);
                            }
                        }
                        else {
                            // dovrei entrare in questo ramo, durante la prima ricerca del PNR
                            for (int idx = 0; idx < this.getPnrListDataRender().get(0).getAllsegments().size(); idx++) {
                                String newArrivalTime;
                                String newDepartureTime;
                                String prevArrivalTime = this.getPnrListDataRender().get(0).getAllsegments().get(idx).getArrivalTime();
                                String prevDepartureTime = this.getPnrListDataRender().get(0).getAllsegments().get(idx).getDepartureTime();

                                newArrivalTime = AlitaliaUtils.getTimeForMarket(market, prevArrivalTime);
                                newDepartureTime = AlitaliaUtils.getTimeForMarket(market, prevDepartureTime);
                                this.getPnrListDataRender().get(0).getAllsegments().get(idx).setArrivalTime(newArrivalTime);
                                this.getPnrListDataRender().get(0).getAllsegments().get(idx).setDepartureTime(newDepartureTime);
                            }
                        }
//                        this.timeConversionAlreadyPerformed = true;
                    }
                    /*End 21-06-2018 per il momento e' richiesto il Formato Orario AM/PM solo per i mercati Americano e Canadese */

                    try {
                        for(Passenger passenger: this.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().get(0).getPassengers()){
                            passenger.setNumberOfBaggage(0);
                            for(Emd emd: passenger.getEmd()){
                                if (emd.getGroup().equals("BG")){
                                    passenger.setNumberOfBaggage(passenger.getNumberOfBaggage() + 1);
                                }
                            }

                        }
                    }catch (Exception e){}

//                    this.setFlightType();
                    // Carico array dei posti per ogni oasseggero
					/*int x1 = 0;
					// Aggiorno il posto sul passeggero
					for (PnrRender pnrSelectedListDataRender : ctx.pnrSelectedListDataRender) {

						int x2 = 0;
						for (FlightsRender flight : pnrSelectedListDataRender.getFlightsRender()) {

							int x3 = 0;
							for (SegmentRender segment : flight.getSegments()) {

								// Ciclo sui passeggeri
								int x4 = 0;
								for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passengerX : segment.getPassengers())
								{
									List<PassengerSeat> seats = new ArrayList<PassengerSeat>();
									if (passengerX.getPassengerSeats() != null)
										seats = passengerX.getPassengerSeats();

									PassengerSeat _seats = new PassengerSeat();
									_seats.setOrigin(segment.getOrigin().getCode());
									_seats.setPostoAssegnato(true);
									_seats.setNumeroPostoAssegnato(passengerX.getNumeroPostoAssegnato());
									seats.add(_seats);

									ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengers().get(x4).setPassengerSeats(seats);

									x4 += 1;
								}

								x3 += 1;

							}
							x2 += 1;
						}
						x1 += 1;
					}*/

                }

//                if (!ctx.checkinSelectedPassengersExtra.isEmpty()){
//                    this.selectedPassengerExtra = new ArrayList<>();
//                    this.setSelectedPassengerExtra(ctx.checkinSelectedPassengersExtra);
//                }
//                else if(!ctx.checkinSelectedPassengers.isEmpty()){
//                    this.selectedPassenger = new ArrayList<>();
//                    this.setSelectedPassenger(ctx.checkinSelectedPassengers);
//                }

                if(ctx.passengerName!= null && ctx.passengerSurname != null){
                    this.passenger = new Passenger();
                    this.getPassenger().setNome(ctx.passengerName);
                    this.getPassenger().setCognome(ctx.passengerSurname);
                }
            }

        } catch (Exception e) {
            logger.error("CheckInPnr: ", e);
//            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    public List<Pnr> getPnrListData() {
        return pnrListData;
    }

    public void setPnrListData(List<Pnr> pnrListData) {
        this.pnrListData = pnrListData;
    }

    public List<PnrRender> getPnrSelectedListDataRender() {
        return pnrSelectedListDataRender;
    }

    public void setPnrSelectedListDataRender(List<PnrRender> pnrSelectedListDataRender) {
        this.pnrSelectedListDataRender = pnrSelectedListDataRender;
    }

    public List<PnrRender> getPnrListDataRender() {
        return pnrListDataRender;
    }

    public void setPnrListDataRender(List<PnrRender> pnrListDataRender) {
        this.pnrListDataRender = pnrListDataRender;
    }

    public void setFlightType(Boolean flightType) {
        this.flightType = flightType;
    }

    public Boolean getFlightType() {
        return flightType;
    }

    private Boolean flightType;

    public String getFFcode() {
        return FFcode;
    }

    public void setFFcode(String FFcode) {
        this.FFcode = FFcode;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

}