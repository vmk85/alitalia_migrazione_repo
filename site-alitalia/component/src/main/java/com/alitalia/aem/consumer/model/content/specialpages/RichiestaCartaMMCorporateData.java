package com.alitalia.aem.consumer.model.content.specialpages;

public class RichiestaCartaMMCorporateData {
	
	public static final String NAME = "RichiestaCartaMMCorporateData";
	private String codiceMillemiglia;
	private String nome;
	private String cognome;
	private String codiceAzienda;
	private String email;
	private String nazione;
	private String check1;
	private String check2;
	private String check3;
	private String check4;
	
	private String error;
	
	
	@Override
	public String toString() {
		return "RichiestaCartaMMCorporateData [codiceMillemiglia=" + codiceMillemiglia + ", nome=" + nome + ", cognome="
				+ cognome + ", codiceAzienda=" + codiceAzienda + ", email=" + email + ", nazione=" + nazione
				+ ", check1=" + check1 + ", check2=" + check2 + ", check3=" + check3 + ", check4=" + check4 + "]";
	}
	
	
	public String getCodiceMillemiglia() {
		return codiceMillemiglia;
	}
	public void setCodiceMillemiglia(String codiceMillemiglia) {
		this.codiceMillemiglia = codiceMillemiglia;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getCodiceAzienda() {
		return codiceAzienda;
	}
	public void setCodiceAzienda(String codiceAzienda) {
		this.codiceAzienda = codiceAzienda;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNazione() {
		return nazione;
	}
	public void setNazione(String nazione) {
		this.nazione = nazione;
	}
	public String getCheck1() {
		return check1;
	}
	public void setCheck1(String check1) {
		this.check1 = check1;
	}
	public String getCheck2() {
		return check2;
	}
	public void setCheck2(String check2) {
		this.check2 = check2;
	}
	public String getCheck3() {
		return check3;
	}
	public void setCheck3(String check3) {
		this.check3 = check3;
	}
	public String getCheck4() {
		return check4;
	}
	public void setCheck4(String check4) {
		this.check4 = check4;
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	
	/*Utils methods*/

	/*Extract the country list for a specific country.
	 *eg.
	 * currentCountry = "JA" 
	 * countryMap = "JA:JAPAN;EN:ALBANIA,ARGENTINA,BULGARIA,GREECE,JAPAN,KOREA,POLAND,ROMANIA,RUSSIA,SPAIN,UK"
	 * output = {"JAPAN"} 
	 * */
	public static String[] extractCountryList(String currentCountry, String countryMap){
		String[] result=null;
		if(currentCountry != null || countryMap != null){
			String[] countryLists = countryMap.split(";");
			boolean found = false;
			for(int i=0; i< countryLists.length && !found; i++){
				String[] split = countryLists[i].split(":");
				if(split.length == 2 && currentCountry.equalsIgnoreCase(split[0])){
					result = split[1].split(",");
					found = true;
				}
			}
		}
		return result;
	}
}
