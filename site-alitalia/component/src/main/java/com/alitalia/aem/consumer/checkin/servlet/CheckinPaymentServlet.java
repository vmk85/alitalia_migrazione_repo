package com.alitalia.aem.consumer.checkin.servlet;

import java.math.BigDecimal;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.DictionaryItemData;
import com.alitalia.aem.common.data.home.PaymentTypeItemData;
import com.alitalia.aem.common.data.home.StateData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checkinpaymentconsumer" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinPaymentServlet extends GenericCheckinFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile CheckinSession checkinSession;	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		
		String tipologiaCarta = request.getParameter("tipologiaCarta");
		
		// type of payment
		String[] countries = null;
		String[] countriesUSAValues = null;

		//prepare countries list for validation
		if (ctx.countries != null) {
			countries = new String[ctx.countries.size()];
			int i = 0;
			for(CountryData country : ctx.countries) {
				countries[i] = country.getCode();
				i++;
			}
		}
		
		//prepare countries list for validation
		if (ctx.countriesUSA != null) {
			countriesUSAValues = new String[ctx.countriesUSA.size()];
			int i = 0;
			for(StateData country : ctx.countriesUSA) {
				countriesUSAValues[i] = country.getStateCode();
				i++;
			}
		}
		
		//cerco il maxCvvLength con cui validare il cvv per la carta di credito selezionata
		Integer maxCvvLength = 0;
		for (PaymentTypeItemData creditCard : ctx.creditCards) {
			if (tipologiaCarta.equals(creditCard.getCode())) {
				for (DictionaryItemData data : creditCard.getOtherInfo()) {
					if (data.getKey().equals("MaxCVC")) {
						maxCvvLength = (Integer) data.getValue();
						break;
					}
				}
				break;
			}
		}
		
		Validator validator = new Validator();
		
		// recupero dati dalla request
		String numeroCarta = request.getParameter("numeroCarta");
		String meseScadenzaCarta = request.getParameter("meseScadenza");
		String annoScadenzaCarta = request.getParameter("annoScadenza");
		String cvc = request.getParameter("cvc");
		String nomeTitolare = request.getParameter("nome");
		String cognomeTitolare = request.getParameter("cognome");
		String email = request.getParameter("email");
		
		String indirizzo = request.getParameter("indirizzo");
		String cap = request.getParameter("cap");
		String citta = request.getParameter("citta");
		String paese = request.getParameter("paese");
		String countriesUSA = request.getParameter("countriesUSA");
		
		
		//validazione
		validator.addDirectCondition("numeroCarta", numeroCarta, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("meseScadenza", meseScadenzaCarta, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("meseScadenza", meseScadenzaCarta, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isMonth");
		
		validator.addDirectCondition("annoScadenza" , annoScadenzaCarta, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("annoScadenza", annoScadenzaCarta, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isYear");
		
		validator.addDirectCondition("cvc", cvc, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("cvc", cvc, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, 
				maxCvvLength == 4 ? "isCVCFourDigits" : "isCVCThreeDigits");
		
		validator.addDirectCondition("nome", nomeTitolare, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("nome", nomeTitolare, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
		
		validator.addDirectCondition("cognome", cognomeTitolare, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("cognome", cognomeTitolare, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
		
		validator.addDirectCondition("email", email, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("email", email, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isEmail");
		
		// check extra fields for American Express
		if (!tipologiaCarta.equals("") && tipologiaCarta != null &&
				tipologiaCarta.equals("AX")) {

			validator.addDirectCondition("indirizzo", indirizzo,
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("cap", cap,
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("cap", cap,
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNumber");
			validator.addDirectCondition("citta", citta,
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("paese", paese,
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.setAllowedValues("paese", paese,
					countries, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD);
			//Controllo campi extra di AMEX solo se il paese è US
			if (paese.equalsIgnoreCase("us")) {
				validator.addDirectCondition("countriesUSA", countriesUSA,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.setAllowedValues("countriesUSA", countriesUSA,
						countriesUSAValues, BookingConstants.MESSAGE_GENERIC_INVALID_FIELD);
			}
		}
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		CheckinSessionContext ctx = getCheckInSessionContext(request); 
		
		// recupero dati dalla request
		String numeroCarta = request.getParameter("numeroCarta");
		Integer meseScadenzaCarta = Integer.parseInt(request.getParameter("meseScadenza"));
		Integer annoScadenzaCarta = Integer.parseInt(request.getParameter("annoScadenza"));
		String cvc = request.getParameter("cvc");
		String nomeTitolare = request.getParameter("nome");
		String cognomeTitolare = request.getParameter("cognome");
		String email = request.getParameter("email");
		//Extra fields AMEX
		String indirizzo = request.getParameter("indirizzo");
		String cap = request.getParameter("cap");
		String citta = request.getParameter("citta");
		String paese = request.getParameter("paese");
		String countriesUSA = request.getParameter("countriesUSA");
		String tipologiaCarta = request.getParameter("tipologiaCarta");
		String creditCardTypeName = "";
		String state = null;
		if (paese.equalsIgnoreCase("us")) {
			state = countriesUSA;
		} 
		
		BigDecimal displayedAmount = new BigDecimal(0);
		if (!StringUtils.isBlank(request.getParameter("displayedAmount"))) {
			displayedAmount = new BigDecimal(request.getParameter("displayedAmount"));
		}
		
		for (PaymentTypeItemData creditCard : ctx.creditCards) {
			if (tipologiaCarta.equals(creditCard.getCode())) {
				creditCardTypeName = creditCard.getDescription();
				break;
			}
		}
		
		ctx.confirmationMailAddress = email;
 		checkinSession.performPayment(ctx, displayedAmount, numeroCarta, 
 				creditCardTypeName, tipologiaCarta, cvc, meseScadenzaCarta, 
 				annoScadenzaCarta, nomeTitolare, cognomeTitolare, indirizzo,
 				cap, citta, paese, state);
		
		String redirectUrl = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCheckinAncillaryConfirmationPage();
		redirectUrl = request.getResourceResolver().map(redirectUrl);
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("redirect").value(redirectUrl);
		json.endObject();
		
		
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
	@Override
	protected String getUnrecoverableErrorRedirect(
			SlingHttpServletRequest request, SlingHttpServletResponse response,
			GenericFormValidatorServletException exception) {
		return AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getMmbThankyouPage();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		Map additionalFailureData = super.getAdditionalFailureData(request, exception);
		if (exception.isRecoverable()) {
			additionalFailureData.put("redirect", 
					AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
					+ getConfiguration().getCheckinPaymentPage());
		}
		return additionalFailureData;
	}
	
}

