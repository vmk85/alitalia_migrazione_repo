package com.alitalia.aem.consumer.model.content.notifiche;

import java.util.Map;

import com.alitalia.aem.consumer.utils.AlitaliaUtils;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("rawtypes")
@Model(adaptables={Resource.class})
public class Notifiche {
	
    @Self
    private Resource resource;

    private Logger logger = LoggerFactory.getLogger(Notifiche.class);
    
    private Map fieldList;

    /**
     * E' l'equivalente di un costruttore di classe. 
     * In questo caso viene utilizzato per prendere i valori del mutlifield
     */
    @PostConstruct
    protected void initModel() {
    	
    	//logger.info("Initializing model << BlockElement >>");
    	try {
            fieldList = AlitaliaUtils.fromJcrMultifieldToMapArrayObject(resource, "items");
        } catch(Exception e) {
            logger.error("Unable to get the values: \n" + e);
        }
    }

    /**
     * Restituisce tutti gli elementi del multifield
     */
    public Map getFieldList() {
        //logger.info("Returning fieldList");
        return fieldList;
    }
}

