package com.alitalia.aem.consumer.checkinrest.servlet;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.updatepassenger.response.UpdatePassengerResp;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdatePassengerResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkinupdatepassenger"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinUpdatePassengerServlet extends GenericCheckinFormValidatorServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private CheckinSession checkinSession;

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		Validator validator = new Validator();

		String departureDate = request.getParameter("departureDate");
		String gender = request.getParameter("gender");
		String numberOfAdultDocuments = request.getParameter("numberOfAdultDocuments");
		String numberOfInfantDocuments = request.getParameter("numberOfInfantDocuments");

		if(numberOfAdultDocuments != null && !numberOfAdultDocuments.isEmpty()){
			validator.addDirectCondition("numberOfAdultDocuments", numberOfAdultDocuments, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		}

		if(numberOfInfantDocuments != null && !numberOfInfantDocuments.isEmpty()){
			validator.addDirectCondition("gender", numberOfInfantDocuments, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		}
		
		validator.addDirectCondition("gender", gender, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		if(gender.length() != 1){
			validator.addDirectCondition("gender", gender, 
					CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isValid");
		}
		
		else{
			validator.addDirectCondition("gender", gender, 
					CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAlphabetic");
		}

		validator.addDirectCondition("departureDate", departureDate, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

		CheckinSessionContext ctx = getCheckInSessionContext(request);

		String errorUrl = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCheckinFailurePage();

		errorUrl = request.getResourceResolver().map(errorUrl);

		String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCheckinPassengersPage();

		successPage = request.getResourceResolver().map(successPage);

		CheckinUpdatePassengerResponse result = checkinSession.checkinUpdatePassenger(request, ctx);

		if(result != null){
			if (result.get_updatepassengerResp().getPassengers() != null && result.get_updatepassengerResp().getPassengers().size() > 0) {
				//				if(result.get_updatepassengerResp().getPassengers().get(0).getResult().equals("OK")){
				//TODO ??
				response.setContentType("application/json");
				Gson gsonOut = new Gson();
				UpdatePassengerResp passObj = result.get_updatepassengerResp();
				String jsonString = gsonOut.toJson(passObj, UpdatePassengerResp.class);
				response.getWriter().write(jsonString);
				//				}
			} else {
				//TODO - redirect to errorPage?
				TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
				response.setContentType("application/json");
				json.object();
				json.key("isError").value(true);
				json.endObject();
			}
		}
		else{
			logger.error("[CheckinUpdatePassengerServlet] - Nessun risultato trovato.");
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		Map<String,String> additionalParams = new HashMap<String, String>();
		return additionalParams;
	}
}