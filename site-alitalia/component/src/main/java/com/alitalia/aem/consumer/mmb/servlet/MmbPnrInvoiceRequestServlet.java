package com.alitalia.aem.consumer.mmb.servlet;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "mmbpnrinvocerequest" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class MmbPnrInvoiceRequestServlet extends GenericMmbFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile MmbSession mmbSession;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		/* Recupero dati dalla request */		
		String tipoFattura = request.getParameter("tipoFattura");
		String nome = request.getParameter("nome");
		String cognome = request.getParameter("cognome");
		String codiceFiscalePIva = request.getParameter("codiceFiscalePIva");
		String indirizzo = request.getParameter("indirizzo");
		String codicePostale = request.getParameter("codicePostale");
		String citta = request.getParameter("citta");
		String provincia = request.getParameter("provincia");
		String email = request.getParameter("email");
		String intestatarioFattura = request.getParameter("intestatarioFattura");
		
		
		/* Validazione */
		Validator validator = new Validator();
		
		validator.addDirectCondition("nome", nome, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("cognome", cognome, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		if ("PF".equals(tipoFattura)) {
			validator.addDirectCondition("codiceFiscalePIva", nome + "|" + cognome + "|" + codiceFiscalePIva, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isValidCodiceFiscaleByConcatNomeCognomeCF");
		} else if ("A".equals(tipoFattura)) {
			validator.addDirectCondition("codiceFiscalePIva", codiceFiscalePIva, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isPartitaIVA");
			
			validator.addDirectCondition("intestatarioFattura", intestatarioFattura, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		} else {
			validator.addDirectCondition("codiceFiscalePIva", codiceFiscalePIva, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		}
		
		validator.addDirectCondition("indirizzo", indirizzo, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("codicePostale", codicePostale, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("citta", citta, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("paese", provincia, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("email", email, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("email", email, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isEmail");
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		MmbSessionContext ctx = getMmbSessionContext(request);
		/* Recupero dati dalla request */		
		String tipoFattura = request.getParameter("tipoFattura");
		String nome = request.getParameter("nome");
		String cognome = request.getParameter("cognome");
		String codiceFiscalePIva = request.getParameter("codiceFiscalePIva");
		String indirizzo = request.getParameter("indirizzo");
		String codicePostale = request.getParameter("codicePostale");
		String citta = request.getParameter("citta");
		String provincia = request.getParameter("provincia");
		String email = request.getParameter("email");
		String intestatarioFattura = request.getParameter("intestatarioFattura");

		mmbSession.askForInvoice(ctx, tipoFattura, nome, cognome, codiceFiscalePIva, indirizzo, codicePostale, 
				citta, provincia, email, intestatarioFattura);
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value("OK");
		json.key("message").value(ctx.i18n.get(MmbConstants.MESSAGE_SUCCESS_ASK_FOR_INVOCE));
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}
