package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryMealDetailData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPassengerData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillarySeatDetailData;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup.AncillariesGroupType;
import com.alitalia.aem.consumer.mmb.render.MmbAncillaryCartPassengerRender;
import com.alitalia.aem.consumer.mmb.render.MmbAncillaryCartRender;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;
import com.alitalia.aem.consumer.mmb.render.MmbThankyouAnalyticsRender;
import com.alitalia.aem.consumer.utils.Lists;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbAncillaryCart extends MmbSessionAncillaryGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	private List<MmbAncillaryCartPassengerRender> passengerRenderList;
	private MmbPriceRender cartTotalPrice;
	private Integer cartTotalQuantity;
	private List<String> routeHeaderRender;
	private Boolean isInsuranceAddedToCart;
	private MmbPriceRender insurancePrice; 
	private String paymentCreditCardNumberHide;
	private String paymentCreditCardType;
	private MmbThankyouAnalyticsRender purchasedInfo;
	
	@PostConstruct
	protected void initModel() throws IOException {
		try{
			initBaseModel(request);
			
			this.cartTotalPrice = new MmbPriceRender(ctx.cartTotalAmount, ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat);
			this.cartTotalQuantity = 0;
			this.routeHeaderRender = new LinkedList<String>();
			
			this.paymentCreditCardNumberHide = ctx.paymentCreditCardNumberHide == null ? "" 
					: ctx.paymentCreditCardNumberHide;
			this.paymentCreditCardType = ctx.paymentCreditCardType == null ? "" : ctx.paymentCreditCardType;
			
			MmbAncillaryStatusEnum ancillaryStatus = !ctx.ancillaryPaymentSuccess ? MmbAncillaryStatusEnum.TO_BE_ISSUED 
					: MmbAncillaryStatusEnum.ISSUED;
			
			List<MmbAncillariesGroup> groupsByPassenger = MmbAncillariesGroup.createGroups(
					AncillariesGroupType.BY_PASSENGER, ctx.route, ctx.cart.getPassengers(), 
					ctx.cart.getAncillaries(), null);
			
			this.passengerRenderList = new LinkedList<MmbAncillaryCartPassengerRender>(); 
			for (MmbAncillariesGroup passengerGroup : groupsByPassenger) {
				MmbAncillaryPassengerData passengerData = passengerGroup.getPassengerData();
				
				List<MmbAncillariesGroup> groups = MmbAncillariesGroup.createGroups(
						AncillariesGroupType.BY_PASSENGER_AND_ROUTE, passengerGroup.getRoute(), Lists.asList(passengerData),
						passengerGroup.getAncillariesByStatuses(
								new MmbAncillaryStatusEnum[] {
										MmbAncillaryStatusEnum.TO_BE_ISSUED, 
										MmbAncillaryStatusEnum.ISSUED}), 
							null);
				
				MmbAncillaryCartPassengerRender passengerRender = new MmbAncillaryCartPassengerRender();
				passengerRender.setId(passengerGroup.getPassengerIndex());
				passengerRender.setFullName(passengerData.getName() + " " + passengerData.getLastName());
				passengerRender.setTicketNumber(
						(passengerData.getEtickets() == null || passengerData.getEtickets().size() == 0) ? "" :
						passengerData.getEtickets().get(0).getNumber());
				passengerRender.setQuantity(0);
				
				passengerRender.setSeatTotalPrice(
						new MmbPriceRender(new BigDecimal(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				passengerRender.setExtraBaggageTotalPrice(
						new MmbPriceRender(new BigDecimal(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				passengerRender.setFastTrackTotalPrice(
						new MmbPriceRender(new BigDecimal(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				passengerRender.setMealTotalPrice(
						new MmbPriceRender(new BigDecimal(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				passengerRender.setLoungeTotalPrice(
						new MmbPriceRender(new BigDecimal(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				
				boolean isRouteHeaderRenderEmpty = this.routeHeaderRender.size() == 0;
				for (MmbAncillariesGroup group : groups) {
					
					if (isRouteHeaderRenderEmpty) {
						routeHeaderRender.add(getRouteTypeLabel(group));
					}
					
					/* Seat */ 
					MmbAncillaryCartRender seatCartRender = new MmbAncillaryCartRender();
					BigDecimal seatPartialPrice = new BigDecimal(0);
					Integer seatQuantity = 0;
					List<MmbAncillaryData> seatAncillaryList = group.getAncillariesByType(MmbAncillaryTypeEnum.SEAT);
					for (MmbAncillaryData seatAncillary : seatAncillaryList) {
						if (MmbAncillaryStatusEnum.TO_BE_ISSUED.equals(seatAncillary.getAncillaryStatus())
								|| MmbAncillaryStatusEnum.ISSUED.equals(seatAncillary.getAncillaryStatus())) {
							seatPartialPrice = seatPartialPrice.add(seatAncillary.getAmount());
							seatQuantity += seatAncillary.getQuantity();
							seatCartRender.setSeat(
									((MmbAncillarySeatDetailData) seatAncillary.getAncillaryDetail()).getSeat());
							seatCartRender.setExtraComfortSeat(
									((MmbAncillarySeatDetailData) seatAncillary.getAncillaryDetail()).isComfort());
						}
					}
					seatCartRender.setQuantity(seatQuantity);
					passengerRender.setSeatTotalQuantity(passengerRender.getSeatTotalQuantity() + seatQuantity);
					this.cartTotalQuantity += seatQuantity;
					passengerRender.setQuantity(passengerRender.getQuantity() + seatQuantity);
					passengerRender.getSeatItemList().add(seatCartRender);
					passengerRender.getSeatTotalPrice().increaseAmount(seatPartialPrice);
					
					
					/* Extra Baggage */
					MmbAncillaryCartRender extraBaggageCartRender = new MmbAncillaryCartRender();
					BigDecimal extraBaggagePartialPrice = new BigDecimal(0);
					Integer extraBaggageQuantity = 0;
					List<MmbAncillaryData> extraBaggageAncillaryList = 
							group.getAncillariesByType(MmbAncillaryTypeEnum.EXTRA_BAGGAGE);
					for (MmbAncillaryData extraBaggageAncillary : extraBaggageAncillaryList) {
						if (ancillaryStatus.equals(extraBaggageAncillary.getAncillaryStatus())) {
							extraBaggagePartialPrice = extraBaggagePartialPrice.add(extraBaggageAncillary.getAmount());
							extraBaggageQuantity += extraBaggageAncillary.getQuantity();
						}
					}
					extraBaggageCartRender.setQuantity(extraBaggageQuantity);
					passengerRender.setExtraBaggageTotalQuantity(passengerRender.getExtraBaggageTotalQuantity() + extraBaggageQuantity);
					this.cartTotalQuantity += extraBaggageQuantity;
					passengerRender.setQuantity(passengerRender.getQuantity() + extraBaggageQuantity);
					extraBaggageCartRender.setI18nKeyDescription(extraBaggageQuantity == 1 ? 
							MmbConstants.CART_EXTRABAGGAGE_TYPE_LABEL : 
								MmbConstants.CART_EXTRABAGGAGE_TYPE_LABEL_PLURAL);
					passengerRender.getExtraBaggageItemList().add(extraBaggageCartRender);
					passengerRender.getExtraBaggageTotalPrice().increaseAmount(extraBaggagePartialPrice);
					
					
					/* Fast Track */
					MmbAncillaryCartRender fastTrackCartRender = new MmbAncillaryCartRender();
					BigDecimal fastTrackPartialPrice = new BigDecimal(0);
					Integer fastTrackQuantity = 0;
					List<MmbAncillaryData> fastTrackAncillaryList = 
							group.getAncillariesByType(MmbAncillaryTypeEnum.FAST_TRACK);
					for (MmbAncillaryData fastTrackAncillary : fastTrackAncillaryList) {
						if (ancillaryStatus.equals(fastTrackAncillary.getAncillaryStatus())) {
							fastTrackPartialPrice = fastTrackPartialPrice.add(fastTrackAncillary.getAmount());
							fastTrackQuantity += fastTrackAncillary.getQuantity();
						}
					}
					fastTrackCartRender.setQuantity(fastTrackQuantity);
					passengerRender.setFastTrackTotalQuantity(passengerRender.getFastTrackTotalQuantity() + fastTrackQuantity);
					this.cartTotalQuantity += fastTrackQuantity;
					passengerRender.setQuantity(passengerRender.getQuantity() + fastTrackQuantity);
					fastTrackCartRender.setI18nKeyDescription(fastTrackQuantity == 1 ? 
							MmbConstants.CART_FASTTRACK_TYPE_LABEL : 
								MmbConstants.CART_FASTTRACK_TYPE_LABEL_PLURAL);
					passengerRender.getFastTrackItemList().add(fastTrackCartRender);
					passengerRender.getFastTrackTotalPrice().increaseAmount(fastTrackPartialPrice);
				
					
					/* Meal */
					MmbAncillaryCartRender mealCartRender = new MmbAncillaryCartRender();
					BigDecimal mealPartialPrice = new BigDecimal(0);
					Integer mealQuantity = 0;
					List<MmbAncillaryData> mealAncillaryList = group.getAncillariesByType(MmbAncillaryTypeEnum.MEAL);
					for (MmbAncillaryData mealAncillary : mealAncillaryList) {
						if (MmbAncillaryStatusEnum.ISSUED.equals(mealAncillary.getAncillaryStatus())) {
							mealPartialPrice = mealPartialPrice.add(mealAncillary.getAmount());
							mealQuantity += mealAncillary.getQuantity();
							mealCartRender.setI18nKeyDescription(MessageFormat.format(MmbConstants.CART_MEAL_TYPE_LABEL, 
									((MmbAncillaryMealDetailData) mealAncillary.getAncillaryDetail()).getMeal()));
						}
					}
					mealCartRender.setQuantity(mealQuantity);
					passengerRender.setMealTotalQuantity(passengerRender.getMealTotalQuantity() + mealQuantity);
					this.cartTotalQuantity += mealQuantity;
					passengerRender.setQuantity(passengerRender.getQuantity() + mealQuantity);
					passengerRender.getMealItemList().add(mealCartRender);
					passengerRender.getMealTotalPrice().increaseAmount(mealPartialPrice);
					
					
					/* Lounge */
					MmbAncillaryCartRender loungeCartRender = new MmbAncillaryCartRender();
					BigDecimal loungePartialPrice = new BigDecimal(0);
					Integer loungeQuantity = 0;
					List<MmbAncillaryData> loungeAncillaryList = 
							group.getAncillariesByType(MmbAncillaryTypeEnum.VIP_LOUNGE);
					for (MmbAncillaryData loungeAncillary : loungeAncillaryList) {
						if (ancillaryStatus.equals(loungeAncillary.getAncillaryStatus())) {
							loungePartialPrice = loungePartialPrice.add(loungeAncillary.getAmount());
							loungeQuantity += loungeAncillary.getQuantity();
						}
					}
					loungeCartRender.setQuantity(loungeQuantity);
					passengerRender.setLoungeTotalQuantity(passengerRender.getLoungeTotalQuantity() + loungeQuantity);
					this.cartTotalQuantity += loungeQuantity;
					passengerRender.setQuantity(passengerRender.getQuantity() + loungeQuantity);
					loungeCartRender.setI18nKeyDescription(loungeQuantity == 1 ? 
							MmbConstants.CART_LOUNGE_TYPE_LABEL : MmbConstants.CART_LOUNGE_TYPE_LABEL_PLURAL);
					passengerRender.getLoungeItemList().add(loungeCartRender);
					passengerRender.getLoungeTotalPrice().increaseAmount(loungePartialPrice);
					
				}
				passengerRenderList.add(passengerRender);
				
			}
			
			
			/* INSURANCE */
			this.isInsuranceAddedToCart = ctx.insuranceAddedToCart;
			if (this.isInsuranceAddedToCart) {
				this.insurancePrice = new MmbPriceRender(ctx.insurancePolicy.getTotalInsuranceCost(), ctx.currencyCode, 
						ctx.currencySymbol, ctx.currentNumberFormat);
				this.cartTotalQuantity += 1;
			}
			
			/*Analytics purchased items info*/
			purchasedInfo = new MmbThankyouAnalyticsRender(ctx);
			
		}
		catch(Exception e){
			logger.error("Unexpectd error", e);
		}
		
	}

	public List<MmbAncillaryCartPassengerRender> getPassengerRenderList() {
		return passengerRenderList;
	}

	public MmbPriceRender getCartTotalPrice() {
		return cartTotalPrice;
	}

	public List<String> getRouteHeaderRender() {
		return routeHeaderRender;
	}

	public Integer getCartTotalQuantity() {
		return cartTotalQuantity;
	}

	public Boolean getIsInsuranceAddedToCart() {
		return isInsuranceAddedToCart;
	}

	public MmbPriceRender getInsurancePrice() {
		return insurancePrice;
	}

	public String getPaymentCreditCardNumberHide() {
		return paymentCreditCardNumberHide;
	}

	public String getPaymentCreditCardType() {
		return paymentCreditCardType;
	}

	public MmbThankyouAnalyticsRender getPurchasedInfo() {
		return purchasedInfo;
	}

}
