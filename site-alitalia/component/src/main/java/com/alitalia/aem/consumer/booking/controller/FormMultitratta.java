package com.alitalia.aem.consumer.booking.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables={SlingHttpServletRequest.class})
public class FormMultitratta extends GenericBaseModel{
	
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private List<String> tratte = new ArrayList<String>();
	
	@PostConstruct
	protected void initModel() {
		
		logger.debug("Starting initModel: <<<FormMultitratta>>>");
		
		tratte.add("booking.homepage.cercaVoloMultitratta.prima.label");
		tratte.add("booking.homepage.cercaVoloMultitratta.seconda.label");
		tratte.add("booking.homepage.cercaVoloMultitratta.terza.label");
		tratte.add("booking.homepage.cercaVoloMultitratta.quarta.label");
	}
	
	public List<String> getTratte() {
		return tratte;
	}

}
