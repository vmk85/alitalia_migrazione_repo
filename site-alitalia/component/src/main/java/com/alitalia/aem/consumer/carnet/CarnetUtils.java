package com.alitalia.aem.consumer.carnet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.SlingRequestProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.wcm.api.WCMMode;

public class CarnetUtils {
	
	protected static Logger logger = LoggerFactory.getLogger(CheckinUtils.class);

	public static String computeCreditCardName(CreditCardTypeEnum creditCardType) {
		String name = "";
		switch (creditCardType) {
			case AMERICAN_EXPRESS:
				name = "American Express";
				break;
			case DINERS:
				name = "Diners";
				break;
			case MASTER_CARD:
				name = "Master Card";
				break;
			case UATP:
				name = "Uatp";
				break;
			case VISA:
				name = "Visa";
				break;
			case VISA_ELECTRON:
				name = "Visa Electron";
				break;
			default:
				name = "";
				break;
		}
		return name;
	}

	public static String computecreditCardIcon(CreditCardTypeEnum creditCardType) {
		String icon = "";
		switch(creditCardType){
			case AMERICAN_EXPRESS:
				icon = "americanExpress";
				break;
			case DINERS:
				icon = "diners";
				break;
			case MASTER_CARD:
				icon = "masterCard";
				break;
			case UATP:
				icon = "uatp";
				break;
			case VISA:
				icon = "visa";
				break;
			case VISA_ELECTRON:
				icon = "visaElectron";
				break;
			default:
				icon = "";
				break;
			}
		return icon;
	}
	
	public static String computeCreditCardImageName(CreditCardTypeEnum creditCardType) {
		String imageName = "";
		switch(creditCardType){
			case AMERICAN_EXPRESS:
				imageName = "AmericanExpress.png";
				break;
			case DINERS:
				imageName = "Diners.png";
				break;
			case MASTER_CARD:
				imageName = "MasterCard.png";
				break;
			case UATP:
				imageName = "Uatp.png";
				break;
			case VISA:
				imageName = "Visa.png";
				break;
			case VISA_ELECTRON:
				imageName = "VisaElectron.png";
				break;
			default:
				imageName = "";
				break;
			}
		return imageName;
	}
	
	public static String getMailBody(Resource resource, String templateMail,
			boolean secure, ResourceResolverFactory resolverFactory,
			RequestResponseFactory requestResponseFactory,
			SlingRequestProcessor requestProcessor) {
		
		String result = null;
		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(resource, false);
		try {

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			/* Get resource resolver */
			@SuppressWarnings("deprecation")
			ResourceResolver resourceResolver =
				resolverFactory.getAdministrativeResourceResolver(null);
			String requestedUrl = baseUrl + AlitaliaConstants.BASE_CONFIG_PATH
					+ "/" + templateMail;

			/* Setup request */
			HttpServletRequest req =
					requestResponseFactory.createRequest("GET", requestedUrl);
			WCMMode.DISABLED.toRequest(req);

			out = new ByteArrayOutputStream();
			HttpServletResponse resp =
					requestResponseFactory.createResponse(out);

			/* Process request through Sling */
			requestProcessor.processRequest(req, resp, resourceResolver);

			String encoding = "UTF-8";

			result = new String(out.toByteArray(), Charset.forName(encoding));

		} catch (LoginException | ServletException | IOException e) {
			logger.error("Generic exception while retrieving email template "
					+ "from JCR repository: {}", e);
			result = "";
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("Template retrieved and APIS/ESTA placeholders "
					+ "replaced: {} ", result);
		}
		
		return result;
		
	}
	
}
