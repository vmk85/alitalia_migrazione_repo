package com.alitalia.aem.consumer.carnet.model;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CarnetLoginModel extends GenericCarnetModel {

	@Self
	private SlingHttpServletRequest request;
	
	private String codiceCarnet;
	
	@PostConstruct
	protected void initModel() throws IOException {
		
		initBaseModel(request);
		codiceCarnet = request.getParameter("codiceCarnet");
		
	}

	public String getCodiceCarnet() {
		return codiceCarnet;
	}
	
}
