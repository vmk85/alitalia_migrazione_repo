package com.alitalia.aem.consumer.checkinrest.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerSeat;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;
import com.alitalia.aem.consumer.checkinrest.render.PnrListDataRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckInThankYou extends GenericCheckinModel{

    @Self
    private SlingHttpServletRequest request;

    @Inject
    private SlingHttpServletResponse response;

    @Inject
    private AlitaliaConfigurationHolder configuration;

    @Inject
    private CheckinSession checkinSession;

    private Logger logger = LoggerFactory.getLogger(this.getClass());



    @PostConstruct
    protected void initModel() throws Exception {

        //Estendendo la classe GenericCheckInModel abbiamo a disposizione anche il contesto
        //Qui posso controllare i dati di risposta e renderli utilizzabili al FrontEnd AEM
        try{

            super.initBaseModel(request);

            // Setto variabile checkinComplete = true
            ctx.checkinComplete = true;

        }catch (Exception e) {
            logger.error("Unexpected error: ", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

}
