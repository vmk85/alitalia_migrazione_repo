package com.alitalia.aem.consumer.checkinrest.render;

public class SeatRender {

	private String number;

	private String row;

	private String column;

	private String availability;

	private String type;

	private String price;

	private Boolean confortSeat;

	private String passengerId;

	private String occupatoDa;

    private String cssClass;

    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getRow() {

		return row;
	}

	public String getColumn() {
		return column;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public Boolean getConfortSeat() {
		return confortSeat;
	}

	public void setConfortSeat(Boolean confortSeat) {
		this.confortSeat = confortSeat;
	}

	public String getPassengerId() {
		return passengerId;
	}

	public void setPassengerId(String passengerId) {
		this.passengerId = passengerId;
	}

	public String getOccupatoDa() {
		return occupatoDa;
	}

	public void setOccupatoDa(String occupatoDa) {
		this.occupatoDa = occupatoDa;
	}

}
