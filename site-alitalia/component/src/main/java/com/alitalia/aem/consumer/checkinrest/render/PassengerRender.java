package com.alitalia.aem.consumer.checkinrest.render;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerSeat;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerRender {
	
	private String nome;
	private String cognome;
	private String frequentFlyer;
	private String codeFrequentFlyer;
	private String numberFrequentFlyer;
	private String tierLevel;
	private String passengerId;
	private Boolean postoAssegnato;
	private String seatCI;
	private String newSeat;
	private String bookingClass;
	private String infantLastName;
	private String infantFirstName;
	private String passengerType;
	private String pnr;
	private List<PassengerSeat> passengerSeats = null;
	
	private ArrayList<SectorRender> listSectorRender;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getFrequentFlyer() {
		return frequentFlyer;
	}

	public void setFrequentFlyer(String frequentFlyer) {
		this.frequentFlyer = frequentFlyer;
	}

	public String getNumberFrequentFlyer() {
		return numberFrequentFlyer;
	}

	public void setNumberFrequentFlyer(String numberFrequentFlyer) {
		this.numberFrequentFlyer = numberFrequentFlyer;
	}

	public String getCodeFrequentFlyer() {
		return codeFrequentFlyer;
	}

	public void setCodeFrequentFlyer(String codeFrequentFlyer) {
		this.codeFrequentFlyer = codeFrequentFlyer;
	}

	public String getPassengerId() {
		return nome+cognome;
	}

	public void setPassengerId(String passengerId) {
		this.passengerId = passengerId;
	}

	public Boolean getPostoAssegnato() {
		return postoAssegnato;
	}

	public void setPostoAssegnato(Boolean postoAssegnato) {
		this.postoAssegnato = postoAssegnato;
	}

	public String getSeatCI() {
		return seatCI;
	}

	public void setSeatCI(String seatCI) {
		this.seatCI = seatCI;
	}

	public String getNewSeat() {
		return newSeat;
	}

	public void setNewSeat(String newSeat) {
		this.newSeat = newSeat;
	}

	public ArrayList<SectorRender> getListSectorRender() {
		return listSectorRender;
	}

	public void setListSectorRender(ArrayList<SectorRender> listSectorRender) {
		this.listSectorRender = listSectorRender;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getInfantLastName() {
		return infantLastName;
	}

	public void setInfantLastName(String infantLastName) {
		this.infantLastName = infantLastName;
	}

	public String getInfantFirstName() {
		return infantFirstName;
	}

	public void setInfantFirstName(String infantFirstName) {
		this.infantFirstName = infantFirstName;
	}

	public String getPassengerType() {
		return passengerType;
	}

	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}

	public List<PassengerSeat> getPassengerSeats() {
		return passengerSeats;
	}

	public void setPassengerSeats(List<PassengerSeat> passengerSeats) {
		this.passengerSeats = passengerSeats;
	}

	public String getTierLevel() {
		return tierLevel;
	}

	public void setTierLevel(String tierLevel) {
		this.tierLevel = tierLevel;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
}
