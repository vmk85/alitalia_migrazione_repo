package com.alitalia.aem.consumer.booking.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.alitalia.aem.common.data.home.FlightData;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.common.data.home.PassengerApisSecureFlightInfoData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PassengerSecureFlightInfoData;
import com.alitalia.aem.common.data.home.PaymentData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.PaymentProviderGlobalCollectData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GlobalCollectPaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.utils.FlightDataUtils;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.booking.render.GenericPriceRender;
import com.alitalia.aem.consumer.booking.render.RegoleTariffarieTrattaRender;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingEmailHelper extends GenericBaseModel {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final String[] charsToBeReplaced = "ÇüéâäàåçêëèéïîìÄÅÉæÆôöòûùÿÖÜ£Ø×?áíóúñÑªº®¡ÁÂÀ©¢¥ãÃÐÊËÈiÍÎÏÌÓÔÒõÕµÚÛÙýÝ§°¹³²žŽšŠčČćĆ".split("");  
	private static final String[] charsToReplace = "CueaaaaceeeeiiiAAEaAooouuyOUL0xfaiounNaoriAAAccYaADEEEiIIIIOOOoOuUUUyYSo132zZsScCcC".split("");
	private static final String charsToRemove = "[ '`]";
	private static final String defaultLanguage = "it";
	private static final String defaultMarket = "IT";
	private static final String ENCODING = "UTF-8";

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private SlingHttpServletResponse response;
	
	@Inject
    private AlitaliaConfigurationHolder configuration;
	
	// informzioni gereriche per la composizione dei path
	private String hostName = "";
	private String mailImageUrl = "";
	private String marketSiteUrl = "";
	private String mailImageBookingLang = "";

	// booking session context proveniente dalla sessione
	protected BookingSessionContext ctx;

	// nome e cognome del passeggero acquirente
	protected String applicantName;
	protected String applicantSurname;

	// elenco dei voli che compongono le "tratte"
	protected ArrayList<ItineraryDetail> itineraryDetails;

	// array contenente le informazioni di prezzo per ogni "tratta"
	protected GenericPriceRender[] selectedBrandPriceRenderers;

	// array contenente la descrizione della tariffa selezionata per ogni "tratta"
	protected String[] selectedBrandDescriptions;

	// array contenente le note da visualizzare in caso di lightfare
	protected String[][] lightFaresNotes;

	// informazioni sul metodo di pagamento provenienti dalla sessione
	protected PaymentData paymentData;

	// variabili per il conteggio delle varie tipologie di passeggero
	protected int countAdult = 0;
	protected int countChildren = 0;
	protected int countInfant = 0;

	private String cuit = "";
	
	// informazioni di prezzo costruito come mappa
	// nella cui chiave c'è la stringa i18n della voce di riepilogo
	// e nel valore una coppia di stringhe con quantita' (ove applicabile)
	// e importo da visualizzare
	protected LinkedHashMap<String, String[]> pricesList;

	protected String paymentType = "";
	protected String formattedCardNumber = "";
	protected String iconPayMethod = "";
	protected String totaleParteDecimale = "";
	protected String totaleParteIntera = "";
	private int residuoCarnet;
	private String codiceCarnet;
	private String pagamentoCarnet;
	private RegoleTariffarieTrattaRender[] carnetFareRules;
	private String scadenzaCarnet;
	private String passwordCarnet;
	
	private String linkSSWMTO;
	private String langParam;

	private boolean firstLabel = false;

	private boolean secondLabel = false;

	private boolean genericLabel = false;

	@Override
	protected void initBaseModel(SlingHttpServletRequest request) {
		logger.debug("BookingEmailHelper initModel");
		
		super.initBaseModel(request);

		try {
			HttpSession session = request.getSession(true);
			if (session != null) {
				ctx = (BookingSessionContext) session.getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
				paymentData = (PaymentData) session.getAttribute(BookingConstants.PAYMENT_DATA_ATTRIBUTE);
			}
			else {
				ctx = (BookingSessionContext) request.getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
				paymentData = (PaymentData) request.getAttribute(BookingConstants.PAYMENT_DATA_ATTRIBUTE);
			}
		} catch (Exception e) {
			ctx = (BookingSessionContext) request.getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
			paymentData = (PaymentData) request.getAttribute(BookingConstants.PAYMENT_DATA_ATTRIBUTE);
		}

		if ((ctx == null || paymentData == null) && !isWCMEnabled()) {
			logger.error("Cannot find booking session context/payment data in session or request.");
			throw new RuntimeException("Cannot find booking session context/payment data in session or request.");
		}

	}

	@PostConstruct
	protected void initModel() throws IOException {

		try {

			this.initBaseModel(request);

			String prefix = "http://";
			if (!isWCMEnabled() && configuration.getHttpsEnabled())
				prefix = "https://";
			hostName = prefix + configuration.getExternalDomain();
			logger.debug("hostName: " + hostName);

//			mailImageUrl = hostName + "/etc/designs/alitalia/clientlibs/images/";
			mailImageUrl = hostName + configuration.getStaticImagesPath();
			logger.debug("mailImageUrl: " + mailImageUrl);

			this.marketSiteUrl = hostName + AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
			
			linkSSWMTO = configuration.getSSWMTODeepLink();
			
			String lang = this.languageCode !=null ? this.languageCode.toLowerCase() : defaultLanguage;
			String country = this.marketCode != null ? this.marketCode.toUpperCase() : defaultMarket;
			mailImageBookingLang = this.languageCode !=null ? this.languageCode.toLowerCase() : defaultLanguage;
			langParam = lang + "_" + country;
			Map<String, String> sswLang = configuration.getMapProperty(AlitaliaConfigurationHolder.SSW_LANGUAGE_MAPPING);
			if(sswLang.get(lang) != null){
				langParam = sswLang.get(lang);
			}
			else if (configuration.getSetProperty(AlitaliaConfigurationHolder.SSW_NOT_SUPPORTED_LANGUAGES).contains(lang)) {
				lang = configuration.getStringProperty(AlitaliaConfigurationHolder.SSW_DEFAULT_LANGUAGE);
				country = configuration.getStringProperty(AlitaliaConfigurationHolder.SSW_DEFAULT_COUNTRY);
				langParam = lang + "_" + country;
			}

			if (!isWCMEnabled() && ctx != null && ctx.flightSelections != null) {
				this.itineraryDetails = new ArrayList<BookingEmailHelper.ItineraryDetail>();
				for(RouteData itinerary : ctx.prenotation.getRoutesList()) 
					this.itineraryDetails.add(initItineraryInfo(itinerary, ctx.prenotation.getPassengers()));

				this.selectedBrandPriceRenderers = new GenericPriceRender[ctx.flightSelections.length];
				this.selectedBrandDescriptions = new String[ctx.flightSelections.length];
				this.lightFaresNotes = new String[ctx.flightSelections.length][2];
				int i = 0;
				carnetFareRules = computeCarnetFareRules();
				for(FlightSelection flightSelection : ctx.flightSelections) {

					BrandData selectedBrand = AlitaliaUtils.getBrandInfo(
							flightSelection.getFlightData().getBrands()
							, flightSelection.getSelectedBrandCode()
							, configuration.getMinifareSourceBrandCode()
							, configuration.getMinifareTargetBrandCode());

					BigDecimal grossFare = null;
					if (selectedBrand != null) { 
						grossFare = selectedBrand.getGrossFare();
					} else {
						grossFare = new BigDecimal(0);
					}
					GenericPriceRender itineraryPriceRenderer = new GenericPriceRender(grossFare, ctx.currency, ctx);
					this.selectedBrandPriceRenderers[i] = itineraryPriceRenderer;
	
					BrandPageData selectedBrandPageData = ctx.codeBrandMap.get(flightSelection.getSelectedBrandCode().toLowerCase());
					String selectedBrandDescription = "";
					if (selectedBrandPageData != null)
						selectedBrandDescription = selectedBrandPageData.getTitle();
					this.selectedBrandDescriptions[i] = selectedBrandDescription;
					this.lightFaresNotes[i][0] = "";
					this.lightFaresNotes[i][1] = "";
					if (flightSelection.isTariffaLight()) {
						int j = 0;
						if (flightSelection.isOnlyHandBaggage()) {
							this.lightFaresNotes[i][j] = "PLACEHOLDER_STIVA_LIGHT";
							j++;
					}
						this.lightFaresNotes[i][j] = "booking.regoleTariffarie.light.checkinOnline.value";
					}
					if (flightSelection.isBus()) {
						int index = flightSelection.isTariffaLight() ? 1 : 0;
						this.lightFaresNotes[i][index] = "booking.regoleTariffarie.light.checkinOnline.value";
					}
					i++;
				}
				if (!ctx.isCarnetProcess) {
					initFareAndPayment();
				} else {
					initFareAndPaymentCarnet();
				}

				showLabelMail();

				setGenericAsterisk();
			}

		} catch(Exception e) {
			logger.error("Unexpected error: ", e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}

	}

	private void showLabelMail() {
		for(int i = 0;i<ctx.flightSelections.length;i++){
			for(String key : ctx.flightSelections[i].getFareRules().keySet()){
				if(ctx.flightSelections[i].getFareRules().get(key).contains("*")){
					firstLabel = true;
				}
				if(ctx.flightSelections[i].getFareRules().get(key).contains("**")){
					secondLabel = true;
				}
				if(ctx.flightSelections[i].getFareRules().get(key).contains("/")){
				    genericLabel = true;
                }
			}
		}
	}

	private void setGenericAsterisk(){
        for(int i = 0;i<ctx.flightSelections.length;i++){
            for(String key : ctx.flightSelections[i].getFareRules().keySet()){
                if(ctx.flightSelections[i].getFareRules().get(key).contains("/")){
                    Map fareRules = ctx.flightSelections[i].getFareRules();
                    fareRules.put(key,ctx.flightSelections[i].getFareRules().get(key).replace("/","*"));
                    ctx.flightSelections[i].setFareRules(fareRules);
                }
            }
        }
    }

	private RegoleTariffarieTrattaRender[] computeCarnetFareRules() {

		RegoleTariffarieTrattaRender[] carnetFareRules = new RegoleTariffarieTrattaRender[ctx.flightSelections.length];
		for (int i = 0; i < ctx.flightSelections.length; i++) {
			carnetFareRules[i] = new RegoleTariffarieTrattaRender(
					ctx.carnetFareRules, false, i18n, ctx, ctx.flightSelections[i], "");
		}
		return carnetFareRules;
	}

	public String getHostName() {
		return hostName;
	}

	public String getMailImageUrl() {
		return mailImageUrl;
	}

	public String getMarketSiteUrl() {
		return marketSiteUrl;
	}

	public String getPnr() {
		String pnr = "PNR123";

		if (!isWCMEnabled()) 
			pnr = this.ctx.prenotation.getPnr();

		return pnr;
	}

	public String getApplicantName() {
		String applicantName = "applicantName";
		if (!isWCMEnabled())
			applicantName = this.applicantName;
		return applicantName;
	}

	public String getApplicantSurname() {
		String applicantSurname = "applicantSurname";
		if (!isWCMEnabled())
			applicantSurname = this.applicantSurname;
		return applicantSurname;
	}
	
	public String getEncodedApplicantSurname() {
		String encodedApplicantSurname = "encodedApplicantSurname";
		if (!isWCMEnabled()){
			encodedApplicantSurname = "";
			try{
				encodedApplicantSurname = URLEncoder.encode(this.applicantSurname, ENCODING).replace("+", "%20");			
			}
			catch(Exception e){}
		}
		return encodedApplicantSurname;
	}

	public boolean isLegacyMmb() {
		boolean isLegacyMmb = configuration.getLegacyMmbEnabled();
		return isLegacyMmb;
	}

	public Boolean getIsApis() {
		Boolean isApis = isWCMEnabled();
		if (!isWCMEnabled()) {
			isApis = (ctx.isApis == null) ? false : ctx.isApis;
		}
		return isApis;
	}

	public Boolean getIsEsta() {
		Boolean isEsta = isWCMEnabled();
		if (!isWCMEnabled()) {
			isEsta = (ctx.isSecureFlightESTA == null) ? false : ctx.isSecureFlightESTA;
		}
		return isEsta;
	}

	public Boolean getIsLottomatica() {
		Boolean isLottomatica = isWCMEnabled() && 
				(configuration.getDefaultSite().toLowerCase().contains("it_it") || 
						configuration.getDefaultSite().toLowerCase().contains("it/it"));

		if (!isWCMEnabled() && paymentData.getType() != null)
			isLottomatica = (paymentData.getType() == PaymentTypeEnum.LOTTOMATICA);

		return isLottomatica;
	}

	public Boolean getIsTicketOffice() {
		return isWCMEnabled() || (paymentData != null && paymentData.getType() != null 
				&& paymentData.getType() == PaymentTypeEnum.PAY_AT_TO);
	}

	public String getLastDateToPay() {
		String lastDateToPay = "";
		Calendar cal = Calendar.getInstance();
		
		if (!isWCMEnabled())
			cal = Calendar.getInstance(ctx.locale);

		cal.add(Calendar.DATE, 1);
		DateRender dateRender = new DateRender(cal);
		lastDateToPay = dateRender.getDay() + " " + i18n.get(dateRender.getTextMonth()) + " " + dateRender.getYear();

		return lastDateToPay;
	}

	public String getLastDateToPayTicketOffice(){
		String lastDateToPay = "";
		Calendar cal = Calendar.getInstance();
		
		int dayIncrement = configuration.getTicketOfficePaymentDayLimit();;
		if (!isWCMEnabled())
			cal = Calendar.getInstance(ctx.locale);
		cal.add(Calendar.DATE, dayIncrement);
		DateRender dateRender = new DateRender(cal);
		lastDateToPay = dateRender.getDay() + " " + i18n.get(dateRender.getTextMonth()) + " " + dateRender.getYear();

		return lastDateToPay;
	}

	public ArrayList<ItineraryDetail> getItineraryDetails() {
		logger.debug("getItineraryDetails: " + itineraryDetails.toString());
		return itineraryDetails;
	}

	public FlightSelection[] getFlightSelections () {
		return ctx.flightSelections;
	}

	public String getCuit() {
		return cuit;
	}

	public String getCurrency() {
		return ctx.currency;
	}

	public GenericPriceRender[] getSelectedBrandPriceRenderers() {
		return selectedBrandPriceRenderers;
	}

	public String[] getSelectedBrandDescriptions() {
		return selectedBrandDescriptions;
	}

	public String[][] getLightFaresNotes() {
		return lightFaresNotes;
	}

	public boolean isCashMiles() {
		return ctx.isCashMilesApplied;
	}

	public long getUsedMileage() {
		return ctx.cashAndMiles.getUsedMileage().longValue();
	}

	public int getCountAdult() {
		return countAdult;
	}

	public int getCountChildren() {
		return countChildren;
	}

	public int getCountInfant() {
		return countInfant;
	}

	public LinkedHashMap<String, String[]> getPricesList() {
		return pricesList;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public String getFormattedCardNumber() {
		return formattedCardNumber;
	}

	public String getIconPayMethod() {
		return iconPayMethod;
	}

	public String getTotaleParteDecimale() {
		return totaleParteDecimale;
	}

	public String getTotaleParteIntera() {
		return totaleParteIntera;
	}

	public String getMigliaSpese() {
		String migliaSpese = "";

		if (!isWCMEnabled()) {
			if (ctx.award && ctx.totalAwardPrice != null) {
				
				migliaSpese = ctx.totalAwardPrice.toPlainString();
				
			}
		}
		else
		{
			migliaSpese = "20000";
		}

		return migliaSpese;
	}

	public boolean isAward() {
		return ctx.award;
	}
	
	public boolean isCarnet() {
		return ctx.isCarnetProcess;
	}
	
	public int getResiduoCarnet() {
		return residuoCarnet;
	}
	
	public String getCodiceCarnet() {
		return codiceCarnet;
	}
	
	public String getScadenzaCarnet() {
		return scadenzaCarnet;
	}
	
	public String getPasswordCarnet() {
		return passwordCarnet;
	}
	
	public String getPagamentoCarnet(){
		return pagamentoCarnet;
	}
	
	public RegoleTariffarieTrattaRender[] getCarnetFareRules() {
		return carnetFareRules;
	}
	
	public String getLinkSSWMTO() {
		return linkSSWMTO;
	}
	
	public String getLangParam() {
		return langParam;
	}

	private void initFareAndPaymentCarnet() {
		pricesList = new LinkedHashMap<String, String[]>();
		String[] values;
		int totalPax = ctx.searchPassengersNumber.getNumAdults() + 
				ctx.searchPassengersNumber.getNumYoung() + 
				ctx.searchPassengersNumber.getNumChildren() + 
				ctx.searchPassengersNumber.getNumInfants();
		
		DateRender dateRender = new DateRender(ctx.infoCarnet.getExpiryDate(), ctx.market);
		scadenzaCarnet = dateRender.getDay() + i18n.get(dateRender.getTextMonth()) + dateRender.getYear();
		if(ctx.updateCarnetSuccess){
			residuoCarnet = ctx.infoCarnet.getResidualRoutes();
		} else{
		residuoCarnet = ctx.infoCarnet.getResidualRoutes() - (ctx.totalSlices * totalPax);
		}
		
		codiceCarnet = ctx.infoCarnet.getCarnetCode();
		passwordCarnet = ctx.infoCarnet.getPassword();
		
		String totalRoutes  = "";
		try {
			totalRoutes = Integer.toString(ctx.infoCarnet.getTotalRoutes());
		} catch(Exception e) {
			logger.error("Error during cast ctx.infoCarnet.getTotalRoutes().", e);
		}
		pagamentoCarnet = i18n.get("booking.carnet.HaiAcquistato.label", "", totalRoutes);
		String voliLabel = i18n.get("carnet.carrello.voli.label");
		if (ctx.totalSlices == 1) {
			voliLabel = i18n.get("carnet.carrello.volo.label");
		}
		values = new String[]{ctx.searchPassengersNumber.getNumAdults() + " " , Integer.toString(ctx.totalSlices), voliLabel};
		pricesList.put("booking.carrello.tariffaAdulti.label", values);
		
	}

	private void initFareAndPayment() {

		pricesList = new LinkedHashMap<String, String[]>();
		String[] values;

		BigDecimal totalFareAdult = new BigDecimal(0);
		BigDecimal totalFareChildren = new BigDecimal(0);
		BigDecimal totalFareInfant = new BigDecimal(0);
		for ( PassengerBase passenger : ctx.prenotation.getPassengers()) {
			if (passenger instanceof ApplicantPassengerData) {
				// WA in attesa di FIX Sabre
				// totalFareAdult = totalFareAdult.add(((ApplicantPassengerData) passenger).getNetFare());
				if (totalFareAdult.equals(BigDecimal.ZERO)) {
					totalFareAdult = ((ApplicantPassengerData) passenger).getNetFare();
				}
				countAdult++;
				this.applicantName = ((ApplicantPassengerData) passenger).getName();
				this.applicantSurname = ((ApplicantPassengerData) passenger).getLastName();
				continue;
			}
			if (passenger instanceof AdultPassengerData) {
				// WA in attesa di FIX Sabre
				// totalFareAdult = totalFareAdult.add(((AdultPassengerData) passenger).getNetFare());
				if (totalFareAdult.equals(BigDecimal.ZERO)) {
					totalFareAdult = ((AdultPassengerData) passenger).getNetFare();
				}
				countAdult++;
				continue;
			}
			if (passenger instanceof ChildPassengerData) {
				// WA in attesa di FIX Sabre (rimosso add)
				// totalFareChildren = totalFareChildren.add(((ChildPassengerData) passenger).getNetFare());
				if (totalFareChildren.equals(BigDecimal.ZERO)) {
					totalFareChildren = ((ChildPassengerData) passenger).getNetFare();
				}
				countChildren++;
				continue;
			}
			if (passenger instanceof InfantPassengerData) {
				// WA in attesa di FIX Sabre
				// totalFareInfant = totalFareInfant.add(((InfantPassengerData) passenger).getNetFare());
				if (totalFareInfant.equals(BigDecimal.ZERO)) {
					totalFareInfant = ((InfantPassengerData) passenger).getNetFare();
				}
				countInfant++;
			}
		}

		GenericPriceRender prices = new GenericPriceRender(totalFareAdult, totalFareChildren, totalFareInfant, 
				ctx.totalTaxes, ctx.totalExtras, ctx.totalExtraCharges, ctx.grossAmount, ctx.currency, ctx);

		values = new String[]{"", prices.getCurrency() + " " + prices.getTaxes()};
		pricesList.put("booking.carrello.tasse.label", values);

		values = ctx.award ? new String[]{countAdult + " ", prices.getCurrency() + " 0"} :
			new String[]{countAdult + " ", prices.getCurrency() + " " + prices.getFare()};
		pricesList.put("booking.carrello.tariffaAdulti.label", values);

		if (totalFareChildren.compareTo(new BigDecimal(0)) > 0) {
			values = new String[]{countChildren + " ", prices.getCurrency() + " " + prices.getFareChildren()};
			pricesList.put("booking.carrello.tariffaBambini.label", values);
		}

		if (totalFareInfant.compareTo(new BigDecimal(0)) > 0) {
			values = new String[]{countInfant + " ", prices.getCurrency() + " " + prices.getFareInfant()};
			pricesList.put("booking.carrello.tariffaNeonati.label", values);
		}

		values = new String[]{"", prices.getCurrency() + " " + prices.getExtra()};
		pricesList.put("booking.carrello.supplementi.label", values);
		
		if (ctx.ccFeeApplied) {
			BigDecimal ccFeePriceAmount = ctx.ccFeeTotalAmount;
			GenericPriceRender ccFeePriceRender = new GenericPriceRender(ccFeePriceAmount, ctx.currency, ctx);
			values = new String[]{"", ccFeePriceRender.getCurrency() + " " + ccFeePriceRender.getFare()};
			pricesList.put("booking.carrello.ccFee.label", values);
			
		}

		if (ctx.totalExtraCharges.compareTo(new BigDecimal(0)) > 0) {
			values = new String[]{"", prices.getCurrency() + " " + prices.getExtraCharge()};
			pricesList.put("booking.carrello.servizioVendita.label", values);
		}

		if (ctx.isCashMilesReady) {
			GenericPriceRender cashMilesPriceRenderer = new GenericPriceRender(ctx.totalCashMilesPrice, ctx.currency, ctx);
			values = new String[]{"", cashMilesPriceRenderer.getCurrency() + " -" + cashMilesPriceRenderer.getFare()};
			pricesList.put("booking.regoleTariffarie.cashMiles.mail", values);

		}

		if (ctx.isCouponValid != null && ctx.isCouponValid) {
			GenericPriceRender couponPriceRenderer = new GenericPriceRender(ctx.totalCouponPrice, ctx.currency, ctx);
			values = new String[]{"", couponPriceRenderer.getCurrency() + " -" + couponPriceRenderer.getFare()};
			pricesList.put("booking.carrello.coupon.label", values);
		}

		
		DecimalFormatSymbols dfs = new DecimalFormatSymbols(ctx.locale);
		int decimalSeparatorIndex = prices.getTotalPrice().lastIndexOf(dfs.getDecimalSeparator());
		if (decimalSeparatorIndex > 0) {
			totaleParteIntera = prices.getCurrency() + " " + prices.getTotalPrice().substring(0, decimalSeparatorIndex + 1);
			totaleParteDecimale = prices.getTotalPrice().substring(decimalSeparatorIndex + 1);
		}
		else {
			totaleParteIntera = prices.getCurrency() + " " + prices.getTotalPrice();
			totaleParteDecimale = "";
		}

		switch (paymentData.getType()) {
			case CREDIT_CARD:
				if (paymentData.getProvider() instanceof PaymentProviderCreditCardData) {
					PaymentProviderCreditCardData provider = (PaymentProviderCreditCardData) paymentData.getProvider();
					paymentType = provider.getType().value().toLowerCase();
					String cardNumber = provider.getCreditCardNumber();
					int starSize = cardNumber.length() - 4;
					formattedCardNumber = cardNumber.substring(0, starSize).replaceAll("[0-9]", "*");
					formattedCardNumber += cardNumber.substring(starSize);
					switch (provider.getType()) {
					case AMERICAN_EXPRESS:
						iconPayMethod = "carte/AmericanExpress.png";
						break;
					case DINERS:
						iconPayMethod = "carte/Diners.png";
						break;
					case MASTER_CARD:
						iconPayMethod = "carte/MasterCard.png";
						break;
					case UATP:
						iconPayMethod = "carte/Uatp.png";
						break;
					case VISA:
						iconPayMethod = "carte/Visa.png";
						break;
					case VISA_ELECTRON:
						iconPayMethod = "carte/VisaElectron.png";
						break;
					case MAESTRO:
						iconPayMethod = "carte/Maestro.png";
						break;
					default:
						iconPayMethod = "";
						break;
					}
				} else {
					iconPayMethod = "";
				}
				break;
			case FINDOMESTIC:
				paymentType = paymentData.getType().value().toLowerCase();
				iconPayMethod = "altrimetodi/Findomestic.png";
				break;
			case INSTALMENTS_BRAZIL:
				paymentType = paymentData.getType().value().toLowerCase();
				iconPayMethod = "";
				break;
			case LOTTOMATICA:
				paymentType = paymentData.getType().value().toLowerCase();
				iconPayMethod = "altrimetodi/Lottomatica.png";
				break;
			case MASTER_PASS:
				paymentType = paymentData.getType().value().toLowerCase();
				iconPayMethod = "altrimetodi/MasterPass.png";
				break;
			case PAY_PAL:
				paymentType = paymentData.getType().value().toLowerCase();
				iconPayMethod = "altrimetodi/PayPal.png";
				break;
			case POSTE_ID:
				paymentType = paymentData.getType().value().toLowerCase();
				iconPayMethod = "altrimetodi/PosteID.png";
				break;
			case BANCA_INTESA:
				paymentType = paymentData.getType().value().toLowerCase();
				iconPayMethod = "bonifici/BancaIntesa.png";
				break;
			case BANCO_POSTA:
				paymentType = paymentData.getType().value().toLowerCase();
				iconPayMethod = "bonifici/BancoPosta.png";
				break;
			case GLOBAL_COLLECT:
				if (((PaymentProviderGlobalCollectData)paymentData.getProvider()).getType() == GlobalCollectPaymentTypeEnum.MAESTRO){
					paymentType = ((PaymentProviderGlobalCollectData)paymentData.getProvider()).getType().value().toLowerCase();
					iconPayMethod = "bonifici/Maestro.png";
				} else {
					paymentType = paymentData.getType().value().toLowerCase();
					iconPayMethod = "bonifici/GlobalCollect.png";
				}
				break;
			case UNICREDIT:
				paymentType = paymentData.getType().value().toLowerCase();
				iconPayMethod = "bonifici/Unicredit.png";
				break;
			case PAY_AT_TO:
				paymentType = paymentData.getType().value().toLowerCase();
				iconPayMethod = "";
				break;
			default:
				paymentType = paymentData.getType().value().toLowerCase();
				iconPayMethod = "";
				formattedCardNumber = "";
		}
		if (!"".equals(iconPayMethod))
			iconPayMethod = "loghi/" + iconPayMethod;
	}

	private ItineraryDetail initItineraryInfo(RouteData itinerary, List<PassengerBaseData> passengersData) {
		ItineraryDetail itineraryDetail = new ItineraryDetail();
		itineraryDetail.flights = new ArrayList<BookingEmailHelper.FlightDetail>();
		
		switch (itinerary.getType()) {
		case OUTBOUND:
			itineraryDetail.itineraryType = "booking.carrello.andata.label";
			break;
		default:
			itineraryDetail.itineraryType = "booking.carrello.ritorno.label";
		}
		List<PassengerBaseData> paxData = passengersData;
		if (ctx.updateCarnetSuccess) {
			if(ctx.prenotation != null) {
				if (ctx.prenotation.getPassengers() != null) {
					paxData = ctx.prenotation.getPassengers();
				} else {
					logger.info("ctx.prenotation.getPassengers() is NULL");
				}
			} else {
				logger.info("ctx.prenotation is NULL");
			}
		}
		if (itinerary.getFlights().get(0).getFlightType().equals(FlightTypeEnum.DIRECT)) {
			itineraryDetail.stopCount = 0;
			FlightDetail flightDetail = initFlightDetail((DirectFlightData) itinerary.getFlights().get(0), paxData);
			if (ctx.award) {
				flightDetail.cabin = computeCabinAward(ctx, itinerary.getIndex());
			}
			itineraryDetail.flights.add(flightDetail);
		}
		else {
			itineraryDetail.stopCount = ((ConnectingFlightData) itinerary.getFlights().get(0)).getFlights().size() - 1;
			ConnectingFlightData flightData = (ConnectingFlightData) itinerary.getFlights().get(0);
			List<Integer> transitsDurations = FlightDataUtils.computeConnectingFlightsWait(flightData);
			for (int j = 0; j <= itineraryDetail.stopCount; j++) {
				FlightDetail flightDetail = initFlightDetail((DirectFlightData) flightData.getFlights().get(j), paxData);
				if (ctx.award) {
					flightDetail.cabin = computeCabinAward(ctx, itinerary.getIndex());
				}

				if (j < itineraryDetail.stopCount && transitsDurations.get(j) > 0) {
					Integer transitDurationValue = transitsDurations.get(j);
					int transitDurationHours = (new Double(Math.floor(transitDurationValue.doubleValue() / 60))).intValue();
					int transitDurationMinutes = transitDurationValue.intValue() % 60;
					flightDetail.stopDuration = String.valueOf(transitDurationHours)
							+ ":" + String.format("%02d", transitDurationMinutes);
				}
				else
					flightDetail.stopDuration = "";

				itineraryDetail.flights.add(flightDetail);
			}
		}

		return itineraryDetail;
	}

	private FlightDetail initFlightDetail (DirectFlightData flight, List<PassengerBaseData> passengers) {
		FlightDetail flightDetail = new FlightDetail();
		flightDetail.departureDateTime = new DateRender(flight.getDepartureDate(), ctx.market);
		flightDetail.carrier = flight.getCarrier();
		flightDetail.flightNumber = flight.getFlightNumber();
		flightDetail.arrivalDateTime = new DateRender(flight.getArrivalDate(), ctx.market);
		flightDetail.daysDifference = Math.abs(ChronoUnit.DAYS.between(
				LocalDate.of(flight.getDepartureDate().get(Calendar.YEAR), (flight.getArrivalDate().get(Calendar.MONTH) + 1), flight.getArrivalDate().get(Calendar.DAY_OF_MONTH)), 
				LocalDate.of(flight.getArrivalDate().get(Calendar.YEAR), (flight.getDepartureDate().get(Calendar.MONTH) + 1), flight.getDepartureDate().get(Calendar.DAY_OF_MONTH))));
		flightDetail.airportFromName = "airportsData." + flight.getFrom().getCode() + ".name";
		flightDetail.airportFromCode = flight.getFrom().getCode();
		flightDetail.airportFromCity = "airportsData." + flight.getFrom().getCityCode() + ".city";
		flightDetail.airportToName = "airportsData." + flight.getTo().getCode() +".name";
		flightDetail.airportToCode = flight.getTo().getCode();
		flightDetail.airportToCity = "airportsData." + flight.getTo().getCityCode() + ".city";
		flightDetail.cabin = flight.getCabin().value();

        flightDetail.isSoggettoApprovazioneGovernativa=computeIsSoggettoAppGov(obtainFlightNumbers(flight));

//		if (!flight.getCarrier().equals("AZ"))
//			flightDetail.operatedBy = "carriersData." + flight.getCarrier() + ".description";
		flightDetail.operatedBy = "NA";
		if (!flight.getCarrier().equals("AZ")) {
			flightDetail.operatedBy = i18n.get("carriersData." + flight.getCarrier() + ".description");
		} else {
			if (flight.getOperationalFlightText() != null) {
				if (flight.getOperationalFlightText().equals(flight.getOperationalCarrier())) {
					flightDetail.operatedBy = i18n.get("carriersData." + flight.getOperationalCarrier() + ".description");
				} else {
					flightDetail.operatedBy = flight.getOperationalFlightText();
				}
				
			} else if (!flight.getOperationalCarrier().equals("")) {
				flightDetail.operatedBy = i18n.get("carriersData." + flight.getOperationalCarrier() + ".description");
			}
		}

		flightDetail.passengers = new ArrayList<BookingEmailHelper.PassengerDetail>();
		for (PassengerBaseData passengerBaseData : passengers) {
			PassengerDetail passengerDetail = new PassengerDetail();
			passengerDetail.meal = "NA";
			passengerDetail.seat = "NA";

			List<SeatPreferencesData> seatPreferences = null;
			MealData mealPreference = null;
			String frequentFlyerCode = null;
			String frequentFlyerProgram = null;
			if (passengerBaseData instanceof ApplicantPassengerData) {
				seatPreferences = ((ApplicantPassengerData) passengerBaseData).getPreferences().getSeatPreferences();
				mealPreference = ((ApplicantPassengerData) passengerBaseData).getPreferences().getMealType();
				frequentFlyerCode = ((ApplicantPassengerData) passengerBaseData).getFrequentFlyerCode();
				frequentFlyerProgram = (((ApplicantPassengerData) passengerBaseData).getFrequentFlyerType() != null) ?
						((ApplicantPassengerData) passengerBaseData).getFrequentFlyerType().getDescription() :
						"";
				passengerDetail.passengerType = i18n.get("booking.carrello.adulto.label");
				cuit = computeCuit((ApplicantPassengerData) passengerBaseData);
			}
			else if (passengerBaseData instanceof AdultPassengerData) {
				seatPreferences = ((AdultPassengerData) passengerBaseData).getPreferences().getSeatPreferences();
				mealPreference = ((AdultPassengerData) passengerBaseData).getPreferences().getMealType();
				frequentFlyerCode = ((AdultPassengerData) passengerBaseData).getFrequentFlyerCode();
				frequentFlyerProgram = (((AdultPassengerData) passengerBaseData).getFrequentFlyerType() != null) ?
						((AdultPassengerData) passengerBaseData).getFrequentFlyerType().getDescription() :
						"";
				passengerDetail.passengerType = i18n.get("booking.carrello.adulto.label");
			}
			else if (passengerBaseData instanceof ChildPassengerData) {
				seatPreferences = ((ChildPassengerData) passengerBaseData).getPreferences().getSeatPreferences();
				mealPreference = ((ChildPassengerData) passengerBaseData).getPreferences().getMealType();
				passengerDetail.passengerType = i18n.get("booking.carrello.bambino.label");
			}
			else if (passengerBaseData instanceof InfantPassengerData) {
				seatPreferences = null;
				mealPreference = null;
				passengerDetail.passengerType = i18n.get("booking.carrello.neonato.label");
			}
			else {
				seatPreferences = null;
				mealPreference = null;
				passengerDetail.passengerType = i18n.get("booking.carrello.adulto.label");
			}

			if (seatPreferences != null && !seatPreferences.isEmpty()) {
				for (SeatPreferencesData seat : seatPreferences)
					//Confronto tra numeri poichè i numeri di volo possono viaggare in formati
					//diversi. Alcuni formattati con zeri all'inizio.
					//Abbiamo assunto che siano solo numeri ma in caso il catch fa il confronto tra stringhe
					try {
						if (Integer.parseInt(seat.getFlightNumber()) == Integer.parseInt(flight.getFlightNumber())) {
							passengerDetail.seat = seat.getRow().trim() + seat.getNumber().trim();
							break;
						}
					} catch (NumberFormatException e) {
						if (seat.getFlightNumber() != null && seat.getFlightNumber().equals(flight.getFlightNumber())) {
							passengerDetail.seat = seat.getRow().trim() + seat.getNumber().trim();
							break;
						}
					}
			}
			else
				passengerDetail.seat = "NA";

			if (mealPreference != null) {
				passengerDetail.meal = "mealsData." + mealPreference.getCode();
			}
			else
				passengerDetail.meal = "NA";

			passengerDetail.mmCode = (frequentFlyerCode != null && !"".equals(frequentFlyerCode)) ?
					(frequentFlyerProgram + " " + frequentFlyerCode)
					: "";
			passengerDetail.surname = passengerBaseData.getLastName();
			passengerDetail.surnameCanonical = ((StringUtils.replaceEach(passengerDetail.surname, charsToBeReplaced, charsToReplace)
												).replaceAll(charsToRemove, "")
												).toUpperCase();
			passengerDetail.name = passengerBaseData.getName();
			passengerDetail.nameCanonical = ((StringUtils.replaceEach(passengerDetail.name, charsToBeReplaced, charsToReplace)
											).replaceAll(charsToRemove, "")
											).toUpperCase();
			
			/*add second name, if present*/
			passengerDetail.secondName = "";
			passengerDetail.secondNameCanonical = "";
			if(passengerBaseData.getInfo() instanceof PassengerSecureFlightInfoData){
				passengerDetail.secondName = ((PassengerSecureFlightInfoData) passengerBaseData.getInfo()).getSecondName();
			}
			else if(passengerBaseData.getInfo() instanceof PassengerApisSecureFlightInfoData){
				passengerDetail.secondName = ((PassengerApisSecureFlightInfoData) passengerBaseData.getInfo()).getSecondName();
			}
			if(passengerDetail.secondName != null && passengerDetail.secondName.replaceAll(" ", "").length() > 0){
				passengerDetail.secondNameCanonical = " " + (StringUtils.replaceEach(passengerDetail.secondName, charsToBeReplaced, charsToReplace)
						).toUpperCase();
			}
					
			passengerDetail.setTicketNumber(computeTicket(passengerBaseData));
			logger.debug("MAIL TicketNumber: ["+computeTicket(passengerBaseData)+"]");
			flightDetail.passengers.add(passengerDetail);
		}
		
		return flightDetail;
	}


	private Boolean computeIsSoggettoAppGov(String numeroVolo) {
		String[] arrayVolo =configuration.getListaVoliSoggettiApprovazioneGovernativa().split(";");
		for (int i=0; i<arrayVolo.length; i++){
			if (arrayVolo[i].equals(numeroVolo)) {return true;}
		}
		return false;
	}

	private String obtainFlightNumbers(FlightData flightData) {
		String result = "";
		//FlightData flightData = flightSelection.getFlightData();
		if (flightData instanceof DirectFlightData) {
			DirectFlightData directFlightData = (DirectFlightData) flightData;
			return directFlightData.getCarrier() + directFlightData.getFlightNumber();
		} else {
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
			for (FlightData flight : connectingFlightData.getFlights()) {
				DirectFlightData directFlightData = (DirectFlightData) flight;
				String number = directFlightData.getCarrier() + directFlightData.getFlightNumber();
				if (!result.equals("")) {
					result += ", " + number;
				} else {
					result = number;
				}
			}
		}
		return result;
	}

	private String computeCuit(ApplicantPassengerData applicantPassenger) {
		String cuit = "";
		String cuitValue = ctx.cuit;
		if (cuitValue != null) {
			switch (applicantPassenger.getArTaxInfoType()) {
				case CUIL:
					cuit = " - " + i18n.get("booking.passengersData.cuil.label") +
						" " + cuitValue;
					break;
				case CUIT:
					cuit = " - " + i18n.get("booking.passengersData.cuit.label") +
					" " + cuitValue;
					break;
				case NONE:
					cuit = "";
					break;
				case NOT_AR:
					cuit = " - " + i18n.get("booking.passengersData.altraNazionalita.label") +
							" " + i18n.get("countryData." + cuitValue);
					break;
				default:
					cuit = "";
					break;
			
			}
		}
		return cuit;
	}
	
	private String computeCabinAward(BookingSessionContext ctx, Integer i) {
		if (i != null && ctx.flightSelections != null && 
				ctx.flightSelections[i] != null && ctx.flightSelections[i].getSelectedBrandCode() != null) {
			if (ctx.flightSelections[i].getSelectedBrandCode().contains("Business")) {
				return "Business";
			} else {
				return "Economy";
			}
		}
		return "";
	}

	/**
	 * Used to obtain ticketnumber or notification carnet email
	 * @param passengers
	 * @param i
	 * @return
	 */
	private String computeTicket(PassengerBaseData pax) {
		String ticket = "";
		int j = 0;
		HashMap<String,Integer> ticketMap = new HashMap<String,Integer>();
		if (pax.getTickets() != null) {
			for (TicketInfoData tkt : pax.getTickets()) {
				Integer ticketIndex = ticketMap.get(tkt.getTicketNumber());
				if (ticketIndex == null) {
					ticketMap.put(tkt.getTicketNumber(), j);
					j++;
				}
			}
			int k = 0;
			for (String ticketNumber : ticketMap.keySet()) {
				if (k==0) {
					ticket = ticketNumber;
				} else {
					ticket += " " + ticketNumber;
				}
				k++;
			}
		}
		return ticket;
	}

	public String getTicketOfficeRTEContentWithDate() {
		String ticketOfficeRTEContentWithDate = "";
		try{
			String rteContent = AlitaliaUtils.getPage(request.getResource()).getContentResource().getChild("mail-conferma-prenotazione").getChild(
					"rich-text-ticketoffice").getValueMap().get("text", String.class);
		if(rteContent != null){
				if(!isWCMEnabled() && PaymentTypeEnum.PAY_AT_TO.equals(paymentData.getType())){
					Calendar cal = Calendar.getInstance(ctx.locale);
					int dayIncrement = configuration.getTicketOfficePaymentDayLimit();
					cal.add(Calendar.DATE, dayIncrement);
					DateRender dateRender = new DateRender(cal);
					String data = dateRender.getExtendedDate();
					ticketOfficeRTEContentWithDate =rteContent.replaceAll("\\{\\}", data);
			}
		}
		}
		catch(Exception e){
			
		}
		return ticketOfficeRTEContentWithDate;
	}

	public boolean isFirstLabel() {
		return firstLabel;
	}

	public boolean isSecondLabel() {
		return secondLabel;
	}

    public boolean isGenericLabel() {
        return genericLabel;
    }

	public String getMailImageBookingLang() {
		return mailImageBookingLang;
	}

	public void setMailImageBookingLang(String mailImageBooking) {
		this.mailImageBookingLang = mailImageBooking;
	}

	public class PassengerDetail {
		protected String surname;
		protected String surnameCanonical;
		protected String name;
		protected String nameCanonical;
		protected String secondNameCanonical;
		protected String secondName;
		protected String mmCode;
		protected String seat;
		protected String meal;
		protected String passengerType;
		protected String ticketNumber;

		public String getSurname() {
			return surname;
		}

		public String getSurnameCanonical() {
			return surnameCanonical;
		}

		public String getName() {
			return name;
		}

		public String getNameCanonical() {
			return nameCanonical;
		}

		public String getSecondName() {
			return secondName;
		}

		public String getSecondNameCanonical() {
			return secondNameCanonical;
		}

		public String getMmCode() {
			return mmCode;
		}

		public String getSeat() {
			return seat;
		}

		public String getMeal() {
			return meal;
		}

		public String getPassengerType() {
			return passengerType;
		}

		public String getTicketNumber() {
			return ticketNumber;
		}

		public void setTicketNumber(String ticketNumber) {
			this.ticketNumber = ticketNumber;
		}

		public String getXslTicketExpression() {
			String xslTicketExpression = "//a:rsPax[a:Name = '" + 
						this.nameCanonical +"' and a:Surname = '" + 
						this.surnameCanonical + "']/a:Tickets/a:rsTicket[generate-id() = generate-id(key('distE-Ticket', substring(a:Number,1,13)))]";
					
			return xslTicketExpression;
		}

		@Override
		public String toString() {
			return "PassengerDetail [surname=" + surname
					+ ", surnameCanonical=" + surnameCanonical + ", name="
					+ name + ", nameCanonical=" + nameCanonical + ", mmCode="
					+ mmCode + ", seat=" + seat + ", meal=" + meal
					+ ", passengerType=" + passengerType + "]";
		}
	}

	public class FlightDetail {
		protected DateRender departureDateTime;
		protected String carrier;
		protected String flightNumber;
		protected DateRender arrivalDateTime;
		protected long daysDifference;
		protected String airportFromName;
		protected String airportFromCode;
		protected String airportFromCity;
		protected String airportToName;
		protected String airportToCode;
		protected String airportToCity;
		protected String cabin;
		protected String operatedBy;
		protected String stopDuration;
		protected ArrayList<PassengerDetail> passengers;
		protected Boolean isSoggettoApprovazioneGovernativa;

		public Boolean getIsSoggettoApprovazioneGovernativa() {return isSoggettoApprovazioneGovernativa;}

		public DateRender getDepartureDateTime() {
			return departureDateTime;
		}

		public String getCarrier() {
			return carrier;
		}

		public String getFlightNumber() {
			return flightNumber;
		}

		public DateRender getArrivalDateTime() {
			return arrivalDateTime;
		}

		public long getDaysDifference() {
			return daysDifference;
		}

		public String getAirportFromName() {
			return airportFromName;
		}

		public String getAirportFromCode() {
			return airportFromCode;
		}

		public String getAirportFromCity() {
			return airportFromCity;
		}

		public String getAirportToName() {
			return airportToName;
		}

		public String getAirportToCode() {
			return airportToCode;
		}

		public String getAirportToCity() {
			return airportToCity;
		}

		public String getCabin() {
			return cabin;
		}

		public String getOperatedBy() {
			return operatedBy;
		}

		public String getStopDuration() {
			return stopDuration;
		}

		public ArrayList<PassengerDetail> getPassengers() {
			return passengers;
		}

		@Override
		public String toString() {
			return "FlightDetail [departureDateTime=" + departureDateTime
					+ ", carrier=" + carrier + ", flightNumber=" + flightNumber
					+ ", arrivalDateTime=" + arrivalDateTime
					+ ", daysDifference=" + daysDifference
					+ ", airportFromName=" + airportFromName
					+ ", airportFromCode=" + airportFromCode
					+ ", airportFromCity=" + airportFromCity
					+ ", airportToName=" + airportToName + ", airportToCode="
					+ airportToCode + ", airportToCity=" + airportToCity
					+ ", cabin=" + cabin + ", operatedBy=" + operatedBy
					+ ", stopDuration=" + stopDuration
					+ ", isSoggettoApprovazioneGovernativa" + isSoggettoApprovazioneGovernativa
					+ ", passengers=" + passengers + "]";
		}
	}

	public class ItineraryDetail {
		protected String itineraryType;
		protected int stopCount;
		protected ArrayList<FlightDetail> flights;

		public String getItineraryType() {
			return itineraryType;
		}

		public int getStopCount() {
			return stopCount;
		}

		public ArrayList<FlightDetail> getFlights() {
			return flights;
		}

		@Override
		public String toString() {
			return "ItineraryDetail [itineraryType=" + itineraryType
					+ ", stopCount=" + stopCount + ", flights=" + flights + "]";
		}
	}
}
