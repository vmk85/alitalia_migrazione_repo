package com.alitalia.aem.consumer.servlet;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "translatemessages" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class TranslateMessagesServlet extends SlingAllMethodsServlet {

	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		logger.debug("[TranslateMessagesServlet] doPost");

		// set content type
		response.setContentType("application/json");
		
		// get messages to translate
		String[] messages = request.getParameterValues("messages");

		// trying to create a JSON response
		try {

			// get a new JSON writer
			TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
			
			// get i18n translator
			Locale locale = AlitaliaUtils.findResourceLocale(
					request.getResource());
			ResourceBundle resourceBundle = request.getResourceBundle(locale);
			I18n i18n = new I18n(resourceBundle);

			// write response
			json.object();
			json.key("messages");
			if (null != messages) {
				json.object();
				for (int i = 0; i < messages.length; i++) {
					json.key(messages[i]).value(i18n.get(messages[i]));
				}
				json.endObject();
			}
			json.endObject();
		}

		// an error occurred during the creation of JSON response
		catch (JSONException e) {
			logger.debug("[TranslateMessagesServlet] error on JSON response", e);
		}

	}

}