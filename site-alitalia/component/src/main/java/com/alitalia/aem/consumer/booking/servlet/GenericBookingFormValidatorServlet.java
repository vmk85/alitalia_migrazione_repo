package com.alitalia.aem.consumer.booking.servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;

@SuppressWarnings("serial")
public abstract class GenericBookingFormValidatorServlet
		extends GenericFormValidatorServlet {
	
	/**
	 * Utility method to access the BookingSessionContext instance.
	 * 
	 * <p>If the instance is not availble, an exception is thrown.</p>
	 * 
	 * @param request The related servlet request.
	 * @return The BookingSessionContext instance.
	 */
	protected BookingSessionContext getBookingSessionContext(SlingHttpServletRequest request) {
		BookingSessionContext ctx = (BookingSessionContext) 
				request.getSession(true).getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
		if (ctx == null) {
			throw new GenericFormValidatorServletException(false, "Booking session context not found.");
		} else {
			return ctx;
		}
	}
	
	/**
	 * Provide booking-specific unrecoverable error page.
	 */
	@Override
	protected String getUnrecoverableErrorRedirect(SlingHttpServletRequest request, 
			SlingHttpServletResponse response, GenericFormValidatorServletException exception) {
		
		BookingSessionContext ctx = (BookingSessionContext) 
				request.getSession(true).getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
		if (ctx != null && ctx.award) {
			return AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
					+ getConfiguration().getBookingAwardFailurePage();
		}
		else {
			return AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
					+ getConfiguration().getBookingFailurePage();
			
		}
		
	}
	
	/**
	 * Provide an empty implementation, should be never used as
	 * booking servlets should always be invoked with ajax requests
	 */
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		logger.warn("The emtpy saveDataIntoSession method was unexpectedly invoked.");
	}
	
	/**
	 * Abstract method to be implemented in subclasses in order to
	 * provide the configuration holder component instance.
	 * 
	 * @return The AlitaliaConfigurationHolder instance.
	 */
	protected abstract AlitaliaConfigurationHolder getConfiguration();
	
}
