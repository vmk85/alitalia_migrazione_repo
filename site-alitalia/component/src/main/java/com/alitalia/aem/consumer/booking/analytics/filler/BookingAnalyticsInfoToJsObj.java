package com.alitalia.aem.consumer.booking.analytics.filler;

import com.alitalia.aem.consumer.booking.analytics.AnalyticsInfo;

public class BookingAnalyticsInfoToJsObj extends BookingAwardAnalyticsInfoToJsObj {

	public BookingAnalyticsInfoToJsObj() {
		initFormats();
	}
	
	@Override
	public String toJSObj(AnalyticsInfo info, boolean trailingSemicolon) {
		String js = super.toJSObj(info, trailingSemicolon);
		
		/*add BookingStep to the head*/
		if(js.indexOf("{") >= 0){
			js = js.replaceFirst("\\{", "{'BookingStep': '" + info.step + "',");
		}
		return js;
	}

}
