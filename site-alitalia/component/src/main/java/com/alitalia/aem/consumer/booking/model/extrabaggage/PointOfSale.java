package com.alitalia.aem.consumer.booking.model.extrabaggage;

// <summary>
/** Enumerazione per le tipologie di point of sale
*/
public enum PointOfSale
{
	IT,
	COM,
	GB,
	US,
	CA,
	ES,
	DE,
	BE,
	NL,
	IE,
	FR,
	DK,
	UK,
	SE,
	GR,
	CH,
	PL,
	RU,
	AR,
	AP,
	BR,
	TR,
	JP,
	CN,
	HU,
	MA,
	TN,
	LB,
	RO,
	BG,
	EG,
	UA;

	public static final int SIZE = java.lang.Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static PointOfSale forValue(int value)
	{
		return values()[value];
	}
}