package com.alitalia.aem.consumer.checkin;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.*;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.AncillaryOffers;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPass;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPassInfo;
import com.alitalia.aem.common.data.checkinrest.model.boardingpassprintable.BoardingPassPrintable;
import com.alitalia.aem.common.data.checkinrest.model.cheangeseats.response.ComfortSeat;
import com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.response.Outcome;
import com.alitalia.aem.common.data.checkinrest.model.fasttrack.response.FastTrack;
import com.alitalia.aem.common.data.checkinrest.model.initpayment.request.FraudNetParam;
import com.alitalia.aem.common.data.checkinrest.model.insurance.InsurancePolicy;
import com.alitalia.aem.common.data.checkinrest.model.lounge.response.Lounge;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Flight;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra;
import com.alitalia.aem.common.data.checkinrest.model.reprintbp.FreeTextInfoList;
import com.alitalia.aem.common.data.checkinrest.model.setbaggage.response.Baggage;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.data.home.PaymentTypeItemData;
import com.alitalia.aem.common.data.home.StateData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryCartData;
import com.alitalia.aem.common.data.home.checkin.CheckinInsurancePolicyData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.messages.checkinrest.CheckinInitPaymentRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinInitPaymentResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinSelectedPassengerRequest;
import com.alitalia.aem.consumer.checkin.render.CheckinSeatMapFlight;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrRender;
import com.day.cq.i18n.I18n;
import org.apache.sling.api.SlingHttpServletRequest;

/**
 * The <code>CheckinSessionContext</code> class contains the state of a checkin
 * session, in order to be stored in the proper way (e.g. in HTTP session).
 * 
 * </p>It is meant to be initialized by the <code>CheckinSession</code>
 * component service, and then provided to it as requested for CheckinSession
 * process management methods.</p>
 * 
 * <p>The values managed in the context shouldn't be modified
 * externally to the <code>CheckinSession</code> component service.</p>
 */
public class CheckinSessionContext {
	public List<CheckinPassengerData> passengersRouteList;
	public I18n i18n;
	
	public String tripId;
	
	public String language;

	public String conversationId;

	public String passengerName;
	
	public String passengerSurname;

	public void setPassengerCheckinSearch_CheckinPerformed(boolean passengerCheckinSearch_CheckinPerformed) {
		this.passengerCheckinSearch_CheckinPerformed = passengerCheckinSearch_CheckinPerformed;
	}

	public boolean isPassengerCheckinSearch_CheckinPerformed() {
		return passengerCheckinSearch_CheckinPerformed;
	}

	public boolean passengerCheckinSearch_CheckinPerformed;

	public void setallPassengersCheckinCompleted(boolean allPassengersCheckinCompleted) {
		this.allPassengersCheckinCompleted = allPassengersCheckinCompleted;
	}

	public boolean allPassengersCheckinCompleted;

	public String MMcode;
	
	public String pin;
	
	public String searchType;
	
	public List<CheckinRouteData> routesList;
	
	public String serviceClient;
	
	public String machineName;
	
	public String sid;
	
	public String callerIp;
	
	public String market;
	
	public String site;
	
	public String currencyCode;
	
	public String currencySymbol;

	public NumberFormat currentNumberFormat;
	
	public Locale locale;
	
	/* trip data */
	
	public String pnr;

	public String eTicket;
	
	public List<CheckinPassengerData> passengers;
	
	public List<CheckinPassengerData> selectedPassengers;
	
	public int[] passengersAdditionalInfo;
	
	public List<FrequentFlyerTypeData> frequentFlyerTypes;

	public String totalPay;

	/**
	 * Selected root for check-in operation
	 */
	public CheckinRouteData selectedRoute;
	
	/**
	 * Index of selected root
	 */
	public int indexRoute;
	
	/**
	 * It indicates whether the flight is apis basic
	 */
	public boolean apisBasic;
	
	/**
	 * It indicates whether the flight is apis plus
	 */
	public boolean apisPlus;
	
	/**
	 * Index of the current passenger in passengers phase
	 */
	public int passengerIndex;
	
	/**
	 * It indicates whether the customer is in the apis data phase
	 */
	public boolean apisPhase;
	
	/**
	 * It indicates whether additional info is required for the current passenger
	 */
	public boolean additionalInfoPhase;
	
	/**
	 * It indicates whether additional info is required for the current passenger (included destination info)
	 */
	public boolean additionalInfoPhaseCase2;
	
	/**
	 * It indicates whether additional info and Green Card or Visto document are required for the current passenger
	 */
	public boolean additionalInfoPhaseCase3;
	
	/**
	 * It indicates whether additional info and  ESTA or Green Card or Visto document are required for the current passenger
	 */
	public boolean additionalInfoPhaseCase4;
	
	/* check-in */
	
	public List<CheckinPassengerData> notCheckedInPassengers;
	
	public boolean checkinDone;
	
	/* ancillary */
	
	public CheckinAncillaryCartData cart;

	public List<MmbAncillaryErrorData> cartErrors;

	public boolean cartInitialized;

	public boolean ancillaryPaymentSuccess;

	public BigDecimal cartTotalAmount;
	
	public BigDecimal cartTotalAncillariesAmount;
	
	public Map<String, CheckinSeatMapFlight> seatMaps;
	
	public CheckinInsurancePolicyData insurancePolicyData;
	
	public boolean insuranceAddedToCart;
	
	public boolean insurancePaymentSuccess;
	
	public String paymentCreditCardNumberHide;
	
	public String paymentCreditCardType;
	
	public boolean comfortSeatAvailable;
	
	/* boarding pass */
	
	public boolean boardingPassHasPDF;
	
	public boolean boardingPassHasEmail;
	
	public boolean boardingPassHasSMS;
	
	public boolean boardingPassIsDelayed;
	
	public List<CheckinPassengerData> acceptedPassengers;
	
	public boolean boardingPassPDFRequested;
	
	public boolean boardingPassEmailRequested;
	
	public boolean boardingPassSMSRequested;
	
	public int boardingPassTYP; // 0 regular, 1 airport, 2 empty

	/* Payment */
	
	public String paymentErrorMessage;
	
	public List<PaymentTypeItemData> creditCards;
	
	public String defaultCardNumber;
	
	public String defaultCardType;
	
	public Integer defaultCardExpireMonth;
	
	public Integer defaultCardExpireYear;

	public String confirmationMailAddress;

	public List<CountryData> countries;

	public List<StateData> countriesUSA;
	
	public boolean isBus;

	public boolean isSelectedRouteBus;
	
	public List<String> newSeats;

	public String messageError;

	public Boolean isError;

	public String sabreStatusCode;

//	public PnrListInfoByPnrSearch pnrListData;
//
//	public PnrListInfoByTicketSearch pnrListTicketData;
//
//	public PnrListInfoByFrequentFlyerSearch pnrListFFData;

	public FreeTextInfoList freeTextInfoList;

	public BoardingPassPrintable isbpprintable;

	public BoardingPass boardingPass;

	public AncillaryOffers ancillaryOffers;

	public FastTrack fastTrack;

	public Lounge lounge;

	public Baggage baggage;

	public InsurancePolicy insurancePolicy;

	public String frequentFlyer;

	public Map<Integer, Map<String, Flight>> flightsDetail;

	public BoardingPassInfo boardingPassInfo;

	public List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Pnr> pnrListData;

	public List<FlightsRender> selectedFlights;

	public List<PnrRender> pnrSelectedListDataRender;

	public List<PnrRender> pnrListDataRender;

	public List<Passenger> checkinSelectedPassengers;

	public List<PassengerExtra> checkinSelectedPassengersExtra;

	public HashMap<String, CheckinSelectedPassengerRequest> originalDataPassengers;

	public List<Passenger> listoriginalDataPassengers;

	public String domain;

	public List<FraudNetParam> listFraudNetParam;

	public CheckinInitPaymentRequest checkinInitPaymentRequest;

	public CheckinInitPaymentResponse checkinInitPaymentResponse;

	public boolean checkinPaymentDone = false;

	public String redirectUrl;

	public SlingHttpServletRequest request;

	public String paymentErrorMsg;

	public boolean comfortSeatInCart = false;

	public boolean checkinComplete = false;

	public boolean standBy = false;

	public List<ComfortSeat> comfortSeat;

	@Override
	public String toString() {
		return "CheckinSessionContext{" +
				"passengersRouteList=" + passengersRouteList +
				", i18n=" + i18n +
				", tripId='" + tripId + '\'' +
				", language='" + language + '\'' +
				", conversationId='" + conversationId + '\'' +
				", passengerName='" + passengerName + '\'' +
				", passengerSurname='" + passengerSurname + '\'' +
				", passengerCheckinSearch_CheckinPerformed='" + passengerCheckinSearch_CheckinPerformed + '\'' +
				", MMcode='" + MMcode + '\'' +
				", pin='" + pin + '\'' +
				", searchType='" + searchType + '\'' +
				", routesList=" + routesList +
				", serviceClient='" + serviceClient + '\'' +
				", machineName='" + machineName + '\'' +
				", sid='" + sid + '\'' +
				", callerIp='" + callerIp + '\'' +
				", market='" + market + '\'' +
				", site='" + site + '\'' +
				", currencyCode='" + currencyCode + '\'' +
				", currencySymbol='" + currencySymbol + '\'' +
				", currentNumberFormat=" + currentNumberFormat +
				", locale=" + locale +
				", pnr='" + pnr + '\'' +
				", eTicket='" + eTicket + '\'' +
				", passengers=" + passengers +
				", selectedPassengers=" + selectedPassengers +
				", passengersAdditionalInfo=" + Arrays.toString(passengersAdditionalInfo) +
				", frequentFlyerTypes=" + frequentFlyerTypes +
				", selectedRoute=" + selectedRoute +
				", indexRoute=" + indexRoute +
				", apisBasic=" + apisBasic +
				", apisPlus=" + apisPlus +
				", passengerIndex=" + passengerIndex +
				", apisPhase=" + apisPhase +
				", additionalInfoPhase=" + additionalInfoPhase +
				", additionalInfoPhaseCase2=" + additionalInfoPhaseCase2 +
				", additionalInfoPhaseCase3=" + additionalInfoPhaseCase3 +
				", additionalInfoPhaseCase4=" + additionalInfoPhaseCase4 +
				", notCheckedInPassengers=" + notCheckedInPassengers +
				", checkinDone=" + checkinDone +
				", cart=" + cart +
				", cartErrors=" + cartErrors +
				", cartInitialized=" + cartInitialized +
				", ancillaryPaymentSuccess=" + ancillaryPaymentSuccess +
				", cartTotalAmount=" + cartTotalAmount +
				", cartTotalAncillariesAmount=" + cartTotalAncillariesAmount +
				", seatMaps=" + seatMaps +
				", insurancePolicyData=" + insurancePolicyData +
				", insuranceAddedToCart=" + insuranceAddedToCart +
				", insurancePaymentSuccess=" + insurancePaymentSuccess +
				", paymentCreditCardNumberHide='" + paymentCreditCardNumberHide + '\'' +
				", paymentCreditCardType='" + paymentCreditCardType + '\'' +
				", comfortSeatAvailable=" + comfortSeatAvailable +
				", boardingPassHasPDF=" + boardingPassHasPDF +
				", boardingPassHasEmail=" + boardingPassHasEmail +
				", boardingPassHasSMS=" + boardingPassHasSMS +
				", boardingPassIsDelayed=" + boardingPassIsDelayed +
				", acceptedPassengers=" + acceptedPassengers +
				", boardingPassPDFRequested=" + boardingPassPDFRequested +
				", boardingPassEmailRequested=" + boardingPassEmailRequested +
				", boardingPassSMSRequested=" + boardingPassSMSRequested +
				", boardingPassTYP=" + boardingPassTYP +
				", paymentErrorMessage='" + paymentErrorMessage + '\'' +
				", creditCards=" + creditCards +
				", defaultCardNumber='" + defaultCardNumber + '\'' +
				", defaultCardType='" + defaultCardType + '\'' +
				", defaultCardExpireMonth=" + defaultCardExpireMonth +
				", defaultCardExpireYear=" + defaultCardExpireYear +
				", confirmationMailAddress='" + confirmationMailAddress + '\'' +
				", countries=" + countries +
				", countriesUSA=" + countriesUSA +
				", isBus=" + isBus +
				", isSelectedRouteBus=" + isSelectedRouteBus +
				", newSeats=" + newSeats +
				", messageError='" + messageError + '\'' +
				", isError=" + isError +
				", sabreStatusCode='" + sabreStatusCode + '\'' +
				", freeTextInfoList=" + freeTextInfoList +
				", isbpprintable=" + isbpprintable +
				", boardingPass=" + boardingPass +
				", ancillaryOffers=" + ancillaryOffers +
				", fastTrack=" + fastTrack +
				", lounge=" + lounge +
				", baggage=" + baggage +
				", insurancePolicy=" + insurancePolicy +
				", frequentFlyer='" + frequentFlyer + '\'' +
				", flightsDetail=" + flightsDetail +
				", boardingPassInfo=" + boardingPassInfo +
				", pnrListData=" + pnrListData +
				", selectedFlights=" + selectedFlights +
				", pnrSelectedListDataRender=" + pnrSelectedListDataRender +
				", pnrListDataRender=" + pnrListDataRender +
				", checkinSelectedPassengers=" + checkinSelectedPassengers +
				", checkinSelectedPassengersExtra=" + checkinSelectedPassengersExtra +
				", originalDataPassengers=" + originalDataPassengers +
				", listoriginalDataPassengers=" + listoriginalDataPassengers +
				", domain='" + domain + '\'' +
				", listFraudNetParam=" + listFraudNetParam +
				", checkinInitPaymentRequest=" + checkinInitPaymentRequest +
				", checkinInitPaymentResponse=" + checkinInitPaymentResponse +
				", checkinPaymentDone=" + checkinPaymentDone +
				", redirectUrl='" + redirectUrl + '\'' +
				", comfortSeatInCart=" + comfortSeatInCart +
				", totalPay=" + totalPay +
				", numberOfComfortSeat=" + comfortSeat +
				'}';
	}
}
