package com.alitalia.aem.consumer.booking.servlet;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.CashAndMilesData;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.global.i18n.I18nKeyBooking;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.CashAndMilesCountryTable;
import com.alitalia.aem.consumer.utils.ValidationUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingcashmilesconsumer" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class BookingCashAndMilesServlet extends GenericBookingFormValidatorServlet {
	
	private static final String ATTR_MILES_USED = "milesUsed";
	private static final String ATTR_CASH_AND_MILES_ACTION= "_cashAndMiles_action";
//	private static final String [] AZ_CARRIER = {"AZ","CT","XM","VE"};
	private static final List<String> AZ_CARRIER = Arrays.asList("AZ","CT","XM","VE");

	@Reference
	private AlitaliaConfigurationHolder alitaliaConfiguration;
	
	@Reference
	private BookingSession bookingSession;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		Validator validator = new Validator();
		
		String milesUsed = request.getParameter(ATTR_MILES_USED);
		BookingSessionContext ctx = getBookingSessionContext(request);
		MMCustomerProfileData user = AlitaliaUtils.getAuthenticatedUser(request);
		
		validator.addDirectConditionMessagePattern(ATTR_MILES_USED, milesUsed,
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		boolean isLoggedIn = user != null;
		if(isLoggedIn){
			validator.addCrossCondition("milesUsed", user.getPointsRemainingTotal().toString(), milesUsed, I18nKeyBooking.MESSAGE_ERROR_TOO_MANY_MILES_USED, "isMoreThan");
		}
		
		boolean isAZ = ValidationUtils.checkAllFlights(ctx, ValidationUtils.isSpecificCarrier(AZ_CARRIER));
		boolean isNotLight = ValidationUtils.checkAllFlights(ctx, ValidationUtils.isNotLight());
		
		/*validazioni fittizie necessarie per gli errori 
		 * di validazione dello stato del Booking Session Context*/
		if(!isLoggedIn)
			validator.addDirectCondition("authentication", "",  I18nKeyBooking.MESSAGE_ERROR_AUTHENTICATION_REQUIRED, "isNotEmpty");
		if(!isAZ)
			validator.addDirectCondition("flights", "",  I18nKeyBooking.MESSAGE_ERROR_FLIGHTS_CARRIER_NOT_AZ, "isNotEmpty");
		if(!isNotLight)
			validator.addDirectCondition("flights", "",  I18nKeyBooking.MESSAGE_ERROR_FLIGHTS_BRAND_LIGHT, "isNotEmpty");
		
		
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws Exception {
		Integer milesUsed = Integer.parseInt(request.getParameter(ATTR_MILES_USED));
		String action = request.getParameter(ATTR_CASH_AND_MILES_ACTION);
		
		BookingSessionContext ctx = getBookingSessionContext(request);
		if("cancel".equals(action)){
			bookingSession.clearCashAndMiles(ctx);
		}
		else{
			BigDecimal discountAmount = new BigDecimal((int) (CashAndMilesCountryTable.getInstance().
					getCashMilesRatio(AlitaliaUtils.getRepositoryPathMarket(request.getResource()))
					* milesUsed));
			MMCustomerProfileData customer =  AlitaliaUtils.getAuthenticatedUser(request);
			if(customer == null){
				throw new GenericFormValidatorServletException(true, "Login needed for apply cash and miles");
			}
			bookingSession.prepareCashAndMiles(ctx, milesUsed, discountAmount, customer);
		}
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		json.key("result").value("OK");
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return alitaliaConfiguration;
	}
	
}