package com.alitalia.aem.consumer.millemiglia.render;

import java.util.Comparator;


public class EstrattoContoRenderComparator implements Comparator<EstrattoContoRender>{
	private static EstrattoContoRenderComparator instance;
	private boolean increasingOrder = false;
	
	private EstrattoContoRenderComparator() {}
	
	@Override
	public int compare(EstrattoContoRender a, EstrattoContoRender b) {
		int result = a.getActivity().getActivityDate().compareTo(b.getActivity().getActivityDate());
		return increasingOrder ? result : -result;
	}
	
	public static EstrattoContoRenderComparator getInstance(boolean increasingOrder){
		if(instance == null){
			instance = new EstrattoContoRenderComparator();
		}
		instance.setIncreasingOrder(increasingOrder);
		return instance;
	}

	public boolean isIncreasingOrder() {
		return increasingOrder;
	}

	public void setIncreasingOrder(boolean increasingOrder) {
		this.increasingOrder = increasingOrder;
	}
}
