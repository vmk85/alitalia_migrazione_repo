package com.alitalia.aem.consumer.booking.render;

import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.RouteData;

public class SearchElementRender {
	
	private String title;
	private DateRender date;
	private SearchElementDetailRender fromFlightDetail;
	private SearchElementDetailRender toFlightDetail;
	private String cabin;
	private String labelInfoMessage;
	
	public SearchElementRender(DateRender date,
			SearchElementDetailRender fromFlightDetail,
			SearchElementDetailRender toFlightDetail, FlightData flightData,
			BookingSessionContext ctx, int i) {
		
		this.title = "";
		this.date = date;
		this.fromFlightDetail = fromFlightDetail;
		this.toFlightDetail = toFlightDetail;
		this.cabin = "";
		this.labelInfoMessage = "";
		init(ctx, i, flightData);
	}
	
	private void init(BookingSessionContext ctx, int i, FlightData flightData) {
		
		if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
			this.title = obtainTitleBySliceIndex(i);
		} else {
			this.title = obtainARMessage(i).toUpperCase();
		}
		
		if ( flightData == null ) {
			if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
				this.labelInfoMessage = obtainMultilegMessage(i);
			} else {
				this.labelInfoMessage = "Seleziona il volo di " + obtainARMessage(i);
			}
		}
		if (ctx.readyToPassengersDataPhase) {
			if (ctx.selectionRoutes!= null && ctx.selectionRoutes.getRoutesList()!= null && ctx.selectionRoutes.getRoutesList().get(i)!=null) {
				if (ctx.award) {
					this.cabin = computeCabinAward(ctx, i);
				} else {
					this.cabin = computeCabin(ctx.selectionRoutes.getRoutesList().get(i));
				}
				
			}
			
		}
	}
	
	public String getTitle() {
		return title;
	}

	public DateRender getDate() {
		return date;
	}

	public SearchElementDetailRender getFromFlightDetail() {
		return fromFlightDetail;
	}

	public SearchElementDetailRender getToFlightDetail() {
		return toFlightDetail;
	}

	public String getCabin() {
		return cabin;
	}

	public String getLabelInfoMessage() {
		return labelInfoMessage;
	}
	
	
	/*private methods*/
	
	private String obtainTitleBySliceIndex(int sliceIndex) {
		String msg = "";
		switch(sliceIndex+1){
			case 1:{
				msg = "PRIMA";
			}break;
			case 2:{
				msg = "SECONDA";		
			}break;
			case 3:{
				msg = "TERZA";
			}break;
			case 4:{
				msg = "QUARTA";
			}break;
		}
		return msg + " TRATTA";
	}
	
	private String obtainMultilegMessage(int i) {
		String msg = "";
		switch(i+1){
			case 1:{
				msg = "primo";
			}break;
			case 2:{
				msg = "secondo";
			}break;
			case 3:{
				msg = "terzo";
			}break;
			case 4:{
				msg = "quarto";
			}break;
		
		}
		return "Seleziona il " + msg + " volo";
	}
	
	private String obtainARMessage(int i) {
		switch(i+1){
			case 1:{
				return "andata";
			}
			case 2:{
				return "ritorno";
			}
		}
		return "";
	}
	
	private String computeCabin(RouteData route) {
		
		FlightData flightData = route.getFlights().get(0);
		if (flightData instanceof ConnectingFlightData) {
			for (FlightData flight : ((ConnectingFlightData)flightData).getFlights()) {
				return ((DirectFlightData)flight).getCabin().value();
			}
		} else {
			return ((DirectFlightData)flightData).getCabin().value();
		}
		return "";
	}
	
	private String computeCabinAward(BookingSessionContext ctx, int i) {
		if (ctx.flightSelections != null && 
				ctx.flightSelections[i] != null && ctx.flightSelections[i].getSelectedBrandCode() != null) {
			if (ctx.flightSelections[i].getSelectedBrandCode().contains("Business")) {
				return "Business";
			} else {
				return "Economy";
			}
		}
		return "";
	}
}
