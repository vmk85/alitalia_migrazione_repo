package com.alitalia.aem.consumer.checkin.model;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinAncillaryConfirmation extends GenericCheckinModel {
	
	@Inject
	AlitaliaConfigurationHolder configurationHolder;
	
	@Self
	private SlingHttpServletRequest request;
	
	private String mail;
	private String nextPage;
	private String previousPage;
	
	@PostConstruct
	protected void initModel() {
		
		initBaseModel(request);
		mail = ctx.confirmationMailAddress;
		
		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
		
		nextPage = baseUrl + configuration.getCheckinBoardingPassPage();
		String ancillaryConfimationUrl = configurationHolder.getCheckinAncillaryConfirmationPage();

		previousPage = baseUrl + ancillaryConfimationUrl.substring(
				0, ancillaryConfimationUrl.length() - 5) + "." + 
				CheckinConstants.PREPARE_ANCILLARY_SERVLET_SELECTOR;
	}
	
	public String getMail() {
		return mail;
	}
	
	public String getNextPage() {
		return nextPage;
	}
	
	public String getPreviousPage() {
		return previousPage;
	}
	
}


