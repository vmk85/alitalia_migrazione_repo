package com.alitalia.aem.consumer.model.content.specialpages;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class NewsletterSubscribeMailHelper extends GenericBaseModel{
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private SlingHttpServletResponse response;
	
	@Inject
    private AlitaliaConfigurationHolder configuration;
	
	// informzioni gereriche per la composizione dei path
	private String hostName = "";
	private String mailImageUrl = "";
	
	@PostConstruct
	protected void initModel() throws IOException {

		try {

			this.initBaseModel(request);

			String prefix = "http://";
			if (!isWCMEnabled() && configuration.getHttpsEnabled())
				prefix = "https://";
			hostName = prefix + configuration.getExternalDomain();
			logger.debug("hostName: " + hostName);

			mailImageUrl = hostName + configuration.getStaticImagesPath();
			logger.debug("mailImageUrl: " + mailImageUrl);

		
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	public String getHostName() {
		return hostName;
	}

	public String getMailImageUrl() {
		return mailImageUrl;
	}
	
}
