package com.alitalia.aem.consumer.checkinrest.jobs.consumer;

import com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.response.Outcome;
import com.alitalia.aem.common.messages.checkinrest.*;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.checkinrest.jobs.CheckinJobConstants;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.google.gson.Gson;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@Component(
        label = "Alitalia CheckinRestAPI - Offload Passenger, Clear Ancillaries and Checkin Job Consumer",
        description = "Call Rest API (BL) for offload, clear ancillaries and checkin (if user add a comfort seat and ancillaries in cart)",

        // One of the few cases where immediate = true; this is so the Event Listener starts listening immediately
        immediate = true
)
@Properties({
        @Property(
                label = "Alitalia CheckinRestAPI - Offload Passenger, Clear Ancillaries and Checkin Job Consumer",
                value = CheckinJobConstants.TOPIC_PASSENGER_OFFLOAD_CHECKIN_AND_CLEAR_ANCILLARIES,
                description = "Call Rest API (BL) for offload, clear ancillaries and checkin (if user add a comfort seat and ancillaries in cart)",
                name = JobConsumer.PROPERTY_TOPICS,
                propertyPrivate = true
        )
})
@Service(value={JobConsumer.class})
@SuppressWarnings("unchecked")
@Deprecated
public class OffloadPassengerCheckinAndClearAncillariesJob implements JobConsumer {

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile ICheckinDelegate checkInDelegateRest;

    private static final Logger logger = LoggerFactory.getLogger(OffloadPassengerCheckinAndClearAncillariesJob.class);

    final public static String PROPERTY_CHECKIN_SESSION_ID = "checkinrest-sessionid";

    //offload passenger request
    final public static String PROPERTY_CHECKIN_OFFLOAD = "checkinrest-offload";

    //clear ancillaries
    final public static String PROPERTY_CHECKIN_CLEARANCILLARIES = "checkinrest-clearancillaries";

    //checkin
    final public static String PROPERTY_CHECKIN_CHECKINPASSENGER = "checkinrest-checkinselectedpassenger";

    public JobResult process(final Job job) {
        // process the job and return the result
        String sessionID = job.getProperty(PROPERTY_CHECKIN_SESSION_ID).toString();
        logger.debug("[{}] OffloadPassengerAndClearAncillariesJob starting", sessionID);

        Gson gson = new Gson();

        List<CheckinOffloadFlightsRequest> offloadRequestList = new ArrayList<>();
        List<CheckinClearAncillarySessionEndRequest> clearAncillariesRequestList = new ArrayList<>();
        List<CheckinSelectedPassengerRequest> checkinSelectedPassengerRequests = new ArrayList<>();

        for(String propertyItemName : job.getPropertyNames()) {
            if(propertyItemName.startsWith(OffloadPassengerCheckinAndClearAncillariesJob.PROPERTY_CHECKIN_CHECKINPASSENGER) ||
                    propertyItemName.startsWith(OffloadPassengerCheckinAndClearAncillariesJob.PROPERTY_CHECKIN_CLEARANCILLARIES) ||
                    propertyItemName.startsWith(OffloadPassengerCheckinAndClearAncillariesJob.PROPERTY_CHECKIN_OFFLOAD)) {

                String jsonProperty = job.getProperty(propertyItemName).toString();

                try {
                    if(propertyItemName.startsWith(OffloadPassengerCheckinAndClearAncillariesJob.PROPERTY_CHECKIN_CLEARANCILLARIES)) {
                        CheckinClearAncillarySessionEndRequest reqItem = gson.fromJson(jsonProperty, CheckinClearAncillarySessionEndRequest.class);
                        clearAncillariesRequestList.add(reqItem);
                    } else if(propertyItemName.startsWith(OffloadPassengerCheckinAndClearAncillariesJob.PROPERTY_CHECKIN_OFFLOAD)) {
                        CheckinOffloadFlightsRequest reqItem = gson.fromJson(jsonProperty, CheckinOffloadFlightsRequest.class);
                        offloadRequestList.add(reqItem);
                    } else if(propertyItemName.startsWith(OffloadPassengerCheckinAndClearAncillariesJob.PROPERTY_CHECKIN_CHECKINPASSENGER)) {
                        CheckinSelectedPassengerRequest reqItem = gson.fromJson(jsonProperty, CheckinSelectedPassengerRequest.class);
                        checkinSelectedPassengerRequests.add(reqItem);
                    }
                } catch(Exception e) {
                    logger.error("[{}] cannot deserialize request {} " + e.toString(), sessionID,jsonProperty);
                }
            }
        }

        if(offloadRequestList.size() > 0) {
            Boolean status = true;
            //TODO: gestione errore 500 null
            for(CheckinOffloadFlightsRequest request : offloadRequestList) {
                request.setSid(sessionID);
                request.setTid(IDFactory.getTid());
                CheckinOffloadFlightsResponse response = checkInDelegateRest.offloadFlights(request);
//                if (response.getOutcome() != null) {
//                    if (!response.getOutcome().equals("OK")) {
//                        logger.warn("[{}] Si è verificato un errore durante offload {}", sessionID, response.getOutcome());
//                        status = status && false;
//                    } else {
//                        logger.info("[{}] Offload ripulito correttamente. Outcome: {}", sessionID, response.getOutcome());
//                        status = status && true;
//                    }
//                } else {
//                    status = status && false;
//                }
            }

            //checkin
            logger.debug("[{}] Calling service rest checkinselectedpassenger",sessionID);
            if(checkinSelectedPassengerRequests.size() > 0) {
                for(CheckinSelectedPassengerRequest request : checkinSelectedPassengerRequests) {
                    request.setSid(sessionID);
                    request.setTid(IDFactory.getTid());

                    CheckinSelectedPassengerResponse serviceResponce = checkInDelegateRest.executePassengerCheckin(request);
                    Outcome outcomeCheckinSelectedPassenger = serviceResponce.getOutcome();

                    if(outcomeCheckinSelectedPassenger.getOutcome().equalsIgnoreCase("OK")){
                        logger.info("[{}] Checkin effettuato {}", sessionID, gson.toJson(serviceResponce));
                        status = status && true;
                    } else {
                        logger.warn("[{}] Si è verificato un errore nel clear ancillary e session end {}", sessionID, gson.toJson(serviceResponce));
                        status = status && false;
                    }
                }
            } else {
                logger.warn("[{}] checkin nothing to do.", sessionID);
            }


            // clear ancillaries
            logger.debug("[{}] Calling service rest clearAncillaySessionEnd",sessionID);
            if(clearAncillariesRequestList.size() > 0) {
                //TODO: gestione errore 500 null
                for (CheckinClearAncillarySessionEndRequest request : clearAncillariesRequestList) {
                    request.setSid(sessionID);
                    request.setTid(IDFactory.getTid());
                    CheckinClearAncillarySessionEndResponse checkinClearAncillarySessionEndResponse = checkInDelegateRest.clearAncillaySessionEnd(request);
                    if (checkinClearAncillarySessionEndResponse.getOutcome().get(0) != null) {
                        if (!checkinClearAncillarySessionEndResponse.getOutcome().get(0).equals("OK")) {
                            logger.warn("[{}] Si è verificato un errore nel clear ancillary {}", sessionID, checkinClearAncillarySessionEndResponse.getOutcome().get(0));
                            status = status && false;
                        } else {
                            logger.info("[{}] Ancillay e sessione ripulite correttamente. Outcome: {}", sessionID, checkinClearAncillarySessionEndResponse.getOutcome().get(0));
                            status = status && true;
                        }
                    }
                }
                return status ? JobResult.OK : JobResult.CANCEL;
            } else {
                logger.warn("[{}] Clear Ancillaries nothing to do", sessionID);
                return JobResult.CANCEL;
            }
        } else {
            logger.warn("[{}] Offload nothing to do", sessionID);
            return JobResult.CANCEL;
        }
    }
}