package com.alitalia.aem.consumer.carnet.model;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.carnet.CarnetConstants;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;

public class GenericCarnetModel extends GenericBaseModel {
	
	protected CarnetSessionContext ctx;
	
	@Inject
	protected AlitaliaConfigurationHolder configuration;
	
	@Override
	protected void initBaseModel(SlingHttpServletRequest request) {
		
		super.initBaseModel(request);
		ctx = (CarnetSessionContext) request.getSession(true).getAttribute(
				CarnetConstants.CARNET_CONTEXT_ATTRIBUTE);
		
		if (ctx == null && !isWCMEnabled()) {
			logger.error("Cannot find carnet session context in session.");
			throw new RuntimeException(
					"Cannot find carnet session context in session.");
		}
	}
	
	public CarnetSessionContext getCtx() {
		return ctx;
	}
	
}