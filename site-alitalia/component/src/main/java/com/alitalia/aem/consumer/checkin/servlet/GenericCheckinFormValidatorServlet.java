package com.alitalia.aem.consumer.checkin.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.exception.CheckinPnrInformationNotFoundException;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;

@SuppressWarnings("serial")
public abstract class GenericCheckinFormValidatorServlet
		extends GenericFormValidatorServlet {
	
	/**
	 * Utility method to access the CheckInSessionContext instance.
	 * 
	 * <p>If the instance is not available, an exception is thrown.</p>
	 * 
	 * @param request The related servlet request.
	 * @return The CheckInSessionContext instance.
	 */
	protected CheckinSessionContext getCheckInSessionContext(SlingHttpServletRequest request) {

		logger.info("GenericCheckinFormValidatorServlet (getCheckInSessionContext) session accessed: {}", request.getSession().getId());

		CheckinSessionContext ctx = (CheckinSessionContext) request.getSession(true).getAttribute(CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE);
		if (ctx == null) {
			throw new GenericFormValidatorServletException(false, "CheckIn session context not found. Referer: "+ request.getHeader("referer") );
		} else {
			return ctx;
		}
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		long time_before = System.currentTimeMillis();
		String action = request.getParameter("_action");
		if ("validate".equals(action)) {
			performValidationBehavior(request, response);
		} else {
			performSubmitBehavior(request, response);
		}
		response.addCookie(new Cookie("JSESSIONID", request.getSession(true).getId()));
		long time_after = System.currentTimeMillis();
		long execution_time = time_after - time_before;
		if (execution_time > 20000) {
			logger.info("------ ALERT EXECUTION TIME ------ Servlet execution time=" + execution_time);
		} else {
			logger.info("Servlet execution time=" + execution_time);
		}
	}

	/**
	 * Utility method to initialize the CheckInSessionContext instance.
	 *
	 *
	 * @param request The related servlet request.
	 * @param configuration The configuration.
	 * @param checkinSession The checkin session.
	 * @return The CheckInSessionContext instance.
	 */
	protected boolean initSession(SlingHttpServletRequest request, AlitaliaConfigurationHolder configuration, CheckinSession checkinSession){
		//
		String passengerName = request.getParameter("firstName") == null ? "" : request.getParameter("firstName");
		passengerName = AlitaliaUtils.replaceUnwantedCharacters(passengerName).trim();
		String passengerSurname = request.getParameter("lastName") == null ? "" : request.getParameter("lastName");
		passengerSurname = AlitaliaUtils.replaceUnwantedCharacters(passengerSurname).trim();
		String pnr = request.getParameter("pnr") == null ? "" : request.getParameter("pnr");

		//Per il ticket viene preso il getParmeter("pnr") perchè in fase di sviluppo si è pensato di utilizzare nella form lo stesso input.
		String ticket = request.getParameter("pnr") == null ? "" : request.getParameter("pnr");

		String frequentFlyer = request.getParameter("frequentFlyer") == null ? "" : request.getParameter("frequentFlyer");

		//TODO - inserisco in sessione il conversationID (utile per ogni servizio invocato)
		String conversationId = IDFactory.getTid();
//		String tripId = request.getParameter("checkin__code");
//		String MMcode = request.getParameter("checkin__MMcode");
//		String pin = request.getParameter("checkin__pin");
		String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		String language = AlitaliaUtils.getRepositoryPathLanguage(request.getResource());
		String currencyCode = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencyCode", String.class);
		String currencySymbol = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencySymbol", String.class);
		Locale locale = AlitaliaUtils.getRepositoryPathLocale(request.getResource());

		String callerIp = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
		I18n i18n = new I18n(request.getResourceBundle(locale));
		String machineName = AlitaliaUtils.getLocalHostName("unknown");
		String searchType = (request.getParameter("checkinSearchType")!=null)?request.getParameter("checkinSearchType"):CheckinConstants.CHECKIN_SEARCH_LOGIN; //[login, code]

		/*Currency format info*/
		InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(request.getResource());
		String decimalSeparator=ivm.getInherited("decimalSeparator", String.class);
		String groupingSeparator=ivm.getInherited("groupingSeparator", String.class);
		Integer numDecDigits=ivm.getInherited("decimalDigit", Integer.class);
		if(numDecDigits == null){
			numDecDigits = 2;
		}
		NumberFormat currentNumberFormat = AlitaliaUtils.getNumberFormat(numDecDigits, decimalSeparator, groupingSeparator, market);

		HttpSession session = request.getSession(true);

		CheckinSessionContext ctx = null;

		try {
			ctx = checkinSession.initializeSessionRest(request, i18n, CheckinConstants.CHECKIN_CLIENT, machineName,
						(session.getId() != null ? session.getId() : ""), CheckinConstants.CHECKIN_SITE, market, language, conversationId,
						currencyCode, currencySymbol, locale, callerIp, pnr, ticket, frequentFlyer,
						passengerName, passengerSurname, searchType, currentNumberFormat, null);

		} catch (CheckinPnrInformationNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		session.setAttribute(CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE, ctx);
		logger.info("Checkinrest context set in session. JSESSIONID: {}", request.getSession().getId());
		return true;
	}


	/**
	 * Provide MMB-specific unrecoverable error page.
	 */
	@Override
	protected String getUnrecoverableErrorRedirect(SlingHttpServletRequest request, 
			SlingHttpServletResponse response, GenericFormValidatorServletException exception) {
		return getUnrecoverableErrorRedirect(request);
	}
	
	private String getUnrecoverableErrorRedirect(SlingHttpServletRequest request) {
		return request.getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getCheckinFailurePage());
	}
	
	/**
	 * Provide custom management for MMB exceptions.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		Map additionalParams = super.getAdditionalFailureData(request, exception);
		String url = getUnrecoverableErrorRedirect(request);
		additionalParams.put("redirect", url);
		return additionalParams;
	}
	
	/**
	 * Provide an empty implementation, should be never used as
	 * CheckIn servlets should always be invoked with ajax requests
	 */
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		logger.warn("The emtpy saveDataIntoSession method was unexpectedly invoked.");
	}
	
	/**
	 * Abstract method to be implemented in subclasses in order to
	 * provide the configuration holder component instance.
	 * 
	 * @return The AlitaliaConfigurationHolder instance.
	 */
	protected abstract AlitaliaConfigurationHolder getConfiguration();
	
}
