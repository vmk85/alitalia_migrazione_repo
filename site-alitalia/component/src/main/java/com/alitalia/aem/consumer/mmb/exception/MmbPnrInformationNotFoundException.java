package com.alitalia.aem.consumer.mmb.exception;

public class MmbPnrInformationNotFoundException extends Exception {

	private static final long serialVersionUID = -8388948758281010112L;

	public MmbPnrInformationNotFoundException(String message) {
		super(message);
	}
	
	public MmbPnrInformationNotFoundException(Throwable cause) {
		super(cause);
	}
	
}
