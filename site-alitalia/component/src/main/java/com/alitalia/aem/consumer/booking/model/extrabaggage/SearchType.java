package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Enumerazione sulla tipologia della ricerca dei voli 
*/
public enum SearchType
{
	BRANDED,
	LOWESTFARE,
	CALENDAR30;
	//al momento tutto uello che viene dopo non è implementato, ma potrà essere usato in futuro
	//BrandSearch,
	//RibbonSearch,
	//FlexDateSearch,
	//CalendarSearch,
	//MultiDestinationSearch,
	//MultiSliceSearch,
	//YouthSearch,
	//CarnetSearch,
	//CarnetFlexDateSearch,
	//ClosedUserGroupSearch,
	//JumpUpSearch,
	//MetaSearch,
	//AwardCalendarSearch,
	//AwardFlightSearch,
	//AwardFlightDetailSearch,
	//FlexDateMatrixSearch

	public static final int SIZE = java.lang.Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static SearchType forValue(int value)
	{
		return values()[value];
	}
}