package com.alitalia.aem.consumer.bookingaward.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.controller.BookingSessionGenericController;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.consumer.booking.render.BrandRender;
import com.alitalia.aem.consumer.booking.render.SelectedFlightInfoRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingAwardTravelInfo extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private BookingSession bookingSession;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private SelectedFlightInfoRender selectedFlightInfo;
	private int indexRoute;
	private int indexFlight;
	private int indexBrand;
	private List<BrandPageData> brandList;
	private List<Boolean> mapBrand;
	private String activeBrand;
	private boolean isBusiness;
	
	private int brandEconomyNumber;

	private boolean showFareRules;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			
			initBaseModel(request);
			
			if (ctx != null) {
				showFareRules = true;
				
				if( ( request.getParameter("indexRoute")==null || ("").equals(request.getParameter("indexRoute")) ) || ( !("1").equals(request.getParameter("indexRoute")) && !("0").equals(request.getParameter("indexRoute")) ) ){
					logger.error("indexRoute not found");
					throw new RuntimeException();		
				}
				indexRoute = Integer.parseInt(request.getParameter("indexRoute"));
				indexFlight = Integer.parseInt(request.getParameter("indexFlight"));
				
				if (request.getParameter("indexBrand") != null) {
					// if a brand is selected the correct class will be retrieved
					indexBrand = Integer.parseInt(request.getParameter("indexBrand"));
					String[] searchBrands = configuration.getBookingAwardBrands();
					activeBrand = searchBrands[indexBrand];
				} else {
					indexBrand = -1;
				}
				
				isBusiness = Boolean.parseBoolean(request.getParameter("isBusiness"));
				
				FlightData selectedflightData = bookingSession.searchSelectionFlightData(ctx, indexRoute, indexFlight);
				
				// retrieve list of brand pages
				Map<String, BrandPageData> brandDataMap = ctx.brandMap;
				List<BrandData> brandsOfFlight = selectedflightData.getBrands();
				List<String> indexBrands = new ArrayList<String>();
				for (BrandData brand : brandsOfFlight) {
					String key = brand.getCode().toLowerCase();
					BrandPageData brandPage = ctx.codeBrandMap.get(key);
					indexBrands.add(brandPage.getId());
				}
				String[] orderedBrand = configuration.getBookingAwardBrands();
				
				
				brandList = new ArrayList<BrandPageData>();
				mapBrand = new ArrayList<Boolean>();
				
				int k;
				for (k=0; k<orderedBrand.length; k++) {
					String key = orderedBrand[k];
					
					BrandPageData page = brandDataMap.get(key);
					if (page != null) {
						
						brandList.add(page);
						
						if (indexBrands.contains(page.getId())) {
							mapBrand.add(true);
						}
						else {
							mapBrand.add(false);
						}
					}
				}
				
				brandEconomyNumber = 3;  // FIXME
				selectedFlightInfo = new SelectedFlightInfoRender(selectedflightData,i18n , ctx.market);
				
				
				
			}
		} catch (Exception e) {
			logger.error("Unexpected error ", e);
		}
	}
	
	public int getIndexRoute() {
		return indexRoute;
	}
	
	public int getIndexFlight() {
		return indexFlight;
	}
	
	public int getIndexBrand() {
		return indexBrand;
	}

	public SelectedFlightInfoRender getSelectedFlightInfo() {
		return selectedFlightInfo;
	}
	
	
	public boolean getShowFareRules() {
		return showFareRules;
	}
	
	public List<BrandPageData> getBrandList() {
		return brandList;
	}
	
	public int getBrandEconomyNumber() {
		return brandEconomyNumber;
	}
	
	public String getActiveBrand() {
		return activeBrand;
	}
	
	public boolean isBusiness() {
		return isBusiness;
	}

	public List<Boolean> getMapBrand() {
		return mapBrand;
	}
	
	
	
}
