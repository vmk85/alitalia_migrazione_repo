package com.alitalia.aem.consumer.model.content.specialpages;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class GenericSpecialPageModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;
	
	@Inject
	private AlitaliaConfigurationHolder alitaliaConfigurationHolder;

	private Boolean success = false;
	private String homepage = "";

	@PostConstruct
	protected void initModel() {
		logger.debug("[GenericSpecialPageModel] initModel");

		super.initBaseModel(slingHttpServletRequest);

		Resource resource = slingHttpServletRequest.getResource();
		homepage = AlitaliaUtils.findSiteBaseExternalUrl(resource, true)
				+ alitaliaConfigurationHolder.getHomePageWithExtension();
		if(logger.isDebugEnabled())
		logger.debug("[GenericSpecialPageModel] return to hp: " + homepage);
		success = Boolean.parseBoolean(
				slingHttpServletRequest.getParameter("success"));
		if(logger.isDebugEnabled())
		logger.debug("[GenericSpecialPageModel] initModel success : [" +
				success.toString() + "]");

	}
	
	public String getHomepage() {
		return homepage;
	}

	public Boolean getSuccess() {
		return success;
	}

}
