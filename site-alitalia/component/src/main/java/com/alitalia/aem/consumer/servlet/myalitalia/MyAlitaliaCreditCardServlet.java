package com.alitalia.aem.consumer.servlet.myalitalia;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;

import com.alitalia.aem.common.data.home.AdyenRecurringDetailData;
import com.alitalia.aem.common.data.home.AdyenRetrievalStoredPaymentData;
import com.alitalia.aem.common.data.home.MACreditCardData;
import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.data.home.MACustomerDataPayment;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenRetrievalStoredPaymentResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.myalitaliauser.delegaterest.IAdyenRecurringPaymentDelegate;
import com.day.cq.commons.TidyJSONWriter;

@SuppressWarnings("serial")
public class MyAlitaliaCreditCardServlet extends GenericFormValidatorServlet {
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		// no input fields, always valid
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
	}
	
	protected void retrieveMyCreditCard(MACustomer loggedMAUserProfile, String market, String language, IAdyenRecurringPaymentDelegate adyenRecurringPaymentDelegate) throws Exception {
		logger.info("MAPayment - MyAlitaliaCreditCardServlet - retrieveMyCreditCard: Recupero Carte di Credito in Adyen");
		try {
			setCreditCardsData(loggedMAUserProfile, null);
			String UID = loggedMAUserProfile.getUID();
			if (UID!=null && UID.trim().isEmpty()==false) {
				ArrayList<MACreditCardData> maCreditCardsData = null;
				
				AdyenRetrievalStoredPaymentRequest adyenRetrievalStoredPaymentRequest = new AdyenRetrievalStoredPaymentRequest();
				adyenRetrievalStoredPaymentRequest.setShopperReference(UID);
				adyenRetrievalStoredPaymentRequest.setMarket(market);
				adyenRetrievalStoredPaymentRequest.setLanguage(language);
				adyenRetrievalStoredPaymentRequest.setConversationID(IDFactory.getTid());
				
				logger.info("MAPayment - MyAlitaliaCreditCardServlet - retrieveMyCreditCard: invocando adyenRecurringPaymentDelegate.retrievalStoredPayment...");
				AdyenRetrievalStoredPaymentResponse adyenRetrievalStoredPaymentResponse = adyenRecurringPaymentDelegate.retrievalStoredPayment(adyenRetrievalStoredPaymentRequest);
				if (adyenRetrievalStoredPaymentResponse!=null && adyenRetrievalStoredPaymentResponse.getAdyenRetrievalStoredPaymentList()!=null &&
					adyenRetrievalStoredPaymentResponse.getAdyenRetrievalStoredPaymentList().isEmpty()==false) {
					for (AdyenRetrievalStoredPaymentData adyenRetrievalStoredPayment: adyenRetrievalStoredPaymentResponse.getAdyenRetrievalStoredPaymentList()) {
						if (adyenRetrievalStoredPayment!=null && adyenRetrievalStoredPayment.getRecurringDetailList()!=null && adyenRetrievalStoredPayment.getRecurringDetailList().isEmpty()==false) {
							for (AdyenRecurringDetailData recurringDetail: adyenRetrievalStoredPayment.getRecurringDetailList()) {
								if (recurringDetail!=null && recurringDetail.getCard()!=null) {

									String cardBin = "";
									if(recurringDetail.getAdditionalDataList() != null && recurringDetail.getAdditionalDataList().size() > 0 && recurringDetail.getAdditionalDataList().get(0).getCardBin() != null)
										cardBin = recurringDetail.getAdditionalDataList().get(0).getCardBin();

									MACreditCardData maCreditCardData = new MACreditCardData(recurringDetail.getCard().getType(), recurringDetail.getPaymentMethodVariant(), recurringDetail.getCard().getNumber(),
											recurringDetail.getCard().getCvc(), recurringDetail.getCard().getHolderName(), recurringDetail.getCard().getExpiryMonth().length() < 2 ? "0" + recurringDetail.getCard().getExpiryMonth() : recurringDetail.getCard().getExpiryMonth(), recurringDetail.getCard().getExpiryYear(),
											recurringDetail.getAlias(), recurringDetail.getFirstPspReference(), recurringDetail.getRecurringDetailReference(), cardBin);

									if(!cardBin.equals(""))
										maCreditCardData.setCardBin(cardBin);
									
									if (recurringDetail.getAddress()!=null) {
										maCreditCardData.fillAddressData(recurringDetail.getAddress().getPostalCode(), recurringDetail.getAddress().getCity(), recurringDetail.getAddress().getCountry(), recurringDetail.getAddress().getStreet(), 
												recurringDetail.getAddress().getProvince()); 
									}
									
									if (maCreditCardsData==null) maCreditCardsData = new ArrayList<MACreditCardData>();
									maCreditCardsData.add(maCreditCardData);
									
									logger.debug("MAPayment - MyAlitaliaCreditCardServlet - retrieveMyCreditCard: Recuperata carta di credito [{}]", maCreditCardData.print());
								}
							}
						}
					}
				}
				
				if (maCreditCardsData!=null) setCreditCardsData(loggedMAUserProfile, maCreditCardsData);
			} else {
				logger.error("MAPayment - MyAlitaliaCreditCardServlet - retrieveMyCreditCard: UID utente MyAlitalia non presente");
				throw new Exception("UID utente MyAlitalia non presente");
			}
		} catch (Exception e) {
			logger.error("MAPayment - MyAlitaliaCreditCardServlet - retrieveMyCreditCard: Errore durante la cancellazione della carta di credito in Adyen", e);
			throw e;
		}
	}
	
	protected void setCreditCardsData(MACustomer userMA, ArrayList<MACreditCardData> maCreditCardsData) {
		if (userMA!=null && userMA.getData()!=null) {
			if (userMA.getData().getPayment()==null) userMA.getData().setPayment(new MACustomerDataPayment());
			userMA.getData().getPayment().setCreditCardsData(maCreditCardsData);
		}
	}

	protected void prepareResponseOK(SlingHttpServletResponse response, String message) throws IOException, JSONException {
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");		
		json.key("isError").value(false);
		json.key("result").value("OK");
		json.key("message").value(message);
		json.endObject();
	}

	protected void prepareResponseNOK(SlingHttpServletResponse response, String errorMessage) throws IOException, JSONException {
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");		
		json.key("isError").value(true);
		json.key("result").value("NOK");
		json.key("errorMessage").value(errorMessage);
		json.endObject();
	}

	/**
	 * Provide an empty implementation, should be never used as
	 * MMB servlets should always be invoked with ajax requests
	 */
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		logger.warn("The emtpy saveDataIntoSession method was unexpectedly invoked.");
	}
}

