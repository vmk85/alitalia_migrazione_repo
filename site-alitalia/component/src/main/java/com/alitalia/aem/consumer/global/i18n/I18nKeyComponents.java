package com.alitalia.aem.consumer.global.i18n;

public class I18nKeyComponents {

	
	/* Componente Prepara viaggio */
	public static final String PREPARAVIAGGIO_NUMEROVOLO_EMPTY = "preparaViaggio.numeroVolo.error.empty";
	public static final String PREPARAVIAGGIO_DATAPARTENZA_EMPTY = "preparaViaggio.dataPartenza.error.empty";
	public static final String PREPARAVIAGGIO_NUMEROVOLO_INVALID = "preparaViaggio.numeroVolo.error.invalid";
	public static final String PREPARAVIAGGIO_DATAPARTENZA_INVALID = "preparaViaggio.dataPartenza.error.invalid";
	public static final String PREPARAVIAGGIO_AEROPORTOPARTENZA_EMPTY = "preparaViaggio.aeroportoPartenza.error.empty";
	public static final String PREPARAVIAGGIO_AEROPORTOARRIVO_EMPTY = "preparaViaggio.aeroportoArrivo.error.empty";
	public static final String PREPARAVIAGGIO_PNRTICKETNUMBER_EMPTY = "preparaViaggio.pnrTicketNumber.error.empty";
	public static final String PREPARAVIAGGIO_PNRTICKETNUMBER_INVALID = "preparaViaggio.pnrTicketNumber.error.invalid";
	public static final String PREPARAVIAGGIO_NOME_EMPTY = "preparaViaggio.nome.error.empty";
	public static final String PREPARAVIAGGIO_NOME_INVALID = "preparaViaggio.nome.error.invalid";
	public static final String PREPARAVIAGGIO_COGNOME_EMPTY = "preparaViaggio.cognome.error.empty";
	public static final String PREPARAVIAGGIO_COGNOME_INVALID = "preparaViaggio.cognome.error.invalid";
	public static final String PREPARAVIAGGIO_ERROR_FLIGHT_NOT_FOUND = "flightstatus.error.notfound";
	public static final String PREPARAVIAGGIO_FLIGHTS_FOUND_PREFIX =  "myFlights.flightsfound.prefix";
	public static final String PREPARAVIAGGIO_FLIGHTS_FOUND_SUFFIX =  "myFlights.flightsfound.suffix";
	public static final String PREPARAVIAGGIO_ONEFLIGHT_FOUND_SUFFIX =  "myFlights.oneflightfound.suffix";
	
	public static final String MMB_MANAGE_NUMBER_NOT_VALID = "mmb.specialAssistance.numberNotValid.error";
	
	
}
