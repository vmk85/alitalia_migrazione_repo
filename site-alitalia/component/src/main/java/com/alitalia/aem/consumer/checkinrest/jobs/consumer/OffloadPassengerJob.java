package com.alitalia.aem.consumer.checkinrest.jobs.consumer;

/**
 * Created by ggadaleta on 01/02/2018.
 */

import com.alitalia.aem.common.data.checkinrest.model.offload.request.PnrFlightInfo;
import com.alitalia.aem.common.data.checkinrest.model.offload.response.FlightPassR;
import com.alitalia.aem.common.data.checkinrest.model.offload.response.PnrFlightInfoR;
import com.alitalia.aem.common.data.checkinrest.model.offload.response.SeatListR;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadFlightsRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadFlightsResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.checkinrest.jobs.CheckinJobConstants;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.google.gson.Gson;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@Component(
    label = "Alitalia CheckinRestAPI - Offload Job Consumer",
    description = "Call Rest API (BL) for offload",

    // One of the few cases where immediate = true; this is so the Event Listener starts listening immediately
    immediate = true
)
@Properties({
    @Property(
        label = "Alitalia CheckinRestAPI - Offload Job Consumer",
        value = CheckinJobConstants.TOPIC_PASSENGER_OFFLOAD,
        description = "Call Rest API (BL) for offload",
        name = JobConsumer.PROPERTY_TOPICS,
        propertyPrivate = true
    )
})
@Service(value={JobConsumer.class})
@SuppressWarnings("unchecked")
public class OffloadPassengerJob implements JobConsumer {

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile ICheckinDelegate checkInDelegateRest;

    private static final Logger logger = LoggerFactory.getLogger(OffloadPassengerJob.class);

    final public static String PROPERTY_CHECKIN_SESSION_ID = "checkinrest-sessionid";

    //OFFLOAD
    final public static String PROPERTY_CHECKIN_OFFLOAD = "checkinrest-offload";

    public JobResult process(final Job job) {
        // process the job and return the result
        String sessionID = job.getProperty(PROPERTY_CHECKIN_SESSION_ID).toString();
        logger.debug("[{}] OffloadPassengerJob starting", sessionID);

        Gson gson = new Gson();

        List<CheckinOffloadFlightsRequest> requestList = new ArrayList<>();
        for(String propertyItemName : job.getPropertyNames()) {
            if(propertyItemName.startsWith(PROPERTY_CHECKIN_OFFLOAD)) {

                String jsonProperty = job.getProperty(propertyItemName).toString();

                try {
                    CheckinOffloadFlightsRequest reqItem = gson.fromJson(jsonProperty, CheckinOffloadFlightsRequest.class);
                    requestList.add(reqItem);
                } catch(Exception e) {
                    logger.error("[{}] cannot deserialize request {} " + e.toString(), sessionID,jsonProperty);
                }
            }
        }

        if(requestList.size() > 0) {
            Boolean status = true;
            //TODO: gestione errore 500 null
            for(CheckinOffloadFlightsRequest request : requestList) {
                request.setSid(sessionID);
                request.setTid(IDFactory.getTid());
                CheckinOffloadFlightsResponse response = checkInDelegateRest.offloadFlights(request);
                logger.debug("["+sessionID+"] OffloadCall request {} response {}", gson.toJson(request), gson.toJson(response));

                for(PnrFlightInfoR flightInfoR : response.getPnrFlightInfoRS()) {
                    for(FlightPassR flightPassR : flightInfoR.getFlightPassRS()) {
                        for(SeatListR seatListR : flightPassR.getSeatListRS()) {
                            if("OK".equals(seatListR.getEsito())) {
                                logger.info("["+sessionID+"] Offload eseguito per {} {}",
                                        seatListR.getFirstName() + " " + seatListR.getLastName(),
                                        "volo " + flightPassR.getAirline() +
                                        flightPassR.getFlight() +
                                        "da " + flightPassR.getOrigin() +
                                        "il " + flightPassR.getDepartureDate() +
                                        "pnr " +flightInfoR.getPnr());
                                status = status && true;
                            } else {
                                logger.info("["+sessionID+"] ERROR Offload per {} {}",
                                        seatListR.getFirstName() + " " + seatListR.getLastName(),
                                        "volo " + flightPassR.getAirline() +
                                        flightPassR.getFlight() +
                                        "da " + flightPassR.getOrigin() +
                                        "il " + flightPassR.getDepartureDate() +
                                        "pnr " +flightInfoR.getPnr());
                                status = status && false;
                            }
                        }
                    }
                }
            }
            return status ? JobResult.OK : JobResult.CANCEL;
            //return JobResult.FAILED;
        } else {
            logger.warn("[{}] Offload nothing to do", sessionID);
            return JobResult.CANCEL;
        }
    }
}