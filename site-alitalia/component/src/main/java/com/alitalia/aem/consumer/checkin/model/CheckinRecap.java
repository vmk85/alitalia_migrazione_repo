package com.alitalia.aem.consumer.checkin.model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.checkin.CheckinRecapData;
import com.alitalia.aem.consumer.checkin.CheckinUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinRecap extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	private List<CheckinRecapData> recapsData;
	
	private static final String KEY_PREFIX = "airportsData.";
	private static final String KEY_SUFFIX = ".city";
	
	@PostConstruct
	protected void initModel() {
		initBaseModel(request);
		int boardingPassTYP = ctx.boardingPassTYP;
		
		
		List<CheckinPassengerData> passengers = ctx.acceptedPassengers;
		
		//List<CheckinPassengerData> passengers = CheckinUtils.getCheckedInPassengers(ctx.passengers);
		recapsData = new ArrayList<CheckinRecapData>();
		
		if (boardingPassTYP == 1) {
			for (CheckinPassengerData passenger : passengers) {
				String passengerName = (passenger.getName() + " " + passenger.getLastName());
				String email = passenger.getBoardingPassRetrieveEmail();
				String phone = passenger.getBoardingPassRetrieveSms();
				if ((email != null && !email.isEmpty()) ||
						(phone != null && !phone.isEmpty())) {
					CheckinRecapData recap = new CheckinRecapData(passengerName, email, phone);
					recapsData.add(recap);
				}
			}
		}
		else if (boardingPassTYP == 2) {
			List<String> seats = ctx.newSeats;
			int index = 0;
			for (CheckinPassengerData passenger : passengers) {
				
				String passengerName = (passenger.getName() + " " + passenger.getLastName());
				String frequentFlyer="";
				if (passenger.getFrequentFlyerCarrier() != null) {
					frequentFlyer = passenger.getFrequentFlyerCarrier().getDescription();
				}
				String ticketNumber = passenger.getEticket();
				String pnr = passenger.getPnr();
				
				for (CheckinCouponData coupon : passenger.getCoupons()) {
					
					
					String seat = seats.get(index);
					index++;
					String terminal = coupon.getTerminal();
					
					CheckinFlightData flight = coupon.getFlight();
					String departureTime = "";
					if (flight.getDepartureDateTime() != null) {
					
						DateRender dateTimeRender = new DateRender(flight.getDepartureDateTime());
						departureTime = (dateTimeRender.getExtendedDate() + " " + dateTimeRender.getHour24());
					}
					
					String itinerary="";
					if (flight.getFrom() != null && flight.getTo() != null) {
						String cityDeparture = i18n.get(KEY_PREFIX + flight.getFrom().getCode() + KEY_SUFFIX ); 
						String cityArrival = i18n.get(KEY_PREFIX + flight.getTo().getCode() + KEY_SUFFIX ); 
						itinerary = (cityDeparture + "," + flight.getFrom().getCode() + " - " + cityArrival + "," + flight.getTo().getCode());
					}
					
					String flightNumber = (flight.getOperatingCarrier() + flight.getOperatingFlightNumber());
					CheckinRecapData recap = new CheckinRecapData(passengerName, frequentFlyer, ticketNumber, pnr, seat, itinerary, flightNumber, departureTime, terminal);
					recapsData.add(recap);
				}
			}
			
		}
	}


	public List<CheckinRecapData> getRecapsData() {
		return recapsData;
	}


	
}

