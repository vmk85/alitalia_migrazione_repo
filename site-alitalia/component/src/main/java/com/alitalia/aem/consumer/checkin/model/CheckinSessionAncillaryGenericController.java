package com.alitalia.aem.consumer.checkin.model;

import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;

import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinUtils;

public class CheckinSessionAncillaryGenericController extends GenericCheckinModel {
	
	@Override
	protected void initBaseModel(SlingHttpServletRequest request) {
		super.initBaseModel(request);
	}
	
	public String getCurrencySymbol() {
		if (ctx != null) {
			return ctx.currencySymbol;
		} else {
			return "";
		}
	}
	
	public boolean getDisplayFeedbackStep() {
		if (request != null) {
			return "1".equals(request.getParameter("displayfeedbackstep"));
		} else {
			return false;
		}
	}
	
	/* utility methods for subclasses */
	
	protected String getAncillariesSelectionIdentifier(List<MmbAncillaryData> targetAncillaries) {
		return getAncillariesSelectionIdentifier(
				(MmbAncillaryData[]) targetAncillaries.toArray(new MmbAncillaryData[targetAncillaries.size()]));
	}
	
	protected String getAncillariesSelectionIdentifier(MmbAncillaryData[] targetAncillaries) {
		String selectionIdentifier = "";
		if (targetAncillaries == null || targetAncillaries.length == 0) {
			selectionIdentifier = "(disabled)";
		} else {
			for (MmbAncillaryData affectedAncillary : targetAncillaries) {
				if (selectionIdentifier.length() > 0) {
					selectionIdentifier += "-";
				}
				selectionIdentifier += affectedAncillary.getId();
			}
		}
		return selectionIdentifier;
	}
	
	protected String getAncillariesSelectionIdentifier(MmbAncillaryData targetAncillary) {
		MmbAncillaryData[] targetAncillaries = null;
		if (targetAncillary != null) {
			targetAncillaries = new MmbAncillaryData[] { targetAncillary };
		}
		return getAncillariesSelectionIdentifier(targetAncillaries);
	}
	
	protected String getRouteTypeLabel(CheckinRouteData selectedRoute) {
		if (CheckinUtils.getFlightsList(selectedRoute).size() > 2) {
			int numTratta = selectedRoute.getId() + 1;
			return i18n.get(CheckinConstants.LABEL_TYPE_TRATTA) + " " + numTratta;
		} else {
			if (selectedRoute.getId() == 0) {
				return i18n.get(CheckinConstants.LABEL_TYPE_ANDATA);
			} else {
				return i18n.get(CheckinConstants.LABEL_TYPE_RITORNO);
			}
		}
	}
	
}
