package com.alitalia.aem.consumer.bookingaward.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.bookingaward.BookingAwardBrandCodeEnum;
import com.alitalia.aem.consumer.bookingaward.BookingAwardSession;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingawardcalendarsearch" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class BookingAwardCalendarSearchServlet extends GenericBookingAwardFormValidatorServlet {
	
	@Reference
    private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private BookingAwardSession bookingAwardSession;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		// no input fields, always valid
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws Exception {
		
		BookingSessionContext ctx = getBookingSessionContext(request);
		
		if (ctx.calendarSearchModified) {
			bookingAwardSession.modifyCalendarSearch(ctx);
		}
		else {
			/*Moved in BookingAwardPrepareSearchServlet*/
			/*bookingAwardSession.performCalendarSearch(ctx);*/
		}
		
		
		String noSolutionsPage = request.getResource().getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ configuration.getBookingAwardFlightSearchNoSolutionsPage());
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		
		if (ctx.availableFlights == null || ctx.availableFlights.getRoutes() == null ||
				ctx.availableFlights.getRoutes().isEmpty()) {
			json.key("solutionNotFound").value("allSolutions");
			json.key("redirect").value(noSolutionsPage);
			
		} else {
			
			boolean solutionsFound = true;
			
			for (RouteData routeData : ctx.availableFlights.getRoutes()) {
				if (routeData.getFlights() == null || routeData.getFlights().isEmpty()) {
					solutionsFound = false;
					break;
				}
			}
			
			if (!solutionsFound) {
				json.key("solutionNotFound").value("allSolutions");
				json.key("redirect").value(noSolutionsPage);
				
			} else {
			
				json.key("bAwardData");
				json.object();
				int i = 0;
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat dateFormatMonth = new SimpleDateFormat("yyyyMM");
				
				for (RouteData routeData : ctx.availableFlights.getRoutes()) {
					
					Map<Integer, Map<Integer, Map<Integer, BookingAwardBrandCodeEnum>>> result = 
							new LinkedHashMap<Integer, Map<Integer, Map<Integer, BookingAwardBrandCodeEnum>>>();
					
					for (FlightData flight : routeData.getFlights()) {
						if (flight instanceof DirectFlightData) {
							
							DirectFlightData currentFlight = (DirectFlightData) flight;
							insertFlight(result, currentFlight);
							
						} else if (flight instanceof ConnectingFlightData) {
							
							ConnectingFlightData currentFlight = (ConnectingFlightData) flight;
							if (currentFlight.getFlights() != null && !currentFlight.getFlights().isEmpty()) {
								DirectFlightData firstFlight = (DirectFlightData) currentFlight.getFlights().get(0);
								insertFlight(result, firstFlight);
								
							} else {
								logger.warn("Method getFlights() of ConnectingFlightData object is {}", currentFlight.getFlights() == null ? "null" : "empty");
							}
						}
					}
					
					json.key(String.valueOf(i));
					json.object();
					
					Calendar currentDate = Calendar.getInstance();
					Calendar firstDayOfDepartureMonth = Calendar.getInstance();
					Calendar departureDate = ctx.searchElements.get(i).getDepartureDate();
					
					if (ctx.calendarSearchModified) {
						int monthSelected = 0;
						int yearSelected = 0;
						
						if (i == 0) {
							monthSelected = ctx.calendarSearchDeparture[0];
							yearSelected = ctx.calendarSearchDeparture[1];
						}
						
						if (i == 1) {
							monthSelected = ctx.calendarSearchReturn[0];
							yearSelected = ctx.calendarSearchReturn[1];
						}
						firstDayOfDepartureMonth.set(Calendar.DAY_OF_MONTH, 1);
						firstDayOfDepartureMonth.set(Calendar.MONTH, monthSelected);
						firstDayOfDepartureMonth.set(Calendar.YEAR, yearSelected);
						departureDate = (Calendar) firstDayOfDepartureMonth.clone();
					}
					else {
						
						
						String selectedDate = dateFormat.format(departureDate.getTime());
						json.key("selectedDate").value(selectedDate);
						
						firstDayOfDepartureMonth = (Calendar) departureDate.clone();
						
						firstDayOfDepartureMonth.set(Calendar.DAY_OF_MONTH, 1);
					}
					
					
					
					json.key("rangeLow").value(firstDayOfDepartureMonth.after(currentDate) ?
							dateFormat.format(firstDayOfDepartureMonth.getTime()) : dateFormat.format(currentDate.getTime()));
					
					for (int year : result.keySet()) {
						json.key(String.valueOf(year));
						json.object();
						
						Map<Integer, Map<Integer, BookingAwardBrandCodeEnum>> innerMap = result.get(year);
						
						for (int month : innerMap.keySet()) {
							json.key(String.valueOf(month));
							json.object();
							
							Map<Integer, BookingAwardBrandCodeEnum> codesMap = innerMap.get(month);
							
							for (int day : codesMap.keySet()) {
								BookingAwardBrandCodeEnum code = codesMap.get(day);
								
								if (code != null) {
									json.key(String.valueOf(day));
									json.object();
									json.key("highlight").value(true);
									json.key("type").value(getCabin(code));
									json.endObject();
									
								} else {
									logger.warn("Empty code value");
								}
							}
							json.endObject();
						}
						json.endObject();
					}
					
					json.key("disabledDates");
					json.object();
					
					
					Map<Integer, BookingAwardBrandCodeEnum> codesMap;
					String prefix = dateFormatMonth.format(departureDate.getTime());
					if (result.get(departureDate.get(Calendar.YEAR)) != null && 
							(codesMap = result.get(departureDate.get(Calendar.YEAR)).get(departureDate.get(Calendar.MONTH) + 1)) != null) {
					
						int dayOfMonth = departureDate.getActualMaximum(Calendar.DAY_OF_MONTH);
						for (int k = 1; k <= dayOfMonth; k++) {
							if (codesMap.get(k) == null) {
								json.key(prefix + (k < 10 ? "0" + k : k)).value(1);
							}
						}
					}
					json.endObject();
					json.endObject();
						
					i++;
				}
				json.endObject();
			}
		}
		
		json.key("result").value("OK");
		json.endObject();
	}
	
	private void insertFlight(Map<Integer, Map<Integer, Map<Integer, BookingAwardBrandCodeEnum>>> result, 
			DirectFlightData currentFlight) {
		
		if (!currentFlight.getBrands().isEmpty()) {
		
			int year = currentFlight.getDepartureDate().get(Calendar.YEAR);
			int month = currentFlight.getDepartureDate().get(Calendar.MONTH) + 1;
			int day = currentFlight.getDepartureDate().get(Calendar.DAY_OF_MONTH);
			
			if (!result.containsKey(year)) {
				result.put(year, new LinkedHashMap<Integer, Map<Integer, BookingAwardBrandCodeEnum>>());
			}
			Map<Integer, Map<Integer, BookingAwardBrandCodeEnum>> map = result.get(year);
			
			if (!map.containsKey(month)) {
				map.put(month, new LinkedHashMap<Integer, BookingAwardBrandCodeEnum>());
			}
			
			BookingAwardBrandCodeEnum code = BookingAwardBrandCodeEnum.fromValue(currentFlight.getBrands().get(0).getCode());
			map.get(month).put(day, code);
			
		} else {
			logger.warn("Empty brand list for flight {}", currentFlight);
		}
	}
	
	private String getCabin(BookingAwardBrandCodeEnum code) {
		if (code == BookingAwardBrandCodeEnum.ECONOMY_STANDARD || code == BookingAwardBrandCodeEnum.ECONOMY_FLEX || code == BookingAwardBrandCodeEnum.CLASSICA_PLUS) {
			return "economy";
		} else { //  || code == BookingAwardBrandCodeEnum.BUSINESS_STANDARD
			return "business";
		}
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}
