package com.alitalia.aem.consumer.mmb.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.mmb.render.MmbSpecialAssistanceListRender;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.model.content.myexperience.MyExperienceModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbSpecialAssistanceExternal extends GenericBaseModel {
	
	private static String KEY_PREFIX = "specialAssistance.";
	private static String KEY_SEPARATOR = ",";
	
	private List<MmbSpecialAssistanceListRender> specialAssistanceList;
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private Logger logger = LoggerFactory.getLogger(MyExperienceModel.class);
	
	@PostConstruct
	protected void initModel() {
		
		try {
			initBaseModel(request);
			String[] specialAssistanceKeys = configuration.getSpecialAssistanceKeys().split(KEY_SEPARATOR);
			List <String> specialAssistanceKeysWheelChair = Arrays.asList(configuration.getSpecialAssistanceWheelchairKeys().split(KEY_SEPARATOR));
			specialAssistanceList = new ArrayList<MmbSpecialAssistanceListRender>();
			for (String key_suffix : specialAssistanceKeys) {
				String value = key_suffix;
				String text = i18n.get(KEY_PREFIX + key_suffix);
				String extraInfo = "";
				if (specialAssistanceKeysWheelChair.contains(value)) {
					extraInfo = "extraInfo";
				}
				MmbSpecialAssistanceListRender specialAssistance = new MmbSpecialAssistanceListRender(value, text, extraInfo);
				specialAssistanceList.add(specialAssistance);
			}
			
		} catch (Exception e) {
			logger.error("Unexpected Error: ", e);
		}
	}

	public List<MmbSpecialAssistanceListRender> getSpecialAssistanceList() {
		return specialAssistanceList;
	}
}
