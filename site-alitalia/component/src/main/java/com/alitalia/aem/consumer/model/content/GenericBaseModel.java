package com.alitalia.aem.consumer.model.content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.jcr.Session;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingBindings;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.i18n.ResourceBundleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.i18n.I18n;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMMode;

public class GenericBaseModel {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	protected SlingHttpServletRequest request;
	protected String tid;
	protected String sid;
	protected String languageCode;
	protected String marketCode;
	public final static String MARKET_CODE_CANADA="ca";
	public final static String MARKET_CODE_USA="us";

	protected I18n i18n;

	protected void initBaseModel(SlingHttpServletRequest slingHttpServletRequest) {
//		logger.debug("[GenericBaseModel] initModel");
		
		request = slingHttpServletRequest;
		
		/* workaround al problema dell'i18n: il resourceBundle non viene più preso dalla request (che ritornava un istanza di NullResourceBundle)
		 * ma, tramite bundleProvider (che è un istanza di JcrResourceBundleProvider), si riesce a prendere un'istanza di JcrResourceBundle
		 * il locale corretto viene impostanto nel momento in cui si va ad istanziare il resourceBundle
		 */
		
		SlingBindings bindings = (SlingBindings) slingHttpServletRequest.getAttribute(SlingBindings.class.getName());
		SlingScriptHelper scriptHelper = bindings.getSling();
		ResourceBundleProvider bundleProvider = scriptHelper.getServices(ResourceBundleProvider.class, "(component.name=org.apache.sling.i18n.impl.JcrResourceBundleProvider)")[0];
		
		// create resourceBundle, TID and SID
		Locale locale = AlitaliaUtils.findResourceLocale(slingHttpServletRequest.getResource());
		ResourceBundle resourceBundle = bundleProvider.getResourceBundle(locale);
		languageCode =  AlitaliaUtils.getRepositoryPathLanguage(slingHttpServletRequest.getResource());//findResourceLocale(slingHttpServletRequest.getResource()).getLanguage(); //getRepositoryPathMarket(slingHttpServletRequest.getResource());//
		marketCode = AlitaliaUtils.getRepositoryPathMarket(slingHttpServletRequest.getResource());//AlitaliaUtils.getRepositoryPathLanguage(slingHttpServletRequest.getResource()).;//AlitaliaUtils.findResourceLocale(slingHttpServletRequest.getResource()).getCountry(); ////
		tid = IDFactory.getTid();
		sid = IDFactory.getSid(slingHttpServletRequest);
		i18n = new I18n(resourceBundle);
	}

	/*
	 * execute query
	 */
	protected SearchResult executeQuery(QueryBuilder queryBuilder, SlingHttpServletRequest slingHttpServletRequest, String path, String value) {
		logger.debug("[GenericBaseModel] executeQuery");
		
		// create query description as hash map (simplest way, same as form post)
		Map<String, String> map = new HashMap<String, String>();
		// create query description as hash map (simplest way, same as form post)
		map.put("path", path);
		map.put("type", "cq:Page");
		// combine this group with OR
		map.put("1_property", "jcr:content/cq:template");
		map.put("1_property.value", value);

		// create query
		logger.debug("[GenericBaseModel] query: " + PredicateGroup.create(map).toString());
		Query query = queryBuilder.createQuery(PredicateGroup.create(map),
				slingHttpServletRequest.getResourceResolver().adaptTo(Session.class));

		SearchResult result = query.getResult();
		logger.debug("[GenericBaseModel] query result: [" + result.getHits().size() + "]");
		
		return result;
	}
	
	protected Page findPageByName(SlingHttpServletRequest request, String rootPath, String name) {
		
		final Resource resource = request.getResource();
		Resource rootResource = resource.getResourceResolver().getResource(rootPath);
		Page rootPage = rootResource.adaptTo(Page.class);
		
		return findPageByName(rootPage, name);
	}
	
	private Page findPageByName(Page page, String name) {
		
		if (page.getName().equals(name)) {
			return page;
		}
		Iterator<Page> iterator = page.listChildren();
		
		while (iterator.hasNext()) {
			Page currentPage = iterator.next();
			Page result = findPageByName(currentPage, name);
			if (result != null) {
				return result;
			}
		}
		return null;
	}
	
	protected List<Page> findPageByTemplate(SlingHttpServletRequest request, String rootPath, String templatePath) {
		
		final Resource resource = request.getResource();
		Resource rootResource = resource.getResourceResolver().getResource(rootPath);
		Page rootPage = rootResource.adaptTo(Page.class);
		
		List<Page> results = new ArrayList<Page>();
		findPageByTemplate(rootPage, templatePath, results);
		
		return results;
	}
	
	private void findPageByTemplate(Page page, String templatePath, List<Page> results) {
		
		ValueMap properties = page.getProperties();
		
		if (properties.get("cq:template") != null && properties.get("cq:template", String.class).equals(templatePath)) {
			results.add(page);
		}
		Iterator<Page> iterator = page.listChildren();
		
		while (iterator.hasNext()) {
			Page currentPage = iterator.next();
			findPageByTemplate(currentPage, templatePath, results);
		}
	}

	protected SlingHttpServletRequest getRequest() {
		return request;
	}
	
	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getMarketCode() {
		return marketCode;
	}

	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public I18n getI18n() {
		return i18n;
	}

	public void setI18n(I18n i18n) {
		this.i18n = i18n;
	}
	
	public boolean isWCMEnabled() {
		if (request != null) {
			WCMMode mode = WCMMode.fromRequest(request);
			return (mode != null && (
					WCMMode.EDIT.equals(mode) ||
					WCMMode.PREVIEW.equals(mode) ||
					WCMMode.DESIGN.equals(mode)));
		} else {
			return false;
		}
	}
	
	public boolean isAmericanCountry(){
		if (this.marketCode == null) {
			return false;
		}
		return (this.marketCode.equals(MARKET_CODE_CANADA) || this.marketCode.equals(MARKET_CODE_USA));
	}
	
}
