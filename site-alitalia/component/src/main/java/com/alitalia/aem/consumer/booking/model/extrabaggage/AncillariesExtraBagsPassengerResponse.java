package com.alitalia.aem.consumer.booking.model.extrabaggage;


import java.util.*;

public class AncillariesExtraBagsPassengerResponse extends AncillaryPassenger
{
	private int freeBaggageAdmitted;
	public final int getfreeBaggageAdmitted()
	{
		return freeBaggageAdmitted;
	}
	public final void setfreeBaggageAdmitted(int value)
	{
		freeBaggageAdmitted = value;
	}
	private boolean isMilleMiglia;
	public final boolean getisMilleMiglia()
	{
		return isMilleMiglia;
	}
	public final void setisMilleMiglia(boolean value)
	{
		isMilleMiglia = value;
	}
}