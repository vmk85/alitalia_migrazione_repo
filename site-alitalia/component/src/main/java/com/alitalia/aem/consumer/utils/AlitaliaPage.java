package com.alitalia.aem.consumer.utils;

import java.util.ArrayList;

public class AlitaliaPage {

    private String path;
    private String title;
    private String description;
    private String image;
    private String redirectPath;

    private boolean leaf;

    private ArrayList<AlitaliaPage> childrenList;

    public AlitaliaPage(String path, String title, boolean leaf) {
        setPath(path);
        setTitle(title);
        setDescription(description);
        setLeaf(leaf);
        initChildrenList();
    }

    public AlitaliaPage(String path, String title, String description, String image, String redirectPath, boolean leaf) {
        setPath(path);
        setTitle(title);
        setDescription(description);
        setImage(image);
        setRedirectPath(redirectPath);
        setLeaf(leaf);

        initChildrenList();
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setRedirectPath(String redirectPath) {
        this.redirectPath = redirectPath;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    public void initChildrenList() {
        childrenList = new ArrayList<AlitaliaPage>();
    }

    public void addChild(AlitaliaPage child) {
        childrenList.add(child);
    }

    public String getTitle() {
        return title;
    }

    public String getPath() {
        return path;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public String getRedirectPath() {
        return redirectPath;
    }

    public boolean isLeaf() {
        return leaf;
    }

    public AlitaliaPage[] getChildren() {
        AlitaliaPage[] children = new AlitaliaPage[childrenList.size()];

        return childrenList.toArray(children);
    }

}
