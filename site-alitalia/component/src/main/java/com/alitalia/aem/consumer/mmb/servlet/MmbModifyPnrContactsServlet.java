package com.alitalia.aem.consumer.mmb.servlet;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "mmbmodifypnrcontacts" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class MmbModifyPnrContactsServlet extends GenericMmbFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile MmbSession mmbSession;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		/* Recupero dati dalla request */		
		String email = request.getParameter("email");
		String telefono = request.getParameter("telefono");
		
		/* Validazione */
		Validator validator = new Validator();
		
		validator.addDirectCondition("email", email, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("email", email, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isEmail");
		
		validator.addDirectCondition("telefono", telefono, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		validator.addDirectCondition("telefono", telefono, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNumeroTelefonoValido");
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		MmbSessionContext ctx = getMmbSessionContext(request);
		
		String email = request.getParameter("email");
		String tipoTelefono = request.getParameter("tipoTelefono");
		String telefono = request.getParameter("telefono");
		String prefissoInternazionale = request.getParameter("prefissoInternazionale");
		
		mmbSession.modifyContacts(getMmbSessionContext(request), email, tipoTelefono, prefissoInternazionale, telefono);
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value("OK");
		json.key("message").value(ctx.i18n.get(MmbConstants.MESSAGE_SUCCESS_MODIFY_CONTACTS));
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}
