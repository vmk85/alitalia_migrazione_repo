package com.alitalia.aem.consumer.model.socialshare;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Source;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;

@Model(adaptables={SlingHttpServletRequest.class})
public class SocialShareBoxRedazionaleModel extends BaseSocialShareModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject @Optional @Default(values = "") @Named("titolo") @Source(value="child-resources") @Via(value="resource") 
	private String boxTitolo;
	
	@Inject @Optional @Default(values = "") @Named("sottotitolo") @Source(value="child-resources") @Via(value="resource") 
	private String boxDescrizione;
	
	@Inject @Optional @Default(values = "") @Named("link") @Source(value="child-resources") @Via(value="resource") 
	private String boxLink;
	
	@PostConstruct
	protected void initModel() {
		super.initBaseModel(request);
		try {
			title = boxTitolo;
			description = boxDescrizione;
			linkBack = boxLink;
			
			// the box component is itself a customized image component, so the image node is the resource itself
			imagePath = request.getResource().getPath();
			
		} catch (Exception e) {
			logger.error("Error while building social share data", e);
		}
	}
	
}
