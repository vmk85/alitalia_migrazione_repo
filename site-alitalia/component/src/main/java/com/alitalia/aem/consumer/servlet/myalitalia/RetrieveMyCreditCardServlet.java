package com.alitalia.aem.consumer.servlet.myalitalia;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaConstants;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.myalitaliauser.delegaterest.IAdyenRecurringPaymentDelegate;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "maretrievemycreditcard" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class RetrieveMyCreditCardServlet extends MyAlitaliaCreditCardServlet {
	
	@Reference
	protected volatile IAdyenRecurringPaymentDelegate adyenRecurringPaymentDelegate;
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		logger.info("MAPayment - RetrieveMyCreditCardServlet - performSubmit: Recupero Carte di Credito in Adyen");
		try {
			MyAlitaliaSessionContext mactx = (MyAlitaliaSessionContext) request.getSession().getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);
			if (mactx != null) {
				Object obj = mactx.getMaCustomer();
				if (obj instanceof MACustomer && obj!=null) {
					MACustomer loggedMAUserProfile = (MACustomer) obj;
					retrieveMyCreditCard(loggedMAUserProfile, AlitaliaUtils.getRepositoryPathMarket(request.getResource()), AlitaliaUtils.getRepositoryPathLanguage(request.getResource()), adyenRecurringPaymentDelegate);
					prepareResponseOK(response, "");
				} else {
					logger.error("MAPayment - RetrieveMyCreditCardServlet - performSubmit: Utente MyAlitalia non loggato");
					throw new Exception("Utente MyAlitalia non loggato");
				}
			} else {
				logger.error("MAPayment - RetrieveMyCreditCardServlet - performSubmit: Utente MyAlitalia non loggato");
				throw new Exception("Utente MyAlitalia non loggato");
			}
		} catch (Exception e) {
			logger.error("MAPayment - RetrieveMyCreditCardServlet - performSubmit: Errore durante la cancellazione della carta di credito in Adyen, Exception [{}]", e.getMessage());
			throw e;
		}
	}
	
	/**
	 * Provide an empty implementation, should be never used as
	 * MMB servlets should always be invoked with ajax requests
	 */
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		logger.warn("The emtpy saveDataIntoSession method was unexpectedly invoked.");
	}
}

