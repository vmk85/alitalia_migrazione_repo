package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Sabre Request Model for Advisory
*/
public class Advisory
{
	private String level;
	public final String getlevel()
	{
		return level;
	}
	public final void setlevel(String value)
	{
		level = value;
	}
	private String code;
	public final String getcode()
	{
		return code;
	}
	public final void setcode(String value)
	{
		code = value;
	}
	private String imageHint;
	public final String getimageHint()
	{
		return imageHint;
	}
	public final void setimageHint(String value)
	{
		imageHint = value;
	}
	private String image;
	public final String getimage()
	{
		return image;
	}
	public final void setimage(String value)
	{
		image = value;
	}
}