package com.alitalia.aem.consumer.mmb.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.consumer.mmb.MmbUtils;
import com.alitalia.aem.consumer.mmb.render.MmbFlightDataRender;

public class MmbFlightsGroup {

	private int routeId;
	private boolean isMultileg;
	private int stops;
	private RouteTypeEnum type = null;
	private List<MmbFlightDataRender> flightDataRender;
	private List<MmbAncillaryData> relatedAncillaries;
	
	public MmbFlightsGroup() {
		super();
		flightDataRender = new LinkedList<MmbFlightDataRender>();
		relatedAncillaries = new LinkedList<MmbAncillaryData>();
	}

	public boolean isMultileg() {
		return isMultileg;
	}

	public void setMultileg(boolean isMultileg) {
		this.isMultileg = isMultileg;
	}

	public Integer getRouteId() {
		return routeId;
	}
	
	public void setRouteId(Integer routeId) {
		this.routeId = routeId;
	}
	
	public int getStops() {
		return stops;
	}

	public void setStops(int stops) {
		this.stops = stops;
	}

	public RouteTypeEnum getType() {
		return type;
	}
	
	public void setType(RouteTypeEnum type) {
		this.type = type;
	}
	
	public List<MmbFlightDataRender> getFlightDataRender() {
		return flightDataRender;
	}

	public List<MmbAncillaryData> getRelatedAncillaries() {
		return relatedAncillaries;
	}
	
	public static List<MmbFlightsGroup> groupByRouteType(List<MmbFlightData> flights) {
		boolean isMultiLeg = MmbUtils.isMultiLeg(flights);
		List<List<MmbFlightData>> flightsByRoute = MmbUtils.groupFlightsByRoute(flights);
		
		List<MmbFlightsGroup> flightsGroupList = new ArrayList<MmbFlightsGroup>(flightsByRoute.size());
		for (int i = 0; i < flightsByRoute.size(); i++) {
			List<MmbFlightData> routeFlights = flightsByRoute.get(i);
			MmbFlightsGroup flightsGroup = new MmbFlightsGroup();
			flightsGroup.setType(routeFlights.get(0).getType());
			flightsGroup.setRouteId(routeFlights.get(0).getRouteId());
			flightsGroup.setStops(routeFlights.size());
			flightsGroup.setMultileg(isMultiLeg);
			for (MmbFlightData flightData : routeFlights) {
				flightsGroup.getFlightDataRender().add(new MmbFlightDataRender(flightData));
			}
			flightsGroupList.add(flightsGroup);
		}
		return flightsGroupList;
	}
	
}
