package com.alitalia.aem.consumer.booking.servlet;

import static com.alitalia.aem.consumer.utils.AlitaliaUtils.trimIfNotNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.Servlet;

import com.alitalia.aem.common.data.crmdatarest.model.*;
import com.alitalia.aem.common.messages.crmdatarest.SetCrmDataInfoRequest;
import com.alitalia.aem.common.messages.crmdatarest.SetCrmDataInfoResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.crmdata.delegaterest.ICRMDataDelegate;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.ContactType;
import com.alitalia.aem.consumer.booking.model.Passenger;
import com.alitalia.aem.consumer.booking.model.PassengerType;
import com.alitalia.aem.consumer.booking.model.PassengersData;
import com.alitalia.aem.consumer.booking.model.Sex;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.bookingaward.BookingAwardSession;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingpassengersdataconsumer" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class BookingPassengersDataServlet extends GenericBookingFormValidatorServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
		
	@SuppressWarnings("unused")
	private ComponentContext componentContext;

	@Reference
	private AlitaliaConfigurationHolder alitaliaConfiguration;
	
	@Reference
	private BookingSession bookingSession;
	
	@Reference
	private BookingAwardSession bookingAwardSession;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ICRMDataDelegate crmdataDelegate;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		BookingSessionContext ctx = getBookingSessionContext(request);
		
		if (ctx.award && ctx.loggedUserProfileData == null) {
			throw new RuntimeException("Customer booking award is null in passengers data phase.");
		}
		
		BookingPassengerDataForm form = new BookingPassengerDataForm(ctx, request);
		
		Validator validator = new Validator();
		
		/* INFORMAZIONI CONTATTO */
		validator.addDirectCondition(PassengerRawData.TIPO_RECAPITO + "1", form.tipoRecapito1,
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition(PassengerRawData.PREFISSO_RECAPITO + "1", form.prefissoRecapito1,
				BookingConstants.MESSAGE_SPECIFIC_PREFIX_NOT_VALID, "isNotEmpty");
		
		validator.addDirectCondition(PassengerRawData.VALORE_RECAPITO + "1", form.valoreRecapito1,
				BookingConstants.MESSAGE_SPECIFIC_VALORE_RECAPITO_NOT_VALID, "isNotEmpty");
		validator.addDirectCondition(PassengerRawData.VALORE_RECAPITO + "1", form.valoreRecapito1,
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNumeroTelefonoValido");
		
		validator.addDirectCondition(PassengerRawData.EMAIL, form.email,
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(PassengerRawData.EMAIL, form.email,
				BookingConstants.MESSAGE_SPECIFIC_MAIL_NOT_VALID, "isEmail");
		
		if (form.valoreRecapito2 != null
				&& !form.valoreRecapito2.isEmpty()) {
			validator.addDirectCondition(PassengerRawData.TIPO_RECAPITO + "2", form.tipoRecapito2,
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			
			validator.addDirectCondition(PassengerRawData.PREFISSO_RECAPITO + "2", form.prefissoRecapito2,
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			
			validator.addDirectCondition(PassengerRawData.VALORE_RECAPITO + "2", form.valoreRecapito2,
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNumeroTelefonoValido");
		}
		
		/* check cuit cuil code */
		//TODO Scommentare in caso di reintroduzione parametri CUIT/CUIL per Sito AR
//		if (AlitaliaUtils.getRepositoryPathMarket(request.getResource()).equalsIgnoreCase("ar")) {
//			
//			validator.addDirectCondition(PassengerRawData.AR_TAX_INFO_TYPE, form.arTaxInfoType,
//					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
//			
//			if (form.arTaxInfoType != null && !form.arTaxInfoType.isEmpty()) {
//
//				if (!form.arTaxInfoType.equals("NotAR")) {
//					validator.addDirectCondition(PassengerRawData.NUMERO_CUIT_CUIL, form.numeroCuitCuil,
//							BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
//					validator.addDirectCondition(PassengerRawData.NUMERO_CUIT_CUIL, form.numeroCuitCuil,
//							BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isCuitCuilCode");
//				} else {
//					validator.addDirectCondition(PassengerRawData.NAZIONALITA_CUIT_CUIL, form.nazionalitaCuitCuil,
//							BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
//				}
//			}
//		}
		
		validator.addDirectCondition(PassengerRawData.CHECK_AGREEMENT,
				form.checkAgreement, BookingConstants.MESSAGE_GENERIC_CHECK_FIELD, "isNotEmpty");
		
		//check for passengers with same name and surname
		String concatPassengerNames = "";
		for (int i = 0; i < form.numeroAdulti; i++) {
			String names = form.adultPassengerRawData[i].nome + form.adultPassengerRawData[i].cognome;
			names = names.replace(" ", "");
			concatPassengerNames += names + "|";
		}
		for (int i = 0; i < form.numeroBambini; i++) {
			String names = form.childrenPassengerRawData[i].nome + form.childrenPassengerRawData[i].cognome;
			names = names.replace(" ", "");
			concatPassengerNames += names + "|";
		}
		for (int i = 0; i < form.numeroNeonati; i++) {
			String names = form.infantsPassengerRawData[i].nome + form.infantsPassengerRawData[i].cognome;
			names = names.replace(" ", "");
			concatPassengerNames += names + "|";
		}
		concatPassengerNames = concatPassengerNames.substring(0, concatPassengerNames.length()-1);
		
		
		/* ADULTI */
//		ArrayList<String> adultiResponsabili = new ArrayList<String> ();
		
		for (int i = 0; i < form.numeroAdulti; i++) {
			
//			adultiResponsabili.add(String.valueOf(i+1));
			
			validator.addDirectCondition(PassengerRawData.NOME_ADULTO + (i + 1),  
					form.adultPassengerRawData[i].nome, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition(PassengerRawData.NOME_ADULTO + (i + 1), 
					form.adultPassengerRawData[i].nome, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
			
			validator.addDirectCondition(PassengerRawData.COGNOME_ADULTO + (i + 1), 
					form.adultPassengerRawData[i].cognome, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition(PassengerRawData.COGNOME_ADULTO + (i + 1), 
					form.adultPassengerRawData[i].cognome, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
			
			validator.addDirectCondition(PassengerRawData.CODICE_CARTA_FREQUENT_FLYER + (i + 1), 
					form.adultPassengerRawData[i].codiceCartaFrequenFlyer, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAlphaNumeric");
			
			validator.addCrossCondition(PassengerRawData.NOME_ADULTO + (i + 1), 
					form.adultPassengerRawData[i].nome + form.adultPassengerRawData[i].cognome, concatPassengerNames, 
					BookingConstants.MESSAGE_EQUALS_NAMES, "isNotEqualsPassengerNames");
			
			if (form.adultPassengerRawData.length > 0 
					&& form.adultPassengerRawData[i].secondoNome != null
					&& !form.adultPassengerRawData[i].secondoNome.isEmpty()) {
				validator.addDirectCondition(PassengerRawData.SECONDO_NOME_ADULTO + (i + 1),
						form.adultPassengerRawData[i].secondoNome,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
			}
			
			if (form.adultPassengerRawData[i].codiceBluebiz != null &&
					!form.adultPassengerRawData[i].codiceBluebiz.isEmpty()) {
				validator.addDirectCondition(PassengerRawData.CODICE_BLUEBIZ + "1", 
						form.adultPassengerRawData[i].codiceBluebiz,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isCodiceBluebizValido");
			}
			
			if (form.adultPassengerRawData[i].codiceSkyBonus != null &&
					!form.adultPassengerRawData[i].codiceSkyBonus.isEmpty()) {
				validator.addDirectCondition(PassengerRawData.CODICE_SKYBONUS + "1", 
						form.adultPassengerRawData[i].codiceSkyBonus,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isCodiceSkyBonusValido");
			}
			
			if (ctx.award && i == 0) {
				validator.addDirectCondition(PassengerRawData.CODICE_PIN + "1", 
						form.adultPassengerRawData[i].codicePin, 
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.setAllowedValues(PassengerRawData.CODICE_PIN + "1", 
						form.adultPassengerRawData[i].codicePin, new String[]{ctx.loggedUserProfileData.getCustomerPinCode()}, 
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD);
			}
			
			String dataNascita = "";
			
			if (form.adultPassengerRawData[i].giornoNascita != null && !form.adultPassengerRawData[i].giornoNascita.isEmpty() &&
						form.adultPassengerRawData[i].meseNascita != null && !form.adultPassengerRawData[i].meseNascita.isEmpty() &&
								form.adultPassengerRawData[i].annoNascita != null && !form.adultPassengerRawData[i].annoNascita.isEmpty()) {
			
			dataNascita = form.adultPassengerRawData[i].giornoNascita + "/"
					+ form.adultPassengerRawData[i].meseNascita + "/"
					+ form.adultPassengerRawData[i].annoNascita;
			
			}
			
			if (ctx.isSecureFlight || ctx.isSecureFlightESTA) {
				
				validator.addDirectCondition(PassengerRawData.GIORNO_NASCITA_ADULTO + (i + 1), 
						form.adultPassengerRawData[i].giornoNascita,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.addDirectCondition(PassengerRawData.MESE_NASCITA_ADULTO + (i + 1), 
						form.adultPassengerRawData[i].meseNascita,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.addDirectCondition(PassengerRawData.ANNO_NASCITA_ADULTO + (i + 1), 
						form.adultPassengerRawData[i].annoNascita,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				
				validator.addDirectCondition(PassengerRawData.GIORNO_NASCITA_ADULTO + (i + 1), dataNascita,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate", "isBeforeToday");
				validator.addDirectCondition(PassengerRawData.MESE_NASCITA_ADULTO + (i + 1), dataNascita,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate", "isBeforeToday");
				validator.addDirectCondition(PassengerRawData.ANNO_NASCITA_ADULTO + (i + 1), dataNascita,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate", "isBeforeToday");
				
				validator.addDirectCondition(PassengerRawData.SESSO_ADULTO + (i + 1), 
						form.adultPassengerRawData[i].sesso,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				
				//check for validity of adult birth date isAllowedMinAge
				DateRender lastDate = 
						new DateRender(ctx.searchElements.get(ctx.searchElements.size() - 1).getDepartureDate());
				String params = alitaliaConfiguration.getMinYearADT() + "|" + lastDate.getExtendedDate();
				validator.addCrossCondition(PassengerRawData.GIORNO_NASCITA_ADULTO + (i + 1), dataNascita, params,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedMinAge");
				validator.addCrossCondition(PassengerRawData.MESE_NASCITA_ADULTO + (i + 1), dataNascita, params,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedMinAge");
				validator.addCrossCondition(PassengerRawData.ANNO_NASCITA_ADULTO + (i + 1), dataNascita, params,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedMinAge");
				
				params = alitaliaConfiguration.getMaxYearADT() + "|" + lastDate.getExtendedDate();
				validator.addCrossCondition(PassengerRawData.GIORNO_NASCITA_ADULTO + (i + 1), dataNascita, params,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
				validator.addCrossCondition(PassengerRawData.MESE_NASCITA_ADULTO + (i + 1), dataNascita, params,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
				validator.addCrossCondition(PassengerRawData.ANNO_NASCITA_ADULTO + (i + 1), dataNascita, params,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
			}
			
			if (ctx.isSecureFlight || ctx.isSecureFlightESTA || ctx.isApis) {
				
				if (!ctx.isApis || dataNascita.length() == 10) {
					// SecureFlight: always check and validate adult birth dates (they are mandatory)
					// Apis no SecureFight: check for validity only if a complete date is provided
					validator.addDirectCondition(PassengerRawData.GIORNO_NASCITA_ADULTO + (i + 1), dataNascita,
							BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate", "isBeforeToday");
					validator.addDirectCondition(PassengerRawData.MESE_NASCITA_ADULTO + (i + 1), dataNascita,
							BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate", "isBeforeToday");
					validator.addDirectCondition(PassengerRawData.ANNO_NASCITA_ADULTO + (i + 1), dataNascita,
							BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate", "isBeforeToday");
					
					if (!dataNascita.isEmpty()) {
						//check for validity of adult birth date isAllowedMinAge
						DateRender lastDate = 
								new DateRender(ctx.searchElements.get(ctx.searchElements.size() - 1).getDepartureDate());
						String params = alitaliaConfiguration.getMinYearADT() + "|" + lastDate.getExtendedDate();
						validator.addCrossCondition(PassengerRawData.GIORNO_NASCITA_ADULTO + (i + 1), dataNascita, params,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedMinAge");
						validator.addCrossCondition(PassengerRawData.MESE_NASCITA_ADULTO + (i + 1), dataNascita, params,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedMinAge");
						validator.addCrossCondition(PassengerRawData.ANNO_NASCITA_ADULTO + (i + 1), dataNascita, params,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedMinAge");
						
						params = alitaliaConfiguration.getMaxYearADT() + "|" + lastDate.getExtendedDate();
						validator.addCrossCondition(PassengerRawData.GIORNO_NASCITA_ADULTO + (i + 1), dataNascita, params,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
						validator.addCrossCondition(PassengerRawData.MESE_NASCITA_ADULTO + (i + 1), dataNascita, params,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
						validator.addCrossCondition(PassengerRawData.ANNO_NASCITA_ADULTO + (i + 1), dataNascita, params,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
					}
				}
			}
		}
		
		
		/* BAMBINI */
		
		for (int i = 0; i < form.numeroBambini; i++) {
			
			validator.addDirectCondition(PassengerRawData.NOME_BAMBINO + (i + 1), 
					form.childrenPassengerRawData[i].nome, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition(PassengerRawData.NOME_BAMBINO + (i + 1), 
					form.childrenPassengerRawData[i].nome, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");

			validator.addDirectCondition(PassengerRawData.COGNOME_BAMBINO + (i + 1), 
					form.childrenPassengerRawData[i].cognome, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition(PassengerRawData.COGNOME_BAMBINO + (i + 1), 
					form.childrenPassengerRawData[i].cognome, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");

			validator.addCrossCondition(PassengerRawData.NOME_BAMBINO + (i + 1), 
					form.childrenPassengerRawData[i].nome + form.childrenPassengerRawData[i].cognome, 
					concatPassengerNames, 
					BookingConstants.MESSAGE_EQUALS_NAMES, "isNotEqualsPassengerNames");
			
			if (form.childrenPassengerRawData.length > 0 
					&& form.childrenPassengerRawData[i].secondoNome != null
					&& !form.childrenPassengerRawData[i].secondoNome.isEmpty()) {
				
				validator.addDirectCondition(PassengerRawData.SECONDO_NOME_BAMBINO + (i + 1),
						form.childrenPassengerRawData[i].secondoNome,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
			}
			
			
			validator.addDirectCondition(PassengerRawData.SESSO_BAMBINO + (i + 1), 
					form.childrenPassengerRawData[i].sesso,
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			
			String dataNascita = "";
			
			if (form.childrenPassengerRawData[i].giornoNascita != null && !form.childrenPassengerRawData[i].giornoNascita.isEmpty() &&
						form.childrenPassengerRawData[i].meseNascita != null && !form.childrenPassengerRawData[i].meseNascita.isEmpty() &&
								form.childrenPassengerRawData[i].annoNascita != null && !form.childrenPassengerRawData[i].annoNascita.isEmpty()) {
			
			dataNascita = form.childrenPassengerRawData[i].giornoNascita + "/"
					+ form.childrenPassengerRawData[i].meseNascita + "/"
					+ form.childrenPassengerRawData[i].annoNascita;
			
			}
			
			validator.addDirectCondition(PassengerRawData.GIORNO_NASCITA_BAMBINO + (i + 1), dataNascita,
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate", "isBeforeToday");
			validator.addDirectCondition(PassengerRawData.MESE_NASCITA_BAMBINO + (i + 1), dataNascita,
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate", "isBeforeToday");
			validator.addDirectCondition(PassengerRawData.ANNO_NASCITA_BAMBINO + (i + 1), dataNascita,
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate", "isBeforeToday");
			
			if (!dataNascita.isEmpty()) {
				// controllo sull'età del bambino
				DateRender lastDate = 
						new DateRender(ctx.searchElements.get(ctx.searchElements.size() - 1).getDepartureDate());
				if (ctx.cug == BookingSearchCUGEnum.FAMILY) {
					if(ctx.familySolutionFound) {
						String params = alitaliaConfiguration.getMaxYearFAMCHD() + "|" + lastDate.getExtendedDate();
						validator.addCrossCondition(PassengerRawData.GIORNO_NASCITA_BAMBINO + (i + 1), dataNascita, params,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
						validator.addCrossCondition(PassengerRawData.MESE_NASCITA_BAMBINO + (i + 1), dataNascita, params,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
						validator.addCrossCondition(PassengerRawData.ANNO_NASCITA_BAMBINO + (i + 1), dataNascita, params,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
					} else {
						String params = alitaliaConfiguration.getMaxYearCHD() + "|" + lastDate.getExtendedDate();
						validator.addCrossCondition(PassengerRawData.GIORNO_NASCITA_BAMBINO + (i + 1), dataNascita, params,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
						validator.addCrossCondition(PassengerRawData.MESE_NASCITA_BAMBINO + (i + 1), dataNascita, params,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
						validator.addCrossCondition(PassengerRawData.ANNO_NASCITA_BAMBINO + (i + 1), dataNascita, params,
								BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
					}
				} else {
					String params = alitaliaConfiguration.getMaxYearCHD() + "|" + lastDate.getExtendedDate();
					validator.addCrossCondition(PassengerRawData.GIORNO_NASCITA_BAMBINO + (i + 1), dataNascita, params,
							BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
					validator.addCrossCondition(PassengerRawData.MESE_NASCITA_BAMBINO + (i + 1), dataNascita, params,
							BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
					validator.addCrossCondition(PassengerRawData.ANNO_NASCITA_BAMBINO + (i + 1), dataNascita, params,
							BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
				}
				
				String params = alitaliaConfiguration.getMinYearCHD() + "|" + lastDate.getExtendedDate();
				validator.addCrossCondition(PassengerRawData.GIORNO_NASCITA_BAMBINO + (i + 1), dataNascita, params,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedMinAge");
				validator.addCrossCondition(PassengerRawData.MESE_NASCITA_BAMBINO + (i + 1), dataNascita, params,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedMinAge");
				validator.addCrossCondition(PassengerRawData.ANNO_NASCITA_BAMBINO + (i + 1), dataNascita, params,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedMinAge");
			}
		}
		
		
		/* NEONATI */
		
		for (int i = 0; i < form.numeroNeonati; i++) {
			
			validator.addDirectCondition(PassengerRawData.NOME_NEONATO + (i + 1), 
					form.infantsPassengerRawData[i].nome, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition(PassengerRawData.NOME_NEONATO + (i + 1), 
					form.infantsPassengerRawData[i].nome, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
			
			validator.addDirectCondition(PassengerRawData.COGNOME_NEONATO + (i + 1), 
					form.infantsPassengerRawData[i].cognome, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition(PassengerRawData.COGNOME_NEONATO + (i + 1), 
					form.infantsPassengerRawData[i].cognome, 
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");

			validator.addCrossCondition(PassengerRawData.NOME_NEONATO + (i + 1), 
					form.infantsPassengerRawData[i].nome + form.infantsPassengerRawData[i].cognome,
					concatPassengerNames, 
					BookingConstants.MESSAGE_EQUALS_NAMES, "isNotEqualsPassengerNames");
			
			if (form.infantsPassengerRawData.length > 0 
					&& form.infantsPassengerRawData[i].secondoNome != null
					&& !form.infantsPassengerRawData[i].secondoNome.isEmpty()) {
				validator.addDirectCondition(PassengerRawData.SECONDO_NOME_NEONATO + (i + 1),
						form.infantsPassengerRawData[i].secondoNome,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
			}
			
			if (ctx.isSecureFlight || ctx.isSecureFlightESTA) {
				validator.addDirectCondition(PassengerRawData.SESSO_NEONATO + (i + 1), 
						form.infantsPassengerRawData[i].sesso,
						BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			}
			
			String dataNascita = "";
			
			if (form.infantsPassengerRawData[i].giornoNascita != null && !form.infantsPassengerRawData[i].giornoNascita.isEmpty() &&
						form.infantsPassengerRawData[i].meseNascita != null && !form.infantsPassengerRawData[i].meseNascita.isEmpty() &&
								form.infantsPassengerRawData[i].annoNascita != null && !form.infantsPassengerRawData[i].annoNascita.isEmpty()) {
			
			dataNascita = form.infantsPassengerRawData[i].giornoNascita + "/"
					+ form.infantsPassengerRawData[i].meseNascita + "/"
					+ form.infantsPassengerRawData[i].annoNascita;
			
			}
			
			validator.addDirectCondition(PassengerRawData.GIORNO_NASCITA_NEONATO + (i + 1), dataNascita,
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate", "isBeforeToday");
			validator.addDirectCondition(PassengerRawData.MESE_NASCITA_NEONATO + (i + 1), dataNascita,
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate", "isBeforeToday");
			validator.addDirectCondition(PassengerRawData.ANNO_NASCITA_NEONATO + (i + 1), dataNascita,
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate", "isBeforeToday");
			
			if (!dataNascita.isEmpty()) {
				// controllo sull'età del neonato
				DateRender lastDate = 
						new DateRender(ctx.searchElements.get(ctx.searchElements.size() - 1).getDepartureDate());
				String params = alitaliaConfiguration.getMaxYearINF() + "|" + lastDate.getExtendedDate();
				validator.addCrossCondition(PassengerRawData.GIORNO_NASCITA_NEONATO + (i + 1), dataNascita, params,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
				validator.addCrossCondition(PassengerRawData.MESE_NASCITA_NEONATO + (i + 1), dataNascita, params,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
				validator.addCrossCondition(PassengerRawData.ANNO_NASCITA_NEONATO + (i + 1), dataNascita, params,
						BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isAllowedAge");
			}
			
			/*validator.addDirectCondition(PassengerRawData.ADULTO_RESPONSABILE_NEONATO + (i + 1),
					form.infantsPassengerRawData[i].adultoResponsabile,
					BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNotEmpty");
			
			validator.setAllowedValuesMessagePattern(PassengerRawData.ADULTO_RESPONSABILE_NEONATO + (i + 1), 
					form.infantsPassengerRawData[i].adultoResponsabile,
					adultiResponsabili.toArray(), 
					BookingConstants.MESSAGE_RESPONSIBLE_ADULTS_NOT_VALID, "Adulto responsabile");
			adultiResponsabili.remove(form.infantsPassengerRawData[i].adultoResponsabile);*/
		}
		
		
		/* Risultato della validazione */
		ResultValidation resultValidation = validator.validate();
/*		commentato codice perché a fronte di una seconda submit sulla pax-data, non veniva chiamata la BL
		if (resultValidation.getResult() && !ctx.passengersSubmitted) {
*/
		if (resultValidation.getResult()) {
			
			// temporarly update passengers data in context after a successful validation (the related
			// data will be required for frequent flyers validation and for seat map selection step)
			bookingSession.updatePassengersData(ctx, form.toPassengersData(ctx));	
			
			// perform the frequent flyer codes validation with RegularExpression
			List<AdultPassengerData> adultPassengersCheckCodeWithWrongFrequentFlyer = bookingSession.checkCodePassengerDetails(ctx);
			
			boolean errorCodeFrequentFlyer = false;
					
			if (adultPassengersCheckCodeWithWrongFrequentFlyer != null) {
				for (AdultPassengerData adultPaxCheckWithWrongFrequentFlyer : adultPassengersCheckCodeWithWrongFrequentFlyer) {
					logger.error("checkCodePassengerDetails. Millemiglia Validation Failure. Passenger: [{}], MMCode: [{}]", adultPaxCheckWithWrongFrequentFlyer.getName() + " "+adultPaxCheckWithWrongFrequentFlyer.getLastName(), adultPaxCheckWithWrongFrequentFlyer.getFrequentFlyerCode());
					//resultValidation.setResult(false);
					// find the relevant passenger by frequent flyer data
					for (int i = 0; i < form.numeroAdulti; i++) {
						if (adultPaxCheckWithWrongFrequentFlyer.getFrequentFlyerType().getCode().equals(form.adultPassengerRawData[i].programmaFrequenFlyer) 
								&& adultPaxCheckWithWrongFrequentFlyer.getFrequentFlyerCode().equals(form.adultPassengerRawData[i].codiceCartaFrequenFlyer)) {
							String validationRegExp =  adultPaxCheckWithWrongFrequentFlyer.getFrequentFlyerType().getRegularExpressionValidation();
							if (!form.adultPassengerRawData[i].codiceCartaFrequenFlyer.matches(validationRegExp)) {
								resultValidation.setResult(false);
								errorCodeFrequentFlyer = true;
								resultValidation.getFields().put(
										PassengerRawData.CODICE_CARTA_FREQUENT_FLYER + (i + 1), "Lunghezza codice tessera non corretto");
							}
							
						}
					}	
				}
			}
			
			if (!errorCodeFrequentFlyer){
				
				// perform the frequent flyer codes validation using the service
				List<AdultPassengerData> adultPassengersWithWrongFrequentFlyer = new ArrayList<>();
				adultPassengersWithWrongFrequentFlyer = bookingSession.submitPassengerDetails(ctx);
				
				boolean errorSabreCodeFrequentFlyer = false;
				if (adultPassengersWithWrongFrequentFlyer != null && adultPassengersWithWrongFrequentFlyer.size() > 0) {
					

					for(int i = 0; i < adultPassengersWithWrongFrequentFlyer.size(); i++){
						logger.error("SubmitPassengerDetails. Millemiglia Validation Failure. Passenger: [{}], MMCode: [{}]", adultPassengersWithWrongFrequentFlyer.get(i).getName() + " "+adultPassengersWithWrongFrequentFlyer.get(i).getLastName(), adultPassengersWithWrongFrequentFlyer.get(i).getFrequentFlyerCode());
						//resultValidation.setResult(false);
						if (adultPassengersWithWrongFrequentFlyer.get(i).getFrequentFlyerType() != null && adultPassengersWithWrongFrequentFlyer.get(i).getFrequentFlyerType().getErrorCodeCardFrequentFlyer() != null && adultPassengersWithWrongFrequentFlyer.get(i).getFrequentFlyerType().getErrorCodeCardFrequentFlyer().equals("VALIDATION.FREQUENTFLYER.INVALID")){
							errorSabreCodeFrequentFlyer = true;
							resultValidation.setResult(false);
							resultValidation.getFields().put(
									PassengerRawData.CODICE_CARTA_FREQUENT_FLYER + (i + 1), "Programma Frequent Flyer o Codice Carta non validi");
						}
					}
					
					if (!errorSabreCodeFrequentFlyer){
						for (int s = 0; s < form.numeroAdulti; s++) {
							String programmaFrequentFlyer 	= "";
							programmaFrequentFlyer 	= form.adultPassengerRawData[s].programmaFrequenFlyer;
							/*Ragionamento al contrario :-) : se viene valorizzato l'oggetto adultPassengersWithWrongFrequentFlyer significa che c'è un eccezione gestita che torna dalla BL nel caso di AZ*/
                            String codiceCartaFrequentFlyer = "";
                            codiceCartaFrequentFlyer = form.adultPassengerRawData[s].codiceCartaFrequenFlyer;
                            if (adultPassengersWithWrongFrequentFlyer.get(s).getFrequentFlyerType().getCode().equals(programmaFrequentFlyer)
                                    || adultPassengersWithWrongFrequentFlyer.get(s).getFrequentFlyerCode().equals(codiceCartaFrequentFlyer)) {
                                resultValidation.setResult(false);
                                resultValidation.getFields().put(
                                        PassengerRawData.CODICE_CARTA_FREQUENT_FLYER + (s + 1), "Codice tessera non valido");

                            }
						}
					}
					
				}			
			}
			
			
		}
/*		commentato codice perché a fronte di una seconda submit sulla pax-data, non veniva chiamata la BL
		ctx.passengersSubmitted = resultValidation.getResult();
*/

		return resultValidation;
	}
	

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		try {
			
			BookingSessionContext ctx = getBookingSessionContext(request);
			
			if (ctx.award && ctx.loggedUserProfileData == null) {
				throw new RuntimeException("Customer booking award is null in passengers data phase.");
			}
			
			BookingPassengerDataForm form = new BookingPassengerDataForm(ctx, request);
			
			bookingSession.confirmPassengersData(ctx, form.toPassengersData(ctx));
			
			if (ctx.award) {
				Long realAwardPrice = bookingAwardSession.retrieveRealAwardPrice(ctx);
				bookingAwardSession.updateAwardPrice(realAwardPrice, ctx);
			}
			
			String baseUrl = request.getResource().getResourceResolver().map(
					AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
			
			String nextPage = alitaliaConfiguration.getBookingAncillaryPage();
			
			TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
			json.object();
			response.setContentType("application/json");
			json.key("result").value("OK");
			
			boolean skipAncillaryStep = bookingSession.skipAncillaryStep(ctx);
			if (skipAncillaryStep) {
				bookingSession.confirmAncillaryData(ctx);
				nextPage = alitaliaConfiguration.getBookingPaymentPage();
			}

			try{
				if (request.getParameter("receivenewsletter") != null && request.getParameter("receivenewsletter").equals("accepted")){
					iscrizioneNL(request);
				}
			}catch (Exception e){

			}
			
			json.key("redirect").value(baseUrl + nextPage);
			json.endObject();
			
		} catch (GenericFormValidatorServletException e) {
			logger.error("Unexpected error", e);
			throw e;
		} catch (Exception e) {
			throw new GenericFormValidatorServletException(false);
		}
	}

	private void iscrizioneNL(SlingHttpServletRequest request) {
		String UIDBooking = request.getParameter("UID");

		if (UIDBooking == null){
			UIDBooking = "";
		}

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		SetCrmDataInfoResponse setCrmDataInfoResponse = new SetCrmDataInfoResponse();
		SetCrmDataInfoRequest setCrmDataInfoRequest = new SetCrmDataInfoRequest(IDFactory.getTid(), IDFactory.getSid(request));
		setCrmDataInfoRequest.setConversationID(IDFactory.getTid());
		setCrmDataInfoRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
		setCrmDataInfoRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
		InfoCliente infoCliente = new InfoCliente();
		infoCliente.setNome(request.getParameter("nomeAdulto_1"));
		infoCliente.setCognome(request.getParameter("cognomeAdulto_1"));
		infoCliente.setSecondoNome(request.getParameter("secondoNomeAdulto_1"));
		infoCliente.setSesso(request.getParameter("sessoAdulto_1"));

		String dataNascita="";
		String anno=request.getParameter("anno_dataNascitaAdulto_1");
		String mese=request.getParameter("mese_dataNascitaAdulto_1");
		String giorno=request.getParameter("giorno_dataNascitaAdulto_1");
		if (anno!=null && mese!=null && giorno!=null){dataNascita=anno + "-" + mese + "-" + giorno;}
		infoCliente.setDataNascita(dataNascita);

		setCrmDataInfoRequest.setInfoCliente(infoCliente);

		RecapitiTelefonici recapitiTelefonici = new RecapitiTelefonici();
		recapitiTelefonici.setNumero(request.getParameter("valoreRecapito_1"));
		String prefix=request.getParameter("prefix");
		prefix=prefix.substring(prefix.indexOf("+")+1,prefix.indexOf(")"));
		recapitiTelefonici.setPrefissioNazionale(prefix);
		recapitiTelefonici.setTipo(request.getParameter("tipoRecapito_1"));
		setCrmDataInfoRequest.setRecapitiTelefonici(recapitiTelefonici);

		setCrmDataInfoRequest.setCanale("BOOKING");
		setCrmDataInfoRequest.setEmail(request.getParameter("email"));
		setCrmDataInfoRequest.setIdMyAlitalia("");
		setCrmDataInfoRequest.setUnsubscribeNewsletter(false);
		setCrmDataInfoRequest.setFrequenzaComunicazioni("1");
		setCrmDataInfoRequest.setListaConsensi(setConsensi(true));

		List<Documento> elencoDocumenti = new ArrayList<>();
		Documento d = new Documento();
		elencoDocumenti.add(d);

		PreferenzePersonali preferenzePersonali1 = new PreferenzePersonali();
		preferenzePersonali1.setLinguaComunicazioniCommerciali(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
		preferenzePersonali1.setLinguaComunicazioniServizio(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
		PreferenzeViaggio preferenzeViaggio = new PreferenzeViaggio();

		CanaleComunicazionePreferito canaleComunicazionePreferito = new CanaleComunicazionePreferito();
		canaleComunicazionePreferito.setTipo("MAIL");
		canaleComunicazionePreferito.setSelezionato(true);
		canaleComunicazionePreferito.setDataUltimoAggiornamento(dateFormat.format(date));

		setCrmDataInfoRequest.setElencoDocumenti(elencoDocumenti);
		setCrmDataInfoRequest.setPreferenzePersonali(preferenzePersonali1);
		setCrmDataInfoRequest.setPreferenzeViaggio(preferenzeViaggio);
		List<CanaleComunicazionePreferito> canaleComunicazionePreferitos = new ArrayList<>();

		canaleComunicazionePreferitos.add(canaleComunicazionePreferito);
		setCrmDataInfoRequest.setCanaleComunicazionePreferito(new ArrayList<>());
		setCrmDataInfoRequest.setCanaleComunicazionePreferito(canaleComunicazionePreferitos);
		setCrmDataInfoRequest.setIdMyAlitalia(UIDBooking);

		setCrmDataInfoResponse = crmdataDelegate.setAzCrmDataInfo(setCrmDataInfoRequest);
		if (setCrmDataInfoResponse != null) {
			if (!setCrmDataInfoResponse.getResult().getCode().equals("0")) {
				logger.error("[SetCRMDataNewsLetterServlet] - Errore durante l'invocazione del servizio.");
			} else {
				logger.error("[SetCRMDataNewsLetterServlet] - Errore durante l'invocazione del servizio.");
			}
		}
	}

	private List<Consenso> setConsensi(boolean consensoValue) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();

		List<Consenso> listaConsensi = new ArrayList<>();
		Consenso c1;
//        c2,c3 ;
		c1= new Consenso();
//        c2= new Consenso();
//        c3= new Consenso();

		c1.setFlagConsenso(consensoValue);
        /*c2.setFlagConsenso(consensoValue);
        c3.setFlagConsenso(consensoValue);*/
//        c2.setFlagConsenso(false);
//        c3.setFlagConsenso(false);

		c1.setTipoConsenso("COM");
//        c2.setTipoConsenso("PRN");
//        c3.setTipoConsenso("PRO");

		c1.setDataUltimoAggiornamento(dateFormat.format(date));
//        c2.setDataUltimoAggiornamento(dateFormat.format(date));
//        c3.setDataUltimoAggiornamento(dateFormat.format(date));

		listaConsensi.add(c1);
//        listaConsensi.add(c2);
//        listaConsensi.add(c3);

		return  listaConsensi;
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return alitaliaConfiguration;
	}
	
}


/**
 * Classe interna per gestire i dati della form e la loro interazione con BookingSessionContext
 */
class BookingPassengerDataForm {
	
	public int numeroAdulti;
	public int numeroBambini;
	public int numeroNeonati;
	
	public String email;
	public String tipoRecapito1;
	public String prefissoRecapito1;
	public String valoreRecapito1;
	public String tipoRecapito2;
	public String prefissoRecapito2;
	public String valoreRecapito2;
	public String checkAgreement;
	
	public String arTaxInfoType;
	public String numeroCuitCuil;
	public String nazionalitaCuitCuil;
	
	public PassengerRawData[] adultPassengerRawData;
	public PassengerRawData[] childrenPassengerRawData;
	public PassengerRawData[] infantsPassengerRawData;
	
	public BookingPassengerDataForm(BookingSessionContext ctx, SlingHttpServletRequest request) {

		/* INFORMAZIONI CONTATTO */
		email = request.getParameter(PassengerRawData.EMAIL);
		
		// Contatto 1
		tipoRecapito1 = request.getParameter(PassengerRawData.TIPO_RECAPITO + "1");
		prefissoRecapito1 = request.getParameter(PassengerRawData.PREFISSO_RECAPITO + "1");
		valoreRecapito1 = request.getParameter(PassengerRawData.VALORE_RECAPITO + "1");
		
		// Contatto 2
		tipoRecapito2 = request.getParameter(PassengerRawData.TIPO_RECAPITO + "2");
		prefissoRecapito2 = request.getParameter(PassengerRawData.PREFISSO_RECAPITO + "2");
		valoreRecapito2 = request.getParameter(PassengerRawData.VALORE_RECAPITO + "2");
		
		/* CUIT/CUIL */
		arTaxInfoType = request.getParameter(PassengerRawData.AR_TAX_INFO_TYPE);
		numeroCuitCuil = request.getParameter(PassengerRawData.NUMERO_CUIT_CUIL);
		nazionalitaCuitCuil = request.getParameter(PassengerRawData.NAZIONALITA_CUIT_CUIL);
		
		checkAgreement = request.getParameter(PassengerRawData.CHECK_AGREEMENT);
		
		/* ADULTI */
		numeroAdulti = ctx.searchPassengersNumber.getNumAdults() + ctx.searchPassengersNumber.getNumYoung();
		adultPassengerRawData = new PassengerRawData[numeroAdulti];
		
		for (int i = 0; i < numeroAdulti; i++) {
			adultPassengerRawData[i] = new PassengerRawData();
			
			adultPassengerRawData[i].nome = 
					request.getParameter(PassengerRawData.NOME_ADULTO + (i + 1));
			adultPassengerRawData[i].cognome =
					request.getParameter(PassengerRawData.COGNOME_ADULTO + (i + 1));
			adultPassengerRawData[i].programmaFrequenFlyer = ctx.award && i == 0 ? "AZ" :
					request.getParameter(PassengerRawData.PROGRAMMA_FREQUENT_FLYER + (i + 1));
			
			String codiceFrequentFlyer = ctx.award && i == 0 ? ctx.loggedUserProfileData.getCustomerNumber() : 
				request.getParameter(PassengerRawData.CODICE_CARTA_FREQUENT_FLYER + (i + 1));
			adultPassengerRawData[i].codiceCartaFrequenFlyer = StringUtils.stripStart(codiceFrequentFlyer, "0");
					
			if (i == 0) {
				adultPassengerRawData[i].codiceBluebiz =
						request.getParameter(PassengerRawData.CODICE_BLUEBIZ + (i + 1));
				
				adultPassengerRawData[i].codiceSkyBonus = 
						request.getParameter(PassengerRawData.CODICE_SKYBONUS + (i + 1));
				
				adultPassengerRawData[i].codicePin =
						request.getParameter(PassengerRawData.CODICE_PIN + (i + 1));
			}
			if (ctx.isSecureFlight) {
				adultPassengerRawData[i].secondoNome = 
						request.getParameter(PassengerRawData.SECONDO_NOME_ADULTO + (i + 1));
			}
			if (ctx.isApis) {
				adultPassengerRawData[i].numeroPassaporto =
						request.getParameter(PassengerRawData.NUMERO_PASSAPORTO_ADULTO + (i + 1));
				adultPassengerRawData[i].nazionalita =
						request.getParameter(PassengerRawData.NAZIONALITA_ADULTO + (i + 1));
			}
			if (ctx.isSecureFlight || ctx.isApis) {
				adultPassengerRawData[i].giornoNascita = 
						request.getParameter(PassengerRawData.GIORNO_NASCITA_ADULTO + (i + 1));
				adultPassengerRawData[i].meseNascita = 
						request.getParameter(PassengerRawData.MESE_NASCITA_ADULTO + (i + 1));
				adultPassengerRawData[i].annoNascita = 
						request.getParameter(PassengerRawData.ANNO_NASCITA_ADULTO + (i + 1));
				adultPassengerRawData[i].sesso =
						request.getParameter(PassengerRawData.SESSO_ADULTO + (i + 1));
			}
		}
		
		/* BAMBINI */
		numeroBambini = ctx.searchPassengersNumber.getNumChildren();
		childrenPassengerRawData = new PassengerRawData[numeroBambini];
		
		for (int i = 0; i < numeroBambini; i++) {
			childrenPassengerRawData[i] = new PassengerRawData();
			
			childrenPassengerRawData[i].nome = 
					request.getParameter(PassengerRawData.NOME_BAMBINO + (i + 1));
			childrenPassengerRawData[i].cognome = 
					request.getParameter(PassengerRawData.COGNOME_BAMBINO + (i + 1));
			childrenPassengerRawData[i].giornoNascita = 
					request.getParameter(PassengerRawData.GIORNO_NASCITA_BAMBINO + (i + 1));
			childrenPassengerRawData[i].meseNascita = 
					request.getParameter(PassengerRawData.MESE_NASCITA_BAMBINO + (i + 1));
			childrenPassengerRawData[i].annoNascita = 
					request.getParameter(PassengerRawData.ANNO_NASCITA_BAMBINO + (i + 1));
			childrenPassengerRawData[i].sesso = 
					request.getParameter(PassengerRawData.SESSO_BAMBINO + (i + 1));
			
			if (ctx.isSecureFlight) {
				childrenPassengerRawData[i].secondoNome = 
						request.getParameter(PassengerRawData.SECONDO_NOME_BAMBINO + (i + 1));
			}
			if (ctx.isApis) {
				childrenPassengerRawData[i].numeroPassaporto = 
						request.getParameter(PassengerRawData.NUMERO_PASSAPORTO_BAMBINO + (i + 1));
				childrenPassengerRawData[i].nazionalita = 
						request.getParameter(PassengerRawData.NAZIONALITA_BAMBINO + (i + 1));
			}
		}
		
		/* NEONATI */
		numeroNeonati = ctx.searchPassengersNumber.getNumInfants();
		infantsPassengerRawData = new PassengerRawData[numeroNeonati];
		
		for (int i = 0; i < numeroNeonati; i++) {
			infantsPassengerRawData[i] = new PassengerRawData();
			
			infantsPassengerRawData[i].nome = 
					request.getParameter(PassengerRawData.NOME_NEONATO + (i + 1));
			infantsPassengerRawData[i].cognome = 
					request.getParameter(PassengerRawData.COGNOME_NEONATO + (i + 1));
			infantsPassengerRawData[i].giornoNascita = 
					request.getParameter(PassengerRawData.GIORNO_NASCITA_NEONATO + (i + 1));
			infantsPassengerRawData[i].meseNascita = 
					request.getParameter(PassengerRawData.MESE_NASCITA_NEONATO + (i + 1));
			infantsPassengerRawData[i].annoNascita = 
					request.getParameter(PassengerRawData.ANNO_NASCITA_NEONATO + (i + 1));
			infantsPassengerRawData[i].sesso = 
					request.getParameter(PassengerRawData.SESSO_NEONATO + (i + 1));
//			infantsPassengerRawData[i].adultoResponsabile = 
//					request.getParameter(PassengerRawData.ADULTO_RESPONSABILE_NEONATO + (i + 1));
			infantsPassengerRawData[i].adultoResponsabile =  String.valueOf(i+1);
			
			if (ctx.isSecureFlight) {
				infantsPassengerRawData[i].secondoNome = 
						request.getParameter(PassengerRawData.SECONDO_NOME_NEONATO + (i + 1));
			}
			if (ctx.isApis) {
				infantsPassengerRawData[i].numeroPassaporto = 
						request.getParameter(PassengerRawData.NUMERO_PASSAPORTO_NEONATO + (i + 1));
				infantsPassengerRawData[i].nazionalita = 
						request.getParameter(PassengerRawData.NAZIONALITA_NEONATO + (i + 1));
			}
		}
	}
	
	public PassengersData toPassengersData(BookingSessionContext ctx) {
		
		PassengersData passengersData = new PassengersData();
		Passenger pax;
		
		// INFORMAZIONI DI CONTATTO
		if (this.valoreRecapito1 != null && !"".equals(this.valoreRecapito1)) {
			passengersData.setContact1(ContactType.valueOf(this.tipoRecapito1), this.prefissoRecapito1,
					trimIfNotNull(this.valoreRecapito1));
		}
		if (this.valoreRecapito2 != null && !"".equals(this.valoreRecapito2)) {
			passengersData.setContact2(ContactType.valueOf(this.tipoRecapito2), this.prefissoRecapito2,
					trimIfNotNull(this.valoreRecapito2));
		}
		passengersData.setEmail(trimIfNotNull(this.email));
		
		// CUIT/CUIL
		passengersData.setArTaxInfoType(arTaxInfoType);
		passengersData.setNumeroCuitCuil(trimIfNotNull(numeroCuitCuil));
		passengersData.setNazionalitaCuitCuil(nazionalitaCuitCuil);
		
		// ADULTI
		for (int i = 0; i < this.numeroAdulti; i++) {
			pax = new Passenger();
			pax.setPassengerType(PassengerType.ADULT);
			pax.setName(trimIfNotNull(this.adultPassengerRawData[i].nome));
			pax.setSurname(trimIfNotNull(this.adultPassengerRawData[i].cognome));
			pax.setFrequentFlyerProgram(this.adultPassengerRawData[i].programmaFrequenFlyer);
			pax.setFrequentFlyerCardNumber(trimIfNotNull(this.adultPassengerRawData[i].codiceCartaFrequenFlyer));
			
			if (i == 0) {
				pax.setBlueBizCode(trimIfNotNull(this.adultPassengerRawData[i].codiceBluebiz));
				pax.setSkyBonusCode(trimIfNotNull(this.adultPassengerRawData[i].codiceSkyBonus));
			}
			
			if (ctx.isSecureFlight || ctx.isSecureFlightESTA) {
				pax.setSecondName(trimIfNotNull(this.adultPassengerRawData[i].secondoNome));
			}
			
			if (ctx.isApis) {
				pax.setPassportNumber(trimIfNotNull(this.adultPassengerRawData[i].numeroPassaporto));
				pax.setNationality(this.adultPassengerRawData[i].nazionalita);
			}
			
			if (ctx.isSecureFlight || ctx.isSecureFlightESTA || ctx.isApis) {
				if (this.adultPassengerRawData[i].giornoNascita != null 
						&& !"".equals(this.adultPassengerRawData[i].giornoNascita)
						&& this.adultPassengerRawData[i].meseNascita != null 
						&& !"".equals(this.adultPassengerRawData[i].meseNascita)
						&& this.adultPassengerRawData[i].annoNascita != null 
						&& !"".equals(this.adultPassengerRawData[i].annoNascita)) {
					
					pax.setBirthDate(toCalendar(
							this.adultPassengerRawData[i].giornoNascita, 
							this.adultPassengerRawData[i].meseNascita, 
							this.adultPassengerRawData[i].annoNascita));
				}
				if (this.adultPassengerRawData[i].sesso != null && 
						!"".equals(this.adultPassengerRawData[i].sesso)) {
					pax.setSex(Sex.fromValue(this.adultPassengerRawData[i].sesso));
				}
			}
			
			passengersData.addPassenger(pax);
		}
		
		// BAMBINI
		for (int i = 0; i < this.numeroBambini; i++) {
			pax = new Passenger();
			pax.setPassengerType(PassengerType.CHILD);
			
			pax.setName(trimIfNotNull(this.childrenPassengerRawData[i].nome));
			pax.setSurname(trimIfNotNull(this.childrenPassengerRawData[i].cognome));
			pax.setBirthDate(toCalendar(
					this.childrenPassengerRawData[i].giornoNascita, 
					this.childrenPassengerRawData[i].meseNascita, 
					this.childrenPassengerRawData[i].annoNascita));
			
			if (this.childrenPassengerRawData[i].sesso != null && 
					!"".equals(this.childrenPassengerRawData[i].sesso)) {
				pax.setSex(Sex.fromValue(this.childrenPassengerRawData[i].sesso));
			}
			
			if (ctx.isSecureFlight || ctx.isSecureFlightESTA) {
				pax.setSecondName(trimIfNotNull(this.childrenPassengerRawData[i].secondoNome));
			}
			
			if (ctx.isApis) {
				pax.setPassportNumber(trimIfNotNull(this.childrenPassengerRawData[i].numeroPassaporto));
				pax.setNationality(this.childrenPassengerRawData[i].nazionalita);
			}
			
			passengersData.addPassenger(pax);
		}
		
		// NEONATI
		for (int i = 0; i < this.numeroNeonati; i++) {
			pax = new Passenger();
			pax.setPassengerType(PassengerType.INFANT);
			
			pax.setName(trimIfNotNull(this.infantsPassengerRawData[i].nome));
			pax.setSurname(trimIfNotNull(this.infantsPassengerRawData[i].cognome));
			pax.setBirthDate(toCalendar(
					this.infantsPassengerRawData[i].giornoNascita, 
					this.infantsPassengerRawData[i].meseNascita, 
					this.infantsPassengerRawData[i].annoNascita));
			
			if (this.infantsPassengerRawData[i].sesso != null && 
					!"".equals(this.infantsPassengerRawData[i].sesso)) {
				pax.setSex(Sex.fromValue(this.infantsPassengerRawData[i].sesso));
			}
			
			if (ctx.isSecureFlight || ctx.isSecureFlightESTA) {
				pax.setSecondName(trimIfNotNull(this.infantsPassengerRawData[i].secondoNome));
			}
			
			if (ctx.isApis) {
				pax.setPassportNumber(trimIfNotNull(this.infantsPassengerRawData[i].numeroPassaporto));
				pax.setNationality(this.infantsPassengerRawData[i].nazionalita);
			}
			
			String adultoResponsabile = this.infantsPassengerRawData[i].adultoResponsabile;
			int numAdultoResponsabile = Integer.parseInt(
					adultoResponsabile.substring(adultoResponsabile.length() - 1, adultoResponsabile.length()));
			pax.setReferenceAdult(numAdultoResponsabile - 1);
			
			passengersData.addPassenger(pax);
		}
		
		return passengersData;
	}
	
	private Calendar toCalendar(String day, String month, String year) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		df.setLenient(false);
		try {
			Calendar date = Calendar.getInstance();
			date.setTime(df.parse(day + "/" + month + "/" + year));
			return date;
		} catch (ParseException e) {
			// (this method is meant to be used only on safe, already-validated input)
			throw new RuntimeException(e);
		}
	}
	
}

/**
 * Classe interna che funge da model per i dati raw passati dalla form.
 * Verrà gestito un array di PassengerRawData. Ogni elemento dell'array conterrà i dati di ogni passeggero presente nella form
 * 
 * Nella classe sono definiti, come attributi statici, i name dei campi della form.
 */
class PassengerRawData {
	public static final String NOME_ADULTO = "nomeAdulto_";
	public static final String SECONDO_NOME_ADULTO = "secondoNomeAdulto_";
	public static final String COGNOME_ADULTO = "cognomeAdulto_";

	public static final String GIORNO_NASCITA_ADULTO = "giorno_dataNascitaAdulto_";
	public static final String MESE_NASCITA_ADULTO = "mese_dataNascitaAdulto_";
	public static final String ANNO_NASCITA_ADULTO = "anno_dataNascitaAdulto_";
	
	public static final String SESSO_ADULTO = "sessoAdulto_";
	public static final String NUMERO_PASSAPORTO_ADULTO = "numeroPassaportoAdulto_";
	public static final String NAZIONALITA_ADULTO = "nazionalitaAdulto_";
	
	public static final String PROGRAMMA_FREQUENT_FLYER = "programmaFrequentFlyer_";
	public static final String CODICE_CARTA_FREQUENT_FLYER = "codiceCartaFrequentFlyer_";
	public static final String CODICE_BLUEBIZ = "codiceBluebiz_";
	public static final String CODICE_SKYBONUS = "codiceSkyBonus_";
	public static final String CODICE_PIN = "codicePin_";
	
	public static final String NOME_BAMBINO = "nomeBambino_";
	public static final String SECONDO_NOME_BAMBINO = "secondoNomeBambino_";
	public static final String COGNOME_BAMBINO = "cognomeBambino_";
	public static final String GIORNO_NASCITA_BAMBINO = "giorno_dataNascitaBambino_";
	public static final String MESE_NASCITA_BAMBINO = "mese_dataNascitaBambino_";
	public static final String ANNO_NASCITA_BAMBINO = "anno_dataNascitaBambino_";
	public static final String SESSO_BAMBINO = "sessoBambino_";
	public static final String NUMERO_PASSAPORTO_BAMBINO = "numeroPassaportoBambino_";
	public static final String NAZIONALITA_BAMBINO = "nazionalitaBambino_";
	
	public static final String NOME_NEONATO = "nomeNeonato_";
	public static final String SECONDO_NOME_NEONATO = "secondoNomeNeonato_";
	public static final String COGNOME_NEONATO = "cognomeNeonato_";
	public static final String GIORNO_NASCITA_NEONATO = "giorno_dataNascitaNeonato_";
	public static final String MESE_NASCITA_NEONATO = "mese_dataNascitaNeonato_";
	public static final String ANNO_NASCITA_NEONATO = "anno_dataNascitaNeonato_";
	public static final String SESSO_NEONATO = "sessoNeonato_";
	public static final String NUMERO_PASSAPORTO_NEONATO = "numeroPassaportoNeonato_";
	public static final String NAZIONALITA_NEONATO = "nazionalitaNeonato_";
	public static final String ADULTO_RESPONSABILE_NEONATO = "adultoResponsabileNeonato_";
	
	public static final String EMAIL = "email";
	public static final String TIPO_RECAPITO = "tipoRecapito_";
	public static final String PREFISSO_RECAPITO = "prefissoRecapito_";
	public static final String VALORE_RECAPITO = "valoreRecapito_";
	public static final String CHECK_AGREEMENT = "checkAgreement";
	
	public static final String AR_TAX_INFO_TYPE = "ARTaxInfoType";
	public static final String NUMERO_CUIT_CUIL = "numeroCuitCuil";
	public static final String NAZIONALITA_CUIT_CUIL = "nazionalitaCuitCuil";
	
	public String nome;
	public String secondoNome;
	public String cognome;
	public String sesso;
	public String numeroPassaporto;
	public String nazionalita;
	
	public String giornoNascita;
	public String meseNascita;
	public String annoNascita;
	
	public String programmaFrequenFlyer;
	public String codiceCartaFrequenFlyer;
	
	public String codiceBluebiz;
	public String codiceSkyBonus;
	
	public String codicePin;
	
	public String adultoResponsabile;
}
