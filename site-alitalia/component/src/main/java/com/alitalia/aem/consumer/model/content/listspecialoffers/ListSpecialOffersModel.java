package com.alitalia.aem.consumer.model.content.listspecialoffers;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.CodeDescriptionData;
import com.alitalia.aem.common.data.home.OfferData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersResponse;
import com.alitalia.aem.common.utils.AlitaliaCommonUtils;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.OfferItem;
import com.alitalia.aem.web.component.geospatial.delegate.GeoSpatialDelegate;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.day.cq.wcm.api.Page;

@Model(adaptables={SlingHttpServletRequest.class})
public class ListSpecialOffersModel extends GenericBaseModel {

    private static final String DEFAULT_BUTTON_LABEL = "Prenota";

    private static final String DEFAULT_PRICE_LABEL = "Da";

    private static final String BUTTON_LABEL_ATTRIBUTE_NAME = "etichetta-pulsante";

    private static final String PRICE_LABEL_ATTRIBUTE_NAME = "etichetta-prezzo";

    private static final String SPECIAL_OFFERS_LIST_COMPONENT_NAME = "lista-offerte-speciali";

    private static final String OFFERS_LIST_COMPONENT_NAME = "lista-offerte";

    @Self
    private SlingHttpServletRequest request;

    @Inject
    private GeoSpatialDelegate geoSpatialDelegate;

    @Inject
    private SearchFlightsDelegate searchFlightsDelegate;

    @Inject
    private AlitaliaConfigurationHolder alitaliaConfigurationHolder;

    private ArrayList<OfferItem> offers = new ArrayList<OfferItem>();
    private ArrayList<OfferItem> offersItaliaOrYoung = new ArrayList<OfferItem>();

    private ArrayList<OfferItem> offersMondo = new ArrayList<OfferItem>();

    // Su indicazione di ENIS, in caso di mercato Ungheria va inserita
    // la classe "invert"
    private boolean invertPrice;

    private String priceLabel;
    private String buttonLabel;

    @PostConstruct
    protected void initModel() {
        logger.debug("[ListSpecialOffersModel] initModel");
        super.initBaseModel(request);
        //TODO: controllare
        if(logger.isDebugEnabled())
            logger.debug("Site market code: " + marketCode);
        // Preleva etichette redazionali dalle proprietà del componente
        // (lista-offerte o lista-offerte-speciali)
        priceLabel = AlitaliaUtils.getComponentProperty(
                AlitaliaUtils.getPage(request.getResource()),
                OFFERS_LIST_COMPONENT_NAME, PRICE_LABEL_ATTRIBUTE_NAME);
        if (priceLabel == null) {
            priceLabel = AlitaliaUtils.getComponentProperty(
                    AlitaliaUtils.getPage(request.getResource()),
                    SPECIAL_OFFERS_LIST_COMPONENT_NAME, PRICE_LABEL_ATTRIBUTE_NAME);
            if (priceLabel == null) {
                priceLabel = DEFAULT_PRICE_LABEL;
            }
        }

        buttonLabel = AlitaliaUtils.getComponentProperty(
                AlitaliaUtils.getPage(request.getResource()),
                OFFERS_LIST_COMPONENT_NAME, BUTTON_LABEL_ATTRIBUTE_NAME);
        if (buttonLabel == null) {
            buttonLabel = AlitaliaUtils.getComponentProperty(
                    AlitaliaUtils.getPage(request.getResource()),
                    SPECIAL_OFFERS_LIST_COMPONENT_NAME, BUTTON_LABEL_ATTRIBUTE_NAME);
            if (buttonLabel == null) {
                buttonLabel = DEFAULT_BUTTON_LABEL;
            }
        }

        // get input from request
        String paxType = request.getParameter("paxType");
        boolean isHomePage =
                Boolean.parseBoolean(request.getParameter("isHomePage")) || false;
        boolean isYoung =
                Boolean.parseBoolean(request.getParameter("isYoung")) || false;
        boolean tab1AsteriskInPrice =
                Boolean.parseBoolean(request.getParameter("tab1AsteriskInPrice"));
        boolean tab2AsteriskInPrice =
                Boolean.parseBoolean(request.getParameter("tab2AsteriskInPrice"));
        String departure = request.getParameter("from");

        if(logger.isDebugEnabled())
            logger.debug("[ListSpecialOffersModel] request: paxType: " + paxType
                    + "; isHomePage: " + isHomePage + "; isYoung: " + isYoung
                    + "; departure: " + departure + "; tab1AsteriskInPrice: "
                    + tab1AsteriskInPrice + "; tab2AsteriskInPrice: "
                    + tab2AsteriskInPrice);

        invertPrice = (getMarketCode().equals(AlitaliaConstants.COUNTRY_UNG)
                ? true : false);

        // populate list of airports
        ArrayList<String> airportList = new ArrayList<String>();
        populateAirportsList(airportList, paxType);

        // check id departure is a valid airport
        if (departure != null && airportList.contains(departure)) {

            // populate sorted lists of offers for Italia
            // and International destinations
            List<OfferData> offersListItaliaOrYoung = new ArrayList<OfferData>();
            List<OfferData> offersListMondo = new ArrayList<OfferData>();
            populateSortedDestinationOffersLists(offersListItaliaOrYoung,
                    offersListMondo, departure.toLowerCase(), paxType, isYoung);

            // populate offers list for Italia destinations, retrieve image
            // paths and create final lists of offers
            ArrayList<String> destinationsListItaliaOrYoung =
                    new ArrayList<String>();
            HashMap<String, OfferItem> offersMapItaliaOrYoung =
                    new HashMap<String, OfferItem>();

            String destinationsPath = AlitaliaUtils.findSiteBaseRepositoryPath(
                    request.getResource()) + AlitaliaConstants.DESTINATIONS_PATH;

            List<Page> destinations = findPageByTemplate(request,
                    destinationsPath, AlitaliaConstants.TEMPLATE_DESTINATION_PAGE);
            HashMap<String, String> destinationsMap = new HashMap<String, String>();
            for (Page page : destinations) {
                destinationsMap.put(page.getName(),
                        page.getPath() + "/jcr:content/image-lista-offerte");
            }

            populateOffersList(destinationsListItaliaOrYoung,
                    offersMapItaliaOrYoung, offersListItaliaOrYoung,
                    isHomePage, departure, isYoung, tab1AsteriskInPrice,
                    destinationsMap);

            offersItaliaOrYoung =
                    new ArrayList<OfferItem>(offersMapItaliaOrYoung.values());

            // if !isYoung: populate offers list for International destinations,
            // retrieve image paths and create final lists of offers
            if (!isYoung) {
                ArrayList<String> destinationsListMondo =
                        new ArrayList<String>();
                HashMap<String, OfferItem> offersMapMondo =
                        new HashMap<String, OfferItem>();
                populateOffersList(destinationsListMondo, offersMapMondo,
                        offersListMondo, isHomePage, departure,
                        isYoung, tab2AsteriskInPrice, destinationsMap);
                offersMondo = new ArrayList<OfferItem>(offersMapMondo.values());
            }

            // merge results for HomePage render purpose only
            if (isHomePage) {
                if (offersMondo.size() > 0) {
                    for (int i = 0; i < offersItaliaOrYoung.size(); i++) {
                        offers.add(offersItaliaOrYoung.get(i));
                        try {
                            offers.add(offersMondo.get(i));
                        } catch (IndexOutOfBoundsException e) {
                            continue;
                        }
                    }
                } else {
                    offers = offersItaliaOrYoung;
                }
            }

        } else {
            logger.debug("[ListSpecialOffersModel] Aereoporto selezionato non "
                    + "valido o assente dalla lista di quelli per i quali e' "
                    + "presente una offerta");

        }
    }

    /*
     * populate airports list
     */
    private void populateAirportsList (ArrayList<String> airportList,
                                       String paxType) {
        logger.debug("[ListSpecialOffersModel] populateAirportsList");

        //INIZIO - MODIFICA INDRA 18/07/2017 ci è stata fatta richiesta di ripristinare il servizio "retrieveGeoCities".
        //String target = "OFFERS"; //questa riga deve essere decommentata nel richiamare il metodo retrieveGeoCitiesByTarget
        // create geoCities request
        RetrieveGeoCitiesRequest requestCity = new RetrieveGeoCitiesRequest();
        requestCity.setSid(sid);
        requestCity.setTid(tid);
        requestCity.setLanguageCode(languageCode);
        requestCity.setMarketCode(marketCode);
        requestCity.setPaxType(paxType);
        //requestCity.setTarget(target); //questa riga deve essere decommentata nel richiamare il metodo retrieveGeoCitiesByTarget

        //RetrieveGeoCitiesResponse cityResponse = geoSpatialDelegate.retrieveGeoCitiesByTarget(requestCity);
        RetrieveGeoCitiesResponse cityResponse = geoSpatialDelegate.retrieveGeoCities(requestCity);
        //FINE.

        // set airportList
        List<CodeDescriptionData> selectCities = cityResponse.getCities();
        for (int i = 0; i < selectCities.size(); i++) {
            airportList.add(selectCities.get(i).getCode());
        }

        if(logger.isDebugEnabled())
            logger.debug("[ListSpecialOffersModel] airportList: " + airportList.toString());
    }

    /*
     * populate sorted destination offers lists
     */
    @SuppressWarnings("unchecked")
    private void populateSortedDestinationOffersLists(
            List<OfferData> offersListItaliaOrYoung,
            List<OfferData> offersListMondo, String departure, String paxType,
            boolean isYoung) {
        logger.debug("[ListSpecialOffersModel] populateSortedDestinationOffersLists");

        //INIZIO - MODIFICA INDRA 18/07/2017 ci è stata fatta richiesta di ripristinare il servizio "retrieveGeoOffers".
        //String target = "OFFERS"; //questa riga deve essere decommentata nel richiamare il metodo retrieveGeoOffersByTarget
        // create geoOffers request
        RetrieveGeoOffersRequest serviceRequest = new RetrieveGeoOffersRequest();
        serviceRequest.setSid(sid);
        serviceRequest.setTid(tid);
        serviceRequest.setLanguageCode(languageCode); //questa riga deve essere commentata nel richiamare il metodo retrieveGeoOffersByTarget
        serviceRequest.setMarketCode(marketCode);
        serviceRequest.setDepartureAirportCode(departure);
        serviceRequest.setPaxType(paxType);
        //serviceRequest.setTarget(target); //questa riga deve essere decommentata nel richiamare il metodo retrieveGeoOffersByTarget
        //serviceRequest.setMonthDailyPrices(false); //questa riga deve essere decommentata nel richiamare il metodo retrieveGeoOffersByTarget


        //RetrieveGeoOffersResponse serviceResponse =	geoSpatialDelegate.retrieveGeoOffersByTarget(serviceRequest);
        RetrieveGeoOffersResponse serviceResponse =	geoSpatialDelegate.retrieveGeoOffers(serviceRequest);
        //FINE.

        List<OfferData> geoOffersList = serviceResponse.getOffers();

        List<OfferData> offerDataList = null;
        try {
            offerDataList = (List<OfferData>) AlitaliaCommonUtils.deepClone(geoOffersList);
        } catch (IOException e) {
            logger.error("Error cloning object", e);
        }

        if (offerDataList != null) {
            logger.info("[ListSpecialOffersModel] number of offers: "
                    + "Departure [" + departure + "], [" + offerDataList.size() + "]");
        }


        // populate specific lists
        if (!isYoung) {
            for (OfferData offerData : offerDataList) {
                if (offerData.getArea() == AreaValueEnum.DOM) {
                    offersListItaliaOrYoung.add(offerData);
                } else {
                    offersListMondo.add(offerData);
                }
            }
        } else {
            offersListItaliaOrYoung.addAll(offerDataList);
        }

        // sort lists ASC by priority
        Collections.sort(offersListItaliaOrYoung);
        if (!isYoung) {
            Collections.sort(offersListMondo);
        }
    }

    /*
     * populate generic offers list
     */
    private void populateOffersList(ArrayList<String> destionationsList,
                                    HashMap<String, OfferItem> offersMap, List<OfferData> offersList,
                                    boolean isHomePage, String departure, boolean isYoung,
                                    boolean asteriskInPrice, Map<String, String> destinationsMap) {
        logger.debug("[ListSpecialOffersModel] populateOffersList");

        // cycle on offers
        int counterHp = 0, counter = 0;

        for (OfferData offerData : offersList) {

            if ((isHomePage && (counterHp < AlitaliaConstants.MAX_NUM_OFFERS_HP))
                    || !isHomePage || offerData.isHomePageOffer()) {
                if(logger.isDebugEnabled())
                    logger.debug("[ListSpecialOffersModel] offer2 #" + counter
                            + ": [" + offerData.toString() + "]");

                Calendar expireDate = offerData.getExpireOfferData();
                Calendar now = Calendar.getInstance();

                // expireDate check
                if (expireDate != null && expireDate.after(now)) {

                    OfferItem offerItem = new OfferItem();
                    String airportCode = offerData.getArrivalAirport().getCode();
                    String destinationCode = I18nKeyCommon.AIRPORT_PREFIX
                            + airportCode + I18nKeyCommon.AIRPORT_CITY;
                    //String destination = i18n.get(destinationCode);

                    // offerItem.setDestinazione(destination.equals(destinationCode)
                    // ? airportCode : destination);
                    offerItem.setDestinazione(destinationCode);
                    // String price = offerData.getBestPrice().toString();
                    double price = offerData.getBestPrice().doubleValue();
                    //per alcuni mercati è necessario aggiungere una extraFee al prezzo
                    price += AlitaliaUtils.getExtraFee(alitaliaConfigurationHolder, marketCode, searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(departure, airportCode), searchFlightsDelegate.isSliceSicilyTerritorialContinuity(departure, airportCode));

                    String prezzo = AlitaliaUtils.round(price, request.getResource());
                    if (asteriskInPrice) {
                        offerItem.setPrezzoDaMostrare(prezzo + "*");
                    }
                    else{
                        offerItem.setPrezzoDaMostrare(prezzo);
                    }
                    offerItem.setPrezzo(NumberFormat.getInstance(Locale.ITALIAN).format(price));
                    // if (price != null) {
                    // int index = price.lastIndexOf(".");
                    // offerItem.setPrezzo(price.substring(0, index));
                    // } else {
                    // offerItem.setPrezzo(price);
                    // }

                    // FIXME ?? [listSpecialOffersModel] translate label
                    // ITALIA/MONDO
                    String area = AreaValueEnum.DOM.equals(
                            offerData.getArrivalAirport().getArea())
                            ? AlitaliaConstants.OFFER_AREA_ITALIA
                            : AlitaliaConstants.OFFER_AREA_MONDO;
                    offerItem.setArea(area);

                    String link = AlitaliaUtils.findSiteBaseExternalUrl(
                            request.getResource(), true)
                            + alitaliaConfigurationHolder.getOfferDetailPageWithExtension();
                    String linkParam = "?from=" + departure;
                    linkParam += "&to=" + airportCode;
                    if (isYoung) {
                        linkParam += "&isYoung=" + isYoung;
                    }
                    offerItem.setLink(link + linkParam);
                    logger.debug("Offer link: " + link + linkParam);

                    if (RouteTypeEnum.RETURN.equals(offerData.getRouteType())) {
                        offerItem.setTipo(I18nKeyCommon.LABEL_AR);
                    } else {
                        offerItem.setTipo(I18nKeyCommon.LABEL_SOLO_ANDATA);
                    }


                    if (destinationsMap.containsKey(airportCode)) {
                        offerItem.setPathImmagine(destinationsMap.get(airportCode));
                    } else {
                        offerItem.setPathImmagine(destinationsMap.get("default"));
                    }

                    if("it".equalsIgnoreCase(marketCode)){
                        if ((AlitaliaConstants.OFFER_AREA_MONDO.equals(offerItem.getArea())
                                && RouteTypeEnum.RETURN.equals(offerData.getRouteType()))
                                || (AlitaliaConstants.OFFER_AREA_ITALIA.equals(offerItem.getArea())
                                && !RouteTypeEnum.RETURN.equals(offerData.getRouteType()))) {
                            offersMap.put(offerItem.getDestinazione(), offerItem);
                            destionationsList.add(offerItem.getDestinazione());
                            if (isHomePage) {
                                counterHp++;
                            } else {
                                counter++;
                            }
                        }
                    } else{
                        offersMap.put(offerItem.getDestinazione(), offerItem);
                        destionationsList.add(offerItem.getDestinazione());
                        if (isHomePage) {
                            counterHp++;
                        } else {
                            counter++;
                        }
                    }
                } else {
                    logger.debug("[ListSpecialOffersModel] expired offer");
                }
            }
        }
        logger.debug("[ListSpecialOffersModel] selected offers: "
                + offersMap.size() + ": [" + offersMap.toString() + "]");
    }

    public ArrayList<OfferItem> getOffers() {
        return offers;
    }

    public ArrayList<OfferItem> getOffersItaliaOrYoung() {
        return offersItaliaOrYoung;
    }

    public ArrayList<OfferItem> getOffersMondo() {
        return offersMondo;
    }

    public boolean isInvertPrice() {
        return invertPrice;
    }

    public void setInvertPrice(boolean invertPrice) {
        this.invertPrice = invertPrice;
    }

    public String getPriceLabel() {
        return priceLabel;
    }

    public String getButtonLabel() {
        return buttonLabel;
    }
}
