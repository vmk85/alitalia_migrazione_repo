package com.alitalia.aem.consumer.checkinrest.servlet;

import com.alitalia.aem.common.data.checkinrest.model.offload.request.FlightPas;
import com.alitalia.aem.common.data.checkinrest.model.offload.request.PnrFlightInfo;
import com.alitalia.aem.common.data.checkinrest.model.offload.request.SeatList;
import com.alitalia.aem.common.data.checkinrest.model.offload.response.FlightPassR;
import com.alitalia.aem.common.data.checkinrest.model.offload.response.PnrFlightInfoR;
import com.alitalia.aem.common.data.checkinrest.model.offload.response.SeatListR;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Flight;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadFlightsRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadFlightsResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkinoffloadflightsrest"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinOffloadFlightsServlet extends GenericCheckinFormValidatorServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ICheckinDelegate checkInDelegateRest;

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		
		try {
			logger.info("Entrati in [CheckinOffloadFlightsServlet - performSubmit]");

			CheckinOffloadFlightsRequest CheckinOffloadFlightsRequest = new CheckinOffloadFlightsRequest();
			CheckinOffloadFlightsRequest.setTid(IDFactory.getTid());
			CheckinOffloadFlightsRequest.setSid(IDFactory.getSid(request));

			SeatList seatList = new SeatList();
			seatList.setFirstName(ctx.passengerName);
			seatList.setLastName(ctx.passengerSurname);
			List<SeatList> seatListL = new ArrayList<>();
			seatListL.add(seatList);

			List<FlightPas> flightPasList = new ArrayList<>();
			for(FlightsRender flightsRender : ctx.selectedFlights) {

				for (SegmentRender segmentRender : flightsRender.getSegments()) {

					FlightPas flightPas = new FlightPas();
					flightPas.setAirline(segmentRender.getAirline());
					flightPas.setDepartureDate(segmentRender.getDepartureDate());
					flightPas.setFlight(segmentRender.getFlight());
					flightPas.setOrigin(segmentRender.getOrigin().getCode());
					flightPas.setSeatList(seatListL);
					flightPasList.add(flightPas);
				}
			}

			PnrFlightInfo pnrFlightInfo = new PnrFlightInfo();
			pnrFlightInfo.setPnr(ctx.pnr);
			pnrFlightInfo.setFlightPass(flightPasList);
			List<PnrFlightInfo> pnrFlightInfoList = new ArrayList<>();
			pnrFlightInfoList.add(pnrFlightInfo);

			CheckinOffloadFlightsRequest.setPnrFlightInfo(pnrFlightInfoList);
			CheckinOffloadFlightsRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
			CheckinOffloadFlightsRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
			CheckinOffloadFlightsRequest.setConversationID(IDFactory.getTid());

			logger.info("[CheckinOffloadFlightsServlet] invocando checkInDelegateRest.offload...");
			CheckinOffloadFlightsResponse CheckinOffloadFlightsResponse = checkInDelegateRest.offloadFlights(CheckinOffloadFlightsRequest);

			if (CheckinOffloadFlightsResponse != null) {
				if(CheckinOffloadFlightsResponse.getError() != null && !CheckinOffloadFlightsResponse.getError().equals("")){
					managementError(request, response, CheckinOffloadFlightsResponse.getError());
				}else{
					//response.sendRedirect(successPage);
					managementSucces(request, response, CheckinOffloadFlightsResponse);
				}
			}
			else {
				logger.error("[CheckinOffloadFlightsServlet] - Errore durante l'invocazione del servizio." );
			}
		}
		catch (Exception e) {
			throw new RuntimeException("[CheckinOffloadFlightsServlet] - Errore durante l'invocazione del servizio.", e);
		}

	}
	
	private void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error){

		logger.info("[CheckinOffloadFlightsServlet] [managementError] error =" + error );

		Gson gson = new Gson();

		com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);

		TidyJSONWriter json;
		try {
			json = new TidyJSONWriter(response.getWriter());

			response.setContentType("application/json");

			json.object();
			json.key("isError").value(true);
			json.key("errorMessage").value(errorObj.getErrorMessage());
			json.key("sabreStatusCode").value(errorObj.getSabreStatusCode());
			json.key("conversationID").value(errorObj.getConversationID());
			json.endObject();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
	}
	
	private void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response, CheckinOffloadFlightsResponse CheckinOffloadFlightsResponse){

		logger.info("[CheckinOffloadFlightsServlet] [managementSucces] ...");
		
		try {
			TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());
	
			jsonOutput.object();
			for(PnrFlightInfoR pnrFlightInfoR : CheckinOffloadFlightsResponse.getPnrFlightInfoRS()) {

				jsonOutput.key("pnr").value(pnrFlightInfoR.getPnr());

				String fp = "";

				for (FlightPassR flightPassR : pnrFlightInfoR.getFlightPassRS()) {

					fp = fp + "airline:" + flightPassR.getAirline() + ",";
					fp = fp + "departureDate:" + flightPassR.getDepartureDate() + ",";
					fp = fp + "flight:" + flightPassR.getFlight() + ",";
					fp = fp + "origin:" + flightPassR.getOrigin() + ",";
					String sl = "seatListRS:[";

					for (SeatListR seatListR : flightPassR.getSeatListRS()) {

						sl = sl + "esito:" + seatListR.getEsito() + ",";
						sl = sl + "firstName:" + seatListR.getFirstName() + ",";
						sl = sl + "lastName:" + seatListR.getLastName() + "]";
					}
					fp = fp + sl;
					jsonOutput.key("flightPassRS").value(fp);
					fp = "";
				}
			}

			jsonOutput.key("conversationID").value(CheckinOffloadFlightsResponse.getConversationID());
			jsonOutput.endObject();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
		
//		String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
//				request.getResource(), false)
//				+ getConfiguration().getCheckinFlightListPage();
//
//		successPage = request.getResourceResolver().map(successPage);
//
//		logger.info("[SetInsuranceServlet] [managementSucces] successPage = " + successPage );
//
//		TidyJSONWriter json;
//		try {
//
//			json = new TidyJSONWriter(response.getWriter());
//			response.setContentType("application/json");
//			json.object();
//			json.key("isError").value(false);
//			json.key("successPage").value(successPage);
//			json.endObject();
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}	

	}


}
