package com.alitalia.aem.consumer.model.content.menu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.LabelUrl;
import com.day.cq.wcm.api.Page;

@Model(adaptables={Resource.class})
public class DynamicMenuLoggedUserModel {

	@SuppressWarnings("unused")
    private Logger logger = LoggerFactory.getLogger(DynamicMenuLoggedUserModel.class);

	@Inject
	private AlitaliaConfigurationHolder configuration;
	
    @Self
    private Resource resource;
    
    private LabelUrl[] menuItems;
    private LabelUrl[] menuItemsMobile;

	private LabelUrl[] menuItemsWaveSectionMmPoints;
	private LabelUrl[] menuItemsWaveSectionDefault;
    
    /**
     * This method is used to get all the first level children of selected page.
     */
    @PostConstruct
    protected void initModel() {
        
    	ResourceResolver resolver = resource.getResourceResolver();
    	List<LabelUrl> menuItemList = new ArrayList<LabelUrl>();
    	List<LabelUrl> menuItemMobileList = new ArrayList<LabelUrl>();

    	//USED IN HOMEPAGE WAVE 1
		List<LabelUrl> menuItemsWaveSectionMmPointsList = new ArrayList<LabelUrl>();
		List<LabelUrl> menuItemsWaveSectionDefaultList = new ArrayList<LabelUrl>();
    	
    	String personalAreaResourcePath = AlitaliaUtils.findSiteBaseRepositoryPath(resource) + "/" 
    			+ AlitaliaConstants.PERSONAL_AREA_PAGE_NAME;
    	Resource personalAreaResource = resource.getResourceResolver().resolve(personalAreaResourcePath);
    	
    	Page currentPage = AlitaliaUtils.getPage(resource);
		ValueMap currentPageProperties = currentPage.getProperties();

		if (personalAreaResource != null) {
    		Page pathPage = personalAreaResource.adaptTo(Page.class);
        	Iterator<Page> children = pathPage.listChildren();
        	int i = 0;
        	while (children.hasNext()) {
        		Page menuItemPage = children.next();
				ValueMap menuItemPageProperties = menuItemPage.getProperties();
        		boolean isActive = false;

				String hideInNav = menuItemPageProperties.get("hideInNav", String.class);
				if (hideInNav == null ||
						!"true".equals(currentPageProperties.get("hideInNav", String.class))) {

					if (menuItemPage.getPath().equals(currentPage.getPath())) {
						isActive = true;
					}

					//USED IN HOMEPAGE WAVE 1
					String menuSectionMMPoints = menuItemPageProperties.get("menuSectionMMPoints", String.class);
					String menuSectionMyProfile = "";
					if("true".equals(menuSectionMMPoints)) {
						menuSectionMMPoints = "true";
					}
					if("menuSectionMyProfile".equals(menuSectionMMPoints)) {
						menuSectionMyProfile = "menuSectionMyProfile";
						menuSectionMMPoints = "true";
					}

					//additional css
					String itemAdditionalInfo = ("menuSectionMyProfile".equals(menuSectionMyProfile) ? "hide-for-large " + menuSectionMyProfile : "");

					boolean httpsEnabled = configuration.getHttpsEnabled();
					//logger.debug("[MenuLoggedUserModel] httpsEnabled: " + httpsEnabled);
					String protocol = "https://";
					if(!httpsEnabled){
						protocol = "http://";
					}
					//logger.debug("[MenuLoggedUserModel] protocol: " + protocol);

					String urlMap = resolver.map(menuItemPage.getPath());
					String url = protocol + configuration.getExternalDomain() + urlMap;//menuItemPage.getPath();//urlMap;//menuItemPage.getPath()
					//logger.debug("[MenuLoggedUserModel] url: " + url);
					menuItemList.add(new LabelUrl(menuItemPage.getTitle(), url + AlitaliaConstants.HTML_EXT , isActive));
					menuItemMobileList.add(new LabelUrl(menuItemPage.getTitle(),  url + AlitaliaConstants.HTML_EXT, isActive));//resolver.map(menuItemPage.getPath())

					//USED IN HOMEPAGE WAVE 1
					LabelUrl menuHomepageWave = new LabelUrl(menuItemPage.getTitle(), url + AlitaliaConstants.HTML_EXT , itemAdditionalInfo, isActive);
					if("true".equals(menuSectionMMPoints)) {
						if("menuSectionMyProfile".equals(menuSectionMyProfile)) {
							menuItemsWaveSectionMmPointsList.add(0, menuHomepageWave);
						} else {
							menuItemsWaveSectionMmPointsList.add(menuHomepageWave);
						}
					} else {
						menuItemsWaveSectionDefaultList.add(menuHomepageWave);
					}
				}

				//
        		i++;
        	}
        	menuItems = (LabelUrl[]) menuItemList.toArray(new LabelUrl[menuItemList.size()]);
        	menuItemsMobile = (LabelUrl[]) menuItemMobileList.toArray(new LabelUrl[menuItemMobileList.size()]);

        	//USED IN HOMEPAGE WAVE 1
			menuItemsWaveSectionMmPoints = (LabelUrl[]) menuItemsWaveSectionMmPointsList.toArray(new LabelUrl[menuItemsWaveSectionMmPointsList.size()]);
			menuItemsWaveSectionDefault = (LabelUrl[]) menuItemsWaveSectionDefaultList.toArray(new LabelUrl[menuItemsWaveSectionDefaultList.size()]);
    	}
    	
    }

    /**
     * Returns all the first level children of selected page
     * @return The menuItems
     */
    public LabelUrl[] getMenuItems() {
    	return menuItems;
    }
    
    /**
     * Returns all the first level children of selected page
     * @return The menuItems
     */
    public LabelUrl[] getMenuItemsMobile() {
    	return menuItemsMobile;
    }


	/**
	 * USED IN HOMEPAGE WAVE 1: Return menu items for MilleMiglia Points Section Menu (Logged Users)
	 * @return LabelUrl[]
	 */
	public LabelUrl[] getMenuItemsWaveSectionMmPoints() {
		return menuItemsWaveSectionMmPoints;
	}

	/**
	 * USED IN HOMEPAGE WAVE 1: Return menu items NOT in MilleMiglia Points Section Menu (Logged Users)
	 * @return
	 */
	public LabelUrl[] getMenuItemsWaveSectionDefault() {
		return menuItemsWaveSectionDefault;
	}
}
