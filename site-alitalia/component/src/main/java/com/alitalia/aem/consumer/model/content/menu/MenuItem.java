package com.alitalia.aem.consumer.model.content.menu;

import java.util.Arrays;

public class MenuItem {

	private String itemTitle;
	private String itemUrl;
	private String itemDescription;
	private MenuItemType type;
	private MenuItem[] itemChildren;
	private String itemUrlImage;
	private int menuClassNumber;
	private String itemResolvedUrl;

	public MenuItem(String itemTitle, String itemUrl, String itemDescription, String itemResolvedUrl) {
		setItemTitle(itemTitle);
		setItemUrl(itemUrl);
		setItemDescription(itemDescription);
		setItemResolvedUrl(itemResolvedUrl);
		this.type = MenuItemType.SECOND_LEVEL;
	}

	public MenuItem(String itemTitle, String itemUrl, String itemDescription,
			MenuItemType type, String itemResolvedUrl) {
		setItemTitle(itemTitle);
		setItemUrl(itemUrl);
		setItemDescription(itemDescription);
		setItemResolvedUrl(itemResolvedUrl);
		this.type = type;
	}

	public MenuItem(String itemTitle, String itemUrl, String itemDescription,
			String itemUrlImage, MenuItemType type, String itemResolvedUrl) {
		setItemTitle(itemTitle);
		setItemUrl(itemUrl);
		setItemDescription(itemDescription);
		this.type = type;
		setItemUrlImage(itemUrlImage);
		setItemResolvedUrl(itemResolvedUrl);
	}

	public String getItemTitle() {
		return itemTitle;
	}

	public String getItemUrl() {
		return itemUrl;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public MenuItem[] getItemChildren() {
		return itemChildren;
	}

	public String getItemUrlImage() {
		return itemUrlImage;
	}

	public void setItemTitle(String itemTitle) {
		this.itemTitle = itemTitle;
	}

	public void setItemUrl(String itemUrl) {
		this.itemUrl = itemUrl;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public void setItemChildren(MenuItem[] itemChildren) {
		this.itemChildren = itemChildren;
	}

	public void setItemUrlImage(String itemUrlImage) {
		this.itemUrlImage = itemUrlImage;
	}

	public int getMenuClassNumber() {
		return menuClassNumber;
	}

	public void setMenuClassNumber(int menuClassNumber) {
		this.menuClassNumber = menuClassNumber;
	}

	public String getType() {
		return type.value();
	}

	public void setType(String type) {
		for (MenuItemType item : MenuItemType.values()) {
			if (type == item.value()) {
				this.type = item;
			}
		}
	}

	@Override
	public String toString() {
		return "MenuItem [itemTitle=" + itemTitle + ", itemUrl=" + itemUrl
			+ ", itemDescription=" + itemDescription + ", type=" + type
			+ ", itemChildren=" + Arrays.toString(itemChildren)
			+ ", itemUrlImage=" + itemUrlImage + "]";
	}

	public String getItemResolvedUrl() {
		return itemResolvedUrl;
	}

	public void setItemResolvedUrl(String itemResolvedUrl) {
		this.itemResolvedUrl = itemResolvedUrl;
	}

}