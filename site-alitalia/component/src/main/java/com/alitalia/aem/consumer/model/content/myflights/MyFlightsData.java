package com.alitalia.aem.consumer.model.content.myflights;

import java.util.List;

public class MyFlightsData {
	public static String NAME = "MyFlightsData";
	
	private String name;
	private String lastname;
	private List<MyRouteRender> myRoutes;
	private int error = 0;

	public List<MyRouteRender> getMyRoutes() {
		return myRoutes;
	}

	public void setMyRoutes(List<MyRouteRender> myRoutes) {
		this.myRoutes = myRoutes;
	}

	public int getError() {
		return error;
	}

	public void setError(int error) {
		this.error = error;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Override
	public String toString() {
		return "MyFlightsData [name=" + name + ", lastname=" + lastname
				+ ", myRoutes=" + myRoutes + ", error=" + error + "]";
	}
	
	
	
}
