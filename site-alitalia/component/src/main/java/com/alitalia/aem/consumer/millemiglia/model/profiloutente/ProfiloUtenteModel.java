package com.alitalia.aem.consumer.millemiglia.model.profiloutente;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import com.alitalia.aem.common.data.home.*;
import com.alitalia.aem.common.messages.checkinrest.CheckinFrequentFlyerSearchResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.exception.CheckinPnrInformationNotFoundException;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaConstants;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSessionContext;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingBindings;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.i18n.ResourceBundleProvider;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.enumerations.MMAddressTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMContractStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MMGenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMPhoneTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMWorkPositionTypeEnum;
import com.alitalia.aem.common.messages.home.CreditCardsListRequest;
import com.alitalia.aem.common.messages.home.CreditCardsListResponse;
import com.alitalia.aem.common.messages.home.ProfileRequest;
import com.alitalia.aem.common.messages.home.ProfileResponse;
import com.alitalia.aem.common.messages.home.SocialAccountsRequest;
import com.alitalia.aem.common.messages.home.SocialAccountsResponse;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloResponseLocal;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloRequestLocal;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.global.enumeration.CompagnieAeree;
import com.alitalia.aem.consumer.millemiglia.render.DateRender;
import com.alitalia.aem.consumer.millemiglia.render.ProfiloUtenteRender;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;

@Model(adaptables = { SlingHttpServletRequest.class })
public class ProfiloUtenteModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest request;

	@Inject
	private ConsumerLoginDelegate consumerLoginDelegate;

	@Inject
	private AlitaliaCustomerProfileManager customerProfileManager;

    @Inject
    private AlitaliaConfigurationHolder configuration;

    @Inject
    private CheckinSession checkinSession;

	private ProfiloUtenteRender profiloUtenteRender;
	private GetDatiSicurezzaProfiloResponseLocal datiSicurezzaProfiloRender;

	private Customer customer = new Customer();

	public CustomerMa getCustomerMa() {
		return customerMa;
	}

	public void setCustomerMa(CustomerMa customerMa) {
		this.customerMa = customerMa;
	}

	private CustomerMa customerMa = new CustomerMa();

	private Boolean resultDatiPersonaliSubmit;
	private Boolean resultDatiLoginSubmit;
	private Boolean resultSocialLoginSubmit;
	private Boolean resultOfferteComunicazioniSubmit;
	private Boolean resultPreferenzeViaggioSubmit;
	private Boolean resultCarteCreditoSubmit;
	private SocialAccountData[] socialAccounts;
	private Map<String, Boolean> frequentFlyerCompaniesSelected;
	private boolean isYoung = false;

	//Tolentino - Inizio
	private boolean loggedMAUser;
	private boolean maUserHasCreditCards;
	private String  actualIsoTime;
	//Tolentino - Fine

	@PostConstruct
	protected void initModel() {
		try {

			initBaseModel(request);

			HttpSession session = request.getSession();

			Date date = new Date(System.currentTimeMillis());

			// Conversion
			SimpleDateFormat sdf;
			sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
			sdf.setTimeZone(TimeZone.getTimeZone("TZD"));
			this.actualIsoTime = sdf.format(date);

			// recuperiamo da sessione oggetto CUSTOMER per evitare dei lenti caricamenti dati.
            // bisogna capire come gestire correttamente l'annullamento di questo oggetto

			// get I18N
			SlingBindings bindings = (SlingBindings) request.getAttribute(SlingBindings.class.getName());
			SlingScriptHelper scriptHelper = bindings.getSling();
			ResourceBundleProvider bundleProvider = scriptHelper.getServices(ResourceBundleProvider.class, "(component.name=org.apache.sling.i18n.impl.JcrResourceBundleProvider)")[0];

			// create resourceBundle, TID and SID
			Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
			ResourceBundle resourceBundle = bundleProvider.getResourceBundle(locale);
			I18n i18n = new I18n(resourceBundle);

			customer.seti18n(i18n);

			// recuperiamo eventuale utente MyAlitalia
			MyAlitaliaSessionContext mactx = (MyAlitaliaSessionContext) session.getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);
			if (mactx != null) {
				Object obj = mactx.getMaCustomer();
				if (obj instanceof MACustomer) {
					MACustomer maCustomer = (MACustomer) obj;
					if (maCustomer.getUID() != null && maCustomer.getProfile() != null) {
						customer.setMaCustomer(maCustomer);
						customerMa.setMaCustomer(maCustomer);
						frequentFlyerCompaniesSelected = new HashMap<String, Boolean>();
						if (customerMa.getMaCustomer().getData().getFlightPref_FF() != null){
							for (String s : customerMa.getMaCustomer().getData().getFlightPref_FF()) {
                                frequentFlyerCompaniesSelected.put(s, true);

							}
						}
                        customerMa.setFrequentFlyerCompaniesSelected(frequentFlyerCompaniesSelected);

						//Tolentino - Inizio
						maUserHasCreditCards = false;
						loggedMAUser = true;

						if (maCustomer.getData().getPayment()!=null && maCustomer.getData().getPayment().getCreditCardsData()!=null &&
							maCustomer.getData().getPayment().getCreditCardsData().isEmpty()==false) {
							maUserHasCreditCards = true;
						}
						//Tolentino - Fine
					}
				}
			}



			MMCustomerProfileData customerProfileData = AlitaliaUtils.getAuthenticatedUser(request);
			if (customerProfileData == null) {
				logger.info("[ProfiloUtenteModel] customerProfileData for AEM Autheticated user is null");
				return;
			}

            GetDatiSicurezzaProfiloResponseLocal  getDatiSicurezzaProfiloResponseLocal = (GetDatiSicurezzaProfiloResponseLocal) session.getAttribute("MilleMigliaProfileDataSicurezza");

			if (getDatiSicurezzaProfiloResponseLocal == null ){

                GetDatiSicurezzaProfiloRequestLocal datiSicurezzaProfiloRequest = new GetDatiSicurezzaProfiloRequestLocal();

                datiSicurezzaProfiloRequest.setIdProfilo(customerProfileData.getCustomerNumber());

                getDatiSicurezzaProfiloResponseLocal = consumerLoginDelegate.GetDatiSicurezzaProfilo(datiSicurezzaProfiloRequest);

                session.setAttribute("MilleMigliaProfileDataSicurezza",getDatiSicurezzaProfiloResponseLocal);

            }

            datiSicurezzaProfiloRender = getDatiSicurezzaProfiloResponseLocal;

			customerProfileData.setCustomerPinCode(customerProfileManager.decodeAndDecryptProperty(customerProfileData.getCustomerPinCode()));

	    	logger.info("[ProfiloUtenteModel] Get customerProfileData for AEM Autheticated user: {}", customerProfileData.getCustomerNumber());
	    	ProfileResponse profileResponse = (ProfileResponse) session.getAttribute("MilleMigliaProfileData");

	    	//Imposto i valori di nome, cognome e nickname contenuti nella clientcontext
	    	profileResponse.getCustomerProfile().setCustomerName(customerProfileData.getCustomerName());
	    	profileResponse.getCustomerProfile().setCustomerSurname(customerProfileData.getCustomerSurname());
	    	profileResponse.getCustomerProfile().setCustomerNickName(customerProfileData.getCustomerNickName());
	    	profileResponse.getCustomerProfile().setBirthDate(customerProfileData.getBirthDate());
	    	profileResponse.getCustomerProfile().setGender(customerProfileData.getGender());

			profiloUtenteRender = new ProfiloUtenteRender(profileResponse.getCustomerProfile());

			try{
				/* Recupero i social assocciati all'utente loggato */
				SocialAccountsRequest socialAccountsRequest = new SocialAccountsRequest();
				socialAccountsRequest.setSid(IDFactory.getSid(request));
				socialAccountsRequest.setTid(IDFactory.getTid());
				socialAccountsRequest.setUserCode(customerProfileData.getCustomerNumber());
				socialAccountsRequest.setPin(customerProfileData.getCustomerPinCode());

				SocialAccountsResponse socialAccountsResponse = (SocialAccountsResponse) session.getAttribute("MilleMigliaSocialAccount");
						if (socialAccountsResponse == null){
                    socialAccountsResponse =consumerLoginDelegate.getSocialAccounts(socialAccountsRequest);
				session.setAttribute("MilleMigliaSocialAccount",socialAccountsResponse);logger.debug("SocialAccountsRequest: " + socialAccountsRequest.toString());
				if(socialAccountsResponse != null){
					logger.debug("SocialAccountsResponse: " + socialAccountsResponse.toString());
				}}

				socialAccounts = (SocialAccountData[]) socialAccountsResponse.getSocialAccounts().toArray(
						new SocialAccountData[socialAccountsResponse.getSocialAccounts().size()]);
			}catch(Exception e){
				logger.error("Exception message ProfileUtenteModel - Social account: "+e);
			}
			/* Recupero carta di credito */
			CreditCardsListRequest creditCardsListRequest = new CreditCardsListRequest();
			creditCardsListRequest.setSid(IDFactory.getSid(request));
			creditCardsListRequest.setTid(IDFactory.getTid());
			creditCardsListRequest.setCustomerProfile(customerProfileData);

            CreditCardsListResponse creditCardsListResponse = (CreditCardsListResponse) session.getAttribute("MilleMigliaCreditCardData");

            if (creditCardsListResponse == null){
                creditCardsListResponse =
                        consumerLoginDelegate.getCreditCardList(creditCardsListRequest);
                session.setAttribute("MilleMigliaCreditCardData",creditCardsListResponse);
            }


			profiloUtenteRender.getCustomerData().setCreditCards(
					creditCardsListResponse.getCustomerProfile().getCreditCards());

			// is Young
			MMCustomerProfileData customerData = profiloUtenteRender.getCustomerData();
			DateRender dateRender = new DateRender(customerData.getBirthDate());
			if (isYoungRange(dateRender.getYear())) {
				isYoung = true;
			}

			/* Gestione del risultato della submit */

			//DATI PERSONALI
			if (session.getAttribute("datiPersonaliData") != null &&
					request.getParameter("success") != null &&
					request.getParameter("success").equals("false")) {
				//performSubmit Error
				resultDatiPersonaliSubmit = false;
				DatiPersonaliData datiPersonaliData =
						(DatiPersonaliData) session.getAttribute("datiPersonaliData");
				setDatiPersonaliFromSession(datiPersonaliData);
				session.removeAttribute("datiPersonaliData");
			} else if (request.getParameter("from") != null &&
					request.getParameter("from").equals("datipersonalisubmit") &&
					request.getParameter("success") != null &&
					request.getParameter("success").equals("true")) {
				//performSubmit Success
				resultDatiPersonaliSubmit = true;
			}

			//DATI LOGIN
			if (session.getAttribute("datiLoginData") != null &&
					request.getParameter("success") != null &&
					request.getParameter("success").equals("false")) {
				//performSubmit Error
				resultDatiLoginSubmit = false;
				DatiLoginData datiLoginData = (DatiLoginData) session.getAttribute("datiLoginData");
				profiloUtenteRender.getCustomerData().setCustomerNickName(datiLoginData.getNickname());
				profiloUtenteRender.getCustomerData().setCustomerPinCode(datiLoginData.getPin());
				session.removeAttribute("datiLoginData");
			} else if (request.getParameter("from") != null &&
					request.getParameter("from").equals("datiloginsubmit") &&
					request.getParameter("success") != null &&
					request.getParameter("success").equals("true")) {
				//performSubmit Success
				resultDatiLoginSubmit = true;
			}

			//SOCIAL LOGIN
			if (request.getParameter("from") != null &&
					request.getParameter("from").equals("socialloginsubmit")) {
				if (request.getParameter("success") != null &&
						request.getParameter("success").equals("false")) {
					//Error
					resultSocialLoginSubmit = false;
				} else {
					//Success
					resultSocialLoginSubmit = true;
				}
			}

			//COMUNICAZIONI E OFFERTE
			if (session.getAttribute("comunicazioniOfferteData") != null &&
					request.getParameter("success") != null &&
					request.getParameter("success").equals("false")) {
				//performSubmit Error
				resultOfferteComunicazioniSubmit = false;
				ComunicazioniOfferteData comunicazioniOfferteData =
						(ComunicazioniOfferteData) session.getAttribute("comunicazioniOfferteData");
				profiloUtenteRender.getCustomerData().setMailingType(comunicazioniOfferteData.getMailingType());
				profiloUtenteRender.getCustomerData().setLanguage(comunicazioniOfferteData.getLanguage());
				profiloUtenteRender.getCustomerData().setSmsAuthorization(comunicazioniOfferteData.getSmsAuth());
				profiloUtenteRender.getCustomerData().setProfiling(comunicazioniOfferteData.getProfiling());
				session.removeAttribute("comunicazioniOfferteData");
			} else if (request.getParameter("from") != null &&
					request.getParameter("from").equals("comunicazionioffertesubmit") &&
					request.getParameter("success") != null &&
					request.getParameter("success").equals("true")) {
				//performSubmit Success
				resultOfferteComunicazioniSubmit = true;
			}

			//PREFERENZE VIAGGIO
			if (session.getAttribute("preferenzeViaggioData") != null &&
					request.getParameter("success") != null &&
					request.getParameter("success").equals("false")) {
				//performSubmit Error
				resultPreferenzeViaggioSubmit = false;
				PreferenzeViaggioData preferenzeViaggioData =
						(PreferenzeViaggioData) session.getAttribute("preferenzeViaggioData");
				profiloUtenteRender.getCustomerData().setLifestyles(
						preferenzeViaggioData.getFrequentFlyerCompaniesSelected());

				if (profiloUtenteRender.getCustomerData().getPreferences() != null) {
					for (MMPreferenceData preferenceData : profiloUtenteRender.getCustomerData().getPreferences()) {
						if (preferenceData.getIdCategory().equals(ProfiloUtenteRender.MEAL_CATEGORY)) {
							preferenceData.setShortNamePreference(preferenzeViaggioData.getPreferenzePasto());
						}
						if (preferenceData.getIdCategory().equals(ProfiloUtenteRender.SEAT_CATEGORY)) {
							preferenceData.setShortNamePreference(preferenzeViaggioData.getPreferenzePosto());
						}
					}
				} else {
					List<MMPreferenceData> preferenceDataList = new ArrayList<MMPreferenceData>();
					MMPreferenceData preferenceData = new MMPreferenceData();
					preferenceData.setIdCategory(ProfiloUtenteRender.MEAL_CATEGORY);
					preferenceData.setShortNamePreference(preferenzeViaggioData.getPreferenzePasto());
					preferenceDataList.add(preferenceData);

					preferenceData = new MMPreferenceData();
					preferenceData.setIdCategory(ProfiloUtenteRender.SEAT_CATEGORY);
					preferenceData.setShortNamePreference(preferenzeViaggioData.getPreferenzePosto());
					preferenceDataList.add(preferenceData);

					profiloUtenteRender.getCustomerData().setPreferences(preferenceDataList);
				}

				session.removeAttribute("preferenzeViaggioData");

			} else if (request.getParameter("from") != null &&
					request.getParameter("from").equals("preferenzeviaggiosubmit") &&
					request.getParameter("success") != null &&
					request.getParameter("success").equals("true")) {
				//performSubmit Success
				resultPreferenzeViaggioSubmit = true;
			}

			// CARTE CREDITO
			if (session.getAttribute("cartaCreditoData") != null &&
					request.getParameter("success") != null &&
					request.getParameter("success").equals("false")) {
				// last operation was a failed submit attempt for credit card form
				resultCarteCreditoSubmit = false;
				session.removeAttribute("cartaCreditoData");
			} else if ("cartacreditosubmit".equals(request.getParameter("from"))) {
				// last operation was a successful submit attempt for credit card form
				resultCarteCreditoSubmit = true;
			}

			/* Mappatura codici lista degli aeroporti */
			frequentFlyerCompaniesSelected = new HashMap<String, Boolean>();
			if (profileResponse.getCustomerProfile().getLifestyles() != null){
				for (MMLifestyleData lifeStyle : profileResponse.getCustomerProfile().getLifestyles()) {
					if (lifeStyle.getCategory().equals("FREQUENT FLYER PROGRAMS")) {
						frequentFlyerCompaniesSelected.put(
								CompagnieAeree.fromValue(lifeStyle.getLifestyle()).value(), true);
					}
				}
			}
			customer.setFrequentFlyerCompaniesSelected(frequentFlyerCompaniesSelected);

			profiloUtenteRender.getCustomerData().setQualifiedFlights(customerProfileData.getQualifiedFlights());
			profiloUtenteRender.getCustomerData().setExpirationDate(customerProfileData.getExpirationDate());



			logger.debug("checkinSessionContext creata");
			try{
//                    logger.info("[CheckinFrequentFlyerSearchServlet] invoco il checkinSession");
//                    CheckinFrequentFlyerSearchResponse result = checkinSession.checkinFrequentFlyerSearch(request, checkinSessionContext);
				CheckinFrequentFlyerSearchResponse result = (CheckinFrequentFlyerSearchResponse) session.getAttribute("searchbyfrequentflyerMM");
				if (result != null && result.getPnrData() != null) {

					customer.setPnrData( result.getPnrData() );
				}  else {
					logger.error("[CheckinFrequentFlyerSearchServlet] - Errore durante l'estrazione dei dati dalla sessione");
				}
			} catch (Exception e) {
				throw new RuntimeException("[CheckinFrequentFlyerSearchServlet] - Errore durante l'estrazione dei dati dalla sessione", e);
			}

            customer.setMmCustomer(profiloUtenteRender.getCustomerData());
			request.getSession(true).setAttribute("CUSTOMER", new CacheWrapper<Customer>( customer, 5*60*1000) );

			if ( session.getAttribute("CUSTOMER")  != null ) {
				try {
					CacheWrapper<Customer> cacheWrapper = (CacheWrapper<Customer>) session.getAttribute("CUSTOMER");
					if (cacheWrapper.valid()) {
						this.customer = cacheWrapper.getValue();
						if(cacheWrapper.getValue().getMmCustomer()!=null){
							profiloUtenteRender = new ProfiloUtenteRender(cacheWrapper.getValue().getMmCustomer());
						}
						return;
					}
				} catch (Exception  ex) {
					session.removeAttribute("CUSTOMER");
				}
			};

		} catch (Exception e) {
			logger.error("Exception in initModel of ProfileUtenteModel  : ",e);
            request.getSession(true).removeAttribute("CUSTOMER");
		}
	}


	protected CheckinSessionContext creaCheckinSessionContext(String passengerName, String passengerSurname, String frequentFlyer, CheckinSession checkinSession){

		// String passengerName = request.getParameter("firstName") == null ? "" : request.getParameter("firstName");
		// String passengerSurname = request.getParameter("lastName") == null ? "" : request.getParameter("lastName");
		String pnr = ""; //request.getParameter("pnr") == null ? "" : request.getParameter("pnr");

		//Per il ticket viene preso il getParmeter("pnr") perch� in fase di sviluppo si � pensato di utilizzare nella form lo stesso input.
		String ticket = ""; //request.getParameter("pnr") == null ? "" : request.getParameter("pnr");

//		String frequentFlyer = request.getParameter("frequentFlyer") == null ? "" : request.getParameter("frequentFlyer");

		//TODO - inserisco in sessione il conversationID (utile per ogni servizio invocato)
		String conversationId = IDFactory.getTid();
//		String tripId = request.getParameter("checkin__code");
//		String MMcode = request.getParameter("checkin__MMcode");
//		String pin = request.getParameter("checkin__pin");
		String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		String language = AlitaliaUtils.getRepositoryPathLanguage(request.getResource());
		String currencyCode = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencyCode", String.class);
		String currencySymbol = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencySymbol", String.class);
		Locale locale = AlitaliaUtils.getRepositoryPathLocale(request.getResource());

		String callerIp = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
		I18n i18n = new I18n(request.getResourceBundle(locale));
		String machineName = AlitaliaUtils.getLocalHostName("unknown");
		String searchType = (request.getParameter("checkinSearchType")!=null)?request.getParameter("checkinSearchType"): CheckinConstants.CHECKIN_SEARCH_LOGIN; //[login, code]

		// *Currency format info
		InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(request.getResource());
		String decimalSeparator=ivm.getInherited("decimalSeparator", String.class);
		String groupingSeparator=ivm.getInherited("groupingSeparator", String.class);
		Integer numDecDigits=ivm.getInherited("decimalDigit", Integer.class);
		if(numDecDigits == null){
			numDecDigits = 2;
		}
		NumberFormat currentNumberFormat = AlitaliaUtils.getNumberFormat(numDecDigits, decimalSeparator, groupingSeparator, market);

		HttpSession session = request.getSession(true);

		CheckinSessionContext ctx = null;

		try {
			ctx = checkinSession.initializeSessionRest(request, i18n, CheckinConstants.CHECKIN_CLIENT, machineName,
					(session.getId() != null ? session.getId() : ""), CheckinConstants.CHECKIN_SITE, market, language, conversationId,
					currencyCode, currencySymbol, locale, callerIp, pnr, ticket, frequentFlyer,
					passengerName, passengerSurname, searchType, currentNumberFormat, null);

		} catch (CheckinPnrInformationNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		//session.setAttribute(CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE, ctx);
		return ctx;
	}

	public ProfiloUtenteRender getProfiloUtenteRender() {
		return profiloUtenteRender;
	}

	public GetDatiSicurezzaProfiloResponseLocal getDatiSicurezzaProfiloRender() {
		return datiSicurezzaProfiloRender;
	}

	public Boolean getResultDatiPersonaliSubmit() {
		return resultDatiPersonaliSubmit;
	}

	public Boolean getResultDatiLoginSubmit() {
		return resultDatiLoginSubmit;
	}

	public Boolean getResultSocialLoginSubmit() {
		return resultSocialLoginSubmit;
	}

	public Boolean getResultOfferteComunicazioniSubmit() {
		return resultOfferteComunicazioniSubmit;
	}

	public Boolean getResultPreferenzeViaggioSubmit() {
		return resultPreferenzeViaggioSubmit;
	}

	public Boolean getResultCarteCreditoSubmit() {
		return resultCarteCreditoSubmit;
	}

	public SocialAccountData[] getSocialAccounts() {
		return socialAccounts;
	}

	public Map<String, Boolean> getFrequentFlyerCompaniesSelected() {
		return frequentFlyerCompaniesSelected;
	}

	public Boolean getIsYoung() {
		return isYoung;
	}

	/* Metodi private per il recupero dei dati da reinserire form nei casi in cui l'aggiornamento dei dati fallisce */
	private void setDatiPersonaliFromSession(DatiPersonaliData datiPersonaliData) {
		profiloUtenteRender.getCustomerData().setGender(MMGenderTypeEnum.fromValue(datiPersonaliData.getGender()));
		profiloUtenteRender.getCustomerData().setCustomerWorkPosition(MMWorkPositionTypeEnum.fromValue(datiPersonaliData.getWorkPosition()));

		List<MMAddressData> addressList = new ArrayList<MMAddressData>();
		MMAddressData addressData = new MMAddressData();
		addressData.setAddressType(MMAddressTypeEnum.fromValue(datiPersonaliData.getAddressType()));
		addressData.setCompanyName(datiPersonaliData.getCompanyName());
		addressData.setStreetFreeText(datiPersonaliData.getAddress());
		addressData.setPostalCode(datiPersonaliData.getPostalCode());
		addressData.setMunicipalityName(datiPersonaliData.getCity());
		addressData.setCountry(datiPersonaliData.getCountry());
		addressData.setStateCode(datiPersonaliData.getState());
		addressData.setMailingIndicator("M");
		addressList.add(addressData);
		profiloUtenteRender.getCustomerData().setAddresses(addressList);
		List<MMTelephoneData> telephoneDataList = new ArrayList<MMTelephoneData>();
		MMTelephoneData emailData = new MMTelephoneData();
		emailData.setTelephoneType(MMPhoneTypeEnum.EMAIL);
		emailData.setEmail(datiPersonaliData.getEmail());
		telephoneDataList.add(emailData);
		MMTelephoneData telephoneData = new MMTelephoneData();
		telephoneData.setCountryNumber(datiPersonaliData.getPhonePrefix());
		telephoneData.setNumber(datiPersonaliData.getPhoneNumber());
		telephoneData.setTelephoneType(MMPhoneTypeEnum.fromValue(datiPersonaliData.getPhoneType()));
		telephoneData.setPhoneZone(datiPersonaliData.getAreaPrefix());
		telephoneDataList.add(telephoneData);
		profiloUtenteRender.getCustomerData().setTelephones(telephoneDataList);

		if (datiPersonaliData.getCheckMillemigliaYoung()) {
			MMContractData contract = new MMContractData();
			contract.setName("YG");
			contract.setStatus(MMContractStatusEnum.REGISTERED);
			List<MMContractData> contractList = new ArrayList<MMContractData>();
			contractList.add(contract);
			profiloUtenteRender.getCustomerData().setContracts(contractList);
		}

	}

	/*
	 * isYoungRange
	 *
	 */
	private boolean isYoungRange(String testYear) {
		if (null != testYear && !"".equals(testYear)) {
			int yearOfBirth = Integer.parseInt(testYear);
			int currentYear = Calendar.getInstance().get(Calendar.YEAR);
			return (currentYear - yearOfBirth >= 14
					&& currentYear - yearOfBirth <= 25);
		}
		return false;
	}

    public Customer getCustomer() {
        return customer;
    }

	//Tolentino - Inizio
	public MACustomer getMaCustomer() {
		return customer.getMaCustomer();
	}

	public boolean isMaUserHasCreditCards() {
		return maUserHasCreditCards;
	}

	public boolean getLoggedMAUser() {
		return loggedMAUser;
	}

	public String getActualIsoTime() {
		return actualIsoTime;
	}
	//Tolentino - Fine
	public AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

}