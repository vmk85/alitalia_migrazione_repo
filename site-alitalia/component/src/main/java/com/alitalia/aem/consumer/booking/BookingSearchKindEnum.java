package com.alitalia.aem.consumer.booking;

public enum BookingSearchKindEnum {
	
	SIMPLE("simple"),
    ROUNDTRIP("roundtrip"),
    MULTILEG("multileg");
    private final String value;
    
    BookingSearchKindEnum(String v) {
        value = v;
    }
    
    public String value() {
    	return this.value;
    }
}