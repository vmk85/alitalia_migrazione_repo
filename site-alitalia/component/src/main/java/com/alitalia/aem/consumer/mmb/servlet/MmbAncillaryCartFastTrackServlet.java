package com.alitalia.aem.consumer.mmb.servlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "mmbancillarycartfasttrack" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class MmbAncillaryCartFastTrackServlet extends GenericMmbFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile MmbSession mmbSession;	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		if ("1".equals(request.getParameter("clear"))) {
			mmbSession.clearAncillariesFromCart(getMmbSessionContext(request), MmbAncillaryTypeEnum.FAST_TRACK, null);
			
		} else {
			Map<Integer, Boolean> fastTrackSelectionByAncillaryId = new HashMap<>();
			String[] split;
			String[] selections = request.getParameterValues("fasttrack-selection");
			if (selections != null) {
				for (String selection : selections) {
					logger.debug("Processing fasttrack-selection {}", selection);
					split = selection.split("#", 2);
					String[] ancillaryIds = split[0].split("-");
					String ancillaryValue = split[1];
					for (String ancillaryId : ancillaryIds) {
						logger.debug("Evaluating ancillary id {} with value {}", ancillaryId, ancillaryValue);
						fastTrackSelectionByAncillaryId.put(Integer.parseInt(ancillaryId), "1".equals(ancillaryValue));
					}
				}
			}
			
			mmbSession.addOrRemoveFastTrackFromCart(getMmbSessionContext(request), fastTrackSelectionByAncillaryId);
			
		}
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value("OK");
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}
