package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;
import java.math.BigDecimal;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbAncillaryInsurance extends MmbSessionAncillaryGenericController {
	 
	@Self
	private SlingHttpServletRequest request;
	
	private MmbPriceRender totalInsuranceCost;
	
	private String priceText;
	
	private boolean added;
	
	private boolean bought;
	
	@PostConstruct
	protected void initModel() throws IOException {
		initBaseModel(request);
		BigDecimal amount = null;
		added = false;
		bought = false;
		if (ctx.insurancePolicy != null) {
			amount = ctx.insurancePolicy.getTotalInsuranceCost();
			if (ctx.insurancePolicy.getPolicyNumber() == null || "".equals(ctx.insurancePolicy.getPolicyNumber())) {
				added = ctx.insuranceAddedToCart;
				bought = false;
			} else {
				added = true;
				bought = true;
			}
			
		}
		totalInsuranceCost = 
				new MmbPriceRender(amount, ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat);
		
		
		 
		priceText = totalInsuranceCost.getFormattedAmountWithCurrency();
		
		
	}

	public MmbPriceRender getTotalInsuranceCost() {
		return totalInsuranceCost;
	}
	
	public String getPriceText() {
		return priceText;
	}
	
	
	public boolean getAdded() {
       
        return added;
	}
	
	public boolean getBought() {
	       
        return bought;
	}
}
