package com.alitalia.aem.consumer.checkin.servlet;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.PDFManager;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinFlightDataMail;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.servlet.PDFCreatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkinboardingpass" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinBoardingPassServlet extends GenericCheckinFormValidatorServlet {

	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private PDFManager pdfManager;
	
	@Reference
	private CheckinSession checkinSession;
	
	private final static String CUMULATIVE_EMAIL = "c_email";
	private final static String EMAIL = "email";
	private final static String EMAIL_ADDRESS = "email_address_";
	private final static String SMS = "sms";
	private final static String SMS_PREFIX = "sms_prefix_";
	private final static String SMS_NUMBER = "sms_number_";
	private static final String KEY_PREFIX = "airportsData.";
	private static final String CITY_SUFFIX = ".city";
	private static final String NAME_SUFFIX = ".name";
	private final static String PDF = "pdf";
	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		logger.debug("[CheckinBoardingPassServlet] validateForm");
		
		// validate form
		ResultValidation resultValidation = performValidation(request);
		
		// return validation
		return resultValidation;

	}
	
	/*
	 * Perform validation
	 */
	private ResultValidation performValidation(
			SlingHttpServletRequest request) {
		logger.debug("[CheckinBoardingPassServlet] performValidation");
		
		// get CTX context data
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		
		// create a new Validator
		Validator validator = new Validator();
		
		// type of boarding pass == "now" ?
		String boardingPassType = request.getParameter("typeofboardingpass");
		
		if ("now".equals(boardingPassType)) {
			
			String email = request.getParameter(EMAIL);
			String sms = request.getParameter(SMS);
			
			// check email addresses and SMS numbers
			int i = -1;
			List<CheckinPassengerData> passengers =
					CheckinUtils.getCheckedInPassengers(ctx.passengers);
			for (CheckinPassengerData passengerData : passengers) {
				if (passengerData.getRouteId() == ctx.selectedRoute.getId()) {
					++i;
					
					String boardingPassRetrieveEmail =
							request.getParameter(EMAIL_ADDRESS + i);
					String boardingPassRetrieveSmsPrefix =
							request.getParameter(SMS_PREFIX + i);
					String boardingPassRetrieveSmsNumber =
							request.getParameter(SMS_NUMBER + i);
					
					// validate email
					if (null != email && !"".equals(email)
							&& null != boardingPassRetrieveEmail
							&& !"".equals(boardingPassRetrieveEmail)) {
						validator.addDirectCondition(EMAIL_ADDRESS + i,
								boardingPassRetrieveEmail,
								CheckinConstants.MESSAGE_ERROR_INVALID_EMAIL,
								"isEmail");
					}
					if (null != email && !"".equals(email) && i==0) {
						validator.addDirectCondition(EMAIL_ADDRESS + i, 
								boardingPassRetrieveEmail, 
								CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, 
								"isNotEmpty");
					}
					
					// validate SMS
					if (null != sms && !"".equals(sms)
							&& null != boardingPassRetrieveSmsNumber
							&& !"".equals(boardingPassRetrieveSmsNumber)) {
		
						// check prefix
						RetrievePhonePrefixResponse response =
								checkinSession.retrievePhonePrefixes(ctx);
						List<String> phones = new ArrayList<String>();
						for (PhonePrefixData data : response.getPhonePrefix()) {
							phones.add(data.getPrefix());
						}
						validator.setAllowedValues(SMS_PREFIX + i,
								boardingPassRetrieveSmsPrefix,
								(String[]) phones.toArray(new String[phones.size()]),
								CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
						
						// check number
						validator.addDirectCondition(SMS_NUMBER + i,
								boardingPassRetrieveSmsPrefix
								+ boardingPassRetrieveSmsNumber,
								CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD,
								"isNumeroTelefonoValido");
					}
					if (null != sms && !"".equals(sms) && i==0) {
						validator.addDirectCondition(SMS_NUMBER + i, 
								boardingPassRetrieveSmsNumber, 
								CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, 
								"isNotEmpty");
				}
			}
			}

		} else {
			
			// validate cumulative email address
			validator.addDirectCondition(
					CUMULATIVE_EMAIL, request.getParameter(CUMULATIVE_EMAIL),
					CheckinConstants.MESSAGE_ERROR_INVALID_EMAIL,
					"isEmail");
			
		}
		
		// validate!
		return validator.validate();
		
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
		logger.debug("[CheckinBoardingPassServlet] performSubmit");
		
		// get ctx context data
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		
		// calculate redirect and PDF URLs
		String baseUrl = request.getResource().getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(
						request.getResource(), false));
		String nextPage = configuration.getCheckinConfirmationPage();
		String pdfUrl = null;
		
		// set boarding pass TYP to empty
		ctx.boardingPassTYP = 3;
		ctx.boardingPassPDFRequested = false;
		ctx.boardingPassEmailRequested = false;
		ctx.boardingPassSMSRequested = false;

		// get only checked in passengers
		List<CheckinPassengerData> passengers =
				CheckinUtils.getCheckedInPassengers(ctx.passengers);
		
		// type of boarding pass == "now" ?
		// then retrieve PDF and send SMS & email
		String boardingPassType = request.getParameter("typeofboardingpass");
		String email = request.getParameter(EMAIL);
		String sms = request.getParameter(SMS);
		String pdf = request.getParameter(PDF);
		if ("now".equals(boardingPassType) &&
				((null != email && !"".equals(email)) ||
				(null != sms && !"".equals(sms)) ||
				(null != pdf && !"".equals(pdf)))) {
		
			// overwrite email and SMS 
			int i = -1;
			for (CheckinPassengerData passengerData : passengers) {
				if (passengerData.getRouteId() == ctx.selectedRoute.getId()) {
					++i;
					
					String boardingPassRetrieveEmail =
							request.getParameter(EMAIL_ADDRESS + i);
					String boardingPassRetrieveSmsPrefix =
							request.getParameter(SMS_PREFIX + i);
					String boardingPassRetrieveSmsNumber =
							request.getParameter(SMS_NUMBER + i);
					
					if (null != email && !"".equals(email)
							&& null != boardingPassRetrieveEmail
							&& !"".equals(boardingPassRetrieveEmail)) {
						passengerData.setBoardingPassRetrieveEmail(
								boardingPassRetrieveEmail);
						passengerData.setBoardingPassViaEmail(true);
						ctx.boardingPassEmailRequested = true;
					}
					
					if (null != sms && !"".equals(sms)
							&& null != boardingPassRetrieveSmsPrefix
							&& null != boardingPassRetrieveSmsNumber
							&& !"".equals(boardingPassRetrieveSmsPrefix)
							&& !"".equals(boardingPassRetrieveSmsNumber)) {
						passengerData.setBoardingPassRetrieveSms(
								"00"
								+ boardingPassRetrieveSmsPrefix
								+ boardingPassRetrieveSmsNumber);
						passengerData.setBoardingPassViaSms(true);
						ctx.boardingPassSMSRequested = true;
					}
				}
			}
			
			// call service and update accepted passengers
			checkinSession.getBoardingPass(ctx);
			
			if (null == ctx.passengers || 0 == ctx.passengers.size()) {
				throw new Exception("[CheckinBoardingPassServlet] Passengers "
						+ "list from boardingPassCheckinResponse is empty");
			}
			
			// create list of boarding pass email receivers
			List<String> receiverMails = new ArrayList<String>();
			for (CheckinPassengerData passengerData : passengers) {
				if (null != passengerData.getBoardingPassRetrieveEmail()) {
					receiverMails.add(
							passengerData.getBoardingPassRetrieveEmail());
				}
			}
			
			// send email with boarding pass to passengers
			String[] values = getValuesMailBoarding(ctx, request);
			
			if ((null != email && !"".equals(email)) ||
				(null != sms && !"".equals(sms))) {
				
			if (Boolean.FALSE.equals(checkinSession.sendEmail(
					ctx, request.getResource(), receiverMails,
					CheckinConstants.CHECKIN_TEMPLATE_EMAIL_BOARDING_PASS,
					CheckinConstants.CHECKIN_BOARDING_PASS_EMAIL_SUBJECT,
					pdfManager.getBoardingPassPDF(request, response),
					CheckinConstants.CHECKIN_BOARDING_PASS_EMAIL_ATTACHMENT,
					CheckinConstants.PLACEHOLDER_BOARDING, values,
						CheckinConstants.CHECKIN_MAIL_BP,
						passengers))) {
				nextPage = configuration.getCheckinFailurePage();
			}
			}

			// send PDF to output ?
			if (null != request.getParameter("pdf")) {
				String bpUrl = configuration.getCheckinBoardingPassPage();
				pdfUrl = bpUrl.substring(0, bpUrl.length() - 5) + ".createpdf?"
						+ PDFCreatorServlet.QUERY_PARAM
						+ "=" + PDFCreatorServlet.QUERY_CHECKIN;
				ctx.boardingPassPDFRequested = true;
			}
			
			// set boarding pass TYP to default
			ctx.boardingPassTYP = 1;

		}

		// type of boarding pass == "airport" ?
		else if (!"now".equals(boardingPassType)) {
			
			
			String[] values = getValuesMailRecap(ctx, request);
			// send cumulative email
			List<String> receiverMails = new ArrayList<String>();
			receiverMails.add(request.getParameter(CUMULATIVE_EMAIL));
			if (!Boolean.TRUE.equals(checkinSession.sendEmail(
					ctx, request.getResource(), receiverMails,
					CheckinConstants.CHECKIN_TEMPLATE_EMAIL_CUMULATIVE,
					CheckinConstants.CHECKIN_CUMULATIVE_EMAIL_SUBJECT,
					pdfManager.getCheckinSummaryPDF(request, response),
					CheckinConstants.CHECKIN_CUMULATIVE_EMAIL_ATTACHMENT,
					CheckinConstants.PLACEHOLDER_RECAP, values,
					CheckinConstants.CHECKIN_MAIL_REMINDER,
					passengers))) {
				nextPage = configuration.getCheckinFailurePage();
			}
			
			// set boarding pass TYP to airport
			ctx.boardingPassTYP = 2;
			
		}
		
		// send response to client
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		json.key("result").value("OK");
		if (null != pdfUrl) {
			json.key("pdf").value(baseUrl + pdfUrl);
		}
		json.key("redirect").value(baseUrl + nextPage);
		json.endObject();
		
	}
	
	private String[] getValuesMailBoarding(CheckinSessionContext ctx, SlingHttpServletRequest request) {
		String[] values = new String[4];
		values[0] = ctx.passengerName + " " + ctx.passengerSurname;
		values[1] = getPnr(ctx);
		values[2] = getFlightsHtml(ctx, request);
		values[3] = computeBusMessage(ctx);
		return values;
	}
	
	private String computeBusMessage(CheckinSessionContext ctx) {
		String[] messageFromStation = CheckinUtils.getMessageFromBusToAirport(ctx,ctx.i18n);
		String[] messageToStation = CheckinUtils.getMessageFromAirportToBus(ctx,ctx.i18n);
		String[] messageFromToStation = CheckinUtils.getMessageFromBusToBus(ctx,ctx.i18n);
		
		String messageFromBus = "";
		for (int i = 0; i < messageFromStation.length; i++) {
			messageFromBus += "<p>" + messageFromStation[i] + "</p>";
		}
		
		String messageToBus = "";
		for (int i = 0; i < messageToStation.length; i++) {
			messageToBus += "<p>" + messageToStation[i] + "</p>";
		}
		
		String messageFromToBus = "";
		for (int i = 0; i < messageFromToStation.length; i++) {
			messageFromToBus += "<p>" + messageFromToStation[i] + "</p>";
		}
		
		return messageFromBus + messageToBus + messageFromToBus;
	}

	private String[] getValuesMailRecap(CheckinSessionContext ctx, SlingHttpServletRequest request) {
		String[] values = new String[3];
		values[0] = ctx.passengerName + " " + ctx.passengerSurname;
		values[1] = getPassengersHtml(ctx);
		values[2] = getFlightsHtml(ctx, request);
		return values;
	}
	
	
	private String getPnr(CheckinSessionContext ctx) {
		if (ctx.passengers!= null && ctx.passengers.size()>0) {
			return ctx.passengers.get(0).getPnr();
		}
		return "";
	}
	
	private String getPassengersHtml(CheckinSessionContext ctx) {
		String html = "";
		List<CheckinPassengerData> passengers = CheckinUtils.getCheckedInPassengers(ctx.passengers);
		for (CheckinPassengerData passenger : passengers) {
			html = html + "<tr><td width='50%' style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top;'><p style='color: #0d4722; font-family: Helvetica,Arial,sans-serif; font-size: 16px; line-height: 24px; margin: 0; padding: 0; text-align: left; text-transform: uppercase; font-weight: bold;'>" + passenger.getName() + " " + passenger.getLastName() + "</p></td><td width='50%' style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top;'><p style='margin: 0; padding: 0; line-height: 24px; font-weight: bold;'>" + passenger.getPnr() + "</p></td></tr>";
		}
		return html;
	}
	
	private String getFlightsHtml(CheckinSessionContext ctx, SlingHttpServletRequest request) {
		String html = "";
		List<CheckinFlightDataMail> flights = toFlightData(ctx, ctx.selectedRoute, request);
		for (CheckinFlightDataMail flight : flights) {
			html = html + "<tr><td style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top;' width='26%'><table border='0' cellpadding='0' cellspacing='0' style=' border: 0; margin: 0; padding:0;  display: inline-block;padding: 0; text-align: left; vertical-align: top;'><tr><td style='border: 0; margin: 0; padding: 0; vertical-align: top;' width='20%'><span style='display: inline-block; font-size: 28px; vertical-align: top; font-family: Helvetica,Arial,sans-serif;'>" + flight.getDay() + "</span></td><td style='border: 0; margin: 0; padding: 0; vertical-align: top;' width='23%'><span style='display: inline-block; font-size: 12px; line-height: 14px; text-transform: uppercase; vertical-align: top; font-family: Helvetica,Arial,sans-serif;'>" + flight.getMonth() + "<br>" + flight.getYear() + "</span></td><td style='border: 0; margin: 0; padding: 0; vertical-align: top;' width='3%'>&nbsp;</td><td style='border: 0; margin: 0; padding: 0; vertical-align: top;' width='50%'><span style='display: inline-block; font-size: 24px; text-transform:uppercase; vertical-align: top; font-family: Helvetica,Arial,sans-serif;'>" + flight.getFlightNumber() + "</span></td></tr></table></td><td style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top;' width='2%'>&nbsp;</td><td style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top; 'width='35%'><table border='0' cellpadding='0' cellspacing='0' style=' padding: 0; text-align: left; vertical-align: top; width: 100%;'><tr><td style='border: 0; margin: 0; padding: 0; text-align: left; vertical-align: top;' width='30%'><span style='display: inline-block; font-size: 24px; vertical-align:top;font-family: Helvetica,Arial,sans-serif; '>" + flight.getDepartureHour() + " &zwnj;</span></td><td style='border:0;margin: 0; padding: 0; text-align: left; vertical-align: top' width='65%'><span style='display: inline-block; font-size: 12px; line-height: 14px; text-transform: uppercase; vertical-align: top;font-family: Helvetica,Arial,sans-serif; '>" + flight.getDepartureCity() + " <br>" + flight.getDepartureAirport() + " " + flight.getDepartureCode() + "</span></td></tr></table></td><td style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top;' width='2%'>&nbsp;</td><td style='border: 0; margin: 0; padding: 0; text-align: center; vertical-align: top; 'width='35%'><table border='0' cellpadding='0' cellspacing='0' style=' padding: 0; text-align: left; vertical-align: top; width: 100%;'><tr><td style='border: 0; margin: 0; padding: 0; text-align: left; vertical-align:top;' width='30%'><span style='display: inline-block; font-size: 24px; vertical- align: top;font-family: Helvetica,Arial,sans-serif; '>" + flight.getArrivalHour() + " &zwnj;</span></td><td style='border:0;margin: 0; padding: 0; text-align: left; vertical-align: top' width='65%'><span style='display: inline-block; font-size: 12px; line-height: 14px; text-transform: uppercase; vertical-align: top;font-family: Helvetica,Arial,sans-serif; '>" + flight.getArrivalCity() + " <br>" + flight.getArrivalAirport() + " " + flight.getArrivalCode() + "</span></td></tr></table></td></tr>";
		}
		return html;
	}
	
	private List<CheckinFlightDataMail> toFlightData(CheckinSessionContext ctx, CheckinRouteData route, SlingHttpServletRequest request) {
		Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		List<CheckinFlightDataMail> checkinFlightDataMail = new ArrayList<CheckinFlightDataMail>();
		List<CheckinFlightData> flightsData = route.getFlights();
		for (CheckinFlightData flightData : flightsData) {
			CheckinFlightDataMail flight = new CheckinFlightDataMail();
			DateRender dateTimeRenderDep = new DateRender(flightData.getDepartureDateTime());
			flight.setDay(dateTimeRenderDep.getDay());
			flight.setMonth(i18n.get(dateTimeRenderDep.getShortTextMonth()));
			flight.setYear(dateTimeRenderDep.getYear());
			flight.setDepartureHour(dateTimeRenderDep.getHour24());
			DateRender dateTimeRenderArr = new DateRender(flightData.getArrivalDateTime());
			flight.setArrivalHour(dateTimeRenderArr.getHour24());
			flight.setFlightNumber(flightData.getCarrier()+ flightData.getFlightNumber());
			String departureAirport = "";
			String cityDeparture = i18n.get(KEY_PREFIX + flightData.getFrom().getCode() + CITY_SUFFIX ); 
			String departureCode = "";
			if (flightData.isFromBusStation()) {
				departureAirport = "(" + flightData.getFrom().getCode() + ")";
				departureCode = " - " + ctx.i18n.get("checkin.bus.operatoDa.label");
			} else {
				departureAirport = i18n.get(KEY_PREFIX + flightData.getFrom().getCode() + NAME_SUFFIX);
				departureCode = "(" + flightData.getFrom().getCode() + ")";
			}
			flight.setDepartureAirport(departureAirport);
			flight.setDepartureCity(cityDeparture);
			flight.setDepartureCode(departureCode);
			
			String arrivalAirport = "";
			String cityArrival = i18n.get(KEY_PREFIX + flightData.getTo().getCode() + CITY_SUFFIX ); 
			String arrivalCode = "";
			if (flightData.isToBusStation()) {
				arrivalAirport = "(" + flightData.getTo().getCode() + ")";
				arrivalCode = " - " + ctx.i18n.get("checkin.bus.operatoDa.label");
			} else {
				arrivalAirport = i18n.get(KEY_PREFIX + flightData.getTo().getCode() + NAME_SUFFIX);
				arrivalCode = "(" + flightData.getTo().getCode() + ")";
			}
			flight.setArrivalCity(cityArrival);
			flight.setArrivalCode(arrivalCode);
			flight.setArrivalAirport(arrivalAirport);
			
			checkinFlightDataMail.add(flight);
			
		}
		return checkinFlightDataMail;
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}
