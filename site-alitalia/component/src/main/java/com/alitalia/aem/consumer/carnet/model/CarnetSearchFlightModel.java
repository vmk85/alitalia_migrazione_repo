package com.alitalia.aem.consumer.carnet.model;



import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.carnet.model.GenericCarnetModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CarnetSearchFlightModel extends GenericCarnetModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	private String maxPassengersAR;
	private String maxPassengersA;
	private boolean outboundEnabled;
	
	private static final int DEFAULT_MAX_PASSENGERS = 7;
	
	
	@PostConstruct
	protected void initModel() {
	
		initBaseModel(request);
		
		if (ctx!=null && ctx.infoCarnet != null) {
			int carnetNumber = ctx.infoCarnet.getResidualRoutes();
			outboundEnabled = true;
			if (carnetNumber == 1) {
				outboundEnabled = false;
			}
			if (carnetNumber>DEFAULT_MAX_PASSENGERS) {
				maxPassengersA = Integer.toString(DEFAULT_MAX_PASSENGERS);
			}
			else {
				maxPassengersA = Integer.toString(carnetNumber);
			}
		    
			int carnetNumberOutbound = carnetNumber / 2;
			maxPassengersAR = Integer.toString(carnetNumberOutbound);
			
		}
		
		
		

	}


	public String getMaxPassengersAR() {
		return maxPassengersAR;
	}
	
	public String getMaxPassengersA() {
		return maxPassengersA;
	}


	public boolean isOutboundEnabled() {
		return outboundEnabled;
	}


	


	
	
}