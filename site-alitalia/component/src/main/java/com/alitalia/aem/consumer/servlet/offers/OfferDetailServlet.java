package com.alitalia.aem.consumer.servlet.offers;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.io.JSONWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.BestPriceData;
import com.alitalia.aem.common.data.home.OfferData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.BestPriceTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersResponse;
import com.alitalia.aem.common.utils.AlitaliaCommonUtils;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.AlitaliaGeolocalizationManager;
import com.alitalia.aem.consumer.AlitaliaGeolocalizationManager.AlitaliaGeolocalizationData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.geospatial.delegate.GeoSpatialDelegate;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;


@Component(name = "Alitalia Offer Detail Servlet", metatype = false, configurationFactory = false)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
		@Property(name = "sling.servlet.selectors", value = "offerdetail"),
		@Property(name = "sling.servlet.extensions", value = {"json"}),
		@Property(name = "sling.servlet.methods", value = "GET")
})
@SuppressWarnings("serial")
public class OfferDetailServlet extends SlingSafeMethodsServlet {
	
	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Reference
	private GeoSpatialDelegate geoSpatialDelegate;
	
	@Reference
	private StaticDataDelegate staticDataDelegate;
	
	@Reference
	private SearchFlightsDelegate searchFlightsDelegate;
	
	@Reference
	private AlitaliaGeolocalizationManager geoManager;

	@Reference
	private AlitaliaConfigurationHolder alitaliaConfiguration;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(SlingHttpServletRequest request, 
			SlingHttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("application/json; charset=utf-8");
		TidyJSONWriter out = new TidyJSONWriter(response.getWriter());

		try {
			
			try {
				
				boolean young = request.getParameter("isYoung") != null && request.getParameter("isYoung").equals("true");
				String paxType = young ? "yg" : "ad";
				
				String arrivalCode = request.getParameter("to");
				if (arrivalCode == null || arrivalCode.isEmpty()) {
					logger.warn("[OfferDetailServlet] Aeroporto di arrivo non specificato");
					out.object();
					out.key("result").value(false);
					out.endObject();
					return;
				}
				
				String departureCode = request.getParameter("from");
				String selectedMonth=request.getParameter("selectedMonth");
				// Geolocalization
				
				if (departureCode == null || departureCode.isEmpty()) {
					AlitaliaGeolocalizationData geoData = geoManager.processFromRequest(request, response);
					if (geoData != null) {
						logger.debug("[OfferDetailServlet] geoData.cityCode = {}", geoData.getCityCode());
						departureCode = geoData.getCityCode();
					} else {
						logger.error("[OfferDetailServlet] Geolocalization data non disponibile.");
						
					}	
				}
				
				if (departureCode == null || departureCode.isEmpty()) {
					logger.warn("[OfferDetailServlet] Aeroporto di partenza non specificato o non calcolabile tramite geolocalizzazione");
					out.object();
					out.key("result").value(false);
					out.endObject();
					return;
				}
				
				AirportData departure = null;
				AirportData arrival = null;
				
				RetrieveAirportsRequest airportsRequest = new RetrieveAirportsRequest(IDFactory.getTid(), IDFactory.getSid());
				
				Locale locale = AlitaliaUtils.getRepositoryPathLocale(request.getResource());
				airportsRequest.setMarket(locale.getCountry());
				airportsRequest.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()));
				RetrieveAirportsResponse airportsResponse = staticDataDelegate.retrieveAirports(airportsRequest);
				
				if (airportsResponse != null && airportsResponse.getAirports() != null && !airportsResponse.getAirports().isEmpty()) {
					for (AirportData airportData : airportsResponse.getAirports()) {
						if (airportData.getCode().equals(departureCode)) {
							departure = airportData;
						}
						if (airportData.getCode().equals(arrivalCode)) {
							arrival = airportData;
						}
					}
				}
				
				if (departure == null || arrival == null) {
					logger.warn("[OfferDetailServlet] Aeroporto {} non valido", 
							departure == null && arrival == null ? departureCode + ", " + arrivalCode : (departure == null ? departureCode : arrivalCode));
					out.object();
					out.key("result").value(false);
					out.endObject();
					return;
				}
				
				//INIZIO - MODIFICA INDRA 18/07/2017 ci è stata fatta richiesta di ripristinare il servizio "retrieveGeoOffers".
				//String target = "OFFERS";//questa riga deve essere decommentata nel richiamare il metodo retrieveGeoOffersByTarget
				
				RetrieveGeoOffersRequest serviceRequest = new RetrieveGeoOffersRequest();
				
				serviceRequest.setSid(IDFactory.getSid(request));
				serviceRequest.setTid(IDFactory.getTid());
				serviceRequest.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()));
				serviceRequest.setMarketCode(locale.getCountry());
				serviceRequest.setDepartureAirportCode(departure.getCode().toLowerCase());
				serviceRequest.setPaxType(paxType);
				//serviceRequest.setTarget(target);//questa riga deve essere decommentata nel richiamare il metodo retrieveGeoOffersByTarget
				//serviceRequest.setMonthDailyPrices(false);//questa riga deve essere decommentata nel richiamare il metodo retrieveGeoOffersByTarget
				
				RetrieveGeoOffersResponse serviceResponse = null;
				
				
				//serviceResponse = geoSpatialDelegate.retrieveGeoOffersByTarget(serviceRequest);
				serviceResponse = geoSpatialDelegate.retrieveGeoOffers(serviceRequest);
				//FINE.
				
				List<OfferData> geoOffersList = serviceResponse.getOffers();
				List<OfferData> offerDataList = null;
				try {
					offerDataList = (List<OfferData>) AlitaliaCommonUtils.deepClone(geoOffersList);
				} catch (IOException e) {
					logger.error("Error cloning object", e);
				}
				// PER TEST
				/*if (departure == null) { departure = new AirportData(); }
				if (arrival == null) { arrival = new AirportData(); }
				List<OfferData> offerDataList = getOfferDataList(departureCode, arrivalCode, departure, arrival);*/
				
				OfferData offerData = null;
				
				if (offerDataList != null && !offerDataList.isEmpty()) {
					// Recupero l'oggetto offerData per la destinazione selezionata;
					for (OfferData currentItem : offerDataList) {
						if (currentItem.getArrivalAirport().getCode().equals(arrival.getCode())) {
							offerData = currentItem;
							break;
						}
					}
				}
				
				if (offerData != null && offerData.getBestPrices() != null && !offerData.getBestPrices().isEmpty()) {
					
					boolean international = !offerData.getArrivalAirport().getArea().equals(AreaValueEnum.DOM);
					boolean roundTrip = offerData.getRouteType().equals(RouteTypeEnum.RETURN);
					
					String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
					logger.debug("Site market code: " + market);
					
					// Controllo aggiunto per mercato diverso da it come concordato con Di Flumeri
					// TODO Da spostare in una costante "it" 
					
					if ("it".equalsIgnoreCase(market)) {
						if (international && !roundTrip || !international && roundTrip) {
							logger.warn("[OfferDetailServlet] Tipologia offerta non compatibile: {}", 
									(international ? "international - " : "domestic - ") + (roundTrip ? "roundTrip" : "oneWay"));
							out.object();
							out.key("result").value(false);
							out.endObject();
							return;
						}
					} else {
						international = true;
					}
										
					List<BestPriceData> offerList = offerData.getBestPrices();
					Collections.sort(offerList);
					
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
					String rangeLow = dateFormat.format(offerList.get(0).getDate().getTime());
					Calendar dateHigh = (Calendar) offerList.get(0).getDate().clone();
					dateHigh.add(Calendar.MONTH, 4);
					String rangeHigh = dateFormat.format(dateHigh.getTime());
					
					BestPriceTypeEnum bestPriceType = international ? BestPriceTypeEnum.MONTH : BestPriceTypeEnum.DAY;
					
					BigDecimal lowestPrice = BigDecimal.valueOf(Double.MAX_VALUE);
					BigDecimal highestPrice = BigDecimal.ZERO;
					
					String numberOfNights = offerData.getNumberOfNights();
					
					// Recupero lowest e highest price
					for (BestPriceData offer : offerList) {
						// Controllo anche che il tipo di dato corrisponda all'area della destinazione
						if (offer.getPrice() != null && bestPriceType.equals(offer.getType())) {
							if (offer.getPrice().compareTo(highestPrice) > 0) {
								highestPrice = offer.getPrice();
							}
							if (offer.getPrice().compareTo(lowestPrice) < 0) {
								lowestPrice = offer.getPrice();
							}
						}
					}
					
					Map<Integer, Map<Integer, Map<Integer, BestPriceData>>> result = new LinkedHashMap<Integer, Map<Integer,Map<Integer,BestPriceData>>>();
					
					for (BestPriceData offer : offerList) {
						// Controllo anche che il tipo di dato corrisponda all'area della destinazione
						if (offer.getPrice() != null && bestPriceType.equals(offer.getType())) {
							
							int year = offer.getDate().get(Calendar.YEAR);
							int month = offer.getDate().get(Calendar.MONTH) + 1;
							int day = offer.getDate().get(Calendar.DAY_OF_MONTH);
							
							if (!result.containsKey(year)) {
								result.put(year, new LinkedHashMap<Integer, Map<Integer, BestPriceData>>());
							}
							Map<Integer, Map<Integer, BestPriceData>> map = result.get(year);
							
							if (!map.containsKey(month)) {
								map.put(month, new LinkedHashMap<Integer, BestPriceData>());
							}
							
							map.get(month).put(day, offer);
						}
					}
					
					ResourceBundle resourceBundle = request.getResourceBundle(
							AlitaliaUtils.findResourceLocale(request.getResource()));
					final I18n i18n = new I18n(resourceBundle);
					
					String departureCityKey = "airportsData." + departure.getCode() + ".city";
					String departureCity = i18n.get(departureCityKey);
					
					String arrivalCityKey = "airportsData." + arrival.getCode() + ".city";
					String arrivalCity = i18n.get(arrivalCityKey);
					
					if (departureCity.equals(departureCityKey)) {
						departureCity = departure.getCity();
					}
					
					if (arrivalCity.equals(arrivalCityKey)) {
						arrivalCity = arrival.getCity();
					}
					
					
					// print JSON
					out.object();

					out.key("result");
					out.object();
					
					out.key("departure");
					out.object();
					out.key("code").value(departure.getCode());
					out.key("city").value(departureCity);
					out.key("countryCode").value(departure.getCountryCode());
					out.endObject();
					
					out.key("arrival");
					out.object();
					out.key("code").value(arrival.getCode());
					out.key("city").value(arrivalCity);
					out.key("countryCode").value(arrival.getCountryCode());
					out.endObject();
					
					out.key("international").value(international);
					out.key("bestPrice").value(AlitaliaUtils.round(lowestPrice.doubleValue()+AlitaliaUtils.getExtraFee(alitaliaConfiguration, market, searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(departureCode, arrivalCode), searchFlightsDelegate.isSliceSicilyTerritorialContinuity(departureCode, arrivalCode)),request.getResource()));
					out.key("roundTrip").value(roundTrip);
					out.key("rangeLow").value(rangeLow);
					out.key("rangeHigh").value(rangeHigh);
					
					out.key("values");
					out.object();
					
					boolean bestPriceFound = false;
					boolean selectedMonthFound = false;
					for (int year : result.keySet()) {

						out.key(String.valueOf(year));
						out.object();
						
						Map<Integer, Map<Integer, BestPriceData>> innerMap = result.get(year);
						
						for (int month : innerMap.keySet()) {
							
							out.key(String.valueOf(month));
							out.object();
							
							Map<Integer, BestPriceData> offersMap = innerMap.get(month);
							
							for (int day : offersMap.keySet()) {
							
								out.key(String.valueOf(day));
								
								BestPriceData offer = offersMap.get(day);
								
								if (roundTrip) {
									out.object();
									out.key("amount").value(AlitaliaUtils.round(offer.getPrice().doubleValue()+AlitaliaUtils.getExtraFee(alitaliaConfiguration, market, searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(departureCode, arrivalCode), searchFlightsDelegate.isSliceSicilyTerritorialContinuity(departureCode, arrivalCode)),request.getResource()));
									out.key("height").value(computeHeight(offer.getPrice().doubleValue(), lowestPrice.doubleValue(), highestPrice.doubleValue()));
									
									if (!bestPriceFound && offer.getPrice().equals(lowestPrice)) {
										out.key("bestPrice").value(true);
										bestPriceFound = true;
									}

									int myMonth=0;
                                    if (tryParseInt(selectedMonth)) {
                                        myMonth=Integer.parseInt(selectedMonth);
                                    }


                                    if (!selectedMonthFound && month==myMonth) {
                                        out.key("selectedMonthFound").value(true);
                                        selectedMonthFound = true;
                                    }

									out.key("departureDate").value(dateFormat.format(offer.getDate().getTime()));
									
									// FIXME va usato numberOfNights o offer.getReturnDate() ??
									if (numberOfNights != null) {
										Calendar returnDate = Calendar.getInstance();
										returnDate.set(year, month - 1, day);
										returnDate.add(Calendar.DAY_OF_MONTH, Integer.parseInt(numberOfNights));
										out.key("returnDate").value(dateFormat.format(returnDate.getTime()));
									}
									
									out.endObject();
								}
								else if (offer.getPrice().equals(lowestPrice)) {
									out.object();
									out.key("highlight").value(true);
									out.key("amount").value(AlitaliaUtils.round(offer.getPrice().doubleValue()+AlitaliaUtils.getExtraFee(alitaliaConfiguration, market, searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(departureCode, arrivalCode), searchFlightsDelegate.isSliceSicilyTerritorialContinuity(departureCode, arrivalCode)),request.getResource()));
									out.endObject();
								} else {
									out.value(AlitaliaUtils.round(offer.getPrice().doubleValue()+AlitaliaUtils.getExtraFee(alitaliaConfiguration, market, searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(departureCode, arrivalCode), searchFlightsDelegate.isSliceSicilyTerritorialContinuity(departureCode, arrivalCode)),request.getResource()));
								}
							}
							out.endObject();
						}
						out.endObject();
					}
					
					out.endObject();
					out.endObject();
					out.endObject();
					
				} else {
					logger.warn("[OfferDetailServlet] Nessuna offerta trovata");
					out.object();
					out.key("result").value(false);
					out.endObject();
				}
				
			} catch (Exception e) {
				logger.error("[OfferDetailServlet] Error", e);
				out.object();
				out.key("result").value(false);
				out.endObject();
			}
		} catch (Exception e) {
			logger.error("[OfferDetailServlet] Error", e);
		}
	}

    boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
	
	private String computeHeight(double price, double lowestPrice, double highestPrice) {
		
		String height;
		
		double max = 200;
		double min = 100;
		
		if (lowestPrice == highestPrice) {
			return String.valueOf(min);
		}
		
		double deltaPx = max - min;
		double deltaP = highestPrice - lowestPrice;
		double deltaPrezzo = highestPrice - price;
		double x = (deltaPx * deltaPrezzo) / deltaP;
		double h = max - x;
        height = String.valueOf(Math.round(h));
        
		return height;
	}
	
	@SuppressWarnings("unused")
	private List<OfferData> getOfferDataList(String departure, String arrival, AirportData departureAirport, AirportData arrivalAirport) {
		
		String numberOfNights = "11";
		
		String[] domesticDestinations = {"ROM", "MIL", "TRN", "PSR"};
		
		boolean roundTrip = false;
		boolean international = true;
		
		if (Arrays.asList(domesticDestinations).contains(arrival)) {
			international = false;
		}
		
		if (departureAirport.getCode() == null) {
			departureAirport.setCode(departure);
			departureAirport.setCity(departure + " - city");
			departureAirport.setCountry(departure + " - country");
			departureAirport.setCountryCode(Arrays.asList(domesticDestinations).contains(departure) ? "IT" : "US");
		}
		
		if (arrivalAirport.getCode() == null) {
			arrivalAirport.setCode(arrival);
			arrivalAirport.setCity(arrival + " - city");
			arrivalAirport.setCountry(arrival + " - country");
			arrivalAirport.setCountryCode(Arrays.asList(domesticDestinations).contains(arrival) ? "IT" : "US");
		}
		
		List<OfferData> offerDataList = new ArrayList<OfferData>();
		
		OfferData offerData = new OfferData();
		
		Calendar expireData = Calendar.getInstance();
		expireData.add(Calendar.HOUR, 2);
		
		offerData.setExpireOfferData(expireData);
		offerData.setNumberOfNights(numberOfNights);
		
		AirportData offerArrivalAirport = new AirportData();
		offerArrivalAirport.setCode(arrival);
		offerArrivalAirport.setArea(international ? AreaValueEnum.INTC : AreaValueEnum.DOM);
		offerData.setArrivalAirport(offerArrivalAirport);
		
		AirportData offerDepartureAirport = new AirportData();
		offerDepartureAirport.setCode(departure);
		offerData.setDepartureAirport(offerDepartureAirport);
		
		offerData.setBestPrice(new BigDecimal("100.10"));
		offerData.setRouteType(roundTrip ? RouteTypeEnum.RETURN : RouteTypeEnum.OUTBOUND);
		
		List<BestPriceData> bestPriceList = new ArrayList<BestPriceData>();
		
		int size = international ? 6 : 20;
		
		for (int i = 0; i < size; i++) {
			Calendar date = Calendar.getInstance();
			if (international) {
				date.add(Calendar.MONTH, i);
			} else {
				date.add(Calendar.DAY_OF_MONTH, i);
			}
			BestPriceData item = new BestPriceData();
			item.setDate(date);
			item.setPrice(new BigDecimal((double) (100 + 100 * i) + (0.1 * (double) i)));
			item.setType(international ? BestPriceTypeEnum.MONTH : BestPriceTypeEnum.DAY);
			
			bestPriceList.add(item);
		}
		offerData.setBestPrices(bestPriceList);
		
		offerDataList.add(offerData);
		
		return offerDataList;
	}
}
