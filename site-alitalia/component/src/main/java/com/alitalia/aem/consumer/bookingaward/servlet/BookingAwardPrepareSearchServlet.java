package com.alitalia.aem.consumer.bookingaward.servlet;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSearchTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.model.SearchElementAirport;
import com.alitalia.aem.consumer.booking.model.SearchPassengersNumber;
import com.alitalia.aem.consumer.bookingaward.BookingAwardSession;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.TypeUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingawardpreparesearch" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class BookingAwardPrepareSearchServlet extends GenericBookingAwardFormValidatorServlet {
	
	@Reference
    private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private BookingAwardSession bookingAwardSession;
	
	private static final String STANDARD_REQUEST_DATE_FORMAT = "dd/MM/yyyy";
	private static final String ATTR_ORIGIN = "Origin";
	private static final String ATTR_DESTINATION = "Destination";
	private static final String ATTR_DEPARTURE_DATE = "DepartureDate";
	private static final String ATTR_RETURN_DATE = "ReturnDate";
	private static final String ATTR_SEARCH_TYPE = "SearchType";
	private static final String ATTR_NADULTS = "Adults";
	private static final String ATTR_NKIDS = "Children";
	private static final String ATTR_NBABY = "Infants";
	private static final String ATTR_CLASS_OF_FLIGHT = "classOfFlight";
	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {

		Validator validator = new Validator();

		// departure airport
		validator.addDirectCondition(ATTR_ORIGIN,
				request.getParameter(ATTR_ORIGIN),
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(ATTR_ORIGIN,
				request.getParameter(ATTR_ORIGIN),
				BookingConstants.MESSAGE_AIRPORT_FROM_NOT_VALID, "isIataAptCode");

		// arrival airport
		validator.addDirectCondition(ATTR_DESTINATION,
				request.getParameter(ATTR_DESTINATION),
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(ATTR_DESTINATION,
				request.getParameter(ATTR_DESTINATION),
				BookingConstants.MESSAGE_AIRPORT_TO_NOT_VALID, "isIataAptCode");
		
		// search type
		validator.addDirectCondition(ATTR_SEARCH_TYPE,
				request.getParameter(ATTR_SEARCH_TYPE),
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		// start date
		String dateFormat = AlitaliaUtils.getDateFormatByMarket(request.getResource());
		String addParamDate = "";
		if (dateFormat != null) {
			addParamDate = "|" + dateFormat;
		}	
		validator.addDirectCondition(ATTR_DEPARTURE_DATE,
				request.getParameter(ATTR_DEPARTURE_DATE),
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");		
		validator.addDirectCondition(ATTR_DEPARTURE_DATE, request.getParameter(ATTR_DEPARTURE_DATE) + addParamDate,
				BookingConstants.MESSAGE_DATE_PAST, "isDate", "isFuture");

		// check if departure airport and arrival airport are different
		validator.addCrossCondition(ATTR_DESTINATION,
				request.getParameter(ATTR_ORIGIN),
				request.getParameter(ATTR_DESTINATION),
				BookingConstants.MESSAGE_FROM_TO_DIFF, "areDifferent");

		// end date
		String end = request.getParameter(ATTR_RETURN_DATE);
		
		// check if start date is before end date
		if (!BookingSearchTypeEnum.isOneWay(request.getParameter(ATTR_SEARCH_TYPE))) {
			validator.addDirectCondition(ATTR_RETURN_DATE, end + addParamDate,
					BookingConstants.MESSAGE_DATE_PAST, "isDate", "isFuture");
			validator.addCrossCondition(ATTR_RETURN_DATE, request.getParameter(ATTR_DEPARTURE_DATE) + addParamDate,
					end + addParamDate, BookingConstants.MESSAGE_DATE_PAST,
					"beforeThen");
		}
		
		// number of adults
		validator.addDirectCondition(ATTR_NADULTS,
				request.getParameter(ATTR_NADULTS),
				BookingConstants.MESSAGE_NADULTS_NOT_VALID, "isRangeAdults");

		// number of kids
		String nkids = request.getParameter(ATTR_NKIDS);
		if (nkids != null && !nkids.isEmpty()) {
			validator.addDirectCondition(ATTR_NKIDS, nkids,
					BookingConstants.MESSAGE_NKINDS_NOT_VALID, "isRangeKids");
		}

		// number of babies
		String nbabies = request.getParameter(ATTR_NBABY);
		if (nbabies != null && !nbabies.isEmpty()) {
			validator.addDirectCondition(ATTR_NBABY, nbabies,
					BookingConstants.MESSAGE_NBABIES_NOT_VALID, "isRangeBabies");
		}
		
		// check sum of adults and kids
		validator.addCrossCondition(ATTR_NADULTS, request.getParameter(ATTR_NADULTS),
				request.getParameter(ATTR_NKIDS),
				BookingConstants.MESSAGE_SUM_ADULTS_KIDS_NOT_VALID,
				"underMaxSumAdultsKids");
		
		// check if number of babies is lower then number of adults
		if (nbabies != null && !nbabies.isEmpty()) {
			validator.addCrossCondition(ATTR_NBABY,
					request.getParameter(ATTR_NADULTS),
					request.getParameter(ATTR_NBABY),
					BookingConstants.MESSAGE_BABY_PER_ADULT_NOT_VALID, "babyPerAdult");
		}
		
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		// prepare search data from received parameters
		String departureInput = request.getParameter(ATTR_ORIGIN);
		String arrivalInput = request.getParameter(ATTR_DESTINATION);
		String departureDate = request.getParameter(ATTR_DEPARTURE_DATE);
		String returnDate = request.getParameter(ATTR_RETURN_DATE);
		
		String currentFormat = AlitaliaUtils.getDateFormatByMarket(request.getResource());
		if (currentFormat == null) {
			currentFormat = STANDARD_REQUEST_DATE_FORMAT;
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat(currentFormat);
		dateFormat.setLenient(false);
		
		// TODO [Booking Award] Implementare corrette casistiche
		CabinEnum filterCabin = CabinEnum.ECONOMY;
		String filterClassFlight = request.getParameter(ATTR_CLASS_OF_FLIGHT);
		if (filterClassFlight != null) {
			if(("business").equals(filterClassFlight)) {
				filterCabin = CabinEnum.BUSINESS;
			}
		}
		
		BookingSearchKindEnum searchKind = BookingSearchKindEnum.SIMPLE;
		List<SearchElement> searchElements = new ArrayList<SearchElement>();
		SearchPassengersNumber searchPassengersNumber = new SearchPassengersNumber();
		String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		String site = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
		
		SearchElementAirport fromElement = new SearchElementAirport(departureInput);
		SearchElementAirport toElement = new SearchElementAirport(arrivalInput);
		
		searchElements.add(new SearchElement(
				fromElement, toElement, TypeUtils.createCalendar(dateFormat.parse(departureDate))));
		
		if (returnDate != null && !"".equals(returnDate)) {
			searchElements.add(new SearchElement(
					toElement, fromElement, TypeUtils.createCalendar(dateFormat.parse(returnDate))));
			searchKind = BookingSearchKindEnum.ROUNDTRIP;
		}
		
		
		searchPassengersNumber.setNumAdults(Integer.parseInt(request.getParameter(ATTR_NADULTS)));
		searchPassengersNumber.setNumChildren(Integer.parseInt(request.getParameter(ATTR_NKIDS)));
		
		
		// create a new booking session context and save it in the session
		HttpSession session = request.getSession(true);
		String clientIp = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
		String sessionId = session.getId();
		
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		
		BookingSessionContext ctx = bookingAwardSession.initSession(clientIp, sessionId, market, site, locale, request);
		session.setAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE, ctx);
		
		// prepare the search data in the booking session
		InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(request.getResource());
		String decimalSeparator=ivm.getInherited("decimalSeparator", String.class);
		String groupingSeparator=ivm.getInherited("groupingSeparator", String.class);
		Integer numDecDigits=ivm.getInherited("decimalDigit", Integer.class);
		NumberFormat currentNumberFormat = AlitaliaUtils.getNumberFormat(numDecDigits, decimalSeparator, groupingSeparator, market);
		bookingAwardSession.prepareSearch(ctx, i18n, searchKind, currentNumberFormat, searchElements,  searchPassengersNumber, filterCabin);
		
		/*moved here from BookingAwardCalendarSearchServlet*/
		bookingAwardSession.performCalendarSearch(ctx);
		
		// redirect to the correct page
		String successPage = configuration.getBookingAwardCalendarPage();
		String noSolutionsPage = configuration.getBookingAwardFlightSearchNoSolutionsPage();
		String redirectPage = successPage;
		
		/*No solutions case*/
		if (ctx.availableFlights == null || ctx.availableFlights.getRoutes() == null || ctx.availableFlights.getRoutes().isEmpty()
				|| ctx.availableFlights.getRoutes().stream()
				.anyMatch(route -> route.getFlights() == null || route.getFlights().isEmpty())) {
			redirectPage = noSolutionsPage;
		} 
		
		if (redirectPage != null && !"".equals(redirectPage)) {
			Resource resource = request.getResource();
			String redirectUrl = resource.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(resource, false)) + redirectPage;
			response.sendRedirect(redirectUrl);
		}
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}
