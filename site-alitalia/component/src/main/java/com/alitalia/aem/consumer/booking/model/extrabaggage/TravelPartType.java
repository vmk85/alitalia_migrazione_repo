package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Enumerazione per il tipo di viaggio. 
   ['SEGMENT', 'ITINERARY_PART', 'ITINERARY']
*/
public enum TravelPartType
{
	SEGMENT,
	ITINERARY_PART,
	ITINERARY;

	public static final int SIZE = java.lang.Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static TravelPartType forValue(int value)
	{
		return values()[value];
	}
}