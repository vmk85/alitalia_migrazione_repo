package com.alitalia.aem.consumer.model.content;
import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import javax.servlet.http.Cookie;


@Model(adaptables = { SlingHttpServletRequest.class })
public class EmptyModel extends GenericBaseModel{
	
		@Self
		private SlingHttpServletRequest slingHttpServletRequest;
	    private boolean isLog=false;
	    private String baseUrl;
	    private String newsHPUrl;
	    private String timeStamp="";

	    private Cookie loginCookie;

		@PostConstruct
		protected void initModel() {
			super.initBaseModel(slingHttpServletRequest);
			
			if (AlitaliaUtils.getAuthenticatedUser(slingHttpServletRequest) != null) {
				isLog = true;
			} else {
				isLog = false;
			}
			
			baseUrl = request.getResource().getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), true));
			String newsPage = "/news/jcr:content";
			newsHPUrl = AlitaliaUtils.findSiteBaseExternalUrl(
					slingHttpServletRequest.getResource(), false)+AlitaliaConstants.BASE_CONFIG_PATH+newsPage ;
			String tmStamp=request.getParameter("time");
			if(tmStamp!=null || !("").equals(tmStamp)){
				timeStamp=tmStamp;
				
			}
			loginCookie = request.getCookie("login-token");
		}
		
	   public boolean getIsLog(){
		   return isLog;
	   }
	   
	   public String getBaseUrl(){
		   return baseUrl;
	   }
		
	   public String getTimeStamp(){
		 
		   return timeStamp;
	   }
	   
	   public String getNewsHPUrl() {
			return newsHPUrl;
	   }
	   
}
