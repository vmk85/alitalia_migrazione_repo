package com.alitalia.aem.consumer.booking;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alitalia.aem.common.data.home.*;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.engine.SlingRequestProcessor;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.carnet.CarnetInfoCarnet;
import com.alitalia.aem.common.data.home.enumerations.ARTaxInfoTypesEnum;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.AvailabilitySeatEnum;
import com.alitalia.aem.common.data.home.enumerations.BancoPostaTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.DiscountTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.FareTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GatewayTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GlobalCollectPaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMTierCodeEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentStepEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.ResidencyTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.ResultTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.StatisticsDataTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.TimeTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.TypeSeatEnum;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.messages.home.AuthorizePaymentRequest;
import com.alitalia.aem.common.messages.home.AuthorizePaymentResponse;
import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailRequest;
import com.alitalia.aem.common.messages.home.CarnetSendNotificationEmailResponse;
import com.alitalia.aem.common.messages.home.CarnetUpdateRequest;
import com.alitalia.aem.common.messages.home.CarnetUpdateResponse;
import com.alitalia.aem.common.messages.home.CashAndMilesData;
import com.alitalia.aem.common.messages.home.CheckPaymentRequest;
import com.alitalia.aem.common.messages.home.CheckPaymentResponse;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerCreatePNRResponse;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerRequest;
import com.alitalia.aem.common.messages.home.InfoPassengerSubmitPassengerResponse;
import com.alitalia.aem.common.messages.home.InitializePaymentRequest;
import com.alitalia.aem.common.messages.home.InitializePaymentResponse;
import com.alitalia.aem.common.messages.home.InsuranceRequest;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponRequest;
import com.alitalia.aem.common.messages.home.LoadWidgetECouponResponse;
import com.alitalia.aem.common.messages.home.LoadWidgetOneClickRequest;
import com.alitalia.aem.common.messages.home.ProfileRequest;
import com.alitalia.aem.common.messages.home.ProfileResponse;
import com.alitalia.aem.common.messages.home.RegisterBillingStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterECouponStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterFlightSearchStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterPNRStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterPaymentAuthAttemptStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterPaymentBookingEtktStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterPaymentBookingInsuranceStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterPaymentFromAuthToTicketStatisticRequest;
import com.alitalia.aem.common.messages.home.RegisterStatisticsRequest;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightMileageRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightMileageResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightSeatMapResponse;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlyersResponse;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.common.messages.home.RetrieveStateListRequest;
import com.alitalia.aem.common.messages.home.RetrieveStateListResponse;
import com.alitalia.aem.common.messages.home.RetrieveTicketRequest;
import com.alitalia.aem.common.messages.home.RetrieveTicketResponse;
import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesRequest;
import com.alitalia.aem.common.messages.home.UpdateWidgetCashMilesResponse;
import com.alitalia.aem.common.messages.home.enumeration.CashMilesOperationType;
import com.alitalia.aem.common.messages.home.enumeration.SearchExecuteResponseType;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.consumer.booking.model.ContactType;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.model.InvoiceType;
import com.alitalia.aem.consumer.booking.model.Passenger;
import com.alitalia.aem.consumer.booking.model.PassengerType;
import com.alitalia.aem.consumer.booking.model.PassengersData;
import com.alitalia.aem.consumer.booking.model.PaymentType;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.model.SearchElementAirport;
import com.alitalia.aem.consumer.booking.model.SearchPassengersNumber;
import com.alitalia.aem.consumer.booking.model.Sex;
import com.alitalia.aem.consumer.booking.render.GenericPriceRender;
import com.alitalia.aem.consumer.carnet.CarnetSession;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.CreditCardUtils;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.alitalia.aem.web.component.bookingancillary.delegate.BookingAncillaryDelegate;
import com.alitalia.aem.web.component.bookingticket.delegate.BookingTicketDelegate;
import com.alitalia.aem.web.component.carnet.delegate.CarnetDelegate;
import com.alitalia.aem.web.component.common.delegate.CommonDelegate;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.alitalia.aem.web.component.infopassenger.delegate.InfoPassengerDelegate;
import com.alitalia.aem.web.component.payment.delegate.PaymentDelegate;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.alitalia.aem.web.component.statistics.delegate.RegisterStatisticsDelegate;
import com.alitalia.aem.web.component.widget.delegate.BookingWidgetDelegate;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMMode;

/**
 * The <code>BookingSession</code> component tracks the
 * advancement of a booking session and the related data,
 * through specialized methods.
 * 
 * <p>Since the SCR component is stateless, the state of the
 * single booking session is tracked in an external object
 * <code>BookingSessionContext</code>.</p>
 * 
 * @see BookingSessionContext
 */
@Component(immediate = true, metatype = true)
@Properties({
	@Property( name = BookingSession.APIS_COUNTRY_CODES_PROPERTY, label = "Country codes for Apis flights, as a comma-separated list." ),
	@Property( name = BookingSession.SECUREFLIGHT_COUNTRY_CODES_PROPERTY, label = "Country codes for SecureFlight flights, as a comma-separated list." ),
	@Property( name = BookingSession.SECUREFLIGHT_ESTA_COUNTRY_CODES_PROPERTY, label = "Country codes for SecureFlight-ESTA flights, as a comma-separated list." ),
	@Property( name = BookingSession.CONTINUITY_SARDINIA_DATE_FROM, label = "Date when the Territorial continuity for Sardinia is starting - (dd/mm)" ),
	@Property( name = BookingSession.CONTINUITY_SARDINIA_DATE_TO, label = "Date when the Territorial continuity for Sardinia is finishing (dd/mm)" )
})
@Service(value = BookingSession.class)
public class BookingSession {

	public static final String CONTINUITY_SARDINIA_OLBIA_DATE_FROM = "continuity.sardinia.date.from";
	public static final String CONTINUITY_SARDINIA_OLBIA_DATE_TO = "continuity.sardinia.date.to";
	public static final String CONTINUITY_SARDINIA_DATE_FROM = "continuity.sardinia.new.date.from";
	public static final String CONTINUITY_SARDINIA_DATE_TO = "continuity.sardinia.new.date.to";
	public static final String APIS_COUNTRY_CODES_PROPERTY = "apis.country.codes";
	public static final String SECUREFLIGHT_COUNTRY_CODES_PROPERTY = "secureflight.country.codes";
	public static final String SECUREFLIGHT_ESTA_COUNTRY_CODES_PROPERTY = "secureflight_esta.country.codes";
	public static final String PREFERENCE_MEAL = "002";
	private static final int INDEX_START_RIBBON_DATE = 6;
	public static final String[] MEALS_AZ_CARRIER = {"AZ", "VE", "A6", "XM", "AP", "CT", "7M"};
	public static final Integer LB_FLIGHT_NUMBER_3 = 3000;
	public static final Integer UB_FLIGHT_NUMBER_3 = 3999;
	public static final Integer LB_FLIGHT_NUMBER_5 = 5000;
	public static final Integer UB_FLIGHT_NUMBER_5 = 5999;
	public static final Integer LB_FLIGHT_NUMBER_7 = 7000;
	public static final Integer UB_FLIGHT_NUMBER_7 = 7999;
	public static final int FLIGHT_MINIMAL_DURATION_FOR_MEALS = 3;
	/* static */

	private static Logger logger = LoggerFactory.getLogger(BookingSession.class);


	/* component references */

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegate staticDataDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile SearchFlightsDelegate searchFlightsDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile CommonDelegate commonDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile InfoPassengerDelegate infoPassengerDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile PaymentDelegate paymentDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BookingTicketDelegate ticketDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BookingWidgetDelegate loadWidgetDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BookingAncillaryDelegate ancillaryDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile RegisterStatisticsDelegate registerStatisticsDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile CarnetDelegate carnetDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile PaymentEmailGenerator paymentEmailGenerator;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaConfigurationHolder configuration;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaCustomerProfileManager customerProfileManager;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BusinessLoginService businessLoginService;

	@Reference
	private CarnetSession carnetSession;

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private RequestResponseFactory requestResponseFactory;

	@Reference
	private SlingRequestProcessor requestProcessor;

	private ComponentContext componentContext;


	/* constructor */

	public BookingSession() {
		super();
	}


	/* SCR lifecycle methods */

	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}


	/* management methods (common) */

	/**
	 * Initialize the booking session object, setting it ready to 
	 * start a new booking process (any existing state is discarded).
	 * 
	 * This must be called when a new booking process is started.
	 * 
	 * @param sid The session identifier, used for tracking purposes.
	 * @param clientIP 
	 * @param sessionId 
	 */
	public BookingSessionContext initializeBookingSession(String sid, String clientIP, 
			String sessionId) {
		logger.debug("initializeBookingSession");
		BookingSessionContext ctx = new BookingSessionContext();
		ctx.sid = sid;
		ctx.phase = BookingPhaseEnum.INITIAL;
		ctx.numberOfMoreFlights = 10;
		ctx.initialNumberOfFlightsToShow = 10;
		ctx.profileData = new ArrayList<MMCustomerProfileData>();
		ctx.clientIP = clientIP;
		ctx.sessionId = sessionId;
		ctx.ccFeeApplied = false;
		ctx.ccFeeTotalAmount = new BigDecimal(0);
		return ctx;
	}

	/**
	 * Initialize the booking session for a carnetSearch
	 * @param string
	 * @param clientIP
	 * @param sessionId
	 */
	public BookingSessionContext initializeBookingSessionWithCarnet(String sid,
			String clientIP, String sessionId, CarnetSessionContext carnetSessionContext,
			Map<String,String> configurationFareRules, int nPax) {

		BookingSessionContext ctx = 
				initializeBookingSession(sid, clientIP, sessionId);

		CarnetInfoCarnet infoCarnet = carnetSession.getInfoUserLogged(carnetSessionContext); 
		if (infoCarnet != null) {
			boolean isValid = carnetSession.checkCarnetValidity(ctx,infoCarnet,nPax);
			if(isValid){
				ctx.isCarnetProcess = true;
				ctx.carnetFareRules = configurationFareRules;
				ctx.isCarnetValid = true;
			} else{
				ctx.isCarnetValid = false;
			}
		} else {
			ctx.isCarnetProcess = false;
			ctx.isCarnetValid = false;
		}
		return ctx;

	}

	/**
	 * This is the first step to be performed just after the
	 * initialize, receiving the initial search data (origin
	 * and destination, dates and number of passengers) and
	 * preparing the search data.
	 * 
	 * <p>The only required field in the SearchAirportData used in
	 * the SearchElement elements is the airport code, all the
	 * other attributes will be automatically filled up
	 * by this method.</p>
	 * 
	 * <p>The <code>searchElements</code> argument should contain:
	 * <ul>
	 * <li>exactly one element when <code>searchKind</code> is
	 * <code>BookingSearchKindEnum.SIMPLE</code></li>
	 * <li>exactly two elements when <code>searchKind</code> is
	 * <code>BookingSearchKindEnum.ROUNDTRIP</code></li>
	 * <li>one or more elements when <code>searchKind</code> is
	 * <code>BookingSearchKindEnum.MULTILEG</code></li>
	 * </ul>
	 * </p>
	 * 
	 * <p>Note that this method does not actually execute
	 * the search, it just prepares the relevant info
	 * in the context object:
	 * <ul>
	 * <li><code>searchKind</code></li>
	 * <li><code>searchElements</code></li>
	 * <li><code>searchPassengersNumber</code></li>
	 * <li><code>cug</code></li>
	 * <li><code>market</code></li>
	 * <li><code>locale</code></li>
	 * <li><code>isAllowedContinuitaTerritoriale</code></li>
	 * </ul>
	 * </p>
	 * 
	 * @param ctx The booking session context to use.
	 * @param request request
	 * @param searchKind The kind of search.
	 * @param searchElements The search elements.
	 * @param searchPassengersNumber The number of passengers for each kind.
	 * @param cug Type of search for special user groups.
	 * @param market The market code to be used when requested by remote services.
	 * @param site The site code to be used when requested by remote services.
	 * @param locale The locale to use for the session.
	 * @param tradeAgencyCode The code of the trade agency, only for business.
	 * @param tradeAgencyContractCode The contract code of the trade agency, only for business.
	 * @param filterCabin 
	 */
	public void prepareSearch(BookingSessionContext ctx, I18n i18n,
			BookingSearchKindEnum searchKind, List<SearchElement> searchElements,
			SearchPassengersNumber searchPassengersNumber, BookingSearchCUGEnum cug,
			String market, String site, Locale locale, NumberFormat currentNumberFormat,
			String currency, String tradeAgencyCode, String tradeAgencyContractCode, CabinEnum filterCabin, 
			boolean isGiovaniLastminute, boolean showRoundTrip) {

		logger.debug("performInitialSearch");
		if (ctx.phase != BookingPhaseEnum.INITIAL) {
			logger.error("performInitialSearch not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}

		String tid = IDFactory.getTid();

		// verify airport codes and retrieve airports data
		RetrieveAirportsRequest retrieveAirportsRequest = 
				new RetrieveAirportsRequest(tid, ctx.sid);
		retrieveAirportsRequest.setMarket(market);
		retrieveAirportsRequest.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()).toUpperCase());
		RetrieveAirportsResponse retrieveAirportsResponse = 
				staticDataDelegate.retrieveAirports(retrieveAirportsRequest);
		List<AirportData> listAirports = retrieveAirportsResponse.getAirports();
		Map<String, AirportData> searchAirportMap = new HashMap<String, AirportData>();
		for (AirportData airport : listAirports) {
			for (SearchElement searchElement : searchElements) {

			//Controllo se nella tratta è presente "LIN", "MXP" o "BGY" e se la data rientra nel periodo chiusura aeroporto Linate.
			//In tal caso, nella request passiamo, invece, "MIL"
			if (("LIN".equals(searchElement.getFrom().getAirportCode()) || "MXP".equals(searchElement.getFrom().getAirportCode()) || "BGY".equals(searchElement.getFrom().getAirportCode())) && (isLinAirportClosed(searchElements))){
				if (airport.getCode().equals("MIL")) {
					searchAirportMap.put(airport.getCode(), airport);
					}
				} else {
					if (airport.getCode().equals(searchElement.getFrom().getAirportCode())) {
						searchAirportMap.put(airport.getCode(), airport);
					}
				}

				if (("LIN".equals(searchElement.getTo().getAirportCode()) || "MXP".equals(searchElement.getTo().getAirportCode()) || "BGY".equals(searchElement.getTo().getAirportCode())) && (isLinAirportClosed(searchElements))){
				if (airport.getCode().equals("MIL")) {
					searchAirportMap.put(airport.getCode(), airport);
					}
				} else {
					if (airport.getCode().equals(searchElement.getTo().getAirportCode())) {
						searchAirportMap.put(airport.getCode(), airport);
					}
				}
			}
		}

		for (SearchElement searchElement : searchElements) {
			for (SearchElementAirport element : 
				new SearchElementAirport[] { searchElement.getFrom(), searchElement.getTo() }) {

				//Controllo se nella tratta è presente "LIN", "MXP" o "BGY" e se la data rientra nel periodo chiusura aeroporto Linate.
				//In tal caso, nella request passiamo, invece, "MIL"
				if (element.getAirportCode().equals("LIN") && isLinAirportClosed(searchElements)) {
					element.setAirportCode(element.getAirportCode().replace("LIN","MIL"));
				}
				if (element.getAirportCode().equals("MXP") && isLinAirportClosed(searchElements)) {
					element.setAirportCode(element.getAirportCode().replace("MXP","MIL"));
				}
				if (element.getAirportCode().equals("BGY") && isLinAirportClosed(searchElements)) {
					element.setAirportCode(element.getAirportCode().replace("BGY","MIL"));
				}

				AirportData airport = searchAirportMap.get(element.getAirportCode());
				if (airport == null) {
					logger.error("Cannot find detail for airport code {}.", element.getAirportCode());
					throw new IllegalArgumentException("Cannot find detail for airport code.");
				} else {
					element.setAirportName(airport.getName());
					element.setLocalizedAirportName(i18n.get("airportsData." + element.getAirportCode() + ".name"));
					element.setCityCode(airport.getCityCode());
					element.setCityName(airport.getCity());
					element.setLocalizedCityName(i18n.get("airportsData." + element.getAirportCode() + ".city"));
					element.setCountryCode(airport.getCountryCode());
					element.setCountryName(airport.getCountry());
					element.setLocalizedCountryName(i18n.get("airportsData." + element.getCountryCode() + ".country"));
				}
			}
		}

		ctx.newSearch = true;
		ctx.searchKind = searchKind;
		ctx.searchElements = searchElements;
		ctx.searchPassengersNumber = searchPassengersNumber;
		ctx.flightSelections = new FlightSelection[ctx.searchElements.size()];
		ctx.numberOfShownFlights = new int[ctx.searchElements.size()];
		ctx.selectedDepartureDateChoices = new int[ctx.searchElements.size()];
		for (int i = 0; i < ctx.searchElements.size(); i++) {
			ctx.selectedDepartureDateChoices[i] = INDEX_START_RIBBON_DATE;
		}
		ctx.isGiovaniLastminute = isGiovaniLastminute;
		ctx.showRoundTrip = showRoundTrip;
		ctx.cug = cug;
		if ("en".equalsIgnoreCase(market) && "en".equals(locale.getLanguage())) {
			// mercato internazionale per Sabre
			ctx.market = "FI";
			ctx.site = "FI";
		} else {
			ctx.market = "gb".equalsIgnoreCase(market) || "en".equalsIgnoreCase(market) ? market.toUpperCase() : market;
			ctx.site = site;
		}
		//new Locale.Builder().setLanguage("es").build();
		ctx.locale = locale;
		ctx.currentNumberFormat = currentNumberFormat;
		ctx.currency = currency;
		ctx.tradeAgencyCode = tradeAgencyCode;
		ctx.tradeAgencyContractCode = tradeAgencyContractCode;
		ctx.filterCabin = filterCabin;
		ctx.isAllowedContinuitaTerritoriale = Boolean.FALSE;
		ctx.residency = null;
		ctx.youthSolutionFound = null;
		ctx.familySolutionFound = null;
		ctx.militarySolutionFound = null;
		ctx.solutionForSelectedDate = new Boolean[ctx.searchElements.size()];
		ctx.readyToPassengersDataPhase = false;
		ctx.insuranceProposalData = null;
		ctx.insuranceAmount = new BigDecimal(0);
		ctx.isInsuranceApplied = false;
		for (int i=0; i<ctx.solutionForSelectedDate.length; i++) {
			ctx.solutionForSelectedDate[i] = false;
		}
		if (searchKind != BookingSearchKindEnum.MULTILEG && ctx.cug != BookingSearchCUGEnum.YOUTH && ctx.cug != BookingSearchCUGEnum.FAMILY && ctx.cug != BookingSearchCUGEnum.MILITARY) {
			for (SearchElement element : searchElements) {

				if (searchFlightsDelegate.isSliceSardiniaTerritorialContinuityOlbia(element.getFrom().getAirportCode(), element.getTo().getAirportCode()) &&
						checkContinuitaTerritorialeOlbiaSardiniaPeriod(searchElements, ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP)) {
					ctx.isAllowedContinuitaTerritoriale = Boolean.TRUE;
					ctx.residency = ResidencyTypeEnum.SARDINIA;
					break;
				}

				if (searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(element.getFrom().getAirportCode(), element.getTo().getAirportCode()) &&
						checkContinuitaTerritorialeSardiniaPeriod(searchElements, ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP)) {
					ctx.isAllowedContinuitaTerritoriale = Boolean.TRUE;
					ctx.residency = ResidencyTypeEnum.SARDINIA;
					break;
				}

				if (searchFlightsDelegate.isSliceSicilyTerritorialContinuity(element.getFrom().getAirportCode(), element.getTo().getAirportCode())){
					ctx.isAllowedContinuitaTerritoriale = Boolean.TRUE;
					ctx.residency = ResidencyTypeEnum.SICILY;
					break;
				}
			}
		} else {
			ctx.currentSliceIndex = 1;
		}

		ctx.phase = BookingPhaseEnum.FLIGHTS_SEARCH;
		ctx.i18n = i18n;
	}

	/**
	 * Set the selection about the "continuita' territoriale" choice.
	 * 
	 * <p>This method does not perform any service activity, it just
	 * save the selection in the <code>isSelectedContinuitaTerritoriale</code>
	 * context object variable.</p>
	 * 
	 * @param ctx The booking session context to use.
	 * @param choice The provided choice.
	 */
	public void selectContinuitaTerritoriale(BookingSessionContext ctx, Boolean choice) {
		logger.debug("selectContinuitaTerritoriale");
		if (ctx.phase != BookingPhaseEnum.FLIGHTS_SEARCH) {
			logger.error("selectContinuitaTerritoriale not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}

		if (choice) {
			ctx.isSelectedContinuitaTerritoriale = true;
			ctx.residency = ResidencyTypeEnum.SARDINIA; // sembra necessario in ogni caso ResidencyTypeEnum.SARDINIA
		} else {
			ctx.isSelectedContinuitaTerritoriale = false;
			ctx.residency = ResidencyTypeEnum.NONE;
		}
	}


	/**
	 * Perform the search with the current search data.
	 * 
	 * <p>The result of the search are saved setting (or replacing)
	 * the values of several context variables:
	 * <ul>
	 * <li><code>availableFlights</code></li>
	 * <li><code>extraChargePassengerList</code></li>
	 * <li><code>flightSelections (only initialization)</code></li>
	 * <li><code>selectedDepartureDateChoices</code></li>
	 * </ul>
	 * </p>
	 * 
	 * <p>Moreover, the current flight selections, if any, are cleared by
	 * resetting the value of the <code>flightSelections</code> context
	 * variable.</p>
	 * 
	 * @param ctx The booking session context to use.
	 * @param rootResource 
	 */
	public void performSearch(BookingSessionContext ctx, Resource rootResource) {
		logger.debug("performSearch");

		String tid = IDFactory.getTid();
		ctx.isCouponValid = null;
		ctx.resultBookingDetails = null;
		ctx.fullTextFareRules = null;

		if (ctx.isGiovaniLastminute) {
			ctx.cug = BookingSearchCUGEnum.YOUTH;
		}

		if (ctx.searchKind == BookingSearchKindEnum.SIMPLE 
				|| ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP) {

			if (ctx.cug == BookingSearchCUGEnum.ADT 
					|| ctx.cug == BookingSearchCUGEnum.FAMILY
					|| ctx.cug == BookingSearchCUGEnum.MILITARY
					|| ctx.cug == BookingSearchCUGEnum.YOUTH) {

				SearchFlightSolutionRequest brandRequest = createFlightBrandSearchRequest(tid, ctx);

				SearchFlightSolutionResponse searchResponse = searchFlightsDelegate.searchInitialSolutions(brandRequest);
				ctx.cookie = searchResponse.getCookie();
				ctx.execution = searchResponse.getExecute();
				ctx.sabreGateWayAuthToken = searchResponse.getSabreGateWayAuthToken();
				ctx.availableFlights = searchResponse.getAvailableFlights();
				ctx.extraChargePassengerList = searchResponse.getExtraChargePassengerList();
				ctx.isBusCarrier = searchResponse.isBusCarrier();
				ctx.territorialContinuitySolutionFound = searchResponse.isTerritorialContinuitySolutionsFoundOtherDates();

				if (ctx.availableFlights != null) {
					RegisterFlightSearchStatisticRequest flightSearchStatisticRequest = new RegisterFlightSearchStatisticRequest(tid,ctx.sid);
					flightSearchStatisticRequest.setSearchType(SearchTypeEnum.BRAND_SEARCH);
					flightSearchStatisticRequest.setSearch(brandRequest.getFilter());
					flightSearchStatisticRequest.setUserId(ctx.loggedUserProfileData!=null ? ctx.loggedUserProfileData.getCustomerNumber() : null);
					flightSearchStatisticRequest.setClientIP(ctx.clientIP);
					flightSearchStatisticRequest.setSessionId(ctx.sessionId);
					flightSearchStatisticRequest.setSiteCode(ctx.site);

					flightSearchStatisticRequest.setSuccess(true);
					flightSearchStatisticRequest.setType(StatisticsDataTypeEnum.AVAIL);

					try {
						logger.debug("Performing avail statistic submission. Request: ["+flightSearchStatisticRequest+"]");
						RegisterStatisticsResponse statisticResponse = registerStatisticsDelegate.registerStatistics(flightSearchStatisticRequest);
						if (statisticResponse == null) {
							logger.error("Error performing avail statistic");
						}
					} catch(Exception e) {
						logger.error("Error performing avail statistic", e);
					}
				}

				//check if the youth/family solutions was returned
				if (ctx.cug == BookingSearchCUGEnum.FAMILY
						|| ctx.cug == BookingSearchCUGEnum.YOUTH
						|| ctx.cug == BookingSearchCUGEnum.MILITARY) {
					ctx.familySolutionFound = false;
					ctx.youthSolutionFound = false;
					ctx.militarySolutionFound = false;
					String cugBrandCode = ctx.cug == BookingSearchCUGEnum.YOUTH ? "ZZ" : (ctx.cug == BookingSearchCUGEnum.FAMILY ? "FA" : "YC");
					int count = 0;
					for ( RouteData routeData : ctx.availableFlights.getRoutes() ) {
						FlightData flightData = routeData.getFlights().get(0);
						BrandData brandData = flightData.getBrands().get(0);
						if (flightData.getBrands().size() == 1 
								&& brandData.getCode().equalsIgnoreCase(cugBrandCode) ) {
							count++;
						}
					}
					if (count == ctx.searchElements.size()) {
						if(ctx.cug == BookingSearchCUGEnum.YOUTH){
							ctx.youthSolutionFound = true;
						}else if(ctx.cug == BookingSearchCUGEnum.FAMILY){
							ctx.familySolutionFound = true;
						}else{
							ctx.militarySolutionFound = true;
						}
					}
				}
			}


			//check if the tab corresponding to selected departure Date in the Ribbon
			//has solution
			if (!ctx.availableFlights.getRoutes().isEmpty()) {
				for (TabData tabData : ctx.availableFlights.getTabs()) {
					int middleIndex = ((tabData.getFlightTabs().size() -1) / 2 );
					if (tabData.getType() == RouteTypeEnum.OUTBOUND) {
						if (tabData.getFlightTabs().get(middleIndex).getCurrency() != null) {
							ctx.solutionForSelectedDate[0] = true;
						}
					}
					if (tabData.getType() == RouteTypeEnum.RETURN) {
						if (tabData.getFlightTabs().get(middleIndex).getCurrency() != null) {
							ctx.solutionForSelectedDate[1] = true;
						}
					}
				}
			}

			//check all tabs to found solutions
			int cont = 0;
			if (!ctx.availableFlights.getRoutes().isEmpty()) {
				if (ctx.availableFlights.getTabs() == null || ctx.availableFlights.getTabs().isEmpty()) {
					if (ctx.searchElements != null) {
						logger.error("Empty or null Ribbon - Search Details: {}", ctx.searchElements.toString());
					} else {
						logger.error("Empty or null Ribbon - Search Details: not found");
					}
					throw new IllegalStateException("Empty or null Ribbon");
				}

				for (TabData tabData : ctx.availableFlights.getTabs()) {
					cont = 0;
					for (FlightTabData tab : tabData.getFlightTabs()) {
						if(tab.getCurrency() == null) {
							cont++;
						}
					}
					if (cont == tabData.getFlightTabs().size()){
						break;
					}
				}
				if (cont == ctx.availableFlights.getTabs().get(0).getFlightTabs().size()) {
					if (ctx.cug == BookingSearchCUGEnum.MILITARY) {
						ctx.militarySolutionFound = false;
					}
					if (ctx.cug == BookingSearchCUGEnum.FAMILY) {
						ctx.familySolutionFound = false;
					}
				}
			}
			if (!ctx.availableFlights.getRoutes().isEmpty()) {
				if (ctx.availableFlights.getTabs() == null || ctx.availableFlights.getTabs().isEmpty()) {
					if (ctx.searchElements != null) {
						logger.error("Empty or null Ribbon - Search Details: {}", ctx.searchElements.toString());
					} else {
						logger.error("Empty or null Ribbon - Search Details: not found");
					}
					throw new IllegalStateException("Empty or null Ribbon");
				}
			}

		} else { // ctx.searchKind == BookingSearchKindEnum.MULTILEG
			SearchFlightSolutionResponse searchResponse = 
					searchFlightsDelegate.searchMultipleLegsSolutions(createMultilegSearchRequest(tid, ctx));
			ctx.availableFlights = searchResponse.getAvailableFlights();
			ctx.extraChargePassengerList = searchResponse.getExtraChargePassengerList();
			ctx.solutionForSelectedDate = null;
			ctx.isBusCarrier = searchResponse.isBusCarrier();
			ctx.cookie = searchResponse.getCookie();
			ctx.execution = searchResponse.getExecute();
			ctx.sabreGateWayAuthToken = searchResponse.getSabreGateWayAuthToken();
		}
		ctx.searchCategory = computeSearchCategory(ctx);
		logger.info("The search category is {}", ctx.searchCategory != null ? ctx.searchCategory.getValue() : "null");
		ctx.totalExtraCharges = new BigDecimal(0);

		ctx.totalSlices = computeTotalSlices(ctx);
		ctx.isRefreshed = false;
		ctx.phase = BookingPhaseEnum.FLIGHTS_SELECTION;
		ctx.newSearch = false;
		ctx.brandMap = computeBrandMap(rootResource);
		ctx.codeBrandMap = computeCodeBrandMap(ctx);
	}

	public void performCarnetSearch(BookingSessionContext ctx, Resource rootResource) {
		String tid = IDFactory.getTid();
		ctx.isCouponValid = null;
		ctx.resultBookingDetails = null;

		if (ctx.cug == BookingSearchCUGEnum.CARNET) {
			SearchFlightSolutionResponse searchResponse = 
					searchFlightsDelegate.searchInitialSolutions(createFlightCarnetSearchRequest(tid, ctx));
			ctx.cookie = searchResponse.getCookie();
			ctx.execution = searchResponse.getExecute();
			ctx.sabreGateWayAuthToken = searchResponse.getSabreGateWayAuthToken();
			ctx.availableFlights = searchResponse.getAvailableFlights();
			ctx.extraChargePassengerList = searchResponse.getExtraChargePassengerList();
			ctx.isBusCarrier = searchResponse.isBusCarrier();
		}

		ctx.searchCategory = computeSearchCategory(ctx);
		logger.info("The search category is {}", ctx.searchCategory != null ? ctx.searchCategory.getValue() : "null");
		ctx.totalExtraCharges = new BigDecimal(0);

		ctx.totalSlices = computeTotalSlices(ctx);
		ctx.isRefreshed = false;
		ctx.phase = BookingPhaseEnum.FLIGHTS_SELECTION;
		ctx.newSearch = false;
		ctx.brandMap = computeBrandMap(rootResource);
		ctx.codeBrandMap = computeCodeBrandMap(ctx);
	}
	private Map<String, BrandPageData> computeBrandMap(Resource rootResource) {
		HashMap<String, BrandPageData> pageMap = new HashMap<String, BrandPageData>();
		if (rootResource == null) {
			logger.error("Brand Directory does not exist");
			throw new IllegalStateException("Brand Directory does not exist");
		}
		if (rootResource.adaptTo(Page.class).listChildren() == null 
				|| !rootResource.adaptTo(Page.class).listChildren().hasNext()) {
			logger.error("Brand pages do not exist");
			throw new IllegalStateException("Brand pages do not exist");
		}
		Iterator<Page> iterator = rootResource.adaptTo(Page.class).listChildren();
		while (iterator.hasNext()) {
			Page currentPage = iterator.next();
			BrandPageData brandPageData = new BrandPageData();
			brandPageData.setTitle(currentPage.getTitle());
			brandPageData.setPath(currentPage.getPath());
			brandPageData.setId(currentPage.getName());
			pageMap.put(currentPage.getName(), brandPageData);
		}
		return pageMap;
	}

	private Map<String,BrandPageData> computeCodeBrandMap(BookingSessionContext ctx) {

		Map<String,BrandPageData> codeBrandMap = new HashMap<String, BrandPageData>();

		BookingSearchCUGEnum cugForConfiguration = null;

		if ((ctx.cug == BookingSearchCUGEnum.MILITARY && ctx.militarySolutionFound) ||
				(ctx.cug == BookingSearchCUGEnum.FAMILY && ctx.familySolutionFound) ||
				(ctx.cug == BookingSearchCUGEnum.YOUTH && ctx.youthSolutionFound) ||
				ctx.cug == BookingSearchCUGEnum.CARNET) {

			cugForConfiguration = ctx.cug;
		} else {
			cugForConfiguration = BookingSearchCUGEnum.ADT;
		}

		String[] conf = configuration.getCodeBrandMap(cugForConfiguration);

		if (conf == null || conf.length == 0) {
			logger.error("Configuration Error - see AlitaliaConfigurationHolder properties");
			throw new IllegalStateException("Configuration Error - see AlitaliaConfigurationHolder properties");
		}

		if (conf.length > 0) {

			HashMap<String, String> codeBrandMapConf = new HashMap<String, String>();
			for (String entry : conf) {
				String[] res = entry.split(":");
				codeBrandMapConf.put(res[0], res[1]);
			}
			if (ctx.availableFlights.getRoutes() != null && !ctx.availableFlights.getRoutes().isEmpty()) {
				for (BrandData brandData : ctx.availableFlights.getRoutes().get(0).getFlights().get(0).getBrands()) {

					String brandCode = brandData.getCode().toLowerCase();
					String pageId = codeBrandMapConf.get(brandCode);

					BrandPageData page = ctx.brandMap.get(pageId);
					if (page != null) {
						codeBrandMap.put(brandCode, page);
					}
				}
			}
		}
		return codeBrandMap;
	}


	/**
	 * Determine the booking search category
	 * @param ctx The booking session context to use.
	 * @return
	 */
	private BookingSearchCategoryEnum computeSearchCategory(BookingSessionContext ctx) {

		if (ctx.availableFlights == null || ctx.availableFlights.getRoutes() == null || ctx.availableFlights.getRoutes().isEmpty()) {
			logger.info("No solutions found");
			return null;
		}

		FlightData flightData = ctx.availableFlights.getRoutes().get(0).getFlights().get(0);

		AreaValueEnum areaFrom = null;
		AreaValueEnum areaTo = null;

		if (flightData instanceof DirectFlightData) {
			DirectFlightData directFlightData = (DirectFlightData) flightData;
			areaFrom = directFlightData.getFrom().getArea();
			areaTo = directFlightData.getTo().getArea();

		} else {
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
			DirectFlightData first = (DirectFlightData) connectingFlightData.getFlights().get(0);
			DirectFlightData last = (DirectFlightData) connectingFlightData.getFlights().get(connectingFlightData.getFlights().size() - 1);
			areaFrom = first.getFrom().getArea();
			areaTo = last.getTo().getArea();
		}

		boolean economyPromo = false;
		if (flightData.getBrands() != null) {
			for (BrandData brandData : flightData.getBrands()) {
				if (brandData.getCode().equals("YD")) {
					economyPromo = true;
					break;
				}
			}
		}

		boolean economyLight = false;
		if (flightData.getBrands() != null) {
			for (BrandData brandData : flightData.getBrands()) {
				if (brandData.getCode().equals("YL")) {
					economyLight = true;
					break;
				}
			}
		}

		boolean economyPremium = false;
		if (flightData.getBrands() != null) {
			for (BrandData brandData : flightData.getBrands()) {
				if (brandData.getCode().equals("YP")) {
					economyPremium = true;
					break;
				}
			}
		}
		
		boolean economySaver = false;
		if(flightData.getBrands() != null) {
			for (BrandData brandData : flightData.getBrands()) {
				if (brandData.getCode().equals("YS")) {
					economySaver = true;
					break;
				}
			}
		}

		boolean semiFlex = false;
		if(flightData.getBrands() != null) {
			for (BrandData brandData : flightData.getBrands()) {
				if (brandData.getCode().equals("YX")) {
					semiFlex = true;
					break;
				}
			}
		}
		
		String from = ctx.searchElements.get(0).getFrom().getAirportCode();
		String to = ctx.searchElements.get(0).getTo().getAirportCode();

		if ((ctx.cug == BookingSearchCUGEnum.MILITARY && ctx.militarySolutionFound) ||
				(ctx.cug == BookingSearchCUGEnum.FAMILY && ctx.familySolutionFound) ||
				(ctx.cug == BookingSearchCUGEnum.YOUTH && ctx.youthSolutionFound)) {

			return ctx.cug == BookingSearchCUGEnum.MILITARY ? BookingSearchCategoryEnum.MIL : 
				(ctx.cug == BookingSearchCUGEnum.FAMILY ? BookingSearchCategoryEnum.FAM : BookingSearchCategoryEnum.YTH);

		} else if (ctx.cug == BookingSearchCUGEnum.CARNET) {

			return BookingSearchCategoryEnum.CARNET;

		} else {

			if (ctx.isSelectedContinuitaTerritoriale != null && ctx.isSelectedContinuitaTerritoriale &&
					ctx.territorialContinuitySolutionFound) {
				return BookingSearchCategoryEnum.CONTINUITY;

			} else if (ctx.isAllowedContinuitaTerritoriale != null && ctx.isAllowedContinuitaTerritoriale && 
					searchFlightsDelegate.isSliceSicilyTerritorialContinuity(from, to)) {

				// not isSelectedContinuitaTerritoriale but isAllowedContinuitaTerritoriale
				// In this case there is a particolar category for Sicily.
				return BookingSearchCategoryEnum.CONTINUITY_REFUSE_SICILY;

			} else if (ctx.isAllowedContinuitaTerritoriale != null && ctx.isAllowedContinuitaTerritoriale && (
					searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(from, to) || searchFlightsDelegate.isSliceSardiniaTerritorialContinuityOlbia(from, to))) {

				// not isSelectedContinuitaTerritoriale but isAllowedContinuitaTerritoriale
				// In this case there is a particolar category for Sardinia.
				return BookingSearchCategoryEnum.CONTINUITY_REFUSE_SARDINIA;

			} else if ((ctx.isAllowedContinuitaTerritoriale == null || !ctx.isAllowedContinuitaTerritoriale) && (
					searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(from, to) || searchFlightsDelegate.isSliceSardiniaTerritorialContinuityOlbia(from, to))) {

				// This is a route for Sardinia but not isAllowedContinuitaTerritoriale
				// In this case we are outside the period for territorial continuity.
				return BookingSearchCategoryEnum.CONTINUITY_OUT_SARDINIA;

			} else if ((from.equals("FCO") && to.equals("LIN")) || 
					(from.equals("LIN") && to.equals("FCO")) ||
					(from.equals("ROM") && to.equals("LIN")) ||
					(from.equals("LIN") && to.equals("ROM")) ||
					(from.equals("ROM") && to.equals("MIL")) ||
					(from.equals("MIL") && to.equals("ROM")) ||
					(from.equals("MIL") && to.equals("FCO")) ||
					(from.equals("FCO") && to.equals("MIL")) ) {
				return BookingSearchCategoryEnum.FCO_LIN;
			} else {

				if (areaFrom == AreaValueEnum.INTC || areaTo == AreaValueEnum.INTC) {
//					return economyLight ? BookingSearchCategoryEnum.INTC_LIGHT :
//							(economyPromo ?
//							BookingSearchCategoryEnum.INTC_PROMO : BookingSearchCategoryEnum.INTC);
					return semiFlex ?
							(economyLight ? BookingSearchCategoryEnum.INTC_LIGHT_SEMIFLEX : (economyPromo ? BookingSearchCategoryEnum.INTC_PROMO_SEMIFLEX : BookingSearchCategoryEnum.INTC))
							:
							(economyLight ? BookingSearchCategoryEnum.INTC_LIGHT : (economyPromo ? BookingSearchCategoryEnum.INTC_PROMO : BookingSearchCategoryEnum.INTC));
				} else if (areaFrom == AreaValueEnum.INTZ || areaTo == AreaValueEnum.INTZ) {
					if(economySaver){
						return BookingSearchCategoryEnum.INTZ_SAVER;
					}
					
					return economyPromo ?
						(economyPremium ? BookingSearchCategoryEnum.INTZ_PREMIUM : BookingSearchCategoryEnum.INTZ_PROMO) : BookingSearchCategoryEnum.INTZ;
				} else {
					return economyPromo ?
							BookingSearchCategoryEnum.DOM_PROMO : BookingSearchCategoryEnum.DOM;
				}
			}
		}
	}

	public int computeBrandEconomySize(BookingSearchCategoryEnum category) {
		try {
			if (category != null) {
				String[] categorySizes = configuration.getSearchCategoryEconomySize();
				for (String item : categorySizes) {
					String[] values = item.split(":");
					if (category.getValue().equals(values[0])) {
						return Integer.parseInt(values[1]);
					}
				}
			}
		} catch (Exception e) {
			logger.error("economySize property error", e);
		}
		return 0;
	}

	/**
	 * Invoked when the currently selected date for one of the search
	 * elements is changed, selecting another date among the list
	 * of available choices.
	 * 
	 * <p>When this happens, a new search is performed to obtain a refreshed
	 * list of the available flights, and the resulting data is saved
	 * by replacing the content of the context variables:
	 * <ul>
	 * <li><code>availableFlights</code></li>
	 * <li><code>extraChargePassengerList</code></li>
	 * </ul>
	 * </p>
	 * 
	 * <p>Moreover, any selection in the <code>flightSelections</code> of the context
	 * object related to the modified search element is cleared.</p>
	 * 
	 * @param ctx The booking session context to use.
	 * @param searchElementIndex The index of the search element for which the change is requested.
	 * @param dateChoiceIndex The index of the new selected date choice, among the list of available ones.
	 * @param rootResource 
	 */
	public void changeSearchElementDate(BookingSessionContext ctx, int searchElementIndex, int dateChoiceIndex, Resource rootResource) {
		logger.debug("changeSearchElementDate");
		if (ctx.phase != BookingPhaseEnum.FLIGHTS_SELECTION) {
			logger.error("changeSearchElementDate not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}
		ctx.phase = BookingPhaseEnum.FLIGHTS_SEARCH;
		ctx.selectedDepartureDateChoices[searchElementIndex] = dateChoiceIndex;
		ctx.numberOfShownFlights[searchElementIndex] = ctx.initialNumberOfFlightsToShow;

		TabData tab = ctx.availableFlights.getTabs().get(searchElementIndex);
		Calendar departureDate = tab.getFlightTabs().get(ctx.selectedDepartureDateChoices[searchElementIndex]).getDate();
		departureDate.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
		logger.debug("Modified searchElement {} with date {}", searchElementIndex, departureDate);
		ctx.searchElements.get(searchElementIndex).setDepartureDate(departureDate);
		ctx.selectionRoutes = null;
		ctx.isCouponValid = null;
		ctx.cashAndMiles = null;
		ctx.isCashMilesReady = false;
		ctx.isCashMilesApplied = false;
		ctx.cookie = null;
		ctx.execution = null;
		ctx.flightSelections = new FlightSelection[ctx.searchElements.size()];
		performSearch(ctx,rootResource);

		for (int i = 0; i < ctx.searchElements.size(); i++) {
			ctx.selectedDepartureDateChoices[i] = INDEX_START_RIBBON_DATE;
		}

		//manage the previous selection just for roundtrip search
		if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP) {
			int otherSelectionIndex = 0;
			RouteTypeEnum otherRouteType = RouteTypeEnum.OUTBOUND;
			if (searchElementIndex == 0) {
				otherSelectionIndex = 1;
				otherRouteType = RouteTypeEnum.RETURN;
			}
			FlightSelection otherSelectedFlight = ctx.flightSelections[otherSelectionIndex];
			if (otherSelectedFlight != null) {
				String solutionId = "";
				int solutionBrandIndex = -1;
				int solutionFlightIndex = -1; 
				String selectedFlightNumber = null;

				BrandData selectedBrandData = null;
				RouteData otherRouteData = null;
				for (RouteData route : ctx.availableFlights.getRoutes()) {
					if (route.getType() == otherRouteType) {
						otherRouteData = route;
					}
				}
				//find the previous selected flight for other route.
				//If it was found, it is available for the new search more.
				FlightData flightData = otherSelectedFlight.getFlightData();
				if ( flightData.getFlightType() == FlightTypeEnum.CONNECTING) {
					selectedFlightNumber = ((DirectFlightData)((ConnectingFlightData)flightData)
							.getFlights().get(0)).getFlightNumber();
				} else {
					selectedFlightNumber = ((DirectFlightData)flightData).getFlightNumber();
				}
				boolean trovato=false;
				for (FlightData flight : otherRouteData.getFlights()) {
					solutionFlightIndex++;
					String flightNumber = null;
					if ( flight.getFlightType() == FlightTypeEnum.CONNECTING) {
						flightNumber = ((DirectFlightData)((ConnectingFlightData)flight)
								.getFlights().get(0)).getFlightNumber();
					} else {
						flightNumber = ((DirectFlightData)flight).getFlightNumber();
					}
					if (flightNumber.equals(selectedFlightNumber)) {
						trovato = true;
						break;
					}

				}
				if (trovato) {
					//the other selected flight is still available for the current search
					//therefore, it's managing the other selected flight by re-performing the selection
					for(BrandData brandData : otherRouteData.getFlights().get(solutionFlightIndex).getBrands() ) {
						solutionBrandIndex++;
						if ( brandData.getCode().equalsIgnoreCase(
								otherSelectedFlight.getSelectedBrandCode()) ) {
							selectedBrandData = brandData;
							break;
						}
					}

					//we use the solution ID because we have perform a new search a this selected flight will
					//be the new firstSelection
					solutionId = selectedBrandData.getSolutionId();

					performFlightSelection(ctx, otherSelectionIndex, solutionId, solutionFlightIndex, solutionBrandIndex);
				} else {
					//the other selected flight is not available for the current search more
					//therefore we flush the previous selection
					ctx.numberOfShownFlights[otherSelectionIndex] = ctx.initialNumberOfFlightsToShow;
					ctx.flightSelections[otherSelectionIndex] = null;
				}
			}
		}
		ctx.readyToPassengersDataPhase = false;
		ctx.phase = BookingPhaseEnum.FLIGHTS_SELECTION;
	}

	/**
	 * Perform a flight selection of the specified kind, according
	 * to the current search type.
	 * 
	 * <p>The performed selection is saved by setting the relevant
	 * element in the <code>flightSelections</code> context variable.</code></p>
	 * 
	 * <p>Internally, an invocation of the sell-up and taxes search service 
	 * methods may be invoked, based on the kind of selection performed. 
	 * In this case, the results are stored in the <code>selectionsRoutes</code> and 
	 * in the <code>selectionTaxes</code>.</p>
	 * 
	 * <p>Also, any currently entered coupon is cleared (also resetting the
	 * <code>selectionCabinClass</code> context variable), and the global
	 * cabin class is computed and saved in the <code>selectionCabinClass</code>
	 * context variable.</p>
	 * 
	 * @param ctx The booking session context to use.
	 * @param elementSelectionIndex The index of the search element for which the selection is performed.
	 * @param solutionId Unique solution identifier of the selection.
	 * @param solutionFlightIndex Index of the selected flight in the list of flights obtained for the search element.
	 * @param solutionBrandIndex Index of the selected brand in the list of brands obtained for the search element.
	 */
	public void performFlightSelection(BookingSessionContext ctx, int elementSelectionIndex,
			String solutionId, int solutionFlightIndex, int solutionBrandIndex) {
		logger.debug("performFlightSelection");
		if (ctx.phase != BookingPhaseEnum.FLIGHTS_SELECTION) {
			logger.error("performFlightSelection not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}
		FlightData flightData = searchSelectionFlightData(ctx, elementSelectionIndex, solutionFlightIndex);
		BrandData brandData = searchSelectionBrandData(flightData, solutionBrandIndex);
		if(brandData.isEnabled()==false){
			logger.error("Invalid brand selected for this flight.");
			throw new IllegalArgumentException("Invalid brand selected for this flight.");
		}

		//obtain the current solution or refreshed solution ID
		String solutionIdOfFlightBrandSelection = "";
		if( brandData.getRefreshSolutionId()!=null && !("").equals(brandData.getRefreshSolutionId()) ){ 
			solutionIdOfFlightBrandSelection = brandData.getRefreshSolutionId();
		}else{
			solutionIdOfFlightBrandSelection = brandData.getSolutionId();
		}

		if (!solutionId.equals(solutionIdOfFlightBrandSelection)) {
			logger.error("Invalid solution id provided for the selection.");
			throw new IllegalArgumentException("Invalid solution id provided for the selection.");
		}
		List<Integer> flightsMileage = null;
		List<String> carriers = computeCarriersList(flightData, ctx);
		FlightSelection flightSelection = new FlightSelection(flightData, brandData.getCode(), null,
				null, carriers, computeIsTariffaLight(brandData.getCode()), solutionFlightIndex, solutionId);
		if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
			elementSelectionIndex = ctx.currentSliceIndex - 1;
		}
		flightSelection.setBus(isBus(flightSelection.getFlightData()));
		ctx.flightSelections[elementSelectionIndex] = flightSelection;
		ctx.busIsSelected = computeSelectedBus(ctx);
		/*if (!flightSelection.isTariffaLight()) {
			flightsMileage = retrieveFlightsMileages(ctx,flightData,solutionBrandIndex);
		} else {*/
		flightsMileage = new ArrayList<Integer>();
		flightsMileage.add(0);
		//}
		ctx.flightSelections[elementSelectionIndex].setFlightsMileage(flightsMileage);
		Map<String, String> fareRules = computeFareRules(brandData,flightData, flightsMileage, elementSelectionIndex, ctx);
		if (fareRules.containsKey("booking.regoleTariffarie.bagaglioInStiva")) {
			ctx.flightSelections[elementSelectionIndex].setOnlyHandBaggage(fareRules.get("booking.regoleTariffarie.bagaglioInStiva").equals("false"));
		}
        setAsterisck(fareRules);
		ctx.flightSelections[elementSelectionIndex].setFareRules(fareRules);

		String tid = IDFactory.getTid();
		if(ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP){

			SearchFlightSolutionResponse searchRefreshResponse = null;
			//if it is the first selection
			if (!ctx.isRefreshed) {
				ctx.firstSelectionIndex = elementSelectionIndex;
				RouteData previouslySelectedRoute = ctx.availableFlights.getRoutes().get(ctx.firstSelectionIndex);
				searchRefreshResponse = performRefreshSearch(tid, ctx);
				if(searchRefreshResponse==null){
					logger.error("The RefreshSearch service has failed");
					throw new IllegalStateException("The RefreshSearch service has failed");
				}
				//replace the old routes with the new routes returned by the refresh service delegate

				ctx.availableFlights = searchRefreshResponse.getAvailableFlights();
				ctx.availableFlights.getRoutes().set(ctx.firstSelectionIndex, previouslySelectedRoute);
				ctx.isRefreshed = true;
				ctx.cookie = searchRefreshResponse.getCookie();
				ctx.execution = searchRefreshResponse.getExecute();
				ctx.sabreGateWayAuthToken = searchRefreshResponse.getSabreGateWayAuthToken();
			} else {
				//check if the first selection is changing
				if (ctx.firstSelectionIndex == elementSelectionIndex) {
					/*Clearing other flight selection, because otherwise Sabre will return only one flight in refreshSearch*/
					ctx.flightSelections[1-ctx.firstSelectionIndex] = null;
					ctx.readyToPassengersDataPhase = false;
					RouteData previouslySelectedRoute = ctx.availableFlights.getRoutes().get(ctx.firstSelectionIndex);
					searchRefreshResponse = performRefreshSearch(tid, ctx);
					if(searchRefreshResponse==null){
						logger.error("The RefreshSearch service has failed");
						throw new IllegalStateException("The RefreshSearch service has failed");
					}
					//replace the old routes with the new routes returned by the refresh service delegate
					ctx.availableFlights = searchRefreshResponse.getAvailableFlights();
					ctx.availableFlights.getRoutes().set(ctx.firstSelectionIndex, previouslySelectedRoute);

					int otherSelectionIndex = 0;
					RouteTypeEnum otherRouteType = RouteTypeEnum.OUTBOUND;
					if(elementSelectionIndex == 0){
						otherSelectionIndex = 1;
						otherRouteType = RouteTypeEnum.RETURN;
					}
					//manage the selection of this selected flight if the other flight was previously selected
					if(ctx.flightSelections[otherSelectionIndex]!=null){
						//the other flight was selected
						String selectedOtherFlightNumber = "";
						String otherFlightNumber = "";
						FlightSelection selectedOtherFlight = ctx.flightSelections[otherSelectionIndex];
						if ( selectedOtherFlight.getFlightData().getFlightType() == FlightTypeEnum.CONNECTING ) {
							selectedOtherFlightNumber = ((DirectFlightData) ((ConnectingFlightData) selectedOtherFlight.getFlightData()).getFlights().get(0)).getFlightNumber();
						} else {
							selectedOtherFlightNumber = ((DirectFlightData) selectedOtherFlight.getFlightData()).getFlightNumber();
						}

						RouteData otherRoute = null;
						for(RouteData route : ctx.availableFlights.getRoutes()) {
							if (route.getType() == otherRouteType) {
								otherRoute = route;
							}
						}
						ArrayList<FlightData> refreshedAvailableFlights = otherRoute.getFlights();
						for (FlightData otherFlightData : refreshedAvailableFlights) {
							if ( otherFlightData.getFlightType() == FlightTypeEnum.CONNECTING ) {
								otherFlightNumber = ((DirectFlightData) ((ConnectingFlightData) otherFlightData).getFlights().get(0)).getFlightNumber();
							} else {
								otherFlightNumber = ((DirectFlightData) otherFlightData).getFlightNumber();
							}
							if ( selectedOtherFlightNumber.equals(otherFlightNumber) ) {

								for(BrandData otherBrandData : otherFlightData.getBrands() ) {
									if ( otherBrandData.getCode().equalsIgnoreCase(
											selectedOtherFlight.getSelectedBrandCode()) ) {
										if ( otherBrandData.getRefreshSolutionId() == null || ("").equals(otherBrandData.getRefreshSolutionId()) ){
											ctx.flightSelections[otherSelectionIndex] = null;
											ctx.readyToPassengersDataPhase = false;
										} else {
											logger.debug("SolutionID Rountrip with Reselection: [" + solutionId + "|"+otherBrandData.getRefreshSolutionId()+"]");
											performBookingSellupAndTaxSearch(tid,ctx, solutionId +"|"+ otherBrandData.getRefreshSolutionId(), flightData);
											ctx.fullTextFareRules = computeFullTextFareRules(ctx, solutionId, tid);
											ctx.readyToPassengersDataPhase = true;
										}
										break;
									}
								}
								break;
							}
						}
					}
				} else {
					//the other selection is changing
					//it invokes sellup and tax search
					logger.debug("SolutionID Rountrip: ["+brandData.getSolutionId()+"|"+brandData.getRefreshSolutionId()+"]");
					performBookingSellupAndTaxSearch(tid,ctx, brandData.getSolutionId()+"|"+brandData.getRefreshSolutionId(), flightData);
					ctx.fullTextFareRules = computeFullTextFareRules(ctx, solutionId, tid);
					ctx.readyToPassengersDataPhase = true;
				}
			}

		}

		if(ctx.searchKind == BookingSearchKindEnum.SIMPLE){
			performBookingSellupAndTaxSearch(tid, ctx, solutionIdOfFlightBrandSelection, flightData);
			ctx.fullTextFareRules = computeFullTextFareRules(ctx, solutionId, tid);
			ctx.readyToPassengersDataPhase = true;
		}

		if(ctx.searchKind == BookingSearchKindEnum.MULTILEG){
			//check if the selection is the last one
			if(ctx.currentSliceIndex == ctx.flightSelections.length) {
				logger.debug("SolutionID Multileg: ["+brandData.getSolutionId()+"|"+brandData.getRefreshSolutionId()+"]");
				performBookingSellupAndTaxSearch(tid, ctx, brandData.getSolutionId()+"|"+brandData.getRefreshSolutionId(), flightData);
				ctx.fullTextFareRules = computeFullTextFareRules(ctx, solutionId, tid);
				ctx.readyToPassengersDataPhase = true;
			}
		}

		ctx.coupon = null;
		ctx.cashAndMiles = null;
		ctx.isCashMilesReady = false;
		ctx.isCashMilesApplied = false;
		ctx.selectionCabinClass = computeSelectionCabin(ctx);
	}

	/**
	 * Perform correct search for refresh
	 * @param tid
	 * @param ctx
	 * @return
	 */
	private SearchFlightSolutionResponse performRefreshSearch(String tid, BookingSessionContext ctx) {
		// ctx.availableFlights have to be set to null in order to set the correct dates 
		// in the createFlightBrandSearchRequest method.
		ctx.availableFlights = null;
		if (ctx.cug == BookingSearchCUGEnum.CARNET) {
			return searchFlightsDelegate.searchInitialSolutions(createFlightCarnetSearchRequest(tid, ctx));
		} else {
			return searchFlightsDelegate.searchInitialSolutions(createFlightBrandSearchRequest(tid, ctx));
		}
	}

	/**
	 * Obtatin fullTextRules for market US_EN - CA_EN
	 * @param ctx
	 * @return
	 */
	public ArrayList<String> computeFullTextFareRules(BookingSessionContext ctx, String solutionID, String tid) {

		//creo il filter
		FullTextSearchData fullTextSearchData = new FullTextSearchData();
		fullTextSearchData.setId(ctx.availableFlights.getId());
		fullTextSearchData.setSessionId(ctx.availableFlights.getSessionId());
		fullTextSearchData.setSolutionSet(ctx.availableFlights.getSolutionSet());
		fullTextSearchData.setType(GatewayTypeEnum.FULL_TEXT_RULES_SEARCH);
		fullTextSearchData.setPassengers(ctx.extraChargePassengerList);
		fullTextSearchData.setSolutionId(solutionID);

		//creo la request
		SearchFlightSolutionRequest fullTextSearchRequest = new SearchFlightSolutionRequest(tid, ctx.sid);
		fullTextSearchRequest.setCookie(ctx.cookie);
		fullTextSearchRequest.setExecution(ctx.execution);
		fullTextSearchRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
		fullTextSearchRequest.setMarket(ctx.market);
		fullTextSearchRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		fullTextSearchRequest.setResponseType(SearchExecuteResponseType.MODEL);
		fullTextSearchRequest.setFilter(fullTextSearchData);

		//chiamo il servizio
		SearchFlightSolutionResponse searchFlightSolutionResponse = 
				searchFlightsDelegate.executeFullTextRulesSearch(fullTextSearchRequest);
		ctx.cookie = searchFlightSolutionResponse.getCookie();
		ctx.execution = searchFlightSolutionResponse.getExecute();
		ctx.sabreGateWayAuthToken = searchFlightSolutionResponse.getSabreGateWayAuthToken();

		if (searchFlightSolutionResponse != null 
				&& searchFlightSolutionResponse.getAvailableFlights() != null) {

			return	searchFlightSolutionResponse.getAvailableFlights().getFullTextRules();
		} else {
			logger.error("SearchSerice for FullTextRules has not returned information");
		}

		return null;
	}

	/**
	 * It invalids the selection for the slice if a connecting flight was selected
	 * @param ctx
	 */
	public boolean hideDirectFlights(BookingSessionContext ctx, int selectedFlightIndex){
		logger.debug("performHideDirectFlights");
		if (ctx.phase != BookingPhaseEnum.FLIGHTS_SELECTION) {
			logger.error("performFlightSelection not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}
		if (ctx.flightSelections != null) {
			FlightSelection selectedFlight = ctx.flightSelections[selectedFlightIndex];
			if (selectedFlight != null) {
				if (selectedFlight.getFlightData().getFlightType() == FlightTypeEnum.CONNECTING) {
					ctx.flightSelections[selectedFlightIndex] = null;
					ctx.readyToPassengersDataPhase = false;
					ctx.selectionCabinClass = computeSelectionCabin(ctx);
					ctx.coupon = null;
					ctx.isCouponValid = null;
					ctx.isCouponFanPlayr = null;
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * it return the list of flights to show in the matrix
	 * @param route
	 * @param moreFlights 
	 * @return
	 */
	public List<FlightData> obtainFlightsToShown(BookingSessionContext ctx, int routeIndex, RouteData route, boolean moreFlights) {
		List<FlightData> flightsToShown = new ArrayList<FlightData>();
		FlightSelection selectedFlight = ctx.flightSelections[routeIndex];
		int indexSelectedFlight = -1;
		if (route.getFlights().size() <= ctx.numberOfShownFlights[routeIndex]) {
			return  route.getFlights();
		}

		if (ctx.numberOfShownFlights[routeIndex] == 0) { //it happens at the first rendering of the partial selection
			if (route.getFlights().size() <= ctx.initialNumberOfFlightsToShow) {
				return  route.getFlights();
			}
			int indexBestPrice = 0;
			boolean found = false;

			if (ctx.searchKind != BookingSearchKindEnum.MULTILEG && !ctx.isCarnetProcess) {
				// Recupero tanti risultati fino a raggiungere quello con il prezzo uguale a quello nella ribbon
				if (ctx.availableFlights.getTabs() != null && ctx.availableFlights.getTabs().get(routeIndex) != null) {
					TabData routeTab = ctx.availableFlights.getTabs().get(routeIndex);
					if(routeIndex==0 && routeTab.getType()==RouteTypeEnum.RETURN){
						routeTab = ctx.availableFlights.getTabs().get(1);
					}
					if(routeIndex==1 && routeTab.getType()==RouteTypeEnum.OUTBOUND){
						routeTab = ctx.availableFlights.getTabs().get(0);
					}

					BigDecimal bestPrice = routeTab.getFlightTabs().get(ctx.selectedDepartureDateChoices[routeIndex]).getPrice();
					logger.debug("Retrieve flights for route: {}. Best price = {}", routeTab.getType().toString(), bestPrice);

					for (FlightData flightData : route.getFlights()) {
						if (flightData.getBrands() != null) {
							for (BrandData brandData : flightData.getBrands()) {
								if (!brandData.getGrossFare().equals(BigDecimal.ZERO) && 
										brandData.getGrossFare() != null && 
										brandData.getGrossFare().compareTo(bestPrice) <= 0) {
									found = true;
									break;
								}
							}
							if (found) {
								break;
							}
						}
						indexBestPrice++;
					}
				}

				logger.debug("Best price: found = {}, position = {}", found, indexBestPrice);
			}
			if (indexBestPrice > ctx.initialNumberOfFlightsToShow) {
				ctx.numberOfShownFlights[routeIndex] = indexBestPrice;
			} else {
				ctx.numberOfShownFlights[routeIndex] = ctx.initialNumberOfFlightsToShow;
			}
			flightsToShown = route.getFlights().subList(0, ctx.numberOfShownFlights[routeIndex]);
		} else {
			if (moreFlights) {
				ctx.numberOfShownFlights[routeIndex] = ctx.numberOfShownFlights[routeIndex] 
						+ ctx.numberOfMoreFlights;
			}
			if (selectedFlight != null) {
				indexSelectedFlight = selectedFlight.getIndexFlight();
				if (indexSelectedFlight > ctx.numberOfShownFlights[routeIndex]) {
					while (indexSelectedFlight >= ctx.numberOfShownFlights[routeIndex]) {
						ctx.numberOfShownFlights[routeIndex] = ctx.numberOfShownFlights[routeIndex] 
								+ ctx.numberOfMoreFlights;
					}
				}
			}
			if (route.getFlights().size() <= ctx.numberOfShownFlights[routeIndex]) {
				ctx.numberOfShownFlights[routeIndex] = route.getFlights().size();
				return  route.getFlights();
			}
			flightsToShown = route.getFlights().subList(0, ctx.numberOfShownFlights[routeIndex]);
		}
		return flightsToShown;
	}

	/**
	 * Confirm the flight selections performed, closing the flight selection phase
	 * and advancing to the passengers data phase.
	 * 
	 * <p>In this step, a set of data required for the passenger data phase
	 * (including the seat maps) are also prepared and made available as 
	 * booking context object variables:
	 * <ul>
	 * <li><code>selectionAllDirectFlights</code></li>
	 * <li><code>selectionSearchElementDirectFlights</code></li>
	 * <li><code>isSeatMapSelectionAllowed</code></li>
	 * <li><code>seatMaps</code></li>
	 * <li><code>seatMapsByFlight</code></li>
	 * <li><code>countries</code></li>
	 * <li><code>phonePrefixes</code></li>
	 * <li><code>mealTypes</code></li>
	 * <li><code>frequentFlyerTypes</code></li>
	 * <li><code>isApis</code></li>
	 * <li><code>isSecureFlight</code></li>
	 * <li><code>isSecureFlightESTA</code></li>
	 * </ul>
	 * </p>
	 * 
	 * <p>The phase is also advanced to passenger data.</p>
	 * 
	 * @param ctx The booking session context to use.
	 */
	@SuppressWarnings("unchecked")
	public void confirmFlightSelections(BookingSessionContext ctx, String solutionIdOfFlightBrandSelection, 
			SlingHttpServletRequest request) {
		logger.debug("confirmFlightSelections");
		if (ctx.phase != BookingPhaseEnum.FLIGHTS_SELECTION) {
			logger.error("confirmFlightSelections not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}
		String tid = IDFactory.getTid();
		if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
			//check if the selection is no the last one
			if(!ctx.readyToPassengersDataPhase) {
				//invoke the searchMultipleLegsNextSolution to obtain the next slice
				SearchFlightSolutionResponse nextSliceSearchResponse = 
						searchFlightsDelegate.searchMultipleLegsSolutions(createMultilegSearchRequest(tid, ctx));
				ctx.availableFlights = nextSliceSearchResponse.getAvailableFlights();
				ArrayList<RouteData> routes = new ArrayList<>();
				routes.add(nextSliceSearchResponse.getAvailableFlights().getRoutes().get(ctx.currentSliceIndex));
				ctx.availableFlights.setRoutes(routes);
				ctx.currentSliceIndex++;
				ctx.isBusCarrier = nextSliceSearchResponse.isBusCarrier();
				ctx.searchCategory = computeSearchCategory(ctx);
				logger.info("The search category is {}", ctx.searchCategory != null ? ctx.searchCategory.getValue() : "null");
				ctx.codeBrandMap = computeCodeBrandMap(ctx);
				ctx.cookie = nextSliceSearchResponse.getCookie();
				ctx.execution = nextSliceSearchResponse.getExecute();
				ctx.sabreGateWayAuthToken = nextSliceSearchResponse.getSabreGateWayAuthToken();
			}
		} 

		if (ctx.readyToPassengersDataPhase) {

			computeTotalAmounts(ctx);

			// retrieve logged user information
			retrieveLoggedUserProfile(ctx, request);



			// prepare the list of all the direct flights involved in the selections
			// (including those that are part of connecting flights), and make them
			// available in a unique list as well as grouped for each search element
			List<DirectFlightData> selectionAllDirectFlights = new ArrayList<DirectFlightData>();
			List<DirectFlightData>[] selectionSearchElementDirectFlights = 
					new ArrayList[ctx.flightSelections.length];
			int flightSelectionIndex = 0;
			for (FlightSelection flightSelection : ctx.flightSelections) {
				selectionSearchElementDirectFlights[flightSelectionIndex] = new ArrayList<DirectFlightData>();
				if (flightSelection.getFlightData() instanceof DirectFlightData) {
					DirectFlightData directFlightData = (DirectFlightData) flightSelection.getFlightData();
					selectionSearchElementDirectFlights[flightSelectionIndex].add(directFlightData);
					selectionAllDirectFlights.add(directFlightData);
				} else if (flightSelection.getFlightData() instanceof ConnectingFlightData) {
					for (FlightData flight : ((ConnectingFlightData) flightSelection.getFlightData()).getFlights()) {
						if (flight instanceof DirectFlightData) {
							DirectFlightData directFlightData = (DirectFlightData) flight;
							selectionSearchElementDirectFlights[flightSelectionIndex].add(directFlightData);
							selectionAllDirectFlights.add((DirectFlightData) flight);
						}
					}
				}
				flightSelectionIndex++;
			}

			// for each direct flight, update the seat map flag according to the local business rule
			flightSelectionIndex = 0;
			for (FlightSelection flightSelection : ctx.flightSelections) {
				for (DirectFlightData directFlightToUpdate : selectionSearchElementDirectFlights[flightSelectionIndex]) {
					directFlightToUpdate.setEnabledSeatMap(
							isSeatSelectionEnabledForDirectFlight(ctx.loggedUserProfileData, directFlightToUpdate, 
									flightSelection.getSelectedBrandCode()));
				}
				flightSelectionIndex++;
			}

			// prepare countries for nationality
			RetrieveCountriesRequest countriesRequest = new RetrieveCountriesRequest(ctx.sid, tid);
			countriesRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
			countriesRequest.setMarket(ctx.market);
			RetrieveCountriesResponse countriesResponse = 
					staticDataDelegate.retrieveCountries(countriesRequest);

			// prepare phone prefixes
			RetrievePhonePrefixRequest phonePrefixesRequest = new RetrievePhonePrefixRequest(ctx.sid, tid);
			phonePrefixesRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
			phonePrefixesRequest.setMarket(ctx.market);
			RetrievePhonePrefixResponse phonePrefixesResponse = 
					staticDataDelegate.retrievePhonePrefixs(phonePrefixesRequest);

			// prepare meal types
			RetrieveMealsRequest mealsRequest = new RetrieveMealsRequest(ctx.sid, tid);
			mealsRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
			mealsRequest.setMarket(ctx.market);
			RetrieveMealsResponse mealsResponse = 
					staticDataDelegate.retrieveMeals(mealsRequest);

			// prepare frequent flyer types
			RetrieveFrequentFlayerRequest frequentFlyerRequest = new RetrieveFrequentFlayerRequest(ctx.sid, tid);
			frequentFlyerRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
			frequentFlyerRequest.setMarket(ctx.market);
			RetrieveFrequentFlyersResponse frequentFlyerResponse = 
					staticDataDelegate.retrieveFrequentFlyersType(frequentFlyerRequest);

			// prepare provinces for Italy
			RetrieveProvincesRequest provinceRequest = new RetrieveProvincesRequest(ctx.sid, tid);
			provinceRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
			provinceRequest.setMarket(ctx.market);
			RetrieveProvincesResponse provinceResponse = 
					staticDataDelegate.retrieveProvinces(provinceRequest);

			// prepare countries for USA
			RetrieveStateListRequest stateListRequest = new RetrieveStateListRequest(ctx.sid, tid);
			stateListRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
			stateListRequest.setMarket(ctx.market);
			stateListRequest.setItemCache("US");
			RetrieveStateListResponse stateListResponse = 
					staticDataDelegate.retrieveStateList(stateListRequest);


			ctx.selectionAllDirectFlights = selectionAllDirectFlights;
			ctx.selectionSearchElementDirectFlights = selectionSearchElementDirectFlights;
			ctx.countries = countriesResponse.getCountries();

			if (ctx.countries != null) {
				for (CountryData countryData : ctx.countries) {
					countryData.setDescription("countryData." + countryData.getCode());
				}
			}

			ctx.districts = provinceResponse.getProvinces();

			if (ctx.districts != null) {
				for (CountryData countryData : ctx.districts) {
					countryData.setDescription("stateProvinceData.it." + countryData.getStateCode());
				}
			}

			ctx.countriesUSA = stateListResponse.getStates();

			if (ctx.countriesUSA != null) {
				for (StateData stateData : ctx.countriesUSA) {
					stateData.setStateDescription("stateProvinceData.us." + stateData.getStateCode());
				}
			}

			ctx.phonePrefixes = phonePrefixesResponse.getPhonePrefix();
			ctx.mealTypes = mealsResponse.getMeals();
			ctx.frequentFlyerTypes = frequentFlyerResponse.getFrequentFlyers();
			ctx.isSecureFlight = computeIsSecureFlight(ctx);
			ctx.isSecureFlightESTA = computeIsSecureFlightESTA(ctx);
			ctx.isApis = computeIsApis(ctx);
			ctx.mealsEnabled = computeIfMealsAreEnabled(ctx);
			ctx.insuranceEnabled = computeIfInsuranceIsEnabled(ctx);
			if (ctx.insuranceEnabled) {
				retrieveInsurance(ctx);
			}

			ctx.phase = BookingPhaseEnum.PASSENGERS_DATA;
		}
	}

	private boolean isBus(FlightData flightData) {
		if (flightData == null) {
			return false;
		}
		if (flightData instanceof DirectFlightData) {
			DirectFlightData directFlight = (DirectFlightData)flightData;
			if (directFlight.isBus()) {
				return true;
			}
		} else {
			ConnectingFlightData connectingFlightData = (ConnectingFlightData)flightData;
			for (FlightData flight : connectingFlightData.getFlights()) {
				DirectFlightData directFlight = (DirectFlightData) flight;
				if (directFlight.isBus()) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean computeSelectedBus(BookingSessionContext ctx) {
		if (ctx.flightSelections != null) {
			for (FlightSelection selectedFlight : ctx.flightSelections) {
				if (selectedFlight != null && selectedFlight.isBus()) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 *
	 */
	public List<AdultPassengerData> checkCodePassengerDetails(BookingSessionContext ctx) {
		logger.debug("checkCodePassengerDetails");
		if (ctx.phase != BookingPhaseEnum.PASSENGERS_DATA) {
			logger.error("submitPassengerDetails not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}

		String tid = IDFactory.getTid();

		// prepare a list including only adult passengers with a selected frequent flyer program
		List<AdultPassengerData> adultPassengersWithFrequentFlyerProgram = new ArrayList<>();
		for (PassengerBaseData passenger : ctx.selectionRoutes.getPassengers()) {
			if (passenger instanceof AdultPassengerData) {
				// Per booking award il codice carta del primo passeggero puo' non corrispondere a nome e cognome
				if (!ctx.award || !(passenger instanceof ApplicantPassengerData)) {
					AdultPassengerData adultPassenger = (AdultPassengerData) passenger;
					if (adultPassenger.getFrequentFlyerType() != null) {
						adultPassengersWithFrequentFlyerProgram.add(adultPassenger);
					}
				}
			}
		}

		return checkCodePassengerDetails(ctx.selectionRoutes.getPassengers(), 
				adultPassengersWithFrequentFlyerProgram, ctx.sid, tid, ctx.cookie, ctx.execution, ctx.sabreGateWayAuthToken, ctx.cug != null ? ctx.cug.value() : null);
	}
	
	
	private List<AdultPassengerData> checkCodePassengerDetails(
			List<PassengerBaseData> allPassengers,
			List<AdultPassengerData> adultPassengersWithFrequentFlyerProgram,
			String sid, String tid, String cookie, String execution, String sabreGateWayAuthToken, String cug) {

		List<AdultPassengerData> adultPassengersCheckCodeWithWrongFrequentFlyer = new ArrayList<>();

		// perform a local check using available check pattern
		Iterator<AdultPassengerData> adultToCheckIterator = adultPassengersWithFrequentFlyerProgram.iterator();
		while (adultToCheckIterator.hasNext()) {
			AdultPassengerData adultPassenger = adultToCheckIterator.next();
			String validationRegExp = adultPassenger.getFrequentFlyerType().getRegularExpressionValidation();
			if (!"".equals(validationRegExp) && validationRegExp != null) {
				if (!adultPassenger.getFrequentFlyerCode().matches(validationRegExp)) {
					// move the element to the error list, and removes it to the check list
					adultPassengersCheckCodeWithWrongFrequentFlyer.add(adultPassenger);
					adultToCheckIterator.remove();
				}
			}
		}

		return adultPassengersCheckCodeWithWrongFrequentFlyer;
	}



	/**
	 * Send passengers data to BL and perform validation logic for the frequent flyer codes 
	 * of current adult passengers data, without modifying the booking session context state.
	 * 
	 * <p>The phase is not changed by this method.</p>
	 * 
	 * @param ctx The booking session context to use.
	 * @returns A subset of the adult passengers data including only passengers with error.
	 */
	public List<AdultPassengerData> submitPassengerDetails(BookingSessionContext ctx) {
		logger.debug("submitPassengerDetails");
		if (ctx.phase != BookingPhaseEnum.PASSENGERS_DATA) {
			logger.error("submitPassengerDetails not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}

		String tid = IDFactory.getTid();

		// prepare a list including only adult passengers with a selected frequent flyer program
		List<AdultPassengerData> adultPassengersWithFrequentFlyerProgram = new ArrayList<>();
		for (PassengerBaseData passenger : ctx.selectionRoutes.getPassengers()) {
			if (passenger instanceof AdultPassengerData) {
				// Per booking award il codice carta del primo passeggero puo' non corrispondere a nome e cognome
				if (!ctx.award || !(passenger instanceof ApplicantPassengerData)) {
					AdultPassengerData adultPassenger = (AdultPassengerData) passenger;
					if (adultPassenger.getFrequentFlyerType() != null) {
						adultPassengersWithFrequentFlyerProgram.add(adultPassenger);
					}
				}
			}
		}

		return submitPassengerDetails(ctx, ctx.selectionRoutes.getPassengers(),
				adultPassengersWithFrequentFlyerProgram, ctx.sid, tid, ctx.cookie, ctx.execution, ctx.sabreGateWayAuthToken, ctx.cug != null ? ctx.cug.value() : null);
	}

	private List<AdultPassengerData> submitPassengerDetails(BookingSessionContext ctx,
			List<PassengerBaseData> allPassengers,
			List<AdultPassengerData> adultPassengersWithFrequentFlyerProgram,
			String sid, String tid, String cookie, String execution, String sabreGateWayAuthToken, String cug) {

		List<AdultPassengerData> adultPassengersWithWrongFrequentFlyer = new ArrayList<>();

		// perform a local check using available check pattern
		Iterator<AdultPassengerData> adultToCheckIterator = adultPassengersWithFrequentFlyerProgram.iterator();
		while (adultToCheckIterator.hasNext()) {
			AdultPassengerData adultPassenger = adultToCheckIterator.next();
			String validationRegExp = adultPassenger.getFrequentFlyerType().getRegularExpressionValidation();
			if (!"".equals(validationRegExp) && validationRegExp != null) {
				if (!adultPassenger.getFrequentFlyerCode().matches(validationRegExp)) {
					// move the element to the error list, and removes it to the check list
					adultPassengersWithWrongFrequentFlyer.add(adultPassenger);
					adultToCheckIterator.remove();
				}
			}
		}

		if (adultPassengersWithWrongFrequentFlyer.size() >= 0) {
			// if any adult is still on the check list (i.e. passed the static validation or
			// static validation rules are not available), then perform a verification using
			// the remote service - this may also perform an actual check on individual codes
			// for certain frequent flyer types (such as MilleMiglia)
			InfoPassengerSubmitPassengerRequest submitPassengerRequest =
					new InfoPassengerSubmitPassengerRequest();
			submitPassengerRequest.setTid(tid);
			submitPassengerRequest.setSid(sid);
			submitPassengerRequest.setCookie(cookie);
			submitPassengerRequest.setCug(cug);
			submitPassengerRequest.setExecution(execution);
			submitPassengerRequest.setSabreGateWayAuthToken(sabreGateWayAuthToken);
			logger.debug("Cookie: ["+cookie+"], Execution: ["+execution+"]");
			submitPassengerRequest.setPassengers(allPassengers);
			submitPassengerRequest.setMarket(ctx.market);
			InfoPassengerSubmitPassengerResponse submitPassengerResponse = infoPassengerDelegate.submitPassengerDetails(submitPassengerRequest);
			cookie = submitPassengerResponse.getCookie();
			execution = submitPassengerResponse.getExecute();
			adultPassengersWithWrongFrequentFlyer.addAll(
					submitPassengerResponse.getAdultPassengersWithWrongFrequentFlyer());
		}

		return adultPassengersWithWrongFrequentFlyer;
	}


	/**
	 * Receives the passengers data and preferences and save them in the
	 * booking context object variables (overwriting the existing 
	 * passengers data if any):
	 * <ul>
	 * <li><code>passengersData</code></li>
	 * <li><code>selectionRoutes</code></li>
	 * </ul>
	 * </p>
	 * 
	 * <p>The phase is not changed by this method.</p>
	 * 
	 * @param ctx The booking session context to use.
	 * @param passengersData The data of the passengers.
	 */
	public void updatePassengersData(BookingSessionContext ctx, PassengersData passengersData) {
		logger.debug("updatePassengersData");
		if (ctx.phase != BookingPhaseEnum.PASSENGERS_DATA) {
			logger.error("updatePassengersData not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}
		
		// RIMOZIONE CARATTERI ACCENTATI
		if (configuration.getSpecialCharactersToBeReplaced())
			replaceSpecialCharacters(passengersData.getPassengersList());

		// update our model of the passegers data
		ctx.passengersData = passengersData;

		// also update the passengers data in the routes selection
		int i;
		for (i = 0; i < passengersData.getPassengersList().size(); i++) {
			Passenger sourcePassenger = passengersData.getPassengersList().get(i);
			PassengerBaseData targetPassenger = ctx.selectionRoutes.getPassengers().get(i);
			updatePassengerData(ctx, passengersData, sourcePassenger, targetPassenger);
		}

	}

	/**
	 * Receives the passengers data and preferences (including seat data),
	 * creates the new prenotation and save it (along the new data) in the
	 * booking context object variables:
	 * <ul>
	 * <li><code>passengersData</code></li>
	 * </ul>
	 * 
	 * <p>The phase is also advanced to ancillary.</p>
	 * 
	 * @param ctx The booking session context to use.
	 * @param passengersData The data of the passengers.
	 */
	public void confirmPassengersData(BookingSessionContext ctx, PassengersData passengersData) {
		logger.debug("confirmPassengersData");
		if (ctx.phase != BookingPhaseEnum.PASSENGERS_DATA) {
			logger.error("confirmPassengersData not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}
		
		updatePassengersData(ctx, passengersData);
		retrieveSeatMaps(ctx, IDFactory.getTid());

		ctx.phase = BookingPhaseEnum.ANCILLARY;
	}

	private void retrieveSeatMaps(BookingSessionContext ctx, String tid) {

		// prepare the seat maps of each direct flight (when available)
		boolean hasSeatMaps = false;
		List<FlightSeatMapData> seatMaps = new ArrayList<FlightSeatMapData>();
		Map<DirectFlightData, SeatMapData> seatMapsByFlight = new LinkedHashMap<DirectFlightData, SeatMapData>();

		try {
			// determine the number of seats required
			int numberOfSeatsRequired = 
					ctx.searchPassengersNumber.getNumAdults() +
					ctx.searchPassengersNumber.getNumChildren() +
					ctx.searchPassengersNumber.getNumYoung();
			for (DirectFlightData directFlight : ctx.selectionAllDirectFlights) {
				try {
					if (Boolean.TRUE.equals(directFlight.getEnabledSeatMap())) {
						RetrieveFlightSeatMapRequest seatMapRequest = new RetrieveFlightSeatMapRequest(ctx.sid, tid);
						seatMapRequest.setCookie(ctx.cookie);
						seatMapRequest.setExecution(ctx.execution);
						seatMapRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
						seatMapRequest.setFlightInfo(directFlight);
						seatMapRequest.setCompartimentalClass(directFlight.getCompartimentalClass());
						seatMapRequest.setMarket(ctx.market);
						RetrieveFlightSeatMapResponse seatMapResponse = 
								commonDelegate.retrieveFlightSeatMap(seatMapRequest);
						ctx.cookie = seatMapResponse.getCookie();
						ctx.execution = seatMapResponse.getExecute();
						ctx.sabreGateWayAuthToken = seatMapResponse.getSabreGateWayAuthToken();
						if (seatMapResponse.getSeatMapMatrix() != null && 
								seatMapResponse.getSeatMapMatrix().getDirectFlight() != null &&
								seatMapResponse.getSeatMapMatrix().getSeatMaps() != null &&
								seatMapResponse.getSeatMapMatrix().getSeatMaps().size() > 0 &&
								!directFlight.isBus()) {
							BrandData selectedBrandData = null;
							DirectFlightData seatMapDirectFlight = seatMapResponse.getSeatMapMatrix().getDirectFlight();
							for (BrandData brandData : seatMapDirectFlight.getBrands()) {
								if (brandData.isSelected()) {
									selectedBrandData = brandData;
									break;
								}
							}
							if (selectedBrandData != null) {
								int numberOfSeatsAvailable = 0;
								SeatMapData seatMapData = seatMapResponse.getSeatMapMatrix().getSeatMaps().get(0);
								for (SeatMapSectorData seatMapSectorData : seatMapData.getSeatMapSectors()) {
									for (SeatMapRowData seatMapRowData : seatMapSectorData.getSeatMapRows()) {
										for (SeatData seatData : seatMapRowData.getSeats()) {
											if (seatData.getTypeSeat() != TypeSeatEnum.NOT_EXIST &&
													seatData.getTypeSeat() != TypeSeatEnum.AISLE &&
													seatData.getAvailability() == AvailabilitySeatEnum.FREE) {
												numberOfSeatsAvailable++;
											}
										}
									}
								}
								if (numberOfSeatsAvailable > (numberOfSeatsRequired + 2)) {
									// seat selection is proposed when the there are at least 2 seats
									// available in addition to those to be selected for the prenotation
									seatMaps.add(seatMapResponse.getSeatMapMatrix());
									seatMapsByFlight.put(directFlight, seatMapData);
									hasSeatMaps = true;
								}
							}
						}else{
							FlightSeatMapData flightSeatMapData = new FlightSeatMapData();
							flightSeatMapData.setDirectFlight(directFlight);
							flightSeatMapData.setSeatMaps(null);
							seatMaps.add(flightSeatMapData);
							seatMapsByFlight.put(directFlight,null);
						}
					}
				} catch (Exception e) {
					logger.error("Unable to retrieve seatmap for flight " + directFlight.getCarrier() + " " + directFlight.getFlightNumber(), e);
				}
			}
			ctx.isSeatMapSelectionAllowed = hasSeatMaps;
			ctx.seatMaps = seatMaps;
			ctx.seatMapsByFlight = seatMapsByFlight;

		} catch (Exception e) {
			logger.error("Unable to retrieve seatmap for all flights", e);
		}
	}

	/**
	 * Receives the meal type preferences for all passengers and save them in
	 * the booking context object variables (overwriting the existing value
	 * already set in passengers data, if any):
	 * <ul>
	 * <li><code>passengersData</code></li>
	 * <li><code>selectionRoutes</code></li>
	 * </ul>
	 * </p>
	 * 
	 * <p>The phase is not changed by this method.</p>
	 * 
	 * @param ctx The booking session context to use.
	 * @param passengersMealPreference An array of strings with the meal type codes for all the passengers.
	 */
	public void updateAncillaryMealPreferences(BookingSessionContext ctx, String[] passengersMealPreference) {
		logger.debug("updateAncillaryMealPreferences");
		if (ctx.phase != BookingPhaseEnum.ANCILLARY) {
			logger.error("updateAncillaryMealPreferences not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}
		if (passengersMealPreference == null) {
			logger.error("passengersMealPreference cannot be null");
			throw new IllegalArgumentException("passengersMealPreference cannot be null.");
		}
		for (int i = 0; i<ctx.passengersData.getPassengersList().size(); i++ ) {

			// if passenger is not infant update his meal preference 
			if (!ctx.passengersData.getPassengersList().get(i).isInfant()) {
				ctx.passengersData.getPassengersList().get(i).setMealPreference(passengersMealPreference[i]);
				Passenger sourcePassenger = ctx.passengersData.getPassengersList().get(i);
				PassengerBaseData targetPassenger = ctx.selectionRoutes.getPassengers().get(i);
				updatePassengerData(ctx, ctx.passengersData, sourcePassenger,
						targetPassenger);
			}
		}
	}

	/**
	 * Receives the seat preferences for all passengers and save them in
	 * the booking context object variables (overwriting the existing value
	 * already set in passengers data, if any):
	 * <ul>
	 * <li><code>passengersData</code></li>
	 * <li><code>selectionRoutes</code></li>
	 * </ul>
	 * </p>
	 * 
	 * <p>The phase is not changed by this method.</p>
	 * 
	 * @param ctx The booking session context to use.
	 * @param passengersSeatPreference An array of strings with the seat selection codes for all the passengers.
	 */
	public void updateAncillarySeatPreferences(BookingSessionContext ctx, HashMap<Integer, 
			String>[] passengersSeatsPreference) {
		logger.debug("updateAncillarySeatPreferences");
		if (ctx.phase != BookingPhaseEnum.ANCILLARY) {
			logger.error("updateAncillarySeatPreferences not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}

		for (int i = 0; i < passengersSeatsPreference.length; i++) {
			Passenger updatedPassenger = ctx.passengersData.getPassengersList().get(i);

			// update our model of the passegers data with the seats preference
			updatedPassenger.setSeat(passengersSeatsPreference[i]);

			// update the model of passengers data in the routes selection with the seats preference
			PassengerBaseData updatedPassengerData = ctx.selectionRoutes.getPassengers().get(i);
			updatePassengerData(ctx, ctx.passengersData, updatedPassenger, updatedPassengerData);
		}
	}

	/**
	 * Receives the coupon code and attempt to apply it.
	 * 
	 * <p>This method can be used during the flight selection phase,
	 * but only if a complete and valid selection is provided, and 
	 * also during passenger data phase. If a valid coupon is already
	 * applied, this method cannot be invoked. Note that changing any 
	 * flight selections will implicitly clear the coupon.</p>
	 * 
	 * <p>The obtained coupon data is stored in the booking context object,
	 * along with an indicator of its validity. In case of a valid coupon
	 * the affected passengers data are also updated accordingly.
	 * <ul>
	 * <li><code>coupon</code></li>
	 * <li><code>isCouponValid</code></li>
	 * <li><code>isCouponFanPlayr</code></li>
	 * <li><code>selectionRoutes (update of passengers data)</code></li>
	 * </ul>
	 * </p>
	 * 
	 * <p>The phase is not changed.</p>
	 * 
	 * @param ctx The booking session context to use.
	 * @param couponCode The coupon code to apply.
	 * @param isFanPlayr True if the coupon is received from FanPlayr.
	 */
	public void applyCoupon(BookingSessionContext ctx, String couponCode, boolean isFanPlayr) {
		logger.debug("applyCoupon");
		if (ctx.phase != BookingPhaseEnum.PASSENGERS_DATA &&
				ctx.phase != BookingPhaseEnum.FLIGHTS_SELECTION) {
			logger.error("applyCoupon not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}
		if (ctx.searchKind != BookingSearchKindEnum.SIMPLE &&
				ctx.searchKind != BookingSearchKindEnum.ROUNDTRIP) {
			logger.error("applyCoupon not allowed for search kind {}", ctx.searchKind);
			throw new IllegalStateException("Operation not allowed for the current search kind.");
		}
		if (ctx.coupon != null && Boolean.TRUE.equals(ctx.isCouponValid)) {
			logger.error("applyCoupon not allowed when a valid coupon is already applied", ctx.searchKind);
			throw new IllegalStateException("Operation not allowed when a valid coupon is already applied.");
		}

		if (ctx.isCashMilesApplied) {
			ctx.coupon = null;
			ctx.isCouponFanPlayr = null;
			ctx.isCouponValid = false;
		}

		List<FlightData> itineraries = new ArrayList<FlightData>();
		for (FlightSelection selection : ctx.flightSelections) {
			if (selection == null) {
				logger.error("applyCoupon not allowed before complete flights selections");
				throw new IllegalStateException("Operation not allowed before complete flights selections.");
			}
			itineraries.add(selection.getFlightData());
		}

		String tid = IDFactory.getTid();

		if (!tariffaLightIsSelected(ctx)) {
			ctx.isECouponWithTariffaLight = false;
		}
		else {
			ctx.isECouponWithTariffaLight = true;
		}

		/*all cabin class in a list*/
		List<CabinEnum> cabinClasses = new ArrayList<>();
		for (int i=0; i<itineraries.size(); i++){
			CabinEnum cabinClass = computeCabinClassForEcoupon(itineraries.get(i));
			if(CabinEnum.MAGNIFICA.equals(cabinClass)){
				cabinClass = CabinEnum.BUSINESS;
			}
			cabinClasses.add(cabinClass);
		}
		
		/*cabin class is the lowest*/
//		CabinEnum cabinClass = computeCabinClassForEcoupon(itineraries.get(0));
//		for(int i=1; i< itineraries.size(); i++){
//			CabinEnum newCabinClass = computeCabinClassForEcoupon(itineraries.get(i));
//			if(cabinClass.compareTo(newCabinClass) > 0){
//				cabinClass = newCabinClass;
//			}
//		}
//		if(CabinEnum.MAGNIFICA.equals(cabinClass)){
//			cabinClass = CabinEnum.BUSINESS;
//		}

		LoadWidgetECouponRequest couponRequest = new LoadWidgetECouponRequest(tid, ctx.sid);
		couponRequest.setCookie(ctx.cookie);
		couponRequest.setExecution(ctx.execution);
		couponRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
		couponRequest.seteCouponCode(couponCode);
		couponRequest.setSite(ctx.site.toUpperCase());
		couponRequest.setIsRoundTrip((ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP));
		couponRequest.setItineraries(itineraries);
		couponRequest.setCabinClassList(cabinClasses);
		//couponRequest.setCabinClass(cabinClass);
		couponRequest.setPassengers(ctx.selectionRoutes.getPassengers());
		couponRequest.setTaxes(ctx.selectionTaxes);

		LoadWidgetECouponResponse couponResponse = loadWidgetDelegate.loadWidgetECoupon(couponRequest);

		if (couponResponse == null 
				|| couponResponse.geteCoupon() == null) {
			throw new RuntimeException("Cannot apply coupon (null response or coupon data)");
		}
		//			ctx.cookie = couponResponse.getCookie();
		//			ctx.execution = couponResponse.getExecute();
		//			ctx.sabreGateWayAuthToken = couponResponse.getSabreGateWayAuthToken();

		boolean isCouponValid = (couponResponse.geteCoupon().getErrorCode() == 0);

		if (isCouponValid){
			// Statistiche Ecoupon
			try {
				RegisterECouponStatisticRequest ecouponStatisticRequest = new RegisterECouponStatisticRequest(tid, ctx.sid);
				ecouponStatisticRequest.setClientIP(ctx.clientIP);
				ecouponStatisticRequest.setErrorDescr("");
				ecouponStatisticRequest.setSessionId(ctx.sessionId);
				ecouponStatisticRequest.setSiteCode(ctx.site);
				ecouponStatisticRequest.setSuccess(true);
				ecouponStatisticRequest.setType(StatisticsDataTypeEnum.ECOUPON);
				ecouponStatisticRequest.setUserId(ctx.loggedUserProfileData != null ? ctx.loggedUserProfileData.getCustomerNumber() : null);
				ecouponStatisticRequest.setDiscountName(couponCode);

				RegisterStatisticsResponse ecouponStatisticResponse = registerStatisticsDelegate.registerStatistics(ecouponStatisticRequest);
				if (ecouponStatisticResponse == null) {
					logger.error("Error while executing Ecoupon Statistic. Null response from Service.");
				}
			} catch (Exception e) {
				logger.error("Error while executing Ecoupon Statistic.");
			}
		}

		boolean isBrandLight = couponResponse.geteCoupon().getIsBrandLight() != null ? 
				couponResponse.geteCoupon().getIsBrandLight() : false;

				//Se seleziono una tariffa light ed il coupon non è applicabile per tariffa light lo considero invalido
				if (isBrandLight && ctx.isECouponWithTariffaLight){
					isCouponValid = false;
					couponResponse.geteCoupon().setValid(Boolean.FALSE);
				}

				if (isCouponValid && couponResponse.getPassengers() == null) {
					throw new RuntimeException("Cannot apply coupon (null passengers data)");
				}
				logger.info("ECoupon error code recived: {}. IsValid Field: {}", couponResponse.geteCoupon().getErrorCode(), couponResponse.geteCoupon().isValid());	

				ctx.isCouponValid = isCouponValid;
				ctx.isCouponFanPlayr = isFanPlayr;
				ctx.coupon = couponResponse.geteCoupon();
				if (isCouponValid) {
					// update the new pricing info for passengers
					int i;
					for (i = 0; i < couponResponse.getPassengers().size(); i++) {
						PassengerBaseData sourcePassenger = couponResponse.getPassengers().get(i);
						PassengerBaseData targetPassenger = ctx.selectionRoutes.getPassengers().get(i);
						targetPassenger.setCouponPrice(sourcePassenger.getCouponPrice());
						targetPassenger.setExtraCharge(sourcePassenger.getExtraCharge());
						targetPassenger.setFee(sourcePassenger.getFee());
						targetPassenger.setGrossFare(sourcePassenger.getGrossFare());
						targetPassenger.setNetFare(sourcePassenger.getNetFare());
					}

					ctx.selectionRoutes.setCoupon(ctx.coupon);

					// refresh the final prices for the current selection
					computeTotalAmounts(ctx);
				}
	}


	private CabinEnum computeCabinClassForEcoupon(FlightData flightData) {
		CabinEnum result = null;
		if(flightData instanceof DirectFlightData){
			result = ((DirectFlightData) flightData).getCabin();
		}
		else if(flightData instanceof ConnectingFlightData){
			ConnectingFlightData connFlight = ((ConnectingFlightData) flightData);
			result = ((DirectFlightData)connFlight.getFlights().get(0)).getCabin();
			for(int i=1; i< connFlight.getFlights().size(); i++){
				CabinEnum newCabinClass = ((DirectFlightData) connFlight.getFlights().get(i)).getCabin();
				if(result.compareTo(newCabinClass) > 0){
					result = newCabinClass;
				}
			}
		}
		return result;
	}



	private boolean tariffaLightIsSelected(BookingSessionContext ctx) {
		for (FlightSelection selectedFlight : ctx.flightSelections) {
			if (selectedFlight.isTariffaLight()) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Clear the currently set coupon code.
	 * 
	 * <p>This method can be used during the flight selection phase,
	 * but only if a complete and valid selection is provided, and 
	 * also during passenger data phase.</p>
	 * 
	 * <p>The coupon data is cleared in the booking context object,
	 * along with an indicator of its validity. If a valid coupon
	 * was applied, the affected passengers data are also updated
	 * accordingly.
	 * <ul>
	 * <li><code>coupon</code></li>
	 * <li><code>isCouponValid</code></li>
	 * <li><code>selectionRoutes (update of passengers data)</code></li>
	 * </ul>
	 * </p>
	 * 
	 * <p>The phase is not changed.</p>
	 * 
	 * @param ctx The booking session context to use.
	 */
	public void clearCoupon(BookingSessionContext ctx) {
		logger.debug("clearCoupon");
		if (ctx.phase != BookingPhaseEnum.PASSENGERS_DATA &&
				ctx.phase != BookingPhaseEnum.FLIGHTS_SELECTION) {
			logger.error("clearCoupon not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}
		if (ctx.searchKind != BookingSearchKindEnum.SIMPLE &&
				ctx.searchKind != BookingSearchKindEnum.ROUNDTRIP) {
			logger.error("clearCoupon not allowed for search kind {}", ctx.searchKind);
			throw new IllegalStateException("Operation not allowed for the current search kind.");
		}

		if (ctx.coupon == null) {
			logger.info("clearCoupon invoked but no coupon is currently set (ignoring request).");
			return;
		}

		ctx.selectionRoutes.setCoupon(null);
		ctx.isCouponValid = null;
		ctx.isCouponFanPlayr = null;
		ctx.coupon = null;

		if (Boolean.TRUE.equals(ctx.isCouponValid)) {
			// reset the pricing info for passengers to the saved values before coupon application
			int i;
			for (i = 0; i < ctx.selectionRoutesPassengersWithoutCoupon.size(); i++) {
				PassengerBaseData sourcePassenger = ctx.selectionRoutesPassengersWithoutCoupon.get(i);
				PassengerBaseData targetPassenger = ctx.selectionRoutes.getPassengers().get(i);
				targetPassenger.setCouponPrice(sourcePassenger.getCouponPrice());
				targetPassenger.setExtraCharge(sourcePassenger.getExtraCharge());
				targetPassenger.setFee(sourcePassenger.getFee());
				targetPassenger.setGrossFare(sourcePassenger.getGrossFare());
				targetPassenger.setNetFare(sourcePassenger.getNetFare());
			}

			// refresh the final prices for the current selection
			computeTotalAmounts(ctx);
		}
	}

	/**
	 * Sets cash and miles data in booking session context. 
	 * It can be called only in flight selection phase.
	 * 
	 * @param ctx Current Booking Session Context
	 * @param milesUsed how much miles will be used
	 * @param discountAmount the discount gained with this miles consumption
	 * @param customer the user that uses miles
	 */
	public void prepareCashAndMiles(BookingSessionContext ctx, Integer milesUsed, BigDecimal discountAmount, MMCustomerProfileData customer){
		logger.debug("prepareCashAndMiles");
		if (ctx.phase != BookingPhaseEnum.FLIGHTS_SELECTION) {
			logger.error("prepareCashAndMiles not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}
		if (ctx.isCashMilesReady || ctx.isCashMilesApplied) {
			logger.error("prepareCashAndMiles not allowed when cash and miles is already applied or ready to apply");
			throw new IllegalStateException("Operation not allowed when cash and miles is already applied or ready to apply");
		}

		// controllo se applicato ECoupon
		if (ctx.coupon != null && ctx.isCouponValid) {
			ctx.cashAndMiles = null;
			ctx.isCashMilesReady = false;
		} else {

			CashAndMilesData cashMiles = new CashAndMilesData();
			cashMiles.setUsedMileage(milesUsed);
			cashMiles.setDiscountAmount(discountAmount);

			GenderTypeEnum gender = null;
			switch(customer.getGender()){
			case FEMALE:
				gender = GenderTypeEnum.FEMALE;
				break;
			case MALE:
				gender = GenderTypeEnum.MALE;
				break;
			case UNKNOWN:
				gender = GenderTypeEnum.UNKNOWN;
				break;
			default:
				gender = GenderTypeEnum.UNKNOWN;
				break;

			}

			String dcryptedPin = customerProfileManager.decodeAndDecryptProperty(customer.getCustomerPinCode());

			MmCustomerData customerData = new MmCustomerData();
			customerData.setBirthDate(customer.getBirthDate());
			customerData.setGender(gender);
			customerData.setIgnore(false);
			customerData.setLastName(customer.getCustomerSurname());
			customerData.setCode(customer.getCustomerNumber());
			customerData.setPin(dcryptedPin);
			customerData.setMilesBalance(new Integer(0));
			customerData.setMilesEarned(new Integer(0));
			customerData.setMilesQualify(new Integer(0));
			customerData.setName(customer.getCustomerName());
			customerData.setTierCode(customer.getTierCode().value());
			cashMiles.setMmCustomer(customerData);

			Calendar date = Calendar.getInstance();
			date.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
			cashMiles.setPaymentDate(date);

			ctx.cashAndMiles = cashMiles;
			ctx.isCashMilesReady = true;
			ctx.selectionRoutes.setCashAndMiles(ctx.cashAndMiles);
			ctx.selectionRoutes.setMilleMigliaCustomer(ctx.cashAndMiles.getMmCustomer());
			for ( PassengerBaseData passenger : ctx.selectionRoutes.getPassengers()){
				if (passenger instanceof ApplicantPassengerData) {
					passenger.setCouponPrice(ctx.cashAndMiles.getDiscountAmount());
				}
			}
		}
	}
	/**
	 * Actually apply the cash and miles discount.
	 * It needs that the PNR and site are set in current booking session context.
	 * WARNING: It subtract the miles previously specified from the mm account! For
	 * canceling the operations and re-add the miles, use BookingSession.clearCashAndMiles
	 * @param ctx Current Booking Session Context
	 */
	public void applyCashAndMiles(BookingSessionContext ctx) {
		logger.debug("applyCashEMiles");

		/*if (ctx.isCashMilesApplied) {
			logger.error("applyCashAndMiles not allowed when cash and miles is already applied");
			throw new IllegalStateException("Cash and miles is already applied");
		}*/

		if (ctx.cashAndMiles != null && ctx.isCashMilesReady) {
			if (ctx.prenotation == null || ctx.prenotation.getPnr() == null) {
				logger.error("applyCashAndMiles not allowed when prenotation is not set");
				throw new IllegalStateException("Operation not allowed when prenotation is not set");
			}
			String tid = IDFactory.getTid();

			UpdateWidgetCashMilesRequest cmRequest = new UpdateWidgetCashMilesRequest(tid, ctx.sid);
			cmRequest.setCookie(ctx.cookie);
			cmRequest.setExecution(ctx.execution);
			cmRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
			cmRequest.setOperationType(CashMilesOperationType.SUBTRACT);
			cmRequest.setPnr(ctx.prenotation.getPnr());
			cmRequest.setSite(ctx.site);
			cmRequest.setCashMiles(ctx.cashAndMiles);

			UpdateWidgetCashMilesResponse cmResponse = loadWidgetDelegate.updateWidgetCashMiles(cmRequest);

			if (cmResponse != null && cmResponse.getCashMiles() != null && cmResponse.isSucceded()) {
				ctx.isCashMilesApplied = true;
				ctx.cookie = cmResponse.getCookie();
				ctx.execution = cmResponse.getExecute();
				ctx.sabreGateWayAuthToken = cmResponse.getSabreGateWayAuthToken();
				// refresh the final prices for the current selection
				computeTotalAmounts(ctx);
			} else {
				logger.error("Cannot apply cash and miles");
			}
		}
	}

	public void loadStoredCreditCard(BookingSessionContext ctx) {
		logger.debug("loadStoredCreditCard");

		if (ctx.loggedUserProfileData != null) {
			if (ctx.loggedUserProfileData.getCustomerNumber() == null || ctx.loggedUserProfileData.getCustomerPinCode() == null
					|| ctx.loggedUserProfileData.getCustomerSurname() == null) {
				logger.debug("User not logged in. loadStoredCreditCard not allowed");
				return;
			}
			String tid = IDFactory.getTid();

			LoadWidgetOneClickRequest oneClickRequest = new LoadWidgetOneClickRequest(tid, ctx.sid);
			oneClickRequest.setCookie(ctx.cookie);
			oneClickRequest.setExecution(ctx.execution);
			oneClickRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
			oneClickRequest.setType("OneClick");
			oneClickRequest.setLastName(ctx.loggedUserProfileData.getCustomerSurname());
			oneClickRequest.setMmCode(ctx.loggedUserProfileData.getCustomerNumber());
			oneClickRequest.setMmPin(ctx.loggedUserProfileData.getCustomerPinCode());

			MMCreditCardData creditCardData = loadWidgetDelegate.loadWidgetOneClick(oneClickRequest);

			if (creditCardData != null) {
				ctx.loggedUserProfileData.setStoredCreditCard(creditCardData);
				//				ctx.cookie = creditCardData.getCookie();
				//				ctx.execution = creditCardData.getExecute();
				//				ctx.sabreGateWayAuthToken = creditCardData.getSabreGateWayAuthToken();
			} else {
				logger.debug("No stored credit card for user {}", ctx.loggedUserProfileData.getCustomerNumber());
			}
		}
	}


	/**
	 * Erase info of the prepared or applied cash and miles 
	 * 
	 * @param ctx
	 */
	public void clearCashAndMiles(BookingSessionContext ctx){
		if (ctx.cashAndMiles!= null && ctx.isCashMilesReady){
			ctx.cashAndMiles = null;
			ctx.isCashMilesReady = false;
			ctx.selectionRoutes.setCashAndMiles(null);
			for (int i = 0; i < ctx.selectionRoutesPassengersWithoutCoupon.size(); i++) {
				PassengerBaseData sourcePassenger = ctx.selectionRoutesPassengersWithoutCoupon.get(i);
				PassengerBaseData targetPassenger = ctx.selectionRoutes.getPassengers().get(i);
				targetPassenger.setCouponPrice(sourcePassenger.getCouponPrice());
				targetPassenger.setExtraCharge(sourcePassenger.getExtraCharge());
				targetPassenger.setFee(sourcePassenger.getFee());
				targetPassenger.setGrossFare(sourcePassenger.getGrossFare());
				targetPassenger.setNetFare(sourcePassenger.getNetFare());
			}
		}
	}

	/**
	 * It add the discounted miles to passenger if the payment fails
	 * @param ctx
	 */
	public void cancelCashAndMilesForPassenger(BookingSessionContext ctx){

		if (ctx.isCashMilesApplied && ctx.cashAndMiles != null) {
			String tid = IDFactory.getTid();
			ctx.cashAndMiles.setPaymentDate(Calendar.getInstance());

			UpdateWidgetCashMilesRequest cmRequest = new UpdateWidgetCashMilesRequest(tid, ctx.sid);
			cmRequest.setCookie(ctx.cookie);
			cmRequest.setExecution(ctx.execution);
			cmRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
			cmRequest.setOperationType(CashMilesOperationType.ADD);
			cmRequest.setPnr(ctx.prenotation.getPnr());
			cmRequest.setSite(ctx.site);
			cmRequest.setCashMiles(ctx.cashAndMiles);
			UpdateWidgetCashMilesResponse cmResponse = loadWidgetDelegate.updateWidgetCashMiles(cmRequest);

			if (cmResponse == null || !cmResponse.isSucceded()) {
				logger.debug("Fail adding Miles to passenger");
			}

			ctx.isCashMilesApplied = false;
			ctx.cookie = cmResponse.getCookie();
			ctx.execution = cmResponse.getExecute();
			ctx.sabreGateWayAuthToken = cmResponse.getSabreGateWayAuthToken();
		}
	}


	/**
	 * Confirms the ancillary data, creates the new prenotation and save it
	 * in the booking context object variables:
	 * <ul>
	 * <li><code>prenotation</code></li>
	 * </ul>
	 * </p>
	 * 
	 * <p>The phase is also advanced to payment.</p>
	 * 
	 * @param ctx The booking session context to use.
	 * @param passengersData The data of the passengers.
	 */
	public void confirmAncillaryData(BookingSessionContext ctx) {
		logger.debug("confirmAncillaryData");
		if (ctx.phase != BookingPhaseEnum.ANCILLARY) {
			logger.error("confirmAncillaryData not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}

		String tid = IDFactory.getTid();

		ctx.selectionRoutes.setTradeAgencyCode(ctx.tradeAgencyContractCode);

		// create the prenotation
		InfoPassengerCreatePNRRequest request = new InfoPassengerCreatePNRRequest(tid, ctx.sid);
		request.setCookie(ctx.cookie);
		request.setExecution(ctx.execution);
		request.setCug(ctx.cug!=null ? ctx.cug.value() : null);
		request.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
		request.setMarketCode(ctx.market);
		request.setPrenotation(ctx.selectionRoutes);
		addExtraFieldsForCarnet(request,ctx);
		InfoPassengerCreatePNRResponse response = infoPassengerDelegate.createPNRRequest(request);
		ctx.cookie = response.getCookie();
		ctx.execution = response.getExecute();
		ctx.sabreGateWayAuthToken = response.getSabreGateWayAuthToken();

		if (response == null || response.getPrenotation() == null) {
			throw new RuntimeException("Cannot create prenotation (null response or prenotation)");
		}

		ctx.prenotation = response.getPrenotation();
		checkIfCcFeeAreApplied(ctx);
		ctx.phase = BookingPhaseEnum.PAYMENT;
	}

	private void addExtraFieldsForCarnet(InfoPassengerCreatePNRRequest request, BookingSessionContext ctx) {
		if (ctx.isCarnetProcess) {

			RoutesData prenotation = request.getPrenotation();

			BigDecimal netAmount = new BigDecimal(0);
			int count=0;
			if (prenotation.getPassengers() != null) {
				for (PassengerBaseData passenger : prenotation.getPassengers()) {
					netAmount = netAmount.add(passenger.getGrossFare());
					count++;
				}
			}

			BigDecimal copuonPrice = netAmount.divide(new BigDecimal(count));
			if (prenotation.getPassengers() != null) {
				for (PassengerBaseData passenger : prenotation.getPassengers()) {
					passenger.setCouponPrice(copuonPrice);
					passenger.setExtraCharge(new BigDecimal(0));
					passenger.setFee(new BigDecimal(0));
				}
			}

			ECouponData couponCarnet = new ECouponData();
			couponCarnet.setAmount(ctx.infoCarnet.getResidualFare());
			couponCarnet.setCode(ctx.infoCarnet.geteCouponCode());
			couponCarnet.setErrorCode(0);
			couponCarnet.setFamilyEcoupon("Carnet");
			couponCarnet.setFare(FareTypeEnum.FARE);
			couponCarnet.setSingleUse(false);
			couponCarnet.setValid(true);
			couponCarnet.setType(DiscountTypeEnum.SCALAR_AMOUNT);
			couponCarnet.setPaxTypes(new ArrayList<PassengerTypeEnum>());
			couponCarnet.getPaxTypes().add(PassengerTypeEnum.ADULT);
			prenotation.setCoupon(couponCarnet);
			request.setCarnetCode(ctx.infoCarnet.getCarnetCode());
		} else {
			request.setCarnetCode(null);
		}

	}

	private void checkIfCcFeeAreApplied(BookingSessionContext ctx) {
		ctx.ccFeeTotalAmount = getCcFeeTotalAmount(ctx);
		if (ctx.ccFeeTotalAmount.doubleValue() > 0) {
			ctx.ccFeeApplied = true;
		}

	}


	/**
	 * Prepare payment with credit card
	 *
	 * @param ctx
	 * @param creditCardNumber
	 * @param creditCardCVV
	 * @param creditCardExpiryMonth
	 * @param creditCardExpiryYear
	 * @param creditCardType
	 * @param creditCardName
	 * @param creditCardLastName
	 * @param billingCAP
	 * @param billingCfPax
	 * @param billingEmailInt
	 * @param billingFormaSocietaria
	 * @param billingIndSped
	 * @param billingLocSped
	 * @param billingPaese
	 * @param billingProv
	 * @param billingName
	 * @param billingSurname
	 * @param billingIntFattura
	 * @param zip
	 * @param city
	 * @param country
	 * @param address
	 * @param state
	 * @param ipAddress
	 * @param userAgent
	 * @param errorUrl
	 * @param returnUrl
	 * @param i18n 
	 */
	public void preparePaymentWithCreditCard(BookingSessionContext ctx,
			String creditCardNumber, String creditCardCVV, Short creditCardExpiryMonth,
			Short creditCardExpiryYear, String creditCardType, String creditCardName,
			String creditCardLastName, boolean memorizzaCarta, String billingCAP, String billingCfPax,
			String billingEmailInt, String billingFormaSocietaria, String billingIndSped,
			String billingLocSped, String billingPaese, String billingProv, String billingName,
			String billingSurname, String billingIntFattura, String zip, String city,
			String country, String address, String state, String ipAddress, String userAgent,
			String errorUrl, String returnUrl, I18n i18n) {
		logger.debug("preparePaymentWithCreditCard");

		// check phase
		if (ctx.phase != BookingPhaseEnum.PAYMENT) {
			logger.error("preparePaymentWithCreditCard not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}

		//add ccFee to GrossAmount if market include it and it's not been added yet
		if (ctx.ccFeeApplied && !ctx.ccFeeAddedToGrossAmount) {
			ctx.grossAmount = ctx.grossAmount.add(ctx.ccFeeTotalAmount);
			ctx.ccFeeAddedToGrossAmount = true;
		}

		// set billing data
		preparePaymentSetBillingData(ctx, billingCAP, billingCfPax, billingEmailInt,
				billingFormaSocietaria, billingIndSped, billingLocSped, billingPaese,
				billingProv, billingName, billingSurname, billingIntFattura);

		// set payment data
		preparePaymentSetPaymentDataWithCreditCard(ctx, creditCardNumber, creditCardCVV,
				creditCardExpiryMonth, creditCardExpiryYear, creditCardType, creditCardName,
				creditCardLastName, creditCardType, memorizzaCarta, zip, city, country, address, state,
				ipAddress, userAgent, errorUrl, returnUrl, i18n);

		if (memorizzaCarta){
			// set millemiglia customer data
			preparePaymentSetMMCustomerData(ctx);
		}

	}

	public void preparePaymentWithOneClick(BookingSessionContext ctx, String creditCardCVV, 
			String creditCardName, String creditCardLastName, String billingCAP, String billingCfPax,
			String billingEmailInt, String billingFormaSocietaria, String billingIndSped,
			String billingLocSped, String billingPaese, String billingProv, String billingName,
			String billingSurname, String billingIntFattura, String zip, String city,
			String country, String address, String state, String ipAddress, String userAgent,
			String errorUrl, String returnUrl, I18n i18n) {
		logger.debug("preparePaymentWithCreditCard");

		// check phase
		if (ctx.phase != BookingPhaseEnum.PAYMENT) {
			logger.error("preparePaymentWithCreditCard not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}

		//add ccFee to GrossAmount if market include it
		if (ctx.ccFeeApplied && !ctx.ccFeeAddedToGrossAmount) {
			ctx.grossAmount = ctx.grossAmount.add(ctx.ccFeeTotalAmount);
			ctx.ccFeeAddedToGrossAmount = true;
		}

		// set billing data
		preparePaymentSetBillingData(ctx, billingCAP, billingCfPax, billingEmailInt,
				billingFormaSocietaria, billingIndSped, billingLocSped, billingPaese,
				billingProv, billingName, billingSurname, billingIntFattura);

		// set payment data
		preparePaymentSetPaymentDataWithOneClick(ctx, creditCardCVV, creditCardName,
				creditCardLastName, zip, city, country, address, state,
				ipAddress, userAgent, errorUrl, returnUrl, i18n);

		// set millemiglia customer data
		preparePaymentSetMMCustomerData(ctx);
	}

	public void prepareZeroPayment(BookingSessionContext ctx, String creditCardCVV, 
			String creditCardName, String creditCardLastName, String billingCAP, String billingCfPax,
			String billingEmailInt, String billingFormaSocietaria, String billingIndSped,
			String billingLocSped, String billingPaese, String billingProv, String billingName,
			String billingSurname, String billingIntFattura, String zip, String city,
			String country, String address, String state, String ipAddress, String userAgent,
			String errorUrl, String returnUrl, I18n i18n) {
		logger.debug("prepareZeroPayment");

		// check phase
		if (ctx.phase != BookingPhaseEnum.PAYMENT) {
			logger.error("prepareZeroPayment not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}

		// set billing data
		preparePaymentSetBillingData(ctx, billingCAP, billingCfPax, billingEmailInt,
				billingFormaSocietaria, billingIndSped, billingLocSped, billingPaese,
				billingProv, billingName, billingSurname, billingIntFattura);

		// set payment data
		preparePaymentSetPaymentDataWithZeroPayment(ctx, creditCardCVV, creditCardName,
				creditCardLastName, zip, city, country, address, state,
				ipAddress, userAgent, errorUrl, returnUrl, i18n);

	}

	/**
	 * Prepare payment with bank transfer 
	 *
	 * @param ctx
	 * @param billingCAP
	 * @param billingCfPax
	 * @param billingEmailInt
	 * @param billingFormaSocietaria
	 * @param billingIndSped
	 * @param billingLocSped
	 * @param billingPaese
	 * @param billingProv
	 * @param billingName
	 * @param billingSurname
	 * @param billingIntFattura
	 * @param BankTansferType
	 * @param ipAddress
	 * @param errorUrl
	 * @param returnUrl
	 * @param cancelUrl
	 * @param i18n 
	 */
	public void preparePaymentWithBankTransfer(BookingSessionContext ctx,
			String billingCAP, String billingCfPax, String billingEmailInt,
			String billingFormaSocietaria, String billingIndSped, String billingLocSped,
			String billingPaese, String billingProv, String billingName, String billingSurname,
			String billingIntFattura, String BankTansferType, String ipAddress,
			String errorUrl, String returnUrl, String cancelUrl, String globalCollectUrl, I18n i18n, String cardType, String numRate) {
		logger.debug("preparePaymentWithBankTransfer");

		// check phase
		if (ctx.phase != BookingPhaseEnum.PAYMENT) {
			logger.error("preparePaymentWithBankTransfer not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}

		// set billing data
		preparePaymentSetBillingData(ctx, billingCAP, billingCfPax, billingEmailInt,
				billingFormaSocietaria, billingIndSped, billingLocSped, billingPaese,
				billingProv, billingName, billingSurname, billingIntFattura);

		// set payment data
		preparePaymentSetPaymentDataWithBankTransfer(ctx, BankTansferType, ipAddress, errorUrl,
				returnUrl, cancelUrl, globalCollectUrl, i18n, cardType, numRate, null);
	}

	public void preparePaymentWithPayLater(BookingSessionContext ctx,
			String billingCAP, String billingCfPax, String billingEmailInt,
			String billingFormaSocietaria, String billingIndSped, String billingLocSped,
			String billingPaese, String billingProv, String billingName, String billingSurname,
			String billingIntFattura, String paymentType, String ipAddress,
			String returnUrl, I18n i18n, List<String> emailForMail) {
		logger.debug("preparePaymentWithPayLater");

		// check phase
		if (ctx.phase != BookingPhaseEnum.PAYMENT) {
			logger.error("preparePaymentWithPayLater not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}

		// set billing data
		preparePaymentSetBillingData(ctx, billingCAP, billingCfPax, billingEmailInt,
				billingFormaSocietaria, billingIndSped, billingLocSped, billingPaese,
				billingProv, billingName, billingSurname, billingIntFattura);

		// set payment data
		preparePaymentSetPaymentDataWithBankTransfer(ctx, paymentType, ipAddress, null,
				returnUrl, null, null, i18n, null, null, emailForMail);
	}

	/*
	 * Prepare payment set billing (invoice) data
	 * 
	 * <p>Note that this method will modify the content of BillingData 
	 * into ctx.prenotation object.</p>
	 */
	private void preparePaymentSetBillingData(BookingSessionContext ctx, String cap, String cfPax,
			String emailInt, String formaSocietaria, String indSped, String locSped, String paese,
			String prov, String name, String surname, String intFattura) {
		logger.debug("preparePaymentSetBillingData");

		// to uppercase the billing field
		if (cfPax != null) {
			cfPax = cfPax.toUpperCase();
		}
		if (emailInt != null) {
			emailInt = emailInt.toUpperCase();
		}
		if (formaSocietaria != null) {
			formaSocietaria = formaSocietaria.toUpperCase();
		}
		if (indSped != null) {
			indSped = indSped.toUpperCase();
		}
		if (locSped != null) {
			locSped = locSped.toUpperCase();
		}
		if (name != null) {
			name = name.toUpperCase();
		}
		if (surname != null) {
			surname = surname.toUpperCase();
		}
		if (prov != null) {
			prov = prov.toUpperCase();
		}
		if (paese != null) {
			paese = paese.toUpperCase();
		}
		if (intFattura != null) {
			intFattura = intFattura.toUpperCase();
		}


		// create billingData and populate it!
		BillingData billingData = new BillingData();
		billingData.setBill(true);
		billingData.setCap(cap);
		billingData.setEmailInt(emailInt);
		billingData.setFormaSocietaria(formaSocietaria);
		billingData.setIndSped(indSped);
		billingData.setLocSped(locSped);
		billingData.setName(name);
		if (name != null && surname != null) {
			billingData.setNome(name + " " + surname);
		} else {
			billingData.setNome(null);
		}
		billingData.setPaese(paese);
		billingData.setProv(prov);
		billingData.setSelectedCountry(paese);
		billingData.setSurname(surname);

		if (InvoiceType.PF.toString().equals(formaSocietaria)) {
			billingData.setCfPax(cfPax);
		} else {
			billingData.setCfInt(cfPax);
			billingData.setIntFattura(intFattura);
		}

		// save billing data on prenotation
		ctx.prenotation.setBilling(billingData);
	}

	private void preparePaymentSetMMCustomerData(BookingSessionContext ctx) {
		logger.debug("preparePaymentSetMMCustomerData");

		MMCustomerProfileData profileData = ctx.loggedUserProfileData;

		MmCustomerData customerData = new MmCustomerData();
		customerData.setBirthDate(profileData.getBirthDate());
		customerData.setCards(new ArrayList<FFCardData>());
		customerData.setCode(profileData.getCustomerNumber());
		customerData.setCustomerPhones(new ArrayList<ContactData>());
		customerData.setEmail(profileData.getEmail());
		if (profileData.getGender()!=null)
			customerData.setGender(GenderTypeEnum.fromValue(profileData.getGender().value()));
		else 
			customerData.setGender(GenderTypeEnum.UNKNOWN);
		customerData.setIgnore(false);
		customerData.setLastName(profileData.getCustomerSurname());
		customerData.setName(profileData.getCustomerName());
		customerData.setMealPreference(new MealData());
		customerData.setMilesBalance(null);
		customerData.setMilesEarned(null);
		customerData.setMilesQualify(null);
		customerData.setPin(profileData.getCustomerPinCode());
		if (profileData.getCustomerTitle()!=null)
			customerData.setTitle(profileData.getCustomerTitle().toString());
		else
			customerData.setTitle(null);
		customerData.setTierCode(null);
		customerData.setSeatPreference(new SeatTypeData());

		ctx.prenotation.setMilleMigliaCustomer(customerData);
	}

	/*
	 * preparePaymentSetPaymentDataWithCreditCard
	 * 
	 * <p>Note that this method will modify the content of PaymentData
	 * and ProviderCreditCardData into prenotation object.</p>
	 */
	private void preparePaymentSetPaymentDataWithCreditCard(BookingSessionContext ctx, 
			String creditCardNumber, String cvv, Short expiryMonth, Short expiryYear, String type,
			String name, String lastName, String creditCardType, boolean memorizzaCarta, String zip, String city,
			String country, String address, String state, String ipAddress, String userAgent,
			String errorUrl, String returnUrl, I18n i18n) {
		logger.debug("preparePaymentSetPaymentDataWithCreditCard");

		// create new paymentData and populate it!
		PaymentData paymentData = new PaymentData();

		// create providerData and populate it!
		PaymentProviderCreditCardData providerCreditCardData = new PaymentProviderCreditCardData();
		providerCreditCardData.setCvv(cvv);
		providerCreditCardData.setCreditCardNumber(creditCardNumber);
		providerCreditCardData.setExpiryMonth(expiryMonth);
		providerCreditCardData.setExpiryYear(expiryYear);
		providerCreditCardData.setIs3DSecure(true);
		providerCreditCardData.setType(CreditCardTypeEnum.fromValue(type));
		providerCreditCardData.setUseOneClick(memorizzaCarta);

		//set payment description
		String description = "Cart containing {PNR:" + ctx.prenotation.getPnr() + "}";

		// create paymentCommunicationCreditCardData
		// and save it into "communication" of providerCreditCardData
		PaymentComunicationCreditCardData paymentComunicationCreditCardData = 
				new PaymentComunicationCreditCardData();
		paymentComunicationCreditCardData.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
		paymentComunicationCreditCardData.setIpAddress(ipAddress);
		paymentComunicationCreditCardData.setUserAgent(userAgent);
		paymentComunicationCreditCardData.setAcceptHeader("*/*");
		paymentComunicationCreditCardData.setDescription(description);
		paymentComunicationCreditCardData.setErrorUrl(errorUrl);
		paymentComunicationCreditCardData.setReturnUrl(returnUrl);
		providerCreditCardData.setComunication(paymentComunicationCreditCardData);

		// create UserInfoData
		// and save it into "userInfo" of providerCreditCardData
		if (!CreditCardTypeEnum.AMERICAN_EXPRESS.equals(
				CreditCardTypeEnum.fromValue(creditCardType))) {
			UserInfoStandardData userInfoStandardData = new UserInfoStandardData();
			userInfoStandardData.setName(name);
			userInfoStandardData.setLastname(lastName);
			providerCreditCardData.setUserInfo(userInfoStandardData);
		} else {
			UserInfoCompleteData userInfoCompleteData = new UserInfoCompleteData();
			userInfoCompleteData.setAdress(address);
			userInfoCompleteData.setCap(zip);
			userInfoCompleteData.setCity(city);
			userInfoCompleteData.setCountry(country);
			userInfoCompleteData.setLastname(lastName);
			userInfoCompleteData.setName(name);
			userInfoCompleteData.setState(state);
			providerCreditCardData.setUserInfo(userInfoCompleteData);
		}

		// save providerCreditCardData into "provider" of paymentData
		paymentData.setProvider(providerCreditCardData);

		// set additional paymentData fields
		paymentData.setCurrency(ctx.currency);
		paymentData.setEmail(ctx.passengersData.getEmail());
		paymentData.setEnabledNewsLetter(false);
		paymentData.setGrossAmount(ctx.grossAmount);
		paymentData.setNetAmount(ctx.netAmountForPayment);
		paymentData.setType(PaymentTypeEnum.CREDIT_CARD);
		paymentData.setDescription(description);

		// create paymentProcessInfoData
		// and save it into "process" of paymentData
		PaymentProcessInfoData process = new PaymentProcessInfoData();
		process.setMarketCode(ctx.market);
		process.setPaRes(null);
		process.setPaymentAttemps(0);
		process.setPointOfSale(null);
		process.setRedirectUrl(null);
		process.setResult(ResultTypeEnum.OK);
		process.setSessionId(null);
		process.setShopId("WB");
		process.setSiteCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
		process.setStep(PaymentStepEnum.INITIALIZE);
		process.setTokenId(null);
		process.setTransactionId(null);
		paymentData.setProcess(process);

		// save payment data on prenotation
		ctx.prenotation.setPayment(paymentData);
	}

	private void preparePaymentSetPaymentDataWithZeroPayment(BookingSessionContext ctx, 
			String cvv, String name, String lastName, String zip, String city,
			String country, String address, String state, String ipAddress, String userAgent,
			String errorUrl, String returnUrl, I18n i18n) {
		logger.debug("preparePaymentSetPaymentDataWithZeroPayment");

		// create new paymentData and populate it!
		PaymentData paymentData = new PaymentData();

		// create providerData and populate it!
		PaymentProviderZeroPaymentData providerZeroPaymentData = new PaymentProviderZeroPaymentData();

		//set payment description
		String description = "Cart containing {PNR:" + ctx.prenotation.getPnr() + "}";

		// create paymentCommunicationCreditCardData
		// and save it into "communication" of providerCreditCardData
		PaymentComunicationCreditCardData paymentComunicationCreditCardData = 
				new PaymentComunicationCreditCardData();
		paymentComunicationCreditCardData.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
		paymentComunicationCreditCardData.setIpAddress(ipAddress);
		paymentComunicationCreditCardData.setUserAgent(userAgent);
		paymentComunicationCreditCardData.setAcceptHeader("*/*");
		paymentComunicationCreditCardData.setDescription(description);
		paymentComunicationCreditCardData.setErrorUrl(errorUrl);
		paymentComunicationCreditCardData.setReturnUrl(returnUrl);
		providerZeroPaymentData.setComunication(paymentComunicationCreditCardData);

		// save providerCreditCardData into "provider" of paymentData
		paymentData.setProvider(providerZeroPaymentData);

		// set additional paymentData fields
		paymentData.setCurrency(ctx.currency);
		paymentData.setEmail(ctx.passengersData.getEmail());
		paymentData.setEnabledNewsLetter(false);
		if (!ctx.isCarnetProcess) {
			paymentData.setGrossAmount(ctx.grossAmount);
		} else {
			paymentData.setGrossAmount(new BigDecimal(0));
		}
		paymentData.setNetAmount(ctx.netAmountForPayment);
		paymentData.setType(PaymentTypeEnum.CREDIT_CARD);
		paymentData.setDescription(description);

		// create paymentProcessInfoData
		// and save it into "process" of paymentData
		PaymentProcessInfoData process = new PaymentProcessInfoData();
		process.setMarketCode(ctx.market);
		process.setPaRes(null);
		process.setPaymentAttemps(0);
		process.setPointOfSale(null);
		process.setRedirectUrl(null);
		process.setResult(ResultTypeEnum.OK);
		process.setSessionId(null);
		process.setShopId("WB");
		process.setSiteCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
		process.setStep(PaymentStepEnum.INITIALIZE);
		process.setTokenId(null);
		process.setTransactionId(null);
		paymentData.setProcess(process);

		// create email to send
		paymentData.setXsltMail("-");
		paymentData.setMailSubject("-");

		// save payment data on prenotation
		ctx.prenotation.setPayment(paymentData);
	}

	private void preparePaymentSetPaymentDataWithOneClick(BookingSessionContext ctx, 
			String cvv, String name, String lastName, String zip, String city,
			String country, String address, String state, String ipAddress, String userAgent,
			String errorUrl, String returnUrl, I18n i18n) {
		logger.debug("preparePaymentSetPaymentDataWithCreditCard");

		MMCreditCardData storedCreditCardData = ctx.loggedUserProfileData.getStoredCreditCard();

		//TODELETE
		//		storedCreditCardData = new MMCreditCardData();
		//		storedCreditCardData.setCircuitCode("Visa");
		//		storedCreditCardData.setNumber("4444333322221111");
		//		storedCreditCardData.setExpireDate(Calendar.getInstance());
		//		storedCreditCardData.setToken("0000771670901210319");
		//ENDTODELETE

		String creditCardType = storedCreditCardData.getCircuitCode();

		// create new paymentData and populate it!
		PaymentData paymentData = new PaymentData();

		// create providerData and populate it!
		PaymentProviderCreditCardData providerCreditCardData = new PaymentProviderCreditCardData();
		providerCreditCardData.setCvv(cvv);
		providerCreditCardData.setCreditCardNumber(storedCreditCardData.getNumber());
		providerCreditCardData.setExpiryMonth(Short.parseShort(CreditCardUtils.fromCalendarToExpirationString(storedCreditCardData.getExpireDate(), "MM")));
		providerCreditCardData.setExpiryYear(Short.parseShort(CreditCardUtils.fromCalendarToExpirationString(storedCreditCardData.getExpireDate(), "YYYY")));
		providerCreditCardData.setIs3DSecure(true);
		providerCreditCardData.setType(CreditCardTypeEnum.fromValue(creditCardType));
		providerCreditCardData.setUseOneClick(true);
		providerCreditCardData.setToken(storedCreditCardData.getToken());

		//set payment description
		String description = "Cart containing {PNR:" + ctx.prenotation.getPnr() + "}";

		// create paymentCommunicationCreditCardData
		// and save it into "communication" of providerCreditCardData
		PaymentComunicationCreditCardData paymentComunicationCreditCardData = 
				new PaymentComunicationCreditCardData();
		paymentComunicationCreditCardData.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
		paymentComunicationCreditCardData.setIpAddress(ipAddress);
		paymentComunicationCreditCardData.setUserAgent(userAgent);
		paymentComunicationCreditCardData.setAcceptHeader("*/*");
		paymentComunicationCreditCardData.setDescription(description);
		paymentComunicationCreditCardData.setErrorUrl(errorUrl);
		paymentComunicationCreditCardData.setReturnUrl(returnUrl);
		providerCreditCardData.setComunication(paymentComunicationCreditCardData);

		// create UserInfoData
		// and save it into "userInfo" of providerCreditCardData
		if (!CreditCardTypeEnum.AMERICAN_EXPRESS.equals(CreditCardTypeEnum.fromValue(creditCardType))) {
			UserInfoStandardData userInfoStandardData = new UserInfoStandardData();
			userInfoStandardData.setName(name);
			userInfoStandardData.setLastname(lastName);
			providerCreditCardData.setUserInfo(userInfoStandardData);
		} else {
			UserInfoCompleteData userInfoCompleteData = new UserInfoCompleteData();
			userInfoCompleteData.setAdress(address);
			userInfoCompleteData.setCap(zip);
			userInfoCompleteData.setCity(city);
			userInfoCompleteData.setCountry(country);
			userInfoCompleteData.setLastname(lastName);
			userInfoCompleteData.setName(name);
			userInfoCompleteData.setState(state);
			providerCreditCardData.setUserInfo(userInfoCompleteData);
		}

		// save providerCreditCardData into "provider" of paymentData
		paymentData.setProvider(providerCreditCardData);

		// set additional paymentData fields
		paymentData.setCurrency(ctx.currency);
		paymentData.setEmail(ctx.passengersData.getEmail());
		paymentData.setEnabledNewsLetter(false);
		paymentData.setGrossAmount(ctx.grossAmount);
		paymentData.setNetAmount(ctx.netAmountForPayment);
		paymentData.setType(PaymentTypeEnum.CREDIT_CARD);
		paymentData.setDescription(description);

		// create paymentProcessInfoData
		// and save it into "process" of paymentData
		PaymentProcessInfoData process = new PaymentProcessInfoData();
		process.setMarketCode(ctx.market);
		process.setPaRes(null);
		process.setPaymentAttemps(0);
		process.setPointOfSale(null);
		process.setRedirectUrl(null);
		process.setResult(ResultTypeEnum.OK);
		process.setSessionId(null);
		process.setShopId("WB");
		process.setSiteCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
		process.setStep(PaymentStepEnum.INITIALIZE);
		process.setTokenId(null);
		process.setTransactionId(null);
		paymentData.setProcess(process);

		// create email to send
		paymentData.setXsltMail("-");
		paymentData.setMailSubject("-");

		// save payment data on prenotation
		ctx.prenotation.setPayment(paymentData);
	}

	/*
	 * preparePaymentSetPaymentDataWithBankTransfer
	 * 
	 * <p>Note that this method will modify the content of PaymentData
	 * and ProviderCreditCardData into ctx.prenotation object.</p>
	 */
	private void preparePaymentSetPaymentDataWithBankTransfer(BookingSessionContext ctx,
			String type, String ipAddress, String errorUrl, String returnUrl, String cancelUrl,
			String globalCollectUrl, I18n i18n, String cardType, String numRate, List<String> emailForMail) {
		logger.debug("preparePaymentSetPaymentDataWithBankTransfer. Type: "+type);

		// create paymentData and populate it!
		PaymentData paymentData = new PaymentData();
		String currency = ctx.currency;
		paymentData.setCurrency(currency);
		String description = 
				"Cart containing {PNR:" + ctx.prenotation.getPnr() + "}";
		paymentData.setDescription(description);
		paymentData.setEmail(ctx.passengersData.getEmail());
		paymentData.setEnabledNewsLetter(false);
		paymentData.setGrossAmount(ctx.grossAmount);
		paymentData.setNetAmount(ctx.netAmountForPayment);

		// set provider and type into paymentData
		paymentData.setProvider(new PaymentProviderData());
		String language = AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase();
		switch (PaymentTypeEnum.fromValue(type)) {
		case BANCA_INTESA :
			preparePaymentSetProviderGlobalCollect(paymentData, language, currency, ipAddress,
					description, returnUrl, errorUrl, GlobalCollectPaymentTypeEnum.SOFORT);
			paymentData.setType(PaymentTypeEnum.GLOBAL_COLLECT);
			break;

		case BANCO_POSTA :
			preparePaymentSetProviderBancoPosta(paymentData, language, description, errorUrl,
					returnUrl);
			paymentData.setType(PaymentTypeEnum.BANCO_POSTA);
			break;

		case FINDOMESTIC :
			preparePaymentSetProviderFindomestic(ctx, paymentData, language, description, returnUrl,
					returnUrl, returnUrl);
			paymentData.setType(PaymentTypeEnum.FINDOMESTIC);
			break;

		case GLOBAL_COLLECT :
			preparePaymentSetProviderGlobalCollect(paymentData, language, currency, ipAddress,
					description, returnUrl, errorUrl, GlobalCollectPaymentTypeEnum.SOFORT);
			paymentData.setType(PaymentTypeEnum.GLOBAL_COLLECT);
			break;

		//case MAESTRO :
			//	preparePaymentSetProviderGlobalCollect(paymentData, language, currency, ipAddress,
			//		description, returnUrl, errorUrl, GlobalCollectPaymentTypeEnum.MAESTRO);
			//paymentData.setType(PaymentTypeEnum.GLOBAL_COLLECT);
			//break;

		case DISCOVER :
			preparePaymentSetProviderDiscover(paymentData, language, currency, ipAddress,
					description, returnUrl, errorUrl);
			paymentData.setType(PaymentTypeEnum.GLOBAL_COLLECT);
			break;

		case CARTE_BLEUE :
			preparePaymentSetProviderCarteBleue(paymentData, language, currency, ipAddress,
					description, returnUrl, errorUrl);
			paymentData.setType(PaymentTypeEnum.GLOBAL_COLLECT);
			break;

		case JCB :
			preparePaymentSetProviderJCB(paymentData, language, currency, ipAddress,
					description, returnUrl, errorUrl);
			paymentData.setType(PaymentTypeEnum.GLOBAL_COLLECT);
			break;

		case YANDEX :
			preparePaymentSetProviderYandex(paymentData, language, currency, ipAddress,
					description, returnUrl, errorUrl);
			paymentData.setType(PaymentTypeEnum.GLOBAL_COLLECT);
			break;

		case WEB_MONEY :
			preparePaymentSetProviderWebMoney(paymentData, language, currency, ipAddress,
					description, returnUrl, errorUrl);
			paymentData.setType(PaymentTypeEnum.GLOBAL_COLLECT);
			break;

		case IDEAL :
			preparePaymentSetProviderIdeal(paymentData, language, currency, ipAddress,
					description, returnUrl, errorUrl);
			paymentData.setType(PaymentTypeEnum.GLOBAL_COLLECT);
			break;

		case INSTALMENTS_BRAZIL:
			preparePaymentSetProviderInstalmentsBrazil(paymentData, language, description,
					returnUrl, cardType, numRate, currency, ipAddress);
			paymentData.setType(PaymentTypeEnum.GLOBAL_COLLECT);
			break;

		case LOTTOMATICA:
			preparePaymentSetProviderLottomatica(paymentData, language, description, returnUrl);
			paymentData.setType(PaymentTypeEnum.LOTTOMATICA);
			break;

		case MASTER_PASS:
			preparePaymentSetProviderMasterPass(paymentData, language, description, returnUrl, cancelUrl);
			paymentData.setType(PaymentTypeEnum.MASTER_PASS);
			break;

		case PAY_AT_TO:
			preparePaymentSetProviderPayAtTO(paymentData, language, description, returnUrl, emailForMail);
			paymentData.setType(PaymentTypeEnum.PAY_AT_TO);
			break;

		case PAY_PAL:
			String descriptionDeparturePayPal = "";
			FlightData departureFlight = ctx.prenotation.getRoutesList().get(0).getFlights().get(0);
			if (departureFlight instanceof DirectFlightData)
			{
				descriptionDeparturePayPal = String.format("%1$s: %2$s %3$tY-%3$tm-%3$td %3$tR %4$s-%5$s ",
						i18n.get("booking.flightSelect.andata.label"),
						((DirectFlightData) departureFlight).getFlightNumber(),
						((DirectFlightData) departureFlight).getDepartureDate(),
						((DirectFlightData) departureFlight).getFrom().getCode(),
						((DirectFlightData) departureFlight).getTo().getCode());
			}
			else
			{
				int lastDepartureFlight = ((ConnectingFlightData) departureFlight).getFlights().size() - 1;
				descriptionDeparturePayPal = String.format("%1$s: %2$s %3$tY-%3$tm-%3$td %3$tR %4$s-%5$s ", 
						i18n.get("booking.flightSelect.andata.label"),
						((DirectFlightData) 
								((ConnectingFlightData) departureFlight).getFlights().get(0)).getFlightNumber(),
								((DirectFlightData) 
										((ConnectingFlightData) departureFlight).getFlights().get(0)).getDepartureDate(),
										((DirectFlightData) 
												((ConnectingFlightData) departureFlight).getFlights().get(0)).getFrom().getCode(),
												((DirectFlightData) 
														((ConnectingFlightData) departureFlight).getFlights().get(lastDepartureFlight)).getTo().getCode());
			}
			String descriptionReturnPayPal = "";
			int lastFlight = ctx.prenotation.getRoutesList().size() - 1;
			if (lastFlight > 0)
			{
				FlightData returnFlight = ctx.prenotation.getRoutesList().get(lastFlight).getFlights().get(0);
				if (returnFlight instanceof DirectFlightData)
				{
					descriptionReturnPayPal = String.format("%1$s: %2$s %3$tY-%3$tm-%3$td %3$tR %4$s-%5$s ", 
							i18n.get("booking.flightSelect.ritorno.label"),
							((DirectFlightData) returnFlight).getFlightNumber(),
							((DirectFlightData) returnFlight).getDepartureDate(),
							((DirectFlightData) returnFlight).getFrom().getCode(),
							((DirectFlightData) returnFlight).getTo().getCode());
				}
				else
				{
					int lastReturnFlight = ((ConnectingFlightData) returnFlight).getFlights().size() - 1;
					descriptionReturnPayPal = String.format("%1$s: %2$s %3$tY-%3$tm-%3$td %3$tR %4$s-%5$s ", 
							i18n.get("booking.flightSelect.ritorno.label"),
							((DirectFlightData) 
									((ConnectingFlightData) returnFlight).getFlights().get(0)).getFlightNumber(),
									((DirectFlightData) 
											((ConnectingFlightData) returnFlight).getFlights().get(0)).getDepartureDate(),
											((DirectFlightData) 
													((ConnectingFlightData) returnFlight).getFlights().get(0)).getFrom().getCode(),
													((DirectFlightData) 
															((ConnectingFlightData) returnFlight).getFlights().get(lastReturnFlight)).getTo().getCode());
				}
			}

			String descriptionPayPal = 
					String.format("%s%s- PNR:%s",
							descriptionDeparturePayPal, 
							descriptionReturnPayPal, 
							ctx.prenotation.getPnr());

			preparePaymentSetProviderPayPal(paymentData, language, descriptionPayPal, returnUrl, cancelUrl);
			paymentData.setType(PaymentTypeEnum.PAY_PAL);
			break;

		case POSTE_ID:
			preparePaymentSetProviderPosteId(paymentData, language, description, returnUrl, errorUrl);
			paymentData.setType(PaymentTypeEnum.POSTE_ID);
			break;

		case UNICREDIT:
			preparePaymentSetProviderGlobalCollect(paymentData, language, currency, ipAddress,
					description, returnUrl, errorUrl, GlobalCollectPaymentTypeEnum.SOFORT);
			paymentData.setType(PaymentTypeEnum.GLOBAL_COLLECT);
			break;
		default:
			break;
		}

		// create paymentProcessInfoData
		// and save it into "process" of paymentData
		PaymentProcessInfoData process = new PaymentProcessInfoData();
		process.setMarketCode(ctx.market);
		if (PaymentTypeEnum.FINDOMESTIC == PaymentTypeEnum.fromValue(type))
			process.setPaRes("AUTHORIZED");
		else
			process.setPaRes(null);
		process.setPaymentAttemps(0);
		process.setPointOfSale(null);
		process.setRedirectUrl(null);
		process.setResult(ResultTypeEnum.OK);
		process.setSessionId(null);
		process.setShopId("WB");
		process.setSiteCode(language);
		process.setStep(PaymentStepEnum.INITIALIZE);
		process.setTokenId(null);
		process.setTransactionId(null);
		paymentData.setProcess(process);

		// create email to send
		paymentData.setXsltMail("-");
		paymentData.setMailSubject("-");

		// save payment data on prenotation
		ctx.prenotation.setPayment(paymentData);
	}

	/*
	 * preparePaymentSetProviderBancaIntesa
	 */
	private void preparePaymentSetProviderBancaIntesa(PaymentData paymentData,
			String language, String description, String errorUrl, String returnUrl,
			String cancelUrl) {
		logger.debug("preparePaymentSetProviderBancaIntesa");

		// retrieve paymentProvider object
		PaymentProviderBancaIntesaData paymentProviderBancaIntesaData = 
				new PaymentProviderBancaIntesaData();

		// create specific communication object and populate it
		PaymentComunicationBancaIntesaData paymentComunicationBancaIntesaData = 
				new PaymentComunicationBancaIntesaData();
		paymentComunicationBancaIntesaData.setDescription(description);
		paymentComunicationBancaIntesaData.setLanguageCode(language);
		paymentComunicationBancaIntesaData.setRequestData(null);
		paymentComunicationBancaIntesaData.setShopperId("");
		paymentComunicationBancaIntesaData.setCancelUrl(cancelUrl);
		paymentComunicationBancaIntesaData.setErrorUrl(errorUrl);
		paymentComunicationBancaIntesaData.setReturnUrl(returnUrl);
		paymentProviderBancaIntesaData.setComunication(paymentComunicationBancaIntesaData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderBancaIntesaData);
	}

	/*
	 * preparePaymentSetProviderBancoPosta
	 */
	private void preparePaymentSetProviderBancoPosta(PaymentData paymentData,
			String language, String description, String errorUrl, String returnUrl) {
		logger.debug("preparePaymentSetProviderBancoPosta");

		// retrieve paymentProvider object
		PaymentProviderBancoPostaData paymentProviderBancoPostaData =
				new PaymentProviderBancoPostaData();

		// create specific communication object and populate it
		PaymentComunicationBancoPostaData paymentComunicationBancoPostaData = 
				new PaymentComunicationBancoPostaData();
		paymentComunicationBancoPostaData.setDescription(description);
		paymentComunicationBancoPostaData.setLanguageCode(language);
		paymentComunicationBancoPostaData.setErrorUrl(errorUrl);
		paymentComunicationBancoPostaData.setReturnUrl(returnUrl);
		BancoPostaTypeEnum providerType = BancoPostaTypeEnum.BPOL;
		paymentComunicationBancoPostaData.setType(providerType);
		paymentProviderBancoPostaData.setComunication(paymentComunicationBancoPostaData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderBancoPostaData);
	}

	/*
	 * preparePaymentSetProviderFindomestic
	 */
	private void preparePaymentSetProviderFindomestic(BookingSessionContext ctx, PaymentData paymentData,
			String language, String description, String returnUrl, String urlCallBack,
			String urlRedirect) {
		logger.debug("preparePaymentSetProviderFindomestic");

		// retrieve paymentProvider object
		PaymentProviderFindomesticData paymentProviderFindomesticData = new PaymentProviderFindomesticData();

		// create specific communication object and populate it
		PaymentComunicationFindomesticData paymentComunicationFindomesticData =
				new PaymentComunicationFindomesticData();
		paymentComunicationFindomesticData.setDescription(description);
		paymentComunicationFindomesticData.setIsPost(false);
		paymentComunicationFindomesticData.setLanguageCode(language);
		paymentComunicationFindomesticData.setRequestData(null);
		paymentComunicationFindomesticData.setReturnUrl(returnUrl);
		paymentComunicationFindomesticData.setUrlCallBack(urlCallBack);
		paymentComunicationFindomesticData.setUrlRedirect(urlRedirect);

		paymentProviderFindomesticData.setComunication(paymentComunicationFindomesticData);

		UserInfoStandardData userInfoStandardData = new UserInfoStandardData();

		ApplicantPassengerData applicantPassengerData = new ApplicantPassengerData();
		List<PassengerBaseData> passengers = ctx.selectionRoutes.getPassengers();
		if (passengers != null){
			for (PassengerBaseData passenger : passengers){
				if (passenger instanceof ApplicantPassengerData){
					applicantPassengerData = (ApplicantPassengerData)passenger;
					break;
				}
			}
		}
		//		String name =  ? ctx.prenotation.getBilling().getName() : null;
		//		String lastName = ctx.prenotation.getBilling() != null ? ctx.prenotation.getBilling().getSurname() : null;
		//		if (name == null && ctx.loggedUserProfileData != null)
		//			name = ctx.loggedUserProfileData.getCustomerName();
		//		if (lastName == null && ctx.loggedUserProfileData != null)
		//			lastName = ctx.loggedUserProfileData.getCustomerSurname();
		userInfoStandardData.setName(applicantPassengerData.getName());
		userInfoStandardData.setLastname(applicantPassengerData.getLastName());
		paymentProviderFindomesticData.setUserInfo(userInfoStandardData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderFindomesticData);
	}

	/*
	 * preparePaymentSetProviderGlobalCollet
	 */
	private void preparePaymentSetProviderGlobalCollect(PaymentData paymentData,
			String language, String currency, String ipAddress, String description,
			String returnUrl, String errorUrl, GlobalCollectPaymentTypeEnum globalCollectType) {
		logger.debug("preparePaymentSetProviderGlobalCollet");

		// retrieve paymentProvider object
		PaymentProviderGlobalCollectData paymentProviderGlobalCollectData =
				new PaymentProviderGlobalCollectData();
		paymentProviderGlobalCollectData.setInstallementsNumber(0);
		paymentProviderGlobalCollectData.setType(globalCollectType);

		// create specific communication object and populate it
		PaymentComunicationGlobalCollectData paymentComunicationGlobalCollectData =
				new PaymentComunicationGlobalCollectData();
		paymentComunicationGlobalCollectData.setCurrency(currency);
		paymentComunicationGlobalCollectData.setCustomerId(null);
		paymentComunicationGlobalCollectData.setDescription(description);
		paymentComunicationGlobalCollectData.setIpAddress(ipAddress);
		paymentComunicationGlobalCollectData.setIsPost(false);
		paymentComunicationGlobalCollectData.setLanguageCode(language);
		paymentComunicationGlobalCollectData.setOrderId(null);
		paymentComunicationGlobalCollectData.setRequestData(null);
		paymentComunicationGlobalCollectData.setReturnUrl(returnUrl);
		paymentComunicationGlobalCollectData.setErrorUrl(errorUrl);
		paymentProviderGlobalCollectData.setComunication(paymentComunicationGlobalCollectData);

		// create userInfoFullData object and populate it
		UserInfoFullData userInfoFullData = new UserInfoFullData();
		userInfoFullData.setAdress(null);
		userInfoFullData.setBirthDate(null);
		userInfoFullData.setCap(null);
		userInfoFullData.setCity(null);
		userInfoFullData.setCountry(null);
		userInfoFullData.setGender(null);
		userInfoFullData.setHouseNumber(null);
		userInfoFullData.setLastname(null);
		userInfoFullData.setName(null);
		userInfoFullData.setState(null);
		userInfoFullData.setTitle(null);
		userInfoFullData.setVatNumber(null);
		paymentProviderGlobalCollectData.setUserInfo(userInfoFullData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderGlobalCollectData);
	}

	private void preparePaymentSetProviderDiscover(PaymentData paymentData,
			String language, String currency, String ipAddress, String description,
			String returnUrl, String errorUrl) {
		logger.debug("preparePaymentSetProviderDiscover");

		// retrieve paymentProvider object
		PaymentProviderGlobalCollectData paymentProviderGlobalCollectData =
				new PaymentProviderGlobalCollectData();
		paymentProviderGlobalCollectData.setInstallementsNumber(0);
		paymentProviderGlobalCollectData.setType(GlobalCollectPaymentTypeEnum.DISCOVER);

		// create specific communication object and populate it
		PaymentComunicationGlobalCollectData paymentComunicationGlobalCollectData =
				new PaymentComunicationGlobalCollectData();
		paymentComunicationGlobalCollectData.setCurrency(currency);
		paymentComunicationGlobalCollectData.setCustomerId(null);
		paymentComunicationGlobalCollectData.setDescription(description);
		paymentComunicationGlobalCollectData.setIpAddress(ipAddress);
		paymentComunicationGlobalCollectData.setIsPost(false);
		paymentComunicationGlobalCollectData.setLanguageCode(language);
		paymentComunicationGlobalCollectData.setOrderId(null);
		paymentComunicationGlobalCollectData.setRequestData(null);
		paymentComunicationGlobalCollectData.setReturnUrl(returnUrl);
		paymentComunicationGlobalCollectData.setErrorUrl(errorUrl);
		paymentProviderGlobalCollectData.setComunication(paymentComunicationGlobalCollectData);

		// create userInfoFullData object and populate it
		UserInfoFullData userInfoFullData = new UserInfoFullData();
		userInfoFullData.setAdress(null);
		userInfoFullData.setBirthDate(Calendar.getInstance());
		userInfoFullData.setCap(null);
		userInfoFullData.setCity(null);
		userInfoFullData.setCountry(null);
		userInfoFullData.setGender(GenderTypeEnum.UNKNOWN);
		userInfoFullData.setHouseNumber(null);
		userInfoFullData.setLastname(null);
		userInfoFullData.setName(null);
		userInfoFullData.setState(null);
		userInfoFullData.setTitle(null);
		userInfoFullData.setVatNumber(null);
		paymentProviderGlobalCollectData.setUserInfo(userInfoFullData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderGlobalCollectData);
	}

	private void preparePaymentSetProviderCarteBleue(PaymentData paymentData,
			String language, String currency, String ipAddress, String description,
			String returnUrl, String errorUrl) {
		logger.debug("preparePaymentSetProviderCarteBleue");

		// retrieve paymentProvider object
		PaymentProviderGlobalCollectData paymentProviderGlobalCollectData =
				new PaymentProviderGlobalCollectData();
		paymentProviderGlobalCollectData.setInstallementsNumber(0);
		paymentProviderGlobalCollectData.setType(GlobalCollectPaymentTypeEnum.CARTE_BLEUE);

		// create specific communication object and populate it
		PaymentComunicationGlobalCollectData paymentComunicationGlobalCollectData =
				new PaymentComunicationGlobalCollectData();
		paymentComunicationGlobalCollectData.setCurrency(currency);
		paymentComunicationGlobalCollectData.setCustomerId(null);
		paymentComunicationGlobalCollectData.setDescription(description);
		paymentComunicationGlobalCollectData.setIpAddress(ipAddress);
		paymentComunicationGlobalCollectData.setIsPost(false);
		paymentComunicationGlobalCollectData.setLanguageCode(language);
		paymentComunicationGlobalCollectData.setOrderId(null);
		paymentComunicationGlobalCollectData.setRequestData(null);
		paymentComunicationGlobalCollectData.setReturnUrl(returnUrl);
		paymentComunicationGlobalCollectData.setErrorUrl(errorUrl);
		paymentProviderGlobalCollectData.setComunication(paymentComunicationGlobalCollectData);

		// create userInfoFullData object and populate it
		UserInfoFullData userInfoFullData = new UserInfoFullData();
		userInfoFullData.setAdress(null);
		userInfoFullData.setBirthDate(Calendar.getInstance());
		userInfoFullData.setCap(null);
		userInfoFullData.setCity(null);
		userInfoFullData.setCountry(null);
		userInfoFullData.setGender(GenderTypeEnum.UNKNOWN);
		userInfoFullData.setHouseNumber(null);
		userInfoFullData.setLastname(null);
		userInfoFullData.setName(null);
		userInfoFullData.setState(null);
		userInfoFullData.setTitle(null);
		userInfoFullData.setVatNumber(null);
		paymentProviderGlobalCollectData.setUserInfo(userInfoFullData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderGlobalCollectData);
	}

	private void preparePaymentSetProviderJCB(PaymentData paymentData,
			String language, String currency, String ipAddress, String description,
			String returnUrl, String errorUrl) {
		logger.debug("preparePaymentSetProviderJCB");

		// retrieve paymentProvider object
		PaymentProviderGlobalCollectData paymentProviderGlobalCollectData =
				new PaymentProviderGlobalCollectData();
		paymentProviderGlobalCollectData.setInstallementsNumber(0);
		paymentProviderGlobalCollectData.setType(GlobalCollectPaymentTypeEnum.JCB);

		// create specific communication object and populate it
		PaymentComunicationGlobalCollectData paymentComunicationGlobalCollectData =
				new PaymentComunicationGlobalCollectData();
		paymentComunicationGlobalCollectData.setCurrency(currency);
		paymentComunicationGlobalCollectData.setCustomerId(null);
		paymentComunicationGlobalCollectData.setDescription(description);
		paymentComunicationGlobalCollectData.setIpAddress(ipAddress);
		paymentComunicationGlobalCollectData.setIsPost(false);
		paymentComunicationGlobalCollectData.setLanguageCode(language);
		paymentComunicationGlobalCollectData.setOrderId(null);
		paymentComunicationGlobalCollectData.setRequestData(null);
		paymentComunicationGlobalCollectData.setReturnUrl(returnUrl);
		paymentComunicationGlobalCollectData.setErrorUrl(errorUrl);
		paymentProviderGlobalCollectData.setComunication(paymentComunicationGlobalCollectData);

		// create userInfoFullData object and populate it
		UserInfoFullData userInfoFullData = new UserInfoFullData();
		userInfoFullData.setAdress(null);
		userInfoFullData.setBirthDate(Calendar.getInstance());
		userInfoFullData.setCap(null);
		userInfoFullData.setCity(null);
		userInfoFullData.setCountry(null);
		userInfoFullData.setGender(GenderTypeEnum.UNKNOWN);
		userInfoFullData.setHouseNumber(null);
		userInfoFullData.setLastname(null);
		userInfoFullData.setName(null);
		userInfoFullData.setState(null);
		userInfoFullData.setTitle(null);
		userInfoFullData.setVatNumber(null);
		paymentProviderGlobalCollectData.setUserInfo(userInfoFullData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderGlobalCollectData);
	}

	private void preparePaymentSetProviderYandex(PaymentData paymentData,
			String language, String currency, String ipAddress, String description,
			String returnUrl, String errorUrl) {
		logger.debug("preparePaymentSetProviderCarteBleue");

		// retrieve paymentProvider object
		PaymentProviderGlobalCollectData paymentProviderGlobalCollectData =
				new PaymentProviderGlobalCollectData();
		paymentProviderGlobalCollectData.setInstallementsNumber(0);
		paymentProviderGlobalCollectData.setType(GlobalCollectPaymentTypeEnum.YANDEX);

		// create specific communication object and populate it
		PaymentComunicationGlobalCollectData paymentComunicationGlobalCollectData =
				new PaymentComunicationGlobalCollectData();
		paymentComunicationGlobalCollectData.setCurrency(currency);
		paymentComunicationGlobalCollectData.setCustomerId(null);
		paymentComunicationGlobalCollectData.setDescription(description);
		paymentComunicationGlobalCollectData.setIpAddress(ipAddress);
		paymentComunicationGlobalCollectData.setIsPost(false);
		paymentComunicationGlobalCollectData.setLanguageCode(language);
		paymentComunicationGlobalCollectData.setOrderId(null);
		paymentComunicationGlobalCollectData.setRequestData(null);
		paymentComunicationGlobalCollectData.setReturnUrl(returnUrl);
		paymentComunicationGlobalCollectData.setErrorUrl(errorUrl);
		paymentProviderGlobalCollectData.setComunication(paymentComunicationGlobalCollectData);

		// create userInfoFullData object and populate it
		UserInfoFullData userInfoFullData = new UserInfoFullData();
		userInfoFullData.setAdress(null);
		userInfoFullData.setBirthDate(Calendar.getInstance());
		userInfoFullData.setCap(null);
		userInfoFullData.setCity(null);
		userInfoFullData.setCountry(null);
		userInfoFullData.setGender(GenderTypeEnum.UNKNOWN);
		userInfoFullData.setHouseNumber(null);
		userInfoFullData.setLastname(null);
		userInfoFullData.setName(null);
		userInfoFullData.setState(null);
		userInfoFullData.setTitle(null);
		userInfoFullData.setVatNumber(null);
		paymentProviderGlobalCollectData.setUserInfo(userInfoFullData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderGlobalCollectData);
	}

	private void preparePaymentSetProviderWebMoney(PaymentData paymentData,
			String language, String currency, String ipAddress, String description,
			String returnUrl, String errorUrl) {
		logger.debug("preparePaymentSetProviderCarteBleue");

		// retrieve paymentProvider object
		PaymentProviderGlobalCollectData paymentProviderGlobalCollectData =
				new PaymentProviderGlobalCollectData();
		paymentProviderGlobalCollectData.setInstallementsNumber(0);
		paymentProviderGlobalCollectData.setType(GlobalCollectPaymentTypeEnum.WEB_MONEY);

		// create specific communication object and populate it
		PaymentComunicationGlobalCollectData paymentComunicationGlobalCollectData =
				new PaymentComunicationGlobalCollectData();
		paymentComunicationGlobalCollectData.setCurrency(currency);
		paymentComunicationGlobalCollectData.setCustomerId(null);
		paymentComunicationGlobalCollectData.setDescription(description);
		paymentComunicationGlobalCollectData.setIpAddress(ipAddress);
		paymentComunicationGlobalCollectData.setIsPost(false);
		paymentComunicationGlobalCollectData.setLanguageCode(language);
		paymentComunicationGlobalCollectData.setOrderId(null);
		paymentComunicationGlobalCollectData.setRequestData(null);
		paymentComunicationGlobalCollectData.setReturnUrl(returnUrl);
		paymentComunicationGlobalCollectData.setErrorUrl(errorUrl);
		paymentProviderGlobalCollectData.setComunication(paymentComunicationGlobalCollectData);

		// create userInfoFullData object and populate it
		UserInfoFullData userInfoFullData = new UserInfoFullData();
		userInfoFullData.setAdress(null);
		userInfoFullData.setBirthDate(Calendar.getInstance());
		userInfoFullData.setCap(null);
		userInfoFullData.setCity(null);
		userInfoFullData.setCountry(null);
		userInfoFullData.setGender(GenderTypeEnum.UNKNOWN);
		userInfoFullData.setHouseNumber(null);
		userInfoFullData.setLastname(null);
		userInfoFullData.setName(null);
		userInfoFullData.setState(null);
		userInfoFullData.setTitle(null);
		userInfoFullData.setVatNumber(null);
		paymentProviderGlobalCollectData.setUserInfo(userInfoFullData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderGlobalCollectData);
	}

	private void preparePaymentSetProviderIdeal(PaymentData paymentData,
			String language, String currency, String ipAddress, String description,
			String returnUrl, String errorUrl) {
		logger.debug("preparePaymentSetProviderCarteBleue");

		// retrieve paymentProvider object
		PaymentProviderGlobalCollectData paymentProviderGlobalCollectData =
				new PaymentProviderGlobalCollectData();
		paymentProviderGlobalCollectData.setInstallementsNumber(0);
		paymentProviderGlobalCollectData.setType(GlobalCollectPaymentTypeEnum.IDEAL);

		// create specific communication object and populate it
		PaymentComunicationGlobalCollectData paymentComunicationGlobalCollectData =
				new PaymentComunicationGlobalCollectData();
		paymentComunicationGlobalCollectData.setCurrency(currency);
		paymentComunicationGlobalCollectData.setCustomerId(null);
		paymentComunicationGlobalCollectData.setDescription(description);
		paymentComunicationGlobalCollectData.setIpAddress(ipAddress);
		paymentComunicationGlobalCollectData.setIsPost(false);
		paymentComunicationGlobalCollectData.setLanguageCode(language);
		paymentComunicationGlobalCollectData.setOrderId(null);
		paymentComunicationGlobalCollectData.setRequestData(null);
		paymentComunicationGlobalCollectData.setReturnUrl(returnUrl);
		paymentComunicationGlobalCollectData.setErrorUrl(errorUrl);
		paymentProviderGlobalCollectData.setComunication(paymentComunicationGlobalCollectData);

		// create userInfoFullData object and populate it
		UserInfoFullData userInfoFullData = new UserInfoFullData();
		userInfoFullData.setAdress(null);
		userInfoFullData.setBirthDate(Calendar.getInstance());
		userInfoFullData.setCap(null);
		userInfoFullData.setCity(null);
		userInfoFullData.setCountry(null);
		userInfoFullData.setGender(GenderTypeEnum.UNKNOWN);
		userInfoFullData.setHouseNumber(null);
		userInfoFullData.setLastname(null);
		userInfoFullData.setName(null);
		userInfoFullData.setState(null);
		userInfoFullData.setTitle(null);
		userInfoFullData.setVatNumber(null);
		paymentProviderGlobalCollectData.setUserInfo(userInfoFullData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderGlobalCollectData);
	}

	/*
	 * preparePaymentSetProviderInstalmentsBrazil
	 */
	private void preparePaymentSetProviderInstalmentsBrazil(PaymentData paymentData,
			String language, String description, String returnUrl, String cardType, String numRate, 
			String currency, String ipAddress) {
		logger.debug("preparePaymentSetProviderInstalmentsBrazil");

		//		 retrieve paymentProvider object

		PaymentProviderGlobalCollectData paymentProviderGlobalCollectData =
				new PaymentProviderGlobalCollectData();

		//		PaymentProviderInstalmentsBrazilData paymentProviderInstalmentsBrazilData =
		//				new PaymentProviderInstalmentsBrazilData();

		PaymentComunicationGlobalCollectData paymentComunicationBaseData = 
				new PaymentComunicationGlobalCollectData();
		paymentComunicationBaseData.setDescription(description);
		paymentComunicationBaseData.setLanguageCode(language);
		paymentComunicationBaseData.setReturnUrl(returnUrl);
		paymentComunicationBaseData.setCurrency(currency);
		paymentComunicationBaseData.setIpAddress(ipAddress);
		paymentComunicationBaseData.setIsPost(false);
		paymentProviderGlobalCollectData.setComunication(paymentComunicationBaseData);
		GlobalCollectPaymentTypeEnum creditCardTypeEnum = GlobalCollectPaymentTypeEnum.fromValue(cardType);
		paymentProviderGlobalCollectData.setType(creditCardTypeEnum);
		int installementsNumber;
		try{
			installementsNumber = Integer.parseInt(numRate);
		} catch (Exception e){
			installementsNumber = 1;
		}
		paymentProviderGlobalCollectData.setInstallementsNumber(installementsNumber);

		// create userInfoFullData object and populate it
		UserInfoFullData userInfoFullData = new UserInfoFullData();
		userInfoFullData.setAdress(null);
		userInfoFullData.setBirthDate(Calendar.getInstance());
		userInfoFullData.setCap(null);
		userInfoFullData.setCity(null);
		userInfoFullData.setCountry(null);
		userInfoFullData.setGender(GenderTypeEnum.UNKNOWN);
		userInfoFullData.setHouseNumber(null);
		userInfoFullData.setLastname(null);
		userInfoFullData.setName(null);
		userInfoFullData.setState(null);
		userInfoFullData.setTitle(null);
		userInfoFullData.setVatNumber(null);
		paymentProviderGlobalCollectData.setUserInfo(userInfoFullData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderGlobalCollectData);
	}

	/*
	 * preparePaymentSetProviderLottomatica
	 */
	private void preparePaymentSetProviderLottomatica(PaymentData paymentData, 
			String language, String description, String returnUrl) {
		logger.debug("preparePaymentSetProviderLottomatica");

		// retrieve paymentProvider object
		PaymentProviderLottomaticaData paymentProviderLottomaticaData =
				new PaymentProviderLottomaticaData();

		// create specific communication object and populate it
		PaymentComunicationLottomaticaData paymentComunicationLottomaticaData =
				new PaymentComunicationLottomaticaData();
		paymentComunicationLottomaticaData.setDescription(description);
		paymentComunicationLottomaticaData.setLanguageCode(language);
		paymentComunicationLottomaticaData.setReturnUrl(returnUrl);
		paymentProviderLottomaticaData.setComunication(paymentComunicationLottomaticaData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderLottomaticaData);
	}

	/*
	 * preparePaymentSetProviderMasterPass
	 */
	private void preparePaymentSetProviderMasterPass(PaymentData paymentData,
			String language, String description, String returnUrl, String cancelUrl) {
		logger.debug("preparePaymentSetProviderMasterPass");

		// retrieve paymentProvider object
		PaymentProviderMasterPassData paymentProviderMasterPassData =
				new PaymentProviderMasterPassData();

		// create specific communication object and populate it
		PaymentComunicationMasterPassData paymentComunicationMasterPassData =
				new PaymentComunicationMasterPassData();
		paymentComunicationMasterPassData.setDescription(description);
		paymentComunicationMasterPassData.setLanguageCode(language);
		paymentComunicationMasterPassData.setReturnUrl(returnUrl);
		paymentComunicationMasterPassData.setCancelUrl(cancelUrl);
		paymentComunicationMasterPassData.setShippingEnabled(false);
		paymentProviderMasterPassData.setComunication(paymentComunicationMasterPassData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderMasterPassData);
	}

	/*
	 * preparePaymentSetProviderPayAtTo
	 */
	private void preparePaymentSetProviderPayAtTO(PaymentData paymentData,
			String language, String description, String returnUrl, List<String> emailForMail) {
		logger.debug("preparePaymentSetProviderPayAtTo");

		// retrieve paymentProvider object
		PaymentProviderPayAtTOData paymentProviderPayAtTOData =
				new PaymentProviderPayAtTOData();

		PaymentComunicationPayLaterData paymentComunicationPayLaterData = 
				new PaymentComunicationPayLaterData();
		paymentComunicationPayLaterData.setDescription(description);
		paymentComunicationPayLaterData.setLanguageCode(language);
		paymentComunicationPayLaterData.setReturnUrl(returnUrl);
		paymentComunicationPayLaterData.setQueues(null);
		paymentComunicationPayLaterData.setRemarks(null);
		List<String> osis = new ArrayList<String>();
		osis.add("WPAY");
		paymentComunicationPayLaterData.setOsis(osis);
		paymentProviderPayAtTOData.setComunication(paymentComunicationPayLaterData);

		paymentProviderPayAtTOData.setEmailForMail(emailForMail);
		String emailForPnr = "";
		paymentProviderPayAtTOData.setEmailForPnr(emailForPnr);
		ContactData otherContact = null;
		paymentProviderPayAtTOData.setOtherContact(otherContact);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderPayAtTOData);
	}

	/*
	 * preparePaymentSetProviderPayPal
	 */
	private void preparePaymentSetProviderPayPal(PaymentData paymentData,
			String language, String description, String returnUrl, String cancelUrl) {
		logger.debug("preparePaymentSetProviderPayAtTo");

		// retrieve paymentProvider object
		PaymentProviderPayPalData paymentProviderPayPalData =
				new PaymentProviderPayPalData();

		// create specific communication object and populate it
		PaymentComunicationPayPalData paymentComunicationPayPalData =
				new PaymentComunicationPayPalData();
		paymentComunicationPayPalData.setDescription(description);
		paymentComunicationPayPalData.setLanguageCode(language);
		paymentComunicationPayPalData.setReturnUrl(returnUrl);
		paymentComunicationPayPalData.setCancelUrl(cancelUrl);
		paymentComunicationPayPalData.setShippingEnabled(false);
		paymentProviderPayPalData.setComunication(paymentComunicationPayPalData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderPayPalData);
	}

	/*
	 * preparePaymentSetProviderPosteId
	 */
	private void preparePaymentSetProviderPosteId(PaymentData paymentData,
			String language, String description, String returnUrl, String errorUrl) {
		logger.debug("preparePaymentSetProviderPosteId");

		// retrieve paymentProvider object
		PaymentProviderPosteIDData paymentProviderPosteIDData =
				new PaymentProviderPosteIDData();

		// create specific communication object and populate it
		PaymentComunicationPosteIDData paymentComunicationPosteIDData = 
				new PaymentComunicationPosteIDData();
		paymentComunicationPosteIDData.setDescription(description);
		paymentComunicationPosteIDData.setLanguageCode(language);
		paymentComunicationPosteIDData.setErrorUrl(errorUrl);
		paymentComunicationPosteIDData.setReturnUrl(returnUrl);
		paymentProviderPosteIDData.setComunication(paymentComunicationPosteIDData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderPosteIDData);

		// paymentData.setProvider();
	}

	/*
	 * preparePaymentSetProviderUnicredit
	 */
	private void preparePaymentSetProviderUnicredit(PaymentData paymentData,
			String language, String description, String errorUrl, String returnUrl) {
		logger.debug("preparePaymentSetProviderUnicredit");

		PaymentProviderUnicreditData paymentProviderUnicrediData = new PaymentProviderUnicreditData();

		// create specific communication object and populate it
		PaymentComunicationUnicreditData paymentComunicationUnicreditData = 
				new PaymentComunicationUnicreditData();
		paymentComunicationUnicreditData.setDescription(description);
		paymentComunicationUnicreditData.setLanguageCode(language);
		paymentComunicationUnicreditData.setErrorUrl(errorUrl);
		paymentComunicationUnicreditData.setReturnUrl(returnUrl);
		paymentProviderUnicrediData.setComunication(paymentComunicationUnicreditData);

		// replace paymentData provider object
		paymentData.setProvider(paymentProviderUnicrediData);
	}

	/**
	 * Perform a payment: it's a 4-steps process baby!
	 * - Step 1: InitializePayment
	 * - Step 2: AuthorizePayment [will be executed calling completePayment]
	 * - Step 3: CheckPayment [will be executed calling completePayment]
	 * - Step 4: RetrieveTicket [will be executed calling completePayment]
	 * 
	 * <p>Note that this method will modify the content of paymentData 
	 * into ctx object.</p> 
	 * 
	 * @param ctx The booking session context to use.
	 * @param request The HTTP request
	 * @param tid transaction ID
	 * @param type type of payment
	 * @param jscString 
	 * @param httpHeaders 
	 * 
	 * @throws BookingPaymentException
	 */
	public void performPayment(BookingSessionContext ctx, String ipAddress, String userAgent,
			String tid, String type, String nomeBanca, I18n i18n, Map<String, String> httpHeaders, String jscString) throws BookingPaymentException {
		logger.debug("performPayment. type: ["+type+"]");

		// check phase
		if (ctx.phase != BookingPhaseEnum.PAYMENT) {
			logger.error("performPayment not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}

		// step 1: initialize payment
		InitializePaymentResponse initializePaymentResponse = 
				performPaymentInitializePayment(ctx, ipAddress, userAgent, tid, httpHeaders, jscString);

		// check for redirect
		PaymentData paymentData = initializePaymentResponse.getPaymentData();
		PaymentProcessInfoData process = paymentData.getProcess();
		ctx.paymentDataForPDF = paymentData;

		if (paymentData.getPnr()!=null && !paymentData.getPnr().equals("")){ // CREATO PNR
			ctx.prenotation.setPnr(paymentData.getPnr());
			String mdp = ctx.prenotation.getPayment().getProvider() instanceof PaymentProviderGlobalCollectData ? ((PaymentProviderGlobalCollectData)ctx.prenotation.getPayment().getProvider()).getType().value() : ctx.prenotation.getPayment().getType().value();
			logger.info("InitializePayment executed. PNR: {}. MdP: {}", paymentData.getPnr(), mdp);

			//STATISTICA PNR
			try {
				RegisterPNRStatisticRequest pnrStatisticRequest = new RegisterPNRStatisticRequest(tid, ctx.sid);
				pnrStatisticRequest.setClientIP(ctx.clientIP);
				pnrStatisticRequest.setErrorDescr("");
				pnrStatisticRequest.setSessionId(ctx.sessionId);
				pnrStatisticRequest.setSiteCode(ctx.site);
				pnrStatisticRequest.setSuccess(true);
				pnrStatisticRequest.setType(StatisticsDataTypeEnum.PNR);
				pnrStatisticRequest.setPnr(paymentData.getPnr());
				pnrStatisticRequest.setTravelInfo(obtainApplicantNameForStatistics(ctx));
				pnrStatisticRequest.setAward(false);

				RegisterStatisticsResponse pnrStatisticResponse = registerStatisticsDelegate.registerStatistics(pnrStatisticRequest);
				if (pnrStatisticResponse == null) {
					logger.error("Error while executing PNR Statistic");
				}
			} catch (Exception e) {
				logger.error("Error while executing PNR Statistic");
			}

			if (PaymentType.CDC.toString().equals(type)){ //AUTH ATTEMPT STAT IN CASO CDC
				try {
					RegisterPaymentAuthAttemptStatisticRequest paymentAuthorizeStatisticRequest = new RegisterPaymentAuthAttemptStatisticRequest(tid, ctx.sid);
					paymentAuthorizeStatisticRequest.setClientIP(ctx.clientIP);
					paymentAuthorizeStatisticRequest.setErrorDescr("");
					paymentAuthorizeStatisticRequest.setSessionId(ctx.sessionId);
					paymentAuthorizeStatisticRequest.setSiteCode(ctx.site);
					paymentAuthorizeStatisticRequest.setSuccess(true);
					paymentAuthorizeStatisticRequest.setPaymentType(paymentData.getType());
					if (paymentData.getType() == PaymentTypeEnum.CREDIT_CARD) {
						PaymentProviderCreditCardData provider = (PaymentProviderCreditCardData) paymentData.getProvider();
						paymentAuthorizeStatisticRequest.setCcName(provider.getType());
						paymentAuthorizeStatisticRequest.setThreeDSCheck("None");
					}
					paymentAuthorizeStatisticRequest.setEmail(paymentData.getEmail());
					if (!paymentData.getType().equals(PaymentTypeEnum.PAY_PAL))
						paymentAuthorizeStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_ATTEMPT);
					else
						paymentAuthorizeStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_PAY_PAL);

					RegisterStatisticsResponse paymentAuthorizeStatisticResponse = registerStatisticsDelegate.registerStatistics(paymentAuthorizeStatisticRequest);
					if (paymentAuthorizeStatisticResponse == null) {
						logger.error("Error while executing Authorize Statistic");
					}
				} catch (Exception e) {
					logger.error("Error while executing Authorize Statistic");
				}
			}
		}

		logger.debug("nomeBanca: "+nomeBanca+ " redirecturl: "+process.getRedirectUrl());
		if (PaymentType.CDC.toString().equals(type) || PaymentType.OneClick.toString().equals(type) || PaymentType.ZeroPayment.toString().equals(type)) {

			if (process.getRedirectUrl() != null && !process.getRedirectUrl().equals("")) {
				logger.debug("MdP Carta di Credito: caso 3DS");
				paymentData.setHtmlData3ds(process.getRedirectUrl());
				String payment3DSUrl = ctx.domain + ctx.baseUrl + "/booking/payment-3ds.html";
				logger.debug("payment3DS url: "+payment3DSUrl);
				paymentData.getProcess().setRedirectUrl(payment3DSUrl);
				// will be used for complete payment later (after redirect)
				ctx.paymentData = paymentData;
				ctx.paymentTid = tid;

			} else { // Caso non 3ds

				logger.debug("MdP Carta di Credito: 3DS non presente");
				// complete payment now!
				// ctx.paymentData = null; // is used to flush the previous payments attempts
				completePaymentCdC(paymentData, ctx, tid, i18n);

				// in case of CDC payment, save the payment data (clearing out the cvv field) for the MMB preset
				if (paymentData != null && paymentData.getProvider() instanceof PaymentProviderCreditCardData) {
					ctx.paymentData = null;
					ctx.paymentDataForMmb = paymentData;
					((PaymentProviderCreditCardData) ctx.paymentDataForMmb.getProvider()).setCvv(null);
				}
			}

		} else if (process.getRedirectUrl() != null && !process.getRedirectUrl().equals("")) {

			//PayPal
			if (nomeBanca!=null && (PaymentTypeEnum.PAY_PAL.value().equals(nomeBanca) /*|| PaymentTypeEnum.MAESTRO.value().equals(nomeBanca)*/)) {
				paymentData.setHtmlData3ds(process.getRedirectUrl());
				String payment3DSUrl = ctx.domain + ctx.baseUrl + "/booking/payment-3ds.html";
				logger.debug("paymentExternal url: "+payment3DSUrl);
				paymentData.getProcess().setRedirectUrl(payment3DSUrl);
			}

			// Caso iFrame
			if (nomeBanca!=null && (PaymentTypeEnum.DISCOVER.value().equals(nomeBanca) || PaymentTypeEnum.CARTE_BLEUE.value().equals(nomeBanca) || PaymentTypeEnum.JCB.value().equals(nomeBanca) || PaymentTypeEnum.INSTALMENTS_BRAZIL.value().equals(nomeBanca))){
				String iframeRedirectUrl = process.getRedirectUrl();
				paymentData.setIframeRedirectUrl(iframeRedirectUrl);
				String iframeUrl = ctx.domain + ctx.baseUrl + "/booking/payment-external.html";
				logger.debug("iframe url: "+iframeUrl);
				paymentData.getProcess().setRedirectUrl(iframeUrl); // Da settare payment-iframe.html
			}
			// will be used for complete payment later (after redirect)
			ctx.paymentData = paymentData;
			ctx.paymentTid = tid;
		} else if (!PaymentType.PagaDopo.toString().equals(type)){ // Per pagaDopo mi fermo ad initialize. Nothing to do.
			// error: nor redirect nor credit card nor pagadopo payment
			throw new BookingPaymentException(
					BookingPaymentException.BOOKING_ERROR_GENERIC, 
					"Unable to perform payment");
		}

		if (PaymentType.PagaDopo.toString().equals(type) &&
				PaymentTypeEnum.LOTTOMATICA.equals(paymentData.getType())) {
			ctx.paymentData = null;

			sendMail(ctx, paymentData, i18n, tid, configuration.getMailNoreplyAlitalia(), "Prenotazione Lottomatica");

			RegisterPaymentBookingEtktStatisticRequest bookingEtktStatisticRequest = 
					new RegisterPaymentBookingEtktStatisticRequest(tid, ctx.sid);
			bookingEtktStatisticRequest.setPnr(ctx.prenotation.getPnr());

			String importo = "";
			try {
				importo = NumberFormat.getInstance(ctx.locale).format(ctx.grossAmount.doubleValue());
			} catch (Exception e) {
				logger.warn("Error casting gross amount to string for statistics: ", e.getMessage());
			}
			bookingEtktStatisticRequest.setImporto(importo);

			bookingEtktStatisticRequest.setUserId(ctx.loggedUserProfileData != null ? ctx.loggedUserProfileData.getCustomerNumber() : null);
			bookingEtktStatisticRequest.setClientIP(ctx.clientIP);
			bookingEtktStatisticRequest.setSessionId(ctx.sessionId);
			bookingEtktStatisticRequest.setSiteCode(ctx.site);
			bookingEtktStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_BOOKING_ETKT);
			bookingEtktStatisticRequest.setSuccess(true);

			try {
				RegisterStatisticsResponse statisticResponse = registerStatisticsDelegate.registerStatistics(bookingEtktStatisticRequest);

				if (logger.isDebugEnabled())
					logger.debug("Called paymentBookingEtktStatistic. Response: "  + statisticResponse);

				if (statisticResponse == null) {
					logger.error("Error performing authorize payment statistic");
				}
			} catch (Exception e) {
				logger.error("Error performing authorize payment statistic", e);
			}


			ctx.phase = BookingPhaseEnum.DONE;
		}

		if (PaymentType.PagaDopo.toString().equals(type) &&
				PaymentTypeEnum.PAY_AT_TO.equals(paymentData.getType())) {
			sendMail(ctx, paymentData, i18n, tid, configuration.getMailNoreplyAlitalia(), configuration.getMailNoreplyAlitalia());
		}

		// if type of payment is not credit card, ccFee is not applied so it will be not shown
		if (!PaymentType.CDC.toString().equals(type) && !PaymentType.OneClick.toString().equals(type)) {
			ctx.ccFeeApplied = false;
		}
	}

	/**
	 * Complete payment: the last steps
	 * 
	 * <p>Note that this method will modify the content of Passengers 
	 * into ctx.prenotation object.</p>
	 * 
	 * @param paymentData
	 * @param ctx The booking session context to use.
	 * @param tid transaction ID
	 * 
	 * @throws BookingPaymentException
	 */
	public void completePayment(PaymentData paymentData,
			BookingSessionContext ctx, String tid, I18n i18n) throws BookingPaymentException {



		// step 2: authorize payment
		AuthorizePaymentResponse authorizePaymentResponse = 
				performPaymentAuthorizePayment(ctx, tid, paymentData);

		String mdp = ctx.prenotation.getPayment().getProvider() instanceof PaymentProviderGlobalCollectData ? ((PaymentProviderGlobalCollectData)ctx.prenotation.getPayment().getProvider()).getType().value() : ctx.prenotation.getPayment().getType().value();
		logger.info("completePayment. AuthorizePayment executed. PNR: {}. MdP: {}", authorizePaymentResponse.getPnr(), mdp);

		long timeAfterAuth = System.currentTimeMillis();

		if (PaymentStepEnum.CHECK == authorizePaymentResponse.getProcess().getStep()) {

			// step 3: check payment
			CheckPaymentResponse checkPaymentResponse =
					performPaymentCheckPayment(ctx, tid, paymentData.getType(),
							authorizePaymentResponse);

			if (PaymentStepEnum.RETRIEVE == checkPaymentResponse.getProcess().getStep()) {

				// step 4: retrieve tickets
				RetrieveTicketResponse retrieveTicketResponse =
						performPaymentRetrieveTicket(ctx, tid, checkPaymentResponse.getProcess(), timeAfterAuth);

				logger.debug("MAIL Payment Type: "+ctx.prenotation.getPayment().getType());


				// store invoicing process result
				ctx.invoiceRequestSuccessful = retrieveTicketResponse.isInvoiceRequestSuccessful();

				// modify passengers data
				ctx.prenotation.setPassengers(retrieveTicketResponse.getPassenger());

				sendMail(ctx, paymentData, i18n, tid, configuration.getMailConfirmationAlitalia(), paymentEmailGenerator.getEmailSubject(i18n));

				if (ctx.isCarnetProcess) {
					ctx.updateCarnetSuccess = performUpdateCarnet(ctx,tid);
					if (ctx.updateCarnetSuccess) {
						ctx.sendNotificationCarnetEmailSuccess = sendNotificationCarnetEmail(ctx,tid);
					} else {
						logger.error("Update Carnet error");
					}
				}
			}

			// set phase to done
			ctx.phase = BookingPhaseEnum.DONE;

		} else {
			throw new BookingPaymentException(BookingPaymentException.BOOKING_ERROR_GENERIC,
					"Unable to perform payment");
		}
	}

	/**
	 * Complete payment: the last steps
	 * 
	 * <p>Note that this method will modify the content of Passengers 
	 * into ctx.prenotation object.</p>
	 * 
	 * @param paymentData
	 * @param ctx The booking session context to use.
	 * @param tid transaction ID
	 * 
	 * @throws BookingPaymentException
	 */
	public void completePaymentCdC(PaymentData paymentData,
			BookingSessionContext ctx, String tid, I18n i18n) throws BookingPaymentException {

		long timeAfterInit = System.currentTimeMillis();

		if (PaymentStepEnum.RETRIEVE == paymentData.getProcess().getStep()) {

			// step 4: retrieve tickets
			RetrieveTicketResponse retrieveTicketResponse =
					performPaymentRetrieveTicket(ctx, tid, paymentData, timeAfterInit);

			logger.debug("MAIL Payment Type: "+ctx.prenotation.getPayment().getType());

			// store invoicing process result
			ctx.invoiceRequestSuccessful = retrieveTicketResponse.isInvoiceRequestSuccessful();

			// modify passengers data
			ctx.prenotation.setPassengers(retrieveTicketResponse.getPassenger());

			sendMail(ctx, paymentData, i18n, tid, configuration.getMailConfirmationAlitalia(), paymentEmailGenerator.getEmailSubject(i18n));

			if (ctx.isCarnetProcess) {
				ctx.updateCarnetSuccess = performUpdateCarnet(ctx,tid);
				if (ctx.updateCarnetSuccess) {
					ctx.sendNotificationCarnetEmailSuccess = sendNotificationCarnetEmail(ctx,tid);
				} else {
					logger.error("Update Carnet error");
				}
			}
		} else {
			throw new BookingPaymentException(BookingPaymentException.BOOKING_ERROR_GENERIC,
					"Unable to perform payment");
		}

		// set phase to done
		ctx.phase = BookingPhaseEnum.DONE;

	}

	/**
	 * Complete payment3ds: the last steps
	 * 
	 * <p>Note that this method will modify the content of Passengers 
	 * into ctx.prenotation object.</p>
	 * 
	 * @param paymentData
	 * @param ctx The booking session context to use.
	 * @param tid transaction ID
	 * 
	 * @throws BookingPaymentException
	 */
	public void completePaymentSabre(PaymentData paymentData,
			BookingSessionContext ctx, String tid, I18n i18n) throws BookingPaymentException {

		// step 2: authorize payment
		AuthorizePaymentResponse authorizePaymentResponse = 
				performPaymentAuthorizePayment(ctx, tid, paymentData);

		//STATISTICA CREAZIONE PNR
		try {
			RegisterPNRStatisticRequest pnrStatisticRequest = new RegisterPNRStatisticRequest(tid, ctx.sid);
			pnrStatisticRequest.setClientIP(ctx.clientIP);
			pnrStatisticRequest.setErrorDescr("");
			pnrStatisticRequest.setSessionId(ctx.sessionId);
			pnrStatisticRequest.setSiteCode(ctx.site);
			pnrStatisticRequest.setSuccess(true);
			pnrStatisticRequest.setType(StatisticsDataTypeEnum.PNR);
			pnrStatisticRequest.setPnr(authorizePaymentResponse.getPnr());
			pnrStatisticRequest.setTravelInfo(obtainApplicantNameForStatistics(ctx));
			pnrStatisticRequest.setAward(false);

			RegisterStatisticsResponse pnrStatisticResponse = registerStatisticsDelegate.registerStatistics(pnrStatisticRequest);
			if (pnrStatisticResponse == null) {
				logger.error("Error while executing PNR Statistic");
			}
		} catch (Exception e) {
			logger.error("Error while executing PNR Statistic");
		}

		long timeAfterAuth = System.currentTimeMillis();

		if (PaymentStepEnum.RETRIEVE == authorizePaymentResponse.getProcess().getStep()) {

			ctx.prenotation.setPnr(authorizePaymentResponse.getPnr());
			String mdp = ctx.prenotation.getPayment().getProvider() instanceof PaymentProviderGlobalCollectData ? ((PaymentProviderGlobalCollectData)ctx.prenotation.getPayment().getProvider()).getType().value() : ctx.prenotation.getPayment().getType().value();
			logger.info("completePaymentSabre. AuthorizePayment executed. PNR: {}. MdP: {}", authorizePaymentResponse.getPnr(), mdp);

			// step 4: retrieve tickets
			RetrieveTicketResponse retrieveTicketResponse =
					performPaymentRetrieveTicket(ctx, tid, authorizePaymentResponse.getProcess(), timeAfterAuth);

			logger.debug("MAIL Payment Type: "+ctx.prenotation.getPayment().getType());

			// store invoicing process result
			ctx.invoiceRequestSuccessful = retrieveTicketResponse.isInvoiceRequestSuccessful();

			// modify passengers data
			ctx.prenotation.setPassengers(retrieveTicketResponse.getPassenger());

			sendMail(ctx, paymentData, i18n, tid, configuration.getMailConfirmationAlitalia(), paymentEmailGenerator.getEmailSubject(i18n));

			if (ctx.isCarnetProcess) {
				ctx.updateCarnetSuccess = performUpdateCarnet(ctx,tid);
				if (ctx.updateCarnetSuccess) {
					ctx.sendNotificationCarnetEmailSuccess = sendNotificationCarnetEmail(ctx,tid);
				} else {
					logger.error("Update Carnet error");
				}
			}
		} else {
			throw new BookingPaymentException(BookingPaymentException.BOOKING_ERROR_GENERIC,
					"Unable to perform payment");
		}

		// set phase to done
		ctx.phase = BookingPhaseEnum.DONE;

	}

	public void completePaymentFindomestic(PaymentData paymentData,
			BookingSessionContext ctx, String tid, I18n i18n) throws BookingPaymentException {

		// step 2: authorize payment
		AuthorizePaymentResponse authorizePaymentResponse = 
				performPaymentAuthorizePayment(ctx, tid, paymentData);

		String mdp = ctx.prenotation.getPayment().getProvider() instanceof PaymentProviderGlobalCollectData ? ((PaymentProviderGlobalCollectData)ctx.prenotation.getPayment().getProvider()).getType().value() : ctx.prenotation.getPayment().getType().value();
		logger.info("completePaymentFindomestic. AuthorizePayment executed. PNR: {}. MdP: {}", authorizePaymentResponse.getPnr(), mdp);

		sendMail(ctx, paymentData, i18n, tid, configuration.getMailNoreplyAlitalia(), configuration.getMailNoreplyAlitalia());

		if (ResultTypeEnum.OK != authorizePaymentResponse.getProcess().getResult()) {
			throw new BookingPaymentException(BookingPaymentException.BOOKING_ERROR_GENERIC,
					"Unable to perform payment");
		}

		ctx.phase = BookingPhaseEnum.DONE;
	}

	/*
	 * First step of payment
	 * 
	 * @param ctx The booking session context to use.
	 * @param request The http request
	 * @param tid transaction ID
	 * @return InitializePaymentResponse
	 */
	private InitializePaymentResponse performPaymentInitializePayment(BookingSessionContext ctx, 
			String ipAddress, String userAgent, String tid, Map<String, String> httpHeaders, String jscString) throws BookingPaymentException {
		logger.debug("performPaymentInitializePayment");

		InitializePaymentRequest initializePaymentRequest = new InitializePaymentRequest();
		initializePaymentRequest.setSid(ctx.sid);
		initializePaymentRequest.setTid(tid);
		initializePaymentRequest.setCookie(ctx.cookie);
		initializePaymentRequest.setExecution(ctx.execution);
		initializePaymentRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
		initializePaymentRequest.setIpAddress(ipAddress);
		initializePaymentRequest.setUserAgent(userAgent);
		ctx.prenotation = addFraudnetInfo(ctx.prenotation, httpHeaders, jscString);
		initializePaymentRequest.setPrenotation(ctx.prenotation);
		initializePaymentRequest.setCarnetInfo(computeInfoCarnet(ctx));

		InitializePaymentResponse initializePaymentResponse = null;
		try {
			// initialize payment!
			initializePaymentResponse = paymentDelegate.initializePayment(initializePaymentRequest);
			String cookieResponse = initializePaymentResponse.getCookie();
			if(cookieResponse != null && !cookieResponse.isEmpty()){
				ctx.cookie = cookieResponse;
				ctx.execution = initializePaymentResponse.getExecute();
				ctx.sabreGateWayAuthToken = initializePaymentResponse.getSabreGateWayAuthToken();
			}
			
		} catch (Exception e) {
			// STATISTICA INIT KO (PAGAMENTO)
			try {
				RegisterPaymentAuthAttemptStatisticRequest paymentAuthorizeStatisticRequest = new RegisterPaymentAuthAttemptStatisticRequest(tid, ctx.sid);
				paymentAuthorizeStatisticRequest.setClientIP(ctx.clientIP);
				paymentAuthorizeStatisticRequest.setErrorDescr("Generic Error");
				paymentAuthorizeStatisticRequest.setSessionId(ctx.sessionId);
				paymentAuthorizeStatisticRequest.setSiteCode(ctx.site);
				paymentAuthorizeStatisticRequest.setSuccess(false);
				paymentAuthorizeStatisticRequest.setPaymentType(ctx.prenotation.getPayment().getType());
				if (ctx.prenotation.getPayment().getType() == PaymentTypeEnum.CREDIT_CARD) {
					paymentAuthorizeStatisticRequest.setCcName(((PaymentProviderCreditCardData) ctx.prenotation.getPayment().getProvider()).getType());
					paymentAuthorizeStatisticRequest.setThreeDSCheck(ctx.prenotation.getPayment().getProcess().getRedirectUrl() != null ? "ThreeDSCheck" : "None");
				}
				paymentAuthorizeStatisticRequest.setEmail(ctx.prenotation.getPayment().getEmail());
				if (!ctx.prenotation.getPayment().getType().equals(PaymentTypeEnum.PAY_PAL))
					paymentAuthorizeStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_ATTEMPT);
				else
					paymentAuthorizeStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_PAY_PAL);

				RegisterStatisticsResponse paymentAuthorizeStatisticResponse = registerStatisticsDelegate.registerStatistics(paymentAuthorizeStatisticRequest);
				if (paymentAuthorizeStatisticResponse == null) {
					logger.error("Error while executing Authorize Statistic");
				}
			} catch (Exception ex) {
				logger.error("Error while executing KO Authorize Statistic");
			}

			logger.error("Error during initialize payment", e);
			throw new BookingPaymentException(
					BookingPaymentException.BOOKING_ERROR_INITIALIZEPAYMENT,
					"Unable to perform initializePayment");
		}

		//ERRORE INIT CATCHATO SU SERVICE
		if (initializePaymentResponse.getPaymentData() == null){
			// KO PAGAMENTO/PNR
			try {
				if (ctx.prenotation.getPayment().getType() != PaymentTypeEnum.PAY_PAL && 
					//	(!(ctx.prenotation.getPayment().getProvider() instanceof PaymentProviderGlobalCollectData) || ((PaymentProviderGlobalCollectData)ctx.prenotation.getPayment().getProvider()).getType() != GlobalCollectPaymentTypeEnum.MAESTRO) &&
						(ctx.prenotation.getPayment().getType() != PaymentTypeEnum.CREDIT_CARD || ctx.prenotation.getPayment().getProcess().getRedirectUrl() == null)
						&& initializePaymentResponse.getErrorStatisticMessage() != null 
						&& initializePaymentResponse.getErrorStatisticMessage().contains("TECHNICAL_ERROR")){ //PNR KO CASE
					//STATISTICA PNR KO
					RegisterPNRStatisticRequest pnrStatisticRequest = new RegisterPNRStatisticRequest(tid, ctx.sid);
					pnrStatisticRequest.setClientIP(ctx.clientIP);
					pnrStatisticRequest.setErrorDescr(initializePaymentResponse.getErrorStatisticMessage());
					pnrStatisticRequest.setSessionId(ctx.sessionId);
					pnrStatisticRequest.setSiteCode(ctx.site);
					pnrStatisticRequest.setSuccess(false);
					pnrStatisticRequest.setType(StatisticsDataTypeEnum.PNR);
					pnrStatisticRequest.setPnr("None");
					pnrStatisticRequest.setTravelInfo(obtainApplicantNameForStatistics(ctx));
					pnrStatisticRequest.setAward(false);

					RegisterStatisticsResponse pnrStatisticResponse = registerStatisticsDelegate.registerStatistics(pnrStatisticRequest);
					if (pnrStatisticResponse == null) {
						logger.error("Error while executing PNR Statistic");
					}
				} else {
					RegisterPaymentAuthAttemptStatisticRequest paymentAuthorizeStatisticRequest = new RegisterPaymentAuthAttemptStatisticRequest(tid, ctx.sid);
					paymentAuthorizeStatisticRequest.setClientIP(ctx.clientIP);
					paymentAuthorizeStatisticRequest.setErrorDescr(initializePaymentResponse.getErrorStatisticMessage());
					paymentAuthorizeStatisticRequest.setSessionId(ctx.sessionId);
					paymentAuthorizeStatisticRequest.setSiteCode(ctx.site);
					paymentAuthorizeStatisticRequest.setSuccess(false);
					paymentAuthorizeStatisticRequest.setPaymentType(ctx.prenotation.getPayment().getType());
					if (ctx.prenotation.getPayment().getType() == PaymentTypeEnum.CREDIT_CARD) {
						paymentAuthorizeStatisticRequest.setCcName(((PaymentProviderCreditCardData) ctx.prenotation.getPayment().getProvider()).getType());
						paymentAuthorizeStatisticRequest.setThreeDSCheck(ctx.prenotation.getPayment().getProcess().getRedirectUrl() != null ? "ThreeDSCheck" : "None");
					}
					paymentAuthorizeStatisticRequest.setEmail(ctx.prenotation.getPayment().getEmail());
					if (!ctx.prenotation.getPayment().getType().equals(PaymentTypeEnum.PAY_PAL))
						paymentAuthorizeStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_ATTEMPT);
					else
						paymentAuthorizeStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_PAY_PAL);

					RegisterStatisticsResponse paymentAuthorizeStatisticResponse = registerStatisticsDelegate.registerStatistics(paymentAuthorizeStatisticRequest);
					if (paymentAuthorizeStatisticResponse == null) {
						logger.error("Error while executing Authorize Statistic");
					}
				}
			} catch (Exception ex) {
				logger.error("Error while executing KO Authorize Statistic", ex);
			}

			throw new BookingPaymentException(
					BookingPaymentException.BOOKING_ERROR_INITIALIZEPAYMENT,
					"Unable to perform initializePayment", initializePaymentResponse.getErrorCode());
		}

		return initializePaymentResponse;
	}

	private RoutesData addFraudnetInfo(RoutesData prenotation, Map<String, String> httpHeaders, String jscString){
		if(prenotation != null && prenotation.getPayment() != null
				&& prenotation.getPayment().getProcess() != null){
			PaymentProcessInfoData procInfo 
			= prenotation.getPayment().getProcess();
			List<FraudNetParamData> params = new LinkedList<FraudNetParamData>();
			if(httpHeaders != null){
				for(String key : httpHeaders.keySet()){
					FraudNetParamData param = new FraudNetParamData();
					param.setName(key);
					param.setValue(httpHeaders.get(key));
					params.add(param);
				}
			}
			/*Device fingerprint id*/
			FraudNetParamData param = new FraudNetParamData();
			param.setName("DEVICEFINGERPRINTID");
			param.setValue(jscString);
			params.add(param);
			procInfo.setMac(configuration.get3dsMacString());
			procInfo.setFraudNetParams(params);
		}

		return prenotation;
	}

	private InfoCarnetData computeInfoCarnet(BookingSessionContext ctx) {
		if (ctx.isCarnetProcess) {
			InfoCarnetData carnet = new InfoCarnetData();
			carnet.setBuyerEmail(ctx.infoCarnet.getBuyerEmail());
			carnet.setBuyerFirstName(ctx.infoCarnet.getBuyerName());
			carnet.setBuyerLastName(ctx.infoCarnet.getBuyerLastname());
			carnet.setCarnetCode(ctx.infoCarnet.getCarnetCode());
			carnet.setCarnetPassword(ctx.infoCarnet.getPassword());
			carnet.setExpiryDate(ctx.infoCarnet.getExpiryDate());
			carnet.setResidualRoutes(ctx.infoCarnet.getResidualRoutes());
			carnet.setTotalFare(ctx.infoCarnet.getTotalFare());
			carnet.setTotalRoutes(ctx.infoCarnet.getTotalRoutes());
			return carnet;
		}
		return null;
	}


	/*
	 * Second step of payment
	 * 
	 * <p>Note that this method will modify the content of PaymentData 
	 * into ctx.prenotation object.</p>
	 * 
	 * @param ctx The booking session context to use.
	 * @param tid transaction ID
	 * @param initializePaymentResponse response of Step 1
	 * @return AuthorizePaymentResponse
	 */
	private AuthorizePaymentResponse performPaymentAuthorizePayment(BookingSessionContext ctx,
			String tid, PaymentData paymentData) throws BookingPaymentException {
		logger.debug("performPaymentInitializePayment");

		// modify current state
		ctx.prenotation.setPayment(paymentData);

		// create authorizePayment request
		AuthorizePaymentRequest authorizePaymentRequest = new AuthorizePaymentRequest();
		authorizePaymentRequest.setSid(ctx.sid);
		authorizePaymentRequest.setTid(tid);
		authorizePaymentRequest.setCookie(ctx.cookie);
		authorizePaymentRequest.setExecution(ctx.execution);
		authorizePaymentRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
		authorizePaymentRequest.setPrenotation(ctx.prenotation);
		authorizePaymentRequest.setFlgUsaCa(ctx.isSecureFlight);

		AuthorizePaymentResponse authorizePaymentResponse = null;
		try {
			// authorize payment!
			authorizePaymentResponse = paymentDelegate.authorizePayment(authorizePaymentRequest);
		} catch (Exception e) {
			logger.error("Error during Authorize Payment ", e);
			// STATISTICA AUTH KO (PAGAMENTO)
			try {
				RegisterPaymentAuthAttemptStatisticRequest paymentAuthorizeStatisticRequest = new RegisterPaymentAuthAttemptStatisticRequest(tid, ctx.sid);
				paymentAuthorizeStatisticRequest.setClientIP(ctx.clientIP);
				paymentAuthorizeStatisticRequest.setErrorDescr("Generic Error");
				paymentAuthorizeStatisticRequest.setSessionId(ctx.sessionId);
				paymentAuthorizeStatisticRequest.setSiteCode(ctx.site);
				paymentAuthorizeStatisticRequest.setSuccess(false);
				paymentAuthorizeStatisticRequest.setPaymentType(ctx.prenotation.getPayment().getType());
				if (ctx.prenotation.getPayment().getType() == PaymentTypeEnum.CREDIT_CARD) {
					paymentAuthorizeStatisticRequest.setCcName(((PaymentProviderCreditCardData) ctx.prenotation.getPayment().getProvider()).getType());
					paymentAuthorizeStatisticRequest.setThreeDSCheck(ctx.prenotation.getPayment().getProcess().getRedirectUrl() != null ? "ThreeDSCheck" : "None");
				}
				paymentAuthorizeStatisticRequest.setEmail(ctx.prenotation.getPayment().getEmail());
				if (!ctx.prenotation.getPayment().getType().equals(PaymentTypeEnum.PAY_PAL))
					paymentAuthorizeStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_ATTEMPT);
				else
					paymentAuthorizeStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_PAY_PAL);

				RegisterStatisticsResponse paymentAuthorizeStatisticResponse = registerStatisticsDelegate.registerStatistics(paymentAuthorizeStatisticRequest);
				if (paymentAuthorizeStatisticResponse == null) {
					logger.error("Error while executing Authorize Statistic");
				}
			} catch (Exception ex) {
				logger.error("Error while executing KO Authorize Statistic");
			}

			throw new BookingPaymentException(
					BookingPaymentException.BOOKING_ERROR_AUTHORIZEPAYMENT,
					"Unable to perform authorizePayment");
		}
		if (authorizePaymentResponse == null || authorizePaymentResponse.getProcess() == null) {
			// STATISTICA AUTH KO (PAGAMENTO/PNR)
			try {
				if (authorizePaymentResponse.getErrorStatisticMessage() != null &&
						authorizePaymentResponse.getErrorStatisticMessage().contains("TECHNICAL_ERROR")){ //PNR KO CASE
					//STATISTICA PNR KO
					RegisterPNRStatisticRequest pnrStatisticRequest = new RegisterPNRStatisticRequest(tid, ctx.sid);
					pnrStatisticRequest.setClientIP(ctx.clientIP);
					pnrStatisticRequest.setErrorDescr(authorizePaymentResponse.getErrorStatisticMessage());
					pnrStatisticRequest.setSessionId(ctx.sessionId);
					pnrStatisticRequest.setSiteCode(ctx.site);
					pnrStatisticRequest.setSuccess(false);
					pnrStatisticRequest.setType(StatisticsDataTypeEnum.PNR);
					pnrStatisticRequest.setPnr("None");
					pnrStatisticRequest.setTravelInfo(obtainApplicantNameForStatistics(ctx));
					pnrStatisticRequest.setAward(false);

					RegisterStatisticsResponse pnrStatisticResponse = registerStatisticsDelegate.registerStatistics(pnrStatisticRequest);
					if (pnrStatisticResponse == null) {
						logger.error("Error while executing PNR Statistic");
					}
				} else {
					RegisterPaymentAuthAttemptStatisticRequest paymentAuthorizeStatisticRequest = new RegisterPaymentAuthAttemptStatisticRequest(tid, ctx.sid);
					paymentAuthorizeStatisticRequest.setClientIP(ctx.clientIP);
					paymentAuthorizeStatisticRequest.setErrorDescr(authorizePaymentResponse != null ? authorizePaymentResponse.getErrorStatisticMessage() : "Generic Error");
					paymentAuthorizeStatisticRequest.setSessionId(ctx.sessionId);
					paymentAuthorizeStatisticRequest.setSiteCode(ctx.site);
					paymentAuthorizeStatisticRequest.setSuccess(false);
					paymentAuthorizeStatisticRequest.setPaymentType(ctx.prenotation.getPayment().getType());
					if (ctx.prenotation.getPayment().getType() == PaymentTypeEnum.CREDIT_CARD) {
						paymentAuthorizeStatisticRequest.setCcName(((PaymentProviderCreditCardData) ctx.prenotation.getPayment().getProvider()).getType());
						paymentAuthorizeStatisticRequest.setThreeDSCheck(ctx.prenotation.getPayment().getProcess().getRedirectUrl() != null ? "ThreeDSCheck" : "None");
					}
					paymentAuthorizeStatisticRequest.setEmail(ctx.prenotation.getPayment().getEmail());
					if (!ctx.prenotation.getPayment().getType().equals(PaymentTypeEnum.PAY_PAL))
						paymentAuthorizeStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_ATTEMPT);
					else
						paymentAuthorizeStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_PAY_PAL);

					RegisterStatisticsResponse paymentAuthorizeStatisticResponse = registerStatisticsDelegate.registerStatistics(paymentAuthorizeStatisticRequest);
					if (paymentAuthorizeStatisticResponse == null) {
						logger.error("Error while executing Authorize Statistic");
					}
				}
			} catch (Exception ex) {
				logger.error("Error while executing KO Authorize Statistic");
			}

			throw new BookingPaymentException(
					BookingPaymentException.BOOKING_ERROR_AUTHORIZEPAYMENT,
					"Unable to perform authorizePayment");
		}

		try {
			RegisterPaymentAuthAttemptStatisticRequest paymentAuthorizeStatisticRequest = new RegisterPaymentAuthAttemptStatisticRequest(tid, ctx.sid);
			paymentAuthorizeStatisticRequest.setClientIP(ctx.clientIP);
			paymentAuthorizeStatisticRequest.setErrorDescr("");
			paymentAuthorizeStatisticRequest.setSessionId(ctx.sessionId);
			paymentAuthorizeStatisticRequest.setSiteCode(ctx.site);
			paymentAuthorizeStatisticRequest.setSuccess(true);
			paymentAuthorizeStatisticRequest.setPaymentType(paymentData.getType());
			if (paymentData.getType() == PaymentTypeEnum.CREDIT_CARD) {
				PaymentProviderCreditCardData provider = (PaymentProviderCreditCardData) paymentData.getProvider();
				paymentAuthorizeStatisticRequest.setCcName(provider.getType());
				paymentAuthorizeStatisticRequest.setThreeDSCheck("ThreeDSCheck");
			}
			paymentAuthorizeStatisticRequest.setEmail(paymentData.getEmail());
			if (!paymentData.getType().equals(PaymentTypeEnum.PAY_PAL))
				paymentAuthorizeStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_ATTEMPT);
			else
				paymentAuthorizeStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_PAY_PAL);

			RegisterStatisticsResponse paymentAuthorizeStatisticResponse = registerStatisticsDelegate.registerStatistics(paymentAuthorizeStatisticRequest);
			if (paymentAuthorizeStatisticResponse == null) {
				logger.error("Error while executing Authorize Statistic");
			}
		} catch (Exception e) {
			logger.error("Error while executing Authorize Statistic");
		}

		return authorizePaymentResponse;
	}

	/*
	 * Third step of payment
	 * 
	 * <p>Note that this method will modify the content of InsuranceData 
	 * into prenotation object.</p>
	 * 
	 * @param ctx The booking session context to use.
	 * @param tid transaction ID
	 * @param type type of payment
	 * @param authorizePaymentResponse response of Step 2
	 * @return CheckPaymentResponse
	 */
	private CheckPaymentResponse performPaymentCheckPayment(BookingSessionContext ctx,
			String tid, PaymentTypeEnum type, 
			AuthorizePaymentResponse authorizePaymentResponse) throws BookingPaymentException {
		logger.debug("performPaymentCheckPayment");

		// modify current state
		ctx.prenotation.setInsurance(authorizePaymentResponse.getInsurance());

		// create checkPayment request
		CheckPaymentRequest checkPaymentRequest = new CheckPaymentRequest();
		checkPaymentRequest.setSid(ctx.sid);
		checkPaymentRequest.setTid(tid);
		checkPaymentRequest.setProcess(authorizePaymentResponse.getProcess());
		checkPaymentRequest.setType(type);

		CheckPaymentResponse checkPaymentResponse = null;
		try { 
			// check payment!
			checkPaymentResponse = paymentDelegate.checkPayment(checkPaymentRequest);
		} catch (Exception e) {
			logger.error("Error during CheckPayment", e);
			throw new BookingPaymentException(
					BookingPaymentException.BOOKING_ERROR_CHECKPAYMENT,
					"Unable to perform checkPayment");
		}

		if (checkPaymentResponse == null || checkPaymentResponse.getProcess() == null) {
			throw new BookingPaymentException(
					BookingPaymentException.BOOKING_ERROR_CHECKPAYMENT,
					"Unable to perform checkPayment");
		}

		if (PaymentTypeEnum.PAY_PAL.equals(type)) {
			try {
				RegisterStatisticsRequest paypalAuthorizationStatisticRequest = new RegisterStatisticsRequest(tid, ctx.sid);
				paypalAuthorizationStatisticRequest.setClientIP(ctx.clientIP);
				paypalAuthorizationStatisticRequest.setErrorDescr("");
				paypalAuthorizationStatisticRequest.setSessionId(ctx.sessionId);
				paypalAuthorizationStatisticRequest.setSiteCode(ctx.site);
				paypalAuthorizationStatisticRequest.setSuccess(true);
				paypalAuthorizationStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_AUTHORIZATION_PAY_PAL);

				RegisterStatisticsResponse paymentAuthorizeStatisticResponse = 
						registerStatisticsDelegate.registerStatistics(paypalAuthorizationStatisticRequest);
				if (paymentAuthorizeStatisticResponse == null) {
					logger.error("Error while executing PayPal Authorization Statistic");
				}
			} catch (Exception e) {
				logger.error("Error while executing PayPal Authorization Statistic");
			}

		}
		return checkPaymentResponse;
	}

	/*
	 * Fourth step of payment
	 * 
	 * @param ctx The booking session context to use.
	 * @param tid transaction ID
	 * @param checkPaymentResponse response of Step 3
	 * @return RetrieveTicketResponse
	 */
	private RetrieveTicketResponse performPaymentRetrieveTicket(BookingSessionContext ctx,
			String tid, PaymentProcessInfoData paymentInfoData, Long timeAfterAuth) throws BookingPaymentException {
		logger.debug("performPaymentRetrieveTickets");

		// create retrieveTicket request
		RetrieveTicketRequest retrieveTicketRequest = new RetrieveTicketRequest();
		retrieveTicketRequest.setSid(ctx.sid);
		retrieveTicketRequest.setTid(tid);
		retrieveTicketRequest.setCookie(ctx.cookie);
		retrieveTicketRequest.setExecution(ctx.execution);
		retrieveTicketRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
		retrieveTicketRequest.setProcess(paymentInfoData);
		retrieveTicketRequest.setRoutes(ctx.prenotation);
		retrieveTicketRequest.setTaxs(ctx.selectionTaxes);
		retrieveTicketRequest.setInvoiceRequired(ctx.invoiceRequired);

		RetrieveTicketResponse retrieveTicketResponse = null;
		try {
			// retrieve ticket!
			retrieveTicketResponse = paymentDelegate.retrieveTicket(retrieveTicketRequest);
			ctx.cookie = retrieveTicketResponse.getCookie();
			ctx.execution = retrieveTicketResponse.getExecute();
			ctx.sabreGateWayAuthToken = retrieveTicketResponse.getSabreGateWayAuthToken();
		} catch (Exception e) {
			logger.error("Error during retrieveTicket ", e);
			throw new BookingPaymentException(
					BookingPaymentException.BOOKING_ERROR_RETRIEVETICKETS,
					"Unable to perform retrieveTicket");
		}
		if (retrieveTicketResponse == null || retrieveTicketResponse.getProcess() == null) {
			throw new BookingPaymentException(
					BookingPaymentException.BOOKING_ERROR_RETRIEVETICKETS,
					"Unable to perform retrieveTicket");
		}

		// RETRIEVE TICKET OK
		if (!PaymentTypeEnum.PAY_PAL.equals(ctx.prenotation.getPayment().getType())) {
			RegisterPaymentFromAuthToTicketStatisticRequest authToTicketStatisticRequest = 
					new RegisterPaymentFromAuthToTicketStatisticRequest(tid, ctx.sid);
			authToTicketStatisticRequest.setUserId(ctx.loggedUserProfileData != null ? 
					ctx.loggedUserProfileData.getCustomerNumber() : null);
			authToTicketStatisticRequest.setClientIP(ctx.clientIP);
			authToTicketStatisticRequest.setErrorDescr("");
			authToTicketStatisticRequest.setSessionId(ctx.sessionId);
			authToTicketStatisticRequest.setSiteCode(ctx.site);
			authToTicketStatisticRequest.setSuccess(true);
			long timeAfterTicket = System.currentTimeMillis();
			authToTicketStatisticRequest.setBackofficeTimeMillis(timeAfterTicket - timeAfterAuth);
			authToTicketStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_FROM_AUTHORIZATION_TO_TICKET);

			try {
				RegisterStatisticsResponse statisticResponse = 
						registerStatisticsDelegate.registerStatistics(authToTicketStatisticRequest);

				if (logger.isDebugEnabled())
					logger.debug("Called fromAuthToTicketStatistic. Response: "+ statisticResponse);

				if (statisticResponse == null) {
					logger.error("Error performing authorizeToTicket payment statistic");
				}
			} catch (Exception e) {
				logger.error("Error performing authorizeToTicket payment statistic", e);
			}
		}
		RegisterPaymentBookingEtktStatisticRequest bookingEtktStatisticRequest = 
				new RegisterPaymentBookingEtktStatisticRequest(tid, ctx.sid);
		bookingEtktStatisticRequest.setPnr(ctx.prenotation.getPnr());

		List<TicketInfoData> tickets = null;
		for (PassengerBaseData passenger : retrieveTicketResponse.getPassenger()) {
			if (passenger instanceof ApplicantPassengerData) {
				ApplicantPassengerData pax = (ApplicantPassengerData) passenger;
				tickets = pax.getTickets();
			}
		}
		String eticket = "";
		if (tickets != null && !tickets.isEmpty()) {
			eticket = tickets.get(0).getTicketNumber();
		}
		bookingEtktStatisticRequest.setEticket(eticket);
		String importo = "";
		try {
			importo = NumberFormat.getInstance(ctx.locale).format(ctx.grossAmount.doubleValue());
		}catch(Exception e) {
			logger.error("Error casting gross amount to string", e);
		}
		bookingEtktStatisticRequest.setImporto(importo);

		bookingEtktStatisticRequest.setUserId(ctx.loggedUserProfileData!=null ? ctx.loggedUserProfileData.getCustomerNumber() : null);
		bookingEtktStatisticRequest.setClientIP(ctx.clientIP);
		bookingEtktStatisticRequest.setSessionId(ctx.sessionId);
		bookingEtktStatisticRequest.setSiteCode(ctx.site);
		bookingEtktStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_BOOKING_ETKT);
		bookingEtktStatisticRequest.setSuccess(true);

		try {
			RegisterStatisticsResponse statisticResponse = registerStatisticsDelegate.registerStatistics(bookingEtktStatisticRequest);
			logger.debug("Called paymentBookingEtktStatistic. Response: "+statisticResponse);
			if (statisticResponse == null) {
				logger.error("Error performing authorize payment statistic");
			}
		} catch(Exception e) {
			logger.error("Error performing authorize payment statistic", e);
		}

		//Insurance Statistics
		if (ctx.insuranceProposalData!=null && ctx.insuranceProposalData.getBuy()){
			try {

				RegisterPaymentBookingInsuranceStatisticRequest bookingInsuranceStatisticRequest = 
						new RegisterPaymentBookingInsuranceStatisticRequest(tid, ctx.sid);
				bookingInsuranceStatisticRequest.setClientIP(ctx.clientIP);
				bookingInsuranceStatisticRequest.setSessionId(ctx.sessionId);
				bookingInsuranceStatisticRequest.setSiteCode(ctx.site);
				bookingInsuranceStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_BOOKING_INSURANCE);
				bookingInsuranceStatisticRequest.setCardType(((PaymentProviderCreditCardData)ctx.prenotation.getPayment().getProvider()).getType().value());
				bookingInsuranceStatisticRequest.setSuccess(true);
				if (ctx.insuranceProposalData.getPolicyNumber()!=null){
					bookingInsuranceStatisticRequest.setPolicyNumber(ctx.insuranceProposalData.getPolicyNumber());
				} else {
					bookingInsuranceStatisticRequest.setPolicyNumber("(unknown - PNR is " + ctx.prenotation.getPnr() + ")");
				}
				bookingInsuranceStatisticRequest.setErrorDescr(ctx.insuranceProposalData.getErrorDescription());

				logger.debug("Registering Insurance Statistics... bookingInsuranceStatisticRequest: ["+bookingInsuranceStatisticRequest+"]");
				RegisterStatisticsResponse statisticsResponse = 
						registerStatisticsDelegate.registerStatistics(bookingInsuranceStatisticRequest);
				logger.debug("Insurance statistics invoked, result is {}", statisticsResponse.getResult());

			} catch (Exception e) {
				logger.debug("An exception occurred invoking insurance statistics.", e);
			}
		}

		if (ctx.invoiceRequired) {
			try {

				RegisterBillingStatisticRequest bookingBillingStatisticRequest = 
						new RegisterBillingStatisticRequest(tid, ctx.sid);
				bookingBillingStatisticRequest.setClientIP(ctx.clientIP);
				bookingBillingStatisticRequest.setSessionId(ctx.sessionId);
				bookingBillingStatisticRequest.setSiteCode(ctx.site);
				bookingBillingStatisticRequest.setType(StatisticsDataTypeEnum.BILLING_REQUEST);
				bookingBillingStatisticRequest.setSuccess(true);

				BillingData billingData = ctx.prenotation.getBilling();

				StringBuilder otherParameters = new StringBuilder("");
				otherParameters.append(billingData.getNome() + "  |  ");
				otherParameters.append("  |  ");
				otherParameters.append(billingData.getLocSped() + "  |  ");
				otherParameters.append(billingData.getIndSped() + "  |  ");
				otherParameters.append(billingData.getCap() + "  |  ");
				otherParameters.append(billingData.getEmailInt());
				bookingBillingStatisticRequest.setOtherParameters(otherParameters.toString());
				bookingBillingStatisticRequest.setParameter1("Si");

				if (InvoiceType.PF.toString().equals(billingData.getFormaSocietaria())) {
					bookingBillingStatisticRequest.setParameter2("-"+billingData.getCfPax());
				} else {
					bookingBillingStatisticRequest.setParameter2("-"+billingData.getCfInt());
				}

				ApplicantPassengerData applicant = null;
				if (retrieveTicketResponse.getPassenger()!=null && retrieveTicketResponse.getPassenger().size()>0){
					for (PassengerBaseData passenger : retrieveTicketResponse.getPassenger()){
						if (passenger instanceof ApplicantPassengerData){
							applicant = (ApplicantPassengerData) passenger;
							break;
						}
					}
				}
				String ticketNum = "";
				if (applicant != null && applicant.getTickets() != null && applicant.getTickets().size()>0)
					ticketNum = applicant.getTickets().get(0).getTicketNumber();
				bookingBillingStatisticRequest.setParameter3(ticketNum);
				bookingBillingStatisticRequest.setParameter4(billingData.getFormaSocietaria());
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");    
				bookingBillingStatisticRequest.setParameter5(df.format(new Date()));

				logger.debug("Registering Insurance Statistics... bookingBillingStatisticRequest: ["+bookingBillingStatisticRequest+"]");
				RegisterStatisticsResponse statisticsResponse = 
						registerStatisticsDelegate.registerStatistics(bookingBillingStatisticRequest);
				logger.debug("Insurance statistics invoked, result is {}", statisticsResponse.getResult());

			} catch (Exception e) {
				logger.debug("An exception occurred invoking billing statistics.", e);
			}
		}

		return retrieveTicketResponse;
	}

	/*
	 * Fourth step of payment
	 * 
	 * @param ctx The booking session context to use.
	 * @param tid transaction ID
	 * @param initializePaymentResponse response of Step 1
	 * @return RetrieveTicketResponse
	 */
	private RetrieveTicketResponse performPaymentRetrieveTicket(BookingSessionContext ctx,
			String tid, PaymentData paymentData, Long timeAfterAuth) throws BookingPaymentException {
		logger.debug("performPaymentRetrieveTickets");

		// create retrieveTicket request
		RetrieveTicketRequest retrieveTicketRequest = new RetrieveTicketRequest();
		retrieveTicketRequest.setSid(ctx.sid);
		retrieveTicketRequest.setTid(tid);
		retrieveTicketRequest.setCookie(ctx.cookie);
		retrieveTicketRequest.setExecution(ctx.execution);
		retrieveTicketRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
		retrieveTicketRequest.setProcess(paymentData.getProcess());
		retrieveTicketRequest.setRoutes(ctx.prenotation);
		retrieveTicketRequest.setTaxs(ctx.selectionTaxes);
		retrieveTicketRequest.setInvoiceRequired(ctx.invoiceRequired);

		RetrieveTicketResponse retrieveTicketResponse = null;
		try {
			// retrieve ticket!
			retrieveTicketResponse = paymentDelegate.retrieveTicket(retrieveTicketRequest);
			ctx.cookie = retrieveTicketResponse.getCookie();
			ctx.execution = retrieveTicketResponse.getExecute();
			ctx.sabreGateWayAuthToken = retrieveTicketResponse.getSabreGateWayAuthToken();
		} catch (Exception e) {
			logger.error("Error during retrieveTicket ", e);
			throw new BookingPaymentException(
					BookingPaymentException.BOOKING_ERROR_RETRIEVETICKETS,
					"Unable to perform retrieveTicket");
		}
		if (retrieveTicketResponse == null || retrieveTicketResponse.getProcess() == null) {
			throw new BookingPaymentException(
					BookingPaymentException.BOOKING_ERROR_RETRIEVETICKETS,
					"Unable to perform retrieveTicket");
		}

		// RETRIEVE TICKET OK
		if (!PaymentTypeEnum.PAY_PAL.equals(ctx.prenotation.getPayment().getType())) {
			RegisterPaymentFromAuthToTicketStatisticRequest authToTicketStatisticRequest = 
					new RegisterPaymentFromAuthToTicketStatisticRequest(tid, ctx.sid);
			authToTicketStatisticRequest.setUserId(ctx.loggedUserProfileData != null ? 
					ctx.loggedUserProfileData.getCustomerNumber() : null);
			authToTicketStatisticRequest.setClientIP(ctx.clientIP);
			authToTicketStatisticRequest.setErrorDescr("");
			authToTicketStatisticRequest.setSessionId(ctx.sessionId);
			authToTicketStatisticRequest.setSiteCode(ctx.site);
			authToTicketStatisticRequest.setSuccess(true);
			long timeAfterTicket = System.currentTimeMillis();
			authToTicketStatisticRequest.setBackofficeTimeMillis(timeAfterTicket - timeAfterAuth);
			authToTicketStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_FROM_AUTHORIZATION_TO_TICKET);

			try {
				RegisterStatisticsResponse statisticResponse = 
						registerStatisticsDelegate.registerStatistics(authToTicketStatisticRequest);

				if (logger.isDebugEnabled())
					logger.debug("Called fromAuthToTicketStatistic. Response: "+ statisticResponse);

				if (statisticResponse == null) {
					logger.error("Error performing authorizeToTicket payment statistic");
				}
			} catch (Exception e) {
				logger.error("Error performing authorizeToTicket payment statistic", e);
			}
		}
		RegisterPaymentBookingEtktStatisticRequest bookingEtktStatisticRequest = 
				new RegisterPaymentBookingEtktStatisticRequest(tid, ctx.sid);
		bookingEtktStatisticRequest.setPnr(ctx.prenotation.getPnr());

		List<TicketInfoData> tickets = null;
		for (PassengerBaseData passenger : retrieveTicketResponse.getPassenger()) {
			if (passenger instanceof ApplicantPassengerData) {
				ApplicantPassengerData pax = (ApplicantPassengerData) passenger;
				tickets = pax.getTickets();
			}
		}
		String eticket = "";
		if (tickets != null && !tickets.isEmpty()) {
			eticket = tickets.get(0).getTicketNumber();
		}
		bookingEtktStatisticRequest.setEticket(eticket);
		String importo = "";
		try {
			importo = NumberFormat.getInstance(ctx.locale).format(ctx.grossAmount.doubleValue());
		}catch(Exception e) {
			logger.error("Error casting gross amount to string", e);
		}
		bookingEtktStatisticRequest.setImporto(importo);

		bookingEtktStatisticRequest.setUserId(ctx.loggedUserProfileData!=null ? ctx.loggedUserProfileData.getCustomerNumber() : null);
		bookingEtktStatisticRequest.setClientIP(ctx.clientIP);
		bookingEtktStatisticRequest.setSessionId(ctx.sessionId);
		bookingEtktStatisticRequest.setSiteCode(ctx.site);
		bookingEtktStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_BOOKING_ETKT);
		bookingEtktStatisticRequest.setSuccess(true);

		try {
			RegisterStatisticsResponse statisticResponse = registerStatisticsDelegate.registerStatistics(bookingEtktStatisticRequest);
			logger.debug("Called paymentBookingEtktStatistic. Response: "+statisticResponse);
			if (statisticResponse == null) {
				logger.error("Error performing authorize payment statistic");
			}
		} catch(Exception e) {
			logger.error("Error performing authorize payment statistic", e);
		}

		//Insurance Statistics
		if (ctx.insuranceProposalData!=null && ctx.insuranceProposalData.getBuy()){
			try {

				RegisterPaymentBookingInsuranceStatisticRequest bookingInsuranceStatisticRequest = 
						new RegisterPaymentBookingInsuranceStatisticRequest(tid, ctx.sid);
				bookingInsuranceStatisticRequest.setClientIP(ctx.clientIP);
				bookingInsuranceStatisticRequest.setSessionId(ctx.sessionId);
				bookingInsuranceStatisticRequest.setSiteCode(ctx.site);
				bookingInsuranceStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_BOOKING_INSURANCE);
				bookingInsuranceStatisticRequest.setCardType(((PaymentProviderCreditCardData)ctx.prenotation.getPayment().getProvider()).getType().value());
				bookingInsuranceStatisticRequest.setSuccess(true);
				if (ctx.insuranceProposalData.getPolicyNumber()!=null){
					bookingInsuranceStatisticRequest.setPolicyNumber(ctx.insuranceProposalData.getPolicyNumber());
				} else {
					bookingInsuranceStatisticRequest.setPolicyNumber("(unknown - PNR is " + ctx.prenotation.getPnr() + ")");
				}
				bookingInsuranceStatisticRequest.setErrorDescr(ctx.insuranceProposalData.getErrorDescription());

				logger.debug("Registering Insurance Statistics... bookingInsuranceStatisticRequest: ["+bookingInsuranceStatisticRequest+"]");
				RegisterStatisticsResponse statisticsResponse = 
						registerStatisticsDelegate.registerStatistics(bookingInsuranceStatisticRequest);
				logger.debug("Insurance statistics invoked, result is {}", statisticsResponse.getResult());

			} catch (Exception e) {
				logger.debug("An exception occurred invoking insurance statistics.", e);
			}
		}

		if (ctx.invoiceRequired) {
			try {

				RegisterBillingStatisticRequest bookingBillingStatisticRequest = 
						new RegisterBillingStatisticRequest(tid, ctx.sid);
				bookingBillingStatisticRequest.setClientIP(ctx.clientIP);
				bookingBillingStatisticRequest.setSessionId(ctx.sessionId);
				bookingBillingStatisticRequest.setSiteCode(ctx.site);
				bookingBillingStatisticRequest.setType(StatisticsDataTypeEnum.BILLING_REQUEST);
				bookingBillingStatisticRequest.setSuccess(true);

				BillingData billingData = ctx.prenotation.getBilling();

				StringBuilder otherParameters = new StringBuilder("");
				otherParameters.append(billingData.getNome() + "  |  ");
				otherParameters.append("  |  ");
				otherParameters.append(billingData.getLocSped() + "  |  ");
				otherParameters.append(billingData.getIndSped() + "  |  ");
				otherParameters.append(billingData.getCap() + "  |  ");
				otherParameters.append(billingData.getEmailInt());
				bookingBillingStatisticRequest.setOtherParameters(otherParameters.toString());
				bookingBillingStatisticRequest.setParameter1("Si");

				if (InvoiceType.PF.toString().equals(billingData.getFormaSocietaria())) {
					bookingBillingStatisticRequest.setParameter2("-"+billingData.getCfPax());
				} else {
					bookingBillingStatisticRequest.setParameter2("-"+billingData.getCfInt());
				}

				ApplicantPassengerData applicant = null;
				if (retrieveTicketResponse.getPassenger()!=null && retrieveTicketResponse.getPassenger().size()>0){
					for (PassengerBaseData passenger : retrieveTicketResponse.getPassenger()){
						if (passenger instanceof ApplicantPassengerData){
							applicant = (ApplicantPassengerData) passenger;
							break;
						}
					}
				}
				String ticketNum = "";
				if (applicant != null && applicant.getTickets() != null && applicant.getTickets().size()>0)
					ticketNum = applicant.getTickets().get(0).getTicketNumber();
				bookingBillingStatisticRequest.setParameter3(ticketNum);
				bookingBillingStatisticRequest.setParameter4(billingData.getFormaSocietaria());
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");    
				bookingBillingStatisticRequest.setParameter5(df.format(new Date()));

				logger.debug("Registering Insurance Statistics... bookingBillingStatisticRequest: ["+bookingBillingStatisticRequest+"]");
				RegisterStatisticsResponse statisticsResponse = 
						registerStatisticsDelegate.registerStatistics(bookingBillingStatisticRequest);
				logger.debug("Insurance statistics invoked, result is {}", statisticsResponse.getResult());

			} catch (Exception e) {
				logger.debug("An exception occurred invoking billing statistics.", e);
			}
		}

		return retrieveTicketResponse;
	}

	private boolean sendNotificationCarnetEmail(BookingSessionContext ctx,
			String tid) {

		/* Setup response */
		String htmlMail = "";
		try {
			logger.debug("retriving email body from jcr");
			ByteArrayOutputStream out = new ByteArrayOutputStream();

			/* Get resource resolver */
			@SuppressWarnings("deprecation")
			ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			String requestedUrl = ctx.baseUrl + configuration.getCarnetMailNotificaUtilizzoCarnetPage();

			/* Setup request */
			HttpServletRequest req = requestResponseFactory.createRequest("GET", requestedUrl);
			req.setAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE, ctx);
			req.setAttribute(BookingConstants.PAYMENT_DATA_ATTRIBUTE, ctx.paymentDataForPDF);
			WCMMode.DISABLED.toRequest(req);

			out = new ByteArrayOutputStream();
			HttpServletResponse resp = requestResponseFactory.createResponse(out);

			/* Process request through Sling */
			requestProcessor.processRequest(req, resp, resourceResolver);

			String encoding = "UTF-8";

			htmlMail = new String(out.toByteArray(), Charset.forName(encoding));

		} catch (Exception e) {
			logger.error("Generic exception while retrieving carnet notification email template from JCR repository: {}", e);
			return false;
		}

		if (!htmlMail.isEmpty()) {
			logger.debug("crating email body");
			CarnetSendNotificationEmailRequest notificationEmailRequest = 
					new CarnetSendNotificationEmailRequest();

			notificationEmailRequest.setSid(ctx.sid);
			notificationEmailRequest.setTid(tid);
			notificationEmailRequest.setCookie(ctx.cookie);
			notificationEmailRequest.setExecution(ctx.execution);
			notificationEmailRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);

			InfoCarnetData carnet = new InfoCarnetData();
			carnet.setBuyerEmail(ctx.infoCarnet.getBuyerEmail());
			carnet.setBuyerFirstName(ctx.infoCarnet.getBuyerName());
			carnet.setBuyerLastName(ctx.infoCarnet.getBuyerLastname());
			carnet.setCarnetCode(ctx.infoCarnet.getCarnetCode());
			carnet.setCarnetPassword(ctx.infoCarnet.getPassword());
			carnet.setExpiryDate(ctx.infoCarnet.getExpiryDate());
			carnet.setResidualRoutes(ctx.infoCarnet.getResidualRoutes());
			carnet.setTotalFare(ctx.infoCarnet.getTotalFare());
			carnet.setTotalRoutes(ctx.infoCarnet.getTotalRoutes());

			notificationEmailRequest.setCarnetInfo(carnet);
			notificationEmailRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
			notificationEmailRequest.setMarketCode(ctx.market);
			notificationEmailRequest.setNotificationMailHtml(htmlMail);
			notificationEmailRequest.setPrenotation(null);

			CarnetSendNotificationEmailResponse notificationEmailResponse = null;
			try {
				notificationEmailResponse = ticketDelegate.sendCarnetNotificationEmail(notificationEmailRequest);	
				ctx.cookie = notificationEmailResponse.getCookie();
				ctx.execution = notificationEmailResponse.getExecute();
				ctx.sabreGateWayAuthToken = notificationEmailResponse.getSabreGateWayAuthToken();
			} catch (Exception e) {
				logger.error("Error send email notification carnet. ", e);
				return false;
			}
			if (notificationEmailResponse != null && notificationEmailResponse.getHaveSendEmail() != null
					&& notificationEmailResponse.getHaveSendEmail()) {
				return true;
			}

		} else {
			logger.error("HTML notification email body is empty");
		}

		return false;
	}


	private boolean performUpdateCarnet(BookingSessionContext ctx, String tid) {
		int totalPax = ctx.searchPassengersNumber.getNumAdults() + 
				ctx.searchPassengersNumber.getNumYoung() + 
				ctx.searchPassengersNumber.getNumChildren() + 
				ctx.searchPassengersNumber.getNumInfants();

		int usedRoutes = (ctx.totalSlices * totalPax);

		CarnetUpdateRequest requestUpdateCarnet = new CarnetUpdateRequest();
		requestUpdateCarnet.setSid(ctx.sid);
		requestUpdateCarnet.setTid(tid);
		requestUpdateCarnet.setAmount(ctx.grossAmount);
		requestUpdateCarnet.setInfoCarnet(ctx.infoCarnet);
		requestUpdateCarnet.setRoutesNumber((short) usedRoutes);

		CarnetUpdateResponse responseUpdateCarnet = null;
		try {
			responseUpdateCarnet = carnetDelegate.updateCarnet(requestUpdateCarnet);
		} catch (Exception e) {
			logger.error("Error executing updateCarnet", e);
			return false;
		}

		if (responseUpdateCarnet != null 
				&& responseUpdateCarnet.getInfoCarnet() != null) {
			if(ctx != null){
				ctx.infoCarnet = responseUpdateCarnet.getInfoCarnet();
			}
			return true;
		}
		return false;
	}

	/* internal methods */

	private boolean checkContinuitaTerritorialeSardiniaPeriod(List<SearchElement> searchElements, boolean isRoundTrip) {
		Calendar departureDate = searchElements.get(0).getDepartureDate();
		Calendar returnDate = null;
		Calendar firstDate = createDate(departureDate, CONTINUITY_SARDINIA_DATE_FROM);
		Calendar secondDate = createDate(departureDate, CONTINUITY_SARDINIA_DATE_TO);

		boolean isContinuityPeriodOutbound = false;
		if(!(departureDate.before(firstDate) || departureDate.after(secondDate))){
			isContinuityPeriodOutbound = true;
		}

		boolean isContinuityPeriodReturn = false;
		if (isRoundTrip) {
			returnDate = searchElements.get(1).getDepartureDate();
			if (!(returnDate.before(firstDate) || returnDate.after(secondDate))) {
				isContinuityPeriodReturn = true;
			}
		}

		return isContinuityPeriodOutbound || isContinuityPeriodReturn;
	}

	private boolean checkContinuitaTerritorialeOlbiaSardiniaPeriod(List<SearchElement> searchElements, boolean isRoundTrip) {
		Calendar departureDate = searchElements.get(0).getDepartureDate();
		Calendar returnDate = null;
		Calendar firstDate = createDate(departureDate, CONTINUITY_SARDINIA_OLBIA_DATE_FROM);
		Calendar secondDate = createDate(departureDate, CONTINUITY_SARDINIA_OLBIA_DATE_TO);

		boolean isContinuityPeriodOutbound = false;
		if(!(departureDate.before(firstDate) || departureDate.after(secondDate))){
			isContinuityPeriodOutbound = true;
		}

		boolean isContinuityPeriodReturn = false;
		if (isRoundTrip) {
			returnDate = searchElements.get(1).getDepartureDate();
			if (!(returnDate.before(firstDate) || returnDate.after(secondDate))) {
				isContinuityPeriodReturn = true;
			}
		}

		return isContinuityPeriodOutbound || isContinuityPeriodReturn;
	}

	private SearchDestinationData[] createSearchDestination(BookingSessionContext ctx){

		SearchDestinationData[] searchDestinations = new SearchDestinationData[ctx.searchElements.size()];

		String[] selectedOffers = new String[ctx.searchElements.size()];
		for (int i = 0; i < ctx.flightSelections.length; i++) {
			if (ctx.flightSelections[i] != null) {
				selectedOffers[i] = ctx.flightSelections[i].getSolutionId();
			}
		}

		int index=0;
		for(SearchElement searchElement : ctx.searchElements){
			SearchElementAirport from = searchElement.getFrom();
			SearchElementAirport to = searchElement.getTo();

			// departure airport
			AirportData fromAirport = new AirportData();
			fromAirport.setCityCode(from.getCityCode());
			fromAirport.setCode(from.getAirportCode());
			fromAirport.setCountryCode(from.getCountryCode());

			// destination airport
			AirportData toAirport = new AirportData();
			toAirport.setCityCode(to.getCityCode());
			toAirport.setCode(to.getAirportCode());
			toAirport.setCountryCode(to.getCountryCode());

			Calendar departureDate = searchElement.getDepartureDate();
			departureDate.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));

			if (ctx.availableFlights != null && ctx.searchKind != BookingSearchKindEnum.MULTILEG
					&& !ctx.isCarnetProcess) {
				TabData tab = ctx.availableFlights.getTabs().get(index);
				departureDate = tab.getFlightTabs()
						.get(ctx.selectedDepartureDateChoices[index]).getDate();
				if (index == 0 && tab.getType() == RouteTypeEnum.RETURN) {
					tab = ctx.availableFlights.getTabs().get(1);
					departureDate = tab.getFlightTabs()
							.get(ctx.selectedDepartureDateChoices[index]).getDate();
				}
				if (index == 1 && tab.getType() == RouteTypeEnum.OUTBOUND) {
					tab = ctx.availableFlights.getTabs().get(0);
					departureDate = tab.getFlightTabs()
							.get(ctx.selectedDepartureDateChoices[index]).getDate();
				}

			}

			// destination data
			SearchDestinationData destination = new SearchDestinationData();
			destination.setFromAirport(fromAirport);
			destination.setToAirport(toAirport);
			if (selectedOffers != null && selectedOffers[index] != null) {
				destination.setSelectedOffer(selectedOffers[index]);
			}
			if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP && index == 1) {
				destination.setRouteType(RouteTypeEnum.RETURN);
			} else {
				destination.setRouteType(RouteTypeEnum.OUTBOUND);
			}
			destination.setTimeType(TimeTypeEnum.ANYTIME);
			destination.setDepartureDate(departureDate);
			searchDestinations[index] = destination;
			index++;
		}

		return searchDestinations;
	}

	private SearchDestinationData[] createMultilegSearchDestination(BookingSessionContext ctx){

		SearchDestinationData[] searchDestinations = new SearchDestinationData[ctx.searchElements.size()];

		String[] selectedOffers = new String[ctx.searchElements.size()];

		if (ctx.currentSliceIndex<2){
			for (int i = 0; i < ctx.flightSelections.length; i++) {
				if (ctx.flightSelections[i] != null) {
					selectedOffers[i] = ctx.flightSelections[i].getSolutionId();
				}
			}
		} else {
			//Prendo solutionId da ultimo volo selezionato
			FlightSelection	lastFlightSelected = ctx.flightSelections[ctx.currentSliceIndex-1];
			if (lastFlightSelected != null && lastFlightSelected.getFlightData() != null){
				for (BrandData brand : lastFlightSelected.getFlightData().getBrands()){
					if (brand.getCode().equals(lastFlightSelected.getSelectedBrandCode())){
						String[] previousSolutionId = brand.getSolutionId().split("\\|");
						int i;
						for (i=0; i<previousSolutionId.length; i++){
							selectedOffers[i] = previousSolutionId[i];
						}
						selectedOffers[i] = brand.getRefreshSolutionId();
						break;
					}
				}
			}
		}

		int index=0;
		for(SearchElement searchElement : ctx.searchElements){
			SearchElementAirport from = searchElement.getFrom();
			SearchElementAirport to = searchElement.getTo();

			// departure airport
			AirportData fromAirport = new AirportData();
			fromAirport.setCityCode(from.getCityCode());
			fromAirport.setCode(from.getAirportCode());
			fromAirport.setCountryCode(from.getCountryCode());

			// destination airport
			AirportData toAirport = new AirportData();
			toAirport.setCityCode(to.getCityCode());
			toAirport.setCode(to.getAirportCode());
			toAirport.setCountryCode(to.getCountryCode());

			Calendar departureDate = searchElement.getDepartureDate();
			departureDate.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));

			if (ctx.availableFlights != null && ctx.searchKind != BookingSearchKindEnum.MULTILEG
					&& !ctx.isCarnetProcess) {
				TabData tab = ctx.availableFlights.getTabs().get(index);
				departureDate = tab.getFlightTabs()
						.get(ctx.selectedDepartureDateChoices[index]).getDate();
				if (index == 0 && tab.getType() == RouteTypeEnum.RETURN) {
					tab = ctx.availableFlights.getTabs().get(1);
					departureDate = tab.getFlightTabs()
							.get(ctx.selectedDepartureDateChoices[index]).getDate();
				}
				if (index == 1 && tab.getType() == RouteTypeEnum.OUTBOUND) {
					tab = ctx.availableFlights.getTabs().get(0);
					departureDate = tab.getFlightTabs()
							.get(ctx.selectedDepartureDateChoices[index]).getDate();
				}

			}

			// destination data
			SearchDestinationData destination = new SearchDestinationData();
			destination.setFromAirport(fromAirport);
			destination.setToAirport(toAirport);
			if (selectedOffers != null && selectedOffers[index] != null) {
				destination.setSelectedOffer(selectedOffers[index]);
			}
			if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP && index == 1) {
				destination.setRouteType(RouteTypeEnum.RETURN);
			} else {
				destination.setRouteType(RouteTypeEnum.OUTBOUND);
			}
			destination.setTimeType(TimeTypeEnum.ANYTIME);
			destination.setDepartureDate(departureDate);
			searchDestinations[index] = destination;
			index++;
		}

		return searchDestinations;
	}

	private SearchFlightSolutionRequest createMultilegSearchRequest(String tid,
			BookingSessionContext ctx) {

		SearchDestinationData[] searchDestinations = createMultilegSearchDestination(ctx);

		MultiSliceSearchData multiSliceSearchData = new MultiSliceSearchData();
		multiSliceSearchData.setCug(ctx.cug.value());
		multiSliceSearchData.setOnlyDirectFlight(false);
		multiSliceSearchData.setType(SearchTypeEnum.BRAND_SEARCH);
		multiSliceSearchData.setMarket(ctx.market);
		multiSliceSearchData.setSearchCabin(CabinEnum.ECONOMY);
		multiSliceSearchData.setSearchCabinType(CabinTypeEnum.PERMITTED);

		for (SearchDestinationData searchDestination : searchDestinations) {
			multiSliceSearchData.addDestination(searchDestination);
		}
		for ( PassengerNumbersData passengerNumber : setPassengersNumber(ctx.searchPassengersNumber) ) {
			multiSliceSearchData.addPassengerNumbers(passengerNumber);
		}

		String fromAriportCode = searchDestinations[0].getFromAirport().getCode();
		String toAriportCode = searchDestinations[0].getToAirport().getCode();

		SearchFlightSolutionRequest searchRequest = new SearchFlightSolutionRequest(tid, ctx.sid);
		searchRequest.setCookie(ctx.cookie);
		searchRequest.setExecution(ctx.execution);
		searchRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
		//INIZIO *** MODIFICA IP ***
		if (ctx.ipAddress != null && !ctx.ipAddress.isEmpty()) {
			searchRequest.setIpAddress(ctx.ipAddress);
		}
		//FINE.
		searchRequest.setFilter(multiSliceSearchData);
		searchRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		searchRequest.setMarket(ctx.market);
		searchRequest.setMarketExtraCharge(ctx.site);
		searchRequest.setResponseType(SearchExecuteResponseType.MODEL);
		searchRequest.setRibbonOutboundStartDate(null);
		searchRequest.setRibbonReturnStartDate(null);
		searchRequest.setFromSearchElement(fromAriportCode);
		searchRequest.setToSearchElement(toAriportCode);
		return searchRequest;
	}

	private SearchFlightSolutionRequest createFlightCarnetSearchRequest(
			String tid, BookingSessionContext ctx) {

		SearchDestinationData[] searchDestinations = createSearchDestination(ctx);

		CarnetSearchData carnetSearchData = new CarnetSearchData();
		carnetSearchData.setCug(ctx.cug.value());
		carnetSearchData.setOnlyDirectFlight(false);
		carnetSearchData.setType(SearchTypeEnum.BRAND_SEARCH);
		carnetSearchData.setMarket(ctx.market);
		carnetSearchData.setResidency(ctx.residency);
		carnetSearchData.setCarnetQuantity(ctx.infoCarnet.getTotalRoutes().intValue());
		for (SearchDestinationData searchDestination : searchDestinations) {
			carnetSearchData.addDestination(searchDestination);
		}
		for ( PassengerNumbersData passengerNumber : setPassengersNumber(ctx.searchPassengersNumber) ) {
			carnetSearchData.addPassengerNumbers(passengerNumber);
		}

		String fromAriportCode = searchDestinations[0].getFromAirport().getCode();
		String toAriportCode = searchDestinations[0].getToAirport().getCode();

		// create search request object
		SearchFlightSolutionRequest searchRequest = new SearchFlightSolutionRequest(tid, ctx.sid);
		searchRequest.setCookie(ctx.cookie);
		searchRequest.setExecution(ctx.execution);
		searchRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
		//INIZIO *** MODIFICA IP ***
		if (ctx.ipAddress != null && !ctx.ipAddress.isEmpty()) {
			searchRequest.setIpAddress(ctx.ipAddress);
		}
		//FINE.
		searchRequest.setFilter(carnetSearchData);
		searchRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		searchRequest.setMarket(ctx.market);
		searchRequest.setMarketExtraCharge(ctx.market);
		searchRequest.setResponseType(SearchExecuteResponseType.MODEL);
		searchRequest.setFromSearchElement(fromAriportCode);
		searchRequest.setToSearchElement(toAriportCode);
		searchRequest.setCug(ctx.cug.value());

		Calendar calendarOutbound = Calendar.getInstance();
		Calendar calendarReturn = Calendar.getInstance();

		setDateForRibbonSearch(calendarOutbound,calendarReturn, carnetSearchData);
		searchRequest.setRibbonOutboundStartDate(calendarOutbound);

		if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP) {
			// set arrival ribbon RETURN START date
			searchRequest.setRibbonReturnStartDate(calendarReturn);
		}

		return searchRequest;

	}

	private SearchFlightSolutionRequest createFlightBrandSearchRequest(String tid, 
			BookingSessionContext ctx) {

		SearchDestinationData[] searchDestinations = createSearchDestination(ctx);

		BrandSearchData brandSearchData = new BrandSearchData();
		brandSearchData.setCug(ctx.cug.value());
		brandSearchData.setOnlyDirectFlight(false);
		brandSearchData.setType(SearchTypeEnum.BRAND_SEARCH);
		brandSearchData.setMarket(ctx.market);
		logger.debug("isAllowedContinuitaTerritoriale: ["+ctx.isAllowedContinuitaTerritoriale+"]");
		if (ctx.isAllowedContinuitaTerritoriale) {
			brandSearchData.setResidency(ctx.residency);
			String from = ctx.searchElements.get(0).getFrom().getAirportCode();
			String to = ctx.searchElements.get(0).getTo().getAirportCode();
			logger.debug("bandoPeriod: ["+searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(from, to)+"]");
			brandSearchData.setBandoPeriod(searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(from, to) || searchFlightsDelegate.isSliceSardiniaTerritorialContinuityOlbia(from, to));
		}
		for (SearchDestinationData searchDestination : searchDestinations) {
			brandSearchData.addDestination(searchDestination);
		}
		for ( PassengerNumbersData passengerNumber : setPassengersNumber(ctx.searchPassengersNumber) ) {
			brandSearchData.addPassengerNumbers(passengerNumber);
		}

		String fromAriportCode = searchDestinations[0].getFromAirport().getCode();
		String toAriportCode = searchDestinations[0].getToAirport().getCode();


		// create search request object
		SearchFlightSolutionRequest searchRequest = new SearchFlightSolutionRequest(tid, ctx.sid);
		searchRequest.setCookie(ctx.cookie);
		searchRequest.setExecution(ctx.execution);
		searchRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
		//INIZIO *** MODIFICA IP ***
		if (ctx.ipAddress != null && !ctx.ipAddress.isEmpty()) {
			searchRequest.setIpAddress(ctx.ipAddress);
		}
		//FINE.
		searchRequest.setFilter(brandSearchData);
		searchRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		searchRequest.setMarket(ctx.market);
		searchRequest.setMarketExtraCharge(ctx.market);
		searchRequest.setResponseType(SearchExecuteResponseType.MODEL);
		searchRequest.setFromSearchElement(fromAriportCode);
		searchRequest.setToSearchElement(toAriportCode);
		searchRequest.setCug(ctx.cug.value());

		Calendar calendarOutbound = Calendar.getInstance();
		Calendar calendarReturn = Calendar.getInstance();

		setDateForRibbonSearch(calendarOutbound,calendarReturn, brandSearchData);
		searchRequest.setRibbonOutboundStartDate(calendarOutbound);

		if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP) {
			// set arrival ribbon RETURN START date
			searchRequest.setRibbonReturnStartDate(calendarReturn);
		}

		return searchRequest;
	}

	private void setDateForRibbonSearch(Calendar calendarOutbound,
			Calendar calendarReturn, BrandSearchData brandSearchData) {

		for ( SearchDestinationData destination : brandSearchData.getDestinations()) {
			if (destination.getRouteType() == RouteTypeEnum.OUTBOUND) {
				calendarOutbound.setTimeInMillis(destination.getDepartureDate().getTimeInMillis());
				calendarOutbound.setTimeZone(destination.getDepartureDate().getTimeZone());
				calendarOutbound.add(Calendar.DATE, -INDEX_START_RIBBON_DATE);
			} else {
				calendarReturn.setTimeInMillis(destination.getDepartureDate().getTimeInMillis());
				calendarReturn.setTimeZone(destination.getDepartureDate().getTimeZone());
				calendarReturn.add(Calendar.DATE, -INDEX_START_RIBBON_DATE);
			}
		}
	}

	private List<PassengerNumbersData> setPassengersNumber(SearchPassengersNumber np) {

		List<PassengerNumbersData> listPassengerNumbersData = new ArrayList<PassengerNumbersData>();

		if (np.getNumAdults() > 0) {
			PassengerNumbersData adultPassengersNumber = new PassengerNumbersData();
			adultPassengersNumber.setNumber(np.getNumAdults());
			adultPassengersNumber.setPassengerType(PassengerTypeEnum.ADULT);
			listPassengerNumbersData.add(adultPassengersNumber);
		}

		if (np.getNumChildren() > 0) {
			PassengerNumbersData childPassengersNumber = new PassengerNumbersData();
			childPassengersNumber.setNumber(np.getNumChildren());
			childPassengersNumber.setPassengerType(PassengerTypeEnum.CHILD);
			listPassengerNumbersData.add(childPassengersNumber);
		}

		if (np.getNumInfants() > 0) {
			PassengerNumbersData infantPassengersNumber = new PassengerNumbersData();
			infantPassengersNumber.setNumber(np.getNumInfants());
			infantPassengersNumber.setPassengerType(PassengerTypeEnum.INFANT);
			listPassengerNumbersData.add(infantPassengersNumber);
		}

		if (np.getNumYoung() > 0) {
			PassengerNumbersData youthPassengersNumber = new PassengerNumbersData();
			youthPassengersNumber.setNumber(np.getNumYoung());
			youthPassengersNumber.setPassengerType(PassengerTypeEnum.YOUTH);
			listPassengerNumbersData.add(youthPassengersNumber);
		}

		return listPassengerNumbersData;

	}

	private void performBookingSellupAndTaxSearch(String tid, BookingSessionContext ctx, String solutionId, FlightData flightData) {

		boolean neutral = setNeutralFieldAndPrepareSellup(ctx, ctx.flightSelections[0].getFlightData());
		if (ctx.flightSelections.length == 2) {
			neutral &= setNeutralFieldAndPrepareSellup(ctx, ctx.flightSelections[1].getFlightData());
		}

		SearchFlightSolutionRequest sellupRequest = createBookingSellupRequest(tid, ctx.sid, ctx, solutionId, neutral);

		// perform the sell-up and save the response data in ctx.selectionRoutes
		SearchBookingSolutionResponse bookingSellupResponse = searchFlightsDelegate.bookingSellup(sellupRequest);
		if (bookingSellupResponse == null) {
			logger.error("The Sellup service has failed");
			throw new IllegalStateException("The Sellup service has failed");
		}
		if(bookingSellupResponse.getRoutesData() == null){
			logger.error("The Sellup service has not returned solutions");
			throw new IllegalStateException("The Sellup service has not returned solutions");
		}
		ctx.cookie = bookingSellupResponse.getCookie();
		ctx.execution = bookingSellupResponse.getExecute();
		ctx.sabreGateWayAuthToken = bookingSellupResponse.getSabreGateWayAuthToken();
		logger.info("BookingSellup executed. Cookie: ["+ctx.cookie+"], Execution: ["+ctx.execution+"], SabreGateWayAuthToken: ["+ctx.sabreGateWayAuthToken+"]");

		// Aggiornamento Fare Rules
/*		if (ctx.flightSelections != null && bookingSellupResponse.getRoutesData() != null){
			logger.debug("Changing FareRules after Sellup");
			for(int i=0; i<ctx.flightSelections.length; i++){
				List<Integer> flightsMileage = ctx.flightSelections[i].getFlightsMileage();
				FlightData flight = bookingSellupResponse.getRoutesData().getRoutesList().get(i).getFlights().get(0);
				BrandData brand = flight.getBrands().get(0);
				Map<String, String> fareRules = computeFareRules(brand, flight, flightsMileage, i, ctx);
				if (fareRules.containsKey("booking.regoleTariffarie.bagaglioInStiva")) {
					ctx.flightSelections[i].setOnlyHandBaggage(fareRules.get("booking.regoleTariffarie.bagaglioInStiva").equals("false"));
				}
				if (ctx.flightSelections[i].getFareRules() != null){
					for (String s : ctx.flightSelections[i].getFareRules().keySet()){
						logger.debug("Old FareRule "+s+": "+ctx.flightSelections[i].getFareRules().get(s));
					}
				}
				ctx.flightSelections[i].setFareRules(fareRules);
				if (fareRules != null){
					for (String s : fareRules.keySet()){
						logger.debug("New FareRule "+s+": "+fareRules.get(s));
					}
				}
			}
		}*/


		if (bookingSellupResponse.getRoutesData() != null &&
				bookingSellupResponse.getRoutesData().getPassengers() != null &&
				bookingSellupResponse.getRoutesData().getPassengers().size() == ctx.extraChargePassengerList.size()) {

			for (int i = 0; i < bookingSellupResponse.getRoutesData().getPassengers().size(); i++) {
				PassengerBaseData passenger = (PassengerBaseData) ctx.extraChargePassengerList.get(i);
				passenger.setExtraCharge(bookingSellupResponse.getRoutesData().getPassengers().get(i).getExtraCharge());
			}
		}

		manageExtraCharge(ctx);
		ctx.totalExtraCharges = computeTotalExtraChargesAmount(ctx);

		// perform the taxes search, using the results of the sell-up
		//		SearchFlightSolutionResponse bookingTaxSearchResponse = searchFlightsDelegate.bookingTaxSearch(
		//				createBookingTaxSearchRequest(tid, ctx.sid, ctx, solutionId, bookingSellupResponse.getRoutesData(), neutral));
		//		if(bookingTaxSearchResponse == null){
		//			logger.error("The TaxSearch service has failed");
		//			throw new IllegalStateException("The TaxSearch service has failed");
		//		}
		// both requests was performed successfully, update the booking context

		/*List<PassengerBaseData> passengers = new ArrayList<>();
		for (PassengerBase pass : ((BookingSearchData)sellupRequest.getFilter()).getPassengers()){
			passengers.add((PassengerBaseData) pass);
		}

		bookingSellupResponse.getRoutesData().setPassengers(passengers);*/

		ctx.selectionRoutes = bookingSellupResponse.getRoutesData();

		ctx.resultBookingDetails = bookingSellupResponse.getRoutesData().getProperties();
		if (bookingSellupResponse.getAvailableFlightsTaxes() != null){
			logger.debug("AvailableFlightsTaxes: [{}]", bookingSellupResponse.getAvailableFlightsTaxes().getTaxes());
			ctx.selectionTaxes = bookingSellupResponse.getAvailableFlightsTaxes().getTaxes();
		} else
			logger.debug("AvailableFlightsTaxes null.");
		ctx.cookie = bookingSellupResponse.getCookie();
		ctx.execution = bookingSellupResponse.getExecute();
		ctx.sabreGateWayAuthToken = bookingSellupResponse.getSabreGateWayAuthToken();


		// set currency booking process for Multileg
		/*for ( TaxData taxData : ctx.selectionTaxes) {
			if (taxData.getCurrency() != null && !("").equals(taxData.getCurrency())) {
				ctx.currency = taxData.getCurrency();
				break;
			}
		}*/

		if (ctx.selectionRoutes != null && ctx.selectionRoutes.getRoutesList() != null) {
			ctx.selectionRoutes.setSliceCount(ctx.selectionRoutes.getRoutesList().size());
		}

		/*Saving brand data for analytics*/
		ctx.preSellupBrandData = new LinkedList<BrandData>();
		for(FlightSelection sel : ctx.flightSelections){
			BrandData brand = null;
			if(sel.getFlightData() != null && sel.getFlightData().getBrands() != null
					&& sel.getFlightData().getBrands().size() > 0){
				brand = AlitaliaUtils.getBrandInfo(sel.getFlightData().getBrands(), sel.getSelectedBrandCode(), configuration.getMinifareSourceBrandCode(), configuration.getMinifareTargetBrandCode());
			}
			ctx.preSellupBrandData.add(brand);
		}

		// for convenience, we copy the flight data obtained from
		// ctx.selectionRoutes also in the ctx.flightSelections data
		if ( ctx.searchKind != BookingSearchKindEnum.MULTILEG ) {
			for (int i = 0; i < ctx.selectionRoutes.getRoutesList().size(); i++) {
				RouteData  route = ctx.selectionRoutes.getRoutesList().get(i);
				if ( i == 0 && route.getType() == RouteTypeEnum.RETURN ) {
					route = ctx.selectionRoutes.getRoutesList().get(1);
				}
				if ( i==1 && route.getType() == RouteTypeEnum.OUTBOUND ){
					route = ctx.selectionRoutes.getRoutesList().get(0);
				}
				ctx.flightSelections[i].setFlightData(route.getFlights().get(0));
			}
		} else {
			for (int i = 0; i < ctx.selectionRoutes.getRoutesList().size(); i++) {
				ctx.flightSelections[i].setFlightData(
						ctx.selectionRoutes.getRoutesList().get(i).getFlights().get(0));
			}
		}

		// save a copy of the original passengers prices data, to allow clearing of the applied coupon
		ctx.selectionRoutesPassengersWithoutCoupon = new ArrayList<>();
		for (PassengerBaseData passengerData : ctx.selectionRoutes.getPassengers()) {
			PassengerBaseData passengerDataCopy = new PassengerBaseData();
			passengerDataCopy.setCouponPrice(passengerData.getCouponPrice());
			passengerDataCopy.setExtraCharge(passengerData.getExtraCharge());
			passengerDataCopy.setFee(passengerData.getFee());
			passengerDataCopy.setGrossFare(passengerData.getGrossFare());
			passengerDataCopy.setNetFare(passengerData.getNetFare());
			ctx.selectionRoutesPassengersWithoutCoupon.add(passengerDataCopy);
		}
	}

	/**
	 * the neutral field must be true for:
	 * <ul>
	 * <li>Multileg</li>
	 * <li>Continuità Territoriale</li>
	 * <li>Youth</li>
	 * <li>Family</li>
	 * <li>Military</li>
	 * <li>INTC</li>
	 * <li>INTZ</li>
	 * </ul>
	 * 
	 * @param flightData
	 * @return
	 */
	public boolean setNeutralFieldAndPrepareSellup(BookingSessionContext ctx, FlightData flightData) {

		boolean isMultiSlice = ctx.searchKind == BookingSearchKindEnum.MULTILEG;

		//multislice
		if(isMultiSlice) {
			return true;
		} else {
			//Territorial Continuity
			if (ctx.isAllowedContinuitaTerritoriale != null && ctx.isAllowedContinuitaTerritoriale) {
				if (ctx.isSelectedContinuitaTerritoriale != null && ctx.isSelectedContinuitaTerritoriale != null) {
					return true;
				}
			}
			//search Military, Family, Military
			if (ctx.cug != null && ctx.cug != BookingSearchCUGEnum.ADT) {
				return true;
			}
			//AREA: INTC e INTZ
			if (flightData instanceof ConnectingFlightData) {
				for (FlightData flight : ((ConnectingFlightData)flightData).getFlights() ) {
					DirectFlightData directFlight = (DirectFlightData) flight;
					AreaValueEnum areaFrom = directFlight.getFrom().getArea();
					AreaValueEnum areaTo = directFlight.getTo().getArea();
					if (areaFrom == AreaValueEnum.INTZ || areaFrom == AreaValueEnum.INTC) {
						return true;
					}
					if (areaTo == AreaValueEnum.INTZ || areaTo == AreaValueEnum.INTC) {
						return true;
					}
				}
			} else {
				DirectFlightData directFlight = (DirectFlightData) flightData;
				AreaValueEnum areaFrom = directFlight.getFrom().getArea();
				AreaValueEnum areaTo = directFlight.getTo().getArea();
				if (areaFrom == AreaValueEnum.INTZ || areaFrom == AreaValueEnum.INTC) {
					return true;
				}
				if (areaTo == AreaValueEnum.INTZ || areaTo == AreaValueEnum.INTC) {
					return true;
				}
			}
		}
		return false;
	}


	private SearchFlightSolutionRequest createBookingSellupRequest(String tid, String sid, BookingSessionContext ctx, 
			String solutionId, boolean neutral) {

		boolean isMultiSlice = ctx.searchKind == BookingSearchKindEnum.MULTILEG;

		BookingSearchData bookingSellupSearchData = new BookingSearchData();
		bookingSellupSearchData.setSessionId(ctx.availableFlights.getSessionId());
		bookingSellupSearchData.setSolutionSet(ctx.availableFlights.getSolutionSet());
		bookingSellupSearchData.setSolutionId(solutionId);
		bookingSellupSearchData.setBookingSolutionId(solutionId);
		bookingSellupSearchData.setPassengers(createPassengersInfoForSellupRequest(ctx));
		bookingSellupSearchData.setMultiSlice(isMultiSlice);
		bookingSellupSearchData.setNeutral(neutral);
		bookingSellupSearchData.setType(GatewayTypeEnum.BOOKING_SELLUP);

		SearchFlightSolutionRequest bookingSellupRequest = new SearchFlightSolutionRequest(tid,sid);
		bookingSellupRequest.setCookie(ctx.cookie);
		bookingSellupRequest.setExecution(ctx.execution);
		bookingSellupRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
		//INIZIO *** MODIFICA IP ***
		if (ctx.ipAddress != null && !ctx.ipAddress.isEmpty()) {
			bookingSellupRequest.setIpAddress(ctx.ipAddress);
		}
		//FINE.
		bookingSellupRequest.setFilter(bookingSellupSearchData);
		bookingSellupRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		bookingSellupRequest.setMarket(ctx.market);
		bookingSellupRequest.setResponseType(SearchExecuteResponseType.MODEL);
		return bookingSellupRequest;
	}


	private List<PassengerBase> createPassengersInfoForSellupRequest(BookingSessionContext ctx) {
		List<PassengerBase> passengers = new ArrayList<PassengerBase>();
		int index=0;
		for ( PassengerBase passengerData : ctx.extraChargePassengerList ) {
			if (index == 0) {
				ApplicantPassengerData applicantPassengerData = (ApplicantPassengerData) passengerData;
				applicantPassengerData.setArTaxInfoType(ARTaxInfoTypesEnum.NONE);
				applicantPassengerData.setBlueBizCode("");
				applicantPassengerData.setSkyBonusCode("");
				((ApplicantPassengerData) passengerData).setName(ctx.searchCategory.name());
				passengers.add(applicantPassengerData);
			} else {
				passengers.add(passengerData);
			}
			index++;
		}
		return passengers;
	}

	@SuppressWarnings("unused")
	private SearchFlightSolutionRequest createBookingTaxSearchRequest(String tid, String sid, 
			BookingSessionContext ctx, String solutionId, 
			RoutesData routesData, boolean neutral) {

		boolean isMultiSlice = ctx.searchKind == BookingSearchKindEnum.MULTILEG;

		BookingSearchData bookingTaxSearchData = new BookingSearchData();
		bookingTaxSearchData.setId(routesData.getId());
		bookingTaxSearchData.setSessionId(ctx.availableFlights.getSessionId());
		bookingTaxSearchData.setSolutionSet(ctx.availableFlights.getSolutionSet());
		bookingTaxSearchData.setSolutionId(solutionId);
		bookingTaxSearchData.setBookingSolutionId(solutionId);
		@SuppressWarnings("unchecked")
		List<PassengerBase> routesPassengersList = (List<PassengerBase>) (List<?>) routesData.getPassengers();
		bookingTaxSearchData.setPassengers(routesPassengersList);
		bookingTaxSearchData.setMultiSlice(isMultiSlice);
		bookingTaxSearchData.setNeutral(neutral);
		bookingTaxSearchData.setType(GatewayTypeEnum.BOOKING_TAX_SEARCH);

		SearchFlightSolutionRequest bookingTaxSearchRequest = new SearchFlightSolutionRequest(tid,sid);
		bookingTaxSearchRequest.setCookie(ctx.cookie);
		bookingTaxSearchRequest.setExecution(ctx.execution);
		bookingTaxSearchRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
		//INIZIO *** MODIFICA IP ***
		if (ctx.ipAddress != null && !ctx.ipAddress.isEmpty()) {
			bookingTaxSearchRequest.setIpAddress(ctx.ipAddress);
		}
		//FINE.
		bookingTaxSearchRequest.setFilter(bookingTaxSearchData);
		bookingTaxSearchRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		bookingTaxSearchRequest.setMarket(ctx.market);
		bookingTaxSearchRequest.setResponseType(SearchExecuteResponseType.MODEL);

		return bookingTaxSearchRequest;
	}

	private boolean computeIsApis(BookingSessionContext ctx) {
		Set<String> countryCodesApis = createSetFromProperty(APIS_COUNTRY_CODES_PROPERTY);
		Set<String> countryCodesToCheck = new HashSet<String>();
		if (ctx.selectionSearchElementDirectFlights != null) {
			for (List<DirectFlightData> directFlights : ctx.selectionSearchElementDirectFlights) {
				if (directFlights != null && directFlights.size() > 0) {
					// solo da/per, non considero eventuali direct flight intermedi di scalo
					countryCodesToCheck.add(directFlights.get(0).getFrom().getCountryCode());
					countryCodesToCheck.add(directFlights.get(0).getTo().getCountryCode());
					countryCodesToCheck.add(directFlights.get(directFlights.size() - 1).getFrom().getCountryCode());
					countryCodesToCheck.add(directFlights.get(directFlights.size() - 1).getTo().getCountryCode());
				}
			}
		}
		countryCodesToCheck.retainAll(countryCodesApis);
		return (countryCodesToCheck.size() > 0);
	}

	private Boolean computeIsSecureFlight(BookingSessionContext ctx) {
		Set<String> countryCodesSecureFlight = createSetFromProperty(SECUREFLIGHT_COUNTRY_CODES_PROPERTY);
		Set<String> countryCodesToCheck = new HashSet<String>();
		if (ctx.selectionSearchElementDirectFlights != null) {
			for (List<DirectFlightData> directFlights : ctx.selectionSearchElementDirectFlights) {
				if (directFlights != null && directFlights.size() > 0) {
					// solo da/per, non considero eventuali direct flight intermedi di scalo
					countryCodesToCheck.add(directFlights.get(0).getFrom().getCountryCode());
					countryCodesToCheck.add(directFlights.get(0).getTo().getCountryCode());
					countryCodesToCheck.add(directFlights.get(directFlights.size() - 1).getFrom().getCountryCode());
					countryCodesToCheck.add(directFlights.get(directFlights.size() - 1).getTo().getCountryCode());
				}
			}
		}
		countryCodesToCheck.retainAll(countryCodesSecureFlight);
		return (countryCodesToCheck.size() > 0);
	}

	private Boolean computeIsSecureFlightESTA(BookingSessionContext ctx) {
		Set<String> countryCodesSecureFlightESTA = createSetFromProperty(SECUREFLIGHT_ESTA_COUNTRY_CODES_PROPERTY);
		Set<String> countryCodesToCheck = new HashSet<String>();
		if (ctx.selectionSearchElementDirectFlights != null) {
			for (List<DirectFlightData> directFlights : ctx.selectionSearchElementDirectFlights) {
				if (directFlights != null && directFlights.size() > 0) {
					// considero anche eventuali direct flight intermedi di scalo
					for (DirectFlightData directFlight : directFlights) {
						countryCodesToCheck.add(directFlight.getFrom().getCountryCode());
						countryCodesToCheck.add(directFlight.getTo().getCountryCode());
					}
				}
			}
		}
		countryCodesToCheck.retainAll(countryCodesSecureFlightESTA);
		return (countryCodesToCheck.size() > 0);
	}

	public boolean computeIsTariffaLight(String brandCode) {
		return "YL".equalsIgnoreCase(brandCode);
	}

	public BrandData searchSelectionBrandData(FlightData flightData,
			int solutionBrandIndex) {
		return flightData.getBrands().get(solutionBrandIndex);
	}

	public FlightData searchSelectionFlightData(BookingSessionContext ctx,
			int elementSelectionIndex, int solutionFlightIndex) {

		RouteData routeData = null;

		if (ctx.searchKind == BookingSearchKindEnum.MULTILEG){
			routeData = ctx.availableFlights.getRoutes().get(0);
		} else {
			routeData = ctx.availableFlights.getRoutes().get(elementSelectionIndex);
			if (elementSelectionIndex == 0 && routeData.getType()==RouteTypeEnum.RETURN) {
				routeData = ctx.availableFlights.getRoutes().get(1);
			}
			if (elementSelectionIndex == 1 && routeData.getType()==RouteTypeEnum.OUTBOUND) {
				routeData = ctx.availableFlights.getRoutes().get(0);
			}
		} 
		return routeData.getFlights().get(solutionFlightIndex);
	}

	@SuppressWarnings("unused")
	private List<Integer> retrieveFlightsMileages(BookingSessionContext ctx, FlightData flightData, int solutionBrandIndex) {
		List<Integer> mileage = new ArrayList<Integer>();
		String tid = IDFactory.getTid();
		RetrieveFlightMileageResponse retrieveFlightMileageResponse = null;
		String selectedCompartimentialclass = flightData.getBrands().get(solutionBrandIndex).getCompartimentalClass();
		ArrayList<String> compClass = new ArrayList<String>();
		compClass.add(selectedCompartimentialclass);

		if(flightData.getFlightType() == FlightTypeEnum.CONNECTING){
			ConnectingFlightData connections = (ConnectingFlightData) flightData;
			for( FlightData flight : connections.getFlights() ){
				RetrieveFlightMileageRequest request = new RetrieveFlightMileageRequest(tid, ctx.sid);
				request.setCookie(ctx.cookie);
				request.setExecution(ctx.execution);
				request.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
				DirectFlightData directFlight = (DirectFlightData) flight;
				if(!ctx.metaSearch){
					directFlight.setCompartimentalClass(compClass);
				}
				request.setDirectFlight(directFlight);
				request.setMarket(ctx.market);
				retrieveFlightMileageResponse = commonDelegate.retrieveFlightMileage(request);
				// ctx.cookie = retrieveFlightMileageResponse.getCookie();
				// ctx.execution = retrieveFlightMileageResponse.getExecute();
				// ctx.sabreGateWayAuthToken = retrieveFlightMileageResponse.getSabreGateWayAuthToken();
				mileage.add(new Integer((int)retrieveFlightMileageResponse.getMileage()));
			}
		}else{
			RetrieveFlightMileageRequest request = new RetrieveFlightMileageRequest(tid, ctx.sid);
			DirectFlightData directFlight = (DirectFlightData) flightData;
			if(!ctx.metaSearch){
				directFlight.setCompartimentalClass(compClass);
			}
			request.setDirectFlight(directFlight);
			try {
				request.setMarket(ctx.market);
				retrieveFlightMileageResponse = commonDelegate.retrieveFlightMileage(request);
				mileage.add(new Integer((int)retrieveFlightMileageResponse.getMileage()));
			} catch (Exception e) {
				if (ctx.searchElements != null) {
					logger.error("Error during retriveFlightMilage - Search Details: {}.", ctx.searchElements.toString(), e);
				} else {
					logger.error("Error during retriveFlightMilage - Search Details: not found.", e);
				}
				mileage.add(new Integer(0));
			}
		}
		return mileage;
	}




	public List<String> computeCarriersList(FlightData flightData, BookingSessionContext ctx) {
		List<String> carriersList = new ArrayList<String>();
		if(flightData.getFlightType() == FlightTypeEnum.CONNECTING){
			ConnectingFlightData connectingFlight = (ConnectingFlightData) flightData;
			for(FlightData flight : connectingFlight.getFlights()){
				DirectFlightData directFlight = (DirectFlightData) flight;
				if(!directFlight.getCarrier().equals("AZ")){
					carriersList.add(directFlight.getCarrier());
				}
			}
		}else{
			DirectFlightData directFlight = (DirectFlightData) flightData;
			if(!directFlight.getCarrier().equals("AZ")){
				carriersList.add(directFlight.getCarrier());
			}
		}
		return carriersList;
	}


	private Map<String, String> computeFareRules(BrandData brand, FlightData flightData, List<Integer> flightsMileage, int elementSelectionIndex, BookingSessionContext ctx) {
		Map<String, String> regole = new LinkedHashMap<String,String>();

		if (ctx.isCarnetProcess) {
			return ctx.carnetFareRules;
		}

		String sceltaPosto = Boolean.toString(isSeatSelectionEnabledForBrand(brand.getCode()));

		Calendar departureDate = null;
		AreaValueEnum areaFrom = null;
		AreaValueEnum areaTo = null;
		if (flightData instanceof DirectFlightData) {
			DirectFlightData directFlightData = (DirectFlightData) flightData;
			departureDate = directFlightData.getDepartureDate();
			areaFrom = directFlightData.getFrom().getArea();
			areaTo = directFlightData.getTo().getArea();

		} else {
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
			DirectFlightData first = (DirectFlightData) connectingFlightData.getFlights().get(0);
			DirectFlightData last = (DirectFlightData) connectingFlightData.getFlights().get(connectingFlightData.getFlights().size() - 1);
			departureDate = first.getDepartureDate();
			areaFrom = first.getFrom().getArea();
			areaTo = last.getTo().getArea();
		}
		AreaValueEnum domestic = (areaFrom.equals(AreaValueEnum.DOM))?areaTo:areaFrom;
		boolean isDomestic = domestic.equals(AreaValueEnum.DOM);

		String bagaglioStiva = computeBaggageAllowed(brand,elementSelectionIndex,ctx, isDomestic);

		/*String miglia = "true";
		int totMileage = 0;
		if(flightsMileage != null && !flightsMileage.isEmpty()){
			for(int flightMileage : flightsMileage){
				totMileage += flightMileage;
			}
			miglia = Integer.toString(totMileage);
		}
		if (totMileage == 0) {
			miglia = "false";
		}*/

		regole.put("booking.regoleTariffarie.bagaglioAMano", "true");
		regole.put("booking.regoleTariffarie.bagaglioInStiva", bagaglioStiva);
		regole.put("booking.regoleTariffarie.sceltaPosto", sceltaPosto);
		regole.put("booking.regoleTariffarie.miglia", "true");
		/*if (miglia.equals("false")) {
			regole.put("booking.regoleTariffarie.miglia", miglia);
		} else {
			//regole.put("booking.regoleTariffarie.miglia", miglia);
			regole.put("booking.regoleTariffarie.miglia", "");
		}*/

		if (configuration.isFareRulesStaticOn()) {
			computeFareRulesByConfig(brand, ctx, regole);
		} else {
			computeFareRulesByService(brand, ctx, regole);
		}

		return regole;
	}

	private void computeFareRulesByService(BrandData brand, BookingSessionContext ctx, Map<String,String> regole) {



//		String prenotazionePrima = ctx.i18n.get("booking.nonconsentito.cambio.rimborso.label");
//		String prenotazioneDopo = ctx.i18n.get("booking.nonconsentito.cambio.rimborso.label");
//		String rimborsoPrima = ctx.i18n.get("booking.nonconsentito.cambio.rimborso.label");
//		String rimborsoDopo = ctx.i18n.get("booking.nonconsentito.cambio.rimborso.label");
//		if(brand.getPenalties()!=null){
//			if(brand.getPenalties().getChangeBeforeDepature()!=null){
//				Boolean c = brand.getPenalties().getChangeBeforeDepature().isPermitted();
//				boolean check = false;
//				if(c == null){
//					check = false;
//				}else{
//					check = c.booleanValue();
//				}
//				if(check){
//					if(brand.getPenalties().getChangeBeforeDepature().getMinPrice() == null) {
//						prenotazionePrima = ctx.i18n.get("booking.gratis.cambio.rimborso.label");
//					} else if (brand.getPenalties().getChangeBeforeDepature().getMinPrice().intValue() == 0 ){
//						prenotazionePrima = ctx.i18n.get("booking.gratis.cambio.rimborso.label");
//					}else{
//						GenericPriceRender gpr = new GenericPriceRender(brand.getPenalties().getChangeBeforeDepature().getMinPrice(), ctx.currency, ctx);
//						prenotazionePrima = gpr.getFare() + " " + brand.getPenalties().getChangeBeforeDepature().getMinPriceCurrency();
//					}
//				}
//			}
//			if(brand.getPenalties().getChangeAfterDepature()!=null){
//				Boolean c = brand.getPenalties().getChangeAfterDepature().isPermitted();
//				boolean check = false;
//				if(c == null){
//					check = false;
//				}else{
//					check = c.booleanValue();
//				}
//				if(check){
//					if(brand.getPenalties().getChangeAfterDepature().getMinPrice() == null) {
//						prenotazioneDopo = ctx.i18n.get("booking.gratis.cambio.rimborso.label");
//					} else if(brand.getPenalties().getChangeAfterDepature().getMinPrice().intValue() == 0 ){
//						prenotazioneDopo = ctx.i18n.get("booking.gratis.cambio.rimborso.label");
//					}else{
//						GenericPriceRender gpr = new GenericPriceRender(brand.getPenalties().getChangeAfterDepature().getMinPrice(), ctx.currency, ctx);
//						prenotazioneDopo =  gpr.getFare() + " " + brand.getPenalties().getChangeAfterDepature().getMinPriceCurrency();
//					}
//				}
//			}
//			if(brand.getPenalties().getRefundBeforeDepature()!=null){
//				Boolean c = brand.getPenalties().getRefundBeforeDepature().isPermitted();
//				boolean check = false;
//				if(c == null){
//					check = false;
//				}else{
//					check = c.booleanValue();
//				}
//				if(check){
//					if(brand.getPenalties().getRefundBeforeDepature().getMinPrice() == null) {
//						rimborsoPrima = ctx.i18n.get("booking.gratis.cambio.rimborso.label");
//					} else if(brand.getPenalties().getRefundBeforeDepature().getMinPrice().intValue() == 0 ){
//						rimborsoPrima = ctx.i18n.get("booking.gratis.cambio.rimborso.label");
//					}else{
//						GenericPriceRender gpr = new GenericPriceRender(brand.getPenalties().getRefundBeforeDepature().getMinPrice(), ctx.currency, ctx);
//						rimborsoPrima = gpr.getFare() + " " + brand.getPenalties().getRefundBeforeDepature().getMinPriceCurrency();
//					}
//				}
//			}
//			if(brand.getPenalties().getRefundAfterDepature()!=null){
//				Boolean c = brand.getPenalties().getRefundAfterDepature().isPermitted();
//				boolean check = false;
//				if(c == null){
//					check = false;
//				}else{
//					check = c.booleanValue();
//				}
//				if(check){
//					if(brand.getPenalties().getRefundAfterDepature().getMinPrice() == null) {
//						rimborsoDopo = ctx.i18n.get("booking.gratis.cambio.rimborso.label");
//					} else if(brand.getPenalties().getRefundAfterDepature().getMinPrice().intValue() == 0 ){
//						rimborsoDopo = ctx.i18n.get("booking.gratis.cambio.rimborso.label");
//					}else{
//						GenericPriceRender gpr = new GenericPriceRender(brand.getPenalties().getRefundAfterDepature().getMinPrice(), ctx.currency, ctx);
//						rimborsoDopo = gpr.getFare() + " " + brand.getPenalties().getRefundAfterDepature().getMinPriceCurrency();
//					}
//				}
//			}
//
//		}
//		regole.put("booking.regoleTariffarie.cambioPrenotazionePrimaPartenza", prenotazionePrima);
//		regole.put("booking.regoleTariffarie.cambioPrenotazioneDopoPartenza", prenotazioneDopo);
//		regole.put("booking.regoleTariffarie.rimborsoPrenotazionePrimaPartenza", rimborsoPrima);
//		regole.put("booking.regoleTariffarie.rimborsoPrenotazioneDopoPartenza", rimborsoDopo);
//	}
        if(brand.getPenalties() != null) {
            BrandPenaltiesData penalties = brand.getPenalties();

            BrandPenaltyData ChangeBefore = penalties.getChangeBeforeDepature();
            BrandPenaltyData ChangeAfter = penalties.getChangeAfterDepature();
            BrandPenaltyData RefundBefore = penalties.getRefundBeforeDepature();
            BrandPenaltyData RefundAfter = penalties.getRefundAfterDepature();

            String changeBefore = "";
            if (ChangeBefore.isPermitted()) {
                if (!ChangeBefore.getMinPriceCurrency().equals("")) {
                    changeBefore = setSpecialCase(ChangeBefore, ctx);
                } else {
                	if(ChangeBefore.getMaxPrice().intValue() == 0) {
						changeBefore = ctx.i18n.get("fareRules.co.change.label");
					}else{
						changeBefore = ChangeBefore.getMaxPriceCurrency() + " " + String.valueOf(ChangeBefore.getMaxPrice());
					}
                }
            } else {
                changeBefore = ctx.i18n.get("fareRules.fa.change.label");
            }
            if(ChangeBefore.getMinPriceCurrency() != null) {
                if (!ChangeBefore.isPermitted() && ChangeBefore.getMinPriceCurrency().equals("NPP")) {
                    changeBefore = setSpecialCase(ChangeBefore, ctx);
                }
                if (!ChangeBefore.isPermitted() && ChangeBefore.getMinPriceCurrency().equals("DEFX")) {
                    changeBefore = setSpecialCase(ChangeBefore, ctx);
                }
            }

            regole.put("booking.regoleTariffarie.cambioPrenotazionePrimaPartenza", changeBefore);
            String changeAfter = "";
            if (ChangeAfter.isPermitted()) {
                if (!ChangeAfter.getMinPriceCurrency().equals("")) {
                    changeAfter = setSpecialCase(ChangeAfter, ctx);
                } else {
					if(ChangeAfter.getMaxPrice().intValue() == 0) {
						changeAfter = ctx.i18n.get("fareRules.co.change.label");
					}else{
						changeAfter = ChangeAfter.getMaxPriceCurrency() + " " + String.valueOf(ChangeAfter.getMaxPrice());
					}
                }
            } else {
                changeAfter = ctx.i18n.get("fareRules.fa.change.label");
            }

            if(ChangeAfter.getMinPriceCurrency() != null) {
                if (!ChangeAfter.isPermitted() && ChangeAfter.getMinPriceCurrency().equals("NPP")) {
                    changeAfter = setSpecialCase(ChangeAfter, ctx);
                }
                if (!ChangeAfter.isPermitted() && ChangeAfter.getMinPriceCurrency().equals("DEFX")) {
                    changeAfter = setSpecialCase(ChangeAfter, ctx);
                }
            }

            regole.put("booking.regoleTariffarie.cambioPrenotazioneDopoPartenza", changeAfter);
            String refundBefore = "";
            if (RefundBefore.isPermitted()) {
                if (!RefundBefore.getMinPriceCurrency().equals("")) {
                    refundBefore = setSpecialCase(RefundBefore, ctx);
                } else {
					if(RefundBefore.getMaxPrice().intValue() == 0) {
						refundBefore = ctx.i18n.get("fareRules.co.change.label");
					}else{
						refundBefore = RefundBefore.getMaxPriceCurrency() + " " + String.valueOf(RefundBefore.getMaxPrice());
					}
                }
            } else {
                refundBefore = ctx.i18n.get("fareRules.fa.change.label");
            }

            if(RefundBefore.getMinPriceCurrency() != null) {
                if (!RefundBefore.isPermitted() && RefundBefore.getMinPriceCurrency().equals("NPP")) {
                    refundBefore = setSpecialCase(RefundBefore, ctx);
                }
                if (!RefundBefore.isPermitted() && RefundBefore.getMinPriceCurrency().equals("DEFX")) {
                    refundBefore = setSpecialCase(RefundBefore, ctx);
                }
            }

            regole.put("booking.regoleTariffarie.rimborsoPrenotazionePrimaPartenza", refundBefore);
            String refundAfter = "";
            if (RefundAfter.isPermitted()) {
                if (!RefundAfter.getMinPriceCurrency().equals("")) {
                    refundAfter = setSpecialCase(RefundAfter, ctx);
                } else {
					if(RefundAfter.getMaxPrice().intValue() == 0) {
						refundAfter = ctx.i18n.get("fareRules.co.change.label");
					}else{
						refundAfter = RefundAfter.getMaxPriceCurrency() + " " + String.valueOf(RefundAfter.getMaxPrice());
					}
                }
            } else {
                refundAfter = ctx.i18n.get("fareRules.fa.change.label");
            }

            if(RefundAfter.getMinPriceCurrency() != null) {
                if (!RefundAfter.isPermitted() && RefundAfter.getMinPriceCurrency().equals("NPP")) {
                    refundAfter = setSpecialCase(RefundAfter, ctx);
                }
                if (!RefundAfter.isPermitted() && RefundAfter.getMinPriceCurrency().equals("DEFX")) {
                    refundAfter = setSpecialCase(RefundAfter, ctx);
                }
            }

            regole.put("booking.regoleTariffarie.rimborsoPrenotazioneDopoPartenza", refundAfter);
        }else{
            regole.put("booking.regoleTariffarie.cambioPrenotazionePrimaPartenza", "");
            regole.put("booking.regoleTariffarie.cambioPrenotazioneDopoPartenza", "");
            regole.put("booking.regoleTariffarie.rimborsoPrenotazionePrimaPartenza", "");
            regole.put("booking.regoleTariffarie.rimborsoPrenotazioneDopoPartenza", "");
        }
	}

	private void computeFareRulesByConfig(BrandData brand, BookingSessionContext ctx, Map<String,String> regole) {
		String selector = brand.getCode().toLowerCase();
		if(BookingSearchCUGEnum.MILITARY.equals(ctx.cug)){
			selector = "mm";
		}
		// altre label se fco_lin o domestico, uguale fra loro e comunque a seconda del brand: da aggiungere nell'I18n
		if (ctx.searchCategory == BookingSearchCategoryEnum.FCO_LIN || ctx.searchCategory == BookingSearchCategoryEnum.DOM){
			regole.put("booking.regoleTariffarie.cambioPrenotazionePrimaPartenza", ctx.i18n.get("fareRules." + selector + ".changebefore.dom.label"));
			regole.put("booking.regoleTariffarie.cambioPrenotazioneDopoPartenza", ctx.i18n.get("fareRules." + selector + ".changeafter.dom.label"));
			regole.put("booking.regoleTariffarie.rimborsoPrenotazionePrimaPartenza", ctx.i18n.get("fareRules." + selector + ".refoundbefore.dom.label"));
			regole.put("booking.regoleTariffarie.rimborsoPrenotazioneDopoPartenza", ctx.i18n.get("fareRules." + selector + ".refoundafter.dom.label"));
		} else {
			regole.put("booking.regoleTariffarie.cambioPrenotazionePrimaPartenza", ctx.i18n.get("fareRules." + selector + ".change.label"));
			regole.put("booking.regoleTariffarie.cambioPrenotazioneDopoPartenza", ctx.i18n.get("fareRules." + selector + ".change.label"));
			regole.put("booking.regoleTariffarie.rimborsoPrenotazionePrimaPartenza", ctx.i18n.get("fareRules." + selector + ".refound.label"));
			regole.put("booking.regoleTariffarie.rimborsoPrenotazioneDopoPartenza", ctx.i18n.get("fareRules." + selector + ".refound.label"));
		}
	}

	private String computeBaggageAllowed(BrandData brand, int elementSelectionIndex, BookingSessionContext ctx, boolean isDomestic) {
		String bagaglioStiva = "false";
		SearchElement searchElement = ctx.searchElements.get(elementSelectionIndex);
		if (computeIsTariffaLight(brand.getCode()) && (!searchFlightsDelegate.checkPromoBaggagePeriod(searchElement.getDepartureDate()) || !isDomestic) ) {
			return "false";
		}
		if(brand.getBaggageAllowanceList()!=null && !brand.getBaggageAllowanceList().isEmpty()){

			int count = brand.getBaggageAllowanceList().get(brand.getBaggageAllowanceList().size() - 1).getCount();

			String weight = brand.getBaggageAllowanceList().get(brand.getBaggageAllowanceList().size() - 1).getCode();

			bagaglioStiva = Integer.toString(count) + "x" + (weight).replaceAll("kilograms", "kg");
		}
		return bagaglioStiva.replaceAll(" ", "");
	}

	public boolean isBusiness(String brandCode) {
		return brandCode != null && brandCode.toLowerCase().startsWith("j"); // JC, JF are business
	}

	/**
	 * Verify if seat selection should be enabled for a brand, based on the brand type only.
	 */
	private boolean isSeatSelectionEnabledForBrand(String brandCode) {
		if (brandCode == null) {
			return false;
		}
		if (computeIsTariffaLight(brandCode)) {
			return false;
		}
		return true;
	}

	/**
	 * Verify if seat selection should be enabled for a direct flight and for a brand,
	 * based on brand and carrier(s).
	 */
	private boolean isSeatSelectionEnabledForDirectFlight(MMCustomerProfileData loggedUser, DirectFlightData directFlightData, String brandCode) {
		if (directFlightData == null || brandCode == null) {
			return false;
		}
		// fare type
		if (computeIsTariffaLight(brandCode)) {
			// if fare type is light and user is not FRECCIA ALATA /PLUS or ULISSE
			if (loggedUser == null || loggedUser.getTierCode() == null) {
				return false;
			}

			MMTierCodeEnum tierCode = loggedUser.getTierCode();
			if (tierCode == MMTierCodeEnum.BASIC || tierCode == MMTierCodeEnum.UNKNOWN) {
				return false;
			}

		}
		// carrier
		if (!"AZ".equals(directFlightData.getCarrier())) {
			return false;
		}
		// all ok
		return true;
	}

	private void updatePassengerData(BookingSessionContext ctx, PassengersData passengersData, Passenger sourcePassenger, 
			PassengerBaseData targetPassenger) {

		// base passenger data
		targetPassenger.setLastName(sourcePassenger.getSurname());
		targetPassenger.setName(sourcePassenger.getName());

		// passenger info
		PassengerBaseInfoData passengerInfo;
		if (ctx.isApis && ctx.isSecureFlight) {
			PassengerApisSecureFlightInfoData typedPassengerInfo = new PassengerApisSecureFlightInfoData();
			passengerInfo = typedPassengerInfo;
			typedPassengerInfo.setSecondName(sourcePassenger.getSecondName());
			typedPassengerInfo.setNationality(sourcePassenger.getNationality());
			typedPassengerInfo.setPassportNumber(sourcePassenger.getPassportNumber());
		} else if (ctx.isApis) {
			PassengerApisInfoData typedPassengerInfo = new PassengerApisInfoData();
			passengerInfo = typedPassengerInfo;
			typedPassengerInfo.setNationality(sourcePassenger.getNationality());
			typedPassengerInfo.setPassportNumber(sourcePassenger.getPassportNumber());
		} else if (ctx.isSecureFlight) {
			PassengerSecureFlightInfoData typedPassengerInfo = new PassengerSecureFlightInfoData();
			passengerInfo = typedPassengerInfo;
			typedPassengerInfo.setSecondName(sourcePassenger.getSecondName());
		} else {
			passengerInfo = new PassengerBaseInfoData();
		}
		passengerInfo.setBirthDate(sourcePassenger.getBirthDate());
		if (sourcePassenger.getSex() != null) {
			passengerInfo.setGender(
					sourcePassenger.getSex() == Sex.MALE ? GenderTypeEnum.MALE : GenderTypeEnum.FEMALE);
		} else {
			passengerInfo.setGender(null);
		}
		targetPassenger.setInfo(passengerInfo);

		// passenger preferences
		PreferencesData preferences = new PreferencesData();

		// passenger preferences - meal type
		if (sourcePassenger.getMealPreference() != null && !"".equals(sourcePassenger.getMealPreference())) {
			for (MealData mealData : ctx.mealTypes) {
				if (sourcePassenger.getMealPreference().equals(mealData.getCode())) {
					preferences.setMealType(mealData);
					break;
				}
			}
		}

		// passenger preferences - seat selection
		List<SeatPreferencesData> seatPreferences = null;
		if (sourcePassenger.getSeat() != null) {
			for (Entry<Integer, String> entry : sourcePassenger.getSeat().entrySet()) {
				SeatPreferencesData seatPreference = new SeatPreferencesData();
				Integer seatMapIndex = entry.getKey();
				FlightSeatMapData flightSeatMapData = ctx.seatMaps.get(seatMapIndex);
				String[] rowNumber = entry.getValue().split("_");
				if (rowNumber.length == 2) {
					seatPreference.setFlightNumber(flightSeatMapData.getDirectFlight().getFlightNumber());
					seatPreference.setFlightCarrier(flightSeatMapData.getDirectFlight().getCarrier());
					seatPreference.setCabinSeat(flightSeatMapData.getDirectFlight().getCabin());
					seatPreference.setNumber(rowNumber[1]);
					int parsedIntRowNumber;
					String paresdRowNumber = "";
					try {
						parsedIntRowNumber = Integer.parseInt(rowNumber[0]);
						if (parsedIntRowNumber < 10) {
							paresdRowNumber = "0" + String.valueOf(parsedIntRowNumber);
						} else {
							paresdRowNumber = String.valueOf(parsedIntRowNumber);
						}
					} catch (NumberFormatException e) {
						logger.error("Unable to parse seat row number {}.", rowNumber[0]);
						throw(e);
					}
					seatPreference.setRow(paresdRowNumber);
					if (seatPreferences == null) {
						seatPreferences = new ArrayList<SeatPreferencesData>();
					}
					seatPreferences.add(seatPreference);
				}
			}	
		}
		preferences.setSeatPreferences(seatPreferences);
		preferences.setSeatType(null);

		// passenger frequent flyer program
		FrequentFlyerTypeData frequentFlyerType = null;
		String frequentFlyerCode = null;
		if (sourcePassenger.getFrequentFlyerProgram() != null && !"".equals(sourcePassenger.getFrequentFlyerProgram())) {
			for (FrequentFlyerTypeData availableFrequentFlyerType : ctx.frequentFlyerTypes) {
				if (sourcePassenger.getFrequentFlyerProgram().equals(availableFrequentFlyerType.getCode())) {
					frequentFlyerType = availableFrequentFlyerType;
					frequentFlyerCode = sourcePassenger.getFrequentFlyerCardNumber();
					break;
				}
			}
		}

		// specific passenger data
		if (targetPassenger instanceof ApplicantPassengerData) {
			ApplicantPassengerData typedTargetPassenger = (ApplicantPassengerData) targetPassenger;
			typedTargetPassenger.setPreferences(preferences);
			typedTargetPassenger.setEmail(passengersData.getEmail());
			typedTargetPassenger.setContact(new ArrayList<ContactData>());
			typedTargetPassenger.setBlueBizCode(sourcePassenger.getBlueBizCode());
			typedTargetPassenger.setSkyBonusCode(sourcePassenger.getSkyBonusCode());
			typedTargetPassenger.setFrequentFlyerType(frequentFlyerType);
			typedTargetPassenger.setFrequentFlyerCode(frequentFlyerCode);
			if (passengersData.getArTaxInfoType() != null) {
				typedTargetPassenger.setArTaxInfoType(ARTaxInfoTypesEnum.fromValue(passengersData.getArTaxInfoType()));
				if (typedTargetPassenger.getArTaxInfoType() != ARTaxInfoTypesEnum.NOT_AR) { 
					typedTargetPassenger.setCuit(passengersData.getNumeroCuitCuil());
					ctx.cuit = passengersData.getNumeroCuitCuil();
				} else {
					typedTargetPassenger.setCountry(passengersData.getNazionalitaCuitCuil());
					ctx.cuit = passengersData.getNazionalitaCuitCuil();
				}

			}
			typedTargetPassenger.getContact().clear();
			// contact 1
			if (passengersData.getContact1() != null) {
				ContactData contactData = new ContactData();
				if (passengersData.getContact1().getContactType() == ContactType.MOBILE) {
					contactData.setContactType(ContactTypeEnum.M);
				} else if (passengersData.getContact1().getContactType() == ContactType.OFFICE) {
					contactData.setContactType(ContactTypeEnum.B);
				} else if (passengersData.getContact1().getContactType() == ContactType.HOTEL) {
					contactData.setContactType(ContactTypeEnum.A);
				} else if (passengersData.getContact1().getContactType() == ContactType.HOME) {
					contactData.setContactType(ContactTypeEnum.H);
				}
				contactData.setPhoneNumber(passengersData.getContact1().getPhoneNumber());
				for (PhonePrefixData phonePrefix : ctx.phonePrefixes) {
					if (phonePrefix.getCode().equals(passengersData.getContact1().getInternationalPrefix())) {
						contactData.setPrefix(phonePrefix);
						break;
					}
				}
				typedTargetPassenger.getContact().add(contactData);
			}
			// contact 2
			if (passengersData.getContact2() != null) {
				ContactData contactData = new ContactData();
				if (passengersData.getContact2().getContactType() == ContactType.MOBILE) {
					contactData.setContactType(ContactTypeEnum.M);
				} else if (passengersData.getContact2().getContactType() == ContactType.OFFICE) {
					contactData.setContactType(ContactTypeEnum.B);
				} else if (passengersData.getContact2().getContactType() == ContactType.HOTEL) {
					contactData.setContactType(ContactTypeEnum.A);
				} else if (passengersData.getContact2().getContactType() == ContactType.HOME) {
					contactData.setContactType(ContactTypeEnum.H);
				}
				contactData.setPhoneNumber(passengersData.getContact2().getPhoneNumber());
				for (PhonePrefixData phonePrefix : ctx.phonePrefixes) {
					if (phonePrefix.getCode().equals(passengersData.getContact2().getInternationalPrefix())) {
						contactData.setPrefix(phonePrefix);
						break;
					}
				}
				typedTargetPassenger.getContact().add(contactData);
			}
			// email contact
			ContactData emailContactData = new ContactData();
			emailContactData.setContactType(ContactTypeEnum.M);
			emailContactData.setPhoneNumber(passengersData.getEmail());
			emailContactData.setPrefix(new PhonePrefixData());
			emailContactData.getPrefix().setCode("");
			emailContactData.getPrefix().setDescription("email");
			emailContactData.getPrefix().setPrefix("");
			typedTargetPassenger.getContact().add(emailContactData);
		} else if (targetPassenger instanceof AdultPassengerData) {
			AdultPassengerData typedTargetPassenger = (AdultPassengerData) targetPassenger;
			typedTargetPassenger.setPreferences(preferences);
			typedTargetPassenger.setFrequentFlyerType(frequentFlyerType);
			typedTargetPassenger.setFrequentFlyerCode(frequentFlyerCode);
		} else if (targetPassenger instanceof ChildPassengerData) {
			ChildPassengerData typedTargetPassenger = (ChildPassengerData) targetPassenger;
			typedTargetPassenger.setPreferences(preferences);
		} else if (targetPassenger instanceof InfantPassengerData) {
			InfantPassengerData typedTargetPassenger = (InfantPassengerData) targetPassenger;
			typedTargetPassenger.setPreferences(preferences);
			typedTargetPassenger.setAdultRefent(sourcePassenger.getReferenceAdult());
		}
	}

	/**
	 * Compute all the total amounts based on the current selections.
	 * @param ctx
	 */
	private void computeTotalAmounts(BookingSessionContext ctx) {
		// GrossAmount is the sum of the fares shown on the brand selection.
		// NetAmount is the sum of the fares without the taxes and extras
		ctx.totalCouponPrice = computeTotalCouponPrice(ctx);
		ctx.totalCashMilesPrice = computeCashMiles(ctx);
		ctx.netAmount = computeTheFinalNetAmount(ctx);
		ctx.totalTaxes = computeTotalTaxesAmount(ctx);
		ctx.totalExtras = computeTotalExtrasAmount(ctx);
		//ctx.insuranceAmount = computeInsuranceAmount(ctx);
		//ctx.grossAmountNoDiscount = computeTheGrossAmountNoDiscount(ctx);
		ctx.grossAmount = computeTheFinalGrossAmount(ctx);
		ctx.ccFeeAddedToGrossAmount = false;
		//ctx.grossAmount = ctx.grossAmountNoDiscount;
		ctx.netAmountForPayment = computeTheFinalNetAmountForPayment(ctx);
	}

	private BigDecimal computeInsuranceAmount(BookingSessionContext ctx) {
		if (ctx.insuranceProposalData != null) {
			return ctx.insuranceProposalData.getTotalInsuranceCost();
		} else {
			return new BigDecimal(0);
		}
	}


	private BigDecimal computeCashMiles(BookingSessionContext ctx) {
		BigDecimal cashMiles = new BigDecimal(0);
		if (ctx.cashAndMiles != null && ctx.isCashMilesReady) {
			cashMiles = cashMiles.add(ctx.cashAndMiles.getDiscountAmount());
		}
		return cashMiles;
	}


	/**
	 * It computes the total coupon price by adding the couponPrice 
	 * for each passenger in selectionRoutes
	 * (We Assuming this total as positive and it represent the total discount
	 * to show in Box la tua selezione)
	 * @param ctx
	 * @return
	 */
	private BigDecimal computeTotalCouponPrice(BookingSessionContext ctx) {
		BigDecimal couponPrice = new BigDecimal(0);
		List<PassengerBaseData> passengerBaseData = ctx.selectionRoutes.getPassengers();
		for(PassengerBaseData passenger : passengerBaseData){
			couponPrice = couponPrice.add(passenger.getCouponPrice());
		}
		BigDecimal totalCouponPrice = new BigDecimal(0);
		totalCouponPrice = totalCouponPrice.add(couponPrice);
		return totalCouponPrice;
	}


	/**
	 * is used to compute the net amount as the sum of the fare for the passengers
	 * taxes and extras excluded
	 * @param ctx
	 * @return
	 */
	private BigDecimal computeTheFinalNetAmount(BookingSessionContext ctx) {
		BigDecimal totalFare = new BigDecimal(0);
		List<TaxData> selectionTaxes = ctx.selectionTaxes;
		if (selectionTaxes != null){
			for(TaxData taxData : selectionTaxes){
				if(taxData.getCode().toLowerCase().contains("fare")){
					totalFare = totalFare.add(taxData.getAmount());
				}
			}
		}
		BigDecimal netAmount = new BigDecimal(0);
		netAmount = netAmount.add(totalFare);
		return netAmount;
	}

	/**
	 * is used to compute total taxes as the sum of the taxes for the passengers
	 * @param ctx
	 * @return
	 */
	private BigDecimal computeTotalTaxesAmount(BookingSessionContext ctx) {
		//Obtaining Taxes
		BigDecimal totalTaxes = new BigDecimal(0);
		List<TaxData> selectionTaxes = ctx.selectionTaxes;
		if (selectionTaxes!=null){
			for(TaxData taxData : selectionTaxes){
				if(taxData.getCode().toLowerCase().contains("taxtotal")){
					totalTaxes = totalTaxes.add(taxData.getAmount());
				}
			}
		}

		return totalTaxes;
	}

	/**
	 * is used to compute total extras included as the sum of the extras for the passengers
	 * @param ctx
	 * @return
	 */
	private BigDecimal computeTotalExtrasAmount(BookingSessionContext ctx) {
		//Obtaining Taxes
		BigDecimal totalExtra = new BigDecimal(0);
		List<TaxData> selectionTaxes = ctx.selectionTaxes;
		if (selectionTaxes != null){
			for(TaxData taxData : selectionTaxes){
				if(taxData.getCode().toLowerCase().contains("yqtotal")){
					totalExtra = totalExtra.add(taxData.getAmount());
				}
			}
		}

		return totalExtra;
	}

	/**
	 * is used to compute total extra charges as the sum of the extra charges for the passengers
	 * @param ctx
	 * @return
	 */
	public BigDecimal computeTotalExtraChargesAmount(BookingSessionContext ctx) {
		//Obtaining Extracharge
		BigDecimal totalExtraCharge = new BigDecimal(0);
		List<PassengerBase> selectionExtraCharge = ctx.extraChargePassengerList;
		for(PassengerBase passenger : selectionExtraCharge){
			totalExtraCharge = totalExtraCharge.add( ((PassengerBaseData) passenger).getExtraCharge() );
		}

		return totalExtraCharge;
	}

	/**
	 * is used to compute the gross amount as the sum of the fare for the passengers
	 * taxes and extras included
	 * @param ctx
	 * @return
	 */
	private BigDecimal computeTheFinalGrossAmount(BookingSessionContext ctx) {

		//Obtaining total price
		BigDecimal grossAmount = new BigDecimal(0);
		grossAmount = grossAmount.add(ctx.netAmount);
		grossAmount = grossAmount.add(ctx.totalTaxes);
		grossAmount = grossAmount.add(ctx.totalExtras);
		grossAmount = grossAmount.add(ctx.totalExtraCharges);
		if (ctx.coupon != null && ctx.coupon.isValid()) {
			grossAmount = grossAmount.subtract(ctx.totalCouponPrice);
		}
		if (ctx.cashAndMiles != null && ctx.isCashMilesReady) {
			grossAmount = grossAmount.subtract(ctx.totalCashMilesPrice);
		}
		if (grossAmount.intValue() < 0)
			grossAmount = new BigDecimal(0);
		//grossAmount = grossAmount.add(ctx.insuranceAmount);
		return grossAmount;
	}


	/**
	 * is used to compute the net amount for the init payment: fare + taxes + extras
	 * excluding discount and extracharge
	 * @param ctx
	 * @return
	 */
	private BigDecimal computeTheFinalNetAmountForPayment(BookingSessionContext ctx) {
		//Obtaining total price
		BigDecimal grossAmount = new BigDecimal(0);
		grossAmount = grossAmount.add(ctx.netAmount);
		grossAmount = grossAmount.add(ctx.totalTaxes);
		grossAmount = grossAmount.add(ctx.totalExtras);
		//grossAmount = grossAmount.add(ctx.insuranceAmount);
		return grossAmount;
	}

	/**
	 * is used to compute the gross amount without subtract the coupon discount
	 * as the sum of the fare for the passengers
	 * taxes and extras included
	 * @param ctx
	 * @return
	 */
	@SuppressWarnings("unused")
	private BigDecimal computeTheGrossAmountNoDiscount(BookingSessionContext ctx) {

		//Obtaining total price
		BigDecimal grossAmount = new BigDecimal(0);
		grossAmount = grossAmount.add(ctx.netAmount);
		grossAmount = grossAmount.add(ctx.totalTaxes);
		grossAmount = grossAmount.add(ctx.totalExtras);
		grossAmount = grossAmount.add(ctx.totalExtraCharges);
		return grossAmount;
	}

	private CabinEnum computeSelectionCabin(BookingSessionContext ctx) {
		if (ctx.selectionRoutes != null) {
			return ctx.selectionRoutes.getCabin();
		} else {
			return null;
		}
	}

	public Set<String> createSetFromProperty(String propertyName) {
		String propertyValue = PropertiesUtil.toString(componentContext.getProperties().get(propertyName), "");
		String[] setElements = propertyValue.split(",");
		Set<String> set = new HashSet<String>();
		for (String setElement : setElements) {
			set.add(setElement);
		}
		return set;
	}

	/**
	 * 
	 * @param propertyName
	 * @return
	 */
	private Calendar createDate(Calendar baseDate, String propertyDate) {
		Calendar newDate = (Calendar) baseDate.clone();
		Map<String,Integer> calendarMonthMapping = new HashMap<String,Integer>();
		calendarMonthMapping.put("01",Calendar.JANUARY);
		calendarMonthMapping.put("02",Calendar.FEBRUARY);
		calendarMonthMapping.put("03",Calendar.MARCH);
		calendarMonthMapping.put("04",Calendar.APRIL);
		calendarMonthMapping.put("05",Calendar.MAY);
		calendarMonthMapping.put("06",Calendar.JUNE);
		calendarMonthMapping.put("07",Calendar.JULY);
		calendarMonthMapping.put("08",Calendar.AUGUST);
		calendarMonthMapping.put("09",Calendar.SEPTEMBER);
		calendarMonthMapping.put("10",Calendar.OCTOBER);
		calendarMonthMapping.put("11",Calendar.NOVEMBER);
		calendarMonthMapping.put("12",Calendar.DECEMBER);

		String date = PropertiesUtil.toString(componentContext.getProperties().get(propertyDate), "");
		String[] splitDate = date.split("/");
		int day = Integer.parseInt(splitDate[0]);
		int month = calendarMonthMapping.get(splitDate[1]);
		int year = Integer.parseInt(splitDate[2]);
		newDate.set(Calendar.MONTH, month);
		newDate.set(Calendar.DAY_OF_MONTH, day);
		newDate.set(Calendar.YEAR, year);
		return newDate;
	}

	/**
	 * Let phase the current phase, it clean the current context according
	 * to current phase.
	 * @param phase
	 */
	private void cleanContextBeforeNewSearch(BookingSessionContext ctx){
		ctx.newSearch = true;
		ctx.phase = BookingPhaseEnum.FLIGHTS_SEARCH;
		ctx.flightSelections = new FlightSelection[ctx.searchElements.size()];
		ctx.numberOfShownFlights = new int[ctx.searchElements.size()];
		ctx.selectedDepartureDateChoices = new int[ctx.searchElements.size()];
		for (int i = 0; i < ctx.searchElements.size(); i++) {
			ctx.selectedDepartureDateChoices[i] = INDEX_START_RIBBON_DATE;
		}
		ctx.youthSolutionFound = null;
		ctx.familySolutionFound = null;
		ctx.militarySolutionFound = null;
		ctx.solutionForSelectedDate = new Boolean[ctx.searchElements.size()];
		ctx.readyToPassengersDataPhase = false;
		for (int i=0; i<ctx.solutionForSelectedDate.length; i++) {
			ctx.solutionForSelectedDate[i] = false;
		}
		ctx.coupon=null;
	}

	/**
	 * It forwards the booking navigation process to the permitted phase.
	 * @param ctx
	 * @param destinationPhase
	 * @return three possible results:
	 * <ul>
	 * 	<li>"" if the navigation is permitted;</li>
	 * 	<li>a page if the behavior was not permitted and it can manage the correct forwarding;</li>
	 * 	<li>NULL for a generic error;</li>
	 * </ul>
	 */
	public String forwardNavigationProcess(BookingSessionContext ctx, BookingPhaseEnum destinationPhase,
			Boolean residency, String nocache) {
		BookingPhaseEnum sourcePhase = ctx.phase;
		String residencyValue="";
		if(residency) {
			residencyValue = "&residency=true";
		}
		switch (sourcePhase.value()) {
		case "initial":{
			if (destinationPhase == BookingPhaseEnum.FLIGHTS_SEARCH) {
				ctx.phase = BookingPhaseEnum.FLIGHTS_SEARCH;
				return "";
			}
			//Followed operations are not permitted. Generate an error.
			if (destinationPhase == BookingPhaseEnum.FLIGHTS_SELECTION
					|| destinationPhase == BookingPhaseEnum.PASSENGERS_DATA
					|| destinationPhase == BookingPhaseEnum.ANCILLARY
					|| destinationPhase == BookingPhaseEnum.PAYMENT
					|| destinationPhase == BookingPhaseEnum.DONE) {
				return null;
			}
		}
		case "flightsSearch":{
			if (destinationPhase == BookingPhaseEnum.FLIGHTS_SEARCH) {
				cleanContextBeforeNewSearch(ctx);
				ctx.phase = BookingPhaseEnum.FLIGHTS_SEARCH;
				return "";
			}
			if (destinationPhase == BookingPhaseEnum.FLIGHTS_SELECTION) {
				ctx.phase = BookingPhaseEnum.FLIGHTS_SELECTION;
				return "";
			}

			//following behaviours are not permitted. Forwarding to search page
			if (destinationPhase == BookingPhaseEnum.PASSENGERS_DATA
					|| destinationPhase == BookingPhaseEnum.ANCILLARY
					|| destinationPhase == BookingPhaseEnum.PAYMENT
					|| destinationPhase == BookingPhaseEnum.DONE) {
				ctx.phase = BookingPhaseEnum.FLIGHTS_SEARCH;
				return configuration.getBookingFlightSelectPage() + "?nocache=" + nocache + residencyValue;
			}
		}break;
		case "flightsSelection":{
			if (destinationPhase == BookingPhaseEnum.FLIGHTS_SEARCH) {
				cleanContextBeforeNewSearch(ctx);
				ctx.phase = BookingPhaseEnum.FLIGHTS_SEARCH;
				return "";
			}

			//following behaviours are not permitted. Forwarding to search page
			if (destinationPhase == BookingPhaseEnum.PASSENGERS_DATA
					|| destinationPhase == BookingPhaseEnum.ANCILLARY
					|| destinationPhase == BookingPhaseEnum.PAYMENT
					|| destinationPhase == BookingPhaseEnum.DONE) {
				ctx.phase = BookingPhaseEnum.FLIGHTS_SEARCH;
				return configuration.getBookingFlightSelectPage() + "?nocache=" + nocache + residencyValue;
			}
		}break;
		case "passengersData":{
			if (destinationPhase == BookingPhaseEnum.FLIGHTS_SEARCH ||
					destinationPhase == BookingPhaseEnum.FLIGHTS_SELECTION) {
				cleanContextBeforeNewSearch(ctx);
				ctx.phase = BookingPhaseEnum.FLIGHTS_SEARCH;
				return "";
			}
			if (destinationPhase == BookingPhaseEnum.PASSENGERS_DATA) {
				return "";
			}

			//following behaviours are not permitted. Forwarding to passengers page
			if (destinationPhase == BookingPhaseEnum.ANCILLARY
					|| destinationPhase == BookingPhaseEnum.PAYMENT
					|| destinationPhase == BookingPhaseEnum.DONE) {
				ctx.phase = BookingPhaseEnum.PASSENGERS_DATA;
				return configuration.getBookingPassengersDataPage()  + "?nocache=" + nocache;
			}
		}break;
		case "ancillary":{
			if (destinationPhase == BookingPhaseEnum.FLIGHTS_SEARCH ||
					destinationPhase == BookingPhaseEnum.FLIGHTS_SELECTION) {
				cleanContextBeforeNewSearch(ctx);
				ctx.phase = BookingPhaseEnum.FLIGHTS_SEARCH;
				return "";
			}
			if (destinationPhase == BookingPhaseEnum.PASSENGERS_DATA) {
				ctx.phase = BookingPhaseEnum.PASSENGERS_DATA;
				return "";
			}
			if (destinationPhase == BookingPhaseEnum.ANCILLARY) {
				return "";
			}
			if (destinationPhase == BookingPhaseEnum.PAYMENT
					|| destinationPhase == BookingPhaseEnum.DONE) {
				ctx.phase = BookingPhaseEnum.ANCILLARY;
				return configuration.getBookingAncillaryPage() + "?nocache=" + nocache;
			}

		}
		case "payment":{
			//following behaviours are not permitted. Forwarding to payment page
			if (destinationPhase == BookingPhaseEnum.FLIGHTS_SEARCH
					|| destinationPhase == BookingPhaseEnum.FLIGHTS_SELECTION
					|| destinationPhase == BookingPhaseEnum.PASSENGERS_DATA
					|| destinationPhase == BookingPhaseEnum.ANCILLARY){
				ctx.phase = BookingPhaseEnum.PAYMENT;
				return configuration.getBookingPaymentPage() + "?nocache=" + nocache;
			}
			if (destinationPhase == BookingPhaseEnum.PAYMENT) {
				return "";
			}
			if (destinationPhase == BookingPhaseEnum.DONE) {
				ctx.phase = BookingPhaseEnum.PAYMENT;
				return configuration.getBookingPaymentPage() + "?nocache=" + nocache;
			}
		}break;
		case "done":{
			//following behaviours are not permitted. Forwarding to confirm page
			if (destinationPhase == BookingPhaseEnum.FLIGHTS_SEARCH
					|| destinationPhase == BookingPhaseEnum.FLIGHTS_SELECTION
					|| destinationPhase == BookingPhaseEnum.PASSENGERS_DATA
					|| destinationPhase == BookingPhaseEnum.ANCILLARY
					|| destinationPhase == BookingPhaseEnum.PAYMENT) {
				ctx.phase = BookingPhaseEnum.DONE;
				return configuration.getBookingConfirmationPage() + "?nocache=" + nocache;
			}
			if (destinationPhase == BookingPhaseEnum.DONE) {
				return "";
			}
		}break;
		default:{
			return null;
		}
		}
		return null;
	}

	/**
	 * It returns the number of slices as the number 
	 * @param ctx
	 * @return
	 */
	private int computeTotalSlices(BookingSessionContext ctx) {
		return ctx.searchElements.size();
	}


	/**
	 * Retrieve passenger's data from their customer profile
	 * 
	 */

	private void retrieveLoggedUserProfile(BookingSessionContext ctx, SlingHttpServletRequest request) {
		// Recupero informazioni sull'utente loggato
		MMCustomerProfileData customerProfileData = AlitaliaUtils.getAuthenticatedUser(request);
		if (customerProfileData != null) {
			customerProfileData.setCustomerPinCode(
					customerProfileManager.decodeAndDecryptProperty(customerProfileData.getCustomerPinCode()));

			//chiamata al servizio getProfile
			ProfileRequest profileRequest = new ProfileRequest();
			profileRequest.setSid(IDFactory.getSid());
			profileRequest.setTid(IDFactory.getTid());
			profileRequest.setCustomerProfile(customerProfileData);
			ProfileResponse profileResponse = consumerLoginDelegate.getProfile(profileRequest);
			ctx.loggedUserProfileData = profileResponse.getCustomerProfile();
		}
	}

	/**
	 * return the meals preference for he logged user.
	 * @param ctx
	 * @return
	 */
	public String retrieveLoggedUserMealPreferences(BookingSessionContext ctx) {

		MMCustomerProfileData loggedUserProfileData = ctx.loggedUserProfileData;
		if (ctx.loggedUserProfileData != null ) {
			if (loggedUserProfileData.getPreferences() != null) {
				for (MMPreferenceData preference : loggedUserProfileData.getPreferences()) {
					if (preference.getIdCategory().equals(PREFERENCE_MEAL)) {
						return preference.getShortNamePreference();
					}
				}
			}

		}
		return "";
	}

	/**
	 * retrieve the insurance
	 * @param ctx
	 */
	private void retrieveInsurance(BookingSessionContext ctx) {
		String tid = IDFactory.getTid();
		InsuranceRequest request = new InsuranceRequest(); 
		request.setTid(tid);
		request.setSid(ctx.sid);
		request.setExecution(ctx.execution);
		request.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
		request.setCookie(ctx.cookie);

		AreaValueEnum area = AreaValueEnum.DOM;
		for (DirectFlightData flightData : ctx.selectionAllDirectFlights) {
			if (area == AreaValueEnum.DOM) {
				if (flightData.getFrom().getArea() == AreaValueEnum.INTZ) {
					area = AreaValueEnum.INTZ;
					continue;
				}
				if (flightData.getTo().getArea() == AreaValueEnum.INTZ) {
					area = AreaValueEnum.INTZ;
					continue;
				}
			}
			if (flightData.getFrom().getArea() == AreaValueEnum.INTC) {
				area = AreaValueEnum.INTC;
				break;
			}
			if (flightData.getTo().getArea() == AreaValueEnum.INTC) {
				area = AreaValueEnum.INTC;
				break;
			}

		}
		String countryCode = ctx.market.toUpperCase();
		String arrivalAirport;
		Calendar arrivalDate;
		String departureAirport;
		Calendar departureDate;
		FlightData flight = ctx.flightSelections[0].getFlightData();
		if (flight instanceof DirectFlightData) {
			DirectFlightData directFlightData = (DirectFlightData) flight;
			arrivalAirport = directFlightData.getTo().getCode();
			arrivalDate = directFlightData.getArrivalDate();
			departureAirport = directFlightData.getFrom().getCode();
			departureDate = directFlightData.getDepartureDate();
		}
		else {
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) flight;
			DirectFlightData first = (DirectFlightData) connectingFlightData.getFlights().get(0);
			DirectFlightData last = (DirectFlightData) connectingFlightData.getFlights().get(connectingFlightData.getFlights().size() - 1);
			arrivalAirport = last.getTo().getCode();
			arrivalDate = last.getArrivalDate();
			departureAirport = first.getFrom().getCode();
			departureDate = first.getDepartureDate();
		}
		if (BookingSearchKindEnum.ROUNDTRIP.equals(ctx.searchKind)){
			FlightData arrivalFlight= ctx.flightSelections[1].getFlightData();
			if(arrivalFlight instanceof DirectFlightData){
				DirectFlightData arrivalDirectFlightData=(DirectFlightData)arrivalFlight;
				arrivalDate=arrivalDirectFlightData.getArrivalDate();
			}else{
				ConnectingFlightData arrivalConnectingFlightData=(ConnectingFlightData) arrivalFlight;
				DirectFlightData arrivalLast= (DirectFlightData)arrivalConnectingFlightData.getFlights().get(arrivalConnectingFlightData.getFlights().size() - 1);
				arrivalDate=arrivalLast.getArrivalDate();
			}
		}

		String language = AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase();
		RouteTypeEnum routeType = BookingSearchKindEnum.SIMPLE.equals(ctx.searchKind) 
				? RouteTypeEnum.OUTBOUND : RouteTypeEnum.RETURN;
		request.setArea(area);
		request.setArrivalAirport(arrivalAirport);
		request.setArrivalDate(arrivalDate);
		request.setCountryCode(countryCode);
		request.setDepartureAirport(departureAirport);
		request.setDepartureDate(departureDate);
		request.setLanguageCode(language);
		List<PassengerBase> passengersData = new ArrayList<PassengerBase>();
		for (PassengerBaseData passenger : ctx.selectionRoutes.getPassengers()) {
			passengersData.add(passenger);
		}
		request.setPassengers(passengersData);
		request.setRouteType(routeType);
		request.setFlgUsaCa(false);

		try {
			InsuranceResponse response = ancillaryDelegate.getInsurance(request);
			ctx.insuranceProposalData = response.getPolicy();
		}
		catch (Exception e) {
			logger.debug("Error Retrieve Insurance : " + e.toString());
		}

		// Se non abbiamo ricevuto dati settiamo ctx.insuranceEnabled a false
		if (ctx.insuranceProposalData == null) {
			ctx.insuranceEnabled = false;
		}

	}

	public void addInsurance(BookingSessionContext ctx) {
		ctx.insuranceProposalData.setBuy(Boolean.TRUE);
		ctx.selectionRoutes.setInsurance(ctx.insuranceProposalData);
		ctx.insuranceAmount = computeInsuranceAmount(ctx);
		computeTotalAmounts(ctx);
		ctx.isInsuranceApplied = true;
	}

	public void removeInsurance(BookingSessionContext ctx) {
		ctx.insuranceProposalData.setBuy(Boolean.FALSE);
		ctx.selectionRoutes.setInsurance(null);
		ctx.insuranceAmount = new BigDecimal(0);
		computeTotalAmounts(ctx);
		ctx.isInsuranceApplied = false;
	}

	/**
	 * Perform the meta search with data from external request
	 * 
	 * <p>The result of the search are saved setting (or replacing)
	 * the values of several context variables:
	 * <ul>
	 * <li><code>availableFlights</code></li>
	 * <li><code>extraChargePassengerList</code></li>
	 * <li><code>flightSelections</code></li>
	 * </ul>
	 * </p>
	 *
	 * @param ctx The booking session context to use.
	 */
	public void performMetaSearch(BookingSessionContext ctx, SearchFlightSolutionRequest searchFlightSolutionRequest,
			Resource rootResource) {
		logger.debug("performMetaSearch");

		SearchFlightSolutionResponse searchFlightSolutionResponse = 
				searchFlightsDelegate.metaSearch(searchFlightSolutionRequest);
		ctx.metaSearch = true;
		ctx.availableFlights = searchFlightSolutionResponse.getAvailableFlights();
		ctx.extraChargePassengerList = searchFlightSolutionResponse.getExtraChargePassengerList();
		ctx.cookie = searchFlightSolutionResponse.getCookie();
		ctx.execution = searchFlightSolutionResponse.getExecute();
		ctx.sabreGateWayAuthToken = searchFlightSolutionResponse.getSabreGateWayAuthToken();
		if (ctx.extraChargePassengerList == null) {
			ctx.extraChargePassengerList = new ArrayList<PassengerBase>();
		}

		String solutionId = "";
		solutionId = ctx.availableFlights.getId();
		// set the currency for the current booking process
		/*if (!ctx.availableFlights.getRoutes().isEmpty()) {
			for (TabData tabData : ctx.availableFlights.getTabs()) {
				if (tabData.getType() == RouteTypeEnum.OUTBOUND) {
					for (FlightTabData flightTabData : tabData.getFlightTabs()) {
						if (flightTabData.getCurrency() != null) {
							ctx.currency = flightTabData.getCurrency();
							break;
						}
					}
					break;
				}
			}
		}*/

		/* Selezione dei voli */

		List<FlightSelection> flightSelectionList = new ArrayList<FlightSelection>();
		int elementSelectionIndex = 0;
		for (RouteData routeData : ctx.availableFlights.getRoutesList().get(0).getRoutesList()) {
			FlightSelection flightSelection;
			int solutionFlightIndex = 0;
			for (FlightData flightData : routeData.getFlights()) {

				List<Integer> flightsMileage = null;
				List<String> carriers = computeCarriersList(flightData, ctx);
				BrandData brandData = (flightData.getBrands() != null ? flightData.getBrands().get(0) : null);
				if(brandData == null){
					logger.error("Brand data vuoto");
					throw (new IllegalArgumentException("Brand data null"));
				}
				//solutionId = brandData.getSolutionId();
				flightSelection = new FlightSelection(flightData, brandData.getCode(), null,
						null, carriers, computeIsTariffaLight(brandData.getCode()), solutionFlightIndex, brandData.getSolutionId());

				ctx.flightSelections[elementSelectionIndex] = flightSelection;

				/*if (!flightSelection.isTariffaLight()) {
					flightsMileage = retrieveFlightsMileages(ctx,flightData,0);
				} else {*/
				flightsMileage = new ArrayList<Integer>();
				flightsMileage.add(0);
				//}
				ctx.flightSelections[elementSelectionIndex].setFlightsMileage(flightsMileage);
				Map<String, String> fareRules = computeFareRules(brandData,flightData, flightsMileage, elementSelectionIndex, ctx);
				ctx.flightSelections[elementSelectionIndex].setFareRules(fareRules);



				//				flightSelection = new FlightSelection(flightData, "", null, null, null, false, 0);
				flightSelectionList.add(flightSelection);
				solutionFlightIndex++;
			}
		}

		/* CONTROLLARE NPE 
		ctx.searchCategory = computeSearchCategory(ctx, expectedBrandLight);
		 */
		ctx.solutionForSelectedDate = null;
		ctx.isRefreshed = false;
		ctx.newSearch = false;	

		ctx.flightSelections = (FlightSelection[]) 
				flightSelectionList.toArray(new FlightSelection[flightSelectionList.size()]);

		ctx.selectionRoutes = ctx.availableFlights.getRoutesList().get(0);
		if(ctx.metaSearchData != null && ctx.metaSearchData.getMseType() != null){
			ctx.selectionRoutes.setMseType(ctx.metaSearchData.getMseType());
		}
		ctx.selectionTaxes = ctx.availableFlights.getTaxes();
		/*for ( TaxData taxData : ctx.selectionTaxes) {
			if (taxData.getCurrency() != null && !("").equals(taxData.getCurrency())) {
				ctx.currency = taxData.getCurrency();
				break;
			}
		}*/
		ctx.totalSlices = computeTotalSlices(ctx);
		manageExtraCharge(ctx);
		ctx.totalExtraCharges = computeTotalExtraChargesAmount(ctx);
		logger.debug("BookingSession MetaSearch totalExtraCharges: ["+ctx.totalExtraCharges+"]");

		ctx.readyToPassengersDataPhase = true;
		ctx.coupon = null;
		ctx.selectionCabinClass = computeSelectionCabin(ctx);

		ctx.fullTextFareRules = computeFullTextFareRules(ctx, solutionId, searchFlightSolutionRequest.getTid());

		if (ctx.market.equalsIgnoreCase("us") || ctx.market.equalsIgnoreCase("ca")) {
			ctx.resultBookingDetails = ctx.availableFlights.getRoutesList().get(0).getProperties();
		}

		ctx.brandMap = computeBrandMap(rootResource);
		ctx.codeBrandMap = computeCodeBrandMap(ctx);

		ctx.phase = BookingPhaseEnum.FLIGHTS_SELECTION;
	}

	private void manageExtraCharge(BookingSessionContext ctx) {
		String from = ctx.searchElements.get(0).getFrom().getAirportCode();
		String to = ctx.searchElements.get(0).getTo().getAirportCode();

		if (searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(from, to)
				|| searchFlightsDelegate.isSliceSicilyTerritorialContinuity(from, to)) {
			cleanExtraChargeFareForPassengers(ctx.extraChargePassengerList);
		}
	}

	private void cleanExtraChargeFareForPassengers(List<PassengerBase> extraChargePassengerList) {
		for (PassengerBase passenger : extraChargePassengerList) {
			PassengerBaseData passengerBaseData = (PassengerBaseData) passenger;
			passengerBaseData.setExtraCharge(new BigDecimal(0));
		}

	}


	/**
	 * It computes the fare rules for all brands by the selected flight.
	 * It uses the index of flight and the index of route.
	 * It uses the method "computeFareRules"
	 * @param indexRoute
	 * @param indexFlight
	 * @param ctx
	 * @return the list of all fare rules for each brand for the selected flight
	 */
	public Map<BrandPageData, Map<String, String>> computeFareRuleBrandsByFlight(
			int indexRoute, int indexFlight, BookingSessionContext ctx) {
		Map<BrandPageData, Map<String, String>> fareRuleBrands = new LinkedHashMap<BrandPageData, Map<String,String>>();
		RouteTypeEnum routeType = indexRoute == 0 ? RouteTypeEnum.OUTBOUND : RouteTypeEnum.RETURN;
		if(ctx.searchKind == BookingSearchKindEnum.MULTILEG){
			for (RouteData routeData : ctx.availableFlights.getRoutes()) {
					FlightData flightData = routeData.getFlights().get(indexFlight); // Volo selezionato

					for (BrandData brandData : flightData.getBrands()) {

						BrandPageData page = ctx.codeBrandMap.get(brandData.getCode().toLowerCase());
						if (page != null) {
							List<Integer> flightsMileage = new ArrayList<Integer>();
						/*if (!computeIsTariffaLight(brandData.getCode())) {
							flightsMileage = retrieveFlightsMileages(ctx,flightData,solutionBrandIndex);
						} else {*/
							flightsMileage.add(0);
							//}
							Map<String,String> fareRules = computeFareRules(brandData, flightData, flightsMileage, indexRoute, ctx);

							fareRuleBrands.put(page, fareRules);
						}
					}
			}
		}
		else {
			for (RouteData routeData : ctx.availableFlights.getRoutes()) {
				if (routeData.getType() == routeType) {
					FlightData flightData = routeData.getFlights().get(indexFlight); // Volo selezionato

					for (BrandData brandData : flightData.getBrands()) {

						BrandPageData page = ctx.codeBrandMap.get(brandData.getCode().toLowerCase());
						if (page != null) {
							List<Integer> flightsMileage = new ArrayList<Integer>();
						/*if (!computeIsTariffaLight(brandData.getCode())) {
							flightsMileage = retrieveFlightsMileages(ctx,flightData,solutionBrandIndex);
						} else {*/
							flightsMileage.add(0);
							//}
							Map<String, String> fareRules = computeFareRules(brandData, flightData, flightsMileage, indexRoute, ctx);

							fareRuleBrands.put(page, fareRules);
						}
					}
				}
			}
		}
		return fareRuleBrands;
	}

	/**
	 * It computes the index of the route to refresh
	 * @param ctx
	 * @param elementSelectionIndex
	 * @return
	 */
	public int obtainRouteIndexToRefresh(BookingSessionContext ctx,
			int elementSelectionIndex) {
		if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP) {
			if (ctx.firstSelectionIndex == elementSelectionIndex) {
				if (elementSelectionIndex == 0) {
					return 1;
				} else {
					return 0;
				}
			} else {
				return -1;
			}
		} else {
			return -1;
		}
	}

	/**
	 * It confirms the seats selection and update the passengersData 
	 * @param ctx
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public HashMap<Integer, String>[] confirmSeatSelection(BookingSessionContext ctx,
			SlingHttpServletRequest request) {

		int passengerNoInfantNumber = 0;
		String[] postoAdulti;

		if (ctx.passengersData == null) {
			return null;
		}
		if (ctx.passengersData.getPassengersList() == null) {
			return null;
		}

		//count the number of passengers that are no infant
		for (Passenger pax : ctx.passengersData.getPassengersList()) {
			if (pax.getPassengerType() != PassengerType.INFANT) {
				passengerNoInfantNumber++;
			}
		}
		postoAdulti = new String[passengerNoInfantNumber];

		//retrive the seat preference of passengers by the request
		for (int i = 0; i < passengerNoInfantNumber; i++) {
			postoAdulti[i] = request.getParameter("seatPax_" + i);
		}

		//set the seat preference to each passengers that are no infant
		int i = 0;
		HashMap<Integer, String>[] seatPreferencesForAllPassengers = new HashMap[postoAdulti.length];
		for (Passenger pax : ctx.passengersData.getPassengersList()) {
			if (pax.getPassengerType() != PassengerType.INFANT) {
				HashMap<Integer, String> mappaPostoPasseggero = new HashMap<Integer, String>();
				if (!postoAdulti[i].isEmpty()) {
					String[] voliPostiPasseggero = postoAdulti[i].split(";");
					for (int j = 0; j < voliPostiPasseggero.length; j++) {
						String[] voloPostoPasseggero = voliPostiPasseggero[j].split(":");
						String formattedSeat = "";
						if (!voloPostoPasseggero[1].equals("null")) {
							if (voloPostoPasseggero[1].length()==2) {
								formattedSeat = voloPostoPasseggero[1].charAt(0) + "_" + voloPostoPasseggero[1].substring(1);
							} else {
								formattedSeat = voloPostoPasseggero[1].substring(0,2) + "_" + voloPostoPasseggero[1].substring(2);
							}
							mappaPostoPasseggero.put(Integer.valueOf(voloPostoPasseggero[0]), formattedSeat);
						}
					}
				}
				seatPreferencesForAllPassengers[i] = mappaPostoPasseggero;
				i++;
			}
		}
		return seatPreferencesForAllPassengers;
	}

	/**
	 * It resets the passengersData cleaning the seat preferences
	 * @param ctx
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public HashMap<Integer, String>[] resetSeatSelection(BookingSessionContext ctx) {
		if (ctx.passengersData == null) {
			return null;
		}
		if (ctx.passengersData.getPassengersList() == null) {
			return null;
		}
		int notInfantSize = 0;
		for (Passenger pax : ctx.passengersData.getPassengersList()) {
			if (pax.getPassengerType() != PassengerType.INFANT) {
				notInfantSize++;
			}
		}

		HashMap<Integer, String>[] seatPreferencesForAllPassengers = new HashMap[notInfantSize];
		int i = 0;
		for (Passenger pax : ctx.passengersData.getPassengersList()) {
			if (pax.getPassengerType() != PassengerType.INFANT) {
				HashMap<Integer, String> mappaPostoPasseggero = new HashMap<Integer, String>();
				seatPreferencesForAllPassengers[i] = mappaPostoPasseggero;
			}
		}
		return seatPreferencesForAllPassengers;
	}

	/**
	 * return true if the passenger has an associated infant
	 * @param passengers
	 * @param indexPassenger
	 * @return
	 */
	public int computeReferedInfant(List<PassengerBaseData> passengers,
			int indexPassenger) {
		int index = 0;
		for (PassengerBaseData pax : passengers) {
			if (pax.getType() == PassengerTypeEnum.INFANT) {
				InfantPassengerData infantPassenger = (InfantPassengerData) pax;
				if (infantPassenger.getAdultRefent() == indexPassenger) {
					return index;
				}
			}
			index++;
		}
		return -1;
	}

	/**
	 * return if there are flights into the ribbon
	 * @param ctx
	 * @return
	 */

	public boolean existFlightsRibbon(BookingSessionContext ctx) {
		AvailableFlightsData flightsData = ctx.availableFlights;
		if (flightsData != null && flightsData.getTabs() != null) {
			for (TabData tab : flightsData.getTabs()) {

				if (tab.getFlightTabs() == null) {
					continue;
				}

				for(FlightTabData tabData : tab.getFlightTabs()) {
					if (!tabData.getPrice().equals(BigDecimal.valueOf(0))) {
						logger.debug("[BRAND] Found flight " + tabData.getPrice().toString());
						logger.debug("[BRAND] ZERO " + BigDecimal.valueOf(0));
						return true;
					}
				}
			}
		}

		return false;
	}

	private boolean computeIfMealsAreEnabled(BookingSessionContext ctx) {

		// check if at least one flight is AZ
		boolean step1NotDOM = false;
		boolean step2RangeFlightNumber = false;
		boolean step3Carrier = false;
		boolean step4TimeOrCabin = false;
		ctx.isLimitedMeals = false;

		ArrayList<String> mealsAzCarrier = new ArrayList<String>();
		for (String carrier : MEALS_AZ_CARRIER) {
			mealsAzCarrier.add(carrier);
		}

		if (ctx.isBusCarrier && ctx.selectionAllDirectFlights.size() == 1) {
			return false;
		}

		//check step 1
		for (DirectFlightData flight : ctx.selectionAllDirectFlights) {
			if (!(flight.getFrom().getArea() == AreaValueEnum.DOM 
					&& flight.getTo().getArea() == AreaValueEnum.DOM)) {
				step1NotDOM = true;
				break;
			}
		}

		//check step 2
		int countStep2 = 0;
		for (DirectFlightData flight : ctx.selectionAllDirectFlights) {
			Integer flightNumber = null;
			try {
				flightNumber = Integer.parseInt(flight.getFlightNumber());
			} catch (Exception e) {
				logger.error("Error diring computing if meal is enabled.");
			}
			if (flightNumber != null) {
				if (!(flightNumber >= LB_FLIGHT_NUMBER_3 && flightNumber <= UB_FLIGHT_NUMBER_3)
						&& !(flightNumber >= LB_FLIGHT_NUMBER_5 && flightNumber <= UB_FLIGHT_NUMBER_5)
						&& !(flightNumber >= LB_FLIGHT_NUMBER_7 && flightNumber <= UB_FLIGHT_NUMBER_7)) {
					step2RangeFlightNumber = true;
					countStep2++;
				}
			}
		}

		//check step 3
		int countStep3 = 0;
		for (DirectFlightData flight : ctx.selectionAllDirectFlights) {
			String operationalCarrier = flight.getCarrier();
			if (flight.getOperationalCarrier() != null) {
				if (!flight.getOperationalCarrier().equals("")) {
					operationalCarrier = flight.getOperationalCarrier();
				}
			}
			if (mealsAzCarrier.contains(operationalCarrier)) {
				step3Carrier = true;
				countStep3++;
			}
		}

		//check step 4
		int countStep4 = 0;
		for (DirectFlightData flight : ctx.selectionAllDirectFlights) {
			if (flight.getDurationHour() > FLIGHT_MINIMAL_DURATION_FOR_MEALS 
					|| ctx.selectionCabinClass == CabinEnum.BUSINESS) {
				step4TimeOrCabin = true;
				countStep4++;
			}
		}

		if (step1NotDOM && step2RangeFlightNumber 
				&& step3Carrier && step4TimeOrCabin) {

			if (countStep2 == ctx.selectionAllDirectFlights.size()
					&& countStep3 == ctx.selectionAllDirectFlights.size()
					&& countStep4 == ctx.selectionAllDirectFlights.size()) {
				return true;
			} else {
				ctx.isLimitedMeals = true;
				return true;
			}

		} else {
			return false;
		}
	}

	private boolean computeIfInsuranceIsEnabled(BookingSessionContext ctx) {
		return (ctx.searchKind != BookingSearchKindEnum.MULTILEG && 
				ctx.market.equalsIgnoreCase("IT") && 
				!ctx.isCarnetProcess &&
				!ctx.isBusCarrier);
	}

	/**
	 * obtain the name of applicant passenger by concat name and surname (used in statistics)
	 * @param ctx
	 * @return
	 */
	private String obtainApplicantNameForStatistics(BookingSessionContext ctx) {
		String name = "";
		if (ctx.selectionRoutes != null) {
			if (ctx.selectionRoutes.getPassengers() != null) {
				for (PassengerBaseData pax : ctx.selectionRoutes.getPassengers()) {
					if (pax instanceof ApplicantPassengerData) {
						ApplicantPassengerData applicantPassengerData = (ApplicantPassengerData) pax;
						name = applicantPassengerData.getName() + " " + applicantPassengerData.getLastName();
					}
				}
			}
		}
		return name;
	}

	/**
	 * It return true if the ancillary step must be skipped, 
	 * because there are no ancillary to show.
	 * The method confirmAncillaryData is called in the servlet,
	 * in order to perform the createPNR.
	 * @param ctx
	 * @return
	 */
	public boolean skipAncillaryStep(BookingSessionContext ctx) {
		return !ctx.mealsEnabled && !ctx.isSeatMapSelectionAllowed && !ctx.insuranceEnabled;
	}

	/**
	 * It save the cart recovery parameters in ctx only if the 
	 * territorial continuity is allowed.
	 * It returns the correct query string according to the territorial continuity:
	 * If the ContinuitaTerritoriale is allowed, then it return empty string in order to 
	 * do not show the query string in the territorial continuity page.
	 * @param ctx
	 * @param cartRecoveryParams has the foramt "parm1=value1&param2=value2"
	 * @return 
	 */
	public String manageCartRecoveryParameters(BookingSessionContext ctx,
			String cartRecoveryParams) {
		if (ctx.isAllowedContinuitaTerritoriale) {
			ctx.cartRecoveryParams = cartRecoveryParams;
			return "";
		}
		return cartRecoveryParams != null ? "?" + cartRecoveryParams : "";
	}

	/**
	 * It calculate the amount of Credit Card Fee
	 * 
	 * @param ctx
	 * @return
	 */

	private BigDecimal getCcFeeTotalAmount (BookingSessionContext ctx) {
		BigDecimal totalCcFee = new BigDecimal(0);
		if (ctx != null && ctx.prenotation != null && ctx.prenotation.getPassengers() != null) {
			for (PassengerBaseData passenger : ctx.prenotation.getPassengers()) {
				if (passenger.getFee() != null) {
					totalCcFee = totalCcFee.add(passenger.getFee());
				}
			}
		}

		return totalCcFee;
	}

	public void thankYouPageViewStatistic(BookingSessionContext ctx, String tid, boolean showCallCenterMsg
			, String site) {
		RegisterPaymentBookingEtktStatisticRequest bookingEtktStatisticRequest = 
				new RegisterPaymentBookingEtktStatisticRequest(tid, ctx.sid);
		bookingEtktStatisticRequest.setPnr(ctx.prenotation.getPnr());

		String importo = "";
		try {
			importo = NumberFormat.getInstance(ctx.locale).format(ctx.grossAmount.doubleValue()) + ctx.currency;
		} catch (Exception e) {
			logger.warn("Error casting gross amount to string for statistics: ", e.getMessage());
		}
		bookingEtktStatisticRequest.setImporto(importo);
		bookingEtktStatisticRequest.setUserId(ctx.loggedUserProfileData != null ? ctx.loggedUserProfileData.getCustomerNumber() : null);
		bookingEtktStatisticRequest.setClientIP(ctx.clientIP);
		bookingEtktStatisticRequest.setSessionId(ctx.sessionId);
		bookingEtktStatisticRequest.setSiteCode(site);
		bookingEtktStatisticRequest.setType(StatisticsDataTypeEnum.PAYMENT_CHECKOUT);
		String tktNumber = "";
		if(ctx.prenotation != null && ctx.prenotation.getPassengers() != null 
				&& ctx.prenotation.getPassengers().size() > 0){
			tktNumber = ctx.prenotation.getPassengers().stream()
					.flatMap(pass -> (pass.getTickets()!=null ? pass.getTickets().stream() : (new LinkedList<TicketInfoData>()).stream()))
					.map(tkt -> tkt.getTicketNumber()).distinct()
					.collect(Collectors.joining(","));
		}
		bookingEtktStatisticRequest.setEticket(tktNumber);
		bookingEtktStatisticRequest.setSuccess(!showCallCenterMsg);

		RegisterStatisticsResponse statisticResponse = registerStatisticsDelegate.registerStatistics(bookingEtktStatisticRequest);

		if (logger.isDebugEnabled())
			logger.debug("Called thankYouPageViewStatistic. Response: "  + statisticResponse);

		if (statisticResponse == null) {
			logger.error("Empty response in Thank You Page View statistic");
		}
	}



	private void sendMail(BookingSessionContext ctx, PaymentData paymentData, I18n i18n, String tid, String from, String subject) throws BookingPaymentException{
		try {
			// Invio mail
			// create email message object
			EmailMessage emailMessage = new EmailMessage();
			emailMessage.setFrom(from);
			emailMessage.setTo(paymentData.getEmail());
			emailMessage.setSubject(subject);
			emailMessage.setMessageText(paymentEmailGenerator.prepareXsltMail(ctx, paymentData, i18n));
			emailMessage.setIsBodyHtml(true);
			emailMessage.setDeliveryNotification(DeliveryNotificationOptionsEnum.NONE);
			emailMessage.setPriority(MailPriorityEnum.NORMAL);

			// create AgencySendMailRequest
			AgencySendMailRequest mailRequest = new AgencySendMailRequest();
			mailRequest.setTid(tid);
			mailRequest.setSid(ctx.sid);
			mailRequest.setEmailMessage(emailMessage);

			// get AgencySendMailResponse
			AgencySendMailResponse mailResponse =
					businessLoginService.sendMail(mailRequest);

			logger.debug("MAIL isSendMailSuccessful: "+mailResponse.isSendMailSuccessful());
		} catch (Exception e) {
			logger.error("Error during sendMail ", e);
			throw new BookingPaymentException(
					BookingPaymentException.BOOKING_ERROR_RETRIEVETICKETS,
					"Unable to perform sendMail");
		}
	}

	private static void replaceSpecialCharacters(List<Passenger> passengers){
		if (passengers != null){
			for (Passenger passenger : passengers){
				if (passenger.getName() != null){
					passenger.setName(replaceUnwantedCharacters(passenger.getName()));
				}
				if (passenger.getSurname() != null){
					passenger.setSurname(replaceUnwantedCharacters(passenger.getSurname()));
				}
				if (passenger.getSecondName() != null){
					passenger.setSecondName(replaceUnwantedCharacters(passenger.getSecondName()));
				}
			}
		}
	}

	private static String replaceUnwantedCharacters(String input){
		return input.replaceAll("à", "a").replaceAll("á", "a")
				.replaceAll("À", "A").replaceAll("Á", "A")
				.replaceAll("è", "e").replaceAll("é", "e")
				.replaceAll("È", "E").replaceAll("É", "E")
				.replaceAll("ì", "i").replaceAll("í", "i")
				.replaceAll("Ì", "I").replaceAll("Í", "I")
				.replaceAll("ò", "o").replaceAll("ó", "o")
				.replaceAll("Ò", "O").replaceAll("Ó", "O")
				.replaceAll("ù", "u").replaceAll("ú", "u")
				.replaceAll("Ù", "U").replaceAll("Ú", "U")
//  Aggiunti altri caratteri per il Bug "CaratteriSeciali"				
//  ÁáÀàÂâÃãÄäĄąĆćÇçÈèÉéÊêËëĘęÌìÍíÎîÏïJ́j́ŁłŃńÑñŐőÔôÒòÓóÔôÖöÕõŚśÚúŰűÙùÛûÜüŸÿÝýŹźŻż
				.replaceAll("Á", "A").replaceAll("á", "a")
				.replaceAll("À", "A").replaceAll("à", "a")
				.replaceAll("Â", "A").replaceAll("â", "a")
				.replaceAll("Ã", "A").replaceAll("ã", "a")
				.replaceAll("Ä", "A").replaceAll("ä", "a")
				.replaceAll("Ą", "A").replaceAll("ą", "a")
				.replaceAll("Ć", "C").replaceAll("ć", "c")
				.replaceAll("Ç", "C").replaceAll("ç", "c")	
				.replaceAll("È", "E").replaceAll("è", "e")
				.replaceAll("É", "E").replaceAll("é", "e")
				.replaceAll("Ê", "E").replaceAll("ê", "e")
				.replaceAll("Ë", "E").replaceAll("ë", "e")
				.replaceAll("Ę", "E").replaceAll("ę", "e")	
				.replaceAll("Ì", "I").replaceAll("ì", "i")	
				.replaceAll("Í", "I").replaceAll("í", "i")					
				.replaceAll("Î", "I").replaceAll("î", "i")					
				.replaceAll("Ï", "I").replaceAll("ï", "i")	
				.replaceAll("J́", "J").replaceAll("j́", "j")	
				.replaceAll("Ł", "L").replaceAll("ł", "l")	
				.replaceAll("Ń", "N").replaceAll("ń", "n")	
				.replaceAll("Ñ", "N").replaceAll("ñ", "n")	
				.replaceAll("Ő", "O").replaceAll("ő", "o")	
				.replaceAll("Ô", "O").replaceAll("ô", "o")																					
				.replaceAll("Ò", "O").replaceAll("ò", "o")																					
				.replaceAll("Ó", "O").replaceAll("ó", "o")																					
				.replaceAll("Ô", "O").replaceAll("ô", "o")																					
				.replaceAll("Ö", "O").replaceAll("ö", "o")																					
				.replaceAll("Õ", "O").replaceAll("õ", "o")	
				.replaceAll("Ś", "S").replaceAll("ś", "s")																									
				.replaceAll("Ú", "U").replaceAll("ú", "u")	
				.replaceAll("Ű", "U").replaceAll("ű", "u")																									
				.replaceAll("Ù", "U").replaceAll("ù", "u")																									
				.replaceAll("Û", "U").replaceAll("û", "u")																									
				.replaceAll("Ü", "U").replaceAll("ü", "u")																									
				.replaceAll("Ÿ", "Y").replaceAll("ÿ", "y")																									
				.replaceAll("Ý", "Y").replaceAll("ý", "y")	
				.replaceAll("Ź", "Z").replaceAll("ź", "z")																									
				.replaceAll("Ż", "Z").replaceAll("ż", "z");

	}

	private String setSpecialCase(BrandPenaltyData brand, BookingSessionContext ctx){
		String result = "";
		switch(brand.getMinPriceCurrency()){
			case "PFM":{
				result = brand.getMaxPriceCurrency() + " " +brand.getMaxPrice() + "*";
				break;
			}
			case "NPP":{
				result = ctx.i18n.get("fareRules.fa.change.label") + "#";
				break;
			}
			case "FFN60":{
				String label = ctx.i18n.get("fareRules.primoGratisPoiPagamento.label");
				result = label.replace("{0}",String.valueOf(brand.getMaxPrice()));
				break;
			}
			case "FFN70": {
				String label = ctx.i18n.get("fareRules.primoGratisPoiPagamento.label");
				result = label.replace("{0}",String.valueOf(brand.getMaxPrice()));
				break;
			}
			case "DEF€":{
				result = ctx.i18n.get("fareRules.zz.change.label") + "/";
				break;
			}
			case "DEFV":{
				result = ctx.i18n.get("fareRules.co.change.label");
				break;
			}
			case "DEFX":{
				result = ctx.i18n.get("fareRules.fa.change.label") + "/";
				break;
			}
			case "NRB":{
				result = ctx.i18n.get("fareRules.notResidentBefore.change.label");
				break;
			}
			case "NRA":{
				result = ctx.i18n.get("fareRules.notResidentAfter.change.label");
				break;
			}
		}
		return result;
	}

    private void setAsterisck(Map<String,String> tempFareRulesList) {
        for (String a : tempFareRulesList.keySet()) {
            if(tempFareRulesList.get(a).contains("#")){
                tempFareRulesList.put(a,tempFareRulesList.get(a).split("#")[0] + "**");
            }
        }
    }

	//controllo se almeno una tratta rientra nel periodo chiusura aeroporto Linate
	private boolean isLinAirportClosed(List<SearchElement> searchElements) {
		boolean retval = false;

		String date_1 = "27-07-2019";
		String date_2 = "27-10-2019";
		long milliseconds = 0;
		long milliseconds_2 = 0;
		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		try {
			Date d_1 = f.parse(date_1);
			milliseconds = d_1.getTime();
			Date d_2 = f.parse(date_2);
			milliseconds_2 = d_2.getTime();

		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (searchElements != null) {
			int i = 0;
			for (SearchElement searchElement : searchElements) {
				i++;
				long depDate = searchElement.getDepartureDate().getTimeInMillis();
				if ((depDate >= milliseconds) && (depDate <= milliseconds_2)) {
					return true;
				}
			}
		}
		return retval;
	}
}