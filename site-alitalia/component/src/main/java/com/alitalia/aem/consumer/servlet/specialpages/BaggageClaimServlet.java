package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.global.enumeration.BaggageClaimReasons;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.BaggageClaimData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "baggageclaimsubmit" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class BaggageClaimServlet extends GenericFormValidatorServlet {
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BusinessLoginService businessLoginService;
	
	private static final int DIM_FORM = 10;
	private static final String KEYVALUE_SEPARATOR = ":";
	private static final String ELEMENT_SEPARATOR = System.getProperty("line.separator");
	
	// Static Property For Mails
	private static final String COMPONENT_NAME = "baggage_claim";
	private static final String PARSYS_NAME = "page-component";
	private static final String MAIL_FROM_PROPERTY_NAME = "mailFrom";
	private static final String MAIL_TO_PROPERTY_NAME = "mailTo";
	private static final String MAIL_SUBJECT_PROPERTY_NAME = "subject";
	private static final String MAIL_FROM_DEFAULT = "backlinesales@albacall.eu";
	private static final String MAIL_TO_DEFAULT = "backlinesales@albacall.eu";
	private static final String MAIL_SUBJECT_DEFAULT = "Richiesta Rimborso Bagaglio Extra";
	
	private static final String ATTR_PIR = "pir";
	private static final String ATTR_REASON = "reason";
	private static final String ATTR_YEAR = "year";
	private static final String ATTR_MONTH = "month";
	private static final String ATTR_DAY = "day";
	private static final String ATTR_FLIGHTNUMBER = "flightnumber";
	private static final String ATTR_FLIGHTCARRIER = "flightcarrier";
	private static final String ATTR_EMAIL = "email";
	private static final String ATTR_PHONENUMBER = "phonenumber";
	private static final String ATTR_SURNAME = "surname";
	private static final String ATTR_NAME = "name";
	private static final String ATTR_TICKETNUMBER = "ticketnumber";
	private static final String ATTR_OTHERREASONS = "reason_explain";
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[BaggageClaimServlet] validateForm");
		
		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			return validator.validate();
			
		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Error during parameters validation", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		logger.debug("[BaggageClaimServlet] performSubmit");
		
		boolean mailResult = sendMail(request);
		logger.debug("Mail Sended = " + mailResult);
		if (mailResult) {
			goBackSuccessfully(request, response);
		}
		else {
			throw new IOException("Service Error");
		}
		
		
	}
	
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[BaggageClaimServlet] saveDataIntoSession");
		
		// create baggageClaimData object and save it into session
		BaggageClaimData baggageClaimData = new BaggageClaimData();
		baggageClaimData.setName(request.getParameter(ATTR_NAME));
		baggageClaimData.setSurname(request.getParameter(ATTR_SURNAME));
		baggageClaimData.setPhonenumber(request.getParameter(ATTR_PHONENUMBER));
		baggageClaimData.setEmail(request.getParameter(ATTR_EMAIL));
		baggageClaimData.setFlightcarrier(request.getParameter(ATTR_FLIGHTCARRIER));
		baggageClaimData.setFlightnumber(request.getParameter(ATTR_FLIGHTNUMBER));
		baggageClaimData.setReason(request.getParameter(ATTR_REASON));
		baggageClaimData.setPir(request.getParameter(ATTR_PIR));
		try {
			baggageClaimData.setDay(Integer.parseInt(
					request.getParameter(ATTR_DAY)));
		} catch (NumberFormatException n) { }
		try {
			baggageClaimData.setMonth(Integer.parseInt(
					request.getParameter(ATTR_MONTH)));
		} catch (NumberFormatException n) { }
		try {
			baggageClaimData.setYear(Integer.parseInt(
					request.getParameter(ATTR_YEAR)));
		} catch (NumberFormatException n) { }
		
		// go back to form with error
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		
		baggageClaimData.setError(i18n.get(I18nKeySpecialPage.ERROR_SERVICE));
		
		request.getSession().setAttribute(BaggageClaimData.NAME,
				baggageClaimData);
		
	}
	
	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {
		logger.debug("[BaggageClaimServlet] setValidatorParameters");

		// get parameters from request
		String name = request.getParameter(ATTR_NAME);
		String surname = request.getParameter(ATTR_SURNAME);
		String phonenumber = request.getParameter(ATTR_PHONENUMBER);
		String email = request.getParameter(ATTR_EMAIL);
		String flightcarrier = request.getParameter(ATTR_FLIGHTCARRIER);
		String flightnumber = request.getParameter(ATTR_FLIGHTNUMBER);
		String ticketnumber = request.getParameter(ATTR_TICKETNUMBER);
		String day = request.getParameter(ATTR_DAY);
		String month = request.getParameter(ATTR_MONTH);
		String year = request.getParameter(ATTR_YEAR);
		String reason = request.getParameter(ATTR_REASON);

		// add validate conditions
		
		// name
		validator.addDirectConditionMessagePattern(ATTR_NAME, name,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern(ATTR_NAME, name,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.NAME_ERROR_NOT_VALID,
				"isAlphabeticWithAccentAndSpaces");
		
		// surname
		validator.addDirectConditionMessagePattern(ATTR_SURNAME, surname,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.SURNAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern(ATTR_SURNAME, surname,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.NAME_ERROR_NOT_VALID,
				"isAlphabeticWithAccentAndSpaces");
		
		// phone number
		validator.addDirectConditionMessagePattern(ATTR_PHONENUMBER, phonenumber,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.PHONE_NUMBER_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern(ATTR_PHONENUMBER, phonenumber,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.PHONE_NUMBER_ERROR_NOT_VALID, "isNumber");
		
		// email
		validator.addDirectConditionMessagePattern(ATTR_EMAIL, email,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.EMAIL_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern(ATTR_EMAIL, email,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID, "isEmail");
		
		// flight carrier
		validator.addDirectConditionMessagePattern(ATTR_FLIGHTCARRIER, flightcarrier,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.FLIGHT_CARRIER_ERROR_EMPTY, "isNotEmpty");
		validator.setAllowedValuesMessagePattern(ATTR_FLIGHTCARRIER, flightcarrier,
				new String[]{"AZ"}, I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.REASON_ERROR_NOT_VALID);
		
		//flight number
		validator.addDirectConditionMessagePattern(ATTR_FLIGHTNUMBER, flightnumber,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.FLIGHT_NUMBER_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern(ATTR_FLIGHTNUMBER, flightnumber,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.FLIGHT_NUMBER_ERROR_NOT_VALID, "isNumber");
		
		// ticket number
		validator.addDirectConditionMessagePattern(ATTR_TICKETNUMBER, ticketnumber,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.TICKET_NUMBER_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern(ATTR_TICKETNUMBER, ticketnumber,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.TICKET_NUMBER_ERROR_NOT_VALID,
				"isNumberWithSpace");
		
		// date
		validator.addDirectConditionMessagePattern(ATTR_DAY, day,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.FLIGHT_DAY_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern(ATTR_MONTH, month,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.FLIGHT_MONTH_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern(ATTR_YEAR, year,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.FLIGHT_YEAR_ERROR_EMPTY, "isNotEmpty");
		if (null != day && null != month && null != year) {
			String date = day + "/" + month + "/" + year;
			validator.addDirectConditionMessagePattern(ATTR_DAY, date,
					I18nKeySpecialPage.FLIGHT_DATE_ERROR_FUTURE,
					I18nKeySpecialPage.FLIGHT_DATE_ERROR_NOT_VALID, "isBeforeNow");
		}
		
		// reason
		validator.addDirectConditionMessagePattern(ATTR_REASON, reason,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.FLIGHT_NUMBER_ERROR_EMPTY, "isNotEmpty");
		validator.setAllowedValuesMessagePattern(ATTR_REASON, reason,
				BaggageClaimReasons.values(),
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.REASON_ERROR_NOT_VALID);
		
		return validator;

	}
	
	/*
	 * sendMail
	 * 
	 */
	private boolean sendMail(SlingHttpServletRequest request) {
		logger.debug("[BaggageClaimServlet] sendMail");
		
		// current page
		Page currentPage = AlitaliaUtils.getPage(request.getResource());
		
		// email from
	    String mailFrom = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_FROM_PROPERTY_NAME);
	    if (mailFrom == null) {
	    	mailFrom = MAIL_FROM_DEFAULT;
	    }
	    
	    // email to
	    String mailTo = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_TO_PROPERTY_NAME);
	    if (mailTo == null) {
	    	mailTo = MAIL_TO_DEFAULT;
	    }
	    
	    // email subject
	    String subject = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_SUBJECT_PROPERTY_NAME);
	    if (subject == null) {
	    	subject = MAIL_SUBJECT_DEFAULT;
	    }
	    
	    // email body
		String messageText = generateBodyMail(request);
	    
		logger.debug("[ MillemigliaKidsServlet] sendMail -- Email data: "
				+ "[From: {" + mailFrom + "}] - [To: {" + mailTo + "}] - "
				+ "[Subject: {" + subject + "}] - [Body: {" + messageText + "}]");
		
		// create email message object
		EmailMessage emailMessage = new EmailMessage();
		emailMessage.setFrom(mailFrom);
		emailMessage.setTo(mailTo);
		emailMessage.setSubject(subject);
		emailMessage.setMessageText(messageText);
		emailMessage.setIsBodyHtml(false);
		emailMessage.setDeliveryNotification(DeliveryNotificationOptionsEnum.NONE);
		emailMessage.setPriority(MailPriorityEnum.NORMAL);
		
		// create AgencySendMailRequest
		AgencySendMailRequest mailRequest = new AgencySendMailRequest();
		mailRequest.setTid(IDFactory.getTid());
		mailRequest.setSid(IDFactory.getSid(request));
		mailRequest.setEmailMessage(emailMessage);
		
		// get AgencySendMailResponse
		AgencySendMailResponse mailResponse =
				businessLoginService.sendMail(mailRequest);

		// return status
		return mailResponse.isSendMailSuccessful();

	}
	
	/*
	 * generateBodyMail
	 * 
	 */
	private String generateBodyMail(SlingHttpServletRequest request) {
		logger.debug("[BaggageClaimServlet] generateBodyMail");
		
		String[][] data = new String[DIM_FORM][2];
		
		// creating Keys
		int k;
		for (k = 0; k < DIM_FORM; k++) {
			String name = "KeyField" + Integer.toString(k);
			data[k][0] = request.getParameter(name);
		}
		
		// get I18N
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		I18n i18n = new I18n(resourceBundle);
		
		// getting Values
		String name = request.getParameter(ATTR_NAME);
		String surname = request.getParameter(ATTR_SURNAME);
		String phonenumber = request.getParameter(ATTR_PHONENUMBER);
		String email = request.getParameter(ATTR_EMAIL);
		String ticketnumber = request.getParameter(ATTR_TICKETNUMBER);
		String flightnumber = request.getParameter(ATTR_FLIGHTNUMBER);
		String day = request.getParameter(ATTR_DAY);
		String month = request.getParameter(ATTR_MONTH);
		String year = request.getParameter(ATTR_YEAR);
		String reason = request.getParameter(ATTR_REASON);
		String pir = request.getParameter(ATTR_PIR);
		String otherreasons = request.getParameter(ATTR_OTHERREASONS);
		
		// setting Values
		data[0][1] = (null != name) ? name : "";
		data[1][1] = (null != surname) ? surname : "";
		data[2][1] = (null != phonenumber) ? phonenumber : "";
		data[3][1] = (null != email) ? email : "";
		data[4][1] = (null != ticketnumber) ? ticketnumber : "";
		data[5][1] = (null != flightnumber) ? flightnumber : "";
		data[6][1] = ""; // (date of flight)
		data[7][1] = (null != reason) ? i18n.get("specialpage.reason." + reason + ".label") : "";
		data[8][1] = (null != pir) ? pir : "";
		data[9][1] = (null != otherreasons) ? otherreasons : "";
		
		// setting Date of Flight
		day = (null != day) ? day : ""; 
		month = (null != month) ? month : "";
		year = (null != year) ? year : "";
		if (!"".equals(day) && !"".equals(month) && !"".equals(year)) {
			try {
				List<String> months = new ArrayList<String>();
				months = I18nKeyCommon.getMonths();
				int numberMonth = Integer.parseInt(month);
				String keyMonth = months.get(numberMonth - 1);
				month = i18n.get(keyMonth);
				data[6][1] = day + "/" + month + "/" + year;
			} catch (NumberFormatException e) { }
		}
		
		// creating email message and return it
		String emailText = "";
		for (k = 0; k < DIM_FORM; k++) {
			emailText = emailText + data[k][0] + KEYVALUE_SEPARATOR + data[k][1];
			if (k < DIM_FORM - 1) {
				emailText = emailText + ELEMENT_SEPARATOR;
			}

		}
		return emailText;
		
	}

	/*
	 * getComponentProperty
	 * 
	 */
	private String getComponentProperty(Page ancestor, String parsysName,
			String compName, String propName) {
		logger.debug("[BaggageClaimServlet] getComponentProperty");
		
		Resource pageResource = ancestor.getContentResource();	
		String prop = null;
		try{
			prop = pageResource.getChild(parsysName).getChild(compName)
					.getValueMap().get(propName, String.class);
		}
		catch(Exception e) {}
		
		return prop;
	}
	
}