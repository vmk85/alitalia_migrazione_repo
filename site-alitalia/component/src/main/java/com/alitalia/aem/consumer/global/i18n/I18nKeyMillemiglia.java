package com.alitalia.aem.consumer.global.i18n;

public class I18nKeyMillemiglia {

	public static final String CLUB_ULISSE = "millemiglia.clubUlisse.label";
	public static final String CLUB_FRECCIA_ALATA = "millemiglia.clubFrecciaAlata.label";
	public static final String CLUB_FRECCIA_ALATA_PLUS = "millemiglia.clubFrecciaAlataPlus.label";
	
	/* STRINGHE DI ERRORE */
	public static final String GENDER_ERROR_EMPTY = "millemiglia.gender.error.empty";
	public static final String ADDRESS_ERROR_EMPTY = "millemiglia.address.error.empty";
	public static final String CAP_ERROR_EMPTY = "millemiglia.cap.error.empty";
	public static final String CITY_ERROR_EMPTY = "millemiglia.city.error.empty";
	public static final String CITY_ERROR_INVALID = "millemiglia.city.error.invalid";
	public static final String NATION_ERROR_EMPTY = "millemiglia.nation.error.empty";
	public static final String EMAIL_ERROR_EMPTY = "millemiglia.email.error.empty";
	public static final String EMAIL_ERROR_INVALID = "millemiglia.email.error.invalid";
	public static final String NOMEAZIENDA_ERROR_EMPTY = "millemiglia.nomeAzienda.error.empty";
	public static final String PREFISSONAZIONALE_ERROR_EMPTY = "millemiglia.prefissoNazionale.error.empty";
	public static final String PREFISSOAREA_ERROR_EMPTY = "millemiglia.prefissoArea.error.empty";
	public static final String TELEFONO_ERROR_EMPTY = "millemiglia.telefono.error.empty";
	public static final String TIPOTELEFONO_ERROR_EMPTY = "millemiglia.tipoTelefono.error.empty";
	public static final String NICKNAME_ERROR_EMPTY = "millemiglia.nickname.error.empty";
	public static final String PIN_ERROR_EMPTY = "millemiglia.pin.error.empty";
	public static final String CONFERMAPIN_ERROR_EMPTY = "millemiglia.confermapin.error.empty";
	public static final String CONFERMAPIN_ERROR_NOTCORRESPONDING = "millemiglia.confermapin.error.notcorresponding";
	public static final String NEWSLETTER_ERROR_EMPTY = "millemiglia.newsletter.error.empty";
	public static final String TIPOCONTATTO_ERROR_EMPTY = "millemiglia.tipoContatto.error.empty";
	public static final String LINGUACORRISPONDENZA_ERROR_EMPTY = "millemiglia.linguaCorrispondenza.error.empty";
	public static final String LINGUACORRISPONDENZA_ERROR_INVALID = "millemiglia.linguaCorrispondenza.error.invalid";
	public static final String CONSENSODATIPERSONALI_ERROR_EMPTY = "millemiglia.consensoDatiPersonali.error.empty";
	public static final String TIPOPASTO_ERROR_EMPTY = "millemiglia.tipoPasto.error.empty";
	public static final String TIPOPOSTO_ERROR_EMPTY = "millemiglia.tipoPosto.error.empty";
	public static final String ATTIVITA_ERROR_EMPTY = "millemiglia.attivita.error.empty";
	public static final String DATE_ERROR_INVALID = "millemiglia.date.error.invalid";
	
	public static final String ERROR_MESSAGE_INVALID_TICKET = "millemiglia.invalidticket.error";
	public static final String ERROR_MESSAGE_MISSING_TICKET = "millemiglia.invalidticket.missing.error";
	public static final String ERROR_MESSAGE_SURNAME_TICKET = "millemiglia.invalidticket.surname.error";
	public static final String ERROR_MESSAGE_MILES_REQUEST_FAILED = "millemiglia.milesRequestFailed.error";
	public static final String ERROR_MESSAGE_MILES_REQUEST_SUCCESS = "millemiglia.milesRequestSuccess.error";
	public static final String ERROR_MESSAGE_GENERIC = "millemiglia.generic.error";
	public static final String ERROR_NO_STRONG_USER = "millemiglia.username.error.notstrong";
	public static final String ERROR_USERNAME_USED = "millemiglia.username.error.invalid.alreadyused";
	public static final String ERROR_PWD_NOT_EQUAL_CONF = "specialpage.recuperacredenziali.message.nopasswordsmatching";
	public static final String ERROR_PREF_AND_PHONE = "millemiglia.phone.error.ifprefix.number";
	public static final String ERROR_QUEST_AND_ANS = "millemiglia.phone.error.ifquestion.answer";
	public static final String ERROR_QUEST_ORR_PHONE = "specialpage.millemiglia.security.isMMOneOfRequiredGroup";
	public static final String ERROR_PWD_NOT_STRONG = "millemiglia.password.error.notstrong";
	public static final String ERROR_EMAIL_SA_USED = "millemiglia.email.error.invalid.alreadyused";





	public static final String USERNAME_CODE_MM = "MM.SA.UserNameOCodiceMM";
	public static final String PASSWORD_MM="MM.SA.Password";
	public static final String RECOVERY_CREDENTIALS_MM="MM.SA.RecuperaCredenziali";
	public static final String CODE_PIN_MM="MM.SA.SePossiediIlCodice";
	public static final String CLICK_HERE_MM="MM.SA.CliccaQui";
    public static final String INVALID_CREDENTIALS_MM="MM.SA.CredenzialiNonValide";

}
