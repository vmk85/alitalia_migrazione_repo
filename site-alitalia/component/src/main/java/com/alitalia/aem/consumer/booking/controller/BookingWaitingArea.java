package com.alitalia.aem.consumer.booking.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingWaitingArea extends BookingSessionGenericController{
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private String fromCityName;
	private String fromAirportName;
	private String fromCountryName;
	private String toCountryName;
	private String toAirportName;
	private String toCityName;
	
	private Logger logger = LoggerFactory.getLogger(BookingWaitingArea.class); 

	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			
			initBaseModel(request);
			if (ctx == null) {
				//TODO: Gestire
				return;
			}
			
			fromCityName = ctx.searchElements.get(0).getFrom().getLocalizedCityName();
			fromAirportName = ctx.searchElements.get(0).getFrom().getLocalizedAirportName();
			fromCountryName = ctx.searchElements.get(0).getFrom().getLocalizedCountryName();
			fromAirportName = manageEqualsCity(fromCityName,fromAirportName);
			
			toCityName = ctx.searchElements.get(0).getTo().getLocalizedCityName();
			toAirportName = ctx.searchElements.get(0).getTo().getLocalizedAirportName();
			toCountryName = ctx.searchElements.get(0).getTo().getLocalizedCountryName();
			toAirportName = manageEqualsCity(toCityName,toAirportName);
			
		}catch(Exception e){
			logger.error("Unexpected error: ", e);
			throw e;
		}
		
	}
	

	public String getFromCityName() {
		return fromCityName;
	}

	public String getFromAirportName() {
		return fromAirportName;
	}

	public String getFromCountryName() {
		return fromCountryName;
	}

	public String getToCountryName() {
		return toCountryName;
	}

	public String getToAirportName() {
		return toAirportName;
	}

	public String getToCityName() {
		return toCityName;
	}

	private String manageEqualsCity(String cityName, String airportName) {
		if (cityName.equalsIgnoreCase(airportName)) {
			return "";
		}
		return airportName;
	}
}
