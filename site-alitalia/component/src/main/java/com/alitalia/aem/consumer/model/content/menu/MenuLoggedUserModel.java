package com.alitalia.aem.consumer.model.content.menu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.LabelUrl;
import com.day.cq.wcm.api.Page;

@Model(adaptables={Resource.class})
public class MenuLoggedUserModel {

	@SuppressWarnings("unused")
    private Logger logger = LoggerFactory.getLogger(MenuLoggedUserModel.class);

	@Inject
	private AlitaliaConfigurationHolder configuration;
	
    @Self
    private Resource resource;
    
    private LabelUrl[] menuItems;
    private LabelUrl[] menuItemsMobile;
    
    /**
     * This method is used to get all the first level children of selected page.
     */
    @PostConstruct
    protected void initModel() {
        
    	String[] menuIcons = {"i-profile","i-list","i-mForMM","i-message","i-diamond"};
    	ResourceResolver resolver = resource.getResourceResolver();
    	List<LabelUrl> menuItemList = new ArrayList<LabelUrl>();
    	List<LabelUrl> menuItemMobileList = new ArrayList<LabelUrl>();
    	
    	String personalAreaResourcePath = AlitaliaUtils.findSiteBaseRepositoryPath(resource) + "/" 
    			+ AlitaliaConstants.PERSONAL_AREA_PAGE_NAME;
    	Resource personalAreaResource = resource.getResourceResolver().resolve(personalAreaResourcePath);
    	
    	Page currentPage = AlitaliaUtils.getPage(resource);
    	
    	if (personalAreaResource != null) {
    		Page pathPage = personalAreaResource.adaptTo(Page.class);
        	Iterator<Page> children = pathPage.listChildren();
        	int i = 0;
        	while (children.hasNext() && i < menuIcons.length) {
        		Page menuItemPage = children.next();
        		boolean isActive = false;
        		if (menuItemPage.getPath().equals(currentPage.getPath())) {
        			isActive = true;
        		}
        		boolean httpsEnabled = configuration.getHttpsEnabled();
        		//logger.debug("[MenuLoggedUserModel] httpsEnabled: " + httpsEnabled);
        		String protocol = "https://";
        		if(!httpsEnabled){
        			protocol = "http://";
        		}
        		//logger.debug("[MenuLoggedUserModel] protocol: " + protocol);
        		
        		String urlMap = resolver.map(menuItemPage.getPath());
        		String url = protocol + configuration.getExternalDomain() + urlMap;//menuItemPage.getPath();//urlMap;//menuItemPage.getPath()
        		//logger.debug("[MenuLoggedUserModel] url: " + url);
        		menuItemList.add(new LabelUrl(menuItemPage.getTitle(), url + AlitaliaConstants.HTML_EXT , menuIcons[i], isActive));
        		menuItemMobileList.add(new LabelUrl(menuItemPage.getTitle(),  url + AlitaliaConstants.HTML_EXT, menuIcons[i], isActive));//resolver.map(menuItemPage.getPath())
        		i++;
        	}
        	menuItems = (LabelUrl[]) menuItemList.toArray(new LabelUrl[menuItemList.size()]);
        	menuItemsMobile = (LabelUrl[]) menuItemMobileList.toArray(new LabelUrl[menuItemMobileList.size()]);
    	}
    	
    }

    /**
     * Returns all the first level children of selected page
     * @return The menuItems
     */
    public LabelUrl[] getMenuItems() {
    	return menuItems;
    }
    
    /**
     * Returns all the first level children of selected page
     * @return The menuItems
     */
    public LabelUrl[] getMenuItemsMobile() {
    	return menuItemsMobile;
    }

}
