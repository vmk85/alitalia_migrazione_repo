package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Segment;
import com.day.cq.i18n.I18n;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkinpnrsearch"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinPnrSearchServlet extends GenericCheckinFormValidatorServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private AlitaliaConfigurationHolder configuration;


	@Reference
	private CheckinSession checkinSession;

	private boolean repeate = true;

	//	@Override
	//	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
	//			throws ServletException, IOException {
	//
	//		// content type
	//		response.setContentType("application/json");
	//
	//		// JSON response
	//		TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());
	//
	//		try{
	//			logger.info("Invocazione CheckinPnrSearchServlet...");
	//			//TODO
	//			// prepare resource bundle for i18n
	//			Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
	//			ResourceBundle resourceBundle = request.getResourceBundle(locale);
	//			final I18n i18n = new I18n(resourceBundle);
	//
	//			// invoco il delegate
	//			CheckinPnrSearchRequest serviceRequest = new CheckinPnrSearchRequest(IDFactory.getTid(), IDFactory.getSid(request));
	//			serviceRequest.setPnr(request.getParameter("pnr"));
	//			serviceRequest.setFirstName(request.getParameter("firstName"));
	//			serviceRequest.setLastName(request.getParameter("lastName"));
	//			CheckinPnrSearchResponse serviceResponce = checkinDelegate.retrieveCheckinPnr(serviceRequest);
	//			
	//			PnrList pnrList = serviceResponce.getPnrData();
	//			
	////			List<Pnr> pnrList = serviceResponce.getPnrData().getPnr();
	//			
	//			for (Pnr pnr : pnrList.getPnr()) {
	//				
	//				jsonOutput.key("pnr");
	//				jsonOutput.object();
	//				jsonOutput.key("number").value(pnr.getNumber());
	//				
	//				for (int i = 0; i < pnr.getFlights().size(); i++) {
	//					jsonOutput.key("flights");
	//					jsonOutput.array();
	//					jsonOutput.key("airline").value(pnr.getFlights().get(i).getAirline());
	//					
	//				//inizio oggetto Origin
	//					jsonOutput.key("origin");
	//					jsonOutput.object();
	//					jsonOutput.key("city").value(pnr.getFlights().get(i).getOrigin().getCity());
	//					jsonOutput.key("cityUrl").value(pnr.getFlights().get(i).getOrigin().getCityUrl());
	//					jsonOutput.key("cityCode").value(pnr.getFlights().get(i).getOrigin().getCityCode());
	//					jsonOutput.key("code").value(pnr.getFlights().get(i).getOrigin().getCode());
	//					jsonOutput.key("country").value(pnr.getFlights().get(i).getOrigin().getCountry());
	//					jsonOutput.key("countryCode").value(pnr.getFlights().get(i).getOrigin().getCountryCode());
	//					jsonOutput.key("state").value(pnr.getFlights().get(i).getOrigin().getState());
	//					jsonOutput.key("stateCode").value(pnr.getFlights().get(i).getOrigin().getStateCode());
	//					jsonOutput.key("name").value(pnr.getFlights().get(i).getOrigin().getName());
	//					jsonOutput.key("marketAreaCode").value(pnr.getFlights().get(i).getOrigin().getMarketAreaCode());
	//					jsonOutput.key("checkInEligibility").value(pnr.getFlights().get(i).getOrigin().getCheckInEligibility());
	//					jsonOutput.endObject();
	//				//fine oggetto Origin
	//					
	//				//inizio oggetto Destination
	//					jsonOutput.key("destination");
	//					jsonOutput.object();
	//					jsonOutput.key("city").value(pnr.getFlights().get(i).getDestination().getCity());
	//					jsonOutput.key("cityUrl").value(pnr.getFlights().get(i).getDestination().getCityUrl());
	//					jsonOutput.key("cityCode").value(pnr.getFlights().get(i).getDestination().getCityCode());
	//					jsonOutput.key("code").value(pnr.getFlights().get(i).getDestination().getCode());
	//					jsonOutput.key("country").value(pnr.getFlights().get(i).getDestination().getCountry());
	//					jsonOutput.key("countryCode").value(pnr.getFlights().get(i).getDestination().getCountryCode());
	//					jsonOutput.key("state").value(pnr.getFlights().get(i).getDestination().getState());
	//					jsonOutput.key("stateCode").value(pnr.getFlights().get(i).getDestination().getStateCode());
	//					jsonOutput.key("name").value(pnr.getFlights().get(i).getDestination().getName());
	//					jsonOutput.key("marketAreaCode").value(pnr.getFlights().get(i).getOrigin().getMarketAreaCode());
	//					jsonOutput.key("checkInEligibility").value(pnr.getFlights().get(i).getDestination().getCheckInEligibility());
	//					jsonOutput.endObject();
	//				//fine oggetto Destination
	//					
	//					jsonOutput.key("arrivalDate").value(pnr.getFlights().get(i).getArrivalDate());
	//					jsonOutput.key("departureDate").value(pnr.getFlights().get(i).getDepartureDate());
	//					jsonOutput.key("flightStatus").value(pnr.getFlights().get(i).getFlightStatus());
	//					jsonOutput.key("checkInGate").value(pnr.getFlights().get(i).getCheckInGate());
	//					
	//				//inizio array Passenger	
	//					for (int j = 0; j < pnr.getFlights().get(i).getPassengers().size(); j++) {
	//						jsonOutput.key("passengers");
	//						jsonOutput.array();
	//						jsonOutput.key("passengerID").value(pnr.getFlights().get(i).getPassengers().get(j).getPassengerID());
	//						jsonOutput.key("checkInLimited").value(pnr.getFlights().get(i).getPassengers().get(j).getCheckInLimited());
	//						jsonOutput.key("checkInComplete").value(pnr.getFlights().get(i).getPassengers().get(j).getCheckInComplete());
	//						jsonOutput.key("nome").value(pnr.getFlights().get(i).getPassengers().get(j).getNome());
	//						jsonOutput.key("cognome").value(pnr.getFlights().get(i).getPassengers().get(j).getCognome());
	//						jsonOutput.key("bookingClass").value(pnr.getFlights().get(i).getPassengers().get(j).getBookingClass());
	//						jsonOutput.key("postoAssegnato").value(pnr.getFlights().get(i).getPassengers().get(j).getPostoAssegnato());
	//						jsonOutput.key("numeroPostoAssegnato").value(pnr.getFlights().get(i).getPassengers().get(j).getNumeroPostoAssegnato());
	//						jsonOutput.endArray();
	//					}
	//				//fine array Passenger
	//					
	//					jsonOutput.endArray();
	//				}
	//				
	//				jsonOutput.endObject();
	//			}
	//			// out JSON response
	//			jsonOutput.object();
	//			
	//			//String successPage = configuration.getBookingFlightSelectPage();
	//			String successPage = "/content/alitalia/master-it/it/test-check-in-1.html";
	//			if (successPage != null && !("").equals(successPage)) {
	//				String successUrl = AlitaliaUtils.findExternalRelativeUrlByPath(successPage, request.getResource());
	//				logger.debug("successUrl: " + successUrl);
	//				response.sendRedirect(response.encodeRedirectURL(successUrl));
	//			}
	//		}
	//
	//		catch(Exception e){
	//			logger.error(e.getMessage());
	//			throw new RuntimeException("[CheckinPnrSearchServlet] - Errore durante la ricerca dei dati di viaggio.", e);
	//		}
	//	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		Validator validator = new Validator();

		String pnr = request.getParameter("pnr");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");

		validator.addDirectCondition("pnr", pnr, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("pnr", pnr, 
				CheckinConstants.MESSAGE_ERROR_INVALID_PNR, "isPnrOrTicketNumber");
		
		validator.addDirectCondition("firstName", firstName, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("firstName", firstName, 
				CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");

		validator.addDirectCondition("lastName", lastName, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("lastName", lastName, 
				CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");


		ResultValidation resultValidation = validator.validate();
		return resultValidation;

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
//		logger.info("[CheckinFrequentFlyerSearchServlet - performSubmit] prima di initSession....");
		
		boolean resultSession = initSession(request, configuration, checkinSession);

		if (!resultSession){
			logger.error("Errore durante l'inizializzazione della sessione.");
			throw new Exception("Errore durante l'inizializzazione della sessione.");
		}else{

			CheckinSessionContext ctx = getCheckInSessionContext(request);
			
			if (ctx.pnrListData != null){
				ctx.pnrListData = new ArrayList<>();
			}	

			logger.info("[CheckinPnrSearchServlet - performSubmit] recupero dati dal contesto.");

			try{
				
				if (request.getParameter("pnr").length() == 6){
					
					logger.info("[CheckinPnrSearchServlet] invoco il checkinSession.checkInPnrSearch");
					CheckinPnrSearchResponse result = checkinSession.checkInPnrSearch(request, ctx);
					logger.info("[CheckinPnrSearchServlet] result=" + result);

					try{
						this.repeate = true;

						for (Segment segment : result.getPnrData().getPnr().get(0).getFlights().get(Integer.parseInt(request.getParameter("flightitinerary"))).getSegments()){
							for (Passenger passenger : segment.getPassengers()){
								if (passenger.getCheckInComplete() == true){
									this.repeate = false;
								}
							}
						}
					}catch(Exception e){}


					
					if (result != null) {
						if(result.getPnrData().getError() != null && !result.getPnrData().getError().equals("")){
							managementError(request, response, result.getPnrData().getError());
						}else{

							// Se request format=json allora torno il json
							if (request.getParameter("format") != null && request.getParameter("format").equals("json"))
							{
								TidyJSONWriter json;
								try {

									Gson gson = new Gson();
									String jsonInString = gson.toJson(result);

									json = new TidyJSONWriter(response.getWriter());

									response.setContentType("application/json");

									json.object();
									json.key("data").value(jsonInString);
									json.endObject();

								} catch (IOException e) {
									e.printStackTrace();
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							else {
								//response.sendRedirect(successPage);
								managementSucces(request, response);
							}

						}
					}
					else {
						logger.error("[CheckinPnrSearchServlet] - Errore durante l'invocazione del servizio." );
					}

				}
				
				if (request.getParameter("pnr").length() == 13){
					logger.info("[CheckinPnrSearchServlet] invoco il checkinSession.checkinTicketSearch");
					CheckinTicketSearchResponse result = checkinSession.checkinTicketSearch(request, ctx);
					logger.info("[CheckinPnrSearchServlet] result=" + result);

					try{
						this.repeate = true;

						for (Segment segment : result.getPnrData().getPnr().get(0).getFlights().get(Integer.parseInt(request.getParameter("flightitinerary"))).getSegments()){
							for (Passenger passenger : segment.getPassengers()){
								if (passenger.getCheckInComplete() == true){
									this.repeate = false;
								}
							}
						}
					}catch(Exception e){}

					if (result != null) {
						if(result.getPnrData().getError() != null && !result.getPnrData().getError().equals("")){
							managementError(request, response, result.getPnrData().getError());
						}else{
							//response.sendRedirect(successPage);
							managementSucces(request, response);
						}
					}else {
						logger.error("[CheckinPnrSearchServlet] - Errore durante l'invocazione del servizio." );
					}
				}

		
			}
			catch(Exception e){
				throw new RuntimeException("[CheckinPnrSearchServlet] - Errore durante l'invocazione del servizio.", e);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		Map<String,String> additionalParams = new HashMap<String, String>();
		return additionalParams;
	}
	
	public void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error){
		
		logger.info("[CheckinPnrSearchServlet] [managementError] error =" + error );
		
		Gson gson = new Gson();
		
		com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);
		
		TidyJSONWriter json;
		try {
			final I18n i18n = new I18n(request.getResourceBundle(AlitaliaUtils.findResourceLocale(request.getResource())));
			json = new TidyJSONWriter(response.getWriter());
		
			response.setContentType("application/json");
			String errorMessageFromBL = "";
			switch (errorObj.getErrorMessage()){
				case "Volo decollato":
					errorMessageFromBL = "checkin.flightList.tooLate.label";
					break;
				case "Nessun volo trovato":
					errorMessageFromBL = "checkin.search.input.error";
					break;
				/*case "KOREAN GOVERNMENT -  STATUS NOT REQUIRED":
					errorMessageFromBL = "checkin.search.korean.status.notrequired";
					break;
				case "Checkin online non disponibile per Pnr di gruppo":
					errorMessageFromBL = "checkin.search.notavalaible.forgroups";
					break;
				case "AIRLINE CODE NOT FOUND":
					errorMessageFromBL = "check.search.airlinedcode.notfound";
					break;
				case "!INVALID LINE NUMBER SELECTED":
					errorMessageFromBL = "checkin.search.invalidlineselected";
					break;*/
				default:
					errorMessageFromBL = "checkin.search.generic.error";
					break;
			}
			
			json.object();
			json.key("isError").value(true);
			json.key("errorMessage").value(errorMessageFromBL);
			json.key("sabreStatusCode").value(errorObj.getSabreStatusCode());
			json.key("conversationID").value(errorObj.getConversationID());
			json.endObject();
		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
	}
	
	public void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response){
		
		logger.info("[CheckinPnrSearchServlet] [managementSucces] ...");
		String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCheckinFlightListPage();

		successPage = request.getResourceResolver().map(successPage);

		if (repeate){
			successPage = successPage + "?&repeate=true";
		}


		logger.info("[CheckinPnrSearchServlet] [managementSucces] successPage = " + successPage );
		
		TidyJSONWriter json;
		try {
			
			json = new TidyJSONWriter(response.getWriter());
			response.setContentType("application/json");
			json.object();
			json.key("isError").value(false);
			json.key("successPage").value(successPage);
			json.endObject();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
		
	}

}
