package com.alitalia.aem.consumer.checkin.model;

import java.math.BigDecimal;
import java.util.Arrays;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinSeatMapGeneric extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private CheckinSession checkinSession;
	
	
	private int selectedSeatsNum;
	private boolean seatmapBoxEnabled;
	
	@PostConstruct
	protected void initModel() {
		
		try {
			initBaseModel(request);
			/*Numero di posti acquistati*/
			int seatToBeIssued = getSelectedAncillary(MmbAncillaryTypeEnum.SEAT);
			int upgradeToBeIssued = getSelectedAncillary(MmbAncillaryTypeEnum.UPGRADE);
			selectedSeatsNum = seatToBeIssued + upgradeToBeIssued;
			seatmapBoxEnabled = CheckinUtils.getFlightsList(ctx.selectedRoute).size() != CheckinUtils.obtainBusFlight(ctx.selectedRoute).size();
		} catch (Exception e) {
			logger.error("Unexpected error:", e);
			throw e;
		}
	}
	
	
	private int getSelectedAncillary(MmbAncillaryTypeEnum type){
		int result = 0;
		MmbAncillaryStatusEnum[] statuses = {MmbAncillaryStatusEnum.TO_BE_ISSUED};
		if(ctx.cart != null && !ctx.cart.getAncillaries().isEmpty()){
			result = (int) ctx.cart.getAncillaries().stream().filter(anc 
					-> anc.getAncillaryType().equals(type) 
					&& Arrays.asList(statuses).contains(anc.getAncillaryStatus())
					).count();
		}
		return result ;
	}
	

	public int getSelectedSeatsNum() {
		return selectedSeatsNum;
	}
	
	public boolean isSeatmapBoxEnabled() {
		return seatmapBoxEnabled;
	}

}
