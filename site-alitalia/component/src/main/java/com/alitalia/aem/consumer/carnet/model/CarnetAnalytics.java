package com.alitalia.aem.consumer.carnet.model;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.carnet.analytics.CarnetAnalyticsInfo;
import com.alitalia.aem.consumer.carnet.analytics.CarnetAnalyticsInfoFiller;
import com.alitalia.aem.consumer.carnet.analytics.CarnetAnalyticsInfoToJSObject;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CarnetAnalytics extends GenericCarnetModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	private int step;
	
	private String dlVar;
	
	@PostConstruct
	protected void initModel() throws IOException{
		try {
			super.initBaseModel(request);
			step = computeCarnetStep();
			CarnetAnalyticsInfoFiller infoFiller = new CarnetAnalyticsInfoFiller(ctx, step);
			CarnetAnalyticsInfo info = infoFiller.fillInfo();
			CarnetAnalyticsInfoToJSObject toJsObj = new CarnetAnalyticsInfoToJSObject(info);
			dlVar = toJsObj.toJSObj(true);
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			dlVar = "{};";
		}
	}
	
	private int computeCarnetStep(){
		int step = 0;
		switch(ctx.phase){
		case CHOOSE:
		case PERSONAL_INFO:
			step = 1;
			break;
		case PAYMENT:
			step = 2;
			break;
		case DONE:
			step = 3;
		}
		return step;
	}

	public int getStep() {
		return step;
	}



	public String getDlVar() {
		return dlVar;
	}

}
