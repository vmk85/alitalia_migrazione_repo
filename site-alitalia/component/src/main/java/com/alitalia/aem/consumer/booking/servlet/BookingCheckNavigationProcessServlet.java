package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingPhaseEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.commons.TidyJSONWriter;

@Component(immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "bookingchecknavigationprocessconsumer" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "error.page", description = "Users will be redirected to the specified page after a failed outcome."),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class BookingCheckNavigationProcessServlet extends SlingSafeMethodsServlet {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private BookingSession bookingSession;
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		String genericErrorPagePlain = "#";
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		
		try {
			json.object();
			
			String baseUrl = request.getResource().getResourceResolver().map(
					AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
			
			
			genericErrorPagePlain = baseUrl + configuration.getErrorNavigationPage();
			
			BookingSessionContext ctx = (BookingSessionContext) 
					request.getSession(true).getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
			boolean error = false;
			String page = "";
			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				//throw new IllegalArgumentException("Cannot find booking session context in session.");
				error = true;
				page = configuration.getBookingFailurePage();
			}
			
			if (!error) {
				String destPhase = request.getParameter("destPhase");
				BookingPhaseEnum destinationPhase = null;
				boolean skip = false;
				try {
					destinationPhase = BookingPhaseEnum.fromValue(destPhase);
				} catch (Exception e) {
					logger.info("destinationPhase: {} unknown", destPhase);
					page = "";
					skip = true;
				}
				if (!skip) {
					String residencyValue = (String) request.getParameter("residency");
					Boolean residency = new Boolean(residencyValue);
					String nocache = (String) request.getParameter("nocache");
					page = bookingSession.forwardNavigationProcess(ctx, destinationPhase, residency, nocache);
				}
			}
			String redirect = baseUrl + page;
			if (page == null) {
				json.key("redirect").value(genericErrorPagePlain);
			}
			if (page != null && !("").equals(page)) {
				json.key("redirect").value(redirect);
			}
			json.endObject();
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			if (!response.isCommitted()) {
				try {
					json.key("redirect").value(genericErrorPagePlain);
					json.endObject();
				} catch (JSONException e1) {
					logger.error("Error building JSON", e1);
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
				}
			}
		}
		
	}

}
