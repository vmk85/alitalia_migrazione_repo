package com.alitalia.aem.consumer.myalitalia.servlet;

import com.alitalia.aem.common.messages.home.SendReminderCheckinRequest;
import com.alitalia.aem.common.messages.home.SendReminderCheckinResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.myalitalia.model.MyalitaliaOTPSessionModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.web.component.webcheckin.delegate.WebCheckinDelegate;
import com.alitalia.aem.ws.otp.service.OTPServiceClient;
import com.alitalia.aem.ws.otp.service.xsd1.*;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import javax.servlet.Servlet;
import javax.xml.bind.JAXBElement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = { "sendMAOTPServlet", "sendMAOTPServletEmail" }),
        @Property(name = "sling.servlet.methods", value = { "POST" }),
        @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) ,
        @Property(name = "sling.servlet.extensions", value = { "json" })
})
//@SuppressWarnings("serial")
public class SendMAOTPServlet extends GenericMyAlitaliaFormValidatorServlet {

    @Reference
    private OTPServiceClient wsClient;

    @Reference
    private AlitaliaConfigurationHolder alitaliaConfigurationHolder;

    @Reference
    private WebCheckinDelegate checkInDelegate;

    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

        logger.debug("[SendMAOTPServlet] performSubmit");
        String selectorString = request.getRequestPathInfo().getSelectorString();

        final I18n i18n = new I18n(request.getResourceBundle(
                AlitaliaUtils.findResourceLocale(request.getResource())));

        String data = (request.getSession().getAttribute("TelephoneNumber") != null ? request.getSession().getAttribute("TelephoneNumber").toString() : null);
        String scope = request.getParameter("scope");
        MyalitaliaOTPSessionModel otpSession;

        if(request.getSession().getAttribute("OTPSession") != null && request.getSession().getAttribute("OTPSession") instanceof MyalitaliaOTPSessionModel) {
            otpSession = (MyalitaliaOTPSessionModel) request.getSession().getAttribute("OTPSession");
        } else {
            otpSession = new MyalitaliaOTPSessionModel();
        }

        String action = request.getParameter("action");

        if(action != null && action.equals("clean")) otpSession.setAttempt(0);

        TidyJSONWriter json = new TidyJSONWriter(response.getWriter());

        json.object();

        response.setContentType("application/json");



        try {
            switch (selectorString){

                case "sendMAOTPServlet":
                    if(scope.equals("getOTP")) {

                        logger.info("[SendMAOTPServlet] scope is get OTP code");

                        if (data != null) {

                            SendOTPWithAppResponse sendOTPResponse = sendOTP(data, i18n.get("myalitalia.otp.smsdescription"));

                            String otp = sendOTPResponse.getOTP().getValue();
                            String sentDate = sendOTPResponse.getSentDate().getValue();
                            String expireDate = sendOTPResponse.getExpiryDate().getValue();

                            logger.info("[SendMAOTPServlet] OTP request sent, response = OTP:" + otp + ", sent date:" + sentDate + ", expiry date:" + expireDate);

                            json.key("sentDate").value(sentDate);
                            json.key("exipiryDate").value(expireDate);

                            json.key("feedback").value(i18n.get("myalitalia.otp.sent"));

                            Date OTPExp = new Date();
                            OTPExp.setSeconds(OTPExp.getSeconds()+30);

//                            request.getSession().setAttribute("OTPCode", otp);
//                            request.getSession().setAttribute("OTPExpire", OTPExp);
//                            request.getSession().setAttribute("numberOTP", data);

                            otpSession.setCode(otp);
                            otpSession.setExpire(OTPExp);
                            otpSession.setData(data);
                            otpSession.setType("SMS");

                            request.getSession().setAttribute("OTPSession", otpSession);

                        } else {
                            json.key("isError").value(true);
                            json.key("errorCode").value(504);
                            json.key("errorDetails").value("missing Phone number");
                            json.key("feedback").value(i18n.get("myalitalia.otpError.missingField"));

                            logger.error("[SendMAOTPServlet] OTP request fail, missing phone number");
                        }
                    } else if(scope.equals("checkOTP")) {

                        /*
                            LISTA ERRORI:
                                0 - OK
                                5000 - Internal Server Error
                                5002 - OTP errato (EMAIL)
                                5003 - Troppi tentativi fatti (SMS)
                                5004 - OTP errato, ne viene inviato un altro (SMS)
                                5005 - OTP scaduto (EMAIL)
                         */

                        logger.info("[SendMAOTPServlet] scope is check OTP code");

                        if(data != null) {

                            if (otpSession.getCode() != null) {

                                logger.info("[SendMAOTPServlet] checking OTP...");

                                if (otpSession.getCode().equals(data) && otpSession.getExpire() != null) {

                                    if (isExpiredOTP(otpSession.getExpire())) {

                                        logger.error("[SendMAOTPServlet] OTP expired");

                                        int attempt = otpSession.getAttempt();

                                        attempt++;

                                        logger.info("[SendMAOTPServlet] attempt n. " + attempt);

                                        if(attempt < 3) {

                                            SendOTPWithAppResponse sendOTPResponse = sendOTP(otpSession.getData(), i18n.get("myalitalia.otp.smsdescription"));

                                            Date OTPExp = new Date();
                                            OTPExp.setSeconds(OTPExp.getSeconds() + 30);
                                            if (sendOTPResponse.getOTP() != null && sendOTPResponse.getExpiryDate() != null && sendOTPResponse.getSentDate() != null) {

                                                logger.info("[SendMAOTPServlet] provided new OTP = " + sendOTPResponse.getOTP().getValue());

                                                json.key("isError").value(true);
                                                json.key("errorCode").value(5004);
                                                json.key("errorDetails").value("OTP expired, new OTP sent");
                                                json.key("feedback").value(i18n.get("myalitalia.checkOTP.newOTPSent"));
                                                json.key("attempt").value(attempt);
                                                json.key("expiry").value(OTPExp);

                                                otpSession.setCode(sendOTPResponse.getOTP().getValue());
                                                otpSession.setExpire(OTPExp);
                                                otpSession.setAttempt(attempt);

                                                request.getSession().setAttribute("OTPSession", otpSession);

                                                logger.info("[SendMAOTPServlet] provided new OTP = " + sendOTPResponse.getOTP().getValue() + ", expiry = " + otpSession.getExpire());

                                                logger.info("[SendMAOTPServlet] OTP Session info = {OTP: " + otpSession.getCode() + ", expiry: " + otpSession.getExpire() + ", attempt: " + otpSession.getAttempt() + ", type: " + otpSession.getType() + ", data: " + otpSession.getData() + "}");
                                            }
                                        } else if(attempt >= 3) {

                                            logger.error("[SendMAOTPServlet] too many attempts");

                                            json.key("isError").value(true);
                                            json.key("errorCode").value(5003);
                                            json.key("errorDetails").value("Too many attempts");
                                            json.key("feedback").value(i18n.get("myalitalia.checkOTP.tooAttempts"));
                                            json.key("attempt").value(attempt);

                                            request.getSession().removeAttribute("OTPSession");

                                        }
//                                        json.key("isError").value(true);
//                                        json.key("errorCode").value(5005);
//                                        json.key("errorDetails").value(i18n.get("OTP Expired"));
//                                        json.key("feedback").value(i18n.get("myalitalia.OTPExpired"));
////                                        request.getSession().removeAttribute("OTPCode");
////                                        request.getSession().removeAttribute("OTPExpire");
//
//                                        otpSession.setCode(null);
//                                        otpSession.setExpire(null);
//
//                                        request.getSession().setAttribute("OTPSession", otpSession);
                                    } else {

                                        logger.info("[SendMAOTPServlet] OTP match!");

                                        json.key("isError").value(false);
                                        json.key("errorCode").value(0);

                                        otpSession.setVerified(true);

                                        request.getSession().setAttribute("OTPSession", otpSession);


                                    }
                                } else {

                                    if(otpSession.getType().equals("SMS") && otpSession.getData() != null) {

                                        logger.error("[SendMAOTPServlet] OTP doesn't match, OTP provided =" + request.getSession().getAttribute("OTPCode") + ", inserted OTP =" + data);

                                        int attempt = otpSession.getAttempt();
                                        if(attempt >= 0) {
                                            attempt++;

                                            logger.info("[SendMAOTPServlet] attempt n. " + attempt);

                                            if (attempt < 3) {

                                                Date OTPExp = new Date();
                                                OTPExp.setSeconds(OTPExp.getSeconds() + 30);

                                                logger.info("[SendMAOTPServlet] sending new OTP");

                                                SendOTPWithAppResponse sendOTPResponse = this.sendOTP(otpSession.getData(), i18n.get("myalitalia.otp.smsdescription"));

                                                if (sendOTPResponse.getOTP() != null && sendOTPResponse.getExpiryDate() != null && sendOTPResponse.getSentDate() != null) {

                                                    logger.info("[SendMAOTPServlet] provided new OTP = " + sendOTPResponse.getOTP().getValue());

                                                    json.key("isError").value(true);
                                                    json.key("errorCode").value(5004);
                                                    json.key("errorDetails").value("OTP doesn't match, new OTP sent");
                                                    json.key("feedback").value(i18n.get("myalitalia.checkOTP.newOTPSent"));
                                                    json.key("attempt").value(attempt);
                                                    json.key("expiry").value(OTPExp);

                                                    otpSession.setCode(sendOTPResponse.getOTP().getValue());
                                                    otpSession.setExpire(OTPExp);
                                                    otpSession.setAttempt(attempt);

                                                    request.getSession().setAttribute("OTPSession", otpSession);

                                                    logger.info("[SendMAOTPServlet] provided new OTP = " + sendOTPResponse.getOTP().getValue() + ", expiry = " + otpSession.getExpire());

                                                    logger.info("[SendMAOTPServlet] OTP Session info = {OTP: " + otpSession.getCode() + ", expiry: " + otpSession.getExpire() + ", attempt: " + otpSession.getAttempt() + ", type: " + otpSession.getType() + ", data: " + otpSession.getData() + "}");
                                                }

                                            } else if (attempt >= 3) {

                                                json.key("isError").value(true);
                                                json.key("errorCode").value(5003);
                                                json.key("errorDetails").value("Too many attempts");
                                                json.key("feedback").value(i18n.get("myalitalia.checkOTP.tooAttempts"));
                                                json.key("attempt").value(attempt);

                                                request.getSession().removeAttribute("OTPSession");
                                            }
                                        } else {

                                            logger.error("[SendMAOTPServlet] attempt not set");

                                            throw new Exception();
                                        }

                                    } else if(otpSession.getType().equals("EMAIL")) {

                                        if(isExpiredOTP(otpSession.getExpire())) {

                                            logger.error("[SendMAOTPServlet] OTP expired");

                                            json.key("isError").value(true);
                                            json.key("errorCode").value(5005);
                                            json.key("errorDetails").value(i18n.get("OTP Expired"));
                                            json.key("feedback").value(i18n.get("myalitalia.OTPExpired"));

                                            otpSession.setCode(null);
                                            otpSession.setExpire(null);

                                            request.getSession().setAttribute("OTPSession", otpSession);
                                        } else {

                                            logger.info("[SendMAOTPServlet] wrong otp");

                                            json.key("isError").value(true);
                                            json.key("errorCode").value(5002);
                                            json.key("errorDetails").value("Wrong OTP");
                                            json.key("feedback").value(i18n.get("myalitalia.checkOTP.wrongOTP"));
                                        }
                                    }else {

                                        logger.error("[SendMAOTPServlet] OTP doesn't match, OTP provided =" + request.getSession().getAttribute("OTPCode") + ", inserted OTP =" + data);

                                        json.key("isError").value(true);
                                        json.key("errorCode").value(5002);
                                        json.key("errorDetails").value("OTP not Match");
                                        json.key("feedback").value(i18n.get("myalitalia.checkOTP.notMatch"));
                                    }
                                }
                            } else {

                                logger.error("[SendMAOTPServlet] no OTP in session");

                                json.key("isError").value(true);
                                json.key("errorCode").value(5000);
                                json.key("errorDetails").value("no OTP in session");
                                json.key("feedback").value(i18n.get("myalitalia.checkOTP.noOTPsession"));
                            }
                        } else {

                            logger.error("[SendMAOTPServlet] missing OTP");

                            json.key("isError").value(true);
                            json.key("errorCode").value(504);
                            json.key("errorDetails").value("missing OTP code");
                            json.key("feedback").value(i18n.get("myalitalia.checkOTP.missingOTP"));
                        }
                    } else {

                        logger.error("[SendMAOTPServlet] missing scope");

                        json.key("isError").value(true);
                        json.key("errorCode").value(504);
                        json.key("errorDetails").value("missing scope");
                        json.key("feedback").value(i18n.get("myalitalia.checkOTP.missingField"));
                    }
                    break;
                case "sendMAOTPServletEmail":

                    String email = request.getParameter("data");
                    GetOTPRequest getOTPRequest = new GetOTPRequest();
                    try {
                        com.alitalia.aem.ws.otp.service.xsd1.ObjectFactory objectFactory = new ObjectFactory();
                        JAXBElement<String> EmailAddress =  objectFactory.createGetOTPRequestEmailAddress(email);

                        getOTPRequest.setEmailAddress(EmailAddress);
                        GetOTPResponse getOTPResponse = wsClient.GetOTP(getOTPRequest);

                        String otp = null ,expDate = null;

                        if(getOTPResponse != null){
                            if(getOTPResponse.getOTP() != null){
                                otp = getOTPResponse.getOTP().getValue();
                            } else {
                                logger.error("[SendMAOTPServlet] Nessun OTP ricevuto");
                                json.key("isError").value(true);
                                json.key("errorMessage").value("Nessun OTP ricevuto");
                            }
                            if(getOTPResponse.getExpiryDate() != null){
                                expDate = getOTPResponse.getExpiryDate().getValue();
                            } else {
                                logger.error("[SendMAOTPServlet] Nessuna ExpiryDat ricevuto");
                                json.key("isError").value(true);
                                json.key("errorMessage").value("Nessuna ExpiryDat ricevuto");
                            }
                        } else {
                            logger.error("[SendMAOTPServlet] getOTPResponse vuota ");
                            json.key("isError").value(true);
                            json.key("errorMessage").value("getOTPResponse vuota");
                        }

                        if (otp != null && expDate != null){

                            logger.info("[SendMAOTPServlet] OTP ricevuto:" + otp);

                            Date OTPExp = new Date();
                            OTPExp.setSeconds(OTPExp.getSeconds()+30);

//                            request.getSession().setAttribute("OTPCode", otp);
//                            request.getSession().setAttribute("OTPExpire", OTPExp);
                            otpSession.setCode(otp);
                            otpSession.setExpire(OTPExp);
                            otpSession.setType("EMAIL");

                            request.getSession().setAttribute("OTPSession", otpSession);

                            SendReminderCheckinResponse sendReminderCheckinResponse = sendEmail(email,otp);

                            if (sendReminderCheckinResponse != null){
                                json.key("isError").value(!sendReminderCheckinResponse.getSendReminderResult());
                                json.key("errorMessage").value("");
                            }else {
                                json.key("isError").value(true);
                                json.key("errorMessage").value("email non inviata");
                            }

                        }
                    } catch (Exception e){
                        logger.error("[SendMAOTPServlet] Errore send EmailOTP " + e);
                        json.key("isError").value(true);
                        json.key("errorMessage").value("Errore send EmailOTP");
                    }

                    break;
            }
        } catch (Exception e) {
            json.key("isError").value(true);
            json.key("errorCode").value(500);
            json.key("errorDetails").value((e.toString() != null ? e.toString() : "ERRORRRSSSSSSSS"));
            json.key("feedback").value(i18n.get("myalitalia.otp.serverError"));

            logger.error("[SendMAOTPServlet] OTP request fail for Exception \n"  + e);
            e.printStackTrace();

        } finally {

            logger.info("[SendMAOTPServlet] finish");

            json.endObject();
        }

    }

    private SendReminderCheckinResponse sendEmail(String email, String otp) {

        SendReminderCheckinRequest sendReminderCheckinRequest = setRequestMail(email,otp);
        SendReminderCheckinResponse sendReminderCheckinResponse = checkInDelegate.sendReminder(sendReminderCheckinRequest);

        return sendReminderCheckinResponse;
    }

    private SendReminderCheckinRequest setRequestMail(String mailAlitaliaUser, String otp) {

        SendReminderCheckinRequest sendReminderCheckinRequest = new SendReminderCheckinRequest();
        sendReminderCheckinRequest.setAttachment(null);
        sendReminderCheckinRequest.setAttachmentName(null);
        sendReminderCheckinRequest.setEmailBody("Email Per cambio password, otp: " + otp);
        sendReminderCheckinRequest.setEmailSubject("Reset Password");
        List<String> mails = new ArrayList();
        mails.add(mailAlitaliaUser);
        sendReminderCheckinRequest.setReceiverMails(mails);
        sendReminderCheckinRequest.setTid(IDFactory.getTid());
        sendReminderCheckinRequest.setSid(IDFactory.getSid());

        return sendReminderCheckinRequest;

    }

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        logger.debug("[RecuperoTelefonoServlet] validateForm");
        Validator validator = new Validator();
        ResultValidation resultValidation = new ResultValidation();
        resultValidation.setResult(true);
        return resultValidation;
    }

    private SendOTPWithAppResponse sendOTP(String number, String text) {

        OTPServiceClient otpServiceClient = wsClient;

        SendOTPWithAppRequest sendOTPRequest = new SendOTPWithAppRequest();

        sendOTPRequest.setMobileNumber(number);
        sendOTPRequest.setTextMessage(text);
        sendOTPRequest.setApplicationName("Alitalia");

        logger.info("[SendMAOTPServlet] Sending OTP request for phone =" + number);

        return otpServiceClient.sendOTPWithApp(sendOTPRequest);
    }

    private boolean isExpiredOTP(Date expireDate) {

        try {
            Date current = new Date();

            return current.after(expireDate);
        } catch (Exception e) {return true;}
    }


}