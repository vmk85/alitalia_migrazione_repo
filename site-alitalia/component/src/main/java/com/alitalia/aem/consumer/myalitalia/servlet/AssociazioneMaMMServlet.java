/**
 * Servlet per associazione profilo MyAlitalia-MilleMiglia *
 * */

package com.alitalia.aem.consumer.myalitalia.servlet;

import com.day.cq.commons.TidyJSONWriter;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;


@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = {"associazioneMaMM", "disassociazioneMaMM"}),
        @Property(name = "sling.servlet.methods", value = {"POST"}),
        @Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
        @Property(name = "sling.servlet.extensions", value = {"json"})
})
public class AssociazioneMaMMServlet extends SlingAllMethodsServlet {

    private Logger logger = LoggerFactory.getLogger(getClass());


    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {

        String selectorString = request.getRequestPathInfo().getSelectorString();

        switch (selectorString) {
            case "associazioneMaMM":
                associaMmToMa(request, response);
                break;
            case "disassociazioneMaMM":
                disassociaMmToMa(request, response);
                break;
        }
    }

    private void disassociaMmToMa(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        String url = request.getParameter("url");
        String action = request.getParameter("action");
        String mm_code = request.getParameter("mm_code");
        boolean isError = false;
        try {
            request.getSession().setAttribute("myalitalia_associa_MM_callbackurl", url);
            request.getSession().setAttribute("myalitalia_associa_MM_action", action);
            request.getSession().setAttribute("myalitalia_associa_MM_code", mm_code);
        } catch (Exception e) {
            logger.error("Errore salvataggio in sessione url per associazione MA-MM" + e.getStackTrace());
            isError = true;
        }

        response.setContentType("application/json");
        TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
        try {
            json.object();
            json.key("url");
            json.value(url);
            json.key("isError");
            json.value(isError);
            json.key("action");
            json.value(action);
            json.endObject();
        } catch (Exception e) {
            logger.error("Unexected error generating JSON response.", e);
        }
    }

    private void associaMmToMa(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        String url = request.getParameter("url");
        String action = request.getParameter("action");
        boolean isError = false;
        try {
            request.getSession().setAttribute("myalitalia_associa_MM_callbackurl", url);
            request.getSession().setAttribute("myalitalia_associa_MM_action", action);
        } catch (Exception e) {
            logger.error("Errore salvataggio in sessione url per associazione MA-MM" + e.getStackTrace());
            isError = true;
        }

        response.setContentType("application/json");
        TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
        try {
            json.object();
            json.key("url");
            json.value(url);
            json.key("isError");
            json.value(isError);
            json.key("action");
            json.value(action);
            json.endObject();
        } catch (Exception e) {
            logger.error("Unexected error generating JSON response.", e);
        }
    }
}