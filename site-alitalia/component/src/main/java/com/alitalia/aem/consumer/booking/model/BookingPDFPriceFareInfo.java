package com.alitalia.aem.consumer.booking.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.render.GenericPriceRender;
import com.alitalia.aem.consumer.booking.render.RegoleTariffarieTrattaRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.i18n.I18n;

public class BookingPDFPriceFareInfo {
	private String[][] priceInfo;
	private String[][][] fareRulesInfo;
	
	private GenericPriceRender ecouponPrice;
	private GenericPriceRender cashAndMilesPrice;
	private GenericPriceRender insurancePrice;
	private GenericPriceRender price;
	private BigDecimal ccFeePrice;
	private String brandCodeToMapOnMiniFare;
	private String miniFareCode;

	List<RegoleTariffarieTrattaRender> fareRules;
	
	private Logger logger = LoggerFactory.getLogger(BookingPDFPriceFareInfo.class);
	private boolean showExtraCharge;
	
	
	public BookingPDFPriceFareInfo(BookingSessionContext ctx, I18n i18n, List<RegoleTariffarieTrattaRender> fareRules
			, String bagaglioInStivaLightValue, BigDecimal ccFeePrice, String brandCodeToMapOnMiniFare, String miniFareCode){
		this.brandCodeToMapOnMiniFare = brandCodeToMapOnMiniFare;
		this.miniFareCode = miniFareCode;
		this.fareRules = fareRules;
		this.ccFeePrice = ccFeePrice;
		priceInfo = computePriceInfo(ctx,i18n);
		fareRulesInfo = computeFareRulesInfo(ctx,bagaglioInStivaLightValue,i18n);
	} 

	public String[][] getPriceInfo() {
		return priceInfo;
	}

	public String[][][] getFareRulesInfo() {
		return fareRulesInfo;
	}
	
	private String[][][] computeFareRulesInfo(BookingSessionContext ctx, String bagaglioInStivaLightValue, I18n i18n) {
		/*
		 * 	{
				{ "REGOLE TARIFFARIE" },
				{ "REGOLE TARIFFARIE PER IL VOLO DI ANDATA", "Tariffa",
					"Economy class", "EUR 511.20" },
				{ "REGOLE TARIFFARIE PER IL VOLO DI RITORNO", "Tariffa",
					"Business", "EUR 1532.60" },
				{ "REGOLE TARIFFARIE PER IL VOLO DI RITORNO1", "Tariffa",
						"Business", "EUR 999" },
			}, { 
				{ "Bagaglio a mano", "V" },
				{ "Bagaglio in stiva", "1 x 23kg" },
				{ "Scelta posto", "V" },
				{ "Chash & Miles", "4230 Miglia" },
				{ "Accredito miglia", "V" }
			}, { 
				{ "Bagaglio a mano", "V" },
				{ "Bagaglio in stiva", "1 x 23kg" },
				{ "Scelta posto", "X" },
				{ "Chash & Miles", "6230 Miglia" },
			}, {
				{ "Bagaglio a mano", "V" },
				{ "Bagaglio in stiva", "1 x 23kg" },
				{ "Scelta posto", "X" },
				{ "Chash & Miles", "6230 Miglia" },
				{ "Bagaglio a mano", "V" },
				{ "Bagaglio in stiva", "1 x 23kg" },
				{ "Scelta posto", "X" },
				{ "Chash & Miles", "6230 Miglia" },
			}
		 */
		
		String[] fareForSlice = new String[fareRules.size()];
		if(ctx.searchKind != BookingSearchKindEnum.MULTILEG) {
			if (ctx.flightSelections != null) {
				for (int i = 0; i < ctx.flightSelections.length; i++ ) {
					if (ctx.flightSelections[i] != null) {
						BigDecimal grossFare = obtainGrossFareForSelectedFlight(ctx.flightSelections[i]);
						GenericPriceRender priceRender = new GenericPriceRender(grossFare, ctx.currency, ctx);
						if (i<fareForSlice.length) {
						fareForSlice[i] = priceRender.getFare();
					}
						
				}
			}
		}
		}
		
		int sliceIndex = 0;
		List<String[]> titlesArray = new ArrayList<String[]>();
		List<String[][]> rulesForSliceArray = new ArrayList<String[][]>();
		titlesArray.add(new String[] {i18n.get("booking.carrello.regoleTariffare.label").toUpperCase()});
		for (RegoleTariffarieTrattaRender fareRule : fareRules) {
			String sliceTitle = i18n.get("booking.carrello.regoleTariffariePerVolo.label");
			if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
				sliceTitle += " " + obtainTitleBySliceIndex(sliceIndex,i18n);
			} else {
				if (sliceIndex == 0) {
					sliceTitle += " " + i18n.get("booking.carrello.andata.label");
				} else {
					sliceTitle += " " + i18n.get("booking.carrello.ritorno.label");
				}
			}
			String farePrice = "";
			if (!ctx.isCarnetProcess) {
				farePrice = ctx.currency + " " + fareForSlice[sliceIndex];
			}
			String[] title = {sliceTitle
					, i18n.get("booking.carrello.tariffa.label")
					, fareRule.getBrandName()
					, farePrice};
			titlesArray.add(title);
			//${fareRules.fareRulesFirstColumn[firstColumn] == fareRules.placeHolderStivaLight ? inheritedPageProperties.bagaglioStivaLightLabel : fareRules.fareRulesFirstColumn[firstColumn] @ context = 'html'}
			List<String[]> arrayFareRules = new ArrayList<String[]>();
			for (String rule : fareRule.getFareRulesFirstColumn().keySet()) {
				String[] ruleElem = new String[2];
//				if (fareRule.getFareRulesFirstColumn().get(rule).equals(fareRule.getPlaceHolderStivaLight())) {
//					ruleElem[0] = i18n.get(rule);
//					ruleElem[1] = bagaglioInStivaLightValue;
//				} else {
					ruleElem[0] = i18n.get(rule);
					ruleElem[1] = fareRule.getFareRulesFirstColumn().get(rule);
//				}
				arrayFareRules.add(ruleElem);
			}
			for (String rule : fareRule.getFareRulesSecondColumn().keySet()) {
				String[] ruleElem = new String[2];
//				if (fareRule.getFareRulesSecondColumn().get(rule).equals(fareRule.getPlaceHolderStivaLight())) {
//					ruleElem[0] = i18n.get(rule);
//					ruleElem[1] = bagaglioInStivaLightValue;
//				} else {
					ruleElem[0] = i18n.get(rule);
					ruleElem[1] = fareRule.getFareRulesSecondColumn().get(rule);
//				}
				arrayFareRules.add(ruleElem);
			}

			rulesForSliceArray.add((String[][])arrayFareRules.toArray(new String[0][0]));
			
			sliceIndex++;
		}
		
		String[][][] returnArray = {
				(String[][]) titlesArray.toArray(new String[0][0])
		};
		
		String[][][] returnFareRulseList = (String[][][]) rulesForSliceArray.toArray(new String[0][0][0]);
		return ArrayUtils.addAll(returnArray, returnFareRulseList);
	}
	
	private BigDecimal obtainGrossFareForSelectedFlight(FlightSelection flightSelections) {
		BrandData selectedBrand = AlitaliaUtils.getBrandInfo(
				flightSelections.getFlightData().getBrands()
				, flightSelections.getSelectedBrandCode()
				, brandCodeToMapOnMiniFare
				, miniFareCode);
		if (selectedBrand != null) { 
			return selectedBrand.getGrossFare();
		} else {
			return new BigDecimal(0);
		}
	}
	
	
	private String[][] computePriceInfo(BookingSessionContext ctx, I18n i18n){
		/* Genera un arry in questo formato
		 ** { "2 Tariffa Adulti:" , "EUR 4513.00" },
		 	{ "1 Tariffa Giovani:" , "EUR 4513.00" },
			{ "Tasse:", "EUR 541.12" },
			{ "Supplementi:", "EUR 1296.00" },
			{ "Hai pagato con visa", "**** **** **** 1234" },
			{ "Totale pagato:", "EUR 9765.09" }
		 */
		List<String[]> priceInfoList = new ArrayList<String[]>();
		setPricesForSelectedFlights(ctx, ccFeePrice);

		int totalPax = ctx.searchPassengersNumber.getNumAdults() + 
				ctx.searchPassengersNumber.getNumYoung() + 
				ctx.searchPassengersNumber.getNumChildren() + 
				ctx.searchPassengersNumber.getNumInfants();
		
		String adultInfo = "";
		if (ctx.searchPassengersNumber.getNumAdults() == 0) {
			adultInfo= Integer.toString(ctx.searchPassengersNumber.getNumYoung())
					+ " " + i18n.get("booking.carrello.tariffaGiovani.label") + ":";
		} else {
			adultInfo = Integer.toString(ctx.searchPassengersNumber.getNumAdults())
					+ " " + i18n.get("booking.carrello.tariffaAdulti.label") + ":";
			
		}
		String voliTotali = i18n.get("carnet.carrello.voli.label");
		if (ctx.totalSlices == 1) {
			voliTotali = i18n.get("carnet.carrello.volo.label");
		}
		
		String adultFareInfo = "";
		if (!ctx.isCarnetProcess) {
			adultFareInfo = ctx.currency + " " + price.getFare();
		} else {
			adultFareInfo = ctx.totalSlices + " " + voliTotali;
		}
		priceInfoList.add(new String[] {adultInfo, adultFareInfo});
		
		String childInfo = "";
		if (ctx.searchPassengersNumber.getNumChildren() > 0) {
			childInfo = Integer.toString(ctx.searchPassengersNumber.getNumChildren())
					+ " " + i18n.get("booking.carrello.tariffaBambini.label") + ":";
			
			String childFareInfo = "";
			if (!ctx.isCarnetProcess) {
				childFareInfo = ctx.currency + " " + price.getFareChildren();
			} else {
				childFareInfo = ctx.totalSlices + " " + voliTotali;
			}
			
			priceInfoList.add(new String[] {childInfo, childFareInfo});
		}
		
		String infantInfo = "";
		if (ctx.searchPassengersNumber.getNumChildren() > 0) {
			infantInfo = Integer.toString(ctx.searchPassengersNumber.getNumInfants())
					+ " " + i18n.get("booking.carrello.tariffaNeonati.label") + ":";
			
			String infantFareInfo = "";
			if (!ctx.isCarnetProcess) {
				infantFareInfo = ctx.currency + " " + price.getFareInfant();
			} else {
				infantFareInfo = ctx.totalSlices + " " + voliTotali;
			}
			
			priceInfoList.add(new String[] {infantInfo, infantFareInfo});
		}
		
		String taxInfo = "";
		String taxFareInfo = "";
		if (!ctx.isCarnetProcess) {
			taxInfo = i18n.get("booking.carrello.tasse.label") + ":";
			taxFareInfo = ctx.currency + " " + price.getTaxes();
		} else {
			int residuoCarnet = 0;
			if (ctx.updateCarnetSuccess){
				residuoCarnet = ctx.infoCarnet.getResidualRoutes();
			} else {
				residuoCarnet = ctx.infoCarnet.getResidualRoutes() - (ctx.totalSlices * totalPax);
			}
			taxInfo = i18n.get("carnet.carrello.residuo.label") + ":";
			String voli = i18n.get("carnet.carrello.voli.label");
			if (residuoCarnet == 1) {
				voli = i18n.get("carnet.carrello.volo.label");
			}
			taxFareInfo = "" + residuoCarnet + " " + voli;
		}
		
		priceInfoList.add(new String[] {taxInfo, taxFareInfo});
		
		if (!ctx.isCarnetProcess) {
			String extraInfo = "";
			extraInfo = i18n.get("booking.carrello.supplementi.label") + ":";
			priceInfoList.add(new String[] {extraInfo, ctx.currency + " " + price.getExtra()});
		}
		
		if (!ctx.isCarnetProcess && ctx.ccFeeApplied) {
			String ccFeeInfo = "";
			ccFeeInfo = i18n.get("booking.carrello.ccFee.label") + ":";
			GenericPriceRender ccFeePriceRender = new GenericPriceRender(ccFeePrice, ctx.currency, ctx);
			priceInfoList.add(new String[] {ccFeeInfo, ctx.currency + " " + ccFeePriceRender.getFare()});
		}
		
		if (showExtraCharge && !ctx.isCarnetProcess) {
			String extraChargeInfo = "";
			extraChargeInfo = i18n.get("booking.carrello.servizioVendita.label") + ":";
			priceInfoList.add(new String[] {extraChargeInfo, ctx.currency + " " + price.getExtraCharge()});
		}
		
		if (ecouponPrice != null) {
			String couponInfo = "";
			couponInfo = i18n.get("booking.carrello.coupon.label") + ":";
			priceInfoList.add(new String[] {couponInfo, "- " + ctx.currency + " " + ecouponPrice.getFare()});
		}
		
		if (cashAndMilesPrice != null) {
			String cashMilesInfo = "";
			cashMilesInfo = i18n.get("booking.carrello.cashMiles.label") + ":";
			priceInfoList.add(new String[] {cashMilesInfo, "- " + ctx.currency + " " + cashAndMilesPrice.getFare()});
		}
		
		if (insurancePrice != null) {
			String insurancePriceInfo = "";
			insurancePriceInfo = i18n.get("booking.carrello.assicurazione.label") + ":";
			priceInfoList.add(new String[] {insurancePriceInfo, ctx.currency + " " + insurancePrice.getFare()});
		}
		
		String formattedCardNumber = "";
		String methodPayment = "";
		if (!ctx.isCarnetProcess && ctx.grossAmount!=null && ctx.grossAmount.equals(new BigDecimal(0))) {
			if (ctx.paymentDataForPDF.getType() == PaymentTypeEnum.CREDIT_CARD) {
				PaymentProviderCreditCardData provider = (PaymentProviderCreditCardData) ctx.paymentDataForPDF.getProvider();
				String cardNumber = provider.getCreditCardNumber();
				int starSize = cardNumber.length() - 4;
				formattedCardNumber = cardNumber.substring(0, starSize).replaceAll("[0-9]", "*");
				formattedCardNumber += cardNumber.substring(starSize);
				methodPayment = provider.getType().value();
			} else {
				methodPayment = ctx.paymentDataForPDF.getType().value().toLowerCase();
			}
		}
		
		String paymentInfo = "";
		if (!ctx.isCarnetProcess) {
			paymentInfo = i18n.get("booking.carrello.haiPagatoCon.label") + " " + methodPayment;
		} else {
			paymentInfo = i18n.get("booking.carnet.HaiAcquistato.label", "", ctx.infoCarnet.getTotalRoutes());
			formattedCardNumber = "";
		}
		priceInfoList.add(new String[] {paymentInfo, formattedCardNumber});
		
		String totalPriceInfo = "";
		String totalFarePriceInfo = "";
		if (!ctx.isCarnetProcess) {
			totalPriceInfo = i18n.get("booking.carrello.prezzoTotale.label") + ":";
			totalFarePriceInfo = ctx.currency + " " + price.getTotalPrice();
		} else {
			totalPriceInfo = i18n.get("carnet.riepilogoAcquisto.codiceCarnet.label") + ":";
			totalFarePriceInfo = ctx.infoCarnet.getCarnetCode();
		}
		priceInfoList.add(new String[] {totalPriceInfo, totalFarePriceInfo});
		
		return (String[][]) priceInfoList.toArray(new String[0][0]);
	}
	
	
	private void setPricesForSelectedFlights(BookingSessionContext ctx, BigDecimal ccFeePrice) {

		//Obtaining Taxes
		BigDecimal totalFareAdult = new BigDecimal(0);
		BigDecimal totalFareChildren = new BigDecimal(0);
		BigDecimal totalFareInfant = new BigDecimal(0);
		BigDecimal totalTaxes = new BigDecimal(0);
		BigDecimal totalExtra = new BigDecimal(0);
		BigDecimal totalPrice = new BigDecimal(0);
		
		List<TaxData> selectionTaxes = ctx.selectionTaxes;
		/*if (ctx.award) { FIXME introdurre per award
			double adultFraction = (double) searchPassengersNumber.getNumAdults() / ( (double) searchPassengersNumber.getNumAdults() +  (double) searchPassengersNumber.getNumChildren());
			double adultQuantity = ctx.totalAwardPrice.intValue() * adultFraction;
			int totalAdult = (int) (adultQuantity);
			awardAdult = Integer.toString(totalAdult);
			
			
			if (searchPassengersNumber.getNumChildren() > 0) {
				double childrenFraction = (double) searchPassengersNumber.getNumChildren() / ( (double) searchPassengersNumber.getNumAdults() +  (double) searchPassengersNumber.getNumChildren());
				double childrenQuantity = ctx.totalAwardPrice.doubleValue() * childrenFraction;
				int totalChildren = (int) (childrenQuantity);
				awardChildren= Integer.toString(totalChildren);
			}
			
			if (selectionTaxes != null) {
				for(TaxData taxData : selectionTaxes){
					if(taxData.getCode().toLowerCase().contains("taxtotal")){
			    		totalTaxes = totalTaxes.add(taxData.getAmount());
			    	}
			    	if(taxData.getCode().toLowerCase().contains("yqtotal")){
			    		totalExtra = totalExtra.add(taxData.getAmount());
			    	}
				}
				
			}
			
		} else {*/
			
			if (selectionTaxes != null) {
				for(TaxData taxData : selectionTaxes){
			    	if(taxData.getCode().toLowerCase().contains("fare adult") 
			    			|| taxData.getCode().toLowerCase().contains("fare youth")){
			    		totalFareAdult = totalFareAdult.add(taxData.getAmount());
			    	}
			    	if(taxData.getCode().toLowerCase().contains("fare children")){
			    		totalFareChildren = totalFareChildren.add(taxData.getAmount());
			    	}
			    	if(taxData.getCode().toLowerCase().contains("fare infant")){
			    		totalFareInfant = totalFareInfant.add(taxData.getAmount());
			    	}
			    	if(taxData.getCode().toLowerCase().contains("taxtotal")){
			    		totalTaxes = totalTaxes.add(taxData.getAmount());
			    	}
			    	if(taxData.getCode().toLowerCase().contains("yqtotal")){
			    		totalExtra = totalExtra.add(taxData.getAmount());
			    	}
		
				}
			}
		//}
		
		
		//Obtaining Extracharge
		BigDecimal totalExtraCharge = ctx.totalExtraCharges;
		showExtraCharge = false;
		if (!totalExtraCharge.equals(new BigDecimal(0))) {
			showExtraCharge = true;
		}
		
		totalPrice = totalPrice.add(totalFareAdult);
		totalPrice = totalPrice.add(totalFareChildren);
		totalPrice = totalPrice.add(totalFareInfant);
		totalPrice = totalPrice.add(totalTaxes);
		totalPrice = totalPrice.add(totalExtra);
		totalPrice = totalPrice.add(totalExtraCharge);
		if (!ctx.isCarnetProcess && ctx.ccFeeApplied) {
			totalPrice = totalPrice.add(ccFeePrice);
		}
		
		if (ctx.coupon != null && ctx.coupon.isValid()) {
			ecouponPrice = new GenericPriceRender(ctx.totalCouponPrice, ctx.currency, ctx);
			totalPrice = totalPrice.subtract(ctx.totalCouponPrice);
		} else {
			ecouponPrice = null;
		}
		if (ctx.cashAndMiles != null) {
			cashAndMilesPrice = new GenericPriceRender(ctx.cashAndMiles.getDiscountAmount(), ctx.currency, ctx);
			totalPrice = totalPrice.subtract(ctx.cashAndMiles.getDiscountAmount());
		} else {
			cashAndMilesPrice = null;
		}
		if (ctx.insuranceProposalData != null && ctx.isInsuranceApplied) {
			insurancePrice = new GenericPriceRender(ctx.insuranceAmount, ctx.currency, ctx);
			totalPrice = totalPrice.add(ctx.insuranceAmount);
		} else {
			insurancePrice = null;
		}

		price =  new GenericPriceRender(totalFareAdult, totalFareChildren, totalFareInfant, totalTaxes, totalExtra, totalExtraCharge, 
				totalPrice, ctx.currency, ctx);
	}
	
	private String obtainTitleBySliceIndex(int sliceIndex, I18n i18n) {
		String msg = "";
		switch(sliceIndex){
			case 0:{
				msg = i18n.get("booking.common.multitratta.prima.label"); //"PRIMA"
			}break;
			case 1:{
				msg = i18n.get("booking.common.multitratta.seconda.label"); //"SECONDA"
			}break;
			case 2:{
				msg = i18n.get("booking.common.multitratta.terza.label"); //"TERZA"
			}break;
			case 3:{
				msg = i18n.get("booking.common.multitratta.quarta.label"); //"QUARTA"
			}break;
			default:{
				return "";
			}
		}
		return msg + " " + i18n.get("booking.common.multitratta.tratta.label"); //"TRATTA"
	}
	
}
