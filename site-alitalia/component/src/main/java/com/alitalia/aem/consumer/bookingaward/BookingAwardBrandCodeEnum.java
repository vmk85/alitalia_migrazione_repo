package com.alitalia.aem.consumer.bookingaward;

public enum BookingAwardBrandCodeEnum {
	
	ECONOMY_STANDARD("U"),
	ECONOMY_FLEX("M"),
	CLASSICA_PLUS("A"),
	BUSINESS_STANDARD("Z");
	
	private final String value;
	
	private BookingAwardBrandCodeEnum(String v) {
		value = v;
	}
	
	public String value() {
		return value;
	}
	
	public static BookingAwardBrandCodeEnum fromValue(String v) {
		for (BookingAwardBrandCodeEnum c: BookingAwardBrandCodeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		return null;
	}
}
