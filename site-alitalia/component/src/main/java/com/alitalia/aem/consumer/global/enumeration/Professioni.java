package com.alitalia.aem.consumer.global.enumeration;

import java.util.ArrayList;

public enum Professioni {
	ALTRO("Altro"),
	DIRIGENTE("Dirigente"),
	GIORNALISTA("Giornalista"),
	IMPIEGATO("Impiegato"),
	IMPRENDITORE("Imprenditore"),
	LIBERO_PROFESSIONISTA("LiberoProfessionista"),
	QUADRO("Quadro");
	
	private final String value;

	Professioni(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	public String getI18nValue() {
		return "millemiglia.professioni." + value + ".label";
	}
	
	public static ArrayList<String> listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (Professioni c: Professioni.values()) {
			result.add(c.value);
		}
		return result;
	}

	public static Professioni fromValue(String v) {
		for (Professioni c: Professioni.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
