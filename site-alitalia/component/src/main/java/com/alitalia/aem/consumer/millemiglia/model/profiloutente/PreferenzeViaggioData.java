package com.alitalia.aem.consumer.millemiglia.model.profiloutente;

import java.util.List;

import com.alitalia.aem.common.data.home.MMLifestyleData;

public class PreferenzeViaggioData {

	public static final String FREQUENT_FLYER_PROGRAMS = "FREQUENT FLYER PROGRAMS";
	
	private String preferenzePosto;
	private String preferenzePasto;
	private List<MMLifestyleData> frequentFlyerCompaniesSelected;
	
	public String getPreferenzePosto() {
		return preferenzePosto;
	}
	
	public void setPreferenzePosto(String preferenzePosto) {
		this.preferenzePosto = preferenzePosto;
	}
	
	public String getPreferenzePasto() {
		return preferenzePasto;
	}
	
	public void setPreferenzePasto(String preferenzePasto) {
		this.preferenzePasto = preferenzePasto;
	}
	
	public List<MMLifestyleData> getFrequentFlyerCompaniesSelected() {
		return frequentFlyerCompaniesSelected;
	}
	
	public void setFrequentFlyerCompaniesSelected(List<MMLifestyleData> frequentFlyerCompaniesSelected) {
		this.frequentFlyerCompaniesSelected = frequentFlyerCompaniesSelected;
	}
	
}
