package com.alitalia.aem.consumer.servlet.preparaviaggio;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.common.data.home.FlightDetailsModelData;
import com.alitalia.aem.common.data.home.FlightInfoData;
import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;
import com.alitalia.aem.common.messages.home.GetSabrePNRRequest;
import com.alitalia.aem.common.messages.home.GetSabrePNRResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsResponse;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.global.i18n.I18nKeyBooking;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeyComponents;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.model.content.flightinfo.FlightStatusData;
import com.alitalia.aem.consumer.ssw.SabreSonicWebDeepLinkCostants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.TypeUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.flightstatus.delegate.FlightStatusDelegate;
import com.alitalia.aem.web.component.managemybooking.delegate.ManageMyBookingDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "preparaviaggiosubmit2" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) 
		})
@SuppressWarnings("serial")
public class PreparaViaggioServlet2 extends GenericFormValidatorServlet {
	
	
	private ComponentContext componentContext;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ManageMyBookingDelegate mmbDelegate;

	@Reference
	private AlitaliaConfigurationHolder configuration;
	private static final String DEFAULT_FLIGHT_VECTOR = "AZ";
	private static final String STANDARD_REQUEST_DATE_FORMAT = "dd/mm/yy";
	private final String AIRPORT_KEY = "airportsData.";
	private final String CITY_SELECTOR = ".city";
	private final String NAME_SELECTOR = ".name";

	protected static final String ENCODING = "UTF-8";	
	private static String ERROR_QSTRING = "?success=false";
	
	private String errorPage;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile FlightStatusDelegate flightStatusDelegate;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[PreparaViaggioServlet] validateForm");

		try {
			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			return validator.validate();
		}

		// an error occurred...
		catch (Exception e) {
			logger.error("[PreparaViaggioServlet] Errore durante "
					+ "la procedura di salvataggio dati ", e);
			return null;
		}
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception{
		logger.debug("[PreparaViaggioServlet] performSubmit");
		
		Boolean result = false;
		String res = null;
		// prepare resource bundle for i18n
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		
		try {
			// set content type JSON and prepare response
			response.setContentType("application/json");
			// identify FORM
			String formType = request.getParameter("formType");
			// stato_volo
			if (("STATO_VOLO").equals(formType)
					|| "STATO_VOLO_BOOKING_TIME_TABLE".equals(formType)) {
				res = performSubmitStatoVolo(request, json);
				result = res.equals("OK");
			} else if(("CHECK_IN").equals(formType)
					|| "MIEI_VOLI".equals(formType)
					|| "CHECK_IN_LOGIN".equals(formType)){
				result = performSubmitSabreDeepLink(request, json, response);
			}
		} catch (Exception e){
			logger.error("[PreparaViaggioServlet] Error during performSubmit ", e);
			result = false;
		}
		json.key("result").value(result ? "OK" : "NOK");
		if (!result) {
			json.key("fields");
			json.object();
			if ("NOT_FOUND".equals(res)) {
				json.key("numeroVolo").value(i18n.get("preparaViaggio.numeroVolo.error.invalid"));
			} else {
				json.key("numeroVolo").value(i18n.get("specialpage.error.service"));
			}
			json.endObject();
		}
		json.endObject();
	}

	private Boolean performSubmitSabreDeepLink(SlingHttpServletRequest request, TidyJSONWriter json, SlingHttpServletResponse response) 
			throws Exception {
		Boolean result = true;
		
		// get params from request
		String codice = request.getParameter("code");
		String nome = request.getParameter("name");
		String cognome = request.getParameter("surname");
		String formType = request.getParameter("formType");
		
		this.errorPage = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ ( "CHECK_IN".equals(formType) ? configuration.getCheckinHomePage() : configuration.getMmbHomePage()) + ERROR_QSTRING;
		
		String pnr = codice;
		
		
		String url = "";
		
		/*Chiamata a servizio per mappare i PNR non Sabre*/
		if(("CHECK_IN".equals(formType) || "MIEI_VOLI".equals(formType)) && codice.length() == 6){
			String callerIp = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
			String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
			String language = AlitaliaUtils.getRepositoryPathLanguage(request.getResource());
			String sid = "MIEI_VOLI".equals(formType) ? MmbConstants.MMB_SID : CheckinConstants.CHECKIN_SID;
			String locale =  language.toLowerCase() + "_" + market.toLowerCase();
			
			MmbRequestBaseInfoData baseInfoData = new MmbRequestBaseInfoData();
			baseInfoData.setClientIp(callerIp);
			baseInfoData.setSessionId(sid);
			baseInfoData.setSiteCode(market);
			
			GetSabrePNRRequest sabrePNRRequest = new GetSabrePNRRequest();
			sabrePNRRequest.setBaseInfo(baseInfoData);
			sabrePNRRequest.setSid(sid);
			sabrePNRRequest.setTid(IDFactory.getTid());
			sabrePNRRequest.setPnr(codice);
			sabrePNRRequest.setLocale(locale);
			GetSabrePNRResponse sabrePNRResponse;
			try{
				sabrePNRResponse =  mmbDelegate.getSabrePNR(sabrePNRRequest);
				if(sabrePNRResponse != null && sabrePNRResponse.getPnrSabre() != null 
						&& sabrePNRResponse.getPnrSabre().length() > 0){
					pnr = sabrePNRResponse.getPnrSabre();
				}
				else{
					logger.error("Sabre Response or PNR is empty");
				}
			}
			catch(Exception e){
				logger.error("Exception in GetSabrePNR: ", e);
//				url = this.errorPage;
//				result = false;
			}
			
		}
		
		if(result){
			if("CHECK_IN".equals(formType)){
				url = configuration.getSSWDXCIDeepLink();
				if(codice.length() == 6){
					/*PNR*/
					url += "?" + SabreSonicWebDeepLinkCostants.DEEP_LINK_PAGE + "=true" 
							+ "&" + SabreSonicWebDeepLinkCostants.SEARCH_OPTIONS_DXCI + "=PNR"
							+ "&" + SabreSonicWebDeepLinkCostants.PNR_DXCI + "=" + pnr ;
					url += "&" + SabreSonicWebDeepLinkCostants.LAST_NAME_DXCI + "=" + encodeWithWitespace(cognome);
				}
				else{
					/*TicketNumber*/
					url += "?" + SabreSonicWebDeepLinkCostants.DEEP_LINK_PAGE + "=true" 
							+ "&" + SabreSonicWebDeepLinkCostants.SEARCH_OPTIONS_DXCI + "=VCR"
							+ "&" + SabreSonicWebDeepLinkCostants.TKT_NUMBER_DXCI + "=" + codice;
					url += "&" + SabreSonicWebDeepLinkCostants.LAST_NAME_DXCI + "=" + encodeWithWitespace(cognome)
						+ "&" + SabreSonicWebDeepLinkCostants.FIRST_NAME_DXCI + "=" + encodeWithWitespace(nome);
				}
				
			}
			else if("MIEI_VOLI".equals(formType)){
				url = configuration.getSSWMTODeepLink();
				url += "?" + SabreSonicWebDeepLinkCostants.PNR_MTO + "=" + pnr 
						+ "&" + SabreSonicWebDeepLinkCostants.LAST_NAME_MTO + "=" + encodeWithWitespace(cognome);
			}
			else if("CHECK_IN_LOGIN".equals(formType)){
				url = configuration.getSSWDXCIDeepLink();
				url += "?" + SabreSonicWebDeepLinkCostants.DEEP_LINK_PAGE + "=true" 
						+ "&" + SabreSonicWebDeepLinkCostants.SEARCH_OPTIONS_DXCI + "=" + SabreSonicWebDeepLinkCostants.FFCODE_DXCI
						+ "&" + SabreSonicWebDeepLinkCostants.FFCODE_DXCI + "=" + codice 
						+ "&" + SabreSonicWebDeepLinkCostants.LAST_NAME_DXCI + "=" + encodeWithWitespace(cognome);
			}
			
			String lang = AlitaliaUtils.getRepositoryPathLanguage(request.getResource()).toLowerCase();
			String country = AlitaliaUtils.getRepositoryPathMarket(request.getResource()).toUpperCase();
			String langParam = lang + "_" + country;
			Map<String, String> sswLang = configuration.getMapProperty(AlitaliaConfigurationHolder.SSW_LANGUAGE_MAPPING);
			if(sswLang.get(lang) != null){
				langParam = sswLang.get(lang);
			}
			else if (configuration.getSetProperty(AlitaliaConfigurationHolder.SSW_NOT_SUPPORTED_LANGUAGES).contains(lang)) {
				lang = configuration.getStringProperty(AlitaliaConfigurationHolder.SSW_DEFAULT_LANGUAGE);
				country = configuration.getStringProperty(AlitaliaConfigurationHolder.SSW_DEFAULT_COUNTRY);
				langParam = lang + "_" + country;
			}
			url += "&" + SabreSonicWebDeepLinkCostants.LANG + "=" + langParam; 
		}
		/*se sync allora deve fare una redirect all'url*/
		if(!isAjaxRequest(request) && url.length() > 0){
			response.sendRedirect(url);
		}
		else{
			json.key("redirect").value(url);
		}
		return result;
	}

	/*
	 * performSubmitStatoVolo
	 * 
	 */
	private String performSubmitStatoVolo(SlingHttpServletRequest request,
			TidyJSONWriter json) throws Exception {
		logger.debug("[PreparaViaggioServlet] performSubmitStatoVolo");
		String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		
		String dataPartenza = request.getParameter("dataPartenza");
		String numeroVolo = request.getParameter("numeroVolo");
		String vettore = request.getParameter("vettore");
		if(vettore == null){
			vettore = DEFAULT_FLIGHT_VECTOR;
		}
		
		String currentFormat = AlitaliaUtils.getDateFormatByMarket(request.getResource());
		if (currentFormat == null) {
			currentFormat = STANDARD_REQUEST_DATE_FORMAT;
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(currentFormat);
		dateFormat.setLenient(false);
		
		Calendar departureDate =
				TypeUtils.createCalendar(dateFormat.parse(dataPartenza));
		
		FlightInfoData flightInfo = new FlightInfoData();
		flightInfo.setVector(vettore);
		flightInfo.setFlightNumber(numeroVolo);
		flightInfo.setDepartureDateTime(departureDate);
		
		RetrieveFlightInfoRequest flightInfoRequest =
				new RetrieveFlightInfoRequest();
		flightInfoRequest.setSid(IDFactory.getSid(request));
		flightInfoRequest.setTid(IDFactory.getTid());
		flightInfoRequest.setFlightInfoData(flightInfo);
		
		RetrieveFlightInfoResponse flightInfoResponse =
				flightStatusDelegate.retrieveFlightInfo(flightInfoRequest);
		
		if (flightInfoResponse.getFlightInfoData().getErrorDescription() != null &&
				flightInfoResponse.getFlightInfoData().getErrorDescription().equals("INVALID FLIGHT NUMBER")) {
			return "NOT_FOUND";
		}
		
		DateRender schedArrTimeDR=new DateRender(flightInfoResponse.getFlightInfoData()
				.getArrivalDateTime(), market);
		String scheduledArrivalTime = schedArrTimeDR.getHour()+" "+schedArrTimeDR.getPeriod();
		DateRender depTimDR=new DateRender(flightInfoResponse.getFlightInfoData()
				.getDepartureDateTime(), market);
		String scheduledDepartureTime =	depTimDR.getHour()+" "+depTimDR.getPeriod();
		DateRender realArrTimeDT=new DateRender(flightInfoResponse.getFlightInfoData()
				.getArrivalGapTime(), market);
		String realArrivalTime =realArrTimeDT.getHour()+" "+realArrTimeDT.getPeriod();
		DateRender realdepTimeDR=new DateRender(flightInfoResponse.getFlightInfoData()
				.getDepartureGapTime(), market);
		String realDepartureTime =	realdepTimeDR.getHour()+" "+realdepTimeDR.getPeriod();
		String flightState = getState(flightInfoResponse.getFlightInfoData());
		
		//RECUPERA I DATI DELLA DURATA DEL VOLO
		
		FlightDetailsModelData flightDetailsModel = new FlightDetailsModelData();
		flightDetailsModel.setFlightDate(departureDate);
		flightDetailsModel.setFlightNumber(numeroVolo);
		flightDetailsModel.setVector(vettore);
		
		RetrieveFlightDetailsRequest flightDetailsRequest = new RetrieveFlightDetailsRequest();
		flightDetailsRequest.setFlightDetailsModelData(flightDetailsModel);
		flightDetailsRequest.setSid(IDFactory.getSid(request));
		flightDetailsRequest.setTid(IDFactory.getTid());
		RetrieveFlightDetailsResponse flightDetailsResponse = 
				flightStatusDelegate.retrieveFlightDetails(flightDetailsRequest);
		String duration = "0h";
		if (flightDetailsResponse.getFlightDetailsModelData() != null 
				&& flightDetailsResponse.getFlightDetailsModelData().getFlySegments() != null
				&& flightDetailsResponse.getFlightDetailsModelData().getFlySegments().size() > 0) {
			
			String timeOfFlight = 
					flightDetailsResponse.getFlightDetailsModelData().getFlySegments().get(0).getTimeFromStart();
			if (timeOfFlight != null) {
				duration = getDifference(timeOfFlight);
				
			}
		}
		
		// RECUPERA I DATI DEGLI AEREOPORTI
		String arrivalAirportCode = flightInfoResponse.getFlightInfoData()
				.getArrivalAirport().getLocationCode();
		String departureAirportCode = flightInfoResponse.getFlightInfoData()
				.getDepartureAirport().getLocationCode();		
		
		/*flight not found case*/
		if(departureAirportCode == null){
			departureAirportCode = flightDetailsResponse
					.getFlightDetailsModelData().getFlySegments().get(0).getBoardPoint();
			Calendar depTime = Calendar.getInstance();
			depTime.setTime(new SimpleDateFormat("HHmm").parse(flightDetailsResponse
					.getFlightDetailsModelData().getFlySegments().get(0).getBoardPointDepTime()));
			scheduledDepartureTime = new DateRender(depTime, market).getHour();
			realDepartureTime = scheduledDepartureTime;
		}
		/*flight not found case*/
		if(arrivalAirportCode == null){
			arrivalAirportCode = flightDetailsResponse
					.getFlightDetailsModelData().getFlySegments().get(0).getOffPoint();
			Calendar arrTime = Calendar.getInstance();
			arrTime.setTime(new SimpleDateFormat("HHmm").parse(flightDetailsResponse
					.getFlightDetailsModelData().getFlySegments().get(0).getOffPointArrTime()));
			scheduledArrivalTime = new DateRender(arrTime, market).getHour();
			realArrivalTime = scheduledArrivalTime;
		}
		
		FlightStatusData flightStatusData = new FlightStatusData();
		flightStatusData.setFlightNumber(vettore
				+ flightInfoResponse.getFlightInfoData().getFlightNumber());
		flightStatusData.setScheduledArrivalTime(scheduledArrivalTime);
		flightStatusData.setScheduledDepartureTime(scheduledDepartureTime);
		flightStatusData.setRealArrivalTime(realArrivalTime);
		flightStatusData.setRealDepartureTime(realDepartureTime);
		flightStatusData.setState(flightState);
		flightStatusData.setArrivalAirportCode(arrivalAirportCode);
		flightStatusData.setArrivalAirport(AIRPORT_KEY
				+ arrivalAirportCode + NAME_SELECTOR);
		flightStatusData.setArrivalCity(AIRPORT_KEY
				+ arrivalAirportCode + CITY_SELECTOR);
		flightStatusData.setDepartureAirportCode(departureAirportCode);
		flightStatusData.setDepartureAirport(AIRPORT_KEY
				+ departureAirportCode + NAME_SELECTOR);
		flightStatusData.setDepartureCity(AIRPORT_KEY
				+ departureAirportCode + CITY_SELECTOR);
		flightStatusData.setTotalDuration(duration);
		
		request.getSession().setAttribute(FlightStatusData.NAME,
				flightStatusData);
		return "OK";
	}
	
	
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[PreparaViaggioServlet] saveDataIntoSession");
		
		FlightStatusData flightStatusData = new FlightStatusData();
		flightStatusData.setError(e.getMessage());
		request.getSession().setAttribute(FlightStatusData.NAME,
				flightStatusData);

	}

	/*
	 * Set Validator parameters for validation
	 * 
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {
		logger.debug("[PreparaViaggioServlet] setValidatorParameters");

		// get parameters from request
		String formType = request.getParameter("formType");
		
		// retrieve correct format date
		String dateFormat = AlitaliaUtils.getDateFormatByMarket(request.getResource());
		String addParamDate = "";
		if (dateFormat != null) {
			addParamDate = "|" + dateFormat;
		}

		// stato_volo
		if ("STATO_VOLO".equals(formType)) {
			// get params from request
			String numeroVolo = request.getParameter("numeroVolo");
			String dataPartenza = request.getParameter("dataPartenza");

			// add validate conditions
			validator.addDirectCondition("numeroVolo", numeroVolo,
					I18nKeyComponents.PREPARAVIAGGIO_NUMEROVOLO_EMPTY,
					"isNotEmpty");
			validator.addDirectCondition("numeroVolo", numeroVolo,
					I18nKeyComponents.PREPARAVIAGGIO_NUMEROVOLO_INVALID,
					"isNumeroVolo");
			validator.addDirectCondition("dataPartenza", dataPartenza,
					I18nKeyComponents.PREPARAVIAGGIO_DATAPARTENZA_EMPTY,
					"isNotEmpty");
			
			validator.addDirectCondition("dataPartenza", dataPartenza + addParamDate,
					I18nKeyComponents.PREPARAVIAGGIO_DATAPARTENZA_INVALID,
					"isDate");
			validator.addDirectCondition("dataPartenza", dataPartenza + addParamDate,
					I18nKeyComponents.PREPARAVIAGGIO_DATAPARTENZA_INVALID,
					"isValidFlightDate");

		}
		
		// orari
		else if (("ORARI").equals(formType)) {

			// get params from request
			String aeroportoPartenza = request.getParameter("infovoli_departures");
			String aeroportoArrivo = request.getParameter("infovoli_arrival");
			String dataPartenza = request.getParameter("dataPartenza");

			// add validate conditions
			validator.addDirectCondition("infovoli_departures",
					aeroportoPartenza,
					I18nKeyComponents.PREPARAVIAGGIO_AEROPORTOPARTENZA_EMPTY,
					"isNotEmpty");
			validator.addDirectCondition("infovoli_departures",
					aeroportoPartenza,
					I18nKeyBooking.MESSAGE_ERROR_AIRPORT_FROM_NOT_VALID,
					"isIataAptCode");
			validator.addDirectCondition("infovoli_arrival", aeroportoArrivo,
					I18nKeyComponents.PREPARAVIAGGIO_AEROPORTOARRIVO_EMPTY,
					"isNotEmpty");
			validator.addDirectCondition("infovoli_arrival", aeroportoArrivo,
					I18nKeyBooking.MESSAGE_ERROR_AIRPORT_TO_NOT_VALID,
					"isIataAptCode");
			validator.addDirectCondition("dataPartenza",
					dataPartenza,
					I18nKeyComponents.PREPARAVIAGGIO_DATAPARTENZA_EMPTY,
					"isNotEmpty");
			validator.addDirectCondition("dataPartenza",
					dataPartenza + addParamDate,
					I18nKeyComponents.PREPARAVIAGGIO_DATAPARTENZA_INVALID,
					"isDate");

		}
		
		// check_in || miei_voli
		else if (("CHECK_IN").equals(formType)
				|| ("MIEI_VOLI").equals(formType)) {
			
			// get params from request
			String codice = request.getParameter("code");
			String nome = request.getParameter("name");
			String cognome = request.getParameter("surname");

			// add validate conditions
			validator.addDirectCondition("code", codice,
					I18nKeyComponents.PREPARAVIAGGIO_PNRTICKETNUMBER_EMPTY,
					"isNotEmpty");
			validator.addDirectCondition("code", codice,
					I18nKeyComponents.PREPARAVIAGGIO_PNRTICKETNUMBER_INVALID,
					"isPnrOrTicketNumber");
			if(("CHECK_IN").equals(formType)){
				validator.addDirectCondition("name", nome,
						I18nKeyComponents.PREPARAVIAGGIO_NOME_EMPTY,
						"isNotEmpty");
				validator.addDirectCondition("name", nome,
						I18nKeyComponents.PREPARAVIAGGIO_NOME_INVALID,
						"isAlphabeticWithAccentAndSpacesExtended");
			}
			validator.addDirectCondition("surname", cognome,
					I18nKeyComponents.PREPARAVIAGGIO_COGNOME_EMPTY,
					"isNotEmpty");
			validator.addDirectCondition("surname", cognome,
					I18nKeyComponents.PREPARAVIAGGIO_COGNOME_INVALID,
					"isAlphabeticWithAccentAndSpacesExtended");
		}
		else if("CHECK_IN_LOGIN".equals(formType)){
			
			String mmCode = request.getParameter("code");
			String cognome = request.getParameter("surname");
			
			validator.addDirectConditionMessagePattern("code", mmCode,
					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
					I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "isNotEmpty");
			validator.addDirectConditionMessagePattern("code", mmCode,
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "isNumber");
			validator.addCrossConditionMessagePattern("code", mmCode, "9",
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "sizeIsLess");
			validator.addDirectCondition("surname", cognome,
					I18nKeyComponents.PREPARAVIAGGIO_COGNOME_EMPTY,
					"isNotEmpty");
			validator.addDirectCondition("surname", cognome,
					I18nKeyComponents.PREPARAVIAGGIO_COGNOME_INVALID,
					"isAlphabeticWithAccentAndSpacesExtended");
		}

		return validator;

	}
	
	
	private String getDifference(String time) {
		String hours = time.length() > 2 ? time.substring(0, time.length()-2) : "0";
		String minutes = time.substring(time.length()-2);
		return hours + "h " + minutes + "'";
	}
	
	/*
	 * getState
	 * 
	 */
	private String getState(FlightInfoData flightInfo) {
		logger.debug("[PreparaViaggioServlet] getState");
		
		String state = "";
		String statoPartenza = flightInfo.getDepartureDetails().getCodeStatus();
		String statoArrivo = flightInfo.getArrivalDetails().getCodeStatus();
		HashMap<String, String> errorState = new HashMap<String, String>();
		errorState.put("FLT CNLD FOR FLT/DATE", I18nKeyBooking.FLIGHT_INFO_DELETED_FLIGHT);
		/*i seguenti non sono usati*/
		errorState.put("INVLD FLT NB", I18nKeyBooking.FLIGHT_INFO_FLIGHT_NUMBER_NOT_VALID);
		errorState.put("FLT NOOP FOR FLT/DAT", I18nKeyBooking.FLIGHT_INFO_FLIGHT_NOT_OPERATIVE);

		if (statoArrivo != null) {
			if (statoArrivo.equals("A")) {
				state = I18nKeyBooking.FLIGHT_INFO_FLIGHT_LANDED;
			} else if (statoPartenza.equals("E") && statoArrivo.equals("E")) {
				state = I18nKeyBooking.FLIGHT_INFO_BEFORE_FLIGHT;

			} else if ((statoPartenza.equals("A") && statoArrivo.equals("E"))
					|| (statoPartenza.equals("T") && (statoArrivo.equals("T")
							|| statoArrivo.equals("S")))) {
				state = I18nKeyBooking.FLIGHT_INFO_IN_FLIGHT;

			} else if (statoArrivo.equals("E") && (statoPartenza.equals("T")
					|| statoPartenza.equals("S"))) {
				state = I18nKeyBooking.FLIGHT_INFO_ARRIVING;
			}

		} else if (errorState.get(flightInfo.getErrorDescription()) != null) {
			state = errorState.get(flightInfo.getErrorDescription());
		}

		return state;
	}
	
	
	@Override
	protected String getSelfPageUrl(SlingHttpServletRequest request) {
		String selfPageUrl = "";
		if(this.errorPage != null && this.errorPage.length() > 0){
			selfPageUrl = this.errorPage;
		}
		else {
			selfPageUrl = super.getSelfPageUrl(request);
		}
		return selfPageUrl;
	}
	
	private String encodeWithWitespace(String string){
		String encodedString = "";
		try{
			encodedString = URLEncoder.encode(string, ENCODING).replace("+", "%20");
		}
		catch(Exception e){}
		return encodedString;
	}
	
}