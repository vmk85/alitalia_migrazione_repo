package com.alitalia.aem.consumer.checkin.model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinRouteSelect extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private CheckinSession checkinSession;
	
	@PostConstruct
	protected void initModel() {
		initBaseModel(request);
		checkinSession.prepareSelectedRoute(request.getParameter(
					CheckinConstants.CHECKIN_SELECTED_ROUTE_PARAM), ctx);
	}
}
