package com.alitalia.aem.consumer.mmb;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.lang3.StringUtils;

import com.alitalia.aem.common.data.home.SeatData;
import com.alitalia.aem.common.data.home.SeatMapData;
import com.alitalia.aem.common.data.home.SeatMapRowData;
import com.alitalia.aem.common.data.home.SeatMapSectorData;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.TypeSeatEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCouponData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryEticketData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPassengerData;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;
import com.alitalia.aem.consumer.utils.Lists;

public class MmbUtils {
	
	/**
	 * Find the passeger for the ancillary, based on coupon number comparison.
	 * 
	 * @param ancillary The ancillary to verify.
	 * @param allAncillaryPassengers The list of all ancillary passengers to check.
	 * @return The passenger, or null when not found.
	 */
	public static MmbAncillaryPassengerData findAncillaryPassenger(MmbAncillaryData ancillary, 
			List<MmbAncillaryPassengerData> allAncillaryPassengers) {
		for (String couponNumber : ancillary.getCouponNumbers()) {
			for (MmbAncillaryPassengerData currentPassenger : allAncillaryPassengers) {
				for (MmbAncillaryEticketData eticket : currentPassenger.getEtickets()) {
					for (MmbAncillaryCouponData coupon : eticket.getCoupons()) {
						if (couponNumber.equals(coupon.getNumber())) {
							return currentPassenger;
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * Find the flight data related the ancillary, based on coupon number comparison.
	 * 
	 * @param ancillary The ancillary to verify.
	 * @param allAncillaryFlights The list of all flights to check.
	 * @return The flight, or null when not found.
	 */
	public static MmbFlightData findAncillaryFlight(MmbAncillaryData ancillary,
			List<MmbFlightData> allAncillaryFlights) {
		for (String couponNumber : ancillary.getCouponNumbers()) {
			for (MmbFlightData flight : allAncillaryFlights) {
				for (MmbPassengerData passenger : flight.getPassengers()) {
					if (couponNumber.equals(passenger.getEticket())) {
						return flight;
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * Find the specified seat number in the seat map and returns its seat type.
	 * 
	 * @param seatMapData The seat map data.
	 * @param seatNumber The seat number to search for.
	 * @return The seat type, or null when not found.
	 */
	public static TypeSeatEnum findSeatTypeInSeatMap(SeatMapData seatMapData, String seatNumber) {
		ListIterator<SeatMapSectorData> sectorsIterator = (new LinkedList<SeatMapSectorData>(seatMapData.getSeatMapSectors())).listIterator();
		while (sectorsIterator.hasNext()) {
			SeatMapSectorData section = sectorsIterator.next();
			for (SeatMapRowData sectionRow : section.getSeatMapRows()) {
				for (SeatData seat : sectionRow.getSeats()) {
					String currentSeatNumber = StringUtils.stripStart(sectionRow.getNumber(), "0") + seat.getNumber();
					if (currentSeatNumber.equals(seatNumber)) {
						return seat.getTypeSeat();
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * Get credit card code given its name (i.e. the enumeration value).
	 */
	public static String getCodeFromCreditCardTypeEnum(String creditCardName) {
		if (creditCardName.equals(CreditCardTypeEnum.AMERICAN_EXPRESS.value())) {
			return "AX";
		} else if (creditCardName.equals(CreditCardTypeEnum.DINERS.value())) {
			return "DC";
		} else if (creditCardName.equals(CreditCardTypeEnum.MASTER_CARD.value())) {
			return "MC";
		} else if (creditCardName.equals(CreditCardTypeEnum.UATP.value())) {
			return "TP";
		} else if (creditCardName.equals(CreditCardTypeEnum.VISA_ELECTRON.value())) {
			return "VE";
		} else if (creditCardName.equals(CreditCardTypeEnum.VISA.value())) {
			return "CV";
		}
		return "";
	}
	
	/**
	 * Get credit card code (i.e. the enumeration value) by code.
	 */
	public static CreditCardTypeEnum getCreditCardTypeEnumFromCode(String creditCardCode) {
		if ("AX".equals(creditCardCode)) {
			return CreditCardTypeEnum.AMERICAN_EXPRESS;
		} else if ("DC".equals(creditCardCode)) {
			return CreditCardTypeEnum.DINERS;
		} else if ("MC".equals(creditCardCode)) {
			return CreditCardTypeEnum.MASTER_CARD;
		} else if ("TP".equals(creditCardCode)) {
			return CreditCardTypeEnum.UATP;
		} else if ("VE".equals(creditCardCode)) {
			return CreditCardTypeEnum.VISA_ELECTRON;
		} else if ("CV".equals(creditCardCode)) {
			return CreditCardTypeEnum.VISA;
		}
		return null;
	}
	
	/**
	 * Groups a list of flights by routes, based on the routeId attribute.
	 */
	public static List<List<MmbFlightData>> groupFlightsByRoute(List<MmbFlightData> flights) {
		List<List<MmbFlightData>> flightsByRoute = new ArrayList<>();
		List<MmbFlightData> routeFlights = new ArrayList<>();
		flightsByRoute.add(routeFlights);
		RouteTypeEnum lastRouteTypeEnum = null;
		Integer lastRouteId = Integer.MIN_VALUE;
		for (MmbFlightData flight : flights) {
			if (lastRouteTypeEnum != null &&
					(!flight.getType().equals(lastRouteTypeEnum) 
					|| !lastRouteId.equals(flight.getRouteId()))) {
				routeFlights = new ArrayList<>();
				flightsByRoute.add(routeFlights);
			}
			routeFlights.add(flight);
			lastRouteTypeEnum = flight.getType();
			lastRouteId = flight.getRouteId();
		}
		return flightsByRoute;
	}

	/**
	 * Determine if a group of flights are to be managed as a multileg flight,
	 * based on the number of routes and the 
	 */
	public static boolean isMultiLeg(List<MmbFlightData> flights) {
		List<List<MmbFlightData>> flightsByRoute = groupFlightsByRoute(flights);
		if (flightsByRoute.size() == 0 || flightsByRoute.size() == 1) {
			return false;
		} else if (flightsByRoute.size() > 2) {
			return true;
		} else {
			// exactly 2 route, determine if it is an outbound/return
			String outboundFrom = Lists.getFirst(flightsByRoute.get(0)).getFrom().getCode();
			String outboundTo = Lists.getLast(flightsByRoute.get(0)).getTo().getCode();
			String returnFrom = Lists.getFirst(flightsByRoute.get(1)).getFrom().getCode();
			String returnTo = Lists.getLast(flightsByRoute.get(1)).getTo().getCode();
			return !(outboundFrom.equals(returnTo) && outboundTo.equals(returnFrom));
		}
	}
	
	/**
	 * Determine if a group of ancillaries refers to an outbound or a return flight
	 */
	public static RouteTypeEnum getAncillariesGroupRouteType(MmbAncillariesGroup group) {
		if (group.getFlights() != null && group.getFlights().size()>0) {
			return group.getFlights().get(0).getType();
		}
		return null;
	}
	
}
