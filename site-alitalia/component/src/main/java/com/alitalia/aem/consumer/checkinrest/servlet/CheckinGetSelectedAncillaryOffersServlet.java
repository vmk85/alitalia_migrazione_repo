package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "getselectedancillaryoffers"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinGetSelectedAncillaryOffersServlet extends GenericCheckinFormValidatorServlet {
	
	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private CheckinSession checkinSession;

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		//TODO - da verificare
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

		CheckinSessionContext ctx = getCheckInSessionContext(request);
		
		String errorUrl = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCheckinFailurePage();
		
		errorUrl = request.getResourceResolver().map(errorUrl);
		
		//TODO - da cambiare!!!
		String successPage = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) + getConfiguration().getCheckinPassengersPage();
		
		successPage = request.getResourceResolver().map(successPage);
		
		//TODO 
		boolean result = checkinSession.checkinGetSelectedAncillaryOffers(request, ctx);
		
		if (result) {
			logger.info("[CheckinGetSelectedAncillaryOffersServlet] - redirect to: " + successPage );
			response.sendRedirect(successPage);
		} else {
			//TODO - redirect to errorPage?
			logger.error("[CheckinGetSelectedAncillaryOffersServlet] - redirect to: " + errorUrl );
			TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
			response.setContentType("application/json");
			json.object();
			json.key("isError").value(true);
			json.endObject();
		}

	}

}
