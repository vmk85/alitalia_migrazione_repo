package com.alitalia.aem.consumer.booking.analytics.filler;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.List;

import com.alitalia.aem.consumer.booking.analytics.AnalyticsInfo;
import com.alitalia.aem.consumer.booking.analytics.AncillaryInfo;
import com.alitalia.aem.consumer.booking.analytics.ConsumerInfo;
import com.alitalia.aem.consumer.booking.analytics.FlightSegmentInfo;
import com.alitalia.aem.consumer.booking.analytics.ItemsInfo;

public class BookingAwardAnalyticsInfoToJsObj extends ToJsObj implements AnalyticsInfoToJsObj {
	
	
	private static final String HYPHEN_FLIGHT_DATE_FORMAT = "yyyy-MM-dd";
	private static final String HYPHEN_EU_FLIGHT_DATE_FORMAT = "dd-MM-yy";
	private static final String EU_FLIGHT_DATE_FORMAT = "dd/MM/yyyy";
	private static final String EN_FLIGHT_DATE_FORMAT = "MM/dd/yyyy";
	private static final String TIME_FORMAT_HH_MM_SS = "kk:mm:ss";
	
	private DecimalFormat format;
	private SimpleDateFormat dateFormat_eu;
	private SimpleDateFormat dateFormat_en;
	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateFormat_eu_hyphen;
	private SimpleDateFormat timeFormat;
	

	public BookingAwardAnalyticsInfoToJsObj() {
		initFormats();
	}
	
	protected void initFormats() {
		format = new DecimalFormat("#0.00");
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setCurrencySymbol(""); 
		format.setDecimalFormatSymbols(otherSymbols);
		format.setGroupingUsed(false);
		format.setPositivePrefix("");
		format.setNegativePrefix("");
		
		dateFormat_eu = new SimpleDateFormat(EU_FLIGHT_DATE_FORMAT);
		dateFormat_eu.setLenient(false);
		
		dateFormat_en = new SimpleDateFormat(EN_FLIGHT_DATE_FORMAT);
		dateFormat_en.setLenient(false);
		
		dateFormat = new SimpleDateFormat(HYPHEN_FLIGHT_DATE_FORMAT);
		dateFormat.setLenient(false);
		dateFormat_eu_hyphen = new SimpleDateFormat(HYPHEN_EU_FLIGHT_DATE_FORMAT);
		dateFormat_eu_hyphen.setLenient(false);
		
		timeFormat = new SimpleDateFormat(TIME_FORMAT_HH_MM_SS);
		timeFormat.setLenient(false);
	}
	
	
	@Override
	public String toJSObj(AnalyticsInfo info, boolean trailingSemicolon) {
		String js = "{";
		/*Fanplayr info*/
		js = addJsTimeElem(js, "depHours", info.depHours, true);
		js = addJsTimeElem(js, "depMinutes",info.depMinutes, true);
		js = addJsObjElem(js, "depDate", formatIfNotNull(info.depDate, dateFormat), true);
		js = addJsObjElem(js, "depcost", formatIfNotNull(info.depcost, format), true);
		js = addJsObjElem(js, "depFlightNumber", info.depFlightNumber, true);
		js = addJsObjElem(js, "depBrand", info.depBrand, true);
		js = addJsObjElem(js, "depFareBasis", info.depFareBasis, true);
		js = addJsObjElem(js, "retDate", formatIfNotNull(info.retDate, dateFormat), true);
		js = addJsTimeElem(js, "retHours", info.retHours, true);
		js = addJsTimeElem(js, "retMinutes", info.retMinutes, true);
		js = addJsObjElem(js, "retcost", formatIfNotNull(info.retcost, format), true);
		js = addJsObjElem(js, "retFlightNumber", info.retFlightNumber, true);
		js = addJsObjElem(js, "retBrand", info.retBrand, true);
		js = addJsObjElem(js, "retFareBasis", info.retFareBasis, true);		
		js = addJsObjElem(js, "travelType", info.travelType, true);
		js = addJsObjElem(js, "numAdults", info.numAdults, true);
		js = addJsObjElem(js, "numYoung", info.numYoung, true);
		js = addJsObjElem(js, "numChildren", info.numChildren, true);
		js = addJsObjElem(js, "numInfant", info.numInfant, true);
		js = addJsObjElem(js, "Boapt", info.Boapt, true);
		js = addJsObjElem(js, "Bocity", info.Bocity, true);
		js = addJsObjElem(js, "Bocountry", info.Bocountry, true);
		js = addJsObjElem(js, "Arapt", info.Arapt, true);
		js = addJsObjElem(js, "Arcity", info.Arcity, true);
		js = addJsObjElem(js, "Arcountry", info.Arcountry, true);
		js = addJsObjElem(js, "Network", info.Network, true);
		js = addJsObjElem(js, "deltaBoToday", info.deltaBoToday, true);
		js = addJsObjElem(js, "deltaBoAr", info.deltaBoAr, true);
		js = addJsObjElem(js, "DayofWeekA", info.DayOfWeekA, true);
		js = addJsObjElem(js, "DayofWeekR", info.DayOfWeekR, true);
		js = addJsObjElem(js, "eCoupon", info.eCoupon, true);
		js = addJsObjElem(js, "totalPrice", formatIfNotNull(info.totalPrice, format), true);
		js = addJsObjElem(js, "fare", formatIfNotNull(info.fare, format), true);
		js = addJsObjElem(js, "taxes", formatIfNotNull(info.taxes, format), true);
		js = addJsObjElem(js, "surcharges", formatIfNotNull(info.surcharges, format), true);
		js = addJsObjElem(js, "email", info.email, true);
		js = addJsObjElem(js, "telefono", info.telefono, true);
		js = addJsObjElem(js, "valuta", info.valuta, true);
		
		/*Other info*/
		switch(info.step){
		case 1:
			js = addJsObjElem(js, "cabin", info.cabinData, false);
			break;
		case 2: 
			js = addJsArrayObjElem(js, "flightSegment", convFlightSegmentsTojsObj(info.flightSegments), true);
			js = addJsObjElem(js, "cabin", info.cabinData, false);

			break;
		case 3:
			js = addJsArrayObjElem(js, "consumers", convConsumersTojsObj(info.consumers), true);
			js = addJsObjElem(js, "cabin", info.cabinData, false);
			break;
		case 4:
			js = addJsArrayObjElem(js, "addAncillaries", convAncillariesTojsObj(info.ancillaries), true);
			js = addJsArrayObjElem(js, "consumers", convConsumersTojsObj(info.consumers), false);

			break;
		case 5:
			js =addJsArrayObjElem(js, "items", convItemsTojsObj(info.items), true);
			js = addJsObjElem(js, "NetRevenue", format.format(info.netRevenue), true);
			js = addJsObjElem(js, "NetRevenueEuro", format.format(info.netRevenue), true);
			js = addJsObjElem(js, "PNR", info.PNR, true);
			js = addJsObjElem(js, "IdOrd", dateFormat_eu.format(info.transactionDate.getTime()) + info.PNR, true);
			js = addJsObjElem(js, "paymentType", info.paymentType, true);
			js = addJsObjElem(js, "CCType", info.CCType, true);
			js = addJsObjElem(js, "tktNumber", info.tktNumber, true);
			js = addJsObjElem(js, "TktPerPNR", info.tktPerPNR, true);
			js = addJsObjElem(js, "Invoice", info.invoiceRequired ? "1" : "0", true);
			js = addJsObjElem(js, "Discount", format.format(info.discount), true);
			js = addJsObjElem(js, "UserType", info.userType, false);

		}
		js += (trailingSemicolon) ? "};" : "}";
		return js;
	}	
	

	
	
	private String convFlightSegmentsTojsObj(List<FlightSegmentInfo> flightSegments) {
		String js = "[";
		for(FlightSegmentInfo segment : flightSegments){
			js += "{";
			js = addJsObjElem(js, "SegBoapt", segment.getSegBoapt(), true);
			js = addJsObjElem(js, "SegArapt", segment.getSegArapt(), true);
			js = addJsObjElem(js, "SegdepDate", formatIfNotNull(segment.getSegdepDate(), dateFormat), true);
			js = addJsObjElem(js, "SegarrDate", formatIfNotNull(segment.getSegarrDate(), dateFormat), true);
			js = addJsTimeElem(js, "SegdepHours", segment.getSegdepHours(), true);
			js = addJsTimeElem(js, "SegdepMinutes", segment.getSegdepMinutes(), true);
			js = addJsTimeElem(js, "SegarrHours", segment.getSegarrHours(), true);
			js = addJsTimeElem(js, "SegarrMinutes", segment.getSegarrMinutes(), true);
			js = addJsObjElem(js, "SegFlightNumber", segment.getSegCarrier() + segment.getSegFlightNumber(), true);
			js = addJsObjElem(js, "SegCarrier", segment.getSegCarrier(), true);
			js = addJsObjElem(js, "SegCabin", segment.getSegCabin(), false);
			js += "},";
		}
		/*Remove last ","*/
		if(js.length() > 1){
			js = js.substring(0,js.length()-1);
		}
		js += "]";
		return js;
	} 
	
	
	private String convConsumersTojsObj(List<ConsumerInfo> consumers) {
		String js = "[";
		for(ConsumerInfo consumer : consumers){
			js += "{";
			js = addJsObjElem(js, "type", consumer.type, true);
			js = addAndEncodeJsObjElem(js, "nome", consumer.name, true);
			js = addAndEncodeJsObjElem(js, "cognome", consumer.surname, false);
			js += "},";
		}
		/*Remove last ","*/
		if(js.length() > 1){
			js = js.substring(0,js.length()-1);
		}
		js += "]";
		return js;
	} 
	
	private String convAncillariesTojsObj(List<AncillaryInfo> ancillaries) {
		String js = "[";
		for(AncillaryInfo anc : ancillaries){
			js += "{";
			js = addJsObjElem(js, "ancType", anc.ancType, true);
			js = addJsObjElem(js, "ancName", anc.ancName, true);
			js = addJsObjElem(js, "ancPrice", formatIfNotNullAncPrice(anc.ancPrice,format), false);
			js += "},";
		}
		/*Remove last ","*/
		if(js.length() > 1){
			js = js.substring(0,js.length()-1);
		}
		js += "]";
		return js;
	}
	
	private String convItemsTojsObj(List<ItemsInfo> items) {
		String js = "[";
		for(ItemsInfo item : items){
			js += "{";
			js = addJsObjElem(js, "type", item.type, true);
			js = addJsObjElem(js, "quantity", item.quantity, false);
			js += "},";
		}
		/*Remove last ","*/
		if(js.length() > 1){
			js = js.substring(0,js.length()-1);
		}
		js += "]";
		return js;
	}
	
}
