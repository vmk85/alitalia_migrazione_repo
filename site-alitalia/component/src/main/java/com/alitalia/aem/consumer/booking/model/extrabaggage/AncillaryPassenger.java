package com.alitalia.aem.consumer.booking.model.extrabaggage;



/** 
 Sabre Request Model for AncillaryPassenger
*/
public class AncillaryPassenger
{
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [JsonProperty(PropertyName = "@ref")][DataMember(Name = "@ref")] public int _ref {get;set;}
	private int _ref;
	public final int get_ref()
	{
		return _ref;
	}
	public final void set_ref(int value)
	{
		_ref = value;
	}
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [JsonProperty(PropertyName = "@id")][DataMember(Name = "@id")] public int id {get;set;}
	private int id;
	public final int getid()
	{
		return id;
	}
	public final void setid(int value)
	{
		id = value;
	}
	private int index;
	public final int getindex()
	{
		return index;
	}
	public final void setindex(int value)
	{
		index = value;
	}
	private String firstName;
	public final String getfirstName()
	{
		return firstName;
	}
	public final void setfirstName(String value)
	{
		firstName = value;
	}
	private String lastName;
	public final String getlastName()
	{
		return lastName;
	}
	public final void setlastName(String value)
	{
		lastName = value;
	}
	private AncillaryPassengerType type;
	public final AncillaryPassengerType gettype()
	{
		return type;
	}
	public final void settype(AncillaryPassengerType value)
	{
		type = value;
	}
	private String tierLevel;
	public final String gettierLevel()
	{
		return tierLevel;
	}
	public final void settierLevel(String value)
	{
		tierLevel = value;
	}
	private int tierLevelNumber;
	public final int gettierLevelNumber()
	{
		return tierLevelNumber;
	}
	public final void settierLevelNumber(int value)
	{
		tierLevelNumber = value;
	}
}