package com.alitalia.aem.consumer.millemiglia.servlet.profiloutente;

import java.io.IOException;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.NewInformationData;
import com.alitalia.aem.common.data.home.enumerations.MMTierCodeEnum;
import com.alitalia.aem.common.messages.home.ProfileRequest;
import com.alitalia.aem.common.messages.home.ProfileResponse;
import com.alitalia.aem.common.messages.home.UpdateUserProfileRequest;
import com.alitalia.aem.common.messages.home.UpdateUserProfileResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.global.enumeration.LinguaCorrispondenza;
import com.alitalia.aem.consumer.global.i18n.I18nKeyMillemiglia;
import com.alitalia.aem.consumer.millemiglia.model.profiloutente.ComunicazioniOfferteData;
import com.alitalia.aem.consumer.millemiglia.render.ProfiloUtenteRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;

@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "comunicazionioffertesubmit" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class ComunicazioniOfferteServlet extends GenericFormValidatorServlet {

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaCustomerProfileManager customerProfileManager;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {

		logger.debug("[ComunicazioniOfferteServlet] validateForm");

		try {
			// validate parameters!
			Validator validator = setValidatorParameters(request, new Validator());
			return validator.validate();
		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Errore durante la procedura di salvataggio dati ", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
								 SlingHttpServletResponse response) throws IOException {

		logger.debug("[ComunicazioniOfferteServlet] performSubmit");

		try {

			// recupero dati da aggiornare dalla request
			String newsletter = request.getParameter("newsletter");
			String tipoContatto = request.getParameter("tipoContatto");
			String linguaCorrispondenza = request.getParameter("linguaCorrispondenza");
			String codeCorrLang=AlitaliaUtils.getCodeLanguagefromLinguaCorrispondenza(LinguaCorrispondenza.fromValue(linguaCorrispondenza));
			String consensoDatiPersonali = request.getParameter("consensoDatiPersonali");
			String smsAuth = request.getParameter("checkSms");
			// recupero i dati dell'utente salvati nella clientcontext
			MMCustomerProfileData userProperties = AlitaliaUtils.getAuthenticatedUser(request);

			// recupero altri dati dell'utente loggato chiamando il servizio getProfile
			userProperties.setCustomerPinCode(
					customerProfileManager.decodeAndDecryptProperty(userProperties.getCustomerPinCode()));

			ProfileRequest profileRequest = new ProfileRequest();
			profileRequest.setSid(IDFactory.getSid());
			profileRequest.setTid(IDFactory.getTid());
			profileRequest.setCustomerProfile(userProperties);

			ProfileResponse profileResponse = consumerLoginDelegate.getProfile(profileRequest);
			MMCustomerProfileData customerProfileData = profileResponse.getCustomerProfile();
			ProfiloUtenteRender profiloUtenteRender = new ProfiloUtenteRender(customerProfileData);

			// creo gli oggetti newInformation e customerProfileToUpdate contenenti le informazioni da aggiornare
			NewInformationData newInformation = new NewInformationData();
			MMCustomerProfileData customerProfileToUpdate = new MMCustomerProfileData();

			// imposto gli attributi obbligatori
			customerProfileToUpdate.setCustomerNumber(userProperties.getCustomerNumber());
			customerProfileToUpdate.setCustomerPinCode(userProperties.getCustomerPinCode());
			customerProfileToUpdate.setCustomerName(userProperties.getCustomerName());
			customerProfileToUpdate.setCustomerSurname(userProperties.getCustomerSurname());
			customerProfileToUpdate.setBirthDate(userProperties.getBirthDate());
			customerProfileToUpdate.setGender(userProperties.getGender());
			customerProfileToUpdate.setEmail(profiloUtenteRender.getEmail().getEmail());
			customerProfileToUpdate.setTelephones(customerProfileData.getTelephones());
			customerProfileToUpdate.setAddresses(customerProfileData.getAddresses());
			customerProfileToUpdate.setCustomerWorkPosition(customerProfileData.getCustomerWorkPosition());
			customerProfileToUpdate.setDefaultAddressPostalCode(customerProfileData.getDefaultAddressPostalCode());
			customerProfileToUpdate.setDefaultAddressCountry(customerProfileData.getDefaultAddressCountry());
			customerProfileToUpdate.setDefaultAddressMunicipalityName(customerProfileData.getDefaultAddressMunicipalityName());
			customerProfileToUpdate.setDefaultAddressType(customerProfileData.getDefaultAddressType());
			customerProfileToUpdate.setDefaultAddressStreetFreeText(customerProfileData.getDefaultAddressStreetFreeText());
			customerProfileToUpdate.setContractAgree(customerProfileData.getContractAgree());
			customerProfileToUpdate.setLifestyles(customerProfileData.getLifestyles());
			customerProfileToUpdate.setMaritalStatus(customerProfileData.getMaritalStatus());
			customerProfileToUpdate.setMarket(customerProfileData.getMarket());
			customerProfileToUpdate.setPreferences(customerProfileData.getPreferences());
			customerProfileToUpdate.setUserIdRequest(customerProfileData.getUserIdRequest());
			customerProfileToUpdate.setTierCode(customerProfileData.getTierCode());
			customerProfileToUpdate.setTierDate(customerProfileData.getTierDate());
			customerProfileToUpdate.setPointsEarnedPartial(customerProfileData.getPointsEarnedPartial());
			customerProfileToUpdate.setPointsEarnedTotal(customerProfileData.getPointsEarnedTotal());
			customerProfileToUpdate.setPointsEarnedTotalQualified(customerProfileData.getPointsEarnedTotalQualified());
			customerProfileToUpdate.setPointsRemainingPartial(customerProfileData.getPointsRemainingPartial());
			customerProfileToUpdate.setPointsRemainingTotal(customerProfileData.getPointsRemainingTotal());
			customerProfileToUpdate.setPointsSpentPartial(customerProfileData.getPointsSpentPartial());
			customerProfileToUpdate.setPointsSpentTotal(customerProfileData.getPointsSpentTotal());
			customerProfileToUpdate.setCustomerNickName(customerProfileData.getCustomerNickName());
			// setting modified data
			customerProfileToUpdate.setMailingType(getMailingTypeCode(newsletter, tipoContatto));
			customerProfileToUpdate.setLanguage(codeCorrLang);
			newInformation.setSmsAuthorizationUpdated(true);
			customerProfileToUpdate.setSmsAuthorization(smsAuth != null);
			customerProfileToUpdate.setProfiling(consensoDatiPersonali.equals("y"));

			// perform update
			UpdateUserProfileRequest updateUserProfileRequest = new UpdateUserProfileRequest();
			updateUserProfileRequest.setTid(IDFactory.getTid());
			updateUserProfileRequest.setSid(IDFactory.getSid(request));
			updateUserProfileRequest.setCustomerProfile(customerProfileToUpdate);
			updateUserProfileRequest.setNewInformation(newInformation);

			UpdateUserProfileResponse updateUserProfileResponse =
					consumerLoginDelegate.updateProfile(updateUserProfileRequest);

			List<String> errors = updateUserProfileResponse.getCustomerUpdateErrors();
			if ((errors != null && errors.size() > 0 && !errors.get(0).equals("None")) ||
					updateUserProfileResponse.getCustomerProfile() == null) {
				throw new IOException(); //TODO: Verificare se sollevare un'eccezione diversa
			}

			// aggiorno i dati dell'utente salvati nella clientcontext
			customerProfileManager.updateCustomerProfileProperties(
					customerProfileToUpdate, request.getResourceResolver(), "");

			goBackSuccessfully(request, response);

		} catch(Exception e) {
			logger.error("Error Dati Personali Servlet", e);
			throw new IOException(); //TODO: Verificare se sollevare un'eccezione diversa
		}

	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {

		logger.debug("[ComunicazioniOfferteServlet] saveDataIntoSession");

		String newsletter = request.getParameter("newsletter");
		String tipoContatto = request.getParameter("tipoContatto");
		String linguaCorrispondenza = request.getParameter("linguaCorrispondenza");
		String consensoDatiPersonali = request.getParameter("consensoDatiPersonali");
		String smsAuth = request.getParameter("checkSms");

		ComunicazioniOfferteData comunicazioniOfferteData = new ComunicazioniOfferteData();

		comunicazioniOfferteData.setMailingType(getMailingTypeCode(newsletter, tipoContatto));
		comunicazioniOfferteData.setLanguage(linguaCorrispondenza);
		comunicazioniOfferteData.setProfiling(consensoDatiPersonali.equals("y"));
		comunicazioniOfferteData.setSmsAuth(smsAuth != null);

		request.getSession().setAttribute("comunicazioniOfferteData", comunicazioniOfferteData);
	}

	/**
	 * Set Validator parameters for validation
	 *
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request, Validator validator) {

		// get parameters from request
		String newsletter = request.getParameter("newsletter");
		String tipoContatto = request.getParameter("tipoContatto");
		String linguaCorrispondenza = request.getParameter("linguaCorrispondenza");
		String nazioneSelezionata = request.getParameter("nazioneSelezionata");
		String consensoDatiPersonali = request.getParameter("consensoDatiPersonali");

		// add validate conditions
		validator.addDirectCondition("newsletter", newsletter,
				I18nKeyMillemiglia.NEWSLETTER_ERROR_EMPTY, "isNotEmpty");

		validator.addDirectCondition("tipoContatto", tipoContatto,
				I18nKeyMillemiglia.TIPOCONTATTO_ERROR_EMPTY, "isNotEmpty");

		validator.addDirectCondition("linguaCorrispondenza", linguaCorrispondenza,
				I18nKeyMillemiglia.LINGUACORRISPONDENZA_ERROR_EMPTY, "isNotEmpty");

		validator.addCrossCondition("linguaCorrispondenza", linguaCorrispondenza, nazioneSelezionata,
				I18nKeyMillemiglia.LINGUACORRISPONDENZA_ERROR_INVALID, "isLinguaCorrispondenzaValid");

		validator.addDirectCondition("consensoDatiPersonali", consensoDatiPersonali,
				I18nKeyMillemiglia.CONSENSODATIPERSONALI_ERROR_EMPTY, "isNotEmpty");

		return validator;
	}

	private String getMailingTypeCode(String newsletter, String tipoContatto) {
		Boolean email = false;
		Boolean app = false;
		if(tipoContatto!=null || tipoContatto!=""){
			String[] values = tipoContatto.split(":");
			for (String v: values) {
				if(v.equals("email")){
					email = true;
				}
				if(v.equals("app")){
					app = true;
				}
			}
		}

		if (newsletter.equals("alitalia") && tipoContatto.equals("posta")) {
			return "001";
		} else if (newsletter.equals("alitalia") && tipoContatto.equals("email")) {
			return "002";
		} else if (newsletter.equals("alitaliaAndPartners") && tipoContatto.equals("posta")) {
			return "003";
		} else if (newsletter.equals("alitaliaAndPartners") && tipoContatto.equals("email")) {
			return "004";
		}  else if(newsletter.equals("alitalia") && email == true && app == false) {
			return "006";
		}  else if(newsletter.equals("alitalia") && app == true && email == false) {
			return "007";
		}  else if(newsletter.equals("alitalia") && app == true && email == true) {
			return "008";
		}  else if(newsletter.equals("alitaliaAndPartners") && email == true && app == false) {
			return "009";
		}  else if(newsletter.equals("alitaliaAndPartners") && app == true && email == false) {
			return "010";
		} else if(newsletter.equals("alitaliaAndPartners") && app == true && email == true) {
			return "011";
		}
		return "005";
	}
}
