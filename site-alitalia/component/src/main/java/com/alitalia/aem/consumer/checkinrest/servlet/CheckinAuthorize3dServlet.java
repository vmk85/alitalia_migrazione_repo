package com.alitalia.aem.consumer.checkinrest.servlet;

import com.alitalia.aem.common.messages.checkinrest.CheckinAuthorize3dResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinEndPaymentResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import javax.servlet.Servlet;
import java.io.IOException;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = {"authorize3d"}),
        @Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
        @Property(name = "sling.servlet.extensions", value = {"json"}),
        @Property(name = "sling.servlet.methods", value = {"POST"})})
@SuppressWarnings("serial")
public class CheckinAuthorize3dServlet extends GenericCheckinFormValidatorServlet {
    @Reference
    private AlitaliaConfigurationHolder configuration;

    @Reference
    private CheckinSession checkinSession;

    @Override
    protected AlitaliaConfigurationHolder getConfiguration() {
        return configuration;
    }

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        //TODO - c'e' solo validazione frontend
        ResultValidation resultValidation = new ResultValidation();
        resultValidation.setResult(true);
        return resultValidation;
    }

    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

        logger.info("[CheckinAuthorize3dServlet] performSubmit");
        String merchantData = request.getParameter("MD");

        String paymentResponse = request.getParameter("PaRes");

        try {

            if(merchantData == null || paymentResponse == null) {
                if(merchantData == null && paymentResponse == null) {
                    logger.error("[CheckinAuthorize3dServlet] missing parameters: merchantData (MD), paymentResponse (paRES)");
                } else if(merchantData == null) logger.error("[CheckinAuthorize3dServlet] missing parameter: merchantData (MD)");
                else if(paymentResponse == null) logger.error("[CheckinAuthorize3dServlet] missing parameter: paymentResponse (paRES)");
            } else {

                String ipAddress = AlitaliaUtils.getRequestRemoteAddr(configuration, request);

                logger.info("[CheckinAuthorize3dServlet] calling service authorize3d...");

                CheckinSessionContext ctx = (CheckinSessionContext) request.getSession(true).getAttribute(CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE);

//                if(getCheckInSessionContext(request) instanceof CheckinSessionContext) ctx = getCheckInSessionContext(request);

                if(ctx != null) {

                    CheckinAuthorize3dResponse res = checkinSession.authorize3d(request, ctx, ipAddress, merchantData, paymentResponse);

                    logger.info("[CheckinAuthorize3dServlet] service authorize3d called");

                    if (res == null || res.getAuthorize3dResponse() == null) {

                        logger.error("[CheckinAuthorize3dServlet] response is null");

                        TidyJSONWriter json = new TidyJSONWriter(response.getWriter());

                        json.object();

                        json.key("isError").value(true);
                        json.key("errorMessage").value("response is null");

                        json.endObject();

                    } else {
                        if (res.getAuthorize3dResponse().getResultCode() != null && res.getAuthorize3dResponse().getResponseCode().equals("APPROVED")) {

                            logger.info("[CheckinAuthorize3dServlet] status is APPROVED, calling endPayment");

//                        CheckinEndPaymentResponse endPayment = checkinSession.endPayment(request, getCheckInSessionContext(request), res.getAuthorize3dResponse().getApprovalCode());

//                        String protocol = configuration.getHttpsEnabled() ? "https://": "http://";
//                        String redirectUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) + "/.checkinendpayment.json";
//                        redirectUrl = request.getResourceResolver().map(redirectUrl);
//                        redirectUrl = protocol + configuration.getExternalDomain() + redirectUrl;
//
//                        response.sendRedirect(redirectUrl);
                            if (res.getAuthorize3dResponse().getApprovalCode() != null) {

                                logger.info("[CheckinAuthorize3dServlet] calling end payment..");

                                request.getSession().setAttribute("ApprovalCode", res.getAuthorize3dResponse().getApprovalCode());
//                                CheckinEndPaymentServlet endPayment = new CheckinEndPaymentServlet();
//                                endPayment.doPost(request, response);
                                String baseUrlForContext = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
                                String baseUrlForRedirect = request.getResource().getResourceResolver().map(baseUrlForContext);
                                String endPayment = ctx.domain + baseUrlForRedirect + "/check-in-search/check-in-finalize-payment.html";
                                response.sendRedirect(endPayment);

                            } else {

                                TidyJSONWriter json = new TidyJSONWriter(response.getWriter());

                                json.object();

                                json.key("isError").value(true);
                                json.key("errorMessage").value("3D payment not approved");

                                json.endObject();

                                logger.error("[CheckinAuthorize3dServlet] no approval code received " + res.getAuthorize3dResponse().toString());

                            }
                        } else {

                            TidyJSONWriter json = new TidyJSONWriter(response.getWriter());

                            json.key("isError").value(true);
                            json.key("errorMessage").value("3D payment not approved");

                            logger.error("[CheckinAuthorize3dServlet] 3D payment not approved " + res.getAuthorize3dResponse().toString());
                        }
                    }

                } else {

                    TidyJSONWriter json = new TidyJSONWriter(response.getWriter());

                    json.key("isError").value(true);
                    json.key("errorMessage").value("Context is null");

                    logger.error("[CheckinAuthorize3dServlet] context is null");

                }

            }


        } catch (Exception e) {
            logger.error("[CheckinAuthorize3dServlet] Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
