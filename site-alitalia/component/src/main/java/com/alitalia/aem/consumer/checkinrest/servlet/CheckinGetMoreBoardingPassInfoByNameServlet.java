package com.alitalia.aem.consumer.checkinrest.servlet;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = { "getmoreboardinigpassinfobyname"}),
        @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
        @Property(name = "sling.servlet.extensions", value = { "json" }),
        @Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinGetMoreBoardingPassInfoByNameServlet extends GenericCheckinFormValidatorServlet {

    // logger
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    private AlitaliaConfigurationHolder configuration;

    @Reference
    private CheckinSession checkinSession;

    @Override
    protected AlitaliaConfigurationHolder getConfiguration() {
        return configuration;
    }

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        //TODO - da verificare
        ResultValidation resultValidation = new ResultValidation();
        resultValidation.setResult(true);
        return resultValidation;
    }

    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
        CheckinSessionContext ctx = getCheckInSessionContext(request);

        String errorUrl = AlitaliaUtils.findSiteBaseExternalUrl(
                request.getResource(), false)
                + getConfiguration().getCheckinFailurePage();

        errorUrl = request.getResourceResolver().map(errorUrl);

        //TODO - da cambiare!!!
        String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
                request.getResource(), false)
                + getConfiguration().getCheckinPassengersPage();

        successPage = request.getResourceResolver().map(successPage);

        boolean result = checkinSession.getMoreBoardingPassInfoByName(request, ctx);

        TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
        response.setContentType("application/json");
        json.object();

        if (result) {

            json.key("passengerID").value(request.getParameter("passengerID"));
            json.key("isError").value(false);

            //response.sendRedirect(successPage);
        } else {
            //TODO - redirect to errorPage?

            json.key("isError").value(true);

        }

        json.endObject();

        //response.getWriter().write(json.toString());

    }

}
