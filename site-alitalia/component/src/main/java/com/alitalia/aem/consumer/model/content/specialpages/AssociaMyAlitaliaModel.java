package com.alitalia.aem.consumer.model.content.specialpages;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;




@Model(adaptables={SlingHttpServletRequest.class})
public class AssociaMyAlitaliaModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	@Inject
	private AlitaliaConfigurationHolder configuration;

	private MillemigliaLoginData millemigliaLoginData;


	private String associaMyAlitaliaAction;


	@PostConstruct
	protected void initModel() {
		try{
			logger.info("[AssociaMyAlitaliaModel] initModel");
		
			super.initBaseModel(slingHttpServletRequest);

		}
		catch(Exception e){
			logger.error("Unexpected Exception: ", e);
		}
	}

	public String getAssociaMyAlitaliaAction() {
		HttpSession session = request.getSession();
		if(session.getAttribute("myalitalia_associa_MM_action")!=null){
			associaMyAlitaliaAction = (String) session.getAttribute("myalitalia_associa_MM_action");
		}
		associaMyAlitaliaAction="ASSOCIA";

		return associaMyAlitaliaAction;
	}

}
