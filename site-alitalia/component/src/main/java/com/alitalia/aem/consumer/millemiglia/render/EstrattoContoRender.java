package com.alitalia.aem.consumer.millemiglia.render;

import com.alitalia.aem.common.data.home.MMActivityData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

public class EstrattoContoRender {

	private MMActivityData activity;
	private String activityDate;
	private String giorno;
	private String mese;
	private String anno;
	private String totalPointsSign;
	private String totalPointsAbsoluteValue;

	private String description;
	private String activityClass;

	public EstrattoContoRender(MMActivityData activity) {
		this.activity = activity;

		if (activity.getActivityDate() != null) {
			DateRender dateRender = new DateRender(activity.getActivityDate());
			this.totalPointsAbsoluteValue = String.valueOf(AlitaliaUtils.setThousandsSeparator(Math.abs(activity.getPointsTotal())));
			this.totalPointsSign = activity.getPointsTotal() < 0 ? "-" : "+";
			String description = activity.getDescription();
			String[] descriptionSplit = description.split(" ");
			this.description = description;
			if (descriptionSplit[descriptionSplit.length - 1].length() == 1) {
				// esiste l'informazione della classe
				this.activityClass = descriptionSplit[descriptionSplit.length - 1];
				this.description = description.substring(0, description.length()-1);
			} 
			this.activityDate = dateRender.getExtendedDate();
			this.giorno = dateRender.getDay();
			this.mese = dateRender.getMonth();
			this.anno = dateRender.getYear();
		}
	}

	public MMActivityData getActivity() {
		return activity;
	}

	public String getActivityDate() {
		return activityDate;
	}

	public String getGiorno() {
		return giorno;
	}

	public String getMese() {
		return mese;
	}

	public String getAnno() {
		return anno;
	}

	public String getTotalPointsSign() {
		return totalPointsSign;
	}

	public String getTotalPointsAbsoluteValue() {
		return totalPointsAbsoluteValue;
	}
	
	public String getDescription() {
		return description;
	}

	public String getActivityClass() {
		return activityClass;
	}

}
