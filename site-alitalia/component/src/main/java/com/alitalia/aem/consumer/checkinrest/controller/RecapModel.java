package com.alitalia.aem.consumer.checkinrest.controller;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.alitalia.aem.web.component.webcheckin.delegaterest.IInsuranceDelegate;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = { SlingHttpServletRequest.class })
public class RecapModel extends GenericCheckinModel{

	@Self
	private SlingHttpServletRequest request;

	@Inject
	private SlingHttpServletResponse response;

	@Inject
	private AlitaliaConfigurationHolder configuration;

	@Inject
	private CheckinSession checkinSession;

	@Inject
	private volatile ICheckinDelegate checkInDelegateRest;

	@Inject
	private volatile IInsuranceDelegate checkInDelegateInsuranceRest;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private List<com.alitalia.aem.common.data.checkinrest.model.boardingpass.Model> models= new ArrayList();

	private List<String> receivers = new ArrayList<>();
	@PostConstruct
	protected void initModel() throws Exception {
		try{
			super.initBaseModel(request);

			if (ctx == null) {
				logger.debug("[AncillaryOffersRenderModel] - Contesto assente.");
				return;
			}
			else {

				setModels(ctx.boardingPassInfo.getModels());
				setReceivers();
			}
		}
		catch(Exception ex){
			logger.error("[AncillaryOffersRenderModel] - Unexpected error: ", ex);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	public List<com.alitalia.aem.common.data.checkinrest.model.boardingpass.Model> getModels() {
		return models;
	}

	public void setModels(List<com.alitalia.aem.common.data.checkinrest.model.boardingpass.Model> models) {
		this.models = models;
	}

	public List<String> getReceivers() {
		return receivers;
	}

	public void setReceivers() {
		for(int i = 0;i<this.models.size();i++){
			if(!this.receivers.contains(this.models.get(i).getPassengerName())) {
				this.receivers.add(this.models.get(i).getPassengerName());
			}
		}
	}
}
