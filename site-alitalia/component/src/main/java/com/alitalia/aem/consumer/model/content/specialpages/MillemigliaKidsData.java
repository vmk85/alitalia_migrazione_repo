package com.alitalia.aem.consumer.model.content.specialpages;

public class MillemigliaKidsData {

	public static final String NAME = "MillemigliaKidsData";

	private String error;
	
	private String cod_millemiglia;
	private String parent_name;
	private String parent_surname;
	private String email;
	private String child_name;
	private String child_surname;
	private String gender;
	private Integer day;
	private Integer month;
	private Integer year;
	private String accettocond;

	public String getError() {
		return error;
	}
	
	public void setError(String error){
		this.error=error;
	}
	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}
	
	public void setYear(Integer year) {
		this.year = year;
	}


	public String getCod_millemiglia() {
		return cod_millemiglia;
	}



	public void setCod_millemiglia(String cod_millemiglia) {
		this.cod_millemiglia = cod_millemiglia;
	}



	public String getParent_surname() {
		return parent_surname;
	}



	public void setParent_surname(String parent_surname) {
		this.parent_surname = parent_surname;
	}



	public String getParent_name() {
		return parent_name;
	}



	public void setParent_name(String parent_name) {
		this.parent_name = parent_name;
	}



	public String getChild_name() {
		return child_name;
	}



	public void setChild_name(String child_name) {
		this.child_name = child_name;
	}



	public String getChild_surname() {
		return child_surname;
	}



	public void setChild_surname(String child_surname) {
		this.child_surname = child_surname;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public void setEmail(String email){
		this.email=email;
	}
	
	public String getEmail(){
		return email;
	}

	
	public String getAccettocond() {
		return accettocond;
	}
	public void setAccettocond(String accettocond) {
		this.accettocond = accettocond;
	}
	@Override
	public String toString() {
		return "MillemigliaKidsData [parent_name=" + parent_name + ", parent_surname=" + parent_surname +
				", child_name=" + child_name + ", email=" + email +
				", child_surname="+child_surname+"gender=" + gender + 
				", day=" + day + ", month=" + month + ", year=" + year + "]";
	}



}
