package com.alitalia.aem.consumer.utils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.octo.captcha.service.CaptchaServiceException;

public class ValidationUtils {
	
	private static Logger logger = LoggerFactory.getLogger(ValidationUtils.class);
	
	/**
	 * Check if a generic condition is true foreach flightData
	 * @param ctx Booking Session Context that contains the flight seletion
	 * @param pr the condition that will be check for all flights
	 * @return true if condition is true for each flight
	 */
	public static boolean checkAllFlights(List<FlightData> flights, Predicate<FlightData> pr){
		return flights.stream().allMatch(pr);
	}
	
	
	/**
	 * Check if a generic condition is true foreach flightSelection in BookingSessionContext
	 * @param ctx Booking Session Context that contains the flight seletion
	 * @param pr the condition that will be check for all flights
	 * @return true if condition is true for each flight
	 */
	public static boolean checkAllFlights(BookingSessionContext ctx, Predicate<FlightSelection> pr){
		return Arrays.asList(ctx.flightSelections).stream().allMatch(pr);
	}
	
	/**
	 * 
	 * @return the predicate that check if a flight is selected
	 */
	public static Predicate<FlightSelection> isFlightSelected(){
		return f -> f != null;
	}
	
	/**
	 * 
	 * @return the predicate that check if a flight is selected and its brand is different from light
	 */
	public static Predicate<FlightSelection> isNotLight(){
		return f -> f != null && !f.isTariffaLight();
	}
	
	/**
	 * 
	 * @param carriers 
	 * @return the predicate that check if a flight is selected and its carrier belong to given carriers set
	 */
	public static Predicate<FlightSelection> isSpecificCarrier(List <String> carriers){
		return f -> {
			boolean carrierCond;
			if(f.getFlightData().getFlightType() == FlightTypeEnum.DIRECT){
				DirectFlightData directFlight = ((DirectFlightData) f.getFlightData());
				if (directFlight.getOperationalCarrier() != null && !directFlight.getOperationalCarrier().equals("")) {
					carrierCond = carriers.contains(directFlight.getOperationalCarrier().toUpperCase());
				} else {
					carrierCond = carriers.contains(((DirectFlightData) f.getFlightData()).getCarrier().toUpperCase());
				}
			}
			else{
				carrierCond = true;
				ConnectingFlightData connFlight = ((ConnectingFlightData) f.getFlightData());
				for(FlightData flight : connFlight.getFlights()){
					DirectFlightData directFlight = ((DirectFlightData) flight);
					if (directFlight.getOperationalCarrier() != null && !directFlight.getOperationalCarrier().equals("")) {
						carrierCond = carrierCond && carriers.contains(directFlight.getOperationalCarrier().toUpperCase());
					} else {
						carrierCond = carrierCond && carriers.contains(directFlight.getCarrier().toUpperCase());
					}
				}
			}
			return f != null && carrierCond;
		};
	}
	
	
	/**
	 * 
	 * @return the predicate that check if a flight is not direct
	 */
	public static Predicate<FlightData> isDirect(){
		return f -> f!= null && f.getFlightType() == FlightTypeEnum.DIRECT;
	}
	
	/**
	 * 
	 * @return the predicate that check if a flight is not direct
	 */
	public static Predicate<FlightData> isConnecting(){
		return f -> f!= null && f.getFlightType() == FlightTypeEnum.CONNECTING;
	}
	
	public static Boolean isCaptchaValid(String jcaptchaResponse, String captchaId){
		logger.info("INIZIO isCaptchaValid in ValidationUtils");
		Boolean isResponseCorrect = Boolean.FALSE;
		logger.debug("Executing isCaptchaValid: captchaId[{}], jcaptchaResponse[{}]", captchaId, jcaptchaResponse);
		// Call the Service method
		try {
			if (captchaId != null && jcaptchaResponse != null){
				isResponseCorrect = CaptchaServiceSingleton.getInstance().validateResponseForID(captchaId, jcaptchaResponse);
				logger.info("isResponseCorrect = " + isResponseCorrect.toString());
			}
		} catch (CaptchaServiceException e) {
			// Should not happen, may be thrown if the id is not valid
			logger.error("Error while perform captcha validation. Not valid ID", e);
		}
		logger.info("FINE isCaptchaValid in ValidationUtils");
		return isResponseCorrect;
	}
	
	/**
	 * @deprecated  It was used to validate login captcha using JCaptcha. Now Re-Captcha is used </br>
	 * {@link #isRecaptchaLoginValid(String secretkey, String recaptchaResponse, String userIp)} instead like this: 
	 * @param jcaptchaResponse
	 * @param captchaId
	 * @return
	 */
	@Deprecated()
	public static Boolean isCaptchaLoginValid(String jcaptchaResponse, String captchaId){
		logger.info("INIZIO isCaptchaLoginValid in ValidationUtils");
		Boolean isResponseCorrect = Boolean.FALSE;
		logger.debug("Executing isCaptchaLoginValid: captchaId[{}], jcaptchaResponse[{}]", captchaId, jcaptchaResponse);
		// Call the Service method
		try {
			if (captchaId != null && jcaptchaResponse != null){
				isResponseCorrect = CaptchaLoginServiceSingleton.getInstance().validateResponseForID(captchaId, jcaptchaResponse);
			}
		} catch (CaptchaServiceException e) {
			// Should not happen, may be thrown if the id is not valid
			logger.error("Error while perform captcha validation. Not valid ID", e);
		}
		logger.info("FINE isCaptchaLoginValid in ValidationUtils");
		return isResponseCorrect;
	}
	
	/**
	 * It is used to validate captcha of Google Re-captcha
	 * @param secretkey
	 * @param recaptchaResponse
	 * @param userIp
	 * @return
	 */
	public static boolean isRecaptchaLoginValid(String secretkey, String recaptchaResponse, String userIp) {
		logger.info("INIZIO isRecaptchaLoginValid in ValidationUtils");
		final String CHECK_HOSTNAME = "https://www.google.com/recaptcha/api/siteverify";
		String formRequest = "secret=" + secretkey
				+ "&response=" + recaptchaResponse;
		try {
			if (userIp != null && !userIp.isEmpty()) {
				formRequest += "&remoteip=" + userIp;
			}
			
			logger.info("Calling ReCaptcha validation to URL [{}] with formRequest [{}]", CHECK_HOSTNAME, formRequest);
			JSONObject jsonResponse = AlitaliaUtils.invokeHttpPostClient(CHECK_HOSTNAME, formRequest);
			logger.info("Called ReCaptcha validation. Response: [{}]", jsonResponse);

			
			if (jsonResponse.has("success")) {
				logger.info("return success: jsonResponse=" + jsonResponse);
				return jsonResponse.getBoolean("success");
			}else{
				logger.info("return fail: jsonResponse=" + jsonResponse);
			}
			
		} catch (Exception e) {
			logger.error("Error validating Re-Captcha", e);
			logger.info("FINE isRecaptchaLoginValid in ValidationUtils - EXCEPTION");
			return false;
		}
		logger.info("FINE isRecaptchaLoginValid in ValidationUtils");
		return false;
	}
}