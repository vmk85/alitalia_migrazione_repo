package com.alitalia.aem.consumer.booking.model.extrabaggage;

public class AncillaryExtraBaggage {
	private String bagaggeCode;
	private String bagaggeAmount;



	
	public String getBagaggeCode() {
		return bagaggeCode;
	}
	public void setBagaggeCode(String bagaggeCode) {
		this.bagaggeCode = bagaggeCode;
	}
	public String getBagaggeAmount() {
		return bagaggeAmount;
	}
	public void setBagaggeAmount(String bagaggeAmount) {
		this.bagaggeAmount = bagaggeAmount;
	}
	

	@Override
	public String toString() {
		return "AncillaryPassenger [bagaggeCode=" + bagaggeCode + ", bagaggeAmount=" + bagaggeAmount +"]";
	}
	
	

}
