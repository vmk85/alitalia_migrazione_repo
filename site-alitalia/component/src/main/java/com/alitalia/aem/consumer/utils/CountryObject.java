package com.alitalia.aem.consumer.utils;


import java.util.ArrayList;

public class CountryObject {

    private int countrySelected;
    private int languageSelected;
    private ArrayList<Country> countries;

    public CountryObject() {
        countries = new ArrayList<Country>();
        countrySelected = 0;
        languageSelected = 0;
    }

    public void setCountries(ArrayList<Country> countries) {
        this.countries = countries;
    }

    public void addCountry(Country country) {
        countries.add(country);
    }

    public void setCountrySelected(int countrySelected) {
        this.countrySelected = countrySelected;
    }

    public void setLanguageSelected(int languageSelected) {
        this.languageSelected = languageSelected;
    }

    public int getCountrySelected() {
        return countrySelected;
    }

    public int getLanguageSelected() {
        return languageSelected;
    }

    public String toJson() {
        String jsonString = "{";

        jsonString += "countrySelected: " + countrySelected + ", ";
        jsonString += "languageSelected: " + languageSelected + ", ";
        jsonString += "countries: [" + getCountriesJson() + "]";
       
        jsonString += "}";

        return jsonString;
    }

    private String getCountriesJson() {
        String jsonString = "";

        for(Country country : countries) {
            jsonString += country.toJson() + ",";
        }

        if (jsonString.length() > 0) {
        	jsonString = jsonString.substring(0, jsonString.length() - 1);
        }
        
        return jsonString;
    }

}
