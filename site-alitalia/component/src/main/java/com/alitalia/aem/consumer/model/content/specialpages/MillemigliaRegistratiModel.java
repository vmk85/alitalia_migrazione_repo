package com.alitalia.aem.consumer.model.content.specialpages;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MillemigliaRegistratiModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private MillemigliaRegistratiData millemigliaRegistratiData;
	private List<Integer> days = new ArrayList<Integer>();
	private List<Integer> months = new ArrayList<Integer>();
	private List<Integer> years = new ArrayList<Integer>();

	private boolean isYoung = false;

	@PostConstruct
	protected void initModel() {
		logger.debug("[MillemigliaRegistratiModel] initModel");

		super.initBaseModel(slingHttpServletRequest);
		
		try {
			// retrieve and remove BaggageClaimData from session if any
			millemigliaRegistratiData = (MillemigliaRegistratiData) 
				slingHttpServletRequest.getSession()
				.getAttribute(MillemigliaRegistratiData.NAME);
	
			// Init date controls
			for (int i = 1; i <= 31; i++) {
				days.add(i);
			}
	
			for (int i = 1; i <= 12; i++) {
				months.add(i);
			}
	
			Calendar c = new GregorianCalendar();
			int thisYear = c.get(Calendar.YEAR);
			for (int i = thisYear - 100; i <= thisYear - 14; i++) {
				years.add(i);
			}
	
			if (null != millemigliaRegistratiData &&
				isYoungRange(millemigliaRegistratiData.getYearOfBirth())) {
				isYoung = true;
			}
			
			String cod = slingHttpServletRequest.getParameter("cod");
			if(cod != null && !"".equals(cod)){
				if(millemigliaRegistratiData == null){
					millemigliaRegistratiData = new MillemigliaRegistratiData();
				}
				millemigliaRegistratiData.setTrackingCod(cod);
				request.getSession().setAttribute(MillemigliaRegistratiData.NAME,
						millemigliaRegistratiData);
				logger.debug("TrackingCode: " + cod);
			} else{
				logger.debug("TrackingCode null!");
			}
		} catch (Exception e) {
			logger.error("Unexpected error: {}", e);
			throw e;
		}
	}

	private boolean isYoungRange(String testYear) {
		if (null != testYear && !"".equals(testYear)) {
			int yearOfBirth = Integer.parseInt(testYear);
			int currentYear = Calendar.getInstance().get(Calendar.YEAR);
			return (currentYear - yearOfBirth >= 14
					&& currentYear - yearOfBirth <= 25);
		}
		return false;
	}

	public MillemigliaRegistratiData getMillemigliaRegistratiData() {
		return millemigliaRegistratiData;
	}

	public List<Integer> getDays() {
		return days;
	}

	public List<Integer> getMonths() {
		return months;
	}

	public List<Integer> getYears() {
		return years;
	}

	public Boolean getIsYoung() {
		return isYoung;
	}
	
	public boolean isYoung() {
		return isYoung;
	}
	

}
