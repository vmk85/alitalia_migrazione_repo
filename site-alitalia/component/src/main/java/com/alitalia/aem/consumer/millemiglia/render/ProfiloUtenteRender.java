package com.alitalia.aem.consumer.millemiglia.render;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.alitalia.aem.common.data.home.MMAddressData;
import com.alitalia.aem.common.data.home.MMContractData;
import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.MMPreferenceData;
import com.alitalia.aem.common.data.home.MMTelephoneData;
import com.alitalia.aem.common.data.home.enumerations.MMContractStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MMPhoneTypeEnum;
import com.alitalia.aem.consumer.global.enumeration.CreditCardCircuits;

public class ProfiloUtenteRender {

	public static final String MEAL_CATEGORY = "002";
	public static final String SEAT_CATEGORY = "003";
	
	private MMCustomerProfileData customerData;
	
	public ProfiloUtenteRender(MMCustomerProfileData customerData) {
		this.customerData = customerData;
	}
	
	public MMCustomerProfileData getCustomerData() {
		return this.customerData;
	}
	
	public String getBirthDate() {
		if (customerData.getBirthDate() != null) {
			DateRender dateRender = new DateRender(customerData.getBirthDate());
			return dateRender.getExtendedDate();	
		}
		return "";
	}
	
	public String getExpirationDate() {
		if (customerData.getBirthDate() != null) {
			DateRender dateRender = new DateRender(customerData.getTierDate());
			return dateRender.getExtendedDate();	
		}
		return "";
	}
	
	public MMTelephoneData getTelephone() {
		if (customerData.getTelephones() != null) {
			for (MMTelephoneData telephoneData : customerData.getTelephones()) {
				if (!telephoneData.getTelephoneType().equals(MMPhoneTypeEnum.EMAIL)) {
					return telephoneData;
				}
			}
		}
		return null;
	}
	
	public MMTelephoneData getEmail() {
		if (customerData.getTelephones() != null) {
			for (MMTelephoneData telephoneData : customerData.getTelephones()) {
				if (telephoneData.getTelephoneType().equals(MMPhoneTypeEnum.EMAIL)) {
					return telephoneData;
				}
			}
		}
		return null;
	}
	
	public MMAddressData getAddress() {
		if (customerData.getAddresses() != null) {
			for (MMAddressData addressData : customerData.getAddresses()) {
				if (addressData.getMailingIndicator() != null &&
						addressData.getMailingIndicator().equals("M")) {
					return addressData;	
				}
			}
		}
		return null;
	}
	
	public MMPreferenceData getMealPreferences() {
		if (customerData.getPreferences() != null) {
			for (MMPreferenceData preferenceData : customerData.getPreferences()) {
				if (preferenceData.getIdCategory().equals(MEAL_CATEGORY)) {
					return preferenceData;
				}
			}
		}
		return null;
	}
	
	public MMPreferenceData getSeatPreferences() {
		if (customerData.getPreferences() != null) {
			for (MMPreferenceData preferenceData : customerData.getPreferences()) {
				if (preferenceData.getIdCategory().equals(SEAT_CATEGORY)) {
					return preferenceData;
				}
			}
		}
		return null;
	}
	
	public boolean getIsMillemigliaYoung() {
		if(customerData.getContracts() != null){
			for (MMContractData contract : customerData.getContracts()) {
				if (contract != null && contract.getName() != null && contract.getStatus() != null && contract.getName().equalsIgnoreCase("YG") && 
						contract.getStatus() == MMContractStatusEnum.REGISTERED) {
					return true;
				}
			}
		}
		return false;
	}
	
	public CreditCardRender getCreditCard() {
		CreditCardRender creditCardRender = new CreditCardRender();
		if (customerData.getCreditCards() != null && customerData.getCreditCards().size() > 0) {
			MMCreditCardData creditCardData = null;
			for (MMCreditCardData creditCard : customerData.getCreditCards()) {
				if (CreditCardCircuits.listValues().contains(creditCard.getCircuitCode()) &&
						creditCard.getToken() != null &&
						!creditCard.getToken().equals("")) {
					creditCardData = creditCard;
					break;
				}
			}

			if (creditCardData == null) {
				return creditCardRender;
			}
			
			creditCardRender.setToken(creditCardData.getToken());
			creditCardRender.setCardType(creditCardData.getCardType().value());
			
			//Render Expiration date
			Calendar creditCardExpireDate = creditCardData.getExpireDate();
			if (creditCardExpireDate != null) {
				DateRender dateRender = new DateRender(creditCardExpireDate);
				if (creditCardExpireDate.after(getToday())) {
					creditCardRender.setExpireDate(dateRender.getMonth() + "/" + dateRender.getYear());
				} else {
					creditCardRender.setExpireDate("millemiglia.cartaScaduta.label");
				}
			}
			
			//Render credit card number
			String creditCardNumber= creditCardData.getNumber();
			String creditCardNumberHide = "";
			for (int i = 0; i < creditCardNumber.length() - 4; i++) {
				creditCardNumberHide = creditCardNumberHide + "X";
			}
			creditCardNumberHide = creditCardNumberHide + creditCardNumber.substring(creditCardNumber.length() - 4);
			creditCardRender.setNumber(creditCardNumberHide);
			
			//Render credit card circuit
			CreditCardCircuits creditCardCircuit = CreditCardCircuits.fromValue(creditCardData.getCircuitCode());
			if (creditCardCircuit != null) {
				creditCardRender.setCircuitCode(creditCardCircuit.getI18nValue());
			}
		}
		return creditCardRender;
	}

	/* Metodo privato che ritorna il calendar del giorno corrente */
	private Calendar getToday() {
		Calendar today = new GregorianCalendar();
	    today.set(Calendar.HOUR_OF_DAY, 0); //anything 0 - 23
	    today.set(Calendar.MINUTE, 0);
	    today.set(Calendar.SECOND, 0);
	    return today;
	}
	
}