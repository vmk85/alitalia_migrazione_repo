package com.alitalia.aem.consumer.millemiglia.exception;


public class MMCodeMismatchException extends Exception {

	private static final long serialVersionUID = 1L;

	public MMCodeMismatchException() {
			// TODO Auto-generated constructor stub
	}

	public MMCodeMismatchException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public MMCodeMismatchException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public MMCodeMismatchException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public MMCodeMismatchException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}
}