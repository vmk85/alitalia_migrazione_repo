package com.alitalia.aem.consumer.checkin.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.PaymentTypeItemData;
import com.alitalia.aem.consumer.mmb.render.MmbCreditCardRender;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinPayment extends GenericCheckinModel {

	@Self
	private SlingHttpServletRequest request;
	
	private MmbPriceRender cartTotalAmount;
	private List<MmbCreditCardRender> creditCards;
	private String paymentErrorMessage;
	
	private String ancillaryPage;
	
	@PostConstruct
	protected void initModel() throws IOException {
		
		initBaseModel(request);
		
		this.cartTotalAmount = new MmbPriceRender(
				ctx.cartTotalAmount, ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat); 
		this.paymentErrorMessage = ctx.paymentErrorMessage;
		this.creditCards = new ArrayList<MmbCreditCardRender>();
		for (PaymentTypeItemData paymentTypeItemData : ctx.creditCards) {
			this.creditCards.add(new MmbCreditCardRender(paymentTypeItemData));
		}
		ancillaryPage = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ configuration.getCheckinAncillaryPage();
		
	}

	public MmbPriceRender getCartTotalAmount() {
		return cartTotalAmount;
	}
	
	public String getPaymentErrorMessage() {
		return paymentErrorMessage;
	}
	
	public List<MmbCreditCardRender> getCreditCards() {
		return creditCards;
	}
	
	public String getAncillaryPage() {
		return ancillaryPage;
	}
}
