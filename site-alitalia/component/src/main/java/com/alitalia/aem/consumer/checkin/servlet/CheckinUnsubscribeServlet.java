package com.alitalia.aem.consumer.checkin.servlet;
import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.checkin.CheckinUserData;
import com.alitalia.aem.common.messages.home.WebCheckinAlertRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAlertResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionDataEmailAlert;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.web.component.webcheckin.delegate.WebCheckinDelegate;

/**
 * REST service to initialize the CheckInSession object in session for
 * the provided passenger name, surname and trip id.
 * 
 * Returns a JSON with outcome and redirect instructions.
 */

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checkinunsubscribe" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinUnsubscribeServlet extends GenericCheckinFormValidatorServlet{
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile CheckinSession checkInSession;
	
	@Reference
	private volatile WebCheckinDelegate checkInDelegate;
	
	
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		  
			ResultValidation resultValidation = new ResultValidation();
		    resultValidation.setResult(true);
		    return resultValidation;

	
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
		
		HttpSession session = request.getSession(true);
		CheckinSessionDataEmailAlert checkinPassenger=(CheckinSessionDataEmailAlert)session.getAttribute(CheckinConstants.CHECKIN_DATA_EMAIL_ALERT);
		CheckinUserData user=new CheckinUserData();
		
		user.setName(checkinPassenger.getName());
		user.setSurname(checkinPassenger.getSurname());
		user.setEmail(checkinPassenger.getEmail());
		user.setEnabled(checkinPassenger.isEnable());
		user.setMmcode(checkinPassenger.getMmcode());
		String tid = IDFactory.getTid();
		WebCheckinAlertRequest requestAlert= new WebCheckinAlertRequest(tid, CheckinConstants.CHECKIN_SID);
		requestAlert.setUser(user);
		
		
		WebCheckinAlertResponse responseAlert=checkInDelegate.submitUser(requestAlert);
		String redirecturlUnsub=AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)+getConfiguration().getCheckinDeregistrationPage(true);
		redirecturlUnsub=request.getResourceResolver().map(redirecturlUnsub)+"?unsubscribeSuccess=true";
		response.sendRedirect(redirecturlUnsub);
	}

}
