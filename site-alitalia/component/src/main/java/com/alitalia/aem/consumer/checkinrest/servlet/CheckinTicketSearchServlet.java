package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "searchbyticket"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinTicketSearchServlet extends GenericCheckinFormValidatorServlet  {

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
//	// logger
//	private Logger logger = LoggerFactory.getLogger(this.getClass());
//
//	@Reference
//	private AlitaliaConfigurationHolder configuration;
//	
////	@Reference
////	private CheckinSession checkinSession;
//	
//	@Override
//	protected AlitaliaConfigurationHolder getConfiguration() {
//		return configuration;
//	}
//
//	@Override
//	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
//		Validator validator = new Validator();
//		
//		String ticket = request.getParameter("ticket");
//		String lastName = request.getParameter("lastName");
//		
//		validator.addDirectCondition("ticket", ticket, 
//				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
//		
//		validator.addDirectCondition("lastName", lastName, 
//				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
//		
//		validator.addDirectCondition("lastName", lastName, 
//				CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
//		
//		ResultValidation resultValidation = validator.validate();
//		return resultValidation;
//	}
//
//	@Override
//	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
//		
//		CheckinSession checkinSession = new CheckinSession();
//		
//		boolean resultSession = initSession(request, configuration, checkinSession);
//		
//		if (!resultSession){
//			logger.error("Errore durante l'inizializzazione della sessione.");
//			throw new Exception("Errore durante l'inizializzazione della sessione.");
//		}else{
//			CheckinSessionContext ctx = getCheckInSessionContext(request);
//			
//			String errorUrl = AlitaliaUtils.findSiteBaseExternalUrl(
//					request.getResource(), false)
//					+ getConfiguration().getCheckinFailurePage();
//			
//			errorUrl = request.getResourceResolver().map(errorUrl);
//			
//			String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
//					request.getResource(), false)
//					+ getConfiguration().getCheckinPassengersPage();
//			
//			successPage = request.getResourceResolver().map(successPage);
//			
//			boolean result = checkinSession.checkinTicketSearch(request, ctx);
//			
//			if (result) {
//				//response.sendRedirect(successPage);
//				TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
//				response.setContentType("application/json");
//				json.object();
//				json.key("isError").value(false);
//				json.key("successPage").value(successPage);
//				json.endObject();
//			} else {
//				//TODO - redirect to errorPage?
//				TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
//				response.setContentType("application/json");
//				json.object();
//				json.key("isError").value(true);
//				json.endObject();
//			}
//		}
//	}



}
