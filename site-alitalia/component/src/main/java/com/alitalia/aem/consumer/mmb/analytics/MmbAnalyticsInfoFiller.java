package com.alitalia.aem.consumer.mmb.analytics;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbCouponData;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;

public class MmbAnalyticsInfoFiller{
	
	private static final String ANC_NAME_INSURANCE = "INSURANCE";
	private static final String ANC_NAME_SALA_LOUNGE = "SALA LOUNGE";
	private static final String ANC_NAME_PASTO = "PASTO";
	private static final String ANC_NAME_FAST_TRACK = "FAST TRACK";
	private static final String ANC_NAME_BAGAGLIO_EXTRA = "BAGAGLIO EXTRA";
	private static final String ANC_NAME_UPGRADE = "UPGRADE";
	private static final String ANC_NAME_SCEGLI_IL_POSTO = "SCEGLI IL POSTO";
	private static final String ANCILLARY_PAGE_BOOKING_RECEIPT = "BookingReceipt";
	private static final String ANCILLARY_PAGE_MMB_PERSONALIZZA = "MMB";
	

	public MmbAnalyticsInfoFiller() {
		initFormats();
	}
	
	private void initFormats() {
	}
	
	
	public MmbAnalyticsInfo fillInfo(MmbSessionContext ctx, int step){
		MmbAnalyticsInfo info = new MmbAnalyticsInfo();
		info.step = step;
		switch (step){
		case 1:
			if(ctx.cart != null){
				info.ancillaryTypeProposed = computeAncillaryTypeProposed(ctx);
				info.ancillaryPage = ANCILLARY_PAGE_BOOKING_RECEIPT;
				info.ancillaryList = computeAncillaryList(ctx, AncillaryInfo.ONLY_NAME, MmbAncillaryStatusEnum.AVAILABLE);
			}
			break;
		case 2:
			if(ctx.cart != null){
				info.ancillaryTypeProposed = computeAncillaryTypeProposed(ctx);
				info.ancillaryPage = ANCILLARY_PAGE_MMB_PERSONALIZZA;
				info.ancillaryList = computeAncillaryList(ctx, AncillaryInfo.ONLY_NAME, MmbAncillaryStatusEnum.AVAILABLE);
			}
			info.route = computeRoute(ctx);
			info.travelType = computeTravelType(ctx);
			break;
		case 3:
			if(ctx.cart != null){
				info.ancillaryList = computeAncillaryList(ctx, AncillaryInfo.NAME_AND_QUANTITY, MmbAncillaryStatusEnum.TO_BE_ISSUED);
			}
			break;
		case 4:
			if(ctx.cart != null){
				info.ancillaryList = computeAncillaryList(ctx, AncillaryInfo.NAME_QUANTITY_AND_REVENUE, MmbAncillaryStatusEnum.ISSUED);
			}
			info.paymentType = "Credit Card";
			info.CCType = ctx.paymentCreditCardType;
			info.invoiceRequested = "0";
			info.PNR = ctx.pnr;
			info.tktNumber = computeTktNumberPurchased(ctx);
			MmbRouteData route = ctx.route;
			MmbFlightData depFlight = route.getFlights().get(0);
			info.depDate = depFlight.getDepartureDateTime();
			MmbFlightData retFlight = getFirstReturnFlight(route);
			if(retFlight != null ){
				info.retDate = retFlight.getDepartureDateTime();
			}
			break;
		}
	return info;
}
	
	private List<AncillaryInfo> computeAncillaryList(MmbSessionContext ctx
			, int completenessType, MmbAncillaryStatusEnum status) {
		List<AncillaryInfo> ancList = null;
		/*Compute Qt and revenue totals*/
		Map<AncillaryInfo,Integer> ancQt = new LinkedHashMap<AncillaryInfo, Integer>();
		Map<AncillaryInfo,BigDecimal> ancRevenue = new LinkedHashMap<AncillaryInfo, BigDecimal>();
		if(completenessType > AncillaryInfo.ONLY_NAME){
			for(MmbAncillaryData ancillary : ctx.cart.getAncillaries()){
				if(status.equals(ancillary.getAncillaryStatus())){
					AncillaryInfo convertedAnc = convertAncillary(ancillary, completenessType);
					Integer qt = ancQt.get(convertedAnc) != null ? ancQt.get(convertedAnc) : 0;
					ancQt.put(convertedAnc, qt + convertedAnc.ancQuantity);
					if(completenessType > AncillaryInfo.NAME_AND_QUANTITY){
						BigDecimal revenue = ancRevenue.get(convertedAnc) != null ? ancRevenue.get(convertedAnc): BigDecimal.ZERO;
						ancRevenue.put(convertedAnc, revenue.add(convertedAnc.ancRevenue));
					}
				}
			}
		}
		ancList = ctx.cart.getAncillaries().stream()
				.filter( anc -> status.equals(anc.getAncillaryStatus()))
				.map(anc -> convertAncillary(anc, completenessType))
				.distinct()
				.map(putInfoIntoAncillaryInfo(completenessType, ancQt, ancRevenue))
				.collect(Collectors.toList());
		/*Adding Insurance*/
		if(ctx.insurancePolicy != null &&
				((status.equals(MmbAncillaryStatusEnum.ISSUED) 
						&& ctx.insurancePaymentSuccess) 
				|| (status.equals(MmbAncillaryStatusEnum.TO_BE_ISSUED) 
					&& ctx.insuranceAddedToCart )
				|| (status.equals(MmbAncillaryStatusEnum.AVAILABLE) 
					&& ctx.insurancePolicy.getBuy() != null 
					&& !ctx.insurancePolicy.getBuy().booleanValue() ))){
			AncillaryInfo insurance = new AncillaryInfo();
			insurance.ancName = ANC_NAME_INSURANCE;
			if(completenessType > AncillaryInfo.ONLY_NAME){
				insurance.ancQuantity = computePassengersList(ctx).size();
				if(completenessType > AncillaryInfo.NAME_AND_QUANTITY){
					insurance.ancRevenue = ctx.insurancePolicy.getTotalInsuranceCost();
				}
			}
			ancList.add(insurance);
		}
		/*Adding free seats*/
		if(status.equals(MmbAncillaryStatusEnum.AVAILABLE)
				&& ancList.stream().noneMatch(anc -> ANC_NAME_SCEGLI_IL_POSTO.equals(anc.ancName))
				&& ctx.cart.getAncillaries().stream()
				.anyMatch(anc -> MmbAncillaryTypeEnum.SEAT.equals(anc.getAncillaryType()))){
			AncillaryInfo seat = new AncillaryInfo();
			seat.ancName = ANC_NAME_SCEGLI_IL_POSTO;
			ancList.add(seat);
		}
		return ancList;
	}
	
	private Function<AncillaryInfo,AncillaryInfo> putInfoIntoAncillaryInfo(int completenessType
			, Map<AncillaryInfo, Integer> ancQt, Map<AncillaryInfo, BigDecimal> ancRevenue){
		return anc -> {
			if(completenessType > AncillaryInfo.ONLY_NAME){
				anc.ancQuantity = ancQt.get(anc);
				if(completenessType > AncillaryInfo.NAME_AND_QUANTITY){
					anc.ancRevenue = ancRevenue.get(anc);
				}
			}
			return anc;
		};
	}
	
	private AncillaryInfo convertAncillary(MmbAncillaryData anc, int completenessType){
		AncillaryInfo ancillary = new AncillaryInfo();
		switch(anc.getAncillaryType()){
		case SEAT:
			ancillary.ancName = ANC_NAME_SCEGLI_IL_POSTO;
			break;
		case UPGRADE:
			ancillary.ancName = ANC_NAME_UPGRADE;
			break;
		case EXTRA_BAGGAGE:
			ancillary.ancName = ANC_NAME_BAGAGLIO_EXTRA;
			break;
		case FAST_TRACK:
			ancillary.ancName = ANC_NAME_FAST_TRACK;
			break;
		case MEAL:
			ancillary.ancName = ANC_NAME_PASTO;
			break;
		case VIP_LOUNGE:
			ancillary.ancName = ANC_NAME_SALA_LOUNGE;
			break;
		default:
		}
		if(completenessType > AncillaryInfo.ONLY_NAME){
			ancillary.ancQuantity = anc.getQuantity();
			if(completenessType > AncillaryInfo.NAME_AND_QUANTITY){
				ancillary.ancRevenue = anc.getAmount();
			}
		}
		return ancillary;
	}

	private String computeAncillaryTypeProposed(MmbSessionContext ctx) {
		String emdTypeProposed = "";
		boolean free = false;
		boolean pay = false;
		for(MmbAncillaryData anc : ctx.cart.getAncillaries()){
			if(MmbAncillaryTypeEnum.SEAT.equals(anc.getAncillaryType())){
				free = true;
			}
			if(anc.getQuotations() != null && anc.getQuotations().size() > 0){
				if(!BigDecimal.ZERO.equals(anc.getQuotations().get(0).getPrice())){
					pay = true;
				}
				else{
					free = true;
				}
			}
			else{
				free = true;
			}
		}
		if(ctx.insurancePolicy != null && ctx.insurancePolicy.getBuy() != null 
				&& !ctx.insurancePolicy.getBuy().booleanValue()){
			pay = true;
		}
		emdTypeProposed = (free ? "Free" : "") + (pay ? "Pay" : "");
		return emdTypeProposed;
	}


	private String computeTravelType(MmbSessionContext ctx) {
		String travelType = "";
		travelType = isRoundTrip(ctx.route) ? "RoundTrip": "OneWay";
		return travelType;
	}
	
	
	
	private boolean isRoundTrip(MmbRouteData routes){
		boolean isRoundTrip = true;
		isRoundTrip = isRoundTrip && hasAReturnFlight(routes);
		return isRoundTrip;
	}
	
	private boolean hasAReturnFlight(MmbRouteData routesList) {
		return routesList.getFlights().stream()
				.anyMatch(flight -> RouteTypeEnum.RETURN.equals(flight.getType()));
	}
	
	private MmbFlightData getFirstReturnFlight(MmbRouteData routesList) {
		return routesList.getFlights().stream()
				.filter(flight -> RouteTypeEnum.RETURN.equals(flight.getType()))
				.findFirst()
				.orElse(null);
	}

	private String computeTktNumberPurchased(MmbSessionContext ctx) {
		String tktNumbers = "";
		String sep = ",";
		/*If insurance is purchased, all the ticket are added*/
		if(ctx.insurancePaymentSuccess){
			tktNumbers = computePassengersList(ctx).stream()
					.map(MmbPassengerData::getCoupons)
					.flatMap(Collection::stream)
					.map(MmbCouponData::getEticket)
					.map(eTkt -> eTkt.split("C")[0])
					.distinct()
					.collect(Collectors.joining(sep));
		}
		else if(ctx.cart != null && ctx.cart.getAncillaries() != null 
				&& ctx.cart.getAncillaries().size() > 0){
			/*Search for Issued Ancillary*/
			List<MmbAncillaryStatusEnum> statuses = new LinkedList<MmbAncillaryStatusEnum>();
			statuses.add(MmbAncillaryStatusEnum.ISSUED);
			tktNumbers = ctx.cart.getAncillaries().stream().filter( anc 
					-> statuses.contains(anc.getAncillaryStatus()))
					.map(MmbAncillaryData::getCouponNumbers)
					.flatMap(Collection::stream)
					.map(couponNum -> couponNum.split("C")[0])
					.distinct()
					.collect(Collectors.joining(sep));
			
		}
		
		
		return tktNumbers;
	}
	
	
	private List<MmbPassengerData> computePassengersList(MmbSessionContext ctx){
		MmbFlightData flightData = ctx.route.getFlights().get(0);
		return flightData.getPassengers();
	}
	
	
	private String computeRoute(MmbSessionContext ctx){
		String route = "";
		String cityFrom = ctx.route.getFlights().get(0).getFrom().getCityCode();
		String cityTo = "";
		for(int i = 0; i < ctx.route.getFlights().size() 
				&& RouteTypeEnum.OUTBOUND.equals(ctx.route.getFlights().get(i).getType()); i++){
			cityTo = ctx.route.getFlights().get(i).getTo().getCityCode();
		}
		route = cityFrom + "-" + cityTo;
		return route;
	}
	
	
}
