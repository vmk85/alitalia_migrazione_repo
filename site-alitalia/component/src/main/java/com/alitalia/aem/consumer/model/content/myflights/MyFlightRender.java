package com.alitalia.aem.consumer.model.content.myflights;

public class MyFlightRender {
	private String carrier;
	private String flightNumber;
	private String cabin;
	private String departureCity;
	private String departureAirportCode;
	private String departureAirportTerminal;
	private String departureDay;
	private String departureMonth;
	private String departureYear;
	private String departureTime;
	private String arrivalCity;
	private String arrivalAirportCode;
	private String arrivalAirportTerminal;
	private String arrivalDay;
	private String arrivalMonth;
	private String arrivalYear;
	private String arrivalTime;
	
	
	
	
	public String getCarrier() {
		return carrier;
	}




	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}




	public String getFlightNumber() {
		return flightNumber;
	}




	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}




	public String getCabin() {
		return cabin;
	}




	public void setCabin(String cabin) {
		this.cabin = cabin;
	}




	public String getDepartureCity() {
		return departureCity;
	}




	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}




	public String getDepartureAirportCode() {
		return departureAirportCode;
	}




	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}




	public String getDepartureAirportTerminal() {
		return departureAirportTerminal;
	}




	public void setDepartureAirportTerminal(String departureAirportTerminal) {
		this.departureAirportTerminal = departureAirportTerminal;
	}




	public String getDepartureDay() {
		return departureDay;
	}




	public void setDepartureDay(String departureDay) {
		this.departureDay = departureDay;
	}




	public String getDepartureMonth() {
		return departureMonth;
	}




	public void setDepartureMonth(String departureMonth) {
		this.departureMonth = departureMonth;
	}




	public String getDepartureYear() {
		return departureYear;
	}




	public void setDepartureYear(String departureYear) {
		this.departureYear = departureYear;
	}




	public String getDepartureTime() {
		return departureTime;
	}




	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}




	public String getArrivalCity() {
		return arrivalCity;
	}




	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}




	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}




	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}




	public String getArrivalAirportTerminal() {
		return arrivalAirportTerminal;
	}




	public void setArrivalAirportTerminal(String arrivalAirportTerminal) {
		this.arrivalAirportTerminal = arrivalAirportTerminal;
	}




	public String getArrivalDay() {
		return arrivalDay;
	}




	public void setArrivalDay(String arrivalDay) {
		this.arrivalDay = arrivalDay;
	}




	public String getArrivalMonth() {
		return arrivalMonth;
	}




	public void setArrivalMonth(String arrivalMonth) {
		this.arrivalMonth = arrivalMonth;
	}




	public String getArrivalYear() {
		return arrivalYear;
	}




	public void setArrivalYear(String arrivalYear) {
		this.arrivalYear = arrivalYear;
	}




	public String getArrivalTime() {
		return arrivalTime;
	}




	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}




	@Override
	public String toString() {
		return "MyFlightRender [carrier=" + carrier + ", flightNumber="
				+ flightNumber + ", cabin=" + cabin + ", departureCity="
				+ departureCity + ", departureAirportCode="
				+ departureAirportCode + ", departureAirportTerminal="
				+ departureAirportTerminal + ", departureDay=" + departureDay
				+ ", departureMonth=" + departureMonth + ", departureYear="
				+ departureYear + ", departureTime=" + departureTime
				+ ", arrivalCity=" + arrivalCity + ", arrivalAirportCode="
				+ arrivalAirportCode + ", arrivalAirportTerminal="
				+ arrivalAirportTerminal + ", arrivalDay=" + arrivalDay
				+ ", arrivalMonth=" + arrivalMonth + ", arrivalYear="
				+ arrivalYear + ", arrivalTime=" + arrivalTime + "]";
	}
	
}
