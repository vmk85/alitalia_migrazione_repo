package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.tripsearch.Item;
import com.alitalia.aem.common.data.checkinrest.model.tripsearch.Error;
import com.alitalia.aem.common.data.checkinrest.model.tripsearch.Trip;
import com.alitalia.aem.common.data.checkinrest.model.tripsearch.Warning;
import com.alitalia.aem.common.messages.checkinrest.CheckinTripSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinTripSearchResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkintripsearch"}),   //TODO - corretto?
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class CheckinTripSearchServlet extends SlingSafeMethodsServlet{
	
	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	// Search Checkin Trip Delegate
	@Reference
	private ICheckinDelegate checkinDelegate;
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		// content type
		response.setContentType("application/json");

		// JSON response
		TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());
		
		try{
			//TODO - gestire il language ?
			// prepare resource bundle for i18n
			Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
			ResourceBundle resourceBundle = request.getResourceBundle(locale);
			final I18n i18n = new I18n(resourceBundle);
			
			// invoco il delegate
			CheckinTripSearchRequest serviceRequest = new CheckinTripSearchRequest(IDFactory.getTid(), IDFactory.getSid(request));
			serviceRequest.setFirstName(request.getParameter("firstName"));
			serviceRequest.setLastName(request.getParameter("lastName"));
			serviceRequest.setTicket(request.getParameter("ticket"));
			serviceRequest.setPnr(request.getParameter("pnr"));
			serviceRequest.setFrequentFlyer(request.getParameter("frequentFlyer"));
			CheckinTripSearchResponse serviceResponse = checkinDelegate.retrieveCheckinTrip(serviceRequest);
			
			Trip trip = serviceResponse.getTripData();
			
			// start JSON response
			jsonOutput.object();
			jsonOutput.array();
			List<Item> itemList = serviceResponse.getTripData().getItems();
			jsonOutput.key("items").value("");
			jsonOutput.endArray(); //TODO - iterare la lista... vuoto?
			
			jsonOutput.key("success").value(trip.getSuccess());
			
			jsonOutput.array();
			List<Warning> warningList = serviceResponse.getTripData().getWarnings();
			jsonOutput.key("warnings");
			for(Warning warning : warningList){
				jsonOutput.object();
				jsonOutput.key("shortText").value(warning.getShortText());
				jsonOutput.key("value").value(warning.getValue());
				jsonOutput.endObject();
			}
			jsonOutput.endArray();
			
			jsonOutput.array();
			List<Error> errorList = serviceResponse.getTripData().getErrors();
			jsonOutput.key("errors");
			for(Error error : errorList){
				jsonOutput.object();
				jsonOutput.key("errorInfo").value(error.getErrorInfo());
				jsonOutput.key("errorCode").value(error.getErrorCode());
				jsonOutput.key("severity").value(error.getSeverity());
				jsonOutput.key("errorMessage").value(error.getErrorMessage());
				jsonOutput.endObject();
			}
			jsonOutput.endArray();
			
			jsonOutput.key("conversationId").value(trip.getConversationId());
			jsonOutput.key("compression").value(trip.getCompression());
			jsonOutput.key("echoToken").value(trip.getEchoToken());
			jsonOutput.key("timeStamp").value(trip.getTimeStamp());
			jsonOutput.key("timeStampSpecified").value(trip.getTimeStampSpecified());
			jsonOutput.key("target").value(trip.getTarget());
			jsonOutput.key("version").value(trip.getVersion());
			jsonOutput.key("sequenceNmbr").value(trip.getSequenceNmbr());
			jsonOutput.key("primaryLangID").value(trip.getPrimaryLangID());
			jsonOutput.key("altLangID").value(trip.getAltLangID());
			jsonOutput.key("returnContent").value(trip.getReturnContent());
			
			// end JSON response
			jsonOutput.endObject();
		}
		
		catch(Exception e){
			logger.error(e.getMessage());
			throw new RuntimeException("[CheckinTripSearchServlet] - Errore durante la ricerca dei dati di viaggio.", e);
		}
	}
}
