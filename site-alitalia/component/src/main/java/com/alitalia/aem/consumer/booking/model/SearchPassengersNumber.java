package com.alitalia.aem.consumer.booking.model;

public class SearchPassengersNumber {
	
	private int numAdults;
	private int numChildren;
	private int numInfants;
	private int numYoung;

	public SearchPassengersNumber() {
		this.numAdults = 0;
		this.numChildren = 0;
		this.numInfants = 0;
		this.numYoung = 0;
	}
	
	public SearchPassengersNumber(int numAdults, int numChildren, int numInfants, int numYoung) {
		this.numAdults = numAdults;
		this.numChildren = numChildren;
		this.numInfants = numInfants;
		this.numYoung = numYoung;
	}

	public int getNumYoung() {
		return numYoung;
	}

	public void setNumYoung(int numYoung) {
		this.numYoung = numYoung;
	}

	public int getNumAdults() {
		return numAdults;
	}

	public void setNumAdults(int numAdults) {
		this.numAdults = numAdults;
	}

	public int getNumChildren() {
		return numChildren;
	}

	public void setNumChildren(int numChildren) {
		this.numChildren = numChildren;
	}

	public int getNumInfants() {
		return numInfants;
	}

	public void setNumInfants(int numInfants) {
		this.numInfants = numInfants;
	}

	@Override
	public String toString() {
		return "SearchPassengersNumber [numAdults=" + numAdults + ", numChildren=" + numChildren + ", numInfants="
				+ numInfants + ", numYoung=" + numYoung + "]";
	}

}
