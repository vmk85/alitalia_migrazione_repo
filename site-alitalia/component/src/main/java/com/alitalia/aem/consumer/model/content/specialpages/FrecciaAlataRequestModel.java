package com.alitalia.aem.consumer.model.content.specialpages;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class FrecciaAlataRequestModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private FrecciaAlataRequestData frecciaAlataRequestData;
	private String conditionsacceptedLabel;


	@PostConstruct
	protected void initModel() {
		logger.debug("[FrecciaAlataRequestModel] initModel");

		super.initBaseModel(slingHttpServletRequest);

		// retrieve and remove FrecciaAlataRequestData from session if any

		frecciaAlataRequestData = (FrecciaAlataRequestData)  slingHttpServletRequest.getSession().getAttribute(FrecciaAlataRequestData.NAME);
		slingHttpServletRequest.getSession().removeAttribute(FrecciaAlataRequestData.NAME);
		
		conditionsacceptedLabel = "Con la compilazione dei dati, il cliente accetta di partecipare all'iniziativa e di ricevere e-mail Alitalia.";

	}

	public FrecciaAlataRequestData getFrecciaAlataRequestData() {
		return frecciaAlataRequestData;
	}
	
	public String getConditionsacceptedLabel() {
		return conditionsacceptedLabel;
	}

}
