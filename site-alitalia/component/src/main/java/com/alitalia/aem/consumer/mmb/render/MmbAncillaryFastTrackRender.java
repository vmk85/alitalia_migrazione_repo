package com.alitalia.aem.consumer.mmb.render;

import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;

public class MmbAncillaryFastTrackRender {
	
	private String identifier = "";
	private MmbAncillariesGroup ancillariesGroup = null;
	private String routeType = null;
	private Integer multilegIndex = null;
	private MmbPriceRender price = null;
	private String routeTypeLabel = "";
	private boolean allowModify = false;
	private boolean currentValue = false;
	private MmbFlightDataRender flight = null;
	
	public MmbAncillaryFastTrackRender(MmbAncillariesGroup ancillariesGroup) {
		super();
		this.ancillariesGroup = ancillariesGroup;
	}

	public String getIdentifier() {
		return this.identifier;
	}
	
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	public MmbAncillariesGroup getAncillariesGroup() {
		return ancillariesGroup;
	}
	
	public boolean isAllowModify() {
		return allowModify;
	}

	public void setAllowModify(boolean allowModify) {
		this.allowModify = allowModify;
	}

	public boolean getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(boolean currentValue) {
		this.currentValue = currentValue;
	}

	public String getRouteType() {
		return routeType;
	}

	public void setRouteType(String routeType) {
		this.routeType = routeType;
	}

	public Integer getMultilegIndex() {
		return multilegIndex;
	}

	public void setMultilegIndex(Integer multilegIndex) {
		this.multilegIndex = multilegIndex;
	}

	public MmbPriceRender getPrice() {
		return price;
	}

	public void setPrice(MmbPriceRender price) {
		this.price = price;
	}

	public String getRouteTypeLabel() {
		return routeTypeLabel;
	}

	public void setRouteTypeLabel(String routeTypeLabel) {
		this.routeTypeLabel = routeTypeLabel;
	}

	public MmbFlightDataRender getFlight() {
		return this.flight;
	}
	
	public void setFlight(MmbFlightDataRender flight) {
		this.flight = flight;
	}
	
}
