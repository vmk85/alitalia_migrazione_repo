package com.alitalia.aem.consumer.booking.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSearchCategoryEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.consumer.booking.render.BrandRender;
import com.alitalia.aem.consumer.booking.render.SelectedFlightInfoRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingScegliVoloTravelInfo extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private BookingSession bookingSession;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private SelectedFlightInfoRender selectedFlightInfo;
	private Map<String, Map<String,String>> fareRuleBrands;
	private Map<String, String> listBrandsMultiSlice;
	private int indexRoute;
	private int indexFlight;
	private int indexBrand;
	private List<BrandPageData> brandList;
	private List<String> fullBrandList;
	private String activeBrand;
	private boolean isBusiness;
	private boolean multileg;
	private boolean carnet;
	private boolean busCarrier;
	private boolean fareRulesStatic;
	
	private int brandEconomyNumber;
	private boolean isPromo;
	
	private boolean isDomestic;

	private boolean showFareRules;
	
	private Map<String, Integer> seatAvailMap;
	private boolean isRTL;

	private List<String> changeBefore = new ArrayList();

	private List<String> changeAfter =  new ArrayList();

	private List<String> refundBefore =  new ArrayList();

	private List<String> refundAfter =  new ArrayList();

	private List<Boolean> firstLabel = new ArrayList<>();

	private List<Boolean> secondLabel = new ArrayList<>();

	private List<Boolean> genericLabel = new ArrayList<>();

	private boolean firstLabelPopup = false;

	private boolean secondLabelPopup = false;

	private boolean genericLabelPopup = false;

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			
			initBaseModel(request);

			seatAvailMap = new LinkedHashMap<String, Integer>();
			if (ctx != null) {
				carnet = ctx.isCarnetProcess;
				isDomestic = ctx.searchCategory.name().equals("DOM") ? true : (ctx.searchCategory.name().equals("FCO_LIN") ? true : false);
				showFareRules = true;
				fareRulesStatic = configuration.isFareRulesStaticOn();
				if( ( request.getParameter("indexRoute")==null || ("").equals(request.getParameter("indexRoute")) ) || ( !("1").equals(request.getParameter("indexRoute")) && !("0").equals(request.getParameter("indexRoute")) ) ){
					logger.error("indexRoute not found");
					throw new RuntimeException();		
				}
				indexRoute = Integer.parseInt(request.getParameter("indexRoute"));
				indexFlight = Integer.parseInt(request.getParameter("indexFlight"));
				boolean popup = request.getParameter("popup") != null;
				
				if (request.getParameter("indexBrand") != null) {
					indexBrand = Integer.parseInt(request.getParameter("indexBrand"));
				} else {
					indexBrand = -1;
				}
				logger.debug("indexBrand: " +  indexBrand);
				
				isBusiness = Boolean.parseBoolean(request.getParameter("isBusiness"));
	
	//			int searchElementIndex;
	//			if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
	//				showFareRules = false;
	//				searchElementIndex = ctx.currentSliceIndex - 1;
	//			} else {
	//				searchElementIndex = indexRoute;
	//			}
				
				brandEconomyNumber = bookingSession.computeBrandEconomySize(ctx.searchCategory);
				isPromo = ctx.searchCategory == BookingSearchCategoryEnum.DOM_PROMO ||
						ctx.searchCategory == BookingSearchCategoryEnum.INTZ_PROMO ||
						ctx.searchCategory == BookingSearchCategoryEnum.INTC_PROMO ||
						ctx.searchCategory == BookingSearchCategoryEnum.INTZ_PREMIUM;
				
				FlightData selectedflightData = bookingSession.searchSelectionFlightData(ctx, indexRoute, indexFlight);

				Boolean isSoggettoAppGov=computeIsSoggettoAppGov(obtainFlightNumbers(selectedflightData));
				selectedflightData.setIsSoggettoApprovazioneGovernativa(isSoggettoAppGov);

				this.busCarrier = computeSelectedBus(selectedflightData);
				List<BrandData> brandDataList = selectedflightData.getBrands();
				brandList = new ArrayList<BrandPageData>();
				for (BrandData brandData : brandDataList) {
					BrandPageData page = ctx.codeBrandMap.get(brandData.getCode().toLowerCase());
					if (page != null) {
						brandList.add(page);
					}
				}
	
				selectedFlightInfo = new SelectedFlightInfoRender(selectedflightData,i18n , ctx.market);
				if(ctx.searchKind != BookingSearchKindEnum.MULTILEG && !carnet){
					multileg = false;
					Map<BrandPageData, Map<String,String>> tempFareRulesList = bookingSession.computeFareRuleBrandsByFlight(indexRoute,indexFlight,ctx);
					setNotes(tempFareRulesList);
					setNotesForPopup();
					SetAsterisck(tempFareRulesList);
 					fareRuleBrands = setFareRulesForAllBrands(tempFareRulesList, popup);
					setAllPenaltiesForType(tempFareRulesList);
					fullBrandList = new ArrayList<String>();
					for (String brandId : fareRuleBrands.keySet()) {
						fullBrandList.add(brandId);
						seatAvailMap.put(brandId, getSeatAvailability(selectedFlightInfo, brandId));
					}
				} else if(ctx.searchKind == BookingSearchKindEnum.MULTILEG || carnet){
					multileg = true;
					Map<BrandPageData, Map<String,String>> tempFareRulesList = bookingSession.computeFareRuleBrandsByFlight(indexRoute,indexFlight,ctx);
					setNotes(tempFareRulesList);
					setNotesForPopup();
					SetAsterisck(tempFareRulesList);
					fareRuleBrands = setFareRulesForAllBrands(tempFareRulesList, popup);
					setAllPenaltiesForType(tempFareRulesList);
					for (String brandId : fareRuleBrands.keySet()) {
						seatAvailMap.put(brandId, getSeatAvailability(selectedFlightInfo, brandId));
					}

					listBrandsMultiSlice = new LinkedHashMap<String,String>();
					boolean found = false;
					for (String brandId : fareRuleBrands.keySet()) {
						for(BrandPageData brandPageData : brandList){
							if(!found && brandPageData.getId().equals(brandId)){
								listBrandsMultiSlice.put(brandId, brandPageData.getPath());
								found = true;
								break;
							}
						}
						if(!found){
							listBrandsMultiSlice.put(brandId, "empty");
						} 
						found = false;
					}
				}
			}
			isRTL = "true".equals(AlitaliaUtils.getInheritedProperty(request.getResource(), "rtl", String.class));
		} catch (Exception e) {
			// true per essere visualizzato nell'author
			isPromo = true;
			logger.error("Unexpected error ", e);
		}
	}
	
	private Integer getSeatAvailability(SelectedFlightInfoRender selectedFlightInfo, String brandId) {
		Integer seatAvailable = null;
		for(BrandData brand: selectedFlightInfo.getFlightData().getBrands()){
			BrandPageData brandData = ctx.codeBrandMap.get((brand.getCode().toLowerCase()));
			if(brandData != null && brandId.equals(brandData.getId())){
				seatAvailable = brand.getSeatsAvailable();
			}
		}
		return seatAvailable;
	}

	public int getIndexRoute() {
		return indexRoute;
	}
	
	public int getIndexFlight() {
		return indexFlight;
	}
	
	public int getIndexBrand() {
		return indexBrand;
	}

	public SelectedFlightInfoRender getSelectedFlightInfo() {
		return selectedFlightInfo;
	}
	
	public Map<String, Map<String, String>> getFareRuleBrands() {
		return fareRuleBrands;
	}

	public boolean getShowFareRules() {
		return showFareRules;
	}
	
	public List<BrandPageData> getBrandList() {
		return brandList;
	}
	
	public int getBrandEconomyNumber() {
		return brandEconomyNumber;
	}
	
	public boolean isPromo() {
		return isPromo;
	}

	public boolean isDomestic(){
		return isDomestic;
	}

	public String getActiveBrand() {
		return activeBrand;
	}
	
	public boolean isBusiness() {
		return isBusiness;
	}
	
	public List<String> getFullBrandList() {
		return fullBrandList;
	}
	
	public boolean isMultileg() {
		return multileg;
	}
	
	public boolean isCarnet() {
		return carnet;
	}

	public void setMultileg(boolean multileg) {
		this.multileg = multileg;
	}
	
	public Map<String, String> getListBrandsMultiSlice() {
		return listBrandsMultiSlice;
	}

	public void setListBrandsMultiSlice(Map<String, String> listBrandsMultiSlice) {
		this.listBrandsMultiSlice = listBrandsMultiSlice;
	}
	
	public boolean isBusCarrier() {
		return busCarrier;
	}
	
	public boolean isFareRulesStatic() {
		return fareRulesStatic; 
	}

	public List<String> getChangeBefore() {
		return changeBefore;
	}

	public List<String> getChangeAfter() {
		return changeAfter;
	}

	public List<String> getRefundBefore() {
		return refundBefore;
	}

	public List<String> getRefundAfter() {
		return refundAfter;
	}

	public List<Boolean> getFirstLabel() {
		return firstLabel;
	}

	public List<Boolean> getSecondLabel() {
		return secondLabel;
	}

	public boolean isFirstLabelPopup() {
		return firstLabelPopup;
	}

	public boolean isSecondLabelPopup() {
		return secondLabelPopup;
	}

	/* private methods */
	
	private Map<String, Map<String, String>> setFareRulesForAllBrands(Map<BrandPageData, Map<String, String>> tempFareRulesList, boolean popup) {
		Map<String, Map<String, String>> fareRulesList = new LinkedHashMap<String, Map<String,String>>();
		Set<BrandPageData> keySet = tempFareRulesList.keySet();
		int i = 0;
		for (BrandPageData page : keySet) {
			Map<String,String> brandFareRule = tempFareRulesList.get(page);
			
			Map<String,String> newFareRule;
			ArrayList<BrandRender> currentMissedBrand = ctx.missedBrandsMap.get(page.getId());
			
			if (currentMissedBrand != null) {
				for (BrandRender brandRender : currentMissedBrand) {
					newFareRule = new LinkedHashMap<String, String>();
					newFareRule.put(brandRender.getId(), "empty");
					fareRulesList.put(brandRender.getId(), newFareRule);
				}
			}
		
			newFareRule = new LinkedHashMap<String, String>();
			String cssClass = popup ? "compareTariff__txt" : "elementList__subtitle";
			for (Entry<String,String> entry : brandFareRule.entrySet()) {
				if (!entry.getValue().equals("false")) {
					String newK, newV;
					newK = obtainCSSClassByFareRule(entry.getKey());
					if (entry.getKey().contains("cambioPrenotazione")) {
						if (entry.getKey().equals("booking.regoleTariffarie.cambioPrenotazionePrimaPartenza") ) {
							newV = popup ? "" : i18n.get("booking.regoleTariffarie.cambioPrenotazione");
							newV = newV + " " + (fareRulesStatic ? "" : "<span class=\"" + cssClass + "\">" + i18n.get("booking.regoleTariffarie.primaDellaPartenza") + "</span>" + (popup ? "<br/>" : "")) ;
							if (!entry.getValue().equals("true")) {
								newV = newV + "<span class=\"" + cssClass + "\">" + entry.getValue() + "</span>" + (popup ? "<br/>" : "");
							} else {
								newV = newV + "<span class=\"" + cssClass + "\">" + i18n.get("booking.regoleTariffarie.gratuito") + "</span>";
							}
							newFareRule.put(newK, newV);
//						} else if(!fareRulesStatic) { //if (entry.getKey().equals("booking.regoleTariffarie.cambioPrenotazioneDopoPartenza")) {
						} else if (entry.getKey().equals("booking.regoleTariffarie.cambioPrenotazioneDopoPartenza")) { //con questo if si differenzia il cambio prenotazione tra prima e dopo il volo
							newV = newFareRule.get(newK);
							if (newV==null) {
								newV = "";
							}
							boolean primaGratuito = newV.contains(i18n.get("booking.regoleTariffarie.gratuito"));
							newV = newV + " " + "<span class=\"" + cssClass + "\">" + i18n.get("booking.regoleTariffarie.dopoLaPartenza") + "</span>" + (popup ? "<br/>" : "");
							if (!entry.getValue().equals("true")) {
								newV = newV + "<span class=\"" + cssClass + "\">" + entry.getValue() + "</span>";
							} else {
								if (primaGratuito) {
									if (popup) {
										newV = "<span class=\"i-checked\"></span><br/>";
									} else {
										newV = i18n.get("booking.regoleTariffarie.cambioPrenotazione") + "<span class=\"" + cssClass + "\">" + i18n.get("booking.regoleTariffarie.gratuito") + "</span>";
									}
								} else {
									newV = newV + "<span class=\"" + cssClass + "\">" + i18n.get("booking.regoleTariffarie.gratuito") + "</span>";
								}
							}
							newFareRule.put(newK, newV);
						}
					} else if (entry.getKey().contains("rimborsoPrenotazione")) {
						if (entry.getKey().equals("booking.regoleTariffarie.rimborsoPrenotazionePrimaPartenza") ) {
							newV = popup ? "" : i18n.get("booking.regoleTariffarie.rimborsoPrenotazione");
							newV = newV + " " + (fareRulesStatic ? "" :  "<span class=\"" + cssClass + "\">" + i18n.get("booking.regoleTariffarie.primaDellaPartenza") + "</span>" + (popup ? "<br/>" : ""));
							if (!entry.getValue().equals("true")) {
								newV = newV + "<span class=\"" + cssClass + "\">" + entry.getValue() + "</span>" + (popup ? "<br/>" : "");
							} else {
								newV = newV + "<span class=\"" + cssClass + "\">" + i18n.get("booking.regoleTariffarie.gratuito") + "</span>";
							}
							newFareRule.put(newK, newV);
//						} else if(!fareRulesStatic) { //(entry.getKey().equals("booking.regoleTariffarie.rimborsoPrenotazioneDopoPartenza")) {
						} else if(entry.getKey().equals("booking.regoleTariffarie.rimborsoPrenotazioneDopoPartenza")) { //con questo if si differenzia il rimborso tra prima e dopo il volo
							newV = newFareRule.get(newK);
							if (newV==null) {
								newV = "";
							}
							boolean primaGratuito = newV.contains(i18n.get("booking.regoleTariffarie.gratuito"));
							newV = newV + " " + "<span class=\"" + cssClass + "\">" + i18n.get("booking.regoleTariffarie.dopoLaPartenza") + "</span>" + (popup ? "<br/>" : "");
							if (!entry.getValue().equals("true")) {
								newV = newV + "<span class=\"" + cssClass + "\">" + entry.getValue() + "</span>";
							} else {
								if (primaGratuito) {
									if (popup) {
										newV = "<span class=\"i-checked\"></span><br/>";
									} else {
										newV = i18n.get("booking.regoleTariffarie.rimborsoPrenotazione") + "<span class=\"" + cssClass + "\">" + i18n.get("booking.regoleTariffarie.gratuito") + "</span>";
									}
								} else {
									newV = newV + "<span class=\"" + cssClass + "\">" + i18n.get("booking.regoleTariffarie.gratuito") + "</span>";
								}
							}
							newFareRule.put(newK, newV);
						}
					} else {
						newV = i18n.get(entry.getKey());
						if (!entry.getValue().equals("true")) {
							if (popup) {
								// Qui puo' essere solo Bagaglio in stiva
								newV = "<span class=\"" + cssClass + "\">" + entry.getValue() + "</span>";
							} else {
								//non entrera' mai dopo aver levato le miglia
								newV = newV + "<br> " + entry.getValue();
							}
						} else if (popup) {
							newV = "<span class=\"i-checked\"></span>";
						}
						newFareRule.put(newK, newV);
					}
					
//					newFareRule.put(newK, newV);
				}
			}
			fareRulesList.put(page.getId(), newFareRule);
			
			if (i == indexBrand) {
				activeBrand = page.getId();
			}
			i++;
		}
		Map<String,String> newFareRule;
		if (ctx.missedBrandsMap.get("-") != null) {
			for (BrandRender brandRender : ctx.missedBrandsMap.get("-")) {
				newFareRule = new LinkedHashMap<String, String>();
				newFareRule.put(brandRender.getId(), "empty");
				fareRulesList.put(brandRender.getId(), newFareRule);
			}
		}
		return fareRulesList;
	}
	
	private String obtainCSSClassByFareRule(String key) {
		/**
		 * bagaglioMano --> bagaglio a mano
		 * mm_tier1 --> miglia
		 * bagaglioStiva --> bagaglio in stiva (peso e capire centimetri)
		 * postoBordo --> scelta posto
		 * cambioPrenotazione --> manca: campio prenotazione prima e dopo partenza
		 * manca rimborso prima e dopo partenza
		 */
		String cssClass = "";
		switch (key) {
			case "booking.regoleTariffarie.bagaglioAMano":{
				cssClass = "i-bagaglioMano";
			}break;
			case "booking.regoleTariffarie.bagaglioInStiva":{
				cssClass = "i-bagaglioStiva";
			}break;
			case "booking.regoleTariffarie.sceltaPosto":{
				cssClass = "i-postoBordo";
			}break;
			case "booking.regoleTariffarie.miglia":{
				cssClass = "i-mm_tier1";
			}break;
			case "booking.regoleTariffarie.cambioPrenotazionePrimaPartenza":{
				cssClass = "i-cambioPrenotazione";
			}break;
			case "booking.regoleTariffarie.cambioPrenotazioneDopoPartenza":{
				cssClass = "i-cambioPrenotazione-dopo";
			}break;
			case "booking.regoleTariffarie.rimborsoPrenotazionePrimaPartenza":{
				cssClass = "i-rimborsoBiglietto";
			}break;
			case "booking.regoleTariffarie.rimborsoPrenotazioneDopoPartenza":{
				cssClass = "i-rimborsoBiglietto-dopo";
			}break;
			default:{
				return "";
			}
		}
		return cssClass;
	}
	
	private boolean computeSelectedBus(FlightData selectedflightData) {
		if (selectedflightData != null) {
			if (selectedflightData instanceof DirectFlightData) {
				DirectFlightData directFlight = (DirectFlightData)selectedflightData;
				if (directFlight.isBus()) {
					return true;
				}
			} else {
				ConnectingFlightData connectingFlightData = (ConnectingFlightData) selectedflightData;
				for (FlightData flightData : connectingFlightData.getFlights()) {
					DirectFlightData directFlight = (DirectFlightData) flightData;
					if (directFlight.isBus()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public Map<String, Integer> getSeatAvailMap() {
		return seatAvailMap;
	}
	
	public boolean isRTL() {
		return isRTL;
	}

	private Boolean computeIsSoggettoAppGov(String numeroVolo) {
		String[] arrayVolo =configuration.getListaVoliSoggettiApprovazioneGovernativa().split(";");
		for (int i=0; i<arrayVolo.length; i++){
			if (arrayVolo[i].equals(numeroVolo)) {return true;}
		}
		return false;
	}

	private String obtainFlightNumbers(FlightData flightData) {
		String result = "";
		//FlightData flightData = flightSelection.getFlightData();
		if (flightData instanceof DirectFlightData) {
			DirectFlightData directFlightData = (DirectFlightData) flightData;
			return directFlightData.getCarrier() + directFlightData.getFlightNumber();
		} else {
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
			for (FlightData flight : connectingFlightData.getFlights()) {
				DirectFlightData directFlightData = (DirectFlightData) flight;
				String number = directFlightData.getCarrier() + directFlightData.getFlightNumber();
				if (!result.equals("")) {
					result += ", " + number;
				} else {
					result = number;
				}
			}
		}
		return result;
	}

	private void setAllPenaltiesForType(Map<BrandPageData, Map<String, String>> tempFareRulesList) {
		for (BrandPageData brandPageData : tempFareRulesList.keySet()){
			Map <String,String> map = tempFareRulesList.get(brandPageData);
			changeBefore.add(map.get("booking.regoleTariffarie.cambioPrenotazionePrimaPartenza"));
			changeAfter.add(map.get("booking.regoleTariffarie.cambioPrenotazioneDopoPartenza"));
			refundBefore.add(map.get("booking.regoleTariffarie.rimborsoPrenotazionePrimaPartenza"));
			refundAfter.add(map.get("booking.regoleTariffarie.rimborsoPrenotazioneDopoPartenza"));
		}

	}

	private void setNotes(Map<BrandPageData, Map<String,String>> map) {
		for (BrandPageData bpdata : map.keySet()) {
			boolean first = false;
			boolean second = false;
			boolean generic = false;
			Map<String, String> fareRules = map.get(bpdata);
			for (String key : fareRules.keySet()) {
				if (fareRules.get(key).contains("*")) {
					first = true;
				}
				if(fareRules.get(key).contains("#")){
					second = true;
				}
				if(fareRules.get(key).contains("/")){
					generic = true;
				}
			}
			firstLabel.add(first);
			secondLabel.add(second);
			getGenericLabel().add(generic);
		}
	}

	private void SetAsterisck(Map<BrandPageData,Map<String,String>> tempFareRulesList) {
		for (BrandPageData bpdata : tempFareRulesList.keySet()) {
			for (String key : tempFareRulesList.get(bpdata).keySet()) {
				if(tempFareRulesList.get(bpdata).get(key).contains("#")){
					tempFareRulesList.get(bpdata).put(key,tempFareRulesList.get(bpdata).get(key).split("#")[0] + "**");
				}
				if(tempFareRulesList.get(bpdata).get(key).contains("/")){
					tempFareRulesList.get(bpdata).put(key,tempFareRulesList.get(bpdata).get(key).split("/")[0] + "*");
				}
			}
		}
	}

	private void setNotesForPopup(){
		for(boolean a : firstLabel){
			if(a){
				firstLabelPopup = true;
			}
		}
		for(boolean b : secondLabel){
			if(b){
				secondLabelPopup = true;
			}
		}
		for(boolean c : genericLabel){
			if(c){
				genericLabelPopup = true;
			}
		}
	}

	public boolean isGenericLabelPopup() {
		return genericLabelPopup;
	}

	public List<Boolean> getGenericLabel() {
		return genericLabel;
	}
}
