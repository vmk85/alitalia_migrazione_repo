package com.alitalia.aem.consumer.millemiglia.servlet.profiloutente;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloResponse;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.TipoProfilo;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.wcm.api.WCMMode;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.global.i18n.I18nKeyMillemiglia;
import com.alitalia.aem.consumer.millemiglia.model.profiloutente.DatiLoginData;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;

/* aggiunto per inviare mail ae sms alert */
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.ws.otp.service.*;
import com.alitalia.aem.ws.sendmail.*;
import com.alitalia.aem.ws.sendsms.*;
import com.alitalia.aem.ws.sendmail.xsd.*;
import com.alitalia.aem.ws.common.exception.*;
import com.alitalia.aem.ws.sendsms.xsd.*;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloRequestLocal;
import com.day.cq.i18n.I18n;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.SlingRequestProcessor;
/*aggiunto per inviare mail ae sms alert */


//datiloginsubmit

@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = {
				"datiloginsubmit",
				"datiloginsubmit-sendalertcomunication",
				"asso1",
				"asso2",
				"asso3",
				"asso4",
				"asso5"
		}),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class DatiLoginServlet  extends GenericFormValidatorServlet {


	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaCustomerProfileManager customerProfileManager;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		
		logger.debug("[DatiLoginServlet] validateForm");
		
		try {
			ResourceResolver resourceResolver = request.getResourceResolver();
			Session session = resourceResolver.adaptTo(Session.class);
			UserPropertiesManager userPropertiesManager = resourceResolver.adaptTo(UserPropertiesManager.class);
			UserProperties profileProperties = userPropertiesManager.getUserProperties(session.getUserID(), "profile");
			UserProperties privateProperties = userPropertiesManager.getUserProperties(session.getUserID(), "private");
			String customerNumber = profileProperties.getProperty("customerNumber");
			String selectorString = request.getRequestPathInfo().getSelectorString();

			if(selectorString==null){
				selectorString = request.getRequestPathInfo().getExtension();
				if(selectorString==null){
					selectorString = request.getRequestPathInfo().getExtension();
					selectorString="";
				}
			}
			Validator validator =new Validator();
			if (selectorString.equals("datiloginsubmit")){
				validator = setValidatorParameters(request, new Validator());
			}
			// validate parameters!


			ResultValidation validateReturn =validator.validate();


			if (selectorString.equals("datiloginsubmit")){

				GetDatiSicurezzaProfiloResponseLocal datiBeforeUpdateSicurezzaProfilo = (GetDatiSicurezzaProfiloResponseLocal) request.getSession().getAttribute("MilleMigliaProfileDataSicurezza");

				if(datiBeforeUpdateSicurezzaProfilo == null) {
					GetDatiSicurezzaProfiloRequestLocal getDatiSicurezzaProfiloRequest = new GetDatiSicurezzaProfiloRequestLocal();


					getDatiSicurezzaProfiloRequest.setIdProfilo(customerNumber);

					datiBeforeUpdateSicurezzaProfilo = consumerLoginDelegate.GetDatiSicurezzaProfilo(getDatiSicurezzaProfiloRequest);

					request.getSession().setAttribute("MilleMigliaProfileDataSicurezza",datiBeforeUpdateSicurezzaProfilo);

				}

				String password = request.getParameter("password");

				if (datiBeforeUpdateSicurezzaProfilo.getUsername() == null
						|| datiBeforeUpdateSicurezzaProfilo.getUsername().isEmpty()) {

					if (password == null ||
							password.isEmpty() ) {

						validateReturn.addField("password", I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD);
					}

				}


				String username = request.getParameter("username");


				SubscribeSACheckUserNameDuplicateRequest req = new SubscribeSACheckUserNameDuplicateRequest();
//			req.setProfileID(userIdRequestCode.toString());
				req.setUsername(username);
				req.setProfileID(customerNumber);
				SubscribeSACheckUserNameDuplicateResponse checkUserNameDuplicateResponse =
						consumerLoginDelegate.CheckUserNameDuplicate(req);

				if (checkUserNameDuplicateResponse.isFlagNoUserNameDuplicate()) {
					//millemiglia.username.error.invalid.alreadyused
					validateReturn.addField("username", I18nKeyMillemiglia.ERROR_USERNAME_USED);
				}

				String emailSA = request.getParameter("emailSA");

				SubscribeSACheckEmailUserNameDuplicateRequest mailDupRec = new SubscribeSACheckEmailUserNameDuplicateRequest();
				mailDupRec.setEmailUserName(emailSA);
				mailDupRec.setProfileID(customerNumber);
				SubscribeSACheckEmailUserNameDuplicateResponse checkEmailUserNameDuplicateResponse =
						consumerLoginDelegate.CheckEmailUserNameDuplicate(mailDupRec);

				if (checkEmailUserNameDuplicateResponse.isFlagNoEmailUserNameDuplicate()) {
					//millemiglia.username.error.invalid.alreadyused
					validateReturn.addField("emailSA", I18nKeyMillemiglia.ERROR_EMAIL_SA_USED);
				}

				String nationalPrefix = request.getParameter("nationalPrefix");
				String cellulare = request.getParameter("cellulare");


				if (nationalPrefix.isEmpty()&& !cellulare.isEmpty() ||
						!nationalPrefix.isEmpty()&& cellulare.isEmpty()) {

					validateReturn.addField("cellulare", I18nKeyMillemiglia.ERROR_PREF_AND_PHONE);
				}

				String domanda = request.getParameter("domanda");
				String risposta = request.getParameter("risposta");

				if (domanda.isEmpty()&& !risposta.isEmpty() ||
						!domanda.isEmpty()&& risposta.isEmpty()) {

					validateReturn.addField("risposta", I18nKeyMillemiglia.ERROR_QUEST_AND_ANS);
					//public static final String ERROR_PREF_AND_PHONE = "millemiglia.phone.error.ifprefix.number";
					//public static final String ERROR_QUEST_AND_ANS = "millemiglia.phone.error.ifquestion.answer";
				}

				if (cellulare.isEmpty()&&  risposta.isEmpty()) {

					validateReturn.addField("cellulare", I18nKeyMillemiglia.ERROR_QUEST_ORR_PHONE);

					//public static final String ERROR_PREF_AND_PHONE = "millemiglia.phone.error.ifprefix.number";
					//public static final String ERROR_QUEST_AND_ANS = "millemiglia.phone.error.ifquestion.answer";
				}


				//
			}

			return validateReturn;
		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Errore durante la procedura di salvataggio dati ", e);
			return null;
		}
	}

	protected void saveData(SlingHttpServletRequest request,
						   SlingHttpServletResponse response) throws IOException {
		logger.debug("[DatiLoginServlet] performSubmit");

		try {
			ResourceResolver resourceResolver = request.getResourceResolver();
			Session session = resourceResolver.adaptTo(Session.class);
			UserPropertiesManager userPropertiesManager = resourceResolver.adaptTo(UserPropertiesManager.class);
			UserProperties profileProperties = userPropertiesManager.getUserProperties(session.getUserID(), "profile");
			UserProperties privateProperties = userPropertiesManager.getUserProperties(session.getUserID(), "private");

			String customerNumber = profileProperties.getProperty("customerNumber");
			String customerPinCode =
					customerProfileManager.decodeAndDecryptProperty(privateProperties.getProperty("customerPinCode"));
			String customerNickName = profileProperties.getProperty("customerNickName");

			if (!request.getParameter("pin").isEmpty()) {
				UpdatePinRequest updatePinRequest = new UpdatePinRequest();
				updatePinRequest.setSid(IDFactory.getSid(request));
				updatePinRequest.setTid(IDFactory.getTid());
				updatePinRequest.setNewPin(request.getParameter("pin"));

				MMCustomerProfileData customerProfileData = new MMCustomerProfileData();
				customerProfileData.setCustomerNumber(customerNumber);
				customerProfileData.setCustomerPinCode(customerPinCode);
				updatePinRequest.setCustomerProfile(customerProfileData);

				UpdatePinResponse updatePinResponse = consumerLoginDelegate.updatePin(updatePinRequest);

				List<String> errors = updatePinResponse.getCustomerUpdateErrors();
				if ((errors != null && errors.size() > 0 && !errors.get(0).equals("None")) ||
						updatePinResponse.getCustomerProfile() == null) {
					throw new IOException(); //TODO: Verificare se sollevare un'eccezione diversa
				}

				customerProfileManager.updateCustomerProfileProperties(customerProfileData,
						request.getResourceResolver(), request.getParameter("pin"));
			}

			if (!request.getParameter("nickname").isEmpty() &&
					!request.getParameter("nickname").equals(customerNickName)) {

				UpdateNicknameRequest updateNicknameRequest = new UpdateNicknameRequest();
				updateNicknameRequest.setSid(IDFactory.getSid(request));
				updateNicknameRequest.setTid(IDFactory.getTid());

				MMCustomerProfileData customerProfileData = new MMCustomerProfileData();
				customerProfileData.setCustomerNickName(customerNickName);
				customerProfileData.setCustomerNumber(customerNumber);
				customerProfileData.setCustomerPinCode(customerPinCode);
				updateNicknameRequest.setCustomerProfile(customerProfileData);

				updateNicknameRequest.setNewNickname((String) request.getParameter("nickname"));

				UpdateNicknameResponse updateNicknameResponse = consumerLoginDelegate.updateNickname(updateNicknameRequest);

				List<String> errors = updateNicknameResponse.getCustomerUpdateErrors();
				if ((errors != null && errors.size() > 0 && !errors.get(0).equals("None")) ||
						updateNicknameResponse.getCustomerProfile() == null) {
					throw new IOException(); //TODO: Verificare se sollevare un'eccezione diversa
				}

				customerProfileData.setCustomerNickName(updateNicknameRequest.getNewNickname());
				customerProfileManager.updateCustomerProfileProperties(customerProfileData,
						request.getResourceResolver(), "");
			}

			///////// dati sicurezza ////////////////

			GetDatiSicurezzaProfiloResponseLocal datiBeforeUpdateSicurezzaProfilo = (GetDatiSicurezzaProfiloResponseLocal) session.getAttribute("MilleMigliaProfileDataSicurezza");

			if (datiBeforeUpdateSicurezzaProfilo == null ){

				GetDatiSicurezzaProfiloRequestLocal datiSicurezzaProfiloRequest1 = new GetDatiSicurezzaProfiloRequestLocal();

				datiSicurezzaProfiloRequest1.setIdProfilo(customerNumber);

				datiBeforeUpdateSicurezzaProfilo = consumerLoginDelegate.GetDatiSicurezzaProfilo(datiSicurezzaProfiloRequest1);

				request.getSession().setAttribute("MilleMigliaProfileDataSicurezza",datiBeforeUpdateSicurezzaProfilo);

			}

			com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloRequest datiSicurezzaProfiloRequest=new com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloRequest();

			ObjectFactory of=new ObjectFactory();

			//datiSicurezzaProfiloRequest.setAccessFailedCount(of.createGetDatiSicurezzaProfiloResponseAccessFailedCount("0"));
			datiSicurezzaProfiloRequest.setCellulare(request.getParameter("cellulare"));
			datiSicurezzaProfiloRequest.setEmailAccount(request.getParameter("emailSA"));
			datiSicurezzaProfiloRequest.setPrefissoNazioneCellulare(request.getParameter("nationalPrefix"));

			datiSicurezzaProfiloRequest.setFlagRememberPassword(true);

			//if (datiBeforeUpdateSicurezzaProfilo.get())
			//fare controlli per settere correttamente flag
			//in sostanza posso dire che un flag è true solo se lo valorizzo (ad esempio se il cellulare non lo scrivo
			// non lo posso mettere a true
			// quindi verifico
			// se prima cera ed ora no lo metto a false se ora cè lo metto a true a prescindere di quanto era prima)
            //non basta controllare del tipo (se cè true , se non cè false perché rispost segreta la inserisco solo se la voglio
            // cambiare (quindi magari cera ))

            datiSicurezzaProfiloRequest.setFlagVerificaCellulareOK(false);
            if (!request.getParameter("cellulare").isEmpty() ) {
                datiSicurezzaProfiloRequest.setFlagVerificaCellulareOK(true);
            }else{
                //a differenza della domanda segreta (obbligatoria in abbinamento con il cellulare ,almeno uno dei due deve essere presente)
                //se non lo scrivo nella textbox è perchè non lo voglio (quindi o già non cera o lo voglio cancllare)
                //mentre per la domanda segreta (essendo simile alla password che non la devo mostarre a video)
                //se non c'è nella textbox non c'è in due casistiche
                //o perchè non la voglio
                //o perche  non la voglio modificare (quindi c'era e non inserendola implicitamente non la modifico)
                datiSicurezzaProfiloRequest.setFlagVerificaCellulareOK(false);
//                if (datiBeforeUpdateSicurezzaProfilo.getCellulare() != null &&
//                        !datiBeforeUpdateSicurezzaProfilo.getCellulare().isEmpty()) {
//                    datiSicurezzaProfiloRequest.setFlagVerificaCellulareOK(true);
//                }
            }



			datiSicurezzaProfiloRequest.setFlagVerificaEmailAccountOK(true);
			datiSicurezzaProfiloRequest.setFlagVerificaPasswordOK(true);

            datiSicurezzaProfiloRequest.setFlagVerificaRispostaOK(false);
            if (!request.getParameter("risposta").isEmpty() ) {
                datiSicurezzaProfiloRequest.setFlagVerificaRispostaOK(true);
            }else{
                if (datiBeforeUpdateSicurezzaProfilo.getSecureAnswer() != null &&
                        !datiBeforeUpdateSicurezzaProfilo.getSecureAnswer().isEmpty()) {
                    datiSicurezzaProfiloRequest.setFlagVerificaRispostaOK(true);
                }
            }

			datiSicurezzaProfiloRequest.setFlagVerificaUserNameOK(true);

			datiSicurezzaProfiloRequest.setIdProfilo(customerNumber);//session.getUserID());
			datiSicurezzaProfiloRequest.setLockedout(false);

			//idProfilo
			//della password viene fatto un hash MD5 convertito in maiuscolo per passarla al webservice


			if (!request.getParameter("risposta").isEmpty() ) {
				String MD5risposta = DigestUtils.md5Hex(request.getParameter("risposta"));
				datiSicurezzaProfiloRequest.setSecureAnswer(MD5risposta);
			}else{

				if (datiBeforeUpdateSicurezzaProfilo.getSecureAnswer() != null &&
						!datiBeforeUpdateSicurezzaProfilo.getSecureAnswer().isEmpty()) {

					datiSicurezzaProfiloRequest.setSecureAnswer(datiBeforeUpdateSicurezzaProfilo.getSecureAnswer());
				}
			}
			//datiSicurezzaProfiloRequest.setSecureAnswer(request.getParameter("risposta"));

			//datiSicurezzaProfiloRequest.setSecureQuestionCode(request.getParameter("domanda"));

			String domanda = request.getParameter("domanda");
			if(domanda != null && !domanda.isEmpty()){
				datiSicurezzaProfiloRequest.setSecureQuestionID(Integer.parseInt(domanda));
			}


			//datiSicurezzaProfiloRequest.setSocialToken(of.createGetDatiSicurezzaProfiloResponseSocialToken("AKFSKDFKSKD"));
			//datiSicurezzaProfiloRequest.setSocialUserID(of.createGetDatiSicurezzaProfiloResponseSocialUserID("2121"));

			//datiSicurezzaProfiloRequest.setTipoProfilo(TipoProfilo.fromValue("MMKids"));

			datiSicurezzaProfiloRequest.setUserName(request.getParameter("username"));

			if (!request.getParameter("password").isEmpty() ) {
				String MD5password = DigestUtils.md5Hex(request.getParameter("password")).toUpperCase();
				datiSicurezzaProfiloRequest.setPassword(MD5password);
			}else{
				if (datiBeforeUpdateSicurezzaProfilo.getPassword() != null &&
						!datiBeforeUpdateSicurezzaProfilo.getPassword().isEmpty()) {

					datiSicurezzaProfiloRequest.setPassword(datiBeforeUpdateSicurezzaProfilo.getPassword());
				}

			}

			MMCustomerProfileData customerProfileData = AlitaliaUtils.getAuthenticatedUser(request);

			com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloResponse resp = consumerLoginDelegate.SetDatiSicurezzaProfilo(datiSicurezzaProfiloRequest);

			GetDatiSicurezzaProfiloRequestLocal getDatiSicurezzaProfiloRequest = new GetDatiSicurezzaProfiloRequestLocal();

			getDatiSicurezzaProfiloRequest.setIdProfilo(customerNumber);

			datiBeforeUpdateSicurezzaProfilo = consumerLoginDelegate.GetDatiSicurezzaProfilo(getDatiSicurezzaProfiloRequest);

			request.getSession().setAttribute("MilleMigliaProfileDataSicurezza",datiBeforeUpdateSicurezzaProfilo);

			ProfileRequest profileRequest = new ProfileRequest();
			profileRequest.setSid(IDFactory.getSid());
			profileRequest.setTid(IDFactory.getTid());
			profileRequest.setCustomerProfile(customerProfileData);

			ProfileResponse profileResponse =
					consumerLoginDelegate.getProfile(profileRequest);
			customerProfileData = profileResponse.getCustomerProfile();

			if(resp.getFlagIDProfiloSicurezzaSaved()){
				//salvataggio effettuato con sucesso
			}else{
				throw new IOException(); //TODO: Verificare se sollevare un'eccezione diversa
			}

			/////////////////////////////////////////

			goBackSuccessfully(request, response);

		} catch(Exception e) {
			logger.error("Error DatiLoginServlet", e);
			throw new IOException(); //TODO: Verificare se sollevare un'eccezione diversa
		}
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws IOException {

		String selectorString = request.getRequestPathInfo().getSelectorString();

		if(selectorString==null){
			selectorString = request.getRequestPathInfo().getExtension();
			if(selectorString==null){
				selectorString = request.getRequestPathInfo().getExtension();
				selectorString="";
			}
		}

		switch (selectorString) {
			case "datiloginsubmit":
				saveData(request,response);
				break;
			case "datiloginsubmit-sendalertcomunication":
				sendAlert(request,response);
				break;
		}
	}


	protected void sendAlert(SlingHttpServletRequest request,
									   SlingHttpServletResponse response) throws IOException{
		String encodedUrlAccount = request.getParameter("encodedUrlAccount");

		//inviare mail o sms di alert che avvisa che qualcuno sta modificando i dati
		//viene lanciata la GetDatiSicurezzaProfilo
		//per leggere il numero o la mail pre esistenti ed utilizzarli per la notifica

		try {
			ResourceResolver resourceResolver = request.getResourceResolver();
			Session session = resourceResolver.adaptTo(Session.class);
			UserPropertiesManager userPropertiesManager = resourceResolver.adaptTo(UserPropertiesManager.class);
			UserProperties profileProperties = userPropertiesManager.getUserProperties(session.getUserID(), "profile");

			String customerNumber = profileProperties.getProperty("customerNumber");
			String customerName = profileProperties.getProperty("customerName");
			String customerSurname = profileProperties.getProperty("customerSurname");


			GetDatiSicurezzaProfiloRequestLocal datiSicurezzaProfiloRequest = new GetDatiSicurezzaProfiloRequestLocal();

			datiSicurezzaProfiloRequest.setIdProfilo(customerNumber);


			GetDatiSicurezzaProfiloResponseLocal datiSicurezzaProfiloRender = (GetDatiSicurezzaProfiloResponseLocal) request.getSession().getAttribute("MilleMigliaProfileDataSicurezza");

			if(datiSicurezzaProfiloRender == null) {
				GetDatiSicurezzaProfiloRequestLocal getDatiSicurezzaProfiloRequest = new GetDatiSicurezzaProfiloRequestLocal();


				getDatiSicurezzaProfiloRequest.setIdProfilo(customerNumber);

				datiSicurezzaProfiloRender = consumerLoginDelegate.GetDatiSicurezzaProfilo(getDatiSicurezzaProfiloRequest);

				request.getSession().setAttribute("MilleMigliaProfileDataSicurezza",datiSicurezzaProfiloRender);

			}

			// TODO: sessione consumerLoginDelegate.GetDatiSicurezzaProfilo

			final I18n i18n = new I18n(request.getResourceBundle(
					AlitaliaUtils.findResourceLocale(request.getResource())));

			String email =datiSicurezzaProfiloRender.getEmailAccount();
			String customerUserName =datiSicurezzaProfiloRender.getUsername();


			 /*
			//MILLEMIGLIA SECURITY ALERT MESSAGE
	        public static final String MMSA_SECURITY_ALERT_MAIL_FROM = "millemiglia.security.alert.mail.from";
	        public static final String MMSA_SECURITY_ALERT_MAIL__SUBJECT = "millemiglia.security.alert.mail.subject";
			 */

//            final I18n i18n = new I18n(request.getResourceBundle(
//                    AlitaliaUtils.findResourceLocale(request.getResource())));
//
//            msg.setFrom(i18n.get(I18nKeySpecialPage.MMSA_SECURITY_ALERT_MAIL_FROM));
//            msg.setSubject(i18n.get(I18nKeySpecialPage.MMSA_SECURITY_ALERT_MAIL__SUBJECT));


			String emailFrom=i18n.get(I18nKeySpecialPage.MMSA_SECURITY_ALERT_MAIL_FROM);

			//String emailSubject="Attenzione";
			String emailSubject=i18n.get(I18nKeySpecialPage.MMSA_SECURITY_ALERT_MAIL_SUBJECT);

//			String emailBody="ti stanno modificando l'account";
			String emailBody = getMailBody(request.getResource(),
					configuration.getMMSATemplateMailAccountWarning(), false);


			emailBody = emailBody.replace("{PLACEHOLDER_FIRSTNAME}", customerName);

			boolean isBodyHtml=true;

			sendEmail(emailFrom,email,emailSubject,emailBody,isBodyHtml);

		} catch (Exception e) {
			logger.error("Error DatiLoginServlet.sendAlert", e);
			throw new IOException(); //TODO: Verificare se sollevare un'eccezione diversa
		}

	}


	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		
		logger.debug("[DatiLoginServlet] saveDataIntoSession");
		
		DatiLoginData datiLoginData = new DatiLoginData();
		datiLoginData.setNickname(request.getParameter("nickname"));
		datiLoginData.setPin(request.getParameter("pin"));
		datiLoginData.setConfermaPin(request.getParameter("confermaPin"));
		request.getSession().setAttribute("datiLoginData", datiLoginData);
	}
	
	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request, Validator validator) {


		// get parameters from request
		String nickname = request.getParameter("nickname");
		String pin = request.getParameter("pin");

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String confpassword = request.getParameter("confpassword");
		String confermaPin = request.getParameter("confermaPin");
		String emailSA = request.getParameter("emailSA");
		String nationalPrefix = request.getParameter("nationalPrefix");
		String cellulare = request.getParameter("cellulare");
		String domanda = request.getParameter("domanda");
		String risposta = request.getParameter("risposta");



//		validator.addDirectCondition("nickname", nickname,
//				I18nKeyMillemiglia.NICKNAME_ERROR_EMPTY, "isNotEmpty");

		if(!pin.isEmpty() || !confermaPin.isEmpty()){

			validator.addCrossCondition("confermaPin", confermaPin, pin,
					I18nKeyMillemiglia.CONFERMAPIN_ERROR_NOTCORRESPONDING, "areEqual");
		}

		//username "isNotEmpty#;#isMMUserName#;#isNotUsernameDuplicate"

		validator.addDirectCondition("username", username,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD , "isNotEmpty");

		validator.addDirectCondition("username", username,
				I18nKeyMillemiglia.ERROR_NO_STRONG_USER , "isMMUserName");

		//password
		//data-validation="isNotEmpty#;

//		validator.addDirectCondition("password", password,
//				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD , "isNotEmpty");


		if(!password.isEmpty() ){

			validator.addDirectCondition("password", password,
					I18nKeyMillemiglia.ERROR_PWD_NOT_STRONG , "isPasswordStrongAuthentication");
		}



		validator.addCrossCondition("confpassword", confpassword, password,
				I18nKeyMillemiglia.ERROR_PWD_NOT_EQUAL_CONF, "areEqual");

		validator.addDirectCondition("emailSA", emailSA,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD , "isNotEmpty");


		return validator;

	}

	@Reference
	private OTPServiceClient wsClient;

	@Reference
	private SendMailServiceClient wsSendMailClient;

	@Reference
	private SendSmsServiceClient wsSendSmsClient;

	@Reference
	private AlitaliaConfigurationHolder configuration;

	private boolean sendEmail(String mailFrom ,String emailTo, String Subject, String messageText, boolean isBodyHtml ) throws Exception
	{
		logger.debug("[MillemigliaComunicazioniServlet] sendEmail ");

		// variabili di input
    //        String email = request.getParameter("email");
//        String textMessage = request.getParameter("textMessage");

		try {


			SendMailServiceClient sendMailClient = wsSendMailClient;
			MessaggioEmail msg= new MessaggioEmail() ;

			msg.setFrom(mailFrom);
			msg.setTo(emailTo);
			msg.setSubject(Subject);
			msg.setMessageText(messageText);

			msg.setIsBodyHtml(isBodyHtml);
			
			msg.setPriority(MailPriority.NORMAL);
			List<String> deliveryNotificationOptionsList=msg.getDeliveryNotification();
			deliveryNotificationOptionsList.add(DeliveryNotificationOptionsEnum.NONE.value());
			
			Boolean sendMailClientResponse = sendMailClient.sendMail(msg);

			return  sendMailClientResponse.booleanValue();

		} catch (WSClientException wse) {
			logger.error("Errore durante chiamata webservice sendMailClient.sendMail()",wse);
			throw new Exception("Errore durante chiamata webservice sendMailClient.sendMail()",wse);

		}
	}


	private void sendSMS(String phoneNumberTo, String messageSMSText, SlingHttpServletRequest request)  {

		logger.info("[MillemigliaComunicazioniServlet] - SendSMS");

/*
    in apps/bundle-fe-web-component-alitalia/config.prod-final.author/com.alitalia.aem.consumer.AlitaliaConfigurationHolder.xml
          millemiglia.sendsms.applicationName="MSA"
          millemiglia.sendsms.fromPhoneNumber="Alitalia"
          millemiglia.sendsms.notificationSend="false"
          millemiglia.sendsms.notificationLocalSend="false"
          millemiglia.sendsms.notificationReceive="false"
          millemiglia.sendsms.notificationNotReceive="false"
          millemiglia.security.alert.sendsms.enable="true"
          millemiglia.security.alert.sendemail.enable="true"
*/
		try{

//			String phoneNumberTo = request.getParameter("phone");
//          String messageSMSText = request.getParameter("smstext");

			logger.debug("[MillemigliaComunicazioniServlet] - SendSMS [INIZIO] ");

			com.alitalia.aem.ws.sendsms.xsd.ObjectFactory factory = new com.alitalia.aem.ws.sendsms.xsd.ObjectFactory();
			SmsMsg requestSms = factory.createSmsMsg();

			requestSms.setFromPhoneNumber(getConfiguration().getMMSASendsmsFromPhoneNumber());
			requestSms.setApplicationName(getConfiguration().getMMSASendsmsApplicationName());

			requestSms.setMessage(messageSMSText);
			requestSms.setToPhoneNumber(phoneNumberTo);

			//setto i parametri di invio SMS
			SmsParametersType parameter = factory.createSmsParametersType();
			parameter.setNotificationSend(getConfiguration().getMMSASendsmsNotificationSend());
			parameter.setNotificationLocalSend(getConfiguration().getMMSASendsmsNotificationLocalSend());
			parameter.setNotificationReceive(getConfiguration().getMMSASendsmsNotificationReceive());
			parameter.setNotificationNotReceive(getConfiguration().getMMSASendsmsNotificationNotReceive());

			requestSms.setSmsParameters(parameter);


			SmsMsgReturn smsMsgReturn = wsSendSmsClient.sendSms(requestSms);

			//logger.info("[MillemigliaComunicazioniServlet - linkSendSms => "+redirectUrl+"  ]");

			logger.info("[MillemigliaComunicazioniServlet] - SendSMS.msgId = " + smsMsgReturn.getMsgId());

//            TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
//            try {
//                json.object();
//                json.key("msgId").value(smsMsgReturn.getMsgId());
//                json.key("description").value(smsMsgReturn.getDescription());
//                json.key("responseCode").value(smsMsgReturn.getResponseCode());
//                json.endObject();
//            } catch (Exception e) {
//                logger.error("[MillemigliaComunicazioniServlet] SendSMS - Unexpected error generating JSON response.", e);
//            }

		}catch(Exception e){
			//throw new RuntimeException("[MillemigliaComunicazioniServlet] - Errore durante l'invocazione del servizio.", e);
		}

		logger.debug("[MillemigliaComunicazioniServlet] - SendSMS [FINE] ");
	}

	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Reference
	private ResourceResolverFactory resolverFactory;

	/* Service to create HTTP Servlet requests and responses */
	@Reference
	private RequestResponseFactory requestResponseFactory;

	/* Service to process requests through Sling */
	@Reference
	private SlingRequestProcessor requestProcessor;

	public String getMailBody(Resource resource, String templateMail,
							  boolean secure) {
		logger.debug("[DatiLoginServlet] getMailBody");

		String result = null;
		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(resource, false);
		try {

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			/* Get resource resolver */
			@SuppressWarnings("deprecation")
			ResourceResolver resourceResolver =
					resolverFactory.getAdministrativeResourceResolver(null);
			String requestedUrl = baseUrl + AlitaliaConstants.BASE_TEMPLATE_EMAIL_PATH
					+ "/" + templateMail;

			logger.debug("[DatiLoginServlet] getMailBody.requestedUrl" + requestedUrl);

			/* Setup request */
			HttpServletRequest req =
					requestResponseFactory.createRequest("GET", requestedUrl);
			WCMMode.DISABLED.toRequest(req);

			out = new ByteArrayOutputStream();
			HttpServletResponse resp =
					requestResponseFactory.createResponse(out);

			/* Process request through Sling */
			requestProcessor.processRequest(req, resp, resourceResolver);

			String encoding = "UTF-8";

			result = new String(out.toByteArray(), Charset.forName(encoding));

		} catch (LoginException | ServletException | IOException e) {
			logger.error("Generic exception while retrieving email template "
					+ "from JCR repository: {}", e);
			result = "";
		}

//		if (logger.isDebugEnabled()) {
//			logger.debug("Template retrieved and APIS/ESTA placeholders "
//					+ "replaced: {} ", result);
//		}

		return result;

	}
}
