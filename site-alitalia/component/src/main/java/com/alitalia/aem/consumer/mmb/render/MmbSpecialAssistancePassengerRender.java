package com.alitalia.aem.consumer.mmb.render;


public class MmbSpecialAssistancePassengerRender {
	private String name;
	private String lastname;
	private String ticket;
	private String mail;
	private String telephone;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	
	@Override
	public String toString() {
		return "MmbSpecialAssistancePassengerRender [name=" + name
				+ ", lastname=" + lastname + ", ticket=" + ticket
				 + "]";
	}
	
	
	
}
