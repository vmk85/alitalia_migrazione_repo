package com.alitalia.aem.consumer.booking;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.SlingRequestProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PaymentData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.utils.FlightDataUtils;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.render.DirectFlightDataRender;
import com.alitalia.aem.consumer.booking.render.GenericPriceRender;
import com.alitalia.aem.consumer.utils.XMLRenderingUtils;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.WCMMode;

/**
 *
 * @author R.Capitini
 *
 * Helper class used to create email message containg payment
 * receipt and booking summary. Email message is in form of
 * XSLT since it is sent by BE Alitalia service during payment
 * orchestration.
 *
 */
@Component(immediate = true)
@Service(value=PaymentEmailGenerator.class)
public class PaymentEmailGenerator {

	@Property(description = "Path to editable email template")
	private static final String TEMPLATE_MAIL_CONFERMAPRENOTAZIONE = "template.mail.confermaprenotazione";
	private String templateMailConfermaPrenotazione = "";

	@Property(description = "Folder containing email template files")
	private static final String EMAIL_FOLDER = "email.folder";
	private String emailFragmentFolder = "";

	@Property(description = "XSL stilesheet wrapper")
	private static final String EMAIL_STILESHEET = "email.stilesheet";
	private String emailStilesheetFragmentName = "";

	@Property
	private static final String PLACEHOLDER_MAIL_BODY = "email.placeholder.message_body";
	private String placeholderMailBody = "";

	@Property
	private static final String PLACEHOLDER_BAGAGLIO_STIVE = "email.placeholder.placeholder_bagaglio_stive";
	private String placeholderBagaglioStive = "";

	@Property(description = "Email subject")
	private static final String EMAIL_SUBJECT = "email.subject";
	private String emailSubject = "";

	@Property(description = "Folder containing booking information for Light fare")
	private static final String BOOKING_CONTENT_PATH = "booking.content.path";
	private String bookingContentPath = "";

	@Property(description = "Proprerty containing baggage information for Light fare")
	private static final String LIGHT_FARE_BAGGAGE_INFO_ATTRIBUTE = "email.attribute.bagaglio_stive_lightfare";
	private String emailBaggageLightfareAttribute = "";

	private static final Logger logger = LoggerFactory.getLogger(PaymentEmailGenerator.class);

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private ResourceResolverFactory resolverFactory;

	/** Service to create HTTP Servlet requests and responses */
	@Reference
	private RequestResponseFactory requestResponseFactory;

	/** Service to process requests through Sling */
	@Reference
	private SlingRequestProcessor requestProcessor;

	@Activate
	protected void activate(final Map<String, Object> config) throws Exception {

		String jcrResourceReference = "";

		this.emailFragmentFolder = String.valueOf(config.get(EMAIL_FOLDER));

		if (this.emailFragmentFolder != null && !"".equals(this.emailFragmentFolder)) {
			jcrResourceReference = String.valueOf(config.get(EMAIL_STILESHEET));
			this.emailStilesheetFragmentName = this.emailFragmentFolder + jcrResourceReference;

		} else {
			throw new Exception("Undefined folder parameter for email configuration");
		}

		this.placeholderBagaglioStive = String.valueOf(config.get(PLACEHOLDER_BAGAGLIO_STIVE));
		this.placeholderMailBody = String.valueOf(config.get(PLACEHOLDER_MAIL_BODY));

		this.templateMailConfermaPrenotazione = String.valueOf(config.get(TEMPLATE_MAIL_CONFERMAPRENOTAZIONE));

		this.emailSubject = String.valueOf(config.get(EMAIL_SUBJECT));
		this.bookingContentPath = String.valueOf(config.get(BOOKING_CONTENT_PATH));
		this.emailBaggageLightfareAttribute = String.valueOf(config.get(LIGHT_FARE_BAGGAGE_INFO_ATTRIBUTE));

	}

	@Modified
	protected void modified(final Map<String, Object> config) throws Exception {

		String jcrResourceReference = "";

		this.emailFragmentFolder = String.valueOf(config.get(EMAIL_FOLDER));

		if (this.emailFragmentFolder != null && !"".equals(this.emailFragmentFolder)) {
			jcrResourceReference = String.valueOf(config.get(EMAIL_STILESHEET));
			this.emailStilesheetFragmentName = this.emailFragmentFolder + jcrResourceReference;


		} else {
			throw new Exception("Undefined folder parameter for email configuration");
		}

		this.placeholderBagaglioStive = String.valueOf(config.get(PLACEHOLDER_BAGAGLIO_STIVE));
		this.placeholderMailBody = String.valueOf(config.get(PLACEHOLDER_MAIL_BODY));
		this.templateMailConfermaPrenotazione = String.valueOf(config.get(TEMPLATE_MAIL_CONFERMAPRENOTAZIONE));

		this.emailSubject = String.valueOf(config.get(EMAIL_SUBJECT));

		this.bookingContentPath = String.valueOf(config.get(BOOKING_CONTENT_PATH));
		this.emailBaggageLightfareAttribute = String.valueOf(config.get(LIGHT_FARE_BAGGAGE_INFO_ATTRIBUTE));

	}

	public String getEmailSubject(I18n i18n) {
		String subject = emailSubject;
		if (i18n != null)
			subject = i18n.get(emailSubject);
		return subject;
	}

	public String prepareXsltMail(BookingSessionContext ctx, PaymentData paymentData, I18n i18n) {

		String emailStilesheetFragment = retrieveContentFromCRXRepository(this.emailStilesheetFragmentName);

		String emailTemplate = this.getEmailTemplate(ctx, paymentData);

		logger.debug("MAIL EmailTemplate: "+emailTemplate);

		String bagaglioStiveLightfare =
				retrieveInformationFromCRXRepository(ctx.baseUrl + this.bookingContentPath, this.emailBaggageLightfareAttribute);

		String emailBody = StringUtils.replace(emailTemplate, this.placeholderBagaglioStive, bagaglioStiveLightfare);
		if (logger.isDebugEnabled())
			logger.debug("Email body to be replaced: " + (emailBody != null ? emailBody : "[null]"));

		if (logger.isDebugEnabled())
			logger.debug("Starting email message cleanup for invalid XML entities");

		if (logger.isDebugEnabled())
			logger.debug("Email body placeholder: " + (this.placeholderMailBody != null ? this.placeholderMailBody : "[null]"));
		String finalXsltEmailMessage =
				XMLRenderingUtils.replaceToCharEntities(
						XMLRenderingUtils.replaceHtmlToXMLEntities(
								StringUtils.replace(emailStilesheetFragment, this.placeholderMailBody, emailBody)));

		if (logger.isDebugEnabled())
			logger.debug("Finished email message cleanup for invalid XML entities");

		if (logger.isDebugEnabled())
			logger.debug("Final XSLT email message: " + finalXsltEmailMessage);

		return finalXsltEmailMessage;
	}

	private String retrieveContentFromCRXRepository(String absPath) {

		String result = "";

		try
		{
			@SuppressWarnings("deprecation")
			ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			Session session = resourceResolver.adaptTo(Session.class);

			Node ntFileNode = session.getNode(absPath);
			Node ntResourceNode = ntFileNode.getNode("jcr:content");
			InputStream is = ntResourceNode.getProperty("jcr:data").getBinary().getStream();
			BufferedInputStream bin = new BufferedInputStream(is);

			String encoding = "UTF-8";
			if (ntResourceNode.hasProperty("jcr:encoding"))
				encoding = ntResourceNode.getProperty("jcr:encoding").getString();

			result = new String(IOUtils.toByteArray(bin), encoding);
			bin.close();
			is.close();
		}
		catch (Exception ex)
		{
			logger.error("Generic exception while retrieving resource {} from JCR repository: {}", absPath, ex);
			result = "";
		}
		return result;
	}

	private String retrieveInformationFromCRXRepository(String absPath, String attributeToRead) {
		String result = "";

		try
		{
			@SuppressWarnings("deprecation")
			ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			Session session = resourceResolver.adaptTo(Session.class);

			Node ntFileNode = session.getNode(absPath);
			Node ntResourceNode = ntFileNode.getNode("jcr:content");
			result = ntResourceNode.getProperty(attributeToRead).getString();
		}
		catch (Exception ex)
		{
			logger.warn("Generic exception while retrieving {}: {}", absPath, ex.getMessage());
		}
		return result;
	}

	@SuppressWarnings("deprecation")
	private String getEmailTemplate(BookingSessionContext ctx, PaymentData paymentData) {

		/* Setup response */
		String result = "";
		try {

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			/* Get resource resolver */
			ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			String requestedUrl = ctx.baseUrl + this.templateMailConfermaPrenotazione;

			/* Setup request */
			HttpServletRequest req = requestResponseFactory.createRequest("GET", requestedUrl);
			req.setAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE, ctx);
			req.setAttribute(BookingConstants.PAYMENT_DATA_ATTRIBUTE, paymentData);
			WCMMode.DISABLED.toRequest(req);

			out = new ByteArrayOutputStream();
			HttpServletResponse resp = requestResponseFactory.createResponse(out);

			/* Process request through Sling */
			requestProcessor.processRequest(req, resp, resourceResolver);

			String encoding = "UTF-8";

			result = new String(out.toByteArray(), Charset.forName(encoding));

			result = addDataToBanner(result,ctx,req);

		} catch (LoginException | ServletException | IOException e) {
			logger.error("Generic exception while retrieving email template from JCR repository: {}", e);
			result = "";
		} catch (ParseException e) {
			logger.error("Exception to parse date for booking banner: {}", e);
		}
		if (logger.isDebugEnabled())
			logger.debug("Template retrieved: ", result);

		return result;
	}

	private String addDataToBanner(String result, BookingSessionContext ctx, HttpServletRequest req) throws ParseException {

		String url = "&checkin={{data-partenza-booking}}&checkout={{data-ritorno-booking}}&iata_orr=1&iata={{areoporto-arrivo-booking}}&lang={{lingua-banner-booking}}&selected_currency={{valuta-banner-booking}}";
		if (ctx != null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			if (!ctx.searchElements.get(0).getDepartureDate().getTime().toString().equals("")){

				String outboundDate = sdf.format(ctx.searchElements.get(0).getDepartureDate().getTime());
				Date date = sdf.parse(outboundDate);

				SimpleDateFormat oldFormatDepDate = new SimpleDateFormat("yyyy-MM-dd");
				String depDateOld=oldFormatDepDate.format(date);

				url = url.replace("{{data-partenza-booking}}",depDateOld);
			}
			if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP){

				String returnDate =  sdf.format(ctx.searchElements.get(1).getDepartureDate().getTime());
				Date dateRe=sdf.parse(returnDate);

				SimpleDateFormat oldFormatRetDate = new SimpleDateFormat("yyyy-MM-dd");
				String retDateOld=oldFormatRetDate.format(dateRe);

				url = url.replace("{{data-ritorno-booking}}",retDateOld);
			}
			else {

				String returnDateOld =  sdf.format(ctx.searchElements.get(0).getDepartureDate().getTime());
				Date dateRe=sdf.parse(returnDateOld);
				Calendar c = Calendar.getInstance();
				c.setTime(dateRe);
				c.add(Calendar.DATE, 1); //same with c.add(Calendar.DAY_OF_MONTH, 1);
				Date d = c.getTime();
				SimpleDateFormat oldFormatRetDate = new SimpleDateFormat("yyyy-MM-dd");
				returnDateOld=oldFormatRetDate.format(d);

				url = url.replace("{{data-ritorno-booking}}",returnDateOld);
			}
			if (ctx.searchElements.get(0).getTo().getAirportCode() != null){

				url = url.replace("{{areoporto-arrivo-booking}}",ctx.searchElements.get(0).getTo().getAirportCode() );

			}
			if (ctx.locale.getLanguage() != null){
				url = url.replace("{{lingua-banner-booking}}",ctx.locale.getLanguage());
			}
			if (ctx.currency != null){
				url = url.replace("{{valuta-banner-booking}}",ctx.currency);
			}
		}

		result = result.replace("https://sp.booking.com/searchresults.it.html?aid=1503416","https://sp.booking.com/searchresults.it.html?aid=1503416" + url);

		return result;
	}
}
