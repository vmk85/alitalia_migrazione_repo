package com.alitalia.aem.consumer.utils;

import java.util.HashMap;
import java.util.Map;

public class CashAndMilesCountryTable {
	private static CashAndMilesCountryTable instance;
	
	private Map<String,MilesCashRatio> table;
	
	private CashAndMilesCountryTable(){
		initTable();
	}
	
	private void initTable() {
		table= new HashMap<String, MilesCashRatio>();
		table.put("be", new MilesCashRatio("EUR", 200, 1));
		table.put("bg", new MilesCashRatio("EUR", 200, 1));
		table.put("ca", new MilesCashRatio("CAD", 150, 1));
		table.put("ch", new MilesCashRatio("CHF", 163, 1));
		table.put("eg", new MilesCashRatio("EGP", 23, 1));
		table.put("gb", new MilesCashRatio("GBP", 230, 1));
		table.put("hu", new MilesCashRatio("HUF", 7, 10));
		table.put("il", new MilesCashRatio("USD", 182, 1));
		table.put("lb", new MilesCashRatio("USD", 182, 1));
		table.put("it", new MilesCashRatio("EUR", 200, 1));
		table.put("nl", new MilesCashRatio("EUR", 200, 1));
		table.put("ro", new MilesCashRatio("EUR", 200, 1));
		table.put("us", new MilesCashRatio("USD", 182, 1));
		table.put("com", new MilesCashRatio("EUR", 200, 1));
		table.put("ar", new MilesCashRatio("ARS", 13, 1));
		table.put("br", new MilesCashRatio("BRL", 76, 1));
		table.put("cn", new MilesCashRatio("CNY", 24, 1));
		table.put("de", new MilesCashRatio("EUR", 200, 1));
		table.put("es", new MilesCashRatio("EUR", 200, 1));
		table.put("fr", new MilesCashRatio("EUR", 200, 1));
		table.put("gr", new MilesCashRatio("EUR", 200, 1));
		table.put("jp", new MilesCashRatio("JPY", 15, 10));
		table.put("ma", new MilesCashRatio("MAD", 17, 1));
		table.put("pl", new MilesCashRatio("PLN", 48, 1));
		table.put("ro", new MilesCashRatio("EUR", 200, 1));
		table.put("ru", new MilesCashRatio("RUB", 5, 1));
		table.put("tn", new MilesCashRatio("TND", 90, 1));
		table.put("tr", new MilesCashRatio("TRY", 85, 1));
		table.put("au", new MilesCashRatio("AUD", 144, 1));
		table.put("dk", new MilesCashRatio("DKK", 27, 1));
		table.put("ua", new MilesCashRatio("AED", 52, 1));
		table.put("ke", new MilesCashRatio("KRW", 2, 10));
		table.put("cl", new MilesCashRatio("CLP", 2, 10));
		table.put("mx", new MilesCashRatio("MXN", 10, 1));
	}
	
	public Double getCashMilesRatio(String country){
		MilesCashRatio mc = table.get(country);
		Double ratio = null;
		if(mc != null){
			ratio = ((double) mc.cash) / mc.miles; 
		}
		else{
			mc = table.get("com");
			ratio = ((double) mc.cash) / mc.miles;
		}
		return ratio;
	}
	
	public Integer getMilesStep(String country){
		MilesCashRatio mc = table.get(country);
		return mc != null ? mc.miles : table.get("com").miles;
	}

	private class MilesCashRatio{
		String currency;
		int miles;
		int cash;
		
		MilesCashRatio(String currency, int miles, int cash){
			this.miles = miles;
			this.cash = cash;
			this.currency = currency;
		}
	}
	
	public static CashAndMilesCountryTable getInstance(){
		if(instance == null)
			instance = new CashAndMilesCountryTable();
		return instance;
	}

}
