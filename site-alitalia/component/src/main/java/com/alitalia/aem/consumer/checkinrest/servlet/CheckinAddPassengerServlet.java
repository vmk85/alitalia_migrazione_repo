package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.*;
import com.alitalia.aem.common.messages.checkinrest.CheckinAddPassengerResponse;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = { "checkinaddpassenger"}),
        @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
        @Property(name = "sling.servlet.extensions", value = { "json" }),
        @Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinAddPassengerServlet extends GenericCheckinFormValidatorServlet {

    // logger
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    private AlitaliaConfigurationHolder configuration;


    @Reference
    private CheckinSession checkinSession;


    @Override
    protected AlitaliaConfigurationHolder getConfiguration() {
        return configuration;
    }

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        Validator validator = new Validator();

        String ticketNumber = request.getParameter("ticketNumber");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");

        validator.addDirectCondition("ticketNumber", ticketNumber,
                CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

        validator.addDirectCondition("ticketNumber", ticketNumber,
                CheckinConstants.MESSAGE_ERROR_INVALID_PNR, "isPnrOrTicketNumber");

        validator.addDirectCondition("firstName", firstName,
                CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

        validator.addDirectCondition("firstName", firstName,
                CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");

        validator.addDirectCondition("lastName", lastName,
                CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

        validator.addDirectCondition("lastName", lastName,
                CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");


        ResultValidation resultValidation = validator.validate();
        return resultValidation;

    }

    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

        CheckinSessionContext ctx = getCheckInSessionContext(request);

        if (request.getParameter("ticketNumber").length() == 13){
            logger.info("[CheckinAddPassengerServlet] invoco il checkinSession.checkinTicketSearch");
            CheckinAddPassengerResponse result = checkinSession.checkinAddPassenger(request, ctx);
            logger.info("[CheckinAddPassengerServlet] result=" + result);

            PnrListInfoByAddPassenger pnrInfoResult = result.getPnrData();

            if (pnrInfoResult != null)
            {

                if (pnrInfoResult.getPnr() != null)
                {

                    if (pnrInfoResult.getOutcome().equals("OK")) {

                        int x1 = 0;
                        int x2 = 0;
                        int x3 = 0;

                        // Aggiunto il passeggero in base al volo selezionato, controllo che sia lo stesso
                        for (PnrRender pnrSelectedListDataRender : ctx.pnrSelectedListDataRender) {

                            x2 = 0;
                            for (FlightsRender flight : pnrSelectedListDataRender.getFlightsRender()) {

                                x3 = 0;
                                for (SegmentRender segment : flight.getSegments()) {

                                    for (Pnr _pnr : pnrInfoResult.getPnr()) {

                                        for (Flight _flight : _pnr.getFlights()) {
                                            for (Segment _segment : _flight.getSegments()) {

                                                // Se ho la stessa data di partenza, la stessa origine e lo stesso aereo...
                                                if (flight.getOriginalDepartureDate().equals(_flight.getDepartureDate()) && segment.getOrigin().getCode().equals(_segment.getOrigin().getCode()) && segment.getAircraft().equals(_segment.getAircraft())) {

                                                    List<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra> pExtraz = new ArrayList<com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra>();

                                                    if(ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengersExtra() != null) {
                                                        pExtraz = ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengersExtra();
                                                    }

                                                    for (Passenger _passenger : _segment.getPassengers()) {
                                                        if (_passenger.getTicket().equals(request.getParameter("ticketNumber"))) {
                                                            com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra pExtra = new com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra();
                                                            pExtra.setIndex((segment.getPassengersExtra() != null ? segment.getPassengersExtra().size() : 0) + 100); // indice passeggeri extra (+100)
                                                            pExtra.setPassenger(_passenger);
                                                            pExtra.setPnr(_pnr.getNumber());
                                                            pExtra.setNumero(_pnr.getNumber());
                                                            pExtra.setAirline(segment.getAirline());
                                                            pExtra.setOriginCode(segment.getOrigin().getCode());
                                                            pExtra.setDestinationCode(segment.getDestination().getCode());
                                                            pExtra.setFlightNumber(segment.getFlight());
                                                            pExtra.setDepartureDate(segment.getOriginalDepartureDate());
                                                            // segment.addPassengersExtra(pExtra);

                                                            pExtraz.add(pExtra);


                                                        }

                                                    }

                                                    ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).setPassengersExtra(pExtraz);

                                                }

                                            }

                                        }

                                    }
                                    x3 += 1; // segments
                                }
                                x2 += 1; // flights
                            }
                            x1 += 1; // pnr
                        }

                        managementSucces(request, response);
                    }
                    else
                    {
                        TidyJSONWriter json;
                        try {
                            json = new TidyJSONWriter(response.getWriter());

                            response.setContentType("application/json");

                            json.object();
                            json.key("isError").value(true);
                            json.key("errorMessage").value(pnrInfoResult.getOutcome());
                            json.key("sabreStatusCode").value("");
                            json.key("conversationID").value("");
                            json.endObject();

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }
                else
                {

                    if (pnrInfoResult.getOutcome() != null)
                    {
                        TidyJSONWriter json;
                        try {
                            json = new TidyJSONWriter(response.getWriter());

                            response.setContentType("application/json");

                            json.object();
                            json.key("isError").value(true);
                            json.key("errorMessage").value(pnrInfoResult.getOutcome());
                            json.key("sabreStatusCode").value("");
                            json.key("conversationID").value("");
                            json.endObject();

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                        managementError(request, response, result.getPnrData().getError());

                }

            }
            else
                {
                logger.error("[CheckinAddPassengerServlet] - Errore durante l'invocazione del servizio." );
            }
        }

    }

    @SuppressWarnings("rawtypes")
    @Override
    protected Map getAdditionalFailureData(SlingHttpServletRequest request,
                                           GenericFormValidatorServletException exception) {
        Map<String,String> additionalParams = new HashMap<String, String>();
        return additionalParams;
    }

    public void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error){

        logger.info("[CheckinAddPassengerServlet] [managementError] error =" + error );

        Gson gson = new Gson();

        com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);

        TidyJSONWriter json;
        try {
            json = new TidyJSONWriter(response.getWriter());

            response.setContentType("application/json");

            json.object();
            json.key("isError").value(true);
            json.key("errorMessage").value(errorObj.getErrorMessage());
            json.key("sabreStatusCode").value(errorObj.getSabreStatusCode());
            json.key("conversationID").value(errorObj.getConversationID());
            json.endObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response){

        logger.info("[CheckinAddPassengerServlet] [managementSucces] ...");
        String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
                request.getResource(), false)
                + getConfiguration().getCheckinPassengersPage();

        successPage = request.getResourceResolver().map(successPage);

        logger.info("[CheckinAddPassengerServlet] [managementSucces] successPage = " + successPage );

        TidyJSONWriter json;
        try {

            json = new TidyJSONWriter(response.getWriter());
            response.setContentType("application/json");
            json.object();
            json.key("isError").value(false);
            json.key("successPage").value(successPage);
            json.endObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
