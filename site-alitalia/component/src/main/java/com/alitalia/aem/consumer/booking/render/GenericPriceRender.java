package com.alitalia.aem.consumer.booking.render;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

import com.alitalia.aem.consumer.booking.BookingSessionContext;

public class GenericPriceRender{

	private NumberFormat numberFormat;
	private NumberFormat analyticsNumberFormat;
	private BigDecimal fare;
	private BigDecimal fareChildren;
	private BigDecimal fareInfant;
	private BigDecimal taxes;
	private BigDecimal extra;
	private BigDecimal extraCharge;
	private BigDecimal totalPrice;
	private String currency;
	private Map<String,String> simboloValuta;

	
	public GenericPriceRender(BigDecimal fare, String currency, BookingSessionContext ctx) {
		this.fare = fare;
		this.currency = currency;
		init(ctx);
	}
	
	public GenericPriceRender(BigDecimal fare, BigDecimal taxes, BigDecimal extra, String currency, BookingSessionContext ctx) {
		this.fare = fare;
		this.taxes = taxes;
		this.extra = extra;
		this.currency = currency;
		init(ctx);
	}
	
	public GenericPriceRender(BigDecimal fare, BigDecimal taxes, BigDecimal extra, BigDecimal extraCharge, String currency, BookingSessionContext ctx) {
		this.fare = fare;
		this.taxes = taxes;
		this.extra = extra;
		this.extraCharge = extraCharge;
		this.currency = currency;
		init(ctx);
	}
	
	public GenericPriceRender(BigDecimal fare, BigDecimal taxes, BigDecimal extra, BigDecimal extraCharge, BigDecimal totalPrice, String currency, BookingSessionContext ctx) {
		this.fare = fare;
		this.taxes = taxes;
		this.extra = extra;
		this.extraCharge = extraCharge;
		this.totalPrice = totalPrice;
		this.currency = currency;
		init(ctx);
	}
	
	public GenericPriceRender(BigDecimal fare, BigDecimal fareChildren, BigDecimal taxes, BigDecimal extra, BigDecimal extraCharge, BigDecimal totalPrice, String currency, BookingSessionContext ctx) {
		this.fare = fare;
		this.fareChildren = fareChildren;
		this.taxes = taxes;
		this.extra = extra;
		this.extraCharge = extraCharge;
		this.totalPrice = totalPrice;
		this.currency = currency;
		init(ctx);
	}
	
	public GenericPriceRender(BigDecimal fare, BigDecimal fareChildren, BigDecimal fareInfant,  BigDecimal taxes, BigDecimal extra, BigDecimal extraCharge, BigDecimal totalPrice, String currency, BookingSessionContext ctx) {
		this.fare = fare;
		this.fareChildren = fareChildren;
		this.fareInfant = fareInfant;
		this.taxes = taxes;
		this.extra = extra;
		this.extraCharge = extraCharge;
		this.totalPrice = totalPrice;
		this.currency = currency;
		init(ctx);
	}

	private void init(BookingSessionContext ctx){
		//Locale locale = ctx.locale;
		/*this.numberFormat = NumberFormat.getInstance(locale);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);*/
		this.numberFormat = ctx.currentNumberFormat;
		simboloValuta = new HashMap<String,String>();
		simboloValuta.put("EUR","€");
		DecimalFormat format = new DecimalFormat("#0.00");
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setCurrencySymbol(""); 
		format.setDecimalFormatSymbols(otherSymbols);
		format.setGroupingUsed(false);
		format.setPositivePrefix("");
		format.setNegativePrefix("");
		analyticsNumberFormat = format;
	}
	
	public String getCurrency(){
		return this.currency;
	}

	public String getSymbolCurrency(){
		return simboloValuta.get(this.currency);
	}
	
	public String getFare() {
		return numberFormat.format(this.fare.doubleValue());
	}
	
	public String getFareChildren() {
		if (this.fareChildren.compareTo(new BigDecimal(0)) == 0) {
			return "0";
		}
		return numberFormat.format(this.fareChildren.doubleValue());
	}
	
	public String getFareInfant() {
		if (this.fareInfant.compareTo(new BigDecimal(0)) == 0) {
			return "0";
		}
		return numberFormat.format(this.fareInfant.doubleValue());
	}

	public String getTaxes() {
		return numberFormat.format(this.taxes.doubleValue());
	}

	public String getExtra() {
		return numberFormat.format(this.extra.doubleValue());
	}

	public String getExtraCharge() {
		if (this.extraCharge.compareTo(new BigDecimal(0)) == 0) {
			return "0";
		}
		return numberFormat.format(this.extraCharge.doubleValue());
	}
	
	public String getTotalPrice(){
		return numberFormat.format(this.totalPrice.doubleValue());
	}

	public String getAnalyticsFare() {
		return analyticsNumberFormat.format(this.fare);
	}


}
