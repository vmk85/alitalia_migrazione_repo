package com.alitalia.aem.consumer.checkinrest.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.FlightAncillaryOffer;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Emd;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.request.FlightPassenger;
import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.AncillaryOffers;
import com.alitalia.aem.common.data.checkinrest.model.insurance.InsurancePolicy;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSelectedAncillaryOffersRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSelectedAncillaryOffersResponse;
import com.alitalia.aem.common.messages.checkinrest.GetInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.GetInsuranceResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;
import com.alitalia.aem.consumer.checkinrest.render.AncillaryRender;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.alitalia.aem.web.component.webcheckin.delegaterest.IInsuranceDelegate;

@Model(adaptables = { SlingHttpServletRequest.class })
public class AncillaryOffersRenderModel extends GenericCheckinModel{

	@Self
	private SlingHttpServletRequest request;

	@Inject
	private SlingHttpServletResponse response;

	@Inject
	private AlitaliaConfigurationHolder configuration;

	@Inject
	private CheckinSession checkinSession;

	@Inject
	private volatile ICheckinDelegate checkInDelegateRest;

	@Inject
	private volatile IInsuranceDelegate checkInDelegateInsuranceRest;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private AncillaryOffers ancillaryOffers;
	private AncillaryRender ancillaryRender;

	private List<Passenger> passengers;

	private InsurancePolicy insurancePolicy;

	private long totalBags = 0;

	private boolean itc = false;

	private long totalLounge;


	@PostConstruct
	protected void initModel() throws Exception {
		try{
			super.initBaseModel(request);

			if (ctx == null) {
				logger.debug("[AncillaryOffersRenderModel] - Contesto assente.");
				return;
			}
			else {
				
				ancillaryOffers = retrieveAncillaryOffers();
				insurancePolicy = retrieveInsurance();
				ctx.insurancePolicy = insurancePolicy;

				if (ctx.cartTotalAmount.compareTo(BigDecimal.ZERO) == 0)
					checkinSession.checkinSelectedPassengerAfterPayment(request, ctx);

			}

			for(FlightAncillaryOffer flightAncillaryOffer : this.ancillaryOffers.getFlightAncillaryOffers()){
				if (flightAncillaryOffer != null){
					this.totalBags += flightAncillaryOffer.getBagsMax();
				}
			}

			if(ctx.selectedFlights != null){
				for (FlightsRender flightsRender: ctx.selectedFlights){
					if (flightsRender != null){
						for(SegmentRender segmentRender : flightsRender.getSegments()){
							if (segmentRender != null){
								if (segmentRender.getPassengers() == null){
									itc = true;
								}
							}
						}
					}
				}
			}
		}
		catch(Exception ex){
			logger.error("[AncillaryOffersRenderModel] - Unexpected error: ", ex);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	private AncillaryOffers retrieveAncillaryOffers(){
		AncillaryOffers ancillaryOffers = null;

		try{
			CheckinGetSelectedAncillaryOffersRequest serviceRequest = new CheckinGetSelectedAncillaryOffersRequest(IDFactory.getTid(), IDFactory.getSid(request));

			serviceRequest.setConversationID(ctx.conversationId);
			serviceRequest.setLanguage(ctx.language);
			serviceRequest.setMarket(ctx.market);
			serviceRequest.setPnr(ctx.pnr);
			List<FlightPassenger> flightPassengers = new ArrayList<>();

			FlightPassenger flightPassenger;
			com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.request.Passenger passenger;

			if(ctx.selectedFlights != null && ctx.selectedFlights.size() > 0){
				for (int sf = 0; sf < ctx.selectedFlights.size(); sf++){
					for (int sfs = 0; sfs < ctx.selectedFlights.get(sf).getSegments().size(); sfs++) {
						if (ctx.selectedFlights.get(sf).getSegments().get(sfs).getPassengers() != null) {
							List<com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.request.Passenger> passengersList = new ArrayList<>();
							flightPassenger = new FlightPassenger();
							flightPassenger.setFlight(ctx.selectedFlights.get(sf).getSegments().get(sfs).getFlight());


							if (ctx.checkinSelectedPassengers != null && ctx.checkinSelectedPassengers.size() > 0) {
								for (int j = 0; j < ctx.checkinSelectedPassengers.size(); j++) {
									passenger = new com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.request.Passenger();
									passenger.setFirstName(ctx.checkinSelectedPassengers.get(j).getNome());
									passenger.setLastName(ctx.checkinSelectedPassengers.get(j).getCognome());
									passenger.setPassengerID(ctx.checkinSelectedPassengers.get(j).getPassengerID());
									passengersList.add(passenger);
								}
								flightPassenger.setPassengers(passengersList);

								//to the template
								passengers = ctx.checkinSelectedPassengers;
							} else {
								logger.warn("[AncillaryOffersRenderModel][checkinGetSelectedAncillaryOffers] - Non sono presenti passeggeri selezionati in sessione.");
								break;
							}
							flightPassengers.add(flightPassenger);
						}
					}
				}

				serviceRequest.setFlightPassengers(flightPassengers);
				CheckinGetSelectedAncillaryOffersResponse serviceResponce = checkInDelegateRest.retrieveSelectedAncillaryOffers(serviceRequest);

				//Oggetto Data dopo la chiamata al servizio
				ancillaryOffers = serviceResponce.getAncillaryOffers();
				this.ancillaryRender = new AncillaryRender(ctx, ancillaryOffers);

				//Bisogna utilizzare l'oggetto per inserirlo in un model, controller o nel contesto di sessione
				ctx.ancillaryOffers = ancillaryOffers;
				if(ancillaryOffers.getConversationID() != null){
					ctx.conversationId = ancillaryOffers.getConversationID();
				}
			}
			else{
				logger.warn("[AncillaryOffersRenderModel][checkinGetSelectedAncillaryOffers] - Non sono presenti voli selezionati in sessione.");
			}
		}catch(Exception e){
			logger.error("[AncillaryOffersRenderModel][checkinGetSelectedAncillaryOffers] - " + e.getMessage());
		}
		return ancillaryOffers;
	}


	private InsurancePolicy retrieveInsurance(){
		InsurancePolicy res = null;
		if(!("it".equalsIgnoreCase(getMarketCode()))){return null;}
		try{
			GetInsuranceRequest request = new GetInsuranceRequest();
			request.setConversationID(ctx.conversationId);
			request.setLanguage(ctx.language);
			request.setMarket(ctx.language);
			request.setPnr(ctx.pnr);

			GetInsuranceResponse response = checkInDelegateInsuranceRest.getInsurance(request);

			res = response.getInsurancePolicy();
		}
		catch(Exception e){
			logger.error("[AncillaryOffersRenderModel][retrieveInsurance] - " + e.getMessage());
		}
		return res;
	}

	public AncillaryOffers getAncillaryOffers() {
		return ancillaryOffers;
	}

	public void setAncillaryOffers(AncillaryOffers ancillaryOffers) {
		this.ancillaryOffers = ancillaryOffers;
	}

	public InsurancePolicy getInsurancePolicy() {
		return insurancePolicy;
	}

	public void setInsurancePolicy(InsurancePolicy insurancePolicy) {
		this.insurancePolicy = insurancePolicy;
	}

	public List<Passenger> getPassengers() {
		return passengers;
	}

	public AncillaryRender getAncillaryRender() {
		return ancillaryRender;
	}

	public long getTotalBags() {
		return totalBags;
	}

	public void setTotalBags(long totalBags) {
		this.totalBags = totalBags;
	}

	public long getTotalLounge() {
		return totalLounge;
	}

	public void setTotalLounge(long totalLounge) {
		this.totalLounge = totalLounge;
	}

	public boolean isItc() {
		return itc;
	}

	public void setItc(boolean itc) {
		this.itc = itc;
	}
}
