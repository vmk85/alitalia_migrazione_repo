package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.PaymentProviderGlobalCollectData;
import com.alitalia.aem.common.data.home.enumerations.GlobalCollectPaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingPaymentException;
import com.alitalia.aem.consumer.booking.BookingPhaseEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingpurchasereturnconsumer" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }),
		@Property(name = "success.page"), @Property(name = "failure.page") })
@SuppressWarnings("serial")
public class BookingPurchaseReturnServlet extends SlingSafeMethodsServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private BookingSession bookingSession;

	@Override
	protected void doGet(SlingHttpServletRequest request,
						 SlingHttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());

		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);

		try {

			BookingSessionContext ctx = (BookingSessionContext)
					request.getSession(true).getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);

			//Token bancoposta
			if (request.getParameter("token") != null && !request.getParameter("token").equals("")) {
				logger.debug("BookingPurchaseReturnServlet Token bancoposta: "+(String)request.getParameter("token"));
				ctx.paymentData.getProcess().setTokenId((String)request.getParameter("token"));
			} else {
				logger.debug("BookingPurchaseReturnServlet Token bancoposta non presente.");
			}


			logger.debug("ctx.isAuthorizePaymentInvoked: ["+ctx.isAuthorizePaymentInvoked+"]");
			if (!ctx.isAuthorizePaymentInvoked){

				try {
					ResourceBundle resourceBundle = request.getResourceBundle(ctx.locale);
					I18n i18n = new I18n(resourceBundle);
					// complete payment!
					ctx.isAuthorizePaymentInvoked = true;

					if (ctx.paymentData.getType() == PaymentTypeEnum.FINDOMESTIC)
						bookingSession.completePaymentFindomestic(ctx.paymentData, ctx, ctx.paymentTid, i18n);
					else if (ctx.paymentData.getType() == PaymentTypeEnum.PAY_PAL || (ctx.paymentData.getProvider() instanceof PaymentProviderGlobalCollectData && ((PaymentProviderGlobalCollectData)ctx.paymentData.getProvider()).getType() == GlobalCollectPaymentTypeEnum.MAESTRO) || (ctx.paymentData.getType() == PaymentTypeEnum.CREDIT_CARD && ctx.paymentData.getProcess().getRedirectUrl()!=null)){ //3ds
						bookingSession.completePaymentSabre(ctx.paymentData, ctx, ctx.paymentTid, i18n);
					}else
						bookingSession.completePayment(ctx.paymentData, ctx, ctx.paymentTid, null);

				} catch (BookingPaymentException e) {
					logger.error("Error during payment step (Bonifico): ", e);

					// open JSON response
					json.object();
					json.key("result").value(false);

					// evaluate specific error
					switch (e.getCode()) {
						case BookingPaymentException.BOOKING_ERROR_AUTHORIZEPAYMENT :
							manageErrorAuthorizePayment(json, ctx, baseUrl);
							break;
						case BookingPaymentException.BOOKING_ERROR_CHECKPAYMENT :
							manageErrorCheckPayment(json, ctx, baseUrl);
							break;
						case BookingPaymentException.BOOKING_ERROR_RETRIEVETICKETS :
							manageErrorRetrieveTickets(json, ctx, baseUrl);
							break;
						default:
							manageGenericError(ctx, json, baseUrl);
							break;
					}

					// close JSON response
					json.endObject();
					return;
				}
				// open JSON response
				json.object();
				json.key("result").value(true);
				json.key("redirect").value(baseUrl + configuration.getBookingConfirmationPage());
				json.endObject();

			} else {
				json.object();
				json.key("authorizeAlreadyInvoked").value(true);
				json.endObject();
			}

		} catch (Exception e) {
			logger.error("Unexpected", e);

			// open JSON response
			try {
				json.object();
				json.key("result").value(false);
				BookingSessionContext ctx = (BookingSessionContext)
						request.getSession(true).getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
				manageGenericError(ctx, json, baseUrl);
				json.endObject();
			} catch (JSONException e1) {
				logger.error("Error creating JSON ", e1);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}

		}

	}

	/**
	 * Manage error on authorizePayment
	 * @param json
	 * @param typeOfPayment
	 * @throws JSONException
	 */
	private void manageErrorAuthorizePayment(JSONWriter json, BookingSessionContext ctx, String baseUrl) throws JSONException {
		json.key("redirect").value(baseUrl + configuration.getBookingPaymentPage() + "?error=true");
	}

	/**
	 * Manage error on checkPayment
	 * @param json
	 * @throws JSONException
	 */
	private void manageErrorCheckPayment(JSONWriter json, BookingSessionContext ctx, String baseUrl) throws JSONException {
		String redirect = baseUrl + configuration.getBookingConfirmationPage();
		redirect += "?showCallCenterMsg=true";
		json.key("redirect").value(redirect);
		ctx.phase = BookingPhaseEnum.DONE;
	}

	/**
	 * Manage error on retrieveTickets. Both for CDC and Bonifico 
	 * if an error occurs during this payment setp, the ticket was confirmed and the email is sent
	 * @param json
	 * @throws JSONException
	 */
	private void manageErrorRetrieveTickets(JSONWriter json, BookingSessionContext ctx, String baseUrl) throws JSONException {
		String redirect = baseUrl + configuration.getBookingConfirmationPage();
		redirect += "?showCallCenterMsg=true";
		json.key("redirect").value(redirect);
		ctx.phase = BookingPhaseEnum.DONE;
	}

	/**
	 * Manage generic error
	 * @param json
	 * @throws JSONException
	 */
	private void manageGenericError(BookingSessionContext ctx, JSONWriter json, String baseUrl) throws JSONException {
		String redirect;
		if (ctx!= null && ctx.award) {
			redirect = baseUrl + configuration.getBookingAwardPaymentFailurePage();
		}
		else {
			redirect = baseUrl + configuration.getBookingPaymentFailurePage();
		}
		json.key("redirect").value(redirect);
	}
}

