package com.alitalia.aem.consumer.servlet.specialpages;  


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.MillemigliaComplainData;
import com.alitalia.aem.consumer.model.content.specialpages.MillemigliaComplainReasonsData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "millemigliacomplainsubmit" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class MillemigliaComplainServlet extends GenericFormValidatorServlet{
	
	private static final int DIM_FORM = 24;
	private static final int DIM_REASONS = 11;
	private static final String KEYVALUE_SEPARATOR = ":";
	private static final String ELEMENT_SEPARATOR = System.getProperty("line.separator");
	private static final String REASON_SEPARATOR = " , ";
	
	// Static Property For Mails
	private static final String COMPONENT_NAME = "millemiglia_complain";
	private static final String PARSYS_NAME = "page-component";
	private static final String MAIL_FROM_PROPERTY_NAME = "mailFrom";
	private static final String MAIL_TO_PROPERTY_NAME = "mailTo";
	private static final String MAIL_SUBJECT_PROPERTY_NAME = "subject";
	private static final String MAIL_FROM_DEFAULT = "cfaperte@alitalia.it";
	private static final String MAIL_TO_DEFAULT = "cfaperte@dataar.it";
	private static final String MAIL_SUBJECT_DEFAULT = "Cfa Per Te - Italia";
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BusinessLoginService businessLoginService;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		logger.debug("[MillemigliaComplainServlet] validateForm");
		
		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request, new Validator());
			return validator.validate();
			
		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Errore durante la procedura di validazione dati richiesta millemiglia complain", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		logger.debug("[MillemigliaComplainServlet] performSubmit");
		boolean mailResult = sendMail(request);
		logger.debug("Mail Sended = " + mailResult);
		if (mailResult) {
			goBackSuccessfully(request, response);
		}
		else {
			throw new IOException("Service Error");
		}
		
	}
	
	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request, Validator validator) {

		// get parameters from request
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String houseNumber=request.getParameter("house_number");
		String nation=request.getParameter("nation");
		String city=request.getParameter("city");
		String postalCode=request.getParameter("postal_code");
		String email=request.getParameter("email");
		String accetto=request.getParameter("accetto"); 
		String address=request.getParameter("address");
		RequestParameter document=request.getRequestParameter("document");
		
		String filesize;
		if (document.isFormField()) {
			filesize = document.getString();
		} else {
			filesize = String.valueOf(document.getSize());
		}
		
		String permission=request.getParameter("permission");
		
		String[] complainReasons=request.getParameterValues("complain-reason[]");
		
		if (complainReasons == null) {
			complainReasons = new String[0];
		}
	
		// add validate conditions
		
		validator.addDirectConditionMessagePattern("name", name,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");	
		
		validator.addDirectConditionMessagePattern("surname", surname,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.SURNAME_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern("nation", nation,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.MMCOMPLAIN_NATION_ERROR_NOT_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern("city", city,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.MMCOMPLAIN_CITY_ERROR_NOT_EMPTY, "isNotEmpty");
	
		validator.addDirectConditionMessagePattern("postal_code", postalCode,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.MMCOMPLAIN_POSTALCODE_ERROR_NOT_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("address", address,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.MMCOMPLAIN_ADDRESS_ERROR_NOT_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern("permission", permission,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.MMCOMPLAIN_PERMISSION_ERROR_NOT_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern("email", email,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.MMCOMPLAIN_EMAIL_ERROR_NOT_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern("house_number", houseNumber,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.MMCOMPLAIN_EMAIL_ERROR_NOT_EMPTY, "isNotEmpty");
		
		validator.addCrossConditionMessagePattern("document", filesize,
				String.valueOf(3*1024*1024+1),
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.MMCOMPLAIN_FILE_SIZE_NOT_VALID,
				"isLessThan");
		String complainReasonLength=complainReasons.length+"";
		validator.addCrossConditionMessagePattern("complain-reason[]",
				complainReasonLength,  "0",
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.REASON_ERROR_EMPTY, "isMoreThan");
		
		validator.addCrossConditionMessagePattern("accetto", accetto, "accepted", 
				I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT,
				I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "areEqual");
		
		return validator;

	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		logger.debug("[ MillemigliaComplainServlet] saveDataIntoSession");
		
		// create baggageClaimData object and save it into session
		MillemigliaComplainData millemigliaComplainData = new MillemigliaComplainData();
		String[]reasons= request.getParameterValues("complain-reason[]");
		
		Class<MillemigliaComplainReasonsData> c = MillemigliaComplainReasonsData.class;
		MillemigliaComplainReasonsData mmcomplainReasons=new MillemigliaComplainReasonsData();
		if (reasons != null) {
			for (String el: reasons){
				try {
					c.getMethod("set" + el, String.class).invoke(mmcomplainReasons, el);
				} catch (Exception e1) {
					logger.error("[ MillemigliaComplainServlet] saveDataIntoSession" + e1.toString());
				}
			}
		}
			
		millemigliaComplainData.setAccepted(request.getParameter("accetto"));
		millemigliaComplainData.setAddress(request.getParameter("address"));
		millemigliaComplainData.setCity(request.getParameter("city"));
		millemigliaComplainData.setClasse(request.getParameter("class"));
		millemigliaComplainData.setEmail(request.getParameter("email"));
		millemigliaComplainData.setFlight(request.getParameter("AZ_flight"));
		millemigliaComplainData.setHouseNumber(request.getParameter("house_number"));
		millemigliaComplainData.setMillemiglia(request.getParameter("millemiglia"));
		millemigliaComplainData.setMillemigliaProfile(request.getParameter("mmtype"));
		millemigliaComplainData.setName(request.getParameter("name"));
		millemigliaComplainData.setNation(request.getParameter("nation"));
		millemigliaComplainData.setNote(request.getParameter("note"));
		millemigliaComplainData.setTicketNumber(request.getParameter("ticket_number"));
		millemigliaComplainData.setOfficeNumber(request.getParameter("office_number" ));
		millemigliaComplainData.setHomeNumber(request.getParameter("home_number" ));
		millemigliaComplainData.setPermission(request.getParameter("permission" ));
		millemigliaComplainData.setPhoneNumber(request.getParameter("telephone_number" ));
		millemigliaComplainData.setPir(request.getParameter("pir" ));
		millemigliaComplainData.setPnr(request.getParameter("pnr"));
		millemigliaComplainData.setPostalCode(request.getParameter("postal_code"));
		millemigliaComplainData.setSeat(request.getParameter("seat"));
		millemigliaComplainData.setSurname(request.getParameter("surname"));
		millemigliaComplainData.setReasons(mmcomplainReasons);
		
		try {
			millemigliaComplainData.setDay(Integer.parseInt(request.getParameter("day")));
		} catch (NumberFormatException n) { }
		try {
			millemigliaComplainData.setMonth(Integer.parseInt(request.getParameter("month")));
		} catch (NumberFormatException n) { }
		try {
			millemigliaComplainData.setYear(Integer.parseInt(request.getParameter("year")));
		} catch (NumberFormatException n) { }
		
		// go back to form with error
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		
		millemigliaComplainData.setError(i18n.get(I18nKeySpecialPage.ERROR_SERVICE));
		
		request.getSession().setAttribute(MillemigliaComplainData.NAME,
				millemigliaComplainData);

	}
	
	/*
	 * sendMail
	 * 
	 */
	private boolean sendMail(SlingHttpServletRequest request) {
		logger.debug("[MillemigliaComplainServlet] sendMail");
		
		// current page
		Page currentPage = AlitaliaUtils.getPage(request.getResource());
		
		// email from
	    String mailFrom = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_FROM_PROPERTY_NAME);
	    if (mailFrom == null) {
	    	mailFrom = MAIL_FROM_DEFAULT;
	    }
	    
	    // email to
	    String mailTo = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_TO_PROPERTY_NAME);
	    if (mailTo == null) {
	    	mailTo = MAIL_TO_DEFAULT;
	    }
	    
	    // email subject
	    String subject = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_SUBJECT_PROPERTY_NAME);
	    if (subject == null) {
	    	subject = MAIL_SUBJECT_DEFAULT;
	    }

	    // email body
	    String messageText = generateBodyMail(request);
	    
	    logger.debug("[ MillemigliaKidsServlet] sendMail -- Email data: "
				+ "[From: {" + mailFrom + "}] - [To: {" + mailTo + "}] - "
				+ "[Subject: {" + subject + "}] - [Body: {" + messageText + "}]");
	    
	    // create email message object
		EmailMessage emailMessage = new EmailMessage();
		emailMessage.setFrom(mailFrom);
		emailMessage.setTo(mailTo);
		emailMessage.setSubject(subject);
		emailMessage.setMessageText(messageText);
		emailMessage.setIsBodyHtml(false);
		emailMessage.setDeliveryNotification(DeliveryNotificationOptionsEnum.NONE);
		emailMessage.setPriority(MailPriorityEnum.NORMAL);

		// create AgencySendMailRequest
		AgencySendMailRequest mailRequest = new AgencySendMailRequest();
		mailRequest.setTid(IDFactory.getTid());
		mailRequest.setSid(IDFactory.getSid(request));
		mailRequest.setEmailMessage(emailMessage);
		
		// get AgencySendMailResponse
		AgencySendMailResponse mailResponse =
				businessLoginService.sendMail(mailRequest);
		
		// return status
		return mailResponse.isSendMailSuccessful();
		
	}
	
	/*
	 * generateBodyMail
	 * 
	 */
	private String generateBodyMail(SlingHttpServletRequest request) {
		logger.debug("[MillemigliaComplainServlet] generateBodyMail");
		
		String[][] data = new String[DIM_FORM][2];
		
		// creating Keys
		int k;
		for (k = 0; k < DIM_FORM; k++) {
			String name = "KeyField" + Integer.toString(k);
			data[k][0] = request.getParameter(name);
		}
		
		// get I18N
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		I18n i18n = new I18n(resourceBundle);
		
		// manage Reasons
		String[][] mapReasons = generateComplainReasons();
		String[] complainReasons = request.getParameterValues("complain-reason[]");
		String reasons = calculateReasons(complainReasons, mapReasons, i18n);
		
		// getting Values
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String nation = request.getParameter("nation");
		String city = request.getParameter("city");
		String postal_code = request.getParameter("postal_code");
		String address = request.getParameter("address");
		String house_number = request.getParameter("house_number");
		String email = request.getParameter("email");
		String home_number = request.getParameter("home_number");
		String office_number = request.getParameter("office_number");
		String telephone_number = request.getParameter("telephone_number");
		String millemiglia = request.getParameter("millemiglia");
		String mmtype = request.getParameter("mmtype");
		String az_flight = request.getParameter("AZ_flight");
		String f_class = request.getParameter("class");
		String day = request.getParameter("day");
		String month = request.getParameter("month");
		String year = request.getParameter("year");
		String seat = request.getParameter("seat");
		String pnr = request.getParameter("pnr");
		String ticket_number = request.getParameter("ticket_number");
		String pir = request.getParameter("pir");
		String note = request.getParameter("note");
		RequestParameter document = request.getRequestParameter("document");
		String accetto = request.getParameter("accetto");
		
		// setting Values
		data[0][1] = reasons;
		data[1][1] = (null != name) ? name : "";
		data[2][1] = (null != surname) ? surname : "";
		data[3][1] = (null != nation) ? nation : "";
		data[4][1] = (null != city) ? city : "";
		data[5][1] = (null != postal_code) ? postal_code : "";
		data[6][1] = (null != address) ? address : "";
		data[7][1] = (null != house_number) ? house_number : "";
		data[8][1] = (null != email) ? email : "";
		data[9][1] = (null != home_number) ? home_number : "";
		data[10][1] = (null != office_number) ? office_number : "";
		data[11][1] = (null != telephone_number) ? telephone_number : "";
		data[12][1] = (null != millemiglia) ? millemiglia : "";
		data[13][1] = (null != mmtype) ? mmtype : "";
		data[14][1] = (null != az_flight) ? az_flight : "";
		data[15][1] = (null != f_class) ? f_class : "";
		data[16][1] = ""; // (day of birth);
		data[17][1] = (null != seat) ? seat : "";
		data[18][1] = (null != pnr) ? pnr : "";
		data[19][1] = (null != ticket_number) ? ticket_number : "";
		data[20][1] = (null != pir) ? pir : "";
		data[21][1] = (null != note) ? note : "";
		data[22][1] = (null != document) ? document.getFileName() : "";
		data[23][1] = (null != accetto) ? accetto : "";
		
		// setting Day of Birth
		day = (null != day) ? day : ""; 
		month = (null != month) ? month : "";
		year = (null != year) ? year : "";
		if (!"".equals(day) && !"".equals(month) && !"".equals(year)) {
			try {
				List<String> months = new ArrayList<String>();
				months = I18nKeyCommon.getMonths();
				int numberMonth = Integer.parseInt(month);
				String keyMonth = months.get(numberMonth - 1);
				month = i18n.get(keyMonth);
				data[16][1] = day + "/" + month + "/" + year;
			} catch (NumberFormatException e) { }
		}
		
		//  creating email message and return it
		String emailText = "";
		for (k = 0; k < DIM_FORM; k++) {
			emailText = emailText + data[k][0] + KEYVALUE_SEPARATOR + data[k][1];
			if (k < DIM_FORM - 1) {
				emailText = emailText + ELEMENT_SEPARATOR;
			}
		}
		return emailText;
		
	}

	/*
	 * generateComplainReasons
	 * 
	 */
	private String[][] generateComplainReasons() {
		logger.debug("[MillemigliaComplainServlet] generateComplainReasons");
		
		// il  primo elemento della coppia e' il valore nella request,
		// il secondo elemento e' la chiave i18n
		String[][]  complainReasons = new String[DIM_REASONS][2];
		
		complainReasons[0][0] = "BookingBuying";
		complainReasons[0][1] = "specialpage.reason.bookingBuying.checkbox";
		
		complainReasons[1][0] = "LostLuggage";
		complainReasons[1][1] = "specialpage.reason.lostbuggage.checkbox";
		
		complainReasons[2][0] = "Delay";
		complainReasons[2][1] = "specialpage.reason.delay.checkbox";
		
		complainReasons[3][0] = "DamagedLuggage";
		complainReasons[3][1] = "specialpage.reason.damagedluggage.checkbox";
		
		complainReasons[4][0] = "Cancellation";
		complainReasons[4][1] = "specialpage.reason.cancellation.checkbox";
		
		complainReasons[5][0] = "Overbooking";
		complainReasons[5][1] = "specialpage.reason.overbooking.checkbox";
		
		complainReasons[6][0] = "WebServices";
		complainReasons[6][1] = "specialpage.reason.webservices.checkbox";
		
		complainReasons[7][0] = "TamperingLuggage";
		complainReasons[7][1] = "specialpage.reason.tamperingbuggage.checkbox";
		
		complainReasons[8][0] = "DelayedLuggage";
		complainReasons[8][1] = "specialpage.reason.delayedbuggage.checkbox";
		
		complainReasons[9][0] = "Millemiglia";
		complainReasons[9][1] = "specialpage.reason.millemiglia.checkbox";
		
		complainReasons[10][0] = "Other";
		complainReasons[10][1] = "specialpage.reason.other.checkbox";
		
		return complainReasons;

	}
	
	/*
	 * calculateReasons
	 * 
	 */
	private String calculateReasons(String[] complainReasons, String[][] map,
			I18n i18n) {
		logger.debug("[MillemigliaComplainServlet] calculateReasons");
		
		StringBuilder reasons = new StringBuilder();
		int k;
		for (k = 0; k < complainReasons.length; k++) {
			String key = findI18NKey(map, complainReasons[k]);
			String reason = i18n.get(key);
			reasons.append(reason);
			if (k < complainReasons.length - 1) {
				reasons.append(REASON_SEPARATOR);
			}
		}
		return reasons.toString();
		
	}
	
	/*
	 * findI18NKey
	 * 
	 */
	private String findI18NKey(String[][] map, String value) {
		logger.debug("[MillemigliaComplainServlet] findI18NKey");

		int k;
		for (k = 0; k < DIM_REASONS; k++) {
			if (map[k][0].equals(value)) {
				return map[k][1];
			}
		}
		return "";
	}
	
	/*
	 * getComponentProperty
	 * 
	 */
	private String getComponentProperty(Page ancestor, String parsysName,
			String compName, String propName) {
		logger.debug("[MillemigliaComplainServlet] getComponentProperty");
		
		Resource pageResource = ancestor.getContentResource();	
		String prop = null;
		try {
			prop = pageResource.getChild(parsysName).getChild(compName)
					.getValueMap().get(propName, String.class);
		} catch(Exception e) {}
		
		return prop;
	}

}