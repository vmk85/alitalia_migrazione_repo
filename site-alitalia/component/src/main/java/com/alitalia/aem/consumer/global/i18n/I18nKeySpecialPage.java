package com.alitalia.aem.consumer.global.i18n;

import java.util.List;

import com.alitalia.aem.consumer.global.enumeration.ComfortSeatsClaimReasons;

public class I18nKeySpecialPage {

	// BAGGAGE_CLAIM
	public static final String PASSENGER_INFO = "specialpage.passengerinfo.label";
	public static final String REQUIRED_FIELDS = "specialpage.requiredfields.label";
	public static final String NAME = "specialpage.name.label";
	public static final String NAME_ERROR_EMPTY = "specialpage.name.error.empty";
	public static final String NAME_ERROR_NOT_VALID = "specialpage.name.error.notvalid";
	public static final String SURNAME = "specialpage.surname.label";
	public static final String SURNAME_ERROR_EMPTY = "specialpage.surname.error.empty";
	public static final String SURNAME_ERROR_NOT_VALID = "specialpage.surname.error.notvalid";
	public static final String PHONE_NUMBER = "specialpage.phonenumber.label";
	public static final String PHONE_NUMBER_ERROR_EMPTY = "specialpage.phonenumber.error.empty";
	public static final String PHONE_NUMBER_ERROR_NOT_VALID = "specialpage.phonenumber.error.notvalid";	
	public static final String EMAIL = "specialpage.email.label";
	public static final String EMAIL_ERROR_EMPTY = "specialpage.email.error.empty";
	public static final String EMAIL_ERROR_NOT_VALID = "specialpage.email.error.notvalid";
	public static final String FLIGHT_INFO = "specialpage.flightinfo.label";
	public static final String FLIGHT_CARRIER = "specialpage.flightcarrier.label";
	public static final String FLIGHT_CARRIER_ERROR_EMPTY = "specialpage.flightcarrier.error.empty";
	public static final String FLIGHT_CARRIER_ERROR_NOT_VALID = "specialpage.flightcarrier.error.notvalid";
	public static final String TICKET_NUMBER = "specialpage.ticketnumber.label";
	public static final String TICKET_NUMBER_ERROR_EMPTY = "specialpage.ticketnumber.error.empty";
	public static final String TICKET_NUMBER_ERROR_NOT_VALID = "specialpage.ticketnumber.error.notvalid";
	public static final String FLIGHT_NUMBER = "specialpage.flightnumber.label";
	public static final String FLIGHT_NUMBER_ERROR_EMPTY = "specialpage.flightnumber.error.empty";
	public static final String FLIGHT_NUMBER_ERROR_NOT_VALID = "specialpage.flightnumber.error.notvalid";
	public static final String FLIGHT_DAY = "specialpage.flightday.label";
	public static final String FLIGHT_DAY_ERROR_EMPTY = "specialpage.ticketnumber.error.empty";
	public static final String FLIGHT_DAY_ERROR_NOT_VALID = "specialpage.ticketnumber.error.notvalid";
	public static final String FLIGHT_MONTH = "specialpage.flightmonth.label";
	public static final String FLIGHT_MONTH_ERROR_EMPTY = "specialpage.ticketnumber.error.empty";
	public static final String FLIGHT_MONTH_ERROR_NOT_VALID = "specialpage.ticketnumber.error.notvalid";
	public static final String FLIGHT_YEAR = "specialpage.flightyear.label";
	public static final String FLIGHT_YEAR_ERROR_EMPTY = "specialpage.ticketnumber.error.empty";
	public static final String FLIGHT_YEAR_ERROR_NOT_VALID = "specialpage.ticketnumber.error.notvalid";
	public static final String FLIGHT_DATE = "specialpage.flightdate.label";
	public static final String FLIGHT_DATE_ERROR_EMPTY = "specialpage.flightdate.error.empty";
	public static final String FLIGHT_DATE_ERROR_NOT_VALID = "specialpage.flightdate.error.notvalid";
	public static final String FLIGHT_DATE_ERROR_FUTURE = "specialpage.flightdate.error.future";
	public static final String REASON = "specialpage.reason.label";
	public static final String REASON_ERROR_EMPTY = "specialpage.reason.error.empty";
	public static final String REASON_ERROR_NOT_VALID = "specialpage.reason.error.notvalid";
	public static final String PIR = "specialpage.pir.label"; // PIR: Property Irregularity Report (reporting lost/damaged luggage at an airport)
	public static final String PIR_ERROR_EMPTY = "specialpage.pir.error.empty";
	public static final String PIR_ERROR_NOT_VALID = "specialpage.pir.error.notvalid";
	public static final String PIN_ERROR_NOT_VALID = "specialpage.pin.error.notvalid";
	
	public static final String CUSTOMER_ALREADY_EXISTS = "specialpage.subscribenewsletter.error.exists";
	public static final String CUSTOMER_NOT_EXISTS = "specialpage.subscribenewsletter.error.not.exists";
	public static final String COMPANYNAME_TOO_LONG = "specialpage.subscribesme.error.companynametoolong";
	
	
	public static final String MAX_FREQUENT_FLYERS_ERROR = "specialpage.frequentflyers.toomuch";
	public static final String MILLEMIGLIACODE_ERROR_EMPTY = "specialpage.millemigliacode.empty";
	public static final String MILLEMIGLIACODE_ERROR_NOT_VALID = "specialpage.millemigliacode.notvalid";
	public static final String MILLEMIGLIAALIAS_ERROR_NOT_VALID = "specialpage.millemigliaalias.notvalid";
	public static final String MILLEMIGLIAPASSWORD_ERROR_NOT_VALID = "specialpage.millemigliapassword.notvalid";
	public static final String MILLEMIGLIACONFERMAPASSWORD_ERROR_NOT_VALID = "specialpage.millemigliaconfermapassword.notvalid";
	public static final String PROMOCODE_ERROR_EMPTY = "specialpage.promocode.empty";
	public static final String PROMOCODE_ERROR_NOT_VALID = "specialpage.promocode.notvalid";
	
	// Millemiglia kids
	public static final String BIRTH_DAY_ERROR_NOT_VALID = "special.birthday.message.notvalid";
	public static final String BIRTH_DAY_ERROR_NOT_EMPTY = "specialpage.birthday.message.empty";
	public static final String GENDER_ERROR_EMPTY = "specialpage.gender.message.empty";

	// COOKIES
	public static final String COOKIES_MARKETING_COOKIE = "specialpage.cookies.marketing.label";
	public static final String COOKIES_MARKETING_COOKIE_NOT_VALID = "specialpage.cookies.marketing.error";
	public static final String COOKIES_THIRDPARTY_COOKIE = "specialpage.cookies.marketing.label";
	public static final String COOKIES_THIRDPARTY_COOKIE_NOT_VALID = "specialpage.cookies.thirdparty.error";
	public static final String COOKIES_ALLOW = "specialpage.cookies.allow.label";
	public static final String COOKIES_DENY = "specialpage.cookies.deny.label";

	// INVOICE
	public static final String INVOICE_TYPE = "specialpage.cookies.type.label";
	public static final String INVOICE_TYPE_ERROR_EMPTY = "specialpage.cookies.type.error.empty";
	public static final String INVOICE_TYPE_ERROR_NOT_VALID = "specialpage.cookies.type.error.notvalid";
	public static final String INVOICE_NAME = "specialpage.cookies.name.label";
	public static final String INVOICE_NAME_ERROR_EMPTY = "specialpage.cookies.name.error.empty";
	public static final String INVOICE_NAME_ERROR_NOT_VALID = "specialpage.cookies.name.error.notvalid";
	public static final String INVOICE_SURNAME = "specialpage.cookies.surname.label";
	public static final String INVOICE_SURNAME_ERROR_EMPTY = "specialpage.cookies.surname.error.empty";
	public static final String INVOICE_SURNAME_ERROR_NOT_VALID = "specialpage.cookies.surname.error.notvalid";
	public static final String INVOICE_CODE = "specialpage.cookies.code.label";
	public static final String INVOICE_CODE_ERROR_EMPTY = "specialpage.cookies.code.error.empty";
	public static final String INVOICE_CODE_ERROR_NOT_VALID = "specialpage.cookies.code.error.notvalid";
	public static final String INVOICE_ADDRESS = "specialpage.cookies.address.label";
	public static final String INVOICE_ADDRESS_ERROR_EMPTY = "specialpage.cookies.address.error.empty";
	public static final String INVOICE_ADDRESS_ERROR_NOT_VALID = "specialpage.cookies.address.error.notvalid";
	public static final String INVOICE_CAP = "specialpage.cookies.cap.label";
	public static final String INVOICE_CAP_ERROR_EMPTY = "specialpage.cookies.cap.error.empty";
	public static final String INVOICE_CAP_ERROR_NOT_VALID = "specialpage.cookies.cap.error.notvalid";
	public static final String INVOICE_CITY = "specialpage.cookies.city.label";
	public static final String INVOICE_CITY_ERROR_EMPTY = "specialpage.cookies.city.error.empty";
	public static final String INVOICE_CITY_ERROR_NOT_VALID = "specialpage.cookies.city.error.notvalid";
	public static final String INVOICE_COMPANY_ERROR_EMPTY = "specialpage.cookies.company.error.empty";
	public static final String INVOICE_NATION = "specialpage.cookies.nation.label";
	public static final String INVOICE_NATION_ERROR_EMPTY = "specialpage.cookies.nation.error.empty";
	public static final String INVOICE_NATION_ERROR_NOT_VALID = "specialpage.cookies.nation.error.notvalid";
	public static final String INVOICE_PROVINCE = "specialpage.cookies.province.label";
	public static final String INVOICE_PROVINCE_ERROR_EMPTY = "specialpage.cookies.province.error.empty";
	public static final String INVOICE_PROVINCE_ERROR_NOT_VALID = "specialpage.cookies.province.error.notvalid";
	public static final String INVOICE_EMAIL = "specialpage.cookies.email.label";
	public static final String INVOICE_EMAIL_ERROR_EMPTY = "specialpage.cookies.email.error.empty";
	public static final String INVOICE_EMAIL_ERROR_NOT_VALID = "specialpage.cookies.email.error.notvalid";
	public static final String INVOICE_PHONE = "specialpage.cookies.phone.label";
	public static final String INVOICE_PHONE_ERROR_EMPTY = "specialpage.cookies.phone.error.empty";
	public static final String INVOICE_PHONE_ERROR_NOT_VALID = "specialpage.cookies.phone.error.notvalid";
	public static final String INVOICE_TICKETNUMBER = "specialpage.cookies.ticketnumber.label";
	public static final String INVOICE_TICKETNUMBER_ERROR_EMPTY = "specialpage.cookies.ticketnumber.error.empty";
	public static final String INVOICE_TICKETNUMBER_ERROR_NOT_VALID = "specialpage.cookies.ticketnumber.error.notvalid";
	public static final String INVOICE_JOURNEY = "specialpage.cookies.journey.label";
	public static final String INVOICE_JOURNEY_ERROR_EMPTY = "specialpage.cookies.journey.error.empty";
	public static final String INVOICE_JOURNEY_ERROR_NOT_VALID = "specialpage.cookies.journey.error.notvalid";
	public static final String INVOICE_PRICE = "specialpage.cookies.price.label";
	public static final String INVOICE_PRICE_ERROR_EMPTY = "specialpage.cookies.price.error.empty";
	public static final String INVOICE_PRICE_ERROR_NOT_VALID = "specialpage.cookies.price.error.notvalid";
	public static final String INVOICE_FARE = "specialpage.cookies.fare.label";
	public static final String INVOICE_FARE_ERROR_EMPTY = "specialpage.cookies.fare.error.empty";
	public static final String INVOICE_FARE_ERROR_NOT_VALID = "specialpage.cookies.fare.error.notvalid";
	public static final String INVOICE_TAXES = "specialpage.cookies.taxes.label";
	public static final String INVOICE_TAXES_ERROR_EMPTY = "specialpage.cookies.taxes.error.empty";
	public static final String INVOICE_TAXES_ERROR_NOT_VALID = "specialpage.cookies.taxes.error.notvalid";

	public static final String COND_ERROR_NOT_ACCEPT = "specialpage.condition.error.notaccepted";

	// MILLEMIGLIA COMPLAIN

	public static final String MMCOMPLAIN_NATION_ERROR_NOT_EMPTY = "specialpage.nation.error.empty";
	public static final String MMCOMPLAIN_CITY_ERROR_NOT_EMPTY = "specialpage.city.error.empty";
	public static final String MMCOMPLAIN_POSTALCODE_ERROR_NOT_EMPTY = "specialpage.postalcode.error.empty";
	public static final String MMCOMPLAIN_ADDRESS_ERROR_NOT_EMPTY = "specialpage.address.error.empty";
	public static final String MMCOMPLAIN_EMAIL_ERROR_NOT_EMPTY = "specialpage.email.error.empty";
	public static final String MMCOMPLAIN_FILE_SIZE_NOT_VALID = "specialpage.filesize.error.empty";
	public static final String MMCOMPLAIN_PERMISSION_ERROR_NOT_EMPTY = "specialpage.permission.error.empty";

	// COMMON
	public static final String BUTTON_SEND = "specialpage.button.send";
	public static final String BUTTON_SAVE = "specialpage.button.send";
	public static final String REQUIRED_FIELD = "specialpages.required.field";

	// ERROR MESSAGES
	public static final String ERROR_MESSAGE_MILLEMIGLIA_LOGIN = "specialpage.error.message.millemiglialogin";
	public static final String ERROR_MESSAGE_MILLEMIGLIA_REGISTRATI = "specialpage.error.message.millemigliaregistrati";
	public static final String ERROR_MESSAGE_MILLEMIGLIA_RECUPERACREDENZIALI = "specialpage.error.message.millemigliarecuperacredenziali";
	public static final String ERROR_SERVICE = "specialpage.error.service";
	public static final String ERROR_SERVICE_MILLMIGLIA_SUBSCRIBE = "specialpage.error.subscribe";
	public static final String ERROR_MESSAGE_NEWSLETTER_NOT_REGISTERED_USER = "specialpage.error.message.newsletterusernotregistered";
	public static final String ERROR_MESSAGE_MILLEMIGLIA_RESETACCOUNT = "specialpage.error.message.resetaccount";

	//MAIL NEWSLETTER
	public static final String SUBJECT_MAIL_NEWSLETTER = "millemiglia.newsletter.mail.subject";

	//OTP SMS SERVICE
	public static final String OTP_TEXT_MESSAGE = "mypersonalarea.otp.textmessage";

	//OTP EMAIL MESSAGE
	public static final String OTP_MAIL_FROM = "mypersonalarea.otp.mail.from";
	public static final String OTP_MAIL_SUBJECT = "mypersonalarea.otp.mail.subject";
	public static final String OTP_MAIL_MESSAGETEXT = "mypersonalarea.otp.mail.messagetext";


	//MILLEMIGLIA SECURITY ALERT MESSAGE
	public static final String MMSA_SECURITY_ALERT_MAIL_FROM = "millemiglia.security.alert.mail.from";
	public static final String MMSA_SECURITY_ALERT_MAIL_SUBJECT = "millemiglia.security.alert.mail.subject";


	public static final List<String> getComfortSeatsRefundReasons() {
		List<String> reasons = ComfortSeatsClaimReasons.listI18nValues();
		return reasons;
	}


}
