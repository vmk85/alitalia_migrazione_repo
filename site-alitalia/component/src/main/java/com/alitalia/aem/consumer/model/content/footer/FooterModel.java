package com.alitalia.aem.consumer.model.content.footer;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;

@Model(adaptables = { SlingHttpServletRequest.class })
public class FooterModel {

	private Logger logger = LoggerFactory.getLogger(FooterModel.class);
	
	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	@Inject
	private Page currentPage;

	private String resourcePath;
	
	@PostConstruct
    protected void initModel() {
		logger.debug("Initializing model << FooterModel >>");
        InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(currentPage.getContentResource());
        String definedFooterResourcePath = ivm.getInherited("footerpath", String.class);
        if (definedFooterResourcePath != null) {
	        final Page footerPage = currentPage.getContentResource().getResourceResolver()
	        		.getResource(definedFooterResourcePath).adaptTo(Page.class);
	        if (footerPage != null) {
		        resourcePath = footerPage.getContentResource().getPath();
	        }
        }
	}
	
	public String getResourcePath(){
		return resourcePath;
	}
}
