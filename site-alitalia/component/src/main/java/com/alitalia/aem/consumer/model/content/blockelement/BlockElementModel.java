package com.alitalia.aem.consumer.model.content.blockelement;

import com.day.cq.wcm.api.Page;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
 
import javax.jcr.Node; 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables={Resource.class})
public class BlockElementModel {
	@Inject @Default(values="")
    private String risorsa;

    @Self
    private Resource resource;

    private Logger logger = LoggerFactory.getLogger(BlockElementModel.class);
    private String boxPath;
    private String resourceType;

    /**
     * E' l'equivalente di un costruttore di classe. In questo caso viene utilizzato per inizializzare
     * i valori di path e resourceType a cui deve puntare il componente.
     */
    @PostConstruct
    protected void initModel() {
    	if(risorsa != null && !risorsa.isEmpty()) {
    		//logger.info("Initializing model << BlockElement >>");
            try {
                final ResourceResolver resolver = resource.getResourceResolver();       
                final Page targetPage = resolver.getResource(risorsa).adaptTo(Page.class);
                final Node contentNode = targetPage.getContentResource("box").adaptTo(Node.class);
                if(contentNode != null){
                	boxPath = contentNode.getPath();
                    resourceType = contentNode.getProperty("sling:resourceType").getString();
                } else{
                	boxPath="box non trovato";
                	resourceType = "res non trovato";
                }
                
            } catch(Exception e) {
                logger.error("Unable to get the values: \n" + e);
                boxPath = "box errore";
                resourceType = "res errore";
            }
    	} else{
    		logger.debug("Risorsa vuota");
    	}
    }

    /**
     * Restituisce il path del componente
     */
    public String getBoxPath() {
        return boxPath;
    }

    /**
     * Restituisce il resource type del componente
     */
    public String getResourceType() {
        return resourceType;
    }
}

