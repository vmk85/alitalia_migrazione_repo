package com.alitalia.aem.consumer.booking.render;

import java.math.BigDecimal;
import java.util.Locale;

import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.common.data.home.BrandData;

public class AvailableFlightBrandSelectionRender{

	private BrandData brandData;
	private BrandRender brand;
	private GenericPriceRender flightFare;
	private boolean isBetter;
	private int seatsAvailable;
	private String solutionId;
	private boolean isEnabled;
	private boolean isSelected;
	private String awardPrice;
	private int indexToShow; // The index in ctx.availableFlights
	
	public AvailableFlightBrandSelectionRender(BrandData brandData, Locale locale, boolean isSelected, int index, BookingSessionContext ctx){
		this.brandData = brandData;
		this.brand = new BrandRender(this.brandData, ctx);
		this.flightFare = null;
		this.isBetter = false;
		this.seatsAvailable = 0;
		this.solutionId = "";
		this.isEnabled = true;
		this.isSelected = isSelected;
		this.indexToShow = index;
		init(locale,ctx);
	}
	
	public AvailableFlightBrandSelectionRender(BrandPageData page, Locale locale, boolean isSelected, int index, BookingSessionContext ctx){
		this.brandData = null;
		this.brand = new BrandRender(page, ctx);
		this.flightFare = null;
		this.isBetter = false;
		this.seatsAvailable = 0;
		this.solutionId = "";
		this.isEnabled = true;
		this.isSelected = isSelected;
		this.indexToShow = index;
		init(locale,ctx);
	}
	
	private void init(Locale locale, BookingSessionContext ctx){
		this.flightFare = computeFlightFare(ctx);
		this.isBetter = this.brandData != null && this.brandData.isBestFare();
		this.isEnabled = this.brandData != null && this.brandData.isEnabled();
		this.seatsAvailable = this.brandData == null ? 0 : this.brandData.getSeatsAvailable();
		this.solutionId = computeSolutionId();
		this.awardPrice = computeAwardPrice();
	}
	

	public BrandData getBrandData() {
		return brandData;
	}

	public BrandRender getBrand() {
		return brand;
	}

	public GenericPriceRender getFlightFare() {
		return flightFare;
	}
	
	public void setFlightFare(GenericPriceRender flightFare) {
		this.flightFare = flightFare;
	}

	public boolean isBetter() {
		return isBetter;
	}

	public int getSeatsAvailable() {
		return seatsAvailable;
	}

	public String getSolutionId() {
		return solutionId;
	}
	
	public boolean isEnabled() {
		return isEnabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.isEnabled = enabled;
	}
	
	public boolean isSelected() {
		return isSelected;
	}
	
	public String getAwardPrice() {
		return awardPrice;
	}
	
	public int getIndexToShow() {
		return indexToShow;
	}
	
	/*private methods to build the Render*/
	
	private GenericPriceRender computeFlightFare(BookingSessionContext ctx) {
		if (this.brandData == null) {
			return null;
		}
		BigDecimal fare = this.brandData.getGrossFare();
		return new GenericPriceRender(fare, this.brandData.getCurrency(), ctx);
	}
	
	private String computeSolutionId() {
		if (this.brandData == null) {
			return null;
		} else if (this.brandData.getRefreshSolutionId()!=null && !("").equals(this.brandData.getRefreshSolutionId())) {
			return this.brandData.getRefreshSolutionId();
		} else {
			return this.brandData.getSolutionId();
		}
	}
	
	private String computeAwardPrice() {
		if (this.brandData == null) {
			return null;
		}
		BigDecimal price = this.brandData.getAwardPrice();
		if (price != null) {
			return String.valueOf(Math.round(price.doubleValue()));
		}
		return null;
	}
}
