package com.alitalia.aem.consumer.mmb.render;

import java.util.List;

public class MmbAncillaryCartRender {

	private Integer quantity;
	private String i18nKeyDescription;
	
	/* campi seat (non corretti per voli con scalo */
	private String seat;
	private Boolean extraComfortSeat;
	
	private String emd;
	
	/* campi seat */
	private List<String> standardSeats;
	private List<String> comfortSeats;
	private List<String> emds;
	
	public MmbAncillaryCartRender() {
		super();
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getI18nKeyDescription() {
		return i18nKeyDescription;
	}

	public void setI18nKeyDescription(String i18nKeyDescription) {
		this.i18nKeyDescription = i18nKeyDescription;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public Boolean getExtraComfortSeat() {
		return extraComfortSeat;
	}

	public void setExtraComfortSeat(Boolean extraComfortSeat) {
		this.extraComfortSeat = extraComfortSeat;
	}

	public String getEmd() {
		return emd;
	}

	public void setEmd(String emd) {
		this.emd = emd;
	}

	public List<String> getStandardSeats() {
		return standardSeats;
	}

	public void setStandardSeats(List<String> standardSeats) {
		this.standardSeats = standardSeats;
	}

	public List<String> getComfortSeats() {
		return comfortSeats;
	}

	public void setComfortSeats(List<String> comfortSeats) {
		this.comfortSeats = comfortSeats;
	}

	public List<String> getEmds() {
		return emds;
	}

	public void setEmds(List<String> emds) {
		this.emds = emds;
	}

}
