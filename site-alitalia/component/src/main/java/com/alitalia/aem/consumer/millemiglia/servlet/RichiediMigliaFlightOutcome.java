package com.alitalia.aem.consumer.millemiglia.servlet;

public class RichiediMigliaFlightOutcome {
	private String flightNum;
	private boolean isError;
	private String errorCode;
	
	RichiediMigliaFlightOutcome(String flightNum) {
		this.flightNum = flightNum;
		this.isError = false;
	}

	RichiediMigliaFlightOutcome(String flightNum, boolean isError, String errorCode) {
		this.flightNum =flightNum;
		this.isError = isError;
		this.errorCode = errorCode;
	}

	public String getFlightNum() {
		return flightNum;
	}

	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}

	public boolean isError() {
		return isError;
	}

	public void setError(boolean isError) {
		this.isError = isError;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
