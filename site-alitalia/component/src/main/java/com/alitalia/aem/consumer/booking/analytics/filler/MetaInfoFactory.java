package com.alitalia.aem.consumer.booking.analytics.filler;

public class MetaInfoFactory {
	
	public static final short BOOKING = 0;
	public static final short BOOKING_AWARD = 1;
	public static final short CARNET = 2;
	
	public static MetaInfoFiller getFiller(short key){
		MetaInfoFiller filler  = null;
		if(key == BOOKING){
			filler = new BookingMetaInfoFiller();
		}
		else if(key == BOOKING_AWARD){
			filler = new BookingAwardMetaInfoFiller();
		}
		else if(key == CARNET){
			filler = new CarnetMetaInfoFiller();
		}
		return filler;
	}
	
	public static MetaInfoToJsObj getJsConverter(short key){
		MetaInfoToJsObj conv = null;
		if(key == BOOKING){
			conv = new BookingMetaInfoToJsObj();
		}
		else if(key == BOOKING_AWARD){
			conv = new BookingAwardMetaInfoToJsObj();
		}
		else if(key == CARNET){
			conv = new CarnetMetaInfoToJsObj();
		}
		return conv;
	}

}
