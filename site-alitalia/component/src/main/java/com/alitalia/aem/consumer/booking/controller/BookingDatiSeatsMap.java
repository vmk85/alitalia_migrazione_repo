package com.alitalia.aem.consumer.booking.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.SeatMapData;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.render.DirectFlightRender;
import com.alitalia.aem.consumer.booking.render.SeatsMapRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingDatiSeatsMap extends BookingSessionGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private BookingSession bookingSession;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private ArrayList<DirectFlightRender> flightsList;
	private List<String> passengers;

	private boolean isSeatEnabled;
	
	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			initBaseModel(request);
			
			if (ctx != null) {
				passengers = new ArrayList<String>();
				List<PassengerBaseData> passengersList = ctx.selectionRoutes.getPassengers();
				if (passengersList != null) {
					int indexPassenger = 0;
					for (PassengerBaseData pax : passengersList) {
						if (pax.getType() != PassengerTypeEnum.INFANT) {
							String passenger = pax.getName() + " " + pax.getLastName();
							if (pax.getType() == PassengerTypeEnum.ADULT) {
								passenger += computeReferedInfant(passengersList,indexPassenger);
							}
							if (pax.getType() == PassengerTypeEnum.CHILD) {
								String child = " " + i18n.get("booking.ancillary.bambino");
								if (ctx.cug == BookingSearchCUGEnum.FAMILY) {
									if (ctx.familySolutionFound) {
										child = " " + i18n.get("booking.ancillary.family.bambino");
									}
								}
								passenger += child;
							}
							this.passengers.add(passenger);
						}
						indexPassenger++;
					}
				}
				
				isSeatEnabled = false;
				this.flightsList = new ArrayList<DirectFlightRender>();
				if (ctx.seatMapsByFlight != null && !ctx.seatMapsByFlight.entrySet().isEmpty()) {
					for (Entry<DirectFlightData, SeatMapData> entry : ctx.seatMapsByFlight.entrySet()) {
						DirectFlightRender directFlightRender = new DirectFlightRender(entry.getKey());
						SeatsMapRender mappaPosti = new SeatsMapRender(entry.getValue());
						if (mappaPosti.getSeatsMap()==null) {
							directFlightRender.setMappaPostiEnabled(false);
						} else {
							directFlightRender.setMappaPostiEnabled(true);
						}
						directFlightRender.setMappaPosti(mappaPosti);
						this.flightsList.add(directFlightRender);
						
					}
					isSeatEnabled = true;
				}
			}
			
		} catch (Exception e) {
			logger.error("Unexpected exception:", e);
			throw (e);
		}
	}
	
	public List<DirectFlightRender> getFlightsList() {
		return this.flightsList;
	}

	public List<String> getPassengers() {
		return passengers;
	}
	
	public boolean isSeatEnabled(){
		return isSeatEnabled;
	}
	
	private String computeReferedInfant(List<PassengerBaseData> passengers,
			int indexPassenger) {
		boolean hasReferedInfant = bookingSession.computeReferedInfant(passengers, indexPassenger) != -1;
		if (hasReferedInfant) {
			return "<span class=\"foodOnBoard__plus\"> + " + i18n.get("booking.ancillary.posto.neonato.label")+ "</span>";
		}
		return "";
	}
	
}
