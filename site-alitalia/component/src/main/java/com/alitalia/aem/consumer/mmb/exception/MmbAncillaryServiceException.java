package com.alitalia.aem.consumer.mmb.exception;

import java.util.List;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;

@SuppressWarnings("serial")
public class MmbAncillaryServiceException extends Exception {
	
	private List<MmbAncillaryErrorData> errors;

	public MmbAncillaryServiceException(List<MmbAncillaryErrorData> errors) {
		super(generateMessage(errors));
		this.errors = errors;
	}
	
	public List<MmbAncillaryErrorData> getErrors() {
		return errors;
	}
	
	private static String generateMessage(List<MmbAncillaryErrorData> errors) {
		String message = "";
		if (errors != null) {
			StringBuilder messageBuilder = new StringBuilder();
			for (MmbAncillaryErrorData error : errors) {
				if (messageBuilder.length() > 0) {
					messageBuilder.append("\n");
				}
				if (error.getDescription() != null) {
					messageBuilder.append(error.getDescription());
				}
			}
			message = messageBuilder.toString();
		}
		return message;
	}
	
}
