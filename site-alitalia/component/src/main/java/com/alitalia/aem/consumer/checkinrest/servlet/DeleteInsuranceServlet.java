package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.web.component.webcheckin.delegaterest.IInsuranceDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;

@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "deleteinsurancerest"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class DeleteInsuranceServlet extends GenericCheckinFormValidatorServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile IInsuranceDelegate insuranceDelegate;

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		CheckinSessionContext ctx = getCheckInSessionContext(request);

		try {
			logger.info("Entrati in [DeleteInsuranceServlet - performSubmit]");

			DeleteInsuranceRequest deleteInsuranceRequest = new DeleteInsuranceRequest();
			deleteInsuranceRequest.setConversationID(ctx.conversationId);
			deleteInsuranceRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
			deleteInsuranceRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
			deleteInsuranceRequest.setPnr(ctx.pnr);

			logger.info("[DeleteInsuranceServlet] invocando insuranceDelegate.setInsurance...");
			DeleteInsuranceResponse deleteInsuranceResponse = insuranceDelegate.deleteInsurance(deleteInsuranceRequest);

			if (deleteInsuranceResponse != null) {
				if(deleteInsuranceResponse.getError() != null && !deleteInsuranceResponse.getError().equals("")){
					managementError(request, response, deleteInsuranceResponse.getError());
				}else{
					//response.sendRedirect(successPage);
					if(deleteInsuranceResponse.getSuccess().equals("OK")){
						if(deleteInsuranceResponse.getConversationID() != null){
							ctx.conversationId = deleteInsuranceResponse.getConversationID();
						}
						managementSucces(request, response, deleteInsuranceResponse);
						ctx.insurancePolicy = null;
					}
				}
			}
			else {
				logger.error("[DeleteInsuranceServlet] - Errore durante l'invocazione del servizio." );
			}
		}
		catch (Exception e) {
			throw new RuntimeException("[DeleteInsuranceServlet] - Errore durante l'invocazione del servizio.", e);
		}
	}

	private void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error){

		logger.info("[DeleteInsuranceServlet] [managementError] error =" + error );

		Gson gson = new Gson();

		com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);

		TidyJSONWriter json;
		try {
			json = new TidyJSONWriter(response.getWriter());

			response.setContentType("application/json");

			json.object();
			json.key("isError").value(true);
			json.key("errorMessage").value(errorObj.getErrorMessage());
			json.key("sabreStatusCode").value(errorObj.getSabreStatusCode());
			json.key("conversationID").value(errorObj.getConversationID());
			json.endObject();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
	}

	private void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response, DeleteInsuranceResponse deleteInsuranceResponse){

		logger.info("[DeleteInsuranceServlet] [managementSucces] ...");

		try {
			TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());

			jsonOutput.object();
			jsonOutput.key("success").value(deleteInsuranceResponse.getSuccess());
			jsonOutput.key("errorDescription").value(deleteInsuranceResponse.getErrorDescription());
			jsonOutput.key("conversationID").value(deleteInsuranceResponse.getConversationID());
			jsonOutput.endObject();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		//		String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
		//				request.getResource(), false)
		//				+ getConfiguration().getCheckinFlightListPage();
		//
		//		successPage = request.getResourceResolver().map(successPage);
		//
		//		logger.info("[CheckinPnrSearchServlet] [managementSucces] successPage = " + successPage );
		//
		//		TidyJSONWriter json;
		//		try {
		//
		//			json = new TidyJSONWriter(response.getWriter());
		//			response.setContentType("application/json");
		//			json.object();
		//			json.key("isError").value(false);
		//			json.key("successPage").value(successPage);
		//			json.endObject();
		//
		//		} catch (IOException e) {
		//			e.printStackTrace();
		//		} catch (JSONException e) {
		//			e.printStackTrace();
		//		}	

	}

}
