package com.alitalia.aem.consumer.global.i18n;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.consumer.global.enumeration.Genders;
import com.alitalia.aem.consumer.global.enumeration.MonthsOfYear;

public class I18nKeyCommon {
	
	public static final String AIRPORT_PREFIX = "airportsData.";
	public static final String AIRPORT_CITY = ".city";
	public static final String LABEL_AR = "allofferts.andataritorno";
	public static final String LABEL_SOLO_ANDATA = "allofferts.soloandata";
	
	public static final String MESSAGE_GENERIC_EMPTY_FIELD = "message.generic.emptyfield";
	public static final String MESSAGE_INVALID_FIELD = "message.generic.invalidfield";
	public static final String MESSAGE_NOT_EQUAL_FIELD = "message.generic.notequal";
	
	public static final List<String> getMonths() {
		List<String> months = MonthsOfYear.listI18nValues();
		return months;
	}
	public static final List<String> getGenders(){
		List<String> genders = Genders.listI18nValues();
		
		return genders;
	}
	
	public static final List<String> getShortGenders() {
		List<String> shortGenderList = new ArrayList<String>();
		shortGenderList.add("genders.male.short");
		shortGenderList.add("genders.female.short");
		return shortGenderList;
	}
}
