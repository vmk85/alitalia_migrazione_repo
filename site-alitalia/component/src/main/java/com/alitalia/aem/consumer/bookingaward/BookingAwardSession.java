package com.alitalia.aem.consumer.bookingaward;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.AwardBookingSearchData;
import com.alitalia.aem.common.data.home.AwardSearchData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.MmCustomerData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PassengerNumbersData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.SearchDestinationData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.common.data.home.enumerations.ARTaxInfoTypesEnum;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.BookingTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GatewayTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.ResidencyTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.TimeTypeEnum;
import com.alitalia.aem.common.messages.home.AwardValueRequest;
import com.alitalia.aem.common.messages.home.AwardValueResponse;
import com.alitalia.aem.common.messages.home.ProfileRequest;
import com.alitalia.aem.common.messages.home.ProfileResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionResponse;
import com.alitalia.aem.common.messages.home.enumeration.SearchExecuteResponseType;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.booking.BookingPhaseEnum;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSearchCategoryEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.model.SearchElementAirport;
import com.alitalia.aem.consumer.booking.model.SearchPassengersNumber;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.bookingaward.delegate.BookingAwardDelegate;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;

/**
 * The <code>BookingAwardSession</code> component tracks the
 * behaviour of a booking award session and the related data,
 * through specialized methods.
 * 
 * <p>Since the SCR component is stateless, the state of the
 * single Booking Award session is tracked in an external object
 * <code>BookingSessionContext</code>.</p>
 * 
 * @see BookingSessionContext
 */
@Component(immediate = true)
@Service(value = BookingAwardSession.class)
public class BookingAwardSession {
		
	/* static */
	
	private static Logger logger = LoggerFactory.getLogger(BookingAwardSession.class);
	
	/* component references */
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegate staticDataDelegate;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile SearchFlightsDelegate searchFlightsDelegate;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BookingAwardDelegate bookingAwardDelegate;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaCustomerProfileManager customerProfileManager;
	
	@Reference
	private BookingSession bookingSession;
	
	@SuppressWarnings("unused")
	private ComponentContext componentContext;
	
	/* constructor */
	
	public BookingAwardSession() {
		super();
	}
	
	
	/* SCR lifecycle methods */
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Modified
	private void modified(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	/* interface methods */
	
	public BookingSessionContext initSession(String clientIp, String sessionId, 
			String market, String site, Locale locale, SlingHttpServletRequest request) {
		
		logger.debug("initSession");
		
		BookingSessionContext ctx = new BookingSessionContext();
		ctx.sid = BookingAwardConstants.SID;
		ctx.phase = BookingPhaseEnum.INITIAL;
		ctx.numberOfMoreFlights = 10;
		ctx.initialNumberOfFlightsToShow = 10;
		ctx.clientIP = clientIp;
		ctx.sessionId = sessionId;
		ctx.market = market;
		ctx.site = site;
		ctx.locale = locale;
		ctx.award = true;
		ctx.totalAwardPrice = BigDecimal.ZERO;
		ctx.loggedUserProfileData = retrieveLoggedUserProfile(ctx, request);
		return ctx;
	}
	
	public void prepareSearch(BookingSessionContext ctx, I18n i18n,
			BookingSearchKindEnum searchKind, NumberFormat currentNumberFormat, List<SearchElement> searchElements,
			SearchPassengersNumber searchPassengersNumber,
			CabinEnum filterCabin) {
		
		logger.debug("prepareSearch");
		
		String tid = IDFactory.getTid();

		// verify airport codes and retrieve airports data
		RetrieveAirportsRequest retrieveAirportsRequest = 
				new RetrieveAirportsRequest(tid, ctx.sid);
		retrieveAirportsRequest.setMarket(ctx.market);
		retrieveAirportsRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
		RetrieveAirportsResponse retrieveAirportsResponse = 
				staticDataDelegate.retrieveAirports(retrieveAirportsRequest);
		List<AirportData> listAirports = retrieveAirportsResponse.getAirports();
		Map<String, AirportData> searchAirportMap = new HashMap<String, AirportData>();
		for (AirportData airport : listAirports) {
			for (SearchElement searchElement : searchElements) {
				if (airport.getCode().equals(searchElement.getFrom().getAirportCode())) {
					searchAirportMap.put(airport.getCode(), airport);
				}
				if (airport.getCode().equals(searchElement.getTo().getAirportCode())) {
					searchAirportMap.put(airport.getCode(), airport);
				}
			}
		}

		for (SearchElement searchElement : searchElements) {
			for (SearchElementAirport element : 
				new SearchElementAirport[] { searchElement.getFrom(), searchElement.getTo() }) {
				AirportData airport = searchAirportMap.get(element.getAirportCode());
				if (airport == null) {
					logger.error("Cannot find detail for airport code {}.", element.getAirportCode());
					throw new IllegalArgumentException("Cannot find detail for airport code.");
				} else {
					element.setAirportName(airport.getName());
					element.setLocalizedAirportName(i18n.get("airportsData." + element.getAirportCode() + ".name"));
					element.setCityCode(airport.getCityCode());
					element.setCityName(airport.getCity());
					element.setLocalizedCityName(i18n.get("airportsData." + element.getAirportCode() + ".city"));
					element.setCountryCode(airport.getCountryCode());
					element.setCountryName(airport.getCountry());
					element.setLocalizedCountryName(i18n.get("airportsData." + element.getCountryCode() + ".country"));
				}
			}
		}
		
		
		int monthD = searchElements.get(0).getDepartureDate().get(Calendar.MONTH);
		int yearD = searchElements.get(0).getDepartureDate().get(Calendar.YEAR);
		ctx.calendarSearchDeparture[0] = monthD;
		ctx.calendarSearchDeparture[1] = yearD;
		int monthR;
		int yearR;
		if (searchElements.size()>1) {
			monthR = searchElements.get(1).getDepartureDate().get(Calendar.MONTH);
			yearR = searchElements.get(1).getDepartureDate().get(Calendar.YEAR);
		}
		else {
			monthR = -1;
			yearR = -1;
		}
		ctx.calendarSearchReturn[0] = monthR;
		ctx.calendarSearchReturn[1] = yearR;
		ctx.calendarSearchModified = false;
		
		ctx.newSearch = true;
		ctx.searchKind = searchKind;
		ctx.searchElements = searchElements;
		ctx.searchPassengersNumber = searchPassengersNumber;
		ctx.flightSelections = new FlightSelection[ctx.searchElements.size()];
		ctx.numberOfShownFlights = new int[ctx.searchElements.size()]; 
		ctx.filterCabin = filterCabin;
		ctx.readyToPassengersDataPhase = false;
		ctx.insuranceProposalData = null;
		ctx.insuranceAmount = new BigDecimal(0);
		ctx.isInsuranceApplied = false;
		ctx.currentNumberFormat = currentNumberFormat;
		
		// Per ricerca multitratta
		if (searchKind == BookingSearchKindEnum.MULTILEG) {
			ctx.currentSliceIndex = 1;
		}
		
		ctx.phase = BookingPhaseEnum.FLIGHTS_SEARCH;
	}
	
	public void performCalendarSearch(BookingSessionContext ctx) {
		
		logger.debug("performCalendarSearch");
		
		String tid = IDFactory.getTid();

		if (ctx.searchKind == BookingSearchKindEnum.SIMPLE 
				|| ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP) {

			SearchFlightSolutionResponse searchResponse = 
					bookingAwardDelegate.retrieveAwardCalendarSolutions(createAwardSearchRequest(tid, SearchTypeEnum.AWARD_CALENDAR_SEARCH, ctx, false));
			
			ctx.availableFlights = searchResponse.getAvailableFlights();
		} /*else { // ctx.searchKind == BookingSearchKindEnum.MULTILEG
			
			// Ricerca multitratta: da implementare in fase successiva
		}*/
		ctx.searchCategory = computeSearchCategory(ctx);

		ctx.totalSlices = computeTotalSlices(ctx);
		ctx.isRefreshed = false;
		ctx.newSearch = false;
		
		
	}
	
	public void modifyCalendarSearch(BookingSessionContext ctx) {
		
		logger.debug("performCalendarSearch");
		
		String tid = IDFactory.getTid();

		if (ctx.searchKind == BookingSearchKindEnum.SIMPLE 
				|| ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP) {

			SearchFlightSolutionResponse searchResponse = 
					bookingAwardDelegate.retrieveAwardCalendarSolutions(createAwardSearchRequest(tid, SearchTypeEnum.AWARD_CALENDAR_SEARCH, ctx, true));
			
			ctx.availableFlights = searchResponse.getAvailableFlights();
			
			
		}
		

		
	}
	
	
	private SearchFlightSolutionRequest createAwardSearchRequest(String tid, SearchTypeEnum type,
			BookingSessionContext ctx, boolean modifySearchData) {

		SearchDestinationData[] searchDestinations = createSearchDestination(ctx);
		if (modifySearchData) {
			searchDestinations = modifyDateOfSearch(ctx, searchDestinations);
		}
		
		List<PassengerNumbersData> passengerNumbers = setPassengersNumber(ctx.searchPassengersNumber);
		
		AwardSearchData awardSearchData = new AwardSearchData();
		awardSearchData.setOnlyDirectFlight(false);
		awardSearchData.setType(type);
		awardSearchData.setMarket(ctx.market);
		awardSearchData.setResidency(ResidencyTypeEnum.NONE);
		awardSearchData.setSearchCabin(CabinEnum.ECONOMY); // TODO [Booking Award] Valorizzare SearchCabin in base alla selezione
		awardSearchData.setSearchCabinType(CabinTypeEnum.PERMITTED);
		
		for (SearchDestinationData searchDestination : searchDestinations) {
			awardSearchData.addDestination(searchDestination);
		}
		for (PassengerNumbersData passengerNumber : passengerNumbers) {
			awardSearchData.addPassengerNumbers(passengerNumber);
		}
		
		// create search request object
		SearchFlightSolutionRequest searchRequest = new SearchFlightSolutionRequest(tid, ctx.sid);
		searchRequest.setCookie(ctx.cookie);
		searchRequest.setExecution(ctx.execution);
		searchRequest.setFilter(awardSearchData);
		searchRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		searchRequest.setMarket(ctx.market);
		//searchRequest.setMarketExtraCharge(ctx.market);
		searchRequest.setResponseType(SearchExecuteResponseType.MODEL);
		
		// String fromAriportCode = searchDestinations[0].getFromAirport().getCode();
		// String toAriportCode = searchDestinations[0].getToAirport().getCode();
		// searchRequest.setFromSearchElement(fromAriportCode);
		// searchRequest.setToSearchElement(toAriportCode);

		return searchRequest;
	}
	
	private SearchDestinationData[] createSearchDestination(BookingSessionContext ctx){

		SearchDestinationData[] searchDestinations = new SearchDestinationData[ctx.searchElements.size()];

		int index=0;
		for(SearchElement searchElement : ctx.searchElements){
			SearchElementAirport from = searchElement.getFrom();
			SearchElementAirport to = searchElement.getTo();

			// departure airport
			AirportData fromAirport = new AirportData();
			fromAirport.setCityCode(from.getCityCode());
			fromAirport.setCode(from.getAirportCode());
			fromAirport.setCountryCode(from.getCountryCode());

			// destination airport
			AirportData toAirport = new AirportData();
			toAirport.setCityCode(to.getCityCode());
			toAirport.setCode(to.getAirportCode());
			toAirport.setCountryCode(to.getCountryCode());

			Calendar departureDate = searchElement.getDepartureDate();
			departureDate.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));

			// destination data
			SearchDestinationData destination = new SearchDestinationData();
			destination.setFromAirport(fromAirport);
			destination.setToAirport(toAirport);
			if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP && index == 1) {
				destination.setRouteType(RouteTypeEnum.RETURN);
			} else {
				destination.setRouteType(RouteTypeEnum.OUTBOUND);
			}
			destination.setTimeType(TimeTypeEnum.ANYTIME);
			destination.setDepartureDate(departureDate);
			searchDestinations[index] = destination;
			index++;
		}

		return searchDestinations;
	}
	
	private SearchDestinationData[] modifyDateOfSearch(BookingSessionContext ctx, SearchDestinationData[] searchDestinations){
		
		SearchDestinationData[] newSearchData = searchDestinations;
		
		if ((searchDestinations[0].getDepartureDate().get(Calendar.MONTH) != ctx.calendarSearchDeparture[0]) || (searchDestinations[0].getDepartureDate().get(Calendar.YEAR) != ctx.calendarSearchDeparture[1])) {
			Calendar departureDate = Calendar.getInstance();
			departureDate.set(Calendar.DAY_OF_MONTH, 1);
			departureDate.set(Calendar.MONTH, ctx.calendarSearchDeparture[0]);
			departureDate.set(Calendar.YEAR, ctx.calendarSearchDeparture[1]);
			newSearchData[0].setDepartureDate(departureDate);
		}
		
		if (searchDestinations.length>1 && searchDestinations[1].getDepartureDate() != null) {
			if ((searchDestinations[1].getDepartureDate().get(Calendar.MONTH) != ctx.calendarSearchReturn[0]) || (searchDestinations[1].getDepartureDate().get(Calendar.YEAR) != ctx.calendarSearchReturn[1])) {
				Calendar returnDate = Calendar.getInstance();
				returnDate.set(Calendar.DAY_OF_MONTH, 1);
				returnDate.set(Calendar.MONTH, ctx.calendarSearchReturn[0]);
				returnDate.set(Calendar.YEAR, ctx.calendarSearchReturn[1]);
				newSearchData[1].setDepartureDate(returnDate);
			}
		}
		
		return newSearchData;
	}

	private List<PassengerNumbersData> setPassengersNumber(SearchPassengersNumber np) {

		List<PassengerNumbersData> listPassengerNumbersData = new ArrayList<PassengerNumbersData>();

		if (np.getNumAdults() > 0) {
			PassengerNumbersData adultPassengersNumber = new PassengerNumbersData();
			adultPassengersNumber.setNumber(np.getNumAdults());
			adultPassengersNumber.setPassengerType(PassengerTypeEnum.ADULT);
			listPassengerNumbersData.add(adultPassengersNumber);
		}

		if (np.getNumChildren() > 0) {
			PassengerNumbersData childPassengersNumber = new PassengerNumbersData();
			childPassengersNumber.setNumber(np.getNumChildren());
			childPassengersNumber.setPassengerType(PassengerTypeEnum.CHILD);
			listPassengerNumbersData.add(childPassengersNumber);
		}

		if (np.getNumInfants() > 0) {
			PassengerNumbersData infantPassengersNumber = new PassengerNumbersData();
			infantPassengersNumber.setNumber(np.getNumInfants());
			infantPassengersNumber.setPassengerType(PassengerTypeEnum.INFANT);
			listPassengerNumbersData.add(infantPassengersNumber);
		}

		if (np.getNumYoung() > 0) {
			PassengerNumbersData youthPassengersNumber = new PassengerNumbersData();
			youthPassengersNumber.setNumber(np.getNumYoung());
			youthPassengersNumber.setPassengerType(PassengerTypeEnum.YOUTH);
			listPassengerNumbersData.add(youthPassengersNumber);
		}

		return listPassengerNumbersData;
	}
	
	public void performFlightSearch(BookingSessionContext ctx, Resource brandRoot) {
		
		logger.debug("performFlightSearch");
		
		String tid = IDFactory.getTid();

		if (ctx.searchKind == BookingSearchKindEnum.SIMPLE 
				|| ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP) {

			SearchFlightSolutionResponse searchResponse = 
					bookingAwardDelegate.retrieveAwardFlightInformation(createAwardSearchRequest(tid, SearchTypeEnum.AWARD_FLIGHT_SEARCH, ctx, false));
			
			ctx.availableFlights = searchResponse.getAvailableFlights();
			
			// Per il booking award non abbiamo la chiamata alla extraCharge per cui la lista dei passeggeri
			// dobbiamo costruirla a partire dai dati inseriti per la ricerca
			ctx.extraChargePassengerList = computePassengersDataList(ctx);
		} /*else { // ctx.searchKind == BookingSearchKindEnum.MULTILEG
			
			// Ricerca multitratta: da implementare in fase successiva
		}*/

		ctx.totalExtraCharges = bookingSession.computeTotalExtraChargesAmount(ctx);
		ctx.totalSlices = computeTotalSlices(ctx);
		ctx.isRefreshed = false;
		ctx.phase = BookingPhaseEnum.FLIGHTS_SELECTION;
		ctx.newSearch = false;
		ctx.searchCategory = computeSearchCategory(ctx);
		
		ctx.brandMap = computeBrandMap(brandRoot);
		ctx.codeBrandMap = computeCodeBrandMap(ctx);
	}
	
	/**
	 * Determine the booking search category
	 * @param ctx The booking session context to use.
	 * @param expectedBrandLight
	 * @return
	 */
	private BookingSearchCategoryEnum computeSearchCategory(BookingSessionContext ctx) {

		if (ctx.availableFlights == null || ctx.availableFlights.getRoutes() == null || ctx.availableFlights.getRoutes().isEmpty()
				|| ctx.availableFlights.getRoutes().stream()
				.anyMatch(route -> route.getFlights() == null || route.getFlights().isEmpty())) {
			logger.info("No solutions found");
			return null;
		}

		FlightData flightData = ctx.availableFlights.getRoutes().get(0).getFlights().get(0);

		AreaValueEnum areaFrom = null;
		AreaValueEnum areaTo = null;

		if (flightData instanceof DirectFlightData) {
			DirectFlightData directFlightData = (DirectFlightData) flightData;
			areaFrom = directFlightData.getFrom().getArea();
			areaTo = directFlightData.getTo().getArea();

		} else {
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
			DirectFlightData first = (DirectFlightData) connectingFlightData.getFlights().get(0);
			DirectFlightData last = (DirectFlightData) connectingFlightData.getFlights().get(connectingFlightData.getFlights().size() - 1);
			areaFrom = first.getFrom().getArea();
			areaTo = last.getTo().getArea();
		}

		String from = ctx.searchElements.get(0).getFrom().getAirportCode();
		String to = ctx.searchElements.get(0).getTo().getAirportCode();
		AreaValueEnum flightType = areaFrom == AreaValueEnum.DOM ? areaTo : areaFrom;

		if ((ctx.cug == BookingSearchCUGEnum.MILITARY && ctx.militarySolutionFound) ||
				(ctx.cug == BookingSearchCUGEnum.FAMILY && ctx.familySolutionFound) ||
				(ctx.cug == BookingSearchCUGEnum.YOUTH && ctx.youthSolutionFound)) {

			return ctx.cug == BookingSearchCUGEnum.MILITARY ? BookingSearchCategoryEnum.MIL : 
				(ctx.cug == BookingSearchCUGEnum.FAMILY ? BookingSearchCategoryEnum.FAM : BookingSearchCategoryEnum.YTH);

		} else {

			if (ctx.isSelectedContinuitaTerritoriale != null && ctx.isSelectedContinuitaTerritoriale) {
				return BookingSearchCategoryEnum.CONTINUITY;

			} else if (ctx.isAllowedContinuitaTerritoriale != null && 
					ctx.isAllowedContinuitaTerritoriale && ctx.residency == ResidencyTypeEnum.SICILY) {

				// not isSelectedContinuitaTerritoriale but isAllowedContinuitaTerritoriale
				// In this case only there is a particolar category only for Sicily. For Sardinia the
				// behaviour is standard.
				return BookingSearchCategoryEnum.CONTINUITY_REFUSE_SICILY;

			} else if ((ctx.isAllowedContinuitaTerritoriale == null || !ctx.isAllowedContinuitaTerritoriale) &&
					searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(from, to)) {

				// This is a route for Sardinia but not isAllowedContinuitaTerritoriale
				// In this case we are outside the period for territorial continuity.
				return BookingSearchCategoryEnum.CONTINUITY_OUT_SARDINIA;

			} else if ((from.equals("FCO") && to.equals("LIN")) || 
					(from.equals("LIN") && to.equals("FCO")) ||
					(from.equals("ROM") && to.equals("LIN")) ||
					(from.equals("LIN") && to.equals("ROM")) ||
					(from.equals("ROM") && to.equals("MIL")) ||
					(from.equals("MIL") && to.equals("ROM")) ||
					(from.equals("MIL") && to.equals("FCO")) ||
					(from.equals("FCO") && to.equals("MIL")) ) {
				return BookingSearchCategoryEnum.FCO_LIN;
			} else {

				if (flightType == AreaValueEnum.DOM) {
					return BookingSearchCategoryEnum.DOM;
				} else if (flightType == AreaValueEnum.INTZ) {
					return BookingSearchCategoryEnum.INTZ;
				} else {
					return BookingSearchCategoryEnum.INTC;
				}
			}
		}
	}


	public void updateSearch(BookingSessionContext ctx, List<SearchElement> newElements) {
		ctx.searchElements = newElements;
		ctx.phase = BookingPhaseEnum.FLIGHTS_SELECTION;
	}
	
	public boolean existFlights(BookingSessionContext ctx, String textDay, int routeIndex) {
		String format = "dd/MM/yyyy";
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		dateFormat.setLenient(false);
		Calendar day = Calendar.getInstance();
		try {
			day.setTime(dateFormat.parse(textDay));
		} catch (ParseException e) {
			logger.debug("[BookingAwardSession] Failed to convert date");
			return false;
		}
		
		RouteData route = ctx.availableFlights.getRoutes().get(routeIndex);
		DirectFlightData flight = null;
		for (FlightData flightData : route.getFlights()) {
				
			// IF IT IS A DIRECT FLIGHT TAKES THE FLIGHT DATA
			if (flightData.getFlightType() == FlightTypeEnum.DIRECT) {
				flight = (DirectFlightData) flightData;
			}
				
			// IF IT IS A CONNECTING FLIGHT TAKE THE FIRST FLIGHT DATA
			if (flightData.getFlightType() == FlightTypeEnum.CONNECTING)  {
				ConnectingFlightData flights = (ConnectingFlightData) flightData;
				flight = (DirectFlightData) flights.getFlights().get(0);
			}
				
			// CHECK DATE
			if (flight != null && flight.getDepartureDate() != null) {
				if (flight.getDepartureDate().get(Calendar.DAY_OF_MONTH) == day.get(Calendar.DAY_OF_MONTH)
						&& flight.getDepartureDate().get(Calendar.MONTH) == day.get(Calendar.MONTH)
						&& flight.getDepartureDate().get(Calendar.YEAR) == day.get(Calendar.YEAR)) {
							
						return true;
				}
			}
				
				
		}
		
		return false;
	}
	
	/**
	 * It returns the number of slices
	 * @param ctx
	 * @return
	 */
	private int computeTotalSlices(BookingSessionContext ctx) {
		return ctx.searchElements.size();
	}


	public List<FlightData> obtainFlightsToShown(BookingSessionContext ctx,
			int routeIndex, RouteData route, boolean moreFlights) {
		List<FlightData> flightsToShown = new ArrayList<FlightData>();
		FlightSelection selectedFlight = ctx.flightSelections[routeIndex];
		int indexSelectedFlight = -1;
		if (route.getFlights().size() <= ctx.numberOfShownFlights[routeIndex]) {
			return  route.getFlights();
		}

		if (ctx.numberOfShownFlights[routeIndex] == 0) { //it happens at the first rendering of the partial selection
			if (route.getFlights().size() <= ctx.initialNumberOfFlightsToShow) {
				return  route.getFlights();
			}

			ctx.numberOfShownFlights[routeIndex] = ctx.initialNumberOfFlightsToShow;
			flightsToShown = route.getFlights().subList(0, ctx.numberOfShownFlights[routeIndex]);
		} else {
			if (moreFlights) {
				ctx.numberOfShownFlights[routeIndex] = ctx.numberOfShownFlights[routeIndex] 
						+ ctx.numberOfMoreFlights;
			}
			if (selectedFlight != null) {
				indexSelectedFlight = selectedFlight.getIndexFlight();
				if (indexSelectedFlight > ctx.numberOfShownFlights[routeIndex]) {
					while (indexSelectedFlight >= ctx.numberOfShownFlights[routeIndex]) {
						ctx.numberOfShownFlights[routeIndex] = ctx.numberOfShownFlights[routeIndex] 
								+ ctx.numberOfMoreFlights;
					}
				}
			}
			if (route.getFlights().size() <= ctx.numberOfShownFlights[routeIndex]) {
				ctx.numberOfShownFlights[routeIndex] = route.getFlights().size();
				return  route.getFlights();
			}
			flightsToShown = route.getFlights().subList(0, ctx.numberOfShownFlights[routeIndex]);
		}
		return flightsToShown;
	}
	
	/**
	 * Obtain MM customer data for create PNR. 
	 * @param request
	 * @return the MM user data if authenticated, null otherwise.
	 */
	public MmCustomerData getMmcustomerData(SlingHttpServletRequest request){
		MMCustomerProfileData customer =  AlitaliaUtils.getAuthenticatedUser(request);
		MmCustomerData customerData = null;
		if(customer != null){
			GenderTypeEnum gender = null;
			switch(customer.getGender()){
			case FEMALE:
				gender = GenderTypeEnum.FEMALE;
				break;
			case MALE:
				gender = GenderTypeEnum.MALE;
				break;
			case UNKNOWN:
				gender = GenderTypeEnum.UNKNOWN;
				break;
			default:
				gender = GenderTypeEnum.UNKNOWN;
				break;
			}
			String dcryptedPin = customerProfileManager.decodeAndDecryptProperty(customer.getCustomerPinCode());
			customerData = new MmCustomerData();
			customerData.setBirthDate(customer.getBirthDate());
			customerData.setGender(gender);
			customerData.setIgnore(false);
			customerData.setLastName(customer.getCustomerSurname());
			customerData.setCode(customer.getCustomerNumber());
			customerData.setPin(dcryptedPin);
			customerData.setMilesBalance(new Integer(0));
			customerData.setMilesEarned(new Integer(0));
			customerData.setMilesQualify(new Integer(0));
			customerData.setName(customer.getCustomerName());
			customerData.setTierCode(customer.getTierCode().value());
		}
		return customerData;
	}
	
	private Map<String, BrandPageData> computeBrandMap(Resource rootResource) {
		HashMap<String, BrandPageData> pageMap = new HashMap<String, BrandPageData>();
		if (rootResource == null) {
			logger.error("Brand Directory does not exist");
			throw new IllegalStateException("Brand Directory does not exist");
		}
		if (rootResource.adaptTo(Page.class).listChildren() == null 
				|| !rootResource.adaptTo(Page.class).listChildren().hasNext()) {
			logger.error("Brand pages do not exist");
			throw new IllegalStateException("Brand pages do not exist");
		}
		Iterator<Page> iterator = rootResource.adaptTo(Page.class).listChildren();
		while (iterator.hasNext()) {
			Page currentPage = iterator.next();
			BrandPageData brandPageData = new BrandPageData();
			brandPageData.setTitle(currentPage.getTitle());
			brandPageData.setPath(currentPage.getPath());
			brandPageData.setId(currentPage.getName());
			pageMap.put(currentPage.getName(), brandPageData);
		}
		return pageMap;
	}

	private Map<String,BrandPageData> computeCodeBrandMap(BookingSessionContext ctx) {

		Map<String,BrandPageData> codeBrandMap = new HashMap<String, BrandPageData>();

		String[] conf = configuration.getCodeBrandAwardMap();

		if (conf == null || conf.length == 0) {
			logger.error("Configuration Error - see AlitaliaConfigurationHolder properties");
			throw new IllegalStateException("Configuration Error - see AlitaliaConfigurationHolder properties");
		}

		if (conf.length > 0) {
			for (String entry : conf) {
				
				String[] res = entry.split(":");
				String brandCode = res[0];
				String pageId = res[1];
				
				BrandPageData page = ctx.brandMap.get(pageId);
				if (page != null) {
					codeBrandMap.put(brandCode, page);
				}
			}
		}
		return codeBrandMap;
	}
	
	public void performFlightSelection(BookingSessionContext ctx, int elementSelectionIndex,
			String solutionId, int solutionFlightIndex, int solutionBrandIndex, SlingHttpServletRequest request) {
		
		logger.debug("performFlightSelection");
		
		if (ctx.phase != BookingPhaseEnum.FLIGHTS_SELECTION) {
			logger.error("performFlightSelection not allowed in phase {}", ctx.phase);
			throw new IllegalStateException("Operation not allowed in the current phase.");
		}
		
		FlightData flightData = bookingSession.searchSelectionFlightData(ctx, elementSelectionIndex, solutionFlightIndex);
		BrandData brandData = bookingSession.searchSelectionBrandData(flightData, solutionBrandIndex);
		if (brandData.isEnabled() == false) {
			logger.error("Invalid brand selected for this flight.");
			throw new IllegalArgumentException("Invalid brand selected for this flight.");
		}
		
		String solutionIdOfFlightBrandSelection = brandData.getSolutionId();
		
		if (!solutionId.equals(solutionIdOfFlightBrandSelection)) {
			logger.error("Invalid solution id provided for the selection.");
			throw new IllegalArgumentException("Invalid solution id provided for the selection.");
		}
		
		List<Integer> flightsMileage = null;
		List<String> carriers = bookingSession.computeCarriersList(flightData, ctx);
		FlightSelection flightSelection = new FlightSelection(flightData, brandData.getCode(), null,
				null, carriers, bookingSession.computeIsTariffaLight(brandData.getCode()), solutionFlightIndex, brandData.getAwardPrice());
		if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
			elementSelectionIndex = ctx.currentSliceIndex - 1;
		}
		ctx.flightSelections[elementSelectionIndex] = flightSelection;
		

		/*if (!flightSelection.isTariffaLight()) {
			flightsMileage = retrieveFlightsMileages(ctx,flightData,solutionBrandIndex);
		} else {*/
			flightsMileage = new ArrayList<Integer>();
			flightsMileage.add(0);
		//}
		ctx.flightSelections[elementSelectionIndex].setFlightsMileage(flightsMileage);
		
		String tid = IDFactory.getTid();
		
		if (ctx.flightSelections[0] != null &&
				(ctx.searchKind == BookingSearchKindEnum.SIMPLE || 
				(ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP && ctx.flightSelections.length == 2 && ctx.flightSelections[1] != null))) {
			
			performBookingSellupAndTaxSearch(tid, ctx, request);
			computeTotalAwardPrice(ctx);
			ctx.readyToPassengersDataPhase = true;
		}
	}
	
	public boolean enoughPointsToBuy(BookingSessionContext ctx, int elementSelectionIndex,
			String solutionId, int solutionFlightIndex, int solutionBrandIndex) {
		
		if (ctx.loggedUserProfileData != null && ctx.loggedUserProfileData.getPointsRemainingTotal() != null) {
			FlightData flightData = bookingSession.searchSelectionFlightData(ctx, elementSelectionIndex, solutionFlightIndex);
			BrandData brandData = bookingSession.searchSelectionBrandData(flightData, solutionBrandIndex);
			Long flightAwardPrice = brandData.getAwardPrice().longValue();
			BigDecimal flightChosenAwardPrice = obtainingAwardPriceForSelectedFlight(ctx);
			return (flightAwardPrice <= ctx.loggedUserProfileData.getPointsRemainingTotal() - flightChosenAwardPrice.longValue());
		}
		else {
			logger.debug("[Points Check] Cannot find Logged User Points");
			throw new IllegalStateException("Cannot find Logged User Points");
		}
	}
	
	
	public boolean isBrandAwardPriceCorrect(BookingSessionContext ctx, Long realAwardPrice) {
		
		Long awardPrice = ctx.totalAwardPrice.longValue();		
		return (awardPrice.equals(realAwardPrice));
	}
	
	public Long retrieveRealAwardPrice(BookingSessionContext ctx) {
		logger.debug("retrieveRealAwardPrice");
		
		if (ctx.loggedUserProfileData != null) {
			String tid = IDFactory.getTid();
			String mmCustomerNumber = ctx.loggedUserProfileData.getCustomerNumber();
			String mmCustomerSurname = ctx.loggedUserProfileData.getCustomerSurname();
			List<RouteData> routes =  computeSelectedFlights(ctx); 
			int passengersNumber = ctx.searchPassengersNumber.getNumAdults() + ctx.searchPassengersNumber.getNumChildren();
			
			AwardValueRequest awardValueRequest = new AwardValueRequest();
			awardValueRequest.setTid(tid);
			awardValueRequest.setSid(ctx.sid);
			awardValueRequest.setMmCustomerNumber(mmCustomerNumber);
			awardValueRequest.setMmCustomerSurname(mmCustomerSurname);
			awardValueRequest.setRoutes(routes);
			awardValueRequest.setPassengersNumber(passengersNumber);
			
			AwardValueResponse awardValueResponse = bookingAwardDelegate.retrieveAwardValue(awardValueRequest);
			Long awardPriceResponse = awardValueResponse.getAwardValue();
			Long genericServiceError = new Long(-1);
			if (genericServiceError.equals(awardPriceResponse)) {
				
				logger.debug("[Booking Award Delegate] Service Error");
				return ctx.totalAwardPrice.longValue();
			}
			else {
				return awardPriceResponse;
			}
			
			
		}
		else {
			throw new IllegalStateException("Logger user not found");
		}
	
		
	}
	
	public void updateAwardPrice(Long realAwardPrice, BookingSessionContext ctx) {
		
		BigDecimal newAwardPrice = new BigDecimal(realAwardPrice);
		ctx.totalAwardPrice = newAwardPrice;
	}
	
	
	public void updateCalendarSearchDepartureData(BookingSessionContext ctx, Calendar calendar) {
		//  type false -> departure, type = true -> return
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		ctx.calendarSearchDeparture[0] = month;
		ctx.calendarSearchDeparture[1] = year;
		ctx.calendarSearchModified = true;
	}
	
	public void updateCalendarSearchReturnData(BookingSessionContext ctx, Calendar calendar) {
		//  type false -> departure, type = true -> return
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		ctx.calendarSearchReturn[0] = month;
		ctx.calendarSearchReturn[1] = year;
		ctx.calendarSearchModified = true;
	}
	
	private void performBookingSellupAndTaxSearch(String tid, BookingSessionContext ctx, SlingHttpServletRequest request) {

		boolean neutral = bookingSession.setNeutralFieldAndPrepareSellup(ctx, ctx.flightSelections[0].getFlightData());
		if (ctx.flightSelections.length == 2) {
			neutral &= bookingSession.setNeutralFieldAndPrepareSellup(ctx, ctx.flightSelections[1].getFlightData());
		}
		
		//boolean neutral = false;

		// perform the sell-up and save the response data in ctx.selectionRoutes
		SearchBookingSolutionResponse bookingSellupResponse = bookingAwardDelegate.retrieveAwardSellupInformation(
				createBookingAwardSellupRequest(tid, ctx.sid, ctx, neutral));
		
		if (bookingSellupResponse == null) {
			logger.error("The Sellup service has failed");
			throw new IllegalStateException("The Sellup service has failed");
		}
		
		if (bookingSellupResponse.getRoutesData() == null || 
				bookingSellupResponse.getRoutesData().getRoutesList() == null ||
				bookingSellupResponse.getRoutesData().getRoutesList().isEmpty()) {
			logger.error("The Sellup service has not returned solutions");
			throw new IllegalStateException("The Sellup service has not returned solutions");
		}
		
		if (bookingSellupResponse.getRoutesData().getPassengers() == null ||
				bookingSellupResponse.getRoutesData().getPassengers().isEmpty()) {
			logger.error("The Sellup service has not returned passengers list");
			throw new IllegalStateException("The Sellup service has not returned passengers list");
		}
		
		List<PassengerBaseData> passengers = bookingSellupResponse.getRoutesData().getPassengers();
		
		String sessionId = bookingSellupResponse.getRoutesData().getSessionId();
		String solutionSet = bookingSellupResponse.getRoutesData().getSolutionSet();
		String solutionId = bookingSellupResponse.getRoutesData().getRoutesList().get(0).getFlights().get(0).getBrands().get(0).getSolutionId();
		
		// perform the taxes search, using the results of the sell-up
		SearchFlightSolutionResponse bookingTaxSearchResponse = bookingAwardDelegate.retrieveAwardTaxInformation(
				createBookingTaxSearchRequest(tid, ctx.sid, ctx, bookingSellupResponse.getRoutesData(), neutral,
						passengers, sessionId, solutionSet, solutionId));
		if(bookingTaxSearchResponse == null){
			logger.error("The TaxSearch service has failed");
			throw new IllegalStateException("The TaxSearch service has failed");
		}
		// both requests was performed successfully, update the booking context
		ctx.selectionRoutes = bookingSellupResponse.getRoutesData();
		ctx.selectionRoutes.setBookingType(BookingTypeEnum.AWARD);
		ctx.selectionRoutes.setMilleMigliaCustomer(getMmcustomerData(request));
		ctx.resultBookingDetails = bookingSellupResponse.getRoutesData().getProperties();
		ctx.selectionTaxes = bookingTaxSearchResponse.getAvailableFlights().getTaxes();

		// set currency booking process for Multileg
		for (TaxData taxData : ctx.selectionTaxes) {
			if (taxData.getCurrency() != null && !("").equals(taxData.getCurrency())) {
				ctx.currency = taxData.getCurrency();
				break;
			}
		}

		if (ctx.selectionRoutes != null && ctx.selectionRoutes.getRoutesList() != null) {
			ctx.selectionRoutes.setSliceCount(ctx.selectionRoutes.getRoutesList().size());
		}

		// for convenience, we copy the flight data obtained from
		// ctx.selectionRoutes also in the ctx.flightSelections data
		if ( ctx.searchKind != BookingSearchKindEnum.MULTILEG ) {
			for (int i = 0; i < ctx.selectionRoutes.getRoutesList().size(); i++) {
				RouteData  route = ctx.selectionRoutes.getRoutesList().get(i);
				if ( i == 0 && route.getType() == RouteTypeEnum.RETURN ) {
					route = ctx.selectionRoutes.getRoutesList().get(1);
				}
				if ( i==1 && route.getType() == RouteTypeEnum.OUTBOUND ){
					route = ctx.selectionRoutes.getRoutesList().get(0);
				}
				// Rimosso perche' per booking award causa poi problemi
				// per la successiva chiamata al metodo computeSelectedFlights()
				//ctx.flightSelections[i].setFlightData(route.getFlights().get(0));
			}
		} /*else {
		
			for (int i = 0; i < ctx.selectionRoutes.getRoutesList().size(); i++) {
				ctx.flightSelections[i].setFlightData(
						ctx.selectionRoutes.getRoutesList().get(i).getFlights().get(0));
			}
			
		}*/
	}
	
	private SearchFlightSolutionRequest createBookingAwardSellupRequest(String tid, String sid,
			BookingSessionContext ctx, boolean neutral) {

		boolean isMultiSlice = ctx.searchKind == BookingSearchKindEnum.MULTILEG;
		
		FlightSelection flightSelection = (FlightSelection) ctx.flightSelections[ctx.flightSelections.length - 1];
		List<BrandData> brandList = flightSelection.getFlightData().getBrands();
		
		BrandData selectedBrand = null;
		
		for (BrandData brandData : brandList) {
			if (brandData.getCode().equals(flightSelection.getSelectedBrandCode())) {
				selectedBrand = brandData;
				break;
			}
		}
		
		String solutionId = selectedBrand.getSolutionId();

		AwardBookingSearchData bookingSellupSearchData = new AwardBookingSearchData();
		bookingSellupSearchData.setSessionId(ctx.availableFlights.getSessionId());
		bookingSellupSearchData.setSolutionSet(ctx.availableFlights.getSolutionSet());
		
		bookingSellupSearchData.setSolutionId(solutionId);
		bookingSellupSearchData.setBookingSolutionId(solutionId);
		
		bookingSellupSearchData.setPassengers(computePassengersDataList(ctx));
		bookingSellupSearchData.setMultiSlice(isMultiSlice);
		bookingSellupSearchData.setNeutral(neutral);
		bookingSellupSearchData.setType(GatewayTypeEnum.AWARD_SELLUP);
		bookingSellupSearchData.setSelectedFlights(computeSelectedFlights(ctx));
		
		SearchFlightSolutionRequest bookingSellupRequest = new SearchFlightSolutionRequest(tid, sid);
		bookingSellupRequest.setCookie(ctx.cookie);
		bookingSellupRequest.setExecution(ctx.execution);
		bookingSellupRequest.setFilter(bookingSellupSearchData);
		bookingSellupRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		bookingSellupRequest.setMarket(ctx.market);
		bookingSellupRequest.setResponseType(SearchExecuteResponseType.MODEL);
		return bookingSellupRequest;
	}
	
	private SearchFlightSolutionRequest createBookingTaxSearchRequest(String tid, String sid, 
			BookingSessionContext ctx, RoutesData routesData, boolean neutral, 
			List<PassengerBaseData> passengers, String sessionId, String solutionSet, String solutionId) {

		boolean isMultiSlice = ctx.searchKind == BookingSearchKindEnum.MULTILEG;
		
		AwardBookingSearchData bookingTaxSearchData = new AwardBookingSearchData();
		bookingTaxSearchData.setId(routesData.getId());
		bookingTaxSearchData.setSessionId(sessionId);
		bookingTaxSearchData.setSolutionSet(solutionSet);
		
		bookingTaxSearchData.setSolutionId(solutionId);
		bookingTaxSearchData.setBookingSolutionId(solutionId);
		
		List<PassengerBase> passengersList = new ArrayList<PassengerBase>();
		for (PassengerBaseData passenger : passengers) {
			passengersList.add((PassengerBase) passenger);
		}
		
		bookingTaxSearchData.setPassengers(passengersList);
		bookingTaxSearchData.setMultiSlice(isMultiSlice);
		bookingTaxSearchData.setNeutral(neutral);
		bookingTaxSearchData.setType(GatewayTypeEnum.AWARD_TAXES);

		SearchFlightSolutionRequest bookingTaxSearchRequest = new SearchFlightSolutionRequest(tid,sid);
		bookingTaxSearchRequest.setCookie(ctx.cookie);
		bookingTaxSearchRequest.setExecution(ctx.execution);
		bookingTaxSearchRequest.setFilter(bookingTaxSearchData);
		bookingTaxSearchRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		bookingTaxSearchRequest.setMarket(ctx.market);
		bookingTaxSearchRequest.setResponseType(SearchExecuteResponseType.MODEL);

		return bookingTaxSearchRequest;
	}
	

	
	private List<RouteData> computeSelectedFlights(BookingSessionContext ctx) {
		int routeSize = ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP ? 2 : 1;
		ArrayList<RouteData> selectedFlights = new ArrayList<RouteData>();
		for (int i = 0; i < routeSize; i++) {
			RouteData routeData = new RouteData();
			routeData.setIndex(i);
			routeData.setType(i == 0 ? RouteTypeEnum.OUTBOUND : RouteTypeEnum.RETURN);
			FlightData selectedFlightData = ctx.flightSelections[i].getFlightData();
			FlightData clone = null;
			
			if (selectedFlightData instanceof DirectFlightData) {
				clone = new DirectFlightData((DirectFlightData) selectedFlightData);
			} else if (selectedFlightData instanceof ConnectingFlightData) {
				clone = new ConnectingFlightData((ConnectingFlightData) selectedFlightData);
			}
			
			for (BrandData brandData : selectedFlightData.getBrands()) {
				
				if (brandData.getCode()!= null && brandData.getCode().equals(ctx.flightSelections[i].getSelectedBrandCode())) {
					clone.setBrands(new ArrayList<BrandData>());
					clone.getBrands().add(brandData);
					break;
				}
			}
			
			if (clone.getBrands() == null) {
				logger.error("brand code saved in ctx.flightSelections not found in selected flight");
				throw new IllegalStateException("brand code saved in ctx.flightSelections not found in selected flight");
			}
						
			ArrayList<FlightData> flights = new ArrayList<FlightData>();
			flights.add(clone);
			routeData.setFlights(flights);
			selectedFlights.add(routeData);
		}
		return selectedFlights;
	}
	
	private List<PassengerBase> computePassengersDataList(BookingSessionContext ctx) {
		
		List<PassengerBase> listPassengers = new ArrayList<PassengerBase>();
		boolean firstAdult = true;
		
		for (int i = 0; i < ctx.searchPassengersNumber.getNumAdults(); i++) {
			if (firstAdult) {
				ApplicantPassengerData passengerBaseData = new ApplicantPassengerData();
				passengerBaseData.setCouponPrice(BigDecimal.valueOf(0L));
				passengerBaseData.setExtraCharge(BigDecimal.valueOf(0L));
				passengerBaseData.setFee(BigDecimal.valueOf(0L));
				passengerBaseData.setGrossFare(BigDecimal.valueOf(0L));
				passengerBaseData.setNetFare(BigDecimal.valueOf(0L));
				passengerBaseData.setType(PassengerTypeEnum.ADULT);
				passengerBaseData.setArTaxInfoType(ARTaxInfoTypesEnum.NONE);
				passengerBaseData.setBlueBizCode("");
				passengerBaseData.setSkyBonusCode("");
				listPassengers.add(passengerBaseData);
				firstAdult = false;
			} else {
				AdultPassengerData passengerBaseData = new AdultPassengerData();
				passengerBaseData.setCouponPrice(BigDecimal.valueOf(0L));
				passengerBaseData.setExtraCharge(BigDecimal.valueOf(0L));
				passengerBaseData.setFee(BigDecimal.valueOf(0L));
				passengerBaseData.setGrossFare(BigDecimal.valueOf(0L));
				passengerBaseData.setNetFare(BigDecimal.valueOf(0L));
				passengerBaseData.setType(PassengerTypeEnum.ADULT);
				listPassengers.add(passengerBaseData);
			}
		}
		
		for (int i = 0; i < ctx.searchPassengersNumber.getNumChildren(); i++) {
			ChildPassengerData passengerBaseData = new ChildPassengerData(); 
			passengerBaseData.setCouponPrice(BigDecimal.valueOf(0L)); 
			passengerBaseData.setExtraCharge(BigDecimal.valueOf(0L)); 
			passengerBaseData.setFee(BigDecimal.valueOf(0L)); 
			passengerBaseData.setGrossFare(BigDecimal.valueOf(0L)); 
			passengerBaseData.setNetFare(BigDecimal.valueOf(0L)); 
			passengerBaseData.setType(PassengerTypeEnum.CHILD);
			listPassengers.add(passengerBaseData);
		}
		
		for (int i = 0; i < ctx.searchPassengersNumber.getNumInfants(); i++) {
			InfantPassengerData passengerBaseData = new InfantPassengerData();
			passengerBaseData.setCouponPrice(BigDecimal.valueOf(0L)); 
			passengerBaseData.setExtraCharge(BigDecimal.valueOf(0L)); 
			passengerBaseData.setFee(BigDecimal.valueOf(0L)); 
			passengerBaseData.setGrossFare(BigDecimal.valueOf(0L)); 
			passengerBaseData.setNetFare(BigDecimal.valueOf(0L)); 
			passengerBaseData.setType(PassengerTypeEnum.INFANT);
			listPassengers.add(passengerBaseData);
		}
		return listPassengers;
	}
	
	private void computeTotalAwardPrice(BookingSessionContext ctx) {
		BigDecimal price = obtainingAwardPriceForSelectedFlight(ctx);
		
		ctx.totalAwardPrice = price;
		
	}
	
	private BigDecimal obtainingAwardPriceForSelectedFlight(BookingSessionContext ctx) {
		BigDecimal price = BigDecimal.ZERO;
		int k;
		if (ctx.flightSelections != null) {
			for (k=0; k<ctx.flightSelections.length; k++) {
				if (ctx.flightSelections[k]!= null &&  ctx.flightSelections[k].getAwardPrice() != null) {
					price = price.add(ctx.flightSelections[k].getAwardPrice());
				}
				
			}
			return price;
		}
		return BigDecimal.ZERO;
		
		
	}
	
	private MMCustomerProfileData retrieveLoggedUserProfile(BookingSessionContext ctx, SlingHttpServletRequest request) {
		// Recupero informazioni sull'utente loggato
		MMCustomerProfileData customerProfileData = AlitaliaUtils.getAuthenticatedUser(request);
		if (customerProfileData != null) {
			customerProfileData.setCustomerPinCode(
					customerProfileManager.decodeAndDecryptProperty(customerProfileData.getCustomerPinCode()));

			//chiamata al servizio getProfile
			ProfileRequest profileRequest = new ProfileRequest();
			profileRequest.setSid(IDFactory.getSid());
			profileRequest.setTid(IDFactory.getTid());
			profileRequest.setCustomerProfile(customerProfileData);
			ProfileResponse profileResponse = consumerLoginDelegate.getProfile(profileRequest);
			return profileResponse.getCustomerProfile();
		}
		return null;
	}
	
	
	
	
}
