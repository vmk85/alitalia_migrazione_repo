package com.alitalia.aem.consumer.booking.render;

public class AirportDetailsRender {
	
	String airportName;
	String distanceFromCity;
	String terminal;
	String timeZone;
	String checkInLimitDOM;
	String checkInLimitINTZ;
	String checkInLimitINTC;
	
	public String getDistanceFromCity() {
		return distanceFromCity;
	}
	public void setDistanceFromCity(String distanceFromCity) {
		this.distanceFromCity = distanceFromCity;
	}
	public String getTerminal() {
		return terminal;
	}
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public String getCheckInLimitDOM() {
		return checkInLimitDOM;
	}
	public void setCheckInLimitDOM(String checkInLimitDOM) {
		this.checkInLimitDOM = checkInLimitDOM;
	}
	public String getCheckInLimitINTZ() {
		return checkInLimitINTZ;
	}
	public void setCheckInLimitINTZ(String checkInLimitINTZ) {
		this.checkInLimitINTZ = checkInLimitINTZ;
	}
	public String getCheckInLimitINTC() {
		return checkInLimitINTC;
	}
	public void setCheckInLimitINTC(String checkInLimitINTC) {
		this.checkInLimitINTC = checkInLimitINTC;
	}
	public String getAirportName() {
		return airportName;
	}
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	
	
	

}
