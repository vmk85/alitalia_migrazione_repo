package com.alitalia.aem.consumer.model.content.substriptionsme;

public class SubscriptionSmeRequestData {

	public static final String NAME = "SubscriptionSmeRequestData";

	private String error;

	private Company company;
	private AdministratorAccount adminAccount; 
	private Area area;
	private Preferences preferences;
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public AdministratorAccount getAdminAccount() {
		return adminAccount;
	}
	public void setAdminAccount(AdministratorAccount adminAccount) {
		this.adminAccount = adminAccount;
	}
	public Area getArea() {
		return area;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public Preferences getPreferences() {
		return preferences;
	}
	public void setPreferences(Preferences preferences) {
		this.preferences = preferences;
	}
	public static String getName() {
		return NAME;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adminAccount == null) ? 0 : adminAccount.hashCode());
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result + ((error == null) ? 0 : error.hashCode());
		result = prime * result + ((preferences == null) ? 0 : preferences.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubscriptionSmeRequestData other = (SubscriptionSmeRequestData) obj;
		if (adminAccount == null) {
			if (other.adminAccount != null)
				return false;
		} else if (!adminAccount.equals(other.adminAccount))
			return false;
		if (area == null) {
			if (other.area != null)
				return false;
		} else if (!area.equals(other.area))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (error == null) {
			if (other.error != null)
				return false;
		} else if (!error.equals(other.error))
			return false;
		if (preferences == null) {
			if (other.preferences != null)
				return false;
		} else if (!preferences.equals(other.preferences))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "SubscriptionSmeRequestData [error=" + error + ", company=" + company + ", adminAccount=" + adminAccount
				+ ", area=" + area + ", preferences=" + preferences + "]";
	}
	
}