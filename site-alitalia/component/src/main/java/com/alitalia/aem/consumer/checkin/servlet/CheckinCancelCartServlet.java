package com.alitalia.aem.consumer.checkin.servlet;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryShoppingCartServiceErrorEnum;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.PDFManager;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinFlightDataMail;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.servlet.PDFCreatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;
import java.lang.*;
import java.math.BigDecimal;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkincancelcartconsumer" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinCancelCartServlet extends GenericCheckinFormValidatorServlet {

	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	
	@Reference
	private CheckinSession checkinSession;
	
	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		logger.debug("[CheckinCancelCartServlet] validateForm");
		
		// validate form
		ResultValidation resultValidation = performValidation(request);
		
		// return validation
		return resultValidation;

	}
	
	/*
	 * Perform validation
	 */
	private ResultValidation performValidation(
			SlingHttpServletRequest request) {
		logger.debug("[CheckinCancelCartServlet] performValidation");
		
		// get CTX context data
		//CheckinSessionContext ctx = getCheckInSessionContext(request);
		
		// create a new Validator
		Validator validator = new Validator();
		
		return validator.validate();
		
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
		logger.debug("[CheckinCancelCartServlet] performSubmit");
		
		String result = "OK";
		
		// get ctx context data
		CheckinSessionContext ctx = getCheckInSessionContext(request);
//		Thread.sleep(2000);
		
		if(!ctx.cartTotalAmount.equals(BigDecimal.ZERO)){
			/*call CancelCart service*/
			List<MmbAncillaryErrorData> errors = checkinSession.cancelCart(ctx);
			if(errors != null && errors.size() > 0){
				for(MmbAncillaryErrorData err : errors){
					if(err != null){
						logger.error("Error in CancelCart for PNR " + ctx.pnr
								+". Code=" + err.getCode() + ", Description=" + err.getDescription());
					}
					else{
						logger.error("Unknown error in CancelCart for PNR " + ctx.pnr + ".");
					}
				}
				result = "KO";
			}
			else if(errors != null){
				logger.warn("Unknown errors in CancelCart for PNR " + ctx.pnr + ".");
				result = "KO";
			}
		}
		
		// send response to client
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		json.key("result").value(result);
		json.endObject();
		
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}
