package com.alitalia.aem.consumer.booking.model.extrabaggage;

import java.util.*;

/** 
 Common response class for deserializing the flight informations
*/
public class Flight
{
	private int flightNumber;
	public final int getflightNumber()
	{
		return flightNumber;
	}
	public final void setflightNumber(int value)
	{
		flightNumber = value;
	}
	private String airlineCode;
	public final String getairlineCode()
	{
		return airlineCode;
	}
	public final void setairlineCode(String value)
	{
		airlineCode = value;
	}
	private String operatingAirlineCode;
	public final String getoperatingAirlineCode()
	{
		return operatingAirlineCode;
	}
	public final void setoperatingAirlineCode(String value)
	{
		operatingAirlineCode = value;
	}
	private ArrayList<StopAirport> stopAirports;
	public final ArrayList<StopAirport> getstopAirports()
	{
		return stopAirports;
	}
	public final void setstopAirports(ArrayList<StopAirport> value)
	{
		stopAirports = value;
	}
}