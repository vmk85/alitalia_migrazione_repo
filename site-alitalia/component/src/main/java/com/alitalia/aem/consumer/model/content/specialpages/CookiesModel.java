package com.alitalia.aem.consumer.model.content.specialpages;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CookiesModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private CookiesData cookiesData;

	@PostConstruct
	protected void initModel() {
		logger.debug("[CookiesModel] initModel");

		super.initBaseModel(slingHttpServletRequest);

		// retrieve and remove CookiesData from session if any
		cookiesData = (CookiesData) slingHttpServletRequest.getSession()
				.getAttribute(CookiesData.NAME);
		slingHttpServletRequest.getSession().removeAttribute(CookiesData.NAME);

	}

	public CookiesData getCookiesData() {
		return cookiesData;
	}

}
