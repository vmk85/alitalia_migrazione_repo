package com.alitalia.aem.consumer.booking.controller;


import com.alitalia.aem.common.data.home.*;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.AncillaryCompDataHolder;
import com.alitalia.aem.consumer.booking.model.AncillaryPassenger;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.model.Passenger;
import com.alitalia.aem.consumer.booking.render.SelectedFlightInfoRender;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Model(adaptables = {SlingHttpServletRequest.class})
public class BookingExtrabag extends BookingSessionGenericController {

    @Self
    private SlingHttpServletRequest request;

    @Inject
    private BookingSession bookingSession;

    private List<AncillaryPassenger> passengers;
    private String[] sliceName;
    private SelectedFlightInfoRender[] flights;
//    private boolean enabled;
    private boolean fam;
//    private boolean showLimitedMessage;
//    private static final String AIRPORT_CODE_TLV = "TLV";


    private String departureAirport;


    @PostConstruct
    protected void initModel() throws Exception {

    	
    	sliceName[0]="CICCIO";
    	sliceName[1]="PIPPO";
        try {
/*
            initBaseModel(request);
            if (ctx == null) {
                return;
            }
//			showLimitedMessage = ctx.isLimitedMeals;
            passengers = new ArrayList<AncillaryPassenger>();
//			fam = false;
//			if (ctx.cug == BookingSearchCUGEnum.FAMILY) {
//				if (ctx.familySolutionFound) {
//					fam=true;
//				}
//			}
//			if (enabled) {
            List<Passenger> passengersList = ctx.passengersData.getPassengersList();

            String applicantMealPreference = bookingSession.retrieveLoggedUserMealPreferences(ctx);

            Passenger applicantPassenger = passengersList.get(0);
            if (applicantPassenger != null) {
                AncillaryPassenger ancillaryPassenger = new AncillaryPassenger();
                ancillaryPassenger.setName(applicantPassenger.getName());
                ancillaryPassenger.setSurname(applicantPassenger.getSurname());
                ancillaryPassenger.setSecondName(applicantPassenger.getSecondName());
                ancillaryPassenger.setMealPreference(applicantMealPreference);
                if (ctx.selectionRoutes.getPassengers().get(0).getType() == PassengerTypeEnum.CHILD) {
                    ancillaryPassenger.setIsChild(true);
                }
                passengers.add(ancillaryPassenger);
                for (int j = 0; j < passengersList.size(); j++) {
                    Passenger referencePassenger = passengersList.get(j);
                    if (referencePassenger.isInfant() && referencePassenger.getReferenceAdult() == 0) {
                        ancillaryPassenger.setIsInfantReferenced(true);

                        int i = 1;
                        while (passengers.get(j-i).isChild()){
                            i++;
                        }
                        passengers.get(j-i).setInfantReferenced(referencePassenger);

                    }
                }
            }
            for (int k = 1; k < passengersList.size(); k++) {

                Passenger passenger = passengersList.get(k);
                if (!passenger.isInfant()) {
                    AncillaryPassenger ancillaryPassenger = new AncillaryPassenger();
                    ancillaryPassenger.setName(passenger.getName());
                    if (passenger.getSecondName() != null) {
                        ancillaryPassenger.setSecondName(passenger.getSecondName());
                    }
                    ancillaryPassenger.setSurname(passenger.getSurname());
                    ancillaryPassenger.setMealPreference("");
                    if (ctx.selectionRoutes.getPassengers().get(k).getType() == PassengerTypeEnum.CHILD) {
                        ancillaryPassenger.setIsChild(true);
                    }

                    for (int j = 0; j < passengersList.size(); j++) {
                        Passenger referencePassenger = passengersList.get(j);
                        if (referencePassenger.isInfant() && referencePassenger.getReferenceAdult() == k) {
                            ancillaryPassenger.setIsInfantReferenced(true);
                        }
                    }

                    passengers.add(ancillaryPassenger);
                }

            }
            sliceName = computeSliceName(ctx);
            flights = new SelectedFlightInfoRender[ctx.flightSelections.length];
            int i = 0;
            for (FlightSelection selectedFlight : ctx.flightSelections) {
                flights[i] = new SelectedFlightInfoRender(selectedFlight.getFlightData(), i18n, ctx.market);
                i++;
            }

//			}
*/

        } catch (Exception e) {
            logger.error("unexpected error: ", e);
            throw e;
        }
    }

    
    public SlingHttpServletRequest getRequest() {
        return request;
    }

    public BookingSession getBookingSession() {
        return bookingSession;
    }

    public List<AncillaryPassenger> getPassengers() {
        return passengers;
    }

    public String[] getSliceName() {
        return sliceName;
    }

    public SelectedFlightInfoRender[] getFlights() {
        return flights;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

//    public boolean getEnabled() {
//        return enabled;
//    }

//    public boolean isFam() {
//        return fam;
//    }

//    public boolean getShowLimitedMessage() {
//        return showLimitedMessage;
//    }














	/* private methods */

    private String computePassengerName(List<PassengerBaseData> passengersList, int i) {
        PassengerBaseData passenger = passengersList.get(i);
        if (passenger instanceof AdultPassengerData) {
            AdultPassengerData typedPassenger = (AdultPassengerData) passenger;
            if (typedPassenger.getLastName() == null) {
                return null;
            }
            String secondName = "";
            if (typedPassenger.getInfo() instanceof PassengerApisSecureFlightInfoData) {
                PassengerApisSecureFlightInfoData apisSecureFlightInfo = (PassengerApisSecureFlightInfoData) typedPassenger.getInfo();
                if (apisSecureFlightInfo.getSecondName() != null) {
                    secondName = " " + apisSecureFlightInfo.getSecondName() + " ";
                }
            } else if (typedPassenger.getInfo() instanceof PassengerSecureFlightInfoData) {
                PassengerSecureFlightInfoData secureFlightInfo = (PassengerSecureFlightInfoData) typedPassenger.getInfo();
                if (secureFlightInfo.getSecondName() != null) {
                    secondName = " " + secureFlightInfo.getSecondName() + " ";
                }
            }
            return typedPassenger.getLastName() + " " + typedPassenger.getName() + secondName;
        } else if (passenger instanceof ChildPassengerData) {
            ChildPassengerData typedPassenger = (ChildPassengerData) passenger;
            if (typedPassenger.getLastName() == null) {
                return null;
            }
            String secondName = "";
            if (typedPassenger.getInfo() instanceof PassengerApisSecureFlightInfoData) {
                PassengerApisSecureFlightInfoData apisSecureFlightInfo = (PassengerApisSecureFlightInfoData) typedPassenger.getInfo();
                if (apisSecureFlightInfo.getSecondName() != null) {
                    secondName = " " + apisSecureFlightInfo.getSecondName() + " ";
                }
            } else if (typedPassenger.getInfo() instanceof PassengerSecureFlightInfoData) {
                PassengerSecureFlightInfoData secureFlightInfo = (PassengerSecureFlightInfoData) typedPassenger.getInfo();
                if (secureFlightInfo.getSecondName() != null) {
                    secondName = " " + secureFlightInfo.getSecondName() + " ";
                }
            }
            return typedPassenger.getLastName() + " " + typedPassenger.getName() + secondName;
        }
        return null;
    }


    private String[] computeSliceName(BookingSessionContext ctx) {
        String[] sliceName = new String[ctx.flightSelections.length];
        if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
            for (int i = 0; i < ctx.flightSelections.length; i++) {
                sliceName[i] = obtainTitleBySliceIndex(i);
            }
        } else {
            for (int i = 0; i < ctx.flightSelections.length; i++) {
                sliceName[i] = obtainARMessage(i);
            }
        }
        return sliceName;
    }

    private String obtainARMessage(int i) {
        switch (i) {
            case 0: {
                return i18n.get("booking.carrello.andata.label"); //"andata"
            }
            case 1: {
                return i18n.get("booking.carrello.ritorno.label"); //"ritorno"
            }
        }
        return "";
    }

    private String obtainTitleBySliceIndex(int sliceIndex) {
        String msg = "";
        switch (sliceIndex) {
            case 0: {
                msg = i18n.get("booking.common.multitratta.prima.label"); //"PRIMA"
            }
            break;
            case 1: {
                msg = i18n.get("booking.common.multitratta.seconda.label"); //"SECONDA"
            }
            break;
            case 2: {
                msg = i18n.get("booking.common.multitratta.terza.label"); //"TERZA"
            }
            break;
            case 3: {
                msg = i18n.get("booking.common.multitratta.quarta.label"); //"QUARTA"
            }
            break;
            default: {
                return "";
            }
        }
        return msg + " " + i18n.get("booking.common.multitratta.tratta.label"); //"TRATTA"
    }

//    private void computeToShowBoxSeatandKosherMeal() {
//        Date departureDate = new Date();
//        Date today = Calendar.getInstance().getTime();
//        String depAirportCode = "";
////         Get the first route to extract the first departure Time to calculate is before 24hours or 48 hours from departure
//        FlightData departureFlight = flights[0].getFlightData();
//        if (departureFlight instanceof ConnectingFlightData) {
//            DirectFlightData connDepartureFlight = (DirectFlightData) ((ConnectingFlightData) departureFlight).getFlights().get(0);
//            departureDate = connDepartureFlight.getDepartureDate().getTime();
//            depAirportCode = connDepartureFlight.getFrom().getCode();
//        } else if (departureFlight instanceof DirectFlightData) {
//            DirectFlightData directDepartureFlight = (DirectFlightData) departureFlight;
//            departureDate = directDepartureFlight.getDepartureDate().getTime();
//            depAirportCode = directDepartureFlight.getFrom().getCode();
//
//        }
//
//        long diffHours = (departureDate.getTime() - today.getTime()) / MILLI_TO_HOUR;
//
//        if (diffHours < 48) {
//            showKosherMeal = false;
//            if (diffHours <= 24) {
//                showBoxMeal = false;
//            } else if (diffHours > 24 && AIRPORT_CODE_TLV.equalsIgnoreCase(depAirportCode)) {
//                showKosherMeal = true;
//            }
//        }
//    }


//    public String getKosherMealCode() {
//        return i18n.get("mealsData.KSML");
//    }






}
