package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Enumerazione per il tipo di servizio speciale per gli ancillari. 
   ['STRUCTURED', 'NOTALLOWED', 'OPTIONAL', 'REQUIRED']
*/
public enum AncillarySpecialServiceType
{
	STRUCTURED,
	NOTALLOWED,
	OPTIONAL,
	REQUIRED;

	public static final int SIZE = java.lang.Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static AncillarySpecialServiceType forValue(int value)
	{
		return values()[value];
	}
}