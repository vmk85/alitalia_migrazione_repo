package com.alitalia.aem.consumer.checkinrest.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.checkinrest.CheckinEndPaymentResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinPayment3DRedirectModel extends GenericCheckinModel {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private SlingHttpServletResponse response;

	@Inject
	private CheckinSession checkinSession;

	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private String urlSuccessRedirect = "";
	
	private String urlErrorRedirect = "";

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@PostConstruct
	protected void initModel() throws Exception {

		try {
			super.initBaseModel(request);
			String approvalCode = request.getParameter("ApprovalCode");
			CheckinEndPaymentResponse result = checkinSession.endPayment(request, ctx, approvalCode);

			if (result != null) {
				if(result.getEndPaymentResponse().getError() != null && !result.getEndPaymentResponse().getError().equals("")){
					managementError(request, response, result.getEndPaymentResponse().getError());
				}else{
					//response.sendRedirect(successPage);
					managementSuccess(request, response, result, ctx.redirectUrl);
				}
			} else {
				logger.error("[CheckinPayment3DRedirectModel] - Errore durante l'invocazione del servizio." );
				managementError(request, response, "Errore nell'invocazione del servizio.");
			}


		}
		catch (Exception e) {
			logger.error("[CheckinPayment3DRedirectModel][initModel] - errore nella redirect 3Ds.");
			e.getMessage();
		}
	}

	public void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error){

		logger.info("[CheckinPayment3DRedirectModel] [managementError] error =" + error );
		
		String errorPage = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false) + configuration.getCheckinThankYouPage(); //TODO sostituire con pagina di errore

		errorPage = request.getResourceResolver().map(errorPage);
		
		setUrlErrorRedirect(errorPage);

		logger.info("[CheckinPayment3DRedirectModel] [managementSuccess 3D] successPage = " + errorPage );

//		Gson gson = new Gson();
//
//		com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);
//
//		TidyJSONWriter json;
//		try {
//			json = new TidyJSONWriter(response.getWriter());
//
//			response.setContentType("application/json");
//
//			json.object();
//			json.key("isError").value(true);
//			json.key("errorMessage").value(errorObj.getErrorMessage());
//			json.key("sabreStatusCode").value(errorObj.getSabreStatusCode());
//			json.key("conversationID").value(errorObj.getConversationID());
//			json.endObject();
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}	
	}

	public void managementSuccess(SlingHttpServletRequest request, SlingHttpServletResponse response, CheckinEndPaymentResponse result, String redirectUrl){

		logger.info("[CheckinPayment3DRedirectModel] [managementSuccess] ...");

		//TidyJSONWriter json;
//		try {
//			json = new TidyJSONWriter(response.getWriter());
//			response.setContentType("application/json");

				String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
						request.getResource(), false) + configuration.getCheckinThankYouPage();

				successPage = request.getResourceResolver().map(successPage);
				
				setUrlSuccessRedirect(successPage);

				logger.info("[CheckinPayment3DRedirectModel] [managementSuccess 3D] successPage = " + successPage );

//				json.object();
//				json.key("isError").value(false);
//				json.key("successPage").value(successPage);
//				json.endObject();
//			}
//		catch (IOException e) {
//			e.printStackTrace();
//		}	

	}

	public String getUrlSuccessRedirect() {
		return urlSuccessRedirect;
	}

	public void setUrlSuccessRedirect(String urlSuccessRedirect) {
		this.urlSuccessRedirect = urlSuccessRedirect;
	}

	public String getUrlErrorRedirect() {
		return urlErrorRedirect;
	}

	public void setUrlErrorRedirect(String urlErrorRedirect) {
		this.urlErrorRedirect = urlErrorRedirect;
	}



}
