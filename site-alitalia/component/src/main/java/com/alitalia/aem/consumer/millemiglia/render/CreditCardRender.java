package com.alitalia.aem.consumer.millemiglia.render;


public class CreditCardRender {
	
	private String circuitCode;
	private String number;
	private String expireDate;
	private String cardType;
	private String token;
	
	public String getCircuitCode() {
		return circuitCode;
	}
	public void setCircuitCode(String circuitCode) {
		this.circuitCode = circuitCode;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
}