package com.alitalia.aem.consumer.booking.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;

@Model(adaptables = { SlingHttpServletRequest.class })
public class KeepAliveSessionController extends BookingSessionGenericController {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;

	@Inject
	private SlingHttpServletResponse response;
	
	private int numberOfTimeout = 0;
	private int timeout = 0;

	@PostConstruct
	protected void initModel() throws Exception {

		try {
	    	numberOfTimeout = 0; //FIXME configuration.getNumberOfTimeout();
	    	timeout = 0; //FIXME configuration.getTimeout();
		} catch (Exception e) {
			logger.error("unexpected error ", e);
			if (!response.isCommitted()){
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
			}
		}

	}
	
	public int getNumberOfTimeout() {
		return numberOfTimeout;
	}
	
	public int getTimeout() {
		return timeout;
	}
	
	
}