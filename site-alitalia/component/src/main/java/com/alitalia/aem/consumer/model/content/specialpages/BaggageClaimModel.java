package com.alitalia.aem.consumer.model.content.specialpages;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.global.enumeration.BaggageClaimReasons;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BaggageClaimModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private BaggageClaimData baggageClaimData;
	private List<String> months;
	private List<Integer> days = new ArrayList<Integer>();
	private List<Integer> years = new ArrayList<Integer>();
	private Map<String, String> reasons = new LinkedHashMap<String, String>();

	@PostConstruct
	protected void initModel() {
		logger.debug("[BaggageClaimModel] initModel");

		super.initBaseModel(slingHttpServletRequest);

		// retrieve and remove BaggageClaimData from session if any
		baggageClaimData = (BaggageClaimData) 
				slingHttpServletRequest.getSession().getAttribute(BaggageClaimData.NAME);
		slingHttpServletRequest.getSession().removeAttribute(BaggageClaimData.NAME);

		for (int i = 1; i < 31; i++) {
			days.add(i);
		}

		months = I18nKeyCommon.getMonths();

		for (int i = 0; i < 10; i++) {
			years.add((Calendar.getInstance().get(Calendar.YEAR) - i));
		}

		reasons.put(BaggageClaimReasons.CANCELED_FLIGHT.toString(),
				BaggageClaimReasons.CANCELED_FLIGHT.getI18nValue());
		reasons.put(BaggageClaimReasons.LOST_LUGGAGE.toString(),
				BaggageClaimReasons.LOST_LUGGAGE.getI18nValue());
		reasons.put(BaggageClaimReasons.OTHER_REASON.toString(),
				BaggageClaimReasons.OTHER_REASON.getI18nValue());

		logger.debug("[BaggageClaimModel] data years: " + years.toString());
		logger.debug("[BaggageClaimModel] data months: " + months.toString());
		logger.debug("[BaggageClaimModel] data days: " + days.toString());
		logger.debug("[BaggageClaimModel] data reasons: " + reasons.toString());
	}

	public BaggageClaimData getBaggageClaimData() {
		return baggageClaimData;
	}

	public List<String> getMonths() {
		return months;
	}

	public List<Integer> getDays() {
		return days;
	}

	public List<Integer> getYears() {
		return years;
	}

	public Map<String, String> getReasons() {
		return reasons;
	}

}
