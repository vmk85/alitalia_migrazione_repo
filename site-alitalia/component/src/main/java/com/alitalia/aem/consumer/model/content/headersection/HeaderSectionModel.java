package com.alitalia.aem.consumer.model.content.headersection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.utils.LabelUrl;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;

@Model(adaptables = { SlingHttpServletRequest.class })
public class HeaderSectionModel {

	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(HeaderSectionModel.class);

	@Self
	private SlingHttpServletRequest request;
	
	private String relatedSecondLevelPageTitle;
	private String relatedSecondLevelPageDescription;
	private LabelUrl[] brotherPages;
	
	/**
	 * This method is used to retrieve title and description of parent page and all "brothers" page of current page
	 */
	@PostConstruct
	protected void initModel() {
		final Resource resource = request.getResource();

		List<LabelUrl> brotherPagesList = new ArrayList<LabelUrl>();
		PageFilter pageFilter = new PageFilter(request);

		// risalgo i parent della resource fino a raggiungere la prima pagina
		// (che è la pagina corrente)
		Resource parentResource = resource;
		while(parentResource.adaptTo(Page.class) == null) {
			parentResource = parentResource.getParent();
		}
		
		Page currentPage = parentResource.adaptTo(Page.class);
		Page relatedSeconLevelPage = parentResource.getParent().adaptTo(Page.class);
		
		relatedSecondLevelPageTitle = relatedSeconLevelPage.getTitle();
		relatedSecondLevelPageDescription = relatedSeconLevelPage.getDescription();
		
		Iterator<Page> brotherPagesIterator = relatedSeconLevelPage.listChildren(pageFilter); 
		while(brotherPagesIterator.hasNext()) {
			Page brotherPage = brotherPagesIterator.next();
			if(brotherPage.getProperties().get("cq:template", String.class).equals("/apps/alitalia/templates/redazionale-principale-2-colonne")){
			brotherPagesList.add(new LabelUrl(brotherPage.getTitle(), brotherPage.getPath(), currentPage.equals(brotherPage)));
			}
			}
			
		
		brotherPages = (LabelUrl[]) brotherPagesList.toArray(new LabelUrl[brotherPagesList.size()]);		
	}

	/**
	 * Returns title of related second level page
	 * 
	 * @return The relatedSecondLevelPageTitle
	 */
	public String getRelatedSecondLevelPageTitle() {
		return relatedSecondLevelPageTitle;
	}
	
	/**
	 * Returns description of related second level page
	 * 
	 * @return The relatedSecondLevelPageDescription
	 */
	public String getRelatedSecondLevelPageDescription() {
		return relatedSecondLevelPageDescription;
	}

	/**
	 * Returns title and url of editorial main two columns at the same level of current page
	 * 
	 * @return The relatedSecondLevelPageDescription
	 */
	public LabelUrl[] getBrotherPages() {
		return brotherPages;
	}
}
