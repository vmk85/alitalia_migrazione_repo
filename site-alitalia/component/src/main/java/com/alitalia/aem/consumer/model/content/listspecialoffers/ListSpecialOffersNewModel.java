package com.alitalia.aem.consumer.model.content.listspecialoffers;

import com.alitalia.aem.common.data.home.CodeDescriptionData;
import com.alitalia.aem.common.data.home.OfferData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersResponse;
import com.alitalia.aem.common.utils.AlitaliaCommonUtils;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.OfferItem;
import com.alitalia.aem.web.component.geospatial.delegate.GeoSpatialDelegate;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.day.cq.wcm.api.Page;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.*;

@Model(adaptables={SlingHttpServletRequest.class})
public class ListSpecialOffersNewModel extends GenericBaseModel {

	private static final String DEFAULT_BUTTON_LABEL = "Prenota";

	private static final String DEFAULT_PRICE_LABEL = "Da";

	private static final String BUTTON_LABEL_ATTRIBUTE_NAME = "etichetta-pulsante";

	private static final String PRICE_LABEL_ATTRIBUTE_NAME = "etichetta-prezzo";

	private static final String SPECIAL_OFFERS_LIST_COMPONENT_NAME = "lista-offerte-speciali";

	private static final String OFFERS_LIST_COMPONENT_NAME = "lista-offerte";

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private GeoSpatialDelegate geoSpatialDelegate;
	
	@Inject
	private SearchFlightsDelegate searchFlightsDelegate;
	
	@Inject
    private AlitaliaConfigurationHolder alitaliaConfigurationHolder;

	private ArrayList<OfferItem> offers = new ArrayList<OfferItem>();
	private ArrayList<OfferItem> offersItaliaOrYoung = new ArrayList<OfferItem>();

	private ArrayList<OfferItem> offersMondo = new ArrayList<OfferItem>();
	
	private ArrayList<OfferItem> offersEuropa = new ArrayList<OfferItem>();

    private ArrayList<OfferItem> offersEuropaMondo = new ArrayList<OfferItem>();

    private String card1editorial;
	private String card2editorial;
	private String card3editorial;
	private String card4editorial;
	private String list1editorial;
	private String list2editorial;
	private String list3editorial;
	private String list4editorial;
	private String list5editorial;
	private String list6editorial;
	private String list7editorial;
	private String list8editorial;
	private String list9editorial;

	// Su indicazione di ENIS, in caso di mercato Ungheria va inserita 
	// la classe "invert"
	private boolean invertPrice;

	private String priceLabel;
	private String buttonLabel;
	private String destinazione;

	@PostConstruct
	protected void initModel() {


        logger.debug("[ListSpecialOffersModel] initModel");
		super.initBaseModel(request);
		//TODO: controllare
		if(logger.isDebugEnabled())
			logger.debug("Site market code: " + marketCode);
		// (lista-offerte o lista-offerte-speciali)
		priceLabel = AlitaliaUtils.getComponentProperty(
				AlitaliaUtils.getPage(request.getResource()),
				OFFERS_LIST_COMPONENT_NAME, PRICE_LABEL_ATTRIBUTE_NAME);
		if (priceLabel == null) {
			priceLabel = AlitaliaUtils.getComponentProperty(
					AlitaliaUtils.getPage(request.getResource()),
					SPECIAL_OFFERS_LIST_COMPONENT_NAME, PRICE_LABEL_ATTRIBUTE_NAME);
			if (priceLabel == null) {
				priceLabel = DEFAULT_PRICE_LABEL;
			}
		}

		buttonLabel = AlitaliaUtils.getComponentProperty(
				                        AlitaliaUtils.getPage(request.getResource()),
                                        OFFERS_LIST_COMPONENT_NAME,
                                        BUTTON_LABEL_ATTRIBUTE_NAME);
		if (buttonLabel == null) {
			buttonLabel = AlitaliaUtils.getComponentProperty(
                                    AlitaliaUtils.getPage(request.getResource()),
                                    SPECIAL_OFFERS_LIST_COMPONENT_NAME,
                                    BUTTON_LABEL_ATTRIBUTE_NAME);
			if (buttonLabel == null) {
				buttonLabel = DEFAULT_BUTTON_LABEL;
			}
		}

		// get input from request
		//String paxType = request.getParameter("paxType");
		//boolean isHomePage = Boolean.parseBoolean(request.getParameter("isHomePage")) || false;
		//boolean isYoung = Boolean.parseBoolean(request.getParameter("isYoung")) || false;
		//boolean tab1AsteriskInPrice = Boolean.parseBoolean(request.getParameter("tab1AsteriskInPrice"));
		//boolean tab2AsteriskInPrice = Boolean.parseBoolean(request.getParameter("tab2AsteriskInPrice"));
		//String departure = request.getParameter("from");
		//boolean italyWorldListsOnly = Boolean.parseBoolean(request.getParameter("italyWorldListsOnly"));
		
		//if(request.getRequestPathInfo().getSelectorString() != null){
			String[] codeArray = request.getRequestPathInfo().getSelectors();
            String departure =codeArray[0];
			
			boolean isHomePage =codeArray[1].equals("true") ? true : false;
			boolean isYoung = codeArray[2].equals("true") ? true : false;
            String paxType =codeArray[3];
            
			boolean tab1AsteriskInPrice = codeArray[4].equals("true") ? true : false;
			boolean tab2AsteriskInPrice = codeArray[5].equals("true") ? true : false;
			
			boolean italyWorldListsOnly=codeArray[6].equals("true") ? true : false;
		//}
		
		if(logger.isDebugEnabled())
			logger.debug("[ListSpecialOffersModel] request: paxType: " + paxType 
				+ "; isHomePage: " + isHomePage + "; isYoung: " + isYoung 
				+ "; departure: " + departure + "; tab1AsteriskInPrice: " 
				+ tab1AsteriskInPrice + "; tab2AsteriskInPrice: " 
				+ tab2AsteriskInPrice);

		invertPrice = (getMarketCode().equals(AlitaliaConstants.COUNTRY_UNG)  ? true : false);
		
		// populate list of airports
		ArrayList<String> airportList = new ArrayList<String>();
		populateAirportsList(airportList, paxType);
		
		// check id departure is a valid airport
		if (departure != null && airportList.contains(departure)) {

			// populate sorted lists of offers for Italia
			// and International destinations
			List<OfferData> offersListItaliaOrYoung = new ArrayList<OfferData>();
			List<OfferData> offersListMondo = new ArrayList<OfferData>();
			List<OfferData> offersListEuropa = new ArrayList<OfferData>();
			
			populateSortedDestinationOffersLists(offersListItaliaOrYoung, offersListEuropa,	offersListMondo, departure.toLowerCase(), paxType, isYoung,italyWorldListsOnly);
			
			// populate offers list for Italia destinations, retrieve image
			// paths and create final lists of offers
			ArrayList<String> destinationsListItaliaOrYoung = new ArrayList<String>();
			LinkedHashMap<String, OfferItem> offersMapItaliaOrYoung = new LinkedHashMap<String, OfferItem>();
			
			String destinationsPath = AlitaliaUtils.findSiteBaseRepositoryPath(request.getResource()) + AlitaliaConstants.DESTINATIONS_PATH_1;
			
			List<Page> destinations = findPageByTemplate(request, destinationsPath, AlitaliaConstants.TEMPLATE_DESTINATION_PAGE_1);
			HashMap<String, String> destinationsMap = new HashMap<String, String>();
			for (Page page : destinations) {
				destinationsMap.put(page.getName(), page.getPath() + "/jcr:content/image-lista-offerte");
			}


			String editorialOriginsPath = AlitaliaUtils.findSiteBaseRepositoryPath(request.getResource()) + AlitaliaConstants.EDITORIAL_DESTINATIONS_PATH_1;

			List<Page> editorialOrigins = findPageByTemplate(request, editorialOriginsPath, "/apps/alitalia/templates/origin-city-editorial-1");

			HashMap<String, String> editorialOriginsMap = new HashMap<String, String>();
			for (Page page : editorialOrigins) {
                editorialOriginsMap.put(page.getName(), page.getPath() + "/jcr:content/"); //qui sono da leggere tutti 4 i blocchi!
			}
            // editorialOriginsMap mappa key=ROM con value=/content/alitalia/master-it/it/alitalia-config/special-offers-editorial/ROM/jcr:content/ con poi blocchieditoriali1-2-3-4 e listeeditoriali
            // da usare per richiamare il blocco editoriale
            try {
                card1editorial = request.getResource().getResourceResolver().getResource(editorialOriginsPath + "/" + departure).adaptTo(Page.class).getContentResource("blocchieditoriali1").adaptTo(ValueMap.class).get("card1editorial", "false");
                card2editorial = request.getResource().getResourceResolver().getResource(editorialOriginsPath + "/" + departure).adaptTo(Page.class).getContentResource("blocchieditoriali2").adaptTo(ValueMap.class).get("card1editorial", "false");
                card3editorial = request.getResource().getResourceResolver().getResource(editorialOriginsPath + "/" + departure).adaptTo(Page.class).getContentResource("blocchieditoriali3").adaptTo(ValueMap.class).get("card1editorial", "false");
                card4editorial = request.getResource().getResourceResolver().getResource(editorialOriginsPath + "/" + departure).adaptTo(Page.class).getContentResource("blocchieditoriali4").adaptTo(ValueMap.class).get("card1editorial", "false");
                list1editorial = request.getResource().getResourceResolver().getResource(editorialOriginsPath + "/" + departure).adaptTo(Page.class).getContentResource("listeeditoriali1").adaptTo(ValueMap.class).get("editorial", "false");
                list2editorial = request.getResource().getResourceResolver().getResource(editorialOriginsPath + "/" + departure).adaptTo(Page.class).getContentResource("listeeditoriali2").adaptTo(ValueMap.class).get("editorial", "false");
                list3editorial = request.getResource().getResourceResolver().getResource(editorialOriginsPath + "/" + departure).adaptTo(Page.class).getContentResource("listeeditoriali3").adaptTo(ValueMap.class).get("editorial", "false");
                list4editorial = request.getResource().getResourceResolver().getResource(editorialOriginsPath + "/" + departure).adaptTo(Page.class).getContentResource("listeeditoriali4").adaptTo(ValueMap.class).get("editorial", "false");
                list5editorial = request.getResource().getResourceResolver().getResource(editorialOriginsPath + "/" + departure).adaptTo(Page.class).getContentResource("listeeditoriali5").adaptTo(ValueMap.class).get("editorial", "false");
                list6editorial = request.getResource().getResourceResolver().getResource(editorialOriginsPath + "/" + departure).adaptTo(Page.class).getContentResource("listeeditoriali6").adaptTo(ValueMap.class).get("editorial", "false");
                list7editorial = request.getResource().getResourceResolver().getResource(editorialOriginsPath + "/" + departure).adaptTo(Page.class).getContentResource("listeeditoriali7").adaptTo(ValueMap.class).get("editorial", "false");
                list8editorial = request.getResource().getResourceResolver().getResource(editorialOriginsPath + "/" + departure).adaptTo(Page.class).getContentResource("listeeditoriali8").adaptTo(ValueMap.class).get("editorial", "false");
                list9editorial = request.getResource().getResourceResolver().getResource(editorialOriginsPath + "/" + departure).adaptTo(Page.class).getContentResource("listeeditoriali9").adaptTo(ValueMap.class).get("editorial", "false");
            } catch (Exception e){
			    card1editorial = "false";
			    card2editorial = "false";
			    card3editorial = "false";
			    card4editorial = "false";
                list1editorial = "false";
                list2editorial = "false";
                list3editorial = "false";
                list4editorial = "false";
                list5editorial = "false";
                list6editorial = "false";
                list7editorial = "false";
                list8editorial = "false";
                list9editorial = "false";
            }
			populateOffersList(destinationsListItaliaOrYoung,
					offersMapItaliaOrYoung, offersListItaliaOrYoung,
					isHomePage, departure, isYoung, tab1AsteriskInPrice,
					destinationsMap, 5, editorialOriginsMap);
			
			offersItaliaOrYoung = 
					new ArrayList<OfferItem>(offersMapItaliaOrYoung.values());
			
			// if !isYoung: populate offers list for International destinations,
			// retrieve image paths and create final lists of offers 
			if (!isYoung) {
				ArrayList<String> destinationsListMondo =
						new ArrayList<String>();
				LinkedHashMap<String, OfferItem> offersMapMondo =
						new LinkedHashMap<String, OfferItem>();
				populateOffersList(destinationsListMondo, offersMapMondo,
						offersListMondo, isHomePage, departure,
						isYoung, tab2AsteriskInPrice, destinationsMap, 5, editorialOriginsMap);
				offersMondo = new ArrayList<OfferItem>(offersMapMondo.values());
				
				ArrayList<String> destinationsListEuropa =
						new ArrayList<String>();
				LinkedHashMap<String, OfferItem> offersMapEuropa =
						new LinkedHashMap<String, OfferItem>();
				populateOffersList(destinationsListEuropa, offersMapEuropa,
						offersListEuropa, isHomePage, departure,
						isYoung, tab2AsteriskInPrice, destinationsMap, 5, editorialOriginsMap);
				offersEuropa = new ArrayList<OfferItem>(offersMapEuropa.values());

				//offersEuropaMondo.addAll(offersEuropa);
                //offersEuropaMondo.addAll(offersMondo);

                for(int k = 0; k < 5; k++){
                    OfferItem e = new OfferItem();
                    OfferItem m = new OfferItem();

                    if(k < offersMondo.size()){
                        m = offersMondo.get(k);
                        offersEuropaMondo.add(m);
                    }

                    if(k < offersEuropa.size()){
                        e = offersEuropa.get(k);
                        offersEuropaMondo.add(e);
                    }
                }
                Collections.sort(offersEuropaMondo);  //offersEuropaMondo
			}
			
			// merge results for HomePage render purpose only
			if (isHomePage) {
				if (offersMondo.size() > 0 || offersEuropa.size() > 0) {
					for (int i = 0; i < offersItaliaOrYoung.size(); i++) {
						offers.add(offersItaliaOrYoung.get(i));
						try {
							
							if(offersEuropa.size() > 0){
								offers.add(offersEuropa.get(i));
							}
							
							if(offersMondo.size()> 0){
								offers.add(offersMondo.get(i));
							}
							
						} catch (IndexOutOfBoundsException e) {
							continue;
						}
					}
				} else {
					offers = offersItaliaOrYoung;
				}
			}
			
		} else {
			logger.debug("[ListSpecialOffersModel] Aereoporto selezionato non "
					+ "valido o assente dalla lista di quelli per i quali e' "
					+ "presente una offerta");

		}
	}
	
	/*
	 * populate airports list
	 */
	private void populateAirportsList (ArrayList<String> airportList,
			String paxType) {
		logger.debug("[ListSpecialOffersModel] populateAirportsList");
		
		//INIZIO - MODIFICA INDRA 18/07/2017 ci è stata fatta richiesta di ripristinare il servizio "retrieveGeoCities".
		//String target = "OFFERS"; //questa riga deve essere decommentata nel richiamare il metodo retrieveGeoCitiesByTarget
		// create geoCities request
		RetrieveGeoCitiesRequest requestCity = new RetrieveGeoCitiesRequest();
		requestCity.setSid(sid);
		requestCity.setTid(tid);
		requestCity.setLanguageCode(languageCode);
		requestCity.setMarketCode(marketCode);
		requestCity.setPaxType(paxType); 
		//requestCity.setTarget(target); //questa riga deve essere decommentata nel richiamare il metodo retrieveGeoCitiesByTarget
				
		//RetrieveGeoCitiesResponse cityResponse = geoSpatialDelegate.retrieveGeoCitiesByTarget(requestCity);
		RetrieveGeoCitiesResponse cityResponse = geoSpatialDelegate.retrieveGeoCities(requestCity);
		//FINE.
		
		// set airportList
		List<CodeDescriptionData> selectCities = cityResponse.getCities();
		for (int i = 0; i < selectCities.size(); i++) {
			airportList.add(selectCities.get(i).getCode());
		}
		
		if(logger.isDebugEnabled())
		logger.debug("[ListSpecialOffersModel] airportList: " + airportList.toString());
	}
	
	/*
	 * populate sorted destination offers lists
	 */
	@SuppressWarnings("unchecked")
	private void populateSortedDestinationOffersLists(
			List<OfferData> offersListItaliaOrYoung,
			List<OfferData> offersListEuropa, List<OfferData> offersListMondo, String departure, String paxType,
			boolean isYoung, boolean italyWorldListsOnly) {
		logger.debug("[ListSpecialOffersModel] populateSortedDestinationOffersLists");
		
		//INIZIO - MODIFICA INDRA 18/07/2017 ci è stata fatta richiesta di ripristinare il servizio "retrieveGeoOffers".
		//String target = "OFFERS"; //questa riga deve essere decommentata nel richiamare il metodo retrieveGeoOffersByTarget
		// create geoOffers request
		RetrieveGeoOffersRequest serviceRequest = new RetrieveGeoOffersRequest();
		serviceRequest.setSid(sid);
		serviceRequest.setTid(tid);
		serviceRequest.setLanguageCode(languageCode); //questa riga deve essere commentata nel richiamare il metodo retrieveGeoOffersByTarget
		serviceRequest.setMarketCode(marketCode);
		serviceRequest.setDepartureAirportCode(departure);
		serviceRequest.setPaxType(paxType);
		//serviceRequest.setTarget(target); //questa riga deve essere decommentata nel richiamare il metodo retrieveGeoOffersByTarget
		//serviceRequest.setMonthDailyPrices(false); //questa riga deve essere decommentata nel richiamare il metodo retrieveGeoOffersByTarget

		
		//RetrieveGeoOffersResponse serviceResponse =	geoSpatialDelegate.retrieveGeoOffersByTarget(serviceRequest);
		RetrieveGeoOffersResponse serviceResponse =	geoSpatialDelegate.retrieveGeoOffers(serviceRequest);
		//FINE.
		
		List<OfferData> geoOffersList = serviceResponse.getOffers();
		
		List<OfferData> offerDataList = null;
		try {
			offerDataList = (List<OfferData>) AlitaliaCommonUtils.deepClone(geoOffersList);
		} catch (IOException e) {
			logger.error("Error cloning object", e);
		}
		
		if (offerDataList != null) {
			logger.info("[ListSpecialOffersModel] number of offers: " 
				+ "Departure [" + departure + "], [" + offerDataList.size() + "]");
		}
		
		
		// populate specific lists
		if (!isYoung) {
			for (OfferData offerData : offerDataList) {
				if (offerData.getArea() == AreaValueEnum.DOM) {
					offersListItaliaOrYoung.add(offerData);
				} else if (offerData.getArea() == AreaValueEnum.INTZ){
					offersListEuropa.add(offerData);


//					TODO aggiungere offerData anche a lista mondo, solo se e' stato passato il parametro delle due colonne
//					TODO farlo passare tra i parametri fino a questo metodo
					/*if(italyWorldListsOnly) {
						offersListMondo.add(offerData);
					}*/
					
				} else {
					offersListMondo.add(offerData);
				}
			}
		} else {
			offersListItaliaOrYoung.addAll(offerDataList);
		}
		
		// sort lists ASC by priority
		Collections.sort(offersListItaliaOrYoung);
		Collections.sort(offersListEuropa);
		if (!isYoung) {
			Collections.sort(offersListMondo);
		}
	}
	
	/*
	 * populate generic offers list
	 */
	private void populateOffersList(ArrayList<String> destionationsList,
			LinkedHashMap<String, OfferItem> offersMap, List<OfferData> offersList,
			boolean isHomePage, String departure, boolean isYoung,
			boolean asteriskInPrice, Map<String, String> destinationsMap, int i, Map<String, String> editorialOriginsMap) {
		logger.debug("[ListSpecialOffersModel] populateOffersList");
		
		// cycle on offers
		int counterHp = 0, counter = 0;
		
		int maxNumOffersHP = 0;
		maxNumOffersHP = i;
		 	
		for (OfferData offerData : offersList) {

			//if ((isHomePage && (counterHp < AlitaliaConstants.MAX_NUM_OFFERS_HP))
			if ((isHomePage && (counterHp < maxNumOffersHP))
					|| !isHomePage || offerData.isHomePageOffer()) {
				if(logger.isDebugEnabled())
				logger.debug("[ListSpecialOffersModel] offer2 #" + counter 
						+ ": [" + offerData.toString() + "]");
				
				Calendar expireDate = offerData.getExpireOfferData();
				Calendar now = Calendar.getInstance();

				// expireDate check
				if (expireDate != null && expireDate.after(now)) {

					OfferItem offerItem = new OfferItem();
					String airportCode = offerData.getArrivalAirport().getCode();
					String destinationCode = I18nKeyCommon.AIRPORT_PREFIX
							+ airportCode + I18nKeyCommon.AIRPORT_CITY;
					//String destination = i18n.get(destinationCode);
					
					// offerItem.setDestinazione(destination.equals(destinationCode)
					// ? airportCode : destination);
					offerItem.setDestinazione(destinationCode);
					// String price = offerData.getBestPrice().toString();
                    Integer priority = offerData.getPriority();

					double price = offerData.getBestPrice().doubleValue();
					//per alcuni mercati è necessario aggiungere una extraFee al prezzo
					price += AlitaliaUtils.getExtraFee(alitaliaConfigurationHolder, marketCode, searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(departure, airportCode), searchFlightsDelegate.isSliceSicilyTerritorialContinuity(departure, airportCode));

					String prezzo = AlitaliaUtils.round_new(price, request.getResource());
					if (asteriskInPrice) {
						offerItem.setPrezzoDaMostrare(prezzo + "*");
					}
					else{
						offerItem.setPrezzoDaMostrare(prezzo);
					}
					offerItem.setPrezzo(NumberFormat.getInstance(Locale.ITALIAN).format(price));

					offerItem.setPriority(priority);

					// if (price != null) {
						// int index = price.lastIndexOf(".");
						// offerItem.setPrezzo(price.substring(0, index));
					// } else {
						// offerItem.setPrezzo(price);
					// }

					// FIXME ?? [listSpecialOffersModel] translate label
					// ITALIA/MONDO
					
					// OLD
					// String area = AreaValueEnum.DOM.equals(offerData.getArrivalAirport().getArea())	? AlitaliaConstants.OFFER_AREA_ITALIA : AlitaliaConstants.OFFER_AREA_MONDO;
					
					// NEW
					String area = "";
					
					if(AreaValueEnum.DOM.equals(offerData.getArrivalAirport().getArea())){
						area = AlitaliaConstants.OFFER_AREA_ITALIA;
					}else if(AreaValueEnum.INTZ.equals(offerData.getArrivalAirport().getArea())){
						area = AlitaliaConstants.OFFER_AREA_EUROPA;
					}else{
						area =  AlitaliaConstants.OFFER_AREA_MONDO;
					}
					
					offerItem.setArea(area);

					String link = AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), true) 
							+ alitaliaConfigurationHolder.getOfferDetailPageWithExtension();
					String linkParam = "?from=" + departure;
					linkParam += "&to=" + airportCode;
					if (isYoung) {
						linkParam += "&isYoung=" + isYoung;
					}
					offerItem.setLink(link + linkParam);
					logger.debug("Offer link: " + link + linkParam);
					
					if (RouteTypeEnum.RETURN.equals(offerData.getRouteType())) {
						offerItem.setTipo(I18nKeyCommon.LABEL_AR);
					} else {
						offerItem.setTipo(I18nKeyCommon.LABEL_SOLO_ANDATA);
					}

					
					if (destinationsMap.containsKey(airportCode)) {
						offerItem.setPathImmagine(destinationsMap.get(airportCode));
					} else {
						offerItem.setPathImmagine(destinationsMap.get("default"));
					}


					// scrive in ogni offerta i path delle 4 offerte editoriali per quell'aeroporto di origine, sarebbe da scrivere nell'oggetto padre
					if (editorialOriginsMap.containsKey(offerData.getDepartureAirport().getCode())) {
					    offerItem.setEditorialResource("true");
					    offerItem.setEditorialResource1(editorialOriginsMap.get(offerData.getDepartureAirport().getCode()) + "blocchieditoriali1");
					    offerItem.setEditorialResource2(editorialOriginsMap.get(offerData.getDepartureAirport().getCode()) + "blocchieditoriali2");
					    offerItem.setEditorialResource3(editorialOriginsMap.get(offerData.getDepartureAirport().getCode()) + "blocchieditoriali3");
					    offerItem.setEditorialResource4(editorialOriginsMap.get(offerData.getDepartureAirport().getCode()) + "blocchieditoriali4");
					    offerItem.setEditorialResource5(editorialOriginsMap.get(offerData.getDepartureAirport().getCode()) + "listeeditoriali1");
					    offerItem.setEditorialResource6(editorialOriginsMap.get(offerData.getDepartureAirport().getCode()) + "listeeditoriali2");
					    offerItem.setEditorialResource7(editorialOriginsMap.get(offerData.getDepartureAirport().getCode()) + "listeeditoriali3");
					    offerItem.setEditorialResource8(editorialOriginsMap.get(offerData.getDepartureAirport().getCode()) + "listeeditoriali4");
					    offerItem.setEditorialResource9(editorialOriginsMap.get(offerData.getDepartureAirport().getCode()) + "listeeditoriali5");
					    offerItem.setEditorialResource10(editorialOriginsMap.get(offerData.getDepartureAirport().getCode()) + "listeeditoriali6");
					    offerItem.setEditorialResource11(editorialOriginsMap.get(offerData.getDepartureAirport().getCode()) + "listeeditoriali7");
					    offerItem.setEditorialResource12(editorialOriginsMap.get(offerData.getDepartureAirport().getCode()) + "listeeditoriali8");
					    offerItem.setEditorialResource13(editorialOriginsMap.get(offerData.getDepartureAirport().getCode()) + "listeeditoriali9");
                    }

                    try {
					    offerItem.setSpanText(request.getResource().getResourceResolver().getResource(destinationsMap.get(airportCode)).adaptTo(ValueMap.class).get("spanText", ""));
                    } catch (Exception e) {
					    offerItem.setSpanText("");
                    }



					if("it".equalsIgnoreCase(marketCode)){
						if ((AlitaliaConstants.OFFER_AREA_MONDO.equals(offerItem.getArea())
								&& RouteTypeEnum.RETURN.equals(offerData.getRouteType()))
								|| (AlitaliaConstants.OFFER_AREA_EUROPA.equals(offerItem.getArea())
								&& RouteTypeEnum.RETURN.equals(offerData.getRouteType()))
								|| (AlitaliaConstants.OFFER_AREA_ITALIA.equals(offerItem.getArea())
								&& !RouteTypeEnum.RETURN.equals(offerData.getRouteType()))) {
							
							offersMap.put(offerItem.getDestinazione(), offerItem);
							destionationsList.add(offerItem.getDestinazione());
							if (isHomePage) {
								counterHp++;
							} else {
								counter++;
							}
						}
					} else{
						offersMap.put(offerItem.getDestinazione(), offerItem);
						destionationsList.add(offerItem.getDestinazione());
						if (isHomePage) {
							counterHp++;
						} else {
							counter++;
						}
					}
				} else {
					logger.debug("[ListSpecialOffersModel] expired offer");
				}
			}
		}
		logger.debug("[ListSpecialOffersModel] selected offers: "
				+ offersMap.size() + ": [" + offersMap.toString() + "]");
	}

	public ArrayList<OfferItem> getOffers() {
		return offers;
	}
	
	public ArrayList<OfferItem> getOffersItaliaOrYoung() {
		return offersItaliaOrYoung;
	}

	public ArrayList<OfferItem> getOffersMondo() {
		return offersMondo;
	}
	
	public boolean isInvertPrice() {
		return invertPrice;
	}

	public void setInvertPrice(boolean invertPrice) {
		this.invertPrice = invertPrice;
	}

	public String getPriceLabel() {
		return priceLabel;
	}

	public String getButtonLabel() {
		return buttonLabel;
	}

	public ArrayList<OfferItem> getOffersEuropa() {
		return offersEuropa;
	}

    public ArrayList<OfferItem> getOffersEuropaMondo() {
        return offersEuropaMondo;
    }





    public String isCard1editorial() {
        return card1editorial;
    }

    public void setCard1editorial(String card1editorial) {
        this.card1editorial = card1editorial;
    }

    public String isCard2editorial() {
        return card2editorial;
    }

    public void setCard2editorial(String card2editorial) {
        this.card2editorial = card2editorial;
    }

    public String isCard3editorial() {
        return card3editorial;
    }

    public void setCard3editorial(String card3editorial) {
        this.card3editorial = card3editorial;
    }

    public String isCard4editorial() {
        return card4editorial;
    }

    public void setCard4editorial(String card4editorial) {
        this.card4editorial = card4editorial;
    }

    public String getList1editorial() {
        return list1editorial;
    }

    public void setList1editorial(String list1editorial) {
        this.list1editorial = list1editorial;
    }

    public String getList2editorial() {
        return list2editorial;
    }

    public void setList2editorial(String list2editorial) {
        this.list2editorial = list2editorial;
    }

    public String getList3editorial() {
        return list3editorial;
    }

    public void setList3editorial(String list3editorial) {
        this.list3editorial = list3editorial;
    }

    public String getList4editorial() {
        return list4editorial;
    }

    public void setList4editorial(String list4editorial) {
        this.list4editorial = list4editorial;
    }

    public String getList5editorial() {
        return list5editorial;
    }

    public void setList5editorial(String list5editorial) {
        this.list5editorial = list5editorial;
    }

    public String getList6editorial() {
        return list6editorial;
    }

    public void setList6editorial(String list6editorial) {
        this.list6editorial = list6editorial;
    }

    public String getList7editorial() {
        return list7editorial;
    }

    public void setList7editorial(String list7editorial) {
        this.list7editorial = list7editorial;
    }

    public String getList8editorial() {
        return list8editorial;
    }

    public void setList8editorial(String list8editorial) {
        this.list8editorial = list8editorial;
    }

    public String getList9editorial() {
        return list9editorial;
    }

    public void setList9editorial(String list9editorial) {
        this.list9editorial = list9editorial;
    }
}
