package com.alitalia.aem.consumer.mmb.render;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPassengerData;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup.AncillariesGroupType;
import com.alitalia.aem.consumer.utils.Lists;

public class MmbThankyouAnalyticsRender {
	private int emdQuantity;
	private String products;
	private String quantities;
	private String prices;
	private String invoiceNumber;
	private String purchaseDate;
	private String purchaseTime;
	private String currency;
	
	

	public MmbThankyouAnalyticsRender(MmbSessionContext ctx) {
		
		List<MmbAncillariesGroup> groupsByPassenger = MmbAncillariesGroup.createGroups(
				AncillariesGroupType.BY_PASSENGER, ctx.route, ctx.cart.getPassengers(), 
				ctx.cart.getAncillaries(), null);
		MmbAnalyticsAncillaryTypeMap ancPurchasedMap = 
				new MmbAnalyticsAncillaryTypeMap();
		
		for (MmbAncillariesGroup passengerGroup : groupsByPassenger) {
			List<MmbAncillaryData> purchased = 
					passengerGroup.getAncillariesByStatus(MmbAncillaryStatusEnum.ISSUED);
			for(MmbAncillaryData ancillary : purchased){
				ancPurchasedMap.registerPurchasedAncillary(ancillary);
			}
			
		}
		if(ctx.insurancePaymentSuccess){
			MmbAncillaryData insurance = new MmbAncillaryData();
			insurance.setAmount(ctx.insurancePolicy.getTotalInsuranceCost());
			insurance.setAncillaryType(MmbAncillaryTypeEnum.INSURANCE);
			insurance.setQuantity(1);
			ancPurchasedMap.registerPurchasedAncillary(insurance);
		}
		
		this.products = "";
		this.quantities = "";
		this.prices = "";
		this.emdQuantity = 0;
		this.invoiceNumber = "";
		this.purchaseDate = "";
		this.purchaseTime = "";
		this.currency = "";
		if(ancPurchasedMap.getAncTypes().length > 0) {
			/*Ancillary types*/
			for(String type : ancPurchasedMap.getAncTypes()){
				products += type + ";";
			}
			if(!products.isEmpty()){
				/*remove last ";"*/
				products = products.substring(0, products.length()-1);
			}
			/*Ancillary quantities and prices*/
			for(MmbAncillaryTypeEnum type : MmbAncillaryTypeEnum.values()){
				List<MmbAnalyticsAncillaryInfo> ancs =
						ancPurchasedMap.getAncillaryByType(type);
				if(ancs.size() > 0){
					quantities +=  ancs.size() + ";";
					if(!type.equals(MmbAncillaryTypeEnum.INSURANCE))
						this.emdQuantity += ancs.size();
					BigDecimal totalPrice = BigDecimal.ZERO;
					for(MmbAnalyticsAncillaryInfo anc : ancs){
						totalPrice = totalPrice.add(anc.price);
					}
					prices +=  totalPrice + ";";
				}
			}
			if(!quantities.isEmpty()){
				/*remove last ";"*/
				quantities = quantities.substring(0, quantities.length()-1);
			}
			if(!prices.isEmpty())
				prices = prices.substring(0, prices.length()-1);
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat timeFormat = new SimpleDateFormat("kk:mm:ss");
			this.purchaseDate = dateFormat.format(Calendar.getInstance(
					TimeZone.getTimeZone("Europe/Rome")).getTime());
			this.purchaseTime = timeFormat.format(Calendar.getInstance(
					TimeZone.getTimeZone("Europe/Rome")).getTime());
			this.invoiceNumber = String.valueOf(ctx.cart.getId());
		}
		
		if(ctx.currencyCode != null){
			this.currency = ctx.currencyCode;
		}
		
			
	}

	public int getEmdQuantity() {
		return emdQuantity;
	}

	public void setEmdQuantity(int emdQuantity) {
		this.emdQuantity = emdQuantity;
	}

	public String getProducts() {
		return products;
	}

	public void setProducts(String products) {
		this.products = products;
	}

	public String getQuantities() {
		return quantities;
	}

	public void setQuantities(String quantities) {
		this.quantities = quantities;
	}

	public String getPrices() {
		return prices;
	}

	public void setPrices(String prices) {
		this.prices = prices;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getPurchaseTime() {
		return purchaseTime;
	}

	public void setPurchaseTime(String purchaseTime) {
		this.purchaseTime = purchaseTime;
	}
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	private class MmbAnalyticsAncillaryTypeMap{
		private Map<String, List<MmbAnalyticsAncillaryInfo>> ancillaryMap = 
				new HashMap<String,List<MmbAnalyticsAncillaryInfo>>();
		
		Boolean isAncillaryPurchased(MmbAncillaryTypeEnum ancType){
			return !ancillaryMap.getOrDefault(toAncTypeToString(ancType),Collections.EMPTY_LIST)
					.isEmpty();
		}
		
		void registerPurchasedAncillary(MmbAncillaryData ancillary){
			List<MmbAnalyticsAncillaryInfo> list = 
					ancillaryMap.getOrDefault(toAncTypeToString(ancillary.getAncillaryType())
					,new LinkedList<MmbAnalyticsAncillaryInfo>());
			if(list.size() == 0){
				ancillaryMap.put(toAncTypeToString(ancillary.getAncillaryType()), list);
			}
			list.add(new MmbAnalyticsAncillaryInfo(ancillary.getAmount()
							, ancillary.getEmd(), ancillary.getAncillaryType()));
		}
		
		private String toAncTypeToString(MmbAncillaryTypeEnum ancType){
			String ancString = "";
			if(ancType != null){
				switch(ancType){
					case SEAT:
						ancString = "posto_confort";
						break;
					case EXTRA_BAGGAGE:
						ancString = "bagaglio_extra";
						break;
					case MEAL:
						ancString = "menu_preferito";
						break;
					case FAST_TRACK:
						ancString = "fasttrack";
						break;
					case VIP_LOUNGE:
						ancString = "sala_lounge";
						break;
					case INSURANCE:
						ancString = "assicura_viaggio";
						break;
				}
			}
			return ancString;
		}
		
		String[] getAncTypes(){
			return ancillaryMap.keySet().toArray(new String[0]);
		}
		
		List<MmbAnalyticsAncillaryInfo> getAncillaryByType(MmbAncillaryTypeEnum type){
			return ancillaryMap.getOrDefault(toAncTypeToString(type)
					,new LinkedList<MmbAnalyticsAncillaryInfo>());
		}
		
	}
	
	private class MmbAnalyticsAncillaryInfo{
		BigDecimal price;
		String emd;
		MmbAncillaryTypeEnum type;
		
		MmbAnalyticsAncillaryInfo(BigDecimal price, String emd,	MmbAncillaryTypeEnum type){
			this.price = price;
			this.emd = emd;
			this.type = type;
		}
		
	}
}
