package com.alitalia.aem.consumer.checkinrest.jobs.consumer;

/**
 * Created by ggadaleta on 01/02/2018.
 */

import com.alitalia.aem.common.messages.checkinrest.*;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.checkinrest.jobs.CheckinJobConstants;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.alitalia.aem.web.component.webcheckin.delegaterest.IInsuranceDelegate;
import com.google.gson.Gson;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@Component(
    label = "Alitalia CheckinRestAPI - Clear Ancillaries Job Consumer",
    description = "Call Rest API (BL) for Clear Ancillaries",

    // One of the few cases where immediate = true; this is so the Event Listener starts listening immediately
    immediate = true
)
@Properties({
    @Property(
        label = "Alitalia CheckinRestAPI - Clear Ancillaries Job Consumer",
        value = CheckinJobConstants.TOPIC_ANCILLARIES_CLEAR,
        description = "Call Rest API (BL) for Clear Ancillaries",
        name = JobConsumer.PROPERTY_TOPICS,
        propertyPrivate = true
    )
})
@Service(value={JobConsumer.class})
public class ClearAncillariesJob implements JobConsumer {

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile ICheckinDelegate checkInDelegateRest;

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile IInsuranceDelegate insuranceDelegateRest;

    private static final Logger logger = LoggerFactory.getLogger(OffloadPassengerJob.class);

    final public static String PROPERTY_CHECKIN_SESSION_ID = "checkinrest-sessionid";

    final public static String PROPERTY_CHECKIN_CLEARANCILLARIES = "checkinrest-clearancillaries";

    final public static String PROPERTY_CHECKIN_CLEARINSURANCE = "checkinrest-clearinsurance";

    public JobResult process(final Job job) {
        // process the job and return the result
        String sessionID = job.getProperty(PROPERTY_CHECKIN_SESSION_ID).toString();
        logger.debug("[{}] Creating Clear Ancillaries Request",sessionID);

        Gson gson = new Gson();

        List<CheckinClearAncillarySessionEndRequest> requestList = new ArrayList<>();
        List<DeleteInsuranceRequest> deleteInsuranceRequestList  = new ArrayList<>();

        for(String propertyItemName : job.getPropertyNames()) {
            if(propertyItemName.startsWith(PROPERTY_CHECKIN_CLEARANCILLARIES) ||
                propertyItemName.startsWith(PROPERTY_CHECKIN_CLEARINSURANCE)) {

                String jsonProperty = job.getProperty(propertyItemName).toString();

                try {
                    if(propertyItemName.startsWith(PROPERTY_CHECKIN_CLEARANCILLARIES)) {
                        CheckinClearAncillarySessionEndRequest reqItem = gson.fromJson(jsonProperty, CheckinClearAncillarySessionEndRequest.class);
                        requestList.add(reqItem);
                    } else if(propertyItemName.startsWith(PROPERTY_CHECKIN_CLEARINSURANCE)) {
                        DeleteInsuranceRequest reqItem = gson.fromJson(jsonProperty, DeleteInsuranceRequest.class);
                        deleteInsuranceRequestList.add(reqItem);
                    }
                } catch(Exception e) {
                    logger.error("[{}] cannot deserialize request {} " + e.toString(), sessionID,jsonProperty);
                }
            }
        }

        logger.debug("[{}] Calling service rest clearAncillaySessionEnd",sessionID);
        if(requestList.size() > 0) {
            Boolean status = true;
            //TODO: gestione errore 500 null
            for (CheckinClearAncillarySessionEndRequest request : requestList) {
                request.setSid(sessionID);
                request.setTid(IDFactory.getTid());
                CheckinClearAncillarySessionEndResponse checkinClearAncillarySessionEndResponse = checkInDelegateRest.clearAncillaySessionEnd(request);
                if (checkinClearAncillarySessionEndResponse.getOutcome().get(0) != null) {
                    if (!checkinClearAncillarySessionEndResponse.getOutcome().get(0).equals("OK")) {
                        logger.warn("[{}] Si è verificato un errore nel clear ancillary e session end {}", sessionID, checkinClearAncillarySessionEndResponse.getOutcome().get(0));
                        status = status && false;
                    } else {
                        logger.info("[{}] Ancillay e sessione ripulite correttamente. Outcome: {}", sessionID, checkinClearAncillarySessionEndResponse.getOutcome().get(0));
                        status = status && true;
                    }
                }
            }

            if(deleteInsuranceRequestList.size() > 0) {
                for (DeleteInsuranceRequest request : deleteInsuranceRequestList) {
                    request.setSid(sessionID);
                    request.setTid(IDFactory.getTid());

                    DeleteInsuranceResponse deleteInsuranceResponse = insuranceDelegateRest.deleteInsurance(request);
                    if ("OK".equals(deleteInsuranceResponse.getSuccess())) {
                        logger.info("[{}] Clear insurance eseguito per {}", sessionID, request.getPnr());
                        status = status && true;
                    } else {
                        logger.error("[" + sessionID + "] ERRORE Clear insurance  per {}", gson.toJson(request), gson.toJson(deleteInsuranceResponse));
                        status = status && false;
                    }
                }
            }

            return status ? JobResult.OK : JobResult.CANCEL;
            //return JobResult.FAILED;
        } else {
            logger.warn("[{}] Clear Ancillaries nothing to do", sessionID);
            return JobResult.CANCEL;
        }
    }
}