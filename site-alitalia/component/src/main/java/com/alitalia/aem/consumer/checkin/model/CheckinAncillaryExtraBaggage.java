package com.alitalia.aem.consumer.checkin.model;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.CheckinPassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLinkTypeEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.checkin.render.CheckinAncillaryExtraBaggagePassengerRender;
import com.alitalia.aem.consumer.checkin.render.CheckinAncillaryExtraBaggageRender;
import com.alitalia.aem.consumer.checkin.render.CheckinFlightRender;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;
import com.alitalia.aem.consumer.utils.Lists;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinAncillaryExtraBaggage extends CheckinSessionAncillaryGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private CheckinSession checkinSession;
	
	private boolean allowClearCart = false;
	private boolean isMultileg = false;
	private boolean isOutbound = false;
	private List<CheckinAncillaryExtraBaggagePassengerRender> extraBaggagePassengers;
	private List<CheckinFlightRender> flightsList;
	private int baggageQuantityForAllPassenger;
	private boolean displayFeedbackStep = false;
	private int purchasableExtraBaggageNum;
	private int selectedExtraBaggageNum;
	private boolean showBoxExtraBaggage;
	
	
	@PostConstruct
	protected void initModel() throws IOException {
		
		try {
			initBaseModel(request);
			List<CheckinFlightData> flights = CheckinUtils.getFlightsList(ctx.selectedRoute); 
			flightsList = new ArrayList<CheckinFlightRender>();
			for (CheckinFlightData flight : flights) {
				CheckinFlightRender checkinFlightRender = new CheckinFlightRender(flight,ctx);
				flightsList.add(checkinFlightRender);
			}
			this.allowClearCart = false;
			this.extraBaggagePassengers = new ArrayList<CheckinAncillaryExtraBaggagePassengerRender>();
			this.baggageQuantityForAllPassenger = 0;
			this.purchasableExtraBaggageNum = 0;
			this.selectedExtraBaggageNum = 0;
			this.displayFeedbackStep = Boolean.parseBoolean(getRequest().getParameter("displayfeedbackstep"));
			this.isMultileg = CheckinUtils.isMultileg(ctx.selectedRoute);
			this.isOutbound = CheckinUtils.isOutbound(ctx.selectedRoute);
			List<MmbAncillaryStatusEnum> statusList = new ArrayList<MmbAncillaryStatusEnum>();
			statusList.add(MmbAncillaryStatusEnum.AVAILABLE);
			statusList.add(MmbAncillaryStatusEnum.TO_BE_ISSUED);
			statusList.add(MmbAncillaryStatusEnum.PREVIOUSLY_ISSUED);
			statusList.add(MmbAncillaryStatusEnum.ISSUED);
			int baggageForPassengernNotFound = 0;
			List<CheckinPassengerData> checkedInPassengers = CheckinUtils.getCheckedInPassengers(ctx.passengers);
			for (CheckinPassengerData passenger : checkedInPassengers) {
				
				CheckinAncillaryExtraBaggagePassengerRender extraBaggagePassenger = 
						new CheckinAncillaryExtraBaggagePassengerRender();
				extraBaggagePassenger.setFullName(
						passenger.getName() + " " + passenger.getLastName());
				if (MmbLinkTypeEnum.INFANT_ACCOMPANIST.equals(passenger.getLinkType())) {
					extraBaggagePassenger.setHasInfant(true);
				}
				if (CheckinPassengerTypeEnum.CHILD.equals(passenger.getType())) {
					extraBaggagePassenger.setChild(true);
				}
				extraBaggagePassenger.setTotalToBeIssuedPrice(
						new MmbPriceRender(BigDecimal.valueOf(0), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				extraBaggagePassenger.setDisplayInFeedback(false);
				extraBaggagePassenger.setExtraBaggageItemList(null);
				
				List<MmbAncillaryData> ancillaryBaggagePassenger = 
						checkinSession.getAncillaryByTypeStatusAndETicket(ctx, MmbAncillaryTypeEnum.EXTRA_BAGGAGE, statusList,
								passenger.getEticket());
				
				if (ancillaryBaggagePassenger == null || ancillaryBaggagePassenger.isEmpty()) {
					baggageForPassengernNotFound++;
					CheckinAncillaryExtraBaggageRender extraBaggage = 
							new CheckinAncillaryExtraBaggageRender(flights,ctx);
					extraBaggage.setRouteTypeLabel(getRouteTypeLabel(ctx.selectedRoute));
					extraBaggage.setFirstFlight(new  CheckinFlightRender(Lists.getFirst(flights), ctx));
					extraBaggage.setLastFlight(new  CheckinFlightRender(Lists.getLast(flights), ctx));
					extraBaggage.setBaggageAllowNumber(passenger.getBaggageAllowanceQuantity());
					extraBaggage.setBaggageWeight(Integer.valueOf(23));
					extraBaggage.setIdentifier("");
					extraBaggage.setCurrentValue(false);
					extraBaggage.setAllowModify(false);
					extraBaggage.setPriceRender(null);
					this.baggageQuantityForAllPassenger++;
					extraBaggagePassenger.getExtraBaggageItemList().add(extraBaggage);
					logger.warn("No baggage ancillary found in cart for passenger " + extraBaggagePassenger.getFullName() + " in PNR " + ctx.pnr);
				}
				
				for (MmbAncillaryData ancillary : ancillaryBaggagePassenger) {
					extraBaggagePassenger.setExtraBaggageItemList(new ArrayList<CheckinAncillaryExtraBaggageRender>());
					
					//Crea ExtraBaggage
					CheckinAncillaryExtraBaggageRender extraBaggage = 
							new CheckinAncillaryExtraBaggageRender(flights,ctx);
					
					extraBaggage.setRouteTypeLabel(getRouteTypeLabel(ctx.selectedRoute));
					extraBaggage.setFirstFlight(new  CheckinFlightRender(Lists.getFirst(flights), ctx));
					extraBaggage.setLastFlight(new  CheckinFlightRender(Lists.getLast(flights), ctx));
					extraBaggage.setBaggageAllowNumber(passenger.getBaggageAllowanceQuantity());
					//FIXME capire se al passeggero posso dare un bagaglio in più
					/*if ("2".equals(passengerGroup.findMmbPassengerDataForFlight(0).getFrequentFlyerCustomerValue())) {
						extraBaggage.setBaggageAllowNumber(extraBaggage.getBaggageAllowNumber() + 1);
					}*/
					extraBaggage.setBaggageWeight(Integer.valueOf(23));
					
					if (ancillary.getAncillaryStatus() == MmbAncillaryStatusEnum.AVAILABLE) {
						// ancillaries available
						extraBaggage.setIdentifier(Integer.toString(ancillary.getId()));
						extraBaggage.setCurrentValue(false);
						extraBaggage.setPriceRender(
								checkinSession.getAncillaryDisplayedPriceInfo(ctx, ancillary, 1));
						extraBaggage.setAllowModify(true);
						purchasableExtraBaggageNum++;
								
					} else if (ancillary.getAncillaryStatus() == MmbAncillaryStatusEnum.TO_BE_ISSUED) {
						// ancillaryies added to cart
						extraBaggage.setIdentifier(Integer.toString(ancillary.getId()));
						extraBaggage.setCurrentValue(true);
						extraBaggage.setPriceRender(
								checkinSession.getAncillaryDisplayedPriceInfo(ctx, ancillary, null));
						extraBaggage.setAllowModify(true);
						this.baggageQuantityForAllPassenger++;
						this.allowClearCart = true;
						extraBaggagePassenger.setDisplayInFeedback(true);
						extraBaggagePassenger.setTotalToBeIssuedQuantity(
								extraBaggagePassenger.getTotalToBeIssuedQuantity() + 1);
						extraBaggagePassenger.getTotalToBeIssuedPrice().increaseAmount(
								extraBaggage.getPriceRender().getAmount());
						extraBaggagePassenger.increaseToBeIssuedQuantityForBaggageWeight(
								extraBaggage.getBaggageWeight());
						selectedExtraBaggageNum++;
						
					} else if (ancillary.getAncillaryStatus() == MmbAncillaryStatusEnum.ISSUED
							|| ancillary.getAncillaryStatus() == MmbAncillaryStatusEnum.PENDING
							|| ancillary.getAncillaryStatus() == MmbAncillaryStatusEnum.PREVIOUSLY_ISSUED) {
						// ancillary already issued
						extraBaggage.setIdentifier(Integer.toString(ancillary.getId()));
						extraBaggage.setCurrentValue(true);
						extraBaggage.setAllowModify(false);
						extraBaggage.setPriceRender(null);
						this.baggageQuantityForAllPassenger++;
					}
					
					extraBaggagePassenger.getExtraBaggageItemList().add(extraBaggage);
				}
				
				extraBaggagePassengers.add(extraBaggagePassenger);
			}
			
			showBoxExtraBaggage = true;
			if (baggageForPassengernNotFound == checkedInPassengers.size()) {
				showBoxExtraBaggage = false;
			}
		} catch (Exception e) {
			logger.error("unexpected error: ", e);
			throw e;
		}
		
	}
	
	public List<CheckinAncillaryExtraBaggagePassengerRender> getExtraBaggagePassengers() {
		return extraBaggagePassengers;
	}

	public List<CheckinFlightRender> getFlightsList() {
		return flightsList;
	}
	
	public boolean isAllowClearCart() {
		return allowClearCart;
	}

	public int getBaggageQuantityForAllPassenger() {
		return baggageQuantityForAllPassenger;
	}

	public int getPurchasableExtraBaggageNum() {
		return purchasableExtraBaggageNum;
	}

	public int getSelectedExtraBaggageNum() {
		return selectedExtraBaggageNum;
	}
	
	public boolean isDisplayFeedbackStep() {
		return displayFeedbackStep;
	}
	
	public boolean isMultileg() {
		return isMultileg;
	}
	
	public boolean isOutbound() {
		return isOutbound;
	}
	
	public boolean getShowBoxExtraBaggage() {
		return showBoxExtraBaggage;
	}
	
}
