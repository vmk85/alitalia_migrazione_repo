package com.alitalia.aem.consumer.utils;


import java.util.ArrayList;

public class Country {

    private boolean selected;
    private String name;
    private String link;
    private String code;
    private ArrayList<Language> languages;

    public Country(String name, String link, String code) {
        setName(name);
        setLink(link);
        setCode(code);
        setSelected(false);  
        languages = new ArrayList<Language>();
    }

    public Country(String name, String link, String code, boolean selected) {
        setName(name);
        setLink(link);
        setCode(code);
        setSelected(selected);
        languages = new ArrayList<Language>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public void setLanguages(ArrayList<Language> languages) {
        this.languages = languages;
    }

    public void addLanguage(Language language) {
        languages.add(language);
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    
    }
    
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

    public boolean isSelected() {
        return selected;
    }

    public ArrayList<Language> getLanguages() {
        return languages;
    }

    public String toJson() {
        String jsonString = "{";

        jsonString += "name: \"" + name + "\", ";
        jsonString += "link: \"" + link + "\", ";
        jsonString += "code: \"" + code + "\", "; 
        jsonString += "languages: [" + getLanguagesJson() + "]";

        jsonString += "}";

        return jsonString;
    }

    private String getLanguagesJson() {
        String jsonString = "";

        for(Language language : languages) {
        	if (jsonString.length() > 0) {
        		jsonString += ",";
        	}
            jsonString += language.toJson();
        }

        return jsonString;
    }



}
