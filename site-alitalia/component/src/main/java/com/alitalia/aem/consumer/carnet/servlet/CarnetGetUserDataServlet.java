package com.alitalia.aem.consumer.carnet.servlet;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.carnet.CarnetData;
import com.alitalia.aem.common.data.home.carnet.CarnetInfoCarnet;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.carnet.CarnetConstants;
import com.alitalia.aem.consumer.carnet.CarnetSession;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.alitalia.aem.consumer.carnet.render.CarnetDateRender;
import com.alitalia.aem.consumer.carnet.render.CarnetGenericPriceRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.web.component.carnet.delegate.CarnetDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "userdatacarnet" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CarnetGetUserDataServlet extends GenericCarnetFormValidatorServlet {

	@Reference
	private volatile AlitaliaConfigurationHolder configuration;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
		
		// content type
		response.setContentType("application/json");
				
		// JSON response
		TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());
				
		// Recupero la ctx
		CarnetSessionContext ctx = isUserLogged(request); 
		
		//MOCK DATA
		/*CarnetSessionContext ctx = new CarnetSessionContext();
		ctx.infoCarnet = new CarnetInfoCarnet();//
		ctx.infoCarnet.setBuyerLastname("Micheli");
		ctx.infoCarnet.setBuyerName("Stefano");
		ctx.infoCarnet.setResidualRoutes(((short) 2));
		Calendar now = Calendar.getInstance();
		ctx.infoCarnet.setExpiryDate(now);*/
		
		CarnetInfoCarnet infoCarnetuser = ctx.infoCarnet;
		String user = infoCarnetuser.getBuyerName() + " " + infoCarnetuser.getBuyerLastname();
		String numCarnetResidui = infoCarnetuser.getResidualRoutes() + "";
		Calendar date = infoCarnetuser.getExpiryDate();
		
		CarnetDateRender dateRender = new CarnetDateRender(date, ctx);
		
		String scadenzaCarnet = dateRender.getTextualDate();
		
		logger.debug("Chiamo getPrice carnet");
		
		jsonOutput.object();
			jsonOutput.key("userData");
			jsonOutput.array();
		
				jsonOutput.object();
					jsonOutput.key("result").value(true);
				jsonOutput.endObject();
				jsonOutput.object();
					jsonOutput.key("user");
					jsonOutput.object();
						jsonOutput.key("utente").value(user);
						jsonOutput.key("numeroVoli").value(numCarnetResidui);
						jsonOutput.key("scadenzaCarnet").value(scadenzaCarnet);
					jsonOutput.endObject();
				jsonOutput.endObject();
			jsonOutput.endArray();
		jsonOutput.endObject();
		
		logger.debug("JSON del servizio: " + jsonOutput.toString());
					
		
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}
