package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.enumeration.SearchExecuteResponseType;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingmetasearch" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class BookingMetaSearchServlet extends GenericBookingFormValidatorServlet {

	// StaticData delegate
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegate staticDataDelegate;
	
	// SearchFlight delegate
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile SearchFlightsDelegate searchFlightsDelegate;
	
	@Reference
	private BookingSession bookingSession;
	
	@Reference
	private AlitaliaConfigurationHolder alitaliaConfiguration;
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws Exception {
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		
		try{
			response.setContentType("application/json");
			
			BookingSessionContext ctx = getBookingSessionContext(request);
			
			//INIZIO *** MODIFICA IP ***
			String ipAddress = "";
			ipAddress = AlitaliaUtils.getRequestRemoteAddr(alitaliaConfiguration, request);
			if (ipAddress != null && !ipAddress.isEmpty()) {
				ctx.ipAddress = ipAddress;
			}
			//FINE.
			
			Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
			String languageCode = AlitaliaUtils.getLanguage(AlitaliaUtils.getLanguage(locale.toLanguageTag()));
			String marketCode = ctx.market;
			
			/* Perform MetaSearch */
			SearchFlightSolutionRequest searchFlightSolutionRequest = 
					new SearchFlightSolutionRequest(IDFactory.getTid(), IDFactory.getSid(request));
			searchFlightSolutionRequest.setCookie(ctx.cookie);
			searchFlightSolutionRequest.setExecution(ctx.execution);
			searchFlightSolutionRequest.setSabreGateWayAuthToken(ctx.sabreGateWayAuthToken);
			
			//INIZIO *** MODIFICA IP ***
			searchFlightSolutionRequest.setIpAddress(ctx.ipAddress);
			//FINE.
			
			if(ctx.metaSearchData.getDestinations().get(0).getFromAirport() != null){
				searchFlightSolutionRequest.setFromSearchElement(ctx.metaSearchData.getDestinations().get(0).getFromAirport().toString());
			}
			if(ctx.metaSearchData.getDestinations().get(0).getToAirport() != null){
				searchFlightSolutionRequest.setFromSearchElement(ctx.metaSearchData.getDestinations().get(0).getToAirport().toString());
			}
			searchFlightSolutionRequest.setFilter(ctx.metaSearchData);
			searchFlightSolutionRequest.setLanguageCode(languageCode.toUpperCase());
			searchFlightSolutionRequest.setMarket(marketCode.toUpperCase());
			searchFlightSolutionRequest.setResponseType(SearchExecuteResponseType.MODEL);
			
			searchFlightSolutionRequest.setFromSearchElement("");
			searchFlightSolutionRequest.setToSearchElement("");
			
			String brandsPath = AlitaliaUtils.findSiteBaseRepositoryPath(request.getResource()) 
					+ AlitaliaConstants.BASE_BRAND_PATH;
			Resource rootResource = request.getResource().getResourceResolver().getResource(brandsPath);
			bookingSession.performMetaSearch(ctx, searchFlightSolutionRequest, rootResource);
	
			/* Confirm Flight Selection */
			bookingSession.confirmFlightSelections(ctx, "", request);
			
			/* result of ajax call */
			String redirectUrl = request.getResource().getResourceResolver().map(
					AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false))
					+ alitaliaConfiguration.getBookingPassengersDataPage();
			
			
			json.object();
			
			json.key("result").value("OK");
			json.key("redirect").value(redirectUrl);
			json.endObject();
		} catch(Exception e){
			logger.error("[BookingMetaSearchServlet] Exception: ", e);
			response.setContentType("application/json");
			String redirectUrl = request.getResource().getResourceResolver().map(
					AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false))
					+ alitaliaConfiguration.getBookingFailurePage();
			
			json.object();
			json.key("result").value("NOK");
			json.key("redirect").value(redirectUrl);
			json.endObject();
		}
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		
		return resultValidation;
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return null;
	}
}
