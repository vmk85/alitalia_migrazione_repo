/**
 * Classe di utility per MyAlitalia
 * ( Sono presenti varie utility da poter utilizzare all'interno della sezione MyAlitalia)
 * [D.V.]
 */

package com.alitalia.aem.consumer.myalitalia;

import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.messages.crmdatarest.*;
import com.alitalia.aem.common.data.crmdatarest.model.*;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.myalitalia.model.ProfileComplite;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


public class MyAlitaliaUtils {

    /**
     * Metodo per la data attuale
     */
    public static Date getActualDate() {
        final Date currentTime = new Date();
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf1.parse(sdf.format(currentTime));
        } catch (ParseException e) {
            throw new RuntimeException("Error to parse date actual MyAlitaliaCheckOtp", e);
        }
    }

    /**
     * Metodo per convertire il prefisso
     */
    public static String fixPrefix(String prefix) {
        String fix = "";
        if (prefix.length() < 4) {
            for (int i = prefix.length(); i < 4; i++) fix = fix.concat("0");
            return fix.concat(prefix);
        } else if (prefix.length() == 4) return prefix;
        else return null;
    }


    /**
     * Metodo per restituire il testo di completamento profilo in base alla percentuale
     */
    public static String getTextUrlByPercent(int percent, GetCrmDataInfoResponse getCrmDataInfoResponse) {

        String text = "";

        if (percentPersonalData(getCrmDataInfoResponse) < 20) {

            text = "myalitalia.info.completaDatiPersonali";

        }
        if (percentDatiDiViaggio(getCrmDataInfoResponse) < 20) {

            text = "myalitalia.info.completaDatiViaggio";

        }
        if (percentPreferenzeViaggio(getCrmDataInfoResponse) < 20) {

            text = "myalitalia.info.completaPreferenza";

        }

        return text;
    }

    /**
     * Metodo per restituire l'url di completamento profilo in base alla percentuale
     */
    static public String getUrlByPercent(int percent, GetCrmDataInfoResponse getCrmDataInfoResponse) {

        String url = "";

        if (percentPersonalData(getCrmDataInfoResponse) < 20) {

            url = "./myalitalia-dati-personali.html?page=personalData";

        }
        if (percentDatiDiViaggio(getCrmDataInfoResponse) < 20) {

            url = "./myalitalia-dati-di-viaggio.html?page=dataTravel";

        }
        if (percentPreferenzeViaggio(getCrmDataInfoResponse) < 20) {

            url = "./myalitalia-millemiglia-preferenze-viaggio.html?page=preferences";

        }

        return url;
    }

    /**
     * Metodo per calcolare la percentuale di completamento del profilo MyAlitalia
     */
    static public int getPercentProfileComplete(GetCrmDataInfoResponse getCrmDataInfoResponse) {

        int percentual = 0;

        percentual += percentPersonalData(getCrmDataInfoResponse);

        percentual += percentDatiDiViaggio(getCrmDataInfoResponse);

        percentual += percentPreferenzeViaggio(getCrmDataInfoResponse);

        /** La percentuale massima è 65% */
        if (percentual >= 65) {

            percentual = 100;

        }

        return percentual;

    }

    public static boolean isChecked(String market, ResourceResolverFactory resolverFactory, String check) throws LoginException {

        ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
        Resource res = resourceResolver.getResource("/content/alitalia/alitalia-" + market);
        Page page = res.adaptTo(Page.class);
        ValueMap props = page.getProperties();
        String title = props.get(check, "default value");
        return Boolean.parseBoolean(title);

    }

    /**
     * Percentuale preferenze viaggio
     */
    private static int percentPreferenzeViaggio(GetCrmDataInfoResponse getCrmDataInfoResponse) {
        PreferenzeViaggio preferenzeViaggio = getCrmDataInfoResponse.getPreferenzeViaggio();

        int percentual = 0;

        if (preferenzeViaggio != null) {

            if (!preferenzeViaggio.getPosto().equals("0")) {

                percentual += 5;

            }

            if (!preferenzeViaggio.getPasto().equals("0")) {

                percentual += 5;

            }

            if (!preferenzeViaggio.getAeroportoPreferito().equals("")) {

                percentual += 10;

            }
            /** Attualmente è booleano, sempre valorizzato */
//            if (!preferenzeViaggio.isViaggiInFamiglia()){

            percentual += 5;

//            }
        }

        return percentual;
    }

    /**
     * Percentuale dati di viaggio
     */
    private static int percentDatiDiViaggio(GetCrmDataInfoResponse getCrmDataInfoResponse) {
        List<Documento> elencoDocumenti = getCrmDataInfoResponse.getElencoDocumenti();

        int percentual = 0;

        if (elencoDocumenti != null) {

            if (elencoDocumenti.size() == 1) {

                percentual += 10;

            } else if (elencoDocumenti.size() >= 2) {

                percentual += 20;

            }
        }

        return percentual;

    }

    /**
     * Percentuale PersonalArea
     */
    private static int percentPersonalData(GetCrmDataInfoResponse getCrmDataInfoResponse) {
        InfoCliente infoCliente = getCrmDataInfoResponse.getInfoCliente();
        RecapitiTelefonici recapitiTelefonici = getCrmDataInfoResponse.getRecapitiTelefonici();
        int percentual = 0;

        if (infoCliente != null) {

            if (!infoCliente.getNome().equals("") && !infoCliente.getCognome().equals(""))
                percentual += 4;

            if (!infoCliente.getSesso().equals("") && !infoCliente.getDataNascita().equals(""))
                percentual += 4;

            if (!infoCliente.getIndirizzo().equals("") && !infoCliente.getCap().equals(""))
                percentual += 4;

            if (!infoCliente.getCitta().equals("") && !infoCliente.getNazione().equals(""))
                percentual += 4;

            if ((!recapitiTelefonici.getNumero().equals("") || !recapitiTelefonici.getCellulare().equals("")) && !recapitiTelefonici.getTipo().equals("") && !recapitiTelefonici.getPrefissioNazionale().equals(""))
                percentual += 4;

        }

        return percentual;

    }

    /**
     * Metodo per impostare la request del GetCrmDataInfo
     */
    static public GetCrmDataInfoRequest prepareGetCrmDataInfoRequest(SlingHttpServletRequest request, MACustomer maCustomer) {

        /** Dichiaro l'oggetto di Request */
        GetCrmDataInfoRequest getCrmDataInfoRequest = new GetCrmDataInfoRequest();

        /** Fill dei campi per a Request */
        getCrmDataInfoRequest.setIdMyAlitalia(maCustomer.getUID());
        getCrmDataInfoRequest.setCanale(MyAlitaliaConstants.MYALITALIA_SERVICE_UPDATE);
        getCrmDataInfoRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
        getCrmDataInfoRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
        getCrmDataInfoRequest.setConversationID(IDFactory.getTid());

        /** Ritorno il contesto di Request
         * ( Se vuoto, non posso effettuare la chiamata al CRM) */
        return getCrmDataInfoRequest;

    }

    /**
     * Metodo per estrarre il contesto MyAlitalia dalla sessione
     */
    static public MyAlitaliaSessionContext getMactxBySession(SlingHttpServletRequest request) {

        MyAlitaliaSessionContext myAlitaliaSessionContext = null;
        /** Ritorno il contesto di sessione
         * ( Se vuoto, non ho un contesto in sessione con elativo utente MyAlitalia) */
        Object obj = request.getSession().getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);

        if (obj instanceof MyAlitaliaSessionContext) {
            myAlitaliaSessionContext = (MyAlitaliaSessionContext) obj;
        }
        return myAlitaliaSessionContext;

    }

    /**
     * Metodo per aggiungere il contesto MyAlitalia in sessione
     */
    static public boolean setMactxToSession(SlingHttpServletRequest request, MyAlitaliaSessionContext mactx) {

        boolean success = false;

        /** Aggiungo il contesto aggiornato in sessione */
        request.getSession().setAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE, mactx);

        /** Estrggo il nuovo contesto dalla sessione */
        mactx = getMactxBySession(request);

        /** Controllo che il contesto sia stato aggiunto in sessione */
        if (mactx != null) {

            success = true;

        } else {

            success = false;

        }
        return success;
    }

    /**
     * Metodo per aggiungere il customer in sessione
     */
    static public boolean setMaCustomerToSession(SlingHttpServletRequest request, MyAlitaliaSession myAlitaliaSession, MACustomer maCustomer) {

        /** Variabile di success salvataggio */
        boolean success = false;

        /** Estrggo il nuovo contesto dalla sessione */
        MyAlitaliaSessionContext mactx = getMactxBySession(request);

        /** Controllo che gia esista una sessione MyAlitalia */
        if (mactx != null) {

            /** Aggiungo il nuovo utente nel contesto */
            mactx.setMaCustomer(maCustomer);

            /** Aggiungo il contesto aggiornato in sessione */
            success = setMactxToSession(request, mactx);

        } else {

            /** Inizializzo la sessione se non presente
             * ( Aggiungo il nuovo Customer ) */
            mactx = myAlitaliaSession.initializeSession(request, maCustomer);

            /** Aggiungo il contesto in sessione*/
            success = setMactxToSession(request, mactx);

        }

        return success;
    }

    /**
     * Metodo per estrarre il customer dalla sessione
     */
    static public MACustomer getMaCustomerBySession(SlingHttpServletRequest request) {

        MACustomer maCustomer = null;

        /** Estrggo contesto dalla sessione */
        MyAlitaliaSessionContext mactx = getMactxBySession(request);

        if (mactx != null) {

            /** Estraggo il customer dal contesto*/
            maCustomer = getMaCustomerByMactx(mactx);
        }

        return maCustomer;
    }

    /**
     * Metodo per estrarre il customer dal contesto
     */
    static public MACustomer getMaCustomerByMactx(MyAlitaliaSessionContext mactx) {

        return mactx.getMaCustomer();

    }

    static public String getJsonResponse(Object object, Result result) {

        String jsonResponse = null;

        if (object != null) {

            jsonResponse = getJsonResponseService(object, result);

        } else {

            jsonResponse = getJsonResponseError();

        }

        return jsonResponse;

    }

    private static String getJsonResponseError() {

        HashMap<Object, Object> map = new HashMap<>();

        Gson gson = new Gson();

        map.put("isError", true);

        return getJsonFromMapObject(map, null);

    }

    private static String getJsonFromMapObject(HashMap<Object, Object> map, Object object) {

        Gson gson = new Gson();

        if (object == null) {

            map.put("errorDescription", "myalitalia.service.not.response");

        }
//        else {

//            map.put("response", object);

//        }

        return gson.toJson(map);

    }

    private static String getJsonResponseService(Object object, Result result) {

        HashMap<Object, Object> map = new HashMap<>();

        if (result != null) {

            map.put("errorSourceDetails", result.getErrorSourceDetails());
            map.put("errorDescription", "myalitalia.crm.error");
            map.put("code", result.getCode());
            if (!result.getStatus().equals(MyAlitaliaConstants.MYALITALIA_SERVICE_ERROR)) {
                map.put("isError", false);
            } else {
                map.put("isError", true);
            }

        } else {
            map.put("isError", true);
        }

        return getJsonFromMapObject(map, object);

    }

    public static boolean setCrmResponseDataToSession(SlingHttpServletRequest request, GetCrmDataInfoResponse getCrmDataInfoResponse) {

        /** Variabile di success salvataggio */
        boolean success = false;

        /** Estrggo il nuovo contesto dalla sessione */
        MyAlitaliaSessionContext mactx = getMactxBySession(request);

        /** Controllo che gia esista una sessione MyAlitalia */
        if (mactx != null) {

            /** Aggiungo il nuovo utente nel contesto */
            mactx.setGetCrmDataInfoResponse(getCrmDataInfoResponse);

            /** Aggiorno la percentuale di completamento profilo */
            ProfileComplite profileComplite = new ProfileComplite();
            profileComplite.setPercent(getPercentProfileComplete(getCrmDataInfoResponse));
            profileComplite.setTesto(getTextUrlByPercent(profileComplite.getPercent(), getCrmDataInfoResponse));
            profileComplite.setRedirectUrl(getUrlByPercent(profileComplite.getPercent(), getCrmDataInfoResponse));
            mactx.setProfileComplite(profileComplite);

            /** Aggiungo il contesto aggiornato in sessione */
            success = setMactxToSession(request, mactx);
        }

        return success;

    }

    /**
     * Metodo per reperire i dati dell'utente in sessione dal CRM
     */
    public static GetCrmDataInfoResponse getMyAlitaliaCrmDataBySession(SlingHttpServletRequest request) {

        GetCrmDataInfoResponse getCrmDataInfoResponse = null;

        MyAlitaliaSessionContext mactx = getMactxBySession(request);

        if (mactx != null) {

            getCrmDataInfoResponse = getGetCrmDataInfoResponseByMactx(mactx);

        }

        /** Ritorno la risposta del servizio */
        return getCrmDataInfoResponse;
    }

    private static GetCrmDataInfoResponse getGetCrmDataInfoResponseByMactx(MyAlitaliaSessionContext mactx) {

        return mactx.getGetCrmDataInfoResponse();

    }

    public static boolean checkSession(SlingHttpServletRequest request) {

        boolean success = false;

        if (getMaCustomerBySession(request) != null && getMyAlitaliaCrmDataBySession(request) != null) {

            success = true;

        }

        return success;
    }

    public static String getCodeLanguagefromLinguaCorrispondenza(String linguaCorrispondenza) {
        String langCode;

        switch (linguaCorrispondenza) {
            case "Italiano":
                langCode = "IT";
                break;
            case "Inglese":
                langCode = "EN";
                break;
            case "Giapponese":
                langCode = "JA";
                break;
            case "Francese":
                langCode = "FR";
                break;
            case "Spagnolo":
                langCode = "ES";
                break;
            case "Portoghese":
                langCode = "PT";
                break;
            case "Tedesco":
                langCode = "DE";
                break;
            default:
                langCode = "IT";
                break;

        }
        return langCode;
    }

    /**
     * Metodo per calcolare la differenza tra 2 date
     */
    public static long getDifferenceDate(String value, String value1) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date startDate = null;
        Date expdate = null;
        try {
            startDate = df.parse(value1);
            expdate = df.parse(value);
            return Math.abs(expdate.getTime() - startDate.getTime());
        } catch (ParseException e) {
            throw new RuntimeException("Error to parse date", e);
        }
    }
}
