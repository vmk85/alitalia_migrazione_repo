package com.alitalia.aem.consumer.servlet.airportdestination;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AirportDestinationsData;
import com.alitalia.aem.common.messages.home.GetDestinationsResponse;
import com.alitalia.aem.common.messages.home.RetrieveAirportDestinationRequest;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.staticdata.delegaterest.StaticDataDelegateRest;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "airportdestinationconsumer" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class AirportDestinationConsumerServlet extends SlingSafeMethodsServlet {
	
	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	//Search Airport Destination
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegateRest staticDataDelegateRest;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		
		// content type
		response.setContentType("application/json");

		// JSON response
		TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());
		
		try {
					
			// prepare resource bundle for i18n
			Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
			ResourceBundle resourceBundle = request.getResourceBundle(locale);
			final I18n i18n = new I18n(resourceBundle);
				
			// setting variables for airport json object
			String airportCode;
			String keyAirportName;
			String airportName;
			String keyCityCode;
			String cityCode;
			String city;
			String countryCode;
			String keyCountryCode;
			String country;
			
			RetrieveAirportDestinationRequest destinationRequest = new RetrieveAirportDestinationRequest(IDFactory.getTid(), IDFactory.getSid(request));
			String language = AlitaliaUtils.getRepositoryPathLanguage(request.getResource());//findResourceLocale(slingHttpServletRequest.getResource()).getLanguage();
			String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());//AlitaliaUtils.findResourceLocale(slingHttpServletRequest.getResource()).getCountry();


			String iata = null;
			if (request.getRequestPathInfo().getSelectorString() != null) {
				String[] codeArray = request.getRequestPathInfo().getSelectors();
				iata = codeArray[(codeArray.length - 1)];
			}
			//String iata = request.getParameter("iata");


			String conversationId = IDFactory.getTid();
			destinationRequest.setMarket(market);
			destinationRequest.setLanguage(language);
			destinationRequest.setIata(iata);
			destinationRequest.setConversationID(conversationId);
			logger.info("Chiamando StaticDataDelegateRest da AirportDestinationServlet...");
			GetDestinationsResponse serviceResponse = staticDataDelegateRest.getAirportDestination(destinationRequest);
			logger.info("Dopo StaticDataDelegateRest da AirportDestinationServlet...");
			
//			// out JSON response
			jsonOutput.object();
		
//			/* Lista degli areoporti presi dal servizio */
			jsonOutput.key("airports");
			jsonOutput.array();
			
			List<AirportDestinationsData> list = serviceResponse.getDestinations();
			List<DestinationInfo> destinationList = new ArrayList<DestinationInfo>();
			
			for (AirportDestinationsData airportDestinationsData : list) {
				
				airportCode = airportDestinationsData.getCode();
				keyAirportName = "airportsData." + airportCode + ".name";
				airportName = i18n.get(keyAirportName);
				
				cityCode = airportDestinationsData.getCityCode();
				keyCityCode = "airportsData." + airportCode + ".city";
				city = i18n.get(keyCityCode);
				
				countryCode = airportDestinationsData.getCountryCode();
				keyCountryCode = "airportsData." + countryCode + ".country";
				country = i18n.get(keyCountryCode);
				
				// add to JSON only if translation is provided
				if (!city.equals(keyCityCode) &&
						!country.equals(keyCountryCode) &&
						!airportName.equals(keyAirportName)) {
					DestinationInfo destination =
							new DestinationInfo(
								airportCode, keyAirportName,
								airportName, cityCode, keyCityCode, city,
								countryCode, keyCountryCode, country);
					destinationList.add(destination);
				}
			}
			Collections.sort(destinationList,
					DestinationInfo.COMPARE_BY_CITY_AND_AIRPORT);
			
			
			for (DestinationInfo destinationInfo : destinationList) {
				
				jsonOutput.object(); 
				jsonOutput.key("value").value(destinationInfo.getCity()
						+ ", " + destinationInfo.getAirportName()
						+ ", " + destinationInfo.getCountry());
				jsonOutput.key("city").value(destinationInfo.getCity());
				jsonOutput.key("country").value(destinationInfo.getCountry());
				jsonOutput.key("airport").value(destinationInfo.getAirportName());
				jsonOutput.key("code").value(destinationInfo.getAirportCode());
				jsonOutput.key("type").value(destinationInfo.getCityCode());
				//TODO aggiungere marketAreaCode al json?
				
				// internationalization keys
				jsonOutput.key("keyCityCode").value(
						destinationInfo.getKeyCityCode());
				jsonOutput.key("keyCountryCode").value(
						destinationInfo.getKeyCountryCode());
				jsonOutput.key("keyAirportName").value(
						destinationInfo.getKeyAirportName());
				
				jsonOutput.endObject();
			}

				jsonOutput.endArray();
			
				jsonOutput.key("conversationID").value(serviceResponse.getConversationID());
			
			jsonOutput.key("result").value("OK");
			jsonOutput.endObject();
			}

		// an error occurred...
		catch (Exception e) {
			e.printStackTrace();
			
			//logger.error("Errore durante recupero Airport List Consumer", e);
			throw new RuntimeException("Errore durante recupero Destination List Consumer", e);
		}
			
	
		}

}
