package com.alitalia.aem.consumer.booking.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SearchElement {
	
	private SearchElementAirport from;
	private SearchElementAirport to;
	private Calendar departureDate;

	public SearchElement(SearchElementAirport from, SearchElementAirport to, Calendar departureDate) {
		this.from = from;
		this.to = to;
		this.departureDate = departureDate;
	}

	public SearchElementAirport getFrom() {
		return from;
	}

	public SearchElementAirport getTo() {
		return to;
	}

	public Calendar getDepartureDate() {
		return departureDate;
	}

	public void setFrom(SearchElementAirport from) {
		this.from = from;
	}

	public void setTo(SearchElementAirport to) {
		this.to = to;
	}

	public void setDepartureDate(Calendar departureDate) {
		this.departureDate = departureDate;
	}

	@Override
	public String toString() {
		if (from != null && to != null && departureDate != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
			return "SearchElement [from=" + from.getAirportCode() 
					+ ", to=" + to.getAirportCode() 
					+ ", departureDate=" + sdf.format(departureDate.getTime())
					+ "]";
		} else {
			return "SearchElement Empty or null fields";
		}
	}
	
}
