package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.Servlet;


import com.alitalia.aem.common.data.checkinrest.model.boardingpass.Model;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.PDFManager;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.PnrRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.day.cq.i18n.I18n;
import org.apache.commons.codec.binary.Base64;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = { "checkisendboardyngpassbymail"}),
        @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
        @Property(name = "sling.servlet.extensions", value = { "json" }),
        @Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinSendBoardingPassByMailServlet extends GenericCheckinFormValidatorServlet {

    // logger
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    private AlitaliaConfigurationHolder configuration;


    @Reference
    private PDFManager pdfManager;

    @Reference
    private CheckinSession checkinSession;

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile BusinessLoginService businessLoginService;

    @Override
    protected AlitaliaConfigurationHolder getConfiguration() {
        return configuration;
    }

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        ResultValidation resultValidation = new ResultValidation();
        resultValidation.setResult(true);
        return resultValidation;
    }

    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

//        logger.info("[CheckinSendBoardingPassByMailServlet - performSubmit] prima di initSession....");

        CheckinSessionContext ctx = getCheckInSessionContext(request);

        try{

//            logger.info("[CheckinSendBoardingPassByMailServlet] [INIZIO] ");

            List<String> mails = new ArrayList<String>();
            String[] _mails = request.getParameter("mailAddress").split(",");
            for(String _mail:_mails)
            {
                mails.add(_mail);
            }

            String[] values = new String[4];
            if(request.getParameter("name_surname") != null && request.getParameter("name_surname").length()>0) {
                values[0] = request.getParameter("name_surname");
            }else{
                String nome_cognome = "";
                for(int i = 0; i<ctx.checkinSelectedPassengers.size(); i++){
                    if(ctx.checkinSelectedPassengers.get(i).getSelectee() == true){
                        continue;
                    }
                    if(i == ctx.checkinSelectedPassengers.size()-1) {
                        nome_cognome += ctx.checkinSelectedPassengers.get(i).getNome() + " " + ctx.checkinSelectedPassengers.get(i).getCognome();
                    }else{
                        nome_cognome += ctx.checkinSelectedPassengers.get(i).getNome() + " " + ctx.checkinSelectedPassengers.get(i).getCognome() + ",";
                    }
                }
                values[0] = nome_cognome;
            }
            values[1] = ctx.pnr;//getPnr(ctx);
            values[2] = "";//getFlightsHtml(ctx, request);
            values[3] = "";//computeBusMessage(ctx);

            ByteArrayOutputStream out = (ByteArrayOutputStream) pdfManager.manageCheckinPDF_1(request, response, false);

            final I18n i18n = new I18n(request.getResourceBundle(
                    AlitaliaUtils.findResourceLocale(request.getResource())));

            String prefix = "http://";
            if (configuration.getHttpsEnabled()) {
                prefix = "https://";
            }
            String flights = "";
            for(Model model : ctx.boardingPassInfo.getModels()){
                String skypriority = "";
                if(model.getIsSkyPriority()){
                    skypriority = "SKY PRIORITY";
                }

                String terminal = "-";
                if(model.getBoardingTerminal() != null){
                    if(!model.getBoardingTerminal().equals("null")){
                        terminal = model.getBoardingTerminal();
                    }
                }

                String gate = "-";
                if(model.getBoardingGate() != null){
                    if(!model.getBoardingGate().equals("null")){
                        gate = model.getBoardingGate();
                    }
                }

                String zone = "-";
                if(model.getBoardingZone() != null){
                    if(!model.getBoardingZone().equals("null")){
                        zone = model.getBoardingZone();
                    }
                }


                String dataPartenza = "";
                String oraPrtenza = "";
                String dataArrivo = "";
                String oraArrivo = "";
                String volocompleto = "";
                String status = "";
                String partenza = "";
                String partenzaCode = "";
                String arrivo = "";
                String arrivoCode = "";
                for(PnrRender pnrRender : ctx.pnrSelectedListDataRender) {
                    for (FlightsRender flightsRender : pnrRender.getFlightsRender()) {
                        for (SegmentRender segmentRender : flightsRender.getSegments()) {
                            if(segmentRender.getFlight().equals(model.getFlightNumber().substring(2,model.getFlightNumber().length()))){
                                dataPartenza = segmentRender.getDepartureDate();
                                oraPrtenza = segmentRender.getDepartureTime();
                                dataArrivo = segmentRender.getArrivalDate();
                                oraArrivo = segmentRender.getArrivalTime();
                                volocompleto = segmentRender.getAirline() + " " + segmentRender.getFlight();
                                status = segmentRender.getFlightDelay();
                                partenza = segmentRender.getOrigin().getCity();
                                partenzaCode = segmentRender.getOrigin().getCode();
                                arrivo = segmentRender.getDestination().getCity();
                                arrivoCode = segmentRender.getDestination().getCode();

                            }
                        }
                    }
                }
                flights = flights + "<p class=\"info-title\" style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">" + partenza  +
                        " <strong>" + partenzaCode + "</strong> - " + arrivo +
                        " <strong>" + arrivoCode+ "</strong>" +
                        "</p>" +
                        "<table class=\"wrapper\" align=\"center\" bgcolor=\"#006643\" style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<td class=\"wrapper-inner\" style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">" +
                        "<p class=\"info-header\" style=\"Margin:10px;Margin-bottom:10px;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin:10px;margin-bottom:10px;padding:0;text-align:left\">"  + model.getPassengerName() + "</p>" +
                        "</td>" +
                        "</tr>" +
                        "</table>" +
                        "<table class=\"wrapper\" align=\"center\" bgcolor=\"#ffffff\" style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<td class=\"wrapper-inner\" style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">" +
                        "<div class=\"info\" style=\"Margin:10px;margin:10px\">" +
                        "<table class=\"spacer\" style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tbody>" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<td height=\"15px\" style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:15px;font-weight:400;hyphens:auto;line-height:15px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">&#xA0;</td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "<table class=\"row\" style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">" +
                        "<tbody>" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th class=\"small-12 large-6 columns first\" style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:50%\">" +
                        "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">" +
                        "<div class=\"arr-dep\" style=\"Margin:0 0 25px;margin:0 0 25px\">" +
                        "<strong style=\"text-transform: uppercase;\">" + i18n.get("checkin.pdf.departure") + "</strong>" +
                        "</div>" +
                        "<table class=\"row\" style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">" +
                        "<tbody>" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th class=\"small-2 large-2 columns first\" valign=\"middle\"" +
                        "style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">" +
                        "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">" +
                        "<img width=\"36\" height=\"20\" src=\"" + prefix + "www.alitalia.com" + configuration.getStaticImagesPath1() + "icon-departure.png\" alt=\"departure\" title=\"departure\""+
                        "style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">" +
                        "</th>" +
                        "</tr>" +
                        "</table>" +
                        "</th>" +
                        "<th class=\"small-10 large-10 columns last\" valign=\"middle\"" +
                        "style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">" +
                        "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">" + dataPartenza +"</th>" +
                        "</tr>" +
                        "</table>" +
                        "</th>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "<table class=\"row no-padding\" style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">" +
                        "<tbody>" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th class=\"small-2 large-2 columns first\" valign=\"middle\"" +
                        "style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">" +
                        "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">" +
                        "<img width=\"29\" height=\"20\" src=\""  + prefix + "www.alitalia.com" + configuration.getStaticImagesPath1() +"icon-clock.png\" alt=\"clock\" title=\"clock\"" +
                        "style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">" +
                        "</th>" +
                        "</tr>" +
                        "</table>" +
                        "</th>" +
                        "<th class=\"small-10 large-10 columns last\" valign=\"middle\"" +
                        "style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">" +
                        "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">" + oraPrtenza + "</th>" +
                        "</tr>" +
                        "</table>" +
                        "</th>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "</th>" +
                        "</tr>" +
                        "</table>" +
                        "</th>" +
                        "<th class=\"small-12 large-6 columns last\" style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:50%\">" +
                        "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">" +
                        "<div class=\"arr-dep\" style=\"Margin:0 0 25px;margin:0 0 25px\">" +
                        "<strong>" + i18n.get("flightstatus.arrival.label") + "</strong>" +
                        "</div>" +
                        "<table class=\"row\" style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">" +
                        "<tbody>" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th class=\"small-2 large-2 columns first\" valign=\"middle\"" +
                        "style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">" +
                        "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">" +
                        "<img width=\"36\" height=\"20\" src=\""  + prefix + "www.alitalia.com" + configuration.getStaticImagesPath1() + "icon-arrival.png\" alt=\"arrival\" title=\"arrival\"\"" +
                        "style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">" +
                        "</th>" +
                        "</tr>" +
                        "</table>" +
                        "</th>" +
                        "<th class=\"small-10 large-10 columns last\" valign=\"middle\"" +
                        "style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">" +
                        "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left\">" + dataArrivo + "</th>" +
                        "</tr>" +
                        "</table>" +
                        "</th>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "<table class=\"row no-padding\" style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">" +
                        "<tbody>" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th class=\"small-2 large-2 columns first\" valign=\"middle\"" +
                        "style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:16.66667%\">" +
                        "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">" +
                        "<img width=\"29\" height=\"20\" src=\"" + prefix + "www.alitalia.com" + configuration.getStaticImagesPath1() + "icon-clock.png\" alt=\"clock\" title=\"clock\"" +
                        "style=\"-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto\">" +
                        "</th>" +
                        "</tr>" +
                        "</table>" +
                        "</th>" +
                        "<th class=\"small-10 large-10 columns last\" valign=\"middle\"" +
                        "style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:83.33333%\">" +
                        "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">" + oraArrivo + "</th>" +
                        "</tr>" +
                        "</table>" +
                        "</th>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "</th>" +
                        "</tr>" +
                        "</table>" +
                        "</th>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "<hr style=\"border:1px solid #e6e6e6\">" +
                        "<table class=\"spacer\" style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tbody>" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<td height=\"15px\" style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:15px;font-weight:400;hyphens:auto;line-height:15px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\">&#xA0;</td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "<table class=\"row no-padding\" style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\">" +
                        "<tbody>" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th class=\"small-12 large-4 columns first\" style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">" +
                        "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">" +
                        "<p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">" + i18n.get("checkin.mail.biglietto.label") +
                        "<br>" +
                        "<strong class=\"info-txt\">"+ model.getTicketNumber() +"</strong>" +
                        "</p>" +
                        "<br>" +
                        "<p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">" + i18n.get("checkin.pdf.gate") + "/" + i18n.get("checkin.pdf.terminal") +
                        "<br>" +
                        "<strong class=\"info-txt\">" + gate + " | " + terminal + "</strong>" +
                        "</p>" +
                        "<br>" +
                        "<p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">" + i18n.get("checkin.pdf.flight") +
                        "<br>" +
                        "<strong class=\"info-txt\">" + volocompleto + "</strong>" +
                        "</p>" +
                        "</th>" +
                        "</tr>" +
                        "</table>" +
                        "</th>" +
                        "<th class=\"small-12 large-4 columns\" style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">" +
                        "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">" +
                        "<p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">" + i18n.get("checkin.mail.pnr.label") +
                        "<br>" +
                        "<strong class=\"info-txt\">" + model.getPnr() + "</strong>" +
                        "</p>" +
                        "<br>" +
                        "<p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">" + i18n.get("checkin.confirmation.recap.seat") +
                        "<br>" +
                        "<strong class=\"info-txt\">" + model.getSeat() + "</strong>" +
                        "</p>" +
                        "<br>" +
                        "<p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">" + i18n.get("checkin.pdf.boarding") +
                        "<br>" +
                        "<strong class=\"info-txt\">" + model.getBoardingTime() + "</strong>" +
                        "</p>" +
                        "</th>" +
                        "</tr>" +
                        "</table>" +
                        "</th>" +
                        "<th class=\"small-12 large-4 columns last\" style=\"Margin:0 auto;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:20px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%\">" +
                        "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\">" +
                        "<tr style=\"padding:0;text-align:left;vertical-align:top\">" +
                        "<th style=\"Margin:0;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left\">" +
                        "<p style=\"Margin:0;Margin-bottom:10px;color:#be210c;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">" + skypriority +
                        "<br>" +
                        "<strong class=\"info-txt\">&nbsp;</strong>" +
                        "</p>" +
                        "<br>" +
                        "<p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">Zona" +
                        "<br>" +
                        "<strong class=\"info-txt\">" + zone + "</strong>" +
                        "</p>" +
                        "<br>" +
                        "<p style=\"Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left\">" + i18n.get("checkin.flightstatus.label") +
                        "<br>" +
                        "<strong class=\"info-txt\">" + i18n.get(status) + "</strong>" +
                        "</p>" +
                        "</th>" +
                        "</tr>" +
                        "</table>" +
                        "</th>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "</div>" +
                        "</td>" +
                        "</tr>" +
                        "</table>";
            }
            values[2] = flights;

            boolean send;

            if(request.getParameter("recap") == null) {

                send = checkinSession.sendEmail(ctx, request.getResource(), mails, CheckinConstants.CHECKIN_TEMPLATE_EMAIL_BOARDING_PASS_NEW, CheckinConstants.CHECKIN_BOARDING_PASS_EMAIL_SUBJECT, out.toByteArray(), CheckinConstants.CHECKIN_BOARDING_PASS_EMAIL_ATTACHMENT, CheckinConstants.PLACEHOLDER_BOARDING, values, CheckinConstants.CHECKIN_MAIL_REMINDER, ctx.selectedPassengers);

            }else {
                ByteArrayOutputStream outEmpty = new ByteArrayOutputStream();
                send = checkinSession.sendEmail(ctx, request.getResource(), mails, CheckinConstants.CHECKIN_TEMPLATE_EMAIL_RECAP, CheckinConstants.CHECKIN_RECAP_EMAIL_SUBJECT, outEmpty.toByteArray(), CheckinConstants.CHECKIN_BOARDING_PASS_EMAIL_ATTACHMENT, CheckinConstants.PLACEHOLDER_BOARDING, values, CheckinConstants.CHECKIN_MAIL_REMINDER, ctx.selectedPassengers);
            }

            if(send){
                managementSucces(request,response);
            } else {
                managementError(request, response);
            }

        }catch(Exception e){
            throw new RuntimeException("[CheckinSendBoardingPassByMailServlet] - Errore durante l'invocazione del servizio.", e);
        }

    }


    public void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response){

        TidyJSONWriter json;
        try {
            json = new TidyJSONWriter(response.getWriter());
            response.setContentType("application/json");
            json.object();
            json.key("isError").value(true);
            json.endObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response){

        TidyJSONWriter json;
        try {

            json = new TidyJSONWriter(response.getWriter());
            response.setContentType("application/json");
            json.object();
            json.key("isError").value(false);
            json.endObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
