package com.alitalia.aem.consumer.myalitalia.model;

import com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse;

import java.util.Date;

public class MyAlitaliaOtpModel {

    private String OTP;

    private long expMillisecondOtp;

    private Date startDate;

    private String numero;

    private String prefisso;

    private int countError = 0;

    public int getCountError() {
        return countError;
    }

    public void setCountError(int countError) {
        this.countError = countError;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getPrefisso() {
        return prefisso;
    }

    public void setPrefisso(String prefisso) {
        this.prefisso = prefisso;
    }

    public String getOTP() {
        return OTP;
    }

    public void setOTP(String OTP) {
        this.OTP = OTP;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public long getExpMillisecondOtp() {
        return expMillisecondOtp;
    }

    public void setExpMillisecondOtp(long expMillisecondOtp) {
        this.expMillisecondOtp = expMillisecondOtp;
    }

    @Override
    public String toString() {
        return "MyAlitaliaOtpModel{" +
                "OTP='" + OTP + '\'' +
                ", expMillisecondOtp=" + expMillisecondOtp +
                ", startDate=" + startDate +
                ", numero='" + numero + '\'' +
                ", prefisso='" + prefisso + '\'' +
                ", countError=" + countError +
                '}';
    }
}
