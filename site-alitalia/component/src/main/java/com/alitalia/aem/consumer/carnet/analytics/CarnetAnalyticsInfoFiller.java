package com.alitalia.aem.consumer.carnet.analytics;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.TimeZone;

import com.alitalia.aem.common.data.home.carnet.CarnetCreditCardProviderData;
import com.alitalia.aem.common.data.home.enumerations.CarnetPaymentTypeEnum;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;

public class CarnetAnalyticsInfoFiller {
	
	private CarnetSessionContext ctx;
	private int step;
	private Calendar now;

	public CarnetAnalyticsInfoFiller(CarnetSessionContext ctx, int step) {
		this.ctx = ctx;
		this.step = step;
		initFormats();
	}
	
	private void initFormats() {
		now = Calendar.getInstance();
		now.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
		
	}
	
	public CarnetAnalyticsInfo fillInfo(){
		CarnetAnalyticsInfo info = new CarnetAnalyticsInfo();
		switch(step){
		case 3:
			if(ctx.infoCarnet != null){
				info.carnetRevenue = ctx.infoCarnet.getTotalFare();
				info.carnetCode = ctx.infoCarnet.getCarnetCode();
			}
			else{
				info.carnetRevenue = BigDecimal.ZERO;
				info.carnetCode = "";
			}
			info.ordDate = now;
			if(ctx.paymentData != null){
				info.carnetPaymentType = ctx.paymentData.getType().value();
				if(ctx.paymentData.getType().equals(CarnetPaymentTypeEnum.CREDIT_CARD)){
					CarnetCreditCardProviderData ccprov 
						= (CarnetCreditCardProviderData) ctx.paymentData.getProvider();
					info.carnetCCType = ccprov.getType().value();
				}
				else{
					info.carnetCCType = "";
				}
			}
		case 1:
		case 2:
			info.step = step;
			if(ctx.selectedCarnet != null){
				info.carnetType = ctx.selectedCarnet.getQuantity();
			}
			else{
				info.carnetType = 0;
			}
		}
		return info;
	}

}
