package com.alitalia.aem.consumer.mmb.exception;

public class MmbModifyPassengerDataException extends Exception {

	private static final long serialVersionUID = -8388948758281010112L;

	public MmbModifyPassengerDataException(String message) {
		super(message);
	}
	
	public MmbModifyPassengerDataException(Throwable cause) {
		super(cause);
	}
	
}
