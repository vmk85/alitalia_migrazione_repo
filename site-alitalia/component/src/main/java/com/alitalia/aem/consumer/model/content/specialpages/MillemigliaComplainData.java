package com.alitalia.aem.consumer.model.content.specialpages;

public class MillemigliaComplainData {
	public final static String NAME="MillemigliaComplainData";
	
	
	private String name;
	private String surname;
	private String nation;
	private String city;
	private String address;

	private String postalCode;
	private String email;
	private String officeNumber;
	private String houseNumber;
	private String phoneNumber;
	private String millemiglia;
	private String millemigliaProfile;
	private String flight;
	private String homeNumber;
	private Integer day;
	private Integer month;
	private Integer year;
	private String classe;
	private String seat;
	private String ticketNumber;
	private String pnr;
	private String pir;
	private String note;

	private String permission;
	private String accepted;
	private String error;
	private  MillemigliaComplainReasonsData complainReasons; 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getNation() {
		return nation;
	}
	public void setNation(String nation) {
		this.nation = nation;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOfficeNumber() {
		return officeNumber;
	}
	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getMillemiglia() {
		return millemiglia;
	}
	public void setMillemiglia(String millemiglia) {
		this.millemiglia = millemiglia;
	}
	public String getMillemigliaProfile() {
		return millemigliaProfile;
	}
	public void setMillemigliaProfile(String millemigliaProfile) {
		this.millemigliaProfile = millemigliaProfile;
	}
	public String getFlight() {
		return flight;
	}
	public void setFlight(String flight) {
		this.flight = flight;
	}
	public Integer getDay() {
		return day;
	}
	public void setDay(Integer day) {
		this.day = day;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public String getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(String ticket) {
		this.ticketNumber = ticket;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public String getPir() {
		return pir;
	}
	public void setPir(String pir) {
		this.pir = pir;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}

	public String getPermission() {
		return permission;
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}
	public String getAccepted() {
		return accepted;
	}
	public void setAccepted(String accepted) {
		this.accepted = accepted;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public MillemigliaComplainReasonsData getComplainReasons(){
		return this.complainReasons;
	}
	public void setReasons( MillemigliaComplainReasonsData reasons){
		this.complainReasons=reasons;
	}
	public String getHomeNumber() {
		return homeNumber;
	}
	public void setHomeNumber(String homeNumber) {
		this.homeNumber = homeNumber;
	}
	
}
