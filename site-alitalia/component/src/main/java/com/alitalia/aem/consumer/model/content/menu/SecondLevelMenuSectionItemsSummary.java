package com.alitalia.aem.consumer.model.content.menu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;

@Model(adaptables = { SlingHttpServletRequest.class })
public class SecondLevelMenuSectionItemsSummary {

	private Logger logger =
			LoggerFactory.getLogger(SecondLevelMenuSectionItemsSummary.class);

	@Self
	private SlingHttpServletRequest request;

	private MenuItem[] sectionItems;

	private static String TEMPLATE_NAME =
			"/apps/alitalia/templates/redazionale-principale-2-colonne";
	
	/**
	 * This method is used to get all the first and second level children of
	 * current page.
	 */
	@PostConstruct
	protected void initModel() {
		logger.debug("[SecondLevelMenuSectionItemsSummary] initModel");
		
		PageFilter pageFilter = new PageFilter(request);

		// risalgo i parent della resource fino a raggiungere la prima pagina
		// (che e' la pagina corrente)
		Resource parentResource = request.getResource();
		ResourceResolver resolver = parentResource.getResourceResolver();
		while (parentResource.adaptTo(Page.class) == null) {
			parentResource = parentResource.getParent();
		}

		List<MenuItem> thirdLevelPageList = new ArrayList<MenuItem>();
		Iterator<Page> thirdLevelPageIterator =
				parentResource.adaptTo(Page.class).listChildren(pageFilter);
		while (thirdLevelPageIterator.hasNext()) {
			
			Page thirdLevelPage = thirdLevelPageIterator.next();

			// Recupero il path corretto del componente della pagina di terzo
			// livello
			String thirdLevelPagePath = thirdLevelPage.getPath()
					+ "/jcr:content";
			
			if (thirdLevelPage.getProperties().get("cq:template", "")
					.equals(TEMPLATE_NAME)) {
				MenuItem thirdLevelPageItem = new MenuItem(
						thirdLevelPage.getTitle(), thirdLevelPage.getPath(),
						thirdLevelPage.getDescription(), thirdLevelPagePath,
						MenuItemType.LEAF, resolver.map(thirdLevelPage.getPath()) );
				thirdLevelPageList.add(thirdLevelPageItem);
			}
			
			sectionItems = (MenuItem[]) thirdLevelPageList.toArray(
					new MenuItem[thirdLevelPageList.size()]);
		}
	}

	/**
	 * Returns all the first and second level children of current page
	 * 
	 * @return The menuItems
	 */
	public MenuItem[] getSectionItems() {
		return sectionItems;
	}

}
