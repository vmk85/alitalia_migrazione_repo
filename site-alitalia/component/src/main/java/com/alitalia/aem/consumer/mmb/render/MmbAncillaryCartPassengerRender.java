package com.alitalia.aem.consumer.mmb.render;

import java.util.ArrayList;
import java.util.List;

public class MmbAncillaryCartPassengerRender {

	private Integer id;
	private String fullName = "";
	private String ticketNumber = "";
	private Integer quantity;
	
	private List<MmbAncillaryCartRender> seatItemList = null;
	private MmbPriceRender seatTotalPrice;
	private MmbPriceRender paymentSeatTotalPrice;
	private Integer seatTotalQuantity;
	
	private List<MmbAncillaryCartRender> extraBaggageItemList = null;
	private MmbPriceRender extraBaggageTotalPrice;
	private Integer extraBaggageTotalQuantity;
	
	private List<MmbAncillaryCartRender> fastTrackItemList = null;
	private MmbPriceRender fastTrackTotalPrice;
	private Integer fastTrackTotalQuantity;
	
	private List<MmbAncillaryCartRender> mealItemList = null;
	private MmbPriceRender mealTotalPrice;
	private Integer mealTotalQuantity;
	
	private List<MmbAncillaryCartRender> loungeItemList = null;
	private MmbPriceRender loungeTotalPrice;
	private Integer loungeTotalQuantity;
	
	private List<MmbAncillaryCartRender> upgradeItemList = null;
	private MmbPriceRender upgradeTotalPrice;
	private Integer upgradeTotalQuantity;
	
	public MmbAncillaryCartPassengerRender() {
		super();
		this.seatItemList = new ArrayList<MmbAncillaryCartRender>();
		this.extraBaggageItemList = new ArrayList<MmbAncillaryCartRender>();
		this.fastTrackItemList = new ArrayList<MmbAncillaryCartRender>();
		this.mealItemList = new ArrayList<MmbAncillaryCartRender>();
		this.loungeItemList = new ArrayList<MmbAncillaryCartRender>();
		this.upgradeItemList = new ArrayList<MmbAncillaryCartRender>();
		this.seatTotalQuantity = 0;
		this.extraBaggageTotalQuantity = 0;
		this.fastTrackTotalQuantity = 0;
		this.loungeTotalQuantity = 0;
		this.mealTotalQuantity = 0;
		this.upgradeTotalQuantity = 0;
		this.quantity = 0;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/* Seat */
	public List<MmbAncillaryCartRender> getSeatItemList() {
		return seatItemList;
	}

	public MmbPriceRender getSeatTotalPrice() {
		return seatTotalPrice;
	}

	public void setSeatTotalPrice(MmbPriceRender seatTotalPrice) {
		this.seatTotalPrice = seatTotalPrice;
	}
	
	public MmbPriceRender getPaymentSeatTotalPrice() {
		return paymentSeatTotalPrice;
	}

	public void setPaymentSeatTotalPrice(MmbPriceRender paymentSeatTotalPrice) {
		this.paymentSeatTotalPrice = paymentSeatTotalPrice;
	}

	public Integer getSeatTotalQuantity() {
		return seatTotalQuantity;
	}

	public void setSeatTotalQuantity(Integer seatTotalQuantity) {
		this.seatTotalQuantity = seatTotalQuantity;
	}

	/* Extra Baggage */
	public List<MmbAncillaryCartRender> getExtraBaggageItemList() {
		return extraBaggageItemList;
	}

	public MmbPriceRender getExtraBaggageTotalPrice() {
		return extraBaggageTotalPrice;
	}

	public void setExtraBaggageTotalPrice(MmbPriceRender extraBaggageTotalPrice) {
		this.extraBaggageTotalPrice = extraBaggageTotalPrice;
	}

	/* Fast Track */
	public List<MmbAncillaryCartRender> getFastTrackItemList() {
		return fastTrackItemList;
	}

	public MmbPriceRender getFastTrackTotalPrice() {
		return fastTrackTotalPrice;
	}

	public void setFastTrackTotalPrice(MmbPriceRender fastTrackTotalPrice) {
		this.fastTrackTotalPrice = fastTrackTotalPrice;
	}

	/* Meal */
	public List<MmbAncillaryCartRender> getMealItemList() {
		return mealItemList;
	}
	
	public MmbPriceRender getMealTotalPrice() {
		return mealTotalPrice;
	}

	public void setMealTotalPrice(MmbPriceRender mealTotalPrice) {
		this.mealTotalPrice = mealTotalPrice;
	}

	public Integer getMealTotalQuantity() {
		return mealTotalQuantity;
	}

	public void setMealTotalQuantity(Integer mealTotalQuantity) {
		this.mealTotalQuantity = mealTotalQuantity;
	}

	/* Lounge */
	public List<MmbAncillaryCartRender> getLoungeItemList() {
		return loungeItemList;
	}
	
	public MmbPriceRender getLoungeTotalPrice() {
		return loungeTotalPrice;
	}

	public void setLoungeTotalPrice(MmbPriceRender loungeTotalPrice) {
		this.loungeTotalPrice = loungeTotalPrice;
	}
	
	/*Upgrade*/
	public List<MmbAncillaryCartRender> getUpgradeItemList() {
		return upgradeItemList;
	}

	public MmbPriceRender getUpgradeTotalPrice() {
		return upgradeTotalPrice;
	}

	public Integer getUpgradeTotalQuantity() {
		return upgradeTotalQuantity;
	}

	public void setUpgradeTotalPrice(MmbPriceRender upgradeTotalPrice) {
		this.upgradeTotalPrice = upgradeTotalPrice;
	}

	public void setUpgradeTotalQuantity(Integer upgradeTotalQuantity) {
		this.upgradeTotalQuantity = upgradeTotalQuantity;
	}

	public Integer getExtraBaggageTotalQuantity() {
		return extraBaggageTotalQuantity;
	}

	public void setExtraBaggageTotalQuantity(Integer extraBaggageTotalQuantity) {
		this.extraBaggageTotalQuantity = extraBaggageTotalQuantity;
	}

	public Integer getFastTrackTotalQuantity() {
		return fastTrackTotalQuantity;
	}

	public void setFastTrackTotalQuantity(Integer fastTrackTotalQuantity) {
		this.fastTrackTotalQuantity = fastTrackTotalQuantity;
	}

	public Integer getLoungeTotalQuantity() {
		return loungeTotalQuantity;
	}

	public void setLoungeTotalQuantity(Integer loungeTotalQuantity) {
		this.loungeTotalQuantity = loungeTotalQuantity;
	}

}
