package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Sabre Request Model for OldAncillary
*/
public class OldAncillary
{
	private String code;
	public final String getcode()
	{
		return code;
	}
	public final void setcode(String value)
	{
		code = value;
	}
	private TravelPart travelPart;
	public final TravelPart gettravelPart()
	{
		return travelPart;
	}
	public final void settravelPart(TravelPart value)
	{
		travelPart = value;
	}
	private AncillaryPrice ancillaryPrice;
	public final AncillaryPrice getancillaryPrice()
	{
		return ancillaryPrice;
	}
	public final void setancillaryPrice(AncillaryPrice value)
	{
		ancillaryPrice = value;
	}
	private int quantity;
	public final int getquantity()
	{
		return quantity;
	}
	public final void setquantity(int value)
	{
		quantity = value;
	}
	private boolean refundIndicator;
	public final boolean getrefundIndicator()
	{
		return refundIndicator;
	}
	public final void setrefundIndicator(boolean value)
	{
		refundIndicator = value;
	}
	private boolean availabilityIndicator;
	public final boolean getavailabilityIndicator()
	{
		return availabilityIndicator;
	}
	public final void setavailabilityIndicator(boolean value)
	{
		availabilityIndicator = value;
	}
}