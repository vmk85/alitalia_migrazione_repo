package com.alitalia.aem.consumer.mmb.render;

import java.util.ArrayList;
import java.util.List;

public class MmbTaxDataListRender {

	private List<MmbTaxDataRender> taxDataRenderList;
	private MmbPriceRender totalAmountRender;
	
	public MmbTaxDataListRender() {
		this.taxDataRenderList = new ArrayList<MmbTaxDataRender>();
	}
	
	public List<MmbTaxDataRender> getTaxDataRenderList() {
		return taxDataRenderList;
	}

	public MmbPriceRender getTotalAmountRender() {
		return totalAmountRender;
	}

	public void setTotalAmountRender(MmbPriceRender totalAmountRender) {
		this.totalAmountRender = totalAmountRender;
	}
	
}
