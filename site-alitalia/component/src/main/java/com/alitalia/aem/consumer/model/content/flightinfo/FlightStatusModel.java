package com.alitalia.aem.consumer.model.content.flightinfo;

import javax.annotation.PostConstruct;



import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class FlightStatusModel extends GenericBaseModel {
	@Self
	private SlingHttpServletRequest slingHttpServletRequest;
	

	private FlightStatusData flightStatusData;
	
	
	
	@PostConstruct
	protected void initModel() {
		logger.debug("[FlightStatusModel] initModel");
		
		try {
			super.initBaseModel(slingHttpServletRequest);
			
			

			flightStatusData = (FlightStatusData) slingHttpServletRequest.getSession()
					.getAttribute(FlightStatusData.NAME);
			slingHttpServletRequest.getSession().removeAttribute(FlightStatusData.NAME);
		}
		
		catch (Exception e ) {
			logger.debug("[FlightStatusModel] error : " + e.toString());
		}
		

	
	}

	public FlightStatusData getFlightStatusData() {
		return flightStatusData;
	}
	
	
	
}
