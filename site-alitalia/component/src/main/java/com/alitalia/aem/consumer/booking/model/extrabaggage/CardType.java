package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Enumerazione per le tipologie di carte di credito disponibili
*/
public enum CardType
{
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("Visa")] Visa,
	Visa,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("MasterCard")] MasterCard,
	MasterCard,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("Diners")] Diners,
	Diners,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("AmericanExpress")] AmericanExpress,
	AmericanExpress,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("Uatp")] Uatp,
	Uatp,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("VisaElectron")] VisaElectron,
	VisaElectron,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("JCB")] JCB,
	JCB,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("Discover")] Discover,
	Discover,
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [Description("Maestro")] Maestro
	Maestro;

	public static final int SIZE = java.lang.Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static CardType forValue(int value)
	{
		return values()[value];
	}
}