package com.alitalia.aem.consumer.carnet;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.SlingRequestProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.StateData;
import com.alitalia.aem.common.data.home.UserInfoCompleteData;
import com.alitalia.aem.common.data.home.UserInfoStandardData;
import com.alitalia.aem.common.data.home.carnet.CarnetBuyerData;
import com.alitalia.aem.common.data.home.carnet.CarnetComunicationCreditCardData;
import com.alitalia.aem.common.data.home.carnet.CarnetCreditCardProviderData;
import com.alitalia.aem.common.data.home.carnet.CarnetData;
import com.alitalia.aem.common.data.home.carnet.CarnetInfoCarnet;
import com.alitalia.aem.common.data.home.carnet.CarnetPaymentData;
import com.alitalia.aem.common.data.home.carnet.CarnetPrenotationData;
import com.alitalia.aem.common.data.home.carnet.CarnetProcessInfoData;
import com.alitalia.aem.common.data.home.enumerations.CarnetPaymentStepEnum;
import com.alitalia.aem.common.data.home.enumerations.CarnetPaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.CarnetResultTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.messages.home.CarnetBuyCarnetRequest;
import com.alitalia.aem.common.messages.home.CarnetBuyCarnetResponse;
import com.alitalia.aem.common.messages.home.CarnetGetListRequest;
import com.alitalia.aem.common.messages.home.CarnetGetListResponse;
import com.alitalia.aem.common.messages.home.CarnetLoginRequest;
import com.alitalia.aem.common.messages.home.CarnetLoginResponse;
import com.alitalia.aem.common.messages.home.CarnetPaymentTypeResponse;
import com.alitalia.aem.common.messages.home.CarnetSendEmailRequest;
import com.alitalia.aem.common.messages.home.CarnetSendEmailResponse;
import com.alitalia.aem.common.messages.home.CarnetStaticDataRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.common.messages.home.RetrieveStateListRequest;
import com.alitalia.aem.common.messages.home.RetrieveStateListResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.CreditCardUtils;
import com.alitalia.aem.web.component.carnet.delegate.CarnetDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.i18n.I18n;

/**
 * The <code>CarnetSession</code> component tracks the behaviour of a Carnet
 * session and the related data through specialized methods.
 * 
 * <p>
 * Since the SCR component is stateless, the state of the single Carnet Session
 * is tracked in an external object <code>CarnetSessionContext</code>.
 * </p>
 * 
 * @see CarnetSessionContext
 */

@Component(immediate = true)
@Service(value = CarnetSession.class)
public class CarnetSession {

	private static Logger logger = LoggerFactory.getLogger(CarnetSession.class);
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile CarnetDelegate carnetDelegate;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegate staticDataDelegate;
	
	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private RequestResponseFactory requestResponseFactory;

	@Reference
	private SlingRequestProcessor requestProcessor;
	
	/**
	 * Constructor.
	 */
	public CarnetSession() {
		super();
	}
	
	
	/* interface methods */
	
	/**
	 * Initialize the Carnet session object, setting it ready to 
	 * start a new process for a given PNR number.
	 * 
	 * <p>This must be called when activating the CheckIn detail function
	 * on a prenotation, based on the provided tripId and
	 * additional environment data.</p>
	 * 
	 * @param i18n The i18n instance to be used for translations.
	 * @param serviceClient A string identifying the client when requested by
	 * 						remote services.
	 * @param machineName A string identifying the calling machine name.
	 * @param sid The session identifier, used for tracking purposes.
	 * @param callerIp The IP address when requested by remote services.
	 * @param market Market code to be used when requested by remote services.
	 * @param site Site code to be used when requested by remote services.
	 * @param currencyCode The code of the currency.
	 * @param currencySymbol The symbol of the currency, encoded as HTML entity.
	 * @param locale The locale to use for the session.
	 */
	public CarnetSessionContext initializeSession(SlingHttpServletRequest request, I18n i18n,
			String sid, String market, String currencyCode, String currencySymbol,
			Locale locale, String callerIp, NumberFormat currentNumberFormat){
		logger.debug("carnet - initializeSession");
		
		CarnetSessionContext ctx;
		
		// create a new CheckIn session
		ctx = new CarnetSessionContext();
		ctx.i18n = i18n;
		ctx.sid = sid;
		ctx.callerIp = callerIp;
				
		if ("en".equalsIgnoreCase(market) && "en".equals(locale.getLanguage())) {
			// mercato internazionale per Sabre
			ctx.market = "FI";
		} else {
			ctx.market = "gb".equalsIgnoreCase(market) || "en".equalsIgnoreCase(market) ? market.toUpperCase() : market;
		}
		
		ctx.currencyCode = currencyCode;
		ctx.currencySymbol = currencySymbol;
		ctx.locale = locale;
		ctx.currentNumberFormat = currentNumberFormat;
		ctx.phase = CarnetStepEnum.CHOOSE;
		String protocol = request.getScheme();
		String serverName = request.getServerName();
//		int port = request.getServerPort();

//		if (port != -1) {
//			ctx.domain = protocol + "://" + serverName + ":" + port;
//		} else {
			ctx.domain = protocol + "://" + serverName;
//		}
		return ctx;
	}
	
	public List<CarnetData> retrieveCarnetList(CarnetSessionContext ctx){
		CarnetGetListRequest requestListCarnet = new CarnetGetListRequest();
		requestListCarnet.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		requestListCarnet.setMarketCode(ctx.market);
		
		CarnetGetListResponse responseListCarnet = carnetDelegate.getCarnetList(requestListCarnet);
		List<CarnetData> listCarnet = new ArrayList<CarnetData>();
		if(responseListCarnet != null){
			 listCarnet = responseListCarnet.getCarnets();	
		}
		
		ctx.availableCarnet = listCarnet;
		
		return listCarnet;
	}
	
	/**
	 * TODO It perform the selection of the carnet and load the static data
	 * for the next steps.
	 * @param ctx
	 * @param carnetId
	 * @return
	 */
	public boolean selectCarnet(CarnetSessionContext ctx, String carnetId){
		boolean result = false;
		loadStaticData(ctx);
		ctx.phase = CarnetStepEnum.PERSONAL_INFO;
		
		if(ctx.availableCarnet != null){
			for(CarnetData carnet : ctx.availableCarnet){
				if(carnetId.equals(carnet.getCode())){
					ctx.selectedCarnet = carnet;
					ctx.carnetTotalAmount = carnet.getPrice();
					result = true;
				}
			}
		}
		
		return result;
	}
	
	/**
	 * It saves the user info in the session.
	 * @param ctx
	 * @param email
	 * @param lastName
	 * @param name
	 * @param mobileNumber
	 * @param address
	 * @param city must be the description value
	 * @param country must be the description value
	 * @param businessName
	 * @param fax
	 * @param stateCode
	 * @param homeNumber
	 * @param vatNumber
	 * @param zipCode
	 */
	public void insertBuyerData(CarnetSessionContext ctx, String email, String lastName,
			String name, String mobileNumber, String address, String city, String country,
			String ragioneSociale, String fax, String stateCode, String homeNumber, 
			String iva, String zipCode, String codiceFiscale, boolean enableNewsletter){
		
		CarnetBuyerData customerInfo = new CarnetBuyerData();
		customerInfo.setAddress(address);
		customerInfo.setBusinessName(ragioneSociale);
		customerInfo.setCity(city);
		customerInfo.setCountryCode(country);
		customerInfo.setEmail(email);
		customerInfo.setFax(fax);
		customerInfo.setLastName(lastName);
		customerInfo.setMobileNumber(mobileNumber);
		customerInfo.setName(name);
		customerInfo.setPrivacyFlag(enableNewsletter);
		customerInfo.setStateCode(stateCode);
		customerInfo.setTelephoneNumber(homeNumber);
		customerInfo.setNationInsuranceNumber(codiceFiscale);
		customerInfo.setVatNumber(iva);
		customerInfo.setZipCode(zipCode);
		ctx.confirmationMailAddress = email;
		ctx.customerInfo = customerInfo;
		ctx.enableNewsletter = enableNewsletter;
		ctx.phase = CarnetStepEnum.PAYMENT;
	}
	
	
	/**
	 * It perform payment of a selectedCarnet by using use the payment info 
	 * retrieved by the form and the buyer info to perform payment
	 * @param ctx
	 * @return true if the payment is success, otherwise false.
	 */
	public boolean performPaymentCarnet(CarnetSessionContext ctx, String cardNumber, 
			String carnetCreditCardType, String cvv, Short expiryMonth, Short expiryYear, String ownerName, 
			String ownerLastName, String indirizzo, String cap, String citta, String paese, String countriesUSA, 
			String userAgent, String errorUrl, String returnUrl){
		
		logger.debug("performPaymentCarnet");
		
		CarnetBuyCarnetRequest buyCarnetRequest = new CarnetBuyCarnetRequest(IDFactory.getTid(), ctx.sid);
		
		CarnetPrenotationData prenotationData = new CarnetPrenotationData();
		CarnetPaymentData paymentData = new CarnetPaymentData();
		CarnetCreditCardProviderData providerData = new CarnetCreditCardProviderData();
		CarnetComunicationCreditCardData comunicationData = new CarnetComunicationCreditCardData();
		
		//populate comunication object
		comunicationData.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
		comunicationData.setReturnUrl(returnUrl);
		comunicationData.setErrorUrl(errorUrl);
		comunicationData.setAcceptHeader("*/*");
		comunicationData.setIpAddress(ctx.callerIp);
		comunicationData.setUserAgent(userAgent);
		
		CarnetCreditCardTypeEnum carnetCredicCardType = 
				CarnetCreditCardTypeEnum.fromValue(carnetCreditCardType);

		CreditCardTypeEnum creditCardType = null;
		
		switch(carnetCredicCardType) {
			case AMERICAN_EXPRESS:
				creditCardType = CreditCardTypeEnum.AMERICAN_EXPRESS;
				break;
			case DINERS:
				creditCardType = CreditCardTypeEnum.DINERS;
				break;
			case MASTER_CARD:
				creditCardType = CreditCardTypeEnum.MASTER_CARD;
				break;
			case UATP:
				creditCardType = CreditCardTypeEnum.UATP;
				break;
			case VISA:
				creditCardType = CreditCardTypeEnum.VISA;
				break;
			case VISA_ELECTRON:
				creditCardType = CreditCardTypeEnum.VISA_ELECTRON;
				break;
			default:
				logger.error("Error during conversion carnetCreditCardType: {}", carnetCredicCardType);
				throw new IllegalArgumentException("Credit card conversion impossible for carnetCreditCardType: " + carnetCredicCardType);
		
		}
		
		providerData.setComunication(comunicationData);
		providerData.setCvv(cvv);
		providerData.setCreditCardNumber(cardNumber);
		providerData.setExpiryMonth(expiryMonth);
		providerData.setExpiryYear(expiryYear);
		providerData.setIs3DSecure(Boolean.FALSE);
		providerData.setType(creditCardType);
		
		if (!CreditCardTypeEnum.AMERICAN_EXPRESS.equals(creditCardType)) {
			UserInfoStandardData userInfoStandardData = new UserInfoStandardData();
			userInfoStandardData.setLastname(ownerLastName);
			userInfoStandardData.setName(ownerName);
			providerData.setUserInfo(userInfoStandardData);
		} else {
			UserInfoCompleteData userInfoCompleteData = new UserInfoCompleteData();
			userInfoCompleteData.setAdress(indirizzo);
			userInfoCompleteData.setCap(cap);
			userInfoCompleteData.setCity(citta);
			userInfoCompleteData.setCountry(paese);
			userInfoCompleteData.setLastname(ownerLastName);
			userInfoCompleteData.setName(ownerName);
			userInfoCompleteData.setState(countriesUSA);
			providerData.setUserInfo(userInfoCompleteData);
		}
		
		paymentData.setProvider(providerData);
		
		CarnetProcessInfoData processData = new CarnetProcessInfoData();
		
		processData.setMarketCode(ctx.market);
		processData.setPaymentAttemps(0);
		processData.setPointOfSale(null);
		processData.setRedirectUrl(null);
		processData.setResult(CarnetResultTypeEnum.OK);
		processData.setSessionId(ctx.sid);
		processData.setShopId("WB");
		processData.setSiteCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
		processData.setStep(CarnetPaymentStepEnum.INITIALIZE);
		processData.setTokenId(null);
		processData.setTransactionId(null);
		
		paymentData.setProcess(processData);
		
		paymentData.setCurrency(ctx.currencyCode);
		String description = "Cart containing {Carnet " + ctx.selectedCarnet.getQuantity() + " tickets}";
		paymentData.setDescription(description);
		paymentData.setEmail(ctx.customerInfo.getEmail());
		paymentData.setEnabledNewsLetter(ctx.enableNewsletter);
		paymentData.setGrossAmount(ctx.carnetTotalAmount);
		paymentData.setNetAmount(ctx.carnetTotalAmount);
		paymentData.setType(CarnetPaymentTypeEnum.CREDIT_CARD);
		paymentData.setXsltMail(null);
		
		prenotationData.setPayment(paymentData);
		
		prenotationData.setBuyer(ctx.customerInfo);
		prenotationData.setCarnet(ctx.selectedCarnet);

		buyCarnetRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		buyCarnetRequest.setMarketCode(ctx.market);
		buyCarnetRequest.setPrenotation(prenotationData);
		
		boolean paymentPerformed = false;
		try {
			CarnetBuyCarnetResponse buyCarnetResponse = carnetDelegate.buyCarnet(buyCarnetRequest);
			CarnetInfoCarnet infoCarnet = buyCarnetResponse.getInfoCarnet();
			
			if (infoCarnet != null && infoCarnet.getCarnetCode() != null && infoCarnet.getPassword() != null){
				ctx.infoCarnet = infoCarnet;
				providerData.setCreditCardNumber(CreditCardUtils.obfuscateCardNumber(cardNumber));
				paymentData.setProvider(providerData);
				ctx.paymentData = paymentData;
				paymentPerformed = true;
				ctx.phase = CarnetStepEnum.DONE;
				logger.debug("Carnet Payment Information received correctly!");
			} else {
				logger.error("No carnet payment information received");
			}
		} catch (Exception e){
			logger.error("Error during performing Carnet payment ", e);
		}	
		return paymentPerformed;
	}
	

	public boolean sendEmail(CarnetSessionContext ctx, Resource resource, String emailBodyTemplate,
			String[] placeholders, String[] values) {
		
		// email body
		String emailBody = CarnetUtils.getMailBody(resource, emailBodyTemplate,
				true, resolverFactory, requestResponseFactory, requestProcessor);
		
		emailBody = AlitaliaUtils.replacePlaceholderBodyMail(emailBody,
				placeholders, values);
		
		CarnetSendEmailRequest carnetSendEmailRequest = new CarnetSendEmailRequest(IDFactory.getTid(), ctx.sid);
		
		carnetSendEmailRequest.setMailTo(ctx.confirmationMailAddress);
		carnetSendEmailRequest.setMailHtml(emailBody);
		CarnetSendEmailResponse carnetSendEmailResponse = null;
		try {
			carnetSendEmailResponse = carnetDelegate.sendEmail(carnetSendEmailRequest);
		} catch (Exception e){
			logger.error("Error during sending mail after Carnet paymnet.");
		}
		
		return (carnetSendEmailResponse != null && carnetSendEmailResponse.getHaveSendEmail() != null) ? carnetSendEmailResponse.getHaveSendEmail() : false;			
	}
	
	
	//Private methods
	
	/**
	 * It prepare the payment step, by retriving all static data information
	 * to be shown in the payment form.
	 * @param ctx
	 */
	private void loadStaticData(CarnetSessionContext ctx) {
		
		String tid = IDFactory.getTid();
		
		//elenco credit cards
		CarnetStaticDataRequest getPaymentListRequest = new CarnetStaticDataRequest();
		getPaymentListRequest.setSid(ctx.sid);
		getPaymentListRequest.setTid(tid);
		getPaymentListRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()));
		getPaymentListRequest.setMarket(ctx.market);
		
		CarnetPaymentTypeResponse getPaymentListResponse =
				carnetDelegate.getPaymentList(getPaymentListRequest);
		
		if (getPaymentListResponse != null) {
			ctx.creditCards = getPaymentListResponse.getPaymentTypeItem();
		} else {
			logger.error("The getPaymentList operation returned a null response");
		}
		
		// elenco countries
		RetrieveCountriesResponse countriesResponse = null;
		try {
			RetrieveCountriesRequest countriesRequest = new RetrieveCountriesRequest(ctx.sid, tid);
			countriesRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
			countriesRequest.setMarket(ctx.market);
			countriesResponse = staticDataDelegate.retrieveCountries(countriesRequest);
		} catch (Exception e) {
			logger.error("Error by retriving countries", e);
		}
		
		ctx.countries = countriesResponse != null ? countriesResponse.getCountries() : new ArrayList<CountryData>();

		for (CountryData countryData : ctx.countries) {
			countryData.setDescription("countryData." + countryData.getCode());
		}
		
		// prepare provinces for Italy
		RetrieveProvincesResponse provinceResponse = null;
		try {
			RetrieveProvincesRequest provinceRequest = new RetrieveProvincesRequest(ctx.sid, tid);
			provinceRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
			provinceRequest.setMarket(ctx.market);
			provinceResponse = staticDataDelegate.retrieveProvinces(provinceRequest);
		} catch (Exception e) {
			logger.error("Error by retriving italian provinces", e);
		}
		
		ctx.provinces = provinceResponse != null ? provinceResponse.getProvinces() : new ArrayList<CountryData>();
		
		for (CountryData countryData : ctx.provinces) {
			countryData.setDescription("stateProvinceData.it." + countryData.getStateCode());
		}
			
		// elenco state for US country
		RetrieveStateListResponse stateUSListResponse = null;
		try {
			RetrieveStateListRequest stateUSListRequest = new RetrieveStateListRequest(ctx.sid, tid);
			stateUSListRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
			stateUSListRequest.setMarket(ctx.market);
			stateUSListRequest.setItemCache("US");
			stateUSListResponse = staticDataDelegate.retrieveStateList(stateUSListRequest);
		} catch (Exception e) {
			logger.error("Error by retriving american states", e);
		}
		
		ctx.countriesUSA = stateUSListResponse != null ? stateUSListResponse.getStates() : new ArrayList<StateData>();

		for (StateData stateData : ctx.countriesUSA) {
			stateData.setStateDescription("stateProvinceData.us." + stateData.getStateCode());
		}
		
		// elenco state for CA country
		RetrieveStateListResponse stateCAListResponse = null;
		try {
			RetrieveStateListRequest stateCAListRequest = new RetrieveStateListRequest(ctx.sid, tid);
			stateCAListRequest.setLanguageCode(AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase());
			stateCAListRequest.setMarket(ctx.market);
			stateCAListRequest.setItemCache("CA");
			stateCAListResponse = staticDataDelegate.retrieveStateList(stateCAListRequest);
		} catch (Exception e) {
			logger.error("Error by retriving american states", e);
		}
				
		ctx.countriesCAN = stateCAListResponse != null ? stateCAListResponse.getStates() : new ArrayList<StateData>();
		
		for (StateData stateData : ctx.countriesCAN) {
			stateData.setStateDescription("stateProvinceData.ca." + stateData.getStateCode());
		}
	}
	
	
	/**
	 * It set if the user enable to receive newsletter
	 * @param ctx
	 * @param enableNewsletter
	 * @return void.
	 */
	public void setNewsletter(CarnetSessionContext ctx, boolean enableNewsletter){
		
	}


	/**
	 * It return the InfoCarnet of a current buy carnet or
	 * a carnet of a logged user
	 * @return
	 */
	public CarnetInfoCarnet getInfoUserLogged(CarnetSessionContext ctx) {
		if (!ctx.isLogged) {
			logger.error("Carnet user not logged");
			return null;
		}
		if (ctx == null || ctx.infoCarnet == null) {
			logger.error("no carnet of a user logged found");
			return null;
		}
		return ctx.infoCarnet;
	}
	
	public boolean checkCarnetValidity(BookingSessionContext ctx, CarnetInfoCarnet carnetInfo, int nPax){
		String code = carnetInfo.getCarnetCode();
		String pwd = carnetInfo.getPassword();
		if(code != null && !"".equals(code) && pwd != null && !"".equals(pwd)){
			CarnetLoginRequest loginRequest = new CarnetLoginRequest(IDFactory.getTid(), ctx.sid);
			loginRequest.setCarnetCode(code);
			loginRequest.setPassword(pwd);
			CarnetLoginResponse loginResponse = carnetDelegate.loginCarnet(loginRequest);
			if (loginResponse != null && loginResponse.getInfoCarnet() != null) {
				CarnetInfoCarnet infoCarnet = loginResponse.getInfoCarnet();
				Calendar calToday = Calendar.getInstance(infoCarnet.getExpiryDate().getTimeZone());
				if(infoCarnet != null){
					ctx.infoCarnet = infoCarnet;
					if(infoCarnet.getResidualRoutes() <= 0 || infoCarnet.getExpiryDate().getTime().before(calToday.getTime()) || nPax > infoCarnet.getResidualRoutes()){
						return false;
					} else{
						return true;
					}
				}
			} else {
				logger.debug("Response of login is null for user {}, and password {}", code, pwd);
				return false;
			}
		}
		return false;
	}


	public boolean performLogin(CarnetSessionContext ctx, String codiceCarnet,
			String password) {
		
		CarnetLoginRequest loginRequest = new CarnetLoginRequest(IDFactory.getTid(), ctx.sid);
		loginRequest.setCarnetCode(codiceCarnet);
		loginRequest.setPassword(password);
		
		CarnetLoginResponse loginResponse = carnetDelegate.loginCarnet(loginRequest);
		if (loginResponse != null && loginResponse.getInfoCarnet() != null) {
			ctx.infoCarnet = loginResponse.getInfoCarnet();
			ctx.isLogged = true;
			return true;
		} else {
			logger.debug("Response of login is null for user {}, and password {}", codiceCarnet, password);
			return false;
		}
	}
	
}