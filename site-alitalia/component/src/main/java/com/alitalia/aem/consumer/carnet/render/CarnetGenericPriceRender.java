package com.alitalia.aem.consumer.carnet.render;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

import com.alitalia.aem.consumer.carnet.CarnetSessionContext;

public class CarnetGenericPriceRender {

	private NumberFormat numberFormat;
	
	public CarnetGenericPriceRender(CarnetSessionContext ctx) {
		init(ctx);
	}

	private void init(CarnetSessionContext ctx){
		this.numberFormat = ctx.currentNumberFormat;
		DecimalFormat format = new DecimalFormat("#0.00");
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setCurrencySymbol(""); 
		format.setDecimalFormatSymbols(otherSymbols);
		format.setGroupingUsed(false);
		format.setPositivePrefix("");
		format.setNegativePrefix("");
	}
	
	public String getFormattedPrice(BigDecimal price){
		if (price != null) {
			return numberFormat.format(price);
		} else {
			return "";
		}
	}
}

