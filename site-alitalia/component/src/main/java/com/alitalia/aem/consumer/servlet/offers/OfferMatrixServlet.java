package com.alitalia.aem.consumer.servlet.offers;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.FlexSearchData;
import com.alitalia.aem.common.data.home.FlexibleDatesMatrixData;
import com.alitalia.aem.common.data.home.PassengerNumbersData;
import com.alitalia.aem.common.data.home.SearchDestinationData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.ResidencyTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.TimeTypeEnum;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.SearchFlexibleDatesMatrixResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.common.messages.home.enumeration.SearchExecuteResponseType;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(name = "Alitalia Offer Matrix Servlet", metatype = false, configurationFactory = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "sling.servlet.selectors", value = "offermatrix"),
	@Property(name = "sling.servlet.extensions", value = {"json"}),
	@Property(name = "sling.servlet.methods", value = "GET")
})
@SuppressWarnings("serial")
public class OfferMatrixServlet extends SlingSafeMethodsServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private StaticDataDelegate staticDataDelegate;

	@Reference
	private SearchFlightsDelegate searchFlightsDelegate;

	@Reference
	private AlitaliaConfigurationHolder alitaliaConfiguration;
	
	@Override
	protected void doGet(SlingHttpServletRequest request, 
			SlingHttpServletResponse response) throws ServletException, IOException {

		TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");

		try {

			Locale locale = AlitaliaUtils.getRepositoryPathLocale(request.getResource());

			String departureCode = request.getParameter("from");
			String arrivalCode = request.getParameter("to");

			String isYoungString = request.getParameter("isYoung");
			boolean isYoung = false;
			if (isYoungString != null && !isYoungString.isEmpty()) {
				isYoung = isYoungString.equals("true");
			}

			if (departureCode == null || departureCode.isEmpty() || arrivalCode == null || arrivalCode.isEmpty()) {
				logger.warn("[OfferMatrixServlet] Aeroporto {} non specificato",
						departureCode == null && arrivalCode == null ? "partenza/arrivo" : (departureCode == null ? "partenza" : "arrivo"));
				out.object();
				out.key("result").value(false);
				out.endObject();
				return;
			}

			AirportData departure = null;
			AirportData arrival = null;

			RetrieveAirportsRequest airportsRequest = new RetrieveAirportsRequest(IDFactory.getTid(), IDFactory.getSid());

			airportsRequest.setMarket(locale.getCountry());
			airportsRequest.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()));
			RetrieveAirportsResponse airportsResponse = staticDataDelegate.retrieveAirports(airportsRequest);

			if (airportsResponse != null && airportsResponse.getAirports() != null && !airportsResponse.getAirports().isEmpty()) {
				for (AirportData airportData : airportsResponse.getAirports()) {
					if (airportData.getCode().equals(departureCode)) {
						departure = airportData;
					}
					if (airportData.getCode().equals(arrivalCode)) {
						arrival = airportData;
					}
				}
			}

			if (departure == null || arrival == null) {
				logger.warn("[OfferMatrixServlet] Aeroporto {} non valido", 
						departure == null && arrival == null ? departureCode + ", " + arrivalCode : (departure == null ? departureCode : arrivalCode));
				out.object();
				out.key("result").value(false);
				out.endObject();
				return;
			}

			SearchFlightSolutionRequest serviceRequest = new SearchFlightSolutionRequest(IDFactory.getTid(), IDFactory.getSid());

			serviceRequest.setMarket(locale.getCountry());

			// FIXME Utilizzare BookingSearchCUGEnum
			serviceRequest.setCug("ADT");

			FlexSearchData filter = new FlexSearchData();

			// FIXME Utilizzare BookingSearchCUGEnum
			filter.setCug("ADT");

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

			ArrayList<SearchDestinationData> destinations = new ArrayList<SearchDestinationData>();
			SearchDestinationData destinationOutbound = new SearchDestinationData();
			Calendar departureDate = Calendar.getInstance();
			departureDate.setTime(dateFormat.parse(request.getParameter("departureDate")));
			destinationOutbound.setDepartureDate(departureDate);
			destinationOutbound.setFromAirport(departure);
			destinationOutbound.setToAirport(arrival);
			destinationOutbound.setRouteType(RouteTypeEnum.OUTBOUND);
			destinationOutbound.setTimeType(TimeTypeEnum.ANYTIME);

			SearchDestinationData destinationReturn = new SearchDestinationData();
			Calendar returnDate = Calendar.getInstance();
			returnDate.setTime(dateFormat.parse(request.getParameter("returnDate")));
			destinationReturn.setDepartureDate(returnDate);
			destinationReturn.setFromAirport(arrival);
			destinationReturn.setToAirport(departure);
			destinationReturn.setRouteType(RouteTypeEnum.RETURN);
			destinationReturn.setTimeType(TimeTypeEnum.ANYTIME);

			destinations.add(destinationOutbound);
			destinations.add(destinationReturn);

			filter.setDestinations(destinations);
			filter.setMarket(locale.getCountry());
			filter.setOnlyDirectFlight(false);

			ArrayList<PassengerNumbersData> passengerNumbers = new ArrayList<PassengerNumbersData>();
			PassengerNumbersData passengerNumbersData = new PassengerNumbersData();
			passengerNumbersData.setNumber(1);
			PassengerTypeEnum passengerType = isYoung ? PassengerTypeEnum.YOUTH : PassengerTypeEnum.ADULT;
			passengerNumbersData.setPassengerType(passengerType);
			passengerNumbers.add(passengerNumbersData);
			filter.setPassengerNumbers(passengerNumbers);

			filter.setSearchCabin(CabinEnum.ECONOMY);
			filter.setSearchCabinType(CabinTypeEnum.PERMITTED);
			filter.setResidency(ResidencyTypeEnum.NONE);
			filter.setFlexSearchSet(true);
			filter.setType(SearchTypeEnum.FLEX_DATE_MATRIX_SEARCH);

			filter.setPlusMinusDays(3);

			serviceRequest.setFilter(filter);
			serviceRequest.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()));
			serviceRequest.setMarketExtraCharge(locale.getCountry());
			serviceRequest.setResponseType(SearchExecuteResponseType.MODEL);

			SearchFlexibleDatesMatrixResponse serviceResponse = searchFlightsDelegate.searchFlexibleDatesMatrixSolutions(serviceRequest);
			List<FlexibleDatesMatrixData> resultList = serviceResponse.getFlexibleDatesMatrixDataList();

			if (resultList != null && !resultList.isEmpty()) {

				out.object();
				out.key("result");

				out.object();

				out.key("matrixDeparture").value(request.getParameter("departureDate"));
				out.key("matrixReturn").value(request.getParameter("returnDate"));
				out.key("matrixData");
				out.object();

				//					double bestPrice = resultList.get(0).getAmount().doubleValue();
				//					String bestPriceKey = dateFormat.format(resultList.get(0).getDepartureDateTime().getTime()) + "_" + 
				//							dateFormat.format(resultList.get(0).getReturnDateTime().getTime());
				double bestPrice = Double.MAX_VALUE;
				String bestPriceKey = "";

				for (FlexibleDatesMatrixData result : resultList) {

					if(result.getAmount() != null){
						String key = dateFormat.format(result.getDepartureDateTime().getTime()) + "_" + 
								dateFormat.format(result.getReturnDateTime().getTime());

							out.key(key).value(AlitaliaUtils.round(result.getAmount().doubleValue()+AlitaliaUtils.getExtraFee(alitaliaConfiguration, locale.getCountry(), searchFlightsDelegate.isSliceSardiniaTerritorialContinuity(departureCode, arrivalCode), searchFlightsDelegate.isSliceSicilyTerritorialContinuity(departureCode, arrivalCode)), request.getResource()));

						if (result.getAmount().doubleValue() < bestPrice) {
							bestPrice = result.getAmount().doubleValue();
							bestPriceKey = key;
						}
					}
				}

				if(bestPrice == Double.MAX_VALUE){
					throw new RuntimeException("Empty offer");
				}

				out.endObject();

				out.key("bestPrice").value(bestPriceKey);
				out.endObject();
				out.endObject();

			} else {
				logger.error("[OfferMatrixServlet] No results");
				out.object();
				out.key("result").value(false);
				out.endObject();
			}

		} catch (Exception e) {
			logger.error("[OfferMatrixServlet] Error", e);
			try {
				out.object();
				out.key("result").value(false);
				out.endObject();
			} catch(Exception ex){
				logger.error("[OfferMatrixServlet] Error creating json", e);
			}
		}
	}
}
