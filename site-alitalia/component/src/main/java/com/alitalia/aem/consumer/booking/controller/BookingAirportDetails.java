package com.alitalia.aem.consumer.booking.controller;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AirportDetailsData;
import com.alitalia.aem.consumer.booking.render.AirportDetailsRender;
import com.alitalia.aem.consumer.global.i18n.I18nKeyBooking;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingAirportDetails extends GenericBaseModel{
	
	@Self
	private SlingHttpServletRequest request;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private AirportDetailsRender detail;
	
	private String cityName;
	
	private String airportName;
	
	@PostConstruct
	protected void initModel() throws Exception {
		try{
			super.initBaseModel(request);
		String airportCode = (String) request.getSession().getAttribute("airportCode");
		detail = renderAirportInfo((AirportDetailsData) 
				request.getSession().getAttribute("airportDetailsData"), airportCode);
		request.getSession().removeAttribute("airportDetailsData");
		request.getSession().removeAttribute("airportCode");
		cityName = i18n.get("airportsData." + airportCode + ".city");
		airportName = i18n.get("airportsData." + airportCode + ".name");
		}
		catch(Exception e){
			logger.error("Unexpected error in [BookingAirportData]: " + e.getMessage());
		}
		
	}

	private AirportDetailsRender renderAirportInfo(AirportDetailsData details, String airportCode){
		AirportDetailsRender render = new AirportDetailsRender();
		render.setDistanceFromCity(details.getCityDistance().toString() + " KM");
		String terminal = details.getAzTerminal();
		render.setTerminal(terminal != null && terminal.length() > 0 ? terminal : "-");
		String timeZone = "GMT ";
		timeZone += details.getFusoOrarioGMT() < 0 ? "-" : "+";
		timeZone += " " + Math.abs((details.getFusoOrarioGMT()/60));
		render.setTimeZone(timeZone);
		Integer checkInTimeDOM = details.getCheckInTimeDomestic();
		Integer checkInTimeINTZ = details.getCheckInTimeInternational();
		Integer checkInTimeINTC = details.getCheckInTimeIntercontinental();
		render.setCheckInLimitDOM(checkInTimeDOM != null && checkInTimeDOM > 0 ? i18n.get(I18nKeyBooking.CHECK_IN_TIME_DOM_FLIGHT_LABEL) + 
				" " +  details.getCheckInTimeDomestic().toString() + "'" : "");
		render.setCheckInLimitINTZ(checkInTimeINTZ != null && checkInTimeINTZ > 0  ? i18n.get(I18nKeyBooking.CHECK_IN_TIME_INTZ_FLIGHT_LABEL) + 
				" " +  details.getCheckInTimeInternational().toString() + "'" : "");
		render.setCheckInLimitINTC(checkInTimeINTC != null && checkInTimeINTC > 0  ? i18n.get(I18nKeyBooking.CHECK_IN_TIME_INTC_FLIGHT_LABEL) + 
				" " +  details.getCheckInTimeIntercontinental().toString() + "'" : "");
		return render;
	}
	
	public AirportDetailsRender getDetail() {
		return detail;
	}

	public String getCityName() {
		return cityName;
	}

	public String getAirportName() {
		return airportName;
	}


}
