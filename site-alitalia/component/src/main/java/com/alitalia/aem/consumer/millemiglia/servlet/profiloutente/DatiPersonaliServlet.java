package com.alitalia.aem.consumer.millemiglia.servlet.profiloutente;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.MMAddressData;
import com.alitalia.aem.common.data.home.MMContractData;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.MMTelephoneData;
import com.alitalia.aem.common.data.home.NewInformationData;
import com.alitalia.aem.common.data.home.enumerations.MMAddressTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMContractStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MMGenderTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMPhoneTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MMWorkPositionTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.WorkPositionTypesEnum;
import com.alitalia.aem.common.messages.home.ProfileRequest;
import com.alitalia.aem.common.messages.home.ProfileResponse;
import com.alitalia.aem.common.messages.home.UpdateUserProfileRequest;
import com.alitalia.aem.common.messages.home.UpdateUserProfileResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeyMillemiglia;
import com.alitalia.aem.consumer.millemiglia.model.profiloutente.DatiPersonaliData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "datipersonalisubmit" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class DatiPersonaliServlet extends GenericFormValidatorServlet {

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaCustomerProfileManager customerProfileManager;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		
		logger.debug("[DatiPersonaliServlet] validateForm");
		
		try {
			// validate parameters!
			Validator validator = setValidatorParameters(request, new Validator());
			return validator.validate();
		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Errore durante la procedura di salvataggio dati ", e);
			return null;
		}
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		logger.debug("[DatiPersonaliServlet] performSubmit");
		
		try {

			// recupero i dati dell'utente salvati nella clientcontext
			MMCustomerProfileData userProperties =
					AlitaliaUtils.getAuthenticatedUser(request);
			
			// recupero altri dati dell'utente loggato chiamando
			// il servizio getProfile
			MMCustomerProfileData customerProfileData =
					new MMCustomerProfileData();
			customerProfileData.setCustomerNumber(
					userProperties.getCustomerNumber());
			customerProfileData.setCustomerPinCode(
					customerProfileManager.decodeAndDecryptProperty(
							userProperties.getCustomerPinCode()));
			customerProfileData.setGender(MMGenderTypeEnum.fromValue(
					request.getParameter("genere")));
			customerProfileData.setCustomerNickName(userProperties.getCustomerNickName());
			
			ProfileRequest profileRequest = new ProfileRequest();
	    	profileRequest.setSid(IDFactory.getSid());
	    	profileRequest.setTid(IDFactory.getTid());
	    	profileRequest.setCustomerProfile(customerProfileData);
	    	
	    	ProfileResponse profileResponse =
	    			consumerLoginDelegate.getProfile(profileRequest);
	    	customerProfileData = profileResponse.getCustomerProfile();
			
	    	// creo gli oggetti newInformation e customerProfileToUpdate
	    	// contenenti le informazioni da aggiornare
			NewInformationData newInformation = new NewInformationData();
			MMCustomerProfileData customerProfileToUpdate =
					new MMCustomerProfileData();
			
			// imposto gli attributi obbligatori
			customerProfileToUpdate.setCustomerNumber(
					userProperties.getCustomerNumber());
			customerProfileToUpdate.setCustomerPinCode(
					customerProfileManager.decodeAndDecryptProperty(
							userProperties.getCustomerPinCode()));
			customerProfileToUpdate.setCustomerNickName(userProperties.getCustomerNickName());
			customerProfileToUpdate.setCustomerName(
					userProperties.getCustomerName());
			customerProfileToUpdate.setCustomerSurname(
					userProperties.getCustomerSurname());
			customerProfileToUpdate.setLanguage(
					userProperties.getLanguage());
			customerProfileToUpdate.setBirthDate(
					userProperties.getBirthDate());
			customerProfileToUpdate.setMailingType(
					userProperties.getMailingType());
			customerProfileToUpdate.setPreferences(customerProfileData.getPreferences());
			customerProfileToUpdate.setLifestyles(customerProfileData.getLifestyles());
			// setting modified data
			customerProfileToUpdate.setGender(MMGenderTypeEnum.fromValue(
					request.getParameter("genere")));
			newInformation.setWorkPositionUpdated(true);
			customerProfileToUpdate.setCustomerWorkPosition(
					MMWorkPositionTypeEnum.fromValue(
							request.getParameter("professione")));
			newInformation.setWorkPosition(
					WorkPositionTypesEnum.fromValue(
							request.getParameter("professione")));
				
			// setting modified telephone and email
			List<MMTelephoneData> updatedTelephoneDataList =
					customerProfileData.getTelephones();
			MMTelephoneData telephoneDataToUpdate = null;
			MMTelephoneData emailDataToUpdate = null;
			
			boolean telephoneFound = false;
			boolean emailFound = false;
			
			if (updatedTelephoneDataList != null) {
				Iterator<MMTelephoneData> telephoneDataIterator =
						updatedTelephoneDataList.iterator();
				while (telephoneDataIterator.hasNext()) {
					MMTelephoneData telephone = telephoneDataIterator.next();
					if (!telephone.getTelephoneType()
							.equals(MMPhoneTypeEnum.EMAIL) && !telephoneFound) {
						telephoneDataToUpdate = telephone;
						telephoneDataIterator.remove();
						telephoneFound = true;
					}
					if (telephone.getTelephoneType()
							.equals(MMPhoneTypeEnum.EMAIL) && !emailFound) {
						emailDataToUpdate = telephone;
						telephoneDataIterator.remove();
						emailFound = true;
					}
					if (telephoneFound && emailFound) {
						break;
					}
				}
			} else {
				updatedTelephoneDataList = new ArrayList<MMTelephoneData>();
			}
			
			// telephone
			if (telephoneDataToUpdate == null) {		
				telephoneDataToUpdate = new MMTelephoneData();
			}
			
			// add new phone number only if prefissoArea && telefono != empty
			if (null != request.getParameter("prefissoArea")
					&& null != request.getParameter("telefono")
					&& !"".equals(request.getParameter("prefissoArea"))
					&& !"".equals(request.getParameter("telefono"))) {
				telephoneDataToUpdate.setPhoneZone(
						request.getParameter("prefissoArea"));
				telephoneDataToUpdate.setCountryNumber(
						request.getParameter("prefissoNazionale"));
				telephoneDataToUpdate.setNumber(
						request.getParameter("telefono"));
				telephoneDataToUpdate.setTelephoneType(MMPhoneTypeEnum.fromValue(
						request.getParameter("tipoTelefono")));
				updatedTelephoneDataList.add(0, telephoneDataToUpdate);
			}
			
			// email
			if (emailDataToUpdate == null) {		
				emailDataToUpdate = new MMTelephoneData();
			}
			
			customerProfileToUpdate.setEmail(request.getParameter("email"));
			emailDataToUpdate.setTelephoneType(MMPhoneTypeEnum.EMAIL);
			emailDataToUpdate.setEmail(request.getParameter("email"));
			updatedTelephoneDataList.add(emailDataToUpdate);
			
			customerProfileToUpdate.setTelephones(updatedTelephoneDataList);
			
			// setting modified address
			MMAddressData addressDataToUpdate = new MMAddressData();
			for (MMAddressData addressData : customerProfileData.getAddresses()) {
				if (addressData.getMailingIndicator().equals("M")) {
					addressDataToUpdate = addressData;
				}
			}
						
			customerProfileToUpdate.setDefaultAddressCountry(
					request.getParameter("nazione"));
			customerProfileToUpdate.setDefaultAddressMunicipalityName(
					request.getParameter("citta"));
			customerProfileToUpdate.setDefaultAddressPostalCode(
					request.getParameter("cap"));
			customerProfileToUpdate.setDefaultAddressStateCode(
					request.getParameter("provincia"));
			customerProfileToUpdate.setDefaultAddressStreetFreeText(
					request.getParameter("indirizzo"));
			customerProfileToUpdate.setDefaultAddressType(
					MMAddressTypeEnum.fromValue(
							request.getParameter("tipoIndirizzo")));
			
			addressDataToUpdate.setAddressType(MMAddressTypeEnum.fromValue(
					request.getParameter("tipoIndirizzo")));
			if (addressDataToUpdate.getAddressType().equals(
					MMAddressTypeEnum.BUSINESS)) {
				addressDataToUpdate.setCompanyName(
						request.getParameter("nomeAzienda"));	
			}else{
				addressDataToUpdate.setCompanyName(null);	
			}
			addressDataToUpdate.setMailingIndicator("M");
			addressDataToUpdate.setInvalidIndicator(null);
			addressDataToUpdate.setMunicipalityName(
					request.getParameter("citta"));
			addressDataToUpdate.setCountry(
					request.getParameter("nazione"));
			addressDataToUpdate.setPostalCode(
					request.getParameter("cap"));
			addressDataToUpdate.setStateCode(
					request.getParameter("provincia"));
			addressDataToUpdate.setStreetFreeText(
					request.getParameter("indirizzo"));
			
			customerProfileToUpdate.setAddresses(customerProfileData.getAddresses());
			if (customerProfileToUpdate.getAddresses() == null) {
				customerProfileToUpdate.setAddresses(new ArrayList<MMAddressData>());
			}
			
			customerProfileToUpdate.getAddresses().add(addressDataToUpdate);
			
			// setting modified millemiglia young
			customerProfileToUpdate.setContratcType("YG");
			MMContractData mmContractData = new MMContractData();
			mmContractData.setName("YG");
			
			if ((String) request.getParameter("millemigliaYoung") != null) {
				customerProfileToUpdate.setContractAgree(true);
				mmContractData.setStatus(MMContractStatusEnum.REGISTERED);
			} else {
				customerProfileToUpdate.setContractAgree(false);
				mmContractData.setStatus(MMContractStatusEnum.DELETE);
			}

			customerProfileToUpdate.setContracts(new ArrayList<MMContractData>());
			customerProfileToUpdate.getContracts().add(mmContractData);
			
			// perform update
			UpdateUserProfileRequest updateUserProfileRequest =
					new UpdateUserProfileRequest();
			updateUserProfileRequest.setTid(IDFactory.getTid());
			updateUserProfileRequest.setSid(IDFactory.getSid(request));
			updateUserProfileRequest.setCustomerProfile(customerProfileToUpdate);
			updateUserProfileRequest.setNewInformation(newInformation);
			
			UpdateUserProfileResponse updateUserProfileResponse = 
					consumerLoginDelegate.updateProfile(updateUserProfileRequest);
			
			List<String> errors = updateUserProfileResponse.getCustomerUpdateErrors();
			if ((errors != null && errors.size() > 0 && !errors.get(0).equals("None"))
					|| updateUserProfileResponse.getCustomerProfile() == null) {
				logger.info("[DatiPersonaliServlet] Errore durante "
						+ "l'aggiornamento dei dati");
				throw new IOException("Update profile error");
				//TODO: Verificare se sollevare un'eccezione diversa
			}
			
			// aggiorno i dati dell'utente salvati nella clientcontext
			customerProfileManager.updateCustomerProfileProperties(
					customerProfileToUpdate, request.getResourceResolver(), "");
			
			goBackSuccessfully(request, response);
			
		} catch(Exception e) {
			logger.error("Error Dati Personali Servlet", e);
			throw new IOException("Error update user profile ");
			//TODO: Verificare se sollevare un'eccezione diversa
		}
		
	}
	
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		
		logger.debug("[DatiPersonaliServlet] saveDataIntoSession");
		
		DatiPersonaliData datiPersonaliData = new DatiPersonaliData();
		datiPersonaliData.setGender(request.getParameter("genere"));
		datiPersonaliData.setWorkPosition(request.getParameter("professione"));
		datiPersonaliData.setAddressType(request.getParameter("tipoIndirizzo"));
		datiPersonaliData.setCompanyName(request.getParameter("nomeAzienda"));
		datiPersonaliData.setAddress(request.getParameter("indirizzo"));
		datiPersonaliData.setPostalCode(request.getParameter("cap"));
		datiPersonaliData.setCity(request.getParameter("citta"));
		datiPersonaliData.setCountry(request.getParameter("nazione"));
		datiPersonaliData.setState(request.getParameter("provincia"));
		datiPersonaliData.setEmail(request.getParameter("email"));
		datiPersonaliData.setPhonePrefix(request.getParameter("prefissoNazionale"));
		datiPersonaliData.setAreaPrefix(request.getParameter("prefissoArea"));
		datiPersonaliData.setPhoneNumber(request.getParameter("telefono"));
		datiPersonaliData.setPhoneType(request.getParameter("tipoTelefono"));
	
		datiPersonaliData.setCheckMillemigliaYoung(request.getParameter("millemigliaYoung") != null);
		request.getSession().setAttribute("datiPersonaliData", datiPersonaliData);
	}
	
	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request, Validator validator) {

		// get parameters from request
		String genere = request.getParameter("genere");
		String tipoIndirizzo = request.getParameter("tipoIndirizzo"); // non obbligatorio
		String nomeAzienda = request.getParameter("nomeAzienda");
		String indirizzo = request.getParameter("indirizzo");
		String cap = request.getParameter("cap");
		String citta = request.getParameter("citta");
		String nazione = request.getParameter("nazione");
		//String provincia = request.getParameter("provincia"); //TODO: obbligatorio solo se per la nazione selezionata sono previste
		String email = request.getParameter("email");
		String prefissoNazionale = request.getParameter("prefissoNazionale");
		String telefono = request.getParameter("telefono");
		String tipoTelefono = request.getParameter("tipoTelefono");
		String prefissoArea = request.getParameter("prefissoArea");

		// add validate conditions
		validator.addDirectCondition("genere", genere,
				I18nKeyMillemiglia.GENDER_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectCondition("indirizzo", indirizzo,
				I18nKeyMillemiglia.ADDRESS_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectCondition("cap", cap,
				I18nKeyMillemiglia.CAP_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectCondition("citta", citta,
				I18nKeyMillemiglia.CITY_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectCondition("citta", citta,
				I18nKeyMillemiglia.CITY_ERROR_INVALID, "isAlphabeticWithAccentAndSpaces");
		
		validator.addDirectCondition("nazione", nazione,
				I18nKeyMillemiglia.NATION_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectCondition("email", email,
				I18nKeyMillemiglia.EMAIL_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectCondition("email", email,
				I18nKeyMillemiglia.EMAIL_ERROR_INVALID, "isEmail");
		
		if (tipoIndirizzo.equals("Business")) {
			validator.addDirectCondition("nomeAzienda", nomeAzienda,
					I18nKeyMillemiglia.NOMEAZIENDA_ERROR_EMPTY, "isNotEmpty");
		}
		
		if (!prefissoArea.equals("") || !telefono.equals("")) {
			validator.addDirectCondition("prefissoNazionale", prefissoNazionale,
					I18nKeyMillemiglia.PREFISSONAZIONALE_ERROR_EMPTY, "isNotEmpty");
			
			validator.addDirectCondition("telefono", telefono,
					I18nKeyMillemiglia.TELEFONO_ERROR_EMPTY, "isNotEmpty");
			
			validator.addDirectCondition("telefono", telefono,
					I18nKeyCommon.MESSAGE_INVALID_FIELD, "isNumber");
			
			validator.addDirectCondition("tipoTelefono", tipoTelefono,
					I18nKeyMillemiglia.TIPOTELEFONO_ERROR_EMPTY, "isNotEmpty");
			
			validator.addDirectCondition("prefissoArea", prefissoArea,
					I18nKeyMillemiglia.PREFISSOAREA_ERROR_EMPTY, "isNotEmpty");
			
			validator.addDirectCondition("prefissoArea", prefissoArea,
					I18nKeyCommon.MESSAGE_INVALID_FIELD, "isNumber");
		}
		
		return validator;
	}
}
