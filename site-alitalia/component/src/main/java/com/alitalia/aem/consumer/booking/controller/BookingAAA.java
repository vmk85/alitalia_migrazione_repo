package com.alitalia.aem.consumer.booking.controller;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.SeatMapData;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.AncillaryCompDataHolder;
import com.alitalia.aem.consumer.booking.model.AncillaryPassenger;
import com.alitalia.aem.consumer.booking.model.FlightData;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.model.Passenger;
import com.alitalia.aem.consumer.booking.model.extrabaggage.AncillariesExtraBagsPassengerRequest;
import com.alitalia.aem.consumer.booking.model.extrabaggage.AncillariesGetExtraBagsRequest;
import com.alitalia.aem.consumer.booking.model.extrabaggage.AncillariesGetResponse;
import com.alitalia.aem.consumer.booking.render.DirectFlightRender;
import com.alitalia.aem.consumer.booking.render.SeatsMapRender;
import com.alitalia.aem.consumer.booking.render.SelectedFlightInfoRender;
import com.google.gson.Gson;
//import com.fasterxml.jackson.databind.objectmapper;
//import org.codehaus.jackson.map.ObjectMapper;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingAAA extends BookingSessionGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private BookingSession bookingSession;
	
	private List<AncillaryPassenger> passengers;
	
	private List<String> passengers_new;
	
	private String[] sliceName;
	
	private List<List<String>> passengers_complete;
	
	private SelectedFlightInfoRender[] flights;
	
	private ArrayList<DirectFlightRender> flightsList;
	
	private boolean enabled;
	private boolean fam;
	private boolean showLimitedMessage;
	private static final String AIRPORT_CODE_TLV="TLV";
	private boolean showKosherMeal=true;
	private static final int MILLI_TO_HOUR = 1000 * 60 * 60;

	private boolean showBoxMeal=true;

	private AncillariesGetResponse ancillariesResponse;
	
	private String APIresponse;
	
	private AncillaryCompDataHolder componentData;
	 
	@PostConstruct
	protected void initModel() throws Exception {
		
		
		try {
			
			
			
			initBaseModel(request);
			
			if (request.getSession(true).getAttribute("AncillaryComponentData") != null) {
				
	    		Map<String, AncillaryCompDataHolder> ancillaryComponentData =
						(HashMap<String, AncillaryCompDataHolder>) request.getSession(true).getAttribute("AncillaryComponentData");
				
	    		if (ancillaryComponentData.get("BAG") != null) {
	    			
	    			componentData = ancillaryComponentData.get("BAG");
	    			/*
	        		componentClass = componentData.getComponentClass();
	        		componentHorizontalIndex = componentData.getHorizontalIndex();
	        		componentVerticalIndex = componentData.getVerticalIndex();
	        		*/
	    		}
	    		
			}
			
			if (ctx == null){
				return;
			}		
			enabled = true;
			showLimitedMessage = ctx.isLimitedMeals;
			passengers = new ArrayList<AncillaryPassenger>();
			fam = false;
			if (ctx.cug == BookingSearchCUGEnum.FAMILY) {
				if (ctx.familySolutionFound) {
					fam=true;
				}
			}
						
			List<Passenger> passengersList = ctx.passengersData.getPassengersList();
			
			String applicantMealPreference = bookingSession.retrieveLoggedUserMealPreferences(ctx);
			
			/* ------------------------------------------------------------------------ */
			ancillariesResponse=mokFillAncillariesExtraBagsPassengerList(passengersList, ctx.cookie);
			/* ------------------------------------------------------------------------ */
			
			Passenger applicantPassenger = passengersList.get(0);
			if (applicantPassenger != null) {
				AncillaryPassenger ancillaryPassenger = new AncillaryPassenger();
				ancillaryPassenger.setName(applicantPassenger.getName());
				ancillaryPassenger.setSurname(applicantPassenger.getSurname());
				ancillaryPassenger.setSecondName(applicantPassenger.getSecondName());
				ancillaryPassenger.setMealPreference(applicantMealPreference);
				if (ctx.selectionRoutes.getPassengers().get(0).getType() == PassengerTypeEnum.CHILD) {
					ancillaryPassenger.setIsChild(true);
				}
				passengers.add(ancillaryPassenger);
				for (int j=0; j<passengersList.size(); j++) {
					Passenger referencePassenger = passengersList.get(j);
					if (referencePassenger.isInfant() && referencePassenger.getReferenceAdult() == 0) {
						ancillaryPassenger.setIsInfantReferenced(true);
					}
				}
			}
				
			passengers_new = new ArrayList<String>();
			List<PassengerBaseData> passengersList_new = ctx.selectionRoutes.getPassengers();
			if (passengersList != null) {
				int indexPassenger = 0;
				for (PassengerBaseData pax : passengersList_new) {
					if (pax.getType() != PassengerTypeEnum.INFANT) {
						String passenger = pax.getName() + " " + pax.getLastName();
						if (pax.getType() == PassengerTypeEnum.ADULT) {
							passenger += computeReferedInfant(passengersList_new,indexPassenger);
						}
						if (pax.getType() == PassengerTypeEnum.CHILD) {
							String child = " " + i18n.get("booking.ancillary.bambino");
							if (ctx.cug == BookingSearchCUGEnum.FAMILY) {
								if (ctx.familySolutionFound) {
									child = " " + i18n.get("booking.ancillary.family.bambino");
								}
							}
							passenger += child;
						}
						this.passengers_new.add(passenger);
					}
					indexPassenger++;
				}
			}
				
				
			this.flightsList = new ArrayList<DirectFlightRender>();
			if (ctx.seatMapsByFlight != null && !ctx.seatMapsByFlight.entrySet().isEmpty()) {
				for (Entry<DirectFlightData, SeatMapData> entry : ctx.seatMapsByFlight.entrySet()) {
					DirectFlightRender directFlightRender = new DirectFlightRender(entry.getKey());
					SeatsMapRender mappaPosti = new SeatsMapRender(entry.getValue());
					if (mappaPosti.getSeatsMap()==null) {
						directFlightRender.setMappaPostiEnabled(false);
					} else {
						directFlightRender.setMappaPostiEnabled(true);
					}
					directFlightRender.setMappaPosti(mappaPosti);
					this.flightsList.add(directFlightRender);
				}
			}
				
				
			for (int k=1; k < passengersList.size(); k++) {
				Passenger passenger = passengersList.get(k);
				if (!passenger.isInfant()) {
					AncillaryPassenger ancillaryPassenger = new AncillaryPassenger();
					ancillaryPassenger.setName(passenger.getName());
					if (passenger.getSecondName() != null) {
						ancillaryPassenger.setSecondName(passenger.getSecondName());
					}
					ancillaryPassenger.setSurname(passenger.getSurname());
					ancillaryPassenger.setMealPreference("");
					if (ctx.selectionRoutes.getPassengers().get(k).getType() == PassengerTypeEnum.CHILD) {
						ancillaryPassenger.setIsChild(true);
					}
					
					for (int j=0; j<passengersList.size(); j++) {
						Passenger referencePassenger = passengersList.get(j);
						if (referencePassenger.isInfant() && referencePassenger.getReferenceAdult() == k) {
							ancillaryPassenger.setIsInfantReferenced(true);
						}
					}				
					passengers.add(ancillaryPassenger);
				}
			}
				
			sliceName = computeSliceName(ctx);
			flights = new SelectedFlightInfoRender[ctx.flightSelections.length];
			int i = 0;
			for (FlightSelection selectedFlight : ctx.flightSelections) {
				flights[i] = new SelectedFlightInfoRender(selectedFlight.getFlightData(), i18n, ctx.market);
				i++;
			} 
			/** Verifica se mostrare la scelta del box sulla base delle seguenti condizioni:
			 * 1)All'interno delle 24 ore non deve essere più mostrata.
			 * 2)Il pasto kosher può essere prenotato fino a 48 ore prima della partenza o 24 ore in partenza da TLV. 
			**/
			//computeToShowBoxSeatandKosherMeal();
			
			
			
		} catch (Exception e) {
			logger.error("unexpected error: ", e);
			throw e;
		}
	}
	
	public AncillaryCompDataHolder getComponentData() {
        return componentData;
    }
	
	public SlingHttpServletRequest getRequest() {
		return request;
	}

	public BookingSession getBookingSession() {
		return bookingSession;
	}

	public List<AncillaryPassenger> getPassengers() {
		return passengers;
	}

	private List<String> getPassengers_new(){
		return passengers_new;
	}
	
	private List<List<String>> getPassengers_complete(){
		return passengers_complete;
	}
	
	private AncillariesGetResponse getAncillariesResponse(){
		return ancillariesResponse;
	}

	public String[] getSliceName() {
		return sliceName;
	}

	public SelectedFlightInfoRender[] getFlights() {
		return flights;
	}
	
	public List<DirectFlightRender> getFlightsList() {
		return this.flightsList;
	}

	public boolean getEnabled() {
		return enabled;
	}
	
	public boolean isFam() {
		return fam;
	}
	
	public boolean getShowLimitedMessage() {
		return showLimitedMessage;
	}


	/* private methods */
	private AncillariesGetResponse mokFillAncillariesExtraBagsPassengerList(List<Passenger> passengersList, String ConversationID) throws Exception {
		
		
		
			Gson gson = new Gson();
			
			ArrayList<AncillariesExtraBagsPassengerRequest> passengers= new ArrayList<AncillariesExtraBagsPassengerRequest>();			
			for(int i=0; i<passengersList.size(); i++){
				AncillariesExtraBagsPassengerRequest item = new AncillariesExtraBagsPassengerRequest();
				item.setname(passengersList.get(i).getName());;
				item.setsurname(passengersList.get(i).getSurname());
				Integer index=i+1;
				item.setId(index.toString());
				item.setcustomerNumber(passengersList.get(i).getFrequentFlyerCardNumber());
				passengers.add(item);
			}
			AncillariesGetExtraBagsRequest request=new AncillariesGetExtraBagsRequest();
			request.setAncillariesPassengers(passengers);
			
			
			String input = gson.toJson(request);
			
			//String input1 = new String(Files.readAllBytes(Paths.get("C:\\request.json")), StandardCharsets.UTF_8);
			                                        
			String serverUrl="http://127.0.0.1:5000/api/v0.1/booking/products/AncillaryExtraBags";
	        URL url = new URL(serverUrl);
	        
	        HttpURLConnection conn = null;
	        try{	        
	 	 		conn = (HttpURLConnection) url.openConnection();
	 	 		
	
		 		conn.setDoOutput(true);
	
		 		//Conversation-ID:JSESSIONID=EA08A563C5E1258402FA39BF340CC113;WLPCOOKIE=sswhli4556;SSWGID=85C7291A49264B1688BAAE4483F2EAD6
		 		conn.setRequestProperty ("Conversation-ID", ConversationID);
		 		conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");
				//conn.setReadTimeout(60000);
				conn.setConnectTimeout(10000);
				
				/* LOG */
				logger.info("Calling WS-AirSearch server:" + url); 
				long before = System.currentTimeMillis();
				/* LOG */
				
				OutputStream os = conn.getOutputStream();
				os.write(input.getBytes());
				os.flush();
				os.close();
				
				/* LOG */
				long after = System.currentTimeMillis();
				long execTime = after - before;
				logger.info("Calling BookingProducts_ancillariesGetExctraBags server:" + url + "...done execution time=" + execTime);
				/* LOG */
				
				if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED && conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					APIresponse = "ERRORE:" + conn.getResponseCode();
				}
				
				/* LOG */
				logger.info("lettura stream risposta");
				before = System.currentTimeMillis();
				/* LOG */
				
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				
		        StringBuilder everything = new StringBuilder();
		        String line;
		        
		        while( (line = br.readLine()) != null) {
		            everything.append(line);
		         }
		       
		        APIresponse = everything.toString();
				conn.disconnect();
				
				try(PrintWriter out = new PrintWriter("C:\\Users\\Francesco Tomo\\Desktop\\responsebl.txt")){
				    out.println( APIresponse );
				}
				
				/* LOG */
				after = System.currentTimeMillis();
				execTime = after - before;
				logger.info("lettura stream risposta...done execution time=" + execTime);
				/* LOG */
				
				AncillariesGetResponse retval=new AncillariesGetResponse();
				
				retval= gson.fromJson(APIresponse, AncillariesGetResponse.class);
	
				return retval;	
				
	        } catch (Exception e) {
				if (conn != null) {
					conn.disconnect();}
				/* LOG */
				logger.error("unexpected error: ", e);
				/* LOG */
				throw e;
			}
	}
	
	/*
	private String computePassengerName(List<PassengerBaseData> passengersList, int i)  {
		PassengerBaseData passenger = passengersList.get(i);
		if (passenger instanceof AdultPassengerData) {
			AdultPassengerData typedPassenger = (AdultPassengerData) passenger;
			if (typedPassenger.getLastName() == null) {
				return null;
			}
			String secondName = "";
			if (typedPassenger.getInfo() instanceof PassengerApisSecureFlightInfoData) {
				PassengerApisSecureFlightInfoData apisSecureFlightInfo = (PassengerApisSecureFlightInfoData) typedPassenger.getInfo();
				if (apisSecureFlightInfo.getSecondName() != null) {
					secondName = " " + apisSecureFlightInfo.getSecondName() + " ";
				} 
			} else if (typedPassenger.getInfo() instanceof PassengerSecureFlightInfoData) {
				PassengerSecureFlightInfoData secureFlightInfo = (PassengerSecureFlightInfoData) typedPassenger.getInfo();
				if (secureFlightInfo.getSecondName() != null ){
					secondName = " " + secureFlightInfo.getSecondName() + " ";
				}
			}
			return typedPassenger.getLastName() + " " + typedPassenger.getName() + secondName;
		} else if (passenger instanceof ChildPassengerData) {
			ChildPassengerData typedPassenger = (ChildPassengerData) passenger;
			if (typedPassenger.getLastName() == null) {
				return null;
			}
			String secondName = "";
			if (typedPassenger.getInfo() instanceof PassengerApisSecureFlightInfoData) {
				PassengerApisSecureFlightInfoData apisSecureFlightInfo = (PassengerApisSecureFlightInfoData) typedPassenger.getInfo();
				if (apisSecureFlightInfo.getSecondName() != null) {
					secondName = " " + apisSecureFlightInfo.getSecondName() + " ";
				} 
			} else if (typedPassenger.getInfo() instanceof PassengerSecureFlightInfoData) {
				PassengerSecureFlightInfoData secureFlightInfo = (PassengerSecureFlightInfoData) typedPassenger.getInfo();
				if (secureFlightInfo.getSecondName() != null ){
					secondName = " " + secureFlightInfo.getSecondName() + " ";
				}
			}
			return typedPassenger.getLastName() + " " + typedPassenger.getName() + secondName;
		}
		return null;
	}
	*/
	
	private String[] computeSliceName(BookingSessionContext ctx) {
		String[] sliceName = new String[ctx.flightSelections.length];
		if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
			for ( int i = 0; i < ctx.flightSelections.length; i++) {
				sliceName[i] = obtainTitleBySliceIndex(i);
			}
		} else {
			for ( int i = 0; i < ctx.flightSelections.length; i++) {
				sliceName[i] = obtainARMessage(i);
			}
		}
		return sliceName;
	}
	
	private String obtainARMessage(int i) {
		switch(i){
			case 0:{
				return i18n.get("booking.carrello.andata.label"); //"andata"
			}
			case 1:{
				return i18n.get("booking.carrello.ritorno.label"); //"ritorno"
			}
		}
		return "";
	}
	
	private String obtainTitleBySliceIndex(int sliceIndex) {
		String msg = "";
		switch(sliceIndex){
			case 0:{
				msg = i18n.get("booking.common.multitratta.prima.label"); //"PRIMA"
			}break;
			case 1:{
				msg = i18n.get("booking.common.multitratta.seconda.label"); //"SECONDA"
			}break;
			case 2:{
				msg = i18n.get("booking.common.multitratta.terza.label"); //"TERZA"
			}break;
			case 3:{
				msg = i18n.get("booking.common.multitratta.quarta.label"); //"QUARTA"
			}break;
			default:{
				return "";
			}
		}
		return msg + " " + i18n.get("booking.common.multitratta.tratta.label"); //"TRATTA"
	}
	
	/*
	private void computeToShowBoxSeatandKosherMeal(){
		Date departureDate=new Date();
		Date today = Calendar.getInstance().getTime();
		String depAirportCode="";
		// Get the first route to extract the first departure Time to calculate is before 24hours or 48 hours from departure
		FlightData departureFlight=flights[0].getFlightData();
		if (departureFlight instanceof ConnectingFlightData){
			DirectFlightData connDepartureFlight=(DirectFlightData)((ConnectingFlightData)departureFlight).getFlights().get(0);
			departureDate=connDepartureFlight.getDepartureDate().getTime();
			depAirportCode=connDepartureFlight.getFrom().getCode();
		}else if(departureFlight instanceof DirectFlightData){				
			DirectFlightData directDepartureFlight=(DirectFlightData)departureFlight;
			departureDate=directDepartureFlight.getDepartureDate().getTime();
			depAirportCode=directDepartureFlight.getFrom().getCode();

		}
		
		long diffHours= (departureDate.getTime()-today.getTime())/MILLI_TO_HOUR;
		
		 if (diffHours<48){ 
				showKosherMeal=false;
				if(diffHours<=24 ){
					showBoxMeal=false;
				}else if(diffHours>24 && AIRPORT_CODE_TLV.equalsIgnoreCase(depAirportCode)){
					showKosherMeal=true;
				}
		}
	}
	*/
	
	public boolean showKosherMeal(){
		return showKosherMeal;
	}
	public boolean showBoxMeal(){
		return showBoxMeal;
	}
	public String getKosherMealCode(){
		return i18n.get("mealsData.KSML");
	}
	public String getAPIresponse(){
		return APIresponse;
	}
	private String computeReferedInfant(List<PassengerBaseData> passengers,
			int indexPassenger) {
		boolean hasReferedInfant = bookingSession.computeReferedInfant(passengers, indexPassenger) != -1;
		if (hasReferedInfant) {
			return "<span class=\"foodOnBoard__plus\"> + " + i18n.get("booking.ancillary.posto.neonato.label")+ "</span>";
		}
		return "";
	}
	
	
}
