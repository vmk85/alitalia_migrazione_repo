package com.alitalia.aem.consumer.model.content.faq;

import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables={Resource.class})
public class FaqModel {

	@Inject @Default(intValues={})
    private String[] faqs;

    @Self
    private Resource resource;

    private Logger logger = LoggerFactory.getLogger(FaqModel.class);

    private String faq11 = "";
    private String faq12 = "";
    private String faq13 = "";
    private String answer11 = "";
    private String answer12 = "";
    private String answer13 = "";

    private boolean active = false;


    /**
     * E' l'equivalente di un costruttore di classe. In questo caso viene utilizzato per inizializzare
     * i valori di path e resourceType a cui deve puntare il componente.
     */
    @PostConstruct
    protected void initModel() {


    	for (String faq : faqs) {
    		if(faq != null && faq.length() > 0){
        		//logger.info("Initializing model << BlockElement >>");
            	try {
                    final ResourceResolver resolver = resource.getResourceResolver();

                    Page page = AlitaliaUtils.getPage(resource);
                    String[] path = resource.getResourceResolver().map(page.getPath()).split("/");
                    String dynamicpath = "/" + path[1] + "/" + path[2] + "/" + path[3] + "/" + path[4] + "/";
                    String[] faqpage = faq.split("/");

                    final Page targetPage = resolver.getResource(dynamicpath + faqpage[5] + "/" +faqpage[6]).adaptTo(Page.class);
                    final Resource resource = targetPage.getContentResource("checkinlistafaq1");
                    if(resource != null) {

                        ValueMap map = resource.getValueMap();
                        String[] multifieldMapKeys = (String[]) map.keySet().toArray(new String[map.keySet().size()]);

                        setFaq11(map.get("faq11").toString().substring(3,map.get("faq11").toString().length()-4));
                        setFaq12(map.get("faq12").toString().substring(3,map.get("faq12").toString().length()-4));
                        setFaq13(map.get("faq13").toString().substring(3,map.get("faq13").toString().length()-4));

                        setAnswer11(map.get("answer11").toString().substring(3,map.get("answer11").toString().length()-4));
                        setAnswer12(map.get("answer12").toString().substring(3,map.get("answer12").toString().length()-4));
                        setAnswer13(map.get("answer13").toString().substring(3,map.get("answer13").toString().length()-4));

                        if(map.get("activate") != null && map.get("activate").toString().equals("on")){
                            setActive(true);
                        }
                    }
                } catch(Exception e) {
                    logger.error("Unable to get the values: \n" + e);
                }
        	} else{
        		logger.info("Risorsa vuota");
        	}
    	}

    }


    public String getFaq11() {
        return faq11;
    }

    public void setFaq11(String faq11) {
        this.faq11 = faq11;
    }

    public String getFaq12() {
        return faq12;
    }

    public void setFaq12(String faq12) {
        this.faq12 = faq12;
    }

    public String getFaq13() {
        return faq13;
    }

    public void setFaq13(String faq13) {
        this.faq13 = faq13;
    }

    public String getAnswer11() {
        return answer11;
    }

    public void setAnswer11(String answer11) {
        this.answer11 = answer11;
    }

    public String getAnswer12() {
        return answer12;
    }

    public void setAnswer12(String answer12) {
        this.answer12 = answer12;
    }

    public String getAnswer13() {
        return answer13;
    }

    public void setAnswer13(String answer13) {
        this.answer13 = answer13;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}

