package com.alitalia.aem.consumer.checkin;

import java.util.Calendar;

public class CheckinFlightDataMail {
	
	private String day;
	private String month;
	private String year;
	private String flightNumber;
	private String departureHour;
	private String departureCity;
	private String departureAirport;
	private String departureCode;
	private String arrivalHour;
	private String arrivalCity;
	private String arrivalAirport;
	private String arrivalCode;
	private String classes;
	
	private Calendar depTime;
	private Calendar arrTime;
	
	
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	
	public String getDepartureHour() {
		return departureHour;
	}
	public void setDepartureHour(String departureHour) {
		this.departureHour = departureHour;
	}
	
	public String getDepartureCity() {
		return departureCity;
	}
	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}
	
	public String getDepartureAirport() {
		return departureAirport;
	}
	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}
	
	public String getDepartureCode() {
		return departureCode;
	}
	public void setDepartureCode(String departureCode) {
		this.departureCode = departureCode;
	}
	
	public String getArrivalHour() {
		return arrivalHour;
	}
	public void setArrivalHour(String arrivalHour) {
		this.arrivalHour = arrivalHour;
	}
	
	public String getArrivalCity() {
		return arrivalCity;
	}
	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}
	
	public String getArrivalAirport() {
		return arrivalAirport;
	}
	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}
	
	public String getArrivalCode() {
		return arrivalCode;
	}
	public void setArrivalCode(String arrivalCode) {
		this.arrivalCode = arrivalCode;
	}
	
	
	
	public String getClasses() {
		return classes;
	}
	public void setClasses(String classes) {
		this.classes = classes;
	}
	
	
	public Calendar getDepTime() {
		return depTime;
	}
	public void setDepTime(Calendar depTime) {
		this.depTime = depTime;
	}
	
	public Calendar getArrTime() {
		return arrTime;
	}
	public void setArrTime(Calendar arrTime) {
		this.arrTime = arrTime;
	}
	
	@Override
	public String toString() {
		return "CheckinFlightDataMail [day=" + day + ", month=" + month
				+ ", year=" + year + ", flightNumber=" + flightNumber
				+ ", departureHour=" + departureHour + ", departureCity="
				+ departureCity + ", departureAirport=" + departureAirport
				+ ", departureCode=" + departureCode + ", arrivalHour="
				+ arrivalHour + ", arrivalCity=" + arrivalCity
				+ ", arrivalAirport=" + arrivalAirport + ", arrivalCode="
				+ arrivalCode + ", classes=" + classes + ", depTime=" + depTime
				+ ", arrTime=" + arrTime + "]";
	}
	
	
	
	
	
	
}
