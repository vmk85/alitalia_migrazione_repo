package com.alitalia.aem.consumer.mmb.model;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPassengerData;
import com.alitalia.aem.consumer.mmb.MmbUtils;

/**
 * A group of ancillaries sharing common data, such as type, passenger, route type and flight.
 * 
 * <p>This method assumes that the passengers are consistent in each flight of the route.</p>
 */
public class MmbAncillariesGroup {
	
	private Logger logger = LoggerFactory.getLogger(MmbAncillariesGroup.class);
	
	/**
	 * Grouping types.
	 */
	public enum AncillariesGroupType { 
		BY_PASSENGER,
		BY_PASSENGER_AND_ROUTE,
		BY_PASSENGER_AND_FLIGHT,
		BY_FLIGHT
	}
	
	/**
	 * List of included ancillaries.
	 */
	protected List<MmbAncillaryData> ancillaries = new ArrayList<MmbAncillaryData>();
	
	/**
	 * Type of the group.
	 */
	protected AncillariesGroupType groupType;
	
	/**
	 * Route data.
	 */
	protected MmbRouteData route = null;
	
	/**
	 * Type of included ancillaries.
	 */
	protected MmbAncillaryTypeEnum ancillaryType = null;
	
	/**
	 * List of all ancillary passengers.
	 */
	private List<MmbAncillaryPassengerData> allAncillaryPassengers;
	
	/**
	 * Index of the passenger of the group.
	 */
	protected Integer passengerIndex;
	
	/**
	 * List of relevant flights.
	 */
	protected List<MmbFlightData> flights = null;
	
	/**
	 * Route type (only if group type is BY_PASSENGER_AND_ROUTE).
	 */
	protected RouteTypeEnum routeType = null;
	
	/**
	 * Route multileg index (only if group type is BY_PASSENGER_AND_ROUTE).
	 */
	protected Integer multiLegIndex = null;
	
	/**
	 * Indicator of a multileg situation.
	 */
	protected boolean isPartOfMultiLeg = false;
	
	
	/* factory method */
	
	/**
	 * Factory method to prepare and populate groups by passengers.
	 * 
	 * @param groupType The type of grouping requested.
	 * @param route The route data.
	 * @param allAncillariesPassengers The complete list of ancillary passengers for the PNR.
	 * @param allAncillaries The complete list of ancillaries for the PNR.
	 * @param ancillaryType The type of ancillary to consider.
	 * @return The list of groups.
	 */
	public static List<MmbAncillariesGroup> createGroups(AncillariesGroupType groupType,
			MmbRouteData route, List<MmbAncillaryPassengerData> allAncillaryPassengers,
			List<MmbAncillaryData> allAncillaries, MmbAncillaryTypeEnum ancillaryType) {
		List<MmbAncillariesGroup> list = new ArrayList<>();
		int passengersNumber = allAncillaryPassengers.size();
		
		if (groupType == AncillariesGroupType.BY_PASSENGER) {
			for (int passengerIndex = 0; passengerIndex < passengersNumber; passengerIndex++) {
				MmbAncillariesGroup group = new MmbAncillariesGroup(groupType, route, ancillaryType,
						allAncillaryPassengers, passengerIndex, route.getFlights(), null);
				group.addAncillariesIfApplicable(allAncillaries);
				list.add(group);
			}
		
		} else if (groupType == AncillariesGroupType.BY_PASSENGER_AND_FLIGHT) {
			for (int passengerIndex = 0; passengerIndex < passengersNumber; passengerIndex++) {
				for (MmbFlightData flight : route.getFlights()) {
					List<MmbFlightData> flights = new ArrayList<MmbFlightData>();
					flights.add(flight);
					MmbAncillariesGroup group = new MmbAncillariesGroup(groupType, route, ancillaryType, 
							allAncillaryPassengers, passengerIndex, flights, null);
					group.addAncillariesIfApplicable(allAncillaries);
					list.add(group);
				}
			}
		
		} else if (groupType == AncillariesGroupType.BY_PASSENGER_AND_ROUTE) {
			List<List<MmbFlightData>> flightsByRoute = MmbUtils.groupFlightsByRoute(route.getFlights());
			for (int passengerIndex = 0; passengerIndex < passengersNumber; passengerIndex++) {
				for (List<MmbFlightData> routeFlights : flightsByRoute) {
					MmbAncillariesGroup group = new MmbAncillariesGroup(groupType, route, ancillaryType, 
							allAncillaryPassengers, passengerIndex, 
							routeFlights, routeFlights.get(0).getType());
					group.addAncillariesIfApplicable(allAncillaries);
					list.add(group);
				}
			}
			
		} else if (groupType == AncillariesGroupType.BY_FLIGHT) {
			for (MmbFlightData flight : route.getFlights()) {
				List<MmbFlightData> flights = new ArrayList<MmbFlightData>();
				flights.add(flight);
				MmbAncillariesGroup group = new MmbAncillariesGroup(groupType, route, ancillaryType, 
						allAncillaryPassengers, null, flights, null);
				group.addAncillariesIfApplicable(allAncillaries);
				list.add(group);
			}
			
		}
		
		return list;
	}
	
	
	/* protected constructor (use factory method) */
	
	/**
	 * Create a new ancillaries group.
	 * 
	 * @param groupType The grouping type.
	 * @param route The route data.
	 * @param ancillaryType The ancillary type of the group.
	 * @param groupType The group type.
	 * @param allAncillaryPassengers The list of ancillary passengers.
	 * @param passengerIndex The index of the ancillary passenger of the group.
	 * @param flights The flights subset.
	 * @param routeType The route type of the group.
	 */
	protected MmbAncillariesGroup(AncillariesGroupType groupType, MmbRouteData route, 
			MmbAncillaryTypeEnum ancillaryType, List<MmbAncillaryPassengerData> allAncillaryPassengers, 
			Integer passengerIndex, List<MmbFlightData> flights, RouteTypeEnum routeType) {
		this.groupType = groupType;
		this.route = route;
		this.ancillaryType = ancillaryType;
		this.allAncillaryPassengers = allAncillaryPassengers;
		this.passengerIndex = passengerIndex;
		this.flights = flights;
		this.routeType = null;
		this.isPartOfMultiLeg = MmbUtils.isMultiLeg(route.getFlights());
	}
	
	
	/* properties */
	
	/**
	 * @return The grouping type related to this group.
	 */
	public AncillariesGroupType getGroupType() {
		return groupType;
	}
	
	/**
	 * @return The route data related to this group.
	 */
	public MmbRouteData getRoute() {
		return route;
	}

	/**
	 * @return The ancillary type related to this group.
	 */
	public MmbAncillaryTypeEnum getAncillaryType() {
		return ancillaryType;
	}

	/**
	 * @return The index of the passenger related to this group.
	 */
	public Integer getPassengerIndex() {
		return passengerIndex;
	}
	
	/**
	 * @return The MMB ancillary passenger related to this group.
	 */
	public MmbAncillaryPassengerData getPassengerData() {
		if (passengerIndex != null) {
			return this.allAncillaryPassengers.get(this.passengerIndex);
		} else {
			return null;
		}
	}
	
	/**
	 * @return The MMB (non-ancillary) passenger related to this group for the given flight index.
	 */
	public MmbPassengerData findMmbPassengerDataForFlight(int flightIndex) {
		if (passengerIndex != null) {
			List<MmbPassengerData> flightPassengers = this.route.getFlights().get(flightIndex).getPassengers();
			return flightPassengers.get(this.passengerIndex);
		} else {
			return null;
		}
	}

	/**
	 * @return The route type related to this group, if set.
	 */
	public RouteTypeEnum getRouteType() {
		return routeType;
	}
	
	/**
	 * @return The list of flights related to this group.
	 */
	public List<MmbFlightData> getFlights() {
		return flights;
	}
	
	/**
	 * @return The list of ancillaries included in this group.
	 */
	public List<MmbAncillaryData> getAncillaries() {
		return ancillaries;
	}
	
	/**
	 * @return The list of ancillaries included in this group.
	 */
	public void setAncillaries(List<MmbAncillaryData> ancillaries ) {
		this.ancillaries=ancillaries;
	}
	
	
	/**
	 * @param type The required type.
	 * @return The list of ancillaries included in this group of the specified type.
	 */
	public List<MmbAncillaryData> getAncillariesByType(MmbAncillaryTypeEnum type) {
		List<MmbAncillaryData> list = new ArrayList<MmbAncillaryData>();
		for (MmbAncillaryData ancillary : this.ancillaries) {
			if (type.equals(ancillary.getAncillaryType())) {
				list.add(ancillary);
			}
		}
		return list;
	}
	
	/**
	 * @param status The required status.
	 * @return The list of ancillaries included in this group having the specified status.
	 */
	public List<MmbAncillaryData> getAncillariesByStatus(MmbAncillaryStatusEnum status) {
		List<MmbAncillaryData> list = new ArrayList<MmbAncillaryData>();
		for (MmbAncillaryData ancillary : this.ancillaries) {
			if (status.equals(ancillary.getAncillaryStatus())) {
				list.add(ancillary);
			}
		}
		return list;
	}
	
	/**
	 * @param status The required statuses.
	 * @return The list of ancillaries included in this group having the specified statuses.
	 */
	public List<MmbAncillaryData> getAncillariesByStatuses(MmbAncillaryStatusEnum[] statuses) {
		List<MmbAncillaryData> list = new ArrayList<MmbAncillaryData>();
		for (MmbAncillaryStatusEnum status : statuses) {
			list.addAll(getAncillariesByStatus(status));
		}
		return list;
	}
	
	/**
	 * @param status The required status.
	 * @return true if at least one of the ancillaries included in this group is in the specified status.
	 */
	public Boolean hasAncillariesInStatus(MmbAncillaryStatusEnum status) {
		for (MmbAncillaryData ancillary : this.ancillaries) {
			if (status.equals(ancillary.getAncillaryStatus())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param status The required statuses.
	 * @return true if at least one of the ancillaries included in this group is in one of the the specified statuses.
	 */
	public boolean hasAncillariesInStatuses(MmbAncillaryStatusEnum[] statuses) {
		for (MmbAncillaryStatusEnum status : statuses) {
			if (hasAncillariesInStatus(status)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param status The required status.
	 * @return The first of the ancillaries included in this group having the specified status, 
	 *         or null if no one is found.
	 */
	public MmbAncillaryData getAncillaryInStatus(MmbAncillaryStatusEnum status) {
		for (MmbAncillaryData ancillary : this.ancillaries) {
			if (status.equals(ancillary.getAncillaryStatus())) {
				return ancillary;
			}
		}
		return null;
	}
	
	/**
	 * @param status The required statuses.
	 * @return The first of the ancillaries included in this group having one of the specified statuses 
	 *         (evaluated in the provided order), or null if no one is found.
	 */
	public MmbAncillaryData getAncillaryInStatuses(MmbAncillaryStatusEnum[] statuses) {
		for (MmbAncillaryStatusEnum status : statuses) {
			MmbAncillaryData ancillary = getAncillaryInStatus(status);
			if (ancillary != null) {
				return ancillary;
			}
		}
		return null;
	}
	
	/**
	 * @return true if the group is related to a multileg PNR.
	 */
	public boolean isPartOfMultiLeg() {
		return isPartOfMultiLeg;
	}
	
	
	/* internal methods */
	
	/**
	 * Verify the ancillaries and add those applicable.
	 * 
	 * @param ancillary The ancillary to verify and add.
	 * @return true if one or more ancillaries have been added, false otherwise.
	 */
	protected boolean addAncillariesIfApplicable(List<MmbAncillaryData> ancillaries) {
		boolean added = false;
		for (MmbAncillaryData ancillary : ancillaries) {
			added = addAncillaryIfApplicable(ancillary) || added;
		}
		return added;
	}

	/**
	 * Verify if the ancillary is applicable to this group and, if so, add it.
	 * 
	 * @param ancillary The ancillary to verify and add.
	 * @return true if the ancillary has been added, false otherwise.
	 */
	protected boolean addAncillaryIfApplicable(MmbAncillaryData ancillary) {
		boolean isApplicable = isAncillaryApplicable(ancillary);
		if (isApplicable) {
			this.ancillaries.add(ancillary);
		}
		return isApplicable;
	}
	
	/**
	 * Verify if an ancillary is applicable to this group.
	 * 
	 * @param ancillary The ancillary to check.
	 * @return true if the ancillary belongs in this group, false otherwise.
	 */
	protected boolean isAncillaryApplicable(MmbAncillaryData ancillary) {
		if (ancillaryType != null && !ancillaryType.equals(ancillary.getAncillaryType())) {
			return false;
		}
		if (passengerIndex != null) {
			MmbAncillaryPassengerData ancillaryPassenger = findAncillaryPassenger(ancillary);
			if (ancillaryPassenger == null) {
				logger.warn("Could not find passenger related to ancillary id {} in PNR {}", 
						ancillary.getId(), route.getPnr());
				return false;
			}
			if (!ancillaryPassenger.getId().equals(this.getPassengerData().getId())) {
				return false;
			}
		}
		MmbFlightData ancillaryFlight = findAncillaryFlight(ancillary);
		if (ancillaryFlight == null) {
			logger.warn("Could not find flight related to ancillary id {} in PNR {}", 
					ancillary.getId(), route.getPnr());
			return false;
		}
		boolean flightFound = false;
		for (MmbFlightData flight : this.flights) {
			if (flight == ancillaryFlight) {
				flightFound = true;
				break;
			}
		}
		if (!flightFound) {
			return false;
		}
		return true;
	}
	
	/**
	 * Find the flight data related the ancillary, based on coupon number comparison.
	 * 
	 * @param ancillary The ancillary to verify.
	 * @return The flight, or null when not found.
	 */
	protected MmbFlightData findAncillaryFlight(MmbAncillaryData ancillary) {
		return MmbUtils.findAncillaryFlight(ancillary, this.route.getFlights());
	}

	/**
	 * Find the passeger for the ancillary, based on coupon number comparison.
	 * 
	 * @param ancillary The ancillary to verify.
	 * @return The passenger, or null when not found.
	 */
	protected MmbAncillaryPassengerData findAncillaryPassenger(MmbAncillaryData ancillary) {
		return MmbUtils.findAncillaryPassenger(ancillary, this.allAncillaryPassengers);
	}
	
}
