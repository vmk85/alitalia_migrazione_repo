package com.alitalia.aem.consumer.checkin.servlet;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkinprepaymentconsumer" }),
	@Property(name = "sling.servlet.methods", value = { "POST" , "GET" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })})
@SuppressWarnings("serial")
public class CheckinPrePaymentServlet extends GenericCheckinFormValidatorServlet {

	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile CheckinSession checkinSession;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

		CheckinSessionContext ctx = getCheckInSessionContext(request);
		
		String redirect = null;
		String baseUrl =  AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
		boolean paymentStepRequired = checkinSession.performPrePayment(ctx);
		if (paymentStepRequired) {
			redirect = baseUrl + getConfiguration().getCheckinPaymentPage();
		} else {
			redirect = baseUrl + configuration.getCheckinBoardingPassPage();
		}
		redirect = request.getResourceResolver().map(redirect);
			
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value("OK");
		json.key("redirect").value(redirect);
		json.key("paymentStepRequired").value(paymentStepRequired);
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected String getUnrecoverableErrorRedirect(
			SlingHttpServletRequest request, SlingHttpServletResponse response,
			GenericFormValidatorServletException exception) {
		return AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getCheckinAncillaryConfirmationPage();
	}
	
}
