package com.alitalia.aem.consumer.ssw;

public class SabreSonicWebDeepLinkCostants {
	/*Common Deep Link Parameter names*/
	public final static String LANG = "lang";

	/*MTO (MMB) Deep Link Parameter names*/
	public final static String PNR_MTO = "reservationCode";
	public final static String LAST_NAME_MTO = "lastName";
	
	/*DXCI (Check-in) Deep Link Parameter names*/
	public final static String DEEP_LINK_PAGE = "deepLinkPage";
	public final static String PNR_DXCI = "pnr";
	public final static String SEARCH_OPTIONS_DXCI = "searchOption";
	public final static String ARRIVAL_CITY_DXCI = "arrivalCity";
	public final static String LAST_NAME_DXCI = "lastName";
	public final static String FIRST_NAME_DXCI = "firstName";
	public final static String FLIGHT_NUMBER_DXCI = "flightNumber";
	public final static String FFCODE_DXCI = "ffNumber";
	public final static String TKT_NUMBER_DXCI = "vcrNumber";
	
	/*Redemption (Award) Deep Link Parameter names*/
	public final static String SEARCH_TYPE_RED = "searchType";
	public final static String DEP_DATE_RED = "departureDate";
	public final static String RET_DATE_RED = "returnDate";
	public final static String JOURNEY_SPAN_RED = "journeySpan";
	public final static String ORIGIN_RED = "origin";
	public final static String DESTINATION_RED = "destination";
	public final static String ALT_LANDING_PAGE_RED = "alternativeLandingPage";
	public final static String NUM_AD_RED = "numAdults";
	public final static String NUM_CH_RED = "numChildren";
	public final static String NUM_INF_RED = "numInfants";
	public final static String NUM_INF_SEAT_RED = "numInfantSeat";
	public final static String NUM_YHT_RED = "numYouth";
	public final static String NUM_MIL_RED = "numMilitary";
	public final static String BOOKING_CLASS_RED = "bookingClass";
	public final static String CURRENCY_RED = "currency";
	public final static String PRICE_RED = "price";
	public final static String COUNTRY_CODE_RED = "countryCode";
	public final static String REFERRED_CODE_RED = "referrerCode";
	public final static String PROMO_CODE_RED = "promoCode";
	public final static String PROMO_SEARCH_RED = "promo_search";
	public final static String IS_AWARD_RED = "isAward";
	public final static String USERNAME_RED = "username";
	public final static String CABIN_CLASS_RED = "cabinClass";
}
