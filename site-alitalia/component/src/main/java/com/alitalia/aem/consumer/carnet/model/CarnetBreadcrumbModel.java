package com.alitalia.aem.consumer.carnet.model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CarnetBreadcrumbModel extends GenericCarnetModel {

	@Inject
	AlitaliaConfigurationHolder configurationHolder;

	@Inject
	CheckinSession session;

	@Self
	private SlingHttpServletRequest request;

	private String phase;
	
	
	@PostConstruct
	protected void initModel() {
		try {
			initBaseModel(request);
			
			if (ctx.phase != null) {
				phase = ctx.phase.value();
			}
			
		} catch(Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}
	}
	
	public String getPhase() {
		return phase;
	}


	
}
