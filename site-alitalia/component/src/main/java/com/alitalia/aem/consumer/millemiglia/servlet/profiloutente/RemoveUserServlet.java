package com.alitalia.aem.consumer.millemiglia.servlet.profiloutente;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import javax.servlet.Servlet;

import com.alitalia.aem.common.messages.crmdatarest.SetCrmDataInfoResponse;
import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;

import com.alitalia.aem.consumer.gigya.GigyaSession;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaConstants;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaUtils;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSession;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSessionContext;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaUtils;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.service.impl.crmdatarest.service.CRMDataServiceImpl;
import com.alitalia.aem.web.component.webcheckin.delegate.WebCheckinDelegate;
import com.alitalia.aem.ws.gigyaService.GigyaClient;

import com.day.cq.commons.TidyJSONWriter;
import com.gigya.socialize.GSRequest;
import com.gigya.socialize.GSResponse;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//import com.

@Component
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = {"removeusersubmit"}),
        @Property(name = "sling.servlet.methods", value = {"POST"}),
        @Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"})})
@SuppressWarnings("serial")
public class RemoveUserServlet extends GenericFormValidatorServlet {

    private static final Logger logger = LoggerFactory.getLogger(RemoveUserServlet.class);

    @Reference
    private volatile AlitaliaConfigurationHolder configuration;

    @Reference
    private volatile GigyaSession gigyaSession;

    private AlitaliaUtils alitaliaUtils = new AlitaliaUtils();

    @Reference
    private volatile MyAlitaliaSession myAlitaliaSession;

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {

        logger.debug("[DatiLoginServlet] validateForm");

        try {
            // validate parameters!
            Validator validator = new Validator();

            ResultValidation resultValidation = validator.validate();
            return resultValidation;
        }

        // an error occurred...
        catch (Exception e) {
            logger.error("Errore durante la procedura di salvataggio dati ", e);
            return null;
        }
    }

    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws Exception {

        logger.info("[RemoveUserServlet] performSubmit");

        boolean isError = true;

        MACustomer maCustomer = MyAlitaliaUtils.getMaCustomerBySession(request);

        String errorMessage = "", cookieName = maCustomer.getSessionInfo().getCookieName(), cookieValue = maCustomer.getSessionInfo().getCookieValue(), UID = maCustomer.getUID();

        int errorCode = 0;

        MyAlitaliaSessionContext mactx = MyAlitaliaUtils.getMactxBySession(request);

        if (mactx != null && mactx.getMaCustomer() != null) {
            UID = mactx.getMaCustomer().getUID();
        } else {
            UID = request.getParameter("UID");
        }

        /** Delete account su CRM*/
        SetCrmDataInfoResponse setCrmDataInfoResponse = myAlitaliaSession.deleteCrmAccount(mactx.getMaCustomer().getProfile().getEmail(), request);

        /** Controllo che abbia avuto successo la cancelazione su CRM */
        if (setCrmDataInfoResponse != null && setCrmDataInfoResponse.getResult() != null) {

            if (setCrmDataInfoResponse.getResult().getStatus().equals("OK") && setCrmDataInfoResponse.getResult().getCode().equals("1")) {
                /** Effettuo la cancellazione dell'account su gigya a freonte della cancellazione su CRM */
                GSResponse gsResponse = gigyaSession.deleteAccount(UID);
                logger.info("Response cancellazione account da gigya:  = " + gsResponse.getResponseText());

                if (gsResponse.getErrorCode() == 0) {
                    /** Cancellazione avvenuta con successo */
                    isError = false;
                    /** Elimino l'utente dalla sessione */
                    logger.info("Cancellazione dell'utente avvenuta con successo:" + mactx.getGetCrmDataInfoResponse().getEmail());
                    request.getSession().removeAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);
                } else {
                    isError = true;
                    errorMessage = gsResponse.getErrorMessage();
                    errorCode = gsResponse.getErrorCode();
                    logger.info("[RemoveUserServlet] Errore durante la cancellazione dell'utente da gigya: " + gsResponse.getResponseText());
                }
            } else {
                isError = true;
                errorMessage = setCrmDataInfoResponse.getResult().getDescription();
                /** il codice di errore che restituisce il CRM risulta essere sempre 0*/
                errorCode = 0;
                logger.info("[RemoveUserServlet] Errore durante la cancellazione dell'utente dal CRM: " + setCrmDataInfoResponse.getResult().getDescription());
            }
        } else {
            isError = true;
            logger.info("[RemoveUserServlet] Errore durante la cancellazione dell'utente, risposta da parte del CRM nulla ");
        }
        try {
            response.setContentType("application/json");
            TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
            json.object();
            json.key("isError").value(isError);
            json.key("errorMessage").value(errorMessage);
            json.key("errorCode").value(errorCode);
            json.key("cookieName").value(cookieName);
            json.key("cookieValue").value(cookieValue);
            json.endObject();
        } catch (Exception e) {
        }

    }

    @Override
    protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {

    }

    /**
     * Set Validator parameters for validation
     *
     * @param request
     * @param validator
     */
    private Validator setValidatorParameters(SlingHttpServletRequest request, Validator validator) {

        // get parameters from request
        String UID = request.getParameter("UID");

        validator.addDirectCondition("UID", UID,
                "UID", "isNotEmpty");

        return validator;
    }
}
