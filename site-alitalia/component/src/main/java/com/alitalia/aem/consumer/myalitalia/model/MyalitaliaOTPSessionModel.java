package com.alitalia.aem.consumer.myalitalia.model;

import java.util.Date;

public class MyalitaliaOTPSessionModel {

    private Date expire;
    private String code;
    private String data;
    private String type;
    private Boolean isVerified;
    private int attempt;

    public MyalitaliaOTPSessionModel() {}

    public MyalitaliaOTPSessionModel(String code, Date expire, String type, Boolean isVerified, String data) {
        if(code != null) this.code = code;
        if(expire != null) this.expire = expire;
        if(type != null) this.type = type;
        if(isVerified != null) this.isVerified = isVerified;
        if(data != null) this.data = data;

    }

    public int getAttempt() {
        return attempt;
    }

    public void setAttempt(int attempt) {
        this.attempt = attempt;
    }

    public Boolean isVerified() {
        return isVerified;
    }

    public void setVerified(Boolean verified) {
        isVerified = verified;
    }

    public Date getExpire() {
        return expire;
    }

    public void setExpire(Date expire) {
        this.expire = expire;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
