package com.alitalia.aem.consumer.carnet;


public enum CarnetCreditCardTypeEnum {

    AMERICAN_EXPRESS("americanexpress"),
    MASTER_CARD("mastercard"),
    VISA("visa"),
    VISA_ELECTRON("visaelectron"),
    DINERS("dinersclub"),
    UATP("uatp"),
    MAESTRO("maestro");

    private final String value;

    CarnetCreditCardTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CarnetCreditCardTypeEnum fromValue(String v) {
        for (CarnetCreditCardTypeEnum c: CarnetCreditCardTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
