package com.alitalia.aem.consumer.booking.analytics.filler;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;

import com.alitalia.aem.consumer.booking.analytics.AnalyticsInfo;

public class CarnetAnalyticsInfoToJsObj extends ToJsObj implements AnalyticsInfoToJsObj {
	
	private static final String HYPHEN_FLIGHT_DATE_FORMAT = "yyyy-MM-dd";
	private static final String HYPHEN_EU_FLIGHT_DATE_FORMAT = "dd-MM-yy";
	private static final String EU_FLIGHT_DATE_FORMAT = "dd/MM/yyyy";
	private static final String EN_FLIGHT_DATE_FORMAT = "MM/dd/yyyy";
	private static final String TIME_FORMAT_HH_MM_SS = "kk:mm:ss";
	
	private DecimalFormat format;
	private SimpleDateFormat dateFormat_eu;
	private SimpleDateFormat dateFormat_en;
	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateFormat_eu_hyphen;
	private SimpleDateFormat timeFormat;
	@Override
	public String toJSObj(AnalyticsInfo info, boolean trailingSemicolon) {
		// TODO Auto-generated method stub
		String js = "{";
		/*Other info*/
		if (info.step>=1 && info.step <6){
			js = addJsObjElem(js, "CarnetUseStep", info.carnetUseStep, true);
			js = addJsObjElem(js, "CarnetTratta", info.carnetTratta, true);
			js = addJsObjElem(js, "CarnetItinType", info.carnetItinType, false);
		}
		js += (trailingSemicolon) ? "};" : "}";
		return js;
	}
	
	
}
