package com.alitalia.aem.consumer.carnet.servlet;


import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.carnet.CarnetConstants;
import com.alitalia.aem.consumer.carnet.CarnetSession;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "carnetpersonaldata" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CarnetPersonalDataServlet extends GenericCarnetFormValidatorServlet {
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private CarnetSession carnetSession;	

	
	private final static String NAME_FIELD = "name";
	private final static String LASTNAME_FIELD = "lastName";
	private final static String E_MAIL_FIELD = "email";
	private final static String CHECK_NEWSLETTER_FIELD = "checkNewsletter";
	private final static String TELEPHONE_FIELD = "telephone";
	private final static String FAX_FIELD = "fax";
	private final static String CELLPHONE_FIELD = "cellphone";
	private final static String AGENCY_FIELD = "ragSoc";
	private final static String PERSONAL_CODE_FIELD = "personalCode";
	private final static String CHECK_ACCEPT_FIELD = "checkAccept";
	private final static String NATION_FIELD = "nation";
	private final static String PROVINCE_FIELD = "province";
	private final static String ADDRESS_FIELD = "address";
	private final static String CITY_FIELD = "city";
	private final static String CAP_FIELD = "cap";
	private final static String IVA_FIELD = "iva";
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		Validator validator = new Validator();
		
		String name = request.getParameter(NAME_FIELD);
		validator.addDirectCondition(NAME_FIELD, 
				name, CarnetConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(NAME_FIELD, 
				name, CarnetConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
		
		String lastname = request.getParameter(LASTNAME_FIELD);
		validator.addDirectCondition(LASTNAME_FIELD, 
				lastname, 
				CarnetConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(LASTNAME_FIELD, 
				lastname, 
				CarnetConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
		
		validator.addDirectCondition(E_MAIL_FIELD, request.getParameter(E_MAIL_FIELD),
				CarnetConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(E_MAIL_FIELD, request.getParameter(E_MAIL_FIELD),
				CarnetConstants.MESSAGE_SPECIFIC_EMAIL_FIELD, "isEmail");
		
		String telephone = request.getParameter(TELEPHONE_FIELD);
		if (!isEmpty(telephone)) {
			validator.addDirectCondition(TELEPHONE_FIELD, 
					telephone, 
					CarnetConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNumeroTelefonoValido");
		}
		
		String fax = request.getParameter(FAX_FIELD);
		if (!isEmpty(fax)) {
			validator.addDirectCondition(FAX_FIELD, 
					telephone, 
					CarnetConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNumeroTelefonoValido");
		}
		
		validator.addDirectCondition(CELLPHONE_FIELD, 
				request.getParameter(CELLPHONE_FIELD), 
				CarnetConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNumeroTelefonoValido");
		
		validator.addDirectCondition(CHECK_ACCEPT_FIELD,
				request.getParameter(CHECK_ACCEPT_FIELD),
				CarnetConstants.MESSAGE_GENERIC_CHECK_FIELD, "isNotEmpty");
		
		String cf = request.getParameter(PERSONAL_CODE_FIELD);
		
		if (!isEmpty(cf)) {
			String validateCF = name + "|" + lastname + "|" + cf;
			validator.addDirectCondition(PERSONAL_CODE_FIELD, 
					validateCF, 
					CarnetConstants.MESSAGE_SPECIFIC_CF_FIELD, "isValidCodiceFiscaleByConcatNomeCognomeCF");
		}
		
		
		validator.addDirectCondition(NATION_FIELD, request.getParameter(NATION_FIELD),
				CarnetConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		
		String cap = request.getParameter(CAP_FIELD);
		if (!isEmpty(cap)) {
			validator.addDirectCondition(CAP_FIELD, 
					cap, 
					CarnetConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNumber");
		}
		
		
		String iva = request.getParameter(IVA_FIELD);
		if (!isEmpty(iva)) {
			validator.addDirectCondition(IVA_FIELD, 
					iva, 
					CarnetConstants.MESSAGE_GENERIC_INVALID_FIELD, "isPartitaIVA");
		}
		
		
		
		/* Risultato della validazione */
		ResultValidation resultValidation = validator.validate();
		
		
		
		return resultValidation;
	}
	
	
		
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		CarnetSessionContext ctx = this.getCarnetSessionContext(request);
		String name = request.getParameter(NAME_FIELD); 
		String lastname = request.getParameter(LASTNAME_FIELD);
		String email = request.getParameter(E_MAIL_FIELD);
		String telephone = request.getParameter(TELEPHONE_FIELD);
		String fax = request.getParameter(FAX_FIELD);
		String cellphone = request.getParameter(CELLPHONE_FIELD);
		String cf = request.getParameter(PERSONAL_CODE_FIELD);
		String nationName = request.getParameter(NATION_FIELD);
		String province = request.getParameter(PROVINCE_FIELD);
		String cap = request.getParameter(CAP_FIELD);
		String iva = request.getParameter(IVA_FIELD);
		String address = request.getParameter(ADDRESS_FIELD);
		String agency = request.getParameter(AGENCY_FIELD);
		String city = request.getParameter(CITY_FIELD);
		String newsletter = request.getParameter(CHECK_NEWSLETTER_FIELD);
		
		nationName = ctx.i18n.get("countryData." + nationName);
		
		if (isEmpty(newsletter)) {
			carnetSession.insertBuyerData(ctx, email, lastname, name, cellphone, address, city, nationName, agency, fax, province, telephone, iva, cap, cf, false);
		}
		else {
			carnetSession.insertBuyerData(ctx, email, lastname, name, cellphone, address, city, nationName, agency, fax, province, telephone, iva, cap, cf, true);
		}
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		
		String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCarnetPaymentPage();
		successPage = request.getResourceResolver().map(successPage);
		json.key("redirect").value(successPage);
		json.endObject();
	}
	
	
	
	private boolean isEmpty(String value) {
		return value == null || "".equals(value) || " ".equals(value);
	}



	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
	
}