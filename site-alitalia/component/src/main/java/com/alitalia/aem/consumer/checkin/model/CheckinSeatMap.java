package com.alitalia.aem.consumer.checkin.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryUpgradeDetailData;
import com.alitalia.aem.common.data.home.checkin.CheckinCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillarySeatDetailData;
import com.alitalia.aem.consumer.checkin.CheckinPassenger;
import com.alitalia.aem.consumer.checkin.CheckinPassengerSeat;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.checkin.render.CheckinSeatMapFlight;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinSeatMap extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private CheckinSession checkinSession;
	
	private List<CheckinFlightData> flights;
	private Map<String, CheckinSeatMapFlight> seatMaps;
	private List<CheckinPassenger> passengers;
	private String bookingSeatSelectionsJS;
	
	private MmbCompartimentalClassEnum businessClass;
	private MmbCompartimentalClassEnum premiumEcoClass;
	
	private String comfortSeatPrice;
	
	@PostConstruct
	protected void initModel() {
		
		initBaseModel(request);
		flights = CheckinUtils.getFlightsList(ctx.selectedRoute);
		seatMaps = ctx.seatMaps;
		
		List<CheckinPassengerData> checkedInPassengers = CheckinUtils.getCheckedInPassengers(ctx.passengers);
		
		businessClass = checkinSession.getBusinessCompClass(ctx);
		premiumEcoClass = checkinSession.getPremiumEcoCompClass(ctx);
		
		Map<String, MmbAncillaryData> ancillariesAssignedSeats = checkinSession.getAncillariesAssignedSeats(ctx);
		Map<String, MmbAncillaryData> ancillariesAssignedUpgradePrevIssued = checkinSession.getAllAncillariesIssuedUpgrade(ctx, true);
		Map<String, MmbAncillaryData> ancillariesAssignedUpgrade = checkinSession.getAllAncillariesIssuedUpgrade(ctx, false);
		passengers = new ArrayList<CheckinPassenger>();
		for (CheckinPassengerData passengerData : checkedInPassengers) {
			CheckinPassenger passenger = checkinSession.toCheckinPassenger(passengerData); 
			Map<String, CheckinPassengerSeat> seats = checkinSession.getPassengerSeats(ctx, passengerData, ancillariesAssignedSeats);
			passenger.setSeats(seats);
			passengers.add(passenger);
		}
		
		bookingSeatSelectionsJS = "[";
		
		for (String flightNumber : seatMaps.keySet()) {
			CheckinSeatMapFlight seatMap;
			if ((seatMap = seatMaps.get(flightNumber)) != null) {
				int k = 0;
				for (CheckinPassengerData passengerData : checkedInPassengers) {
					
					CheckinPassenger passenger = passengers.get(k);
					
					String eticket;
					if ((eticket = getEtickets(passengerData).get(seatMap.getFlightNumber())) != null) {
						String jsDataPassenger = String.valueOf(k + 1);
						String jsDataFlight = String.valueOf(seatMap.getFlightNumber());
						String jsDataSeatNumber = getSeatByAncillary(ancillariesAssignedSeats.get(eticket), ancillariesAssignedUpgrade.get(eticket)); 
						
						String jsDataPreviousSeatNumber = jsDataSeatNumber;
						
						String jsDataHasInfant = passenger.isHasInfant() ? "true" : "false";
						String jsDataIsChild = passenger.isChild() ? "true" : "false";
						String jsDataComfortSeatPrice = "";
						String jsDataPaymentSeatPrice = "";
						String jsDataComfortSeatPriceCurrency = "";
						String jsDataHasFreeComfortSeat = "false";
						
						// Prezzo posto comfort
						MmbAncillaryData comfortSeat = checkinSession.getAvailableOrCurrentAncillarySeat(ctx, eticket, true, false);
						// Prezzo posto a pagamento (per tariffe light)
						MmbAncillaryData paymentSeat = checkinSession.getAvailableOrCurrentAncillarySeat(ctx, eticket, false, true);
						
						String jsDataIsChangeable = String.valueOf(comfortSeat != null);
						
						MmbPriceRender comfortPriceRender = null;
						if (comfortSeat != null) {
							comfortPriceRender = checkinSession.getAncillaryDisplayedPriceInfo(ctx, comfortSeat, 1);
						}
						
						if (comfortPriceRender != null) {
							jsDataComfortSeatPrice = comfortPriceRender.getFormattedAmount();
							jsDataHasFreeComfortSeat = comfortPriceRender.getIsAmountZeroOrNull() ? "true" : "false";
						}
						
						MmbPriceRender paymentPriceRender = null;
						if (paymentSeat != null) {
							paymentPriceRender = checkinSession.getAncillaryDisplayedPriceInfo(ctx, paymentSeat, 1);
						}
						
						if (paymentPriceRender != null) {
							jsDataPaymentSeatPrice = paymentPriceRender.getFormattedAmount();
						}
						
						if (comfortPriceRender != null || paymentPriceRender != null) {
							jsDataComfortSeatPriceCurrency = comfortPriceRender != null ?
									comfortPriceRender.getCurrencySymbol() : paymentPriceRender.getCurrencySymbol();
									comfortSeatPrice = jsDataComfortSeatPrice+" "+jsDataComfortSeatPriceCurrency;
						}
						
						String jsDataAncillaryIds = comfortSeat == null ? "" : String.valueOf(comfortSeat.getId());
						if (paymentSeat != null) {
							jsDataAncillaryIds += (jsDataAncillaryIds.isEmpty() ? "" : "-") + String.valueOf(paymentSeat.getId());
						}
						
						
						String jsUpgradeChoosed = "";
						String jsDataUpgradeAncillaryIds = "{}";
						MmbAncillaryData upgrade;
						if((upgrade = ancillariesAssignedUpgradePrevIssued.get(eticket)) != null){
							CheckinAncillaryUpgradeDetailData detail = (CheckinAncillaryUpgradeDetailData) upgrade.getAncillaryDetail();
							if(detail != null){
								jsUpgradeChoosed = fromMmbCompartimentalClassEnumToString(detail.getCompartimentalClass());
							}
							else{
								/*If Upgrade detail is null, retrieve info of seatClass from coupon*/
								/*assumes that there is only one flight (cannot purchase upgrade for route with steps)*/
								jsUpgradeChoosed = fromMmbCompartimentalClassEnumToString(passengerData.getCoupons().get(0).getSeatClass());
							}
						}
						else{
							Map<MmbCompartimentalClassEnum, MmbAncillaryData> upgradeMap = checkinSession.getAvailableUpdateForEticket(ctx, eticket);
							if(upgradeMap != null){
								jsDataUpgradeAncillaryIds = fromUpgradesMapToJsObject(upgradeMap);
							}
						}
						bookingSeatSelectionsJS += "{" +
								"passenger: '" + jsDataPassenger + "'," +
								"flight: '" + jsDataFlight + "'," +
								"seatNumber: '" + jsDataSeatNumber + "'," +
								"previousSeatNumber: '" + jsDataPreviousSeatNumber + "'," +
								"isChangeable: '" + jsDataIsChangeable + "', " +
								"hasInfant: '" + jsDataHasInfant + "', " +
								"isChild: '" + jsDataIsChild + "', " +
								"comfortSeatPrice: '" + jsDataComfortSeatPrice + "', " +
								"paymentSeatPrice: '" + jsDataPaymentSeatPrice + "', " +
								"comfortSeatPriceCurrency: '" + jsDataComfortSeatPriceCurrency + "', " +
								"hasFreeComfortSeat: '" + jsDataHasFreeComfortSeat + "', " +
								"ancillaryIds: '" + jsDataAncillaryIds + "', " +
								"upgradeChoosed: '" + jsUpgradeChoosed + "', " +
								"upgradeAncillaryIds: " + jsDataUpgradeAncillaryIds + " " +
								"},\n";
						k++;
					}
				}
			}
		}
		bookingSeatSelectionsJS += "]";
	}
	
	private Map<String, String> getEtickets(CheckinPassengerData passengerData) {
		Map<String, String> etickets = new HashMap<String, String>();
		if (passengerData.getCoupons() != null) {
			for (CheckinCouponData coupon : passengerData.getCoupons()) {
				etickets.put(coupon.getFlight().getFlightNumber(), coupon.getEticket() + "C" + coupon.getId());
			}
		}
		return etickets;
	}
	
	private String getSeatByAncillary(MmbAncillaryData seat, MmbAncillaryData upgrade) {
		if(upgrade != null){
			CheckinAncillaryUpgradeDetailData detail = (CheckinAncillaryUpgradeDetailData) upgrade.getAncillaryDetail();
			if(detail != null){
				return StringUtils.stripStart(detail.getSeat(), "0");
			}
		}
		else if (seat != null) {
			MmbAncillarySeatDetailData ancillaryDetail;
			if ((ancillaryDetail = (MmbAncillarySeatDetailData) seat.getAncillaryDetail()) != null) {
				return StringUtils.stripStart(ancillaryDetail.getSeat(), "0");
			}
		}
		return "";
	}
	
	private String fromUpgradesMapToJsObject(Map<MmbCompartimentalClassEnum, MmbAncillaryData> upgrades){
		String jsObj = "{";
		int i = 0;
		for(MmbCompartimentalClassEnum compClass : upgrades.keySet()){
			jsObj += "'" + fromMmbCompartimentalClassEnumToString(compClass) + "': '";
			if(upgrades.get(compClass) != null)
				jsObj += upgrades.get(compClass).getId();
			jsObj += "',";
			i++;
		}
		//remove last ","
		if (i>0){
			jsObj = jsObj.substring(0, jsObj.length()-1);
		}
		jsObj += "}";
		return jsObj;
	}
	
	private String fromMmbCompartimentalClassEnumToString(MmbCompartimentalClassEnum compClass){
		String str = "";
		
		if(businessClass.equals(compClass)){
			str = "Business";
		}
		else if(premiumEcoClass != null && premiumEcoClass.equals(compClass)){
			str = "PremiumEconomy";
		}
		else if (MmbCompartimentalClassEnum.Y.equals(compClass)){
			str = "Economy";
		}
		else{
			str = "Unknow";
		}
		return str;
	}

	public List<CheckinFlightData> getFlights() {
		return flights;
	}

	public void setFlights(List<CheckinFlightData> flights) {
		this.flights = flights;
	}

	public Map<String, CheckinSeatMapFlight> getSeatMaps() {
		return seatMaps;
	}

	public void setSeatMaps(Map<String, CheckinSeatMapFlight> seatMaps) {
		this.seatMaps = seatMaps;
	}

	public List<CheckinPassenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<CheckinPassenger> passengers) {
		this.passengers = passengers;
	}

	public String getBookingSeatSelectionsJS() {
		return bookingSeatSelectionsJS;
	}

	public void setBookingSeatSelectionsJS(String bookingSeatSelectionsJS) {
		this.bookingSeatSelectionsJS = bookingSeatSelectionsJS;
	}

	public String getBusinessClass() {
		return businessClass.value();
	}

	public String getPremiumEcoClass() {
		if (premiumEcoClass != null){
			return premiumEcoClass.value();
		}
		else 
			return "";
	}

	public String getComfortSeatPrice() {
		return comfortSeatPrice;
	}

	public void setComfortSeatPrice(String comfortSeatPrice) {
		this.comfortSeatPrice = comfortSeatPrice;
	}
	
	
}
