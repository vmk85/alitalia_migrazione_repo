package com.alitalia.aem.consumer.servlet;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.PDFManager;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "createpdf" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class PDFCreatorServlet extends SlingAllMethodsServlet {

	public static final String QUERY_PARAM = "q";
	public static final String QUERY_MESSAGES = "messages";
	public static final String QUERY_BALANCE = "balance";
	public static final String QUERY_TICKET = "ticket";
	public static final String QUERY_SERVICES = "services";
	public static final String QUERY_BOOKING = "booking";
	public static final String QUERY_CHECKIN = "checkin";
	public static final String QUERY_CHECKIN_1 = "checkin-1";
	public static final String QUERY_CHECKIN_SUMMARY = "summary";
	public static final String QUERY_CARNET = "carnet";
	public static final String QUERY_ANCILLARY = "ancillary";
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Reference
	private PDFManager pdfManager;
	
	@Override
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		logger.debug("[PDFCreatorServlet] doGet");
		
		// create and return PDF to user
		String query = request.getParameter(QUERY_PARAM);
		try {

			// Forzo download PDF per il nuovo checkin
			if (query.equals(QUERY_CHECKIN_1))
			{
				response.setContentType("application/octet-stream");
				response.setHeader("Content-disposition","attachment; filename=boardingpass.pdf");
			}
			
			switch (query) {
				case QUERY_MESSAGES :
					pdfManager.manageMessagesPDF(request, response);
					break;
				case QUERY_BALANCE :
					pdfManager.manageBalancePDF(request, response);
					break;
				case QUERY_TICKET :
					pdfManager.manageTicketPDF(request, response);
					break;
				case QUERY_SERVICES :
					pdfManager.manageServicesPDF(request, response);
					break;
				case QUERY_BOOKING :
					pdfManager.manageBookingPDF(request, response);
					break;
				case QUERY_CHECKIN :
					pdfManager.manageCheckinPDF(request, response, true);
					break;
				case QUERY_CHECKIN_SUMMARY :
					pdfManager.manageCheckinSummaryPDF(request, response, true);
					break;
				case QUERY_CARNET :
					pdfManager.manageCarnetPDF(request, response);
					break;
				case QUERY_ANCILLARY :
					pdfManager.manageAncillaryPDF(request, response);
					break;
				case QUERY_CHECKIN_1 :
					OutputStream out = pdfManager.manageCheckinPDF_1(request, response, true);
					out.flush();
					break;
			}
			
		} catch (Exception e) {
			logger.error("[PDFCreatorServlet] An error occurred creating PDF", e);
		}
	}
	
}