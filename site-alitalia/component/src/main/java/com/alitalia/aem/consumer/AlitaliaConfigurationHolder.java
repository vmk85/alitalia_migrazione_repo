package com.alitalia.aem.consumer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;

import com.adobe.granite.crypto.CryptoException;
import com.adobe.granite.crypto.CryptoSupport;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSearchCategoryEnum;
import com.alitalia.aem.consumer.model.AlitaliaConfigurationModel;

/**
 * Generic configuration service used to hold and expose
 * a set of misc properties (configurable through OSGI).
 * 
 * <p>This component can be used, for example, to define system-wide 
 * properties whose value must be managed through OSGI nodes, or
 * other configurations not tied to specific components.</p>
 * 
 * <p>Properties values can be reached by page also by using
 * the <code>AlitaliaConfigurationModel</model> Sling model.</p>
 * 
 * @see AlitaliaConfigurationModel
 */

@Component(metatype = true, immediate = true)
@Service(value=AlitaliaConfigurationHolder.class)
@Properties({

	@Property(name = AlitaliaConfigurationHolder.HTTPS_ENABLED, boolValue = true),
	@Property(name = AlitaliaConfigurationHolder.ERROR_NAVIGATION_PAGE, value = "", description = "Users will be redirected to the specified page after a 404 error."),
	@Property(name = AlitaliaConfigurationHolder.LEGACY_LOGIN_ENABLED, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.LEGACY_LOGIN_AP_BASE_URL, value = ""),
	@Property(name = AlitaliaConfigurationHolder.LEGACY_LOGIN_MM_BASE_URL, value = ""),
	@Property(name = AlitaliaConfigurationHolder.SOCIAL_LOGIN_API_KEY, value = ""),
	@Property(name = AlitaliaConfigurationHolder.SOCIAL_LOGIN_ENABLED_PROVIDERS, value = ""),
	@Property(name = AlitaliaConfigurationHolder.HOME_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.INDEX_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.OFFER_DETAIL_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.SOCIAL_SHARE_ENABLED, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.SOCIAL_SHARE_ENABLED_PROVIDERS, value = ""),
	@Property(name = AlitaliaConfigurationHolder.SOCIAL_SHARE_MORE_ENABLED_PROVIDERS, value = ""),
	@Property(name = AlitaliaConfigurationHolder.SOCIAL_SHARE_OPERATION_MODE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.SOCIAL_SHARE_SHOW_MORE_BUTTON, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.SOCIAL_SHARE_SHOW_EMAIL_BUTTON, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.EGON_USER, value = ""),
	@Property(name = AlitaliaConfigurationHolder.EGON_PASSWORD, value = ""),
	@Property(name = AlitaliaConfigurationHolder.EGON_DISABLED, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CLIENT_IP_HEADERS, value = {}),
	@Property(name = AlitaliaConfigurationHolder.CONTEXT, value = ""),
	@Property(name = AlitaliaConfigurationHolder.DEFAULT_SITE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.BUY_POINTS_ENDPOINT, value = ""),
	@Property(name = AlitaliaConfigurationHolder.BUY_POINTS_KEY, value = ""),
	@Property(name = AlitaliaConfigurationHolder.BUY_POINTS_IV, value = ""),
	@Property(name = AlitaliaConfigurationHolder.BUY_POINTS_KEY_ALGORITHM, value = ""),
	@Property(name = AlitaliaConfigurationHolder.BUY_POINTS_CIPHER_CONF, value = ""),
	@Property(name = AlitaliaConfigurationHolder.TEMPLATE_MAIL_NEWSLETTER, value = ""),
	@Property(name = AlitaliaConfigurationHolder.GSA_HOST, value = ""),
	@Property(name = AlitaliaConfigurationHolder.GSA_RESULT_PER_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.GSA_PAGE_OFFSET, value = ""),
	@Property(name = AlitaliaConfigurationHolder.GSA_SITE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.GSA_CLIENT, value = ""),
	@Property(name = AlitaliaConfigurationHolder.GSA_ALLOW, value = ""),
	@Property(name = AlitaliaConfigurationHolder.META_NO_INDEX_ALLOW, value = ""),
	@Property(name = AlitaliaConfigurationHolder.PAGE_NOT_FOUND, value = ""),
	@Property(name = AlitaliaConfigurationHolder.EXTERNAL_DOMAIN, value = ""),
	@Property(name = AlitaliaConfigurationHolder.STATIC_IMAGES_PATH, value = ""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_EXTERNAL_DOMAIN, value = ""),
	@Property(name = AlitaliaConfigurationHolder.WEB_CHECKIN_URL_CHECKIN, value = ""),
	@Property(name = AlitaliaConfigurationHolder.WEB_CHECKIN_URL_CHECKIN_SEARCH_HANDLER, value = ""),
	@Property(name = AlitaliaConfigurationHolder.WEB_CHECKIN_URL_CHECKIN_REDIRECT_HANDLER, value = ""),
	@Property(name = AlitaliaConfigurationHolder.WEB_CHECKIN_URL_MYFLYGHT, value = ""),
	@Property(name = AlitaliaConfigurationHolder.WEB_CHECKIN_URL_MYFLYGHT_SEARCH_HANDLER, value = ""),
	@Property(name = AlitaliaConfigurationHolder.WEB_CHECKIN_URL_MYFLYGHT_REDIRECT_HANDLER, value = ""),
	@Property(name = AlitaliaConfigurationHolder.TEMPLATE_MAIL_WELCOME, value = ""),
    @Property(name = AlitaliaConfigurationHolder.TEMPLATE_MAIL_WELCOME_SA, value = ""),
	@Property(name = AlitaliaConfigurationHolder.BASE_PERSONAL_AREA, value = ""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_HOME_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_METASEARCH, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_FLIGHT_SEARCH_CONTINUITY_SARDINIA_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_FLIGHT_SEARCH_CONTINUITY_SICILY_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_FLIGHT_SEARCH_NO_SOLUTIONS_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_FLIGHT_SELECT_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_PASSENGERS_DATA_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_ANCILLARY_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_PAYMENT_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_PAYMENT_RETURN_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_PAYMENT_GLOBAL_COLLECT_RETURN_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_PAYMENT_FAILURE_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_PAYMENT_CANCEL_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_CONFIRMATION_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_FLIGHT_INFO_LIST_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AIRPORT_DETAILS_LIGHTBOX_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_FAILURE_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_HOME_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_FLIGHT_SEARCH_NO_SOLUTIONS_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_CALENDAR_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_FLIGHT_SELECT_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_PASSENGERS_DATA_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_ANCILLARY_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_PAYMENT_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_PAYMENT_RETURN_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_PAYMENT_GLOBAL_COLLECT_RETURN_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_PAYMENT_FAILURE_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_PAYMENT_CANCEL_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_CONFIRMATION_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_FAILURE_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.MMB_HOME_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.MMB_LIST_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.MMB_DETAIL_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.MMB_PAYMENT_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.MMB_THANKYOU_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.MMB_FAILURE_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.MIN_YEAR_ADT, description = "Lower bound for Adult passenger birth year"),
	@Property(name = AlitaliaConfigurationHolder.MAX_YEAR_ADT, description = "Upper bound for Adult passenger birth year"),
	@Property(name = AlitaliaConfigurationHolder.MIN_YEAR_CHD, description = "Lower bound for Child passenger birth year"),
	@Property(name = AlitaliaConfigurationHolder.MAX_YEAR_CHD, description = "Upper bound for Child passenger birth year."),
	@Property(name = AlitaliaConfigurationHolder.MAX_YEAR_FAM_CHD, description = "Upper bound for Child passenger birth year in family offers."),
	@Property(name = AlitaliaConfigurationHolder.MIN_YEAR_INF, description = "Lower bound for Infant passenger birth year."),
	@Property(name = AlitaliaConfigurationHolder.MAX_YEAR_INF, description = "Upper bound for Infant passenger birth year."),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_BRAND_ADT_LIST, description = "Map between brand codes and real brand for CUG ADT"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_BRAND_FAM_LIST, description = "Map between brand codes and real brand for CUG FAM"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_BRAND_MIL_LIST, description = "Map between brand codes and real brand for CUG MIL"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_BRAND_YTH_LIST, description = "Map between brand codes and real brand for CUG YTH"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_BRAND_CRN_LIST, description = "Map between brand codes and real brand for Carnet Search"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_BRAND_LIST, description = "Map between brand codes and real brand for booking award"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SEARCH_CATEGORY_DOM_MINIFARE_LIST, description = "List of brands for category DOM (light)"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SEARCH_CATEGORY_DOM_LIST, description = "List of brands for category DOM"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SEARCH_CATEGORY_FCO_LIN_LIST, description = "List of brands for category FCO_LIN"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SEARCH_CATEGORY_INTZ_MINIFARE_LIST, description = "List of brands for category INTZ (light)"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SEARCH_CATEGORY_INTZ_LIST, description = "List of brands for category INTZ"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SEARCH_CATEGORY_INTC_LIST, description = "List of brands for category INTC"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SEARCH_CATEGORY_CONTINUITY_LIST, description = "List of brands for sardinia/sicily during period of continuity"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SEARCH_CATEGORY_CONTINUITY_OUT_SARDINIA_LIST, description = "List of brands for sardinia outside period of continuity"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SEARCH_CATEGORY_CONTINUITY_REFUSE_SICILY_LIST, description = "List of brands for sicily during period of continuity and answer is no"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SEARCH_CATEGORY_CONTINUITY_REFUSE_SARDINIA_LIST, description = "List of brands for sardinia during period of continuity and answer is no"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SEARCH_CATEGORY_CARNET, description = "List of brands for carnet search"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SEARCH_CATEGORY_INFO_POPUP_AVAILABLE, description = "List of categories wich we have Brand comparison data"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SEARCH_CATEGORY_ECONOMY_SIZE, description = "Map between categories and number of economy brands for that category"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_SEARCH_CATEGORY_DEFAULT_LIST, description = "List of brands for booking award"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_AWARD_SEARCH_CATEGORY_INTZ_LIST, description = "List of brands for booking award for category INTZ"),
	@Property(name = AlitaliaConfigurationHolder.ERROR_MESSAGE_RETRIEVE_PIN_CUSTOMER_NOT_FOUND, description = "Error message returned by the service RetrievePinByNickName"),
	@Property(name = AlitaliaConfigurationHolder.MAIL_NO_REPLY_ALITALIA, description = ""),
	@Property(name = AlitaliaConfigurationHolder.BODY_MAIL_RETRIEVE_PIN, description = ""),
	@Property(name = AlitaliaConfigurationHolder.BODY_MAIL_MMB_PRENOTAZIONE, description = ""),
	@Property(name = AlitaliaConfigurationHolder.FLIGHT_INFO_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.MYFLIGHTS_PAGE, value=""),
	@Property(name = AlitaliaConfigurationHolder.SPECIAL_ASSISTANCE_KEYS, value=""),
	@Property(name = AlitaliaConfigurationHolder.SPECIAL_ASSISTANCE_WHEELCHAIR_KEYS, value=""),
	@Property(name = AlitaliaConfigurationHolder.LEGACY_MMB_ENABLED, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.MDC_CLIENT_IP_ENABLED, boolValue = true),
	@Property(name = AlitaliaConfigurationHolder.MDC_JSESSIONID_ENABLED, boolValue = true),
	@Property(name = AlitaliaConfigurationHolder.MDC_AWSELB_ENABLED, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.MDC_USERAGENT_ENABLED, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.MDC_REFERRER_ENABLED, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_FLIGHT_LIST_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_PASSENGERS_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_ANCILLARY_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_BOARDINGPASS_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_DANGEROUSGOODS_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_UNDO_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_HOME_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_FAILURE_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_DE_REGISTRATION_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_CONFIRMATION_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_SEAT_MAP_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_ANCILARYCONFIRMATION_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_INFANT_LIGHTBOX_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_SPECIAL_FARE_LIGHTBOX_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_PAYMENT_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CHECKIN_THANKYOU_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CARNET_LOGIN_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CARNET_HOME_LOGGED_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CARNET_LIST_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CARNET_PERSONAL_DATA_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CARNET_PAYMENT_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CARNET_CONFIRMATION_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CARNET_EMAIL_RICEVUTA_ACQUISTO_CARNET, value = ""),
	@Property(name = AlitaliaConfigurationHolder.CARNET_EMAIL_NOTIFICA_UTILIZZO_CARNET, value = ""),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_GB_AIRPORT, value = ""),
	@Property(name = AlitaliaConfigurationHolder.MINIFARE_SOURCE_BRAND_CODE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.MINIFARE_TARGET_BRAND_CODE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.TICKET_OFFICE_PAYMENT_DAY_LIMIT, value = "0"),
	@Property(name = AlitaliaConfigurationHolder.BOOKING_SURCHARGE_MARKETS, description = "List of markets for surcharge label"),
	@Property(name = AlitaliaConfigurationHolder.PAYMENT_ONE_CLICK_ENABLED, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.PAYMENT_ONE_CLICK_MARKETS, value = ""),
	@Property(name = AlitaliaConfigurationHolder.SPECIAL_OFFER_FEE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.SPECIAL_OFFER_FEE_MARKET, value = ""),
	@Property(name = AlitaliaConfigurationHolder.MAC_3DS_STRING, value = ""),
	@Property(name = AlitaliaConfigurationHolder.SSW_DXCI_DEEP_LINK, value = ""),
	@Property(name = AlitaliaConfigurationHolder.SSW_MTO_DEEP_LINK, value = ""),
	@Property(name = AlitaliaConfigurationHolder.SSW_DEFAULT_LANGUAGE, description = "The default language to be used instead of the not supported language"),
	@Property(name = AlitaliaConfigurationHolder.SSW_DEFAULT_COUNTRY, description = "The default country to be used with default language"),
	@Property(name = AlitaliaConfigurationHolder.SSW_NOT_SUPPORTED_LANGUAGES, description = "List of not supported languages"),
	@Property(name = AlitaliaConfigurationHolder.SSW_LANGUAGE_MAPPING, description = "list of language mapping for SSW"),
	@Property(name = AlitaliaConfigurationHolder.SSW_REDEMPTION_DEEP_LINK, value = ""),
	@Property(name = AlitaliaConfigurationHolder.FARERULES_SATIC_ON, value = ""),
	@Property(name = AlitaliaConfigurationHolder.RECAPTCHA_SITE_KEY, value= ""),
	@Property(name = AlitaliaConfigurationHolder.RECAPTCHA_SECRET_KEY, value= ""),
	@Property(name = AlitaliaConfigurationHolder.INVISIBLE_RECAPTCHA_SITE_KEY, value= ""),
	@Property(name = AlitaliaConfigurationHolder.INVISIBLE_RECAPTCHA_SECRET_KEY, value= ""),
	@Property(name = AlitaliaConfigurationHolder.EXTERNAL_LINK_CONFIG, value= ""),
	@Property(name = AlitaliaConfigurationHolder.EXTERNAL_LINK_MARKETS, value= ""),
	@Property(name = AlitaliaConfigurationHolder.FORM_SME_COUNTRIES, value = ""),
	@Property(name = AlitaliaConfigurationHolder.MM_CORPORATE_COUNTRIES_MAP, value = ""),
	@Property(name = AlitaliaConfigurationHolder.SME_ZIP_ALPHANUMERIC_COUNTRIES, value = ""),
	@Property(name = AlitaliaConfigurationHolder.IS_SPECIAL_CHAR_TO_BE_REPLACED, boolValue = true),
	@Property(name = AlitaliaConfigurationHolder.SPECIALPAGE_BUSINESSCONNECT_EMAIL_TO, value= ""),
	@Property(name = AlitaliaConfigurationHolder.SPECIALPAGE_BUSINESSCONNECT_EMAIL_FROM, value= ""),
	@Property(name = AlitaliaConfigurationHolder.SPECIALPAGE_BUSINESSCONNECT_EMAIL_SUBJECT, value= ""),
	@Property(name = AlitaliaConfigurationHolder.LISTA_VOLI_SOGGETTI_APPROVAZIONE_GOVERNATIVA, value= ""),
	@Property(name = AlitaliaConfigurationHolder.WEBCHECKIN_PAGE, value = ""),
	@Property(name = AlitaliaConfigurationHolder.SENDSMS_APPLICATION_NAME, value= ""),
	@Property(name = AlitaliaConfigurationHolder.SENDSMS_FROM_PHONE_NUMBER, value= ""),
	@Property(name = AlitaliaConfigurationHolder.SENDSMS_NOTIFICATION_SEND, value= ""),
	@Property(name = AlitaliaConfigurationHolder.SENDSMS_NOTIFICATION_LOCAL_SEND, value= ""),
	@Property(name = AlitaliaConfigurationHolder.SENDSMS_NOTIFICATION_RECEIVE, value= ""),
	@Property(name = AlitaliaConfigurationHolder.SENDSMS_NOTIFICATION_NOT_RECEIVE, value= ""),
	@Property(name = AlitaliaConfigurationHolder.MA_API_KEY, value= ""),
	@Property(name = AlitaliaConfigurationHolder.MA_SECRET_KEY, value= ""),
	@Property(name = AlitaliaConfigurationHolder.MA_USER_KEY, value= ""),
	@Property(name = AlitaliaConfigurationHolder.MYALITALIA_PAYMENT_KEY, value= ""),
	@Property(name = AlitaliaConfigurationHolder.MYALITALIA_PAYMENT_LIBRARY_URL, value= ""),
	@Property(name = AlitaliaConfigurationHolder.WIDGETADR_KEY, value= ""),

		//millemiglia SA
	@Property(name = AlitaliaConfigurationHolder.MMSA_SENDSMS_APPLICATION_NAME, value= ""),
	@Property(name = AlitaliaConfigurationHolder.MMSA_SENDSMS_FROM_PHONE_NUMBER, value= ""),
	@Property(name = AlitaliaConfigurationHolder.MMSA_SENDSMS_NOTIFICATION_SEND, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.MMSA_SENDSMS_NOTIFICATION_LOCAL_SEND, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.MMSA_SENDSMS_NOTIFICATION_RECEIVE, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.MMSA_SENDSMS_NOTIFICATION_NOT_RECEIVE, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.MMSA_SECURITY_ALERT_SENDEMAIL_ENABLE, boolValue = false),
	@Property(name = AlitaliaConfigurationHolder.MMSA_SECURITY_ALERT_SENDSMS_ENABLE, boolValue = false),
		//end millimiglia SA

})
public class AlitaliaConfigurationHolder {
	
	public static final String HTTPS_ENABLED = "https.enabled";
	public static final String NETWORK_ENABLED = "network.enabled";
	public static final String LEGACY_LOGIN_ENABLED = "legacyLogin.enabled";
	public static final String LEGACY_LOGIN_AP_BASE_URL = "legacyLogin.apBaseUrl";
	public static final String LEGACY_LOGIN_MM_BASE_URL = "legacyLogin.mmBaseUrl";
	public static final String MA_API_KEY = "myalitalia.apiKey";
	public static final String MA_SECRET_KEY = "myalitalia.secretKey";
	public static final String MA_USER_KEY = "myalitalia.userKey";

	public static final String MA_SECRET_QUESTION_DEFAULT = "myalitalia.secretQ";
	public static final String MA_SECRET_ANSWER_DEFAULT = "myalitalia.secretA";

	public static final String SOCIAL_LOGIN_API_KEY = "socialLogin.apiKey";
	public static final String SOCIAL_LOGIN_ENABLED_PROVIDERS = "socialLogin.enabledProviders";
	public static final String HOME_PAGE = "home.page";
	public static final String HOMEPAGE = "homepage";
	public static final String INDEX_PAGE = "index.page";
	public static final String OFFER_DETAIL_PAGE = "offer.detail.page";
	public static final String SOCIAL_SHARE_ENABLED = "socialShare.enabled";
	public static final String SOCIAL_SHARE_ENABLED_PROVIDERS = "socialShare.enabledProviders";
	public static final String SOCIAL_SHARE_MORE_ENABLED_PROVIDERS = "socialShare.moreEnabledProviders";
	public static final String SOCIAL_SHARE_OPERATION_MODE = "socialShare.operationMode";
	public static final String SOCIAL_SHARE_SHOW_MORE_BUTTON = "socialShare.showMoreButton";
	public static final String SOCIAL_SHARE_SHOW_EMAIL_BUTTON = "socialShare.showEmailButton";
	public static final String EGON_USER = "egon.user";
	public static final String EGON_PASSWORD = "egon.password";
	public static final String EGON_DISABLED = "egon.disabled";
	public static final String CLIENT_IP_HEADERS = "clientIp.headers";
	public static final String CONTEXT = "context";
	public static final String DEFAULT_SITE = "default.site";
	public static final String BUY_POINTS_ENDPOINT = "buy.points.endpoint";
	public static final String BUY_POINTS_KEY = "buy.points.key";
	public static final String BUY_POINTS_IV = "buy.points.iv";
	public static final String BUY_POINTS_KEY_ALGORITHM = "buy.points.keyAlgorithm";
	public static final String BUY_POINTS_CIPHER_CONF = "buy.points.cipherConf";
	public static final String ERROR_NAVIGATION_PAGE = "errorNavigation.page";
	public static final String TEMPLATE_MAIL_NEWSLETTER = "template.mail.newsletter";
	public static final String GSA_HOST = "gsa.host";
	public static final String GSA_RESULT_PER_PAGE = "gsa.result.per.page";
	public static final String GSA_PAGE_OFFSET = "gsa.page.offset";
	public static final String GSA_SITE = "gsa.site";
	public static final String GSA_CLIENT = "gsa.client";
	public static final String GSA_ALLOW = "gsa.allow";
	public static final String META_NO_INDEX_ALLOW = "meta.noIndex.allow";
	public static final String PAGE_NOT_FOUND = "page.notfound";
	public static final String EXTERNAL_DOMAIN = "external.domain";
	public static final String STATIC_IMAGES_PATH = "static.images.path";
	public static final String STATIC_IMAGES_PATH_1 = "static.images1.path";
	public static final String STATIC_IMAGES_PATH_2 = "static.images2.path";
	public static final String BOOKING_EXTERNAL_DOMAIN = "booking.external.domain";
	public static final String WEB_CHECKIN_URL_CHECKIN = "web.checkin.url.checkin";
	public static final String WEB_CHECKIN_URL_CHECKIN_SEARCH_HANDLER = "web.checkin.url.checkin.search.handler";
	public static final String WEB_CHECKIN_URL_CHECKIN_REDIRECT_HANDLER = "web.checkin.url.checkin.redirect.handler";
	public static final String WEB_CHECKIN_URL_MYFLYGHT = "web.checkin.url.myflight";
	public static final String WEB_CHECKIN_URL_MYFLYGHT_SEARCH_HANDLER = "web.checkin.url.myflight.search.handler";
	public static final String WEB_CHECKIN_URL_MYFLYGHT_REDIRECT_HANDLER = "web.checkin.url.myflight.redirect.handler";
	public static final String TEMPLATE_MAIL_WELCOME = "template.mail.welcome";
	public static final String TEMPLATE_MAIL_WELCOME_SA = "template.mail.welcome.sa";
	public static final String BASE_PERSONAL_AREA = "base.personal.area";
	public static final String BOOKING_HOME_PAGE = "booking.home.page";
	public static final String BOOKING_METASEARCH = "booking.metasearch.page";
	public static final String BOOKING_FLIGHT_SEARCH_CONTINUITY_SARDINIA_PAGE = "booking.flight-search-continuity-sardinia.page";
	public static final String BOOKING_FLIGHT_SEARCH_CONTINUITY_SICILY_PAGE = "booking.flight-search-continuity-sicily.page";
	public static final String BOOKING_FLIGHT_SEARCH_NO_SOLUTIONS_PAGE = "booking.flight-search-no-solutions.page";
	public static final String BOOKING_FLIGHT_SELECT_PAGE = "booking.flight-select.page";
	public static final String BOOKING_PASSENGERS_DATA_PAGE = "booking.passengers-data.page";
	public static final String BOOKING_ANCILLARY_PAGE = "booking.ancillary.page";
	public static final String BOOKING_PAYMENT_PAGE = "booking.payment.page";
	public static final String BOOKING_PAYMENT_RETURN_PAGE = "booking.payment-return.page";
	public static final String BOOKING_PAYMENT_GLOBAL_COLLECT_RETURN_PAGE = "booking.payment-global-collect-return.page";
	public static final String BOOKING_PAYMENT_FAILURE_PAGE = "booking.payment-failure.page";
	public static final String BOOKING_PAYMENT_CANCEL_PAGE = "booking.payment-cancel.page";
	
	public static final String BOOKING_PAYMENT_PTO_TN_ML = "booking.payment-pto.tn.mailing-list";
	public static final String BOOKING_PAYMENT_PTO_BG_ML = "booking.payment-pto.bg.mailing-list";
	public static final String BOOKING_PAYMENT_PTO_EG_ML = "booking.payment-pto.eg.mailing-list";
	public static final String BOOKING_PAYMENT_PTO_LB_ML = "booking.payment-pto.lb.mailing-list";
	public static final String BOOKING_PAYMENT_PTO_RO_ML = "booking.payment-pto.ro.mailing-list";
	public static final String BOOKING_PAYMENT_PTO_MA_ML = "booking.payment-pto.ma.mailing-list";
	public static final String BOOKING_PAYMENT_PTO_DAYS_BEFORE_DEPARTURE = "booking.payment-pto.days-before-departure";
	
	public static final String BOOKING_CONFIRMATION_PAGE = "booking.confirmation.page";
	public static final String BOOKING_FLIGHT_INFO_LIST_PAGE = "booking.flight-info-list.page";
	public static final String BOOKING_AIRPORT_DETAILS_LIGHTBOX_PAGE = "booking.airportDetailsLightbox.page";
	public static final String BOOKING_FAILURE_PAGE = "booking.failure.page";
	public static final String MMB_HOME_PAGE = "mmb.home.page";
	public static final String MMB_LIST_PAGE = "mmb.list.page";
	public static final String MMB_DETAIL_PAGE = "mmb.detail.page";
	public static final String MMB_PAYMENT_PAGE = "mmb.payment.page";
	public static final String MMB_THANKYOU_PAGE = "mmb.thankyou.page";
	public static final String MMB_FAILURE_PAGE = "mmb.failure.page";
	public static final String BOOKING_AWARD_HOME_PAGE = "booking.award.home.page";
	public static final String BOOKING_AWARD_FLIGHT_SEARCH_NO_SOLUTIONS_PAGE = "booking.award.flight-search-no-solutions.page";
	public static final String BOOKING_AWARD_CALENDAR_PAGE = "booking.award.calendar.page";
	public static final String BOOKING_AWARD_FLIGHT_SELECT_PAGE = "booking.award.flight-select.page";
	public static final String BOOKING_AWARD_PASSENGERS_DATA_PAGE = "booking.award.passengers-data.page";
	public static final String BOOKING_AWARD_ANCILLARY_PAGE = "booking.award.ancillary.page";
	public static final String BOOKING_AWARD_PAYMENT_PAGE = "booking.award.payment.page";
	public static final String BOOKING_AWARD_PAYMENT_RETURN_PAGE = "booking.award.payment-return.page";
	public static final String BOOKING_AWARD_PAYMENT_GLOBAL_COLLECT_RETURN_PAGE = "booking.award.payment-global-collect-return.page";
	public static final String BOOKING_AWARD_PAYMENT_FAILURE_PAGE = "booking.award.payment-failure.page";
	public static final String BOOKING_AWARD_PAYMENT_CANCEL_PAGE = "booking.award.payment-cancel.page";
	public static final String BOOKING_AWARD_CONFIRMATION_PAGE = "booking.award.confirmation.page";
	public static final String BOOKING_AWARD_FAILURE_PAGE = "booking.award.failure.page";
	public static final String MIN_YEAR_ADT = "min.year.adt";
	public static final String MAX_YEAR_ADT = "max.year.adt";
	public static final String MIN_YEAR_CHD = "min.year.chd";
	public static final String MAX_YEAR_CHD = "max.year.chd";
	public static final String MAX_YEAR_FAM_CHD = "max.year.fam.chd";
	public static final String MIN_YEAR_INF = "min.year.inf";
	public static final String MAX_YEAR_INF = "max.year.inf";
	public static final String BOOKING_BRAND_ADT_LIST = "booking.brand.ADT.list";
	public static final String BOOKING_BRAND_FAM_LIST = "booking.brand.FAM.list";
	public static final String BOOKING_BRAND_MIL_LIST = "booking.brand.MIL.list";
	public static final String BOOKING_BRAND_YTH_LIST = "booking.brand.YTH.list";
	public static final String BOOKING_BRAND_CRN_LIST = "booking.brand.CRN.list";
	public static final String BOOKING_AWARD_BRAND_LIST = "booking.award.brand.list";
	public static final String BOOKING_SEARCH_CATEGORY_DOM_MINIFARE_LIST = "booking.searchCategory.domesticMinifare.list";
	public static final String BOOKING_SEARCH_CATEGORY_DOM_LIST = "booking.searchCategory.domestic.list";
	public static final String BOOKING_SEARCH_CATEGORY_FCO_LIN_LIST = "booking.searchCategory.fcoLin.list";
	public static final String BOOKING_SEARCH_CATEGORY_INTZ_MINIFARE_LIST = "booking.searchCategory.internationalMinifare.list";
	public static final String BOOKING_SEARCH_CATEGORY_INTZ_LIST = "booking.searchCategory.international.list";
	public static final String BOOKING_SEARCH_CATEGORY_INTC_LIST = "booking.searchCategory.intercontinental.list";
	public static final String BOOKING_SEARCH_CATEGORY_CONTINUITY_LIST = "booking.searchCategory.continuity.list";
	public static final String BOOKING_SEARCH_CATEGORY_CONTINUITY_OUT_SARDINIA_LIST = "booking.searchCategory.continuityOutSardinia.list";
	public static final String BOOKING_SEARCH_CATEGORY_CONTINUITY_REFUSE_SICILY_LIST = "booking.searchCategory.continuityRefuseSicily.list";
	public static final String BOOKING_SEARCH_CATEGORY_CONTINUITY_REFUSE_SARDINIA_LIST = "booking.searchCategory.continuityRefuseSardinia.list";
	public static final String BOOKING_SEARCH_CATEGORY_CARNET = "booking.searchCategory.carnet.list";
	public static final String BOOKING_SEARCH_CATEGORY_INFO_POPUP_AVAILABLE = "booking.searchCategory.popupInfoAvailable.list";
	public static final String BOOKING_SEARCH_CATEGORY_ECONOMY_SIZE = "booking.searchCategory.economySize.list";
	public static final String BOOKING_AWARD_SEARCH_CATEGORY_DEFAULT_LIST = "booking.award.searchCategory.default.list";
	public static final String BOOKING_AWARD_SEARCH_CATEGORY_INTZ_LIST = "booking.award.searchCategory.international.list";
	public static final String FLIGHT_INFO_PAGE = "flight.info.page";
	public static final String FLIGHT_INFO_HOME_PAGE = "flightInfoHome.page";
	public static final String ERROR_MESSAGE_RETRIEVE_PIN_CUSTOMER_NOT_FOUND = "error.message.retrieve.pin.customer.notfound";
	public static final String MAIL_NO_REPLY_ALITALIA = "mail.noreply.alitalia";
	public static final String MAIL_CONFIRMATION_ALITALIA = "mail.confirmation.alitalia";
	public static final String BODY_MAIL_RETRIEVE_PIN = "mail.body.retrievepin";
	public static final String BODY_MAIL_MMB_PRENOTAZIONE = "mail.body.mmb.prenotazione";
	public static final String LOUNGE_AIRPORTS = "mmb.lounge.airports";
	public static final String MYFLIGHTS_PAGE = "myflights.page";
	public static final String SPECIAL_ASSISTANCE_KEYS = "special.assistance.keys";
	public static final String SPECIAL_ASSISTANCE_WHEELCHAIR_KEYS = "special.assistance.wheelchair.keys";
	public static final String LEGACY_MMB_ENABLED = "legacyMmb.enabled";
	public static final String UNICREDIT_REDIRECT_FAILURE_PAGE = "booking.unicredit.redirect.failure.page";
	public static final String MDC_CLIENT_IP_ENABLED = "mdc.clientIP.enabled";
	public static final String MDC_JSESSIONID_ENABLED = "mdc.jsessionid.enabled";
	public static final String MDC_AWSELB_ENABLED = "mdc.awselb.enabled";
	public static final String MDC_USERAGENT_ENABLED = "mdc.useragent.enabled";
	public static final String MDC_REFERRER_ENABLED = "mdc.referrer.enabled";
	public static final String CHECKIN_FLIGHT_LIST_PAGE = "checkin.flight.list.page";
	public static final String CHECKIN_SEAT_MAP_PAGE = "checkin.seat.map.page";
	public static final String CHECKIN_PASSENGERS_PAGE = "checkin.passengers.page";
	public static final String CHECKIN_DANGEROUSGOODS_PAGE = "checkin.dangerousgoods.page";
	public static final String CHECKIN_ANCILLARY_PAGE = "checkin.ancillary.page";
	public static final String CHECKIN_BOARDINGPASS_PAGE = "checkin.boardingpass.page";
	public static final String CHECKIN_UNDO_PAGE = "checkin.undo.page";
	public static final String CHECKIN_HOME_PAGE = "checkin.home.page";
	public static final String CHECKIN_SEARCH = "checkin.search";
	public static final String CHECKIN_FAILURE_PAGE = "checkin.failure.page";
	public static final String CHECKIN_CONFIRMATION_PAGE = "checkin.confirmation.page";
	public static final String CHECKIN_ANCILARYCONFIRMATION_PAGE = "checkin.ancillary.confirmation.page";
	public static final String CHECKIN_DE_REGISTRATION_PAGE = "checkin.deregistration.page";
	public static final String CHECKIN_INFANT_LIGHTBOX_PAGE = "checkin.infantLightbox.page";
	public static final String CHECKIN_SPECIAL_FARE_LIGHTBOX_PAGE = "checkin.specialFareLightbox.page";
	public static final String CHECKIN_PAYMENT_PAGE = "checkin.payment.page";
	public static final String CHECKIN_PAYMENT_REDIRECT = "checkin.payment.redirect";
	public static final String CHECKIN_THANKYOU_PAGE = "checkin.thankyou.page";
	public static final String CHECKIN_MANAGE_PAGE = "checkin.manage.page";
	public static final String CARNET_LOGIN_PAGE = "carnet.login.page";
	public static final String CARNET_HOME_LOGGED_PAGE = "carnet.home-logged.page";
	public static final String CARNET_LIST_PAGE = "carnet.carnet-list.page";
	public static final String CARNET_PERSONAL_DATA_PAGE = "carnet.personal-data.page";
	public static final String CARNET_PAYMENT_PAGE = "carnet.payment.page";
	public static final String CARNET_CONFIRMATION_PAGE = "carnet.confirmation.page";
	public static final String CARNET_EMAIL_RICEVUTA_ACQUISTO_CARNET = "carnet.email-ricevuta-acquisto-carnet.page";
	public static final String CARNET_EMAIL_NOTIFICA_UTILIZZO_CARNET = "carnet.mail.notifica-utilizzo-carnet";
	public static final String BOOKING_GB_AIRPORT = "booking.gb.airport.array";
	public static final String TICKET_OFFICE_PAYMENT_DAY_LIMIT = "payment.ticketoffice.daylimit";
	public static final String PAYMENT_ONE_CLICK_ENABLED = "payment.oneclick.enabled";
	public static final String BOOKING_SURCHARGE_MARKETS = "booking.payment.surcharge.market-list";
	public static final String PAYMENT_ONE_CLICK_MARKETS = "payment.oneclick.markets";
	public static final String MINIFARE_SOURCE_BRAND_CODE = "minifare.source.brand.code";
	public static final String MINIFARE_TARGET_BRAND_CODE = "minifare.target.brand.code";
	public static final String SPECIAL_OFFER_FEE = "specialOffer.extraFee";
	public static final String SPECIAL_OFFER_FEE_MARKET = "specialOffer.extraFee.market.array";

	public static final String MAC_3DS_STRING = "sabre.3ds.mac";
	public static final String SSW_DXCI_DEEP_LINK = "sabre.ssw.dxci.deeplink";
	public static final String SSW_MTO_DEEP_LINK = "sabre.ssw.mto.deeplink";
	public static final String SSW_REDEMPTION_DEEP_LINK = "sabre.ssw.redemption.deeplink";
	public static final String SSW_DEFAULT_LANGUAGE = "sabre.ssw.default.language";
	public static final String SSW_DEFAULT_COUNTRY = "sabre.ssw.default.country";
	public static final String SSW_NOT_SUPPORTED_LANGUAGES = "sabre.ssw.not.supported.languages";
	public static final String SSW_LANGUAGE_MAPPING = "sabre.ssw.language.mapping";
		
	public static final String SSO_COOKIE_TIMEOUT = "sso.cookie.timeout";
	public static final String SSO_COOKIE_DOMAIN = "sso.cookie.domain";
	
	public static final String FARERULES_SATIC_ON = "farerules.static.on";
	public static final String RECAPTCHA_SITE_KEY = "recaptcha.site.key";
	public static final String RECAPTCHA_SECRET_KEY = "recaptcha.secret.key";
	public static final String INVISIBLE_RECAPTCHA_SITE_KEY = "grecaptcha.invisible.site.key";
	public static final String INVISIBLE_RECAPTCHA_SECRET_KEY = "grecaptcha.invisible.secret.key";
	public static final String MDP_CDC_MARKETS = "mdp.cdc.markets";
	public static final String MDP_BRCDC_MARKETS = "mdp.brcdc.markets";
	public static final String MDP_BANK_MARKETS = "mdp.bank.markets";
	public static final String MDP_OTHER_MARKETS = "mdp.other.markets";
	public static final String MDP_CDC_ALL = "mdp.cdc.all";
	public static final String MDP_CDC = "mdp.cdc";
	public static final String MDP_BRCDC = "mdp.brcdc";
	public static final String MDP_BANK = "mdp.bank";
	public static final String MDP_OTHER = "mdp.other";
	public static final String EXTERNAL_LINK_CONFIG = "externalLink.config";
	public static final String EXTERNAL_LINK_MARKETS = "externalLink.markets";
	public static final String FORM_SME_COUNTRIES = "formSme.countries";
	public static final String MM_CORPORATE_COUNTRIES_MAP = "mmcorporate.countries";
	
	public static final String SME_ZIP_ALPHANUMERIC_COUNTRIES = "formSme.cap.country.alphanumeric";
	public static final String IS_SPECIAL_CHAR_TO_BE_REPLACED = "specialchars.submitpassenger.replace";

	public static final String SPECIALPAGE_BUSINESSCONNECT_EMAIL_TO = "specialpage.businessconnect.email.to"; 
	public static final String SPECIALPAGE_BUSINESSCONNECT_EMAIL_FROM = "specialpage.businessconnect.email.from"; 
	public static final String SPECIALPAGE_BUSINESSCONNECT_EMAIL_SUBJECT = "specialpage.businessconnect.email.subject";

	public static final String LISTA_VOLI_SOGGETTI_APPROVAZIONE_GOVERNATIVA="voli.soggetti.approvazione.governativa";
	public static final String WEBCHECKIN_PAGE = "webcheckin.page";
	public static final String SENDSMS_APPLICATION_NAME="sendsms.applicationName";
	public static final String SENDSMS_FROM_PHONE_NUMBER="sendsms.fromPhoneNumber";
	public static final String SENDSMS_NOTIFICATION_SEND="sendsms.notificationSend";
	public static final String SENDSMS_NOTIFICATION_LOCAL_SEND="sendsms.notificationLocalSend";
	public static final String SENDSMS_NOTIFICATION_RECEIVE="sendsms.notificationReceive";
	public static final String SENDSMS_NOTIFICATION_NOT_RECEIVE="sendsms.notificationNotReceive";

	public static final String MYALITALIA_TEMPLATE_EMAIL_FOLDER = "template-email";
	public static final String MAIL_CONFERMA_ISCRIZIONE_MA = "conferma-iscrizione-ma.html";
	public static final String MAIL_REGISTRAZIONE_COMPLETATA_MA = "registrazione-completata-ma.html";
	public static final String MAIL_ELIMINAZIONE_COMPLETATA_MA = "eliminazione-completata-ma.html";
    public static final String MAIL_RESET_PASSWORD_MA = "reset-password-ma.html";
	public static final String MYALITALIA_PAYMENT_LIBRARY_URL = "myalitalia.payment.libraryurl";
	public static final String MYALITALIA_PAYMENT_KEY = "myalitalia.payment.key";

    public static final String MAIL_RICHIESTA_RIMBORSO= "richiesta-rimborso.html";

	/*
	 millemiglia.sendsms.applicationName="MSA"
          millemiglia.sendsms.fromPhoneNumber="Alitalia"
          millemiglia.sendsms.notificationSend="false"
          millemiglia.sendsms.notificationLocalSend="false"
          millemiglia.sendsms.notificationReceive="false"
          millemiglia.sendsms.notificationNotReceive="false"
          millemiglia.security.alert.sendsms.enable="true"
          millemiglia.security.alert.sendemail.enable="true"
	* */

	public static final String MMSA_SENDSMS_APPLICATION_NAME="millemiglia.sendsms.applicationName";
	public static final String MMSA_SENDSMS_FROM_PHONE_NUMBER="millemiglia.sendsms.fromPhoneNumber";
	public static final String MMSA_SENDSMS_NOTIFICATION_SEND="millemiglia.sendsms.notificationSend";
	public static final String MMSA_SENDSMS_NOTIFICATION_LOCAL_SEND="millemiglia.sendsms.notificationLocalSend";
	public static final String MMSA_SENDSMS_NOTIFICATION_RECEIVE="millemiglia.sendsms.notificationReceive";
	public static final String MMSA_SENDSMS_NOTIFICATION_NOT_RECEIVE="millemiglia.sendsms.notificationNotReceive";
	public static final String MMSA_SECURITY_ALERT_SENDSMS_ENABLE="millemiglia.security.alert.sendsms.enable";
	public static final String MMSA_SECURITY_ALERT_SENDEMAIL_ENABLE="millemiglia.security.alert.sendemail.enable";

	public static final String MMSA_SECURITY_LOCK_ACCOUNT_PAGE="millemiglia.security.lock.account.page";

	public static final String TEMPLATE_MAIL_ACCOUNT_WARNING="template.mail.account.warning";

	public static final String WIDGETADR_KEY="widgetADR.key";



	@Reference
	private CryptoSupport cryptoSupport;

	public String getMyalitaliaPaymentLibraryUrl() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MYALITALIA_PAYMENT_LIBRARY_URL), "");
	}

	public String getMyalitaliaPaymentKey() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MYALITALIA_PAYMENT_KEY), "");
	}

	public String getTemplateEmailFolderMA() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MYALITALIA_TEMPLATE_EMAIL_FOLDER), "");
	}

	public String getTemplateEmailConfermaIscrizioneMA() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MAIL_CONFERMA_ISCRIZIONE_MA), "");
	}

	public String getSendsmsApplicationName() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SENDSMS_APPLICATION_NAME), "");
	}

	public String getSendsmsFromPhoneNumber() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SENDSMS_FROM_PHONE_NUMBER), "");
	}

	public Boolean getSendsmsNotificationSend() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(SENDSMS_NOTIFICATION_SEND), false);
	}

	public Boolean getSendsmsNotificationLocalSend() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(SENDSMS_NOTIFICATION_LOCAL_SEND), false);
	}

	public Boolean getSendsmsNotificationReceive() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(SENDSMS_NOTIFICATION_RECEIVE), false);
	}

	public Boolean getSendsmsNotificationNotReceive() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(SENDSMS_NOTIFICATION_NOT_RECEIVE), false);
	}

	public String getWebcheckinPage() {
		return PropertiesUtil.toString(
				getConfigurationProperty(WEBCHECKIN_PAGE), "");
	}

	public boolean getHttpsEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(HTTPS_ENABLED), true);
	}
	
	public int getSsoCookieTimeout() {
		return PropertiesUtil.toInteger(
				getConfigurationProperty(SSO_COOKIE_TIMEOUT), -1);
	}
	
	public String getSsoCookieDomain() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SSO_COOKIE_DOMAIN), "");
	}
	
	public String getErrorNavigationPage() {
		return PropertiesUtil.toString(
				getConfigurationProperty(ERROR_NAVIGATION_PAGE), "");
	}
	
	public Boolean getLegacyLoginEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(LEGACY_LOGIN_ENABLED), false);
	}
	
	public String getLegacyLoginApBaseUrl() {
		return PropertiesUtil.toString(
				getConfigurationProperty(LEGACY_LOGIN_AP_BASE_URL), "");
	}
	
	public String getLegacyLoginMmBaseUrl() {
		return PropertiesUtil.toString(
				getConfigurationProperty(LEGACY_LOGIN_MM_BASE_URL), "");
	}
	
	public String getSocialLoginApiKey() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SOCIAL_LOGIN_API_KEY), "");
	}

	public String getMaApiKey() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MA_API_KEY), "");
	}

	public String getMaSecretKey() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MA_SECRET_KEY), "");
	}

	public String getMaUserKey() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MA_USER_KEY), "");
	}

	public String getMADefaultSecretQuestion() {
		return PropertiesUtil.toString(getConfigurationProperty(MA_SECRET_QUESTION_DEFAULT), "");
	}

	public String getMADefaultSecretAnswer() {
		return PropertiesUtil.toString(getConfigurationProperty(MA_SECRET_ANSWER_DEFAULT), "");
	}

	public String getSocialLoginEnabledProviders() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SOCIAL_LOGIN_ENABLED_PROVIDERS), "");
	}
	
	public String getHomePage() {
		return PropertiesUtil.toString(
				getConfigurationProperty(HOME_PAGE), "");
	}

	public String getHomePage1() {
		return PropertiesUtil.toString(
				getConfigurationProperty(HOMEPAGE), "");
	}

	public String getIndexPage() {
		return PropertiesUtil.toString(
				getConfigurationProperty(INDEX_PAGE), "");
	}
	
	public String getOfferDetailPage() {
		return PropertiesUtil.toString(
				getConfigurationProperty(OFFER_DETAIL_PAGE), "");
	}
	
	public Boolean getSocialShareEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(SOCIAL_SHARE_ENABLED), false);
	}
	
	public String getSocialShareEnabledProviders() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SOCIAL_SHARE_ENABLED_PROVIDERS), "");
	}
	
	public String getSocialShareMoreEnabledProviders() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SOCIAL_SHARE_MORE_ENABLED_PROVIDERS), "");
	}
	
	public String getSocialShareOperationMode() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SOCIAL_SHARE_OPERATION_MODE), "");
	}
	
	public Boolean getSocialShareShowMoreBUtton() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(SOCIAL_SHARE_SHOW_MORE_BUTTON), false);
	}
	
	public Boolean getSocialShareShowEmailBUtton() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(SOCIAL_SHARE_SHOW_EMAIL_BUTTON), false);
	}
	
	public String getEgonUser() {
		return PropertiesUtil.toString(
				getConfigurationProperty(EGON_USER), "");
	}
	
	public String getEgonPassword() throws CryptoException {
		return unprotect(PropertiesUtil.toString(
				getConfigurationProperty(EGON_PASSWORD), ""));
	}
	
	public boolean getEgonDisabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(EGON_DISABLED), false);
	}
	
	public String[] getClientIpHeaders() {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty(CLIENT_IP_HEADERS), new String[0]);
	}
	
	public String getContext() {
		return PropertiesUtil.toString(
				getConfigurationProperty(CONTEXT), "");
	}
	
	public String getDefaultSite() {
		return PropertiesUtil.toString(
				getConfigurationProperty(DEFAULT_SITE), "");
	}
	
	public String getBuyPointsEndpoint() {
		return PropertiesUtil.toString(
				getConfigurationProperty(BUY_POINTS_ENDPOINT), "");
	}
	
	public String getBuyPointsKey() throws CryptoException {
		return unprotect(PropertiesUtil.toString(
				getConfigurationProperty(BUY_POINTS_KEY), ""));
	}
	
	public String getBuyPointsIV() throws CryptoException {
		return unprotect(PropertiesUtil.toString(
				getConfigurationProperty(BUY_POINTS_IV), ""));
	}
	
	public String getBuyPointsKeyAlgorithm() {
		return PropertiesUtil.toString(
				getConfigurationProperty(BUY_POINTS_KEY_ALGORITHM), "");
	}
	
	public String getBuyPointsCipherConf() {
		return PropertiesUtil.toString(
				getConfigurationProperty(BUY_POINTS_CIPHER_CONF), "");
	}
	
	public String getTemplateMailNewsletter() {
		return PropertiesUtil.toString(
				getConfigurationProperty(TEMPLATE_MAIL_NEWSLETTER), "");
	}
	
	public String getGSAHost() {
		return PropertiesUtil.toString(
				getConfigurationProperty(GSA_HOST), "");
	}
	
	public String getGSAResultPerPage() {
		return PropertiesUtil.toString(
				getConfigurationProperty(GSA_RESULT_PER_PAGE), "");
	}
	
	public String getGSAPageOffset() {
		return PropertiesUtil.toString(
				getConfigurationProperty(GSA_PAGE_OFFSET), "");
	}
	
	public String getGSASite() {
		return PropertiesUtil.toString(
				getConfigurationProperty(GSA_SITE), "");
	}
	
	public String getGSAClient() {
		return PropertiesUtil.toString(
				getConfigurationProperty(GSA_CLIENT), "");
	}
	
	public String getGSAAllow() {
		return PropertiesUtil.toString(
				getConfigurationProperty(GSA_ALLOW), "");
	}
	
	public String getMetaNoIndexAllow() {
		return PropertiesUtil.toString(
				getConfigurationProperty(META_NO_INDEX_ALLOW), "");
	}
	
	public String getBodyMailRetrievePin() {
		return PropertiesUtil.toString(
				getConfigurationProperty(BODY_MAIL_RETRIEVE_PIN), "");
	}
	
	public String getBodyMailMmbPrenotazione() {
		return PropertiesUtil.toString(
				getConfigurationProperty(BODY_MAIL_MMB_PRENOTAZIONE), "");
	}
	
	public String getPageNotFound() {
		return PropertiesUtil.toString(
				getConfigurationProperty(PAGE_NOT_FOUND), "");
	}
	
	public String getExternalDomain() {
		return PropertiesUtil.toString(
				getConfigurationProperty(EXTERNAL_DOMAIN), "");
	}
	
	public String getStaticImagesPath() {
		return PropertiesUtil.toString(
				getConfigurationProperty(STATIC_IMAGES_PATH), "");
	}

	public String getStaticImagesPath1() {
		return PropertiesUtil.toString(
				getConfigurationProperty(STATIC_IMAGES_PATH_1), "");
	}

	public String getStaticImagesPath2() {
		return PropertiesUtil.toString(
				getConfigurationProperty(STATIC_IMAGES_PATH_2), "");
	}

	public String getBookingExternalDomain() {
		return PropertiesUtil.toString(
				getConfigurationProperty(BOOKING_EXTERNAL_DOMAIN), "");
	}
	
	public String getBookingHomePage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_HOME_PAGE), "");
	}
	
	public String getBookingMetasearchPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_METASEARCH), "");
	}
	
	
	public String getWebCheckinUrlCheckin() {
		return PropertiesUtil.toString(
				getConfigurationProperty(WEB_CHECKIN_URL_CHECKIN), "");
	}
	
	public String getWebCheckinUrlCheckinSearchHandler() {
		return PropertiesUtil.toString(
				getConfigurationProperty(WEB_CHECKIN_URL_CHECKIN_SEARCH_HANDLER), "");
	}

	public String getWebCheckinUrlCheckinRedirectHandler() {
		return PropertiesUtil.toString(
				getConfigurationProperty(WEB_CHECKIN_URL_CHECKIN_REDIRECT_HANDLER), "");
	}
	
	public String getWebCheckinUrlMyFlight() {
		return PropertiesUtil.toString(
				getConfigurationProperty(WEB_CHECKIN_URL_MYFLYGHT), "");
	}
	
	public String getWebCheckinUrlMyFlightSearchHandler() {
		return PropertiesUtil.toString(
				getConfigurationProperty(WEB_CHECKIN_URL_MYFLYGHT_SEARCH_HANDLER), "");
	}
	
	public String getWebCheckinUrlMyFlightRedirectHandler() {
		return PropertiesUtil.toString(
				getConfigurationProperty(WEB_CHECKIN_URL_MYFLYGHT_REDIRECT_HANDLER), "");
	}
	
	public String getTemplateMailWelcome() {
		return PropertiesUtil.toString(
				getConfigurationProperty(TEMPLATE_MAIL_WELCOME), "");
	}

	public String getTemplateMailWelcomeSA() {
		return PropertiesUtil.toString(
				getConfigurationProperty(TEMPLATE_MAIL_WELCOME_SA), "");
	}

	public String getBasePersonalArea() {
		return PropertiesUtil.toString(
				getConfigurationProperty(BASE_PERSONAL_AREA), "");
	}
	
	public String getBookingFlightSearchContinuitySardiniaPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_FLIGHT_SEARCH_CONTINUITY_SARDINIA_PAGE), "");
	}
	
	public String getBookingFlightSearchContinuitySicilyPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_FLIGHT_SEARCH_CONTINUITY_SICILY_PAGE), "");
	}
	
	public String getBookingFlightSearchNoSolutionsPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_FLIGHT_SEARCH_NO_SOLUTIONS_PAGE), "");
	}
	
	public String getBookingFlightSelectPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_FLIGHT_SELECT_PAGE), "");
	}
	
	public String getBookingPassengersDataPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_PASSENGERS_DATA_PAGE), "");
	}
	
	public String getBookingAncillaryPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_ANCILLARY_PAGE), "");
	}
	
	public String getBookingPaymentPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_PAGE), "");
	}
	
	public String getBookingPaymentReturnPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_RETURN_PAGE), "");
	}
	
	public String getBookingPaymentGlobalCollectReturnPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_GLOBAL_COLLECT_RETURN_PAGE), "");
	}
	
	public String getBookingPaymentFailurePage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_FAILURE_PAGE), "");
	}
	
	public String getBookingPaymentCancelPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_CANCEL_PAGE), "");
	}
	
	public List<String> getBookingPaymentPTOMailingList(String market) {
		List<String> mailingList = null;
		String mlString = null;
		if(market != null){
			switch (market.toLowerCase()){
			case "tn":
				mlString = PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_PTO_TN_ML), "");
				break;
			case "bg":
				mlString = PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_PTO_BG_ML), "");
				break;
			case "eg":
				mlString = PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_PTO_EG_ML), "");
				break;
			case "lb":
				mlString = PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_PTO_LB_ML), "");
				break;
			case "ro":
				mlString = PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_PTO_RO_ML), "");
				break;
			case "ma":
				mlString = PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_PTO_MA_ML), "");
				break;
			}
		}
		if (mlString != null){
			String[] mlArr = mlString.split(";");
			mailingList = new ArrayList<String>(Arrays.asList(mlArr));
		}
		return mailingList;
	}
	
	public String getPtoDaysBeforeDeparture() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_PTO_DAYS_BEFORE_DEPARTURE), "");
	}
	
	public String getBookingConfirmationPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_CONFIRMATION_PAGE), "");
	}
	
	public String getBookingFlightInfoListPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_FLIGHT_INFO_LIST_PAGE), "");
	}
	
	public String getBookingAirportDetailsLightboxPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AIRPORT_DETAILS_LIGHTBOX_PAGE), "");
	}
	
	public String getBookingFailurePage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_FAILURE_PAGE), "");
	}
	
	public String getMmbHomePage() {
		return PropertiesUtil.toString(getConfigurationProperty(MMB_HOME_PAGE), "");
	}
	
	public String getMmbListPage() {
		return PropertiesUtil.toString(getConfigurationProperty(MMB_LIST_PAGE), "");
	}
	
	public String getMmbDetailPage() {
		return PropertiesUtil.toString(getConfigurationProperty(MMB_DETAIL_PAGE), "");
	}
	
	public String getMmbPaymentPage() {
		return PropertiesUtil.toString(getConfigurationProperty(MMB_PAYMENT_PAGE), "");
	}
	
	public String getMmbThankyouPage() {
		return PropertiesUtil.toString(getConfigurationProperty(MMB_THANKYOU_PAGE), "");
	}
	
	public String getMmbFailurePage() {
		return PropertiesUtil.toString(getConfigurationProperty(MMB_FAILURE_PAGE), "");
	}
	
	public String getBookingAwardHomePage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AWARD_HOME_PAGE), "");
	}

	public String getBookingAwardFlightSearchNoSolutionsPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AWARD_FLIGHT_SEARCH_NO_SOLUTIONS_PAGE), "");
	}

	public String getBookingAwardCalendarPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AWARD_CALENDAR_PAGE), "");
	}
	
	public String getBookingAwardFlightSelectPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AWARD_FLIGHT_SELECT_PAGE), "");
	}
	
	public String getBookingAwardPassengersDataPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AWARD_PASSENGERS_DATA_PAGE), "");
	}
	
	public String getBookingAwardAncillaryPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AWARD_ANCILLARY_PAGE), "");
	}
	
	public String getBookingAwardPaymentPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AWARD_PAYMENT_PAGE), "");
	}
	
	public String getBookingAwardPaymentReturnPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AWARD_PAYMENT_RETURN_PAGE), "");
	}
	
	public String getBookingAwardPaymentGlobalCollectReturnPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AWARD_PAYMENT_GLOBAL_COLLECT_RETURN_PAGE), "");
	}
	
	public String getBookingAwardPaymentFailurePage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AWARD_PAYMENT_FAILURE_PAGE), "");
	}
	
	public String getBookingAwardPaymentCancelPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AWARD_PAYMENT_CANCEL_PAGE), "");
	}
	
	public String getBookingAwardConfirmationPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AWARD_CONFIRMATION_PAGE), "");
	}
		
	public String getBookingAwardFailurePage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_AWARD_FAILURE_PAGE), "");
	}
	
	public String getMinYearADT() {
		return PropertiesUtil.toString(getConfigurationProperty(MIN_YEAR_ADT), "");
	}
	
	public String getMaxYearADT() {
		return PropertiesUtil.toString(getConfigurationProperty(MAX_YEAR_ADT), "");
	}
	
	public String getMinYearCHD() {
		return PropertiesUtil.toString(getConfigurationProperty(MIN_YEAR_CHD), "");
	}
	
	public String getMaxYearCHD() {
		return PropertiesUtil.toString(getConfigurationProperty(MAX_YEAR_CHD), "");
	}
	
	public String getMaxYearFAMCHD() {
		return PropertiesUtil.toString(getConfigurationProperty(MAX_YEAR_FAM_CHD), "");
	}
	
	public String getMinYearINF() {
		return PropertiesUtil.toString(getConfigurationProperty(MIN_YEAR_INF), "");
	}
	
	public String getMaxYearINF() {
		return PropertiesUtil.toString(getConfigurationProperty(MAX_YEAR_INF), "");
	}
	
	public String getCustomerNotFoundRetrievePin() {
		return PropertiesUtil.toString(getConfigurationProperty(ERROR_MESSAGE_RETRIEVE_PIN_CUSTOMER_NOT_FOUND), "");
	}
	
	public String[] getSearchCategoryBrands(BookingSearchCategoryEnum category) {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty("booking.searchCategory." + category.getValue() + ".list"), new String[0]);
	}
	
	public String[] getSearchCategoryPopupInfoAvailable() {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty(BOOKING_SEARCH_CATEGORY_INFO_POPUP_AVAILABLE), new String[0]);
	}
	
	public String[] getSearchCategoryEconomySize() {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty(BOOKING_SEARCH_CATEGORY_ECONOMY_SIZE), new String[0]);
	}
	
	public String[] getBookingAwardBrands() {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty(BOOKING_AWARD_SEARCH_CATEGORY_DEFAULT_LIST), new String[0]);
	}
	
	public String[] getBookingAwardBrandsIntz() {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty(BOOKING_AWARD_SEARCH_CATEGORY_INTZ_LIST), new String[0]);
	}
	
	public String[] getCodeBrandMap(BookingSearchCUGEnum cug) {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty("booking.brand." + cug.value() + ".list"), new String[0]);
	}
	
	public String[] getCodeBrandAwardMap() {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty(BOOKING_AWARD_BRAND_LIST), new String[0]);
	}
	
	public String getFlightInfoPage() {
		  return PropertiesUtil.toString(
		    getConfigurationProperty(FLIGHT_INFO_PAGE), "");
		  
	}
	
	public String getFlightInfoHomePage() {
		  return PropertiesUtil.toString(
		    getConfigurationProperty(FLIGHT_INFO_HOME_PAGE), "");
		  
	}
	
	public String getMailNoreplyAlitalia(){
		return PropertiesUtil.toString(getConfigurationProperty(MAIL_NO_REPLY_ALITALIA), "noreply@alitalia.com");
	}
	
	public String getMailConfirmationAlitalia(){
		return PropertiesUtil.toString(getConfigurationProperty(MAIL_CONFIRMATION_ALITALIA), "confirmation@alitalia.com");
	}
	
	public String getMyFlightsPage() {
		  return PropertiesUtil.toString(
		    getConfigurationProperty(MYFLIGHTS_PAGE), "");
		  
	}
	
	public String getSpecialAssistanceKeys() {
		  return PropertiesUtil.toString(
		    getConfigurationProperty(SPECIAL_ASSISTANCE_KEYS), "");
		  
	}
	
	public String getSpecialAssistanceWheelchairKeys() {
		  return PropertiesUtil.toString(
		    getConfigurationProperty(SPECIAL_ASSISTANCE_WHEELCHAIR_KEYS), "");
		  
	}
	
	public Boolean getLegacyMmbEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(LEGACY_MMB_ENABLED), false);
	}
	
	public String getSpecialAssistancePage() {
		return PropertiesUtil.toString(getConfigurationProperty(MMB_DETAIL_PAGE), "");
	}
	
	public String getCheckinFlightListPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_FLIGHT_LIST_PAGE), "");
	}
	
	public String getCheckinThankYouPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_THANKYOU_PAGE), "");
	}

	public String getCheckinHomePage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_HOME_PAGE), "");
	}
	
	public String getCheckinSearch() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_SEARCH), "");
	}

	public String getCheckinPassengersPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_PASSENGERS_PAGE), "");
	}

	public String getCheckinSeatMapPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_SEAT_MAP_PAGE), "");
	}

	public String getCheckinDangerousGoodsPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_DANGEROUSGOODS_PAGE), "");
	}

	public String getCheckinAncillaryPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_ANCILLARY_PAGE), "");
	}
	
	public String getCheckinBoardingPassPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_BOARDINGPASS_PAGE), "");
	}
	
	public String getCheckinUndoPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_UNDO_PAGE), "");
	}
	
	public String getCheckinFailurePage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_FAILURE_PAGE), "");
	}
	public String getCheckinDeregistrationPage(boolean extension){
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_DE_REGISTRATION_PAGE), "")+(extension ? ".html" : "");
	}
	
	public String getCheckinPaymentPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_PAYMENT_PAGE), "");
	}
	
	public String getCheckinPaymentRedirectPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_PAYMENT_REDIRECT), "");
	}

	public String getCheckinConfirmationPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_CONFIRMATION_PAGE), "");
	}
	
	public String getCheckinAncillaryConfirmationPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_ANCILARYCONFIRMATION_PAGE), "");
	}
	
	public String getCheckinInfantLightboxPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_INFANT_LIGHTBOX_PAGE), "");
	}
	
	public String getCheckinSpeciaFareLightboxPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_SPECIAL_FARE_LIGHTBOX_PAGE), "");
	}

	public String getCheckinManagePage() {
		return PropertiesUtil.toString(getConfigurationProperty(CHECKIN_MANAGE_PAGE), "");
	}

	public String getCarnetLoginPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CARNET_LOGIN_PAGE), "");
	}
	
	public String getCarnetHomeLoggedPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CARNET_HOME_LOGGED_PAGE), "");
	}
	
	public String getCarnetListPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CARNET_LIST_PAGE), "");
	}
	
	public String getCarnetPersonalDataPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CARNET_PERSONAL_DATA_PAGE), "");
	}
	
	public String getCarnetPaymentPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CARNET_PAYMENT_PAGE), "");
	}
	
	public String getCarnetConfirmationPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CARNET_CONFIRMATION_PAGE), "");
	}
	
	public String getCarnetMailRicevutaAcquistoCarnetPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CARNET_EMAIL_RICEVUTA_ACQUISTO_CARNET), "");
	}
	
	public String getCarnetMailNotificaUtilizzoCarnetPage() {
		return PropertiesUtil.toString(getConfigurationProperty(CARNET_EMAIL_NOTIFICA_UTILIZZO_CARNET), "");
	}
	
	public String get3dsMacString() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MAC_3DS_STRING), "");
	}
	
	/*Sabre Sonic Web Deeplinks*/
	
	public String getSSWDXCIDeepLink() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SSW_DXCI_DEEP_LINK), "");
	}
	
	public String getSSWMTODeepLink() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SSW_MTO_DEEP_LINK), "");
	}
	
	public String getSSWRedemptionDeepLink() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SSW_REDEMPTION_DEEP_LINK), "");
	}	
	
	/* utility methods */
	
	public String getStringProperty(String propertyName){
		return PropertiesUtil.toString(
				getConfigurationProperty(propertyName), "");
	}
	
	public Map<String, String> getMapProperty(String propertyName){
		return createMapFromProperty(propertyName);
	}
	
	public Set<String> getSetProperty(String propertyName){
		return createSetFromProperty(propertyName);
	}
	
	/**
     * Utility method to obtain the home page, based on configuration,
     * with the .html extension applied.
     */
	public String getHomePageWithExtension() {
		return PropertiesUtil.toString(
				getConfigurationProperty(HOME_PAGE) + ".html", "");
	}
	
	/**
     * Utility method to obtain the offer detail page, based on configuration,
     * with the .html extension applied.
     */
	public String getOfferDetailPageWithExtension() {
		return PropertiesUtil.toString(
				getConfigurationProperty(OFFER_DETAIL_PAGE) + ".html", "");
	}
	
	public String getLoungeAirports() {
		  return PropertiesUtil.toString(
		    getConfigurationProperty(LOUNGE_AIRPORTS), "");
		  
	}
	
	public String getBookingUnicreditRedirectFailurePage() {
		  return PropertiesUtil.toString(
		    getConfigurationProperty(UNICREDIT_REDIRECT_FAILURE_PAGE), "");
	}
		
	public boolean getMdcClientIpEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(MDC_CLIENT_IP_ENABLED), true);
	}
	
	public String[] getGBAirports() {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty(BOOKING_GB_AIRPORT), new String[0]);
	}
	
	public boolean getMdcJsessionIdEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(MDC_JSESSIONID_ENABLED), true);
	}
	
	public boolean getMdcAwsElbEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(MDC_AWSELB_ENABLED), false);
	}
	
	public boolean getMdcUserAgentEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(MDC_USERAGENT_ENABLED), false);
	}
	
	public boolean getMdcReferrerEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(MDC_REFERRER_ENABLED), false);
	}
	
	public int getTicketOfficePaymentDayLimit() {
		return PropertiesUtil.toInteger(
				getConfigurationProperty(TICKET_OFFICE_PAYMENT_DAY_LIMIT), 0);
	}
	
	public boolean getPaymentOneClickEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(PAYMENT_ONE_CLICK_ENABLED), false);
	}
	
	public String[] getBookingSurchargeMarkets() {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty(BOOKING_SURCHARGE_MARKETS), new String[0]);
	}
	
	
	public String[] getOneClickMarkets() {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty(PAYMENT_ONE_CLICK_MARKETS), new String[0]);
	}
	
	public String getMinifareSourceBrandCode() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MINIFARE_SOURCE_BRAND_CODE), "");
	}
	
	public String getMinifareTargetBrandCode() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MINIFARE_TARGET_BRAND_CODE), "");
	}
	
	public String getFeeSpecialOffers() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SPECIAL_OFFER_FEE) , "");
	}
	
	public String[] getFeeSpecialOffersMarket() {
		return PropertiesUtil.toStringArray(
				getConfigurationProperty(SPECIAL_OFFER_FEE_MARKET) , new String[0]);
	}
	
	public boolean isFareRulesStaticOn() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(FARERULES_SATIC_ON), false);
	}
	
	public String getRecaptchaSiteKey() {
		return PropertiesUtil.toString(
				getConfigurationProperty(RECAPTCHA_SITE_KEY) , "");
	}
	
	public String getRecaptchaSecretKey() {
		return PropertiesUtil.toString(
				getConfigurationProperty(RECAPTCHA_SECRET_KEY) , "");
	}
	
	public String getInvisibleRecaptchaSiteKey() {
		return PropertiesUtil.toString(
				getConfigurationProperty(INVISIBLE_RECAPTCHA_SITE_KEY) , "");
	}

	public String getInvisibleRecaptchaSecretKey() {
		return PropertiesUtil.toString(
				getConfigurationProperty(INVISIBLE_RECAPTCHA_SECRET_KEY) , "");
	}

	public String getMdPCdCMarkets() {
		return PropertiesUtil.toString(getConfigurationProperty(MDP_CDC_MARKETS), "");
	}
	
	public String getMdPBrCdCMarkets() {
		return PropertiesUtil.toString(getConfigurationProperty(MDP_BRCDC_MARKETS), "");
	}
	
	public String getMdPBankMarkets() {
		return PropertiesUtil.toString(getConfigurationProperty(MDP_BANK_MARKETS), "");
	}
	
	public String getMdPOtherMarkets() {
		return PropertiesUtil.toString(getConfigurationProperty(MDP_OTHER_MARKETS), "");
	}
	
	public String getMdPCdCAll() {
		return PropertiesUtil.toString(getConfigurationProperty(MDP_CDC_ALL), "");
	}
	
	public String getMdPCdC(String market) {
		return PropertiesUtil.toString(getConfigurationProperty(MDP_CDC+"."+market), "");
	}
	
	public String getMdPBrCdC(String market) {
		return PropertiesUtil.toString(getConfigurationProperty(MDP_BRCDC+"."+market), "");
	}
	
	public String getMdPBank(String market) {
		return PropertiesUtil.toString(getConfigurationProperty(MDP_BANK+"."+market), "");
	}
	
	public String getMdPOther(String market) {
		return PropertiesUtil.toString(getConfigurationProperty(MDP_OTHER+"."+market), "");
	}
	
	public String getExternalLinkConfig() {
		return PropertiesUtil.toString(getConfigurationProperty(EXTERNAL_LINK_CONFIG), "");
	}
	
	public String getExternalLinkMarkets() {
		return PropertiesUtil.toString(getConfigurationProperty(EXTERNAL_LINK_MARKETS), "");
	}
	
	public String getFormSmeCountries() {
		return PropertiesUtil.toString(getConfigurationProperty(FORM_SME_COUNTRIES), "");
	}
	
	public String getMmCorporateCountriesMap() {
		return PropertiesUtil.toString(getConfigurationProperty(MM_CORPORATE_COUNTRIES_MAP), "");
	}
	
	public boolean getNetworkEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(NETWORK_ENABLED), true);
	}
	
	public String getSmeZipAlphanumericCountries() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SME_ZIP_ALPHANUMERIC_COUNTRIES), "");
	}
	
	public boolean getSpecialCharactersToBeReplaced() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(IS_SPECIAL_CHAR_TO_BE_REPLACED), true);
	}

	public String getSpecialpageBusinessConnectEmailTo() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SPECIALPAGE_BUSINESSCONNECT_EMAIL_TO) , "");
	}
	
	public String getSpecialpageBusinessConnectEmailFrom() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SPECIALPAGE_BUSINESSCONNECT_EMAIL_FROM) , "");
	}
	
	public String getSpecialpageBusinessConnectEmailSubject() {
		return PropertiesUtil.toString(
				getConfigurationProperty(SPECIALPAGE_BUSINESSCONNECT_EMAIL_SUBJECT) , "");
	}

	public String getListaVoliSoggettiApprovazioneGovernativa() {
		return PropertiesUtil.toString(
				getConfigurationProperty(LISTA_VOLI_SOGGETTI_APPROVAZIONE_GOVERNATIVA) , "");
	}

	public String getWidgetadrKey() {
		return PropertiesUtil.toString(
				getConfigurationProperty(WIDGETADR_KEY) , "");
	}

	// millemiglia SA

	/*
in apps/bundle-fe-web-component-alitalia/config.prod-final.author/com.alitalia.aem.consumer.AlitaliaConfigurationHolder.xml
 millemiglia.sendsms.applicationName="MSA"
          millemiglia.sendsms.fromPhoneNumber="Alitalia"
          millemiglia.sendsms.notificationSend="false"
          millemiglia.sendsms.notificationLocalSend="false"
          millemiglia.sendsms.notificationReceive="false"
          millemiglia.sendsms.notificationNotReceive="false"
          millemiglia.security.alert.sendsms.enable="true"
          millemiglia.security.alert.sendemail.enable="true"
* */

	public String getMMSASendsmsApplicationName() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MMSA_SENDSMS_APPLICATION_NAME), "");
	}

	public String getMMSASendsmsFromPhoneNumber() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MMSA_SENDSMS_FROM_PHONE_NUMBER), "");
	}

	public Boolean getMMSASendsmsNotificationSend() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(MMSA_SENDSMS_NOTIFICATION_SEND), false);
	}

	public Boolean getMMSASendsmsNotificationLocalSend() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(MMSA_SENDSMS_NOTIFICATION_LOCAL_SEND), false);
	}

	public Boolean getMMSASendsmsNotificationReceive() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(MMSA_SENDSMS_NOTIFICATION_RECEIVE), false);
	}

	public Boolean getMMSASendsmsNotificationNotReceive() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(MMSA_SENDSMS_NOTIFICATION_NOT_RECEIVE), false);
	}


	public Boolean getMMSASecurityAlertSendEmailEnable() {
		return PropertiesUtil.toBoolean(
			getConfigurationProperty(MMSA_SECURITY_ALERT_SENDEMAIL_ENABLE), true);
	}

	public Boolean getMMSASecurityAlertSendSmsEnable() {
		return PropertiesUtil.toBoolean(
			getConfigurationProperty(MMSA_SECURITY_ALERT_SENDSMS_ENABLE), true);
	}

	public String getMMSALockAccountPage() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MMSA_SECURITY_LOCK_ACCOUNT_PAGE), "");
	}

	public String getMMSATemplateMailAccountWarning() {
		return PropertiesUtil.toString(
				getConfigurationProperty(TEMPLATE_MAIL_ACCOUNT_WARNING), "");
	}


	//end - millemiglia SA


	/* internals */
	
	/*
	 * Create set from property
	 */
	protected Set<String> createSetFromProperty(String propertyName) {
		Set<String> set = new HashSet<String>();
		String propertyValue = PropertiesUtil.toString(getConfigurationProperty(propertyName), "");
		if(!propertyValue.isEmpty()){
			String[] setElements = propertyValue.split(",");
			for (String setElement : setElements) {
				set.add(setElement);
			}
		}
		return set;
	}
	
	/*
	 * Create map from property
	 */
	protected Map<String,String> createMapFromProperty(String propertyName) {
		Map<String,String> map = new HashMap<String,String>();
		String propertyValue = PropertiesUtil.toString(getConfigurationProperty(propertyName), "");
		if(!propertyValue.isEmpty()){
			String[] entryList = propertyValue.split(",");
			for (String entry : entryList) {
				String[] entrySplit = entry.split("=");
				map.put(entrySplit[0], entrySplit[1]);
			}
		}
		return map;
	}
	
	private ComponentContext ctx = null;
	
	protected Object getConfigurationProperty(String propertyName) {
		if (ctx != null) {
			return ctx.getProperties().get(propertyName);
		}
		return null;
	}
	
	private String unprotect(String protectedValue) throws CryptoException {
		if (!protectedValue.startsWith("{")) {
			protectedValue = "{" + protectedValue + "}";
		}
		return cryptoSupport.unprotect(protectedValue);
	}
	
	@Activate
	protected void activate(final ComponentContext ctx) {
		this.ctx = ctx;
	}	
	
}