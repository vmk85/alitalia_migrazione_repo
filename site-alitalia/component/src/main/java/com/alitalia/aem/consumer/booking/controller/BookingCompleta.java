package com.alitalia.aem.consumer.booking.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import com.alitalia.aem.consumer.model.AlitaliaConfigurationModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingCompleta extends BookingSessionGenericController{

	private String redirectFailurePage = "";
	
	@Inject
	private AlitaliaConfigurationModel configuration;

	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			redirectFailurePage = ""; //FIXME configuration.getBookingConfirmPage() + "?showCallCenterMsg=true";
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			redirectFailurePage = ""; //FIXME configuration.getBookingConfirmPage() + "?showCallCenterMsg=true";
		}
	}

	public String getRedirectFailurePage() {
		return redirectFailurePage;
	}
	
}
