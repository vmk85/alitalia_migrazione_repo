package com.alitalia.aem.consumer.checkin.render;

import java.util.Calendar;

import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinUtils;

public class CheckinFlightRender {
	
	private CheckinFlightData flight;
	private DateRender departureDate;
	private DateRender arrivalDate;
	private String flightClass;
	private String fromInfo;
	private String toInfo;
	
	public CheckinFlightRender(CheckinFlightData flight, CheckinSessionContext ctx) {
		super();
		this.flight = flight;
		this.departureDate = new DateRender(flight.getDepartureDateTime(), ctx.market);
		// check date validity (service returns 1/1/1 00:00 when arrivalDate is available
		if (flight.getArrivalDateTime().get(Calendar.YEAR) > 1) {
			this.arrivalDate = new DateRender(flight.getArrivalDateTime(), ctx.market);
		}
		flightClass = CheckinUtils.getCompartimentalClass(flight.getSeatClass(), flight);
		fromInfo = flight.isFromBusStation() ? " - " + ctx.i18n.get("checkin.bus.operatoDa.label") : "";
		toInfo = flight.isToBusStation() ? " - " + ctx.i18n.get("checkin.bus.operatoDa.label") : "";
	}

	public CheckinFlightData getFlight() {
		return flight;
	}
	
	public void setFlight(CheckinFlightData flight) {
		this.flight = flight;
	}
	
	public DateRender getDepartureDate() {
		return departureDate;
	}
	
	public void setDepartureDate(DateRender departureDate) {
		this.departureDate = departureDate;
	}
	
	public DateRender getArrivalDate() {
		return arrivalDate;
	}
	
	public void setArrivalDate(DateRender arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getFlightClass() {
		return flightClass;
	}

	public void setFlightClass(String flightClass) {
		this.flightClass = flightClass;
	}

	public String getFromInfo() {
		return fromInfo;
	}

	public void setFromInfo(String fromInfo) {
		this.fromInfo = fromInfo;
	}

	public String getToInfo() {
		return toInfo;
	}

	public void setToInfo(String toInfo) {
		this.toInfo = toInfo;
	}
	
}
