package com.alitalia.aem.consumer.booking.model.extrabaggage;


import java.util.ArrayList;

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
///#region [References]
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
///#endregion
/** 
 Response Model for Ancillaries
*/
public class AncillariesGetResponse
{
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
		///#region [Properties]
	private boolean extraBaggageAdmitted;
	private ArrayList<AncillariesPassenger> ancillariesPassenger;
	private ArrayList<AncillaryPassenger> ancillaryPassengers;
	private Itinerary itinerary;
	private ArrayList<Ancillary> ancillaries;
	private ArrayList<AncillaryGroup> ancillaryGroups;
	private ArrayList<OldAncillary> notPreselectedAncillaries;
	private ArrayList<MessageAncillaryResponse> message;
	
	public final boolean getextraBaggageAdmitted()
	{
		return extraBaggageAdmitted;
	}
	
	public final void setextraBaggageAdmitted(boolean value)
	{
		extraBaggageAdmitted = value;
	}	

	public final ArrayList<AncillariesPassenger> getAncillariesPassenger()
	{
		return ancillariesPassenger;
	}
	
	public final void setAncillariesPassenger(ArrayList<AncillariesPassenger> value)
	{
		ancillariesPassenger = value;
	}
	
	public final ArrayList<AncillaryPassenger> getancillaryPassengers()
	{
		return ancillaryPassengers;
	}
	
	public final void setancillaryPassengers(ArrayList<AncillaryPassenger> value)
	{
		ancillaryPassengers = value;
	}
	
	public final Itinerary getitinerary()
	{
		return itinerary;
	}
	
	public final void setitinerary(Itinerary value)
	{
		itinerary = value;
	}
	
	public final ArrayList<Ancillary> getancillaries()
	{
		return ancillaries;
	}
	
	public final void setancillaries(ArrayList<Ancillary> value)
	{
		ancillaries = value;
	}
		
	public final ArrayList<AncillaryGroup> getancillaryGroups()
	{
		return ancillaryGroups;
	}
	
	public final void setancillaryGroups(ArrayList<AncillaryGroup> value)
	{
		ancillaryGroups = value;
	}
		
	public final ArrayList<OldAncillary> getnotPreselectedAncillaries()
	{
		return notPreselectedAncillaries;
	}
	
	public final void setnotPreselectedAncillaries(ArrayList<OldAncillary> value)
	{
		notPreselectedAncillaries = value;
	}
	
	public final ArrayList<MessageAncillaryResponse> getmessage()
	{
		return message;
	}
	
	public final void setmessage(ArrayList<MessageAncillaryResponse> value)
	{
		message = value;
	}
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
		///#endregion
}