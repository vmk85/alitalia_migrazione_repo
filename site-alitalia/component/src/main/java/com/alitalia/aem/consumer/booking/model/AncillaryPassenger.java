package com.alitalia.aem.consumer.booking.model;

public class AncillaryPassenger {
	private String name;
	private String surname;
	private String secondName;
	private String mealPreference;
	private boolean isInfantReferenced;
	private boolean isChild;

	private Passenger infantReferenced;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	public String getMealPreference() {
		return mealPreference;
	}
	public void setMealPreference(String mealPreference) {
		this.mealPreference = mealPreference;
	}
	public boolean isInfantReferenced() {
		return isInfantReferenced;
	}
	public void setIsInfantReferenced(boolean isInfantReferenced) {
		this.isInfantReferenced = isInfantReferenced;
	}
	public boolean isChild() {
		return isChild;
	}
	public void setIsChild(boolean isChild) {
		this.isChild = isChild;
	}


	public Passenger getInfantReferenced() {
		return infantReferenced;
	}
	public void setInfantReferenced(Passenger infantReferenced) {
		this.infantReferenced = infantReferenced;
	}


	@Override
	public String toString() {
		return "AncillaryPassenger [name=" + name + ", surname=" + surname
				+ ", mealPreference=" + mealPreference + ", hasInfant="
				+ isInfantReferenced +  ", isChild="
						+ isChild +"]";
	}
	
	

}
