package com.alitalia.aem.consumer.booking.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.MMTelephoneData;
import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.data.home.enumerations.MMPhoneTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.model.ContactType;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.render.AvailableFlightsSelectionRender;
import com.alitalia.aem.consumer.bookingaward.BookingAwardSession;
import com.alitalia.aem.consumer.millemiglia.render.DateRender;
import com.alitalia.aem.consumer.millemiglia.render.ProfiloUtenteRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingDati extends BookingSessionGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private SlingHttpServletResponse response;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	@Inject
	private BookingAwardSession bookingAwardSession;

	private String nameLoggedUser;
	private String surnameLoggedUser;
	private String frequentFlyerCodeLoggedUser;
	private String dayOfBirthLoggedUser;
	private String monthOfBirthLoggedUser;
	private String yearOfBirthLoggedUser;
	private String genderLoggedUser;
	private String emailLoggedUser;
	private String phonePrefixLoggedUser;
	private String phoneTypeLoggedUser;
	private String phoneLoggedUser;
	
	private String inputSelectDefaultValue;
	private String[] numAdulti;
	private String[] numBambini;
	private String[] numNeonati;
	private Boolean isArgentina;
	private Boolean isSecureFlight;
	private Boolean isSecureFlightESTA;
	private Boolean isApis;
	private Boolean isTariffaLight;
	private boolean award;
	private List<AvailableFlightsSelectionRender> flightSelectionsDetails;
	private List<CountryData> countries;
	private List<CountryData> countriesCuitCuil;
	private List<ContactType> contactTypes;
	private List<PhonePrefixData> phonePrefixes;
	private List<MealData> mealTypes;
	private List<FrequentFlyerTypeData> frequentFlyerTypes;
	private boolean multileg;
	private String cug;
	private int minYearAdt;
	private int maxYearAdt;
	private int minYearChd;
	private int maxYearChd;
	private int minYearInf;
	private int maxYearInf;
	private boolean awardMessageDisplay;
	private Long realAwardPrice;
	private String defaultPhoneType;
	private String defaultPrefix;
	private static final String DEFAULT_PREFIX_VALUE="39";
	private boolean metasearch;
	private boolean busCarrier;
	
	private String infantLabelRTEContent;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private String kayakclickid;

	//variabili per cripting widget ADR
    private String encryptionKey = "";
    private static final String characterEncoding       = "UTF-8";
    private static final String cipherTransformation    = "AES/CBC/PKCS5PADDING";
    private static final String aesEncryptionAlgorithem = "AES";

    private String cryptedMMcode = "";
    private boolean isNotEmptyMMcode = false;

	@PostConstruct
	protected void initModel() throws IOException {
		
		try {
			
			initBaseModel(request);
			
			//TODO Scommentare ctx.market.equalsIgnoreCase("ar") in caso di reintroduzione parametri CUIT/CUIL per Sito AR
			this.isArgentina = false; //ctx.market.equalsIgnoreCase("ar"); 
		
			this.busCarrier = ctx.busIsSelected;
			this.nameLoggedUser = "";
	    	this.surnameLoggedUser = "";
	    	this.frequentFlyerCodeLoggedUser = "";
	    	this.dayOfBirthLoggedUser = "";
	    	this.monthOfBirthLoggedUser = "";
	    	this.yearOfBirthLoggedUser = "";
	    	this.genderLoggedUser = "";
	    	this.emailLoggedUser = "";
	    	this.phoneLoggedUser = "";
	    	this.phonePrefixLoggedUser = "";
	    	this.phoneTypeLoggedUser = "";
	    	
	    	this.isApis = false;
	    	this.isSecureFlight = false;
	    	this.isSecureFlightESTA = false;
	    	
	    	if (ctx != null) {
	    		
	    		// Recupero informazioni sull'utente loggato
				MMCustomerProfileData customerProfileData = ctx.loggedUserProfileData;
				if (customerProfileData != null) {
			    	ProfiloUtenteRender profiloUtenteRender = new ProfiloUtenteRender(customerProfileData);
			    	this.nameLoggedUser = profiloUtenteRender.getCustomerData().getCustomerName();
			    	this.surnameLoggedUser = profiloUtenteRender.getCustomerData().getCustomerSurname();
			    	this.frequentFlyerCodeLoggedUser = StringUtils.stripStart(profiloUtenteRender.getCustomerData().getCustomerNumber(), "0");
			    	
			    	this.emailLoggedUser = profiloUtenteRender.getEmail() == null ? "" :
			    			profiloUtenteRender.getEmail().getEmail();
			    	
			    	DateRender dateRender = new DateRender(profiloUtenteRender.getCustomerData().getBirthDate());
			    	this.dayOfBirthLoggedUser = dateRender.getDay();
			    	this.monthOfBirthLoggedUser = dateRender.getMonth();
			    	this.yearOfBirthLoggedUser = dateRender.getYear();
			    	
			    	this.genderLoggedUser = profiloUtenteRender.getCustomerData().getGender() == null ? "" :
			    			profiloUtenteRender.getCustomerData().getGender().value();
			    	
			    	MMTelephoneData userTelephone = profiloUtenteRender.getTelephone();
					if (userTelephone != null) {
						if (userTelephone.getTelephoneType().equals(MMPhoneTypeEnum.BUSINESS_FAX)
								|| userTelephone.getTelephoneType().equals(MMPhoneTypeEnum.BUSINESS_PHONE)) {
							this.phoneTypeLoggedUser = ContactType.OFFICE.name();
				    	} else if (userTelephone.getTelephoneType().equals(MMPhoneTypeEnum.HOME_FAX)
								|| userTelephone.getTelephoneType().equals(MMPhoneTypeEnum.HOME_PHONE)) {
				    		this.phoneTypeLoggedUser = ContactType.HOME.name();
				    	} else if (userTelephone.getTelephoneType().equals(MMPhoneTypeEnum.MOBILE_BUSINESS)
								|| userTelephone.getTelephoneType().equals(MMPhoneTypeEnum.MOBILE_HOME)) {
				    		this.phoneTypeLoggedUser = ContactType.MOBILE.name();
				    	} else if (userTelephone.getTelephoneType().equals(MMPhoneTypeEnum.STATEMENT_PHONE)
								|| userTelephone.getTelephoneType().equals(MMPhoneTypeEnum.TEMPORARY_PHONE)
								|| userTelephone.getTelephoneType().equals(MMPhoneTypeEnum.UNKNOWN)) {
				    		this.phoneTypeLoggedUser = ContactType.HOTEL.name();
				    	}
						this.phonePrefixLoggedUser = userTelephone.getCountryNumber();
				    	this.phoneLoggedUser = userTelephone.getNumber();
				    	
				    	defaultPhoneType=this.phoneTypeLoggedUser;
				    	defaultPrefix=this.phonePrefixLoggedUser;
			    	}
				}else{
					defaultPhoneType="";
					defaultPrefix="";
//					if(marketCode.equalsIgnoreCase("IT")){
						defaultPhoneType=ContactType.MOBILE.name();
//						defaultPrefix=DEFAULT_PREFIX_VALUE;
						defaultPrefix = marketCode.toUpperCase();
//					}
					
					
		    	}
				
				
				this.inputSelectDefaultValue = BookingConstants.INPUT_SELECT_DEFAULT_VALUE;
				
				this.isSecureFlight = ctx.isSecureFlight;
				this.isSecureFlightESTA = ctx.isSecureFlightESTA;
				this.isApis = ctx.isApis;
				this.multileg = ctx.searchKind == BookingSearchKindEnum.MULTILEG;
				this.cug = ctx.cug == null ? null : ctx.cug.value();
				this.isTariffaLight = false;
				for (FlightSelection flightSelection : ctx.flightSelections) {
					if (flightSelection.isTariffaLight()) {
						this.isTariffaLight = true;
						break;
					}
				}
				this.award = ctx.award;
				
				int adulti = 0;
				if (ctx.searchPassengersNumber.getNumYoung() > 0) {
					adulti = ctx.searchPassengersNumber.getNumYoung();
				} else {
					adulti = ctx.searchPassengersNumber.getNumAdults();
				}
				int bambini = ctx.searchPassengersNumber.getNumChildren();
				int neonati = ctx.searchPassengersNumber.getNumInfants();
				
				int numeroPasseggeri = 1; // l'applicant (cioe' il primo adulto) è sempre presente
				
				/* 
				 * I tre array numAdulti, numBambini e numNeonati servono per gestire il numero di form di inserimento dati
				 * relative ad ognuno di questo tipo di passeggero. Almeno un passeggero adulto sarà sempre presente.
				 * Il valore degli array è il numero progressivo del passeggero.
				 */
				this.numAdulti = new String[adulti];
				this.numAdulti[0] = "1";
				for (int i = 1; i < (numAdulti.length); i++) { 
					numeroPasseggeri++;
					numAdulti[i] = Integer.toString(numeroPasseggeri);
				}
				this.numBambini = new String[bambini];
				for (int i = 0; i < numBambini.length; i++) {
					numeroPasseggeri++;
					numBambini[i] = Integer.toString(numeroPasseggeri);
				}
				this.numNeonati = new String[neonati];
				for (int i = 0; i < numNeonati.length; i++) {
					numeroPasseggeri++;
					numNeonati[i] = Integer.toString(numeroPasseggeri);
				}
				
				this.contactTypes = new ArrayList<ContactType>();
				for (String contactType : ContactType.listValues()) {
					this.contactTypes.add(ContactType.fromValue(contactType));
				}
				
				this.countries = new ArrayList<CountryData>();
				// filtro le country che non hanno traduzione nell'i18n
				for (CountryData country : ctx.countries) {
					String countryI18nKey = "countryData." + country.getCode();
					String countryI18nDescription = i18n.get(countryI18nKey);
					if (!countryI18nKey.equals(countryI18nDescription)) {
						CountryData countryDataI18n = new CountryData();
						countryDataI18n.setCode(country.getCode());
						countryDataI18n.setStateCode(country.getStateCode());
						countryDataI18n.setStateDescription(country.getStateDescription());
						countryDataI18n.setDescription(countryI18nDescription);
						this.countries.add(countryDataI18n);
					}
				}
				
				this.countriesCuitCuil = new ArrayList<CountryData>();
				// filtro le country che non hanno traduzione nell'i18n
				for (CountryData country : ctx.countries) {
					String countryI18nKey = "countryData." + country.getCode();
					String countryI18nDescription = i18n.get(countryI18nKey);
					if (!countryI18nKey.equals(countryI18nDescription) && !country.getCode().equalsIgnoreCase("ar")) {
						CountryData countryDataI18n = new CountryData();
						countryDataI18n.setCode(country.getCode());
						countryDataI18n.setStateCode(country.getStateCode());
						countryDataI18n.setStateDescription(country.getStateDescription());
						countryDataI18n.setDescription(countryI18nDescription);
						this.countriesCuitCuil.add(countryDataI18n);
					}
				}
				
				this.phonePrefixes = new ArrayList<PhonePrefixData>();
				// filtro i prefissi che non hanno traduzione nell'i18n
				for (PhonePrefixData phonePrefix : ctx.phonePrefixes) {
					String phonePrefixI18nKey = "countryData." + phonePrefix.getCode();
					String phonePrefixI18nDescription = i18n.get(phonePrefixI18nKey);
					if (!phonePrefixI18nKey.equals(phonePrefixI18nDescription)) {
						PhonePrefixData phonePrefixDataI18n = new PhonePrefixData();
						phonePrefixDataI18n.setCode(phonePrefix.getCode());
						phonePrefixDataI18n.setPrefix(phonePrefix.getPrefix());
						phonePrefixDataI18n.setDescription(phonePrefixI18nDescription);
						this.phonePrefixes.add(phonePrefixDataI18n);
					}
				}
				
				this.frequentFlyerTypes = ctx.frequentFlyerTypes;
	    	}
	    	//It used to retrieve the lower and upper bounds for each passenger, according to configuration.
	    	if (configuration != null) {
		    	minYearAdt = Integer.parseInt(configuration.getMinYearADT());
		    	maxYearAdt = Integer.parseInt(configuration.getMaxYearADT());
		    	minYearChd = Integer.parseInt(configuration.getMinYearCHD());
		    	if (ctx.cug == BookingSearchCUGEnum.FAMILY) {
		    		if(ctx.familySolutionFound) {
		    			maxYearChd = Integer.parseInt(configuration.getMaxYearFAMCHD());
		    		} else {
		    			maxYearChd = Integer.parseInt(configuration.getMaxYearCHD());
		    		}
		    	} else {
		    		maxYearChd = Integer.parseInt(configuration.getMaxYearCHD());
		    	}
		    	minYearInf = Integer.parseInt(configuration.getMinYearINF());
		    	maxYearInf = Integer.parseInt(configuration.getMaxYearINF());
	    	} else {
	    		logger.error("AlitaliaConfigurationHolder is null");
	    		throw new IllegalStateException("AlitaliaConfigurationHolder is null");
	    	}
	    	
	    	metasearch = ctx.metaSearch;
			if(request.getSession(true).getAttribute("kayakclickid") != null ){
				kayakclickid = request.getSession(true).getAttribute("kayakclickid").toString();
				logger.info("BookingConferma - kayakclickid = " + kayakclickid);
			}

	    	awardMessageDisplay = false;
	    	if (award) {
	    		
	    		realAwardPrice = bookingAwardSession.retrieveRealAwardPrice(ctx);
	    		if (!bookingAwardSession.isBrandAwardPriceCorrect(ctx, realAwardPrice)) {
	    			awardMessageDisplay = true;
	    		}
	    		
	    	}
	    	
	    	/*ottiene il testo dell'informativa infant senza il tag <p>*/
	    	this.infantLabelRTEContent = "";
			try{
				Resource pageResource = AlitaliaUtils.getPage(request.getResource()).getContentResource();	
				String rteContent = pageResource.getChild("dati-passeggero").getChild("rich-text-custom-infantWarning").getValueMap().get("text", String.class);
				if(rteContent != null){
					this.infantLabelRTEContent = rteContent.replaceAll("^<p>", "").replaceAll("</p>$", "");
				}
			}
			catch(Exception e){
				logger.error("Errore durante il recupero del testo dell'informativa Infant: ", e);
			}

			if(!frequentFlyerCodeLoggedUser.equals("")){
			    isNotEmptyMMcode = true;
            }
			//cripting in AES-128 del codice millemiglia per widget ADR

            if(isNotEmptyMMcode) {
                encryptionKey = configuration.getWidgetadrKey();
                Cipher cipher = Cipher.getInstance(cipherTransformation);
                byte[] key = encryptionKey.getBytes(characterEncoding);
                SecretKeySpec secretKey = new SecretKeySpec(key, aesEncryptionAlgorithem);
                IvParameterSpec ivparameterspec = new IvParameterSpec(key);
                cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivparameterspec);
                byte[] cipherText = cipher.doFinal(frequentFlyerCodeLoggedUser.getBytes("UTF8"));
                Base64.Encoder encoder = Base64.getEncoder();
                cryptedMMcode = encoder.encodeToString(cipherText);
            }
			
		} catch(Exception e) {
			logger.error("Unexpected error: ", e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}

	}
	
	public String getDefaultPrefix(){
		return defaultPrefix;
	}
	public String getDefaultPhoneType(){
		return defaultPhoneType;
	}
	public String getNameLoggedUser() {
		return nameLoggedUser;
	}

	public String getSurnameLoggedUser() {
		return surnameLoggedUser;
	}

	public String getFrequentFlyerCodeLoggedUser() {
		return frequentFlyerCodeLoggedUser;
	}

	public String getDayOfBirthLoggedUser() {
		return dayOfBirthLoggedUser;
	}

	public String getMonthOfBirthLoggedUser() {
		return monthOfBirthLoggedUser;
	}

	public String getYearOfBirthLoggedUser() {
		return yearOfBirthLoggedUser;
	}
	
	public String getGenderLoggedUser() {
		return genderLoggedUser;
	}

	public String getEmailLoggedUser() {
		return emailLoggedUser;
	}

	public String getPhonePrefixLoggedUser() {
		return phonePrefixLoggedUser;
	}

	public String getPhoneTypeLoggedUser() {
		return phoneTypeLoggedUser;
	}

	public String getPhoneLoggedUser() {
		return phoneLoggedUser;
	}

	public String getInputSelectDefaultValue() {
		return inputSelectDefaultValue;
	}
	
	public Boolean getIsArgentina() {
		return isArgentina;
	}
	
	public Boolean getIsSecureFlight() {
		return isSecureFlight;
	}
	
	public Boolean getIsSecureFlightESTA() {
		return isSecureFlightESTA;
	}
	
	public Boolean getIsApis() {
		return isApis;
	}
	
	public Boolean getIsTariffaLight() {
		return isTariffaLight;
	}
	
	public String[] getNumAdulti() {
		return numAdulti;
	}
	
	public String[] getNumBambini() {
		return numBambini;
	}
	
	public String[] getNumNeonati() {
		return numNeonati;
	}
	
	public List<AvailableFlightsSelectionRender> getFlightSelectionsDetails() {
		return flightSelectionsDetails;
	}
	
	public List<ContactType> getContactTypes() {
		return contactTypes;
	}
	
	public List<CountryData> getCountries() {
		return countries;
	}
	
	public List<CountryData> getCountriesCuitCuil() {
		return countriesCuitCuil;
	}
	
	public List<MealData> getMealTypes() {
		return mealTypes;
	}

	public List<FrequentFlyerTypeData> getFrequentFlyerTypes() {
		return frequentFlyerTypes;
	}

	public List<PhonePrefixData> getPhonePrefixes() {
		return phonePrefixes;
	}

	public boolean getMultileg() {
		return multileg;
	}
	
	public String getCug(){
		return cug;
	}
	
	public int getMinYearAdt() {
		return minYearAdt;
	}

	public int getMaxYearAdt() {
		return maxYearAdt;
	}

	public int getMinYearChd() {
		return minYearChd;
	}

	public int getMaxYearChd() {
		return maxYearChd;
	}

	public int getMinYearInf() {
		return minYearInf;
	}

	public int getMaxYearInf() {
		return maxYearInf;
	}

	public boolean isMetasearch() {
		return metasearch;
	}

	public boolean isAward() {
		return award;
	}
	
	public boolean getAwardMessageDisplay() {
		return awardMessageDisplay;
	}

	public Long getRealAwardPrice() {
		return realAwardPrice;
	}

	public boolean isBusCarrier() {
		return busCarrier;
	}

	public String getInfantLabelRTEContent() {
		return infantLabelRTEContent;
	}

    public String getCryptedMMcode() {
        return cryptedMMcode;
    }

    public boolean isNotEmptyMMcode() {
        return isNotEmptyMMcode;
    }
}
