package com.alitalia.aem.consumer.mmb.exception;

public class InvalidAncillaryIdentifierException extends Exception {

	private static final long serialVersionUID = -8388948758281010112L;
	
	public InvalidAncillaryIdentifierException(String message) {
		super(message);
	}
	
	public InvalidAncillaryIdentifierException(Throwable cause) {
		super(cause);
	}
	
}
