package com.alitalia.aem.consumer.bookingaward.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.bookingaward.BookingAwardBrandCodeEnum;
import com.alitalia.aem.consumer.bookingaward.BookingAwardSession;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingawardmodifysearch" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class BookingAwardModifySearchServlet extends GenericBookingAwardFormValidatorServlet {
	
	@Reference
    private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private BookingAwardSession bookingAwardSession;
	
	private static final String ATTR_STEP = "step";
	private static final String ATTR_FLIGHT = "flight";
	
	private static final String STEP_PREVIOUS = "P";
	private static final String STEP_NEXT = "N";
	private static final String DEPARTURE = "A";
	private static final String RETURN = "R";
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		// no input fields, always valid
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws Exception {
		
		
		String typeOfFlight = request.getParameter(ATTR_FLIGHT);
		String step = request.getParameter(ATTR_STEP);
		
		
		BookingSessionContext ctx = getBookingSessionContext(request);
		int index = -1;
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		int monthOfSearch=0;
		int yearOfSearch=0;
		if (DEPARTURE.equals(typeOfFlight)) {
			index = 0;
			monthOfSearch = ctx.calendarSearchDeparture[0];
			yearOfSearch = ctx.calendarSearchDeparture[1];
		}
		if (RETURN.equals(typeOfFlight)) {
			index = 1;
			monthOfSearch = ctx.calendarSearchReturn[0];
			yearOfSearch = ctx.calendarSearchReturn[1];
		}
			
		calendar.set(Calendar.MONTH, monthOfSearch);
		calendar.set(Calendar.YEAR, yearOfSearch);
			
		if (STEP_PREVIOUS.equals(step)) {
			calendar.add(Calendar.MONTH, -1);
		}
		if (STEP_NEXT.equals(step)) {
			calendar.add(Calendar.MONTH, 1);
		}
			
			
				
		if (index == 0) {
			bookingAwardSession.updateCalendarSearchDepartureData(ctx, calendar);
		}
		else {
			bookingAwardSession.updateCalendarSearchReturnData(ctx, calendar);
		}
			
			
			
	
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		json.key("result").value("OK");
		json.endObject();
	}
	
	
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}
