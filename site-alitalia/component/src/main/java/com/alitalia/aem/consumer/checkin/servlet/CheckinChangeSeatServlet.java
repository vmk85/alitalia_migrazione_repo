package com.alitalia.aem.consumer.checkin.servlet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.MmbAncillaryErrorData;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checkinchangeseat" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinChangeSeatServlet extends GenericCheckinFormValidatorServlet {
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private CheckinSession checkinSession;
	
	public final static String PASSENGER_FIELD = "passenger_";
	
	private static final String UPGRADE_ADDITION = "_UPGRADE";
	private static final String UPGRADE_BUSINESS = "Business";
	private static final String UPGRADE_PREMIUM_ECONOMY = "PremiumEconomy";
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		
		Map<Integer, String> seatsSelectionsByAncillaryId = new HashMap<Integer, String>();
		Map<Integer, String> upgradeByAncillaryId = new HashMap<Integer, String>();
		MmbCompartimentalClassEnum businessClass = checkinSession.getBusinessCompClass(ctx);
		MmbCompartimentalClassEnum premiumEcoClass = checkinSession.getPremiumEcoCompClass(ctx);

		String[] split;
		String[] selections = request.getParameterValues("seat-selection");
		List<MmbAncillaryErrorData> errors = new ArrayList<MmbAncillaryErrorData>();
		if (selections != null) {
			for (String selection : selections) {
				logger.debug("Processing seat-selection {}", selection);
				split = selection.split("#", 2);
				String[] ancillaryIds = split[0].split("-");
				String ancillaryValue = split[1];
				for (String ancillaryIdString : ancillaryIds) {
					int ancillaryId = Integer.parseInt(ancillaryIdString);
					logger.debug("Evaluating ancillary id {} with value {}", ancillaryId, ancillaryValue);
					if(ancillaryValue.endsWith(UPGRADE_ADDITION)){
						String updateSeat = ancillaryValue.replace(UPGRADE_ADDITION, "")
								.replace(UPGRADE_BUSINESS, businessClass.value());
						if(premiumEcoClass != null)
							updateSeat = 	updateSeat.replace(UPGRADE_PREMIUM_ECONOMY, premiumEcoClass.value());
						upgradeByAncillaryId.put(ancillaryId, updateSeat);
					}
					else{
						seatsSelectionsByAncillaryId.put(ancillaryId, ancillaryValue);
					}
				}
			}
			errors = checkinSession.addOrUpdateSeatsInCart(ctx, seatsSelectionsByAncillaryId);
			checkinSession.addSeatUpgradeInCart(ctx, upgradeByAncillaryId);
		}
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value(errors.isEmpty() ? true : false);
		json.endObject();
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}
