package com.alitalia.aem.consumer.utils;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PDFCreator {
	
	private static Logger logger = LoggerFactory.getLogger(PDFCreator.class);

	private static PDImageXObject pdPixelMap = null;
	private static PDImageXObject[] pdPixelMapBanner;
	
	private static int logoImageSize = 50;
	private static int logo1ImageSize = 110;
	private static int logo1ImageSize_c = 100;
	private static int logo2ImageSize = 25;
	private static int logo2ImageSize_c = 20;
	private static int logo3ImageSize = 70;
	private static int logo3ImageSize_c = 60;
	private static int logosImageHeight = 30;
	private static int logosImageHeight_c = 25;
	private static int checkinLogoWidth = 20;
	private static int checkinLogoHeight = 20;
	private static float headerHeight = 90;
	private static float footerHeight = 120;
	private static float genericPadding = 20;
	private static float smallPadding = 5;
	private static float headerFontSize = 18;
	private static float headerSpace = 5;
	private static float fontSize = 13;
	private static float fontSizeSmall = 9;
	private static float fontSizeMicro = 7;
	private static float fontSizeHuge = 25;
	private static float fontTicketSize = 20;
	private static float tableFontSize = 11;
	private static float headingFontSize = 14;
	private static float messagesInterline = 25;
	private static float messageInterline = 2;
	private static float messagesLineLimit = 100;
	private static float genericInterline = 15;
	private static float middleInterline = 10;
	private static String fontAlternateColor = "#006643";
	private static String fontColor = "#000000";
	private static String fontRedColor = "#cc0000";
	private static String bgColor = "#f8f8f8";
	private static String rowAlternateColor = "#F5F8F4";
	private static String rowColor = "#D9D9D9";
	private static String tableColor = "#B2B2B2";
	private static float borderWidth = 1.2f;
	private static float borderPadding = 17;
	private static String borderColor = "#E5E5E5";
	private static int linesPerPage = 21;
	private static float boardingPassWidth = 210; //420
	private static float boardingPassHeigth = 60; //180
	private static float bannerWidth = 153; // 330
	private static float bannerHeight = 170; // 365
	
	private static float _line_x = 0;
	private static float _line_y = 0;
	private static float elfSpace = 0;
	
	/**
	 * Generate Messages PDF
	 * @param output
	 * @param fontFamily
	 * @param fontFamilyBold
	 * @param headerLogo
	 * @param headerTitle
	 * @param userName
	 * @param userMigliaLabel
	 * @param userMiglia
	 * @param userStatusLabel
	 * @param userStatus
	 * @param contents
	 * @throws Exception
	 */
	public final static void generateMessagesPDF(OutputStream output, boolean rtl, 
			InputStream fontFamily, InputStream fontFamilyBold,
			InputStream headerLogo, String headerTitle, String userName,
			String userMigliaLabel, String userMiglia, String userStatusLabel,
			String userStatus, String[][] contents) throws Exception {
		logger.debug("[PDFCreator] generateMessagesPDF");

		// create a new PDDocument file
		PDDocument pdDocument = new PDDocument();

		// attach font to document
		PDFont font = PDType0Font.load(pdDocument, fontFamily);
		PDFont headerFont = PDType0Font.load(pdDocument, fontFamilyBold);

		// split messages into lines
		String[][][] newContents = copySplitMessages(contents, font);
		ArrayList<ArrayList<String[][][]>> pages = calculatePDFpages(newContents);
		
		// write messages into all PDF pages (one by one)
		for (ArrayList<String[][][]> pageMessages : pages) {
			writeMessagesIntoPDFPage(pdDocument, rtl, font, headerFont, headerLogo,
					headerTitle, userName, userMigliaLabel, userMiglia,
					userStatusLabel, userStatus, pageMessages);
		}

		// save document to output and close it!
		pdDocument.save(output);
		pdDocument.close();

	}
	
	/*
	 * copy split messages
	 */
	private static String[][][] copySplitMessages(String[][] contents,
			PDFont font) throws Exception {
		
		// calculate messages lines
		float pageWidth = PDRectangle.A4.getWidth();
		float maxWidthMessage = pageWidth - (pageWidth / 3)
				- (messagesLineLimit);

		// copy data into new array of array of array of strings
		// with messages already split
		String[][][] newContents = new String[contents.length][][];
		for (int i = 0; i < contents.length; i++) {
			
			newContents[i] = new String[contents[i].length][];
			newContents[i][0] = new String[]{contents[i][0]};
			newContents[i][1] = new String[]{contents[i][1]};
			newContents[i][2] = new String[]{contents[i][2]};
			newContents[i][3] = new String[]{contents[i][3]};
			String[] text = splitString(contents[i][4], maxWidthMessage,
					font, headingFontSize);
			newContents[i][4] = text;
		}
		
		// return new contents with split messages
		return newContents;
	}
	
	/*
	 * generate PDF pages
	 */
	private static ArrayList<ArrayList<String[][][]>> calculatePDFpages(
			String[][][] contents) throws Exception {
		
		// calculate max available area to fit messages
		float pageHeight = PDRectangle.A4.getHeight(); 
		float maxAvailableArea = 
				(pageHeight - headerHeight - footerHeight);

		// temporary variables
		ArrayList<ArrayList<String[][][]>> pages =
				new ArrayList<ArrayList<String[][][]>>();
		ArrayList<String[][][]> messagesInPage = new ArrayList<String[][][]>();
		ArrayList<String> linesOfMessage = new ArrayList<String>();
		
		// current sum of heights of lines
		float heightOfLines = 0;
		
		// read all messages to print into PDF
		for (int i = 0; i < contents.length; i ++) {
			
			// consider the heading of the Ith message (2 lines of texts)
			heightOfLines = heightOfLines + fontSize * 2 - messageInterline * 2;
			
			// read all lines of Ith message
			for (int k = 0; k < contents[i][4].length; k++) {
				
				// check if the Kth line fits into available area
				if (heightOfLines + genericInterline > maxAvailableArea) {
					
					// add the Kth message into the list of
					// messages for this page
					String[][][] message = new String[1][5][];
					message[0][0] = contents[i][0];
					message[0][1] = contents[i][1];
					message[0][2] = contents[i][2];
					message[0][3] = contents[i][3];
					message[0][4] = new String[linesOfMessage.size()];
					for (int j = 0; j < linesOfMessage.size(); j++) {
						message[0][4][j] = linesOfMessage.get(j);
					}
					messagesInPage.add(message);
					
					// if it doesn't fit --> flush messages...
					pages.add(messagesInPage);
					
					// ...and restart counters
					messagesInPage = new ArrayList<String[][][]>();
					linesOfMessage = new ArrayList<String>();
					heightOfLines = messagesInterline;
					
				}
					
				// add the Kth line to lines of messages to print into PDF
				heightOfLines = heightOfLines + genericInterline;
				linesOfMessage.add(contents[i][4][k]);
			}
			
			// add the Kth message into the list of messages for this page
			String[][][] message = new String[1][5][];
			message[0][0] = contents[i][0];
			message[0][1] = contents[i][1];
			message[0][2] = contents[i][2];
			message[0][3] = contents[i][3];
			message[0][4] = new String[linesOfMessage.size()];
			for (int j = 0; j < linesOfMessage.size(); j++) {
				message[0][4][j] = linesOfMessage.get(j);
			}
			messagesInPage.add(message);
			
			// clean lines
			linesOfMessage = new ArrayList<String>();
			
			// consider the interline between two messages
			heightOfLines = heightOfLines + messagesInterline;
		}
		
		// some data not in page? flush messages into page (adding a new one)
		if (0 != heightOfLines) {
			pages.add(messagesInPage);
		}
		
		return pages;
	}
	
	/*
	 * generate messages PDF page
	 */
	private static void writeMessagesIntoPDFPage(PDDocument pdDocument,
			boolean rtl, PDFont font, PDFont headerFont, InputStream headerLogo,
			String headerTitle, String userName, String userMigliaLabel,
			String userMiglia, String userStatusLabel, String userStatus, 
			ArrayList<String[][][]> contents) throws Exception {
		
		// create a new A4 page and add it to document
		PDPage pdPage = new PDPage();
		pdPage.setMediaBox(PDRectangle.A4);
		pdDocument.addPage(pdPage);
		
		// create a new page content stream
		PDPageContentStream pdPageContentStream = 
				new PDPageContentStream(pdDocument, pdPage, false, false);
		
		// add header
		createPDFHeader(pdDocument, pdPage, pdPageContentStream, rtl, font,
				headerFont, headerLogo, headerTitle, userName,
				userMigliaLabel, userMiglia, userStatusLabel, userStatus);
		
		// add messages
		createPDFMessagesContent(pdDocument, pdPage, pdPageContentStream,
				rtl, font, contents);
	
		// close content stream
		pdPageContentStream.close();
		
	}
	
	/**
	 * Generate Ticket PDF
	 * @param output
	 * @param fontFamily
	 * @param fontFamilyBold
	 * @param headerLogo1
	 * @param headerLogo2
	 * @param headerLogo3
	 * @param contents
	 * @throws Exception
	 */
	public final static void generateTicketPDF(OutputStream output, boolean rtl, 
			InputStream fontFamily, InputStream fontFamilyBold,
			InputStream headerLogo1, InputStream headerLogo2,
			InputStream headerLogo3, String[][][] contents,
			String[][][] pricerules, String[] notes) throws Exception {
		logger.debug("[PDFCreator] generateTicketPDF");

		// create a new PDDocument file
		PDDocument pdDocument = new PDDocument();
		
		// attach font to document
		PDFont font = PDType0Font.load(pdDocument, fontFamily);
		PDFont fontB = PDType0Font.load(pdDocument, fontFamilyBold);
		
		writeTicketPDFPage(pdDocument, rtl, font, fontB, headerLogo1, headerLogo2,
				headerLogo3, contents, pricerules,notes);
		
		// save document to output and close it!
		pdDocument.save(output);
		pdDocument.close();
	}
	
	/*
	 * writeTicketPDFPage
	 */
	private final static void writeTicketPDFPage(PDDocument pdDocument, boolean rtl, 
			PDFont font, PDFont fontB, InputStream headerLogo1,
			InputStream headerLogo2, InputStream headerLogo3,
			String[][][] contents, String[][][] pricerules, String[] notes) throws Exception {
		logger.debug("[PDFCreator] writeTicketPDFPage");
		
		// create a new A4 page and add it to document
		PDPage pdPage = new PDPage();
		pdPage.setMediaBox(PDRectangle.A4);
		pdDocument.addPage(pdPage);

		// create a new page content stream
		PDPageContentStream pdPageContentStream = 
				new PDPageContentStream(pdDocument, pdPage, false, false);
		
		createPDFTicketHeader(pdDocument, pdPage, pdPageContentStream, rtl,
				headerLogo1, headerLogo2, headerLogo3);

		pdPageContentStream.close();
		
		pdPageContentStream = createPDFTicketContent(pdDocument, pdPage,
				pdPageContentStream, rtl, font, fontB, contents, pricerules,notes);
		
		// close content stream
		pdPageContentStream.close();

	}
	
	/*
	 * createPDFTicketHeader
	 */
	private static void createPDFTicketHeader(PDDocument pdDocument,
			PDPage pdPage, PDPageContentStream pdPageContentStream, boolean rtl,
			InputStream headerLogo1, InputStream headerLogo2,
			InputStream headerLogo3) throws IOException {
		logger.debug("[PDFCreator] createPDFTicketHeader");
		
		// calculate positions
		float pageWidth = pdPage.getMediaBox().getWidth();
		float pageHeight = pdPage.getMediaBox().getHeight();
		
		// load logo1 image
		pdPixelMap = null;
		float logoX = genericPadding;
		float logoY = ((pageHeight - (logosImageHeight + genericPadding)));
		BufferedImage bufferedImage = ImageIO.read(headerLogo1);
		if (null != bufferedImage && null == pdPixelMap) {
			pdPixelMap = LosslessFactory.createFromImage(pdDocument, bufferedImage);
			bufferedImage.flush();
		}
		pdPageContentStream.drawImage(pdPixelMap, 
				rtl ? pageWidth - logoX - logo1ImageSize : logoX, logoY,
				logo1ImageSize, logosImageHeight);

		// load logo2 image
		pdPixelMap = null;
		logoX = logoX + logo1ImageSize + genericPadding;
		bufferedImage = ImageIO.read(headerLogo2);
		if (null != bufferedImage && null == pdPixelMap) {
			pdPixelMap = LosslessFactory.createFromImage(pdDocument, bufferedImage);
			bufferedImage.flush();
		}
		pdPageContentStream.drawImage(pdPixelMap, 
				rtl ? pageWidth - logoX - logo2ImageSize : logoX, logoY,
				logo2ImageSize, logosImageHeight);
		
		// load logo3 image
		pdPixelMap = null;
		logoX = pageWidth - genericPadding - logo3ImageSize;
		bufferedImage = ImageIO.read(headerLogo3);
		if (null != bufferedImage && null == pdPixelMap) {
			pdPixelMap = LosslessFactory.createFromImage(pdDocument, bufferedImage);
			bufferedImage.flush();
		}
		pdPageContentStream.drawImage(pdPixelMap, 
				rtl ? pageWidth - logoX - logo3ImageSize : logoX, logoY,
				logo3ImageSize, logosImageHeight);

	}
	
	/*
	 * create PDF Ticket Content
	 */
	private static PDPageContentStream createPDFTicketContent(
			PDDocument pdDocument, PDPage pdPage,
			PDPageContentStream pdPageContentStream, boolean rtl,
			PDFont font, PDFont fontB, String[][][] contents,
			String[][][] pricerules, String[] notes) throws Exception {
		logger.debug("[PDFCreator] createPDFTicketContent");
		
		// calculate page width & height, set start x & y;
		float pageWidth = pdPage.getMediaBox().getWidth();
		float pageHeight = pdPage.getMediaBox().getHeight();
		_line_x = genericPadding;
		_line_y = pageHeight - logosImageHeight - (messagesInterline * 2);

		// print flight and passengers infos
		if (null != contents[0] && null != contents[1]) {
			
			// flight summary
			createPDFTicketContentHeader(pdPageContentStream, rtl, font, fontB,
					pageWidth, pageHeight, contents[0]);
			
			// flight passengers data
			for (int i = 2; i < contents.length; i = i + 2) {
				
				if (null != contents[i] && null != contents[i + 1]) {
					pdPageContentStream = createPDFTicketContentFlight(
							pdDocument, pdPageContentStream, rtl, font, fontB,
							pageWidth, pageHeight, contents[i], contents[1]);
					pdPageContentStream = createPDFTicketContentFlightPassengers(
							pdDocument, pdPageContentStream, rtl, font, fontB,
							pageWidth, pageHeight, contents[i + 1]);
				}
				
			}
			
		}
		
		// prices
		if (null != pricerules[0]) {
			pdPageContentStream = createPDFTicketContentPrice(pdDocument,
					pdPageContentStream, rtl, font, fontB, pageWidth, pageHeight,
					pricerules[0]);
		}
		
		// flight rules
		if (null != pricerules[1]) {
			pdPageContentStream = createPDFTicketContentFlightRules(pdDocument,
					pdPageContentStream, rtl, font, fontB, pageWidth, pageHeight,
					pricerules);
		}

		//add notes
		pdPageContentStream = createPDFTicketContentFlightRulesNotes(pdDocument,
				pdPageContentStream, rtl, font, fontB, pageWidth, pageHeight,
				pricerules[2],notes);
		
		// return modified stream
		return pdPageContentStream;
		
	}



	/*
	 * create PDF Ticket Content // generic header data
	 */
	private static void createPDFTicketContentHeader(
			PDPageContentStream pdPageContentStream, boolean rtl,
			PDFont font, PDFont fontB,
			float pageWidth, float pageHeight, String[][] contents)
					throws Exception {
		logger.debug("[PDFCreator] createPDFTicketContentHeader");
		
		// write line 1 (with PNR)
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
				contents[0][0] }, fontSize, font, fontColor);
		float line_x1 = 
				_line_x + font.getStringWidth(contents[0][0]) / 1000 * fontSize
					+ smallPadding;
		writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
				contents[0][1] }, fontTicketSize, fontB, fontAlternateColor);
		
		// write line 2 (with Price)
		_line_y = _line_y - (messagesInterline);
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, contents[1],
				fontSize, font, fontColor);
		
		int nextIndex = 4;
		if (contents.length == 5) {
			//write lottomatica info only if exist
			// write line 3+ (with summary description)
			_line_y = _line_y - (messagesInterline);
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, contents[2],
					fontSizeSmall, font, fontColor);

			// write line 4 (with the expiration date of payment)
			_line_y = _line_y - (messagesInterline * 2);
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					contents[3][0] }, fontSizeSmall, font, fontColor);
			line_x1 =
					_line_x + font.getStringWidth(contents[3][0]) / 1000 * fontSizeSmall
						+ smallPadding;
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
					contents[3][1] }, fontSizeSmall, fontB, fontColor);
		} else {
			nextIndex = 2;
		}

		// write line 5 (route details, i.e. number of passengers)
		_line_y = _line_y - (messagesInterline * 2);
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, contents[nextIndex],
				fontSize, font, fontColor);

	}
	
	/*
	 * calculateNewHeight
	 */
	private static float calculateNewHeight(float h, float maxH,
			float newH) {
		return (h < maxH) ? newH : h;
	}
	
	/*
	 * createPDFNewPage
	 */
	private static PDPageContentStream createPDFNewPage(PDDocument pdDocument,
			PDPageContentStream pdPageContentStream, float h, float newH)
					 throws Exception {
		logger.debug("[PDFCreator] createPDFNewPage");
		
		if (h == newH) { 
			
			// close content stream
			pdPageContentStream.close();
			
			// create a new A4 page and add it to document
			PDPage pdPage = new PDPage();
			pdPage.setMediaBox(PDRectangle.A4);
			pdDocument.addPage(pdPage);
			
			// create a new page content stream
			pdPageContentStream = 
					new PDPageContentStream(pdDocument, pdPage, false, false);

		}
		
		return pdPageContentStream;
		
	}
	
	/*
	 * create PDF Ticket Content // generic flight data
	 */
	private static PDPageContentStream createPDFTicketContentFlight(
			PDDocument pdDocument, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontB, float pageWidth, float pageHeight,
			String[][] flight_data, String[][] table_data) throws Exception {
		logger.debug("[PDFCreator] createPDFTicketContentFlight");
		
		
		_line_y = _line_y - (messagesInterline);
		
		for (int k = 0; k < flight_data.length; k = k + 5) {
			
			// create a new page if needed
			float newH = pageHeight - messagesInterline * 2;
			_line_y = calculateNewHeight(_line_y,
					genericPadding + messagesInterline * 2, newH);
			pdPageContentStream = createPDFNewPage(pdDocument,
					pdPageContentStream, _line_y, newH);
			
			// flight info
			if (k > 0) {
				
				// waiting time
				String waiting_time =
						"---------- " + flight_data[k][0] + " ----------";
				float line_x1 = (pageWidth - font.getStringWidth(waiting_time)
						/ 1000 * fontSizeSmall) / 2 ;
				writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
						waiting_time }, fontSizeSmall, fontB, fontColor);
				
				_line_y = _line_y - genericInterline;
				
			} else {
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, flight_data[k],
						fontSize, fontB, fontColor);
			}

			_line_y = _line_y - messagesInterline;
			
			// flight date and airport information
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
					new String[] { flight_data[k + 1][0] },
					fontSizeHuge, font, fontColor);
			
			float space = font.getStringWidth(flight_data[k + 1][0]) / 1000 * fontSizeHuge;
			float line_x1 = _line_x + space + smallPadding;
			float line_y1 = _line_y + (messageInterline * 6);
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, line_y1,
					new String[] { flight_data[k + 1][1], flight_data[k + 1][2] },
					fontSizeSmall, font, fontColor);
			
			line_x1 = (pageWidth / 3);
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y,
					new String[] { flight_data[k + 2][0] },
					fontSizeHuge, font, fontColor);

			space = font.getStringWidth(flight_data[k + 2][0]) / 1000 * fontSizeHuge;
			String[] lines = new String[flight_data[k + 2].length - 1];
			for (int i = 1; i < flight_data[k + 2].length; i++) {
				lines[i - 1] = flight_data[k + 2][i];
			}
			line_x1 = line_x1 + space + smallPadding;
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, line_y1, new String[] {
					flight_data[k + 2][1], flight_data[k + 2][2] },
					fontSizeSmall, font, fontColor);

			line_x1 = (2 * (pageWidth / 3));
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y,
					new String[] { flight_data[k + 3][0] },
					fontSizeHuge, font, fontColor);

			space = font.getStringWidth(flight_data[k + 3][0]) / 1000 * fontSizeHuge;
			lines = new String[flight_data[k + 3].length - 1];
			for (int i = 1; i < flight_data[k + 3].length; i++) {
				lines[i - 1] = flight_data[k + 3][i];
			}
			line_x1 = line_x1 + space + smallPadding;
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, line_y1, lines,
						fontSizeSmall, font, fontColor);
			
			_line_y = _line_y - (messagesInterline);
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					flight_data[k + 4][0] }, fontSizeSmall, font, fontColor);
			
			_line_y = _line_y - (messagesInterline / 2);
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					flight_data[k + 4][1] }, fontSizeSmall, fontB, fontColor);
		
			_line_y = _line_y - messagesInterline;
			
		}
		
		// passengers table head
		float bgWidth = pageWidth - (genericPadding * 2) + (messageInterline * 2);
		float bgHeight = (tableFontSize + messageInterline * 3);
		float bgX = genericPadding - messageInterline;
		float bgY = _line_y - messageInterline;
		writeRectangle(pdPageContentStream, bgX, bgY, bgWidth,
				bgHeight, tableColor);
		
		float line_y1 = _line_y + messageInterline;
		float line_x1 = _line_x + messageInterline;
		writeLine(pdPageContentStream, rtl, pageWidth, line_x1, line_y1, new String[] {
				table_data[0][0] }, tableFontSize, font, fontColor);

		line_x1 = (2* pageWidth / 4);
		writeLine(pdPageContentStream, rtl, pageWidth, line_x1, line_y1, new String[] {
				table_data[0][1] }, tableFontSize, font, fontColor);
		
		line_x1 = (3 * (pageWidth / 4));
		writeLine(pdPageContentStream, rtl, pageWidth, line_x1, line_y1, new String[] {
				table_data[0][2] }, tableFontSize, font, fontColor);
		
		// change line_y
		_line_y = bgY - bgHeight;
		
		// return modified stream
		return pdPageContentStream;
		
	}
	
	/*
	 * create PDF Ticket Content // flight passengers table data
	 */
	private static PDPageContentStream createPDFTicketContentFlightPassengers(
			PDDocument pdDocument, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontB, float pageWidth, float pageHeight,
			String[][] passengers) throws Exception {
		logger.debug("[PDFCreator] createPDFTicketContentFlightPassengers");
		
		// print all passengers
		int p = 0;
		for (String[] passenger : passengers) {
			
			// create a new page if needed
			float newH = pageHeight - messagesInterline * 2;
			_line_y = calculateNewHeight(_line_y,
					genericPadding + messagesInterline * 2, newH);
			pdPageContentStream = createPDFNewPage(pdDocument,
					pdPageContentStream, _line_y, newH);
			
			String[] passenger_data = new String[passenger.length - 2];
			passenger_data[0] = passenger[0];
			for (int i = 3; i < passenger.length; i++) {
				passenger_data[i - 2] = passenger[i];
			}
			
			float interline = messageInterline * (2 * passenger_data.length + 2);
			float bgWidth = pageWidth - (genericPadding * 2) + (messageInterline * 2);
			float bgHeight = (tableFontSize * passenger_data.length + interline);
			float bgX = genericPadding - messageInterline;
			float bgY = _line_y - bgHeight + tableFontSize + messageInterline * 2;
			
			String color = ((++p) % 2 == 0) ? rowColor : rowAlternateColor;
			writeRectangle(pdPageContentStream, bgX, bgY, bgWidth,
					bgHeight, color);
			
			float line_y1 = _line_y + messageInterline;
			float line_x1 = _line_x + messageInterline;
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, line_y1, passenger_data,
					tableFontSize, font, fontColor);

			line_x1 = (2 * pageWidth / 4);
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, line_y1, new String[] {
					passenger[1] }, tableFontSize, font, fontColor);
			
			line_x1 = (3 * (pageWidth / 4));
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, line_y1, new String[] {
					passenger[2] }, tableFontSize, font, fontColor);
		
			_line_y = bgY - genericInterline - messageInterline;
			
		}
		
		// return modified stream
		return pdPageContentStream;

	}
	
	/*
	 * createPDFTicketContentPrice
	 */
	private static PDPageContentStream createPDFTicketContentPrice(
			PDDocument pdDocument, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontB, float pageWidth, float pageHeight,
			String[][] contents) throws Exception {
		logger.debug("[PDFCreator] createPDFTicketContentPrice");
		
		// create a new page if needed
		float newH = pageHeight - messagesInterline * 2;
		_line_y = calculateNewHeight(_line_y,
				genericPadding + messagesInterline * 2, newH);
		pdPageContentStream = createPDFNewPage(pdDocument, pdPageContentStream,
				_line_y, newH);
		
		_line_y = _line_y - (genericInterline);
		
		for (String[] content : contents) {
			
			// create a new page if needed
			_line_y = calculateNewHeight(_line_y,
					genericPadding + messagesInterline * 2, newH);
			pdPageContentStream = createPDFNewPage(pdDocument,
					pdPageContentStream, _line_y, newH);
			
			_line_y = _line_y - (genericInterline);
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					content[0] }, fontSizeSmall, font, fontColor);
			
			float line_x1 = pageWidth / 2;
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
					content[1] }, fontSizeSmall, fontB, fontColor);
			
		}
		
		// return modified stream
		return pdPageContentStream;
		
	}
	
	/*
	 * createPDFTicketContentFlightRules
	 */
	private static PDPageContentStream createPDFTicketContentFlightRules(
			PDDocument pdDocument, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontB, float pageWidth, float pageHeight,
			String[][][] contents) throws Exception {
		logger.debug("[PDFCreator] createPDFTicketContentFlightRules");
		
		// create a new page if needed
		float newH = pageHeight - messagesInterline * 2;
		_line_y = calculateNewHeight(_line_y,
				genericPadding + messagesInterline * 2, newH);
		pdPageContentStream = createPDFNewPage(pdDocument, pdPageContentStream,
				_line_y, newH);
		
		// section title
		_line_y = _line_y - (messagesInterline * 2);
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, contents[1][0],
				fontSize, font, fontColor);
		
		// flight rules for each flight
		for (int i = 1; i < contents[1].length; i++) {
			
			_line_y = _line_y - genericInterline;
			
			// create a new page if needed
			_line_y = calculateNewHeight(_line_y,
					genericPadding + messagesInterline * 2, newH);
			pdPageContentStream = createPDFNewPage(pdDocument,
					pdPageContentStream, _line_y, newH);
			
			_line_y = _line_y - (genericInterline);
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					contents[1][i][0] }, fontSizeSmall, fontB, fontColor);

			float space = font.getStringWidth(contents[1][i][0]) / 1000 * fontSizeSmall;
			float line_x1 = _line_x + space + genericPadding;
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
					contents[1][i][1] }, fontSizeSmall, font, fontColor);

			space = font.getStringWidth(contents[1][i][1]) / 1000 * fontSizeSmall;
			line_x1 = line_x1 + space + smallPadding;
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
					contents[1][i][2] }, fontSizeSmall, fontB, fontColor);
			
			space = font.getStringWidth(contents[1][i][2]) / 1000 * fontSizeSmall;
			line_x1 = line_x1 + space + genericPadding;
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
					contents[1][i][3] }, fontSizeSmall, font, fontColor);
			
			int index = 1 + i;
			if (null != contents[index]) {
				
				_line_y = _line_y - (messageInterline);

				for (String[] content : contents[index]) {
					
					// create a new page if needed
					_line_y = calculateNewHeight(_line_y,
							genericPadding + messagesInterline * 2, newH);
					pdPageContentStream = createPDFNewPage(pdDocument,
							pdPageContentStream, _line_y, newH);
					
					_line_y = _line_y - (genericInterline);
					writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
							content[0] }, fontSizeSmall, font, fontColor);
					
					line_x1 = pageWidth / 2;
					writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
							content[1] }, fontSizeSmall, font, fontColor);
					
				}
				
			}

		}
		
		// return modified stream
		return pdPageContentStream;
		
	}

	private static PDPageContentStream createPDFTicketContentFlightRulesNotes(
			PDDocument pdDocument, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontB, float pageWidth, float pageHeight,
			String[][] pricerules, String[] notes) throws Exception {

		// create a new page if needed
		float newH = pageHeight - messagesInterline * 2;
		_line_y = calculateNewHeight(_line_y,
				genericPadding + messagesInterline * 2, newH);
		pdPageContentStream = createPDFNewPage(pdDocument, pdPageContentStream,
				_line_y, newH);

		boolean genericLabel = false;
		boolean firstLabel = false;
		boolean secondLabel = false;

		for(int i = 0;i<pricerules.length;i++){
			if (pricerules[i][1].contains("*")) {
				if (pricerules[i][1].equals(notes[0]) || pricerules[i][1].equals(notes[1])) {
					genericLabel = true;
				} else {
					firstLabel = true;
				}
			}
			if (pricerules[i][1].equals(notes[2])) {
				secondLabel = true;
			}
		}


		if(genericLabel){
			_line_y = _line_y - (genericInterline);
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					notes[5] }, fontSizeSmall, font, fontColor);
		}
		if(firstLabel){
			_line_y = _line_y - (genericInterline);
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					notes[3] }, fontSizeSmall, font, fontColor);
		}
		if(secondLabel){
			_line_y = _line_y - (genericInterline);
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					notes[4] }, fontSizeSmall, font, fontColor);
		}



		return pdPageContentStream;
	}
	
	/**
	 * Generate Services PDF
	 * @param output
	 * @param fontFamily
	 * @param fontFamilyBold
	 * @param headerLogo1
	 * @param headerLogo2
	 * @param headerLogo3
	 * @param contents
	 * @param table_data
	 * @throws Exception
	 */
	public final static void generateServicesPDF(OutputStream output, boolean rtl,
			InputStream fontFamily, InputStream fontFamilyBold,
			InputStream headerLogo1, InputStream headerLogo2,
			InputStream headerLogo3, String[][][] contents, 
			String[][][] table_data) throws Exception {
		logger.debug("[PDFCreator] generateServicesPDF");
		
		// create a new PDDocument file
		PDDocument pdDocument = new PDDocument();
		
		// attach font to document
		PDFont font = PDType0Font.load(pdDocument, fontFamily);
		PDFont fontB = PDType0Font.load(pdDocument, fontFamilyBold);
		
		writeServicesPDFPage(pdDocument, rtl, font, fontB, headerLogo1, headerLogo2,
				headerLogo3, contents, table_data);
		
		// save document to output and close it!
		pdDocument.save(output);
		pdDocument.close();
	}
	
	/*
	 * write Services PDF Page
	 */
	private final static void writeServicesPDFPage(PDDocument pdDocument, boolean rtl, 
			PDFont font, PDFont fontB, InputStream headerLogo1,
			InputStream headerLogo2, InputStream headerLogo3,
			String[][][] contents, String[][][] table_data) throws Exception {
		logger.debug("[PDFCreator] writeServicesPDFPage");
		
		// create a new A4 page and add it to document
		PDPage pdPage = new PDPage();
		pdPage.setMediaBox(PDRectangle.A4);
		pdDocument.addPage(pdPage);

		// create a new page content stream
		PDPageContentStream pdPageContentStream = 
				new PDPageContentStream(pdDocument, pdPage, false, false);
		
		createPDFTicketHeader(pdDocument, pdPage, pdPageContentStream, rtl,
				headerLogo1, headerLogo2, headerLogo3);
		
		pdPageContentStream = createPDFServicesContent(pdDocument, pdPage,
				pdPageContentStream, rtl, font, fontB, contents, table_data);
		
		// close content stream
		pdPageContentStream.close();
	}
	
	/*
	 * create PDF Services Content
	 */
	private final static PDPageContentStream createPDFServicesContent(
			PDDocument pdDocument, PDPage pdPage,
			PDPageContentStream pdPageContentStream, boolean rtl,
			PDFont font, PDFont fontB, String[][][] contents, String[][][] table_data)
					 throws Exception {
		logger.debug("[PDFCreator] createPDFServicesContent");
	
		// calculate page width & height, set start x & y;
		float pageWidth = pdPage.getMediaBox().getWidth();
		float pageHeight = pdPage.getMediaBox().getHeight();
		_line_x = genericPadding;
		_line_y = pageHeight - logosImageHeight - (messagesInterline * 2);

		// print recap info
		if (null != contents[0]) {
			pdPageContentStream = createPDFServicesContentHeader(pdDocument,
					pdPageContentStream, rtl, font, fontB, pageWidth, pageHeight,
					contents);
		}
		
		// print passengers data
		if (null != contents[1] && null != table_data && table_data.length > 0) {
			pdPageContentStream = createPDFServicesContentPassengers(pdDocument,
					pdPageContentStream, rtl, font, fontB, pageWidth, pageHeight,
					contents, table_data);
		}
		
		// print end info
		if (null != contents[0]) {
			pdPageContentStream = createPDFServicesContentFooter(pdDocument,
					pdPageContentStream, rtl, font, fontB, pageWidth, pageHeight,
					contents);
		}
				
		// return modified stream
		return pdPageContentStream;
				
	}
	
	/*
	 * create PDF Services Content Header
	 */
	private static PDPageContentStream createPDFServicesContentHeader(
			PDDocument pdDocument, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontB, float pageWidth, float pageHeight,
			String[][][] contents) throws Exception {
		logger.debug("[PDFCreator] createPDFServicesContentPassengers");
		
		// create a new page if needed
		float newH = pageHeight - messagesInterline * 2;
		_line_y = calculateNewHeight(_line_y,
				genericPadding + messagesInterline * 2, newH);
		pdPageContentStream = createPDFNewPage(pdDocument,
				pdPageContentStream, _line_y, newH);
		
		// write line 1 (with PNR)
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] { 
				contents[0][0][0] }, fontSize, font, fontColor);
		float line_x1 = 
				_line_x + font.getStringWidth(contents[0][0][0]) / 1000 * fontSize
					+ smallPadding;
		writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
				contents[0][0][1] }, fontTicketSize, fontB, fontAlternateColor);
		
		// write line 2 (message)
		_line_y = _line_y - messagesInterline;
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, contents[0][1],
				fontSizeSmall, font, fontColor);
		
		// write line 3 (details title with price)
		_line_y = _line_y - messagesInterline * 2;
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, contents[0][2],
				headerFontSize, font, fontColor);
		line_x1 = pageWidth - genericPadding * 2 + smallPadding -
				font.getStringWidth(contents[0][3][0]) / 1000 * headerFontSize;
		writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, contents[0][3],
				headerFontSize, fontB, fontColor);
		
		// return modified stream
		return pdPageContentStream;
		
	}
	
	/*
	 * create PDF Services Content Passengers
	 */
	private static PDPageContentStream createPDFServicesContentPassengers(
			PDDocument pdDocument, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontB, float pageWidth, float pageHeight,
			String[][][] contents, String[][][] table_data) throws Exception {
		logger.debug("[PDFCreator] createPDFServicesContentPassengers");

		// all passengers
		int l = 0;
		for (int i = 1; i < contents.length; i++) {
		
			_line_y = _line_y - genericInterline * 2;
			
			// create a new page if needed
			float newH = pageHeight - messagesInterline * 2;
			_line_y = calculateNewHeight(_line_y,
					genericPadding + messagesInterline * 2, newH);
			pdPageContentStream = createPDFNewPage(pdDocument,
					pdPageContentStream, _line_y, newH);
			
			// passenger
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					contents[i][0][0] }, fontTicketSize, fontB, fontColor);
			
			// ticket number
			if (contents[i][0].length > 1 && null != contents[i][0][1]) {
				_line_y = _line_y - genericInterline;
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
						contents[i][0][1] }, fontSizeSmall, fontB, fontColor);
			}
			
			// table header
			_line_y = _line_y - messagesInterline;
			int number_of_columns = contents[i][1].length;
			int number_of_rows = Integer.parseInt(contents[i][2][0]);
			float column_size = ((pageWidth - genericPadding * 2) / number_of_columns);
			for (int k = 0; k < number_of_columns; k++) {
				float line_x1 = _line_x + column_size * k;
				writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
						contents[i][1][k] }, tableFontSize, fontB, fontColor);
			}
			
			// separator
			_line_y = _line_y - genericInterline / 2;
			writeRectangle(pdPageContentStream, _line_x, _line_y,
					pageWidth - genericPadding * 2, borderWidth,
					rowAlternateColor);
			
			// table rows
			_line_y = _line_y - genericInterline + messageInterline;
			for (int k = 0; k < number_of_rows; k++) {
				
				// create a new page if needed
				newH = pageHeight - messagesInterline * 2;
				_line_y = calculateNewHeight(_line_y,
						genericPadding + messagesInterline * 2, newH);
				pdPageContentStream = createPDFNewPage(pdDocument,
						pdPageContentStream, _line_y, newH);
				
				int line_height = 0;
				for (int j = 0; j < number_of_columns; j++) {
					float line_x1 = _line_x + column_size * j;
					if (null != table_data[k + l] && null != table_data[k + l][j]) {
						writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y,
								table_data[k + l][j], fontSizeSmall, font,
								fontColor);
						line_height = table_data[k + l][j].length > line_height
								? table_data[k + l][j].length - 1 : line_height;
					}
				}
				
				// separator
				_line_y = _line_y - genericInterline / 2
						- (genericInterline * (line_height));
				writeRectangle(pdPageContentStream, _line_x, _line_y,
						pageWidth - genericPadding * 2, borderWidth,
						rowAlternateColor);
				_line_y = _line_y - genericInterline + messageInterline;
				
			}
			
			l += number_of_rows;
			
			//_line_y = _line_y - genericInterline + (messageInterline * 2);
			
		}
		
		return pdPageContentStream;
		
	}
	
	/*
	 * create PDF Services Content Footer
	 */
	private static PDPageContentStream createPDFServicesContentFooter(
			PDDocument pdDocument, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontB, float pageWidth, float pageHeight,
			String[][][] contents) throws Exception {
		logger.debug("[PDFCreator] createPDFServicesContentFooter");
		
		// create a new page if needed
		float newH = pageHeight - messagesInterline * 2;
		_line_y = calculateNewHeight(_line_y,
				genericPadding + messagesInterline * 2, newH);
		pdPageContentStream = createPDFNewPage(pdDocument,
				pdPageContentStream, _line_y, newH);
		
		_line_y = _line_y - messagesInterline;
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, contents[0][4],
				fontSizeSmall, font, fontColor);
		
		_line_y = _line_y - genericInterline;
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, contents[0][5],
				fontSizeSmall, fontB, fontColor);
		
		float line_x1 = pageWidth - genericPadding * 2 + smallPadding * 2
				- font.getStringWidth(contents[0][6][0]) / 1000 * fontSize
				- font.getStringWidth(contents[0][6][1]) / 1000 * fontSize;
		writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
				contents[0][6][0] }, fontSize, font, fontColor);
		line_x1 = 
				line_x1 + font.getStringWidth(contents[0][6][0]) / 1000 * fontSize
				+ smallPadding;
		writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
				contents[0][6][1] }, fontSize, fontB, fontColor);
		
		_line_y = _line_y - messagesInterline - genericInterline;
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, contents[0][7],
				fontSizeSmall, font, fontColor);
		
		// return modified stream
		return pdPageContentStream;
	}
	
	/**
	 * Generate Booking PDF
	 * @param output
	 * @param fontFamily
	 * @param fontFamilyBold
	 * @param headerLogo1
	 * @param headerLogo2
	 * @param headerLogo3
	 * @param contents
	 * @throws Exception
	 */
	public final static void generateBookingPDF(OutputStream output, boolean rtl, 
			InputStream fontFamily, InputStream fontFamilyBold,
			InputStream headerLogo1, InputStream headerLogo2,
			InputStream headerLogo3, String[][][] contents,
			String[][][] passengers)
					throws Exception {
		logger.debug("[PDFCreator] generateServicesPDF");
		
		// create a new PDDocument file
		PDDocument pdDocument = new PDDocument();
		
		// attach font to document
		PDFont font = PDType0Font.load(pdDocument, fontFamily);
		PDFont fontB = PDType0Font.load(pdDocument, fontFamilyBold);
		
		writeBookingPDFPage(pdDocument, rtl, font, fontB, headerLogo1, headerLogo2,
				headerLogo3, contents, passengers);
		
		// save document to output and close it!
		pdDocument.save(output);
		pdDocument.close();
	}
	
	/*
	 * write Booking PDF Page
	 */
	private final static void writeBookingPDFPage(PDDocument pdDocument, boolean rtl, 
			PDFont font, PDFont fontB, InputStream headerLogo1,
			InputStream headerLogo2, InputStream headerLogo3,
			String[][][] contents, String[][][] passengers) throws Exception {
		logger.debug("[PDFCreator] writeBookingPDFPage");
		
		// create a new A4 page and add it to document
		PDPage pdPage = new PDPage();
		pdPage.setMediaBox(PDRectangle.A4);
		pdDocument.addPage(pdPage);

		// create a new page content stream
		PDPageContentStream pdPageContentStream = 
				new PDPageContentStream(pdDocument, pdPage, false, false);
		
		createPDFTicketHeader(pdDocument, pdPage, pdPageContentStream, rtl,
				headerLogo1, headerLogo2, headerLogo3);
		
		pdPageContentStream = createPDFBookingContent(pdDocument, pdPage,
				pdPageContentStream, rtl, font, fontB, contents, passengers);
		
		// close content stream
		pdPageContentStream.close();
	}
	
	/*
	 * create PDF Services Content
	 */
	private final static PDPageContentStream createPDFBookingContent(
			PDDocument pdDocument, PDPage pdPage,
			PDPageContentStream pdPageContentStream, boolean rtl,
			PDFont font, PDFont fontB, String[][][] contents,
			String[][][] passengers) throws Exception {
		logger.debug("[PDFCreator] createPDFBookingContent");
		
		// calculate page width & height, set start x & y;
		float pageWidth = pdPage.getMediaBox().getWidth();
		float pageHeight = pdPage.getMediaBox().getHeight();
		_line_x = genericPadding;
		_line_y = pageHeight - logosImageHeight - (messagesInterline * 2);
		
		// header
		if (null != contents[0]) {
			pdPageContentStream = createPDFBookingContentHeader(pdDocument,
					pdPageContentStream, rtl, font, fontB, pageWidth, pageHeight,
					contents);
		}
		
		// flight data
		if (null != contents[1]) {
			pdPageContentStream = createPDFBookingContentFlight(pdDocument,
					pdPageContentStream, rtl, font, fontB, pageWidth, pageHeight,
					contents);
		}
			
		// passengers & data
		if (null != passengers[0]) {
			pdPageContentStream = createPDFBookingContentPassengers(pdDocument,
					pdPageContentStream, rtl, font, fontB, pageWidth, pageHeight,
					passengers);
		}
		
		// return modified stream
		return pdPageContentStream;
	}
	
	/*
	 * create PDF Booking Content Header
	 */
	private static PDPageContentStream createPDFBookingContentHeader(
			PDDocument pdDocument, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontB, float pageWidth, float pageHeight,
			String[][][] contents) throws Exception {
		logger.debug("[PDFCreator] createPDFBookingContentHeader");
		
		// create a new page if needed
		float newH = pageHeight - messagesInterline * 2;
		_line_y = calculateNewHeight(_line_y,
				genericPadding + messagesInterline * 2, newH);
		pdPageContentStream = createPDFNewPage(pdDocument,
				pdPageContentStream, _line_y, newH);
		
		// write line 1 (with PNR)
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] { 
				contents[0][0][0] }, fontSize, font, fontColor);
		float line_x1 = 
				_line_x + font.getStringWidth(contents[0][0][0]) / 1000 * fontSize
					+ smallPadding;
		writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
				contents[0][0][1] }, fontTicketSize, fontB, fontAlternateColor);
		
		// write line 2 (message)
		_line_y = _line_y - messagesInterline;
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, contents[0][1],
				fontSizeSmall, font, fontColor);
		
		_line_y = _line_y - genericInterline * contents[0][1].length;
		
		// return modified stream
		return pdPageContentStream;
		
	}
	
	/*
	 * create PDF Booking Content Flight
	 */
	private static PDPageContentStream createPDFBookingContentFlight(
			PDDocument pdDocument, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontB, float pageWidth, float pageHeight,
			String[][][] contents) throws Exception {
		logger.debug("[PDFCreator] createPDFBookingContentFlight");
		
		_line_y = _line_y - genericInterline * 2;
		
		// passengers
		for (int i = 2; i < contents.length; i++) {
			
			// create a new page if needed
			float newH = pageHeight - messagesInterline * 2;
			_line_y = calculateNewHeight(_line_y,
					genericPadding + messagesInterline * 2, newH);
			pdPageContentStream = createPDFNewPage(pdDocument,
					pdPageContentStream, _line_y, newH);
			
			for (int k = 0; k < contents[i].length; k = k + 5) {
				
				// flight info
				if (k > 0) {
					
					// waiting time
					String waiting_time =
							"---------- " + contents[i][k][0] + " ----------";
					float line_x1 = (pageWidth - font.getStringWidth(waiting_time)
							/ 1000 * fontSizeSmall) / 2 ;
					writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
							waiting_time }, fontSizeSmall, fontB, fontColor);
					
					_line_y = _line_y - genericInterline;
					
				} else {
					writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
							contents[i][k], fontSize, fontB, fontColor);
				}
	
				_line_y = _line_y - messagesInterline;
				
				// flight date and airport information
				float line_y1 = _line_y + (messageInterline * 3);
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x, line_y1,
						new String[] { contents[i][k + 1][0] },
						fontSize, fontB, fontColor);
				line_y1 = _line_y - messageInterline * 3;
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x, line_y1,
						new String[] { contents[i][k + 1][1] }, fontSize,
						fontB, fontColor);

				float line_x1 = 2 * ((pageWidth - genericPadding * 2) / 7);
				line_y1 = _line_y + (messageInterline * 6);
				writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y,
				new String[] { contents[i][k + 2][0] },
				fontSizeHuge, font, fontColor);
				
				float space = font.getStringWidth(contents[i][k + 2][0]) / 1000 * fontSizeHuge;
				line_x1 = line_x1 + space + smallPadding;
				writeLine(pdPageContentStream, rtl, pageWidth, line_x1, line_y1,
						new String[] { contents[i][k + 2][1],
								contents[i][k + 2][2] },
						fontSizeSmall, font, fontColor);
				
				line_x1 = 3 * ((pageWidth - genericPadding * 2) / 7);
				writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y,
						new String[] { contents[i][k + 3][0] },
						fontSizeHuge, font, fontColor);
	
				space = font.getStringWidth(contents[i][k + 3][0]) / 1000 * fontSizeHuge;
				String[] lines = new String[contents[i][k + 3].length - 1];
				for (int j = 1; j < contents[i][k + 3].length; j++) {
					lines[j - 1] = contents[i][k + 3][j];
				}
				line_x1 = line_x1 + space + smallPadding;
				writeLine(pdPageContentStream, rtl, pageWidth, line_x1, line_y1, new String[] {
						contents[i][k + 3][1], contents[i][k + 3][2] },
						fontSizeSmall, font, fontColor);
	
				line_x1 = 5 * ((pageWidth - genericPadding * 2) / 7);
				writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y,
						new String[] { contents[i][k + 4][0] },
						fontSizeHuge, font, fontColor);
	
				space = font.getStringWidth(contents[i][k + 4][0]) / 1000 * fontSizeHuge;
				lines = new String[contents[i][k + 4].length - 1];
				for (int j = 1; j < contents[i][k + 4].length; j++) {
					lines[j - 1] = contents[i][k + 4][j];
				}
				line_x1 = line_x1 + space + smallPadding;
				writeLine(pdPageContentStream, rtl, pageWidth, line_x1, line_y1, lines,
							fontSizeSmall, font, fontColor);
			
				_line_y = _line_y - messagesInterline;
				
			}
			
		}
		
		// create a new page if needed
		float newH = pageHeight - messagesInterline * 2;
		_line_y = calculateNewHeight(_line_y,
				genericPadding + messagesInterline * 2, newH);
		pdPageContentStream = createPDFNewPage(pdDocument,
				pdPageContentStream, _line_y, newH);
		
		// flight recap
		_line_y = _line_y - messagesInterline;
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
				contents[1][0][0] }, fontSize, fontB, fontColor);
		_line_y = _line_y - messagesInterline;
		
		float size1 = font.getStringWidth(contents[1][0][1]) / 1000 * fontSize;
		float size2 = font.getStringWidth(contents[1][0][2]) / 1000 * fontSize;
		float longer = size1 > size2 ? size1 : size2;
		float line_x1 = pageWidth - genericPadding * 2 - longer;
		writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
				contents[1][0][1] }, fontSize, fontB, fontColor);
		float line_y1 = _line_y - genericInterline;
		line_x1 = pageWidth - genericPadding * 2 - longer;
		writeLine(pdPageContentStream, rtl, pageWidth, line_x1, line_y1, new String[] {
				contents[1][0][2] }, fontSize, fontB, fontColor);
		
		for (int j = 1; j < contents[1].length; j++) {
			
			// create a new page if needed
			newH = pageHeight - messagesInterline * 2;
			_line_y = calculateNewHeight(_line_y,
					genericPadding + messagesInterline * 2, newH);
			pdPageContentStream = createPDFNewPage(pdDocument,
					pdPageContentStream, _line_y, newH);

			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					contents[1][j][0] }, fontSizeSmall, font, fontColor);
			
			line_x1 = 2 * pageWidth / 4;
			writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
					contents[1][j][1] }, fontSizeSmall, font, fontColor);
			
			_line_y = _line_y - genericInterline;
			
		}
		
		// return modified stream
		return pdPageContentStream;
		
	}
	
	/*
	 * create PDF Booking Content Passengers
	 */
	private static PDPageContentStream createPDFBookingContentPassengers(
			PDDocument pdDocument, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontB, float pageWidth, float pageHeight,
			String[][][] passengers) throws Exception {
		logger.debug("[PDFCreator] createPDFBookingContentPassengers");
		
		_line_y = _line_y - messagesInterline * 2;
		
		// create a new page if needed
		float newH = pageHeight - messagesInterline * 2;
		_line_y = calculateNewHeight(_line_y,
				genericPadding + messagesInterline * 2, newH);
		pdPageContentStream = createPDFNewPage(pdDocument,
				pdPageContentStream, _line_y, newH);

		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
				passengers[0][0][0] }, fontSize, fontB, fontColor);
		float line_x1 = font.getStringWidth(passengers[0][1][0]) / 1000 * fontSize;
		writeLine(pdPageContentStream, rtl, pageWidth, line_x1, _line_y, new String[] {
				passengers[0][0][1] }, fontSize, font, fontColor);
		
		for (int j = 1; j < passengers[0].length; j++) {
			
			_line_y = _line_y - messagesInterline;
			
			// create a new page if needed
			newH = pageHeight - messagesInterline * 2;
			_line_y = calculateNewHeight(_line_y,
					genericPadding + messagesInterline * 2, newH);
			pdPageContentStream = createPDFNewPage(pdDocument,
					pdPageContentStream, _line_y, newH);

			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					passengers[0][j][0] }, tableFontSize, fontB, fontColor);
			
			_line_y = _line_y - genericInterline;
			
			String[] lines = new String[passengers[0][j].length - 1];
			for (int i = 0; i < lines.length; i++) {
				lines[i] = passengers[0][j][i + 1];
			}
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, lines,
					tableFontSize, font, fontColor);
			
			_line_y = _line_y - fontSize * passengers[0][j].length - 1
					+ messagesInterline;
			
		}
		
		_line_y = _line_y - messagesInterline * 2;
		
		// create a new page if needed
		newH = pageHeight - messagesInterline * 2;
		_line_y = calculateNewHeight(_line_y,
				genericPadding + messagesInterline * 2, newH);
		pdPageContentStream = createPDFNewPage(pdDocument,
				pdPageContentStream, _line_y, newH);
		
		// contacts data
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
				passengers[1][0], fontSize, fontB, fontColor);
		
		_line_y = _line_y - genericInterline;
		
		writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
				passengers[1][1], fontSizeSmall, font, fontColor);
		
		_line_y = _line_y - genericInterline / 2;
		
		for (int i = 2; i < passengers[1].length; i++) {
			
			_line_y = _line_y - genericInterline;
			
			// create a new page if needed
			newH = pageHeight - messagesInterline * 2;
			_line_y = calculateNewHeight(_line_y,
					genericPadding + messagesInterline * 2, newH);
			pdPageContentStream = createPDFNewPage(pdDocument,
					pdPageContentStream, _line_y, newH);
			
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
					passengers[1][i], tableFontSize, font, fontColor);
			
		}
		
		// return modified stream
		return pdPageContentStream;
		
	}
	
	/**
	 * Generate Balance PDF
	 * @param output
	 * @param fontFamily
	 * @param fontFamilyBold
	 * @param headerLogo
	 * @param headerTitle
	 * @param userName
	 * @param userMigliaLabel
	 * @param userMiglia
	 * @param userStatusLabel
	 * @param userStatus
	 * @throws Exception
	 */
	public final static void generateBalancePDF(OutputStream output, boolean rtl, 
			InputStream fontFamily, InputStream fontFamilyBold,
			InputStream headerLogo, String headerTitle, String userName,
			String userMigliaLabel, String userMiglia, String userStatusLabel,
			String userStatus, String[][] summary, String[][] contents)
					throws Exception {
		logger.debug("[PDFCreator] generateBalancePDF");

		// create a new PDDocument file
		PDDocument pdDocument = new PDDocument();
		
		// attach font to document
		PDFont font = PDType0Font.load(pdDocument, fontFamily);
		PDFont headerFont = PDType0Font.load(pdDocument, fontFamilyBold);
					
		// split table's lines into pages
		ArrayList<String[][]> newContents = new ArrayList<String[][]>();
		for (int j = 0; j <= contents.length; j = j + linesPerPage) {
			int lastElement = 
					(contents.length - j < linesPerPage
							? contents.length - j : linesPerPage);
			String[][] newContent = new String[lastElement][];
			for (int i = 0; i < lastElement; i++) {
				newContent[i] = contents[j + i];
			}
			newContents.add(newContent);
		}
		
		for (String[][] newContent : newContents) {
			writeLinesIntoPDFPage(pdDocument, rtl, font, headerFont, headerLogo,
					headerTitle, userName, userMigliaLabel, userMiglia,
					userStatusLabel, userStatus, summary, newContent);
		}
		
		// save document to output and close it!
		pdDocument.save(output);
		pdDocument.close();

	}
	
	/*
	 * Write table's lines into PDF 
	 */
	private final static void writeLinesIntoPDFPage(PDDocument pdDocument,
			boolean rtl, PDFont font, PDFont headerFont, InputStream headerLogo,
			String headerTitle, String userName, String userMigliaLabel,
			String userMiglia, String userStatusLabel, String userStatus,
			String[][] summary, String[][] contents) throws Exception {
		
		// create a new A4 page and add it to document
		PDPage pdPage = new PDPage();
		pdPage.setMediaBox(PDRectangle.A4);
		pdDocument.addPage(pdPage);

		// create a new page content stream
		PDPageContentStream pdPageContentStream = 
				new PDPageContentStream(pdDocument, pdPage, false, false);
		
		// add header
		createPDFHeader(pdDocument, pdPage, pdPageContentStream, rtl,
				font, headerFont, headerLogo, headerTitle, userName,
				userMigliaLabel, userMiglia, userStatusLabel, userStatus);
		
		// add contents
		createPDFBalanceContent(pdDocument, pdPage, pdPageContentStream,
				rtl, font, summary, contents);
		
		// close content stream
		pdPageContentStream.close();
	}
	
	/*
	 * create PDF header
	 */
	private final static void createPDFHeader(PDDocument pdDocument,
			PDPage pdPage, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont headerFont, InputStream headerLogo,
			String headerTitle, String userName, String userMigliaLabel,
			String userMiglia, String userStatusLabel, String userStatus)
					throws Exception {
		logger.debug("[PDFCreator] createPDFHeader");
		
		// calculate positions
		float pageWidth = pdPage.getMediaBox().getWidth();
		float pageHeight = pdPage.getMediaBox().getHeight();
		float logoX = ((pageWidth / 2 + logoImageSize / 2));
		float logoY = ((pageHeight - (logoImageSize + genericPadding)));

		// load logo image
		pdPixelMap = null;
		BufferedImage bufferedImage = ImageIO.read(headerLogo);
		if (null != bufferedImage && null == pdPixelMap) {
			pdPixelMap = LosslessFactory.createFromImage(pdDocument, bufferedImage);
			bufferedImage.flush();
		}
		pdPageContentStream.drawImage(pdPixelMap, 
				rtl ? pageWidth - logoX - logoImageSize : logoX, logoY,
				logoImageSize, logoImageSize);
		
		// add page title
		float titleX = genericPadding;
		float titleY = pageHeight - headerFontSize - genericPadding;
		writeLine(pdPageContentStream, rtl, pageWidth, titleX, titleY, new String[]{headerTitle},
				headerFontSize, headerFont, fontAlternateColor);
		
		// user data block
		float userBlockX = logoX + logoImageSize + headerSpace;
		float userBlockY = pageHeight - fontSize - genericPadding;
		
		// user name
		writeLine(pdPageContentStream, rtl, pageWidth, userBlockX, userBlockY,
				new String[]{userName}, fontSize, font, fontColor);
		
		// user miglia label
		float migliaY = userBlockY - fontSize;
		writeLine(pdPageContentStream, rtl, pageWidth, userBlockX, migliaY,
				new String[]{userMigliaLabel}, fontSize, font, fontColor);
		
		// user miglia
		float userMigliaLabelWidth = 
				font.getStringWidth(userMigliaLabel) / 1000 * fontSize;
		float migliaX = userBlockX + headerSpace + userMigliaLabelWidth;
		writeLine(pdPageContentStream, rtl, pageWidth, migliaX, migliaY,
				new String[]{userMiglia}, fontSize, font, fontAlternateColor);
		
		// user status label
		float statusY = userBlockY - fontSize * 2;
		writeLine(pdPageContentStream, rtl, pageWidth, userBlockX, statusY,
				new String[]{userStatusLabel}, fontSize, font, fontColor);
		
		// user status
		float userStatusLabelWidth = 
				font.getStringWidth(userStatusLabel) / 1000 * fontSize;
		float statusX = userBlockX + headerSpace + userStatusLabelWidth;
		writeLine(pdPageContentStream, rtl, pageWidth, statusX, statusY,
				new String[]{userStatus}, fontSize, font, fontAlternateColor);
		
	}
	
	/*
	 * create PDF messages content
	 */
	private static void createPDFMessagesContent(PDDocument pdDocument,
			PDPage pdPage, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, ArrayList<String[][][]> contents) throws Exception {
		logger.debug("[PDFCreator] createPDFMessagesContent");
		
		float pageWidth = pdPage.getMediaBox().getWidth();
		float pageHeight = pdPage.getMediaBox().getHeight();
		
		float tableX = genericPadding;
		float tableY = pageHeight - headerHeight;
		float messageX = pageWidth / 3;
		
		float lineHeight = 0;
		
		for (String[][][] arrayOfContents : contents) {
			for (String[][] content : arrayOfContents) {
				
				tableY = tableY - lineHeight - messagesInterline;
				float lineY = tableY - fontSize - messageInterline;
				float secondLineY = tableY - fontSize * 2 - messageInterline * 2;
				
				String[] text = content[4];
				lineHeight = ((text.length + 1) * genericInterline) + 
						(fontSize * 2 + messageInterline * 2);
				
				// draw background
				float bgX = tableX - (messageInterline * 3);
				float bgY = tableY - lineHeight + genericPadding;
				float bgWidth = pageWidth - (genericPadding * 2)
						+ (messageInterline * 5);
				float bgHeight = lineHeight;
				writeRectangle(pdPageContentStream, bgX, bgY, bgWidth,
						bgHeight, bgColor);
				
				// draw texts
				writeLine(pdPageContentStream, rtl, pageWidth, tableX, tableY,
						content[0], headingFontSize, font,
						fontColor);
				writeLine(pdPageContentStream, rtl, pageWidth, tableX, lineY,
						content[3], headingFontSize, font,
						fontAlternateColor);

				writeLine(pdPageContentStream, rtl, pageWidth, messageX, tableY,
						content[1], headingFontSize, font,
						fontColor);
				writeLine(pdPageContentStream, rtl, pageWidth, messageX, lineY,
						content[2], headingFontSize, font,
						fontAlternateColor);
				
				writeLine(pdPageContentStream, rtl, pageWidth, messageX, secondLineY,
						content[4], headingFontSize, font, fontColor);
				
			}
		}
	}
	
	/*
	 * create PDF messages content
	 */
	private static void createPDFBalanceContent(PDDocument pdDocument,
			PDPage pdPage, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, String[][] summary, String[][] contents)
					throws Exception {
		logger.debug("[PDFCreator] createPDFBalanceContent");
		
		float pageWidth = pdPage.getMediaBox().getWidth();
		float pageHeight = pdPage.getMediaBox().getHeight();
		
		// add summary
		float summaryCellWidth = (pageWidth - genericPadding * 2) / 4;
		float cellX = genericPadding - summaryCellWidth;
		float cellY = pageHeight - headerHeight - genericPadding;
		float cellY2Line = cellY - headingFontSize - messageInterline;
		int i = 0;
		for (String[] cell : summary) {
			
			cellX = cellX + summaryCellWidth;
			i++;
			
			// draw texts
			writeLine(pdPageContentStream, rtl, pageWidth, cellX, cellY,
					new String[] { cell[0] }, headingFontSize, font,
					fontAlternateColor);
			writeLine(pdPageContentStream, rtl, pageWidth, cellX, cellY2Line,
					new String[] { cell[1] }, tableFontSize, font,
					fontColor);
			
			if (i < summary.length) {
				float borderHeight = 
						headingFontSize + messageInterline + tableFontSize;
				float borderX = cellX + summaryCellWidth - borderPadding;
				writeRectangle(pdPageContentStream, borderX, cellY2Line,
						borderWidth, borderHeight, borderColor);
			}
			
		}
		
		// add table rows
		float tableLineheigth = tableFontSize * 2 + messageInterline * 3;
		float tableX = genericPadding;
		float tableY = cellY2Line - genericPadding + tableLineheigth;
		float tableCellWidth = (pageWidth - genericPadding * 2) / 6;
		i = 0;
		for (String[] content : contents) {
			
			tableY = tableY - tableLineheigth - messageInterline * 2;
			
			// alternate background
			i++;
			float bgHeight = tableY - tableLineheigth + tableFontSize;
			if (0 != i % 2) {
				writeRectangle(pdPageContentStream, 0, bgHeight,
						pageWidth, tableLineheigth, bgColor);
			}
			
			// draw date
			writeLine(pdPageContentStream, rtl, pageWidth, tableX, tableY - messageInterline,
					new String[] {content[4]}, tableFontSize, font, fontColor);
			
			// draw activity
			writeLine(pdPageContentStream, rtl, pageWidth, tableCellWidth,
					tableY - messageInterline, new String[] {content[0]},
					tableFontSize, font, fontAlternateColor);
			writeLine(pdPageContentStream, rtl, pageWidth, tableCellWidth,
					tableY - messageInterline - tableFontSize,
					new String[] {content[5]}, tableFontSize, font,
					fontColor);

			// draw class
			writeLine(pdPageContentStream, rtl, pageWidth, tableCellWidth * 3,
					tableY - messageInterline, new String[] {content[1]},
					tableFontSize, font, fontAlternateColor);
			writeLine(pdPageContentStream, rtl, pageWidth, tableCellWidth * 3,
					tableY - messageInterline - tableFontSize,
					new String[] {content[6]}, tableFontSize, font,
					fontColor);
			
			// draw description
			writeLine(pdPageContentStream, rtl, pageWidth, tableCellWidth * 4,
					tableY - messageInterline, new String[] {content[2]},
					tableFontSize, font, fontAlternateColor);
			writeLine(pdPageContentStream, rtl, pageWidth, tableCellWidth * 4,
					tableY - messageInterline - tableFontSize,
					new String[] {content[7]}, tableFontSize, font,
					fontColor);
			
			// draw miles
			writeLine(pdPageContentStream, rtl, pageWidth, tableCellWidth * 5.5f,
					tableY - messageInterline, new String[] {content[3]},
					tableFontSize, font, fontAlternateColor);
			writeLine(pdPageContentStream, rtl, pageWidth, tableCellWidth * 5.5f,
					tableY - messageInterline - tableFontSize,
					new String[] {content[8]}, tableFontSize, font,
					fontColor);

		}
		
	}
	
	/**
	 * Generate Check-in PDF
	 * @param output
	 * @param fontFamily
	 * @param fontFamilyBold
	 * @param headerLogo1
	 * @param headerLogo2
	 * @param headerLogo3
	 * @param contents
	 * @param cutHere
	 * @param time
	 * @param bag
	 * @param boardingPass
	 * @param banners
	 * @throws Exception
	 */
	public static void generateCheckinPDF(OutputStream output, boolean rtl, 
			InputStream fontFamily, InputStream fontFamilyBold,
			InputStream headerLogo1, InputStream headerLogo2,
			InputStream headerLogo3, List<String[][][]> contents,
			InputStream cutHere, InputStream time, InputStream bag,
			List<InputStream> boardingPass, List<InputStream> banners)
					throws Exception {
		logger.debug("[PDFCreator] generateCheckinPDF");
		
		// create a new PDDocument file
		PDDocument pdDocument = new PDDocument();

		// attach font to document
		// PDFont font = PDType1Font.HELVETICA;
		// PDFont fontBold = PDType1Font.HELVETICA_BOLD;
		
		PDFont font = PDType0Font.load(pdDocument, fontFamily);
		PDFont fontBold;
		if (fontFamilyBold != null) {
			fontBold = PDType0Font.load(pdDocument, fontFamilyBold);
		} else {
			fontBold = font;
		}
		
		// 1 page : 1 passenger
		int i = -1;
		BufferedImage cutHereBuffer = ImageIO.read(cutHere);
		BufferedImage timeBuffer = ImageIO.read(time);
		BufferedImage bagBuffer = ImageIO.read(bag);
		
		pdPixelMapBanner = new PDImageXObject[banners.size()];
		
		for (String[][][] content : contents) {
			generateCheckinPDFPage(pdDocument, rtl, font, fontBold, headerLogo1,
					headerLogo2, headerLogo3, content, cutHereBuffer,
				timeBuffer, bagBuffer, boardingPass.size() > 0 ? boardingPass.get(++i) : null, banners);
		}
		
		cutHereBuffer.flush();
		timeBuffer.flush();
		bagBuffer.flush();
		
		// save document to output and close it!
		pdDocument.save(output);
		pdDocument.close();
		
	}
	
	/*
	 * generate messages PDF page
	 */
	private static void generateCheckinPDFPage(PDDocument pdDocument,
			boolean rtl, PDFont font, PDFont fontBold, InputStream headerLogo1,
			InputStream headerLogo2, InputStream headerLogo3,
			String[][][] contents, BufferedImage cutHere, BufferedImage time,
			BufferedImage bag, InputStream boardingPass,
			List<InputStream> banners) throws Exception {
		logger.debug("[PDFCreator] generateCheckinPDFPage");
		
		// create a new A4 page and add it to document
		PDPage pdPage = new PDPage();
		pdPage.setMediaBox(PDRectangle.A4);
		pdDocument.addPage(pdPage);
		
		// create a new page content stream
		PDPageContentStream pdPageContentStream = 
				new PDPageContentStream(pdDocument, pdPage, false, true);
		
		_line_x = genericPadding;
		_line_y = pdPage.getMediaBox().getHeight() - genericPadding;
		
		// content
		createPDFCheckinContent(pdDocument, pdPage, pdPageContentStream,
				rtl, font, fontBold, contents, headerLogo1,
				headerLogo2, headerLogo3, 1, boardingPass);
		
		// additional content data
		createPDFCheckinContentAdditional(pdDocument, pdPage,
				pdPageContentStream, rtl, font, fontBold, contents, cutHere,
				time, bag);
		
		// content
		createPDFCheckinContent(pdDocument, pdPage, pdPageContentStream,
				rtl, font, fontBold, contents, headerLogo1,
				headerLogo2, headerLogo3, 2, boardingPass);
		
		// banners & notice
		createPDFCheckinContentBannersNotice(pdDocument, pdPage,
				pdPageContentStream, rtl, font, fontBold, contents, banners);
		
		// close content stream
		pdPageContentStream.close();
		
	}
	
	/*
	 * Create PDF Check-in Content
	 */
	private static void createPDFCheckinContent(PDDocument pdDocument,
			PDPage pdPage, PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontBold, String[][][] contents,
			InputStream headerLogo1, InputStream headerLogo2,
			InputStream headerLogo3, int index, InputStream boardingPass)
					throws Exception {
		logger.debug("[PDFCreator] createPDFCheckinContent");
		
		float pageWidth = pdPage.getMediaBox().getWidth();
		
		// header data
		if (null != contents[0]) {
			
			// header
			createPDFCheckinHeader(pdDocument, pdPage, pdPageContentStream, rtl,
					headerLogo1, headerLogo2, headerLogo3);
			
			// heading
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
					new String[] { contents[0][0][0] },
					headerFontSize, fontBold, fontColor);
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y - genericInterline,
					new String[] { contents[0][0][index] },
					fontSizeSmall, fontBold, fontColor); // index = 1 or 2
			
			// sky priority
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y - 2*genericInterline,
					new String[] { contents[0][0][3] },
					fontSizeSmall, fontBold, fontRedColor);
			
			_line_y = _line_y - genericPadding - genericInterline;
			float bp_lineY = _line_y;
			float bp_lineX = (pageWidth - genericPadding * 2) / 2
					+ genericPadding * 4.5f;
			float ss_x = (pageWidth - genericPadding * 2) / 2
					+ genericPadding * 3.7f;
			
			// passenger's details
			for (int i = 2; i < contents[0].length; i++) {
				
				_line_y = _line_y - genericInterline;
				
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
						new String[] { contents[0][i][0] },
						fontSizeSmall, font, fontColor);
				
				writeLine(pdPageContentStream, rtl, pageWidth, 150,
						_line_y, new String[] { contents[0][i][1] },
						fontSizeSmall, fontBold, fontColor);
				
			}

			// sequence number
			writeLine(pdPageContentStream, rtl, pageWidth, ss_x, bp_lineY,
					new String[] { contents[0][1][0] },
					fontSizeSmall, fontBold, fontColor);
			writeLine(pdPageContentStream, rtl, pageWidth, ss_x + ss_x / 4,
					bp_lineY, new String[] { contents[0][1][1] },
					fontSizeSmall, font, fontColor);
			writeLine(pdPageContentStream, rtl, pageWidth, ss_x + (ss_x / 4) * 2,
					bp_lineY, new String[] { contents[0][1][2] },
					fontSizeSmall, font, fontColor);
			
			// boarding pass BAR CODE
			if (null != boardingPass) {
				pdPixelMap = null;
				boardingPass.reset();
				//BufferedImage bufferedImage = ImageIO.read(boardingPass);
				java.awt.image.BufferedImage bufferedImage = javax.imageio.ImageIO.read(boardingPass);
				if (null != bufferedImage && null == pdPixelMap) {
					pdPixelMap = LosslessFactory.createFromImage(pdDocument, bufferedImage);
					bufferedImage.flush();
				}
				pdPageContentStream.drawImage(pdPixelMap,
						rtl ? pageWidth - bp_lineX + genericPadding - boardingPassWidth :
						bp_lineX - genericPadding,
						bp_lineY - boardingPassHeigth - (messageInterline * 3),
						boardingPassWidth, boardingPassHeigth);
				
				_line_y = _line_y - (messageInterline * 3) + 30;
			}
		}
		
		if (null != contents[1]) {
			createPDFCheckinContentTable(pdPage, pdPageContentStream, rtl, font,
					fontBold, contents);
		}
		
	}
	
	/*
	 * Create PDF Ticket Header
	 */
	private static void createPDFCheckinHeader(PDDocument pdDocument,
			PDPage pdPage, PDPageContentStream pdPageContentStream, boolean rtl,
			InputStream headerLogo1, InputStream headerLogo2,
			InputStream headerLogo3) throws IOException {
		logger.debug("[PDFCreator] createPDFCheckinHeader");
		
		// calculate positions
		float pageWidth = pdPage.getMediaBox().getWidth();
		_line_y = _line_y - logosImageHeight_c - (messageInterline * 3);

		// size of logos
		float _logo1size = logo1ImageSize_c + genericInterline;
		float _logo2size = logo2ImageSize_c + genericInterline;
		float _logo3size = logo3ImageSize_c + genericInterline * 2;
		
		// load & draw logo1 image
		pdPixelMap = null;
		float logoX = pageWidth - _logo2size - _logo1size;
		headerLogo1.reset();
		BufferedImage bufferedImage = ImageIO.read(headerLogo1);
		if (null != bufferedImage && null == pdPixelMap) {
			pdPixelMap = LosslessFactory.createFromImage(pdDocument, bufferedImage);
			bufferedImage.flush();
		}
		pdPageContentStream.drawImage(pdPixelMap, 
				rtl ? pageWidth - logoX - logo1ImageSize_c : logoX, _line_y,
				logo1ImageSize_c, logosImageHeight_c);

		// load and draw logo2 image
		pdPixelMap = null;
		logoX = logoX + _logo1size;
		headerLogo2.reset();
		bufferedImage = ImageIO.read(headerLogo2);
		if (null != bufferedImage && null == pdPixelMap) {
			pdPixelMap = LosslessFactory.createFromImage(pdDocument, bufferedImage);
			bufferedImage.flush();
		}	
		pdPageContentStream.drawImage(pdPixelMap, 
				rtl ? pageWidth - logoX - logo2ImageSize_c : logoX, _line_y,
				logo2ImageSize_c, logosImageHeight_c);
		
		// load and draw logo3 image
		pdPixelMap = null;
		logoX = pageWidth - _logo2size - _logo1size - _logo3size;
		headerLogo3.reset();
		bufferedImage = ImageIO.read(headerLogo3);
		if (null != bufferedImage && null == pdPixelMap) {
			pdPixelMap = LosslessFactory.createFromImage(pdDocument, bufferedImage);
			bufferedImage.flush();
		}
		pdPageContentStream.drawImage(pdPixelMap, 
				rtl ? pageWidth - logoX - logo3ImageSize_c : logoX, _line_y,
				logo3ImageSize_c, logosImageHeight_c);
		
		_line_y = _line_y + genericPadding - smallPadding * 2;

	}
	
	/*
	 * Create PDF Check-in Content Table
	 */
	private static void createPDFCheckinContentTable(PDPage pdPage,
			PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontBold, String[][][] contents)
					throws Exception {
		logger.debug("[PDFCreator] createPDFCheckinContentTable");
		
		float pageWidth = pdPage.getMediaBox().getWidth();
		
		_line_y = _line_y - messagesInterline - genericPadding;
		
		float[] previousColumnSize = {
				0.5f, 1, 0.5f, 2, 2, 1, 0.5f, 1, 1
		};
		
		// flight table header (part 1)
		float maxLength = (pageWidth - (genericPadding * 2));
		float _lineX = _line_x;
		for (int j = 0; j < contents[1][0].length; j++) {
			
			float slot = (maxLength / contents[1][0].length)
					* (j > 0 ? previousColumnSize[j - 1] : 0);
			
			_lineX = _lineX + slot;
			writeLine(pdPageContentStream, rtl, pageWidth, _lineX,
					_line_y, new String[] { contents[1][0][j] },
					fontSizeMicro, font, fontColor);
			
		}
		
		_line_y = _line_y - fontSize;
		
		// flight table header (part 2)
		_lineX = _line_x;
		for (int j = 0; j < contents[1][1].length; j++) {
			
			float slot = (maxLength / contents[1][1].length)
					* (j > 0 ? previousColumnSize[j - 1] : 0);
			
			_lineX = _lineX + slot;
			writeLine(pdPageContentStream, rtl, pageWidth, _lineX,
					_line_y, new String[] { contents[1][1][j] },
					fontSizeMicro, fontBold, fontColor);
			
		}

		// separator
		_line_y = _line_y - smallPadding;
		writeRectangle(pdPageContentStream, _line_x, _line_y,
				pageWidth - genericPadding * 2, borderWidth, fontColor);
		_line_y = _line_y - smallPadding * 2;
		
		// flight table content (the flight data)
		_lineX = _line_x;
		for (int k = 0; k < contents[1][2].length; k++) {
			
			float slot = (maxLength / contents[1][2].length)
					* (k > 0 ? previousColumnSize[k - 1] : 0);
			
			_lineX = _lineX + slot;
			writeLine(pdPageContentStream, rtl, pageWidth, _lineX,
					_line_y, new String[] { contents[1][2][k] },
					fontSizeMicro, font, fontColor);
			
		}
		
		// Operato da BUS
		if (contents[1][3] != null) {
			_line_y = _line_y - smallPadding * 2;
			_lineX = _line_x;
			for (int k = 0; k < contents[1][3].length; k++) {
				
				float slot = (maxLength / contents[1][3].length)
						* (k > 0 ? previousColumnSize[k - 1] : 0);
				
				_lineX = _lineX + slot;
				writeLine(pdPageContentStream, rtl, pageWidth, _lineX,
						_line_y, new String[] { contents[1][3][k] },
						fontSizeMicro, font, fontColor);
				
			}
		}
		
		// separator
		_line_y = _line_y - (smallPadding * 2 + elfSpace);
		elfSpace = 0;
		writeRectangle(pdPageContentStream, _line_x, _line_y,
				pageWidth - genericPadding * 2, borderWidth, fontColor);
		_line_y = _line_y - smallPadding * 2;
		
	}
	
	/*
	 * Create PDF Check-in Content Additional
	 */
	private static void createPDFCheckinContentAdditional(
			PDDocument pdDocument, PDPage pdPage,
			PDPageContentStream pdPageContentStream,
			boolean rtl, PDFont font, PDFont fontBold, String[][][] contents,
			BufferedImage cutHere, BufferedImage time, BufferedImage bag)
					throws Exception {
		logger.debug("[PDFCreator] createPDFCheckinContentAdditional");
		
		float pageWidth = pdPage.getMediaBox().getWidth();
		
		// draw "notice" area
		if (null != contents[2]) {
			
			_line_y = _line_y - genericInterline;
			
			float _lineX1 = _line_x + genericPadding;
			writeLine(pdPageContentStream, rtl, pageWidth, _lineX1 + smallPadding * 2, _line_y,
					new String[] { contents[2][0][0] },
					tableFontSize, fontBold, fontColor);
			
			float _lineX2 =
					((pageWidth - genericPadding * 2) / 2) + genericPadding * 2;
			writeLine(pdPageContentStream, rtl, pageWidth, _lineX2 + smallPadding * 2, _line_y,
					new String[] { contents[2][1][0] },
					tableFontSize, fontBold, fontColor);

			// bag logo
			pdPixelMap = null;
			if (null != bag && null == pdPixelMap) {
				pdPixelMap = LosslessFactory.createFromImage(pdDocument, bag);
			}
			pdPageContentStream.drawImage(pdPixelMap,
					rtl ? pageWidth - _lineX1 + genericPadding - checkinLogoWidth :
					_lineX1 - genericPadding, _line_y - genericInterline/2,
					checkinLogoWidth, checkinLogoHeight);
			
			float _lineY = _line_y;
			for (int i = 1; i < contents[2][0].length; i++) {
				_lineY = _lineY - middleInterline;
				writeLine(pdPageContentStream, rtl, pageWidth, _lineX1 + smallPadding * 2,
						_lineY, new String[] { contents[2][0][i] },
						fontSizeSmall, font, fontColor);
			}

			// time logo
			pdPixelMap = null;
			if (null != time && null == pdPixelMap) {
				pdPixelMap = LosslessFactory.createFromImage(pdDocument, time);
			}
			pdPageContentStream.drawImage(pdPixelMap,
					rtl ? pageWidth - _lineX2 + genericPadding - checkinLogoWidth :
					_lineX2 - genericPadding, _line_y - genericInterline/2,
					checkinLogoWidth, checkinLogoHeight);
			
			_lineY = _line_y;
			for (int i = 1; i < contents[2][1].length; i++) {
				_lineY = _lineY - middleInterline;
				writeLine(pdPageContentStream, rtl, pageWidth, _lineX2 + smallPadding * 2,
						_lineY, new String[] { contents[2][1][i] },
						fontSizeSmall, font, fontColor);
			}
			_line_y = _line_y - Math.max(contents[2][0].length - 1, contents[2][1].length - 1) * middleInterline;
		}
		_line_y = _line_y - 2 * middleInterline;
		
		// draw "cut here" area
		if (null != contents[3]) {
			
			float lineY1 = _line_y;
			_line_y = _line_y - genericInterline;
			
			// cut here logo
			pdPixelMap = null;
			if (null != cutHere && null == pdPixelMap) {
				pdPixelMap = LosslessFactory.createFromImage(pdDocument, cutHere);
			}
			pdPageContentStream.drawImage(pdPixelMap, _line_x, _line_y,
					(pageWidth - genericPadding * 2), checkinLogoHeight);
			
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x + genericPadding, lineY1,
					new String[] { contents[3][0][0] },
					fontSizeSmall, fontBold, fontColor);

		}
		
		_line_y = _line_y - genericInterline;
		
	}
	
	/*
	 * Create PDF Check-in Content Banners & Notice
	 */
	private static void createPDFCheckinContentBannersNotice(
			PDDocument pdDocument, PDPage pdPage,
			PDPageContentStream pdPageContentStream, 
			boolean rtl, PDFont font, PDFont fontBold,
			String[][][] contents, List<InputStream> banners) throws Exception {
		logger.debug("[PDFCreator] createPDFCheckinContentNotice");
		
		float pageWidth = pdPage.getMediaBox().getWidth();
		
		// banners
		if (0 < banners.size()) {
			
			float imageSize = (pageWidth - genericPadding * 2) / banners.size();

			_line_y = _line_y - bannerHeight;
			
			for (int i = 0; i < banners.size(); i++) {
			
				float lineX = genericPadding + imageSize * i;
				
				InputStream banner = banners.get(i);
				
				if (null != banner && null == pdPixelMapBanner[i]) {
					try {
						pdPixelMapBanner[i] = JPEGFactory.createFromStream(pdDocument, banner);
					} catch (IIOException e) {
						logger.warn("Banner {}: {}", String.valueOf(i + 1), e.getMessage());
					}
				}
				
				if (null != pdPixelMapBanner[i]) {
					pdPageContentStream.drawImage(pdPixelMapBanner[i],
							lineX + ((imageSize - bannerWidth) / 2), _line_y,
							bannerWidth, bannerHeight);
				}
			}
			
			_line_y = _line_y - messagesInterline;
			
		}
		
		if (null != contents[3]) {
		
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
					contents[3][1], tableFontSize, fontBold, fontColor);

			_line_y = _line_y - genericInterline;
			
			float _lineY = _line_y;
			for (int i = 0; i < contents[3][2].length; i++) {
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x,
						_lineY, new String[] { contents[3][2][i] },
						fontSizeMicro, font, fontColor);
				_lineY = _lineY - middleInterline;
			}
		}
		
	}
	
	/**
	 * Generate Check-in Summary PDF
	 * @param output
	 * @param fontFamily
	 * @param fontFamilyBold
	 * @param headerLogo1
	 * @param headerLogo2
	 * @param headerLogo3
	 * @param contents
	 * @throws Exception
	 */
	public static void generateCheckinSummaryPDF(OutputStream output, boolean rtl, 
			InputStream fontFamily, InputStream fontFamilyBold,
			InputStream headerLogo1, InputStream headerLogo2,
			InputStream headerLogo3, String[][][] contents)
					throws Exception {
		logger.debug("[PDFCreator] generateCheckinSummaryPDF");
		
		// create a new PDDocument file
		PDDocument pdDocument = new PDDocument();

		// attach font to document
		PDFont font = PDType0Font.load(pdDocument, fontFamily);
		PDFont fontBold = PDType0Font.load(pdDocument, fontFamilyBold);

		// 1 page : 1 passenger
		generateCheckinSummaryPDFPage(pdDocument, rtl, font, fontBold, headerLogo1,
				headerLogo2, headerLogo3, contents);

		// save document to output and close it!
		pdDocument.save(output);
		pdDocument.close();
		
	}
	
	/*
	 * Generate Check-in Summary PDF Page
	 */
	private static void generateCheckinSummaryPDFPage(PDDocument pdDocument,
			boolean rtl, PDFont font, PDFont fontBold, InputStream headerLogo1,
			InputStream headerLogo2, InputStream headerLogo3,
			String[][][] contents) throws Exception {
		logger.debug("[PDFCreator] generateCheckinSummaryPDFPage");
		
		// create a new A4 page and add it to document
		PDPage pdPage = new PDPage();
		pdPage.setMediaBox(PDRectangle.A4);
		pdDocument.addPage(pdPage);
		
		// create a new page content stream
		PDPageContentStream pdPageContentStream = 
				new PDPageContentStream(pdDocument, pdPage, false, false);
		
		_line_x = genericPadding;
		_line_y = pdPage.getMediaBox().getHeight() - genericPadding;
		
		float pageWidth = pdPage.getMediaBox().getWidth();
		float pageHeight = pdPage.getMediaBox().getHeight();
		
		// header
		createPDFCheckinHeader(pdDocument, pdPage, pdPageContentStream, rtl,
				headerLogo1, headerLogo2, headerLogo3);
		
		// heading
		if (null != contents[0]) {
			
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					contents[0][0][0] }, fontSizeHuge, fontBold,
					fontAlternateColor);
			
			_line_y = _line_y - messagesInterline;
			
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					contents[0][0][1] }, fontSize, fontBold,
					fontAlternateColor);
			
			_line_y = _line_y - messagesInterline;
			
			for (int i = 2; i < contents[0][0].length; i++) {
				
				_line_y = _line_y - genericInterline;
				
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
						contents[0][0][i] }, tableFontSize, font, fontColor);
				
			}
			
		}
		
		// all tables
		for (int k = 1; k < contents.length; k++) {
			generateCheckinSummaryPDFPageTable(pdDocument, pdPage,
					pdPageContentStream, rtl, font, fontBold, contents, k);
		}
		
		// notice
		if (null != contents[0]) {
			
			// create a new page if needed
			float newH = pageHeight - messagesInterline * 2;
			_line_y = calculateNewHeight(_line_y,
					genericPadding + messagesInterline * 2, newH);
			pdPageContentStream = createPDFNewPage(pdDocument,
					pdPageContentStream, _line_y, newH);
			
			_line_y = _line_y - messagesInterline;
			
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
					contents[0][3][0] }, tableFontSize, font, fontColor);
			
			for (int i = 1; i < contents[0][3].length; i++) {
				
				_line_y = _line_y - genericInterline;
				
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
						contents[0][3][i] }, fontSizeSmall, font, fontColor);
			}
			
		}
		
		// close content stream
		pdPageContentStream.close();
		
	}
	
	/*
	 * Generate Check-in Summary PDF Page Table
	 */
	private static void generateCheckinSummaryPDFPageTable(
			PDDocument pdDocument, PDPage pdPage,
			PDPageContentStream pdPageContentStream, boolean rtl, 
			PDFont font, PDFont fontBold, String[][][] contents, int index)
					throws Exception {
		
		if (null == contents[index]) {
			return;
		}
		
		float pageWidth = pdPage.getMediaBox().getWidth();
		float pageHeight = pdPage.getMediaBox().getHeight();
		
		_line_y = _line_y - genericInterline * 2;
		
		// create a new page if needed
		float newH = pageHeight - messagesInterline * 2;
		_line_y = calculateNewHeight(_line_y,
				genericPadding + messagesInterline * 2, newH);
		pdPageContentStream = createPDFNewPage(pdDocument,
				pdPageContentStream, _line_y, newH);
		
		// table header
		float maxlength = (pageWidth - genericPadding * 2);
		float _lineX = _line_x;
		for (int i = 0; i < contents[0][1].length; i++) {
			float slot = ((maxlength / contents[0][1].length) * i)
					+ ((0 < i) ? genericPadding * 2 : 0);
			writeLine(pdPageContentStream, rtl, pageWidth, _lineX + slot, _line_y,
					new String[] { contents[0][1][i] }, fontSizeSmall,
					font, fontColor);
			
		}
		
		_line_y = _line_y - messageInterline;
		
		// passengers
		for (int k = 1; k < contents[index].length; k++) {
			
			_line_y = _line_y - genericInterline;
			
			_lineX = _line_x;
			for (int i = 0; i < contents[index][k].length; i++) {
				float slot = ((maxlength / contents[index][k].length) * i)
						+ ((0 < i) ? genericPadding * 2 : 0);
				writeLine(pdPageContentStream, rtl, pageWidth, _lineX + slot, _line_y,
						new String[] { contents[index][k][i] }, fontSizeSmall,
						fontBold, fontColor);
			}
		}
		
		// separator
		_line_y = _line_y - genericInterline;
		writeRectangle(pdPageContentStream, _line_x, _line_y,
				(pageWidth - genericPadding * 2), borderWidth, rowColor);
		_line_y = _line_y - genericInterline;
		
		// flight informations header
		float[] previousColumnSize = {
			0.5f, 0.5f, 1.5f, 1.5f, 1f
		};
		_lineX = _line_x;
		for (int i = 0; i < contents[0][2].length; i++) {
			float slot = ((maxlength / contents[0][2].length))
					* (0 < i ? previousColumnSize[i - 1] : 0);
			_lineX = _lineX + slot;
			writeLine(pdPageContentStream, rtl, pageWidth, _lineX, _line_y,
					new String[] { contents[0][2][i] }, fontSizeSmall, font,
					fontColor);
		}
		
		
		// flight informations data
		int[] previousColumnLenght = {
			2, 0, 2, 2, 0
		};
		for (int i = 0, j = 0; i < contents[index][0].length; i++, j++) {
			int module = j % previousColumnSize.length;
			if (0 == module) {
				_lineX = _line_x;
				_line_y = _line_y - messagesInterline;
			}
			
			float slot = ((maxlength / contents[0][2].length))
					* (0 < module ? previousColumnSize[module - 1] : 0);
			_lineX = _lineX + slot;
			writeLine(pdPageContentStream, rtl, pageWidth, _lineX, _line_y, new String[] {
					contents[index][0][i] }, tableFontSize, fontBold,
					fontColor);
			
			float space = font.getStringWidth(contents[index][0][i]) / 1000 * tableFontSize;
			for (int k = 1; k <= previousColumnLenght[module]; k++) {
				writeLine(pdPageContentStream, rtl, pageWidth, _lineX + space + smallPadding,
						_line_y - smallPadding + messageInterline * 4 *
							(previousColumnLenght[module] - k),
						new String[] { contents[index][0][i + k] },
						fontSizeMicro, font, fontColor);
			}
			
			i += previousColumnLenght[module];
			
		}
		
		_line_y = _line_y - messagesInterline;
		
	}
	
	/**
	 * Generate Carnet PDF
	 * @param output
	 * @param fontFamily
	 * @param fontFamilyBold
	 * @param headerLogo1
	 * @param headerLogo2
	 * @param headerLogo3
	 * @throws Exception
	 */
	public final static void generateCarnetPDF(OutputStream output, boolean rtl, 
			InputStream fontFamily, InputStream fontFamilyBold,
			InputStream headerLogo1, InputStream headerLogo2,
			InputStream headerLogo3, String[][][] contents)
					throws Exception {
		logger.debug("[PDFCreator] generateCarnetPDF");
		
		// create a new PDDocument file
		PDDocument pdDocument = new PDDocument();
		
		// attach font to document
		PDFont font = PDType0Font.load(pdDocument, fontFamily);
		PDFont fontB = PDType0Font.load(pdDocument, fontFamilyBold);
		
		// write data into PDF
		writeCarnetPDFPage(pdDocument, rtl, font, fontB, headerLogo1, headerLogo2,
				headerLogo3, contents);
		
		// save document to output and close it!
		pdDocument.save(output);
		pdDocument.close();

	}
	
	/*
	 * writeTicketPDFPage
	 */
	private final static void writeCarnetPDFPage(PDDocument pdDocument,
			boolean rtl, PDFont font, PDFont fontBold, InputStream headerLogo1,
			InputStream headerLogo2, InputStream headerLogo3,
			String[][][] contents) throws Exception {
		logger.debug("[PDFCreator] writeCarnetPDFPage");
		
		// create a new A4 page and add it to document
		PDPage pdPage = new PDPage();
		pdPage.setMediaBox(PDRectangle.A4);
		pdDocument.addPage(pdPage);

		// create a new page content stream
		PDPageContentStream pdPageContentStream = 
				new PDPageContentStream(pdDocument, pdPage, false, false);
		
		_line_x = genericPadding;
		_line_y = pdPage.getMediaBox().getHeight() - genericPadding;
		float pageWidth = pdPage.getMediaBox().getWidth();
		
		// add header
		createPDFCheckinHeader(pdDocument, pdPage, pdPageContentStream, rtl,
				headerLogo1, headerLogo2, headerLogo3);
		
		// add title & heading
		if (null != contents[0]) {
			if (null != contents[0][0]) {
				
				_line_y = _line_y - genericInterline;
				
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
						contents[0][0][0] }, headerFontSize, fontBold,
						fontColor);
				
				_line_y = _line_y - genericInterline;
				
				for (int i = 1; i < contents[0][0].length; i++) {
					_line_y = _line_y - genericInterline;
					
					writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
							new String[] { contents[0][0][i] }, fontSize, font,
							fontColor);
				}
			}
			
			// carnet data info
			if (null != contents[0][1]) {
				_line_y = _line_y - messagesInterline - genericInterline * 2;
				
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
						contents[0][1][0] }, fontTicketSize, font,
						fontColor);
				
				float space =
						font.getStringWidth(contents[0][1][0]) / 1000
							* fontTicketSize + smallPadding;
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x + space, _line_y,
						new String[] { contents[0][1][1] }, fontTicketSize,
						fontBold, fontAlternateColor);
				
				_line_y = _line_y - genericInterline;
				
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y, new String[] {
						contents[0][1][2] }, headingFontSize, font,
						fontColor);
				
				space = font.getStringWidth(contents[0][1][2]) / 1000
							* headingFontSize + smallPadding;
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x + space, _line_y,
						new String[] { contents[0][1][3] }, headingFontSize,
						fontBold, fontColor);
				
			}
		}
		
		// add recap data
		if (null != contents[1]) {
			if (null != contents[1][0] && null != contents[1][1]) {
				
				_line_y = _line_y - messagesInterline - genericInterline;
				
				// table heading
				float maxlength = (pageWidth - (genericPadding * 2))
						/ contents[1][0].length;
				for (int i = 0; i < contents[1][0].length; i++) {
					float lineX = _line_x + maxlength * i; 
					writeLine(pdPageContentStream, rtl, pageWidth, lineX, _line_y,
							new String[] { contents[1][0][i] }, fontSize,
							font, fontColor);
				}
				
				_line_y = _line_y - genericInterline;
				
				// table content
				maxlength = (pageWidth - (genericPadding * 2))
						/ contents[1][1].length;
				for (int i = 0; i < contents[1][1].length; i++) {
					float lineX = _line_x + maxlength * i; 
					writeLine(pdPageContentStream, rtl, pageWidth, lineX, _line_y,
							new String[] { contents[1][1][i] }, fontSize,
							fontBold, fontColor);
				}
				
				_line_y = _line_y - messagesInterline - genericInterline;
				
				// table heading
				maxlength = (pageWidth - (genericPadding * 2))
						/ contents[1][2].length;
				for (int i = 0; i < contents[1][2].length; i++) {
					float lineX = _line_x + maxlength * i; 
					writeLine(pdPageContentStream, rtl, pageWidth, lineX, _line_y,
							new String[] { contents[1][2][i] }, fontSize,
							font, fontColor);
				}
				
				_line_y = _line_y - genericInterline;
				
				// table content
				maxlength = (pageWidth - (genericPadding * 2))
						/ contents[1][1].length;
				for (int i = 0; i < contents[1][3].length; i++) {
					float lineX = _line_x + maxlength * i; 
					writeLine(pdPageContentStream, rtl, pageWidth, lineX, _line_y,
							new String[] { contents[1][3][i] }, fontSize,
							fontBold, fontColor);
				}
				
			}
			
		}
		
		// recap carnet
		if (null != contents[2]) {
			
			_line_y = _line_y - genericPadding * 2;
			
			float maxlength = (pageWidth - (genericPadding * 2)) / 2;
			for (int i = 0; i < contents[2].length; i++) {
				
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
						new String[] { contents[2][i][0] }, headingFontSize,
						fontBold, fontColor);
				
				writeLine(pdPageContentStream, rtl, pageWidth, _line_x + maxlength, _line_y,
						new String[] { contents[2][i][1] }, headingFontSize,
						fontBold, fontColor);
				
				_line_y = _line_y - genericInterline;

			}
			
		}
		
		// recap buy
		if (null != contents[3]) {
			
			_line_y = _line_y - genericPadding * 2;
			
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
					new String[] { contents[3][0][0] }, headingFontSize,
					font, fontColor);
			
			_line_y = _line_y - genericInterline - messageInterline * 2;
			
			float maxlength = (pageWidth - (genericPadding * 2)) / 2;
			
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
					new String[] { contents[3][1][0] }, fontSize,
					font, fontColor);
			
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x + maxlength, _line_y,
					new String[] { contents[3][1][1] }, fontSize,
					font, fontColor);
			
			_line_y = _line_y - genericInterline;
			
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
					new String[] { contents[3][1][2] }, fontSize,
					font, fontColor);
			
			_line_y = _line_y - genericInterline;
			
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
					new String[] { contents[3][1][3] }, fontSize,
					font, fontColor);
			
			_line_y = _line_y - messagesInterline;
			
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x, _line_y,
					new String[] { contents[0][1][2] }, headingFontSize,
					font, fontColor);
			
			writeLine(pdPageContentStream, rtl, pageWidth, _line_x + maxlength, _line_y,
					new String[] { contents[0][1][3] }, headingFontSize,
					font, fontColor);
			
		}

		// close content stream
		pdPageContentStream.close();

	}
	
	/*
	 * Translate an HEX code to RGB color
	 */
	private static Color hex2Rgb(String hexCode) {
		logger.debug("[PDFCreator] hex2Rgb");
		
		return new Color(
				Integer.valueOf(hexCode.substring(1, 3), 16),
				Integer.valueOf(hexCode.substring(3, 5), 16),
				Integer.valueOf(hexCode.substring(5, 7), 16));
	}
	
	/*
	 * Write a line into PDF 
	 */
	private static void writeLine(PDPageContentStream pdPageContentStream, 
			boolean rtl, float pageWidth, float x, float y, 
			String[] t, float size, PDFont pdFont,
			String color) throws Exception {
		logger.debug("[PDFCreator] writeLine");



		if (t[0].equals("ECONOMY LIGHT")){
			String[] a = new String[3];
			a[0] = "ECONOMY";
			a[1] = "LIGHT";
			a[2] = "FARE";

			for (int k = 0; k < a.length; k++) {
				if (null != a[k] && !"".equals(a[k])) {
					pdPageContentStream.beginText();
					pdPageContentStream.setFont(pdFont, size);
					pdPageContentStream.setNonStrokingColor(hex2Rgb(color));
					if (rtl) {
						float textWidth = (pdFont.getStringWidth(a[k]) / 1000) * size;
						pdPageContentStream.newLineAtOffset(
								pageWidth - x - textWidth, y);
					} else {
						pdPageContentStream.newLineAtOffset(x, y);
					}
					try {
						pdPageContentStream.showText(a[k]);
					} catch (IllegalArgumentException e) {
						logger.warn("Unable to write string {}: {}", a[k], e.getMessage());
					}
					pdPageContentStream.endText();
					y = y - 7;
				}
				elfSpace = 5 * k;
			}
		} else {
			for (int k = 0; k < t.length; k++) {
				if (null != t[k] && !"".equals(t[k])) {
					pdPageContentStream.beginText();
					pdPageContentStream.setFont(pdFont, size);
					pdPageContentStream.setNonStrokingColor(hex2Rgb(color));
					if (rtl) {
						float textWidth = (pdFont.getStringWidth(t[k]) / 1000) * size;
						pdPageContentStream.newLineAtOffset(
								pageWidth - x - textWidth, y);
					} else {
						pdPageContentStream.newLineAtOffset(x, y);
					}
					try {
						pdPageContentStream.showText(t[k]);
					} catch (IllegalArgumentException e) {
						logger.warn("Unable to write string {}: {}", t[k], e.getMessage());
					}
					pdPageContentStream.endText();
					y = y - genericInterline;
				}
			}
		}
	}
	
	/*
	 * Write a rectangle into PDF
	 */
	private static void writeRectangle(PDPageContentStream pdPageContentStream,
			float x, float y, float width, float height, String color)
					throws Exception {
		pdPageContentStream.setNonStrokingColor(hex2Rgb(color));
		pdPageContentStream.addRect(x, y, width, height);
		pdPageContentStream.fill();
	}
	
	/*
	 * Split string
	 */
	private static String[] splitString(String text, float maxWidth,
			PDFont font, float fontSize) throws Exception {
		
		float length = font.getStringWidth(text) / 1000 * fontSize;
		int linebreaks = (int) Math.floor(length / maxWidth);
		
		String[] newText = new String[linebreaks + 1];
		String[] parts = text.split(" ");
		
		float totalTextLength = 0;
		int k = 0;
		
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < parts.length; i++) {
			
			totalTextLength = totalTextLength + 
					(font.getStringWidth(parts[i] + " ") / 1000 * fontSize);
			if (totalTextLength > maxWidth || i == parts.length - 1) {
				newText[k] = stringBuffer.toString();
				stringBuffer = new StringBuffer();
				k++;
				totalTextLength = 0;
			}

			stringBuffer.append(parts[i]);
			stringBuffer.append(" ");

		}
		newText[(k-1)] = newText[(k-1)].trim() + " " + stringBuffer.toString();
		
		String[] nText = removeElements(newText, null);
		return nText;
		
	}
	
	/*
	 * Remove elements from array
	 */
	private static String[] removeElements(String[] input, String deleteMe) {
	    List<String> result = new LinkedList<String>();
	    for (int i = 0; i < input.length; i++) {
	    	if (input[i] != null && input[i] != deleteMe) {
	            result.add(input[i]);
	    	}
	    }
	    return result.toArray(new String[result.size()]);
	}
}