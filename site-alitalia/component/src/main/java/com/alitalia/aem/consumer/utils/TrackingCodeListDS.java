package com.alitalia.aem.consumer.utils;

import java.util.HashMap;
import java.util.Map;

public class TrackingCodeListDS {
	private static TrackingCodeListDS instance;
	
	private Map<Integer,String> table;
	
	private TrackingCodeListDS(){
		initTable();
	}
	
	private void initTable() {
		table= new HashMap<Integer,String>();
		table.put(0,"INTERNET");
		table.put(1,"BVITAMIN");
		table.put(2,"MLAZ");
		table.put(3,"DBANKIT");
		table.put(4,"DBANKIN");
		table.put(5,"RIVULI");
		table.put(6,"RIVULO");
		table.put(7,"CCPMO");
		table.put(8,"WISCRAXB");
		table.put(9,"WISCAXBC");
		table.put(10,"WPROMMM");
		table.put(11,"WTIMCFA");
		table.put(12,"WTIMCFA");
		table.put(13,"WSAXUS");
		table.put(14,"WACCORB");
		table.put(15,"WACWT");
		table.put(16,"WVODAFON");
		table.put(17,"WACISAL");
		table.put(18,"WAUVAX");
		table.put(19,"WAUVNT");
		table.put(20,"WALCC");
		table.put(21,"WAHRGR");
		table.put(22,"WAWTGFR");
		table.put(23,"WASIECO");
		table.put(24,"WAVENT");
		table.put(25,"WAITN");
		table.put(26,"WAWTGPR");
		table.put(27,"WABVIAG");
		table.put(28,"WABTI");
		table.put(29,"WAPOLO");
		table.put(30,"WAUNIGL");
		table.put(31,"WAROBIN");
		table.put(32,"WAG40");
		table.put(33,"WAMARS");
		table.put(34,"WATRAV");
		table.put(35,"WAVISET");
		table.put(36,"GOOGLE");
		table.put(37,"YAHOO");
	}
	

	public String getTrackingCodeForService(Integer trackingCode){
		return table.get(trackingCode)!=null ? table.get(trackingCode) :table.get(0);
	}
	
	public static TrackingCodeListDS getInstance(){
		if(instance == null)
			instance = new TrackingCodeListDS();
		return instance;
	}

}
