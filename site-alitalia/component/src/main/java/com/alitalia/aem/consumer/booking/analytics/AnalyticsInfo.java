package com.alitalia.aem.consumer.booking.analytics;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

public class AnalyticsInfo {
	public int step;
	public boolean isAward;
	
	/*Step 1*/
	public String cabinData;
	
	/*Step 2*/
	public List<FlightSegmentInfo> flightSegments;
	
	/*Step 3*/
	public List<ConsumerInfo> consumers;
	
	/*Step 4*/
	public List<AncillaryInfo> ancillaries;
	
	/*Step 5*/
	public BigDecimal netRevenue;
	public String PNR;
	public Calendar transactionDate;
	public String paymentType="";
	public String CCType="";
	public String tktNumber="";
	public Integer tktPerPNR;
	public boolean invoiceRequired;
	public BigDecimal discount;
	public String userType;
	
	public List<ItemsInfo> items;
	
	
	/*Fanplayr Step 1*/
	public String travelType;
	public String valuta;
	public Integer numAdults;
	public Integer numYoung;
	public Integer numChildren;
	public Integer numInfant;
	
	public String Boapt;
	public String Bocity;
	public String Bocountry;
	public Calendar depDate;
	
	public String Arapt;
	public String Arcity;
	public String Arcountry;
	public Calendar retDate;
	
	public String Network;
	public String NetworkExt;
	public Integer deltaBoToday;
	public Integer deltaBoAr;
	public Integer DayOfWeekA;
	public Integer DayOfWeekR;
	
	/*Fanplayr Step 2*/
	public Integer depHours;
	public Integer depMinutes;
	public BigDecimal depcost;
	public String depFlightNumber="";
	public String depBrand="";
	public String depFareBasis="";
	
	
	public Integer retHours;
	public Integer retMinutes;
	public BigDecimal retcost;
	public String retFlightNumber="";
	public String retBrand="";
	public String retFareBasis="";
	
	public String eCoupon="NO";
	public BigDecimal totalPrice;
	public BigDecimal fare;
	public BigDecimal taxes;
	public BigDecimal surcharges;
	
	
	/*Fanplayr Step 3*/
	public String email="";
	public String telefono="";

	/*Carnet Step 1*/

	public Integer carnetUseStep;
	public String carnetTratta;
	public String carnetItinType;

}
