package com.alitalia.aem.consumer.checkinrest.controller;

import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPassInfo;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPassSms;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetBoardingPassInfoRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetBoardingPassInfoResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSmsDataResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.PDFManager;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.SlingRequestProcessor;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinPdfSms extends GenericCheckinModel {

    @Inject
    private SlingHttpServletRequest request;

    @Inject
    private SlingHttpServletResponse response;

    @Inject
    private CheckinSession checkinSession;

    @Inject
    private volatile ICheckinDelegate checkInDelegateRest;

    @Inject
    private PDFManager pdfManager;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostConstruct
    protected void initModel() throws Exception {
        try {
            CheckinGetSmsDataResponse responseGetSmsData = checkinSession.getMoreSmsData(request);
            BoardingPassSms boradingPassSms = responseGetSmsData.getBoardingPassData();

            List<String> seats = boradingPassSms.getSeats();

            BoardingPassInfo boardingPassInfoTot = null;

            for(int i = 0;i < seats.size();i++)
            {
                CheckinGetBoardingPassInfoRequest serviceRequest = new CheckinGetBoardingPassInfoRequest(IDFactory.getTid(), IDFactory.getSid(request));
                serviceRequest.setAirline(boradingPassSms.getAirline());
                serviceRequest.setConversationID(IDFactory.getTid());
                serviceRequest.setDepartureDate(boradingPassSms.getDepartureDate());
                serviceRequest.setMarket(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                serviceRequest.setLanguage(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                serviceRequest.setFlight(boradingPassSms.getFlight());
                serviceRequest.setOrigin(boradingPassSms.getOrigin());
                serviceRequest.setDestination(boradingPassSms.getDestination());
                serviceRequest.setPnr(boradingPassSms.getPnr());
                List<String> seatsTemp = new ArrayList<String>();
                seatsTemp.add(seats.get(i));
                serviceRequest.setSeats(seatsTemp);
                CheckinGetBoardingPassInfoResponse serviceResponce = checkInDelegateRest.getBoardingPassInfo(serviceRequest);
                BoardingPassInfo boardingPassInfo = serviceResponce.getBoardingPassInfo();

                if (boardingPassInfoTot == null) {
                    boardingPassInfoTot = boardingPassInfo;
                } else {
                    for (com.alitalia.aem.common.data.checkinrest.model.boardingpass.Model model : boardingPassInfo.getModels()) {
                        boardingPassInfoTot.addModel(model);
                    }
                }
            }

            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition","attachment; filename=boardingpass.pdf");
            response.setContentType("application/pdf");

            pdfManager.downloadPdfSms(request, response, true,boardingPassInfoTot);
        }
        catch (Exception e) {
            logger.error("[CheckinPdfSms][initModel] Error.");
        }
    }
}
