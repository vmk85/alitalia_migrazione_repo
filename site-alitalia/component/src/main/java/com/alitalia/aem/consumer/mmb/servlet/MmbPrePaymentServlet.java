package com.alitalia.aem.consumer.mmb.servlet;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "mmbprepaymentconsumer" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })})
@SuppressWarnings("serial")
public class MmbPrePaymentServlet extends GenericMmbFormValidatorServlet {

	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile MmbSession mmbSession;	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

		MmbSessionContext ctx = getMmbSessionContext(request);
		
		String redirect = null;
		
		boolean paymentStepRequired = mmbSession.performPrePayment(ctx);
		if (paymentStepRequired) {
			redirect = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
					+ getConfiguration().getMmbPaymentPage();
		} else {
			redirect = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
					+ getConfiguration().getMmbThankyouPage();
		}
		redirect = request.getResourceResolver().map(redirect);
			
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value("OK");
		json.key("redirect").value(redirect);
		json.key("paymentStepRequired").value(paymentStepRequired);
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected String getUnrecoverableErrorRedirect(
			SlingHttpServletRequest request, SlingHttpServletResponse response,
			GenericFormValidatorServletException exception) {
		return AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getMmbThankyouPage();
	}
	
}
