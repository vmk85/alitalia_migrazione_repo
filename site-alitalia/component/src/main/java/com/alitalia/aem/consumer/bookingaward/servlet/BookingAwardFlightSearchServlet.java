package com.alitalia.aem.consumer.bookingaward.servlet;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.bookingaward.BookingAwardSession;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingawardflightsearch" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class BookingAwardFlightSearchServlet extends GenericBookingAwardFormValidatorServlet {
	
	@Reference
    private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private BookingAwardSession bookingAwardSession;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		// no input fields, always valid
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws Exception {
		
		BookingSessionContext ctx = getBookingSessionContext(request);
		
		String brandsPath = AlitaliaUtils.findSiteBaseRepositoryPath(request.getResource()) + AlitaliaConstants.BASE_BRAND_AWARD_PATH;
		Resource brandRoot = request.getResource().getResourceResolver().getResource(brandsPath);
		
		bookingAwardSession.performFlightSearch(ctx, brandRoot);
		
		if (ctx.availableFlights == null || ctx.availableFlights.getRoutes() == null) {
			throw new IllegalStateException("ctx.availableFlights.getRoutes() is null");
		}
		
		String noSolutionsPage = request.getResource().getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ configuration.getBookingAwardFlightSearchNoSolutionsPage());
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		
		if (ctx.availableFlights.getRoutes().isEmpty()) {
			logger.info("availableFlights.getRoutes() is empty.");
			json.key("solutionNotFound").value("allSolutions");
			json.key("redirect").value(noSolutionsPage);
		}
		
		json.key("result").value("OK");
		json.endObject();
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}
