package com.alitalia.aem.consumer.model.content;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables = { Resource.class })
public class HeadBaseModel {

	@Self
	private Resource resource;

	private Logger logger = LoggerFactory.getLogger(HeadBaseModel.class);
	private String internalUrl;
	private String internalUrl1;
	private Boolean jsonProtocol;
	private String dateFormat;
	private String recaptchaSiteKey;
	private String invisibleRecaptchaSiteKey;
	
	@Inject
	private AlitaliaConfigurationHolder alitaliaConfigurationHolder;

	@PostConstruct
	protected void initModel() {
		logger.debug("[HeadBaseModel] initModel");
		
		internalUrl = resource.getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(resource, true) +
				alitaliaConfigurationHolder.getHomePage());
		internalUrl1 = resource.getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(resource, true) +
				alitaliaConfigurationHolder.getHomePage1());
		jsonProtocol = alitaliaConfigurationHolder.getHttpsEnabled();
		
		//Utilizzato per definire il formato data
		dateFormat = "%d/%m/%Y";
		String country = AlitaliaUtils.getRepositoryPathMarket(resource);
		if (country.equalsIgnoreCase("us") || country.equalsIgnoreCase("ca")) {
			dateFormat = "%m/%d/%Y";
		}
		recaptchaSiteKey = alitaliaConfigurationHolder.getRecaptchaSiteKey();
		invisibleRecaptchaSiteKey = alitaliaConfigurationHolder.getInvisibleRecaptchaSiteKey();
	}

	public String getInternalUrl() {
		return internalUrl;
	}

	public String getInternalUrl1() {
		return internalUrl1;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public Boolean getJsonProtocol() {
		return jsonProtocol;
	}
	
	public String getRecaptchaSiteKey() {
		return recaptchaSiteKey;
	}
	
	public String getInvisibleRecaptchaSiteKey() {
		return invisibleRecaptchaSiteKey;
	}
	
}