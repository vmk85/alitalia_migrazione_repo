package com.alitalia.aem.consumer.checkin.render;

import java.util.List;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;

public class NewPassengerInfo {
	private String pnr;
	private String eTicket;
	private String lastname;
	private String name;
	private List<CheckinPassengerData> passengersList;
	private List<CheckinRouteData> routesList;
	
	public String getPnr() {
		return pnr;
	}
	
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	
	public String geteTicket() {
		return eTicket;
	}
	
	public void seteTicket(String eTicket) {
		this.eTicket = eTicket;
	}
	
	public String getLastname() {
		return lastname;
	}
	
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<CheckinPassengerData> getPassengersList() {
		return passengersList;
	}
	
	public void setPassengersList(List<CheckinPassengerData> passengersList) {
		this.passengersList = passengersList;
	}
	
	public List<CheckinRouteData> getRoutesList() {
		return routesList;
	}
	
	public void setRoutesList(List<CheckinRouteData> routesList) {
		this.routesList = routesList;
	}
}
