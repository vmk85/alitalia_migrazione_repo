package com.alitalia.aem.consumer.booking.model;


public class AncillaryCodeAndPath {
	
	private String ancillaryCode;
	private String ancillaryPath;
	private String ancillaryCodePath;
	

	public AncillaryCodeAndPath( ) {
    }
	
	public AncillaryCodeAndPath(String code , String path , String codePath ) {
        this.ancillaryCode = code;
        this.ancillaryPath = path;
        this.ancillaryCodePath = codePath;
    }
	
	public String getAncillaryCode() {
		return ancillaryCode;
	}	

	public void setAncillaryCode(String name) {
		this.ancillaryCode = name;
	}
	
	public String getAncillaryPath() {
		return ancillaryPath;
	}	

	public void setAncillaryPath(String name) {
		this.ancillaryPath = name;
	}
	

	public String getAncillaryCodePath() {
		return ancillaryCodePath;
	}	

	public void setAncillaryCodePath(String name) {
		this.ancillaryCodePath = name;
	}



}
