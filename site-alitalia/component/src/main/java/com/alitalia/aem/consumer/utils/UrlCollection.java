package com.alitalia.aem.consumer.utils;

import java.util.ArrayList;

public class UrlCollection {

	private ArrayList<UrlObject> urls;

	public UrlCollection() {

		urls = new ArrayList<UrlObject>();

	}

	public void setUrls(ArrayList<UrlObject> urls) {
		this.urls = urls;
	}

	public void addUrl(UrlObject url) {
		urls.add(url);
	}

	public String toJson() {
		String jsonString = "{";

		jsonString += getUrlsJson();

		jsonString += "}";

		return jsonString;
	}

	private String getUrlsJson() {
		String jsonString = "";

		for (UrlObject url : urls) {
			jsonString += url.toString() + ",";
		}

		if (jsonString.length() > 0) {
			jsonString = jsonString.substring(0, jsonString.length() - 1);
		}
		return jsonString;
	}

}
