package com.alitalia.aem.consumer.model.content.substriptionsme;

public class Preferences {
	private String accepted;
	private String promotionalCode;
	private String alitaliaOffers;
	private String language;
	private String captchaString;
	
	public String getAccepted() {
		return accepted;
	}
	public void setAccepted(String accepted) {
		this.accepted = accepted;
	}
	public String getPromotionalCode() {
		return promotionalCode;
	}
	public void setPromotionalCode(String promotionalCode) {
		this.promotionalCode = promotionalCode;
	}
	public String getAlitaliaOffers() {
		return alitaliaOffers;
	}
	public void setAlitaliaOffers(String alitaliaOffers) {
		this.alitaliaOffers = alitaliaOffers;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getCaptchaString() {
		return captchaString;
	}
	public void setCaptchaString(String captchaString) {
		this.captchaString = captchaString;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accepted == null) ? 0 : accepted.hashCode());
		result = prime * result + ((alitaliaOffers == null) ? 0 : alitaliaOffers.hashCode());
		result = prime * result + ((captchaString == null) ? 0 : captchaString.hashCode());
		result = prime * result + ((language == null) ? 0 : language.hashCode());
		result = prime * result + ((promotionalCode == null) ? 0 : promotionalCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Preferences other = (Preferences) obj;
		if (accepted == null) {
			if (other.accepted != null)
				return false;
		} else if (!accepted.equals(other.accepted))
			return false;
		if (alitaliaOffers == null) {
			if (other.alitaliaOffers != null)
				return false;
		} else if (!alitaliaOffers.equals(other.alitaliaOffers))
			return false;
		if (captchaString == null) {
			if (other.captchaString != null)
				return false;
		} else if (!captchaString.equals(other.captchaString))
			return false;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (promotionalCode == null) {
			if (other.promotionalCode != null)
				return false;
		} else if (!promotionalCode.equals(other.promotionalCode))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Preferences [accepted=" + accepted + ", promotionalCode=" + promotionalCode + ", alitaliaOffers="
				+ alitaliaOffers + ", language=" + language + ", captchaString=" + captchaString + "]";
	}
	
}
