package com.alitalia.aem.consumer.booking.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.FlightTabData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.TabData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingFlightFilterTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSearchCategoryEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.render.AvailableFlightBrandSelectionRender;
import com.alitalia.aem.consumer.booking.render.AvailableFlightsSelectionRender;
import com.alitalia.aem.consumer.booking.render.BrandRender;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.booking.render.GenericPriceRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.FlightDataComparator;
import com.alitalia.aem.consumer.utils.ValidationUtils;
import com.day.cq.i18n.I18n;

@Model(adaptables={ SlingHttpServletRequest.class })
public class BookingScegliVoloSelection extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private BookingSession bookingSession;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private List<AvailableFlightsSelectionRender> availableFlightSelections;
	private List<BrandRender> brandHeaderList;
	private int routeIndex;
	private boolean solutionForOtherDates;
	private boolean showMoreFlights;
	private int brandEconomySize;
	private int brandBusinessSize;
	private HashMap<String, ArrayList<BrandRender>> missedBrandsMap;

	private BookingFlightFilterTypeEnum orderType;
	private boolean directFlightOnly;
	private boolean showCheckBoxDirectFlightOnly;
	private boolean isFcoLin;
	private String popupCategory;
	private String cssClass;
	
	private DateRender date;
	private boolean isFilterBusiness;
	private boolean showFarePrice;
	private boolean showBrandInfoPopupIcon;
	private String baseUrl;
	private boolean isBusiness;
	private boolean isRTL;

	private ArrayList<BrandRender> missedBrandsDxMap;
	
	private String brandNameForBusinessTrigger;

	private String recapFlightSliceText;
	
	private static final int START_DAY_HH= 0;
	private static final int START_DAY_MM= 0;
	private static final int START_AFTERNOON_HH= 11;
	private static final int START_AFTERNOON_MM= 0;
	private static final int START_NIGHT_HH= 17;
	private static final int START_NIGHT_MM= 0;
	
	@PostConstruct
	protected void initModel() {
		
		try {
			
			initBaseModel(request);
			
			String[] selectors = request.getRequestPathInfo().getSelectors();
			
			if (selectors.length < 2 || !(selectors[1].equals("0") || selectors[1].equals("1"))) {
				logger.error("[BookingScegliVoloSelection] Direction of the route not found");
				throw new RuntimeException("direction of the route not found");
			}
			routeIndex = Integer.parseInt(selectors[1]);
			
			isBusiness = Boolean.parseBoolean(request.getParameter("isBusiness"));
			
			boolean moreFlights = Boolean.parseBoolean(request.getParameter("moreFlights"));
			orderType = BookingFlightFilterTypeEnum.fromValue(request.getParameter("orderType"));
			directFlightOnly = Boolean.parseBoolean(request.getParameter("directFlightOnly"));
			
			if (routeIndex != 0 && routeIndex != 1) {
				logger.error("[BookingScegliVoloSelection] Direction of the route not found");
				throw new RuntimeException("direction of the route not found");
			}
			
			if (!(ctx.searchKind != BookingSearchKindEnum.ROUNDTRIP && routeIndex == 1)) {
				
				// Per evitare di definire due contenuti (uno per category con PROMO uno senza)
				// utilizziamo lo stesso contenuto popup sia con o senza brand Economy Promo
				// Inoltre per evitare di rinominare i contenuti attualmente esistenti,
				// aggiungiamo il suffisso "Minifare" per DOM e INTZ
				if (ctx.searchCategory == null) {
					popupCategory = "";
				}else if (ctx.searchCategory == BookingSearchCategoryEnum.DOM || 
						ctx.searchCategory == BookingSearchCategoryEnum.DOM_PROMO) {
					popupCategory = BookingSearchCategoryEnum.DOM.getValue() + "Minifare";
					
				} else if (ctx.searchCategory == BookingSearchCategoryEnum.INTZ || 
						ctx.searchCategory == BookingSearchCategoryEnum.INTZ_PROMO
						) {
					popupCategory = BookingSearchCategoryEnum.INTZ.getValue() + "Minifare";
					
				} else if (ctx.searchCategory == BookingSearchCategoryEnum.INTZ_SAVER) {
					
					popupCategory = BookingSearchCategoryEnum.INTZ_SAVER.getValue() + "Minifare";
					
				} else if (ctx.searchCategory == BookingSearchCategoryEnum.INTC ||
						ctx.searchCategory == BookingSearchCategoryEnum.INTC_PROMO) {
                    popupCategory = BookingSearchCategoryEnum.INTC.getValue();
                } else if (ctx.searchCategory == BookingSearchCategoryEnum.INTC_LIGHT){
                    popupCategory = BookingSearchCategoryEnum.INTC_LIGHT.getValue();
				} else if(ctx.searchCategory == BookingSearchCategoryEnum.INTZ_PREMIUM){
					popupCategory = BookingSearchCategoryEnum.INTC.getValue();
				} else {
					popupCategory = ctx.searchCategory.getValue();
				}
				String[] expectedBrands = {};
				if(ctx.searchCategory != null){
					expectedBrands = configuration.getSearchCategoryBrands(ctx.searchCategory);
				}
				brandEconomySize = bookingSession.computeBrandEconomySize(ctx.searchCategory);
				brandBusinessSize = expectedBrands.length - brandEconomySize;
				
				String[] cssNumbers = {"", "one", "two", "three", "four","five"};
				if (brandEconomySize > 0) {
					if (brandEconomySize == 1 && brandBusinessSize == 0) {
						cssClass = "forOneCol";
					} else if (brandEconomySize == 2 && brandBusinessSize == 2) {
						cssClass = "forTwoCol";
					} else if (brandEconomySize == 3 && brandBusinessSize == 0) {
						cssClass = "forThreeCol";
					} else {
						cssClass = cssNumbers[brandEconomySize] + "Col" + 
								(brandBusinessSize > 0 ? (cssNumbers[brandBusinessSize].substring(0, 1).toUpperCase() + cssNumbers[brandBusinessSize].substring(1) + "Col") : "");
					}
				}
				
				if (brandEconomySize > 0 && brandBusinessSize > 0) {
					brandNameForBusinessTrigger = expectedBrands[brandEconomySize - 1];
				}
			
				boolean solutionsFound = ctx.availableFlights != null && ctx.availableFlights.getRoutes() != null &&
						!ctx.availableFlights.getRoutes().isEmpty();
				
				solutionForOtherDates = ctx.searchKind != BookingSearchKindEnum.MULTILEG && bookingSession.existFlightsRibbon(ctx) && !ctx.solutionForSelectedDate[routeIndex];
				
				//populate the list rows with each row of the choosing selection flight matrix
				if (solutionsFound) {
					availableFlightSelections = prepareAvailableFlightSelections(ctx, i18n, moreFlights, orderType, directFlightOnly);
					if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
						addExtraChargeToFare(ctx.totalExtraCharges, ctx);
					} else {
						if (routeIndex == ctx.totalSlices-1) {
							addExtraChargeToFare(ctx.totalExtraCharges, ctx);
						}
					}
					setBrandHeader(ctx, expectedBrands, routeIndex);
					
					/*Se volo FCO-LIN o viceversa deve popolare filtri Mattina-Pomeriggio-Sera*/
					isFcoLin = ctx.searchCategory == BookingSearchCategoryEnum.FCO_LIN;
					
					if (ctx.searchKind != BookingSearchKindEnum.MULTILEG
							&& !ctx.isCarnetProcess) {
						date = computeCurrentSelectedDate(ctx);
					}
					
					isFilterBusiness = ctx.filterCabin == CabinEnum.BUSINESS;
					showFarePrice = !ctx.isCarnetProcess;
					baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
				}
			}
			
			showBrandInfoPopupIcon = false;
			if (ctx.searchCategory != null) {
				showBrandInfoPopupIcon = Arrays.asList(configuration.getSearchCategoryPopupInfoAvailable())
						.contains(ctx.searchCategory.getValue());
			}
			
			/*Computing Slice Text*/
			if(BookingSearchKindEnum.MULTILEG.equals(ctx.searchKind)){
				String label;
				switch(ctx.currentSliceIndex){
				case 1:
					label = BookingConstants.FLIGHT_SELECT_FIRST_SLICE_TEXT;
					break;
				case 2:
					label = BookingConstants.FLIGHT_SELECT_SECOND_SLICE_TEXT;
					break;
				case 3:
					label = BookingConstants.FLIGHT_SELECT_THIRD_SLICE_TEXT;
					break;
				case 4:
					label = BookingConstants.FLIGHT_SELECT_FOURTH_SLICE_TEXT;
					break;
				default:
					label = "";
				}
				recapFlightSliceText = i18n.get(label);
			} else if (routeIndex == 0) {
				recapFlightSliceText = i18n.get(BookingConstants.FLIGHT_SELECT_OUTBOUND_TEXT);
			} else {
				recapFlightSliceText = i18n.get(BookingConstants.FLIGHT_SELECT_RETURN_TEXT);
			}
			
			isRTL = "true".equals(AlitaliaUtils.getInheritedProperty(request.getResource(), "rtl", String.class));
			
		} catch(Exception e) {
			logger.error("[BookingScegliVoloSelection] Unexpected error", e);
			throw e;
		}
	}

	private DateRender computeCurrentSelectedDate(BookingSessionContext ctx) {
		List<FlightTabData> tabList = null;
		RouteTypeEnum routeType = null;
		if (routeIndex == 0) {
			routeType = RouteTypeEnum.OUTBOUND;
		} else {
			routeType = RouteTypeEnum.RETURN;
		}
		
		for ( TabData tabs : ctx.availableFlights.getTabs()) {
			if (tabs.getType() == routeType) {
				tabList = tabs.getFlightTabs();
			} 
		}
		
		FlightTabData selectedTab = tabList.get(ctx.selectedDepartureDateChoices[routeIndex]);
		return new DateRender(selectedTab.getDate());
	}

	/**
	 * It sum the extra charge price to the flight fare
	 * @param totalExtraCharges
	 * @param ctx
	 */
	private void addExtraChargeToFare(BigDecimal totalExtraCharges, BookingSessionContext ctx) {
		for (AvailableFlightsSelectionRender availableFlightRender : this.availableFlightSelections) {
			for (AvailableFlightBrandSelectionRender availableFlightBrandRender 
					: availableFlightRender.getAvailableFlightBrandSelectionRenderList()) {
				
				GenericPriceRender currentPriceRender = availableFlightBrandRender.getFlightFare();
				BigDecimal newPrice = new BigDecimal(0);
				newPrice = newPrice.add(availableFlightBrandRender.getBrandData().getGrossFare());
				newPrice = newPrice.add(totalExtraCharges);
				GenericPriceRender newPriceRender = new GenericPriceRender(
						newPrice, currentPriceRender.getCurrency(), ctx);
				availableFlightBrandRender.setFlightFare(newPriceRender);
			}
		}
		
	}
	
	private List<AvailableFlightsSelectionRender> prepareAvailableFlightSelections(BookingSessionContext ctx, I18n i18n, boolean moreFlights,
			BookingFlightFilterTypeEnum orderType, boolean directFlightOnly) {
		
		List<AvailableFlightsSelectionRender> availableFlightSelections = new ArrayList<AvailableFlightsSelectionRender>(); 
		
		AvailableFlightsData availableFlights = ctx.availableFlights;
		if(availableFlights==null){
			logger.error("Unexpected error - availableFligths not in session");
			throw new RuntimeException("Unexpected error - availableFligths not in session");
		}
		if(availableFlights.getRoutes()==null){
			logger.error("Unexpected error - Routes not available");
			throw new RuntimeException("Unexpected error - Routes not available");
		}

		RouteData route =  availableFlights.getRoutes().get(routeIndex);
		
		BrandData selectedBrandData = null;
		FlightData selectedFlightData = null;
		if(ctx.flightSelections[routeIndex] != null){
			FlightSelection selectedFlight = ctx.flightSelections[routeIndex];
			selectedFlightData = selectedFlight.getFlightData();
			for(BrandData brandData : selectedFlight.getFlightData().getBrands() ) {
				if ( brandData.getCode().equalsIgnoreCase(
						selectedFlight.getSelectedBrandCode()) ) {
					selectedBrandData = brandData;
					break;
				}
			}
		}
		
		showCheckBoxDirectFlightOnly = !(ValidationUtils.checkAllFlights(route.getFlights(), 
				ValidationUtils.isDirect()) 
				|| ValidationUtils.checkAllFlights(route.getFlights(), 
						ValidationUtils.isConnecting()));
		
		
		List<FlightData> currentFlightToShow = bookingSession.obtainFlightsToShown(ctx, routeIndex, route, moreFlights);
		showMoreFlights = (route.getFlights().size() != currentFlightToShow.size());
		
		Iterator<FlightData> itFlight = currentFlightToShow.iterator();
		int i = 0;
		while(itFlight.hasNext()){
			FlightData flightData = itFlight.next();
			boolean isRefreshedSlice = false;
			if (ctx.isRefreshed) {
				if (ctx.firstSelectionIndex != routeIndex) {
					isRefreshedSlice = true;
				}
			}

			Boolean isSoggettoAppGov=computeIsSoggettoAppGov(obtainFlightNumbers(flightData));
			flightData.setIsSoggettoApprovazioneGovernativa(isSoggettoAppGov);

			availableFlightSelections.add(new AvailableFlightsSelectionRender(flightData, 
					ctx.locale, i18n, isRefreshedSlice, selectedFlightData, selectedBrandData, ctx, i++));
		}
		
		return (filterAndOrderFlights(availableFlightSelections, orderType, directFlightOnly)); 
	}

	private Boolean computeIsSoggettoAppGov(String numeroVolo) {
		String[] arrayVolo =configuration.getListaVoliSoggettiApprovazioneGovernativa().split(";");
		for (int i=0; i<arrayVolo.length; i++){
			if (arrayVolo[i].equals(numeroVolo)) {return true;}
		}
		return false;
	}

	private String obtainFlightNumbers(FlightData flightData) {
		String result = "";
		//FlightData flightData = flightSelection.getFlightData();
		if (flightData instanceof DirectFlightData) {
			DirectFlightData directFlightData = (DirectFlightData) flightData;
			return directFlightData.getCarrier() + directFlightData.getFlightNumber();
		} else {
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
			for (FlightData flight : connectingFlightData.getFlights()) {
				DirectFlightData directFlightData = (DirectFlightData) flight;
				String number = directFlightData.getCarrier() + directFlightData.getFlightNumber();
				if (!result.equals("")) {
					result += ", " + number;
				} else {
					result = number;
				}
			}
		}
		return result;
	}

	/**
	 * 
	 * @param availableFlightsSelectionRender is the first available flight selection and it uses to
	 * build the header list of brands
	 * @return the brands header
	 */
	private void setBrandHeader(BookingSessionContext ctx, String[] expectedBrands, int routeIndex) {
		
		ArrayList<String> missedBrands = new ArrayList<String>();
		ArrayList<String> receivedBrands = new ArrayList<String>();
		for (String s : expectedBrands) {
			missedBrands.add(s); // Arrays.asList() does not provide an implementation of list with remove operation
		}
		
		logger.info("Expected brands = {}", missedBrands);
		
		brandHeaderList = new ArrayList<BrandRender>();
		missedBrandsMap = new HashMap<String, ArrayList<BrandRender>>();
		
		RouteData selectedRoute = null;
		if (ctx.searchKind == BookingSearchKindEnum.MULTILEG){
			selectedRoute = ctx.availableFlights.getRoutes().get(0); //Sempre solo rotta corrente una rotta
		} else {
			for ( RouteData routeData : ctx.availableFlights.getRoutes()) {
				if(routeIndex==0 && routeData.getType() == RouteTypeEnum.OUTBOUND) {
					selectedRoute = routeData;
					break;
				}
				if(routeIndex==1 && routeData.getType() == RouteTypeEnum.RETURN) {
					selectedRoute = routeData;
					break;
				}
			}
		}
		for (BrandData brandData : selectedRoute.getFlights().get(0).getBrands()) {
			
			BrandRender brandRender = new BrandRender(brandData, ctx);
			receivedBrands.add(brandRender.getId());
			missedBrands.remove(brandRender.getId());
				
			this.brandHeaderList.add(brandRender);
		}
		
		logger.info("Received brands = {}; Missed brands = {}", receivedBrands, missedBrands);
		logger.info("Expected brands size = {}", expectedBrands.length);
		
		ArrayList<BrandRender> a = new ArrayList<BrandRender>();
		
		for (String brand : expectedBrands) {
			
			if (missedBrands.contains(brand)) {
				BrandPageData page = ctx.brandMap.get(brand);
				if (page != null) {
					BrandRender brandRender = new BrandRender(page, ctx);
					a.add(brandRender);
				}
			} else if (receivedBrands.contains(brand)) {
				missedBrandsMap.put(brand, a);
				a = new ArrayList<BrandRender>();
			}
		}
		if (!a.isEmpty()) {
			missedBrandsMap.put("-", a);
		}
		ctx.missedBrandsMap = missedBrandsMap;
		
		missedBrandsDxMap = missedBrandsMap.get("-");
	}
	
	private List<AvailableFlightsSelectionRender> filterAndOrderFlights (
			List<AvailableFlightsSelectionRender> flights, BookingFlightFilterTypeEnum filter, boolean directFlightOnly){	
		return flights.stream().filter(filterFlights(directFlightOnly, filter))
				.sorted(new FlightDataComparator(filter , ctx.locale))
				.collect(Collectors.toList());
	}
	
	
	private Predicate<? super AvailableFlightsSelectionRender> filterFlights (boolean directFlightOnly, BookingFlightFilterTypeEnum filter) {
		return p -> {
			Calendar departureDate = p.getFlightData().getFlightType().equals(FlightTypeEnum.DIRECT) 
					? ((DirectFlightData)p.getFlightData()).getDepartureDate() 
					: ((DirectFlightData)((ConnectingFlightData)p.getFlightData()).getFlights().get(0)).getDepartureDate();
			Calendar startDay = Calendar.getInstance();
			startDay.setTime(departureDate.getTime());
			startDay.set(Calendar.HOUR_OF_DAY, START_DAY_HH);
			startDay.set(Calendar.MINUTE, START_DAY_MM);
			Calendar startAfternoon = Calendar.getInstance();
			startAfternoon.setTime(departureDate.getTime());
			startAfternoon.set(Calendar.HOUR_OF_DAY, START_AFTERNOON_HH);
			startAfternoon.set(Calendar.MINUTE, START_AFTERNOON_MM);
			Calendar startNight = Calendar.getInstance();
			startNight.setTime(departureDate.getTime());
			startNight.set(Calendar.HOUR_OF_DAY, START_NIGHT_HH);
			startNight.set(Calendar.MINUTE, START_NIGHT_MM);
			boolean filterResult = true;
			switch(filter){
			case MORNING:
				filterResult =  (departureDate.compareTo(startDay) >= 0 && departureDate.before(startAfternoon));
				break;
			case AFTERNOON:
				filterResult =  (departureDate.compareTo(startAfternoon) >= 0 && departureDate.before(startNight));
				break;
			case EVENING:
				startDay.add(Calendar.DAY_OF_MONTH, 1);
				filterResult =  (departureDate.compareTo(startNight) >= 0 && departureDate.before(startDay));
				break;
			default:
				filterResult = true;
		}
			return (!directFlightOnly || p.getFlightData().getFlightType().equals(FlightTypeEnum.DIRECT)) && filterResult;
			
			};
		}

	
	/*private RouteData cloneRouteData(RouteData route){
		RouteData route2 = new RouteData();
		ArrayList<FlightData> flights2= new ArrayList<FlightData>();
		for(FlightData fl : route.getFlights()){
			flights2.add(fl);
		}
		route2.setFlights(flights2);
		route2.setIndex(route.getIndex());
		route2.setType(route.getType());
		return route2;
	}*/
	

	public List<AvailableFlightsSelectionRender> getAvailableFlightSelections() {
		return availableFlightSelections;
	}

	public List<BrandRender> getBrandHeaderList() {
		return brandHeaderList;
	}

	public int getRouteIndex() {
		return routeIndex;
	}

	public boolean getSolutionForOtherDates() {
		return solutionForOtherDates;
	}
	
	public boolean getShowMoreFlights() {
		return showMoreFlights;
	}

	public int getBrandEconomySize() {
		return brandEconomySize;
	}
	
	public int getBrandBusinessSize() {
		return brandBusinessSize;
	}
	
	public HashMap<String, ArrayList<BrandRender>> getMissedBrandsMap() {
		return missedBrandsMap;
	}
	
	public List<BrandRender> getMissedBrandsDxMap(){
		return missedBrandsDxMap;
	}

	public String getOrderType() {
		return orderType.value();
	}

	public boolean isDirectFlightOnly() {
		return directFlightOnly;
	}

	public boolean isShowCheckBoxDirectFlightOnly() {
		return showCheckBoxDirectFlightOnly;
	}

	public boolean isFcoLin() {
		return isFcoLin;
	}

	public String getPopupCategory() {
		return popupCategory;
	}
	
	public String getBrandNameForBusinessTrigger(){
		return brandNameForBusinessTrigger;
	}
	
	public String getCssClass() {
		return cssClass;
	}

	public boolean isFilterBusiness() {
		return isFilterBusiness;
	}

	public DateRender getDate() {
		return date;
	}
	
	public String getBaseUrl() {
		return baseUrl;
	}
	public boolean isMultiLeg(){
		boolean isMultiLeg=false;
		if(ctx.searchKind!=null){
		isMultiLeg= ctx.searchKind==BookingSearchKindEnum.MULTILEG;}
		return isMultiLeg;
	}
	
	public boolean getShowFarePrice(){
		return showFarePrice;
	}
	public String isCarnet(){
		return ctx.isCarnetProcess?"1":"0";
	}
	
	public boolean isBusiness() {
		return isBusiness;
	}

	public boolean isShowBrandInfoPopupIcon() {
		return showBrandInfoPopupIcon;
	}

	public String getRecapFlightSliceText() {
		return recapFlightSliceText;
	}

	public boolean isRTL() {
		return isRTL;
	}
}
