/** Classe di log per Errori Gigya */

package com.alitalia.aem.consumer.gigya.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GigyaErrorLog {

    private static Logger logger = LoggerFactory.getLogger(GigyaErrorLog.class);

    public void error(String log) {
        logger.error(log);
    }
}
