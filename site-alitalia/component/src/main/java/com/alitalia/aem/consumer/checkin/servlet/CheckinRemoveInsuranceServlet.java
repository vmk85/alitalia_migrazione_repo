package com.alitalia.aem.consumer.checkin.servlet;
import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checkinancillarycartremoveinsurance" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinRemoveInsuranceServlet extends GenericCheckinFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private CheckinSession checkinSession;	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		checkinSession.removeInsuranceFromCart(getCheckInSessionContext(request));
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value("OK");
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}


