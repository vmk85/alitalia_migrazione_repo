package com.alitalia.aem.consumer.checkin.exception;

import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;

public class CheckinPaymentException extends GenericFormValidatorServletException {

	private static final long serialVersionUID = -8388948758281010112L;
	
	public CheckinPaymentException(boolean isRecoverable, String message) {
		super(isRecoverable, message);
	}
	
}
