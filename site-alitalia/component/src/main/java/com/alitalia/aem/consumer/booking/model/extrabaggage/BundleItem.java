package com.alitalia.aem.consumer.booking.model.extrabaggage;


/** 
 Sabre Request Model for BundleItem
*/
public class BundleItem
{
	private String code;
	public final String getcode()
	{
		return code;
	}
	public final void setcode(String value)
	{
		code = value;
	}
	private String groupCode;
	public final String getgroupCode()
	{
		return groupCode;
	}
	public final void setgroupCode(String value)
	{
		groupCode = value;
	}
}