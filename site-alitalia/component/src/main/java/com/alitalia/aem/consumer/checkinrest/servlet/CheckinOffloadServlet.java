package com.alitalia.aem.consumer.checkinrest.servlet;

import com.alitalia.aem.common.data.checkinrest.model.offload.request.PnrFlightInfo;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;
import java.util.List;

@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkinoffloadrest"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinOffloadServlet extends GenericCheckinFormValidatorServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ICheckinDelegate checkInDelegateRest;

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		
		try {
			logger.info("Entrati in [CheckinOffloadServlet - performSubmit]");

			CheckinOffloadRequest CheckinOffloadRequest = new CheckinOffloadRequest();
			CheckinOffloadRequest.setTid(IDFactory.getTid());
			CheckinOffloadRequest.setSid(IDFactory.getSid(request));
			CheckinOffloadRequest.setAirline(request.getParameter("airline"));
			CheckinOffloadRequest.setDepartureDate(request.getParameter("departureDate"));
			CheckinOffloadRequest.setFlight(request.getParameter("flight"));
			CheckinOffloadRequest.setOrigin(request.getParameter("origin"));
			CheckinOffloadRequest.setSeat(new String[]{request.getParameter("seat")});
			CheckinOffloadRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
			CheckinOffloadRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
			CheckinOffloadRequest.setConversationID(ctx.conversationId);

			logger.info("[CheckinOffloadServlet] invocando checkInDelegateRest.offload...");
			CheckinOffloadResponse CheckinOffloadResponse = checkInDelegateRest.offload(CheckinOffloadRequest);

			if (CheckinOffloadResponse != null) {
				if(CheckinOffloadResponse.getError() != null && !CheckinOffloadResponse.getError().equals("")){
					managementError(request, response, CheckinOffloadResponse.getError());
				}else{
					//response.sendRedirect(successPage);
					managementSucces(request, response, CheckinOffloadResponse);
				}
			}
			else {
				logger.error("[CheckinOffloadServlet] - Errore durante l'invocazione del servizio." );
			}
		}
		catch (Exception e) {
			throw new RuntimeException("[CheckinOffloadServlet] - Errore durante l'invocazione del servizio.", e);
		}

	}
	
	private void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error){

		logger.info("[CheckinOffloadServlet] [managementError] error =" + error );

		Gson gson = new Gson();

		com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);

		TidyJSONWriter json;
		try {
			json = new TidyJSONWriter(response.getWriter());

			response.setContentType("application/json");

			json.object();
			json.key("isError").value(true);
			json.key("errorMessage").value(errorObj.getErrorMessage());
			json.key("sabreStatusCode").value(errorObj.getSabreStatusCode());
			json.key("conversationID").value(errorObj.getConversationID());
			json.endObject();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
	}
	
	private void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response, CheckinOffloadResponse CheckinOffloadResponse){

		logger.info("[CheckinOffloadServlet] [managementSucces] ...");
		
		try {
			TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());
	
			jsonOutput.object();
			jsonOutput.key("outcome").value(CheckinOffloadResponse.getOutcome());
			jsonOutput.key("conversationID").value(CheckinOffloadResponse.getConversationID());
			jsonOutput.endObject();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
		
//		String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
//				request.getResource(), false)
//				+ getConfiguration().getCheckinFlightListPage();
//
//		successPage = request.getResourceResolver().map(successPage);
//
//		logger.info("[SetInsuranceServlet] [managementSucces] successPage = " + successPage );
//
//		TidyJSONWriter json;
//		try {
//
//			json = new TidyJSONWriter(response.getWriter());
//			response.setContentType("application/json");
//			json.object();
//			json.key("isError").value(false);
//			json.key("successPage").value(successPage);
//			json.endObject();
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}	

	}


}
