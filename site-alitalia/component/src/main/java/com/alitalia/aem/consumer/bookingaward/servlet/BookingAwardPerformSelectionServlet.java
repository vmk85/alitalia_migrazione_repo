package com.alitalia.aem.consumer.bookingaward.servlet;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.bookingaward.BookingAwardSession;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingawardperformselection" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class BookingAwardPerformSelectionServlet extends GenericBookingAwardFormValidatorServlet {

	@Reference
    private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private BookingAwardSession bookingAwardSession;
	@Reference
	private BookingSession bookingSession;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		// no input fields, always valid
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws Exception {
		
		BookingSessionContext ctx = getBookingSessionContext(request);
		
		String solutionId = request.getParameter("solutionId");
		String indexFlight = request.getParameter("indexFlight");
		String indexBrand = request.getParameter("indexBrand");
		String indexRoute = request.getParameter("indexRoute");
		
		if (indexFlight == null || ("").equals(indexFlight)) {
			logger.error("indexFlight not found");
			throw new RuntimeException("indexFlight not found");
		}
		int solutionFlightIndex = Integer.parseInt(indexFlight);
		
		if (indexBrand == null || ("").equals(indexBrand)) {
			logger.error("indexBrand not found");
			throw new RuntimeException("indexBrand not found");
		}
		int solutionBrandIndex = Integer.parseInt(indexBrand);
		
		if (("").equals(solutionId) || solutionId==null ) {
			logger.error("solutionId not found");
			throw new RuntimeException("solutionId not found");
		}
		
		
		if (indexRoute == null || ("").equals(indexRoute)) {
			logger.error("indexRoute not found");
			throw new RuntimeException("indexRoute not found");
		}
		int elementSelectionIndex = Integer.parseInt(indexRoute);
		
		if (elementSelectionIndex != 1 && elementSelectionIndex != 0) {
			logger.error("indexRoute > 1");
			throw new RuntimeException("indexRoute > 1");
		}
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		if (bookingAwardSession.enoughPointsToBuy(ctx, elementSelectionIndex, solutionId, solutionFlightIndex, solutionBrandIndex)) {
			bookingAwardSession.performFlightSelection(ctx, elementSelectionIndex, solutionId, solutionFlightIndex, solutionBrandIndex, request);
			int indexToRefresh = bookingSession.obtainRouteIndexToRefresh(ctx, elementSelectionIndex);
			json.key("indexToRefresh").value(indexToRefresh);
			
		}
		else {
			json.key("error").value("true");
		}
		
		json.endObject();
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}
