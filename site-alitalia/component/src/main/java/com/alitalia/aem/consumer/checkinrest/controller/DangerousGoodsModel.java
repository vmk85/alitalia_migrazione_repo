package com.alitalia.aem.consumer.checkinrest.controller;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = { SlingHttpServletRequest.class })
public class DangerousGoodsModel extends GenericCheckinModel {

    @Self
    private SlingHttpServletRequest request;

    @PostConstruct
    protected void initModel() throws Exception {
        try {
            super.initBaseModel(request);

            if (ctx == null) {
                logger.debug("[DangerousGoodsModel] - Contesto assente.");
                return;
            } else {

                    for(int j = 0; j < ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().size();j++){

                        if(ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().get(j).getPassengers() != null) {

                            for (Passenger passenger : ctx.pnrSelectedListDataRender.get(0).getFlightsRender().get(0).getSegments().get(j).getPassengers()) {

                                if (passenger.getCheckInComplete() == false && passenger.getNumeroPostoAssegnato() == null && passenger.getEmployee()) {
                                    ctx.standBy = true;
                                }
                            }
                        }

                    }

                }

        } catch (Exception ex) {
            logger.error("[DangerousGoodsModel] - Unexpected error: ", ex);
        }
    }

}