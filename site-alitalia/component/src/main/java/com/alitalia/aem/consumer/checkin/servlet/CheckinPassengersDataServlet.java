package com.alitalia.aem.consumer.checkin.servlet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.CheckinPassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLinkTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFrequentFlyerCarrierData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinPassenger;
import com.alitalia.aem.consumer.checkin.CheckinPassengersForm;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checkinpassengersdata" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinPassengersDataServlet extends GenericCheckinFormValidatorServlet {
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private CheckinSession checkinSession;
	
	public final static String CHECK_IN_FIELD = "checkin_";
	public final static String FREQUENT_FLYER_CODE_FIELD = "frequentFlyerCode_";
	public final static String FREQUENT_FLYER_TYPE_FIELD = "frequentFlyerType_";
	public final static String EXTRA_PASSENGER_NAME_FIELD = "addPassengerName_";
	public final static String EXTRA_PASSENGER_LAST_NAME_FIELD = "addPassengerLastName_";
	public final static String EXTRA_PASSENGER_TICKET_FIELD = "addPassengerTicket_";
	public static final String CHECK_AGREEMENT_FIELD = "checkAgreement";
	public static final String HAS_LIGTHBOX_INFANT_TO_BE_SHOWN = "hasLigthboxInfantToBeShown";
	public static final String HAS_LIGTHBOX_SPECIAL_FARE_TO_BE_SHOWN = "hasLigthboxSpecialFareToBeShown";
	
	public final static String ERROR_NO_ADULTS_FIELD = "noAdult";
	public final static String FREQUENT_FLYER_CODE_NOT_VALID_MESSAGE = "checkin.passeggeri.codiceMillemigliaNonValido.label";
	public final static String TERMS_NOT_CHECKED = "checkin.passeggeri.necessariaAccettazioneCondizioni.label";
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		Validator validator = null;
		CheckinPassengersForm form = null;
		
		form = prepareForm(request, ctx.passengers.size());
		validator = validateFormBasic(form, ctx);
		
		/* Risultato della validazione */
		ResultValidation resultValidation = validator.validate();
		
		if (resultValidation.getResult()) {
			
			// temporarly update passengers data in context after a successful validation (the related
			// data will be required for frequent flyers validation and for seat map selection step)
			// bookingSession.updatePassengersData(ctx, form.toPassengersData(ctx));
			
			// check if a freqentFlyer code is specified for at least a passenger
			int cont = 0;
			for (CheckinPassenger passenger : form.getPassengers()) {
				CheckinPassengerData passengerData = ctx.passengers.get(passenger.getIndex());
				if (CheckinPassengerTypeEnum.NORMAL.equals(passengerData.getType())) {
					if (passenger.getFrequentFlyerCode() != null && !passenger.getFrequentFlyerCode().isEmpty() &&
							passenger.getFrequentFlyerType() != null && !passenger.getFrequentFlyerType().isEmpty()) {
						
						for (FrequentFlyerTypeData frequentFlyerTypeData : ctx.frequentFlyerTypes) {
							if (passenger.getFrequentFlyerType().equals(frequentFlyerTypeData.getCode())) {
								passengerData.setFrequentFlyerCode(passenger.getFrequentFlyerCode());
								MmbFrequentFlyerCarrierData frequentFlyerCarrier = new MmbFrequentFlyerCarrierData();
								frequentFlyerCarrier.setCode(frequentFlyerTypeData.getCode());
								frequentFlyerCarrier.setDescription(frequentFlyerTypeData.getDescription());
								frequentFlyerCarrier.setId(0);
								frequentFlyerCarrier.setLenght(frequentFlyerTypeData.getLenght() != null ? frequentFlyerTypeData.getLenght() : 0);
								frequentFlyerCarrier.setPreFillChar(frequentFlyerTypeData.getPreFillChar());
								frequentFlyerCarrier.setRegularExpressionValidation(
										frequentFlyerTypeData.getRegularExpressionValidation());
								passengerData.setFrequentFlyerCarrier(frequentFlyerCarrier);
							}
						}
						cont++;
					} else {
						passengerData.setFrequentFlyerCode(null);
						passengerData.setFrequentFlyerCarrier(null);
					}
				}
			}
			if (cont > 0) {
				// perform the frequent flyer codes validation using the service
				List<String> adultPassengersWithWrongFrequentFlyer = 
						checkinSession.checkFrequentFlyerCodes(ctx, ctx.passengers);
				if (adultPassengersWithWrongFrequentFlyer != null) {
					for (String adultPaxWithWrongFrequentFlyer : adultPassengersWithWrongFrequentFlyer) {
						resultValidation.setResult(false);
						// find the relevant passenger by frequent flyer data
						for (int i = 0; i < form.getPassengers().size(); i++) {
							if (adultPaxWithWrongFrequentFlyer.equals(form.getPassengers().get(i).getFrequentFlyerCode())) {
								resultValidation.getFields().put(
										FREQUENT_FLYER_CODE_FIELD + i, CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD);
							}
						}
					}
				}
			}
		}
		
		return resultValidation;
	}
	
	private CheckinPassengersForm prepareForm(SlingHttpServletRequest request, int size) {
		
		CheckinPassengersForm form = new CheckinPassengersForm();
		
		List<CheckinPassenger> passengers = new ArrayList<CheckinPassenger>();
		for (int i = 0; i < size; i++) {
			boolean checkin = Boolean.parseBoolean(request.getParameter(CHECK_IN_FIELD + i));
			if (checkin) {
				CheckinPassenger passenger = new CheckinPassenger();
				passenger.setIndex(i);
				passenger.setCheckin(checkin);
				passenger.setFrequentFlyerType(request.getParameter(FREQUENT_FLYER_TYPE_FIELD + i));
				passenger.setFrequentFlyerCode(request.getParameter(FREQUENT_FLYER_CODE_FIELD + i));
				passengers.add(passenger);
			}
		}
		form.setHasLigthboxInfantToBeShown(Boolean.parseBoolean(request.getParameter(HAS_LIGTHBOX_INFANT_TO_BE_SHOWN)));
		form.setHasLigthboxSpecialFareToBeShown(Boolean.parseBoolean(request.getParameter(HAS_LIGTHBOX_SPECIAL_FARE_TO_BE_SHOWN)));
		form.setPassengers(passengers);
		form.setCheckAgreement(request.getParameter(CHECK_AGREEMENT_FIELD));
		return form;
	}
		
	private Validator validateFormBasic(CheckinPassengersForm form, CheckinSessionContext ctx) {
		
		Validator validator = new Validator();
		
		if (!form.hasPassengersToCheckIn()) {
			// Inserisco un errore sul primo passeggero
			validator.addDirectCondition(CHECK_IN_FIELD + "0", "", CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			return validator;
		}
		
		List<CheckinPassenger> passengers = form.getPassengers();
		
		boolean noAdults = CheckinUtils.getCheckedInPassengers(ctx.passengers).isEmpty();
		// check if there is at least an adult
		if (noAdults) {
			for (CheckinPassenger passenger : passengers) {
				if (CheckinPassengerTypeEnum.NORMAL.equals(ctx.passengers.get(passenger.getIndex()).getType())) {
					noAdults = false;
					break;
				}
			}
		}
		
		if (noAdults) {
			validator.addDirectCondition(ERROR_NO_ADULTS_FIELD, "", "true", "isNotEmpty");
			return validator;
		}
		
		for (CheckinPassenger passenger : passengers) {
			if (passenger.getFrequentFlyerCode() != null && 
					!passenger.getFrequentFlyerCode().isEmpty()) {
				
				validator.addDirectCondition(FREQUENT_FLYER_TYPE_FIELD + passenger.getIndex(), 
						passenger.getFrequentFlyerType(), CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				validator.addDirectCondition(FREQUENT_FLYER_CODE_FIELD + passenger.getIndex(), 
						passenger.getFrequentFlyerCode(), FREQUENT_FLYER_CODE_NOT_VALID_MESSAGE, "isNumber");
			}
			
			if (passenger.getFrequentFlyerType() != null &&
					!passenger.getFrequentFlyerType().isEmpty()) {
				
				validator.addDirectCondition(FREQUENT_FLYER_CODE_FIELD + passenger.getIndex(), 
						passenger.getFrequentFlyerCode(), CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
				
			}
		}
				
		validator.addDirectCondition(CHECK_AGREEMENT_FIELD, form.getCheckAgreement(),
				TERMS_NOT_CHECKED, "isTrue");
		
		return validator;
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		CheckinSessionContext ctx = getCheckInSessionContext(request);
		CheckinPassengersForm form = prepareForm(request, ctx.passengers.size());
		
		ctx.selectedPassengers = new ArrayList<CheckinPassengerData>();
		
		boolean isThereChild = false;
		boolean isThereInfant = false;
		boolean isSpecialFare = false;
		
		for (int i = 0; i < form.getPassengers().size(); i++) {
			CheckinPassenger passenger = form.getPassengers().get(i);
			CheckinPassengerData passengerData = ctx.passengers.get(passenger.getIndex());

			if(!isThereChild && CheckinPassengerTypeEnum.CHILD.equals(passengerData.getType()) &&
					!form.getHasLigthboxInfantToBeShown()){
				isThereChild = true;
			}
			if(!isThereInfant && MmbLinkTypeEnum.INFANT_ACCOMPANIST.equals(passengerData.getLinkType()) &&
					!form.getHasLigthboxInfantToBeShown()){
				isThereInfant = true;
			}
			if(!isSpecialFare && passengerData.getSpecialFare() != null && passengerData.getSpecialFare() &&
					!form.getHasLigthboxSpecialFareToBeShown()){
				isSpecialFare = true;
			}
			
			ctx.selectedPassengers.add(passengerData);
		}
		
		if(logger.isDebugEnabled()){
			logger.debug("isThereChildOrInfant: " + (isThereChild || isThereInfant)  + ", isSpecialFare: " + isSpecialFare);
		}
		
		// checkinSession.updatePassengersFirstStep(form, ctx);
		
		String baseUrl = request.getResource().getResourceResolver().map(
				AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
		
		String nextPage = null;
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		
		if (isThereChild || isThereInfant || isSpecialFare){ 
			//devo mostrate le relative lightbox
			if(isThereChild && isThereInfant){
				json.key("lightboxInfant").value("true");
				json.key("youngType").value("infant+child");

			}
			else if (isThereChild){
				json.key("lightboxInfant").value("true");
				json.key("youngType").value("child");
			}
			else if (isThereInfant){
				json.key("lightboxInfant").value("true");
				json.key("youngType").value("infant");
			}
			if (isSpecialFare)
				json.key("lightboxSpecialFare").value("true");
			
		} else if (ctx.apisBasic || ctx.apisPlus) {
			ctx.apisPhase = true;
			nextPage = configuration.getCheckinPassengersPage();
		} else {
			checkinSession.performCheckin(ctx);
			nextPage = configuration.getCheckinAncillaryPage();
		}
		
		json.key("result").value("OK");
		json.key("redirect").value(baseUrl + nextPage);
		json.endObject();
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}
