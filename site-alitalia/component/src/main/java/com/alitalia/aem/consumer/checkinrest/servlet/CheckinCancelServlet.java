package com.alitalia.aem.consumer.checkinrest.servlet;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.validation.ResultValidation;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;

/**
 * Created by ggadaleta on 01/02/2018.
 */

@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = { "checkincancel"}),
        @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
        @Property(name = "sling.servlet.extensions", value = { "json" }),
        @Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinCancelServlet extends GenericCheckinFormValidatorServlet {

    private static final Logger logger = LoggerFactory.getLogger(CheckinCancelServlet.class);

    @Reference
    private AlitaliaConfigurationHolder configuration;

    @Override
    protected AlitaliaConfigurationHolder getConfiguration() {
        return configuration;
    }

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        ResultValidation resultValidation = new ResultValidation();
        resultValidation.setResult(true);
        return resultValidation;
    }

    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
        String sessionId = request.getSession().getId();
        try {
            CheckinSessionContext ctx = getCheckInSessionContext(request);
            if(ctx != null) {
                logger.debug("[{}] destroying checkin session",sessionId);
                request.getSession().invalidate(); // --> event listening in CheckinSessionListener
            }
        } catch (Exception e) {
            logger.error("["+sessionId+"] error while destroying checkin session : " + e.getMessage());
            e.printStackTrace();
        }
    }

}
