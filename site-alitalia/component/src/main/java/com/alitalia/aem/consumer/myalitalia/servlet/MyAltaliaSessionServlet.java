/**
 * Servlet impostare la sessione MyAlitalia
 */

package com.alitalia.aem.consumer.myalitalia.servlet;

import com.alitalia.aem.common.data.crmdatarest.model.Result;
import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.data.home.MyFlightsDataModel;
import com.alitalia.aem.common.data.home.QueryResult;
import com.alitalia.aem.common.messages.crmdatarest.GetCrmDataInfoResponse;
import com.alitalia.aem.consumer.gigya.GigyaSession;
import com.alitalia.aem.consumer.gigya.GigyaUtils;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSession;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSessionContext;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaUtils;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaUtilsService;
import com.alitalia.aem.consumer.myalitalia.model.MyAlitaliaOtpModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.ws.otp.service.xsd1.SendOTPWithAppResponse;
import com.day.cq.i18n.I18n;
import com.gigya.socialize.GSResponse;
import com.google.gson.Gson;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = {"GetMyAlitaliaCRMDataToSession", "SetMyAlitaliaPassword", "GetGigyaCookie", "deleteAccountAssociated","SetMyAlitaliaGigyaFlights","MyAlitaliaSendOtp","MyAlitaliaCheckOtp","MyAlitaliaCheckLinkAccount"}),
        @Property(name = "sling.servlet.methods", value = {"POST"}),
        @Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
        @Property(name = "sling.servlet.extensions", value = {"json"})
})
public class MyAltaliaSessionServlet extends GenericMyAlitaliaFormValidatorServlet {

    @Reference
    private volatile MyAlitaliaSession myAlitaliaSession;

    @Reference
    private volatile GigyaSession gigyaSession;

    protected static Logger logger = LoggerFactory.getLogger(MyAltaliaSessionServlet.class);

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) {

        Validator validator = new Validator();

        ResultValidation resultValidation = validator.validate();
        if (!resultValidation.getResult()) {
            logger.error("Error in validateForm");

        }
        return resultValidation;

    }

    /** Metodo PostValidator, setto ed estraggo i dati dell'utente dalla sessione
     * ( Modificare la descrizione del metodo nel momento in cui vengono aggiunti nuovi sviluppi ) */
    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

        /** Estraggo il selettore Utilizzato per comunicare con la servlet */
        String selectorString = request.getRequestPathInfo().getSelectorString();

        String jsonString = "";

        Result result = null;

        MyAlitaliaSessionContext mactx = null;

        JSONObject obj = new JSONObject();

        boolean isError = false;

        GSResponse gsResponse = null;

        final I18n i18n = new I18n(request.getResourceBundle(AlitaliaUtils.findResourceLocale(request.getResource())));

        logger.debug("MyAlitaliaCrmDataServlet interrogata per " + selectorString);

        switch (selectorString) {

            /** Check otp MyAlitalia*/
            case "MyAlitaliaCheckOtp":

                String OTP = request.getParameter("checkOtp");

                mactx = getMyAlitaliaSessionContext(request);

                int countError = 0;

                if (mactx != null && mactx.getMyAlitaliaOtpModel() != null){
                    countError = mactx.getMyAlitaliaOtpModel().getCountError();
                }

                /** Estraggo dalla sessione i dati dell'otp inviato in precedenza */
                MyAlitaliaOtpModel myAlitaliaOtpModel = mactx.getMyAlitaliaOtpModel();
                String originalOtp = myAlitaliaOtpModel.getOTP();
                Date startDate = myAlitaliaOtpModel.getStartDate();

                /** imposto la data */
                Date dateAct = MyAlitaliaUtils.getActualDate();

                /** imposto la data di scadenza dell'otp */


                long expOtp = mactx.getMyAlitaliaOtpModel().getExpMillisecondOtp();
                long expOtpFe = Math.abs(dateAct.getTime() - startDate.getTime());

                /** Controllo che l'otp coincida con il codice in sessione e che non sia scaduto */
                if (originalOtp.equals(OTP) && expOtpFe <= expOtp){

                    if (mactx.getMaCustomer() != null && mactx.getMaCustomer().getData().getIsStrong() == null ){
                        JSONObject obj2 = new JSONObject();
                        obj2.put("isStrong","true");
                        obj2.put("phone1Number",myAlitaliaOtpModel.getNumero());
                        obj2.put("phone1CountryCode",myAlitaliaOtpModel.getPrefisso());
                        try {

                            /** Salvo i dati su gigya */
                            gsResponse = gigyaSession.setAccount(mactx.getMaCustomer().getUID(),GigyaUtils.mapJsonObjGigya(obj2.toString()));
                        } catch (org.apache.sling.commons.json.JSONException e) {
                            logger.error("Error to parse mapJsonObjGigya MyAlitaliaCheckOtp");
                        }
                        /** Se non ci sono stati errori, salvo tutto in sessione */
                        if (gsResponse != null && gsResponse.getErrorCode() == 0){
                            MACustomer maCustomer = mactx.getMaCustomer();
                            maCustomer.getData().setPhone1Number(myAlitaliaOtpModel.getNumero());
                            maCustomer.getData().setPhone1CountryCode(myAlitaliaOtpModel.getPrefisso());
                            maCustomer.getData().setIsStrong("true");
                            mactx.setMaCustomer(maCustomer);
                            MyAlitaliaUtils.setMactxToSession(request, mactx);
                        } else {
                            isError = true;
                        }

                    }

                 } else {
                    isError = true;
                    countError = countError + 1;
                    /** invio l'otp in caso di errore/otp scaduto */
                    if (countError < 3){
                        SendOTPWithAppResponse sendOTPWithAppResponse = myAlitaliaSession.SendVerificationOtp( myAlitaliaOtpModel.getPrefisso(), myAlitaliaOtpModel.getNumero() ,i18n.get("myalitalia.otp.smsdescription"));
                        if (sendOTPWithAppResponse != null){
                            mactx.getMyAlitaliaOtpModel().setOTP(sendOTPWithAppResponse.getOTP().getValue());
                            mactx.getMyAlitaliaOtpModel().setStartDate(MyAlitaliaUtils.getActualDate());
                            mactx.getMyAlitaliaOtpModel().setCountError(countError);
                            mactx.getMyAlitaliaOtpModel().setExpMillisecondOtp(MyAlitaliaUtils.getDifferenceDate(sendOTPWithAppResponse.getExpiryDate().getValue(),sendOTPWithAppResponse.getSentDate().getValue()));
                        } else {
                            isError = true;
                            obj.put("errorMessage", "myalitalia.otp.serverError");
                        }
                    } else {
                        mactx.setMyAlitaliaOtpModel(new MyAlitaliaOtpModel());
                    }
                    MyAlitaliaUtils.setMactxToSession(request,mactx);
                }

                obj.put("isError", isError);
                obj.put("countError", countError);
                jsonString = obj.toString();


                break;

            /** Invio otp myalitalia*/
            case  "MyAlitaliaSendOtp":

                /** Inizzializzo le variabili per l'invio dell'otp */
                mactx = getMyAlitaliaSessionContext(request);
                mactx.setMyAlitaliaOtpModel(new MyAlitaliaOtpModel());
                String prefisso = request.getParameter("prefix");
                String numero = request.getParameter("num");

                /** Invio l'otp */
                SendOTPWithAppResponse sendOTPWithAppResponse = myAlitaliaSession.SendVerificationOtp( prefisso, numero ,i18n.get("myalitalia.otp.smsdescription"));

                /** Se non ci sono stati errori nell'invio, salvo in sessione i dati */
                if (sendOTPWithAppResponse != null){
                    mactx.getMyAlitaliaOtpModel().setOTP(sendOTPWithAppResponse.getOTP().getValue());
                    mactx.getMyAlitaliaOtpModel().setStartDate(MyAlitaliaUtils.getActualDate());
                    mactx.getMyAlitaliaOtpModel().setExpMillisecondOtp(MyAlitaliaUtils.getDifferenceDate(sendOTPWithAppResponse.getExpiryDate().getValue(),sendOTPWithAppResponse.getSentDate().getValue()));
                    mactx.getMyAlitaliaOtpModel().setNumero(numero);
                    mactx.getMyAlitaliaOtpModel().setPrefisso(prefisso);
                    MyAlitaliaUtils.setMactxToSession(request,mactx);
                    obj.put("isError", isError);
                    jsonString = obj.toString();
                } else {
                    isError = true;
                    obj.put("isError", isError);
                    obj.put("errorMessage", "myalitalia.otp.serverError");
                    jsonString = obj.toString();
                }

                break;

            /** Cancello l'account associato */
            case "deleteAccountAssociated":

                mactx = getMyAlitaliaSessionContext(request);

                String UID = mactx.getMaCustomer().getUID();

                String provider = request.getParameter("provider");

                isError = gigyaSession.removeConnection(UID, provider);

                if (!isError) {
                    gsResponse = gigyaSession.getAccount(mactx.getMaCustomer().getUID());
                    if (gsResponse.getErrorCode() == 0) {
                        Gson gson = new Gson();
                        MACustomer maCustomer = gson.fromJson(gsResponse.getResponseText(), MACustomer.class);
                        MyAlitaliaUtils.setMaCustomerToSession(request, myAlitaliaSession, maCustomer);
                    } else {
                        isError = true;
                    }
                }
                try {
                    obj.put("isError", isError);
                    jsonString = obj.toString();
                } catch (Exception e) {
                    logger.error("Errore creazione json risposta servlet selettore => deleteAccountAssociated : " + e.toString(), e);
                }

                break;

            case "GetGigyaCookie":

                String cookie = "";

                try {
                    mactx = getMyAlitaliaSessionContext(request);

                    if (mactx != null) {

                        cookie = mactx.getMaCustomer().getSessionInfo().getCookieValue();

                    } else {

                        isError = true;

                    }

                    obj.put("isError", isError);
                    obj.put("cookie", cookie);
                    jsonString = obj.toString();

                } catch (Exception e) {
                    logger.error("Errore creazione json risposta servlet selettore => GetGigyaCookie : " + e.toString(), e);

                }

                break;

            case "SetMyAlitaliaPassword":

                try {
                    String gigyaData = request.getParameter("gigya");
                    mactx = getMyAlitaliaSessionContext(request);

                    if (gigyaData != null) {

                        gsResponse = gigyaSession.setAccount(mactx.getMaCustomer().getUID(), GigyaUtils.mapJsonDynamicObjGigya(gigyaData));

                        if (gsResponse.getErrorCode() != 0) {
                            isError = true;
                        }

                    } else {
                        isError = true;
                    }
                    obj.put("isError", isError);
                    jsonString = obj.toString();
                } catch (Exception e) {
                    logger.error("Errore creazione json risposta servlet selettore => SetMyAlitaliaPassword : " + e.toString(), e);

                }

                break;

            case "SetMyAlitaliaGigyaFlights":

                try {
                    String gigyaData = request.getParameter("gigya");
                    mactx = getMyAlitaliaSessionContext(request);

                    if (gigyaData != null) {

                        List<MyFlightsDataModel> myFlightsDataModelList = mactx.getMaCustomer().getData().getFlights();
                        Gson gson = new Gson();
                        MyFlightsDataModel myFlightsDataModel = gson.fromJson(gigyaData,MyFlightsDataModel.class);
                        if (myFlightsDataModelList == null){
                            myFlightsDataModelList = new ArrayList<>();
                            myFlightsDataModelList.add(myFlightsDataModel);
                        } else {
                            myFlightsDataModelList.add(myFlightsDataModel);
                        }
                        JSONObject obj2 = new JSONObject();
                        obj2.put("flights", myFlightsDataModelList);
                        gsResponse = gigyaSession.setAccount(mactx.getMaCustomer().getUID(),GigyaUtils.mapJsonObjGigya(obj2.toString()));
                        if (gsResponse.getErrorCode() != 0) {
                            isError = true;
                        } else {
                            MACustomer maCustomer = MyAlitaliaUtils.getMaCustomerByMactx(mactx);
                            maCustomer.getData().setFlights(new ArrayList<>());
                            maCustomer.getData().setFlights(myFlightsDataModelList);
                            mactx.setMaCustomer(maCustomer);
                            mactx = myAlitaliaSession.setSingleFlightToMactx(request,maCustomer,mactx);
                            MyAlitaliaUtils.setMactxToSession(request,mactx);
                        }

                    } else {
                        isError = true;
                    }
                    obj.put("isError", isError);
                    jsonString = obj.toString();
                } catch (Exception e) {
                    logger.error("Errore creazione json risposta servlet selettore => SetMyAlitaliaGigyaFlights : " + e.toString(), e);

                }

                break;

            case "GetMyAlitaliaCRMDataToSession":

                GetCrmDataInfoResponse getCrmDataInfoResponse = MyAlitaliaUtils.getMyAlitaliaCrmDataBySession(request);
                result = (getCrmDataInfoResponse != null ? getCrmDataInfoResponse.getResult() : null);

                try {

                    jsonString = MyAlitaliaUtils.getJsonResponse(getCrmDataInfoResponse, result);
                    if (getMyAlitaliaSessionContext(request).getMaCustomer().getData() != null && getMyAlitaliaSessionContext(request).getMaCustomer().getData().getFlightPref_FF() != null) {
                        obj = new JSONObject(jsonString);
                        obj.put("gigya", getMyAlitaliaSessionContext(request).getMaCustomer().getData().getFlightPref_FF());
                        obj.put("gigya", getMyAlitaliaSessionContext(request).getMaCustomer().getData().getFlightPref_FF());
                        jsonString = obj.toString();
                    }

                } catch (Exception e) {
                    logger.error("Errore creazione json risposta servlet selettore => GetMyAlitaliaCRMDataToSession : " + e.toString(), e);
                }

                logger.debug("Response json Servlet : " + jsonString);

                break;

                case "MyAlitaliaCheckLinkAccount":

                    boolean isOnlySite = false, isOnlySocial = false, isFull = false;

                    boolean isSite = false, isSocial = false;

                    boolean facebook = false, googleplus = false, linkedin = false;
                    String token = request.getParameter("authToken");

                    String regMail = null;

                    GSResponse gsResponseLinkAccount = gigyaSession.getAccountLinkAccount(token);

                    if (gsResponseLinkAccount.getErrorCode() == 0){

                        Gson gson = new Gson();
                        MACustomer maCustomer = gson.fromJson(gsResponseLinkAccount.getResponseText(), MACustomer.class);

                        String query = "SELECT socialProviders FROM accounts WHERE profile.email = \"" + maCustomer.getProfile().getEmail() + "\"";

                        GSResponse gsResponseQuery = gigyaSession.search(query);

                        if (gsResponseQuery.getErrorCode() == 0){

                            regMail = maCustomer.getProfile().getEmail();

                            maCustomer = gson.fromJson(gsResponseQuery.getResponseText(),MACustomer.class);

                            if (maCustomer.getResults() != null && maCustomer.getResults().length > 0 ){

                                for(QueryResult queryResult : maCustomer.getResults()){
                                    if(queryResult.getSocialProviders().contains("site")){
                                        isSite = true;
                                    }
                                    if(queryResult.getSocialProviders().contains("linkedin")){
                                        isSocial = true;
                                        linkedin = true;
                                    }
                                    if(queryResult.getSocialProviders().contains("facebook")){
                                        isSocial = true;
                                        facebook = true;
                                    }
                                    if(queryResult.getSocialProviders().contains("googleplus")){
                                        isSocial = true;
                                        googleplus = true;
                                    }
                                }

                                if (isSocial && isSite){
                                    isFull = true;
                                } else if(isSite && !isSocial){
                                    isOnlySite = true;
                                } else if(!isSite && isSocial){
                                    isOnlySocial = true;
                                }

                            } else {

                                isError = true;

                            }
                        } else {

                            isError = true;

                        }
                    } else {

                        isError = true;

                    }

                    obj.put("isError", isError);
                    obj.put("isFull", isFull);
                    obj.put("isOnlyWeb", isOnlySite);
                    obj.put("isOnlySocial", isOnlySocial);
                    obj.put("googleplus", googleplus);
                    obj.put("facebook", facebook);
                    obj.put("linkedin", linkedin);
                    obj.put("regMail", regMail);
                    obj.put("token", token);
                    jsonString = obj.toString();

                    break;

        }
        try {
            Gson gson = new Gson();
            response.setContentType("application/json");
            PrintWriter writer = response.getWriter();
            writer.write(jsonString);

        } catch (Exception e) {
            logger.error("Unexected error generating JSON response MyAlitaliaCrmDataServlet : " + e.toString(), e);
        }
    }
}

