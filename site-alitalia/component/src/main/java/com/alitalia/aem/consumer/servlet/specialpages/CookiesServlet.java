package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.http.Cookie;

import com.alitalia.aem.consumer.utils.CookieUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.CookiesData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.day.cq.i18n.I18n;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "cookiessubmit" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class CookiesServlet extends GenericFormValidatorServlet {
	
	

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[CookiesServlet] validateForm");

		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request, new Validator());
			return validator.validate();

		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Error during parameters validation", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		logger.debug("[CookiesServlet] performSubmit");

		Boolean marketingCookie = Boolean.parseBoolean(request.getParameter("accettazione"));
		Boolean thidpartyCookie = Boolean.parseBoolean(request.getParameter("accettazione1"));

		// set no 3rdparty nor commercial cookies
		Cookie thirdParty = new Cookie(AlitaliaConstants.COOKIE_NO_THIRDPARTY_COOKIES, "0");
		if (!thidpartyCookie) {
			thirdParty.setValue("1");
			thirdParty.setPath("/");
			thirdParty.setMaxAge(AlitaliaConstants.COOKIES_MAX_AGE);
   
   
			/*Cookie fanplayr_user =new Cookie("fanplayr_user", "0");
            fanplayr_user.setValue(" ");
            fanplayr_user.setPath("/");
            fanplayr_user.setMaxAge(365);
            fanplayr_user.setDomain("e1.fanplayr.com");
            response.addCookie(fanplayr_user);*/
			
            /*Cookie fanplayr_sc =new Cookie("fanplayr_sc", "0");
            fanplayr_sc.setValue(" ");
            fanplayr_sc.setPath("/");
            fanplayr_sc.setMaxAge(365);
            fanplayr_sc.setDomain("e1.fanplayr.com");
            response.addCookie(fanplayr_sc);*/
            
            //fanplayr
            
            Cookie fanplayr =new Cookie("fanplayr", "novalue");
            fanplayr.setPath("/");
            fanplayr.setMaxAge(AlitaliaConstants.COOKIES_MAX_AGE);
            response.addCookie(fanplayr);
		}
		else{
			//delete cookie
			thirdParty.setValue("0");
			thirdParty.setPath("/");
			thirdParty.setMaxAge(0);
		}
		response.addCookie(thirdParty);
		
		Cookie marketing = new Cookie(AlitaliaConstants.COOKIE_NO_MARKETING_COOKIES, "0");
		if (!marketingCookie) {
			marketing.setValue("1");
			marketing.setPath("/");
			marketing.setMaxAge(AlitaliaConstants.COOKIES_MAX_AGE);

			
            Cookie alitalia_consumer_geo =new Cookie("alitalia-consumer-geo", "novalue");
            alitalia_consumer_geo.setPath("/");
            alitalia_consumer_geo.setMaxAge(AlitaliaConstants.COOKIES_MAX_AGE);
            response.addCookie(alitalia_consumer_geo);
		}
		else{
			//delete cookie
			marketing.setValue("0");
			marketing.setPath("/");
			marketing.setMaxAge(0);
		}
		response.addCookie(marketing);
		
		Cookie cookieConfirmed = new Cookie(AlitaliaConstants.COOKIE_CONFIRMED, "true");
		cookieConfirmed.setPath("/");
		cookieConfirmed.setMaxAge(AlitaliaConstants.COOKIES_MAX_AGE);
		response.addCookie(cookieConfirmed);
		
		/*CookieUtils.setCookieValue(response, "fanplayr_user", "e1.fanplayr.com","/", 2, "AAAA", false, false);
		CookieUtils.setCookieValue(response, "fanplayr_sc", "e1.fanplayr.com","/", 2, "AAAA", false, false);
		CookieUtils.setCookieValue(response, "fanplayr", ".alitalia.com","/", 2, "AAAA", false, false);
		CookieUtils.setCookieValue(response, "alitalia-consumer-geo", ".alitalia.com","/", 2, "AAAA", false, false);*/
		// go back baby!
		goBackSuccessfully(request, response);
		
	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[CookiesServlet] saveDataIntoSession");
		
		// create cookiesData object and save it into session
		CookiesData cookiesData = new CookiesData();
		cookiesData.setMarketingCookie(request.getParameter("accettazione"));
		cookiesData.setThirdpartyCookie(request.getParameter("accettazione1"));
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		// go back to form with error
		cookiesData.setError(i18n.get(I18nKeySpecialPage.ERROR_SERVICE));
		request.getSession().setAttribute(CookiesData.NAME, cookiesData);
	}

	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {

		String marketingCookie = request.getParameter("accettazione");
		String thidpartyCookie = request.getParameter("accettazione1");
		
		// add validate conditions
		
		// third party cookies
		validator.setAllowedValuesMessagePattern("accettazione1",
				thidpartyCookie, new String[]{"true", "false"},
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.COOKIES_THIRDPARTY_COOKIE_NOT_VALID);
		// marketing cookies
		validator.setAllowedValuesMessagePattern("accettazione",
				marketingCookie, new String[]{"true", "false"},
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.COOKIES_MARKETING_COOKIE_NOT_VALID);
				
		return validator;

	}

}
