package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Enumerazione per il livello dei messaggi per gli ancillari. 
  ['info', 'warn', 'error']
*/
public enum AncillaryMessageLevel
{
	info,
	warn,
	error;

	public static final int SIZE = java.lang.Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static AncillaryMessageLevel forValue(int value)
	{
		return values()[value];
	}
}