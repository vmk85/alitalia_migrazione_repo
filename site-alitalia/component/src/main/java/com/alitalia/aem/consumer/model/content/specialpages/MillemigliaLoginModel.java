package com.alitalia.aem.consumer.model.content.specialpages;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;


import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.ssw.SabreSonicWebDeepLinkCostants;

@Model(adaptables={SlingHttpServletRequest.class})
public class MillemigliaLoginModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private MillemigliaLoginData millemigliaLoginData;
	
	
	private String sswRedemptionRedirectUrl;

	
	@PostConstruct
	protected void initModel() {
		try{
			logger.info("[MillemigliaLoginModel] initModel");
		
			super.initBaseModel(slingHttpServletRequest);
		
			// retrieve and remove BaggageClaimData from session if any
			millemigliaLoginData = (MillemigliaLoginData) 
					slingHttpServletRequest.getSession().getAttribute(MillemigliaLoginData.NAME);
			slingHttpServletRequest.getSession().removeAttribute(MillemigliaLoginData.NAME);
			
			sswRedemptionRedirectUrl = configuration.getSSWRedemptionDeepLink();
		}
		catch(Exception e){
			logger.error("Unexpected Exception: ", e);
			sswRedemptionRedirectUrl = "";
		}
		
	}

	public MillemigliaLoginData getMillemigliaLoginData() {
		return millemigliaLoginData;
	}

	public String getSswRedemptionRedirectUrl() {
		return sswRedemptionRedirectUrl;
	}

}
