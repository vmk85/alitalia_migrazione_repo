package com.alitalia.aem.consumer.booking.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.ResultBookingDetailsData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsSolutionPricingTaxData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.render.GenericPriceRender;
import com.alitalia.aem.ws.booking.searchservice.xsd12.ResultBookingDetails;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingAmericanTaxes extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private HashMap<String,String> taxes;
	

	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			initBaseModel(request);
			
			taxes = null;
			HashMap<String,BigDecimal> taxPrices = new HashMap<String,BigDecimal>();
			if (ctx.resultBookingDetails != null) {
				taxes = new HashMap<String,String>();
				if (ctx.resultBookingDetails.get("BookingDetails") instanceof ResultBookingDetailsData) {
					ResultBookingDetailsData bookingDetails = 
							(ResultBookingDetailsData) ctx.resultBookingDetails.get("BookingDetails");
					List<ResultBookingDetailsSolutionData> solutionField = bookingDetails.getSolutionField();
					if (solutionField != null) {
						for (ResultBookingDetailsSolutionData solutionData : solutionField) {
							List<ResultBookingDetailsSolutionPricingData> pricingField = solutionData.getPricingField();
							if (pricingField != null) {
								for (ResultBookingDetailsSolutionPricingData solutionPricingData : pricingField) {
									List<ResultBookingDetailsSolutionPricingTaxData> taxField = 
											solutionPricingData.getTaxField();
									if (taxField != null) {
										for (ResultBookingDetailsSolutionPricingTaxData solutionPricingTax : taxField) {
											if (solutionPricingTax.getSalePriceField() != null) {
												if (solutionPricingTax.getSalePriceField().getAmountField() != null) {
													String key = solutionPricingTax.getCodeField();
													BigDecimal previousValue = taxPrices.get(key);
													BigDecimal currentValue = solutionPricingTax.getSalePriceField().getAmountField();
													if (previousValue != null) {
														currentValue = currentValue.add(previousValue);
													}
													taxPrices.put(key, currentValue);
													GenericPriceRender gpr = new GenericPriceRender(currentValue , ctx.currency, ctx);
													taxes.put(key, gpr.getFare());
												} else {
													logger.error("Tax details for american country not found - solutionPricingTax.getSalePriceField().getAmountField() is NULL");
												}
											} else {
												logger.error("Tax details for american country not found - solutionPricingTax.getSalePriceField() is NULL");
											}
										}
									} else {
										logger.error("Tax details for american country not found - solutionPricingData.getTaxField() is NULL");
									}
								}
							} else {
								logger.error("Tax details for american country not found - solutionData.getPricingField() is NULL");
							}
						}
					} else {
						logger.error("Tax details for american country not found - bookingDetails.getSolutionField() is NULL");
					}
				} else {
					logger.error("Tax details for american country not found - BookingDetails is not instanceof ResultBookingDetails");
				}
			} else {
				logger.error("Tax details for american country not found - ctx.resultBookingDetails is NULL");
			}
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
		}
	}


	public HashMap<String,String> getTaxes() {
		return taxes;
	}
	
}