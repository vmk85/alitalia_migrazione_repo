/** Classe di log per i servizi MyAlitalia */

package com.alitalia.aem.consumer.myalitalia.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyAlitaliaUtilsServiceLog implements MyAlitaliaLogger {
    private static Logger logger = LoggerFactory.getLogger(MyAlitaliaUtilsServiceLog.class);

    MyAlitaliaErrorLog myAlitaliaErrorLog = new MyAlitaliaErrorLog();

    @Override
    public void error(String log) {
        logger.error(log);
        myAlitaliaErrorLog.error(log);
    }

    @Override
    public void debug(String log) {
        logger.debug(log);
    }

    @Override
    public void info(String log) {
        logger.info(log);
    }

    @Override
    public void trace(String log) {
        logger.trace(log);
    }
}
