package com.alitalia.aem.consumer.checkinrest.jobs;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkinrest.jobs.publisher.*;
import org.apache.sling.event.jobs.JobManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ggadaleta on 06/02/2018.
 */

public class CheckinJobPublisher {

    public static final String CASE_OFFLOAD = "offload";
    public static final String CASE_CLEAR_ANCILLARIES = "clear-ancillaries";
    public static final String CASE_OFFLOAD_AND_CLEAR_ANCILLARIES = "offload-and-clear-ancillaries";
    public static final String CASE_OFFLOAD_AND_CHECKIN = "offload-and-checkin";
    public static final String CASE_OFFLOAD_CHECKIN_AND_CLEAR_ANCILLARIES = "offload-checkin-and-clear-ancillaries";
    public static final String CASE_COMFORTSEAT = "clear-comfortseat-offload-and-checkin";

    protected static final Logger logger = LoggerFactory.getLogger(CheckinJobPublisher.class);

    public static boolean Publish(JobManager jobManager, String sessionId, CheckinSessionContext ctx) {

        String useCase = recognizeUseCase(ctx);
        try {

            JobPublisher jobPublisher;
            switch (useCase) {
                case CASE_OFFLOAD:
                    jobPublisher = new OffloadPassengerPublisher();
                    break;
                case CASE_CLEAR_ANCILLARIES:
                    jobPublisher = new ClearAncillariesPublisher();
                    break;
                case CASE_OFFLOAD_AND_CLEAR_ANCILLARIES:
                    jobPublisher = new OffloadPassengerAndClearAncillariesPublisher();
                    break;
                case CASE_OFFLOAD_AND_CHECKIN:
                    jobPublisher = new OffloadPassengerAndCheckinPublisher();
                    break;
                case CASE_OFFLOAD_CHECKIN_AND_CLEAR_ANCILLARIES:
                    jobPublisher = new OffloadPassengerCheckinAndClearAncillariesPublisher();
                    break;
                case CASE_COMFORTSEAT:
                    jobPublisher = new ClearComfortSeatPublisher();
                    break;
                default:
                    logger.error("no useCase recognized {}", useCase);
                    return false;
            }

            jobPublisher.setJobManager(jobManager);

            return jobPublisher.publish(sessionId, ctx);

        } catch (Exception e) {
            logger.error("[{}] ERROR factory CheckinJob {} " + e.toString(), sessionId, useCase != null ? useCase : "NOUSECASE");
            return false;
        }

    }

    /**
     * recognizeUseCase : riconosce le seguenti casistiche e instrada le azioni da intraprendere
     * Desiderata:
     * Passeggero con precheckin (senza carta di imbarco) che non completa il flusso di checkin (Non arrivo in ThankYou Page)
     * azione1) Eseguo l'offload dei Passeggeri (chiamata rest servizio "offload", per cancellare il precheckin)
     * Passeggero con precheckin (senza carta di imbarco) con servizio ancillary (bagaglio, lounge, fasttrack, insurance) aggiunto a carrello ma non pagato
     * azione1) Eseguo l'offload dei Passeggeri (chiamata rest servizio "offload", per cancellare il precheckin)
     * azione2) Elimino gli ancillary aggiunto a carrello ma non pagato (chiamata rest servizio "clearancillariessessionend", per rimuovere tutti i servizi ancillary non pagati)
     * Passeggero con carta di imbarco già emessa con servizio ancillary (bagaglio, lounge, fasttrack, insurance) aggiunto a carrello ma non pagato
     * azione1) Elimino gli ancillary aggiunto a carrello ma non pagato (chiamata rest servizio "clearancillariessessionend", per rimuovere tutti i servizi ancillary non pagati)
     * Passeggero con carta di imbarco già emessa che a distanza di ore ritorna su checkin e sceglie un posto comfort (a pagamento)
     * azione1) Offload dei Passeggeri (chiamata rest servizio "offload", per cancellare il precheckin che è stato fatto nel momento in cui ha scelto un posto comfort)
     * azione2) Checkin (chiamata rest servizio "checkinselectedpassenger", per effettuare checkin dei passeggeri selezionati sui voli disponibili)
     * Passeggero con carta di imbarco già emessa che a distanza di ore ritorna su checkin e sceglie un posto comfort (a pagamento) e abbandona la sessione prima del pagamento / non paga / non è arrivato in ThankYou Pag
     * azione1) Offload dei Passeggeri (chiamata rest servizio "offload", per cancellare il precheckin che è stato fatto nel momento in cui ha scelto un posto comfort)
     * azione2) Checkin (chiamata rest servizio "checkinselectedpassenger", per effettuare checkin dei passeggeri selezionati sui voli disponibili)
     * azione3) Elimino gli ancillary aggiunto a carrello ma non pagato (chiamata rest servizio "clearancillariessessionend", per rimuovere tutti i servizi ancillary non pagati)
     *
     * @param ctx
     * @return String useCase
     */
    private static String recognizeUseCase(CheckinSessionContext ctx) {

        Boolean almenoUnPrecheckin = false;
        Boolean almenoUnAncillare = false;
        Boolean almenoUnInsurance = false;
        Boolean almenoUnPostoComfort = false;



            //Stato passeggeri selezionati: controllo per precheckin
            for (int pnr = 0; pnr < ctx.pnrSelectedListDataRender.size(); pnr++) {

                try {
                    if (ctx.pnrSelectedListDataRender.get(pnr).getInsurance()) {
                        almenoUnAncillare = true;
                    }
                } catch (Exception e) {
                    logger.info("eccezione in CheckinJobPublisher, " + e.getMessage());
                    System.out.print(e.getStackTrace());
                } finally {

                //---- Flights ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
                for (int f = 0; f < ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().size(); f++) {
                    //---- Segments	---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
                    for (int s = 0; s < ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().size(); s++) {
                        //---- Passengers ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
                        if (ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getOpenCI()) {
                            for (int p = 0; p < ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().size(); p++) {
                                //---- SelectedPassengers ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
                                for (int sp = 0; sp < ctx.checkinSelectedPassengers.size(); sp++) {
                                    logger.debug(" - " + ctx.checkinSelectedPassengers.get(sp).getNome() + " - " + ctx.checkinSelectedPassengers.get(sp).getCognome() + " - " + ctx.checkinSelectedPassengers.get(sp).getPassengerID());
                                    if (ctx.checkinSelectedPassengers.get(sp).getPassengerID().equals(ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().get(p).getPassengerID())) {
                                        Passenger passenger = ctx.pnrSelectedListDataRender.get(pnr).getFlightsRender().get(f).getSegments().get(s).getPassengers().get(p);
                                        if (passenger.isPrecheckin()) {
                                            //precheckin effettuato per questo passeggero su questo volo su questa tratta
                                            logger.debug("Precheckin recognized for {}", passenger.getNome() + " " + passenger.getCognome() + " " + passenger.getPassengerID());
                                            almenoUnPrecheckin = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            }

            //TODO: refactor in caso di ancillari distribuiti su più di un PNR
            if (ctx.baggage != null) {
                if ("OK".equals(ctx.baggage.getOutcome())) {
                    //c'è bagaglio
                    logger.debug("Ancillary Baggage recognized {}", ctx.baggage.getOutcome());
                    almenoUnAncillare = true;
                }
            }

            //TODO: refactor in caso di ancillari distribuiti su più di un PNR
            if (ctx.fastTrack != null) {
                if ("OK".equals(ctx.fastTrack.getOutcome())) {
                    //c'è bagaglio
                    logger.debug("Ancillary FastTrack recognized {}", ctx.fastTrack.getOutcome());
                    almenoUnAncillare = true;
                }
            }

            //TODO: refactor in caso di ancillari distribuiti su più di un PNR
            if (ctx.lounge != null) {
                if ("OK".equals(ctx.lounge.getOutcome())) {
                    //c'è bagaglio
                    logger.debug("Ancillary Lounge recognized {}", ctx.lounge.getOutcome());
                    almenoUnAncillare = true;
                }
            }

            if (ctx.comfortSeatInCart) {
                //c'è almeno un posto comfort nel carrello
                logger.debug("Ancillary ComfortSeat recognized {}", ctx.comfortSeatInCart);
                almenoUnPostoComfort = true;
            }

            if (ctx.checkinPaymentDone) {
                //nothing to do
                return null;
            }

            if ((almenoUnPrecheckin && almenoUnAncillare) || (almenoUnPrecheckin && almenoUnAncillare && almenoUnPostoComfort) ||
                    (almenoUnPrecheckin && almenoUnPostoComfort)) {
                return CheckinJobPublisher.CASE_OFFLOAD_AND_CLEAR_ANCILLARIES;
            } else if (almenoUnAncillare && almenoUnPostoComfort) {
                return CheckinJobPublisher.CASE_COMFORTSEAT;
            } else if (almenoUnAncillare) {
                return CheckinJobPublisher.CASE_CLEAR_ANCILLARIES;
            } else if (almenoUnPrecheckin) {
                return CheckinJobPublisher.CASE_OFFLOAD;
            } else if (almenoUnPostoComfort) {
                return CheckinJobPublisher.CASE_COMFORTSEAT;
            } else {
                logger.debug("NO USE CASE RECOGNIZED");
                return "no-use-case";
            }
        }


}
