package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.MillemigliaSocialLoginData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.day.cq.i18n.I18n;

@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "millemigliasocialloginsubmit" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")

public class MillemigliaSocialLoginServlet extends GenericFormValidatorServlet {

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[MillemigliaSocialLoginServlet] validateForm");

		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			return validator.validate();

		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Error during parameters validation", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		logger.debug("[MillemigliaSocialLoginServlet] performSubmit");
		

	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[MillemigliaSocialLoginServlet] saveDataIntoSession");
		
		// create baggageClaimData object and save it into session
		MillemigliaSocialLoginData millemigliaSocialLoginData = new MillemigliaSocialLoginData();
		millemigliaSocialLoginData.setCode(request.getParameter("code"));
		millemigliaSocialLoginData.setPin(request.getParameter("pin"));
		request.getSession().setAttribute(MillemigliaSocialLoginData.NAME, millemigliaSocialLoginData);
		
		// go back to form with error
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		millemigliaSocialLoginData.setError(i18n.get(I18nKeySpecialPage.ERROR_MESSAGE_MILLEMIGLIA_LOGIN));
		

	}

	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {

		// get parameters from request
		String code = request.getParameter("code");
		String pin = request.getParameter("pin");
		

		// add validate conditions
		validator.addDirectConditionMessagePattern("code", code,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("pin", pin,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("pin", pin,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.PIN_ERROR_NOT_VALID, "isNumber");
		

		return validator;

	}

}


