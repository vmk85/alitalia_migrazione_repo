package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.data.home.PaymentComunicationBancaIntesaData;
import com.alitalia.aem.common.data.home.PaymentData;
import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.StateData;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GlobalCollectPaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingPaymentException;
import com.alitalia.aem.consumer.booking.BookingPhaseEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.InvoiceType;
import com.alitalia.aem.consumer.booking.model.PaymentType;
import com.alitalia.aem.consumer.carnet.CarnetSession;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.ValidationUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "bookingtypviewstatisticconsumer" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })})
@SuppressWarnings("serial")
public class BookingThankYouPageViewStatistic extends GenericBookingFormValidatorServlet {

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private BookingSession bookingSession;
	

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {

		logger.info("BookingThankYouPageViewStatistic. performSubmit executing");
		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		boolean result = true;
		try {
			BookingSessionContext ctx = (BookingSessionContext) 
					request.getSession(true).getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
			
			String showCallCenterMsgString = request.getParameter("showCallCenterMsg");
			String site = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
			boolean showCallCenterMsg = false;
			if (showCallCenterMsgString != null && !showCallCenterMsgString.equals("")) {
				showCallCenterMsg  = Boolean.parseBoolean(showCallCenterMsgString);
			}
			
			if (ctx != null) {
				bookingSession.thankYouPageViewStatistic(ctx, IDFactory.getTid(), showCallCenterMsg, site);
			}
			else {
				logger.error("Cannot find booking session context in session.");
				result = false;
			}
			// open JSON response
			json.object();
			json.key("result").value(result);
			json.endObject();

		} catch(Exception e) {
			logger.error("Error performing Thank You Page View statistic", e);
			result = false;
			try {
				// open JSON response
				json.object();
				json.key("result").value(result);
				json.endObject();
			} catch (JSONException e1) {
				logger.error("Error creating JSON ", e1);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
	}


	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}


	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		logger.info("BookingThankYouPageViewStatistic. validateForm executing");
		Validator v = new Validator();
		return v.validate();
	}
}
