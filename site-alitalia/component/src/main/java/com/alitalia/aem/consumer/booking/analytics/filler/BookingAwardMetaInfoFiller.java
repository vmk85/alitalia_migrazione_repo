package com.alitalia.aem.consumer.booking.analytics.filler;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.enumerations.MMTierCodeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingPhaseEnum;
import com.alitalia.aem.consumer.booking.BookingSearchCategoryEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.analytics.MetaInfo;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.model.SearchPassengersNumber;
import com.alitalia.aem.consumer.utils.DateUtils;

public class BookingAwardMetaInfoFiller implements MetaInfoFiller{
	
	private static final float FROM_MILLIS_TO_DAYS = 1000 * 60 * 60 * 24;
	
	private Calendar now;

	public BookingAwardMetaInfoFiller() {
		initFormats();
	}
	
	private void initFormats() {
		now = Calendar.getInstance();
		now.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
		
	}
	
	
	public MetaInfo fillInfo(BookingSessionContext ctx, int step, AlitaliaConfigurationHolder configurationHolder){
		MetaInfo info = new MetaInfo();
//		if(ctx.availableFlights != null){
//			info.bbxSessionId = ctx.availableFlights.getSessionId();
//		}
		String currencySymbol = ctx.currency;
		info.revenuePhase = computeAwardMetaStep(ctx.phase);
		info.step = step;
		switch(step){
		case 1:
			info.Pn = computeProductName(ctx.searchElements, true);
			info.tx_u = computePaxQuantity(ctx.searchPassengersNumber);
			info.ItinType = computeItinType(ctx.searchKind);
			info.tratta = computeTratta(ctx.searchElements);
			info.boApt = computeBoapt(ctx.searchElements);
			info.arApt = computeArapt(ctx.searchElements);
			info.NumWE = "0";
			if (ctx.searchElements != null && !ctx.searchElements.isEmpty()) {
				info.departureDate_eu = ctx.searchElements.get(0).getDepartureDate();
				info.departureDate = ctx.searchElements.get(0).getDepartureDate();
				info.deltaBoToday = DateUtils.computeDiffDays(now, 
						ctx.searchElements.get(0).getDepartureDate());
if(ctx.award){
					info.DateOfTravel = ctx.searchElements.get(0).getDepartureDate();
				}
				if (ctx.searchElements.size() > 1) {
					info.returnDate_eu = ctx.searchElements.get(1).getDepartureDate();
					info.returnDate = ctx.searchElements.get(1).getDepartureDate();
					info.deltaBoAr = DateUtils.computeDiffDays(ctx.searchElements.get(0).getDepartureDate()
							, ctx.searchElements.get(1).getDepartureDate());
					info.NumWE = String.valueOf(DateUtils.computeWeekEndsNumber(ctx.searchElements.get(0).getDepartureDate()
							, ctx.searchElements.get(1).getDepartureDate()));
				}
			}
			info.tx_e = "V";
			info.z_sitecode = ctx.site;
			info.z_tx_cur = currencySymbol != null && currencySymbol.length() > 0
						? currencySymbol : "EUR";
			info.Time = computeTime(now); 
			info.pn_gr = "Alitalia";
			info.pn_fa = "Alitalia";
			info.pn_sc = "Biglietto";
			info.pn_sku = "PNR";
			info.ServClass = "ECO";
			info.si_n = "Award2015";
			info.booking_award_control = "aem_2015";
			info.BFormItem = "result";
			info.pc = "BookingAward";
			info.USellingRevInc = "0";
			info.FareBasis = computeFareBasis(ctx.selectionRoutes, ctx.searchKind);
			info.SearchType = "Schedule";
			info.numAdults = ctx.searchPassengersNumber.getNumAdults();
			info.numYoung = ctx.searchPassengersNumber.getNumYoung();
			info.numChildren = ctx.searchPassengersNumber.getNumChildren();
			info.numInfant = ctx.searchPassengersNumber.getNumInfants();
			info.NetworkExt = computeNetworkExt(ctx.searchCategory);
			if(info.revenuePhase == 3){
				info.tx_e = "A";
				info.BFormItem = "ticketdetails";
			}
			break;
		case 2:
			info.ItinType = computeItinType(ctx.searchKind);
			info.si_n = "Award2015";
			info.booking_award_control = "aem_2015";
			info.BFormItem = "infopassengers";
			break;
		case 3:
			break;
		case 4:
			info.ItinType = computeItinType(ctx.searchKind);
			info.si_n = "Award2015";
			info.booking_award_control = "aem_2015";
			info.BFormItem = "purchaseticket";
			break;
		case 5:
			RoutesData prenotation = ctx.prenotation;
			info.Pn = computeProductName(ctx.searchElements, true);
			info.tx_u = computePaxQuantity(ctx.searchPassengersNumber);
			info.tratta = computeTratta(ctx.searchElements);
			info.acqtratta = "-";
			info.FareBasis = computeFareBasis(ctx.selectionRoutes, ctx.searchKind);
			info.NumWE = "0";
			info.ItinType = computeItinType(ctx.searchKind);
			if (ctx.flightSelections != null) {
				FlightSelection departure = ctx.flightSelections[0];
				Calendar depDate = now;
				if (departure != null) {
					DirectFlightData flight;
					if (departure.getFlightData() instanceof DirectFlightData) {
						flight = (DirectFlightData) departure.getFlightData();
					} else {
						flight = (DirectFlightData) ((ConnectingFlightData) departure.getFlightData()).getFlights().get(0);
					}
					depDate = flight.getDepartureDate();
					info.DateOfTravel = depDate;
					info.deltaBoToday = DateUtils.computeDiffDays(now, depDate);
				}
				if (ctx.flightSelections.length > 1) {
					FlightSelection back = ctx.flightSelections[1];
					if (back != null) {
						DirectFlightData flight;
						if (back.getFlightData() instanceof DirectFlightData) {
							flight = (DirectFlightData) back.getFlightData();
						} else {
							flight = (DirectFlightData) ((ConnectingFlightData) back.getFlightData()).getFlights()
									.get(0);
						}
						info.NumWE = String.valueOf(DateUtils.computeWeekEndsNumber(depDate , flight.getDepartureDate()));
						info.deltaBoAr = DateUtils.computeDiffDays(depDate, flight.getDepartureDate());
					}
				}
			}
			info.tx_e = "P";
			info.z_sitecode = ctx.site;
			info.z_tx_cur = currencySymbol != null && currencySymbol.length() > 0
						? currencySymbol : "EUR";
			info.Time = computeTime(now); 
			info.pn_gr = "Alitalia";
			info.pn_fa = "Alitalia";
			info.pn_sc = "Biglietto";
			info.pn_sku = "PNR";
			info.tx_id = now;
			info.tx_it = now;
			info.ServClass = "ECO";
			info.Pax_leg = "0";
			info.SearchType = "Schedule";
			info.BFormItem = "receipt";
			info.pc = "BookingAward";
			info.boApt = computeBoapt(ctx.searchElements);
			info.arApt = computeArapt(ctx.searchElements);
			info.si_n = "Award2015";
			info.booking_award_control = "aem_2015";
			info.USellingRevInc = "0";
			info.EResponse = "False";
			info.si_cs = "1";
			info.z_tx_eur = ctx.grossAmount != null ? ctx.grossAmount : BigDecimal.ZERO;
			info.z_tx_txn = ctx.grossAmount != null ? ctx.grossAmount : BigDecimal.ZERO;
			info.tx_s = ctx.grossAmount != null ? ctx.grossAmount : BigDecimal.ZERO;
			info.FareAmn = ctx.netAmount != null ? ctx.netAmount : BigDecimal.ZERO;
			info.TaxesAmn = ctx.totalTaxes != null ? ctx.totalTaxes : BigDecimal.ZERO;
			if (prenotation != null) {
				info.tx_i = prenotation.getPnr();
				if (prenotation.getPayment() != null) {
					info.CCType = (prenotation.getPayment().getProvider() != null 
							&& prenotation.getPayment().getProvider() instanceof PaymentProviderCreditCardData) 
							? ((PaymentProviderCreditCardData)prenotation.getPayment().getProvider()).getType().value() : "";
				}
			}
			info.OrdMMUserType = "Standard";
			info.awardUser = false;
			info.mmUser = false;
			info.standardUser = false;
			if ( ctx.loggedUserProfileData != null ){
				if(ctx.award){
					info.OrdMMUserType = "Awarded";
					info.awardUser = true;
				}
				else{
					info.OrdMMUserType = "MMPaid";
					info.mmUser = true;
				}
			} else{
				info.standardUser = true;
			}
			info.MMUserClub = computeTierName(ctx.loggedUserProfileData.getTierCode());
			info.numAdults = ctx.searchPassengersNumber.getNumAdults();
			info.numYoung = ctx.searchPassengersNumber.getNumYoung();
			info.numChildren = ctx.searchPassengersNumber.getNumChildren();
			info.numInfant = ctx.searchPassengersNumber.getNumInfants();
			info.NetworkExt = computeNetworkExt(ctx.searchCategory);
		}
		return info;
	}
	
	
	private String computeBoapt(List<SearchElement> searchElements) {
		String boapt = "";
		if (searchElements != null && !searchElements.isEmpty()) {
			boapt += searchElements.get(0).getFrom().getLocalizedCityName() 
					+ ", " + searchElements.get(0).getFrom().getLocalizedAirportName() 
					+ ", " + searchElements.get(0).getFrom().getLocalizedCountryName();
		}
		return boapt;
	}
	
	private String computeArapt(List<SearchElement> searchElements) {
		String arapt = "";
		if (searchElements != null && !searchElements.isEmpty()) {
			arapt += searchElements.get(0).getTo().getLocalizedCityName() 
					+ ", " + searchElements.get(0).getTo().getLocalizedAirportName() 
					+ ", " + searchElements.get(0).getTo().getLocalizedCountryName();
		}
		return arapt;
	}
	
	private String computeTratta(List<SearchElement> searchElements){
		String tratta = "";
		tratta = computeProductName(searchElements, false);
		return tratta;
	}
	
	
	private String computeTierName(MMTierCodeEnum tierCode) {
		String tierName = "";
		switch(tierCode){
		case BASIC:
			tierName = "Basic";
			break;
		case ULISSE:
			tierName = "Ulisse";
			break;
		case FRECCIA_ALATA:
			tierName = "FrecciaAlata";
			break;
		case PLUS:
			tierName = "FrecciaAlataPlus";
			break;
		case ELITE:
			tierName = "Elite";
			break;
		case ELITEPLUS:
			tierName = "ElitePlus";
			break;
		default:
			break;
		}
		return tierName;
	}
	
		
	private String computeProductName(List<SearchElement> searchElements, boolean includeTravelType){
		String pName = "";
		if (searchElements != null && !searchElements.isEmpty()) {
			pName += searchElements.get(0).getFrom().getLocalizedCityName() 
					+ ", " + searchElements.get(0).getFrom().getLocalizedAirportName() 
					+ ", " + searchElements.get(0).getFrom().getLocalizedCountryName();
			pName += "-" + searchElements.get(0).getTo().getLocalizedCityName() 
					+ ", " + searchElements.get(0).getTo().getLocalizedAirportName() 
					+ ", " + searchElements.get(0).getTo().getLocalizedCountryName();
			if(includeTravelType){
				pName += (searchElements.size() > 1 ? "-AwardRoundTrip" : "-AwardOneWay");
			}
		}
		return pName;
	}
	
	private String computeDepFlightNumber(FlightSelection[] selections){
		String fNum = "";
		if (selections != null) {
			FlightSelection departure = selections[0];
			if (departure != null) {
				DirectFlightData flight;
				if (departure.getFlightData() instanceof DirectFlightData) {
					flight = (DirectFlightData) departure.getFlightData();
				} else {
					flight = (DirectFlightData) ((ConnectingFlightData) departure.getFlightData()).getFlights().get(0);
				}
				fNum = flight.getFlightNumber();
				
			}
		}
		return fNum;
		
	}
	
	private String computeRetFlightNumber(FlightSelection[] selections){
		String fNum = "";
		if (selections != null && selections.length > 1){
			FlightSelection back = selections[1];
			if (back != null) {
				DirectFlightData flight;
				if (back.getFlightData() instanceof DirectFlightData) {
					flight = (DirectFlightData) back.getFlightData();
				} else {
					flight = (DirectFlightData) ((ConnectingFlightData) back.getFlightData()).getFlights()
							.get(0);
				}
				fNum = flight.getFlightNumber();
			}
		}
		return fNum;
		
	}
	
	private String computePaxQuantity(SearchPassengersNumber paxNumbers){
		String paxQt = "";
		if (paxNumbers != null) {
			paxQt = String.valueOf(paxNumbers.getNumAdults()
					+ paxNumbers.getNumYoung()
					+paxNumbers.getNumChildren()
					+paxNumbers.getNumInfants());
		}
		return paxQt;
	}
	
	private String computeItinType(BookingSearchKindEnum searchKind) {
		String travelType = "";
		if (searchKind != null) {
			travelType = searchKind == BookingSearchKindEnum.SIMPLE ? "AwardOneWay" : "AwardRoundTrip";
		}
		return travelType;
	}
	
	private String computeFareBasis(RoutesData route, BookingSearchKindEnum searchKind){
		String fareBasis = "";
		fareBasis = "MAWARD";
		if(!BookingSearchKindEnum.SIMPLE.equals(searchKind))
			fareBasis += ",MAWARD";
		return fareBasis;
	}
	
	
	private int computeAwardMetaStep(BookingPhaseEnum phase) {
		int step = 0;
		switch(phase){
		case INITIAL:
		case FLIGHTS_SEARCH:
			step = 2;
			break;
		case FLIGHTS_SELECTION:
			step = 3;
			break;
		case PASSENGERS_DATA:
			step = 4;
			break;
		case ANCILLARY:
		case PAYMENT:
			step = 5;
			break;
		case DONE:
			step = 6;
			break;
		}
		return step;
	}
	
	private String computeTime(Calendar now){
		int hh = now.get(Calendar.HOUR_OF_DAY);
		int mm = now.get(Calendar.MINUTE);
		String hour = String.valueOf(hh);
		String min = String.valueOf(mm);
		return (hh > 9 ? hour : "0" + hour) + ":" 
				+ (mm > 9 ? min : "0" + min);
	}
	
	private String computeNetworkExt(BookingSearchCategoryEnum searchCategory) {
		String networkExt = "";
		if(searchCategory != null){
			switch(searchCategory){
			case INTC:
			case INTC_PROMO:
				networkExt = "Intercontinental";
				break;
			case INTZ:
			case INTZ_PROMO:
				networkExt = "International";
				break;
			default:
				networkExt = "Domestic";
			}
		}
		return networkExt;
	}
	
	

}
