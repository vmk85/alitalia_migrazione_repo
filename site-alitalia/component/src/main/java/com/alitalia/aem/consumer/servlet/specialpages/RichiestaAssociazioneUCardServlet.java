package com.alitalia.aem.consumer.servlet.specialpages;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

import javax.servlet.Servlet;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.utils.AlitaliaCommonUtils;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.global.i18n.I18nKeySubscriptionSme;
import com.alitalia.aem.consumer.model.content.specialpages.RichiestaAssociazioneUCardData;
import com.alitalia.aem.consumer.model.content.substriptionsme.AdministratorAccount;
import com.alitalia.aem.consumer.model.content.substriptionsme.Area;
import com.alitalia.aem.consumer.model.content.substriptionsme.Company;
import com.alitalia.aem.consumer.model.content.substriptionsme.Preferences;
import com.alitalia.aem.consumer.model.content.substriptionsme.TravelAgency;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.ValidationUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = {"ucardassociationsubmit"}),
        @Property(name = "sling.servlet.methods", value = {"POST"}),
        @Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"})})
@SuppressWarnings("serial")
public class RichiestaAssociazioneUCardServlet extends GenericFormValidatorServlet {

    // Static Property For Mails
    private static final String COMPONENT_NAME = "richiesta_u_card";
    private static final String PARSYS_NAME = "page-component";
    private static final String MAIL_FROM_PROPERTY_NAME = "mailFrom";
    private static final String MAIL_TO_PROPERTY_NAME = "mailTo";
    private static final String MAIL_SUBJECT_PROPERTY_NAME = "subject";
    private static final String MAIL_FROM_DEFAULT = "azcorporate@dataar.it";
    private static final String MAIL_TO_DEFAULT = "azcorporate@dataar.it";
    private static final String MAIL_SUBJECT_DEFAULT = "UCARD";


    private static final String REQ_ATTR_CODICE_MILLEMIGLIA = "mm_code";
    private static final String REQ_ATTR_NOME = "appl_name";
    private static final String REQ_ATTR_COGNOME = "appl_surname";
    private static final String REQ_ATTR_EMAIL = "appl_email";
    private static final String REQ_ATTR_CODICE_UNIVERSITA = "university_code";
    private static final String REQ_ATTR_CHECK_1 = "auth-1";
    private static final String REQ_ATTR_CHECK_2 = "auth-2";

    private static final String CHECKBOX_VALUE = "yes";

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile BusinessLoginService businessLoginService;

    @Reference
    private AlitaliaConfigurationHolder configuration;

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request)
            throws IOException {
        logger.debug("[RichiestaAssociazioneUCardRequestServlet] validateForm");

        try {

            // validate parameters!
            Validator validator = setValidatorParameters(request,
                    new Validator());


			/*boolean captchaAlreadyValidated = (Boolean)(request.getSession(true).getAttribute("captchaAlreadyValidatedSME") != null ? request.getSession(true).getAttribute("captchaAlreadyValidatedSME") : Boolean.FALSE);
			if(!captchaAlreadyValidated){
				String jcaptchaResponse = request.getParameter("captcha");
				String captchaId = request.getSession().getId();
				Boolean isResponseCorrect = ValidationUtils.isCaptchaValid(jcaptchaResponse, captchaId);

				if (!isResponseCorrect){
					ResultValidation resultValidation = new ResultValidation();
					resultValidation.setResult(isResponseCorrect);
					resultValidation.addField("captcha", "Valore non valido");
					return resultValidation;
				} else {
					request.getSession(true).setAttribute("captchaAlreadyValidatedSME", true);
					logger.debug("Captcha check passed");
				}
			}*/
            return validator.validate();

        }

        // an error occurred...
        catch (Exception e) {
            logger.error("Errore durante la procedura di validazione dati "
                    + " Adesione Sme ", e);
            return null;
        }

    }

    @Override
    protected void performSubmit(SlingHttpServletRequest request,
                                 SlingHttpServletResponse response) throws IOException {
        logger.debug("[RichiestaAssociazioneUCardRequestServlet] performSubmit");

        boolean mailResult = sendMail(request);
        logger.debug("Mail Sended = " + mailResult);
        if (mailResult) {
            goBackSuccessfully(request, response);
        } else {
            throw new IOException("Service Error");
        }

    }

    /**
     * Set Validator parameters for validation
     *
     * @param request
     * @param validator
     */
    private Validator setValidatorParameters(SlingHttpServletRequest request,
                                             Validator validator) {

        RichiestaAssociazioneUCardData data = populateData(request);

        //### add validate conditions to Company
        validator.addDirectCondition(REQ_ATTR_CODICE_MILLEMIGLIA, data.getCodiceMillemiglia()
                , I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
        validator.addCrossCondition(REQ_ATTR_CODICE_MILLEMIGLIA, data.getCodiceMillemiglia(), "9"
                , I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "sizeIsLess");

        validator.addDirectCondition(REQ_ATTR_NOME, data.getNome()
                , I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
        validator.addDirectCondition(REQ_ATTR_NOME, data.getNome()
                , I18nKeySpecialPage.NAME_ERROR_NOT_VALID, "isAlphabeticWithAccentAndSpaces");

        validator.addDirectCondition(REQ_ATTR_COGNOME, data.getCognome()
                , I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
        validator.addDirectCondition(REQ_ATTR_COGNOME, data.getCognome()
                , I18nKeySpecialPage.SURNAME_ERROR_NOT_VALID, "isAlphabeticWithAccentAndSpaces");

        validator.addDirectCondition(REQ_ATTR_EMAIL, data.getEmail()
                , I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
        validator.addDirectCondition(REQ_ATTR_EMAIL, data.getEmail()
                , I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID, "isEmail");

        validator.addDirectCondition(REQ_ATTR_CODICE_UNIVERSITA, data.getcodiceUniversita()
                , I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

        validator.addCrossCondition(REQ_ATTR_CHECK_1, data.getCheck1(), CHECKBOX_VALUE
                , I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "areEqual");

        validator.addCrossCondition(REQ_ATTR_CHECK_2, data.getCheck2(), CHECKBOX_VALUE
                , I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "areEqual");

        return validator;
    }

    @Override
    protected void saveDataIntoSession(SlingHttpServletRequest request,
                                       Exception e) {
        logger.debug("[richiestaAssociazioneUCardRequestServlet] saveDataIntoSession");

        // create baggageClaimData object and save it into session
        RichiestaAssociazioneUCardData richiestaAssociazioneUCardRequestData = populateData(request);

        // go back to form with error
        Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
        ResourceBundle resourceBundle = request.getResourceBundle(locale);
        final I18n i18n = new I18n(resourceBundle);
        richiestaAssociazioneUCardRequestData.setError(i18n.get(I18nKeySpecialPage.ERROR_SERVICE));

        request.getSession().setAttribute(RichiestaAssociazioneUCardData.NAME,
                richiestaAssociazioneUCardRequestData);
    }

    /*
     * sendMail
     *
     */
    private boolean sendMail(SlingHttpServletRequest request) {
        logger.debug("[RichiestaAssociazioneUCardRequestServlet] sendMail");

        // current page
        Page currentPage = AlitaliaUtils.getPage(request.getResource());

        // email from
        String mailFrom = getComponentProperty(currentPage,
                PARSYS_NAME, COMPONENT_NAME, MAIL_FROM_PROPERTY_NAME);
        if (mailFrom == null) {
            mailFrom = MAIL_FROM_DEFAULT;
        }

        // email to
        String mailTo = getComponentProperty(currentPage,
                PARSYS_NAME, COMPONENT_NAME, MAIL_TO_PROPERTY_NAME);
        if (mailTo == null) {
            mailTo = MAIL_TO_DEFAULT;
        }

        // email subject
        String subject = getComponentProperty(currentPage,
                PARSYS_NAME, COMPONENT_NAME, MAIL_SUBJECT_PROPERTY_NAME);
        if (subject == null) {
            subject = MAIL_SUBJECT_DEFAULT;
        }

        // email body
        String messageText = generateBodyMail(request);

        logger.debug("[ RichiestaAssociazioneUCard] sendMail -- Email data: "
                + "[From: {" + mailFrom + "}] - [To: {" + mailTo + "}] - "
                + "[Subject: {" + subject + "}] - [Body: {" + messageText + "}]");

        // create email message object
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.setFrom(mailFrom);
        emailMessage.setTo(mailTo);
        emailMessage.setSubject(subject);
        emailMessage.setMessageText(messageText);
        emailMessage.setIsBodyHtml(false);
        emailMessage.setDeliveryNotification(DeliveryNotificationOptionsEnum.NONE);
        emailMessage.setPriority(MailPriorityEnum.NORMAL);

        // create AgencySendMailRequest
        AgencySendMailRequest mailRequest = new AgencySendMailRequest();
        mailRequest.setTid(IDFactory.getTid());
        mailRequest.setSid(IDFactory.getSid(request));
        mailRequest.setEmailMessage(emailMessage);

        // get AgencySendMailResponse
        AgencySendMailResponse mailResponse =
                businessLoginService.sendMail(mailRequest);

        // return status
        return mailResponse.isSendMailSuccessful();

    }


    /*
     * generateBodyMail
     *
     */
    private String generateBodyMail(SlingHttpServletRequest request) {
        logger.debug("[RichiestaAssociazioneUCardRequestServlet] generateBodyMail");

        String msg = "";
        RichiestaAssociazioneUCardData mmcorpReq = populateData(request);
        try {
            Map<String, Object> mapChild = AlitaliaCommonUtils.introspect(mmcorpReq);
            Map<String, Object> mapChild2 = new LinkedHashMap<String, Object>();
            String kC2 = "";
            for (String kC : mapChild.keySet()) {
                if (kC.equals("check1")) {
                    kC2 = "Presa visione regolamento";
                    mapChild2.put(kC2, mapChild.get(kC));
                } else if (kC.equals("check2")) {
                    kC2 = "Conferma dipendente università";
                    mapChild2.put(kC2, mapChild.get(kC));
                } else {
                    mapChild2.put(kC, mapChild.get(kC));
                }
            }
            mapChild = mapChild2;
            for (String kC : mapChild.keySet()) {
                if (kC.equalsIgnoreCase("error") || kC.equalsIgnoreCase("name") || kC.equalsIgnoreCase("class"))
                    continue;
                if (mapChild.get(kC) != null && !mapChild.get(kC).toString().isEmpty())
                    msg += kC + ": " + mapChild.get(kC) + "\n";
            }

        } catch (Exception e) {
            logger.error("Errore durante la creazione del corpo della mail", e);
        }

        return msg;

    }


    private RichiestaAssociazioneUCardData populateData(SlingHttpServletRequest request) {
        RichiestaAssociazioneUCardData richiestaAssociazioneUCardData = new RichiestaAssociazioneUCardData();

        //### get parameters from request
        richiestaAssociazioneUCardData.setCodiceMillemiglia(request.getParameter(REQ_ATTR_CODICE_MILLEMIGLIA));
        richiestaAssociazioneUCardData.setNome(request.getParameter(REQ_ATTR_NOME));
        richiestaAssociazioneUCardData.setCognome(request.getParameter(REQ_ATTR_COGNOME));
        richiestaAssociazioneUCardData.setEmail(request.getParameter(REQ_ATTR_EMAIL));
        richiestaAssociazioneUCardData.setcodiceUniversita(request.getParameter(REQ_ATTR_CODICE_UNIVERSITA));
        richiestaAssociazioneUCardData.setCheck1(request.getParameter(REQ_ATTR_CHECK_1));
        richiestaAssociazioneUCardData.setCheck2(request.getParameter(REQ_ATTR_CHECK_2));

        return richiestaAssociazioneUCardData;

    }


    /*
     * getComponentProperty
     *
     */
    private String getComponentProperty(Page ancestor, String parsysName,
                                        String compName, String propName) {
        logger.debug("[BaggageClaimServlet] getComponentProperty");

        Resource pageResource = ancestor.getContentResource();
        String prop = null;
        try {
            prop = pageResource.getChild(parsysName).getChild(compName)
                    .getValueMap().get(propName, String.class);
        } catch (Exception e) {
        }

        return prop;
    }

}