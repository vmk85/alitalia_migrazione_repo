package com.alitalia.aem.consumer.model.content.menu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MenuSectionItemsSummary {

	private Logger logger =
			LoggerFactory.getLogger(MenuSectionItemsSummary.class);

	@Self
	private SlingHttpServletRequest request;

	@Inject
	private Page currentPage;

	private MenuItem[] sectionItems;

	private static String TEMPLATE_NAME =
			"/apps/alitalia/templates/redazionale-principale-2-colonne";

	/**
	 * This method is used to get all the first and second level children of
	 * current page.
	 */
	@PostConstruct
	protected void initModel() {
		logger.debug("[MenuSectionItemsSummary] initModel");
		Resource resource = request.getResource();
		ResourceResolver resolver = resource.getResourceResolver();
		List<MenuItem> sectionItemsList = new ArrayList<MenuItem>();
		try {
			Iterator<Page> secondLevelPageIterator =
					currentPage.listChildren(new PageFilter(request));
			while (secondLevelPageIterator.hasNext()) {
				
				Page secondLevelPage = secondLevelPageIterator.next();
				
				// Recupero il path corretto del componente della pagina di
				// secondo livello
				String secondLevelPagePath = secondLevelPage.getPath() + "/"
						+ secondLevelPage.getContentResource().getName();

				MenuItem secondLevelPageItem = new MenuItem(
						secondLevelPage.getTitle(), secondLevelPage.getPath(),
						secondLevelPage.getDescription(), secondLevelPagePath,
						MenuItemType.LEAF, resolver.map(secondLevelPage.getPath()));

				List<MenuItem> thirdLevelPageList = new ArrayList<MenuItem>();
				Iterator<Page> thirdLevelPageIterator =
						secondLevelPage.listChildren(new PageFilter(request));
				while (thirdLevelPageIterator.hasNext()) {
					
					Page thirdLevelPage = thirdLevelPageIterator.next();

					if (thirdLevelPage.getProperties().get("cq:template", "")
							.equals(TEMPLATE_NAME)) {
						MenuItem thirdLevelPageItem = new MenuItem(
								thirdLevelPage.getTitle(),
								thirdLevelPage.getPath(),
								thirdLevelPage.getDescription(), null,
								MenuItemType.SECOND_LEVEL, resolver.map(secondLevelPage.getPath()));
						thirdLevelPageList.add(thirdLevelPageItem);
					}
				}
				
				secondLevelPageItem.setItemChildren(
						(MenuItem[]) thirdLevelPageList.toArray(
								new MenuItem[thirdLevelPageList.size()]));
				sectionItemsList.add(secondLevelPageItem);
			}
			
			sectionItems = (MenuItem[]) sectionItemsList.toArray(
					new MenuItem[sectionItemsList.size()]);
			
		} catch (Exception e) {
			logger.error("ERROR message: ",e);
		}

	}

	/**
	 * Returns all the first and second level children of current page
	 * 
	 * @return The menuItems
	 */
	public MenuItem[] getSectionItems() {
		return sectionItems;
	}

}