package com.alitalia.aem.consumer.checkin.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryUpgradeDetailData;
import com.alitalia.aem.common.data.home.checkin.CheckinCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillarySeatDetailData;
import com.alitalia.aem.consumer.checkin.CheckinPassenger;
import com.alitalia.aem.consumer.checkin.CheckinPassengerSeat;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinSeatMapTeaserPassengers extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private CheckinSession checkinSession;
	
	private List<CheckinPassenger> passengers;
	
	@PostConstruct
	protected void initModel() {
		
		try {
			initBaseModel(request);
			
			// Gestione TEASER
			List<CheckinPassengerData> checkedInPassengers = CheckinUtils.getCheckedInPassengers(ctx.passengers);
			Map<String, MmbAncillaryData> ancillariesAssignedSeats = checkinSession.getAncillariesAssignedSeats(ctx);
			Map<String, MmbAncillaryData> ancillariesAssignedUpgrade = checkinSession.getAllAncillariesIssuedUpgrade(ctx, true);
			
			passengers = new ArrayList<CheckinPassenger>();
			for (CheckinPassengerData passengerData : checkedInPassengers) {
				Map<String, CheckinPassengerSeat> seats = new HashMap<String, CheckinPassengerSeat>();
				for (CheckinCouponData coupon : passengerData.getCoupons()) {
					String eTicketPlusCoupon = coupon.getEticket() + "C" + coupon.getId();
					MmbAncillaryData ancillary = ancillariesAssignedUpgrade.get(eTicketPlusCoupon);
					boolean upgradeIssued = false;
					if(ancillary != null){
						CheckinAncillaryUpgradeDetailData ancillaryDetail = (CheckinAncillaryUpgradeDetailData) ancillary.getAncillaryDetail();
						if(ancillaryDetail != null){
							if (!coupon.getFlight().isBus()) {
								seats.put(coupon.getFlight().getFlightNumber(), 
										new CheckinPassengerSeat(StringUtils.stripStart(ancillaryDetail.getSeat(), "0")
												, ancillaryDetail.getCompartimentalClass()));
							}
							upgradeIssued = true;
						}
					}
					if (!upgradeIssued && (ancillary = ancillariesAssignedSeats.get(eTicketPlusCoupon)) != null) {
						MmbAncillarySeatDetailData ancillaryDetail;
						if ((ancillaryDetail = (MmbAncillarySeatDetailData) ancillary.getAncillaryDetail()) != null) {
							if (!coupon.getFlight().isBus()) {
								seats.put(coupon.getFlight().getFlightNumber(), 
										new CheckinPassengerSeat(StringUtils.stripStart(ancillaryDetail.getSeat(), "0"), coupon.getSeatClass()));
							}
						}
					}
				}
				CheckinPassenger passenger = checkinSession.toCheckinPassenger(passengerData);
				passenger.setSeats(seats);
				passengers.add(passenger);
			}
			
		} catch (Exception e) {
			logger.error("Unexpected error:", e);
			throw e;
		}
	}
	
	public List<CheckinPassenger> getPassengers() {
		return passengers;
	}
}
