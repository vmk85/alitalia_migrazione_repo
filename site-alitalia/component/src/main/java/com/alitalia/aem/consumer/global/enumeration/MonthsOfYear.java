package com.alitalia.aem.consumer.global.enumeration;

import java.util.ArrayList;

public enum MonthsOfYear {
	
	JANUARY("january"),
	FEBRUARY("february"),
	MARCH("march"),
	APRIL("april"),
	MAY("may"),
	JUNE("june"),
	JULY("july"),
	AUGUST("august"),
	SEPTEMBER("september"),
	OCTOBER("october"),
	NOVEMBER("november"),
	DECEMBER("december");

	private final String value;

	MonthsOfYear(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	public String getI18nValue() {
		return "common.monthsOfYear." + value;
	}
	
	public static ArrayList<String> listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (MonthsOfYear c: MonthsOfYear.values()) {
			result.add(c.value);
		}
		return result;
	}
	
	public static ArrayList<String> listI18nValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (MonthsOfYear c: MonthsOfYear.values()) {
			result.add(c.getI18nValue());
		}
		return result;
	}

	public static MonthsOfYear fromValue(String v) {
		for (MonthsOfYear c: MonthsOfYear.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
