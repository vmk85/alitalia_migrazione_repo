package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Enumerazione sulla tipologia cabina
*/
public enum CabinType
{
	Permitted,
	Preferred;

	public static final int SIZE = java.lang.Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static CabinType forValue(int value)
	{
		return values()[value];
	}
}