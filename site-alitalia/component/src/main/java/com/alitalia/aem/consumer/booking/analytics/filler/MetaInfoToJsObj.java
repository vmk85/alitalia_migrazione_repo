package com.alitalia.aem.consumer.booking.analytics.filler;

import com.alitalia.aem.consumer.booking.analytics.MetaInfo;

public interface MetaInfoToJsObj {
	
	public String toJSObj(MetaInfo info, boolean trailingSemicolon);
	
}
