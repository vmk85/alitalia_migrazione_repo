package com.alitalia.aem.consumer.checkin.model;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.util.List;

@Model(adaptables = {SlingHttpServletRequest.class})
public class CheckinCheckNavigation1 extends GenericCheckinModel {

    @Self
    private SlingHttpServletRequest request;

    @Inject
    private SlingHttpServletResponse response;

    @Inject
    private AlitaliaConfigurationHolder configuration;

    private int navbarStep;

    @PostConstruct
    protected void initModel() throws IOException {

        try {
            initBaseModel(request);
        } catch (Exception e) {
            logger.error("", e);
        }

        String path = request.getRequestURL().toString();

        if (path.endsWith(configuration.getCheckinFlightListPage())) {
            navbarStep = 1;
        } else if (path.endsWith(configuration.getCheckinPassengersPage())) {
            navbarStep = 2;
        } else if (path.endsWith(configuration.getCheckinDangerousGoodsPage())) {
            navbarStep = 3;
        } else if (path.endsWith(configuration.getCheckinSeatMapPage())) {
            navbarStep = 4;
        } else if (path.endsWith(configuration.getCheckinAncillaryPage())) {
            navbarStep = 5;
        } else if (path.endsWith(configuration.getCheckinPaymentPage())) {
            navbarStep = 5;
        } else {
            navbarStep = 0;
        }
    }

    public int getNavbarStep() {
        return navbarStep;
    }

    public void setNavbarStep(int navbarStep) {
        this.navbarStep = navbarStep;
    }

}
