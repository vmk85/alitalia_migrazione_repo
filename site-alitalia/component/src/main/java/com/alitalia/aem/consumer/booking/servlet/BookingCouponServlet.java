package com.alitalia.aem.consumer.booking.servlet;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingcouponconsumer" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class BookingCouponServlet extends GenericBookingFormValidatorServlet {
	
	@Reference
	private AlitaliaConfigurationHolder alitaliaConfiguration;
	
	@Reference
	private BookingSession bookingSession;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		Validator validator = new Validator();
		validator.addDirectConditionMessagePattern("inputEcoupon", request.getParameter("inputEcoupon"),
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "Codice", "isNotEmpty");
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws Exception {
		
		BookingSessionContext ctx = getBookingSessionContext(request);
		String couponCode = request.getParameter("inputEcoupon");
		Boolean isFanPlayer = "true".equals(request.getParameter("fanplayr"));
		bookingSession.applyCoupon(ctx, couponCode, isFanPlayer);
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		json.key("result").value("OK");
		json.key("fanplayr").value(isFanPlayer);
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return alitaliaConfiguration;
	}
	
}
