package com.alitalia.aem.consumer.global.enumeration;

import java.util.ArrayList;

public enum RichiediMigliaNumberCode {
	N055("055"),
	N005("005"),
	N006("006"),
	N012("012"),
	N057("057"),
	N064("064"),
	N074("074"),
	N081("081"),
	N139("139"),
	N180("180"),
	N230("230"),
	N555("555"),
	N706("706"),
	N784("784"),
	N867("867"),
	N996("996"),
	N607("607"),
	N127("127");

	private final String value;

	RichiediMigliaNumberCode(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	public static ArrayList<String> listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (RichiediMigliaNumberCode c: RichiediMigliaNumberCode.values()) {
			result.add(c.value);
		}
		return result;
	}

	public static RichiediMigliaNumberCode fromValue(String v) {
		for (RichiediMigliaNumberCode c: RichiediMigliaNumberCode.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
