package com.alitalia.aem.consumer.checkinrest.render;

import java.util.ArrayList;
import java.util.List;

public class SectorRender {
	
	private String startRow;
    private String endRow;
    private String prevStartRow;
    private String prevEndRow;
	private String nextStartRow;
    private String nextEndRow;
	private String structureMap;
	
    private ArrayList<THead> thead;
    
    private ArrayList<ArrayList> TBody;
	
	
    public String getStartRow() {
		return startRow;
	}

	public void setStartRow(String startRow) {
		this.startRow = startRow;
	}

	public String getEndRow() {
		return endRow;
	}

	public void setEndRow(String endRow) {
		this.endRow = endRow;
	}

    public String getPrevStartRow() {
        return prevStartRow;
    }

    public void setPrevStartRow(String prevStartRow) {
        this.prevStartRow = prevStartRow;
    }

    public String getPrevEndRow() {
        return prevEndRow;
    }

    public void setPrevEndRow(String prevEndRow) {
        this.prevEndRow = prevEndRow;
    }

    public String getNextStartRow() {
        return nextStartRow;
    }

    public void setNextStartRow(String nextStartRow) {
        this.nextStartRow = nextStartRow;
    }

    public String getNextEndRow() {
        return nextEndRow;
    }

    public void setNextEndRow(String nextEndRow) {
        this.nextEndRow = nextEndRow;
    }

	public String getStructureMap() {
		return structureMap;
	}

	public void setStructureMap(String structureMap) {
		this.structureMap = structureMap;
	}

	public ArrayList<THead> getThead() {
		return thead;
	}

	public void setThead(ArrayList<THead> thead) {
		this.thead = thead;
	}

	public ArrayList<ArrayList> getTBody() {
		return TBody;
	}

	public void setTBody(ArrayList<ArrayList> tBody) {
		TBody = tBody;
	}

}
