package com.alitalia.aem.consumer.mmb.servlet;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.messages.home.MmbSendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbSendEmailResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeyComponents;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.alitalia.aem.web.component.managemybooking.delegate.ManageMyBookingDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "specialassistanceservlet" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class SpecialAssistanceServlet extends GenericMmbFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile MmbSession mmbSession;
	
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ManageMyBookingDelegate mmbDelegate;
	
	private static final String KEYVALUE_SEPARATOR = ":";
	private static final String ELEMENT_SEPARATOR = System.getProperty("line.separator");
	private static final String KEY_PREFIX = "specialAssistance.";
	private static final String KEY_SEPARATOR = ",";
	private static final String MAIL_FROM = "AlitaliaSpecialServices@alitalia.com";
	private static final String MAIL_TO = "backoffice.alitalia@abramo.com";
	//private static final String MAIL_TO = "m.vitagliano@reply.it";
	private static final String MAIL_SUBJECT_PREFIX = "Special Assistance + ";
	private static final String MAIL_SUBJECT_SEPARATOR = "&";
	private static final String MAIL_TO_SEPARATOR = ";";
	

	private static final String ATTR_TICKET= "etktNumber";
	private static final String ATTR_PNR = "pnrCode";
	private static final String ATTR_NAME = "name";
	private static final String ATTR_LASTNAME = "lastname";
	private static final String ATTR_CHECK_PASSENGER = "checkPassenger";
	private static final String ATTR_CHECK_FLIGHT = "checkFlights[]";
	private static final String ATTR_TELEPHONE = "phoneNumber";
	private static final String ATTR_EMAIL = "email";
	private static final String ATTR_NOTES = "notes";
	private static final String ATTR_SPECIAL_ASSISTANCE = "specialAssistance";
	private static final String ATTR_HEIGHT = "wheelchairHeight";
	private static final String ATTR_WEIGHT = "wheelchairWeight";
	private static final String ATTR_LENGTH = "wheelchairLength";
	private static final String ATTR_WIDTH = "wheelchairWidth";
	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		logger.debug("[SpecialAssistanceServlet] validateForm");
		Validator validator = new Validator();
		
		// get parameters from request
		String check_passenger = request.getParameter(ATTR_CHECK_PASSENGER);
		String[] check_flights = request.getParameterValues(ATTR_CHECK_FLIGHT);
		String phonenumber = request.getParameter(ATTR_TELEPHONE);
		String email = request.getParameter(ATTR_EMAIL);
		String specialAssistance = request.getParameter(ATTR_SPECIAL_ASSISTANCE);
				
		validator.addDirectConditionMessagePattern(ATTR_CHECK_PASSENGER, check_passenger,
			I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
			I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern(ATTR_SPECIAL_ASSISTANCE, specialAssistance,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
				
		validator.addDirectConditionMessagePattern(ATTR_TELEPHONE, phonenumber,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.PHONE_NUMBER_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern(ATTR_TELEPHONE, phonenumber,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.PHONE_NUMBER_ERROR_NOT_VALID, "isNumber");
				
		validator.addDirectConditionMessagePattern(ATTR_EMAIL, email,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.EMAIL_ERROR_EMPTY, "isNotEmpty");
		
		validator.addDirectConditionMessagePattern(ATTR_EMAIL, email,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID, "isEmail");
				
		//CHECK FOR FLIGHT SELECT (can be null or empty vector, it depends on the bwowser9
		if (check_flights == null) {
			validator.addDirectConditionMessagePattern(ATTR_CHECK_FLIGHT, null,
					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
					I18nKeySpecialPage.EMAIL_ERROR_EMPTY, "isNotEmpty");
		}
		else {
			String checkFlightLength=check_flights.length+"";
			validator.addCrossConditionMessagePattern(ATTR_CHECK_FLIGHT + "[]", checkFlightLength,  "0",
					I18nKeyCommon.MESSAGE_INVALID_FIELD, I18nKeySpecialPage.REASON_ERROR_EMPTY, "isMoreThan");
		}
		
				
		String[] wheelchairKeys = configuration.getSpecialAssistanceWheelchairKeys().split(KEY_SEPARATOR);
		String specialAssistanceKey = request.getParameter(ATTR_SPECIAL_ASSISTANCE);
				
		boolean extended = isWheelChairAssistance(specialAssistanceKey, wheelchairKeys);
				
		if (extended) {
			String height = request.getParameter(ATTR_HEIGHT);
			String weigth = request.getParameter(ATTR_WEIGHT);
			String lenght = request.getParameter(ATTR_LENGTH);
			String width = request.getParameter(ATTR_WIDTH);
					
			validator.addDirectConditionMessagePattern(ATTR_HEIGHT, height,
					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
					I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
			validator.addDirectConditionMessagePattern(ATTR_HEIGHT, height,
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					I18nKeyComponents.MMB_MANAGE_NUMBER_NOT_VALID, "isNumberWithDecimalPrecision");
					
			validator.addDirectConditionMessagePattern(ATTR_WEIGHT, weigth,
					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
					I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
			validator.addDirectConditionMessagePattern(ATTR_WEIGHT, weigth,
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					I18nKeyComponents.MMB_MANAGE_NUMBER_NOT_VALID, "isNumberWithDecimalPrecision");
					
			validator.addDirectConditionMessagePattern(ATTR_LENGTH, lenght,
					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
					I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
			validator.addDirectConditionMessagePattern(ATTR_LENGTH, lenght,
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					I18nKeyComponents.MMB_MANAGE_NUMBER_NOT_VALID, "isNumberWithDecimalPrecision");
					
			validator.addDirectConditionMessagePattern(ATTR_WIDTH, width,
					I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
					I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
			validator.addDirectConditionMessagePattern(ATTR_WIDTH, width,
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					I18nKeyComponents.MMB_MANAGE_NUMBER_NOT_VALID, "isNumberWithDecimalPrecision");
		}
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
	
		logger.debug("[SpecialAssistanceServlet] performSubmit");
		String[] wheelchairKeys = configuration.getSpecialAssistanceWheelchairKeys().split(KEY_SEPARATOR);
		String key = request.getParameter(ATTR_SPECIAL_ASSISTANCE);
		
		boolean extended = isWheelChairAssistance(key, wheelchairKeys);
		
		boolean mailResult = sendMail(request, extended);
		if (mailResult) {
			TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
			response.setContentType("application/json");
			json.object();
			json.key("result").value("OK");
			json.endObject();
		}
		else {
			throw new GenericFormValidatorServletException(true, "Mail not sended : Service Error");
		}
		
		
	}
	
	
	
	private boolean sendMail(SlingHttpServletRequest request, boolean extended) {
		String name = request.getParameter(ATTR_NAME);
		String lastname = request.getParameter(ATTR_LASTNAME);
		String customerMail = request.getParameter(ATTR_EMAIL);
		
		MmbSendEmailRequest mailRequest = new MmbSendEmailRequest();
		mailRequest.setTid(IDFactory.getTid());
		mailRequest.setSid(IDFactory.getSid(request));
		mailRequest.setSender(MAIL_FROM);
		mailRequest.setRecipient(MAIL_TO + MAIL_TO_SEPARATOR + customerMail);
		mailRequest.setMailSubject(MAIL_SUBJECT_PREFIX + name + MAIL_SUBJECT_SEPARATOR + lastname);
		String emailText = generateBodyMail(request, extended);
		mailRequest.setMailBody(emailText);
		mailRequest.setHtmlBody(false);
		
		MmbSendEmailResponse mailResponse =  mmbDelegate.sendMail(mailRequest);

		return mailResponse.isMailSent();
	}
	
	private String generateBodyMail(SlingHttpServletRequest request, boolean extended) {
		boolean hasNotes = true;
		String[] check_flights = request.getParameterValues(ATTR_CHECK_FLIGHT);
		String[][] data;
		int dimForm;
		if (extended) {
			dimForm = check_flights.length + 10;
		}
		else {
			dimForm = check_flights.length + 6;
		}
		if (request.getParameter(ATTR_NOTES) == null || "".equals(request.getParameter(ATTR_NOTES))) {
			dimForm--;
			hasNotes = false;
		}
		
		data = new String[dimForm][2];
		
		// recupero le chiavi
		data[0][0] =  request.getParameter("KeyField0");
		data[1][0] =  request.getParameter("KeyField1");
		data[2][0] =  request.getParameter("KeyField2");
		int k;
		for (k=0; k<check_flights.length; k++) {
			data[k+3][0] =  request.getParameter("KeyField3");
		}
		int index = check_flights.length + 3;
		data[index][0] =  request.getParameter("KeyField4");
		data[index+1][0] =  request.getParameter("KeyField5");
		if (extended) {
			data[index+2][0] = request.getParameter("KeyField6");
			data[index+3][0] = request.getParameter("KeyField7");
			data[index+4][0] = request.getParameter("KeyField8");
			data[index+5][0] = request.getParameter("KeyField9");
			if (hasNotes) {
				data[index+6][0] = request.getParameter("KeyField10");
			}
			
		}
		else {
			if (hasNotes) {
				data[index+2][0] = request.getParameter("KeyField10");
			}
			
		}
		
		// associazione chiave valore
		
		data[0][1] = request.getParameter(ATTR_TICKET);
		data[1][1] = request.getParameter(ATTR_PNR);
		data[2][1] = request.getParameter(ATTR_NAME) + " " + request.getParameter(ATTR_LASTNAME);
		
		for (k=0; k<check_flights.length; k++) {
			data[k+3][1] = check_flights[k];
		}
		index = check_flights.length + 3;
		data[index][1] = request.getParameter(ATTR_EMAIL) + "/" + request.getParameter(ATTR_TELEPHONE);
		
		I18n i18n = getI18n(request);
		String specialAssistance = i18n.get(KEY_PREFIX + request.getParameter(ATTR_SPECIAL_ASSISTANCE));
		data[index+1][1] = specialAssistance;
		
		if (extended) {
			data[index+2][1] = request.getParameter(ATTR_LENGTH);
			data[index+3][1] = request.getParameter(ATTR_WIDTH);
			data[index+4][1] = request.getParameter(ATTR_HEIGHT);
			data[index+5][1] = request.getParameter(ATTR_WEIGHT);
			if (hasNotes) {
				data[index+6][1] = request.getParameter(ATTR_NOTES);
			}
			
		}
		else {
			if (hasNotes) {
				data[index+2][1] = request.getParameter(ATTR_NOTES);
			}
			
		}
		
		//  genero il testo del messaggio
		String emailText = "";
		for (k=0; k<dimForm; k++) {
			emailText = emailText + data[k][0] + " " + KEYVALUE_SEPARATOR + " " + data[k][1];
			if (k<dimForm-1) {
				emailText = emailText + ELEMENT_SEPARATOR;
			}
			
		}
		return emailText;
		
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
	private boolean isWheelChairAssistance(String key, String[] keys) {
		for (String wheelchairKey : keys) {
			if (wheelchairKey.equals(key)) {
				return true;
			}
		}
		return false;
	}
	
	private I18n getI18n(SlingHttpServletRequest request) {
		
		
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		
		return new I18n(resourceBundle);
	}
	
}
