package com.alitalia.aem.consumer.booking.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.PassengerApisSecureFlightInfoData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PassengerSecureFlightInfoData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.render.DirectFlightDataRender;
import com.alitalia.aem.consumer.booking.render.SelectedFlightInfoRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingSeatmapFeedback extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private BookingSession bookingSession;
	
	private SelectedFlightInfoRender[] selectedFlightInfo;
	private String[] sliceName;
	private String[][] seats;
	private String[] numberOfSeatForPassenger;
	private String[] passengers;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			
			initBaseModel(request);
			
			selectedFlightInfo = new SelectedFlightInfoRender[ctx.flightSelections.length];
			for (int i = 0; i<ctx.flightSelections.length; i++) { 
				selectedFlightInfo[i] = new SelectedFlightInfoRender(ctx.flightSelections[i].getFlightData(), i18n, ctx.market);
			}
			
			int numberOfPassengersNoInfant = 0;
			for( PassengerBaseData pax : ctx.selectionRoutes.getPassengers()) {
				if (!(pax instanceof InfantPassengerData)) {
					numberOfPassengersNoInfant++;
				}
			}
			
			sliceName = computeSliceName(ctx);
			seats = new String[ctx.flightSelections.length][];
			for (int i = 0; i < ctx.flightSelections.length; i++) {
				seats[i] = computeSeatsString(ctx,i);
			}
			
			numberOfSeatForPassenger = new String[numberOfPassengersNoInfant];
			passengers = new String[numberOfPassengersNoInfant];
			for(int i = 0; i < ctx.selectionRoutes.getPassengers().size(); i++) {
				String numberSeat = computeNumberSeatForPassenger(ctx.selectionRoutes.getPassengers(), i);
				if (numberSeat != null) {
					numberOfSeatForPassenger[i] = numberSeat;
				}
				String passengerName = computePassengerName(ctx.selectionRoutes.getPassengers(),i);
				if (passengerName != null) {
					passengers[i] = passengerName;
				}
			}
			
		} catch (Exception e) {
			logger.error("Unexpected error", e);
			throw e;
		}
	}
	
	public SelectedFlightInfoRender[] getSelectedFlightInfo() {
		return selectedFlightInfo;
	}

	public String[] getSliceName() {
		return sliceName;
	}

	public String[][] getSeats() {
		return seats;
	}

	public String[] getNumberOfSeatForPassenger() {
		return numberOfSeatForPassenger;
	}

	public String[] getPassengers() {
		return passengers;
	}

	
	/* private methods */ 
	
	private String computeNumberSeatForPassenger(List<PassengerBaseData> passengersList, int i) {
		PassengerBaseData passenger = passengersList.get(i);
		if (passenger instanceof AdultPassengerData) {
			AdultPassengerData typedPassenger = (AdultPassengerData) passenger;
			if (typedPassenger.getPreferences() == null){
				return "0";
			}
			if (typedPassenger.getPreferences().getSeatPreferences()==null 
					|| typedPassenger.getPreferences().getSeatPreferences().isEmpty()) {
				return "0";
			}
			return Integer.toString(typedPassenger.getPreferences().getSeatPreferences().size());
		} else if (passenger instanceof ChildPassengerData) {
			ChildPassengerData typedPassenger = (ChildPassengerData) passenger;
			if (typedPassenger.getPreferences() == null){
				return "0";
			}
			if (typedPassenger.getPreferences().getSeatPreferences()==null 
					|| typedPassenger.getPreferences().getSeatPreferences().isEmpty()) {
				return "0";
			}
			return Integer.toString(typedPassenger.getPreferences().getSeatPreferences().size());
		}
		return null;
	}

	private String computePassengerName(List<PassengerBaseData> passengersList,
			int i) {
		PassengerBaseData passenger = passengersList.get(i);
		if (passenger instanceof AdultPassengerData) {
			AdultPassengerData typedPassenger = (AdultPassengerData) passenger;
			if (typedPassenger.getLastName() == null) {
				return null;
			}
			String secondName = "";
			if (typedPassenger.getInfo() instanceof PassengerApisSecureFlightInfoData) {
				PassengerApisSecureFlightInfoData apisSecureFlightInfo = (PassengerApisSecureFlightInfoData) typedPassenger.getInfo();
				if (apisSecureFlightInfo.getSecondName() != null) {
					secondName = " " + apisSecureFlightInfo.getSecondName() + " ";
				} 
			} else if (typedPassenger.getInfo() instanceof PassengerSecureFlightInfoData) {
				PassengerSecureFlightInfoData secureFlightInfo = (PassengerSecureFlightInfoData) typedPassenger.getInfo();
				if (secureFlightInfo.getSecondName() != null ){
					secondName = " " + secureFlightInfo.getSecondName() + " ";
				}
			}
			String infant = computeReferedInfant(passengersList,i);
			return typedPassenger.getLastName() + " " + typedPassenger.getName() + secondName + infant;
		} else if (passenger instanceof ChildPassengerData) {
			ChildPassengerData typedPassenger = (ChildPassengerData) passenger;
			if (typedPassenger.getLastName() == null) {
				return null;
			}
			String secondName = "";
			if (typedPassenger.getInfo() instanceof PassengerApisSecureFlightInfoData) {
				PassengerApisSecureFlightInfoData apisSecureFlightInfo = (PassengerApisSecureFlightInfoData) typedPassenger.getInfo();
				if (apisSecureFlightInfo.getSecondName() != null) {
					secondName = " " + apisSecureFlightInfo.getSecondName() + " ";
				} 
			} else if (typedPassenger.getInfo() instanceof PassengerSecureFlightInfoData) {
				PassengerSecureFlightInfoData secureFlightInfo = (PassengerSecureFlightInfoData) typedPassenger.getInfo();
				if (secureFlightInfo.getSecondName() != null ){
					secondName = " " + secureFlightInfo.getSecondName() + " ";
				}
			}
			return typedPassenger.getLastName() + " " + typedPassenger.getName() + secondName;
		}
		return null;
	}

	private String computeReferedInfant(List<PassengerBaseData> passengers,
			int indexPassenger) {
		boolean hasReferedInfant = bookingSession.computeReferedInfant(passengers, indexPassenger) != -1;
		if (hasReferedInfant) {
			return "</span><span class=\"plus\"> +" + i18n.get("booking.ancillary.posto.neonato.label") + "</span>";
		}
		return "";
	}

	private String[] computeSliceName(BookingSessionContext ctx) {
		String[] sliceName = new String[ctx.flightSelections.length];
		if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
			for ( int i = 0; i < ctx.flightSelections.length; i++) {
				sliceName[i] = obtainTitleBySliceIndex(i);
			}
		} else {
			for ( int i = 0; i < ctx.flightSelections.length; i++) {
				sliceName[i] = obtainARMessage(i);
			}
		}
		return sliceName;
	}

	private String[] computeSeatsString(BookingSessionContext ctx, int indexRoute) {
		
		String[] seats = new String[this.selectedFlightInfo[indexRoute].getUsedFligthsInSelection().size()];
		
		int i = 0;
		for (DirectFlightDataRender directFlight : selectedFlightInfo[indexRoute].getUsedFligthsInSelection()) {
			String flightNumber = directFlight.getDirectFlightData().getFlightNumber();
			for ( PassengerBaseData pax : ctx.selectionRoutes.getPassengers()) {
				if (pax instanceof AdultPassengerData) {
					AdultPassengerData typedPassenger = (AdultPassengerData) pax;
					if (typedPassenger.getPreferences() != null) {
						if(typedPassenger.getPreferences().getSeatPreferences() != null && !typedPassenger.getPreferences().getSeatPreferences().isEmpty()) {
							for (SeatPreferencesData seatPreference : typedPassenger.getPreferences().getSeatPreferences()){
								if (seatPreference.getFlightNumber() != null && seatPreference.getFlightNumber().equals(flightNumber)) {
									if (seats == null || seats[i] == null || seats[i].equals("")) {
										seats[i] = seatPreference.getRow() + seatPreference.getNumber();
									} else {
										seats[i] += " / " + seatPreference.getRow() + seatPreference.getNumber();
									}
								}
							}
						}
					}
				}
				if (pax instanceof ChildPassengerData) {
					ChildPassengerData typedPassenger = (ChildPassengerData) pax;
					if (typedPassenger.getPreferences() != null) {
						if(typedPassenger.getPreferences().getSeatPreferences() != null && !typedPassenger.getPreferences().getSeatPreferences().isEmpty()) {
							for (SeatPreferencesData seatPreference : typedPassenger.getPreferences().getSeatPreferences()){
								if (seatPreference.getFlightNumber() != null && seatPreference.getFlightNumber().equals(flightNumber)) {
									if (seats == null || seats[i] == null || seats[i].equals("")) {
										seats[i] = seatPreference.getRow() + seatPreference.getNumber();
									} else {
										seats[i] += " / " + seatPreference.getRow() + seatPreference.getNumber();
									}
								}
							}
						}
					}
				}
			}
			i++;
		}
		return seats;
	}
	
	
	private String obtainARMessage(int i) {
		switch(i){
			case 0:{
				return i18n.get("booking.carrello.andata.label"); //"andata"
			}
			case 1:{
				return i18n.get("booking.carrello.ritorno.label"); //"ritorno"
			}
		}
		return "";
	}
	
	private String obtainTitleBySliceIndex(int sliceIndex) {
		String msg = "";
		switch(sliceIndex){
			case 0:{
				msg = i18n.get("booking.common.multitratta.prima.label"); //"PRIMA"
			}break;
			case 1:{
				msg = i18n.get("booking.common.multitratta.seconda.label"); //"SECONDA"
			}break;
			case 2:{
				msg = i18n.get("booking.common.multitratta.terza.label"); //"TERZA"
			}break;
			case 3:{
				msg = i18n.get("booking.common.multitratta.quarta.label"); //"QUARTA"
			}break;
			default:{
				return "";
			}
		}
		return msg + " " + i18n.get("booking.common.multitratta.tratta.label"); //"TRATTA"
	}
	
}

