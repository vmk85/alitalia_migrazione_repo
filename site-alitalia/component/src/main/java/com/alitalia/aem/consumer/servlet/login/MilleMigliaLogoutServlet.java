package com.alitalia.aem.consumer.servlet.login;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.auth.core.spi.AuthenticationHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

/**
 * Servlet exposing a service to perform a logout of the 
 * currently logged-in MilleMiglia user.
 * 
 * This servlet is meant to be called in a normal (non AJAX) POST,
 * so the user can be redirected to the home page.
 */

@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "millemiglialogout" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.extensions", value = { "html" })
})
@SuppressWarnings("serial")
public class MilleMigliaLogoutServlet extends SlingAllMethodsServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile AlitaliaConfigurationHolder alitaliaConfig;
	
	@Reference(target = "(isAlitaliaConsumerAH=true)", policy = ReferencePolicy.DYNAMIC)
    private volatile AuthenticationHandler authenticationHandler;
	
	
	public String getExternalDomain() {
    	return alitaliaConfig.getExternalDomain();
    }
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		
		logger.debug("MilleMiglia logout requested.");
		
		try {
		
			// clear remember-me cookie
			authenticationHandler.dropCredentials(request, response);
			
			// clear login token
			Cookie loginCookie = request.getCookie("login-token");
			if (loginCookie != null) {
				loginCookie.setMaxAge(0);
				loginCookie.setValue(null);
				loginCookie.setPath("/");
				response.addCookie(loginCookie);
			}

			request.getSession().removeAttribute("MilleMigliaProfileDataSicurezza");
			request.getSession().removeAttribute("MilleMigliaSocialAccount");
			request.getSession().removeAttribute("MilleMigliaCreditCardData");
			request.getSession().removeAttribute("datiPersonaliData");
			request.getSession().removeAttribute("datiLoginData");
			request.getSession().removeAttribute("comunicazioniOfferteData");
			request.getSession().removeAttribute("preferenzeViaggioData");
			request.getSession().removeAttribute("cartaCreditoData");
			request.getSession().removeAttribute("CUSTOMER");

			// redirect to home page
			String protocol = "http://";
			String logoutLandingPage = protocol + getExternalDomain() + request.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
			response.sendRedirect(logoutLandingPage);                   
			
		} catch (Exception e) {
			logger.error("Error trying to logout MilleMiglia.", e);
			
		}
		
	}
	
}
