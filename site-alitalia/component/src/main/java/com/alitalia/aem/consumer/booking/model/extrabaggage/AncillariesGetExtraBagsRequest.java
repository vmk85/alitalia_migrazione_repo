package com.alitalia.aem.consumer.booking.model.extrabaggage;


import java.util.*;

public class AncillariesGetExtraBagsRequest
{
	private ArrayList<AncillariesExtraBagsPassengerRequest> ancillariesPassenger;
	
	public final ArrayList<AncillariesExtraBagsPassengerRequest> getAncillariesPassengers()
	{
		return ancillariesPassenger;
	}
	
	public final void setAncillariesPassengers(ArrayList<AncillariesExtraBagsPassengerRequest> value)
	{
		ancillariesPassenger = value;
	}
}