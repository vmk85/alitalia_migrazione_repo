package com.alitalia.aem.consumer.checkin.model;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.checkin.CheckinConstants;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinPassengersRoot extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	private boolean index;
	private boolean apisBasic;
	private boolean apisPlus;
	private boolean apisPlusAdditionalInfo;
	
	@PostConstruct
	protected void initModel() {
		
		initBaseModel(request);
		
		if (request.getParameter(CheckinConstants.CHECKIN_PASSENGER_BACK_PARAM) != null) {
			if(ctx.additionalInfoPhase){
				ctx.additionalInfoPhase = false;
				ctx.additionalInfoPhaseCase2 = false;
				ctx.additionalInfoPhaseCase3 = false;
				ctx.additionalInfoPhaseCase4 = false;
			} else if (ctx.apisPhase){
				ctx.apisPhase = false;
			}
		}
		
		if (ctx != null && ctx.additionalInfoPhase) {
			apisPlusAdditionalInfo = true;
		} else if (ctx != null && ctx.apisPhase) {
			if (ctx.apisBasic) {
				apisBasic = true;
			} else {
				apisPlus = true;
			}
		} else {
			index = true;
		}
	}

	public boolean isIndex() {
		return index;
	}

	public boolean isApisBasic() {
		return apisBasic;
	}

	public boolean isApisPlus() {
		return apisPlus;
	}

	public boolean isApisPlusAdditionalInfo() {
		return apisPlusAdditionalInfo;
	}
}
