package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Enumerazione sulle tipologie di pagamenti Bancoposta
*/
public enum BancoPostaType
{
	BPIOL,
	BPOL,
	CreditCard,
	PostePayCard,
	PostePayImpresa;

	public static final int SIZE = java.lang.Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static BancoPostaType forValue(int value)
	{
		return values()[value];
	}
}