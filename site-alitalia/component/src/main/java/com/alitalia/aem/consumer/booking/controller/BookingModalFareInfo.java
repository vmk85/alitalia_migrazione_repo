package com.alitalia.aem.consumer.booking.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;

public class BookingModalFareInfo extends WCMUse {
	
	private String modal;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void activate() throws Exception {
		
		try{
			modal="";
			BookingSessionContext ctx = (BookingSessionContext) getRequest().getSession().getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
			DirectFlightData directFlight = null;
			for( RouteData routesData : ctx.availableFlights.getRoutes() ){
				FlightData flightData = routesData.getFlights().get(0);
				modal=computeModalForFCOLIN(flightData);
				if(modal.equals("")){
					if(flightData instanceof ConnectingFlightData){
						directFlight = 
								(DirectFlightData) (((ConnectingFlightData) flightData).getFlights().get(0));
					}else{
						directFlight = (DirectFlightData) flightData;
					}
					modal=computeModalBySliceType(directFlight);
				}
			}
				
		}catch(Exception e){
			logger.error("Errore Booking dati.html: ", e);
		}

	}

	private String computeModalForFCOLIN(FlightData flightData) {
		for(BrandData brandData : flightData.getBrands()){
			if(brandData.getCode().toLowerCase().contains("fcolin")){
				return "fcolin";
			}
		}
		return "";
	}

	private String computeModalBySliceType(DirectFlightData directFlight) {
		if(directFlight.getFrom().getArea() == AreaValueEnum.INTC
				|| directFlight.getTo().getArea() == AreaValueEnum.INTC){
			return "intercontinentale";
		}
		if(directFlight.getFrom().getArea() == AreaValueEnum.INTZ
				|| directFlight.getTo().getArea() == AreaValueEnum.INTZ){
			return "internazionale";
		}
		if(directFlight.getFrom().getArea() == AreaValueEnum.DOM
				&& directFlight.getTo().getArea() == AreaValueEnum.DOM){
			return "nazionale";
		}
		return "";
	}

	public String getModal() {
		return modal;
	}
	
}
