/**
 * Servlet per la comunicazione con il CRM
 * [D.V.]
 */

package com.alitalia.aem.consumer.myalitalia.crmdata.servlet;

import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.data.home.MACustomerProfile;
import com.alitalia.aem.common.messages.crmdatarest.*;
import com.alitalia.aem.consumer.gigya.GigyaConstans;
import com.alitalia.aem.consumer.gigya.GigyaSession;
import com.alitalia.aem.consumer.gigya.GigyaUtils;
import com.alitalia.aem.consumer.gigya.GigyaUtilsService;
import com.alitalia.aem.consumer.myalitalia.*;
import com.alitalia.aem.consumer.myalitalia.servlet.GenericMyAlitaliaFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.common.data.crmdatarest.model.*;
import com.alitalia.aem.ws.gigyaService.GigyaClient;
import com.day.cq.commons.TidyJSONWriter;
import com.gigya.socialize.GSKeyNotFoundException;
import com.gigya.socialize.GSResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import javax.servlet.Servlet;
import java.io.PrintWriter;
import java.util.HashMap;

@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = {"SetMyAlitaliaCrmDataConsensi",
                "GetMyAlitaliaCrmData",
                "RegistrerMyAlitaliaCrmUser",
                "SetMyAlitaliaCrmDataProfiloPersonale",
                "SetMyAlitaliaCrmDataDocumenti",
                "DeleteMyAlitaliaCrmDataDocumenti",
                "SetMyAlitaliaCrmDataPreferenze",
                "CRMdataPercent"
        }),
        @Property(name = "sling.servlet.methods", value = {"POST"}),
        @Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
        @Property(name = "sling.servlet.extensions", value = {"json"})
})
public class MyAlitaliaCrmDataServlet extends GenericMyAlitaliaFormValidatorServlet {

    @Reference
    private volatile MyAlitaliaSession myAlitaliaSession;

    @Reference
    private volatile GigyaSession gigyaSession;

    /**
     * Validatore, da implementare
     * (attualmente valida qualsiasi tipo di request)
     */
    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) {

        Validator validator = new Validator();

        ResultValidation resultValidation = validator.validate();
        if (!resultValidation.getResult()) {
            myAlitaliaServletLog.error("Error in validateForm");

        }
        return resultValidation;

    }

    /**
     * Metodo PostValidator, setto ed estraggo i dati dell'utente loggato e presente in sessione dal CRM
     * ( Modificare la descrizione del metodo nel momento in cui vengono aggiunti nuovi sviluppi )
     */
    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

        /** Estraggo il selettore Utilizzato per comunicare con la servlet */
        String selectorString = request.getRequestPathInfo().getSelectorString();

        MyAlitaliaSessionContext mactx = null;

        String jsonString = "";

        Result result = null;

        myAlitaliaServletLog.debug("MyAlitaliaCrmDataServlet interrogata per " + selectorString);

        SetCrmDataInfoResponse setCrmDataInfoResponse = null;

        boolean gigyaSave = false;

        /** Eseguo il Metodo in base al selettore utilizzato */
        switch (selectorString) {
            case "CRMdataPercent":
                mactx = MyAlitaliaUtils.getMactxBySession(request);

                if (mactx != null) {

                    jsonString = getPercentBySession(mactx);

                } else {
                    try {

                        HashMap<Object, Object> map = new HashMap<>();
                        Gson gson = new Gson();
                        map.put("isError", true);
                        jsonString = gson.toJson(map);

                    } catch (Exception e) {
                        myAlitaliaServletLog.error("Error to create a json response: " + e.getMessage());
                    }
                }

                break;
            case "SetMyAlitaliaCrmDataConsensi":

                /** Invio al CRM i dati per impostare nuovi valori nel profilo dell'utente
                 * ( richiamo la funzione getMyAlitaliaCrmData per avere tutti i dati aggiornati dell'utente ) */
                setCrmDataInfoResponse = MyAlitaliaUtilsService.setMyAlitaliaCrmDataConsensi(request, response, myAlitaliaSession);
                result = (setCrmDataInfoResponse != null ? setCrmDataInfoResponse.getResult() : null);

                if (result == null) {

                    myAlitaliaServletLog.error("Errore servizio SetMyAlitaliaCrmDataConsensi crm");

                } else if (result.getStatus() != null) {

                    if (!result.getStatus().equals(MyAlitaliaConstants.MYALITALIA_SERVICE_ERROR)) {
                        myAlitaliaServletLog.info("Utente [" + MyAlitaliaUtils.getMactxBySession(request).getGetCrmDataInfoResponse().getIdMyAlitalia() + "]; Consensi salvati correttamente");
                    } else {
                        myAlitaliaServletLog.error("Utente [" + MyAlitaliaUtils.getMactxBySession(request).getGetCrmDataInfoResponse().getIdMyAlitalia() + "]; Errore salvataggio consensi");
                    }

                } else {

                    myAlitaliaServletLog.error("Errore servizio SetMyAlitaliaCrmDataConsensi crm");

                }

                try {

                    jsonString = MyAlitaliaUtils.getJsonResponse(setCrmDataInfoResponse, result);

                } catch (Exception e) {
                    myAlitaliaServletLog.error("Errore creazione json risposta servlet selettore => SetMyAlitaliaCrmDataConsensi : " + e.toString());
                }

                myAlitaliaServletLog.debug("Response json Servlet : " + jsonString);

                break;
            case "GetMyAlitaliaCrmData":

                /** Richiamo il CRM per reperire i dati dell'utente in sessione */
                GetCrmDataInfoResponse getCrmDataInfoResponse = MyAlitaliaUtilsService.getCrmDataInfoFromCrm(request, myAlitaliaSession);
                MyAlitaliaUtils.setCrmResponseDataToSession(request, getCrmDataInfoResponse);
                result = (getCrmDataInfoResponse != null ? getCrmDataInfoResponse.getResult() : null);

                if (result == null) {

                    myAlitaliaServletLog.error("Errore servizio GetMyAlitaliaCrmData crm");

                } else if (result.getStatus() != null) {

                    if (!result.getStatus().equals(MyAlitaliaConstants.MYALITALIA_SERVICE_ERROR)) {
                        myAlitaliaServletLog.info("Utente [" + getCrmDataInfoResponse.getIdMyAlitalia() + "]; Dati caricati correttamente dal CMR");
                    } else {
                        myAlitaliaServletLog.error("Errore caricamento dati da CMR");
                    }

                } else {

                    myAlitaliaServletLog.error("Errore servizio GetMyAlitaliaCrmData crm");

                }

                try {

                    jsonString = MyAlitaliaUtils.getJsonResponse(getCrmDataInfoResponse, result);

                } catch (Exception e) {
                    myAlitaliaServletLog.error("Errore creazione json risposta servlet selettore => SetMyAlitaliaCrmDataConsensi : " + e.toString());
                }

                myAlitaliaServletLog.debug("Response json Servlet : " + jsonString);

                break;

            case "RegistrerMyAlitaliaCrmUser":

                String regToken = request.getParameter("regToken");

                MACustomer maCustomer = new MACustomer();

                if (regToken == null) {

                    /** Registrazione nuovo utente tramite email e password*/
                    maCustomer = gigyaRegistration(request);


                } else {

                    maCustomer = gigyaSocialRegistration(regToken, request, myAlitaliaSession);

                }

                if (maCustomer != null) {

                    /** Registro il nuovo utente nel CRM*/
                    setCrmDataInfoResponse = MyAlitaliaUtilsService.setMyAlitaliaCrmDataRegistration(request, response, myAlitaliaSession, maCustomer);
                    result = (setCrmDataInfoResponse != null ? setCrmDataInfoResponse.getResult() : null);

                    /** Controllo che il CRM abbia correttamente registrato l'utente*/
                    if (result != null && !result.getStatus().equals(MyAlitaliaConstants.MYALITALIA_SERVICE_ERROR)) {

                        if (request.getSession().getAttribute("regToken") != null) {

                            /** Finalizzo la registrazione a Gigya con il token salvato in sessione */
                            GSResponse gsResponse = gigyaSession.finalizeRegistration(request.getSession().getAttribute("regToken").toString());

                        }

                    } else {

                        /** Eseguo la cancellazione dell'utente Gigya a valle dell'errore di registrazoine CRM */
                        gigyaSession.deleteAccount(maCustomer.getUID());

                    }

                    request.getSession().removeAttribute("regToken");

                    if (result == null) {

                        myAlitaliaServletLog.error("Errore servizio RegistrerMyAlitaliaCrmUser crm");

                    } else if (result.getStatus() != null) {

                        if (result.getStatus().equals(MyAlitaliaConstants.MYALITALIA_SERVICE_ERROR)) {

                            myAlitaliaServletLog.error("Errore registrazione cliente");

                        } else {

                            myAlitaliaServletLog.error("Errore servizio RegistrerMyAlitaliaCrmUser crm");

                        }

                    } else {


                    }

                    try {

                        jsonString = MyAlitaliaUtils.getJsonResponse(setCrmDataInfoResponse, result);

                    } catch (Exception e) {
                        myAlitaliaServletLog.error("Errore creazione json risposta servlet selettore => RegistrerMyAlitaliaCrmUser : " + e.toString());
                    }

                    myAlitaliaServletLog.debug("Response json Servlet : " + jsonString);
                } else {

                    try {

                        HashMap<Object, Object> map = new HashMap<>();
                        Gson gson = new Gson();
                        map.put("isError", true);
                        map.put("errorDescription", "myalitalia.user.mail.exist");
                        jsonString = gson.toJson(map);

                    } catch (Exception e) {
                        myAlitaliaServletLog.error("Error to create a json response: " + e.getMessage());
                    }

                }


                break;

            case "SetMyAlitaliaCrmDataProfiloPersonale":

                gigyaSave = gigyaSetAccount(request);

                /** Invio al CRM i dati per impostare nuovi valori nel profilo dell'utente
                 * ( richiamo la funzione getMyAlitaliaCrmData per avere tutti i dati aggiornati dell'utente ) */
                setCrmDataInfoResponse = MyAlitaliaUtilsService.setMyAlitaliaCrmPersonalData(request, response, myAlitaliaSession);
                result = (setCrmDataInfoResponse != null ? setCrmDataInfoResponse.getResult() : null);

                if (result == null) {

                    myAlitaliaServletLog.error("Errore servizio SetMyAlitaliaCrmDataProfiloPersonale crm");

                } else if (result.getStatus() != null) {

                    if (!result.getStatus().equals(MyAlitaliaConstants.MYALITALIA_SERVICE_ERROR)) {
                        myAlitaliaServletLog.info("Utente  [" + MyAlitaliaUtils.getMactxBySession(request).getGetCrmDataInfoResponse().getIdMyAlitalia() + "]; Profilo personale salvati correttamente");
                    } else {
                        myAlitaliaServletLog.error("Utente [" + MyAlitaliaUtils.getMactxBySession(request).getGetCrmDataInfoResponse().getIdMyAlitalia() + "]; Errore salvataggio profilo personale");
                    }

                } else {

                    myAlitaliaServletLog.error("Errore servizio SetMyAlitaliaCrmDataProfiloPersonale crm");

                }

                try {

                    jsonString = MyAlitaliaUtils.getJsonResponse(setCrmDataInfoResponse, result);

                } catch (Exception e) {
                    myAlitaliaServletLog.error("Errore creazione json risposta servlet selettore => SetMyAlitaliaCrmDataProfiloPersonale : " + e.toString());
                }

                myAlitaliaServletLog.debug("Response json Servlet : " + jsonString);

                break;

            case "SetMyAlitaliaCrmDataDocumenti":

                /** Invio al CRM i dati per impostare nuovi valori nel profilo dell'utente
                 * ( richiamo la funzione getMyAlitaliaCrmData per avere tutti i dati aggiornati dell'utente ) */
                setCrmDataInfoResponse = MyAlitaliaUtilsService.setMyAlitaliaCrmDocument(request, response, myAlitaliaSession);
                result = (setCrmDataInfoResponse != null ? setCrmDataInfoResponse.getResult() : null);

                if (result == null) {

                    myAlitaliaServletLog.error("Errore servizio SetMyAlitaliaCrmDataDocumenti crm");

                } else if (result.getStatus() != null) {

                    if (!result.getStatus().equals(MyAlitaliaConstants.MYALITALIA_SERVICE_ERROR)) {
                        myAlitaliaServletLog.info("Utente [" + MyAlitaliaUtils.getMactxBySession(request).getGetCrmDataInfoResponse().getIdMyAlitalia() + "]; Documenti salvati correttamente");
                    } else {
                        myAlitaliaServletLog.error("Utente [" + MyAlitaliaUtils.getMactxBySession(request).getGetCrmDataInfoResponse().getIdMyAlitalia() + "]; Errore salvataggio documenti");
                    }

                } else {

                    myAlitaliaServletLog.error("Errore servizio SetMyAlitaliaCrmDataDocumenti crm");

                }

                try {

                    jsonString = getJsonDocumentResponse(MyAlitaliaUtils.getJsonResponse(setCrmDataInfoResponse, result), request);

                } catch (Exception e) {
                    myAlitaliaServletLog.error("Errore creazione json risposta servlet selettore => SetMyAlitaliaCrmDataProfiloPersonale : " + e.toString());
                }

                myAlitaliaServletLog.debug("Response json Servlet : " + jsonString);
                break;
            case "DeleteMyAlitaliaCrmDataDocumenti":

                /** Invio al CRM i dati per eliminare valori nel profilo dell'utente
                 * ( richiamo la funzione getMyAlitaliaCrmData per avere tutti i dati aggiornati dell'utente ) */

                setCrmDataInfoResponse = MyAlitaliaUtilsService.deleteMyAlitaliaCrmDocument(request, response, myAlitaliaSession);
                result = (setCrmDataInfoResponse != null ? setCrmDataInfoResponse.getResult() : null);

                if (result == null) {

                    myAlitaliaServletLog.error("Errore servizio DeleteMyAlitaliaCrmDataDocumenti crm");

                } else if (result.getStatus() != null) {

                    if (!result.getStatus().equals(MyAlitaliaConstants.MYALITALIA_SERVICE_ERROR)) {
                        myAlitaliaServletLog.info("Utente [" + MyAlitaliaUtils.getMactxBySession(request).getGetCrmDataInfoResponse().getIdMyAlitalia() + "]; Documento eliminato correttamente");
                    } else {
                        myAlitaliaServletLog.error("Utente [" + MyAlitaliaUtils.getMactxBySession(request).getGetCrmDataInfoResponse().getIdMyAlitalia() + "]; Errore eliminazione documento");
                    }

                } else {

                    myAlitaliaServletLog.error("Errore servizio DeleteMyAlitaliaCrmDataDocumenti crm");

                }

                try {

                    jsonString = getJsonDocumentResponse(MyAlitaliaUtils.getJsonResponse(setCrmDataInfoResponse, result), request);

                } catch (Exception e) {
                    myAlitaliaServletLog.error("Errore creazione json risposta servlet selettore => SetMyAlitaliaCrmDataProfiloPersonale : " + e.toString());
                }

                myAlitaliaServletLog.debug("Response json Servlet : " + jsonString);
                break;
            case "SetMyAlitaliaCrmDataPreferenze":
                String crmData = request.getParameter("data");

                gigyaSave = gigyaSetAccount(request);

                /** Invio al CRM i dati per inserire valori nel profilo dell'utente
                 * ( richiamo la funzione getMyAlitaliaCrmData per avere tutti i dati aggiornati dell'utente ) */
                setCrmDataInfoResponse = MyAlitaliaUtilsService.setMyAlitaliaCrmPreferenze(request, response, myAlitaliaSession);
                result = (setCrmDataInfoResponse != null ? setCrmDataInfoResponse.getResult() : null);

                if (result == null) {

                    myAlitaliaServletLog.error("Errore servizio SetMyAlitaliaCrmDataPreferenze crm");

                } else if (result.getStatus() != null) {

                    if (!result.getStatus().equals(MyAlitaliaConstants.MYALITALIA_SERVICE_ERROR)) {
                        myAlitaliaServletLog.info("Utente [" + MyAlitaliaUtils.getMactxBySession(request).getGetCrmDataInfoResponse().getIdMyAlitalia() + "]; Preferenze salvati correttamente");
                    } else {
                        myAlitaliaServletLog.error("Utente [" + MyAlitaliaUtils.getMactxBySession(request).getGetCrmDataInfoResponse().getIdMyAlitalia() + "]; Errore salvataggio preferenze");
                    }

                } else {

                    myAlitaliaServletLog.error("Errore servizio SetMyAlitaliaCrmDataPreferenze crm");

                }

                try {

                    jsonString = getJsonDocumentResponse(MyAlitaliaUtils.getJsonResponse(setCrmDataInfoResponse, result), request);

//                    if(gigyaSave){
//                        JSONObject obj = new JSONObject(jsonString);
//                        obj.put("gigya", getMyAlitaliaSessionContext(request).getMaCustomer().getData().getFlightPref_FF());
//                        jsonString = obj.toString();
//                    }


                } catch (Exception e) {
                    myAlitaliaServletLog.error("Errore creazione json risposta servlet selettore => SetMyAlitaliaCrmDataProfiloPersonale : " + e.toString());
                }

                myAlitaliaServletLog.debug("Response json Servlet : " + jsonString);
                break;
        }
        try {

            response.setContentType("application/json");
            PrintWriter writer = response.getWriter();
            writer.write(jsonString);

        } catch (Exception e) {
            myAlitaliaServletLog.error("Unexected error generating JSON response MyAlitaliaCrmDataServlet : " + e.toString());
        }
    }

    private boolean gigyaSetAccount(SlingHttpServletRequest request) throws Exception {

        boolean success = false;
        if (request.getParameter("gigya") != null) {
            MyAlitaliaSessionContext mactx = getMyAlitaliaSessionContext(request);
            GSResponse gsResponse = gigyaSession.setAccount(mactx.getMaCustomer().getUID(), GigyaUtils.mapJsonObjGigya(request.getParameter("gigya")));
            if (gsResponse.getErrorCode() == 0){
                gsResponse = gigyaSession.getAccount(mactx.getMaCustomer().getUID());
                Gson gson = new Gson();
                MACustomer maCustomer1 = gson.fromJson(gsResponse.getResponseText(), MACustomer.class);
                mactx.getMaCustomer().setData(maCustomer1.getData());
                MyAlitaliaUtils.setMactxToSession(request, mactx);
                success = true;
            } else {
                myAlitaliaServletLog.error("Errore salvataggio dati su Gigya");
            }
        }
        return success;
    }

    private String getPercentBySession(MyAlitaliaSessionContext mactx) {
        String json = "";
        Gson gson = new Gson();
        try {
            JSONObject obj = new JSONObject();
            obj.put("percent", mactx.getProfileComplite().getPercent());
            obj.put("redirectUrl", mactx.getProfileComplite().getRedirectUrl());
            obj.put("testo", mactx.getProfileComplite().getTesto());
            obj.put("isError", false);
            json = obj.toString();
        } catch (Exception e) {
            myAlitaliaServletLog.error("Error to create a json response: " + e.getMessage());
        }
        return json;

    }

    private MACustomer gigyaRegistration(SlingHttpServletRequest request) throws Exception {

        String email = request.getParameter("email");
        String psw = request.getParameter("password");
        String authToken = "";
        String uid = "";
        String lang = AlitaliaUtils.getRepositoryPathLanguage(request.getResource());
        boolean auth_com = false;
        boolean auth_profile = false;
        GSResponse gsResponse = null;
        MACustomer maCustomer = new MACustomer();

        /** Verifico che l'email non sia già registrata */
        if (gigyaSession.isAvailableLoginID(email)) {

            /** Inizializzo la registrazione per prendere il token*/
            authToken = gigyaSession.initRegistration();

            /** Effettuo la registrazione */
            gsResponse = gigyaSession.register(email, psw, authToken, lang, auth_com, auth_profile);

            uid = gsResponse.getData().get("UID").toString();

            /** Nuovo token per finalizzare la registrazione */
            authToken = gsResponse.getData().get("regToken").toString();

            request.getSession().setAttribute("regToken", authToken);

            maCustomer.setUID(uid);
            maCustomer.setProfile(new MACustomerProfile());
            maCustomer.getProfile().setEmail(email);

        } else {
            maCustomer = null;
        }

        return maCustomer;
    }

    private MACustomer gigyaSocialRegistration(String regToken, SlingHttpServletRequest request, MyAlitaliaSession myAlitaliaSession) throws Exception {

        GSResponse gsResponse = null;

        MACustomer maCustomer = null;

        /** Se presente finalizzo il social login, mi restituisce i dati dell'utente */
        if (regToken != null) {

            /** Parametri obbligatori per la registrazione a gigya*/
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("auth_Profile",false);
            jsonObject.put("auth_Com",false);
            /** Chiamata al servizio per prendere i dati di registrazione */
            gsResponse = gigyaSession.setSocialAccount(regToken, GigyaUtils.mapJsonObjGigya(jsonObject.toString()));

            /** Chiamata al servizio per finalizzare la registrazione */
            gsResponse = gigyaSession.finalizeRegistration(regToken);

        }

        /** Se non ci sono errori genero il customer per la sessione */
        if (gsResponse.getErrorCode() == 0) {

            Gson gson = new Gson();

            /** Converto la response da Gigya per salvare l'utente in sessione ed effettuare il login su CRM  */
            maCustomer = gson.fromJson(gsResponse.getResponseText(), MACustomer.class);

            MyAlitaliaUtils.setMaCustomerToSession(request, myAlitaliaSession, maCustomer);

        } else {

            maCustomer = null;

        }

        return maCustomer;

    }


    private String getJsonDocumentResponse(String jsonResponse, SlingHttpServletRequest request) {

        String json = "";
        Gson gson = new Gson();
        try {
            JSONObject obj = new JSONObject(jsonResponse);
            obj.put("documento", request.getParameter("data"));
            obj.put("crmData", gson.toJson(MyAlitaliaUtils.getMyAlitaliaCrmDataBySession(request)));
            json = obj.toString();
        } catch (Exception e) {
            myAlitaliaServletLog.error("Error to create a json response: " + e.getMessage());
        }
        return json;

    }

}
