package com.alitalia.aem.consumer.booking.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables={ SlingHttpServletRequest.class })
public class BookingScegliVolo extends BookingSessionGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private SearchElement outboundSearchElement;
	private boolean performSearch;
	private String errorUrl;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@PostConstruct
	protected void initModel() {
		
		try {
			
			initBaseModel(request);
			if (ctx != null) {
				outboundSearchElement = ctx.searchElements.get(0);
				if (ctx.currentSliceIndex > 1) {
					performSearch = false;
				} else {
					performSearch = true;
				}
			}
			String baseUrl = request.getResource().getResourceResolver().map(
					AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false));
			
			errorUrl = baseUrl + configuration.getBookingFailurePage();
			
		} catch(Exception e) {
			logger.error("[BookingScegliVolo] Unexpected error", e);
			throw e;
		}
	}
	
	public SearchElement getOutboundSearchElement() {
		return outboundSearchElement;
	}
	
	public boolean isPerformSearch() {
		return performSearch;
	}
	
	public String getErrorUrl() {
		return errorUrl;
	}
}
