package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.FrecciaAlataRequestData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "frecciaalatarequestsubmit" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class FrecciaAlataRequestServlet extends GenericFormValidatorServlet{
	
	private static final int DIM_FORM = 9;
	private static final String KEYVALUE_SEPARATOR = ":";
	private static final String ELEMENT_SEPARATOR = System.getProperty("line.separator");
	
	// Static Property For Mails
	private static final String COMPONENT_NAME = "freccia_alata_card_r";
	private static final String PARSYS_NAME = "page-component";
	private static final String MAIL_FROM_PROPERTY_NAME = "mailFrom";
	private static final String MAIL_TO_PROPERTY_NAME = "mailTo";
	private static final String MAIL_SUBJECT_PROPERTY_NAME = "subject";
	private static final String MAIL_FROM_DEFAULT = "fatturazione.postvendita@alitalia.it";
	private static final String MAIL_TO_DEFAULT = "cfaperte@dataar.it";
	private static final String MAIL_SUBJECT_DEFAULT = "Richiesta Fatturazione";
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BusinessLoginService businessLoginService;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[FrecciaAlataRequestServlet] validateForm");
		
		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			return validator.validate();
			
		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Errore durante la procedura di validazione dati "
					+ "richiesta freccia alata", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		logger.debug("[FrecciaAlataRequestServlet] performSubmit");
		
		boolean mailResult = sendMail(request);
		logger.debug("Mail Sended = " + mailResult);
		if (mailResult) {
			goBackSuccessfully(request, response);
		}
		else {
			throw new IOException("Service Error");
		}
		
	}
	
	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {

		// get parameters from request
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String email = request.getParameter("email");
		String millemigliacode = request.getParameter("millemigliacode");
		String membername = request.getParameter("membername");
		String membersurname = request.getParameter("membersurname");
		String promocode = request.getParameter("promocode");
		String conditionsaccepted = request.getParameter("conditionsaccepted");

		// add validate conditions
		validator.addDirectConditionMessagePattern("name", name,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("name", name,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.NAME_ERROR_NOT_VALID, "isAlphabeticWithSpaces");
		
		validator.addDirectConditionMessagePattern("surname", surname,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.SURNAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("surname", surname,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.SURNAME_ERROR_NOT_VALID, "isAlphabeticWithSpaces");
		
		validator.addDirectConditionMessagePattern("email", email,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.EMAIL_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("email", email,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID, "isEmail");
		
		validator.addDirectConditionMessagePattern("millemigliacode", millemigliacode,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("millemigliacode", millemigliacode,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "isNumberWithSpace");
		
		validator.addDirectConditionMessagePattern("membername", membername,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("membername", membername,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.NAME_ERROR_NOT_VALID, "isAlphabeticWithSpaces");
		
		validator.addDirectConditionMessagePattern("membersurname", membersurname,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.SURNAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("membersurname", membersurname,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.SURNAME_ERROR_NOT_VALID, "isAlphabeticWithSpaces");
	
		validator.addDirectConditionMessagePattern("promocode", promocode,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.PROMOCODE_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("promocode", promocode,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.PROMOCODE_ERROR_NOT_VALID, "isPromoCode");
		
		validator.addCrossCondition("conditionsaccepted", conditionsaccepted, "accepted",
				I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "areEqual");
		
		return validator;

	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		logger.debug("[FrecciaAlataRequestServlet] saveDataIntoSession");
		
		// create baggageClaimData object and save it into session
		FrecciaAlataRequestData frecciaAlataRequestData = new FrecciaAlataRequestData();
		frecciaAlataRequestData.setName(request.getParameter("name"));
		frecciaAlataRequestData.setSurname(request.getParameter("surname"));
		frecciaAlataRequestData.setPhonenumber(request.getParameter("phonenumber"));
		frecciaAlataRequestData.setEmail(request.getParameter("email"));
		frecciaAlataRequestData.setMillemigliacode(request.getParameter("millemigliacode"));
		frecciaAlataRequestData.setMembername(request.getParameter("membername"));
		frecciaAlataRequestData.setMembersurname(request.getParameter("membersurname"));
		frecciaAlataRequestData.setPromocode(request.getParameter("promocode"));
		frecciaAlataRequestData.setConditionsaccepted(request.getParameter("conditionsaccepted"));
		
		// go back to form with error
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		
		frecciaAlataRequestData.setError(i18n.get(I18nKeySpecialPage.ERROR_SERVICE));
		
		request.getSession().setAttribute(FrecciaAlataRequestData.NAME,
				frecciaAlataRequestData);
		
	}
	
	/*
	 * sendMail
	 * 
	 */
	private boolean sendMail(SlingHttpServletRequest request) {
		logger.debug("[FrecciaAlataRequestServlet] sendMail");
		
		// current page
		Page currentPage = AlitaliaUtils.getPage(request.getResource());
		
		// email from
	    String mailFrom = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_FROM_PROPERTY_NAME);
	    if (mailFrom == null) {
	    	mailFrom = MAIL_FROM_DEFAULT;
	    }
	    
	    // email to
	    String mailTo = getComponentProperty(currentPage, 
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_TO_PROPERTY_NAME);
	    if (mailTo == null) {
	    	mailTo = MAIL_TO_DEFAULT;
	    }
	    
	    // email subject
	    String subject = getComponentProperty(currentPage,
	    		PARSYS_NAME, COMPONENT_NAME, MAIL_SUBJECT_PROPERTY_NAME);
	    if (subject == null) {
	    	subject = MAIL_SUBJECT_DEFAULT;
	    }

	    // email body
	    String messageText = generateBodyMail(request);
	    
	    logger.debug("[ MillemigliaKidsServlet] sendMail -- Email data: "
				+ "[From: {" + mailFrom + "}] - [To: {" + mailTo + "}] - "
				+ "[Subject: {" + subject + "}] - [Body: {" + messageText + "}]");
		
	    // create email message object
		EmailMessage emailMessage = new EmailMessage();
		emailMessage.setFrom(mailFrom);
		emailMessage.setTo(mailTo);
		emailMessage.setSubject(subject);
		emailMessage.setMessageText(messageText);
		emailMessage.setIsBodyHtml(false);
		emailMessage.setDeliveryNotification(DeliveryNotificationOptionsEnum.NONE);
		emailMessage.setPriority(MailPriorityEnum.NORMAL);

		// create AgencySendMailRequest
	    AgencySendMailRequest mailRequest = new AgencySendMailRequest();
		mailRequest.setTid(IDFactory.getTid());
		mailRequest.setSid(IDFactory.getSid(request));
		mailRequest.setEmailMessage(emailMessage);

		// get AgencySendMailResponse
		AgencySendMailResponse mailResponse =
				businessLoginService.sendMail(mailRequest);
		
		// return status
		return mailResponse.isSendMailSuccessful();
		
	}
	
	/*
	 * generateBodyMail
	 * 
	 */
	private String generateBodyMail(SlingHttpServletRequest request) {
		logger.debug("[FrecciaAlataRequestServlet] generateBodyMail");
		
		String[][] data = new String[DIM_FORM][2];
		
		// creating Keys
		int k;
		for (k = 0; k < DIM_FORM; k++) {
			String name = "KeyField" + Integer.toString(k);
			data[k][0] = request.getParameter(name);
		}
		
		// getting Values
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String millemigliacode = request.getParameter("millemigliacode");
		String email = request.getParameter("email");
		String phonenumber = request.getParameter("phonenumber");
		String membername = request.getParameter("membername");
		String membersurname = request.getParameter("membersurname");
		String promocode = request.getParameter("promocode");
		String conditionsaccepted = request.getParameter("conditionsaccepted");
		
		// setting Values
		data[0][1] = (null != name) ? name : "";
		data[1][1] = (null != surname) ? surname : "";
		data[2][1] = (null != millemigliacode) ? millemigliacode : "";
		data[3][1] = (null != email) ? email : "";
		data[4][1] = (null != phonenumber) ? phonenumber : "";
		data[5][1] = (null != membername) ? membername : "";
		data[6][1] = (null != membersurname) ? membersurname : "";
		data[7][1] = (null != promocode) ? promocode : "";
		data[8][1] = (null != conditionsaccepted) ? conditionsaccepted : "";
		
		// creating email message and return it		
		String emailText = "";
		for (k = 0; k < DIM_FORM; k++) {
			emailText = emailText + data[k][0] + KEYVALUE_SEPARATOR + data[k][1];
			if (k < DIM_FORM - 1) {
				emailText = emailText + ELEMENT_SEPARATOR;
			}

		}
		return emailText;
		
	}
	
	/*
	 * getComponentProperty
	 * 
	 */
	private String getComponentProperty(Page ancestor, String parsysName,
			String compName, String propName) {
		logger.debug("[FrecciaAlataRequestServlet] getComponentProperty");
		
		Resource pageResource = ancestor.getContentResource();	
		String prop = null;
		try{
			prop = pageResource.getChild(parsysName).getChild(compName)
					.getValueMap().get(propName, String.class);
		}
		catch(Exception e) {}
		
		return prop;
		
	}

}