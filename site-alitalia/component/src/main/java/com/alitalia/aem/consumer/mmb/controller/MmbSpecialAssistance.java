package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.mmb.model.MmbFlightsGroup;
import com.alitalia.aem.consumer.mmb.render.MmbFlightDataRender;
import com.alitalia.aem.consumer.mmb.render.MmbSpecialAssistanceFlightRender;
import com.alitalia.aem.consumer.mmb.render.MmbSpecialAssistanceListRender;
import com.alitalia.aem.consumer.mmb.render.MmbSpecialAssistancePassengerRender;


@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbSpecialAssistance extends MmbSessionGenericController {
	private static String CARRIER_ENABLED ="AZ";
	private static int HOURS_BOUND = 48;
	private static String KEY_PREFIX = "specialAssistance.";
	private static String KEY_SEPARATOR = ",";
	private static String DATE_SEPARATOR = "-";
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private List<MmbSpecialAssistancePassengerRender> passengers;
	private List<MmbSpecialAssistanceFlightRender> flights;
	private List<MmbSpecialAssistanceListRender> specialAssistanceList;
	private String pnr;
	private boolean enabled;
	
	
	
	@PostConstruct
	protected void initModel() throws IOException {
		initBaseModel(request);
		
		List<MmbFlightsGroup> flightsGroupList = MmbFlightsGroup.groupByRouteType(ctx.route.getFlights());
	
		//RETRIEVE PNR
		pnr = ctx.pnr;
		
		//RETRIEVE APPLICANT PASSENGER DATA
		MmbPassengerData applicantPassenger = ctx.route.getApplicantPassenger();
		
		//RETRIEVE PASSENGERS DATA
		this.passengers = new ArrayList<MmbSpecialAssistancePassengerRender>();
		MmbFlightData flightData = ctx.route.getFlights().get(0);
		List<MmbPassengerData> passengerDataRenderList = flightData.getPassengers();
		for (MmbPassengerData passengerData : passengerDataRenderList) {
			//RENDER THE PASSENGER ONLY IF HE IS NOT AN INFANT
			if (passengerData.getType() != PassengerTypeEnum.INFANT) {
				MmbSpecialAssistancePassengerRender passenger = new MmbSpecialAssistancePassengerRender();
				passenger.setName(passengerData.getName());
				passenger.setLastname(passengerData.getLastName());
				passenger.setTicket(passengerData.getEticket());
				passenger.setMail(passengerData.getEmail1());
				passenger.setTelephone(passengerData.getTelephone1());
				passengers.add(passenger);
				
				
			}
		}
		
		// TAKE MAIL AND TELEPHONE FROM THE APPLICANT PASSENGER (THE FIRST ONE INTO THE LIST)
		if (applicantPassenger != null) {
			if (passengers.get(0).getMail() == null) {
				String email = applicantPassenger.getEmail1();
				email = email.substring(email.lastIndexOf("/") + 1);
				passengers.get(0).setMail(email);
			}
			
			if (passengers.get(0).getTelephone() == null) {
				String[] telephoneData= applicantPassenger.getTelephone1().split("/");
				String telephone = telephoneData[2];
				passengers.get(0).setTelephone(telephone);
			}
		}
	
		
		// RETRIEVE FLIGHTS DATA
		this.flights = new ArrayList<MmbSpecialAssistanceFlightRender>();
		for (MmbFlightsGroup flightGroup : flightsGroupList) {
			for(MmbFlightDataRender flightRender : flightGroup.getFlightDataRender()) {
				boolean checkCarrier = false;
				boolean checkDepartureDate = false;
				
				// CHECK IF FLIGHT CARRIER IS AZ
				if (CARRIER_ENABLED.equals(flightRender.getFlightData().getCarrier())) {
					checkCarrier = true;
				}
				
				if (checkCarrier) {
					// CHECK IF DEPARTURE HOURE IS AT LEAST AFTER 48h FROM NOW
					Calendar departureDate = flightRender.getFlightData().getDepartureDateTime();
					Calendar boundDate = Calendar.getInstance();
					boundDate.add(Calendar.HOUR, HOURS_BOUND);
					if (departureDate.after(boundDate)) {
						checkDepartureDate = true;
					}
				}
				
				// IF CONTROLS ARE SATISFIED IT WILL RENDER THE FLIGHT
				if (checkCarrier && checkDepartureDate) {
					MmbSpecialAssistanceFlightRender flight = new MmbSpecialAssistanceFlightRender();
					flight.setDepartureAirportCode(flightRender.getFlightData().getFrom().getCode());
					flight.setDepartureCityCode(flightRender.getFlightData().getFrom().getCityCode());
					flight.setDepartureCityI18nKey(flightRender.getDepartureCityI18nKey());
					flight.setArrivalAirportCode(flightRender.getFlightData().getTo().getCode());
					flight.setArrivalCityCode(flightRender.getFlightData().getTo().getCityCode());
					flight.setArrivalCityI18nKey(flightRender.getArrivalCityI18nKey());
					flight.setFlightDate(i18n.get(flightRender.getDepartureMonth()) + DATE_SEPARATOR + flightRender.getDepartureDay() + DATE_SEPARATOR + flightRender.getDepartureYear());
					flight.setFlightNumber(flightRender.getFlightData().getCarrier() + flightRender.getFlightData().getFlightNumber());
					flights.add(flight);
				}
				

			}
		}
		
		if (flights.isEmpty()) {
			// NO FLIGHT AVAILABLE TO CHOOSE. FORM WON'T BE SHOWN
			enabled = false;
		}
		else {
			// SOME FLIGHT AVAILABLE
			enabled = true;
			
			//RETRIEVE STATIC LIST 
			String[] specialAssistanceKeys = configuration.getSpecialAssistanceKeys().split(KEY_SEPARATOR);
			List <String> specialAssistanceKeysWheelChair = Arrays.asList(configuration.getSpecialAssistanceWheelchairKeys().split(KEY_SEPARATOR));
			specialAssistanceList = new ArrayList<MmbSpecialAssistanceListRender>();
			for (String key_suffix : specialAssistanceKeys) {
				String value = key_suffix;
				String text = i18n.get(KEY_PREFIX + key_suffix);
				String extraInfo = "";
				if (specialAssistanceKeysWheelChair.contains(value)) {
					extraInfo = "extraInfo";
				}
				MmbSpecialAssistanceListRender specialAssistance = new MmbSpecialAssistanceListRender(value, text, extraInfo);
				specialAssistanceList.add(specialAssistance);
			}
		}
		
		
	}


	
	

	public List<MmbSpecialAssistancePassengerRender> getPassengers() {
		return passengers;
	}



	public List<MmbSpecialAssistanceFlightRender> getFlights() {
		return flights;
	}



	public List<MmbSpecialAssistanceListRender> getSpecialAssistanceList() {
		return specialAssistanceList;
	}
	
	public String getPnr() {
		return pnr;
	}


	public boolean isEnabled() {
		return enabled;
	}

	
}

