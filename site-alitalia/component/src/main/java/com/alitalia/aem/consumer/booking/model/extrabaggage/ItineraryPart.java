package com.alitalia.aem.consumer.booking.model.extrabaggage;


import java.util.*;

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
///#region[References]
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
///#endregion
/** 
 Common response class for deserializing itinerary part
*/
public class ItineraryPart
{
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
		///#region[Properties]
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [JsonProperty(PropertyName = "@type")] public string type {get;set;}
	private String type;
	public final String gettype()
	{
		return type;
	}
	public final void settype(String value)
	{
		type = value;
	}
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [JsonProperty(PropertyName = "@id")] public string _id {get;set;}
	private String _id;
	public final String get_id()
	{
		return _id;
	}
	public final void set_id(String value)
	{
		_id = value;
	}
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [JsonProperty(PropertyName = "@ref")] public string itineraryRef {get;set;}
	private String itineraryRef;
	public final String getitineraryRef()
	{
		return itineraryRef;
	}
	public final void setitineraryRef(String value)
	{
		itineraryRef = value;
	}
	private ArrayList<Segment> segments;
	public final ArrayList<Segment> getsegments()
	{
		return segments;
	}
	public final void setsegments(ArrayList<Segment> value)
	{
		segments = value;
	}
	private int stops;
	public final int getstops()
	{
		return stops;
	}
	public final void setstops(int value)
	{
		stops = value;
	}
	private int totalDuration;
	public final int gettotalDuration()
	{
		return totalDuration;
	}
	public final void settotalDuration(int value)
	{
		totalDuration = value;
	}
	private ArrayList<ConnectionInformation> connectionInformations;
	public final ArrayList<ConnectionInformation> getconnectionInformations()
	{
		return connectionInformations;
	}
	public final void setconnectionInformations(ArrayList<ConnectionInformation> value)
	{
		connectionInformations = value;
	}
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
		///#endregion
}