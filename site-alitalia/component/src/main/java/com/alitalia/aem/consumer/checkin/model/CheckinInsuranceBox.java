package com.alitalia.aem.consumer.checkin.model;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinInsurancePolicyData;
import com.alitalia.aem.common.data.home.enumerations.CheckinPassengerStatusEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinInsuranceBox extends GenericCheckinModel {
	
	@Inject
	AlitaliaConfigurationHolder configurationHolder;
	
	@Self
	private SlingHttpServletRequest request;
	
	private MmbPriceRender price;
	
	private boolean bought;
	
	private boolean added;
	
	private boolean showInsuranceBox;
	
	@PostConstruct
	protected void initModel() {
		try{
			initBaseModel(request);
			CheckinInsurancePolicyData data = ctx.insurancePolicyData;
			boolean insuranceConditionOK = checkInsuranceConditions(ctx);
			if(data != null && insuranceConditionOK){
				showInsuranceBox = true;
				price = new MmbPriceRender(data.getTotalInsuranceCost()
						, ctx.currencyCode, ctx.currencySymbol
						, ctx.currentNumberFormat);
				if (data.getPolicyNumber() != null && !data.getPolicyNumber().equals("")) {
					bought = true;
					added = true;
				} else {
					bought = false;
					added = ctx.insuranceAddedToCart;
				}
			}
			else{
				showInsuranceBox = false;
				if(data == null && insuranceConditionOK){
					logger.warn("No InsurancePolicyData found in session for PNR " + ctx.pnr);
				}
			}
		}
		catch(Exception e){
			logger.error("Unexpected error" , e);
		}
	}
	
	private boolean checkInsuranceConditions(CheckinSessionContext ctx){
		boolean ok = false;
		List<CheckinPassengerStatusEnum> checkedInStatus 
			= Arrays.asList(CheckinPassengerStatusEnum.ALREADY_CHECKED_IN
			, CheckinPassengerStatusEnum.CHECKED_IN);
		
		/*ITA market*/
		ok = "IT".equalsIgnoreCase(ctx.market);
		
		if (ctx.selectedRoute.isBus()) {
			return false;
		}
		
		if(ctx.passengers != null && ctx.selectedRoute != null){
			/*All passenger are checked in*/
			ok = ok && ctx.passengers.stream().allMatch(pax 
					-> checkedInStatus.contains(pax.getStatus()));
			
			/*Insurance must be visible only on outgoing route*/
			ok = ok && ctx.selectedRoute.getId() == 0;
			
			/*Only for flights which departure is in ITA*/
			ok = ok && "IT".equalsIgnoreCase(ctx.selectedRoute.getFlights()
					.get(0).getFrom().getCountryCode());
		}
		return ok;
	}
	
	public MmbPriceRender getPrice() {
		return price;
	}
	
	public boolean getBought() {
	       
        return bought;
	}
	
	public boolean getAdded() {
	       
        return added;
	}

	public boolean isShowInsuranceBox() {
		return showInsuranceBox;
	}
	
}
