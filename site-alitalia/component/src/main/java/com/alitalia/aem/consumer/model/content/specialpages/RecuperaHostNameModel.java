package com.alitalia.aem.consumer.model.content.specialpages;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;

@Model(adaptables={SlingHttpServletRequest.class})
public class RecuperaHostNameModel {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	private String hostName = null;

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;
	
	@Inject
    private AlitaliaConfigurationHolder configuration;
	
	@PostConstruct
	protected void initModel() {
		logger.debug("RecuperaHostNameModel initModel");
		String prefix = "http://";
		hostName = prefix + configuration.getExternalDomain();
		logger.debug("hostName: " + hostName);	
	}
	
	public String getHostName(){
		return hostName;
	}
}
