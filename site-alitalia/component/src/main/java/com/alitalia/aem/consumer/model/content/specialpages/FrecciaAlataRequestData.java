package com.alitalia.aem.consumer.model.content.specialpages;

public class FrecciaAlataRequestData {

	public static final String NAME = "FrecciaAlataRequestData";

	private String error;

	private String name;
	private String surname;
	private String millemigliacode;
	private String email;
	private String phonenumber;
	private String membername;
	private String membersurname;
	private String promocode;
	private String conditionsaccepted;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getMillemigliacode() {
		return millemigliacode;
	}

	public void setMillemigliacode(String millemigliacode) {
		this.millemigliacode = millemigliacode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getMembername() {
		return membername;
	}

	public void setMembername(String membername) {
		this.membername = membername;
	}

	public String getMembersurname() {
		return membersurname;
	}

	public void setMembersurname(String membersurname) {
		this.membersurname = membersurname;
	}

	public String getPromocode() {
		return promocode;
	}

	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}

	public String getConditionsaccepted() {
		return conditionsaccepted;
	}

	public void setConditionsaccepted(String conditionsaccepted) {
		this.conditionsaccepted = conditionsaccepted;
	}

	@Override
	public String toString() {
		return "FrecciaAlataRequestData [name=" + name + ", surname=" + surname + ", phonenumber=" + phonenumber
				+ ", email=" + email + ", millemigliacode=" + millemigliacode + ", membername=" + membername
				+ ", membersurname=" + membersurname + ", promocode=" + promocode + ", conditionsaccepted=" + conditionsaccepted + "]";
	}

}