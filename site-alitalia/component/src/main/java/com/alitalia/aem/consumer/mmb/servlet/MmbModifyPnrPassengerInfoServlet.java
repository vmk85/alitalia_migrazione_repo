package com.alitalia.aem.consumer.mmb.servlet;

import java.util.Calendar;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.utils.DateUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "mmbmodifypnrpassengerinfo" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class MmbModifyPnrPassengerInfoServlet extends GenericMmbFormValidatorServlet {
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile MmbSession mmbSession;	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		MmbSessionContext ctx = getMmbSessionContext(request);
		
		/* Recupero dati dalla request */		
		String nazionalita = request.getParameter("nazionalita");
		String residenza = request.getParameter("residenza");
		String giornoDataNascita = request.getParameter("giorno_dataNascita");
		String meseDataNascita = request.getParameter("mese_dataNascita");
		String annoDataNascita = request.getParameter("anno_dataNascita");
		String genere = request.getParameter("genere");
		String indirizzoDestinazione = request.getParameter("indirizzoDestinazione");
		String paeseDestinazione = request.getParameter("paeseDestinazione");
		String cittaDestinazione = request.getParameter("cittaDestinazione");
		String capDestinazione = request.getParameter("capDestinazione");
		String numeroPassaporto = request.getParameter("numeroPassaporto");
		String giornoScadenzaPassaporto = request.getParameter("giorno_scadenzaPassaporto");
		String meseScadenzaPassaporto = request.getParameter("mese_scadenzaPassaporto");
		String annoScadenzaPassaporto = request.getParameter("anno_scadenzaPassaporto");
		
		/* Validazione */
		Validator validator = new Validator();
		
		validator.addDirectCondition("nazionalita", nazionalita, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("residenza", residenza, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");

		validator.addDirectCondition("genere", genere, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("numeroPassaporto", numeroPassaporto, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		if (ctx.route.getApisTypeRequired().equals(MmbApisTypeEnum.PLUS)) {
			validator.addDirectCondition("indirizzoDestinazione", indirizzoDestinazione, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			
			validator.addDirectCondition("paeseDestinazione", paeseDestinazione, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			
			validator.addDirectCondition("cittaDestinazione", cittaDestinazione, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			
			validator.addDirectCondition("capDestinazione", capDestinazione, 
					BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		}
		
		// data nascita
		validator.addDirectCondition("giorno_dataNascita", giornoDataNascita, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("mese_dataNascita", meseDataNascita, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("anno_dataNascita", annoDataNascita, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		String dataNascita = giornoDataNascita + "/" + meseDataNascita + "/" + annoDataNascita;
		validator.addDirectCondition("giorno_dataNascita", dataNascita, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate");
		validator.addDirectCondition("mese_dataNascita", dataNascita, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate");
		validator.addDirectCondition("anno_dataNascita", dataNascita, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate");

		// data scadenza passaporto
		validator.addDirectCondition("giorno_scadenzaPassaporto", giornoScadenzaPassaporto, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("mese_scadenzaPassaporto", meseScadenzaPassaporto, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("anno_scadenzaPassaporto", annoScadenzaPassaporto, 
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		String dataScadenzaPassaporto = 
				giornoScadenzaPassaporto + "/" + meseScadenzaPassaporto + "/" + annoScadenzaPassaporto;
		validator.addDirectCondition("giorno_scadenzaPassaporto", dataScadenzaPassaporto, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate");
		validator.addDirectCondition("mese_scadenzaPassaporto", dataScadenzaPassaporto, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate");
		validator.addDirectCondition("anno_scadenzaPassaporto", dataScadenzaPassaporto, 
				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD, "isDate");

		ResultValidation resultValidation = validator.validate();
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		
		MmbSessionContext ctx = getMmbSessionContext(request);
		
		int passengerIndex = Integer.parseInt(request.getParameter("passengerIndex"));
		boolean isApisDataEmpty = Boolean.getBoolean(request.getParameter("isApisDataEmpty"));
		String nazionalita = request.getParameter("nazionalita");
		String residenza = request.getParameter("residenza");
		String nome = request.getParameter("nome");
		String secondoNome = request.getParameter("secondoNome");
		String cognome = request.getParameter("cognome");
		String giornoDataNascita = request.getParameter("giorno_dataNascita");
		String meseDataNascita = request.getParameter("mese_dataNascita");
		String annoDataNascita = request.getParameter("anno_dataNascita");
		String genere = request.getParameter("genere");
		String indirizzoDestinazione = request.getParameter("indirizzoDestinazione");
		String paeseDestinazione = request.getParameter("paeseDestinazione");
		String cittaDestinazione = request.getParameter("cittaDestinazione");
		String capDestinazione = request.getParameter("capDestinazione");
		String numeroPassaporto = request.getParameter("numeroPassaporto");
		String giornoScadenzaPassaporto = request.getParameter("giorno_scadenzaPassaporto");
		String meseScadenzaPassaporto = request.getParameter("mese_scadenzaPassaporto");
		String annoScadenzaPassaporto = request.getParameter("anno_scadenzaPassaporto");
		
		Calendar dataNascita = DateUtils.createCalendar(
				giornoDataNascita, meseDataNascita, annoDataNascita);
		Calendar dataScadenzaPassaporto = DateUtils.createCalendar(
				giornoScadenzaPassaporto, meseScadenzaPassaporto, annoScadenzaPassaporto);
		
		mmbSession.modifyPassengerInfo(getMmbSessionContext(request), passengerIndex, 
				nazionalita, residenza, nome, secondoNome, cognome, 
				dataNascita, genere, numeroPassaporto, dataScadenzaPassaporto, indirizzoDestinazione,
				paeseDestinazione, cittaDestinazione, capDestinazione, isApisDataEmpty);
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		json.key("result").value("OK");
		json.key("message").value(ctx.i18n.get(MmbConstants.MESSAGE_SUCCESS_MODIFY_PASSENGER_DATA));
		json.endObject();
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
	
}
