package com.alitalia.aem.consumer.booking.model.extrabaggage;


import java.util.*;

public class AncillariesGetExtraBagsResponse extends AncillariesGetResponse
{
	private ArrayList<AncillariesExtraBagsPassengerResponse> ancillariesPassenger;
	public final ArrayList<AncillariesExtraBagsPassengerResponse> getancillariesPassenger()
	{
		return ancillariesPassenger;
	}
	public final void setancillariesPassenger(ArrayList<AncillariesExtraBagsPassengerResponse> value)
	{
		ancillariesPassenger = value;
	}
}