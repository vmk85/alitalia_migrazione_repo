package com.alitalia.aem.consumer.booking.model.extrabaggage;


import java.util.*;

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
///#region References
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
///#endregion

public class Itinerary
{
	private String type;
	public final String gettype()
	{
		return type;
	}
	public final void settype(String value)
	{
		type = value;
	}
	private String id;
	public final String getid()
	{
		return id;
	}
	public final void setid(String value)
	{
		id = value;
	}
	private ArrayList<ItineraryPart> itineraryParts;
	public final ArrayList<ItineraryPart> getitineraryParts()
	{
		return itineraryParts;
	}
	public final void setitineraryParts(ArrayList<ItineraryPart> value)
	{
		itineraryParts = value;
	}
}