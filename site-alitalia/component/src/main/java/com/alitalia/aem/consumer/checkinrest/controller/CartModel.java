package com.alitalia.aem.consumer.checkinrest.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.alitalia.aem.common.data.checkinrest.model.getcart.AziInsurancePolicy;
import com.alitalia.aem.common.data.checkinrest.model.getcart.CheckinCart;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.getcart.Cart;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetCartRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetCartResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;
import com.alitalia.aem.web.component.webcheckin.delegaterest.IPaymentDelegate;

@Model(adaptables = {SlingHttpServletRequest.class})
public class CartModel extends GenericCheckinModel {

    @Self
    private SlingHttpServletRequest request;

    @Inject
    private SlingHttpServletResponse response;

    @Inject
    private AlitaliaConfigurationHolder configuration;

    @Inject
    private volatile IPaymentDelegate paymentDelegate;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Cart cart;

    private String price;

    private String decimale;

    private String separatore;

    private String thousandSeparator;

    @PostConstruct
    protected void initModel() throws Exception {
        try {
            super.initBaseModel(request);

            if (ctx == null) {
                logger.debug("[CartModel] - Contesto assente.");
                return;
            } else {
                String decimalSeparator = new HierarchyNodeInheritanceValueMap(request.getResource().getParent().getParent().getParent().getParent().getParent()).getInherited("decimalSeparator", String.class);
                try {
                    this.setThousandSeparator(new HierarchyNodeInheritanceValueMap(request.getResource().getParent().getParent().getParent().getParent().getParent()).getInherited("thousandSeparator", String.class));
                }catch(Exception e){}


                cart = retrieveCartImport(decimalSeparator);

                float cartTotal = 0;
                CheckinCart checkinCart = cart.getCheckinCart();
                AziInsurancePolicy aziInsurancePolicy = cart.getAziInsurancePolicy();

                if (checkinCart != null) {
                    float cartPriceToBeIssued = checkinCart.getCartPriceToBeIssued();
                    cartTotal += cartPriceToBeIssued;
                }

                if (aziInsurancePolicy != null) {
                    float totalInsuranceCost = aziInsurancePolicy.getTotalInsuranceCost() * 100;
                    cartTotal += totalInsuranceCost;
                }

                cart.setTotalPriceToBeIssued(cartTotal);

                this.setCart(cart);

                String decimalDigit = new HierarchyNodeInheritanceValueMap(request.getResource().getParent().getParent().getParent().getParent().getParent()).getInherited("decimalDigit", String.class);


                this.setSeparatore(decimalSeparator);

                String decimale2 = "";
                for (int i = 0; i < Integer.parseInt(decimalDigit); i++){
                    decimale2 = "0" + decimale2;
                }

                this.setDecimale(decimale2);
                int totCartInfo = (int) cartTotal;
                price = String.valueOf(totCartInfo);
                if (totCartInfo != 0) {
                    if (this.marketCode != "jp") {
                        price = price.substring(0, price.length() - Integer.parseInt(decimalDigit)) + (Integer.parseInt(decimalDigit) != 0 ? decimalSeparator + price.substring(price.length() - Integer.parseInt(decimalDigit), price.length()) : "");
                    } else {
                        price = price + decimalSeparator + decimale2;

                    }
                }
            }
        } catch (Exception ex) {
            logger.error("[CartModel] - Unexpected error: ", ex);
        }
    }

    private Cart retrieveCartImport(String decimalSeparator) {
        Cart newCart = null;
        try {
            CheckinGetCartRequest cartRequest = new CheckinGetCartRequest(IDFactory.getTid(), IDFactory.getSid(request));
            cartRequest.setPnr(ctx.pnr);
            cartRequest.setMarket(ctx.market);
            cartRequest.setLanguage(ctx.language);
            cartRequest.setConversationID(ctx.conversationId);
            cartRequest.setSeparatore(decimalSeparator);

            logger.debug("[CheckinSession] Chiamata del servizio getCart...");
            CheckinGetCartResponse serviceResponse = paymentDelegate.getCart(cartRequest);

            //Oggetto Data dopo la chiamata al servizio
            newCart = serviceResponse.getCart();
        } catch (Exception e) {
            logger.error("[CartModel][retrieveCartImport] - errore durante il recupero del "
                    + "nuovo importo aggiornato del carrello." + e.getMessage());
        }
        return newCart;
    }


    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDecimale() {
        return decimale;
    }

    public void setDecimale(String decimale) {
        this.decimale = decimale;
    }

    public String getSeparatore() {
        return separatore;
    }

    public void setSeparatore(String separatore) {
        this.separatore = separatore;
    }

    public String getThousandSeparator() {
        return thousandSeparator;
    }

    public void setThousandSeparator(String thousandSeparator) {
        this.thousandSeparator = thousandSeparator;
    }
}
