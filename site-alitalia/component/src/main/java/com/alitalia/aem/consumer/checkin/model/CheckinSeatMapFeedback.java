package com.alitalia.aem.consumer.checkin.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryUpgradeDetailData;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinFeedbackRowRender;
import com.alitalia.aem.consumer.checkin.CheckinPassenger;
import com.alitalia.aem.consumer.checkin.CheckinPassengerSeat;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinSeatMapFeedback extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private CheckinSession checkinSession;
	
	private List<CheckinFlightData> flights;
	private Map<String, Map<String, Boolean>> assignedSeats;
	private List<CheckinFeedbackRowRender> feedbacks;
	
	@PostConstruct
	protected void initModel() {
		
		try {
			initBaseModel(request);
			
			List<CheckinPassengerData> checkedInPassengers = CheckinUtils.getCheckedInPassengers(ctx.passengers);
			flights = CheckinUtils.getFlightsList(ctx.selectedRoute);
			assignedSeats = checkinSession.getAssignedSeatsWithComfortInfo(ctx, checkedInPassengers);
			feedbacks = new ArrayList<CheckinFeedbackRowRender>();
			
			List<MmbAncillaryStatusEnum> issuedStatus = new ArrayList<MmbAncillaryStatusEnum>();
			issuedStatus.add(MmbAncillaryStatusEnum.TO_BE_ISSUED);
			
			Map<String, MmbAncillaryData> ancillariesAssignedSeats = checkinSession.getAncillariesAssignedSeats(ctx);
			for (CheckinPassengerData passengerData : checkedInPassengers) {
				CheckinPassenger passenger = checkinSession.toCheckinPassenger(passengerData);
				List<MmbAncillaryData> upgrades = checkinSession.getAncillaryByTypeStatusAndETicket(ctx
						, MmbAncillaryTypeEnum.UPGRADE, issuedStatus, passengerData.getEticket());
				CheckinFeedbackRowRender feedback = new CheckinFeedbackRowRender();
				BigDecimal totalAmout = new BigDecimal(0);
				/*Check if a seat is added to cart only if passenger hasn't add to cart an upgrade*/
				if(!upgrades.isEmpty()){
					feedback.setUpgrade(true);
					/*FIXME si assume che le tratte con scalo abbiano un solo upgrade per tutta la tratta*/
					MmbCompartimentalClassEnum compClass = ((CheckinAncillaryUpgradeDetailData) upgrades.get(0).getAncillaryDetail())
							.getCompartimentalClass();
					BigDecimal amount = upgrades.get(0).getAmount();
					if(amount != null && !amount.equals(BigDecimal.ZERO)){
						totalAmout = totalAmout.add(amount);
					}
					MmbCompartimentalClassEnum businessClass = checkinSession.getBusinessCompClass(ctx);
					MmbCompartimentalClassEnum premiumEcoClass = checkinSession.getPremiumEcoCompClass(ctx);
					if(businessClass.equals(compClass)){
						feedback.setUpgradeClassText(i18n.get(CheckinConstants.CHECKIN_UPGRADE_BUSINESS_CART_LABEL));					}
					else if(premiumEcoClass != null && premiumEcoClass.equals(compClass)){
						feedback.setUpgradeClassText(i18n.get(CheckinConstants.CHECKIN_UPGRADE_PREMIUM_ECONOMY_CART_LABEL));
					}
					else{
						feedback.setUpgradeClassText(i18n.get(CheckinConstants.CHECKIN_UPGRADE_OTHER_CART_LABEL));
					}
				}
				else{
					Map<String, CheckinPassengerSeat> seats = checkinSession.getPassengerSeats(ctx, passengerData, ancillariesAssignedSeats);
					int standard = 0;
					int comfort = 0;
					for (String key : seats.keySet()) {
						CheckinPassengerSeat seat = seats.get(key);
						if (seat.getPrice() != null && seat.getPrice().getAmount() != null) { 
							totalAmout = totalAmout.add(seat.getPrice().getAmount());
						}
						if (seat.isComfortSeat()) {
							comfort++;
						} else {
							standard++;
						}
					}
					feedback.setComfortSeat(comfort);
					feedback.setStandardSeat(standard);
					feedback.setChild(passenger.isChild());
					feedback.setHasInfant(passenger.isHasInfant());
				}
				feedback.setName(passenger.getName() + " " +
						(passenger.getMiddleName() != null ? passenger.getMiddleName() + " " : "") 
						+ passenger.getLastName());
				feedback.setTotalPrice(new MmbPriceRender(totalAmout, ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				feedbacks.add(feedback);
			}
			
		} catch (Exception e) {
			logger.error("Unexpected error:", e);
			throw e;
		}
	}

	public List<CheckinFlightData> getFlights() {
		return flights;
	}

	public Map<String, Map<String, Boolean>> getAssignedSeats() {
		return assignedSeats;
	}

	public List<CheckinFeedbackRowRender> getFeedbacks() {
		return feedbacks;
	}
}
