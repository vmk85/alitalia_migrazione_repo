package com.alitalia.aem.consumer.servlet.specialpages;

import com.alitalia.aem.common.data.home.SecureQuestionData;
import com.alitalia.aem.common.data.home.enumerations.TipoProfiloEnum;
import com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.MillemigliaRecuperaCredenzialiData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.MilleMigliaServiceClient;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.wsdl.IMilleMigliaServiceFindProfileServiceFaultFaultFaultMessage;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.wsdl.IMilleMigliaServiceGetDatiSicurezzaProfiloServiceFaultFaultFaultMessage;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.*;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloRequest;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.StaticDataServiceMillemigliaClient;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.SecureQuestionStaticDataResponse;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.StaticDataRequest;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd2.ArrayOfSecureQuestion;
import com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd2.SecureQuestion;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;
import com.alitalia.aem.common.messages.home.*;

import javax.servlet.Servlet;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementRef;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import org.apache.commons.lang3.StringUtils;

@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = {"millemiglia-recuperacredenziali-submit"}),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")

public class MillemigliaRecuperaCredenzialiServlet extends GenericFormValidatorServlet {
    private String selectorString;
    
	private ComponentContext componentContext;
	private MilleMigliaServiceClient milleMigliaServiceClient;
	private StaticDataServiceMillemigliaClient staticDataServiceMillemigliaClient;

    private String sessionName;

    private Locale locale;
	private String itemCache;
	private String languageCode;
	private String market;

	private I18n i18n;

    private MillemigliaRecuperaCredenzialiData millemigliaRecuperaCredenzialiData = new MillemigliaRecuperaCredenzialiData() ;
	private String retrieveResetChoice;// [resetPassword,retrieveUsername]
	private Integer stato ; // [1 scelta azione, 2 find profile, 3 salvataggio nuova Password o mostra/invia username, 4 finale??]
    private String trackCode;
    private String username;
    private String usernameForm;
	private boolean flagVerificaUserName_OK;
	private String alias;
	private String mmCode;
	private String surname;

	private String idProfilo;
    private String password_MD5;
    private String confermaPassword_MD5;
    private String password;
    private String confermaPassword;
	private boolean flagVerificaPassword_OK = false;
	private String certifiedEmailAccount;
	private boolean flagVerificaEmailUser_OK = false;
	private String certifiedCountryNumber;
	private String certifiedPhoneNumber;
	private boolean flagVerificaCellulare_OK = false;
	private int secretQuestionID;
	private String secretQuestionCode;
	private String secretQuestionText;
	private String rispostaSegreta_MD5;
	private boolean flagVerificaRisposta_OK = false;

	private boolean isLockedOut = false;

	private String flagVerifica_OTP_DS_OK;
	private TidyJSONWriter json ;



	private String currentPageUrl = null;
	private String homePagePageUrl = null;
	private String isError = null;
	private String msgId = null;
	private String description = null;
	private String responseCode = null;



	public String setCurrentPageUrl(SlingHttpServletRequest request) {
		if (currentPageUrl == null) {
			String protocol = configuration.getHttpsEnabled() ? "https://" : "http://";
			String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
			baseUrl = request.getResourceResolver().map(baseUrl);
			currentPageUrl = protocol + configuration.getExternalDomain() + baseUrl + "/special-pages/recovery_mm_crd.html";
		}
		return currentPageUrl;
	}
	public String getCurrentPageUrl() {
		return currentPageUrl;
	}

	public String setHomePageUrl(SlingHttpServletRequest request) {
		if (homePagePageUrl == null) {
			String protocol = configuration.getHttpsEnabled() ? "https://" : "http://";
			String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
			baseUrl = request.getResourceResolver().map(baseUrl);
			homePagePageUrl = protocol + configuration.getExternalDomain() + baseUrl + "/homepage.html";
		}
		return homePagePageUrl;
	}

	public String getHomePagePageUrl() {
		return homePagePageUrl;
	}



//	private com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory objectFactoryMMS =
//			new com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory();
//	private com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.ObjectFactory objectFactorySDS =
//			new com.alitalia.aem.ws.mypersonalarea.staticdataservice.xsd1.ObjectFactory();


	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegate staticDataDelegate;

	@Reference
	private volatile AlitaliaConfigurationHolder configuration;


	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[MillemigliaRecuperaCredenzialiServlet] validateForm");

		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			return validator.validate();

		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Error during parameters validation", e);
			return null;
		}

	}
    /**
     * Set Validator parameters for validation
     *
     * @param request
     * @param validator
     */
    private Validator 	setValidatorParameters(SlingHttpServletRequest request,
                                             Validator validator) {
        logger.debug("[MillemigliaRecuperaCredenzialiServlet] setValidatorParameters");


        getSessionData(request);

        stato = Integer.valueOf(request.getParameter("passo"));


        if (stato == 1){
            // non occurre validate i radio button se è stato sempre valorizzato il default
        }
        if (stato == 2){

//            username = request.getParameter("username");
//            alias = request.getParameter("alias");
//            mmCode =request.getParameter("codiceMillemiglia");
//            surname = request.getParameter("surname");

			mmCode =request.getParameter("codiceMillemiglia");
			surname = request.getParameter("surname");

			username = "";
			alias = "";

			if(!StringUtils.isNumeric(mmCode))
			{
				username=mmCode;
				mmCode="";
			}


//
//            if (!alias.equals("")){
//                validator.addDirectConditionMessagePattern("alias", alias,
//                        I18nKeyCommon.MESSAGE_INVALID_FIELD,
//                        I18nKeySpecialPage.MILLEMIGLIAALIAS_ERROR_NOT_VALID,
//                        "isAlphabeticWithAccentAndSpaces");
//
//            }
            if (!mmCode.equals("")){
                validator.addDirectConditionMessagePattern("codiceMillemiglia", mmCode,
                        I18nKeyCommon.MESSAGE_INVALID_FIELD,
                        I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID,
                        "isNumber");


            }

            validator.addDirectConditionMessagePattern("surname", surname,
                    I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
                    I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
            validator.addDirectConditionMessagePattern("surname", surname,
                    I18nKeyCommon.MESSAGE_INVALID_FIELD,
                    I18nKeySpecialPage.SURNAME_ERROR_EMPTY,
                    "isAlphabeticWithSpaces");


            if (retrieveResetChoice.equals("resetPassword")) {
                 if (!username.equals("")){
                    // validatore dell username
                    validator.addDirectConditionMessagePattern("codiceMillemiglia", username,
                            I18nKeyCommon.MESSAGE_INVALID_FIELD,
                            I18nKeySpecialPage.NAME_ERROR_EMPTY, "isMMUserName");
                }

				if (username.equals("") && mmCode.equals("") ){

					validator.addDirectConditionMessagePattern("codiceMillemiglia", mmCode,
							I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
							I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
				}

            }else{

				validator.addDirectConditionMessagePattern("codiceMillemiglia", mmCode,
						I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
						I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");

			}

        }
        if (stato == 3){
            password = request.getParameter("password");
            confermaPassword = request.getParameter("confermaPassword");
//            if (retrieveResetChoice.equals("resetPassword")) {
//                // validatori password
//                validator.addDirectConditionMessagePattern("password", password,
//                        I18nKeyCommon.MESSAGE_INVALID_FIELD,
//                        I18nKeySpecialPage.MILLEMIGLIAPASSWORD_ERROR_NOT_VALID, "isPasswordStrongAuthentication");
//                validator.addDirectConditionMessagePattern("confermaPassword", confermaPassword,
//                        I18nKeyCommon.MESSAGE_INVALID_FIELD,
//                        I18nKeySpecialPage.MILLEMIGLIACONFERMAPASSWORD_ERROR_NOT_VALID, "isPasswordStrongAuthentication");
//
//            }

        }
        
        return validator;

    }

    @Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {

		setCurrentPageUrl(request);
		setHomePageUrl(request);




		selectorString = request.getRequestPathInfo().getSelectorString();

		if(selectorString==null){
			selectorString="millemiglia-recuperacredenziali-submit";
		}

		isError = "FALSE";
		msgId="NO_ERROR";
		description="NO_ERROR";
		responseCode="NO_ERROR";

        recuperaCredenzialiSubmit(request,response);
		/*switch (selectorString) {
			case "millemiglia-recuperacredenziali-submit":
				recuperaCredenzialiSubmit(request,response);
				break;
			case "millemiglia-recuperacredenziali-GetSecureAnswerMD5":
				recuperacredenzialiGetSecureAnswerMD5(request,response);
				break;
			case "millemiglia-recuperacredenziali-ConvertSecureAnswerMD5":
				recuperacredenzialiConvertSecureAnswerMD5(request,response);
				break;
		}*/



		response.setContentType("application/json");
		json = new TidyJSONWriter(response.getWriter());

		try {
			json.object();
			json.key("currentPageUrl").value(currentPageUrl);
			json.key("homePageUrl").value(homePagePageUrl);
			json.key("isError").value(isError);
			json.key("msgId").value(msgId);
			json.key("description").value(msgId);
			json.key("responseCode").value(responseCode);
			json.endObject();
		} catch (Exception e) {
			logger.error("[MillemigliaRecuperaCredenzialiServlet] Il Customer non è stato aggiornato alle nuove credenziali.", e);
		}

	}

	private void recuperaCredenzialiSubmit(SlingHttpServletRequest request,
									   SlingHttpServletResponse response)throws IOException {
	logger.debug("[MillemigliaRecuperaCredenzialiServlet] performSubmit");

	getSessionData(request);
	stato = Integer.valueOf(request.getParameter("passo"));

	if (locale == null){
		locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		i18n = new I18n(resourceBundle);
		itemCache = AlitaliaUtils.getLanguage(locale.toLanguageTag()).toUpperCase();
		languageCode = AlitaliaUtils.getLanguage(locale.toLanguageTag()).toUpperCase();
		market = locale.getCountry();

	}


	if (stato == 0){
		resetSessionData(request);
		stato = 1;

	}else 	if (stato == 1){
		retrieveResetChoice = request.getParameter("recuperaCredenziali");
		stato = 2;

	}else 	if (stato == 2){


//		username = request.getParameter("username");
//		mmCode = request.getParameter("codiceMillemiglia");
//		alias = request.getParameter("alias");
//		surname = request.getParameter("surname");

		mmCode =request.getParameter("codiceMillemiglia");

		surname = request.getParameter("surname");

		username = "";
		alias = "";

		if(!StringUtils.isNumeric(mmCode))
		{
			username=mmCode;
			mmCode="";
		}

			FindProfileRequestLocal findProfileRequestLocal = new FindProfileRequestLocal();
		findProfileRequestLocal.setUsername(username);
		findProfileRequestLocal.setCodiceMM(mmCode);
		findProfileRequestLocal.setAlias(alias);
		findProfileRequestLocal.setCognome(surname);

		FindProfileResponseLocal findProfileResponseLocal =
				consumerLoginDelegate.FindProfile(findProfileRequestLocal);

		if ((findProfileResponseLocal != null) && (findProfileResponseLocal.getFlagIDProfiloExists() != null) && (findProfileResponseLocal.getFlagIDProfiloExists())) {
			idProfilo = findProfileResponseLocal.getIdProfilo();


			millemigliaRecuperaCredenzialiData.setIdProfilo(idProfilo);

			GetDatiSicurezzaProfiloRequestLocal datiSicurezzaProfiloRequest =
					new GetDatiSicurezzaProfiloRequestLocal();

			datiSicurezzaProfiloRequest.setIdProfilo(idProfilo);


			GetDatiSicurezzaProfiloResponseLocal getDatiSicurezzaProfiloResponse =
					consumerLoginDelegate.GetDatiSicurezzaProfilo(datiSicurezzaProfiloRequest);

			if (getDatiSicurezzaProfiloResponse != null){

				boolean isKid=false;
				TipoProfiloEnum tipoProfiloUtente = getDatiSicurezzaProfiloResponse.getTipoProfilo();

				if (tipoProfiloUtente != null){
					if (tipoProfiloUtente == TipoProfiloEnum.MM_KIDS){
						isKid=true;
					}
				}

				isLockedOut = getDatiSicurezzaProfiloResponse.getLockedout();

				millemigliaRecuperaCredenzialiData.setLockedOut(isLockedOut);
				username = getDatiSicurezzaProfiloResponse.getUsername();
				usernameForm = "*****************";
				millemigliaRecuperaCredenzialiData.setUsernameForm(usernameForm);
				millemigliaRecuperaCredenzialiData.setUsername(username);
				flagVerificaUserName_OK = getDatiSicurezzaProfiloResponse.getFlagVerificaUserNameOK();
				millemigliaRecuperaCredenzialiData.setFlagVerificaUserName_OK(flagVerificaUserName_OK);
				password_MD5 = getDatiSicurezzaProfiloResponse.getPassword();
				millemigliaRecuperaCredenzialiData.setPassword_MD5(password_MD5);
				flagVerificaPassword_OK = getDatiSicurezzaProfiloResponse.getFlagVerificaPasswordOK();
				millemigliaRecuperaCredenzialiData.setFlagVerificaPassword_OK(flagVerificaPassword_OK);
				certifiedEmailAccount = getDatiSicurezzaProfiloResponse.getEmailAccount();
				millemigliaRecuperaCredenzialiData.setCertifiedEmailAccount(certifiedEmailAccount);
				flagVerificaEmailUser_OK = getDatiSicurezzaProfiloResponse.getFlagVerificaEmailAccountOK();
				millemigliaRecuperaCredenzialiData.setFlagVerificaEmailUser_OK(flagVerificaEmailUser_OK);
				certifiedCountryNumber = getDatiSicurezzaProfiloResponse.getPrefissoNazioneCellulare();
				millemigliaRecuperaCredenzialiData.setCertifiedCountryNumber(certifiedCountryNumber);
				certifiedPhoneNumber = getDatiSicurezzaProfiloResponse.getCellulare();
				millemigliaRecuperaCredenzialiData.setCertifiedPhoneNumber(certifiedPhoneNumber);
				flagVerificaCellulare_OK = getDatiSicurezzaProfiloResponse.getFlagVerificaCellulareOK();
				millemigliaRecuperaCredenzialiData.setFlagVerificaCellulare_OK(flagVerificaCellulare_OK);
				
				secretQuestionID = getDatiSicurezzaProfiloResponse.getSecureQuestionID();
                if(secretQuestionID==0){secretQuestionID=2;}   //TODO da rimuovere se BL OK
				
                millemigliaRecuperaCredenzialiData.setSecretQuestionID(secretQuestionID);

                rispostaSegreta_MD5 = getDatiSicurezzaProfiloResponse.getSecureAnswer();
				if(rispostaSegreta_MD5.equals("")){rispostaSegreta_MD5=DigestUtils.md5Hex("GATTO");}    //TODO da rimuovere se BL OK
				
                millemigliaRecuperaCredenzialiData.setRispostaSegreta_MD5(rispostaSegreta_MD5);
				flagVerificaRisposta_OK = getDatiSicurezzaProfiloResponse.getFlagVerificaRispostaOK();
				millemigliaRecuperaCredenzialiData.setFlagVerificaRisposta_OK(flagVerificaRisposta_OK);

				secretQuestionText = GetDomandaSicurezza(request, secretQuestionID);
				millemigliaRecuperaCredenzialiData.setSecretQuestionText(secretQuestionText);
				secretQuestionCode = "securequestion." + getDatiSicurezzaProfiloResponse.getSecureQuestionCode();
				if(secretQuestionCode.equals("")){secretQuestionCode="securequestion.CLS.MM.DS.CaneGatto";}    //TODO da rimuovere se BL OK

				millemigliaRecuperaCredenzialiData.setSecretQuestionCode(secretQuestionCode);


				if ((username.equals("") || isKid) || (!flagVerificaRisposta_OK && !flagVerificaCellulare_OK)){

					isError="TRUE";
					if (isKid){
						msgId = i18n.get("specialpage.recuperacredenziali.message.isKid");
						description = "CUSTOMER_IS_KIDS";
						responseCode = "CUSTOMER_IS_KIDS";
					}else {
						msgId = i18n.get("specialpage.recuperacredenziali.message.notSAcustomer");
						description = "CUSTOMER_NOT_SA_UPGRADE";
						responseCode = "CUSTOMER_NOT_SA_UPGRADE";
					}

//					response.setContentType("application/json");
//					json = new TidyJSONWriter(response.getWriter());
//
//					try {
//						json.object();
//						json.key("currentPageUrl").value(currentPageUrl);
//						json.key("homePageUrl").value(homePagePageUrl);
//						json.key("isError").value("TRUE");
//                        json.key("msgId").value(i18n.get("specialpage.recuperacredenziali.message.notSAcustomer"));
//						json.key("description").value("CUSTOMER_NOT_SA_UPGRADE");
//						json.key("responseCode").value("CUSTOMER_NOT_SA_UPGRADE");
//						json.endObject();
//					} catch (Exception e) {
//						logger.error("[MillemigliaRecuperaCredenzialiServlet] Il Customer non è stato aggiornato alle nuove credenziali.", e);
//					}
					stato = 2;
				}else {
					stato = 3;
				}
			} else {

				isError="TRUE";
				msgId=i18n.get("specialpage.recuperacredenziali.message.customersecuredatanotfound");
				description="CUSTOMER_SECUREDATA_NOT_FOUND";
				responseCode="CUSTOMER_SECUREDATA_NOT_FOUND";

//				response.setContentType("application/json");
//				json = new TidyJSONWriter(response.getWriter());
//
//				try {
//					json.object();
//					json.key("currentPageUrl").value(currentPageUrl);
//					json.key("homePageUrl").value(homePagePageUrl);
//					json.key("isError").value("TRUE");
//                    json.key("msgId").value(i18n.get("specialpage.recuperacredenziali.message.customersecuredatanotfound"));
//					json.key("description").value("CUSTOMER_SECUREDATA_NOT_FOUND");
//					json.key("responseCode").value("CUSTOMER_SECUREDATA_NOT_FOUND");
//					json.endObject();
//				} catch (Exception e) {
//					logger.error("[MillemigliaRecuperaCredenzialiServlet] Dati Sicurezza non trovati.", e);
//				}
			}
		} else {

			isError="TRUE";
			msgId=i18n.get("specialpage.recuperacredenziali.message.customernotfound");
			description="CUSTOMER_NOT_FOUND";
			responseCode="CUSTOMER_NOT_FOUND";


//			response.setContentType("application/json");
//			json = new TidyJSONWriter(response.getWriter());
//
//			try {
//				json.object();
//				json.key("currentPageUrl").value(currentPageUrl);
//				json.key("homePageUrl").value(homePagePageUrl);
//				json.key("isError").value("TRUE");
//				json.key("msgId").value(i18n.get("specialpage.recuperacredenziali.message.customernotfound"));
//				json.key("description").value("CUSTOMER_NOT_FOUND");
//				json.key("responseCode").value("CUSTOMER_NOT_FOUND");
//				json.endObject();
//			} catch (Exception e) {
//				logger.error("[MillemigliaRecuperaCredenzialiServlet] Customer Millemiglia npn trovato.", e);
//			}
			stato = 2;
		}

	}else 	if (stato == 3){
		flagVerifica_OTP_DS_OK = request.getParameter("flagVerifica_OTP_DS_OK");
		if (flagVerifica_OTP_DS_OK.equals("True")){

			if (retrieveResetChoice.equals("resetPassword")){

				password = request.getParameter("password");
				password_MD5 = DigestUtils.md5Hex(password);
				confermaPassword = request.getParameter("confermaPassword");
				confermaPassword_MD5 = request.getParameter("confermaPassword");
				if (password.equals(confermaPassword)) {
					try {
						setDatiSicurezzaProfilo();
						stato= 4;
					}catch (Exception e){
						// impossibile salvare

					}
				}else {

					isError="TRUE";
					msgId=i18n.get("specialpage.recuperacredenziali.message.nopasswordsmatching");
					description="PASSWORD_NOT_MATCH_TO_CONFIRMPASSWORD";
					responseCode="PASSWORD_NOT_MATCH_TO_CONFIRMPASSWORD";

//					response.setContentType("application/json");
//					json = new TidyJSONWriter(response.getWriter());
//
//					try {
//						json.object();
//						json.key("currentPageUrl").value(currentPageUrl);
//						json.key("homePageUrl").value(homePagePageUrl);
//						json.key("isError").value("TRUE");
//                        json.key("msgId").value(i18n.get("specialpage.recuperacredenziali.message.nopasswordsmatching"));
//                        json.key("description").value("PASSWORD_NOT_MATCH_TO_CONFIRMPASSWORD");
//						json.key("responseCode").value("PASSWORD_NOT_MATCH_TO_CONFIRMPASSWORD");
//						json.endObject();
//					} catch (Exception e) {
//						logger.error("[MillemigliaRecuperaCredenzialiServlet] Password diversa da Conferma Password.", e);
//					}
					stato = 3;
				}
			}else if (retrieveResetChoice.equals("retrieveUsername")){
//                username = usernameForm;
                usernameForm = username;
				stato= 4;
			}


		}else{
			stato = 3;
		}



	}else if (stato == 4){

		resetSessionData(request);
		//stato = 5;


	}

	saveSessionData(request);

	/*String selector = (String) componentContext.getProperties()
			.get("sling.servlet.selectors");*/
	/*String urlServlet = request.getRequestURL().toString();
	String url = urlServlet.substring(0,
			urlServlet.length() - selectorString.length());
	url = url + "html";*/
	
	/*String url="http://localhost:11111/content/alitalia/alitalia-it/it/special-pages/recovery_mm_crd.html";
	response.sendRedirect(url);*/

}

	private void setDatiSicurezzaProfilo(){
	///////// dati sicurezza ////////////////

	com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloRequest datiSicurezzaProfiloRequest=
			new com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloRequest();

//??	ObjectFactory of=new ObjectFactory();
//??	GetDatiSicurezzaProfiloRequestLocal getDatiSicurezzaProfiloRequest = new GetDatiSicurezzaProfiloRequestLocal();

	//datiSicurezzaProfiloRequest.setAccessFailedCount(of.createGetDatiSicurezzaProfiloResponseAccessFailedCount("0"));
	datiSicurezzaProfiloRequest.setIdProfilo(idProfilo);
	datiSicurezzaProfiloRequest.setUserName(username);
	datiSicurezzaProfiloRequest.setFlagVerificaUserNameOK(flagVerificaUserName_OK);
	datiSicurezzaProfiloRequest.setPassword(password_MD5);
	datiSicurezzaProfiloRequest.setFlagVerificaRispostaOK(flagVerificaPassword_OK);
	datiSicurezzaProfiloRequest.setEmailAccount(certifiedEmailAccount);
	datiSicurezzaProfiloRequest.setFlagVerificaEmailAccountOK(flagVerificaEmailUser_OK);
	datiSicurezzaProfiloRequest.setPrefissoNazioneCellulare(certifiedCountryNumber);
	datiSicurezzaProfiloRequest.setCellulare(certifiedPhoneNumber);
	datiSicurezzaProfiloRequest.setFlagVerificaCellulareOK(flagVerificaCellulare_OK);
	datiSicurezzaProfiloRequest.setSecureQuestionID(secretQuestionID);
	datiSicurezzaProfiloRequest.setSecureQuestionCode(secretQuestionCode);
	datiSicurezzaProfiloRequest.setSecureAnswer(rispostaSegreta_MD5);
	datiSicurezzaProfiloRequest.setFlagVerificaRispostaOK(flagVerificaRisposta_OK);
	//datiSicurezzaProfiloRequest.setAccessFailedCount(???);
	//datiSicurezzaProfiloRequest.setLockedout(???);
	//datiSicurezzaProfiloRequest.setFlagRememberPassword(???);


	com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloResponse resp =
			consumerLoginDelegate.SetDatiSicurezzaProfilo(datiSicurezzaProfiloRequest);

//	if(resp.getFlagIDProfiloSicurezzaSaved()){
//		//salvataggio effettuato con sucesso
//	}

	/////////////////////////////////////////

}
	protected String GetDomandaSicurezza(SlingHttpServletRequest request, Integer inputSecretQuestionID){

		String localInputSecretQuestionText = "";
		String localInputSecretQuestionCode = "";

		RetrieveSecureQuestionListRequest secureQuestionRequest = new RetrieveSecureQuestionListRequest(IDFactory.getTid(),
				IDFactory.getSid(request));
		secureQuestionRequest.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()).toUpperCase());
		secureQuestionRequest.setMarket(locale.getCountry());

		RetrieveSecureQuestionListResponse secureQuestionResponse =
				staticDataDelegate.retrieveSecureQuestionList(secureQuestionRequest);

			List<SecureQuestionData> listOfSecureQuestion =
					secureQuestionResponse.getSecureQuestions();
			for (SecureQuestionData secureQuestion : listOfSecureQuestion ) {
				if (inputSecretQuestionID == secureQuestion.getSecureQuestionID()){
					localInputSecretQuestionCode =  "securequestion." + secureQuestion.getSecureQuestionCode();
					localInputSecretQuestionText =  i18n.get(localInputSecretQuestionCode);
				}
			}

		return localInputSecretQuestionText;
	}

	private boolean isEmpty(String value) {
		if (null == value || 0 == value.length()) {
			return true;
		}
		return false;
	}
	
	private void getSessionData(SlingHttpServletRequest request) {
		logger.debug("[MillemigliaRegistratiServlet] getSessionData");

        sessionName = millemigliaRecuperaCredenzialiData.NAME;

		millemigliaRecuperaCredenzialiData = (MillemigliaRecuperaCredenzialiData)
				request.getSession()
				.getAttribute(sessionName);

		if(millemigliaRecuperaCredenzialiData == null){
			millemigliaRecuperaCredenzialiData =
					new MillemigliaRecuperaCredenzialiData();
            logger.debug("TrackingCode NON trovato in sessione");

		}else{
            trackCode = "0";
            mmCode                  = millemigliaRecuperaCredenzialiData.getMmcode();
            alias                   = millemigliaRecuperaCredenzialiData.getAlias();
            surname                 = millemigliaRecuperaCredenzialiData.getSurname();
            isLockedOut             = millemigliaRecuperaCredenzialiData.getLockedOut();
            username                = millemigliaRecuperaCredenzialiData.getUsername();
            usernameForm            = millemigliaRecuperaCredenzialiData.getUsernameForm();
            flagVerificaUserName_OK = millemigliaRecuperaCredenzialiData.getFlagVerificaUserName_OK();
            password_MD5            = millemigliaRecuperaCredenzialiData.getPassword_MD5();
            flagVerificaPassword_OK = millemigliaRecuperaCredenzialiData.getFlagVerificaPassword_OK();
            certifiedEmailAccount   = millemigliaRecuperaCredenzialiData.getCertifiedEmailAccount();
            flagVerificaEmailUser_OK= millemigliaRecuperaCredenzialiData.getFlagVerificaEmailUser_OK();
            certifiedCountryNumber  = millemigliaRecuperaCredenzialiData.getCertifiedCountryNumber();
            certifiedPhoneNumber    = millemigliaRecuperaCredenzialiData.getCertifiedPhoneNumber();
            flagVerificaCellulare_OK= millemigliaRecuperaCredenzialiData.getFlagVerificaCellulare_OK();
            secretQuestionID        = millemigliaRecuperaCredenzialiData.getSecretQuestionID();
            rispostaSegreta_MD5     = millemigliaRecuperaCredenzialiData.getRispostaSegreta_MD5();
            flagVerificaRisposta_OK = millemigliaRecuperaCredenzialiData.getFlagVerificaRisposta_OK();
            secretQuestionText      = millemigliaRecuperaCredenzialiData.getSecretQuestionText();
            secretQuestionCode		= millemigliaRecuperaCredenzialiData.getSecretQuestionCode();

            sessionName 			= millemigliaRecuperaCredenzialiData.NAME;
            trackCode = millemigliaRecuperaCredenzialiData.getTrackingCod();
            logger.debug("TrackingCode trovato in sessione: " + trackCode);
            stato = millemigliaRecuperaCredenzialiData.getState();
        }
	}

	protected void saveSessionData(SlingHttpServletRequest request) {
		logger.debug("[MillemigliaRecuperaCredenzialiServlet] saveSessionData");
		millemigliaRecuperaCredenzialiData.setMmcode(mmCode);
		millemigliaRecuperaCredenzialiData.setAlias(alias);
		millemigliaRecuperaCredenzialiData.setSurname(surname);
		millemigliaRecuperaCredenzialiData.setLockedOut(isLockedOut);
		millemigliaRecuperaCredenzialiData.setUsername(username);
		millemigliaRecuperaCredenzialiData.setUsernameForm(usernameForm);
		millemigliaRecuperaCredenzialiData.setFlagVerificaUserName_OK(flagVerificaUserName_OK);
		millemigliaRecuperaCredenzialiData.setPassword_MD5(password_MD5);
		millemigliaRecuperaCredenzialiData.setFlagVerificaPassword_OK(flagVerificaPassword_OK);
		millemigliaRecuperaCredenzialiData.setCertifiedEmailAccount(certifiedEmailAccount);
		millemigliaRecuperaCredenzialiData.setFlagVerificaEmailUser_OK(flagVerificaEmailUser_OK);
		millemigliaRecuperaCredenzialiData.setCertifiedCountryNumber(certifiedCountryNumber);
		millemigliaRecuperaCredenzialiData.setCertifiedPhoneNumber(certifiedPhoneNumber);
		millemigliaRecuperaCredenzialiData.setFlagVerificaCellulare_OK(flagVerificaCellulare_OK);
		millemigliaRecuperaCredenzialiData.setSecretQuestionID(secretQuestionID);
		millemigliaRecuperaCredenzialiData.setRispostaSegreta_MD5(rispostaSegreta_MD5);
		millemigliaRecuperaCredenzialiData.setFlagVerificaRisposta_OK(flagVerificaRisposta_OK);
		millemigliaRecuperaCredenzialiData.setSecretQuestionText(secretQuestionText);
		millemigliaRecuperaCredenzialiData.setSecretQuestionCode(secretQuestionCode);
		millemigliaRecuperaCredenzialiData.setState(stato);
		millemigliaRecuperaCredenzialiData.setRetrieveResetChoice(retrieveResetChoice);
		millemigliaRecuperaCredenzialiData.setTrackingCod(trackCode);

		locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);

		millemigliaRecuperaCredenzialiData.setError(i18n.get(
				I18nKeySpecialPage.ERROR_MESSAGE_MILLEMIGLIA_RECUPERACREDENZIALI));

		request.getSession().setAttribute(sessionName,
				millemigliaRecuperaCredenzialiData);

	}


	protected void resetSessionData(SlingHttpServletRequest request) {
		logger.debug("[MillemigliaRecuperaCredenzialiServlet] resetSessionData");

		millemigliaRecuperaCredenzialiData =
				new MillemigliaRecuperaCredenzialiData();


		request.getSession().setAttribute(sessionName,
				millemigliaRecuperaCredenzialiData);

		getSessionData(request);

	}

	@Override
    protected void saveDataIntoSession(SlingHttpServletRequest request,
                                       Exception e) {
        logger.debug("[MillemigliaRecuperaCredenzialiServlet] saveDataIntoSession");

        saveSessionData(request);

        locale = AlitaliaUtils.findResourceLocale(request.getResource());
        ResourceBundle resourceBundle = request.getResourceBundle(locale);
        final I18n i18n = new I18n(resourceBundle);

        millemigliaRecuperaCredenzialiData.setError(i18n.get(
                I18nKeySpecialPage.ERROR_MESSAGE_MILLEMIGLIA_RECUPERACREDENZIALI));

        request.getSession().setAttribute(sessionName,
                millemigliaRecuperaCredenzialiData);
    }
}