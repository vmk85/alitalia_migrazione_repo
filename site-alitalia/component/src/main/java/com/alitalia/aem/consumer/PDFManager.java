package com.alitalia.aem.consumer;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;

import javax.imageio.ImageIO;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Session;

import com.alitalia.aem.common.data.checkinrest.model.boardingpass.BoardingPassInfo;
import com.alitalia.aem.common.data.checkinrest.model.boardingpass.Model;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.MmbAncillaryDetail;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.PassengerApisSecureFlightInfoData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PassengerSecureFlightInfoData;
import com.alitalia.aem.common.data.home.SeatPreferencesData;
import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.common.data.home.carnet.CarnetCreditCardProviderData;
import com.alitalia.aem.common.data.home.carnet.CarnetInfoCarnet;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryEticketData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryUpgradeDetailData;
import com.alitalia.aem.common.data.home.checkin.CheckinCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.ActivityTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.MmbTaxData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryMealDetailData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPassengerData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillarySeatDetailData;
import com.alitalia.aem.common.messages.home.StatementByDateRequest;
import com.alitalia.aem.common.messages.home.StatementByDateResponse;
import com.alitalia.aem.common.utils.FlightDataUtils;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingPhaseEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.BookingPDFPriceFareInfo;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.model.PDFSliceInfo;
import com.alitalia.aem.consumer.booking.render.AvailableFlightsSelectionRender;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.booking.render.GenericPriceRender;
import com.alitalia.aem.consumer.booking.render.RegoleTariffarieTrattaRender;
import com.alitalia.aem.consumer.carnet.CarnetConstants;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.alitalia.aem.consumer.carnet.CarnetUtils;
import com.alitalia.aem.consumer.carnet.render.CarnetGenericPriceRender;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.millemiglia.model.messaggimiglia.MessaggiMillemigliaData;
import com.alitalia.aem.consumer.millemiglia.render.EstrattoContoRender;
import com.alitalia.aem.consumer.mmb.MmbConstants;
import com.alitalia.aem.consumer.mmb.MmbSession;
import com.alitalia.aem.consumer.mmb.MmbSessionContext;
import com.alitalia.aem.consumer.mmb.MmbUtils;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup;
import com.alitalia.aem.consumer.mmb.model.MmbAncillariesGroup.AncillariesGroupType;
import com.alitalia.aem.consumer.mmb.model.MmbFlightsGroup;
import com.alitalia.aem.consumer.mmb.render.MmbFlightDataRender;
import com.alitalia.aem.consumer.mmb.render.MmbPassengerDataRender;
import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;
import com.alitalia.aem.consumer.mmb.render.MmbTaxDataListRender;
import com.alitalia.aem.consumer.mmb.render.MmbTaxDataRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.Lists;
import com.alitalia.aem.consumer.utils.PDFCreator;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.i18n.I18n;
import com.lowagie.text.pdf.BarcodePDF417;


@Component
@Service(value = PDFManager.class)
public class PDFManager {

	public static final String UNICODE_LANGUAGES = "unicode.languages";
	public static final String RIGHT_TO_LEFT_LANGUAGES = "rtl.languages";
	public static final String MINIFARE_SOURCE_BRAND_CODE_PROPERTY = "minifare.source.brand.code";
	public static final String MINIFARE_TARGET_BRAND_CODE_PROPERTY = "minifare.target.brand.code";

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private ConsumerLoginDelegate consumerDataDelegate;

	@Reference
	private AlitaliaCustomerProfileManager customerProfileManager;

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private BookingSession bookingSession;

	@Reference
	private CheckinSession checkinSession;

	@Reference
	private MmbSession mmbSession;

	private ComponentContext componentContext;

/* mini-fare configuration (read from component properties) */

	private String brandCodeToMapOnMiniFare = "";
	private String miniFareCode = "";

	/* SCR lifecycle methods */

	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
		readMiniFareConfiguration(componentContext);
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		this.componentContext = componentContext;
		readMiniFareConfiguration(componentContext);
	}

	/**
	 * Manage Messages PDF
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void manageMessagesPDF(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {

		InputStream headerLogo = null;
		InputStream fontFamily = null;
		InputStream fontFamilyBold = null;

		try {

			final I18n i18n = new I18n(request.getResourceBundle(
					AlitaliaUtils.findResourceLocale(request.getResource())));

			// retrieve MMCustomerUser profile data
			MMCustomerProfileData user =
				 AlitaliaUtils.getAuthenticatedUser(request);
			if (null != user) {

				// decode and decrypt user
				user.setCustomerPinCode(
						customerProfileManager.decodeAndDecryptProperty(
								user.getCustomerPinCode()));

				// logo and fonts
				String image = null;
				switch (user.getTierCode()) {
					default:
					case UNKNOWN :
					case BASIC :
						image = "tier_1_millemiglia.png";
						break;

					case FRECCIA_ALATA :
						image = "tier_3_frecciaalata.png";
						break;

					case PLUS :
						image = "tier_4_frecciaalataplus.png";
						break;

					case ULISSE :
						image = "tier_2_ulisse.png";
						break;
				}
				if (null != image) {
					headerLogo = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/images/MMTier/" + image);
				}

				Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
				boolean rtl = locale != null &&
						createSetFromProperty(RIGHT_TO_LEFT_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()));

				if (locale != null &&
						createSetFromProperty(UNICODE_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()))) {
					fontFamily = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialu.ttf");
					fontFamilyBold = retrieveContentFromCRXRepository(
							"/etc/designs/alitalia/clientlibs/fonts/arialubd.ttf");
				} else {
					fontFamily = retrieveContentFromCRXRepository(
							"/etc/designs/alitalia/clientlibs/fonts/Trebuchet.ttf");
					fontFamilyBold = retrieveContentFromCRXRepository(
							"/etc/designs/alitalia/clientlibs/fonts/Trebuchet_Bold.ttf");
				}

				// labels
				String userMigliaLabel = i18n.get("millemiglia.saldoMiglia.label");
				String userStatusLabel = i18n.get("millemiglia.scandenzaStatus.label");
				String headerTitle = i18n.get("messaggimiglia.messaggi.label").toUpperCase();
				String messageExpireDate = i18n.get("millemiglia.dataDiScadenza.label").toUpperCase();
				String messageSubject = i18n.get("messaggimiglia.oggetto.label").toUpperCase();
				String messageDescription = i18n.get("millemiglia.descrizione.label").toUpperCase();

				// user data
				String userName = user.getCustomerName() + " " + user.getCustomerSurname();
				String userMiglia = "" + user.getPointsRemainingTotal();
				SimpleDateFormat sdf = new SimpleDateFormat(AlitaliaConstants.REQUEST_DATE_FORMAT);
				String userStatus = sdf.format(user.getTierDate().getTime());

				// messages data
				List<MessaggiMillemigliaData> listMessages =
						AlitaliaUtils.getListaMessaggi(user, consumerDataDelegate);
				String[][] contents = new String[0][];
				if (null != listMessages) {
					contents = new String[listMessages.size()][];
					for (int i = 0; i < listMessages.size(); i++) {
						String[] string = {
							messageExpireDate, messageSubject, messageDescription,
							listMessages.get(i).getEndValidatDate(),
							listMessages.get(i).getDescription()
						};
						contents[i] = string;
					}
				}

				// create PDF! (and send to output)
				response.setContentType("application/pdf");
				PDFCreator.generateMessagesPDF(
						(OutputStream) response.getOutputStream(), rtl, fontFamily,
						fontFamilyBold, headerLogo, headerTitle, userName,
						userMigliaLabel, userMiglia, userStatusLabel, userStatus,
						contents);

			} else {
				throw new Exception("[PDFCreatorServlet] - User doesn't exist!");
			}

		} catch(Exception e) {
			throw e;
		} finally {
			if (null != headerLogo) {
				headerLogo.close();
			}
			if (null != fontFamily) {
				fontFamily.close();
			}
			if (null != fontFamilyBold) {
				fontFamilyBold.close();
			}
		}

	}

	/**
	 * Manage Balance PDF
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void manageBalancePDF(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {

		InputStream headerLogo = null;
		InputStream fontFamily = null;
		InputStream fontFamilyBold = null;

		try {

			final I18n i18n = new I18n(request.getResourceBundle(
					AlitaliaUtils.findResourceLocale(request.getResource())));

			// retrieve MMCustomerUser profile data
			MMCustomerProfileData user =
					AlitaliaUtils.getAuthenticatedUser(request);
			if (null != user) {

				// decode and decrypt user
				user.setCustomerPinCode(
						customerProfileManager.decodeAndDecryptProperty(
								user.getCustomerPinCode()));

				// logo and fonts
				String image = null;
				switch (user.getTierCode()) {
					default:
					case UNKNOWN :
					case BASIC :
						image = "tier_1_millemiglia.png";
						break;

					case FRECCIA_ALATA :
						image = "tier_3_frecciaalata.png";
						break;

					case PLUS :
						image = "tier_4_frecciaalataplus.png";
						break;

					case ULISSE :
						image = "tier_2_ulisse.png";
						break;
				}

				if (null != image) {
					headerLogo = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/images/MMTier/" + image);
				}

				Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
				boolean rtl = locale != null &&
						createSetFromProperty(RIGHT_TO_LEFT_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()));

				if (locale != null &&
						createSetFromProperty(UNICODE_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()))) {
					fontFamily = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialu.ttf");
					fontFamilyBold = retrieveContentFromCRXRepository(
							"/etc/designs/alitalia/clientlibs/fonts/arialubd.ttf");
				} else {
					fontFamily = retrieveContentFromCRXRepository(
							"/etc/designs/alitalia/clientlibs/fonts/Trebuchet.ttf");
					fontFamilyBold = retrieveContentFromCRXRepository(
							"/etc/designs/alitalia/clientlibs/fonts/Trebuchet_Bold.ttf");
				}

				// labels
				String userMigliaLabel = i18n.get("millemiglia.saldoMiglia.label");
				String userStatusLabel = i18n.get("millemiglia.scandenzaStatus.label");
				String headerTitle = i18n.get("millemiglia.estrattoConto.label").toUpperCase();
				String activity = i18n.get("millemiglia.attivita.label").toUpperCase();
				String className = i18n.get("millemiglia.classe.label").toUpperCase();
				String description = i18n.get("millemiglia.descrizione.label").toUpperCase();
				String miles = i18n.get("millemiglia.miglia.label").toUpperCase();
				String miles_gained = i18n.get("millemiglia.migliaAccumulate.label").toUpperCase();
				String miles_spent = i18n.get("millemiglia.migliaSpese.label").toUpperCase();
				String miles_qualified = i18n.get("millemiglia.migliaQualificanti.label").toUpperCase();
				String flights_qualified = i18n.get("millemiglia.voliQualificati.label").toUpperCase();

				// user data
				String userName = user.getCustomerName() + " " + user.getCustomerSurname();
				String userMiglia = "" + user.getPointsRemainingTotal();
				SimpleDateFormat sdf = new SimpleDateFormat(AlitaliaConstants.REQUEST_DATE_FORMAT);
				String userStatus = sdf.format(user.getTierDate().getTime());

				// set summary
				Long spent_total = user.getPointsEarnedTotal()
						- user.getPointsRemainingTotal();

				String[][] summary = {
					{ "" + user.getPointsEarnedTotal(), miles_gained },
					{ "" + spent_total, miles_spent },
					{ "" + user.getPointsEarnedTotalQualified(), miles_qualified },
					{ "" + user.getQualifyingSegment(), flights_qualified }
				};

				// set contents
				StatementByDateRequest statementRequest =
						new StatementByDateRequest();
				statementRequest.setActivityType(ActivityTypeEnum.NOT_SPECIFIED);
				statementRequest.setCustomerProfile(user);
				StatementByDateResponse statementResponse =
						consumerDataDelegate.getStatementByDate(
								statementRequest);
				List<EstrattoContoRender> listContents =
						AlitaliaUtils.convertActivities(statementResponse
								.getCustomerProfile().getActivities());

				String[][] contents = new String[0][];
				if (null != listContents) {
					contents = new String[listContents.size()][];
					for (int i = 0; i < listContents.size(); i++) {
						String[] content = {
							activity, className, description, miles,
							listContents.get(i).getActivityDate(),
							listContents.get(i).getDescription(),
							listContents.get(i).getActivityClass(),
							listContents.get(i).getActivity().getReferenceNumber(),
							listContents.get(i).getActivity().getQualified()
								? "Q " : ""
								+ listContents.get(i).getTotalPointsSign()
								+ listContents.get(i).getTotalPointsAbsoluteValue()
						};
						contents[i] = content;
					}
				}

				response.setContentType("application/pdf");
				PDFCreator.generateBalancePDF(
						(OutputStream) response.getOutputStream(), rtl, fontFamily,
						fontFamilyBold, headerLogo, headerTitle, userName,
						userMigliaLabel, userMiglia, userStatusLabel, userStatus,
						summary, contents);

			} else {
				throw new Exception("[PDFCreatorServlet] - User doesn't exist!");
			}

		} catch(Exception e) {
			throw e;
		} finally {
			if (null != headerLogo) {
				headerLogo.close();
			}
			if (null != fontFamily) {
				fontFamily.close();
			}
			if (null != fontFamilyBold) {
				fontFamilyBold.close();
			}
		}

	}

	/**
	 * Get Boarding Pass PDF
	 * @param request
	 * @param response
	 * @return
	 */
	public byte[] getBoardingPassPDF(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {

		ByteArrayOutputStream out = (ByteArrayOutputStream)
				manageCheckinPDF(request, response, false);
		return out.toByteArray();

	}

	/**
	 * Manage Check-in PDF
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public OutputStream manageCheckinPDF(SlingHttpServletRequest request,
			SlingHttpServletResponse response, boolean output)
					throws Exception {

		List<InputStream> logos = null;
		InputStream fontFamily = null;
		InputStream fontFamilyBold = null;
		InputStream cutHere = null;
		InputStream time = null;
		InputStream bag = null;
		InputStream banner1 = null;
		InputStream banner2 = null;
		InputStream banner3 = null;
		List<InputStream> boardingPass = new ArrayList<InputStream>();
		List<InputStream> banners = new ArrayList<InputStream>();

		OutputStream out = null;

		// choose type of output stream
		if (output) {
			out = response.getOutputStream();
		} else {
			out = new ByteArrayOutputStream();
		}

		try {

			// translator
			final I18n i18n = new I18n(request.getResourceBundle(
					AlitaliaUtils.findResourceLocale(request.getResource())));

			// check-in session context
			CheckinSessionContext ctx = (CheckinSessionContext)
					request.getSession(true).getAttribute(
							CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE);

			if (ctx == null) {
				logger.error("Cannot find check-in session context in session.");
				throw new RuntimeException(
						"Cannot find check-in session context in session.");
			}


			// get logos
			logos = retrieveLogos(request.getResource());

			Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
			boolean rtl = locale != null &&
					createSetFromProperty(RIGHT_TO_LEFT_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()));

			if (locale != null &&
					createSetFromProperty(UNICODE_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()))) {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/arialu.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialubd.ttf");
			} else {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/arial.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/arialbd.ttf");
			}

			cutHere = retrieveContentFromCRXRepository(
				rtl ? "/etc/designs/alitalia/clientlibs/images/checkin_cutHere_rtl.jpg" :
				"/etc/designs/alitalia/clientlibs/images/checkin_cutHere.jpg");
			time = retrieveContentFromCRXRepository(
				"/etc/designs/alitalia/clientlibs/images/checkin_time.jpg");
			bag = retrieveContentFromCRXRepository(
				"/etc/designs/alitalia/clientlibs/images/checkin_bag.jpg");

			// notice
			String[] notice = { "" };
			try {
				String notice_path = AlitaliaUtils.findSiteBaseExternalUrl(
						request.getResource(), false)
						+ "/alitalia-config/template-pdf"
						+ "/boarding-pass/jcr:content/checkin-pdf-note";
				ArrayList<String> notice_list = new ArrayList<String>();
				for (String string : cleanHtml(
						retrieveTextFromCRXRepository(notice_path))) {
					for (String s : getParts(string, 170)) {
						notice_list.add(s);
					}
				}
				notice = notice_list.toArray(new String[0]);
			} catch (Exception e) { }

			// banners
			if (null != (banner1 = retrieveImageFromCRXRepository(
					AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), false)
					+ "/alitalia-config/template-pdf"
					+ "/boarding-pass/jcr:content/checkin-pdf-banner-1"))) {
				banners.add(banner1);
			}
			if (null != (banner2 = retrieveImageFromCRXRepository(
					AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), false)
					+ "/alitalia-config/template-pdf"
					+ "/boarding-pass/jcr:content/checkin-pdf-banner-2"))) {
				banners.add(banner2);
			}
			if (null != (banner3 = retrieveImageFromCRXRepository(
					AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), false)
					+ "/alitalia-config/template-pdf"
					+ "/boarding-pass/jcr:content/checkin-pdf-banner-3"))) {
				banners.add(banner3);
			}

			// boarding luggage alert
			String[] boardingluggage = ArrayUtils.addAll(
					new String[] { i18n.get("checkin.pdf.boardingluggage") },
					getParts(i18n.get("checkin.pdf.boardingluggage.text"), 50));

			// all passengers
			List<String[][][]> contents = new ArrayList<String[][][]>();
			for (CheckinPassengerData passengerData : ctx.passengers) {

				// frequent flyer
				String fflyer = null != passengerData.getFrequentFlyerCode()
						? passengerData.getFrequentFlyerCode() : "";

				List<CheckinCouponData> couponData = passengerData.getCoupons();
				for (CheckinCouponData checkinCouponData : couponData) {

					if (checkinCouponData.getBarcode() != null) {

					// passenger name
					String name = passengerData.getName() + " ";
					if (null != passengerData.getApisData() &&
							null != passengerData.getApisData().getSecondName()) {
						name += passengerData.getApisData().getSecondName();
					}
					name += passengerData.getLastName();

					if (checkinCouponData.getInfantCoupon()) {
						name += " " + i18n.get("checkin.pdf.infant");
					}

					// ticket number
					String ticket = null != passengerData.getEticket() &&
							!Boolean.TRUE.equals(checkinCouponData.getInfantCoupon())
							? passengerData.getEticket() : "";

					// baggage
					String baggage = null != passengerData.getBaggageAllowance() &&
							!Boolean.TRUE.equals(checkinCouponData.getInfantCoupon())
							? passengerData.getBaggageAllowance() : "";

					// sequence number
					String sequencenumber =
							null != checkinCouponData.getSequenceNumber()
							? checkinCouponData.getSequenceNumber() : "";

					// terminal
					String terminal = null != checkinCouponData.getTerminal()
							? checkinCouponData.getTerminal() : "";

					// gate
					String gate = null != checkinCouponData.getGate()
							? checkinCouponData.getGate() : "";

					// seat
					String seat = Boolean.TRUE.equals(checkinCouponData.getInfantCoupon())
							? i18n.get("checkin.pdf.seat.infant") :
								checkinCouponData.getSeat();

					// flight number
					CheckinFlightData flightData =
							checkinCouponData.getFlight();
					String flightnumber = flightData.getCarrier()
							+ flightData.getFlightNumber();

					// class
					String compartimentalclass =
							flightData.getSeatClass() + " "
								+ checkinCouponData.getSeatClassName();

					// departure airport
					String departureairport = i18n.get("airportsData."
							+ flightData.getFrom().getCode() + ".city") + ", "
							+ i18n.get("airportsData."
							+ flightData.getFrom().getCode() + ".name");

					// arrival airport
					String arrivalairport = i18n.get("airportsData."
							+ flightData.getTo().getCode() + ".city") + ", "
							+ i18n.get("airportsData."
							+ flightData.getTo().getCode() + ".name");

					// departure date
					Calendar departuredate = flightData.getDepartureDateTime();
					DateRender dateRender = new DateRender(departuredate);
					String ddate = dateRender.getDay()
							+ i18n.get(dateRender.getShortTextMonth());

					// departure time
					String dtime = dateRender.getHour();

					// boarding time
					Calendar boardingtimedate =
							checkinCouponData.getBoardingTime();
					dateRender = new DateRender(boardingtimedate);
					String boardingtime = dateRender.getHour();

					// boarding time alert
					String[] boarding = ArrayUtils.addAll(
						new String[] { i18n.get("checkin.pdf.boardingtime")
								+ " " + boardingtime},
						getParts(i18n.get("checkin.pdf.boardingtime.text"), 50));

					// barcode
					byte[] buffer = checkinCouponData.getBarcode();
					boardingPass.add(new ByteArrayInputStream(buffer));

					// sky priority
					String skyPriority = Boolean.TRUE.equals(checkinCouponData.getSkyPriority())
							? i18n.get("checkin.pdf.skyPriority") : "";

					// passengers details data
					List<String[]> passengersData = new ArrayList<String[]>();
					passengersData.add(new String[]{
							i18n.get("checkin.pdf.title"),
							i18n.get("checkin.pdf.subtitle"),
							i18n.get("checkin.pdf.subtitle2"),
							skyPriority});
					passengersData.add(new String[]{
							i18n.get("checkin.pdf.securenumber"),
							flightnumber, sequencenumber });
					passengersData.add(new String[]
						{ i18n.get("checkin.pdf.name"), name });
					if (!"".equals(fflyer)) {
						passengersData.add(new String[]
							{ i18n.get("checkin.pdf.frequentflyer"), fflyer });
					}
					passengersData.add(new String[]
							{ i18n.get("checkin.pdf.ticketnumber"), ticket });
					passengersData.add(new String[]
							{ i18n.get("checkin.pdf.baggage"), baggage });

					String[][] data = new String[passengersData.size()][];
					for (int i = 0; i < passengersData.size(); i ++) {
						data[i] = passengersData.get(i);
					}

					String[] secondLine = null;

					if (flightData.isBus()) {
						String departureStation = "";
						String arrivalStation = "";
						if (flightData.isFromBusStation()) {
							departureStation = i18n.get("checkin.bus.operatoDa.label");
						}
						if (flightData.isToBusStation()) {
							arrivalStation = i18n.get("checkin.bus.operatoDa.label");
						}
						String[] busLine = {
								"", "", "", departureStation,
								arrivalStation, "", "", "",
								"", ""
							};
						secondLine = busLine;

					} else if (checkinCouponData.getServiceOperatingDetails() != null &&
							!checkinCouponData.getServiceOperatingDetails().isEmpty()) {
						String[] otherLine = {
								i18n.get("checkin.pdf.operatoDa.label") + " " +
										checkinCouponData.getServiceOperatingDetails(), "", "", "",
										"", "", "", "", "", ""
						};
						secondLine = otherLine;
					}


					// data to print
					String[][][] content = {
						data,
						{
							{
								i18n.get("checkin.pdf.flight"),
								i18n.get("checkin.pdf.terminal"),
								i18n.get("checkin.pdf.date"),
								i18n.get("checkin.pdf.from"),
								i18n.get("checkin.pdf.to"),
								i18n.get("checkin.pdf.departure"),
								i18n.get("checkin.pdf.gate"),
								i18n.get("checkin.pdf.boarding"),
								i18n.get("checkin.pdf.class"),
								i18n.get("checkin.pdf.seat")
							},
							{
								i18n.get("checkin.pdf.flight.generic"),
								i18n.get("checkin.pdf.terminal.generic"),
								i18n.get("checkin.pdf.date.generic"),
								i18n.get("checkin.pdf.from.generic"),
								i18n.get("checkin.pdf.to.generic"),
								i18n.get("checkin.pdf.departure.generic"),
								i18n.get("checkin.pdf.gate.generic"),
								i18n.get("checkin.pdf.boarding.generic"),
								i18n.get("checkin.pdf.class.generic"),
								i18n.get("checkin.pdf.seat.generic")
							},
							{
								flightnumber, terminal, ddate, departureairport,
								arrivalairport, dtime, gate, boardingtime,
								compartimentalclass, seat
						},
							secondLine,
						},
						{ boardingluggage, boarding },
						{
							{ i18n.get("checkin.pdf.cuthere") },
							{ i18n.get("checkin.pdf.notice") },
							notice
						}
					};

					contents.add(content);
				}
			}
			}

			// send response => PDF
			if (output) {
				response.setContentType("application/pdf");
			}

			// generate PDF
			PDFCreator.generateCheckinPDF(
					out, rtl, fontFamily, fontFamilyBold, logos.get(0),
					logos.get(1), logos.get(2), contents, cutHere, time, bag,
					boardingPass, banners);

		} catch(Exception e) {
			throw e;
		} finally {
			for (InputStream logo : logos) {
				if (null != logo) {
					logo.close();
				}
			}
			if (null != fontFamily) {
				fontFamily.close();
			}
			if (null != fontFamilyBold) {
				fontFamilyBold.close();
			}
			if (null != cutHere) {
				cutHere.close();
			}
			if (null != time) {
				time.close();
			}
			if (null != bag) {
				bag.close();
			}
			for (InputStream inputStream : boardingPass) {
				if (null != inputStream) {
					inputStream.close();
				}
			}
			for (InputStream inputStream : banners) {
				if (null != inputStream) {
					inputStream.close();
				}
			}
			if (null != banner1) {
				banner1.close();
			}
			if (null != banner2) {
				banner2.close();
			}
			if (null != banner3) {
				banner3.close();
			}
		}

		// return stream
		return out;

	}

	/**
	 * Get Boarding Pass PDF
	 * @param request
	 * @param response
	 * @return
	 */
	public byte[] getCheckinSummaryPDF(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {

		// convert to ByteArrayOutputStream
		ByteArrayOutputStream out = (ByteArrayOutputStream)
				manageCheckinSummaryPDF(request, response, false);
		return out.toByteArray();

	}

	/**
	 * Manage Check-in Summary PDF
	 * @param request
	 * @param response
	 * @param output
	 * @return
	 * @throws Exception
	 */
	public OutputStream manageCheckinSummaryPDF(SlingHttpServletRequest request,
			SlingHttpServletResponse response, boolean output)
					throws Exception {

		List<InputStream> logos = null;
		InputStream fontFamily = null;
		InputStream fontFamilyBold = null;

		OutputStream out = null;

		// choose type of output stream
		if (output) {
			out = response.getOutputStream();
		} else {
			out = new ByteArrayOutputStream();
		}

		try {

			final I18n i18n = new I18n(request.getResourceBundle(
					AlitaliaUtils.findResourceLocale(request.getResource())));

			// check-in session context
			CheckinSessionContext ctx = (CheckinSessionContext)
					request.getSession(true).getAttribute(
							CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE);

			if (ctx == null) {
				logger.error("Cannot find check-in session context in session.");
				throw new RuntimeException(
						"Cannot find check-in session context in session.");
			}

			// get logos
			logos = retrieveLogos(request.getResource());

			Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
			boolean rtl = locale != null &&
					createSetFromProperty(RIGHT_TO_LEFT_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()));

			if (locale != null &&
					createSetFromProperty(UNICODE_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()))) {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/arialu.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialubd.ttf");
			} else {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/Trebuchet.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/Trebuchet_Bold.ttf");
			}


			// send response => PDF
			if (output) {
				response.setContentType("application/pdf");
			}

			String name = ctx.passengerName + " " + ctx.passengerSurname;
			String[] heading = ArrayUtils.addAll(
					new String[] { i18n.get("checkin.pdf.summary.title"),
							i18n.get("checkin.pdf.summary.subtitle")},
					getParts(i18n.get("checkin.pdf.summary.message")
							.replace("{0}", name), 110));

			int size = 135;
			String[] notice = ArrayUtils.addAll(
					new String[] { i18n.get("checkin.pdf.summary.notice") },
					getParts(i18n.get("checkin.pdf.summary.notice.message"),
							size));
			notice = ArrayUtils.addAll(notice,
					getParts(i18n.get("checkin.pdf.summary.notice1.message"),
							size));
			notice = ArrayUtils.addAll(notice,
					getParts(i18n.get("checkin.pdf.summary.notice2.message"),
							size));
			notice = ArrayUtils.addAll(notice,
					getParts(i18n.get("checkin.pdf.summary.notice3.message"),
							size));
			notice = ArrayUtils.addAll(notice,
					getParts(i18n.get("checkin.pdf.summary.notice4.message"),
							size));

			List<String[]> headers = new ArrayList<String[]>();
			List<String[]> passengers = new ArrayList<String[]>();
			List<CheckinPassengerData> acceptedPassengers = CheckinUtils.getCheckedInPassengers(ctx.passengers);
			int i = 0;
			checkinSession.inizializeNewSeats(ctx);
			for (CheckinPassengerData passengerData : acceptedPassengers) {

				// passenger data
				List<CheckinCouponData> coupons = passengerData.getCoupons();
				for (CheckinCouponData couponData : coupons) {
					String seat = getPassengerSeat(ctx, couponData);
					String[] passenger = {
						(null != passengerData.getName()
								&& null != passengerData.getLastName())
							? passengerData.getName() + " "
								+ passengerData.getLastName() : "",
						(null != passengerData.getFrequentFlyerCode())
							? passengerData.getFrequentFlyerCode() : "",
						(null != passengerData.getEticket())
							? passengerData.getEticket() : "",
						(null != passengerData.getPnr())
							? passengerData.getPnr() : "",
						(null != seat)
							? seat : ""
					};
					passengers.add(passenger);
					checkinSession.addNewSeat(ctx, seat);
				}

				// header
				if (0 == i) {
					for (CheckinCouponData checkinCouponData : coupons) {

						// flight number
						CheckinFlightData flightData =
								checkinCouponData.getFlight();

						if (null != flightData) {
							// departure date
							Calendar departuredate =
									flightData.getDepartureDateTime();
							DateRender depDate = new DateRender(departuredate);

							// arrival date
							Calendar arrivaldate =
									flightData.getArrivalDateTime();
							DateRender arrDate = new DateRender(arrivaldate);

							if (null != depDate && null != arrDate) {
								String[] header =  {
									depDate.getDay(),
									i18n.get(depDate.getShortTextMonth()),
									depDate.getYear(),
									flightData.getCarrier()
										+ flightData.getFlightNumber(),
									depDate.getHour(),
									i18n.get("airportsData."
											+ flightData.getFrom().getCode()
											+ ".city"),
									i18n.get("airportsData."
											+ flightData.getFrom().getCode()
											+ ".name") + "("
											+ flightData.getFrom().getCode()
											+ ")",
									arrDate.getHour(),
									i18n.get("airportsData."
											+ flightData.getTo().getCode()
											+ ".city"),
									i18n.get("airportsData."
											+ flightData.getTo().getCode()
											+ ".name") + "("
											+ flightData.getTo().getCode()
											+ ")",
									null != checkinCouponData.getTerminal()
										? checkinCouponData.getTerminal() : ""
								};
								headers.add(header);
							}
						}

					}
				}
				++i;

			}

			String[][][] fligths = null;
			int numCoupon = headers.size();
			if (null != passengers && 0 < passengers.size() &&
					null != headers && 0 < headers.size()) {
				int psize = passengers.size() / headers.size();
				fligths = new String[headers.size()][1 + psize][];
				for (int l = 0; l < headers.size(); l++) {
					String[][] passengersArray = new String[psize][];
					passengersArray[0] = passengers.get(l);
					for (int k = numCoupon + l, j = 1; j < psize && k < passengers.size(); k+=numCoupon, j++) {
						passengersArray[j] = passengers.get(k);
					}
					fligths[l] = ArrayUtils.addAll(new String[][] {
						headers.get(l) }, passengersArray);
				}
			}

			// data to print
			String[][][] contents = {
				{
					heading,
					{
						i18n.get("checkin.pdf.summary.passenger"),
						i18n.get("checkin.pdf.summary.frequentflyer"),
						i18n.get("checkin.pdf.summary.ticketnumber"),
						i18n.get("checkin.pdf.summary.pnr"),
						i18n.get("checkin.pdf.summary.seat")
					},
					{
						i18n.get("checkin.pdf.summary.date"),
						i18n.get("checkin.pdf.summary.flight"),
						i18n.get("checkin.pdf.summary.outbound"),
						i18n.get("checkin.pdf.summary.inbound"),
						i18n.get("checkin.pdf.summary.terminal")
					},
					notice
				},
			};
			contents = ArrayUtils.addAll(contents, fligths);

			// generate PDF
			PDFCreator.generateCheckinSummaryPDF(
					out, rtl, fontFamily, fontFamilyBold, logos.get(0),
					logos.get(1), logos.get(2), contents);

		} catch (Exception e) {
			throw e;
		} finally {
			for (InputStream logo : logos) {
				if (null != logo) {
					logo.close();
				}
			}
			if (null != fontFamily) {
				fontFamily.close();
			}
			if (null != fontFamilyBold) {
				fontFamilyBold.close();
			}
		}

		// return stream
		return out;

	}

	/**
	 * Obtain the real seat of a passenger in a particular flight.
	 * @param ctx
	 * @param coupon
	 * @return
	 */
	private String getPassengerSeat(CheckinSessionContext ctx, CheckinCouponData coupon){
		String seat = null;
		try{
			if(ctx.cart != null && ctx.cart.getAncillaries() != null
					&& ctx.cart.getAncillaries().size() > 0){
				String eTicketPlusCoupon = coupon.getEticket() + "C" + coupon.getId();
				MmbAncillaryStatusEnum issued = MmbAncillaryStatusEnum.ISSUED;
				MmbAncillaryTypeEnum[] types = {MmbAncillaryTypeEnum.UPGRADE, MmbAncillaryTypeEnum.SEAT};
				/*Search in this order: (UPGRADE,ISSUED), (SEAT,ISSUED)*/
				for(int j = 0; j< types.length && seat == null; j++){
					for(int i = 0; i < ctx.cart.getAncillaries().size() && seat == null; i++){
						MmbAncillaryData anc = ctx.cart.getAncillaries().get(i);
						if(issued.equals(anc.getAncillaryStatus())
								&& types[j].equals(anc.getAncillaryType())
								&& eTicketPlusCoupon.equals(anc.getCouponNumbers().get(0))){
							MmbAncillaryDetail details = anc.getAncillaryDetail();
							if(details != null && details instanceof CheckinAncillaryUpgradeDetailData){
								seat = ((CheckinAncillaryUpgradeDetailData) details).getSeat();
							}
							else if(details != null && details instanceof MmbAncillarySeatDetailData){
								seat = ((MmbAncillarySeatDetailData) details).getSeat();
							}
						}
					}
				}
			}
		}
		catch(Exception e){
			logger.error("Unexpected Exception while retriving passenger seat", e);
		}
		/*Not found in ancillary*/
		if(seat == null){
			seat = coupon.getSeat();
		}
		return seat;
	}


	/**
	 * Manage Ticket PDF
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void manageTicketPDF(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {

		List<InputStream> logos = null;
		InputStream fontFamily = null;
		InputStream fontFamilyBold = null;

		try {

			final I18n i18n = new I18n(request.getResourceBundle(
					AlitaliaUtils.findResourceLocale(request.getResource())));

			BookingSessionContext ctx = (BookingSessionContext)
					request.getSession(true).getAttribute(
							BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);

			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				throw new RuntimeException(
						"Cannot find booking session context in session.");
			}

			if (ctx.phase != BookingPhaseEnum.DONE) {
				logger.error("This operation is not permitted in this phase: {}",
						ctx.phase != null ? ctx.phase : "null");
				throw new IllegalStateException(
						"This operation is not permitted in this phase");
			}

			// get logos
			logos = retrieveLogos(request.getResource());

			Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
			boolean rtl = locale != null &&
					createSetFromProperty(RIGHT_TO_LEFT_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()));

			if (locale != null &&
					createSetFromProperty(UNICODE_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()))) {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/arialu.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialubd.ttf");
			} else {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/Trebuchet.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/Trebuchet_Bold.ttf");
			}

			//Total Price
			String totalPrice;
			GenericPriceRender price = new GenericPriceRender(ctx.grossAmount,
					ctx.currency,ctx);
			totalPrice = price.getFare();

			//Lottomatica
			Calendar cal = Calendar.getInstance(ctx.locale);
			cal.add(Calendar.DATE, 1);
			DateRender dateRender = new DateRender(cal);

			//Stringa num Passeggeri
			String passengersNumbers = computePassengersNumberInfo(ctx, i18n);

			String totalePagatoHeader = "";
			if (!ctx.isCarnetProcess) {
				totalePagatoHeader = i18n.get("booking.thankyoupage.totalePagato.label") + " "
						+ ctx.currency + " " + totalPrice;
			}
			String[][] hederInfo0 = {
				{ i18n.get("booking.thankyoupage.codicePrenotazione.label") + " ",
					ctx.prenotation.getPnr() },
				{ totalePagatoHeader },
			};


			String[][] hederInfo2 = {
					{ i18n.get("booking.carrello.dettagliVoli.label") + " "
							+ passengersNumbers }
			};

			String[][] hederInfoHead = hederInfo0;
			String[][] lottomatica = {null, null};
			String lang = AlitaliaUtils.getRepositoryPathLanguage(
					request.getResource());

			if (ctx.paymentDataForPDF.getType() == PaymentTypeEnum.LOTTOMATICA) {
				String path = "/content/alitalia/alitalia-" + lang
						+ "/it/booking/confirmation/jcr:content/rich-text";
				try {
					// FIXME splittare sul numero di caratteri e non sul punto
					lottomatica[0] =
							retrieveTextFromCRXRepository(path).split("[.]");

					for (int i = 0; i < lottomatica[0].length; i++) {
						lottomatica[0][i] = lottomatica[0][i].trim();
						lottomatica[0][i] = lottomatica[0][i] + ".";
					}

				} catch (Exception e) {

					if (ctx.prenotation != null) {
						logger.error("Error retriving Lottomatica message from jcr. "
								+ "Path: {}, PNR: {}", path, ctx.prenotation.getPnr());
					} else {
						logger.error("Error retriving Lottomatica message from jcr. "
								+ "Path: {}, PNR: null", path);
					}
					lottomatica[0] = new String[] {""};

				}

				String lastDateToPay = dateRender.getDay() + " "
						+ i18n.get(dateRender.getTextMonth()) + " "
						+ dateRender.getYear();
				lottomatica[1] = new String[]{
						i18n.get("booking.thankyoupage.tempoLimiteAcquisto.label")
							+ ":", lastDateToPay
				};

				String[][] hederInfo1 = {
					lottomatica[0],
					lottomatica[1],
				};

				hederInfoHead = ArrayUtils.addAll(hederInfo0, hederInfo1);

			}

			hederInfoHead =  ArrayUtils.addAll(hederInfoHead, hederInfo2);

			String[][][] hederInfo = {
				hederInfoHead, {
					{ i18n.get("booking.carrello.cognome.label") + " "
							+ i18n.get("booking.carrello.nome.label"),
						i18n.get("booking.carrello.posto.label"),
						i18n.get("booking.carrello.pasto.label") }
				}
			};

			// riepilogo del volo
			String[][][] sliceInfoArray = computeRecapSlice(ctx, i18n);

			String[][][] contents = (String[][][])ArrayUtils.addAll(hederInfo, sliceInfoArray);

			// fare rules e price
			String[][][] pricesrules = computePriceFareRules(ctx, i18n, request);

			//fare rules notes
			String[] notes = computeNotes(i18n);

			response.setContentType("application/pdf");
			PDFCreator.generateTicketPDF(
					(OutputStream) response.getOutputStream(), rtl,
					fontFamily, fontFamilyBold, logos.get(0), logos.get(1),
					logos.get(2), contents, pricesrules,notes);

		} catch(Exception e) {
			throw e;
		} finally {
			for (InputStream logo : logos) {
				if (null != logo) {
					logo.close();
				}
			}
			if (null != fontFamily) {
				fontFamily.close();
			}
			if (null != fontFamilyBold) {
				fontFamilyBold.close();
			}
		}

	}

	private String[] computeNotes(I18n i18n) {
		String[] a = new String[6];
		a[0] = i18n.get("fareRules.fa.change.label") + "*";
		a[1] = i18n.get("fareRules.zz.change.label") + "*";
		a[2] = i18n.get("fareRules.fa.change.label") + "**";
		a[3] = i18n.get("fareRules.importoMaxPenale.label");
		a[4] = i18n.get("fareRules.notPermitted.label");
		a[5] = i18n.get("fareRules.casoGenerale.label");

		return a;
	}

	/*
	 * computePriceFareRules
	 */
	private String[][][] computePriceFareRules(BookingSessionContext ctx,
			I18n i18n, SlingHttpServletRequest request) {

		List<RegoleTariffarieTrattaRender> fareRules = null;
		if (!ctx.market.equalsIgnoreCase("us")
				|| !ctx.market.equalsIgnoreCase("ca")) {

			fareRules = new ArrayList<RegoleTariffarieTrattaRender>();
			if(ctx.flightSelections != null){
				fareRules = new ArrayList<RegoleTariffarieTrattaRender>();
				for(FlightSelection flightSelection : ctx.flightSelections){
					if (flightSelection != null
							&& flightSelection.getFareRules() != null) {
						fareRules.add(new RegoleTariffarieTrattaRender(
								flightSelection.getFareRules(),
								flightSelection.isTariffaLight(), i18n,
								ctx, flightSelection, flightSelection.getSelectedBrandCode()));
					}
				}
			}

		}

		String bagaglioInStivaLightValue = AlitaliaUtils.getInheritedProperty(
				request.getResource(), "bagaglioStivaLightLabel", String.class);
		String convertedLabel = "";

		// previene se per qualche ambiente e' rimasta la vecchia configurazione
		if (bagaglioInStivaLightValue != null) {
			convertedLabel = bagaglioInStivaLightValue.replaceAll("&#8364;", "EUR");
			convertedLabel = convertedLabel.replaceAll("&#[0-9]*;", "");
		}

		BigDecimal ccFeePriceAmount = new BigDecimal(0);
		if (ctx.ccFeeApplied) {
			ccFeePriceAmount = ctx.ccFeeTotalAmount;

		}

		BookingPDFPriceFareInfo bookingPDFPriceFareInfo =
				new BookingPDFPriceFareInfo(ctx, i18n, fareRules, convertedLabel, ccFeePriceAmount, brandCodeToMapOnMiniFare, miniFareCode);

		String[][][] pricesrules =  {
			bookingPDFPriceFareInfo.getPriceInfo()
		};

		return ArrayUtils.addAll(pricesrules,
				bookingPDFPriceFareInfo.getFareRulesInfo());

	}

	/*
	 * computeRecapSlice
	 */
	private String[][][] computeRecapSlice(BookingSessionContext ctx,
			I18n i18n) {

		String[][][] slice = null;

		int index = 0;
		List<AvailableFlightsSelectionRender> flightSelectionsDetails =
				new ArrayList<AvailableFlightsSelectionRender>();
		for (FlightSelection flightSelection : ctx.flightSelections) {
			if(flightSelection != null){
				AvailableFlightsSelectionRender availableFlightsSelectionRender =
						new AvailableFlightsSelectionRender(
								flightSelection.getFlightData(), ctx.locale,
								i18n, ctx.isRefreshed, null, null, ctx, index);
				flightSelectionsDetails.add(availableFlightsSelectionRender);
			}
			index++;
		}

		List<PassengerBaseData> passengers = null;
		if (ctx.prenotation != null) {
			passengers = ctx.prenotation.getPassengers();
		}
		String[] passengersInfo = new String[passengers.size()];
		String[][] ticket = new String[passengers.size()][];
		String[][] referedInfant = new String[passengers.size()][];
		String[][] seatsInfo = new String[ctx.flightSelections.length][];
		String[][] mealsInfo = new String[ctx.flightSelections.length][];
		String cuit = "";
		for (int j = 0; j < ctx.flightSelections.length; j++) {
			seatsInfo[j] = new String[passengers.size()];
			mealsInfo[j] = new String[passengers.size()];
			for(int i = 0; i < ctx.selectionRoutes.getPassengers().size(); i++) {
				passengersInfo[i] = computeInfoPassengers(passengers,i);
				ticket[i] = computeTicket(passengers,i,i18n);
				referedInfant[i] = computeReferedInfant(passengers,i,i18n);
				seatsInfo[j][i] = computeInfoSeats(passengers,i,j,ctx);
				mealsInfo[j][i] = computeInfoMeals(passengers,i,j,ctx,i18n);
				if (i==0) {
					cuit = computeCuit(passengers,i, i18n, ctx);
				}
			}
		}

		int sliceIndex = 0;
		PDFSliceInfo[] pdfSliceInfoArray =
				new PDFSliceInfo[ctx.flightSelections.length];
		for (FlightSelection flightSelection : ctx.flightSelections) {
			pdfSliceInfoArray[sliceIndex] = new PDFSliceInfo(
					flightSelectionsDetails, sliceIndex, passengersInfo, ticket,
					referedInfant, seatsInfo, mealsInfo, cuit, ctx, i18n, configuration);
			sliceIndex++;
		}

		int j = 0;
		slice = new String[pdfSliceInfoArray.length*2][][];
		for (PDFSliceInfo pdfSlice : pdfSliceInfoArray) {
			slice[j] = pdfSlice.getRecapFlight();
			slice[j+1] = pdfSlice.getRecapPassenger();
			j = j + 2;
		}

		return slice;
	}

	private String computeCuit(List<PassengerBaseData> passengers,
			int i, I18n i18n, BookingSessionContext ctx) {
		String cuit = "";
		String cuitValue = ctx.cuit;
		PassengerBaseData passenger = passengers.get(i);
		if (passenger instanceof ApplicantPassengerData && cuitValue != null) {
			ApplicantPassengerData applicantPassenger = (ApplicantPassengerData) passenger;
			switch (applicantPassenger.getArTaxInfoType()) {
				case CUIL:
					cuit = i18n.get("booking.passengersData.cuil.label") +
						" " + cuitValue;
					break;
				case CUIT:
					cuit = i18n.get("booking.passengersData.cuit.label") +
					" " + cuitValue;
					break;
				case NONE:
					cuit = "";
					break;
				case NOT_AR:
					cuit = i18n.get("booking.passengersData.altraNazionalita.label") +
							" " + i18n.get("countryData." + cuitValue);
					break;
				default:
					cuit = "";
					break;

			}
		}
		return cuit;
	}



	/*
	 * computePassengersNumberInfo
	 */
	private String computePassengersNumberInfo(BookingSessionContext ctx,
			I18n i18n) {

		String passengersInfo = "";
		int adults = ctx.searchPassengersNumber.getNumAdults();
		int children = ctx.searchPassengersNumber.getNumChildren();
		int infants = ctx.searchPassengersNumber.getNumInfants();
		passengersInfo = Integer.toString(adults);
		if (adults > 1) {
			passengersInfo += " " + i18n.get("booking.carrello.adulti.label");
		} else {
			passengersInfo += " " + i18n.get("booking.carrello.adulto.label");
		}
		if (children > 0) {
			children = ctx.searchPassengersNumber.getNumChildren();
			passengersInfo += " - " + Integer.toString(children);
			if (children > 1) {
				passengersInfo += " " + i18n.get("booking.carrello.bambini.label");
			} else {
				passengersInfo += " " + i18n.get("booking.carrello.bambino.label");
			}
		}
		if (infants > 0) {
			infants = ctx.searchPassengersNumber.getNumChildren();
			passengersInfo += " - " + Integer.toString(infants);
			if (infants > 1) {
				passengersInfo += " " + i18n.get("booking.carrello.neonati.label");
			} else {
				passengersInfo += " " + i18n.get("booking.carrello.neonato.label");
			}
		}
		return passengersInfo;
	}

	/*
	 * computeInfoPassengers
	 */
	private String computeInfoPassengers(List<PassengerBaseData> passengers,
			int i) {

		PassengerBaseData passenger = passengers.get(i);
		if (passenger instanceof AdultPassengerData) {

			AdultPassengerData typedPassenger = (AdultPassengerData) passenger;
			if (typedPassenger.getLastName() == null) {
				return null;
			}

			String secondName = "";
			if (typedPassenger.getInfo() instanceof PassengerApisSecureFlightInfoData) {
				PassengerApisSecureFlightInfoData apisSecureFlightInfo =
						(PassengerApisSecureFlightInfoData) typedPassenger.getInfo();
				if (apisSecureFlightInfo.getSecondName() != null) {
					secondName = " " + apisSecureFlightInfo.getSecondName() + " ";
				}

			} else if (typedPassenger.getInfo()
					instanceof PassengerSecureFlightInfoData) {

				PassengerSecureFlightInfoData secureFlightInfo =
						(PassengerSecureFlightInfoData) typedPassenger.getInfo();
				if (secureFlightInfo.getSecondName() != null ){
					secondName = " " + secureFlightInfo.getSecondName() + " ";
				}
			}

			return typedPassenger.getLastName() + " " + typedPassenger.getName()
				+ secondName;

		} else if (passenger instanceof ChildPassengerData) {

			ChildPassengerData typedPassenger = (ChildPassengerData) passenger;
			if (typedPassenger.getLastName() == null) {
				return null;
			}

			String secondName = "";
			if (typedPassenger.getInfo()
					instanceof PassengerApisSecureFlightInfoData) {

				PassengerApisSecureFlightInfoData apisSecureFlightInfo =
						(PassengerApisSecureFlightInfoData) typedPassenger.getInfo();
				if (apisSecureFlightInfo.getSecondName() != null) {
					secondName = " " + apisSecureFlightInfo.getSecondName() + " ";
				}

			} else if (typedPassenger.getInfo()
					instanceof PassengerSecureFlightInfoData) {

				PassengerSecureFlightInfoData secureFlightInfo =
						(PassengerSecureFlightInfoData) typedPassenger.getInfo();
				if (secureFlightInfo.getSecondName() != null ){
					secondName = " " + secureFlightInfo.getSecondName() + " ";
				}
			}

			return typedPassenger.getLastName() + " " + typedPassenger.getName()
				+ secondName;

		}
		return null;

	}

	/*
	 * computeTicket
	 */
	private String[] computeTicket(List<PassengerBaseData> passengers, int i,
			I18n i18n) {

		String ticket[] = new String[]{""};
		PassengerBaseData pax = passengers.get(i);
		if (pax instanceof AdultPassengerData) {
			AdultPassengerData passenger = (AdultPassengerData) pax;
			int j = 0;
			HashMap<String,Integer> ticketMap = new HashMap<String,Integer>();
			if (passenger.getTickets() != null) {
				for (TicketInfoData tkt : passenger.getTickets()) {
					Integer ticketIndex = ticketMap.get(tkt.getTicketNumber());
					if (ticketIndex == null) {
						ticketMap.put(tkt.getTicketNumber(), j);
						j++;
					}
				}
				int k = 0;
				ticket = new String[ticketMap.keySet().size()];
				for (String ticketNumber : ticketMap.keySet()) {
					if (k==0) {
						ticket[k] = i18n.get("booking.carrello.numeroBiglietto")
								+ " " + ticketNumber;
					}
					k++;
				}
			}

		} else if (pax instanceof ChildPassengerData) {

			ChildPassengerData passenger = (ChildPassengerData) pax;
			int j = 0;
			HashMap<String,Integer> ticketMap = new HashMap<String,Integer>();
			if (passenger.getTickets() != null) {
				for (TicketInfoData tkt : passenger.getTickets()) {
					Integer ticketIndex = ticketMap.get(tkt.getTicketNumber());
					if (ticketIndex == null) {
						ticketMap.put(tkt.getTicketNumber(), j);
						j++;
					}
				}
				int k = 0;
				ticket = new String[ticketMap.keySet().size()];
				for (String ticketNumber : ticketMap.keySet()) {
					if (k==0) {
						ticket[k] = i18n.get("booking.carrello.numeroBiglietto")
								+ " " + ticketNumber;
					}
					k++;
				}
			}

		} else if (pax instanceof InfantPassengerData) { // infant

			InfantPassengerData passenger = (InfantPassengerData) pax;
			int j = 0;
			HashMap<String,Integer> ticketMap = new HashMap<String,Integer>();
			if (passenger.getTickets() != null) {
				for (TicketInfoData tkt : passenger.getTickets()) {
					Integer ticketIndex = ticketMap.get(tkt.getTicketNumber());
					if (ticketIndex == null) {
						ticketMap.put(tkt.getTicketNumber(), j);
						j++;
					}
				}
				int k = 0;
				ticket = new String[ticketMap.keySet().size()];
				for (String ticketNumber : ticketMap.keySet()) {
					if (k==0) {
						ticket[k] = i18n.get("booking.carrello.numeroBiglietto")
								+ " " + ticketNumber;
					}
					k++;
				}

			}
		}
		return ticket;
	}

	/*
	 * computeReferedInfant
	 */
	private String[] computeReferedInfant(List<PassengerBaseData> passengers,
			int i, I18n i18n) {
		String infant[] = null;
		int indexInfant = bookingSession.computeReferedInfant(passengers, i);
		if (indexInfant > -1) {
			String[] infantTicket = computeTicket(passengers,indexInfant,i18n);
			infant = new String[infantTicket.length+1];
			InfantPassengerData infantData= (InfantPassengerData)passengers.get(indexInfant);
			infant[0] = "+ "+infantData.getLastName()+" "+infantData.getName();
			infant = (String[]) ArrayUtils.addAll(infant, infantTicket);
		}
		return infant;
	}

	/*
	 * computeInfoSeats
	 */
	private String computeInfoSeats(List<PassengerBaseData> passengers, int i,
			int slice, BookingSessionContext ctx) {

		PassengerBaseData passenger = passengers.get(i);
		if (passenger instanceof AdultPassengerData) {
			AdultPassengerData typedPassenger = (AdultPassengerData) passenger;

			if (typedPassenger.getPreferences() == null) {
				return "-";
			} else {

				if (typedPassenger.getPreferences().getSeatPreferences() != null
						&& !typedPassenger.getPreferences().getSeatPreferences()
						.isEmpty()) {

					String result = "";
					List<DirectFlightData> directFlightsList =
							ctx.selectionSearchElementDirectFlights[slice];
					for (DirectFlightData directFlight : directFlightsList) {

						String fligthNumber = directFlight.getFlightNumber();
						for (SeatPreferencesData seatPreferenceData
								: typedPassenger.getPreferences()
									.getSeatPreferences()) {

							boolean hasSeat = false;
							try {
								int numberA = Integer.parseInt(
										seatPreferenceData.getFlightNumber());
								int numberB = Integer.parseInt(fligthNumber);
								hasSeat = numberA == numberB;
							} catch(NumberFormatException e) {
								logger.debug("Error casting flight Numebr in "
										+ "Integer {} and {}. Now Using string "
										+ "for equals",
										seatPreferenceData.getFlightNumber(),
										fligthNumber);
								hasSeat = seatPreferenceData.getFlightNumber() != null && seatPreferenceData.getFlightNumber()
											.equals(fligthNumber);
							}
							if (hasSeat) {
								String number = "";
								if (seatPreferenceData.getNumber() != null) {
									number = seatPreferenceData.getNumber();
								}
								if (!result.equals("")) {
									result += " / "
											+ seatPreferenceData.getRow() + number;
								} else {
									result += seatPreferenceData.getRow() + number;
								}
							}
						}
					}
					if (result.equals("")){
						return "-";
					} else {
						return result;
					}

				} else {
					return "-";
				}
			}

		} else if (passenger instanceof ChildPassengerData) {

			ChildPassengerData typedPassenger = (ChildPassengerData) passenger;
			if (typedPassenger.getPreferences() == null) {
				return "-";
			} else {

				if (typedPassenger.getPreferences().getSeatPreferences() != null
						&& !typedPassenger.getPreferences().getSeatPreferences().isEmpty()) {

					String result = "";
					List<DirectFlightData> directFlightsList =
							ctx.selectionSearchElementDirectFlights[slice];

					for (DirectFlightData directFlight : directFlightsList) {
						String fligthNumber = directFlight.getFlightNumber();
						for (SeatPreferencesData seatPreferenceData
								: typedPassenger.getPreferences()
									.getSeatPreferences()) {
							boolean hasSeat = false;
							try {
								int numberA = Integer.parseInt(seatPreferenceData.getFlightNumber());
								int numberB = Integer.parseInt(fligthNumber);
								hasSeat = numberA == numberB;
							} catch (NumberFormatException e) {
								logger.debug("Error casting flight Numebr in "
										+ "Integer {} and {}. Now Using string "
										+ "for equals",
										seatPreferenceData.getFlightNumber(),
										fligthNumber);
								hasSeat = seatPreferenceData.getFlightNumber() != null && seatPreferenceData.getFlightNumber()
										.equals(fligthNumber);
							}

							if (hasSeat) {
								String number = "";
								if (seatPreferenceData.getNumber() != null) {
									number = seatPreferenceData.getNumber();
								}
								if (!result.equals("")) {
									result += " / "
											+ seatPreferenceData.getRow() + number;
								} else {
									result += seatPreferenceData.getRow() + number;
								}
							}
						}
					}
					if (result.equals("")){
						return "-";
					} else {
						return result;
					}

				} else {
					return "-";
				}
			}
		}
		return "-";
	}

	/*
	 * computeInfoMeals
	 */
	private String computeInfoMeals(List<PassengerBaseData> passengers, int i,
			int slice, BookingSessionContext ctx, I18n i18n) {

		PassengerBaseData passenger = passengers.get(i);
		if (passenger instanceof AdultPassengerData) {
			AdultPassengerData typedPassenger = (AdultPassengerData) passenger;
			if (typedPassenger.getPreferences() == null ){
				return "-";
			} else {
				List<DirectFlightData> directFlightsList =
						ctx.selectionSearchElementDirectFlights[slice];
				boolean isAZ = false;
				for (DirectFlightData directFlight : directFlightsList) {
					if (directFlight.getCarrier().equals("AZ")) {
						isAZ = true;
						break;
					}
				}
				if (typedPassenger.getPreferences().getMealType() != null) {
					if (isAZ) {
						return i18n.get("mealsData."
								+ typedPassenger.getPreferences().getMealType()
								.getCode());
					} else {
						return "-";
					}
				} else {
					return "-";
				}
			}
		}  else if (passenger instanceof ChildPassengerData) {
			ChildPassengerData typedPassenger = (ChildPassengerData) passenger;
			if (typedPassenger.getPreferences() == null ){
				return "-";
			} else {
				List<DirectFlightData> directFlightsList =
						ctx.selectionSearchElementDirectFlights[slice];
				boolean isAZ = false;
				for (DirectFlightData directFlight : directFlightsList) {
					if (directFlight.getCarrier().equals("AZ")) {
						isAZ = true;
						break;
					}
				}
				if (typedPassenger.getPreferences().getMealType() != null) {
					if (isAZ) {
						return i18n.get("mealsData."
								+ typedPassenger.getPreferences().getMealType()
								.getCode());
					} else {
						return "-";
					}
				} else {
					return "-";
				}
			}
		}
		return "-";
	}

	/**
	 * manage Services PDF
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void manageServicesPDF(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {

		List<InputStream> logos = null;
		InputStream fontFamily = null;
		InputStream fontFamilyBold = null;

		try {

			// check-in session context
			MmbSessionContext ctx = (MmbSessionContext)
					request.getSession(true).getAttribute(
							MmbConstants.MMB_CONTEXT_ATTRIBUTE);

			if (ctx == null) {
				logger.error("Cannot find MMB session context in session.");
				throw new RuntimeException(
						"Cannot find MMB session context in session.");
			}

			final I18n i18n = new I18n(request.getResourceBundle(
					AlitaliaUtils.findResourceLocale(request.getResource())));

			// get logos
			logos = retrieveLogos(request.getResource());

			Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
			boolean rtl = locale != null &&
					createSetFromProperty(RIGHT_TO_LEFT_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()));

			// get fonts
			if (locale != null &&
					createSetFromProperty(UNICODE_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()))) {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/arialu.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialubd.ttf");
			} else {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/Trebuchet.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/Trebuchet_Bold.ttf");
			}

			List<String[][]> ancillary_list = new ArrayList<String[][]>();
			List<String[][]> contents_list = new ArrayList<String[][]>();

			String[][] content =
					{
						{ i18n.get("booking.thankyoupage.codicePrenotazione.label"),
							ctx.pnr },
						{ i18n.get("mmb.acquistoServizi.label") },
						{ i18n.get("mmb.pdf.serviziAggiuntivi") },
						{ getPriceText(ctx.cartTotalAmount, ctx.currencyCode, i18n)},
						{ i18n.get("mmb.pdf.paymentType") },
						{ i18n.get("booking.carrello.haiPagatoCon.label")
							+ " " + ctx.paymentCreditCardType,
							ctx.paymentCreditCardNumberHide },
						{ i18n.get("booking.thankyoupage.totalePagato.label"),
								 getPriceText(ctx.cartTotalAmount, ctx.currencyCode, i18n) },
						{ i18n.get("mmb.pdf.note") },
					};


			contents_list.add(content);

			//retrieve ancillary for passenger
			List<MmbAncillariesGroup> groupsByPassenger = MmbAncillariesGroup.createGroups(
					AncillariesGroupType.BY_PASSENGER, ctx.route, ctx.cart.getPassengers(),
					ctx.cart.getAncillaries(), null);

			for (MmbAncillariesGroup passengerGroup : groupsByPassenger) {
				MmbAncillaryPassengerData passengerData = passengerGroup.getPassengerData();
				int numberAncillaries = 0;
				String ticketNumber = "";
				if (null != passengerData.getEtickets() && passengerData.getEtickets().size()>0) {
					ticketNumber = passengerData.getEtickets().get(0).getNumber();
				}

				List<MmbAncillariesGroup> groups = MmbAncillariesGroup.createGroups(
						AncillariesGroupType.BY_PASSENGER_AND_ROUTE, passengerGroup.getRoute(), Lists.asList(passengerData),
						passengerGroup.getAncillariesByStatuses(
								new MmbAncillaryStatusEnum[] {MmbAncillaryStatusEnum.ISSUED}),
							null);

				//DICHIARAZIONE PARAMETRI

				/* SEAT */
				int[] standardSeats = {0,0};
				BigDecimal standardPrice = new BigDecimal(0);
				int[] comfortSeats = {0,0};
				BigDecimal comfortPrice = new BigDecimal(0);
				String standardSeatDetailOutbound = "";
				String standardSeatDetailReturn = "";
				String comfortSeatDetailOutbound = "";
				String comfortSeatDetailReturn = "";
				String standardSeatPrice = "";
				String comfortSeatPrice = "";
				String seatEmd = "";

				/* Extra Baggage */
				int[] extraBaggages = {0,0};
				BigDecimal extraBaggagePrice = new BigDecimal(0);
				String extraBaggageDetailOutbound = "";
				String extraBaggageDetailReturn = "";
				String extraBaggageEmd = "";

				/* Lounge */
				int[] lounges = {0,0};
				BigDecimal loungePrice = new BigDecimal(0);
				String loungeDetailOutbound = "";
				String loungeDetailReturn = "";
				String loungeEmd = "";

				/* Fast Track */
				int[] fastTracks = {0,0};
				BigDecimal fastTrackPrice = new BigDecimal(0);
				String fastTrackDetailOutbound = "";
				String fastTrackDetailReturn = "";
				String fastTrackEmd = "";

				/* Meal */
				String[] meals = {"--", "--"};
				BigDecimal mealPrice = new BigDecimal(0);
				String mealDetailOutbound = "";
				String mealDetailReturn = "";
				String mealEmd = "";

				// RECUPERO ANCILLARY
				for (MmbAncillariesGroup group : groups) {

					/* SEAT */
					List<MmbAncillaryData> seatAncillaries = group.getAncillariesByType(MmbAncillaryTypeEnum.SEAT);
					for (MmbAncillaryData seatAncillary : seatAncillaries) {
						boolean standardSeatOutbound = seatAncillary.getAncillaryDetail() == null ||
								!((MmbAncillarySeatDetailData) seatAncillary.getAncillaryDetail()).isComfort();
						if (MmbUtils.getAncillariesGroupRouteType(group) == RouteTypeEnum.OUTBOUND && standardSeatOutbound) {
							standardSeats[0] = standardSeats[0] + seatAncillary.getQuantity();
							standardPrice = standardPrice.add(seatAncillary.getAmount());
							seatEmd = seatAncillary.getEmd();

						}
						if (MmbUtils.getAncillariesGroupRouteType(group) == RouteTypeEnum.OUTBOUND && !standardSeatOutbound) {
							comfortSeats[0] = comfortSeats[0] + seatAncillary.getQuantity();
							comfortPrice = comfortPrice.add(seatAncillary.getAmount());
						}
						if (MmbUtils.getAncillariesGroupRouteType(group) == RouteTypeEnum.RETURN && standardSeatOutbound) {
							standardSeats[1] = standardSeats[1] + seatAncillary.getQuantity();
							standardPrice = standardPrice.add(seatAncillary.getAmount());
						}
						if (MmbUtils.getAncillariesGroupRouteType(group) == RouteTypeEnum.RETURN && !standardSeatOutbound) {
							comfortSeats[1] = comfortSeats[1] + seatAncillary.getQuantity();
							comfortPrice = comfortPrice.add(seatAncillary.getAmount());
						}
					}


					/* Extra Baggage */
					List<MmbAncillaryData> extraBaggageAncillaries = group.getAncillariesByType(MmbAncillaryTypeEnum.EXTRA_BAGGAGE);
					for (MmbAncillaryData extraBaggageAncillary : extraBaggageAncillaries) {
						if (MmbUtils.getAncillariesGroupRouteType(group) == RouteTypeEnum.OUTBOUND) {
							extraBaggages[0] = extraBaggages[0] + extraBaggageAncillary.getQuantity();
							extraBaggageEmd = extraBaggageAncillary.getEmd();
						}
						if (MmbUtils.getAncillariesGroupRouteType(group) == RouteTypeEnum.RETURN) {
							extraBaggages[1] = extraBaggages[1] + extraBaggageAncillary.getQuantity();
						}
						extraBaggagePrice = extraBaggagePrice.add(extraBaggageAncillary.getAmount());
					}


					/* Lounge */
					List<MmbAncillaryData> loungeAncillaries = group.getAncillariesByType(MmbAncillaryTypeEnum.VIP_LOUNGE);
					for (MmbAncillaryData loungeAncillary : loungeAncillaries) {
						if (MmbUtils.getAncillariesGroupRouteType(group) == RouteTypeEnum.OUTBOUND) {
							lounges[0] = lounges[0] + loungeAncillary.getQuantity();
							loungeEmd = loungeAncillary.getEmd();
						}
						if (MmbUtils.getAncillariesGroupRouteType(group) == RouteTypeEnum.RETURN) {
							lounges[1] = lounges[1] + loungeAncillary.getQuantity();
						}
						loungePrice = loungePrice.add(loungeAncillary.getAmount());
					}

					/* Fast Tracks */
					List<MmbAncillaryData> fastTrackAncillaries = group.getAncillariesByType(MmbAncillaryTypeEnum.FAST_TRACK);
					for (MmbAncillaryData fastTrackAncillary : fastTrackAncillaries) {
						if (MmbUtils.getAncillariesGroupRouteType(group) == RouteTypeEnum.OUTBOUND) {
							fastTracks[0] = fastTracks[0] + fastTrackAncillary.getQuantity();
							fastTrackEmd = fastTrackAncillary.getEmd();
						}
						if (MmbUtils.getAncillariesGroupRouteType(group) == RouteTypeEnum.RETURN) {
							fastTracks[1] = fastTracks[1] + fastTrackAncillary.getQuantity();
						}
						fastTrackPrice = fastTrackPrice.add(fastTrackAncillary.getAmount());
					}

					/* Meals */
					List<MmbAncillaryData> mealAncillaries = group.getAncillariesByType(MmbAncillaryTypeEnum.MEAL);
					for (MmbAncillaryData mealAncillary : mealAncillaries) {
						MmbAncillaryMealDetailData mealData = (MmbAncillaryMealDetailData) mealAncillary.getAncillaryDetail();
						if (MmbUtils.getAncillariesGroupRouteType(group) == RouteTypeEnum.OUTBOUND) {
							meals[0] = mealData.getMeal();
							mealEmd = mealAncillary.getEmd();
						}
						if (MmbUtils.getAncillariesGroupRouteType(group) == RouteTypeEnum.RETURN) {
							meals[1] = mealData.getMeal();
						}
						mealPrice = mealPrice.add(mealAncillary.getAmount());
					}

				}

				//INSERIMENTO RIGHE NELLA TABELLA

				/* SEAT */
				boolean standardSeatsIssued = standardSeats[0]>0 || standardSeats[1]>0;
				boolean comfortSeatsIssued = comfortSeats[0]>0 || comfortSeats[1]>0;
				if (standardSeatsIssued) {
					standardSeatDetailOutbound = getDetail(standardSeats[0], i18n, "mmb.seat.standardSeat.label", "mmb.seat.standardSeats.label");
					standardSeatDetailReturn = getDetail(standardSeats[1], i18n, "mmb.seat.standardSeat.label", "mmb.seat.standardSeats.label");
					standardSeatPrice = getPriceText(standardPrice, ctx.currencyCode, i18n);
				}
				if (comfortSeatsIssued) {
					comfortSeatDetailOutbound = getDetail(comfortSeats[0], i18n, "mmb.seat.comfortSeat.label", "mmb.seat.comfortSeats.label");
					comfortSeatDetailReturn = getDetail(comfortSeats[1], i18n, "mmb.seat.comfortSeat.label", "mmb.seat.comfortSeats.label");
					comfortSeatPrice = getPriceText(comfortPrice, ctx.currencyCode, i18n);
				}


				if (standardSeatsIssued && comfortSeatsIssued) {
					String[][] ancillarySeatData = {
							{i18n.get("mmb.seat.postoAssegnato.label").toLowerCase() },
							{standardSeatDetailOutbound, comfortSeatDetailOutbound},
							{standardSeatDetailReturn, comfortSeatDetailReturn},
							{seatEmd, ""},
							{standardSeatPrice, comfortSeatPrice}
					};
					ancillary_list.add(ancillarySeatData);
					numberAncillaries++;
				}
				if (standardSeatsIssued && !comfortSeatsIssued) {
					String[][] ancillarySeatData = {
							{i18n.get("mmb.seat.postoAssegnato.label").toLowerCase() },
							{standardSeatDetailOutbound},
							{standardSeatDetailReturn},
							{seatEmd},
							{standardSeatPrice}
					};
					ancillary_list.add(ancillarySeatData);
					numberAncillaries++;
				}
				if (!standardSeatsIssued && comfortSeatsIssued) {
					String[][] ancillarySeatData = {
							{i18n.get("mmb.seat.postoAssegnato.label").toLowerCase() },
							{comfortSeatDetailOutbound},
							{comfortSeatDetailReturn},
							{""},
							{comfortSeatPrice}
					};
					ancillary_list.add(ancillarySeatData);
					numberAncillaries++;
				}


				/* Extra Baggage */
				boolean extraBaggageIssued =  extraBaggages[0]>0 ||  extraBaggages[1]>0;
				if (extraBaggageIssued) {
					extraBaggageDetailOutbound = getDetail(extraBaggages[0], i18n, "mmb.extraBaggage.bagaglioExtra.label", "mmb.extraBaggage.bagagliExtra.label");
					extraBaggageDetailReturn = getDetail(extraBaggages[1], i18n, "mmb.extraBaggage.bagaglioExtra.label", "mmb.extraBaggage.bagagliExtra.label");

					String[][] ancillaryExtraBaggageData = {
							{i18n.get("mmb.extraBaggage.bagaglioExtra.label").toLowerCase() },
							{extraBaggageDetailOutbound},
							{extraBaggageDetailReturn},
							{extraBaggageEmd},
							{getPriceText(extraBaggagePrice, ctx.currencyCode, i18n)}
					};
					ancillary_list.add(ancillaryExtraBaggageData);
					numberAncillaries++;
				}


				/* Lounge */
				boolean loungeIssued = lounges[0]>0 || lounges[1]>0;
				if (loungeIssued) {
					loungeDetailOutbound = getDetail(lounges[0], i18n, "mmb.lounge.chooseItemDetail.label", "mmb.lounge.chooseItemsDetail.label");
					loungeDetailReturn = getDetail(lounges[1], i18n, "mmb.lounge.chooseItemDetail.label", "mmb.lounge.chooseItemsDetail.label");

					String[][] ancillaryLoungeData = {
							{i18n.get("mmb.lounge.ancillaryItem.label").toLowerCase() },
							{loungeDetailOutbound},
							{loungeDetailReturn},
							{loungeEmd},
							{getPriceText(loungePrice, ctx.currencyCode, i18n)}
					};
					ancillary_list.add(ancillaryLoungeData);
					numberAncillaries++;
				}

				/* Fast Track */
				boolean fastTrackIssued = fastTracks[0]>0 || fastTracks[1]>0;
				if (fastTrackIssued) {
					fastTrackDetailOutbound = getDetail(fastTracks[0], i18n, "mmb.fastTrack.item.label", "mmb.fastTrack.items.label");
					fastTrackDetailReturn = getDetail(fastTracks[1], i18n, "mmb.fastTrack.item.label", "mmb.fastTrack.items.label");

					String[][] fastTrackData = {
							{i18n.get("mmb.cart.fastTrackAncillaryType.label").toLowerCase() },
							{fastTrackDetailOutbound},
							{fastTrackDetailReturn},
							{fastTrackEmd},
							{getPriceText(fastTrackPrice, ctx.currencyCode, i18n)}
					};
					ancillary_list.add(fastTrackData);
					numberAncillaries++;
				}

				/* Meals */
				boolean mealsIssued = !("--".equals(meals[0]) && "--".equals(meals[1]));
				if (mealsIssued) {
					if ("--".equals(meals[0])) {
						mealDetailOutbound = "--";
					}
					else {
						mealDetailOutbound = i18n.get("mealsData." + meals[0]).toLowerCase();
					}

					if ("--".equals(meals[1])) {
						mealDetailReturn = "--";
					}
					else {
						mealDetailReturn = i18n.get("mealsData." + meals[1]).toLowerCase();
					}

					String[][] mealData = {
							{i18n.get("mmb.cart.mealAncillaryType.label").toLowerCase() },
							{mealDetailOutbound},
							{mealDetailReturn},
							{mealEmd},
							{getPriceText(mealPrice, ctx.currencyCode, i18n)}
					};
					ancillary_list.add(mealData);
					numberAncillaries++;
				}

				if (numberAncillaries > 0) {
					String[][] passenger = new String[][] {
							{passengerData.getName() + " " + passengerData.getLastName(), i18n.get("checkin.confirmation.recap.ticketNumber") + ": " + ticketNumber},
							{i18n.get("mmb.pdf.tipologiaServizio.label"),i18n.get("mmb.common.andata.label"),i18n.get("mmb.common.ritorno.label"),i18n.get("mmb.pdf.emd.label"),i18n.get("mmb.cart.dettaglioCosti.label")},
							{Integer.toString(numberAncillaries)}

					};
					contents_list.add(passenger);
				}


			}


			// add assurance
			if (ctx.insuranceAddedToCart) {

				String assurancePrice = getPriceText(ctx.cartTotalAncillariesAmount, ctx.currencyCode, i18n);
				String[][] assurance = {

					{i18n.get("mmb.pdf.assurance")},
					{ "" },
					{assurancePrice}
				};
				String[][] assuranceHeader = new String[][] {

					{i18n.get("mmb.cart.insurance.label")},
					{i18n.get("mmb.pdf.tipologiaServizio.label"), "", i18n.get("mmb.cart.dettaglioCosti.label")},
					{"1"}

				};
				contents_list.add(assuranceHeader);
				ancillary_list.add(assurance);

			}


			// set response content type
			response.setContentType("application/pdf");

			//build contents
			String[][][] contents = fromListToTable(contents_list);

			// build table_data
			String[][][] table_data = fromListToTable(ancillary_list);


			// generate PDF
			PDFCreator.generateServicesPDF(
					(OutputStream) response.getOutputStream(), rtl,
					fontFamily, fontFamilyBold, logos.get(0), logos.get(1),
					logos.get(2), contents, table_data);


		} catch (Exception e) {
			throw e;
		} finally {
			if (null != fontFamily) {
				fontFamily.close();
			}
			if (null != fontFamilyBold) {
				fontFamilyBold.close();
			}
			for (InputStream logo : logos) {
				if (null != logo) {
					logo.close();
				}
			}
		}

	}


	/**
	 * manage balance PDF
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void manageBookingPDF(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {

		List<InputStream> logos = null;
		InputStream fontFamily = null;
		InputStream fontFamilyBold = null;

		try {

			final I18n i18n = new I18n(request.getResourceBundle(
					AlitaliaUtils.findResourceLocale(request.getResource())));

			// get logos
			logos = retrieveLogos(request.getResource());

			Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
			boolean rtl = locale != null &&
					createSetFromProperty(RIGHT_TO_LEFT_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()));
			;

			if (locale != null &&
					createSetFromProperty(UNICODE_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()))) {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/arialu.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialubd.ttf");
			} else {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/Trebuchet.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/Trebuchet_Bold.ttf");
			}

			MmbSessionContext ctx = (MmbSessionContext)
					request.getSession(true).getAttribute(
							MmbConstants.MMB_CONTEXT_ATTRIBUTE);

			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				throw new RuntimeException(
						"Cannot find booking session context in session.");
			}

			String[] description = { "" };
			try {
				String description_path = AlitaliaUtils.findSiteBaseExternalUrl(
						request.getResource(), false)
						+ "manage-my-booking/detail"
						+ "/jcr:content/mmb-manage/disclaimer-esta";
				ArrayList<String> description_list = new ArrayList<String>();
				for (String string : cleanHtml(
						retrieveTextFromCRXRepository(description_path))) {
					for (String s : getParts(string, 170)) {
						description_list.add(s);
					}
				}
				description = description_list.toArray(new String[0]);
			} catch (Exception e) { }




			String[] title={ i18n.get("booking.thankyoupage.codicePrenotazione.label"), ctx.pnr };


			// heading
			String[][] header =  {title , description};
	        // total amount
			String[][] detailsPrice=computePrice(i18n,ctx);
			// flights info
			String[][] flightsOutbound = computeFlightInfo(RouteTypeEnum.OUTBOUND,i18n, ctx);
			String[][] flightsReturn = computeFlightInfo(RouteTypeEnum.RETURN,i18n, ctx);


			String[][][] contents = {header,detailsPrice,flightsOutbound,flightsReturn};

			String[][] passengersInfo = computePassengerInfo(i18n, ctx);

			String[][] contactsInfo = computeContactInfo(i18n, ctx);

			String[][][] passengers = {passengersInfo,contactsInfo};


			response.setContentType("application/pdf");
			PDFCreator.generateBookingPDF(
					(OutputStream) response.getOutputStream(), rtl,
					fontFamily, fontFamilyBold, logos.get(0), logos.get(1),
					logos.get(2), contents, passengers);

		} catch(Exception e) {
			throw e;
		} finally {
			for (InputStream logo : logos) {
				if (null != logo) {
					logo.close();
				}
			}
			if (null != fontFamily) {
				fontFamily.close();
			}
			if (null != fontFamilyBold) {
				fontFamilyBold.close();
			}
		}

	}

	private String[][] computePassengerInfo(I18n i18n,
			MmbSessionContext ctx) {
		int numeroAdulti = 0;
		int numeroBambini = 0;
		int numeroNeonati = 0;

		ArrayList<String[]> passenger_list = new ArrayList<String[]>();
		MmbFlightData flightData = ctx.route.getFlights().get(0);
		ArrayList<MmbPassengerDataRender> passengerDataRenderList = new ArrayList<MmbPassengerDataRender>();
		for (MmbPassengerData passengerData : flightData.getPassengers()) {
			passengerDataRenderList.add(new MmbPassengerDataRender(passengerData));
		}



		/*  DATI PASSEGGERI */
		for (MmbPassengerDataRender passengerDataRender : passengerDataRenderList) {
			MmbPassengerData passengerData = passengerDataRender.getPassengerData();
			if (passengerData.getType().equals(PassengerTypeEnum.ADULT)
					|| passengerData.getType().equals(PassengerTypeEnum.NORMAL)
					|| passengerData.getType().equals(PassengerTypeEnum.APPLICANT)) {
				numeroAdulti++;
			} else if (passengerData.getType().equals(PassengerTypeEnum.CHILD)) {
				numeroBambini++;
			} else if (passengerData.getType().equals(PassengerTypeEnum.INFANT)) {
				numeroNeonati++;
			}
		  if(passengerData.getFrequentFlyerCode()!=null){
			  passenger_list.add(new String[] {passengerData.getName()+ " "+passengerData.getLastName() ,i18n.get("mmb.gestisciPrenotazione.numeroBiglietto.label") +" "+passengerData.getEticket() ,i18n.get("mmb.gestisciPrenotazione.codiceFrequentFlyer.label")+" n°" +passengerData.getFrequentFlyerCode()  });
		  }else{
			  passenger_list.add(new String[] {passengerData.getName()+ " "+passengerData.getLastName() ,i18n.get("mmb.gestisciPrenotazione.numeroBiglietto.label") +" "+passengerData.getEticket() });
		  }


		}

		String[][] passengersInfo=new String[passenger_list.size()+1][];
		int i=1;
		String numberPassenger="      "+numeroAdulti+" " +((numeroAdulti>1)  ? i18n.get("mmb.gestisciPrenotazione.adulto.label.plural") : i18n.get("mmb.gestisciPrenotazione.adulto.label"));

		if(numeroBambini>0){
			numberPassenger+=", "+numeroBambini+" " +((numeroBambini>1)? i18n.get("mmb.gestisciPrenotazione.bambino.label.plural"):i18n.get("mmb.gestisciPrenotazione.bambino.label"));
		}

		if(numeroNeonati>0){
			numberPassenger+=", "+numeroNeonati+" " +((numeroNeonati>1) ? i18n.get("mmb.gestisciPrenotazione.neonato.label.plural"):i18n.get("mmb.gestisciPrenotazione.neonato.label"));
		}

		passengersInfo[0]=new String[] {i18n.get("mmb.gestisciPrenotazione.datiPasseggeri.label") ,numberPassenger };
		for(String[] fli: passenger_list ){
			passengersInfo[i]=fli;
			i++;
		}
		return passengersInfo;
	}

	private String[][] computeContactInfo(I18n i18n, MmbSessionContext ctx) {
		String email="";
		String telefono="";
		/* CONTATTI */
		MmbPassengerData applicantPassenger = ctx.route.getApplicantPassenger();
		if (applicantPassenger != null) {
			email = applicantPassenger.getEmail1();
			if (email != null && !"".equals(email)) {
				email = email.substring(email.lastIndexOf("/") + 1);
			} else {
				email = "";
			}

			telefono = applicantPassenger.getTelephone1();
			if (telefono != null && !"".equals(telefono)) {
				String[] telefonoSplit = telefono.split("/");
				telefono = telefonoSplit[2];
			} else {
				telefono = "";
			}

		} else {
			email = "";
			telefono = "";
		}

		String[][] contactsInfo=new String[4][];
		contactsInfo[0]=new String[] {i18n.get("mmb.mail.contacts.label")};
		contactsInfo[1]=new String[] {i18n.get("mmb.mail.contacts.message")};
		contactsInfo[2]=new String[] {i18n.get("mmb.mail.contacts.email.label")+ email  };
		contactsInfo[3]=new String[] {i18n.get("mmb.mail.contacts.phone.label") + telefono};
		return contactsInfo;
	}



	private String[][] computeFlightInfo(RouteTypeEnum type,I18n i18n, MmbSessionContext ctx) {
		List<Integer> flightsWait = FlightDataUtils.computeConnectingFlightsWait(ctx.route.getFlights());
		List<MmbFlightsGroup> flightsGroupList = MmbFlightsGroup.groupByRouteType(ctx.route.getFlights());
		ArrayList<String[]> flight_list = new ArrayList<String[]>();
		int j=0;
		for (MmbFlightsGroup flightsGroup :flightsGroupList) {
			if(type.value()=="Outbound" && (flightsGroup.getType().value()=="Outbound") ){
				flight_list.add(new String[] {i18n.get("booking.flightSelect.andata.label") });
				for(MmbFlightDataRender flight: flightsGroup.getFlightDataRender()){
					String from=i18n.get(flight.getDepartureAirportI18nKey())+" ("+flight.getFlightData().getFrom().getCode()+ ")";
					String to=i18n.get(flight.getArrivalAirportI18nKey())+" ("+flight.getFlightData().getTo().getCode()+ ")";
					flight_list.add(new String[] {flight.getFlightData().getCarrier() +" "+ flight.getFlightData().getFlightNumber(),flight.getFlightData().getCabin().value()});
					flight_list.add(new String[] {flight.getDepartureDay(),i18n.get(flight.getDepartureMonth()),flight.getDepartureYear()});
					flight_list.add(new String[] {flight.getDepartureHour(),i18n.get(flight.getDepartureCityI18nKey()),from});
					flight_list.add(new String[] {flight.getArrivalHour(),i18n.get(flight.getArrivalCityI18nKey()),to});

					if(j!=flightsGroup.getFlightDataRender().size()-1){
						String wait=convertWaitingTimeToString(flightsWait.get(j));
						if(wait!=null || wait!=""){
							flight_list.add(new String[] {wait});
						}

					}

					j++;
				}
			}else if(type.value()=="Return" && (flightsGroup.getType().value()=="Return") ){
				flight_list.add(new String[] {i18n.get("booking.flightSelect.ritorno.label") });
				for(MmbFlightDataRender flight: flightsGroup.getFlightDataRender()){
					String from=i18n.get(flight.getDepartureAirportI18nKey())+" ("+flight.getFlightData().getFrom().getCode()+ ")";
					String to=i18n.get(flight.getArrivalAirportI18nKey())+" ("+flight.getFlightData().getTo().getCode()+ ")";
					flight_list.add(new String[] {flight.getFlightData().getCarrier() +" "+ flight.getFlightData().getFlightNumber(),flight.getFlightData().getCabin().value()});
					flight_list.add(new String[] {flight.getDepartureDay(),i18n.get(flight.getDepartureMonth()),flight.getDepartureYear()});
					flight_list.add(new String[] {flight.getDepartureHour(),i18n.get(flight.getDepartureCityI18nKey()),from});
					flight_list.add(new String[] {flight.getArrivalHour(),i18n.get(flight.getArrivalCityI18nKey()),to});

					if(j!=flightsGroup.getFlightDataRender().size()-1){
						String wait=convertWaitingTimeToString(flightsWait.get(j));
						if(wait!=null || wait!=""){
							flight_list.add(new String[] {wait});
						}

					}

					j++;
				}
			}

		}

		String[][] flightsInfo=new String[flight_list.size()][];
		int i=0;
		for(String[] fli: flight_list ){
			flightsInfo[i]=fli;
			i++;
		}
		return flightsInfo;
	}

	private String convertWaitingTimeToString(Integer waiting) {
		if(waiting==null){
			return "";
		}
		if(waiting.intValue()==0){
			return "";
		}
		int durationHours = waiting.intValue()/60;
		int durationMinutes = waiting.intValue() - (durationHours*60);
		String minutes = Integer.toString(durationMinutes);
		String hours = Integer.toString(durationHours);
		if(durationMinutes<10){
			minutes = "0" + minutes;
		}
		if(durationHours<10){
			hours = "0" + hours;
		}
		return hours + "H:" + minutes;
	}


	private String[][] computePrice(I18n i18n,MmbSessionContext ctx) {
		MmbTaxDataListRender taxDataListRender = new MmbTaxDataListRender();
		for (MmbTaxData taxData : ctx.route.getTaxes()) {
			if (!taxData.getCode().equals("TotalAmount")) {
				MmbTaxDataRender taxDataRender = new MmbTaxDataRender(taxData);
				taxDataRender.setPriceRender(
						new MmbPriceRender(taxData.getAmount(), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
				taxDataListRender.getTaxDataRenderList().add(taxDataRender);
			} else {
				taxDataListRender.setTotalAmountRender(
						new MmbPriceRender(taxData.getAmount(), ctx.currencyCode, ctx.currencySymbol, ctx.currentNumberFormat));
			}
		}


		ArrayList<String[]> payment_list = new ArrayList<String[]>();
		payment_list.add(new String[]{i18n.get("booking.thankyoupage.dettaglioPrenotazione.label"),i18n.get("booking.thankyoupage.totalePagato.label"),
			taxDataListRender.getTotalAmountRender().getCurrencyCode()+" "+	taxDataListRender.getTotalAmountRender().getFormattedAmount()
		});

		for(MmbTaxDataRender taxData: taxDataListRender.getTaxDataRenderList()){
			String passengerNum=(taxData.getTaxData().getPassengerTypeQuantity()!=null) ? taxData.getTaxData().getPassengerTypeQuantity()+ " " : "";
			payment_list.add(new String[]{passengerNum+ i18n.get(taxData.getCodeI18nKey()),taxData.getPriceRender().getCurrencyCode()+" "+taxData.getPriceRender().getFormattedAmount()});

		}

		String[][] totalPayment=new String[payment_list.size()][];
		int i=0;
		for(String[] fli: payment_list ){
			totalPayment[i]=fli;
			i++;
		}

		return totalPayment;
	}

	/**
	 * manage carnet PDF
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void manageCarnetPDF(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {

		List<InputStream> logos = null;
		InputStream fontFamily = null;
		InputStream fontFamilyBold = null;

		try {

			CarnetSessionContext ctx = (CarnetSessionContext)
					request.getSession(true).getAttribute(
							CarnetConstants.CARNET_CONTEXT_ATTRIBUTE);

			if (ctx == null) {
				logger.error("Cannot find carnet session context in session.");
				throw new RuntimeException(
						"Cannot find carnet session context in session.");
			}

			final I18n i18n = new I18n(request.getResourceBundle(
					AlitaliaUtils.findResourceLocale(request.getResource())));

			// get logos
			logos = retrieveLogos(request.getResource());

			Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
			boolean rtl = locale != null &&
					createSetFromProperty(RIGHT_TO_LEFT_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()));

			// get fonts
			if (locale != null &&
					createSetFromProperty(UNICODE_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()))) {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/arialu.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialubd.ttf");
			} else {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/Trebuchet.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/Trebuchet_Bold.ttf");
			}

			// carnet & payment data
			CarnetCreditCardProviderData creditCardInfo = null;
			if (ctx.paymentData.getProvider()
					instanceof CarnetCreditCardProviderData) {
				creditCardInfo = (CarnetCreditCardProviderData)
						ctx.paymentData.getProvider();
			}
			CarnetInfoCarnet infoCarnet = ctx.infoCarnet;

			String carnetnumber = (null != infoCarnet)
					? "" + infoCarnet.getTotalRoutes() : "";
			String carnetcode = (null != infoCarnet)
					? infoCarnet.getCarnetCode() : "";
			String carnetpassword = (null != infoCarnet)
					? infoCarnet.getPassword() : "";
			String carnetprice =
					(null != ctx.currencyCode && null != ctx.carnetTotalAmount)
					? ctx.currencyCode + " " + new CarnetGenericPriceRender(ctx).getFormattedPrice(ctx.carnetTotalAmount) : "";
			String user = (null != ctx.customerInfo)
					? ctx.customerInfo.getName() + " "
					+ ctx.customerInfo.getLastName() : "";
			String email = (null != ctx.customerInfo)
					? ctx.customerInfo.getEmail() : "";
			String phonenumber = (null != ctx.customerInfo)
					? ctx.customerInfo.getMobileNumber() : "";
			String cardname = (null != creditCardInfo)
					? CarnetUtils.computeCreditCardName(
							creditCardInfo.getType()) : "";
			String cardnumber = (null != creditCardInfo)
					? creditCardInfo.getCreditCardNumber() : "";

			// page description
			String[] description = { "" };
			try {
				String description_path = AlitaliaUtils.findSiteBaseExternalUrl(
						request.getResource(), false)
						+ "/carnet/confirmation"
						+ "/jcr:content/header-banner-carnet/rich-text-custom";
				ArrayList<String> description_list = new ArrayList<String>();
				for (String string : cleanHtml(
						retrieveTextFromCRXRepository(description_path))) {
					for (String s : getParts(string, 170)) {
						description_list.add(s);
					}
				}
				description = description_list.toArray(new String[0]);
			} catch (Exception e) { }

			// page title
			String title = "";
			try {
				String title_path = AlitaliaUtils.findSiteBaseExternalUrl(
						request.getResource(), false)
						+ "/carnet/confirmation"
						+ "/jcr:content/header-banner-carnet";
				title = retrieveStringPropertyFromCRXRepository(
						title_path, "titolo");
			} catch (Exception e) { }

			// heading
			String[] heading = ArrayUtils.addAll(
					new String[] { title }, description);

			// set content to print
			String[][][] contents = {
				{
					heading,
					{
						i18n.get("carnet.riepilogoScelta.acquistato.label", "",
								carnetnumber),
						carnetcode,
						i18n.get("carnet.riepilogoAcquisto.totalePagato.label"),
						carnetprice,
					}
				},
				{
					{
						i18n.get("carnet.riepilogoAcquisto.cognome.label")
							+ " " + i18n.get("carnet.riepilogoAcquisto.nome.label"),
						i18n.get("carnet.riepilogoAcquisto.telefono.label")
					},
					{ user, phonenumber },
					{
						i18n.get("carnet.riepilogoAcquisto.email.label")
				},
					{ email }
				},
				{
					{
						i18n.get("carnet.riepilogoAcquisto.codiceCarnet.label"),
						carnetcode
					},
					{
						i18n.get("carnet.riepilogoAcquisto.password.label"),
						carnetpassword
					}
				},
				{
					{ i18n.get("carnet.riepilogoAcquisto.label").toUpperCase() },
					{
						i18n.get("carnet.riepilogoAcquisto.carnet.label", "", carnetnumber),
						carnetcode,
						i18n.get("carnet.riepilogoAcquisto.metodoPagamento.label")
							+ " " + cardname.toUpperCase(),
						cardnumber,
					}
				}
			};

			// set response content type
			response.setContentType("application/pdf");

			// generate PDF
			PDFCreator.generateCarnetPDF(
					(OutputStream) response.getOutputStream(), rtl,
					fontFamily, fontFamilyBold, logos.get(0), logos.get(1),
					logos.get(2), contents);

		} catch(Exception e) {
			throw e;
		} finally {
			if (null != fontFamily) {
				fontFamily.close();
			}
			if (null != fontFamilyBold) {
				fontFamilyBold.close();
			}
			for (InputStream logo : logos) {
				if (null != logo) {
					logo.close();
				}
			}
		}

	}

	/**
	 * manage ancillary check-in PDF
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void manageAncillaryPDF(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {

		List<InputStream> logos = null;
		InputStream fontFamily = null;
		InputStream fontFamilyBold = null;

		try {

			// check-in session context
			CheckinSessionContext ctx = (CheckinSessionContext)
					request.getSession(true).getAttribute(
							CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE);

			if (ctx == null) {
				logger.error("Cannot find check-in session context in session.");
				throw new RuntimeException(
						"Cannot find check-in session context in session.");
			}

			final I18n i18n = new I18n(request.getResourceBundle(
					AlitaliaUtils.findResourceLocale(request.getResource())));

			// get logos
			logos = retrieveLogos(request.getResource());

			Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
			boolean rtl = locale != null &&
					createSetFromProperty(RIGHT_TO_LEFT_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()));

			// get fonts
			if (locale != null &&
					createSetFromProperty(UNICODE_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()))) {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/arialu.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialubd.ttf");
			} else {
				fontFamily = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/Trebuchet.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/fonts/Trebuchet_Bold.ttf");
			}

			List<String[][]> ancillary_list = new ArrayList<String[][]>();
			List<String[][]> contents_list = new ArrayList<String[][]>();

			String[][] content =
					{
						{ i18n.get("booking.thankyoupage.codicePrenotazione.label"),
							ctx.pnr },
						{ i18n.get("checkin.ancillary.confirmation.acquistoServizi.label") },
						{ i18n.get("checkin.ancillary.pdf.serviziAggiuntivi") },
						{ getPriceText(ctx.cartTotalAmount, ctx.currencyCode, i18n)},
						{ i18n.get("checkin.ancillary.pdf.paymentType") },
						{ i18n.get("booking.carrello.haiPagatoCon.label")
							+ " " + ctx.paymentCreditCardType,
							ctx.paymentCreditCardNumberHide },
						{ i18n.get("booking.thankyoupage.totalePagato.label"),
								 getPriceText(ctx.cartTotalAmount, ctx.currencyCode, i18n) },
						{ i18n.get("checkin.ancillary.pdf.note") },
					};


			contents_list.add(content);

			//retrieve ancillary for passenger

			List <CheckinAncillaryPassengerData> ancillaryPassengersData = ctx.cart.getPassengers();

			for (CheckinAncillaryPassengerData ancillaryPassengerData : ancillaryPassengersData) {
				String ticketNumber = "";
				if (null != ancillaryPassengerData.getEtickets() && ancillaryPassengerData.getEtickets().size()>0) {
					ticketNumber = ancillaryPassengerData.getEtickets().get(0).getNumber();
				}
				List<MmbAncillaryStatusEnum> ancillaryStatus = new ArrayList<MmbAncillaryStatusEnum>();
				ancillaryStatus.add(MmbAncillaryStatusEnum.ISSUED);

				int numberOfAncillaries = 0;

				for (CheckinAncillaryEticketData ticket : ancillaryPassengerData.getEtickets()) {

					/* Seat */
					List<MmbAncillaryData> seatAncillaryList = checkinSession.getAncillaryByTypeStatusAndETicket(
							ctx, MmbAncillaryTypeEnum.SEAT, ancillaryStatus, ticket.getNumber());
					for (MmbAncillaryData seatAncillary : seatAncillaryList) {
						numberOfAncillaries++;
						String seatPrice = getPriceText(seatAncillary.getAmount(), ctx.currencyCode, i18n);
						String seatEmd = "";
						String seatDetail = "";
						String seatValue = StringUtils.stripStart(((MmbAncillarySeatDetailData) seatAncillary.getAncillaryDetail()).getSeat(), "0");

						boolean standardSeat = seatAncillary.getAncillaryDetail() == null ||
								!((MmbAncillarySeatDetailData) seatAncillary.getAncillaryDetail()).isComfort();

						if (standardSeat) {

							if ( seatAncillary.getQuantity() > 1) {
								seatDetail = seatAncillary.getQuantity() + i18n.get("checkin.ancillary.seatMap.postiStandard.label");
							}
							else {
								seatDetail = i18n.get("checkin.ancillary.seatMap.postoStandard.label");
							}
							seatEmd = "";

						} else {
							if ( seatAncillary.getQuantity() > 1) {
								seatDetail = seatAncillary.getQuantity() + i18n.get("checkin.ancillary.seatMap.postiExtraComfort.label");
							}
							else {
								seatDetail = i18n.get("checkin.ancillary.seatMap.postoExtraComfort.label");
							}
							seatEmd = seatAncillary.getEmd();
						}

						String[][] ancillarySeatData = {
								{i18n.get("checkin.ancillary.seatMap.postoAssegnato.label") },
								{seatValue, seatDetail},
								{seatEmd},
								{seatPrice}
						};
						ancillary_list.add(ancillarySeatData);



					}

					/* Extra Baggage */
					List<MmbAncillaryData> extraBaggageAncillaryList = checkinSession.getAncillaryByTypeStatusAndETicket(
							ctx, MmbAncillaryTypeEnum.EXTRA_BAGGAGE, ancillaryStatus, ticket.getNumber());

					for (MmbAncillaryData extraBaggageAncillary : extraBaggageAncillaryList) {

						numberOfAncillaries++;

						String extraBaggagePrice = getPriceText(extraBaggageAncillary.getAmount(), ctx.currencyCode, i18n);

						String extraBaggageDetail = "";


						if (extraBaggageAncillary.getQuantity() > 1) {
							extraBaggageDetail = extraBaggageAncillary.getQuantity() + i18n.get("checkin.ancillary.bagagliExtra.label");
						}
						else {
							extraBaggageDetail = i18n.get("checkin.ancillary.bagaglioExtra.label");
						}


						String extraBaggageEmd = extraBaggageAncillary.getEmd();

						String[][] ancillaryExtraBaggageData = {
								{i18n.get("checkin.ancillary.bagaglioExtra.label") },
								{extraBaggageDetail},
								{extraBaggageEmd},
								{extraBaggagePrice}
						};
						ancillary_list.add(ancillaryExtraBaggageData);


					}

					/* Upgrade */

					List<MmbAncillaryData> upgradeAncillaries = checkinSession.getAncillaryByTypeStatusAndETicket(
							ctx, MmbAncillaryTypeEnum.UPGRADE, ancillaryStatus, ticket.getNumber());

					for (MmbAncillaryData upgradeAncillary : upgradeAncillaries) {

						numberOfAncillaries++;

						MmbCompartimentalClassEnum upType = ((CheckinAncillaryUpgradeDetailData)
								upgradeAncillary.getAncillaryDetail()).getCompartimentalClass();
						String newClass = (checkinSession.getBusinessCompClass(ctx).equals(upType)
								? CheckinConstants.CHECKIN_UPGRADE_BUSINESS_CART_LABEL
								: CheckinConstants.CHECKIN_UPGRADE_PREMIUM_ECONOMY_CART_LABEL);
						String quantity = "";
						if (upgradeAncillary.getQuantity()>1) {
							quantity = upgradeAncillary.getQuantity().toString();
						}
						String upgradeDetail = quantity + "upgrade " + newClass;
						String upgradeEmd = upgradeAncillary.getEmd();
						String upgradePrice = getPriceText(upgradeAncillary.getAmount(), ctx.currencyCode, i18n);

						String[][] ancillaryUpgradeData = {
								{i18n.get("checkin.cart.upgradeAncillaryType.label") },
								{upgradeDetail},
								{upgradeEmd},
								{upgradePrice}
						};
						ancillary_list.add(ancillaryUpgradeData);
					}


				}

				if (numberOfAncillaries > 0) {
					String[][] passenger = new String[][] {


							{ancillaryPassengerData.getName() + " " + ancillaryPassengerData.getLastName(), i18n.get("checkin.confirmation.recap.ticketNumber") + ": " + ticketNumber},
							{i18n.get("checkin.cart.tipologiaServizio.label"),i18n.get("checkin.cart.dettaglioServizio.label"),i18n.get("checkin.cart.emd.label"),i18n.get("checkin.cart.dettaglioCosti.label")},
							{Integer.toString(numberOfAncillaries)}


					};
					contents_list.add(passenger);
				}

			}

			// add assurance
			if (ctx.insuranceAddedToCart) {

				String assurancePrice = getPriceText(ctx.cartTotalAncillariesAmount, ctx.currencyCode, i18n);
				String[][] assurance = {

						{i18n.get("checkin.ancillary.pdf.assurance")},
						{ "" },
						{assurancePrice}
				};
				String[][] assuranceHeader = new String[][] {

					{i18n.get("checkin.ancillary.pdf.assurance.title")},
					{i18n.get("checkin.cart.tipologiaServizio.label"), "", i18n.get("checkin.cart.dettaglioCosti.label")},
					{"1"}

				};
				contents_list.add(assuranceHeader);
				ancillary_list.add(assurance);

			}



			// set response content type
			response.setContentType("application/pdf");

			//build contents
			String[][][] contents = fromListToTable(contents_list);

			// build table_data
			String[][][] table_data = fromListToTable(ancillary_list);


			// generate PDF
			PDFCreator.generateServicesPDF(
					(OutputStream) response.getOutputStream(), rtl,
					fontFamily, fontFamilyBold, logos.get(0), logos.get(1),
					logos.get(2), contents, table_data);

		} catch (Exception e) {
			throw e;
		} finally {
			if (null != fontFamily) {
				fontFamily.close();
			}
			if (null != fontFamilyBold) {
				fontFamilyBold.close();
			}
			for (InputStream logo : logos) {
				if (null != logo) {
					logo.close();
				}
			}
		}

	}

	private String[][][] fromListToTable(List<String[][]> list) {
		String[][][] table = new String[list.size()][][];
		int k = 0;
		for (String[][] data : list) {
			table[k] = data;
			k++;
		}
		return table;
	}

	/*
	 * getAncillaryType
	 */
	private String getAncillaryType(I18n i18n, MmbAncillaryTypeEnum type) {

		// FIXME cambiare le etichette prendendole da i18n
		String typeOf = "";

		switch (type) {
			case SEAT :
				typeOf = "posto";
				break;
			case EXTRA_BAGGAGE :
				typeOf = "bagaglio extra";
				break;
			case MEAL :
				typeOf = "pasto";
				break;
			case VIP_LOUNGE :
				typeOf = "vip lounge";
				break;
			case FAST_TRACK :
				typeOf = "fast track";
				break;
			case INSURANCE :
				typeOf = "assicurazione";
				break;
			case UPGRADE :
				typeOf = "upgrade";
				break;
		}

		return typeOf;
	}

	/*
	 * getAncillaryQuantity
	 */
	private String getAncillaryQuantity(I18n i18n, MmbAncillaryTypeEnum type,
			int quantity) {

		// FIXME cambiare le etichette prendendole da i18n
		String typeOf = "";

		switch (type) {
			case SEAT :
				typeOf = "posto extra";
				break;
			case EXTRA_BAGGAGE :
				typeOf = "bagaglio extra";
				break;
			case MEAL :
				typeOf = "pasto extra";
				break;
			case VIP_LOUNGE :
				typeOf = "vip lounge extra";
				break;
			case FAST_TRACK :
				typeOf = "fast track extra";
				break;
			case INSURANCE :
				typeOf = "assicurazione extra";
				break;
			case UPGRADE :
				typeOf = "upgrade extra";
				break;
		}

		return quantity + " " + typeOf;
	}

	/*
	 * Retrieve site's logos
	 */
	private List<InputStream> retrieveLogos(Resource resource)
			throws Exception {

		// logos
		List<InputStream> logos = new ArrayList<InputStream>();
		InputStream logo1 = null;
		InputStream logo2 = null;
		InputStream logo3 = null;

		// alitalia logo
		if (null != (logo1 = retrieveImageFromCRXRepository(
				AlitaliaUtils.findSiteBaseExternalUrl(resource, false)
				+ "/etc/designs/alitalia/clientlibs-1/images/logo.png"))) {
			logos.add(logo1);
		} else {
			logos.add(retrieveContentFromCRXRepository(
				"/etc/designs/alitalia/clientlibs-1/images/logo.png"));
		}

		// skyteam logo
		if (null != (logo2 = retrieveImageFromCRXRepository(
				AlitaliaUtils.findSiteBaseExternalUrl(resource, false)
				+ "/alitalia-config/header-footer"
				+ "/header/header/jcr:content/header/logo2"))) {
			logos.add(logo2);
		} else {
			logos.add(retrieveContentFromCRXRepository(
				"/etc/designs/alitalia/clientlibs/images/skyteamLogo.png"));
		}

		// ethiad logo
		if (null != (logo3 = retrieveImageFromCRXRepository(
				AlitaliaUtils.findSiteBaseExternalUrl(resource, false)
				+ "/alitalia-config/header-footer"
				+ "/header/header/jcr:content/header/logo3"))) {
			logos.add(logo3);
		} else {
			logos.add(retrieveContentFromCRXRepository(
				"/etc/designs/alitalia/clientlibs/images/empty.gif"));
		}

		// return logos
		return logos;

	}

	/*
	 * Retrieve content from Repository
	 */
	private InputStream retrieveContentFromCRXRepository(String absPath)
			throws Exception {

		// info qui: http://www.tothenew.com/blog/access-content-repository-via-getserviceresourceresolver-in-aem6sling7/
		@SuppressWarnings("deprecation")
		ResourceResolver resourceResolver =
			resolverFactory.getAdministrativeResourceResolver(null);
		Session session = resourceResolver.adaptTo(Session.class);

		Node ntFileNode = session.getNode(absPath);
		Node ntResourceNode = ntFileNode.getNode("jcr:content");
		return ntResourceNode.getProperty("jcr:data").getBinary().getStream();
	}

	/*
	 * Retrieve Image content from Repository
	 */
	private InputStream retrieveImageFromCRXRepository(String absPath)
			throws LoginException {

		// info qui: http://www.tothenew.com/blog/access-content-repository-via-getserviceresourceresolver-in-aem6sling7/
		@SuppressWarnings("deprecation")
		ResourceResolver resourceResolver =
			resolverFactory.getAdministrativeResourceResolver(null);
		Session session = resourceResolver.adaptTo(Session.class);

		Node ntFileNode = null;

		try {

			try {
				// DAM file
				ntFileNode = session.getNode(absPath);
				absPath = ntFileNode.getProperty("fileReference").getString();
				absPath = absPath + "/jcr:content/renditions/original";
				ntFileNode = session.getNode(absPath);
			} catch (PathNotFoundException e) {
				// uploaded file
				absPath = absPath + "/file";
				ntFileNode = session.getNode(absPath);
			}

			if (null != ntFileNode) {
				Node ntResourceNode = ntFileNode.getNode("jcr:content");
				return ntResourceNode.getProperty("jcr:data").getBinary().getStream();
			}

		} catch (Exception e) {
			return null;
		}

		return null;

	}

	/*
	 * Retrieve Text content from Repository
	 */
	private String retrieveTextFromCRXRepository(String absPath)
			throws Exception {

		// http://www.tothenew.com/blog/access-content-repository-via-getserviceresourceresolver-in-aem6sling7/
		@SuppressWarnings("deprecation")
		ResourceResolver resourceResolver =
			resolverFactory.getAdministrativeResourceResolver(null);
		Session session = resourceResolver.adaptTo(Session.class);

		Node ntFileNode = session.getNode(absPath);
		return ntFileNode.getProperty("text").getString();
	}

	/*
	 * Retrieve String Property From Repository
	 */
	private String retrieveStringPropertyFromCRXRepository(String absPath,
			String property) throws Exception {

		// http://www.tothenew.com/blog/access-content-repository-via-getserviceresourceresolver-in-aem6sling7/
		@SuppressWarnings("deprecation")
		ResourceResolver resourceResolver =
			resolverFactory.getAdministrativeResourceResolver(null);
		Session session = resourceResolver.adaptTo(Session.class);

		Node ntFileNode = session.getNode(absPath);
		return ntFileNode.getProperty(property).getString();
	}

	/**
	 * Get Parts - split string into parts by partition size
	 * @param input
	 * @param maxCharInLine
	 * @return
	 */
	private static String[] getParts(String input, int maxCharInLine) {

		StringTokenizer tok = new StringTokenizer(input, " ");
		StringBuilder output = new StringBuilder(input.length());
		int lineLen = 0;
		while (tok.hasMoreTokens()) {
			String word = tok.nextToken();

			while (word.length() > maxCharInLine) {
				output.append(word.substring(0, maxCharInLine - lineLen) + "\n");
				word = word.substring(maxCharInLine - lineLen);
				lineLen = 0;
			}

			if (lineLen + word.length() > maxCharInLine) {
				output.append("\n");
				lineLen = 0;
			}
			output.append(word + " ");

			lineLen += word.length() + 1;
		}

		return output.toString().split("\n");

	}

	/*
	 * Clean HTML
	 */
	private String[] cleanHtml(String input) {

		String[] output = input.split("\\r?\\n|\\<br>");
		for (int i = 0; i < output.length; i ++) {
			output[i] = output[i].replaceAll("\\<.*?>", "");
		}
		return output;

	}

	private String getPriceText(BigDecimal price, String currencyCode, I18n i18n) {
		 DecimalFormat moneyFormatter = new DecimalFormat("0.00");
		 if (price.doubleValue()>0) {
			 return currencyCode + " "  + moneyFormatter.format(price.doubleValue());
		 }
		 else {
			 return i18n.get("checkin.ancillary.pdf.gratis");
		 }

	}

	private String getDetail(int quantity, I18n i18n, String keySingle,  String keyMultiple) {
		if (quantity == 0) {
			return "--";
		}
		if ( quantity > 1) {
			return quantity + i18n.get(keyMultiple);
		}
		else {
			return i18n.get(keySingle);
		}
	}

	/*
	 * Create set from property
	 */
	public Set<String> createSetFromProperty(String propertyName) {
		String propertyValue = PropertiesUtil.toString(componentContext.getProperties().get(propertyName), "");
		String[] setElements = propertyValue.split(",");
		Set<String> set = new HashSet<String>();
		for (String setElement : setElements) {
			set.add(setElement);
		}
		return set;
	}



/* private methods to manage TariffaLight */

	/**
	 * Reads mini-fare configuration from component properties.
	 */
	private void readMiniFareConfiguration(ComponentContext ctx) {
		brandCodeToMapOnMiniFare = PropertiesUtil.toString(
				ctx.getProperties().get(MINIFARE_SOURCE_BRAND_CODE_PROPERTY), "");
		miniFareCode = PropertiesUtil.toString(
				ctx.getProperties().get(MINIFARE_TARGET_BRAND_CODE_PROPERTY), "");

	}

	public byte[] getBoardingPassPDF_1(SlingHttpServletRequest request,
									 SlingHttpServletResponse response) throws Exception {

		ByteArrayOutputStream out = (ByteArrayOutputStream)
				manageCheckinPDF_1(request, response, false);
		return out.toByteArray();

	}

	/**
	 * Manage Check-in PDF
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public OutputStream manageCheckinPDF_1(SlingHttpServletRequest request,
										 SlingHttpServletResponse response, boolean output)
			throws Exception {

		List<InputStream> logos = null;
		InputStream fontFamily = null;
		InputStream fontFamilyBold = null;
		InputStream cutHere = null;
		InputStream time = null;
		InputStream bag = null;
		InputStream banner1 = null;
		InputStream banner2 = null;
		InputStream banner3 = null;
		List<InputStream> boardingPass = new ArrayList<InputStream>();

		List<InputStream> banners = new ArrayList<InputStream>();

		OutputStream out = null;

		// choose type of output stream
		if (output) {
			out = response.getOutputStream();
		} else {
			out = new ByteArrayOutputStream();
		}

		try {

			// translator
			final I18n i18n = new I18n(request.getResourceBundle(
					AlitaliaUtils.findResourceLocale(request.getResource())));

			// check-in session context
			CheckinSessionContext ctx = (CheckinSessionContext)
					request.getSession(true).getAttribute(
							CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE);

			if (ctx == null) {
				logger.error("Cannot find check-in session context in session.");
				throw new RuntimeException(
						"Cannot find check-in session context in session.");
			}


			// get logos
			logos = retrieveLogos(request.getResource());

			Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
			boolean rtl = locale != null &&
					createSetFromProperty(RIGHT_TO_LEFT_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()));

			if (locale != null &&
					createSetFromProperty(UNICODE_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()))) {
				fontFamily = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialu.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialubd.ttf");
			} else {
				fontFamily = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arial.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialbd.ttf");
			}

			cutHere = retrieveContentFromCRXRepository(
					rtl ? "/etc/designs/alitalia/clientlibs/images/checkin_cutHere_rtl.jpg" :
							"/etc/designs/alitalia/clientlibs/images/checkin_cutHere.jpg");
			time = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/images/checkin_time.jpg");
			bag = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/images/checkin_bag.jpg");

			// notice
			String[] notice = { "" };
			try {
				String notice_path = AlitaliaUtils.findSiteBaseExternalUrl(
						request.getResource(), false)
						+ "/alitalia-config/template-pdf"
						+ "/boarding-pass/jcr:content/checkin-pdf-note";
				ArrayList<String> notice_list = new ArrayList<String>();
				for (String string : cleanHtml(
						retrieveTextFromCRXRepository(notice_path))) {
					for (String s : getParts(string, 170)) {
						notice_list.add(s);
					}
				}
				notice = notice_list.toArray(new String[0]);
			} catch (Exception e) { }

			// banners
			if (null != (banner1 = retrieveImageFromCRXRepository(
					AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), false)
							+ "/alitalia-config/template-pdf"
							+ "/boarding-pass/jcr:content/checkin-pdf-banner-1"))) {
				banners.add(banner1);
			}
			if (null != (banner2 = retrieveImageFromCRXRepository(
					AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), false)
							+ "/alitalia-config/template-pdf"
							+ "/boarding-pass/jcr:content/checkin-pdf-banner-2"))) {
				banners.add(banner2);
			}
			if (null != (banner3 = retrieveImageFromCRXRepository(
					AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), false)
							+ "/alitalia-config/template-pdf"
							+ "/boarding-pass/jcr:content/checkin-pdf-banner-3"))) {
				banners.add(banner3);
			}

			// boarding luggage alert
			String[] boardingluggage = ArrayUtils.addAll(
					new String[] { i18n.get("checkin.pdf.boardingluggage") },
					getParts(i18n.get("checkin.pdf.boardingluggage.text"), 50));


			// all passengers
			List<String[][][]> contents = new ArrayList<String[][][]>();

			BoardingPassInfo bp = ctx.boardingPassInfo;
			if (null != bp)
			{


				for (Model model : bp.getModels()) {
					String ticket = model.getTicketNumber();
					Passenger currentPassenger = new Passenger();
					for(Passenger p : ctx.checkinSelectedPassengers){
						if(p.getTicket().equals(ticket)){
							currentPassenger = p;
						}
					}
					if(currentPassenger.getSelectee() != null) {
						if (currentPassenger.getSelectee() == true) {
							continue;
						}
					}

					String _name = model.getPassengerName(); // .getFirstName() + " ";
					/*if (null != passengerData.getApisData() &&
							null != passengerData.getApisData().getSecondName()) {
						name += passengerData.getApisData().getSecondName();
					}
					name += model.getLastName();*/

					if (model.getIsInfantCoupon())
						_name += " " + i18n.get("checkin.pdf.infant");
					/*
					if (checkinCouponData.getInfantCoupon()) {
						name += " " + i18n.get("checkin.pdf.infant");
					}
					*/

					// ticket number
					String _ticket = model.getTicketNumber();/* null != passengerData.getEticket() &&
							!Boolean.TRUE.equals(checkinCouponData.getInfantCoupon())tz
							? passengerData.getEticket() : "";*/

					// baggage
					String _baggage = "";/*null != passengerData.getBaggageAllowance() &&
							!Boolean.TRUE.equals(checkinCouponData.getInfantCoupon())
							? passengerData.getBaggageAllowance() : "";*/

					// sequence number
					String _sequencenumber = model.getSequenceNumber();/*
							null != checkinCouponData.getSequenceNumber()
									? checkinCouponData.getSequenceNumber() : "";*/

					// terminal
					String _terminal = "";
					if(model.getBoardingTerminal() != null) {
						_terminal = model.getBoardingTerminal();
					}

					// gate
					String _gate = "";
					if(model.getBoardingGate() != null) {
						_gate = model.getBoardingGate();
					}

					// seat
					String _seat = model.getSeat();/* Boolean.TRUE.equals(checkinCouponData.getInfantCoupon())
							? i18n.get("checkin.pdf.seat.infant") :
							checkinCouponData.getSeat();*/

					// flight number
					//CheckinFlightData flightData =
					//		checkinCouponData.getFlight();
					String _flightnumber = model.getFlightNumber();/* flightData.getCarrier()
							+ flightData.getFlightNumber();*/

					// class
					String _compartimentalclass = model.getSeatClassName();

					// departure airport
					String _departureairport = model.getFromAirport();/* i18n.get("airportsData."
							+ model.getFromAirport() + ".city") + ", "
							+ i18n.get("airportsData."
							+ flightData.getFrom().getCode() + ".name");*/

					// arrival airport
					String _arrivalairport = model.getToAirport();/* i18n.get("airportsData."
							+ flightData.getTo().getCode() + ".city") + ", "
							+ i18n.get("airportsData."
							+ flightData.getTo().getCode() + ".name");*/

					// departure date
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Date date = sdf.parse(model.getDepartureDate());
					Calendar cal = Calendar.getInstance();
					cal.setTime(date);

					DateRender dateRender = new DateRender(cal);
					String _ddate = dateRender.getDay() + i18n.get(dateRender.getShortTextMonth());

					// departure time
					String _dtime = model.getDepartureTime();// dateRender.getHour();

					// boarding time
					/*Calendar boardingtimedate = model.getBoardingTime();

							checkinCouponData.getBoardingTime();
					dateRender = new DateRender(boardingtimedate);*/
					String _boardingtime = model.getBoardingTime();//dateRender.getHour();

					// boarding time alert
					String[] _boarding = ArrayUtils.addAll(
							new String[]{i18n.get("checkin.pdf.boardingtime")
									+ " " + _boardingtime},
							getParts(i18n.get("checkin.pdf.boardingtime.text"), 50));

					// barcode
					BarcodePDF417 _barcode = new BarcodePDF417();
					_barcode.setText(model.getBarCode());

					java.awt.Image _image = _barcode.createAwtImage(Color.black, Color.white);

					BufferedImage _bffImg = new BufferedImage(_image.getWidth(null),_image.getHeight(null), BufferedImage.TYPE_3BYTE_BGR);
					Graphics _offg = _bffImg.createGraphics();
					_offg.drawImage(_image, 0, 0, null);

					byte[] _buffer = toByteArrayAutoClosable(_bffImg, "png");
					boardingPass.add(new ByteArrayInputStream(_buffer));

					// sky priority
					String _skyPriority = Boolean.TRUE.equals(model.getIsSkyPriority())
							? i18n.get("checkin.pdf.skyPriority") : "";

					// passengers details data
					List<String[]> _passengersData = new ArrayList<String[]>();
					_passengersData.add(new String[]{
							i18n.get("checkin.pdf.title"),
							i18n.get("checkin.pdf.subtitle"),
							i18n.get("checkin.pdf.subtitle2"),
							_skyPriority});
					_passengersData.add(new String[]{
							i18n.get("checkin.pdf.securenumber"),
							_flightnumber, _sequencenumber});
					_passengersData.add(new String[]
							{i18n.get("checkin.pdf.name"), _name});

					//if (null != model.getFrequentFlyerCode()) {
						_passengersData.add(new String[]
								{i18n.get("checkin.pdf.frequentflyer"), null != model.getFrequentFlyerCode() ? (model.getSkyTeam() != null ? model.getSkyTeam() + " " + model.getFrequentFlyerCode() : model.getFrequentFlyerCode()) : ""});
					//}

					_passengersData.add(new String[]
							{i18n.get("checkin.pdf.ticketnumber"), _ticket});

					_passengersData.add(new String[]
							{i18n.get("checkin.pdf.baggage"), _baggage});



					String[][] _data = new String[_passengersData.size()][];
					for (int i = 0; i < _passengersData.size(); i++) {
						_data[i] = _passengersData.get(i);
					}


					String serviceOperatingDetails = "";
					if(!model.getServiceOperatingDetails().equals("")) {
						serviceOperatingDetails += i18n.get("checkin.pdf.operatoDa.label") + " " + i18n.get("carriersData." + model.getServiceOperatingDetails() + ".description");
					}

					String[] _secondLine = {serviceOperatingDetails};


					// TODO gestire extra
					/*
					if (flightData.isBus()) {
						String departureStation = "";
						String arrivalStation = "";
						if (flightData.isFromBusStation()) {
							departureStation = i18n.get("checkin.bus.operatoDa.label");
						}
						if (flightData.isToBusStation()) {
							arrivalStation = i18n.get("checkin.bus.operatoDa.label");
						}
						String[] busLine = {
								"", "", "", departureStation,
								arrivalStation, "", "", "",
								"", ""
						};
						secondLine = busLine;

					} else if (checkinCouponData.getServiceOperatingDetails() != null &&
							!checkinCouponData.getServiceOperatingDetails().isEmpty()) {
						String[] otherLine = {
								i18n.get("checkin.pdf.operatoDa.label") + " " +
										checkinCouponData.getServiceOperatingDetails(), "", "", "",
								"", "", "", "", "", ""
						};
						secondLine = otherLine;
					}
					*/

					// data to print
					String[][][] _content = {
							_data,
							{
									{
											i18n.get("checkin.pdf.flight"),
											i18n.get("checkin.pdf.terminal"),
											i18n.get("checkin.pdf.date"),
											i18n.get("checkin.pdf.from"),
											i18n.get("checkin.pdf.to"),
											i18n.get("checkin.pdf.departure"),
											i18n.get("checkin.pdf.gate"),
											i18n.get("checkin.pdf.boarding"),
											i18n.get("checkin.pdf.class"),
											i18n.get("checkin.pdf.seat")
									},
									{
											i18n.get("checkin.pdf.flight.generic"),
											i18n.get("checkin.pdf.terminal.generic"),
											i18n.get("checkin.pdf.date.generic"),
											i18n.get("checkin.pdf.from.generic"),
											i18n.get("checkin.pdf.to.generic"),
											i18n.get("checkin.pdf.departure.generic"),
											i18n.get("checkin.pdf.gate.generic"),
											i18n.get("checkin.pdf.boarding.generic"),
											i18n.get("checkin.pdf.class.generic"),
											i18n.get("checkin.pdf.seat.generic")
									},
									{
											_flightnumber, _terminal, _ddate, _departureairport,
											_arrivalairport, _dtime, _gate, _boardingtime,
											_compartimentalclass, _seat
									},
									_secondLine,
							},
							{boardingluggage, _boarding},
							{
									{i18n.get("checkin.pdf.cuthere")},
									{i18n.get("checkin.pdf.notice")},
									notice
							}
					};

					contents.add(_content);

				}
			}

			// send response => PDF
			if (output) {
				response.setContentType("application/pdf");
			}

			// generate PDF
			PDFCreator.generateCheckinPDF(
					out, rtl, fontFamily, fontFamilyBold, logos.get(0),
					logos.get(1), logos.get(2), contents, cutHere, time, bag,
					boardingPass, banners);

		} catch(Exception e) {
			throw e;
		} finally {
			for (InputStream logo : logos) {
				if (null != logo) {
					logo.close();
				}
			}
			if (null != fontFamily) {
				fontFamily.close();
			}
			if (null != fontFamilyBold) {
				fontFamilyBold.close();
			}
			if (null != cutHere) {
				cutHere.close();
			}
			if (null != time) {
				time.close();
			}
			if (null != bag) {
				bag.close();
			}
			for (InputStream inputStream : boardingPass) {
				if (null != inputStream) {
					inputStream.close();
				}
			}
			for (InputStream inputStream : banners) {
				if (null != inputStream) {
					inputStream.close();
				}
			}
			if (null != banner1) {
				banner1.close();
			}
			if (null != banner2) {
				banner2.close();
			}
			if (null != banner3) {
				banner3.close();
			}
		}

		// return stream
		return out;

	}

	public OutputStream downloadPdfSms(SlingHttpServletRequest request,
									   SlingHttpServletResponse response, boolean output,BoardingPassInfo boardingPassInfo)
			throws Exception {

		List<InputStream> logos = null;
		InputStream fontFamily = null;
		InputStream fontFamilyBold = null;
		InputStream cutHere = null;
		InputStream time = null;
		InputStream bag = null;
		InputStream banner1 = null;
		InputStream banner2 = null;
		InputStream banner3 = null;
		List<InputStream> boardingPass = new ArrayList<InputStream>();

		List<InputStream> banners = new ArrayList<InputStream>();

		OutputStream out = null;

		// choose type of output stream
		if (output) {
			out = response.getOutputStream();
		} else {
			out = new ByteArrayOutputStream();
		}

		try {

			// translator
			final I18n i18n = new I18n(request.getResourceBundle(
					AlitaliaUtils.findResourceLocale(request.getResource())));


			// get logos
			logos = retrieveLogos(request.getResource());

			Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
			boolean rtl = locale != null &&
					createSetFromProperty(RIGHT_TO_LEFT_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()));

			if (locale != null &&
					createSetFromProperty(UNICODE_LANGUAGES).contains(AlitaliaUtils.getLanguage(locale.toLanguageTag()))) {
				fontFamily = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialu.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialubd.ttf");
			} else {
				fontFamily = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arial.ttf");
				fontFamilyBold = retrieveContentFromCRXRepository(
						"/etc/designs/alitalia/clientlibs/fonts/arialbd.ttf");
			}

			cutHere = retrieveContentFromCRXRepository(
					rtl ? "/etc/designs/alitalia/clientlibs/images/checkin_cutHere_rtl.jpg" :
							"/etc/designs/alitalia/clientlibs/images/checkin_cutHere.jpg");
			time = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/images/checkin_time.jpg");
			bag = retrieveContentFromCRXRepository(
					"/etc/designs/alitalia/clientlibs/images/checkin_bag.jpg");

			// notice
			String[] notice = { "" };
			try {
				String notice_path = AlitaliaUtils.findSiteBaseExternalUrl(
						request.getResource(), false)
						+ "/alitalia-config/template-pdf"
						+ "/boarding-pass/jcr:content/checkin-pdf-note";
				ArrayList<String> notice_list = new ArrayList<String>();
				for (String string : cleanHtml(
						retrieveTextFromCRXRepository(notice_path))) {
					for (String s : getParts(string, 170)) {
						notice_list.add(s);
					}
				}
				notice = notice_list.toArray(new String[0]);
			} catch (Exception e) { }

			// banners
			if (null != (banner1 = retrieveImageFromCRXRepository(
					AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), false)
							+ "/alitalia-config/template-pdf"
							+ "/boarding-pass/jcr:content/checkin-pdf-banner-1"))) {
				banners.add(banner1);
			}
			if (null != (banner2 = retrieveImageFromCRXRepository(
					AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), false)
							+ "/alitalia-config/template-pdf"
							+ "/boarding-pass/jcr:content/checkin-pdf-banner-2"))) {
				banners.add(banner2);
			}
			if (null != (banner3 = retrieveImageFromCRXRepository(
					AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), false)
							+ "/alitalia-config/template-pdf"
							+ "/boarding-pass/jcr:content/checkin-pdf-banner-3"))) {
				banners.add(banner3);
			}

			// boarding luggage alert
			String[] boardingluggage = ArrayUtils.addAll(
					new String[] { i18n.get("checkin.pdf.boardingluggage") },
					getParts(i18n.get("checkin.pdf.boardingluggage.text"), 50));


			// all passengers
			List<String[][][]> contents = new ArrayList<String[][][]>();

			BoardingPassInfo bp = boardingPassInfo;
			if (null != bp)
			{


				for (Model model : bp.getModels()) {

					String _name = model.getPassengerName(); // .getFirstName() + " ";
					/*if (null != passengerData.getApisData() &&
							null != passengerData.getApisData().getSecondName()) {
						name += passengerData.getApisData().getSecondName();
					}
					name += model.getLastName();*/

					if (model.getIsInfantCoupon())
						_name += " " + i18n.get("checkin.pdf.infant");
					/*
					if (checkinCouponData.getInfantCoupon()) {
						name += " " + i18n.get("checkin.pdf.infant");
					}
					*/

					// ticket number
					String _ticket = model.getTicketNumber();/* null != passengerData.getEticket() &&
							!Boolean.TRUE.equals(checkinCouponData.getInfantCoupon())
							? passengerData.getEticket() : "";*/

					// baggage
					String _baggage = "";/*null != passengerData.getBaggageAllowance() &&
							!Boolean.TRUE.equals(checkinCouponData.getInfantCoupon())
							? passengerData.getBaggageAllowance() : "";*/

					// sequence number
					String _sequencenumber = model.getSequenceNumber();/*
							null != checkinCouponData.getSequenceNumber()
									? checkinCouponData.getSequenceNumber() : "";*/

					// terminal
					String _terminal = model.getBoardingTerminal();/* null != checkinCouponData.getTerminal()
							? checkinCouponData.getTerminal() : "";*/

					// gate
					String _gate = model.getBoardingGate();/* null != checkinCouponData.getGate()
							? checkinCouponData.getGate() : "";*/

					// seat
					String _seat = model.getSeat();/* Boolean.TRUE.equals(checkinCouponData.getInfantCoupon())
							? i18n.get("checkin.pdf.seat.infant") :
							checkinCouponData.getSeat();*/

					// flight number
					//CheckinFlightData flightData =
					//		checkinCouponData.getFlight();
					String _flightnumber = model.getFlightNumber();/* flightData.getCarrier()
							+ flightData.getFlightNumber();*/

					// class
					String _compartimentalclass = model.getSeatClassCode() + " "
							+ model.getSeatClassName();

					// departure airport
					String _departureairport = model.getFromAirport();/* i18n.get("airportsData."
							+ model.getFromAirport() + ".city") + ", "
							+ i18n.get("airportsData."
							+ flightData.getFrom().getCode() + ".name");*/

					// arrival airport
					String _arrivalairport = model.getToAirport();/* i18n.get("airportsData."
							+ flightData.getTo().getCode() + ".city") + ", "
							+ i18n.get("airportsData."
							+ flightData.getTo().getCode() + ".name");*/

					// departure date
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Date date = sdf.parse(model.getDepartureDate());
					Calendar cal = Calendar.getInstance();
					cal.setTime(date);

					DateRender dateRender = new DateRender(cal);
					String _ddate = dateRender.getDay() + i18n.get(dateRender.getShortTextMonth());

					// departure time
					String _dtime = model.getDepartureTime();// dateRender.getHour();

					// boarding time
					/*Calendar boardingtimedate = model.getBoardingTime();

							checkinCouponData.getBoardingTime();
					dateRender = new DateRender(boardingtimedate);*/
					String _boardingtime = model.getBoardingTime();//dateRender.getHour();

					// boarding time alert
					String[] _boarding = ArrayUtils.addAll(
							new String[]{i18n.get("checkin.pdf.boardingtime")
									+ " " + _boardingtime},
							getParts(i18n.get("checkin.pdf.boardingtime.text"), 50));

					// barcode
					BarcodePDF417 _barcode = new BarcodePDF417();
					_barcode.setText(model.getBarCode());

					java.awt.Image _image = _barcode.createAwtImage(Color.black, Color.white);

					BufferedImage _bffImg = new BufferedImage(_image.getWidth(null),_image.getHeight(null), BufferedImage.TYPE_3BYTE_BGR);
					Graphics _offg = _bffImg.createGraphics();
					_offg.drawImage(_image, 0, 0, null);

					byte[] _buffer = toByteArrayAutoClosable(_bffImg, "png");
					boardingPass.add(new ByteArrayInputStream(_buffer));

					// sky priority
					String _skyPriority = Boolean.TRUE.equals(model.getIsSkyPriority())
							? i18n.get("checkin.pdf.skyPriority") : "";

					// passengers details data
					List<String[]> _passengersData = new ArrayList<String[]>();
					_passengersData.add(new String[]{
							i18n.get("checkin.pdf.title"),
							i18n.get("checkin.pdf.subtitle"),
							i18n.get("checkin.pdf.subtitle2"),
							_skyPriority});
					_passengersData.add(new String[]{
							i18n.get("checkin.pdf.securenumber"),
							_flightnumber, _sequencenumber});
					_passengersData.add(new String[]
							{i18n.get("checkin.pdf.name"), _name});

					//if (null != model.getFrequentFlyerCode()) {
					_passengersData.add(new String[]
							{i18n.get("checkin.pdf.frequentflyer"), null != model.getFrequentFlyerCode() ? model.getFrequentFlyerCode() : ""});
					//}

					_passengersData.add(new String[]
							{i18n.get("checkin.pdf.ticketnumber"), _ticket});

					_passengersData.add(new String[]
							{i18n.get("checkin.pdf.baggage"), _baggage});



					String[][] _data = new String[_passengersData.size()][];
					for (int i = 0; i < _passengersData.size(); i++) {
						_data[i] = _passengersData.get(i);
					}

					String[] _secondLine = null;

					// TODO gestire extra
					/*
					if (flightData.isBus()) {
						String departureStation = "";
						String arrivalStation = "";
						if (flightData.isFromBusStation()) {
							departureStation = i18n.get("checkin.bus.operatoDa.label");
						}
						if (flightData.isToBusStation()) {
							arrivalStation = i18n.get("checkin.bus.operatoDa.label");
						}
						String[] busLine = {
								"", "", "", departureStation,
								arrivalStation, "", "", "",
								"", ""
						};
						secondLine = busLine;

					} else if (checkinCouponData.getServiceOperatingDetails() != null &&
							!checkinCouponData.getServiceOperatingDetails().isEmpty()) {
						String[] otherLine = {
								i18n.get("checkin.pdf.operatoDa.label") + " " +
										checkinCouponData.getServiceOperatingDetails(), "", "", "",
								"", "", "", "", "", ""
						};
						secondLine = otherLine;
					}
					*/

					// data to print
					String[][][] _content = {
							_data,
							{
									{
											i18n.get("checkin.pdf.flight"),
											i18n.get("checkin.pdf.terminal"),
											i18n.get("checkin.pdf.date"),
											i18n.get("checkin.pdf.from"),
											i18n.get("checkin.pdf.to"),
											i18n.get("checkin.pdf.departure"),
											i18n.get("checkin.pdf.gate"),
											i18n.get("checkin.pdf.boarding"),
											i18n.get("checkin.pdf.class"),
											i18n.get("checkin.pdf.seat")
									},
									{
											i18n.get("checkin.pdf.flight.generic"),
											i18n.get("checkin.pdf.terminal.generic"),
											i18n.get("checkin.pdf.date.generic"),
											i18n.get("checkin.pdf.from.generic"),
											i18n.get("checkin.pdf.to.generic"),
											i18n.get("checkin.pdf.departure.generic"),
											i18n.get("checkin.pdf.gate.generic"),
											i18n.get("checkin.pdf.boarding.generic"),
											i18n.get("checkin.pdf.class.generic"),
											i18n.get("checkin.pdf.seat.generic")
									},
									{
											_flightnumber, _terminal, _ddate, _departureairport,
											_arrivalairport, _dtime, _gate, _boardingtime,
											_compartimentalclass, _seat
									},
									_secondLine,
							},
							{boardingluggage, _boarding},
							{
									{i18n.get("checkin.pdf.cuthere")},
									{i18n.get("checkin.pdf.notice")},
									notice
							}
					};

					contents.add(_content);

				}
			}

			// send response => PDF
			if (output) {
				response.setContentType("application/pdf");
			}

			// generate PDF
			PDFCreator.generateCheckinPDF(
					out, rtl, fontFamily, fontFamilyBold, logos.get(0),
					logos.get(1), logos.get(2), contents, cutHere, time, bag,
					boardingPass, banners);

		} catch(Exception e) {
			throw e;
		} finally {
			for (InputStream logo : logos) {
				if (null != logo) {
					logo.close();
				}
			}
			if (null != fontFamily) {
				fontFamily.close();
			}
			if (null != fontFamilyBold) {
				fontFamilyBold.close();
			}
			if (null != cutHere) {
				cutHere.close();
			}
			if (null != time) {
				time.close();
			}
			if (null != bag) {
				bag.close();
			}
			for (InputStream inputStream : boardingPass) {
				if (null != inputStream) {
					inputStream.close();
				}
			}
			for (InputStream inputStream : banners) {
				if (null != inputStream) {
					inputStream.close();
				}
			}
			if (null != banner1) {
				banner1.close();
			}
			if (null != banner2) {
				banner2.close();
			}
			if (null != banner3) {
				banner3.close();
			}
		}

		// return stream
		return out;

	}


	private static byte[] toByteArrayAutoClosable(BufferedImage image, String type) throws Exception {
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
			ImageIO.write(image, type, out);
			return out.toByteArray();
		}
	}

}
