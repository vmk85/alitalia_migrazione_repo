package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	//@Property(name = "sling.servlet.selectors", value = { "checkinenhancedseatmap"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class CheckinEnhancedSeatMapServlet extends GenericCheckinFormValidatorServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private CheckinSession checkinSession;

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

		logger.info("Entrati in [CheckinEnhancedSeatMapServlet - performSubmit]");

		CheckinSessionContext ctx = getCheckInSessionContext(request);
		
		try {
		
			//DATI INVIATI DA FORM			
//			enhancedSeatMapRequest.get_enhancedseatmapReq().setAirline(request.getParameter("airline"));
//			enhancedSeatMapRequest.get_enhancedseatmapReq().setFlight(request.getParameter("flight"));
//			enhancedSeatMapRequest.get_enhancedseatmapReq().setBookingClass(request.getParameter("bookingClass"));
//			enhancedSeatMapRequest.get_enhancedseatmapReq().setOrigin(request.getParameter("origin"));
//			enhancedSeatMapRequest.get_enhancedseatmapReq().setDestination(request.getParameter("destination"));
//			enhancedSeatMapRequest.get_enhancedseatmapReq().setDepartureDate(request.getParameter("departureDate"));
//			enhancedSeatMapRequest.get_enhancedseatmapReq().setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
//			enhancedSeatMapRequest.get_enhancedseatmapReq().setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
//			enhancedSeatMapRequest.get_enhancedseatmapReq().setConversationID(IDFactory.getTid());
//			
//			for (int i = 0; i <= 7; i ++) {
//				request.getParameter("passenger_" + i);
//				
//			}

			//CheckinEnhancedSeatMapResponse enhancedSeatMapResponse = checkinSession.checkinEnhancedSeatMap(request, ctx);
			
			//TODO aggiungere riposta json in caso di success e fail
			
		}
		catch (Exception e) {
			throw new RuntimeException("[CheckinEnhancedSeatMapServlet] - Errore durante l'invocazione del servizio.", e);
		}
		
//		EnhancedSeatMapResp mapResponse = enhancedSeatMapResponse.get_enhancedseatmapResp();
//		List<SeatMapSector> seatMapSector = mapResponse.getSeatMAp().getSeatMapSectors();
//		List<SeatMapColumn> seatMapColumn = mapResponse.getSeatMAp().getSeatMapColumns();
//		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
//		response.setContentType("application/json");
//		json.key("seatMAp");
//		json.object();
//		json.key("cabinType").value(mapResponse.getSeatMAp().getCabinType());
//		json.key("startingRow").value(mapResponse.getSeatMAp().getStartingRow());
//		json.key("endingRow").value(mapResponse.getSeatMAp().getEndingRow());
//		json.key("seatMapSectors");
//		json.array();
//		for (SeatMapSector seatmapsector : seatMapSector) {
//			//for (int i = 0; i < seatMapSector.size(); i++) {
//			json.key("startingRow").value(seatmapsector.getStartingRow());
//			json.key("endingRow").value(seatmapsector.getEndingRow());
//			json.key("seatMapRows");
//			json.array();
//			List<SeatMapRow> seatMapRows = seatmapsector.getSeatMapRows();
//			for (SeatMapRow seatmaprows : seatMapRows) {
//				//for (int j = 0; j < seatMapRows.size(); j++) {
//				json.key("rowNumber").value(seatmaprows.getRowNumber());
//				json.key("seats");
//				json.array();
//				List<Seat> seatList = seatmaprows.getSeats();
//				for (Seat seats : seatList) {
//					//for (int k = 0; k < seats.size(); i++) {
//					json.key("number").value(seats.getNumber());
//					json.key("availability").value(seats.getAvailability());
//					json.key("type").value(seats.getType());
//					json.key("price").value(seats.getPrice());
//					json.key("confortSeat").value(seats.getConfortSeat());
//					//json.key("passengerId").value(seats.get(k).getPassengerId()); //dati commentati perchè non vanno inclusi
//					//json.key("occupatoDa").value(seats.get(k).getOccupatoDa()); //nel Json da passare al JavaScript	
//				}
//				json.endArray();
//			}
//			json.endArray();
//		}
//		json.endArray();
//		json.key("seatMapColumns");
//		json.array();
//		for (SeatMapColumn seatmapcolumn : seatMapColumn) {
//			//for (int z = 0; z < seatMapColumn.size(); z++) {
//			json.key("column").value(seatmapcolumn.getColumn());
//			json.key("typeCharacteristics").value(seatmapcolumn.getTypeCharacteristics());
//		}
//		json.endArray();
//		json.key("conversationID").value(mapResponse.getConversationID());
//		json.endObject();


		//response.sendRedirect(successPage);



		//TODO - redirect to errorPage?
//		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
//		response.setContentType("application/json");
//		json.object();
//		json.key("isError").value(true);
//		json.endObject();

	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		// TODO Auto-generated method stub
		return configuration;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		Map<String,String> additionalParams = new HashMap<String, String>();
		return additionalParams;
	}


	//	@Override
	//	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
	//
	//			CheckinSessionContext ctx = getCheckInSessionContext(request);
	//			
	//			String errorUrl = AlitaliaUtils.findSiteBaseExternalUrl(
	//					request.getResource(), false)
	//					+ getConfiguration().getCheckinFailurePage();
	//			
	//			errorUrl = request.getResourceResolver().map(errorUrl);
	//			
	//			String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
	//					request.getResource(), false)
	//					+ getConfiguration().getCheckinPassengersPage();
	//			
	//			successPage = request.getResourceResolver().map(successPage);
	//			
	//			boolean result = checkinSession.checkinEnhancedSeatMap(request, ctx);
	//			
	//			EnhancedSeatMapResp mapResponse = new EnhancedSeatMapResp();
	//			List<SeatMapSector> seatMapSector = mapResponse.getSeatMAp().getSeatMapSectors();
	//			List<SeatMapColumn> seatMapColumn = mapResponse.getSeatMAp().getSeatMapColumns();
	//			
	//			if (result) {
	//				
	//				TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
	//				response.setContentType("application/json");
	//				json.key("seatMAp");
	//				json.object();
	//				json.key("cabinType").value(mapResponse.getSeatMAp().getCabinType());
	//				json.key("startingRow").value(mapResponse.getSeatMAp().getStartingRow());
	//				json.key("endingRow").value(mapResponse.getSeatMAp().getEndingRow());
	//				json.key("seatMapSectors");
	//				json.array();
	//				for (SeatMapSector seatmapsector : seatMapSector) {
	//				//for (int i = 0; i < seatMapSector.size(); i++) {
	//					json.key("startingRow").value(seatmapsector.getStartingRow());
	//					json.key("endingRow").value(seatmapsector.getEndingRow());
	//					json.key("seatMapRows");
	//					json.array();
	//					List<SeatMapRow> seatMapRows = seatmapsector.getSeatMapRows();
	//					for (SeatMapRow seatmaprows : seatMapRows) {
	//					//for (int j = 0; j < seatMapRows.size(); j++) {
	//						json.key("rowNumber").value(seatmaprows.getRowNumber());
	//						json.key("seats");
	//						json.array();
	//						List<Seat> seatList = seatmaprows.getSeats();
	//						for (Seat seats : seatList) {
	//						//for (int k = 0; k < seats.size(); i++) {
	//							json.key("number").value(seats.getNumber());
	//							json.key("availability").value(seats.getAvailability());
	//							json.key("type").value(seats.getType());
	//							json.key("price").value(seats.getPrice());
	//							json.key("confortSeat").value(seats.getConfortSeat());
	//							//json.key("passengerId").value(seats.get(k).getPassengerId()); //dati commentati perchè non vanno inclusi
	//							//json.key("occupatoDa").value(seats.get(k).getOccupatoDa()); //nel Json da passare al JavaScript	
	//						}
	//						json.endArray();
	//					}
	//					json.endArray();
	//				}
	//				json.endArray();
	//				json.key("seatMapColumns");
	//				json.array();
	//				for (SeatMapColumn seatmapcolumn : seatMapColumn) {
	//				//for (int z = 0; z < seatMapColumn.size(); z++) {
	//					json.key("column").value(seatmapcolumn.getColumn());
	//					json.key("typeCharacteristics").value(seatmapcolumn.getTypeCharacteristics());
	//				}
	//				json.endArray();
	//				json.key("conversationID").value(mapResponse.getConversationID());
	//				json.endObject();
	//				
	//				//response.sendRedirect(successPage);
	//			} else {
	//				//TODO - redirect to errorPage?
	//				TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
	//				response.setContentType("application/json");
	//				json.object();
	//				json.key("isError").value(true);
	//				json.endObject();
	//			}
	//		
	//	}

	
}