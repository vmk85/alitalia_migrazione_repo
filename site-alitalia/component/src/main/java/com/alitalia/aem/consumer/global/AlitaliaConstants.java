package com.alitalia.aem.consumer.global;

public class AlitaliaConstants {
	
	public static final String CONTENT_PATH = "/content/alitalia";
	public static final String LOCAL_SITE_PREFIX = "alitalia-";
	public static final String MASTER_SITE_PREFIX = "master-";
	
    // levels
    public static final int SITE_ROOT_LEVEL = 1;
    public static final int SITE_HOME_LEVEL = 3;
    public static final int REPOSITORY_PATH_COUNTRY_DEPTH = 3;
    public static final int REPOSITORY_PATH_LANGUAGE_DEPTH = 4;
    
    // paths and page names
    public static final String BASE_CONFIG_PATH = "/alitalia-config";
    public static final String BASE_TEMPLATE_EMAIL_PATH = BASE_CONFIG_PATH + "/template-email";
    public static final String BASE_BRAND_PATH = BASE_CONFIG_PATH + "/brand";
    public static final String BASE_BRAND_AWARD_PATH = BASE_CONFIG_PATH + "/brand-award";
    public static final String DESTINATIONS_PATH = BASE_CONFIG_PATH + "/destinazioni";
    public static final String DESTINATIONS_PATH_1 = BASE_CONFIG_PATH + "/destinazioni-1";
    public static final String EDITORIAL_DESTINATIONS_PATH_1 = BASE_CONFIG_PATH + "/special-offers-editorial";
    public static final String GENERAL_CONFIG_PAGE_NAME = "general-configuration";
    public static final String PERSONAL_AREA_PAGE_NAME = "personal-area";
    public static final String J_SECURITY_CHECK = "j_security_check";
    public static final String MM_LOGOUT_SELECTOR = "home-page.millemiglialogout.html";
    
    // templates
    public static final String TEMPLATE_SECOND_LEVEL= "/apps/alitalia/templates/pagine-secondo-livello";
    public static final String TEMPLATE_DESTINATION_PAGE = "/apps/alitalia/templates/destinazione-page";
    public static final String TEMPLATE_DESTINATION_PAGE_1 = "/apps/alitalia/templates/destinazione-page-1";

    // cookies
    public static final String COOKIE_NO_THIRDPARTY_COOKIES = "no3rdparty";
    public static final String COOKIE_NO_MARKETING_COOKIES = "nomarketing";
    public static final String COOKIE_REMEMBER_LOCALE = "alitalia_consumer_locale";
    public static final String COOKIE_CONFIRMED = "alitaliaCookieConfirmed";
    public static final int COOKIES_MAX_AGE = Integer.MAX_VALUE;
    
    // other
    public static final String HTML_EXT = ".html";
    public static final String JSON_CONTENT_TYPE = "application/json";
    public static final String DEFAULT_MENU_DEPTH = "2";
    public static final int MAX_NUM_OFFERS_HP = 3;
    
	public static final String MESSAGES_DATE_FORMAT = "dd/MM/yy";
	public static final String REQUEST_DATE_FORMAT = "dd/MM/yyyy";
	
	public static final String COUNTRY_ITA = "IT";
	public static final String COUNTRY_UNG = "UN";
	
	public static final String OFFER_AREA_MONDO = "MONDO";
	public static final String OFFER_AREA_ITALIA = "ITALIA";
	public static final String OFFER_AREA_EUROPA = "EUROPA";
	
	public static final String PLACEHOLDER_PIN = "[MILLEMIGLIA.PIN]";
    
}