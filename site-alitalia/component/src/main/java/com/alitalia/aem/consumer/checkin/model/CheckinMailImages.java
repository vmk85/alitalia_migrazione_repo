package com.alitalia.aem.consumer.checkin.model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinMailImages extends GenericBaseModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private final static String LOGO_NAME = "logo.png";
	private final static String LOGO2_NAME = "logo2.png";
	private final static String TELEPHONE_IMG_NAME = "tel.png";
	private final static String FACEBOOK_IMG_NAME = "facebook.png";
	private final static String GPLUS_IMG_NAME = "gplus.png";
	private final static String TWITTER_IMG_NAME = "twitter.png";
	private final static String SPACER_IMG_NAME = "spacer.gif";
	
	private String imageLogo;
	private String imageLogo2;
	private String imageTelephone;
	private String imageFacebook;
	private String imageGplus;
	private String imageTwitter;
	private String imageSpacer;
	
	@PostConstruct
	protected void initModel() {
		
		initBaseModel(request);
		String prefix = "http://";
		if (configuration.getHttpsEnabled())
			prefix = "https://";
		String path = prefix + configuration.getExternalDomain() + configuration.getStaticImagesPath() + CheckinConstants.CHECKIN_MAIL_IMAGES_FOLDER;
		
		imageLogo = path + LOGO_NAME;
		imageLogo2 = path + LOGO2_NAME;
		imageTelephone = path + TELEPHONE_IMG_NAME;
		imageFacebook = path + FACEBOOK_IMG_NAME;
		imageGplus = path + GPLUS_IMG_NAME;
		imageTwitter = path + TWITTER_IMG_NAME;
		imageSpacer = path + SPACER_IMG_NAME;
	}

	public String getImageLogo() {
		return imageLogo;
	}

	public String getImageLogo2() {
		return imageLogo2;
	}

	public String getImageTelephone() {
		return imageTelephone;
	}

	public String getImageFacebook() {
		return imageFacebook;
	}

	public String getImageGplus() {
		return imageGplus;
	}

	public String getImageTwitter() {
		return imageTwitter;
	}

	public String getImageSpacer() {
		return imageSpacer;
	}

}


