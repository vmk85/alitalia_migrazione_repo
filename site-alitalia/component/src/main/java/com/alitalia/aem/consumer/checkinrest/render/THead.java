package com.alitalia.aem.consumer.checkinrest.render;

import java.util.ArrayList;

public class THead {
	
	private String column;
	private String typeCharacteristics;
	
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public String getTypeCharacteristics() {
		return typeCharacteristics;
	}
	public void setTypeCharacteristics(String typeCharacteristics) {
		this.typeCharacteristics = typeCharacteristics;
	}

}
