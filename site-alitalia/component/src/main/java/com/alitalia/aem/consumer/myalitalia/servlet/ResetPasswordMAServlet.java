package com.alitalia.aem.consumer.myalitalia.servlet;

import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.myalitalia.model.MyalitaliaOTPSessionModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;
import com.gigya.socialize.GSRequest;
import com.gigya.socialize.GSResponse;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import javax.inject.Inject;
import javax.servlet.Servlet;
import java.io.IOException;
import java.util.ArrayList;

@Component
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = { "resetPasswordMAServlet" }),
        @Property(name = "sling.servlet.methods", value = { "POST" }),
        @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) ,
        @Property(name = "sling.servlet.extensions", value = { "json" })
})
//@SuppressWarnings("serial")
public class ResetPasswordMAServlet extends GenericMyAlitaliaFormValidatorServlet {

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile AlitaliaCustomerProfileManager customerProfileManager;

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile ConsumerLoginDelegate consumerLoginDelegate;

    @Reference
    private AlitaliaConfigurationHolder configuration;


    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

        logger.debug("[ResetPasswordMAServlet] performSubmit");

        final I18n i18n = new I18n(request.getResourceBundle(
                AlitaliaUtils.findResourceLocale(request.getResource())));

        if(configuration==null)
            configuration = new AlitaliaConfigurationHolder();

        String apiMethod = "accounts.resetPassword";

        String apiKey = configuration.getMaApiKey(); // "3_FDF25EQ_XpOyo4xiZloa6QmF7QXYsb5r-rJaZj5ocvqW8B4bdCucFfqNdpaQPIec";
        String secretKey = configuration.getMaSecretKey();// "7XDjmCjYblwLmP+ejS79/RhqJPGapRbl";
        String userKey = configuration.getMaUserKey(); // "AI9W/RQHPi9r";

        String email = request.getParameter("email");
        String resetType = request.getParameter("resetType");
        String secretAnswer = request.getParameter("secretA");
        String newPassword = request.getParameter("newPassword");

        MyalitaliaOTPSessionModel otpSession = (request.getSession().getAttribute("OTPSession") != null ? (MyalitaliaOTPSessionModel) request.getSession().getAttribute("OTPSession") : new MyalitaliaOTPSessionModel());
        String isOTPVerified = (otpSession.isVerified() != null && otpSession.isVerified() ? "true" : "false");

        if(resetType.equals("OTP")) secretAnswer = configuration.getMADefaultSecretAnswer();

        GSRequest req = new GSRequest(apiKey, secretKey, apiMethod, null, true, userKey);
        GSResponse res;

        req.setAPIDomain("eu1.gigya.com");

        TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
        ArrayList<String> fields;

        json.object();


        response.setContentType("application/json");

        try {

            if(isOTPVerified != null && isOTPVerified.equals("true")) {

                logger.debug("[ResetPasswordMAServlet] l'OTP è stato recentemente verificato correttamente");

                if (resetType != null && email != null && email.length() > 0 && secretAnswer != null && secretAnswer.length() > 0 && newPassword != null && newPassword.length() > 0) {

                    logger.debug("[ResetPasswordMAServlet] setto i parametri per la chiamata");

                    req.setParam("loginID", email);
                    req.setParam("secretAnswer", secretAnswer);
                    req.setParam("newPassword", newPassword);
                    req.setParam("sendEmail", false);
                    res = req.send();
                    if (res.getErrorCode() == 0) {

                        logger.debug("[ResetPasswordMAServlet] chiamata effettuata");

                        json.key("isError").value(false);
                        json.key("errorCode").value(res.getErrorCode());
                        json.key("feedback").value(i18n.get("myalitalia.resetPassword.success"));

                        request.getSession().removeAttribute("isOTPVerified");

                    } else {

                        json.key("isError").value(true);
                        json.key("errorCode").value(res.getErrorCode());
                        json.key("errorMessage").value(res.getErrorMessage());
                        json.key("errorDetails").value(res.getErrorDetails());
                        json.key("feedback").value(i18n.get("myalitalia.resetPassword.fail"));

                        logger.error("[ResetPasswordMAServlet] errore nella chiamata a gigya " + json.toString());
                    }
                } else {
                    fields = new ArrayList<String>();
                    String joinFields = "";

                    if (resetType == null) fields.add("resetType");
                    if (email == null || email.length() <= 0) fields.add("email");
                    if (secretAnswer == null || secretAnswer.length() <= 0) fields.add("secretAnswer");
                    if (newPassword == null || newPassword.length() <= 0) fields.add("newPassword");

                    if (fields.size() > 0) joinFields = ArrayListToString(fields);

                    json.key("isError").value(true);
                    json.key("errorCode").value(400);
                    json.key("errorMessage").value("Bad Request");
                    json.key("errorDetails").value((joinFields.length() > 0 ? joinFields + " not setted" : ""));
                    json.key("feedback").value(i18n.get("myalitalia.resetPassword.fieldsNotSetted"));

                    logger.error("[ResetPasswordMAServlet] errore nella chiamata: " + (joinFields.length() > 0 ? joinFields + " non settati" : ""));

                }
            } else {

                json.key("isError").value(true);
                json.key("errorCode").value(500);
                json.key("errorMessage").value("error verifing OTP");
                json.key("errorDetails").value("nessun OTP verificato");
                json.key("feedback").value(i18n.get("myalitalia.resetPassword.genericError"));

                logger.error("[ResetPasswordMAServlet] nessun OTP recentemente verificato ");

            }

        } catch (Exception e) {

            json.key("isError").value(true);
            json.key("errorCode").value("500");
            json.key("errorDetails").value("Java Exception");
            json.key("errorMessage").value(e.toString());
            json.key("feedback").value(i18n.get("myalitalia.resetPassword.genericError"));

            logger.debug(e.toString());

        } finally {
            json.endObject();
        }

    }

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        logger.debug("[RecuperoTelefonoServlet] validateForm");
        Validator validator = new Validator();
        ResultValidation resultValidation = new ResultValidation();
        resultValidation.setResult(true);
        return resultValidation;
    }

    @Override
    protected void saveDataIntoSession(SlingHttpServletRequest request, Exception exception) {
        logger.debug("[RecuperoTelefonoServlet] saveDataIntoSession");

    }

    public String ArrayListToString(ArrayList<String> array) {
        String res = "";
        for(String current : array) {
            res += current + ", ";
        }
        return res;
    }


}



