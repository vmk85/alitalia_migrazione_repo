package com.alitalia.aem.consumer.carnet.model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.carnet.render.CarnetGenericPriceRender;
import com.alitalia.aem.consumer.checkin.CheckinSession;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CarnetRiepilogoSceltaModel extends GenericCarnetModel {

	@Inject
	AlitaliaConfigurationHolder configurationHolder;

	@Inject
	CheckinSession session;

	@Self
	private SlingHttpServletRequest request;

	private String email;
	private String codiceCarnet;
	private String totalAmount;
	private String riepilogoSceltaLabelAcquistato;
	private String riepilogoSceltaLabel;
	private String phase;
	
	@PostConstruct
	protected void initModel() {
		try {
			initBaseModel(request);
			email = ctx.confirmationMailAddress;
			phase = ctx.phase.value();
			
			if (ctx.infoCarnet != null) {
				codiceCarnet = ctx.infoCarnet.getCarnetCode();
			}
			
			CarnetGenericPriceRender priceRender = new CarnetGenericPriceRender(ctx);
			totalAmount = priceRender.getFormattedPrice(ctx.carnetTotalAmount);
			
			if (ctx.selectedCarnet != null) {
				riepilogoSceltaLabel = i18n.get("carnet.riepilogoScelta.label", "", ctx.selectedCarnet.getQuantity());
				riepilogoSceltaLabelAcquistato = i18n.get("carnet.riepilogoScelta.acquistato.label", "", ctx.selectedCarnet.getQuantity());
			}
			
			
		} catch(Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}
	}

	public String getEmail() {
		return email;
	}

	public String getCodiceCarnet() {
		return codiceCarnet;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public String getRiepilogoSceltaLabelAcquistato() {
		return riepilogoSceltaLabelAcquistato;
	}

	public String getRiepilogoSceltaLabel() {
		return riepilogoSceltaLabel;
	}

	public String getPhase() {
		return phase;
	}
	
}
