package com.alitalia.aem.consumer.millemiglia.model.richiedimiglia.exceptions;

public class NotCorrespondingSurnameException extends Exception {

	public NotCorrespondingSurnameException() {
		// TODO Auto-generated constructor stub
	}

	public NotCorrespondingSurnameException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public NotCorrespondingSurnameException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public NotCorrespondingSurnameException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public NotCorrespondingSurnameException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
