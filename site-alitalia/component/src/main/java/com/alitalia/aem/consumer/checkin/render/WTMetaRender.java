package com.alitalia.aem.consumer.checkin.render;

public class WTMetaRender {

	public String si_n = "";
	public String si_x = "";
	public String si_cs = "";
	public String cg_n = "";
	public String cg_s = "";
	public String wci_control = "";
	public String wci_passeggeroeleggibile = "";
	public String wci_postodisponibile = "";
	public String wci_classe = "";
	public String wck_v2_control = "";
	public String wci_newwebcheckin = "";
	public String wci_ristampa = "";
	public String wck_v2_riepilogoserviziapagamento = "";
	public String arsummary = "";
	public String pn_gr = "";
	public String pn_fa = "";
	public String pc = "";
	public String pn_sc = "";
	public String pn = "";
	public String wck_v2_riepilogoservizigratuiti = "";
	public String wck_v2_ricevutaserviziapagamento = "";
	public String wck_v2_quantitaemd = "";
	public String arproduct = "";
	public String arquantity = "";
	public String pn_sku = "";
	public String tx_curr = "";
	public String tx_it = "";
	public String tx_id = "";
	public String tx_i = "";
	public String tx_e = "";
	public String tx_s = "";
	public String tx_u = "";
	

}
