package com.alitalia.aem.consumer.crmdatarest;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.Servlet;


import com.alitalia.aem.common.data.crmdatarest.model.*;
import com.alitalia.aem.common.messages.crmdatarest.SetCrmDataInfoRequest;
import com.alitalia.aem.common.messages.crmdatarest.SetCrmDataInfoResponse;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.servlet.GenericBookingFormValidatorServlet;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.web.component.crmdata.delegaterest.ICRMDataDelegate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.crmdatarest.SetCRMDataNewsLetterRequest;
import com.alitalia.aem.common.messages.crmdatarest.SetCRMDataNewsLetterResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;

import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;


@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = { "newsletter-subscribe-crm", "newsletter-subscribe-crm-booking", "newsletter-unsubscribe-crm","newsletter-subscribe-crm-footer"}),
        @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
        @Property(name = "sling.servlet.extensions", value = { "json" }),
        @Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class SetCRMDataNewsLetterServlet extends GenericCheckinFormValidatorServlet {
    
    // logger
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Reference
    private AlitaliaConfigurationHolder configuration;
    
    @Reference
    private CheckinSession checkinSession;
    
    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile ICRMDataDelegate crmdataDelegate;
    
    @Override
    protected AlitaliaConfigurationHolder getConfiguration() {
        return configuration;
    }
    
    
    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        logger.debug("[NewsletterSubscribeServlet] validateForm");
    
        try {
            // validate parameters!
            Validator validator = setValidatorParameters(request,
                    new Validator());
            return validator.validate();
        }
        // an error occurred...
        catch (Exception e) {
            logger.error(
                    "Errore durante la procedura di iscrizione newsletter", e);
            return null;
        }
        
        
        /*ResultValidation resultValidation = new ResultValidation();
        resultValidation.setResult(true);
        return resultValidation;*/
    }
    
    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
    
        boolean resultSession = initSession(request, configuration, checkinSession);

        String UIDBooking = request.getParameter("UID");

        if (UIDBooking == null){
            UIDBooking = "";
        }
        
        String baseURL=request.getResource().getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), true));
        if (!resultSession){
            logger.error("Errore durante l'inizializzazione della sessione.");
            throw new Exception("Errore durante l'inizializzazione della sessione.");
        }else{
            CheckinSessionContext ctx = getCheckInSessionContext(request);
            logger.info("Entrati in [SetCRMDataNewsLetterServlet - performSubmit]");

            try {
                SetCRMDataNewsLetterResponse setCRMDataNewsLetterResponse = new SetCRMDataNewsLetterResponse();
                SetCRMDataNewsLetterRequest setCRMDataNewsLetterRequest = new SetCRMDataNewsLetterRequest(IDFactory.getTid(), IDFactory.getSid(request));

                SetCrmDataInfoResponse setCrmDataInfoResponse = new SetCrmDataInfoResponse();
                SetCrmDataInfoRequest setCrmDataInfoRequest = new SetCrmDataInfoRequest(IDFactory.getTid(), IDFactory.getSid(request));

                /*setCrmDataInfoRequest.setConversationID(ctx.conversationId);
                setCrmDataInfoRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));*/
                CanaleComunicazionePreferito canaleComunicazionePreferito = null;

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();

                String selector=request.getRequestPathInfo().getSelectorString();
                switch (selector){
                    case "newsletter-subscribe-crm":
                        setCRMDataNewsLetterRequest.setConversationID(ctx.conversationId);
                        setCRMDataNewsLetterRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                        setCRMDataNewsLetterRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                        setCRMDataNewsLetterRequest.setCanale("HP");
                        setCRMDataNewsLetterRequest.setEmail(request.getParameter("newsletter-email"));
                        setCRMDataNewsLetterRequest.setIdMyAlitalia("");
                        setCRMDataNewsLetterRequest.setUnsubscribeNewsletter(false);
                        setCRMDataNewsLetterRequest.setListaConsensi(setConsensi(true));
                        PreferenzePersonali preferenzePersonali = new PreferenzePersonali();
                        preferenzePersonali.setLinguaComunicazioniServizio(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                        preferenzePersonali.setLinguaComunicazioniCommerciali(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                        setCRMDataNewsLetterRequest.setPreferenzePersonali(preferenzePersonali);
                        logger.info("[SetCRMDataNewsLetterServlet] invocando crmdataDelegate.setCRMDataNewsLetter...");
                        List <CanaleComunicazionePreferito> lcanale = new ArrayList<>();
                        canaleComunicazionePreferito = new CanaleComunicazionePreferito() ;
                        canaleComunicazionePreferito.setSelezionato(true);
                        canaleComunicazionePreferito.setTipo("MAIL");
                        canaleComunicazionePreferito.setDataUltimoAggiornamento(dateFormat.format(date));
                        lcanale.add(canaleComunicazionePreferito);
                        setCRMDataNewsLetterRequest.setCanaleComunicazionePreferito(new ArrayList<>());
                        setCRMDataNewsLetterRequest.setCanaleComunicazionePreferito(lcanale);
                        setCRMDataNewsLetterResponse = crmdataDelegate.setCRMDataNewsLetter(setCRMDataNewsLetterRequest);
                        if (setCRMDataNewsLetterResponse != null) {
                            if (setCRMDataNewsLetterResponse.getResult().getCode().equals("0")) {
                                managementError(request, response, ctx.i18n.get("myalitalia.service.error"),baseURL);
                            } else {
                                //response.sendRedirect(successPage);
                                managementSucces(request, response, setCRMDataNewsLetterResponse,baseURL);
                            }
                        } else {
                            logger.error("[SetCRMDataNewsLetterServlet] - Errore durante l'invocazione del servizio.");
                        }
                        break;

                    case "newsletter-subscribe-crm-footer":
                        setCRMDataNewsLetterRequest.setConversationID(ctx.conversationId);
                        setCRMDataNewsLetterRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                        setCRMDataNewsLetterRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                        setCRMDataNewsLetterRequest.setCanale("FT");
                        setCRMDataNewsLetterRequest.setEmail(request.getParameter("newsletter-email"));
                        setCRMDataNewsLetterRequest.setIdMyAlitalia("");
                        setCRMDataNewsLetterRequest.setUnsubscribeNewsletter(false);
                        setCRMDataNewsLetterRequest.setListaConsensi(setConsensi(true));
                        PreferenzePersonali preferenzePersonali2 = new PreferenzePersonali();
                        preferenzePersonali2.setLinguaComunicazioniServizio(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                        preferenzePersonali2.setLinguaComunicazioniCommerciali(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                        setCRMDataNewsLetterRequest.setPreferenzePersonali(preferenzePersonali2);
                        logger.info("[SetCRMDataNewsLetterServlet] invocando crmdataDelegate.setCRMDataNewsLetter...");
                        List <CanaleComunicazionePreferito> lcanale2 = new ArrayList<>();
                        canaleComunicazionePreferito = new CanaleComunicazionePreferito() ;
                        canaleComunicazionePreferito.setSelezionato(true);
                        canaleComunicazionePreferito.setTipo("MAIL");
                        canaleComunicazionePreferito.setDataUltimoAggiornamento(dateFormat.format(date));
                        lcanale2.add(canaleComunicazionePreferito);
                        setCRMDataNewsLetterRequest.setCanaleComunicazionePreferito(new ArrayList<>());
                        setCRMDataNewsLetterRequest.setCanaleComunicazionePreferito(lcanale2);
                        setCRMDataNewsLetterResponse = crmdataDelegate.setCRMDataNewsLetter(setCRMDataNewsLetterRequest);
                        if (setCRMDataNewsLetterResponse != null) {
                            if (setCRMDataNewsLetterResponse.getResult().getCode().equals("0")) {
                                managementError(request, response, ctx.i18n.get("myalitalia.service.error"),baseURL);
                            } else {
                                //response.sendRedirect(successPage);
                                managementSucces(request, response, setCRMDataNewsLetterResponse,baseURL);
                            }
                        } else {
                            logger.error("[SetCRMDataNewsLetterServlet] - Errore durante l'invocazione del servizio.");
                        }
                        break;
                    case "newsletter-subscribe-crm-booking":
                        setCrmDataInfoRequest.setConversationID(ctx.conversationId);
                        setCrmDataInfoRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                        setCrmDataInfoRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                        InfoCliente infoCliente = new InfoCliente();
                        infoCliente.setNome(request.getParameter("nomeAdulto_1"));
                        infoCliente.setCognome(request.getParameter("cognomeAdulto_1"));
                        infoCliente.setSecondoNome(request.getParameter("secondoNomeAdulto_1"));
                        infoCliente.setSesso(request.getParameter("sessoAdulto_1"));

                        String dataNascita="";
                        String anno=request.getParameter("anno_dataNascitaAdulto_1");
                        String mese=request.getParameter("mese_dataNascitaAdulto_1");
                        String giorno=request.getParameter("giorno_dataNascitaAdulto_1");
                        if (anno!=null && mese!=null && giorno!=null){dataNascita=anno + "-" + mese + "-" + giorno;}
                        infoCliente.setDataNascita(dataNascita);

                        setCrmDataInfoRequest.setInfoCliente(infoCliente);

                        RecapitiTelefonici recapitiTelefonici = new RecapitiTelefonici();
                        recapitiTelefonici.setNumero(request.getParameter("valoreRecapito_1"));
                        String prefix=request.getParameter("prefix");
                        prefix=prefix.substring(prefix.indexOf("+")+1,prefix.indexOf(")"));
                        recapitiTelefonici.setPrefissioNazionale(prefix);
                        recapitiTelefonici.setTipo(request.getParameter("tipoRecapito_1"));
                        setCrmDataInfoRequest.setRecapitiTelefonici(recapitiTelefonici);

                        setCrmDataInfoRequest.setCanale("BOOKING");
                        setCrmDataInfoRequest.setEmail(request.getParameter("email"));
                        setCrmDataInfoRequest.setIdMyAlitalia("");
                        setCrmDataInfoRequest.setUnsubscribeNewsletter(false);
                        setCrmDataInfoRequest.setFrequenzaComunicazioni("1");
                        setCrmDataInfoRequest.setListaConsensi(setConsensi(true));

                        List<Documento> elencoDocumenti = new ArrayList<>();
                        Documento d = new Documento();
                        elencoDocumenti.add(d);

                        PreferenzePersonali preferenzePersonali1 = new PreferenzePersonali();
                        preferenzePersonali1.setLinguaComunicazioniCommerciali(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                        preferenzePersonali1.setLinguaComunicazioniServizio(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                        PreferenzeViaggio preferenzeViaggio = new PreferenzeViaggio();

                        canaleComunicazionePreferito = new CanaleComunicazionePreferito();
                        canaleComunicazionePreferito.setTipo("MAIL");
                        canaleComunicazionePreferito.setSelezionato(true);
                        canaleComunicazionePreferito.setDataUltimoAggiornamento(dateFormat.format(date));

                        setCrmDataInfoRequest.setElencoDocumenti(elencoDocumenti);
                        setCrmDataInfoRequest.setPreferenzePersonali(preferenzePersonali1);
                        setCrmDataInfoRequest.setPreferenzeViaggio(preferenzeViaggio);
                        List<CanaleComunicazionePreferito> canaleComunicazionePreferitos = new ArrayList<>();

                        canaleComunicazionePreferitos.add(canaleComunicazionePreferito);
                        setCrmDataInfoRequest.setCanaleComunicazionePreferito(new ArrayList<>());
                        setCrmDataInfoRequest.setCanaleComunicazionePreferito(canaleComunicazionePreferitos);
                        setCrmDataInfoRequest.setIdMyAlitalia(UIDBooking);

                        logger.info("[SetCRMDataNewsLetterServlet] invocando crmdataDelegate.setCRMDataNewsLetter...");
                        setCrmDataInfoResponse = crmdataDelegate.setAzCrmDataInfo(setCrmDataInfoRequest);
                        if (setCrmDataInfoResponse != null) {
                            if (setCrmDataInfoResponse.getResult().getCode().equals("0")) {
                                managementError(request, response, ctx.i18n.get("myalitalia.service.error"),baseURL);
                            } else {
                                //response.sendRedirect(successPage);
                                managementSucces(request, response, setCrmDataInfoResponse,baseURL);
                            }
                        } else {
                            logger.error("[SetCRMDataNewsLetterServlet] - Errore durante l'invocazione del servizio.");
                        }
                        break;
                    case "newsletter-unsubscribe-crm":
                        setCRMDataNewsLetterRequest.setConversationID(ctx.conversationId);
                        setCRMDataNewsLetterRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
                        setCRMDataNewsLetterRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
                        setCRMDataNewsLetterRequest.setCanale("NEWSLETTER");
                        setCRMDataNewsLetterRequest.setEmail(request.getParameter("email"));
                        setCRMDataNewsLetterRequest.setIdMyAlitalia("");
                        setCRMDataNewsLetterRequest.setUnsubscribeNewsletter(true);
                        setCRMDataNewsLetterRequest.setListaConsensi(setConsensi(false));
                        setCRMDataNewsLetterRequest.setPreferenzePersonali(new PreferenzePersonali());
                        setCRMDataNewsLetterRequest.setCanaleComunicazionePreferito(new ArrayList<>());
                        logger.info("[SetCRMDataNewsLetterServlet] invocando crmdataDelegate.setCRMDataNewsLetter...");
                        setCRMDataNewsLetterResponse = crmdataDelegate.setCRMDataNewsLetter(setCRMDataNewsLetterRequest);
                        if (setCRMDataNewsLetterResponse != null) {
                            if (setCRMDataNewsLetterResponse.getResult().getCode().equals("0")) {
                                managementError(request, response, ctx.i18n.get("myalitalia.service.error"),baseURL);
                            } else {
                                //response.sendRedirect(successPage);
                                managementSucces(request, response, setCRMDataNewsLetterResponse,baseURL);
                            }
                        } else {
                            logger.error("[SetCRMDataNewsLetterServlet] - Errore durante l'invocazione del servizio.");
                        }
                        break;
                }
            } catch (Exception e) {
                throw new RuntimeException(ctx.i18n.get("myalitalia.service.error"), e);
            }
        }
    }
    
    private List<Consenso> setConsensi(boolean consensoValue) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        List<Consenso> listaConsensi = new ArrayList<>();
        Consenso c1;
//        c2,c3 ;
        c1= new Consenso();
//        c2= new Consenso();
//        c3= new Consenso();
        
        c1.setFlagConsenso(consensoValue);
        /*c2.setFlagConsenso(consensoValue);
        c3.setFlagConsenso(consensoValue);*/
//        c2.setFlagConsenso(false);
//        c3.setFlagConsenso(false);
        
        c1.setTipoConsenso("COM");
//        c2.setTipoConsenso("PRN");
//        c3.setTipoConsenso("PRO");
        
        c1.setDataUltimoAggiornamento(dateFormat.format(date));
//        c2.setDataUltimoAggiornamento(dateFormat.format(date));
//        c3.setDataUltimoAggiornamento(dateFormat.format(date));
        
        listaConsensi.add(c1);
//        listaConsensi.add(c2);
//        listaConsensi.add(c3);
        
        return  listaConsensi;
    }
    
    /**
     * Set Validator parameters for validation
     *
     * @param request
     * @param validator
     */
    private Validator setValidatorParameters(SlingHttpServletRequest request,
                                             Validator validator) {
        
        String selector=request.getRequestPathInfo().getSelectorString();
        switch (selector){
            case "newsletter-subscribe-crm":
    
                // get parameters from request
                String email = request.getParameter("newsletter-email");
                String newsletterCond = request.getParameter("checkbox1");
    
                // add validate conditions
                validator.addDirectConditionMessagePattern("email", email,
                        I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
                        I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID, "isNotEmpty");
    
                //validator.addDirectCondition("receivenewsletter", newsletterCond,
                //I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "isNotEmpty");
                validator.addCrossCondition("receivenewsletter", newsletterCond, "accepted",
                        I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "areEqual");
    
                validator.addDirectConditionMessagePattern("email",email ,I18nKeyCommon.MESSAGE_INVALID_FIELD,
                        I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID, "isEmail");
                break;
            case "newsletter-subscribe-crm-booking":
        
        
                break;
            case "newsletter-unsubscribe-crm":

                break;
        }
        
        return validator;
    }
    
    private void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error, String baseURL) {
        
        logger.info("[SetCRMDataNewsLetterServlet] [managementError] error =" + error);
        
        Gson gson = new Gson();
        
        //todo : sostituire con analogo per crmdatainfo
        //com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);
        
        TidyJSONWriter json;
        try {
            json = new TidyJSONWriter(response.getWriter());
            response.setContentType("application/json");
            
            json.object();
            json.key("isError").value(true);
            json.key("errorMessage").value(error);
            json.key("baseURL").value(baseURL);
            json.endObject();
            
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    private void managementSucces(
            SlingHttpServletRequest request,
            SlingHttpServletResponse response,
            SetCRMDataNewsLetterResponse setCRMDataNewsLetterResponse,
            String baseURL) {
        
        logger.info("[SetCRMDataNewsLetterServlet] [managementSucces] ...");
        
        try {
            TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
            response.setContentType("application/json");
            
            json.object();
            json.key("isError").value(false);
            
            //json.key("result").value(setCRMDataNewsLetterResponse.getResult());
            json.key("result");
            json.object();
            json.key("code").value(setCRMDataNewsLetterResponse.getResult().getCode());
            json.key("description").value(setCRMDataNewsLetterResponse.getResult().getDescription());
            json.key("status").value(setCRMDataNewsLetterResponse.getResult().getStatus());
            //json.key("errorSourceDetails").value(setCRMDataNewsLetterResponse.getResult().getErrorSourceDetails());

            json.key("errorSourceDetails");
            json.array();
            List<ErrorSourceDetail> errorSourceDetail = new ArrayList<ErrorSourceDetail>();
            errorSourceDetail=setCRMDataNewsLetterResponse.getResult().getErrorSourceDetails();
            for (int j = 0; j < errorSourceDetail.size(); j++) {
                json.object();
                json.key("details").value(errorSourceDetail.get(j).getDetails());
                json.key("source").value(errorSourceDetail.get(j).getSource());;
                json.endObject();
            }
            json.endArray();
            json.endObject();

            //json.key("error").value(setCRMDataNewsLetterResponse.getError());
            json.key("baseURL").value(baseURL);
            json.key("conversationID").value(setCRMDataNewsLetterResponse.getConversationID());
    
            json.endObject();



        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    private void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response, SetCrmDataInfoResponse setCrmDataInfoResponse, String baseURL) {
        
        logger.info("[SetCRMDataNewsLetterServlet] [managementSucces] ...");

        try {
            TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
            response.setContentType("application/json");

            json.object();
            json.key("isError").value(false);

            //json.key("result").value(setCRMDataNewsLetterResponse.getResult());
            json.key("result");
            json.object();
            json.key("code").value(setCrmDataInfoResponse.getResult().getCode());
            json.key("description").value(setCrmDataInfoResponse.getResult().getDescription());
            json.key("status").value(setCrmDataInfoResponse.getResult().getStatus());
            //json.key("errorSourceDetails").value(setCRMDataNewsLetterResponse.getResult().getErrorSourceDetails());

            json.key("errorSourceDetails");
            json.array();
            List<ErrorSourceDetail> errorSourceDetail = new ArrayList<ErrorSourceDetail>();
            errorSourceDetail=setCrmDataInfoResponse.getResult().getErrorSourceDetails();
            for (int j = 0; j < errorSourceDetail.size(); j++) {
                json.object();
                json.key("details").value(errorSourceDetail.get(j).getDetails());
                json.key("source").value(errorSourceDetail.get(j).getSource());;
                json.endObject();
            }
            json.endArray();
            json.endObject();

            json.key("baseURL").value(baseURL);
            json.key("conversationID").value(setCrmDataInfoResponse.getConversationID());

            json.endObject();



        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

