package com.alitalia.aem.consumer.model.content;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables={Resource.class})
public class GetPagePathModel {

    @Self
    private Resource resource;

    private Logger logger = LoggerFactory.getLogger(GetPagePathModel.class);
    
    private String pagePath;

    @PostConstruct
    protected void initModel() {
    	logger.debug("<<<Executing initModel of GetPagePathModel>>>");
            try {
                pagePath = resource.getPath();
                //elimino la parte finale del path che contiene la stringa /jcr:content
                pagePath = pagePath.substring(0, pagePath.length() - 12);
            } catch(Exception e) {
                logger.error("Unable to get the page path: " + e);
            }
    }

    /**
     * Restituisce il path della pagina
     */
    public String getPagePath() {
        return pagePath;
    }

}

