package com.alitalia.aem.consumer.model.content.specialpages;

public class MillemigliaLoginData {
	public static final String NAME = "MillemigliaLoginData";
	
	private String code;
	private String pin;
	private String sms_check;
	private String error;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getSmsCheck() {
		return sms_check;
	}
	
	public void setSmsCheck(String sms_check) {
		this.sms_check = sms_check;
	}

	

	
	@Override
	public String toString() {
		return "RecuperaPinData [code=" + code + ", pin=" + pin + ", smsCheck =" + sms_check + "]";
	}
}
