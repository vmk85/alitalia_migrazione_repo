package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.SlingRequestProcessor;

import com.alitalia.aem.common.messages.home.RetrievePinRequest;
import com.alitalia.aem.common.messages.home.RetrievePinResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.RecuperaPinData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.WCMMode;

@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "recuperapinsubmit" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")

public class RecuperaPinServlet extends GenericFormValidatorServlet {
	
	private static final String ERROR = "Pin Not Found";
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BusinessLoginService businessLoginService;
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private ResourceResolverFactory resolverFactory;

    /** Service to create HTTP Servlet requests and responses */
    @Reference
    private RequestResponseFactory requestResponseFactory;

    /** Service to process requests through Sling */
    @Reference
    private SlingRequestProcessor requestProcessor;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		logger.debug("[RecuperaPinServlet] validateForm");

		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			
			return validator.validate();

		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Error during parameters validation", e);
			return null;
		}

	}

	@SuppressWarnings("unused")
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		logger.debug("[RecuperaPinServlet] performSubmit");
		RetrievePinRequest retrievePinRequest = new RetrievePinRequest();
		retrievePinRequest.setTid(IDFactory.getTid());
		retrievePinRequest.setSid(IDFactory.getSid(request));
		retrievePinRequest.setCustomerNumber((String) request.getParameter("code"));
		retrievePinRequest.setCustomerLastName(request.getParameter("surname"));
		retrievePinRequest.setMailSender(configuration.getMailNoreplyAlitalia());
		retrievePinRequest.setMailSubject("Retrieve PIN");
		RetrievePinResponse retrievePinResponse = null;
		String pin = null;
		String mailTo = null;
		
		try {
			String mailBody = getMailBody(request.getResource(), configuration.getBodyMailRetrievePin(), false );

			if(mailBody == null || (mailBody != null && mailBody.equals("")) ){
				throw new IOException(ERROR);
			}
			retrievePinRequest.setMailBody(mailBody);

			retrievePinResponse = consumerLoginDelegate.retrievePinByCustomerNumber(retrievePinRequest);
			RecuperaPinData recuperaPinData = getData(request, null);
			request.getSession().setAttribute(RecuperaPinData.NAME, recuperaPinData);

			if (retrievePinResponse.isRetrieved() && retrievePinResponse != null && retrievePinResponse.getCustomerProfile() != null) {
				String customerNumber = retrievePinResponse.getCustomerProfile().getCustomerNumber();
				logger.info("[RecuperaPinServlet] Success: retrieved PIN by Customer Number: " + customerNumber);
				pin = retrievePinResponse.getCustomerProfile().getCustomerPinCode();
				mailTo = retrievePinResponse.getCustomerProfile().getEmail();
			} else {
				retrievePinRequest.setCustomerNickName(request.getParameter("code"));
				retrievePinResponse = consumerLoginDelegate.retrievePinByNickname(retrievePinRequest);
				recuperaPinData = getData(request, null);
				request.getSession().setAttribute(RecuperaPinData.NAME, recuperaPinData);
				if (retrievePinResponse.isRetrieved() && retrievePinResponse != null && retrievePinResponse.getCustomerProfile() != null) {
					logger.debug("[RecuperaPinServlet] Retrieved PIN by Nickname");
					pin = retrievePinResponse.getCustomerProfile().getCustomerPinCode();
				} else {
					logger.error("[RecuperaPinServlet] ERRORE RETRIEVE PIN: " + retrievePinResponse.getErrorMessage() + ", " + retrievePinResponse.getReturnCode());
					throw new IOException(retrievePinResponse.getErrorMessage());
				}
			}
			if (retrievePinResponse.isRetrieved() && !retrievePinResponse.isMailSent()) {
				logger.error("[RecuperaPinServlet] ERRORE INVIO EMAIL PIN");
				throw new IOException(ERROR);
			}

			goBackSuccessfully(request, response);
		}
		catch (Exception e) {
			logger.error("[RecuperaPinServlet] Exception during retrieve pin: " + e.getMessage(), e);
			throw new IOException(e.getMessage());
		}
	}

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
			Exception e) {
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		logger.debug("[RecuperaPinServlet] saveDataIntoSession.");
		logger.debug("[RecuperaPinServlet] EXCEPTION saveDataIntoSession: " + e.getMessage());
		String errorCustomNotFound = configuration.getCustomerNotFoundRetrievePin();
		logger.debug("[RecuperaPinServlet] CUSTOMER NOT FOUND ERROR: " + errorCustomNotFound);
		if (e.getMessage().contains(errorCustomNotFound)) {
			logger.debug("[RecuperaPinServlet] ERROR DETECTED: customer error not found: " + errorCustomNotFound);
			RecuperaPinData recuperaPinData = getData(request, i18n.get(I18nKeySpecialPage.PIR_ERROR_NOT_VALID));
			request.getSession().setAttribute(RecuperaPinData.NAME, recuperaPinData);
		}
		else {
			logger.debug("[RecuperaPinServlet] ERROR DETECTED: generic error : " + errorCustomNotFound);
			RecuperaPinData recuperaPinData = getData(request, I18nKeySpecialPage.ERROR_SERVICE);
			request.getSession().setAttribute(RecuperaPinData.NAME, recuperaPinData);
		}
	}

	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request,
			Validator validator) {

		// get parameters from request
		String code = request.getParameter("code");
		String surname = request.getParameter("surname");

		// add validate conditions
		validator.addDirectConditionMessagePattern("code", code,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "isNotEmpty");
		/*validator.addDirectConditionMessagePattern("code", code,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "isNumber");*/

		/* AMS Bugfix #4835. Recupero password fallisce con codice MM da 10 char perché il campo ne supporta 8 */
		validator.addCrossConditionMessagePattern("code", code, "11",
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "sizeIsLess");
		
		validator.addDirectConditionMessagePattern("surname", surname,
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
				I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
		validator.addDirectConditionMessagePattern("surname", surname,
				I18nKeyCommon.MESSAGE_INVALID_FIELD,
				I18nKeySpecialPage.EMAIL_ERROR_NOT_VALID, "isAlphabeticWithAccentAndSpaces");

		return validator;

	}
	
	private RecuperaPinData getData(SlingHttpServletRequest request, String error) {
		RecuperaPinData recuperaPinData = new RecuperaPinData();
		recuperaPinData.setSurname(request.getParameter("surname"));
		recuperaPinData.setCode(request.getParameter("code"));
		recuperaPinData.setError(error);
		return recuperaPinData;
	}

	public String getMailBody(Resource resource, String templateMail, boolean secure) {
        
		String result = null;
		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(resource, false);
		try {

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			/* Get resource resolver */
			@SuppressWarnings("deprecation")
			ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			String requestedUrl = baseUrl + AlitaliaConstants.BASE_CONFIG_PATH + "/" + templateMail;

			/* Setup request */
			HttpServletRequest req = requestResponseFactory.createRequest("GET", requestedUrl);
			WCMMode.DISABLED.toRequest(req);

			out = new ByteArrayOutputStream();
			HttpServletResponse resp = requestResponseFactory.createResponse(out);

			/* Process request through Sling */
			requestProcessor.processRequest(req, resp, resourceResolver);

			String encoding = "UTF-8";

			result = new String(out.toByteArray(), Charset.forName(encoding));

		} catch (LoginException | ServletException | IOException e) {
			logger.error("Generic exception while retrieving email template from JCR repository: {}", e);
			result = "";
		}
		if (logger.isDebugEnabled())
			logger.debug("Template retrieved and APIS/ESTA placeholders replaced: {} ", result);
      return result;
	}
	
}
