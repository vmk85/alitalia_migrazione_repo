package com.alitalia.aem.consumer.bookingaward.controller;

import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingAwardFormModificaController extends BookingAwardGenericController{
	
	@Self
	private SlingHttpServletRequest request;
	
	private String departureAirport;
	private String arrivalAirport;
	private String departureCity;
	private String arrivalCity;
	private String outboundDate;
	private String returnDate;
	private String cug;
	private boolean isRoundtrip;
	private int numYoungs;
	private int numInfants;
	private int numChildren;
	private int numAdults;
	private String filterCabin;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			
			initBaseModel(request);
			
			if (ctx != null) {
				
				//obtain dates
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				outboundDate = sdf.format(ctx.searchElements.get(0)
						.getDepartureDate().getTime());
				if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP) {
					returnDate =  sdf.format(ctx.searchElements.get(1)
							.getDepartureDate().getTime());
				}
				logger.debug("[MODIFICA RICERCA] OUTBOUND DATE = " + outboundDate);
				
				//obtain airports
				departureAirport = ctx.searchElements.get(0).getFrom().getAirportCode();
				departureCity = i18n.get("airportsData." + departureAirport + ".city");
				arrivalAirport = ctx.searchElements.get(0).getTo().getAirportCode();
				arrivalCity = i18n.get("airportsData." + arrivalAirport + ".city");
				logger.debug("[MODIFICA RICERCA] ARRIVAL CITY = " + arrivalCity);
				//obtain searchKind
				isRoundtrip = ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP || ctx.showRoundTrip;
				logger.debug("[MODIFICA RICERCA] ROUND TRIP = " + isRoundtrip);
				
				//obtain passengers
				numAdults = ctx.searchPassengersNumber.getNumAdults();
				numChildren = ctx.searchPassengersNumber.getNumChildren();
				numInfants = ctx.searchPassengersNumber.getNumInfants();
				numYoungs = ctx.searchPassengersNumber.getNumYoung();
				logger.debug("[MODIFICA RICERCA] ADULTI = " + numAdults);
				
				if(ctx.filterCabin != null){
					filterCabin = ctx.filterCabin.value();
				}
			} else {
				//in case of error the form will be compiled with empty field
				setDefaultConfiguration();
			}
			
		}catch(Exception e){
			logger.error("Unexpected error", e);
			
			//in case of error the form will be compiled with empty field
			setDefaultConfiguration();
		}
	}

	/**
	 * It sets the default configuration for the form modifica
	 * pre-compiling empty field
	 */
	private void setDefaultConfiguration() {
		this.departureAirport = "";
		this.arrivalAirport = "";
		this.departureCity = "";
		this.arrivalCity = "";
		this.outboundDate = "";
		this.returnDate = "";
		this.cug = "ADT";
		this.isRoundtrip = true;
		this.numYoungs = 0;
		this.numInfants = 0;
		this.numChildren = 0;
		this.numAdults = 0;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}
	
	public String getDepartureCity() {
		return departureCity;
	}

	public String getArrivalCity() {
		return arrivalCity;
	}

	public String getOutboundDate() {
		return outboundDate;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public String getCug() {
		return cug;
	}
	
	public boolean isRoundtrip() {
		return isRoundtrip;
	}

	public String getNumYoungs() {
		return Integer.toString(numYoungs);
	}

	public String getNumInfants() {
		return Integer.toString(numInfants);
	}

	public String getNumChildren() {
		return Integer.toString(numChildren);
	}

	public String getNumAdults() {
		return Integer.toString(numAdults);
	}
	
	public String getFilterCabin(){
		return this.filterCabin;
	}
}
