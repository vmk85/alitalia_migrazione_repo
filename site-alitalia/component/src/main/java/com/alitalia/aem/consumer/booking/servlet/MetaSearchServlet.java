package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.ApplicantPassengerData;
import com.alitalia.aem.common.data.home.ChildPassengerData;
import com.alitalia.aem.common.data.home.InfantPassengerData;
import com.alitalia.aem.common.data.home.MetaSearchData;
import com.alitalia.aem.common.data.home.MetaSearchDestinationData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PassengerNumbersData;
import com.alitalia.aem.common.data.home.SearchDestinationData;
import com.alitalia.aem.common.data.home.enumerations.ARTaxInfoTypesEnum;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.SearchTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.TimeTypeEnum;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.model.SearchElementAirport;
import com.alitalia.aem.consumer.booking.model.SearchPassengersNumber;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.web.component.searchflights.delegate.SearchFlightsDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.selectors", value = { "metasearch" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class MetaSearchServlet extends GenericBookingFormValidatorServlet {

	// StaticData delegate
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegate staticDataDelegate;

	// SearchFlight delegate
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile SearchFlightsDelegate searchFlightsDelegate;

	@Reference
	private BookingSession bookingSession;

	@Reference
	private AlitaliaConfigurationHolder alitaliaConfiguration;

	private List<AirportData> airportDataList;

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

		String site = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
		String languageCode = AlitaliaUtils.getLanguage(locale.toLanguageTag());
		String marketCode = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(request.getResource());
		String decimalSeparator = ivm.getInherited("decimalSeparator", String.class);
		String groupingSeparator = ivm.getInherited("groupingSeparator", String.class);
		Integer numDecDigits = ivm.getInherited("decimalDigit", Integer.class);
		String currency = ivm.getInherited("currencyCode", String.class);
		if (numDecDigits == null) {
			numDecDigits = 2;
		}
		NumberFormat currentNumberFormat = AlitaliaUtils.getNumberFormat(numDecDigits, decimalSeparator,
				groupingSeparator, marketCode);

		final I18n i18n = new I18n(request.getResourceBundle(locale));

		/* Inizializzazione Booking Session */
		HttpSession sessionCTX = request.getSession(true);
		
		
		//INIZIO *** MODIFICA IP ***	
		String clientIP = "";
		clientIP = AlitaliaUtils.getRequestRemoteAddr(alitaliaConfiguration, request);
		//FINE.
		
		
		String sessionId = sessionCTX.getId();
		BookingSessionContext ctx = bookingSession.initializeBookingSession("alitaliaConsumerBooking", clientIP,
				sessionId);
		
		//INIZIO *** MODIFICA IP ***
		if (clientIP != null && !clientIP.isEmpty()) {
			ctx.ipAddress = clientIP;
		}
		//FINE.
		
		sessionCTX.setAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE, ctx);

		/* Recupero elenco degli areoporti */
		try {
			RetrieveAirportsRequest serviceRequest = new RetrieveAirportsRequest(IDFactory.getTid(),
					IDFactory.getSid(request));
			serviceRequest.setMarket(marketCode);
			serviceRequest.setLanguageCode(languageCode);
			RetrieveAirportsResponse serviceResponse = staticDataDelegate.retrieveAirports(serviceRequest);
			airportDataList = serviceResponse.getAirports();
		} catch (Exception e) {
			logger.error("ERRORE durante il caricamento degli aeroporti", e);
		}

		/* Recupero dati dalla request */
		ArrayList<SearchDestinationData> metaSearchDestinationList = new ArrayList<SearchDestinationData>();
		ctx.searchKind = BookingSearchKindEnum.SIMPLE;
		int i = 0;
		String from = "";
		String to = "";
		do {
			MetaSearchDestinationData metaSearchDestinationData;
			from = request.getParameter("MetaSearchDestinations[" + i + "].From");
			to = request.getParameter("MetaSearchDestinations[" + i + "].To");

			if (from != null && !from.equals("")) {
				metaSearchDestinationData = new MetaSearchDestinationData();
				metaSearchDestinationData.setFromAirport(getAirportDataFromCode(from));
				metaSearchDestinationData.setToAirport(getAirportDataFromCode(to));

				metaSearchDestinationData.setCode(request.getParameter("MetaSearchDestinations[" + i + "].code"));
				metaSearchDestinationData
						.setFlight(Integer.parseInt(request.getParameter("MetaSearchDestinations[" + i + "].Flight")));
				metaSearchDestinationData
						.setSlices(Integer.parseInt(request.getParameter("MetaSearchDestinations[" + i + "].slices")));

				if (metaSearchDestinationData.getSlices() == 1) {
					ctx.searchKind = BookingSearchKindEnum.ROUNDTRIP;
				}

				metaSearchDestinationData.setRouteType(i == 0 ? RouteTypeEnum.OUTBOUND : RouteTypeEnum.RETURN);
				metaSearchDestinationData.setTimeType(TimeTypeEnum.ANYTIME);

				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd:HH:mm");
				dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
				Calendar departureDate = Calendar.getInstance();
				departureDate.setTimeZone(TimeZone.getTimeZone("UTC"));
				departureDate.setTime(
						dateFormat.parse(request.getParameter("MetaSearchDestinations[" + i + "].DepartureDate")));
				metaSearchDestinationData.setDepartureDate(departureDate);
				metaSearchDestinationList.add(metaSearchDestinationData);

				if (request.getParameter("MetaSearchDestinations[" + i + "].ArrivalDate") != null
						&& !request.getParameter("MetaSearchDestinations[" + i + "].ArrivalDate").isEmpty()) {
					Calendar arrivalDate = Calendar.getInstance();
					arrivalDate.setTimeZone(TimeZone.getTimeZone("UTC"));
					arrivalDate.setTime(
							dateFormat.parse(request.getParameter("MetaSearchDestinations[" + i + "].ArrivalDate")));
					metaSearchDestinationData.setArrivalDate(arrivalDate);
				}
				metaSearchDestinationData
						.setBookingClass(request.getParameter("MetaSearchDestinations[" + i + "].bookingClass"));
				metaSearchDestinationData.setCabinClass(
						CabinEnum.fromValue(request.getParameter("MetaSearchDestinations[" + i + "].cabinClass")));
				i++;
			} else {
				break;
			}
		} while (from != null && !from.equals(""));

		
		/* Impostazione oggetti da passare al servizio */
		MetaSearchData metaSearchData = new MetaSearchData();
		metaSearchData.setDestinations(metaSearchDestinationList);
		metaSearchData.setMarket(marketCode.toUpperCase());
		metaSearchData.setType(SearchTypeEnum.META_SEARCH);
		metaSearchData.setCug("ADT");

		/* Imposto MSE Type */
		String mseType = request.getParameter("MetaSearchDestinations[0].MseType");
		if (mseType != null && !"".equals(mseType)) {
			metaSearchData.setMseType(mseType);
		}

		PassengerNumbersData passengerNumberData;
		ArrayList<PassengerNumbersData> passengerNumberDataList = new ArrayList<PassengerNumbersData>();

		passengerNumberData = new PassengerNumbersData();
		passengerNumberData.setNumber(Integer.parseInt(request.getParameter("adult_number")));
		passengerNumberData.setPassengerType(PassengerTypeEnum.ADULT);
		passengerNumberDataList.add(passengerNumberData);

		passengerNumberData = new PassengerNumbersData();
		passengerNumberData.setNumber(Integer.parseInt(request.getParameter("children_number")));
		passengerNumberData.setPassengerType(PassengerTypeEnum.CHILD);
		passengerNumberDataList.add(passengerNumberData);

		passengerNumberData = new PassengerNumbersData();
		passengerNumberData.setNumber(Integer.parseInt(request.getParameter("newborn_number")));
		passengerNumberData.setPassengerType(PassengerTypeEnum.INFANT);
		passengerNumberDataList.add(passengerNumberData);

		metaSearchData.setPassengerNumbers(passengerNumberDataList);

		List<PassengerBaseData> passengerBaseDataList = computePassengersDataList(passengerNumberDataList);
		metaSearchData.setPassengers(passengerBaseDataList);

		/* PrepareSearch */
		BookingSearchCUGEnum cug = metaSearchData.getCug() == null ? null
				: BookingSearchCUGEnum.fromValue(metaSearchData.getCug());

		SearchPassengersNumber searchPassengersNumber = new SearchPassengersNumber();
		for (PassengerNumbersData passengerNumbersData : metaSearchData.getPassengerNumbers()) {
			if (passengerNumbersData.getPassengerType().equals(PassengerTypeEnum.ADULT)
					|| passengerNumbersData.getPassengerType().equals(PassengerTypeEnum.APPLICANT)
					|| passengerNumbersData.getPassengerType().equals(PassengerTypeEnum.NORMAL)) {
				searchPassengersNumber.setNumAdults(passengerNumbersData.getNumber());
			} else if (passengerNumbersData.getPassengerType().equals(PassengerTypeEnum.YOUTH)) {
				searchPassengersNumber.setNumYoung(passengerNumbersData.getNumber());
			} else if (passengerNumbersData.getPassengerType().equals(PassengerTypeEnum.CHILD)) {
				searchPassengersNumber.setNumChildren(passengerNumbersData.getNumber());
			} else if (passengerNumbersData.getPassengerType().equals(PassengerTypeEnum.INFANT)) {
				searchPassengersNumber.setNumInfants(passengerNumbersData.getNumber());
			}
		}

		SearchElement searchElementOutbound = null;
		SearchElement searchElementRoundTrip = null;
		List<SearchElement> searchElementList = new ArrayList<SearchElement>();
		String fromAirportCodeOutbound = null;
		String toAirportCodeOutbound = null;
		Calendar departureDateOutbound = null;

		String fromAirportCodeRoundTrip = null;
		String toAirportCodeRoundTrip = null;
		Calendar departureDateRoundTrip = null;

		for (SearchDestinationData searchDestinationData : metaSearchData.getDestinations()) {
			MetaSearchDestinationData metaSearchDestinationData = (MetaSearchDestinationData) searchDestinationData;

			if (metaSearchDestinationData.getSlices() == 0) {
				if (fromAirportCodeOutbound == null) {
					fromAirportCodeOutbound = metaSearchDestinationData.getFromAirport().getCode();
				}
				if (departureDateOutbound == null) {
					departureDateOutbound = metaSearchDestinationData.getDepartureDate();
				}
				toAirportCodeOutbound = metaSearchDestinationData.getToAirport().getCode();
			}

			if (metaSearchDestinationData.getSlices() == 1) {
				if (fromAirportCodeRoundTrip == null) {
					fromAirportCodeRoundTrip = metaSearchDestinationData.getFromAirport().getCode();
				}
				if (departureDateRoundTrip == null) {
					departureDateRoundTrip = metaSearchDestinationData.getDepartureDate();
				}
				toAirportCodeRoundTrip = metaSearchDestinationData.getToAirport().getCode();
			}
		}

		searchElementOutbound = new SearchElement(new SearchElementAirport(fromAirportCodeOutbound),
				new SearchElementAirport(toAirportCodeOutbound), departureDateOutbound);

		if (fromAirportCodeRoundTrip != null && toAirportCodeRoundTrip != null && departureDateRoundTrip != null) {
			searchElementRoundTrip = new SearchElement(new SearchElementAirport(fromAirportCodeRoundTrip),
					new SearchElementAirport(toAirportCodeRoundTrip), departureDateRoundTrip);
		}

		searchElementList.add(searchElementOutbound);
		if (searchElementRoundTrip != null) {
			searchElementList.add(searchElementRoundTrip);
		}

		bookingSession.prepareSearch(ctx, i18n, ctx.searchKind, searchElementList, searchPassengersNumber, cug,
				marketCode, site, locale, currentNumberFormat, currency, "", "", CabinEnum.ECONOMY, false, false);

		ctx.metaSearchData = metaSearchData;
		
		//********************
		//MODIFICA DEL 03/05/2017
		//X aggiunta gestione risposta Kayak
		//INIZIO
		//********************
		request.getSession(true).removeAttribute("kayakclickid");
		//metto questa parte quì apposta dopo il bookingSession.prepareSearch
		//perchè prevedo di mettere li la parte che farà request.getSession(true).removeAttribute("kayakclickid");
		//per pulire la variabile di sessione (oltre che nel model del confirmation page)
		if (request.getParameter("kayakclickid") != null && !request.getParameter("kayakclickid").isEmpty()) {
			request.getSession(true).setAttribute("kayakclickid", request.getParameter("kayakclickid"));
			//request.getSession(true).getAttribute("kayakclickid");
			//request.getSession(true).removeAttribute("kayakclickid");
		}
		//********************
		//MODIFICA DEL 03/05/2017
		//X aggiunta gestione risposta Kayak
		//FINE
		//********************
		
		String redirect = alitaliaConfiguration.getBookingMetasearchPage();
		if (redirect != null && !"".equals(redirect)) {
			Resource resource = request.getResource();
			String protocol = "http://";
			if (alitaliaConfiguration.getHttpsEnabled()) {
				protocol = "https://";
			}
			String redirectUrl = protocol + alitaliaConfiguration.getBookingExternalDomain()
					+ resource.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(resource, false))
					+ redirect;
			logger.debug("redirectUrl: " + redirectUrl);
			response.sendRedirect(redirectUrl);
		} else {
			logger.error("Configuration page redirect metasearch: {} not found", redirect);
			String redirectUrl = request.getResource().getResourceResolver()
					.map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false))
					+ alitaliaConfiguration.getBookingFailurePage();
			response.sendRedirect(redirectUrl);
		}

	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {

		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);

		return resultValidation;
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return alitaliaConfiguration;
	}

	private AirportData getAirportDataFromCode(String aptCode) {
		if (airportDataList != null) {
			for (AirportData airportData : airportDataList) {
				if (airportData.getCode().equals(aptCode)) {
					return airportData;
				}
			}
		}
		return null;
	}

	private List<PassengerBaseData> computePassengersDataList(ArrayList<PassengerNumbersData> passengerNumberDataList) {

		List<PassengerBaseData> listPassengers = new ArrayList<PassengerBaseData>();

		for (PassengerNumbersData passengerNumbersData : passengerNumberDataList) {
			if (passengerNumbersData.getPassengerType().equals(PassengerTypeEnum.ADULT)) {
				for (int i = 0; i < passengerNumbersData.getNumber().intValue(); i++) {
					AdultPassengerData passengerBaseData = new ApplicantPassengerData();
					passengerBaseData.setCouponPrice(BigDecimal.valueOf(0L));
					passengerBaseData.setExtraCharge(BigDecimal.valueOf(0L));
					passengerBaseData.setFee(BigDecimal.valueOf(0L));
					passengerBaseData.setGrossFare(BigDecimal.valueOf(0L));
					passengerBaseData.setNetFare(BigDecimal.valueOf(0L));
					passengerBaseData.setType(PassengerTypeEnum.ADULT);
					listPassengers.add(passengerBaseData);
				}
			} else if (passengerNumbersData.getPassengerType().equals(PassengerTypeEnum.APPLICANT)) {
				for (int i = 0; i < passengerNumbersData.getNumber().intValue(); i++) {
					ApplicantPassengerData passengerBaseData = new ApplicantPassengerData();
					passengerBaseData.setCouponPrice(BigDecimal.valueOf(0L));
					passengerBaseData.setExtraCharge(BigDecimal.valueOf(0L));
					passengerBaseData.setFee(BigDecimal.valueOf(0L));
					passengerBaseData.setGrossFare(BigDecimal.valueOf(0L));
					passengerBaseData.setNetFare(BigDecimal.valueOf(0L));
					passengerBaseData.setType(PassengerTypeEnum.ADULT);
					passengerBaseData.setArTaxInfoType(ARTaxInfoTypesEnum.NONE);
					passengerBaseData.setBlueBizCode("");
					passengerBaseData.setSkyBonusCode("");
					listPassengers.add(passengerBaseData);
				}
			} else if (passengerNumbersData.getPassengerType().equals(PassengerTypeEnum.CHILD)) {
				for (int i = 0; i < passengerNumbersData.getNumber().intValue(); i++) {
					ChildPassengerData passengerBaseData = new ChildPassengerData();
					passengerBaseData.setCouponPrice(BigDecimal.valueOf(0L));
					passengerBaseData.setExtraCharge(BigDecimal.valueOf(0L));
					passengerBaseData.setFee(BigDecimal.valueOf(0L));
					passengerBaseData.setGrossFare(BigDecimal.valueOf(0L));
					passengerBaseData.setNetFare(BigDecimal.valueOf(0L));
					passengerBaseData.setType(PassengerTypeEnum.CHILD);
					listPassengers.add(passengerBaseData);
				}
			} else if (passengerNumbersData.getPassengerType().equals(PassengerTypeEnum.INFANT)) {
				for (int i = 0; i < passengerNumbersData.getNumber().intValue(); i++) {
					InfantPassengerData passengerBaseData = new InfantPassengerData();
					passengerBaseData.setCouponPrice(BigDecimal.valueOf(0L));
					passengerBaseData.setExtraCharge(BigDecimal.valueOf(0L));
					passengerBaseData.setFee(BigDecimal.valueOf(0L));
					passengerBaseData.setGrossFare(BigDecimal.valueOf(0L));
					passengerBaseData.setNetFare(BigDecimal.valueOf(0L));
					passengerBaseData.setType(PassengerTypeEnum.INFANT);
					listPassengers.add(passengerBaseData);
				}
			}
		}
		return listPassengers;
	}
}
