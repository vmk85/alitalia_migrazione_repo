package com.alitalia.aem.consumer.booking.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import org.apache.commons.lang3.ArrayUtils;

import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.render.AvailableFlightsSelectionRender;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.booking.render.DirectFlightDataRender;
import com.alitalia.aem.consumer.booking.render.SearchElementDetailRender;
import com.alitalia.aem.consumer.booking.render.SearchElementRender;
import com.day.cq.i18n.I18n;


import javax.inject.Inject;

public class PDFSliceInfo  {

	private List<AvailableFlightsSelectionRender> flightSelectionsDetails;
	private String[][] recapFlight;
	private String[][] recapPassenger;



	public PDFSliceInfo(List<AvailableFlightsSelectionRender> flightSelectionsDetails, int sliceIndex, 
			String[] passengersInfo, String[][] ticket, String[][] referedInfant, 
			String[][] seatsInfo, String[][] mealsInfo, String cuit, BookingSessionContext ctx, I18n i18n, AlitaliaConfigurationHolder configurationHolder){
		
		this.flightSelectionsDetails = flightSelectionsDetails;
		recapFlight = computeRecapFlight(sliceIndex, ctx, i18n, configurationHolder);
		recapPassenger = computeRecapPassenger(sliceIndex, passengersInfo, ticket, referedInfant, seatsInfo, mealsInfo, cuit, ctx, i18n);
	}
	
	private String[][] computeRecapFlight(int sliceIndex, BookingSessionContext ctx, I18n i18n,AlitaliaConfigurationHolder configurationHolder) {
		/*{ "ANDATA" },
		{ "24", "mag", "2015" },
		{ "15:30", "Milano", "MALPENSA (mxp)" },
		{ "23:40", "new york", "John F. Kennedy (jfk)"},
		{ "Economy confort", 
			"Numero volo: AZ232 | Operato da: Alitalia, Air France" },
		{ "3 h 2 m" },
		{ "25", "mag", "2015" },
		{ "23:50", "new york", "John F. Kennedy (jfk)" },
		{ "06:00", "las vegas", "Maccarran International Airport (LAS)", "+1 gg"},
		{ "Business", 
			"Numero volo: AZ675 | Operato da: Alitalia" }*/
		
		String[][] recapFlight = null;
		List<String[]> arrayRecapFlight = new ArrayList<String[]>();
		List<SearchElementRender> searchElementRenderList = computeSearchEementRenderList(ctx,i18n);
		if(ctx.flightSelections != null){
			
			if (ctx.searchKind != BookingSearchKindEnum.MULTILEG) {
				if (sliceIndex == 0) {
					String[] sliceName = {i18n.get("booking.carrello.andata.label").toUpperCase()};
					arrayRecapFlight.add(sliceName);
				} else {
					String[] sliceName = {i18n.get("booking.carrello.ritorno.label").toUpperCase()};
					arrayRecapFlight.add(sliceName);
				}
			} else {
				String[] sliceName = {obtainTitleBySliceIndex(sliceIndex, i18n)};
				arrayRecapFlight.add(sliceName);
			}
			
			AvailableFlightsSelectionRender flightSelection = this.flightSelectionsDetails.get(sliceIndex);
			//ciclo sugli scali
			for (DirectFlightDataRender stop : flightSelection.getStops()) {
				
				DateRender dateRenderDeparture = stop.getFlightDetailsInfo().get(0).getDate();
				String[] date = {dateRenderDeparture.getDay(), i18n.get(dateRenderDeparture.getShortTextMonth()), dateRenderDeparture.getYear()}; //{ "24", "mag", "2015" },
				arrayRecapFlight.add(date);
				
				
				String departureAirportName = stop.getDirectFlightData().isBus() ? 
						"" : i18n.get("airportsData." + stop.getFlightDetailsInfo().get(0).getAirportCode() + ".name");
				String[] departure = {dateRenderDeparture.getHour() + dateRenderDeparture.getPeriod(),
						stop.getFlightDetailsInfo().get(0).getCityName(),
						departureAirportName + " (" + stop.getFlightDetailsInfo().get(0).getAirportCode() + ")"};
				arrayRecapFlight.add(departure); //{ "15:30", "Milano", "MALPENSA (mxp)" }
				
				DateRender dateRenderArrival = stop.getFlightDetailsInfo().get(1).getDate();
				String nextDays = stop.getNextDays() > 0 ? " (+" + stop.getNextDays() + i18n.get("booking.carrello.nextDays.label") + ")" : "";
				String arrivalI18nString = "airportsData." + stop.getFlightDetailsInfo().get(1).getAirportCode() + ".name";
				String arrivalAirportName = i18n.get(arrivalI18nString);
				if (arrivalI18nString.equals(arrivalAirportName)) {
					arrivalAirportName = "";
				}
				String[] arrival = {dateRenderArrival.getHour() + dateRenderArrival.getPeriod() + nextDays,
						stop.getFlightDetailsInfo().get(1).getCityName(),
						arrivalAirportName + " (" + stop.getFlightDetailsInfo().get(1).getAirportCode() + ")"};
				arrayRecapFlight.add(arrival);  //{ "23:40", "new york", "John F. Kennedy (jfk)"},
				
				String cabin = searchElementRenderList.get(sliceIndex).getCabin();
				String codShare = "";
				if (stop.getCarrier() != null && !stop.getCarrier().isEmpty()) {
					codShare = " | " + i18n.get("booking.flightSelect.voloOperatoDa.label") + " " + stop.getCarrier();
				}
				String numeroLabel = stop.getDirectFlightData().isBus() ? i18n.get("booking.bus.numero.label") : i18n.get("booking.carrello.numeroVolo.label");
				
				String voloSoggettoApprovazione = "";
				
				if (stop.getDirectFlightData() != null && stop.getDirectFlightData().getFlightNumber() != null &&
						stop.getDirectFlightData().getCarrier()  != null ) {
					String flightNumber = stop.getDirectFlightData().getFlightNumber();
					String carrier = stop.getDirectFlightData().getCarrier();

                    Boolean isSoggettoAppGov=computeIsSoggettoAppGov(obtainFlightNumbers(stop.getDirectFlightData()),configurationHolder);
                    stop.getDirectFlightData().setIsSoggettoApprovazioneGovernativa(isSoggettoAppGov);
                    if (isSoggettoAppGov.equals(Boolean.TRUE)) {
						voloSoggettoApprovazione = " - " + i18n.get("booking.common.voloSoggettoApprovazione.label");
					}
				}
				
				
				String flightInfo = numeroLabel
						+ ": " + stop.getDirectFlightData().getCarrier()
						+ stop.getDirectFlightData().getFlightNumber()
						+ codShare + voloSoggettoApprovazione;
				String[] flightDetails =  {cabin, flightInfo};
				arrayRecapFlight.add(flightDetails); //{ "Economy confort", "Numero volo: AZ232 | Operato da: Alitalia, Air France" },
				
				String wait = "";
				if (stop.getWaitingTime() != null && !stop.getWaitingTime().isEmpty()) {
					wait = stop.getWaitingTime() + " " + i18n.get("booking.flightselect.flightdetails.sosta.label");
					String[] waitingTime = {wait};
					arrayRecapFlight.add(waitingTime); //{ "3 h 2 m" }
				}
			}
			
		}
		
		recapFlight = (String[][]) arrayRecapFlight.toArray(new String[0][0]);
		return recapFlight;
	}

	private Boolean computeIsSoggettoAppGov(String numeroVolo, AlitaliaConfigurationHolder configuration) {

		String[] arrayVolo =configuration.getListaVoliSoggettiApprovazioneGovernativa().split(";");
		for (int i=0; i<arrayVolo.length; i++){
			if (arrayVolo[i].equals(numeroVolo)) {return true;}
		}
		return false;
	}

	private String obtainFlightNumbers(FlightData flightData) {
		String result = "";
		//FlightData flightData = flightSelection.getFlightData();
		if (flightData instanceof DirectFlightData) {
			DirectFlightData directFlightData = (DirectFlightData) flightData;
			return directFlightData.getCarrier() + directFlightData.getFlightNumber();
		} else {
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
			for (FlightData flight : connectingFlightData.getFlights()) {
				DirectFlightData directFlightData = (DirectFlightData) flight;
				String number = directFlightData.getCarrier() + directFlightData.getFlightNumber();
				if (!result.equals("")) {
					result += ", " + number;
				} else {
					result = number;
				}
			}
		}
		return result;
	}

	public String[][] getRecapFlight() {
		return recapFlight;
	}

	public String[][] getRecapPassenger() {
		return recapPassenger;
	}
	
	private String[][] computeRecapPassenger(int sliceIndex, String[] passengersInfo, String[][] ticket, 
			String[][] referedInfant, String[][] seatsInfo, String[][] mealsInfo, String cuit,
			BookingSessionContext ctx, I18n i18n) {
		/*
		 * { "Mario Rossi", "6D", "non assegnato",
			"Numero biglietto: 0239807890125" },
			{ "Tagliabue Alessandra", "5B", "Vegetariano",
				"Numero biglietto: 1234567890123",
				"Numero biglietto 2: 1234567890123", "+infant",
				"Numero biglietto: 2585252525212" },
		 */
		
		List<String[]> arrayRecapPassenger = new ArrayList<String[]>();
		
		int indexPax = 0;
		for (String pax : passengersInfo) {
			if (pax != null) {
				String seat = seatsInfo[sliceIndex][indexPax];
				String meal = mealsInfo[sliceIndex][indexPax];
				
				String[] passengerPref = {pax, seat, meal};
				String[] passengerCuit = {cuit};
				String[] passengerTicket = ticket[indexPax]; 
				String[] infantTicket = referedInfant[indexPax];
				
				String[] passenger = null;
				if (indexPax == 0 && !cuit.isEmpty()) {
					passenger = (String[]) ArrayUtils.addAll(passengerPref, passengerCuit);
					passenger = (String[]) ArrayUtils.addAll(passenger, passengerTicket);
				} else {
					passenger = (String[]) ArrayUtils.addAll(passengerPref, passengerTicket);
				}
				if (infantTicket != null) {
					passenger = (String[]) ArrayUtils.addAll(passenger, infantTicket);
				}
				arrayRecapPassenger.add(passenger);
			}
			indexPax++;
		}
		
		return (String[][]) arrayRecapPassenger.toArray(new String[0][0]);
	}

	private List<SearchElementRender> computeSearchEementRenderList(BookingSessionContext ctx, I18n i18n) {
		List<SearchElementRender> searchElementRenderList = new ArrayList<SearchElementRender>();
		if(ctx.searchKind != BookingSearchKindEnum.MULTILEG){
			for (int i=0; i < ctx.flightSelections.length; i++){
				SearchElementRender searchElementRender = null;
				Calendar departureDate = ctx.searchElements.get(i).getDepartureDate();
				
				if (ctx.flightSelections[i] != null) { //the flight was selected
					FlightData selectedFlightData = ctx.flightSelections[i].getFlightData();
					DirectFlightData firstFlight;
					DirectFlightData lastFlight;
					if (selectedFlightData.getFlightType() == FlightTypeEnum.CONNECTING) {
						ConnectingFlightData connectingFlights = (ConnectingFlightData) selectedFlightData;
						firstFlight = (DirectFlightData) connectingFlights.getFlights().get(0);
						lastFlight = (DirectFlightData) connectingFlights.getFlights().get(connectingFlights.getFlights().size()-1);
					} else {
						DirectFlightData directFlight = (DirectFlightData) selectedFlightData;
						firstFlight = directFlight;
						lastFlight = directFlight;
					}

					SearchElementDetailRender from = new SearchElementDetailRender(
							new DateRender(firstFlight.getDepartureDate()),
							firstFlight.getFrom().getCode(),
							firstFlight.getFrom().getName(),
							firstFlight.getFrom().getCityCode(),
							i18n.get("airportsData." + firstFlight.getFrom().getCode() + ".city"));

					SearchElementDetailRender to = new SearchElementDetailRender(
							new DateRender(lastFlight.getArrivalDate()),
							lastFlight.getTo().getCode(),
							lastFlight.getTo().getName(),
							lastFlight.getTo().getCityCode(),
							i18n.get("airportsData." + lastFlight.getTo().getCode() + ".city"));

					searchElementRender = new SearchElementRender(
							new DateRender(departureDate), from, 
							to, selectedFlightData, ctx , i);
					
					searchElementRenderList.add(searchElementRender); 
				}
			}
		} else {
			int i = 0;
			for(SearchElement searchElement : ctx.searchElements){
				SearchElementDetailRender departureSearchElement = new SearchElementDetailRender(null, searchElement.getFrom().getAirportCode(), searchElement.getFrom().getCityCode(), searchElement.getFrom().getCityName());
				SearchElementDetailRender arrivalSearchElement = new SearchElementDetailRender(null, searchElement.getTo().getAirportCode(), searchElement.getTo().getCityCode(), searchElement.getTo().getCityName());
				SearchElementRender searchElementRender = new SearchElementRender(new DateRender(searchElement.getDepartureDate()), departureSearchElement, arrivalSearchElement, null, ctx, i);
				searchElementRenderList.add(searchElementRender);
				i++;
			}
		}
		return searchElementRenderList;
	}
	
	private String obtainTitleBySliceIndex(int sliceIndex, I18n i18n) {
		String msg = "";
		switch(sliceIndex){
			case 0:{
				msg = i18n.get("booking.common.multitratta.prima.label"); //"PRIMA"
			}break;
			case 1:{
				msg = i18n.get("booking.common.multitratta.seconda.label"); //"SECONDA"
			}break;
			case 2:{
				msg = i18n.get("booking.common.multitratta.terza.label"); //"TERZA"
			}break;
			case 3:{
				msg = i18n.get("booking.common.multitratta.quarta.label"); //"QUARTA"
			}break;
			default:{
				return "";
			}
		}
		return (msg + " " + i18n.get("booking.common.multitratta.tratta.label")).toUpperCase(); //"TRATTA"
	}

	
}
