package com.alitalia.aem.consumer.checkin;

public class CheckinStatus {

	private String url;
	private String urlLabel;
	private String target;
	
	public CheckinStatus(String url, String urlLabel) {
		this.setUrl(url);
		this.setUrlLabel(urlLabel);
	}
	
	public CheckinStatus(String url, String urlLabel, String target) {
		this(url, urlLabel);
		this.target = target;
	}

	public String getUrl() {
		return url;
	}

	private void setUrl(String url) {
		this.url = url;
	}

	public String getUrlLabel() {
		return urlLabel;
	}

	private void setUrlLabel(String urlLabel) {
		this.urlLabel = urlLabel;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

}
