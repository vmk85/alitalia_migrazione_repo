package com.alitalia.aem.consumer.checkin.servlet;


import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.component.behaviour.exc.BehaviourException;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinUtils;
import com.alitalia.aem.consumer.checkin.exception.CheckinPnrInformationNotFoundException;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.alitalia.aem.service.api.home.exc.AlitaliaServiceException;
import com.alitalia.aem.web.component.webcheckin.delegate.WebCheckinDelegate;
import com.alitalia.aem.ws.common.exception.WSClientException;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;

/**
 * REST service to initialize the CheckInSession object in session for
 * the provided passenger name, surname and trip id.
 * 
 * Returns a JSON with outcome and redirect instructions.
 */

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checkinsessioninit" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinSessionInitServlet extends GenericCheckinFormValidatorServlet {
	
	private final static String ERROR_QSTRING = "?success=false";
	private final static String REASON_QSTRING = "&reason=";
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile CheckinSession checkInSession;
	
	@Reference
	private volatile WebCheckinDelegate checkInDelegate;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		String passengerName = request.getParameter("checkin__name");
		String passengerSurname = request.getParameter("checkin__surname");
		String tripId = request.getParameter("checkin__code");
		String MMcode = request.getParameter("checkin_code");
		String pin = request.getParameter("checkin_pin");
		String searchType = request.getParameter("checkinSearchType"); //[login, code]
		
		Validator validator = new Validator();
		
		if (CheckinConstants.CHECKIN_SEARCH_PNR.equals(searchType)) {
			validator.addDirectCondition("checkin__name", passengerName, 
					CheckinConstants.MESSAGE_ERROR_EMPTY_NAME, "isNotEmpty");
			validator.addDirectCondition("checkin__name", passengerName, 
					CheckinConstants.MESSAGE_ERROR_INVALID_NAME, "isNomeValido");
			
			validator.addDirectCondition("checkin__surname", passengerSurname, 
					CheckinConstants.MESSAGE_ERROR_EMPTY_LAST_NAME, "isNotEmpty");
			validator.addDirectCondition("checkin__surname", passengerSurname, 
					CheckinConstants.MESSAGE_ERROR_INVALID_LAST_NAME, "isNomeValido");
			
			validator.addDirectCondition("checkin__code", tripId, 
					CheckinConstants.MESSAGE_ERROR_EMPTY_PNR, "isNotEmpty");
			validator.addDirectCondition("checkin__code", tripId, 
					CheckinConstants.MESSAGE_ERROR_INVALID_PNR, "isPnrOrTicketNumber");
		} 
//		else {
//			validator.addDirectCondition("checkin__MMcode", MMcode, 
//					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
//			validator.addCrossConditionMessagePattern("checkin__MMcode", MMcode, "9",
//					I18nKeyCommon.MESSAGE_INVALID_FIELD,
//					I18nKeySpecialPage.MILLEMIGLIACODE_ERROR_NOT_VALID, "sizeIsLess");
//			
//			validator.addDirectCondition("checkin__pin", pin, 
//					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
//			validator.addDirectConditionMessagePattern("checkin__pin", pin,
//					I18nKeyCommon.MESSAGE_INVALID_FIELD,
//					I18nKeySpecialPage.PIN_ERROR_NOT_VALID, "isNumber");
//			validator.addCrossConditionMessagePattern("checkin__pin", pin, "4",
//					I18nKeyCommon.MESSAGE_INVALID_FIELD,
//					I18nKeySpecialPage.PIN_ERROR_NOT_VALID, "sizeIsEqual");
//		}

		ResultValidation resultValidation = validator.validate();
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		String passengerName = request.getParameter("checkin__name");
		String passengerSurname = request.getParameter("checkin__surname");
		String tripId = request.getParameter("checkin__code");
		String MMcode = request.getParameter("checkin__MMcode");
		String pin = request.getParameter("checkin__pin");
		String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		String currencyCode = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencyCode", String.class);
		String currencySymbol = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencySymbol", String.class);
		Locale locale = AlitaliaUtils.getRepositoryPathLocale(request.getResource());
		String callerIp = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
		I18n i18n = new I18n(request.getResourceBundle(locale));
		String machineName = AlitaliaUtils.getLocalHostName("unknown");
		String searchType = (request.getParameter("checkinSearchType")!=null)?request.getParameter("checkinSearchType"):CheckinConstants.CHECKIN_SEARCH_LOGIN; //[login, code]
		
		/*Currency format info*/
		InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(request.getResource());
		String decimalSeparator=ivm.getInherited("decimalSeparator", String.class);
		String groupingSeparator=ivm.getInherited("groupingSeparator", String.class);
		Integer numDecDigits=ivm.getInherited("decimalDigit", Integer.class);
		if(numDecDigits == null){
			numDecDigits = 2;
		}
		NumberFormat currentNumberFormat = AlitaliaUtils.getNumberFormat(numDecDigits, decimalSeparator, groupingSeparator, market);
		
		HttpSession session = request.getSession(true);
		
		CheckinSessionContext ctx = checkInSession.initializeSession(request, i18n, CheckinConstants.CHECKIN_CLIENT, machineName, 
				(session.getId() != null ? session.getId() : ""), CheckinConstants.CHECKIN_SITE, market, currencyCode, currencySymbol, locale, 
				callerIp, tripId, passengerName, passengerSurname, MMcode, pin, searchType, currentNumberFormat, null);
		
		session.setAttribute(CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE, ctx);
		
		// String protocol = configuration.getHttpsEnabled() ? "https://": "http://";
		String redirectUrl = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCheckinFlightListPage();
		redirectUrl = request.getResourceResolver().map(redirectUrl);
		//redirectUrl = protocol + configuration.getExternalDomain() + redirectUrl;
		if (isAjaxRequest(request)) {
			// for normal ajax requests, perform a standard JSON based redirect on successful search
			TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
			response.setContentType("application/json");
			json.object();
			json.key("result").value("OK");
			json.key("pnr").value(ctx.pnr);
			json.key("Routes");
			json.array();
			for (CheckinRouteData routeData:ctx.routesList) {
				if (CheckinUtils.checkValidCheckin(routeData, 0, "") != null ||
						CheckinUtils.checkPartner(routeData, 0, ctx) != null ||
						CheckinUtils.checkAlreadyCheckedIn(routeData) != null) {
					json.object();
					json.key("Status").value(routeData.getStatus().value());
					json.key("Flights");
					json.array();
					 List <CheckinFlightData> flights = CheckinUtils.getFlightsList(routeData); 
					for (CheckinFlightData flight:flights) {
						json.object();
						json.key("Carrier").value(flight.getCarrier());
						json.key("Number").value(flight.getFlightNumber());
						json.key("FromCity").value(flight.getFrom().getCity());
						json.key("ToCity").value(flight.getTo().getCity());
						json.endObject();
					}
					json.endArray();
					json.endObject();
				}
			}
			json.endArray();
			json.key("redirect").value(redirectUrl);
			json.endObject();
		} else {
			// for non-ajax requests, perform an HTTP redirection on successful search
			response.sendRedirect(redirectUrl);
		}
	}
	
	/**
	 * For this servlet, hardcode the self page URL to the PNR search page.
	 * This provide correct user redirection when a wrong search is performed
	 * with a non-ajax request (e.g. from confirmation email link).
	 */
	@Override
	protected String getSelfPageUrl(SlingHttpServletRequest request) {
		return AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getCheckinHomePage();
	}
	
	/**
	 * For this servlet, overwrite the default MMB error page with the PNR search page.
	 * This provide correct user redirection when a wrong search is performed
	 * with an ajax request (e.g. from PNR search page itself).
	 */
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		Map additionalParams = super.getAdditionalFailureData(request, exception);
		
		String url =  AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getCheckinHomePage() + ERROR_QSTRING;
		
		if (exception.getCause() != null &&
				exception.getCause() instanceof CheckinPnrInformationNotFoundException &&
				exception.getCause().getMessage() != null) {
			url += REASON_QSTRING + exception.getCause().getMessage();
		}
				
		additionalParams.put("redirect", request.getResourceResolver().map(url));
		return additionalParams;
	}
	
	
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}
