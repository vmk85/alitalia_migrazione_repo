package com.alitalia.aem.consumer.millemiglia.model.profiloutente;

public class DatiLoginData {

	private String nickname;
	private String pin;
	private String confermaPin;
	
	public String getNickname() {
		return nickname;
	}
	
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public String getPin() {
		return pin;
	}
	
	public void setPin(String pin) {
		this.pin = pin;
	}
	
	public String getConfermaPin() {
		return confermaPin;
	}
	
	public void setConfermaPin(String confermaPin) {
		this.confermaPin = confermaPin;
	}
}
