package com.alitalia.aem.consumer.servlet.getoedavailability;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.Availability;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityRequest;
import com.alitalia.aem.common.messages.home.RetrieveAvailabilityResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.staticdata.delegaterest.StaticDataDelegateRest;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "oedavailabilityconsumer" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class OedAvailabilityConsumerServlet extends SlingSafeMethodsServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	//Search Availability
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegateRest staticDataDelegateRest;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		// content type
		response.setContentType("application/json");

		// JSON response
		TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());
		try {

			// prepare resource bundle for i18n
			Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
			ResourceBundle resourceBundle = request.getResourceBundle(locale);
			final I18n i18n = new I18n(resourceBundle);

			// setting variables for availability json object
			//				String originIataCode;
			//				String originDescription;
			//				String originCountryCode;
			//				String destinationIataCode;
			//				String destionationDescription;
			//				String destinationCountryCode;
			//				String monFirstHalfYear;
			//				String tueFirstHalfYear;
			//				String wedFirstHalfYear;
			//				String thuFirstHalfYear;
			//				String friFirstHalfYear;
			//				String satFirstHalfYear;
			//				String sunFirstHalfYear;
			//				String monSecondHalfYear;
			//				String tueSecondHalfYear;
			//				String wedSecondHalfYear;
			//				String thuSecondHalfYear;
			//				String friSecondHalfYear;
			//				String satSecondHalfYear;
			//				String sunSecondHalfYear;

			RetrieveAvailabilityRequest serviceRequest = new RetrieveAvailabilityRequest(IDFactory.getTid(), IDFactory.getSid(request));
			String language =  AlitaliaUtils.getRepositoryPathLanguage(request.getResource());//findResourceLocale(slingHttpServletRequest.getResource()).getLanguage();
			String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());//AlitaliaUtils.findResourceLocale(slingHttpServletRequest.getResource()).getCountry();
			String conversationId = IDFactory.getTid();
			serviceRequest.setOriginIATA(request.getParameter("originIATA"));
			serviceRequest.setDestinationIATA(request.getParameter("destinationIATA"));
			serviceRequest.setMarket(market);
			serviceRequest.setLanguage(language);
			serviceRequest.setConversationID(conversationId);
			logger.info("Chiamando StaticDataDelegateRest da OedAvailabilityConsumerServlet...");
			RetrieveAvailabilityResponse serviceResponse = staticDataDelegateRest.getAvailabilty(serviceRequest);
			logger.info("Dopo StaticDataDelegateRest da OedAvailabilityConsumerServlet...");

			Availability availability = serviceResponse.getAvailability();



			logger.info("Chiamando StaticDataDelegateRest da OedAvailabilityConsumerServlet...");
			serviceRequest.setOriginIATA(request.getParameter("destinationIATA"));
			serviceRequest.setDestinationIATA(request.getParameter("originIATA"));
			RetrieveAvailabilityResponse serviceResponse_ret = staticDataDelegateRest.getAvailabilty(serviceRequest);
			logger.info("Dopo StaticDataDelegateRest da OedAvailabilityConsumerServlet...");

			Availability availability_ret = serviceResponse_ret.getAvailability();

			// out JSON response
			jsonOutput.object();

			//inizio oggetto Availability
			jsonOutput.key("availability");
			jsonOutput.object();
			jsonOutput.key("iataOrigine").value(availability.getIataOrigine());
			jsonOutput.key("iataDestinazione").value(availability.getIataDestinazione());
			jsonOutput.key("mondaySummer").value(availability.isMondaySummer());
			jsonOutput.key("tuesdaySummer").value(availability.isTuesdaySummer());
			jsonOutput.key("wednesdaySummer").value(availability.isWednesdaySummer());
			jsonOutput.key("thursdaySummer").value(availability.isThursdaySummer());
			jsonOutput.key("fridaySummer").value(availability.isFridaySummer());
			jsonOutput.key("saturdaySummer").value(availability.isSaturdaySummer());
			jsonOutput.key("sundaySummer").value(availability.isSundaySummer());
			jsonOutput.key("mondayWinter").value(availability.isMondayWinter());
			jsonOutput.key("tuesdayWinter").value(availability.isTuesdayWinter());
			jsonOutput.key("wednesdayWinter").value(availability.isWednesdayWinter());
			jsonOutput.key("thursdayWinter").value(availability.isThursdayWinter());
			jsonOutput.key("fridayWinter").value(availability.isFridayWinter());
			jsonOutput.key("saturdayWinter").value(availability.isSaturdayWinter());
			jsonOutput.key("sundayWinter").value(availability.isSundayWinter());
			jsonOutput.key("startSummer").value(availability.getStartSummer());
			jsonOutput.key("endSummer").value(availability.getEndSummer());
			jsonOutput.key("startWinter").value(availability.getStartWinter());
			jsonOutput.key("endWinter").value(availability.getEndWinter());
			//fine oggetto Availability
			jsonOutput.endObject();

			//inizio oggetto Availability
			jsonOutput.key("availability_ret");
			jsonOutput.object();
			jsonOutput.key("iataOrigine").value(availability_ret.getIataOrigine());
			jsonOutput.key("iataDestinazione").value(availability_ret.getIataDestinazione());
			jsonOutput.key("mondaySummer").value(availability_ret.isMondaySummer());
			jsonOutput.key("tuesdaySummer").value(availability_ret.isTuesdaySummer());
			jsonOutput.key("wednesdaySummer").value(availability_ret.isWednesdaySummer());
			jsonOutput.key("thursdaySummer").value(availability_ret.isThursdaySummer());
			jsonOutput.key("fridaySummer").value(availability_ret.isFridaySummer());
			jsonOutput.key("saturdaySummer").value(availability_ret.isSaturdaySummer());
			jsonOutput.key("sundaySummer").value(availability_ret.isSundaySummer());
			jsonOutput.key("mondayWinter").value(availability_ret.isMondayWinter());
			jsonOutput.key("tuesdayWinter").value(availability_ret.isTuesdayWinter());
			jsonOutput.key("wednesdayWinter").value(availability_ret.isWednesdayWinter());
			jsonOutput.key("thursdayWinter").value(availability_ret.isThursdayWinter());
			jsonOutput.key("fridayWinter").value(availability_ret.isFridayWinter());
			jsonOutput.key("saturdayWinter").value(availability_ret.isSaturdayWinter());
			jsonOutput.key("sundayWinter").value(availability_ret.isSundayWinter());
			jsonOutput.key("startSummer").value(availability_ret.getStartSummer());
			jsonOutput.key("endSummer").value(availability_ret.getEndSummer());
			jsonOutput.key("startWinter").value(availability_ret.getStartWinter());
			jsonOutput.key("endWinter").value(availability_ret.getEndWinter());
			//fine oggetto Availability
			jsonOutput.endObject();

			jsonOutput.key("conversationID").value(serviceResponse.getConversationID());

			jsonOutput.endObject();

			//					List<AirportInfo> airportList = new ArrayList<AirportInfo>();
			//					for (AirportData airportData : list) {
			//						
			//						airportCode = airportData.getCode();
			//						keyAirportName = "airportsData." + airportCode + ".name";
			//						airportName = i18n.get(keyAirportName);
			//						
			//						cityCode = airportData.getCityCode();
			//						keyCityCode = "airportsData." + airportCode + ".city";
			//						city = i18n.get(keyCityCode);
			//						
			//						countryCode = airportData.getCountryCode();
			//						keyCountryCode = "airportsData." + countryCode + ".country";
			//						country = i18n.get(keyCountryCode);

			// add to JSON only if translation is provided
			//						if (!city.equals(keyCityCode) &&
			//								!country.equals(keyCountryCode) &&
			//								!airportName.equals(keyAirportName)) {
			//							AirportInfo airport =
			//									new AirportInfo(
			//										airportCode, keyAirportName,
			//										airportName, cityCode, keyCityCode, city,
			//										countryCode, keyCountryCode, country);
			//							airportList.add(airport);
			//						}
			//					}
			//					Collections.sort(airportList,
			//							AirportInfo.COMPARE_BY_CITY_AND_AIRPORT);


			//for (AirportInfo airportInfo : airportList) {

		}

		// an error occurred...
		catch (Exception e) {
			e.printStackTrace();

			//logger.error("Errore durante recupero Airport List Consumer", e);
			throw new RuntimeException("Errore durante recupero Availability", e);
		}
	}


}
