package com.alitalia.aem.consumer.checkinrest.jobs.consumer;

import com.alitalia.aem.common.data.checkinrest.model.offload.response.FlightPassR;
import com.alitalia.aem.common.data.checkinrest.model.offload.response.PnrFlightInfoR;
import com.alitalia.aem.common.data.checkinrest.model.offload.response.SeatListR;
import com.alitalia.aem.common.messages.checkinrest.*;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.checkinrest.jobs.CheckinJobConstants;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;
import com.alitalia.aem.web.component.webcheckin.delegaterest.IInsuranceDelegate;
import com.google.gson.Gson;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@Component(
        label = "Alitalia CheckinRestAPI - Clear Comfort Seat Job Consumer",
        description = "Call Rest API (BL) for Clear Comfort Seat",

        // One of the few cases where immediate = true; this is so the Event Listener starts listening immediately
        immediate = true
)
@Properties({
        @Property(
                label = "Alitalia CheckinRestAPI - Clear Comfort Seat Job Consumer",
                value = CheckinJobConstants.TOPIC_ANCILLARIES_COMFORT_SEAT_CLEAR_AND_CHECKIN,
                description = "Call Rest API (BL) for Clear Comfort Seat",
                name = JobConsumer.PROPERTY_TOPICS,
                propertyPrivate = true
        )
})
@Service(value={JobConsumer.class})
public class ClearComfortSeatJob implements JobConsumer {

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile ICheckinDelegate checkInDelegateRest;

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile IInsuranceDelegate insuranceDelegateRest;

    private static final Logger logger = LoggerFactory.getLogger(OffloadPassengerJob.class);

    final public static String PROPERTY_CHECKIN_SESSION_ID = "checkinrest-sessionid";

    final public static String PROPERTY_CHECKIN_CLEARANCILLARIES = "checkinrest-clearancillaries";

    final public static String PROPERTY_CHECKIN_COMFORTSEATCLEAR = "checkinrest-clearcomfortseat";

    final public static String PROPERTY_CHECKIN_CLEARINSURANCE = "checkinrest-clearinsurance";

    final public static String PROPERTY_CHECKIN_OFFLOAD = "checkinrest-offload";

    @Override
    public JobResult process(Job job) {
        // process the job and return the result
        String sessionID = job.getProperty(PROPERTY_CHECKIN_SESSION_ID).toString();
        logger.debug("[{}] Creating Clear Comfort Seat Request",sessionID);

        Gson gson = new Gson();

        List<CheckinClearAncillarySessionEndRequest> ancillaryRequestList = new ArrayList<>();
        List<DeleteInsuranceRequest> deleteInsuranceRequestList  = new ArrayList<>();
        List<CheckinOffloadFlightsRequest> offloadRequestList = new ArrayList<>();
        List<CheckinSelectedPassengerRequest> redoCheckinPassengerList = new ArrayList<>();

        for(String propertyItemName : job.getPropertyNames()) {
            if(propertyItemName.startsWith(PROPERTY_CHECKIN_CLEARANCILLARIES) ||
                    propertyItemName.startsWith(PROPERTY_CHECKIN_CLEARINSURANCE) ||
                    propertyItemName.startsWith(PROPERTY_CHECKIN_COMFORTSEATCLEAR) ||
                    propertyItemName.startsWith(PROPERTY_CHECKIN_OFFLOAD)) {

                String jsonProperty = job.getProperty(propertyItemName).toString();

                try {
                    if(propertyItemName.startsWith(PROPERTY_CHECKIN_CLEARANCILLARIES)) {
                        CheckinClearAncillarySessionEndRequest reqItem = gson.fromJson(jsonProperty, CheckinClearAncillarySessionEndRequest.class);
                        ancillaryRequestList.add(reqItem);
                    } else if(propertyItemName.startsWith(PROPERTY_CHECKIN_CLEARINSURANCE)) {
                        DeleteInsuranceRequest reqItem = gson.fromJson(jsonProperty, DeleteInsuranceRequest.class);
                        deleteInsuranceRequestList.add(reqItem);
                    } else if (propertyItemName.startsWith(PROPERTY_CHECKIN_COMFORTSEATCLEAR)) {
                        CheckinSelectedPassengerRequest reqItem = gson.fromJson(jsonProperty, CheckinSelectedPassengerRequest.class);
                        redoCheckinPassengerList.add(reqItem);
                    } else if (propertyItemName.startsWith(PROPERTY_CHECKIN_OFFLOAD)) {
                        CheckinOffloadFlightsRequest reqItem = gson.fromJson(jsonProperty, CheckinOffloadFlightsRequest.class);
                        offloadRequestList.add(reqItem);
                    }
                } catch(Exception e) {
                    logger.error("[{}] cannot deserialize request {} " + e.toString(), sessionID,jsonProperty);
                }
            }
        }

        logger.debug("[{}] Calling service rest clearAncillaySessionEnd",sessionID);
        if(ancillaryRequestList.size() > 0) {
            Boolean status = true;
            //TODO: gestione errore 500 null
            for (CheckinClearAncillarySessionEndRequest request : ancillaryRequestList) {
                request.setSid(sessionID);
                request.setTid(IDFactory.getTid());
                CheckinClearAncillarySessionEndResponse checkinClearAncillarySessionEndResponse = checkInDelegateRest.clearAncillaySessionEnd(request);
                if (checkinClearAncillarySessionEndResponse.getOutcome().get(0) != null) {
                    if (!checkinClearAncillarySessionEndResponse.getOutcome().get(0).equals("OK")) {
                        logger.warn("[{}] Si è verificato un errore nel clear ancillary e session end {}", sessionID, checkinClearAncillarySessionEndResponse.getOutcome().get(0));
                        status = status && false;
                    } else {
                        logger.info("[{}] Ancillay e sessione ripulite correttamente. Outcome: {}", sessionID, checkinClearAncillarySessionEndResponse.getOutcome().get(0));
                        status = status && true;
                    }
                }
            }

            if(deleteInsuranceRequestList.size() > 0) {
                for (DeleteInsuranceRequest request : deleteInsuranceRequestList) {
                    request.setSid(sessionID);
                    request.setTid(IDFactory.getTid());

                    DeleteInsuranceResponse deleteInsuranceResponse = insuranceDelegateRest.deleteInsurance(request);
                    if ("OK".equals(deleteInsuranceResponse.getSuccess())) {
                        logger.info("[{}] Clear insurance eseguito per {}", sessionID, request.getPnr());
                        status = status && true;
                    } else {
                        logger.error("[" + sessionID + "] ERRORE Clear insurance  per {}", gson.toJson(request), gson.toJson(deleteInsuranceResponse));
                        status = status && false;
                    }
                }
            }

            //return status ? JobResult.OK : JobResult.CANCEL;
            //return JobResult.FAILED;
        }

        if(offloadRequestList.size() > 0) {
            Boolean status = true;
            //TODO: gestione errore 500 null
            for(CheckinOffloadFlightsRequest request : offloadRequestList) {
                request.setSid(sessionID);
                request.setTid(IDFactory.getTid());
                CheckinOffloadFlightsResponse response = checkInDelegateRest.offloadFlights(request);
                logger.debug("["+sessionID+"] OffloadCall request {} response {}", gson.toJson(request), gson.toJson(response));

                for(PnrFlightInfoR flightInfoR : response.getPnrFlightInfoRS()) {
                    for(FlightPassR flightPassR : flightInfoR.getFlightPassRS()) {
                        for(SeatListR seatListR : flightPassR.getSeatListRS()) {
                            if("OK".equals(seatListR.getEsito())) {
                                logger.info("["+sessionID+"] Offload eseguito per {} {}",
                                        seatListR.getFirstName() + " " + seatListR.getLastName(),
                                        "volo " + flightPassR.getAirline() +
                                                flightPassR.getFlight() +
                                                "da " + flightPassR.getOrigin() +
                                                "il " + flightPassR.getDepartureDate() +
                                                "pnr " +flightInfoR.getPnr());
                                status = status && true;
                            } else {
                                logger.info("["+sessionID+"] ERROR Offload per {} {}",
                                        seatListR.getFirstName() + " " + seatListR.getLastName(),
                                        "volo " + flightPassR.getAirline() +
                                                flightPassR.getFlight() +
                                                "da " + flightPassR.getOrigin() +
                                                "il " + flightPassR.getDepartureDate() +
                                                "pnr " +flightInfoR.getPnr());
                                status = status && false;
                            }
                        }
                    }
                }
            }
            //return status ? JobResult.OK : JobResult.CANCEL;
            //return JobResult.FAILED;
        } else {
            logger.warn("[{}] Offload nothing to do", sessionID);
            return JobResult.CANCEL;
        }
        
        if(redoCheckinPassengerList.size() > 0) {
        	Boolean status = true;
            for (CheckinSelectedPassengerRequest request : redoCheckinPassengerList) {
                request.setSid(sessionID);
                request.setTid(IDFactory.getTid());
                CheckinSelectedPassengerResponse checkinSelectedPassengerResponse = checkInDelegateRest.executePassengerCheckin(request);
                if (checkinSelectedPassengerResponse.getOutcome() != null) {
                    if (!checkinSelectedPassengerResponse.getOutcome().equals("OK")) {
                        logger.warn("[{}] Si è verificato un errore nel clear ancillary e session end {}", sessionID, checkinSelectedPassengerResponse.getOutcome());
                        status = status && false;
                    } else {
                        logger.info("[{}] Ancillay e sessione ripulite correttamente. Outcome: {}", sessionID, checkinSelectedPassengerResponse.getOutcome());
                        status = status && true;
                    }
                }
            }
            return status ? JobResult.OK : JobResult.CANCEL;

        } else {
            logger.warn("[{}] Offload nothing to do", sessionID);
            return JobResult.CANCEL;
        }

        }
}
