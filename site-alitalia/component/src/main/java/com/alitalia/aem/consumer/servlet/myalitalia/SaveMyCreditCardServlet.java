package com.alitalia.aem.consumer.servlet.myalitalia;

import javax.servlet.Servlet;

import com.alitalia.aem.common.data.home.*;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.messages.home.AdyenStoringPaymentRequest;
import com.alitalia.aem.common.messages.home.AdyenStoringPaymentResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaConstants;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.myalitaliauser.delegaterest.IAdyenRecurringPaymentDelegate;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "masavemycreditcard" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class SaveMyCreditCardServlet extends MyAlitaliaCreditCardServlet {
	
    @Reference
    private AlitaliaConfigurationHolder configuration;

	@Reference
	protected volatile IAdyenRecurringPaymentDelegate adyenRecurringPaymentDelegate;
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		logger.info("MAPayment - SaveMyCreditCardServlet - performSubmit: Memorizzazione Carta di Credito in Adyen");
		try {
			MyAlitaliaSessionContext mactx = (MyAlitaliaSessionContext) request.getSession().getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);
			if (mactx != null) {
				Object obj = mactx.getMaCustomer();
				if (obj instanceof MACustomer && obj!=null) {
					MACustomer loggedMAUserProfile = (MACustomer) obj;
					String UID = loggedMAUserProfile.getUID();
					if (UID!=null && UID.trim().isEmpty()==false) {
						logger.debug("MAPayment - SaveMyCreditCardServlet - performSubmit: UID [{}]", UID);
	
						logger.info("MAPayment - SaveMyCreditCardServlet - performSubmit: recupero i dati della Carta di Credito dalla request");
//						MACreditCardData creditCardData = new MACreditCardData(request.getParameter("cardCode"), request.getParameter("cardNumber"), request.getParameter("cardSecurityCode"), request.getParameter("cardHolderFirstName"),
//								request.getParameter("cardHolderLastName"), request.getParameter("expireMonth"), request.getParameter("expireYear"), request.getParameter("cardBin"));

						MACreditCardData creditCardData = new MACreditCardData(request.getParameter("cardCode"));

						AdyenAdditionalData additionalData = new AdyenAdditionalData("card.encrypted.json",request.getParameter("adyen-encrypted-data"));

						AdyenStoringPaymentRequest storingPaymentRequest = new AdyenStoringPaymentRequest();
//						AdyenCardData card = new AdyenCardData(creditCardData.getCvv(), creditCardData.getExpireMonth(), creditCardData.getExpireYear(), creditCardData.getHolderName(), creditCardData.getNumber(), creditCardData.getType());
//						storingPaymentRequest.setCard(card);
						storingPaymentRequest.setType(creditCardData.getType());
						storingPaymentRequest.setAdditionalData(additionalData);

						if (creditCardData.getCircuitCode()!=null && creditCardData.getCircuitCode().trim().isEmpty()==false && creditCardData.getCircuitCode().equals("americanexpress")) {
							creditCardData.fillAddressData(request.getParameter("postalCode"), request.getParameter("cityName"), request.getParameter("countryName"), request.getParameter("address"));
							AdyenAddressData address = new AdyenAddressData(creditCardData.getCity(), creditCardData.getCountry(), creditCardData.getZip(), creditCardData.getState(), creditCardData.getAddress());
							storingPaymentRequest.setAddress(address);
						}
						
						storingPaymentRequest.setShopperEmail(loggedMAUserProfile.getProfile().getEmail());
						storingPaymentRequest.setShopperReference(UID);
						storingPaymentRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
						storingPaymentRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
						storingPaymentRequest.setConversationID(IDFactory.getTid());
						storingPaymentRequest.setiP_Address(AlitaliaUtils.getRequestRemoteAddr(configuration, request));
	
	
						logger.info("MAPayment - SaveMyCreditCardServlet - performSubmit: invocando adyenRecurringPaymentDelegate.storingPayment...");
						AdyenStoringPaymentResponse storingPaymentResponse = adyenRecurringPaymentDelegate.storingPayment(storingPaymentRequest);
						if (storingPaymentResponse!=null) {
							if (storingPaymentResponse.getResultCode()!=null && storingPaymentResponse.getResultCode().equalsIgnoreCase("Authorised")) {
								logger.info("MAPayment - SaveMyCreditCardServlet - performSubmit: Carta di credito [{}] memorizzata", creditCardData.print());
								try {
									retrieveMyCreditCard(loggedMAUserProfile, AlitaliaUtils.getRepositoryPathMarket(request.getResource()), AlitaliaUtils.getRepositoryPathLanguage(request.getResource()), adyenRecurringPaymentDelegate);
								} catch (Exception e) {
								} finally {
									prepareResponseOK(response, "");
								}
							} else {
								logger.error("MAPayment - SaveMyCreditCardServlet - performSubmit: Errore durante la memorizzazione della carta di credito in Adyen, resultCode [{}]", storingPaymentResponse.getResultCode());
								prepareResponseNOK(response, "");
							}
						} else {
							logger.error("MAPayment - SaveMyCreditCardServlet - performSubmit: Errore durante la memorizzazione della carta di credito in Adyen, response is null");
							prepareResponseNOK(response, "");
						}
					} else {
						logger.error("MAPayment - SaveMyCreditCardServlet - performSubmit: UID utente MyAlitalia non presente");
						throw new Exception("UID utente MyAlitalia non presente");
					}
				} else {
					logger.error("MAPayment - SaveMyCreditCardServlet - performSubmit: Utente MyAlitalia non loggato");
					throw new Exception("Utente MyAlitalia non loggato");
				}
			} else {
				logger.error("MAPayment - SaveMyCreditCardServlet - performSubmit: Utente MyAlitalia non loggato");
				throw new Exception("Utente MyAlitalia non loggato");
			}
		} catch (Exception e) {
			logger.error("MAPayment - SaveMyCreditCardServlet - performSubmit: Errore durante la memorizzazione della carta di credito in Adyen, Exception [{}]", e.getMessage());
			throw e;
		}
	}

	/**
	 * Provide an empty implementation, should be never used as
	 * MMB servlets should always be invoked with ajax requests
	 */
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		logger.warn("The emtpy saveDataIntoSession method was unexpectedly invoked.");
	}
}

