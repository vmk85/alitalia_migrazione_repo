/** Modello di sessione Myalitalia
 * (Qesto modello rappresnta il contesto MyAlitalia da salvare in sessione, qui dovranno essere inseriti tutti gli oggetti e campi relativi a MyAlitalia)
 *  [D.V.] */

package com.alitalia.aem.consumer.myalitalia;

import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.consumer.myalitalia.model.MyAlitaliaOtpModel;
import com.alitalia.aem.consumer.myalitalia.model.MyFlightsModel;
import com.alitalia.aem.common.messages.crmdatarest.GetCrmDataInfoResponse;
import com.alitalia.aem.consumer.myalitalia.model.MyFlightsModel;
import com.alitalia.aem.consumer.myalitalia.model.ProfileComplite;
import com.day.cq.i18n.I18n;

public class MyAlitaliaSessionContext {

    private MACustomer maCustomer;

    private MyFlightsModel myFlightsModel;

    private GetCrmDataInfoResponse getCrmDataInfoResponse;

    private ProfileComplite profileComplite;

    private MyAlitaliaOtpModel myAlitaliaOtpModel;

    public I18n i18n;

    public String sid;

    public String tid;

    public ProfileComplite getProfileComplite() {
        return profileComplite;
    }

    public void setProfileComplite(ProfileComplite profileComplite) {
        this.profileComplite = profileComplite;
    }

    public MACustomer getMaCustomer() {
        return maCustomer;
    }

    public void setMaCustomer(MACustomer maCustomer) {
        this.maCustomer = maCustomer;
    }

    public I18n getI18n() {
        return i18n;
    }

    public void setI18n(I18n i18n) {
        this.i18n = i18n;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public GetCrmDataInfoResponse getGetCrmDataInfoResponse() {
        return getCrmDataInfoResponse;
    }

    public void setGetCrmDataInfoResponse(GetCrmDataInfoResponse getCrmDataInfoResponse) {
        this.getCrmDataInfoResponse = getCrmDataInfoResponse;
    }

    public MyFlightsModel getMyFlightsModel() {
        return myFlightsModel;
    }

    public void setMyFlightsModel(MyFlightsModel myFlightsModel) {
        this.myFlightsModel = myFlightsModel;
    }

    public MyAlitaliaOtpModel getMyAlitaliaOtpModel() {
        return myAlitaliaOtpModel;
    }

    public void setMyAlitaliaOtpModel(MyAlitaliaOtpModel myAlitaliaOtpModel) {
        this.myAlitaliaOtpModel = myAlitaliaOtpModel;
    }

    @Override
    public String toString() {
        return "MyAlitaliaSessionContext{" +
                "maCustomer=" + maCustomer +
                ", myFlightsModel=" + myFlightsModel +
                ", getCrmDataInfoResponse=" + getCrmDataInfoResponse +
                ", profileComplite=" + profileComplite +
                ", myAlitaliaOtpModel=" + myAlitaliaOtpModel +
                ", i18n=" + i18n +
                ", sid='" + sid + '\'' +
                ", tid='" + tid + '\'' +
                '}';
    }
}
