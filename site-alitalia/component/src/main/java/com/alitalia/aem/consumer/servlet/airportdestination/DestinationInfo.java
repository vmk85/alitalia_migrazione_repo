package com.alitalia.aem.consumer.servlet.airportdestination;

import java.util.Comparator;

public class DestinationInfo {
	
	private String airportCode;
	private String keyAirportName;
	private String airportName;
	private String keyCityCode;
	private String cityCode;
	private String city;
	private String countryCode;
	private String keyCountryCode;
	private String country;
	
	public static final Comparator<DestinationInfo> COMPARE_BY_CITY_AND_AIRPORT =
			new Comparator<DestinationInfo>() {

		@Override
		public int compare(DestinationInfo one, DestinationInfo other) {
			if (one.getCity().compareToIgnoreCase(other.getCity()) == 0) {
				int oneCityAllAirport = one.getCityCode()
						.compareToIgnoreCase(one.getAirportCode());
				int otherCityAllAirport = other.getCityCode()
						.compareToIgnoreCase(other.getAirportCode());
				// INTRODUCTION OF AIRPORT EXCEPTIONS - CR 14444
				int exceptionRule = findAirportExceptions(one, other);
				if (exceptionRule != 0){
					return exceptionRule;
				} else if (oneCityAllAirport == otherCityAllAirport) {
					return one.getAirportName()
							.compareToIgnoreCase(other.getAirportName());
				} else if (oneCityAllAirport < otherCityAllAirport) {
					return -1;
				} else {
					return 1;
				}
			} else if (one.getCity().compareToIgnoreCase(other.getCity()) < 0) {
				return -1;
			} else {
				return 1;
			}
		}
	};
	
	private static int findAirportExceptions(DestinationInfo one, DestinationInfo other){
		if ("VCP".equals(one.getAirportCode())){
			return 1;
		} else if ("VCP".equals(other.getAirportCode())){
			return -1;
		} else if ("MIL".equals(one.getAirportCode())) {
			return -1;
		} else if ("MIL".equals(other.getAirportCode())) {
			return 1;
		}
		return 0;
	}
	
	
	public DestinationInfo(String airportCode, String keyAirportName, String airportName, String keyCityCode,
			String cityCode, String city, String countryCode, String keyCountryCode, String country) {
		super();
		this.airportCode = airportCode;
		this.keyAirportName = keyAirportName;
		this.airportName = airportName;
		this.keyCityCode = keyCityCode;
		this.cityCode = cityCode;
		this.city = city;
		this.countryCode = countryCode;
		this.keyCountryCode = keyCountryCode;
		this.country = country;
	}

	public String getAirportCode() {
		return airportCode;
	}
	
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}
	
	public String getKeyAirportName() {
		return keyAirportName;
	}
	
	public void setKeyAirportName(String keyAirportName) {
		this.keyAirportName = keyAirportName;
	}
	
	public String getAirportName() {
		return airportName;
	}
	
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	
	public String getKeyCityCode() {
		return keyCityCode;
	}
	
	public void setKeyCityCode(String keyCityCode) {
		this.keyCityCode = keyCityCode;
	}
	
	public String getCityCode() {
		return cityCode;
	}
	
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public String getKeyCountryCode() {
		return keyCountryCode;
	}
	
	public void setKeyCountryCode(String keyCountryCode) {
		this.keyCountryCode = keyCountryCode;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	

}
