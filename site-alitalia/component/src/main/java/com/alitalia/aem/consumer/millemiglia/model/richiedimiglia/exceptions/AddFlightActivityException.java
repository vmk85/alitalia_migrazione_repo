package com.alitalia.aem.consumer.millemiglia.model.richiedimiglia.exceptions;

@SuppressWarnings("serial")
public class AddFlightActivityException extends Exception{
	private String errorKeyCode;
	
	public AddFlightActivityException(String code) {
		this.setCode(code);
	}

	public String getCode() {
		return errorKeyCode;
	}

	public void setCode(String code) {
		this.errorKeyCode = code;
	}
	

}
