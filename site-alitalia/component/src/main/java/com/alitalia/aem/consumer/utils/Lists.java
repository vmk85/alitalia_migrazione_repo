package com.alitalia.aem.consumer.utils;

import java.util.ArrayList;
import java.util.List;

public final class Lists {

    private Lists() {
    }

    public static <T> List<T> asList(T item) {
    	List<T> list = new ArrayList<T>(1);
    	list.add(item);
    	return list;
    }
    
    public static <T> T getFirst(List<T> list) {
        return list != null && !list.isEmpty() ? list.get(0) : null;
    }

    public static <T> T getLast(List<T> list) {
        return list != null && !list.isEmpty() ? list.get(list.size() - 1) : null;
    }
    
}