package com.alitalia.aem.consumer.checkin.servlet;


import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinSessionDataEmailAlert;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.alitalia.aem.web.component.webcheckin.delegate.WebCheckinDelegate;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;

/**
 * REST service to initialize the CheckInSession object in session for
 * the provided passenger name, surname and trip id.
 * 
 * Returns a JSON with outcome and redirect instructions.
 */

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checkinemailalert" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class CheckinEmailAlertServlet extends GenericCheckinFormValidatorServlet {
	
	private static String ERROR_QSTRING = "?success=false";
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Reference
	private volatile CheckinSession checkInSession;
	
	@Reference
	private volatile WebCheckinDelegate checkInDelegate;
	

	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
	
		String passengerName = request.getParameter("nome");
		String passengerSurname = request.getParameter("cognome");
		String tripId = request.getParameter("pnr");
		String MMcode = request.getParameter("mmcode");
		String pin = "";
		String email=request.getParameter("email");
		String flight_number=request.getParameter("flight");
		String depdes=request.getParameter("depdes");
		String deptime=request.getParameter("deptime");
		String arrdes=request.getParameter("arrdes");
		String arrtime=request.getParameter("arrtime");
		String action=request.getParameter("action");
		String lang=request.getParameter("lang");
		String DCSextWciAcc=request.getParameter("DCSext.wci_acc");
		String DCSextWciDel=request.getParameter("DCSext.wci_del");
		String isMobile=request.getParameter("ismobile");
		String DCSextWciMailAlert=request.getParameter("DCSext.wci_mailalert");
		String DCSextWciMailLanguage=request.getParameter("DCSext.wci_language");
		String searchType = "code";
		
		Validator validator = new Validator();
		
		if (CheckinConstants.CHECKIN_SEARCH_PNR.equals(searchType)) {
			validator.addDirectCondition("name_email_alert", passengerName, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("checkin__name_email_alert", passengerName, 
					CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
			validator.addDirectCondition("checkin_surname_email_alert", passengerSurname, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");		
			validator.addDirectCondition("surname_email_alert", passengerSurname, 
					CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
			validator.addDirectCondition("code_email_alert", tripId, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("code_email_alert", tripId, 
					CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isPnrOrTicketNumber");
			validator.addDirectCondition("email_email_alert", email, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("email_email_alert", email, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isEmail");
			validator.addDirectCondition("flight_number_email_alert", flight_number, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("checkin_depde_from_email", depdes, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("deptime_email_alert", deptime, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");		
			validator.addDirectCondition("action_email_alert", action, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("lang_email_alert", lang, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			if (action.equalsIgnoreCase("WebCheckin")){
				validator.addDirectCondition("wci_acc__email_alert", DCSextWciAcc, 
						CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			}
			if(action.equalsIgnoreCase("Unsubscrive")){
				validator.addDirectCondition("wci_del__email_alert", DCSextWciDel, 
						CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			}
			
			validator.addDirectCondition("wci_mail_alert_from_email", DCSextWciMailAlert, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
			validator.addDirectCondition("wci_lang__email_alert", DCSextWciMailLanguage, 
					CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		}
	


		ResultValidation resultValidation = validator.validate();
		if(!resultValidation.getResult()){
			logger.error("Error in validateForm");

		}
		return resultValidation;
	
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		try{
		String passengerName = request.getParameter("nome");
		String passengerSurname = request.getParameter("cognome");
		String tripId = request.getParameter("pnr");
		String MMcode = request.getParameter("mmcode");
		String pin = "";
		String email=request.getParameter("email");
		String flight_number=request.getParameter("flight");
		String depdes=request.getParameter("depdes");
		String deptime=request.getParameter("deptime");
		String arrdes=request.getParameter("arrdes");
		String arrtime=request.getParameter("arrtime");
		String action=request.getParameter("action");
		String lang=request.getParameter("lang");
		String DCSextWciAcc=request.getParameter("DCSext.wci_acc");
		String DCSextWciMailAlert=request.getParameter("DCSext.wci_mailalert");
		String DCSextWciMailLanguage=request.getParameter("DCSext.wci_language");
		String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		String currencyCode = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencyCode", String.class);
		String currencySymbol = AlitaliaUtils.getInheritedProperty(request.getResource(), "currencySymbol", String.class);
		Locale locale = AlitaliaUtils.getRepositoryPathLocale(request.getResource());
		String callerIp = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
		I18n i18n = new I18n(request.getResourceBundle(locale));
		String machineName = AlitaliaUtils.getLocalHostName("unknown");
		String searchType = "code"; //[login, code]
		/*Currency format info*/
		InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(request.getResource());
		String decimalSeparator=ivm.getInherited("decimalSeparator", String.class);
		String groupingSeparator=ivm.getInherited("groupingSeparator", String.class);
		Integer numDecDigits=ivm.getInherited("decimalDigit", Integer.class);
		if(numDecDigits == null){
			numDecDigits = 2;
		}
		
		NumberFormat currentNumberFormat = AlitaliaUtils.getNumberFormat(numDecDigits, decimalSeparator, groupingSeparator, market);
		HttpSession session = request.getSession(true);
		if (action.equalsIgnoreCase("WebCheckin")){
		CheckinSessionContext ctx = checkInSession.initializeSession(request, i18n, CheckinConstants.CHECKIN_CLIENT, machineName, 
				CheckinConstants.CHECKIN_SID, CheckinConstants.CHECKIN_SITE, market, currencyCode, currencySymbol, locale, 
				callerIp, tripId, passengerName, passengerSurname, MMcode, pin, searchType,currentNumberFormat, null);
		
		session.setAttribute(CheckinConstants.CHECKIN_CONTEXT_ATTRIBUTE, ctx);
		// Put parameters from query string of Alitalia email alert in the session 
		String redirectUrl = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCheckinFlightListPage();
		redirectUrl = request.getResourceResolver().map(redirectUrl);
			// for non-ajax requests, perform an HTTP redirection on successful search
			response.sendRedirect(redirectUrl);
		}else if(action.equalsIgnoreCase("Unsubscrive")){
			CheckinSessionDataEmailAlert dataEmailAlert= new CheckinSessionDataEmailAlert(passengerName, passengerSurname, email, MMcode, false);
			session.setAttribute(CheckinConstants.CHECKIN_DATA_EMAIL_ALERT, dataEmailAlert);
			
			String redirecturlUnsub=AlitaliaUtils.findSiteBaseExternalUrl(
					request.getResource(), false)+getConfiguration().getCheckinDeregistrationPage(true);
			redirecturlUnsub=request.getResourceResolver().map(redirecturlUnsub);
			response.sendRedirect(redirecturlUnsub);

		}else{
			response.sendRedirect(request.getResourceResolver().map(getSelfPageUrl(request))+"?invalidAction=true");
		}
		}catch(Exception e){
			logger.error("Error in CheckinEmailRedirectServet", e);
			response.sendRedirect(request.getResourceResolver().map(getSelfPageUrl(request)));
		}
		
	}
	
	/**
	 * This method returns a url to redirect an user to  check-in  failure
	 */
	protected String getSelfPageUrl(SlingHttpServletRequest request) {
		return AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getCheckinFailurePage();
	}

	
	

	/**
	 * For this servlet, overwrite the default MMB error page with the PNR search page.
	 * This provide correct user redirection when a wrong search is performed
	 * with an ajax request (e.g. from PNR search page itself).
	 */
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		HashMap<String, String> additionalParams =  new HashMap<String, String>();
		String url =  AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ getConfiguration().getCheckinHomePage() + ERROR_QSTRING;
		additionalParams.put("redirect", request.getResourceResolver().map(url));
		return additionalParams;
	}
	
	
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}
}