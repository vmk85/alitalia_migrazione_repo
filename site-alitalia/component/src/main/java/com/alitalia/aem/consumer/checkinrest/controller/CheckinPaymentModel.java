package com.alitalia.aem.consumer.checkinrest.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.initpayment.response.RedirectInfoFor3D;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinPaymentModel extends GenericCheckinModel {

	@Self
	private SlingHttpServletRequest request;

	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private RedirectInfoFor3D redirectInfoFor3D = null;
//	private String paRequest;
//	private String redirectUrl;
//	private String merchantData;
//	private String redirectHTML;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PostConstruct
	protected void initModel() throws Exception {
		
		try {
			super.initBaseModel(request);
			redirectInfoFor3D = new RedirectInfoFor3D();
			redirectInfoFor3D.setPaRequest(ctx.checkinInitPaymentResponse.getInitPaymentResponse().getRedirectInfoFor3D().getPaRequest());
			redirectInfoFor3D.setRedirectUrl(ctx.checkinInitPaymentResponse.getInitPaymentResponse().getRedirectInfoFor3D().getRedirectUrl());
			redirectInfoFor3D.setMerchantData(ctx.checkinInitPaymentResponse.getInitPaymentResponse().getRedirectInfoFor3D().getMerchantData());

			String html = ctx.checkinInitPaymentResponse.getInitPaymentResponse().getRedirectInfoFor3D().getRedirectHTML();

			if(html.contains("{servletAuthorize3dUrl}")) {


				String baseUrlForContext = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
				String baseUrlForRedirect = request.getResource().getResourceResolver().map(baseUrlForContext);
				String redirectUrl = ctx.domain + baseUrlForRedirect + "/homepage/.authorize3d.json";

				html = html.replace("{servletAuthorize3dUrl}", redirectUrl);

			}

			redirectInfoFor3D.setRedirectHTML(html);
			String modified = redirectInfoFor3D.getRedirectHTML().replace("\\", "").replace("<html>", "").replace("</html>", "").replace("<body>", "").replace("</body>", "");
			redirectInfoFor3D.setRedirectHTML(modified);
		}
		catch (Exception e) {
			logger.error("[CheckinPaymentModel][initModel] - errore nel pagamento 3Ds.");
			e.getMessage();
		}
	}

	public RedirectInfoFor3D getRedirectInfoFor3D() {
		return redirectInfoFor3D;
	}

	public void setRedirectInfoFor3D(RedirectInfoFor3D redirectInfoFor3D) {
		this.redirectInfoFor3D = redirectInfoFor3D;
	}
	
}
