package com.alitalia.aem.consumer.booking.servlet;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;

import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;

@Component(immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "bookingflightsearchserviceconsumer" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class BookingFlightSearchService extends GenericBookingFormValidatorServlet {
	
	@Reference
	private AlitaliaConfigurationHolder alitaliaConfiguration;
	
	@Reference
	private BookingSession bookingSession;
	
	private final String FRAUDNET_IP_HEADER = "X-Forwarded-For";
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		// no input fields, always valid
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws Exception {
		
		BookingSessionContext ctx = getBookingSessionContext(request);
		
		try {
			
			//INIZIO *** MODIFICA IP ***	
			String ipAddress = "";
			ipAddress = AlitaliaUtils.getRequestRemoteAddr(alitaliaConfiguration, request);
			if (ipAddress != null && !ipAddress.isEmpty()) {
				ctx.ipAddress = ipAddress;
			}
			//FINE.
		
			// imposta selezione continuita' territoriale
			String residency = request.getParameter("isResidency");
			if( (residency == null || ("").equals(residency)) || (!residency.equals("true") && !residency.equals("false")) ) {
				logger.error("Residency in request is not found");
				throw new IllegalArgumentException("Residency in request is not found");
			}
			boolean isResidency = Boolean.parseBoolean(residency);
			if (ctx.isAllowedContinuitaTerritoriale) {
				bookingSession.selectContinuitaTerritoriale(ctx, new Boolean(isResidency));
			}
			
			
			
			// esegue la ricerca
			String brandsPath = AlitaliaUtils.findSiteBaseRepositoryPath(request.getResource()) + AlitaliaConstants.BASE_BRAND_PATH;
			Resource rootResource = request.getResource().getResourceResolver().getResource(brandsPath);
			if (!ctx.isCarnetProcess) {
				bookingSession.performSearch(ctx,rootResource);
			} else {
				bookingSession.performCarnetSearch(ctx,rootResource);
			}
			if (ctx.availableFlights == null || ctx.availableFlights.getRoutes() == null) {
				throw new GenericFormValidatorServletException(false);
			}
			
			// produce i risultati
			String noSolutionsPage = request.getResource().getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
					+ alitaliaConfiguration.getBookingFlightSearchNoSolutionsPage());
			TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
			json.object();
			response.setContentType("application/json");
			if (ctx.cug == BookingSearchCUGEnum.YOUTH) {
				if (!ctx.youthSolutionFound) {
					json.key("solutionNotFound").value("youth");
				}
			}
			if (ctx.cug == BookingSearchCUGEnum.FAMILY) {
				if (!ctx.familySolutionFound) {
					json.key("solutionNotFound").value("family");
				}
			}
			if (ctx.cug == BookingSearchCUGEnum.MILITARY) {
				if (!ctx.militarySolutionFound) {
					json.key("solutionNotFound").value("military");
				}
			}
			
			if (ctx.isSelectedContinuitaTerritoriale != null && 
					ctx.isSelectedContinuitaTerritoriale &&
					!ctx.territorialContinuitySolutionFound) {
				json.key("solutionNotFound").value("territorialContinuity");
			}
			
			if (ctx.cug == BookingSearchCUGEnum.ADT || ctx.cug == BookingSearchCUGEnum.CARNET) {
				if (ctx.availableFlights.getRoutes().isEmpty()) {
					logger.info("availableFlights.getRoutes() is empty. searchElements: {}", ctx.searchElements);
					if (ctx.isCarnetProcess) {
						String noCarnetSolutions = request.getResource().getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
								+ alitaliaConfiguration.getCarnetHomeLoggedPage() + "?resultError=noFlights");
						json.key("solutionNotFound").value("allSolutions");
						json.key("redirect").value(noCarnetSolutions);
					} else {
						if (!bookingSession.existFlightsRibbon(ctx)) {
							json.key("solutionNotFound").value("allSolutions");
							json.key("redirect").value(noSolutionsPage);
						}
					}
					
				} else {
					for (RouteData routeData : ctx.availableFlights.getRoutes()) {
						if (routeData.getFlights().isEmpty()) {
							logger.info("RouteData {} .getFlights() is empty. searchElements: {}", routeData.getIndex(), ctx.searchElements);
							if (ctx.isCarnetProcess) {
								String noCarnetSolutions = request.getResource().getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
										+ alitaliaConfiguration.getCarnetHomeLoggedPage());
								json.key("solutionNotFound").value("allSolutions");
								json.key("redirect").value(noCarnetSolutions);
							} else {
								json.key("solutionNotFound").value("allSolutions");
								json.key("redirect").value(noSolutionsPage);
							}
							break;
						}
					}
				}
			}
			json.key("result").value("OK");
			json.key("roundtrip").value(ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP);
			json.key("type").value(ctx.cug.value());
			json.endObject();
			
		} catch (Exception e) {
			if (ctx == null) {
				logger.error("Error during flight search - details: ctx is null");
				TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
				json.object();
				response.setContentType("application/json");
				json.key("result").value("KO");
				json.key("isError").value("true");
				json.endObject();
			} else {
				logger.info("Error during flight search - details: {}", ctx.searchElements);
				throw e;
			}
			
		}
		
	}
	
	/**
	 * Return a map that contains all HTTP Headers present in the request.
	 * @param request
	 * @param separator used to concat in case of headers with multiple values
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Map<String, String> getHttpHeaders(SlingHttpServletRequest request, String separator) {
		// TODO Auto-generated method stub
		Map<String, String> httpHeaders = new HashMap<String,String>();
		Enumeration<String> headerNames = request.getHeaderNames();
		while(headerNames.hasMoreElements()){
			String headerName = headerNames.nextElement();
			if(headerName != null && headerName.length() > 0){
				Enumeration<String> headerValues = request.getHeaders(headerName);
				String headerValue = concatEnumeration(headerValues, separator);
				/*Insert into map*/
				httpHeaders.put(headerName, headerValue);
			}
		}
		return httpHeaders;
	}
	
	private String concatEnumeration(Enumeration<String> enumeration, String separator){
		String concat = "";
		while(enumeration.hasMoreElements()){
			String value = enumeration.nextElement();
			concat += value + separator;
		}
		/*remove trailing separator*/
		if(concat.length() > 0){
			concat = concat.substring(0, concat.lastIndexOf(separator));
		}
		return concat;
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return alitaliaConfiguration;
	}
	
}
