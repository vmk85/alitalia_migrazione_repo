package com.alitalia.aem.consumer.checkin;

import java.util.Map;

import com.alitalia.aem.common.data.home.enumerations.CheckinPassengerTypeEnum;

public class CheckinPassenger {
	
	private int index;
	private boolean checkin;
	private boolean alreadyCheckedIn;
	private String name;
	private String lastName;
	private String frequentFlyerCode;
	private String frequentFlyerType;
	private CheckinPassengerTypeEnum type;
	
	
	private boolean hasInfant;
	private boolean child;
	
	/* Campi APIS Basic */
	private String nationality;
	private String birthDay;
	private String birthMonth;
	private String birthYear;
	private String gender;
	private String documentType;
	private String passport;
	private String passportExpiryDay;
	private String passportExpiryMonth;
	private String passportExpiryYear;
	
	/* Campi APIS PLUS */
	private String residency;
	private String middleName;
	
	/* Campi APIS PLUS additional info */
	private String destinationState;
	private String destinationAddress;
	private String destinationCity;
	private String destinationZipCode;
	private String extraDocumentType;
	private String extraDocumentNumber;
	private String extraDocumentExpiryDay;
	private String extraDocumentExpiryMonth;
	private String extraDocumentExpiryYear;
	private String extraDocumentDeliveryCountry;
	
	/* Campi boarding pass */
	private boolean cleared;
	
	/* Campi seat map */
	private Map<String, CheckinPassengerSeat> seats;
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}
	
	public boolean isCheckin() {
		return checkin;
	}

	public void setCheckin(boolean checkin) {
		this.checkin = checkin;
	}
	
	public boolean isAlreadyCheckedIn() {
		return alreadyCheckedIn;
	}

	public void setAlreadyCheckedIn(boolean alreadyCheckedIn) {
		this.alreadyCheckedIn = alreadyCheckedIn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getFrequentFlyerCode() {
		return frequentFlyerCode;
	}
	
	public void setFrequentFlyerCode(String frequentFlyerCode) {
		this.frequentFlyerCode = frequentFlyerCode;
	}

	public String getFrequentFlyerType() {
		return frequentFlyerType;
	}

	public void setFrequentFlyerType(String frequentFlyerType) {
		this.frequentFlyerType = frequentFlyerType;
	}

	public CheckinPassengerTypeEnum getType() {
		return type;
	}

	public void setType(CheckinPassengerTypeEnum type) {
		this.type = type;
	}

	public boolean isHasInfant() {
		return hasInfant;
	}

	public void setHasInfant(boolean hasInfant) {
		this.hasInfant = hasInfant;
	}

	public boolean isChild() {
		return child;
	}

	public void setChild(boolean child) {
		this.child = child;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getBirthMonth() {
		return birthMonth;
	}

	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}

	public String getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getPassportExpiryDay() {
		return passportExpiryDay;
	}

	public void setPassportExpiryDay(String passportExpiryDay) {
		this.passportExpiryDay = passportExpiryDay;
	}

	public String getPassportExpiryMonth() {
		return passportExpiryMonth;
	}

	public void setPassportExpiryMonth(String passportExpiryMonth) {
		this.passportExpiryMonth = passportExpiryMonth;
	}

	public String getPassportExpiryYear() {
		return passportExpiryYear;
	}

	public void setPassportExpiryYear(String passportExpiryYear) {
		this.passportExpiryYear = passportExpiryYear;
	}

	public String getResidency() {
		return residency;
	}

	public void setResidency(String residency) {
		this.residency = residency;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getDestinationState() {
		return destinationState;
	}

	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}

	public String getDestinationAddress() {
		return destinationAddress;
	}

	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public String getDestinationZipCode() {
		return destinationZipCode;
	}

	public void setDestinationZipCode(String destinationZipCode) {
		this.destinationZipCode = destinationZipCode;
	}
	
	public String getExtraDocumentType() {
		return extraDocumentType;
	}

	public void setExtraDocumentType(String extraDocumentType) {
		this.extraDocumentType = extraDocumentType;
	}

	public String getExtraDocumentNumber() {
		return extraDocumentNumber;
	}

	public void setExtraDocumentNumber(String extraDocumentNumber) {
		this.extraDocumentNumber = extraDocumentNumber;
	}

	public String getExtraDocumentExpiryDay() {
		return extraDocumentExpiryDay;
	}

	public void setExtraDocumentExpiryDay(String extraDocumentExpiryDay) {
		this.extraDocumentExpiryDay = extraDocumentExpiryDay;
	}

	public String getExtraDocumentExpiryMonth() {
		return extraDocumentExpiryMonth;
	}

	public void setExtraDocumentExpiryMonth(String extraDocumentExpiryMonth) {
		this.extraDocumentExpiryMonth = extraDocumentExpiryMonth;
	}

	public String getExtraDocumentExpiryYear() {
		return extraDocumentExpiryYear;
	}

	public void setExtraDocumentExpiryYear(String extraDocumentExpiryYear) {
		this.extraDocumentExpiryYear = extraDocumentExpiryYear;
	}

	public String getExtraDocumentDeliveryCountry() {
		return extraDocumentDeliveryCountry;
	}

	public void setExtraDocumentDeliveryCountry(String extraDocumentDeliveryCountry) {
		this.extraDocumentDeliveryCountry = extraDocumentDeliveryCountry;
	}

	public boolean isCleared() {
		return cleared;
	}

	public void setCleared(boolean cleared) {
		this.cleared = cleared;
	}

	public Map<String, CheckinPassengerSeat> getSeats() {
		return seats;
	}

	public void setSeats(Map<String, CheckinPassengerSeat> seats) {
		this.seats = seats;
	}

	@Override
	public String toString() {
		return "CheckinPassenger [index=" + index + ", checkin=" + checkin
				+ ", alreadyCheckedIn=" + alreadyCheckedIn + ", name=" + name
				+ ", lastName=" + lastName + ", frequentFlyerCode="
				+ frequentFlyerCode + ", frequentFlyerType="
				+ frequentFlyerType + ", type=" + type + ", hasInfant="
				+ hasInfant + ", child=" + child + ", nationality="
				+ nationality + ", birthDay=" + birthDay + ", birthMonth="
				+ birthMonth + ", birthYear=" + birthYear + ", gender="
				+ gender + ", documentType=" + documentType + ", passport="
				+ passport + ", passportExpiryDay=" + passportExpiryDay
				+ ", passportExpiryMonth=" + passportExpiryMonth
				+ ", passportExpiryYear=" + passportExpiryYear + ", residency="
				+ residency + ", middleName=" + middleName
				+ ", destinationState=" + destinationState
				+ ", destinationAddress=" + destinationAddress
				+ ", destinationCity=" + destinationCity
				+ ", destinationZipCode=" + destinationZipCode
				+ ", extraDocumentType=" + extraDocumentType
				+ ", extraDocumentNumber=" + extraDocumentNumber
				+ ", extraDocumentExpiryDay=" + extraDocumentExpiryDay
				+ ", extraDocumentExpiryMonth=" + extraDocumentExpiryMonth
				+ ", extraDocumentExpiryYear=" + extraDocumentExpiryYear
				+ ", extraDocumentDeliveryCountry="
				+ extraDocumentDeliveryCountry + ", cleared=" + cleared
				+ ", seats=" + seats + "]";
	}
}
