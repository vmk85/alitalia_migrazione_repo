package com.alitalia.aem.consumer.mmb;

public class MmbConstants {

	public static final String MMB_CONTEXT_ATTRIBUTE = "alitaliaConsumerMmbSessionContext";

	public static final String MMB_SID = "alitaliaConsumerMmb";
	public static final String MMB_CLIENT = "alitaliaConsumerSiteMmb";
	public static final String MMB_SITE = "site";

	public static final String MESSAGE_GENERIC_EMPTY_FIELD = "mmb.common.genericEmptyField.label";
	public static final String MESSAGE_GENERIC_INVALID_FIELD = "mmb.common.genericInvalidField.label";
	public static final String MESSAGE_GENERIC_CHECK_FIELD = "mmb.common.genericUncheckedField.{0}.label";

	public static final String MESSAGE_ERROR_INVALID_DATA = "mmb.common.invalidData.label";
	public static final String MESSAGE_ERROR_PNR_NOT_FOUND = "mmb.common.pnrNotFound.label";
	public static final String MESSAGE_ERROR_MODIFY_PASSENGER_DATA = "mmb.common.modifyPassengerDataError.label";
	public static final String MESSAGE_ERROR_INVALID_ANCILLARY_ID = "mmb.common.invalidAncillaryId.label";
	public static final String MESSAGE_ERROR_INVALID_ANCILLARY_STATUS = "mmb.common.invalidAncillaryStatus.label";
	public static final String MESSAGE_ERROR_PAYMENT_WRONG_AMOUNT = "mmb.common.paymentWrongAmount.label";
	public static final String MESSAGE_ERROR_ANCILLARY_PAYMENT = "mmb.common.ancillaryPaymentFail.label";
	public static final String MESSAGE_ERROR_RECOVERABLE_ANCILLARY_PAYMENT = "mmb.common.ancillaryPaymentRecoverableFail.label";
	public static final String MESSAGE_ERROR_INSURANCE_PAYMENT = "mmb.common.insurancePaymentFail.label";
	
	public static final String MESSAGE_ERROR_MODIFY_PASSENGER_FREQUENT_FLYER = "mmb.common.modifyPassengerFrequentFlyerError.label";
	public static final String MESSAGE_ERROR_MODIFY_CONTACTS = "mmb.common.modifyPassengerContactsError.label";
	public static final String MESSAGE_ERROR_ASK_FOR_INVOICE = "mmb.common.askForInvoiceError.label";

	public static final String MESSAGE_SUCCESS_MODIFY_PASSENGER_DATA = "mmb.common.modifyPassengerDataSuccess.label";
	public static final String MESSAGE_SUCCESS_MODIFY_FREQUENT_FLYER_INFO = "mmb.common.modifyFrequentFlyerInfoSuccess.label";
	public static final String MESSAGE_SUCCESS_MODIFY_CONTACTS = "mmb.common.modifyContactsSuccess.label";
	public static final String MESSAGE_SUCCESS_ASK_FOR_INVOCE = "mmb.common.askForInvoiceSuccess.label";
	
	public static final String CART_SEAT_TYPE_LABEL = "mmb.cartSeatType.{0}.label";
	public static final String CART_MEAL_TYPE_LABEL = "mealsData.{0}";
	public static final String CART_EXTRABAGGAGE_TYPE_LABEL = "mmb.cart.extraBaggage.label";
	public static final String CART_EXTRABAGGAGE_TYPE_LABEL_PLURAL = "mmb.cart.extraBaggage.label.plural";
	public static final String CART_FASTTRACK_TYPE_LABEL = "mmb.cart.fastTrack.label";
	public static final String CART_FASTTRACK_TYPE_LABEL_PLURAL = "mmb.cart.fastTrack.label.plural";
	public static final String CART_LOUNGE_TYPE_LABEL = "mmb.cart.lounge.label";
	public static final String CART_LOUNGE_TYPE_LABEL_PLURAL = "mmb.cart.lounge.label.plural";
	
	public static final String LABEL_TYPE_TRATTA = "mmb.common.tratta.label";
	public static final String LABEL_TYPE_ANDATA = "mmb.common.andata.label";
	public static final String LABEL_TYPE_RITORNO = "mmb.common.ritorno.label";
	
}
