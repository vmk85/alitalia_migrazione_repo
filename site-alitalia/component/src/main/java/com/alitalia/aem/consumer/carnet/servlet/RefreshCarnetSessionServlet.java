package com.alitalia.aem.consumer.carnet.servlet;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.carnet.CarnetConstants;
import com.alitalia.aem.consumer.carnet.CarnetSession;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "refreshcarnetsession" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET"}),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class RefreshCarnetSessionServlet extends GenericCarnetFormValidatorServlet {

	@Reference
	private volatile CarnetSession carnetSession;
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		Validator validator = new Validator();
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
		logger.debug("Refresh carnet session");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		json.object();
		
		String hpCarnetPage = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCarnetLoginPage();
		hpCarnetPage = request.getResourceResolver().map(hpCarnetPage);
		
		BookingSessionContext bookingCtx = getBookingSessionContext(request);
		if(bookingCtx.isCarnetProcess){
			CarnetSessionContext carnetCtx = getCarnetSessionContext(request);
			if(bookingCtx.infoCarnet != null){
				carnetCtx.infoCarnet = bookingCtx.infoCarnet;
				json.key("success").value("session updated");
			} else{
				json.key("redirect").value(hpCarnetPage);
			}
		}
		json.endObject();
	}

}
