package com.alitalia.aem.consumer.booking.analytics.filler;

import com.alitalia.aem.consumer.booking.analytics.AnalyticsInfo;

public interface AnalyticsInfoToJsObj {
	

	public String toJSObj(AnalyticsInfo info, boolean trailingSemicolon);
	
}
