package com.alitalia.aem.consumer.checkinrest.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerSeat;
import com.alitalia.aem.consumer.checkinrest.render.*;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.request.ListItemsFareAvailQualifier;
import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.EnhancedSeatMapResp;
import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.Seat;
import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.SeatMapColumn;
import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.SeatMapRow;
import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.SeatMapSector;
import com.alitalia.aem.common.messages.checkinrest.CheckinEnhancedSeatMapRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinEnhancedSeatMapResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;
import com.alitalia.aem.consumer.checkinrest.enumeration.CheckinRestTypeSeatEnum;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;

@Model(adaptables = {SlingHttpServletRequest.class})
public class CheckinESeatMap extends GenericCheckinModel {

    @Self
    private SlingHttpServletRequest request;

    @Inject
    private AlitaliaConfigurationHolder configuration;

    @Inject
    private CheckinSession checkinSession;

    @Inject
    private volatile ICheckinDelegate checkInDelegateRest;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private List<SeatMapRender> listSeatMapRender;

    private ArrayList<ArrayList> matrixFlightsSectorRender = null;


    @PostConstruct
    protected void initModel() throws Exception {

        try {
            super.initBaseModel(request);

            listSeatMapRender = new ArrayList<SeatMapRender>();

            AllSeatMaps(); //Viene effettuata la chiamata al delegate per tutti segmenti di ogni volo.

            adjustPrice();

            GraphicModelSeatMaps(); //Viene costruito il Model grafico.

        } catch (Exception e) {
            logger.error("[CheckinESeatMap][initModel] - errore nella SeatMap.");
            e.getMessage();
        }
    }

    private void AllSeatMaps() {

        try {

            for (int sf = 0; sf < ctx.selectedFlights.size(); sf++) {
                for (int sfs = 0; sfs < ctx.selectedFlights.get(sf).getSegments().size(); sfs++) {
                    if(ctx.selectedFlights.get(sf).getSegments().get(sfs).getPassengers() != null){

                        SeatMapRender seatMapRender = new SeatMapRender();
                        FlightsSectorRender flightsSectorRender = new FlightsSectorRender();

                        CheckinEnhancedSeatMapResponse checkinEnhancedSeatMapresponse = new CheckinEnhancedSeatMapResponse(IDFactory.getTid(), IDFactory.getSid(request));
                        CheckinEnhancedSeatMapRequest checkinEnhancedSeatMapRequest = new CheckinEnhancedSeatMapRequest(IDFactory.getTid(), IDFactory.getSid(request));
                        List<ListItemsFareAvailQualifier> listOfPassengerNames = new ArrayList<ListItemsFareAvailQualifier>();

                        checkinEnhancedSeatMapRequest.setLanguage(ctx.language);
                        checkinEnhancedSeatMapRequest.setMarket(ctx.market);
                        checkinEnhancedSeatMapRequest.setConversationID(ctx.conversationId);
                        checkinEnhancedSeatMapRequest.setFlight(ctx.selectedFlights.get(sf).getSegments().get(sfs).getFlight());
                        checkinEnhancedSeatMapRequest.setBookingClass(ctx.selectedFlights.get(sf).getSegments().get(sfs).getPassengers().get(0).getBookingClass());
                        checkinEnhancedSeatMapRequest.setOrigin(ctx.selectedFlights.get(sf).getSegments().get(sfs).getOrigin().getCode());
                        checkinEnhancedSeatMapRequest.setDestination(ctx.selectedFlights.get(sf).getSegments().get(sfs).getDestination().getCode());
                        checkinEnhancedSeatMapRequest.setDepartureDate(ctx.selectedFlights.get(sf).getSegments().get(sfs).getOriginalDepartureDate());
                        checkinEnhancedSeatMapRequest.setAirline(ctx.selectedFlights.get(sf).getSegments().get(sfs).getAirline());
                        checkinEnhancedSeatMapRequest.setPnr(ctx.pnr);

                        //bisogna verifcare se i passeggeri del volo selezionato sono quelli selezionati nella pagina di checkin-lista-pax-1.html.
                        for (int p = 0; p < ctx.selectedFlights.get(sf).getSegments().get(sfs).getPassengers().size(); p++) {
                            for (int sp = 0; sp < ctx.checkinSelectedPassengers.size(); sp++) {
                                //if (ctx.selectedFlights.get(sf).getSegments().get(sfs).getPassengers().get(p).getPassengerID().equals(ctx.checkinSelectedPassengers.get(sp).getPassengerID())){
                                String nomeCompletoBySelectedFlight = ctx.selectedFlights.get(sf).getSegments().get(sfs).getPassengers().get(p).getCognome() + ctx.selectedFlights.get(sf).getSegments().get(sfs).getPassengers().get(p).getNome();
                                String nomeCompletoBycheckinSelectedPassengers = ctx.checkinSelectedPassengers.get(sp).getCognome() + ctx.checkinSelectedPassengers.get(sp).getNome();
                                if (nomeCompletoBySelectedFlight.equals(nomeCompletoBycheckinSelectedPassengers)) {
                                    ListItemsFareAvailQualifier listItemsFareAvailQualifier = new ListItemsFareAvailQualifier();
                                    listItemsFareAvailQualifier.setGivenName(ctx.checkinSelectedPassengers.get(sp).getNome());
                                    listItemsFareAvailQualifier.setSurname(ctx.checkinSelectedPassengers.get(sp).getCognome());
                                    listOfPassengerNames.add(listItemsFareAvailQualifier);
                                }
                            }
                        }

                        // Aggiunto i passeggeri extra
                        for (int k = 0; k < ctx.checkinSelectedPassengersExtra.size(); k++) {
                            ListItemsFareAvailQualifier listItemsFareAvailQualifier = new ListItemsFareAvailQualifier();
                            listItemsFareAvailQualifier.setGivenName(ctx.checkinSelectedPassengersExtra.get(k).getPassenger().getNome());
                            listItemsFareAvailQualifier.setSurname(ctx.checkinSelectedPassengersExtra.get(k).getPassenger().getCognome());
                            listOfPassengerNames.add(listItemsFareAvailQualifier);
                        }

                        checkinEnhancedSeatMapRequest.setListItemsFareAvailQualifiers(listOfPassengerNames);

                        checkinEnhancedSeatMapresponse = checkInDelegateRest.enhancedSeatMap(checkinEnhancedSeatMapRequest);

                        if (checkinEnhancedSeatMapresponse != null) {
                            if (checkinEnhancedSeatMapresponse.get_enhancedseatmapResp() != null) {
                                if (checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getError() != null && !checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getError().equals("")) {
                                // todo managementError();
                                } else {
                                    if (checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getConversationID() != null) {
                                        ctx.conversationId = checkinEnhancedSeatMapresponse.get_enhancedseatmapResp().getConversationID();
                                    }
                                }
                            }
                        }
                        flightsSectorRender.setAirline(ctx.selectedFlights.get(sf).getSegments().get(sfs).getAirline());
                        flightsSectorRender.setFlight(ctx.selectedFlights.get(sf).getSegments().get(sfs).getFlight());

                        flightsSectorRender.setFlightStatus("");
                        flightsSectorRender.setCheckInGate(ctx.selectedFlights.get(sf).getSegments().get(sfs).getCheckInGate());
                        flightsSectorRender.setOrigin(ctx.selectedFlights.get(sf).getSegments().get(sfs).getOrigin().getCode());
                        flightsSectorRender.setDestination(ctx.selectedFlights.get(sf).getSegments().get(sfs).getDestination().getCode());
                        flightsSectorRender.setDepartureDate(ctx.selectedFlights.get(sf).getSegments().get(sfs).getOriginalDepartureDate());
                        flightsSectorRender.setDescriptionAirBus(ctx.selectedFlights.get(sf).getSegments().get(sfs).getAircraft());

                        seatMapRender.setFlightsSectorRender(flightsSectorRender);
                        seatMapRender.setEnhancedSeatMapResp(checkinEnhancedSeatMapresponse.get_enhancedseatmapResp());

                        this.listSeatMapRender.add(seatMapRender);//così possiamo sapere quante SeatMap verranno costruite

                    } else {
                        SeatMapRender seatMapRender = new SeatMapRender();
                        FlightsSectorRender flightsSectorRender = new FlightsSectorRender();

                        flightsSectorRender.setAirline(ctx.selectedFlights.get(sf).getSegments().get(sfs).getAirline());
                        flightsSectorRender.setFlight(ctx.selectedFlights.get(sf).getSegments().get(sfs).getFlight());

                        flightsSectorRender.setFlightStatus("");
                        flightsSectorRender.setCheckInGate(ctx.selectedFlights.get(sf).getSegments().get(sfs).getCheckInGate());
                        flightsSectorRender.setOrigin(ctx.selectedFlights.get(sf).getSegments().get(sfs).getOrigin().getCode());
                        flightsSectorRender.setDestination(ctx.selectedFlights.get(sf).getSegments().get(sfs).getDestination().getCode());
                        flightsSectorRender.setDepartureDate(ctx.selectedFlights.get(sf).getSegments().get(sfs).getOriginalDepartureDate());
                        flightsSectorRender.setDescriptionAirBus(ctx.selectedFlights.get(sf).getSegments().get(sfs).getAircraft());

                        seatMapRender.setFlightsSectorRender(flightsSectorRender);

                        this.listSeatMapRender.add(seatMapRender);//così possiamo sapere quante SeatMap verranno costruite
                    }
                }
            }
        } catch (Exception e) {
            logger.error("[CheckinESeatMap][AllSeatMaps] - errore nella chiamata al Delegate da CheckinESeatMap");
            e.getMessage();
        }
    }

    private void GraphicModelSeatMaps() {

        try {
            //Questo ciclo costituisce il Model grafico.
            //Viene Costruita una matrice di settori riguardante ogni volo, segmenti di quel volo e passeggeri
            matrixFlightsSectorRender = new ArrayList<>();

            for (int e = 0; e < listSeatMapRender.size(); e++) {

                ArrayList<FlightsSectorRender> listFlightsSectorRender = new ArrayList<>();

                FlightsSectorRender flightsSectorRender = new FlightsSectorRender();

                flightsSectorRender = listSeatMapRender.get(e).getFlightsSectorRender();

                if(listSeatMapRender.get(e).getEnhancedSeatMapResp() != null ){

                    String structureNumericMap = structureMapSeats(listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapColumns());

                    ArrayList<PassengerRender> listPassengerRender = new ArrayList<>();

                    //setta i posti preassegnati in fase di check-in ciclando la response della enhanced seatmap ed assegnandoli ai ctx.checkinSelectedPassengers
                    setAssignedSeatToCtxPassengers(listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapSectors(), flightsSectorRender.getOrigin(), flightsSectorRender.getDepartureDate());
                    if (ctx.checkinSelectedPassengers != null) {

                        //if(result){//TEST
                        //for (int p = 0; p < 1; p++){ //TEST
                        for (int p = 0; p < ctx.checkinSelectedPassengers.size(); p++) {

                            PassengerRender passengerRender = new PassengerRender();

                            passengerRender.setNome(ctx.checkinSelectedPassengers.get(p).getNome().trim());
                            passengerRender.setCognome(ctx.checkinSelectedPassengers.get(p).getCognome().trim());
                            passengerRender.setPassengerId(ctx.checkinSelectedPassengers.get(p).getPassengerID());

                            if (ctx.checkinSelectedPassengers.get(p).getFrequentFlyer() != null && !ctx.checkinSelectedPassengers.get(p).getFrequentFlyer().isEmpty()) {
                                passengerRender.setFrequentFlyer(ctx.checkinSelectedPassengers.get(p).getFrequentFlyer().get(0).getDescription());
                                passengerRender.setCodeFrequentFlyer(ctx.checkinSelectedPassengers.get(p).getFrequentFlyer().get(0).getCode());
                                passengerRender.setNumberFrequentFlyer(ctx.checkinSelectedPassengers.get(p).getFrequentFlyer().get(0).getNumber());
                                passengerRender.setTierLevel(ctx.checkinSelectedPassengers.get(p).getTierLevel());
                            }
                            //passengerRender.setFrequentFlyer(ctx.checkinSelectedPassengers.get(p).getFrequentFlyer().get(0).getDescription()); //index out of bound se non e' frequent flyer
                            //passengerRender.setCodeFrequentFlyer(ctx.checkinSelectedPassengers.get(p).getFrequentFlyer().get(0).getCode()); //index out of bound se non e' frequent flyer

                            passengerRender.setPostoAssegnato(ctx.checkinSelectedPassengers.get(p).getPostoAssegnato());

                            String numeroPosto = "";
                            // Cerco il posto assegnato
                            for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerSeat seats : ctx.checkinSelectedPassengers.get(p).getPassengerSeats()) {
                                if (seats.getOrigin().equals(flightsSectorRender.getOrigin())) {
                                    numeroPosto = seats.getNumeroPostoAssegnato();
                                }
                            }
                            passengerRender.setSeatCI(numeroPosto);
                            // passengerRender.setSeatCI(ctx.checkinSelectedPassengers.get(p).getNumeroPostoAssegnato());

                            passengerRender.setBookingClass(ctx.checkinSelectedPassengers.get(p).getBookingClass());
                            passengerRender.setPassengerType(ctx.checkinSelectedPassengers.get(p).getPassengerType());

                            if (ctx.checkinSelectedPassengers.get(p).getInfantFirstName() != null) {
                                passengerRender.setInfantFirstName(ctx.checkinSelectedPassengers.get(p).getInfantFirstName());
                            }

                            if (ctx.checkinSelectedPassengers.get(p).getInfantLastName() != null) {
                                passengerRender.setInfantLastName(ctx.checkinSelectedPassengers.get(p).getInfantLastName());
                            }

                            passengerRender.setPassengerSeats(ctx.checkinSelectedPassengers.get(p).getPassengerSeats());

                            int x1 = 0;
                            // Aggiorno il posto sul passeggero
                            for (PnrRender pnrSelectedListDataRender : ctx.pnrSelectedListDataRender) {

                                int x2 = 0;
                                for (FlightsRender flight : pnrSelectedListDataRender.getFlightsRender()) {

                                    int x3 = 0;
                                    for (SegmentRender segment : flight.getSegments()) {

                                        if (segment.getOrigin().getCode().equals(flightsSectorRender.getOrigin())) {
                                            // Ciclo sui passeggeri
                                            int x4 = 0;
                                            for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passengerX : segment.getPassengers()) {

                                                if (passengerX.getNome().equals(passengerRender.getNome()) && passengerX.getCognome().equals(passengerRender.getCognome()))
                                                    ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengers().get(x4).setPassengerSeats(ctx.checkinSelectedPassengers.get(p).getPassengerSeats());

                                                x4 += 1;
                                            }
                                        }
                                        x3 += 1;

                                    }
                                    x2 += 1;
                                }
                                x1 += 1;
                            }

                            //passengerRender.setNumberFrequentFlyer(ctx.checkinSelectedPassengers.get(p).getFrequentFlyer().get(0).getNumber()); //index out of bound se non e' frequent flyer
                            passengerRender.setNewSeat("");

                            List<SeatMapSector> listSeatMapSector = new ArrayList<>();

                            listSeatMapSector = startOrEndRow_13_17(listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapSectors());

                            int sectors = listSeatMapSector.size();

                            ArrayList<SectorRender> listSectorsRender = new ArrayList<>();
                            for (int i = 0; i < sectors; i++) {

                                SectorRender sectorRender = new SectorRender();
                                int sectorsStartRow = Integer.parseInt(listSeatMapSector.get(i).getStartingRow());
                                int sectorsEndingRow = Integer.parseInt(listSeatMapSector.get(i).getEndingRow());
                                int prevSectorsStartRow = 0;
                                int prevSectorsEndingRow = 0;
                                int nextSectorsStartRow = 0;
                                int nextSectorsEndingRow = 0;

                                if (i > 0) {
                                    prevSectorsStartRow = Integer.parseInt(listSeatMapSector.get(i - 1).getStartingRow());
                                    prevSectorsEndingRow = Integer.parseInt(listSeatMapSector.get(i - 1).getEndingRow());
                                }

                                if (i < sectors - 1) {
                                    nextSectorsStartRow = Integer.parseInt(listSeatMapSector.get(i + 1).getStartingRow());
                                    nextSectorsEndingRow = Integer.parseInt(listSeatMapSector.get(i + 1).getEndingRow());
                                }

                                sectorRender.setStartRow(String.valueOf(sectorsStartRow));
                                sectorRender.setEndRow(String.valueOf(sectorsEndingRow));
                                sectorRender.setPrevStartRow(String.valueOf(prevSectorsStartRow));
                                sectorRender.setPrevEndRow(String.valueOf(prevSectorsEndingRow));
                                sectorRender.setNextStartRow(String.valueOf(nextSectorsStartRow));
                                sectorRender.setNextEndRow(String.valueOf(nextSectorsEndingRow));
                                sectorRender.setStructureMap(structureNumericMap);

                                // ***** START : THEAD per AEM Seat Map *****
                                ArrayList<THead> listTHead = new ArrayList<>();
                                String[] items = structureNumericMap.split("_");
                                int aisles = items.length - 1; //corridoi

                                //exitEmergency1
                                THead theadEmergency1 = new THead();
                                theadEmergency1.setColumn("SC");
                                theadEmergency1.setTypeCharacteristics("Exit Emergency");
                                listTHead.add(theadEmergency1);

                                //corpo
                                int cont1 = 0;
                                int cont2 = 0;
                                for (int u = 0; u < items.length; u++) {
                                    cont1 = cont1 + Integer.parseInt(items[u]);

                                    for (int c = cont2; c < cont1; c++) {
                                        THead theadCorpo = new THead();
                                        theadCorpo.setColumn(listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapColumns().get(c).getColumn());
                                        theadCorpo.setTypeCharacteristics(listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapColumns().get(c).getTypeCharacteristics());
                                        listTHead.add(theadCorpo);
                                        cont2 = cont1;
                                    }

                                    if (u < aisles) {
                                        THead theadAisles = new THead();
                                        theadAisles.setColumn("SC");
                                        theadAisles.setTypeCharacteristics("");
                                        listTHead.add(theadAisles);
                                    }
                                }

                                //exitEmergency1
                                THead theadEmergency2 = new THead();
                                theadEmergency2.setColumn("SC");
                                theadEmergency2.setTypeCharacteristics("Exit Emergency");
                                listTHead.add(theadEmergency2);

                                sectorRender.setThead(listTHead);

                                // ***** END : THEAD per AEM Seat Map *****

                                // ***** START : TBODY per AEM Seat Map *****
                                ArrayList<ArrayList> listTBody = new ArrayList<>();


                                int struttura = listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapColumns().size();
                                for (int s = 0; s < listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().size(); s++) {
                                    ArrayList<SeatRender> listSeatsRender = new ArrayList<>(); //questo array deve essere inizializzato ad ogni giro//
                                    String rowNumber = "";
                                    rowNumber = listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getRowNumber();

                                    List<SeatRender> listSeatRenderOccupiedForPassenger = new ArrayList<>();
                                    listSeatRenderOccupiedForPassenger = seatOccupiedForPassenger(listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats(), passengerRender.getPassengerId());

                                    //exitEmergency1
                                    SeatRender seatEmergency1 = new SeatRender();
                                    seatEmergency1 = seatSC("", "", "", "", false, "SC", "");
                                    listSeatsRender.add(seatEmergency1);
//                                logger.info("[CheckinESeatMap] [GraphicModelSeatMaps] <---> EXIT-EMERGENCY-1");

                                    //corpo
                                    int c1 = 0;
                                    int c2 = 0;
                                    for (int u = 0; u < items.length; u++) {
                                        c1 = c1 + Integer.parseInt(items[u]);

                                        for (int c = c2; c < c1; c++) {
                                            String passengerId = "";
                                            passengerId = listSeatRenderOccupiedForPassenger.get(c).getPassengerId().trim();
//                                        logger.info("[CheckinESeatMap] [GraphicModelSeatMaps] <---> PassengerID del Servizio EnanchedSeatMap : " + passengerId + " - PassengerID che ha effettuato il Check-in " + passengerRender.getPassengerId());
                                            //TODO : inserire l'infant
                                            if (passengerId.equals(passengerRender.getPassengerId())) {
//                                            logger.info("[CheckinESeatMap] [GraphicModelSeatMaps] <---> Management SeatBody Passeggeri Verificati.");
                                                SeatRender seatBody = new SeatRender();
                                                seatBody.setRow(rowNumber);
                                                seatBody.setColumn(listSeatRenderOccupiedForPassenger.get(c).getNumber());
                                                seatBody.setNumber(listSeatRenderOccupiedForPassenger.get(c).getNumber() + rowNumber);
                                                seatBody.setPassengerId(listSeatRenderOccupiedForPassenger.get(c).getPassengerId());
                                                seatBody.setOccupatoDa(listSeatRenderOccupiedForPassenger.get(c).getOccupatoDa());
                                                //seatBody.setOccupatoDa(listSeatRenderOccupiedForPassenger.get(c).getOccupatoDa());
                                                seatBody.setPrice(String.valueOf(listSeatRenderOccupiedForPassenger.get(c).getPrice()));
                                                seatBody.setConfortSeat(listSeatRenderOccupiedForPassenger.get(c).getConfortSeat());
                                                seatBody.setType(listSeatRenderOccupiedForPassenger.get(c).getType());
                                                seatBody.setAvailability(listSeatRenderOccupiedForPassenger.get(c).getAvailability());
                                                seatBody.setCssClass(setCssClass(seatBody, passengerRender, flightsSectorRender.getOrigin()));
                                                listSeatsRender.add(seatBody);
                                                c2 = c1;
                                            }
                                        }

                                        if (u < aisles) {
                                            SeatRender seatAisles = new SeatRender();
                                            seatAisles = seatSC("", "", "", "", false, "SC", "");
                                            listSeatsRender.add(seatAisles);
                                        }
                                    }

                                    //exitEmergency2
                                    SeatRender seatEmergency2 = new SeatRender();
                                    seatEmergency2 = seatSC("", "", "", "", false, "SC", "");
                                    listSeatsRender.add(seatEmergency2);
//                                logger.info("[CheckinESeatMap] [GraphicModelSeatMaps] <---> EXIT-EMERGENCY-2");

                                    listTBody.add(s, listSeatsRender);
                                }

                                // ***** END : TBODY per AEM Seat Map *****
                                sectorRender.setTBody(listTBody);
                                listSectorsRender.add(sectorRender);

                            }

                            passengerRender.setListSectorRender(listSectorsRender);
                            listPassengerRender.add(passengerRender);
                        }
                    }

                    flightsSectorRender.setPassengerRender(listPassengerRender);

                    // Ciclo sui passeggeri extra
                    ArrayList<PassengerRender> listPassengerRenderExtra = new ArrayList<>();

                    if (ctx.checkinSelectedPassengersExtra != null) {

                        //if(result){//TEST
                        //for (int p = 0; p < 1; p++){ //TEST
                        for (int p = 0; p < ctx.checkinSelectedPassengersExtra.size(); p++) {

                            // Controllo se il passeggero è del segmento
                            //if (ctx.checkinSelectedPassengersExtra.get(p).get)

                            PassengerRender passengerRender = new PassengerRender();

                            passengerRender.setNome(ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getNome().trim());
                            passengerRender.setCognome(ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getCognome().trim());
                            passengerRender.setPassengerId(ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getPassengerID());
                            passengerRender.setPostoAssegnato(ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getPostoAssegnato());
                            passengerRender.setSeatCI(ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getNumeroPostoAssegnato());
                            passengerRender.setBookingClass(ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getBookingClass());
                            passengerRender.setPassengerType(ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getPassengerType());

                            passengerRender.setPnr(ctx.checkinSelectedPassengersExtra.get(p).getPrn());

                            if (ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getInfantFirstName() != null) {
                                passengerRender.setInfantFirstName(ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getInfantFirstName());
                            }

                            if (ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getInfantLastName() != null) {
                                passengerRender.setInfantLastName(ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getInfantLastName());
                            }

                            passengerRender.setNewSeat("");

                            passengerRender.setPassengerSeats(ctx.checkinSelectedPassengersExtra.get(p).getPassengerSeats());

                            // listPassengerRenderExtra.add(passengerRender);

                            // commentato per mostrare il posto al pax aggiuntivo sotto al suo nome: dopo gli sviluppi della nuova seatmap non serve piu' il setSeatCI; gli veniva sovrascritto con un posto assegnato nullo
                            // voli con scalo ok

//						String numeroPosto = "";
                            // Cerco il posto assegnato
//						for(com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerSeat seats : ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getPassengerSeats())
//						{
//							if (seats.getOrigin().equals(flightsSectorRender.getOrigin()))
//							{
//								numeroPosto = seats.getNumeroPostoAssegnato();
//							}
//						}
//						passengerRender.setSeatCI(numeroPosto);

                            passengerRender.setBookingClass(ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getBookingClass());
                            passengerRender.setPassengerType(ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getPassengerType());

                            if (ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getInfantFirstName() != null) {
                                passengerRender.setInfantFirstName(ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getInfantFirstName());
                            }

                            if (ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getInfantLastName() != null) {
                                passengerRender.setInfantLastName(ctx.checkinSelectedPassengersExtra.get(p).getPassenger().getInfantLastName());
                            }

                            passengerRender.setPassengerSeats(ctx.checkinSelectedPassengersExtra.get(p).getPassengerSeats());

                            int x1 = 0;
                            // Aggiorno il posto sul passeggero
                            for (PnrRender pnrSelectedListDataRender : ctx.pnrSelectedListDataRender) {

                                int x2 = 0;
                                for (FlightsRender flight : pnrSelectedListDataRender.getFlightsRender()) {

                                    int x3 = 0;
                                    for (SegmentRender segment : flight.getSegments()) {

                                        if (segment.getOrigin().getCode().equals(flightsSectorRender.getOrigin())) {
                                            // Ciclo sui passeggeri
                                            int x4 = 0;
                                            for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra passengerX : segment.getPassengersExtra()) {

                                                if (passengerX.getPassenger().getNome().equals(passengerRender.getNome()) && passengerX.getPassenger().getCognome().equals(passengerRender.getCognome()))
                                                    ctx.pnrSelectedListDataRender.get(x1).getFlightsRender().get(x2).getSegments().get(x3).getPassengersExtra().get(x4).setPassengerSeats(ctx.checkinSelectedPassengersExtra.get(p).getPassengerSeats());

                                                x4 += 1;
                                            }
                                        }
                                        x3 += 1;

                                    }
                                    x2 += 1;
                                }
                                x1 += 1;
                            }

                            //passengerRender.setNumberFrequentFlyer(ctx.checkinSelectedPassengers.get(p).getFrequentFlyer().get(0).getNumber()); //index out of bound se non e' frequent flyer
                            passengerRender.setNewSeat("");

                            List<SeatMapSector> listSeatMapSector = new ArrayList<>();

                            listSeatMapSector = startOrEndRow_13_17(listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapSectors());

                            int sectors = listSeatMapSector.size();

                            ArrayList<SectorRender> listSectorsRender = new ArrayList<>();
                            for (int i = 0; i < sectors; i++) {

                                SectorRender sectorRender = new SectorRender();
                                int sectorsStartRow = Integer.parseInt(listSeatMapSector.get(i).getStartingRow());
                                int sectorsEndingRow = Integer.parseInt(listSeatMapSector.get(i).getEndingRow());
                                int prevSectorsStartRow = 0;
                                int prevSectorsEndingRow = 0;
                                int nextSectorsStartRow = 0;
                                int nextSectorsEndingRow = 0;

                                if (i > 0) {
                                    prevSectorsStartRow = Integer.parseInt(listSeatMapSector.get(i - 1).getStartingRow());
                                    prevSectorsEndingRow = Integer.parseInt(listSeatMapSector.get(i - 1).getEndingRow());
                                }

                                if (i < sectors - 1) {
                                    nextSectorsStartRow = Integer.parseInt(listSeatMapSector.get(i + 1).getStartingRow());
                                    nextSectorsEndingRow = Integer.parseInt(listSeatMapSector.get(i + 1).getEndingRow());
                                }

                                sectorRender.setStartRow(String.valueOf(sectorsStartRow));
                                sectorRender.setEndRow(String.valueOf(sectorsEndingRow));
                                sectorRender.setPrevStartRow(String.valueOf(prevSectorsStartRow));
                                sectorRender.setPrevEndRow(String.valueOf(prevSectorsEndingRow));
                                sectorRender.setNextStartRow(String.valueOf(nextSectorsStartRow));
                                sectorRender.setNextEndRow(String.valueOf(nextSectorsEndingRow));
                                sectorRender.setStructureMap(structureNumericMap);

                                // ***** START : THEAD per AEM Seat Map *****
                                ArrayList<THead> listTHead = new ArrayList<>();
                                String[] items = structureNumericMap.split("_");
                                int aisles = items.length - 1; //corridoi

                                //exitEmergency1
                                THead theadEmergency1 = new THead();
                                theadEmergency1.setColumn("SC");
                                theadEmergency1.setTypeCharacteristics("Exit Emergency");
                                listTHead.add(theadEmergency1);

                                //corpo
                                int cont1 = 0;
                                int cont2 = 0;
                                for (int u = 0; u < items.length; u++) {
                                    cont1 = cont1 + Integer.parseInt(items[u]);

                                    for (int c = cont2; c < cont1; c++) {
                                        THead theadCorpo = new THead();
                                        theadCorpo.setColumn(listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapColumns().get(c).getColumn());
                                        theadCorpo.setTypeCharacteristics(listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapColumns().get(c).getTypeCharacteristics());
                                        listTHead.add(theadCorpo);
                                        cont2 = cont1;
                                    }

                                    if (u < aisles) {
                                        THead theadAisles = new THead();
                                        theadAisles.setColumn("SC");
                                        theadAisles.setTypeCharacteristics("");
                                        listTHead.add(theadAisles);
                                    }
                                }

                                //exitEmergency1
                                THead theadEmergency2 = new THead();
                                theadEmergency2.setColumn("SC");
                                theadEmergency2.setTypeCharacteristics("Exit Emergency");
                                listTHead.add(theadEmergency2);

                                sectorRender.setThead(listTHead);

                                // ***** END : THEAD per AEM Seat Map *****

                                // ***** START : TBODY per AEM Seat Map *****
                                ArrayList<ArrayList> listTBody = new ArrayList<>();


                                int struttura = listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapColumns().size();
                                for (int s = 0; s < listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().size(); s++) {
                                    ArrayList<SeatRender> listSeatsRender = new ArrayList<>(); //questo array deve essere inizializzato ad ogni giro//
                                    String rowNumber = "";
                                    rowNumber = listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getRowNumber();

                                    List<SeatRender> listSeatRenderOccupiedForPassenger = new ArrayList<>();
                                    listSeatRenderOccupiedForPassenger = seatOccupiedForPassenger(listSeatMapRender.get(e).getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats(), passengerRender.getPassengerId());

                                    //exitEmergency1
                                    SeatRender seatEmergency1 = new SeatRender();
                                    seatEmergency1 = seatSC("", "", "", "", false, "SC", "");
                                    listSeatsRender.add(seatEmergency1);
//                                logger.info("[CheckinESeatMap] [GraphicModelSeatMaps] <---> EXIT-EMERGENCY-1");

                                    //corpo
                                    int c1 = 0;
                                    int c2 = 0;
                                    for (int u = 0; u < items.length; u++) {
                                        c1 = c1 + Integer.parseInt(items[u]);

                                        for (int c = c2; c < c1; c++) {
                                            String passengerId = "";
                                            passengerId = listSeatRenderOccupiedForPassenger.get(c).getPassengerId().trim();
//                                        logger.info("[CheckinESeatMap] [GraphicModelSeatMaps] <---> PassengerID del Servizio EnanchedSeatMap : " + passengerId + " - PassengerID che ha effettuato il Check-in " + passengerRender.getPassengerId());
                                            //TODO : inserire l'infant
                                            if (passengerId.equals(passengerRender.getPassengerId())) {
//                                            logger.info("[CheckinESeatMap] [GraphicModelSeatMaps] <---> Management SeatBody Passeggeri Verificati.");
                                                SeatRender seatBody = new SeatRender();
                                                seatBody.setRow(rowNumber);
                                                seatBody.setColumn(listSeatRenderOccupiedForPassenger.get(c).getNumber());
                                                seatBody.setNumber(listSeatRenderOccupiedForPassenger.get(c).getNumber() + rowNumber);
                                                seatBody.setPassengerId(listSeatRenderOccupiedForPassenger.get(c).getPassengerId());
                                                seatBody.setOccupatoDa(listSeatRenderOccupiedForPassenger.get(c).getOccupatoDa());
                                                //seatBody.setOccupatoDa(listSeatRenderOccupiedForPassenger.get(c).getOccupatoDa());
                                                seatBody.setPrice(String.valueOf(listSeatRenderOccupiedForPassenger.get(c).getPrice()));
                                                seatBody.setConfortSeat(listSeatRenderOccupiedForPassenger.get(c).getConfortSeat());
                                                seatBody.setType(listSeatRenderOccupiedForPassenger.get(c).getType());
                                                seatBody.setAvailability(listSeatRenderOccupiedForPassenger.get(c).getAvailability());
                                                seatBody.setCssClass(setCssClass(seatBody, passengerRender, flightsSectorRender.getOrigin()));
                                                listSeatsRender.add(seatBody);
                                                c2 = c1;
                                            }
                                        }

                                        if (u < aisles) {
                                            SeatRender seatAisles = new SeatRender();
                                            seatAisles = seatSC("", "", "", "", false, "SC", "");
                                            listSeatsRender.add(seatAisles);
                                        }
                                    }

                                    //exitEmergency2
                                    SeatRender seatEmergency2 = new SeatRender();
                                    seatEmergency2 = seatSC("", "", "", "", false, "SC", "");
                                    listSeatsRender.add(seatEmergency2);
//                                logger.info("[CheckinESeatMap] [GraphicModelSeatMaps] <---> EXIT-EMERGENCY-2");

                                    listTBody.add(s, listSeatsRender);
                                }

                                // ***** END : TBODY per AEM Seat Map *****
                                sectorRender.setTBody(listTBody);
                                listSectorsRender.add(sectorRender);

                            }

                            passengerRender.setListSectorRender(listSectorsRender);
                            listPassengerRenderExtra.add(passengerRender);
                            listPassengerRender.add(passengerRender); // * * *


                        }
                        flightsSectorRender.setPassengerRenderExtra(listPassengerRenderExtra);
                    }
                }

                listFlightsSectorRender.add(flightsSectorRender);
                this.matrixFlightsSectorRender.add(listFlightsSectorRender);
            }

        } catch (Exception e) {
            logger.error("[CheckinESeatMap][GraphicModelSeatMaps] - errore nella Costruzione DEl Modello Grafico per la SeatMap.");
            e.getMessage();
        }
    }

    private List<SeatMapSector> startOrEndRow_13_17(List<SeatMapSector> seatMapSectors) {
        List<SeatMapSector> listSeatMapSector = new ArrayList<>();

        for (int s = 0; s < seatMapSectors.size(); s++) {
            SeatMapSector sms = new SeatMapSector();


            if (seatMapSectors.get(s).getStartingRow().equals("13")) {
                sms.setStartingRow("14");
            } else if (seatMapSectors.get(s).getStartingRow().equals("17")) {
                sms.setStartingRow("18");
            } else {
                sms.setStartingRow(seatMapSectors.get(s).getStartingRow());
            }
			
			/*if(seatMapSectors.get(s).getStartingRow().equals("17")){
				sms.setStartingRow("18");
			}else{
				sms.setStartingRow(seatMapSectors.get(s).getStartingRow());
			}*/

            if (seatMapSectors.get(s).getEndingRow().equals("13")) {
                sms.setEndingRow("12");
            } else if (seatMapSectors.get(s).getEndingRow().equals("17")) {
                sms.setEndingRow("16");
            } else {
                sms.setEndingRow(seatMapSectors.get(s).getEndingRow());
            }
			
			/*if(seatMapSectors.get(s).getEndingRow().equals("17")){
				sms.setEndingRow("16");
			}else{
				sms.setEndingRow(seatMapSectors.get(s).getEndingRow());
			}*/

            sms.setSeatMapRows(seatMapSectors.get(s).getSeatMapRows());
            listSeatMapSector.add(sms);
        }

        return listSeatMapSector;

    }

    private void setAssignedSeatToCtxPassengers(List<SeatMapSector> list, String origin, String departureDate) {
        for (PnrRender pnrRender : ctx.pnrSelectedListDataRender) {
            for (FlightsRender flightsRender : pnrRender.getFlightsRender()) {
                for (SegmentRender segmentRender : flightsRender.getSegments()) {
                    if (segmentRender.getPassengers() != null) {
                        for (Passenger passengerRender : segmentRender.getPassengers()) {

                            for (SeatMapSector seatMapSector : list) {
                                for (SeatMapRow seatMapRow : seatMapSector.getSeatMapRows()) {
                                    for (Seat seat : seatMapRow.getSeats()) {
                                        for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger passenger : ctx.checkinSelectedPassengers) {
                                            if (seat.getOccupatoDa() != null) {
                                                String occupatoDaTrimmed = seat.getOccupatoDa().replaceAll(" ", "");
                                                if (occupatoDaTrimmed.equalsIgnoreCase((passenger.getCognome() + passenger.getNome()).replaceAll(" ", ""))) {

                                                    boolean exist = false;

                                                    // Aggiunto/aggiorno il posto assegnato
                                                    List<PassengerSeat> pSeats = new ArrayList<PassengerSeat>();

                                                    if (passenger.getPassengerSeats() != null) {
                                                        pSeats = passenger.getPassengerSeats();
                                                        for (PassengerSeat seats : passenger.getPassengerSeats()) {
                                                            if (seats.getOrigin().equals(origin) && seats.getDepartureDate().equals(departureDate)) {
                                                                seats.setPostoAssegnato(true);
                                                                seats.setNumeroPostoAssegnato(seatMapRow.getRowNumber() + seat.getNumber());

                                                                passenger.setPostoAssegnato(true);
                                                                //passenger.setNumeroPostoAssegnato(seat.getNumber() + seatMapRow.getRowNumber());
                                                                passenger.setNumeroPostoAssegnato(seatMapRow.getRowNumber() + seat.getNumber());

                                                                exist = true;
                                                                if (passengerRender.getNome().equals(passenger.getNome()) && passengerRender.getCognome().equals(passenger.getCognome())) {
                                                                    passengerRender.setPostoAssegnato(true);
                                                                    passengerRender.setNumeroPostoAssegnato(seatMapRow.getRowNumber() + seat.getNumber());
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (!exist) {
                                                        PassengerSeat seats = new PassengerSeat();
                                                        seats.setOrigin(origin);
                                                        seats.setDepartureDate(departureDate);
                                                        seats.setPostoAssegnato(true);
                                                        seats.setNumeroPostoAssegnato(seatMapRow.getRowNumber() + seat.getNumber());
                                                        pSeats.add(seats);
                                                        if (passengerRender.getNome().equals(passenger.getNome()) && passengerRender.getCognome().equals(passenger.getCognome())) {
                                                            passengerRender.setPostoAssegnato(true);
                                                            passengerRender.setNumeroPostoAssegnato(seatMapRow.getRowNumber() + seat.getNumber());
                                                        }

                                                        passenger.setPostoAssegnato(true);
                                                        //passenger.setNumeroPostoAssegnato(seat.getNumber() + seatMapRow.getRowNumber());
                                                        passenger.setNumeroPostoAssegnato(seatMapRow.getRowNumber() + seat.getNumber());

                                                    }

                                                    passenger.setPassengerSeats(pSeats);


                                                }
                                            }
                                        }
                                        if (!(segmentRender.getPassengersExtra() == null)) {
                                            for (PassengerExtra passengerExtra : segmentRender.getPassengersExtra()) {

                                                for (com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerExtra passenger : ctx.checkinSelectedPassengersExtra) {
                                                    if (seat.getOccupatoDa() != null) {
                                                        String occupatoDaTrimmed = seat.getOccupatoDa().replaceAll(" ", "");
                                                        if (occupatoDaTrimmed.equalsIgnoreCase((passenger.getPassenger().getCognome() + passenger.getPassenger().getNome()).replaceAll(" ", ""))) {

                                                            boolean exist = false;

                                                            // Aggiunto/aggiorno il posto assegnato
                                                            List<PassengerSeat> pSeats = new ArrayList<PassengerSeat>();

                                                            if (passenger.getPassengerSeats() != null) {
                                                                pSeats = passenger.getPassengerSeats();
                                                                for (PassengerSeat seats : passenger.getPassengerSeats()) {
                                                                    if (seats.getOrigin().equals(origin) && seats.getDepartureDate().equals(departureDate)) {
                                                                        seats.setPostoAssegnato(true);
                                                                        seats.setNumeroPostoAssegnato(seatMapRow.getRowNumber() + seat.getNumber());

                                                                        passenger.getPassenger().setPostoAssegnato(true);
                                                                        //passenger.setNumeroPostoAssegnato(seat.getNumber() + seatMapRow.getRowNumber());
                                                                        passenger.getPassenger().setNumeroPostoAssegnato(seatMapRow.getRowNumber() + seat.getNumber());

                                                                        exist = true;
                                                                        if (passengerExtra.getPassenger().getNome().equals(passenger.getPassenger().getNome()) && passengerExtra.getPassenger().getCognome().equals(passenger.getPassenger().getCognome())) {
                                                                            passengerExtra.getPassenger().setPostoAssegnato(true);
                                                                            passengerExtra.getPassenger().setNumeroPostoAssegnato(seatMapRow.getRowNumber() + seat.getNumber());
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            if (!exist) {
                                                                PassengerSeat seats = new PassengerSeat();
                                                                seats.setOrigin(origin);
                                                                seats.setDepartureDate(departureDate);
                                                                seats.setPostoAssegnato(true);
                                                                seats.setNumeroPostoAssegnato(seatMapRow.getRowNumber() + seat.getNumber());
                                                                pSeats.add(seats);

                                                                passenger.getPassenger().setPostoAssegnato(true);
                                                                //passenger.setNumeroPostoAssegnato(seat.getNumber() + seatMapRow.getRowNumber());
                                                                passenger.getPassenger().setNumeroPostoAssegnato(seatMapRow.getRowNumber() + seat.getNumber());
                                                                if (passengerExtra.getPassenger().getNome().equals(passenger.getPassenger().getNome()) && passengerExtra.getPassenger().getCognome().equals(passenger.getPassenger().getCognome())) {
                                                                    passengerExtra.getPassenger().setPostoAssegnato(true);
                                                                    passengerExtra.getPassenger().setNumeroPostoAssegnato(seatMapRow.getRowNumber() + seat.getNumber());
                                                                }

                                                            }

                                                            passenger.setPassengerSeats(pSeats);


                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private String setCssClass(SeatRender s, PassengerRender p, String origin) {
        String cssClass = "";

        if (s.getType().equals("NotExist")) {
            return "notExist";
        }

        if (s.getAvailability().equals("NotApplicable")) {
            return "notExist";
        }
		/*
		// In base al volo cerco il numero posto
		String numeroPosto = "";
		// Aggiunto/aggiorno il posto assegnato
		for(com.alitalia.aem.common.data.checkinrest.model.pnrinfo.PassengerSeat seats : p.getPassengerSeats())
		{
			if (seats.getOrigin().equals(origin))
			{
				numeroPosto = seats.getNumeroPostoAssegnato();
			}
		}

		if(!numeroPosto.equals("")) {
			if (numeroPosto.equals(s.getNumber())) {
				return "seatAssigned";
			}
		}*/

        //posto assegnato al passeggero
        if (p.getSeatCI() != null) {
            String tmpPosto = s.getRow() + s.getColumn();
            //if (p.getSeatCI().equals(s.getNumber()))
            if (p.getSeatCI().equals(tmpPosto)) {
                return "seatAssigned";
            }
        }

        //posto occupato
        if (s.getAvailability().equals("Occupied")) {
            return "occupied";
        }
        if (s.getAvailability().equals("SeatDenied")) {
            return "occupied";
        }

        //posto libero
        if (s.getAvailability().equals("Free")) {
            if (s.getConfortSeat()) {
                return "extraComfort";
            } else {
                return "available";
            }
        }
        return cssClass;
    }

    private List<SeatRender> seatOccupiedForPassenger(List<Seat> seats, String passengerID) {
        List<SeatRender> listSeatRenderOccupiedForPassenger = new ArrayList<>();

        for (int s = 0; s < seats.size(); s++) {
            SeatRender seatRender = new SeatRender();
            String passengerIdSeat = "";
            passengerIdSeat = seats.get(s).getNome().trim() + seats.get(s).getCognome().trim();
            //TODO : inserire l'infant
            if (passengerIdSeat.equalsIgnoreCase(passengerID)) {
                seatRender.setNumber(seats.get(s).getNumber());
                seatRender.setPassengerId(passengerIdSeat);

                String occupatoDa;
                String numeroAltroPax = "";
                occupatoDa = seats.get(s).getOccupatoDa();
                if (occupatoDa != null) {
                    for (int i = 0; i < ctx.checkinSelectedPassengers.size(); i++) {
                        if (occupatoDa.equalsIgnoreCase(ctx.checkinSelectedPassengers.get(i).getCognome() + " " + ctx.checkinSelectedPassengers.get(i).getNome())) {
                            numeroAltroPax = Integer.toString(i + 1);
                        }
                    }
                }

                seatRender.setOccupatoDa(!numeroAltroPax.equals("") ? numeroAltroPax : "x");
                seatRender.setPrice(seats.get(s).getPriceString());
                seatRender.setConfortSeat(seats.get(s).getConfortSeat());
                seatRender.setType(seats.get(s).getType());
                seatRender.setAvailability(seats.get(s).getAvailability());
                listSeatRenderOccupiedForPassenger.add(seatRender);
            }
        }
        return listSeatRenderOccupiedForPassenger;
    }

    private String structureMapSeats(List<SeatMapColumn> list) {
        List<SeatMapColumn> posti = list;
        String structureMapSeats = "";

        String rigaPosti = "";

        int numPosti = 1;
        boolean isCorridoio;
        boolean newCorridoio = false;
        //      int postiTemp = 1;

        for (int i = 0; i < posti.size(); i++) {
            isCorridoio = posti.get(i).getTypeCharacteristics().equals(CheckinRestTypeSeatEnum.AISLE.value()) ? true : false;

            if (isCorridoio) {
                if (rigaPosti.equals("") || !newCorridoio) {
                    rigaPosti += numPosti + "_";
                    newCorridoio = true;
                } else if (newCorridoio) {
                    numPosti = 1;
                    newCorridoio = false;
                } else {
                    //                           rigaPosti += numPosti;
                    newCorridoio = false;
                }
            }
            if (i == posti.size() - 1) {
                rigaPosti += numPosti;
            }
            numPosti++;
        }
        //System.out.println("RIGA:" + rigaPosti);
        structureMapSeats = rigaPosti;
        return structureMapSeats;
    }

    private SeatRender seatSC(String number, String passengerId, String occupatoDa, String price, boolean confrotSeat, String type, String availability) {
        SeatRender seatRenderSC = new SeatRender();

        seatRenderSC.setNumber(number);
        seatRenderSC.setPassengerId(passengerId);
        seatRenderSC.setOccupatoDa(occupatoDa);
        seatRenderSC.setPrice(price);
        seatRenderSC.setConfortSeat(confrotSeat);
        seatRenderSC.setType(type);
        seatRenderSC.setAvailability(availability);

        return seatRenderSC;

    }

    public List<SeatMapRender> getListSeatMapRender() {
        return listSeatMapRender;
    }

    public void setListSeatMapRender(List<SeatMapRender> listSeatMapRender) {
        this.listSeatMapRender = listSeatMapRender;
    }

    public ArrayList<ArrayList> getMatrixFlightsSectorRender() {
        return matrixFlightsSectorRender;
    }

    public void setMatrixFlightsSectorRender(ArrayList<ArrayList> matrixFlightsSectorRender) {
        this.matrixFlightsSectorRender = matrixFlightsSectorRender;
    }

    private void adjustPrice() {
        String jcrThousandSeparator = new HierarchyNodeInheritanceValueMap(request.getResource().getParent().getParent().getParent().getParent().getParent()).getInherited("thousandSeparator", String.class);
        String jcrSep = new HierarchyNodeInheritanceValueMap(request.getResource().getParent().getParent().getParent().getParent().getParent()).getInherited("decimalSeparator", String.class);
        String jcrDecimale = new HierarchyNodeInheritanceValueMap(request.getResource().getParent().getParent().getParent().getParent().getParent()).getInherited("decimalDigit", String.class);
        for (SeatMapRender seatMapRender : this.listSeatMapRender) {
            if (seatMapRender != null && seatMapRender.getEnhancedSeatMapResp() != null && seatMapRender.getEnhancedSeatMapResp().getSeatMAp() != null){
                for (SeatMapSector seatMapSector : seatMapRender.getEnhancedSeatMapResp().getSeatMAp().getSeatMaps().getSeatMapSectors()) {
                    for (SeatMapRow seatMapRow : seatMapSector.getSeatMapRows()) {
                        for (Seat seat : seatMapRow.getSeats()) {
                            if (jcrSep != null && jcrDecimale != null) {
                                if (seat.getPrice() != 0.0 && jcrSep != "" && jcrDecimale != "") {
                                    String separatore = (jcrSep != null ? jcrSep : ""), decimale = (jcrDecimale != null ? jcrDecimale : "0"), decimale2 = "";
                                    String[] price = null;
                                    price = String.valueOf(seat.getPrice()).split("\\.");
                                    if (!decimale.equals("0")) {
                                        for (int i = 0; i < Integer.parseInt(decimale); i++) {
                                            decimale2 += "0";
                                        }
                                        if (price[1].length() < Integer.parseInt(decimale)) {
                                            seat.setPriceString(addThousandSeparator(price[0],jcrThousandSeparator) + separatore + price[1] + decimale2.substring(price[1].length(), decimale2.length()));
                                        } else {
                                            seat.setPriceString(addThousandSeparator(price[0],jcrThousandSeparator) + separatore + price[1]);
                                        }
                                    } else {
                                        seat.setPriceString(addThousandSeparator(price[0],jcrThousandSeparator));
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }


    private String addThousandSeparator (String price, String jcrThousandSeparator){
        if(jcrThousandSeparator != null){
            if (jcrThousandSeparator != "" && price.length() > 3){
                price = price.substring(0,price.length()-3) + jcrThousandSeparator + price.substring(price.length()-3,price.length());
            }
        }
        return price;
    }


}

