package com.alitalia.aem.consumer.booking.render;

import java.util.List;

public class TrattaRender {

	private String aereoportoPartenza;
	private String cittaPartenza;
	private String aereoportoArrivo;
	private String cittaArrivo;
	private String oraPartenza;
	private String oraArrivo;
	private String codiceVolo;
	private String tempoViaggio;
	private String miglia;
	private String vector;
	private String codiceAereomobile;
	private List<Boolean> frequenza;
	
	public TrattaRender(String aereoportoPartenza, String aereoportoArrivo, String oraPartenza,
			String oraArrivo, String codiceVolo, String tempoViaggio,
			String miglia, String codiceAereomobile, List<Boolean> frequenza, String vector) {
		this.aereoportoPartenza = aereoportoPartenza;
		this.cittaPartenza = "airportsData." + aereoportoPartenza + ".city";
		this.aereoportoArrivo = aereoportoArrivo;
		this.cittaArrivo = "airportsData." + aereoportoArrivo + ".city";
		this.oraPartenza = oraPartenza;
		this.oraArrivo = oraArrivo;
		this.codiceVolo = codiceVolo;
		this.tempoViaggio = tempoViaggio;
		this.miglia = miglia;
		this.codiceAereomobile = codiceAereomobile;
		this.frequenza = frequenza;
		this.vector = vector;
	}

	public String getAereoportoPartenza() {
		return aereoportoPartenza;
	}

	public void setAereoportoPartenza(String aereoportoPartenza) {
		this.aereoportoPartenza = aereoportoPartenza;
	}

	public String getCittaPartenza() {
		return cittaPartenza;
	}

	public void setCittaPartenza(String cittaPartenza) {
		this.cittaPartenza = cittaPartenza;
	}

	public String getAereoportoArrivo() {
		return aereoportoArrivo;
	}

	public void setAereoportoArrivo(String aereoportoArrivo) {
		this.aereoportoArrivo = aereoportoArrivo;
	}

	public String getCittaArrivo() {
		return cittaArrivo;
	}

	public void setCittaArrivo(String cittaArrivo) {
		this.cittaArrivo = cittaArrivo;
	}

	public String getOraPartenza() {
		return oraPartenza;
	}

	public void setOraPartenza(String oraPartenza) {
		this.oraPartenza = oraPartenza;
	}

	public String getOraArrivo() {
		return oraArrivo;
	}

	public void setOraArrivo(String oraArrivo) {
		this.oraArrivo = oraArrivo;
	}

	public String getCodiceVolo() {
		return codiceVolo;
	}

	public void setCodiceVolo(String codiceVolo) {
		this.codiceVolo = codiceVolo;
	}

	public String getTempoViaggio() {
		return tempoViaggio;
	}

	public void setTempoViaggio(String tempoViaggio) {
		this.tempoViaggio = tempoViaggio;
	}

	public String getMiglia() {
		return miglia;
	}

	public void setMiglia(String miglia) {
		this.miglia = miglia;
	}

	public String getCodiceAereomobile() {
		return codiceAereomobile;
	}

	public void setCodiceAereomobile(String codiceAereomobile) {
		this.codiceAereomobile = codiceAereomobile;
	}

	public List<Boolean> getFrequenza() {
		return frequenza;
	}

	public void setFrequenza(List<Boolean> frequenza) {
		this.frequenza = frequenza;
	}
	
	public String getVector() {
		return vector;
	}

	public void setVector(String vector) {
		this.vector = vector;
	}
}

