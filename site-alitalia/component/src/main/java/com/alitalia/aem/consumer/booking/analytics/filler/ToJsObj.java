package com.alitalia.aem.consumer.booking.analytics.filler;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public abstract class ToJsObj {
	
	protected static final String ENCODING = "UTF-8";
	
	protected String addAndEncodeJsObjElem(String jsObj, String elemName
			, String elemValue, boolean trailingComma){
		String jsString = "";
		try {
			String jsValue = URLEncoder.encode(elemValue, ENCODING).replace("+", "%20");
			jsString =  jsObj + "'"+elemName+"': decodeURIComponent('"+jsValue+"')" 
			+ (trailingComma ? "," : "");
		} catch (UnsupportedEncodingException e) {
		};
		return jsString;
	}
	
	protected String formatIfNotNull(BigDecimal price, DecimalFormat formatter){
		return price != null ? formatter.format(price) : "";
	}
	
	protected String addJsObjElem(String jsObj, String elemName
			, String elemValue, boolean trailingComma){
		String elemString = "";
		if(elemValue != null){
			elemString = elemValue;
		}
		return jsObj + "'"+elemName+"':'"+elemString+"'" 
			+ (trailingComma ? "," : "");
	}
	
	protected String addJsObjElem(String jsObj, String elemName, Integer elemValue, boolean trailingComma){
		String elemString = "";
		if(elemValue != null){
			elemString = String.valueOf(elemValue);
		}
		
		return jsObj + "'"+elemName+"':'"+elemString+"'" 
		+ (trailingComma ? "," : "");
	}
	
	protected String addJsTimeElem(String jsObj, String elemName, Integer elemValue, boolean trailingComma){
			String elemValueString="";
		if (elemValue==null){
				elemValueString="";
			}else{
				if(elemValue.intValue()<10){
					elemValueString="0"+String.valueOf(elemValue.intValue());;
				}else{
				elemValueString=String.valueOf(elemValue.intValue());}
			}
			
		return jsObj + "'"+elemName+"':'"+elemValueString+"'" 
			+ (trailingComma ? "," : "");
	}
	
	protected String addJsArrayObjElem(String jsObj, String elemName
			, String elemValue, boolean trailingComma){
		String elemString = "";
		if(elemValue != null){
			elemString = elemValue;
		}
		return jsObj + "'"+elemName+"':"+elemString+""
				+ (trailingComma ? "," : "");
	}
	
	protected String formatIfNotNullAncPrice(BigDecimal price, DecimalFormat formatter){
		return price != null ? formatter.format(price) : formatter.format(BigDecimal.ZERO);
	}
	
	protected String formatIfNotNull(Calendar date, SimpleDateFormat formatter){
		
		return date != null ? formatter.format(date.getTime()) : "";
	}
	
	protected String formatIfNotNullString(String string){
		return string != null ? string : "";
	}

}
