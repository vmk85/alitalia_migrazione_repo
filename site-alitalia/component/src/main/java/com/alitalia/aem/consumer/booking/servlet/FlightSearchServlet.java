package com.alitalia.aem.consumer.booking.servlet;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.ResidencyTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSearchCUGEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSearchTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.model.SearchElementAirport;
import com.alitalia.aem.consumer.booking.model.SearchPassengersNumber;
import com.alitalia.aem.consumer.carnet.CarnetConstants;
import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.TypeUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "flightsearchconsumer" }),
		@Property(name = "sling.servlet.methods", value = { "POST", "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class FlightSearchServlet extends GenericBookingFormValidatorServlet {
	
	@Reference
    private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private ResourceResolverFactory resolverFactory;
	
	public String getBookingExternalDomain() {
    	return configuration.getBookingExternalDomain();
    }
	
	private static final String STANDARD_REQUEST_DATE_FORMAT = "dd/MM/yyyy";
	
	private static final String ATTR_NBABY = "Infants";

	private static final String ATTR_NKIDS = "Children";

	private static final String ATTR_NADULTS = "Adults";

	private static final String ATTR_YOUTHS = "Youths";

	private static final String ATTR_CUG = "CUG";

	private static final String ATTR_RETURN_DATE = "ReturnDate";

	private static final String ATTR_DEPARTURE_DATE = "DepartureDate";

	private static final String ATTR_SEARCH_TYPE = "SearchType";
	
	private static final String ATTR_SEARCH_TYPE_GIOVANI = "SearchType_Giovani";

	private static final String ATTR_DESTINATION = "Destination";

	private static final String ATTR_ORIGIN = "Origin";
	
	private static final String ATTR_CLASS_OF_FLIGHT = "classOfFlight";
	
	private static final String ATTR_CTA_SPECIAL_OFFERS = "fromCtaSpecialOffers";

	@Reference
	private AlitaliaConfigurationHolder alitaliaConfiguration;
	
	@Reference
	private BookingSession bookingSession;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {

		Validator validator = new Validator();
		
		// departure airport
		validator.addDirectCondition(ATTR_ORIGIN,
				request.getParameter(ATTR_ORIGIN),
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(ATTR_ORIGIN,
				request.getParameter(ATTR_ORIGIN),
				BookingConstants.MESSAGE_AIRPORT_FROM_NOT_VALID, "isIataAptCode");

		// arrival airport
		validator.addDirectCondition(ATTR_DESTINATION,
				request.getParameter(ATTR_DESTINATION),
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(ATTR_DESTINATION,
				request.getParameter(ATTR_DESTINATION),
				BookingConstants.MESSAGE_AIRPORT_TO_NOT_VALID, "isIataAptCode");
		
		// search type
		validator.addDirectCondition(ATTR_SEARCH_TYPE,
				request.getParameter(ATTR_SEARCH_TYPE),
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
//		validator.setAllowedValues(ATTR_SEARCH_TYPE,
//				request.getParameter(ATTR_SEARCH_TYPE),BookingSearchTypeEnum.listValues(),
//				BookingConstants.MESSAGE_GENERIC_INVALID_FIELD);


		// start date
		String dateFormat = AlitaliaUtils.getDateFormatByMarket(request.getResource());
		String addParamDate = "";
		if (dateFormat != null) {
			addParamDate = "|" + dateFormat;
		}
		validator.addDirectCondition(ATTR_DEPARTURE_DATE,
				request.getParameter(ATTR_DEPARTURE_DATE),
				BookingConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");		
		validator.addDirectCondition(ATTR_DEPARTURE_DATE, request.getParameter(ATTR_DEPARTURE_DATE) + addParamDate,
				BookingConstants.MESSAGE_DATE_PAST, "isDate", "isFuture");

		// check if departure airport and arrival airport are different
		validator.addCrossCondition(ATTR_DESTINATION,
				request.getParameter(ATTR_ORIGIN),
				request.getParameter(ATTR_DESTINATION),
				BookingConstants.MESSAGE_FROM_TO_DIFF, "areDifferent");

		// end date
		String end = request.getParameter(ATTR_RETURN_DATE);
		
		// check if start date is before end date
		if (!BookingSearchTypeEnum.isOneWay(request.getParameter(ATTR_SEARCH_TYPE))) {
			
			validator.addDirectCondition(ATTR_RETURN_DATE, end + addParamDate,
					BookingConstants.MESSAGE_DATE_PAST, "isDate");
			
			if (end != null && !end.isEmpty() && request.getParameter(ATTR_DEPARTURE_DATE) != null && !request.getParameter(ATTR_DEPARTURE_DATE).isEmpty()) {
				validator.addCrossCondition(ATTR_RETURN_DATE, request.getParameter(ATTR_DEPARTURE_DATE) + addParamDate,
						end + addParamDate, BookingConstants.MESSAGE_DATE_PAST,
						"beforeThen");
			}
			
			if (end != null && !end.isEmpty()) {
				validator.addDirectCondition(ATTR_RETURN_DATE, end + addParamDate,
						BookingConstants.MESSAGE_DATE_PAST, "isFuture");
			}
		} 
		
		/* check for the CUG */
		String cug = request.getParameter(ATTR_CUG);
		if(cug != null){
			validator.setAllowedValues(ATTR_CUG,
				request.getParameter(ATTR_CUG), BookingSearchCUGEnum.listValues(),
				BookingConstants.MESSAGE_CUG_NOT_VALID);
		}
		
		if( cug!=null && BookingSearchCUGEnum.YOUTH.value().equals(cug) ) {
			validator.addDirectCondition(ATTR_YOUTHS, 
					request.getParameter(ATTR_YOUTHS),
					BookingConstants.MESSAGE_NYOUNGS_NOT_VALID, "isNotEmpty", "isNumber");
		}else{
		
			// number of adults
			validator.addDirectCondition(ATTR_NADULTS,
					request.getParameter(ATTR_NADULTS),
					BookingConstants.MESSAGE_NADULTS_NOT_VALID, "isRangeAdults");

			// number of kids
			String nkids = request.getParameter(ATTR_NKIDS);
			if (nkids != null && !nkids.isEmpty()) {
				validator.addDirectCondition(ATTR_NKIDS, nkids,
						BookingConstants.MESSAGE_NKINDS_NOT_VALID, "isRangeKids");
			}

			// number of babies
			String nbabies = request.getParameter(ATTR_NBABY);
			if (nbabies != null && !nbabies.isEmpty()) {
				validator.addDirectCondition(ATTR_NBABY, nbabies,
						BookingConstants.MESSAGE_NBABIES_NOT_VALID, "isRangeBabies");
			}
			
			// check sum of adults and kids
			validator.addCrossCondition(ATTR_NADULTS, request.getParameter(ATTR_NADULTS),
					request.getParameter(ATTR_NKIDS),
					BookingConstants.MESSAGE_SUM_ADULTS_KIDS_NOT_VALID,
					"underMaxSumAdultsKids");
			
			// check if number of babies is lower then number of adults
			if (nbabies != null && !nbabies.isEmpty()) {
				validator.addCrossCondition(ATTR_NBABY,
						request.getParameter(ATTR_NADULTS),
						request.getParameter(ATTR_NBABY),
						BookingConstants.MESSAGE_BABY_PER_ADULT_NOT_VALID, "babyPerAdult");
			}
			
			if (cug!=null && BookingSearchCUGEnum.FAMILY.value().equals(cug)) {
				
				validator.addDirectCondition(ATTR_NADULTS, request.getParameter(ATTR_NADULTS),
						BookingConstants.MESSAGE_NADULTS_NOT_VALID, "isRangeAdultForFamily");
				
				validator.addCrossCondition(ATTR_NKIDS,
						request.getParameter(ATTR_NKIDS),
						request.getParameter(ATTR_NBABY),
						BookingConstants.MESSAGE_NKINDS_NOT_VALID, "isRangeKidsAndBabiesForFamily");
				
				validator.addCrossCondition(ATTR_NBABY,
						request.getParameter(ATTR_NKIDS),
						request.getParameter(ATTR_NBABY),
						BookingConstants.MESSAGE_NBABIES_NOT_VALID, "isRangeKidsAndBabiesForFamily");
			}
			
		}
		
		return validator.validate();
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws Exception {
		
		// prepare search data from received parameters
		String departureInput = request.getParameter(ATTR_ORIGIN);
		String arrivalInput = request.getParameter(ATTR_DESTINATION);
		String departureDate = request.getParameter(ATTR_DEPARTURE_DATE);
		String returnDate = request.getParameter(ATTR_RETURN_DATE);
		String searchTypeGiovani = request.getParameter(ATTR_SEARCH_TYPE_GIOVANI);
		String fromCallToActionSpecialOffers = request.getParameter(ATTR_CTA_SPECIAL_OFFERS);
		String cugSearch = request.getParameter(ATTR_CUG) == null ? 
				BookingSearchCUGEnum.ADT.value() : request.getParameter(ATTR_CUG);
		
		boolean isGiovaniLastminute = searchTypeGiovani == null ? false : "JumpUP".equals(searchTypeGiovani);
		boolean showRoundTrip = fromCallToActionSpecialOffers == null ? false : "true".equals(fromCallToActionSpecialOffers);
				
		String currentFormat = AlitaliaUtils.getDateFormatByMarket(request.getResource());
		if (currentFormat == null) {
			currentFormat = STANDARD_REQUEST_DATE_FORMAT;
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat(currentFormat);
		dateFormat.setLenient(false);
		
		CabinEnum filterCabin = CabinEnum.ECONOMY;
		String filterClassFlight = request.getParameter(ATTR_CLASS_OF_FLIGHT);
		if (filterClassFlight != null) {
			if(("business").equals(filterClassFlight)) {
				filterCabin = CabinEnum.BUSINESS;
			}
		}
		
		BookingSearchKindEnum searchKind = BookingSearchKindEnum.SIMPLE;
		List<SearchElement> searchElements = new ArrayList<SearchElement>();
		SearchPassengersNumber searchPassengersNumber = new SearchPassengersNumber();
		BookingSearchCUGEnum cug = BookingSearchCUGEnum.fromValue(cugSearch);
		String market = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		String site = AlitaliaUtils.getRepositoryPathMarket(request.getResource());
		Locale locale = AlitaliaUtils.getPageLocale(request.getResource());
		InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(request.getResource());
		String decimalSeparator=ivm.getInherited("decimalSeparator", String.class);
		String groupingSeparator=ivm.getInherited("groupingSeparator", String.class);
		Integer numDecDigits=ivm.getInherited("decimalDigit", Integer.class);
		String currency = ivm.getInherited("currencyCode", String.class);
		if(numDecDigits == null){
			numDecDigits = 2;
		}
		NumberFormat currentNumberFormat = AlitaliaUtils.getNumberFormat(numDecDigits, decimalSeparator, groupingSeparator, market);
		
		SearchElementAirport fromElement = new SearchElementAirport(departureInput);
		SearchElementAirport toElement = new SearchElementAirport(arrivalInput);
		
		searchElements.add(new SearchElement(
				fromElement, toElement, TypeUtils.createCalendar(dateFormat.parse(departureDate))));
		
		if (returnDate != null && !"".equals(returnDate)) {
			searchElements.add(new SearchElement(
					toElement, fromElement, TypeUtils.createCalendar(dateFormat.parse(returnDate))));
			searchKind = BookingSearchKindEnum.ROUNDTRIP;
		}
		
		if (cug == BookingSearchCUGEnum.YOUTH) {
			if (isGiovaniLastminute) {
				searchPassengersNumber.setNumAdults(Integer.parseInt(request.getParameter(ATTR_NADULTS)));
			} else {
				searchPassengersNumber.setNumYoung(Integer.parseInt(request.getParameter(ATTR_YOUTHS)));
			}
		} else {
			searchPassengersNumber.setNumAdults(Integer.parseInt(request.getParameter(ATTR_NADULTS)));
			searchPassengersNumber.setNumChildren(Integer.parseInt(request.getParameter(ATTR_NKIDS)));
			searchPassengersNumber.setNumInfants(Integer.parseInt(request.getParameter(ATTR_NBABY)));
		}
		
		//obtain info isCarnetSearch
		boolean isCarnet = false;
		String carnetSearch = request.getParameter("isCarnet");
		if (("CRN").equals(carnetSearch)){
			isCarnet = true;
		}
		
		// create a new booking session context and save it in the session
		HttpSession session = request.getSession(true);
		
		//INIZIO *** MODIFICA IP ***	
		String clientIP = "";
		clientIP = AlitaliaUtils.getRequestRemoteAddr(configuration, request);
		//FINE.
		
		String sessionId = session.getId();
		BookingSessionContext ctx = null;
		
		if (!isCarnet) {
			ctx = bookingSession.initializeBookingSession("alitaliaConsumerBooking",
					clientIP,sessionId);
		} else {
			CarnetSessionContext carnetSessionContext = null;
			Object carnetSessionCtx = request.getSession().getAttribute(CarnetConstants.CARNET_CONTEXT_ATTRIBUTE);
			if (carnetSessionCtx != null) {
				carnetSessionContext = (CarnetSessionContext) carnetSessionCtx;
			}
			String nPaxReq = request.getParameter(ATTR_NADULTS);
			int nPax = 0;
			if(nPaxReq != null && !"".equals(nPaxReq)){
				nPax = Integer.parseInt(nPaxReq);
			}
			Map<String,String> configurationFareRules = retriveFareRulesFromCRX(request);
			ctx = bookingSession.initializeBookingSessionWithCarnet("alitaliaConsumerBooking",
					clientIP,sessionId,carnetSessionContext,configurationFareRules,nPax);
			cug=BookingSearchCUGEnum.CARNET;
			if(!ctx.isCarnetValid){
				Resource resource = request.getResource();
				String protocol = "http://";
		    	if(configuration.getHttpsEnabled()){
		    		protocol = "https://";
		    	}
		    	String domain = configuration.getExternalDomain();
		    	String redirectUrl = protocol + domain +
		    			AlitaliaUtils.findSiteExternalUrlWithDomain(resource, false)  
						+ getConfiguration().getCarnetHomeLoggedPage();
				response.sendRedirect(redirectUrl);
				return;
			}
		}
		
		//INIZIO *** MODIFICA IP ***
		if (clientIP != null && !clientIP.isEmpty()) {
			ctx.ipAddress = clientIP;
		}
		//FINE.
		
		session.setAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE, ctx);
		
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		
		String codiceAgenzia = "";
		String codiceAccordo = "";
	    
		// prepare the search data in the booking session
		bookingSession.prepareSearch(ctx, i18n, searchKind, searchElements,
				searchPassengersNumber, cug, market, site, locale, currentNumberFormat, currency, codiceAgenzia,
				codiceAccordo, filterCabin, isGiovaniLastminute, showRoundTrip);
		
			//********************
			//MODIFICA DEL 04/05/2017
			//X aggiunta gestione risposta Kayak
			//INIZIO
			//********************
			request.getSession(true).removeAttribute("kayakclickid");
			//metto questa parte quì apposta dopo il bookingSession.prepareSearch
			//perchè prevedo di mettere li la parte che farà request.getSession(true).removeAttribute("kayakclickid");
			//per pulire la variabile di sessione (oltre che nel model del confirmation page)
			if (request.getParameter("kayakclickid") != null && !request.getParameter("kayakclickid").isEmpty()) {
				request.getSession(true).setAttribute("kayakclickid", request.getParameter("kayakclickid"));
			}
			//********************
			//MODIFICA DEL 04/05/2017
			//X aggiunta gestione risposta Kayak
			//FINE
			//********************


		String queryString = computeQuesryStringCartRecovery(request);
		queryString = bookingSession.manageCartRecoveryParameters(ctx,queryString);
		
		// redirect to the correct page
		String successPagePlain = alitaliaConfiguration.getBookingFlightSelectPage();
		String successPageContinuitaSicilia = alitaliaConfiguration.getBookingFlightSearchContinuitySicilyPage();
		String successPageContinuitaSardegna = alitaliaConfiguration.getBookingFlightSearchContinuitySardiniaPage();
		
		String successPage = null;
		if (ctx.isAllowedContinuitaTerritoriale) {
			if (ctx.residency == ResidencyTypeEnum.SARDINIA) {
				successPage = successPageContinuitaSardegna;
			} else {
				successPage = successPageContinuitaSicilia;
			}
		} else { 
			successPage = successPagePlain;
		}
		
		if (successPage != null && !"".equals(successPage)) {
			/* Modifiche https start */
			Resource resource = request.getResource();
			String protocol = "http://";
	    	if(configuration.getHttpsEnabled()){
	    		protocol = "https://";
	    	}
			String successUrl = protocol 
					+ getBookingExternalDomain() 
					+ resource.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(resource, false)) 
					+ successPage
					+ queryString;
			response.sendRedirect(successUrl);
			/* Modifiche https end */
		}
	}
	
	private Map<String,String> retriveFareRulesFromCRX(SlingHttpServletRequest request) {
		Map<String,String> fareRulesMap = new LinkedHashMap<String, String>();
		String path = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) 
				+ "/alitalia-config/fareRulesCarnet/jcr:content";
		ResourceResolver resourceResolver =	request.getResourceResolver();
		Resource fareResource = resourceResolver.getResource(path);
		if (fareResource == null) {
			logger.error("Errore retriving fare rules for cart: /alitalia-config/fareRulesCarnet.html not found");
			throw new IllegalStateException("Page /alitalia-config/fareRulesCarnet.html not found");
		}
		for (Resource fareRule : fareResource.getChildren()) {
			fareRulesMap.put((String) fareRule.getValueMap().get("testo"), 
					(String) fareRule.getValueMap().get("valore"));
		}
		return fareRulesMap;
	}
	
	private String computeQuesryStringCartRecovery(
			SlingHttpServletRequest request) {
		String WT_mc_id = request.getParameter("WT.mc_id");
		String utr = request.getParameter("utr");
		if (WT_mc_id == null && utr == null) {
			return null;
		}
		if (WT_mc_id != null && utr == null) {
			return "WT.mc_id=" + WT_mc_id;
		}
		if (WT_mc_id == null && utr != null) {
			return "utr=" + utr;
		}
		if (WT_mc_id != null && utr != null) {
			return "WT.mc_id=" + WT_mc_id + "&utr=" + utr;
		}
		return null;
	}

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return alitaliaConfiguration;
	}
	
	@Override
	/*Send to Booking failure page if an error occurred*/
	protected String getSelfPageUrl(SlingHttpServletRequest request) {
		String selfPageUrl = "";
		String errorPage = configuration.getBookingFailurePage();
		if(errorPage != null && errorPage.length() > 0){
			String baseUrlForContext = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
			String baseUrlForRedirect = request.getResource().getResourceResolver().map(baseUrlForContext);
			selfPageUrl = baseUrlForRedirect + errorPage;
		}
		else {
			selfPageUrl = super.getSelfPageUrl(request);
		}
		return selfPageUrl;
	}
	
}
