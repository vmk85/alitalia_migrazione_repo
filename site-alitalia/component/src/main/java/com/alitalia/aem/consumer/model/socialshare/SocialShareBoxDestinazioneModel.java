package com.alitalia.aem.consumer.model.socialshare;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Source;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;

@Model(adaptables={SlingHttpServletRequest.class})
public class SocialShareBoxDestinazioneModel extends BaseSocialShareModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject @Optional @Default(values = "") @Named("intestazione") @Source(value="child-resources") @Via(value="resource") 
	private String boxIntestazione;
	
	@Inject @Optional @Default(values = "") @Named("destinazione1") @Source(value="child-resources") @Via(value="resource") 
	private String boxDestinazione1;
	
	@Inject @Optional @Default(values = "") @Named("destinazione2") @Source(value="child-resources") @Via(value="resource") 
	private String boxDestinazione2;
	
	@Inject @Optional @Default(values = "") @Named("sottotitolo1") @Source(value="child-resources") @Via(value="resource") 
	private String boxSottotitolo1;
	
	@Inject @Optional @Default(values = "") @Named("sottotitolo2") @Source(value="child-resources") @Via(value="resource") 
	private String boxSottotitolo2;
	
	@Inject @Optional @Default(values = "") @Named("link") @Source(value="child-resources") @Via(value="resource") 
	private String boxLink;
	
	@PostConstruct
	protected void initModel() {
		super.initBaseModel(request);
		try {
			title = boxIntestazione + " " + boxDestinazione1 + " " + boxDestinazione2;
			description = boxSottotitolo1 + " " + boxSottotitolo2;
			linkBack = boxLink;
			
			// the box component is itself a customized image component, so the image node is the resource itself
			imagePath = request.getResource().getPath();
			
		} catch (Exception e) {
			logger.error("Error while building social share data", e);
		}
	}
	
}
