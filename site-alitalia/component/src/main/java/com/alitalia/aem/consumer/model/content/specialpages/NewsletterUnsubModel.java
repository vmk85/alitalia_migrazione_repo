package com.alitalia.aem.consumer.model.content.specialpages;



import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;



@Model(adaptables={SlingHttpServletRequest.class})
public class NewsletterUnsubModel extends GenericBaseModel {
	
	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private NewsletterUnsubData newsletterUnsubData;
 
	private String emailToUnsubscribe;

	@Self
	private SlingHttpServletRequest request;
	
	@PostConstruct
	protected void initModel(){
		logger.debug("[NewsletterSubscribeModel] initModel");

		super.initBaseModel(slingHttpServletRequest);

	
		newsletterUnsubData = (NewsletterUnsubData) slingHttpServletRequest.getSession()
				.getAttribute(NewsletterUnsubData.NAME);
		slingHttpServletRequest.getSession().removeAttribute(NewsletterUnsubData.NAME);

	}
	
	public NewsletterUnsubData getNewsletterUnsubData() {
		return newsletterUnsubData;
	}

	public String getEmailToUnsubscribe() {
		return request.getParameter("a");
	}
	
	public void setEmailToUnsubscribe(String emailToUnsubscribe) {
		this.emailToUnsubscribe = emailToUnsubscribe;
	}
}
