/*package com.alitalia.aem.consumer.checkinrest.controller;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.checkinrest.CheckinChangeSeatsRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinChangeSeatsResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.model.GenericCheckinModel;
import com.alitalia.aem.consumer.checkinrest.render.FlightsSectorRender;
import com.alitalia.aem.consumer.checkinrest.render.SectorRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.webcheckin.delegaterest.ICheckinDelegate;

@Model(adaptables = { SlingHttpServletRequest.class})
public class ChangeSeats extends GenericCheckinModel {
	
	@Self
	private SlingHttpServletRequest request;

	@Inject
	private AlitaliaConfigurationHolder configuration;

	@Inject
	private CheckinSession checkinSession;

	@Inject 
	private volatile ICheckinDelegate checkInDelegateRest;

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private CheckinChangeSeatsResponse ChangeSeatsResponse;
	
	private ArrayList<ArrayList> matrixFlightsSectorRender = null;
	
	@PostConstruct
	protected void initModel() throws Exception {
		
		ChangeSeatsResponse = new CheckinChangeSeatsResponse();
		
		try {
			super.initBaseModel(request);
			int d = 0;
			
			CheckinChangeSeatsRequest changeSeatsRequest = new CheckinChangeSeatsRequest(IDFactory.getTid(), IDFactory.getSid(request));
			changeSeatsRequest.setBookingClass(request.getParameter("bookingClass"));
			changeSeatsRequest.setAirline(request.getParameter("airline"));
			changeSeatsRequest.setFlight(request.getParameter("flight"));
			changeSeatsRequest.setDepartureDate(request.getParameter("departureDate"));
			changeSeatsRequest.setOrigin(request.getParameter("origin"));
			changeSeatsRequest.setDestination(request.getParameter("destination"));
			changeSeatsRequest.setPnr(request.getParameter("pnr"));
			changeSeatsRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
			changeSeatsRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
			changeSeatsRequest.setConversationID(request.getParameter("conversationID"));
			//TODO il form JavaScript ci invia una lista di passeggeri distinti --- da mappare
			CheckinChangeSeatsResponse checkinChangeSeatsResponse = new CheckinChangeSeatsResponse();
			checkinChangeSeatsResponse = checkInDelegateRest.changeSeats(changeSeatsRequest);
			
			d++; //questo contatore indica quante volte viene invocato il delegate a seconda del numero dei segements
			
			//Viene Costruita una matrice di settori riguardante il volo ed i posti
			matrixFlightsSectorRender = new ArrayList<>();
			
			ArrayList<FlightsSectorRender> listFlightsSectorRender  =  new ArrayList<>();
			
			FlightsSectorRender flightsSectorRender = new FlightsSectorRender();
			
			flightsSectorRender.setAirline(checkinChangeSeatsResponse.get_changeseatsResp().getSegmentSeat().getAirline());
			flightsSectorRender.setFlight(checkinChangeSeatsResponse.get_changeseatsResp().getSegmentSeat().getFlight());
			flightsSectorRender.setFlightStatus(checkinChangeSeatsResponse.get_changeseatsResp().getSegmentSeat().getFlightStatus());
			flightsSectorRender.setCheckInGate(checkinChangeSeatsResponse.get_changeseatsResp().getSegmentSeat().getCheckInGate());
			flightsSectorRender.setDescriptionAirBus("AIRBUS ALITALIA 343");//da reperire
			flightsSectorRender.setMapSeats("2_2");//da calcolare e inserire
			
			
			int sectors = checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().size();
			
			for (int i = 0; i < sectors; i++){
				ArrayList<SectorRender> listSectorsRender = new ArrayList<>(); 
				int sectorsStartRow = Integer.parseInt(checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getStartingRow());
				int sectorsEndingRow = Integer.parseInt(checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getEndingRow());
				for (int s = sectorsStartRow; s <= sectorsEndingRow; s++){
					String rowNumber = checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getRowNumber();
					for(int r = 0; r < checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().size(); r++){
						SectorRender sectorRender = new SectorRender();
						
						sectorRender.setNumberSeat(checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(r).getNumber() + rowNumber);
						if (checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(r).getOccupatoDa() != null && checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(r).getPassengerId().equals(checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(r).getOccupatoDa())){
							if (checkinChangeSeatsResponse.getEsitoCambioPosto().get(i).isEsito()) {
								sectorRender.setEsito("Cambio posto avvenuto con successo");
							}											//TODO come comunicare l'esito al front-end? Sostituire con costante String?
							else {										//TODO prendere il nuovo posto da EsitoCambioPosto oppure da SeatMap?
								sectorRender.setEsito("Operazione non effettuata: posto non disponibile");
							}
							
							sectorRender.setName("Giancarlo");//Da reperire
							sectorRender.setSurName("Negrin");//Da reperire
							sectorRender.setFrequentFlyer("MILLEMIGLIA");//Da reperire
						}else{
							sectorRender.setName("");//Da reperire
							sectorRender.setSurName("");//Da reperire
							sectorRender.setFrequentFlyer("");//Da reperire
							
						}
						sectorRender.setIdPassenger(checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(r).getPassengerId()); 
						sectorRender.setOccupatoDa(checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(r).getOccupatoDa());
						sectorRender.setPrice(String.valueOf(checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(r).getPrice()));
						sectorRender.setConfortSeat(checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(r).getConfortSeat()); 
						sectorRender.setType(checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(r).getType());
						sectorRender.setAvailability(checkinChangeSeatsResponse.get_changeseatsResp().getSeatMaps().getSeatMapSectors().get(i).getSeatMapRows().get(s).getSeats().get(r).getAvailability());
						
						
						listSectorsRender.add(sectorRender);
					}
				}
				flightsSectorRender.setListSectorRender(listSectorsRender);
				listFlightsSectorRender.add(flightsSectorRender);
				this.matrixFlightsSectorRender.add(d, listFlightsSectorRender);
			}
		}
		catch (Exception e) {
			logger.error("[CheckinChangeSeats][initModel] - errore nella chiamata al Delegate da ChangeSeats");
			e.getMessage();
		}
		
	}

	public CheckinChangeSeatsResponse getChangeSeatsResponse() {
		return ChangeSeatsResponse;
	}

	public void setChangeSeatsResponse(CheckinChangeSeatsResponse changeSeatsResponse) {
		ChangeSeatsResponse = changeSeatsResponse;
	}

} */
