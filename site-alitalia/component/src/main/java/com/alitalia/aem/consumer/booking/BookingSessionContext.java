package com.alitalia.aem.consumer.booking;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.ECouponData;
import com.alitalia.aem.common.data.home.FlightSeatMapData;
import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.data.home.InsurancePolicyData;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.common.data.home.MetaSearchData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PaymentData;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.data.home.PropertiesData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.SeatMapData;
import com.alitalia.aem.common.data.home.StateData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.common.data.home.carnet.CarnetInfoCarnet;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.ResidencyTypeEnum;
import com.alitalia.aem.common.messages.home.CashAndMilesData;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.model.PassengersData;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.model.SearchPassengersNumber;
import com.alitalia.aem.consumer.booking.render.BrandRender;
import com.day.cq.i18n.I18n;

/**
 * The <code>BookingSessionContext</code> class contains the state of a booking
 * session, in order to be stored in the proper way (e.g. in HTTP session).
 * 
 * </p>It is meant to be initialized by the <code>BookingSession</code>
 * component service, and then provided to it as requested for booking
 * process management methods.</p>
 * 
 * <p>The values managed in the context shouldn't be modified
 * externally to the <code>BookingSession</code> component service.</p>
 */
public class BookingSessionContext {
	
	/* common data */

	public String sid;
	
	public String execution;
	
	public String cookie;
	
	public String sabreGateWayAuthToken;
	
	public String ipAddress;
	
	public BookingPhaseEnum phase;
	
	public Locale locale;
	
	public MMCustomerProfileData loggedUserProfileData;
	
	public Map<String, BrandPageData> brandMap;
	
	public Map<String, BrandPageData> codeBrandMap;

	/**
	 * Contains the domain extracted from URL
	 */
	public String domain;
	public String baseUrl;
	
	/* search data */
	
	public boolean newSearch;
	
	public BookingSearchKindEnum searchKind;
	
	public CabinEnum filterCabin;
	
	public List<SearchElement> searchElements;
	
	public SearchPassengersNumber searchPassengersNumber;
	
	public BookingSearchCUGEnum cug;
	
	public BookingSearchCategoryEnum searchCategory;
	
	public ResidencyTypeEnum residency;
	
	public String market;
	
	public String site;
	
	public NumberFormat currentNumberFormat;

	public String tradeAgencyCode;
	
	public String tradeAgencyContractCode;
	
	public String currency;
	
	public String symbolCurrency;
	
	public int[] selectedDepartureDateChoices;
	
	public MetaSearchData metaSearchData;
	
	public boolean isGiovaniLastminute;
	
	public boolean showRoundTrip;
	
	/* search result data */
	
	public String cartRecoveryParams;
	
	public Boolean isAllowedContinuitaTerritoriale;
	
	public Boolean isSelectedContinuitaTerritoriale;
	
	public List<PassengerBase> extraChargePassengerList;

	public AvailableFlightsData availableFlights;
	
	public Boolean isApis;
	
	public Boolean isSecureFlight;
	
	public Boolean isSecureFlightESTA;
	
	public Boolean youthSolutionFound;
	
	public Boolean familySolutionFound;
	
	public Boolean militarySolutionFound;
	
	public boolean territorialContinuitySolutionFound;
	
	public Boolean solutionForSelectedDate[];
	
	public Boolean solutionForAllDates;
	
	public Boolean mealsEnabled;
	
	public boolean isLimitedMeals;
	
	public Boolean insuranceEnabled;
	
	public int numberOfShownFlights[];
	
	public int numberOfMoreFlights;
	
	public int initialNumberOfFlightsToShow;
	
	public int totalSlices;
	
	/* flight selections data */
	
	public int firstSelectionIndex;
	
	public FlightSelection[] flightSelections;
	
	public RoutesData selectionRoutes;
	
	public List<PassengerBaseData> selectionRoutesPassengersWithoutCoupon;
	
	public List<TaxData> selectionTaxes;
	
	public List<DirectFlightData> selectionAllDirectFlights;
	
	public List<DirectFlightData>[] selectionSearchElementDirectFlights;
	
	public CabinEnum selectionCabinClass;
	
	public boolean isRefreshed;
	
	public HashMap<String, ArrayList<BrandRender>> missedBrandsMap;
	
	
	public InsurancePolicyData insuranceProposalData;
	
	public boolean isInsuranceApplied;
	
	public BigDecimal grossAmount;
	
	public BigDecimal grossAmountNoDiscount;
	
	public BigDecimal netAmountForPayment;
	
	public BigDecimal netAmount;
	
	public BigDecimal totalTaxes;
	
	public BigDecimal totalExtras;
	
	public BigDecimal totalExtraCharges;
	
	public BigDecimal totalCouponPrice;
	
	public BigDecimal insuranceAmount;
	
	public int currentSliceIndex;
	
	public boolean readyToPassengersDataPhase;
	
	public ArrayList<String> fullTextFareRules;
	
	public PropertiesData resultBookingDetails;
	
	/*introduced for analytics purpose*/
	public List<BrandData> preSellupBrandData;
	
	/* e-coupon data */
	
	public ECouponData coupon;
	
	public Boolean isCouponFanPlayr;
	
	public Boolean isCouponValid;
	
	public boolean isECouponWithTariffaLight;
	
	/* cash e miles data */
	
	public CashAndMilesData cashAndMiles;
	
	public boolean isCashMilesReady;
	
	public boolean isCashMilesApplied;
	
	public BigDecimal totalCashMilesPrice;
	
	
	/* passengers and seats data */
	
	public Boolean isSeatMapSelectionAllowed;
	
	public List<FlightSeatMapData> seatMaps;
	
	public Map<DirectFlightData, SeatMapData> seatMapsByFlight;
	
	public List<CountryData> countries;
	
	public List<CountryData> districts;
	
	public List<StateData> countriesUSA;
	
	public List<PhonePrefixData> phonePrefixes;
	
	public List<MealData> mealTypes;
	
	public List<FrequentFlyerTypeData> frequentFlyerTypes;
	
	public PassengersData passengersData;
	
	public boolean passengersSubmitted;
	
	
	/* prenotation data */
	
	public RoutesData prenotation;
	
	public PaymentData paymentData;
	
	public PaymentData paymentDataForMmb;
	
	public boolean isAuthorizePaymentInvoked;
	
	public String paymentTid;
	
	/* CC Fee */
	public boolean ccFeeApplied;
	
	public boolean ccFeeAddedToGrossAmount;
	
	public BigDecimal ccFeeTotalAmount;

	
	/* invoicing flags data */

	/**
	 * Indicates if user has requested invoice generation
	 */
	public Boolean invoiceRequired;

	/**
	 * Indicates if invoice generation request was successful
	 */
	public Boolean invoiceRequestSuccessful;
	
	/**
	 * Passenger's data from their profile
	 */
	public List<MMCustomerProfileData> profileData;
	
	/**
	 * Metasearch 
	 */
	
	public boolean metaSearch = false;

	/**
	 * PDF 
	 */
	public PaymentData paymentDataForPDF;
	
	/**
	 * Dati per statistiche
	 */
	public String clientIP;

	public String sessionId;
	
	/**
	 * Definisce se il Booking è di tipo Award
	 */
	public boolean award = false;
	
	public BigDecimal totalAwardPrice;
	
	// Calendar Search Award
	public int[] calendarSearchDeparture = new int[2];
	public int[] calendarSearchReturn = new int[2];
	
	public boolean calendarSearchModified;
	
	// carnet 
	public boolean isCarnetProcess;

	public CarnetInfoCarnet infoCarnet;

	public boolean updateCarnetSuccess;

	public Map<String, String> carnetFareRules;

	public boolean sendNotificationCarnetEmailSuccess;

	
	public boolean isCarnetValid;

	public boolean isBusCarrier;

	public boolean busIsSelected;

	public String cuit;
	
	public I18n i18n;

	
	@Override
	public String toString() {
		return "BookingSessionContext [sid=" + sid + ", execution=" + execution
				+ ", cookie=" + cookie + ", sabreGateWayAuthToken="
				+ sabreGateWayAuthToken + ", phase=" + phase + ", locale="
				+ locale + ", loggedUserProfileData=" + loggedUserProfileData
				+ ", brandMap=" + brandMap + ", codeBrandMap=" + codeBrandMap
				+ ", domain=" + domain + ", baseUrl=" + baseUrl
				+ ", newSearch=" + newSearch + ", searchKind=" + searchKind
				+ ", filterCabin=" + filterCabin + ", searchElements="
				+ searchElements + ", searchPassengersNumber="
				+ searchPassengersNumber + ", cug=" + cug + ", searchCategory="
				+ searchCategory + ", residency=" + residency + ", market="
				+ market + ", site=" + site + ", currentNumberFormat="
				+ currentNumberFormat + ", tradeAgencyCode=" + tradeAgencyCode
				+ ", tradeAgencyContractCode=" + tradeAgencyContractCode
				+ ", currency=" + currency + ", symbolCurrency="
				+ symbolCurrency + ", selectedDepartureDateChoices="
				+ Arrays.toString(selectedDepartureDateChoices)
				+ ", metaSearchData=" + metaSearchData
				+ ", isGiovaniLastminute=" + isGiovaniLastminute
				+ ", showRoundTrip=" + showRoundTrip + ", cartRecoveryParams="
				+ cartRecoveryParams + ", isAllowedContinuitaTerritoriale="
				+ isAllowedContinuitaTerritoriale
				+ ", isSelectedContinuitaTerritoriale="
				+ isSelectedContinuitaTerritoriale
				+ ", extraChargePassengerList=" + extraChargePassengerList
				+ ", availableFlights=" + availableFlights + ", isApis="
				+ isApis + ", isSecureFlight=" + isSecureFlight
				+ ", isSecureFlightESTA=" + isSecureFlightESTA
				+ ", youthSolutionFound=" + youthSolutionFound
				+ ", familySolutionFound=" + familySolutionFound
				+ ", militarySolutionFound=" + militarySolutionFound
				+ ", territorialContinuitySolutionFound="
				+ territorialContinuitySolutionFound
				+ ", solutionForSelectedDate="
				+ Arrays.toString(solutionForSelectedDate)
				+ ", solutionForAllDates=" + solutionForAllDates
				+ ", mealsEnabled=" + mealsEnabled + ", isLimitedMeals="
				+ isLimitedMeals + ", insuranceEnabled=" + insuranceEnabled
				+ ", numberOfShownFlights="
				+ Arrays.toString(numberOfShownFlights)
				+ ", numberOfMoreFlights=" + numberOfMoreFlights
				+ ", initialNumberOfFlightsToShow="
				+ initialNumberOfFlightsToShow + ", totalSlices=" + totalSlices
				+ ", firstSelectionIndex=" + firstSelectionIndex
				+ ", flightSelections=" + Arrays.toString(flightSelections)
				+ ", selectionRoutes=" + selectionRoutes
				+ ", selectionRoutesPassengersWithoutCoupon="
				+ selectionRoutesPassengersWithoutCoupon + ", selectionTaxes="
				+ selectionTaxes + ", selectionAllDirectFlights="
				+ selectionAllDirectFlights
				+ ", selectionSearchElementDirectFlights="
				+ Arrays.toString(selectionSearchElementDirectFlights)
				+ ", selectionCabinClass=" + selectionCabinClass
				+ ", isRefreshed=" + isRefreshed + ", missedBrandsMap="
				+ missedBrandsMap + ", insuranceProposalData="
				+ insuranceProposalData + ", isInsuranceApplied="
				+ isInsuranceApplied + ", grossAmount=" + grossAmount
				+ ", grossAmountNoDiscount=" + grossAmountNoDiscount
				+ ", netAmountForPayment=" + netAmountForPayment
				+ ", netAmount=" + netAmount + ", totalTaxes=" + totalTaxes
				+ ", totalExtras=" + totalExtras + ", totalExtraCharges="
				+ totalExtraCharges + ", totalCouponPrice=" + totalCouponPrice
				+ ", insuranceAmount=" + insuranceAmount
				+ ", currentSliceIndex=" + currentSliceIndex
				+ ", readyToPassengersDataPhase=" + readyToPassengersDataPhase
				+ ", fullTextFareRules=" + fullTextFareRules
				+ ", resultBookingDetails=" + resultBookingDetails
				+ ", coupon=" + coupon + ", isCouponFanPlayr="
				+ isCouponFanPlayr + ", isCouponValid=" + isCouponValid
				+ ", isECouponWithTariffaLight=" + isECouponWithTariffaLight
				+ ", cashAndMiles=" + cashAndMiles + ", isCashMilesReady="
				+ isCashMilesReady + ", isCashMilesApplied="
				+ isCashMilesApplied + ", totalCashMilesPrice="
				+ totalCashMilesPrice + ", isSeatMapSelectionAllowed="
				+ isSeatMapSelectionAllowed + ", seatMaps=" + seatMaps
				+ ", seatMapsByFlight=" + seatMapsByFlight + ", countries="
				+ countries + ", districts=" + districts + ", countriesUSA="
				+ countriesUSA + ", phonePrefixes=" + phonePrefixes
				+ ", mealTypes=" + mealTypes + ", frequentFlyerTypes="
				+ frequentFlyerTypes + ", passengersData=" + passengersData
				+ ", passengersSubmitted=" + passengersSubmitted
				+ ", prenotation=" + prenotation + ", paymentData="
				+ paymentData + ", paymentDataForMmb=" + paymentDataForMmb
				+ ", paymentTid=" + paymentTid + ", ccFeeApplied="
				+ ccFeeApplied + ", ccFeeAddedToGrossAmount="
				+ ccFeeAddedToGrossAmount + ", ccFeeTotalAmount="
				+ ccFeeTotalAmount + ", invoiceRequired=" + invoiceRequired
				+ ", invoiceRequestSuccessful=" + invoiceRequestSuccessful
				+ ", profileData=" + profileData + ", metaSearch=" + metaSearch
				+ ", paymentDataForPDF=" + paymentDataForPDF + ", clientIP="
				+ clientIP + ", sessionId=" + sessionId + ", award=" + award
				+ ", totalAwardPrice=" + totalAwardPrice
				+ ", calendarSearchDeparture="
				+ Arrays.toString(calendarSearchDeparture)
				+ ", calendarSearchReturn="
				+ Arrays.toString(calendarSearchReturn)
				+ ", calendarSearchModified=" + calendarSearchModified
				+ ", isCarnetProcess=" + isCarnetProcess + ", infoCarnet="
				+ infoCarnet + ", updateCarnetSuccess=" + updateCarnetSuccess
				+ ", carnetFareRules=" + carnetFareRules
				+ ", sendNotificationCarnetEmailSuccess="
				+ sendNotificationCarnetEmailSuccess + ", isCarnetValid="
				+ isCarnetValid + ", isBusCarrier=" + isBusCarrier
				+ ", busIsSelected=" + busIsSelected + ", cuit=" + cuit
				+ ", ipAddress=" + ipAddress
				+ "]";
	}
}