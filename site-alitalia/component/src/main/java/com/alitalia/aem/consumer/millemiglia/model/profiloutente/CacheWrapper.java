package com.alitalia.aem.consumer.millemiglia.model.profiloutente;

public class CacheWrapper<T> {

    private long cacheDuration;
    private long insertTimestamp;
    private T data;

    public CacheWrapper(T obj, long duration) {
        this.data = obj;
        this.cacheDuration = duration;
        insertTimestamp = System.currentTimeMillis();
    }
    public boolean valid() {
        return System.currentTimeMillis() - insertTimestamp <= cacheDuration;
    }

    public T getValue() {
        return data;
    }
}
