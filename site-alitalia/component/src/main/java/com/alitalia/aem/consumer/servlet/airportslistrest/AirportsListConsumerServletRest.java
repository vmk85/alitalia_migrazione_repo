package com.alitalia.aem.consumer.servlet.airportslistrest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaGeolocalizationManager;
import com.alitalia.aem.consumer.AlitaliaGeolocalizationManager.AlitaliaGeolocalizationData;
import com.alitalia.aem.consumer.servlet.airportslist.AirportInfo;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.staticdata.delegaterest.StaticDataDelegateRest;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "airportslistconsumerrest", "airportslistconsumergeorest" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class AirportsListConsumerServletRest extends SlingSafeMethodsServlet {
	
		// logger
		private Logger logger = LoggerFactory.getLogger(this.getClass());

		// Geolocalization Manager
		@Reference(policy = ReferencePolicy.DYNAMIC)
		private volatile AlitaliaGeolocalizationManager alitaliaGeolocalizationManager;
		
		//Search Airports
		@Reference(policy = ReferencePolicy.DYNAMIC)
		private volatile StaticDataDelegateRest staticDataDelegateRest;

		@Override
		protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
				throws ServletException, IOException {
			// content type
			response.setContentType("application/json");

			// JSON response
			TidyJSONWriter jsonOutput = new TidyJSONWriter(response.getWriter());
			try {
				
				// prepare resource bundle for i18n
				Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
				ResourceBundle resourceBundle = request.getResourceBundle(locale);
				final I18n i18n = new I18n(resourceBundle);
			
			// setting variables for airport json object
			String airportCode;
			String keyAirportName;
			String airportName;
			String keyCityCode;
			String cityCode;
			String city;
			String countryCode;
			String keyCountryCode;
			String country;
				
			// search servlet selector
			String[] geoLoc = request.getRequestPathInfo().getSelectors();
			
			// geolocalization
			if (geoLoc.length > 0 && geoLoc[0].equals("airportslistconsumergeorest")) {
				
				// out JSON response
				jsonOutput.object();
				
				// getting geolocalization data
				AlitaliaGeolocalizationData alitaliaGeolocalizationData = 
						alitaliaGeolocalizationManager.processFromRequest(request, response);
				
				// gelocated airport
				if (alitaliaGeolocalizationData != null) {
					logger.debug("[AirportsListConsumerServlet] alitaliaGeolocalizationData: " + alitaliaGeolocalizationData.toCookieString());

					// airport's name & city 
					airportCode = alitaliaGeolocalizationData.getCityCode();
					keyAirportName = "airportsData." + airportCode + ".name";
					airportName = i18n.get(keyAirportName);
					keyCityCode = "airportsData." + airportCode + ".city";
					city = i18n.get(keyCityCode);
					
					// airport's country
					countryCode = alitaliaGeolocalizationData.getCountryCode();
					keyCountryCode = "airportsData." + countryCode + ".country";
					country = i18n.get(keyCountryCode);
					
					// JSON object
					jsonOutput.key("geoAirport");
					jsonOutput.object();
					jsonOutput.key("value").value(city + ", " + airportName + ", " + country);
					jsonOutput.key("city").value(city);
					jsonOutput.key("country").value(country);
					jsonOutput.key("airport").value(airportName);
					jsonOutput.key("code").value(airportCode);
					jsonOutput.key("type").value(airportCode);
					jsonOutput.endObject();
					
				} else{
					logger.debug("[AirportsListConsumerServlet] Errore durante la geolocalizzazione");
				}
				
			}
			
			// otherwise airportslist
			else {
			
				RetrieveAirportsRequest serviceRequest = new RetrieveAirportsRequest(IDFactory.getTid(), IDFactory.getSid(request));
				String languageCode =  AlitaliaUtils.getRepositoryPathLanguage(request.getResource());//findResourceLocale(slingHttpServletRequest.getResource()).getLanguage();
				String marketCode = AlitaliaUtils.getRepositoryPathMarket(request.getResource());//AlitaliaUtils.findResourceLocale(slingHttpServletRequest.getResource()).getCountry();
				String conversationId = IDFactory.getTid();
				//System.out.println("conversationID is " + conversationId);
				serviceRequest.setMarket(marketCode);
				serviceRequest.setLanguageCode(languageCode);
				serviceRequest.setConversationID(conversationId);
				logger.info("Chiamando StaticDataDelegateRest da AirportsListServlet...");
				RetrieveAirportsResponse serviceResponse = staticDataDelegateRest.getAllAirports(serviceRequest);
				logger.info("Dopo StaticDataDelegateRest da AirportsListServlet...");
				// out JSON response
				jsonOutput.object();
			
				/* Lista degli areoporti presi dal servizio */
				jsonOutput.key("airports");
				jsonOutput.array();
				
				List<AirportData> list = serviceResponse.getAirports();
				List<AirportInfo> airportList = new ArrayList<AirportInfo>();
				for (AirportData airportData : list) {
					
					airportCode = airportData.getCode();
					keyAirportName = "airportsData." + airportCode + ".name";
					airportName = i18n.get(keyAirportName);
					
					cityCode = airportData.getCityCode();
					keyCityCode = "airportsData." + airportCode + ".city";
					city = i18n.get(keyCityCode);
					
					countryCode = airportData.getCountryCode();
					keyCountryCode = "airportsData." + countryCode + ".country";
					country = i18n.get(keyCountryCode);
					
					// add to JSON only if translation is provided
					if (!city.equals(keyCityCode) &&
							!country.equals(keyCountryCode) &&
							!airportName.equals(keyAirportName)) {
						AirportInfo airport =
								new AirportInfo(
									airportCode, keyAirportName,
									airportName, cityCode, keyCityCode, city,
									countryCode, keyCountryCode, country);
						airportList.add(airport);
					}
				}
				Collections.sort(airportList,
						AirportInfo.COMPARE_BY_CITY_AND_AIRPORT);
				
				
				for (AirportInfo airportInfo : airportList) {
					
					jsonOutput.object(); 
					jsonOutput.key("value").value(airportInfo.getCity()
							+ ", " + airportInfo.getAirportName()
							+ ", " + airportInfo.getCountry());
					jsonOutput.key("city").value(airportInfo.getCity());
					jsonOutput.key("country").value(airportInfo.getCountry());
					jsonOutput.key("airport").value(airportInfo.getAirportName());
					jsonOutput.key("code").value(airportInfo.getAirportCode());
					jsonOutput.key("type").value(airportInfo.getCityCode());
					//TODO aggiungere marketAreaCode al json?
					
					// internationalization keys
					jsonOutput.key("keyCityCode").value(
							airportInfo.getKeyCityCode());
					jsonOutput.key("keyCountryCode").value(
							airportInfo.getKeyCountryCode());
					jsonOutput.key("keyAirportName").value(
							airportInfo.getKeyAirportName());
					
					jsonOutput.endObject();
				}
	
					jsonOutput.endArray();
				
					jsonOutput.key("conversationID").value(serviceResponse.getConversationID());
				}
				jsonOutput.key("result").value("OK");
				jsonOutput.endObject();
				//Fine JsonoOutput ---> "result" = "OK" Viene restituito anche in caso di Geolocalizzazione a NULL
			}catch (Exception e) {
				e.printStackTrace();
				
				//logger.error("Errore durante recupero Airport List Consumer", e);
				throw new RuntimeException("Errore durante recupero Airport List Consumer", e);
			}
		}

	
}
