package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbDetail extends MmbSessionAncillaryGenericController {

	@Self
	private SlingHttpServletRequest request;
	private boolean preselectCustomizeSection;
	
	@PostConstruct
	protected void initModel() throws IOException {
		try {
			initBaseModel(request);
			String[] selectors = request.getRequestPathInfo().getSelectors();
			if (selectors.length > 0 && "customize".equalsIgnoreCase(selectors[0])) {
				preselectCustomizeSection = true;
			} else {
				preselectCustomizeSection = false;
			}
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}
	}
	
	public boolean isPreselectCustomizeSection() {
		return preselectCustomizeSection;
	}
	
}
