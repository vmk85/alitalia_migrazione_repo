package com.alitalia.aem.consumer.model.socialshare;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;

public abstract class BaseSocialShareModel extends GenericBaseModel {
	
	protected String title;
	protected String description;
	protected String imagePath;
	protected String linkBack;

	public String getTitle() {
		return title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getImagePath() {
		return imagePath;
	}
	
	public String getLinkBack() {
		return linkBack;
	}
	
}
