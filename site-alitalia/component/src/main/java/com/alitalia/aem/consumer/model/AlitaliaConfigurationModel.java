package com.alitalia.aem.consumer.model;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSearchCategoryEnum;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.wcm.api.Page;

/**
 * This model exposes all Alitalia consumer site general configuration
 * variables held by the <code>AlitaliaConfigurationModel</code> service.
 * 
 * @see AlitaliaConfigurationHolder
 */

@Model(adaptables={ Resource.class })
public class AlitaliaConfigurationModel {

	@Self
    private Resource resource;
	
    @Inject
    private AlitaliaConfigurationHolder configuration;

    public String getWidgetADRKey(){ return configuration.getWidgetadrKey(); }

    public String getMyalitaliaPaymentKey(){ return configuration.getMyalitaliaPaymentKey(); }

    public String getMyalitaliaPaymentLibraryUrl(){ return configuration.getMyalitaliaPaymentLibraryUrl(); }

    public Boolean getLegacyLoginEnabled() {
    	return configuration.getLegacyLoginEnabled();
    }
    
    public String getLegacyLoginApBaseUrl() {
    	return configuration.getLegacyLoginApBaseUrl();
    }
    
    public String getLegacyLoginMmBaseUrl() {
    	return configuration.getLegacyLoginMmBaseUrl();
    }
    
    public String getSocialLoginEnabledProviders() {
    	return configuration.getSocialLoginEnabledProviders();
    }
    
    public String getSocialLoginApiKey() {
    	return configuration.getSocialLoginApiKey();
    }

	public String getMASocialLoginApiKey() {
		return configuration.getMaApiKey();
	}

    public String getHomePage() {
    	return configuration.getHomePage();
    }
    
    public String getIndexPage() {
    	return configuration.getIndexPage();
    }
    
    public String getHomePageWithExtension() {
    	return configuration.getHomePageWithExtension();
    }
    
    public String getOfferDetailPage() {
    	return configuration.getOfferDetailPage();
    }
    
    public String getOfferDetailPageWithExtension() {
    	return configuration.getOfferDetailPageWithExtension();
    }
    
    public Boolean getSocialShareEnabled() {
    	return configuration.getSocialShareEnabled();
    }
    
    public String getSocialShareEnabledProviders() {
    	return configuration.getSocialShareEnabledProviders();
    }
    
    public String getSocialShareMoreEnabledProviders() {
    	return configuration.getSocialShareMoreEnabledProviders();
    }
    
    public String getSocialShareOperationMode() {
    	return configuration.getSocialShareOperationMode();
    }
    
    public Boolean getSocialShareShowMoreButton() {
    	return configuration.getSocialShareShowMoreBUtton();
    }
    
    public Boolean getSocialShareShowEmailButton() {
    	return configuration.getSocialShareShowEmailBUtton();
    }
    
    public String getContext() {
    	return configuration.getContext();
    }
    
    public String getDefaultSite() {
    	return configuration.getDefaultSite();
    }
    
    public String getTemplateMailNewsletter() {
    	return configuration.getTemplateMailNewsletter();
    }
    
    public String getGSAHost() {
    	return configuration.getGSAHost();
    }
    
    public String getGSAResultPerPage() {
    	return configuration.getGSAResultPerPage();
    }
    
    public String getGSAPageOffset() {
    	return configuration.getGSAPageOffset();
    }
    
    public String getGSASite() {
    	return configuration.getGSASite();
    }
    
    public String getGSAClient() {
    	return configuration.getGSAClient();
    }
    
    public String getGSAAllow() {
    	return configuration.getGSAAllow();
    }
    
    public String getMetaNoIndexAllow() {
    	return configuration.getMetaNoIndexAllow();
    }
    
    public String getPageNotFound() {
    	return configuration.getPageNotFound();
    }
    
    public String getExternalDomain() {
    	return configuration.getExternalDomain();
    }
    
    public String getBookingFlightSelectPage() {
		return configuration.getBookingFlightSelectPage();
	}
    
    public String getBookingFlightSelectPageWithExtension() {
    	return AlitaliaUtils.findSiteBaseExternalUrl(resource, false) + getBookingFlightSelectPage();
	}
	
	public String getBookingPassengersDataPage() {
		return configuration.getBookingPassengersDataPage();
	}
	
	public String getBookingAncillaryPage() {
		return configuration.getBookingAncillaryPage();
	}
	
	public String getBookingPaymentPage() {
		return configuration.getBookingPaymentPage();
	}
	
	public String getBookingPaymentReturnPage() {
		return configuration.getBookingPaymentReturnPage();
	}
	
	public String getBookingPaymentGlobalCollectReturnPage() {
		return configuration.getBookingPaymentGlobalCollectReturnPage();
	}
	
	public String getBookingPaymentFailurePage() {
		return configuration.getBookingPaymentFailurePage();
	}
	
	public String getBookingPaymentCancelPage() {
		return configuration.getBookingPaymentCancelPage();
	}
	
	public String getBookingConfirmationPage() {
		return configuration.getBookingConfirmationPage();
	}
	
	public String getBookingFailurePage() {
		return configuration.getBookingFailurePage();
	}
	
	public String getMinYearADT() {
		return configuration.getMinYearADT();
	}
	
	public String getMaxYearADT() {
		return configuration.getMaxYearADT();
	}
	
	public String getMinYearCHD() {
		return configuration.getMinYearCHD();
	}
	
	public String getMaxYearCHD() {
		return configuration.getMaxYearCHD();
	}
	
	public String getMinYearINF() {
		return configuration.getMinYearINF();
	}
	
	public String getMaxYearINF() {
		return configuration.getMaxYearINF();
	}
	
	public String[] getSearchCategoryBrands(BookingSearchCategoryEnum category) {
		return configuration.getSearchCategoryBrands(category);
	}
    
	public String getCustomerNotFoundRetrievePin() {
		return configuration.getCustomerNotFoundRetrievePin();
	}
	
	public String getMmbPaymentPage() {
		return AlitaliaUtils.findSiteBaseExternalUrl(resource, false) 
				+ configuration.getMmbPaymentPage();
	}
	
	public String getMmbThankyouPage() {
		return AlitaliaUtils.findSiteBaseExternalUrl(resource, false) 
				+ configuration.getMmbThankyouPage();
	}

	public String getExternalLinkConfig(){
		return configuration.getExternalLinkConfig();
	}
	
	public String getExternalLinkMarkets(){
		return configuration.getExternalLinkMarkets();
	}
	
	
    /**
     * Utility method to resolve and map the external URL of the home page
     * for the current site.
     */
    public String getResolvedHomePage() {
    	return resource.getResourceResolver().map(
    			AlitaliaUtils.findSiteBaseExternalUrl(resource, false));
	}
    
    /**
     * Utility method to resolve and map the external URL of flight info list
     * (time table) for the current site.
     */
    public String getResolvedFlightInfoList() {
    	return resource.getResourceResolver().map(
    			AlitaliaUtils.findSiteBaseExternalUrl(resource, false)) + configuration.getBookingFlightInfoListPage();
	}
    
    /**
     * Utility method to resolve and map the external URL of the personal area
     * for the current site.
     */
    public String getResolvedPersonalArea() {
    	return resource.getResourceResolver().map(
    			AlitaliaUtils.findSiteBaseExternalUrl(resource, true)) + configuration.getBasePersonalArea();
	}
    
    /**
     * Utility method to resolve and map the external URL of the offer detail page
     * for the current site.
     */
    public String getResolvedOfferDetailPage() {
    	return resource.getResourceResolver().map(
    			AlitaliaUtils.findSiteBaseExternalUrl(resource, true)
    			+ getOfferDetailPageWithExtension());
    }
    
    /**
     * Utility method to resolve and map the external URL of the j_security_check
     */
    public String getResolvedJSecurityCheck(){
    	String protocol = "http://";
    	if(configuration.getHttpsEnabled()){
    		protocol = "https://";
    	} 
    	return protocol + getExternalDomain() + 
    			resource.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(resource, true) + AlitaliaConstants.J_SECURITY_CHECK);
    }
    
    /**
     * Utility method to resolve and map the external URL of the Mille Miglia Logout URL
     */
    public String getResolvedMilleMigliaLogout(){
    	String protocol = "http://";
    	if(configuration.getHttpsEnabled()){
    		protocol = "https://";
    	} 
    	return protocol + getExternalDomain() + 
    			resource.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(resource, true)) + AlitaliaConstants.MM_LOGOUT_SELECTOR;
    }
    
    public String getMarketAndLanguageCodes() {
    	return AlitaliaUtils.getRepositoryPathMarket(resource) + "_" +
    			AlitaliaUtils.getRepositoryPathLanguage(resource);
    }
    
    public String getCurrentPage() {
    	Page page = AlitaliaUtils.getPage(resource);
    	if(page == null || (page != null && isHomePage(page.getPath()))){
    		return getResolvedHomePage();
    	} else{
    		String path = resource.getResourceResolver().map(page.getPath());
    		return path + ".html";
    	}
    }

	public String getCurrentPageFullUrl() {
		Page page = AlitaliaUtils.getPage(resource);
		if(page == null || (page != null && isHomePage(page.getPath()))){
			return getResolvedHomePage();
		} else{
			String protocol = "http://";
			if(configuration.getHttpsEnabled()){
				protocol = "https://";
			}
			return protocol + getExternalDomain() + resource.getResourceResolver().map(page.getPath()) + ".html";
		}
	}

    public String getCheckinBasePath() {
        String protocol = "http://";
        if(configuration.getHttpsEnabled()){
            protocol = "https://";
        }
        return protocol + getExternalDomain() +
                resource.getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(resource, true)) + "check-in-search";
    }

    private boolean isHomePage(String page){
    	if(getHomePage() != null){
    		return page.endsWith(getHomePage());
    	} else{
    		return false;
    	}
    }
}
