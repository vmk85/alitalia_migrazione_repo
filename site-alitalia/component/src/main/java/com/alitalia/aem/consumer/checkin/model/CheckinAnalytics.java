package com.alitalia.aem.consumer.checkin.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.enumerations.CheckinPassengerStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.analytics.CheckinAnalyticsInfo;
import com.alitalia.aem.consumer.checkin.analytics.CheckinAnalyticsInfoFiller;
import com.alitalia.aem.consumer.checkin.analytics.CheckinAnalyticsInfoToJsObj;
import com.alitalia.aem.consumer.checkin.render.WTMetaRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CheckinAnalytics extends GenericCheckinModel {
	
	@Inject
	AlitaliaConfigurationHolder configurationHolder;
	
	@Self
	private SlingHttpServletRequest request;
	
	private static final String EN_FLIGHT_DATE_FORMAT = "MM/dd/yyyy";
	private static final String TIME_FORMAT_HH_MM_SS = "kk:mm:ss";
	
	private int step;
	
	private WTMetaRender wtMeta = new WTMetaRender();
	private String webTrendsMeta;
	
	private String dlVar = "";
	
	private DecimalFormat format;
	private SimpleDateFormat dateFormat_en;
	private SimpleDateFormat timeFormat;
	private Calendar now;
	
	@PostConstruct
	protected void initModel() {
		try {
			initBaseModel(request);
			initFormats();
			String path = request.getRequestURL().toString();
			if (request.getRequestPathInfo().getSelectorString() != null &&
					!request.getRequestPathInfo().getSelectorString().isEmpty()) {
				path = request.getRequestURL().toString().replace(
						request.getRequestPathInfo().getSelectorString() + ".", "");
			}
			step = computeCheckInStep(path);
			webTrendsMeta = computeWTMeta();
			
			/* dataLayer Enhancement*/
			CheckinAnalyticsInfoFiller filler = new CheckinAnalyticsInfoFiller();
			CheckinAnalyticsInfoToJsObj toJsObj = new CheckinAnalyticsInfoToJsObj();
			CheckinAnalyticsInfo info = filler.fillInfo(ctx, step);
			dlVar = toJsObj.toJSObj(info, false);
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			dlVar = "{}";
			webTrendsMeta = "{}";
		}
	}
	
	private void initFormats() {
		format = new DecimalFormat("#0.00");
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setCurrencySymbol(""); 
		format.setDecimalFormatSymbols(otherSymbols);
		format.setGroupingUsed(false);
		format.setPositivePrefix("");
		format.setNegativePrefix("");
		dateFormat_en = new SimpleDateFormat(EN_FLIGHT_DATE_FORMAT);
		dateFormat_en.setLenient(false);
		timeFormat = new SimpleDateFormat(TIME_FORMAT_HH_MM_SS);
		timeFormat.setLenient(false);
		now = Calendar.getInstance();
		now.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
		
	}
	
	
	private int computeCheckInStep(String path){
		int step = 0;
		/*Flight List*/
		if(path.endsWith(configurationHolder.getCheckinFlightListPage())){
			step = 1;
		}
		/*Passenger data*/
		else if(path.endsWith(configurationHolder.getCheckinPassengersPage())){
			step = 2;
		}
		/*Ancillary*/
		else if(path.endsWith(configurationHolder.getCheckinAncillaryPage())){
			step = 3;
		}
		/*Ancillary Payment*/
		else if(path.endsWith(configurationHolder.getCheckinPaymentPage())){
			step = 4;
		}
		/*Ancillary Confirmation*/
		else if(path.endsWith(configurationHolder.getCheckinAncillaryConfirmationPage())){
			step = 5;
		}
		/*Boarding pass*/
		else if(path.endsWith(configurationHolder.getCheckinBoardingPassPage())){
			step = 6;
		}
		/*Confirmation*/
		else if(path.endsWith(configurationHolder.getCheckinConfirmationPage())){
			step = 7;
		}
		/*Undo checkin*/
		else if(path.endsWith(configurationHolder.getCheckinUndoPage())){
			step = 8;
		}
		return step;
	}
	
	private String computeWTMeta() {
		switch (step) {
		case 1:
			computeObjMeta_flightList();
			break;
		case 2:
			computeObjMeta_paxData();
			break;
		case 3:
			computeObjMeta_ancillary();
			break;
		case 4:
			computeObjMeta_ancillaryPayment();
			break;
		case 5:
			computeObjMeta_ancillaryReceipt();
			break;
		case 6:
			computeObjMeta_BoardingPass();
			break;
		case 7:
			computeObjMeta_receipt();
			break;
		case 8:
			computeObjMeta_undo();
			break;
		default:
		}
		String jsVar = "var analytics_wt_meta = {";
		jsVar += "'WT.si_n': '" + wtMeta.si_n+ "', ";
		jsVar += "'WT.si_x': '" + wtMeta.si_x+ "', ";
		jsVar += "'WT.si_cs': '" + wtMeta.si_cs+ "', ";
		jsVar += "'WT.cg_n': '" + wtMeta.cg_n+ "', ";
		jsVar += "'WT.cg_s': '" + wtMeta.cg_s+ "', ";
		jsVar += "'DCSext.wck_v2 _control': '" + wtMeta.wck_v2_control+ "', ";
		jsVar += "'DCSext.wci_control': '" + wtMeta.wci_control+ "', ";
		jsVar += "'DCSext.wci_newwebcheckin': '" + wtMeta.wci_newwebcheckin+ "', ";
		jsVar += "'DCSext.wci_ristampa': '" + wtMeta.wci_ristampa+ "', ";
		jsVar += "'DCSext.wck_v2_riepilogoserviziapagamento': '" + wtMeta.wck_v2_riepilogoserviziapagamento+ "', ";
		jsVar += "'DCSext.arsummary': '" + wtMeta.arsummary+ "', ";
		jsVar += "'DCSext.wck_v2_riepilogoservizigratuiti': '" + wtMeta.wck_v2_riepilogoservizigratuiti+ "', ";
		jsVar += "'DCSext.wck_v2_ricevutaserviziapagamento': '" + wtMeta.wck_v2_ricevutaserviziapagamento+ "', ";
		jsVar += "'DCSext.wck_v2_quantitaemd': '" + wtMeta.wck_v2_quantitaemd+ "', ";
		jsVar += "'DCSext.arproduct': '" + wtMeta.arproduct+ "', ";
		jsVar += "'DCSext.arquantity': '" + wtMeta.arquantity+ "', ";
		jsVar += "'WT.pn_gr': '" + wtMeta.pn_gr+ "', ";
		jsVar += "'WT.pn_fa': '" + wtMeta.pn_fa+ "', ";
		jsVar += "'WT.pc': '" + wtMeta.pc+ "', ";
		jsVar += "'WT.pn_sc': '" + wtMeta.pn_sc+ "', ";
		jsVar += "'WT.pn_sku': '" + wtMeta.pn_sku+ "', ";
		jsVar += "'WT.tx_u': '" + wtMeta.tx_u+ "', ";
		jsVar += "'WT.tx_s': '" + wtMeta.tx_s+ "', ";
		jsVar += "'WT.tx_e': '" + wtMeta.tx_e+ "', ";
		jsVar += "'WT.tx_i': '" + wtMeta.tx_i+ "', ";
		jsVar += "'WT.tx_id': '" + wtMeta.tx_id+ "', ";
		jsVar += "'WT.tx_it': '" + wtMeta.tx_it+ "', ";
		jsVar += "'WT.tx_curr': '" + wtMeta.tx_curr+ "' ";


		jsVar += "};\n";
		return jsVar;
	}


	private void computeObjMeta_flightList() {
		wtMeta.si_n = "IntroNewWebCheckIn";
		wtMeta.si_x = "2";
		wtMeta.si_cs = "1";
		wtMeta.cg_n = "WebCheckin";
		wtMeta.cg_s = "SceltaVolo";
		wtMeta.wci_control = "aem_webcheckin_2016";
	}
	
	private void computeObjMeta_paxData() {
		wtMeta.wci_control = "aem_webcheckin_2016";
		PassengerAnalyticsInfo paxInfo = getPassengerInfo();
		if(paxInfo.isApiStep){
			wtMeta.si_n = "Api";
			wtMeta.si_x = "1";
			wtMeta.cg_n = "WebCheckin";
			wtMeta.cg_s = "Api";
			wtMeta.si_cs = "1";
		}
		else if(paxInfo.isApiPlusStep){
			wtMeta.si_n = "ApiPlus";
			wtMeta.cg_n = "WebCheckin";
			wtMeta.cg_s = "ApiPlus";
			if(paxInfo.isApiPlusStep2){
				wtMeta.si_x = "1";
			}
			else{
				wtMeta.si_x = "2";
				wtMeta.si_cs = "1";
			}
		}
		else{
			wtMeta.si_n = "NewWebCheckIn";
			wtMeta.si_x = "1";
			wtMeta.cg_n = "WebCheckin";
			wtMeta.cg_s = "BaseWebCheckin";
		}
	}
	
	private void computeObjMeta_ancillary() {
		
		wtMeta.si_n = "NewWebCheckIn";
		wtMeta.si_x = "2";
		wtMeta.cg_n = "WebCheckin";
		wtMeta.cg_s = "BaseWebCheckin";
		wtMeta.wci_control = "aem_webcheckin_2016";
		wtMeta.wck_v2_control = "wck_aem_gen_2016";
		wtMeta.pn_gr = "Alitalia";
		wtMeta.pn_fa = "Alitalia";
		wtMeta.pc = "Webcheckin";
		wtMeta.pn_sc = "Ancillary";
		wtMeta.pn = "CKINAncillary";
		wtMeta.pn_sku = "?";
		wtMeta.tx_u = "?";
		wtMeta.tx_e = "?";
	}
	
	private void computeObjMeta_BoardingPass() {
		if(!isBoardingPassReprint()){
			wtMeta.si_n = "NewWebCheckIn";
			wtMeta.si_x = "3";
			wtMeta.cg_n = "WebCheckin";
			wtMeta.cg_s = "BaseWebCheckin";
			wtMeta.wci_control = "aem_webcheckin_2016";
		}
	}
	
	private void computeObjMeta_receipt() {
		wtMeta.si_n = "NewWebCheckIn";
		wtMeta.si_x = "4";
		wtMeta.cg_n = "WebCheckin";
		wtMeta.wci_control = "aem_webcheckin_2016";
		
		if(isBoardingPassReprint()){
			wtMeta.wci_newwebcheckin = "newwebcheckin";
			wtMeta.wci_ristampa = "1";
			wtMeta.cg_s = "RistampaCarta";
		}
		else{
			wtMeta.cg_s = "BaseWebCheckin";
			wtMeta.si_cs = "1";
		}
		
	}
	
	private void computeObjMeta_ancillaryPayment() {
		wtMeta.si_n = "wck_riepilogo_ancillari";
		wtMeta.si_x = "1";
		wtMeta.wck_v2_control = "wck_aem_gen_2016";
		wtMeta.wck_v2_riepilogoserviziapagamento = "1";
		wtMeta.pn_gr = "Alitalia";
		wtMeta.pn_fa = "Alitalia";
		wtMeta.pc = "Webcheckin";
		wtMeta.pn_sc = "Ancillary";
		wtMeta.pn = "CKINAncillary";
		wtMeta.pn_sku = "?";
		wtMeta.tx_u = "?";
		wtMeta.tx_e = "a";
	}
	
	private void computeObjMeta_ancillaryReceipt() {
		Function<BigDecimal,String> toString = price -> format.format(price);
		
		AncillaryPurchaseInfo ancillaryInfo = getAncillaryPurchaseInfo();
		wtMeta.si_n = "wck_riepilogo_ancillari";
		wtMeta.si_x = "3";
		wtMeta.wck_v2_control = "wck_aem_gen_2016";
		wtMeta.pn_gr = "Alitalia";
		wtMeta.pn_fa = "Alitalia";
		wtMeta.pc = "Webcheckin";
		wtMeta.pn_sc = "Ancillary";
		wtMeta.pn = "CKINAncillary";
		wtMeta.tx_u = ancillaryInfo.purchasedQuantity.stream()
				.map(qt -> String.valueOf(qt))
				.collect(Collectors.joining(";"));
		wtMeta.tx_s = ancillaryInfo.purchasedPrices.stream()
				.map(toString)
				.collect(Collectors.joining(";"));
		wtMeta.tx_e = "p";
		wtMeta.tx_i = ctx.eTicket;
		wtMeta.tx_id = dateFormat_en.format(now.getTime());
		wtMeta.tx_it = timeFormat.format(now.getTime());
		wtMeta.tx_curr = ctx.currencyCode;
		if(ancillaryInfo.isZeroPayment){
			wtMeta.wck_v2_riepilogoservizigratuiti = "1";
		}
		else{
			wtMeta.wck_v2_ricevutaserviziapagamento = "1";
			wtMeta.si_cs = "1";
			wtMeta.wck_v2_quantitaemd = String.valueOf(ancillaryInfo.numOfPurchasedEMD);
			wtMeta.arsummary = "Webcheckin";
			wtMeta.arproduct = ancillaryInfo.purchasedProducts.stream()
					.collect(Collectors.joining(";"));
			wtMeta.arquantity = wtMeta.tx_u;
		}
		
	}
	

	private void computeObjMeta_undo() {
		
	}
	
	
	private PassengerAnalyticsInfo getPassengerInfo(){
		PassengerAnalyticsInfo paxInfo = new PassengerAnalyticsInfo();
		paxInfo.isApiStep = ctx.apisPhase && ctx.apisBasic;
		paxInfo.isApiPlusStep = ctx.apisPhase && ctx.apisPlus;
		paxInfo.isApiPlusStep2 = ctx.apisPhase && ctx.apisPlus && ctx.additionalInfoPhase;
		return paxInfo;
	}
	
	private boolean isBoardingPassReprint(){
		/*Se un passeggero ha effettuato il checkin in una sessione precedente*/
		List<CheckinPassengerData> paxList = ctx.passengers;
		if(paxList == null){
			paxList = ctx.acceptedPassengers;
		}
		return paxList.stream().anyMatch(pax -> pax.getStatus().equals(CheckinPassengerStatusEnum.ALREADY_CHECKED_IN));
	}
	
	
	private AncillaryPurchaseInfo getAncillaryPurchaseInfo(){
		AncillaryPurchaseInfo ancInfo = new AncillaryPurchaseInfo();
		Predicate<MmbAncillaryData> issuedAncillary = anc -> anc.getAncillaryStatus().equals(MmbAncillaryStatusEnum.ISSUED);
		ancInfo.isZeroPayment = ctx.cartTotalAmount.equals(BigDecimal.ZERO);
		ancInfo.numOfPurchasedEMD = ctx.cart.getAncillaries().stream()
				.filter(issuedAncillary)
				.map(anc -> anc.getEmd())
				.distinct()
				.count();
		ancInfo.purchasedProducts = null;
		List<MmbAncillaryTypeEnum> ancTypes = ctx.cart.getAncillaries().stream()
				.filter(issuedAncillary)
				.map(anc -> anc.getAncillaryType())
				.distinct()
				.collect(Collectors.toList());
		ancInfo.purchasedProducts = ancTypes.stream().map(ancillaryToString()).collect(Collectors.toList());
		ancInfo.purchasedQuantity = new LinkedList<Long>();
		ancInfo.purchasedPrices = new LinkedList<BigDecimal>();
		for(MmbAncillaryTypeEnum ancType : ancTypes){
			ancInfo.purchasedQuantity.add(
					ctx.cart.getAncillaries().stream()
					.filter(issuedAncillary)
					.filter(anc -> anc.getAncillaryType().equals(ancType))
					.count());
			ancInfo.purchasedPrices.add(ctx.cart.getAncillaries().stream()
					.filter(issuedAncillary)
					.filter(anc -> anc.getAncillaryType().equals(ancType))
					.map(anc -> anc.getAmount())
					.reduce(BigDecimal.ZERO, BigDecimal::add)
					);
		}
		return ancInfo;
	}
	
	private Function<MmbAncillaryTypeEnum, String> ancillaryToString(){
		return ancType -> {
			String ancString = "";
			switch(ancType){
				case SEAT:
					ancString = "posto_confort";
					break;
				case EXTRA_BAGGAGE:
					ancString = "bagaglio_extra";
					break;
				case INSURANCE:
					ancString = "assicura_viaggio";
					break;
				case UPGRADE:
					ancString = "upgrade";
					break;
				default:
					break;
		}
			return ancString;
		};
	}
	
	
	class PassengerAnalyticsInfo{
		boolean isApiStep;
		boolean isApiPlusStep;
		boolean isApiPlusStep2;
	}
	
	class AncillaryPurchaseInfo{
		boolean isZeroPayment;
		long numOfPurchasedEMD;
		List<String> purchasedProducts;
		List<Long> purchasedQuantity;
		List<BigDecimal> purchasedPrices;
	}

	public int getStep() {
		return step;
	}


	public String getWebTrendsMeta() {
		return webTrendsMeta;
	}

	public String getDlVar() {
		return dlVar;
	}

}
