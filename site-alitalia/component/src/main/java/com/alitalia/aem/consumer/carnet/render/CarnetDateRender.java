package com.alitalia.aem.consumer.carnet.render;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.carnet.CarnetSessionContext;
import com.day.cq.i18n.I18n;

public class CarnetDateRender {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	Calendar date;
	String market;
	protected I18n i18n;
	
	public CarnetDateRender(Calendar date, CarnetSessionContext ctx) {
		this.date = date;
		this.market = ctx.market;
		this.i18n = ctx.i18n;
	}
	
	public String getDay() {
		String day = "";
		try {
			day = Integer.toString(date.get(Calendar.DATE));
			if(date.get(Calendar.DATE)<10){
				day = "0" + day;
			}
		} catch (Exception e) {
			logger.error("Errore format giorno", e);
		}
		return day;
	}
	
	public String getMonth() {
		String month = "";
		try {
			month = Integer.toString(date.get(Calendar.MONTH)+1);
			if((date.get(Calendar.MONTH)+1)<10)
				month = "0" + month;
		} catch (Exception e) {
			logger.error("Errore format mese", e);
		}
		return month;
	}
	
	public String getYear() {
		String year = "";
		try {
			year = Integer.toString(date.get(Calendar.YEAR));
		} catch (Exception e) {
			logger.error("Errore format anno", e);
		}
		return year;
	}

	public String getTwoDigitYear() {
		return getYear().substring(2);
	}
	
	public String getExtendedDate() {
		String extendedDate = "";
		try {
			extendedDate = getDay() + "/" + getMonth() + "/" + getYear();
			/* DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, locale);
			 if (df instanceof SimpleDateFormat){
	            SimpleDateFormat sdf = (SimpleDateFormat) df;
	            // To show Locale specific short date expression with full year
	            String pattern = sdf.toPattern().replaceAll("y+","yyyy");
	            sdf.applyPattern(pattern); 
	            return sdf.format(date.getTime());
	            }
			 extendedDate = df.format(date.getTime());*/
		} catch (Exception e) {
			logger.error("Errore format data", e);
		}
		return extendedDate;
	}
	
	public String getTextualDate() {
		String textualDate = "";
		try {
			textualDate = getDay() + " " + getShortTextMonth() + " " + getYear();
		} catch (Exception e) {
			logger.error("Errore format data", e);
		}
		return textualDate;
	}
	
	public String getHour() {
		String hour = "";
		try {
			SimpleDateFormat dateFormatter = null;
			
			switch(market.toLowerCase()){
				case "ca":
				case "us":{
					dateFormatter = new SimpleDateFormat("hh:mm");
					break;
				}
				default:{
					dateFormatter = new SimpleDateFormat("HH:mm");
				}					
			}
			hour = dateFormatter.format(date.getTime());
			
		} catch (Exception e) {
			logger.error("Errore format ora", e);
		}
		return hour;
	}
	
	/** Restituisce l'ora e i minuti nel formato orario a 24 ore*/
	public String getHour24() {
		String hour = "";
		try {
			hour = new SimpleDateFormat("HH:mm").format(date.getTime());		
		} catch (Exception e) {
			logger.error("Errore format ora", e);
		}
		return hour;
	}
	
	/** Restituisce il periodo AM o PM da aggiungere all'orario nel caso di mercato americano o canadese*/
	public String getPeriod(){
		String period = "";	
		if(market.equalsIgnoreCase("us") || market.equalsIgnoreCase("ca")){
			try {
				period = new SimpleDateFormat("aa").format(date.getTime());		
			} catch (Exception e) {
				logger.error("Errore format ora", e);
			}
		}
		return period;
	}
	
	/**
	 * 
	 * @return Ritorna la chiave dell'i18n per recuperare la stringa di tre lettere relativa al nome del mese
	 */
	public String getShortTextMonth(){
		String shortTextMonth = "";
		try{
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MMMM",Locale.ENGLISH);
			shortTextMonth = "common.monthsOfYear." + dateFormatter.format(date.getTime()).toLowerCase() + ".short";
		}catch(IllegalArgumentException IAE){
			logger.error("Errore format mese in getShortTextMonth", IAE);
		}	
		return i18n.get(shortTextMonth);
	}
	
	/**
	 * 
	 * @return Ritorna la chiave dell'i18n per recuperare il nome intero del mese
	 */
	public String getTextMonth(){
		String textMonth = "";
		try{
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MMMM",Locale.ENGLISH);
			textMonth = "common.monthsOfYear." + dateFormatter.format(date.getTime()).toLowerCase();
		}catch(IllegalArgumentException IAE){
			logger.error("Errore format mese in getTextMonth", IAE);
		}	
		return i18n.get(textMonth);
	}
	
}
