package com.alitalia.aem.consumer.model;

public class UserMA {

    public static final String NAME = "MACustomer";
    private String UID;
    private String UIDSignature;
    private boolean isGlobal;
    private boolean newUser;
    private UserMAProfile profile;
    private String loginMode;
    private UserMAData data;

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getUIDSignature() {
        return UIDSignature;
    }

    public void setUIDSignature(String UIDSignature) {
        this.UIDSignature = UIDSignature;
    }

    public boolean isGlobal() {
        return isGlobal;
    }

    public void setGlobal(boolean global) {
        isGlobal = global;
    }

    public boolean isNewUser() {
        return newUser;
    }

    public void setNewUser(boolean newUser) {
        this.newUser = newUser;
    }

    public UserMAProfile getProfile() {
        return profile;
    }

    public void setProfile(UserMAProfile profile) {
        this.profile = profile;
    }

    public UserMAData getData() {
        return data;
    }

    public void setData(UserMAData data) {
        this.data = data;
    }

    public String getLoginMode() {
        return loginMode;
    }

    public void setLoginMode(String loginMode) {
        this.loginMode = loginMode;
    }
}
