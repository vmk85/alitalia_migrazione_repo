package com.alitalia.aem.consumer.global.enumeration;

import java.util.ArrayList;

public enum CreditCardCircuits {
	DINERS_CLUB("DC"),
	AMERICAN_EXPRESS("AX"),
	MASTER_CARD("MC"),
	VISA("VS"),
	MAESTRO("MA");
	
	
	private final String value;
		
	CreditCardCircuits(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	public String getI18nValue() {
		return "creditCardCircuit." + value + ".label";
	}
	
	public static ArrayList<String> listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (CreditCardCircuits c: CreditCardCircuits.values()) {
			result.add(c.value);
		}
		return result;
	}

	public static CreditCardCircuits fromValue(String v) {
		for (CreditCardCircuits c: CreditCardCircuits.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
