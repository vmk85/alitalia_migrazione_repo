package com.alitalia.aem.consumer.validation;

import java.util.List;

public class Element {
	private List<String> methods;
	private String key;
	private String[] values;
	private String comparingValue;
	private String message;
	private Object[] allowedValues;

	public List<String> getMethods() {
		return methods;
	}

	public void setMethods(List<String> methods) {
		this.methods = methods;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String[] getValues() {
		return values;
	}

	public void setValues(String[] values) {
		this.values = values;
	}

	public String getComparingValue() {
		return comparingValue;
	}

	public void setComparingValue(String comparingValue) {
		this.comparingValue = comparingValue;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object[] getAllowedValues() {
		return allowedValues;
	}

	public void setAllowedValues(Object[] allowedValues) {
		this.allowedValues = allowedValues;
	}

}
