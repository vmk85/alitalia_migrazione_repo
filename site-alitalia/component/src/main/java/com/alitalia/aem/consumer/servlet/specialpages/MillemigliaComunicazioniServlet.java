package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.*;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBElement;

import com.alitalia.aem.common.data.home.*;
import com.alitalia.aem.common.data.home.enumerations.*;
import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.alitalia.aem.ws.booking.commonservice.xsd.ObjectFactory;
import com.alitalia.aem.ws.common.exception.WSClientException;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd.GetDatiSicurezzaProfilo;
import com.alitalia.aem.ws.otp.service.OTPServiceClient;
import com.alitalia.aem.ws.otp.service.xsd1.GetOTPRequest;
import com.alitalia.aem.ws.otp.service.xsd1.GetOTPResponse;
import com.alitalia.aem.ws.sendmail.SendMailServiceClient;
import com.alitalia.aem.ws.sendmail.xsd.MailPriority;
import com.alitalia.aem.ws.sendmail.xsd.MessaggioEmail;
import com.alitalia.aem.ws.sendsms.SendSmsServiceClient;
import com.alitalia.aem.ws.sendsms.xsd.SmsMsg;
import com.alitalia.aem.ws.sendsms.xsd.SmsMsgReturn;
import com.alitalia.aem.ws.sendsms.xsd.SmsParametersType;
import com.day.cq.commons.TidyJSONWriter;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.engine.SlingRequestProcessor;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.model.content.specialpages.MillemigliaRegistratiData;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.FrequentFlyer;
import com.alitalia.aem.consumer.utils.TrackingCodeListDS;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.WCMMode;

/// OTPMOCK
import com.alitalia.aem.ws.otp.service.xsd1.SendOTPRequest;
import com.alitalia.aem.ws.otp.service.xsd1.SendOTPResponse;

// security check - Begin
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
// security check - End

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;

@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = {
		        "millemigliacomunicazionisubmit",
                "millemigliacomunicazionisubmit-checkemailusernameduplicate",
                "millemigliacomunicazionisubmit-checkusernameduplicate",
                "millemigliacomunicazionisubmit-sendotpsms",
                "millemigliacomunicazionisubmit-sendotpemail",
                "millemigliacomunicazionisubmit-resetaccount",
				"millemigliacomunicazionisubmit-lockaccount",
				"millemigliacomunicazionisubmit-sendsms",
				"millemigliacomunicazionisubmit-associa-ma",
				"millemigliacomunicazionisubmit-get-login-ma-mm"

		}),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class MillemigliaComunicazioniServlet extends GenericFormValidatorServlet {

	private static final String CHECKBOX_CHECKED = "on";
	
	private static final String OBJ_MAIL = "object.registrazione.millemiglia.mail";

	private static final String GENDER_MALE = "m";
	private static final String GENDER_FEMALE = "f";

	private static final String WORKPOSITION_OTHER = "Altro";
	private static final String WORKPOSITION_EXECUTIVE = "Dirigente";
	private static final String WORKPOSITION_JOURNALIST = "Giornalista";
	private static final String WORKPOSITION_EMPLOYER = "Impiegato";
	private static final String WORKPOSITION_ENTREPENEUR = "Imprenditore";
	private static final String WORKPOSITION_FREELANCE = "Libero Professionista";
	private static final String WORKPOSITION_MANAGER = "Quadro";

	private static final String ADDRESSTYPE_HOME = "home";
	private static final String ADDRESSTYPE_OFFICE = "office";

	private static final String TELEPHONETYPE_BUSINESS = "BusinessPhone";
	private static final String TELEPHONETYPE_HOME = "HomePhone";
	private static final String MOBILETYPE_BUSINESS = "MobileBusiness";
	private static final String MOBILETYPE_HOME = "MobileHome";

	private static final String NEWSLETTER_ALITALIA = "A";
	private static final String NEWSLETTER_PARTNERS = "AP";
	private static final String NEWSLETTER_DISABLED = "NO";

	private static final String METHOD_EMAIL = "E";
	private static final String METHOD_POSTAL = "P";
	private static final String MAILINGTYPE_ALITALIA_POSTAL = "001";
	private static final String MAILINGTYPE_ALITALIA_EMAIL = "002";
	private static final String MAILINGTYPE_PARTNERS_POSTAL = "003";
	private static final String MAILINGTYPE_PARTNERS_EMAIL = "004";
	private static final String MAILINGTYPE_DISABLED = "005";

	private static final String THREAT_CONSENT = "y";
	private static final String THREAT_DENY = "n";

/*
	private static final String YOUNG_CONSENT = "true";
	private static final String YOUNG_DENY = "false";
*/

	private static final String PREFERENCE_SEAT = "003";
	private static final String PREFERENCE_MEAL = "002";

	private static final String CODE_FREQUENT_FLYER = "FREQUENT FLYER PROGRAMS";
	private static final String CODE_AMERICAN = "AA";
	private static final String CODE_FRANCE = "AK";
	private static final String CODE_BRITISH = "BA";
	private static final String CODE_DELTA = "DL";
	private static final String CODE_EMIRATES = "EK";

	private static final String CODE_IBERIA = "IB";
	private static final String CODE_MERIDIANA = "IG";
	private static final String CODE_LUFTHANSA = "LH";
	private static final String CODE_OTHER = "OTHER";
	private static final String CODE_TURKISH = "TK";
	private static final String CODE_EASYJET = "U2";
	private static final String MAX_FREQUENT_FLYERS = "5";

	private static final String LANGUAGE_ITA = "Italiano";
	private static final String LANGUAGE_FRA = "Francese";
	private static final String LANGUAGE_TED = "Tedesco";
	private static final String LANGUAGE_ING = "Inglese";
	private static final String LANGUAGE_GIA = "Giapponese";
	private static final String LANGUAGE_POR = "Portoghese";
	private static final String LANGUAGE_SPA = "Spagnolo";

	private static final String CODE_ITA = "IT";
	private static final String CODE_FRA = "FR";
	private static final String CODE_TED = "GE";
	private static final String CODE_ING = "EN";
	private static final String CODE_GIA = "JA";
	private static final String CODE_POR = "PO";
	private static final String CODE_SPA = "SP";

	private static final String CODE_NO_PREFERENCE = "QLSS";
	private TidyJSONWriter json ;

	private ComponentContext componentContext;
    
    @Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private ResourceResolverFactory resolverFactory;

	/* Service to create HTTP Servlet requests and responses */
	@Reference
	private RequestResponseFactory requestResponseFactory;

	/* Service to process requests through Sling */
	@Reference
	private SlingRequestProcessor requestProcessor;

	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}

	@Reference
	private OTPServiceClient wsClient;

    @Reference
    private SendMailServiceClient wsSendMailClient;

    @Reference
    private SendSmsServiceClient wsSendSmsClient;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		logger.debug("[MillemigliaComunicazioniServlet] validateForm");

		try {

			// validate parameters!
			Validator validator = setValidatorParameters(request,
					new Validator());
			return validator.validate();
		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Error during parameters validation", e);
			return null;
		}
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {

		 String selectorString = request.getRequestPathInfo().getSelectorString();

		if(selectorString==null){
            selectorString="millemigliacomunicazionisubmit-finale";
        }

		switch (selectorString) {
			case "millemigliacomunicazionisubmit":
			    
            case "millemigliacomunicazionisubmit-finale":
				salvaSA(request,response);
				break;
			case "millemigliacomunicazionisubmit-checkemailusernameduplicate":
				CheckEmailUserNameDuplicate(request,response);
				break;
			case "millemigliacomunicazionisubmit-checkusernameduplicate":
				CheckUserNameDuplicate(request,response);
				break;
			case "millemigliacomunicazionisubmit-sendotpsms":
				sendOTPsms(request,response);
				break;
			case "millemigliacomunicazionisubmit-sendotpemail":
				sendOTPEmail(request,response);
				break;
			case "millemigliacomunicazionisubmit-resetaccount":
				performResetAccount(request,response);
				break;
			case "millemigliacomunicazionisubmit-lockaccount":
				performLockAccount(request,response);
				break;
			case "millemigliacomunicazionisubmit-associa-ma":
				performAssociaMyAlitalia(request,response);
				break;
			case "millemigliacomunicazionisubmit-get-login-ma-mm":
				performGetLoginMAMM(request,response);
				break;
		}
	}

	/** Metodo per prendere il pin utente mm associato ad utente ma loggato */
	private void performGetLoginMAMM(SlingHttpServletRequest request, SlingHttpServletResponse response)  {

		/** Prendo il codice dalla request */
		String mm_code = request.getParameter("mm_pin");
		GetDatiSicurezzaProfiloRequestLocal datiSicurezzaProfiloRequest = new GetDatiSicurezzaProfiloRequestLocal();

		datiSicurezzaProfiloRequest.setIdProfilo(mm_code);
		/** richiamo il servizio per prendere username e password per la chiamata di login */
		GetDatiSicurezzaProfiloResponseLocal datiBeforeUpdateSicurezzaProfilo = consumerLoginDelegate.GetDatiSicurezzaProfilo(datiSicurezzaProfiloRequest);

		String tid = IDFactory.getTid();
		String sid = IDFactory.getSid(request);

		MilleMigliaLoginSARequest loginSARequest = new MilleMigliaLoginSARequest(tid, sid);
//loginSARequest.setCodiceMM("marcaco2");
		/** prendo username e password per fare la chiamata di login e salvarmi il pin
		 * ( questo mi servirà per effettuare il login dell'account mm associato all'account ma che si sta loggando lato fegigya ) */
		loginSARequest.setUsername(datiBeforeUpdateSicurezzaProfilo.getUsername());
		loginSARequest.setPassword(datiBeforeUpdateSicurezzaProfilo.getPassword());
		MilleMigliaLoginSAResponse loginSAResponse = consumerLoginDelegate.milleMigliaLoginSA(loginSARequest);

		boolean isError = true;

		String password = "";

		if(loginSAResponse != null){

			if (loginSAResponse.getCustomerSA() != null){

				if(loginSAResponse.getCustomerSA().getMmCustomerProfileData() != null){

					if(loginSAResponse.getCustomerSA().getMmCustomerProfileData().getCustomerPinCode() != null ){
						isError = false;
						password = loginSAResponse.getCustomerSA().getMmCustomerProfileData().getCustomerPinCode();
					}
				}

			}

		}

		response.setContentType("application/json");

		try {
            json = new TidyJSONWriter(response.getWriter());

            json.object();

			json.key("isError").value(isError);
			json.key("MM_code").value(mm_code);
			json.key("pw").value(password);
			json.endObject();
		} catch (Exception e) {
			logger.error("[MillemigliaComunicazioniServlet] retrive password per MyAlitalia non riuscita.", e);
		}


	}

	protected void salvaOLD(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {

		logger.debug("[MillemigliaComunicazioniServlet] performSubmit");

		final I18n i18n = new I18n(request.getResourceBundle(
				AlitaliaUtils.findResourceLocale(request.getResource())));
		// RECUPERA L'OGGETTO DALLA SESSIONE
		MillemigliaRegistratiData millemigliaRegistratiData =
				(MillemigliaRegistratiData) request.getSession()
						.getAttribute(MillemigliaRegistratiData.NAME);

		String name = millemigliaRegistratiData.getName();
		String surname = millemigliaRegistratiData.getSurname();
		String dayOfBirth = millemigliaRegistratiData.getDayOfBirth();
		String monthOfBirth = millemigliaRegistratiData.getMonthOfBirth();
		String yearOfBirth = millemigliaRegistratiData.getYearOfBirth();
		String gender = millemigliaRegistratiData.getGender();
		String profession = millemigliaRegistratiData.getProfession();
		String postalAddress = millemigliaRegistratiData.getPostalAddress();
		String address = millemigliaRegistratiData.getAddress();
		String agency = millemigliaRegistratiData.getCompany();
		String cap = millemigliaRegistratiData.getCap();
		String city = millemigliaRegistratiData.getCity();
		String nation = millemigliaRegistratiData.getNation();
		String province = millemigliaRegistratiData.getProvince();
		String email = millemigliaRegistratiData.getEmail();
		String prefix = millemigliaRegistratiData.getNationalPrefix();
		String number = millemigliaRegistratiData.getNumber();
		String numberType = millemigliaRegistratiData.getTelephoneType();
		String areaPrefix = millemigliaRegistratiData.getAreaPrefix();
		String newsletter = request.getParameter("newsletter");
		String method = request.getParameter("method");
		String sms = request.getParameter("sms");
		String language = request.getParameter("language");
		String consent = request.getParameter("trattamento");
		String seat = request.getParameter("posto");
		String meal = request.getParameter("pasto");
		String airFrance = request.getParameter("airFrance");
		String american = request.getParameter("americanAirlines");
		String british = request.getParameter("britishAirways");
		String delta = request.getParameter("delta");
		String easyjet = request.getParameter("easyjet");
		String emirates = request.getParameter("emirates");
		String iberia = request.getParameter("iberia");
		String lufthansa = request.getParameter("lufthansa");
		String meridiana = request.getParameter("meridiana");
		String turkish = request.getParameter("turkishAirlines");
		String other = request.getParameter("altra");
		String trackingCode=millemigliaRegistratiData.getTrackingCod();
		Integer userIdRequestCode;

		try{
			userIdRequestCode= Integer.parseInt(trackingCode);

		}catch (Exception e){
			userIdRequestCode=0;
		}
		String userIdRequest =TrackingCodeListDS.getInstance().getTrackingCodeForService(userIdRequestCode);


		// CODIFICO I DATI RICEVUTI IN MODO DA RENDERLI CONFORMI ALLA REQUEST
		MMCustomerProfileData profile = new MMCustomerProfileData();
		profile.setCustomerName(name);
		profile.setCustomerSurname(surname);
		profile.setUserIdRequest(userIdRequest);

		Calendar dateOfBirth = Calendar.getInstance();
		int day = Integer.parseInt(dayOfBirth);
		int month = Integer.parseInt(monthOfBirth);
		int year = Integer.parseInt(yearOfBirth);
		dateOfBirth.set(year, month - 1, day);
		profile.setBirthDate(dateOfBirth);

		if (gender.equals(GENDER_MALE)) {
			profile.setGender(MMGenderTypeEnum.MALE);
		}
		if (gender.equals(GENDER_FEMALE)) {
			profile.setGender(MMGenderTypeEnum.FEMALE);
		}

		// nell'enum ci sono tipi in piu' rispetto alla form
		if (profession.equals(WORKPOSITION_OTHER)) {
			profile.setCustomerWorkPosition(MMWorkPositionTypeEnum.ALTRO);
		}
		if (profession.equals(WORKPOSITION_EXECUTIVE)) {
			profile.setCustomerWorkPosition(MMWorkPositionTypeEnum.DIRIGENTE);
		}
		if (profession.equals(WORKPOSITION_JOURNALIST)) {
			profile.setCustomerWorkPosition(MMWorkPositionTypeEnum.GIORNALISTA);
		}
		if (profession.equals(WORKPOSITION_EMPLOYER)) {
			profile.setCustomerWorkPosition(MMWorkPositionTypeEnum.IMPIEGATO);
		}
		if (profession.equals(WORKPOSITION_ENTREPENEUR)) {
			profile.setCustomerWorkPosition(MMWorkPositionTypeEnum.IMPRENDITORE);
		}
		if (profession.equals(WORKPOSITION_FREELANCE)) {
			profile.setCustomerWorkPosition(
					MMWorkPositionTypeEnum.LIBERO_PROFESSIONISTA);
		}
		if (profession.equals(WORKPOSITION_MANAGER)) {
			profile.setCustomerWorkPosition(MMWorkPositionTypeEnum.QUADRO);
		}

		MMAddressData addressData = new MMAddressData();
		if (postalAddress.equals(ADDRESSTYPE_HOME)) {
			addressData.setAddressType(MMAddressTypeEnum.HOME);
		}
		if (postalAddress.equals(ADDRESSTYPE_OFFICE)) {
			addressData.setAddressType(MMAddressTypeEnum.BUSINESS);
		}
		addressData.setStreetFreeText(address);
		addressData.setPostalCode(cap);
		addressData.setMunicipalityName(city);
		if (!isEmpty(province)) {
			addressData.setStateCode(province);
		} else {
			addressData.setStateCode("");
		}
		addressData.setCountry(nation);
		addressData.setCompanyName(agency);
		List<MMAddressData> addressList = new ArrayList<MMAddressData>();
		addressList.add(addressData);
		profile.setAddresses(addressList);

		profile.setEmail(email);
		MMTelephoneData telephone = null;
		if ((prefix != null && !"".equals(prefix))
				&& (number != null && !"".equals(number))
				&& (areaPrefix != null && !"".equals(areaPrefix))) {

			telephone = new MMTelephoneData();
			telephone.setCountryNumber(prefix);
			telephone.setNumber(number);
			telephone.setPhoneZone(areaPrefix);

			// nell'enum ci sono piu' tipi rispetto alla form
			if (numberType.equals(TELEPHONETYPE_BUSINESS)) {
				telephone.setTelephoneType(MMPhoneTypeEnum.BUSINESS_PHONE);
			}
			if (numberType.equals(TELEPHONETYPE_HOME)) {
				telephone.setTelephoneType(MMPhoneTypeEnum.HOME_PHONE);
			}
			if (numberType.equals(MOBILETYPE_BUSINESS)) {
				telephone.setTelephoneType(MMPhoneTypeEnum.MOBILE_BUSINESS);
			}
			if (numberType.equals(MOBILETYPE_HOME)) {
				telephone.setTelephoneType(MMPhoneTypeEnum.MOBILE_HOME);
			}

		}

		if (telephone != null) {
			List<MMTelephoneData> telephoneList =
					new ArrayList<MMTelephoneData>();
			telephoneList.add(telephone);
			profile.setTelephones(telephoneList);
		}

		if (newsletter.equals(NEWSLETTER_ALITALIA)
				&& method.equals(METHOD_EMAIL)) {
			profile.setMailingType(MAILINGTYPE_ALITALIA_EMAIL);
		}
		if (newsletter.equals(NEWSLETTER_ALITALIA)
				&& method.equals(METHOD_POSTAL)) {
			profile.setMailingType(MAILINGTYPE_ALITALIA_POSTAL);
		}
		if (newsletter.equals(NEWSLETTER_PARTNERS)
				&& method.equals(METHOD_EMAIL)) {
			profile.setMailingType(MAILINGTYPE_PARTNERS_EMAIL);
		}
		if (newsletter.equals(NEWSLETTER_PARTNERS)
				&& method.equals(METHOD_POSTAL)) {
			profile.setMailingType(MAILINGTYPE_PARTNERS_POSTAL);
		}

		if (newsletter.equals(NEWSLETTER_DISABLED)) {
			profile.setMailingType(MAILINGTYPE_DISABLED);
		}

		if (!isEmpty(sms)) {
			profile.setSmsAuthorization(true);
		} else {
			profile.setSmsAuthorization(false);
		}

		profile.setLanguage(getLanguageCode(language));

		if (consent.equals(THREAT_CONSENT)) {
			profile.setProfiling(true);
		}
		if (consent.equals(THREAT_DENY)) {
			profile.setProfiling(false);
		}

		if (!seat.equals(CODE_NO_PREFERENCE)
				|| !meal.equals(CODE_NO_PREFERENCE)) {
			List<MMPreferenceData> preferences =
					new ArrayList<MMPreferenceData>();

			if (!seat.equals(CODE_NO_PREFERENCE)) {
				MMPreferenceData seatPreference = new MMPreferenceData();
				seatPreference.setIdCategory(PREFERENCE_SEAT);
				seatPreference.setShortNamePreference(seat);
				preferences.add(seatPreference);
			}

			if (!meal.equals(CODE_NO_PREFERENCE)) {
				MMPreferenceData mealPreference = new MMPreferenceData();
				mealPreference.setIdCategory(PREFERENCE_MEAL);
				mealPreference.setShortNamePreference(meal);

				preferences.add(mealPreference);
			}
			profile.setPreferences(preferences);
		}

		List<MMLifestyleData> lifestyles = new ArrayList<MMLifestyleData>();
		if (!isEmpty(airFrance)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_FRANCE);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(american)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_AMERICAN);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(british)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_BRITISH);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(delta)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_DELTA);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(emirates)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_EMIRATES);
			lifestyles.add(lifestyle);
		}

		if (!isEmpty(easyjet)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_EASYJET);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(iberia)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_IBERIA);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(lufthansa)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_LUFTHANSA);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(meridiana)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_MERIDIANA);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(turkish)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_TURKISH);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(other)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_OTHER);
			lifestyles.add(lifestyle);
		}
		profile.setLifestyles(lifestyles);

//		String corpoMail = getMailBody(request.getResource(),
//				configuration.getTemplateMailWelcome(), false);

        String corpoMail = getMailBody(request.getResource(),
                configuration.getTemplateMailWelcome(), false);

		SubscribeRequest subscribeRequest = new SubscribeRequest();
		if (corpoMail != null) {
			String objMail = i18n.get(OBJ_MAIL);
			subscribeRequest.setTid(IDFactory.getTid());
			subscribeRequest.setSid(IDFactory.getSid(request));
			subscribeRequest.setCustomerProfile(profile);
			subscribeRequest.setBodyWelcomeMail(corpoMail);
			subscribeRequest.setObjectMail(objMail);
			logger.debug("Subscribe request subscribeRequest: "
					+ subscribeRequest.toString());
		} else {
			throw new IOException(I18nKeySpecialPage.ERROR_SERVICE_MILLMIGLIA_SUBSCRIBE);
		}

		try {
			//primo salvataggio
			SubscribeResponse subscribeResponse =
					consumerLoginDelegate.subscribeByCustomer(subscribeRequest);


			if (subscribeResponse != null) {
				logger.debug("subscribe response: "
						+ subscribeResponse.toString());
			} else {
				logger.error("Subscribe response NULL");
				throw new IOException("Subscribe response NULL");
			}

			if(subscribeResponse.getReturnCode() != null && subscribeResponse.getReturnCode().intValue() < 0){
				logger.error("Subscribe error. Message from service is: {}", subscribeResponse.getErrorMessage());
				throw new IOException("Subscribe error. Message from service is: " + subscribeResponse.getErrorMessage());
			}

			profile = subscribeResponse.getCustomerProfile();

			if (profile != null) {
				logger.debug("Subscribe response PROFILE: " + profile.toString());
				millemigliaRegistratiData.setMmcode(profile.getCustomerNumber());
				millemigliaRegistratiData.setState(3);
			} else {
				logger.error("Subscribe response PROFILE: NULL");
			}

		} catch (Exception e) {
			throw new IOException(I18nKeySpecialPage.ERROR_SERVICE_MILLMIGLIA_SUBSCRIBE);
		}

		request.getSession().setAttribute(MillemigliaRegistratiData.NAME,
				millemigliaRegistratiData);
		String selector = (String) componentContext.getProperties()
				.get("sling.servlet.selectors");
		String urlServlet = request.getRequestURL().toString();
		String url = urlServlet.substring(0,
				urlServlet.length() - selector.length());

		ResourceResolver resolver = request.getResourceResolver();
		url = resolver.map(url) + "html";
		logger.debug(resolver.map(url));

		response.sendRedirect(url);

	}

	protected void salvaSA(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {

		logger.debug("[MillemigliaComunicazioniServlet] performSubmit SA");

		final I18n i18n = new I18n(request.getResourceBundle(
				AlitaliaUtils.findResourceLocale(request.getResource())));
		// RECUPERA L'OGGETTO DALLA SESSIONE
		MillemigliaRegistratiData millemigliaRegistratiData =
				(MillemigliaRegistratiData) request.getSession()
						.getAttribute(MillemigliaRegistratiData.NAME);

		String name = millemigliaRegistratiData.getName();
		String surname = millemigliaRegistratiData.getSurname();
		String dayOfBirth = millemigliaRegistratiData.getDayOfBirth();
		String monthOfBirth = millemigliaRegistratiData.getMonthOfBirth();
		String yearOfBirth = millemigliaRegistratiData.getYearOfBirth();
		String gender = millemigliaRegistratiData.getGender();
		String profession = millemigliaRegistratiData.getProfession();
		String postalAddress = millemigliaRegistratiData.getPostalAddress();
		String address = millemigliaRegistratiData.getAddress();
		String agency = millemigliaRegistratiData.getCompany();
		String cap = millemigliaRegistratiData.getCap();
		String city = millemigliaRegistratiData.getCity();
		String nation = millemigliaRegistratiData.getNation();
		String province = millemigliaRegistratiData.getProvince();
		String email = millemigliaRegistratiData.getEmail();
		String prefix = millemigliaRegistratiData.getNationalPrefix();
		String number = millemigliaRegistratiData.getNumber();
		String numberType = millemigliaRegistratiData.getTelephoneType();
		String areaPrefix = millemigliaRegistratiData.getAreaPrefix();
		/*Boolean check_young = false;
		if (check_young!=null) {
			check_young = (request.getParameter("check_young").equals("on") ? true : false);
			}*/
		/*
		String check_young = request.getParameter("check_young");
		*/
		String newsletter = request.getParameter("newsletter");
		String method = request.getParameter("method");
		String sms = request.getParameter("sms");
		String language = request.getParameter("language");
		String consent = request.getParameter("trattamento");
		String seat = request.getParameter("posto");
		String meal = request.getParameter("pasto");
		String airFrance = request.getParameter("airFrance");
		String american = request.getParameter("americanAirlines");
		String british = request.getParameter("britishAirways");
		String delta = request.getParameter("delta");
		String easyjet = request.getParameter("easyjet");
		String emirates = request.getParameter("emirates");
		String iberia = request.getParameter("iberia");
		String lufthansa = request.getParameter("lufthansa");
		String meridiana = request.getParameter("meridiana");
		String turkish = request.getParameter("turkishAirlines");
		String other = request.getParameter("altra");
		String trackingCode=millemigliaRegistratiData.getTrackingCod();
		Integer userIdRequestCode;

		boolean EmailCertificateFlag = millemigliaRegistratiData.isFlagVerificaEmailUser_OK();
		boolean FlagOldCustomer = false;
		boolean FlagRememberPassword = false;
		boolean FlagVerificaPassword = millemigliaRegistratiData.isFlagVerificaPassword_OK();
		boolean FlagVerificaRisposta = millemigliaRegistratiData.isFlagVerificaRisposta_OK();
		boolean FlagVerificaUsername = millemigliaRegistratiData.isFlagVerificaUserName_OK();
		boolean Lockedout = false;
		boolean PhoneCertificateFlag = millemigliaRegistratiData.isFlagVerificaCellulare_OK();
		boolean Profiling = true;
		Integer AccessFailureCount = 0;
		
		Integer InputSecretQuestion=(millemigliaRegistratiData.getInputSecretQuestion()!=null && !millemigliaRegistratiData.getInputSecretQuestion().equals("") ?
					Integer.parseInt(millemigliaRegistratiData.getInputSecretQuestion()) : 0);
		//Integer InputSecretQuestion = Integer.parseInt(millemigliaRegistratiData.getInputSecretQuestion());
		
		
		String phonePrefix = millemigliaRegistratiData.getPhonePrefix();
		String certifiPhoneNumber = millemigliaRegistratiData.getCertifiedPhoneNumber();
		String rispostaSegreta =  DigestUtils.md5Hex(millemigliaRegistratiData.getRispostaSegreta()).toUpperCase();

		//TODO strong auth: recuperare nuovi valori con request.getParameter
		//String username= request.getParameter("username");
		//String password= request.getParameter("password");

		String username=millemigliaRegistratiData.getUsername();
		String password= millemigliaRegistratiData.getPassword();

		// --- in ogni caso in inserimento se non c'e email ci copio l'email certificata
		if(email==null)
			email=millemigliaRegistratiData.getCertifiedEmailAccount();
		else if (email.isEmpty())
			email=millemigliaRegistratiData.getCertifiedEmailAccount();
		// -----------------------------------------------------------------------------

		//della password viene fatto un hash MD5 convertito in maiuscolo per passarla al webservice
		String md5password = DigestUtils.md5Hex(password).toUpperCase();

		try{
			userIdRequestCode= Integer.parseInt(trackingCode);

		}catch (Exception e){
			userIdRequestCode=0;
		}
		String userIdRequest =TrackingCodeListDS.getInstance().getTrackingCodeForService(userIdRequestCode);

		// CODIFICO I DATI RICEVUTI IN MODO DA RENDERLI CONFORMI ALLA REQUEST
		MMCustomerProfileData profile = new MMCustomerProfileData();
		profile.setCustomerName(name);
		profile.setCustomerSurname(surname);
		profile.setUserIdRequest(userIdRequest);

		//todo: manca la mappatura del FLAG CHECK_YOUNG (check_young)
		
		profile.setEmailCertificateFlag(EmailCertificateFlag);
		profile.setPhoneCertificateFlag(PhoneCertificateFlag);
		profile.setFlagLockedAccount(Lockedout);
		profile.setFlagOldCustomer(FlagOldCustomer);
		profile.setFlagRememberPassword(FlagRememberPassword);
		profile.setFlagVerificaPassword(FlagVerificaPassword);
		profile.setFlagVerificaRisposta(FlagVerificaRisposta);
		profile.setFlagVerificaUserName(FlagVerificaUsername);
		profile.setProfiling(Profiling);
		profile.setAccessFailureCount(AccessFailureCount);

		Calendar dateOfBirth = Calendar.getInstance();
		int day = Integer.parseInt(dayOfBirth);
		int month = Integer.parseInt(monthOfBirth);
		int year = Integer.parseInt(yearOfBirth);
		dateOfBirth.set(year, month - 1, day);
		profile.setBirthDate(dateOfBirth);

		if (gender.equals(GENDER_MALE)) {
			profile.setGender(MMGenderTypeEnum.MALE);
		}
		if (gender.equals(GENDER_FEMALE)) {
			profile.setGender(MMGenderTypeEnum.FEMALE);
		}

		// nell'enum ci sono tipi in piu' rispetto alla form
		if (profession.equals(WORKPOSITION_OTHER)) {
			profile.setCustomerWorkPosition(MMWorkPositionTypeEnum.ALTRO);
		}
		if (profession.equals(WORKPOSITION_EXECUTIVE)) {
			profile.setCustomerWorkPosition(MMWorkPositionTypeEnum.DIRIGENTE);
		}
		if (profession.equals(WORKPOSITION_JOURNALIST)) {
			profile.setCustomerWorkPosition(MMWorkPositionTypeEnum.GIORNALISTA);
		}
		if (profession.equals(WORKPOSITION_EMPLOYER)) {
			profile.setCustomerWorkPosition(MMWorkPositionTypeEnum.IMPIEGATO);
		}
		if (profession.equals(WORKPOSITION_ENTREPENEUR)) {
			profile.setCustomerWorkPosition(MMWorkPositionTypeEnum.IMPRENDITORE);
		}
		if (profession.equals(WORKPOSITION_FREELANCE)) {
			profile.setCustomerWorkPosition(
					MMWorkPositionTypeEnum.LIBERO_PROFESSIONISTA);
		}
		if (profession.equals(WORKPOSITION_MANAGER)) {
			profile.setCustomerWorkPosition(MMWorkPositionTypeEnum.QUADRO);
		}

		MMAddressData addressData = new MMAddressData();
		if (postalAddress.equals(ADDRESSTYPE_HOME)) {
			addressData.setAddressType(MMAddressTypeEnum.HOME);
			profile.setDefaultAddressType(MMAddressTypeEnum.HOME);
		}
		if (postalAddress.equals(ADDRESSTYPE_OFFICE)) {
			addressData.setAddressType(MMAddressTypeEnum.BUSINESS);
			profile.setDefaultAddressType(MMAddressTypeEnum.BUSINESS);
		}
		addressData.setStreetFreeText(address);
		addressData.setPostalCode(cap);
		addressData.setMunicipalityName(city);
		if (!isEmpty(province)) {
			addressData.setStateCode(province);
			profile.setDefaultAddressStateCode(province);
		} else {
			addressData.setStateCode("");
			profile.setDefaultAddressStateCode("");
		}
		addressData.setCountry(nation);
		addressData.setCompanyName(agency);
		List<MMAddressData> addressList = new ArrayList<MMAddressData>();
		addressList.add(addressData);
		profile.setAddresses(addressList);
		
		if(number!=null && !number.equals("") && prefix!=null && !prefix.equals("") && numberType!=null && !numberType.equals("")){
			
			MMTelephoneData telephoneData = new MMTelephoneData();
			
			telephoneData.setTelephoneType(MMPhoneTypeEnum.fromValue(numberType));
			telephoneData.setCountryNumber(prefix);
			telephoneData.setNumber(number);
			telephoneData.setPhoneZone("00");
			telephoneData.setEffectiveDate(Calendar.getInstance());
			
			List<MMTelephoneData> telephonesList = new ArrayList<MMTelephoneData>();
			telephonesList.add(telephoneData);
			profile.setTelephones(telephonesList);
			
		}
		
		
		profile.setDefaultAddressCountry(nation);
		profile.setDefaultAddressMunicipalityName(city);
		profile.setDefaultAddressPostalCode(cap);

		profile.setDefaultAddressStreetFreeText(address);
		profile.setIdDomandaSegreta(InputSecretQuestion != null ? InputSecretQuestion : 0);
		profile.setRispostaSegreta(rispostaSegreta);

		if(!millemigliaRegistratiData.getRispostaSegreta().isEmpty()){
			profile.setFlagVerificaRisposta(true);
		}

		profile.setEmail(email);
		profile.setCertifiedEmailAccount(millemigliaRegistratiData.getCertifiedEmailAccount());
		profile.setCertifiedCountryNumber(phonePrefix);
		profile.setCertifiedPhoneAccount(certifiPhoneNumber);

		if(!millemigliaRegistratiData.getCertifiedEmailAccount().isEmpty()){
			profile.setEmailCertificateFlag(true);
		}

		if(!certifiPhoneNumber.isEmpty()){
			profile.setPhoneCertificateFlag(true);
		}
//		profile.getContractAgree()

		//MMTelephoneData telephone = null;

        /*  SA - area prefix non e' piu obbligatorio e quindi non va verificato */
        //TODO: togliere se decidono mettere nella maschera di registrazione area prefix
        /*
        if(areaPrefix != null && !"".equals(areaPrefix))
        {
            areaPrefix="06";
        }

        if ((prefix != null && !"".equals(prefix))
                && (number != null && !"".equals(number))
                && (areaPrefix != null && !"".equals(areaPrefix))) {

			telephone = new MMTelephoneData();
			telephone.setCountryNumber(prefix);
			telephone.setNumber(number);
			telephone.setPhoneZone(areaPrefix);

			// nell'enum ci sono piu' tipi rispetto alla form
			if (numberType.equals(TELEPHONETYPE_BUSINESS)) {
				telephone.setTelephoneType(MMPhoneTypeEnum.BUSINESS_PHONE);
			}
			if (numberType.equals(TELEPHONETYPE_HOME)) {
				telephone.setTelephoneType(MMPhoneTypeEnum.HOME_PHONE);
			}
			if (numberType.equals(MOBILETYPE_BUSINESS)) {
				telephone.setTelephoneType(MMPhoneTypeEnum.MOBILE_BUSINESS);
			}
			if (numberType.equals(MOBILETYPE_HOME)) {
				telephone.setTelephoneType(MMPhoneTypeEnum.MOBILE_HOME);
			}

		}

		if (telephone != null) {
			List<MMTelephoneData> telephoneList =
					new ArrayList<MMTelephoneData>();
			telephoneList.add(telephone);
			profile.setTelephones(telephoneList);
		}
		*/

		if (newsletter.equals(NEWSLETTER_ALITALIA)
				&& method.equals(METHOD_EMAIL)) {
			profile.setMailingType(MAILINGTYPE_ALITALIA_EMAIL);
		}
		if (newsletter.equals(NEWSLETTER_ALITALIA)
				&& method.equals(METHOD_POSTAL)) {
			profile.setMailingType(MAILINGTYPE_ALITALIA_POSTAL);
		}
		if (newsletter.equals(NEWSLETTER_PARTNERS)
				&& method.equals(METHOD_EMAIL)) {
			profile.setMailingType(MAILINGTYPE_PARTNERS_EMAIL);
		}
		if (newsletter.equals(NEWSLETTER_PARTNERS)
				&& method.equals(METHOD_POSTAL)) {
			profile.setMailingType(MAILINGTYPE_PARTNERS_POSTAL);
		}

		if (newsletter.equals(NEWSLETTER_DISABLED)) {
			profile.setMailingType(MAILINGTYPE_DISABLED);
		}

		if (!isEmpty(sms)) {
			profile.setSmsAuthorization(true);
		} else {
			profile.setSmsAuthorization(false);
		}

		profile.setLanguage(getLanguageCode(language));

		if (consent.equals(THREAT_CONSENT)) {
			profile.setProfiling(true);
		}
		if (consent.equals(THREAT_DENY)) {
			profile.setProfiling(false);
		}
/*
		if (check_young.equals(YOUNG_CONSENT)) {
			profile.setProfiling(true);
		}
		if (check_young.equals(YOUNG_DENY)) {
			profile.setProfiling(false);
		}*/

		if (!seat.equals(CODE_NO_PREFERENCE)
				|| !meal.equals(CODE_NO_PREFERENCE)) {
			List<MMPreferenceData> preferences =
					new ArrayList<MMPreferenceData>();

			if (!seat.equals(CODE_NO_PREFERENCE)) {
				MMPreferenceData seatPreference = new MMPreferenceData();
				seatPreference.setIdCategory(PREFERENCE_SEAT);
				seatPreference.setShortNamePreference(seat);
				preferences.add(seatPreference);
			}

			if (!meal.equals(CODE_NO_PREFERENCE)) {
				MMPreferenceData mealPreference = new MMPreferenceData();
				mealPreference.setIdCategory(PREFERENCE_MEAL);
				mealPreference.setShortNamePreference(meal);

				preferences.add(mealPreference);
			}
			profile.setPreferences(preferences);
		}

		List<MMLifestyleData> lifestyles = new ArrayList<MMLifestyleData>();
		if (!isEmpty(airFrance)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_FRANCE);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(american)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_AMERICAN);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(british)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_BRITISH);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(delta)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_DELTA);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(emirates)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_EMIRATES);
			lifestyles.add(lifestyle);
		}

		if (!isEmpty(easyjet)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_EASYJET);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(iberia)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_IBERIA);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(lufthansa)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_LUFTHANSA);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(meridiana)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_MERIDIANA);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(turkish)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_TURKISH);
			lifestyles.add(lifestyle);
		}
		if (!isEmpty(other)) {
			MMLifestyleData lifestyle = new MMLifestyleData();
			lifestyle.setCategory(CODE_FREQUENT_FLYER);
			lifestyle.setLifestyle(CODE_OTHER);
			lifestyles.add(lifestyle);
		}
		profile.setLifestyles(lifestyles);

		String corpoMail = getMailBody(request.getResource(),
				configuration.getTemplateMailWelcome(), false);

		//imposto userneme e password in hash MD5 maiuscolo
		profile.setUserName(username);
		profile.setPassword(md5password);

		SubscribeSARequest subscribeRequest = new SubscribeSARequest();
		if (corpoMail != null) {
			String objMail = i18n.get(OBJ_MAIL);
			subscribeRequest.setTid(IDFactory.getTid());
			subscribeRequest.setSid(IDFactory.getSid(request));
			subscribeRequest.setCustomerProfile(profile);
			subscribeRequest.setBodyWelcomeMail(corpoMail);
			subscribeRequest.setObjectMail(objMail);
			logger.debug("Subscribe SA request subscribeSARequest: "
					+ subscribeRequest.toString());
		} else {
			throw new IOException(I18nKeySpecialPage.ERROR_SERVICE_MILLMIGLIA_SUBSCRIBE);
		}

		try {
			//primo salvataggio SA
			SubscribeSAResponse subscribeResponse =
					consumerLoginDelegate.subscribeSA(subscribeRequest);


			if (subscribeResponse != null) {
				logger.debug("subscribe SA response: "
						+ subscribeResponse.toString());
			} else {
				logger.error("Subscribe SA response NULL");
				throw new IOException("Subscribe SA response NULL");
			}

			if(subscribeResponse.getReturnCode() != null && subscribeResponse.getReturnCode().intValue() < 0){
				logger.error("Subscribe SA error. Message from service is: {}", subscribeResponse.getErrorMessage());
				throw new IOException("Subscribe SA error. Message from service is: " + subscribeResponse.getErrorMessage());
			}

			String profileID = subscribeResponse.getProfileID();

			if (profileID != null) {
				logger.debug("Subscribe response PROFILEID: " + profileID.toString());

				millemigliaRegistratiData.setMmcode(profileID);
				//todo check
				millemigliaRegistratiData.setCertifiedEmailAccount(profile.getEmail());

				millemigliaRegistratiData.setState(3);
			} else {
				logger.error("Subscribe response PROFILE: NULL");
			}

		} catch (Exception e) {
			throw new IOException(I18nKeySpecialPage.ERROR_SERVICE_MILLMIGLIA_SUBSCRIBE);
		}

		request.getSession().setAttribute(MillemigliaRegistratiData.NAME,
				millemigliaRegistratiData);
		String[] c = (String[]) componentContext.getProperties()
				.get("sling.servlet.selectors");
		String selector = c[0];
		String urlServlet = request.getRequestURL().toString();
		String url = urlServlet.substring(0,
				urlServlet.length() - selector.length());

		ResourceResolver resolver = request.getResourceResolver();
		url = resolver.map(url) + "html";
		logger.debug(resolver.map(url));

		response.sendRedirect(url);

	}

	protected void CheckEmailUserNameDuplicate(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		
	    logger.debug("[MillemigliaComunicazioniServlet] CheckEmailUserNameDuplicate ");

		final I18n i18n = new I18n(request.getResourceBundle(
				AlitaliaUtils.findResourceLocale(request.getResource())));
		// RECUPERA L'OGGETTO DALLA SESSIONE

		String notFromRegistrazione= request.getParameter("notFromRegistrazione");

        MillemigliaRegistratiData millemigliaRegistratiData =
                getData(request, 2);

//		MillemigliaRegistratiData millemigliaRegistratiData =
//				(MillemigliaRegistratiData) request.getSession()
//						.getAttribute(MillemigliaRegistratiData.NAME);

		try {

			SubscribeSACheckEmailUserNameDuplicateRequest req = new SubscribeSACheckEmailUserNameDuplicateRequest();

			if(notFromRegistrazione != null && !notFromRegistrazione.isEmpty()){
				String input_email= request.getParameter("input_email");
				String profileId= request.getParameter("profileId");

				req.setProfileID(profileId);
				req.setEmailUserName(input_email);
			}else{
				String trackingCode=millemigliaRegistratiData.getTrackingCod();
				Integer userIdRequestCode;

				try{
					userIdRequestCode= Integer.parseInt(trackingCode);

				}catch (Exception e){
					userIdRequestCode=0;
				}
				String userIdRequest =TrackingCodeListDS.getInstance().getTrackingCodeForService(userIdRequestCode);

				req.setProfileID(userIdRequestCode.toString());
				req.setEmailUserName(millemigliaRegistratiData.getCertifiedEmailAccount());
			}

				SubscribeSACheckEmailUserNameDuplicateResponse checkEmailUserNameDuplicateResponse =
						consumerLoginDelegate.CheckEmailUserNameDuplicate(req);

			TidyJSONWriter json;
			try {
				json = new TidyJSONWriter(response.getWriter());

				response.setContentType("application/json");

				json.object();
				json.key("flagVerificaEmailUserNameNotDuplicate").value(checkEmailUserNameDuplicateResponse.isFlagNoEmailUserNameDuplicate());
				json.key("returncode").value(checkEmailUserNameDuplicateResponse.getReturnCode());
				json.key("errormessage").value(checkEmailUserNameDuplicateResponse.getErrorMessage());
				json.endObject();

			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			throw new IOException(I18nKeySpecialPage.ERROR_SERVICE_MILLMIGLIA_SUBSCRIBE);
		}
	}

	protected void CheckUserNameDuplicate(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
	 
		logger.debug("[MillemigliaComunicazioniServlet] CheckUserNameDuplicate ");

		final I18n i18n = new I18n(request.getResourceBundle(
				AlitaliaUtils.findResourceLocale(request.getResource())));
		// RECUPERA L'OGGETTO DALLA SESSIONE
		//MillemigliaRegistratiData millemigliaRegistratiData =
		//		(MillemigliaRegistratiData) request.getSession()
		//				.getAttribute(MillemigliaRegistratiData.NAME);

		// variabili di input
		String username= request.getParameter("username");

		String profileId= request.getParameter("profileId");


		try {
			SubscribeSACheckUserNameDuplicateRequest req = new SubscribeSACheckUserNameDuplicateRequest();
			req.setUsername(username);
			req.setProfileID(profileId);
			SubscribeSACheckUserNameDuplicateResponse checkUserNameDuplicateResponse =
					consumerLoginDelegate.CheckUserNameDuplicate(req);

			TidyJSONWriter json;
			try {
				json = new TidyJSONWriter(response.getWriter());

				response.setContentType("application/json");

				json.object();
				json.key("flagVerificaUserNameNotDuplicate").value(checkUserNameDuplicateResponse.isFlagNoUserNameDuplicate());
				json.key("returncode").value(checkUserNameDuplicateResponse.getReturnCode());
				json.key("errormessage").value(checkUserNameDuplicateResponse.getErrorMessage());
				json.endObject();

			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			throw new IOException(I18nKeySpecialPage.ERROR_SERVICE_MILLMIGLIA_SUBSCRIBE);
		}
	}

	private String getOtpFromService(SlingHttpServletRequest request, SlingHttpServletResponse response, String emailAddress) throws IOException {
		logger.debug("[MillemigliaComunicazioniServlet] getOtpFromService ");

		String ret="";

		try {
			ret=getOTP(request,response,emailAddress);
		}
		catch (Exception e){
			logger.error(e.getMessage());

			//fallback >>> lo genero io e lo restituisco
			ret=getRandomOtp();
		}

		return ret;
	}

	private String getRandomOtp(){
		Random rand = new Random();

		Integer  n = rand.nextInt(999999) + 1;
		//999999 is the maximum and the 1 is our minimum
		String paddedString = StringUtils.leftPad(n.toString(), 6, "0");

		return paddedString;
	}

	private String getOTPTestMessageForEmail(String OTPTextMessageFormat, String otpNumber){
		if(OTPTextMessageFormat.contains("{0}")){
			return OTPTextMessageFormat.replace("{0}",otpNumber);
		}else{
			return OTPTextMessageFormat + otpNumber;
		}
	}

	protected void sendOTPEmail(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		logger.debug("[MillemigliaComunicazioniServlet] sendOTPEmail ");

		// variabili di input
		String email = request.getParameter("email");

		//chiamare ws getOTP
		String otpNumber=getOtpFromService(request,response,email);

		//mock
		Calendar sentDate = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
		Calendar ExpiryDate = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
		ExpiryDate.add(Calendar.SECOND, 30);
		//System.out.println(now.getTime());

		final I18n i18n = new I18n(request.getResourceBundle(
				AlitaliaUtils.findResourceLocale(request.getResource())));
		// RECUPERA L'OGGETTO DALLA SESSIONE
		//MillemigliaRegistratiData millemigliaRegistratiData =
		//		(MillemigliaRegistratiData) request.getSession()
		//				.getAttribute(MillemigliaRegistratiData.NAME);

		try {

			SendMailServiceClient sendMailClient = wsSendMailClient;
			MessaggioEmail msg= new MessaggioEmail() ;

			msg.setTo(email);

			msg.setFrom(i18n.get(I18nKeySpecialPage.OTP_MAIL_FROM));
			msg.setSubject(i18n.get(I18nKeySpecialPage.OTP_MAIL_SUBJECT));
			String textMessage = i18n.get(I18nKeySpecialPage.OTP_MAIL_MESSAGETEXT);

			textMessage= getOTPTestMessageForEmail(textMessage,otpNumber);

			msg.setMessageText(textMessage);
			msg.setIsBodyHtml(false);
			
			msg.setPriority(MailPriority.NORMAL);
            List<String> deliveryNotificationOptionsList=msg.getDeliveryNotification();
            deliveryNotificationOptionsList.add(DeliveryNotificationOptionsEnum.NONE.value());
            
			Boolean sendMailClientResponse = sendMailClient.sendMail(msg);
   
			TidyJSONWriter json;
			try {
				json = new TidyJSONWriter(response.getWriter());

				response.setContentType("application/json");

				json.object();
				json.key("OTP").value(otpNumber);
				json.key("SentDate").value(sentDate.toString());
				json.key("ExpiryDate").value(ExpiryDate.toString());
				json.endObject();

			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			throw new IOException(I18nKeySpecialPage.ERROR_SERVICE_MILLMIGLIA_SUBSCRIBE);
		}
	}

	protected void sendOTPsms_OLD(SlingHttpServletRequest request,
										  SlingHttpServletResponse response) throws IOException
	{
		logger.debug("[MillemigliaComunicazioniServlet] sendOTPsms ");

		final I18n i18n = new I18n(request.getResourceBundle(
				AlitaliaUtils.findResourceLocale(request.getResource())));

		try {

			//SendOTP
			TidyJSONWriter json;
			try {
				json = new TidyJSONWriter(response.getWriter());

				response.setContentType("application/json");

				Calendar sentDate = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
				Calendar ExpiryDate = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
				ExpiryDate.add(Calendar.SECOND, 30);
				//System.out.println(now.getTime());

				json.object();
				json.key("OTP").value("123456");
				json.key("SentDate").value(sentDate.toString());
				json.key("ExpiryDate").value(ExpiryDate.toString());
				json.endObject();

			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			throw new IOException(I18nKeySpecialPage.ERROR_SERVICE_MILLMIGLIA_SUBSCRIBE);
		}
	}

	private String getOTP(SlingHttpServletRequest request,
						  SlingHttpServletResponse response,
						  String emailAddress) throws IOException
	{

		// variabili di output
		String otpNumber;
		String sentDate;
		String expiryDate;
		logger.debug("[MillemigliaComunicazioniServlet] getOTP ");

		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		// go back to form with error

		String textMessage = i18n.get(I18nKeySpecialPage.OTP_TEXT_MESSAGE) ;   //i18n


		OTPServiceClient otpServiceClient = wsClient;

		GetOTPRequest wsclientRequest=new GetOTPRequest();


		com.alitalia.aem.ws.otp.service.xsd1.ObjectFactory objectFactory = new com.alitalia.aem.ws.otp.service.xsd1.ObjectFactory ();
		JAXBElement<String> em =  objectFactory.createGetOTPRequestEmailAddress(emailAddress);
		wsclientRequest.setEmailAddress(em);

		GetOTPResponse wsclientResponse = otpServiceClient.GetOTP(wsclientRequest);

		response.setContentType("application/json");

		otpNumber = wsclientResponse.getOTP().getValue();
		sentDate = ""; //wsclientResponse.getSentDate().getValue();
		expiryDate = wsclientResponse.getExpiryDate().getValue();
//
//		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
//		try {
//			json.object();
//			json.key("OTP").value(otpNumber);
//			json.key("ExpiryDate").value(expiryDate);
//			json.key("SentDate").value(sentDate);
//			json.endObject();
//		} catch (Exception e) {
//			logger.error("[MillemigliaComunicazioniServlet] getOTP - Unexpected error generating JSON response.", e);
//		}

		return otpNumber;
	}

    private void sendOTPsms(SlingHttpServletRequest request,
                            SlingHttpServletResponse response) throws IOException
    {
        // variabili di input
        String phoneNumber = request.getParameter("phone");

        // variabili di output
        String otpNumber;
        String sentDate;
        String expiryDate;
        logger.debug("[MillemigliaComunicazioniServlet] sendOTPsms ");


        Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
        ResourceBundle resourceBundle = request.getResourceBundle(locale);
        final I18n i18n = new I18n(resourceBundle);
        // go back to form with error

        String textMessage = i18n.get(I18nKeySpecialPage.OTP_TEXT_MESSAGE) ;   //i18n

        OTPServiceClient otpServiceClient = wsClient;

        SendOTPRequest wsclientRequest=new SendOTPRequest();

        wsclientRequest.setMobileNumber(phoneNumber);
        wsclientRequest.setTextMessage(textMessage);

        SendOTPResponse wsclientResponse = otpServiceClient.sendOTP(wsclientRequest);

        response.setContentType("application/json");

        otpNumber = wsclientResponse.getOTP().getValue();
        sentDate = wsclientResponse.getSentDate().getValue();
        expiryDate = wsclientResponse.getExpiryDate().getValue();

        TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
        try {
            json.object();
            json.key("OTP").value(otpNumber);
            json.key("ExpiryDate").value(expiryDate);
            json.key("SentDate").value(sentDate);
            json.endObject();
        } catch (Exception e) {
            logger.error("[OTPServlet] getData - Unexpected error generating JSON response.", e);
        }
    }

    private MillemigliaRegistratiData getData(SlingHttpServletRequest request,
                                              int passo) {
        logger.debug("[MillemigliaRegistratiServlet] getData");

        MillemigliaRegistratiData millemigliaRegistratiData = (MillemigliaRegistratiData)
                request.getSession()
                        .getAttribute(MillemigliaRegistratiData.NAME);

        String trackCode = "0";
        if(millemigliaRegistratiData != null && millemigliaRegistratiData.getTrackingCod() != null && !"".equals(millemigliaRegistratiData.getTrackingCod())){
            trackCode = millemigliaRegistratiData.getTrackingCod();
            logger.debug("TrackingCode trovato in sessione: " + trackCode);
        } else{
            logger.debug("TrackingCode NON trovato in sessione");
        }

        millemigliaRegistratiData = new MillemigliaRegistratiData();

        millemigliaRegistratiData.setName(
                request.getParameter("name"));
        millemigliaRegistratiData.setSurname(
                request.getParameter("surname"));
        millemigliaRegistratiData.setDayOfBirth(
                request.getParameter("day")); // wsdl ok
        millemigliaRegistratiData.setMonthOfBirth(
                request.getParameter("month")); // wsdl ok
        millemigliaRegistratiData.setYearOfBirth(
                request.getParameter("year")); // wsdl ok
        millemigliaRegistratiData.setGender(
                request.getParameter("gender"));
        millemigliaRegistratiData.setProfession(
                request.getParameter("profession"));
        millemigliaRegistratiData.setPostalAddress(
                request.getParameter("postalAddress"));
        millemigliaRegistratiData.setCompany(
                request.getParameter("company"));
        millemigliaRegistratiData.setAddress(
                request.getParameter("address")); // wsdl ok
        millemigliaRegistratiData.setCap(
                request.getParameter("cap"));
        millemigliaRegistratiData.setUsername(
                request.getParameter("input_username"));
        millemigliaRegistratiData.setPassword(
                request.getParameter("input_password"));
        millemigliaRegistratiData.setCertifiedEmailAccount(
                request.getParameter("input_email"));
        millemigliaRegistratiData.setCity(
                request.getParameter("city"));
        millemigliaRegistratiData.setNation(
                request.getParameter("nation"));
        millemigliaRegistratiData.setProvince(
                request.getParameter("province"));
        millemigliaRegistratiData.setEmail(
                request.getParameter("email"));

        millemigliaRegistratiData.setNationalPrefix(
                request.getParameter("nationalPrefix"));

        millemigliaRegistratiData.setCountryNumber(
                request.getParameter("nationalPrefix"));

        millemigliaRegistratiData.setAreaPrefix(
                request.getParameter("areaPrefix"));
        millemigliaRegistratiData.setNumber(
                request.getParameter("number"));

        millemigliaRegistratiData.setTelephoneType(
                request.getParameter("numberType"));

        millemigliaRegistratiData.setTelephoneType(
                request.getParameter("typedelivery"));

        millemigliaRegistratiData.setTrackingCod(trackCode);
        millemigliaRegistratiData.setState(passo);

        millemigliaRegistratiData.setInputSecretQuestion(
                request.getParameter("secretQuestion"));

        millemigliaRegistratiData.setRispostaSegreta(
                request.getParameter("secureAnswer"));

        millemigliaRegistratiData.setCertifiedPhoneNumber(
                request.getParameter("input_newdelivery"));

		millemigliaRegistratiData.setPhonePrefix(
				request.getParameter("phonePrefix"));

		millemigliaRegistratiData.setFlagVerificaUserName_OK(
				Boolean.parseBoolean(request.getParameter("flagVerificaUserName_OK")));

		millemigliaRegistratiData.setFlagVerificaPassword_OK(
				Boolean.parseBoolean(request.getParameter("flagVerificaPassword_OK")));

		millemigliaRegistratiData.setFlagVerificaCellulare_OK(
				Boolean.parseBoolean(request.getParameter("flagVerificaCellulare_OK")));

		millemigliaRegistratiData.setFlagVerificaRisposta_OK(
				Boolean.parseBoolean(request.getParameter("flagVerificaRisposta_OK")));

		millemigliaRegistratiData.setFlagVerificaEmailUser_OK(
				Boolean.parseBoolean(request.getParameter("flagVerificaEmailUser_OK")));

		return millemigliaRegistratiData;
    }

	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request,
									   Exception e) {
		logger.debug("[MillemigliaComunicazioniServlet] saveDataIntoSession");

		MillemigliaRegistratiData millemigliaRegistratiData =
				(MillemigliaRegistratiData) request.getSession()
						.getAttribute(MillemigliaRegistratiData.NAME);

		millemigliaRegistratiData.setNewsletter(
				request.getParameter("newsletter"));
		millemigliaRegistratiData.setMethod(
				request.getParameter("method"));
		millemigliaRegistratiData.setSms(
				request.getParameter("sms"));
		millemigliaRegistratiData.setLanguage(
				request.getParameter("language"));
		millemigliaRegistratiData.setConsent(
				request.getParameter("trattamento"));
		millemigliaRegistratiData.setPlace(
				request.getParameter("posto"));
		millemigliaRegistratiData.setMeal(
				request.getParameter("pasto"));

		// go back to form with error
		Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		final I18n i18n = new I18n(resourceBundle);
		millemigliaRegistratiData.setError(i18n.get(
				I18nKeySpecialPage.ERROR_SERVICE_MILLMIGLIA_SUBSCRIBE));

		setData(millemigliaRegistratiData, request);

	}

	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request, Validator validator) {
		logger.debug("[MillemigliaComunicazioniServlet] setValidatorParameters");

		String selectorString = request.getRequestPathInfo().getSelectorString();

		if(selectorString==null){
			selectorString="millemigliacomunicazionisubmit-finale";
		}

		switch (selectorString) {
			case "millemigliacomunicazionisubmit":
			
			case "millemigliacomunicazionisubmit-finale":
				// get parameters from request
				String newsletter = request.getParameter("newsletter");
				String method = request.getParameter("method");
				String sms = request.getParameter("sms");
				String language = request.getParameter("language");
				String consent = request.getParameter("trattamento");
				String airFrance = request.getParameter("airFrance");
				String american = request.getParameter("americanAirlines");
				String british = request.getParameter("britishAirways");
				String delta = request.getParameter("delta");
				String easyjet = request.getParameter("easyjet");
				String emirates = request.getParameter("emirates");
				String iberia = request.getParameter("iberia");
				String lufthansa = request.getParameter("lufthansa");
				String meridiana = request.getParameter("meridiana");
				String turkish = request.getParameter("turkishAirlines");
				String other = request.getParameter("altra");
				String check_millemiglia = request.getParameter("check_millemiglia");

				// add validate conditions
				validator.addDirectConditionMessagePattern("newsletter", newsletter,
						I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
						I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
				validator.setAllowedValues("newsletter", newsletter,
						new String[]{"A", "AP", "NO"},
						I18nKeyCommon.MESSAGE_INVALID_FIELD);

				validator.addDirectConditionMessagePattern("method", method,
						I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
						I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
				validator.setAllowedValues("method", method,
						new String[]{"E", "P"},
						I18nKeyCommon.MESSAGE_INVALID_FIELD);

				if (!isEmpty(sms)) {
					validator.addCrossCondition("sms", sms, CHECKBOX_CHECKED,
							I18nKeyCommon.MESSAGE_INVALID_FIELD, "areEqual");
				}

				validator.addDirectConditionMessagePattern("language", language,
						I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
						I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");

				validator.addDirectConditionMessagePattern("trattamento", consent,
						I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD,
						I18nKeySpecialPage.NAME_ERROR_EMPTY, "isNotEmpty");
				validator.setAllowedValues("trattamento", consent,
						new String[]{"y", "n"},
						I18nKeyCommon.MESSAGE_INVALID_FIELD);

				int numberFF = 0;

				if (!isEmpty(airFrance)) {
					numberFF++;
					validator.addCrossCondition("airFrance", airFrance,
							CHECKBOX_CHECKED, I18nKeyCommon.MESSAGE_INVALID_FIELD,
							"areEqual");
				}

				if (!isEmpty(american)) {
					numberFF++;
					validator.addCrossCondition("americanAirlines", american,
							CHECKBOX_CHECKED, I18nKeyCommon.MESSAGE_INVALID_FIELD,
							"areEqual");
				}

				if (!isEmpty(british)) {
					numberFF++;
					validator.addCrossCondition("britishAirways", british,
							CHECKBOX_CHECKED, I18nKeyCommon.MESSAGE_INVALID_FIELD,
							"areEqual");
				}

				if (!isEmpty(delta)) {
					numberFF++;
					validator.addCrossCondition("delta", delta, CHECKBOX_CHECKED,
							I18nKeyCommon.MESSAGE_INVALID_FIELD, "areEqual");
				}

				if (!isEmpty(easyjet)) {
					numberFF++;
					validator.addCrossCondition("easyjet", easyjet, CHECKBOX_CHECKED,
							I18nKeyCommon.MESSAGE_INVALID_FIELD, "areEqual");
				}

				if (!isEmpty(emirates)) {
					numberFF++;
					validator.addCrossCondition("emirates", emirates, CHECKBOX_CHECKED,
							I18nKeyCommon.MESSAGE_INVALID_FIELD, "areEqual");
				}

				if (!isEmpty(iberia)) {
					numberFF++;
					validator.addCrossCondition("iberia", iberia, CHECKBOX_CHECKED,
							I18nKeyCommon.MESSAGE_INVALID_FIELD, "areEqual");
				}

				if (!isEmpty(lufthansa)) {
					numberFF++;
					validator.addCrossCondition("lufthansa", lufthansa, CHECKBOX_CHECKED,
							I18nKeyCommon.MESSAGE_INVALID_FIELD, "areEqual");
				}

				if (!isEmpty(meridiana)) {
					numberFF++;
					validator.addCrossCondition("meridiana", meridiana, CHECKBOX_CHECKED,
							I18nKeyCommon.MESSAGE_INVALID_FIELD, "areEqual");
				}

				if (!isEmpty(turkish)) {
					numberFF++;
					validator.addCrossCondition("turkish", turkish, CHECKBOX_CHECKED,
							I18nKeyCommon.MESSAGE_INVALID_FIELD, "areEqual");
				}

				if (!isEmpty(other)) {
					numberFF++;
					validator.addCrossCondition("altra", other, CHECKBOX_CHECKED,
							I18nKeyCommon.MESSAGE_INVALID_FIELD, "areEqual");
				}

				validator.addDirectConditionMessagePattern("check_millemiglia",
						check_millemiglia, I18nKeySpecialPage.REQUIRED_FIELD,
						I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "isNotEmpty");
				validator.addCrossCondition("airFrance",
						Integer.toString(numberFF), MAX_FREQUENT_FLYERS,
						I18nKeySpecialPage.MAX_FREQUENT_FLYERS_ERROR,
						"isLessThanMaxFrequentFlyers");
		}

		return validator;

	}

	/*
	 * isEmpty
	 */
	private boolean isEmpty(String value) {
		try {
			if (null == value || 0 == value.length()) {
				return true;
			}
			return false;
		} catch (Exception e) {
			return true;
		}
	}

	/*
	 * setData
	 */
	private void setData(MillemigliaRegistratiData millemigliaRegistratiData, SlingHttpServletRequest request) {
		logger.debug("[MillemigliaComunicazioniServlet] setData");
		
		millemigliaRegistratiData.setNewsletter(
				request.getParameter("newsletter"));
		millemigliaRegistratiData.setMethod(
				request.getParameter("method"));
		millemigliaRegistratiData.setSms(
				request.getParameter("sms"));
		millemigliaRegistratiData.setLanguage(
				request.getParameter("language"));
		millemigliaRegistratiData.setConsent(
				request.getParameter("trattamento"));
		millemigliaRegistratiData.setPlace(
				request.getParameter("posto"));
		millemigliaRegistratiData.setMeal(
				request.getParameter("pasto"));
		millemigliaRegistratiData.setCheckMillemiglia(
				request.getParameter("check_millemiglia"));
		millemigliaRegistratiData.setCheckYoung(
				request.getParameter("check_young"));
		millemigliaRegistratiData.setState(2);

		String airFrance = request.getParameter("airFrance");
		String american = request.getParameter("americanAirlines");
		String british = request.getParameter("britishAirways");
		String delta = request.getParameter("delta");
		String easyjet = request.getParameter("easyjet");
		String emirates = request.getParameter("emirates");
		String iberia = request.getParameter("iberia");
		String lufthansa = request.getParameter("lufthansa");
		String meridiana = request.getParameter("meridiana");
		String turkish = request.getParameter("turkishAirlines");
		String other = request.getParameter("altra");
		FrequentFlyer flyers = new FrequentFlyer(airFrance, american, british,
				delta, easyjet, emirates, iberia, lufthansa, meridiana,
				turkish, other);
		millemigliaRegistratiData.setFrequentFlyer(flyers);
		
		request.getSession().setAttribute(MillemigliaRegistratiData.NAME,
				millemigliaRegistratiData);

	}

	/*
	 *  getLanguageCode
	 */
	private String getLanguageCode(String value) {

		if (value != null) {

			if (value.equals(LANGUAGE_ITA)) {
				return CODE_ITA;
			}
			if (value.equals(LANGUAGE_FRA)) {
				return CODE_FRA;
			}
			if (value.equals(LANGUAGE_TED)) {
				return CODE_TED;
			}
			if (value.equals(LANGUAGE_ING)) {
				return CODE_ING;
			}
			if (value.equals(LANGUAGE_GIA)) {
				return CODE_GIA;
			}
			if (value.equals(LANGUAGE_POR)) {
				return CODE_POR;
			}
			if (value.equals(LANGUAGE_SPA)) {
				return CODE_SPA;
			}
			return "";

		} else {
			return null;
		}

	}

	/*
	 * getMailBody
	 */
	public String getMailBody(Resource resource, String templateMail, boolean secure) {
		logger.debug("[MillemigliaComunicazioniServlet] getMailBody");
		
		String result = null;
		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(resource, false);
		try {

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			/* Get resource resolver */
			@SuppressWarnings("deprecation")
			ResourceResolver resourceResolver =
				resolverFactory.getAdministrativeResourceResolver(null);
			String requestedUrl = baseUrl + AlitaliaConstants.BASE_CONFIG_PATH
					+ "/" + templateMail;

			logger.debug("[MillemigliaComunicazioniServlet] getMailBody.requestedUrl" + requestedUrl);

			/* Setup request */
			HttpServletRequest req =
					requestResponseFactory.createRequest("GET", requestedUrl);
			WCMMode.DISABLED.toRequest(req);

			out = new ByteArrayOutputStream();
			HttpServletResponse resp =
					requestResponseFactory.createResponse(out);

			/* Process request through Sling */
			requestProcessor.processRequest(req, resp, resourceResolver);

			String encoding = "UTF-8";

			result = new String(out.toByteArray(), Charset.forName(encoding));

		} catch (LoginException | ServletException | IOException e) {
			logger.error("Generic exception while retrieving email template "
					+ "from JCR repository: {}", e);
			result = "";
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("Template retrieved and APIS/ESTA placeholders "
					+ "replaced: {} ", result);
		}
		
		return result;
	}

	protected void performResetAccount(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException{
		String encodedUrlAccount = request.getParameter("encodedUrlAccount");

		try {
			if (encodedUrlAccount.isEmpty()) {
				logger.error("Encoded URL Field EMPTY !!!");
				throw new IOException(I18nKeySpecialPage.ERROR_MESSAGE_MILLEMIGLIA_RESETACCOUNT);
			}
			String decodedUrlAccount = AlitaliaUtils.decrypt(AlitaliaUtils.getSecretKey(), encodedUrlAccount);
			//TODO invocare redirect della pagina che andra' chiamata
			configuration.getBasePersonalArea();

		} catch (Exception e) {
			throw new IOException(I18nKeySpecialPage.ERROR_MESSAGE_MILLEMIGLIA_RESETACCOUNT);
		}

	}

    protected AlitaliaConfigurationHolder getConfiguration() {
        return configuration;
    }

	protected void performLockAccount(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException{

		logger.debug("[MillemigliaComunicazioniServlet] performLockAccount");

		String encodedUrlAccount = request.getParameter("encodedUrlAccount");

		try {
			if (encodedUrlAccount.isEmpty()) {
				logger.error("Encoded URL Field EMPTY !!!");
				throw new IOException(I18nKeySpecialPage.ERROR_MESSAGE_MILLEMIGLIA_RESETACCOUNT);
			}
			String decodedUrlAccount = AlitaliaUtils.decrypt(AlitaliaUtils.getSecretKey(), encodedUrlAccount);

			String[] pairs = decodedUrlAccount.split(";");

			String IdProfilo = "";
			String dataScadenza = "";
			if(pairs.length>0){
				IdProfilo=pairs[0];
			}
			if(pairs.length>1){
				dataScadenza=pairs[1];
			}

			//TODO: verificare datascadenza

			GetDatiSicurezzaProfiloRequestLocal wsGetDatiSicurezzaProfiloRequest=new GetDatiSicurezzaProfiloRequestLocal();
			wsGetDatiSicurezzaProfiloRequest.setIdProfilo(IdProfilo);

			GetDatiSicurezzaProfiloResponseLocal wsGetDatiSicurezzaProfiloResponse = consumerLoginDelegate.GetDatiSicurezzaProfilo(wsGetDatiSicurezzaProfiloRequest);

			String EmailAccount = wsGetDatiSicurezzaProfiloResponse.getEmailAccount();
			String PrefissoNazioneCellulare =wsGetDatiSicurezzaProfiloResponse.getPrefissoNazioneCellulare();
			String Cellulare = wsGetDatiSicurezzaProfiloResponse.getCellulare();
			String UserName =wsGetDatiSicurezzaProfiloResponse.getUsername();


			SetDatiSicurezzaProfiloRequest wsSetDatiSicurezzaProfiloRequest=new SetDatiSicurezzaProfiloRequest();

			wsSetDatiSicurezzaProfiloRequest.setPrefissoNazioneCellulare(PrefissoNazioneCellulare); //es. "0039"
			wsSetDatiSicurezzaProfiloRequest.setEmailAccount(EmailAccount); //es. "mail@example.com"
			wsSetDatiSicurezzaProfiloRequest.setCellulare(Cellulare);   //es. "3470000000"
			wsSetDatiSicurezzaProfiloRequest.setIdProfilo(IdProfilo);  //es. "676713"
			wsSetDatiSicurezzaProfiloRequest.setLockedout(true);
			wsSetDatiSicurezzaProfiloRequest.setUserName(UserName); //es. "usercorazza"

			SetDatiSicurezzaProfiloResponse wsSetDatiSicurezzaProfiloResponse = consumerLoginDelegate.SetDatiSicurezzaProfilo(wsSetDatiSicurezzaProfiloRequest);

			if(wsSetDatiSicurezzaProfiloResponse.getFlagIDProfiloSicurezzaSaved())
			{
				//account bloccato con successo
				logger.debug("[MillemigliaComunicazioniServlet] performLockAccount - account bloccato con successo");
			}else {
				//impossibile bloccare account
				logger.debug("[MillemigliaComunicazioniServlet] performLockAccount - impossibile bloccare account");
			}

		} catch (Exception e) {
			logger.error("[MillemigliaComunicazioniServlet] performLockAccount - errore in blocco account",e);

			throw new IOException(I18nKeySpecialPage.ERROR_MESSAGE_MILLEMIGLIA_RESETACCOUNT);
		}

	}

	protected void performAssociaMyAlitalia(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException{
		logger.debug("[MillemigliaComunicazioniServlet] performAssociaMyAlitalia");

		HttpSession session = request.getSession();

		String MM_code = "";
		String MM_callbackurl = "";
		String MM_action = "";
		String MM_emailcert = "";
		String MM_success = "";
		String isError = "FALSE";
		String errorDescription = "";
		;

		if(session.getAttribute("myalitalia_associa_MM_code")!=null){
			MM_code = (String) session.getAttribute("myalitalia_associa_MM_code");
		}

		if(session.getAttribute("myalitalia_associa_MM_callbackurl")!=null){
			MM_callbackurl = (String) session.getAttribute("myalitalia_associa_MM_callbackurl");
		}

		if(session.getAttribute("myalitalia_associa_MM_action")!=null){
			MM_action = (String) session.getAttribute("myalitalia_associa_MM_action");
		}

		if(session.getAttribute("myalitalia_associa_MM_emailcert")!=null){
			MM_emailcert = (String) session.getAttribute("myalitalia_associa_MM_emailcert");
		}

		if(session.getAttribute("myalitalia_associa_MM_success")!=null){
			MM_success = (String) session.getAttribute("myalitalia_associa_MM_success");
		}

		// variabili di input
        String username = request.getParameter("username");
        String password = request.getParameter("password");

		String mmCode = null;
		String mmPin = null;

		//della password viene fatto un hash MD5 convertito in maiuscolo per passarla al webservice
		String md5password = DigestUtils.md5Hex(password).toUpperCase();

		//se userName e' tutto numerico allora e' stato ustato un codice millemiglia
		if(StringUtils.isNumeric(username))
		{
			mmCode=username;
			username=null;
		}

		try {
			if(MM_action.equalsIgnoreCase("ASSOCIA")){

				String tid = IDFactory.getTid();
				String sid = IDFactory.getSid(request);

				MilleMigliaLoginSARequest loginSARequest = new MilleMigliaLoginSARequest(tid, sid);
				loginSARequest.setCodiceMM(mmCode);
				loginSARequest.setUsername(username);
				loginSARequest.setPassword(md5password);
				MilleMigliaLoginSAResponse loginSAResponse = consumerLoginDelegate.milleMigliaLoginSA(loginSARequest);

				if (loginSAResponse.getCustomerSA() != null) {

					MM_emailcert=loginSAResponse.getCustomerSA().getCertifiedEmailAccount();

					MMCustomerProfileData customerProfile = loginSAResponse.getCustomerSA().getMmCustomerProfileData();

					if(customerProfile !=null )
					{
						if(!customerProfile.getCustomerNumber().isEmpty()){
							MM_code=customerProfile.getCustomerNumber();
						}
					}
				}

				boolean successful = loginSAResponse.isLoginSuccessful();
				MM_success=(successful)?"true":"false";

				if(!successful) {
					MM_code="";
					MM_emailcert="";
				}

				session.setAttribute("myalitalia_associa_MM_code",MM_code);
				session.setAttribute("myalitalia_associa_MM_action",MM_action);
				session.setAttribute("myalitalia_associa_MM_emailcert",MM_emailcert);
				session.setAttribute("myalitalia_associa_MM_success",MM_success);

				if(!successful){
					isError = "TRUE";
					if (loginSAResponse.getErrorDescription() != null){
						errorDescription=loginSAResponse.getErrorDescription();
					}else{
						final I18n i18n = new I18n(request.getResourceBundle(
								AlitaliaUtils.findResourceLocale(request.getResource())));

						errorDescription = i18n.get("MM.SA.CredenzialiNonValide");
					}
				}

			}else if(MM_action.equalsIgnoreCase("DISASSOCIA")){

				String tid = IDFactory.getTid();
				String sid = IDFactory.getSid(request);

				MilleMigliaLoginSARequest loginSARequest = new MilleMigliaLoginSARequest(tid, sid);
				loginSARequest.setCodiceMM(mmCode);
				loginSARequest.setUsername(username);
				loginSARequest.setPassword(md5password);
				MilleMigliaLoginSAResponse loginSAResponse = consumerLoginDelegate.milleMigliaLoginSA(loginSARequest);

				if (loginSAResponse.getCustomerSA() != null) {

					MM_emailcert=loginSAResponse.getCustomerSA().getCertifiedEmailAccount();

					MMCustomerProfileData customerProfile = loginSAResponse.getCustomerSA().getMmCustomerProfileData();

					if(customerProfile !=null )
					{
						if(!customerProfile.getCustomerNumber().isEmpty()){
							MM_code=customerProfile.getCustomerNumber();
						}
					}
				}

				boolean successful = loginSAResponse.isLoginSuccessful();


				if(MM_code.equalsIgnoreCase(mmCode)) {
					//controllo di sicurezza se CodiceMilleMiglia corrisponde e' OK
					successful = true;
				}else{
					//altrimenti
					successful = false;
					MM_code="";
					MM_emailcert="";
				}

				MM_success=(successful)?"true":"false";

				session.setAttribute("myalitalia_associa_MM_code",MM_code);
				session.setAttribute("myalitalia_associa_MM_action",MM_action);
				session.setAttribute("myalitalia_associa_MM_emailcert",MM_emailcert);
				session.setAttribute("myalitalia_associa_MM_success",MM_success);

				if(!successful){
					isError = "TRUE";
					if (loginSAResponse.getErrorDescription() != null){
						errorDescription=loginSAResponse.getErrorDescription();
					}else{
						final I18n i18n = new I18n(request.getResourceBundle(
								AlitaliaUtils.findResourceLocale(request.getResource())));

						errorDescription = i18n.get("MM.SA.CredenzialiNonValide");
					}
				}
			}

			//ridireziono sulla callbackUrl MyAlitalia
			//response.sendRedirect(MM_callbackurl);

			response.setContentType("application/json");
			json = new TidyJSONWriter(response.getWriter());

			try {
				json.object();
				json.key("redirectUrl").value(MM_callbackurl);
				json.key("isError").value(isError);
				json.key("errorDescription").value(errorDescription);
				json.key("MM_code").value(MM_code);
				json.key("MM_action").value(MM_action);
				json.key("pw").value(password);
				json.endObject();
			} catch (Exception e) {
				logger.error("[MillemigliaComunicazioniServlet] Associazione MyAlitalia non riuscita.", e);
			}

		} catch (Exception e) {
			logger.error("[MillemigliaComunicazioniServlet] performAssociaMyAlitalia - errore in LoginSA per account",e);

			throw new IOException(I18nKeySpecialPage.ERROR_MESSAGE_MILLEMIGLIA_RESETACCOUNT);
		}

	}
 
	/*NON UNSATA*/
    private void sendSMS(String phoneNumberTo, String messageSMSText, SlingHttpServletRequest request)  {
        
        logger.info("[MillemigliaComunicazioniServlet] - SendSMS");
        
        try{
            
            logger.debug("[MillemigliaComunicazioniServlet] - SendSMS [INIZIO] ");
            
            com.alitalia.aem.ws.sendsms.xsd.ObjectFactory factory = new com.alitalia.aem.ws.sendsms.xsd.ObjectFactory();
            SmsMsg requestSms = factory.createSmsMsg();
            
            requestSms.setFromPhoneNumber(getConfiguration().getMMSASendsmsFromPhoneNumber());
            requestSms.setApplicationName(getConfiguration().getMMSASendsmsApplicationName());
            
            requestSms.setMessage(messageSMSText);
            requestSms.setToPhoneNumber(phoneNumberTo);
            
            //setto i parametri di invio SMS
            SmsParametersType parameter = factory.createSmsParametersType();
            parameter.setNotificationSend(getConfiguration().getMMSASendsmsNotificationSend());
            parameter.setNotificationLocalSend(getConfiguration().getMMSASendsmsNotificationLocalSend());
            parameter.setNotificationReceive(getConfiguration().getMMSASendsmsNotificationReceive());
            parameter.setNotificationNotReceive(getConfiguration().getMMSASendsmsNotificationNotReceive());
            
            requestSms.setSmsParameters(parameter);
            
            SmsMsgReturn smsMsgReturn = wsSendSmsClient.sendSms(requestSms);
            
            logger.info("[MillemigliaComunicazioniServlet] - SendSMS.msgId = " + smsMsgReturn.getMsgId());

//            TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
//            try {
//                json.object();
//                json.key("msgId").value(smsMsgReturn.getMsgId());
//                json.key("description").value(smsMsgReturn.getDescription());
//                json.key("responseCode").value(smsMsgReturn.getResponseCode());
//                json.endObject();
//            } catch (Exception e) {
//                logger.error("[MillemigliaComunicazioniServlet] SendSMS - Unexpected error generating JSON response.", e);
//            }
        
        }catch(Exception e){
            //throw new RuntimeException("[MillemigliaComunicazioniServlet] - Errore durante l'invocazione del servizio.", e);
        }
        
        logger.debug("[MillemigliaComunicazioniServlet] - SendSMS [FINE] ");
    }
    
    /*NON UNSATA*/
    private boolean sendEmail(String mailFrom ,String emailTo, String Subject, String messageText, boolean isBodyHtml ) throws Exception	{
        logger.debug("[MillemigliaComunicazioniServlet] sendEmail ");
        
        try {
            
            
            SendMailServiceClient sendMailClient = wsSendMailClient;
            MessaggioEmail msg= new MessaggioEmail() ;
            
            msg.setFrom(mailFrom);
            msg.setTo(emailTo);
            msg.setSubject(Subject);
            msg.setMessageText(messageText);
            
            msg.setIsBodyHtml(isBodyHtml);
            
            Boolean sendMailClientResponse = sendMailClient.sendMail(msg);
            
            return  sendMailClientResponse.booleanValue();
            
        } catch (WSClientException wse) {
            logger.error("Errore durante chiamata webservice sendMailClient.sendMail()",wse);
            throw new Exception("Errore durante chiamata webservice sendMailClient.sendMail()",wse);
            
        }
    }

}