package com.alitalia.aem.consumer.model.content.breadcrumb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.utils.LabelUrl;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;

@Model(adaptables = { SlingHttpServletRequest.class })
public class Breadcrumb {

	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(Breadcrumb.class);

	@Self
	private SlingHttpServletRequest request;

	private LabelUrl[] breadcrumbItems;

	/**
	 * This method is used to get the resource of the home page.
	 */
	@PostConstruct
	protected void initModel() {
		try{
		final Resource resource = request.getResource();

		List<LabelUrl> breadcrumbItemList = new ArrayList<LabelUrl>();
		PageFilter pageFilter = new PageFilter(request);

		// risalgo i parent della resource fino a raggiungere la prima pagina
		// (che è la pagina corrente)
		Resource parentResource = resource;
		while (parentResource.adaptTo(Page.class) == null) {
			parentResource = parentResource.getParent();
			if (parentResource == null) {
				parentResource = resource.getResourceResolver().getResource(
						resource.getPath().substring(0,
								resource.getPath().lastIndexOf("jcr:content")));
				break;
			}
		}

		Page parentPage = parentResource.adaptTo(Page.class);

		while (parentPage != null) {
			if (pageFilter.includes(parentPage)) {
				String navLabel = parentPage.getNavigationTitle();
				if (navLabel == null) {
					navLabel = parentPage.getTitle();
				}
				breadcrumbItemList.add(new LabelUrl(navLabel, parentPage
						.getPath()));
				// breadcrumbItemList.add(new LabelUrl(parentPage.getTitle(),
				// parentPage.getPath()));
			}

			if (((String) parentPage.getProperties().get("cq:template"))
					.contains("/apps/alitalia/templates/language-template")
					|| ((String) parentPage.getProperties().get("cq:template"))
							.contains("/apps/alitalia/templates/directory")) {
				break;
			}

			parentPage = parentPage.getParent();
		}
		if (!breadcrumbItemList.isEmpty()) {
			breadcrumbItemList.remove(breadcrumbItemList.size() - 1);
		}
		Collections.reverse(breadcrumbItemList);

		breadcrumbItems = (LabelUrl[]) breadcrumbItemList
				.toArray(new LabelUrl[breadcrumbItemList.size()]);
		} catch(Exception e){
			logger.error("Errore durante costruzione breadcrumb" , e);
			
		}

	}

	/**
	 * Returns all the parents of current page
	 * 
	 * @return The breadcrumb
	 */
	public LabelUrl[] getBreadcrumbItems() {
		return breadcrumbItems;
	}

}
