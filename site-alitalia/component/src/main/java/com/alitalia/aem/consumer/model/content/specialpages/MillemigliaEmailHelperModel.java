package com.alitalia.aem.consumer.model.content.specialpages;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.wcm.api.WCMMode;

@Model(adaptables={SlingHttpServletRequest.class})
public class MillemigliaEmailHelperModel {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	private String hostName = "";
	private String personalAreaBaseUrl = "";
	private String mailImageUrl = "";
	private String marketSiteUrl = "";
	private String yourProfileUrl = "";
	private String recuperaPinUrl = "";
	private String statementUrl = "";
	private String awardTicketUrl = "";
	private String webCheckinUrl = "";
	private String buyMilesUrl = "";
	private String privacyStatementUrl = "";
	private String mmRulesUrl = "";
	private String faqUrl = "";
	private String unsubscribeNewsletterUrl = "";
	private String promozioniUrl = "";
	private String contattiAssistenza = "";

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;
	
	@Inject
    private AlitaliaConfigurationHolder configuration;
	
	@PostConstruct
	protected void initModel() {
		ResourceResolver resourceResolver = slingHttpServletRequest.getResourceResolver();
		logger.debug("RecuperaHostNameModel initModel");
		String prefix = "http://";
		if (!isWCMEnabled() && configuration.getHttpsEnabled())
			prefix = "https://";
		hostName = prefix + configuration.getExternalDomain();
		logger.debug("hostName: " + hostName);
//		mailImageUrl = hostName + "/etc/designs/alitalia/clientlibs/images/";
		mailImageUrl = hostName + configuration.getStaticImagesPath();
		logger.debug("mailImageUrl: " + mailImageUrl);
		this.marketSiteUrl = hostName + resourceResolver.map(AlitaliaUtils.findSiteBaseExternalUrl(slingHttpServletRequest.getResource(), false));
		this.personalAreaBaseUrl = this.marketSiteUrl + "/" + configuration.getBasePersonalArea();
		this.yourProfileUrl = "/your-profile.html";
		this.recuperaPinUrl = "/special-pages/recupera-pin.html";
		this.statementUrl = "/account-statement.html";
		this.awardTicketUrl = "/dedicated-services.html";
		this.webCheckinUrl = "/fly-alitalia/check-in/online-check-in.html";
		this.buyMilesUrl = "/dedicated-services.html";
		this.privacyStatementUrl = "/special-pages/privacy.html"; 
		this.mmRulesUrl = "/millemiglia/theprogram/rules.html";
		this.faqUrl = "/millemiglia/Unsubscribe-NewsletterMM.html";
		this.unsubscribeNewsletterUrl = "/support/contact-assistance/customer-center.html";
		this.promozioniUrl="/millemiglia/news-e-promozioni.html";
		this.contattiAssistenza="/supporto/contatti-assistenza.html";
	}
	
	public String getHostName() {
		return hostName;
	}

	public String getPersonalAreaBaseUrl() {
		return personalAreaBaseUrl;
	}

	public String getMailImageUrl() {
		return mailImageUrl;
	}

	public String getMarketSiteUrl() {
		return marketSiteUrl;
	}

	public String getYourProfileUrl() {
		return yourProfileUrl;
	}

	public String getRecuperaPinUrl() {
		return recuperaPinUrl;
	}

	public String getStatementUrl() {
		return statementUrl;
	}

	public String getAwardTicketUrl() {
		return awardTicketUrl;
	}

	public String getWebCheckinUrl() {
		return webCheckinUrl;
	}

	public String getBuyMilesUrl() {
		return buyMilesUrl;
	}

	public String getPrivacyStatementUrl() {
		return privacyStatementUrl;
	}

	public String getMmRulesUrl() {
		return mmRulesUrl;
	}

	public String getFaqUrl() {
		return faqUrl;
	}

	public String getUnsubscribeNewsletterUrl() {
		return unsubscribeNewsletterUrl;
	}

	public String getPromozioniUrl() {
		return promozioniUrl;
	}

	public String getContattiAssistenza() {
		return contattiAssistenza;
	}

	private boolean isWCMEnabled() {
		if (slingHttpServletRequest != null) {
			WCMMode mode = WCMMode.fromRequest(slingHttpServletRequest);
			return (mode != null && (
					WCMMode.EDIT.equals(mode) ||
					WCMMode.PREVIEW.equals(mode) ||
					WCMMode.DESIGN.equals(mode)));
		} else {
			return false;
		}
	}
}
