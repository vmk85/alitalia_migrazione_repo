package com.alitalia.aem.consumer.booking.model.extrabaggage;

import java.util.*;

/** 
 Sabre Request Model for RequiredProperties 
*/
public class RequiredProperties
{
	private ArrayList<AbstractPropertyDefinition> propertyDefinitionList;
	public final ArrayList<AbstractPropertyDefinition> getpropertyDefinitionList()
	{
		return propertyDefinitionList;
	}
	public final void setpropertyDefinitionList(ArrayList<AbstractPropertyDefinition> value)
	{
		propertyDefinitionList = value;
	}
}