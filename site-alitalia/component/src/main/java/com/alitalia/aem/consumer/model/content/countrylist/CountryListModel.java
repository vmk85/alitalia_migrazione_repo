package com.alitalia.aem.consumer.model.content.countrylist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.day.cq.wcm.api.PageManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.Country;
import com.alitalia.aem.consumer.utils.CountryObject;
import com.alitalia.aem.consumer.utils.Language;
import com.alitalia.aem.consumer.utils.UrlCollection;
import com.alitalia.aem.consumer.utils.UrlObject;
import com.day.cq.analytics.testandtarget.Resolution;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CountryListModel extends GenericBaseModel {
    
    public class AlternateDetail extends ArrayList<String> {
        
        public AlternateDetail(){
            hreflang ="";
            href="";
        }
    
        public String getHreflang() {
            return hreflang;
        }
    
        public void setHreflang(String hreflang) {
            this.hreflang = hreflang;
        }
    
        public String getHref() {
            return href;
        }
    
        public void setHref(String href) {
            this.href = href;
        }
    
        private String hreflang;
        private String href;
    

    }
    
    
    public class Metatag {
        
        
        public Metatag() {
            Canonical = "";
            Alternate = new ArrayList<AlternateDetail>();
        }
        
        
        public String getCanonical() {
            return Canonical;
        }
    
        public void setCanonical(String canonical) {
            Canonical = canonical;
        }
    
        public ArrayList<AlternateDetail> getAlternate() {
            return Alternate;
        }
    
        public void setAlternate(ArrayList<AlternateDetail> alternate) {
            Alternate = alternate;
        }
    
        private String Canonical;
        private ArrayList<AlternateDetail> Alternate;
        
    }
    
	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	@Inject
	private Page currentPage;
	
	@Inject
	private AlitaliaConfigurationHolder alitaliaConfigurationHolder;

	private String countryRoot;
	private String languageRoot;
	private CountryObject countryObject;
	private UrlCollection urls;
	private Logger logger = LoggerFactory.getLogger(CountryListModel.class);
	private Metatag metatag;
	
	@PostConstruct
	protected void initModel() {
		super.initBaseModel(slingHttpServletRequest);
		logger.trace("Initializing model << CountryListModel >>");
		ResourceResolver resourceResolver = slingHttpServletRequest.getResourceResolver();

		countryRoot = resourceResolver.map(currentPage.getAbsoluteParent(AlitaliaConstants.SITE_ROOT_LEVEL + 1).getPath());
		languageRoot = resourceResolver.map(currentPage.getAbsoluteParent(AlitaliaConstants.SITE_ROOT_LEVEL + 2).getPath());
		logger.trace("countryRoot "+countryRoot );
		logger.trace("languageRoot "+languageRoot );

	}


	public Metatag getMetatag(){
        //ArrayList<String> tags=new ArrayList<String>();
	    try{
            Page ancestor = currentPage.getAbsoluteParent(AlitaliaConstants.SITE_ROOT_LEVEL);
            setMetatag(ancestor);
            return metatag;
        }catch(Exception e){
            logger.debug("Exception"+e.getMessage());
            return null;
        }
    }
    
    private void setMetatag(Page rootPage) {
	    
	    String alias="";
        metatag = new Metatag();
        countryObject = new CountryObject();
        String domain = alitaliaConfigurationHolder.getExternalDomain();
        
        ResourceResolver resourceResolver1 = request.getResourceResolver();
        PageManager pageManager = resourceResolver1.adaptTo(PageManager.class);
        String currentPageLink = currentPage.getPath();
        
        String baseUri = currentPageLink.substring(0,33); //Restituisce la sottostringa del tipo "/content/alitalia/alitalia-xx/yy/"
        String currentPageUri=currentPageLink.substring(33);  //Restituisce la sottostringa del tipo "homepage.html", "check-in-search/check-in-seatmap.html"
    
        ResourceResolver resourceResolver = slingHttpServletRequest.getResourceResolver();
        
        Iterator<Page> countries = null;
        if (rootPage != null) {
            countries = rootPage.listChildren(new PageFilter(slingHttpServletRequest));
        }
        
        if (countries != null) {
            while (countries.hasNext()) {
                
                Page countryPage = countries.next();
                String countryLink = resourceResolver.map(countryPage.getPath() + AlitaliaConstants.HTML_EXT);

                Iterator<Page> languages = countryPage.listChildren(new PageFilter(slingHttpServletRequest));
                int languageIdx = 0;
                while (languages.hasNext()) {
                    
                    Page languagePage = languages.next();
                    String languageName = i18n.get(languagePage.getTitle().toLowerCase());
                    
                    String link = AlitaliaUtils.findSiteBaseExternalUrl(languagePage.adaptTo(Resource.class), true) + currentPageUri;
                    String baseLanguageUri=AlitaliaUtils.findSiteBaseExternalUrl(languagePage.adaptTo(Resource.class), true);
                    link = languagePage.getProperties().get("landingpage", link);
                    
                    String languageLink = resourceResolver.map(link);
                    
                    boolean languageIsSelected = languageLink.startsWith(languageRoot);
    
                    String market=baseLanguageUri.substring(27,29);
                    String language=baseLanguageUri.substring(30,32);
                    String baseCurrentPageUri=currentPageUri.substring(0,currentPageUri.lastIndexOf("/")+1);;

                    if (languageIsSelected) {
                        String completeAlias="";
                        Page page_1 = pageManager.getPage(baseLanguageUri + currentPageUri);
    
                        if(pageManager.getPage(baseLanguageUri + currentPageUri)!=null ){
                            completeAlias=computeCompleteAlias(pageManager,baseLanguageUri,currentPageUri);
                            alias=(pageManager.getPage(baseLanguageUri + currentPageUri).getProperties().get("sling:alias")!=null ? pageManager.getPage(baseLanguageUri + currentPageUri).getProperties().get("sling:alias").toString() : "");
                        }
    
                        if (alias.equals("homepage") || (currentPageUri.equals("homepage"))) {
                            if (alias!=""){
                                metatag.Canonical="https://" + domain + "/" + language + "_" + market;
                            }else{
                                metatag.Canonical="https://" + domain + "/" + language + "_" + market;
                            }
                        }else{
                            metatag.Canonical="https://" + domain + "/" + language + "_" + market + "/" + completeAlias + AlitaliaConstants.HTML_EXT;
                            /*if (alias!=""){
                                metatag.Canonical="https://" + domain + "/" + language + "_" + market + "/" + baseCurrentPageUri + alias + AlitaliaConstants.HTML_EXT;
                            }else{
                                metatag.Canonical="https://" + domain + "/" + language + "_" + market + "/" + currentPageUri + AlitaliaConstants.HTML_EXT;
                            }*/
                        }
                        
                        logger.trace("IdLanguadeSelect "+languageIdx);
                        countryObject.setLanguageSelected(languageIdx);
                    }else {
                    
                    }
                    
                    
                    //inizio sezione ALTERNATE
                    String completeAlias="";
                    AlternateDetail ad = new AlternateDetail();
                    Page page_1 = pageManager.getPage(baseLanguageUri + currentPageUri);
    
                    if(pageManager.getPage(baseLanguageUri + currentPageUri)!=null ){
                        completeAlias=computeCompleteAlias(pageManager,baseLanguageUri,currentPageUri);
                        alias=(pageManager.getPage(baseLanguageUri + currentPageUri).getProperties().get("sling:alias")!=null ? pageManager.getPage(baseLanguageUri + currentPageUri).getProperties().get("sling:alias").toString() : "");
        
                        if (alias.equals("homepage") || (currentPageUri.equals("homepage"))) {
                            if (alias!=""){
                                ad.href="https://" + domain + "/" + language + "_" + market;
                            }else{
                                ad.href="https://" + domain + "/" + language + "_" + market;
                            }
                        }else{
                            ad.href="https://" + domain + "/" + language + "_" + market + "/" + completeAlias + AlitaliaConstants.HTML_EXT;
                                /*if (alias!=""){
                                    ad.href="https://" + domain + "/" + language + "_" + market + "/" + baseCurrentPageUri + alias + AlitaliaConstants.HTML_EXT;
                                }else{
                                    ad.href="https://" + domain + "/" + language + "_" + market + "/" + currentPageUri + AlitaliaConstants.HTML_EXT;
                                }*/
                        }
        
                        ad.hreflang=language + "_" + market;
                        metatag.Alternate.add(ad);
        
                    }
                    
                    languageIdx++;
                }
            }
        }
    }
    
    public String computeCompleteAlias(PageManager pageManager,String baseLanguageUri, String currentPageUri) {
	    String res="";
     
	    Page currentPage=pageManager.getPage(baseLanguageUri + currentPageUri);
        String[] path = currentPageUri.split("/");
        Integer length=path.length;
        
        for  (int i=path.length-1; i>=0; i--) {
            path[i]=(currentPage.getProperties().get("sling:alias")!=null ? currentPage.getProperties().get("sling:alias").toString() : path[i]);
            currentPage=currentPage.getParent();
        }
    
        for (int i = 0; i < path.length; i++) {
            res += path[i] + "/";
        }
        res=res.substring(0,res.length()-1);
        return res;
    }
    
    
    
    
	/**
	 * 
	 * @return
	 */
	public String getCountryObject() {
		try{
            Page ancestor = currentPage.getAbsoluteParent(AlitaliaConstants.SITE_ROOT_LEVEL);
            setCountryObject(ancestor);
            if (ancestor != null) {
             
                String javascriptObject =  "var countries = " + countryObject.toJson() + ";\r\n" + "var urls = " + urls.toJson() + ";";
                javascriptObject=javascriptObject.replace("'","\"");
                return javascriptObject;
            } else {
                return "var countries = null; \n var urls=null ";
            }
		}catch(Exception e){
			logger.debug("Exception"+e.getMessage());
			return ("Error in CountryListModel");
		}
	}

	/*
	 * 
	 */
	private void setCountryObject(Page rootPage) {

		countryObject = new CountryObject();
		urls = new UrlCollection();
		ResourceResolver resourceResolver = slingHttpServletRequest.getResourceResolver();

		Iterator<Page> countries = null;
		if (rootPage != null) {
			countries = rootPage.listChildren(new PageFilter(slingHttpServletRequest));
		}

		int countryIdx = 0;
		if (countries != null) {
			while (countries.hasNext()) {

				Page countryPage = countries.next();
				String countryName = i18n.get(countryPage.getTitle());
				logger.trace("countryPage.getTitle(): " + countryPage.getTitle());
				logger.trace("countryName: " + countryName);

				String countryLink = resourceResolver.map(countryPage.getPath() + AlitaliaConstants.HTML_EXT);
				boolean countryIsSelected = countryLink.startsWith(countryRoot);

				String countryCode = AlitaliaUtils.getRepositoryPathMarket(
						countryPage.getContentResource());
				Country country = new Country(countryName, countryLink,
						countryCode, countryIsSelected);

				if (countryIsSelected) {
					countryObject.setCountrySelected(countryIdx);
				}

				// Iterate through all the languages for the current country
				Iterator<Page> languages = countryPage.listChildren(
						new PageFilter(slingHttpServletRequest));
				int languageIdx = 0;
				while (languages.hasNext()) {

					Page languagePage = languages.next();
					String languageName = i18n.get(languagePage.getTitle().toLowerCase());

					String link = AlitaliaUtils.findSiteBaseExternalUrl(
							languagePage.adaptTo(Resource.class), true)
							+ alitaliaConfigurationHolder.getIndexPage();
					
					link = languagePage.getProperties().get("landingpage", link);
					logger.trace("link-landingpage "+link);
					String languageLink = resourceResolver.map(link);
					logger.trace("link-after map"+languageLink);
					String actualLangCode = 
							AlitaliaUtils.getRepositoryPathLanguage(
									languagePage.adaptTo(Resource.class));
					
					boolean languageIsSelected =
							languageLink.startsWith(languageRoot);
					String countryLangCode = actualLangCode + "_" + countryCode;
					UrlObject url = new UrlObject(countryLangCode, languageLink);
					Language language = new Language(languageName, languageLink, languageIsSelected, actualLangCode);

					if (languageIsSelected) {
						logger.trace("IdLanguadeSelect "+languageIdx);
						countryObject.setLanguageSelected(languageIdx);
					}

					urls.addUrl(url);
					country.addLanguage(language);
					languageIdx++;
				}

				countryObject.addCountry(country);
				countryIdx++;
			}
		}
	}

}
