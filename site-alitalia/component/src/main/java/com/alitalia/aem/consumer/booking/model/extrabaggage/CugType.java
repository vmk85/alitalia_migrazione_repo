package com.alitalia.aem.consumer.booking.model.extrabaggage;

/** 
 Enumerazione sul CUG.
 MIL = MILITARY; YTH = YOUTH; ADT = ADULT; FAM = FAMILY
*/
public enum CugType
{
	MIL,
	YTH,
	ADT,
	FAM;

	public static final int SIZE = java.lang.Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static CugType forValue(int value)
	{
		return values()[value];
	}
}