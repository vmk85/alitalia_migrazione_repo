package com.alitalia.aem.consumer.booking.render;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.day.cq.i18n.I18n;

public class RegoleTariffarieTrattaRender {
	
	private Map<String,String> fareRulesFirstColumn;
	private Map<String,String> fareRulesSecondColumn;
	private Map<String,String> fareRulesSecondColumnAsterisk;
	private String brandName;
	private String placeHolderStivaLight;
	private boolean isAmericanCountry;
	private boolean onlyHandBaggage;
	
	public RegoleTariffarieTrattaRender(Map<String, String> totalareRules, 
			boolean isTariffaLight, I18n i18n, BookingSessionContext ctx, FlightSelection selectedFlight, String brandCode) {
		fareRulesFirstColumn = new HashMap<String,String>();
		fareRulesSecondColumn = new HashMap<String,String>();
		fareRulesSecondColumnAsterisk = new HashMap<String,String>();
		onlyHandBaggage = selectedFlight.isOnlyHandBaggage();
		init(totalareRules,isTariffaLight,i18n,ctx,selectedFlight,brandCode);
	}
	
	private void init(Map<String, String> totalareRules,
			boolean isTariffaLight, I18n i18n, BookingSessionContext ctx, FlightSelection selectedFlight, String brandCode) {

		placeHolderStivaLight = "PLACEHOLDER_STIVA_LIGHT";
		isAmericanCountry = (ctx.market.equalsIgnoreCase("us") || ctx.market.equalsIgnoreCase("ca"));
		setFareRulesInTwoColumns(totalareRules,isTariffaLight,selectedFlight.isBus(),i18n);
		BrandPageData page = ctx.codeBrandMap.get(brandCode.toLowerCase());
		if (page != null) {
			brandName = page.getTitle();
		} else {
			brandName = "";
		}
		
	}

	public Map<String, String> getFareRulesFirstColumn() {
		return fareRulesFirstColumn;
	}
	
	public Map<String, String> getFareRulesSecondColumn() {
		return fareRulesSecondColumn;
	}
	
	public Map<String, String> getFareRulesSecondColumnAsterisk() {
		return fareRulesSecondColumnAsterisk;
	}

	public void setFareRulesSecondColumnAsterisk(
			Map<String, String> fareRulesSecondColumnAsterisk) {
		this.fareRulesSecondColumnAsterisk = fareRulesSecondColumnAsterisk;
	}

	public String getPlaceHolderStivaLight(){
		return placeHolderStivaLight;
	}
	
	public String getBrandName(){
		return brandName;
	}

	public boolean isOnlyHandBaggage() {
		return onlyHandBaggage;
	}

	public void setOnlyHandBaggage(boolean onlyHandBaggage) {
		this.onlyHandBaggage = onlyHandBaggage;
	}

	/*Private method*/
	private void setFareRulesInTwoColumns(Map<String, String> fareRules, boolean isTariffaLight, boolean busIsSelected, I18n i18n) {
		if(fareRules==null || fareRules.isEmpty()){
			return;
		}
//		ArrayList <String> policiesWithAsterisk = new ArrayList <String>();
//		policiesWithAsterisk.add("booking.regoleTariffarie.rimborsoPrenotazioneDopoPartenza");
//		policiesWithAsterisk.add("booking.regoleTariffarie.rimborsoPrenotazionePrimaPartenza");
//		policiesWithAsterisk.add("booking.regoleTariffarie.cambioPrenotazioneDopoPartenza");
//		policiesWithAsterisk.add("booking.regoleTariffarie.cambioPrenotazionePrimaPartenza");
		
		int len = fareRules.size();
		int i=0;
		Set<Entry<String,String>> entries =  fareRules.entrySet();
		Iterator<Entry<String,String>> it = entries.iterator();
		while(i<len/2){
			Entry<String,String> entry = it.next();
			if (isTariffaLight && this.onlyHandBaggage) {
				applyRulesForTariffaLight(entry);
			}
			String value = entry.getValue();
			if(value.equals("true")){
				value = "V";
			}
			if(value.equals("false")){
				value = "-";
			}
			this.fareRulesFirstColumn.put(entry.getKey(), value);
			i++;
		}
		if (isTariffaLight || busIsSelected) {
			this.fareRulesFirstColumn.put(
					i18n.get("booking.regoleTariffarie.light.checkinOnline.label"),
					i18n.get("booking.regoleTariffarie.light.checkinOnline.value"));
		}
		while(i<len){
			Entry<String,String> entry = it.next();
			String value = entry.getValue();
			if(value.contains("#")){
				value = value.split("#")[0] + "**";
			}
			if(value.contains("/")){
				value = value.split("/")[0]+ "*";
			}
			if (isTariffaLight && this.onlyHandBaggage) {
				applyRulesForTariffaLight(entry);
			}
			if(value.equals("true")){
				value = "V";
			}
			if(value.equals("false")){
				value = "-";
			}
			String key = entry.getKey();
			this.fareRulesSecondColumn.put(key, value);
//			if (isAmericanCountry && policiesWithAsterisk.contains(key)) {
//				fareRulesSecondColumnAsterisk.put(key, "*");
//			}
//			else {
				fareRulesSecondColumnAsterisk.put(key, "");
//			}
			i++;
		}
	}

	private void applyRulesForTariffaLight(
			Entry<String, String> entry) {
		
		if (entry.getKey().equals("booking.regoleTariffarie.bagaglioInStiva")) {
			entry.setValue(placeHolderStivaLight);
		}
	}
	
}
