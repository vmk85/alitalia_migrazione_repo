package com.alitalia.aem.consumer.checkinrest.servlet;

import com.alitalia.aem.common.messages.checkinrest.CheckinSetSmsDataResponse;
import com.alitalia.aem.common.messages.home.CheckinSendSmsRequest;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.day.cq.i18n.I18n;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = { "checkisendmoreboardyngpassbyphone"}),
        @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
        @Property(name = "sling.servlet.extensions", value = { "json" }),
        @Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinSendMoreBoardingPassByPhoneServlet extends GenericCheckinFormValidatorServlet {

    // logger
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    private AlitaliaConfigurationHolder configuration;


    @Reference
    private CheckinSession checkinSession;


    public CheckinSendMoreBoardingPassByPhoneServlet() {
    }

    @Override
    protected AlitaliaConfigurationHolder getConfiguration() {
        return configuration;
    }

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        ResultValidation resultValidation = new ResultValidation();
        resultValidation.setResult(true);
        return resultValidation;
    }


    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

        logger.info("[CheckinSendMoreBoardingPassByPhoneServlet - performSubmit]");


        try{

            CheckinSessionContext ctx = getCheckInSessionContext(request);

            logger.info("[CheckinSendMoreBoardingPassByPhoneServlet] [INIZIO] ");

            CheckinSetSmsDataResponse result_setSms = checkinSession.setMoreSmsData(request, ctx);

            String key = result_setSms.getKey();


            String protocol = "http://";
            if(configuration.getHttpsEnabled()){
                protocol = "https://";
            }

            String domain = configuration.getExternalDomain();

            String successPage = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false) + getConfiguration().getWebcheckinPage()  + "?key="+key;

            String redirectUrl = protocol + domain + successPage;

            final I18n i18n = new I18n(request.getResourceBundle(
                    AlitaliaUtils.findResourceLocale(request.getResource())));

            String message = i18n.get("checkin.sms.text");

            String[] _phones = request.getParameter("tel").split(",");
            for(String _phone:_phones)
            {
                CheckinSendSmsRequest requestSms = new CheckinSendSmsRequest();
                requestSms.setFromPhone(getConfiguration().getSendsmsFromPhoneNumber());
                requestSms.setApplicationName(getConfiguration().getSendsmsApplicationName());
                requestSms.setNotificationSend(getConfiguration().getSendsmsNotificationSend());
                requestSms.setNotificationLocalSend(getConfiguration().getSendsmsNotificationLocalSend());
                requestSms.setNotificationReceive(getConfiguration().getSendsmsNotificationReceive());
                requestSms.setNotificationNotReceive(getConfiguration().getSendsmsNotificationNotReceive());

                requestSms.setPhoneNumber(_phone);
                requestSms.setLink(message + " " + redirectUrl);
                checkinSession.sendSms(requestSms);
            }

            logger.info("[CheckinSendMoreBoardingPassByPhoneServlet - linkSendSms => "+redirectUrl+"  ]");


        }catch(Exception e){
            throw new RuntimeException("[CheckinSendMoreBoardingPassByPhoneServlet] - Errore durante l'invocazione del servizio.", e);
        }

    }
}
