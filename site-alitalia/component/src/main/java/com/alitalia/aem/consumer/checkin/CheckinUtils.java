package com.alitalia.aem.consumer.checkin;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.SlingRequestProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryCartData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryEticketData;
import com.alitalia.aem.common.data.home.checkin.CheckinAncillaryPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinCouponData;
import com.alitalia.aem.common.data.home.checkin.CheckinFlightData;
import com.alitalia.aem.common.data.home.checkin.CheckinPassengerData;
import com.alitalia.aem.common.data.home.checkin.CheckinRouteData;
import com.alitalia.aem.common.data.home.enumerations.CheckinPassengerStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAncillaryTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbRouteStatusEnum;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryData;
import com.alitalia.aem.common.utils.AlitaliaCommonUtils;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.WCMMode;

public class CheckinUtils {
	
	protected static Logger logger =
			LoggerFactory.getLogger(CheckinUtils.class);
	
	/**
	 * Is CheckedIn
	 * @param status
	 * @return
	 */
	public static boolean isCheckedIn(CheckinPassengerStatusEnum status) {
		return CheckinPassengerStatusEnum.ALREADY_CHECKED_IN.equals(status) || 
				CheckinPassengerStatusEnum.CHECKED_IN.equals(status);
	}
	
	/**
	 * Get CheckedIn Passengers
	 * @param allPassengers
	 * @return
	 */
	public static List<CheckinPassengerData> getCheckedInPassengers(
			List<CheckinPassengerData> allPassengers) {
		List<CheckinPassengerData> passengers =
			new ArrayList<CheckinPassengerData>();
		for (CheckinPassengerData data : allPassengers) {
			if (CheckinUtils.isCheckedIn(data.getStatus())) {
				passengers.add(data);
			}
		}
		return passengers;
	}
	
	/**
	 * Get Not CheckedIn Passengers
	 * @param allPassengers
	 * @return
	 */
	public static List<CheckinPassengerData> getNotCheckedInPassengers(
			List<CheckinPassengerData> allPassengers) {
		List<CheckinPassengerData> passengers =
				new ArrayList<CheckinPassengerData>();
		for (CheckinPassengerData data : allPassengers) {
			if (!CheckinUtils.isCheckedIn(data.getStatus())) {
				passengers.add(data);
			}
		}
		return passengers;
	}
	
	/**
	 * Retrieve coupon by flight number
	 * @param coupons
	 * @param flightNumber
	 * @return
	 */
	public static CheckinCouponData getCouponByFlightNumber(
			List<CheckinCouponData> coupons, String flightNumber) {
		
		for (CheckinCouponData coupon : coupons) {
			if (coupon.getFlight() != null && 
					flightNumber.equals(coupon.getFlight().getFlightNumber())) {
				
				return coupon;
			}
		}
		return null;
	}
	
	
	/**
	 * Verify Check-in Status
	 * @param routeData
	 * @param flight_number
	 * @return
	 */
	public static CheckinStatus verifyCheckinStatus(CheckinRouteData routeData,
			int flight_number, String passengersUrl, CheckinSessionContext ctx) {
		logger.debug("[CheckinUtils] verifyCheckinStatus");
		
		CheckinStatus checkinStatus;
		
		// verify check-in
		if (null != (checkinStatus = checkPartner(routeData, flight_number,
					ctx)) ||
				null != (checkinStatus = checkPreviousFlight(routeData)) ||
				null != (checkinStatus = checkValidCheckin(routeData,
						flight_number, passengersUrl)) ||
				null != (checkinStatus = checkNotAvailableYet(routeData)) ||
				null != (checkinStatus = checkAlreadyCheckedIn(routeData))) {
			return checkinStatus;
		}

		// CASO GENERICO
		else {
			checkinStatus = new CheckinStatus(
					"#",
					"checkin.flightslist.url.forbidden.label");
		}
		
		return checkinStatus;
		
	}
	
	/**
	 * Verify CHECK-IN => external URL
	 * @param routeData
	 * @return
	 */
	public static CheckinStatus checkPartner(CheckinRouteData routeData,
			int flight_number, CheckinSessionContext ctx) {
		logger.debug("[CheckinUtils] checkPartner");
		
		CheckinStatus checkinStatus = null;
		if (null != routeData.getDeepLink() &&
				"AZ" != routeData.getDeepLink().getCode() &&
				routeData.getCheckinEnabled()) {
			
			String url = createPartnerUrl(routeData, flight_number, ctx);
			
			checkinStatus = new CheckinStatus(
					url,
					"checkin.flightslist.url.partner.label",
					"_blank");
		}
		return checkinStatus;
		
	}
	
	/*
	 * Create Partner URL
	 */
	private static String createPartnerUrl(CheckinRouteData routeData,
			int flight_number, CheckinSessionContext ctx) {

		if (routeData.getFlights().isEmpty()) {
			return "";
		}
		
		// set URL
		String url = routeData.getDeepLink().getUrl();
		
		CheckinFlightData flightData = routeData.getFlights().get(0);
		
		// set ticket number
		String ticketnumber = flightData.getEticket();
		
		// set name & surname
		String name = ctx.passengerName;
		String surname = ctx.passengerSurname;
		
		// set language
		String language = AlitaliaUtils.getLanguage(ctx.locale.toLanguageTag()).toUpperCase();
		if ("1A".equals(routeData.getDeepLink().getCode())) {
			language = language.toLowerCase();
		}
		if ("ED".equals(routeData.getDeepLink().getCode())) {
			String[] _s = { "it", "de" };
			String[] _r= { "4", "2" };
			language = StringUtils.replaceEach(language, _s, _r);
			try {
				Integer.parseInt(language);
			} catch (NumberFormatException e) {
				language = "1";
			}
		}
		
		// set market
		String market = ctx.market.toUpperCase();
		if ("AF".equals(routeData.getDeepLink().getCode())
				|| "KL".equals(routeData.getDeepLink().getCode())
				&& "EN".equals(market)) {
			market = "GB";
		}
		
		// set code
		String code = flightData.getOperatingCarrier();
		if ("1A".equals(routeData.getDeepLink().getCode())) {
			code = "AZ";
		}
		
		// set flight number
		String flightnumber = flightData.getOperatingFlightNumber();

		// set IATA
		String iata = flightData.getFrom().getCode();
		
		// set number of days
		Date today = new GregorianCalendar().getTime();
		Date departureDate = flightData.getDepartureDateTime().getTime();
		String days = "" + ((departureDate.getTime() - today.getTime())
				/ (1000 * 60 * 60 * 24));
		
		// set first 3 of ticket number
		String first3ofticketnumber = ticketnumber.substring(0, 3);
		
		// set last of ticket number
		String lastofticketnumber = ticketnumber.substring(3);
		
		// set departure date (yyyyMMdd)
		Calendar calendar = flightData.getDepartureDateTime();
		DateRender dateRender = new DateRender(calendar);
		String departuredate = dateRender.getYear()
				+ dateRender.getMonth() + dateRender.getDay();
		
		String departureDateWithDash = dateRender.getDay() + "-" + dateRender.getMonth() + "-" + dateRender.getYear();
		
		// set PNR
		String pnr = routeData.getPnr();
		
		String[] searchList = {
			"{0}", "{1}", "{2}", "{3}", "{4}",
			"{5}", "{6}", "{7}", "{8}", "{9}",
			"{10}", "{11}", "{12}","{13}"
		};
		String[] replacementList = {
			ticketnumber, name, surname, language, market,
			code, flightnumber, iata, days, first3ofticketnumber,
			lastofticketnumber, departuredate, pnr, departureDateWithDash
		};
		
		return StringUtils.replaceEach(url, searchList, replacementList);
		
	}
	
	/**
	 * Verify CHECK-IN => previous flight needed
	 * @param routeData
	 * @return
	 */
	public static CheckinStatus checkPreviousFlight(
			CheckinRouteData routeData) {
		logger.debug("[CheckinUtils] checkPreviousFlight");
		
		CheckinStatus checkinStatus = null;
		if ((null != routeData.getDeepLink() &&
				"AZ" != routeData.getDeepLink().getCode() &&
				!routeData.getCheckinEnabled()) ||
				(MmbRouteStatusEnum.REGULAR.equals(routeData.getStatus()) &&
						!routeData.getCheckinEnabled())) {
			checkinStatus = new CheckinStatus(
					"#",
					"checkin.flightslist.url.previous.label");
		}
		return checkinStatus;
		
	}
	
	/**
	 * Verify CHECK-IN => valid checkin
	 * @param routeData
	 * @param flight_number
	 * @param passengersUrl
	 * @return
	 */
	public static CheckinStatus checkValidCheckin(CheckinRouteData routeData,
			int flight_number, String passengersUrl) {
		logger.debug("[CheckinUtils] checkValidCheckin");
		
		CheckinStatus checkinStatus = null;
		if (MmbRouteStatusEnum.REGULAR.equals(routeData.getStatus()) &&
				routeData.getCheckinEnabled()) {
			checkinStatus = new CheckinStatus(passengersUrl + "?"
							+ CheckinConstants.CHECKIN_SELECTED_ROUTE_PARAM
							+ "=" + flight_number,
					"checkin.flightslist.url.checkin.label");
		}
		return checkinStatus;

	}
	
	/**
	 * Verify CHECK-IN => not available yet
	 * @param routeData
	 * @return
	 */
	public static CheckinStatus checkNotAvailableYet(CheckinRouteData routeData) {
		logger.debug("[CheckinUtils] checkNotAvailableYet");
		
		CheckinStatus checkinStatus = null;
		if (MmbRouteStatusEnum.AVAILABLE.equals(routeData.getStatus()) ||
				MmbRouteStatusEnum.SPECIAL_TIME_RESTRICTION.equals(
						routeData.getStatus())) {
			checkinStatus = new CheckinStatus(
					"#",
					"checkin.flightslist.url.notavailable.label");
		}
		return checkinStatus;
		
	}
	
	/**
	 * Verify CHECK-IN => already checked in
	 * @param routeData
	 * @return
	 */
	public static CheckinStatus checkAlreadyCheckedIn(
			CheckinRouteData routeData) {
		logger.debug("[CheckinUtils] checkAlreadyCheckedIn");
		
		CheckinStatus checkinStatus = null;
		if (MmbRouteStatusEnum.ALREADY_CHECKED_IN.equals(
					routeData.getStatus())) {
			checkinStatus = new CheckinStatus(
					"0",
					"checkin.flightslist.url.manage.label");
		}
		return checkinStatus;
		
	}
	
	/**
	 * 
	 * @param routeData
	 * @return
	 */
	public static List<CheckinFlightData> getFlightsList (
			CheckinRouteData routeData) {
		return AlitaliaCommonUtils.getFlightsList(routeData);
	}
	
	public static boolean isInterline(CheckinRouteData routeData) {
		return routeData.getInterlineFlights() != null
				&& routeData.getInterlineFlights().size() > 0;
	}
	
	/**
	 * 
	 * @param resource
	 * @param templateMail
	 * @param secure
	 * @return
	 */
	public static String getMailBody(Resource resource, String templateMail,
			boolean secure, ResourceResolverFactory resolverFactory,
			RequestResponseFactory requestResponseFactory,
			SlingRequestProcessor requestProcessor) {
		
		String result = null;
		String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(resource, false);
		try {

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			/* Get resource resolver */
			@SuppressWarnings("deprecation")
			ResourceResolver resourceResolver =
				resolverFactory.getAdministrativeResourceResolver(null);
			String requestedUrl = baseUrl + AlitaliaConstants.BASE_CONFIG_PATH
					+ "/" + CheckinConstants.CHECKIN_TEMPLATE_EMAIL_FOLDER
					+ "/" + templateMail;

			/* Setup request */
			HttpServletRequest req =
					requestResponseFactory.createRequest("GET", requestedUrl);
			WCMMode.DISABLED.toRequest(req);

			out = new ByteArrayOutputStream();
			HttpServletResponse resp =
					requestResponseFactory.createResponse(out);

			/* Process request through Sling */
			requestProcessor.processRequest(req, resp, resourceResolver);

			String encoding = "UTF-8";

			result = new String(out.toByteArray(), Charset.forName(encoding));

		} catch (LoginException | ServletException | IOException e) {
			logger.error("Generic exception while retrieving email template "
					+ "from JCR repository: {}", e);
			result = "";
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("Template retrieved and APIS/ESTA placeholders "
					+ "replaced: {} ", result);
		}
		
		return result;
		
	}

	/**
	 * Converte la classe di viaggio
	 * @param seatClass
	 * @return
	 */
	public static String getCompartimentalClass(
			MmbCompartimentalClassEnum seatClass, CheckinFlightData flight) {
		MmbCompartimentalClassEnum businessClass = null;
		MmbCompartimentalClassEnum premiumEcoClass = null;
		MmbLegTypeEnum legType = flight.getLegType();
		List<MmbCompartimentalClassEnum> cabins = flight.getCabins();
		if(MmbLegTypeEnum.INC.equals(legType) && cabins.contains(MmbCompartimentalClassEnum.F)){
			/*Volo intercontinentale a 3 classi*/
			businessClass = MmbCompartimentalClassEnum.F;
			premiumEcoClass = MmbCompartimentalClassEnum.C;
		}
		else{
			businessClass = MmbCompartimentalClassEnum.C;
		}
		if(MmbCompartimentalClassEnum.Y.equals(seatClass)){
			return "Economy";
			
		}
		else if(businessClass.equals(seatClass)){
			return "Business";

		}
		else if(premiumEcoClass != null && premiumEcoClass.equals(seatClass)){
			return "Premium Economy";

		}
		else{
			return "";
		}
				
//			    J("J"),
//			    EP("EP"),
//			    F("F");
				
	}

	/**
	 * Find the passeger for the ancillary, based on coupon number comparison.
	 * 
	 * @param ancillary The ancillary to verify.
	 * @param allAncillaryPassengers The list of all ancillary passengers to check.
	 * @return The passenger, or null when not found.
	 */
	public static CheckinAncillaryPassengerData findAncillaryPassenger(MmbAncillaryData ancillary, 
			List<CheckinAncillaryPassengerData> allAncillaryPassengers) {
		for (String couponNumber : ancillary.getCouponNumbers()) {
			for (CheckinAncillaryPassengerData currentPassenger : allAncillaryPassengers) {
				for (CheckinAncillaryEticketData eticket : currentPassenger.getEtickets()) {
					for (CheckinAncillaryCouponData coupon : eticket.getCoupons()) {
						if (couponNumber.equals(coupon.getNumber())) {
							return currentPassenger;
						}
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * isMultileg
	 * @param selectedRoute
	 * @return
	 */
	public static boolean isMultileg(CheckinRouteData selectedRoute) {
		//TODO capire come fare
		return false;
	}

	/**
	 * isOutbound
	 * @param selectedRoute
	 * @return
	 */
	public static boolean isOutbound(CheckinRouteData selectedRoute) {
		return (selectedRoute.getIsOutwardRoute() != null) ? selectedRoute.getIsOutwardRoute() : false;
	}
	
	/**
	 * getAncillaryFromCartByType
	 * @param cart
	 * @param type
	 * @return
	 */
	public static List<MmbAncillaryData> getAncillaryFromCartByType(
			CheckinAncillaryCartData cart, MmbAncillaryTypeEnum type) {
		return cart.getAncillaries().stream().filter( anc 
				-> anc.getAncillaryType().equals(type))
				.collect(Collectors.toList());
	}
	
	/**
	 * getAncillaryFromCartByStatus
	 * @param cart
	 * @param type
	 * @return
	 */
	public static List<MmbAncillaryData> getAncillaryFromCartByStatus(
			CheckinAncillaryCartData cart, List<MmbAncillaryStatusEnum> statuses) {
		return cart.getAncillaries().stream().filter( anc 
				-> statuses.contains(anc.getAncillaryStatus()))
				.collect(Collectors.toList());
	}
	
	/**
	 * Obtain the array of messages to show when a flight in selected route
	 * is a bus form bus station to bus station 
	 * @param ctx
	 * @param i18n
	 * @return
	 */
	public static String[] getMessageFromBusToBus(CheckinSessionContext ctx, I18n i18n) {
		List<CheckinFlightData> flightsFromToBus = obtainFligthFromBusStationToBusStation(ctx);
		String[] messageFromToStation = new String[flightsFromToBus.size()];
		int i = 0;
		for (CheckinFlightData fligth : flightsFromToBus) {
			String stationOrigin = i18n.get("airportsData." + fligth.getFrom().getCode() + ".name");
			String stationDest = i18n.get("airportsData." + fligth.getTo().getCode() + ".name");
			messageFromToStation[i] = i18n.get("checkin.bus.message.fromToStation" ,"", stationOrigin, stationDest);
			i++;
		}
		return messageFromToStation;
	}

	/**
	 * Obtain the array of messages to show when a flight in selected route
	 * is a bus form airport to bus station 
	 * @param ctx
	 * @param i18n
	 * @return
	 */
	public static String[] getMessageFromAirportToBus(
			CheckinSessionContext ctx, I18n i18n) {
		List<CheckinFlightData> flightsToBus = obtainFligthFromAirportToBusStation(ctx);
		String[] messageToStation = new String[flightsToBus.size()];
		int i = 0;
		for (CheckinFlightData fligth : flightsToBus) {
			String station = i18n.get("airportsData." + fligth.getTo().getCode() + ".name");
			messageToStation[i] = i18n.get("checkin.bus.message.toStation" ,"", fligth.getFrom().getCode(), station);
			i++;
		}
		return messageToStation;
	}
	
	/**
	 * Obtain the array of messages to show when a flight in selected route
	 * is a bus form bus station to airport 
	 * @param ctx
	 * @param i18n
	 * @return
	 */
	public static String[] getMessageFromBusToAirport(
			CheckinSessionContext ctx, I18n i18n) {
		List<CheckinFlightData> flightsFromBus = obtainFligthFromBusStationToAirport(ctx);
		String[] messageFromStation = new String[flightsFromBus.size()];
		int i = 0;
		for (CheckinFlightData fligth : flightsFromBus) {
			messageFromStation[i] = i18n.get("checkin.bus.message.fromStation" ,"", fligth.getTo().getCode());
			i++;
		}
		return messageFromStation;
	}
	
	/**
	 * Return the list of fligths operated by bus
	 * @param selectedRoute
	 * @return
	 */
	public static List<CheckinFlightData> obtainBusFlight(CheckinRouteData selectedRoute) {
		List<CheckinFlightData> busFlight = new ArrayList<CheckinFlightData>();
		for (CheckinFlightData flight : getFlightsList(selectedRoute)) {
			if (flight.isBus()) {
				busFlight.add(flight);
			}
		}
		return busFlight;
	}
	
	private static List<CheckinFlightData> obtainFligthFromBusStationToAirport(CheckinSessionContext ctx) {
		List<CheckinFlightData> fligthsFromBusStationToAirport = new ArrayList<CheckinFlightData>();
		for (CheckinFlightData fligthInRoute : getFlightsList(ctx.selectedRoute)) {
			if (fligthInRoute.isFromBusStation()) {
				fligthsFromBusStationToAirport.add(fligthInRoute);
			}
		}
		return fligthsFromBusStationToAirport;
	}
	
	private static List<CheckinFlightData> obtainFligthFromAirportToBusStation(CheckinSessionContext ctx) {
		List<CheckinFlightData> fligthsToBusStationToAirport = new ArrayList<CheckinFlightData>();
		for (CheckinFlightData fligthInRoute : getFlightsList(ctx.selectedRoute)) {
			if (fligthInRoute.isToBusStation()) {
				fligthsToBusStationToAirport.add(fligthInRoute);
			}
		}
		return fligthsToBusStationToAirport;
	}
	
	private static List<CheckinFlightData> obtainFligthFromBusStationToBusStation(CheckinSessionContext ctx) {
		List<CheckinFlightData> fligthsFromToBusStationToAirport = new ArrayList<CheckinFlightData>();
		for (CheckinFlightData fligthInRoute : getFlightsList(ctx.selectedRoute)) {
			if (fligthInRoute.isToBusStation() && fligthInRoute.isFromBusStation()) {
				fligthsFromToBusStationToAirport.add(fligthInRoute);
			}
		}
		return fligthsFromToBusStationToAirport;
	}

}