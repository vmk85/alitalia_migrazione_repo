package com.alitalia.aem.consumer.checkin.analytics;

import java.math.BigDecimal;

public class EMDInfo {
	public String ancName;
	public Integer ancQuantity;
	public BigDecimal ancRevenue;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ancName == null) ? 0 : ancName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EMDInfo other = (EMDInfo) obj;
		if (ancName == null) {
			if (other.ancName != null)
				return false;
		} else if (!ancName.equals(other.ancName))
			return false;
		return true;
	}
	
}
