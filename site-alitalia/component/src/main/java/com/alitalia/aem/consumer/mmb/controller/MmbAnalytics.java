package com.alitalia.aem.consumer.mmb.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.mmb.analytics.MmbAnalyticsInfo;
import com.alitalia.aem.consumer.mmb.analytics.MmbAnalyticsInfoFiller;
import com.alitalia.aem.consumer.mmb.analytics.MmbAnalyticsInfoToJsObj;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbAnalytics extends MmbSessionGenericController {
	
	@Inject
	AlitaliaConfigurationHolder configurationHolder;
	
	@Self
	private SlingHttpServletRequest request;
	
	private int step;
	
	private String dlVar = "";
	
	@PostConstruct
	protected void initModel() {
		try {
			initBaseModel(request);
			String path = request.getRequestURL().toString();
			if (request.getRequestPathInfo().getSelectorString() != null &&
					!request.getRequestPathInfo().getSelectorString().isEmpty()) {
				path = request.getRequestURL().toString().replace(
						request.getRequestPathInfo().getSelectorString() + ".", "");
			}
			step = computeMmbStep(path);
			
			/* dataLayer Enhancement*/
			MmbAnalyticsInfoFiller filler = new MmbAnalyticsInfoFiller();
			MmbAnalyticsInfoToJsObj toJsObj = new MmbAnalyticsInfoToJsObj();
			MmbAnalyticsInfo info = filler.fillInfo(ctx, step);
			dlVar = toJsObj.toJSObj(info, false);
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			dlVar = "{}";
		}
	}
	
	
	
	private int computeMmbStep(String path){
		int step = 0;
		
		/*Booking Confirmation*/
		if(path.endsWith(configurationHolder.getBookingConfirmationPage())){
			step = 1;
		}
		/*Ancillary*/
		if(path.endsWith(configurationHolder.getMmbDetailPage())){
			step = 2;
		}
		/*Ancillary Payment*/
		else if(path.endsWith(configurationHolder.getMmbPaymentPage())){
			step = 3;
		}
		/*Ancillary Confirmation*/
		else if(path.endsWith(configurationHolder.getMmbThankyouPage())){
			step = 4;
		}
		return step;
	}
	

	public int getStep() {
		return step;
	}


	public String getDlVar() {
		return dlVar;
	}

}
