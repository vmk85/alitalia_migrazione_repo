package com.alitalia.aem.consumer.servlet.login;

import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.google.gson.Gson;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;

/**
 * .
 * 
 *
 *
 *
 */

@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "savemauser" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.extensions", value = { "json" })
})
@SuppressWarnings("serial")
public class SaveMAUserServlet extends SlingAllMethodsServlet {
	

	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * Reference to the service to remote consumer login service.
	 */
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_UNARY)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;
	
	private ComponentContext componentContext;
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		try {
			logger.debug("salvataggio utente MyAlitalia in sessione.");

			String userAsJsonString = request.getParameter("userMAAsJsonString");
			logger.debug("userMAAsJsonString = " + userAsJsonString);
			logger.debug("type = " + request.getParameter("type"));
			logger.debug("user = " + request.getParameter("user"));
			if (request.getParameter("type") != null && request.getParameter("type").equals("logout")) {
				String user = request.getParameter("user");
				if (user.equalsIgnoreCase("MA")) {
					request.getSession().setAttribute(MACustomer.NAME, null);
					response.setContentType("text/plain");
					response.getWriter().append("ok - logout effettuato");
					logger.debug("logout effettuato");
				} else if (user.equalsIgnoreCase("MM")) {
					request.getSession().setAttribute(MMCustomerProfileData.NAME, null);
					response.setContentType("text/plain");
					response.getWriter().append("ok - logout effettuato");
					logger.debug("logout effettuato");
					request.getSession().removeAttribute(MMCustomerProfileData.NAME);
				} else {
					response.setContentType("text/plain");
					response.getWriter().append("fail - parameter 'user' missing");
					logger.debug("Error - parameter 'user' missing");
				}
				request.getSession().removeAttribute("CUSTOMER");
			} else {
				if (userAsJsonString != null && !"".equals(userAsJsonString)) {
					Gson gson = new Gson();
					MACustomer userMA = gson.fromJson(userAsJsonString, MACustomer.class);
					request.getSession().setAttribute(MACustomer.NAME, userMA);

					response.setContentType("text/plain");
					response.getWriter().append("ok - utente MyAlitalia salvato correttamente");
					logger.debug("ok - utente MyAlitalia salvato correttamente");
				} else {
					response.setContentType("text/plain");
					response.getWriter().append("fail - parameter 'userMAAsJsonString' missing");
					logger.debug("fail - parameter 'userMAAsJsonString' missing");
					request.getSession().removeAttribute(MACustomer.NAME);
				}
			}
			logger.debug("NO EXCEPTION");
		} catch (Exception e) {
			logger.debug("Exception: " + e.toString());
		}

	}
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
}
