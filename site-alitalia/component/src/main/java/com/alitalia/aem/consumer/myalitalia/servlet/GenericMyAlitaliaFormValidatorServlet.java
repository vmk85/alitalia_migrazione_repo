package com.alitalia.aem.consumer.myalitalia.servlet;

import com.alitalia.aem.common.data.home.MACustomer;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaConstants;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSession;
import com.alitalia.aem.consumer.myalitalia.MyAlitaliaSessionContext;
import com.alitalia.aem.consumer.myalitalia.logger.MyAlitaliaServletLog;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.i18n.I18n;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;

public class GenericMyAlitaliaFormValidatorServlet extends GenericFormValidatorServlet {

    protected MyAlitaliaServletLog myAlitaliaServletLog = new MyAlitaliaServletLog();

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        return null;
    }

    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
        long time_before = System.currentTimeMillis();
        String action = request.getParameter("_action");
        if ("validate".equals(action)) {
            performValidationBehavior(request, response);
        } else {
            performSubmitBehavior(request, response);
        }
        response.addCookie(new Cookie("JSESSIONID", request.getSession(true).getId()));
        long time_after = System.currentTimeMillis();
        long execution_time = time_after - time_before;
        if (execution_time > 20000) {
            myAlitaliaServletLog.info("------ ALERT EXECUTION TIME ------ Servlet execution time=" + execution_time);
        } else {
            myAlitaliaServletLog.info("Servlet execution time=" + execution_time);
        }
    }

    @Override
    protected void saveDataIntoSession(SlingHttpServletRequest request, Exception exception) {
        logger.warn("The emtpy saveDataIntoSession method was unexpectedly invoked.");
    }

    protected boolean initSession(SlingHttpServletRequest request, AlitaliaConfigurationHolder configuration, MyAlitaliaSession myAlitaliaSession){

        MACustomer maCustomer  = new MACustomer();

        HttpSession session = request.getSession(true);

        MyAlitaliaSessionContext mactx = null;

        mactx = myAlitaliaSession.initializeSession(request, maCustomer);

        session.setAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE, mactx);

        myAlitaliaServletLog.info("Checkinrest context set in session. JSESSIONID: [" + request.getSession().getId() + "]");

        return true;

    }

    protected MyAlitaliaSessionContext getMyAlitaliaSessionContext(SlingHttpServletRequest request) {

        myAlitaliaServletLog.info("GenericCheckinFormValidatorServlet (getCheckInSessionContext) session accessed: [" + request.getSession().getId() + "]");

        MyAlitaliaSessionContext mactx = (MyAlitaliaSessionContext) request.getSession(true).getAttribute(MyAlitaliaConstants.MYALITALIA_CONTEXT_ATTRIBUTE);
        if (mactx == null) {
            throw new GenericFormValidatorServletException(false, "CheckIn session context not found.");
        } else {
            return mactx;
        }
    }

}
