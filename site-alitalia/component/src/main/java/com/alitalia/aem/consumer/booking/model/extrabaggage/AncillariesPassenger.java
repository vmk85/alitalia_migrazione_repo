package com.alitalia.aem.consumer.booking.model.extrabaggage;

public class AncillariesPassenger
{
	//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//ORIGINAL LINE: [JsonProperty(PropertyName = "@ref")][DataMember(Name = "@ref")] public int _ref {get;set;}
		private int _ref;
	//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//ORIGINAL LINE: [JsonProperty(PropertyName = "@id")][DataMember(Name = "@id")] public int id {get;set;}
		private int id;		
		private int index;
		private String firstName;		
		private String lastName;		
		private AncillaryPassengerType type;
		private String tierLevel;
		private int tierLevelNumber;
		private boolean isMilleMiglia;
		private int freeBaggageAdmitted;
		
		public final int get_ref()
		{
			return _ref;
		}
		
		public final void set_ref(int value)
		{
			_ref = value;
		}

		public final int getid()
		{
			return id;
		}
		
		public final void setid(int value)
		{
			id = value;
		}
			
		public final int getindex()
		{
			return index;
		}
		
		public final void setindex(int value)
		{
			index = value;
		}
		
		public final String getfirstName()
		{
			return firstName;
		}
		
		public final void setfirstName(String value)
		{
			firstName = value;
		}
		
		public final String getlastName()
		{
			return lastName;
		}
		
		public final void setlastName(String value)
		{
			lastName = value;
		}
		
		public final AncillaryPassengerType gettype()
		{
			return type;
		}
		
		public final void settype(AncillaryPassengerType value)
		{
			type = value;
		}
		
		public final String gettierLevel()
		{
			return tierLevel;
		}
		
		public final void settierLevel(String value)
		{
			tierLevel = value;
		}
		
		public final int gettierLevelNumber()
		{
			return tierLevelNumber;
		}
		
		public final void settierLevelNumber(int value)
		{
			tierLevelNumber = value;
		}
		
		public final boolean getIsMilleMiglia()
		{
			return isMilleMiglia;
		}
		
		public final void setIsMilleMiglia(boolean value)
		{
			isMilleMiglia = value;
		}
		
		public final int getFreeBaggageAdmitted()
		{
			return freeBaggageAdmitted;
		}
		
		public final void setFreeBaggageAdmitted(int value)
		{
			freeBaggageAdmitted = value;
		}
}