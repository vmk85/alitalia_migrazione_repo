package com.alitalia.aem.consumer.bookingaward.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingFlightFilterTypeEnum;
import com.alitalia.aem.consumer.booking.BookingSearchCategoryEnum;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.booking.model.BrandPageData;
import com.alitalia.aem.consumer.booking.model.FlightSelection;
import com.alitalia.aem.consumer.booking.render.AvailableFlightsSelectionRender;
import com.alitalia.aem.consumer.booking.render.BrandRender;
import com.alitalia.aem.consumer.bookingaward.BookingAwardSession;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.FlightDataComparator;
import com.alitalia.aem.consumer.utils.ValidationUtils;
import com.day.cq.i18n.I18n;

@Model(adaptables={ SlingHttpServletRequest.class })
public class BookingAwardFlightSelectSelection extends BookingAwardGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private BookingAwardSession bookingAwardSession;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private List<AvailableFlightsSelectionRender> availableFlightSelections;
	private String[] expectedBrands;
	private List<BrandRender> brandHeaderList;
	private int brandListSize;
	private int routeIndex;
	private String urlCalendarPage;
	private BookingFlightFilterTypeEnum orderType;
	private boolean directFlightOnly;
	private boolean showCheckBoxDirectFlightOnly;
	private boolean showMoreFlights;
	private boolean isFcoLin;
	
	private static final int START_DAY_HH= 0;
	private static final int START_DAY_MM= 0;
	private static final int START_AFTERNOON_HH= 11;
	private static final int START_AFTERNOON_MM= 0;
	private static final int START_NIGHT_HH= 17;
	private static final int START_NIGHT_MM= 0;
	
	@PostConstruct
	protected void initModel() {
		try{
		
			initBaseModel(request);
		
			String[] selectors = request.getRequestPathInfo().getSelectors();
		
			if (selectors.length < 2 || !(selectors[1].equals("0") || selectors[1].equals("1"))) {
				logger.error("Direction of the route not found");
				throw new RuntimeException("direction of the route not found");
			}
			routeIndex = Integer.parseInt(selectors[1]);
		
			if (routeIndex != 0 && routeIndex != 1) {
				logger.error("Direction of the route not found");
				throw new RuntimeException("direction of the route not found");
			}
		
			boolean moreFlights = Boolean.parseBoolean(request.getParameter("moreFlights"));
			orderType = BookingFlightFilterTypeEnum.fromValue(request.getParameter("orderType"));
			directFlightOnly = Boolean.parseBoolean(request.getParameter("directFlightOnly"));
			urlCalendarPage = request.getResource().getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false)) + configuration.getBookingAwardCalendarPage();
			
			if (!(ctx.searchKind != BookingSearchKindEnum.ROUNDTRIP && routeIndex == 1)) {
			
				boolean solutionsFound = ctx.availableFlights != null && ctx.availableFlights.getRoutes() != null &&
						!ctx.availableFlights.getRoutes().isEmpty();
			
				//populate the list rows with each row of the choosing selection flight matrix
				if (solutionsFound) {
					expectedBrands = computeExpectedBrands(ctx);
					brandHeaderList = setBrandHeader(ctx, routeIndex);
					brandListSize = brandHeaderList.size();
					availableFlightSelections = prepareAvailableFlightSelections(ctx, i18n, moreFlights, orderType, directFlightOnly);
					
					/*Se volo FCO-LIN o viceversa deve popolare filtri Mattina-Pomeriggio-Sera*/
					isFcoLin = ctx.searchCategory == BookingSearchCategoryEnum.FCO_LIN;
				} else {
					logger.error("availableFligths not in session or empty");
					throw new RuntimeException("availableFligths not in session or empty");
				}
			} else {
				logger.warn("Parametri in input non corretti");
			}
		}
		catch(Exception e){
			logger.error("Unexpected Exception in BookingAwardFlightSelectSelection", e);
			
		}
	}
	
	private List<BrandRender> setBrandHeader(BookingSessionContext ctx, int routeIndex) {
		
		List<BrandRender> brandList = new ArrayList<BrandRender>();
		
		for (String brand : expectedBrands) {
			BrandPageData page = ctx.brandMap.get(brand);
			BrandRender brandRender = new BrandRender(page, ctx);
			brandList.add(brandRender);
		}

		return brandList;
	}
	
	private List<AvailableFlightsSelectionRender> prepareAvailableFlightSelections(BookingSessionContext ctx, I18n i18n, boolean moreFlights,
			BookingFlightFilterTypeEnum orderType, boolean directFlightOnly) {
		
		logger.debug("prepareAvailableFlightSelections");
		
		List<AvailableFlightsSelectionRender> availableFlightSelections = new ArrayList<AvailableFlightsSelectionRender>(); 
		
		RouteData route =  ctx.availableFlights.getRoutes().get(routeIndex);
		
		BrandData selectedBrandData = null;
		FlightData selectedFlightData = null;
		if(ctx.flightSelections[routeIndex] != null){
			FlightSelection selectedFlight = ctx.flightSelections[routeIndex];
			selectedFlightData = selectedFlight.getFlightData();
			for(BrandData brandData : selectedFlight.getFlightData().getBrands() ) {
				if ( brandData.getCode().equalsIgnoreCase(
						selectedFlight.getSelectedBrandCode()) ) {
					selectedBrandData = brandData;
					break;
				}
			}
		}
		
		showCheckBoxDirectFlightOnly = !(ValidationUtils.checkAllFlights(route.getFlights(), 
				ValidationUtils.isDirect()) 
				|| ValidationUtils.checkAllFlights(route.getFlights(), 
						ValidationUtils.isConnecting()));
		
		List<FlightData> currentFlightToShow = bookingAwardSession.obtainFlightsToShown(ctx, routeIndex, route, moreFlights);
		showMoreFlights = (route.getFlights().size() != currentFlightToShow.size());
		
		Iterator<FlightData> itFlight = currentFlightToShow.iterator();
		int i = 0;
		while(itFlight.hasNext()){
			FlightData flightData = itFlight.next();
			// isRefreshedSlice sempre false per booking award perche' non esiste l'operazione di refresh
			availableFlightSelections.add(new AvailableFlightsSelectionRender(flightData, 
					ctx.locale, i18n, false, selectedFlightData, selectedBrandData, expectedBrands, ctx, i++));
		}
		
		
		return (filterAndOrderFlights(availableFlightSelections, orderType, directFlightOnly));
	}
	
	private String[] computeExpectedBrands(BookingSessionContext ctx) {
		
		if (!ctx.availableFlights.getRoutes().isEmpty() &&
				!ctx.availableFlights.getRoutes().get(routeIndex).getFlights().isEmpty()) {
			FlightData flightData = ctx.availableFlights.getRoutes().get(0).getFlights().get(0);

			AreaValueEnum areaFrom = null;
			AreaValueEnum areaTo = null;

			if (flightData instanceof DirectFlightData) {
				DirectFlightData directFlightData = (DirectFlightData) flightData;
				areaFrom = directFlightData.getFrom().getArea();
				areaTo = directFlightData.getTo().getArea();

			} else {
				ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
				DirectFlightData first = (DirectFlightData) connectingFlightData.getFlights().get(0);
				DirectFlightData last = (DirectFlightData) connectingFlightData.getFlights().get(connectingFlightData.getFlights().size() - 1);
				areaFrom = first.getFrom().getArea();
				areaTo = last.getTo().getArea();
			}
			
			AreaValueEnum flightType = areaFrom == AreaValueEnum.DOM ? areaTo : areaFrom;
			
			if (flightType == AreaValueEnum.DOM) {
				return configuration.getBookingAwardBrandsIntz();
			} else if (flightType == AreaValueEnum.INTZ) {
				return configuration.getBookingAwardBrandsIntz();
			}	
		} else {
			if (ctx.availableFlights.getRoutes().isEmpty()) {
				logger.error("ctx.availableFlights.getRoutes() is empty");
			} else {
				logger.error("ctx.availableFlights.getRoutes().get({}).getFlights() is empty", routeIndex);
			}
		}
		return configuration.getBookingAwardBrands();
	}
	
	private List<AvailableFlightsSelectionRender> filterAndOrderFlights (
			List<AvailableFlightsSelectionRender> flights, BookingFlightFilterTypeEnum filter, boolean directFlightOnly){	
		return flights.stream().filter(filterFlights(directFlightOnly, filter))
				.sorted(new FlightDataComparator(filter , ctx.locale))
				.collect(Collectors.toList());
	}
	
	
	private Predicate<? super AvailableFlightsSelectionRender> filterFlights (boolean directFlightOnly, BookingFlightFilterTypeEnum filter) {
		return p -> {
			Calendar departureDate = p.getFlightData().getFlightType().equals(FlightTypeEnum.DIRECT) 
					? ((DirectFlightData)p.getFlightData()).getDepartureDate() 
					: ((DirectFlightData)((ConnectingFlightData)p.getFlightData()).getFlights().get(0)).getDepartureDate();
			Calendar startDay = Calendar.getInstance();
			startDay.setTime(departureDate.getTime());
			startDay.set(Calendar.HOUR_OF_DAY, START_DAY_HH);
			startDay.set(Calendar.MINUTE, START_DAY_MM);
			Calendar startAfternoon = Calendar.getInstance();
			startAfternoon.setTime(departureDate.getTime());
			startAfternoon.set(Calendar.HOUR_OF_DAY, START_AFTERNOON_HH);
			startAfternoon.set(Calendar.MINUTE, START_AFTERNOON_MM);
			Calendar startNight = Calendar.getInstance();
			startNight.setTime(departureDate.getTime());
			startNight.set(Calendar.HOUR_OF_DAY, START_NIGHT_HH);
			startNight.set(Calendar.MINUTE, START_NIGHT_MM);
			boolean filterResult = true;
			switch(filter){
			case MORNING:
				filterResult =  (departureDate.compareTo(startDay) >= 0 && departureDate.before(startAfternoon));
				break;
			case AFTERNOON:
				filterResult =  (departureDate.compareTo(startAfternoon) >= 0 && departureDate.before(startNight));
				break;
			case EVENING:
				startDay.add(Calendar.DAY_OF_MONTH, 1);
				filterResult =  (departureDate.compareTo(startNight) >= 0 && departureDate.before(startDay));
				break;
			default:
				filterResult = true;
		}
			return (!directFlightOnly || p.getFlightData().getFlightType().equals(FlightTypeEnum.DIRECT)) && filterResult;
			
			};
		}

	
	public List<AvailableFlightsSelectionRender> getAvailableFlightSelections() {
		return availableFlightSelections;
	}
	
	public List<BrandRender> getBrandHeaderList() {
		return brandHeaderList;
	}
	
	public int getBrandListSize() {
		return brandListSize;
	}

	public int getRouteIndex() {
		return routeIndex;
	}
	
	public String getUrlCalendarPage() {
		return urlCalendarPage;
	}
	
	public String getOrderType() {
		return orderType.value();
	}

	public boolean isDirectFlightOnly() {
		return directFlightOnly;
	}
	
	public boolean isShowCheckBoxDirectFlightOnly() {
		return showCheckBoxDirectFlightOnly;
	}
	
	public boolean getShowMoreFlights() {
		return showMoreFlights;
	}
	
	public boolean isFcoLin() {
		return isFcoLin;
	}
}
