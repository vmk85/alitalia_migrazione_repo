package com.alitalia.aem.consumer.checkinrest.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.countries.Countries;
import com.alitalia.aem.common.messages.home.CountriesRequest;
import com.alitalia.aem.common.messages.home.CountriesResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.staticdata.delegaterest.StaticDataDelegateRest;

@Model(adaptables = { SlingHttpServletRequest.class })
public class CountriesModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private volatile StaticDataDelegateRest staticDataDelegateRest;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private Countries countries;
	
	@PostConstruct
	protected void initModel() throws Exception {
//		logger.info("initModel di Countries Model -- chiamando getCountries...");
		countries = new Countries();
		CountriesRequest countriesRequest = new CountriesRequest(IDFactory.getTid(), IDFactory.getSid(request));
		countriesRequest.setLanguage(AlitaliaUtils.getRepositoryPathLanguage(request.getResource()));
		countriesRequest.setMarket(AlitaliaUtils.getRepositoryPathMarket(request.getResource()));
		countriesRequest.setConversationID(IDFactory.getTid());
		
		CountriesResponse countriesResponse = staticDataDelegateRest.getCountries(countriesRequest);
		
		this.setCountries(countriesResponse.getCountries());
	}

	public Countries getCountries() {
		return countries;
	}

	public void setCountries(Countries countries) {
		this.countries = countries;
	}

	
}
