package com.alitalia.aem.consumer.checkinrest.render.ancillary;

import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.AncillaryOffer;
import com.alitalia.aem.common.data.checkinrest.model.ancillaryoffers.response.FlightAncillaryOffer;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Flight;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Segment;
import com.alitalia.aem.consumer.checkinrest.render.FlightsRender;
import com.alitalia.aem.consumer.checkinrest.render.SegmentRender;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ggadaleta on 29/01/2018.
 */
public class FlightAncillaryRender {

    private FlightAncillaryOffer flightAncillary;
    
    private List<SegmentRender> flightData;

    private List<PassengerAncillaryRender> ancillaryOfferRender;

    public FlightAncillaryRender(FlightAncillaryOffer inputFlight, List<SegmentRender> f, List<Passenger> p)
    {
        this.flightAncillary = inputFlight;
        //setFlightData(f);
        setSegmentData(f);
        setAncillaryOfferRender(p);
    }
    protected void setSegmentData(List<SegmentRender> f) {
        this.flightData = f;
    }

    protected void setAncillaryOfferRender(List<Passenger> passengers) {

        List<PassengerAncillaryRender> result = new ArrayList<>();

        int counter = 0;
        for(AncillaryOffer ancillaryOffer : this.flightAncillary.getAncillaryOffers())
        {
            result.add(new PassengerAncillaryRender(ancillaryOffer, passengers.get(counter)));
            /*for (Passenger selectedPassenger : passengers) {
                result.add(new PassengerAncillaryRender(ancillaryOffer, selectedPassenger));
            }*/
            counter++;
        }
        this.ancillaryOfferRender = result;
    }

    public List<SegmentRender> getFlightData() {
        return this.flightData;
    }

    public FlightAncillaryOffer getFlightAncillary() {
        return this.flightAncillary;
    }

    public List<PassengerAncillaryRender> getAncillaryOfferRender() {
        return this.ancillaryOfferRender;
    }

}
