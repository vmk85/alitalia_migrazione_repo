package com.alitalia.aem.consumer.booking.render;

import java.text.SimpleDateFormat;
import java.util.Locale;

import com.alitalia.aem.common.data.home.FlightTabData;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.day.cq.i18n.I18n;

public class AvailableRibbonTabsRender{
	private FlightTabData tabData;
	private String weekDay;
	private String month;
	private String day;
	private GenericPriceRender price;
	private String simbolCurrency;
	private boolean isAvailable;
	
	public AvailableRibbonTabsRender(FlightTabData tab, BookingSessionContext ctx, I18n i18n){
		this.tabData = tab;
		this.weekDay  = "";
		this.month  = "";
		this.day  = "";
		this.price = null;
		this.simbolCurrency = "";
		this.isAvailable = false;
		init(tab,ctx,i18n);
	}
	
	private void init(FlightTabData tab, BookingSessionContext ctx, I18n i18n){
		this.weekDay = computeWeekDay(tab,i18n);
		this.month  = computeMonth(tab, i18n);
		this.day = (new DateRender(tab.getDate())).getDay();
		this.price =  new GenericPriceRender(tab.getPrice(), tab.getCurrency(), ctx);
		this.simbolCurrency = "EUR";
		this.isAvailable = computeIsAvailable(tab);
	}

	public FlightTabData getTabData() {
		return tabData;
	}

	public String getWeekDay() {
		return weekDay;
	}

	public String getMonth() {
		return month;
	}

	public String getDay() {
		return day;
	}

	public GenericPriceRender getPrice() {
		return price;
	}
	
	public void setPrice(GenericPriceRender price) {
		this.price = price;
	}

	public String getSimbolCurrency() {
		return simbolCurrency;
	}

	public boolean isAvailable() {
		return isAvailable;
	}

	
	/* private methods */
	
	private boolean computeIsAvailable(FlightTabData tab) {
		return (tab.getPrice() != null && tab.getPrice().doubleValue() > 0);
	}
	
	private String computeWeekDay(FlightTabData tab, I18n i18n) {
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.ENGLISH);
		String keyDay = (sdf.format(tab.getDate().getTime())).toLowerCase();
		return (i18n.get("common.daysOfWeek." + keyDay + ".short")).toUpperCase();
	}
	
	private String computeMonth(FlightTabData tab, I18n i18n) {
		SimpleDateFormat sdf = new SimpleDateFormat("MMMMM", Locale.ENGLISH);
		String keyDay = (sdf.format(tab.getDate().getTime())).toLowerCase();
		return (i18n.get("common.monthsOfYear." + keyDay + ".short")).toUpperCase();
	}

}