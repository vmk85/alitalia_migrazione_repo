package com.alitalia.aem.consumer.myalitalia.servlet;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;
import com.gigya.socialize.GSRequest;
import com.gigya.socialize.GSResponse;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import javax.servlet.Servlet;
import java.io.IOException;

@Component
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = { "setAccountInfoServlet" }),
        @Property(name = "sling.servlet.methods", value = { "POST" }),
        @Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) ,
        @Property(name = "sling.servlet.extensions", value = { "json" })
})
//@SuppressWarnings("serial")
public class setAccountInfoServlet extends GenericMyAlitaliaFormValidatorServlet {

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile AlitaliaCustomerProfileManager customerProfileManager;

    @Reference(policy = ReferencePolicy.DYNAMIC)
    private volatile ConsumerLoginDelegate consumerLoginDelegate;

    @Reference
    private AlitaliaConfigurationHolder configuration;


    @Override
    protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

        logger.debug("[setAccountInfoServlet] performSubmit");

        final I18n i18n = new I18n(request.getResourceBundle(
                AlitaliaUtils.findResourceLocale(request.getResource())));

        if(configuration==null)
            configuration = new AlitaliaConfigurationHolder();

        String apiMethod = "accounts.setAccountInfo";

        String apiKey = configuration.getMaApiKey(); // "3_FDF25EQ_XpOyo4xiZloa6QmF7QXYsb5r-rJaZj5ocvqW8B4bdCucFfqNdpaQPIec";
        String secretKey = configuration.getMaSecretKey();// "7XDjmCjYblwLmP+ejS79/RhqJPGapRbl";
        String userKey = configuration.getMaUserKey(); // "AI9W/RQHPi9r";

        GSRequest req = new GSRequest(apiKey, secretKey, apiMethod, null, true, userKey);
        GSResponse res;

        req.setAPIDomain("eu1.gigya.com");

        TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
        json.object();


        response.setContentType("application/json");

        try {

            logger.debug("[setAccountInfoServlet] setto i parametri per la request = {UID: " + request.getParameter("uid") + ", password: " + request.getParameter("password") + ", secretA: " + configuration.getMADefaultSecretAnswer() + ", secretQ: " + configuration.getMADefaultSecretQuestion());

            if(request.getParameter("data") != null) req.setParam("data", request.getParameter("data"));
            req.setParam("UID", request.getParameter("uid"));
            req.setParam("password", request.getParameter("password"));
            req.setParam("secretAnswer", configuration.getMADefaultSecretAnswer());
            req.setParam("secretQuestion", configuration.getMADefaultSecretQuestion());

            logger.debug("[setAccountInfoServlet] chiamata in corso..");

            res = req.send();

            if(res.getErrorCode() == 0) {

                logger.debug("[setAccountInfoServlet] chiamata effettuata");

                json.key("isError").value(false);
                json.key("errorCode").value(0);

            } else {

                json.key("isError").value(true);
                json.key("errorCode").value(res.getErrorCode());
                json.key("errorMessage").value(res.getErrorMessage());
                json.key("errorDetails").value(res.getErrorDetails());
                json.key("feedback").value(i18n.get("myalitalia.setOTP.genericError"));

                logger.error("[setAccountInfoServlet] errore nella chiamata:" + json.toString());

            }

        } catch (Exception e) {

            json.key("isError").value(true);
            json.key("errorCode").value("500");
            json.key("errorDetails").value("Java Exception");
            json.key("errorMessage").value(e.toString());
            json.key("feedback").value(i18n.get("myalitalia.setOTP.genericError"));

            logger.debug("[setAccountInfoServlet] " + e.toString());

        } finally {
            json.endObject();
        }

    }

    @Override
    protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
        logger.debug("[RecuperoTelefonoServlet] validateForm");
        Validator validator = new Validator();
        ResultValidation resultValidation = new ResultValidation();
        resultValidation.setResult(true);
        return resultValidation;
    }

    @Override
    protected void saveDataIntoSession(SlingHttpServletRequest request, Exception exception) {
        logger.debug("[RecuperoTelefonoServlet] saveDataIntoSession");

    }


}