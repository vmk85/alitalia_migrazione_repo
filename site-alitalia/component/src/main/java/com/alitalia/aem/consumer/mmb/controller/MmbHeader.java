package com.alitalia.aem.consumer.mmb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.mmb.render.MmbFlightDataRender;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MmbHeader extends MmbSessionGenericController {

	@Self
	private SlingHttpServletRequest request;
	
	private List<MmbFlightDataRender> flightDataRenderList;
	private String datePaymentLimit;
	
	@PostConstruct
	protected void initModel() throws IOException {
		initBaseModel(request);
		flightDataRenderList = new ArrayList<MmbFlightDataRender>();
		for (MmbFlightData flightData : ctx.route.getFlights()) {
			flightDataRenderList.add(new MmbFlightDataRender(flightData));
		}
		
		if (ctx.route.getDateTimePaymentLimit() != null) {
			DateRender dateRender = new DateRender(ctx.route.getDateTimePaymentLimit());
			datePaymentLimit = dateRender.getExtendedDate();
		}
	}

	public List<MmbFlightDataRender> getFlightDataRenderList() {
		return flightDataRenderList;
	}

	public String getDatePaymentLimit() {
		return datePaymentLimit;
	}

	public String getPnr() {
		if (ctx != null) {
			return ctx.pnr;
		} else {
			return null;
		}
	}
	
}
