package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.Servlet;

import com.alitalia.aem.consumer.checkinrest.controller.DangerousGoodsModel;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.request.Passenger;
import com.alitalia.aem.common.data.checkinrest.model.checkinselectedpassenger.response.Outcome;
import com.alitalia.aem.common.messages.checkinrest.CheckinSelectedPassengerResponse;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.day.cq.commons.TidyJSONWriter;
import com.google.gson.Gson;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkinselectedpassenger"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinSelectedPassengerServlet extends GenericCheckinFormValidatorServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private AlitaliaConfigurationHolder configuration;

	@Reference
	private CheckinSession checkinSession;

	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		Validator validator = new Validator();
		
		String pnr = request.getParameter("pnr");
		String airline = request.getParameter("airline");
		String origin = request.getParameter("origin");
		String flight = request.getParameter("flight");
		
		validator.addDirectCondition("pnr", pnr, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("pnr", pnr, 
				CheckinConstants.MESSAGE_ERROR_INVALID_PNR, "isPnrOrTicketNumber");
		
		validator.addDirectCondition("airline", airline, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("origin", origin, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition("flight", flight, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		ResultValidation resultValidation = new ResultValidation();
		resultValidation.setResult(true);
		return resultValidation;
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

		CheckinSessionContext ctx = getCheckInSessionContext(request);

		// If request gotodangerous vado direttamente alla pagina dei materiali pericolosi
		if (request.getParameter("gotodangerous") != null)
		{

			// Metto la request in sessione

			ctx.request = request;

			goToDangerous(request, response);

		}
		else {

			//Oggetto Data utilizzato per la response del servizio
			CheckinSelectedPassengerResponse checkinSelectedPassengerResponse = new CheckinSelectedPassengerResponse();

			try {
				if(ctx.request!=null){
					checkinSelectedPassengerResponse = checkinSession.checkinSelectedPassenger(ctx.request, ctx);
				}else{
					checkinSelectedPassengerResponse = checkinSession.checkinSelectedPassenger(request, ctx);
				}


				if (checkinSelectedPassengerResponse != null) {
					if (checkinSelectedPassengerResponse.getOutcome() != null && (checkinSelectedPassengerResponse.getOutcome().getOutcome().equals("OK") || checkinSelectedPassengerResponse.getOutcome().getOutcome().equals("OK-PRIORITY"))) {

						managementSucces(request, response);

					} else if (checkinSelectedPassengerResponse.getOutcome() != null && checkinSelectedPassengerResponse.getOutcome().getOutcome().equals("KO")) {

						if (checkinSelectedPassengerResponse.getOutcome().getError() != null) {
							Gson gson = new Gson();
							com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(checkinSelectedPassengerResponse.getOutcome().getError(), com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);
							if (errorObj.getErrorMessage().equals("!PASSENGER ALREADY ON PRIORITY LIST")) {
								managementSucces(request, response);
							} else {
								managementError(request, response, checkinSelectedPassengerResponse.getOutcome().getError());
							}
						}
					}

				} else {
					logger.error("[CheckinSelectedPassengerServlet] - Errore.");
				}


			} catch (Exception e) {
				logger.error("[CheckinSelectedPassengerServlet] - Errore durante l'invocazione del servizio", e);
			}
		}
	}
	
	public void managementError(SlingHttpServletRequest request, SlingHttpServletResponse response, String error){
		Gson gson = new Gson();
		
		logger.info("[CheckinSelectedPassengerServlet] [managementError] error =" + error );
		
		String errorUrl = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCheckinFailurePage();

		errorUrl = request.getResourceResolver().map(errorUrl);
		
		com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error errorObj = gson.fromJson(error, com.alitalia.aem.common.data.checkinrest.model.pnrinfo.Error.class);
		
		TidyJSONWriter json;
		try {
			json = new TidyJSONWriter(response.getWriter());
		
			response.setContentType("application/json");
			
			json.object();
			json.key("isError").value(true);
			json.key("errorMessage").value(errorObj.getErrorMessage());
			json.key("sabreStatusCode").value(errorObj.getSabreStatusCode());
			json.key("conversationID").value(errorObj.getConversationID());
			json.key("errorUrl").value(errorUrl);
			json.endObject();
		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
	}
	
	public void managementSucces(SlingHttpServletRequest request, SlingHttpServletResponse response){
		
		logger.info("[CheckinSelectedPassengerServlet] [managementSucces] ...");
		String successPage = "";
		if(getCheckInSessionContext(request).standBy) {
			successPage = AlitaliaUtils.findSiteBaseExternalUrl(
					request.getResource(), false)
					+ getConfiguration().getCheckinThankYouPage();
		}
		else{
			successPage = AlitaliaUtils.findSiteBaseExternalUrl(
					request.getResource(), false)
					+ getConfiguration().getCheckinSeatMapPage();// .getCheckinDangerousGoodsPage();
		}
		successPage = request.getResourceResolver().map(successPage);

		logger.info("[CheckinSelectedPassengerServlet] [managementSucces] successPage = " + successPage );
		
		TidyJSONWriter json;
		try {
			
			json = new TidyJSONWriter(response.getWriter());
			response.setContentType("application/json");
			json.object();
			json.key("isError").value(false);
			json.key("successPage").value(successPage);
			json.endObject();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}	
		
	}

	public void goToDangerous(SlingHttpServletRequest request, SlingHttpServletResponse response){

		logger.info("[CheckinSelectedPassengerServlet] [managementSucces] ...");
		String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
				request.getResource(), false)
				+ getConfiguration().getCheckinDangerousGoodsPage();

		successPage = request.getResourceResolver().map(successPage);

		logger.info("[CheckinSelectedPassengerServlet] [managementSucces] successPage = " + successPage );

		TidyJSONWriter json;
		try {

			json = new TidyJSONWriter(response.getWriter());
			response.setContentType("application/json");
			json.object();
			json.key("isError").value(false);
			json.key("successPage").value(successPage);
			json.endObject();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	

}
