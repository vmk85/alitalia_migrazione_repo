package com.alitalia.aem.consumer.servlet.login;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.SetSocialAccountRequest;
import com.alitalia.aem.common.messages.home.SetSocialAccountResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;

/**
 * Servlet exposing a REST-based service to perform the association
 * between a user and a social account.
 * 
 * <p>The expected input includes the following request parameters:
 * <ul>
 * <li>mmcode: the MilleMiglia code of the user.</li>
 * <li>mmpin: the MilleMiglia PIN of the user.</li>
 * <li>gigyaId: the Gigyia UID.</li> 
 * <li>gigyaSignature: the Gigyia signature.</li> 
 * </ul>
 * </p>
 * 
 * <p>The output is a JSON object with the following attributes:
 * <ul>
 * <li>successful: true for a successful association, false otherwise.</li>
 * <li>errorCode: code related to the failed attempt, if applicable.</li> 
 * <li>errorDescription: a description related to the failed attempt, if applicable.</li>
 * <li>gigyaId: the Gigyia UID.</li> 
 * <li>gigyaSignature: the Gigyia signature.</li> 
 * <li>mmCode: the MilleMiglia code of the user.</li>
 * <li>mmPincode: the MilleMiglia PIN of the user.</li>
 * </ul>
 * </p>
 */

@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "setsocialaccount" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.extensions", value = { "json" })
})
@SuppressWarnings("serial")
public class SetSocialAccountServlet extends SlingAllMethodsServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * Reference to the service to remote consumer login service.
	 */
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_UNARY)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;
	
	@Reference
	private volatile AlitaliaConfigurationHolder configuration;
	
	@SuppressWarnings("unused")
	private ComponentContext componentContext;
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		
		logger.debug("Social account association requested.");
		
		String mmCode = request.getParameter("mmCode");
		String mmPin = request.getParameter("mmPin");
		String gigyaId = request.getParameter("gigyaId");
		String gigyaSignature = request.getParameter("gigyaSignature");
		
		boolean successful = false;
		String errorCode = "";
		String errorDescription = "";
		
		if (consumerLoginDelegate == null) {
			logger.error("Cannot associate social account, delegate not available.");
			successful = false;
			errorCode = "unknown";
			errorDescription = "Login delegate currently unavailable";
		
		} else {
			
			try {
				String tid = IDFactory.getTid();
				String sid = IDFactory.getSid(request);
				
				SetSocialAccountRequest setSocialAccountRequest = new SetSocialAccountRequest(tid, sid);
				setSocialAccountRequest.setGigyaId(gigyaId);
				setSocialAccountRequest.setGigyaSignature(gigyaSignature);
				setSocialAccountRequest.setUserCode(mmCode);
				setSocialAccountRequest.setPin(mmPin);
				SetSocialAccountResponse setSocialAccountResponse = 
						consumerLoginDelegate.setSocialAccount(setSocialAccountRequest);
				
				successful = setSocialAccountResponse.isSucceeded();
				if (!successful) {
					errorCode = "unknown";
					errorDescription = "Cannot perform the operation";
				} else {
					
					logger.debug("SSOToken retrieved: "+setSocialAccountResponse.getSsoToken());
					if (setSocialAccountResponse.getSsoToken() != null && !setSocialAccountResponse.getSsoToken().isEmpty()) {
						Cookie ssoCookie = new Cookie("ibeopentoken", setSocialAccountResponse.getSsoToken());
						int ssoCookieExpiry = configuration.getSsoCookieTimeout();
						String ssoCookieDomain = configuration.getSsoCookieDomain();
						
						ssoCookie.setPath("/");
						ssoCookie.setSecure(configuration.getHttpsEnabled());
						if (ssoCookieExpiry != -1)
							ssoCookie.setMaxAge(ssoCookieExpiry);
						if (!"".equals(ssoCookieDomain))
							ssoCookie.setDomain(ssoCookieDomain);
						response.addCookie(ssoCookie);
					} else {
						logger.error("SSOToken is null or empty");
					}
					
					// clear pending social login data after using it
					request.getSession().removeAttribute("pendingSocialLoginData");
				}
				
			} catch (Exception e) {
				logger.error("Error trying to set social account.", e);
				successful = false;
				errorCode = "unknown";
				errorDescription = "Unexpected error";
				
			}
			
		}
		
		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		try {
			json.object();
			json.key("successful");
			json.value(successful);
			json.key("errorCode");
			json.value(errorCode);
			json.key("errorDescription");
			json.value(errorDescription);
			json.key("mmCode");
			json.value(mmCode);
			json.key("mmPin");
			json.value(mmPin);
			json.key("gigyaId");
			json.value(gigyaId);
			json.key("gigyaSignature");
			json.value(gigyaSignature);
			json.endObject();
		} catch (Exception e) {
			logger.error("Unexected error generating JSON response.", e);
		}
	}
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
}
