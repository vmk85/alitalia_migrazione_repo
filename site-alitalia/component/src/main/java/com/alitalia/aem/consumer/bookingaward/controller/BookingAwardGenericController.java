package com.alitalia.aem.consumer.bookingaward.controller;

import org.apache.sling.api.SlingHttpServletRequest;

import com.alitalia.aem.consumer.booking.BookingConstants;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.day.cq.wcm.api.WCMMode;

public class BookingAwardGenericController extends GenericBaseModel {

	// booking award session context
	protected BookingSessionContext ctx;
	
	@Override
	protected void initBaseModel(SlingHttpServletRequest request) {
		
		super.initBaseModel(request);
		ctx = (BookingSessionContext) request.getSession(true).getAttribute(BookingConstants.BOOKING_CONTEXT_ATTRIBUTE);
		WCMMode wcmMode = WCMMode.fromRequest(request);
		
		if (ctx == null && !WCMMode.EDIT.equals(wcmMode)) {
			logger.error("Cannot find booking session context in session.");
			throw new RuntimeException("Cannot find booking session context in session.");
		}
	}

	public BookingSessionContext getCtx() {
		return ctx;
	}
}
