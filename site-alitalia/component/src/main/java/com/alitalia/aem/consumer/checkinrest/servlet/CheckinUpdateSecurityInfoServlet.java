package com.alitalia.aem.consumer.checkinrest.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.checkin.CheckinConstants;
import com.alitalia.aem.consumer.checkin.CheckinSession;
import com.alitalia.aem.consumer.checkin.CheckinSessionContext;
import com.alitalia.aem.consumer.checkin.servlet.GenericCheckinFormValidatorServlet;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "checkinupdatesecurityinfo"}),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }) })
@SuppressWarnings("serial")
public class CheckinUpdateSecurityInfoServlet extends GenericCheckinFormValidatorServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Reference
	private AlitaliaConfigurationHolder configuration;
	
	@Reference
	private CheckinSession checkinSession;
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return configuration;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		Validator validator = new Validator();
		
		String pnr = request.getParameter("pnr");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		
		validator.addDirectCondition("pnr", pnr, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		
		validator.addDirectCondition("firstName", firstName, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("firstName", firstName, 
				CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
		
		validator.addDirectCondition("lastName", lastName, 
				CheckinConstants.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition("lastName", lastName, 
				CheckinConstants.MESSAGE_GENERIC_INVALID_FIELD, "isNomeValido");
		
		
		ResultValidation resultValidation = validator.validate();
		return resultValidation;
		
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {

			CheckinSessionContext ctx = getCheckInSessionContext(request);
			
			String errorUrl = AlitaliaUtils.findSiteBaseExternalUrl(
					request.getResource(), false)
					+ getConfiguration().getCheckinFailurePage();
			
			errorUrl = request.getResourceResolver().map(errorUrl);
			
			String successPage = AlitaliaUtils.findSiteBaseExternalUrl(
					request.getResource(), false)
					+ getConfiguration().getCheckinPassengersPage();
			
			successPage = request.getResourceResolver().map(successPage);
			
			boolean result = checkinSession.checkinUpdateSecurityInfo(request, ctx);
			
			if (result) {
				response.sendRedirect(successPage);
			} else {
				//TODO - redirect to errorPage?
				TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
				response.setContentType("application/json");
				json.object();
				json.key("isError").value(true);
				json.endObject();
			}
		
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Map getAdditionalFailureData(SlingHttpServletRequest request,
			GenericFormValidatorServletException exception) {
		Map<String,String> additionalParams = new HashMap<String, String>();
		return additionalParams;
	}
}