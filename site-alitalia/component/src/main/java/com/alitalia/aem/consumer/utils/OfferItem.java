package com.alitalia.aem.consumer.utils;

public class OfferItem implements Comparable<OfferItem>{

	String destinazione;
	String tipo;
	String prezzo;
	String link;
	String pathImmagine;
	String area;
	String prezzoDaMostrare;
    String editorialResource;
    String editorialResource1;
    String editorialResource2;
    String editorialResource3;
    String editorialResource4;
    String editorialResource5;
    String editorialResource6;
    String editorialResource7;
    String editorialResource8;
    String editorialResource9;
    String editorialResource10;
    String editorialResource11;
    String editorialResource12;
    String editorialResource13;
    String spanText;
    Integer priority;



	public OfferItem() {}
	
	public OfferItem(String destinazione, String tipo, String prezzo, String link, String area) {
		this.destinazione = destinazione;
		this.tipo = tipo;
		this.prezzo = prezzo;
		this.link = link;
		this.area = area;
		this.prezzoDaMostrare = prezzo;
	}
	
	public OfferItem(String destinazione, String tipo, String prezzo, String link, String area, String pathImg) {
		this.destinazione = destinazione;
		this.tipo = tipo;
		this.prezzo = prezzo;
		this.link = link;
		this.area = area;
		this.pathImmagine = pathImg;
		this.prezzoDaMostrare = prezzo;
	}

    public int compareTo(OfferItem otherObject)
    {
        return priority.compareTo(otherObject.getPriority());
    }

	public String getDestinazione() {
		return destinazione;
	}
	
	public void setDestinazione(String destinazione) {
		this.destinazione = destinazione;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getPrezzo() {
		return prezzo;
	}
	
	public void setPrezzo(String prezzo) {
		this.prezzo = prezzo;
	}
	
	public String getLink() {
		return link;
	}
	
	public void setLink(String link) {
		this.link = link;
	}
	
	public String getArea() {
		return area;
	}
	
	public void setArea(String area) {
		this.area = area;
	}
	
	public String getPathImmagine() {
		return pathImmagine;
	}

	public void setPathImmagine(String pathImmagine) {
		this.pathImmagine = pathImmagine;
	}
	

	public String getPrezzoDaMostrare() {
		return prezzoDaMostrare;
	}

	public void setPrezzoDaMostrare(String prezzoDaMostrare) {
		this.prezzoDaMostrare = prezzoDaMostrare;
	}

    public String getEditorialResource() {
        return editorialResource;
    }

    public void setEditorialResource(String editorialResource) {
        this.editorialResource = editorialResource;
    }

    public void setEditorialResource1(String editorialResource) {
        this.editorialResource1 = editorialResource;
    }

    public String getEditorialResource1() {
        return editorialResource1;
    }

    public void setEditorialResource2(String editorialResource) {
        this.editorialResource2 = editorialResource;
    }

    public String getEditorialResource2() {
        return editorialResource2;
    }

    public void setEditorialResource3(String editorialResource) {
        this.editorialResource3 = editorialResource;
    }

    public String getEditorialResource3() {
        return editorialResource3;
    }

    public void setEditorialResource4(String editorialResource) {
        this.editorialResource4 = editorialResource;
    }

    public String getEditorialResource4() {
        return editorialResource4;
    }

    public String getEditorialResource5() {
        return editorialResource5;
    }

    public void setEditorialResource5(String editorialResource5) {
        this.editorialResource5 = editorialResource5;
    }

    public String getEditorialResource6() {
        return editorialResource6;
    }

    public void setEditorialResource6(String editorialResource6) {
        this.editorialResource6 = editorialResource6;
    }

    public String getEditorialResource7() {
        return editorialResource7;
    }

    public void setEditorialResource7(String editorialResource7) {
        this.editorialResource7 = editorialResource7;
    }

    public String getEditorialResource8() {
        return editorialResource8;
    }

    public void setEditorialResource8(String editorialResource8) {
        this.editorialResource8 = editorialResource8;
    }

    public String getEditorialResource9() {
        return editorialResource9;
    }

    public void setEditorialResource9(String editorialResource9) {
        this.editorialResource9 = editorialResource9;
    }

    public String getEditorialResource10() {
        return editorialResource10;
    }

    public void setEditorialResource10(String editorialResource10) {
        this.editorialResource10 = editorialResource10;
    }

    public String getEditorialResource11() {
        return editorialResource11;
    }

    public void setEditorialResource11(String editorialResource11) {
        this.editorialResource11 = editorialResource11;
    }

    public String getEditorialResource12() {
        return editorialResource12;
    }

    public void setEditorialResource12(String editorialResource12) {
        this.editorialResource12 = editorialResource12;
    }

    public String getEditorialResource13() {
        return editorialResource13;
    }

    public void setEditorialResource13(String editorialResource13) {
        this.editorialResource13 = editorialResource13;
    }

    public String getSpanText() {
        return spanText;
    }

    public void setSpanText(String spanText) {
        this.spanText = spanText;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
