package com.alitalia.aem.consumer.booking.render;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.utils.DateUtils;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.day.cq.i18n.I18n;

public class DirectFlightDataRender {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private DirectFlightData directFlightData;
	private List<SearchElementDetailRender> flightDetailsInfo;
	private String duration;
	private String waitingTime;
	private String carrier;
	private String classLogoCarrier;
	private Long nextDays;
	private SeatsMapRender seatMap;
	private CabinEnum cabin;
	
	public DirectFlightDataRender(DirectFlightData directFlightData, Integer waiting, I18n i18n, String market) {
		this.directFlightData = directFlightData;
		this.flightDetailsInfo = new ArrayList<SearchElementDetailRender>();
		this.duration = "";
		this.waitingTime = "";
		this.carrier = "";
		this.classLogoCarrier = "";
		this.nextDays = null;
		this.seatMap = null;
		this.cabin = directFlightData.getCabin();
		init(waiting, i18n, market);
	}

	public DirectFlightData getDirectFlightData() {
		return directFlightData;
	}


	public List<SearchElementDetailRender> getFlightDetailsInfo() {
		return flightDetailsInfo;
	}


	public String getDuration() {
		return duration;
	}
	
	public String getWaitingTime() {
		return waitingTime;
	}

	public String getCarrier() {
		return carrier;
	}

	public SeatsMapRender getSeatMap() {
		return seatMap;
	}
	
	public String getClassLogoCarrier() {
		return classLogoCarrier;
	}
	
	public CabinEnum getCabin() {
		return cabin;
	}
	
	
	/*private methods*/

	private void init(Integer waiting, I18n i18n, String market) {
		this.duration = computeDuration();
		this.carrier = obtainCarrier(i18n);
		classLogoCarrier = computeLogoCarrier(directFlightData);
		this.waitingTime = convertWaitingTimeToString(waiting);
		this.flightDetailsInfo = computeflightDetailsInfo(i18n, market);
		this.nextDays = computeNextDays(directFlightData);
	}

	private String computeLogoCarrier(DirectFlightData flight) {
		return	flight.getCarrier().toLowerCase() + "_small_logo";
	}
	
	private String convertWaitingTimeToString(Integer waiting) {
		if(waiting==null){
			return "";
		}
		if(waiting.intValue()==0){
			return "";
		}
		int durationHours = waiting.intValue()/60;
		int durationMinutes = waiting.intValue() - (durationHours*60); 
		String minutes = Integer.toString(durationMinutes);
		String hours = Integer.toString(durationHours);
		if(durationMinutes<10){
			minutes = "0" + minutes;
		}
		if(durationHours<10){
			hours = "0" + hours;
		}
		return hours + "H:" + minutes; 
	}


	private String obtainCarrier(I18n i18n) {
		if (directFlightData.isBus()) {
			return i18n.get("booking.bus.label").toUpperCase() + " " + directFlightData.getOperationalFlightText();
		} else if (!directFlightData.getCarrier().equals("AZ")) {
			return i18n.get("carriersData." + directFlightData.getCarrier() + ".description");
		} else {
			if (directFlightData.getOperationalFlightText() != null) {
				if (directFlightData.getOperationalFlightText().equals(directFlightData.getOperationalCarrier())) {
					return i18n.get("carriersData." + directFlightData.getOperationalCarrier() + ".description");
				} else {
					return directFlightData.getOperationalFlightText();
				}
				
			} else if (!directFlightData.getOperationalCarrier().equals("")) {
					return i18n.get("carriersData." + directFlightData.getOperationalCarrier() + ".description");
			}
			return "";
		}
	}

	private String computeDuration() {
		int durationHours = this.directFlightData.getDurationHour();
		int durationMinutes = this.directFlightData.getDurationMinutes();
		String minutes = Integer.toString(durationMinutes);
		String hours = Integer.toString(durationHours);
		if(durationMinutes<10){
			minutes = "0" + minutes;
		}
		if(durationHours<10){
			hours = "0" + hours;
		}
		return hours + "H:" + minutes;
	}
	
	public long getNextDays() {
		if (nextDays == null) {
			return 0;
		}
		return nextDays.longValue();
	}

	private List<SearchElementDetailRender> computeflightDetailsInfo(I18n i18n, String market) {
		
		List<SearchElementDetailRender> flightDetailsInfo = new ArrayList<SearchElementDetailRender>();
		SearchElementDetailRender infoDeparture = 
				new SearchElementDetailRender(new DateRender(this.directFlightData.getDepartureDate(), market), 
						this.directFlightData.getFrom().getCode(), 
						this.directFlightData.getFrom().getCityCode(),
						i18n.get("airportsData." + this.directFlightData.getFrom().getCityCode() + ".city"));
		
		SearchElementDetailRender infoArrival = 
				new SearchElementDetailRender(new DateRender(this.directFlightData.getArrivalDate(), market), 
						this.directFlightData.getTo().getCode(), 
						this.directFlightData.getTo().getCityCode(),
						i18n.get("airportsData." + this.directFlightData.getTo().getCityCode() + ".city"));
		
		flightDetailsInfo.add(infoDeparture);
		flightDetailsInfo.add(infoArrival);
		
		return flightDetailsInfo;
	}
	
	private Long computeNextDays(DirectFlightData directFlightData) {
		try {
			long daysBetween = DateUtils.daysBetween(directFlightData.getDepartureDate(), directFlightData.getArrivalDate());
			return (daysBetween != 0 ? daysBetween : null);
		} catch (Exception e) {
			logger.error("Unexpected error in computeNextDays: ", e);
		}
		return null;
	}
	
}
