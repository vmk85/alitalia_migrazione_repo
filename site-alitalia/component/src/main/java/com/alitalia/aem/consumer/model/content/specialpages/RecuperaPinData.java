package com.alitalia.aem.consumer.model.content.specialpages;

public class RecuperaPinData {
	public static final String NAME = "RecuperaPinData";
	
	private String code;
	private String surname;
	private String error;
	
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
	
	public String getError() {
		return error;
	}
	
	public void setError(String error) {
		this.error = error;
	}

	
	@Override
	public String toString() {
		return "RecuperaPinData [code=" + code + ", surname=" + surname +  "]";
	}
}
