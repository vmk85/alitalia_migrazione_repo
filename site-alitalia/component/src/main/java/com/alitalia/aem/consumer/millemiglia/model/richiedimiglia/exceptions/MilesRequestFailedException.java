package com.alitalia.aem.consumer.millemiglia.model.richiedimiglia.exceptions;

import com.alitalia.aem.consumer.millemiglia.servlet.RichiediMigliaFlightOutcome;

public class MilesRequestFailedException extends Exception {
	
	private RichiediMigliaFlightOutcome[] results;

	public MilesRequestFailedException() {
			// TODO Auto-generated constructor stub
	}
	
	public MilesRequestFailedException(RichiediMigliaFlightOutcome[] results) {
		this.results = results;
	}

	public MilesRequestFailedException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public MilesRequestFailedException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public MilesRequestFailedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public MilesRequestFailedException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public RichiediMigliaFlightOutcome[] getResults() {
		return results;
	}


}
