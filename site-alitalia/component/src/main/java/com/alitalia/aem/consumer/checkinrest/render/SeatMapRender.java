package com.alitalia.aem.consumer.checkinrest.render;

import java.util.List;

import com.alitalia.aem.common.data.checkinrest.model.enhancedseatmap.response.EnhancedSeatMapResp;

public class SeatMapRender {
	
	
	private FlightsSectorRender flightsSectorRender;
	
	private EnhancedSeatMapResp enhancedSeatMapResp;
	
	
	public FlightsSectorRender getFlightsSectorRender() {
		return flightsSectorRender;
	}

	public void setFlightsSectorRender(FlightsSectorRender flightsSectorRender) {
		this.flightsSectorRender = flightsSectorRender;
	}
	
	
	public EnhancedSeatMapResp getEnhancedSeatMapResp() {
		return enhancedSeatMapResp;
	}

	public void setEnhancedSeatMapResp(EnhancedSeatMapResp enhancedSeatMapResp) {
		this.enhancedSeatMapResp = enhancedSeatMapResp;
	}




}
