package com.alitalia.aem.consumer.bookingaward.controller;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@SuppressWarnings("rawtypes")
@Model(adaptables = { SlingHttpServletRequest.class })

public class MultifieldBookingAward extends GenericBaseModel{
	
    
    @Self
	private SlingHttpServletRequest slingHttpServletRequest;

    private Logger logger = LoggerFactory.getLogger(MultifieldBookingAward.class);
    
    private Map fieldList;

    /**
     * E' l'equivalente di un costruttore di classe. 
     * In questo caso viene utilizzato per prendere i valori del mutlifield
     */
    @PostConstruct
    protected void initModel() {
    	super.initBaseModel(slingHttpServletRequest);
    	//logger.info("Initializing model << BlockElement >>");
    	try {
            fieldList = AlitaliaUtils.fromJcrMultifieldToMapArrayObject(slingHttpServletRequest.getResource().getChild("comparative-panel-award"), "items");
        } catch(Exception e) {
            logger.error("Unable to get the values: \n" + e);
        }
    }

    /**
     * Restituisce tutti gli elementi del multifield
     */
    public Map getFieldList() {
        //logger.info("Returning fieldList");
        return fieldList;
    }
}



