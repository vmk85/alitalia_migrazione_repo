package com.alitalia.aem.consumer.booking.servlet;


import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSession;
import com.alitalia.aem.consumer.booking.BookingSessionContext;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServletException;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingremoveinsuranceconsumer" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.") })
@SuppressWarnings("serial")
public class BookingRemoveInsuranceServlet extends GenericBookingFormValidatorServlet {
	
	private ComponentContext componentContext;
	
	@Reference
	private AlitaliaConfigurationHolder alitaliaConfiguration;
	
	@Reference
	private BookingSession bookingSession;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		Validator validator = new Validator();
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
		BookingSessionContext ctx = getBookingSessionContext(request);
		if (ctx == null) {
			throw new GenericFormValidatorServletException(false);
		}
		bookingSession.removeInsurance(ctx);
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		json.key("result").value("OK");
		json.endObject();
	}
	
	@Override
	protected AlitaliaConfigurationHolder getConfiguration() {
		return alitaliaConfiguration;
	}
	
}
