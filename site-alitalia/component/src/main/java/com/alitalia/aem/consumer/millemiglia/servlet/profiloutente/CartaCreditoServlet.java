package com.alitalia.aem.consumer.millemiglia.servlet.profiloutente;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.DeletePanRequest;
import com.alitalia.aem.common.messages.home.DeletePanResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "cartacreditosubmit" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class CartaCreditoServlet extends GenericFormValidatorServlet {

	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile AlitaliaCustomerProfileManager customerProfileManager;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		
		logger.debug("[CartaCreditoServlet] validateForm");
		
		try {
			// validate parameters!
			Validator validator = setValidatorParameters(request, new Validator());
			return validator.validate();
		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Errore durante la procedura di salvataggio dati ", e);
			return null;
		}

	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, 
			SlingHttpServletResponse response) throws IOException {
		
		logger.debug("[CartaCreditoServlet] performSubmit");
		
		MMCustomerProfileData customerProfileData = AlitaliaUtils.getAuthenticatedUser(request);
		customerProfileData.setCustomerPinCode(
				customerProfileManager.decodeAndDecryptProperty(customerProfileData.getCustomerPinCode()));
		
		DeletePanRequest deletePanRequest = new DeletePanRequest();
		deletePanRequest.setSid(IDFactory.getSid(request));
		deletePanRequest.setTid(IDFactory.getTid());
		deletePanRequest.setCustomerProfile(customerProfileData);
		deletePanRequest.setPanCode(request.getParameter("creditCardToken"));
		
		DeletePanResponse deletePanResponse = consumerLoginDelegate.deletePan(deletePanRequest);
		
		if (!deletePanResponse.isSucceeded()) {
			throw new IOException(); //TODO: Verificare se sollevare un'eccezione diversa
		}
		
		goBackSuccessfully(request, response);
	}
	
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
		
		logger.debug("[CartaCreditoServlet] saveDataIntoSession");
		
		request.getSession().setAttribute("cartaCreditoData", "submitFail");
	}
	
	/**
	 * Set Validator parameters for validation
	 * 
	 * @param request
	 * @param validator
	 */
	private Validator setValidatorParameters(SlingHttpServletRequest request, Validator validator) {
		return validator;
	}
}
