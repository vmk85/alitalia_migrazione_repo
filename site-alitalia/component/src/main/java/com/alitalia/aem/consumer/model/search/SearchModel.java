package com.alitalia.aem.consumer.model.search;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.LabelUrl;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.xml.parsers.*;

import org.xml.sax.InputSource;
import org.w3c.dom.*;

import java.io.*;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

import org.apache.commons.lang3.StringEscapeUtils;

@Model(adaptables={SlingHttpServletRequest.class})
public class SearchModel extends GenericBaseModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private String searchedString;
	private Integer selectedPage;
	private Integer previousPage;
	private Integer nextPage;
	private String selectedCluster;
	private boolean isClusterSearch;
	private String result;
	
	private List <ResultData> elencoRisultati;
	private List <ResultCluster> elencoCluster;
	private List <Integer> pages;
	
	private String numRisultati;
	
	@PostConstruct
	protected void initModel() {
		super.initBaseModel(request);

		searchedString = (String) request.getSession().getAttribute("searchedString");
		selectedPage = (Integer) request.getSession().getAttribute("selectedPage");
		selectedCluster = (String) request.getSession().getAttribute("selectedCluster");
		isClusterSearch = false;
		if (!"".equals(selectedCluster)) {
			isClusterSearch = true;
		}

		logger.debug("[SearchModel] selectedPage: "+selectedPage);
		elencoRisultati = new ArrayList<ResultData> ();
		elencoCluster = new ArrayList<ResultCluster> ();
		pages = new ArrayList<Integer> ();
		result = (String) request.getSession().getAttribute("xmlGSAResponse");
		parseXml(result);
	}
	
	private void parseXml (String xmlString) {
		  try {
		        DocumentBuilderFactory dbf =
		            DocumentBuilderFactory.newInstance();
		        DocumentBuilder db = dbf.newDocumentBuilder();
		        InputSource is = new InputSource();
		        is.setCharacterStream(new StringReader(xmlString));

		        Document doc = db.parse(is);
		        
		        numRisultati = getCharacterDataFromElement((Element) doc.getElementsByTagName("M").item(0));
		        Integer maxPageNumber = new Integer ((int) Math.ceil(Integer.parseInt(numRisultati)/Integer.parseInt(configuration.getGSAResultPerPage())));
		        Integer pageOffset = Integer.parseInt(configuration.getGSAPageOffset());
		        Integer pageNumber = 2*pageOffset+1; 
		        pages = new ArrayList<Integer> (pageNumber);
        		
        		int startPage = Math.max(1,selectedPage-pageOffset); 
        		int endPage = Math.min(selectedPage+pageOffset, maxPageNumber); 
        		previousPage = 0;
        		nextPage = 0;
        		
        		if (selectedPage != startPage) {
        			previousPage = selectedPage-1;
        		}
        		if (selectedPage != endPage) {
        			nextPage = selectedPage+1;
        		}
                    
		        for (int i=startPage; i<=endPage; i++) {
		        	pages.add(i);
		        } 
		        
		        NodeList results = doc.getElementsByTagName("R");
		        // iterate the results
		        for (int i = 0; i < results.getLength(); i++) {
		           Element result = (Element) results.item(i);
		           
		           NodeList descriptionNode = result.getElementsByTagName("S");
		           Element line = (Element) descriptionNode.item(0);
		           String description = StringEscapeUtils.unescapeHtml4(getCharacterDataFromElement(line));
		           
		           NodeList titleNode = result.getElementsByTagName("T");
		           line = (Element) titleNode.item(0);
		           String title = StringEscapeUtils.unescapeHtml4(getCharacterDataFromElement(line));
		           
		           NodeList linkNode = result.getElementsByTagName("U");
		           line = (Element)  linkNode.item(0);
		           String link = getCharacterDataFromElement(line);
		           ResultData res = new ResultData (title, description, link);
		           elencoRisultati.add(res);
		           
		        }
		        
		        NodeList clusters = doc.getElementsByTagName("PV");
		        // iterate the cluster
		        for (int i = 0; i < clusters.getLength(); i++) {
		        	
		           Element cluster = (Element) clusters.item(i);

		           String title = cluster.getAttribute("V");
		           String count = cluster.getAttribute("C");
		           
		           ResultCluster clu = new ResultCluster(title, count);
		           elencoCluster.add(clu);
		        }
		        
		    }
		    catch (Exception e) {
		        e.printStackTrace();
		    }
	}
	
	  private static String getCharacterDataFromElement(Element e) {
		    Node child = e.getFirstChild();
		    if (child instanceof CharacterData) {
		       CharacterData cd = (CharacterData) child;
		       return cd.getData();
		    }
		    return "?";
		  }
	  
		public String getSearchedString() {
			return searchedString;
		}
		
		public void setSearchedString(String searched) {
			searchedString = searched;
		}
		
		public String getResult() {
			return result;
		}
		
		public void setResult(String resultP) {
			result = resultP;
		}
		
		public List<ResultData> getElencoRisultati() {
			return elencoRisultati;
		}

		public void setElencoRisultati(List<ResultData> elencoRisultati) {
			this.elencoRisultati = elencoRisultati;
		}
		
		public String getNumRisultati() {
			return numRisultati;
		}

		public void setNumRisultati(String numRisultati) {
			this.numRisultati = numRisultati;
		}
		
		public List<ResultCluster> getElencoCluster() {
			return elencoCluster;
		}

		public void setElencoCluster(List<ResultCluster> elencoCluster) {
			this.elencoCluster = elencoCluster;
		}
		
		public List<Integer> getPages() {
			return pages;
		}

		public void setPages(List<Integer> pages) {
			this.pages = pages;
		}
		
		public Integer getSelectedPage() {
			return selectedPage;
		}

		public void setSelectedPage(Integer selectedPage) {
			this.selectedPage = selectedPage;
		}

		public String getSelectedCluster() {
			return selectedCluster;
		}

		public void setSelectedCluster(String selectedCluster) {
			this.selectedCluster = selectedCluster;
		}

		public boolean isClusterSearch() {
			return isClusterSearch;
		}

		public void setClusterSearch(boolean isClusterSearch) {
			this.isClusterSearch = isClusterSearch;
		}

		public Integer getPreviousPage() {
			return previousPage;
		}

		public void setPreviousPage(Integer previousPage) {
			this.previousPage = previousPage;
		}

		public Integer getNextPage() {
			return nextPage;
		}

		public void setNextPage(Integer nextPage) {
			this.nextPage = nextPage;
		}
		
}
