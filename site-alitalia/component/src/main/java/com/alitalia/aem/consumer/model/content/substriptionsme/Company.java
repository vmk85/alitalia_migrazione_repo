package com.alitalia.aem.consumer.model.content.substriptionsme;

public class Company {
	private String companyName;
	private String piva;
	private String address;
	private String state_prov;
	private String country;
	private String city;
	private String zipCode;
	private String companyPhoneNumber, companyAreaPrefix, companyNationalPrefix;
	private String companyEmail;
	private String companyFax;
	private String websiteUrl;
	
	
	public String getState_prov() {
		return state_prov;
	}
	public void setState_prov(String state_prov) {
		this.state_prov = state_prov;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getPiva() {
		return piva;
	}
	public void setPiva(String piva) {
		this.piva = piva;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getWebsiteUrl() {
		return websiteUrl;
	}
	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCompanyPhoneNumber() {
		return companyPhoneNumber;
	}
	public void setCompanyPhoneNumber(String companyPhone_number) {
		this.companyPhoneNumber = companyPhone_number;
	}
	public String getCompanyAreaPrefix() {
		return companyAreaPrefix;
	}
	public void setCompanyAreaPrefix(String companyArea_prefix) {
		this.companyAreaPrefix = companyArea_prefix;
	}
	public String getCompanyNationalPrefix() {
		return companyNationalPrefix;
	}
	public void setCompanyNationalPrefix(String companyNationalPrefix) {
		this.companyNationalPrefix = companyNationalPrefix;
	}
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public String getCompanyFax() {
		return companyFax;
	}
	public void setCompanyFax(String companyFax) {
		this.companyFax = companyFax;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((companyAreaPrefix == null) ? 0 : companyAreaPrefix.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((companyEmail == null) ? 0 : companyEmail.hashCode());
		result = prime * result + ((companyFax == null) ? 0 : companyFax.hashCode());
		result = prime * result + ((companyNationalPrefix == null) ? 0 : companyNationalPrefix.hashCode());
		result = prime * result + ((companyPhoneNumber == null) ? 0 : companyPhoneNumber.hashCode());
		result = prime * result + ((piva == null) ? 0 : piva.hashCode());
		result = prime * result + ((state_prov == null) ? 0 : state_prov.hashCode());
		result = prime * result + ((websiteUrl == null) ? 0 : websiteUrl.hashCode());
		result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (companyAreaPrefix == null) {
			if (other.companyAreaPrefix != null)
				return false;
		} else if (!companyAreaPrefix.equals(other.companyAreaPrefix))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (companyEmail == null) {
			if (other.companyEmail != null)
				return false;
		} else if (!companyEmail.equals(other.companyEmail))
			return false;
		if (companyFax == null) {
			if (other.companyFax != null)
				return false;
		} else if (!companyFax.equals(other.companyFax))
			return false;
		if (companyNationalPrefix == null) {
			if (other.companyNationalPrefix != null)
				return false;
		} else if (!companyNationalPrefix.equals(other.companyNationalPrefix))
			return false;
		if (companyPhoneNumber == null) {
			if (other.companyPhoneNumber != null)
				return false;
		} else if (!companyPhoneNumber.equals(other.companyPhoneNumber))
			return false;
		if (piva == null) {
			if (other.piva != null)
				return false;
		} else if (!piva.equals(other.piva))
			return false;
		if (state_prov == null) {
			if (other.state_prov != null)
				return false;
		} else if (!state_prov.equals(other.state_prov))
			return false;
		if (websiteUrl == null) {
			if (other.websiteUrl != null)
				return false;
		} else if (!websiteUrl.equals(other.websiteUrl))
			return false;
		if (zipCode == null) {
			if (other.zipCode != null)
				return false;
		} else if (!zipCode.equals(other.zipCode))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Company [companyName=" + companyName + ", piva=" + piva + ", address=" + address + ", state_prov="
				+ state_prov + ", country=" + country + ", city=" + city + ", zipCode=" + zipCode + ", phone_number="
				+ companyPhoneNumber + ", area_prefix=" + companyAreaPrefix + ", nationalPrefix=" + companyNationalPrefix + ", email="
				+ companyEmail + ", fax=" + companyFax + ", websiteUrl=" + websiteUrl + "]";
	}
	
}
