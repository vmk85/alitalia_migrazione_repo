package com.alitalia.aem.consumer.model.content.specialpages;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

@Model(adaptables = { SlingHttpServletRequest.class })
public class RichiestaAssociazioneUCardModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;

	private RichiestaAssociazioneUCardData data;
	private boolean showCountry;
	private boolean multipleCountries;
	private List<String> countryList;

	@PostConstruct
	protected void initModel() {
		logger.debug("[RichiestaAssociazioneUCardData] initModel");

		try {
			super.initBaseModel(slingHttpServletRequest);
			
			// retrieve and remove data from session if any
			data = (RichiestaAssociazioneUCardData) slingHttpServletRequest.getSession().getAttribute(RichiestaAssociazioneUCardData.NAME);
			slingHttpServletRequest.getSession().removeAttribute(RichiestaAssociazioneUCardData.NAME);
			
			String[] countryArray = RichiestaAssociazioneUCardData.extractCountryList(
					marketCode.toUpperCase()
					, configuration.getStringProperty(AlitaliaConfigurationHolder.MM_CORPORATE_COUNTRIES_MAP));
			showCountry = countryArray != null && countryArray.length > 0;
			multipleCountries = countryArray != null && countryArray.length > 1;
			if(countryArray != null)
				countryList = Arrays.asList(countryArray);
			
		} catch (Exception e) {
			logger.error("Unexpected error: {}", e);
		}
	}

	public RichiestaAssociazioneUCardData getData() {
		return data;
	}

	public void setData(RichiestaAssociazioneUCardData data) {
		this.data = data;
	}

	public boolean isShowCountry() {
		return showCountry;
	}

	public boolean isMultipleCountries() {
		return multipleCountries;
	}

	public List<String> getCountryList() {
		return countryList;
	}

}
