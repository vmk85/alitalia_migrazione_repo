package com.alitalia.aem.consumer.bookingaward.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;

@Model(adaptables={ SlingHttpServletRequest.class })
public class BookingAwardCalendar extends BookingAwardGenericController {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	private String infoMessage;
	private String errorUrl;
	private boolean error;
	private boolean roundTrip;
	private SearchElement outboundSearchElement;
	
	

	@PostConstruct
	protected void initModel() {
		errorUrl = request.getResource().getResourceResolver().map(AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false)) + configuration.getBookingAwardFailurePage();
		try {
			initBaseModel(request);
			if (ctx != null) {
				roundTrip = ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP;
				outboundSearchElement = ctx.searchElements.get(0);
				
				String infoMessageKey = "booking.award.calendar.testoInformativo.text";
				String infoMessageValue = i18n.get(infoMessageKey); 
				infoMessage = !infoMessageValue.equals(infoMessageKey) ? infoMessageValue : "";
			}
			
			
		} catch (Exception e) {
			logger.error("Unexpected error", e);
			error = true;
		}
	}
	

	public String getInfoMessage() {
		return infoMessage;
	}

	public String getErrorUrl() {
		return errorUrl;
	}
	
	public boolean getError() {
		return error;
	}

	public boolean isRoundTrip() {
		return roundTrip;
	}
	
	
	
	public SearchElement getOutboundSearchElement() {
		return outboundSearchElement;
	}
}
