package com.alitalia.aem.consumer.model.content.specialpages;

import com.alitalia.aem.consumer.utils.FrequentFlyer;

public class MillemigliaRegistratiData {

	public static final String NAME = "MillemigliaRegistratiData";

	public static final String[] ADDRESSES = new String[] { "home", "office" };

	private String name;
	private String surname;
	private String dayOfBirth;
	private String monthOfBirth;
	private String yearOfBirth;
	private String gender;
	private String profession;
	private String postalAddress;
	private String company;
	private String address;
	private String cap;
	private String city;
	private String nation;
	private String province;
	private String email;
	private String nationalPrefix;
	private String areaPrefix;
	private String phonePrefix;
	private String countryNumber;
	private String number;
	private String telephoneType;
	private String newsletter;
	private String method;
	private String sms;
	private String language;
	private String consent;
	private String place;
	private String meal;

	private FrequentFlyer frequentFlyer;
	private String checkMillemiglia;
	private String checkYoung;
	private boolean viewSms;
	private String trackingCod;
	private String mmcode;
	
	private String error;
	private Integer state;


	private String birthDate;
	 // Strong Auth Fields
    private boolean  flagVerifica_OTP_DS_OK = false;

    private String idProfilo;
	private String username;
    private boolean flagVerificaUserName_OK = false;
	private String password;
    private boolean flagVerificaPassword_OK = false;
    private String certifiedEmailAccount;
    private boolean flagVerificaEmailUser_OK = false;
	private String inputSecretQuestion;
	private String rispostaSegreta;
    private boolean flagVerificaRisposta_OK = false;
    private String certifiedCountryNumber; // Prefisso Internazionale. Es : 0039
	private String certifiedPhoneNumber;   //
    private boolean flagVerificaCellulare_OK = false;

    private String SocialUser_ID;

    private String confPassword;
    private boolean flagVerificaPasswordConfirmed_OK = false;
    private boolean flagVerificaDomanda_OK = false;


	public String getPhonePrefix() {
		return phonePrefix;
	}

	public void setPhonePrefix(String phonePrefix) {
		this.phonePrefix = phonePrefix;
	}

	public String getCountryNumber() {
		return countryNumber;
	}

	public void setCountryNumber(String countryNumber) {
		this.countryNumber = countryNumber;
	}

	public String getBirthDate() {
		return yearOfBirth + "-" + monthOfBirth + "-" + dayOfBirth;
	}


	public String getCertifiedCountryNumber() {
		return certifiedCountryNumber;
	}

	public void setCertifiedCountryNumber(String certifiedCountryNumber) {
		this.certifiedCountryNumber = certifiedCountryNumber;
	}

	public String getCertifiedPhoneNumber() {
		return certifiedPhoneNumber;
	}

	public void setCertifiedPhoneNumber(String certifiedPhoneNumber) {
		this.certifiedPhoneNumber = certifiedPhoneNumber;
	}

	public String getSocialUser_ID() {
		return SocialUser_ID;
	}

	public void setSocialUser_ID(String socialUser_ID) {
		SocialUser_ID = socialUser_ID;
	}

	public void setIdProfilo(String idProfilo) {
		this.idProfilo = idProfilo;
	}

	public String getIdProfilo() {
		return idProfilo;
	}

	public String getInputSecretQuestion() {
		return inputSecretQuestion;
	}

	public String getRispostaSegreta() {
		return rispostaSegreta;
	}

	public void setInputSecretQuestion(String inputSecretQuestion) {
		this.inputSecretQuestion = inputSecretQuestion;
	}

	public void setRispostaSegreta(String rispostaSegreta) {
		this.rispostaSegreta = rispostaSegreta;
	}

	public boolean isFlagVerificaUserName_OK() {
		return flagVerificaUserName_OK;
	}

	public boolean isFlagVerificaPassword_OK() {
		return flagVerificaPassword_OK;
	}

	public boolean isFlagVerificaPasswordConfirmed_OK() {
		return flagVerificaPasswordConfirmed_OK;
	}

	public boolean isFlagVerificaCellulare_OK() {
		return flagVerificaCellulare_OK;
	}

	public boolean isFlagVerificaDomanda_OK() {
		return flagVerificaDomanda_OK;
	}

	public boolean isFlagVerificaRisposta_OK() {
		return flagVerificaRisposta_OK;
	}

	public boolean isFlagVerificaEmailUser_OK() {
		return flagVerificaEmailUser_OK;
	}

	public boolean isFlagVerifica_OTP_DS_OK() {
		return flagVerifica_OTP_DS_OK;
	}

	public void setFlagVerificaUserName_OK(boolean flagVerificaUserName_OK) {
		this.flagVerificaUserName_OK = flagVerificaUserName_OK;
	}

	public void setFlagVerificaPassword_OK(boolean flagVerificaPassword_OK) {
		this.flagVerificaPassword_OK = flagVerificaPassword_OK;
	}

	public void setFlagVerificaPasswordConfirmed_OK(boolean flagVerificaPasswordConfirmed_OK) {
		this.flagVerificaPasswordConfirmed_OK = flagVerificaPasswordConfirmed_OK;
	}

	public void setFlagVerificaCellulare_OK(boolean flagVerificaCellulare_OK) {
		this.flagVerificaCellulare_OK = flagVerificaCellulare_OK;
	}

	public void setFlagVerificaDomanda_OK(boolean flagVerificaDomanda_OK) {
		this.flagVerificaDomanda_OK = flagVerificaDomanda_OK;
	}

	public void setFlagVerificaRisposta_OK(boolean flagVerificaRisposta_OK) {
		this.flagVerificaRisposta_OK = flagVerificaRisposta_OK;
	}

	public void setFlagVerificaEmailUser_OK(boolean flagVerificaEmailUser_OK) {
		this.flagVerificaEmailUser_OK = flagVerificaEmailUser_OK;
	}

	public void setFlagVerifica_OTP_DS_OK(boolean flagVerifica_OTP_DS_OK) {
		this.flagVerifica_OTP_DS_OK = flagVerifica_OTP_DS_OK;
	}


	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCertifiedEmailAccount() {
		return certifiedEmailAccount;
	}

	public void setCertifiedEmailAccount(String certifiedEmailAccount) {
		this.certifiedEmailAccount = certifiedEmailAccount;
	}


	public String getDayOfBirth() {
		return dayOfBirth;
	}

	public void setDayOfBirth(String dayOfBirth) {
		this.dayOfBirth = dayOfBirth;
	}

	public String getMonthOfBirth() {
		return monthOfBirth;
	}

	public void setMonthOfBirth(String monthOfBirth) {
		this.monthOfBirth = monthOfBirth;
	}

	public String getYearOfBirth() {
		return yearOfBirth;
	}

	public void setYearOfBirth(String yearOfBirth) {
		this.yearOfBirth = yearOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNationalPrefix() {
		return nationalPrefix;
	}

	public void setNationalPrefix(String prefix) {
		this.nationalPrefix = prefix;
	}

	public String getAreaPrefix() {
		return areaPrefix;
	}

	public void setAreaPrefix(String prefix) {
		this.areaPrefix = prefix;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getTelephoneType() {
		return telephoneType;
	}

	public void setTelephoneType(String telephoneType) {
		this.telephoneType = telephoneType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;

	}

	public String getConfPassword() {
		return confPassword;
	}

	public void setConfPassword(String confPassword) {
		this.confPassword = confPassword;

	}

	public String getConsent() {
		return consent;
	}

	public void setConsent(String consent) {
		this.consent = consent;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getNewsletter() {
		return newsletter;
	}

	public void setNewsletter(String newsletter) {
		this.newsletter = newsletter;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSms() {
		return sms;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getMeal() {
		return meal;
	}

	public void setMeal(String meal) {
		this.meal = meal;
	}

	public FrequentFlyer getFrequentFlyer() {
		return frequentFlyer;
	}

	public void setFrequentFlyer(FrequentFlyer frequentFlyer) {
		this.frequentFlyer = frequentFlyer;
	}

	public String getCheckMillemiglia() {
		return checkMillemiglia;
	}

	public void setCheckMillemiglia(String checkMillemiglia) {
		this.checkMillemiglia = checkMillemiglia;
	}

	public String getCheckYoung() {
		return checkYoung;
	}

	public void setCheckYoung(String checkYoung) {
		this.checkYoung = checkYoung;
	}
	
	public String getMmcode() {
		return mmcode;
	}

	public void setMmcode(String mmcode) {
		this.mmcode = mmcode;
	}

	@Override
	public String toString() {
		String output = "MillemigliaRegistratiData [name=" + name
				+ ", surname=" + surname + ", day of birth = " + dayOfBirth
				+ ", month of birth = " + monthOfBirth + ", year of birth = "
				+ yearOfBirth + ", gender = " + gender + ", profession = "
				+ profession + ", postalAddress = " + postalAddress
				+ ", company = " + company + ", address = " + address
				+ ", cap = " + cap + ", city = " + city + ", nation = "
				+ nation + ", state or province = " + province + ", email = "
				+ email + ", nationalPrefix = " + nationalPrefix
				+ ", areaPrefix = " + areaPrefix + ", number = " + number
				+ ", telephone type = " + telephoneType + ", newsletter = "
				+ newsletter + ", method = " + method + ", language = "
				+ language + ", consent = " + consent+", tracking code "+trackingCod;

		if (state.equals("2") || state.equals("3")) {
			output = output + ", newsletter = " + newsletter + ", method = "
					+ method + ", language = " + language + ", sms = " + sms
					+ ", place = " + place + ", meal = " + meal
					+ frequentFlyer.toString() + ", checkMillemiglia = "
					+ checkMillemiglia + ", checkYoung = " + checkYoung;
		}
		
		output = output + ", state = " + state + "]";
		return output;
	}

	public boolean isViewSms() {
		return viewSms;
	}

	public void setViewSms(boolean viewSms) {
		this.viewSms = viewSms;
	}

	public String getTrackingCod() {
		return trackingCod;
	}

	public void setTrackingCod(String trackingcod) {
		this.trackingCod = trackingcod;
	}

}
