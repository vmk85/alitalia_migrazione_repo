package com.alitalia.aem.consumer.utils;

import java.util.Locale;

import org.apache.sling.api.resource.Resource;

public class LocalizationUtils {
	
	public static Locale getLocaleByResource(Resource resource) {
		return AlitaliaUtils.findResourceLocale(resource);
	}
	
	@Deprecated
	public static Locale getLocaleByResource(Resource resource, String defaultLanguage) {
		// FIXME [booking] rimuovere metodo deprecato
		return AlitaliaUtils.findResourceLocale(resource);
	}
	
}
