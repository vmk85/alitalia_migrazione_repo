package com.alitalia.aem.consumer.exception;

public class NoUserLoggedInException extends Exception {

	public NoUserLoggedInException() {
		// TODO Auto-generated constructor stub
	}

	public NoUserLoggedInException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public NoUserLoggedInException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public NoUserLoggedInException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public NoUserLoggedInException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
