package com.alitalia.aem.consumer.booking;

public enum BookingFlightFilterTypeEnum {
	
	DEFAULT(""),
	PRICE("price"),
	FLIGHT_TIME("flightTime"),
	DEPARTURE_TIME("departureTime"),
	ARRIVAL_TIME("arrivalTime"),
	
	MORNING("morningFilter"),
	AFTERNOON("afternoonFilter"),
	EVENING("eveningFilter");
	
    private final String value;
    
    BookingFlightFilterTypeEnum(String v) {
        value = v;
    }
    
    public String value() {
    	return this.value;
    }
    
    public static BookingFlightFilterTypeEnum fromValue(String v) {
    	if(v == null) 
    		return BookingFlightFilterTypeEnum.DEFAULT;
        for (BookingFlightFilterTypeEnum c: BookingFlightFilterTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
