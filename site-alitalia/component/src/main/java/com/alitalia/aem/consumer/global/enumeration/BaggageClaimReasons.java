package com.alitalia.aem.consumer.global.enumeration;

import java.util.ArrayList;

public enum BaggageClaimReasons {
	
	CANCELED_FLIGHT("canceledflight"),
	LOST_LUGGAGE("lostluggage"),
	OTHER_REASON("otherreason");
	
	private final String value;

	BaggageClaimReasons(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	public String getI18nValue() {
		return "specialpage.reason." + value + ".label";
	}
	
	public static ArrayList<String> listValues(){
		ArrayList<String> result = new ArrayList<String>();
		for (BaggageClaimReasons c: BaggageClaimReasons.values()) {
			result.add(c.value);
		}
		return result;
	}

	public static BaggageClaimReasons fromValue(String v) {
		for (BaggageClaimReasons c: BaggageClaimReasons.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
