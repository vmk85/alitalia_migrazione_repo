package com.alitalia.aem.consumer.booking.model.extrabaggage;



//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
 ///#region references
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
///#endregion


/** 
 Sabre Request Model for Segment
*/
public class Segment
{
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [JsonProperty(PropertyName = "@type")] public string type {get;set;}
	private String type;
	public final String gettype()
	{
		return type;
	}
	public final void settype(String value)
	{
		type = value;
	}
	private SegmentOfferInformation segmentOfferInformation;
	public final SegmentOfferInformation getsegmentOfferInformation()
	{
		return segmentOfferInformation;
	}
	public final void setsegmentOfferInformation(SegmentOfferInformation value)
	{
		segmentOfferInformation = value;
	}
	private int duration;
	public final int getduration()
	{
		return duration;
	}
	public final void setduration(int value)
	{
		duration = value;
	}
	private String cabinClass;
	public final String getcabinClass()
	{
		return cabinClass;
	}
	public final void setcabinClass(String value)
	{
		cabinClass = value;
	}
	private String equipment;
	public final String getequipment()
	{
		return equipment;
	}
	public final void setequipment(String value)
	{
		equipment = value;
	}
	private Flight flight;
	public final Flight getflight()
	{
		return flight;
	}
	public final void setflight(Flight value)
	{
		flight = value;
	}
	private String origin;
	public final String getorigin()
	{
		return origin;
	}
	public final void setorigin(String value)
	{
		origin = value;
	}
	private String destination;
	public final String getdestination()
	{
		return destination;
	}
	public final void setdestination(String value)
	{
		destination = value;
	}
	private String departure;
	public final String getdeparture()
	{
		return departure;
	}
	public final void setdeparture(String value)
	{
		departure = value;
	}
	private String arrival;
	public final String getarrival()
	{
		return arrival;
	}
	public final void setarrival(String value)
	{
		arrival = value;
	}
	private String bookingClass;
	public final String getbookingClass()
	{
		return bookingClass;
	}
	public final void setbookingClass(String value)
	{
		bookingClass = value;
	}
	private int layoverDuration;
	public final int getlayoverDuration()
	{
		return layoverDuration;
	}
	public final void setlayoverDuration(int value)
	{
		layoverDuration = value;
	}
	private String fareBasis;
	public final String getfareBasis()
	{
		return fareBasis;
	}
	public final void setfareBasis(String value)
	{
		fareBasis = value;
	}
}