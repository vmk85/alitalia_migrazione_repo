package com.alitalia.aem.consumer.model.content.substriptionsme;

public class AdministratorAccount {
	private String name;
	private String surname;
	private String year, month, day;
	private String qualification;
	private String administratorPhoneNumber, administratorAreaPrefix, administratorNationalPrefix;
	private String administratorEmail;
	private String administratorFax;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getAdministratorPhoneNumber() {
		return administratorPhoneNumber;
	}
	public void setAdministratorPhoneNumber(String administratorPhone_number) {
		this.administratorPhoneNumber = administratorPhone_number;
	}
	public String getAdministratorAreaPrefix() {
		return administratorAreaPrefix;
	}
	public void setAdministratorAreaPrefix(String administratorArea_prefix) {
		this.administratorAreaPrefix = administratorArea_prefix;
	}
	public String getAdministratorNationalPrefix() {
		return administratorNationalPrefix;
	}
	public void setAdministratorNationalPrefix(String administratorNationalPrefix) {
		this.administratorNationalPrefix = administratorNationalPrefix;
	}
	public String getAdministratorEmail() {
		return administratorEmail;
	}
	public void setAdministratorEmail(String administratorEmail) {
		this.administratorEmail = administratorEmail;
	}
	public String getAdministratorFax() {
		return administratorFax;
	}
	public void setAdministratorFax(String administratorFax) {
		this.administratorFax = administratorFax;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((administratorAreaPrefix == null) ? 0 : administratorAreaPrefix.hashCode());
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + ((administratorEmail == null) ? 0 : administratorEmail.hashCode());
		result = prime * result + ((administratorFax == null) ? 0 : administratorFax.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((administratorNationalPrefix == null) ? 0 : administratorNationalPrefix.hashCode());
		result = prime * result + ((administratorPhoneNumber == null) ? 0 : administratorPhoneNumber.hashCode());
		result = prime * result + ((qualification == null) ? 0 : qualification.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdministratorAccount other = (AdministratorAccount) obj;
		if (administratorAreaPrefix == null) {
			if (other.administratorAreaPrefix != null)
				return false;
		} else if (!administratorAreaPrefix.equals(other.administratorAreaPrefix))
			return false;
		if (day == null) {
			if (other.day != null)
				return false;
		} else if (!day.equals(other.day))
			return false;
		if (administratorEmail == null) {
			if (other.administratorEmail != null)
				return false;
		} else if (!administratorEmail.equals(other.administratorEmail))
			return false;
		if (administratorFax == null) {
			if (other.administratorFax != null)
				return false;
		} else if (!administratorFax.equals(other.administratorFax))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (administratorNationalPrefix == null) {
			if (other.administratorNationalPrefix != null)
				return false;
		} else if (!administratorNationalPrefix.equals(other.administratorNationalPrefix))
			return false;
		if (administratorPhoneNumber == null) {
			if (other.administratorPhoneNumber != null)
				return false;
		} else if (!administratorPhoneNumber.equals(other.administratorPhoneNumber))
			return false;
		if (qualification == null) {
			if (other.qualification != null)
				return false;
		} else if (!qualification.equals(other.qualification))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		if (year == null) {
			if (other.year != null)
				return false;
		} else if (!year.equals(other.year))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AdministratorAccount [name=" + name + ", surname=" + surname + ", year=" + year + ", month=" + month
				+ ", day=" + day + ", qualification=" + qualification + ", phone_number=" + administratorPhoneNumber
				+ ", area_prefix=" + administratorAreaPrefix + ", nationalPrefix=" + administratorNationalPrefix + ", email=" + administratorEmail + ", fax="
				+ administratorFax + "]";
	}
	
}
