package com.alitalia.aem.consumer.booking.analytics.filler;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;

import com.alitalia.aem.consumer.booking.analytics.MetaInfo;

public class BookingMetaInfoToJsObj extends ToJsObj implements  MetaInfoToJsObj {
	
	private static final String HYPHEN_FLIGHT_DATE_FORMAT = "yyyy-MM-dd";
	private static final String HYPHEN_EU_FLIGHT_DATE_FORMAT = "dd-MM-yy";
	private static final String EU_FLIGHT_DATE_FORMAT = "dd/MM/yyyy";
	private static final String EN_FLIGHT_DATE_FORMAT = "MM/dd/yyyy";
	private static final String TIME_FORMAT_HH_MM_SS = "kk:mm:ss";
	
	private DecimalFormat format;
	private SimpleDateFormat dateFormat_eu;
	private SimpleDateFormat dateFormat_en;
	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateFormat_eu_hyphen;
	private SimpleDateFormat timeFormat;

	public BookingMetaInfoToJsObj() {
		initFormats();
	}
	
	private void initFormats() {
		
		format = new DecimalFormat("#0.00");
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setCurrencySymbol(""); 
		format.setDecimalFormatSymbols(otherSymbols);
		format.setGroupingUsed(false);
		format.setPositivePrefix("");
		format.setNegativePrefix("");
		
		dateFormat_eu = new SimpleDateFormat(EU_FLIGHT_DATE_FORMAT);
		dateFormat_eu.setLenient(false);
		
		dateFormat_en = new SimpleDateFormat(EN_FLIGHT_DATE_FORMAT);
		dateFormat_en.setLenient(false);
		
		dateFormat = new SimpleDateFormat(HYPHEN_FLIGHT_DATE_FORMAT);
		dateFormat.setLenient(false);
		dateFormat_eu_hyphen = new SimpleDateFormat(HYPHEN_EU_FLIGHT_DATE_FORMAT);
		dateFormat_eu_hyphen.setLenient(false);
		
		timeFormat = new SimpleDateFormat(TIME_FORMAT_HH_MM_SS);
		timeFormat.setLenient(false);
		
	}
	
	

	@Override
	public String toJSObj(MetaInfo info, boolean trailingSemicolon) {
		String js = "{";
		js = addJsObjElem(js, "BbxSessionId", info.bbxSessionId, true);
		js = addJsObjElem(js, "BbxSolutionSet", info.bbxSolutionSet, true);
		js = addJsObjElem(js, "SabreSessionId", info.sabreSessionId, true);
		switch(info.step){
		case 1:
			js = addAndEncodeJsObjElem(js, "WT.Pn", info.Pn, true);
			js = addJsObjElem(js, "WT.tx_u", info.tx_u, true);
			js = addJsObjElem(js, "DCSext.ItinType", info.ItinType, true);
			js = addAndEncodeJsObjElem(js, "DCSext.tratta", info.tratta, true);
			js = addAndEncodeJsObjElem(js, "DCSext.Boapt", info.boApt, true);
			js = addAndEncodeJsObjElem(js, "DCSext.Arapt", info.arApt, true);
			js = addJsObjElem(js, "DCSext.NumWE", info.NumWE, true);
			js = addJsObjElem(js, "DCSext.departureDate_eu", formatIfNotNull(info.departureDate_eu, dateFormat_eu_hyphen), true);
			js = addJsObjElem(js, "DCSext.returnDate_eu", formatIfNotNull(info.returnDate_eu, dateFormat_eu_hyphen), true);
			js = addJsObjElem(js, "DCSext.departureDate", formatIfNotNull(info.departureDate, dateFormat), true);
			js = addJsObjElem(js, "DCSext.returnDate", formatIfNotNull(info.returnDate, dateFormat), true);
			js = addJsObjElem(js, "WT.tx_e", info.tx_e, true);
			js = addJsObjElem(js, "WT.z_sitecode", info.z_sitecode, true);
			js = addJsObjElem(js, "WT.z_tx_cur", info.z_tx_cur, true);
			js = addJsObjElem(js, "DCSext.Time", info.Time, true);
			js = addJsObjElem(js, "WT.pn_gr", info.pn_gr, true);
			js = addJsObjElem(js, "WT.pn_fa", info.pn_fa, true);
			js = addJsObjElem(js, "WT.pc", info.pc, true);
			js = addJsObjElem(js, "WT.pn_sc", info.pn_sc, true);
			js = addJsObjElem(js, "WT.pn_sku", info.pn_sku, true);
			js = addJsObjElem(js, "DCSext.ServClass", info.ServClass, true);
			js = addJsObjElem(js, "WT.si_n", info.si_n, true);
			js = addJsObjElem(js, "DCSext.BStep", info.revenuePhase, true);
			js = addJsObjElem(js, "DCSext.BFormItem", info.BFormItem, true);
			js = addJsObjElem(js, "WT.si_x", info.revenuePhase, true);
			js = addJsObjElem(js, "DCSext.NumAd", info.numAdults, true);
			js = addJsObjElem(js, "DCSext.NumYg", info.numYoung, true);
			js = addJsObjElem(js, "DCSext.NumCh", info.numChildren, true);
			js = addJsObjElem(js, "DCSext.NumIn", info.numInfant, true);
			js = addJsObjElem(js, "DCSext.Network", info.NetworkExt, true);
			js = addJsObjElem(js, "DCSext.DayOfWeekA", info.DayOfWeekA, true);
			js = addJsObjElem(js, "DCSext.DayOfWeekR", info.DayOfWeekR, true);
			js = addJsObjElem(js, "DCSext.DeltaBoToday", info.deltaBoToday, true);
			js = addJsObjElem(js, "DCSext.DeltaBoAr", info.deltaBoAr, true);
			js = addJsObjElem(js, "DCSext.SearchType", info.SearchType, false);
			break;
		case 2:
			js = addJsObjElem(js, "DCSext.ItinType", info.ItinType, true);
			js = addJsObjElem(js, "WT.si_n", info.si_n, true);
			js = addJsObjElem(js, "DCSext.BStep", info.revenuePhase, true);
			js = addJsObjElem(js, "WT.si_x", info.revenuePhase, true);
			js = addAndEncodeJsObjElem(js, "WT.Pn", info.Pn, true);
			js = addJsObjElem(js, "DCSext.departureFlight", info.departureFlight, true);
			js = addJsObjElem(js, "DCSext.returnFlight", info.returnFlight, true);
			js = addJsObjElem(js, "WT.tx_u", info.tx_u, true);
			js = addAndEncodeJsObjElem(js, "DCSext.tratta", info.tratta, true);
			js = addJsObjElem(js, "DCSext.FareBasis", info.FareBasis, true);
			js = addJsObjElem(js, "DCSext.SolutionBrandA", info.SolutionBrandA, true);
			js = addJsObjElem(js, "DCSext.SolutionBrandR", info.SolutionBrandR, true);
			js = addJsObjElem(js, "DCSext.departureHouro", info.departureHouro, true);
			js = addJsObjElem(js, "DCSext.returnHouro", info.returnHouro, true);
			js = addJsObjElem(js, "DCSext.departureHourd", info.departureHourd, true);
			js = addJsObjElem(js, "DCSext.returnHourd", info.returnHourd, true);
			js = addJsObjElem(js, "WT.tx_e", info.tx_e, true);
			js = addJsObjElem(js, "WT.z_sitecode", info.z_sitecode, true);
			js = addJsObjElem(js, "WT.z_tx_cur", info.z_tx_cur, true);
			js = addJsObjElem(js, "DCSext.BFormItem", info.BFormItem, true);
			js = addJsObjElem(js, "WT.pn_gr", info.pn_gr, true);
			js = addJsObjElem(js, "WT.pn_fa", info.pn_fa, true);
			js = addJsObjElem(js, "WT.pc", info.pc, true);
			js = addJsObjElem(js, "WT.pn_sc", info.pn_sc, true);
			js = addJsObjElem(js, "WT.pn_sku", info.pn_sku, true);
			js = addJsObjElem(js, "DCSext.checknewsletter", info.checknewsletter, false);

			break;
		case 3:
			js = addJsObjElem(js, "DCSext.BFormItem", info.BFormItem, true);
			js = addJsObjElem(js, "DCSext.ItinType", info.ItinType, true);
			js = addJsObjElem(js, "WT.si_n", info.si_n, true);
			js = addJsObjElem(js, "DCSext.BStep", info.revenuePhase, true);
			js = addJsObjElem(js, "WT.si_x", info.revenuePhase, false);
			break;
		case 4:
			js = addJsObjElem(js, "DCSext.ItinType", info.ItinType, true);
			js = addJsObjElem(js, "DCSext.SolutionBrandA", info.SolutionBrandA, true);
			js = addJsObjElem(js, "DCSext.SolutionBrandR", info.SolutionBrandR, true);
			js = addJsObjElem(js, "DCSext.BFormItem", info.BFormItem, true);
			js = addJsObjElem(js, "WT.si_n", info.si_n, true);
			js = addJsObjElem(js, "DCSext.BStep", info.revenuePhase, true);
			js = addJsObjElem(js, "WT.si_x", info.revenuePhase, true);
			js = addAndEncodeJsObjElem(js, "WT.dcsvid", info.dcsvid, true);
			js = addJsObjElem(js, "DCSext.phonetel", info.phonetel, true);
			js = addAndEncodeJsObjElem(js, "DCSext.contact_name", info.contact_name, false);
			break;
		case 5:
			js = addAndEncodeJsObjElem(js, "WT.Pn", info.Pn, true);
			js = addJsObjElem(js, "DCSext.departureFlight", info.departureFlight, true);
			js = addJsObjElem(js, "DCSext.returnFlight", info.returnFlight, true);
			js = addJsObjElem(js, "WT.tx_u", info.tx_u, true);
			js = addAndEncodeJsObjElem(js, "DCSext.tratta", info.tratta, true);
			js = addJsObjElem(js, "DCSext.FareBasis", info.FareBasis, true);
			js = addJsObjElem(js, "DCSext.NumWE", info.NumWE, true);
			js = addJsObjElem(js, "DCSext.ItinType", info.ItinType, true);
			js = addJsObjElem(js, "DCSext.departureDate_eu", formatIfNotNull(info.departureDate_eu, dateFormat_eu_hyphen), true);
			js = addJsObjElem(js, "DCSext.returnDate_eu", formatIfNotNull(info.returnDate_eu, dateFormat_eu_hyphen), true);
			js = addJsObjElem(js, "DCSext.departureDate", formatIfNotNull(info.departureDate, dateFormat), true);
			js = addJsObjElem(js, "DCSext.returnDate", formatIfNotNull(info.returnDate, dateFormat), true);
			js = addJsObjElem(js, "DCSext.DateOfTravel", formatIfNotNull(info.DateOfTravel, dateFormat_eu), true);
			js = addJsObjElem(js, "DCSext.USellingA", info.USellingA, true);
			js = addJsObjElem(js, "DCSext.USellingR", info.USellingR, true);
			js = addJsObjElem(js, "DCSext.SolutionBrandA", info.SolutionBrandA, true);
			js = addJsObjElem(js, "DCSext.SolutionBrandR", info.SolutionBrandR, true);
			js = addJsObjElem(js, "DCSext.NumAd", info.numAdults, true);
			js = addJsObjElem(js, "DCSext.NumYg", info.numYoung, true);
			js = addJsObjElem(js, "DCSext.NumCh", info.numChildren, true);
			js = addJsObjElem(js, "DCSext.NumIn", info.numInfant, true);
			js = addJsObjElem(js, "DCSext.Network", info.NetworkExt, true);
			js = addJsObjElem(js, "DCSext.DeltaBoToday", info.deltaBoToday, true);
			js = addJsObjElem(js, "DCSext.DeltaBoAr", info.deltaBoAr, true);
			js = addJsObjElem(js, "WT.z_tx_eur", formatIfNotNull(info.z_tx_eur, format), true);
			js = addJsObjElem(js, "WT.z_tx_txn", formatIfNotNull(info.z_tx_txn, format), true);
			js = addJsObjElem(js, "WT.tx_s", formatIfNotNull(info.tx_s, format), true);
			js = addJsObjElem(js, "WT.tx_i", info.tx_i, true);
			js = addJsObjElem(js, "DCSext.CCType", info.CCType, true);
			js = addJsObjElem(js, "DCSext.FareAmn", formatIfNotNull(info.FareAmn, format), true);
			js = addJsObjElem(js, "DCSext.TaxesAmn", formatIfNotNull(info.TaxesAmn, format), true);
			js = addJsObjElem(js, "DCSext.OrdMMUserType", info.OrdMMUserType, true);
			js = addJsObjElem(js, "DCSext.OrdStandard", info.standardUser ? "1" : "0", true);
			js = addJsObjElem(js, "DCSext.OrdMMPaid", info.mmUser ? "1" : "0", true);
			js = addJsObjElem(js, "DCSext.OrdMMAward", info.awardUser ? "1" : "0", true);
			js = addJsObjElem(js, "WT.tx_e", info.tx_e, true);
			js = addJsObjElem(js, "WT.z_sitecode", info.z_sitecode, true);
			js = addJsObjElem(js, "WT.z_tx_cur", info.z_tx_cur, true);
			js = addJsObjElem(js, "DCSext.Time", info.Time, true);
			js = addJsObjElem(js, "WT.pn_gr", info.pn_gr, true);
			js = addJsObjElem(js, "WT.pn_fa", info.pn_fa, true);
			js = addJsObjElem(js, "WT.pc", info.pc, true);
			js = addJsObjElem(js, "WT.pn_sc", info.pn_sc, true);
			js = addJsObjElem(js, "WT.pn_sku", info.pn_sku, true);
			js = addJsObjElem(js, "WT.tx_id", formatIfNotNull(info.tx_id, dateFormat_en), true);
			js = addJsObjElem(js, "WT.tx_it", formatIfNotNull(info.tx_it, timeFormat), true);
			js = addJsObjElem(js, "DCSext.ServClass", info.ServClass, true);
			js = addJsObjElem(js, "DCSext.Pax_leg", info.Pax_leg, true);
			js = addJsObjElem(js, "WT.si_n", info.si_n, true);
			js = addJsObjElem(js, "DCSext.BStep", info.revenuePhase, true);
			js = addJsObjElem(js, "WT.si_x", info.revenuePhase, true);
			js = addJsObjElem(js, "DCSext.SearchType", info.SearchType, true);
			js = addAndEncodeJsObjElem(js, "WT.seg_2", info.seg_2, true);
			js = addAndEncodeJsObjElem(js, "DCSext.acqtratta", info.acqtratta, true);
			js = addJsObjElem(js, "DCSext.EResponse", info.EResponse, true);
			js = addJsObjElem(js, "DCSext.emailreceipt", info.emailreceipt, true);
			js = addJsObjElem(js, "DCSext.ServFeeAmn", format.format(info.ServFeeAmn), true);
			js = addJsObjElem(js, "DCSext.FareYQ", format.format(info.FareYQ), false);

			break;
		}
		js += (trailingSemicolon) ? "};" : "}";
		return js;
	}
	
}
