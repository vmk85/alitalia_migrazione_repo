package com.alitalia.aem.consumer.booking.model.extrabaggage;


/** 
 Sabre Request Model for AncillaryPrice
*/
public class AncillaryPrice
{
	private ApiPrice basePrice;
	public final ApiPrice getbasePrice()
	{
		return basePrice;
	}
	public final void setbasePrice(ApiPrice value)
	{
		basePrice = value;
	}
	private ApiPrice totalTax;
	public final ApiPrice gettotalTax()
	{
		return totalTax;
	}
	public final void settotalTax(ApiPrice value)
	{
		totalTax = value;
	}
	private ApiPrice totalPrice;
	public final ApiPrice gettotalPrice()
	{
		return totalPrice;
	}
	public final void settotalPrice(ApiPrice value)
	{
		totalPrice = value;
	}
	private DealDiscount dealDiscount;
	public final DealDiscount getdealDiscount()
	{
		return dealDiscount;
	}
	public final void setdealDiscount(DealDiscount value)
	{
		dealDiscount = value;
	}
	private Object bagCharacteristics;
	public final Object getbagCharacteristics()
	{
		return bagCharacteristics;
	}
	public final void setbagCharacteristics(Object value)
	{
		bagCharacteristics = value;
	}
	private int assignedQuantity;
	public final int getassignedQuantity()
	{
		return assignedQuantity;
	}
	public final void setassignedQuantity(int value)
	{
		assignedQuantity = value;
	}
	private boolean includedInBundle;
	public final boolean getincludedInBundle()
	{
		return includedInBundle;
	}
	public final void setincludedInBundle(boolean value)
	{
		includedInBundle = value;
	}
	private boolean available;
	public final boolean getavailable()
	{
		return available;
	}
	public final void setavailable(boolean value)
	{
		available = value;
	}
	private int originalAssignedQuantity;
	public final int getoriginalAssignedQuantity()
	{
		return originalAssignedQuantity;
	}
	public final void setoriginalAssignedQuantity(int value)
	{
		originalAssignedQuantity = value;
	}
	private boolean modificationAllowed;
	public final boolean getmodificationAllowed()
	{
		return modificationAllowed;
	}
	public final void setmodificationAllowed(boolean value)
	{
		modificationAllowed = value;
	}
	private Object ssrProperties;
	public final Object getssrProperties()
	{
		return ssrProperties;
	}
	public final void setssrProperties(Object value)
	{
		ssrProperties = value;
	}
	private String ssrDescription;
	public final String getssrDescription()
	{
		return ssrDescription;
	}
	public final void setssrDescription(String value)
	{
		ssrDescription = value;
	}
	private ApiPrice totalAssignedPrice;
	public final ApiPrice gettotalAssignedPrice()
	{
		return totalAssignedPrice;
	}
	public final void settotalAssignedPrice(ApiPrice value)
	{
		totalAssignedPrice = value;
	}
	private ApiPrice originalBasePrice;
	public final ApiPrice getoriginalBasePrice()
	{
		return originalBasePrice;
	}
	public final void setoriginalBasePrice(ApiPrice value)
	{
		originalBasePrice = value;
	}
	private ApiPrice originalTotalTax;
	public final ApiPrice getoriginalTotalTax()
	{
		return originalTotalTax;
	}
	public final void setoriginalTotalTax(ApiPrice value)
	{
		originalTotalTax = value;
	}
	private ApiPrice originalTotalPrice;
	public final ApiPrice getoriginalTotalPrice()
	{
		return originalTotalPrice;
	}
	public final void setoriginalTotalPrice(ApiPrice value)
	{
		originalTotalPrice = value;
	}
}