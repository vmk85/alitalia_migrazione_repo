package com.alitalia.aem.consumer.model.content.listcities;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.CodeDescriptionData;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesResponse;
import com.alitalia.aem.consumer.AlitaliaGeolocalizationManager;
import com.alitalia.aem.consumer.AlitaliaGeolocalizationManager.AlitaliaGeolocalizationData;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.CityOffers;
import com.alitalia.aem.web.component.geospatial.delegate.GeoSpatialDelegate;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;

@Model(adaptables = { SlingHttpServletRequest.class })
public class ListCitiesModel extends GenericBaseModel {

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	@Inject
	GeoSpatialDelegate geoSpatialDelegate;

	@Inject
	private AlitaliaGeolocalizationManager geoManager;

	@Inject
	private Page currentPage;

	private Logger logger = LoggerFactory.getLogger(ListCitiesModel.class);

	private ArrayList<CityOffers> listCity = new ArrayList<CityOffers>();
	private String citySelected = null;

	@PostConstruct
	protected void initModel() {

		super.initBaseModel(slingHttpServletRequest);
		ArrayList<String> listCityCode = new ArrayList<String>();

		String paxType = "ad";
		//INIZIO - MODIFICA INDRA 18/07/2017 ci è stata fatta richiesta di ripristinare il servizio "retrieveGeoCities".
		//String target = "OFFERS"; //questa riga deve essere decommentata nel richiamare il metodo retrieveGeoCitiesByTarget

		paxType = slingHttpServletRequest.getParameter("paxType");

		RetrieveGeoCitiesRequest requestCity = new RetrieveGeoCitiesRequest();
		RetrieveGeoCitiesResponse cityResponse = null;

		requestCity.setSid(sid);
		requestCity.setTid(tid);

		Locale locale = AlitaliaUtils.getRepositoryPathLocale(
				slingHttpServletRequest.getResource());
		requestCity.setLanguageCode(AlitaliaUtils.getLanguage(locale.toLanguageTag()));
		requestCity.setMarketCode(locale.getCountry());
		requestCity.setPaxType(paxType);
		//requestCity.setTarget(target); //questa riga deve essere decommentata nel richiamare il metodo retrieveGeoCitiesByTarget
	
		//cityResponse = geoSpatialDelegate.retrieveGeoCitiesByTarget(requestCity);
		cityResponse = geoSpatialDelegate.retrieveGeoCities(requestCity);
		//FINE.
		
		List<CodeDescriptionData> selectCities = cityResponse.getCities();
		logger.debug("selectCities: " + selectCities.size());
		for (int i = 0; i < selectCities.size(); i++) {
			CityOffers city = new CityOffers(
					selectCities.get(i).getDescription(),
					selectCities.get(i).getCode());
			listCity.add(city);
			listCityCode.add(city.getCodice());
		}

		// cerco la citta' da selezionare
		citySelected = slingHttpServletRequest.getParameter("destination");
		if (citySelected == null || (citySelected != null &&
				!listCityCode.contains(citySelected))) {
			AlitaliaGeolocalizationData geolocData = 
					geoManager.processFromRequest(slingHttpServletRequest, null);
			if (geolocData != null && geolocData.getCityCode() != null) {
				logger.debug("GEOLOC - LIST CITY MODEL: " + geolocData.toCookieString() + ", CITY CODE: " + geolocData.getCityCode());
				citySelected = geolocData.getCityCode();
			} else{
				logger.debug("GEOLOC - NULL");
			}
			
			// controllo se la destinazione recuperata dalla geolocalizzazione
			// sia presente tra gli aereoporti per cui vi e' una offerta
			if (citySelected == null || (citySelected != null &&
					!listCityCode.contains(citySelected))) {
				InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(
								currentPage.getContentResource());
				citySelected = ivm.getInherited("defaultAirport", String.class);
				if (citySelected == null || (citySelected != null &&
						!listCityCode.contains(citySelected))) {
					citySelected = listCityCode.get(0);
				}
			}
		}
	}

	public ArrayList<CityOffers> getListCity() {
		return listCity;
	}

	public void setListCity(ArrayList<CityOffers> listCity) {
		this.listCity = listCity;
	}

	public String getCitySelected() {
		return citySelected;
	}

	public void setCitySelected(String citySelected) {
		this.citySelected = citySelected;
	}
}
