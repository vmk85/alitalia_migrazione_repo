package com.alitalia.aem.consumer.checkinrest.enumeration;

public enum CheckinRestTypeSeatEnum {

	EMPTY(""),
	NOT_EXIST("NotExist"),
	AISLE("Aisle"),
    NORMAL("Normal"),
    CENTERSEAT("CenterSeat"),
    CHARGEABLE("Chargeable"),
	WINDOW("Window");
    
	private final String value;

	CheckinRestTypeSeatEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CheckinRestTypeSeatEnum fromValue(String v) {
        for (CheckinRestTypeSeatEnum c: CheckinRestTypeSeatEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}