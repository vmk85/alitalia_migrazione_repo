package com.alitalia.aem.consumer.booking.controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;


@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingUnicreditRedirectController extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;

	@Inject
	private AlitaliaConfigurationHolder configuration;

	private String redirectFailurePage = "";


	@PostConstruct
	protected void initModel() throws Exception {
		String baseUrlForContext = null;
		String baseUrlForRedirect = null;
		try {
			baseUrlForContext = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);
			baseUrlForRedirect = request.getResource().getResourceResolver().map(baseUrlForContext);
			initBaseModel(request);
			redirectFailurePage = baseUrlForRedirect + configuration.getBookingPaymentPage() + "?error=true";
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			if(baseUrlForRedirect != null ){
				if (ctx != null && ctx.award) {
					redirectFailurePage = baseUrlForRedirect + configuration.getBookingAwardFailurePage();
				}
				else {
					redirectFailurePage = baseUrlForRedirect + configuration.getBookingFailurePage();
				}
				
			} else{
				logger.error("Exception while try to redirect", e);
			}
		}
	}

	public String getRedirectFailurePage(){
		return redirectFailurePage;
	}
}