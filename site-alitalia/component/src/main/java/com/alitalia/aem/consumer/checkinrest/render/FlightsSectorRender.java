package com.alitalia.aem.consumer.checkinrest.render;

import java.util.List;

public class FlightsSectorRender {
	
	private String airline;
	private String flight;
    private String descriptionAirBus;
    private String flightStatus;
    private String checkInGate;
    private String origin;
    private String destination;
    private String departureDate;
    
    private List<PassengerRender> passengerRender;

	private List<PassengerRender> passengerRenderExtra;
	
    public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getFlight() {
		return flight;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}

	public String getDescriptionAirBus() {
		return descriptionAirBus;
	}

	public void setDescriptionAirBus(String descriptionAirBus) {
		this.descriptionAirBus = descriptionAirBus;
	}

	public String getFlightStatus() {
		return flightStatus;
	}

	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}

	public String getCheckInGate() {
		return checkInGate;
	}

	public void setCheckInGate(String checkInGate) {
		this.checkInGate = checkInGate;
	}

	public List<PassengerRender> getPassengerRender() {
		return passengerRender;
	}

	public void setPassengerRender(List<PassengerRender> passengerRender) {
		this.passengerRender = passengerRender;
	}

	public List<PassengerRender> getPassengerRenderExtra() {
		return passengerRenderExtra;
	}

	public void setPassengerRenderExtra(List<PassengerRender> passengerRenderExtra) {
		this.passengerRenderExtra = passengerRenderExtra;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}	
	

}
