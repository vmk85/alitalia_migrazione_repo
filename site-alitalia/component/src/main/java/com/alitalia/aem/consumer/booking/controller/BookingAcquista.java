package com.alitalia.aem.consumer.booking.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.GlobalCollectPaymentTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.BookingSearchKindEnum;
import com.alitalia.aem.consumer.booking.model.InvoiceType;
import com.alitalia.aem.consumer.booking.model.SearchElement;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.booking.render.GenericPriceRender;
import com.alitalia.aem.consumer.booking.render.SearchElementDetailRender;
import com.alitalia.aem.consumer.booking.render.SearchElementRender;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.consumer.utils.CreditCardUtils;
import com.alitalia.aem.consumer.utils.DateUtils;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingAcquista extends BookingSessionGenericController {

	@Self
	private SlingHttpServletRequest request;

	@Inject
	private AlitaliaConfigurationHolder configuration;

	private String name;
	private String surname;
	private String grandTotal;
	private ArrayList<String> banks;
	private ArrayList<String> creditCards;
	private ArrayList<String> otherPayments;
	private ArrayList<String> brCreditCards;
	private ArrayList<String> numeroRateBrasil;
	private int maxNumRateBrasile;
	private String billingTypeCF = InvoiceType.PF.toString();
	private String billingTypePiva = InvoiceType.PG.toString();
	private String message = "";
	private String confirmationPage = "";
	private String redirectFailurePage = "";
	private String redirectFailureValidationPage = "";
	private String market;
	private boolean oneClickPayment;
	private boolean zeroPayment = false;
	private boolean zeroPaymentCarnet = false;
	private boolean insurance = false;
	private boolean ptoEnabled = true;
	private MMCreditCardData mmCreditCardData;
	private boolean loggedUser;
	private String iframeRedirectUrl = "";
	private static Logger logger = LoggerFactory.getLogger(BookingAcquista.class);
	private final int MAX_IMPORTO_LOTTOMATICA = 998;
	private final int MIN_IMPORTO_FINDOMESTIC = 400;
	private final int MIN_DAYS_DIFFERENCE_FINDOMESTIC = 14;
	private final int MIN_IMPORTO_RATA_BRASIL = 200;
	private final int MAX_RATE_BRASIL = 10;
	private boolean award;
	private boolean oneClickEnabled;
	private boolean surcharge;
	private String payment3DSPage = "";

	private String ticketOfficeRTEContentWithDate;
	private String sofortRTEContentWithLink;
	private boolean paypalAvailable = true;

	private static final float FROM_MILLIS_TO_DAYS = 1000 * 60 * 60 * 24;

	@PostConstruct
	protected void initModel() throws Exception {

		// TODO [booking payment] Differenziare per mercato 

		try {

			initBaseModel(request);


			name = ctx.passengersData.getPassengersList() != null ? ctx.passengersData.getPassengersList().get(0).getName() : null;
			surname = ctx.passengersData.getPassengersList() != null ? ctx.passengersData.getPassengersList().get(0).getSurname() : null;
			market = ctx.market;
			insurance = (ctx.insuranceProposalData!=null && ctx.insuranceProposalData.getBuy());

			Integer ptoDaysBeforeDeparture = Integer.parseInt(configuration.getPtoDaysBeforeDeparture());
			FlightData outboundFlight = ctx.flightSelections[0].getFlightData();
			Calendar now = Calendar.getInstance();
			now.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
			Calendar outboundDepartureDate = null;
			if ( outboundFlight.getFlightType() == FlightTypeEnum.CONNECTING ) {
				outboundDepartureDate = ((DirectFlightData) ((ConnectingFlightData) outboundFlight).getFlights().get(0)).getDepartureDate();
			} else {
				outboundDepartureDate = ((DirectFlightData) outboundFlight).getDepartureDate();
			}
			if (outboundDepartureDate!=null && DateUtils.computeDiffDays(now, outboundDepartureDate) < ptoDaysBeforeDeparture){
				ptoEnabled = false;
			}

			if (isFlightFromOrTo("HAV"))
				paypalAvailable = false;

			int numRate = ctx.grossAmount.intValue()/MIN_IMPORTO_RATA_BRASIL;
			maxNumRateBrasile = numRate <= MAX_RATE_BRASIL ? numRate : MAX_RATE_BRASIL;

			loggedUser = (ctx.loggedUserProfileData != null && ctx.loggedUserProfileData.getCustomerNumber() != null) ? true : false;

			//TEST ZEROPAYMENT
			zeroPayment = (ctx.grossAmount!=null && ctx.grossAmount.equals(0.00));

			//TEST ZEROPAYMENT CARNET
			zeroPaymentCarnet = ctx.isCarnetProcess;

			mmCreditCardData = ctx.loggedUserProfileData!=null ? ctx.loggedUserProfileData.getStoredCreditCard() : null;

			if (mmCreditCardData!=null){
				if (mmCreditCardData.getExpireDate()!=null)
					mmCreditCardData.setExpirationDate(CreditCardUtils.fromCalendarToExpirationString(mmCreditCardData.getExpireDate(), "MM/YYYY"));
				if (mmCreditCardData.getNumber()!=null)
					mmCreditCardData.setObfuscatedNumber(CreditCardUtils.obfuscateCardNumber(mmCreditCardData.getNumber()));
			}

			oneClickPayment = mmCreditCardData != null ? true : false;

			surcharge = showSurchargeLabel();

			String baseUrl = AlitaliaUtils.findSiteBaseExternalUrl(request.getResource(), false);

			confirmationPage = baseUrl + configuration.getBookingConfirmationPage();
			redirectFailurePage = baseUrl + configuration.getBookingConfirmationPage() + "?showCallCenterMsg=true";

			//define if booking is award
			award = ctx.award;

			if (award) {
				redirectFailureValidationPage = baseUrl + configuration.getBookingAwardPaymentFailurePage();
			}
			else {
				redirectFailureValidationPage = baseUrl + configuration.getBookingPaymentFailurePage();
			}
			// set message on error
			if (request.getParameter("error") != null && !request.getParameter("error").equals("")) {
				this.message = i18n.get("booking.payment.errorPayment");
			}

			iframeRedirectUrl = ctx.paymentData != null ? ctx.paymentData.getIframeRedirectUrl() : "";

			// calculate grand total
			GenericPriceRender genericPriceRender = new GenericPriceRender(
					ctx.grossAmount, ctx.currency, ctx);
			grandTotal = genericPriceRender.getFare();

			// retrieve list of available credit cards
			creditCards = populateCreditCards();

			// retrieve list of available br credit cards
			brCreditCards = populateBrCreditCards();
			numeroRateBrasil = populateNumeroRateBrasil();

			// retrieve list of available banks
			banks = populateBanks();

			// retrieve list of available otherPayments
			otherPayments = populateOtherPayments();

			/*TicketOffice RTE*/
			Calendar cal = Calendar.getInstance(ctx.locale);
			int dayIncrement = configuration.getTicketOfficePaymentDayLimit();
			cal.add(Calendar.DATE, dayIncrement);
			DateRender dateRender = new DateRender(cal);
			/*Sostituisce il placeholder {} nel RTE con la data*/
			this.ticketOfficeRTEContentWithDate = "";
			String rteContent = AlitaliaUtils.getComponentProperty(AlitaliaUtils.getPage(request.getResource())
					, "rich-text-ticketoffice" , "text");
			if(rteContent != null){
				String data = dateRender.getExtendedDate();
				this.ticketOfficeRTEContentWithDate =rteContent.replaceAll("\\{\\}", data);
			}
			this.sofortRTEContentWithLink = AlitaliaUtils.getComponentProperty(AlitaliaUtils.getPage(request.getResource())
					, "rich-text-sofort" , "text");

			oneClickEnabled = computeOneClickEnabled();

			//			logger.debug("redirecturl 3ds: "+ctx.paymentData.getHtmlData3ds());
			logger.debug("Computing 3ds page");

			payment3DSPage = ctx.paymentData != null && ctx.paymentData.getHtmlData3ds()!=null ? 
					ctx.paymentData.getHtmlData3ds().replace("<html>", "").replace("</html>", "").replace("<body>", "").replace("</body>", "").replace("<head>", "").replace("</head>", "") : "";


		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			redirectFailurePage = configuration.getBookingConfirmationPage() + "?showCallCenterMsg=true";
		}
	}

	/**
	 * Populate an ArrayList of supported credit cards
	 * @return ArrayList<String> credit cards
	 */
	private ArrayList<String> populateCreditCards() {

		ArrayList<String> creditCards = new ArrayList<>();

		// Inserimento CdC valide per tutti i mercati
		String cdcAllMarkets = configuration.getMdPCdCAll();
		if (cdcAllMarkets!=null && !cdcAllMarkets.equals("")){
			for (String cdc : cdcAllMarkets.split(","))
				creditCards.add(cdc.trim());
		}

		// Inserimento cdc per mercato corrente (se presente)
		String cdcMarkets = configuration.getMdPCdCMarkets();
		if (cdcMarkets!=null && !cdcMarkets.equals("") && cdcMarkets.toLowerCase().contains(market.toLowerCase())){
			String cdcMdpForMarket = configuration.getMdPCdC(market.toLowerCase());
			if (cdcMdpForMarket!=null && !cdcMdpForMarket.equals("")){
				for (String cdcMarket : cdcMdpForMarket.split(",")){
					if (!insurance || CreditCardTypeEnum.AMERICAN_EXPRESS.value().equalsIgnoreCase(cdcMarket.trim()))
						creditCards.add(cdcMarket.trim());
				}
			}
		}

		return creditCards;

	}

//	private ArrayList<String> populateBrCreditCards() {
//
//		ArrayList<String> creditCards = new ArrayList<String>();
//
//		if (ctx.isCouponValid == null || !ctx.isCouponValid){
//			// Inserimento cdc per mercato corrente (se presente)
//			String brcdcMarkets = configuration.getMdPBrCdCMarkets();
//			if (brcdcMarkets!=null && !brcdcMarkets.equals("") && brcdcMarkets.toLowerCase().contains(market.toLowerCase())){
//				String brcdcMdpForMarket = configuration.getMdPBrCdC(market.toLowerCase());
//				if (brcdcMdpForMarket!=null && !brcdcMdpForMarket.equals("")){
//					for (String brcdcMarket : brcdcMdpForMarket.split(",")){
//						creditCards.add(brcdcMarket.trim());
//					}
//				}
//			}
//		}
//		return creditCards;
//	}
	
	private ArrayList<String> populateBrCreditCards() {

		ArrayList<String> creditCards = new ArrayList<String>();

		// Inserimento cdc per mercato corrente (se presente)
		String brcdcMarkets = configuration.getMdPBrCdCMarkets();
		if (brcdcMarkets!=null && !brcdcMarkets.equals("") && brcdcMarkets.toLowerCase().contains(market.toLowerCase())){
			String brcdcMdpForMarket = configuration.getMdPBrCdC(market.toLowerCase());
			if (brcdcMdpForMarket!=null && !brcdcMdpForMarket.equals("")){
				for (String brcdcMarket : brcdcMdpForMarket.split(",")){
					creditCards.add(brcdcMarket.trim());
				}
			}
		}
		return creditCards;
	}

	private ArrayList<String> populateNumeroRateBrasil() {
		ArrayList<String> numeroRate = new ArrayList<String>();
		numeroRate.add("Unica Soluzione");
		for(int i=2; i<=maxNumRateBrasile; i++){
			numeroRate.add(String.valueOf(i));
		}
		return numeroRate;
	}

//	/**
//	 * Populate a LinkedHashMap of supported banks
//	 * @return LinkedHashMap<String, String> banks
//	 */
//	private ArrayList<String> populateBanks() {
//		ArrayList<String> banks = new ArrayList<String>();
//
//		if (ctx.isCouponValid== null || !ctx.isCouponValid){
//
//			// Inserimento bankMethod per mercato corrente (se presente)
//			String bankMarkets = configuration.getMdPBankMarkets();
//			if (bankMarkets!=null && !bankMarkets.equals("") && bankMarkets.toLowerCase().contains(market.toLowerCase())){
//				String bankMdpForMarket = configuration.getMdPBank(market.toLowerCase());
//				if (bankMdpForMarket!=null && !bankMdpForMarket.equals("")){
//					for (String bankMethodMarket : bankMdpForMarket.split(",")){
//						if (!("it".equals(market.toLowerCase()) && insurance))
//							banks.add(bankMethodMarket.trim());
//					}
//				}
//			}
//		}
//
//		return banks;
//	}
	
	/**
	 * Populate a LinkedHashMap of supported banks
	 * @return LinkedHashMap<String, String> banks
	 */
	private ArrayList<String> populateBanks() {
		ArrayList<String> banks = new ArrayList<String>();
		
		// Inserimento bankMethod per mercato corrente (se presente)
		String bankMarkets = configuration.getMdPBankMarkets();
		if (bankMarkets!=null && !bankMarkets.equals("") && bankMarkets.toLowerCase().contains(market.toLowerCase())){
			String bankMdpForMarket = configuration.getMdPBank(market.toLowerCase());
			if (bankMdpForMarket!=null && !bankMdpForMarket.equals("")){
				for (String bankMethodMarket : bankMdpForMarket.split(",")){
					if (!("it".equals(market.toLowerCase()) && insurance))
						banks.add(bankMethodMarket.trim());
				}
			}
		}
		return banks;
	}

	private ArrayList<String> populateOtherPayments() {
		ArrayList<String> otherPayments = new ArrayList<String>();

		/* rimossa la condizione della riga seguente al fine di visualizzare ALTRI TIPI DI PAGAMENTO anche quando è stato inserito un COUPON valido*/
		//if (ctx.isCouponValid == null || !ctx.isCouponValid){

			// Inserimento otherMethod per mercato corrente (se presente)
			String otherMdpMarkets = configuration.getMdPOtherMarkets();
			if (otherMdpMarkets!=null && !otherMdpMarkets.equals("") && otherMdpMarkets.toLowerCase().contains(market.toLowerCase())){
				String otherMdpForMarket = configuration.getMdPOther(market.toLowerCase());
				if (otherMdpForMarket!=null && !otherMdpForMarket.equals("")){
					for (String otherMethodMarket : otherMdpForMarket.split(",")){
						if (!("it".equals(market.toLowerCase()) && insurance)){
							if (PaymentTypeEnum.PAY_PAL.value().equalsIgnoreCase(otherMethodMarket)){
								
								/* 
								 * aggiunta condizione !isAfter_1_novembre_2017()
								 */
								if (paypalAvailable && !isAfter_1_novembre_2017())
									otherPayments.add(PaymentTypeEnum.PAY_PAL.value());
							} else if (PaymentTypeEnum.LOTTOMATICA.value().equalsIgnoreCase(otherMethodMarket)){
								if ((ctx.grossAmount!=null && ctx.grossAmount.intValue() <= MAX_IMPORTO_LOTTOMATICA) 
										&& (ctx.isCouponValid== null || !ctx.isCouponValid)) {
									otherPayments.add(PaymentTypeEnum.LOTTOMATICA.value());
								}
							} else if (PaymentTypeEnum.FINDOMESTIC.value().equalsIgnoreCase(otherMethodMarket)){
								if (isFindomesticEnabled())
									otherPayments.add(PaymentTypeEnum.FINDOMESTIC.value());
							} else if (PaymentTypeEnum.PAY_AT_TO.value().equalsIgnoreCase(otherMethodMarket)){
								if (ptoEnabled)
									otherPayments.add(PaymentTypeEnum.PAY_AT_TO.value());
							} else {
								otherPayments.add(otherMethodMarket.trim());
							}
						}
					}
				}
			}
		//}
		return otherPayments;
	}

	private boolean isAfter_1_novembre_2017() {
		boolean retval=false;

		String string_date = "02-11-2017";
		long milliseconds=0;
		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		try {
		    Date d = f.parse(string_date);
		    milliseconds = d.getTime();
		} catch (ParseException e) {
		    e.printStackTrace();
		}
		
		if (ctx.searchElements != null) {
			int i = 0;
			for (SearchElement searchElement : ctx.searchElements) {
				i++;
				long depDate = searchElement.getDepartureDate().getTimeInMillis();
				if (depDate>=milliseconds) {return true;}
			}
		}
		return retval;
	}
	
	private boolean isFindomesticEnabled(){

		boolean findomestic = false;
		if (ctx.selectionAllDirectFlights!=null && ctx.selectionAllDirectFlights.size()>0){
			Calendar dataPartenzaAndata = ctx.selectionAllDirectFlights.get(0).getDepartureDate();
			Calendar dataCorrente = Calendar.getInstance();
			long timeDiff = dataPartenzaAndata.getTimeInMillis() - dataCorrente.getTimeInMillis();
			long daysDifference = TimeUnit.DAYS.convert(timeDiff, TimeUnit.MILLISECONDS);
			logger.debug("Days Difference: " + daysDifference);
			logger.debug("grossAmount: " + ctx.grossAmount.intValue());
			if (daysDifference >= MIN_DAYS_DIFFERENCE_FINDOMESTIC && ctx.grossAmount!=null && ctx.grossAmount.intValue() >= MIN_IMPORTO_FINDOMESTIC)
				findomestic = true;
		}
		return findomestic;
	}

	private boolean showSurchargeLabel() {
		String[] surchargeMarkets = configuration.getBookingSurchargeMarkets();
		for (String surchargeMarket : surchargeMarkets) {
			if (surchargeMarket.equalsIgnoreCase(market)) {
				return true;
			}
		}
		return false;
	}

	private boolean computeOneClickEnabled() {
		boolean paymentEnabled = configuration.getPaymentOneClickEnabled();
		if (paymentEnabled) {
			String[] oneClickMarkets = configuration.getOneClickMarkets();
			for (String oneClickMarket : oneClickMarkets) {
				if (oneClickMarket.equalsIgnoreCase(market)) {
					return true;
				}

			}

		}
		return false;
	}

	private boolean isFlightFromOrTo(String airportCode){
		RoutesData routesData = null;
		AirportData from = null;
		AirportData to = null;
		if (ctx.prenotation != null)
			routesData = ctx.prenotation;
		else
			routesData = ctx.selectionRoutes;
		if (routesData.getRoutesList().get(0).getFlights().get(0) instanceof DirectFlightData){
			from = ((DirectFlightData) ctx.prenotation.getRoutesList().get(0).getFlights().get(0)).getFrom();
			to = ((DirectFlightData) ctx.prenotation.getRoutesList().get(0).getFlights().get(0)).getTo();
		} else { //Connecting Flight
			List<FlightData> flights = ((ConnectingFlightData) ctx.prenotation.getRoutesList().get(0).getFlights().get(0)).getFlights();
			from = ((DirectFlightData) flights.get(0)).getFrom();
			to = ((DirectFlightData) flights.get(flights.size()-1)).getTo();
		}
		logger.debug("From airport code: "+from.getCode());
		logger.debug("To airport code: "+to.getCode());
		if (airportCode.equalsIgnoreCase(from.getCode()) || airportCode.equalsIgnoreCase(to.getCode()))
			return true;
		return false;
	}

	public ArrayList<String> getBanks() {
		return banks;
	}

	public ArrayList<String> getCreditCards() {
		return creditCards;
	}

	public ArrayList<String> getBrCreditCards() {
		return brCreditCards;
	}

	public ArrayList<String> getNumeroRateBrasil() {
		return numeroRateBrasil;
	}

	public int getMaxNumRateBrasile() {
		return maxNumRateBrasile;
	}

	public ArrayList<String> getOtherPayments() {
		return otherPayments;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getGrandTotal() {
		return grandTotal;
	}

	public String getBillingTypeCF() {
		return billingTypeCF;
	}

	public String getBillingTypePiva() {
		return billingTypePiva;
	}

	public String getMessage() {
		return message;
	}

	public String getConfirmationPage() {
		return confirmationPage;
	}

	public String getRedirectFailurePage() {
		return redirectFailurePage;
	}

	public String getRedirectFailureValidationPage() {
		return redirectFailureValidationPage;
	}

	public String getMarket() {
		return market;
	}

	public boolean isOneClickPayment() {
		return oneClickPayment;
	}

	public MMCreditCardData getMmCreditCardData() {
		return mmCreditCardData;
	}

	public boolean isLoggedUser() {
		return loggedUser;
	}

	public String getIframeRedirectUrl() {
		return iframeRedirectUrl;
	}

	public boolean isAward() {
		return award;
	}

	public boolean isZeroPayment() {
		return zeroPayment;
	}

	public boolean isZeroPaymentCarnet(){
		return zeroPaymentCarnet;
	}

	public String getTicketOfficeRTEContentWithDate() {
		return ticketOfficeRTEContentWithDate;
	}

	public String getSofortRTEContentWithLink() {
		return sofortRTEContentWithLink;
	}

	public String getPayment3DSPage() {
		return payment3DSPage;
	}
	public boolean isOneClickEnabled(){
		return oneClickEnabled;
	}

	public boolean isSurcharge() {
		return surcharge;
	}

	public boolean isPtoEnabled() {
		return ptoEnabled;
	}

	public boolean isPaypalAvailable() {
		return paypalAvailable;
	}
}