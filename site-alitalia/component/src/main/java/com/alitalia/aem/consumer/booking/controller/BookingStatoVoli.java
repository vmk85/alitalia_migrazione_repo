package com.alitalia.aem.consumer.booking.controller;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.xml.datatype.Duration;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.FlightItinerayData;
import com.alitalia.aem.common.data.home.ItineraryModelData;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesRequest;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaConfigurationHolder;
import com.alitalia.aem.consumer.booking.model.DatiRicercaOrariVoli;
import com.alitalia.aem.consumer.booking.render.DateRender;
import com.alitalia.aem.consumer.booking.render.ItinerarioRender;
import com.alitalia.aem.consumer.booking.render.TrattaRender;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.flightstatus.delegate.FlightStatusDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;

@Model(adaptables = { SlingHttpServletRequest.class })
public class BookingStatoVoli extends GenericBaseModel {
	
	@Inject
	private AlitaliaConfigurationHolder configuration;
	
	@Inject
	private StaticDataDelegate staticDataDelegate;
	
	@Inject
	private FlightStatusDelegate flightStatusDelegate;
	 
	@Self
	private SlingHttpServletRequest request;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private DatiRicercaOrariVoli datiRicercaOrariVoli;
	private List<ItinerarioRender> itinerariOrariVoli;
	private Boolean showDetail;
	private Boolean hasItinerary;
	
	private String airportDetailsURL;
	private String flightInfoURL;
	
	private String changeSearchURL;
	private String purchaseURL;
	
	private static final String ATTR_NBABY = "Infants";

	private static final String ATTR_NKIDS = "Children";

	private static final String ATTR_NADULTS = "Adults";

	private static final String ATTR_CUG = "CUG";

	private static final String ATTR_DEPARTURE_DATE = "DepartureDate";

	private static final String ATTR_SEARCH_TYPE = "SearchType";

	private static final String ATTR_DESTINATION = "Destination";

	private static final String ATTR_ORIGIN = "Origin";
	
	private static final String STANDARD_REQUEST_DATE_FORMAT = "dd/MM/yyyy";

	@PostConstruct
	protected void initModel() throws Exception {
		try {
			super.initBaseModel(request);
			String aptFrom = request.getParameter("infovoli_departures");
			aptFrom = aptFrom.substring(aptFrom.length() -3 );
			String aptTo = request.getParameter("infovoli_arrival");
			aptTo = aptTo.substring(aptTo.length() -3 );
			String dataPartenza = request.getParameter("dataPartenza");

			datiRicercaOrariVoli =
					buildDatiRicercaOrariVoli(request);
		/*	HttpSession session = request.getSession();
			session.setAttribute("datiRicercaOrariVoli", datiRicercaOrariVoli);
			session.setAttribute("dataRicerca", dataPartenza);*/
			String dateFormat = AlitaliaUtils.getDateFormatByMarket(request.getResource());
			if (dateFormat==null){
				dateFormat=STANDARD_REQUEST_DATE_FORMAT;
			}
			SimpleDateFormat df = new SimpleDateFormat(dateFormat);
			df.setLenient(false);
			Date departureDate = df.parse(dataPartenza);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(departureDate);
			int dayofWeekDeparture=calendar.get(Calendar.DAY_OF_WEEK);
			// data arrivo deve essere uguale a data partenza
			FlightItinerayData flightItinerayData = new FlightItinerayData();
			flightItinerayData.setDeparture(aptFrom);
			flightItinerayData.setArrival(aptTo);
			flightItinerayData.setDepartureDate(calendar);
			flightItinerayData.setArrivalDate(calendar);

			RetrieveItinerariesRequest serviceRequest =
					new RetrieveItinerariesRequest();
			serviceRequest.setFlight(flightItinerayData);
			try{
				RetrieveItinerariesResponse serviceResponse = 
						flightStatusDelegate.retrieveItineraries(serviceRequest);
				if (serviceResponse != null
						&& serviceResponse.getItineraries() != null) {
					/*session.setAttribute("itinerariOrariVoli",
							serviceResponse.getItineraries());*/
					itinerariOrariVoli = createItineraryRender(
							(List<ItineraryModelData>) serviceResponse.getItineraries(),dayofWeekDeparture);
				}
			}catch(Exception e){
				logger.error("Exception on GetItineraryListModel:", e);
			}

			ResourceResolver resolver = request.getResource().getResourceResolver();
			String protocol = "http://";
	    	if(configuration.getHttpsEnabled()){
	    		protocol = "https://";
	    	}
			airportDetailsURL = 	protocol + configuration.getExternalDomain() + 
					resolver.map(AlitaliaUtils.findSiteBaseExternalUrl( 
							request.getResource(), false)
						+ configuration.getBookingAirportDetailsLightboxPage());
			flightInfoURL = 	protocol + configuration.getExternalDomain() + 
					resolver.map(AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), false)
						+ configuration.getFlightInfoPage());
			changeSearchURL = AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), false)
						+ configuration.getFlightInfoHomePage();
			purchaseURL = AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), true)
						+ configuration.getHomePage() + "/.flightsearchconsumer.json";
			
			String bookingSearchParameter = "?";
			bookingSearchParameter += ATTR_ORIGIN + "=" + aptFrom;
			bookingSearchParameter += "&" + ATTR_DESTINATION + "=" + aptTo;
			bookingSearchParameter += "&" + ATTR_DEPARTURE_DATE + "=" + URLEncoder.encode(dataPartenza, "UTF-8");
			bookingSearchParameter += "&" + ATTR_SEARCH_TYPE + "=a";
			bookingSearchParameter += "&" + ATTR_CUG + "=ADT";
			bookingSearchParameter += "&" + ATTR_NADULTS + "=1";
			bookingSearchParameter += "&" + ATTR_NKIDS + "=0";
			bookingSearchParameter += "&" + ATTR_NBABY + "=0";
			purchaseURL = purchaseURL + bookingSearchParameter;
			/*session.setAttribute("airportDetailsURL", airportDetailsURL);
			session.setAttribute("flightInfoURL", flightInfoURL);
			session.setAttribute("changeSearchURL", changeSearchURL);
			session.setAttribute("purchaseURL", purchaseURL + bookingSearchParameter);*/
			
			
			

		/*	String successPage = resolver.map(
					AlitaliaUtils.findSiteBaseExternalUrl(
							request.getResource(), false)
					+ configuration.getBookingFlightInfoListPage());*/
			
			
			showDetail = isBeforeTodayPlusSevenDay(dataPartenza,dateFormat);
			
		} catch (Exception e) {
			logger.error("Unexpected exception:", e);
			/*throw (e);*/
		}
	}
	
	public DatiRicercaOrariVoli getDatiRicercaOrariVoli() {
		return datiRicercaOrariVoli;
	}

	public List<ItinerarioRender> getItinerariOrariVoli() {
		return itinerariOrariVoli;
	}

	public Boolean getShowDetail() {
		return showDetail;
	}
	
	public Boolean hasItinerary() {
		return hasItinerary;
	}

	private List<ItinerarioRender> createItineraryRender(List<ItineraryModelData> itinerari, int dayOfWeekDeparture) {
		itinerariOrariVoli = new ArrayList<ItinerarioRender>();
		for (ItineraryModelData itinerario : itinerari) {
			List<FlightItinerayData> flights = itinerario.getFlights();
			List<TrattaRender> tratte = new ArrayList<TrattaRender>();

			List<Boolean> frequenza = new ArrayList<Boolean>();
			frequenza.add(itinerario.getFrequency().getMonday());
			frequenza.add(itinerario.getFrequency().getTuesday());
			frequenza.add(itinerario.getFrequency().getWednesday());
			frequenza.add(itinerario.getFrequency().getThursday());
			frequenza.add(itinerario.getFrequency().getFriday());
			frequenza.add(itinerario.getFrequency().getSaturday());
			frequenza.add(itinerario.getFrequency().getSunday());
			Boolean check = true;
			Boolean addFlight = false;
			
			for (FlightItinerayData volo : flights) {
				
				if (check && checkFlightAvailable(dayOfWeekDeparture, frequenza)) {
					addFlight = true;
				}
				check = false;
				String aereoportoPartenza = volo.getDeparture();
				String aereoportoArrivo = volo.getArrival();
				String oraPartenza = getOrarioFormattato(volo.getDepartureTime());
				String oraArrivo = getOrarioFormattato(volo.getArrivalTime());
				String codiceVolo = volo.getFlightNumber();
				String vector = volo.getVector();
				String tempoViaggio = volo.getFlightLength().getHours() + "h" + volo.getFlightLength().getMinutes() + "'";
				String miglia = "";//getMile(volo.getArrivalDate(), volo.getFlightNumber(), volo.getVector());
				String codiceAereomobile = volo.getFlightDetails()!=null?volo.getFlightDetails().getAirCraft():"";
				TrattaRender tratta = new TrattaRender (aereoportoPartenza, aereoportoArrivo, oraPartenza, oraArrivo, codiceVolo, tempoViaggio, miglia, codiceAereomobile, frequenza, vector);
				tratte.add(tratta);
			}
			ItinerarioRender itinerarioRender = new ItinerarioRender(tratte);
			Boolean voloDiretto = (tratte.size() > 1) ? false : true;
			itinerarioRender.setVoloDiretto(voloDiretto);
			if (addFlight) {
				itinerariOrariVoli.add(itinerarioRender);
			}
		}
		hasItinerary = (itinerariOrariVoli.size()>0)?true:false;
		return itinerariOrariVoli;
	}

	private boolean isBeforeTodayPlusSevenDay(String day,String dateFormat) throws ParseException {

		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(new Date());
		cal1.add(Calendar.DATE, -1);

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(new Date());
		cal2.add(Calendar.DATE, 7);

		SimpleDateFormat df = new SimpleDateFormat(dateFormat);
		df.setLenient(false);
		Date date = df.parse(day);
		Calendar cal = Calendar.getInstance();

		cal.setTime(date);
		return cal.after(cal1) && cal.before(cal2);

	}

	private String getOrarioFormattato(Duration orario) {
		
		String ora = String.valueOf(orario.getHours());
		String minuti = String.valueOf(orario.getMinutes());
		minuti = (minuti.length() > 1 ? "" : "0") + minuti;
		String time=ora + ":" + minuti;
		SimpleDateFormat dateFormatter = new SimpleDateFormat("kk:mm");
		try {
			Date date=dateFormatter.parse(time);
			Calendar cal=Calendar.getInstance();
			cal.setTime(date);
			DateRender dataobj=new DateRender(cal, this.marketCode);
				time = dataobj.getHour()+" "+ dataobj.getPeriod();
				
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return time;
	}
	

	private Boolean checkFlightAvailable(int dayofWeekdeparture, List<Boolean> frequenza) {
		boolean result=false;
		switch (dayofWeekdeparture) {
			
			case Calendar.MONDAY:
				return frequenza.get(0);
			case Calendar.TUESDAY:
				return frequenza.get(1);
			case Calendar.WEDNESDAY:
				return frequenza.get(2);
			case Calendar.THURSDAY:
				return frequenza.get(3);
			case Calendar.FRIDAY:
				return frequenza.get(4);
			case Calendar.SATURDAY:
				return frequenza.get(5);
			case Calendar.SUNDAY:
				return frequenza.get(6);

			default:
				return result;
		}
		
	}


	
	/*
	 * buildDatiRicercaOrariVoli
	 * 
	 */
	private DatiRicercaOrariVoli buildDatiRicercaOrariVoli(
			SlingHttpServletRequest request) throws ParseException {
		logger.debug("[PreparaViaggioServlet] buildDatiRicercaOrariVoli");
		
		String aptFrom =
				request.getParameter("infovoli_departures");
		String codiceAereoportoPartenza = aptFrom.substring(aptFrom.length() -3 );
		String aptTo =
				request.getParameter("infovoli_arrival");
		String codiceAereoportoArrivo = aptTo.substring(aptTo.length() -3 );
		String dataPartenza = request.getParameter("dataPartenza");
		String codiceStatoPartenza = "";
		String codiceStatoArrivo = "";
		
		String dateFormat = AlitaliaUtils.getDateFormatByMarket(request.getResource());
		if (dateFormat==null){
			dateFormat=STANDARD_REQUEST_DATE_FORMAT;
		}
		
		
		// call service to retrieve airport county code
		RetrieveAirportsRequest req =
				new RetrieveAirportsRequest(IDFactory.getTid(),
						IDFactory.getSid(request));
		req.setMarket(AlitaliaUtils.getRepositoryPathMarket(
				request.getResource()).toUpperCase());
		req.setLanguageCode(AlitaliaUtils.getRepositoryPathLanguage(
				request.getResource()).toUpperCase());
		
		RetrieveAirportsResponse res = staticDataDelegate.retrieveAirports(req);
		List<AirportData> list = res.getAirports();
		for (AirportData airportData : list) {
			if (airportData.getCode().equals(codiceAereoportoPartenza)) {
				codiceStatoPartenza = airportData.getCountryCode();
			}
			if (airportData.getCode().equals(codiceAereoportoArrivo)) {
				codiceStatoArrivo = airportData.getCountryCode();
			}
		}
		
		DatiRicercaOrariVoli datiVoli =
				new DatiRicercaOrariVoli(codiceAereoportoPartenza,
						codiceAereoportoArrivo, dataPartenza, null,
						codiceStatoPartenza, codiceStatoArrivo,dateFormat);
		return datiVoli;

	}
	
	public String getAirportDetailsURL() {
		return airportDetailsURL;
	}

	public String getFlightInfoURL() {
		return flightInfoURL;
	}

	public String getChangeSearchURL() {
		return changeSearchURL;
	}

	public String getPurchaseURL() {
		return purchaseURL;
	}
}
