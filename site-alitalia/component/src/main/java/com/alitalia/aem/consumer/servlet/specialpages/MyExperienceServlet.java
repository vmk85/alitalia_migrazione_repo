package com.alitalia.aem.consumer.servlet.specialpages;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.common.data.home.EmailMessage;
import com.alitalia.aem.common.data.home.enumerations.DeliveryNotificationOptionsEnum;
import com.alitalia.aem.common.data.home.enumerations.MailPriorityEnum;
import com.alitalia.aem.common.messages.home.AgencySendMailRequest;
import com.alitalia.aem.common.messages.home.AgencySendMailResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.global.i18n.I18nKeyCommon;
import com.alitalia.aem.consumer.global.i18n.I18nKeySpecialPage;
import com.alitalia.aem.consumer.validation.ResultValidation;
import com.alitalia.aem.consumer.validation.Validator;
import com.alitalia.aem.consumer.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.service.api.home.BusinessLoginService;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "myexperience" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "file.upload.size", description = "Maximum file upload size"),
})
@SuppressWarnings("serial")
public class MyExperienceServlet extends GenericFormValidatorServlet {
	
	private static final String PARAM_STEP = "step";
	private static final String PARAM_TYPE = "exptype";
	private static final String PARAM_SELECT_A = "selectA";
	private static final String PARAM_SELECT_B = "selectB";
	private static final String PARAM_SELECT_C = "selectC";
	private static final String PARAM_COMMENTS = "comments";
	private static final String PARAM_RESPONSE = "response";
	private static final String PARAM_CONTACT = "contact";
	private static final String PARAM_AGREE = "agree";
	private static final String PARAM_ATTACHMENT = "attachment";
	private static final String PARAM_TITLE = "title";
	private static final String PARAM_FIRST_NAME = "firstName";
	private static final String PARAM_LAST_NAME = "lastName";
	private static final String PARAM_EMAIL = "email";
	private static final String PARAM_EMAIL_REPEAT = "emailRepeat";
	private static final String PARAM_CITY = "city";
	private static final String PARAM_ADDRESS = "address";
	private static final String PARAM_ZIP_CODE = "zipCode";
	private static final String PARAM_COUNTRY = "country";
	private static final String PARAM_MOBILE_PHONE = "mobilePhone";
	private static final String PARAM_BUSINESS_PHONE = "businessPhone";
	private static final String PARAM_HOME_PHONE = "homePhone";
	private static final String PARAM_METHOD_OF_CONTACT = "methodOfContact";
	private static final String PARAM_AIRLINE_PROGRAM = "airlineProgram";
	private static final String PARAM_MILLEMIGLIA = "millemiglia";
	private static final String PARAM_FREQUENT_FLYER_NUMBER = "frequentFlyerNumber";
	private static final String PARAM_SPECIFY_FLIGHT = "specflight";
	private static final String PARAM_COMPANY = "company";
	private static final String PARAM_FLIGHT_NUMBER = "flightNumber";
	private static final String PARAM_DAY = "dateDay";
	private static final String PARAM_MONTH = "dateMonth";
	private static final String PARAM_YEAR = "dateYear";
	private static final String PARAM_TRAVEL_CLASS = "travelClass";
	private static final String PARAM_TRAVEL_FROM = "travelFrom";
	private static final String PARAM_TRAVEL_TO = "travelTo";
	private static final String PARAM_PIR = "pir";
	private static final String PARAM_TICKET_NUMBER = "ticketNumber";
	private static final String PARAM_BOOKING_CODE = "bookingCode";
	private static final String PARAM_FIRST_NAME_UP = "firstNameUp";
	private static final String PARAM_LAST_NAME_UP = "lastNameUp";
	private static final String PARAM_EMAIL_UP = "emailUp";
	private static final String PARAM_MOBILE_PHONE_UP = "mobilePhoneUp";
	private static final String PARAM_FEEDBACK_ID = "feedbackID";
	private static final String PARAM_COMMENTS_UP = "commentsUp";
	private static final String PARAM_ATTACHMENT_UP = "attachmentUp";
	private static final String PARAM_RESOURCE_PATH = "resourcePath";
	
	private static final String PARAM_TYPE_NEW = "expnew";
	private static final String PARAM_TYPE_UPDATE = "expupd";
	private static final String PARAM_SPECIFY_FLIGHT_YES = "specflight-yes";
	private static final String CHECKBOX_VALUE = "true";
	
	private static final String COMPONENT_NAME = "my-experience-form";
	private static final String MAIL_FROM_PROPERTY_NAME = "mailFrom";
	private static final String MAIL_TO_PROPERTY_NAME = "mailTo";
	private static final String MAIL_SUBJECT_PROPERTY_NAME = "subject";
	private static final String MAIL_FROM_DEFAULT = "cfaperte@alitalia.it";
	private static final String MAIL_TO_DEFAULT = "cfaperte@dataar.it";
	private static final String MAIL_SUBJECT_DEFAULT = "Cfa Per Te - Italia";
	
	private static final String KEYVALUE_SEPARATOR = ":";
	private static final String ELEMENT_SEPARATOR = System.getProperty("line.separator");

	
	private ComponentContext componentContext;
	
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile BusinessLoginService businessLoginService;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) throws IOException {
		
		Validator validator = new Validator();
		
		String type = request.getParameter(PARAM_TYPE);
		switch (type) {
		case PARAM_TYPE_NEW:
			String step = request.getParameter(PARAM_STEP);
			switch (step) {
				
			case "2":
				validateStep2(validator, request);
				break;
				
			case "3":
				validateStep3(validator, request);
				break;

			default:
				validateNew(validator, request);
				break;
			}
			break;
			
		case PARAM_TYPE_UPDATE:
			validateUpdate(validator, request);
			break;

		default:
			ResultValidation resultValidation = new ResultValidation();
			resultValidation.setResult(true);
			return resultValidation;
		}
		
		return validator.validate();
	}
	
	private void validateStep2(Validator validator, SlingHttpServletRequest request) {
		
		validator.addDirectCondition(PARAM_SELECT_A, request.getParameter(PARAM_SELECT_A),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition(PARAM_SELECT_B, request.getParameter(PARAM_SELECT_B),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validator.addDirectCondition(PARAM_SELECT_C, request.getParameter(PARAM_SELECT_C),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validateFileSize(validator, request, PARAM_ATTACHMENT);
		
		validator.addDirectCondition(PARAM_AGREE, request.getParameter(PARAM_AGREE),
				I18nKeySpecialPage.COND_ERROR_NOT_ACCEPT, "isTrue");
	}
	
	private void validateFileSize(Validator validator, SlingHttpServletRequest request, String paramName) {
		
		String maxSizeValue = (String) componentContext.getProperties().get("file.upload.size");
		int maxSize = 3*1024*1024;
		if (maxSizeValue != null) {
			try {
				maxSize = Integer.parseInt(maxSizeValue);
			} catch (NumberFormatException e) {
				logger.warn("file.upload.size property not properly configurated.");
			}
		}
		
		RequestParameter document = request.getRequestParameter(paramName);
		
		if (document != null) {
			
			String filesize;
			if (document.isFormField()) {
				filesize = document.getString();
			} else {
				filesize = String.valueOf(document.getSize());
			}
			
			validator.addCrossConditionMessagePattern(paramName, filesize,
					String.valueOf(maxSize + 1),
					I18nKeyCommon.MESSAGE_INVALID_FIELD,
					I18nKeySpecialPage.MMCOMPLAIN_FILE_SIZE_NOT_VALID,
					"isLessThan");
		}
	}
	
	private void validateStep3(Validator validator, SlingHttpServletRequest request) {
		
		validator.addDirectCondition(PARAM_FIRST_NAME, request.getParameter(PARAM_FIRST_NAME),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(PARAM_LAST_NAME, request.getParameter(PARAM_LAST_NAME),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(PARAM_COUNTRY, request.getParameter(PARAM_COUNTRY),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(PARAM_CITY, request.getParameter(PARAM_CITY),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(PARAM_ZIP_CODE, request.getParameter(PARAM_ZIP_CODE),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(PARAM_ADDRESS, request.getParameter(PARAM_ADDRESS),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(PARAM_EMAIL, request.getParameter(PARAM_EMAIL),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(PARAM_HOME_PHONE, request.getParameter(PARAM_HOME_PHONE),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		if (request.getParameter(PARAM_EMAIL) != null && 
				!request.getParameter(PARAM_EMAIL).isEmpty()) {
			validator.addCrossCondition(PARAM_EMAIL_REPEAT, request.getParameter(PARAM_EMAIL), 
					request.getParameter(PARAM_EMAIL_REPEAT),
					I18nKeyCommon.MESSAGE_NOT_EQUAL_FIELD, "areEqual");
		}
	}
	
	
	private void validateNew(Validator validator, SlingHttpServletRequest request) {
		validateStep2(validator, request);
		validateStep3(validator, request);
	}
	
	private void validateUpdate(Validator validator, SlingHttpServletRequest request) {
		
		validator.addDirectCondition(PARAM_FIRST_NAME_UP, request.getParameter(PARAM_FIRST_NAME_UP),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(PARAM_LAST_NAME_UP, request.getParameter(PARAM_LAST_NAME_UP),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		validator.addDirectCondition(PARAM_EMAIL_UP, request.getParameter(PARAM_EMAIL_UP),
				I18nKeyCommon.MESSAGE_GENERIC_EMPTY_FIELD, "isNotEmpty");
		
		validateFileSize(validator, request, PARAM_ATTACHMENT_UP);
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws Exception {
		boolean result = sendMail(request);
		logger.debug("Result {}", String.valueOf(result));
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		response.setContentType("application/json");
		json.key("result").value(result);
		json.endObject();
	}
	
	private boolean sendMail(SlingHttpServletRequest request) {
		
		// resource
		Resource resource = null;
		if (request.getParameter(PARAM_RESOURCE_PATH) != null) {
			resource = request.getResourceResolver().getResource(request.getParameter(PARAM_RESOURCE_PATH));
		}
		
		// email from
	    String mailFrom = getComponentProperty(resource,
	    		COMPONENT_NAME, MAIL_FROM_PROPERTY_NAME);
	    if (mailFrom == null) {
	    	mailFrom = MAIL_FROM_DEFAULT;
	    }
	    
	    // email to
	    String mailTo = getComponentProperty(resource,
	    		COMPONENT_NAME, MAIL_TO_PROPERTY_NAME);
	    if (mailTo == null) {
	    	mailTo = MAIL_TO_DEFAULT;
	    }
	    
	    // email subject
	    String subject = getComponentProperty(resource,
	    		COMPONENT_NAME, MAIL_SUBJECT_PROPERTY_NAME);
	    if (subject == null) {
	    	subject = MAIL_SUBJECT_DEFAULT;
	    }

	    // email body
	    String messageText = generateBodyMail(request);
	    
	    logger.debug("Email data: "
				+ "[From: {" + mailFrom + "}] - [To: {" + mailTo + "}] - "
				+ "[Subject: {" + subject + "}] - [Body: {" + messageText + "}]");
	    
	    // create email message object
		EmailMessage emailMessage = new EmailMessage();
		emailMessage.setFrom(mailFrom);
		emailMessage.setTo(mailTo);
		emailMessage.setSubject(subject);
		emailMessage.setMessageText(messageText);
		emailMessage.setIsBodyHtml(false);
		emailMessage.setDeliveryNotification(DeliveryNotificationOptionsEnum.NONE);
		emailMessage.setPriority(MailPriorityEnum.NORMAL);

		// create AgencySendMailRequest
		AgencySendMailRequest mailRequest = new AgencySendMailRequest();
		mailRequest.setTid(IDFactory.getTid());
		mailRequest.setSid(IDFactory.getSid(request));
		mailRequest.setEmailMessage(emailMessage);
		
		// get AgencySendMailResponse
		AgencySendMailResponse mailResponse =
				businessLoginService.sendMail(mailRequest);
		
		// return status
		return mailResponse.isSendMailSuccessful();
		
	}
	
	private String getComponentProperty(Resource resource,
			String compName, String propName) {
		
		String prop = null;
		try {
			prop = resource.getValueMap().get(propName, String.class);
		} catch(Exception e) {
			logger.error("Error getComponentProperty: ", e);
		}
		return prop;
	}
	
	private String generateBodyMail(SlingHttpServletRequest request) {
		
		Map<String, String> values = new LinkedHashMap<String, String>();
		
		if (PARAM_TYPE_NEW.equals(request.getParameter(PARAM_TYPE))) {
			values.put("Tipo esperienza", "Nuova");
			values.put("Categoria A", request.getParameter(PARAM_SELECT_A));
			values.put("Categoria B", request.getParameter(PARAM_SELECT_B));
			values.put("Categoria C", request.getParameter(PARAM_SELECT_C));
			values.put("Commenti", request.getParameter(PARAM_COMMENTS));
			values.put("Richiesta una risposta", CHECKBOX_VALUE.equals(request.getParameter(PARAM_RESPONSE)) ? "Si" : "No");
			values.put("Autorizzazione a contattare", CHECKBOX_VALUE.equals(request.getParameter(PARAM_CONTACT)) ? "Si" : "No");
			values.put("Accetto", CHECKBOX_VALUE.equals(request.getParameter(PARAM_AGREE)) ? "Si" : "No");
			values.put("Titolo", request.getParameter(PARAM_TITLE));
			values.put("Nome", request.getParameter(PARAM_FIRST_NAME));
			values.put("Cognome", request.getParameter(PARAM_LAST_NAME));
			values.put("E-mail", request.getParameter(PARAM_EMAIL));
			values.put("Citta", request.getParameter(PARAM_CITY));
			values.put("Indirizzo", request.getParameter(PARAM_ADDRESS));
			values.put("Codice postale", request.getParameter(PARAM_ZIP_CODE));
			values.put("Stato", request.getParameter(PARAM_COUNTRY));
			values.put("Numero cellulare", request.getParameter(PARAM_MOBILE_PHONE));
			values.put("Numero ufficio", request.getParameter(PARAM_BUSINESS_PHONE));
			values.put("Numero abitazione", request.getParameter(PARAM_HOME_PHONE));
			values.put("Modalita di contatto", request.getParameter(PARAM_METHOD_OF_CONTACT));
			values.put("Programma Frequent Flyer", request.getParameter(PARAM_AIRLINE_PROGRAM));
			values.put("Profilo Millemiglia", request.getParameter(PARAM_MILLEMIGLIA));
			values.put("Codice Frequent Flyer", request.getParameter(PARAM_FREQUENT_FLYER_NUMBER));
			values.put("Specifica volo", PARAM_SPECIFY_FLIGHT_YES.equals(request.getParameter(PARAM_SPECIFY_FLIGHT)) ? "Si" : "No");
			
			if (PARAM_SPECIFY_FLIGHT_YES.equals(request.getParameter(PARAM_SPECIFY_FLIGHT))) {
				values.put("Compagnia volo", request.getParameter(PARAM_COMPANY));
				values.put("Numero volo", request.getParameter(PARAM_FLIGHT_NUMBER));
				
				String day = request.getParameter(PARAM_DAY);
				String month = request.getParameter(PARAM_MONTH);
				String year = request.getParameter(PARAM_YEAR);
				
				if (day != null && !day.isEmpty() &&
						month != null && !month.isEmpty() &&
						year != null && !year.isEmpty()) {
					values.put("Data volo", day + "/" + month + "/" + year);
				}
				
				values.put("Classe", request.getParameter(PARAM_TRAVEL_CLASS));
				values.put("Partenza", request.getParameter(PARAM_TRAVEL_FROM));
				values.put("Arrivo", request.getParameter(PARAM_TRAVEL_TO));
				values.put("PIR", request.getParameter(PARAM_PIR));
				values.put("Numero biglietto", request.getParameter(PARAM_TICKET_NUMBER));
				values.put("PNR", request.getParameter(PARAM_BOOKING_CODE));
			}
			
		} else if (PARAM_TYPE_UPDATE.equals(request.getParameter(PARAM_TYPE))) {
			values.put("Tipo esperienza", "Update");
			values.put("Nome", request.getParameter(PARAM_FIRST_NAME_UP));
			values.put("Cognome", request.getParameter(PARAM_LAST_NAME_UP));
			values.put("E-mail", request.getParameter(PARAM_EMAIL_UP));
			values.put("Numero cellulare", request.getParameter(PARAM_MOBILE_PHONE_UP));
			values.put("Feedback ID", request.getParameter(PARAM_FEEDBACK_ID));
			values.put("Commenti", request.getParameter(PARAM_COMMENTS_UP));
		}
		
		// get i18n
		// Locale locale = AlitaliaUtils.findResourceLocale(request.getResource());
		// ResourceBundle resourceBundle = request.getResourceBundle(locale);
		// I18n i18n = new I18n(resourceBundle);
		
		// create e-mail message and return it
		StringBuffer emailText = new StringBuffer();
		int k = 0;
		for (String key : values.keySet()) {
			emailText.append(key + KEYVALUE_SEPARATOR + " " + (values.get(key) == null ? "" : values.get(key)));
			if (k < values.size()) {
				emailText.append(ELEMENT_SEPARATOR);
			}
			k++;
		}
		return emailText.toString();
		
	}
	
	@Override
	protected void saveDataIntoSession(SlingHttpServletRequest request, Exception e) {
	}
}
