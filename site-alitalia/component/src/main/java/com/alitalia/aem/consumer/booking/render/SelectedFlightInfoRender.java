package com.alitalia.aem.consumer.booking.render;

import java.util.ArrayList;
import java.util.List;

import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.utils.FlightDataUtils;
import com.day.cq.i18n.I18n;

public class SelectedFlightInfoRender{
	
	private FlightData flightData;
	private List<DirectFlightDataRender> usedFligthsInSelection;
	private List<Integer> connectingFlightsWait;

	public SelectedFlightInfoRender(FlightData flightData, I18n i18n, String market){
		this.flightData = flightData;
		this.usedFligthsInSelection = new ArrayList<DirectFlightDataRender>();
		init(i18n, market);
	}
	
	public FlightData getFlightData() {
		return flightData;
	}

	public List<DirectFlightDataRender> getUsedFligthsInSelection() {
		return usedFligthsInSelection;
	}

	
	/*private methods*/
	
	private void init(I18n i18n, String market) {
		connectingFlightsWait = FlightDataUtils.computeConnectingFlightsWait(flightData);
		usedFligthsInSelection = computeUsedFlightsInSelection(flightData, i18n, market);
	}

	private List<DirectFlightDataRender> computeUsedFlightsInSelection(FlightData flightData, I18n i18n, String market) {
		
		List<DirectFlightDataRender> directFlightsRenderList = new ArrayList<DirectFlightDataRender>();
		if(flightData.getFlightType() == FlightTypeEnum.CONNECTING){
			ConnectingFlightData connectingFlights = (ConnectingFlightData) flightData;
			int index = 0;
			for(FlightData flight : connectingFlights.getFlights()){
				DirectFlightData directFlight = (DirectFlightData) flight;
				directFlightsRenderList.add(
						new DirectFlightDataRender(directFlight, this.connectingFlightsWait.get(index), i18n, market));
				index++;
			}
		}else{ //direct flight
			DirectFlightData directFlight = (DirectFlightData) flightData;
			directFlightsRenderList.add(
					new DirectFlightDataRender(directFlight, this.connectingFlightsWait.get(0), i18n, market));
		}
		
		return directFlightsRenderList;
	}

}
