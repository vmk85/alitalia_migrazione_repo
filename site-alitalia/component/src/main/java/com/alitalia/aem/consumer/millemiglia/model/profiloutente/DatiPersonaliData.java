package com.alitalia.aem.consumer.millemiglia.model.profiloutente;

public class DatiPersonaliData {

	private String gender; 
	private String workPosition;
	private String addressType;
	private String companyName;
	private String address;
	private String postalCode;
	private String city;
	private String country;
	private String state;
	private String email;
	private String phonePrefix;
	private String phoneType;
	private String phoneNumber;
	private String areaPrefix;
	private Boolean checkMillemigliaYoung;
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getWorkPosition() {
		return workPosition;
	}
	
	public void setWorkPosition(String workPosition) {
		this.workPosition = workPosition;
	}
	
	public String getAddressType() {
		return addressType;
	}
	
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getPostalCode() {
		return postalCode;
	}
	
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPhonePrefix() {
		return phonePrefix;
	}
	
	public void setPhonePrefix(String phonePrefix) {
		this.phonePrefix = phonePrefix;
	}
	
	public String getAreaPrefix() {
		return areaPrefix;
	}
	
	public void setAreaPrefix(String areaPrefix) {
		this.areaPrefix = areaPrefix;
	}
	
	public String getPhoneType() {
		return phoneType;
	}
	
	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String numero) {
		this.phoneNumber = numero;
	}
	
	public Boolean getCheckMillemigliaYoung() {
		return checkMillemigliaYoung;
	}
	
	public void setCheckMillemigliaYoung(Boolean checkMillemigliaYoung) {
		this.checkMillemigliaYoung = checkMillemigliaYoung;
	}
	
}
