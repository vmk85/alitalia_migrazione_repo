package com.alitalia.aem.consumer.model.content.specialpages;

public class MillemigliaComplainReasonsData {
	
	private String bookingBuying;
	private String lostluggage;
	private String delay;
	private String damagedluggage;
	private String cancellation;
	private String overbooking;
	private String webservices;
	private String tamperingluggage;
	private String delayedluggage;
	private String millemiglia;
	private String other;
	public String getBookingBuying() {
		return bookingBuying;
	}
	public void setBookingBuying(String bookingBuying) {
		this.bookingBuying = bookingBuying;
	}
	public String getLostLuggage() {
		return lostluggage;
	}
	public void setLostLuggage(String lostluggage) {
		this.lostluggage = lostluggage;
	}
	public String getDamagedLuggage() {
		return damagedluggage;
	}
	public void setDamagedLuggage(String damagedluggage) {
		this.damagedluggage = damagedluggage;
	}
	public String getDelay() {
		return delay;
	}
	public void setDelay(String delay) {
		this.delay = delay;
	}
	public String getCancellation() {
		return cancellation;
	}
	public void setCancellation(String cancellation) {
		this.cancellation = cancellation;
	}
	public String getOverbooking() {
		return overbooking;
	}
	public void setOverbooking(String overbooking) {
		this.overbooking = overbooking;
	}
	public String getWebServices() {
		return webservices;
	}
	public void setWebServices(String webservices) {
		this.webservices = webservices;
	}
	public String getTamperingLuggage() {
		return tamperingluggage;
	}
	public void setTamperingLuggage(String tamperingluggage) {
		this.tamperingluggage = tamperingluggage;
	}
	public String getDelayedLuggage() {
		return delayedluggage;
	}
	public void setDelayedLuggage(String delayedluggage) {
		this.delayedluggage = delayedluggage;
	}
	public String getMillemiglia() {
		return millemiglia;
	}
	public void setMillemiglia(String millemiglia) {
		this.millemiglia = millemiglia;
	}
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}

}
