package com.alitalia.aem.consumer.model.socialshare;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Source;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.day.cq.wcm.api.Page;

@Model(adaptables={SlingHttpServletRequest.class})
public class SocialShareRedazionaleBookingModel extends BaseSocialShareModel {
	
	@Self
	private SlingHttpServletRequest request;
	
	@Inject @Optional @Default(values = "") @Named("jcr:title") @Source(value="child-resources") @Via(value="resource") 
	private String paginaTitolo;
	
	@Inject @Optional @Default(values = "") @Named("jcr:description") @Source(value="child-resources") @Via(value="resource") 
	private String paginaDescrizione;
	
	@PostConstruct
	protected void initModel() {
		super.initBaseModel(request);
		try {
			title = paginaTitolo;
			description = paginaDescrizione;
			
			// link to page itself
			linkBack = findLinkBack(request);
			
			// build the path of the image component in the destination box node
			imagePath = findPageImagePath(request);
			
		} catch (Exception e) {
			logger.error("Error while building social share data", e);
		}
	}
	
	private String findPageImagePath(SlingHttpServletRequest request) 
			throws RepositoryException, ValueFormatException, PathNotFoundException {
		String url = "";
		String baseConfigurationPath = AlitaliaUtils.findSiteBaseRepositoryPath(request.getResource()) 
				+ AlitaliaConstants.BASE_CONFIG_PATH;
		Page generalConfigurationPage = findPageByName(request, baseConfigurationPath, 
				AlitaliaConstants.GENERAL_CONFIG_PAGE_NAME);
		if (generalConfigurationPage != null) {
			url = generalConfigurationPage.getPath() + "/jcr:content/image-social-share-default";
		}
		return url;
	}
	
	private String findLinkBack(SlingHttpServletRequest request) {
		String url = request.getRequestURI();
		if (request.getQueryString() != null && request.getQueryString().length() > 0) {
			url += "?" + request.getQueryString();
		}
		return url;
	}
	
}
