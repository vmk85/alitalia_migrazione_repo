package com.alitalia.aem.consumer.booking.model.extrabaggage;


import java.util.*;

/** 
 Sabre Request Model for TravelPartOffer
*/
public class TravelPartOffer
{
	private TravelPart travelPart;
	public final TravelPart gettravelPart()
	{
		return travelPart;
	}
	public final void settravelPart(TravelPart value)
	{
		travelPart = value;
	}
	private ArrayList<PassengerOffer> passengerOffers;
	public final ArrayList<PassengerOffer> getpassengerOffers()
	{
		return passengerOffers;
	}
	public final void setpassengerOffers(ArrayList<PassengerOffer> value)
	{
		passengerOffers = value;
	}
	private int availableInventory;
	public final int getavailableInventory()
	{
		return availableInventory;
	}
	public final void setavailableInventory(int value)
	{
		availableInventory = value;
	}
}