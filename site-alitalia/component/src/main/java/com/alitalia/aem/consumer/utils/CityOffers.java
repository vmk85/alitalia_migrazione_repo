package com.alitalia.aem.consumer.utils;

public class CityOffers {

	private String descrizione;
	private String codice;
	
	public CityOffers() {}
	
	public CityOffers(String descrizione, String codice) {
		this.descrizione = descrizione;
		this.codice = codice;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}
	
	@Override
	public String toString() {
		return "CityOffers [descrizione=" + descrizione + ", codice=" + codice
				+ "]";
	}
}
