package com.alitalia.aem.consumer.checkin.render;

import java.util.List;

public class CheckinSeatMapSectorRender {
	
	private String label;
	private List<CheckinSeatMapRowRender> rows;
	private String previous;
	private String next;
	
	public CheckinSeatMapSectorRender(String label, List<CheckinSeatMapRowRender> rows) {
		this.label = label;
		this.rows = rows;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<CheckinSeatMapRowRender> getRows() {
		return rows;
	}

	public void setRows(List<CheckinSeatMapRowRender> rows) {
		this.rows = rows;
	}

	public String getPrevious() {
		return previous;
	}

	public void setPrevious(String previous) {
		this.previous = previous;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}
}
