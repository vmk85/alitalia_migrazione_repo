package com.alitalia.aem.consumer.checkinrest.jobs;

/**
 * Created by ggadaleta on 02/02/2018.
 */
public class CheckinJobConstants {

    public final static String TOPIC_PASSENGER_OFFLOAD = "com/alitalia/aem/consumer/checkinrest/passenger/offload";
    public final static String TOPIC_ANCILLARIES_CLEAR = "com/alitalia/aem/consumer/checkinrest/ancillaries/clear";
    public final static String TOPIC_PASSENGER_OFFLOAD_AND_CLEAR_ANCILLARIES = "com/alitalia/aem/consumer/checkinrest/passenger/offloadandclear";
    public final static String TOPIC_PASSENGER_OFFLOAD_AND_CHECKIN = "com/alitalia/aem/consumer/checkinrest/passenger/offloadandcheckin";
    public final static String TOPIC_PASSENGER_OFFLOAD_CHECKIN_AND_CLEAR_ANCILLARIES = "com/alitalia/aem/consumer/checkinrest/passenger/offloadcheckinandclear";
    public final static String TOPIC_ANCILLARIES_COMFORT_SEAT_CLEAR_AND_CHECKIN = "com/alitalia/aem/consumer/checkinrest/ancillaries/comfortseatclear";

}
