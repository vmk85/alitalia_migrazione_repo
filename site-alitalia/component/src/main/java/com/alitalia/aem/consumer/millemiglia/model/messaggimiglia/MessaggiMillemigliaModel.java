package com.alitalia.aem.consumer.millemiglia.model.messaggimiglia;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.consumer.global.AlitaliaConstants;
import com.alitalia.aem.consumer.model.content.GenericBaseModel;
import com.alitalia.aem.consumer.utils.AlitaliaUtils;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MessaggiMillemigliaModel extends GenericBaseModel {

	@Inject
	private static ConsumerLoginDelegate consumerDataDelegate;
	
	@Inject
	private AlitaliaCustomerProfileManager customerProfileManager;

	@Self
	private SlingHttpServletRequest request;

	private List<MessaggiMillemigliaData> listMessages;
	private static final int MAX_MESSAGES_NUM = 16;

	@PostConstruct
	protected void initModel() {
		logger.debug("[MessaggiMillemigliaModel] - Init model");
		
		try {

			MMCustomerProfileData user = 
					AlitaliaUtils.getAuthenticatedUser(request);
			if (user != null) {
				
				user.setCustomerPinCode(
						customerProfileManager.decodeAndDecryptProperty(
								user.getCustomerPinCode()));
				
				listMessages = 
						AlitaliaUtils.getListaMessaggi(user, consumerDataDelegate);
				try {
					
					Calendar filterDate = Calendar.getInstance();
					filterDate.setTime(
							new SimpleDateFormat(AlitaliaConstants.REQUEST_DATE_FORMAT)
							.parse(request.getParameter("day") + "/" + 
									request.getParameter("month") + "/" + 
									request.getParameter("year")));
					listMessages = filterMessagesByData(filterDate);
					
				} catch (Exception e) {
					logger.debug("Filtro dei messaggi per data non riuscito per data non corretta");
				}

			} else {
				// TODO: Gestire
				// Utente non autenticato che prova ad accedere al partial.
				logger.info("Utente non autenticato.");
			}
			
		} catch (Exception e) {
			logger.error(e.toString());
		}
	}

	/**
	 * 
	 * @return
	 */
	public List<MessaggiMillemigliaData> getListMessages() {
		if (null != listMessages) {
			return listMessages.stream().limit(MAX_MESSAGES_NUM)
					.collect(Collectors.toList());
		}
		return null;
	}

	public int getTotalUnreadMessages() {
	    int count = 0;
		if (null != listMessages) {
		    for(MessaggiMillemigliaData msg : listMessages) {
		        if(!msg.isAlreadyRead()){
		            count++;
                }
            }
			return count;
		}
		return count;
	}

	/**
	 * 
	 * @param filterDate
	 * @return
	 */
	public List<MessaggiMillemigliaData> filterMessagesByData(Calendar filterDate) {
		List<MessaggiMillemigliaData> result = new ArrayList<MessaggiMillemigliaData>();
		if (listMessages != null) {

			result = listMessages.stream().filter(message -> {

				Calendar msgDate = Calendar.getInstance();
				try {
					msgDate.setTime(new SimpleDateFormat(
							AlitaliaConstants.MESSAGES_DATE_FORMAT)
							.parse(message.getEndValidatDate()));
				} catch (ParseException e) {
					throw new RuntimeException();
				}
				return (msgDate.compareTo(filterDate) >= 0);
			}).limit(MAX_MESSAGES_NUM).collect(Collectors.toList());

		}

		return result;
	}

}
