package com.alitalia.aem.consumer.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.alitalia.aem.common.data.home.MMCreditCardData;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.consumer.booking.BookingSessionContext;

public class CreditCardUtils {

	public static MMCreditCardData retrieveLoggedUserCreditCards(BookingSessionContext ctx) {

		MMCreditCardData storedCC = null;

		MMCustomerProfileData loggedUserProfileData = ctx.loggedUserProfileData;
		if (loggedUserProfileData != null) {
			List<MMCreditCardData> creditCards = loggedUserProfileData.getCreditCards();
			if (creditCards!=null && creditCards.size()>0)
				storedCC = creditCards.get(0); // Da BR: Solo una CdC
		}
		return storedCC;
	}

	public static String obfuscateCardNumber(String cardNumber) {

		String obfuscatedNumber = "";
		if (cardNumber != null && cardNumber.length()>=4){
			int i;
			for (i=0; i<cardNumber.length()-4; i++){
				obfuscatedNumber += "*";
			}
			obfuscatedNumber+=cardNumber.substring(i);
		}

		return obfuscatedNumber;
	}
	
	public static String fromCalendarToExpirationString(Calendar expireDate, String dateFormat){
		
		String expirationDate = "";
		
		if (expireDate != null){
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			expirationDate = sdf.format(expireDate.getTime());
		}
		return expirationDate;
		
	}
}
