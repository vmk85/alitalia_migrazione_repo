package com.alitalia.aem.consumer.mmb.render;

import org.apache.commons.lang3.StringUtils;

import com.alitalia.aem.common.data.home.mmb.MmbApisInfoData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.consumer.booking.render.DateRender;

public class MmbPassengerDataRender {
	
	private MmbPassengerData passengerData;
	private DateRender apisDataBirthDateRender;
	private DateRender apisDataPassportExpirationDateRender;
	private Boolean isApisDataEmpty;
	
	public MmbPassengerDataRender(MmbPassengerData passengerData) {
		this.passengerData = passengerData;
		
		MmbApisInfoData apisData = getApisData();
		if (apisData != null) {
			if (apisData.getBirthDate() != null) {
				this.apisDataBirthDateRender = new DateRender(apisData.getBirthDate());
			}
			if (apisData.getPassportExpirationDate() != null) {
				this.apisDataPassportExpirationDateRender = new DateRender(apisData.getPassportExpirationDate());		
			}
		}
		
		this.isApisDataEmpty = computeIsApisDataEmpty();
	}
	
	public MmbPassengerData getPassengerData() {
		return this.passengerData;
	}
	
	public String getEticket() {
		String eticket = this.passengerData.getEticket();
		if (!StringUtils.isBlank(eticket)) {
			int endIndex = eticket.lastIndexOf("C");
			return eticket.substring(0, endIndex);	
		}
		return "";
	}
	
	public Boolean getIsApisDataEmpty() {
		return isApisDataEmpty;
	}

	/* apis data */
	public MmbApisInfoData getApisData() {
		return this.passengerData.getApisData();
	}
	
	/* apis birth date */
	public String getApisDataBirthDateDay() {
		if (apisDataBirthDateRender != null) {
			return this.apisDataBirthDateRender.getDay();	
		}
		return "";
	}
	
	public String getApisDataBirthDateMonth() {
		if (apisDataBirthDateRender != null) {
			return this.apisDataBirthDateRender.getMonth();	
		}
		return "";
	}
	
	public String getApisDataBirthDateYear() {
		if (apisDataBirthDateRender != null) {
			return this.apisDataBirthDateRender.getYear();	
		}
		return "";
	}
	
	/* apis passport expiration date */
	public String getApisDataPassportExpirationDateDay() {
		if (apisDataPassportExpirationDateRender != null) {
			return this.apisDataPassportExpirationDateRender.getDay();	
		}
		return "";
	}
	
	public String getApisDataPassportExpirationDateMonth() {
		if (apisDataPassportExpirationDateRender != null) {
			return this.apisDataPassportExpirationDateRender.getMonth();	
		}
		return "";
	}
	
	public String getApisDataPassportExpirationDateYear() {
		if (apisDataPassportExpirationDateRender != null) {
			return this.apisDataPassportExpirationDateRender.getYear();	
		}
		return "";
	}
	
	
	/* internal privates utility methods */
	
	private Boolean computeIsApisDataEmpty() {
		MmbApisInfoData apisData = passengerData.getApisData();
		
		if (apisData == null)  {
			return true;
		}
		
		if ((apisData.getDestinationAddress() == null || "".equals(apisData.getDestinationAddress()))
				&& (apisData.getDestinationCity() == null || "".equals(apisData.getDestinationCity()))
				&& (apisData.getDestinationState() == null || "".equals(apisData.getDestinationState()))
				&& (apisData.getDestinationZipCode() == null || "".equals(apisData.getDestinationZipCode()))
				&& (apisData.getDocType() == null || "".equals(apisData.getDocType()))
				&& (apisData.getExtraDocumentIssuingCountry() == null || "".equals(apisData.getExtraDocumentIssuingCountry()))
				&& (apisData.getExtraDocumentIssuingState() == null || "".equals(apisData.getExtraDocumentIssuingState()))
				&& (apisData.getExtraDocumentNumber() == null || "".equals(apisData.getExtraDocumentNumber()))
				&& (apisData.getExtraDocumentType() == null || "None".equals(apisData.getExtraDocumentType()))
				&& (apisData.getGender() == null || "".equals(apisData.getGender()))
				&& (apisData.getLastname() == null || "".equals(apisData.getLastname()))
				&& (apisData.getName() == null || "".equals(apisData.getName()))
				&& (apisData.getNationality() == null || "".equals(apisData.getNationality()))
				&& (apisData.getPassportNumber() == null || "".equals(apisData.getPassportNumber()))
				&& (apisData.getResidenceCountry() == null || "".equals(apisData.getResidenceCountry()))
				&& (apisData.getSecondName() == null || "".equals(apisData.getSecondName()))) {
					return true;
		}
		return false;
	}
}
