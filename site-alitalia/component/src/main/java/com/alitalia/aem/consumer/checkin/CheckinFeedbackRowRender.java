package com.alitalia.aem.consumer.checkin;

import com.alitalia.aem.consumer.mmb.render.MmbPriceRender;

public class CheckinFeedbackRowRender {
	
	private String name;
	private boolean child;
	private boolean hasInfant;
	private int comfortSeat;
	private int standardSeat;
	private boolean upgrade;
	private String upgradeClassText;
	private MmbPriceRender totalPrice;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public boolean isChild() {
		return child;
	}

	public void setChild(boolean child) {
		this.child = child;
	}

	public boolean isHasInfant() {
		return hasInfant;
	}

	public void setHasInfant(boolean hasInfant) {
		this.hasInfant = hasInfant;
	}

	public int getComfortSeat() {
		return comfortSeat;
	}

	public void setComfortSeat(int comfortSeat) {
		this.comfortSeat = comfortSeat;
	}

	public int getStandardSeat() {
		return standardSeat;
	}

	public void setStandardSeat(int standardSeat) {
		this.standardSeat = standardSeat;
	}

	public MmbPriceRender getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(MmbPriceRender totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	public boolean isUpgrade() {
		return upgrade;
	}
	
	public String getUpgradeClassText() {
		return upgradeClassText;
	}

	public void setUpgrade(boolean upgrade) {
		this.upgrade = upgrade;
	}

	public void setUpgradeClassText(String upgradeClassTextKey) {
		this.upgradeClassText = upgradeClassTextKey;
	}

	@Override
	public String toString() {
		return "CheckinFeedbackRowRender [name=" + name + ", comfortSeat="
				+ comfortSeat + ", standardSeat=" + standardSeat
				+ ", totalPrice=" + totalPrice + "]";
	}

}
