package com.alitalia.aem.consumer.authenticationhandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.auth.core.spi.AuthenticationFeedbackHandler;
import org.apache.sling.auth.core.spi.AuthenticationHandler;
import org.apache.sling.auth.core.spi.AuthenticationInfo;
import org.apache.sling.auth.core.spi.AuthenticationInfoPostProcessor;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Custom authentication post processor used in supports for cookie-based persistent login.
 * 
 * <p>This post processor is triggered after a successfull login and, inspecting the ,
 * request parameters determines if a persistent login cookie must be created.</p>
 */

@Component
@Service(value = AuthenticationInfoPostProcessor.class)
public class AlitaliaConsumerAuthenticationInfoPostProcessor implements
		AuthenticationInfoPostProcessor {
	
	/**
	 * Logger.
	 */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    /**
	 * The component context provided by OSGi runtime.
	 */
	@SuppressWarnings("unused")
	private ComponentContext componentContext = null;
    
    @Reference(target = "(isAlitaliaConsumerAH=true)", policy = ReferencePolicy.DYNAMIC)
    private volatile AuthenticationHandler authenticationHandler;
	
	@Override
	public void postProcess(AuthenticationInfo authenticationInfo, HttpServletRequest request, 
			HttpServletResponse response) throws LoginException {
		
		if ("1".equals(request.getParameter("mm_remember_me"))) {
			
			if (authenticationHandler == null) {
				logger.error("Alitalia Consumer Authentication Handler not available, skipping remember-me.");
				
			} else if (!(authenticationHandler instanceof AlitaliaConsumerAuthenticationHandler)) {
				logger.error("Unrecognized Alitalia Consumer Authentication Handler, skipping remember-me.");
				
			} else {
				// replace the current authentication feedback handler with a new one that will invoke,
				// in chain, the original one and a custom one (the AlitaliaConsumerAuthenticationHandler
				// instance itself) in order to trigger remember-me cookie creation
				AuthenticationFeedbackHandler additionalHandler = 
						(AuthenticationFeedbackHandler) authenticationHandler;
				AuthenticationFeedbackHandler originalHandler = 
						(AuthenticationFeedbackHandler) authenticationInfo.get("$$sling.auth.AuthenticationFeedbackHandler$$");
				AlitaliaConsumerAuthenticationFeedbackHelper chainedFeedbackHandler = 
						new AlitaliaConsumerAuthenticationFeedbackHelper(originalHandler, additionalHandler);
				authenticationInfo.put("$$sling.auth.AuthenticationFeedbackHandler$$", chainedFeedbackHandler);
				
			}
		}
	}
	
	/**
	 * OSGi Declarative Service activate method.
	 */
	@Activate
	protected synchronized void activate(ComponentContext componentContext) {
		logger.info("Service activated.");
		this.componentContext = componentContext;
	}
	
	/**
	 * OSGi Declarative Service modified method.
	 */
	@Modified
	protected synchronized void modified(ComponentContext componentContext) {
		logger.info("Service modified.");
		this.componentContext = componentContext;
	}
	
	/**
	 * OSGi Declarative Service deactivate method.
	 */
	@Deactivate
	protected synchronized void deactivate(ComponentContext componentContext) {
		logger.info("Service deactivated.");
		this.componentContext = componentContext;
	}
	
}
