package com.alitalia.aem.consumer.authenticationhandler;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Dictionary;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.jcr.SimpleCredentials;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.auth.core.spi.AuthenticationFeedbackHandler;
import org.apache.sling.auth.core.spi.AuthenticationHandler;
import org.apache.sling.auth.core.spi.AuthenticationInfo;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.consumer.utils.CookieUtils;

/**
 * Custom authentication handler that add supports for cookie-based persistent login.
 * 
 * <p>This handler is not designed to replace the default HTTP form-based authentication handler,
 * that will be still used to manage credentials check, but to add a layer on top of it to capture
 * and manage authentication checks and challenge them against a cookie-based persistent login data.</p>
 */

@Component(label = "Alitalia Consumer - Custom Authentication Handler", metatype = true)
@Properties({
		@Property(name = "isAlitaliaConsumerAH", boolValue = true, propertyPrivate = true ),
		@Property(label = "Enabled", description = "Can be used to enable or disable the persistent login feature through configuration.", name = AlitaliaConsumerAuthenticationHandler.ENABLED_PROPERTY, value = { "false" }, cardinality = 1),
		@Property(label = "Cookie domain", description = "The domain of the cookie used to store persistent login data.", name = AlitaliaConsumerAuthenticationHandler.COOKIE_DOMAIN_PROPERTY, value = "alitalia.com", propertyPrivate = false),
		@Property(label = "Cookie path", description = "The path of the cookie used to store persistent login data.", name = AlitaliaConsumerAuthenticationHandler.COOKIE_PATH_PROPERTY, value = "/", propertyPrivate = false),
		@Property(label = "Cookie name", description = "The name of the cookie used to store persistent login data.", name = AlitaliaConsumerAuthenticationHandler.COOKIE_NAME_PROPERTY, value = "alitalia-mm-remember-me", propertyPrivate = false),
		@Property(label = "Cookie max-age", description = "The max-age of the cookie (default is 90 days).", name = AlitaliaConsumerAuthenticationHandler.COOKIE_MAX_AGE_PROPERTY, intValue = 7776000, propertyPrivate = false),
		@Property(label = "Cookie encryption password", description = "The password to use for the HMAC-SHA1 cryptographic hash in cookie generation.", name = AlitaliaConsumerAuthenticationHandler.COOKIE_PASSWORD_PROPERTY, value = "changeit", propertyPrivate = false),
		@Property(label = "Authentication Paths", description = "JCR Paths which this Authentication Handler will authenticate.", name = AuthenticationHandler.PATH_PROPERTY, value = { "/content/alitalia" }, cardinality = Integer.MAX_VALUE),
		@Property(label = "Service Ranking", description = "Service ranking. Higher gives more priority. This should sit at the bottom of the stack.", name = "service.ranking", intValue = 10, propertyPrivate = false),
		@Property(label = "Vendor", name = "service.vendor", value = "Alitalia", propertyPrivate = true) })
@Service(value=AuthenticationHandler.class)
public class AlitaliaConsumerAuthenticationHandler implements AuthenticationHandler, AuthenticationFeedbackHandler {
	
	/**
	 * Logger.
	 */
	private final Logger logger = LoggerFactory.getLogger(AlitaliaConsumerAuthenticationHandler.class);
	
	
	/**
	 * Name of the enabled component property.
	 */
	public static final String ENABLED_PROPERTY = "enabled";
	
	/**
	 * Name of the cookie-domain component property.
	 */
	public static final String COOKIE_DOMAIN_PROPERTY = "cookie-domain";
	
	/**
	 * Name of the cookie-path component property.
	 */
	public static final String COOKIE_PATH_PROPERTY = "cookie-path";
	
	/**
	 * Name of the cookie-name component property.
	 */
	public static final String COOKIE_NAME_PROPERTY = "cookie-name";
	
	/**
	 * Name of the cookie-max-age component property.
	 */
	public static final String COOKIE_MAX_AGE_PROPERTY = "cookie-max-age";
	
	/**
	 * Name of the cookie-password component property.
	 */
	public static final String COOKIE_PASSWORD_PROPERTY = "cookie-password";
	
	
	/**
	 * Name of the request attribute used to mark the request as processed
	 * by the remember-me management.
	 */
	public static final String REQUEST_MARKER = "alitaliaconsumer-process.remember-me";
	
	/**
	 * Authentication type.
	 */
	public static final String AUTH_TYPE = "alitaliaconsumer-remember-me";
	
	
	/**
	 * The component context provided by OSGi runtime.
	 */
	private ComponentContext componentContext = null;
	
	/**
	 * Try to extract the current user credentials from the remember-me persistent cookie.
	 * 
	 * <p>Due to the lower ranking this is called for requests after the default handler.</p>
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public AuthenticationInfo extractCredentials(HttpServletRequest request, HttpServletResponse response) {
		logger.trace("extractCredentials invoked");
		if (CookieUtils.getCookieValue(request, "login-token") != null) {
			// skip, AEM login token found
			logger.trace("Skipping: AEM login token found");
			return null;
		}
		Dictionary properties = componentContext.getProperties();
		// manage enable switch configuration
		boolean enabled = PropertiesUtil.toBoolean(
				properties.get(AlitaliaConsumerAuthenticationHandler.ENABLED_PROPERTY), false);
		if (!enabled) {
			logger.trace("Ignoring request, component is disabled through configuration.");
			return null;
		}
		logger.trace("Matched configured JCR path was {}", request.getAttribute("path"));
		logger.trace("Actual Requested path is {}", request.getPathInfo());
		// should manage request, verify cookie
		SimpleCredentials credentials = consumeRememberMeUser(request, response);
		if (credentials != null) {
			logger.debug("Found credentials for user {} ({}) in remember-me cookie", 
					credentials.getUserID(), Arrays.toString(credentials.getPassword()));
			AuthenticationInfo authenticationInfo = new AuthenticationInfo(AUTH_TYPE, 
					credentials.getUserID(), credentials.getPassword());
			return authenticationInfo;
		} else {
			logger.debug("No valid user credentials found in remember-me cookie");
			return null;
		}
	}
	
	/**
	 * This method is invoked when credentials are not found by AEM for the request,
	 * and some kind of user authentication process should be activated.
	 * 
	 * <p>Thanks to the lower ranking this is called after the standard handler, so it
	 * should be actually not reached. If so, just ignore the request.</p>
	 */
	@Override
	public boolean requestCredentials(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("requestCredentials invoked");
		return false;
	}
	
	/**
	 * This method is invoked when credentials must be dropped permanently for 
	 * the session, e.g. in case of a logout.
	 * 
	 * <p>This implementation will make sure that the remember-me cookie is cleared
	 * to avoid an automatic and immediate auto-login again.</p>
	 */
	@Override
	public void dropCredentials(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("dropCredentials invoked");
		clearRememberMeUser(request, response);
	}
	
	/**
	 * This method is called after a successfull authentication attempt, even if initiated
	 * by another authentication handler (thanks to the helper class).
	 * 
	 * <p>At this point, we need to create the rememeber-me cookie. Note that this is performed
	 * always, even if the authentication is granted by a remember-me cookie - we want to
	 * renew the token each time.</p>
	 * 
	 * @see AlitaliaConsumerAuthenticationFeedbackHelper
	 */
	@Override
	public boolean authenticationSucceeded(HttpServletRequest request, HttpServletResponse response, 
			AuthenticationInfo authenticationInfo) {
		// manage enable switch configuration
		Dictionary properties = componentContext.getProperties();
		boolean enabled = PropertiesUtil.toBoolean(
				properties.get(AlitaliaConsumerAuthenticationHandler.ENABLED_PROPERTY), false);
		if (!enabled) {
			logger.trace("Ignoring succeeded authentication, component is disabled through configuration.");
			return false;
		}
		if (authenticationInfo != null) {
			SimpleCredentials credentials = (SimpleCredentials) authenticationInfo.get("user.jcr.credentials");
			if (credentials != null) {
				// FIXME [remember-me] complete implementation encoding the user credentials in cookie
				// TODO [remember-me] let user surname survive up to here after a login, so we can save it in the cookie
				String userId = credentials.getUserID();
				String userSurname = ""; 
				createRememberMeCookie(request, response, userId, userSurname);
			} else {
				logger.error("Could not create remember-me cookie, cannot find credentials.");
			}
		} else {
			logger.error("Could not create remember-me cookie, cannot read authentication info.");
		}
		return false;
	}
	
	/**
	 * This method is called after a failed authentication attempt, even if initiated
	 * by another authentication handler (thanks to the helper class).
	 * 
	 * <p>We simply do nothing in this case.</p>
	 * 
	 * @see AlitaliaConsumerAuthenticationFeedbackHelper
	 */
	@Override
	public void authenticationFailed(HttpServletRequest authenticationInfo, HttpServletResponse request, 
			AuthenticationInfo response) {
		logger.debug("authenticationFailed invoked");
	}
	
	/**
	 * Create the remember-me token, encoding the user code and surname in the cookie value.
	 */
	private void createRememberMeCookie(HttpServletRequest request, HttpServletResponse response, 
			String userId, String userSurname) {
		String cookieName = PropertiesUtil.toString(
				componentContext.getProperties().get(COOKIE_NAME_PROPERTY), "");
		String cookieDomain = (PropertiesUtil.toString(
				componentContext.getProperties().get(COOKIE_DOMAIN_PROPERTY), ""));
		String cookiePath = PropertiesUtil.toString(
				componentContext.getProperties().get(COOKIE_PATH_PROPERTY), "");
		int cookieMaxAge = PropertiesUtil.toInteger(
				componentContext.getProperties().get(COOKIE_MAX_AGE_PROPERTY), 0);
		String cookieValue = createRememberMeCookieValue(userId, userSurname);
		CookieUtils.setCookieValue(response, cookieName, cookieDomain, cookiePath, 
				cookieMaxAge, cookieValue, true, false);
		logger.debug("Remember-me cookie created for domain {}, path {}, name {}, maxAge {}, value {}",
				new Object[] { cookieDomain, cookiePath, cookieName, cookieMaxAge, cookieValue });
	}

	/**
	 * Used to consume the remember-me cookie from the request, if present.
	 * 
	 * <p>If a remember-me cookie is found, it will be invalidated.</p>
	 * 
	 * @param request The HTTP request to work with.
	 * @param response The HTTP response to work with.
	 * @return The user name if a valid cookie was found, or null otherwise.
	 */
	private SimpleCredentials consumeRememberMeUser(final HttpServletRequest request, 
			final HttpServletResponse response) {
		String cookieName = PropertiesUtil.toString(
				componentContext.getProperties().get(COOKIE_NAME_PROPERTY), "");
		String cookieValue = CookieUtils.getCookieValue(request, cookieName);
		if (cookieValue != null) {
			// check current cookie value and invalidate the cookie once used
			// (a new one will be generated in authentication feedback management)
			SimpleCredentials credentials = validateRememberMeCookieValue(cookieValue);
			CookieUtils.clearCookie(request, response, cookieName);
			return credentials;
		}
		return null;
	}

	/**
	 * Used to clear the remember-me cookie trough the response.
	 * 
	 * @param request The HTTP request to work with.
	 * @param response The HTTP response to work with.
	 */
	private void clearRememberMeUser(final HttpServletRequest request, HttpServletResponse response) {
		String cookieName = PropertiesUtil.toString(
				componentContext.getProperties().get(COOKIE_NAME_PROPERTY), "");
		CookieUtils.clearCookie(request, response, cookieName);
	}
	
	/**
	 * Internal utility method to encode user credentials in a HMAC-SHA1 cookie value.
	 * 
	 * @param userId The user identifier.
	 * @param userSurname The user surname.
	 * @return The encoded String.
	 */
	private String createRememberMeCookieValue(String userId, String userSurname) throws RuntimeException {
		try {
			String cookiePassword = PropertiesUtil.toString(
					componentContext.getProperties().get(COOKIE_PASSWORD_PROPERTY), "");
			String escapedUserId = URLEncoder.encode(userId, "UTF-8");
			String escapedUserSurname = URLEncoder.encode(userSurname, "UTF-8");
			String clearTextValue = escapedUserId + "#" + escapedUserSurname + "#" + System.currentTimeMillis();
			String signatureValue = URLEncoder.encode(hmacSha1(clearTextValue, cookiePassword), "UTF-8");
			String cookieValue = clearTextValue + "#" + signatureValue;
			return cookieValue;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e); // (should never happen with UTF-8)
		}
	}

	/**
	 * Internal utility method to validate the value of a remember-me token.
	 * 
	 * <p>Recognized but timed out values are considered invalid by this method.</p>
	 * 
	 * @param cookieValue The value.
	 * @return The credentials containing user id and user surname for a valid value, or null.
	 * @throws Exception
	 */
	private SimpleCredentials validateRememberMeCookieValue(String cookieValue) {
		logger.debug("Validating cookie value {}...", cookieValue);
		int cookieMaxAge = PropertiesUtil.toInteger(
				componentContext.getProperties().get(COOKIE_MAX_AGE_PROPERTY), 0);
		String cookiePassword = PropertiesUtil.toString(
				componentContext.getProperties().get(COOKIE_PASSWORD_PROPERTY), "");
		try {
			if (cookieValue == null || cookieValue.length() == 0) {
				logger.error("Invalid cookie: empty cookie value.");
				return null;
			}
			String[] elements = cookieValue.split("#");
			if (elements.length != 4) {
				logger.error("Invalid cookie: unrecognized value format.");
				return null;
			}
			String escapedUserId = elements[0];
			String escapedUserSurname = elements[1];
			String timestampValue = elements[2];
			String signatureValue = elements[3];
			String userId = URLDecoder.decode(escapedUserId, "UTF-8");
			if (userId == null || userId.length() == 0) {
				logger.error("Invalid cookie: empty user id.");
				return null;
			}
			String userSurname = URLDecoder.decode(escapedUserSurname, "UTF-8");
			if (userSurname == null || userSurname.length() == 0) {
				logger.error("Invalid cookie: empty user surname.");
				return null;
			}
			long timestamp = Long.parseLong(timestampValue);
			if (timestamp > System.currentTimeMillis()
					|| timestamp < System.currentTimeMillis() - cookieMaxAge) {
				logger.debug("Invalid cookie: timed out.");
				return null;
			}
			if (signatureValue == null || signatureValue.length() == 0) {
				logger.error("Invalid cookie: empty signature.");
				return null;
			}
			String clearTextValue = escapedUserId + "#" + escapedUserSurname + "#" + timestampValue;
			String checkSignatureValue = URLEncoder.encode(hmacSha1(clearTextValue, cookiePassword), "UTF-8");
			if (signatureValue.equals(checkSignatureValue)) {
				logger.debug("Cookie is valid, user id and surname are {} {}.", userId, userSurname);
				return new SimpleCredentials(userId, userSurname.toCharArray());
			} else {
				logger.error("Invalid cookie: unrecognized signature.");
				return null;
			}
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e); // (should never happen with UTF-8)
		} catch (NumberFormatException e) {
			logger.error("Invalid cookie: cannot parse timestampe value.");
			return null;
		}
	}
	
	
	/* hmac utility methods */
	
	/**
	 * Compute and returns an HMAC-SHA1 of the provided value using the specified key.
	 * 
	 * @param value The value to encode.
	 * @param key The key to use.
	 * @return The encoded value as a UTF-8 string.
	 */
	private static String hmacSha1(String value, String key) {
        try {
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(value.getBytes());
            String hmac = new String(rawHmac, "UTF-8");
            return hmac;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
	
	/* SCR lifecycle methods */
	
	/**
	 * OSGi Declarative Service activate method.
	 */
	@Activate
	private synchronized void activate(ComponentContext componentContext) {
		logger.info("Service activated.");
		this.componentContext = componentContext;
	}
	
	/**
	 * OSGi Declarative Service modified method.
	 */
	@Modified
	private synchronized void modified(ComponentContext componentContext) {
		logger.info("Service modified.");
		this.componentContext = componentContext;
	}
	
	/**
	 * OSGi Declarative Service deactivate method.
	 */
	@Deactivate
	private synchronized void deactivate(ComponentContext componentContext) {
		logger.info("Service deactivated.");
		this.componentContext = componentContext;
	}

}
