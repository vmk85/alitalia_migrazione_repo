package com.alitalia.aem.consumer.authenticationhandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.auth.core.spi.AuthenticationFeedbackHandler;
import org.apache.sling.auth.core.spi.AuthenticationInfo;

/**
 * Helper implementation of AuthenticationFeedbackHandler that allows to 
 * pre-process the request and the response with another AuthenticationFeedbackHandler
 * implementation before applying the default one.
 */
public class AlitaliaConsumerAuthenticationFeedbackHelper implements AuthenticationFeedbackHandler {
	
	private AuthenticationFeedbackHandler originalHandler;
	private AuthenticationFeedbackHandler additionalHandler;
	
	public AlitaliaConsumerAuthenticationFeedbackHelper(AuthenticationFeedbackHandler originalHandler,
			AuthenticationFeedbackHandler additionalHandler) {
		this.originalHandler = originalHandler;
		this.additionalHandler = additionalHandler;
	}
	
	@Override
	public void authenticationFailed(HttpServletRequest request, HttpServletResponse response, 
			AuthenticationInfo authInfo) {
		if (originalHandler != null) {
			originalHandler.authenticationFailed(request, response, authInfo);
		}
		if (additionalHandler != null) {
			additionalHandler.authenticationFailed(request, response, authInfo);
		}
	}
	
	@Override
	public boolean authenticationSucceeded(HttpServletRequest request, HttpServletResponse response, 
			AuthenticationInfo authInfo) {
		boolean block = false;
		if (additionalHandler != null) {
			block = additionalHandler.authenticationSucceeded(request, response, authInfo);
		}
		if (!block && originalHandler != null) {
			block = originalHandler.authenticationSucceeded(request, response, authInfo);
		}
		return block;
	}
	
}
