package com.alitalia.aem.consumer.identityprovider;

import javax.security.auth.login.LoginException;

@SuppressWarnings("serial")
public class AlitaliaConsumerLoginException extends LoginException {
	
	private boolean isLoginSuccessful = false;
	
	private String failureReasonCode = null;
	
	private String failureReasonDescription = null;
	
	public AlitaliaConsumerLoginException(boolean isLoginSuccessful,
			String failureReasonCode, String failureReasonDescription) {
		super();
		this.isLoginSuccessful = isLoginSuccessful;
		this.failureReasonCode = failureReasonCode;
		this.failureReasonDescription = failureReasonDescription;
	}

	public boolean isLoginSuccessful() {
		return isLoginSuccessful;
	}

	public String getFailureReasonCode() {
		return failureReasonCode;
	}

	public String getFailureReasonDescription() {
		return failureReasonDescription;
	}
	
}
