package com.alitalia.aem.consumer.identityprovider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.jcr.Credentials;
import javax.jcr.SimpleCredentials;
import javax.security.auth.login.LoginException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalGroup;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentity;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityException;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityProvider;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityRef;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalUser;
import org.apache.sling.auth.core.spi.AuthenticationInfo;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginRequest;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.consumer.AlitaliaCustomerProfileManager;
import com.alitalia.aem.web.component.consumerlogin.delegate.ConsumerLoginDelegate;

/**
 * Alitalia Consumer Custom Identity Provider implementation.
 * 
 * <p>The default behavior performs a service request to validate the credentials.</p>
 * 
 * <p>If the <code>use.mock</code> property is set to <code>"true"</code>
 * then the authentication is performed against a fixed set of users,
 * defined with internal mock data.</p>
 * 
 * <p>Note that the identity provider is designed to support only the
 * normal login based on MM code and pin - the entire social login process is
 * managed before triggering the AEM authentication, associating or resolving
 * the related MM credentials in advance. This design is required by the
 * requirements of the social login process, that can be a complex process
 * involving user interaction.</p>
 */
@Component(label = "Alitalia Consumer - Custom Identity Provider", metatype = true, immediate = true)
@Service(value = ExternalIdentityProvider.class)
@Properties({
		@Property(name = "isAlitaliaConsumerIDP", value = "true", propertyPrivate = true),
		@Property(label = "Enable module", description = "Used to enable the identity provider (should be 'false' in authoring environment).", name = AlitaliaConsumerIdentityProvider.ENABLE_MODULE_PROPERTY, value = "false"),
		@Property(label = "Enable init", description = "Used to enable the group identities initialization (should be 'false' in publish environment).", name = AlitaliaConsumerIdentityProvider.ENABLE_INIT_PROPERTY, value = "false"),
		@Property(label = "Use mock implementation", description = "Use a local mock implementation instead of the real service invocation (could be set to 'true' in local publish environment).", name = AlitaliaConsumerIdentityProvider.USE_MOCK_PROPERTY, value = "false"),
		@Property(label = "Vendor", name = "service.vendor", value = "Alitalia", propertyPrivate = true) })
public class AlitaliaConsumerIdentityProvider implements ExternalIdentityProvider {
	
	/**
	 * Logger.
	 */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
    /**
     * Unique name of the identity provider.
     */
	public static final String IDENTITY_PROVIDER_NAME = "alitaliaconsumer-identity-provider";
	
	/**
	 * Name of the property used to enable or disable the identity provider by configuration.
	 */
	public static final String ENABLE_MODULE_PROPERTY = "enable.module";

	/**
	 * Name of the property used to enable or disable the group identities initialization by configuration.
	 */
	public static final String ENABLE_INIT_PROPERTY = "enable.init";
	
	/**
	 * Name of the property used to enable a local mock implementation instead of the real service invocation.
	 */
	public static final String USE_MOCK_PROPERTY = "use.mock";
	
	/**
     * Group for all users.
     */
	public static final String GROUP_ALITALIA_USERS = "alitaliaconsumer-users";
	
	/**
	 * A map to hold a set of default groups, locally defined on activation.
	 */
	private Map<String, AlitaliaConsumerGroup> groups = null;
	
	/**
	 * Reference to the service to remote consumer login service.
	 */
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_UNARY)
	private volatile ConsumerLoginDelegate consumerLoginDelegate;
	
	/**
	 * Reference to the AlitaliaCustomerProfileManager service.
	 */
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_UNARY)
	private volatile AlitaliaCustomerProfileManager customerProfileManager;
	
	/**
	 * The component context provided by OSGi SCR runtime.
	 */
	private ComponentContext componentContext = null;
	
	
	/* implementation of ExternalIdentityProvider interface */
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return IDENTITY_PROVIDER_NAME;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExternalUser authenticate(Credentials credentials)
			throws ExternalIdentityException, LoginException {
		logger.debug("authenticate() invoked.");
		boolean enableModule = "true".equals(componentContext.getProperties().get(ENABLE_MODULE_PROPERTY));
		boolean enableInit = "true".equals(componentContext.getProperties().get(ENABLE_INIT_PROPERTY));
		boolean useMock = "true".equals(componentContext.getProperties().get(USE_MOCK_PROPERTY));
		if (!enableModule && !enableInit) {
			logger.debug("Ignoring request, identity provider is disabled by configuration.");
			return null;
		}
		SimpleCredentials simpleCredentials;
		if (credentials == null) {
			logger.debug("Null credentials provided.");
			throw new ExternalIdentityException("Null credentials provided.");
		}
		ExternalUser externalUser = null;
		if (credentials instanceof SimpleCredentials) {
			simpleCredentials = (SimpleCredentials) credentials;
			if (simpleCredentials.getUserID() == null) {
				logger.debug("Null user ID provided.");
				throw new ExternalIdentityException("Null user ID provided.");
			}
			String authType = (String) simpleCredentials.getAttribute(AuthenticationInfo.AUTH_TYPE);
			String username = simpleCredentials.getUserID();
			if (!checkCandidateUserName(username)) {
				logger.debug("Credentials provided does not match expected pattern for user {}, skipping.", username);
				return null;
			}
			String password = new String(simpleCredentials.getPassword());
			logger.debug("Credentials provided for user {}, authType is {}", username, authType);
			if (enableInit) {
				logger.warn("Using init authentication.");
				externalUser = authenticateWithInitCredentials(authType, username, password);
			} else if (useMock) {
				logger.warn("Using mock authentication data to validate credentials.");
				externalUser = authenticateWithMockCredentials(authType, username, password);
			} else {
				externalUser = authenticateWithCredentials(authType, username, password);
			}
		}
		return externalUser;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExternalIdentity getIdentity(ExternalIdentityRef ref) throws ExternalIdentityException {
		logger.debug("getIdentity() invoked for {}", ref.getId());
		return this.groups.get(ref.getId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExternalGroup getGroup(String name) throws ExternalIdentityException {
		logger.error("getGroup() invoked - unsupported in this implementation");
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExternalUser getUser(String userId) throws ExternalIdentityException {
		logger.error("getUser() invoked - unsupported in this implementation.");
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<ExternalGroup> listGroups() throws ExternalIdentityException {
		logger.error("listGroups() invoked, returning list of locally defined groups.");
		if (groups != null) {
			return new ArrayList<ExternalGroup>(groups.values()).iterator();
		} else {
			return null;
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<ExternalUser> listUsers() throws ExternalIdentityException {
		logger.error("listUsers() invoked - unsupported in this implementation.");
		return null;
	}
	
	
	/* OSGi declarative services methods */
	
	/**
	 * OSGi Declarative Service activate method.
	 */
	@Activate
	protected synchronized void activate(ComponentContext componentContext) {
		logger.info("Service activated.");
		this.componentContext = componentContext;
		refreshGroupDefinitions();
	}
	
	/**
	 * OSGi Declarative Service modified method.
	 */
	@Modified
	protected synchronized void modified(ComponentContext componentContext) {
		logger.info("Service modified.");
		this.componentContext = componentContext;
		refreshGroupDefinitions();
	}
	
	/**
	 * OSGi Declarative Service deactivate method.
	 */
	@Deactivate
	protected synchronized void deactivate(ComponentContext componentContext) {
		logger.info("Service deactivated.");
		this.componentContext = componentContext;
		this.groups = null;
	}
	
	
	/* internal methods */
	
	private void refreshGroupDefinitions() {
		logger.debug("Refreshing locally defined groups.");
		this.groups = new HashMap<String, AlitaliaConsumerGroup>();
		defineGroup(GROUP_ALITALIA_USERS);
	}
	
	private void defineGroup(String groupName) {
		ExternalIdentityRef groupRef = new ExternalIdentityRef(groupName, IDENTITY_PROVIDER_NAME);
		AlitaliaConsumerGroup group = new AlitaliaConsumerGroup(null, Arrays.asList(groupRef), 
				groupRef, groupName, null, groupName);
		group.setProperty("isAlitaliaTradeGroup", true);
		groups.put(groupName, group);
	}
	
	private boolean checkCandidateUserName(String userName) {
		if (userName != null && userName.length() > 3) {
			if (userName.startsWith("MM:")) {
				return true;
			}
		}
		return false;
	}
	
	private ExternalUser authenticateWithCredentials(String authType, String userName, String password) 
			throws LoginException, ExternalIdentityException {
		// verify availability of required references
		if (consumerLoginDelegate == null) {
			logger.error("BusinessLoginDelegate component is not currently available.");
			throw new ExternalIdentityException("BusinessLoginDelegate component is not currently available.");
		}
		if (customerProfileManager == null) {
			logger.error("AlitaliaCustomerProfileManager component is not currently available.");
			throw new ExternalIdentityException("AlitaliaCustomerProfileManager component is not currently available.");
		}
		// determine mmCode and mmPin
		String mmPin = password;
		String mmCode = null;
		if (userName != null && userName.length() > 3 && userName.startsWith("MM:")) {
			mmCode = userName.substring(3);
		} else {
			logger.error("Cannot determine MilleMiglia code for user {0}.", userName);
			throw new ExternalIdentityException("Cannot determine MilleMiglia code for user.");
		}
		// perform the login request
		String tid = IDFactory.getTid();
		String sid = IDFactory.getSid();
		MMCustomerProfileData customerProfile = null;
		String failureReasonCode = "unknown";
		String failureReasonDescription = "unknown";
		boolean isLoginSuccessful = false;
		boolean isRememberMeAuthentication = false;
		// TODO [remember-me] manage remember me? (currently disabled)
		/*
		if (AlitaliaConsumerAuthenticationHandler.AUTH_TYPE.equals(authType)) {
			// credentials provided by trusted remember-me authentication handler so
			// we have the user surname from the cookie as the credentials password,
			// we have to retrieve the actual PIN before going on.
			logger.debug("Performing remember-me based login for user {}.", mmCode);
			isRememberMeAuthentication = true;
			RetrievePINRequest request = new RetrievePINRequest(tid, sid);
			request.setCustomerSurname(mmCode);
			request.setCustomerNumber(mmPin);
			RetrievePINResponse response = consumerLoginDelegate.retrievePin(request);
			if (response != null && response.getCustomerProfile() != null) {
				mmPin = response.getCustomerProfile().getCustomerPIN();
			} else {
				isLoginSuccessful = false;
				failureReasonCode = "unknown";
				failureReasonDescription = "unknown";
				return null;
			}
		}
		*/
		// perform the actual login call (in case of remember-me, this is still required
		// to obtain the set of required user data from the returned customer profile)
		MilleMigliaLoginRequest request = new MilleMigliaLoginRequest(tid, sid);
		request.setCustomerCode(mmCode);
		request.setCustomerPin(mmPin);
		MilleMigliaLoginResponse response = consumerLoginDelegate.milleMigliaLogin(request);
		isLoginSuccessful = response.isLoginSuccessful();
		if (isLoginSuccessful) {
			customerProfile = response.getCustomerProfileData();
		} else {
			failureReasonCode = response.getErrorCode();
			failureReasonDescription = response.getErrorDescription();
		}
		// analyze the response data
		boolean failed = false;
		if (isLoginSuccessful && customerProfile == null) {
			isLoginSuccessful = false;
			failureReasonDescription = "Invalid or null customer data received.";
		}
		if (!isLoginSuccessful) {
			failed = true;
			logger.info("Denied authentication for user {}, reason is {} ({}).", 
					new Object[] { userName, failureReasonCode, failureReasonDescription });
		}
		if (failed) {
			throw new AlitaliaConsumerLoginException(
					isLoginSuccessful,
					failureReasonCode,
					failureReasonDescription);
		}
		// create and return the local user model
		ExternalIdentityRef userRef = new ExternalIdentityRef(userName, IDENTITY_PROVIDER_NAME);
		AlitaliaConsumerUser user = new AlitaliaConsumerUser(userRef, userName, null, userName);
		Map<String, String> userProperties = customerProfileManager.getCustomerProfileProperties(customerProfile);
		for (Map.Entry<String, String> userProperty : userProperties.entrySet()) {
			user.setProperty(userProperty.getKey(), userProperty.getValue());
		}
		user.setProperty("isRememberMeAuthentication", isRememberMeAuthentication);
		user.setProperty("isAlitaliaConsumerUser", true);
		user.addDeclaredGroup(this.groups.get(GROUP_ALITALIA_USERS).externalId);
		logger.info("Authentication success for user: {} - MMCode: {}", userName, mmCode);
		return user;
	}
	
	private ExternalUser authenticateWithInitCredentials(String authType, String userName, String password) 
			throws LoginException, ExternalIdentityException, AlitaliaConsumerLoginException {
		AlitaliaConsumerUser user = null;
		if (("alitalia-init".equals(userName) && "alitalia-init".equals(password))) {
			ExternalIdentityRef userRef = new ExternalIdentityRef(userName, IDENTITY_PROVIDER_NAME);
			user = new AlitaliaConsumerUser(userRef, userName, null, userName);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALIA_USERS).externalId);
		}
		if (user != null) {
			logger.debug("Allowed init authentication for user {}.", userName);
			return user;
		} else {
			logger.debug("Denied init authentication for user {}, wrong user id or password.", userName);
			throw new AlitaliaConsumerLoginException(false, "", "");
		}
	}
	
	private ExternalUser authenticateWithMockCredentials(String authType, String userName, String password) 
			throws LoginException, ExternalIdentityException, AlitaliaConsumerLoginException {
		AlitaliaConsumerUser user = null;
		if (("MM:00000000".equals(userName) && "1234".equals(password))) {
			MMCustomerProfileData customerProfile = customerProfileManager.getAuthenticatedUserMock();
			// create and return the local user model
			ExternalIdentityRef userRef = new ExternalIdentityRef(userName, IDENTITY_PROVIDER_NAME);
			user = new AlitaliaConsumerUser(userRef, userName, null, userName);
			Map<String, String> userProperties = customerProfileManager.getCustomerProfileProperties(customerProfile);
			for (Map.Entry<String, String> userProperty : userProperties.entrySet()) {
				user.setProperty(userProperty.getKey(), userProperty.getValue());
			}
			user.setProperty("isRememberMeAuthentication", false);
			user.setProperty("isAlitaliaConsumerUser", true);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALIA_USERS).externalId);
		}
		if (user != null) {
			logger.warn("Allowed MOCK authentication for user {}.", userName);
			return user;
		} else {
			logger.debug("Denied MOCK authentication for user {}, wrong user id or password.", userName);
			throw new AlitaliaConsumerLoginException(false, "" ,"");
		}
	}
	
}
