Manual steps required to enable usage of this external IDP in a publish instance of AEM:

1. Configure IDP
Configure ExternalLoginModule, SyncHandler and Identity Provider components 
(this should be done automatically through sling:OsgiConfig nodes in ui.apps project)

2. Perform a login in authoring environment using the special user "alitaliatrade-init", password "alitaliatrade-init"
This will trigger creation of all alitaliatrade groups in the authoring environments, to let their usage in CUG properties
(this should be done automatically through CRX packages)

3. Make sure that the alitaliatrade-* groups are published to all the AEM publish servers.
(this should be done automatically through CRX packages)
This can be done using the classic mode useradmin (right click -> activate for each relevant group), and must be done:
- after the publish servers details have been configured in the publish agents of the authoring environment (so each publish server is reached), and
- before publishing any protected page (otherwise, correct page permission won't be set on target pages in publish servers).
This step must be taken into account also if new publish instances are added to the farm (unless created cloning an existing instance).
   
4. During authoring, ensure that the custom login page is specified for all root CUG pages.
(initial pages CUG settings should be done automatically delivered CRX content packages)