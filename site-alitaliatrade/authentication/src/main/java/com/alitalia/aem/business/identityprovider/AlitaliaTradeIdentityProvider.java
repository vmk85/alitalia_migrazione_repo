package com.alitalia.aem.business.identityprovider;

import com.adobe.granite.crypto.CryptoSupport;
import com.alitalia.aem.common.data.home.AlitaliaTradeAgencyData;
import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.alitalia.aem.common.messages.home.AgencyLoginRequest;
import com.alitalia.aem.common.messages.home.AgencyLoginResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.*;
import org.apache.jackrabbit.oak.spi.security.authentication.external.*;
import org.apache.sling.auth.core.spi.AuthenticationInfo;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Credentials;
import javax.jcr.SimpleCredentials;
import javax.security.auth.login.LoginException;
import java.util.*;

/**
 * Alitalia Trade Custom Identity Provider implementation.
 * 
 * <p>The default behavior performs a service request to validate the credentials.</p>
 * 
 * <p>If the <code>use.mock</code> property is set to <code>"true"</code>
 * then the authentication is performed against a fixed set of users,
 * defined with internal mock data.</p>
 */
@Component(label = "Alitalia Trade - Custom Identity Provider", metatype = true, immediate = true)
@Service(value = ExternalIdentityProvider.class)
@Properties({
		@Property(name = "isAlitaliaTradeIDP", value = "true", propertyPrivate = true),
		@Property(label = "Enable module", description = "Used to enable the identity provider (should be 'false' in authoring environment).", name = AlitaliaTradeIdentityProvider.ENABLE_MODULE_PROPERTY, value = "false"),
		@Property(label = "Enable init", description = "Used to enable the group identities initialization (should be 'false' in publish environment).", name = AlitaliaTradeIdentityProvider.ENABLE_INIT_PROPERTY, value = "false"),
		@Property(label = "Use mock implementation", description = "Use a local mock implementation instead of the real service invocation (could be set to 'true' in local publish environment).", name = AlitaliaTradeIdentityProvider.USE_MOCK_PROPERTY, value = "false"),
		@Property(label = "Vendor", name = "service.vendor", value = "Alitalia", propertyPrivate = true) })
public class AlitaliaTradeIdentityProvider implements ExternalIdentityProvider {
	
	/**
	 * Logger.
	 */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
    /**
     * Unique name of the identity provider.
     */
	public static final String IDENTITY_PROVIDER_NAME = "alitaliatrade-identity-provider";
	
	/**
	 * Name of the property used to enable or disable the identity provider by configuration.
	 */
	public static final String ENABLE_MODULE_PROPERTY = "enable.module";

	/**
	 * Name of the property used to enable or disable the group identities initialization by configuration.
	 */
	public static final String ENABLE_INIT_PROPERTY = "enable.init";
	
	/**
	 * Name of the property used to enable a local mock implementation instead of the real service invocation.
	 */
	public static final String USE_MOCK_PROPERTY = "use.mock";
	
	/**
     * Group for all users.
     */
	public static final String GROUP_ALITALITATRADE_USERS = "alitaliatrade-users";
	
	/**
     * Group for IATA users.
     */
	public static final String GROUP_ALITALITATRADE_USERS_IATA = "alitaliatrade-users-iata";
	
	/**
     * Group for non-IATA users.
     */
	public static final String GROUP_ALITALITATRADE_USERS_NOIATA = "alitaliatrade-users-noiata";
	
	 /**
      * Group for users enabled to groups management.
      */
	public static final String GROUP_ALITALITATRADE_USERS_GROUPSMGMT = "alitaliatrade-users-groupsmgmt";
	
	 /**
      * Group for users not enabled to groups management.
      */
	public static final String GROUP_ALITALITATRADE_USERS_NOGROUPSMGMT = "alitaliatrade-users-nogroupsmgmt";
	
	/**
	 * A map to hold a set of default groups, locally defined on activation.
	 */
	private Map<String, AlitaliaTradeGroup> groups = null;
	
	/**
	 * Reference to the service to remote login service.
	 */
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_UNARY)
	private volatile BusinessLoginDelegate businessLoginDelegate;
	
	/**
	 * Reference to the service to AEM Granite Crypto API.
	 */
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_UNARY)
	private volatile CryptoSupport cryptoSupport;
	
	/**
	 * The component context provided by OSGi SCR runtime.
	 */
	private ComponentContext componentContext = null;
	
	
	/* implementation of ExternalIdentityProvider interface */
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return IDENTITY_PROVIDER_NAME;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExternalUser authenticate(Credentials credentials)
			throws ExternalIdentityException, LoginException {
		logger.debug("authenticate() invoked.");
		boolean enableModule = "true".equals(componentContext.getProperties().get(ENABLE_MODULE_PROPERTY));
		boolean enableInit = "true".equals(componentContext.getProperties().get(ENABLE_INIT_PROPERTY));
		boolean useMock = "true".equals(componentContext.getProperties().get(USE_MOCK_PROPERTY));
		if (!enableModule && !enableInit) {
			logger.debug("Ignoring request, identity provider is disabled by configuration.");
			return null;
		}
		SimpleCredentials simpleCredentials;
		if (credentials == null) {
			logger.debug("Null credentials provided.");
			throw new ExternalIdentityException("Null credentials provided.");
		}
		ExternalUser externalUser = null;
		if (credentials instanceof SimpleCredentials) {
			simpleCredentials = (SimpleCredentials) credentials;
			if (simpleCredentials.getUserID() == null) {
				logger.debug("Null user ID provided.");
				throw new ExternalIdentityException("Null user ID provided.");
			}
			String authType = (String) simpleCredentials.getAttribute(AuthenticationInfo.AUTH_TYPE);
			String username = simpleCredentials.getUserID();
			if (!checkCandidateUserName(username)) {
				logger.debug("Credentials provided does not match expected pattern  for user {}, skipping.", username);
				return null;
			}
			String password = new String(simpleCredentials.getPassword());
			logger.debug("Credentials provided for user {}, authType is {}", username, authType);
			if (enableInit) {
				logger.warn("Using init authentication.");
				externalUser = authenticateWithInitCredentials(authType, username, password);
			} else if (useMock) {
				logger.warn("Using mock authentication data to validate credentials.");
				externalUser = authenticateWithMockCredentials(authType, username, password);
			} else {
				externalUser = authenticateWithCredentials(authType, username, password);
			}
		}
		return externalUser;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExternalIdentity getIdentity(ExternalIdentityRef ref) throws ExternalIdentityException {
		logger.debug("getIdentity() invoked for {}", ref.getId());
		return this.groups.get(ref.getId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExternalGroup getGroup(String name) throws ExternalIdentityException {
		logger.error("getGroup() invoked - unsupported in this implementation");
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExternalUser getUser(String userId) throws ExternalIdentityException {
		logger.error("getUser() invoked - unsupported in this implementation.");
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<ExternalGroup> listGroups() throws ExternalIdentityException {
		logger.error("listGroups() invoked, returning list of locally defined groups.");
		if (groups != null) {
			return new ArrayList<ExternalGroup>(groups.values()).iterator();
		} else {
			return null;
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<ExternalUser> listUsers() throws ExternalIdentityException {
		logger.error("listUsers() invoked - unsupported in this implementation.");
		return null;
	}
	
	
	/* OSGi declarative services methods */
	
	/**
	 * OSGi Declarative Service activate method.
	 */
	@Activate
	protected synchronized void activate(ComponentContext componentContext) {
		logger.debug("Service activated.");
		this.componentContext = componentContext;
		refreshGroupDefinitions();
	}
	
	/**
	 * OSGi Declarative Service modified method.
	 */
	@Modified
	protected synchronized void modified(ComponentContext componentContext) {
		logger.debug("Service modified.");
		this.componentContext = componentContext;
		refreshGroupDefinitions();
	}
	
	/**
	 * OSGi Declarative Service deactivate method.
	 */
	@Deactivate
	protected synchronized void deactivate(ComponentContext componentContext) {
		logger.debug("Service deactivated.");
		this.componentContext = componentContext;
		this.groups = null;
	}
	
	
	/* internal methods */
	
	private void refreshGroupDefinitions() {
		logger.debug("Refreshing locally defined groups.");
		this.groups = new HashMap<String, AlitaliaTradeGroup>();
		defineGroup(GROUP_ALITALITATRADE_USERS);
		defineGroup(GROUP_ALITALITATRADE_USERS_IATA);
		defineGroup(GROUP_ALITALITATRADE_USERS_NOIATA);
		defineGroup(GROUP_ALITALITATRADE_USERS_GROUPSMGMT);
		defineGroup(GROUP_ALITALITATRADE_USERS_NOGROUPSMGMT);
	}
	
	private void defineGroup(String groupName) {
		ExternalIdentityRef groupRef = new ExternalIdentityRef(groupName, IDENTITY_PROVIDER_NAME);
		AlitaliaTradeGroup group = new AlitaliaTradeGroup(null, Arrays.asList(groupRef), 
				groupRef, groupName, null, groupName);
		group.setProperty("isAlitaliaTradeGroup", true);
		groups.put(groupName, group);
	}
	
	private boolean checkCandidateUserName(String userName) {
		if (userName != null && userName.length() > 2) {
			if (userName.startsWith("B:") || userName.startsWith("T:")) {
				return true;
			}
		}
		return false;
	}
	
	private ExternalUser authenticateWithCredentials(String authType, String userName, String password) 
			throws LoginException, ExternalIdentityException {
		// verify availability of delegate service reference
		if (businessLoginDelegate == null) {
			logger.error("BusinessLoginDelegate component is not currently available.");
			throw new ExternalIdentityException("BusinessLoginDelegate component is not currently available.");
		}
		if (cryptoSupport == null) {
			logger.error("CryptoSupport component is not currently available.");
			throw new ExternalIdentityException("CryptoSupport component is not currently available.");
		}
		// determine agency code and role from credentials
		String codiceAgenzia = null;
		AlitaliaTradeUserType ruolo = null;
		if (userName != null && userName.length() > 2) {
			codiceAgenzia = userName.substring(2);
			if (userName.startsWith("B:")) {
				ruolo = AlitaliaTradeUserType.BANCONISTA;
			} else if (userName.startsWith("T:")) {
				ruolo = AlitaliaTradeUserType.TITOLARE;
			} else {
				logger.debug("Cannot determine login role for user {0}.", userName);
				throw new ExternalIdentityException("Cannot determine login role for user.");
			}
		} else {
			logger.error("Cannot determine login role for user {0}.", userName);
			throw new ExternalIdentityException("Cannot determine login role for user.");
		}
		// perform the login request
		String tid = IDFactory.getTid();
		String sid = IDFactory.getSid();
		AgencyLoginRequest agencyLoginRequest = new AgencyLoginRequest(tid, sid);
		agencyLoginRequest.setCodiceAgenzia(codiceAgenzia);
		agencyLoginRequest.setPassword(password);
		agencyLoginRequest.setRuolo(ruolo);
		AgencyLoginResponse agencyLoginResponse = businessLoginDelegate.agencyLogin(agencyLoginRequest);
		// analyze the response
		boolean failed = false;
		if (!agencyLoginResponse.getAgencyData().isGroupEnabled()){
			failed = true;
			logger.info("Account for agency login in not enabled for groups ticketing {0}.", userName);
		}
		if (!agencyLoginResponse.isLoginSuccessful()) {
			failed = true;
			logger.info("Denied authentication for user {}, wrong user id or password.", userName);
		}
		if (agencyLoginResponse.isAccountLocked()) {
			failed = true;
			logger.info("Account for agency login is locked for user {0}.", userName);
		}
		if (agencyLoginResponse.isPasswordExpired()) {
			failed = true;
			logger.info("Account for agency login is expired for user {0}.", userName);
		}
		if (agencyLoginResponse.isFirstAccess()) {
			failed = true;
			logger.info("Account for agency login requires first password change for user {0}.", userName);
		}		
		if (agencyLoginResponse.isApprovalRequired()) {
			failed = true;
			logger.info("Account for agency login requires approval of terms for user {0}.", userName);
		}
		if (failed) {
			throw new AlitaliaTradeLoginException(
					agencyLoginResponse.isLoginSuccessful(),
					agencyLoginResponse.isAccountLocked(),
					agencyLoginResponse.isPasswordExpired(),
					agencyLoginResponse.isApprovalRequired(),
					agencyLoginResponse.isFirstAccess(),
					agencyLoginResponse.getAgencyData().isGroupEnabled());

		}
		AlitaliaTradeAgencyData agencyData = agencyLoginResponse.getAgencyData();
		boolean isIata = agencyData.isIataAgency();
		boolean isGroupEnabled = agencyData.isGroupEnabled();
		// prepare encrypted password
		String encryptedPassword;
		try {
			byte[] passwordBytes = password.getBytes("UTF-8");
			byte[] encryptedPasswordBytes = cryptoSupport.encrypt(passwordBytes);
			encryptedPassword = Base64.getEncoder().encodeToString(encryptedPasswordBytes);
		} catch (Exception e) {
			logger.error("Error performing encryption of user password code", e);
			throw new ExternalIdentityException("Error performing encryption of user password code.", e);
		}
		// create and return the local user model
		ExternalIdentityRef userRef = new ExternalIdentityRef(userName, IDENTITY_PROVIDER_NAME);
		AlitaliaTradeUser user = new AlitaliaTradeUser(userRef, userName, null, userName);
		user.setProperty("codiceAgenzia", codiceAgenzia);
		user.setProperty("ruolo", ruolo.value());
		user.setProperty("ragioneSociale", agencyData.getRagioneSociale());
		user.setProperty("isAlitaliaTradeUser", true);
		user.setProperty("codiceAccordo", agencyData.getCodiceAccordo());
		user.setProperty("password", encryptedPassword);
		user.setProperty("isIata", isIata);
		user.setProperty("isGruppi", isGroupEnabled);
		user.setProperty("isLogged", true);
		logger.debug("Partita IVA Agenzia: ["+agencyData.getPartitaIVA()+"]");
		user.setProperty("partitaIva", agencyData.getPartitaIVA());
		user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS).externalId);
		if (isIata) {
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_IATA).externalId);
		} else {
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_NOIATA).externalId);
		}
		if (isGroupEnabled) {
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_GROUPSMGMT).externalId);
		} else {
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_NOGROUPSMGMT).externalId);
		}
		logger.debug("Allowed authentication for user {}.", userName);
		return user;
	}
	
	private ExternalUser authenticateWithInitCredentials(String authType, String userName, String password) 
			throws LoginException, ExternalIdentityException, AlitaliaTradeLoginException {
		AlitaliaTradeUser user = null;
		if (("alitaliatrade-init".equals(userName) && "alitaliatrade-init".equals(password))) {
			ExternalIdentityRef userRef = new ExternalIdentityRef(userName, IDENTITY_PROVIDER_NAME);
			user = new AlitaliaTradeUser(userRef, userName, null, userName);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS).externalId);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_IATA).externalId);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_GROUPSMGMT).externalId);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_NOGROUPSMGMT).externalId);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_NOIATA).externalId);
		}
		if (user != null) {
			logger.debug("Allowed init authentication for user {}.", userName);
			return user;
		} else {
			logger.debug("Denied init authentication for user {}, wrong user id or password.", userName);
			throw new AlitaliaTradeLoginException(false, false, false, false, false, false);
		}
	}
	
	private ExternalUser authenticateWithMockCredentials(String authType, String userName, String password) 
			throws LoginException, ExternalIdentityException, AlitaliaTradeLoginException {
		AlitaliaTradeUser user = null;
		// prepare encrypted password
		String encryptedPassword;
		try {
			byte[] encryptedPasswordBytes = cryptoSupport.encrypt(password.getBytes("UTF-8"));
			encryptedPassword = new String(encryptedPasswordBytes, "UTF-8");
		} catch (Exception e) {
			logger.error("Error performing encryption of user password code", e);
			throw new ExternalIdentityException("Error performing encryption of user password code.", e);
		}
		if (("B:test1".equals(userName) && "test1".equals(password))) {
			ExternalIdentityRef userRef = new ExternalIdentityRef(userName, IDENTITY_PROVIDER_NAME);
			user = new AlitaliaTradeUser(userRef, userName, null, userName);
			user.setProperty("codiceAgenzia", "test1");
			user.setProperty("ruolo", AlitaliaTradeUserType.BANCONISTA.value());
			user.setProperty("ragioneSociale", "Test1 s.r.l.");
			user.setProperty("isAlitaliaTradeUser", true);
			user.setProperty("password", encryptedPassword);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS).externalId);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_IATA).externalId);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_GROUPSMGMT).externalId);
		} else if (("B:test2".equals(userName) && "test2".equals(password))) {
			ExternalIdentityRef userRef = new ExternalIdentityRef(userName, IDENTITY_PROVIDER_NAME);
			user = new AlitaliaTradeUser(userRef, userName, null, userName);
			user.setProperty("codiceAgenzia", "test2");
			user.setProperty("ruolo", AlitaliaTradeUserType.BANCONISTA.value());
			user.setProperty("ragioneSociale", "Test1 s.r.l.");
			user.setProperty("isAlitaliaTradeUser", true);
			user.setProperty("password", encryptedPassword);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS).externalId);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_NOIATA).externalId);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_NOGROUPSMGMT).externalId);
		} else if (("T:test3".equals(userName) && "test3".equals(password))) {
			ExternalIdentityRef userRef = new ExternalIdentityRef(userName, IDENTITY_PROVIDER_NAME);
			user = new AlitaliaTradeUser(userRef, userName, null, userName);
			user.setProperty("codiceAgenzia", "test3");
			user.setProperty("ruolo", AlitaliaTradeUserType.TITOLARE.value());
			user.setProperty("ragioneSociale", "Test1 s.r.l.");
			user.setProperty("isAlitaliaTradeUser", true);
			user.setProperty("password", encryptedPassword);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS).externalId);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_IATA).externalId);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_NOGROUPSMGMT).externalId);
		} else if (("T:test4".equals(userName) && "test4".equals(password))) {
			ExternalIdentityRef userRef = new ExternalIdentityRef(userName, IDENTITY_PROVIDER_NAME);
			user = new AlitaliaTradeUser(userRef, userName, null, userName);
			user.setProperty("codiceAgenzia", "test4");
			user.setProperty("ruolo", AlitaliaTradeUserType.TITOLARE.value());
			user.setProperty("ragioneSociale", "Test1 s.r.l.");
			user.setProperty("isAlitaliaTradeUser", true);
			user.setProperty("password", encryptedPassword);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS).externalId);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_NOIATA).externalId);
			user.addDeclaredGroup(this.groups.get(GROUP_ALITALITATRADE_USERS_GROUPSMGMT).externalId);
		}
		if (user != null) {
			logger.debug("Allowed authentication for user {}.", userName);
			return user;
		} else {
			logger.debug("Denied authentication for user {}, wrong user id or password.", userName);
			throw new AlitaliaTradeLoginException(false, false, false, false, false, false);
		}
	}
	
}
