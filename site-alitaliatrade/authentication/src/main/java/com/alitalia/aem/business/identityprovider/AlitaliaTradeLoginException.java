package com.alitalia.aem.business.identityprovider;

import javax.security.auth.login.LoginException;

@SuppressWarnings("serial")
public class AlitaliaTradeLoginException extends LoginException {

	private boolean credentialsFound = false;

	private boolean locked = false;

	private boolean expired = false;

	private boolean firstAccess = false;

	private boolean acceptRequired = false;

	private boolean isGruppiEnabled = false;

	public AlitaliaTradeLoginException(boolean credentialsFound, boolean locked, boolean expired, boolean acceptRequired, boolean firstAccess, boolean isGruppiEnabled) {
		super();
		this.credentialsFound = credentialsFound;
		this.locked = locked;
		this.expired = expired;
		this.firstAccess = firstAccess;
		this.acceptRequired = acceptRequired;
		this.isGruppiEnabled = isGruppiEnabled;
	}

	public boolean isGruppiEnabled() {
		return isGruppiEnabled;
	}

	public boolean isCredentialsFound() {
		return credentialsFound;
	}

	public boolean isLocked() {
		return locked;
	}

	public boolean isExpired() {
		return expired;
	}

	public boolean isFirstAccess() {
		return firstAccess;
	}

	public boolean isAcceptRequired() {
		return acceptRequired;
	}

}
