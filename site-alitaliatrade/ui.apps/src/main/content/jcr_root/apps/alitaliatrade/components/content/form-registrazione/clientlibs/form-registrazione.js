$("#btn_registrati").bind('click', function(e) {
	generalOverlayLoading(true);
	validation(e, 'agencyregistration', '#form-registrazione', successRegistration);
});
 
function successRegistration(data) {
	removeErrors();
	if (data.result) {
		$('#btn_registrati').unbind('click').click();
	} else {
		var checkEr = false;
		for (var key in data.fields) {
			
			var field = $('[name="' + key + '"]');
			if (field.attr('type') == "checkbox") {
				$("#double-check").append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>' + data.fields[key] + '</p>');
			} else {
				field.parents(".form-group").addClass("has-error");
				field.parents(".form-group").append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>' + data.fields[key] + '</p>');
			}
		}
		generalOverlayLoading(false);
	 }
 }

$(document).ready(function() {
	
	$("#selectStato").change(function() {
		
		if ($(this).val() == "SM") {
			
			// CAP
			$("#InputCap").val("47890");
			$("#InputCap").addClass("filled");
			$("#InputCap").attr("readonly", true);
			
			// Citta
			$("#selectCitta option").remove();
			$("#selectCitta").append('<option value="San Marino">San Marino</option>');
			$("#selectCitta").removeAttr("disabled");
			$("#selectCitta").attr("readonly", true);
			
			// Province
			$("#selectProvincia option").remove();
			$("#selectProvincia").append('<option value="SM">San Marino</option>');
			$("#selectProvincia").removeAttr("disabled");
			$("#selectProvincia").attr("readonly", true);
			
		} else {
			
			$("select.dynamic").attr("disabled", true);
			replaceLocationSelect("#selectProvincia", "prov", "", "Selezionare una provincia", function() {
				$("#selectCitta option").remove();
				$("#selectCitta").append('<option value="">Selezionare una citt&agrave</option>');
				$("#selectCitta").removeAttr("readonly");
				$("#selectProvincia").removeAttr("readonly");
				$("#selectProvincia").removeAttr("disabled");
			},
			false);
			
			if ($("#InputCap").val() == "47890") {
				$("#InputCap").val("");
				$("#InputCap").removeClass("filled");
			}
			$("#InputCap").removeAttr("readonly");
		}
	});
	$("#selectProvincia").change(function() {
		
		var inputCap = $("#InputCap");
		
		// CAP
		if (inputCap.val() == "47890") {
			inputCap.val("");
			inputCap.removeClass("filled");
		}
		inputCap.removeAttr("readonly");
		
		// Citta
		$("select.dynamic").attr("disabled", true);
		replaceLocationSelect("#selectCitta", "city", $(this).val(), "Selezionare una citt&agrave", function(){
			$("select.dynamic").removeAttr("disabled");
		}, false);
	});
});
