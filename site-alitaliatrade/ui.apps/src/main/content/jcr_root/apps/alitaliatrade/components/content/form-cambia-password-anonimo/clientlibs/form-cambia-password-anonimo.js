$(document).ready(function() {
	initialize();
});

function initialize(){
	$("#btn_cambia").unbind('click',changePasswordAnonymous).bind('click', changePasswordAnonymous);
	retrieveParameters();
}

function changePasswordAnonymous(e) {
    validation(e, 'anonymouseditpassword', '#form-modifica-password', successPassword);
}

function successPassword(data) {
	removeErrors();
	if (data.result) {
		$('#btn_cambia').unbind('click').click();
		//$('#form-modifica-password').submit();
		$.ajax({
			url : cleanExtensioUrl() + ".anonymouseditpassword.json",
			method: "post",
			data : $('#form-modifica-password').serialize(),
			context : document.body
		})
		.done(function(data) {
			if (data.result) {
				if (data.changed) {
					$("#conferma").modal();
				} else {
                    var passwordIncorretta = Granite.I18n.get("PasswordIncorretta");
					$('[name="passwordOld"]').parent('div').addClass('has-error');
					$('[name="passwordOld"]').parent('div').append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>' + passwordIncorretta + '</p>');
				}
			}
			else {
				$("#errore").modal();
			}
		})
		.fail(function() {
			$("#errore").modal();
		});
	} else {
		for ( key in data.fields) {
			if (key == "username" || key == "ruolo") {
				$("#errore").modal();
			}
			else {
				$('[name="' + key + '"]').parent('div').addClass('has-error');
				$('[name="' + key + '"]').parent('div').append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>'+data.fields[key]+'</p>');
			}
			
		}
		//$('.anagrafica-content').prepend('<div class="alert alert-warning" role="alert"><i class="icon icon-alert-warning"></i>Formato password invalido</div>');
	}
	initialize();
}

function retrieveParameters() {
	var queryString = window.location.search;
	var parameters = queryString.split('&');
	var username = parameters[0].split('=');
	var role = parameters[1].split('=');
	$('#usr').val(username[1]);
	$('#rl').val(role[1]);	
}