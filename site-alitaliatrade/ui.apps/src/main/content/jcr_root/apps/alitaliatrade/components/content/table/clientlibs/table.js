function initCustomTable(table) {
	if ( $(table).find("thead").length === 0 ) {
		var thElements = $(table).find("tbody tr th").parent('tr');
		var thElement = $(thElements[0]);
		if (thElement.length > 0) {
			$(table).prepend("<thead>"+thElement.html()+"</thead>");
			thElement.remove();
		}
	}
}