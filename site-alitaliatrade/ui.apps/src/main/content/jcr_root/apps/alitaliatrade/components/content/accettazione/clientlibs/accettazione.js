$("#btn_procedi").bind('click', function(e) {
	validation(e, 'agencyacceptconditions', '#form-accettazione', successAccept);
});

$("#btn_login").bind('click', function(e) {
	$('#accettaCondizioni').modal("hide");
});


 
function successAccept(data) {
	
	removeErrors();
	if (data.result) {
		//$('#btn_procedi').unbind('click').click();
		var path = window.location.href;
		path = path.split("?")[0]; // remove querystring
		path = path.substring(0, path.length-4);
		$.ajax({
			url: path + 'agencyacceptconditions.json',
			data: {
				j_username: $('input[name=usertype]:checked').val() + ":" + $('#inputusername').val(),
				j_password: $('#inputpwd').val(),
				checkAccept : $('#checkAccept').val()
			},
			success: onAcceptResult,
			dataType: 'json',
			method: 'POST'
			
		});
	} else {
		
		var checkEr = false;
		for (var key in data.fields) {
			$("#check").append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>' + data.fields[key] + '</p>');
		}
	 }
 }

function onAcceptResult(data) {
	if (data.result) {
		$('#accettaCondizioni').modal("hide");
		startLogin();
	}
	else {
		$("#check").append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>' + 'Si è verificato un errore nella procedura di accettazione. Riprovare' + '</p>');
	}
}
