function getOffersDefaultCity(departure, isHomePage, isYoung, type) {
	
	var selector = ".partialContentOfferte" + (type == "INT" ? "Mondo" : (type == "DOM" ? "Ita" : ""));
	
	var url = location.protocol + '//' + location.host;
	var hp = $("#whichHomePage").text();
	var fixed = "/content/alitaliatrade/it/loggato";
	var urlAjax = url + fixed + "/" + hp + ".lista-offerte-partial.html";
		
	console.log(urlAjax);
	
	$.ajax({
		url : urlAjax,
		data : {
			from: departure,
			isHomePage: isHomePage,
			isYoung: isYoung,
			type: type
		},
		context : document.body
	})
	.done(function(data) {
		$(selector).empty();
		$(selector).append(data);
	})
	.fail(function() {
		console.log('error during offers retrieve');
	});
}

$(document).ready(function () {
	$(".selectPart").change(function(){
		var isHomePage = $(this).parent().find(".isHomePage").text();
		var isYoung = $(this).parent().find(".isYoung").text();
		var type = $(this).parent().find(".type").text();
		if (isHomePage) {
			if ($(this).parent().find(".offerteLink").text()) {
				window.location.href = $(this).parent().find(".offerteLink").text() + ".html?from=" + $(this).val();
			}
		} else {
			if ($(this).is("#italia .selectPart")){
				$("#mondo .selectPart").val($(this).val());
				getOffersDefaultCity($("#mondo .selectPart").val(), isHomePage, isYoung, $("#mondo .type").text());
			} else {
				$("#italia .selectPart").val($(this).val());
				getOffersDefaultCity($("#italia .selectPart").val(), isHomePage, isYoung, $("#italia .type").text());
			}
			getOffersDefaultCity($(this).val(), isHomePage, isYoung, type);
		}
	});
	
	$(".selectPart").each(function(){
		var isHomePage = $(this).parent().find(".isHomePage").text();
		var isYoung = $(this).parent().find(".isYoung").text();
		var type = $(this).parent().find(".type").text();
		getOffersDefaultCity($(this).val(), isHomePage, isYoung, type);
	});
});
