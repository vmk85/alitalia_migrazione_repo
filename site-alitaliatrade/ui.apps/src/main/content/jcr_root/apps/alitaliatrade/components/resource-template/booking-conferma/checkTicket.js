"use strict";
use (function(){
	if (this.passengers == null) {
		return false;
	}
	var nPasseggeri = this.passengers.size();
	for (var i=0;i<nPasseggeri;i++){
		if (this.passengers.get(i).tickets == null) {
			return false;
		}
		for (var j=0;j<this.passengers.get(i).tickets.size();j++) {
			if (this.passengers.get(i).tickets.get(j).ticketNumber == "" || 
				this.passengers.get(i).tickets.get(j).ticketNumber == null ||
				this.passengers.size() == 0 ||
				this.passengers.get(i).tickets.size() == 0) {
				return false;
			}
		}
	}
	return true;
});

