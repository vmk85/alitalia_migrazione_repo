$( document ).ready(function() {
	$.ajax({url: cleanExtensioUrl()+".agencyuser.json", success: function(result){
		if (result.username) $(".user-name").text("Agenzia " + result.username);
		else $(".user-name").text("Agenzia");
    }});
   
   $("#logout").bind("click",logout);
    
});

function logout(){
	$.ajax({
		url: cleanExtensioUrl() + ".agencylogout.json"
	})
	.done(function(data){
		window.location.replace(data.redirect);
	})
	.fail(function(){
		console.error("logout fail");
	});
}