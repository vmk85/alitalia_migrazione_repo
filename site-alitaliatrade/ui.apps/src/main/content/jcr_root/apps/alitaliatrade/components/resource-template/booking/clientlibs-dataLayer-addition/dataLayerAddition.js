var DTMRule = "";
jQuery(document).ready(function(){
	if(typeof window["analytics_booking_step"] !== "undefined" && 
			window["analytics_booking_step"] != "1"){
		analytics_bookingPartialCallback();
	}
});

function analytics_bookingPartialCallback(){
	if(typeof window["analytics_booking_step"] !== "undefined"){
		addBookingAnalyticsData(analytics_booking_step);
		activateGTMOnce("booking");
		activateWebtrendsOnce("booking");
		if(DTMRule){
			activateDTMRule(DTMRule);
		}
	}
}

function addBookingAnalyticsData(bookingStep){
		switch(bookingStep){
		case "0":
			addHomePageErrorBookingAnalyticsData();
			break;
		case "1":
			DTMRule = "SelectFlight";
			break;
		case "2":
			DTMRule = "PassengerEdit";
			break;
		case "3":
			DTMRule = "Payment";
			break;
		case "4":
			DTMRule = "Receipt";
		}
		if( typeof window["analytics_bookingInfo"] != "undefined"){
			/*network is loaded dynamically in step 1*/
			if(typeof window["analytics_network"] != "undefined"){
				analytics_bookingInfo["Network"] = analytics_network;
			}
			/*Sabre JSESSIONID is not available immediately on step 1*/
			if(typeof window["analytics_sabreSessionId"] != "undefined"){
				jQuery("meta[name='SabreSessionId']").attr("content", window["analytics_sabreSessionId"]);
			}
			/*IdOrd must not be present in step != 4*/
			if(bookingStep != "4"){
				delete analytics_bookingInfo["IdOrd"];
			}
			addAnalyticsData(analytics_bookingInfo);
		}
	}

	function addHomePageErrorBookingAnalyticsData(){
		var errorMessages = jQuery("#booking-error .text-page-title").text();
		if(errorMessages){
			addAnalyticsData({"BError": errorMessages});
			activateDTMRule("Error");
		}
	}	
	
	
	function analytics_trackBookingErrors(errors){
		var errorMessages = "";
		if(typeof errors === "object"){
			for(key in errors){
				errorMessages = errorMessages + key + " "  + errors[key] + "; ";
			}	
			if(errorMessages.length > 0)
				errorMessages = errorMessages.slice(0,-2);
		}
		else if (typeof errors === "string"){
			errorMessages = errors;
		}
		if(errorMessages.length > 0){
			pushAnalyticsData({"BError": errorMessages});
			activateDTMRule("Error");
		}
	}