$(document).ready(function() {
	$("#btn_newsletter_iata").bind('click', function(e) {
		generalOverlayLoading(true);
		validation(e, 'subscribenewsletteriata', '#form-newsletter-iata', successRegistrationNewsletterIATA);
	});
});

function successSubmitRegNewsletterIata(data) {
 	if (data.redirect) {
 		window.location.replace(data.redirect);
 	} else {
 		if (data.existing) {
 			$('[name="email"]').parent('div').addClass('has-error');
         	$('[name="email"]').parent('div').append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>Siamo spiacenti, ma l\'iscrizione non &egrave; andata a buon fine: l\'indirizzo email &egrave; gi&agrave; esistente</p>');
 		}
 		
 		if (data.notinserted) {
 		 	$('[name="email"]').parent('div').addClass('has-error');
         	$('[name="email"]').parent('div').append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i><i class="icon icon-escl-mark"></i>Siamo spiacenti, ma non &egrave; possibile soddisfare la tua richiesta. Ti invitiamo a controllare i dati richiesti per l\'iscrizione</p>');
 		}
 	}
 	generalOverlayLoading(false);
}
 
function failSubmitRegNewsletterIata(){
 	console.error("error");
 	generalOverlayLoading(false);
}

function successRegistrationNewsletterIATA(data) {
 	removeErrors();
     if (data.result) {
         performSubmit('subscribenewsletteriata', '#form-newsletter-iata', successSubmitRegNewsletterIata, failSubmitRegNewsletterIata);
     } else {
         for (var key in data.fields) {
         	$('[name="' + key + '"]').parent('div').addClass('has-error');
         	$('[name="' + key + '"]').parent('div').append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>' + data.fields[key] + '</p>');
         	if (key == "usertype") {
         		$('.row radio-button-container').append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>Per favore, controlla i dati inseriti</p>');
         	}
         }
         generalOverlayLoading(false);
     }
}

$(document).ready(function(){
	 var url = window.location.href;
	 if (url.indexOf('error') != -1) {
		 $('#exist').show();
		 $('#exist').parent('div').addClass('has-error');
	 }
});