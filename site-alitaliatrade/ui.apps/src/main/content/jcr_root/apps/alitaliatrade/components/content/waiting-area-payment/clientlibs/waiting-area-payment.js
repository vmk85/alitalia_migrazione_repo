/*
*****************BITMAMA JAVASCRIPT FOR SLIDING PALNE***************
*/
$.fn.animateRotate = function(angle, duration, easing, complete) {
  var args = $.speed(duration, easing, complete);
  var step = args.step;
  return this.each(function(i, e) {
    args.complete = $.proxy(args.complete, e);
    args.step = function(now) {
      $.style(e, 'transform', 'rotate(' + now + 'deg)');
      if (step) return step.apply(e, arguments);
    };

    $({deg: 0}).animate({deg: angle}, args);
  });
};
/*
*******************************************************************
*/
function startWaitingArea(){
	/*
	*****************BITMAMA JAVASCRIPT FOR SLIDING PALNE***************
	*/
	var tempoDiAttesa = 10000; //in millesimi di secondo
	plane = $(".plane-container>img");
    $(".linea-cover").animate({
        left: "100%" 
    }, tempoDiAttesa);
    $(".plane-container").animateRotate(60, tempoDiAttesa);
    $(".plane-container>img").animateRotate(-15, tempoDiAttesa);
    /*
    *******************************************************************
    */	
}