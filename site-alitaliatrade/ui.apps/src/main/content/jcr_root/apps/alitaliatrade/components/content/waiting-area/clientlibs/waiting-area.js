/*
*****************BITMAMA JAVASCRIPT FOR SLIDING PALNE***************
*/
$.fn.animateRotate = function(angle, duration, easing, complete) {
  var args = $.speed(duration, easing, complete);
  var step = args.step;
  return this.each(function(i, e) {
    args.complete = $.proxy(args.complete, e);
    args.step = function(now) {
      $.style(e, 'transform', 'rotate(' + now + 'deg)');
      if (step) return step.apply(e, arguments);
    };

    $({deg: 0}).animate({deg: angle}, args);
  });
};
/*
*******************************************************************
*/
(function($) {

	
	/*
	*****************BITMAMA JAVASCRIPT FOR SLIDING PALNE***************
	*/
	var tempoDiAttesa = 5000; //in millesimi di secondo
	plane = $(".plane-container>img");
    $(".linea-cover").animate({
        left: "100%" 
    }, tempoDiAttesa);
    $(".plane-container").animateRotate(60, tempoDiAttesa);
    $(".plane-container>img").animateRotate(-15, tempoDiAttesa);
    /*
    *******************************************************************
    */
		
	
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));

	var res_json = getJsonFromUrl();
	var isResidency = res_json.residency || "false";
	var status = 0;

	function getJsonFromUrl() {
		var query = location.search.substr(1);
		var result = {};
		query.split("&").forEach(function(part) {
			var item = part.split("=");
			result[item[0]] = decodeURIComponent(item[1]);
		});
		return result;
	}
	
	checkBookingNavigationProcess('flightsSearch',executeBookingSearch);
	
	function executeBookingSearch(){
		$("#alertFamily").hide();
		$("#alertYouth").hide();
		$("#alertRibbonMismatch").hide();
		$.ajax({
			url : url + ".bookingflightsearchservice.json?isResidency=" + isResidency + "&nocache=" + nocache(),
			context : document.body
		}).done(flightDone).fail(flightFail);
	}

	function flightDone(data) {
		if (data.redirect) {
			window.location.replace(data.redirect);
		}
		if (data.result) {
			if (data.solutionNotFound == "allSolutions") {
				window.location.replace(data.redirect);
			} else if (data.solutionNotFound == "youth") {
				$("#alertYouth").show();
			} else if (data.solutionNotFound == "family") {
				$("#alertFamily").show();
			} else if (data.solutionNotFound == "territorialContinuity") {
				$("#alertTerritorialContinuity").show();
			}
			var roundtrip = data.roundtrip
			$.ajax({
				url: url + ".partialLaTuaSelezione.html?nocache=" + nocache(),
				context: document.body
			}).done(function(data) {
				$(".my-selection-review").html(data);
				populateRibbon(url, roundtrip); 
				populateSelection(url, roundtrip); 
			});
			if (!data.roundtrip) {
				status += 2;
			}
			
		} else {
			//flightFail();
			window.location.replace(data.redirect);
		}
	}
	
	function populateRibbon(url, roundtrip) {
		$.ajax({ 
			url : url + ".partialribbon.html?direction=0&nocache=" + nocache(),
			context : document.body 
		})
		.done(function(data) { 
			$("#ribbon-outbound").html(data);
			if (roundtrip) {
				$.ajax({ 
					url : url + ".partialribbon.html?direction=1&nocache=" + nocache(),
					context : document.body 
				})
				.done(function(data) { 
					$("#ribbon-return").html(data);
					finallyDone(); 
				})
				.fail(flightFail);
			} 
			finallyDone(); 
		})
		.fail(flightFail);
	}
	
	function populateSelection(url, roundtrip) {
		$.ajax({ 
			url : url + ".partialselection.html?direction=0&nocache=" + nocache(),
			context : document.body 
		})
		.done(function(data) { 
			$("#flights-outbound").html(data);
			analytics_bookingPartialCallback();
			if (roundtrip) {
				$.ajax({ 
					url : url + ".partialselection.html?direction=1&nocache=" + nocache(),
					context : document.body 
				})
				.done(function(data) { 
					$("#flights-return").html(data);
					finallyDone(); 
				})
				.fail(flightFail);
			} 
			finallyDone(); 
		})
		.fail(flightFail);
	}

	function finallyDone() {
		if ((++status) === 4) {
			$("#tab-andata-3").attr("class","active");
			$("#tab-ritorno-3").attr("class","active");
			$('#booking-navbar').show();
			$('#booking-search').show();
			$('#booking-content').show();
			$('#prefooter').show();
			refreshBindings();
			$('#booking-waiting').remove();
		}
	}

	function flightFail() {
		console.log('fail: unexpected Error');
	}

})(jQuery);
