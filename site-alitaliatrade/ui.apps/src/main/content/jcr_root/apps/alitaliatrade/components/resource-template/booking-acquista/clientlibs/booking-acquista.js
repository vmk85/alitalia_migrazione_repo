$(document).ready(function() {
	checkBookingNavigationProcess("payment");
	if($("#paymentErrorMessage").length > 0){
		var errorMsg = $("#paymentErrorMessage").text().trim();
		if(typeof window["analytics_trackBookingErrors"] === "function"){
			window["analytics_trackBookingErrors"](errorMsg);
		}
	}
});

$('#InputTipologiaCarta').bind('change', function() { 
	removeErrors();
	resetCDCFormPaymentChangeCreditCard();
	manageCreditCards(this); 
});
manageCreditCards();

$("#CDCOption").bind("click",function(){
	removeErrors(); 
	resetFormPayment();
});
$("#BonificoOption").bind("click",function(){
	removeErrors();
	resetFormPayment();
});

function resetFormPayment(){
	resetCDCFormPayment();
	resetBonificoFormPayment();
}
function resetCDCFormPaymentChangeCreditCard(){
	$("#booking-acquista-cdc-form :text").val("");
	$("#booking-acquista-cdc-form #paeseFattura option:first").attr("selected",true);
	if ($("#booking-acquista-cdc-form input[name='richiediFattura']:checked").length > 0) {
		$("#abilitaFatturaCDC").click();
	}
}

function resetCDCFormPayment(){
	$("#booking-acquista-cdc-form #InputTipologiaCarta option:first").attr("selected",true);
	$("#booking-acquista-cdc-form :text").val("");
	$("#booking-acquista-cdc-form #paeseFattura option:first").attr("selected",true);
	if ($("#booking-acquista-cdc-form input[name='richiediFattura']:checked").length > 0) {
		$("#abilitaFatturaCDC").click();
	}
}

function resetBonificoFormPayment(){
	$("#booking-acquista-bonifico-form :text").val("");
	$("#booking-acquista-bonifico-form #paeseFattura option:first").attr("selected",true);
	if ($("#booking-acquista-bonifico-form input[name='richiediFattura']:checked").length > 0) {
		$("#abilitaFatturaBonifico").click();
	}
}

$('#paeseFattura').bind('change', function() { manageInvoice(this, 1); } );
$('#paeseFattura2').bind('change', function() { manageInvoice(this, 2); } );
$('#booking-acquista-cdc-form input[name="tipoIntestatario"]').bind('change', 
		function() { manageBilling(this, 1); } );
$('#booking-acquista-bonifico-form input[name="tipoIntestatario"]').bind('change', 
		function() { manageBilling(this, 2); } );
manageInvoice();
manageBilling();

$("#booking-acquista-cdc-submit").bind("click", acquistaCDCSubmit);

function acquistaCDCSubmit(e) {
	$("#booking-acquista-cdc-submit").unbind("click", acquistaCDCSubmit);
	validation(e, "bookingpurchasedata", "#booking-acquista-cdc-form", function(data) { 
		donePurchaseData(data, "booking-acquista-cdc-form");
	}, failValidationBookingPurchase);
}

$("#booking-acquista-bonifico-submit").bind("click", acquistaBonificoSubmit);

function acquistaBonificoSubmit(e) {
	$("#booking-acquista-bonifico-submit").unbind("click", acquistaBonificoSubmit);
	validation(e, "bookingpurchasedata", "#booking-acquista-bonifico-form", function(data) {
		donePurchaseData(data, "booking-acquista-bonifico-form");
	}, failValidationBookingPurchase);
}

function failValidationBookingPurchase(){
	window.location.replace($("#redirectFailureValidationPage").val());
}

function donePurchaseData(data, selector) {
	removeErrors();
	if (data.result) {
		doPayment(selector);
	} else {
		showErrors(data);
		if (selector == "booking-acquista-bonifico-form") {
			$("#booking-acquista-bonifico-submit").bind("click", acquistaBonificoSubmit);
		} else {
			$("#booking-acquista-cdc-submit").bind("click", acquistaCDCSubmit);
		}
	}
}

function showErrors(data) {
	var analytics_msg = data.fields;
	if (data.fields) {
		for (var key in data.fields) {
			var field = $('[name="' + key + '"]');
			if (field.attr("type") == "radio") {
				field.parents(".radio").addClass("has-error");
			} else {
				field.parents(".form-group").addClass("has-error");
				field.parents(".form-group").append(
					'<p class="help-block alert-block">' +
					'<i class="icon icon-escl-mark"></i>' +
					data.fields[key] + '</p>'
				);
			}			
		}
	}
	
	var message = "";
	if (data.message) {
		message = data.message;
		analytics_msg = message;
	} else {
		message = 'Attenzione, uno o pi&ugrave; campi non sono stati inseriti correttamente';
	}
	
	$(".booking-main-content").prepend(
		'<div class="alert alert-warning" role="alert">' +
		'<i class="icon icon-alert-warning"></i>' + message + '</div>'
	);
	if(window["analytics_trackBookingErrors"] 
			&& typeof window["analytics_trackBookingErrors"] === "function"){
		window["analytics_trackBookingErrors"](analytics_msg);
	}
}

function doPayment(selector) {
	$('#booking-waiting').show();
	$('#booking-waiting .' + selector).show();
	$('.booking-search').hide();
	$('.booking-table').hide();
	$('.booking-main-content').hide();
	$('.regole-tariffarie').hide();
	$('.more-details-header').hide();
	$('.navbar-booking').hide();
	$('.header-top').scroll();
	startWaitingArea();
	beirut(
			function (beirutString) {
				doPayment2(selector, beirutString);
			});
	
}

function doPayment2(selector, beirutString){
	$('#' + selector + " input[name='beirutString']").val(beirutString);
	performSubmit('bookingpurchasedata', '#' + selector, function(data){
			doneDoPayment(data,selector);
		}, 
		function () {
			var isCDC = true;
			if (selector == "booking-acquista-bonifico-form") {
				isCDC = false;
			}
			failDoPayment(isCDC);
		});
}

function doneDoPayment(data,selector) {
	if (data.complete != undefined) {
	    if(data.complete.redirect){
        window.location.replace(data.complete.redirect);
        }
    }
	if (data.redirect) {
		window.location.replace(data.redirect);
	} else if (!data.result) {
		showErrors(data);
		hideWaiting(data);
		if (selector == "booking-acquista-bonifico-form") {
			$("#booking-acquista-bonifico-submit").bind("click", acquistaBonificoSubmit);
		} else {
			$("#booking-acquista-cdc-submit").bind("click", acquistaCDCSubmit);
		}
	} else if (data.complete) {
		doCompletePayment(data.complete);
	} else {
		hideWaiting(data);
		if (selector == "booking-acquista-bonifico-form") {
			$("#booking-acquista-bonifico-submit").bind("click", acquistaBonificoSubmit);
		} else {
			$("#booking-acquista-cdc-submit").bind("click", acquistaCDCSubmit);
		}
	}
}

function doCompletePayment(data) {
	
	if (data.method == 'GET'){
		window.location.replace(data.redirect);
	} else {
	    var newForm = $('<form>', {
	        'action': data.redirect,
	        'method': 'POST'
	    })
	    for (var key in data.params) {
	        if (data.params.hasOwnProperty(key)) {
	        	newForm.append($('<input>', {
	    	        'name': key,
	    	        'value': data.params[key],
	    	        'type': 'hidden'
	    	    }));
	         }
	    }
	    newForm.submit();
	}
}

function failDoPayment(isCDC) {
	var redirectPage;
	if (isCDC) {
		redirectPage = $("#redirectFailurePageCDC").val();
	} else {
		redirectPage = $("#redirectFailurePageBonifico").val();
	}
	window.location.replace(redirectPage);
}

function hideWaiting(data) {
	$('#booking-waiting').hide();
	$('#booking-waiting .header-inner').hide();
	$('.booking-search').show();
	$('.booking-table').show();
	$('.booking-main-content').show();
	$('.regole-tariffarie').show();
	$('.more-details-header').show();
	$('.navbar-booking').show();
}

function manageCreditCards(e) {
	if (e && $(e) && $(e).val() && $(e).val() == 'AmericanExpress') {
		$('.extrafields').show();
	} else {
		$('.extrafields').hide();
	}
}

function manageInvoice(e, i) {
	if (e && $(e) && $(e).val() && $(e).val() == 'IT') {
		$('.extrafield' + i).show();
	} else {
		if (i) {
			$('.extrafield' + i).hide();
		} else {
			$('.extrafield').hide();
		}
	}
}

function manageBilling(e, i) {
	if (e && $(e) && $(e).val() && $(e).val() == 'PF') {
		$('.intFattura' + i).hide();
	} else {
		$('.intFattura' + i).show();
	}
}