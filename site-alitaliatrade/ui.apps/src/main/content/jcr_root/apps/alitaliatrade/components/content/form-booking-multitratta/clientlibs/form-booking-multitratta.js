function changeValuesMultitratta() {
	if ($("#nbabies-multi")) {
		var nadults = $('#nadults-multi').val();
		var nbabies = '<option value="0">0</option>';
		for (var i = 1; i <= nadults; i++) {
			nbabies += '<option value="' + i + '">' + i + '</option>';
		}
		$("#nbabies-multi").html(nbabies);
	}
	if ($("#nkids-multi")) {
		var nkids = '<option value="0">0</option>';
		for (var i = 1; i <= (7 - nadults); i++) {
			nkids += '<option value="' + i + '">' + i + '</option>';
		}
		$("#nkids-multi").html(nkids);
	}
}

$("#nadults-multi").bind('change', changeValuesMultitratta);
changeValuesMultitratta();

$(document).ready(function () {	
	$('#booking-multitratta-submit').bind('click', function(e) {
		addNameAttribute();		
		validation(e, 'bookingmultilegflightsearch', '#formBookingMultitratta', successBookingMultitratta);
	});	
});

function successBookingMultitratta(data) {
	$("#formBookingMultitratta .error-form").removeClass("error-form");
	if (data.result) {
		$('#booking-multitratta-submit').unbind('click').click();
		$('#formBookingMultitratta').submit();
		//cleanFields();
	} else {
		for ( var key in data.fields) {
			var selector = '[name="' + key + '"]';
			if ($(selector).css('display') == 'none') {
				selector = $(selector).next();
			}
			// gestione per multitratta
			if ($(selector).parents(".mt-edit").hasClass("hidden")) {
				$(selector).parents(".mt-cont").find(".mt-completed").addClass("error-form");
			}
			$(selector).addClass('error-form');
		}
	}
}

function addNameAttribute() {
	$('[id]').each(function(i, field) {
		if (field.getAttribute('name') == null) {
			field.setAttribute('name', field.getAttribute('id'));
		}		
	});
}

/*function cleanFields() {
	$('input.toToggle').each(function(i, inputDestination) {
		var $inputDestination = $(inputDestination);
		$inputDestination.val("");
	});
}*/

$(function() {
	$("#flighttype3").click();
});