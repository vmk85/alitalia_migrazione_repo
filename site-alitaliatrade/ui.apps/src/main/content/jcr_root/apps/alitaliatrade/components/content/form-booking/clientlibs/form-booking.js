function getAirportsList() {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	$('#departureInput').prop('disabled', true);
	$('#arrivalInput').prop('disabled', true);
	$.ajax({
		url : url + ".airportslist.json",
		context : document.body
	}).done(function(data) {
		all_airports = data.airports;
		var partenza = getParameterByName('Dep');
		var arrivo = getParameterByName('Arr');
		if (partenza && arrivo){
			$('#departureInput').val(partenza);
			$('#departureInput').blur();
			$('#departureInput').change();
			$('#departureInput').siblings('.autocomplete-suggestions').find('#sugg_dest_dep_' + partenza).click();
			$('#arrivalInput').val(arrivo);
			$('#arrivalInput').blur();
			$('#arrivalInput').change();
			$('#arrivalInput').siblings('.autocomplete-suggestions').find('#sugg_dest_arr_' + arrivo).click();
		}
	}).always(function() {
		$('#departureInput').prop('disabled', false);
		$('#arrivalInput').prop('disabled', false);
	});
}

function copyValue(v) {
	var c = {
		"value" : v.value,
		"city" : v.city,
		"country" : v.country,
		"airport" : v.airport,
		"type" : v.type,
		"code" : v.code,
		"best" : v.best
	};
	return c;
}

function findAirport(textInput) {
	if (textInput && textInput.length >= 3) {
		var size = 6;
		airports.suggestions = [];
		airports.keys = [];
		var regExString = "^" + textInput + ".*"
		$.each(all_airports, function(i, jsonValue) {
			if (airports.keys.length < size && jsonValue.city.search(new RegExp(regExString, "i")) != -1) {
				if (-1 === airports.keys.indexOf(jsonValue.code)) {
					airports.keys.push(jsonValue.code);
					airports.suggestions.push(copyValue(jsonValue));
				}
			}
		});
		$.each(all_airports, function(i, jsonValue) {
			if (airports.keys.length < size && jsonValue.airport.search(new RegExp(regExString, "i")) != -1) {
				if (-1 === airports.keys.indexOf(jsonValue.code)) {
					airports.keys.push(jsonValue.code);
					airports.suggestions.push(copyValue(jsonValue));
				}
			}
		});
		if (textInput.length == 3) {
			$.each(all_airports, function(i, jsonValue) {
				if (airports.keys.length < size && jsonValue.code.search(new RegExp(regExString, "i")) != -1) {
					if (-1 === airports.keys.indexOf(jsonValue.code)) { 
						airports.keys.push(jsonValue.code);
						airports.suggestions.push(copyValue(jsonValue));
					}
				}
			});
		}
		return true;
	}
	return false;
}

function changeValues() {
	if ($("#nbabies")) {
		var nadults = $('#nadults').val();
		var nbabies = '<option value="0">0</option>';
		for (var i = 1; i <= nadults; i++) {
			nbabies += '<option value="' + i + '">' + i + '</option>';
		}
		$("#nbabies").html(nbabies);
	}
	if ($("#nkids")) {
		var nkids = '<option value="0">0</option>';
		for (var i = 1; i <= (7 - nadults); i++) {
			nkids += '<option value="' + i + '">' + i + '</option>';
		}
		$("#nkids").html(nkids);
	}
}

function successBooking(data) {
	$("#form .error-form").removeClass('error-form');
	if (data.result) {
		$('#cerca-booking').unbind('click').click();
		$("#form").submit();
	} else {
		for ( var key in data.fields) {
			var selector = '[name="' + key + '"]';
			if ($(selector).css('display') == 'none') {
				selector = $(selector).next();
			}
			$(selector).addClass('error-form');
		}
		if(window["analytics_trackBookingErrors"] 
			&& typeof window["analytics_trackBookingErrors"] === "function"){
				window["analytics_trackBookingErrors"](analytics_msg);
		}
		else{
			console.error("function analytics_trackBookingErrors not found");
		}
	}
}

function errorBooking() {
	$('#cerca-booking').unbind('click').click();
}

var all_airports;

$("#nadults").bind('change', changeValues);
$("#cerca-booking").bind('click', function(e) {
	validation(e, 'flightsearch', '#form', successBooking, errorBooking);
});


getAirportsList();
$(function() {
	if($("#ricerca_abilitata").length == 0) {
		//$("#flighttype2").click();
		changeValues();
	}
});