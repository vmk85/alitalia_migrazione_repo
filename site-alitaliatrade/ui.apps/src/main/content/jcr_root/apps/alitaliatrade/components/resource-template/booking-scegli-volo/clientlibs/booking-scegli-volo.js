var countOverlay = 0;
$(document).ready(function() {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	refreshCouponAfterSelection(url,true);
	$('#btn_modifica_ricerca').bind('click', function(e) {
		$('#ricerca_disabilitata').hide();
		$('#ricerca_abilitata').show();
	});
});

function validationSelection() {
	var requiredSelectionsCount = 0;
	if ($("input[name='andataSelection']").length > 0) {
		requiredSelectionsCount++;
	}
	if ($("input[name='ritornoSelection']").length > 0) {
		requiredSelectionsCount++;
	}
	var performedSelectionsCount =
		$("input[name='andataSelection']:checked").length +
		$("input[name='ritornoSelection']:checked").length;
	return (performedSelectionsCount == requiredSelectionsCount);
}

function performFinalSubmit(e) {
	toggleFlightSelectionOverlay(true);
	var valid = validationSelection();
	if (valid) {
		validation(e, 'bookingflightdata', '#booking-flight-data-form',
			function(data) {
				if (data.result) {
					$('#booking-flight-data-form').attr('action', getServiceUrl('bookingflightdata', 'html')); 
					$('#booking-flight-data-form').submit();
				}
			});
	}
}

function performNextSlice(e) {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	var valid = validationSelection();
	if (valid) {
		toggleFlightSelectionOverlay(true);
		solutionId = $("input[name='andataSelection']:checked").val();
		$.ajax({ //refresh matrice outbound
			url: url + ".bookingflightdata",
			data : "andataSelection=" + solutionId,
			method: "POST",
			context: document.body
		}).done(function(data) {
			$.ajax({ //refresh matrice outbound
				url: url + ".partialselection.html?direction=0",
				context: document.body
			}).done(function(data) {
				$("#flights-outbound").html(data);
				$.ajax({ // refresh partial box la tua selezione
					url: url + ".partialLaTuaSelezione.html",
					context: document.body
				}).done(function(data) {
					$(".my-selection-review").html(data);
					$.ajax({ //refresh ribbon 
						url: url + ".partialribbon.html?direction=0",
						context: document.body
					}).done(function(data) {
						$("#ribbon-outbound").html(data);
					}).fail(function() {
						console.error("Fail Ribbon refresh");
					}).always(function() {
						refreshBindings();
						toggleFlightSelectionOverlay(false);
					});
						
				}).fail(function() {
					console.error("Error Partial selection");
					refreshBindings();
					toggleFlightSelectionOverlay(false);
					//redirect
				});
			}).fail(function(){
				refreshBindings();
				toggleFlightSelectionOverlay(false);
				console.error("Error Partial selection");
				//redirect
			});
		}).fail(function(){
			refreshBindings();
			toggleFlightSelectionOverlay(false);
			console.error("Error Partial selection");
			//redirect
		});
		
	}
}

function toggleDirectFlightsOnly() {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	toggleFlightSelectionOverlay(true);
	
	var indexRoute = 0;
	var id = "directflightsOutbound";
	var panel = "panelAndata";
	var selection = "andataSelection";
	var matrix = "flights-outbound"
	var slice = "andata";
	if ( $(this).attr("id") == "directflightsReturn" ){
		indexRoute = 1;
		id = "directflightsReturn";
		panel = "panelRitorno";
		matrix = "flights-return";
		selection = "ritornoSelection";
		slice = "ritorno";
	}
	if ( $("#" + id + ":checked").length == 0  ) { //checkbox gia selezionata
		$.ajax({ // refresh partial box la tua selezione
			url: url + ".partialselection.html?direction=" + indexRoute + "&nocache=" + nocache(),
			context: document.body
		}).done(function(data) {
			$("#" + matrix).html(data);
			var selectedFlight = $("input[name='" + selection + "']:checked");
			if ( selectedFlight.length > 0 ) {
				id = selectedFlight.attr("id");
				indexFlight = id.substring(0, id.lastIndexOf("-"));
				selectedFlight.parents(".flight-row").children(".flight-info").click();
				if (slice == "andata") {
					showFlightRowOutbound(null, "#flight-info-" + slice + "-" + indexFlight);
				} else {
					showFlightRowReturn(null, "#flight-info-" + slice + "-" + indexFlight);
				}
							}
		}).fail(function() {
			console.error("Error Refresh selection");
		}).always(function(){
			refreshBindings();
			toggleFlightSelectionOverlay(false);
		});
	} else {
		$.ajax({
			url: url + ".bookinghidedirectflight.json?nocache=" + nocache(),
			data: "direction=" + indexRoute,
			context: document.body
		}).done(function(data) {
			if(data.redirect){
				window.location.replace(data.redirect);
			}else{ //success
				if (data.invalid) { //alcune soluzioni sono state invalidate
					$.ajax({ // refresh partial box la tua selezione
						url: url + ".partialLaTuaSelezione.html?nocache=" + nocache(),
						context: document.body
					}).done(function(data) {
						$(".my-selection-review").html(data);
						$("#" + panel + " .extra-tratte .flight-info").not(".collapsed").click();
						$("#" + panel + " .extra-tratte").toggle();
					}).fail(function() {
						console.error("Error Refresh Box La Tua Selezione");
						toggleFlightSelectionOverlay(false);
					}).always(function(){
						refreshBindings();
						refreshCouponAfterSelection(url);
						toggleFlightSelectionOverlay(false);
					});
				} else { //erano stati selezionati connecting
					$("#" + panel + " .extra-tratte .flight-info").not(".collapsed").click();
					$("#" + panel + " .extra-tratte").toggle();
					toggleFlightSelectionOverlay(false);
				}
			}
		}).fail(function() {
			console.error("Error Hide Direct Flights");
			toggleFlightSelectionOverlay(false);
			refreshBindings();
			//redirect
		});
	}
}

function refreshBindings() {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	toggleFlightSelectionOverlay(true); 
	$.ajax({
		url: url + ".isReadyToPassengersDataPhase.json?nocache=" + nocache(),
		context: document.body
	}).done(function(data) {
		if (data.redirect) {
			window.location.replace(data.redirect);
		}
		$("#booking-flight-data-submit").removeAttr("href");
		if(data.ready) {
			$("#booking-flight-data-submit").unbind('click', performFinalSubmit);
			$("#booking-flight-data-submit").bind('click', performFinalSubmit);
		} else {
			if (data.func) {
				$("#booking-flight-data-submit").unbind('click', performNextSlice);
				$("#booking-flight-data-submit").bind('click', performNextSlice);
			} else { //ROUNDTRIP or SIMPLE not selected all
				$("#booking-flight-data-submit").unbind('click',performFinalSubmit);
				$("#booking-flight-data-submit").unbind('click',performNextSlice);
				if ($("input[name='ritornoSelection']:checked").length == 0 ) {
					$("#booking-flight-data-submit").attr("href","#targetButtonReturnFlight");
				} else {
					$("#booking-flight-data-submit").attr("href","#targetButtonOutboundFlight");
				}
			}
		}
	}).fail(function(){
		console.error("Error isReadyToPassengersDataPhase");
	}).always(function(){
		toggleFlightSelectionOverlay(false); 
	});

	$('#directflightsOutbound').unbind('click', toggleDirectFlightsOnly);
	$('#directflightsOutbound').bind('click', toggleDirectFlightsOnly);
	
	$('#directflightsReturn').unbind('click', toggleDirectFlightsOnly);
	$('#directflightsReturn').bind('click', toggleDirectFlightsOnly);

	$(".flight-info.andata").unbind('click', showFlightRowOutbound);
	$(".flight-info.andata").bind('click', showFlightRowOutbound);

	$(".flight-info.ritorno").unbind('click', showFlightRowReturn);
	$(".flight-info.ritorno").bind('click', showFlightRowReturn);

	refreshBindingsTab();

	$("input[name='andataSelection']").unbind('click', performSelctionOutbound);
	$("input[name='andataSelection']:not(:checked)").bind('click', performSelctionOutbound);

	$("input[name='ritornoSelection']").unbind('click', performSelctionReturn);
	$("input[name='ritornoSelection']:not(:checked)").bind('click', performSelctionReturn);
	
	$('#moreflights-andata').unbind('click', showMoreFligths);
	$('#moreflights-andata').bind('click', showMoreFligths);
	
	$('#moreflights-ritorno').unbind('click', showMoreFligths);
	$('#moreflights-ritorno').bind('click', showMoreFligths);
}

function refreshBindingsTab() {
	var tabsAndata = $("#tab-andata").children();
	var tabsRitorno = $("#tab-ritorno").children();
	for (var i = 0; i < tabsAndata.length; i++) {
		$(tabsAndata[i]).unbind("click", changeTabAndata);
	}
	for (var i = 0; i < tabsRitorno.length; i++) {
		$(tabsRitorno[i]).unbind("click", changeTabRitorno);
	}
	for (var i = 0; i < tabsAndata.length; i++) {
		if (!$(tabsAndata[i]).hasClass("active") && !$(tabsAndata[i]).hasClass("notBrand")) {
			$(tabsAndata[i]).bind('click', changeTabAndata);
		}
	}
	for (var i = 0; i < tabsRitorno.length; i++) {
		if (!$(tabsRitorno[i]).hasClass("active") && !$(tabsRitorno[i]).hasClass("notBrand")) {
			$(tabsRitorno[i]).bind('click', changeTabRitorno);
		}
	}
}

function showFlightRowOutbound(e, selector) {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	var indexRoute = 0;
	var t = (selector) ? selector : this;
	var target = $(t).attr("href");
	var indexFlight = target.substring(target.lastIndexOf("-") + 1, target.length);
	var p = $('#flight-info-andata-' + indexFlight).parent();
	var selectedFlight = ($('input:checked', p).length > 0) ? "true" : "";
	$(".flight-info.andata:not(.collapsed):not(#" + $(t).attr("id") + ")").click();
	toggleFlightSelectionOverlay(true);
	$.ajax({
		url: url + ".travelinfo.html?nocache=" + nocache(),
		data: "indexRoute=" + indexRoute + "&indexFlight=" + indexFlight +
				"&selectedFlight=" + selectedFlight,
		context: document.body
	})
	.done(function(data) {
		$("#travel-info-andata-" + indexFlight).empty().html(data);
		if(!selectedFlight){
			$("#travel-info-andata-" + indexFlight + " .miles").remove();
		}
	})
	.fail(function() {
		console.error("Fail Partial Travel Info");
	}).always(function() {
		toggleFlightSelectionOverlay(false);
	});
}

function showFlightRowReturn(e, selector) {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	var indexRoute = 1;
	var t = (selector) ? selector : this;
	var target = $(t).attr("href");
	var indexFlight = target.substring(target.lastIndexOf("-") + 1, target.length);
	var p = $('#flight-info-ritorno-' + indexFlight).parent();
	var selectedFlight = ($('input:checked', p).length > 0) ? "true" : "";
	$(".flight-info.ritorno:not(.collapsed):not(#" + $(t).attr("id") + ")").click();
	toggleFlightSelectionOverlay(true);
	$.ajax({
		url: url + ".travelinfo.html?nocache=" + nocache(),
		data: "indexRoute=" + indexRoute + "&indexFlight=" + indexFlight +
				"&selectedFlight=" + selectedFlight,
		context: document.body
	})
	.done(function(data) {
		$("#travel-info-ritorno-" + indexFlight).empty().html(data);
		if(!selectedFlight){
			$("#travel-info-ritorno-" + indexFlight + " .miles").remove();
		}
	})
	.fail(function() {
		console.error("Fail Partial Travel Info");
	}).always(function() {
		toggleFlightSelectionOverlay(false);
	});
}

function changeTabAndata(e) {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	var selectedTab = $(this).attr("id");
	toggleFlightSelectionOverlay(true);
	$.ajax({
		url: url + ".bookingchangetab.json?nocache=" + nocache(),
		data: "selectedTab=" + selectedTab + "&direction=0",
		context: document.body
	})
	.done(function(data) {
		checkAvailableSolution(data);
		changeFanPlayrRibbonClickData(data);
		$.ajax({ // refresh partial box la tua selezione
			url: url + ".partialLaTuaSelezione.html?nocache=" + nocache(),
			context: document.body
		}).done(function(data) {
			$(".my-selection-review").html(data);
			$.ajax({ //refresh matrice outbound
				url: url + ".partialselection.html?direction=0" + "&nocache=" + nocache(),
				context: document.body
			}).done(function(data) {
				$("#flights-outbound").html(data);
				if ($("#flights-return").length > 0) {
					$.ajax({ //refresh matrice return
						url: url + ".partialselection.html?direction=1" + "&nocache=" + nocache(),
						context: document.body
					}).done(function(data) {
						$("#flights-return").html(data);
						refreshBindings();
						refreshCouponAfterSelection(url);
						var returnedSelected = $("input[name='ritornoSelection']:checked");
						if ( returnedSelected.length > 0 ) {
							returnedSelected.parents(".flight-row").children(".flight-info").click();
						}
						$.ajax({ //refresh ribbon return
							url: url + ".partialribbon.html?direction=1" + "&nocache=" + nocache(),
							context: document.body
						}).done(function(data) {
							$("#ribbon-return").html(data);
							$.ajax({ //refresh ribbon outbound
								url: url + ".partialribbon.html?direction=0" + "&nocache=" + nocache(),
								context: document.body
							}).done(function(data) {
								$("#ribbon-outbound").html(data);
							}).fail(function() {
								console.error("FAIL REFRESH RIBBON CHANGE TAB");
							}).always(function() {
								refreshBindings();
								toggleFlightSelectionOverlay(false);
							});
						}).fail(function() {
							console.error("FAIL REFRESH RIBBON CHANGE TAB");
						});
					})
					.fail(function() {
						console.error("FAIL REFRESH SELECTION 1 CHANGE TAB");
						toggleFlightSelectionOverlay(false);
						refreshBindings();
					});
				} else {
					$.ajax({ //refresh ribbon outbound
						url: url + ".partialribbon.html?direction=0" + "&nocache=" + nocache(),
						context: document.body
					}).done(function(data) {
						$("#ribbon-outbound").html(data);
					}).fail(function() {
						console.error("FAIL REFRESH RIBBON CHANGE TAB");
					}).always(function() {
						refreshBindings();
						refreshCouponAfterSelection(url);
						toggleFlightSelectionOverlay(false);
					});
				}
			})
			.fail(function() {
				console.error("FAIL REFRESH SELECTION 0 CHANGE TAB");
				toggleFlightSelectionOverlay(false);
				refreshBindings();
			});
			
		}).fail(function() {
			console.error("FAIL REFRESH BOX LA TUA SELEZIONE");
			toggleFlightSelectionOverlay(false);
			refreshBindings();
		});
	})
	.fail(function() {
		console.error("FAIL CHANGE OUTBOUND TAB");
		toggleFlightSelectionOverlay(false);
		refreshBindings();
	})
}

function changeTabRitorno(e) {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	var selectedTab = $(this).attr("id");
	toggleFlightSelectionOverlay(true);
	$.ajax({
		url: url + ".bookingchangetab.json",
		data: "selectedTab=" + selectedTab + "&direction=1",
		context: document.body
	})
	.done(function(data) {
		checkAvailableSolution(data);
		changeFanPlayrRibbonClickData(data);
		$.ajax({ //refresh box la tua selezione
			url: url + ".partialLaTuaSelezione.html?nocache=" + nocache(),
			context: document.body
		}).done(function(data) {
			$(".my-selection-review").html(data);
			$.ajax({  //refresh matrice return
				url: url + ".partialselection.html?direction=1" + "&nocache=" + nocache(),
				context: document.body
			}).done(function(data) {
				$("#flights-return").html(data);
				$.ajax({ //refresh matrice outbound
					url: url + ".partialselection.html?direction=0" + "&nocache=" + nocache(),
					context: document.body
				}).done(function(data) {
					$("#flights-outbound").html(data);
					refreshBindings();
					var outboundSelected = $("input[name='andataSelection']:checked");
					if ( outboundSelected.length > 0 ) {
						performSelctionOutbound();
						outboundSelected.parents(".flight-row").children(".flight-info").click();
					}
					$.ajax({ //refresh ribbon outbound
						url: url + ".partialribbon.html?direction=0" + "&nocache=" + nocache(),
						context: document.body
					}).done(function(data) {
						$("#ribbon-outbound").html(data);
						$.ajax({ //refresh ribbon outbound
							url: url + ".partialribbon.html?direction=1" + "&nocache=" + nocache(),
							context: document.body
						}).done(function(data) {
							$("#ribbon-return").html(data);
						}).fail(function() {
							console.error("FAIL REFRESH RIBBON CHANGE TAB");
						}).always(function() {
							toggleFlightSelectionOverlay(false);
							refreshBindings();
							refreshCouponAfterSelection(url);
						});
					}).fail(function() {
						console.error("FAIL REFRESH RIBBON CHANGE TAB");
					});
				}).fail(function() {
					console.error("FAIL REFRESH 0 SELECTION CHANGE TAB");
					toggleFlightSelectionOverlay(false);
					refreshBindings();
				});
			})
			.fail(function() {
				console.error("FAIL REFRESH 1 SELECTION CHANGE TAB");
				toggleFlightSelectionOverlay(false);
				refreshBindings();
			});
		}).fail(function() {
			console.error("FAIL REFRESH BOX LA TUA SELEZIONE AJAX");
			toggleFlightSelectionOverlay(false);
			refreshBindings();
		});
	})
	.fail(function() {
		console.error("FAIL change tab ritorno");
		toggleFlightSelectionOverlay(false);
		refreshBindings();
	});
}

function toggleFlightSelectionOverlay(active) {
	if (active) {
		countOverlay++;
		if ($("#overlayLoading").length == 0 ) {
			var overlayLoading = document.createElement("div");
			$(overlayLoading).attr("id", "overlayLoading");
			$(overlayLoading).attr("class", "overlayLoading");
			$("#mainContainer").append(overlayLoading);
		}
	} else {
		countOverlay--;
		if ( countOverlay == 0) {
			$("#overlayLoading").remove();
		} else if (countOverlay < 0) {
			countOverlay = 0;
		}
	}
}

function performSelctionOutbound() {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	var solutionId;
	var id;
	var indexFlight;
	var indexBrand;
	var indexRoute = 0;
	if ($("input[name='andataSelection']:checked").size() > 0) {
		solutionId = $("input[name='andataSelection']:checked").val();
		id = $("input[name='andataSelection']:checked").attr("id");
		indexFlight = id.substring(0, id.lastIndexOf("-"));
		indexBrand = id.substring(id.lastIndexOf("-") + 1, id.length);
		toggleFlightSelectionOverlay(true);
		$.ajax({
			url: url + ".bookingrefreshsearch.json?nocache=" + nocache(),
			data: "direction=0&indexFlight=" + indexFlight + "&indexBrand=" + indexBrand + "&solutionId=" + solutionId,
			context: document.body
		}).done(function(data) {
			if (data.redirect) {
				window.location.replace(data.redirect);
			}
			var refreshReturn = data.indexToRefresh;
			changeFanPlayrFlightSelectionData(data, indexRoute);
			$.ajax({
				url: url + ".partialLaTuaSelezione.html?nocache=" + nocache(),
				context: document.body
			}).done(function(data) {
				$(".my-selection-review").html(data);
				if ($("#ribbon-return").length > 0 && refreshReturn==1) {
					$.ajax({
						url: url + ".partialselection.html?direction=1" + "&nocache=" + nocache(),
						context: document.body
					})
					.done(function(data) {
						$("#flights-return").html(data);
						refreshBindings();
						refreshCouponAfterSelection(url);
						if (!$("#row-info-voli-andata-" + indexFlight).hasClass("collapse in")) {
							$("#flight-info-andata-" + indexFlight).click();
						} else {
							showFlightRowOutbound(null, "#flight-info-andata-" + indexFlight);
						}
						var returnedSelected = $("input[name='ritornoSelection']:checked");
						if ( returnedSelected.length > 0 ) {
							returnedSelected.parents(".flight-row").children(".flight-info").click();
						}
					}).fail(function() {
						console.error("FAIL REFRESH 1 SELECTION");
					}).always(function() {
						toggleFlightSelectionOverlay(false);
					});
				} else {
					refreshBindings();
					refreshCouponAfterSelection(url);
					if (!$("#row-info-voli-andata-" + indexFlight).hasClass("collapse in")) {
						$("#flight-info-andata-" + indexFlight).click();
					} else {
						showFlightRowOutbound(null, "#flight-info-andata-" + indexFlight);
					}
					toggleFlightSelectionOverlay(false);
				}
			}).fail(function() {
				console.error("FAIL REFRESH LA TUA SELEZIONE AJAX");
				toggleFlightSelectionOverlay(false);
			})
		}).fail(function() {
			console.error("FAIL REFRESH SERVLET AJAX");
			toggleFlightSelectionOverlay(false);
		});
	}
}

function performSelctionReturn() {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	var solutionId;
	var id;
	var indexFlight;
	var indexBrand;
	var indexRoute = 1;
	if ($("input[name='ritornoSelection']:checked").size() > 0) {
		$("input[name='ritornoSelection']:checked").unbind('click');
		solutionId = $("input[name='ritornoSelection']:checked").val();
		id = $("input[name='ritornoSelection']:checked").attr("id");
		indexFlight = id.substring(0, id.lastIndexOf("-"));
		indexBrand = id.substring(id.lastIndexOf("-") + 1, id.length);
		toggleFlightSelectionOverlay(true);
		$.ajax({
			url: url + ".bookingrefreshsearch.json?nocache=" + nocache(),
			data: "direction=1&indexFlight=" + indexFlight + "&indexBrand=" + indexBrand + "&solutionId=" + solutionId,
			context: document.body
		}).done(function(data) {
			if (data.redirect) {
				window.location.replace(data.redirect);
			}
			var refreshOutbound = data.indexToRefresh;
			changeFanPlayrFlightSelectionData(data, indexRoute);
			$.ajax({
				url: url + ".partialLaTuaSelezione.html?nocache=" + nocache(),
				context: document.body
			}).done(function(data) {
				$(".my-selection-review").html(data);
				if (refreshOutbound==0) {
					$.ajax({
						url: url + ".partialselection.html?direction=0" + "&nocache=" + nocache(),
						context: document.body
					})
					.done(function(data) {
						$("#flights-outbound").html(data);
						refreshBindings();
						refreshCouponAfterSelection(url);
						if (!$("#row-info-voli-ritorno-" + indexFlight).hasClass("collapse in")) {
							$("#flight-info-ritorno-" + indexFlight).click();
						} else {
							showFlightRowReturn(null, "#flight-info-ritorno-" + indexFlight);
						}
						var outboundSelected = $("input[name='andataSelection']:checked");
						if ( outboundSelected.length > 0 ) {
							outboundSelected.parents(".flight-row").children(".flight-info").click();
						}
					}).fail(function() {
						console.error("FAIL REFRESH 1 SELECTION");
					}).always(function() {
						toggleFlightSelectionOverlay(false);
					});
				} else {
					refreshBindings();
					refreshCouponAfterSelection(url);
					if (!$("#row-info-voli-ritorno-" + indexFlight).hasClass("collapse in")) {
						$("#flight-info-ritorno-" + indexFlight).click();
					} else {
						showFlightRowReturn(null, "#flight-info-ritorno-" + indexFlight);
					}
					toggleFlightSelectionOverlay(false);
				}
			}).fail(function() {
				console.error("FAIL REFRESH LA TUA SELEZIONE AJAX");
				toggleFlightSelectionOverlay(false);
			});
		}).fail(function() {
			console.error("FAIL REFRESHSEARCH AJAX");
			toggleFlightSelectionOverlay(false);
		});
	}
}

function showMoreFligths(){
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	toggleFlightSelectionOverlay(true);
	
	var indexRoute = 0;
	var id = "moreflights-andata";
	var panel = "panelAndata";
	var target = "flights-outbound";
	var lastRowElement = "flight-info-andata-";
	var lastIndexRowDivider = $("#tbodyAndata").children(".divider").length;
	
	if ( $(this).attr("id") == "moreflights-ritorno" ){
		indexRoute = 1;
		id = "moreflights-ritorno";
		panel = "panelRitorno";
		target = "flights-return";
		lastRowElement = "flight-info-ritorno-";
		lastIndexRowDivider = $("#tbodyRitorno").children(".divider").length;
	}
	$.ajax({
		url: url + ".partialselection.html",
		data: "direction=" + indexRoute + "&more=1" + "&nocache=" + nocache(),
		context: document.body
	})
	.done(function(data) {
		$("#" + target).html(data);
		refreshBindings();
		var selection = "andataSelection";
		if ( panel == "panelRitorno" ) {
			selection = "ritornoSelection";
		}
		var selected = $("input[name='" + selection + "']:checked");
		if ( selected.length > 0 ) {
			selected.parents(".flight-row").children(".flight-info").click();
		}
		$("#" + lastRowElement + (lastIndexRowDivider - 1))[0].scrollIntoView();
		toggleFlightSelectionOverlay(false);
	})
	.fail(function() {
		console.error("Fail Show More Flights");
		toggleFlightSelectionOverlay(false);
	});
}

function checkAvailableSolution(data) {
	$("#alertFamily").hide();
	$("#alertYouth").hide();
	$("#alertRibbonMismatch").hide();
	if (data.solutionNotFound == "allSolutions") {
		window.location.replace(data.redirect);
	} else if (data.solutionNotFound == "youth") {
		$("#alertYouth").show();
	} else if (data.solutionNotFound == "family") {
		$("#alertFamily").show();
	} else if (data.solutionNotFound == "territorialContinuity") {
		$("#alertTerritorialContinuity").show();
	} else if (data.solutionNotFound == "ribbonMismatch") {
		$("#alertRibbonMismatch").show();
	}
}

function failureCouponProcessing() {
	console.log("FAILURE COUPON PROCESSING");
	toggleFlightSelectionOverlay(false);
}

function successCouponProcessing(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	}
	var baseUrl = cleanExtensioUrl();
	$.when(
		$.ajax({
			url: baseUrl + ".partialLaTuaSelezione.html?nocache=" + nocache(), 
			success: function(result){
				$(".my-selection-review").html(result);
			}
		}),
		$.ajax({
			url: baseUrl + ".partial-box-ecoupon.html?insertNow=true" + "&nocache=" + nocache(), 
			success: function(result){
				$('#partial-container-ecoupon').html(result);
			}
		})
	).then(function () {
		bindCouponFunctions();
		refreshBindings();
		toggleFlightSelectionOverlay(false);
	});
}

function refreshCouponAfterSelection(url, first) {
	toggleFlightSelectionOverlay(true); 
	$.ajax({
		url: url + ".partial-box-ecoupon.html?nocache=" + nocache(), 
		success: function(result){
			$('#partial-container-ecoupon').html(result);
		}
	}).done(function() {
		bindCouponFunctions();
		if (first) {
			$("#showCouponToggle").click();
		}
	}).fail(function(){
		console.error();
	}).always(function(){
		toggleFlightSelectionOverlay(false); 
	});
}

function successCouponValidation(data) {
	$("#inputEcoupon").removeClass('error-form');
	if (data.result) {
		toggleFlightSelectionOverlay(true);
		performSubmit('bookingcoupon', '#form-ecoupon', successCouponProcessing, failureCouponProcessing);
	} else {
		$("#inputEcoupon").addClass('error-form');
	}
}

function bindCouponFunctions() {
	$("#booking-dati-ecoupon-submit").unbind('click', onCouponSubmit);
	$("#booking-dati-ecoupon-submit").bind('click', onCouponSubmit);
}

function onCouponSubmit(e) {
	validation(e, 'bookingcoupon', '#form-ecoupon', successCouponValidation);
}


function changeFanPlayrFlightSelectionData(data, indexRoute){
	var obj = data["analytics_flightInfo"];
	if(typeof obj !== "undefined"){
		pushAnalyticsData(obj);		
		pushFanplayrEvent();
		/*all flights are selected*/
		if(typeof obj["depFareBasis"] !== "undefined" 
			&& obj["depFareBasis"].length > 0){
			activateDTMRule("flightSerpReturnFlight");
		}
	}
}

function changeFanPlayrRibbonClickData(data){
	var obj = data["analytics_flightInfo"];
	if(typeof obj !== "undefined"){
		pushAnalyticsData(obj);		
		pushFanplayrEvent();
	}
}

