$("#btn_modifica").bind('click', function(e) {
	generalOverlayLoading(true);
	validation(e, 'agencyprofileupdate', '#form-modifica-anagrafica', successModifica);
});


function successModifica(data) {
	removeErrors();
	if (data.result) {
		$('#btn_modifica').unbind('click').click();
	} else {
		for (var key in data.fields) {
			var field = $('[name="' + key + '"]');
			field.parents(".form-group").addClass("has-error");
			field.parents(".form-group").append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>' + data.fields[key] + '</p>');
		 }
	}
	generalOverlayLoading(false);
}

$(document).ready(function(){

	
	$("#selectStato").change(function() {
		
		if ($(this).val() == "SM") {
			
			// CAP
			$("#InputCap").val("47890");
			$("#InputCap").addClass("filled");
			$("#InputCap").attr("readonly", true);
			
			// Citta
			$("#selectCitta option").remove();
			$("#selectCitta").append('<option key="San Marino">San Marino</option>');
			$("#selectCitta").removeAttr("disabled");
			$("#selectCitta").attr("readonly", true);
			
			// Province
			$("#selectProvincia option").remove();
			$("#selectProvincia").append('<option key="SM">San Marino</option>');
			$("#selectProvincia").removeAttr("disabled");
			$("#selectProvincia").attr("readonly", true);
			
		} else {
			
			$("#selectCitta").attr("disabled", true);
			$("select.dynamic").attr("disabled", true);
			replaceLocationSelect("#selectProvincia", "prov", "", "Selezionare una provincia", function() {
				$("#selectCitta option").remove();
				$("#selectCitta").append('<option key="">Selezionare una citt&agrave</option>');
				$("#selectCitta").removeAttr("readonly");
				$("#selectProvincia").removeAttr("readonly");
				$("#selectProvincia").removeAttr("disabled");
			},
			false);
			
			if ($("#InputCap").val() == "47890") {
				$("#InputCap").val("");
				$("#InputCap").removeClass("filled");
			}
			$("#InputCap").removeAttr("readonly");
		}
	});
	$("#selectProvincia").change(function() {
		
		$("#selectCitta").attr("disabled", true);
		var inputCap = $("#InputCap");
		
		// CAP
		if (inputCap.val() == "47890") {
			inputCap.val("");
			inputCap.removeClass("filled");
		}
		inputCap.removeAttr("readonly");
		
		// Citta
		$("select.dynamic").attr("disabled", true);
		replaceLocationSelect("#selectCitta", "city", $(this).val(), "Selezionare una citt&agrave", function(){
			$("select.dynamic").removeAttr("disabled");
			$("#selectCitta").removeAttr("disabled");
		}, false);
	});
});