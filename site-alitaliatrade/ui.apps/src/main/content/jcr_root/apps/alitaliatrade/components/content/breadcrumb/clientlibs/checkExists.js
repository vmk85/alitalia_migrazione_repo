use(function () {

    var hasStats = false;
    var stats = resource.getResourceResolver().getResource(this.url+'/jcr:content/welcome-box');
    if ( stats != null ) {
        hasStats = true;
    } 

    return {
        hasStats: hasStats
    }
});