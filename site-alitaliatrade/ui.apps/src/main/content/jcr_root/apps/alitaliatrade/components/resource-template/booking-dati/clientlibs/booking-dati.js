$(function(){
	$("#booking-dati-submit").bind('click', function(e) {
		addNameAttribute();
		generalOverlayLoading(true);
		validation(e, 'bookingpassengersdata', '#booking-dati-form', successPassengersDataValidation);
	});
	
    $('.agreement-container input[type="checkbox"]').change(function() {
    	$('#checkAgreement').click();
    });
});

$(document).ready(function() {
	checkBookingNavigationProcess("passengersData");

	$("#showdetails").click(function(){
	    if ($('#dettagliviaggio').hasClass('in')) {
            var chkbox = $("#target-scroll-binding");
            $('html,body').animate({scrollTop: chkbox.offset().top},'slow');
        }
	});
});

function successPassengersDataValidation(data) {
	removeErrors();
	$('.agreement-text-container').hide();
	if (data.result) {
		if (data.openModal) {
			$.ajax({
				url: cleanExtensioUrl()+".seatsmap.html", 
				success: function(result){
					generalOverlayLoading(false);
					$('#chooseSeat').html(result);
					$('#chooseSeat').modal();
					bindSeatMapFunctions();
				}
			}).fail(function() {generalOverlayLoading(false);});
		} else {
			$('#booking-dati-form').submit();
		}
	} else {
		generalOverlayLoading(false);
		for ( var key in data.fields) {
			var field = $('[name="' + key + '"]');			
			
			if (field.attr('type') == "radio") {
				field.parents('.radio').addClass('has-error');
			} else if (field.attr('type') == "checkbox") {
				field.parents('.checkbox').addClass('has-error');
				if (field.attr('name') == 'agreementContainer') {
					$('.agreement-text-container').show();
				}
			} else {
				var formgroup = field.parents('.form-group');
				formgroup.addClass('has-error');
				formgroup.append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>' + data.fields[key] + '</p>');
			}			
		}

		$('.booking-main-content')
				.prepend(
						'<div class="alert alert-warning" role="alert"><i class="icon icon-alert-warning"></i> Attenzione, uno o pi&ugrave; campi non sono stati inseriti correttamente</div>');
		if(window["analytics_trackBookingErrors"] 
			&& typeof window["analytics_trackBookingErrors"] === "function"){
			window["analytics_trackBookingErrors"](data.fields);
		}
	}
}

function failureCouponProcessing() {
	console.log("FAILURE COUPON PROCESSING");
	togglePassengersDataOverlay(false);
}

function successCouponProcessing(data) {
	togglePassengersDataOverlay(false);
	if (data.redirect) {
		window.location.replace(data.redirect);
		return;
	}
	var baseUrl = cleanExtensioUrl();
	$.when(
		$.ajax({
			url: baseUrl + ".partial-box-dettagliviaggio.html", 
			success: function(result){
				$('#partial-container-dettagliviaggio').html(result);
			}
		}),
		$.ajax({
			url: baseUrl + ".partial-box-latuaselezione.html", 
			success: function(result){
				$('#partial-container-latuaselezione').html(result);
			}
		}),
		$.ajax({
			url: baseUrl + ".partial-box-ecoupon.html?insertNow=true", 
			success: function(result){
				$('#partial-container-ecoupon').html(result);
			}
		})
	).then(function () {
		bindCouponFunctions();
	});
}

function successCouponValidation(data) {
	$("#inputEcoupon").removeClass('error-form');
	if (data.result) {
		togglePassengersDataOverlay(true);
		performSubmit('bookingcoupon', '#form-ecoupon', successCouponProcessing, failureCouponProcessing);
	} else {
		$("#inputEcoupon").addClass('error-form');
	}
}

function addNameAttribute() {
	$('[id]').each(function(i, field) {
		if (field.getAttribute('name') == null) {
			field.setAttribute('name', field.getAttribute('id'));
		}		
	});
}

function bindCouponFunctions() {
	$("#booking-dati-ecoupon-submit").bind('click', function(e) {
		validation(e, 'bookingcoupon', '#form-ecoupon', successCouponValidation);
	});
}

function bindSeatMapFunctions() {
	$(".btnModifySeat").click(function(e){
        e.preventDefault();
        var btn = $(this);
        var paxIndex = btn.data("seattarget");
        $(".passenger-info").removeClass("active");
        $(".passenger-info[data-paxindex='"+paxIndex+"']").addClass("active");
        $(".fellow_seat").removeClass("active");
        $(".fellow_seat[data-paxindex='"+paxIndex+"']").addClass("active");
    });
    $(".seatmap_choose_seat").on("click", ".available_seat",function(e){
        e.preventDefault();
        var paxIndex = $(".passenger-info.active .btnModifySeat").data("seattarget");
        var clickedPaxSeat = $(this);
        var parentDiv = clickedPaxSeat.parents('div[id^="sceltaPostoTool"]');
        var flightTable = $("#"+parentDiv.attr('id')+" .seatmap_choose_seat");
        var prevSelectedPaxSeat = flightTable.find(".fellow_seat.active");
        prevSelectedPaxSeat.attr('data-paxindex', '');
        prevSelectedPaxSeat.removeClass().addClass("available_seat seatFreeAssignment").text("");
        clickedPaxSeat.attr('data-paxindex', paxIndex);
        clickedPaxSeat.removeClass().addClass('fellow_seat active').text(paxIndex);
        parentDiv.find(".passenger-info.passenger-completed.active .seat-num").text(clickedPaxSeat.attr('data-seatcode'));
        var labelChoosePax = parentDiv.find(".passenger-info.active .btnModifySeat");
        if (!labelChoosePax.hasClass("selected")) {
        	labelChoosePax.find(".modifySeatText").text("Modifica");
        	labelChoosePax.addClass("selected")
    	}
    });
    $(".row_choose a").click(function(){
    	var parentId = $(this).parents('.sceltaposto-container').attr('id');
        $("#"+parentId+" .row_choose li").removeClass("active");
        $(this).parent().addClass("active");
        var tg = $(this).attr("href");
        var airplaneClass = $(this).data("airplane");
        $("#"+parentId+" .seatmap_seat_tab").hide();
        $(tg).show();
        $("#"+parentId+" .seatmap_airplane").removeClass("top middle bottom").addClass(airplaneClass);
        return false;
    });
    $("#choose-seat-submit").bind('click', chooseSeatSubmit);
	$("#choose-seat-default-submit").bind('click', chooseSeatDefaultSubmit);
}

function chooseSeatSubmit(e) {
	$("#choose-seat-submit").unbind('click', chooseSeatSubmit);
	$('.fellow_seat:not(.legend_seat)').each(function(){
		var numPasseggero = $(this).text();
		var volo = $(this).attr('data-flightindex');
		var postoPasseggero = $(this).attr('data-seatcode');
		var postiPasseggeroElement = $('input[id^="postoPassegero_"][id$="_'+numPasseggero+'"]');
		var postiPasseggeroValue = postiPasseggeroElement.val();
		if (postiPasseggeroValue != '') {
			postiPasseggeroValue = postiPasseggeroValue + ";";
		}
		postiPasseggeroValue = postiPasseggeroValue + volo + ":" + postoPasseggero;
		postiPasseggeroElement.val(postiPasseggeroValue);
	});
	$('#booking-dati-form').submit();
}

function chooseSeatDefaultSubmit(e) {
	$("#choose-seat-default-submit").unbind('click', chooseSeatDefaultSubmit);
	$('#booking-dati-form').submit();
}

function togglePassengersDataOverlay(active) {
	if (active) {
		var overlayLoading = document.createElement("div");
		$(overlayLoading).attr("id", "overlayLoading");
		$(overlayLoading).attr("class", "overlayLoading");
		$("#mainContainer").append(overlayLoading);
	} else {
		$("#overlayLoading").remove();
	}
}

bindCouponFunctions();

function openDetails(){
	if (!$('#dettagliviaggio').hasClass('in')) {
		$('#showdetails').click();
	}
}