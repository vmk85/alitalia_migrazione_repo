$('#booking-waiting').show();
$('#booking-waiting .booking-acquista-cdc-form').show();
startWaitingArea();
invokeGenericFormService('bookingpurchasereturn', 'GET', '#', doneCompletePayment, failCompletePayment);

function doneCompletePayment(data) {
	if (data.redirect) {
		window.location.replace(data.redirect);
	} else if (!data.authorizeAlreadyInvoked){
		failCompletePayment();
	}
}

function failCompletePayment() {
	window.location.replace($("#redirectFailurePage").val());
}