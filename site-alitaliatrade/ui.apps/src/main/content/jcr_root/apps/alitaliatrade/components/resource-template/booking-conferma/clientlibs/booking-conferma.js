$(document).ready(function() {
	checkBookingNavigationProcess("done");
});

$(function () {
	$(".icon-reload").click(function(event) {
		event.preventDefault();
		var total = parseFloat(($("#InputTotal").val()).replace(",","."));
		total = isNaN(total)?0.0:total;
		$(".partialPrice").each(function(){
			var price = ($(this).text()).replace(",",".");
			price = price.replace(" ","");
			total += parseFloat(price); 
		});
		var totalPrice = (total.toString()).replace(".",",");
		var index = totalPrice.indexOf(",");
		if ( index != -1){
			var temp = totalPrice.substring(index,totalPrice.length);
			if (temp.length > 3){
				totalPrice = totalPrice.substring(0,index+3);
			}
			if (temp.length == 2) {
				totalPrice = totalPrice + "0";
			}
		} else {
			totalPrice = totalPrice + ",00";
		}
		$(".totalPrice").text(totalPrice);
	});
	
});

function demoFromHTML() {
	//generalOverlayLoading(true);
    var doc = new jsPDF('p', 'in', 'letter');
    var source = $('body');
    var specialElementHandlers = {
        '#bypassme': function(element, renderer) {
            return true;
        }
    };

    doc.fromHTML(
       $('body').get(0), // [Refer Exact code tutorial][2]HTML string or DOM elem ref.
        0.5,    // x coord
        0.5,    // y coord
        {
            'width': 7.5, // max width of content on PDF
            'elementHandlers': specialElementHandlers
        });

	//generalOverlayLoading(false);
    doc.output('dataurl');
}

function demoFromHTML2() {
	//generalOverlayLoading(true);
	html2canvas(document.body, {
		  onrendered: function(canvas) {         
		    var imgData = canvas.toDataURL(
		      'image/jpeg');              
		    var doc = new jsPDF('p', 'mm', 'a4');
		    doc.addImage(imgData, 'JPEG', 1, 1, 210, 297);
		    //generalOverlayLoading(false);
		    doc.output('save');
		  },
		});
	//dimensioni A4 in mm 210x297 ma stretcha
	
}

