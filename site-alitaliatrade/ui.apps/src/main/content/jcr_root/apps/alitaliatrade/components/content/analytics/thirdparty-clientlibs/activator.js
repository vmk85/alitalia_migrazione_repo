function dcsuriRewrite(){
	var regex = /\.\w+$/;
	var pathname = window.location.pathname
	if(!regex.test(pathname)){
		var addition = "index.cshtml";
		if(pathname.substr(pathname.length - 1) !== "/"){
			addition = "/" + addition;
		}
		if($("meta[name='DCS.dcsuri']").length > 0){
			$("meta[name='DCS.dcsuri']").attr("content", pathname + addition);
		}
		else{
			$('head').append('<meta name="DCS.dcsuri" content="' + pathname + addition + '" />');
		}
	}
}

