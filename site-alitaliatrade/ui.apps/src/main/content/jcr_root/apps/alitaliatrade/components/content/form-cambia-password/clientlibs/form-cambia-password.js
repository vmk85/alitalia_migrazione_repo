$(document).ready(function() {

	$("#btn_cambia").bind('click', function(e) {
		validation(e, 'passwordchange', '#form-modifica-password', successPassword);
	});
});

function successPassword(data) {
	removeErrors();
	if (data.result) {
		$('#btn_cambia').unbind('click').click();
		performSubmit('passwordchange', '#form-modifica-password', successChangePassword, failureChangePassword);
		//$('#form-modifica-password').submit();
	} else {
		for ( key in data.fields) {
			$('[name="' + key + '"]').parent('div').addClass('has-error');
			$('[name="' + key + '"]').parent('div').append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>'+data.fields[key]+'</p>');
		}
		//$('.anagrafica-content').prepend('<div class="alert alert-warning" role="alert"><i class="icon icon-alert-warning"></i>Formato password invalido</div>');
	}
}


function successChangePassword(data){
	if (data.error) {
	 	window.location.replace(data.error);
	}
	if (data.success) {
		if (data.changed) {
			$('#successGoHP').attr("href",data.successPage);
			$('#succesModal').modal();
		} else {
			$('[name="passwordOld"]').parent('div').addClass('has-error');
			$('[name="passwordOld"]').parent('div').append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>Vecchia password non riconosciuta</p>');
		}
	} 
}

function failureChangePassword(){
	log.error("Fail change password");
}
