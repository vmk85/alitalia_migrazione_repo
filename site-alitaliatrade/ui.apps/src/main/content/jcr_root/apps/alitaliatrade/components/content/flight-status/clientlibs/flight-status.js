
$("#cerca-infoTime-flightStatus").bind('click', function(e){
	generalOverlayLoading(true);
	validation(e, 'flightinfolist', '#infoTime-flightStatus', successInfoTimeFlightStatus);
});

$("#cerca-info-flightStatus").bind('click', function(e){
	generalOverlayLoading(true);
	validation(e, 'flightinfo', '#info-flightStatus', successInfoFlightStatus, null, null, 'html');
});

function successInfoTimeFlightStatus(data) {
	$("#infoTime-flightStatus .error-form").removeClass('error-form');
	if(data.result){
		$('#infoTime-flightStatus').submit();
	} else {
		for(var key in data.fields){
			var selector = $('[name="' + key + '"]');
			if($(selector).css('display')=='none' ) {
				selector = $(selector).next();
			}
			$(selector).addClass('error-form');
		}
		generalOverlayLoading(false);
	}
}

function successInfoFlightStatus(data) {
	$("#info-flightStatus .error-form").removeClass('error-form');
	if (data.result) {
		$.ajax({
			url : cleanExtensioUrl() + ".flightinfo.json",
			method: "post",
			data : $('#info-flightStatus').serialize(),
			context : document.body
		})
		.done(function(data) {
			if (data.redirect) {
				window.location.replace(data.redirect);
			} else {
				if (data.result) {
					$("#dettaglioVolo .common-text-template").hide();
					$("#dettaglioVolo .table-infovolo").show();
					$("#infoVoloTitolo").html("Dettaglio del volo " + data.flightNumber);
					$("#infoVoloCod").html(data.flightNumber);
					$("#infoVoloPartenza").html(data.departure);
					$("#infoVoloArrivo").html(data.arrival);
					$("#infoVoloOrarioSchPartenza").html(data.departureTime);
					$("#infoVoloOrarioSchArrivo").html(data.arrivalTime);
					$("#infoVoloOrarioEffPartenza").html(data.departureEffectiveTime);
					$("#infoVoloOrarioEffArrivo").html(data.arrivalEffectiveTime);
					$("#infoVoloDettagli").html(data.flightDetails);
					generalOverlayLoading(false);
					$("#dettaglioVolo").modal();
				}
				else if (data.message) {
					$("#dettaglioVolo .table-infovolo").hide();
					$("#dettaglioVolo .common-text-template").show();
					$("#infoVoloTitolo").text("Dettaglio del volo");
					$("#dettaglioVolo .common-text-template p").text(data.message);
					generalOverlayLoading(false);
					$("#dettaglioVolo").modal();
				}
				else {
					for (var key in data.fields) {
						$('[name="' + key + '"]').addClass('error-form');
					}
					generalOverlayLoading(false);
				}
				
			}
		})
		.fail(function() {generalOverlayLoading(false);});
	} else {
		for (var key in data.fields) {
			$('[name="' + key + '"]').addClass('error-form');
		}
		generalOverlayLoading(false);
	}
		
}

$(function() {
//	var hash = window.location.hash;
//	if (hash.indexOf("#profilo") > -1){
//		$('#tabInfoVoli').click();
//	}
	var profilo = getParameterByName('profilo');
	if (profilo){
		$('#tabInfoVoli').click();
	}
	
});
