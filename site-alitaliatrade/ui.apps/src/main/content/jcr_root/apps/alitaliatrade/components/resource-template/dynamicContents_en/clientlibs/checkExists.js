use(function () {

    var hasStats = false;
    var stats = resource.getResourceResolver().getResource(this.url);
    if ( stats != null ) {
        hasStats = true;
    } 

    return {
        hasStats: hasStats
    }
});