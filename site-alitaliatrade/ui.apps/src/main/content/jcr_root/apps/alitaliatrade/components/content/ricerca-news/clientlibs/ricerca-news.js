$("#btn_ricerca_news").bind('click', function(e) {
        validation(e, 'searchnewsservlet', '#form-ricerca-news', successRegistration);
 });
 
 function successRegistration(data) {
 	removeErrors();
     if (data.result) {
         $('#btn_ricerca_news').unbind('click').click();
     } else {
         for (var key in data.fields) {
         	$('[name="' + key + '"]').parent('div').addClass('has-error');
         	$('[name="' + key + '"]').parent('div').parent('div').parent('div').parent('div').append('<p class="help-block alert-block"><i class="icon icon-escl-mark"></i>Per favore, controlla i dati inseriti</p>');
         }
     }
 }