$(window).ready(function() {
    
	/* DESTINAZIONI AUTOCOMPLETE */
	$('input.toToggle').each(function(i, inputDestination) {
		var $inputDestination = $(inputDestination);
		$inputDestination.autocomplete({
	        lookup: function (query, done) {
	        	if (findAirport(query)) { done(airports); };
	        }, 
	        killSuggestions : false,
	        // lookup da sostituire con --> serviceUrl: 'path-to.json',
	        preserveInput: true,
	        appendTo: $inputDestination.parent(),
	        onSearchComplete : function (val, suggestions) {
	            //update with value
	            for (var j=0; j < suggestions.length; j++ ) {
	                var obj= suggestions[j];
	                //obj.value = obj.city + " " + obj.type;
	                obj.value = obj.code;
	            }
	        },
	        formatResult: function (suggestion, currentValue) {
	            return '<div class="sugg_dest_data"><span class="sugg_city_name toPass ' + (suggestion.best ? "best" : "") + '">'
	            + suggestion.city + '</span> - <span class="sugg_city_nation ">'
	            + suggestion.country + '</span><br><span class="sugg_airport">'
	            + suggestion.airport + '</span></div>';
	        },
	        onInvalidateSelection: function () {},
	        onSelect: function (suggestion) {
	            $(this).val(suggestion.value);
	            $(this).parent().find(".dep-code").text(suggestion.code);
	            $(this).parent().find(".dep-name").text(suggestion.city);
	            $inputDestination.focus();
	        }
	    });
	});
	
	/* listener Modifica tratta */
	modificaTratta();
	
	/* Modifica funzione setMultitratta del javascript fornito da bitmama */
	setMultitratta();
	
	/* Modifica funzione attivata sul change degli elementi flighttype fornita da bitmama */
	$("[name=flighttype]").on("change", function(){
    	var icon = $("#InputDataArchivA").siblings(".input-group-addon").find(".icon");
    	
        if ($(this).val() == "flighttype2") {
        	if ($(icon).hasClass("icon-depart-gray")) {
        		$(icon).toggleClass("icon-depart-gray icon-depart-red");
        	}
        
	        $("#InputDataArchivA").removeAttr("disabled");
            
	        $("#andataRitornoTitle").removeClass("hidden");
	        $("#soloAndataTitle").addClass("hidden");
	        
        	$("#superFormBooking").removeClass("hidden");
            $("#formBookingMultitratta").addClass("hidden");
        } else if ($(this).val() == "flighttype3") {
        	
        	$("#formBookingMultitratta").removeClass("hidden");
        	$("#superFormBooking").addClass("hidden");
        	
        } else {
        	if ($(icon).hasClass("icon-depart-red")) {
        		$(icon).toggleClass("icon-depart-gray icon-depart-red");
        	}
        	
        	$("#InputDataArchivA").attr("disabled", "disabled");
        	
        	$("#soloAndataTitle").removeClass("hidden");
        	$("#andataRitornoTitle").addClass("hidden");
        	
        	$("#superFormBooking").removeClass("hidden");
            $("#formBookingMultitratta").addClass("hidden");
        }
        
    });
	
});

function modificaTratta() {
	$mt = $(".multitratta-content");
	$mt.delegate(".modTratta", "click", function() {
    	var nTratta = $(this).attr('nTratta');
    	$mt.find(".mt_"+nTratta+" .mt-edit").removeClass('hidden');
    	$mt.find(".mt_"+nTratta+" .mt-completed").addClass('hidden');
    	$mt.find("#save-mod-button_"+nTratta).removeClass('hidden');
    });
	
	$('[id*="save-mod-button"]').each(function(i, saveModButton) {
		var $saveModButton = $(saveModButton);
		$saveModButton.bind("click", function() {
			var tratta = i+1;
			$mt.find(".mt_"+tratta+" .mt-edit").addClass('hidden');
	    	$mt.find(".mt_"+tratta+" .mt-completed").removeClass('hidden');
	    	$mt.find("#save-mod-button_"+tratta).addClass('hidden');
	    	
	    	$mt.find('#destination_from_resize_'+tratta).empty();
	    	$mt.find('#destination_to_resize_'+tratta).empty();
	    	$mt.find('#destination_date_resize_'+tratta).empty();
	    	$mt.find('#destination_from_resize_'+tratta).append($('div#from_'+tratta).children('.dep-name').text() + ' <b>' + $('div#from_'+tratta).children('.dep-code').text() + '</b>');
	    	$mt.find('#destination_to_resize_'+tratta).append($('div#to_'+tratta).children('.dep-name').text() + ' <b>' + $('div#to_'+tratta).children('.dep-code').text() + '</b>'); 
	    	$mt.find('#destination_date_resize_'+tratta).append($('[id="start['+tratta+']"]').val());
		})
	});
}

var tratta = 1;
var setMultitratta = function() {
	$mt = $(".multitratta-content");
	$mt.delegate(".addTratta", "click", function(){
		$('[id="from[1]"]')
		if($('[id="from['+tratta+']"]').val() != "" && $('[id="to['+tratta+']"]').val() != "" && $('[id="InputDataArchivDa['+tratta+']"]').val() != "") {       

			$mt.find('#destination_from_resize_'+tratta).append($('div#from_'+tratta).children('.dep-name').text() + ' <b>' + $('div#from_'+tratta).children('.dep-code').text() + '</b>');
			$mt.find('#destination_to_resize_'+tratta).append($('div#to_'+tratta).children('.dep-name').text() + ' <b>' + $('div#to_'+tratta).children('.dep-code').text() + '</b>'); 
			$mt.find('#destination_date_resize_'+tratta).append($('[id="start['+tratta+']"]').val());
    		
			$mt.find(".mt_"+tratta+" .mt-completed").removeClass("hidden");
			$mt.find(".mt_"+tratta+" .mt-edit").addClass("hidden");
			$mt.find(".mt_"+(tratta+1)+" .mt-edit").removeClass("hidden");
			$mt.find(".mt_"+(tratta+1)+" .mt-view").addClass("hidden");
			$mt.find(".mt_"+(tratta+2)).removeClass("hidden");
			tratta++;
			$mt.find('#nTratteHidden').val(tratta);
		}
		return false;
	});
}
