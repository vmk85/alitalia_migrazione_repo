use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js",
     "/libs/wcm/foundation/components/utils/ResourceUtils.js",
     "/libs/sightly/js/3rd-party/q.js"], function (AuthoringUtils, ResourceUtils, Q) {
 
 
    var linkPromise = Q.defer();
    var href = this.href;
 
    /* First check if we need to append .html and set target location */
    function getLink() {
        if (typeof (href) === 'string') {
            if (href.substring(0, 1) === '/' && href.substring(0, 2) !== '//') {
                (href.substring(href.length - 4) !== "html" && href.indexOf("#") == -1) ? href = href + '.html' : href = href;
                target = '_self';
            } else {
                href = href;
                target = '_blank';
            }
            var link = {
                href: href,
                target: target
            };
            linkPromise.resolve(link)
        } else {
            linkPromise.reject(new Error('Failure in linkhandler: Link location not specified'))
        }
        return linkPromise.promise;
    }
 
    anchorAttributes = getLink()
        .then(function (link) {
        	//log.error(link.href);
            return link;
        })
        .fail(function (error) {
            //log.error(error);
            return '#';
        });
    return {
        link: anchorAttributes
    };
});