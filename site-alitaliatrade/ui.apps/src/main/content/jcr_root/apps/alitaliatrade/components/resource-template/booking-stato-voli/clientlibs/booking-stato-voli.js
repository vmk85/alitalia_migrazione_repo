$(function() {
	$("#checkbox-voli-diretti").click(function() {
		var numberOfFlight = $(".voli-listing > tbody").children().length - 1;
		var numberOfConnecting = $(".voli-listing > tbody").children(".hasScalo").length;
		if (numberOfFlight == numberOfConnecting) {
			$(".hasScalo").toggle();
			$("#noFlightsFound").toggle();
		} else {
			$(".hasScalo").toggle();
		}
	});
	
	var url = location.protocol + '//' + location.host + location.pathname;
	$('#mailBody').attr('href', 'mailto:your@email.address?subject=dettaglio del volo&body='+url);
	
	$(".flightInfo").bind('click', function(e){
		e.preventDefault();
		$this = $(this);
		var codiceVolo = $(this).attr("rel");
		var flightNum = codiceVolo.substr(2,codiceVolo.length-2);
		var depTime = $('#dataCercata').val();
		var initials = codiceVolo.substr(0,2);
		$.ajax({
			url : cleanExtensioUrl() + ".flightinfo.json",
			method: "post",
			data : { flightnum: flightNum, infodeptime: depTime, initials: initials },
			context : document.body
		})
		.done(function(data) {
			if (data.result) {
				$("#infoVoloTitolo").html("Dettaglio del volo " + data.flightNumber);
				$("#infoVoloCod").html(data.flightNumber);
				$("#infoVoloPartenza").html(data.departure);
				$("#infoVoloArrivo").html(data.arrival);
				$("#infoVoloOrarioSchPartenza").html(data.departureTime);
				$("#infoVoloOrarioSchArrivo").html(data.arrivalTime);
				$("#infoVoloOrarioEffPartenza").html(data.departureEffectiveTime);
				$("#infoVoloOrarioEffArrivo").html(data.arrivalEffectiveTime);
				$("#infoVoloDettagli").html(data.flightDetails);
				$("#dettaglioVolo").modal();
			}
		})
		.fail(function() {});
	});
	
	$('.flightDetail').click(function(e) {
		var arrParam = $(this).attr("rel").split('-');
		var flightNum = arrParam[1];
		var flightVector = arrParam[0];
		var flightDate = arrParam[2];
		var idTarget = $(this).attr("href");
		$.ajax({
			url : cleanExtensioUrl() + ".flightdetail.json",
			method: "post",
			data : { flightnum: flightNum, flightvector: flightVector, flightdate: flightDate },
			context : document.body
		})
		.done(function(data) {
			var detailsContainer = $(idTarget).children(".flight-data-container");
			if (data.redirect) {
				window.location.replace(data.redirect);
			} else {
				detailsContainer.children("#miles").children(".bolder").html(data.miles);
				detailsContainer.children("#aircraft").children(".bolder").html(data.aircraft);
				detailsContainer.children("#duration").children(".bolder").html(data.duration);
			}
		})
		.fail(function() {});
	});
	
});

