$(document).ready(function() {
	
	$("#btn_usa").bind('click', function(e) {
		$("#md_usa").modal();
		$("#md_can").hide();
		$("#md_jap").hide();
	});
	
	$("#btn_can").bind('click', function(e) {
		$("#md_usa").hide();
		$("#md_can").modal();
		$("#md_jap").hide();
	});
	
	$("#btn_jap").bind('click', function(e) {
		$("#md_usa").hide();
		$("#md_can").hide();
		$("#md_jap").modal();
	});
	
	
});