function invokeGenericFormService(service, method, form, done, fail, always, selector, additionalParams) {
	var serializedData = $(form).serialize();
	if (serializedData != "") {
		serializedData += "&";
	}
	serializedData += "_isAjax=true";
	if (additionalParams) {
		for (var paramName in additionalParams) {
			serializedData += "&" + paramName + "=" + encodeURIComponent(additionalParams[paramName]);
		}
	}
	$.ajax({
		url : getServiceUrl(service, selector),
		method : method,
		data : serializedData,
		context : document.body
	}).done(function(data) {
		if (done) {
			done(data);
		}
	}).fail(function() {
		if (fail) {
			fail();
		}
	}).always(function() {
		if (always) {
			always();
		}
	});
}

function validation(e, service, form, done, fail, always, selector) {
	e.preventDefault();
	performValidation(service, form, done, fail, always, selector);
}

function performValidation(service, form, done, fail, always, selector) {
	var additionalParams = { '_action' : 'validate' };
	invokeGenericFormService(service, 'POST', form, done, fail, always, selector, additionalParams);
}

function performSubmit(service, form, done, fail, always, selector) {
	invokeGenericFormService(service, 'POST', form, done, fail, always, selector);
}

function getServiceUrl(service, selector) {
	return cleanExtensioUrl() + "." + service + "."
			+ ((selector) ? selector : "json");
}

function cleanExtensioUrl() {
	var url = location.protocol + '//' + location.host + location.pathname;
	return url.substr(0, url.lastIndexOf('.'));
}

function removeErrors() {
	$('.has-error').removeClass('has-error');
	$('.alert-warning').remove();
	$('.alert-block').remove();
}

function replaceLocationSelect(target, type, filter, empty, success, fail) {
	$.ajax({
		url : cleanExtensioUrl() + ".locationinfo.json",
		data : {
			type : type,
			filter : filter
		},
		context : document.body
	}).done(
			function(data) {
				$(target + " option").remove();
				$(target).append('<option value="">' + empty + '</option>');
				if (data.result) {
					$.each(data.result, function(key, value) {
						$(target).append(
								'<option value="' + key + '">' + value
										+ '</option>');
					});
				}
				if (success) {
					success(data);
				}
			}).fail(function() {
		if (fail) {
			fail(data);
		}
	});
}

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex
			.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g,
			" "));
}

function openPopUp(path) {
	window.open(path, '_news',
			'toolbar=no,scrollbars=yes,resizable=no,height=500,width=455');
}

function generalOverlayLoading(active) {
	if (active) {
		var overlayLoading = document.createElement("div");
		$(overlayLoading).attr("id", "overlayLoading");
		$(overlayLoading).attr("class", "overlayLoading");
		$("#mainContainer").append(overlayLoading);
	} else {
		$("#overlayLoading").remove();
	}
}

function checkBookingNavigationProcess(destPhase,always) {
	var url = location.protocol + '//' + location.host + location.pathname;
	url = url.substr(0, (url.length - 5));
	var residency = getParameterByName("residency");
	var showCallCenter = getParameterByName("showCallCenterMsg");
	var callCenter = "";
	if (destPhase == "done") {
		callCenter = "&showCallCenterMsg=" + showCallCenter;
	}

	$.ajax({
		url: url + ".bookingchecknavigationprocess.json",
		data : "destPhase=" + destPhase + "&nocache=" + nocache() + "&residency=" + residency + callCenter,
		method: "GET",
		context: document.body
	}).done(function(data) {
		if (data.redirect) {
			window.location.replace(data.redirect);
		}
	}).fail(function(){
	}).always(function(){
		if (always) {
			always();
		}
	});
}

function nocache(){
	return new Date().getTime();
}

function initKeepAliveSession(numberOfTimeout,millisecondsTimeout){
	var currentNumberOfTimeout = numberOfTimeout;
	$(document).on("ready", function(){
		if (numberOfTimeout > 0){
			setTimeout(keepAliveSession,millisecondsTimeout);
		}
	});
}

function keepAliveSession(){
	$.ajax({
		url : cleanExtensioUrl() + ".keepalivesession.json",
		context : document.body
	}).done(function(data){
		currentNumberOfTimeout = --numberOfTimeout;
		if (currentNumberOfTimeout > 0) {
			setTimeout(keepAliveSession,millisecondsTimeout);
		}
	});
}

$(document).ready(
		function() {
			$(".logout-sfdc").on("click",function(e){
				e.preventDefault();
				$.ajax(
					{
						url : cleanExtensioUrl() + ".logout-sfdc.json",
					}
				).done(function(data) {
					window.close();
				});
			});
			$(".user-loogged").hover(function() {
				$( ".dropdown-menu-sfdc" ).fadeIn( 500 );
			},function() {
				$( ".dropdown-menu-sfdc" ).fadeOut( 500 );
			});
			$(document).ajaxComplete(function(e,xhr,settings){

				var isJSON = xhr.getResponseHeader("Content-Type").search("json") > 0;
				if (!isJSON) {
					var data = $(xhr.responseText);
					var is200OK = xhr.status === 200;
					var is404Page = data.find("meta[name='IS_NOTFOUND_PAGE']").length > 0;
					var isLoginPage = data.find("meta[name='IS_AUTH_PAGE']").length > 0;

					if (is404Page || isLoginPage) {
						window.location.reload();
					}
					// else if(!is200OK) {
					// 	window.location.replace("/content/alitaliatrade/main/loggato/home-page-selector.html");
					// }
				}
			});
			$(".link-gruppi").bind(
					'click',
					function(e) {
						e.preventDefault();
						var that = $(this);
						$.ajax({
							url : cleanExtensioUrl() + ".encrypttoken.json",
							context : document.body
						}).done(
								function(data) {
									if (data.result) {
										window.location.href = that
												.attr("href")
												+ "?token=" + data.message;
									}
								}).fail(function() {
						});
			});

			if( $(".j-customTable").length > 0) {
				$(".j-customTable").each(function(i,table){
					initCustomTable(table);
				});
			}
		}
);