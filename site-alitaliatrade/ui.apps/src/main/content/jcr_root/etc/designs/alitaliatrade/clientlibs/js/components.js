function log(a) {
    window.console && showLog && console.log(a);
}

function css_browser_selector(a) {
    function b() {
        var a = window.outerWidth || u.clientWidth, b = window.outerHeight || u.clientHeight;
        d.orientation = b > a ? "portrait" : "landscape", u.className = u.className.replace(/ ?orientation_\w+/g, "").replace(/ [min|max|cl]+[w|h]_\d+/g, "");
        for (var c = f - 1; c >= 0; c--) if (a >= e[c]) {
            d.maxw = e[c];
            break;
        }
        widthClasses = "";
        for (var g in d) widthClasses += " " + g + "_" + d[g];
        return u.className = u.className + widthClasses, widthClasses;
    }
    function c() {
        var a = window.devicePixelRatio > 1;
        a ? u.className += " retina" : u.className += " non-retina";
    }
    var d = {}, e = [ 320, 480, 640, 768, 1024, 1152, 1280, 1440, 1680, 1920, 2560 ], f = e.length, g = a.toLowerCase(), h = function(a) {
        return RegExp(a, "i").test(g);
    }, i = function(a, b) {
        b = b.replace(".", "_");
        for (var c = b.indexOf("_"), d = ""; c > 0; ) d += " " + a + b.substring(0, c), 
        c = b.indexOf("_", c + 1);
        return d += " " + a + b;
    }, j = "gecko", k = "webkit", l = "chrome", m = "firefox", n = "safari", o = "opera", p = "mobile", q = "android", r = "blackberry", s = "lang_", t = "device_", u = document.documentElement, v = [ !/opera|webtv/i.test(g) && /msie\s(\d+)/.test(g) || /trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.test(g) ? "ie ie" + (/trident\/4\.0/.test(g) ? "8" : "11.0" == RegExp.$1 ? "11" : RegExp.$1) : h("firefox/") ? j + " " + m + (/firefox\/((\d+)(\.(\d+))(\.\d+)*)/.test(g) ? " " + m + RegExp.$2 + " " + m + RegExp.$2 + "_" + RegExp.$4 : "") : h("gecko/") ? j : h("opera") ? o + (/version\/((\d+)(\.(\d+))(\.\d+)*)/.test(g) ? " " + o + RegExp.$2 + " " + o + RegExp.$2 + "_" + RegExp.$4 : /opera(\s|\/)(\d+)\.(\d+)/.test(g) ? " " + o + RegExp.$2 + " " + o + RegExp.$2 + "_" + RegExp.$3 : "") : h("konqueror") ? "konqueror" : h("blackberry") ? r + (/Version\/(\d+)(\.(\d+)+)/i.test(g) ? " " + r + RegExp.$1 + " " + r + RegExp.$1 + RegExp.$2.replace(".", "_") : /Blackberry ?(([0-9]+)([a-z]?))[\/|;]/gi.test(g) ? " " + r + RegExp.$2 + (RegExp.$3 ? " " + r + RegExp.$2 + RegExp.$3 : "") : "") : h("android") ? q + (/Version\/(\d+)(\.(\d+))+/i.test(g) ? " " + q + RegExp.$1 + " " + q + RegExp.$1 + RegExp.$2.replace(".", "_") : "") + (/Android (.+); (.+) Build/i.test(g) ? " " + t + RegExp.$2.replace(/ /g, "_").replace(/-/g, "_") : "") : h("chrome") ? k + " " + l + (/chrome\/((\d+)(\.(\d+))(\.\d+)*)/.test(g) ? " " + l + RegExp.$2 + (RegExp.$4 > 0 ? " " + l + RegExp.$2 + "_" + RegExp.$4 : "") : "") : h("iron") ? k + " iron" : h("applewebkit/") ? k + " " + n + (/version\/((\d+)(\.(\d+))(\.\d+)*)/.test(g) ? " " + n + RegExp.$2 + " " + n + RegExp.$2 + RegExp.$3.replace(".", "_") : / Safari\/(\d+)/i.test(g) ? "419" == RegExp.$1 || "417" == RegExp.$1 || "416" == RegExp.$1 || "412" == RegExp.$1 ? " " + n + "2_0" : "312" == RegExp.$1 ? " " + n + "1_3" : "125" == RegExp.$1 ? " " + n + "1_2" : "85" == RegExp.$1 ? " " + n + "1_0" : "" : "") : h("mozilla/") ? j : "", h("android|mobi|mobile|j2me|iphone|ipod|ipad|blackberry|playbook|kindle|silk") ? p : "", h("j2me") ? "j2me" : h("ipad|ipod|iphone") ? (/CPU( iPhone)? OS (\d+[_|\.]\d+([_|\.]\d+)*)/i.test(g) ? "ios" + i("ios", RegExp.$2) : "") + " " + (/(ip(ad|od|hone))/gi.test(g) ? RegExp.$1 : "") : h("playbook") ? "playbook" : h("kindle|silk") ? "kindle" : h("playbook") ? "playbook" : h("mac") ? "mac" + (/mac os x ((\d+)[.|_](\d+))/.test(g) ? " mac" + RegExp.$2 + " mac" + RegExp.$1.replace(".", "_") : "") : h("win") ? "win" + (h("windows nt 6.2") ? " win8" : h("windows nt 6.1") ? " win7" : h("windows nt 6.0") ? " vista" : h("windows nt 5.2") || h("windows nt 5.1") ? " win_xp" : h("windows nt 5.0") ? " win_2k" : h("windows nt 4.0") || h("WinNT4.0") ? " win_nt" : "") : h("freebsd") ? "freebsd" : h("x11|linux") ? "linux" : "", /[; |\[](([a-z]{2})(\-[a-z]{2})?)[)|;|\]]/i.test(g) ? (s + RegExp.$2).replace("-", "_") + ("" != RegExp.$3 ? (" " + s + RegExp.$1).replace("-", "_") : "") : "", h("ipad|iphone|ipod") && !h("safari") ? "ipad_app" : "" ];
    window.onresize = b, b(), c();
    var w = v.join(" ") + " js ";
    return u.className = (w + u.className.replace(/\b(no[-|_]?)?js\b/g, "")).replace(/^ /, "").replace(/ +/g, " "), 
    w;
}

function set11Month() {
    var a = 11, b = new Date();
    b = b.setMonth(b.getMonth() + a);
    var c = new Date(b), d = c.getFullYear(), e = 10 > c.getMonth() + 1 ? "0" + (c.getMonth() + 1) : c.getMonth() + 1, f = c.getDate();
    return d + "" + e + f;
}

function next10Days() {
    var a = (newDate(), new Date()), b = new Date(a.getFullYear(), a.getMonth(), a.getDate() + 10);
    return b;
}

function highlightPeriod() {
    var a = datePickerController.getSelectedDate("andata"), b = $("#soloandata").is(":checked") ? null : datePickerController.getSelectedDate("ritorno");
    null != a && null != b && (a > b && ($("#ritorno").val(""), b = null), datePickerController.setPeriodHighlight("ritorno", a, b)), 
    datePickerController.setPeriodHighlight("andata", a, b), datePickerController.setPeriodHighlight("ritorno", a, b);
}

function openAndSetReturn() {
    highlightPeriod(), null !== datePickerController.getSelectedDate("andata") && ($("#soloandata").is(":checked") ? showToolset() : (datePickerController.setRangeLow("ritorno", datePickerController.getSelectedDate("andata")), 
    datePickerController.show("ritorno")));
}

function openToolStage() {
    highlightPeriod(), null !== datePickerController.getSelectedDate("ritorno") && null !== datePickerController.getSelectedDate("andata") ? showToolset() : null !== datePickerController.getSelectedDate("ritorno") && null === datePickerController.getSelectedDate("andata") && datePickerController.show("andata");
}

function showToolset() {
    $(".flightFinder, .innerFlightFinder").removeClass("ff-stage4").addClass("ff-stage5");
}

function openAndSetDepart() {}

function openHoverMenu(a) {
    console.log("opeeen");
    var b = $(".mainMenu__firstLevelItem").eq(a);
    b.hasClass("noSecondLevel") || b.addClass("isActiveFirst").find(".mainMenu__secondLevelCover").velocity("transition.slideDownIn", {
        duration: 250
    });
}

function hideHoverMenu(a) {
    $(".mainMenu__firstLevelItem").removeClass("isActiveFirst"), $(".mainMenu__firstLevelItem").find(".mainMenu__secondLevelCover").velocity("transition.slideDownOut", {
        duration: 250
    });
}

function changeLinks(a) {
    var b = $(".mainMenu__firstLevelLink");
    firstLevelLink = b.attr("data-href"), secondLevel = $(".mainMenu__secondLevelLink"), 
    secondLevelLink = secondLevel.attr("data-href"), stopLink = "javascript:;", a ? (b.each(function() {
        var a = $(this);
        $(this).hasClass("noSecondLevel") || a.attr({
            href: stopLink
        });
    }), secondLevel.each(function() {
        var a = $(this);
        $(this).hasClass("noThirdLevel") || a.attr({
            href: stopLink
        });
    })) : (b.each(function() {
        var a = $(this);
        a.attr({
            href: firstLevelLink
        });
    }), secondLevel.each(function() {
        var a = $(this);
        a.attr({
            href: secondLevelLink
        });
    }));
}

function dropDownInit() {
    var a = $(window).width() > 1023 ? !0 : !1;
    if (window.ismobile || window.supportsTouch || !a) changeLinks(!0), $(".mainMenu__firstLevelItem").unbind("mouseenter mouseleave"), 
    $(".mainMenu__firstLevelItem").unfastClick(), $(".mainMenu__secondLevelItem").unfastClick(), 
    $(".mainMenu__firstLevelItem").fastClick(function(b) {
        b.preventDefault(), console.log("enis"), $(this).hasClass("isActiveFirst") ? ($(".mainMenu__firstLevelItem").removeClass("isActiveFirst"), 
        $(".mainMenu__secondLevelCover").velocity("transition.slideDownOut", {
            duration: window.menuSpeed
        }), $(".mainMenu__secondLevelItem").removeClass("isActiveSecond")) : a ? ($(".mainMenu__firstLevelItem").removeClass("isActiveFirst"), 
        $(".mainMenu__secondLevelCover:visible").velocity("transition.slideDownOut", {
            duration: window.menuSpeed
        }), $(".mainMenu__secondLevelItem").removeClass("isActiveSecond"), $(this).addClass("isActiveFirst").find(".mainMenu__secondLevelCover").velocity("transition.slideDownIn", {
            duration: window.menuSpeed
        })) : (console.log($(this).find(".mainMenu__firstLevelLink").text()), $(this).find(".mainMenu__firstLevelLink").next(".mainMenu__secondLevelCover").show().addClass("isActiveSecond"));
    }), $(".mainMenu__secondLevelItem").fastClick(function(a) {
        a.preventDefault();
        var b = $(this).find(".mainMenu__secondLevelLink");
        b.hasClass("noThirdLevel") ? window.location.href = b.attr("href") : b.next(".mainMenu__thirdLevelCover").addClass("isActiveThird");
    }); else {
        var b;
        $(".mainMenu__firstLevelItem").unfastClick(), $(".mainMenu__secondLevelItem").unfastClick(), 
        $(".mainMenu__firstLevelItem").unbind("mouseenter mouseleave"), $(".mainMenu__firstLevelItem").mouseenter(function() {
            if (!$(this).find(".mainMenu__firstLevelLink").hasClass("noSecondLevel")) {
                var a = $(this).index();
                clearTimeout(b), b = setTimeout(function() {
                    openHoverMenu(a);
                }, window.menuSpeedTimer);
            }
        }).mouseleave(function() {
            hideHoverMenu($(this)), clearTimeout(b);
        });
    }
}

function _classCallCheck(a, b) {
    if (!(a instanceof b)) throw new TypeError("Cannot call a class as a function");
}

function fixPositions() {
    if ($('.j-groupContainer[data-group="notificationGroup"]').length > 0) {
        var a = $('.j-groupContainer[data-group="notificationGroup"]'), b = $('.j-groupContainer[data-group="recentGroup"]'), c = $('.j-userMenuLink[data-group="notificationGroup"]'), d = $('.j-userMenuLink[data-group="recentGroup"]'), e = (parseInt($(".header").find(".mod").css("margin-left").replace("px", "")), 
        c.offset().left), f = c.outerWidth(), g = (parseInt(c.css("margin-left").replace("px", "")), 
        a.outerWidth()), h = d.offset().left, i = d.outerWidth(), j = (parseInt(d.css("margin-left").replace("px", "")), 
        b.outerWidth());
        a.css({
            left: e - (g - f)
        }), b.css({
            left: h - (j - i)
        });
    }
}

function _classCallCheck(a, b) {
    if (!(a instanceof b)) throw new TypeError("Cannot call a class as a function");
}

function _classCallCheck(a, b) {
    if (!(a instanceof b)) throw new TypeError("Cannot call a class as a function");
}

function _classCallCheck(a, b) {
    if (!(a instanceof b)) throw new TypeError("Cannot call a class as a function");
}

function _classCallCheck(a, b) {
    if (!(a instanceof b)) throw new TypeError("Cannot call a class as a function");
}

function _classCallCheck(a, b) {
    if (!(a instanceof b)) throw new TypeError("Cannot call a class as a function");
}

function SetCookie(a, b, c) {
    if (c) {
        var d = new Date();
        d.setTime(d.getTime() + 24 * c * 60 * 60 * 1e3);
        var e = ";expires=" + d.toGMTString();
    } else var e = "";
    document.cookie = a + "=" + String(b + "") + e + ";path=/";
}

function GetCookie(a) {
    for (var b = a + "=", c = document.cookie.split(";"), d = 0; d < c.length; d++) {
        for (var e = c[d]; " " == e.charAt(0); ) e = e.substring(1, e.length);
        if (0 == e.indexOf(b)) return "true" === e.substring(b.length, e.length);
    }
    return null;
}

function stickyHeaderNavigation(a) {
    if (!$("[data-fixedheader]").data("fixedheader")) {
        var b = 0, c = 40, d = $(window);
        cookieBar_height = 0;
        var e = $(".j-cookieBar").is(":visible");
        e && (cookieBar_height = $(".j-cookieBar").outerHeight()), $(".mainWrapper").velocity({
            "margin-top": cookieBar_height
        }, {
            duration: a ? 250 : 0
        }), $("body").hasClass("hasAlert") && (c = $(".menu.mainMenu").offset().top), d.unbind("scroll.cookie").bind("scroll.cookie", function(a, e) {
            b = d.scrollTop();
            $("body").hasClass("userMenuIsActive");
            0 > b && (b = 0);
            var f = c > b ? "static" : "header";
            f != header_status && ("static" == f ? ($(".menu.mainMenu").removeClass("posFixed"), 
            e ? $(".menu.mainMenu").velocity({
                top: 41
            }, {
                duration: 250
            }) : $(".menu.mainMenu").css({
                top: 41
            })) : ($(".menu.mainMenu").addClass("posFixed"), e ? $(".menu.mainMenu").velocity({
                top: cookieBar_height
            }, {
                duration: 250
            }) : $(".menu.mainMenu").css({
                top: cookieBar_height
            })), header_status = f);
        }), d.trigger("scroll", a);
    }
}

function cookieController() {
    $(".j-cookieBar").hide(), GetCookie("alitaliaCookieConfirmed") || (COOKIE_CONFIRMED = !1, 
    $(".j-cookieBar").show(), stickyHeaderNavigation(), $(".j-closeCookie").bind("click", function(a) {
        a.preventDefault(), setCookieHandler();
    }), $(window).bind("resize.cookie", function() {
        $(".j-cookieBar").outerHeight() != cookieBar_height && (header_status = void 0, 
        stickyHeaderNavigation());
    }));
}

function setCookieHandler() {
    SetCookie("alitaliaCookieConfirmed", !0, 365), COOKIE_CONFIRMED = !0, $(window).unbind("resize.cookie"), 
    $(".j-cookieBar").fadeOut(200, function() {
        header_status = void 0, $(this).remove(), stickyHeaderNavigation(!0);
    });
}

function numberSelector() {
    $(".j-numberSelector-plus").on("click", function() {
        var a, b = $(this), c = b.closest(".j-numberSelector"), d = c.find(".j-numberSelector-input"), e = parseInt(d.val());
        a = e + 1, a > 10 && (a = 10), d.val(a);
    }), $(".j-numberSelector-minus").on("click", function() {
        var a, b = $(this), c = b.closest(".j-numberSelector"), d = c.find(".j-numberSelector-input"), e = parseInt(d.val());
        a = e - 1, 0 > a && (a = 0), d.val(a);
    });
}

function listSorter(a, b) {
    var c = a, d = c.children("li").get();
    d.sort(function(a, c) {
        if ("name" === b) var d = $(a).data(b).toUpperCase(), e = $(c).data(b).toUpperCase(); else var d = parseInt($(a).data(b)), e = parseInt($(c).data(b));
        return e > d ? -1 : d > e ? 1 : 0;
    }), $.each(d, function(a, b) {
        c.append(b);
    });
}

function messageSorter(a) {
    var b = $(".millemiglia__acordions--messages"), c = b.children(".millemiglia__accordion").get();
    c.sort(function(b, c) {
        if ("name" === a) var d = $(b).data("title").toUpperCase(), e = $(c).data("title").toUpperCase(); else if ("number" === a) var d = parseInt($(b).data("date")), e = parseInt($(c).data("date"));
        return e > d ? -1 : d > e ? 1 : 0;
    }), $.each(c, function(a, c) {
        b.append(c);
    });
}

function destinationCarousel() {
    if (0 !== $(".destinationCarousel__listContainer").length) if ($(".destinationCarousel__listContainer").iosSlider("destroy"), 
    $(".destinationCarousel__indicatorsItem").remove(), 641 > $(window).width()) {
        0 === $(".destinationItem__imageCover").length && $(".destinationItem__image").wrap(function() {
            return '<div class="destinationItem__imageCover"></div>';
        });
        var a = $(".destinationItem__imageCover").outerHeight(), b = $(".destinationItem__imageCover").siblings(".verticalAlignBottom").height(), c = $(".destinationCarousel__indicatorsItem").length;
        if ($(".destinationCarousel__list, .destinationCarousel__listContainer").height(a + b), 
        0 === c) for (var d = 0; d < $(".destinationItem").length; d++) $(".destinationCarousel__indicators").append('<li class="destinationCarousel__indicatorsItem"><span class="i-dot"></span></li>');
        $(".destinationCarousel__listContainer").iosSlider("destroy"), "" !== $(".destinationCarousel__listContainer").attr("style") && $(".destinationCarousel__listContainer").iosSlider({
            desktopClickDrag: !0,
            snapToChildren: !0,
            autoSlide: !0,
            autoSlideTimer: 8e3,
            infiniteSlider: !0,
            navSlideSelector: ".destinationCarousel__indicatorsItem",
            onSliderLoaded: destinationCarouselLoaded,
            onSlideChange: destinationCarouselSlideChange,
            onSlideComplete: destinationCarouselSlideComplete
        });
    } else $(".destinationCarousel__listContainer").iosSlider("destroy"), $(".destinationCarousel__indicatorsItem").removeClass("active");
}

function destinationCarouselLoaded(a) {
    $(".destinationCarousel__indicatorsItem").eq(0).addClass("active");
}

function destinationCarouselSlideChange(a) {
    var b = $(".destinationCarousel__indicatorsItem");
    b.removeClass("active"), b.eq(a.currentSlideNumber - 1).addClass("active");
}

function destinationCarouselSlideComplete(a) {}

function dateMatrixOnResize() {
    !$(".dateMatrix__caption").length > 0 || ($(window).width() > 641 ? $(".dateMatrix__caption").attr({
        colspan: 2
    }) : $(".dateMatrix__caption").attr({
        colspan: 1
    }));
}

mobileCheck = function() {
    var a;
    return a = !1, function(b) {
        return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(b) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(b.substr(0, 4)) ? a = !0 : void 0;
    }(navigator.userAgent || navigator.vendor || window.opera), a;
}, window.supportsTouch = Modernizr.touch || "ontouchstart" in window || navigator.MaxTouchPoints > 0 || navigator.msMaxTouchPoints > 0 || -1 !== navigator.userAgent.indexOf("IEMobile") || !1, 
window.ismobile = mobileCheck(), window.breakpointS = 641, window.breakpointM = 801, 
window.breakpointL = 1024, window.menuSpeed = 500, window.menuSpeedTimer = 600, 
window.windowW = $(window).width(), window.bookingHeaderStop = !1, $(function() {
    window.supportsTouch && $("body").addClass("isTouch");
}), $(window).resize(function(a) {
    window.windowW = $(window).width();
}), !function(a, b, c) {
    !function(a) {
        "use strict";
        "function" == typeof define && define.amd ? define([ "jquery" ], a) : jQuery && !jQuery.fn.qtip && a(jQuery);
    }(function(d) {
        "use strict";
        function e(a, b, c, e) {
            this.id = c, this.target = a, this.tooltip = F, this.elements = {
                target: a
            }, this._id = S + "-" + c, this.timers = {
                img: {}
            }, this.options = b, this.plugins = {}, this.cache = {
                event: {},
                target: d(),
                disabled: E,
                attr: e,
                onTooltip: E,
                lastClass: ""
            }, this.rendered = this.destroyed = this.disabled = this.waiting = this.hiddenDuringWait = this.positioning = this.triggering = E;
        }
        function f(a) {
            return a === F || "object" !== d.type(a);
        }
        function g(a) {
            return !(d.isFunction(a) || a && a.attr || a.length || "object" === d.type(a) && (a.jquery || a.then));
        }
        function h(a) {
            var b, c, e, h;
            return f(a) ? E : (f(a.metadata) && (a.metadata = {
                type: a.metadata
            }), "content" in a && (b = a.content, f(b) || b.jquery || b.done ? b = a.content = {
                text: c = g(b) ? E : b
            } : c = b.text, "ajax" in b && (e = b.ajax, h = e && e.once !== E, delete b.ajax, 
            b.text = function(a, b) {
                var f = c || d(this).attr(b.options.content.attr) || "Loading...", g = d.ajax(d.extend({}, e, {
                    context: b
                })).then(e.success, F, e.error).then(function(a) {
                    return a && h && b.set("content.text", a), a;
                }, function(a, c, d) {
                    b.destroyed || 0 === a.status || b.set("content.text", c + ": " + d);
                });
                return h ? f : (b.set("content.text", f), g);
            }), "title" in b && (d.isPlainObject(b.title) && (b.button = b.title.button, b.title = b.title.text), 
            g(b.title || E) && (b.title = E))), "position" in a && f(a.position) && (a.position = {
                my: a.position,
                at: a.position
            }), "show" in a && f(a.show) && (a.show = a.show.jquery ? {
                target: a.show
            } : a.show === D ? {
                ready: D
            } : {
                event: a.show
            }), "hide" in a && f(a.hide) && (a.hide = a.hide.jquery ? {
                target: a.hide
            } : {
                event: a.hide
            }), "style" in a && f(a.style) && (a.style = {
                classes: a.style
            }), d.each(R, function() {
                this.sanitize && this.sanitize(a);
            }), a);
        }
        function i(a, b) {
            for (var c, d = 0, e = a, f = b.split("."); e = e[f[d++]]; ) d < f.length && (c = e);
            return [ c || a, f.pop() ];
        }
        function j(a, b) {
            var c, d, e;
            for (c in this.checks) for (d in this.checks[c]) (e = new RegExp(d, "i").exec(a)) && (b.push(e), 
            ("builtin" === c || this.plugins[c]) && this.checks[c][d].apply(this.plugins[c] || this, b));
        }
        function k(a) {
            return V.concat("").join(a ? "-" + a + " " : " ");
        }
        function l(a, b) {
            return b > 0 ? setTimeout(d.proxy(a, this), b) : void a.call(this);
        }
        function m(a) {
            this.tooltip.hasClass(aa) || (clearTimeout(this.timers.show), clearTimeout(this.timers.hide), 
            this.timers.show = l.call(this, function() {
                this.toggle(D, a);
            }, this.options.show.delay));
        }
        function n(a) {
            if (!this.tooltip.hasClass(aa) && !this.destroyed) {
                var b = d(a.relatedTarget), c = b.closest(W)[0] === this.tooltip[0], e = b[0] === this.options.show.target[0];
                if (clearTimeout(this.timers.show), clearTimeout(this.timers.hide), this !== b[0] && "mouse" === this.options.position.target && c || this.options.hide.fixed && /mouse(out|leave|move)/.test(a.type) && (c || e)) try {
                    a.preventDefault(), a.stopImmediatePropagation();
                } catch (f) {} else this.timers.hide = l.call(this, function() {
                    this.toggle(E, a);
                }, this.options.hide.delay, this);
            }
        }
        function o(a) {
            !this.tooltip.hasClass(aa) && this.options.hide.inactive && (clearTimeout(this.timers.inactive), 
            this.timers.inactive = l.call(this, function() {
                this.hide(a);
            }, this.options.hide.inactive));
        }
        function p(a) {
            this.rendered && this.tooltip[0].offsetWidth > 0 && this.reposition(a);
        }
        function q(a, c, e) {
            d(b.body).delegate(a, (c.split ? c : c.join("." + S + " ")) + "." + S, function() {
                var a = y.api[d.attr(this, U)];
                a && !a.disabled && e.apply(a, arguments);
            });
        }
        function r(a, c, f) {
            var g, i, j, k, l, m = d(b.body), n = a[0] === b ? m : a, o = a.metadata ? a.metadata(f.metadata) : F, p = "html5" === f.metadata.type && o ? o[f.metadata.name] : F, q = a.data(f.metadata.name || "qtipopts");
            try {
                q = "string" == typeof q ? d.parseJSON(q) : q;
            } catch (r) {}
            if (k = d.extend(D, {}, y.defaults, f, "object" == typeof q ? h(q) : F, h(p || o)), 
            i = k.position, k.id = c, "boolean" == typeof k.content.text) {
                if (j = a.attr(k.content.attr), k.content.attr === E || !j) return E;
                k.content.text = j;
            }
            if (i.container.length || (i.container = m), i.target === E && (i.target = n), k.show.target === E && (k.show.target = n), 
            k.show.solo === D && (k.show.solo = i.container.closest("body")), k.hide.target === E && (k.hide.target = n), 
            k.position.viewport === D && (k.position.viewport = i.container), i.container = i.container.eq(0), 
            i.at = new A(i.at, D), i.my = new A(i.my), a.data(S)) if (k.overwrite) a.qtip("destroy", !0); else if (k.overwrite === E) return E;
            return a.attr(T, c), k.suppress && (l = a.attr("title")) && a.removeAttr("title").attr(ca, l).attr("title", ""), 
            g = new e(a, k, c, !!j), a.data(S, g), g;
        }
        function s(a) {
            return a.charAt(0).toUpperCase() + a.slice(1);
        }
        function t(a, b) {
            var d, e, f = b.charAt(0).toUpperCase() + b.slice(1), g = (b + " " + ra.join(f + " ") + f).split(" "), h = 0;
            if (qa[b]) return a.css(qa[b]);
            for (;d = g[h++]; ) if ((e = a.css(d)) !== c) return qa[b] = d, e;
        }
        function u(a, b) {
            return Math.ceil(parseFloat(t(a, b)));
        }
        function v(a, b) {
            this._ns = "tip", this.options = b, this.offset = b.offset, this.size = [ b.width, b.height ], 
            this.init(this.qtip = a);
        }
        function w(a, b) {
            this.options = b, this._ns = "-modal", this.init(this.qtip = a);
        }
        function x(a) {
            this._ns = "ie6", this.init(this.qtip = a);
        }
        var y, z, A, B, C, D = !0, E = !1, F = null, G = "x", H = "y", I = "width", J = "height", K = "top", L = "left", M = "bottom", N = "right", O = "center", P = "flipinvert", Q = "shift", R = {}, S = "qtip", T = "data-hasqtip", U = "data-qtip-id", V = [ "ui-widget", "ui-tooltip" ], W = "." + S, X = "click dblclick mousedown mouseup mousemove mouseleave mouseenter".split(" "), Y = S + "-fixed", Z = S + "-default", $ = S + "-focus", _ = S + "-hover", aa = S + "-disabled", ba = "_replacedByqTip", ca = "oldtitle", da = {
            ie: function() {
                for (var a = 4, c = b.createElement("div"); (c.innerHTML = "<!--[if gt IE " + a + "]><i></i><![endif]-->") && c.getElementsByTagName("i")[0]; a += 1) ;
                return a > 4 ? a : NaN;
            }(),
            iOS: parseFloat(("" + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [ 0, "" ])[1]).replace("undefined", "3_2").replace("_", ".").replace("_", "")) || E
        };
        z = e.prototype, z._when = function(a) {
            return d.when.apply(d, a);
        }, z.render = function(a) {
            if (this.rendered || this.destroyed) return this;
            var b, c = this, e = this.options, f = this.cache, g = this.elements, h = e.content.text, i = e.content.title, j = e.content.button, k = e.position, l = ("." + this._id + " ", 
            []);
            return d.attr(this.target[0], "aria-describedby", this._id), f.posClass = this._createPosClass((this.position = {
                my: k.my,
                at: k.at
            }).my), this.tooltip = g.tooltip = b = d("<div/>", {
                id: this._id,
                "class": [ S, Z, e.style.classes, f.posClass ].join(" "),
                width: e.style.width || "",
                height: e.style.height || "",
                tracking: "mouse" === k.target && k.adjust.mouse,
                role: "alert",
                "aria-live": "polite",
                "aria-atomic": E,
                "aria-describedby": this._id + "-content",
                "aria-hidden": D
            }).toggleClass(aa, this.disabled).attr(U, this.id).data(S, this).appendTo(k.container).append(g.content = d("<div />", {
                "class": S + "-content",
                id: this._id + "-content",
                "aria-atomic": D
            })), this.rendered = -1, this.positioning = D, i && (this._createTitle(), d.isFunction(i) || l.push(this._updateTitle(i, E))), 
            j && this._createButton(), d.isFunction(h) || l.push(this._updateContent(h, E)), 
            this.rendered = D, this._setWidget(), d.each(R, function(a) {
                var b;
                "render" === this.initialize && (b = this(c)) && (c.plugins[a] = b);
            }), this._unassignEvents(), this._assignEvents(), this._when(l).then(function() {
                c._trigger("render"), c.positioning = E, c.hiddenDuringWait || !e.show.ready && !a || c.toggle(D, f.event, E), 
                c.hiddenDuringWait = E;
            }), y.api[this.id] = this, this;
        }, z.destroy = function(a) {
            function b() {
                if (!this.destroyed) {
                    this.destroyed = D;
                    var a, b = this.target, c = b.attr(ca);
                    this.rendered && this.tooltip.stop(1, 0).find("*").remove().end().remove(), d.each(this.plugins, function() {
                        this.destroy && this.destroy();
                    });
                    for (a in this.timers) clearTimeout(this.timers[a]);
                    b.removeData(S).removeAttr(U).removeAttr(T).removeAttr("aria-describedby"), this.options.suppress && c && b.attr("title", c).removeAttr(ca), 
                    this._unassignEvents(), this.options = this.elements = this.cache = this.timers = this.plugins = this.mouse = F, 
                    delete y.api[this.id];
                }
            }
            return this.destroyed ? this.target : (a === D && "hide" !== this.triggering || !this.rendered ? b.call(this) : (this.tooltip.one("tooltiphidden", d.proxy(b, this)), 
            !this.triggering && this.hide()), this.target);
        }, B = z.checks = {
            builtin: {
                "^id$": function(a, b, c, e) {
                    var f = c === D ? y.nextid : c, g = S + "-" + f;
                    f !== E && f.length > 0 && !d("#" + g).length ? (this._id = g, this.rendered && (this.tooltip[0].id = this._id, 
                    this.elements.content[0].id = this._id + "-content", this.elements.title[0].id = this._id + "-title")) : a[b] = e;
                },
                "^prerender": function(a, b, c) {
                    c && !this.rendered && this.render(this.options.show.ready);
                },
                "^content.text$": function(a, b, c) {
                    this._updateContent(c);
                },
                "^content.attr$": function(a, b, c, d) {
                    this.options.content.text === this.target.attr(d) && this._updateContent(this.target.attr(c));
                },
                "^content.title$": function(a, b, c) {
                    return c ? (c && !this.elements.title && this._createTitle(), void this._updateTitle(c)) : this._removeTitle();
                },
                "^content.button$": function(a, b, c) {
                    this._updateButton(c);
                },
                "^content.title.(text|button)$": function(a, b, c) {
                    this.set("content." + b, c);
                },
                "^position.(my|at)$": function(a, b, c) {
                    "string" == typeof c && (this.position[b] = a[b] = new A(c, "at" === b));
                },
                "^position.container$": function(a, b, c) {
                    this.rendered && this.tooltip.appendTo(c);
                },
                "^show.ready$": function(a, b, c) {
                    c && (!this.rendered && this.render(D) || this.toggle(D));
                },
                "^style.classes$": function(a, b, c, d) {
                    this.rendered && this.tooltip.removeClass(d).addClass(c);
                },
                "^style.(width|height)": function(a, b, c) {
                    this.rendered && this.tooltip.css(b, c);
                },
                "^style.widget|content.title": function() {
                    this.rendered && this._setWidget();
                },
                "^style.def": function(a, b, c) {
                    this.rendered && this.tooltip.toggleClass(Z, !!c);
                },
                "^events.(render|show|move|hide|focus|blur)$": function(a, b, c) {
                    this.rendered && this.tooltip[(d.isFunction(c) ? "" : "un") + "bind"]("tooltip" + b, c);
                },
                "^(show|hide|position).(event|target|fixed|inactive|leave|distance|viewport|adjust)": function() {
                    if (this.rendered) {
                        var a = this.options.position;
                        this.tooltip.attr("tracking", "mouse" === a.target && a.adjust.mouse), this._unassignEvents(), 
                        this._assignEvents();
                    }
                }
            }
        }, z.get = function(a) {
            if (this.destroyed) return this;
            var b = i(this.options, a.toLowerCase()), c = b[0][b[1]];
            return c.precedance ? c.string() : c;
        };
        var ea = /^position\.(my|at|adjust|target|container|viewport)|style|content|show\.ready/i, fa = /^prerender|show\.ready/i;
        z.set = function(a, b) {
            if (this.destroyed) return this;
            var c, e = this.rendered, f = E, g = this.options;
            return this.checks, "string" == typeof a ? (c = a, a = {}, a[c] = b) : a = d.extend({}, a), 
            d.each(a, function(b, c) {
                if (e && fa.test(b)) return void delete a[b];
                var h, j = i(g, b.toLowerCase());
                h = j[0][j[1]], j[0][j[1]] = c && c.nodeType ? d(c) : c, f = ea.test(b) || f, a[b] = [ j[0], j[1], c, h ];
            }), h(g), this.positioning = D, d.each(a, d.proxy(j, this)), this.positioning = E, 
            this.rendered && this.tooltip[0].offsetWidth > 0 && f && this.reposition("mouse" === g.position.target ? F : this.cache.event), 
            this;
        }, z._update = function(a, b) {
            var c = this, e = this.cache;
            return this.rendered && a ? (d.isFunction(a) && (a = a.call(this.elements.target, e.event, this) || ""), 
            d.isFunction(a.then) ? (e.waiting = D, a.then(function(a) {
                return e.waiting = E, c._update(a, b);
            }, F, function(a) {
                return c._update(a, b);
            })) : a === E || !a && "" !== a ? E : (a.jquery && a.length > 0 ? b.empty().append(a.css({
                display: "block",
                visibility: "visible"
            })) : b.html(a), this._waitForContent(b).then(function(a) {
                c.rendered && c.tooltip[0].offsetWidth > 0 && c.reposition(e.event, !a.length);
            }))) : E;
        }, z._waitForContent = function(a) {
            var b = this.cache;
            return b.waiting = D, (d.fn.imagesLoaded ? a.imagesLoaded() : d.Deferred().resolve([])).done(function() {
                b.waiting = E;
            }).promise();
        }, z._updateContent = function(a, b) {
            this._update(a, this.elements.content, b);
        }, z._updateTitle = function(a, b) {
            this._update(a, this.elements.title, b) === E && this._removeTitle(E);
        }, z._createTitle = function() {
            var a = this.elements, b = this._id + "-title";
            a.titlebar && this._removeTitle(), a.titlebar = d("<div />", {
                "class": S + "-titlebar " + (this.options.style.widget ? k("header") : "")
            }).append(a.title = d("<div />", {
                id: b,
                "class": S + "-title",
                "aria-atomic": D
            })).insertBefore(a.content).delegate(".qtip-close", "mousedown keydown mouseup keyup mouseout", function(a) {
                d(this).toggleClass("ui-state-active ui-state-focus", "down" === a.type.substr(-4));
            }).delegate(".qtip-close", "mouseover mouseout", function(a) {
                d(this).toggleClass("ui-state-hover", "mouseover" === a.type);
            }), this.options.content.button && this._createButton();
        }, z._removeTitle = function(a) {
            var b = this.elements;
            b.title && (b.titlebar.remove(), b.titlebar = b.title = b.button = F, a !== E && this.reposition());
        }, z._createPosClass = function(a) {
            return S + "-pos-" + (a || this.options.position.my).abbrev();
        }, z.reposition = function(c, e) {
            if (!this.rendered || this.positioning || this.destroyed) return this;
            this.positioning = D;
            var f, g, h, i, j = this.cache, k = this.tooltip, l = this.options.position, m = l.target, n = l.my, o = l.at, p = l.viewport, q = l.container, r = l.adjust, s = r.method.split(" "), t = k.outerWidth(E), u = k.outerHeight(E), v = 0, w = 0, x = k.css("position"), y = {
                left: 0,
                top: 0
            }, z = k[0].offsetWidth > 0, A = c && "scroll" === c.type, B = d(a), C = q[0].ownerDocument, F = this.mouse;
            if (d.isArray(m) && 2 === m.length) o = {
                x: L,
                y: K
            }, y = {
                left: m[0],
                top: m[1]
            }; else if ("mouse" === m) o = {
                x: L,
                y: K
            }, (!r.mouse || this.options.hide.distance) && j.origin && j.origin.pageX ? c = j.origin : !c || c && ("resize" === c.type || "scroll" === c.type) ? c = j.event : F && F.pageX && (c = F), 
            "static" !== x && (y = q.offset()), C.body.offsetWidth !== (a.innerWidth || C.documentElement.clientWidth) && (g = d(b.body).offset()), 
            y = {
                left: c.pageX - y.left + (g && g.left || 0),
                top: c.pageY - y.top + (g && g.top || 0)
            }, r.mouse && A && F && (y.left -= (F.scrollX || 0) - B.scrollLeft(), y.top -= (F.scrollY || 0) - B.scrollTop()); else {
                if ("event" === m ? c && c.target && "scroll" !== c.type && "resize" !== c.type ? j.target = d(c.target) : c.target || (j.target = this.elements.target) : "event" !== m && (j.target = d(m.jquery ? m : this.elements.target)), 
                m = j.target, m = d(m).eq(0), 0 === m.length) return this;
                m[0] === b || m[0] === a ? (v = da.iOS ? a.innerWidth : m.width(), w = da.iOS ? a.innerHeight : m.height(), 
                m[0] === a && (y = {
                    top: (p || m).scrollTop(),
                    left: (p || m).scrollLeft()
                })) : R.imagemap && m.is("area") ? f = R.imagemap(this, m, o, R.viewport ? s : E) : R.svg && m && m[0].ownerSVGElement ? f = R.svg(this, m, o, R.viewport ? s : E) : (v = m.outerWidth(E), 
                w = m.outerHeight(E), y = m.offset()), f && (v = f.width, w = f.height, g = f.offset, 
                y = f.position), y = this.reposition.offset(m, y, q), (da.iOS > 3.1 && da.iOS < 4.1 || da.iOS >= 4.3 && da.iOS < 4.33 || !da.iOS && "fixed" === x) && (y.left -= B.scrollLeft(), 
                y.top -= B.scrollTop()), (!f || f && f.adjustable !== E) && (y.left += o.x === N ? v : o.x === O ? v / 2 : 0, 
                y.top += o.y === M ? w : o.y === O ? w / 2 : 0);
            }
            return y.left += r.x + (n.x === N ? -t : n.x === O ? -t / 2 : 0), y.top += r.y + (n.y === M ? -u : n.y === O ? -u / 2 : 0), 
            R.viewport ? (h = y.adjusted = R.viewport(this, y, l, v, w, t, u), g && h.left && (y.left += g.left), 
            g && h.top && (y.top += g.top), h.my && (this.position.my = h.my)) : y.adjusted = {
                left: 0,
                top: 0
            }, j.posClass !== (i = this._createPosClass(this.position.my)) && k.removeClass(j.posClass).addClass(j.posClass = i), 
            this._trigger("move", [ y, p.elem || p ], c) ? (delete y.adjusted, e === E || !z || isNaN(y.left) || isNaN(y.top) || "mouse" === m || !d.isFunction(l.effect) ? k.css(y) : d.isFunction(l.effect) && (l.effect.call(k, this, d.extend({}, y)), 
            k.queue(function(a) {
                d(this).css({
                    opacity: "",
                    height: ""
                }), da.ie && this.style.removeAttribute("filter"), a();
            })), this.positioning = E, this) : this;
        }, z.reposition.offset = function(a, c, e) {
            function f(a, b) {
                c.left += b * a.scrollLeft(), c.top += b * a.scrollTop();
            }
            if (!e[0]) return c;
            var g, h, i, j, k = d(a[0].ownerDocument), l = !!da.ie && "CSS1Compat" !== b.compatMode, m = e[0];
            do "static" !== (h = d.css(m, "position")) && ("fixed" === h ? (i = m.getBoundingClientRect(), 
            f(k, -1)) : (i = d(m).position(), i.left += parseFloat(d.css(m, "borderLeftWidth")) || 0, 
            i.top += parseFloat(d.css(m, "borderTopWidth")) || 0), c.left -= i.left + (parseFloat(d.css(m, "marginLeft")) || 0), 
            c.top -= i.top + (parseFloat(d.css(m, "marginTop")) || 0), g || "hidden" === (j = d.css(m, "overflow")) || "visible" === j || (g = d(m))); while (m = m.offsetParent);
            return g && (g[0] !== k[0] || l) && f(g, 1), c;
        };
        var ga = (A = z.reposition.Corner = function(a, b) {
            a = ("" + a).replace(/([A-Z])/, " $1").replace(/middle/gi, O).toLowerCase(), this.x = (a.match(/left|right/i) || a.match(/center/) || [ "inherit" ])[0].toLowerCase(), 
            this.y = (a.match(/top|bottom|center/i) || [ "inherit" ])[0].toLowerCase(), this.forceY = !!b;
            var c = a.charAt(0);
            this.precedance = "t" === c || "b" === c ? H : G;
        }).prototype;
        ga.invert = function(a, b) {
            this[a] = this[a] === L ? N : this[a] === N ? L : b || this[a];
        }, ga.string = function(a) {
            var b = this.x, c = this.y, d = b !== c ? "center" === b || "center" !== c && (this.precedance === H || this.forceY) ? [ c, b ] : [ b, c ] : [ b ];
            return a !== !1 ? d.join(" ") : d;
        }, ga.abbrev = function() {
            var a = this.string(!1);
            return a[0].charAt(0) + (a[1] && a[1].charAt(0) || "");
        }, ga.clone = function() {
            return new A(this.string(), this.forceY);
        }, z.toggle = function(a, c) {
            var e = this.cache, f = this.options, g = this.tooltip;
            if (c) {
                if (/over|enter/.test(c.type) && e.event && /out|leave/.test(e.event.type) && f.show.target.add(c.target).length === f.show.target.length && g.has(c.relatedTarget).length) return this;
                e.event = d.event.fix(c);
            }
            if (this.waiting && !a && (this.hiddenDuringWait = D), !this.rendered) return a ? this.render(1) : this;
            if (this.destroyed || this.disabled) return this;
            var h, i, j, k = a ? "show" : "hide", l = this.options[k], m = (this.options[a ? "hide" : "show"], 
            this.options.position), n = this.options.content, o = this.tooltip.css("width"), p = this.tooltip.is(":visible"), q = a || 1 === l.target.length, r = !c || l.target.length < 2 || e.target[0] === c.target;
            return (typeof a).search("boolean|number") && (a = !p), h = !g.is(":animated") && p === a && r, 
            i = h ? F : !!this._trigger(k, [ 90 ]), this.destroyed ? this : (i !== E && a && this.focus(c), 
            !i || h ? this : (d.attr(g[0], "aria-hidden", !a), a ? (this.mouse && (e.origin = d.event.fix(this.mouse)), 
            d.isFunction(n.text) && this._updateContent(n.text, E), d.isFunction(n.title) && this._updateTitle(n.title, E), 
            !C && "mouse" === m.target && m.adjust.mouse && (d(b).bind("mousemove." + S, this._storeMouse), 
            C = D), o || g.css("width", g.outerWidth(E)), this.reposition(c, arguments[2]), 
            o || g.css("width", ""), l.solo && ("string" == typeof l.solo ? d(l.solo) : d(W, l.solo)).not(g).not(l.target).qtip("hide", d.Event("tooltipsolo"))) : (clearTimeout(this.timers.show), 
            delete e.origin, C && !d(W + '[tracking="true"]:visible', l.solo).not(g).length && (d(b).unbind("mousemove." + S), 
            C = E), this.blur(c)), j = d.proxy(function() {
                a ? (da.ie && g[0].style.removeAttribute("filter"), g.css("overflow", ""), "string" == typeof l.autofocus && d(this.options.show.autofocus, g).focus(), 
                this.options.show.target.trigger("qtip-" + this.id + "-inactive")) : g.css({
                    display: "",
                    visibility: "",
                    opacity: "",
                    left: "",
                    top: ""
                }), this._trigger(a ? "visible" : "hidden");
            }, this), l.effect === E || q === E ? (g[k](), j()) : d.isFunction(l.effect) ? (g.stop(1, 1), 
            l.effect.call(g, this), g.queue("fx", function(a) {
                j(), a();
            })) : g.fadeTo(90, a ? 1 : 0, j), a && l.target.trigger("qtip-" + this.id + "-inactive"), 
            this));
        }, z.show = function(a) {
            return this.toggle(D, a);
        }, z.hide = function(a) {
            return this.toggle(E, a);
        }, z.focus = function(a) {
            if (!this.rendered || this.destroyed) return this;
            var b = d(W), c = this.tooltip, e = parseInt(c[0].style.zIndex, 10), f = y.zindex + b.length;
            return c.hasClass($) || this._trigger("focus", [ f ], a) && (e !== f && (b.each(function() {
                this.style.zIndex > e && (this.style.zIndex = this.style.zIndex - 1);
            }), b.filter("." + $).qtip("blur", a)), c.addClass($)[0].style.zIndex = f), this;
        }, z.blur = function(a) {
            return !this.rendered || this.destroyed ? this : (this.tooltip.removeClass($), this._trigger("blur", [ this.tooltip.css("zIndex") ], a), 
            this);
        }, z.disable = function(a) {
            return this.destroyed ? this : ("toggle" === a ? a = !(this.rendered ? this.tooltip.hasClass(aa) : this.disabled) : "boolean" != typeof a && (a = D), 
            this.rendered && this.tooltip.toggleClass(aa, a).attr("aria-disabled", a), this.disabled = !!a, 
            this);
        }, z.enable = function() {
            return this.disable(E);
        }, z._createButton = function() {
            var a = this, b = this.elements, c = b.tooltip, e = this.options.content.button, f = "string" == typeof e, g = f ? e : "Close tooltip";
            b.button && b.button.remove(), b.button = e.jquery ? e : d("<a />", {
                "class": "qtip-close " + (this.options.style.widget ? "" : S + "-icon"),
                title: g,
                "aria-label": g
            }).prepend(d("<span />", {
                "class": "ui-icon ui-icon-close",
                html: "&times;"
            })), b.button.appendTo(b.titlebar || c).attr("role", "button").click(function(b) {
                return c.hasClass(aa) || a.hide(b), E;
            });
        }, z._updateButton = function(a) {
            if (!this.rendered) return E;
            var b = this.elements.button;
            a ? this._createButton() : b.remove();
        }, z._setWidget = function() {
            var a = this.options.style.widget, b = this.elements, c = b.tooltip, d = c.hasClass(aa);
            c.removeClass(aa), aa = a ? "ui-state-disabled" : "qtip-disabled", c.toggleClass(aa, d), 
            c.toggleClass("ui-helper-reset " + k(), a).toggleClass(Z, this.options.style.def && !a), 
            b.content && b.content.toggleClass(k("content"), a), b.titlebar && b.titlebar.toggleClass(k("header"), a), 
            b.button && b.button.toggleClass(S + "-icon", !a);
        }, z._storeMouse = function(a) {
            return (this.mouse = d.event.fix(a)).type = "mousemove", this;
        }, z._bind = function(a, b, c, e, f) {
            if (a && c && b.length) {
                var g = "." + this._id + (e ? "-" + e : "");
                return d(a).bind((b.split ? b : b.join(g + " ")) + g, d.proxy(c, f || this)), this;
            }
        }, z._unbind = function(a, b) {
            return a && d(a).unbind("." + this._id + (b ? "-" + b : "")), this;
        }, z._trigger = function(a, b, c) {
            var e = d.Event("tooltip" + a);
            return e.originalEvent = c && d.extend({}, c) || this.cache.event || F, this.triggering = a, 
            this.tooltip.trigger(e, [ this ].concat(b || [])), this.triggering = E, !e.isDefaultPrevented();
        }, z._bindEvents = function(a, b, c, e, f, g) {
            var h = c.filter(e).add(e.filter(c)), i = [];
            h.length && (d.each(b, function(b, c) {
                var e = d.inArray(c, a);
                e > -1 && i.push(a.splice(e, 1)[0]);
            }), i.length && (this._bind(h, i, function(a) {
                var b = this.rendered ? this.tooltip[0].offsetWidth > 0 : !1;
                (b ? g : f).call(this, a);
            }), c = c.not(h), e = e.not(h))), this._bind(c, a, f), this._bind(e, b, g);
        }, z._assignInitialEvents = function(a) {
            function b(a) {
                return this.disabled || this.destroyed ? E : (this.cache.event = a && d.event.fix(a), 
                this.cache.target = a && d(a.target), clearTimeout(this.timers.show), void (this.timers.show = l.call(this, function() {
                    this.render("object" == typeof a || c.show.ready);
                }, c.prerender ? 0 : c.show.delay)));
            }
            var c = this.options, e = c.show.target, f = c.hide.target, g = c.show.event ? d.trim("" + c.show.event).split(" ") : [], h = c.hide.event ? d.trim("" + c.hide.event).split(" ") : [];
            this._bind(this.elements.target, [ "remove", "removeqtip" ], function() {
                this.destroy(!0);
            }, "destroy"), /mouse(over|enter)/i.test(c.show.event) && !/mouse(out|leave)/i.test(c.hide.event) && h.push("mouseleave"), 
            this._bind(e, "mousemove", function(a) {
                this._storeMouse(a), this.cache.onTarget = D;
            }), this._bindEvents(g, h, e, f, b, function() {
                return this.timers ? void clearTimeout(this.timers.show) : E;
            }), (c.show.ready || c.prerender) && b.call(this, a);
        }, z._assignEvents = function() {
            var c = this, e = this.options, f = e.position, g = this.tooltip, h = e.show.target, i = e.hide.target, j = f.container, k = f.viewport, l = d(b), q = (d(b.body), 
            d(a)), r = e.show.event ? d.trim("" + e.show.event).split(" ") : [], s = e.hide.event ? d.trim("" + e.hide.event).split(" ") : [];
            d.each(e.events, function(a, b) {
                c._bind(g, "toggle" === a ? [ "tooltipshow", "tooltiphide" ] : [ "tooltip" + a ], b, null, g);
            }), /mouse(out|leave)/i.test(e.hide.event) && "window" === e.hide.leave && this._bind(l, [ "mouseout", "blur" ], function(a) {
                /select|option/.test(a.target.nodeName) || a.relatedTarget || this.hide(a);
            }), e.hide.fixed ? i = i.add(g.addClass(Y)) : /mouse(over|enter)/i.test(e.show.event) && this._bind(i, "mouseleave", function() {
                clearTimeout(this.timers.show);
            }), ("" + e.hide.event).indexOf("unfocus") > -1 && this._bind(j.closest("html"), [ "mousedown", "touchstart" ], function(a) {
                var b = d(a.target), c = this.rendered && !this.tooltip.hasClass(aa) && this.tooltip[0].offsetWidth > 0, e = b.parents(W).filter(this.tooltip[0]).length > 0;
                b[0] === this.target[0] || b[0] === this.tooltip[0] || e || this.target.has(b[0]).length || !c || this.hide(a);
            }), "number" == typeof e.hide.inactive && (this._bind(h, "qtip-" + this.id + "-inactive", o, "inactive"), 
            this._bind(i.add(g), y.inactiveEvents, o)), this._bindEvents(r, s, h, i, m, n), 
            this._bind(h.add(g), "mousemove", function(a) {
                if ("number" == typeof e.hide.distance) {
                    var b = this.cache.origin || {}, c = this.options.hide.distance, d = Math.abs;
                    (d(a.pageX - b.pageX) >= c || d(a.pageY - b.pageY) >= c) && this.hide(a);
                }
                this._storeMouse(a);
            }), "mouse" === f.target && f.adjust.mouse && (e.hide.event && this._bind(h, [ "mouseenter", "mouseleave" ], function(a) {
                return this.cache ? void (this.cache.onTarget = "mouseenter" === a.type) : E;
            }), this._bind(l, "mousemove", function(a) {
                this.rendered && this.cache.onTarget && !this.tooltip.hasClass(aa) && this.tooltip[0].offsetWidth > 0 && this.reposition(a);
            })), (f.adjust.resize || k.length) && this._bind(d.event.special.resize ? k : q, "resize", p), 
            f.adjust.scroll && this._bind(q.add(f.container), "scroll", p);
        }, z._unassignEvents = function() {
            var c = this.options, e = c.show.target, f = c.hide.target, g = d.grep([ this.elements.target[0], this.rendered && this.tooltip[0], c.position.container[0], c.position.viewport[0], c.position.container.closest("html")[0], a, b ], function(a) {
                return "object" == typeof a;
            });
            e && e.toArray && (g = g.concat(e.toArray())), f && f.toArray && (g = g.concat(f.toArray())), 
            this._unbind(g)._unbind(g, "destroy")._unbind(g, "inactive");
        }, d(function() {
            q(W, [ "mouseenter", "mouseleave" ], function(a) {
                var b = "mouseenter" === a.type, c = d(a.currentTarget), e = d(a.relatedTarget || a.target), f = this.options;
                b ? (this.focus(a), c.hasClass(Y) && !c.hasClass(aa) && clearTimeout(this.timers.hide)) : "mouse" === f.position.target && f.position.adjust.mouse && f.hide.event && f.show.target && !e.closest(f.show.target[0]).length && this.hide(a), 
                c.toggleClass(_, b);
            }), q("[" + U + "]", X, o);
        }), y = d.fn.qtip = function(a, b, e) {
            var f = ("" + a).toLowerCase(), g = F, i = d.makeArray(arguments).slice(1), j = i[i.length - 1], k = this[0] ? d.data(this[0], S) : F;
            return !arguments.length && k || "api" === f ? k : "string" == typeof a ? (this.each(function() {
                var a = d.data(this, S);
                if (!a) return D;
                if (j && j.timeStamp && (a.cache.event = j), !b || "option" !== f && "options" !== f) a[f] && a[f].apply(a, i); else {
                    if (e === c && !d.isPlainObject(b)) return g = a.get(b), E;
                    a.set(b, e);
                }
            }), g !== F ? g : this) : "object" != typeof a && arguments.length ? void 0 : (k = h(d.extend(D, {}, a)), 
            this.each(function(a) {
                var b, c;
                return c = d.isArray(k.id) ? k.id[a] : k.id, c = !c || c === E || c.length < 1 || y.api[c] ? y.nextid++ : c, 
                b = r(d(this), c, k), b === E ? D : (y.api[c] = b, d.each(R, function() {
                    "initialize" === this.initialize && this(b);
                }), void b._assignInitialEvents(j));
            }));
        }, d.qtip = e, y.api = {}, d.each({
            attr: function(a, b) {
                if (this.length) {
                    var c = this[0], e = "title", f = d.data(c, "qtip");
                    if (a === e && f && "object" == typeof f && f.options.suppress) return arguments.length < 2 ? d.attr(c, ca) : (f && f.options.content.attr === e && f.cache.attr && f.set("content.text", b), 
                    this.attr(ca, b));
                }
                return d.fn["attr" + ba].apply(this, arguments);
            },
            clone: function(a) {
                var b = (d([]), d.fn["clone" + ba].apply(this, arguments));
                return a || b.filter("[" + ca + "]").attr("title", function() {
                    return d.attr(this, ca);
                }).removeAttr(ca), b;
            }
        }, function(a, b) {
            if (!b || d.fn[a + ba]) return D;
            var c = d.fn[a + ba] = d.fn[a];
            d.fn[a] = function() {
                return b.apply(this, arguments) || c.apply(this, arguments);
            };
        }), d.ui || (d["cleanData" + ba] = d.cleanData, d.cleanData = function(a) {
            for (var b, c = 0; (b = d(a[c])).length; c++) if (b.attr(T)) try {
                b.triggerHandler("removeqtip");
            } catch (e) {}
            d["cleanData" + ba].apply(this, arguments);
        }), y.version = "2.2.1", y.nextid = 0, y.inactiveEvents = X, y.zindex = 15e3, y.defaults = {
            prerender: E,
            id: E,
            overwrite: D,
            suppress: D,
            content: {
                text: D,
                attr: "title",
                title: E,
                button: E
            },
            position: {
                my: "top left",
                at: "bottom right",
                target: E,
                container: E,
                viewport: E,
                adjust: {
                    x: 0,
                    y: 0,
                    mouse: D,
                    scroll: D,
                    resize: D,
                    method: "flipinvert flipinvert"
                },
                effect: function(a, b) {
                    d(this).animate(b, {
                        duration: 200,
                        queue: E
                    });
                }
            },
            show: {
                target: E,
                event: "mouseenter",
                effect: D,
                delay: 90,
                solo: E,
                ready: E,
                autofocus: E
            },
            hide: {
                target: E,
                event: "mouseleave",
                effect: D,
                delay: 0,
                fixed: E,
                inactive: E,
                leave: "window",
                distance: E
            },
            style: {
                classes: "",
                widget: E,
                width: E,
                height: E,
                def: D
            },
            events: {
                render: F,
                move: F,
                show: F,
                hide: F,
                toggle: F,
                visible: F,
                hidden: F,
                focus: F,
                blur: F
            }
        };
        var ha, ia = "margin", ja = "border", ka = "color", la = "background-color", ma = "transparent", na = " !important", oa = !!b.createElement("canvas").getContext, pa = /rgba?\(0, 0, 0(, 0)?\)|transparent|#123456/i, qa = {}, ra = [ "Webkit", "O", "Moz", "ms" ];
        if (oa) var sa = a.devicePixelRatio || 1, ta = function() {
            var a = b.createElement("canvas").getContext("2d");
            return a.backingStorePixelRatio || a.webkitBackingStorePixelRatio || a.mozBackingStorePixelRatio || a.msBackingStorePixelRatio || a.oBackingStorePixelRatio || 1;
        }(), ua = sa / ta; else var va = function(a, b, c) {
            return "<qtipvml:" + a + ' xmlns="urn:schemas-microsoft.com:vml" class="qtip-vml" ' + (b || "") + ' style="behavior: url(#default#VML); ' + (c || "") + '" />';
        };
        d.extend(v.prototype, {
            init: function(a) {
                var b, c;
                c = this.element = a.elements.tip = d("<div />", {
                    "class": S + "-tip"
                }).prependTo(a.tooltip), oa ? (b = d("<canvas />").appendTo(this.element)[0].getContext("2d"), 
                b.lineJoin = "miter", b.miterLimit = 1e5, b.save()) : (b = va("shape", 'coordorigin="0,0"', "position:absolute;"), 
                this.element.html(b + b), a._bind(d("*", c).add(c), [ "click", "mousedown" ], function(a) {
                    a.stopPropagation();
                }, this._ns)), a._bind(a.tooltip, "tooltipmove", this.reposition, this._ns, this), 
                this.create();
            },
            _swapDimensions: function() {
                this.size[0] = this.options.height, this.size[1] = this.options.width;
            },
            _resetDimensions: function() {
                this.size[0] = this.options.width, this.size[1] = this.options.height;
            },
            _useTitle: function(a) {
                var b = this.qtip.elements.titlebar;
                return b && (a.y === K || a.y === O && this.element.position().top + this.size[1] / 2 + this.options.offset < b.outerHeight(D));
            },
            _parseCorner: function(a) {
                var b = this.qtip.options.position.my;
                return a === E || b === E ? a = E : a === D ? a = new A(b.string()) : a.string || (a = new A(a), 
                a.fixed = D), a;
            },
            _parseWidth: function(a, b, c) {
                var d = this.qtip.elements, e = ja + s(b) + "Width";
                return (c ? u(c, e) : u(d.content, e) || u(this._useTitle(a) && d.titlebar || d.content, e) || u(d.tooltip, e)) || 0;
            },
            _parseRadius: function(a) {
                var b = this.qtip.elements, c = ja + s(a.y) + s(a.x) + "Radius";
                return da.ie < 9 ? 0 : u(this._useTitle(a) && b.titlebar || b.content, c) || u(b.tooltip, c) || 0;
            },
            _invalidColour: function(a, b, c) {
                var d = a.css(b);
                return !d || c && d === a.css(c) || pa.test(d) ? E : d;
            },
            _parseColours: function(a) {
                var b = this.qtip.elements, c = this.element.css("cssText", ""), e = ja + s(a[a.precedance]) + s(ka), f = this._useTitle(a) && b.titlebar || b.content, g = this._invalidColour, h = [];
                return h[0] = g(c, la) || g(f, la) || g(b.content, la) || g(b.tooltip, la) || c.css(la), 
                h[1] = g(c, e, ka) || g(f, e, ka) || g(b.content, e, ka) || g(b.tooltip, e, ka) || b.tooltip.css(e), 
                d("*", c).add(c).css("cssText", la + ":" + ma + na + ";" + ja + ":0" + na + ";"), 
                h;
            },
            _calculateSize: function(a) {
                var b, c, d, e = a.precedance === H, f = this.options.width, g = this.options.height, h = "c" === a.abbrev(), i = (e ? f : g) * (h ? .5 : 1), j = Math.pow, k = Math.round, l = Math.sqrt(j(i, 2) + j(g, 2)), m = [ this.border / i * l, this.border / g * l ];
                return m[2] = Math.sqrt(j(m[0], 2) - j(this.border, 2)), m[3] = Math.sqrt(j(m[1], 2) - j(this.border, 2)), 
                b = l + m[2] + m[3] + (h ? 0 : m[0]), c = b / l, d = [ k(c * f), k(c * g) ], e ? d : d.reverse();
            },
            _calculateTip: function(a, b, c) {
                c = c || 1, b = b || this.size;
                var d = b[0] * c, e = b[1] * c, f = Math.ceil(d / 2), g = Math.ceil(e / 2), h = {
                    br: [ 0, 0, d, e, d, 0 ],
                    bl: [ 0, 0, d, 0, 0, e ],
                    tr: [ 0, e, d, 0, d, e ],
                    tl: [ 0, 0, 0, e, d, e ],
                    tc: [ 0, e, f, 0, d, e ],
                    bc: [ 0, 0, d, 0, f, e ],
                    rc: [ 0, 0, d, g, 0, e ],
                    lc: [ d, 0, d, e, 0, g ]
                };
                return h.lt = h.br, h.rt = h.bl, h.lb = h.tr, h.rb = h.tl, h[a.abbrev()];
            },
            _drawCoords: function(a, b) {
                a.beginPath(), a.moveTo(b[0], b[1]), a.lineTo(b[2], b[3]), a.lineTo(b[4], b[5]), 
                a.closePath();
            },
            create: function() {
                var a = this.corner = (oa || da.ie) && this._parseCorner(this.options.corner);
                return (this.enabled = !!this.corner && "c" !== this.corner.abbrev()) && (this.qtip.cache.corner = a.clone(), 
                this.update()), this.element.toggle(this.enabled), this.corner;
            },
            update: function(b, c) {
                if (!this.enabled) return this;
                var e, f, g, h, i, j, k, l, m = this.qtip.elements, n = this.element, o = n.children(), p = this.options, q = this.size, r = p.mimic, s = Math.round;
                b || (b = this.qtip.cache.corner || this.corner), r === E ? r = b : (r = new A(r), 
                r.precedance = b.precedance, "inherit" === r.x ? r.x = b.x : "inherit" === r.y ? r.y = b.y : r.x === r.y && (r[b.precedance] = b[b.precedance])), 
                f = r.precedance, b.precedance === G ? this._swapDimensions() : this._resetDimensions(), 
                e = this.color = this._parseColours(b), e[1] !== ma ? (l = this.border = this._parseWidth(b, b[b.precedance]), 
                p.border && 1 > l && !pa.test(e[1]) && (e[0] = e[1]), this.border = l = p.border !== D ? p.border : l) : this.border = l = 0, 
                k = this.size = this._calculateSize(b), n.css({
                    width: k[0],
                    height: k[1],
                    lineHeight: k[1] + "px"
                }), j = b.precedance === H ? [ s(r.x === L ? l : r.x === N ? k[0] - q[0] - l : (k[0] - q[0]) / 2), s(r.y === K ? k[1] - q[1] : 0) ] : [ s(r.x === L ? k[0] - q[0] : 0), s(r.y === K ? l : r.y === M ? k[1] - q[1] - l : (k[1] - q[1]) / 2) ], 
                oa ? (g = o[0].getContext("2d"), g.restore(), g.save(), g.clearRect(0, 0, 6e3, 6e3), 
                h = this._calculateTip(r, q, ua), i = this._calculateTip(r, this.size, ua), o.attr(I, k[0] * ua).attr(J, k[1] * ua), 
                o.css(I, k[0]).css(J, k[1]), this._drawCoords(g, i), g.fillStyle = e[1], g.fill(), 
                g.translate(j[0] * ua, j[1] * ua), this._drawCoords(g, h), g.fillStyle = e[0], g.fill()) : (h = this._calculateTip(r), 
                h = "m" + h[0] + "," + h[1] + " l" + h[2] + "," + h[3] + " " + h[4] + "," + h[5] + " xe", 
                j[2] = l && /^(r|b)/i.test(b.string()) ? 8 === da.ie ? 2 : 1 : 0, o.css({
                    coordsize: k[0] + l + " " + (k[1] + l),
                    antialias: "" + (r.string().indexOf(O) > -1),
                    left: j[0] - j[2] * Number(f === G),
                    top: j[1] - j[2] * Number(f === H),
                    width: k[0] + l,
                    height: k[1] + l
                }).each(function(a) {
                    var b = d(this);
                    b[b.prop ? "prop" : "attr"]({
                        coordsize: k[0] + l + " " + (k[1] + l),
                        path: h,
                        fillcolor: e[0],
                        filled: !!a,
                        stroked: !a
                    }).toggle(!(!l && !a)), !a && b.html(va("stroke", 'weight="' + 2 * l + 'px" color="' + e[1] + '" miterlimit="1000" joinstyle="miter"'));
                })), a.opera && setTimeout(function() {
                    m.tip.css({
                        display: "inline-block",
                        visibility: "visible"
                    });
                }, 1), c !== E && this.calculate(b, k);
            },
            calculate: function(a, b) {
                if (!this.enabled) return E;
                var c, e, f = this, g = this.qtip.elements, h = this.element, i = this.options.offset, j = (g.tooltip.hasClass("ui-widget"), 
                {});
                return a = a || this.corner, c = a.precedance, b = b || this._calculateSize(a), 
                e = [ a.x, a.y ], c === G && e.reverse(), d.each(e, function(d, e) {
                    var h, k, l;
                    e === O ? (h = c === H ? L : K, j[h] = "50%", j[ia + "-" + h] = -Math.round(b[c === H ? 0 : 1] / 2) + i) : (h = f._parseWidth(a, e, g.tooltip), 
                    k = f._parseWidth(a, e, g.content), l = f._parseRadius(a), j[e] = Math.max(-f.border, d ? k : i + (l > h ? l : -h)));
                }), j[a[c]] -= b[c === G ? 0 : 1], h.css({
                    margin: "",
                    top: "",
                    bottom: "",
                    left: "",
                    right: ""
                }).css(j), j;
            },
            reposition: function(a, b, d) {
                function e(a, b, c, d, e) {
                    a === Q && j.precedance === b && k[d] && j[c] !== O ? j.precedance = j.precedance === G ? H : G : a !== Q && k[d] && (j[b] = j[b] === O ? k[d] > 0 ? d : e : j[b] === d ? e : d);
                }
                function f(a, b, e) {
                    j[a] === O ? p[ia + "-" + b] = o[a] = g[ia + "-" + b] - k[b] : (h = g[e] !== c ? [ k[b], -g[b] ] : [ -k[b], g[b] ], 
                    (o[a] = Math.max(h[0], h[1])) > h[0] && (d[b] -= k[b], o[b] = E), p[g[e] !== c ? e : b] = o[a]);
                }
                if (this.enabled) {
                    var g, h, i = b.cache, j = this.corner.clone(), k = d.adjusted, l = b.options.position.adjust.method.split(" "), m = l[0], n = l[1] || l[0], o = {
                        left: E,
                        top: E,
                        x: 0,
                        y: 0
                    }, p = {};
                    this.corner.fixed !== D && (e(m, G, H, L, N), e(n, H, G, K, M), (j.string() !== i.corner.string() || i.cornerTop !== k.top || i.cornerLeft !== k.left) && this.update(j, E)), 
                    g = this.calculate(j), g.right !== c && (g.left = -g.right), g.bottom !== c && (g.top = -g.bottom), 
                    g.user = this.offset, (o.left = m === Q && !!k.left) && f(G, L, N), (o.top = n === Q && !!k.top) && f(H, K, M), 
                    this.element.css(p).toggle(!(o.x && o.y || j.x === O && o.y || j.y === O && o.x)), 
                    d.left -= g.left.charAt ? g.user : m !== Q || o.top || !o.left && !o.top ? g.left + this.border : 0, 
                    d.top -= g.top.charAt ? g.user : n !== Q || o.left || !o.left && !o.top ? g.top + this.border : 0, 
                    i.cornerLeft = k.left, i.cornerTop = k.top, i.corner = j.clone();
                }
            },
            destroy: function() {
                this.qtip._unbind(this.qtip.tooltip, this._ns), this.qtip.elements.tip && this.qtip.elements.tip.find("*").remove().end().remove();
            }
        }), ha = R.tip = function(a) {
            return new v(a, a.options.style.tip);
        }, ha.initialize = "render", ha.sanitize = function(a) {
            if (a.style && "tip" in a.style) {
                var b = a.style.tip;
                "object" != typeof b && (b = a.style.tip = {
                    corner: b
                }), /string|boolean/i.test(typeof b.corner) || (b.corner = D);
            }
        }, B.tip = {
            "^position.my|style.tip.(corner|mimic|border)$": function() {
                this.create(), this.qtip.reposition();
            },
            "^style.tip.(height|width)$": function(a) {
                this.size = [ a.width, a.height ], this.update(), this.qtip.reposition();
            },
            "^content.title|style.(classes|widget)$": function() {
                this.update();
            }
        }, d.extend(D, y.defaults, {
            style: {
                tip: {
                    corner: D,
                    mimic: E,
                    width: 6,
                    height: 6,
                    border: D,
                    offset: 0
                }
            }
        });
        var wa, xa, ya = "qtip-modal", za = "." + ya;
        xa = function() {
            function a(a) {
                if (d.expr[":"].focusable) return d.expr[":"].focusable;
                var b, c, e, f = !isNaN(d.attr(a, "tabindex")), g = a.nodeName && a.nodeName.toLowerCase();
                return "area" === g ? (b = a.parentNode, c = b.name, a.href && c && "map" === b.nodeName.toLowerCase() ? (e = d("img[usemap=#" + c + "]")[0], 
                !!e && e.is(":visible")) : !1) : /input|select|textarea|button|object/.test(g) ? !a.disabled : "a" === g ? a.href || f : f;
            }
            function c(a) {
                k.length < 1 && a.length ? a.not("body").blur() : k.first().focus();
            }
            function e(a) {
                if (i.is(":visible")) {
                    var b, e = d(a.target), h = f.tooltip, j = e.closest(W);
                    b = j.length < 1 ? E : parseInt(j[0].style.zIndex, 10) > parseInt(h[0].style.zIndex, 10), 
                    b || e.closest(W)[0] === h[0] || c(e), g = a.target === k[k.length - 1];
                }
            }
            var f, g, h, i, j = this, k = {};
            d.extend(j, {
                init: function() {
                    return i = j.elem = d("<div />", {
                        id: "qtip-overlay",
                        html: "<div></div>",
                        mousedown: function() {
                            return E;
                        }
                    }).hide(), d(b.body).bind("focusin" + za, e), d(b).bind("keydown" + za, function(a) {
                        f && f.options.show.modal.escape && 27 === a.keyCode && f.hide(a);
                    }), i.bind("click" + za, function(a) {
                        f && f.options.show.modal.blur && f.hide(a);
                    }), j;
                },
                update: function(b) {
                    f = b, k = b.options.show.modal.stealfocus !== E ? b.tooltip.find("*").filter(function() {
                        return a(this);
                    }) : [];
                },
                toggle: function(a, e, g) {
                    var k = (d(b.body), a.tooltip), l = a.options.show.modal, m = l.effect, n = e ? "show" : "hide", o = i.is(":visible"), p = d(za).filter(":visible:not(:animated)").not(k);
                    return j.update(a), e && l.stealfocus !== E && c(d(":focus")), i.toggleClass("blurs", l.blur), 
                    e && i.appendTo(b.body), i.is(":animated") && o === e && h !== E || !e && p.length ? j : (i.stop(D, E), 
                    d.isFunction(m) ? m.call(i, e) : m === E ? i[n]() : i.fadeTo(parseInt(g, 10) || 90, e ? 1 : 0, function() {
                        e || i.hide();
                    }), e || i.queue(function(a) {
                        i.css({
                            left: "",
                            top: ""
                        }), d(za).length || i.detach(), a();
                    }), h = e, f.destroyed && (f = F), j);
                }
            }), j.init();
        }, xa = new xa(), d.extend(w.prototype, {
            init: function(a) {
                var b = a.tooltip;
                return this.options.on ? (a.elements.overlay = xa.elem, b.addClass(ya).css("z-index", y.modal_zindex + d(za).length), 
                a._bind(b, [ "tooltipshow", "tooltiphide" ], function(a, c, e) {
                    var f = a.originalEvent;
                    if (a.target === b[0]) if (f && "tooltiphide" === a.type && /mouse(leave|enter)/.test(f.type) && d(f.relatedTarget).closest(xa.elem[0]).length) try {
                        a.preventDefault();
                    } catch (g) {} else (!f || f && "tooltipsolo" !== f.type) && this.toggle(a, "tooltipshow" === a.type, e);
                }, this._ns, this), a._bind(b, "tooltipfocus", function(a, c) {
                    if (!a.isDefaultPrevented() && a.target === b[0]) {
                        var e = d(za), f = y.modal_zindex + e.length, g = parseInt(b[0].style.zIndex, 10);
                        xa.elem[0].style.zIndex = f - 1, e.each(function() {
                            this.style.zIndex > g && (this.style.zIndex -= 1);
                        }), e.filter("." + $).qtip("blur", a.originalEvent), b.addClass($)[0].style.zIndex = f, 
                        xa.update(c);
                        try {
                            a.preventDefault();
                        } catch (h) {}
                    }
                }, this._ns, this), void a._bind(b, "tooltiphide", function(a) {
                    a.target === b[0] && d(za).filter(":visible").not(b).last().qtip("focus", a);
                }, this._ns, this)) : this;
            },
            toggle: function(a, b, c) {
                return a && a.isDefaultPrevented() ? this : void xa.toggle(this.qtip, !!b, c);
            },
            destroy: function() {
                this.qtip.tooltip.removeClass(ya), this.qtip._unbind(this.qtip.tooltip, this._ns), 
                xa.toggle(this.qtip, E), delete this.qtip.elements.overlay;
            }
        }), wa = R.modal = function(a) {
            return new w(a, a.options.show.modal);
        }, wa.sanitize = function(a) {
            a.show && ("object" != typeof a.show.modal ? a.show.modal = {
                on: !!a.show.modal
            } : "undefined" == typeof a.show.modal.on && (a.show.modal.on = D));
        }, y.modal_zindex = y.zindex - 200, wa.initialize = "render", B.modal = {
            "^show.modal.(on|blur)$": function() {
                this.destroy(), this.init(), this.qtip.elems.overlay.toggle(this.qtip.tooltip[0].offsetWidth > 0);
            }
        }, d.extend(D, y.defaults, {
            show: {
                modal: {
                    on: E,
                    effect: D,
                    blur: D,
                    stealfocus: D,
                    escape: D
                }
            }
        }), R.viewport = function(c, d, e, f, g, h, i) {
            function j(a, b, c, e, f, g, h, i, j) {
                var k = d[f], s = u[a], t = v[a], w = c === Q, x = s === f ? j : s === g ? -j : -j / 2, y = t === f ? i : t === g ? -i : -i / 2, z = q[f] + r[f] - (n ? 0 : m[f]), A = z - k, B = k + j - (h === I ? o : p) - z, C = x - (u.precedance === a || s === u[b] ? y : 0) - (t === O ? i / 2 : 0);
                return w ? (C = (s === f ? 1 : -1) * x, d[f] += A > 0 ? A : B > 0 ? -B : 0, d[f] = Math.max(-m[f] + r[f], k - C, Math.min(Math.max(-m[f] + r[f] + (h === I ? o : p), k + C), d[f], "center" === s ? k - x : 1e9))) : (e *= c === P ? 2 : 0, 
                A > 0 && (s !== f || B > 0) ? (d[f] -= C + e, l.invert(a, f)) : B > 0 && (s !== g || A > 0) && (d[f] -= (s === O ? -C : C) + e, 
                l.invert(a, g)), d[f] < q && -d[f] > B && (d[f] = k, l = u.clone())), d[f] - k;
            }
            var k, l, m, n, o, p, q, r, s = e.target, t = c.elements.tooltip, u = e.my, v = e.at, w = e.adjust, x = w.method.split(" "), y = x[0], z = x[1] || x[0], A = e.viewport, B = e.container, C = (c.cache, 
            {
                left: 0,
                top: 0
            });
            return A.jquery && s[0] !== a && s[0] !== b.body && "none" !== w.method ? (m = B.offset() || C, 
            n = "static" === B.css("position"), k = "fixed" === t.css("position"), o = A[0] === a ? A.width() : A.outerWidth(E), 
            p = A[0] === a ? A.height() : A.outerHeight(E), q = {
                left: k ? 0 : A.scrollLeft(),
                top: k ? 0 : A.scrollTop()
            }, r = A.offset() || C, ("shift" !== y || "shift" !== z) && (l = u.clone()), C = {
                left: "none" !== y ? j(G, H, y, w.x, L, N, I, f, h) : 0,
                top: "none" !== z ? j(H, G, z, w.y, K, M, J, g, i) : 0,
                my: l
            }) : C;
        }, R.polys = {
            polygon: function(a, b) {
                var c, d, e, f = {
                    width: 0,
                    height: 0,
                    position: {
                        top: 1e10,
                        right: 0,
                        bottom: 0,
                        left: 1e10
                    },
                    adjustable: E
                }, g = 0, h = [], i = 1, j = 1, k = 0, l = 0;
                for (g = a.length; g--; ) c = [ parseInt(a[--g], 10), parseInt(a[g + 1], 10) ], 
                c[0] > f.position.right && (f.position.right = c[0]), c[0] < f.position.left && (f.position.left = c[0]), 
                c[1] > f.position.bottom && (f.position.bottom = c[1]), c[1] < f.position.top && (f.position.top = c[1]), 
                h.push(c);
                if (d = f.width = Math.abs(f.position.right - f.position.left), e = f.height = Math.abs(f.position.bottom - f.position.top), 
                "c" === b.abbrev()) f.position = {
                    left: f.position.left + f.width / 2,
                    top: f.position.top + f.height / 2
                }; else {
                    for (;d > 0 && e > 0 && i > 0 && j > 0; ) for (d = Math.floor(d / 2), e = Math.floor(e / 2), 
                    b.x === L ? i = d : b.x === N ? i = f.width - d : i += Math.floor(d / 2), b.y === K ? j = e : b.y === M ? j = f.height - e : j += Math.floor(e / 2), 
                    g = h.length; g-- && !(h.length < 2); ) k = h[g][0] - f.position.left, l = h[g][1] - f.position.top, 
                    (b.x === L && k >= i || b.x === N && i >= k || b.x === O && (i > k || k > f.width - i) || b.y === K && l >= j || b.y === M && j >= l || b.y === O && (j > l || l > f.height - j)) && h.splice(g, 1);
                    f.position = {
                        left: h[0][0],
                        top: h[0][1]
                    };
                }
                return f;
            },
            rect: function(a, b, c, d) {
                return {
                    width: Math.abs(c - a),
                    height: Math.abs(d - b),
                    position: {
                        left: Math.min(a, c),
                        top: Math.min(b, d)
                    }
                };
            },
            _angles: {
                tc: 1.5,
                tr: 7 / 4,
                tl: 5 / 4,
                bc: .5,
                br: .25,
                bl: .75,
                rc: 2,
                lc: 1,
                c: 0
            },
            ellipse: function(a, b, c, d, e) {
                var f = R.polys._angles[e.abbrev()], g = 0 === f ? 0 : c * Math.cos(f * Math.PI), h = d * Math.sin(f * Math.PI);
                return {
                    width: 2 * c - Math.abs(g),
                    height: 2 * d - Math.abs(h),
                    position: {
                        left: a + g,
                        top: b + h
                    },
                    adjustable: E
                };
            },
            circle: function(a, b, c, d) {
                return R.polys.ellipse(a, b, c, c, d);
            }
        }, R.svg = function(a, c, e) {
            for (var f, g, h, i, j, k, l, m, n, o = (d(b), c[0]), p = d(o.ownerSVGElement), q = o.ownerDocument, r = (parseInt(c.css("stroke-width"), 10) || 0) / 2; !o.getBBox; ) o = o.parentNode;
            if (!o.getBBox || !o.parentNode) return E;
            switch (o.nodeName) {
              case "ellipse":
              case "circle":
                m = R.polys.ellipse(o.cx.baseVal.value, o.cy.baseVal.value, (o.rx || o.r).baseVal.value + r, (o.ry || o.r).baseVal.value + r, e);
                break;

              case "line":
              case "polygon":
              case "polyline":
                for (l = o.points || [ {
                    x: o.x1.baseVal.value,
                    y: o.y1.baseVal.value
                }, {
                    x: o.x2.baseVal.value,
                    y: o.y2.baseVal.value
                } ], m = [], k = -1, i = l.numberOfItems || l.length; ++k < i; ) j = l.getItem ? l.getItem(k) : l[k], 
                m.push.apply(m, [ j.x, j.y ]);
                m = R.polys.polygon(m, e);
                break;

              default:
                m = o.getBBox(), m = {
                    width: m.width,
                    height: m.height,
                    position: {
                        left: m.x,
                        top: m.y
                    }
                };
            }
            return n = m.position, p = p[0], p.createSVGPoint && (g = o.getScreenCTM(), l = p.createSVGPoint(), 
            l.x = n.left, l.y = n.top, h = l.matrixTransform(g), n.left = h.x, n.top = h.y), 
            q !== b && "mouse" !== a.position.target && (f = d((q.defaultView || q.parentWindow).frameElement).offset(), 
            f && (n.left += f.left, n.top += f.top)), q = d(q), n.left += q.scrollLeft(), n.top += q.scrollTop(), 
            m;
        }, R.imagemap = function(a, b, c) {
            b.jquery || (b = d(b));
            var e, f, g, h, i, j = (b.attr("shape") || "rect").toLowerCase().replace("poly", "polygon"), k = d('img[usemap="#' + b.parent("map").attr("name") + '"]'), l = d.trim(b.attr("coords")), m = l.replace(/,$/, "").split(",");
            if (!k.length) return E;
            if ("polygon" === j) h = R.polys.polygon(m, c); else {
                if (!R.polys[j]) return E;
                for (g = -1, i = m.length, f = []; ++g < i; ) f.push(parseInt(m[g], 10));
                h = R.polys[j].apply(this, f.concat(c));
            }
            return e = k.offset(), e.left += Math.ceil((k.outerWidth(E) - k.width()) / 2), e.top += Math.ceil((k.outerHeight(E) - k.height()) / 2), 
            h.position.left += e.left, h.position.top += e.top, h;
        };
        var Aa, Ba = '<iframe class="qtip-bgiframe" frameborder="0" tabindex="-1" src="javascript:\'\';"  style="display:block; position:absolute; z-index:-1; filter:alpha(opacity=0); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";"></iframe>';
        d.extend(x.prototype, {
            _scroll: function() {
                var b = this.qtip.elements.overlay;
                b && (b[0].style.top = d(a).scrollTop() + "px");
            },
            init: function(c) {
                var e = c.tooltip;
                d("select, object").length < 1 && (this.bgiframe = c.elements.bgiframe = d(Ba).appendTo(e), 
                c._bind(e, "tooltipmove", this.adjustBGIFrame, this._ns, this)), this.redrawContainer = d("<div/>", {
                    id: S + "-rcontainer"
                }).appendTo(b.body), c.elements.overlay && c.elements.overlay.addClass("qtipmodal-ie6fix") && (c._bind(a, [ "scroll", "resize" ], this._scroll, this._ns, this), 
                c._bind(e, [ "tooltipshow" ], this._scroll, this._ns, this)), this.redraw();
            },
            adjustBGIFrame: function() {
                var a, b, c = this.qtip.tooltip, d = {
                    height: c.outerHeight(E),
                    width: c.outerWidth(E)
                }, e = this.qtip.plugins.tip, f = this.qtip.elements.tip;
                b = parseInt(c.css("borderLeftWidth"), 10) || 0, b = {
                    left: -b,
                    top: -b
                }, e && f && (a = "x" === e.corner.precedance ? [ I, L ] : [ J, K ], b[a[1]] -= f[a[0]]()), 
                this.bgiframe.css(b).css(d);
            },
            redraw: function() {
                if (this.qtip.rendered < 1 || this.drawing) return this;
                var a, b, c, d, e = this.qtip.tooltip, f = this.qtip.options.style, g = this.qtip.options.position.container;
                return this.qtip.drawing = 1, f.height && e.css(J, f.height), f.width ? e.css(I, f.width) : (e.css(I, "").appendTo(this.redrawContainer), 
                b = e.width(), 1 > b % 2 && (b += 1), c = e.css("maxWidth") || "", d = e.css("minWidth") || "", 
                a = (c + d).indexOf("%") > -1 ? g.width() / 100 : 0, c = (c.indexOf("%") > -1 ? a : 1) * parseInt(c, 10) || b, 
                d = (d.indexOf("%") > -1 ? a : 1) * parseInt(d, 10) || 0, b = c + d ? Math.min(Math.max(b, d), c) : b, 
                e.css(I, Math.round(b)).appendTo(g)), this.drawing = 0, this;
            },
            destroy: function() {
                this.bgiframe && this.bgiframe.remove(), this.qtip._unbind([ a, this.qtip.tooltip ], this._ns);
            }
        }), Aa = R.ie6 = function(a) {
            return 6 === da.ie ? new x(a) : E;
        }, Aa.initialize = "render", B.ie6 = {
            "^content|style$": function() {
                this.redraw();
            }
        };
    });
}(window, document), function(a) {
    var b = 0, c = 0, d = 0, e = 10, f = 0, g = "ontouchstart" in window || navigator.msMaxTouchPoints > 0, h = "onorientationchange" in window, i = !1, j = !1, k = !1, l = !1, m = !1, n = !1, o = !1, p = "pointer", q = "pointer", r = new Array(), s = new Array(), t = new Array(), u = new Array(), v = new Array(), w = new Array(), x = new Array(), y = new Array(), z = new Array(), A = new Array(), B = new Array(), C = new Array(), D = new Array(), E = {
        showScrollbar: function(b, c) {
            b.scrollbarHide && a("." + c).css({
                opacity: b.scrollbarOpacity,
                filter: "alpha(opacity:" + 100 * b.scrollbarOpacity + ")"
            });
        },
        hideScrollbar: function(a, b, c, d, f, g, h, i, j, k) {
            if (a.scrollbar && a.scrollbarHide) for (var l = c; c + 25 > l; l++) b[b.length] = E.hideScrollbarIntervalTimer(e * l, d[c], (c + 24 - l) / 24, f, g, h, i, j, k, a);
        },
        hideScrollbarInterval: function(b, c, d, e, g, h, i, j, k) {
            f = -1 * b / B[j] * (g - h - i - e), E.setSliderOffset("." + d, f), a("." + d).css({
                opacity: k.scrollbarOpacity * c,
                filter: "alpha(opacity:" + k.scrollbarOpacity * c * 100 + ")"
            });
        },
        slowScrollHorizontalInterval: function(b, c, d, e, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u) {
            if (u.infiniteSlider) {
                if (d <= -1 * B[r] || d <= -1 * C[r]) {
                    var v = a(b).width();
                    if (d <= -1 * C[r]) {
                        var w = -1 * m[0];
                        a(c).each(function(b) {
                            E.setSliderOffset(a(c)[b], w + s), b < n.length && (n[b] = -1 * w), w += q[b];
                        }), d += -1 * n[0], A[r] = -1 * n[0] + s, B[r] = A[r] + v - h, z[r] = 0;
                    }
                    for (;d <= -1 * B[r]; ) {
                        var D = 0, F = E.getSliderOffset(a(c[0]), "x");
                        a(c).each(function(a) {
                            E.getSliderOffset(this, "x") < F && (F = E.getSliderOffset(this, "x"), D = a);
                        });
                        var G = A[r] + v;
                        E.setSliderOffset(a(c)[D], G), A[r] = -1 * n[1] + s, B[r] = A[r] + v - h, n.splice(0, 1), 
                        n.splice(n.length, 0, -1 * G + s), z[r]++;
                    }
                }
                if (d >= -1 * A[r] || d >= 0) {
                    var v = a(b).width();
                    if (d > 0) {
                        var w = -1 * m[0];
                        for (a(c).each(function(b) {
                            E.setSliderOffset(a(c)[b], w + s), b < n.length && (n[b] = -1 * w), w += q[b];
                        }), d -= -1 * n[0], A[r] = -1 * n[0] + s, B[r] = A[r] + v - h, z[r] = p; -1 * n[0] - v + s > 0; ) {
                            var H = 0, I = E.getSliderOffset(a(c[0]), "x");
                            a(c).each(function(a) {
                                E.getSliderOffset(this, "x") > I && (I = E.getSliderOffset(this, "x"), H = a);
                            });
                            var G = A[r] - q[H];
                            E.setSliderOffset(a(c)[H], G), n.splice(0, 0, -1 * G + s), n.splice(n.length - 1, 1), 
                            A[r] = -1 * n[0] + s, B[r] = A[r] + v - h, z[r]--, x[r]++;
                        }
                    }
                    for (;d > -1 * A[r]; ) {
                        var H = 0, I = E.getSliderOffset(a(c[0]), "x");
                        a(c).each(function(a) {
                            E.getSliderOffset(this, "x") > I && (I = E.getSliderOffset(this, "x"), H = a);
                        });
                        var G = A[r] - q[H];
                        E.setSliderOffset(a(c)[H], G), n.splice(0, 0, -1 * G + s), n.splice(n.length - 1, 1), 
                        A[r] = -1 * n[0] + s, B[r] = A[r] + v - h, z[r]--;
                    }
                }
            }
            var J = !1, K = E.calcActiveOffset(u, d, n, h, z[r], p, l, r), G = (K + z[r] + p) % p;
            if (u.infiniteSlider ? G != y[r] && (J = !0) : K != x[r] && (J = !0), J) {
                var L = new E.args("change", u, b, a(b).children(":eq(" + G + ")"), G, t);
                a(b).parent().data("args", L), "" != u.onSlideChange && u.onSlideChange(L);
            }
            if (x[r] = K, y[r] = G, d = Math.floor(d), r != a(b).parent().data("args").data.sliderNumber) return !0;
            if (E.setSliderOffset(b, d), u.scrollbar) {
                f = Math.floor((-1 * d - A[r] + s) / (B[r] - A[r] + s) * (i - j - g));
                var M = g - k;
                d >= -1 * A[r] + s ? (M = g - k - -1 * f, E.setSliderOffset(a("." + e), 0), a("." + e).css({
                    width: M + "px"
                })) : d <= -1 * B[r] + 1 ? (M = i - j - k - f, E.setSliderOffset(a("." + e), f), 
                a("." + e).css({
                    width: M + "px"
                })) : (E.setSliderOffset(a("." + e), f), a("." + e).css({
                    width: M + "px"
                }));
            }
        },
        slowScrollHorizontal: function(b, c, d, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, C) {
            var D = E.getSliderOffset(b, "x"), F = new Array(), G = new Array(), H = 0, I = 25 / 1024 * j;
            frictionCoefficient = C.frictionCoefficient, elasticFrictionCoefficient = C.elasticFrictionCoefficient, 
            snapFrictionCoefficient = C.snapFrictionCoefficient, g > C.snapVelocityThreshold && C.snapToChildren && !u ? H = 1 : g < -1 * C.snapVelocityThreshold && C.snapToChildren && !u && (H = -1), 
            -1 * I > g ? g = -1 * I : g > I && (g = I), a(b)[0] !== a(t)[0] && (H = -1 * H, 
            g = -2 * g);
            var J = z[q];
            if (C.infiniteSlider) var K = A[q], L = B[q];
            for (var M = new Array(), N = new Array(), O = 0; O < o.length; O++) M[O] = o[O], 
            O < c.length && (N[O] = E.getSliderOffset(a(c[O]), "x"));
            for (;g > 1 || -1 > g; ) {
                if (g *= frictionCoefficient, D += g, (D > -1 * A[q] || D < -1 * B[q]) && !C.infiniteSlider && (g *= elasticFrictionCoefficient, 
                D += g), C.infiniteSlider) {
                    if (-1 * L >= D) {
                        for (var P = a(b).width(), Q = 0, R = N[0], O = 0; O < N.length; O++) N[O] < R && (R = N[O], 
                        Q = O);
                        var S = K + P;
                        N[Q] = S, K = -1 * M[1] + v, L = K + P - j, M.splice(0, 1), M.splice(M.length, 0, -1 * S + v), 
                        J++;
                    }
                    if (D >= -1 * K) {
                        for (var P = a(b).width(), T = 0, U = N[0], O = 0; O < N.length; O++) N[O] > U && (U = N[O], 
                        T = O);
                        var S = K - p[T];
                        N[T] = S, M.splice(0, 0, -1 * S + v), M.splice(M.length - 1, 1), K = -1 * M[0] + v, 
                        L = K + P - j, J--;
                    }
                }
                F[F.length] = D, G[G.length] = g;
            }
            var V = !1, W = E.calcActiveOffset(C, D, M, j, J, s, x[q], q), X = (W + J + s) % s;
            if (C.snapToChildren && (C.infiniteSlider ? X != y[q] && (V = !0) : W != x[q] && (V = !0), 
            0 > H && !V ? (W++, W >= o.length && !C.infiniteSlider && (W = o.length - 1)) : H > 0 && !V && (W--, 
            0 > W && !C.infiniteSlider && (W = 0))), C.snapToChildren || (D > -1 * A[q] || D < -1 * B[q]) && !C.infiniteSlider) {
                for ((D > -1 * A[q] || D < -1 * B[q]) && !C.infiniteSlider ? F.splice(0, F.length) : (F.splice(.1 * F.length, F.length), 
                D = F.length > 0 ? F[F.length - 1] : D); D < M[W] - .5 || D > M[W] + .5; ) D = (D - M[W]) * snapFrictionCoefficient + M[W], 
                F[F.length] = D;
                F[F.length] = M[W];
            }
            var Y = 1;
            F.length % 2 != 0 && (Y = 0);
            for (var Z = 0; Z < d.length; Z++) clearTimeout(d[Z]);
            for (var $ = (W + J + s) % s, _ = 0, Z = Y; Z < F.length; Z += 2) (Z == Y || Math.abs(F[Z] - _) > 1 || Z >= F.length - 2) && (_ = F[Z], 
            d[d.length] = E.slowScrollHorizontalIntervalTimer(e * Z, b, c, F[Z], f, i, j, k, l, m, W, n, o, r, s, p, q, v, $, C));
            var V = !1, X = (W + z[q] + s) % s;
            C.infiniteSlider ? X != y[q] && (V = !0) : W != x[q] && (V = !0), "" != C.onSlideComplete && F.length > 1 && (d[d.length] = E.onSlideCompleteTimer(e * (Z + 1), C, b, a(b).children(":eq(" + X + ")"), $, q)), 
            d[d.length] = E.updateBackfaceVisibilityTimer(e * (Z + 1), c, q, s, C), w[q] = d, 
            E.hideScrollbar(C, d, Z, F, f, i, j, l, m, q);
        },
        onSlideComplete: function(b, c, d, e, f) {
            var g = (r[f] != e ? !0 : !1, new E.args("complete", b, a(c), d, e, e));
            a(c).parent().data("args", g), "" != b.onSlideComplete && b.onSlideComplete(g), 
            r[f] = e;
        },
        getSliderOffset: function(b, c) {
            var d = 0;
            if (c = "x" == c ? 4 : 5, !j || k || l) d = parseInt(a(b).css("left"), 10); else {
                for (var e, f = new Array("-webkit-transform", "-moz-transform", "transform"), g = 0; g < f.length; g++) if (void 0 != a(b).css(f[g]) && a(b).css(f[g]).length > 0) {
                    e = a(b).css(f[g]).split(",");
                    break;
                }
                d = void 0 == e[c] ? 0 : parseInt(e[c], 10);
            }
            return d;
        },
        setSliderOffset: function(b, c) {
            c = parseInt(c, 10), !j || k || l ? a(b).css({
                left: c + "px"
            }) : a(b).css({
                msTransform: "matrix(1,0,0,1," + c + ",0)",
                webkitTransform: "matrix(1,0,0,1," + c + ",0)",
                MozTransform: "matrix(1,0,0,1," + c + ",0)",
                transform: "matrix(1,0,0,1," + c + ",0)"
            });
        },
        setBrowserInfo: function() {
            null != navigator.userAgent.match("WebKit") ? (i = !0, p = "-webkit-grab", q = "-webkit-grabbing") : null != navigator.userAgent.match("Gecko") ? (o = !0, 
            p = "move", q = "-moz-grabbing") : null != navigator.userAgent.match("MSIE 7") ? (k = !0, 
            n = !0) : null != navigator.userAgent.match("MSIE 8") ? (l = !0, n = !0) : null != navigator.userAgent.match("MSIE 9") && (m = !0, 
            n = !0);
        },
        has3DTransform: function() {
            var b = !1, c = a("<div />").css({
                msTransform: "matrix(1,1,1,1,1,1)",
                webkitTransform: "matrix(1,1,1,1,1,1)",
                MozTransform: "matrix(1,1,1,1,1,1)",
                transform: "matrix(1,1,1,1,1,1)"
            });
            return "" == c.attr("style") ? b = !1 : o && parseInt(navigator.userAgent.split("/")[3], 10) >= 21 ? b = !1 : void 0 != c.attr("style") && (b = !0), 
            b;
        },
        getSlideNumber: function(a, b, c) {
            return (a - z[b] + c) % c;
        },
        calcActiveOffset: function(a, b, c, d, e, f, g, h) {
            var i, j = !1, k = new Array();
            b > c[0] && (i = 0), b < c[c.length - 1] && (i = f - 1);
            for (var l = 0; l < c.length; l++) c[l] <= b && c[l] > b - d && (j || c[l] == b || (k[k.length] = c[l - 1]), 
            k[k.length] = c[l], j = !0);
            0 == k.length && (k[0] = c[c.length - 1]);
            for (var m = d, n = 0, l = 0; l < k.length; l++) {
                var o = Math.abs(b - k[l]);
                m > o && (n = k[l], m = o);
            }
            for (var l = 0; l < c.length; l++) n == c[l] && (i = l);
            return i;
        },
        changeSlide: function(b, c, d, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t) {
            E.autoSlidePause(p);
            for (var u = 0; u < f.length; u++) clearTimeout(f[u]);
            var v = Math.ceil(t.autoSlideTransTimer / 10) + 1, A = E.getSliderOffset(c, "x"), B = n[b], C = B - A, D = b - (x[p] + z[p] + r) % r;
            if (t.infiniteSlider) {
                b = (b - z[p] + 2 * r) % r;
                var F = !1;
                0 == b && 2 == r && (b = r, n[b] = n[b - 1] - a(d).eq(0).outerWidth(!0), F = !0), 
                B = n[b], C = B - A;
                var G = new Array(n[b] - a(c).width(), n[b] + a(c).width());
                F && n.splice(n.length - 1, 1);
                for (var H = 0; H < G.length; H++) Math.abs(G[H] - A) < Math.abs(C) && (C = G[H] - A);
            }
            0 > C && -1 == D ? C += a(c).width() : C > 0 && 1 == D && (C -= a(c).width());
            var I, J, K = new Array();
            E.showScrollbar(t, g);
            for (var H = 0; v >= H; H++) I = H, I /= v, I--, J = A + C * (Math.pow(I, 5) + 1), 
            K[K.length] = J;
            for (var L = (b + z[p] + r) % r, M = 0, H = 0; H < K.length; H++) if ((0 == H || Math.abs(K[H] - M) > 1 || H >= K.length - 2) && (M = K[H], 
            f[H] = E.slowScrollHorizontalIntervalTimer(e * (H + 1), c, d, K[H], g, h, i, j, k, l, b, m, n, q, r, o, p, s, L, t)), 
            0 == H && "" != t.onSlideStart) {
                var N = (x[p] + z[p] + r) % r;
                t.onSlideStart(new E.args("start", t, c, a(c).children(":eq(" + N + ")"), N, b));
            }
            var O = !1;
            t.infiniteSlider ? L != y[p] && (O = !0) : b != x[p] && (O = !0), O && "" != t.onSlideComplete && (f[f.length] = E.onSlideCompleteTimer(e * (H + 1), t, c, a(c).children(":eq(" + L + ")"), L, p)), 
            w[p] = f, E.hideScrollbar(t, f, H, K, g, h, i, k, l, p), E.autoSlide(c, d, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t);
        },
        changeOffset: function(b, c, d, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t) {
            E.autoSlidePause(p);
            for (var u = 0; u < f.length; u++) clearTimeout(f[u]);
            t.infiniteSlider || (b = b > -1 * A[p] + s ? -1 * A[p] + s : b, b = b < -1 * B[p] ? -1 * B[p] : b);
            var v = Math.ceil(t.autoSlideTransTimer / 10) + 1, C = E.getSliderOffset(c, "x"), D = (E.calcActiveOffset(t, b, n, i, z, r, x[p], p) + z[p] + r) % r, F = n.slice();
            if (t.snapToChildren && !t.infiniteSlider) b = n[D]; else if (t.infiniteSlider && t.snapToChildren) {
                for (;b >= F[0]; ) F.splice(0, 0, F[r - 1] + a(c).width()), F.splice(r, 1);
                for (;b <= F[r - 1]; ) F.splice(r, 0, F[0] - a(c).width()), F.splice(0, 1);
                D = E.calcActiveOffset(t, b, F, i, z, r, x[p], p), b = F[D];
            }
            var G, H, I = b - C, J = new Array();
            E.showScrollbar(t, g);
            for (var K = 0; v >= K; K++) G = K, G /= v, G--, H = C + I * (Math.pow(G, 5) + 1), 
            J[J.length] = H;
            for (var L = (D + z[p] + r) % r, M = 0, K = 0; K < J.length; K++) if ((0 == K || Math.abs(J[K] - M) > 1 || K >= J.length - 2) && (M = J[K], 
            f[K] = E.slowScrollHorizontalIntervalTimer(e * (K + 1), c, d, J[K], g, h, i, j, k, l, D, m, n, q, r, o, p, s, L, t)), 
            0 == K && "" != t.onSlideStart) {
                var L = (x[p] + z[p] + r) % r;
                t.onSlideStart(new E.args("start", t, c, a(c).children(":eq(" + L + ")"), L, D));
            }
            var N = !1;
            t.infiniteSlider ? L != y[p] && (N = !0) : D != x[p] && (N = !0), N && "" != t.onSlideComplete && (f[f.length] = E.onSlideCompleteTimer(e * (K + 1), t, c, a(c).children(":eq(" + L + ")"), L, p)), 
            w[p] = f, E.hideScrollbar(t, f, K, J, g, h, i, k, l, p), E.autoSlide(c, d, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t);
        },
        autoSlide: function(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q) {
            return u[m].autoSlide ? (E.autoSlidePause(m), void (s[m] = setTimeout(function() {
                !q.infiniteSlider && x[m] > k.length - 1 && (x[m] = x[m] - o);
                var r = x[m] + z[m] + 1;
                E.changeSlide(r, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q), E.autoSlide(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q);
            }, q.autoSlideTimer + q.autoSlideTransTimer))) : !1;
        },
        autoSlidePause: function(a) {
            clearTimeout(s[a]);
        },
        isUnselectable: function(b, c) {
            return "" != c.unselectableSelector && 1 == a(b).closest(c.unselectableSelector).length ? !0 : !1;
        },
        slowScrollHorizontalIntervalTimer: function(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t) {
            var u = setTimeout(function() {
                E.slowScrollHorizontalInterval(b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t);
            }, a);
            return u;
        },
        onSlideCompleteTimer: function(a, b, c, d, e, f) {
            var g = setTimeout(function() {
                E.onSlideComplete(b, c, d, e, f);
            }, a);
            return g;
        },
        hideScrollbarIntervalTimer: function(a, b, c, d, e, f, g, h, i, j) {
            var k = setTimeout(function() {
                E.hideScrollbarInterval(b, c, d, e, f, g, h, i, j);
            }, a);
            return k;
        },
        updateBackfaceVisibilityTimer: function(a, b, c, d, e) {
            var f = setTimeout(function() {
                E.updateBackfaceVisibility(b, c, d, e);
            }, a);
            return f;
        },
        updateBackfaceVisibility: function(b, c, d, e) {
            for (var f = (x[c] + z[c] + d) % d, g = Array(), h = 0; h < 2 * e.hardwareAccelBuffer; h++) {
                var i = E.mod(f + h - e.hardwareAccelBuffer, d);
                if ("visible" == a(b).eq(i).css("-webkit-backface-visibility")) {
                    g[g.length] = i;
                    var j = E.mod(i + 2 * e.hardwareAccelBuffer, d), k = E.mod(i - 2 * e.hardwareAccelBuffer, d);
                    a(b).eq(i).css("-webkit-backface-visibility", "hidden"), -1 == g.indexOf(k) && a(b).eq(k).css("-webkit-backface-visibility", ""), 
                    -1 == g.indexOf(j) && a(b).eq(j).css("-webkit-backface-visibility", "");
                }
            }
        },
        mod: function(a, b) {
            var c = a % b;
            return 0 > c ? c + b : c;
        },
        args: function(b, c, d, e, f, g) {
            this.prevSlideNumber = void 0 == a(d).parent().data("args") ? void 0 : a(d).parent().data("args").prevSlideNumber, 
            this.prevSlideObject = void 0 == a(d).parent().data("args") ? void 0 : a(d).parent().data("args").prevSlideObject, 
            this.targetSlideNumber = g + 1, this.targetSlideObject = a(d).children(":eq(" + g + ")"), 
            this.slideChanged = !1, "load" == b ? (this.targetSlideNumber = void 0, this.targetSlideObject = void 0) : "start" == b ? (this.targetSlideNumber = void 0, 
            this.targetSlideObject = void 0) : "change" == b ? (this.slideChanged = !0, this.prevSlideNumber = void 0 == a(d).parent().data("args") ? c.startAtSlide : a(d).parent().data("args").currentSlideNumber, 
            this.prevSlideObject = a(d).children(":eq(" + this.prevSlideNumber + ")")) : "complete" == b && (this.slideChanged = a(d).parent().data("args").slideChanged), 
            this.settings = c, this.data = a(d).parent().data("iosslider"), this.sliderObject = d, 
            this.sliderContainerObject = a(d).parent(), this.currentSlideObject = e, this.currentSlideNumber = f + 1, 
            this.currentSliderOffset = -1 * E.getSliderOffset(d, "x");
        },
        preventDrag: function(a) {
            a.preventDefault();
        },
        preventClick: function(a) {
            return a.stopImmediatePropagation(), !1;
        },
        enableClick: function() {
            return !0;
        }
    };
    E.setBrowserInfo();
    var F = {
        init: function(e, i) {
            j = E.has3DTransform();
            var m = a.extend(!0, {
                elasticPullResistance: .6,
                frictionCoefficient: .92,
                elasticFrictionCoefficient: .6,
                snapFrictionCoefficient: .92,
                snapToChildren: !1,
                snapSlideCenter: !1,
                startAtSlide: 1,
                scrollbar: !1,
                scrollbarDrag: !1,
                scrollbarHide: !0,
                scrollbarPaging: !1,
                scrollbarLocation: "top",
                scrollbarContainer: "",
                scrollbarOpacity: .4,
                scrollbarHeight: "4px",
                scrollbarBorder: "0",
                scrollbarMargin: "5px",
                scrollbarBackground: "#000",
                scrollbarBorderRadius: "100px",
                scrollbarShadow: "0 0 0 #000",
                scrollbarElasticPullResistance: .9,
                desktopClickDrag: !1,
                keyboardControls: !1,
                tabToAdvance: !1,
                responsiveSlideContainer: !0,
                responsiveSlides: !0,
                navSlideSelector: "",
                navPrevSelector: "",
                navNextSelector: "",
                autoSlideToggleSelector: "",
                autoSlide: !1,
                autoSlideTimer: 5e3,
                autoSlideTransTimer: 750,
                autoSlideHoverPause: !0,
                infiniteSlider: !1,
                snapVelocityThreshold: 5,
                slideStartVelocityThreshold: 0,
                horizontalSlideLockThreshold: 5,
                verticalSlideLockThreshold: 3,
                hardwareAccelBuffer: 5,
                stageCSS: {
                    position: "relative",
                    top: "0",
                    left: "0",
                    overflow: "hidden",
                    zIndex: 1
                },
                unselectableSelector: "",
                onSliderLoaded: "",
                onSliderUpdate: "",
                onSliderResize: "",
                onSlideStart: "",
                onSlideChange: "",
                onSlideComplete: ""
            }, e);
            return void 0 == i && (i = this), a(i).each(function(e) {
                function i() {
                    E.autoSlidePause(j), qa = a(ga).find("a"), ra = a(ga).find("[onclick]"), sa = a(ga).find("*"), 
                    a($).css("width", ""), a($).css("height", ""), a(ga).css("width", ""), R = a(ga).children().not("script").get(), 
                    S = new Array(), T = new Array(), m.responsiveSlides && a(R).css("width", ""), B[j] = 0, 
                    Q = new Array(), J = a($).parent().width(), L = a($).outerWidth(!0), m.responsiveSlideContainer && (L = a($).outerWidth(!0) > J ? J : a($).width()), 
                    a($).css({
                        position: m.stageCSS.position,
                        top: m.stageCSS.top,
                        left: m.stageCSS.left,
                        overflow: m.stageCSS.overflow,
                        zIndex: m.stageCSS.zIndex,
                        webkitPerspective: 1e3,
                        webkitBackfaceVisibility: "hidden",
                        msTouchAction: "pan-y",
                        width: L
                    }), a(m.unselectableSelector).css({
                        cursor: "default"
                    });
                    for (var b = 0; b < R.length; b++) {
                        S[b] = a(R[b]).width(), T[b] = a(R[b]).outerWidth(!0);
                        var c = T[b];
                        m.responsiveSlides && (T[b] > L ? (c = L + -1 * (T[b] - S[b]), S[b] = c, T[b] = L) : c = S[b], 
                        a(R[b]).css({
                            width: c
                        })), a(R[b]).css({
                            overflow: "hidden",
                            position: "absolute"
                        }), Q[b] = -1 * B[j], B[j] = B[j] + c + (T[b] - S[b]);
                    }
                    m.snapSlideCenter && (Z = .5 * (L - T[0]), m.responsiveSlides && T[0] > L && (Z = 0)), 
                    C[j] = 2 * B[j];
                    for (var b = 0; b < R.length; b++) E.setSliderOffset(a(R[b]), -1 * Q[b] + B[j] + Z), 
                    Q[b] = Q[b] - B[j];
                    if (!m.infiniteSlider && !m.snapSlideCenter) {
                        for (var d = 0; d < Q.length && !(Q[d] <= -1 * (2 * B[j] - L)); d++) ja = d;
                        Q.splice(ja + 1, Q.length), Q[Q.length] = -1 * (2 * B[j] - L);
                    }
                    for (var d = 0; d < Q.length; d++) ba[d] = Q[d];
                    if (_ && (u[j].startAtSlide = u[j].startAtSlide > Q.length ? Q.length : u[j].startAtSlide, 
                    m.infiniteSlider ? (u[j].startAtSlide = (u[j].startAtSlide - 1 + ha) % ha, x[j] = u[j].startAtSlide) : (u[j].startAtSlide = u[j].startAtSlide - 1 < 0 ? Q.length - 1 : u[j].startAtSlide, 
                    x[j] = u[j].startAtSlide - 1), y[j] = x[j]), A[j] = B[j] + Z, a(ga).css({
                        position: "relative",
                        cursor: p,
                        webkitPerspective: "0",
                        webkitBackfaceVisibility: "hidden",
                        width: B[j] + "px"
                    }), pa = B[j], B[j] = 2 * B[j] - L + 2 * Z, ma = L > pa + Z || 0 == L ? !0 : !1, 
                    ma && a(ga).css({
                        cursor: "default"
                    }), K = a($).parent().outerHeight(!0), M = a($).height(), m.responsiveSlideContainer && (M = M > K ? K : M), 
                    a($).css({
                        height: M
                    }), E.setSliderOffset(ga, Q[x[j]]), m.infiniteSlider && !ma) {
                        for (var e = E.getSliderOffset(a(ga), "x"), f = (z[j] + ha) % ha * -1; 0 > f; ) {
                            var g = 0, h = E.getSliderOffset(a(R[0]), "x");
                            a(R).each(function(a) {
                                E.getSliderOffset(this, "x") < h && (h = E.getSliderOffset(this, "x"), g = a);
                            });
                            var i = A[j] + pa;
                            E.setSliderOffset(a(R)[g], i), A[j] = -1 * Q[1] + Z, B[j] = A[j] + pa - L, Q.splice(0, 1), 
                            Q.splice(Q.length, 0, -1 * i + Z), f++;
                        }
                        for (;-1 * Q[0] - pa + Z > 0 && m.snapSlideCenter && _; ) {
                            var k = 0, l = E.getSliderOffset(a(R[0]), "x");
                            a(R).each(function(a) {
                                E.getSliderOffset(this, "x") > l && (l = E.getSliderOffset(this, "x"), k = a);
                            });
                            var i = A[j] - T[k];
                            E.setSliderOffset(a(R)[k], i), Q.splice(0, 0, -1 * i + Z), Q.splice(Q.length - 1, 1), 
                            A[j] = -1 * Q[0] + Z, B[j] = A[j] + pa - L, z[j]--, x[j]++;
                        }
                        for (;e <= -1 * B[j]; ) {
                            var g = 0, h = E.getSliderOffset(a(R[0]), "x");
                            a(R).each(function(a) {
                                E.getSliderOffset(this, "x") < h && (h = E.getSliderOffset(this, "x"), g = a);
                            });
                            var i = A[j] + pa;
                            E.setSliderOffset(a(R)[g], i), A[j] = -1 * Q[1] + Z, B[j] = A[j] + pa - L, Q.splice(0, 1), 
                            Q.splice(Q.length, 0, -1 * i + Z), z[j]++, x[j]--;
                        }
                    }
                    return E.setSliderOffset(ga, Q[x[j]]), E.updateBackfaceVisibility(R, j, ha, m), 
                    m.desktopClickDrag || a(ga).css({
                        cursor: "default"
                    }), m.scrollbar && (a("." + X).css({
                        margin: m.scrollbarMargin,
                        overflow: "hidden",
                        display: "none"
                    }), a("." + X + " ." + Y).css({
                        border: m.scrollbarBorder
                    }), N = parseInt(a("." + X).css("marginLeft")) + parseInt(a("." + X).css("marginRight")), 
                    O = parseInt(a("." + X + " ." + Y).css("borderLeftWidth"), 10) + parseInt(a("." + X + " ." + Y).css("borderRightWidth"), 10), 
                    H = "" != m.scrollbarContainer ? a(m.scrollbarContainer).width() : L, I = L / pa * (H - N), 
                    m.scrollbarHide || (ca = m.scrollbarOpacity), a("." + X).css({
                        position: "absolute",
                        left: 0,
                        width: H - N + "px",
                        margin: m.scrollbarMargin
                    }), "top" == m.scrollbarLocation ? a("." + X).css("top", "0") : a("." + X).css("bottom", "0"), 
                    a("." + X + " ." + Y).css({
                        borderRadius: m.scrollbarBorderRadius,
                        background: m.scrollbarBackground,
                        height: m.scrollbarHeight,
                        width: I - O + "px",
                        minWidth: m.scrollbarHeight,
                        border: m.scrollbarBorder,
                        webkitPerspective: 1e3,
                        webkitBackfaceVisibility: "hidden",
                        position: "relative",
                        opacity: ca,
                        filter: "alpha(opacity:" + 100 * ca + ")",
                        boxShadow: m.scrollbarShadow
                    }), E.setSliderOffset(a("." + X + " ." + Y), Math.floor((-1 * Q[x[j]] - A[j] + Z) / (B[j] - A[j] + Z) * (H - N - I))), 
                    a("." + X).css({
                        display: "block"
                    }), s = a("." + X + " ." + Y), G = a("." + X)), m.scrollbarDrag && !ma && a("." + X + " ." + Y).css({
                        cursor: p
                    }), m.infiniteSlider && (U = (B[j] + L) / 3), "" != m.navSlideSelector && a(m.navSlideSelector).each(function(b) {
                        a(this).css({
                            cursor: "pointer"
                        }), a(this).unbind(va).bind(va, function(c) {
                            "touchstart" == c.type ? a(this).unbind("click.iosSliderEvent") : a(this).unbind("touchstart.iosSliderEvent"), 
                            va = c.type + ".iosSliderEvent", E.changeSlide(b, ga, R, o, Y, I, L, H, N, O, ba, Q, T, j, U, ha, Z, m);
                        });
                    }), "" != m.navPrevSelector && (a(m.navPrevSelector).css({
                        cursor: "pointer"
                    }), a(m.navPrevSelector).unbind(va).bind(va, function(b) {
                        "touchstart" == b.type ? a(this).unbind("click.iosSliderEvent") : a(this).unbind("touchstart.iosSliderEvent"), 
                        va = b.type + ".iosSliderEvent";
                        var c = (x[j] + z[j] + ha) % ha;
                        (c > 0 || m.infiniteSlider) && E.changeSlide(c - 1, ga, R, o, Y, I, L, H, N, O, ba, Q, T, j, U, ha, Z, m);
                    })), "" != m.navNextSelector && (a(m.navNextSelector).css({
                        cursor: "pointer"
                    }), a(m.navNextSelector).unbind(va).bind(va, function(b) {
                        "touchstart" == b.type ? a(this).unbind("click.iosSliderEvent") : a(this).unbind("touchstart.iosSliderEvent"), 
                        va = b.type + ".iosSliderEvent";
                        var c = (x[j] + z[j] + ha) % ha;
                        (c < Q.length - 1 || m.infiniteSlider) && E.changeSlide(c + 1, ga, R, o, Y, I, L, H, N, O, ba, Q, T, j, U, ha, Z, m);
                    })), m.autoSlide && !ma && "" != m.autoSlideToggleSelector && (a(m.autoSlideToggleSelector).css({
                        cursor: "pointer"
                    }), a(m.autoSlideToggleSelector).unbind(va).bind(va, function(b) {
                        "touchstart" == b.type ? a(this).unbind("click.iosSliderEvent") : a(this).unbind("touchstart.iosSliderEvent"), 
                        va = b.type + ".iosSliderEvent", na ? (E.autoSlide(ga, R, o, Y, I, L, H, N, O, ba, Q, T, j, U, ha, Z, m), 
                        na = !1, a(m.autoSlideToggleSelector).removeClass("on")) : (E.autoSlidePause(j), 
                        na = !0, a(m.autoSlideToggleSelector).addClass("on"));
                    })), E.autoSlide(ga, R, o, Y, I, L, H, N, O, ba, Q, T, j, U, ha, Z, m), a($).bind("mouseleave.iosSliderEvent", function() {
                        return na ? !0 : void E.autoSlide(ga, R, o, Y, I, L, H, N, O, ba, Q, T, j, U, ha, Z, m);
                    }), a($).bind("touchend.iosSliderEvent", function() {
                        return na ? !0 : void E.autoSlide(ga, R, o, Y, I, L, H, N, O, ba, Q, T, j, U, ha, Z, m);
                    }), m.autoSlideHoverPause && a($).bind("mouseenter.iosSliderEvent", function() {
                        E.autoSlidePause(j);
                    }), a($).data("iosslider", {
                        obj: wa,
                        settings: m,
                        scrollerNode: ga,
                        slideNodes: R,
                        numberOfSlides: ha,
                        centeredSlideOffset: Z,
                        sliderNumber: j,
                        originalOffsets: ba,
                        childrenOffsets: Q,
                        sliderMax: B[j],
                        scrollbarClass: Y,
                        scrollbarWidth: I,
                        scrollbarStageWidth: H,
                        stageWidth: L,
                        scrollMargin: N,
                        scrollBorder: O,
                        infiniteSliderOffset: z[j],
                        infiniteSliderWidth: U,
                        slideNodeOuterWidths: T,
                        shortContent: ma
                    }), _ = !1, !0;
                }
                b++;
                var j = b, o = new Array();
                u[j] = a.extend({}, m), A[j] = 0, B[j] = 0;
                var s, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V = new Array(0, 0), W = new Array(0, 0), X = "scrollbarBlock" + b, Y = "scrollbar" + b, Z = 0, $ = a(this), _ = !0, aa = -1, ba = (new Array(), 
                new Array()), ca = 0, da = 0, ea = 0, fa = 0, ga = a(this).children(":first-child"), ha = a(ga).children().not("script").length, ia = !1, ja = 0, ka = !1, la = void 0;
                z[j] = 0;
                var ma = !1;
                r[j] = -1;
                var na = !1;
                t[j] = $, v[j] = !1;
                var oa, pa, qa, ra, sa, ta = !1, ua = !1, va = "touchstart.iosSliderEvent click.iosSliderEvent";
                D[j] = !1, w[j] = new Array(), m.scrollbarDrag && (m.scrollbar = !0, m.scrollbarHide = !1);
                var wa = a(this), xa = wa.data("iosslider");
                if (void 0 != xa) return !0;
                if (parseInt(a().jquery.split(".").join(""), 10) >= 14.2 ? a(this).delegate("img", "dragstart.iosSliderEvent", function(a) {
                    a.preventDefault();
                }) : a(this).find("img").bind("dragstart.iosSliderEvent", function(a) {
                    a.preventDefault();
                }), m.infiniteSlider && (m.scrollbar = !1), m.infiniteSlider && 1 == ha && (m.infiniteSlider = !1), 
                m.scrollbar && ("" != m.scrollbarContainer ? a(m.scrollbarContainer).append("<div class = '" + X + "'><div class = '" + Y + "'></div></div>") : a(ga).parent().append("<div class = '" + X + "'><div class = '" + Y + "'></div></div>")), 
                !i()) return !0;
                a(this).find("a").bind("mousedown", E.preventDrag), a(this).find("[onclick]").bind("click", E.preventDrag).each(function() {
                    a(this).data("onclick", this.onclick);
                });
                var aa = E.calcActiveOffset(m, E.getSliderOffset(a(ga), "x"), Q, L, z[j], ha, void 0, j), ya = (aa + z[j] + ha) % ha, za = new E.args("load", m, ga, a(ga).children(":eq(" + ya + ")"), ya, ya);
                if (a($).data("args", za), "" != m.onSliderLoaded && m.onSliderLoaded(za), r[j] = ya, 
                m.scrollbarPaging && m.scrollbar && !ma && (a(G).css("cursor", "pointer"), a(G).bind("click.iosSliderEvent", function(b) {
                    this == b.target && (b.pageX > a(s).offset().left ? F.nextPage($) : F.prevPage($));
                })), u[j].responsiveSlides || u[j].responsiveSlideContainer) {
                    var Aa = h ? "orientationchange" : "resize";
                    a(window).bind(Aa + ".iosSliderEvent-" + j, function() {
                        if (!i()) return !0;
                        var b = a($).data("args");
                        "" != m.onSliderResize && m.onSliderResize(b);
                    });
                }
                if (!m.keyboardControls && !m.tabToAdvance || ma || a(document).bind("keydown.iosSliderEvent", function(a) {
                    if (!k && !l) var a = a.originalEvent;
                    if ("INPUT" == a.target.nodeName) return !0;
                    if (D[j]) return !0;
                    if (37 == a.keyCode && m.keyboardControls) {
                        a.preventDefault();
                        var b = (x[j] + z[j] + ha) % ha;
                        (b > 0 || m.infiniteSlider) && E.changeSlide(b - 1, ga, R, o, Y, I, L, H, N, O, ba, Q, T, j, U, ha, Z, m);
                    } else if (39 == a.keyCode && m.keyboardControls || 9 == a.keyCode && m.tabToAdvance) {
                        a.preventDefault();
                        var b = (x[j] + z[j] + ha) % ha;
                        (b < Q.length - 1 || m.infiniteSlider) && E.changeSlide(b + 1, ga, R, o, Y, I, L, H, N, O, ba, Q, T, j, U, ha, Z, m);
                    }
                }), g || m.desktopClickDrag) {
                    var Ba = !1, Ca = !1, Da = a(ga), Ea = a(ga), Fa = !1;
                    m.scrollbarDrag && (Da = Da.add(s), Ea = Ea.add(G)), a(Da).bind("mousedown.iosSliderEvent touchstart.iosSliderEvent", function(b) {
                        if (a(window).one("scroll.iosSliderEvent", function(a) {
                            Ba = !1;
                        }), Ba) return !0;
                        if (Ba = !0, Ca = !1, "touchstart" == b.type ? a(Ea).unbind("mousedown.iosSliderEvent") : a(Ea).unbind("touchstart.iosSliderEvent"), 
                        D[j] || ma) return Ba = !1, ia = !1, !0;
                        if (Fa = E.isUnselectable(b.target, m)) return Ba = !1, ia = !1, !0;
                        if (oa = a(this)[0] === a(s)[0] ? s : ga, !k && !l) var b = b.originalEvent;
                        if (E.autoSlidePause(j), sa.unbind(".disableClick"), "touchstart" == b.type) eventX = b.touches[0].pageX, 
                        eventY = b.touches[0].pageY; else {
                            if (window.getSelection) window.getSelection().empty ? window.getSelection().empty() : window.getSelection().removeAllRanges && window.getSelection().removeAllRanges(); else if (document.selection) if (l) try {
                                document.selection.empty();
                            } catch (b) {} else document.selection.empty();
                            eventX = b.pageX, eventY = b.pageY, ka = !0, la = ga, a(this).css({
                                cursor: q
                            });
                        }
                        V = new Array(0, 0), W = new Array(0, 0), c = 0, ia = !1;
                        for (var d = 0; d < o.length; d++) clearTimeout(o[d]);
                        var e = E.getSliderOffset(ga, "x");
                        e > -1 * A[j] + Z + pa ? (e = -1 * A[j] + Z + pa, E.setSliderOffset(a("." + Y), e), 
                        a("." + Y).css({
                            width: I - O + "px"
                        })) : e < -1 * B[j] && (e = -1 * B[j], E.setSliderOffset(a("." + Y), H - N - I), 
                        a("." + Y).css({
                            width: I - O + "px"
                        }));
                        var f = a(this)[0] === a(s)[0] ? A[j] : 0;
                        da = -1 * (E.getSliderOffset(this, "x") - eventX - f), ea = -1 * (E.getSliderOffset(this, "y") - eventY), 
                        V[1] = eventX, W[1] = eventY, ua = !1;
                    }), a(document).bind("touchmove.iosSliderEvent mousemove.iosSliderEvent", function(b) {
                        if (!k && !l) var b = b.originalEvent;
                        if (D[j] || ma || Fa || !Ba) return !0;
                        var e = 0;
                        if ("touchmove" == b.type) eventX = b.touches[0].pageX, eventY = b.touches[0].pageY; else {
                            if (window.getSelection) window.getSelection().empty || window.getSelection().removeAllRanges && window.getSelection().removeAllRanges(); else if (document.selection) if (l) try {
                                document.selection.empty();
                            } catch (b) {} else document.selection.empty();
                            if (eventX = b.pageX, eventY = b.pageY, !ka) return !0;
                            if (!n && ("undefined" != typeof b.webkitMovementX || "undefined" != typeof b.webkitMovementY) && 0 === b.webkitMovementY && 0 === b.webkitMovementX) return !0;
                        }
                        if (V[0] = V[1], V[1] = eventX, c = (V[1] - V[0]) / 2, W[0] = W[1], W[1] = eventY, 
                        d = (W[1] - W[0]) / 2, !ia) {
                            var g = (x[j] + z[j] + ha) % ha, h = new E.args("start", m, ga, a(ga).children(":eq(" + g + ")"), g, void 0);
                            a($).data("args", h), "" != m.onSlideStart && m.onSlideStart(h);
                        }
                        if ((d > m.verticalSlideLockThreshold || d < -1 * m.verticalSlideLockThreshold) && "touchmove" == b.type && !ia && (ta = !0), 
                        (c > m.horizontalSlideLockThreshold || c < -1 * m.horizontalSlideLockThreshold) && "touchmove" == b.type && b.preventDefault(), 
                        (c > m.slideStartVelocityThreshold || c < -1 * m.slideStartVelocityThreshold) && (ia = !0), 
                        ia && !ta) {
                            var i = E.getSliderOffset(ga, "x"), o = a(oa)[0] === a(s)[0] ? A[j] : Z, p = a(oa)[0] === a(s)[0] ? (A[j] - B[j] - Z) / (H - N - I) : 1, q = a(oa)[0] === a(s)[0] ? m.scrollbarElasticPullResistance : m.elasticPullResistance, r = m.snapSlideCenter && a(oa)[0] === a(s)[0] ? 0 : Z, t = m.snapSlideCenter && a(oa)[0] === a(s)[0] ? Z : 0;
                            if ("touchmove" == b.type && (fa != b.touches.length && (da = -1 * i + eventX), 
                            fa = b.touches.length), m.infiniteSlider) {
                                if (i <= -1 * B[j]) {
                                    var u = a(ga).width();
                                    if (i <= -1 * C[j]) {
                                        var v = -1 * ba[0];
                                        a(R).each(function(b) {
                                            E.setSliderOffset(a(R)[b], v + Z), b < Q.length && (Q[b] = -1 * v), v += T[b];
                                        }), da -= -1 * Q[0], A[j] = -1 * Q[0] + Z, B[j] = A[j] + u - L, z[j] = 0;
                                    } else {
                                        var w = 0, F = E.getSliderOffset(a(R[0]), "x");
                                        a(R).each(function(a) {
                                            E.getSliderOffset(this, "x") < F && (F = E.getSliderOffset(this, "x"), w = a);
                                        });
                                        var G = A[j] + u;
                                        E.setSliderOffset(a(R)[w], G), A[j] = -1 * Q[1] + Z, B[j] = A[j] + u - L, Q.splice(0, 1), 
                                        Q.splice(Q.length, 0, -1 * G + Z), z[j]++;
                                    }
                                }
                                if (i >= -1 * A[j] || i >= 0) {
                                    var u = a(ga).width();
                                    if (i >= 0) {
                                        var v = -1 * ba[0];
                                        for (a(R).each(function(b) {
                                            E.setSliderOffset(a(R)[b], v + Z), b < Q.length && (Q[b] = -1 * v), v += T[b];
                                        }), da += -1 * Q[0], A[j] = -1 * Q[0] + Z, B[j] = A[j] + u - L, z[j] = ha; -1 * Q[0] - u + Z > 0; ) {
                                            var J = 0, K = E.getSliderOffset(a(R[0]), "x");
                                            a(R).each(function(a) {
                                                E.getSliderOffset(this, "x") > K && (K = E.getSliderOffset(this, "x"), J = a);
                                            });
                                            var G = A[j] - T[J];
                                            E.setSliderOffset(a(R)[J], G), Q.splice(0, 0, -1 * G + Z), Q.splice(Q.length - 1, 1), 
                                            A[j] = -1 * Q[0] + Z, B[j] = A[j] + u - L, z[j]--, x[j]++;
                                        }
                                    } else {
                                        var J = 0, K = E.getSliderOffset(a(R[0]), "x");
                                        a(R).each(function(a) {
                                            E.getSliderOffset(this, "x") > K && (K = E.getSliderOffset(this, "x"), J = a);
                                        });
                                        var G = A[j] - T[J];
                                        E.setSliderOffset(a(R)[J], G), Q.splice(0, 0, -1 * G + Z), Q.splice(Q.length - 1, 1), 
                                        A[j] = -1 * Q[0] + Z, B[j] = A[j] + u - L, z[j]--;
                                    }
                                }
                            } else {
                                var u = a(ga).width();
                                i > -1 * A[j] + Z && (e = (A[j] + -1 * (da - o - eventX + r) * p - o) * q * -1 / p), 
                                i < -1 * B[j] && (e = (B[j] + t + -1 * (da - o - eventX) * p - o) * q * -1 / p);
                            }
                            if (E.setSliderOffset(ga, -1 * (da - o - eventX - e) * p - o + t), m.scrollbar) {
                                E.showScrollbar(m, Y), f = Math.floor((da - eventX - e - A[j] + r) / (B[j] - A[j] + Z) * (H - N - I) * p);
                                var M = I;
                                0 >= f ? (M = I - O - -1 * f, E.setSliderOffset(a("." + Y), 0), a("." + Y).css({
                                    width: M + "px"
                                })) : f >= H - N - O - I ? (M = H - N - O - f, E.setSliderOffset(a("." + Y), f), 
                                a("." + Y).css({
                                    width: M + "px"
                                })) : E.setSliderOffset(a("." + Y), f);
                            }
                            "touchmove" == b.type && (P = b.touches[0].pageX);
                            var S = !1, U = E.calcActiveOffset(m, -1 * (da - eventX - e), Q, L, z[j], ha, void 0, j), X = (U + z[j] + ha) % ha;
                            if (m.infiniteSlider ? X != y[j] && (S = !0) : U != x[j] && (S = !0), S) {
                                x[j] = U, y[j] = X, ua = !0;
                                var h = new E.args("change", m, ga, a(ga).children(":eq(" + X + ")"), X, X);
                                a($).data("args", h), "" != m.onSlideChange && m.onSlideChange(h), E.updateBackfaceVisibility(R, j, ha, m);
                            }
                        }
                    });
                    var Ga = a(window);
                    if (l || k) var Ga = a(document);
                    a(Da).bind("touchcancel.iosSliderEvent touchend.iosSliderEvent", function(a) {
                        var a = a.originalEvent;
                        if (Ca) return !1;
                        if (Ca = !0, D[j] || ma) return !0;
                        if (Fa) return !0;
                        if (0 != a.touches.length) for (var b = 0; b < a.touches.length; b++) a.touches[b].pageX == P && E.slowScrollHorizontal(ga, R, o, Y, c, d, I, L, H, N, O, ba, Q, T, j, U, ha, oa, ua, Z, m); else E.slowScrollHorizontal(ga, R, o, Y, c, d, I, L, H, N, O, ba, Q, T, j, U, ha, oa, ua, Z, m);
                        return ta = !1, Ba = !1, !0;
                    }), a(Ga).bind("mouseup.iosSliderEvent-" + j, function(b) {
                        if (ia ? qa.unbind("click.disableClick").bind("click.disableClick", E.preventClick) : qa.unbind("click.disableClick").bind("click.disableClick", E.enableClick), 
                        ra.each(function() {
                            this.onclick = function(b) {
                                return ia ? !1 : void (a(this).data("onclick") && a(this).data("onclick").call(this, b || window.event));
                            }, this.onclick = a(this).data("onclick");
                        }), parseFloat(a().jquery) >= 1.8 ? sa.each(function() {
                            var b = a._data(this, "events");
                            if (void 0 != b && void 0 != b.click && "iosSliderEvent" != b.click[0].namespace) {
                                if (!ia) return !1;
                                a(this).one("click.disableClick", E.preventClick);
                                var c = a._data(this, "events").click, d = c.pop();
                                c.splice(0, 0, d);
                            }
                        }) : parseFloat(a().jquery) >= 1.6 && sa.each(function() {
                            var b = a(this).data("events");
                            if (void 0 != b && void 0 != b.click && "iosSliderEvent" != b.click[0].namespace) {
                                if (!ia) return !1;
                                a(this).one("click.disableClick", E.preventClick);
                                var c = a(this).data("events").click, d = c.pop();
                                c.splice(0, 0, d);
                            }
                        }), !v[j]) {
                            if (ma) return !0;
                            if (m.desktopClickDrag && a(ga).css({
                                cursor: p
                            }), m.scrollbarDrag && a(s).css({
                                cursor: p
                            }), ka = !1, void 0 == la) return !0;
                            E.slowScrollHorizontal(la, R, o, Y, c, d, I, L, H, N, O, ba, Q, T, j, U, ha, oa, ua, Z, m), 
                            la = void 0;
                        }
                        ta = !1, Ba = !1;
                    });
                }
            });
        },
        destroy: function(b, c) {
            return void 0 == c && (c = this), a(c).each(function() {
                var c = a(this), d = c.data("iosslider");
                if (void 0 == d) return !1;
                void 0 == b && (b = !0), E.autoSlidePause(d.sliderNumber), v[d.sliderNumber] = !0, 
                a(window).unbind(".iosSliderEvent-" + d.sliderNumber), a(document).unbind(".iosSliderEvent-" + d.sliderNumber), 
                a(document).unbind("keydown.iosSliderEvent"), a(this).unbind(".iosSliderEvent"), 
                a(this).children(":first-child").unbind(".iosSliderEvent"), a(this).children(":first-child").children().unbind(".iosSliderEvent"), 
                a(d.settings.scrollbarBlockNode).unbind(".iosSliderEvent"), b && (a(this).attr("style", ""), 
                a(this).children(":first-child").attr("style", ""), a(this).children(":first-child").children().attr("style", ""), 
                a(d.settings.navSlideSelector).attr("style", ""), a(d.settings.navPrevSelector).attr("style", ""), 
                a(d.settings.navNextSelector).attr("style", ""), a(d.settings.autoSlideToggleSelector).attr("style", ""), 
                a(d.settings.unselectableSelector).attr("style", "")), d.settings.scrollbar && a(".scrollbarBlock" + d.sliderNumber).remove();
                for (var e = w[d.sliderNumber], f = 0; f < e.length; f++) clearTimeout(e[f]);
                c.removeData("iosslider"), c.removeData("args");
            });
        },
        update: function(b) {
            return void 0 == b && (b = this), a(b).each(function() {
                var b = a(this), c = b.data("iosslider");
                if (void 0 == c) return !1;
                c.settings.startAtSlide = b.data("args").currentSlideNumber, F.destroy(!1, this), 
                1 != c.numberOfSlides && c.settings.infiniteSlider && (c.settings.startAtSlide = (x[c.sliderNumber] + 1 + z[c.sliderNumber] + c.numberOfSlides) % c.numberOfSlides), 
                F.init(c.settings, this);
                var d = new E.args("update", c.settings, c.scrollerNode, a(c.scrollerNode).children(":eq(" + (c.settings.startAtSlide - 1) + ")"), c.settings.startAtSlide - 1, c.settings.startAtSlide - 1);
                a(c.stageNode).data("args", d), "" != c.settings.onSliderUpdate && c.settings.onSliderUpdate(d);
            });
        },
        addSlide: function(b, c) {
            return this.each(function() {
                var d = a(this), e = d.data("iosslider");
                return void 0 == e ? !1 : (0 == a(e.scrollerNode).children().length ? (a(e.scrollerNode).append(b), 
                d.data("args").currentSlideNumber = 1) : e.settings.infiniteSlider ? (1 == c ? a(e.scrollerNode).children(":eq(0)").before(b) : a(e.scrollerNode).children(":eq(" + (c - 2) + ")").after(b), 
                z[e.sliderNumber] < -1 && x[e.sliderNumber]--, d.data("args").currentSlideNumber >= c && x[e.sliderNumber]++) : (c <= e.numberOfSlides ? a(e.scrollerNode).children(":eq(" + (c - 1) + ")").before(b) : a(e.scrollerNode).children(":eq(" + (c - 2) + ")").after(b), 
                d.data("args").currentSlideNumber >= c && d.data("args").currentSlideNumber++), 
                d.data("iosslider").numberOfSlides++, void F.update(this));
            });
        },
        removeSlide: function(b) {
            return this.each(function() {
                var c = a(this), d = c.data("iosslider");
                return void 0 == d ? !1 : (a(d.scrollerNode).children(":eq(" + (b - 1) + ")").remove(), 
                x[d.sliderNumber] > b - 1 && x[d.sliderNumber]--, c.data("iosslider").numberOfSlides--, 
                void F.update(this));
            });
        },
        goToSlide: function(b, c, d) {
            return void 0 == d && (d = this), a(d).each(function() {
                var d = a(this), e = d.data("iosslider");
                return void 0 == e || e.shortContent ? !1 : (b = b > e.childrenOffsets.length ? e.childrenOffsets.length - 1 : b - 1, 
                void 0 != c && (e.settings.autoSlideTransTimer = c), void E.changeSlide(b, a(e.scrollerNode), a(e.slideNodes), w[e.sliderNumber], e.scrollbarClass, e.scrollbarWidth, e.stageWidth, e.scrollbarStageWidth, e.scrollMargin, e.scrollBorder, e.originalOffsets, e.childrenOffsets, e.slideNodeOuterWidths, e.sliderNumber, e.infiniteSliderWidth, e.numberOfSlides, e.centeredSlideOffset, e.settings));
            });
        },
        prevSlide: function(b) {
            return this.each(function() {
                var c = a(this), d = c.data("iosslider");
                if (void 0 == d || d.shortContent) return !1;
                var e = (x[d.sliderNumber] + z[d.sliderNumber] + d.numberOfSlides) % d.numberOfSlides;
                void 0 != b && (d.settings.autoSlideTransTimer = b), (e > 0 || d.settings.infiniteSlider) && E.changeSlide(e - 1, a(d.scrollerNode), a(d.slideNodes), w[d.sliderNumber], d.scrollbarClass, d.scrollbarWidth, d.stageWidth, d.scrollbarStageWidth, d.scrollMargin, d.scrollBorder, d.originalOffsets, d.childrenOffsets, d.slideNodeOuterWidths, d.sliderNumber, d.infiniteSliderWidth, d.numberOfSlides, d.centeredSlideOffset, d.settings), 
                x[d.sliderNumber] = e;
            });
        },
        nextSlide: function(b) {
            return this.each(function() {
                var c = a(this), d = c.data("iosslider");
                if (void 0 == d || d.shortContent) return !1;
                var e = (x[d.sliderNumber] + z[d.sliderNumber] + d.numberOfSlides) % d.numberOfSlides;
                void 0 != b && (d.settings.autoSlideTransTimer = b), (e < d.childrenOffsets.length - 1 || d.settings.infiniteSlider) && E.changeSlide(e + 1, a(d.scrollerNode), a(d.slideNodes), w[d.sliderNumber], d.scrollbarClass, d.scrollbarWidth, d.stageWidth, d.scrollbarStageWidth, d.scrollMargin, d.scrollBorder, d.originalOffsets, d.childrenOffsets, d.slideNodeOuterWidths, d.sliderNumber, d.infiniteSliderWidth, d.numberOfSlides, d.centeredSlideOffset, d.settings), 
                x[d.sliderNumber] = e;
            });
        },
        prevPage: function(b, c) {
            return void 0 == c && (c = this), a(c).each(function() {
                var c = a(this), d = c.data("iosslider");
                if (void 0 == d) return !1;
                var e = E.getSliderOffset(d.scrollerNode, "x") + d.stageWidth;
                void 0 != b && (d.settings.autoSlideTransTimer = b), E.changeOffset(e, a(d.scrollerNode), a(d.slideNodes), w[d.sliderNumber], d.scrollbarClass, d.scrollbarWidth, d.stageWidth, d.scrollbarStageWidth, d.scrollMargin, d.scrollBorder, d.originalOffsets, d.childrenOffsets, d.slideNodeOuterWidths, d.sliderNumber, d.infiniteSliderWidth, d.numberOfSlides, d.centeredSlideOffset, d.settings);
            });
        },
        nextPage: function(b, c) {
            return void 0 == c && (c = this), a(c).each(function() {
                var c = a(this), d = c.data("iosslider");
                if (void 0 == d) return !1;
                var e = E.getSliderOffset(d.scrollerNode, "x") - d.stageWidth;
                void 0 != b && (d.settings.autoSlideTransTimer = b), E.changeOffset(e, a(d.scrollerNode), a(d.slideNodes), w[d.sliderNumber], d.scrollbarClass, d.scrollbarWidth, d.stageWidth, d.scrollbarStageWidth, d.scrollMargin, d.scrollBorder, d.originalOffsets, d.childrenOffsets, d.slideNodeOuterWidths, d.sliderNumber, d.infiniteSliderWidth, d.numberOfSlides, d.centeredSlideOffset, d.settings);
            });
        },
        lock: function() {
            return this.each(function() {
                var b = a(this), c = b.data("iosslider");
                return void 0 == c || c.shortContent ? !1 : (a(c.scrollerNode).css({
                    cursor: "default"
                }), void (D[c.sliderNumber] = !0));
            });
        },
        unlock: function() {
            return this.each(function() {
                var b = a(this), c = b.data("iosslider");
                return void 0 == c || c.shortContent ? !1 : (a(c.scrollerNode).css({
                    cursor: p
                }), void (D[c.sliderNumber] = !1));
            });
        },
        getData: function() {
            return this.each(function() {
                var b = a(this), c = b.data("iosslider");
                return void 0 == c || c.shortContent ? !1 : c;
            });
        },
        autoSlidePause: function() {
            return this.each(function() {
                var b = a(this), c = b.data("iosslider");
                return void 0 == c || c.shortContent ? !1 : (u[c.sliderNumber].autoSlide = !1, E.autoSlidePause(c.sliderNumber), 
                c);
            });
        },
        autoSlidePlay: function() {
            return this.each(function() {
                var b = a(this), c = b.data("iosslider");
                return void 0 == c || c.shortContent ? !1 : (u[c.sliderNumber].autoSlide = !0, E.autoSlide(a(c.scrollerNode), a(c.slideNodes), w[c.sliderNumber], c.scrollbarClass, c.scrollbarWidth, c.stageWidth, c.scrollbarStageWidth, c.scrollMargin, c.scrollBorder, c.originalOffsets, c.childrenOffsets, c.slideNodeOuterWidths, c.sliderNumber, c.infiniteSliderWidth, c.numberOfSlides, c.centeredSlideOffset, c.settings), 
                c);
            });
        }
    };
    a.fn.iosSlider = function(b) {
        return F[b] ? F[b].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof b && b ? void a.error("invalid method call!") : F.init.apply(this, arguments);
    };
}(jQuery), function(a) {
    a.fn.fastClick = function(b) {
        return a(this).each(function() {
            a.FastButton(a(this)[0], b);
        });
    }, a.fn.unfastClick = function(b) {
        return a(this).each(function() {
            a(this).trigger("removeFastClick");
        });
    }, a.FastButton = function(b, c) {
        var d, e, f = function() {
            a(b).unbind("touchend"), a("body").unbind("touchmove.fastClick");
        }, g = function(b) {
            b.stopPropagation(), f(), c.call(this, b), "touchend" === b.type && a.clickbuster.preventGhostClick(d, e);
        }, h = function(a) {
            (Math.abs(a.originalEvent.touches[0].clientX - d) > 10 || Math.abs(a.originalEvent.touches[0].clientY - e) > 10) && f();
        }, i = function(c) {
            a(b).unbind({
                touchstart: j,
                click: g,
                removeFastClick: i
            });
        }, j = function(c) {
            c.stopPropagation(), a(b).bind("touchend", g), a("body").bind("touchmove.fastClick", h), 
            d = c.originalEvent.touches[0].clientX, e = c.originalEvent.touches[0].clientY;
        };
        a(b).bind({
            touchstart: j,
            click: g,
            removeFastClick: i
        });
    }, a.clickbuster = {
        coordinates: [],
        preventGhostClick: function(b, c) {
            a.clickbuster.coordinates.push(b, c), window.setTimeout(a.clickbuster.pop, 2500);
        },
        pop: function() {
            a.clickbuster.coordinates.splice(0, 2);
        },
        onClick: function(b) {
            var c, d, e;
            for (e = 0; e < a.clickbuster.coordinates.length; e += 2) c = a.clickbuster.coordinates[e], 
            d = a.clickbuster.coordinates[e + 1], Math.abs(b.clientX - c) < 25 && Math.abs(b.clientY - d) < 25 && (b.stopPropagation(), 
            b.preventDefault());
        }
    }, a(function() {
        document.addEventListener ? document.addEventListener("click", a.clickbuster.onClick, !0) : document.attachEvent && document.attachEvent("onclick", a.clickbuster.onClick);
    });
}(jQuery), showLog = !1, css_browser_selector(navigator.userAgent), function(a) {
    a.browserTest = function(b, c) {
        var d = "unknown", e = "X", f = function(a, b) {
            for (var c = 0; c < b.length; c += 1) a = a.replace(b[c][0], b[c][1]);
            return a;
        }, g = function(b, c, g, h) {
            var i = {
                name: f((c.exec(b) || [ d, d ])[1], g)
            };
            return i[i.name] = !0, i.version = (h.exec(b) || [ e, e, e, e ])[3], i.name.match(/safari/) && i.version > 400 && (i.version = "2.0"), 
            "presto" === i.name && (i.version = a.browser.version > 9.27 ? "futhark" : "linear_b"), 
            i.versionNumber = parseFloat(i.version, 10) || 0, i.versionX = i.version !== e ? (i.version + "").substr(0, 1) : e, 
            i.className = i.name + i.versionX, i;
        };
        b = (b.match(/Opera|Navigator|Minefield|KHTML|Chrome/) ? f(b, [ [ /(Firefox|MSIE|KHTML,\slike\sGecko|Konqueror)/, "" ], [ "Chrome Safari", "Chrome" ], [ "KHTML", "Konqueror" ], [ "Minefield", "Firefox" ], [ "Navigator", "Netscape" ] ]) : b).toLowerCase(), 
        a.browser = a.extend(c ? {} : a.browser, g(b, /(camino|chrome|firefox|netscape|konqueror|lynx|msie|opera|safari)/, [], /(camino|chrome|firefox|netscape|netscape6|opera|version|konqueror|lynx|msie|safari)(\/|\s)([a-z0-9\.\+]*?)(\;|dev|rel|\s|$)/)), 
        a.layout = g(b, /(gecko|konqueror|msie|opera|webkit)/, [ [ "konqueror", "khtml" ], [ "msie", "trident" ], [ "opera", "presto" ] ], /(applewebkit|rv|konqueror|msie)(\:|\/|\s)([a-z0-9\.]*?)(\;|\)|\s)/), 
        a.os = {
            name: (/(win|mac|linux|sunos|solaris|iphone)/.exec(navigator.platform.toLowerCase()) || [ d ])[0].replace("sunos", "solaris")
        }, c || a("html").addClass([ a.os.name, a.browser.name, a.browser.className, a.layout.name, a.layout.className ].join(" "));
    }, a.browserTest(navigator.userAgent);
}(jQuery), function(a) {
    "use strict";
    "function" == typeof define && define.amd ? define([ "jquery" ], a) : a("object" == typeof exports && "function" == typeof require ? require("jquery") : jQuery);
}(function(a) {
    "use strict";
    function b(c, d) {
        var e = function() {}, f = this, g = {
            ajaxSettings: {},
            autoSelectFirst: !1,
            appendTo: document.body,
            serviceUrl: null,
            lookup: null,
            onSelect: null,
            width: "auto",
            minChars: 1,
            maxHeight: 300,
            deferRequestBy: 0,
            params: {},
            formatResult: b.formatResult,
            delimiter: null,
            zIndex: 9999,
            type: "GET",
            noCache: !1,
            onSearchStart: e,
            onSearchComplete: e,
            onSearchError: e,
            preserveInput: !1,
            containerClass: "autocomplete-suggestions",
            tabDisabled: !1,
            dataType: "text",
            currentRequest: null,
            triggerSelectOnValidInput: !0,
            preventBadQueries: !0,
            lookupFilter: function(a, b, c) {
                return -1 !== a.value.toLowerCase().indexOf(c);
            },
            paramName: "query",
            transformResult: function(b) {
                return "string" == typeof b ? a.parseJSON(b) : b;
            },
            showNoSuggestionNotice: !1,
            noSuggestionNotice: "No results",
            orientation: "bottom",
            forceFixPosition: !1
        };
        f.element = c, f.el = a(c), f.suggestions = [], f.badQueries = [], f.selectedIndex = -1, 
        f.currentValue = f.element.value, f.intervalId = 0, f.cachedResponse = {}, f.onChangeInterval = null, 
        f.onChange = null, f.isLocal = !1, f.suggestionsContainer = null, f.noSuggestionsContainer = null, 
        f.options = a.extend({}, g, d), f.classes = {
            selected: "autocomplete-selected",
            suggestion: "autocomplete-suggestion"
        }, f.hint = null, f.hintValue = "", f.selection = null, f.initialize(), f.setOptions(d);
    }
    var c = function() {
        return {
            escapeRegExChars: function(a) {
                return a.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
            },
            createNode: function(a) {
                var b = document.createElement("div");
                return b.className = a, b.style.position = "absolute", b.style.display = "none", 
                b;
            }
        };
    }(), d = {
        ESC: 27,
        TAB: 9,
        RETURN: 13,
        LEFT: 37,
        UP: 38,
        RIGHT: 39,
        DOWN: 40
    };
    b.utils = c, a.Autocomplete = b, b.formatResult = function(a, b) {
        var d = a.value.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;"), e = "(" + c.escapeRegExChars(b) + ")";
        return d.replace(new RegExp(e, "gi"), "<strong>$1</strong>");
    }, b.prototype = {
        killerFn: null,
        initialize: function() {
            var c, d = this, e = "." + d.classes.suggestion, f = d.classes.selected, g = d.options;
            d.element.setAttribute("autocomplete", "off"), d.killerFn = function(b) {
                0 === a(b.target).closest("." + d.options.containerClass).length && (d.killSuggestions(), 
                d.disableKillerFn());
            }, d.noSuggestionsContainer = a('<div class="autocomplete-no-suggestion"></div>').html(this.options.noSuggestionNotice).get(0), 
            d.suggestionsContainer = b.utils.createNode(g.containerClass), c = a(d.suggestionsContainer), 
            c.appendTo(g.appendTo), "auto" !== g.width && c.width(g.width), c.on("mouseover.autocomplete", e, function() {
                d.activate(a(this).data("index"));
            }), c.on("mouseout.autocomplete", function() {
                d.selectedIndex = -1, c.children("." + f).removeClass(f);
            }), c.on("click.autocomplete", e, function() {
                d.select(a(this).data("index"));
            }), d.fixPositionCapture = function() {
                d.visible && d.fixPosition();
            }, a(window).on("resize.autocomplete", d.fixPositionCapture), d.el.on("keydown.autocomplete", function(a) {
                d.onKeyPress(a);
            }), d.el.on("keyup.autocomplete", function(a) {
                d.onKeyUp(a);
            }), d.el.on("blur.autocomplete", function() {
                d.onBlur();
            }), d.el.on("focus.autocomplete", function() {
                d.onFocus();
            }), d.el.on("change.autocomplete", function(a) {
                d.onKeyUp(a);
            }), d.el.on("input.autocomplete", function(a) {
                d.onKeyUp(a);
            });
        },
        onFocus: function() {
            var a = this;
            a.fixPosition(), 0 === a.options.minChars && 0 === a.el.val().length && a.onValueChange();
        },
        onBlur: function() {
            this.enableKillerFn();
        },
        abortAjax: function() {
            var a = this;
            a.currentRequest && (a.currentRequest.abort(), a.currentRequest = null);
        },
        setOptions: function(b) {
            var c = this, d = c.options;
            a.extend(d, b), c.isLocal = a.isArray(d.lookup), c.isLocal && (d.lookup = c.verifySuggestionsFormat(d.lookup)), 
            d.orientation = c.validateOrientation(d.orientation, "bottom"), a(c.suggestionsContainer).css({
                "max-height": d.maxHeight + "px",
                width: d.width + "px",
                "z-index": d.zIndex
            });
        },
        clearCache: function() {
            this.cachedResponse = {}, this.badQueries = [];
        },
        clear: function() {
            this.clearCache(), this.currentValue = "", this.suggestions = [];
        },
        disable: function() {
            var a = this;
            a.disabled = !0, clearInterval(a.onChangeInterval), a.abortAjax();
        },
        enable: function() {
            this.disabled = !1;
        },
        fixPosition: function() {
            var b = this, c = a(b.suggestionsContainer), d = c.parent().get(0);
            if (d === document.body || b.options.forceFixPosition) {
                var e = b.options.orientation, f = c.outerHeight(), g = b.el.outerHeight(), h = b.el.offset(), i = {
                    top: h.top,
                    left: h.left
                };
                if ("auto" === e) {
                    var j = a(window).height(), k = a(window).scrollTop(), l = -k + h.top - f, m = k + j - (h.top + g + f);
                    e = Math.max(l, m) === l ? "top" : "bottom";
                }
                if ("top" === e ? i.top += -f : i.top += g, d !== document.body) {
                    var n, o = c.css("opacity");
                    b.visible || c.css("opacity", 0).show(), n = c.offsetParent().offset(), i.top -= n.top, 
                    i.left -= n.left, b.visible || c.css("opacity", o).hide();
                }
                "auto" === b.options.width && (i.width = b.el.outerWidth() - 2 + "px"), c.css(i);
            }
        },
        enableKillerFn: function() {
            var b = this;
            a(document).on("click.autocomplete", b.killerFn);
        },
        disableKillerFn: function() {
            var b = this;
            a(document).off("click.autocomplete", b.killerFn);
        },
        killSuggestions: function() {
            var a = this;
            a.stopKillSuggestions(), a.intervalId = window.setInterval(function() {
                a.hide(), a.stopKillSuggestions();
            }, 50);
        },
        stopKillSuggestions: function() {
            window.clearInterval(this.intervalId);
        },
        isCursorAtEnd: function() {
            var a, b = this, c = b.el.val().length, d = b.element.selectionStart;
            return "number" == typeof d ? d === c : document.selection ? (a = document.selection.createRange(), 
            a.moveStart("character", -c), c === a.text.length) : !0;
        },
        onKeyPress: function(a) {
            var b = this;
            if (!b.disabled && !b.visible && a.which === d.DOWN && b.currentValue) return void b.suggest();
            if (!b.disabled && b.visible) {
                switch (a.which) {
                  case d.ESC:
                    b.el.val(b.currentValue), b.hide();
                    break;

                  case d.RIGHT:
                    if (b.hint && b.options.onHint && b.isCursorAtEnd()) {
                        b.selectHint();
                        break;
                    }
                    return;

                  case d.TAB:
                    if (b.hint && b.options.onHint) return void b.selectHint();
                    if (-1 === b.selectedIndex) return void b.hide();
                    if (b.select(b.selectedIndex), b.options.tabDisabled === !1) return;
                    break;

                  case d.RETURN:
                    if (-1 === b.selectedIndex) return void b.hide();
                    b.select(b.selectedIndex), b.options.preserveInput && (b.el.val(b.currentValue), 
                    b.el.blur());
                    break;

                  case d.UP:
                    b.moveUp();
                    break;

                  case d.DOWN:
                    b.moveDown();
                    break;

                  default:
                    return;
                }
                a.stopImmediatePropagation(), a.preventDefault();
            }
        },
        onKeyUp: function(a) {
            var b = this;
            if (!b.disabled) {
                switch (a.which) {
                  case d.UP:
                  case d.DOWN:
                    return;
                }
                clearInterval(b.onChangeInterval), b.currentValue !== b.el.val() && (b.findBestHint(), 
                b.options.deferRequestBy > 0 ? b.onChangeInterval = setInterval(function() {
                    b.onValueChange();
                }, b.options.deferRequestBy) : b.onValueChange());
            }
        },
        onValueChange: function() {
            var b = this, c = b.options, d = b.el.val(), e = b.getQuery(d);
            return b.selection && b.currentValue !== e && (b.selection = null, (c.onInvalidateSelection || a.noop).call(b.element)), 
            clearInterval(b.onChangeInterval), b.currentValue = d, b.selectedIndex = -1, c.triggerSelectOnValidInput && b.isExactMatch(e) ? void b.select(0) : void (e.length < c.minChars ? b.hide() : b.getSuggestions(e));
        },
        isExactMatch: function(a) {
            var b = this.suggestions;
            return 1 === b.length && b[0].value.toLowerCase() === a.toLowerCase();
        },
        getQuery: function(b) {
            var c, d = this.options.delimiter;
            return d ? (c = b.split(d), a.trim(c[c.length - 1])) : b;
        },
        getSuggestionsLocal: function(b) {
            var c, d = this, e = d.options, f = b.toLowerCase(), g = e.lookupFilter, h = parseInt(e.lookupLimit, 10);
            return c = {
                suggestions: a.grep(e.lookup, function(a) {
                    return g(a, b, f);
                })
            }, h && c.suggestions.length > h && (c.suggestions = c.suggestions.slice(0, h)), 
            c;
        },
        getSuggestions: function(b) {
            var c, d, e, f, g = this, h = g.options, i = h.serviceUrl;
            if (h.params[h.paramName] = b, d = h.ignoreParams ? null : h.params, h.onSearchStart.call(g.element, h.params) !== !1) {
                if (a.isFunction(h.lookup)) return void h.lookup(b, function(a) {
                    g.suggestions = a.suggestions, g.suggest(), h.onSearchComplete.call(g.element, b, a.suggestions);
                });
                g.isLocal ? c = g.getSuggestionsLocal(b) : (a.isFunction(i) && (i = i.call(g.element, b)), 
                e = i + "?" + a.param(d || {}), c = g.cachedResponse[e]), c && a.isArray(c.suggestions) ? (g.suggestions = c.suggestions, 
                g.suggest(), h.onSearchComplete.call(g.element, b, c.suggestions)) : g.isBadQuery(b) ? h.onSearchComplete.call(g.element, b, []) : (g.abortAjax(), 
                f = {
                    url: i,
                    data: d,
                    type: h.type,
                    dataType: h.dataType
                }, a.extend(f, h.ajaxSettings), g.currentRequest = a.ajax(f).done(function(a) {
                    var c;
                    g.currentRequest = null, c = h.transformResult(a, b), g.processResponse(c, b, e), 
                    h.onSearchComplete.call(g.element, b, c.suggestions);
                }).fail(function(a, c, d) {
                    h.onSearchError.call(g.element, b, a, c, d);
                }));
            }
        },
        isBadQuery: function(a) {
            if (!this.options.preventBadQueries) return !1;
            for (var b = this.badQueries, c = b.length; c--; ) if (0 === a.indexOf(b[c])) return !0;
            return !1;
        },
        hide: function() {
            var b = this, c = a(b.suggestionsContainer);
            a.isFunction(b.options.onHide) && b.visible && b.options.onHide.call(b.element, c), 
            b.visible = !1, b.selectedIndex = -1, clearInterval(b.onChangeInterval), a(b.suggestionsContainer).hide(), 
            b.signalHint(null);
        },
        suggest: function() {
            if (0 === this.suggestions.length) return void (this.options.showNoSuggestionNotice ? this.noSuggestions() : this.hide());
            var b, c = this, d = c.options, e = d.groupBy, f = d.formatResult, g = c.getQuery(c.currentValue), h = c.classes.suggestion, i = c.classes.selected, j = a(c.suggestionsContainer), k = a(c.noSuggestionsContainer), l = d.beforeRender, m = "", n = function(a, c) {
                var d = a.data[e];
                return b === d ? "" : (b = d, '<div class="autocomplete-group"><strong>' + b + "</strong></div>");
            };
            return d.triggerSelectOnValidInput && c.isExactMatch(g) ? void c.select(0) : (a.each(c.suggestions, function(a, b) {
                e && (m += n(b, g, a)), m += '<div class="' + h + '" data-index="' + a + '">' + f(b, g) + "</div>";
            }), this.adjustContainerWidth(), k.detach(), j.html(m), a.isFunction(l) && l.call(c.element, j), 
            c.fixPosition(), j.show(), d.autoSelectFirst && (c.selectedIndex = 0, j.scrollTop(0), 
            j.children("." + h).first().addClass(i)), c.visible = !0, void c.findBestHint());
        },
        noSuggestions: function() {
            var b = this, c = a(b.suggestionsContainer), d = a(b.noSuggestionsContainer);
            this.adjustContainerWidth(), d.detach(), c.empty(), c.append(d), b.fixPosition(), 
            c.show(), b.visible = !0;
        },
        adjustContainerWidth: function() {
            var b, c = this, d = c.options, e = a(c.suggestionsContainer);
            "auto" === d.width && (b = c.el.outerWidth() - 2, e.width(b > 0 ? b : 300));
        },
        findBestHint: function() {
            var b = this, c = b.el.val().toLowerCase(), d = null;
            c && (a.each(b.suggestions, function(a, b) {
                var e = 0 === b.value.toLowerCase().indexOf(c);
                return e && (d = b), !e;
            }), b.signalHint(d));
        },
        signalHint: function(b) {
            var c = "", d = this;
            b && (c = d.currentValue + b.value.substr(d.currentValue.length)), d.hintValue !== c && (d.hintValue = c, 
            d.hint = b, (this.options.onHint || a.noop)(c));
        },
        verifySuggestionsFormat: function(b) {
            return b.length && "string" == typeof b[0] ? a.map(b, function(a) {
                return {
                    value: a,
                    data: null
                };
            }) : b;
        },
        validateOrientation: function(b, c) {
            return b = a.trim(b || "").toLowerCase(), -1 === a.inArray(b, [ "auto", "bottom", "top" ]) && (b = c), 
            b;
        },
        processResponse: function(a, b, c) {
            var d = this, e = d.options;
            a.suggestions = d.verifySuggestionsFormat(a.suggestions), e.noCache || (d.cachedResponse[c] = a, 
            e.preventBadQueries && 0 === a.suggestions.length && d.badQueries.push(b)), b === d.getQuery(d.currentValue) && (d.suggestions = a.suggestions, 
            d.suggest());
        },
        activate: function(b) {
            var c, d = this, e = d.classes.selected, f = a(d.suggestionsContainer), g = f.find("." + d.classes.suggestion);
            return f.find("." + e).removeClass(e), d.selectedIndex = b, -1 !== d.selectedIndex && g.length > d.selectedIndex ? (c = g.get(d.selectedIndex), 
            a(c).addClass(e), c) : null;
        },
        selectHint: function() {
            var b = this, c = a.inArray(b.hint, b.suggestions);
            b.select(c);
        },
        select: function(a) {
            var b = this;
            b.hide(), b.onSelect(a);
        },
        moveUp: function() {
            var b = this;
            if (-1 !== b.selectedIndex) return 0 === b.selectedIndex ? (a(b.suggestionsContainer).children().first().removeClass(b.classes.selected), 
            b.selectedIndex = -1, b.el.val(b.currentValue), void b.findBestHint()) : void b.adjustScroll(b.selectedIndex - 1);
        },
        moveDown: function() {
            var a = this;
            a.selectedIndex !== a.suggestions.length - 1 && a.adjustScroll(a.selectedIndex + 1);
        },
        adjustScroll: function(b) {
            var c = this, d = c.activate(b);
            if (d) {
                var e, f, g, h = a(d).outerHeight();
                e = d.offsetTop, f = a(c.suggestionsContainer).scrollTop(), g = f + c.options.maxHeight - h, 
                f > e ? a(c.suggestionsContainer).scrollTop(e) : e > g && a(c.suggestionsContainer).scrollTop(e - c.options.maxHeight + h), 
                c.options.preserveInput || c.el.val(c.getValue(c.suggestions[b].value)), c.signalHint(null);
            }
        },
        onSelect: function(b) {
            var c = this, d = c.options.onSelect, e = c.suggestions[b];
            c.currentValue = c.getValue(e.value), c.currentValue === c.el.val() || c.options.preserveInput || c.el.val(c.currentValue), 
            c.signalHint(null), c.suggestions = [], c.selection = e, a.isFunction(d) && d.call(c.element, e);
        },
        getValue: function(a) {
            var b, c, d = this, e = d.options.delimiter;
            return e ? (b = d.currentValue, c = b.split(e), 1 === c.length ? a : b.substr(0, b.length - c[c.length - 1].length) + a) : a;
        },
        dispose: function() {
            var b = this;
            b.el.off(".autocomplete").removeData("autocomplete"), b.disableKillerFn(), a(window).off("resize.autocomplete", b.fixPositionCapture), 
            a(b.suggestionsContainer).remove();
        }
    }, a.fn.autocomplete = a.fn.devbridgeAutocomplete = function(c, d) {
        var e = "autocomplete";
        return 0 === arguments.length ? this.first().data(e) : this.each(function() {
            var f = a(this), g = f.data(e);
            "string" == typeof c ? g && "function" == typeof g[c] && g[c](d) : (g && g.dispose && g.dispose(), 
            g = new b(this, c), f.data(e, g));
        });
    };
});

var datePickerController = function a() {
    "use strict";
    function b(a) {
        for (;a.firstChild; ) a.removeChild(a.firstChild);
    }
    function c(a, b) {
        new RegExp("(^|[" + W + "])" + b + "([" + W + "]|$)").test(a.className) || (a.className += (a.className ? " " : "") + b);
    }
    function d(a, b) {
        a.className = b ? a.className.replace(new RegExp("(^|[" + W + "])" + b + "([" + W + "]|$)"), " ").replace(new RegExp("/^[" + W + "][" + W + "]*/"), "").replace(new RegExp("/[" + W + "][" + W + "]*$/"), "") : "";
    }
    function e() {
        var a = document.getElementsByTagName("html")[0].getAttribute("lang") || document.getElementsByTagName("html")[0].getAttribute("xml:lang");
        return a = a ? a.toLowerCase() : "en", -1 != a.search(/^([a-z]{2,3})-([a-z]{2})$/) ? [ a.match(/^([a-z]{2,3})-([a-z]{2})$/)[1], a ] : [ a ];
    }
    function f(a) {
        if ("object" == typeof a) {
            var b, c = {
                debug: function(a) {
                    return r = !!a, !0;
                },
                lang: function(a) {
                    return "string" == typeof a && -1 != a.search(/^[a-z]{2,3}(-([a-z]{2}))?$/i) && (u = [ a.toLowerCase() ], 
                    I = !0, E = !0), !0;
                },
                nodrag: function(a) {
                    return G = !!a, !0;
                },
                buttontabindex: function(a) {
                    return C = !!a, !0;
                },
                derivelocale: function(a) {
                    return E = !!a, !0;
                },
                mousewheel: function(a) {
                    return D = !!a, !0;
                },
                cellformat: function(a) {
                    return "string" == typeof a && h(a), !0;
                },
                titleformat: function(a) {
                    return "string" == typeof a && (L = a), !0;
                },
                statusformat: function(a) {
                    return "string" == typeof a && (M = a), !0;
                },
                describedby: function(a) {
                    return "string" == typeof a && (t = a), !0;
                },
                finalopacity: function(a) {
                    return "number" == typeof a && +a > 20 && 100 >= +a && (z = parseInt(a, 10)), !0;
                },
                bespoketitles: function(a) {
                    if ("object" == typeof a) {
                        y = {};
                        for (var b in a) a.hasOwnProperty(b) && -1 != String(b).match(V) && (y[b] = String(a[b]));
                    }
                    return !0;
                },
                dateparsefallback: function(a) {
                    return J = !!a, !0;
                },
                languagefilelocation: function(a) {
                    return H = a, !0;
                },
                _default: function() {
                    if (r) throw "Unknown key located within JSON data: " + b;
                    return !0;
                }
            };
            for (b in a) a.hasOwnProperty(b) && (c.hasOwnProperty(String(b).toLowerCase()) && c[String(b).toLowerCase()] || c._default)(a[b]);
        }
    }
    function g(a) {
        if ("string" != typeof a || "" == a) return {};
        try {
            if ("object" == typeof JSON && JSON.parse) return window.JSON.parse(a);
            if (/debug|lang|nodrag|buttontabindex|derivelocale|mousewheel|cellformat|titleformat|statusformat|describedby|finalopacity|bespoketitles|dateparsefallback/.test(a.toLowerCase())) {
                var b = Function([ "var document,top,self,window,parent,Number,Date,Object,Function,", "Array,String,Math,RegExp,Image,ActiveXObject;", "return (", a.replace(/<\!--.+-->/gim, "").replace(/\bfunction\b/g, "function-"), ");" ].join(""));
                return b();
            }
        } catch (c) {}
        if (r) throw "Could not parse the JSON object";
        return {
            err: 1
        };
    }
    function h(a) {
        return s ? (N = [ "%j" ], void (K = "%j %F %Y")) : void (-1 != a.match(/%([d|j])/) && (N = Y(a, /%([d|j])/), 
        K = a));
    }
    function i(a, b) {
        return b = Math.min(4, b || 2), "0000".substr(0, b - Math.min(String(a).length, b)) + a;
    }
    function j(a, b, c) {
        a.addEventListener ? a.addEventListener(b, c, !0) : a.attachEvent && a.attachEvent("on" + b, c);
    }
    function k(a, b, c) {
        try {
            a.removeEventListener ? a.removeEventListener(b, c, !0) : a.detachEvent && a.detachEvent("on" + b, c);
        } catch (d) {}
    }
    function l(a) {
        return a = a || document.parentWindow.event, a.stopPropagation && (a.stopPropagation(), 
        a.preventDefault()), X && (a.cancelBubble = !0, a.returnValue = !1), !1;
    }
    function m(a, b) {
        a && a.tagName && a.setAttribute("role", b);
    }
    function n(a, b, c) {
        a && a.tagName && a.setAttribute("aria-" + b, c);
    }
    function o(a, b) {
        a.setAttribute(X ? "tabIndex" : "tabindex", b), a.tabIndex = b;
    }
    function p(a) {
        return a instanceof Date && !isNaN(a) ? a.getFullYear() + i(a.getMonth() + 1) + "" + i(a.getDate()) : a;
    }
    function q(a) {
        this.dateSet = null, this.timerSet = !1, this.visible = !1, this.fadeTimer = null, 
        this.timer = null, this.yearInc = 0, this.monthInc = 0, this.dayInc = 0, this.mx = 0, 
        this.my = 0, this.x = 0, this.y = 0, this.created = !1, this.disabled = !1, this.opacity = 0, 
        this.opacityTo = 100, this.finalOpacity = 100, this.inUpdate = !1, this.kbEventsAdded = !1, 
        this.fullCreate = !1, this.selectedTD = null, this.cursorTD = null, this.cursorDate = a.cursorDate ? a.cursorDate : "", 
        this.date = a.cursorDate ? new Date(+a.cursorDate.substr(0, 4), +a.cursorDate.substr(4, 2) - 1, +a.cursorDate.substr(6, 2), 5, 0, 0) : new Date(), 
        this.defaults = {}, this.dynDisabledDates = {}, this.dateList = [], this.bespokeClass = a.bespokeClass, 
        this.firstDayOfWeek = F.firstDayOfWeek, this.interval = new Date(), this.clickActivated = !1, 
        this.showCursor = !1, this.noFocus = !0, this.kbEvent = !1, this.delayedUpdate = !1, 
        this.bespokeTitles = {}, this.bespokeTabIndex = a.bespokeTabIndex;
        for (var e in a) a.hasOwnProperty(e) && -1 == String(e).search(/^(callbacks|formElements|enabledDates|disabledDates)$/) && (this[e] = a[e]);
        X && (this.iePopUp = null);
        for (var f, g = 0; f = [ "callbacks", "formElements" ][g]; g++) if (this[f] = {}, 
        f in a) for (e in a[f]) a[f].hasOwnProperty(e) && (this[f][e] = a[f][e]);
        this.date.setHours(5), this.changeHandler = function() {
            h.disabled || (h.setDateFromInput(), h.callback("dateset", h.createCbArgObj()));
        }, this.createCbArgObj = function() {
            return this.dateSet ? {
                id: this.id,
                date: this.dateSet,
                dd: i(this.date.getDate()),
                mm: i(this.date.getMonth() + 1),
                yyyy: this.date.getFullYear()
            } : {
                id: this.id,
                date: null,
                dd: null,
                mm: null,
                yyyy: null
            };
        }, this.getScrollOffsets = function() {
            return "number" == typeof window.pageYOffset ? [ window.pageXOffset, window.pageYOffset ] : document.body && (document.body.scrollLeft || document.body.scrollTop) ? [ document.body.scrollLeft, document.body.scrollTop ] : document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop) ? [ document.documentElement.scrollLeft, document.documentElement.scrollTop ] : [ 0, 0 ];
        }, this.getDateExceptions = function(a, b) {
            b = i(b);
            var c, d, e, f, g, j, k, l, m, n, o, p = {}, q = h.firstDateShown, r = h.lastDateShown, s = h.dateList.length;
            for (r && q || (q = h.firstDateShown = a + i(b) + "01", r = h.lastDateShown = a + i(b) + i(ha(b, a))), 
            g = Number(q.substr(0, 6)), j = Number(r.substr(0, 6)), d = String(g); j >= +d; ) {
                for (e = d.substr(0, 4), f = d.substr(4, 2), c = 0; s > c; c++) if (l = String(h.dateList[c].rLow).replace(/^(\*\*\*\*)/, e).replace(/^(\d\d\d\d)(\*\*)/, "$1" + f), 
                m = String(h.dateList[c].rHigh).replace(/^(\*\*\*\*)/, e).replace(/^(\d\d\d\d)(\*\*)/, "$1" + f), 
                1 != m) {
                    if (m >= l && d >= l.substr(0, 6) && d <= m.substr(0, 6)) for (n = Math.max(l, Math.max(String(d) + "01", this.firstDateShown)), 
                    o = Math.min(m, Math.min(String(d) + "31", this.lastDateShown)), k = n; o >= k; k++) p[k] = h.dateList[c].type;
                } else +l >= +h.firstDateShown && +l <= +h.lastDateShown && (p[l] = h.dateList[c].type);
                d = new Date(e, +f, 2), d = d.getFullYear() + "" + i(d.getMonth() + 1);
            }
            return p;
        }, this.reposition = function() {
            if (h.created && !h.staticPos && (h.div.style.visibility = "hidden", h.div.style.display = "block", 
            !h.wrapped)) {
                h.div.style.left = h.div.style.top = "0px", console.log("repo");
                var a = h.div.offsetHeight, b = h.div.offsetWidth, c = document.getElementById("fd-but-" + h.id), d = h.truePosition(c), e = document.compatMode && "BackCompat" != document.compatMode ? document.documentElement : document.body, f = h.getScrollOffsets(), g = f[1], i = f[0], j = parseInt(d[1] - 2) - parseInt(g), k = parseInt(e.clientHeight + g) - parseInt(d[1] + c.offsetHeight + 2);
                h.div.style.visibility = "visible", h.div.style.left = Number(parseInt(e.clientWidth + i) < parseInt(b + d[0]) ? Math.abs(parseInt(e.clientWidth + i - b)) : d[0]) + "px", 
                h.div.style.top = k > j ? Math.abs(parseInt(d[1] + c.offsetHeight + 2)) + "px" : Math.abs(parseInt(d[1] - (a + 2))) + "px", 
                h.div.style.top = Math.abs(parseInt(d[1] + c.offsetHeight + 2)) + "px", 6 === X && (h.iePopUp.style.top = h.div.style.top, 
                h.iePopUp.style.left = h.div.style.left, h.iePopUp.style.width = b + "px", h.iePopUp.style.height = a - 2 + "px");
            }
        }, this.removeCursorHighlight = function() {
            var a = document.getElementById(h.id + "-date-picker-hover");
            a && d(a, "date-picker-hover");
        }, this.addCursorHighlight = function() {
            var a = document.getElementById(h.id + "-date-picker-hover");
            a && c(a, "date-picker-hover");
        }, this.removeOldFocus = function() {
            var a = document.getElementById(h.id + "-date-picker-hover");
            if (a) try {
                o(a, -1), d(a, "date-picker-hover"), a.id = "", a.onblur = null, a.onfocus = null;
            } catch (b) {}
        }, this.setNewFocus = function() {
            var a = document.getElementById(h.id + "-date-picker-hover");
            if (a) try {
                o(a, 0), this.showCursor && c(a, "date-picker-hover"), this.clickActivated || (a.onblur = h.onblur, 
                a.onfocus = h.onfocus), s || this.clickActivated || h.addAccessibleDate(), this.noFocus || this.clickActivated || setTimeout(function() {
                    try {
                        a.focus();
                    } catch (b) {}
                }, 0);
            } catch (b) {}
        }, this.addAccessibleDate = function() {
            var a = document.getElementById(h.id + "-date-picker-hover");
            if (a && !a.getElementsByTagName("span").length) {
                var c, d = a.className.match(/cd-([\d]{4})([\d]{2})([\d]{2})/), e = -1 != a.className.search(R), f = document.createElement("span");
                f.className = "fd-screen-reader", b(a), e && (c = f.cloneNode(!1), c.appendChild(document.createTextNode(ea(13))), 
                a.appendChild(c));
                for (var g, i = 0; g = N[i]; i++) "%j" == g || "%d" == g ? a.appendChild(document.createTextNode(ka(new Date(d[1], +d[2] - 1, d[3], 5, 0, 0), g, !0))) : (c = f.cloneNode(!1), 
                c.appendChild(document.createTextNode(ka(new Date(d[1], +d[2] - 1, d[3], 5, 0, 0), g, !0))), 
                a.appendChild(c));
            }
        }, this.setCursorDate = function(a) {
            -1 != String(a).search(/^([0-9]{8})$/) && (this.date = new Date(+a.substr(0, 4), +a.substr(4, 2) - 1, +a.substr(6, 2), 5, 0, 0), 
            this.cursorDate = a, this.staticPos && this.updateTable());
        }, this.updateTable = function(a) {
            if (h && !h.inUpdate && h.created) {
                if (h.inUpdate = !0, h.removeOldFocus(), h.div.dir = F.rtl ? "rtl" : "ltr", h.timerSet && !h.delayedUpdate) if (h.monthInc) {
                    var c = h.date.getDate(), d = new Date(h.date);
                    d.setDate(2), d.setMonth(d.getMonth() + 1 * h.monthInc), d.setDate(Math.min(c, ha(d.getMonth(), d.getFullYear()))), 
                    h.date = new Date(d);
                } else h.date.setDate(Math.min(h.date.getDate() + h.dayInc, ha(h.date.getMonth() + h.monthInc, h.date.getFullYear() + h.yearInc))), 
                h.date.setMonth(h.date.getMonth() + h.monthInc), h.date.setFullYear(h.date.getFullYear() + h.yearInc);
                h.outOfRange(), h.noToday || h.disableTodayButton(), h.showHideButtons(h.date);
                var e = h.date.getDate(), f = h.date.getMonth(), g = h.date.getFullYear(), j = String(g) + i(f + 1) + i(e), k = new Date(g, f, 1, 5, 0, 0);
                k.setHours(5);
                var l, m, o, p, q, r, s, t, u, w, x, y, z, A = (k.getDay() + 6) % 7, B = (A - h.firstDayOfWeek + 7) % 7 - 1, C = ha(f, g), D = new Date(), D = D.getFullYear() + i(D.getMonth() + 1) + i(D.getDate()), E = String(k.getFullYear()) + i(k.getMonth() + 1), s = [ 4, 4, 4, 4, 4, 4 ], G = new Date(g, f - 1, 1, 5, 0, 0), H = new Date(g, f + 1, 1, 5, 0, 0), I = ha(G.getMonth(), G.getFullYear()), J = String(H.getFullYear()) + i(H.getMonth() + 1), K = String(G.getFullYear()) + i(G.getMonth() + 1), M = (H.getDay() + 6) % 7, N = (G.getDay() + 6) % 7, O = document.createElement("span");
                O.className = "fd-screen-reader", h.firstDateShown = !h.constrainSelection && h.fillGrid && 1 > 0 - B ? String(K) + (I + (0 - B)) : E + "01", 
                h.lastDateShown = !h.constrainSelection && h.fillGrid ? J + i(41 - B - C) : E + String(C), 
                h.currentYYYYMM = E, u = h.callback("redraw", {
                    id: h.id,
                    dd: i(e),
                    mm: i(f + 1),
                    yyyy: g,
                    firstDateDisplayed: h.firstDateShown,
                    lastDateDisplayed: h.lastDateShown
                }) || {}, m = h.getDateExceptions(g, f + 1), h.checkSelectedDate(), x = null != h.dateSet ? h.dateSet.getFullYear() + i(h.dateSet.getMonth() + 1) + i(h.dateSet.getDate()) : !1, 
                null != this.selectedTD && (n(this.selectedTD, "selected", !1), this.selectedTD = null);
                for (var P = 0; 42 > P; P++) p = Math.floor(P / 7), q = h.tds[P], w = O.cloneNode(!1), 
                b(q), P > B && B + C >= P || h.fillGrid ? (t = E, z = A, l = P - B, o = [], y = !0, 
                1 > l ? (l = I + l, t = K, z = N, y = !h.constrainSelection, o.push("month-out")) : l > C && (l -= C, 
                t = J, z = M, y = !h.constrainSelection, o.push("month-out")), z = (z + l + 6) % 7, 
                o.push("day-" + z + " cell-" + P), r = t + String(10 > l ? "0" : "") + l, h.rangeLow && +r < +h.rangeLow || h.rangeHigh && +r > +h.rangeHigh ? (q.className = "out-of-range", 
                q.title = "", q.appendChild(document.createTextNode(l)), h.showWeeks && (s[p] = Math.min(s[p], 2))) : (y ? (q.title = L ? ka(new Date(+String(t).substr(0, 4), +String(t).substr(4, 2) - 1, +l, 5, 0, 0), L, !0) : "", 
                o.push("cd-" + r + " yyyymmdd-" + r + " yyyymm-" + t + " mmdd-" + t.substr(4, 2) + i(l))) : (q.title = L ? ea(13) + " " + ka(new Date(+String(t).substr(0, 4), +String(t).substr(4, 2) - 1, +l, 5, 0, 0), L, !0) : "", 
                o.push("yyyymmdd-" + r + " yyyymm-" + t + " mmdd-" + t.substr(4, 2) + i(l) + " not-selectable")), 
                r == D && o.push("date-picker-today"), null != h.startPeriod && null != h.endPeriod && r > h.startPeriod && r < h.endPeriod && o.push("date-picker-period"), 
                r === h.startPeriod && o.push("date-picker-startperiod"), r === h.endPeriod && o.push("date-picker-endperiod"), 
                x == r && (o.push("date-picker-selected-date"), n(q, "selected", "true"), this.selectedTD = q), 
                (r in m && 1 == m[r] || h.disabledDays[z] && !(r in m && 0 == m[r])) && (o.push("day-disabled"), 
                L && y && (q.title = ea(13) + " " + q.title)), r in u && o.push(u[r]), h.highlightDays[z] && o.push("date-picker-highlight"), 
                j == r && (q.id = h.id + "-date-picker-hover"), q.appendChild(document.createTextNode(l)), 
                q.className = o.join(" "), h.showWeeks && (s[p] = Math.min("month-out" == o[0] ? 3 : 1, s[p])))) : (q.className = "date-picker-unused", 
                q.appendChild(document.createTextNode(v)), q.title = ""), h.showWeeks && P - 7 * p == 6 && (b(h.wkThs[p]), 
                h.wkThs[p].appendChild(document.createTextNode(4 != s[p] || h.fillGrid ? ja(g, f, P - B - 6) : v)), 
                h.wkThs[p].className = "date-picker-week-header" + [ "", "", " out-of-range", " month-out", "" ][s[p]]);
                var Q = h.titleBar.getElementsByTagName("span");
                b(Q[0]), b(Q[1]), Q[0].appendChild(document.createTextNode(ga(f, !1) + v)), Q[1].appendChild(document.createTextNode(g)), 
                h.timerSet && (h.timerInc = 50 + Math.round((h.timerInc - 50) / 1.8), h.timer = window.setTimeout(h.updateTable, h.timerInc)), 
                h.inUpdate = h.delayedUpdate = !1, h.setNewFocus();
            }
        }, this.destroy = function() {
            if (document.getElementById("fd-but-" + this.id) && document.getElementById("fd-but-" + this.id).parentNode.removeChild(document.getElementById("fd-but-" + this.id)), 
            this.created) {
                if (k(this.table, "mousedown", h.onmousedown), k(this.table, "mouseover", h.onmouseover), 
                k(this.table, "mouseout", h.onmouseout), k(document, "mousedown", h.onmousedown), 
                k(document, "mouseup", h.clearTimer), window.addEventListener && !window.devicePixelRatio) try {
                    window.removeEventListener("DOMMouseScroll", this.onmousewheel, !1);
                } catch (a) {} else k(document, "mousewheel", this.onmousewheel), k(window, "mousewheel", this.onmousewheel);
                if (h.removeOnFocusEvents(), clearTimeout(h.fadeTimer), clearTimeout(h.timer), 6 === X && !h.staticPos) try {
                    h.iePopUp.parentNode.removeChild(h.iePopUp), h.iePopUp = null;
                } catch (a) {}
                this.div && this.div.parentNode && this.div.parentNode.removeChild(this.div), h = null;
            }
        }, this.resizeInlineDiv = function() {
            h.div.style.width = h.table.offsetWidth + "px", h.div.style.height = h.table.offsetHeight + "px";
        }, this.reset = function() {
            var a, b;
            for (a in h.formElements) b = document.getElementById(a), b && ("select" == b.tagName.toLowerCase() ? b.selectedIndex = h.defaultVals[a] : b.value = h.defaultVals[a]);
            h.changeHandler();
        }, this.create = function() {
            function a(a) {
                var b = document.createElement("th");
                return a.thClassName && (b.className = a.thClassName), a.colspan && b.setAttribute(X ? "colSpan" : "colspan", a.colspan), 
                b.unselectable = "on", b;
            }
            function b(b, c) {
                for (var d, e = 0; d = c[e]; e++) {
                    var f = a(d);
                    b.appendChild(f);
                    var g = document.createElement("span");
                    g.className = d.className, g.id = h.id + d.id, g.appendChild(document.createTextNode(d.text || h.nbsp)), 
                    g.title = d.title || "", g.unselectable = "on", f.appendChild(g);
                }
            }
            if (!document.getElementById("fd-" + this.id)) {
                var c, d, e, f, g, i;
                this.noFocus = !0, this.div = document.createElement("div"), this.div.id = "fd-" + this.id, 
                this.div.className = "date-picker" + (A ? " fd-dp-fade " : "") + this.bespokeClass, 
                this.div.style.visibility = "hidden", this.div.style.display = "none", this.describedBy && document.getElementById(this.describedBy) && n(this.div, "describedby", this.describedBy), 
                this.labelledBy && n(this.div, "labelledby", this.labelledBy.id), this.idiv = document.createElement("div"), 
                this.table = document.createElement("table"), this.table.className = "date-picker-table", 
                this.table.onmouseover = this.onmouseover, this.table.onmouseout = this.onmouseout, 
                this.table.onclick = this.onclick, this.finalOpacity < 100 && (this.idiv.style.opacity = Math.min(Math.max(parseInt(this.finalOpacity, 10) / 100, .2), 1)), 
                this.staticPos && (this.table.onmousedown = this.onmousedown), this.div.appendChild(this.idiv), 
                this.idiv.appendChild(this.table);
                var j = this.dragDisabled ? "" : " drag-enabled";
                if (this.staticPos) {
                    var k = document.getElementById(this.positioned ? this.positioned : this.id);
                    if (!k) {
                        if (this.div = null, r) throw this.positioned ? "Could not locate a datePickers associated parent element with an id:" + this.positioned : "Could not locate a datePickers associated input with an id:" + this.id;
                        return;
                    }
                    if (this.div.className += " static-datepicker", this.positioned ? k.appendChild(this.div) : k.parentNode.insertBefore(this.div, k.nextSibling), 
                    this.hideInput) for (var l in this.formElements) k = document.getElementById(l), 
                    k && (k.className += " fd-hidden-input");
                    setTimeout(this.resizeInlineDiv, 300);
                } else {
                    if (this.div.style.visibility = "hidden", this.div.className += j, this.wrapped) {
                        var k = document.getElementById(this.positioned ? this.positioned : this.id);
                        if (!k) {
                            if (this.div = null, r) throw this.positioned ? "Could not locate a datePickers associated parent element with an id:" + this.positioned : "Could not locate a datePickers associated input with an id:" + this.id;
                            return;
                        }
                        k.parentNode.insertBefore(this.div, k.nextSibling), this.div.className += " wrapped-datepicker";
                    } else document.getElementsByTagName("body")[0].appendChild(this.div);
                    6 === X && (this.iePopUp = document.createElement("iframe"), this.iePopUp.src = "javascript:'<html></html>';", 
                    this.iePopUp.setAttribute("className", "iehack"), this.iePopUp.setAttribute("tabIndex", -1), 
                    m(this.iePopUp, "presentation"), n(this.iePopUp, "hidden", "true"), this.iePopUp.scrolling = "no", 
                    this.iePopUp.frameBorder = "0", this.iePopUp.name = this.iePopUp.id = this.id + "-iePopUpHack", 
                    document.body.appendChild(this.iePopUp)), n(this.div, "hidden", "true");
                }
                m(this.div, "application"), this.statusFormat && (i = document.createElement("tfoot"), 
                this.table.appendChild(i), c = document.createElement("tr"), c.className = "date-picker-tfoot", 
                i.appendChild(c), this.statusBar = a({
                    thClassName: "date-picker-statusbar" + j,
                    colspan: this.showWeeks ? 8 : 7
                }), c.appendChild(this.statusBar), this.updateStatus()), f = document.createElement("thead"), 
                f.className = "date-picker-thead", this.table.appendChild(f), c = document.createElement("tr"), 
                m(c, "presentation"), f.appendChild(c), this.titleBar = a({
                    thClassName: "date-picker-title" + j,
                    colspan: this.showWeeks ? 8 : 7
                }), c.appendChild(this.titleBar), c = null;
                var o = document.createElement("span");
                o.appendChild(document.createTextNode(v)), o.className = "month-display" + j, this.titleBar.appendChild(o), 
                o = document.createElement("span"), o.appendChild(document.createTextNode(v)), o.className = "year-display" + j, 
                this.titleBar.appendChild(o), o = null, c = document.createElement("tr"), m(c, "presentation"), 
                f.appendChild(c), b(c, [ {
                    className: "prev-but prev-year",
                    id: "-prev-year-but",
                    text: "«",
                    title: ea(2)
                }, {
                    className: "prev-but prev-month",
                    id: "-prev-month-but",
                    text: "‹",
                    title: ea(0)
                }, {
                    colspan: this.showWeeks ? 4 : 3,
                    className: "today-but",
                    id: "-today-but",
                    text: ea(4)
                }, {
                    className: "next-but next-month",
                    id: "-next-month-but",
                    text: "›",
                    title: ea(1)
                }, {
                    className: "next-but next-year",
                    id: "-next-year-but",
                    text: "»",
                    title: ea(3)
                } ]), g = document.createElement("tbody"), this.table.appendChild(g);
                for (var p, q = this.showWeeks ? 8 : 7, s = this.showWeeks ? 0 : -1, t = 0; 7 > t; t++) {
                    d = document.createElement("tr"), 0 != t ? (m(d, "row"), g.appendChild(d)) : f.appendChild(d);
                    for (var u = 0; q > u; u++) 0 === t || this.showWeeks && 0 === u ? e = document.createElement("th") : (e = document.createElement("td"), 
                    n(e, "describedby", this.id + "-col-" + u + (this.showWeeks ? " " + this.id + "-row-" + t : "")), 
                    n(e, "selected", "false")), X && (e.unselectable = "on"), d.appendChild(e), this.showWeeks && u > 0 && t > 0 || !this.showWeeks && t > 0 || (0 === t && u > s ? (e.className = "date-picker-day-header", 
                    e.scope = "col", e.id = this.id + "-col-" + u) : (e.className = "date-picker-week-header", 
                    e.scope = "row", e.id = this.id + "-row-" + t));
                }
                e = d = null, this.ths = this.table.getElementsByTagName("thead")[0].getElementsByTagName("tr")[2].getElementsByTagName("th");
                for (var w = 0; q > w; w++) 0 == w && this.showWeeks ? (this.ths[w].appendChild(document.createTextNode(ea(6))), 
                this.ths[w].title = ea(8)) : w > (this.showWeeks ? 0 : -1) && (p = document.createElement("span"), 
                p.className = "fd-day-header", X && (p.unselectable = "on"), this.ths[w].appendChild(p));
                p = null, this.trs = this.table.getElementsByTagName("tbody")[0].getElementsByTagName("tr"), 
                this.tds = this.table.getElementsByTagName("tbody")[0].getElementsByTagName("td"), 
                this.butPrevYear = document.getElementById(this.id + "-prev-year-but"), this.butPrevMonth = document.getElementById(this.id + "-prev-month-but"), 
                this.butToday = document.getElementById(this.id + "-today-but"), this.butNextYear = document.getElementById(this.id + "-next-year-but"), 
                this.butNextMonth = document.getElementById(this.id + "-next-month-but"), this.noToday && (this.butToday.style.display = "none"), 
                this.showWeeks && (this.wkThs = this.table.getElementsByTagName("tbody")[0].getElementsByTagName("th"), 
                this.div.className += " weeks-displayed"), g = f = c = b = a = null, this.updateTableHeaders(), 
                this.created = !0, this.updateTable(), this.staticPos ? (this.visible = !0, this.opacity = 100, 
                this.div.style.visibility = "visible", this.div.style.display = "block", this.noFocus = !0, 
                this.fade()) : (this.reposition(), this.div.style.visibility = "visible", this.fade(), 
                this.noFocus = !0), this.callback("domcreate", {
                    id: this.id
                });
            }
        }, this.transEnd = function() {
            h.div.style.display = "none", h.div.style.visibility = "hidden", n(h.div, "hidden", "true");
        }, this.fade = function() {
            if (window.clearTimeout(h.fadeTimer), h.fadeTimer = null, A) return h.opacity = h.opacityTo, 
            void (0 == h.opacityTo ? (h.visible = !1, j(h.div, B, h.transEnd), c(h.div, "fd-dp-fade")) : (k(h.div, B, h.transEnd), 
            h.visible = !0, h.div.style.display = "block", h.div.style.visibility = "visible", 
            n(h.div, "hidden", "false"), d(h.div, "fd-dp-fade")));
            var a = Math.round(h.opacity + (h.opacityTo - h.opacity) / 4);
            h.setOpacity(a), Math.abs(h.opacityTo - a) > 3 && !h.noFadeEffect ? h.fadeTimer = window.setTimeout(h.fade, 50) : (h.setOpacity(h.opacityTo), 
            0 == h.opacityTo ? (h.div.style.display = "none", h.div.style.visibility = "hidden", 
            n(h.div, "hidden", "true"), h.visible = !1) : (h.div.style.display = "block", h.div.style.visibility = "visible", 
            n(h.div, "hidden", "false"), h.visible = !0));
        }, this.trackDrag = function(a) {
            a = a || window.event;
            var b = (a.pageX ? a.pageX : a.clientX ? a.clientX : a.x) - h.mx, c = (a.pageY ? a.pageY : a.clientY ? a.clientY : a.Y) - h.my;
            h.div.style.left = Math.round(h.x + b) > 0 ? Math.round(h.x + b) + "px" : "0px", 
            h.div.style.top = Math.round(h.y + c) > 0 ? Math.round(h.y + c) + "px" : "0px", 
            6 !== X || h.staticPos || (h.iePopUp.style.top = h.div.style.top, h.iePopUp.style.left = h.div.style.left);
        }, this.stopDrag = function(a) {
            var b = document.getElementsByTagName("body")[0];
            d(b, "fd-drag-active"), k(document, "mousemove", h.trackDrag, !1), k(document, "mouseup", h.stopDrag, !1), 
            h.div.style.zIndex = 9999;
        }, this.onmousedown = function(a) {
            a = a || document.parentWindow.event;
            var b = null != a.target ? a.target : a.srcElement, d = b, e = !0, f = new RegExp("^fd-(but-)?" + h.id + "$");
            for (h.mouseDownElem = null; b; ) {
                if (b.id && b.id.length && -1 != b.id.search(f)) {
                    e = !1;
                    break;
                }
                try {
                    b = b.parentNode;
                } catch (g) {
                    break;
                }
            }
            if (e) return _(), !0;
            if (-1 != (h.div.className + d.className).search("fd-disabled")) return !0;
            if (-1 != d.id.search(new RegExp("^" + h.id + "(-prev-year-but|-prev-month-but|-next-month-but|-next-year-but)$"))) {
                h.mouseDownElem = d, j(document, "mouseup", h.clearTimer), j(d, "mouseout", h.clearTimer);
                var k = {
                    "-prev-year-but": [ 0, -1, 0 ],
                    "-prev-month-but": [ 0, 0, -1 ],
                    "-next-year-but": [ 0, 1, 0 ],
                    "-next-month-but": [ 0, 0, 1 ]
                }, m = d.id.replace(h.id, ""), n = Number(h.date.getFullYear() + i(h.date.getMonth() + 1));
                return h.timerInc = 800, h.timerSet = !0, h.dayInc = k[m][0], h.yearInc = k[m][1], 
                h.monthInc = k[m][2], h.accellerator = 1, h.currentYYYYMM != n && (h.currentYYYYMM < n && (-1 == h.yearInc || -1 == h.monthInc) || h.currentYYYYMM > n && (1 == h.yearInc || 1 == h.monthInc) ? (h.delayedUpdate = !1, 
                h.timerInc = 1200) : h.delayedUpdate = !0), h.updateTable(), l(a);
            }
            return -1 != b.className.search("drag-enabled") ? (h.mx = a.pageX ? a.pageX : a.clientX ? a.clientX : a.x, 
            h.my = a.pageY ? a.pageY : a.clientY ? a.clientY : a.Y, h.x = parseInt(h.div.style.left, 10), 
            h.y = parseInt(h.div.style.top, 10), j(document, "mousemove", h.trackDrag, !1), 
            j(document, "mouseup", h.stopDrag, !1), c(document.getElementsByTagName("body")[0], "fd-drag-active"), 
            h.div.style.zIndex = 1e4, l(a)) : !0;
        }, this.onclick = function(a) {
            if (!A && h.opacity != h.opacityTo || h.disabled) return l(a);
            a = a || document.parentWindow.event;
            for (var b = null != a.target ? a.target : a.srcElement; b.parentNode; ) {
                if (b.tagName && "td" == b.tagName.toLowerCase()) {
                    if (-1 == b.className.search(/cd-([0-9]{8})/) || -1 != b.className.search(R)) return l(a);
                    var c = b.className.match(/cd-([0-9]{8})/)[1];
                    h.date = new Date(c.substr(0, 4), c.substr(4, 2) - 1, c.substr(6, 2), 5, 0, 0), 
                    h.dateSet = new Date(h.date), h.noFocus = !0, h.callback("dateset", {
                        id: h.id,
                        date: h.dateSet,
                        dd: h.dateSet.getDate(),
                        mm: h.dateSet.getMonth() + 1,
                        yyyy: h.dateSet.getFullYear()
                    }), h.returnFormattedDate(), h.hide(), h.stopTimer();
                    break;
                }
                if (b.id && b.id == h.id + "-today-but") {
                    h.date = new Date(), h.updateTable(), h.stopTimer();
                    break;
                }
                if (-1 != b.className.search(/date-picker-day-header/)) {
                    for (var d = h.showWeeks ? -1 : 0, e = b; e.previousSibling; ) e = e.previousSibling, 
                    e.tagName && "th" == e.tagName.toLowerCase() && d++;
                    h.firstDayOfWeek = (h.firstDayOfWeek + d) % 7, h.updateTableHeaders();
                    break;
                }
                try {
                    b = b.parentNode;
                } catch (f) {
                    break;
                }
            }
            return l(a);
        }, this.show = function(a) {
            if (!this.staticPos) {
                var b, d;
                for (d in this.formElements) if (b = document.getElementById(this.id), !b || b && b.disabled) return;
                this.noFocus = !0, this.created && document.getElementById("fd-" + this.id) ? (this.setDateFromInput(), 
                this.reposition()) : (this.created = !1, this.fullCreate = !1, this.create(), this.fullCreate = !0), 
                this.noFocus = !a, this.noFocus ? (this.clickActivated = !0, this.showCursor = !1, 
                j(document, "mousedown", this.onmousedown), D && (window.addEventListener && !window.devicePixelRatio ? window.addEventListener("DOMMouseScroll", this.onmousewheel, !1) : (j(document, "mousewheel", this.onmousewheel), 
                j(window, "mousewheel", this.onmousewheel)))) : (this.clickActivated = !1, this.showCursor = !0), 
                this.opacityTo = 100, this.div.style.display = "block", 6 === X && (this.iePopUp.style.width = this.div.offsetWidth + "px", 
                this.iePopUp.style.height = this.div.offsetHeight + "px", this.iePopUp.style.display = "block"), 
                this.setNewFocus(), this.fade();
                var e = document.getElementById("fd-but-" + this.id);
                e && c(e, "date-picker-button-active");
            }
        }, this.hide = function() {
            if (this.visible && this.created && document.getElementById("fd-" + this.id) && (this.kbEvent = !1, 
            d(h.div, "date-picker-focus"), this.stopTimer(), this.removeOnFocusEvents(), this.clickActivated = !1, 
            this.noFocus = !0, this.showCursor = !1, this.setNewFocus(), !this.staticPos)) {
                this.statusBar && this.updateStatus(ea(9));
                var a = document.getElementById("fd-but-" + this.id);
                if (a && d(a, "date-picker-button-active"), k(document, "mousedown", this.onmousedown), 
                D) if (window.addEventListener && !window.devicePixelRatio) try {
                    window.removeEventListener("DOMMouseScroll", this.onmousewheel, !1);
                } catch (b) {} else k(document, "mousewheel", this.onmousewheel), k(window, "mousewheel", this.onmousewheel);
                6 === X && (this.iePopUp.style.display = "none"), this.opacityTo = 0, this.fade();
            }
        }, this.onblur = function(a) {
            h.removeCursorHighlight(), h.hide();
        }, this.onfocus = function(a) {
            h.noFocus = !1, c(h.div, "date-picker-focus"), h.statusBar && h.updateStatus(ka(h.date, h.statusFormat, !0)), 
            h.showCursor = !0, h.addCursorHighlight(), h.addOnFocusEvents();
        }, this.onmousewheel = function(a) {
            a = a || document.parentWindow.event;
            var b = 0;
            a.wheelDelta ? (b = a.wheelDelta / 120, s && window.opera.version() < 9.2 && (b = -b)) : a.detail && (b = -a.detail / 3);
            var c = h.date.getDate(), d = new Date(h.date), e = b > 0 ? 1 : -1;
            return d.setDate(2), d.setMonth(d.getMonth() + 1 * e), d.setDate(Math.min(c, ha(d.getMonth(), d.getFullYear()))), 
            h.outOfRange(d) ? l(a) : (h.date = new Date(d), h.updateTable(), h.statusBar && h.updateStatus(ka(h.date, h.statusFormat, !0)), 
            l(a));
        }, this.onkeydown = function(a) {
            if (h.stopTimer(), !h.visible) return !1;
            a = a || document.parentWindow.event;
            var b = a.keyCode ? a.keyCode : a.charCode;
            if (13 == b) {
                var c = document.getElementById(h.id + "-date-picker-hover");
                return c && -1 != c.className.search(/cd-([0-9]{8})/) && -1 == c.className.search(/out-of-range|day-disabled/) ? (h.dateSet = new Date(h.date), 
                h.callback("dateset", h.createCbArgObj()), h.returnFormattedDate(), h.hide(), l(a)) : l(a);
            }
            if (27 == b) {
                if (!h.staticPos) {
                    h.hide();
                    var d = document.getElementById("fd-but-" + h.id);
                    return d && setTimeout(function() {
                        try {
                            d.focus();
                        } catch (a) {}
                    }, 0), l(a);
                }
                return !0;
            }
            if (32 == b || 0 == b) return h.date = new Date(), h.updateTable(), l(a);
            if (9 == b) return h.staticPos ? !0 : l(a);
            if (X) {
                if (new Date().getTime() - h.interval.getTime() < 50) return l(a);
                h.interval = new Date();
            }
            if (b > 49 && 56 > b || b > 97 && 104 > b) return b > 96 && (b -= 48), b -= 49, 
            h.firstDayOfWeek = (h.firstDayOfWeek + b) % 7, h.updateTableHeaders(), l(a);
            if (33 > b || b > 40) return !0;
            var e = new Date(h.date);
            h.date.getFullYear() + i(h.date.getMonth() + 1);
            if (36 == b) e.setDate(1); else if (35 == b) e.setDate(ha(e.getMonth(), e.getFullYear())); else if (33 == b || 34 == b) {
                var f = 34 == b ? 1 : -1;
                if (a.ctrlKey) e.setFullYear(e.getFullYear() + 1 * f); else {
                    var g = h.date.getDate();
                    e.setDate(2), e.setMonth(e.getMonth() + 1 * f), e.setDate(Math.min(g, ha(e.getMonth(), e.getFullYear())));
                }
            } else 37 == b ? e = new Date(h.date.getFullYear(), h.date.getMonth(), h.date.getDate() - 1, 5, 0, 0) : 39 == b || 34 == b ? e = new Date(h.date.getFullYear(), h.date.getMonth(), h.date.getDate() + 1, 5, 0, 0) : 38 == b ? e = new Date(h.date.getFullYear(), h.date.getMonth(), h.date.getDate() - 7, 5, 0, 0) : 40 == b && (e = new Date(h.date.getFullYear(), h.date.getMonth(), h.date.getDate() + 7, 5, 0, 0));
            if (h.outOfRange(e)) return l(a);
            h.date = e, h.statusBar && h.updateStatus(h.getBespokeTitle(h.date.getFullYear(), h.date.getMonth() + 1, h.date.getDate()) || ka(h.date, h.statusFormat, !0));
            var j = String(h.date.getFullYear()) + i(h.date.getMonth() + 1) + i(h.date.getDate());
            if (a.ctrlKey || 33 == b || 34 == b || j < h.firstDateShown || j > h.lastDateShown) h.updateTable(), 
            X && (h.interval = new Date()); else {
                h.noToday || h.disableTodayButton(), h.removeOldFocus(), h.showHideButtons(h.date);
                for (var c, k = 0; c = h.tds[k]; k++) if (-1 != c.className.search("cd-" + j)) {
                    c.id = h.id + "-date-picker-hover", h.setNewFocus();
                    break;
                }
            }
            return l(a);
        }, this.onmouseout = function(a) {
            a = a || document.parentWindow.event;
            for (var b = a.toElement || a.relatedTarget; b && b != this; ) try {
                b = b.parentNode;
            } catch (a) {
                b = this;
            }
            return b == this ? !1 : ((h.clickActivated || h.staticPos && !h.kbEventsAdded) && (h.showCursor = !1, 
            h.removeCursorHighlight()), h.currentTR && (h.currentTR.className = "", h.currentTR = null), 
            void (h.statusBar && h.updateStatus(h.dateSet ? h.getBespokeTitle(h.dateSet.getFullYear(), h.dateSet.getMonth() + 1, h.dateSet.getDate()) || ka(h.dateSet, h.statusFormat, !0) : ea(9))));
        }, this.onmouseover = function(a) {
            a = a || document.parentWindow.event;
            for (var b = null != a.target ? a.target : a.srcElement; 1 != b.nodeType; ) b = b.parentNode;
            if (b && b.tagName) {
                h.noFocus = !0;
                var c = ea(9);
                switch ((h.clickActivated || h.staticPos && !h.kbEventsAdded) && (h.showCursor = !1), 
                b.tagName.toLowerCase()) {
                  case "td":
                    if (-1 != b.className.search(/date-picker-unused|out-of-range/) && (c = ea(9)), 
                    -1 != b.className.search(/cd-([0-9]{8})/)) {
                        h.showCursor = !0, h.stopTimer();
                        var d = b.className.match(/cd-([0-9]{8})/)[1];
                        h.removeOldFocus(), b.id = h.id + "-date-picker-hover", h.setNewFocus(), h.date = new Date(+d.substr(0, 4), +d.substr(4, 2) - 1, +d.substr(6, 2), 5, 0, 0), 
                        h.noToday || h.disableTodayButton(), c = h.getBespokeTitle(+d.substr(0, 4), +d.substr(4, 2), +d.substr(6, 2)) || ka(h.date, h.statusFormat, !0);
                    }
                    break;

                  case "th":
                    if (!h.statusBar) break;
                    if (-1 != b.className.search(/drag-enabled/)) c = ea(10); else if (-1 != b.className.search(/date-picker-week-header/)) {
                        var e = b.firstChild ? b.firstChild.nodeValue : "";
                        c = -1 != e.search(/^(\d+)$/) ? ea(7, [ e, 3 > e && 11 == h.date.getMonth() ? ia(h.date.getFullYear()) + 1 : ia(h.date.getFullYear()) ]) : ea(9);
                    }
                    break;

                  case "span":
                    if (!h.statusBar) break;
                    if (-1 != b.className.search(/day-([0-6])/)) {
                        var f = b.className.match(/day-([0-6])/)[1];
                        c = ea(11, [ fa(f, !1) ]);
                    } else -1 != b.className.search(/(drag-enabled|today-but|prev-(year|month)|next-(year|month))/) && -1 == b.className.search(/disabled/) && (c = ea({
                        "drag-enabled": 10,
                        "prev-year": 2,
                        "prev-month": 0,
                        "next-year": 3,
                        "next-month": 1,
                        "today-but": 12
                    }[b.className.match(/(drag-enabled|today-but|prev-(year|month)|next-(year|month))/)[0]]));
                    break;

                  default:
                    c = "";
                }
                for (;b.parentNode; ) if (b = b.parentNode, 1 == b.nodeType && "tr" == b.tagName.toLowerCase()) {
                    if (h.currentTR) {
                        if (b == h.currentTR) break;
                        h.currentTR.className = "";
                    }
                    b.className = "dp-row-highlight", h.currentTR = b;
                    break;
                }
                h.statusBar && c && h.updateStatus(c), h.showCursor || h.removeCursorHighlight();
            }
        }, this.clearTimer = function() {
            h.stopTimer(), h.timerInc = 800, h.yearInc = 0, h.monthInc = 0, h.dayInc = 0, k(document, "mouseup", h.clearTimer), 
            null != h.mouseDownElem && k(h.mouseDownElem, "mouseout", h.clearTimer), h.mouseDownElem = null;
        };
        var h = this;
        this.setDateFromInput(), this.staticPos ? this.create() : this.createButton(), function() {
            var a, b, c = 0;
            for (a in h.formElements) b = document.getElementById(a), b && b.tagName && -1 != b.tagName.search(/select|input/i) && (j(b, "change", h.changeHandler), 
            0 == c && b.form && j(b.form, "reset", h.reset), c++), b && 1 != b.disabled || h.disableDatePicker();
        }(), this.fullCreate = !0;
    }
    var r = !1, s = "[object Opera]" === Object.prototype.toString.call(window.opera), t = "", u = e(), v = String.fromCharCode(160), w = {}, x = {}, y = {}, z = 100, A = null, B = "", C = !0, D = !0, E = !0, F = !1, G = !1, H = !1, I = !1, J = !0, K = "%d %F %Y", L = "%F %d, %Y", M = "", N = s ? [ "%j" ] : [ "%j", " %F %Y" ], O = /%([d|j])/, P = /%([M|F|m|n])/, Q = /%[y|Y]/, R = /date-picker-unused|out-of-range|day-disabled|not-selectable/, S = /%([d|j|M|F|m|n|Y|y])/, T = /%([d|D|l|j|N|w|S|W|M|F|m|n|t|Y|y])/, U = /^((\d\d\d\d)(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01]))$/, V = /^(((\d\d\d\d)|(\*\*\*\*))((0[1-9]|1[012])|(\*\*))(0[1-9]|[12][0-9]|3[01]))$/, W = "	\n\f\r   ᠎             　\u2028\u2029", X = function() {
        for (var a, b = 3, c = document.createElement("div"), d = c.getElementsByTagName("i"); c.innerHTML = "<!--[if gt IE " + ++b + "]><i></i><![endif]-->", 
        d[0]; ) ;
        return b > 4 ? b : a;
    }();
    !function() {
        var a = document.getElementsByTagName("script"), b = g(String(a[a.length - 1].innerHTML).replace(/[\n\r\s\t]+/g, " ").replace(/^\s+/, "").replace(/\s+$/, ""));
        "object" != typeof b || "err" in b || f(b), E && "object" != typeof fdLocale || (I = !0);
    }();
    var Y = function(a, b, c) {
        if ("[object RegExp]" !== Object.prototype.toString.call(b)) return Y._nativeSplit.call(a, b, c);
        var d, e, f, g, h = [], i = 0, j = "", b = RegExp(b.source, "g");
        if (a += "", Y._compliantExecNpcg || (d = RegExp("^" + b.source + "$(?!\\s)", j)), 
        void 0 === c || 0 > +c) c = 1 / 0; else if (c = Math.floor(+c), !c) return [];
        for (;(e = b.exec(a)) && (f = e.index + e[0].length, !(f > i && (h.push(a.slice(i, e.index)), 
        !Y._compliantExecNpcg && e.length > 1 && e[0].replace(d, function() {
            for (var a = 1; a < arguments.length - 2; a++) void 0 === arguments[a] && (e[a] = void 0);
        }), e.length > 1 && e.index < a.length && Array.prototype.push.apply(h, e.slice(1)), 
        g = e[0].length, i = f, h.length >= c))); ) b.lastIndex === e.index && b.lastIndex++;
        return i === a.length ? (g || !b.test("")) && h.push("") : h.push(a.slice(i)), h.length > c ? h.slice(0, c) : h;
    };
    Y._compliantExecNpcg = void 0 === /()??/.exec("")[1], Y._nativeSplit = String.prototype.split, 
    q.prototype.addButtonEvents = function(a) {
        function b(a) {
            a = a || window.event;
            var b = this.id.replace("fd-but-", ""), e = ra(b), f = !1, g = w[b].kbEvent;
            if (g) return void (w[b].kbEvent = !1);
            if ("keydown" == a.type) {
                var h = null != a.keyCode ? a.keyCode : a.charCode;
                if (13 != h) return !0;
                if (w[b].kbEvent = !0, e) return d(this, "date-picker-button-active"), _(), l(a);
                f = !0;
            } else w[b].kbEvent = !1;
            return e ? (d(this, "date-picker-button-active"), _()) : (c(this, "date-picker-button-active"), 
            _(b), ba(b, f)), l(a);
        }
        a.onclick = b, a.onkeydown = b, C ? o(a, this.bespokeTabIndex) : o(a, -1);
    }, q.prototype.createButton = function() {
        if (!this.staticPos && !document.getElementById("fd-but-" + this.id)) {
            var a = document.getElementById(this.id), b = document.createElement("span"), c = document.createElement("a");
            c.href = "#" + this.id, c.className = "date-picker-control", c.title = ea(5), c.id = "fd-but-" + this.id, 
            b.appendChild(document.createTextNode(v)), c.appendChild(b), b = document.createElement("span"), 
            b.className = "fd-screen-reader", b.appendChild(document.createTextNode(c.title)), 
            c.appendChild(b), m(c, "button"), n(c, "haspopup", !0), this.positioned && document.getElementById(this.positioned) ? document.getElementById(this.positioned).appendChild(c) : a.parentNode.insertBefore(c, a.nextSibling), 
            this.addButtonEvents(c);
            var d = document.getElementById(this.id);
            d && this.addButtonEvents(d), c = null, this.callback("dombuttoncreate", {
                id: this.id
            });
        }
    }, q.prototype.setBespokeTitles = function(a) {
        this.bespokeTitles = {}, this.addBespokeTitles(a);
    }, q.prototype.addBespokeTitles = function(a) {
        for (var b in a) a.hasOwnProperty(b) && (this.bespokeTitles[b] = a[b]);
    }, q.prototype.getBespokeTitle = function(a, b, c) {
        var d, e, f = a + String(i(b)) + i(c);
        for (d in this.bespokeTitles) if (this.bespokeTitles.hasOwnProperty(d) && (e = String(d).replace(/^(\*\*\*\*)/, a).replace(/^(\d\d\d\d)(\*\*)/, "$1" + i(b)), 
        e == f)) return this.bespokeTitles[d];
        for (d in y) if (y.hasOwnProperty(d) && (e = String(d).replace(/^(\*\*\*\*)/, a).replace(/^(\d\d\d\d)(\*\*)/, "$1" + i(b)), 
        e == f)) return y[d];
        return !1;
    }, q.prototype.returnSelectedDate = function() {
        return this.dateSet;
    }, q.prototype.setRangeLow = function(a) {
        if (-1 == String(a).search(U)) {
            if (r) throw "Invalid value passed to setRangeLow method: " + a;
            return !1;
        }
        this.rangeLow = a, this.inUpdate || this.setDateFromInput();
    }, q.prototype.setRangeHigh = function(a) {
        if (-1 == String(a).search(U)) {
            if (r) throw "Invalid value passed to setRangeHigh method: " + a;
            return !1;
        }
        this.rangeHigh = a, this.inUpdate || this.setDateFromInput();
    }, q.prototype.setPeriodHighlight = function(a, b) {
        this.startPeriod = a, this.endPeriod = b;
    }, q.prototype.setDisabledDays = function(a) {
        if (!a.length || -1 == a.join("").search(/^([0|1]{7})$/)) {
            if (r) throw "Invalid values located when attempting to call setDisabledDays";
            return !1;
        }
        this.disabledDays = a, this.inUpdate || this.setDateFromInput();
    }, q.prototype.setDisabledDates = function(a) {
        this.filterDateList(a, !0);
    }, q.prototype.setEnabledDates = function(a) {
        this.filterDateList(a, !1);
    }, q.prototype.addDisabledDates = function(a) {
        this.addDatesToList(a, !0);
    }, q.prototype.addEnabledDates = function(a) {
        this.addDatesToList(a, !1);
    }, q.prototype.filterDateList = function(a, b) {
        for (var c = [], d = 0; d < this.dateList.length; d++) this.dateList[d].type != b && c.push(this.dateList[d]);
        this.dateList = c.concat(), this.addDatesToList(a, b);
    }, q.prototype.addDatesToList = function(a, b) {
        var c;
        for (c in a) if (-1 != String(c).search(V) && (1 == a[c] || -1 != String(a[c]).search(V))) {
            if (1 != a[c] && Number(String(c).replace(/^\*\*\*\*/, 2010).replace(/^(\d\d\d\d)(\*\*)/, "$122")) > Number(String(a[c]).replace(/^\*\*\*\*/, 2010).replace(/^(\d\d\d\d)(\*\*)/, "$122"))) continue;
            this.dateList.push({
                type: !!b,
                rLow: c,
                rHigh: a[c]
            });
        }
        this.inUpdate || this.setDateFromInput();
    }, q.prototype.setSelectedDate = function(a) {
        if (-1 == String(a).search(V)) return !1;
        var b = a.match(U), c = new Date(+b[2], +b[3] - 1, +b[4], 5, 0, 0);
        return c && !isNaN(c) && this.canDateBeSelected(c) ? (this.dateSet = new Date(c), 
        this.inUpdate || this.updateTable(), this.callback("dateset", this.createCbArgObj()), 
        void this.returnFormattedDate()) : !1;
    }, q.prototype.checkSelectedDate = function() {
        this.dateSet && !this.canDateBeSelected(this.dateSet) && (this.dateSet = null), 
        this.inUpdate || this.updateTable();
    }, q.prototype.addOnFocusEvents = function() {
        console.log(this), this.kbEventsAdded || this.noFocus || (j(document, "keypress", this.onkeydown), 
        j(document, "mousedown", this.onmousedown), X && (k(document, "keypress", this.onkeydown), 
        j(document, "keydown", this.onkeydown)), window.devicePixelRatio && (k(document, "keypress", this.onkeydown), 
        j(document, "keydown", this.onkeydown)), this.noFocus = !1, this.kbEventsAdded = !0);
    }, q.prototype.removeOnFocusEvents = function() {
        this.kbEventsAdded && (k(document, "keypress", this.onkeydown), k(document, "keydown", this.onkeydown), 
        k(document, "mousedown", this.onmousedown), this.kbEventsAdded = !1);
    }, q.prototype.stopTimer = function() {
        this.timerSet = !1, window.clearTimeout(this.timer);
    }, q.prototype.setOpacity = function(a) {
        this.div.style.opacity = a / 100, this.div.style.filter = "alpha(opacity=" + a + ")", 
        this.opacity = a;
    }, q.prototype.truePosition = function(a) {
        var b = this.cumulativeOffset(a);
        if (s) return b;
        var c = document.compatMode && "BackCompat" != document.compatMode ? document.documentElement : document.body, d = document.all ? c.scrollLeft : window.pageXOffset, e = document.all ? c.scrollTop : window.pageYOffset, f = this.realOffset(a);
        return [ b[0] - f[0] + d, b[1] - f[1] + e ];
    }, q.prototype.realOffset = function(a) {
        var b = 0, c = 0;
        do b += a.scrollTop || 0, c += a.scrollLeft || 0, a = a.parentNode; while (a);
        return [ c, b ];
    }, q.prototype.cumulativeOffset = function(a) {
        var b = 0, c = 0;
        do b += a.offsetTop || 0, c += a.offsetLeft || 0, a = a.offsetParent; while (a);
        return [ c, b ];
    }, q.prototype.outOfRange = function(a) {
        if (!this.rangeLow && !this.rangeHigh) return !1;
        var b = !1;
        a || (b = !0, a = this.date);
        var c = i(a.getDate()), d = i(a.getMonth() + 1), e = a.getFullYear(), f = String(e) + String(d) + String(c);
        if (this.rangeLow && +f < +this.rangeLow) return b ? (this.date = new Date(this.rangeLow.substr(0, 4), this.rangeLow.substr(4, 2) - 1, this.rangeLow.substr(6, 2), 5, 0, 0), 
        !1) : !0;
        if (this.rangeHigh && +f > +this.rangeHigh) {
            if (!b) return !0;
            this.date = new Date(this.rangeHigh.substr(0, 4), this.rangeHigh.substr(4, 2) - 1, this.rangeHigh.substr(6, 2), 5, 0, 0);
        }
        return !1;
    }, q.prototype.canDateBeSelected = function(a) {
        if (!a || isNaN(a)) return !1;
        var b = i(a.getDate()), c = i(a.getMonth() + 1), d = a.getFullYear(), e = d + "" + c + b, f = this.getDateExceptions(d, c), g = 0 == a.getDay() ? 7 : a.getDay();
        return this.rangeLow && +e < +this.rangeLow || this.rangeHigh && +e > +this.rangeHigh || e in f && 1 == f[e] || this.disabledDays[g - 1] && (!(e in f) || e in f && 1 == f[e]) ? !1 : !0;
    }, q.prototype.updateStatus = function(a) {
        if (b(this.statusBar), a && -1 != this.statusFormat.search(/%S/) && -1 != a.search(/([0-9]{1,2})(st|nd|rd|th)/)) {
            a = Y(a.replace(/([0-9]{1,2})(st|nd|rd|th)/, "$1<sup>$2</sup>"), /<sup>|<\/sup>/);
            for (var c, d = document.createDocumentFragment(), e = 0; c = a[e]; e++) if (/^(st|nd|rd|th)$/.test(c)) {
                var f = document.createElement("sup");
                f.appendChild(document.createTextNode(c)), d.appendChild(f);
            } else d.appendChild(document.createTextNode(c));
            this.statusBar.appendChild(d);
        } else this.statusBar.appendChild(document.createTextNode(a ? a : ea(9)));
    }, q.prototype.setDateFromInput = function() {
        var a, b, e, f, g, h, i, j, k, l, m, n = (this.dateSet, !1), o = this.staticPos ? !1 : document.getElementById("fd-but-" + this.id), p = F.imported ? [].concat(Z.fullMonths).concat(Z.monthAbbrs) : [], q = F.imported ? [].concat(F.fullMonths).concat(F.monthAbbrs) : [], r = /(3[01]|[12][0-9]|0?[1-9])(st|nd|rd|th)/i, s = 0, t = !1;
        this.dateSet = null;
        for (e in this.formElements) {
            if (f = document.getElementById(e), !f) return !1;
            if (s++, j = String(f.value)) for (g = this.formElements[e], a = [ g ], t = !1, 
            k = -1 != g.search(O), l = -1 != g.search(P), m = -1 != g.search(Q), k && l && m || (!m || l || k ? !l || m || k ? !k || m || l || (a = a.concat([ "%d%", "%j" ])) : a = a.concat([ "%M", "%F", "%m", "%n" ]) : a = a.concat([ "%Y", "%y" ])), 
            b = 0; b < a.length && (t = la(j, a[b]), t && (!h && k && t.d && (h = t.d), n === !1 && l && t.m && (n = t.m), 
            !i && m && t.y && (i = t.y)), !(k && h || !k) || (!l || !n != !1) && l || !(m && i || !m)); b++) ;
        }
        if (J && (!h || n === !1 || !i) && k && l && m && 1 == s && j) {
            if (F.imported) for (b = 0; b < q.length; b++) j = j.replace(new RegExp(q[b], "i"), p[b]);
            -1 != j.search(r) && (j = j.replace(r, j.match(r)[1])), t = new Date(j.replace(new RegExp("-", "g"), "/")), 
            t && !isNaN(t) && (h = t.getDate(), n = t.getMonth() + 1, i = t.getFullYear());
        }
        if (t = !1, h && n !== !1 && i && (+h > ha(+n - 1, +i) ? (h = ha(+n - 1, +i), t = !1) : t = new Date(+i, +n - 1, +h, 5, 0, 0)), 
        o && d(o, "date-picker-dateval"), !t || isNaN(t)) {
            var u = new Date(i || new Date().getFullYear(), n !== !1 ? n - 1 : new Date().getMonth(), 1, 5, 0, 0);
            return this.date = this.cursorDate ? new Date(+this.cursorDate.substr(0, 4), +this.cursorDate.substr(4, 2) - 1, +this.cursorDate.substr(6, 2), 5, 0, 0) : new Date(u.getFullYear(), u.getMonth(), Math.min(+h || new Date().getDate(), ha(u.getMonth(), u.getFullYear())), 5, 0, 0), 
            this.outOfRange(), void (this.fullCreate && this.updateTable());
        }
        t.setHours(5), this.date = new Date(t), this.outOfRange(), t.getTime() == this.date.getTime() && this.canDateBeSelected(this.date) && (this.dateSet = new Date(this.date), 
        o && c(o, "date-picker-dateval"), this.returnFormattedDate(!0)), this.fullCreate && this.updateTable();
    }, q.prototype.setSelectIndex = function(a, b) {
        for (var c = a.options.length - 1; c >= 0; c--) if (a.options[c].value == b) return void (a.selectedIndex = c);
    }, q.prototype.returnFormattedDate = function(a) {
        var b = this.staticPos ? !1 : document.getElementById("fd-but-" + this.id);
        if (!this.dateSet) return void (b && d(b, "date-picker-dateval"));
        var e, f, g, h, j = (i(this.dateSet.getDate()), i(this.dateSet.getMonth() + 1), 
        this.dateSet.getFullYear(), !1);
        a = !!a;
        for (e in this.formElements) {
            if (f = document.getElementById(e), !f) return;
            j || (j = f), g = this.formElements[e], h = ka(this.dateSet, g, I), "input" == f.tagName.toLowerCase() ? f.value = h : this.setSelectIndex(f, h);
        }
        if (this.staticPos ? (this.noFocus = !0, this.updateTable(), this.noFocus = !1) : b && c(b, "date-picker-dateval"), 
        this.fullCreate && j.type && "hidden" != j.type && !a) try {
            j.focus();
        } catch (k) {}
        a || this.callback("datereturned", this.createCbArgObj());
    }, q.prototype.disableDatePicker = function() {
        if (!this.disabled) {
            if (this.staticPos) this.removeOnFocusEvents(), this.removeOldFocus(), this.noFocus = !0, 
            c(this.div, "date-picker-disabled"), this.table.onmouseover = this.table.onclick = this.table.onmouseout = this.table.onmousedown = null, 
            k(document, "mousedown", this.onmousedown), k(document, "mouseup", this.clearTimer); else {
                this.visible && this.hide();
                var a = document.getElementById("fd-but-" + this.id);
                a && (c(a, "date-picker-control-disabled"), n(a, "disabled", !0), a.onkeydown = a.onclick = function() {
                    return !1;
                }, o(a, -1), a.title = "");
            }
            clearTimeout(this.timer), this.disabled = !0;
        }
    }, q.prototype.enableDatePicker = function() {
        if (console.log("passo"), this.disabled) {
            if (this.staticPos) this.removeOldFocus(), null != this.dateSet && (this.date = this.dateSet), 
            this.noFocus = !0, this.updateTable(), d(this.div, "date-picker-disabled"), this.disabled = !1, 
            this.table.onmouseover = this.onmouseover, this.table.onmouseout = this.onmouseout, 
            this.table.onclick = this.onclick, this.table.onmousedown = this.onmousedown; else {
                var a = document.getElementById("fd-but-" + this.id);
                a && (d(a, "date-picker-control-disabled"), n(a, "disabled", !1), this.addButtonEvents(a), 
                a.title = ea(5));
            }
            this.disabled = !1;
        }
    }, q.prototype.disableTodayButton = function() {
        var a = new Date();
        d(this.butToday, "fd-disabled"), (this.outOfRange(a) || this.date.getDate() == a.getDate() && this.date.getMonth() == a.getMonth() && this.date.getFullYear() == a.getFullYear()) && c(this.butToday, "fd-disabled");
    }, q.prototype.updateTableHeaders = function() {
        for (var a, e, f = this.showWeeks ? 8 : 7, g = this.showWeeks ? 1 : 0, h = g; f > h; h++) a = (this.firstDayOfWeek + (h - g)) % 7, 
        this.ths[h].title = fa(a, !1), h > g ? (e = this.ths[h].getElementsByTagName("span")[0], 
        b(e), e.appendChild(document.createTextNode(fa(a, !0))), e.title = this.ths[h].title, 
        e = null) : (b(this.ths[h]), this.ths[h].appendChild(document.createTextNode(fa(a, !0)))), 
        d(this.ths[h], "date-picker-highlight"), this.highlightDays[a] && c(this.ths[h], "date-picker-highlight");
        this.created && this.updateTable();
    }, q.prototype.callback = function(a, b) {
        if (!(a && a in this.callbacks)) return !1;
        var c, d = !1;
        for (c = 0; c < this.callbacks[a].length; c++) d = this.callbacks[a][c](b || this.id);
        return d;
    }, q.prototype.showHideButtons = function(a) {
        if (this.butPrevYear) {
            var b = a.getMonth(), e = a.getFullYear();
            this.outOfRange(new Date(e - 1, b, ha(+b, e - 1), 5, 0, 0)) ? (c(this.butPrevYear, "fd-disabled"), 
            -1 == this.yearInc && this.stopTimer()) : d(this.butPrevYear, "fd-disabled"), this.outOfRange(new Date(e, +b - 1, ha(+b - 1, e), 5, 0, 0)) ? (c(this.butPrevMonth, "fd-disabled"), 
            -1 == this.monthInc && this.stopTimer()) : d(this.butPrevMonth, "fd-disabled"), 
            this.outOfRange(new Date(e + 1, +b, 1, 5, 0, 0)) ? (c(this.butNextYear, "fd-disabled"), 
            1 == this.yearInc && this.stopTimer()) : d(this.butNextYear, "fd-disabled"), this.outOfRange(new Date(e, +b + 1, 1, 5, 0, 0)) ? (c(this.butNextMonth, "fd-disabled"), 
            1 == this.monthInc && this.stopTimer()) : d(this.butNextMonth, "fd-disabled");
        }
    };
    var Z = {
        fullMonths: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
        monthAbbrs: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
        fullDays: [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ],
        dayAbbrs: [ "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" ],
        titles: [ "Previous month", "Next month", "Previous year", "Next year", "Today", "Show Calendar", "wk", "Week [[%0%]] of [[%1%]]", "Week", "Select a date", "Click & Drag to move", "Display “[[%0%]]” first", "Go to Today’s date", "Disabled date :" ],
        rtl: !1,
        firstDayOfWeek: 0,
        imported: !1
    }, $ = function() {
        var a, b;
        for (a in w) for (b in w[a].formElements) if (!document.getElementById(b)) {
            w[a].destroy(), w[a] = null, delete w[a];
            break;
        }
    }, _ = function(a) {
        var b;
        for (b in w) !w[b].created || a && a == w[b].id || w[b].hide();
    }, aa = function(a) {
        if (a in w) {
            if (!w[a].created || w[a].staticPos) return;
            w[a].hide();
        }
    }, ba = function(a, b) {
        return a in w ? (w[a].clickActivated = !b, w[a].show(b), !0) : !1;
    }, ca = function(b) {
        if (b = b || window.event, !b.persisted) {
            var c;
            for (c in w) w[c].destroy(), w[c] = null, delete w[c];
            w = null, k(window, "unload", a.destroy);
        }
    }, da = function(a) {
        a && a in w && (w[a].destroy(), w[a] = null, delete w[a]);
    }, ea = function(a, b) {
        if (b = b || [], F.titles.length > a) {
            var c = F.titles[a];
            if (b && b.length) for (var d = 0; d < b.length; d++) c = c.replace("[[%" + d + "%]]", b[d]);
            return c.replace(/[[%(\d)%]]/g, "");
        }
        return "";
    }, fa = function(a, b) {
        var c = F[b ? "dayAbbrs" : "fullDays"];
        return c.length && c.length > a ? c[a] : "";
    }, ga = function(a, b) {
        var c = F[b ? "monthAbbrs" : "fullMonths"];
        return c.length && c.length > a ? c[a] : "";
    }, ha = function(a, b) {
        return a = (a + 12) % 12, 0 != b % 4 || 0 == b % 100 && 0 != b % 400 || 1 != a ? [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ][a] : 29;
    }, ia = function(a) {
        if (a in x) return x[a];
        var b = new Date(a, 0, 4), c = new Date(a, 11, 28);
        return b.setDate(b.getDate() - (6 + b.getDay()) % 7), c.setDate(c.getDate() + (7 - c.getDay()) % 7), 
        x[a] = Math.round((c - b) / 6048e5), x[a];
    }, ja = function(a, b, c) {
        var d, c = new Date(a, b, c, 0, 0, 0), e = c.getDay();
        return c.setDate(c.getDate() - (e + 6) % 7 + 3), d = c.valueOf(), c.setMonth(0), 
        c.setDate(4), Math.round((d - c.valueOf()) / 6048e5) + 1;
    }, ka = function(a, b, c) {
        if (!a || isNaN(a)) return b;
        var d, e, f = a.getDate(), g = a.getDay(), h = a.getMonth(), j = a.getFullYear(), k = c ? F : Z, l = String(b).split(T), l = Y(b, T), m = [], n = {
            d: i(f),
            D: k.dayAbbrs[0 == g ? 6 : g - 1],
            l: k.fullDays[0 == g ? 6 : g - 1],
            j: f,
            N: 0 == g ? 7 : g,
            w: g,
            W: ja(j, h, f),
            M: k.monthAbbrs[h],
            F: k.fullMonths[h],
            m: i(h + 1),
            n: h + 1,
            t: ha(h, j),
            y: String(j).substr(2, 2),
            Y: j,
            S: [ "th", "st", "nd", "rd" ][f % 10 > 3 ? 0 : (f % 100 - f % 10 != 10) * f % 10]
        }, o = l.length;
        for (e = 0; o > e; e++) d = l[e], m.push(d in n ? n[d] : d);
        return m.join("");
    }, la = function(a, b) {
        var c, d, e, f = !1, g = !1, h = !1, i = -1 != b.search(O) ? 1 : 0, j = -1 != b.search(P) ? 1 : 0, k = -1 != b.search(Q) ? 1 : 0, l = (new Date(), 
        Y(b, T)), a = "" + a, m = l.length;
        a: for (c = 0; m > c; c++) if (d = l[c], "" !== d) {
            if (0 == a.length) break;
            switch (d) {
              case "/":
              case ".":
              case " ":
              case "-":
              case ",":
              case ":":
                a = a.substr(1);
                break;

              case "d":
                if (-1 != a.search(/^(3[01]|[12][0-9]|0[1-9])/)) {
                    f = a.substr(0, 2), a = a.substr(2);
                    break;
                }
                return !1;

              case "j":
                if (-1 != a.search(/^(3[01]|[12][0-9]|[1-9])/)) {
                    f = +a.match(/^(3[01]|[12][0-9]|[1-9])/)[0], a = a.substr(a.match(/^(3[01]|[12][0-9]|[1-9])/)[0].length);
                    break;
                }
                return !1;

              case "D":
              case "l":
                e = Z.fullDays.concat(Z.dayAbbrs), F.imported && (e = e.concat(F.fullDays).concat(F.dayAbbrs));
                for (var n = 0; n < e.length; n++) if (new RegExp("^" + e[n], "i").test(a)) {
                    a = a.substr(e[n].length);
                    continue a;
                }
                break;

              case "N":
              case "w":
                -1 != a.search("N" == d ? /^([1-7])/ : /^([0-6])/) && (a = a.substr(1));
                break;

              case "S":
                -1 != a.search(/^(st|nd|rd|th)/i) && (a = a.substr(2));
                break;

              case "W":
                -1 != a.search(/^([1-9]|[1234[0-9]|5[0-3])/) && (a = a.substr(a.match(/^([1-9]|[1234[0-9]|5[0-3])/)[0].length));
                break;

              case "M":
              case "F":
                e = Z.fullMonths.concat(Z.monthAbbrs), F.imported && (e = e.concat(F.fullMonths).concat(F.monthAbbrs));
                for (var n = 0; n < e.length; n++) if (-1 != a.search(new RegExp("^" + e[n], "i"))) {
                    a = a.substr(e[n].length), g = (n + 12) % 12 + 1;
                    continue a;
                }
                return !1;

              case "m":
                if (e = /^(1[012]|0[1-9])/, -1 != a.search(e)) {
                    g = +a.substr(0, 2), a = a.substr(2);
                    break;
                }
                return !1;

              case "n":
                if (e = /^(1[012]|[1-9])/, -1 != a.search(e)) {
                    g = +a.match(e)[0], a = a.substr(a.match(e)[0].length);
                    break;
                }
                return !1;

              case "t":
                if (-1 != a.search(/2[89]|3[01]/)) {
                    a = a.substr(2);
                    break;
                }
                return !1;

              case "Y":
                if (-1 != a.search(/^(\d{4})/)) {
                    h = a.substr(0, 4), a = a.substr(4);
                    break;
                }
                return !1;

              case "y":
                if (-1 != a.search(/^(0[0-9]|[1-9][0-9])/)) {
                    h = a.substr(0, 2), h = 50 > +h ? "20" + String(h) : "19" + String(h), a = a.substr(2);
                    break;
                }
                return !1;

              default:
                a = a.substr(d.length);
            }
        }
        return i && f === !1 || j && g === !1 || k && h === !1 ? !1 : i && j && k && +f > ha(+g - 1, +h) ? !1 : {
            d: i ? +f : !1,
            m: j ? +g : !1,
            y: k ? +h : !1
        };
    }, ma = function(a) {
        var b;
        if (a.parentNode && "label" == a.parentNode.tagName.toLowerCase()) b = a.parentNode; else for (var c = document.getElementsByTagName("label"), d = 0; d < c.length; d++) if (c[d].htmlFor && c[d].htmlFor == a.id || c[d].getAttribute("for") == a.id) {
            b = c[d];
            break;
        }
        return b && !b.id && a.id && (b.id = a.id + "_label"), b;
    }, na = function() {
        "object" == typeof pageSettings ? F = "object" == typeof pageSettings ? {
            titles: pageSettings.titles,
            fullMonths: pageSettings.fullMonths,
            monthAbbrs: pageSettings.monthAbbrs,
            fullDays: pageSettings.fullDays,
            dayAbbrs: pageSettings.dayAbbrs,
            firstDayOfWeek: "firstDayOfWeek" in pageSettings ? pageSettings.firstDayOfWeek : 0,
            rtl: "rtl" in pageSettings ? !!pageSettings.rtl : !1,
            imported: !0
        } : {
            titles: fdLocale.titles,
            fullMonths: fdLocale.fullMonths,
            monthAbbrs: fdLocale.monthAbbrs,
            fullDays: fdLocale.fullDays,
            dayAbbrs: fdLocale.dayAbbrs,
            firstDayOfWeek: "firstDayOfWeek" in fdLocale ? fdLocale.firstDayOfWeek : 0,
            rtl: "rtl" in fdLocale ? !!fdLocale.rtl : !1,
            imported: !0
        } : F || (F = Z);
    }, oa = function() {
        na();
        var a;
        for (a in w) w[a].created && w[a].updateTable();
    }, pa = function(a) {
        return !(!a || !a.tagName || ("input" != a.tagName.toLowerCase() || "text" != a.type && "hidden" != a.type) && "select" != a.tagName.toLowerCase());
    }, qa = function(a) {
        if (na(), null === A && (A = ta()), a.formElements) {
            a.id = a.id && a.id in a.formElements ? a.id : "", a.enabledDates = !1, a.disabledDates = !1;
            var b, c, d, e, f, g, h, j = {
                d: 0,
                m: 0,
                y: 0
            }, k = {}, l = !1, m = 0, n = 0;
            for (e in a.formElements) {
                if (f = document.getElementById(e), !pa(f)) {
                    if (r) throw "Element '" + e + "' is of the wrong type or does not exist within the DOM";
                    return !1;
                }
                if (!a.formElements[e].match(S)) {
                    if (r) throw "Element '" + e + "' has a date format that does not contain either a day (d|j), month (m|F|n) or year (y|Y) part: " + a.formElements[e];
                    return !1;
                }
                if (a.id || (a.id = e), k[e] = "select" == f.tagName ? f.selectedIndex || 0 : f.defaultValue, 
                b = {
                    value: a.formElements[e]
                }, b.d = -1 != b.value.search(O), b.m = -1 != b.value.search(P), b.y = -1 != b.value.search(Q), 
                b.d && j.d++, b.m && j.m++, b.y && j.y++, "select" == f.tagName.toLowerCase()) {
                    var o = f.options;
                    if (b.d && b.m && b.y) {
                        for (l = !1, a.enabledDates = {}, a.disabledDates = {}, h = 0; h < o.length; h++) g = la(o[h].value, b.value), 
                        g && g.y && g.m !== !1 && g.d && (d = g.y + "" + i(g.m) + i(g.d), l || (l = d), 
                        a.enabledDates[d] = 1, (!m || +m > +d) && (m = d), (!n || +d > +n) && (n = d));
                        !a.cursorDate && l && (a.cursorDate = l), a.disabledDates[m] = n;
                    } else if (b.m && b.y) {
                        for (h = 0; h < o.length; h++) g = la(o[h].value, b.value), g.y && g.m !== !1 && (d = g.y + "" + i(g.m), 
                        (!m || +m > +d) && (m = d), (!n || +d > +n) && (n = d));
                        m += "01", n += "" + ha(+n.substr(4, 2) - 1, +n.substr(0, 4));
                    } else if (b.y) {
                        for (h = 0; h < o.length; h++) g = la(o[h].value, b.value), g.y && ((!m || +g.y < +m) && (m = g.y), 
                        (!n || +g.y > +n) && (n = g.y));
                        m += "0101", n += "1231";
                    }
                }
            }
            if (1 != j.d || 1 != j.m || 1 != j.y) {
                if (r) throw "Could not find all of the required date parts within the date format for element: " + f.id;
                return !1;
            }
            a.rangeLow = p(a.rangeLow || !1), a.rangeHigh = p(a.rangeHigh || !1), a.cursorDate = p(a.cursorDate || !1), 
            a.startPeriod = p(a.startPeriod || !1), a.endPeriod = p(a.endPeriod || !1), m && (!a.rangeLow || +a.rangeLow < +m) && (a.rangeLow = m), 
            n && (!a.rangeHigh || +a.rangeHigh > +n) && (a.rangeHigh = n), c = {
                formElements: a.formElements,
                defaultVals: k,
                id: a.id,
                staticPos: !(!a.staticPos && !a.nopopup),
                wrapped: a.wrapped || !1,
                positioned: a.positioned && document.getElementById(a.positioned) ? a.positioned : "",
                rangeLow: a.rangeLow && -1 != String(a.rangeLow).search(U) ? a.rangeLow : "",
                rangeHigh: a.rangeHigh && -1 != String(a.rangeHigh).search(U) ? a.rangeHigh : "",
                statusFormat: a.statusFormat || M,
                noFadeEffect: a.staticPos ? !0 : !!a.noFadeEffect,
                dragDisabled: G || a.staticPos ? !0 : !!a.dragDisabled,
                bespokeTabIndex: a.bespokeTabindex && "number" == typeof a.bespokeTabindex ? parseInt(a.bespokeTabindex, 10) : 0,
                bespokeTitles: a.bespokeTitles || y || {},
                finalOpacity: a.finalOpacity && "number" == typeof a.finalOpacity && a.finalOpacity > 20 && a.finalOpacity <= 100 ? parseInt(+a.finalOpacity, 10) : a.staticPos ? 100 : z,
                hideInput: !!a.hideInput,
                noToday: !!a.noTodayButton,
                showWeeks: !!a.showWeeks,
                fillGrid: !!a.fillGrid,
                constrainSelection: "constrainSelection" in a ? !!a.constrainSelection : !0,
                cursorDate: a.cursorDate && -1 != String(a.cursorDate).search(U) ? a.cursorDate : "",
                labelledBy: ma(f),
                describedBy: a.describedBy && document.getElementById(a.describedBy) ? a.describedBy : t && document.getElementById(t) ? t : "",
                callbacks: a.callbackFunctions ? a.callbackFunctions : {},
                highlightDays: a.highlightDays && a.highlightDays.length && 7 == a.highlightDays.length ? a.highlightDays : [ 0, 0, 0, 0, 0, 1, 1 ],
                disabledDays: a.disabledDays && a.disabledDays.length && 7 == a.disabledDays.length ? a.disabledDays : [ 0, 0, 0, 0, 0, 0, 0 ],
                bespokeClass: a.bespokeClass ? " " + a.bespokeClass : ""
            }, w[a.id] = new q(c), "disabledDates" in a && a.disabledDates !== !1 && w[a.id].setDisabledDates(a.disabledDates), 
            "enabledDates" in a && a.enabledDates !== !1 && w[a.id].setEnabledDates(a.enabledDates), 
            w[a.id].callback("create", w[a.id].createCbArgObj());
        } else if (r) throw "No form elements stipulated within initialisation parameters";
    }, ra = function(a) {
        return a && a in w ? w[a].visible : !1;
    }, sa = function() {
        var a;
        for (a in w) w.hasOwnProperty(a) && w[a].changeHandler();
    }, ta = function() {
        var a, b = [ "Webkit", "Moz", "ms", "O" ], c = document.createElement("div"), d = [ "WebkitTransition", "transition", "OTransition", "MozTransition", "msTransition" ];
        for (a = 0; a < d.length; a++) if (d[a] in c.style) {
            B = "webkitTransition" == d[a] || "OTransition" == d[a] ? d[a] + "End" : "transitionend";
            break;
        }
        if (!B) return !1;
        if (c.style.animationName) return !0;
        for (var e = 0; e < b.length; e++) if (void 0 !== c.style[b[e] + "AnimationName"]) return !0;
        return !1;
    };
    return j(window, "unload", ca), j(window, "load", function() {
        setTimeout(sa, 0);
    }), X && c(document.documentElement, "oldie"), {
        addEvent: function(a, b, c) {
            return j(a, b, c);
        },
        removeEvent: function(a, b, c) {
            return k(a, b, c);
        },
        stopEvent: function(a) {
            return l(a);
        },
        show: function(a) {
            return ba(a, !1);
        },
        hide: function(a) {
            return aa(a);
        },
        createDatePicker: function(a) {
            qa(a);
        },
        destroyDatePicker: function(a) {
            da(a);
        },
        cleanUp: function() {
            $();
        },
        printFormattedDate: function(a, b, c) {
            return ka(a, b, c);
        },
        setDateFromInput: function(a) {
            return a && a in w ? void w[a].setDateFromInput() : !1;
        },
        setRangeLow: function(a, b) {
            return a && a in w ? void w[a].setRangeLow(p(b)) : !1;
        },
        setRangeHigh: function(a, b) {
            return a && a in w ? void w[a].setRangeHigh(p(b)) : !1;
        },
        setBespokeTitles: function(a, b) {
            return a && a in w ? void w[a].setBespokeTitles(b) : !1;
        },
        addBespokeTitles: function(a, b) {
            return a && a in w ? void w[a].addBespokeTitles(b) : !1;
        },
        parseDateString: function(a, b) {
            return la(a, b);
        },
        setGlobalOptions: function(a) {
            f(a);
        },
        setSelectedDate: function(a, b) {
            return a && a in w ? void w[a].setSelectedDate(p(b)) : !1;
        },
        dateValidForSelection: function(a, b) {
            return a && a in w ? w[a].canDateBeSelected(b) : !1;
        },
        addDisabledDates: function(a, b) {
            return a && a in w ? void w[a].addDisabledDates(b) : !1;
        },
        setDisabledDates: function(a, b) {
            return a && a in w ? void w[a].setDisabledDates(b) : !1;
        },
        addEnabledDates: function(a, b) {
            return a && a in w ? void w[a].addEnabledDates(b) : !1;
        },
        setEnabledDates: function(a, b) {
            return a && a in w ? void w[a].setEnabledDates(b) : !1;
        },
        setPeriodHighlight: function(a, b, c) {
            return a && a in w ? void w[a].setPeriodHighlight(p(b), p(c)) : !1;
        },
        disable: function(a) {
            return a && a in w ? void w[a].disableDatePicker() : !1;
        },
        enable: function(a) {
            return a && a in w ? void w[a].enableDatePicker() : !1;
        },
        setCursorDate: function(a, b) {
            return a && a in w ? void w[a].setCursorDate(p(b)) : !1;
        },
        getSelectedDate: function(a) {
            return a && a in w ? w[a].returnSelectedDate() : !1;
        },
        loadLanguage: function() {
            oa();
        },
        setDebug: function(a) {
            r = !!a;
        },
        dateToYYYYMMDDStr: function(a) {
            return p(a);
        }
    };
}();

!function(a) {
    function b(a) {
        var b = a.length, d = c.type(a);
        return "function" === d || c.isWindow(a) ? !1 : 1 === a.nodeType && b ? !0 : "array" === d || 0 === b || "number" == typeof b && b > 0 && b - 1 in a;
    }
    if (!a.jQuery) {
        var c = function(a, b) {
            return new c.fn.init(a, b);
        };
        c.isWindow = function(a) {
            return null != a && a == a.window;
        }, c.type = function(a) {
            return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? e[g.call(a)] || "object" : typeof a;
        }, c.isArray = Array.isArray || function(a) {
            return "array" === c.type(a);
        }, c.isPlainObject = function(a) {
            var b;
            if (!a || "object" !== c.type(a) || a.nodeType || c.isWindow(a)) return !1;
            try {
                if (a.constructor && !f.call(a, "constructor") && !f.call(a.constructor.prototype, "isPrototypeOf")) return !1;
            } catch (d) {
                return !1;
            }
            for (b in a) ;
            return void 0 === b || f.call(a, b);
        }, c.each = function(a, c, d) {
            var e, f = 0, g = a.length, h = b(a);
            if (d) {
                if (h) for (;g > f && (e = c.apply(a[f], d), e !== !1); f++) ; else for (f in a) if (e = c.apply(a[f], d), 
                e === !1) break;
            } else if (h) for (;g > f && (e = c.call(a[f], f, a[f]), e !== !1); f++) ; else for (f in a) if (e = c.call(a[f], f, a[f]), 
            e === !1) break;
            return a;
        }, c.data = function(a, b, e) {
            if (void 0 === e) {
                var f = a[c.expando], g = f && d[f];
                if (void 0 === b) return g;
                if (g && b in g) return g[b];
            } else if (void 0 !== b) {
                var f = a[c.expando] || (a[c.expando] = ++c.uuid);
                return d[f] = d[f] || {}, d[f][b] = e, e;
            }
        }, c.removeData = function(a, b) {
            var e = a[c.expando], f = e && d[e];
            f && c.each(b, function(a, b) {
                delete f[b];
            });
        }, c.extend = function() {
            var a, b, d, e, f, g, h = arguments[0] || {}, i = 1, j = arguments.length, k = !1;
            for ("boolean" == typeof h && (k = h, h = arguments[i] || {}, i++), "object" != typeof h && "function" !== c.type(h) && (h = {}), 
            i === j && (h = this, i--); j > i; i++) if (null != (f = arguments[i])) for (e in f) a = h[e], 
            d = f[e], h !== d && (k && d && (c.isPlainObject(d) || (b = c.isArray(d))) ? (b ? (b = !1, 
            g = a && c.isArray(a) ? a : []) : g = a && c.isPlainObject(a) ? a : {}, h[e] = c.extend(k, g, d)) : void 0 !== d && (h[e] = d));
            return h;
        }, c.queue = function(a, d, e) {
            function f(a, c) {
                var d = c || [];
                return null != a && (b(Object(a)) ? !function(a, b) {
                    for (var c = +b.length, d = 0, e = a.length; c > d; ) a[e++] = b[d++];
                    if (c !== c) for (;void 0 !== b[d]; ) a[e++] = b[d++];
                    return a.length = e, a;
                }(d, "string" == typeof a ? [ a ] : a) : [].push.call(d, a)), d;
            }
            if (a) {
                d = (d || "fx") + "queue";
                var g = c.data(a, d);
                return e ? (!g || c.isArray(e) ? g = c.data(a, d, f(e)) : g.push(e), g) : g || [];
            }
        }, c.dequeue = function(a, b) {
            c.each(a.nodeType ? [ a ] : a, function(a, d) {
                b = b || "fx";
                var e = c.queue(d, b), f = e.shift();
                "inprogress" === f && (f = e.shift()), f && ("fx" === b && e.unshift("inprogress"), 
                f.call(d, function() {
                    c.dequeue(d, b);
                }));
            });
        }, c.fn = c.prototype = {
            init: function(a) {
                if (a.nodeType) return this[0] = a, this;
                throw new Error("Not a DOM node.");
            },
            offset: function() {
                var b = this[0].getBoundingClientRect ? this[0].getBoundingClientRect() : {
                    top: 0,
                    left: 0
                };
                return {
                    top: b.top + (a.pageYOffset || document.scrollTop || 0) - (document.clientTop || 0),
                    left: b.left + (a.pageXOffset || document.scrollLeft || 0) - (document.clientLeft || 0)
                };
            },
            position: function() {
                function a() {
                    for (var a = this.offsetParent || document; a && "html" === !a.nodeType.toLowerCase && "static" === a.style.position; ) a = a.offsetParent;
                    return a || document;
                }
                var b = this[0], a = a.apply(b), d = this.offset(), e = /^(?:body|html)$/i.test(a.nodeName) ? {
                    top: 0,
                    left: 0
                } : c(a).offset();
                return d.top -= parseFloat(b.style.marginTop) || 0, d.left -= parseFloat(b.style.marginLeft) || 0, 
                a.style && (e.top += parseFloat(a.style.borderTopWidth) || 0, e.left += parseFloat(a.style.borderLeftWidth) || 0), 
                {
                    top: d.top - e.top,
                    left: d.left - e.left
                };
            }
        };
        var d = {};
        c.expando = "velocity" + new Date().getTime(), c.uuid = 0;
        for (var e = {}, f = e.hasOwnProperty, g = e.toString, h = "Boolean Number String Function Array Date RegExp Object Error".split(" "), i = 0; i < h.length; i++) e["[object " + h[i] + "]"] = h[i].toLowerCase();
        c.fn.init.prototype = c.fn, a.Velocity = {
            Utilities: c
        };
    }
}(window), function(a) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = a() : "function" == typeof define && define.amd ? define(a) : a();
}(function() {
    return function(a, b, c, d) {
        function e(a) {
            for (var b = -1, c = a ? a.length : 0, d = []; ++b < c; ) {
                var e = a[b];
                e && d.push(e);
            }
            return d;
        }
        function f(a) {
            return p.isWrapped(a) ? a = [].slice.call(a) : p.isNode(a) && (a = [ a ]), a;
        }
        function g(a) {
            var b = m.data(a, "velocity");
            return null === b ? d : b;
        }
        function h(a) {
            return function(b) {
                return Math.round(b * a) * (1 / a);
            };
        }
        function i(a, c, d, e) {
            function f(a, b) {
                return 1 - 3 * b + 3 * a;
            }
            function g(a, b) {
                return 3 * b - 6 * a;
            }
            function h(a) {
                return 3 * a;
            }
            function i(a, b, c) {
                return ((f(b, c) * a + g(b, c)) * a + h(b)) * a;
            }
            function j(a, b, c) {
                return 3 * f(b, c) * a * a + 2 * g(b, c) * a + h(b);
            }
            function k(b, c) {
                for (var e = 0; p > e; ++e) {
                    var f = j(c, a, d);
                    if (0 === f) return c;
                    var g = i(c, a, d) - b;
                    c -= g / f;
                }
                return c;
            }
            function l() {
                for (var b = 0; t > b; ++b) x[b] = i(b * u, a, d);
            }
            function m(b, c, e) {
                var f, g, h = 0;
                do g = c + (e - c) / 2, f = i(g, a, d) - b, f > 0 ? e = g : c = g; while (Math.abs(f) > r && ++h < s);
                return g;
            }
            function n(b) {
                for (var c = 0, e = 1, f = t - 1; e != f && x[e] <= b; ++e) c += u;
                --e;
                var g = (b - x[e]) / (x[e + 1] - x[e]), h = c + g * u, i = j(h, a, d);
                return i >= q ? k(b, h) : 0 == i ? h : m(b, c, c + u);
            }
            function o() {
                y = !0, (a != c || d != e) && l();
            }
            var p = 4, q = .001, r = 1e-7, s = 10, t = 11, u = 1 / (t - 1), v = "Float32Array" in b;
            if (4 !== arguments.length) return !1;
            for (var w = 0; 4 > w; ++w) if ("number" != typeof arguments[w] || isNaN(arguments[w]) || !isFinite(arguments[w])) return !1;
            a = Math.min(a, 1), d = Math.min(d, 1), a = Math.max(a, 0), d = Math.max(d, 0);
            var x = v ? new Float32Array(t) : new Array(t), y = !1, z = function(b) {
                return y || o(), a === c && d === e ? b : 0 === b ? 0 : 1 === b ? 1 : i(n(b), c, e);
            };
            z.getControlPoints = function() {
                return [ {
                    x: a,
                    y: c
                }, {
                    x: d,
                    y: e
                } ];
            };
            var A = "generateBezier(" + [ a, c, d, e ] + ")";
            return z.toString = function() {
                return A;
            }, z;
        }
        function j(a, b) {
            var c = a;
            return p.isString(a) ? t.Easings[a] || (c = !1) : c = p.isArray(a) && 1 === a.length ? h.apply(null, a) : p.isArray(a) && 2 === a.length ? u.apply(null, a.concat([ b ])) : p.isArray(a) && 4 === a.length ? i.apply(null, a) : !1, 
            c === !1 && (c = t.Easings[t.defaults.easing] ? t.defaults.easing : s), c;
        }
        function k(a) {
            if (a) {
                var b = new Date().getTime(), c = t.State.calls.length;
                c > 1e4 && (t.State.calls = e(t.State.calls));
                for (var f = 0; c > f; f++) if (t.State.calls[f]) {
                    var h = t.State.calls[f], i = h[0], j = h[2], n = h[3], o = !!n, q = null;
                    n || (n = t.State.calls[f][3] = b - 16);
                    for (var r = Math.min((b - n) / j.duration, 1), s = 0, u = i.length; u > s; s++) {
                        var w = i[s], y = w.element;
                        if (g(y)) {
                            var z = !1;
                            if (j.display !== d && null !== j.display && "none" !== j.display) {
                                if ("flex" === j.display) {
                                    var A = [ "-webkit-box", "-moz-box", "-ms-flexbox", "-webkit-flex" ];
                                    m.each(A, function(a, b) {
                                        v.setPropertyValue(y, "display", b);
                                    });
                                }
                                v.setPropertyValue(y, "display", j.display);
                            }
                            j.visibility !== d && "hidden" !== j.visibility && v.setPropertyValue(y, "visibility", j.visibility);
                            for (var B in w) if ("element" !== B) {
                                var C, D = w[B], E = p.isString(D.easing) ? t.Easings[D.easing] : D.easing;
                                if (1 === r) C = D.endValue; else {
                                    var F = D.endValue - D.startValue;
                                    if (C = D.startValue + F * E(r, j, F), !o && C === D.currentValue) continue;
                                }
                                if (D.currentValue = C, "tween" === B) q = C; else {
                                    if (v.Hooks.registered[B]) {
                                        var G = v.Hooks.getRoot(B), H = g(y).rootPropertyValueCache[G];
                                        H && (D.rootPropertyValue = H);
                                    }
                                    var I = v.setPropertyValue(y, B, D.currentValue + (0 === parseFloat(C) ? "" : D.unitType), D.rootPropertyValue, D.scrollData);
                                    v.Hooks.registered[B] && (g(y).rootPropertyValueCache[G] = v.Normalizations.registered[G] ? v.Normalizations.registered[G]("extract", null, I[1]) : I[1]), 
                                    "transform" === I[0] && (z = !0);
                                }
                            }
                            j.mobileHA && g(y).transformCache.translate3d === d && (g(y).transformCache.translate3d = "(0px, 0px, 0px)", 
                            z = !0), z && v.flushTransformCache(y);
                        }
                    }
                    j.display !== d && "none" !== j.display && (t.State.calls[f][2].display = !1), j.visibility !== d && "hidden" !== j.visibility && (t.State.calls[f][2].visibility = !1), 
                    j.progress && j.progress.call(h[1], h[1], r, Math.max(0, n + j.duration - b), n, q), 
                    1 === r && l(f);
                }
            }
            t.State.isTicking && x(k);
        }
        function l(a, b) {
            if (!t.State.calls[a]) return !1;
            for (var c = t.State.calls[a][0], e = t.State.calls[a][1], f = t.State.calls[a][2], h = t.State.calls[a][4], i = !1, j = 0, k = c.length; k > j; j++) {
                var l = c[j].element;
                if (b || f.loop || ("none" === f.display && v.setPropertyValue(l, "display", f.display), 
                "hidden" === f.visibility && v.setPropertyValue(l, "visibility", f.visibility)), 
                f.loop !== !0 && (m.queue(l)[1] === d || !/\.velocityQueueEntryFlag/i.test(m.queue(l)[1])) && g(l)) {
                    g(l).isAnimating = !1, g(l).rootPropertyValueCache = {};
                    var n = !1;
                    m.each(v.Lists.transforms3D, function(a, b) {
                        var c = /^scale/.test(b) ? 1 : 0, e = g(l).transformCache[b];
                        g(l).transformCache[b] !== d && new RegExp("^\\(" + c + "[^.]").test(e) && (n = !0, 
                        delete g(l).transformCache[b]);
                    }), f.mobileHA && (n = !0, delete g(l).transformCache.translate3d), n && v.flushTransformCache(l), 
                    v.Values.removeClass(l, "velocity-animating");
                }
                if (!b && f.complete && !f.loop && j === k - 1) try {
                    f.complete.call(e, e);
                } catch (o) {
                    setTimeout(function() {
                        throw o;
                    }, 1);
                }
                h && f.loop !== !0 && h(e), g(l) && f.loop === !0 && !b && (m.each(g(l).tweensContainer, function(a, b) {
                    /^rotate/.test(a) && 360 === parseFloat(b.endValue) && (b.endValue = 0, b.startValue = 360), 
                    /^backgroundPosition/.test(a) && 100 === parseFloat(b.endValue) && "%" === b.unitType && (b.endValue = 0, 
                    b.startValue = 100);
                }), t(l, "reverse", {
                    loop: !0,
                    delay: f.delay
                })), f.queue !== !1 && m.dequeue(l, f.queue);
            }
            t.State.calls[a] = !1;
            for (var p = 0, q = t.State.calls.length; q > p; p++) if (t.State.calls[p] !== !1) {
                i = !0;
                break;
            }
            i === !1 && (t.State.isTicking = !1, delete t.State.calls, t.State.calls = []);
        }
        var m, n = function() {
            if (c.documentMode) return c.documentMode;
            for (var a = 7; a > 4; a--) {
                var b = c.createElement("div");
                if (b.innerHTML = "<!--[if IE " + a + "]><span></span><![endif]-->", b.getElementsByTagName("span").length) return b = null, 
                a;
            }
            return d;
        }(), o = function() {
            var a = 0;
            return b.webkitRequestAnimationFrame || b.mozRequestAnimationFrame || function(b) {
                var c, d = new Date().getTime();
                return c = Math.max(0, 16 - (d - a)), a = d + c, setTimeout(function() {
                    b(d + c);
                }, c);
            };
        }(), p = {
            isString: function(a) {
                return "string" == typeof a;
            },
            isArray: Array.isArray || function(a) {
                return "[object Array]" === Object.prototype.toString.call(a);
            },
            isFunction: function(a) {
                return "[object Function]" === Object.prototype.toString.call(a);
            },
            isNode: function(a) {
                return a && a.nodeType;
            },
            isNodeList: function(a) {
                return "object" == typeof a && /^\[object (HTMLCollection|NodeList|Object)\]$/.test(Object.prototype.toString.call(a)) && a.length !== d && (0 === a.length || "object" == typeof a[0] && a[0].nodeType > 0);
            },
            isWrapped: function(a) {
                return a && (a.jquery || b.Zepto && b.Zepto.zepto.isZ(a));
            },
            isSVG: function(a) {
                return b.SVGElement && a instanceof b.SVGElement;
            },
            isEmptyObject: function(a) {
                for (var b in a) return !1;
                return !0;
            }
        }, q = !1;
        if (a.fn && a.fn.jquery ? (m = a, q = !0) : m = b.Velocity.Utilities, 8 >= n && !q) throw new Error("Velocity: IE8 and below require jQuery to be loaded before Velocity.");
        if (7 >= n) return void (jQuery.fn.velocity = jQuery.fn.animate);
        var r = 400, s = "swing", t = {
            State: {
                isMobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
                isAndroid: /Android/i.test(navigator.userAgent),
                isGingerbread: /Android 2\.3\.[3-7]/i.test(navigator.userAgent),
                isChrome: b.chrome,
                isFirefox: /Firefox/i.test(navigator.userAgent),
                prefixElement: c.createElement("div"),
                prefixMatches: {},
                scrollAnchor: null,
                scrollPropertyLeft: null,
                scrollPropertyTop: null,
                isTicking: !1,
                calls: []
            },
            CSS: {},
            Utilities: m,
            Redirects: {},
            Easings: {},
            Promise: b.Promise,
            defaults: {
                queue: "",
                duration: r,
                easing: s,
                begin: d,
                complete: d,
                progress: d,
                display: d,
                visibility: d,
                loop: !1,
                delay: !1,
                mobileHA: !0,
                _cacheValues: !0
            },
            init: function(a) {
                m.data(a, "velocity", {
                    isSVG: p.isSVG(a),
                    isAnimating: !1,
                    computedStyle: null,
                    tweensContainer: null,
                    rootPropertyValueCache: {},
                    transformCache: {}
                });
            },
            hook: null,
            mock: !1,
            version: {
                major: 1,
                minor: 2,
                patch: 2
            },
            debug: !1
        };
        b.pageYOffset !== d ? (t.State.scrollAnchor = b, t.State.scrollPropertyLeft = "pageXOffset", 
        t.State.scrollPropertyTop = "pageYOffset") : (t.State.scrollAnchor = c.documentElement || c.body.parentNode || c.body, 
        t.State.scrollPropertyLeft = "scrollLeft", t.State.scrollPropertyTop = "scrollTop");
        var u = function() {
            function a(a) {
                return -a.tension * a.x - a.friction * a.v;
            }
            function b(b, c, d) {
                var e = {
                    x: b.x + d.dx * c,
                    v: b.v + d.dv * c,
                    tension: b.tension,
                    friction: b.friction
                };
                return {
                    dx: e.v,
                    dv: a(e)
                };
            }
            function c(c, d) {
                var e = {
                    dx: c.v,
                    dv: a(c)
                }, f = b(c, .5 * d, e), g = b(c, .5 * d, f), h = b(c, d, g), i = 1 / 6 * (e.dx + 2 * (f.dx + g.dx) + h.dx), j = 1 / 6 * (e.dv + 2 * (f.dv + g.dv) + h.dv);
                return c.x = c.x + i * d, c.v = c.v + j * d, c;
            }
            return function d(a, b, e) {
                var f, g, h, i = {
                    x: -1,
                    v: 0,
                    tension: null,
                    friction: null
                }, j = [ 0 ], k = 0, l = 1e-4, m = .016;
                for (a = parseFloat(a) || 500, b = parseFloat(b) || 20, e = e || null, i.tension = a, 
                i.friction = b, f = null !== e, f ? (k = d(a, b), g = k / e * m) : g = m; h = c(h || i, g), 
                j.push(1 + h.x), k += 16, Math.abs(h.x) > l && Math.abs(h.v) > l; ) ;
                return f ? function(a) {
                    return j[a * (j.length - 1) | 0];
                } : k;
            };
        }();
        t.Easings = {
            linear: function(a) {
                return a;
            },
            swing: function(a) {
                return .5 - Math.cos(a * Math.PI) / 2;
            },
            spring: function(a) {
                return 1 - Math.cos(4.5 * a * Math.PI) * Math.exp(6 * -a);
            }
        }, m.each([ [ "ease", [ .25, .1, .25, 1 ] ], [ "ease-in", [ .42, 0, 1, 1 ] ], [ "ease-out", [ 0, 0, .58, 1 ] ], [ "ease-in-out", [ .42, 0, .58, 1 ] ], [ "easeInSine", [ .47, 0, .745, .715 ] ], [ "easeOutSine", [ .39, .575, .565, 1 ] ], [ "easeInOutSine", [ .445, .05, .55, .95 ] ], [ "easeInQuad", [ .55, .085, .68, .53 ] ], [ "easeOutQuad", [ .25, .46, .45, .94 ] ], [ "easeInOutQuad", [ .455, .03, .515, .955 ] ], [ "easeInCubic", [ .55, .055, .675, .19 ] ], [ "easeOutCubic", [ .215, .61, .355, 1 ] ], [ "easeInOutCubic", [ .645, .045, .355, 1 ] ], [ "easeInQuart", [ .895, .03, .685, .22 ] ], [ "easeOutQuart", [ .165, .84, .44, 1 ] ], [ "easeInOutQuart", [ .77, 0, .175, 1 ] ], [ "easeInQuint", [ .755, .05, .855, .06 ] ], [ "easeOutQuint", [ .23, 1, .32, 1 ] ], [ "easeInOutQuint", [ .86, 0, .07, 1 ] ], [ "easeInExpo", [ .95, .05, .795, .035 ] ], [ "easeOutExpo", [ .19, 1, .22, 1 ] ], [ "easeInOutExpo", [ 1, 0, 0, 1 ] ], [ "easeInCirc", [ .6, .04, .98, .335 ] ], [ "easeOutCirc", [ .075, .82, .165, 1 ] ], [ "easeInOutCirc", [ .785, .135, .15, .86 ] ] ], function(a, b) {
            t.Easings[b[0]] = i.apply(null, b[1]);
        });
        var v = t.CSS = {
            RegEx: {
                isHex: /^#([A-f\d]{3}){1,2}$/i,
                valueUnwrap: /^[A-z]+\((.*)\)$/i,
                wrappedValueAlreadyExtracted: /[0-9.]+ [0-9.]+ [0-9.]+( [0-9.]+)?/,
                valueSplit: /([A-z]+\(.+\))|(([A-z0-9#-.]+?)(?=\s|$))/gi
            },
            Lists: {
                colors: [ "fill", "stroke", "stopColor", "color", "backgroundColor", "borderColor", "borderTopColor", "borderRightColor", "borderBottomColor", "borderLeftColor", "outlineColor" ],
                transformsBase: [ "translateX", "translateY", "scale", "scaleX", "scaleY", "skewX", "skewY", "rotateZ" ],
                transforms3D: [ "transformPerspective", "translateZ", "scaleZ", "rotateX", "rotateY" ]
            },
            Hooks: {
                templates: {
                    textShadow: [ "Color X Y Blur", "black 0px 0px 0px" ],
                    boxShadow: [ "Color X Y Blur Spread", "black 0px 0px 0px 0px" ],
                    clip: [ "Top Right Bottom Left", "0px 0px 0px 0px" ],
                    backgroundPosition: [ "X Y", "0% 0%" ],
                    transformOrigin: [ "X Y Z", "50% 50% 0px" ],
                    perspectiveOrigin: [ "X Y", "50% 50%" ]
                },
                registered: {},
                register: function() {
                    for (var a = 0; a < v.Lists.colors.length; a++) {
                        var b = "color" === v.Lists.colors[a] ? "0 0 0 1" : "255 255 255 1";
                        v.Hooks.templates[v.Lists.colors[a]] = [ "Red Green Blue Alpha", b ];
                    }
                    var c, d, e;
                    if (n) for (c in v.Hooks.templates) {
                        d = v.Hooks.templates[c], e = d[0].split(" ");
                        var f = d[1].match(v.RegEx.valueSplit);
                        "Color" === e[0] && (e.push(e.shift()), f.push(f.shift()), v.Hooks.templates[c] = [ e.join(" "), f.join(" ") ]);
                    }
                    for (c in v.Hooks.templates) {
                        d = v.Hooks.templates[c], e = d[0].split(" ");
                        for (var a in e) {
                            var g = c + e[a], h = a;
                            v.Hooks.registered[g] = [ c, h ];
                        }
                    }
                },
                getRoot: function(a) {
                    var b = v.Hooks.registered[a];
                    return b ? b[0] : a;
                },
                cleanRootPropertyValue: function(a, b) {
                    return v.RegEx.valueUnwrap.test(b) && (b = b.match(v.RegEx.valueUnwrap)[1]), v.Values.isCSSNullValue(b) && (b = v.Hooks.templates[a][1]), 
                    b;
                },
                extractValue: function(a, b) {
                    var c = v.Hooks.registered[a];
                    if (c) {
                        var d = c[0], e = c[1];
                        return b = v.Hooks.cleanRootPropertyValue(d, b), b.toString().match(v.RegEx.valueSplit)[e];
                    }
                    return b;
                },
                injectValue: function(a, b, c) {
                    var d = v.Hooks.registered[a];
                    if (d) {
                        var e, f, g = d[0], h = d[1];
                        return c = v.Hooks.cleanRootPropertyValue(g, c), e = c.toString().match(v.RegEx.valueSplit), 
                        e[h] = b, f = e.join(" ");
                    }
                    return c;
                }
            },
            Normalizations: {
                registered: {
                    clip: function(a, b, c) {
                        switch (a) {
                          case "name":
                            return "clip";

                          case "extract":
                            var d;
                            return v.RegEx.wrappedValueAlreadyExtracted.test(c) ? d = c : (d = c.toString().match(v.RegEx.valueUnwrap), 
                            d = d ? d[1].replace(/,(\s+)?/g, " ") : c), d;

                          case "inject":
                            return "rect(" + c + ")";
                        }
                    },
                    blur: function(a, b, c) {
                        switch (a) {
                          case "name":
                            return t.State.isFirefox ? "filter" : "-webkit-filter";

                          case "extract":
                            var d = parseFloat(c);
                            if (!d && 0 !== d) {
                                var e = c.toString().match(/blur\(([0-9]+[A-z]+)\)/i);
                                d = e ? e[1] : 0;
                            }
                            return d;

                          case "inject":
                            return parseFloat(c) ? "blur(" + c + ")" : "none";
                        }
                    },
                    opacity: function(a, b, c) {
                        if (8 >= n) switch (a) {
                          case "name":
                            return "filter";

                          case "extract":
                            var d = c.toString().match(/alpha\(opacity=(.*)\)/i);
                            return c = d ? d[1] / 100 : 1;

                          case "inject":
                            return b.style.zoom = 1, parseFloat(c) >= 1 ? "" : "alpha(opacity=" + parseInt(100 * parseFloat(c), 10) + ")";
                        } else switch (a) {
                          case "name":
                            return "opacity";

                          case "extract":
                            return c;

                          case "inject":
                            return c;
                        }
                    }
                },
                register: function() {
                    9 >= n || t.State.isGingerbread || (v.Lists.transformsBase = v.Lists.transformsBase.concat(v.Lists.transforms3D));
                    for (var a = 0; a < v.Lists.transformsBase.length; a++) !function() {
                        var b = v.Lists.transformsBase[a];
                        v.Normalizations.registered[b] = function(a, c, e) {
                            switch (a) {
                              case "name":
                                return "transform";

                              case "extract":
                                return g(c) === d || g(c).transformCache[b] === d ? /^scale/i.test(b) ? 1 : 0 : g(c).transformCache[b].replace(/[()]/g, "");

                              case "inject":
                                var f = !1;
                                switch (b.substr(0, b.length - 1)) {
                                  case "translate":
                                    f = !/(%|px|em|rem|vw|vh|\d)$/i.test(e);
                                    break;

                                  case "scal":
                                  case "scale":
                                    t.State.isAndroid && g(c).transformCache[b] === d && 1 > e && (e = 1), f = !/(\d)$/i.test(e);
                                    break;

                                  case "skew":
                                    f = !/(deg|\d)$/i.test(e);
                                    break;

                                  case "rotate":
                                    f = !/(deg|\d)$/i.test(e);
                                }
                                return f || (g(c).transformCache[b] = "(" + e + ")"), g(c).transformCache[b];
                            }
                        };
                    }();
                    for (var a = 0; a < v.Lists.colors.length; a++) !function() {
                        var b = v.Lists.colors[a];
                        v.Normalizations.registered[b] = function(a, c, e) {
                            switch (a) {
                              case "name":
                                return b;

                              case "extract":
                                var f;
                                if (v.RegEx.wrappedValueAlreadyExtracted.test(e)) f = e; else {
                                    var g, h = {
                                        black: "rgb(0, 0, 0)",
                                        blue: "rgb(0, 0, 255)",
                                        gray: "rgb(128, 128, 128)",
                                        green: "rgb(0, 128, 0)",
                                        red: "rgb(255, 0, 0)",
                                        white: "rgb(255, 255, 255)"
                                    };
                                    /^[A-z]+$/i.test(e) ? g = h[e] !== d ? h[e] : h.black : v.RegEx.isHex.test(e) ? g = "rgb(" + v.Values.hexToRgb(e).join(" ") + ")" : /^rgba?\(/i.test(e) || (g = h.black), 
                                    f = (g || e).toString().match(v.RegEx.valueUnwrap)[1].replace(/,(\s+)?/g, " ");
                                }
                                return 8 >= n || 3 !== f.split(" ").length || (f += " 1"), f;

                              case "inject":
                                return 8 >= n ? 4 === e.split(" ").length && (e = e.split(/\s+/).slice(0, 3).join(" ")) : 3 === e.split(" ").length && (e += " 1"), 
                                (8 >= n ? "rgb" : "rgba") + "(" + e.replace(/\s+/g, ",").replace(/\.(\d)+(?=,)/g, "") + ")";
                            }
                        };
                    }();
                }
            },
            Names: {
                camelCase: function(a) {
                    return a.replace(/-(\w)/g, function(a, b) {
                        return b.toUpperCase();
                    });
                },
                SVGAttribute: function(a) {
                    var b = "width|height|x|y|cx|cy|r|rx|ry|x1|x2|y1|y2";
                    return (n || t.State.isAndroid && !t.State.isChrome) && (b += "|transform"), new RegExp("^(" + b + ")$", "i").test(a);
                },
                prefixCheck: function(a) {
                    if (t.State.prefixMatches[a]) return [ t.State.prefixMatches[a], !0 ];
                    for (var b = [ "", "Webkit", "Moz", "ms", "O" ], c = 0, d = b.length; d > c; c++) {
                        var e;
                        if (e = 0 === c ? a : b[c] + a.replace(/^\w/, function(a) {
                            return a.toUpperCase();
                        }), p.isString(t.State.prefixElement.style[e])) return t.State.prefixMatches[a] = e, 
                        [ e, !0 ];
                    }
                    return [ a, !1 ];
                }
            },
            Values: {
                hexToRgb: function(a) {
                    var b, c = /^#?([a-f\d])([a-f\d])([a-f\d])$/i, d = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;
                    return a = a.replace(c, function(a, b, c, d) {
                        return b + b + c + c + d + d;
                    }), b = d.exec(a), b ? [ parseInt(b[1], 16), parseInt(b[2], 16), parseInt(b[3], 16) ] : [ 0, 0, 0 ];
                },
                isCSSNullValue: function(a) {
                    return 0 == a || /^(none|auto|transparent|(rgba\(0, ?0, ?0, ?0\)))$/i.test(a);
                },
                getUnitType: function(a) {
                    return /^(rotate|skew)/i.test(a) ? "deg" : /(^(scale|scaleX|scaleY|scaleZ|alpha|flexGrow|flexHeight|zIndex|fontWeight)$)|((opacity|red|green|blue|alpha)$)/i.test(a) ? "" : "px";
                },
                getDisplayType: function(a) {
                    var b = a && a.tagName.toString().toLowerCase();
                    return /^(b|big|i|small|tt|abbr|acronym|cite|code|dfn|em|kbd|strong|samp|var|a|bdo|br|img|map|object|q|script|span|sub|sup|button|input|label|select|textarea)$/i.test(b) ? "inline" : /^(li)$/i.test(b) ? "list-item" : /^(tr)$/i.test(b) ? "table-row" : /^(table)$/i.test(b) ? "table" : /^(tbody)$/i.test(b) ? "table-row-group" : "block";
                },
                addClass: function(a, b) {
                    a.classList ? a.classList.add(b) : a.className += (a.className.length ? " " : "") + b;
                },
                removeClass: function(a, b) {
                    a.classList ? a.classList.remove(b) : a.className = a.className.toString().replace(new RegExp("(^|\\s)" + b.split(" ").join("|") + "(\\s|$)", "gi"), " ");
                }
            },
            getPropertyValue: function(a, c, e, f) {
                function h(a, c) {
                    function e() {
                        j && v.setPropertyValue(a, "display", "none");
                    }
                    var i = 0;
                    if (8 >= n) i = m.css(a, c); else {
                        var j = !1;
                        if (/^(width|height)$/.test(c) && 0 === v.getPropertyValue(a, "display") && (j = !0, 
                        v.setPropertyValue(a, "display", v.Values.getDisplayType(a))), !f) {
                            if ("height" === c && "border-box" !== v.getPropertyValue(a, "boxSizing").toString().toLowerCase()) {
                                var k = a.offsetHeight - (parseFloat(v.getPropertyValue(a, "borderTopWidth")) || 0) - (parseFloat(v.getPropertyValue(a, "borderBottomWidth")) || 0) - (parseFloat(v.getPropertyValue(a, "paddingTop")) || 0) - (parseFloat(v.getPropertyValue(a, "paddingBottom")) || 0);
                                return e(), k;
                            }
                            if ("width" === c && "border-box" !== v.getPropertyValue(a, "boxSizing").toString().toLowerCase()) {
                                var l = a.offsetWidth - (parseFloat(v.getPropertyValue(a, "borderLeftWidth")) || 0) - (parseFloat(v.getPropertyValue(a, "borderRightWidth")) || 0) - (parseFloat(v.getPropertyValue(a, "paddingLeft")) || 0) - (parseFloat(v.getPropertyValue(a, "paddingRight")) || 0);
                                return e(), l;
                            }
                        }
                        var o;
                        o = g(a) === d ? b.getComputedStyle(a, null) : g(a).computedStyle ? g(a).computedStyle : g(a).computedStyle = b.getComputedStyle(a, null), 
                        "borderColor" === c && (c = "borderTopColor"), i = 9 === n && "filter" === c ? o.getPropertyValue(c) : o[c], 
                        ("" === i || null === i) && (i = a.style[c]), e();
                    }
                    if ("auto" === i && /^(top|right|bottom|left)$/i.test(c)) {
                        var p = h(a, "position");
                        ("fixed" === p || "absolute" === p && /top|left/i.test(c)) && (i = m(a).position()[c] + "px");
                    }
                    return i;
                }
                var i;
                if (v.Hooks.registered[c]) {
                    var j = c, k = v.Hooks.getRoot(j);
                    e === d && (e = v.getPropertyValue(a, v.Names.prefixCheck(k)[0])), v.Normalizations.registered[k] && (e = v.Normalizations.registered[k]("extract", a, e)), 
                    i = v.Hooks.extractValue(j, e);
                } else if (v.Normalizations.registered[c]) {
                    var l, o;
                    l = v.Normalizations.registered[c]("name", a), "transform" !== l && (o = h(a, v.Names.prefixCheck(l)[0]), 
                    v.Values.isCSSNullValue(o) && v.Hooks.templates[c] && (o = v.Hooks.templates[c][1])), 
                    i = v.Normalizations.registered[c]("extract", a, o);
                }
                if (!/^[\d-]/.test(i)) if (g(a) && g(a).isSVG && v.Names.SVGAttribute(c)) if (/^(height|width)$/i.test(c)) try {
                    i = a.getBBox()[c];
                } catch (p) {
                    i = 0;
                } else i = a.getAttribute(c); else i = h(a, v.Names.prefixCheck(c)[0]);
                return v.Values.isCSSNullValue(i) && (i = 0), t.debug >= 2 && console.log("Get " + c + ": " + i), 
                i;
            },
            setPropertyValue: function(a, c, d, e, f) {
                var h = c;
                if ("scroll" === c) f.container ? f.container["scroll" + f.direction] = d : "Left" === f.direction ? b.scrollTo(d, f.alternateValue) : b.scrollTo(f.alternateValue, d); else if (v.Normalizations.registered[c] && "transform" === v.Normalizations.registered[c]("name", a)) v.Normalizations.registered[c]("inject", a, d), 
                h = "transform", d = g(a).transformCache[c]; else {
                    if (v.Hooks.registered[c]) {
                        var i = c, j = v.Hooks.getRoot(c);
                        e = e || v.getPropertyValue(a, j), d = v.Hooks.injectValue(i, d, e), c = j;
                    }
                    if (v.Normalizations.registered[c] && (d = v.Normalizations.registered[c]("inject", a, d), 
                    c = v.Normalizations.registered[c]("name", a)), h = v.Names.prefixCheck(c)[0], 8 >= n) try {
                        a.style[h] = d;
                    } catch (k) {
                        t.debug && console.log("Browser does not support [" + d + "] for [" + h + "]");
                    } else g(a) && g(a).isSVG && v.Names.SVGAttribute(c) ? a.setAttribute(c, d) : a.style[h] = d;
                    t.debug >= 2 && console.log("Set " + c + " (" + h + "): " + d);
                }
                return [ h, d ];
            },
            flushTransformCache: function(a) {
                function b(b) {
                    return parseFloat(v.getPropertyValue(a, b));
                }
                var c = "";
                if ((n || t.State.isAndroid && !t.State.isChrome) && g(a).isSVG) {
                    var d = {
                        translate: [ b("translateX"), b("translateY") ],
                        skewX: [ b("skewX") ],
                        skewY: [ b("skewY") ],
                        scale: 1 !== b("scale") ? [ b("scale"), b("scale") ] : [ b("scaleX"), b("scaleY") ],
                        rotate: [ b("rotateZ"), 0, 0 ]
                    };
                    m.each(g(a).transformCache, function(a) {
                        /^translate/i.test(a) ? a = "translate" : /^scale/i.test(a) ? a = "scale" : /^rotate/i.test(a) && (a = "rotate"), 
                        d[a] && (c += a + "(" + d[a].join(" ") + ") ", delete d[a]);
                    });
                } else {
                    var e, f;
                    m.each(g(a).transformCache, function(b) {
                        return e = g(a).transformCache[b], "transformPerspective" === b ? (f = e, !0) : (9 === n && "rotateZ" === b && (b = "rotate"), 
                        void (c += b + e + " "));
                    }), f && (c = "perspective" + f + " " + c);
                }
                v.setPropertyValue(a, "transform", c);
            }
        };
        v.Hooks.register(), v.Normalizations.register(), t.hook = function(a, b, c) {
            var e = d;
            return a = f(a), m.each(a, function(a, f) {
                if (g(f) === d && t.init(f), c === d) e === d && (e = t.CSS.getPropertyValue(f, b)); else {
                    var h = t.CSS.setPropertyValue(f, b, c);
                    "transform" === h[0] && t.CSS.flushTransformCache(f), e = h;
                }
            }), e;
        };
        var w = function() {
            function a() {
                return h ? B.promise || null : i;
            }
            function e() {
                function a(a) {
                    function l(a, b) {
                        var c = d, e = d, g = d;
                        return p.isArray(a) ? (c = a[0], !p.isArray(a[1]) && /^[\d-]/.test(a[1]) || p.isFunction(a[1]) || v.RegEx.isHex.test(a[1]) ? g = a[1] : (p.isString(a[1]) && !v.RegEx.isHex.test(a[1]) || p.isArray(a[1])) && (e = b ? a[1] : j(a[1], h.duration), 
                        a[2] !== d && (g = a[2]))) : c = a, b || (e = e || h.easing), p.isFunction(c) && (c = c.call(f, y, x)), 
                        p.isFunction(g) && (g = g.call(f, y, x)), [ c || 0, e, g ];
                    }
                    function n(a, b) {
                        var c, d;
                        return d = (b || "0").toString().toLowerCase().replace(/[%A-z]+$/, function(a) {
                            return c = a, "";
                        }), c || (c = v.Values.getUnitType(a)), [ d, c ];
                    }
                    function r() {
                        var a = {
                            myParent: f.parentNode || c.body,
                            position: v.getPropertyValue(f, "position"),
                            fontSize: v.getPropertyValue(f, "fontSize")
                        }, d = a.position === I.lastPosition && a.myParent === I.lastParent, e = a.fontSize === I.lastFontSize;
                        I.lastParent = a.myParent, I.lastPosition = a.position, I.lastFontSize = a.fontSize;
                        var h = 100, i = {};
                        if (e && d) i.emToPx = I.lastEmToPx, i.percentToPxWidth = I.lastPercentToPxWidth, 
                        i.percentToPxHeight = I.lastPercentToPxHeight; else {
                            var j = g(f).isSVG ? c.createElementNS("http://www.w3.org/2000/svg", "rect") : c.createElement("div");
                            t.init(j), a.myParent.appendChild(j), m.each([ "overflow", "overflowX", "overflowY" ], function(a, b) {
                                t.CSS.setPropertyValue(j, b, "hidden");
                            }), t.CSS.setPropertyValue(j, "position", a.position), t.CSS.setPropertyValue(j, "fontSize", a.fontSize), 
                            t.CSS.setPropertyValue(j, "boxSizing", "content-box"), m.each([ "minWidth", "maxWidth", "width", "minHeight", "maxHeight", "height" ], function(a, b) {
                                t.CSS.setPropertyValue(j, b, h + "%");
                            }), t.CSS.setPropertyValue(j, "paddingLeft", h + "em"), i.percentToPxWidth = I.lastPercentToPxWidth = (parseFloat(v.getPropertyValue(j, "width", null, !0)) || 1) / h, 
                            i.percentToPxHeight = I.lastPercentToPxHeight = (parseFloat(v.getPropertyValue(j, "height", null, !0)) || 1) / h, 
                            i.emToPx = I.lastEmToPx = (parseFloat(v.getPropertyValue(j, "paddingLeft")) || 1) / h, 
                            a.myParent.removeChild(j);
                        }
                        return null === I.remToPx && (I.remToPx = parseFloat(v.getPropertyValue(c.body, "fontSize")) || 16), 
                        null === I.vwToPx && (I.vwToPx = parseFloat(b.innerWidth) / 100, I.vhToPx = parseFloat(b.innerHeight) / 100), 
                        i.remToPx = I.remToPx, i.vwToPx = I.vwToPx, i.vhToPx = I.vhToPx, t.debug >= 1 && console.log("Unit ratios: " + JSON.stringify(i), f), 
                        i;
                    }
                    if (h.begin && 0 === y) try {
                        h.begin.call(o, o);
                    } catch (u) {
                        setTimeout(function() {
                            throw u;
                        }, 1);
                    }
                    if ("scroll" === C) {
                        var w, z, A, D = /^x$/i.test(h.axis) ? "Left" : "Top", E = parseFloat(h.offset) || 0;
                        h.container ? p.isWrapped(h.container) || p.isNode(h.container) ? (h.container = h.container[0] || h.container, 
                        w = h.container["scroll" + D], A = w + m(f).position()[D.toLowerCase()] + E) : h.container = null : (w = t.State.scrollAnchor[t.State["scrollProperty" + D]], 
                        z = t.State.scrollAnchor[t.State["scrollProperty" + ("Left" === D ? "Top" : "Left")]], 
                        A = m(f).offset()[D.toLowerCase()] + E), i = {
                            scroll: {
                                rootPropertyValue: !1,
                                startValue: w,
                                currentValue: w,
                                endValue: A,
                                unitType: "",
                                easing: h.easing,
                                scrollData: {
                                    container: h.container,
                                    direction: D,
                                    alternateValue: z
                                }
                            },
                            element: f
                        }, t.debug && console.log("tweensContainer (scroll): ", i.scroll, f);
                    } else if ("reverse" === C) {
                        if (!g(f).tweensContainer) return void m.dequeue(f, h.queue);
                        "none" === g(f).opts.display && (g(f).opts.display = "auto"), "hidden" === g(f).opts.visibility && (g(f).opts.visibility = "visible"), 
                        g(f).opts.loop = !1, g(f).opts.begin = null, g(f).opts.complete = null, s.easing || delete h.easing, 
                        s.duration || delete h.duration, h = m.extend({}, g(f).opts, h);
                        var F = m.extend(!0, {}, g(f).tweensContainer);
                        for (var G in F) if ("element" !== G) {
                            var H = F[G].startValue;
                            F[G].startValue = F[G].currentValue = F[G].endValue, F[G].endValue = H, p.isEmptyObject(s) || (F[G].easing = h.easing), 
                            t.debug && console.log("reverse tweensContainer (" + G + "): " + JSON.stringify(F[G]), f);
                        }
                        i = F;
                    } else if ("start" === C) {
                        var F;
                        g(f).tweensContainer && g(f).isAnimating === !0 && (F = g(f).tweensContainer), m.each(q, function(a, b) {
                            if (RegExp("^" + v.Lists.colors.join("$|^") + "$").test(a)) {
                                var c = l(b, !0), e = c[0], f = c[1], g = c[2];
                                if (v.RegEx.isHex.test(e)) {
                                    for (var h = [ "Red", "Green", "Blue" ], i = v.Values.hexToRgb(e), j = g ? v.Values.hexToRgb(g) : d, k = 0; k < h.length; k++) {
                                        var m = [ i[k] ];
                                        f && m.push(f), j !== d && m.push(j[k]), q[a + h[k]] = m;
                                    }
                                    delete q[a];
                                }
                            }
                        });
                        for (var K in q) {
                            var L = l(q[K]), M = L[0], N = L[1], O = L[2];
                            K = v.Names.camelCase(K);
                            var P = v.Hooks.getRoot(K), Q = !1;
                            if (g(f).isSVG || "tween" === P || v.Names.prefixCheck(P)[1] !== !1 || v.Normalizations.registered[P] !== d) {
                                (h.display !== d && null !== h.display && "none" !== h.display || h.visibility !== d && "hidden" !== h.visibility) && /opacity|filter/.test(K) && !O && 0 !== M && (O = 0), 
                                h._cacheValues && F && F[K] ? (O === d && (O = F[K].endValue + F[K].unitType), Q = g(f).rootPropertyValueCache[P]) : v.Hooks.registered[K] ? O === d ? (Q = v.getPropertyValue(f, P), 
                                O = v.getPropertyValue(f, K, Q)) : Q = v.Hooks.templates[P][1] : O === d && (O = v.getPropertyValue(f, K));
                                var R, S, T, U = !1;
                                if (R = n(K, O), O = R[0], T = R[1], R = n(K, M), M = R[0].replace(/^([+-\/*])=/, function(a, b) {
                                    return U = b, "";
                                }), S = R[1], O = parseFloat(O) || 0, M = parseFloat(M) || 0, "%" === S && (/^(fontSize|lineHeight)$/.test(K) ? (M /= 100, 
                                S = "em") : /^scale/.test(K) ? (M /= 100, S = "") : /(Red|Green|Blue)$/i.test(K) && (M = M / 100 * 255, 
                                S = "")), /[\/*]/.test(U)) S = T; else if (T !== S && 0 !== O) if (0 === M) S = T; else {
                                    e = e || r();
                                    var V = /margin|padding|left|right|width|text|word|letter/i.test(K) || /X$/.test(K) || "x" === K ? "x" : "y";
                                    switch (T) {
                                      case "%":
                                        O *= "x" === V ? e.percentToPxWidth : e.percentToPxHeight;
                                        break;

                                      case "px":
                                        break;

                                      default:
                                        O *= e[T + "ToPx"];
                                    }
                                    switch (S) {
                                      case "%":
                                        O *= 1 / ("x" === V ? e.percentToPxWidth : e.percentToPxHeight);
                                        break;

                                      case "px":
                                        break;

                                      default:
                                        O *= 1 / e[S + "ToPx"];
                                    }
                                }
                                switch (U) {
                                  case "+":
                                    M = O + M;
                                    break;

                                  case "-":
                                    M = O - M;
                                    break;

                                  case "*":
                                    M = O * M;
                                    break;

                                  case "/":
                                    M = O / M;
                                }
                                i[K] = {
                                    rootPropertyValue: Q,
                                    startValue: O,
                                    currentValue: O,
                                    endValue: M,
                                    unitType: S,
                                    easing: N
                                }, t.debug && console.log("tweensContainer (" + K + "): " + JSON.stringify(i[K]), f);
                            } else t.debug && console.log("Skipping [" + P + "] due to a lack of browser support.");
                        }
                        i.element = f;
                    }
                    i.element && (v.Values.addClass(f, "velocity-animating"), J.push(i), "" === h.queue && (g(f).tweensContainer = i, 
                    g(f).opts = h), g(f).isAnimating = !0, y === x - 1 ? (t.State.calls.push([ J, o, h, null, B.resolver ]), 
                    t.State.isTicking === !1 && (t.State.isTicking = !0, k())) : y++);
                }
                var e, f = this, h = m.extend({}, t.defaults, s), i = {};
                switch (g(f) === d && t.init(f), parseFloat(h.delay) && h.queue !== !1 && m.queue(f, h.queue, function(a) {
                    t.velocityQueueEntryFlag = !0, g(f).delayTimer = {
                        setTimeout: setTimeout(a, parseFloat(h.delay)),
                        next: a
                    };
                }), h.duration.toString().toLowerCase()) {
                  case "fast":
                    h.duration = 200;
                    break;

                  case "normal":
                    h.duration = r;
                    break;

                  case "slow":
                    h.duration = 600;
                    break;

                  default:
                    h.duration = parseFloat(h.duration) || 1;
                }
                t.mock !== !1 && (t.mock === !0 ? h.duration = h.delay = 1 : (h.duration *= parseFloat(t.mock) || 1, 
                h.delay *= parseFloat(t.mock) || 1)), h.easing = j(h.easing, h.duration), h.begin && !p.isFunction(h.begin) && (h.begin = null), 
                h.progress && !p.isFunction(h.progress) && (h.progress = null), h.complete && !p.isFunction(h.complete) && (h.complete = null), 
                h.display !== d && null !== h.display && (h.display = h.display.toString().toLowerCase(), 
                "auto" === h.display && (h.display = t.CSS.Values.getDisplayType(f))), h.visibility !== d && null !== h.visibility && (h.visibility = h.visibility.toString().toLowerCase()), 
                h.mobileHA = h.mobileHA && t.State.isMobile && !t.State.isGingerbread, h.queue === !1 ? h.delay ? setTimeout(a, h.delay) : a() : m.queue(f, h.queue, function(b, c) {
                    return c === !0 ? (B.promise && B.resolver(o), !0) : (t.velocityQueueEntryFlag = !0, 
                    void a(b));
                }), "" !== h.queue && "fx" !== h.queue || "inprogress" === m.queue(f)[0] || m.dequeue(f);
            }
            var h, i, n, o, q, s, u = arguments[0] && (arguments[0].p || m.isPlainObject(arguments[0].properties) && !arguments[0].properties.names || p.isString(arguments[0].properties));
            if (p.isWrapped(this) ? (h = !1, n = 0, o = this, i = this) : (h = !0, n = 1, o = u ? arguments[0].elements || arguments[0].e : arguments[0]), 
            o = f(o)) {
                u ? (q = arguments[0].properties || arguments[0].p, s = arguments[0].options || arguments[0].o) : (q = arguments[n], 
                s = arguments[n + 1]);
                var x = o.length, y = 0;
                if (!/^(stop|finish)$/i.test(q) && !m.isPlainObject(s)) {
                    var z = n + 1;
                    s = {};
                    for (var A = z; A < arguments.length; A++) p.isArray(arguments[A]) || !/^(fast|normal|slow)$/i.test(arguments[A]) && !/^\d/.test(arguments[A]) ? p.isString(arguments[A]) || p.isArray(arguments[A]) ? s.easing = arguments[A] : p.isFunction(arguments[A]) && (s.complete = arguments[A]) : s.duration = arguments[A];
                }
                var B = {
                    promise: null,
                    resolver: null,
                    rejecter: null
                };
                h && t.Promise && (B.promise = new t.Promise(function(a, b) {
                    B.resolver = a, B.rejecter = b;
                }));
                var C;
                switch (q) {
                  case "scroll":
                    C = "scroll";
                    break;

                  case "reverse":
                    C = "reverse";
                    break;

                  case "finish":
                  case "stop":
                    m.each(o, function(a, b) {
                        g(b) && g(b).delayTimer && (clearTimeout(g(b).delayTimer.setTimeout), g(b).delayTimer.next && g(b).delayTimer.next(), 
                        delete g(b).delayTimer);
                    });
                    var D = [];
                    return m.each(t.State.calls, function(a, b) {
                        b && m.each(b[1], function(c, e) {
                            var f = s === d ? "" : s;
                            return f === !0 || b[2].queue === f || s === d && b[2].queue === !1 ? void m.each(o, function(c, d) {
                                d === e && ((s === !0 || p.isString(s)) && (m.each(m.queue(d, p.isString(s) ? s : ""), function(a, b) {
                                    p.isFunction(b) && b(null, !0);
                                }), m.queue(d, p.isString(s) ? s : "", [])), "stop" === q ? (g(d) && g(d).tweensContainer && f !== !1 && m.each(g(d).tweensContainer, function(a, b) {
                                    b.endValue = b.currentValue;
                                }), D.push(a)) : "finish" === q && (b[2].duration = 1));
                            }) : !0;
                        });
                    }), "stop" === q && (m.each(D, function(a, b) {
                        l(b, !0);
                    }), B.promise && B.resolver(o)), a();

                  default:
                    if (!m.isPlainObject(q) || p.isEmptyObject(q)) {
                        if (p.isString(q) && t.Redirects[q]) {
                            var E = m.extend({}, s), F = E.duration, G = E.delay || 0;
                            return E.backwards === !0 && (o = m.extend(!0, [], o).reverse()), m.each(o, function(a, b) {
                                parseFloat(E.stagger) ? E.delay = G + parseFloat(E.stagger) * a : p.isFunction(E.stagger) && (E.delay = G + E.stagger.call(b, a, x)), 
                                E.drag && (E.duration = parseFloat(F) || (/^(callout|transition)/.test(q) ? 1e3 : r), 
                                E.duration = Math.max(E.duration * (E.backwards ? 1 - a / x : (a + 1) / x), .75 * E.duration, 200)), 
                                t.Redirects[q].call(b, b, E || {}, a, x, o, B.promise ? B : d);
                            }), a();
                        }
                        var H = "Velocity: First argument (" + q + ") was not a property map, a known action, or a registered redirect. Aborting.";
                        return B.promise ? B.rejecter(new Error(H)) : console.log(H), a();
                    }
                    C = "start";
                }
                var I = {
                    lastParent: null,
                    lastPosition: null,
                    lastFontSize: null,
                    lastPercentToPxWidth: null,
                    lastPercentToPxHeight: null,
                    lastEmToPx: null,
                    remToPx: null,
                    vwToPx: null,
                    vhToPx: null
                }, J = [];
                m.each(o, function(a, b) {
                    p.isNode(b) && e.call(b);
                });
                var K, E = m.extend({}, t.defaults, s);
                if (E.loop = parseInt(E.loop), K = 2 * E.loop - 1, E.loop) for (var L = 0; K > L; L++) {
                    var M = {
                        delay: E.delay,
                        progress: E.progress
                    };
                    L === K - 1 && (M.display = E.display, M.visibility = E.visibility, M.complete = E.complete), 
                    w(o, "reverse", M);
                }
                return a();
            }
        };
        t = m.extend(w, t), t.animate = w;
        var x = b.requestAnimationFrame || o;
        return t.State.isMobile || c.hidden === d || c.addEventListener("visibilitychange", function() {
            c.hidden ? (x = function(a) {
                return setTimeout(function() {
                    a(!0);
                }, 16);
            }, k()) : x = b.requestAnimationFrame || o;
        }), a.Velocity = t, a !== b && (a.fn.velocity = w, a.fn.velocity.defaults = t.defaults), 
        m.each([ "Down", "Up" ], function(a, b) {
            t.Redirects["slide" + b] = function(a, c, e, f, g, h) {
                var i = m.extend({}, c), j = i.begin, k = i.complete, l = {
                    height: "",
                    marginTop: "",
                    marginBottom: "",
                    paddingTop: "",
                    paddingBottom: ""
                }, n = {};
                i.display === d && (i.display = "Down" === b ? "inline" === t.CSS.Values.getDisplayType(a) ? "inline-block" : "block" : "none"), 
                i.begin = function() {
                    j && j.call(g, g);
                    for (var c in l) {
                        n[c] = a.style[c];
                        var d = t.CSS.getPropertyValue(a, c);
                        l[c] = "Down" === b ? [ d, 0 ] : [ 0, d ];
                    }
                    n.overflow = a.style.overflow, a.style.overflow = "hidden";
                }, i.complete = function() {
                    for (var b in n) a.style[b] = n[b];
                    k && k.call(g, g), h && h.resolver(g);
                }, t(a, l, i);
            };
        }), m.each([ "In", "Out" ], function(a, b) {
            t.Redirects["fade" + b] = function(a, c, e, f, g, h) {
                var i = m.extend({}, c), j = {
                    opacity: "In" === b ? 1 : 0
                }, k = i.complete;
                i.complete = e !== f - 1 ? i.begin = null : function() {
                    k && k.call(g, g), h && h.resolver(g);
                }, i.display === d && (i.display = "In" === b ? "auto" : "none"), t(this, j, i);
            };
        }), t;
    }(window.jQuery || window.Zepto || window, window, document);
}), !function(a) {
    "function" == typeof require && "object" == typeof exports ? module.exports = a() : "function" == typeof define && define.amd ? define([ "velocity" ], a) : a();
}(function() {
    return function(a, b, c, d) {
        function e(a, b) {
            var c = [];
            return a && b ? (g.each([ a, b ], function(a, b) {
                var d = [];
                g.each(b, function(a, b) {
                    for (;b.toString().length < 5; ) b = "0" + b;
                    d.push(b);
                }), c.push(d.join(""));
            }), parseFloat(c[0]) > parseFloat(c[1])) : !1;
        }
        if (!a.Velocity || !a.Velocity.Utilities) return void (b.console && console.log("Velocity UI Pack: Velocity must be loaded first. Aborting."));
        var f = a.Velocity, g = f.Utilities, h = f.version, i = {
            major: 1,
            minor: 1,
            patch: 0
        };
        if (e(i, h)) {
            var j = "Velocity UI Pack: You need to update Velocity (jquery.velocity.js) to a newer version. Visit http://github.com/julianshapiro/velocity.";
            throw alert(j), new Error(j);
        }
        f.RegisterEffect = f.RegisterUI = function(a, b) {
            function c(a, b, c, d) {
                var e, h = 0;
                g.each(a.nodeType ? [ a ] : a, function(a, b) {
                    d && (c += a * d), e = b.parentNode, g.each([ "height", "paddingTop", "paddingBottom", "marginTop", "marginBottom" ], function(a, c) {
                        h += parseFloat(f.CSS.getPropertyValue(b, c));
                    });
                }), f.animate(e, {
                    height: ("In" === b ? "+" : "-") + "=" + h
                }, {
                    queue: !1,
                    easing: "ease-in-out",
                    duration: c * ("In" === b ? .6 : 1)
                });
            }
            return f.Redirects[a] = function(e, h, i, j, k, l) {
                function m() {
                    h.display !== d && "none" !== h.display || !/Out$/.test(a) || g.each(k.nodeType ? [ k ] : k, function(a, b) {
                        f.CSS.setPropertyValue(b, "display", "none");
                    }), h.complete && h.complete.call(k, k), l && l.resolver(k || e);
                }
                var n = i === j - 1;
                b.defaultDuration = "function" == typeof b.defaultDuration ? b.defaultDuration.call(k, k) : parseFloat(b.defaultDuration);
                for (var o = 0; o < b.calls.length; o++) {
                    var p = b.calls[o], q = p[0], r = h.duration || b.defaultDuration || 1e3, s = p[1], t = p[2] || {}, u = {};
                    if (u.duration = r * (s || 1), u.queue = h.queue || "", u.easing = t.easing || "ease", 
                    u.delay = parseFloat(t.delay) || 0, u._cacheValues = t._cacheValues || !0, 0 === o) {
                        if (u.delay += parseFloat(h.delay) || 0, 0 === i && (u.begin = function() {
                            h.begin && h.begin.call(k, k);
                            var b = a.match(/(In|Out)$/);
                            b && "In" === b[0] && q.opacity !== d && g.each(k.nodeType ? [ k ] : k, function(a, b) {
                                f.CSS.setPropertyValue(b, "opacity", 0);
                            }), h.animateParentHeight && b && c(k, b[0], r + u.delay, h.stagger);
                        }), null !== h.display) if (h.display !== d && "none" !== h.display) u.display = h.display; else if (/In$/.test(a)) {
                            var v = f.CSS.Values.getDisplayType(e);
                            u.display = "inline" === v ? "inline-block" : v;
                        }
                        h.visibility && "hidden" !== h.visibility && (u.visibility = h.visibility);
                    }
                    o === b.calls.length - 1 && (u.complete = function() {
                        if (b.reset) {
                            for (var a in b.reset) {
                                var c = b.reset[a];
                                f.CSS.Hooks.registered[a] !== d || "string" != typeof c && "number" != typeof c || (b.reset[a] = [ b.reset[a], b.reset[a] ]);
                            }
                            var g = {
                                duration: 0,
                                queue: !1
                            };
                            n && (g.complete = m), f.animate(e, b.reset, g);
                        } else n && m();
                    }, "hidden" === h.visibility && (u.visibility = h.visibility)), f.animate(e, q, u);
                }
            }, f;
        }, f.RegisterEffect.packagedEffects = {
            "callout.bounce": {
                defaultDuration: 550,
                calls: [ [ {
                    translateY: -30
                }, .25 ], [ {
                    translateY: 0
                }, .125 ], [ {
                    translateY: -15
                }, .125 ], [ {
                    translateY: 0
                }, .25 ] ]
            },
            "callout.shake": {
                defaultDuration: 800,
                calls: [ [ {
                    translateX: -11
                }, .125 ], [ {
                    translateX: 11
                }, .125 ], [ {
                    translateX: -11
                }, .125 ], [ {
                    translateX: 11
                }, .125 ], [ {
                    translateX: -11
                }, .125 ], [ {
                    translateX: 11
                }, .125 ], [ {
                    translateX: -11
                }, .125 ], [ {
                    translateX: 0
                }, .125 ] ]
            },
            "callout.flash": {
                defaultDuration: 1100,
                calls: [ [ {
                    opacity: [ 0, "easeInOutQuad", 1 ]
                }, .25 ], [ {
                    opacity: [ 1, "easeInOutQuad" ]
                }, .25 ], [ {
                    opacity: [ 0, "easeInOutQuad" ]
                }, .25 ], [ {
                    opacity: [ 1, "easeInOutQuad" ]
                }, .25 ] ]
            },
            "callout.pulse": {
                defaultDuration: 825,
                calls: [ [ {
                    scaleX: 1.1,
                    scaleY: 1.1
                }, .5, {
                    easing: "easeInExpo"
                } ], [ {
                    scaleX: 1,
                    scaleY: 1
                }, .5 ] ]
            },
            "callout.swing": {
                defaultDuration: 950,
                calls: [ [ {
                    rotateZ: 15
                }, .2 ], [ {
                    rotateZ: -10
                }, .2 ], [ {
                    rotateZ: 5
                }, .2 ], [ {
                    rotateZ: -5
                }, .2 ], [ {
                    rotateZ: 0
                }, .2 ] ]
            },
            "callout.tada": {
                defaultDuration: 1e3,
                calls: [ [ {
                    scaleX: .9,
                    scaleY: .9,
                    rotateZ: -3
                }, .1 ], [ {
                    scaleX: 1.1,
                    scaleY: 1.1,
                    rotateZ: 3
                }, .1 ], [ {
                    scaleX: 1.1,
                    scaleY: 1.1,
                    rotateZ: -3
                }, .1 ], [ "reverse", .125 ], [ "reverse", .125 ], [ "reverse", .125 ], [ "reverse", .125 ], [ "reverse", .125 ], [ {
                    scaleX: 1,
                    scaleY: 1,
                    rotateZ: 0
                }, .2 ] ]
            },
            "transition.fadeIn": {
                defaultDuration: 500,
                calls: [ [ {
                    opacity: [ 1, 0 ]
                } ] ]
            },
            "transition.fadeOut": {
                defaultDuration: 500,
                calls: [ [ {
                    opacity: [ 0, 1 ]
                } ] ]
            },
            "transition.flipXIn": {
                defaultDuration: 700,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    transformPerspective: [ 800, 800 ],
                    rotateY: [ 0, -55 ]
                } ] ],
                reset: {
                    transformPerspective: 0
                }
            },
            "transition.flipXOut": {
                defaultDuration: 700,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    transformPerspective: [ 800, 800 ],
                    rotateY: 55
                } ] ],
                reset: {
                    transformPerspective: 0,
                    rotateY: 0
                }
            },
            "transition.flipYIn": {
                defaultDuration: 800,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    transformPerspective: [ 800, 800 ],
                    rotateX: [ 0, -45 ]
                } ] ],
                reset: {
                    transformPerspective: 0
                }
            },
            "transition.flipYOut": {
                defaultDuration: 800,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    transformPerspective: [ 800, 800 ],
                    rotateX: 25
                } ] ],
                reset: {
                    transformPerspective: 0,
                    rotateX: 0
                }
            },
            "transition.flipBounceXIn": {
                defaultDuration: 900,
                calls: [ [ {
                    opacity: [ .725, 0 ],
                    transformPerspective: [ 400, 400 ],
                    rotateY: [ -10, 90 ]
                }, .5 ], [ {
                    opacity: .8,
                    rotateY: 10
                }, .25 ], [ {
                    opacity: 1,
                    rotateY: 0
                }, .25 ] ],
                reset: {
                    transformPerspective: 0
                }
            },
            "transition.flipBounceXOut": {
                defaultDuration: 800,
                calls: [ [ {
                    opacity: [ .9, 1 ],
                    transformPerspective: [ 400, 400 ],
                    rotateY: -10
                }, .5 ], [ {
                    opacity: 0,
                    rotateY: 90
                }, .5 ] ],
                reset: {
                    transformPerspective: 0,
                    rotateY: 0
                }
            },
            "transition.flipBounceYIn": {
                defaultDuration: 850,
                calls: [ [ {
                    opacity: [ .725, 0 ],
                    transformPerspective: [ 400, 400 ],
                    rotateX: [ -10, 90 ]
                }, .5 ], [ {
                    opacity: .8,
                    rotateX: 10
                }, .25 ], [ {
                    opacity: 1,
                    rotateX: 0
                }, .25 ] ],
                reset: {
                    transformPerspective: 0
                }
            },
            "transition.flipBounceYOut": {
                defaultDuration: 800,
                calls: [ [ {
                    opacity: [ .9, 1 ],
                    transformPerspective: [ 400, 400 ],
                    rotateX: -15
                }, .5 ], [ {
                    opacity: 0,
                    rotateX: 90
                }, .5 ] ],
                reset: {
                    transformPerspective: 0,
                    rotateX: 0
                }
            },
            "transition.swoopIn": {
                defaultDuration: 850,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    transformOriginX: [ "100%", "50%" ],
                    transformOriginY: [ "100%", "100%" ],
                    scaleX: [ 1, 0 ],
                    scaleY: [ 1, 0 ],
                    translateX: [ 0, -700 ],
                    translateZ: 0
                } ] ],
                reset: {
                    transformOriginX: "50%",
                    transformOriginY: "50%"
                }
            },
            "transition.swoopOut": {
                defaultDuration: 850,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    transformOriginX: [ "50%", "100%" ],
                    transformOriginY: [ "100%", "100%" ],
                    scaleX: 0,
                    scaleY: 0,
                    translateX: -700,
                    translateZ: 0
                } ] ],
                reset: {
                    transformOriginX: "50%",
                    transformOriginY: "50%",
                    scaleX: 1,
                    scaleY: 1,
                    translateX: 0
                }
            },
            "transition.whirlIn": {
                defaultDuration: 850,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    transformOriginX: [ "50%", "50%" ],
                    transformOriginY: [ "50%", "50%" ],
                    scaleX: [ 1, 0 ],
                    scaleY: [ 1, 0 ],
                    rotateY: [ 0, 160 ]
                }, 1, {
                    easing: "easeInOutSine"
                } ] ]
            },
            "transition.whirlOut": {
                defaultDuration: 750,
                calls: [ [ {
                    opacity: [ 0, "easeInOutQuint", 1 ],
                    transformOriginX: [ "50%", "50%" ],
                    transformOriginY: [ "50%", "50%" ],
                    scaleX: 0,
                    scaleY: 0,
                    rotateY: 160
                }, 1, {
                    easing: "swing"
                } ] ],
                reset: {
                    scaleX: 1,
                    scaleY: 1,
                    rotateY: 0
                }
            },
            "transition.shrinkIn": {
                defaultDuration: 750,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    transformOriginX: [ "50%", "50%" ],
                    transformOriginY: [ "50%", "50%" ],
                    scaleX: [ 1, 1.5 ],
                    scaleY: [ 1, 1.5 ],
                    translateZ: 0
                } ] ]
            },
            "transition.shrinkOut": {
                defaultDuration: 600,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    transformOriginX: [ "50%", "50%" ],
                    transformOriginY: [ "50%", "50%" ],
                    scaleX: 1.3,
                    scaleY: 1.3,
                    translateZ: 0
                } ] ],
                reset: {
                    scaleX: 1,
                    scaleY: 1
                }
            },
            "transition.expandIn": {
                defaultDuration: 700,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    transformOriginX: [ "50%", "50%" ],
                    transformOriginY: [ "50%", "50%" ],
                    scaleX: [ 1, .625 ],
                    scaleY: [ 1, .625 ],
                    translateZ: 0
                } ] ]
            },
            "transition.expandOut": {
                defaultDuration: 700,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    transformOriginX: [ "50%", "50%" ],
                    transformOriginY: [ "50%", "50%" ],
                    scaleX: .5,
                    scaleY: .5,
                    translateZ: 0
                } ] ],
                reset: {
                    scaleX: 1,
                    scaleY: 1
                }
            },
            "transition.bounceIn": {
                defaultDuration: 800,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    scaleX: [ 1.05, .3 ],
                    scaleY: [ 1.05, .3 ]
                }, .4 ], [ {
                    scaleX: .9,
                    scaleY: .9,
                    translateZ: 0
                }, .2 ], [ {
                    scaleX: 1,
                    scaleY: 1
                }, .5 ] ]
            },
            "transition.bounceOut": {
                defaultDuration: 800,
                calls: [ [ {
                    scaleX: .95,
                    scaleY: .95
                }, .35 ], [ {
                    scaleX: 1.1,
                    scaleY: 1.1,
                    translateZ: 0
                }, .35 ], [ {
                    opacity: [ 0, 1 ],
                    scaleX: .3,
                    scaleY: .3
                }, .3 ] ],
                reset: {
                    scaleX: 1,
                    scaleY: 1
                }
            },
            "transition.bounceUpIn": {
                defaultDuration: 800,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    translateY: [ -30, 1e3 ]
                }, .6, {
                    easing: "easeOutCirc"
                } ], [ {
                    translateY: 10
                }, .2 ], [ {
                    translateY: 0
                }, .2 ] ]
            },
            "transition.bounceUpOut": {
                defaultDuration: 1e3,
                calls: [ [ {
                    translateY: 20
                }, .2 ], [ {
                    opacity: [ 0, "easeInCirc", 1 ],
                    translateY: -1e3
                }, .8 ] ],
                reset: {
                    translateY: 0
                }
            },
            "transition.bounceDownIn": {
                defaultDuration: 800,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    translateY: [ 30, -1e3 ]
                }, .6, {
                    easing: "easeOutCirc"
                } ], [ {
                    translateY: -10
                }, .2 ], [ {
                    translateY: 0
                }, .2 ] ]
            },
            "transition.bounceDownOut": {
                defaultDuration: 1e3,
                calls: [ [ {
                    translateY: -20
                }, .2 ], [ {
                    opacity: [ 0, "easeInCirc", 1 ],
                    translateY: 1e3
                }, .8 ] ],
                reset: {
                    translateY: 0
                }
            },
            "transition.bounceLeftIn": {
                defaultDuration: 750,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    translateX: [ 30, -1250 ]
                }, .6, {
                    easing: "easeOutCirc"
                } ], [ {
                    translateX: -10
                }, .2 ], [ {
                    translateX: 0
                }, .2 ] ]
            },
            "transition.bounceLeftOut": {
                defaultDuration: 750,
                calls: [ [ {
                    translateX: 30
                }, .2 ], [ {
                    opacity: [ 0, "easeInCirc", 1 ],
                    translateX: -1250
                }, .8 ] ],
                reset: {
                    translateX: 0
                }
            },
            "transition.bounceRightIn": {
                defaultDuration: 750,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    translateX: [ -30, 1250 ]
                }, .6, {
                    easing: "easeOutCirc"
                } ], [ {
                    translateX: 10
                }, .2 ], [ {
                    translateX: 0
                }, .2 ] ]
            },
            "transition.bounceRightOut": {
                defaultDuration: 750,
                calls: [ [ {
                    translateX: -30
                }, .2 ], [ {
                    opacity: [ 0, "easeInCirc", 1 ],
                    translateX: 1250
                }, .8 ] ],
                reset: {
                    translateX: 0
                }
            },
            "transition.slideUpIn": {
                defaultDuration: 900,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    translateY: [ 0, 20 ],
                    translateZ: 0
                } ] ]
            },
            "transition.slideUpOut": {
                defaultDuration: 900,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    translateY: -20,
                    translateZ: 0
                } ] ],
                reset: {
                    translateY: 0
                }
            },
            "transition.slideDownIn": {
                defaultDuration: 900,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    translateY: [ 0, -20 ],
                    translateZ: 0
                } ] ]
            },
            "transition.slideDownOut": {
                defaultDuration: 900,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    translateY: 20,
                    translateZ: 0
                } ] ],
                reset: {
                    translateY: 0
                }
            },
            "transition.slideLeftIn": {
                defaultDuration: 1e3,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    translateX: [ 0, -20 ],
                    translateZ: 0
                } ] ]
            },
            "transition.slideLeftOut": {
                defaultDuration: 1050,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    translateX: -20,
                    translateZ: 0
                } ] ],
                reset: {
                    translateX: 0
                }
            },
            "transition.slideRightIn": {
                defaultDuration: 1e3,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    translateX: [ 0, 20 ],
                    translateZ: 0
                } ] ]
            },
            "transition.slideRightOut": {
                defaultDuration: 1050,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    translateX: 20,
                    translateZ: 0
                } ] ],
                reset: {
                    translateX: 0
                }
            },
            "transition.slideUpBigIn": {
                defaultDuration: 850,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    translateY: [ 0, 75 ],
                    translateZ: 0
                } ] ]
            },
            "transition.slideUpBigOut": {
                defaultDuration: 800,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    translateY: -75,
                    translateZ: 0
                } ] ],
                reset: {
                    translateY: 0
                }
            },
            "transition.slideDownBigIn": {
                defaultDuration: 850,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    translateY: [ 0, -75 ],
                    translateZ: 0
                } ] ]
            },
            "transition.slideDownBigOut": {
                defaultDuration: 800,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    translateY: 75,
                    translateZ: 0
                } ] ],
                reset: {
                    translateY: 0
                }
            },
            "transition.slideLeftBigIn": {
                defaultDuration: 800,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    translateX: [ 0, -75 ],
                    translateZ: 0
                } ] ]
            },
            "transition.slideLeftBigOut": {
                defaultDuration: 750,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    translateX: -75,
                    translateZ: 0
                } ] ],
                reset: {
                    translateX: 0
                }
            },
            "transition.slideRightBigIn": {
                defaultDuration: 800,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    translateX: [ 0, 75 ],
                    translateZ: 0
                } ] ]
            },
            "transition.slideRightBigOut": {
                defaultDuration: 750,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    translateX: 75,
                    translateZ: 0
                } ] ],
                reset: {
                    translateX: 0
                }
            },
            "transition.perspectiveUpIn": {
                defaultDuration: 800,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    transformPerspective: [ 800, 800 ],
                    transformOriginX: [ 0, 0 ],
                    transformOriginY: [ "100%", "100%" ],
                    rotateX: [ 0, -180 ]
                } ] ],
                reset: {
                    transformPerspective: 0,
                    transformOriginX: "50%",
                    transformOriginY: "50%"
                }
            },
            "transition.perspectiveUpOut": {
                defaultDuration: 850,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    transformPerspective: [ 800, 800 ],
                    transformOriginX: [ 0, 0 ],
                    transformOriginY: [ "100%", "100%" ],
                    rotateX: -180
                } ] ],
                reset: {
                    transformPerspective: 0,
                    transformOriginX: "50%",
                    transformOriginY: "50%",
                    rotateX: 0
                }
            },
            "transition.perspectiveDownIn": {
                defaultDuration: 800,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    transformPerspective: [ 800, 800 ],
                    transformOriginX: [ 0, 0 ],
                    transformOriginY: [ 0, 0 ],
                    rotateX: [ 0, 180 ]
                } ] ],
                reset: {
                    transformPerspective: 0,
                    transformOriginX: "50%",
                    transformOriginY: "50%"
                }
            },
            "transition.perspectiveDownOut": {
                defaultDuration: 850,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    transformPerspective: [ 800, 800 ],
                    transformOriginX: [ 0, 0 ],
                    transformOriginY: [ 0, 0 ],
                    rotateX: 180
                } ] ],
                reset: {
                    transformPerspective: 0,
                    transformOriginX: "50%",
                    transformOriginY: "50%",
                    rotateX: 0
                }
            },
            "transition.perspectiveLeftIn": {
                defaultDuration: 950,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    transformPerspective: [ 2e3, 2e3 ],
                    transformOriginX: [ 0, 0 ],
                    transformOriginY: [ 0, 0 ],
                    rotateY: [ 0, -180 ]
                } ] ],
                reset: {
                    transformPerspective: 0,
                    transformOriginX: "50%",
                    transformOriginY: "50%"
                }
            },
            "transition.perspectiveLeftOut": {
                defaultDuration: 950,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    transformPerspective: [ 2e3, 2e3 ],
                    transformOriginX: [ 0, 0 ],
                    transformOriginY: [ 0, 0 ],
                    rotateY: -180
                } ] ],
                reset: {
                    transformPerspective: 0,
                    transformOriginX: "50%",
                    transformOriginY: "50%",
                    rotateY: 0
                }
            },
            "transition.perspectiveRightIn": {
                defaultDuration: 950,
                calls: [ [ {
                    opacity: [ 1, 0 ],
                    transformPerspective: [ 2e3, 2e3 ],
                    transformOriginX: [ "100%", "100%" ],
                    transformOriginY: [ 0, 0 ],
                    rotateY: [ 0, 180 ]
                } ] ],
                reset: {
                    transformPerspective: 0,
                    transformOriginX: "50%",
                    transformOriginY: "50%"
                }
            },
            "transition.perspectiveRightOut": {
                defaultDuration: 950,
                calls: [ [ {
                    opacity: [ 0, 1 ],
                    transformPerspective: [ 2e3, 2e3 ],
                    transformOriginX: [ "100%", "100%" ],
                    transformOriginY: [ 0, 0 ],
                    rotateY: 180
                } ] ],
                reset: {
                    transformPerspective: 0,
                    transformOriginX: "50%",
                    transformOriginY: "50%",
                    rotateY: 0
                }
            }
        };
        for (var k in f.RegisterEffect.packagedEffects) f.RegisterEffect(k, f.RegisterEffect.packagedEffects[k]);
        f.RunSequence = function(a) {
            var b = g.extend(!0, [], a);
            b.length > 1 && (g.each(b.reverse(), function(a, c) {
                var d = b[a + 1];
                if (d) {
                    var e = c.o || c.options, h = d.o || d.options, i = e && e.sequenceQueue === !1 ? "begin" : "complete", j = h && h[i], k = {};
                    k[i] = function() {
                        var a = d.e || d.elements, b = a.nodeType ? [ a ] : a;
                        j && j.call(b, b), f(c);
                    }, d.o ? d.o = g.extend({}, h, k) : d.options = g.extend({}, h, k);
                }
            }), b.reverse()), f(b[0]);
        };
    }(window.jQuery || window.Zepto || window, window, document);
});

var maxRange = set11Month();

datePickerController.setGlobalOptions({
    nodrag: !0,
    mousewheel: !1
}), datePickerController.createDatePicker({
    formElements: {
        andata: "%d/%m/%Y"
    },
    noTodayButton: !0,
    fillGrid: !0,
    rangeLow: new Date(),
    rangeHigh: maxRange,
    noFadeEffect: !0,
    constrainSelection: !1,
    wrapped: !0,
    callbackFunctions: {
        dateset: [ openAndSetReturn ]
    }
}), datePickerController.createDatePicker({
    formElements: {
        ritorno: "%d/%m/%Y"
    },
    noTodayButton: !0,
    fillGrid: !0,
    rangeLow: new Date(),
    rangeHigh: maxRange,
    constrainSelection: !1,
    noFadeEffect: !0,
    wrapped: !0,
    callbackFunctions: {
        dateset: [ openToolStage ]
    }
}), datePickerController.createDatePicker({
    formElements: {
        flightStatusDatePicker: "%d/%m/%Y"
    },
    noTodayButton: !0,
    fillGrid: !0,
    rangeLow: new Date(),
    rangeHigh: next10Days,
    noFadeEffect: !0,
    constrainSelection: !1,
    wrapped: !1
}), datePickerController.createDatePicker({
    formElements: {
        flightStatusDatePicker2: "%d/%m/%Y"
    },
    noTodayButton: !0,
    fillGrid: !0,
    rangeLow: new Date(),
    rangeHigh: next10Days,
    noFadeEffect: !0,
    constrainSelection: !1,
    wrapped: !1
}), $(function() {
    $("[data-tooltip]").each(function() {
        var a = $(this).attr("data-tooltipAuto"), b = "mouseenter focus", c = "mouseleave blur";
        Modernizr.touch && (b = "click", c = "click"), a && (b = null, c = null), $(this).qtip({
            content: {
                text: $(this).attr("data-tooltip"),
                title: $(this).attr("data-tooltipTitle")
            },
            style: "dark",
            position: {
                viewport: !0,
                effect: !1
            },
            show: {
                event: b
            },
            hide: {
                event: c
            },
            style: {
                tip: {
                    corner: !0
                }
            }
        });
    });
}), $(function() {
    var a = '<div class="mainMenu__secondLevelOrganizer"></div>';
    $(".mainMenu__secondLevelList").each(function() {
        $(this).find(".j-colType1").wrapAll(a), $(this).find(".j-colType2").wrapAll(a), 
        $(this).find(".j-colType3").wrapAll(a), $(this).find(".j-colType4").wrapAll(a), 
        $(this).find(".j-colType5").wrapAll(a), $(this).find(".j-colType6").wrapAll(a);
    }), dropDownInit(), $(".j-goBackThird").fastClick(function(a) {
        a.preventDefault(), $(".mainMenu__thirdLevelCover").removeClass("isActiveThird");
    }), $(".j-goBackSecond").fastClick(function(a) {
        a.preventDefault(), $(".mainMenu__secondLevelCover").removeClass("isActiveSecond");
    }), $(".j-openMobileMenu").fastClick(function(a) {
        a.preventDefault(), $("body").toggleClass("showMenuActivated").unbind().one(cssTransitionEnd, function(a) {
            $(".mainMenu__secondLevelCover").removeClass("isActiveSecond"), $(".mainMenu__thirdLevelCover").removeClass("isActiveThird");
        });
    }), $(".mainMenu__secondLevelCover").fastClick(function(a) {
        return !1;
    });
}), function() {
    var a = function(a, b) {
        return function() {
            return a.apply(b, arguments);
        };
    };
    window.SliderRange = function() {
        function b(b) {
            this.ref = b, this.range_max = parseInt(this.ref.attr("data-max")), this.range_min = parseInt(this.ref.attr("data-min")), 
            this.range = this.range_max - this.range_min, this.step = parseInt(this.ref.attr("data-step")), 
            this.total_steps = Math.round(this.range / this.step), this.gotoStep = a(this.gotoStep, this), 
            this.updatePosition = a(this.updatePosition, this), this.isIE = a(this.isIE, this), 
            this.onTouchEnd = a(this.onTouchEnd, this), this.onTouchMove = a(this.onTouchMove, this), 
            this.onTouchStart = a(this.onTouchStart, this), this.onMouseUp = a(this.onMouseUp, this), 
            this.onMouseDown = a(this.onMouseDown, this), this.onMouseMove = a(this.onMouseMove, this), 
            this.setInteractions = a(this.setInteractions, this), this.sliderRangeValue = this.ref.find(".sliderRange-value span"), 
            this.sliderRangeValue_min = this.ref.find(".sliderRange__amount__min .number"), 
            this.sliderRangeValue_max = this.ref.find(".sliderRange__amount__max .number"), 
            this.window_ref = $(window), this.is_ie = this.isIE(), this.is_mobile = navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/Android/i) ? !0 : !1, 
            this.b = $("body,html"), this.controller = this.ref.find(".js-controller"), this.dragger = this.controller.find(".js-slider"), 
            this.slider = this.controller.find(".js-bar"), this.notify = this.controller.find(".js-notify"), 
            this.dragger.css("left", "0px"), this.slide_amp = this.dragger.width(), this.angles = [], 
            this.current_pos = 0, this.current_step = 0, this.mouse_x = 0, this.mouse_y = 0, 
            this.firstAngle = 0, this.setInteractions(), this.setTexts();
        }
        return b.prototype.setTexts = function() {
            this.sliderRangeValue.text(this.range_min), this.sliderRangeValue_min.text(this.range_min), 
            this.sliderRangeValue_max.text(this.range_max);
        }, b.prototype.setInteractions = function() {
            return this.is_mobile ? (this.dragger.bind("touchstart", this.onTouchStart), this.dragger.bind("touchmove", this.onTouchMove), 
            this.dragger.bind("touchend", this.onTouchEnd)) : (this.b.bind("mousemove", this.onMouseMove), 
            this.dragger.bind("mousedown", this.onMouseDown));
        }, b.prototype.onMouseMove = function(a) {
            return this.is_ie ? this.mouse_x = a.clientX : this.mouse_x = a.pageX, this.mouse_x = parseInt(this.draggerPos) + (this.mouse_x - this.startX), 
            this.maxLeft = this.slider.closest(".sliderRange__bar").width(), this.mouse_x = this.mouse_x < 0 ? 0 : this.mouse_x > this.maxLeft ? this.maxLeft : this.mouse_x, 
            this.is_scrolling ? this.updatePosition() : void 0;
        }, b.prototype.onMouseDown = function(a) {
            return a.preventDefault(), this.is_ie ? this.startX = a.clientX : this.startX = a.pageX, 
            this.is_scrolling = !0, this.b.bind("mouseup", this.onMouseUp), this.draggerPos = this.dragger.css("left");
        }, b.prototype.onMouseUp = function(a) {
            return a.preventDefault(), this.b.unbind("mouseup"), this.is_scrolling = !1;
        }, b.prototype.onTouchStart = function(a) {
            var b;
            return b = a.originalEvent.changedTouches[0], this.is_scrolling = !0, this.startX = b.clientX, 
            this.draggerPos = this.dragger.css("left");
        }, b.prototype.onTouchMove = function(a) {
            var b;
            return a.preventDefault(), b = a.originalEvent.changedTouches[0], this.mouse_x = b.clientX, 
            this.mouse_x = parseInt(this.draggerPos) + (this.mouse_x - this.startX), this.maxLeft = this.slider.closest(".sliderRange__bar").width() - this.dragger.width(), 
            this.mouse_x = this.mouse_x < 0 ? 0 : this.mouse_x > this.maxLeft ? this.maxLeft : this.mouse_x, 
            this.is_scrolling ? this.updatePosition() : void 0;
        }, b.prototype.onTouchEnd = function(a) {
            return this.is_scrolling = !1;
        }, b.prototype.isIE = function() {
            var a;
            return a = navigator.userAgent.toLowerCase(), -1 !== a.indexOf("msie") ? parseInt(a.split("msie")[1]) : !1;
        }, b.prototype.updatePosition = function() {
            var a;
            this.area_wx = this.slider.closest(".sliderRange__bar").width(), a = Math.round(this.mouse_x * this.range_max / this.area_wx);
            Math.round(this.mouse_x * this.range / this.area_wx) + this.range_min;
            a === this.range_max && (a = this.range_max - 1), a !== this.current_state && (this.current_state = a), 
            this.dragger.css({
                left: "" + this.mouse_x + "px"
            }), this.slider.css({
                width: "" + this.mouse_x + "px"
            });
            var b = Math.round(this.mouse_x * this.total_steps / this.area_wx) * this.step, c = b + this.range_min;
            c > this.range_max && (c = this.range_max), this.sliderRangeValue.text(c);
        }, b.prototype.gotoStep = function() {
            var a;
            return this.current_state = this.firstAngle, a = this.area_wx * this.firstAngle / this.range_max, 
            this.dragger.css({
                left: "" + a + "px"
            });
        }, b;
    }();
}.call(this);

var sliderRange = $(".js-sliderRange");

sliderRange.length && sliderRange.each(function(a, b) {
    new SliderRange($(b));
}), $.fn.responsiveTable = function(a) {
    var b = this.selector.replace(".", "") + "", c = "@media screen and (max-width: " + a.maxWidth + "px) {";
    0 != $("style" + b).length && $("style" + b).remove(), this.each(function(a, b) {
        var d = $(this).attr("data-tableType");
        if (0 === $(this).find("thead").length) {
            var e = $(this).find("tbody tr th").parent("tr");
            $(this).prepend("<thead>" + e.html() + "</thead>"), e.remove();
        }
        if (("threeCol--repeat" === d || "fourCol--repeat" === d || "fiveCol--repeat" === d) && $(this).addClass("repeatVersion"), 
        "twoCol" === d) {
            var f = $(this).find("thead tr th").eq(0).text().trim(), g = $(this).find("thead tr th").eq(1).text().trim();
            c += "" === f ? "" : ".respTable" + a + ' .thTitle0:before { content: "' + f + '"; }', 
            c += ".respTable" + a + ' .thTitle1:before { content: "' + g + '"; }', $(this).addClass("respTable" + a).find("tbody tr").each(function(a, b) {
                $(this).find("td").eq(0).addClass("title").addClass("thTitle0"), $(this).find("td").eq(1).addClass("thTitle1");
            });
        }
        if ("threeCol" === d || "threeCol--repeat" === d) {
            var f = $(this).find("thead tr th").eq(0).text().trim(), g = $(this).find("thead tr th").eq(1).html(), h = $(this).find("thead tr th").eq(2).html();
            c += "" === f ? "" : ".respTable" + a + ' .thTitle0:before { content: "' + f + '"; }', 
            c += ".respTable" + a + ' .thTitle1:before { content: "' + g + '"; }', c += ".respTable" + a + ' .thTitle2:before { content: "' + h + '"; }', 
            $(this).addClass("respTable" + a).find("tbody tr").each(function(a, b) {
                if (3 === $(this).find("td").length ? ($(this).find("td").eq(0).addClass("thTitle0 title"), 
                $(this).find("td").eq(1).addClass("thTitle1"), $(this).find("td").eq(2).addClass("thTitle2")) : ($(this).find("td").eq(0).addClass("thTitle1"), 
                $(this).find("td").eq(1).addClass("thTitle2")), "threeCol" === d) {
                    var c = $(this), e = $(this).find(".thTitle1").height(), f = $(this).find(".thTitle2").height();
                    e > f ? c.addClass("leftFix") : c.addClass("rightFix");
                }
            });
        }
        if ("fourCol" === d || "fourCol--repeat" === d) {
            var f = ($(this).text().trim(), $(this).find("thead tr th").eq(0).text().trim()), g = $(this).find("thead tr th").eq(1).text().trim(), h = $(this).find("thead tr th").eq(2).text().trim(), i = $(this).find("thead tr th").eq(3).text().trim();
            c += "" === f ? "" : ".respTable" + a + ' .thTitle0:before { content: "' + f + '"; }', 
            c += ".respTable" + a + ' .thTitle1:before { content: "' + g + '"; }', c += ".respTable" + a + ' .thTitle2:before { content: "' + h + '"; }', 
            c += ".respTable" + a + ' .thTitle3:before { content: "' + i + '"; }';
            var j = 0, k = 0, l = $(this).addClass("respTable" + a).find("tbody tr");
            l.each(function(a, b) {
                if (k > 0) {
                    var c = $(this).find("td"), d = c.eq(0).clone().addClass("thTitle1").addClass("showOnMobile"), e = c.eq(1).clone().addClass("thTitle2").addClass("showOnMobile"), f = l.eq(a - (j - k + 1));
                    $(this).addClass("hideOnMobile"), f.find(".thTitle2:last").after(d), f.find(".thTitle1:last").after(e);
                    var g = d.outerHeight(), h = e.outerHeight();
                    return g >= h ? d.addClass("rightFix") : e.addClass("leftFix"), k -= 1, !0;
                }
                var c = $(this).find("td");
                c.eq(0).addClass("title thTitle0 j-openTable"), c.eq(1).addClass("thTitle1"), c.eq(2).addClass("thTitle2"), 
                c.eq(3).addClass("thTitle3");
                var i = parseInt(c.attr("rowspan"));
                i > 1 && ($(this).addClass("hasRowSpan"), j = k = i - 1, $(".hasRowSpan").each(function() {
                    var a = $(this).find(".thTitle1").outerHeight(), b = $(this).find(".thTitle2").outerHeight();
                    a >= b ? $(this).find(".thTitle1").addClass("rightFix") : $(this).find(".thTitle2").addClass("leftFix");
                }));
            });
        }
        if ("fiveCol--repeat" === d || "five--repeat" === d) {
            var f = ($(this).text().trim(), $(this).find("thead tr th").eq(0).text().trim()), g = $(this).find("thead tr th").eq(1).text().trim(), h = $(this).find("thead tr th").eq(2).text().trim(), i = $(this).find("thead tr th").eq(3).text().trim();
            thTitle4 = $(this).find("thead tr th").eq(4).text().trim(), c += "" === f ? "" : ".respTable" + a + ' .thTitle0:before { content: "' + f + '"; }', 
            c += ".respTable" + a + ' .thTitle1:before { content: "' + g + '"; }', c += ".respTable" + a + ' .thTitle2:before { content: "' + h + '"; }', 
            c += ".respTable" + a + ' .thTitle3:before { content: "' + i + '"; }', c += ".respTable" + a + ' .thTitle4:before { content: "' + thTitle4 + '"; }';
            var j = 0, k = 0, l = $(this).addClass("respTable" + a).find("tbody tr");
            l.each(function(a, b) {
                if (k > 0) {
                    var c = $(this).find("td"), d = c.eq(0).clone().addClass("thTitle1").addClass("showOnMobile"), e = c.eq(1).clone().addClass("thTitle2").addClass("showOnMobile"), f = l.eq(a - (j - k + 1));
                    $(this).addClass("hideOnMobile"), f.find(".thTitle2:last").after(d), f.find(".thTitle1:last").after(e);
                    var g = d.outerHeight(), h = e.outerHeight();
                    return g >= h ? d.addClass("rightFix") : e.addClass("leftFix"), k -= 1, !0;
                }
                var c = $(this).find("td");
                c.eq(0).addClass("title thTitle0 j-openTable"), c.eq(1).addClass("thTitle1"), c.eq(2).addClass("thTitle2"), 
                c.eq(3).addClass("thTitle3"), c.eq(4).addClass("thTitle4");
                var i = parseInt(c.attr("rowspan"));
                i > 1 && ($(this).addClass("hasRowSpan"), j = k = i - 1, $(".hasRowSpan").each(function() {
                    var a = $(this).find(".thTitle1").outerHeight(), b = $(this).find(".thTitle2").outerHeight();
                    a >= b ? $(this).find(".thTitle1").addClass("rightFix") : $(this).find(".thTitle2").addClass("leftFix");
                }));
            });
        }
    }), c += "}", $("head").append('<style id="' + b + '">' + c + "</style>"), $(".j-openTable").on("click", function() {
        a.maxWidth > $(window).width() && ($(this).closest("tr").toggleClass("isActive"), 
        $(window).trigger("resize.accordion"));
    }), $(window).resize(function() {
        var a;
        clearTimeout(a), $(".j-responsiveTable").hide(), a = setTimeout(function() {
            $(".j-responsiveTable").show();
        }, 50);
    });
};

var _createClass = function() {
    function a(a, b) {
        for (var c = 0; c < b.length; c++) {
            var d = b[c];
            d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), 
            Object.defineProperty(a, d.key, d);
        }
    }
    return function(b, c, d) {
        return c && a(b.prototype, c), d && a(b, d), b;
    };
}();

$(function() {
    var a = function() {
        function a(b) {
            _classCallCheck(this, a), this.wrap = b, this.filterTargets = b.find("[data-filter]"), 
            this.filterSelect = b.find("[data-filterSelect]"), this.init();
        }
        return _createClass(a, [ {
            key: "init",
            value: function() {
                this.hideItems(), this.listners(), this.showItems("*");
            }
        }, {
            key: "showItems",
            value: function(a) {
                if ("" !== a) if ("*" !== a) {
                    var b = this.wrap.find('[data-filter="' + a + '"]');
                    this.hideItems(), b.length && b.show();
                } else this.filterTargets.show(); else this.hideItems();
            }
        }, {
            key: "hideItems",
            value: function() {
                this.filterTargets.hide();
            }
        }, {
            key: "listners",
            value: function() {
                var a = this;
                this.filterSelect.on("change", function(b) {
                    var c = a.filterSelect.val();
                    a.showItems(c);
                });
            }
        } ]), a;
    }(), b = $("[data-selectFiltering]");
    b.each(function(b, c) {
        console.log(c), $(c).data("selectFilter", new a($(c)));
    });
}), $(function() {
    $(".j-readNotification").on("click", function(a) {
        return $(this).parent().addClass("isRead"), !1;
    }), $(".mainMenu__navigationSearchIcon, .mainMenu__navigationSearchSubmit").focusin(function(a) {
        if (!$(".mainMenu__navigationSearch").hasClass("open")) {
            var b;
            window.windowW > 1023 ? (b = $(".mainMenu__nav").width() + $(".mainMenu__navigationSearch").width() + 20, 
            $(".mainMenu__navigationSearch").velocity({
                width: b
            }).addClass("open")) : window.windowW > 800 && (b = 400, $(".mainMenu__navigationSearch").velocity({
                width: b
            }).addClass("open"));
        }
    }).focusout(function(a) {
        $(".mainMenu__navigationSearch").velocity("reverse").attr("style", "").removeClass("open");
    }), $(".mainMenu__navigationSearchSubmit").click(function(a) {
        a.preventDefault(), a.stopPropagation(), a.stopImmediatePropagation();
        var b = $("#mainMenu__search").val().length;
        b > 0 && $(this).closest("form").submit();
    }), $(".mainMenu__navigationSearchSubmit").mousedown(function(a) {
        var b = $("#mainMenu__navigationSearch");
        a.preventDefault(), a.stopPropagation(), a.stopImmediatePropagation(), b.hasClass("open") || $(".mainMenu__navigationSearchIcon").focus();
    }), $(".j-openSearch").click(function(a) {
        $(".header").toggleClass("openSearchMobile");
    });
});

var _createClass = function() {
    function a(a, b) {
        for (var c = 0; c < b.length; c++) {
            var d = b[c];
            d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), 
            Object.defineProperty(a, d.key, d);
        }
    }
    return function(b, c, d) {
        return c && a(b.prototype, c), d && a(b, d), b;
    };
}();

$(function() {
    var a = function() {
        function a(b) {
            _classCallCheck(this, a), this.ref = b, this.doc = $(document), this.body = $("body"), 
            this.lastOpen = null, this.isAnimating = !1, this.eventType = "click.topBar", this.init();
        }
        return _createClass(a, [ {
            key: "init",
            value: function() {
                this.listeners();
            }
        }, {
            key: "closeLast",
            value: function() {
                var a = this, b = (arguments.length <= 0 || void 0 === arguments[0] ? !0 : arguments[0], 
                $.Deferred());
                if (null == this.lastOpen) b.resolve(); else {
                    var c = this.lastOpen.submenu.find("[tabindex],input,select,a");
                    c.attr("tabindex", -1), this.lastOpen.cur.removeClass("isActive");
                    var d = this.lastOpen.cur.data().group, e = this.getAnimationType(d, !1);
                    this.lastOpen.submenu.velocity(e, {
                        complete: function() {
                            a.lastOpen.submenu && a.lastOpen.submenu.removeClass("isActive"), a.lastOpen = null, 
                            a.body.removeClass("userMenuIsActive"), b.resolve();
                        },
                        duration: 200
                    });
                }
                return b.promise();
            }
        }, {
            key: "focusOnChild",
            value: function(a) {
                var b = a.find("[tabindex],input,select,a	");
                b.attr("tabindex", 1).eq(0).focus();
            }
        }, {
            key: "getAnimationType",
            value: function(a) {
                var b = arguments.length <= 1 || void 0 === arguments[1] ? !0 : arguments[1], c = {
                    height: function() {
                        return $(this).get(0).scrollHeight;
                    }
                };
                switch (a) {
                  case "languageGroup":
                  case "loginGroup":
                  case "supportGroup":
                    return b ? c : {
                        height: 0
                    };

                  case "notificationGroup":
                  case "recentGroup":
                    return b ? $(window).width() >= 800 ? "transition.slideUpIn" : c : $(window).width() >= 800 ? "transition.slideDownOut" : {
                        height: 0
                    };
                }
            }
        }, {
            key: "onAnimationComplete",
            value: function(a, b) {
                a.addClass("isActive"), b.addClass("isActive"), this.lastOpen = {
                    cur: a,
                    submenu: b
                };
            }
        }, {
            key: "closeMainMenu",
            value: function() {
                this.body.removeClass("showMenuActivated"), $(".mainMenu__thirdLevelContainer, .mainMenu__thirdLevel, .mainMenu__secondLevel, .mainMenu__navigation__firstLevelItemLink").removeClass("isActive");
            }
        }, {
            key: "toggleSubmenu",
            value: function(a, b) {
                var c = this, d = b.hasClass("isActive"), e = $('.j-groupContainer[data-group="' + a + '"]');
                if (this.closeMainMenu(), d) {
                    if (e.length) switch (b.removeClass("isActive"), a) {
                      case "languageGroup":
                      case "loginGroup":
                      case "supportGroup":
                        e.velocity(this.getAnimationType(a, !1), {
                            queue: !1,
                            complete: function() {
                                b.removeClass("isActive"), e.removeClass("isActive"), c.body.removeClass("userMenuIsActive");
                            }
                        });
                        break;

                      case "notificationGroup":
                      case "recentGroup":
                        e.velocity(this.getAnimationType(a, !1), {
                            begin: function() {
                                return c.body.addClass("userMenuIsActive");
                            },
                            queue: !1,
                            duration: 150,
                            complete: function() {
                                b.removeClass("isActive"), e.removeClass("isActive"), c.body.removeClass("userMenuIsActive"), 
                                c.lastOpen = null;
                            }
                        });
                    }
                } else if (b.addClass("isActive"), e.length) switch (this.isAnimating = !0, a) {
                  case "languageGroup":
                  case "loginGroup":
                  case "supportGroup":
                    e.get(0).scrollHeight;
                    this.closeLast().then(function() {
                        e.velocity(c.getAnimationType(a, !0), {
                            queue: !1,
                            duration: 300,
                            begin: function() {
                                return c.body.addClass("userMenuIsActive");
                            },
                            complete: function() {
                                c.onAnimationComplete(b, e), c.isAnimating = !1, c.focusOnChild(e);
                            }
                        });
                    });
                    break;

                  case "notificationGroup":
                    $(".userMenu__notificationsCount").text(0);

                  case "recentGroup":
                    this.closeLast(!1).then(function() {
                        e.velocity(c.getAnimationType(a, !0), {
                            queue: !1,
                            duration: 300,
                            complete: function() {
                                c.onAnimationComplete(b, e), c.isAnimating = !1, c.focusOnChild(e);
                            }
                        });
                    });
                }
            }
        }, {
            key: "isMenuLink",
            value: function(a) {
                var b = $(a.target);
                return _parent = b.closest(".j-userMenuLink"), _parent.length ? !0 : !1;
            }
        }, {
            key: "listeners",
            value: function() {
                var a = this;
                this.doc.on("MAINMENU:TOGGLE", function() {
                    a.closeLast();
                }), this.doc.on(this.eventType, function(a) {}), this.doc.on(this.eventType, function(b) {
                    var c = $(b.target);
                    if (c.is(".j-userMenuLink") || a.isMenuLink(b)) {
                        b.preventDefault(), a.isMenuLink(b) && (c = c.closest(".j-userMenuLink"));
                        var d = c.data().group || null;
                        null !== d && (a.isAnimating || a.toggleSubmenu(d, c));
                    } else null !== a.lastOpen && (a.lastOpen.cur.is(".userMenu__notifications.j-userMenuLink") || a.lastOpen.cur.is(".userMenu__recent.j-userMenuLink")) && a.closeLast();
                });
            }
        } ]), a;
    }();
    window.TopBarRef = new a($(".header"));
});

var _createClass = function() {
    function a(a, b) {
        for (var c = 0; c < b.length; c++) {
            var d = b[c];
            d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), 
            Object.defineProperty(a, d.key, d);
        }
    }
    return function(b, c, d) {
        return c && a(b.prototype, c), d && a(b, d), b;
    };
}();

$(function() {
    var a = function() {
        function a(b) {
            _classCallCheck(this, a), this.$doc = $(document), this.mode = $(b).data("mode"), 
            this.accordions = $(b).find(".j-accordion"), this.atleast = 1 == $(b).data("atleast"), 
            this.sync = 1 == $(b).data("sync"), this.nestedAccordion = $(b).closest(".j-accordion, .j-infowidget-accordion").length > 0, 
            this.resizeID, this.init();
        }
        return _createClass(a, [ {
            key: "init",
            value: function() {
                var a = this;
                this.listners(), this.accordions.attr("tabindex", 0), this.accordions.css("transition", "none"), 
                this.resizeHeights(), this.accordions.siblings(".active").each(function(b, c) {
                    a.openAccordion($(c));
                });
            }
        }, {
            key: "checkAutoCollapse",
            value: function(a) {
                if ("nocollapse" == this.mode) return $.Deferred().resolve();
                var b = a.siblings(".active");
                return b.length > 0 ? this.sync ? (this.closeAccordion(b), $.Deferred().resolve()) : this.closeAccordion(b) : $.Deferred().resolve();
            }
        }, {
            key: "openAccordion",
            value: function(a) {
                var b = this, c = a;
                this.checkAutoCollapse(c).then(function() {
                    c.addClass("active").addClass("opening"), c.attr("aria-selected", "true").find(".j-accordion-body").attr("aria-hidden", "false");
                    var d = c.get(0).scrollHeight + 2;
                    return b.nestedAccordion && $(a).trigger("resizebychild"), void 0 === d || 2 >= d ? void (d = "auto") : void c.velocity("stop").velocity({
                        height: d
                    }, {
                        duration: 300,
                        queue: !1
                    });
                });
            }
        }, {
            key: "closeAccordion",
            value: function(a) {
                var b = $.Deferred(), c = a;
                if (null !== c) {
                    c.removeClass("opening").attr("aria-selected", "false"), c.find(".j-accordion-body").attr("aria-hidden", "true");
                    var d = c.data("closed_height");
                    if (0 == d) return;
                    c.velocity("stop").velocity({
                        height: d
                    }, {
                        duration: 300,
                        queue: !1,
                        begin: function() {},
                        complete: function(a) {
                            c.removeClass("active"), b.resolve();
                        }
                    });
                } else b.resolve();
                return b.promise();
            }
        }, {
            key: "resizeHeights",
            value: function() {
                this.accordions.each(function(a, b) {
                    var c = $(this);
                    c.data("closed_height", c.children().first().outerHeight()), c.data("opened_height", c.height("auto").height($(this).outerHeight()).height());
                });
            }
        }, {
            key: "listners",
            value: function() {
                var a = this, b = this.accordions.find(".j-accordion-header");
                b.children().attr("tabindex", -1), $(window).on("resize.accordion", function(b) {
                    clearTimeout(a.resizeID), a.resizeID = setTimeout(function() {
                        a.resizeHeights();
                    }, 200);
                }), this.accordions.on("recalculate", function(b) {
                    a.resizeHeights();
                }), this.accordions.on("keypress.accordion", function(b) {
                    if (13 == b.which) {
                        b.preventDefault();
                        var c = $(b.currentTarget);
                        if (c.is(":focus")) return c.hasClass("opening") ? a.closeAccordion(c) : a.openAccordion(c);
                    }
                }), this.accordions.find(".j-accordion-header a").on("click.accordion", function(a) {
                    return a.preventDefault(), a.stopImmediatePropagation(), $(a.currentTarget).parent().trigger("click.accordion"), 
                    !1;
                }), this.accordions.on("click.accordion", ".j-accordion-header", function(b) {
                    b.stopPropagation(), b.preventDefault();
                    var c = $(b.currentTarget);
                    if (c.hasClass("j-accordion-header")) {
                        var d = $(b.currentTarget).closest(".j-accordion");
                        d.hasClass("opening") ? a.atleast || a.closeAccordion(d) : a.openAccordion(d);
                    }
                    return !1;
                });
            }
        } ]), a;
    }(), b = $(".j-accordions");
    b.length && b.each(function(b, c) {
        new a(c);
    });
});

var _createClass = function() {
    function a(a, b) {
        for (var c = 0; c < b.length; c++) {
            var d = b[c];
            d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), 
            Object.defineProperty(a, d.key, d);
        }
    }
    return function(b, c, d) {
        return c && a(b.prototype, c), d && a(b, d), b;
    };
}();

$(function() {
    var a = function() {
        function a(b) {
            _classCallCheck(this, a), this.$doc = $(document), this.mode = $(b).data("mode"), 
            this.el = $(b), this.accordions = $(b).find(".j-infowidget-accordion"), this.headersDesktop = this.el.find(".j-infowidget-tab"), 
            this.lastAccordion = null, this.currentBreakpoint, this.resizeID, this.init();
        }
        return _createClass(a, [ {
            key: "init",
            value: function() {
                this.listners(), this.accordions.attr("tabindex", 0), this.accordions.css("transition", "none"), 
                this.resizeHeights();
            }
        }, {
            key: "checkAutoCollapse",
            value: function(a) {
                return "s" == this.currentBreakpoint || a.is(this.lastAccordion) ? $.Deferred().resolve() : this.closeAccordion();
            }
        }, {
            key: "detectBreakpoint",
            value: function() {
                var a = $(window).width();
                return br = 640 >= a ? "s" : a > 800 ? "l" : "m";
            }
        }, {
            key: "changeBreakpoint",
            value: function() {
                this.accordions.attr("style", ""), this.accordions.removeClass("active"), this.headersDesktop.removeClass("active"), 
                "l" == this.currentBreakpoint && this.headersDesktop.eq(0).trigger("click");
            }
        }, {
            key: "openAccordion",
            value: function(a) {
                var b = this;
                this.headersDesktop.removeClass("active"), this.headersDesktop.eq(a.index()).addClass("active"), 
                this.checkAutoCollapse(a).then(function() {
                    a.addClass("active").addClass("opening"), a.find(".j-accordion").triggerHandler("recalculate");
                    var c = a.get(0).scrollHeight;
                    b.lastAccordion = a, void 0 === c && (c = "auto");
                    var d = "l" == b.currentBreakpoint ? {
                        opacity: 1
                    } : {
                        height: c
                    }, e = "l" == b.currentBreakpoint ? 200 : 400;
                    a.velocity("stop").velocity(d, {
                        queue: !1,
                        duration: e,
                        begin: function() {},
                        complete: function(a) {}
                    });
                });
            }
        }, {
            key: "closeAccordion",
            value: function(a) {
                var b = this, c = $.Deferred();
                if ("s" == this.currentBreakpoint && (this.lastAccordion = a), null !== this.lastAccordion) {
                    this.lastAccordion.removeClass("opening");
                    var d = "l" == this.currentBreakpoint ? {
                        opacity: 0,
                        duration: 100
                    } : {
                        height: this.lastAccordion.data("oheight")
                    }, e = "l" == this.currentBreakpoint ? 200 : 400;
                    this.lastAccordion.velocity("stop").velocity(d, {
                        queue: !1,
                        duration: e,
                        begin: function() {},
                        complete: function(a) {
                            b.lastAccordion.removeClass("active"), b.lastAccordion = null, c.resolve();
                        }
                    });
                } else c.resolve();
                return c.promise();
            }
        }, {
            key: "resizeHeights",
            value: function() {
                this.detectBreakpoint() != this.currentBreakpoint && (this.currentBreakpoint = br, 
                this.changeBreakpoint(), this.accordions.each(function(a, b) {
                    var c = $(this), d = c.children().first();
                    c.data("oheight", d.is(":visible") ? d.outerHeight() : 0), $(this).height("auto").height($(this).height());
                }));
            }
        }, {
            key: "listners",
            value: function() {
                var a = this, b = this.accordions.find(".j-infowidget-header");
                b.children().attr("tabindex", -1), $(window).on("resize.accordion", function(b) {
                    clearTimeout(a.resizeID), a.resizeID = setTimeout(function() {
                        a.resizeHeights();
                    }, 200);
                }), this.accordions.on("resizebychild", function(b) {
                    var c = a.accordions.siblings(".active");
                    c.length > 0 && c.height("auto");
                }), this.accordions.on("keypress.accordion", function(b) {
                    if (13 == b.which) {
                        b.preventDefault();
                        var c = $(b.currentTarget);
                        if (c.is(":focus")) return c.hasClass("opening") ? a.closeAccordion(c) : a.openAccordion(c);
                    }
                }), this.headersDesktop.on("click.accordion", function(b) {
                    var c = $(b.currentTarget);
                    b.preventDefault(), a.accordions.eq(c.index()).find(".j-infowidget-header").trigger("click");
                }), this.accordions.on("click.accordion", ".j-infowidget-header", function(b) {
                    b.preventDefault();
                    var c = $(b.currentTarget);
                    if (c.hasClass("j-infowidget-header")) {
                        var d = $(b.currentTarget).closest(".j-infowidget-accordion");
                        if (!d.hasClass("active")) return a.openAccordion(d);
                        if ("l" != a.currentBreakpoint) return a.closeAccordion(d);
                    }
                });
            }
        } ]), a;
    }(), b = $(".j-infowidget");
    b.length && b.each(function(b, c) {
        new a(c);
    });
});

var _createClass = function() {
    function a(a, b) {
        for (var c = 0; c < b.length; c++) {
            var d = b[c];
            d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), 
            Object.defineProperty(a, d.key, d);
        }
    }
    return function(b, c, d) {
        return c && a(b.prototype, c), d && a(b, d), b;
    };
}();

$(function() {
    var a = function() {
        function a() {
            _classCallCheck(this, a), this.$doc = $(document), this.accordions = $(".millemiglia__accordion-"), 
            this.services = $(".j-millemiglia__service"), this.lastAccordion = null, this.resizeID, 
            this.serviceFeatures = $(".j-millemiglia__service_features"), this.init();
        }
        return _createClass(a, [ {
            key: "init",
            value: function() {
                this.listners(), this.accordions.prop("open", !1), this.accordions.attr("tabindex", 0), 
                this.accordions.css("transition", "none");
            }
        }, {
            key: "openAccordion",
            value: function(a) {
                var b = this;
                this.closeAccordion().then(function() {
                    a.prop("open", "true"), a.addClass("active");
                    var c = a.get(0), d = c.scrollHeight;
                    b.lastAccordion = a, a.data("oheight", a.outerHeight()), void 0 === d && (d = "auto"), 
                    a.velocity({
                        height: d
                    }, {
                        queue: !1,
                        complete: function() {}
                    });
                });
            }
        }, {
            key: "closeAccordion",
            value: function() {
                var a = this, b = $.Deferred();
                return null !== this.lastAccordion ? this.lastAccordion.velocity({
                    height: this.lastAccordion.data("oheight")
                }, {
                    queue: !1,
                    begin: function() {
                        a.lastAccordion = null;
                    },
                    complete: function(c) {
                        a.accordions.removeClass("active"), $(c).prop("open", !1), b.resolve();
                    }
                }) : b.resolve(), b.promise();
            }
        }, {
            key: "openService",
            value: function(a) {
                if (a.length && this.services.length) {
                    this.closeService(), a.addClass("isActive");
                    var b = a.data("featureid"), c = $('[data-feature="' + b + '"]'), d = c.clone();
                    if (d.addClass("cloned-feature"), c.length) return $(window).width() <= 800 ? (d.height(0), 
                    a.after(d), d.addClass("isActive"), void d.height(d.get(0).scrollHeight)) : ($(".cloned-feature").remove(), 
                    c.addClass("isActive"));
                }
            }
        }, {
            key: "closeService",
            value: function() {
                var a = arguments.length <= 0 || void 0 === arguments[0] ? null : arguments[0];
                null == a ? ($(".cloned-feature").each(function(a, b) {
                    b = $(b), b.height(0), setTimeout(function() {
                        b.remove();
                    }, 450);
                }), this.services.removeClass("isActive"), this.serviceFeatures.removeClass("isActive")) : ($(".cloned-feature").remove(), 
                a.removeClass("isActive"), this.serviceFeatures.removeClass("isActive"));
            }
        }, {
            key: "resizeHeights",
            value: function() {
                this.accordions.each(function(a, b) {
                    var c = $(this);
                    c.data("oheight", c.children().first().outerHeight()), $(this).height("auto").height($(this).height());
                });
            }
        }, {
            key: "listners",
            value: function() {
                var a = this;
                this.accordions.prop("open", !0), $(window).on("resize.millemiglia", function(b) {
                    clearTimeout(a.resizeID), a.resizeID = setTimeout(function() {
                        a.resizeHeights();
                    }, 200);
                }), this.$doc.on("keypress.millemiglia", ".millemiglia__accordion", function(b) {
                    if (13 == b.which) {
                        b.preventDefault();
                        var c = $(b.currentTarget);
                        if (c.is(":focus")) return c.hasClass("active") ? a.closeAccordion(c) : a.openAccordion(c);
                    }
                }), this.$doc.on("click.millemiglia", ".millemiglia__accordion__header,.j-millemiglia__service,.millemiglia__service__close", function(b) {
                    var c = $(b.currentTarget);
                    if (c.hasClass("millemiglia__accordion__header")) {
                        b.preventDefault();
                        var d = $(b.currentTarget).closest(".millemiglia__accordion");
                        return d.hasClass("active") ? a.closeAccordion() : a.openAccordion(d);
                    }
                    return c.hasClass("j-millemiglia__service") ? (b.preventDefault(), c.hasClass("isActive") ? a.closeService() : a.openService(c)) : c.hasClass("millemiglia__service__close") ? (b.preventDefault(), 
                    a.closeService()) : void 0;
                });
            }
        } ]), a;
    }(), b = $(".millemiglia");
    if (b.length) {
        new a();
    }
});

var _createClass = function() {
    function a(a, b) {
        for (var c = 0; c < b.length; c++) {
            var d = b[c];
            d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), 
            Object.defineProperty(a, d.key, d);
        }
    }
    return function(b, c, d) {
        return c && a(b.prototype, c), d && a(b, d), b;
    };
}();

var cssTransitionEnd = "webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", cookieBar_height = 0, header_status;

$(function() {
    function a(a) {
        $(".heroCarousel__indicatorsItem").eq(0).addClass("active");
    }
    function b(a) {
        var b = $(".heroCarousel__indicatorsItem");
        b.removeClass("active"), b.eq(a.currentSlideNumber - 1).addClass("active");
    }
    function c(a) {
        $(".next, .prev").removeClass("unselectable"), 1 == a.currentSlideNumber ? $(".heroCarousel__prev").addClass("unselectable") : a.currentSliderOffset == a.data.sliderMax && $(".heroCarousel__next").addClass("unselectable");
    }
    $("#departureDate").on("change", function(a) {
        $("#returnDate").is(":disabled") ? $(".flightFinder, .innerFlightFinder").removeClass("ff-stage4").addClass("ff-stage5") : "" !== $("#returnDate").val() ? $(".flightFinder, .innerFlightFinder").removeClass("ff-stage4").addClass("ff-stage5") : $("#returnDate").focus();
    }), $("#returnDate").on("change", function(a) {
        "" !== $("#departureDate").val() ? $(".flightFinder, .innerFlightFinder").removeClass("ff-stage4").addClass("ff-stage5") : $("#departureDate").focus();
    }), $(".j-responsiveTable").responsiveTable({
        maxWidth: 640
    }), $("#arrivalInput").on("focus", function() {
        $(".flightFinder, .innerFlightFinder").removeClass("ff-stage1").addClass("ff-stage4");
    }), listSorter($(".specialOffers__list:visible"), "name"), $(".j-sorter").on("change", function(a) {
        var b = $(".specialOffers__list:visible"), c = $(this).find("input").val();
        listSorter(b, c);
    }), $(".j-messageSorter").fastClick(function(a) {
        a.preventDefault();
        var b = $(this).data("sorter");
        messageSorter(b), $(".j-messageSorter").removeClass("isActive"), $(this).addClass("isActive");
    }), $(".j-openFooter").on("click", function(a) {
        $(this).toggleClass("isActive"), $(".footer__row.first").toggleClass("isActive");
    }), $(".j-openAccordionMobile").on("click", function() {
        var a = $(this), b = a.hasClass("isActive");
        $(".j-openAccordionMobile").removeClass("isActive"), b || a.addClass("isActive");
    }), $(".j-openFooterSections").on("click", function() {
        var a = $(this), b = a.closest(".mobileExpendable").hasClass("isActive");
        $(".mobileExpendable").removeClass("isActive"), b || a.closest(".mobileExpendable").addClass("isActive");
    }), $(".j-flightFinder__containerClose").on("click", function() {
        $(".flightFinder, .innerFlightFinder").removeClass("ff-stage2 ff-stage3 ff-stage4 ff-stage5").addClass("ff-stage1"), 
        $("#arrivalInput, #andata, #ritorno").val(""), $(".j-numberSelector-input").val(0), 
        $(".adulti .j-numberSelector-input").val(1);
    }), $("input.j-typeOfFlight").on("change", function(a) {
        var b = $(this).val();
        "a" === b ? ($("#ritorno").attr("disabled", "disabled").attr("aria-disabled", "true"), 
        $("#returnDate").attr("disabled", "disabled").attr("aria-disabled", "true").parent().velocity({
            opacity: .3
        })) : ($("#ritorno").removeAttr("disabled").removeAttr("aria-disabled"), $("#returnDate").removeAttr("disabled").removeAttr("aria-disabled").parent().velocity({
            opacity: 1
        })), highlightPeriod();
    }), $(".j-cookieBar").length > 0 && cookieController(), stickyHeaderNavigation(), 
    numberSelector(), $(window).on("load", function() {
        fixPositions();
    }), $(".j-ajaxLoad, .j-ajaxLoadIframe").each(function(a, b) {
        var c = $(this), d = c.attr("href");
        c.attr("data-link", d).removeAttr("target");
    }), $(".j-ajaxLoad").on("click", function(a) {
        a.preventDefault();
        var b = $(this).attr("data-link"), c = $(this).attr("data-overlayClass") || "";
        $.ajax({
            url: b,
            context: document.body
        }).done(function(a) {
            var b = '<div class="overlay__bg j-overlayBg"></div><div class="overlay__container j-overlayContainer ' + c + ' ">' + a + '</div><div class="overlay__closeCont j-overlayCloseCont"><a class="overlay__close j-overlayClose" href="#">&times;</a></div>';
            $("body").append(b).addClass("overlayActive"), $(".j-overlayBg, .j-overlayContainer, .j-overlayCloseCont, .j-overlayClose").fadeIn(), 
            $(".j-overlayClose").css("display", "inline-block"), $(".j-overlayClose, .j-overlayBg").on("click", function(a) {
                a.preventDefault(), $(".j-overlayBg, .j-overlayContainer, .j-overlayClose").fadeOut("normal", function() {
                    $(this).remove();
                }), $("body").removeClass("overlayActive");
            });
        });
    }), $(".j-ajaxLoadIframe").on("click", function(a) {
        a.preventDefault();
        var b = $(this).attr("data-link"), c = '<iframe border="0" src="' + b + '"></iframe>', d = '<div class="overlay__bg j-overlayBg"></div><div class="overlay__container j-overlayContainer">' + c + '</div><div class="overlay__closeCont j-overlayCloseCont"><a class="overlay__close j-overlayClose" href="#">&times;</a></div>';
        $("body").append(d).addClass("overlayActive"), $(".j-overlayBg, .j-overlayContainer, .j-overlayCloseCont, .j-overlayClose").fadeIn(), 
        $(".j-overlayClose").css("display", "inline-block"), $("iframe").contents().find(".closePopup").on("click", function(a) {
            a.preventDefault(), $(".j-overlayBg, .j-overlayContainer, .j-overlayClose").fadeOut("normal", function() {
                $(this).remove();
            }), $("body").removeClass("overlayActive");
        }), $(".j-overlayClose, .j-overlayBg").on("click", function(a) {
            a.preventDefault(), $(".j-overlayBg, .j-overlayContainer, .j-overlayClose").fadeOut("normal", function() {
                $(this).remove();
            }), $("body").removeClass("overlayActive");
        });
    }), $(".j-accordionHead").on("click", function(a) {
        var b = $(this).data("accordion");
        $('.j-accordionHead[data-accordion="' + b + '"], .j-accordionContainer[data-accordion="' + b + '"]').toggleClass("isActive");
    }), $(".j-sliderDefault").iosSlider({
        desktopClickDrag: !0,
        snapToChildren: !0,
        autoSlide: !0,
        autoSlideTimer: 8e3,
        infiniteSlider: !0
    }), $(".heroCarousel").iosSlider({
        desktopClickDrag: !0,
        snapToChildren: !0,
        autoSlide: !0,
        autoSlideTimer: 8e3,
        infiniteSlider: !0,
        navSlideSelector: ".heroCarousel__indicatorsItem",
        navNextSelector: $(".heroCarousel__next"),
        navPrevSelector: $(".heroCarousel__prev"),
        onSliderLoaded: a,
        onSlideChange: b,
        onSlideComplete: c
    }), $(".j-tabs ul li a").on("click", function(a) {
        a.preventDefault();
        var b = $(this), c = 0;
        return $(".j-tabs ul li, .j-tabs ul li div").removeClass("tabActive"), b.parent().addClass("tabActive"), 
        c = b.data("tab"), $(".j-tabsContainer").removeClass("tabActive"), $('.j-tabsContainer[data-tab="' + c + '"]').addClass("tabActive"), 
        !1;
    }), $(".j-openOverlay").on("click", function() {
        var a = $(this).data("overlay");
        $('.overlay__container[data-overlay="' + a + '"]').addClass("isActive"), $("body").addClass("overlayActive");
    }), $(".j-closePopup").on("click", function() {
        $("body, .overlayBg").removeClass("overlayActive"), $(this).closest(".overlay__container").removeClass("isActive");
    }), $("#departureInput, #arrivalInput").on("keyup focus", function(a) {
        var b = $(this).val();
        3 > b.length ? $(this).next().fadeIn() : $(this).next().fadeOut();
    }).on("blur", function() {
        $(".flightFinder__suggest").fadeOut();
    }), $(".j-switchDestination").fastClick(function() {
        var a = $("#departureInput").val(), b = $("#arrivalInput").val(), c = "", d = "";
        dep_p_1 = a.slice(0, a.length - 3), dep_p_2 = a.slice(a.length - 3, a.length), selectorDeparture = $(".customInput--flightFinder.departure"), 
        arr_p_1 = b.slice(0, b.length - 3), arr_p_2 = b.slice(b.length - 3, b.length), selectorArrival = $(".customInput--flightFinder.arrival"), 
        selectorArrival.find(".j-countryCode").val(dep_p_2), selectorArrival.find(".apt").text(dep_p_1), 
        selectorArrival.find(".city").text(dep_p_2), c = dep_p_1 + " " + dep_p_2, $("#arrivalInput").val(c.trim()), 
        selectorDeparture.find(".j-countryCode").val(arr_p_2), selectorDeparture.find(".apt").text(arr_p_1), 
        selectorDeparture.find(".city").text(arr_p_2), d = arr_p_1 + " " + arr_p_2, $("#departureInput").val(d.trim());
    }), $(".flightFinder__suggest .autocomplete-suggestion").click(function(a) {
        a.preventDefault();
        var b = $(this).find(".sugg_city_name").text(), c = $(this).find(".sugg_airportCode").text().replace("(", "").replace(")", ""), d = $(this).closest(".customInput--flightFinder");
        d.find(".city").text(c), d.find(".apt").text(b), d.find(".j-autoCompleteInput").val(b + " " + c), 
        d.find(".j-countryCode").val(c), d.hasClass("arrival") && (datePickerController.show("andata"), 
        $("input.j-typeOfFlight").eq(0).focus());
    }), $(".flightSel__infoText").each(function() {
        var a = $(this).find("p").eq(0);
        a.length > 0 && a.prepend('<span class="i-infoCircle"></span>');
    }), $(".j-showHideToggle").fastClick(function() {
        var a = $(this).closest(".j-showHide"), b = a.find(".j-showHideDetail");
        $(".j-showHide").removeClass("opened"), $(".j-showHideDetail").velocity("slideUp");
        var c = b.is(":visible");
        b.velocity(c ? "slideUp" : "slideDown", {
            complete: function() {
                c || a.addClass("opened");
            }
        });
    });
}), $(window).resize(function() {
    var a;
    clearTimeout(a), a = setTimeout(function() {
        destinationCarousel(), dropDownInit();
    }, 50), dateMatrixOnResize(), $(document).on("gestureend", function(a) {
        var b = a.originalEvent.scale;
        b > 1 ? $("body").addClass("isPinched") : $("body").removeClass("isPinched");
    });
});