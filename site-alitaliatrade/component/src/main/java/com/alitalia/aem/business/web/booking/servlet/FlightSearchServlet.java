package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.booking.BookingSearchCUGEnum;
import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.BookingSession;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.booking.model.SearchElement;
import com.alitalia.aem.business.web.booking.model.SearchElementAirport;
import com.alitalia.aem.business.web.booking.model.SearchPassengersNumber;
import com.alitalia.aem.business.web.statistics.utils.TradeStatisticUtils;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.alitalia.aem.business.web.utils.TypeUtils;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.common.data.home.enumerations.ResidencyTypeEnum;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "flightsearch" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome."),
		@Property(name = "continuita-territoriale-sardegna.page", description = "Users will be redirected to the specified page after a warning outcome."),
		@Property(name = "continuita-territoriale-sicilia.page", description = "Users will be redirected to the specified page after a warning outcome.") })
@SuppressWarnings("serial")
public class FlightSearchServlet extends GenericFormValidatorServlet {

	private ComponentContext componentContext;

	@Reference
	private BookingSession bookingSession;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Reference
	private AlitaliaTradeConfigurationHolder configuration;

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {

		Validator validator = new Validator();

		// departure airport
		validator.addDirectParameter("departureInput",
				request.getParameter("departureInput"),
				Constants.MESSAGE_AIRPORT_FROM_NOT_VALID, "isAirport");

		// arrival airport
		validator.addDirectParameter("arrivalInput",
				request.getParameter("arrivalInput"),
				Constants.MESSAGE_AIRPORT_TO_NOT_VALID, "isAirport");

		// start date
		validator.addDirectParameter("start", request.getParameter("start"),
				Constants.MESSAGE_DATE_PAST, "isDate", "isFuture");

		// end date
		String end = request.getParameter("end");
		if (end != null && !end.isEmpty()) {
			validator.addDirectParameter("end", end,
					Constants.MESSAGE_DATE_PAST, "isDate", "isFuture");
		}

		// check if departure airport and arrival airport are different
		validator.addCrossParameter("arrivalInput",
				request.getParameter("departureInput"),
				request.getParameter("arrivalInput"),
				Constants.MESSAGE_FROM_TO_DIFF, "areDifferent");

		// check if start date is before end date
		if ("flighttype2".equals(request.getParameter("flighttype"))) {
			validator.addCrossParameter("end", request.getParameter("start"),
					request.getParameter("end"), Constants.MESSAGE_DATE_PAST,
					"beforeThen");
		}
		
		/* check for the CUG */
		String cug = request.getParameter("cug");
		validator.addDirectParameter("cug", cug,
				Constants.MESSAGE_CUG_NOT_VALID, "isCug");
		
		if( cug!=null && ("YTH").equals(cug) ) {
			validator.addDirectParameter("nyoung", 
					request.getParameter("nyoung"),
					Constants.MESSAGE_NYOUNGS_NOT_VALID, "isNotEmpty", "isNumber");
		}else{
		
			// number of adults
			validator.addDirectParameter("nadults",
					request.getParameter("nadults"),
					Constants.MESSAGE_NADULTS_NOT_VALID, "isRangeAdults");

			// number of kids
			String nkids = request.getParameter("nkids");
			if (nkids != null && !nkids.isEmpty()) {
				validator.addDirectParameter("nkids", nkids,
						Constants.MESSAGE_NKINDS_NOT_VALID, "isRangeKids");
			}

			// number of babies
			String nbabies = request.getParameter("nbabies");
			if (nbabies != null && !nbabies.isEmpty()) {
				validator.addDirectParameter("nbabies", nbabies,
						Constants.MESSAGE_NBABIES_NOT_VALID, "isRangeBabies");
			}
			
			// check sum of adults and kids
			validator.addCrossParameter("nadults", request.getParameter("nadults"),
					request.getParameter("nkids"),
					Constants.MESSAGE_SUM_ADULTS_KIDS_NOT_VALID,
					"underMaxSumAdultsKids");
			
			// check if number of babies is lower then number of adults
			if (nbabies != null && !nbabies.isEmpty()) {
				validator.addCrossParameter("nbabies",
						request.getParameter("nadults"),
						request.getParameter("nbabies"),
						Constants.MESSAGE_BABY_PER_ADULT_NOT_VALID, "babyPerAdult");
			}
			
			if (cug!=null && ("FAM").equals(cug)) {
				
				validator.addDirectParameter("nadults", request.getParameter("nadults"),
						Constants.MESSAGE_NADULTS_NOT_VALID, "isRangeAdultForFamily");
				
				validator.addCrossParameter("nkids",
						request.getParameter("nkids"),
						request.getParameter("nbabies"),
						Constants.MESSAGE_NKINDS_NOT_VALID, "isRangeKidsAndBabiesForFamily");
				
				validator.addCrossParameter("nbabies",
						request.getParameter("nkids"),
						request.getParameter("nbabies"),
						Constants.MESSAGE_NBABIES_NOT_VALID, "isRangeKidsAndBabiesForFamily");
			}
			
		}
		
		

		return validator.validate();
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		
		try {
			
			// prepare search data from received parameters
			String departureInput = request.getParameter("departureInput");
			String arrivalInput = request.getParameter("arrivalInput");
			String departureDate = request.getParameter("start");
			String returnDate = request.getParameter("end");
			String cugSearch = request.getParameter("cug") == null ? "ADT" : request.getParameter("cug");
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
			dateFormat.setLenient(false);
			
			BookingSearchKindEnum searchKind = BookingSearchKindEnum.SIMPLE;
			List<SearchElement> searchElements = new ArrayList<SearchElement>();
			SearchPassengersNumber searchPassengersNumber = new SearchPassengersNumber();
			BookingSearchCUGEnum cug = BookingSearchCUGEnum.fromValue(cugSearch);
			String market = "LU";
			String site = "LU";
			Locale locale = LocalizationUtils.getLocaleByResource(request.getResource(), "it");
			
			SearchElementAirport fromElement = new SearchElementAirport(departureInput);
			SearchElementAirport toElement = new SearchElementAirport(arrivalInput);
			
			searchElements.add(new SearchElement(
					fromElement, toElement, TypeUtils.createCalendar(dateFormat.parse(departureDate))));
			
			if (returnDate != null && !"".equals(returnDate)) {
				searchElements.add(new SearchElement(
						toElement, fromElement, TypeUtils.createCalendar(dateFormat.parse(returnDate))));
				searchKind = BookingSearchKindEnum.ROUNDTRIP;
			}
			
			if (cug == BookingSearchCUGEnum.YOUTH) {
				searchPassengersNumber.setNumYoung(Integer.parseInt(request.getParameter("nyoung")));
			} else {
				searchPassengersNumber.setNumAdults(Integer.parseInt(request.getParameter("nadults")));
				searchPassengersNumber.setNumChildren(Integer.parseInt(request.getParameter("nkids")));
				searchPassengersNumber.setNumInfants(Integer.parseInt(request.getParameter("nbabies")));
			}
			
			// create a new booking session context and save it in the session
			BookingSessionContext ctx = bookingSession.initializeBookingSession("alitaliaTradeBooking");
			
			//INIZIO *** MODIFICA IP ***
			String[] clientIpHeaders = configuration.getClientIpHeaders();
			String ipAddress = "";
			ipAddress = TradeStatisticUtils.getClientIP(clientIpHeaders, request);
			if (ipAddress != null && !ipAddress.isEmpty()) {
				ctx.ipAddress = ipAddress;
			}
			//FINE.
			
			request.getSession(true).setAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE, ctx);
			
			ResourceBundle resourceBundle = request.getResourceBundle(locale);
			final I18n i18n = new I18n(resourceBundle);
			
			String codiceAgenzia = "";
			String codiceAccordo = "";
			String partitaIva = "";
			ResourceResolver resourceResolver = request.getResourceResolver();
			Session session = resourceResolver.adaptTo(Session.class);
			UserManager userManager = resourceResolver.adaptTo(UserManager.class);
			String userID = session.getUserID();
		    Authorizable auth = userManager.getAuthorizable(userID);
		    
		    if (auth.getProperty("codiceAgenzia") != null 
		    		&& auth.getProperty("codiceAgenzia").length > 0) {
		    	codiceAgenzia = auth.getProperty("codiceAgenzia")[0].getString();
		    }
		    
		    if (auth.getProperty("codiceAccordo") != null 
		    		&& auth.getProperty("codiceAccordo").length > 0) {
		    	codiceAccordo = auth.getProperty("codiceAccordo")[0].getString();
		    }
		    
		    if (auth.getProperty("partitaIva") != null 
		    		&& auth.getProperty("partitaIva").length > 0) {
		    	partitaIva = auth.getProperty("partitaIva")[0].getString();
		    }
		    
			// prepare the search data in the booking session
			bookingSession.prepareSearch(ctx, i18n, searchKind, searchElements,
					searchPassengersNumber, cug, market, site, locale, codiceAgenzia, partitaIva, codiceAccordo, request);
			
			// redirect to the correct page
			String successPagePlain = (String) 
					componentContext.getProperties().get("success.page");
			String successPageContinuitaSicilia = (String) 
					componentContext.getProperties().get("continuita-territoriale-sicilia.page");
			String successPageContinuitaSardegna = (String) 
					componentContext.getProperties().get("continuita-territoriale-sardegna.page");
			
			String successPage = null;
			if (ctx.isAllowedContinuitaTerritoriale) {
				if (ctx.residency == ResidencyTypeEnum.SARDINIA) {
					successPage = successPageContinuitaSardegna;
				} else { //ctx.residency == ResidencyTypeEnum.SICILY
					successPage = successPageContinuitaSicilia;
				}
			} else { 
				successPage = successPagePlain;
			}
			
			if (successPage != null && !"".equals(successPage)) {
				response.sendRedirect(response.encodeRedirectURL(successPage));
			}
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			
			if (!response.isCommitted()) {
				String failurePage = (String) componentContext.getProperties().get("failure.page");
				if (failurePage != null && !"".equals(failurePage)) {
					response.sendRedirect(response.encodeRedirectURL(failurePage));
				} else {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
			}
		}

	}

}
