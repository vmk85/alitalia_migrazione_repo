package com.alitalia.aem.business.web.booking.render;

import java.text.SimpleDateFormat;
import java.util.Locale;

import com.alitalia.aem.common.data.home.FlightTabData;

public class AvailableRibbonTabsRender{
	private FlightTabData tabData;
	private DateRender date;
	private String weekDay;
	private GenericPriceRender price;
	private String currency;
	private boolean isAvailable;
	
	public AvailableRibbonTabsRender(FlightTabData tab, Locale locale){
		this.tabData = tab;
		this.date = new DateRender(tab.getDate());
		this.weekDay = computeWeekDay(tab,locale);
		this.price =  new GenericPriceRender(tab.getPrice(), currency, locale);
		this.currency = tab.getCurrency();
		this.isAvailable = computeIsAvailable(tab);
	}
	
	public FlightTabData getTabData(){
		return tabData;
	}
	
	
	public DateRender getDate() {
		return date;
	}

	public String getWeekDay() {
		return weekDay;
	}

	public GenericPriceRender getPrice() {
		return price;
	}
	
	public void setPrice(GenericPriceRender price) {
		this.price = price;
	}

	public String getCurrency() {
		return currency;
	}

	public boolean isAvailable() {
		return isAvailable;
	}


	
	/* private methods */

	private boolean computeIsAvailable(FlightTabData tab) {
		return (tab.getPrice() != null && tab.getPrice().doubleValue() > 0);
	}
	
	private String computeWeekDay(FlightTabData tab, Locale locale) {
		SimpleDateFormat sdf = new SimpleDateFormat("E",locale);
		return (sdf.format(tab.getDate().getTime())).toUpperCase();
	}
}