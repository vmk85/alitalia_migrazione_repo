package com.alitalia.aem.business.web.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.sling.api.scripting.SlingScriptHelper;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.HistogramItem;
import com.alitalia.aem.business.web.utils.ListOffersUtils;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.BestPriceData;
import com.alitalia.aem.common.data.home.OfferData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.BestPriceTypeEnum;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.geospatial.delegate.GeoSpatialDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;

public class BoxIstogramma extends WCMUse {
	
	private String destinationTitle;
	private String histogramTitle;
	private String bestPrice;
	private boolean domestic;
	private boolean young;
	List<HistogramItem> histogramList;
	
	private String departure;
	private String arrival;
	
	@Override
    public void activate() throws Exception {
		
		SlingScriptHelper helper = getSlingScriptHelper();
		StaticDataDelegate staticDataDelegate = helper.getService(StaticDataDelegate.class);
		
		RetrieveAirportsRequest airportsRequest = new RetrieveAirportsRequest(IDFactory.getTid(), IDFactory.getSid(getRequest()));
		airportsRequest.setMarket("IT");
		airportsRequest.setLanguageCode("IT");
		RetrieveAirportsResponse airportsResponse = staticDataDelegate.retrieveAirports(airportsRequest);
		
		ArrayList<String> airportList = new ArrayList<String>();
		List<AirportData> airportDataList = airportsResponse.getAirports();
		
		if (airportDataList != null && ! airportDataList.isEmpty()) {
			for (AirportData airportData : airportDataList) {
				airportList.add(airportData.getCode());
			}
		}
		
		if (!airportList.contains(getRequest().getParameter("from")) || !airportList.contains(getRequest().getParameter("to"))) {
			return;
		}
		
		String[] monthList = {"Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno",
				"Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"};
		
		String[] dayNames = {"LUN", "MAR", "MER", "GIO", "VEN", "SAB", "DOM"};
		
		departure = getRequest().getParameter("from");
		arrival = getRequest().getParameter("to");
		destinationTitle = "airportsData." + arrival + ".city";
		boolean isDom =  Boolean.parseBoolean(getRequest().getParameter("isDOM"));
		int id = Integer.parseInt(getRequest().getParameter("id"));
		
		young = getRequest().getParameter("isYoung") != null && getRequest().getParameter("isYoung").equals("true");
		getRequest().setAttribute("isYoung", young);
		String paxType = young ? "yg" : "ad";
		
		GeoSpatialDelegate geoSpatialDelegate = helper.getService(GeoSpatialDelegate.class);
		
		RetrieveGeoOffersRequest serviceRequest = new RetrieveGeoOffersRequest();
		
		serviceRequest.setSid(IDFactory.getSid(getRequest()));
		serviceRequest.setTid(IDFactory.getTid());
		
		serviceRequest.setLanguageCode("IT");
		serviceRequest.setMarketCode("IT");
		serviceRequest.setDepartureAirportCode(departure.toLowerCase());
		serviceRequest.setPaxType(paxType);
		
		RetrieveGeoOffersResponse serviceResponse = null;
		serviceResponse = geoSpatialDelegate.retrieveGeoOffers(serviceRequest);
		
		List<OfferData> offerDataList = serviceResponse.getOffers();
		List<OfferData> filteredOfferDataList = ListOffersUtils.filterOffers(offerDataList);
		//List<OfferData> offerDataList = getOfferDataList();
		
		OfferData offerData = null;
		
		// Recupero l'oggetto offerData per la destinazione selezionata;
		int cont = 0;
		for (OfferData currentItem : filteredOfferDataList) {
			if (isDom) {
				if (currentItem.getArea() == AreaValueEnum.DOM && id == cont) {
					offerData = currentItem;
					break;
				}
				if (currentItem.getArea() == AreaValueEnum.DOM || young) {
					cont++;
				}
			} else {
				if (currentItem.getArea() != AreaValueEnum.DOM && id == cont) {
					offerData = currentItem;
					break;
				}
				if (currentItem.getArea() != AreaValueEnum.DOM || young) {
					cont++;
				}
			}
		}
		
		if (offerData != null) {
			
			AreaValueEnum destinationType = offerData.getArrivalAirport().getArea();
			
			List<BestPriceData> offerList = offerData.getBestPrices();
			if (offerList != null && !offerList.isEmpty()) {
				
				histogramList = new ArrayList<HistogramItem>();
				
				// Se e' una destinazione nazionale visualizzo il mese
				if (destinationType.equals(AreaValueEnum.DOM)) {
					histogramTitle = monthList[offerList.get(0).getDate().get(Calendar.MONTH)] + " " + String.valueOf(offerList.get(0).getDate().get(Calendar.YEAR));
					domestic = true;
				} else {
					domestic = false;
				}
				
				// Per i voli internazionali ogni barra dell'istogramma rappresenta un mese, per i voli nazionali un giorno
				BestPriceTypeEnum bestPriceType = destinationType.equals(AreaValueEnum.DOM) ? BestPriceTypeEnum.DAY : BestPriceTypeEnum.MONTH;
				
				double lowPrice = Double.MAX_VALUE;
				double highPrice = 0.0;
				
				for (BestPriceData offer : offerList) {
					if (offer.getPrice() != null && bestPriceType.equals(offer.getType())) {
						double price = offer.getPrice().doubleValue();
						if (price > highPrice) {
							highPrice = price;
						}
						if (price < lowPrice) {
							lowPrice = price;
						}
					}
				}
				
				bestPrice = String.valueOf(Math.round(lowPrice));
				boolean found = false;
				int i = 0;
				int limit = 15;
				if (offerList.size() < limit) {
					limit = offerList.size();
				}
				while (i < limit) {
					BestPriceData offer = offerList.get(i);
					if (offer.getPrice() != null) {
						HistogramItem item = new HistogramItem();
						double price = offer.getPrice().doubleValue();
						boolean equality = Double.doubleToLongBits(price) == Double.doubleToLongBits(lowPrice);
						
						if (!found && equality) {
							item.setBestPrice(true);
							found = true;
						}
						item.setPrice(String.valueOf(Math.round(price)));
						item.setHeight(computeHeight(price, lowPrice, highPrice));
						
						item.setDay(String.valueOf(offer.getDate().get(Calendar.DAY_OF_MONTH)));
						
						SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
						item.setMonth(monthFormat.format(offer.getDate().getTime()));
						
						SimpleDateFormat yearFormat = new SimpleDateFormat("yy");
						item.setYear(yearFormat.format(offer.getDate().getTime()));
						
						if (bestPriceType.equals(BestPriceTypeEnum.DAY)) {
							item.setDayName(dayNames[offer.getDate().get(Calendar.DAY_OF_WEEK) - 1]);
						} else if (bestPriceType.equals(BestPriceTypeEnum.MONTH)) {
							item.setMonthName(monthList[offer.getDate().get(Calendar.MONTH)] + " " + offer.getDate().get(Calendar.YEAR));
						}
						Calendar returnDate = Calendar.getInstance(); 
						returnDate.setTimeInMillis(offer.getDate().getTimeInMillis());
						returnDate.setTimeZone(offer.getDate().getTimeZone());
						String numberOfNights = offerData.getNumberOfNights();
						if(numberOfNights==null) {
							numberOfNights = "0";
						}
						returnDate.add(Calendar.DATE, Integer.parseInt(numberOfNights));
						String returnDay = String.valueOf(returnDate.get(Calendar.DAY_OF_MONTH));
						String returnMonth = monthFormat.format(returnDate.getTime());
						String returnYear = yearFormat.format(returnDate.getTime());
						String returnDateFormatted = returnDay + "/" + returnMonth + "/" + returnYear;
						item.setReturnDate(returnDateFormatted);
						histogramList.add(item);
					}
					i++;
				}
			}
		}
	}
	
	/*
	 * Metodo di utility per test
	 */
	/*private List<OfferData> getOfferDataList() {
		
		List<OfferData> offerDataList = new ArrayList<OfferData>();
		
		// nazionale (day) / internazionale (month)
		boolean day = true;
		
		OfferData offerData = new OfferData();
		
		Calendar expireData = Calendar.getInstance();
		expireData.add(Calendar.HOUR, 2);
		
		offerData.setExpireOfferData(expireData);
		
		AirportData arrivalAirport = new AirportData();
		arrivalAirport.setCode(getRequest().getParameter("to"));
		arrivalAirport.setArea(day ? AreaValueEnum.DOM : AreaValueEnum.INTC);
		offerData.setArrivalAirport(arrivalAirport);
		
		AirportData departureAirport = new AirportData();
		departureAirport.setCode(getRequest().getParameter("from"));
		offerData.setDepartureAirport(departureAirport);
		
		offerData.setBestPrice(new BigDecimal(new BigInteger("100")));
		
		List<BestPriceData> bestPriceList = new ArrayList<BestPriceData>();
		
		int size = day ? 10 : 5;
		
		for (int i = 0; i < size; i++) {
			Calendar date = Calendar.getInstance();
			if (day) {
				date.set(Calendar.DAY_OF_MONTH, 12 + i);
				date.set(Calendar.MONTH, 4);
			} else {
				date.set(Calendar.DAY_OF_MONTH, 12);
				date.set(Calendar.MONTH, 3 + i);
			}
			date.set(Calendar.YEAR, 2015);
			BestPriceData item = new BestPriceData();
			item.setDate(date);
			item.setPrice(new BigDecimal(100 + 100 * i));
			item.setType(day ? BestPriceTypeEnum.DAY : BestPriceTypeEnum.MONTH);
			
			bestPriceList.add(item);
		}
		offerData.setBestPrices(bestPriceList);
		
		offerDataList.add(offerData);
		
		return offerDataList;
	}*/
	
	/*
	 * Calcola altezza elemento istogramma
	 */
	private String computeHeight(double price, double lowPrice, double highPrice) {
		
		String height;
		
		double max = 180;
		double min = 120;
		double deltaPx = max - min;
		double deltaP = highPrice - lowPrice;
		double deltaPrezzo = highPrice - price;
		boolean equalityWithZero = Double.doubleToLongBits(deltaP) == Double.doubleToLongBits(0);
		if (equalityWithZero) {
			height =  Double.toString(min);
		} else {
			double x = (deltaPx * deltaPrezzo) / deltaP;
			double h = max - x;
			height = Double.toString(h);
		}
		return height;
	}
	
	public String getDestinationTitle() {
		return destinationTitle;
	}
	
	public String getHistogramTitle() {
		return histogramTitle;
	}
	
	public boolean isDomestic() {
		return domestic;
	}
	
	public boolean isYoung() {
		return young;
	}
	
	public List<HistogramItem> getHistogramList() {
		return histogramList;
	}
	
	public String getDeparture() {
		return departure;
	}
	
	public String getArrival() {
		return arrival;
	}

	public String getBestPrice() {
		return bestPrice;
	}
	
}