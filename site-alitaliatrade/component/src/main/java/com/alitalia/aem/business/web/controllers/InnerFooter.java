package com.alitalia.aem.business.web.controllers;

import java.util.HashMap;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUse;

public class InnerFooter extends WCMUse{
	
	private Map<String,String> image;
	
	@Override
    public void activate() throws Exception {
		
		image = new HashMap<String,String>();
		/*image.put("msg"," ");
		String defaultMsg = "Clicca qui per aggiungere il banner logo";*/
	
		Resource local = getResource();
		Resource imageResource = local.getChild("image");
		
		ValueMap properties = null;
		try{
			properties = imageResource.getValueMap();
        }catch(NullPointerException e){
        	/*if(getWcmMode().isEdit()){
        		image.put("msg",defaultMsg);
        	}*/
		}
        
        if(properties!=null){
       
        	String fileRef = properties.get("fileReference", String.class);
        	//if(fileRef != null)
        		image.put("img", fileRef);
        	/*else
        		image.put("msg",defaultMsg);*/
        }
		
	}
	
	public Map<String,String> getImage(){
		return image;
	}
	

}

