package com.alitalia.aem.business.web.booking.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.booking.model.SearchElement;
import com.alitalia.aem.business.web.booking.render.AvailableRibbonTabsRender;
import com.alitalia.aem.business.web.booking.render.GenericPriceRender;
import com.alitalia.aem.business.web.booking.render.InfoSliceRibbonTabsRender;
import com.alitalia.aem.common.data.home.FlightTabData;
import com.alitalia.aem.common.data.home.TabData;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;

public class BookingScegliVoloRibbon extends BookingSessionGenericController {

	private List<AvailableRibbonTabsRender> ribbonTabsList;
	private List<InfoSliceRibbonTabsRender> multilegTabsList;
	private int routeIndex;
	private int indexActiveTab;
	private boolean multileg;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
    public void activate() throws Exception {
		super.activate();
		
		try{
			SlingHttpServletRequest request = getRequest();
			
			if (isIllegalCTXState(getResponse())){
				return;
			}
			
			if( ( request.getParameter("direction")==null || ("").equals(request.getParameter("direction")) ) 
					|| ( !("1").equals(request.getParameter("direction")) 
							&& !("0").equals(request.getParameter("direction")) ) ){
				logger.error("direction of the route not found");
				throw new RuntimeException("direction of the route not found");
			}
			routeIndex = Integer.parseInt(request.getParameter("direction"));
			multileg=false;
			
			if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
				multileg = true;
				indexActiveTab = ctx.currentSliceIndex-1;
				multilegTabsList = prepareMultilegTabsList(routeIndex,ctx);
			} else {
				if (ctx.availableFlights.getTabs() != null && ctx.availableFlights.getTabs().size() > 0) {
					if(routeIndex == 1){
						if(ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP){
							ribbonTabsList = prepareRibbonTabsList(routeIndex,ctx,ctx.locale);
							indexActiveTab = ctx.selectedDepartureDateChoices[routeIndex];
						}
					}else{
						ribbonTabsList = prepareRibbonTabsList(routeIndex,ctx,ctx.locale);
						indexActiveTab = ctx.selectedDepartureDateChoices[routeIndex];
					}
					if (routeIndex == ctx.totalSlices-1) {
						addExtraChargeToFare(ctx.totalExtraCharges, ctx);
					}
				}
			}
			
		}catch(Exception e){
			logger.error("unexpected error ", e);
			throw e;
		}

	}
	
	public List<AvailableRibbonTabsRender> getRibbonTabsList(){
		return ribbonTabsList;
	}
	
	public int getRouteIndex(){
		return routeIndex;
	}
	
	public int getIndexActiveTab(){
		return indexActiveTab;
	}
	
	public List<InfoSliceRibbonTabsRender> getMultilegTabsList() {
		return multilegTabsList;
	}

	public boolean isMultileg() {
		return multileg;
	}
	
	
	/* private methods */
	
	/**
	 * It sum the extra charge price to the tab fare
	 * @param totalExtraCharges
	 * @param ctx
	 */
	private void addExtraChargeToFare(BigDecimal totalExtraCharges, BookingSessionContext ctx) {
		for (AvailableRibbonTabsRender tabRender : this.ribbonTabsList) {
			GenericPriceRender currentPriceRender = tabRender.getPrice();
			BigDecimal newPrice = new BigDecimal(0);
			newPrice = newPrice.add(tabRender.getTabData().getPrice());
			newPrice = newPrice.add(totalExtraCharges);
			GenericPriceRender newPriceRender = new GenericPriceRender(
					newPrice, currentPriceRender.getCurrency(), ctx.locale);
			tabRender.setPrice(newPriceRender);
		}
		
	}
	
	/**
	 * 
	 * @param routeIndex is the index of the route speficified by the call of this partial
	 * @param ctx 
	 * @return the tabs list
	 */
	private List<AvailableRibbonTabsRender> prepareRibbonTabsList(int routeIndex, BookingSessionContext ctx, Locale locale){
		List<AvailableRibbonTabsRender> ribbonTabsList = new ArrayList<AvailableRibbonTabsRender>();
		
		TabData routeTab = ctx.availableFlights.getTabs().get(routeIndex);
		if(routeIndex==0 && routeTab.getType()==RouteTypeEnum.RETURN){
			routeTab = ctx.availableFlights.getTabs().get(1);
		}
		if(routeIndex==1 && routeTab.getType()==RouteTypeEnum.OUTBOUND){
			routeTab = ctx.availableFlights.getTabs().get(0);
		}
		if(routeTab.getFlightTabs()==null || routeTab.getFlightTabs().size()==0){
			logger.error("Unexpected error - FlightTabs not found");
			throw new RuntimeException("Unexpected error - FlightTabs not found");
		}
		
		for(FlightTabData tabData : routeTab.getFlightTabs()){
			AvailableRibbonTabsRender tabRibbon = new AvailableRibbonTabsRender(tabData,locale);
			ribbonTabsList.add(tabRibbon);
		}
		
		return ribbonTabsList;
	}
	
	/**
	 * 
	 * @param routeIndex is the index of the route speficified by the call of this partial
	 * @param ctx 
	 * @return the tabs list
	 */
	private List<InfoSliceRibbonTabsRender> prepareMultilegTabsList(
			int routeIndex, BookingSessionContext ctx) {
		List<InfoSliceRibbonTabsRender> multilegTabsList = new ArrayList<InfoSliceRibbonTabsRender>();
		
		int sliceIndex = 1;
		for (SearchElement searchElement : ctx.searchElements) {
			InfoSliceRibbonTabsRender infoSliceTab = 
					new InfoSliceRibbonTabsRender(sliceIndex, searchElement.getDepartureDate(), 
							searchElement.getFrom().getLocalizedCityName(), searchElement.getTo().getLocalizedCityName());
			multilegTabsList.add(infoSliceTab);
			sliceIndex++;
		}
		
		return multilegTabsList;
	}
	
}