package com.alitalia.aem.business.web.controllers;

import java.util.ArrayList;
import java.util.List;

import com.adobe.cq.sightly.WCMUse;

public class FormBookingMultitratta extends WCMUse {
	
	private List<String> tratte = new ArrayList<String>();
	
	@Override
	public void activate() throws Exception {
		
		tratte.add("Prima");
		tratte.add("Seconda");
		tratte.add("Terza");
		tratte.add("Quarta");
		
	}

	public List<String> getTratte() {
		return tratte;
	}
	
}
