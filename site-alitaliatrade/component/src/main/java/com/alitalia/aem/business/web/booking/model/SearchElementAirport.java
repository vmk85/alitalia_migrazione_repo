package com.alitalia.aem.business.web.booking.model;

public class SearchElementAirport {
	
	private String airportCode;
	private String airportName;
	private String localizedAirportName;
	private String cityCode;
	private String cityName;
	private String localizedCityName;
	private String countryCode;
	private String countryName;
	private String localizedCountryName;
	
	public SearchElementAirport(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	public String getLocalizedAirportName() {
		return localizedAirportName;
	}

	public void setLocalizedAirportName(String localizedAirportName) {
		this.localizedAirportName = localizedAirportName;
	}

	public String getLocalizedCityName() {
		return localizedCityName;
	}

	public void setLocalizedCityName(String localizedCityName) {
		this.localizedCityName = localizedCityName;
	}

	public String getLocalizedCountryName() {
		return localizedCountryName;
	}

	public void setLocalizedCountryName(String localizedCountryName) {
		this.localizedCountryName = localizedCountryName;
	}

	@Override
	public String toString() {
		return "SearchElementAirport [airportCode=" + airportCode + ", airportName=" + airportName
				+ ", localizedAirportName=" + localizedAirportName + ", cityCode=" + cityCode + ", cityName="
				+ cityName + ", localizedCityName=" + localizedCityName + ", countryCode=" + countryCode
				+ ", countryName=" + countryName + ", localizedCountryName=" + localizedCountryName + "]";
	}
	
}
