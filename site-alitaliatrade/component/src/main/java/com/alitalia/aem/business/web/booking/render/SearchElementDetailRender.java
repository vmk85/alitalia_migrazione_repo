package com.alitalia.aem.business.web.booking.render;

public class SearchElementDetailRender {

	private DateRender date;
	private String airportCode;
	private String cityCode;
	private String cityName;
	
	public SearchElementDetailRender(DateRender date, String airportCode, String cityCode, String cityName) {
		this.date = date;
		this.airportCode = airportCode;
		this.cityCode = cityCode;
		this.cityName = cityName;
	}

	public DateRender getDate() {
		return date;
	}

	public String getAirportCode() {
		return airportCode;
	}
	
	public String getCityCode() {
		return cityCode;
	}
	
	public String getCityName() {
		return cityName;
	}
	
}
