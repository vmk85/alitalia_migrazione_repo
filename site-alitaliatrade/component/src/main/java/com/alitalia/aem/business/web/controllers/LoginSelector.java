package com.alitalia.aem.business.web.controllers;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.*;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.AlitaliaTradeAuthUtils;
import com.alitalia.aem.business.web.utils.CookieUtils;
import com.alitalia.aem.business.web.utils.LocalizationUtils;

public class LoginSelector extends WCMUse {

	private static Logger logger = LoggerFactory.getLogger(LoginSelector.class);

	public String language, market;
	private HashMap<String, String> geoResult;
	private String COOKIE_NAME = "alitaliatrade-consumer-geo";
	private int COOKIE_MAX_AGE = 7776000; // 90 days
	private String cookieValue = null;

	@Override
	public void activate() throws Exception {

		String queryString = "";

		try {

			logger.debug("Verifying home page redirection...");

			// load configuration
			SlingScriptHelper helper = getSlingScriptHelper();

			
			SlingSettingsService slingSettings =
			 helper.getService(SlingSettingsService.class); if (slingSettings
			 == null || !slingSettings.getRunModes().contains("publish")) {
			 logger.debug("Not on a publish instance, ignoring."); return; }
			
			SlingHttpServletRequest request = getRequest();
			// geolocalizzazione
			geoResult = LocalizationUtils.getUserMarketLanguage(helper, request);
			language = geoResult.get("language");
			market = geoResult.get("market");

			ValueMap pageProperties = getCurrentPage().getProperties();

			queryString = getRequestQueryString(request);
			logger.debug("queryString attribute: {}", queryString);

            String link = null;
            // modificare url (placeholder <language>)
            if(language != null) {
                link = (String) pageProperties.get("linklogin_" + language);
            }


            if (link != null) {
                logger.debug("Redirect to {}", link);
                SlingHttpServletResponse response = getResponse();
				response.setStatus(302);
				response.setHeader("Location", link + ".html" + queryString);
				response.setHeader("Connection", "close");
				if (response != null) {
//				    finche' non torna a funzionare la geolocalizzazione di Egon, non scriviamo cookie inutili e con contenuto vuoto
//					cookieValue = CookieUtils.toCookieString(market, language);
//					CookieUtils.setCookieValue(response, COOKIE_NAME, null, "/", COOKIE_MAX_AGE, cookieValue, false, false);
				}
				return;
			} else {
                link = (String) pageProperties.get("linklogin_it");
                if(link.equals("") || link != null){
                    link = "/content/alitaliatrade/it/non-loggato/login";
                }
                logger.debug("Redirect to {}", link);
                SlingHttpServletResponse response = getResponse();
                response.setStatus(302);
                response.setHeader("Location", link + ".html" + queryString);
                response.setHeader("Connection", "close");
                if (response != null) {
//				    finche' non torna a funzionare la geolocalizzazione di Egon, non scriviamo cookie inutili e con contenuto vuoto
//                    cookieValue = CookieUtils.toCookieString(market, language);
//                    CookieUtils.setCookieValue(response, COOKIE_NAME, null, "/", COOKIE_MAX_AGE, cookieValue, false, false);
                }
                return;
            }

		} catch (Exception e) {
			logger.error("Unexpected error", e);
			throw e;
		}

	}

	private String getRequestQueryString(HttpServletRequest request) {
		String queryString = request.getQueryString();
		if (queryString != null && queryString != "") {
			return "?" + queryString;
		} else {
			return "";
		}
	}

}
