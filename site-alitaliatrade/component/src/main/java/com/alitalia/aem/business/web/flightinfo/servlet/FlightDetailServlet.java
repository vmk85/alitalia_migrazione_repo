package com.alitalia.aem.business.web.flightinfo.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.common.data.home.FlightDetailsModelData;
import com.alitalia.aem.common.data.home.FlySegmentData;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsResponse;
import com.alitalia.aem.web.component.flightstatus.delegate.FlightStatusDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "flightdetail" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.")
})
@SuppressWarnings("serial")
public class FlightDetailServlet extends GenericFormValidatorServlet {
	
	private ComponentContext componentContext;
	
	@Reference
	private FlightStatusDelegate flightStatusDelegate;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		Validator validator = new Validator();
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		
		response.setContentType("application/json");
		TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
		
		try {
			out.object();
			String failurePage = (String) componentContext.getProperties().get("failure.page");
			String flightNumber = request.getParameter("flightnum");
			String flightVector = request.getParameter("flightvector");
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
			df.setLenient(false);
			Date departureDate = df.parse(request.getParameter("flightdate"));
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(departureDate);

			RetrieveFlightDetailsResponse serviceResponseDetail = null;
			RetrieveFlightDetailsRequest serviceRequestDetail = new RetrieveFlightDetailsRequest ();
			FlightDetailsModelData flightDetailsModelData = new FlightDetailsModelData();
			flightDetailsModelData.setFlightDate(calendar);
			flightDetailsModelData.setFlightNumber(flightNumber);
			flightDetailsModelData.setVector(flightVector);
			serviceRequestDetail.setFlightDetailsModelData(flightDetailsModelData);
			serviceResponseDetail = flightStatusDelegate.retrieveFlightDetails(serviceRequestDetail);
			
			if (serviceResponseDetail != null && serviceResponseDetail.getFlightDetailsModelData() != null) {
				 String aircraft ="";
				 String miles = "";
				 String duration = "";
				 String plusDays = "";
				 if (serviceResponseDetail.getFlightDetailsModelData().getAirCraft() != null) {
					 aircraft = serviceResponseDetail.getFlightDetailsModelData().getAirCraft();
					 out.key("aircraft").value(aircraft);
				 }
				
				 List<FlySegmentData> flySegments = serviceResponseDetail.getFlightDetailsModelData().getFlySegments();
				 if (flySegments != null && !flySegments.isEmpty()) {
					 
					 if (flySegments.get(0).getMilesFromStart() != null) {
						 miles = flySegments.get(0).getMilesFromStart().toString();
						 out.key("miles").value(miles);
					 }
					 if (flySegments.get(0).getTimeFromStart() != null) {
						 duration = flySegments.get(0).getTimeFromStart().toString();
						 duration = (duration.length() > 2 ? duration.substring(0, duration.length()-2) : "0") + "h" + duration.substring(duration.length()-2, 
									duration.length()) + "&apos;";
						 out.key("duration").value(duration);
					 }
					 if (flySegments.get(0).getDaysFromStart() != null 
							 && !("").equals(flySegments.get(0).getDaysFromStart())) {
						 plusDays = flySegments.get(0).getDaysFromStart().toString();
						 out.key("plusDays").value(plusDays);
					 }
				 }
				 
			} else {
				out.key("redirect").value(failurePage);
			}
			out.endObject();
		} catch (Exception e) {
			logger.error("unexpected error: ", e);
			String failurePage = (String) componentContext.getProperties().get("failure.page");
			try {
				out.key("redirect").value(failurePage);
				out.endObject();
			} catch (JSONException e1) {
				logger.error("Error creating JSON: ", e);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
	}

}
