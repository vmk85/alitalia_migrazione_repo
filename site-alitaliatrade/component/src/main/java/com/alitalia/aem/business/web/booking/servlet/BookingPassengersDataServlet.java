package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingSession;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.booking.model.ContactType;
import com.alitalia.aem.business.web.booking.model.Passenger;
import com.alitalia.aem.business.web.booking.model.PassengerType;
import com.alitalia.aem.business.web.booking.model.PassengersData;
import com.alitalia.aem.business.web.booking.model.Sex;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.business.web.utils.StringUtils;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.utils.IDFactory;
import com.day.cq.commons.TidyJSONWriter;
import static com.alitalia.aem.business.web.utils.StringUtils.trimIfNotNull;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingpassengersdata" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.") })
@SuppressWarnings("serial")
public class BookingPassengersDataServlet extends GenericFormValidatorServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private ComponentContext componentContext;
	
	@Reference
	private BookingSession bookingSession;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		try{

			BookingSessionContext ctx = (BookingSessionContext) request.getSession(true).getAttribute(
					Constants.BOOKING_CONTEXT_ATTRIBUTE);
			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				throw new IllegalStateException("Booking session context not found.");
			}
		
			BookingPassengerDataForm form = new BookingPassengerDataForm(ctx, request);
		
			int c;
		
			Validator validator = new Validator();
		
			// INFORMAZIONI CONTATTO
		
			validator.addDirectParameterMessagePattern("tipoRecapito1", form.tipoRecapito1,
					Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Tipo recapito", "isNotEmpty");
		
			validator.addDirectParameterMessagePattern("prefissoRecapito1", form.prefissoRecapito1,
					Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Prefisso internazionale", "isNotEmpty");
		
			validator.addDirectParameterMessagePattern("valoreRecapito1", form.valoreRecapito1,
					Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Numero di telefono", "isNotEmpty");
			validator.addDirectParameterMessagePattern("valoreRecapito1", form.valoreRecapito1,
					Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Numero di telefono", "isNumeroTelefonoValido");
		
			validator.addDirectParameterMessagePattern("InputEmailRicevuta", form.email,
					Constants.MESSAGE_GENERIC_EMPTY_FIELD, "email", "isNotEmpty");
			validator.addDirectParameterMessagePattern("InputEmailRicevuta", form.email,
					Constants.MESSAGE_GENERIC_INVALID_FIELD, "email", "isEmail");
		
			if (!form.valoreRecapito2.isEmpty()) {
				validator.addDirectParameterMessagePattern("tipoRecapito2", form.tipoRecapito2,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Tipo recapito", "isNotEmpty");
			
				validator.addDirectParameterMessagePattern("prefissoRecapito2", form.prefissoRecapito2,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Prefisso internazionale", "isNotEmpty");
			
				validator.addDirectParameterMessagePattern("valoreRecapito2", form.valoreRecapito2,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Numero di telefono", "isNumeroTelefonoValido");
			}
		
			validator.addDirectParameterMessagePattern("agreementContainer", form.checkAgreement, "", "", "isNotEmpty");
		
			//check for passengers with same name and surname
			String concatPassengerNames = "";
			for (int i = 0; i<form.numAdulti; i++) {
				String names = form.nomeAdulti[i] + form.cognomeAdulti[i];
				names = names.replace(" ", "");
				concatPassengerNames += names + "|";
			}
			for (int i = 0; i<form.numBambini; i++) {
				String names = form.nomeBambini[i] + form.cognomeBambini[i];
				names = names.replace(" ", "");
				concatPassengerNames += names + "|";
			}
			for (int i = 0; i<form.numNeonati; i++) {
				String names = form.nomeNeonati[i] + form.cognomeNeonati[i];
				names = names.replace(" ", "");
				concatPassengerNames += names + "|";
			}
			concatPassengerNames = concatPassengerNames.substring(0, concatPassengerNames.length()-1);
		
			// ADULTI
		
	//		ArrayList<String> adultiResponsabili = new ArrayList<String> ();
		
			c = 1;
			for (int i = 0; i < form.numAdulti; i++) {
			
	//			adultiResponsabili.add(String.valueOf(i+1));
			
				validator.addDirectParameterMessagePattern("InputPrimoNome_Adulto_" + (i + c), form.nomeAdulti[i],
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Nome", "isNotEmpty");
				validator.addDirectParameterMessagePattern("InputPrimoNome_Adulto_" + (i + c), form.nomeAdulti[i],
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Nome", "isNomeValido");
			
				validator.addDirectParameterMessagePattern("InputCognome_Adulto_" + (i + c), form.cognomeAdulti[i],
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Cognome", "isNotEmpty");
				validator.addDirectParameterMessagePattern("InputCognome_Adulto_" + (i + c), form.cognomeAdulti[i],
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Cognome", "isNomeValido");
			
				validator.addCrossParameter("InputPrimoNome_Adulto_" + (i + c), 
						form.nomeAdulti[i]+form.cognomeAdulti[i], concatPassengerNames, 
						Constants.MESSAGE_EQUALS_NAMES, "isNotEqualsPassengerNames");
			
				if (form.secondoNomeAdulti.length > 0 && form.secondoNomeAdulti[i] != null
						&& !form.secondoNomeAdulti[i].isEmpty()) {
					validator.addDirectParameterMessagePattern("InputSecondoNomeAdulto_Adulto_" + (i + c),
							form.secondoNomeAdulti[i], Constants.MESSAGE_GENERIC_INVALID_FIELD, "Secondo nome",
							"isNomeValido");
				}
			
				if (!form.codiceBluebizAdulti.isEmpty()) {
					validator.addDirectParameterMessagePattern("InputCodiceFedeltaBBiz_Adulto_1", form.codiceBluebizAdulti,
							Constants.MESSAGE_GENERIC_INVALID_FIELD, "Codice Bluebiz", "isCodiceBluebizValido");
				}
			
				if (ctx.isSecureFlight) {
				
					validator.addDirectParameterMessagePattern("sessoAdulto_Adulto_" + (i + c), form.genereAdulti[i],
							Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Genere", "isNotEmpty");
				
				}
			
				if (ctx.isSecureFlight || ctx.isApis) {
				
					String dataNascita = form.giornoNascitaAdulti[i] + "/" + form.meseNascitaAdulti[i] + "/"
							+ form.annoNascitaAdulti[i];
				
					if (ctx.isSecureFlight || dataNascita.length() == 10) {
						// SecureFlight: always check and validate adult birth dates (they are mandatory)
						// Apis no SecureFight: check for validity only if a complete date is provided
					
						validator.addDirectParameterMessagePattern("giornoNascitaAdulto_Adulto_" + (i + c), dataNascita,
								Constants.MESSAGE_GENERIC_INVALID_FIELD, "Data", "isDate", "isBeforeToday");
					
						validator.addDirectParameterMessagePattern("meseNascitaAdulto_Adulto_" + (i + c), dataNascita,
								Constants.MESSAGE_GENERIC_INVALID_FIELD, "Data", "isDate", "isBeforeToday");
					
						validator.addDirectParameterMessagePattern("annoNascitaAdulto_Adulto_" + (i + c), dataNascita,
								Constants.MESSAGE_GENERIC_INVALID_FIELD, "Data", "isDate", "isBeforeToday");
					
					}
				
				}
			}
		
			// BAMBINI
		
			c = form.numAdulti + 1;
			for (int i = 0; i < form.numBambini; i++) {
			
				validator.addDirectParameterMessagePattern("InputPrimoNomeBambino_Bambino_" + (i + c), form.nomeBambini[i],
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Nome", "isNotEmpty");
				validator.addDirectParameterMessagePattern("InputPrimoNomeBambino_Bambino_" + (i + c), form.nomeBambini[i],
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Nome", "isNomeValido");
			
				validator.addDirectParameterMessagePattern("InputCognomeBambino_Bambino_" + (i + c),
						form.cognomeBambini[i], Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Cognome", "isNotEmpty");
				validator.addDirectParameterMessagePattern("InputCognomeBambino_Bambino_" + (i + c),
						form.cognomeBambini[i], Constants.MESSAGE_GENERIC_INVALID_FIELD, "Cognome", "isNomeValido");
			
				validator.addCrossParameter("InputPrimoNomeBambino_Bambino_" + (i + c), 
						form.nomeBambini[i]+form.cognomeBambini[i], concatPassengerNames, 
						Constants.MESSAGE_EQUALS_NAMES, "isNotEqualsPassengerNames");
			
				if (form.secondoNomeBambini.length > 0 && form.secondoNomeBambini[i] != null
						&& !form.secondoNomeBambini[i].isEmpty()) {
					validator.addDirectParameterMessagePattern("InputSecondoNomeBambino_Bambino_" + (i + c),
							form.secondoNomeBambini[i], Constants.MESSAGE_GENERIC_INVALID_FIELD, "Secondo Nome",
							"isNomeValido");
				}
			
				String dataNascita = form.giornoNascitaBambini[i] + "/" + form.meseNascitaBambini[i] + "/"
						+ form.annoNascitaBambini[i];
			
				validator.addDirectParameterMessagePattern("giornoNascitaBambino_Bambino_" + (i + c), dataNascita,
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Data", "isDate", "isBeforeToday");
			
				validator.addDirectParameterMessagePattern("meseNascitaBambino_Bambino_" + (i + c), dataNascita,
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Data", "isDate", "isBeforeToday");
			
				validator.addDirectParameterMessagePattern("annoNascitaBambino_Bambino_" + (i + c), dataNascita,
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Data", "isDate", "isBeforeToday");
			}
		
			// NEONATI
		
			c = form.numAdulti + form.numBambini + 1;
		
			for (int i = 0; i < form.numNeonati; i++) {
			
				validator.addDirectParameterMessagePattern("InputPrimoNomeNeonato_Neonato_" + (i + c), form.nomeNeonati[i],
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Nome", "isNotEmpty");
				validator.addDirectParameterMessagePattern("InputPrimoNomeNeonato_Neonato_" + (i + c), form.nomeNeonati[i],
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Nome", "isNomeValido");
			
				validator.addDirectParameterMessagePattern("InputCognomeNeonato_Neonato_" + (i + c),
						form.cognomeNeonati[i], Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Cognome", "isNotEmpty");
				validator.addDirectParameterMessagePattern("InputCognomeNeonato_Neonato_" + (i + c),
						form.cognomeNeonati[i], Constants.MESSAGE_GENERIC_INVALID_FIELD, "Cognome", "isNomeValido");
			
				validator.addCrossParameter("InputPrimoNomeNeonato_Neonato_" + (i + c), 
						form.nomeNeonati[i]+form.cognomeNeonati[i], concatPassengerNames, 
						Constants.MESSAGE_EQUALS_NAMES, "isNotEqualsPassengerNames");
			
				if (form.secondoNomeNeonati.length > 0 && form.secondoNomeNeonati[i] != null
						&& !form.secondoNomeNeonati[i].isEmpty()) {
					validator.addDirectParameterMessagePattern("InputSecondoNeonato_Neonato_" + (i + c),
							form.secondoNomeNeonati[i], Constants.MESSAGE_GENERIC_INVALID_FIELD, "Secondo Nome",
							"isNomeValido");
				}
			
				String dataNascita = form.giornoNascitaNeonati[i] + "/" + form.meseNascitaNeonati[i] + "/"
						+ form.annoNascitaNeonati[i];
			
				validator.addDirectParameterMessagePattern("giornoNascitaNeonato_Neonato_" + (i + c), dataNascita,
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Data", "isDate", "isBeforeToday");
			
				validator.addDirectParameterMessagePattern("meseNascitaNeonato_Neonato_" + (i + c), dataNascita,
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Data", "isDate", "isBeforeToday");
			
				validator.addDirectParameterMessagePattern("annoNascitaNeonato_Neonato_" + (i + c), dataNascita,
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Data", "isDate", "isBeforeToday");
			
				/*validator.addDirectParameterMessagePattern("adultoResponsabile_Neonato_" + (i + c),
						form.adultoResponsabileNeonati[i], Constants.MESSAGE_GENERIC_INVALID_FIELD, "Adulto responsabile",
						"isNotEmpty");
			
				validator.setAllowedValuesMessagePattern("adultoResponsabile_Neonato_" + (i + c), form.adultoResponsabileNeonati[i], 
						adultiResponsabili.toArray(), Constants.MESSAGE_RESPONSIBLE_ADULTS_NOT_VALID, "Adulto responsabile");
				logger.error("TEST - Controllo l'adulto: "+form.adultoResponsabileNeonati[i]+" se è contenuto dentro: "+adultiResponsabili);
				adultiResponsabili.remove(form.adultoResponsabileNeonati[i]);
				logger.error("TEST - Ora adulti responsabili vale: "+adultiResponsabili);*/
			}
		
			ResultValidation resultValidation = validator.validate();
		
			if (resultValidation.getResult()) {
			
				// temporarly update passengers data in context after a successful validation (the related  
				// data will be required for frequent flyers validation and for seat map selection step)
				bookingSession.updatePassengersData(ctx, form.toPassengersData(ctx));
			
				// perform the frequent flyer codes validation using the service
				List<AdultPassengerData> adultPassengersWithWrongFrequentFlyer = 
						bookingSession.submitPassengerDetails(ctx);
				if (adultPassengersWithWrongFrequentFlyer != null) {
					for (AdultPassengerData adultPaxWithWrongFrequentFlyer : adultPassengersWithWrongFrequentFlyer) {
						resultValidation.setResult(false);
						// find the relevant passenger by frequent flyer data
						for (int i = 0; i < form.numAdulti; i++) {
							if (adultPaxWithWrongFrequentFlyer.getFrequentFlyerType().getCode().equals(form.programmaFrequentFlyerAdulti[i]) &&
									adultPaxWithWrongFrequentFlyer.getFrequentFlyerCode().equals(form.codiceTesseraAdulti[i])) {
								resultValidation.getFields().put("InputCodiceTessera_Adulto_" + (i + 1), 
										"Codice tessera non valido");
							}
						}
					}
				}
			
			}
		
			bookingSession.retrieveSeatMaps(ctx, IDFactory.getTid());
		
			return resultValidation;

		}catch (Exception e) {
			logger.error("Error during validation form passenger", e);
			throw e;
		}
	}
	
	@Override
	protected void writeAdditionalValidationResponseData(SlingHttpServletRequest request,
			SlingHttpServletResponse response, TidyJSONWriter json, ResultValidation resultValidation) throws Exception {
		super.writeAdditionalValidationResponseData(request, response, json, resultValidation);
		
		BookingSessionContext ctx = (BookingSessionContext) request.getSession(true).getAttribute(
				Constants.BOOKING_CONTEXT_ATTRIBUTE);
		if (ctx == null) {
			logger.error("Cannot find booking session context in session.");
			throw new IllegalStateException("Booking session context not found.");
		}
		
		json.key("openModal").value(ctx.isSeatMapSelectionAllowed);
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		
		try {
			
			BookingSessionContext ctx = (BookingSessionContext) request.getSession(true).getAttribute(
					Constants.BOOKING_CONTEXT_ATTRIBUTE);
			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				throw new IllegalStateException("Booking session context not found.");
			}
			
			BookingPassengerDataForm form = new BookingPassengerDataForm(ctx, request);
			bookingSession.confirmPassengersData(ctx, form.toPassengersData(ctx));
			
			String successPage = (String) componentContext.getProperties().get("success.page");
			if (successPage != null && !"".equals(successPage)) {
				response.sendRedirect(response.encodeRedirectURL(successPage));
			}
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			
			if (!response.isCommitted()) {
				String failurePage = (String) componentContext.getProperties().get("failure.page");
				if (failurePage != null && !"".equals(failurePage)) {
					response.sendRedirect(response.encodeRedirectURL(failurePage));
				} else {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
			}
		}
	}
	
}

class BookingPassengerDataForm {
	
	public int numAdulti;
	public int numBambini;
	public int numNeonati;
	
	public String email;
	public String tipoRecapito1;
	public String prefissoRecapito1;
	public String valoreRecapito1;
	public String tipoRecapito2;
	public String prefissoRecapito2;
	public String valoreRecapito2;
	public String checkAgreement;
	
	public String[] nomeAdulti;
	public String[] secondoNomeAdulti;
	public String[] cognomeAdulti;
	public String[] programmaFrequentFlyerAdulti;
	public String[] codiceTesseraAdulti;
	public String codiceBluebizAdulti;
	public String[] preferenzePastoAdulti;
	public String[] giornoNascitaAdulti;
	public String[] meseNascitaAdulti;
	public String[] annoNascitaAdulti;
	public String[] genereAdulti;
	public String[] numeroPassaportoAdulti;
	public String[] nazionalitaAdulti;
	public String[] postoAdulti;
	
	public String[] nomeBambini;
	public String[] secondoNomeBambini;
	public String[] cognomeBambini;
	public String[] preferenzePastoBambini;
	public String[] giornoNascitaBambini;
	public String[] meseNascitaBambini;
	public String[] annoNascitaBambini;
	public String[] genereBambini;
	public String[] numeroPassaportoBambini;
	public String[] nazionalitaBambini;
	public String[] postoBambini;
	
	public String[] nomeNeonati;
	public String[] secondoNomeNeonati;
	public String[] cognomeNeonati;
	public String[] preferenzePastoNeonati;
	public String[] giornoNascitaNeonati;
	public String[] meseNascitaNeonati;
	public String[] annoNascitaNeonati;
	public String[] genereNeonati;
	public String[] numeroPassaportoNeonati;
	public String[] nazionalitaNeonati;
	public String[] adultoResponsabileNeonati;
	public String[] postoNeonati;
	
	public BookingPassengerDataForm(BookingSessionContext ctx, SlingHttpServletRequest request) {
		
		int c;
		
		/* INFORMAZIONI CONTATTO */
		
		email = request.getParameter("InputEmailRicevuta");
		
		// Contatto 1
		tipoRecapito1 = request.getParameter("tipoRecapito1");
		prefissoRecapito1 = request.getParameter("prefissoRecapito1");
		valoreRecapito1 = request.getParameter("valoreRecapito1");
		
		// Contatto 2
		tipoRecapito2 = request.getParameter("tipoRecapito2");
		prefissoRecapito2 = request.getParameter("prefissoRecapito2");
		valoreRecapito2 = request.getParameter("valoreRecapito2");
		
		checkAgreement = request.getParameter("checkAgreement");
		
		/* ADULTI */
		
		numAdulti = ctx.searchPassengersNumber.getNumAdults() + ctx.searchPassengersNumber.getNumYoung();
		c = 1;
		nomeAdulti = new String[numAdulti];
		secondoNomeAdulti = new String[numAdulti];
		cognomeAdulti = new String[numAdulti];
		programmaFrequentFlyerAdulti = new String[numAdulti];
		codiceTesseraAdulti = new String[numAdulti];
		codiceBluebizAdulti = new String();
		preferenzePastoAdulti = new String[numAdulti];
		giornoNascitaAdulti = new String[numAdulti];
		meseNascitaAdulti = new String[numAdulti];
		annoNascitaAdulti = new String[numAdulti];
		genereAdulti = new String[numAdulti];
		numeroPassaportoAdulti = new String[numAdulti];
		nazionalitaAdulti = new String[numAdulti];
		postoAdulti = new String[numAdulti];
		
		for (int i = 0; i < numAdulti; i++) {
			nomeAdulti[i] = request.getParameter("InputPrimoNome_Adulto_" + (i + c));
			cognomeAdulti[i] = request.getParameter("InputCognome_Adulto_" + (i + c));
			programmaFrequentFlyerAdulti[i] = request.getParameter("programmaFedeltaFreqFlyer_Adulto_" + (i + c));
			codiceTesseraAdulti[i] = StringUtils.stripStart(request.getParameter("InputCodiceTessera_Adulto_" + (i + c)), "0");
			preferenzePastoAdulti[i] = request.getParameter("preferenzePasto_Adulto_" + (i + c));
			postoAdulti[i] = request.getParameter("postoPassegero_Adulto_" + (i + c));
			if (i == 0) {
				codiceBluebizAdulti = request.getParameter("InputCodiceFedeltaBBiz_Adulto_" + (i + c));
			}
			if (ctx.isSecureFlight) {
				secondoNomeAdulti[i] = request.getParameter("InputSecondoNomeAdulto_Adulto_" + (i + c));
			}
			if (ctx.isApis) {
				numeroPassaportoAdulti[i] = request.getParameter("InputNumPassaporto_Adulto_" + (i + c));
				nazionalitaAdulti[i] = request.getParameter("Nazionalita_Adulto_" + (i + c));
			}
			if (ctx.isSecureFlight || ctx.isApis) {
				giornoNascitaAdulti[i] = request.getParameter("giornoNascitaAdulto_Adulto_" + (i + c));
				meseNascitaAdulti[i] = request.getParameter("meseNascitaAdulto_Adulto_" + (i + c));
				annoNascitaAdulti[i] = request.getParameter("annoNascitaAdulto_Adulto_" + (i + c));
				genereAdulti[i] = request.getParameter("sessoAdulto_Adulto_" + (i + c));
			}
		}
		
		/* BAMBINI */
		
		numBambini = ctx.searchPassengersNumber.getNumChildren();
		c = numAdulti + 1;
		nomeBambini = new String[numBambini];
		secondoNomeBambini = new String[numBambini];
		cognomeBambini = new String[numBambini];
		preferenzePastoBambini = new String[numBambini];
		giornoNascitaBambini = new String[numBambini];
		meseNascitaBambini = new String[numBambini];
		annoNascitaBambini = new String[numBambini];
		genereBambini = new String[numBambini];
		numeroPassaportoBambini = new String[numBambini];
		nazionalitaBambini = new String[numBambini];
		postoBambini = new String[numBambini];
		
		for (int i = 0; i < numBambini; i++) {
			nomeBambini[i] = request.getParameter("InputPrimoNomeBambino_Bambino_" + (i + c));
			cognomeBambini[i] = request.getParameter("InputCognomeBambino_Bambino_" + (i + c));
			giornoNascitaBambini[i] = request.getParameter("giornoNascitaBambino_Bambino_" + (i + c));
			meseNascitaBambini[i] = request.getParameter("meseNascitaBambino_Bambino_" + (i + c));
			annoNascitaBambini[i] = request.getParameter("annoNascitaBambino_Bambino_" + (i + c));
			genereBambini[i] = request.getParameter("sessoBambino_Bambino_" + (i + c));
			preferenzePastoBambini[i] = request.getParameter("preferenzePasto_Bambino_" + (i + c));
			postoBambini[i] = request.getParameter("postoPassegero_Bambino_" + (i + c));
			if (ctx.isSecureFlight) {
				secondoNomeBambini[i] = request.getParameter("InputSecondoNomeBambino_Bambino_" + (i + c));
			}
			if (ctx.isApis) {
				numeroPassaportoBambini[i] = request.getParameter("InputNumPassaporto_Bambino_" + (i + c));
				nazionalitaBambini[i] = request.getParameter("Nazionalita_Bambino_" + (i + c));
			}
		}
		
		/* NEONATI */
		
		numNeonati = ctx.searchPassengersNumber.getNumInfants();
		c = numAdulti + numBambini + 1;
		nomeNeonati = new String[numNeonati];
		secondoNomeNeonati = new String[numNeonati];
		cognomeNeonati = new String[numNeonati];
		preferenzePastoNeonati = new String[numNeonati];
		giornoNascitaNeonati = new String[numNeonati];
		meseNascitaNeonati = new String[numNeonati];
		annoNascitaNeonati = new String[numNeonati];
		genereNeonati = new String[numNeonati];
		numeroPassaportoNeonati = new String[numNeonati];
		nazionalitaNeonati = new String[numNeonati];
		adultoResponsabileNeonati = new String[numNeonati];
		postoNeonati = new String[numNeonati];
		
		for (int i = 0; i < numNeonati; i++) {
			nomeNeonati[i] = request.getParameter("InputPrimoNomeNeonato_Neonato_" + (i + c));
			cognomeNeonati[i] = request.getParameter("InputCognomeNeonato_Neonato_" + (i + c));
			giornoNascitaNeonati[i] = request.getParameter("giornoNascitaNeonato_Neonato_" + (i + c));
			meseNascitaNeonati[i] = request.getParameter("meseNascitaNeonato_Neonato_" + (i + c));
			annoNascitaNeonati[i] = request.getParameter("annoNascitaNeonato_Neonato_" + (i + c));
			genereNeonati[i] = request.getParameter("sessoNeonato_Neonato_" + (i + c));
//			adultoResponsabileNeonati[i] = request.getParameter("adultoResponsabile_Neonato_" + (i + c));
			adultoResponsabileNeonati[i] =  String.valueOf(i+1);
			preferenzePastoNeonati[i] = request.getParameter("preferenzePasto_Neonato_" + (i + c));
			postoNeonati[i] = request.getParameter("postoPassegero_Neonato_" + (i + c));
			if (ctx.isSecureFlight) {
				secondoNomeNeonati[i] = request.getParameter("InputSecondoNomeNeonato_Neonato_" + (i + c));
			}
			if (ctx.isApis) {
				numeroPassaportoNeonati[i] = request.getParameter("InputNumPassaporto_Neonato_" + (i + c));
				nazionalitaNeonati[i] = request.getParameter("Nazionalita_Neonato_" + (i + c));
			}
		}
		
	}
	
	public PassengersData toPassengersData(BookingSessionContext ctx) {
		
		PassengersData passengersData = new PassengersData();
		Passenger pax;
		
		// INFORMAZIONI DI CONTATTO
		
		if (this.valoreRecapito1 != null && !"".equals(this.valoreRecapito1)) {
			passengersData.setContact1(ContactType.valueOf(this.tipoRecapito1), this.prefissoRecapito1,
					trimIfNotNull(this.valoreRecapito1));
		}
		if (this.valoreRecapito2 != null && !"".equals(this.valoreRecapito2)) {
			passengersData.setContact2(ContactType.valueOf(this.tipoRecapito2), this.prefissoRecapito2,
					trimIfNotNull(this.valoreRecapito2));
		}
		passengersData.setEmail(trimIfNotNull(this.email));
		
		// ADULTI
		
		for (int i = 0; i < this.numAdulti; i++) {
			pax = new Passenger();
			pax.setPassengerType(PassengerType.ADULT);
			pax.setName(trimIfNotNull(this.nomeAdulti[i]));
			pax.setSurname(trimIfNotNull(this.cognomeAdulti[i]));
			pax.setFrequentFlyerProgram(this.programmaFrequentFlyerAdulti[i]);
			pax.setFrequentFlyerCardNumber(trimIfNotNull(this.codiceTesseraAdulti[i]));
			pax.setMealPreference(this.preferenzePastoAdulti[i]);
			
			if (i == 0) {
				pax.setBlueBizCode(trimIfNotNull(this.codiceBluebizAdulti));
			}
			
			if (ctx.isSecureFlight) {
				pax.setSecondName(trimIfNotNull(this.secondoNomeAdulti[i]));
			}
			
			if (ctx.isApis) {
				pax.setPassportNumber(trimIfNotNull(this.numeroPassaportoAdulti[i]));
				pax.setNationality(this.nazionalitaAdulti[i]);
			}
			
			if (ctx.isSecureFlight || ctx.isApis) {
				if (this.giornoNascitaAdulti[i] != null && !"".equals(this.giornoNascitaAdulti[i]) &&
						this.meseNascitaAdulti[i] != null && !"".equals(this.meseNascitaAdulti[i]) &&
						this.annoNascitaAdulti[i] != null && !"".equals(this.annoNascitaAdulti[i])) {
					pax.setBirthDate(
							toCalendar(this.giornoNascitaAdulti[i], this.meseNascitaAdulti[i], this.annoNascitaAdulti[i]));
				}
				if (this.genereAdulti[i] != null && !"".equals(this.genereAdulti[i])) {
					pax.setSex(Sex.valueOf(this.genereAdulti[i]));
				}
			}
			
			HashMap<Integer, String> mappaPostoPasseggero = new HashMap<Integer, String>();
			pax.setSeat(mappaPostoPasseggero);
			if (!this.postoAdulti[i].isEmpty()) {
				String[] voliPostiPasseggero = this.postoAdulti[i].split(";");
				for (int j = 0; j < voliPostiPasseggero.length; j++) {
					String[] voloPostoPasseggero = voliPostiPasseggero[j].split(":");
					mappaPostoPasseggero.put(Integer.valueOf(voloPostoPasseggero[0]), voloPostoPasseggero[1]);
				}
			}
			
			passengersData.addPassenger(pax);
		}
		
		// BAMBINI
		
		for (int i = 0; i < this.numBambini; i++) {
			pax = new Passenger();
			pax.setPassengerType(PassengerType.CHILD);
			
			pax.setName(trimIfNotNull(this.nomeBambini[i]));
			pax.setSurname(trimIfNotNull(this.cognomeBambini[i]));
			pax.setBirthDate(
					toCalendar(this.giornoNascitaBambini[i], this.meseNascitaBambini[i], this.annoNascitaBambini[i]));
			
			if (this.genereBambini[i] != null && !"".equals(this.genereBambini[i])) {
				pax.setSex(Sex.valueOf(this.genereBambini[i]));
			}
			
			pax.setMealPreference(this.preferenzePastoBambini[i]);
			
			if (ctx.isSecureFlight) {
				pax.setSecondName(trimIfNotNull(this.secondoNomeBambini[i]));
			}
			
			if (ctx.isApis) {
				pax.setPassportNumber(trimIfNotNull(this.numeroPassaportoBambini[i]));
				pax.setNationality(this.nazionalitaBambini[i]);
			}
			
			HashMap<Integer, String> mappaPostoPasseggero = new HashMap<Integer, String>();
			pax.setSeat(mappaPostoPasseggero);
			if (!this.postoBambini[i].isEmpty()) {
				String[] voliPostiPasseggero = this.postoBambini[i].split(";");
				for (int j = 0; j < voliPostiPasseggero.length; j++) {
					String[] voloPostoPasseggero = voliPostiPasseggero[j].split(":");
					mappaPostoPasseggero.put(Integer.valueOf(voloPostoPasseggero[0]), voloPostoPasseggero[1]);
				}
			}
			
			passengersData.addPassenger(pax);
		}
		
		// NEONATI
		
		for (int i = 0; i < this.numNeonati; i++) {
			pax = new Passenger();
			pax.setPassengerType(PassengerType.INFANT);
			
			pax.setName(trimIfNotNull(this.nomeNeonati[i]));
			pax.setSurname(trimIfNotNull(this.cognomeNeonati[i]));
			pax.setBirthDate(
					toCalendar(this.giornoNascitaNeonati[i], this.meseNascitaNeonati[i], this.annoNascitaNeonati[i]));
			
			if (this.genereNeonati[i] != null && !"".equals(this.genereNeonati[i])) {
				pax.setSex(Sex.valueOf(this.genereNeonati[i]));
			}
			
			pax.setMealPreference(this.preferenzePastoNeonati[i]);
			
			if (ctx.isSecureFlight) {
				pax.setSecondName(trimIfNotNull(this.secondoNomeNeonati[i]));
			}
			
			if (ctx.isApis) {
				pax.setPassportNumber(trimIfNotNull(this.numeroPassaportoNeonati[i]));
				pax.setNationality(this.nazionalitaNeonati[i]);
			}
			
			String adultoResponsabile = this.adultoResponsabileNeonati[i];
			int numAdultoResponsabile = Integer.parseInt(adultoResponsabile.substring(
					adultoResponsabile.length() - 1, adultoResponsabile.length()));
			pax.setReferenceAdult(numAdultoResponsabile-1);
			
			HashMap<Integer, String> mappaPostoPasseggero = new HashMap<Integer, String>();
			pax.setSeat(mappaPostoPasseggero);
			if (!this.postoNeonati[i].isEmpty()) {
				String[] voliPostiPasseggero = this.postoNeonati[i].split(";");
				for (int j = 0; j < voliPostiPasseggero.length; j++) {
					String[] voloPostoPasseggero = voliPostiPasseggero[j].split(":");
					mappaPostoPasseggero.put(Integer.valueOf(voloPostoPasseggero[0]), voloPostoPasseggero[1]);
				}
			}
			
			passengersData.addPassenger(pax);
			
		}
		
		return passengersData;
	}
	
	private Calendar toCalendar(String day, String month, String year) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		df.setLenient(false);
		try {
			Calendar date = Calendar.getInstance();
			date.setTime(df.parse(day + "/" + month + "/" + year));
			return date;
		} catch (ParseException e) {
			// (this method is meant to be used only on safe, already-validated input)
			throw new RuntimeException(e);
		}
	}
	
}
