package com.alitalia.aem.business.web.controllers;

import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUse;

public class AllNewsLink extends WCMUse {

	private String urllink;
	
	@Override
    public void activate() throws Exception {
    	
		ValueMap properties = getProperties();
		
		urllink = properties.get("urllink", String.class) != null ? properties.get("urllink", String.class) : "#";
	}
	
	public String getUrllink() {
        return urllink;
    }

}
