package com.alitalia.aem.business.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.AlitaliaTradeAuthUtils;

public class Anagrafica extends WCMUse {

	private String agenzia;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
    public void activate() throws Exception {
		try{
			agenzia = AlitaliaTradeAuthUtils.getProperty(getRequest(), "ragioneSociale");
		} catch (Exception e) {
			logger.error("Retrieve Provinces error", e);
			throw e;
		}
	}
	
	public String getAgenzia() {
		return (agenzia).toUpperCase();
	}
	
}