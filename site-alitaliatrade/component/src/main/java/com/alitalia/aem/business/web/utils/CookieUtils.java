package com.alitalia.aem.business.web.utils;

import java.net.*;
import java.text.*;
import java.util.*;

import javax.servlet.http.*;

import org.slf4j.*;


/**
 * Cookies management utility methods.
 */
public class CookieUtils {
	
	private static Logger logger = LoggerFactory.getLogger(CookieUtils.class); 
	
	/**
	 * Utility method to read a cookie value.
	 *
	 * @param request The request to work with.
	 * @param cookieName The name of the cookie.
	 * @return The cookie value, or null if not found.
	 */
	public static String getCookieValue(HttpServletRequest request, 
			String cookieName) {
		
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookieName.equals(cookie.getName())) {
					return cookie.getValue();
				}
			}
		}
		return null;

	}
	
	/**
	 * Utility method to clear a cookie.
	 *
	 * @param request The request to work with.
	 * @param response The response to work with.
	 * @param cookieName The name of the cookie.
	 */
	public static void clearCookie(HttpServletRequest request,
			HttpServletResponse response, String cookieName) {
		
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookieName.equals(cookie.getName())) {
					cookie.setMaxAge(0);
					cookie.setValue(null);
	                response.addCookie(cookie);
	                return;
				}
			}
		}

	}
	
	/**
	 * Utility method to set a persistent cookie value.
	 *
	 * @param response The response to work with.
	 * @param cookieName The name of the cookie.
	 * @param domain The domain of the cookie to set.
	 * @param path The path of the cookie to set.
	 * @param maxAge The max age of the cookie.
	 * @param value The value to set.
	 * @param secure If true, set the cookie as secure (HTTPS-only).
	 * @param setExpires If true, the maxAge will be also set with expires (IE compatibility).
	 */
	public static void setCookieValue(HttpServletResponse response,
			String cookieName, String domain, String path, Integer maxAge,
			String value, boolean secure, boolean setExpires) {
		
		if (maxAge != null && setExpires) {
			// use alternative, manual way
			setCookieValueWithExpires(response, cookieName, domain, path,
					maxAge.intValue(), value, secure);
		} else {
			
			Cookie cookie = new Cookie(cookieName, value);
			if (domain != null && !"".equals(domain)) {
				cookie.setDomain(domain);
			}
			if (path != null) {
				cookie.setPath(path);
			}
			if (maxAge != null) {
				cookie.setMaxAge(maxAge);
			}
			cookie.setSecure(secure);
			response.addCookie(cookie);
			
		}
	}
	
	/**
	 * Manually set a persistent cookie setting the header response, also specifying the expires. 
	 * 
	 * @param response The response to work with.
	 * @param cookieName The name of the cookie.
	 * @param domain The domain of the cookie to set.
	 * @param path The path of the cookie to set.
	 * @param maxAge The max age of the cookie.
	 * @param value The value to set.
	 * @param secure If true, set the cookie as secure (HTTPS-only).
	 */
	private static void setCookieValueWithExpires(
			HttpServletResponse response, String cookieName, String domain,
			String path, int maxAge, String value, boolean secure) {
		
	    StringBuilder cookie =
	    		new StringBuilder(cookieName + "=" + value + "; ");
	    DateFormat df =
	    		new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss 'GMT'", Locale.US);
	    
	    Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.SECOND, maxAge);
	    cookie.append("Expires=" + df.format(cal.getTime()) + "; ");
	    cookie.append("Domain=; ");
	    cookie.append("Version=0; ");
	    cookie.append("Path=/; ");
	    cookie.append("Max-Age=" + maxAge + "; ");
	    if (secure) {
	    	cookie.append("Secure; ");
	    }
	    
	    response.setHeader("Set-Cookie", cookie.toString());
	}
	
	
	public static HashMap<String,String> fromCookieString(String cookieString){
		
		HashMap<String,String> result = new HashMap<>();
		
		if (cookieString == null || cookieString.length() == 0) {
			return null;
		}
		try {
			String[] cookieStringTokens = cookieString.split("#");
			String countryCode = URLDecoder.decode(cookieStringTokens[0], "UTF-8");
			String languageCode = URLDecoder.decode(cookieStringTokens[1], "UTF-8");
			
			result.put("language", languageCode);
			result.put("market",countryCode);
			
			return result;
			
		} catch (Exception e) {
			logger.error("Error decoding geolocalization data, ignoring.", e);
			return null;
		}
	}
	
	public static String toCookieString(String countryCode, String languageCode) {
		String cookieString = null;
		try {
			String[] cookieStringTokens = new String[2];
			cookieStringTokens[0] = URLEncoder.encode(countryCode, "UTF-8");
			cookieStringTokens[1] = URLEncoder.encode(languageCode, "UTF-8");
			cookieString = StringUtils.joinArray(cookieStringTokens, "#");
		} catch (Exception e) {
			logger.error("Error encoding geolocalization data.", e);
			return null;
		}
		return cookieString;
	}
}