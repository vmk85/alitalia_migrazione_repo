package com.alitalia.aem.business.web.booking.render;

import java.util.Map;

public class RegoleTariffarieTrattaRender {
	Map<String,String> fareRulesFirstColumn;
	Map<String,String> fareRulesSecondColumn;
	String titolo;
	
	public RegoleTariffarieTrattaRender(Map<String, String> fareRulesFirstColumn,
			Map<String, String> fareRulesSecondColumn, String titolo) {
		this.fareRulesFirstColumn = fareRulesFirstColumn;
		this.fareRulesSecondColumn = fareRulesSecondColumn;
		this.titolo = titolo;
	}
	
	public Map<String, String> getFareRulesFirstColumn() {
		return fareRulesFirstColumn;
	}
	
	public Map<String, String> getFareRulesSecondColumn() {
		return fareRulesSecondColumn;
	}
	
	public String getTitolo() {
		return titolo;
	}
	
}
