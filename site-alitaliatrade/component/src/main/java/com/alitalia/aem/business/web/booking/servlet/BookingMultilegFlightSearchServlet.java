package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingSearchCUGEnum;
import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.BookingSession;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.booking.model.SearchElement;
import com.alitalia.aem.business.web.booking.model.SearchElementAirport;
import com.alitalia.aem.business.web.booking.model.SearchPassengersNumber;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.alitalia.aem.business.web.utils.TypeUtils;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.day.cq.i18n.I18n;

@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingmultilegflightsearch" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "error.page", description = "Users will be redirected to the specified page after a failed outcome.")
})
@SuppressWarnings("serial")
public class BookingMultilegFlightSearchServlet extends GenericFormValidatorServlet {
	
private Logger logger = LoggerFactory.getLogger(getClass());
	
	private ComponentContext componentContext;
	
	@Reference
	private BookingSession bookingSession;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		try {
			/* -- RECUPERO DATI DALLA FORM -- */
			Validator validator = new Validator();
			
			String[] startMulti;

			// MULTITRATTA
			int nTratte = Integer.parseInt(request.getParameter("nTratteHidden"));

			startMulti = new String[nTratte];

			for (int i = 0; i < nTratte; i++) {
				
				String from = request.getParameter("from[" + (i + 1) + "]");
				String to = request.getParameter("to[" + (i + 1) + "]");
				startMulti[i] = request.getParameter("start["+ (i + 1) + "]");
				
				// departure airport
				validator.addDirectParameter("from[" + (i + 1) + "]", from, Constants.MESSAGE_AIRPORT_FROM_NOT_VALID, "isAirport");
				
				// arrival airport
				validator.addDirectParameter("to[" + (i + 1) + "]", to, Constants.MESSAGE_AIRPORT_FROM_NOT_VALID, "isAirport");
				
				// start date
				validator.addDirectParameter("start[" + (i + 1) + "]", startMulti[i], 
						Constants.MESSAGE_DATE_PAST, "isDate", "isFuture");
				
				if (i > 0) {
					validator.addCrossParameterMessagePattern("start[" + (i + 1) + "]", startMulti[i-1], startMulti[i], 
							Constants.MESSAGE_DATE_AFTER_NOT_VALID, Integer.toString(i), "beforeThen");
				}
				
				// check if departure airport and arrival airport are different
				validator.addCrossParameter("to[" + (i + 1) + "]",	from, to, Constants.MESSAGE_FROM_TO_DIFF, "areDifferent");
				
			}
			
			// number of adults
			validator.addDirectParameter("nadults-multi",
					request.getParameter("nadults-multi"),
					Constants.MESSAGE_NADULTS_NOT_VALID, "isRangeAdults");
	
			// number of kids
			String nkids = request.getParameter("nkids-multi");
			if (nkids != null && !nkids.isEmpty()) {
				validator.addDirectParameter("nkids-multi", nkids,
						Constants.MESSAGE_NKINDS_NOT_VALID, "isRangeKids");
			}
	
			// number of babies
			String nbabies = request.getParameter("nbabies-multi");
			if (nbabies != null && !nbabies.isEmpty()) {
				validator.addDirectParameter("nbabies-multi", nbabies,
						Constants.MESSAGE_NBABIES_NOT_VALID, "isRangeBabies");
			}
			
			// check sum of adults and kids
			validator.addCrossParameter("nadults-multi", request.getParameter("nadults-multi"),
					request.getParameter("nkids-multi"),
					Constants.MESSAGE_SUM_ADULTS_KIDS_NOT_VALID,
					"underMaxSumAdultsKids");
			
			// check if number of babies is lower then number of adults
			if (nbabies != null && !nbabies.isEmpty()) {
				validator.addCrossParameter("nbabies-multi",
						request.getParameter("nadults-multi"),
						request.getParameter("nbabies-multi"),
						Constants.MESSAGE_BABY_PER_ADULT_NOT_VALID, "babyPerAdult");
			}
			
			return validator.validate();
			
		} catch (Exception e) {
			logger.error("BookingMultilegFlightSearchServlet error: ", e);
			return null;
		}
	}
	

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		
		try {
			
			
			
			int numberOfSlices = Integer.parseInt(request.getParameter("nTratteHidden"));
			
			String[] fromMulti;
			String[] toMulti;
			String[] startMulti;
			fromMulti = new String[numberOfSlices];
			toMulti = new String[numberOfSlices];
			startMulti = new String[numberOfSlices];
			
			for (int i = 0; i < numberOfSlices; i++) { 
				setSlices(fromMulti,toMulti,startMulti, i, request); 
			}
			
			int nadults = Integer.parseInt(request.getParameter("nadults-multi")); 
			int nchildren = Integer.parseInt(request.getParameter("nkids-multi"));
			int ninfants = Integer.parseInt(request.getParameter("nbabies-multi"));
			
			//prepare search parameters
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
			dateFormat.setLenient(false);
			
			BookingSearchKindEnum searchKind = BookingSearchKindEnum.MULTILEG;
			List<SearchElement> searchElements = new ArrayList<SearchElement>();
			SearchPassengersNumber searchPassengersNumber = new SearchPassengersNumber();
			BookingSearchCUGEnum cug = BookingSearchCUGEnum.ADT;
			String market = "LU";
			String site = "LU";
			Locale locale = LocalizationUtils.getLocaleByResource(request.getResource(), "it");
			
			//set search elements
			for (int i=0; i<numberOfSlices; i++) {
				
				SearchElementAirport fromElement = new SearchElementAirport(fromMulti[i]);
				SearchElementAirport toElement = new SearchElementAirport(toMulti[i]);
				
				searchElements.add(new SearchElement(
						fromElement, toElement, TypeUtils.createCalendar(dateFormat.parse(startMulti[i]))));
				
			}
			
			//set the passengers
			searchPassengersNumber.setNumAdults(nadults);
			searchPassengersNumber.setNumChildren(nchildren);
			searchPassengersNumber.setNumInfants(ninfants);
			
			// create a new booking session context and save it in the session
			BookingSessionContext ctx = bookingSession.initializeBookingSession("alitaliaTradeBooking");
			request.getSession(true).setAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE, ctx);
			
			ResourceBundle resourceBundle = request.getResourceBundle(locale);
			final I18n i18n = new I18n(resourceBundle);

			String codiceAgenzia = "";
			String codiceAccordo = "";
			String partitaIva = "";
			ResourceResolver resourceResolver = request.getResourceResolver();
			Session session = resourceResolver.adaptTo(Session.class);
			UserManager userManager = resourceResolver.adaptTo(UserManager.class);
			String userID = session.getUserID();
		    Authorizable auth = userManager.getAuthorizable(userID);
		    
		    if (auth.getProperty("codiceAgenzia") != null 
		    		&& auth.getProperty("codiceAgenzia").length > 0) {
		    	codiceAgenzia = auth.getProperty("codiceAgenzia")[0].getString();
		    }
		    
		    if (auth.getProperty("codiceAccordo") != null 
		    		&& auth.getProperty("codiceAccordo").length > 0) {
		    	codiceAccordo = auth.getProperty("codiceAccordo")[0].getString();
		    }
		   
		    if (auth.getProperty("partitaIva") != null 
		    		&& auth.getProperty("partitaIva").length > 0) {
		    	partitaIva = auth.getProperty("partitaIva")[0].getString();
		    }
		   
			// prepare the search data in the booking session
			bookingSession.prepareSearch(ctx, i18n, searchKind, searchElements,
					searchPassengersNumber, cug, market, site, locale, codiceAgenzia, partitaIva, codiceAccordo, request);
			
			
			String successPage = (String) 
					componentContext.getProperties().get("success.page");
			if (successPage != null && !("").equals(successPage)) {
				response.sendRedirect(response.encodeRedirectURL(successPage));
			}
			
		} catch(Exception e) {
			logger.error("Unexpected error. ", e);
			
			if (!response.isCommitted()) {
				String failurePage = (String) componentContext.getProperties().get("failure.page");
				if (failurePage != null && !"".equals(failurePage)) {
					response.sendRedirect(response.encodeRedirectURL(failurePage));
				} else {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
			}
		}
		
	}
	
	private void setSlices(String[] fromMulti, String[] toMulti,
			String[] startMulti, int i, SlingHttpServletRequest request) {
		
		fromMulti[i] = request.getParameter("from[" + (i + 1) + "]");
		toMulti[i] = request.getParameter("to[" + (i + 1) + "]");
		startMulti[i] = request.getParameter("start["+ (i + 1) + "]");
	}
}
