package com.alitalia.aem.business.web.controllers;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.AnagraficaUtils;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.MunicipalityData;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataResponse;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;

public class RiepilogoAnagrafica extends WCMUse {
	
	private boolean alert;
	private boolean error;
	private Map<String, String> agenzia;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void activate() throws Exception {
		
		Map<String, String> mapStati = new LinkedHashMap<String, String>();
		agenzia = new HashMap<String, String>();
		
		try{
			alert = getRequest().getParameter("result") != null && getRequest().getParameter("result").equals("1");
			error = getRequest().getParameter("error") != null && getRequest().getParameter("error").equals("1");
			
			SlingHttpServletRequest request = getRequest();
			SlingScriptHelper helper = getSlingScriptHelper();
			String market = "IT";
			String language= LocalizationUtils.getLocaleByResource(request.getResource(), "it").toString().toUpperCase();
			
			StaticDataDelegate staticDataDelegate = helper.getService(StaticDataDelegate.class);
			BusinessLoginDelegate businessLoginDelegate = helper.getService(BusinessLoginDelegate.class);
			
			//obtain the Agency Infromation
			AgencyRetrieveDataResponse serviceResponse = AnagraficaUtils.obtainAgencyInformation(businessLoginDelegate,request);
			if (serviceResponse != null) {
			   
				agenzia.put("ragSociale", serviceResponse.getRagioneSociale());
				agenzia.put("piva", serviceResponse.getPartitaIVA());
				agenzia.put("cf", serviceResponse.getCodiceFiscale());
				agenzia.put("indirizzo", serviceResponse.getIndirizzo());
				agenzia.put("cap", serviceResponse.getZip());
				agenzia.put("tel", serviceResponse.getTelefono());
				agenzia.put("fax", serviceResponse.getFax());
				agenzia.put("emailTitolare", serviceResponse.getEmailTitolare());
				agenzia.put("emailBanconista", serviceResponse.getEmailPubblica());
				
				//obtain the correct state
				mapStati=AnagraficaUtils.obtainMapState();
				String stato = mapStati.get(serviceResponse.getStato().trim());
				String statoValidato = "";
				if (stato.equalsIgnoreCase("italia")) {
					statoValidato = stato;
				}
				if (stato.equalsIgnoreCase("san marino")) {
					statoValidato = stato;
				}
				agenzia.put("stato", statoValidato);
				
				//Obtain the correct Province by the stateCode
				List<CountryData> listProvinces = AnagraficaUtils.obtainListOfProvinces(staticDataDelegate,request,market,language);
				String provincia = serviceResponse.getProvincia().trim();
				String provinciaValidata = "";
				if (listProvinces != null && !listProvinces.isEmpty()) {
					for (CountryData item : listProvinces) {
						if (item.getStateCode().equalsIgnoreCase(provincia)) {
							provinciaValidata = item.getStateCode();
							break;
						}
					}
				}
				agenzia.put("provincia", provinciaValidata);
				
				//Obtain the correct City by the councilCode
				List<MunicipalityData> councilsList = AnagraficaUtils.obtainCouncilsList(staticDataDelegate,request,market,language);
				String citta = serviceResponse.getCitta().trim();
				String cittaValidata = "";
				if (councilsList != null && !councilsList.isEmpty()) {
					for (MunicipalityData item : councilsList) {
						if (item.getDescription().equalsIgnoreCase(citta)) {
							cittaValidata = item.getDescription();
							break;
						}
					}
				}
				agenzia.put("citta", cittaValidata);
			}
		}catch(Exception e){
			logger.error("Unexpected error: ", e);
			throw e;
		}
		
	}
	
	public Map<String, String> getAgenzia() {
		return agenzia;
	}
	
	public boolean getAlert() {
		return alert;
	}
	
	public boolean getError() {
		return error;
	}
}