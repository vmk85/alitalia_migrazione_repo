package com.alitalia.aem.business.web.utils;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.servlet.http.HttpServletRequest;

import org.apache.sling.api.SlingHttpServletRequest;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.MunicipalityData;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataResponse;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsRequest;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;

public class AnagraficaUtils {
	
	public static Map<String,String> obtainMapState(){
		Map<String, String> mapStati = new LinkedHashMap<String, String>();
		mapStati .put("ITA", "Italia");
		mapStati.put("SM", "San Marino");
		return mapStati;
	}
	
	public static AgencyRetrieveDataResponse obtainAgencyInformation(BusinessLoginDelegate businessLoginDelegate,
			SlingHttpServletRequest request) throws ValueFormatException, RepositoryException {
		
		AgencyRetrieveDataRequest serviceRequest = new AgencyRetrieveDataRequest(IDFactory.getTid(), IDFactory.getSid(request));
		AgencyRetrieveDataResponse serviceResponse = null;
		
		String codiceAgenzia = AlitaliaTradeAuthUtils.getProperty(request, "codiceAgenzia");
		if (codiceAgenzia != null) {
			serviceRequest.setIdAgenzia(codiceAgenzia);
			serviceResponse = businessLoginDelegate.retrieveAgencyData(serviceRequest);
		}
		return serviceResponse;
	}
	
	public static List<MunicipalityData> obtainCouncilsList(StaticDataDelegate staticDataDelegate,
			HttpServletRequest request, String market, String language) {
		
		RetrieveCouncilsRequest retrieveCouncilsRequest = new RetrieveCouncilsRequest(IDFactory.getTid(), IDFactory.getSid(request));
		retrieveCouncilsRequest.setLanguageCode(market);
		retrieveCouncilsRequest.setMarket(language);
		
		RetrieveCouncilsResponse retrieveCouncilsResponse = staticDataDelegate.retrieveCouncils(retrieveCouncilsRequest);
		return retrieveCouncilsResponse.getCouncils();
	}

	public static List<CountryData> obtainListOfProvinces(StaticDataDelegate staticDataDelegate,
			HttpServletRequest request, String market, String language){
		
			RetrieveProvincesRequest serviceProvinceRequest = new RetrieveProvincesRequest(IDFactory.getTid(), IDFactory.getSid(request));
			serviceProvinceRequest.setLanguageCode(market);
			serviceProvinceRequest.setMarket(language);
			
			RetrieveProvincesResponse serviceProvinceResponse = staticDataDelegate.retrieveProvinces(serviceProvinceRequest);
			return  serviceProvinceResponse.getProvinces();
	}
}
