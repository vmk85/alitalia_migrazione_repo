package com.alitalia.aem.business.web.utils;

public class StringUtils {
	
	private static final int INDEX_NOT_FOUND = -1;
    public static final String EMPTY = "";

	public static String stripStart(final String str, final String stripChars) {
	    int strLen;
	    if (str == null || (strLen = str.length()) == 0) {
	        return str;
	    }
	    int start = 0;
	    if (stripChars == null) {
	        while (start != strLen && Character.isWhitespace(str.charAt(start))) {
	            start++;
	        }
	    } else if (stripChars.isEmpty()) {
	        return str;
	    } else {
	        while (start != strLen && stripChars.indexOf(str.charAt(start)) != INDEX_NOT_FOUND) {
	            start++;
	        }
	    }
	    return str.substring(start);
	}
	
	public static String trimIfNotNull(String str){
		return str != null ? str.trim() : null;
	}
	
	
	/**
	 * Create a string joining the values of the provider array.
	 * 
	 * @param source The source array.
	 * @param delimiter The delimeter to apply.
	 * @return a String with the joint values.
	 */
	public static String joinArray(Object[] source, String delimiter) {
		if (source == null || source.length == 0) { 
			return "";
		}
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < source.length; i++) {
			if (i > 0) {
				builder.append(delimiter);
			}
			builder.append(source[i]);
		}
	    return builder.toString();
	}




}
