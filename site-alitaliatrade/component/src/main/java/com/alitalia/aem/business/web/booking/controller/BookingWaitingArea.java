package com.alitalia.aem.business.web.booking.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BookingWaitingArea extends BookingSessionGenericController{

	private String fromCityName;
	private String fromAirportName;
	private String fromCountryName;
	private String toCountryName;
	private String toAirportName;
	private String toCityName;
	
	private Logger logger = LoggerFactory.getLogger(BookingWaitingArea.class); 

	@Override
	public void activate() throws Exception {
		super.activate();
		
		try {
			if (isIllegalCTXState(getResponse())) {
				return;
			}
			
			fromCityName = ctx.searchElements.get(0).getFrom().getLocalizedCityName();
			fromAirportName = ctx.searchElements.get(0).getFrom().getLocalizedAirportName();
			fromCountryName = ctx.searchElements.get(0).getFrom().getLocalizedCountryName();
			fromAirportName = manageEqualsCity(fromCityName,fromAirportName);
			
			toCityName = ctx.searchElements.get(0).getTo().getLocalizedCityName();
			toAirportName = ctx.searchElements.get(0).getTo().getLocalizedAirportName();
			toCountryName = ctx.searchElements.get(0).getTo().getLocalizedCountryName();
			toAirportName = manageEqualsCity(toCityName,toAirportName);
			
		}catch(Exception e){
			logger.error("Unexpected error: ", e);
			throw e;
		}
		
	}
	

	public String getFromCityName() {
		return fromCityName;
	}

	public String getFromAirportName() {
		return fromAirportName;
	}

	public String getFromCountryName() {
		return fromCountryName;
	}

	public String getToCountryName() {
		return toCountryName;
	}

	public String getToAirportName() {
		return toAirportName;
	}

	public String getToCityName() {
		return toCityName;
	}

	private String manageEqualsCity(String cityName, String airportName) {
		if (cityName.equalsIgnoreCase(airportName)) {
			return "";
		}
		return airportName;
	}
}
