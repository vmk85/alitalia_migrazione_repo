package com.alitalia.aem.business.web.controllers;

import java.util.Map;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.MultifieldUtils;

public class BoxMappa extends WCMUse {
	
	@SuppressWarnings("rawtypes")
	private Map menuArray;
	
	@Override
    public void activate() throws Exception {
			
			menuArray = MultifieldUtils.fromJcrMultifieldToMapArrayObject(getResource(), "menus");
			
		}
		
		@SuppressWarnings("rawtypes")
		public Map getMenuArray(){
			return menuArray;
		}
	
}
