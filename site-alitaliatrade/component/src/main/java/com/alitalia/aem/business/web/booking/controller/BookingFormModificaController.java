package com.alitalia.aem.business.web.booking.controller;

import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.day.cq.i18n.I18n;

public class BookingFormModificaController extends BookingSessionGenericController{
	
	private String departureAirport;
	private String arrivalAirport;
	private String departureCity;
	private String arrivalCity;
	private String outboundDate;
	private String returnDate;
	private String cug;
	private boolean isRoundtrip;
	private int numYoungs;
	private int numInfants;
	private int numChildren;
	private int numAdults;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void activate() throws Exception {
		super.activate();
		
		try{
			
			SlingHttpServletRequest request = getRequest();
			
			if (ctx != null) {
				
				ResourceBundle resourceBundle = request.getResourceBundle(
						LocalizationUtils.getLocaleByResource(request.getResource(), LocalizationUtils.getLanguage(ctx.locale.toLanguageTag())));
				final I18n i18n = new I18n(resourceBundle);
				
				//obtain cug
				cug = ctx.cug.value();
				
				//obtain dates
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
				outboundDate = sdf.format(ctx.searchElements.get(0)
						.getDepartureDate().getTime());
				if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP) {
					returnDate =  sdf.format(ctx.searchElements.get(1)
							.getDepartureDate().getTime());
				}
				
				//obtain airports
				departureAirport = ctx.searchElements.get(0).getFrom().getAirportCode();
				departureCity = i18n.get("airportsData." + departureAirport + ".city");
				arrivalAirport = ctx.searchElements.get(0).getTo().getAirportCode();
				arrivalCity = i18n.get("airportsData." + arrivalAirport + ".city");
				
				//obtain searchKind
				isRoundtrip = ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP;
				
				//obtain passengers
				numAdults = ctx.searchPassengersNumber.getNumAdults();
				numChildren = ctx.searchPassengersNumber.getNumChildren();
				numInfants = ctx.searchPassengersNumber.getNumInfants();
				numYoungs = ctx.searchPassengersNumber.getNumYoung();
			} else {
				//in case of error the form will be compiled with empty field
				setDefaultConfiguration();
			}
			
		}catch(Exception e){
			logger.error("Unexpected error", e);
			
			//in case of error the form will be compiled with empty field
			setDefaultConfiguration();
		}
	}

	/**
	 * It sets the default configuration for the form modifica
	 * pre-compiling empty field
	 */
	private void setDefaultConfiguration() {
		this.departureAirport = "";
		this.arrivalAirport = "";
		this.departureCity = "";
		this.arrivalCity = "";
		this.outboundDate = "";
		this.returnDate = "";
		this.cug = "ADT";
		this.isRoundtrip = true;
		this.numYoungs = 0;
		this.numInfants = 0;
		this.numChildren = 0;
		this.numAdults = 0;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}
	
	public String getDepartureCity() {
		return departureCity;
	}

	public String getArrivalCity() {
		return arrivalCity;
	}

	public String getOutboundDate() {
		return outboundDate;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public String getCug() {
		return cug;
	}
	
	public boolean isRoundtrip() {
		return isRoundtrip;
	}

	public String getNumYoungs() {
		return Integer.toString(numYoungs);
	}

	public String getNumInfants() {
		return Integer.toString(numInfants);
	}

	public String getNumChildren() {
		return Integer.toString(numChildren);
	}

	public String getNumAdults() {
		return Integer.toString(numAdults);
	}
}
