package com.alitalia.aem.business.web.booking.model;

import java.util.ArrayList;
import java.util.List;

public class PassengersData {

	private List<Passenger> passengersList;
	private Contact contact1;
	private Contact contact2;
	private String email;
	
	public PassengersData() {
		passengersList = new ArrayList<Passenger>();
	}

	public List<Passenger> getPassengersList() {
		return passengersList;
	}

	public void setPassengersList(ArrayList<Passenger> passengersList) {
		this.passengersList = passengersList;
	}
	
	public void addPassenger(Passenger passenger) {
		this.passengersList.add(passenger);
	}
	
	public void setContact1(ContactType contactType, String internationalPrefix, String phoneNumber) {
		this.contact1 = new Contact(contactType, internationalPrefix, phoneNumber);
	}
	
	public Contact getContact1() {
		return contact1;
	}
	
	public void setContact2(ContactType contactType, String internationalPrefix, String phoneNumber) {
		this.contact2 = new Contact(contactType, internationalPrefix, phoneNumber);
	}
	
	public Contact getContact2() {
		return contact2;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	
}
