package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingSearchCUGEnum;
import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.BookingSession;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsData;
import com.alitalia.aem.common.data.home.TaxData;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingrefreshsearch" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.")
})
@SuppressWarnings("serial")
public class BookingRefreshSearchServlet extends SlingSafeMethodsServlet {

	@Reference
	private BookingSession bookingSession;
	
	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private ComponentContext componentContext;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
		
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException { 
		
		String genericErrorPagePlain = "#";
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		try{
			response.setContentType("application/json");
			json.object();
			genericErrorPagePlain = (String) 
					componentContext.getProperties().get("failure.page");
			
			/* -- RECUPERO DATI DALLA FORM -- */
			String solutionId = request.getParameter("solutionId");
			String indexFlight = request.getParameter("indexFlight");
			String indexBrand = request.getParameter("indexBrand");
			String direction = request.getParameter("direction");
			
			if(indexFlight == null || ("").equals(indexFlight)){
				logger.error("indexFlight not found");
				throw new RuntimeException();
			}
			int solutionFlightIndex = Integer.parseInt(indexFlight);
			
			if(indexBrand == null || ("").equals(indexBrand)){
				logger.error("indexBrand not found");
				throw new RuntimeException();
			}
			int solutionBrandIndex = Integer.parseInt(indexBrand);
			
			if(("").equals(solutionId) || solutionId==null ){
				logger.error("solutionId not found");
				throw new RuntimeException();
			}
			
			int elementSelectionIndex = Integer.parseInt(direction);
			if(direction == null || ("").equals(direction) || (elementSelectionIndex != 1 && elementSelectionIndex != 0) ){
				logger.error("direction not found");
				throw new RuntimeException();
			}
			
			BookingSessionContext ctx = (BookingSessionContext) request.getSession().getAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE);
			bookingSession.performFlightSelection(ctx, elementSelectionIndex, solutionId, solutionFlightIndex, solutionBrandIndex);
			int indexToRefresh = 0;
			if (ctx.firstSelectionIndex == elementSelectionIndex) {
				if (elementSelectionIndex == 0) {
					indexToRefresh = 1;
				}
			} else {
				indexToRefresh = -1;
			}
			json.key("indexToRefresh").value(indexToRefresh);
			writeAnalyticsInfo(ctx, json);
			json.endObject();
		}catch(Exception e){
			logger.error("Unexpected error: ", e);
			try {
				json.key("redirect").value(genericErrorPagePlain);
				json.endObject();
			} catch (JSONException e1) {
				logger.error("Error json creation: ", e1);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
			
			
			
		}
	}
	
	private void writeAnalyticsInfo(BookingSessionContext ctx, TidyJSONWriter json) throws JSONException{
		try {
			json.key("analytics_flightInfo").object();
			DecimalFormat format = new DecimalFormat("#0.00");
			DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
			otherSymbols.setDecimalSeparator('.');
			otherSymbols.setCurrencySymbol(""); 
			format.setDecimalFormatSymbols(otherSymbols);
			format.setGroupingUsed(false);
			format.setPositivePrefix("");
			format.setNegativePrefix("");
			/*Fare Basis Info*/
			try{
				if(ctx.selectionRoutes != null){
					ResultBookingDetailsData details = ((ResultBookingDetailsData)ctx.selectionRoutes.getProperties().get("BookingDetails"));
					if(details != null){
						String depFareBasis = details.getSolutionField().get(0).getPricingField().get(0).getFareField().get(0).getExtendedFareCodeField();
						json.key("depFareBasis").value(depFareBasis);
						if(!BookingSearchKindEnum.SIMPLE.equals(ctx.searchKind)){
							String retFareBasis = details.getSolutionField().get(0).getPricingField().get(0).getFareField().get(1).getExtendedFareCodeField();
							json.key("retFareBasis").value(retFareBasis);
						}
					}
				}
			}
			catch(Exception e){
					logger.debug("Exception while retriving farebasis" , e);
			}
			/*Computing fare, surcharges, taxes, totalPrice*/
			BigDecimal fare = BigDecimal.ZERO;
			BigDecimal taxes = BigDecimal.ZERO;
			BigDecimal surcharges = BigDecimal.ZERO;
			BigDecimal totalPrice = BigDecimal.ZERO;
			List<TaxData> selectionTaxes = ctx.selectionTaxes;
			if(selectionTaxes != null){
				for(TaxData taxData : selectionTaxes){
					totalPrice = totalPrice.add(taxData.getAmount());
			    	if(taxData.getCode().toLowerCase().contains("fare")){
			    		fare = fare.add(taxData.getAmount());
			    	}
			    	else if(taxData.getCode().toLowerCase().contains("taxtotal")){
			    		taxes = taxes.add(taxData.getAmount());
			    	}
			    	else if(taxData.getCode().toLowerCase().contains("yqtotal")){
			    		surcharges = surcharges.add(taxData.getAmount());
			    	}
				}
			}
			json.key("fare").value(format.format(fare));
			json.key("taxes").value(format.format(taxes));
			json.key("surcharges").value(format.format(surcharges));
			json.key("totalPrice").value(format.format(totalPrice));
			/*TODO tasso di cambio?*/
			json.key("totalPriceEuro").value(format.format(totalPrice));
			/*Compute time, brand, cost and flight number*/
			if(ctx.flightSelections != null){
				int i= 0;
				String[] prefixes = {"dep", "ret"};
				for(String prefix : prefixes){
					if(ctx.flightSelections.length > i && ctx.flightSelections[i] != null){
						FlightData genericFlight = ctx.flightSelections[i].getFlightData();
						String brandCode = ctx.flightSelections[i].getSelectedBrandCode();
						int hh = 0;
						int mm = 0;
						String flightNumber = "";
						String carrier = "";
						List<BrandData> brands = new ArrayList<BrandData>(0);
						if(genericFlight instanceof DirectFlightData){
							brands = ((DirectFlightData) genericFlight).getBrands();
							hh = ((DirectFlightData) genericFlight).getDepartureDate()
									.get(Calendar.HOUR_OF_DAY);
							mm = ((DirectFlightData) genericFlight).getDepartureDate()
									.get(Calendar.MINUTE);
							carrier = ((DirectFlightData) genericFlight).getCarrier();
							flightNumber = 	((DirectFlightData) genericFlight).getFlightNumber();
						}
						else if(genericFlight instanceof ConnectingFlightData){
							brands = ((ConnectingFlightData) genericFlight).getBrands();
							hh = ((DirectFlightData)((ConnectingFlightData) genericFlight)
									.getFlights().get(0)).getDepartureDate()
									.get(Calendar.HOUR_OF_DAY);
							mm = ((DirectFlightData)((ConnectingFlightData) genericFlight)
									.getFlights().get(0)).getDepartureDate()
									.get(Calendar.MINUTE);
							carrier = ((DirectFlightData)((ConnectingFlightData) genericFlight)
									.getFlights().get(0)).getCarrier();
							flightNumber =  ((DirectFlightData)((ConnectingFlightData) genericFlight)
											.getFlights().get(0)).getFlightNumber();
						}
						String hour = hh < 10 ? "0" + hh : String.valueOf(hh);
						String minutes = mm < 10 ? "0" + mm : String.valueOf(mm);
						json.key(prefix + "Hours").value(hour);
						json.key(prefix + "Minutes").value(minutes);
						json.key(prefix + "FlightNumber").value(carrier + flightNumber);
						for(BrandData br : brands){
							if(br.getCode().equals(brandCode)){
								String brandName = computeBrandName(ctx.cug, br);
								String brandPrice = format.format(br.getGrossFare());
								json.key(prefix + "Brand").value(brandName);
								json.key(prefix + "cost").value(brandPrice);
							}
						}
						
					}
					i++;
				}
			}
			json.endObject();
		} catch (Exception e) {
			json.endObject();
			throw e;
		}
		
	}
	
	
	private String computeBrandName(BookingSearchCUGEnum cug, BrandData brandData) {
		String brandName ="";
		if ( brandData.getCode().toLowerCase().equals("yc") ) {
			if (cug == BookingSearchCUGEnum.MILITARY) {
				return "MILITARI POLIZIA";
			} else { //caso ADT
				return "ECONOMY CLASSIC";
			}
		}
		if ( brandData.getCode().toLowerCase().equals("fa") ) {
			if (cug == BookingSearchCUGEnum.FAMILY) {
				return "FAMILY";
			} else { //caso ADT
				return "ECONOMY BASIC";
			}
		}
		switch(brandData.getCode().toLowerCase()){
			case "economybasic":{
				brandName="ECONOMY BASIC";
			}break;
			case "yl":{
				brandName="ECONOMY LIGHT";
			}break;
			case "yc":{
				brandName="ECONOMY CLASSIC";
			}break;
			case "yf":{
				brandName="ECONOMY FLEX";
			}break;
			case "co":{
				brandName="COMFORT FLEX";
			}break; 
			case "yp":{
				brandName="PREMIUM ECONOMY";
			}break;
			case "yd":{
				brandName="ECONOMY PROMO";
			}break;
			case "ys":{
				brandName="ECONOMY SAVER";
			}break;
			case "jc":{
				brandName="BUSINESS CLASSIC";
			}break;
			case "jf":{
				brandName="BUSINESS FLEX";
			}break;
			case "zz":{
				brandName="GIOVANI";
			}break;
			case "tc":{
				brandName="CONTINUITà TERRITORIALE";
			}break;
		}
		return brandName;
	}

	

}
