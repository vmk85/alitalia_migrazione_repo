package com.alitalia.aem.business.web.controllers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import javax.servlet.http.Cookie;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.AlitaliaTradeAuthUtils;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;


public class Analytics extends WCMUse{


	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private String thirdPartyCookiesAccepted = "true";
	private String marketingCookiesAccepted = "true";

	private String timestamp = "";
	private String marketCode = "";
	private String languageCode = "";
	private String currencyCode = "";
	private String dcsid = "";
	private String loggedUser = "";



	public void activate() {
		try {
			Cookie noThirdPartyCookie = getRequest().getCookie(Constants.COOKIE_NO_THIRDPARTY_COOKIES);
			Cookie noMarketingCookie = getRequest().getCookie(Constants.COOKIE_NO_MARKETING_COOKIES);
			if (noThirdPartyCookie != null) {
				thirdPartyCookiesAccepted = noThirdPartyCookie.getValue().equals("1") ? "" : "true";
			}

			if (noMarketingCookie != null) {
				marketingCookiesAccepted = noMarketingCookie.getValue().equals("1") ? "" : "true";
			}
			
			timestamp = (new SimpleDateFormat("kk:mm")).format(
					Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome")).getTime());
			marketCode = "IT";
			languageCode = "it";
			currencyCode = "EUR";
			dcsid = "dcs2229w19x87gglbe2u3ca7p_9g8e";
			AlitaliaTradeUserType role = AlitaliaTradeAuthUtils.getRuolo(getRequest());
			loggedUser = (role != null) ? (AlitaliaTradeUserType.TITOLARE.equals(role) 
					? "titolare" : "banconista") : "";
		} catch (Exception e) {
			logger.error(e.toString());

		}
	}

	public String getThirdPartyCookiesAccepted() {
		return thirdPartyCookiesAccepted;
	}

	public String getMarketingCookiesAccepted() {
		return marketingCookiesAccepted;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public String getMarketCode() {
		return marketCode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public String getDcsid() {
		return dcsid;
	}

	public String getLoggedUser() {
		return loggedUser;
	}


}
