package com.alitalia.aem.business.web.validation.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.validation.ResultValidation;
import com.day.cq.commons.TidyJSONWriter;

/**
 * This class can be extended to expose form submission
 * handlers with support for validation.
 */
@SuppressWarnings("serial")
public abstract class GenericFormValidatorServlet extends SlingAllMethodsServlet {
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * Entry point of the servlet (only POST is allowed for security reasons),
	 * look at the special request parameter and forward to the correct behavior.
	 */
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		String action = request.getParameter("_action");
		if ("validate".equals(action)) {
			performValidationBehavior(request, response);
		} else {
			performSubmitBehavior(request, response);
		}
	}
	
	/**
	 * When reached for validation, the servlet performs the validation of the
	 * received form (using the <code>validateForm</code> method) and
	 * returns the validation outcome as a JSON object in the response.
	 */
	protected void performValidationBehavior(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		ResultValidation resultValidation = validateForm(request);
		try {
			writeValidationResponse(request, response, resultValidation);
		} catch (Exception e) {
			logger.error("Error during validation.", e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * When reached for submission method, the servlet performs the validation of the
	 * received form (using the <code>validateForm method</code> method) and,
	 * if valid, execute the requested behavior (using the <code>performSubmit</code> method)
	 */
	protected void performSubmitBehavior(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		// perform the validation again before trying to execute
		ResultValidation r = validateForm(request);
		try {
			if (r.getResult()) {
				// validation passed, perform the submit
				performSubmit(request, response);
			} else {
				// unexpected invalid data, log and send HTTP error (if possible)
				logger.warn("Unexpected invalid form on submit request: {}", r.toString());
				if (!response.isCommitted()) {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				}
			}
		} catch (Exception e) {
			// unexpected exception, log and send HTTP error (if possible)
			logger.error("Error during executing form submission.", e);
			if (!response.isCommitted()) {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
	}
	
	/**
	 * Base method used to write to the output stream the validation results.
	 * 
	 * This method could be overriden to completely modify the content of the validation
	 * response. To just add some additional return value, override the
	 * <code>writeAdditionalValidationResponseData</code> instead.
	 * 
	 * @param request The request.
	 * @param response The response.
	 * @param resultValidation The validation result.
	 */
	protected void writeValidationResponse(SlingHttpServletRequest request, SlingHttpServletResponse response, 
			ResultValidation resultValidation) throws Exception {
		response.setContentType("application/json");
		// set response message (JSON)
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		json.object();
		if (!resultValidation.getFields().isEmpty()) {
			json.key("fields");
			json.object();
			for (String k : resultValidation.getFields().keySet()) {
				json.key(k).value(resultValidation.getFields().get(k));
			}
			json.endObject();
		}
		json.key("result").value(resultValidation.getResult());
		writeAdditionalValidationResponseData(request, response, json, resultValidation);
		json.endObject();
	}
	
	/**
	 * Hook method that can be used to provide additional data in the
	 * JSON output of the validation data.
	 * 
	 * @param request The request.
	 * @param response The response.
	 * @param json The JSON writer.
	 * @param resultValidation The validation result.
	 */
	protected void writeAdditionalValidationResponseData(SlingHttpServletRequest request, 
			SlingHttpServletResponse response, TidyJSONWriter json, ResultValidation resultValidation)
					throws Exception {
	}
	
	/**
	 * Utility method to expose request form validation logic to external classes.
	 * 
	 * @param request page request
	 * @return result of validation
	 */
	public final ResultValidation performValidateForm(SlingHttpServletRequest request)
			throws IOException {
		return validateForm(request);
	}
	
	/**
	 * Form validation method.
	 * 
	 * @param request page request
	 * @return result of validation
	 */
	protected abstract ResultValidation validateForm(SlingHttpServletRequest request)
		throws IOException;
	
	/**
	 * Form submit execution method.
	 * 
	 * @param request page request
	 * @param response page response
	 */
	protected abstract void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response)
		throws IOException;
	
}
