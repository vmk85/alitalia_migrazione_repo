package com.alitalia.aem.business.web.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "resourcebusiness" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class FraudnetResourceServlet extends HttpServlet  {

	private static byte[] f0a;

    static {
        f0a = new byte[]{(byte) -119, (byte) 80, (byte) 78, (byte) 71, (byte) 13, (byte) 10, (byte) 26, (byte) 10, (byte) 0, (byte) 0, (byte) 0, (byte) 13, (byte) 73, (byte) 72, (byte) 68, (byte) 82, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 8, (byte) 6, (byte) 0, (byte) 0, (byte) 0, (byte) 31, (byte) 21, (byte) -60, (byte) -119, (byte) 0, (byte) 0, (byte) 0, (byte) 10, (byte) 73, (byte) 68, (byte) 65, (byte) 84, (byte) 120, (byte) -100, (byte) 99, (byte) 0, (byte) 1, (byte) 0, (byte) 0, (byte) 5, (byte) 0, (byte) 1, (byte) 13, (byte) 10, (byte) 45, (byte) -76, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 73, (byte) 69, (byte) 78, (byte) 68, (byte) -82, (byte) 66, (byte) 96, (byte) -126};
    }

    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        Object obj = 1;
        httpServletResponse.setHeader("Access-Control-Allow-Origin", "*");
        Date date = new Date();
        try {
            long dateHeader = httpServletRequest.getDateHeader("If-Modified-Since");
            if (dateHeader == -1) {
                m0a(httpServletResponse, date, 365);
                return;
            }
            Date date2 = new Date(dateHeader);
            if (!(date2 == null || date == null)) {
                if (date2.getTime() + 31536000000L >= date.getTime()) {
                    obj = null;
                }
            }
            if (obj != null) {
                m0a(httpServletResponse, date, 365);
            } else {
                httpServletResponse.setStatus(304);
            }
        } catch (IllegalArgumentException e) {
            httpServletResponse.setStatus(400);
        }
    }

    private static void m0a(HttpServletResponse httpServletResponse, Date date, long j) throws IOException {
        long time = date.getTime();
        httpServletResponse.setDateHeader("Last-Modified", time);
        httpServletResponse.setDateHeader("Expires", time + 31536000000L);
        httpServletResponse.setHeader("Cache-Control", "max-age=" + 31536000 + ", private");
        httpServletResponse.setStatus(200);
        httpServletResponse.setContentType("image/png");
        httpServletResponse.setContentLength(f0a.length);
        httpServletResponse.getOutputStream().write(f0a);
    }
	
}