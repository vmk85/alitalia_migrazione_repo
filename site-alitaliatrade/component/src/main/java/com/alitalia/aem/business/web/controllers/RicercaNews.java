package com.alitalia.aem.business.web.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.scripting.SlingScriptHelper;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.SearchNewsData;
import com.alitalia.aem.common.messages.home.RetrieveNewsCategoriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsCategoriesResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.news.delegate.SearchNewsDelegate;

public class RicercaNews extends WCMUse {
	
	List<String> categories;
	private SearchNewsData searchValues;
	
	@Override
    public void activate() throws Exception {
		
		SlingScriptHelper helper = getSlingScriptHelper();
		SearchNewsDelegate searchNewsDelegate = helper.getService(SearchNewsDelegate.class);
		
		RetrieveNewsCategoriesRequest serviceRequest = new RetrieveNewsCategoriesRequest();
		
		serviceRequest.setTid(IDFactory.getTid());
		serviceRequest.setSid(IDFactory.getSid(getRequest()));
		
		RetrieveNewsCategoriesResponse serviceResponse = searchNewsDelegate.searchNewsCategories(serviceRequest);
		
		categories = serviceResponse.getCategories() != null ? serviceResponse.getCategories() : (new ArrayList<String>());
		searchValues = (SearchNewsData) getRequest().getSession().getAttribute("searchNewsData");
		if (searchValues == null) {
			searchValues = new SearchNewsData();
		}
	}
	
	public List<String> getCategories() {
		return categories;
	}
	
	public SearchNewsData getSearchValues() {
		return searchValues;
	}
}
