package com.alitalia.aem.business.web.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import com.alitalia.aem.common.messages.home.AgencyLoginRequest;
import com.alitalia.aem.common.messages.home.AgencyLoginResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import org.apache.felix.scr.annotations.*;
import org.apache.felix.scr.annotations.Properties;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.crypto.CryptoSupport;
import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = {"encrypttoken"}),
        @Property(name = "sling.servlet.methods", value = {"GET"}),
        @Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
        @Property(name = "encrypt.key", description = "Key used for encrypt data"),
        @Property(name = "encrypt.initializationVector", description = "IV used for encrypt data"),
        @Property(name = "encrypt.algorithm", description = "Algorithm used for encrypt data"),
        @Property(name = "encrypt.transformation", description = "Transformation parameter for encryption data"),
})
@SuppressWarnings("serial")
public class EncryptTokenServlet extends SlingSafeMethodsServlet {

    /**
     * Reference to the service to AEM Granite Crypto API.
     */
    @Reference
    private CryptoSupport cryptoSupport;

    private ComponentContext componentContext;

    @Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_UNARY)
    private volatile BusinessLoginDelegate businessLoginDelegate;


    // logger
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Activate
    private void activate(ComponentContext componentContext) {
        this.componentContext = componentContext;
    }

    @Override
    protected void doGet(SlingHttpServletRequest request,
                         SlingHttpServletResponse response) throws ServletException, IOException {

        boolean isError = false;
        String errorMessage = null;
        String token = null;
        String tradeUser = request.getParameter("tradeUser");
        String tradePsw = request.getParameter("tradePsw");
        String ruoloForm = request.getParameter("ruolo");
//        String tradeUser = "160354";
//        String tradePsw = "Lavinia2";
//        String ruoloForm = "T";

        /** Validazione dati di login */

        logger.debug("Inizio creazione Token per Gemini");

        if (validateRequestLoginAccessData(tradeUser, tradePsw, ruoloForm)) {

            /** controllo a db se esiste l'utente*/
            AgencyLoginResponse agencyLoginResponse = userLogin(tradeUser, tradePsw, ruoloForm);

            if (agencyLoginResponse.getAgencyData().isGroupEnabled()){

                /** Genero il Token */
                token = "https://gruppi.alitaliatrade.it/2B_IT/AlitaliaTrade/RedirectToGemini.aspx?token=" + encryptToken(tradeUser, tradePsw, ruoloForm);

                if (token.equals("")) {
                    logger.error("Errore generazione token");
                    errorMessage = "ErrorLogin";
                    isError = true;
                }
            } else {
                logger.error("Utente non autorizzato");
                errorMessage = "ErrorLogin.not.enabled";
                isError = true;
            }
        } else {
            logger.error("Errore validazione credenziali, dati mancanti");
            errorMessage = "ErrorLogin";
            isError = true;
        }

            try {

                response.setContentType("application/json");
                TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
                json.object();
                json.key("isError").value(isError);
                json.key("errorMessage").value(errorMessage);
                json.key("result").value(token);
                json.endObject();
            } catch (Exception e) {
                logger.error("Unexected error generating JSON response." + e.getMessage());
            }
    }

    private String encryptToken(String tradeUser, String tradePsw, String ruoloForm) {

        String messageEncrypted = "";

        try {
            AlitaliaTradeUserType ruolo = null;
            if (ruoloForm != null) {
                if (ruoloForm.equals("T")) {
                    ruolo = AlitaliaTradeUserType.TITOLARE;
                } else if (ruoloForm.equals("B")) {
                    ruolo = AlitaliaTradeUserType.BANCONISTA;
                }
            }

            long ts = System.currentTimeMillis();
            Date localTime = new Date(ts);

            // Convert Local Time to UTC (Works Fine)
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
            String timestamp = sdf.format(localTime);

            String message = (ruolo.equals(AlitaliaTradeUserType.BANCONISTA) ? "1" : "0") + "-" +
                    tradeUser + "-" + tradePsw + "-" + timestamp;

            logger.debug("Message: {}", message.replaceAll(tradePsw, "********"));

            String algorithm = (String) componentContext.getProperties().get("encrypt.algorithm");
            String key = (String) componentContext.getProperties().get("encrypt.key");
            String initializationVector = (String) componentContext.getProperties().get("encrypt.initializationVector");
            String transformation = (String) componentContext.getProperties().get("encrypt.transformation");

            messageEncrypted = encrypt(algorithm, key, initializationVector, transformation, message);
            logger.debug("Message encrypted: {}", messageEncrypted);

        } catch (Exception e) {
            logger.error("Error during token encrypton ", e);
        }

        return messageEncrypted;
    }

    private AgencyLoginResponse userLogin(String tradeUser, String tradePsw, String ruoloForm){

        AlitaliaTradeUserType ruolo = null;
        if (ruoloForm != null) {
            if (ruoloForm.equals("T")) {
                ruolo = AlitaliaTradeUserType.TITOLARE;
            } else if (ruoloForm.equals("B")) {
                ruolo = AlitaliaTradeUserType.BANCONISTA;
            }
        }

        AgencyLoginResponse agencyLoginResponse = null;
        try {
            String tid = IDFactory.getTid();
            String sid = IDFactory.getSid();
            AgencyLoginRequest agencyLoginRequest = new AgencyLoginRequest(tid, sid);
            agencyLoginRequest.setCodiceAgenzia(tradeUser);
            agencyLoginRequest.setPassword(tradePsw);
            agencyLoginRequest.setRuolo(ruolo);
            agencyLoginResponse = businessLoginDelegate.agencyLogin(agencyLoginRequest);

        } catch (Exception e) {
            logger.error("Error to try login ", e);
        }

        return agencyLoginResponse;
    }

//    private boolean userLoggedExist(String tradeUser, String tradePsw, String ruoloForm) {
//
//        boolean userExist = false;
//
//        AlitaliaTradeUserType ruolo = null;
//        if (ruoloForm != null) {
//            if (ruoloForm.equals("T")) {
//                ruolo = AlitaliaTradeUserType.TITOLARE;
//            } else if (ruoloForm.equals("B")) {
//                ruolo = AlitaliaTradeUserType.BANCONISTA;
//            }
//        }
//
//        AgencyLoginResponse agencyLoginResponse = null;
//        try {
//            String tid = IDFactory.getTid();
//            String sid = IDFactory.getSid();
//            AgencyLoginRequest agencyLoginRequest = new AgencyLoginRequest(tid, sid);
//            agencyLoginRequest.setCodiceAgenzia(tradeUser);
//            agencyLoginRequest.setPassword(tradePsw);
//            agencyLoginRequest.setRuolo(ruolo);
//            agencyLoginResponse = businessLoginDelegate.agencyLogin(agencyLoginRequest);
//            if (agencyLoginResponse != null && agencyLoginResponse.isLoginSuccessful() && !agencyLoginResponse.isAccountLocked() && agencyLoginResponse.getAgencyData().isGroupEnabled()) {
//                userExist = true;
//            }
//
//        } catch (Exception e) {
//            logger.error("Error to try login ", e);
//        }
//
//        return userExist;
//    }

    private boolean validateRequestLoginAccessData(String tradeUser, String tradePsw, String ruoloForm) {

        boolean isValid = false;

        if (tradeUser != null && tradePsw != null && ruoloForm != null) {

            if (!tradePsw.equals("") && !tradeUser.equals("") && !ruoloForm.equals("")) {

                if (ruoloForm.equals("B") || ruoloForm.equals("T")) {
                    isValid = true;
                }

            }
        }

        return isValid;
    }

    private String encrypt(String algorithm, String key, String initializationVector, String transformation, String message) throws Exception {

        logger.debug("Try to encrypt message with parameters: [algorithm = " + algorithm +
                ", transformation = " + transformation + ", initializationVector = " + initializationVector + ", message = " + message + "]");

        if (algorithm == null || algorithm.isEmpty() || transformation == null || transformation.isEmpty() ||
                initializationVector == null || initializationVector.isEmpty() || key == null || key.isEmpty()) {
            logger.error("Some parameters are empty: [" +
                    (algorithm == null || algorithm.isEmpty() ? " algorithm" : "") +
                    (transformation == null || transformation.isEmpty() ? " transformation" : "") +
                    (initializationVector == null || initializationVector.isEmpty() ? " initializationVector" : "") +
                    (key == null || key.isEmpty() ? " key" : "") +
                    "]");
            return null;
        }

        byte[] keyValue = key.getBytes("UTF-8");
        DESKeySpec keySpec = new DESKeySpec(keyValue);

        IvParameterSpec ivSpec = new IvParameterSpec(initializationVector.getBytes("UTF-8"));

        SecretKey secretKey = SecretKeyFactory.getInstance(algorithm).generateSecret(keySpec);
        Cipher encrypter = Cipher.getInstance(transformation);

        encrypter.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);

        byte[] input = message.getBytes("UTF-8");
        byte[] encrypted = encrypter.doFinal(input);

        return Base64.getEncoder().encodeToString(encrypted);
    }
}
