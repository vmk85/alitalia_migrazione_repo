package com.alitalia.aem.business.web.controllers;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.AlitaliaTradeAuthUtils;
import com.alitalia.aem.common.data.home.CodeDescriptionData;
import com.alitalia.aem.common.data.home.IPAddressData;
import com.alitalia.aem.common.data.home.enumerations.AddressFamilyEnum;
import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoCitiesResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.geospatial.delegate.GeoSpatialDelegate;

public class ListaOfferte extends WCMUse {
	
	private static Logger logger = LoggerFactory.getLogger(ListaOfferte.class);
	
	private Map<String, String> airportMap;
	private String selectedCity = null;
	private String whichHomepage;
	
	@Override
    public void activate() throws Exception {
		
		try {
			SlingScriptHelper helper = getSlingScriptHelper();
			GeoSpatialDelegate geoSpatialDelegate = helper.getService(GeoSpatialDelegate.class);
			
			ValueMap properties = getProperties();
			boolean isYoung = properties.get("isYoung") != null && properties.get("isYoung", Boolean.class);
			
			Long ip = ipToLong(getRequest().getRemoteAddr());
			airportMap = new LinkedHashMap<String, String>();
			
			if (getRequest().getParameter("from") != null && !getRequest().getParameter("from").isEmpty()) {
				selectedCity = getRequest().getParameter("from");
				
			} else if (ip != null) {
				RetrieveGeoAirportsRequest serviceRequest = new RetrieveGeoAirportsRequest();
				serviceRequest.setTid(IDFactory.getTid());
				serviceRequest.setSid(IDFactory.getSid(getRequest()));
				IPAddressData ipAddressData = new IPAddressData();
				//ipAddressData.setmAddress(1554008144L);
				ipAddressData.setmAddress(ip.longValue());
				ipAddressData.setmFamily(AddressFamilyEnum.INTER_NETWORK);
				
				serviceRequest.setIpAddress(ipAddressData);
				serviceRequest.setLanguageCode("IT");
				serviceRequest.setMarketCode("IT");
				
				RetrieveGeoAirportsResponse serviceResponse = geoSpatialDelegate.retrieveGeoAirports(serviceRequest);
				
				if (serviceResponse != null && serviceResponse.getCodesDescriptions() != null && !serviceResponse.getCodesDescriptions().isEmpty()) {
					selectedCity = serviceResponse.getCodesDescriptions().get(0).getCode();
				}
			}
			
			RetrieveGeoCitiesRequest serviceRequest = new RetrieveGeoCitiesRequest();
			serviceRequest.setTid(IDFactory.getTid());
			serviceRequest.setSid(IDFactory.getSid(getRequest()));
			serviceRequest.setLanguageCode("IT");
			serviceRequest.setMarketCode("IT");
			serviceRequest.setPaxType(isYoung ? "yg" : "ad");
			
			RetrieveGeoCitiesResponse serviceResponse = geoSpatialDelegate.retrieveGeoCities(serviceRequest);
			
			boolean found = false;
			
			
			if (serviceResponse != null && serviceResponse.getCities() != null && !serviceResponse.getCities().isEmpty()) {
				for (CodeDescriptionData code : serviceResponse.getCities()) {
					airportMap.put(code.getCode(), "airportsData." + code.getCode() + ".city");
					if (code.getCode().equals(selectedCity)) {
						found = true;
					}
				}
			}
			
			if (!found) {
				selectedCity = null;
			}
			
			if (selectedCity == null) {
				selectedCity = properties.containsValue("defaultCity") ? (String) properties.get("defaultCity") : "ROM";
			}
			
			whichHomepage = "home-page";
			boolean isIata = Boolean.parseBoolean(AlitaliaTradeAuthUtils.getProperty(getRequest(), "isIata"));
//			if (isIata) {
//				whichHomepage = "hp-iata";
//			}
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}
	}
	
	public Long ipToLong(String ipAddress) {
		
		String[] ipAddressInArray = ipAddress.split("\\.");
		
		if (ipAddressInArray.length == 4) {
			long result = 0;
			for (int i = 3; i >= 0; i--) {
				int ip = Integer.parseInt(ipAddressInArray[i]);
				result += ip * Math.pow(256, i);
			}
			
			return result;
		}
		return null;
	}
	
	public Map<String, String> getAirportMap() {
        return airportMap;
    }
	
	public String getSelectedCity() {
		return selectedCity;
	}
	
	public String getWhichHomepage(){
		return whichHomepage;
	}
}
