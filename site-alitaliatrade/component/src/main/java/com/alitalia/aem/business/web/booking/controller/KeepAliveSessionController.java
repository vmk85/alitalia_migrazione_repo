package com.alitalia.aem.business.web.booking.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;

public class KeepAliveSessionController extends WCMUse {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	//configuration holder
	protected AlitaliaTradeConfigurationHolder configuration;

	private int numberOfTimeout = 0;
	private int timeout = 0;
	@Override
	public void activate() throws Exception {

		try {
			SlingScriptHelper helper = getSlingScriptHelper();
	    	configuration = helper.getService(AlitaliaTradeConfigurationHolder.class);
			if (configuration != null) {
		    	numberOfTimeout = configuration.getNumberOfTimeout();
		    	timeout = configuration.getTimeout();
			}
		} catch (Exception e) {
			logger.error("unexpected error ", e);
			if (!getResponse().isCommitted()){
				getResponse().sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
			}
		}

	}
	
	public int getNumberOfTimeout() {
		return numberOfTimeout;
	}
	
	public int getTimeout() {
		return timeout;
	}
	
	
}