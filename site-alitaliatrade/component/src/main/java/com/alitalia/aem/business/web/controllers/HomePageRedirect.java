package com.alitalia.aem.business.web.controllers;

import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUse;

public class HomePageRedirect extends WCMUse{
	
	@Override
    public void activate() throws Exception {
		ValueMap properties = getPageProperties();
		String link = (String) properties.get("linkhpselector");
		if (link != null) {
			SlingHttpServletResponse response = getResponse();			
			response.setStatus(302);
	        response.setHeader("Location", link + ".html");
	        response.setHeader("Connection", "close");
	        return;
		}
	}
}
