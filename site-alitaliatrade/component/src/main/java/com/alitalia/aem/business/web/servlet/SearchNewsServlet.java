package com.alitalia.aem.business.web.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.business.web.utils.SearchNewsData;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;


@Component(metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "searchnewsservlet" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
	@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.") })

@SuppressWarnings("serial")
public class SearchNewsServlet extends GenericFormValidatorServlet {
	
	private ComponentContext componentContext;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		Validator validator = new Validator();
        boolean filter_start = false;
        boolean filter_end = false;

		// start date
		String start = request.getParameter("start");
		if (start!=null && !start.isEmpty()) {
			filter_start = true;
			validator.addDirectParameterMessagePattern("start", start, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Data inizio", "isDate", "isBeforeToday");
		}
		
		// end date
		String end = request.getParameter("end");
		if (end != null && !end.isEmpty()) {
			filter_end = true;
			validator.addDirectParameterMessagePattern("end", end, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Data fine", "isDate", "isBeforeToday");
		}
		
		// check if start date is before end date
		if (filter_start && filter_end) {
			validator.addCrossParameter("start", start, end, Constants.MESSAGE_DATE_PAST, "beforeThen");
		}
		
		return validator.validate();
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		
		try {
		
			String start = request.getParameter("start");
			String end = request.getParameter("end");
			String category = request.getParameter("category");
			String title = request.getParameter("title");
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
			dateFormat.setLenient(false);
			
			SearchNewsData searchNewsData = new SearchNewsData();
			
			if (start != null && !start.isEmpty()) {
				Calendar dateFrom = Calendar.getInstance();
				dateFrom.setTime(dateFormat.parse(start));
				searchNewsData.setDateFrom(dateFrom);
			}
			
			if (end != null && !end.isEmpty()) {
				Calendar dateTo = Calendar.getInstance();
				dateTo.setTime(dateFormat.parse(end));
				searchNewsData.setDateTo(dateTo);
			}
			
			searchNewsData.setCategory(category);
			searchNewsData.setTitle(title);
			
			request.getSession().setAttribute("searchNewsData", searchNewsData);
			
			String resultPage = (String) componentContext.getProperties().get("success.page");
			
			if (resultPage != null) {
				response.sendRedirect(resultPage);
			} else {
				logger.error("Property success.page is null");
			}
		
		} catch (Exception e) {
			logger.error("Exception during performSubmit of SearchNewsServlet", e);
			String failurePage = (String) componentContext.getProperties().get("failure.page");
			if (failurePage != null) {
				response.sendRedirect(failurePage);
			}
		}
	}
}
