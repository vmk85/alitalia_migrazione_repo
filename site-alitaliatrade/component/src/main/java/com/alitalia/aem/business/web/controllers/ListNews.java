package com.alitalia.aem.business.web.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.News;
import com.alitalia.aem.business.web.utils.PagerElement;
import com.alitalia.aem.business.web.utils.SearchNewsData;
import com.alitalia.aem.common.data.home.NewsData;
import com.alitalia.aem.common.messages.home.RetrieveFeaturedNewsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFeaturedNewsResponse;
import com.alitalia.aem.common.messages.home.RetrieveNewsRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.news.delegate.SearchNewsDelegate;

public class ListNews extends WCMUse {
	
	public static String TIPOLOGIA_IN_EVIDENZA = "IN EVIDENZA";

	private List<News> newsList;
	private List<PagerElement> links;
	private Integer previous;
	private Integer next;
	private String detailURL;
	
	@Override
    public void activate() throws Exception {
		
		ValueMap properties = getProperties();
		boolean featured = properties.get("featured") != null && properties.get("featured", Boolean.class);
		boolean pager = properties.get("pager") != null && properties.get("pager", Boolean.class);
		boolean search = getRequest().getParameter("search") != null;
		
		SearchNewsData searchNewsData = null;
		
		// Recupero il filtro se si tratta di una ricerca, altrimenti lo elimino
		if (search) {
			searchNewsData = (SearchNewsData) getRequest().getSession().getAttribute("searchNewsData");
		} else {
			getRequest().getSession().setAttribute("searchNewsData", null);
		}
		
		int pageSize = properties.get("numNews") != null ? properties.get("numNews", Integer.class) : 0;
		int numLinks = properties.get("numLink") != null ? properties.get("numLink", Integer.class) : 5;
		detailURL = properties.get("urlDettaglio") != null ? properties.get("urlDettaglio", String.class) : "";
		
		int selectedPage = 0;
		if (getRequest().getParameter("page") != null && !getRequest().getParameter("page").isEmpty()) {
			try {
				selectedPage = Integer.parseInt(getRequest().getParameter("page")) - 1;
				if (selectedPage < 0) {
					selectedPage = 0;
				}
			} catch (NumberFormatException e) {}
		}
		
		// Retrieve news
		
		// WS
		List<NewsData> newsDataList = featured ? getFeaturedNews() : searchNews(searchNewsData);
		
		// MOCK
		/*ArrayList<NewsData> newsDataList = new ArrayList<NewsData>();
		
		for (int i = 0; i < 219; i++) {
			NewsData item = new NewsData();
			item.setDataPub(Calendar.getInstance());
			item.setId(i);
			item.setTipologia((i > 0 && i % 91 == 0 ? TIPOLOGIA_IN_EVIDENZA : "ALTRO"));
			item.setTitolo("News " + i);
			newsDataList.add(item);
		}*/
		
		int numPages = (int) Math.round(Math.ceil((double) newsDataList.size() / (double) pageSize));
		if (selectedPage > numPages) {
			return;
		}
		
		if (featured) {
			addNewsFeatured(newsDataList, pageSize);
		}
		else if (pager) {
			addNews(newsDataList, pageSize, selectedPage);
		} else {
			addNews(newsDataList, pageSize);
		}
		
		// Memorizzo in sessione per il successivo recupero dei valori titolo, data e categoria nel dettaglio news
		getRequest().getSession().setAttribute("newsList", newsList);
		
		if (pager) {
			links = new ArrayList<PagerElement>();
			
			int start = selectedPage - (numLinks / 2);
			if (start < 0) {
				start = 0;
			}
			
			int end = start + numLinks; 
			
			if (end > numPages) {
				end = numPages;
				start = end - numLinks;
				while (start < 0) {
					start++;
				}
			}
			
			for (int i = start; i < end; i++) {
				PagerElement element = new PagerElement();
				element.setNumber(i+1);
				element.setLabel(String.valueOf(i+1));
				element.setLink(String.valueOf(i+1));
				if (i == selectedPage) {
					element.setActive(true);
				}
				links.add(element);
			}
			
			if (selectedPage > 0) {
				previous = selectedPage;
			}
			
			if (selectedPage < numPages - 1) {
				next = selectedPage + 2;
			}
		}
	}
	
	private List<NewsData> getFeaturedNews() {
		SlingScriptHelper helper = getSlingScriptHelper();
		SearchNewsDelegate searchNewsDelegate = helper.getService(SearchNewsDelegate.class);
		
		RetrieveFeaturedNewsRequest serviceRequest = new RetrieveFeaturedNewsRequest();
		
		serviceRequest.setTid(IDFactory.getTid());
		serviceRequest.setSid(IDFactory.getSid(getRequest()));
		
		RetrieveFeaturedNewsResponse serviceResponse = searchNewsDelegate.searchFeaturedNews(serviceRequest);
		
		return serviceResponse.getNews();
	}
	
	private List<NewsData> searchNews(SearchNewsData searchNewsData) {
		SlingScriptHelper helper = getSlingScriptHelper();
		SearchNewsDelegate searchNewsDelegate = helper.getService(SearchNewsDelegate.class);
		
		RetrieveNewsRequest serviceRequest = new RetrieveNewsRequest();
		
		serviceRequest.setTid(IDFactory.getTid());
		serviceRequest.setSid(IDFactory.getSid(getRequest()));
		
		if (searchNewsData != null) {
			
			if (searchNewsData.getCategory() != null && !searchNewsData.getCategory().isEmpty()) {
				serviceRequest.setCategory(searchNewsData.getCategory());
			}
			
			if (searchNewsData.getDateFrom() != null) {
				serviceRequest.setDateFrom(searchNewsData.getDateFrom());
			}
			
			if (searchNewsData.getDateFrom() != null) {
				serviceRequest.setDateTo(searchNewsData.getDateTo());
			}
			
			if (searchNewsData.getTitle() != null && !searchNewsData.getTitle().isEmpty()) {
				serviceRequest.setTitle(searchNewsData.getTitle());
			}
		}
		
		RetrieveNewsResponse serviceResponse = searchNewsDelegate.searchNews(serviceRequest);
		return serviceResponse.getNews();
	}
	
	private void addNews(List<NewsData> newsDataList, int pageSize) {
		addNews(newsDataList, pageSize, 0);
	}
	
	private void addNews(List<NewsData> newsDataList, int pageSize, int selectedPage) {
		
		newsList = new ArrayList<News>();
		
		int counter = 0;
		Iterator<NewsData> iterator = newsDataList.iterator();
		
		// skip to selected page
		while (iterator.hasNext() && counter < selectedPage*pageSize) {
			iterator.next();
			counter++;
		}
		
		counter = 0;
		while (iterator.hasNext() && counter < pageSize) {
			addSingleNews(iterator.next(), newsList);
			counter++;
		}
	}
	
	private void addNewsFeatured(List<NewsData> newsDataList, int size) {
		
		ArrayList<News> featuredNewsList = new ArrayList<News>();
		ArrayList<News> commonNewsList = new ArrayList<News>();
		
		Iterator<NewsData> iterator = newsDataList.iterator();
		
		while (iterator.hasNext()) {
			NewsData newsData = iterator.next();
			if (TIPOLOGIA_IN_EVIDENZA.equalsIgnoreCase(newsData.getTipologia())) {
				addSingleNews(newsData, featuredNewsList);
			} else {
				addSingleNews(newsData, commonNewsList);
			}
		}
		
		ArrayList<News> tempList = new ArrayList<News>();
		tempList.addAll(featuredNewsList);
		tempList.addAll(commonNewsList);
		
		newsList = new ArrayList<News>();
		
		int n = size > tempList.size() ? tempList.size() : size;
		for (int i = 0; i < n; i++) {
			newsList.add(tempList.get(i));
		}
	}
	
	private void addSingleNews(NewsData newsData, List<News> list) {
		News news = new News();
		news.setId(newsData.getId());
		news.setTitle(newsData.getTitolo().trim());
		news.setDay(String.valueOf(newsData.getDataPub().get(Calendar.DAY_OF_MONTH)));
		news.setMonth(String.valueOf(newsData.getDataPub().get(Calendar.MONTH) +1));
		SimpleDateFormat sdf = new SimpleDateFormat("yy");
		String year = sdf.format(newsData.getDataPub().getTime());
		news.setYear(year);
		news.setCategory(newsData.getTipologia().trim());
		news.setLink(detailURL + ".html");
		news.setFeatured(TIPOLOGIA_IN_EVIDENZA.equalsIgnoreCase(newsData.getTipologia()));
		list.add(news);
	}
	
	public List<News> getNewsList() {
		return newsList;
	}
	
	public List<PagerElement> getLinks() {
		return links;
	}
	
	public Integer getPrevious() {
		return previous;
	}
	
	public Integer getNext() {
		return next;
	}
}