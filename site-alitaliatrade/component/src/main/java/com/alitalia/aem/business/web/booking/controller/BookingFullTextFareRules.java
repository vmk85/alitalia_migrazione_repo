package com.alitalia.aem.business.web.booking.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.scripting.SlingScriptHelper;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.utils.Constants;

public class BookingFullTextFareRules extends BookingSessionGenericController {

	// Configuration manager
	protected AlitaliaTradeConfigurationHolder configuration;
	
	private ArrayList<String> fullTextFareRules;
	
	@Override
	public void activate() throws Exception {

		super.activate();
		
		try {
			SlingScriptHelper helper = getSlingScriptHelper();
	    	configuration = helper.getService(AlitaliaTradeConfigurationHolder.class);
			ctx = (BookingSessionContext) getRequest().getSession(true).getAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE);
			
			fullTextFareRules = null; 
			if (ctx.fullTextFareRules != null) {
				fullTextFareRules = ctx.fullTextFareRules;
			}
			
		} catch (Exception e) {
			logger.error("Cannot find booking session context in session. ", e);
			ctx = null;
		}
		
	}

	public List<String> getFullTextFareRules() {
		return fullTextFareRules;
	}
	
}