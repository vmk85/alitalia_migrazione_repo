package com.alitalia.aem.business.web.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.ListOffersUtils;
import com.alitalia.aem.business.web.utils.Offerta;
import com.alitalia.aem.common.data.home.OfferData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersRequest;
import com.alitalia.aem.common.messages.home.RetrieveGeoOffersResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.geospatial.delegate.GeoSpatialDelegate;

public class ListaOffertePartial extends WCMUse {
	
	private static final String URL_DESTINAZIONE_ITALIA = "dettaglio-destinazione-italia.html";
	private static final String URL_DESTINAZIONE_MONDO = "dettaglio-destinazione-mondo.html";

	private ArrayList<Offerta> offerteLeft;
	private ArrayList<Offerta> offerteRight;
	
	private static final int MAX_HP_OFFERS = 3;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
    public void activate() throws Exception {
		
		try {
			offerteLeft = new ArrayList<Offerta>();
			offerteRight = new ArrayList<Offerta>();
			
			SlingScriptHelper helper = getSlingScriptHelper();
			
			String departure = getRequest().getParameter("from");
			
			boolean isHomePage = getRequest().getParameter("isHomePage") != null && getRequest().getParameter("isHomePage").equals("true");
			boolean isYoung = getRequest().getParameter("isYoung") != null && getRequest().getParameter("isYoung").equals("true");
			String type = getRequest().getParameter("type");
			
			if (departure != null) {
			
				String paxType = isYoung ? "yg" : "ad";
				
				GeoSpatialDelegate geoSpatialDelegate = helper.getService(GeoSpatialDelegate.class);
				
				RetrieveGeoOffersRequest serviceRequest = new RetrieveGeoOffersRequest();
				
				serviceRequest.setSid(IDFactory.getSid(getRequest()));
				serviceRequest.setTid(IDFactory.getTid());
				
				serviceRequest.setLanguageCode("IT");
				serviceRequest.setMarketCode("IT");
				serviceRequest.setDepartureAirportCode(departure.toLowerCase());
				serviceRequest.setPaxType(paxType);
				
				RetrieveGeoOffersResponse serviceResponse = null;
				serviceResponse = geoSpatialDelegate.retrieveGeoOffers(serviceRequest);
				
				List<OfferData> offerDataList = serviceResponse.getOffers();
				List<OfferData> filteredOfferDataList = ListOffersUtils.filterOffers(offerDataList);
				
				int counter = 0;
				int counterSx = 0;
				int counterDx = 0;
				ArrayList<Offerta> offerteItalia = new ArrayList<Offerta>();
				ArrayList<Offerta> offerteMondo = new ArrayList<Offerta>();
				
				for (OfferData offerData : filteredOfferDataList) {
						
					if (type == null || type.isEmpty() || (type.equals("INT") && (AreaValueEnum.INTC.equals(offerData.getArea()) || AreaValueEnum.INTZ.equals(offerData.getArea()))) ||
						(type.equals("DOM") && AreaValueEnum.DOM.equals(offerData.getArea()))) {
				
						Offerta offerta = new Offerta();
						
						offerta.setDestinazione("airportsData." + offerData.getArrivalAirport().getCode() + ".city");
						offerta.setPrezzo(String.valueOf(Math.round(offerData.getBestPrice().doubleValue())));
						
						String link = AreaValueEnum.DOM.equals(offerData.getArrivalAirport().getArea()) ? URL_DESTINAZIONE_ITALIA + "?isDOM=true" : URL_DESTINAZIONE_MONDO + "?isDOM=false";
						link += "&from=" + departure + "&to=" + offerData.getArrivalAirport().getCode();
						link += "&id=" + counter;
						if (isYoung) {
							link += "&isYoung=true";
						}
						
						offerta.setLink(link);
						offerta.setId(counter);
						
						if (RouteTypeEnum.RETURN.equals(offerData.getRouteType())) {
							offerta.setTipo("andata e ritorno");
						} else {
							offerta.setTipo("sola andata");
						}
						
						
						if (isHomePage) {
							link = AreaValueEnum.DOM.equals(offerData.getArrivalAirport().getArea()) ? URL_DESTINAZIONE_ITALIA + "?isDOM=true" : URL_DESTINAZIONE_MONDO + "?isDOM=false";
							link += "&from=" + departure + "&to=" + offerData.getArrivalAirport().getCode();
							if (AreaValueEnum.DOM.equals(offerData.getArrivalAirport().getArea())) {
								link += "&id=" + counterSx;
								offerta.setLink(link);
								offerteItalia.add(offerta);
								counterSx++;
							}
							else {
								link += "&id=" + counterDx;
								offerta.setLink(link);
								offerteMondo.add(offerta);
								counterDx++;
							}

							if (counterSx == MAX_HP_OFFERS && counterDx ==MAX_HP_OFFERS) {
								break;
							}
						}
						else {
							if (counter % 2 == 0) {
								offerteLeft.add(offerta);
							} else {
								offerteRight.add(offerta);
							}
						}
			
						counter++;
						
					}
				}
				if (isHomePage) {
					for (int i=0;i<Math.min(offerteItalia.size(), MAX_HP_OFFERS);i++) {
						offerteLeft.add(offerteItalia.get(i));
					}
					for (int i=0;i<Math.min(offerteMondo.size(), offerteLeft.size());i++) {
						offerteRight.add(offerteMondo.get(i));
					}
					int delta = 0;
					while(offerteRight.size()<offerteLeft.size()) {
						try {
							offerteRight.add(offerteItalia.get(offerteLeft.size()+delta));
							delta++;
						} catch (IndexOutOfBoundsException e) {
							break;
						}
					}
				}

			}
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			throw (e);
		}
	}
	
	public List<Offerta> getOfferteLeft() {
        return offerteLeft;
    }
	public List<Offerta> getOfferteRight() {
        return offerteRight;
    }
	
}
