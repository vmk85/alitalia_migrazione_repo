package com.alitalia.aem.business.web.controllers;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingPhaseEnum;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.booking.controller.BookingSessionGenericController;
import com.alitalia.aem.business.web.booking.render.GenericPriceRender;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;

public class RiepilogoTariffe extends BookingSessionGenericController {

	private GenericPriceRender price;
	private boolean isBestFare;
	private boolean showFee;
	private boolean showChildren;
	private boolean showInfant;
	private GenericPriceRender ecoupon;

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void activate() throws Exception {
		super.activate();
		
		try{

			if (isIllegalCTXState(getResponse())){
				return;
			}
			
			computeAndSpareFareAndTaxes(ctx);
			isBestFare = isSelectedAtLeastOneBestFare(ctx);
			showFee = (ctx.phase == BookingPhaseEnum.DONE);
			showChildren = ctx.searchPassengersNumber.getNumChildren() > 0;
			showInfant = ctx.searchPassengersNumber.getNumInfants() > 0;
		}catch(Exception e){
			logger.error("Unexpected error: ", e);
			throw e;
		}
	}
	
	public GenericPriceRender getPrice() {
		return price;
	}

	public boolean isBestFare() {
		return isBestFare;
	}
	
	public boolean showFee() {
		return showFee;
	}
	
	public boolean showChildren() {
		return showChildren;
	}
	
	public boolean showInfant() {
		return showInfant;
	}
	
	public GenericPriceRender getEcoupon() {
		return ecoupon;
	}

	
	/*private methods*/
	
	private boolean isSelectedAtLeastOneBestFare(BookingSessionContext ctx) {
		
		int index = 0;
		if (ctx.selectionRoutes!=null) {
			for ( RouteData selectionRouts : ctx.selectionRoutes.getRoutesList() ){
				FlightData flightData = selectionRouts.getFlights().get(0); 
				String selectedFlightNumber = "";
				String currentFlightNumber = "";
				if(flightData.getFlightType() == FlightTypeEnum.CONNECTING){
					selectedFlightNumber = ((DirectFlightData)( ((ConnectingFlightData)flightData).getFlights().get(0) ) ).getFlightNumber();
				} else {
					selectedFlightNumber =  ((DirectFlightData)flightData).getFlightNumber();
				}
				for(FlightData flight : ctx.availableFlights.getRoutes().get(index).getFlights() ){
					if(flight.getFlightType() == FlightTypeEnum.CONNECTING){
						currentFlightNumber = ((DirectFlightData)( ((ConnectingFlightData)flight).getFlights().get(0) ) ).getFlightNumber();
					} else {
						currentFlightNumber =  ((DirectFlightData)flight).getFlightNumber();
					}
					if (selectedFlightNumber.equals(currentFlightNumber)) {
						BrandData selectedBrandData = flightData.getBrands().get(0);
						for (BrandData brandData : flight.getBrands()) {
							if(brandData.getCode().equals(selectedBrandData.getCode()) ) {
								if (brandData.isBestFare()) {
									return true;
								}
							}
						}
					}
					
				}
				index++;	
			}
		}
		return false;
		
	}

	private void computeAndSpareFareAndTaxes(BookingSessionContext ctx) {
		
		//Obtaining Taxes
		BigDecimal totalFareAdult = new BigDecimal(0);
		BigDecimal totalFareChildren = new BigDecimal(0);
		BigDecimal totalFareInfant = new BigDecimal(0);
		BigDecimal totalTaxes = new BigDecimal(0);
		BigDecimal totalExtra = new BigDecimal(0);
		List<TaxData> selectionTaxes = ctx.selectionTaxes;
		if (selectionTaxes != null) {
			for(TaxData taxData : selectionTaxes){
		    	if(taxData.getCode().toLowerCase().contains("fare adult") 
		    			|| taxData.getCode().toLowerCase().contains("fare youth")){
		    		totalFareAdult = totalFareAdult.add(taxData.getAmount());
		    	}
		    	if(taxData.getCode().toLowerCase().contains("fare children")){
		    		totalFareChildren = totalFareChildren.add(taxData.getAmount());
		    	}
		    	if(taxData.getCode().toLowerCase().contains("fare infant")){
		    		totalFareInfant = totalFareInfant.add(taxData.getAmount());
		    	}
		    	if(taxData.getCode().toLowerCase().contains("taxtotal")){
		    		totalTaxes = totalTaxes.add(taxData.getAmount());
		    	}
		    	if(taxData.getCode().toLowerCase().contains("yqtotal")){
		    		totalExtra = totalExtra.add(taxData.getAmount());
		    	}
	
			}
		}
		
		//Obtaining Extracharge
		BigDecimal totalExtraCharge = ctx.totalExtraCharges;
		
		//Obtaining total price
		BigDecimal totalPrice = ctx.grossAmount;
		
		if (ctx.coupon != null && ctx.coupon.isValid() != null) {
			if (ctx.coupon.isValid()) {
				ecoupon = new GenericPriceRender(ctx.totalCouponPrice, ctx.currency, ctx.locale);
			}
		}
		
		if (totalExtraCharge!= null && totalPrice != null) {
			price = new GenericPriceRender(totalFareAdult, totalFareChildren, totalFareInfant, totalTaxes, totalExtra, totalExtraCharge, 
					totalPrice, ctx.currency, ctx.locale);
		}
		
	}
	
}