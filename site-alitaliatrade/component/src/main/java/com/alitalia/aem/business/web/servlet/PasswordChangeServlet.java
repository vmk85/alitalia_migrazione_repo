package com.alitalia.aem.business.web.servlet;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.statistics.utils.TradeStatisticUtils;
import com.alitalia.aem.business.web.utils.AlitaliaTradeAuthUtils;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.alitalia.aem.common.data.home.enumerations.TradeStatisticDataTypeEnum;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeRequest;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeResponse;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.common.messages.home.TradeRegisterStatisticRequest;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.alitalia.aem.web.component.tradestatistics.delegate.RegisterTradeStatisticsDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "passwordchange" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.")
})
@SuppressWarnings("serial")
public class PasswordChangeServlet extends GenericFormValidatorServlet {
	
	private ComponentContext componentContext;
	
	@Reference
	private BusinessLoginDelegate businessLoginDelegate;
	
	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile RegisterTradeStatisticsDelegate registerStatisticsDelegate; 
	
	@Reference
	private AlitaliaTradeConfigurationHolder configuration;
	
	private String errorLanguage;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	private I18n createI18n(SlingHttpServletRequest request) 
	{
		errorLanguage = LocalizationUtils.getLanguageStatic();
		Locale locale = new Locale(errorLanguage);
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		I18n i18n = new I18n(resourceBundle);
				
		return i18n;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) 
	{
		I18n i18n = createI18n(request);
		
		String emptyField = i18n.get("Enter") + " {0}";	
		String oldPassword = i18n.get("your_old_password");
		String newPassword = i18n.get("new_password");
		String passwordNotValid = i18n.get("ErrorFormatoPassword");
		String confirmPassword = i18n.get("Confirm_new_password");
		String passwordNotMatch = i18n.get("Passwords_do_not_match");
		
		oldPassword = oldPassword.replaceAll("_", " ");
		newPassword = newPassword.replaceAll("_", " ");
		confirmPassword = confirmPassword.replaceAll("_", " ");
		passwordNotMatch = passwordNotMatch.replaceAll("_", " ");
		
		Validator validator = new Validator();
		
		String oldPwd = request.getParameter("passwordOld");
		validator.addDirectParameterMessagePattern("passwordOld", oldPwd, emptyField, oldPassword, "isNotEmpty");
		
		String newPwd = request.getParameter("passwordNew");
		validator.addDirectParameterMessagePattern("passwordNew", newPwd, emptyField, newPassword, "isNotEmpty");
		validator.addDirectParameterMessagePattern("passwordNew", newPwd, passwordNotValid, newPassword, "isPassword");
		
		String repeatPwd = request.getParameter("passwordRepeat");
		validator.addDirectParameterMessagePattern("passwordRepeat", repeatPwd, "", confirmPassword, "isNotEmpty");
		validator.addDirectParameterMessagePattern("passwordRepeat", repeatPwd, passwordNotValid, confirmPassword, "isPassword");
		
		if (newPwd != null && !newPwd.isEmpty() && repeatPwd != null && !repeatPwd.isEmpty()) {
			validator.addCrossParameter("passwordRepeat", newPwd, repeatPwd, passwordNotMatch, "areEqual");
		}
		
		return validator.validate();
	}
	
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		
		String oldPwd = request.getParameter("passwordOld");
		String newPwd = request.getParameter("passwordNew");
		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		try {
			
			AgencyPasswordChangeRequest serviceRequest = new AgencyPasswordChangeRequest(IDFactory.getTid(), IDFactory.getSid(request));
			
			String codiceAgenzia = AlitaliaTradeAuthUtils.getProperty(request, "codiceAgenzia");
			AlitaliaTradeUserType ruolo = AlitaliaTradeAuthUtils.getRuolo(request);
			
			serviceRequest.setCodiceAgenzia(codiceAgenzia);
			serviceRequest.setRuolo(ruolo);
			serviceRequest.setOldPassword(oldPwd);
			serviceRequest.setNewPassword(newPwd);
			
			AgencyPasswordChangeResponse serviceResponse = businessLoginDelegate.changePassword(serviceRequest);
			
			if (serviceResponse == null) {
				logger.debug("Error during password change for user {} ({}).", codiceAgenzia, ruolo.toString());
				String errorPage = (String) componentContext.getProperties().get("failure.page");
				if (errorPage != null) {
					json.object();
					json.key("error").value(errorPage);
					json.endObject();
				} else {
					logger.error("Error page not found");
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
			} else if (serviceResponse.isOperationSuccessful()) {
				logger.debug("Password changed for user {} ({}).", codiceAgenzia, ruolo.toString());
				String successPage = (String) componentContext.getProperties().get("success.page");
				if (successPage != null) {
					TradeRegisterStatisticRequest tradeStatisticrequest = TradeStatisticUtils.createStatisticRequet(TradeStatisticDataTypeEnum.CHANGE_PSW, 
							request, true, null, null, configuration.getClientIpHeaders());
					
					try {
						RegisterStatisticsResponse statisticResponse = registerStatisticsDelegate.registerStatistics(tradeStatisticrequest);
						if (statisticResponse == null) {
							logger.error("Statistic Service Failed");
						}
					} catch(Exception e) {
						logger.error("Statistic Service Failed");
					}
					
					json.object();
					json.key("success").value(true);
					json.key("successPage").value(successPage);
					json.key("changed").value(true);
					json.endObject();				
				} else {
					logger.error("Success page not found");
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
			} else {
				json.object();
				json.key("success").value(true);
				json.key("changed").value(false);
				json.endObject();	
			}
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			String errorPage = (String) componentContext.getProperties().get("failure.page");
			if (errorPage != null) {
				try {
					json.object();
					json.key("error").value(errorPage);
					json.endObject();
				} catch (JSONException e1) {
					logger.error("Error creating JSON: ", e);
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
				
			}
		}
	}
}
