package com.alitalia.aem.business.web.statistics.utils;

import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.utils.AlitaliaTradeAuthUtils;
import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.alitalia.aem.common.data.home.enumerations.TradeStatisticDataTypeEnum;
import com.alitalia.aem.common.messages.home.TradeRegisterStatisticRequest;
import com.alitalia.aem.common.utils.IDFactory;

public class TradeStatisticUtils {
	
	protected static Logger logger = LoggerFactory.getLogger(TradeStatisticUtils.class);
	
	public static TradeRegisterStatisticRequest createStatisticRequet(TradeStatisticDataTypeEnum statisticDataType,
			SlingHttpServletRequest request, boolean success, String errorDescr, 
			String userID, String[] clientIPHeaders) throws ValueFormatException, RepositoryException{

		String clientIP = getClientIP(clientIPHeaders, request);
		String siteCode = "it";
		
		if (userID == null) {
			AlitaliaTradeUserType tipo = AlitaliaTradeAuthUtils.getRuolo(request);
			String ruolo = tipo.value().equals("0") ? "T" : "B";
			userID = ruolo + ":" + AlitaliaTradeAuthUtils.getProperty(request, "codiceAgenzia");
		}
		
		TradeRegisterStatisticRequest statisticRequest = 
				new TradeRegisterStatisticRequest(IDFactory.getTid(),IDFactory.getSid(request));
		
		statisticRequest.setUserId(userID);
		statisticRequest.setClientIP(clientIP);
		statisticRequest.setSessionId(request.getSession().getId());
		statisticRequest.setSiteCode(siteCode);
		statisticRequest.setSuccess(success);
		if (errorDescr!=null && !("").equals(errorDescr)) {
			statisticRequest.setErrorDescr(errorDescr);
		}
		statisticRequest.setType(statisticDataType);
		
		return statisticRequest;
	}
	
	
	public static String getClientIP(String[] clientIpHeaders, SlingHttpServletRequest request){
		String ipAddress = null;
		if (clientIpHeaders != null) {
			logger.debug("clientIpHeaders diverso da null: " + clientIpHeaders.toString());
			for (String headerName : clientIpHeaders) {
				String headerValue = request.getHeader(headerName);
				if (headerValue != null && headerValue.length() > 0) {
					logger.debug("Found client ip in header {}: {}", headerName, headerValue);
					logger.debug("headerValue: " + headerValue);
					if(headerValue.contains(",")){
						String[] ipValues = headerValue.split(",");
						if(ipValues.length > 1){
							headerValue = ipValues[ipValues.length-2];
							logger.debug("More than one ip: " + headerValue);
						} else{
							headerValue = ipValues[0];
							logger.debug("Only one ip: " + headerValue);
						}
					}
					ipAddress = headerValue.trim();
					break;
				}
			}
		}
		
		if (ipAddress == null) {
			logger.debug("Client ip header not found, fallback to request remote address: {}", 
					request.getRemoteAddr());
			ipAddress = request.getRemoteAddr();
		}
		return ipAddress;
	}
}
