package com.alitalia.aem.business.web.controllers;

import org.apache.sling.api.resource.*;
import org.slf4j.*;

import com.adobe.cq.sightly.WCMUse;

public class GetProperties extends WCMUse{

	private Logger logger = LoggerFactory.getLogger(GetProperties.class);
	private ValueMap properties;
	
	private ResourceResolverFactory resolverFactory;

	
	@Override
	public void activate() throws Exception {
		
		try {
			
			if(!get("imageUrl", String.class).equalsIgnoreCase("")){
				
				logger.debug("Image URL correct: {}",get("imageUrl", String.class));
				
				String imageUrl = get("imageUrl", String.class);
				ResourceResolver resourceResolver = getRequest().getResourceResolver();
				Resource res = resourceResolver.getResource(imageUrl);
				
				properties = res.adaptTo(ValueMap.class);
			
				logger.debug("Image URL properties: {}",properties);
			} else {
				logger.debug("Image URL error: {}",get("imageUrl", String.class));
				properties = null;
			}
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}
	}
	
	public ValueMap getProperties() {
		return properties;
	}
	
}
