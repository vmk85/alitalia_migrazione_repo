package com.alitalia.aem.business.web.servlet;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeRequest;
import com.alitalia.aem.common.messages.home.AgencyPasswordChangeResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "anonymouseditpassword" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })		
})
@SuppressWarnings("serial")
public class AnonymousEditPasswordServlet extends GenericFormValidatorServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	private final String BANCONISTA = "B";
	private final String TITOLARE = "T";
	
	@SuppressWarnings("unused")
	private ComponentContext componentContext;
	
	private String errorLanguage;
	
	@Reference
	private BusinessLoginDelegate businessLoginDelegate;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	private I18n createI18n(SlingHttpServletRequest request) 
	{
		errorLanguage = LocalizationUtils.getLanguageStatic();
		Locale locale = new Locale(errorLanguage);
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		I18n i18n = new I18n(resourceBundle);
				
		return i18n;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) 
	{
		I18n i18n = createI18n(request);
		
		String fieldNotValid = "{0} " + i18n.get("not_valid");
		String roleError = i18n.get("Retrieving_Role_Error");
		String emptyField = i18n.get("Enter") + " {0}";	
		String oldPassword = i18n.get("your_old_password");
		String newPassword = i18n.get("new_password");
		String passwordNotValid = i18n.get("ErrorFormatoPassword");
		String confirmPassword = i18n.get("Confirm_new_password");
		String passwordNotMatch = i18n.get("Passwords_do_not_match");
		
		fieldNotValid = fieldNotValid.replaceAll("_", " ");
		roleError = roleError.replaceAll("_", " ");
		oldPassword = oldPassword.replaceAll("_", " ");
		newPassword = newPassword.replaceAll("_", " ");
		confirmPassword = confirmPassword.replaceAll("_", " ");
		passwordNotMatch = passwordNotMatch.replaceAll("_", " ");
		
		Validator validator = new Validator();
		
		String user = request.getParameter("username");
		validator.addDirectParameterMessagePattern("username", user, fieldNotValid, "Username", "isUsername");
		
		String ruolo = request.getParameter("ruolo");
		String[] ruoli = {BANCONISTA, TITOLARE};
		validator.setAllowedValues("ruolo", ruolo, ruoli, roleError);
		
		String oldPwd = request.getParameter("passwordOld");
		validator.addDirectParameterMessagePattern("passwordOld", oldPwd, emptyField, oldPassword, "isNotEmpty");
		
		String newPwd = request.getParameter("passwordNew");
		validator.addDirectParameterMessagePattern("passwordNew", newPwd, emptyField, newPassword, "isNotEmpty");
		validator.addDirectParameterMessagePattern("passwordNew", newPwd, passwordNotValid, newPassword, "isPassword");
		
		String repeatPwd = request.getParameter("passwordRepeat");
		validator.addDirectParameterMessagePattern("passwordRepeat", repeatPwd, "{0}", confirmPassword, "isNotEmpty");
		validator.addDirectParameterMessagePattern("passwordRepeat", repeatPwd, passwordNotValid, confirmPassword, "isPassword");

		if (newPwd != null && !newPwd.isEmpty() && repeatPwd != null && !repeatPwd.isEmpty()) {
			validator.addCrossParameter("passwordRepeat", newPwd, repeatPwd, passwordNotMatch, "areEqual");
		}
		
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		String oldPwd = request.getParameter("passwordOld");
		String newPwd = request.getParameter("passwordNew");
		
		// 	QUERY STRING
		String codiceAgenzia =  request.getParameter("username");
		String ruolo =  request.getParameter("ruolo");
		
		boolean error = false;
		boolean success = false;
		
		try {
			AgencyPasswordChangeRequest serviceRequest = new AgencyPasswordChangeRequest(IDFactory.getTid(), IDFactory.getSid(request));
			
			serviceRequest.setCodiceAgenzia(codiceAgenzia);
			if (ruolo.equals(BANCONISTA)) {
				serviceRequest.setRuolo(AlitaliaTradeUserType.BANCONISTA);
			}
			if (ruolo.equals(TITOLARE)) {
				serviceRequest.setRuolo(AlitaliaTradeUserType.TITOLARE);
			}
		
			serviceRequest.setOldPassword(oldPwd);
			serviceRequest.setNewPassword(newPwd);
			
			AgencyPasswordChangeResponse serviceResponse = businessLoginDelegate.changePassword(serviceRequest);
			
			if (serviceResponse == null) {
				error = true;
			} else if (serviceResponse.isOperationSuccessful() != null && serviceResponse.isOperationSuccessful()) {
				logger.debug("Password changed for user {} ({}).", codiceAgenzia, ruolo.toString());
				success = true;
			} else {
				logger.debug("Error during password change for user {} ({}).", codiceAgenzia, ruolo.toString());
				success = false;
			}
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			error = true;
			success = false;
		}
		
		response.setContentType("application/json");

		TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
		
		try {
			out.object();
			if (!error) {
				out.key("result").value(true);
				if(success) {
					out.key("changed").value(true);
				} 
			} else {
				out.key("result").value(false);
			}
			out.endObject();

		} catch (Exception e) {
			logger.error("Error AnonymousEditPassword.", e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		
	}
	
}
