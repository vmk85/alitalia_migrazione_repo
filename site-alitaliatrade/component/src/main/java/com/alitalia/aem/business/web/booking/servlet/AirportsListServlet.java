package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.model.AirportInfo;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;

@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "airportslist" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }), 
	@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failure outcome.")})
@SuppressWarnings("serial")
public class AirportsListServlet extends SlingSafeMethodsServlet {

	// logger
	private static final Logger logger = LoggerFactory.getLogger(AirportsListServlet.class);

	private ComponentContext componentContext;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	// Search Flight Delegate
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegate staticDataDelegate;

	@Override
	protected void doGet(SlingHttpServletRequest request, 
			SlingHttpServletResponse response) throws ServletException, IOException {

		// content type
		response.setContentType("application/json");

		// JSON response
		TidyJSONWriter o = new TidyJSONWriter(response.getWriter());
		try {

			// call service
			RetrieveAirportsRequest req = new RetrieveAirportsRequest(IDFactory.getTid(),
					IDFactory.getSid(request));
			req.setMarket("IT");
			req.setLanguageCode("IT");
			RetrieveAirportsResponse res = staticDataDelegate.retrieveAirports(req);

			// out JSON response
			o.object();

			o.key("airports");
			o.array();

			ResourceBundle resourceBundle = 
					request.getResourceBundle(getLocale(request.getResource()));
			final I18n i18n = new I18n(resourceBundle);

			List<AirportData> list = res.getAirports();
			List<AirportInfo> airportList = new ArrayList<AirportInfo>();
			for (AirportData airportData : list) {
				
				String ac = airportData.getCode();
				String cc = airportData.getCityCode();
				String sc = airportData.getCountryCode();
				
				String city_key = "airportsData." + cc + ".city";
				String cityName = i18n.get(city_key);
				
				String country_key = "airportsData." + sc + ".country";
				String country = i18n.get(country_key);
				
				String name_key = "airportsData." + ac + ".name";
				String airportName = i18n.get(name_key);
				
				// add to list only if translation is provided
				if (!cityName.equals(city_key) && !country.equals(country_key) &&
						!airportName.equals(name_key)) {
					AirportInfo airport = new AirportInfo(airportData.getCode(),
							airportName,
							airportData.getCityCode(),
							cityName,
							country);
					airportList.add(airport);
				}
			}
			Collections.sort(airportList, AirportInfo.COMPARE_BY_CITY_AND_AIRPORT);
			
			
			for (AirportInfo airportInfo : airportList) {
				o.object();
				o.key("value").value(airportInfo.getCityName() + ", " 
						+ airportInfo.getAirportName() 
						+ ", " + airportInfo.getCountry());
				o.key("city").value(airportInfo.getCityName());
				o.key("country").value(airportInfo.getCountry());
				o.key("airport").value(airportInfo.getAirportName());
				o.key("code").value(airportInfo.getCode());
				o.endObject();
			}
			

			o.endArray();
			o.key("result").value("OK");
			o.endObject();

		} catch (Exception e) {
			logger.error("Unexpected error", e);
			try {
				String failurePage = (String) componentContext.getProperties().get("failure.page");
				o.object();
				o.key("result").value(false);
				o.key("redirect").value(failurePage);
				o.endObject();
			} catch (JSONException e1) {
				logger.error("Error Creating JSON", e1);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
	}

	private Locale getLocale(Resource resource) {
		boolean flag = true;
		Resource currentResource = resource;
		Locale locale = null;

		while (flag) {
			if (currentResource.getResourceType().equalsIgnoreCase("cq:Page")) {
				Page p = currentResource.adaptTo(Page.class);
				locale = p.getLanguage(false);
				flag = false;
			} else {
				currentResource = currentResource.getParent();
				flag = !(currentResource == null);
			}
		}

		if (locale == null) {
			locale = new Locale("en");
		}

		return locale;
	}

}
