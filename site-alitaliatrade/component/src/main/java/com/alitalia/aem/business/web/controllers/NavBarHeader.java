package com.alitalia.aem.business.web.controllers;

import java.util.Map;

import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.utils.MultifieldUtils;

public class NavBarHeader extends WCMUse {
	
	@SuppressWarnings("rawtypes")
	private Map menuArray;
	
	private Logger logger = LoggerFactory.getLogger(NavBarHeader.class);

	
	@Override
    public void activate() throws Exception {
		try{
			menuArray = MultifieldUtils.fromJcrMultifieldToMapArrayObject(getResource(), "menus");
			for (int i = 0; i < menuArray.size(); i++) {
				@SuppressWarnings("unchecked")
				Map<String,Object> field = (Map<String, Object>) menuArray.get(Integer.toString(i));
				Boolean checkLinkGruppi = Boolean.parseBoolean((String)field.get("checkLinkGruppi"));
				if (checkLinkGruppi != null) {
					if (checkLinkGruppi) {
						SlingScriptHelper helper = getSlingScriptHelper();
						AlitaliaTradeConfigurationHolder configuration = helper.getService(AlitaliaTradeConfigurationHolder.class);
						String linkGruppi = configuration.getLinkGruppiPage();
						field.put("pathmenu", linkGruppi);
						break;
					}
				}
				
			}
		}catch(Exception e){
			logger.error("Unexpected error: ", e);
			throw e;
		}
		
	}
	
	@SuppressWarnings("rawtypes")
	public Map getMenuArray(){
		return menuArray;
	}
}
