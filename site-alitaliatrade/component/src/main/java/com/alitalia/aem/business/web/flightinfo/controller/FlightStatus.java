package com.alitalia.aem.business.web.flightinfo.controller;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import com.adobe.cq.sightly.WCMUse;

public class FlightStatus extends WCMUse {

	private Map<String, String> dates;

	public Map<String, String> getDates() {
		return dates;
	}

	@Override
	public void activate() throws Exception {
		dates = new LinkedHashMap<String, String>();
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMMM yyyy", Locale.ITALIAN);
		SimpleDateFormat sdfkey = new SimpleDateFormat("dd/MM/yy");

		Calendar cal = Calendar.getInstance(Locale.ITALIAN);
		cal.setTime(new Date());
		cal.add(Calendar.DATE, -2);

		for (int i = 0; i < 10; i++) {
			dates.put(sdfkey.format(cal.getTime()),
					sdf.format(cal.getTime()));
			cal.add(Calendar.DATE, 1);
		}

	}

}
