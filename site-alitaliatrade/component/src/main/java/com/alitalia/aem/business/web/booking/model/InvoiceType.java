package com.alitalia.aem.business.web.booking.model;

public enum InvoiceType {
	PF("PF"),
	PG("PG");
	private final String value;

	private InvoiceType(String v) {
        value = v;
    }
	
	public String value() {
		return value;
	}

	public static InvoiceType fromValue(String v) {
		for (InvoiceType c : InvoiceType.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
