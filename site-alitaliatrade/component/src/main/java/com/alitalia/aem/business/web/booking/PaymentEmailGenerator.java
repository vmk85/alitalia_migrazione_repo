package com.alitalia.aem.business.web.booking;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.jcr.Node;
import javax.jcr.Session;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.common.data.home.*;

import org.apache.commons.io.IOUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.render.DirectFlightDataRender;
import com.alitalia.aem.business.web.booking.render.GenericPriceRender;
import com.alitalia.aem.business.web.booking.render.PassengerRender;
import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.utils.FlightDataUtils;
import com.day.cq.i18n.I18n;

/**
 * 
 * @author R.Capitini
 *
 * Helper class used to create email message containg payment
 * receipt and booking summary. Email message is in form of
 * XSLT since it is sent by BE Alitalia service during payment
 * orchestration.
 *
 */
@Component(immediate = true)
@Service(value=PaymentEmailGenerator.class)
public class PaymentEmailGenerator {

	@Property(description = "Folder containing email template files")
	private static final String EMAIL_FOLDER = "email.folder";
	private String emailFragmentFolder = "";

	@Property(description = "XSL stilesheet wrapper")
	private static final String EMAIL_STILESHEET = "email.stilesheet";
	private String emailStilesheetFragmentName = "";

	@Property(description = "Header for purchase conrimation email")
	private static final String EMAIL_HEADER = "email.header";
	private String emailHeaderFragmentName = "";

	@Property(description = "Ticket detail section")
	private static final String TICKET_DETAIL = "email.ticket.detail";
	private String ticketDetailFragmentName = "";

	@Property(description = "Passenger row section")
	private static final String PASSENGER_ROW = "email.passenger.row";
	private String passengerRowFragmentName = "";

	@Property(description = "Passenger detail section")
	private static final String PASSENGER_DETAIL = "email.passenger.detail";
	private String passengerDetailFragmentName = "";

	@Property(description = "Passenger row section end")
	private static final String PASSENGER_ROWEND = "email.passenger.rowend";
	private String passengerRowendFragmentName = "";

	@Property(description = "Ticket detail section end")
	private static final String TICKET_DETAILEND = "email.ticket.detailend";
	private String ticketDetailendFragmentName = "";

	@Property(description = "Itinerary summary section")
	private static final String ITINERARY_SUMMARY = "email.itinerary.summary";
	private String itinerarySummaryFragmentName = "";

	@Property(description = "Itinerary type section")
	private static final String ITINERARY_TYPE = "email.itinerary.type";
	private String itineraryTypeFragmentName = "";

	@Property(description = "Flight detail section")
	private static final String FLIGHT_DETAIL = "email.flight.detail";
	private String flightDetailFragmentName = "";

	@Property(description = "Passenger seat row section")
	private static final String PASSENGER_SEATROW = "email.passenger.seatrow";
	private String passengerSeatrowFragmentName = "";

	@Property(description = "Passenger seat section")
	private static final String PASSENGER_SEAT = "email.passenger.seat";
	private String passengerSeatFragmentName = "";

	@Property(description = "Passenger seat row section end")
	private static final String PASSENGER_SEATROWEND = "email.passenger.seatrowend";
	private String passengerSeatrowendFragmentName = "";

	@Property(description = "Flight detail section end")
	private static final String FLIGHT_DETAILEND = "email.flight.detailend";
	private String flightDetailendFragmentName = "";

	@Property(description = "Flight transit section")
	private static final String FLIGHT_TRANSIT = "email.flight.transit";
	private String flightTransitFragmentName = "";

	@Property(description = "Flight seat description section")
	private static final String FLIGHT_SEATDESCR = "email.flight.seatdescr";
	private String flightSeatdescrFragmentName = "";

	@Property(description = "A/R divider section")
	private static final String FLIGHT_AR_DIVIDER = "email.flight.ardivider";
	private String flightArdividerFragmentName = "";

	@Property(description = "Itinerary summary section end")
	private static final String ITINERARY_SUMMARYEND = "email.itinerary.summaryend";
	private String itinerarySummaryendFragmentName = "";

	@Property(description = "Costs summary section")
	private static final String COSTS_SUMMARY = "email.costs.summary";
	private String costsSummaryFragmentName = "";
	
	@Property(description = "Invoice section")
	private static final String INVOICE = "email.invoice";
	private String invoiceFragmentName = "";

	@Property(description = "Dynamic message communication for APIS procedure")
	private static final String EMAIL_MESSAGE_APIS = "email.message.apis";
	private String emailMessageApisFragmentName = "";

	@Property(description = "Dynamic message communication for ESTA procedure")
	private static final String EMAIL_MESSAGE_ESTA = "email.message.esta";
	private String emailMessageEstaFragmentName = "";

	@Property(description = "Trailer for purchase confirmation email")
	private static final String EMAIL_TRAILER = "email.trailer";
	private String emailTrailerFragmentName = "";

	@Property
	private static final String PLACEHOLDER_MAIL_BODY = "email.placeholder.message_body";
	private String placeholderMailBody = "";

	@Property
	private static final String PLACEHOLDER_CODICE_PRENOTAZIONE = "email.placeholder.codice_prenotazione";
	private String placeholderCodicePrenotazione = "";

	@Property
	private static final String PLACEHOLDER_NUM_PASSEGGERO = "email.placeholder.num_passeggero";
	private String placeholderNumPasseggero = "";

	@Property
	private static final String PLACEHOLDER_NOME_PASSEGGERO = "email.placeholder.nome_passeggero";
	private String placeholderNomePasseggero = "";
	
	@Property
	private static final String PLACEHOLDER_SECONDO_NOME_PASSEGGERO = "email.placeholder.secondo_nome_passeggero";
	private String placeholderSecondoNomePasseggero = "";

	@Property
	private static final String PLACEHOLDER_COGNOME_PASSEGGERO = "email.placeholder.cognome_passeggero";
	private String placeholderCognomePasseggero = "";
	
	@Property
	private static final String PLACEHOLDER_TICKET_NUM_PASSEGGERO = "email.placeholder.ticket_num_passeggero";
	private String placeholderTicketNumPasseggero = "";

	@Property
	private static final String PLACEHOLDER_TIPO_ITINERARIO = "email.placeholder.tipo_itinerario";
	private String placeholderTipoItinerario = "";

	@Property
	private static final String PLACEHOLDER_NUM_SCALI = "email.placeholder.num_scali";
	private String placeholderNumScali = "";

	@Property
	private static final String PLACEHOLDER_NUM_VOLO = "email.placeholder.num_volo";
	private String placeholderNumVolo = "";

	@Property
	private static final String PLACEHOLDER_DATA_VOLO = "email.placeholder.data_volo";
	private String placeholderDataVolo = "";

	@Property
	private static final String PLACEHOLDER_ORA_PARTENZA = "email.placeholder.ora_partenza";
	private String placeholderOraPartenza = "";

	@Property
	private static final String PLACEHOLDER_CODICE_AEREOPORTO_PARTENZA = "email.placeholder.codice_aereoporto_partenza";
	private String placeholderCodiceAereoportoPartenza = "";

	@Property
	private static final String PLACEHOLDER_CITTA_PARTENZA = "email.placeholder.citta_partenza";
	private String placeholderCittaPartenza = "";

	@Property
	private static final String PLACEHOLDER_ORA_ARRIVO = "email.placeholder.ora_arrivo";
	private String placeholderOraArrivo = "";

	@Property
	private static final String PLACEHOLDER_CODICE_AEREOPORTO_ARRIVO = "email.placeholder.codice_aereoporto_arrivo";
	private String placeholderCodiceAereoportoArrivo = "";

	@Property
	private static final String PLACEHOLDER_CITTA_ARRIVO = "email.placeholder.citta_arrivo";
	private String placeholderCittaArrivo = "";

	@Property
	private static final String PLACEHOLDER_POSTO_NUM_PASSEGGERO = "email.placeholder.posto_num_passeggero";
	private String placeholderPostoNumPasseggero = "";

	@Property
	private static final String PLACEHOLDER_NUM_POSTO = "email.placeholder.num_posto";
	private String placeholderNumPosto = "";

	@Property
	private static final String PLACEHOLDER_DURATA_TRANSITO = "email.placeholder.durata_transito";
	private String placeholderDurataTransito = "";

	@Property
	private static final String PLACEHOLDER_TASSE = "email.placeholder.tasse";
	private String placeholderTasse = "";

	@Property
	private static final String PLACEHOLDER_SUPPLEMENTI = "email.placeholder.supplementi";
	private String placeholderSupplementi = "";

	@Property
	private static final String PLACEHOLDER_TOTALE = "email.placeholder.totale";
	private String placeholderTotale = "";

	@Property
	private static final String PLACEHOLDER_FORMA_PAGAMENTO = "email.placeholder.forma_pagamento";
	private String placeholderFormaPagamento = "";
	
	@Property
	private static final String PLACEHOLDER_RICHIESTA_FATTURA = "email.placeholder.richiesta_fattura";
	private String placeholderRichiestaFattura = "";
	
	@Property(description = "Email subject")
	private static final String EMAIL_SUBJECT = "email.subject";
	private String emailSubject = "";

	@Property(description = "Server name to be replaced in images links")
	private static final String EMAIL_IMAGES_SERVERNAME = "email.images.servername";
	private String emailImagesServername = "";

	@Property
	private static final String PLACEHOLDER_IMAGES_SERVER = "email.images.placeholder.server";
	private String placeholderImagesServer = "";

	@Property(description = "Server name to be replaced in images links")
	private static final String EMAIL_IMAGES_FOLDER = "email.images.folder";
	private String emailImagesFolder = "";

	@Property
	private static final String PLACEHOLDER_IMAGES_FOLDER = "email.images.placeholder.folder";
	private String placeholderImagesFolder = "";
	
	@Property
	private static final String PLACEHOLDER_TIMONE = "email.placeholder.timone";
	private String placeholderTimone = "";
	
	@Property
	private static final String TIMONE = "email.timone";
	private String emailTimoneFragmentName = "";
	
	@Property
	private static final String CARRIER = "email.carrier";
	private String emailCarrierFragmentName = "";
	
	@Property
	private static final String PLACEHOLDER_CARRIER = "email.placeholder.carrier";
	private String placeholderCarrier = "";
	
	@Property
	private static final String PLACEHOLDER_CARRIER_VALUE = "email.placeholder.carrier.value";
	private String placeholderCarrierValue = "";
	
	@Property
	private static final String PLACEHOLDER_FARE = "email.placeholder.fare";
	private String placeholderFare = "";
	
	@Property
	private static final String PLACEHOLDER_FARE_VALUE = "email.placeholder.fare.value";
	private String placeholderFareValue = "";
	
	@Property
	private static final String PLACEHOLDER_ECOUPON = "email.placeholder.ecoupon";
	private String placeholderEcoupon = "";
	
	@Property
	private static final String PLACEHOLDER_ECOUPON_VALUE = "email.placeholder.ecoupon.value";
	private String placeholderEcouponValue = "";
	
	@Property
	private static final String PLACEHOLDER_EXTRACHARGE = "email.placeholder.extracharge";
	private String placeholderExtracharge = "";
	
	@Property
	private static final String PLACEHOLDER_EXTRACHARGE_VALUE = "email.placeholder.extracharge.value";
	private String placeholderExtrachargeValue = "";
		
	@Property
	private static final String LABEL_FARE = "email.label.fare";
	private String labelFare = "";
	
	@Property
	private static final String LABEL_FARE_CHILD = "email.label.fare.child";
	private String labelFareChild = "";
	
	@Property
	private static final String LABEL_FARE_INFANT = "email.label.fare.infant";
	private String labelFareInfant = "";
	
	@Property
	private static final String LABEL_ECOUPON = "email.label.ecoupon";
	private String labelEcoupon = "";
	
	@Property
	private static final String LABEL_EXTRACHARGE = "email.label.extracharge";
	private String labelExtracharge = "";
	
	@Property
	private static final String PLACEHOLDER_PNR = "email.placeholder.pnr";
	private String placeholderPnr = "";
	
	@Property
	private static final String PLACEHOLDER_APPLICANT_SURNAME = "email.placeholder.applicant_surname";
	private String placeholderLastname = "";
	
	@Property
	private static final String PLACEHOLDER_DEEPLINK_MTO = "email.placeholder.deeplink_mto";
	private String placeholderMTODeepLink = "";
	
	@Property
	private static final String PLACEHOLDER_LANGUAGE = "email.placeholder.language";
	private String placeholderLanguage = "";
	
	@Property
	private static final String DEEPLINK_MTO_VALUE = "email.deeplink_mto";
	private String mtoDeepLink = "";
	

	private static final Logger logger = LoggerFactory.getLogger(PaymentEmailGenerator.class);

	@Reference
	private ResourceResolverFactory resolverFactory;
	
	@Activate
	protected void activate(final Map<String, Object> config) throws Exception {

		String jcrResourceReference = "";
		
		this.emailFragmentFolder = String.valueOf(config.get(EMAIL_FOLDER));

		if (this.emailFragmentFolder != null && !"".equals(this.emailFragmentFolder)) {
			jcrResourceReference = String.valueOf(config.get(EMAIL_STILESHEET));
			this.emailStilesheetFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(EMAIL_HEADER));
			this.emailHeaderFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(TICKET_DETAIL));
			this.ticketDetailFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(PASSENGER_ROW));
			this.passengerRowFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(PASSENGER_DETAIL));
			this.passengerDetailFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(PASSENGER_ROWEND));
			this.passengerRowendFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(TICKET_DETAILEND));
			this.ticketDetailendFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(ITINERARY_SUMMARY));
			this.itinerarySummaryFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(ITINERARY_TYPE));
			this.itineraryTypeFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(FLIGHT_DETAIL));
			this.flightDetailFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(PASSENGER_SEATROW));
			this.passengerSeatrowFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(PASSENGER_SEAT));
			this.passengerSeatFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(PASSENGER_SEATROWEND));
			this.passengerSeatrowendFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(FLIGHT_DETAILEND));
			this.flightDetailendFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(FLIGHT_TRANSIT));
			this.flightTransitFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(FLIGHT_SEATDESCR));
			this.flightSeatdescrFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(FLIGHT_AR_DIVIDER));
			this.flightArdividerFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(ITINERARY_SUMMARYEND));
			this.itinerarySummaryendFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(COSTS_SUMMARY));
			this.costsSummaryFragmentName = this.emailFragmentFolder + jcrResourceReference;
			
			jcrResourceReference = String.valueOf(config.get(INVOICE));
			this.invoiceFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(EMAIL_MESSAGE_APIS));
			this.emailMessageApisFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(EMAIL_MESSAGE_ESTA));
			this.emailMessageEstaFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(EMAIL_TRAILER));
			this.emailTrailerFragmentName = this.emailFragmentFolder + jcrResourceReference;
			
			jcrResourceReference = String.valueOf(config.get(TIMONE));
			this.emailTimoneFragmentName = this.emailFragmentFolder + jcrResourceReference;
			
			jcrResourceReference = String.valueOf(config.get(CARRIER));
			this.emailCarrierFragmentName = this.emailFragmentFolder + jcrResourceReference;
			
		} else {
			throw new Exception("Undefined folder parameter for email configuration");
		}

		this.placeholderMailBody = String.valueOf(config.get(PLACEHOLDER_MAIL_BODY));
		this.placeholderCodicePrenotazione = String.valueOf(config.get(PLACEHOLDER_CODICE_PRENOTAZIONE));
		this.placeholderNumPasseggero = String.valueOf(config.get(PLACEHOLDER_NUM_PASSEGGERO));
		this.placeholderNomePasseggero = String.valueOf(config.get(PLACEHOLDER_NOME_PASSEGGERO));
		this.placeholderSecondoNomePasseggero = String.valueOf(config.get(PLACEHOLDER_SECONDO_NOME_PASSEGGERO));
		this.placeholderCognomePasseggero = String.valueOf(config.get(PLACEHOLDER_COGNOME_PASSEGGERO));
		this.placeholderTicketNumPasseggero = String.valueOf(config.get(PLACEHOLDER_TICKET_NUM_PASSEGGERO));
		this.placeholderTipoItinerario = String.valueOf(config.get(PLACEHOLDER_TIPO_ITINERARIO));
		this.placeholderNumScali = String.valueOf(config.get(PLACEHOLDER_NUM_SCALI));
		this.placeholderNumVolo = String.valueOf(config.get(PLACEHOLDER_NUM_VOLO));
		this.placeholderDataVolo = String.valueOf(config.get(PLACEHOLDER_DATA_VOLO));
		this.placeholderOraPartenza = String.valueOf(config.get(PLACEHOLDER_ORA_PARTENZA));
		this.placeholderCodiceAereoportoPartenza = String.valueOf(config.get(PLACEHOLDER_CODICE_AEREOPORTO_PARTENZA));
		this.placeholderCittaPartenza = String.valueOf(config.get(PLACEHOLDER_CITTA_PARTENZA));
		this.placeholderOraArrivo = String.valueOf(config.get(PLACEHOLDER_ORA_ARRIVO));
		this.placeholderCodiceAereoportoArrivo = String.valueOf(config.get(PLACEHOLDER_CODICE_AEREOPORTO_ARRIVO));
		this.placeholderCittaArrivo = String.valueOf(config.get(PLACEHOLDER_CITTA_ARRIVO));
		this.placeholderPostoNumPasseggero = String.valueOf(config.get(PLACEHOLDER_POSTO_NUM_PASSEGGERO));
		this.placeholderNumPosto = String.valueOf(config.get(PLACEHOLDER_NUM_POSTO));
		this.placeholderDurataTransito = String.valueOf(config.get(PLACEHOLDER_DURATA_TRANSITO));
		this.placeholderTasse = String.valueOf(config.get(PLACEHOLDER_TASSE));
		this.placeholderSupplementi = String.valueOf(config.get(PLACEHOLDER_SUPPLEMENTI));
		this.placeholderTotale = String.valueOf(config.get(PLACEHOLDER_TOTALE));
		this.placeholderFormaPagamento = String.valueOf(config.get(PLACEHOLDER_FORMA_PAGAMENTO));
		this.placeholderRichiestaFattura = String.valueOf(config.get(PLACEHOLDER_RICHIESTA_FATTURA));
		this.emailSubject = String.valueOf(config.get(EMAIL_SUBJECT));

		this.placeholderImagesServer = String.valueOf(config.get(PLACEHOLDER_IMAGES_SERVER));
		this.emailImagesServername = String.valueOf(config.get(EMAIL_IMAGES_SERVERNAME));
		this.placeholderImagesFolder = String.valueOf(config.get(PLACEHOLDER_IMAGES_FOLDER));
		this.emailImagesFolder = String.valueOf(config.get(EMAIL_IMAGES_FOLDER));
		this.placeholderTimone = String.valueOf(config.get(PLACEHOLDER_TIMONE));
		this.placeholderCarrier = String.valueOf(config.get(PLACEHOLDER_CARRIER));
		this.placeholderCarrierValue = String.valueOf(config.get(PLACEHOLDER_CARRIER_VALUE));
		this.placeholderFare = String.valueOf(config.get(PLACEHOLDER_FARE));
		this.placeholderFareValue = String.valueOf(config.get(PLACEHOLDER_FARE_VALUE));
		this.placeholderEcoupon = String.valueOf(config.get(PLACEHOLDER_ECOUPON));
		this.placeholderEcouponValue = String.valueOf(config.get(PLACEHOLDER_ECOUPON_VALUE));
		this.placeholderExtracharge = String.valueOf(config.get(PLACEHOLDER_EXTRACHARGE));
		this.placeholderExtrachargeValue = String.valueOf(config.get(PLACEHOLDER_EXTRACHARGE_VALUE));
		this.labelFare = String.valueOf(config.get(LABEL_FARE));
		this.labelFareChild = String.valueOf(config.get(LABEL_FARE_CHILD));
		this.labelFareInfant = String.valueOf(config.get(LABEL_FARE_INFANT));
		this.labelEcoupon = String.valueOf(config.get(LABEL_ECOUPON));
		this.labelExtracharge = String.valueOf(config.get(LABEL_EXTRACHARGE));
		this.placeholderPnr = String.valueOf(config.get(PLACEHOLDER_PNR));
		this.placeholderLastname = String.valueOf(config.get(PLACEHOLDER_APPLICANT_SURNAME));
		this.placeholderMTODeepLink = String.valueOf(config.get(PLACEHOLDER_DEEPLINK_MTO));
		this.placeholderLanguage = String.valueOf(config.get(PLACEHOLDER_LANGUAGE));
		this.mtoDeepLink = String.valueOf(config.get(DEEPLINK_MTO_VALUE));
	}

	@Modified
	protected void modified(final Map<String, Object> config) throws Exception {

		String jcrResourceReference = "";
		
		this.emailFragmentFolder = String.valueOf(config.get(EMAIL_FOLDER));

		if (this.emailFragmentFolder != null && !"".equals(this.emailFragmentFolder)) {
			jcrResourceReference = String.valueOf(config.get(EMAIL_STILESHEET));
			this.emailStilesheetFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(EMAIL_HEADER));
			this.emailHeaderFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(TICKET_DETAIL));
			this.ticketDetailFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(PASSENGER_ROW));
			this.passengerRowFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(PASSENGER_DETAIL));
			this.passengerDetailFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(PASSENGER_ROWEND));
			this.passengerRowendFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(TICKET_DETAILEND));
			this.ticketDetailendFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(ITINERARY_SUMMARY));
			this.itinerarySummaryFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(ITINERARY_TYPE));
			this.itineraryTypeFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(FLIGHT_DETAIL));
			this.flightDetailFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(PASSENGER_SEATROW));
			this.passengerSeatrowFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(PASSENGER_SEAT));
			this.passengerSeatFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(PASSENGER_SEATROWEND));
			this.passengerSeatrowendFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(FLIGHT_DETAILEND));
			this.flightDetailendFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(FLIGHT_TRANSIT));
			this.flightTransitFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(FLIGHT_SEATDESCR));
			this.flightSeatdescrFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(FLIGHT_AR_DIVIDER));
			this.flightArdividerFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(ITINERARY_SUMMARYEND));
			this.itinerarySummaryendFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(COSTS_SUMMARY));
			this.costsSummaryFragmentName = this.emailFragmentFolder + jcrResourceReference;
			
			jcrResourceReference = String.valueOf(config.get(INVOICE));
			this.invoiceFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(EMAIL_MESSAGE_APIS));
			this.emailMessageApisFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(EMAIL_MESSAGE_ESTA));
			this.emailMessageEstaFragmentName = this.emailFragmentFolder + jcrResourceReference;

			jcrResourceReference = String.valueOf(config.get(EMAIL_TRAILER));
			this.emailTrailerFragmentName = this.emailFragmentFolder + jcrResourceReference;
			
			jcrResourceReference = String.valueOf(config.get(TIMONE));
			this.emailTimoneFragmentName = this.emailFragmentFolder + jcrResourceReference;
			
			jcrResourceReference = String.valueOf(config.get(CARRIER));
			this.emailCarrierFragmentName = this.emailFragmentFolder + jcrResourceReference;
		} else {
			throw new Exception("Undefined folder parameter for email configuration");
		}

		this.placeholderMailBody = String.valueOf(config.get(PLACEHOLDER_MAIL_BODY));
		this.placeholderCodicePrenotazione = String.valueOf(config.get(PLACEHOLDER_CODICE_PRENOTAZIONE));
		this.placeholderNumPasseggero = String.valueOf(config.get(PLACEHOLDER_NUM_PASSEGGERO));
		this.placeholderNomePasseggero = String.valueOf(config.get(PLACEHOLDER_NOME_PASSEGGERO));
		this.placeholderCognomePasseggero = String.valueOf(config.get(PLACEHOLDER_COGNOME_PASSEGGERO));
		this.placeholderTicketNumPasseggero = String.valueOf(config.get(PLACEHOLDER_TICKET_NUM_PASSEGGERO));
		this.placeholderTipoItinerario = String.valueOf(config.get(PLACEHOLDER_TIPO_ITINERARIO));
		this.placeholderNumScali = String.valueOf(config.get(PLACEHOLDER_NUM_SCALI));
		this.placeholderNumVolo = String.valueOf(config.get(PLACEHOLDER_NUM_VOLO));
		this.placeholderDataVolo = String.valueOf(config.get(PLACEHOLDER_DATA_VOLO));
		this.placeholderOraPartenza = String.valueOf(config.get(PLACEHOLDER_ORA_PARTENZA));
		this.placeholderCodiceAereoportoPartenza = String.valueOf(config.get(PLACEHOLDER_CODICE_AEREOPORTO_PARTENZA));
		this.placeholderCittaPartenza = String.valueOf(config.get(PLACEHOLDER_CITTA_PARTENZA));
		this.placeholderOraArrivo = String.valueOf(config.get(PLACEHOLDER_ORA_ARRIVO));
		this.placeholderCodiceAereoportoArrivo = String.valueOf(config.get(PLACEHOLDER_CODICE_AEREOPORTO_ARRIVO));
		this.placeholderCittaArrivo = String.valueOf(config.get(PLACEHOLDER_CITTA_ARRIVO));
		this.placeholderPostoNumPasseggero = String.valueOf(config.get(PLACEHOLDER_POSTO_NUM_PASSEGGERO));
		this.placeholderNumPosto = String.valueOf(config.get(PLACEHOLDER_NUM_POSTO));
		this.placeholderDurataTransito = String.valueOf(config.get(PLACEHOLDER_DURATA_TRANSITO));
		this.placeholderTasse = String.valueOf(config.get(PLACEHOLDER_TASSE));
		this.placeholderSupplementi = String.valueOf(config.get(PLACEHOLDER_SUPPLEMENTI));
		this.placeholderTotale = String.valueOf(config.get(PLACEHOLDER_TOTALE));
		this.placeholderFormaPagamento = String.valueOf(config.get(PLACEHOLDER_FORMA_PAGAMENTO));
		this.placeholderRichiestaFattura = String.valueOf(config.get(PLACEHOLDER_RICHIESTA_FATTURA));
		this.emailSubject = String.valueOf(config.get(EMAIL_SUBJECT));

		this.placeholderImagesServer = String.valueOf(config.get(PLACEHOLDER_IMAGES_SERVER));
		this.emailImagesServername = String.valueOf(config.get(EMAIL_IMAGES_SERVERNAME));
		this.placeholderImagesFolder = String.valueOf(config.get(PLACEHOLDER_IMAGES_FOLDER));
		this.emailImagesFolder = String.valueOf(config.get(EMAIL_IMAGES_FOLDER));
		this.placeholderTimone = String.valueOf(config.get(PLACEHOLDER_TIMONE));
		this.placeholderCarrier = String.valueOf(config.get(PLACEHOLDER_CARRIER));
		this.placeholderCarrierValue = String.valueOf(config.get(PLACEHOLDER_CARRIER_VALUE));
		this.placeholderFare = String.valueOf(config.get(PLACEHOLDER_FARE));
		this.placeholderFareValue = String.valueOf(config.get(PLACEHOLDER_FARE_VALUE));
		this.placeholderEcoupon = String.valueOf(config.get(PLACEHOLDER_ECOUPON));
		this.placeholderEcouponValue = String.valueOf(config.get(PLACEHOLDER_ECOUPON_VALUE));
		this.placeholderExtracharge = String.valueOf(config.get(PLACEHOLDER_EXTRACHARGE));
		this.placeholderExtrachargeValue = String.valueOf(config.get(PLACEHOLDER_EXTRACHARGE_VALUE));
		this.labelFare = String.valueOf(config.get(LABEL_FARE));
		this.labelFareChild = String.valueOf(config.get(LABEL_FARE_CHILD));
		this.labelFareInfant = String.valueOf(config.get(LABEL_FARE_INFANT));
		this.labelEcoupon = String.valueOf(config.get(LABEL_ECOUPON));
		this.labelExtracharge = String.valueOf(config.get(LABEL_EXTRACHARGE));
		this.placeholderPnr = String.valueOf(config.get(PLACEHOLDER_PNR));
		this.placeholderLastname = String.valueOf(config.get(PLACEHOLDER_APPLICANT_SURNAME));
		this.placeholderMTODeepLink = String.valueOf(config.get(PLACEHOLDER_DEEPLINK_MTO));
		this.placeholderLanguage = String.valueOf(config.get(PLACEHOLDER_LANGUAGE));
		this.mtoDeepLink = String.valueOf(config.get(DEEPLINK_MTO_VALUE));
	}

	public String prepareXsltMail(BookingSessionContext ctx, PaymentData paymentData, I18n i18n, AlitaliaTradeConfigurationHolder configuration) {

		String emailStilesheetFragment = retrieveContentFromCRXRepository(this.emailStilesheetFragmentName);
		String emailHeaderFragment = retrieveContentFromCRXRepository(this.emailHeaderFragmentName);
		String ticketDetailFragment = retrieveContentFromCRXRepository(this.ticketDetailFragmentName);
		String passengerRowFragment = retrieveContentFromCRXRepository(this.passengerRowFragmentName);
		String passengerDetailFragment = retrieveContentFromCRXRepository(this.passengerDetailFragmentName);
		String passengerRowendFragment = retrieveContentFromCRXRepository(this.passengerRowendFragmentName);
		String ticketDetailendFragment = retrieveContentFromCRXRepository(this.ticketDetailendFragmentName);
		String itinerarySummaryFragment = retrieveContentFromCRXRepository(this.itinerarySummaryFragmentName);
		String itineraryTypeFragment = retrieveContentFromCRXRepository(this.itineraryTypeFragmentName);
		String flightTransitFragment = retrieveContentFromCRXRepository(this.flightTransitFragmentName);
		String flightSeatdescrFragment = retrieveContentFromCRXRepository(this.flightSeatdescrFragmentName);
		String flightArdividerFragment = retrieveContentFromCRXRepository(this.flightArdividerFragmentName);
		String itinerarySummaryendFragment = retrieveContentFromCRXRepository(this.itinerarySummaryendFragmentName);
		String costsSummaryFragment = retrieveContentFromCRXRepository(this.costsSummaryFragmentName);
		String invoiceFragment = retrieveContentFromCRXRepository(this.invoiceFragmentName);
		String emailMessageApisFragment = retrieveContentFromCRXRepository(this.emailMessageApisFragmentName);
		String emailMessageEstaFragment = retrieveContentFromCRXRepository(this.emailMessageEstaFragmentName);
		String emailTrailerFragment = retrieveContentFromCRXRepository(this.emailTrailerFragmentName);
		String emailTimoneFragment = retrieveContentFromCRXRepository(this.emailTimoneFragmentName);
		String emailCarrierFragment = retrieveContentFromCRXRepository(this.emailCarrierFragmentName);

		String serverName = ((ctx.domain != null) && !("".equals(ctx.domain))) ? ctx.domain : this.emailImagesServername;

		RoutesData prenotation = ctx.prenotation;
		List<PassengerBaseData> passengers = prenotation.getPassengers();
		StringBuilder xsltMail = new StringBuilder();

		xsltMail.append((emailHeaderFragment.replaceAll(this.placeholderImagesServer, serverName))
							.replaceAll(this.placeholderImagesFolder, this.emailImagesFolder));

		xsltMail.append(ticketDetailFragment.replaceAll(this.placeholderCodicePrenotazione, prenotation.getPnr()));

		for (int i = 0; i < passengers.size(); i++) {
			xsltMail.append(passengerRowFragment);
			/*
			 * Primo passeggero della riga
			 */
			String ticketNum = "";
			if(passengers.get(i).getTickets() != null && passengers.get(i).getTickets().size() > 0){
				ticketNum = passengers.get(i).getTickets().stream()
					.map( tkt -> tkt != null ? tkt.getTicketNumber() : "").collect(Collectors.joining(" | "));
			}
			PassengerRender pax = new PassengerRender(passengers.get(i));
			String secondName = pax.getSecondName().length() > 0 ? " " + pax.getSecondName() : "";
			String passengerDetailReplaced =
					((((passengerDetailFragment.replaceAll(this.placeholderSecondoNomePasseggero, secondName)
						).replaceAll(this.placeholderNomePasseggero, passengers.get(i).getName().toUpperCase())
						).replaceAll(this.placeholderCognomePasseggero, passengers.get(i).getLastName().toUpperCase())
						).replaceAll(this.placeholderNumPasseggero, String.valueOf(i + 1))
						).replaceAll(this.placeholderTicketNumPasseggero, ticketNum);
			xsltMail.append(passengerDetailReplaced);

			/*
			 * Incremento il contatore per il secondo passeggero della riga 
			 */
			i++;
			/*
			 * Secondo passeggero della riga se non ho già raggiunto il limite dell'array
			 */
			if (passengers.size() > i) {
				ticketNum = "";
				if(passengers.get(i).getTickets() != null && passengers.get(i).getTickets().size() > 0){
					ticketNum = passengers.get(i).getTickets().stream()
						.map( tkt -> tkt != null ? tkt.getTicketNumber() : "").collect(Collectors.joining(" | "));
				}
				pax = new PassengerRender(passengers.get(i));
				secondName = pax.getSecondName().length() > 0 ? " " + pax.getSecondName() : "";
				passengerDetailReplaced =
						((((passengerDetailFragment.replaceAll(this.placeholderSecondoNomePasseggero, secondName)
							).replaceAll(this.placeholderNomePasseggero, passengers.get(i).getName().toUpperCase())
							).replaceAll(this.placeholderCognomePasseggero, passengers.get(i).getLastName().toUpperCase())
							).replaceAll(this.placeholderNumPasseggero, String.valueOf(i + 1))
							).replaceAll(this.placeholderTicketNumPasseggero, ticketNum);
				xsltMail.append(passengerDetailReplaced);
			}
			xsltMail.append(passengerRowendFragment);
		}

		xsltMail.append(ticketDetailendFragment);

		xsltMail.append(itinerarySummaryFragment);

		for (int i = 0; i < prenotation.getRoutesList().size(); i++) {
			RouteData itinerary = prenotation.getRoutesList().get(i);

			String itineraryTypeDescr;
			switch (itinerary.getType()) {
				case OUTBOUND:
					itineraryTypeDescr = "ANDATA";
					break;
				default:
					itineraryTypeDescr = "RITORNO";
			}

			String transits;
			if (itinerary.getFlights().get(0).getFlightType().equals(FlightTypeEnum.DIRECT))
				transits = "DIRETTO";
			else {
				int transitCount = ((ConnectingFlightData) itinerary.getFlights().get(0)).getFlights().size() - 1;
				if (transitCount == 1)
					transits = "1 SCALO";
				else
					transits = String.valueOf(transitCount) + " SCALI";
			}

			String itineraryTypeReplaced = 
					(itineraryTypeFragment.replaceAll(placeholderTipoItinerario, itineraryTypeDescr)
							).replaceAll(placeholderNumScali, transits);
			xsltMail.append(itineraryTypeReplaced);

			if (itinerary.getFlights().get(0).getFlightType().equals(FlightTypeEnum.DIRECT)) {
				xsltMail.append(
						(prepareFlightFragment((DirectFlightData) itinerary.getFlights().get(0), passengers, emailTimoneFragment, emailCarrierFragment, i18n, configuration)
							.replaceAll(this.placeholderImagesServer, serverName))
							.replaceAll(this.placeholderImagesFolder, this.emailImagesFolder)
						);
			} else {
				ConnectingFlightData flightData = (ConnectingFlightData) itinerary.getFlights().get(0);
				List<Integer> transitsDurations = FlightDataUtils.computeConnectingFlightsWait(flightData);
				int connectingFlights = flightData.getFlights().size();
				for (int j = 0; j < connectingFlights; j++) {
					xsltMail.append(
							(prepareFlightFragment((DirectFlightData) flightData.getFlights().get(j), passengers, emailTimoneFragment, emailCarrierFragment, i18n, configuration)
								.replaceAll(this.placeholderImagesServer, serverName))
								.replaceAll(this.placeholderImagesFolder, this.emailImagesFolder)
							);
					if (j < (connectingFlights - 1)) {
						Integer transitDurationValue = transitsDurations.get(j);
						int transitDurationHours = (new Double(Math.floor(transitDurationValue.doubleValue() / 60))).intValue();
						int transitDurationMinutes = transitDurationValue.intValue() % 60;
						String transitDuration = 
								String.valueOf(transitDurationHours) + "H" + 
								String.valueOf(transitDurationMinutes) + "'";
						xsltMail.append(
								((flightTransitFragment.replaceAll(this.placeholderDurataTransito, transitDuration))
									.replaceAll(this.placeholderImagesServer, serverName))
									.replaceAll(this.placeholderImagesFolder, this.emailImagesFolder)
								);
					}
				}
			}

			xsltMail.append(flightSeatdescrFragment);

			if (i < (prenotation.getRoutesList().size() - 1) )
				xsltMail.append(flightArdividerFragment);
		}

		xsltMail.append(itinerarySummaryendFragment);

		String formaPagamento;
		switch (paymentData.getType()) {
			case CREDIT_CARD:
				formaPagamento = "Carta di credito " + 
					((PaymentProviderCreditCardData) paymentData.getProvider()).getType().toString() + " " +
					"XXXX-XXXX-XXXX-" + ((PaymentProviderCreditCardData) 
							paymentData.getProvider()).getCreditCardNumber().substring(12);
				break;
			case BANCA_INTESA:
			case UNICREDIT:
				formaPagamento = "Bonifico bancario " + paymentData.getType().toString();
				break;
			case GLOBAL_COLLECT:
				formaPagamento = "Bonifico bancario SOFORT";
				break;
			case STS:
				formaPagamento = "Bonifico bancario  STS";
				break;
			default:
				formaPagamento = "";
		}
		BigDecimal netAmount = ctx.netAmount;
		netAmount = netAmount.add(ctx.totalExtraCharges);
		String fare = composefragmentFareLabel(ctx);
		String fareValue = composefragmentFare(ctx);
		String ecouponValue = "";
		String extracharge = "";
		String labelEcoupon = "";
		if (ctx.coupon != null && ctx.coupon.isValid() != null) {
			if (ctx.coupon.isValid()) {
				ecouponValue = "<br/>- " + (new GenericPriceRender(ctx.totalCouponPrice, ctx.currency, ctx.locale)).getFare();
				labelEcoupon = "<br/>" + this.labelEcoupon;
			} else {
				labelEcoupon = "";
			}
		} else {
			labelEcoupon = "";
		}
		String labelExtracharge = "";
		if (ctx.totalExtraCharges.compareTo(new BigDecimal(0)) > 0) {
			extracharge = ctx.totalExtraCharges.toString();
			labelExtracharge = this.labelExtracharge;
		} else {
			labelExtracharge = "";
		}
		if (ctx.invoiceRequired == null || !ctx.invoiceRequired) {
			invoiceFragment = "";
		}
		xsltMail.append(
				((((((((((costsSummaryFragment.replaceAll(placeholderTotale, ctx.grossAmount.toString())
						).replaceAll(placeholderFare, fare)
						).replaceAll(placeholderFareValue, fareValue)
						).replaceAll(placeholderTasse, ctx.totalTaxes.toString())
						).replaceAll(placeholderSupplementi, ctx.totalExtras.toString())
						).replaceAll(placeholderEcoupon, labelEcoupon)
						).replaceAll(placeholderEcouponValue, ecouponValue)
						).replaceAll(placeholderExtracharge, labelExtracharge)
						).replaceAll(placeholderExtrachargeValue, extracharge)
						).replaceAll(placeholderFormaPagamento, formaPagamento)
						).replaceAll(placeholderRichiestaFattura, invoiceFragment)
				);

		if (ctx.isSecureFlightESTA) {
			xsltMail.append(emailMessageEstaFragment);
		} else {
			if (ctx.isApis && !ctx.isApisCanada) {
				String applicantSurname = "";
				for (PassengerBase passenger : ctx.prenotation.getPassengers()) {
					if (passenger instanceof ApplicantPassengerData) {
						applicantSurname = ((ApplicantPassengerData) passenger).getLastName();
						break;
					}
				}
				xsltMail.append((((emailMessageApisFragment.replaceAll(placeholderPnr, ctx.prenotation.getPnr())
						).replaceAll(placeholderLastname, applicantSurname)
						).replaceAll(placeholderMTODeepLink, mtoDeepLink)
						).replaceAll(placeholderLanguage, "it"));
			}
		}
		

		xsltMail.append((emailTrailerFragment.replaceAll(this.placeholderImagesServer, serverName))
								.replaceAll(this.placeholderImagesFolder, this.emailImagesFolder));
		
		return emailStilesheetFragment.replaceAll(this.placeholderMailBody, xsltMail.toString());
	}

	private String prepareFlightFragment (DirectFlightData flight, List<PassengerBaseData> passengers, String emailTimoneFragment, String emailCarrierFragment, I18n i18n, AlitaliaTradeConfigurationHolder configuration) {

		String flightDetailFragment = retrieveContentFromCRXRepository(this.flightDetailFragmentName);
		String passengerSeatrowFragment = retrieveContentFromCRXRepository(this.passengerSeatrowFragmentName);
		String passengerSeatFragment = retrieveContentFromCRXRepository(this.passengerSeatFragmentName);
		String passengerSeatrowendFragment = retrieveContentFromCRXRepository(this.passengerSeatrowendFragmentName);
		String flightDetailendFragment = retrieveContentFromCRXRepository(this.flightDetailendFragmentName);

		StringBuilder flightFragment = new StringBuilder();
		SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formatTime = new SimpleDateFormat("H.mm");

		String dataPartenzaVolo = formatDate.format(flight.getDepartureDate().getTime());
		String oraPartenzaVolo = formatTime.format(flight.getDepartureDate().getTime());
		//String dataArrivoVolo = formatDate.format(flight.getArrivalDate().getTime());
		String oraArrivoVolo = formatTime.format(flight.getArrivalDate().getTime());
		if(!flight.getCarrier().equals("AZ")) {
			emailTimoneFragment = "";
		}
		String emailCarrierFragmentReplaced = "";
		DirectFlightDataRender flightRender = new DirectFlightDataRender(flight, null, i18n, 0);
		String carrierValue = flightRender.getCarrier(); 
		if (!carrierValue.equals("")) {
			Boolean isSoggettoAppGov=computeIsSoggettoAppGov(obtainFlightNumbers(flight),configuration);
			if (isSoggettoAppGov.equals(Boolean.TRUE)){
				carrierValue=carrierValue + " - " + i18n.get("booking.common.voloSoggettoApprovazione.label");
			}
			emailCarrierFragmentReplaced = emailCarrierFragment.replaceAll(placeholderCarrierValue, carrierValue);
		}



		String flightDetailReplaced = 
				(((((((((flightDetailFragment.replaceAll(placeholderTimone, emailTimoneFragment)
						).replaceAll(placeholderNumVolo, flight.getCarrier() + flight.getFlightNumber())
						).replaceAll(placeholderDataVolo, dataPartenzaVolo)
						).replaceAll(placeholderOraPartenza, oraPartenzaVolo)
						).replaceAll(placeholderCodiceAereoportoPartenza, flight.getFrom().getCode())
						).replaceAll(placeholderCittaPartenza, i18n.get("airportsData." + flight.getFrom().getCityCode() + ".city"))
						).replaceAll(placeholderOraArrivo, oraArrivoVolo)
						).replaceAll(placeholderCodiceAereoportoArrivo, flight.getTo().getCode())
						).replaceAll(placeholderCittaArrivo, i18n.get("airportsData." + flight.getTo().getCityCode() + ".city"))
						).replaceAll(placeholderCarrier, emailCarrierFragmentReplaced);
		flightFragment.append(flightDetailReplaced);

		boolean seatPreferenceInserted = false;
		for (int i = 0; i < passengers.size(); ) {
			flightFragment.append(passengerSeatrowFragment);
			for (int j = 0; j < 3; j++) {
				String passengerSeat = "";
				if (passengers.size() > i) {
					List<SeatPreferencesData> seatPreferences;
					PassengerBaseData passengerBaseData = passengers.get(i);
					if (passengerBaseData instanceof ApplicantPassengerData)
						seatPreferences = ((ApplicantPassengerData) passengerBaseData).getPreferences().getSeatPreferences();
					else if (passengerBaseData instanceof AdultPassengerData)
						seatPreferences = ((AdultPassengerData) passengerBaseData).getPreferences().getSeatPreferences();
					else if (passengerBaseData instanceof ChildPassengerData)
						seatPreferences = ((ChildPassengerData) passengerBaseData).getPreferences().getSeatPreferences();
					else
						seatPreferences = null;
	
					if (seatPreferences != null && !seatPreferences.isEmpty()) {
						for (SeatPreferencesData seat : seatPreferences)
							//Confronto tra numeri poichè i numeri di volo possono viaggare in formati
							//diversi. Alcuni formattati con zeri all'inizio.
							//Abbiamo assunto che siano solo numeri ma in caso il catch fa il confronto tra stringhe
							try {
								if (Integer.parseInt(seat.getFlightNumber()) == Integer.parseInt(flight.getFlightNumber())) {
									passengerSeat = seat.getRow()+seat.getNumber();
									break;
								}
							} catch (NumberFormatException e) {
								if (seat.getFlightNumber() != null && seat.getFlightNumber().equals(flight.getFlightNumber())) {
									passengerSeat = seat.getRow()+seat.getNumber();
									break;
								}
							}
						String passengerSeatReplaced = 
								(passengerSeatFragment.replaceAll(placeholderNumPosto, passengerSeat)
										).replaceAll(placeholderPostoNumPasseggero, String.valueOf(i + 1));
						flightFragment.append(passengerSeatReplaced);
						seatPreferenceInserted = true;
					}
				}
				else
					break;
				i++;
			}
			flightFragment.append(passengerSeatrowendFragment);
		}

		if (!seatPreferenceInserted)
			flightFragment.append("<td/>");

		flightFragment.append(flightDetailendFragment);

		return flightFragment.toString();
	}

	private String retrieveContentFromCRXRepository(String absPath) {
		String result = null;
		InputStream is = null;
		BufferedInputStream bin = null;

		try
		{ 
			@SuppressWarnings("deprecation")
			ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			Session session = resourceResolver.adaptTo(Session.class);

			Node ntFileNode = session.getNode(absPath);
			Node ntResourceNode = ntFileNode.getNode("jcr:content");
			is = ntResourceNode.getProperty("jcr:data").getBinary().getStream();
			bin = new BufferedInputStream(is);
			
			String encoding = "UTF-8";
			if (ntResourceNode.hasProperty("jcr:encoding"))
				encoding = ntResourceNode.getProperty("jcr:encoding").getString();

			result = new String(IOUtils.toByteArray(bin), encoding);
		}
		catch (Exception ex)
		{
			logger.error("Generic exception: {}", ex);
		}
		finally
		{
			try {
				if (bin != null)
					bin.close();
				if (is != null)
					is.close();
			} catch (IOException e) {
				logger.error("IO exception: {}", e);
			}
		}
		return result;
	}

	public String getEmailSubject() {
		return emailSubject;
	}
	
	private BigDecimal[] setSparedFares(BookingSessionContext ctx) {
		BigDecimal[] sparedFares = new BigDecimal[3];
		BigDecimal totalFareAdult = new BigDecimal(0);
		BigDecimal totalFareChildren = new BigDecimal(0);
		BigDecimal totalFareInfant = new BigDecimal(0);
		List<TaxData> selectionTaxes = ctx.selectionTaxes;
		if (selectionTaxes != null) {
			for(TaxData taxData : selectionTaxes){
		    	if(taxData.getCode().toLowerCase().contains("fare adult") 
		    			|| taxData.getCode().toLowerCase().contains("fare youth")){
		    		totalFareAdult = totalFareAdult.add(taxData.getAmount());
		    	}
		    	if(taxData.getCode().toLowerCase().contains("fare children")){
		    		totalFareChildren = totalFareChildren.add(taxData.getAmount());
		    	}
		    	if(taxData.getCode().toLowerCase().contains("fare infant")){
		    		totalFareInfant = totalFareInfant.add(taxData.getAmount());
		    	}
			}
		}
		sparedFares[0] = totalFareAdult;
		sparedFares[1] = totalFareChildren;
		sparedFares[2] = totalFareInfant;
		return sparedFares;
	}
	
	private String composefragmentFareLabel(BookingSessionContext ctx) {
		String fare = this.labelFare;
		if (ctx.searchPassengersNumber.getNumChildren() > 0){
			fare += "<br/>" + this.labelFareChild;
		}
		if (ctx.searchPassengersNumber.getNumInfants() > 0){
			fare += "<br/>" + this.labelFareInfant;

		}
		return fare + "<br/>";
	}

	private String composefragmentFare(BookingSessionContext ctx) {
		String fareValue = "";
		BigDecimal[] sparedFares = setSparedFares(ctx);
		BigDecimal totalFareAdult = sparedFares[0];
		BigDecimal totalFareChildren = sparedFares[1];
		BigDecimal totalFareInfant = sparedFares[2];
		GenericPriceRender price = new GenericPriceRender(totalFareAdult, totalFareChildren, totalFareInfant, null, null, null, 
				null, ctx.currency, ctx.locale);
		
		fareValue = price.getFare();
		if (ctx.searchPassengersNumber.getNumChildren() > 0){
			fareValue += "<br/>" + price.getFareChildren();
		}
		if (ctx.searchPassengersNumber.getNumInfants() > 0){
			fareValue += "<br/>" + price.getFareInfant();

		}
		return fareValue + "<br/>";
	}

    private Boolean computeIsSoggettoAppGov(String numeroVolo, AlitaliaTradeConfigurationHolder configuration) {
        String[] arrayVolo =configuration.getListaVoliSoggettiApprovazioneGovernativa().split(";");
        for (int i=0; i<arrayVolo.length; i++){
            if (arrayVolo[i].equals(numeroVolo)) {return true;}
        }
        return false;
    }

    private String obtainFlightNumbers(FlightData flightData) {
        String result = "";
        //FlightData flightData = flightSelection.getFlightData();
        if (flightData instanceof DirectFlightData) {
            DirectFlightData directFlightData = (DirectFlightData) flightData;
            return directFlightData.getCarrier() + directFlightData.getFlightNumber();
        } else {
            ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
            for (FlightData flight : connectingFlightData.getFlights()) {
                DirectFlightData directFlightData = (DirectFlightData) flight;
                String number = directFlightData.getCarrier() + directFlightData.getFlightNumber();
                if (!result.equals("")) {
                    result += ", " + number;
                } else {
                    result = number;
                }
            }
        }
        return result;
    }
}
