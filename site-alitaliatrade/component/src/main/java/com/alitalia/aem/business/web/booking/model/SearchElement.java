package com.alitalia.aem.business.web.booking.model;

import java.util.Calendar;

public class SearchElement {
	
	private SearchElementAirport from;
	private SearchElementAirport to;
	private Calendar departureDate;

	public SearchElement(SearchElementAirport from, SearchElementAirport to, Calendar departureDate) {
		this.from = from;
		this.to = to;
		this.departureDate = departureDate;
	}

	public SearchElementAirport getFrom() {
		return from;
	}

	public SearchElementAirport getTo() {
		return to;
	}

	public Calendar getDepartureDate() {
		return departureDate;
	}

	public void setFrom(SearchElementAirport from) {
		this.from = from;
	}

	public void setTo(SearchElementAirport to) {
		this.to = to;
	}

	public void setDepartureDate(Calendar departureDate) {
		this.departureDate = departureDate;
	}

	@Override
	public String toString() {
		return "SearchElement [from=" + from + ", to=" + to + ", departureDate=" + departureDate
				+ "]";
	}
	
}
