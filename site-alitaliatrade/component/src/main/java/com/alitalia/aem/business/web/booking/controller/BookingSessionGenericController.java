package com.alitalia.aem.business.web.booking.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.utils.Constants;

public class BookingSessionGenericController extends WCMUse {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	// booking session context
	protected BookingSessionContext ctx;
	
	// Configuration manager
	protected AlitaliaTradeConfigurationHolder configuration;

	@Override
	public void activate() throws Exception {

		try {
			SlingScriptHelper helper = getSlingScriptHelper();
	    	configuration = helper.getService(AlitaliaTradeConfigurationHolder.class);
			ctx = (BookingSessionContext) getRequest().getSession(true).getAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE);
		} catch (Exception e) {
			logger.error("Cannot find booking session context in session. ", e);
			ctx = null;
		}

	}
	
	public boolean isIllegalCTXState(HttpServletResponse response) throws IOException{
		if (ctx == null) {
			logger.error("IllegalState: booking session context not found in session ");
			if (!getResponse().isCommitted()) {
				response.sendRedirect(configuration.getErrorNavigationPage());
			}
			return true;
		}
		return false;
	}

	public BookingSessionContext getCtx() {
		return ctx;
	}

}
