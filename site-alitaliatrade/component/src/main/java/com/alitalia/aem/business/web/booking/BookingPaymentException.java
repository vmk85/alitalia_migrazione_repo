package com.alitalia.aem.business.web.booking;

import com.alitalia.aem.common.data.home.enumerations.InitializePaymentErrorEnum;

@SuppressWarnings("serial")
public class BookingPaymentException extends Exception {

	public final static int BOOKING_ERROR_INITIALIZEPAYMENT = 101;
	public final static int BOOKING_ERROR_AUTHORIZEPAYMENT = 201;
	public final static int BOOKING_ERROR_CHECKPAYMENT = 301;
	public final static int BOOKING_ERROR_RETRIEVETICKETS = 401;
	public final static int BOOKING_ERROR_GENERIC = 501;
	
	private int code;
	private InitializePaymentErrorEnum initializePaymentEnum;

	public BookingPaymentException(int code, String message) {
		super(message);
		this.code = code;
	}
	
	public BookingPaymentException(int code, String message, InitializePaymentErrorEnum initializePaymentEnum) {
		super(message);
		this.code = code;
		this.initializePaymentEnum = initializePaymentEnum;
	}
	
	public int getCode() {
		return code;
	}
	
	public InitializePaymentErrorEnum getInitializePaymentEnum() {
		return initializePaymentEnum;
	}
	
}
