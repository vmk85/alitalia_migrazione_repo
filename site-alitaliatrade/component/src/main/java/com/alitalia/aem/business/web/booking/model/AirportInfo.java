package com.alitalia.aem.business.web.booking.model;

import java.util.Comparator;

public class AirportInfo {
	
	private String code;
	private String airportName;
	private String cityCode;
	private String cityName;
	private String country;
	
	public static final Comparator<AirportInfo> COMPARE_BY_CITY_AND_AIRPORT = new Comparator<AirportInfo>() {
		/*
		 * Comparazione per ordinamento Aeroporti
		 * Gli aeroporti mostrati nella casella di testo della form devono rispettare l'ordinamento 
		 * definito dalle seguenti regole:
		 * - A parità di città, prima Tutti lgi aeroproti.
		 * - Ordinamento alfabetico per citta
		 * - Ordinamento alfabetico per aeroporto
		 * 
		 */
		@Override
		public int compare(AirportInfo one, AirportInfo other) {
			if (one.getCityName().compareToIgnoreCase(other.getCityName()) == 0) {
				int oneCityAllAirport = one.getCityCode().compareToIgnoreCase(one.getCode());
				int otherCityAllAirport = other.getCityCode().compareToIgnoreCase(other.getCode());
				if (oneCityAllAirport == otherCityAllAirport) {
					return one.getAirportName().compareToIgnoreCase(other.getAirportName());
				} else if (oneCityAllAirport < otherCityAllAirport) {
					return -1;
				} else {
					return 1;
				}
			} else if (one.getCityName().compareToIgnoreCase(other.getCityName()) < 0) {
				return -1;
			} else {
				return 1;
			}
		}
	};
	

	public AirportInfo(String code, String airportName, String cityCode,
			String cistyName, String country) {
		super();
		this.code = code;
		this.airportName = airportName;
		this.cityCode = cityCode;
		this.cityName = cistyName;
		this.country = country;
	}

	public String getCode() {
		return code;
	}

	public String getAirportName() {
		return airportName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public String getCountry() {
		return country;
	}

}
