package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.utils.Constants;


@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "redirectPaymentBusinessServlet" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) ,
	@Property(name = "returnUrl.page"),
	@Property(name = "errorUrl.page") })
@SuppressWarnings("serial")
public class Redirect3dsBusinessServlet extends SlingAllMethodsServlet {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Reference
	private AlitaliaTradeConfigurationHolder configuration;

	private ComponentContext componentContext;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {

		logger.info("Executing redirectPaymentBusinessServlet");
		
		BookingSessionContext ctx = (BookingSessionContext) request.getSession(true).getAttribute(
				Constants.BOOKING_CONTEXT_ATTRIBUTE);

		if (ctx == null) {
			logger.error("Cannot find booking session context in session.");
			throw new IllegalStateException("Booking session context not found.");
		}

		String returnUrl = ctx.domain + (String) componentContext.getProperties().get("returnUrl.page");
		String errorUrl = ctx.domain + (String) componentContext.getProperties().get("errorUrl.page");

		try {

			String resultCode = request.getParameter("ResultCode");
			String responseCode = request.getParameter("ResponseCode");
			String merchantId = request.getParameter("MerchantID");
			String mac = request.getParameter("Mac");
			String supplierId = request.getParameter("SupplierID");

			logger.debug("Parameters: resultCode["+resultCode+"], responseCode["+responseCode+"], merchantId["+merchantId+"], mac["+mac+"], supplierId["+supplierId+"]");
			
			if (!"error".equalsIgnoreCase(responseCode)){
				ctx.paymentData.getProcess().setMac(mac);
				ctx.paymentData.getProcess().setResponseCode(responseCode);
				ctx.paymentData.getProcess().setResultCode(resultCode);
				ctx.paymentData.getProcess().setSupplierId(supplierId);
				ctx.paymentData.getProcess().setMerchantId(merchantId);
				response.sendRedirect(returnUrl);
			} else {
				logger.debug("Error received from Adyen. Redirecting to: ["+errorUrl+"]");
				response.sendRedirect(errorUrl);
			}

		} catch(Exception e) {
			logger.error("Unexpected error: ", e);
			response.sendRedirect(errorUrl);
		}
	}

}
