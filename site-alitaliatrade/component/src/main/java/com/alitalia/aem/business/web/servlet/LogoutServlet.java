package com.alitalia.aem.business.web.servlet;

import com.day.cq.commons.TidyJSONWriter;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.http.Cookie;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
        @Property(name = "sling.servlet.selectors", value = {"logout-sfdc"}),
        @Property(name = "sling.servlet.methods", value = {"GET"}),
        @Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
})
@SuppressWarnings("serial")
public class LogoutServlet extends SlingSafeMethodsServlet {

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {

        Cookie loginCookie = request.getCookie("login-token");
        loginCookie.setValue(null);
        loginCookie.setPath("/");
        loginCookie.setMaxAge(0);
        response.addCookie(loginCookie);

        try {

            response.setContentType("application/json");
            TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
            json.object();
            json.key("isError").value(false);
            json.endObject();
        } catch (Exception e) {
            logger.error("Unexected error generating JSON response." + e.getMessage());
        }

    }

}
