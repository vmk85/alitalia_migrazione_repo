package com.alitalia.aem.business.web.controllers;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.AlitaliaTradeAuthUtils;

public class FormNewsletter extends WCMUse {

	private String agenzia;
	private String codice;
	

	@Override
    public void activate() throws Exception {
		agenzia = AlitaliaTradeAuthUtils.getProperty(getRequest(), "ragioneSociale");
		codice = AlitaliaTradeAuthUtils.getProperty(getRequest(), "codiceAgenzia");
	}
	
	public void setAgenzia(String agenzia){
		this.agenzia = agenzia;
	}
	
	public String getAgenzia(){
		return agenzia;
	}
	
	public void setCodice(String codice){
		this.codice = codice;
	}
	
	public String getCodice(){
		return codice;
	}
	
}