package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.booking.BookingSearchCUGEnum;
import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.BookingSession;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.statistics.utils.TradeStatisticUtils;
import com.alitalia.aem.business.web.utils.Constants;
import com.day.cq.commons.TidyJSONWriter;

@Component(immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "bookingflightsearchservice" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "error.page", description = "Users will be redirected to the specified page after a failed outcome."),
	@Property(name = "errorfunctionality.page", description = "Users will be redirected to the specified page after a failed outcome."),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class BookingFlightSearchService extends SlingSafeMethodsServlet {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private ComponentContext componentContext;

	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Reference
	private AlitaliaTradeConfigurationHolder configuration;

	@Reference
	private BookingSession bookingSession;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		String genericErrorPagePlain = "#";
		String errorFunctionalityPagePlain = "#";
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		
		try {
			json.object();
			genericErrorPagePlain = (String) 
					componentContext.getProperties().get("error.page");
			errorFunctionalityPagePlain = (String) 
					componentContext.getProperties().get("errorfunctionality.page");
			
			BookingSessionContext ctx = (BookingSessionContext) 
					request.getSession(true).getAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE);

			//INIZIO *** MODIFICA IP ***
			String[] clientIpHeaders = configuration.getClientIpHeaders();
			String ipAddress = "";
			ipAddress = TradeStatisticUtils.getClientIP(clientIpHeaders, request);
			if (ipAddress != null && !ipAddress.isEmpty()) {
				ctx.ipAddress = ipAddress;
			}
			//FINE.

			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				if (!response.isCommitted()) {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Booking session context not found.");
				}
				return;
			}
			String residency = request.getParameter("isResidency");
			if( (residency == null || ("").equals(residency)) || (!residency.equals("true") && !residency.equals("false")) ) {
				logger.error("Residency in request is not found");
				throw new IllegalArgumentException("Residency in request is not found");
			}
			boolean isResidency = Boolean.parseBoolean(residency);
			if ( ctx.isAllowedContinuitaTerritoriale ){
				bookingSession.selectContinuitaTerritoriale(ctx, new Boolean(isResidency));
			}
			
			boolean success = false;
			
			bookingSession.performSearch(ctx);
			if (ctx.availableFlights != null && ctx.availableFlights.getRoutes() != null) {
				success = true;
			}
			
			response.setContentType("application/json");
			

			if (success) {
				if (ctx.cug == BookingSearchCUGEnum.YOUTH) {
					if (!ctx.youthSolutionFound) {
						json.key("solutionNotFound").value("youth");
					}
				}
				if (ctx.cug == BookingSearchCUGEnum.FAMILY) {
					if (!ctx.familySolutionFound) {
						json.key("solutionNotFound").value("family");
					}
				}
				if (ctx.cug == BookingSearchCUGEnum.MILITARY) {
					if (!ctx.militarySolutionFound) {
						json.key("solutionNotFound").value("allSolutions");
						json.key("redirect").value(errorFunctionalityPagePlain);
					}
				}
				if (ctx.isSelectedContinuitaTerritoriale != null && 
						ctx.isSelectedContinuitaTerritoriale && 
						!ctx.territorialContinuitySolutionFound) {
					json.key("solutionNotFound").value("territorialContinuity");
				}
				if (ctx.cug == BookingSearchCUGEnum.ADT) {
					if (ctx.availableFlights.getRoutes().isEmpty()) {
						json.key("solutionNotFound").value("allSolutions");
						json.key("redirect").value(errorFunctionalityPagePlain);
					}
				}
				json.key("result").value("OK");
				json.key("roundtrip").value(ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP);
				json.key("type").value(ctx.cug.value());
			} else {
				json.key("result").value("NOK");
				json.key("redirect").value(genericErrorPagePlain);
			}
			json.endObject();

			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			if (!response.isCommitted()) {
				response.setContentType("application/json");
				try {
					json.key("redirect").value(genericErrorPagePlain);
					json.endObject();
				} catch (JSONException e1) {
					logger.error("Error building JSON");
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
				}
			}
		}
		
	}

}
