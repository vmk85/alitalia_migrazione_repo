package com.alitalia.aem.business.web.booking.render;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.SeatData;
import com.alitalia.aem.common.data.home.SeatMapData;
import com.alitalia.aem.common.data.home.SeatMapRowData;
import com.alitalia.aem.common.data.home.SeatMapSectorData;
import com.alitalia.aem.common.data.home.enumerations.AvailabilitySeatEnum;
import com.alitalia.aem.common.data.home.enumerations.TypeSeatEnum;

public class SeatsMapRender {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final static String NOT_EXISTS = "";
	private final static String OCCUPIED_SEAT = "occupied_seat";
	private final static String AVAILABLE_SEAT = "available_seat seatFreeAssignment";
	private final static String AISLE_SEAT = "seatAisle";
	private final static String LEFT_EXIT = "exit_left exit2_pos";
	private final static String RIGHT_EXIT = "exit_right exit2_pos";
	
	private SeatMapData seatMapData;
	private String[] sectorLabel;
	private String[][] sectorRowLabel;
	private String[][] headerMap;
	private String[][][] seatsMap;
	private HashMap<Integer, String> seatToHeader;
	
	public SeatsMapRender () {
		init();
	}
	
    public SeatsMapRender (SeatMapData seatMapData) {
    	this.seatMapData = seatMapData;
    	init();
	}
	
	private void init() {
		
		this.sectorLabel = null;
		this.sectorRowLabel = null;
		this.headerMap = null;
		this.seatsMap = null;
		this.seatToHeader = null;
		
		// check data
		
		if (seatMapData == null) { 
			return;
		}
		if (seatMapData.getSeatMapSectors() == null || seatMapData.getSeatMapSectors().size() == 0) {
			logger.warn("Ignored seat map data: sectors list is null or empty");
			return;
		}
		for (SeatMapSectorData sector : seatMapData.getSeatMapSectors()) {
			if (sector.getSeatMapRows() == null || sector.getSeatMapRows().size() == 0) {
				logger.warn("Ignored seat map data: rows list for a section is null or empty");
				return;
			}
			for (SeatMapRowData row : sector.getSeatMapRows()) {
				if (row.getSeats() == null || row.getSeats().size() == 0) {
					logger.warn("Ignored seat map data: seats list for a row is null or empty");
					return;
				}
			}
		}
		
		// process data
		
		this.sectorLabel = new String[seatMapData.getSeatMapSectors().size()];
		this.sectorRowLabel = new String[seatMapData.getSeatMapSectors().size()][];
		this.headerMap = new String[seatMapData.getSeatMapSectors().size()][];
		this.seatsMap = new String[seatMapData.getSeatMapSectors().size()][][];
		
		int sectorIndex = -1;
		int rowIndex = -1;
		int seatIndex = -1;
		
		sectorIndex = -1;
		for (SeatMapSectorData section : seatMapData.getSeatMapSectors()) {
			sectorIndex++;
			
			// for each section, prepare the section label
			this.sectorLabel[sectorIndex] = "FILA " + section.getStartingRow() + " - " + section.getEndingRow();
			
			this.seatsMap[sectorIndex] = new String[section.getSeatMapRows().size()][];
			
			rowIndex = -1;
			for (SeatMapRowData sectionRow : section.getSeatMapRows()) {
				rowIndex++;
				
				// on first row of each section, initialize the header
				// label map and the row label for the section
				if (rowIndex == 0) {
					
					// include room for two additional header elements for the exit 
					// graphical indications, presetting them to an empty string
					this.headerMap[sectorIndex] = new String[sectionRow.getSeats().size() + 2];
					this.headerMap[sectorIndex][0] = "";
					this.headerMap[sectorIndex][sectionRow.getSeats().size() + 1] = "";
					
					this.sectorRowLabel[sectorIndex] = new String[section.getSeatMapRows().size()];
				}
				
				// set the global row label
				this.sectorRowLabel[sectorIndex][rowIndex] = sectionRow.getNumber();
				
				// include room for two additional elements for the exit graphical indications
				this.seatsMap[sectorIndex][rowIndex] = new String[sectionRow.getSeats().size() + 2];
				
				// preset the first and last seats (used to display emergency exists) to blank
				this.seatsMap[sectorIndex][rowIndex][0] = NOT_EXISTS;
				this.seatsMap[sectorIndex][rowIndex][sectionRow.getSeats().size() + 1] = NOT_EXISTS;
				
				seatIndex = -1;
				for (SeatData seat : sectionRow.getSeats()) {
					seatIndex++;
					
					// on first row of each section, initialize the elements of the header label map for the section
					if (rowIndex == 0) {
						this.headerMap[sectorIndex][seatIndex + 1] = seat.getNumber();
					}
					
					// set seat type
					if (seat.getTypeSeat() == TypeSeatEnum.AISLE) {
						this.seatsMap[sectorIndex][rowIndex][seatIndex + 1] = AISLE_SEAT;
					} else if (seat.getTypeSeat() == TypeSeatEnum.NOT_EXIST) {
						this.seatsMap[sectorIndex][rowIndex][seatIndex + 1] = NOT_EXISTS;
					} else {
						if (seat.getAvailability() == AvailabilitySeatEnum.FREE) {
							this.seatsMap[sectorIndex][rowIndex][seatIndex + 1] = AVAILABLE_SEAT;
						} else {
							this.seatsMap[sectorIndex][rowIndex][seatIndex + 1] = OCCUPIED_SEAT;
						}
					}
					
					// evaluate emergency exits
					if (seat.getTypeSeat() == TypeSeatEnum.WINDOW_EXIT_SEAT) {
						if (seatIndex == 0) {
							seatsMap[sectorIndex][rowIndex][0] = LEFT_EXIT;
						} else if (seatIndex == (sectionRow.getSeats().size() - 1)) {
							seatsMap[sectorIndex][rowIndex][sectionRow.getSeats().size() + 1] = RIGHT_EXIT;
						}
					}
					
				}
				
			}
			
		}
		
	}
	
	public SeatMapData getSeatMapData() {
		return seatMapData;
	}
	
	public String[][] getHeaderMap() {
		return headerMap;
	}
	
	public String[] getSectorLabel() {
		return sectorLabel;
	}
	
	public String[][] getSectorRowLabel() {
		return sectorRowLabel;
	}
	
	public String[][][] getSeatsMap() {
		return seatsMap;
	}
	
	public HashMap<Integer, String> getSeatToHeader() {
		return seatToHeader;
	}
	
}
