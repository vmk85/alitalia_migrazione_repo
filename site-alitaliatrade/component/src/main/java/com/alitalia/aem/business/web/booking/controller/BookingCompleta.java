package com.alitalia.aem.business.web.booking.controller;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;

public class BookingCompleta extends BookingSessionGenericController{

	private String redirectFailurePage = "";
	
	private AlitaliaTradeConfigurationHolder configuration = null;

	@Override
	public void activate() throws Exception {
		super.activate();
		
		try {
			configuration = getSlingScriptHelper().getService(AlitaliaTradeConfigurationHolder.class);
			redirectFailurePage = configuration.getBookingConfirmPage() + "?showCallCenterMsg=true";
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			redirectFailurePage = configuration.getBookingConfirmPage() + "?showCallCenterMsg=true";
		}
	}

	public String getRedirectFailurePage() {
		return redirectFailurePage;
	}
	
}
