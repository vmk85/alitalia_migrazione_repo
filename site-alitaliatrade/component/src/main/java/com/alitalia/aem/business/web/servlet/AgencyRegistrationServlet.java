package com.alitalia.aem.business.web.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.business.web.utils.AnagraficaUtils;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.MunicipalityData;
import com.alitalia.aem.common.messages.home.AgencySubscriptionRequest;
import com.alitalia.aem.common.messages.home.AgencySubscriptionResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "agencyregistration" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.") 
})
@SuppressWarnings("serial")
public class AgencyRegistrationServlet extends GenericFormValidatorServlet {
	
	private static String COD_ITA = "ITA";
	
	private static String REPLY_OK = "0000";
	
	private ComponentContext componentContext;
	
	@Reference
	private BusinessLoginDelegate businessLoginDelegate;
	
	@Reference
	private StaticDataDelegate staticDataDelegate;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		Validator validator = new Validator();
		String market = "IT";
		String language= LocalizationUtils.getLocaleByResource(request.getResource(), "it").toString().toUpperCase();
		String ragioneSociale = request.getParameter("ragioneSociale");
		validator.addDirectParameterMessagePattern("ragioneSociale", ragioneSociale, Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Ragione sociale", "isNotEmpty");
		validator.addDirectParameter("ragioneSociale", ragioneSociale, Constants.MESSAGE_MAX_LENGTH, "maxLengthAddress");
		
		String stato = request.getParameter("stato");
		
		String partitaIva = request.getParameter("partitaIva");
		
		if (stato.equals(COD_ITA)) {
			validator.addDirectParameterMessagePattern("partitaIva", partitaIva, Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Partita Iva", "isNotEmpty");
		}
		/*if (stato.equals(COD_ITA) || (partitaIva != null && !partitaIva.isEmpty())) {
			validator.addDirectParameterMessagePattern("partitaIva", partitaIva, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Partita Iva", "isIva");
		}*/
		
		String codiceFiscale = request.getParameter("codiceFiscale");
		
		if (codiceFiscale!= null && !codiceFiscale.isEmpty()) {
			validator.addDirectParameterMessagePattern("codiceFiscale", codiceFiscale, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Codice Fiscale", "isCodiceFiscale");
		}
		
		/* Validate the parameter is in the select*/
		Map<String, String> mapStati = AnagraficaUtils.obtainMapState();
		String[] allowedStateValues = (String[]) mapStati.keySet().toArray(new String[0]);
		validator.setAllowedValuesMessagePattern("stato", request.getParameter("stato"), allowedStateValues, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Stato");
		
		String[] allowedProvincesValues = null;
		if (request.getParameter("stato").equalsIgnoreCase("SM")) {
			allowedProvincesValues = new String[1];
			allowedProvincesValues[0] = "SM";
		} else {
			List<CountryData> provinces = AnagraficaUtils.obtainListOfProvinces(staticDataDelegate,request,market,language);
			allowedProvincesValues = new String[provinces.size()];
			for (int i=0; i < provinces.size(); i++){
				allowedProvincesValues[i] = provinces.get(i).getStateCode();
			}
		}
		validator.setAllowedValuesMessagePattern("provincia", request.getParameter("provincia"), allowedProvincesValues, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Provincia");

		String[] allowedCouncilsValues = null;
		if (request.getParameter("stato").equalsIgnoreCase("SM")) {
			allowedCouncilsValues = new String[1];
			allowedCouncilsValues[0] = "San Marino";
		} else {
			List<MunicipalityData> councils = AnagraficaUtils.obtainCouncilsList(staticDataDelegate,request,market,language);
			allowedCouncilsValues = new String[councils.size()];
			for (int i=0; i < councils.size(); i++){
				allowedCouncilsValues[i] = councils.get(i).getDescription();
			}
		}
		validator.setAllowedValuesMessagePattern("citta", request.getParameter("citta"), allowedCouncilsValues, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Citt&agrave;");
		/* end */
		
		validator.addDirectParameterMessagePattern("stato", request.getParameter("stato"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Stato", "isNotEmpty");
		validator.addDirectParameterMessagePattern("provincia", request.getParameter("provincia"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Provincia", "isNotEmpty");
		validator.addDirectParameterMessagePattern("citta", request.getParameter("citta"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Citt&agrave;", "isNotEmpty");
		
		validator.addDirectParameterMessagePattern("cap", request.getParameter("cap"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "C.A.P.", "isNotEmpty");
		validator.addDirectParameterMessagePattern("cap", request.getParameter("cap"), Constants.MESSAGE_GENERIC_INVALID_FIELD, "C.A.P.", "isCAP");
		
		validator.addDirectParameterMessagePattern("indirizzo", request.getParameter("indirizzo"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Indirizzo", "isNotEmpty");
		validator.addDirectParameter("indirizzo", request.getParameter("indirizzo"), Constants.MESSAGE_MAX_LENGTH, "maxLengthAddress");
		
		validator.addDirectParameterMessagePattern("telefono", request.getParameter("telefono"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Telefono", "isNotEmpty");
		validator.addDirectParameterMessagePattern("telefono", request.getParameter("telefono"), Constants.MESSAGE_GENERIC_INVALID_FIELD, "Telefono", "isNumber");
		
		String fax = request.getParameter("fax");
		if (fax != null && !fax.isEmpty()) {
			validator.addDirectParameterMessagePattern("fax", fax, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Fax", "isNumber");
		}		
		
		String emailBanconista = request.getParameter("emailBanconista");
		String emailTitolare = request.getParameter("emailTitolare");
		String emailBanconistaVerifica = request.getParameter("emailBanconistaVerifica");
		String emailTitolareVerifica = request.getParameter("emailTitolareVerifica");
		
		validator.addDirectParameterMessagePattern("emailBanconista", emailBanconista, Constants.MESSAGE_GENERIC_EMPTY_FIELD, "E-mail", "isNotEmpty");
		validator.addDirectParameterMessagePattern("emailTitolare", emailTitolare, Constants.MESSAGE_GENERIC_EMPTY_FIELD, "E-mail", "isNotEmpty");
		validator.addDirectParameterMessagePattern("emailBanconistaVerifica", emailBanconistaVerifica, Constants.MESSAGE_GENERIC_EMPTY_FIELD, "E-mail", "isNotEmpty");
		validator.addDirectParameterMessagePattern("emailTitolareVerifica", emailTitolareVerifica, Constants.MESSAGE_GENERIC_EMPTY_FIELD, "E-mail", "isNotEmpty");
		
		validator.addDirectParameterMessagePattern("emailBanconista", emailBanconista, Constants.MESSAGE_GENERIC_INVALID_FIELD, "E-mail", "isEmail");
		validator.addDirectParameterMessagePattern("emailTitolare", emailTitolare, Constants.MESSAGE_GENERIC_INVALID_FIELD, "E-mail", "isEmail");
		validator.addDirectParameterMessagePattern("emailBanconistaVerifica", emailBanconistaVerifica, Constants.MESSAGE_GENERIC_INVALID_FIELD, "E-mail", "isEmail");
		validator.addDirectParameterMessagePattern("emailTitolareVerifica", emailTitolareVerifica, Constants.MESSAGE_GENERIC_INVALID_FIELD, "E-mail", "isEmail");
		
		if (emailBanconista != null && !emailBanconista.isEmpty() && emailBanconistaVerifica != null && !emailBanconistaVerifica.isEmpty()) {
			validator.addCrossParameter("emailBanconistaVerifica", emailBanconista, emailBanconistaVerifica,	Constants.MESSAGE_EMAILS_NOT_EQUALS, "areEqual");
		}
		
		if (emailTitolare != null && !emailTitolare.isEmpty() && emailTitolareVerifica != null && !emailTitolareVerifica.isEmpty()) {
			validator.addCrossParameter("emailTitolareVerifica", emailTitolare, request.getParameter("emailTitolareVerifica"),
					Constants.MESSAGE_EMAILS_NOT_EQUALS, "areEqual");
		}
		
		validator.addDirectParameter("checkTrattamento", request.getParameter("checkTrattamento"), Constants.MESSAGE_CHECK_PERSONAL_INFO, "isNotEmpty");
		validator.addDirectParameter("checkCondizioni", request.getParameter("checkCondizioni"), Constants.MESSAGE_CHECK_NECESSARY, "isNotEmpty");
		
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		
		try {
			
			AgencySubscriptionRequest serviceRequest = new AgencySubscriptionRequest();
			serviceRequest.setTid(IDFactory.getTid());
			serviceRequest.setSid(IDFactory.getSid(request));
			
			serviceRequest.setIndirizzo(request.getParameter("indirizzo"));
			serviceRequest.setCitta(request.getParameter("citta"));
			serviceRequest.setCodiceFiscale(request.getParameter("codiceFiscale"));			
			serviceRequest.setNomeCompagnia(request.getParameter("ragioneSociale"));
			serviceRequest.setEmailOperatore(request.getParameter("emailBanconista"));
			serviceRequest.setEmailTitolare(request.getParameter("emailTitolare"));
			serviceRequest.setNumeroFax(request.getParameter("fax"));
			serviceRequest.setPartitaIva(request.getParameter("partitaIva"));
			serviceRequest.setCap(request.getParameter("cap"));
			serviceRequest.setProvincia(request.getParameter("provincia"));
			String stato = request.getParameter("stato");
			if("SM".equals(stato)){
				stato = "RSM";
			}
			serviceRequest.setStato(stato);
			serviceRequest.setNumeroTelefono(request.getParameter("telefono"));
			AgencySubscriptionResponse serviceResponse = businessLoginDelegate.subscribeAgency(serviceRequest);
			
			if (serviceResponse.getErrorCode() != null && serviceResponse.getErrorCode().equals(REPLY_OK)) {
				
				logger.debug("Agency {} succesfully registered.", request.getParameter("partitaIva"));
				String successPage = (String) componentContext.getProperties().get("success.page");
				if (successPage != null) {
					response.sendRedirect(response.encodeRedirectURL(successPage));
				}
				
			} else {
				
				logger.debug("Error during registration for agency {}.", request.getParameter("partitaIva"));
				String errorPage = (String) componentContext.getProperties().get("failure.page");
				if (errorPage != null) {
					response.sendRedirect(response.encodeRedirectURL(errorPage));
				}
			}
		} catch (Exception e) {
			logger.error("Unexpected error", e);
			String errorPage = (String) componentContext.getProperties().get("failure.page");
			if (errorPage != null) {
				response.sendRedirect(response.encodeRedirectURL(errorPage));
			}
		}
	}
}
