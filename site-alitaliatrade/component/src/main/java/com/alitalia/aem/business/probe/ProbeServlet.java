package com.alitalia.aem.business.probe;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.day.cq.commons.TidyJSONWriter;

@Component(immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "probe" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class ProbeServlet extends SlingSafeMethodsServlet {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private ComponentContext componentContext;

	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		
		
		try {
			json.object();
			logger.debug("CALL Probe");
			response.setContentType("application/json");
			json.key("Server publisher IP").value(request.getLocalAddr());
			json.key("Server publisher host name").value(request.getLocalName());
			json.key("IP Chiamante:").value(request.getRemoteHost());
			json.key("Server name that the request was sent to:").value(request.getServerName());
			if (request.getSession() != null) {
				json.key("Session Info").array();
				json.object();
				json.key("SessionObject").value(request.getSession().toString());
				json.endObject();
				Enumeration<?> enumeration = request.getSession().getAttributeNames();
				while (enumeration.hasMoreElements()) {
					json.object();
					String elem = (String) enumeration.nextElement();
					if (elem.equals("alitaliaTradeBookingSessionContext")) {
						BookingSessionContext ctx = (BookingSessionContext) request.getSession().getAttribute(elem);
						json.key(elem).value(ctx.toString());
					} else {
						json.key(elem).value(request.getSession().getAttribute(elem));
					}
					json.endObject();
				}
				json.endArray();
			} else {
				json.key("Session").value(null);
			}
			json.key("SUCCESS").value(true);
			json.endObject();

		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			if (!response.isCommitted()) {
				response.setContentType("application/json");
				try {
					json.key("SUCCESS").value(false);
					json.endObject();
				} catch (JSONException e1) {
					logger.error("Error building JSON");
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
				}
			}
		}
		
	}

}

