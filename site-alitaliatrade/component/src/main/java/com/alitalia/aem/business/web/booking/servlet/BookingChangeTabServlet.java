package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.booking.BookingSearchCUGEnum;
import com.alitalia.aem.business.web.booking.BookingSession;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.statistics.utils.TradeStatisticUtils;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.day.cq.commons.TidyJSONWriter;

@Component(immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "bookingchangetab" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "error.page", description = "Users will be redirected to the specified page after a failed outcome."),
	@Property(name = "errorfunctionality.page", description = "Users will be redirected to the specified page after a failed outcome."),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class BookingChangeTabServlet extends SlingSafeMethodsServlet {

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private BookingSession bookingSession;
	
	@Reference
	private AlitaliaTradeConfigurationHolder configuration;
	
	private ComponentContext componentContext;
	
	
	
	private static final String HYPHEN_FLIGHT_DATE_FORMAT = "yyyy-MM-dd";
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {

		String genericErrorPagePlain = "#";
		String errorFunctionalityPagePlain = "#";
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		
		try {
			json.object();
			genericErrorPagePlain = (String) 
					componentContext.getProperties().get("error.page");
			errorFunctionalityPagePlain = (String) 
					componentContext.getProperties().get("errorfunctionality.page");

			//retrive parameters form request
			if( ( request.getParameter("direction")==null && ("").equals(request.getParameter("direction")) ) 
					|| ( !("1").equals(request.getParameter("direction")) && !("0").equals(request.getParameter("direction")) ) ){
				logger.error("direction not found");
				throw new RuntimeException();
			}
			String direction=request.getParameter("direction");
			int searchElementIndex = Integer.parseInt(direction);
			
			if( request.getParameter("selectedTab")==null && ("").equals(request.getParameter("selectedTab")) ){
				logger.error("selectedTab not found");
				throw new RuntimeException();
			}
			String selectedTab=request.getParameter("selectedTab");
			selectedTab = selectedTab.substring(
					selectedTab.lastIndexOf("-")+1, selectedTab.length());
			int dateChoiceIndex = Integer.parseInt(selectedTab);
			
			
			BookingSessionContext ctx =  (BookingSessionContext) request.getSession().getAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE);
			
			//INIZIO *** MODIFICA IP ***
			String[] clientIpHeaders = configuration.getClientIpHeaders();
			String ipAddress = "";
			ipAddress = TradeStatisticUtils.getClientIP(clientIpHeaders, request);
			if (ipAddress != null && !ipAddress.isEmpty()) {
				ctx.ipAddress = ipAddress;
			}
			//FINE.
			
			bookingSession.changeSearchElementDate(ctx, searchElementIndex, dateChoiceIndex);
			int middleindex = ctx.availableFlights.getTabs().get(searchElementIndex).getFlightTabs().size() / 2;
			BigDecimal selectedPrice = ctx.availableFlights.getTabs().get(searchElementIndex).getFlightTabs().get(middleindex).getPrice();
			
			response.setContentType("application/json");
			
			if (ctx.cug == BookingSearchCUGEnum.MILITARY || ctx.cug == BookingSearchCUGEnum.FAMILY || ctx.cug == BookingSearchCUGEnum.YOUTH){
				if (!isRibbonFareMatchingResults(ctx, searchElementIndex, selectedPrice))
					json.key("solutionNotFound").value("ribbonMismatch");
			}
			
			if (ctx.cug == BookingSearchCUGEnum.YOUTH) {
				if (!ctx.youthSolutionFound) {
					json.key("solutionNotFound").value("youth");
				}
			} else if (ctx.cug == BookingSearchCUGEnum.FAMILY) {
				if (!ctx.familySolutionFound) {
					json.key("solutionNotFound").value("family");
				}
			} else if (ctx.cug == BookingSearchCUGEnum.MILITARY) {
				if (!ctx.militarySolutionFound) {
					json.key("solutionNotFound").value("allSolutions");
					json.key("redirect").value(errorFunctionalityPagePlain);
				} 
			} else if (ctx.cug == BookingSearchCUGEnum.ADT) {
				if (ctx.availableFlights.getRoutes().isEmpty()) {
					json.key("solutionNotFound").value("allSolutions");
					json.key("redirect").value(errorFunctionalityPagePlain);
				}
			}
			writeAnalyticsInfo(ctx, json);
			json.endObject();
		}catch (Exception e) {
			logger.error("Unexpected error: ", e);
			if (!response.isCommitted()) {
				response.setContentType("application/json");
				try {
					json.key("redirect").value(genericErrorPagePlain);
					json.endObject();
				} catch (JSONException e1) {
					logger.error("Error building JSON");
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
				}
			}
		}
	}
	
	private void writeAnalyticsInfo(BookingSessionContext ctx, TidyJSONWriter json) throws JSONException{
		json.key("analytics_flightInfo").object();
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat(HYPHEN_FLIGHT_DATE_FORMAT);
			dateFormat.setLenient(false);
			DecimalFormat format = new DecimalFormat("#0.00");
			DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
			otherSymbols.setDecimalSeparator('.');
			otherSymbols.setCurrencySymbol(""); 
			format.setDecimalFormatSymbols(otherSymbols);
			format.setGroupingUsed(false);
			format.setPositivePrefix("");
			format.setNegativePrefix("");
			
			int searchElementIndex= 0;
			String[] prefixes = {"dep", "ret"};
			for(String prefix : prefixes){
				if(ctx.availableFlights.getRoutes().size() > searchElementIndex && ctx.availableFlights.getRoutes().get(searchElementIndex) != null){
					json.key(prefix + "Hours").value("");
					json.key(prefix + "Minutes").value("");
					json.key(prefix + "Brand").value("");
					json.key(prefix + "FlightNumber").value("");
					json.key(prefix + "cost").value("");
					json.key(prefix + "FareBasis").value("");
					String nuovaData = "";
					FlightData genericFlight = ctx.availableFlights.getRoutes().get(searchElementIndex).getFlights().get(0);
					if(genericFlight instanceof ConnectingFlightData){
						nuovaData = dateFormat.format(((DirectFlightData) ((ConnectingFlightData) genericFlight).getFlights().get(0)).getDepartureDate().getTime());
					}
					else if (genericFlight instanceof DirectFlightData){
						nuovaData = dateFormat.format(((DirectFlightData) genericFlight).getDepartureDate().getTime());
					}
					json.key(prefix + "Date").value(nuovaData);
					searchElementIndex++;
				}
			}
			BigDecimal fare = BigDecimal.ZERO;
			BigDecimal taxes = BigDecimal.ZERO;
			BigDecimal surcharges = BigDecimal.ZERO;
			BigDecimal totalPrice = BigDecimal.ZERO;
			json.key("fare").value(format.format(fare));
			json.key("taxes").value(format.format(taxes));
			json.key("surcharges").value(format.format(surcharges));
			json.key("totalPrice").value(format.format(totalPrice));
			/*TODO tasso di cambio?*/
			json.key("totalPriceEuro").value(format.format(totalPrice));
			String total = "";
			if(ctx.grossAmount != null){
				total = format.format(total);
			}
			json.key("totalPrice").value(total);
			
			json.endObject();
		}
		catch(Exception e){
			json.endObject();
			throw e;
		}
		
	}
	
	private boolean isRibbonFareMatchingResults(BookingSessionContext ctx, int searchElementIndex, BigDecimal selectedPrice){
		logger.debug("isRibbonMatchingResults. Selected Ribbon Price: "+selectedPrice);
		boolean ribbonMatch = false;
		double minPrice = Double.MAX_VALUE;
		if (ctx.availableFlights.getRoutes() != null && ctx.availableFlights.getRoutes().get(searchElementIndex) != null){
			for (FlightData flightData : ctx.availableFlights.getRoutes().get(searchElementIndex).getFlights()){
				if (flightData.getBrands() != null){
					for (BrandData brandData : flightData.getBrands()){
						if (brandData.isBestFare()){
							logger.debug("isRibbonMatchingResults. AvailableFlights BestFare: "+brandData.getGrossFare());
							if (brandData.getGrossFare().compareTo(selectedPrice) == 0)
								ribbonMatch = true;
							break;
						}
						if (brandData.getGrossFare() != null && brandData.getGrossFare().compareTo(new BigDecimal(0)) != 0 
								&& brandData.getGrossFare().doubleValue() < minPrice)
							minPrice = brandData.getGrossFare().doubleValue();
					}
				}
			}
		}
		logger.debug("isRibbonMatchingResults. MinPrice: [" + minPrice + "]");
		logger.debug("isRibbonMatchingResults. selectedPrice.doubleValue(): [" + selectedPrice.doubleValue() + "]");
		if (!ribbonMatch && (minPrice == selectedPrice.doubleValue()))
			ribbonMatch = true;
		return ribbonMatch;
	}
}