package com.alitalia.aem.business.web.booking.controller;

import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.render.PassengerRender;
import com.alitalia.aem.common.data.home.PassengerBaseData;

import java.util.LinkedList;
import java.util.List;


public class BookingConferma extends BookingSessionGenericController {
	
	private boolean showFareRules;
	private boolean showCallCenterMsg;
	private List<PassengerRender> passengers;
	private String paymentType = "";
	
	@Override
	public void activate() throws Exception {
		super.activate();
		try {
			if (isIllegalCTXState(getResponse())){
				return;
			}

			showFareRules = ctx.searchKind != BookingSearchKindEnum.MULTILEG;

			if (ctx.paymentData != null){
				paymentType = ctx.paymentData.getType().value();
			}

			
			String showCallCenterMsgString = getRequest().getParameter("showCallCenterMsg");
			if (showCallCenterMsgString != null && !showCallCenterMsgString.equals("")) {
				showCallCenterMsg = Boolean.parseBoolean(showCallCenterMsgString);
			} else {
				showCallCenterMsg = false;
			}
			
			/*For passenger's second name*/
			passengers = new LinkedList<PassengerRender>();
			if(ctx.prenotation != null && ctx.prenotation.getPassengers() != null){
				for (PassengerBaseData pax : ctx.prenotation.getPassengers()){
					passengers.add(new PassengerRender(pax));
				}
			}
			
		}catch (Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}
	}

	public boolean getShowFareRules() {
		return showFareRules;
	}
	
	public boolean getShowCallCenterMsg() {
		return showCallCenterMsg;
	}

	public List<PassengerRender> getPassengers() {
		return passengers;
	}
	
	public String getPaymentType() {
		return paymentType;
	}
}