package com.alitalia.aem.business.web.booking.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BookingDatiPrefooter extends BookingSessionGenericController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private Boolean isSecureFlight;
	private Boolean isSecureFlightESTA;
	private Boolean isApis;
	private Boolean isApisCanada;
	
	@Override
	public void activate() throws Exception {
		super.activate();
		
		try {
			
			if (ctx != null) {
				this.isSecureFlight = ctx.isSecureFlight;
				this.isSecureFlightESTA = ctx.isSecureFlightESTA;
				this.isApis = ctx.isApis;
				this.isApisCanada = ctx.isApisCanada;
			}
			
		} catch(Exception e) {
			logger.error("Unexpected error: ", e);
			throw (e);
		}

	}
	
	public Boolean getIsSecureFlight() {
		return isSecureFlight;
	}
	
	public Boolean getIsSecureFlightESTA() {
		return isSecureFlightESTA;
	}
	
	public Boolean getIsApis() {
		return isApis;
	}
	
	public Boolean getIsApisCanada() {
		return isApisCanada;
	}
	
}
