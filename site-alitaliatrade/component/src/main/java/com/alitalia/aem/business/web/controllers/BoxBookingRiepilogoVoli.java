package com.alitalia.aem.business.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.controller.BookingSessionGenericController;
import com.alitalia.aem.business.web.booking.model.FlightSelection;
import com.alitalia.aem.business.web.booking.render.AvailableFlightsSelectionRender;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.day.cq.i18n.I18n;

public class BoxBookingRiepilogoVoli extends BookingSessionGenericController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private BookingSearchKindEnum searchKind;
	private List<AvailableFlightsSelectionRender> flightSelectionsDetails;
	
	@Override
	public void activate() throws Exception {
		super.activate();
		
		try {
			if (isIllegalCTXState(getResponse())) {
				return;
			}

			SlingHttpServletRequest request = getRequest();
			ResourceBundle resourceBundle = request.getResourceBundle(
					LocalizationUtils.getLocaleByResource(request.getResource(), LocalizationUtils.getLanguage(ctx.locale.toLanguageTag())));
			final I18n i18n = new I18n(resourceBundle);
			
			this.searchKind = ctx.searchKind;
			
			this.flightSelectionsDetails = new ArrayList<AvailableFlightsSelectionRender>();
			for (FlightSelection flightSelection : ctx.flightSelections) {

                Boolean isSoggettoAppGov=computeIsSoggettoAppGov(obtainFlightNumbers(flightSelection.getFlightData()));
                flightSelection.getFlightData().setIsSoggettoApprovazioneGovernativa(isSoggettoAppGov);

				this.flightSelectionsDetails.add(
						new AvailableFlightsSelectionRender(flightSelection.getFlightData(),
								ctx.locale, i18n, ctx.isRefreshed, null, null, ctx));
			}
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
		}
	}

    private Boolean computeIsSoggettoAppGov(String numeroVolo) {
        String[] arrayVolo =configuration.getListaVoliSoggettiApprovazioneGovernativa().split(";");
        for (int i=0; i<arrayVolo.length; i++){
            if (arrayVolo[i].equals(numeroVolo)) {return true;}
        }
        return false;
    }

    private String obtainFlightNumbers(FlightData flightData) {
        String result = "";
        //FlightData flightData = flightSelection.getFlightData();
        if (flightData instanceof DirectFlightData) {
            DirectFlightData directFlightData = (DirectFlightData) flightData;
            return directFlightData.getCarrier() + directFlightData.getFlightNumber();
        } else {
            ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
            for (FlightData flight : connectingFlightData.getFlights()) {
                DirectFlightData directFlightData = (DirectFlightData) flight;
                String number = directFlightData.getCarrier() + directFlightData.getFlightNumber();
                if (!result.equals("")) {
                    result += ", " + number;
                } else {
                    result = number;
                }
            }
        }
        return result;
    }

	public BookingSearchKindEnum getSearchKind() {
		return searchKind;
	}
	
	public List<AvailableFlightsSelectionRender> getFlightSelectionsDetails() {
		return flightSelectionsDetails;
	}
	
}
