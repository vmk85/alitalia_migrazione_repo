package com.alitalia.aem.business.web.controllers;

import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUse;

public class LoginTitle extends WCMUse {

	private String headTitle;
	private String title; 
	private String subTitle;
	
	@Override
    public void activate() throws Exception {
    	
		ValueMap properties = getProperties();
		
		headTitle = 
				properties.get("headTitle", String.class) != null ? 
						properties.get("headTitle", String.class) : "";
		title = 
				properties.get("title", String.class) != null ? 
						properties.get("title", String.class) : "";
		subTitle = 
				properties.get("subTitle", String.class) != null ? 
						properties.get("subTitle", String.class) : "";	
	}

	public String getHeadTitle() {
		return headTitle;
	}

	public String getTitle() {
		return title;
	}

	public String getSubTitle() {
		return subTitle;
	}
	
}
