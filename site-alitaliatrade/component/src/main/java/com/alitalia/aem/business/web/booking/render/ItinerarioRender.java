package com.alitalia.aem.business.web.booking.render;

import java.util.List;

public class ItinerarioRender {
	
	private List<TrattaRender> tratte;
	private Boolean voloDiretto;

	public ItinerarioRender(List<TrattaRender> tratte) {
		this.tratte = tratte;
	}

	public List<TrattaRender> getTratte() {
		return tratte;
	}

	public void setTratte(List<TrattaRender> tratte) {
		this.tratte = tratte;
	}
	
	public Boolean getVoloDiretto() {
		return voloDiretto;
	}

	public void setVoloDiretto(Boolean voloDiretto) {
		this.voloDiretto = voloDiretto;
	}
	
}
