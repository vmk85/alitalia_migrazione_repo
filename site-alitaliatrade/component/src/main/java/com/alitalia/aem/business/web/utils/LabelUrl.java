package com.alitalia.aem.business.web.utils;

public class LabelUrl {

	private String label;
	private String url;
	private boolean isSelected;
	private String additionalInfo;
	
	public LabelUrl() {
	
	}
	
	public LabelUrl(String label, String url) {
		this.label = label;
		this.url = url;
	}
	
	public LabelUrl(String label, String url, boolean isSelected) {
		this.label = label;
		this.url = url;
		this.isSelected = isSelected;
	}
	
	public LabelUrl(String label, String url, String additionalInfo) {
		this.label = label;
		this.url = url;
		this.additionalInfo = additionalInfo;
	}
	
	public LabelUrl(String label, String url, String additionalInfo, boolean isSelected) {
		this.label = label;
		this.url = url;
		this.additionalInfo = additionalInfo;
		this.isSelected = isSelected;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
}
