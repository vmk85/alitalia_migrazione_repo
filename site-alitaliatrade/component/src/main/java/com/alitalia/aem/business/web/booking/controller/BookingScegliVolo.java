package com.alitalia.aem.business.web.booking.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.booking.model.SearchElement;
import com.alitalia.aem.business.web.booking.model.SearchPassengersNumber;
import com.alitalia.aem.business.web.booking.render.DateRender;
import com.alitalia.aem.business.web.utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BookingScegliVolo extends WCMUse {

	private SearchElement outboundSearchElement;
	private SearchElement returnSearchElement;
	private SearchPassengersNumber searchPassengersNumber;
	private DateRender outboundDate;
	private DateRender returnDate;
	private boolean isRoundtrip;
	private boolean multileg;
	private String cug;
	private String linAirportMessage;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void activate() throws Exception {
		
		try{
			BookingSessionContext ctx = (BookingSessionContext) getRequest().getSession().getAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE);
			
			cug = ctx.cug.value();
			multileg = ctx.searchKind == BookingSearchKindEnum.MULTILEG;
			outboundSearchElement = ctx.searchElements.get(0);
			isRoundtrip = ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP;
			searchPassengersNumber = ctx.searchPassengersNumber;
			linAirportMessage = bookingLinAirportMessage(ctx);
			
			outboundDate = new DateRender(outboundSearchElement.getDepartureDate());
			returnDate = null;
			if(isRoundtrip){
				returnSearchElement = ctx.searchElements.get(1);
				returnDate = new DateRender(returnSearchElement.getDepartureDate());
			}
		}catch(Exception e){
			logger.error("Unexpected error in controller BookingScegliVolo: ", e);
		}
	}

	
	public SearchElement getOutboundSearchElement() {
		return outboundSearchElement;
	}

	// genero l'html che mostrerà l'alert nella flight-select se isLinAirportClosedMessage = true
	private String bookingLinAirportMessage(BookingSessionContext ctx) {
		String linMessage = "";
		if (isLinAirportClosedMessage(ctx)) {
			linMessage += "<div class=\"inner-container\" id=\"alertChiusuraLin\">\n" +
					"<h3 class=\"bigger-text color-darkergreen\">\n" +
					"\t\t\t\t\t\t\tDal 27 luglio al 27 ottobre 2019 l’aeroporto di Milano Linate sarà chiuso. Riportiamo qui di seguito le migliori soluzioni di viaggio per volare su Milano Malpensa e Bergamo Orio al Serio.\n" +
					"\t\t\t\t\t\t</h3>\n" +
					"</div>";
		}
		return linMessage;
	}

	//in caso di ricerca "MIL" e periodo chiusura aeroporto Linate, ritorno true per mostrare un alert nella flight-select (vedi bookingLinAirportMessage)
	private boolean isLinAirportClosedMessage(BookingSessionContext ctx) {
		boolean retval = false;

		String date_1 = "27-07-2019";
		String date_2 = "27-10-2019";
		long milliseconds = 0;
		long milliseconds_2 = 0;
		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		try {
			Date d_1 = f.parse(date_1);
			milliseconds = d_1.getTime();
			Date d_2 = f.parse(date_2);
			milliseconds_2 = d_2.getTime();

		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (ctx.searchElements != null) {
			int i = 0;
			for (SearchElement searchElement : ctx.searchElements) {
				i++;
				long depDate = searchElement.getDepartureDate().getTimeInMillis();
				if (("MIL".equals(searchElement.getFrom().getAirportCode()) || "MIL".equals(searchElement.getTo().getAirportCode())) && (depDate >= milliseconds) && (depDate <= milliseconds_2)) {
					return true;
				}
			}
		}
		return retval;
	}

	public SearchElement getReturnSearchElement() {
		return returnSearchElement;
	}
	
	public SearchPassengersNumber getSearchPassengersNumber() {
		return searchPassengersNumber;
	}

	public DateRender getOutboundDate() {
		return outboundDate;
	}

	public DateRender getReturnDate() {
		return returnDate;
	}

	public boolean isRoundtrip() {
		return isRoundtrip;
	}

	public boolean getMultileg() {
		return multileg;
	}
	
	public String getCug(){
		return cug;
	}

	public String getLinAirportMessage() { return linAirportMessage; }
	
}
