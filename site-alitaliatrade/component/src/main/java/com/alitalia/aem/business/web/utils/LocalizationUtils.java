package com.alitalia.aem.business.web.utils;

import java.util.*;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.*;
import com.alitalia.aem.business.web.statistics.utils.*;
import com.alitalia.aem.common.messages.home.LocateRequest;
import com.alitalia.aem.common.messages.home.LocateResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.egon.delegate.EgonDelegate;
import com.day.cq.wcm.api.Page;

public class LocalizationUtils {

	private static String languageStatic;
	private static Logger logger = LoggerFactory.getLogger(LocalizationUtils.class);

	public static Locale getLocaleByResource(Resource resource, String defaultLanguage) {
		boolean flag = true;
		Resource currentResource = resource;
		Locale locale = null;
		while (flag) {
			if (currentResource.getResourceType().equalsIgnoreCase("cq:Page")) {
				Page p = currentResource.adaptTo(Page.class);
				locale = p.getLanguage(false);
				flag = false;
			} else {
				currentResource = currentResource.getParent();
				flag = !(currentResource == null);
			}
		}
		if (locale == null) {
			locale = new Locale(defaultLanguage);
		}
		return locale;
	}

	public static String getLanguage(String languageTag) {
		String[] elements = languageTag.split("-");
		String language = elements[0];
		if ("und".equals(language)) {
			return "";
		} else {
			return language;
		}
	}

	/**
	 * Usando il ResourceResolver fornito, individua il mercato relativo al
	 * country code specificato e determina il language code della lingua di
	 * default per il mercato, opzionalmente applicando anche le preferenze di
	 * lingua specificate a livello di singole citta'.
	 * 
	 * @param resolver
	 *            il ResourceResolver da usare per la risoluzione dei path.
	 * @param countryCode
	 *            il country code del mercato.
	 * @param cityCode
	 *            il city code della citta' specifica, o null.
	 * @return il language code determinato, o null in caso di errori.
	 */
	public static String findResourceDefaultLanguage(ResourceResolver resourceResolver, String countryCode,
			String cityCode) {
		Resource marketContentResource = getMarketResource(resourceResolver, countryCode, true);
		if (marketContentResource == null) {
			logger.info("Cannot find market resource for country code {}", countryCode);
			return null;
		}
		String languageCode = null;
		try {
			ValueMap marketProperties = marketContentResource.getValueMap();
			languageCode = (String) marketProperties.getOrDefault("defaultLanguage", null);
			if (languageCode == null) {
				logger.info("defaultLanguage is null for market {}", countryCode);
				List<String> marketLanguages = getListMarketLanguages(resourceResolver, countryCode);
				if (marketLanguages != null && marketLanguages.size() > 0) {
					languageCode = marketLanguages.get(0);
					logger.info("Language set to the first available found: {}", languageCode);
				} else {
					throw new Exception("No default language set and no available languages found.");
				}
			}
			if (cityCode != null) {
				// process city-specific market default language settings
				Resource marketCitiesLanguage = marketContentResource.getChild("market-cities-language");
				if (marketCitiesLanguage != null) {
					Map<String, Object> defaultCityLanguage = MultifieldUtils
							.fromJcrMultifieldToMapArrayObject(marketCitiesLanguage, "items");
					if (defaultCityLanguage != null) {
						for (Map.Entry<String, Object> setting : defaultCityLanguage.entrySet()) {
							String settingCity = (String) ((Map) setting.getValue()).get("city");
							String settingLanguage = (String) ((Map) setting.getValue()).get("language");
							if (cityCode.equalsIgnoreCase(settingCity)) {
								languageCode = settingLanguage;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error finding default language code.", e);
			return null;
		}
		return languageCode;
	}

	/**
	 * Usando il ResourceResolver fornito, individua la risorsa corrispondente
	 * al mercato per il country code specificato e restituisce la relativa
	 * risorsa.
	 * 
	 * @param resolver
	 *            il ResourceResolver da usare per la risoluzione del path.
	 * @param countryCode
	 *            il codice country da usare per la costruzione del path.
	 * @param jcrContent
	 *            indica se individuare il nodo jcr:content o il nodo market
	 *            stesso.
	 * @return la risorsa risolta, o null.
	 */
	static public Resource getMarketResource(ResourceResolver resolver, String countryCode, boolean jcrContent) {
		Resource resource = null;
		try {
			String path = Constants.CONTENT_PATH + "/" + countryCode.toLowerCase();
			if (jcrContent) {
				path += "/jcr:content";
			}
			logger.debug("Path resource: {}", path);
			resource = resolver.getResource(path);
		} catch (Exception e) {
			logger.debug("Error resolving the market node", e);
		}
		return resource;
	}

	/**
	 * Usando il ResourceResolver fornito, individua la risosra corrispondente
	 * al mercato per il country code specificato, enumera i nodi sottostanti
	 * corrispondenti ai siti localizzati disponibili e restituisce l'elenco dei
	 * relativi language code.
	 * 
	 * @param resolver
	 *            il ResourceResolver da usare per la risoluzione del path.
	 * @param countryCode
	 *            il codice country da usare per la costruzione del path.
	 * @return la lista dei language code supportati.
	 */
	static public List<String> getListMarketLanguages(ResourceResolver resolver, String countryCode) {
		List<String> languages = new ArrayList<String>();
		try {
			Resource marketResource = getMarketResource(resolver, countryCode, false);
			Iterator<Resource> marketLanguagesIterator = marketResource.getChildren().iterator();
			while (marketLanguagesIterator.hasNext()) {
				Resource marketLanguage = marketLanguagesIterator.next();
				if (marketLanguage != null && !"jcr:content".equals(marketLanguage.getName())) {
					languages.add(getRepositoryPathLanguage(marketLanguage));
				}
			}
		} catch (Exception e) {
			logger.debug("Error resolving the market available languages", e);
		}
		return languages;
	}

	/**
	 * Restituisce il codice language del sito a partire dal repository path
	 * della risorsa specificata.
	 * 
	 * @param resource
	 *            la risorsa di riferimento.
	 * @return il codice language individuato.
	 */
	static public String getRepositoryPathLanguage(Resource resource) {
		String s = resource.getPath().split("\\/")[Constants.REPOSITORY_PATH_LANGUAGE_DEPTH];
		return s;
	}

	static public HashMap<String,String> getUserMarketLanguage(SlingScriptHelper helper, SlingHttpServletRequest request) {

		String language = "it";
		String market = "IT";
		
		String COOKIE_NAME = "alitaliatrade-consumer-geo";
		String cookieValue = null;
		
		HashMap<String,String> result = null;

		try {
			// load configuration
			AlitaliaTradeConfigurationHolder configuration = helper.getService(AlitaliaTradeConfigurationHolder.class);
			EgonDelegate egonDelegate = helper.getService(EgonDelegate.class);
			
			/* Accrocchio per test - START */
			/*result = new HashMap<>();
			String codiceAgenzia = AlitaliaTradeAuthUtils.getProperty(request, "codiceAgenzia");
			logger.debug("Codice agenzia per test: {}", codiceAgenzia);
			if(codiceAgenzia != null){
			
				switch (codiceAgenzia.trim()) {
				case "2021183":
					// francia
					market = "FR";
					language = "en";
					break;
				case "5749004":
					// olanda
					market = "NL";
					language = "en";
					break;
				case "9990717":
					// gran bretagna
					market = "GB";
					language = "en";
					break;
				case "2349100":
					// germania
					market = "DE";
					language = "en";
					break;
				case "7849027":
					// spagna
					market = "ES";
					language = "en";
					break;
				case "0849010":
					// belgio
					market = "BE";
					language = "en";
					break;
				case "5701993":
					// brasile
					market = "BR";
					language = "en";
					break;
				case "5599015":
					// argentina
					market = "AR";
					language = "en";
					break;
				case "1630102":
					// giappone
					market = "JP";
					language = "en";
					break;
				default:
					market = "IT";
					language = "it";
					break;
				}
			}
			
			logger.debug("Mercato (Lingua): {} ({})", new Object[] { market, language });
			
			result.put("language", language);
			result.put("market",market);*/
			/* Accrocchio per test - STOP */

			cookieValue = CookieUtils.getCookieValue(request, COOKIE_NAME);
			result = CookieUtils.fromCookieString(cookieValue);
			
			if (result == null) {
				
				result = new HashMap<>();
			
				String[] clientIpHeaders = configuration.getClientIpHeaders();
				String ipAddress = TradeStatisticUtils.getClientIP(clientIpHeaders, request);
				//ip per Test
				//String ipAddress = "80.72.160.92"; // Roma
				//String ipAddress = "93.51.235.42"; // Milano
				//String ipAddress = "212.19.44.195"; // Germany
				logger.debug("Client ip (remote): {} ({})", new Object[] { request.getRemoteAddr(), ipAddress });
				String sid = "AlitaliaGeolocalizationManager"; // cambiare nome se possibile
				String tid = IDFactory.getTid();
	
				LocateRequest locateRequest = new LocateRequest();
				locateRequest.setSid(sid);
				locateRequest.setTid(tid);
				locateRequest.setWpuser(configuration.getEgonUser());
				locateRequest.setWppasw(configuration.getEgonPassword());
				locateRequest.setCdxipa(ipAddress);
				logger.debug("Locate request: {}", locateRequest.toString());
				LocateResponse locateResponse = egonDelegate.locate(locateRequest);
				logger.debug("Locate response: {}", locateResponse.toString());
				//market = null;
				market = locateResponse.getCdxiso();
	
				logger.debug("Data from EGON for IP {}: countryCode={}", new Object[] { ipAddress, market });
	
				if (market == null || market.length() == 0) {
					// invalid EGON data
					logger.warn("Invalid data received from EGON, error {} {}", locateResponse.getDsxerr(),
							locateResponse.getWp9STC());
					market = "GB";
				}
	
				switch (market) {
				case "IT":
					language = "it";
					break;
				case "FR":
					language = "en";
					break;
				case "NL":
					language = "en";
					break;
				case "GB":
					language = "en";
					break;
				case "DE":
					language = "en";
					break;
				case "ES":
					language = "en";
					break;
				case "BE":
					language = "en";
					break;
				case "BR":
					language = "en";
					break;
				case "AR":
					language = "en";
					break;
				case "JP":
					language = "en";
					break;
				default:
					language = "it";
					market = "IT";
					break;
				}
				
				result.put("language", language);
				result.put("market",market);
			}
			
			languageStatic = result.get("language");
			
		} catch (Exception e) {
			logger.error("Invalid data received from EGON", e);
		}
		
		return result;
	}
	
	public static String getLanguageStatic()
	{
		return languageStatic;
	}
}
