package com.alitalia.aem.business.web.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.xml.datatype.Duration;

import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.controller.BookingSessionGenericController;
import com.alitalia.aem.business.web.booking.render.ItinerarioRender;
import com.alitalia.aem.business.web.booking.render.TrattaRender;
import com.alitalia.aem.business.web.flightinfo.model.DatiRicercaOrariVoli;
import com.alitalia.aem.common.data.home.FlightDetailsModelData;
import com.alitalia.aem.common.data.home.FlightItinerayData;
import com.alitalia.aem.common.data.home.ItineraryModelData;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightDetailsResponse;
import com.alitalia.aem.web.component.flightstatus.delegate.FlightStatusDelegate;


public class BookingStatoVoli extends BookingSessionGenericController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private DatiRicercaOrariVoli datiRicercaOrariVoli;
	private List<ItinerarioRender> itinerariOrariVoli;
	private Boolean showDetail;
	private Boolean hasItinerary;

	@SuppressWarnings("unchecked")
	@Override
	public void activate() throws Exception {
		try {
			HttpSession session = getRequest().getSession();
			datiRicercaOrariVoli = (DatiRicercaOrariVoli) session.getAttribute("datiRicercaOrariVoli");
			itinerariOrariVoli = createItineraryRender((List<ItineraryModelData>) session.getAttribute("itinerariOrariVoli"), (String)session.getAttribute("fasciaOraria"));
			showDetail = isMinusTwoPlusSevenDay((String) session.getAttribute("dataRicerca"));
		} catch (Exception e) {
			logger.error("Unexpected exception:", e);
			throw (e);
		}
	}

	public DatiRicercaOrariVoli getDatiRicercaOrariVoli() {
		return datiRicercaOrariVoli;
	}

	public List<ItinerarioRender> getItinerariOrariVoli() {
		return itinerariOrariVoli;
	}

	public Boolean getShowDetail() {
		return showDetail;
	}
	
	public Boolean hasItinerary() {
		return hasItinerary;
	}

	private List<ItinerarioRender> createItineraryRender(List<ItineraryModelData> itinerari, String fasciaOraria) {
		itinerariOrariVoli = new ArrayList<ItinerarioRender>();
		for (ItineraryModelData itinerario : itinerari) {
			List<FlightItinerayData> flights = itinerario.getFlights();
			List<TrattaRender> tratte = new ArrayList<TrattaRender>();

			List<Boolean> frequenza = new ArrayList<Boolean>();
			frequenza.add(itinerario.getFrequency().getMonday());
			frequenza.add(itinerario.getFrequency().getTuesday());
			frequenza.add(itinerario.getFrequency().getWednesday());
			frequenza.add(itinerario.getFrequency().getThursday());
			frequenza.add(itinerario.getFrequency().getFriday());
			frequenza.add(itinerario.getFrequency().getSaturday());
			frequenza.add(itinerario.getFrequency().getSunday());
			Boolean check = true;
			Boolean addFlight = false;
			
			for (FlightItinerayData volo : flights) {
				
				if (check && checkFasciaOraria (volo.getDepartureTime().getHours() ,fasciaOraria)) {
					addFlight = true;
				}
				check = false;
				String aereoportoPartenza = volo.getDeparture();
				String aereoportoArrivo = volo.getArrival();
				String oraPartenza = getOrarioFormattato(volo.getDepartureTime());
				String oraArrivo = getOrarioFormattato(volo.getArrivalTime());
				String codiceVolo = volo.getFlightNumber();
				String vector = volo.getVector();
				String tempoViaggio = volo.getFlightLength().getHours() + "h" + volo.getFlightLength().getMinutes() + "'";
				String miglia = "";//getMile(volo.getArrivalDate(), volo.getFlightNumber(), volo.getVector());
				String codiceAereomobile = volo.getFlightDetails()!=null?volo.getFlightDetails().getAirCraft():"";
				TrattaRender tratta = new TrattaRender (aereoportoPartenza, aereoportoArrivo, oraPartenza, oraArrivo, codiceVolo, tempoViaggio, miglia, codiceAereomobile, frequenza, vector);
				tratte.add(tratta);
			}
			ItinerarioRender itinerarioRender = new ItinerarioRender(tratte);
			Boolean voloDiretto = (tratte.size() > 1) ? false : true;
			itinerarioRender.setVoloDiretto(voloDiretto);
			if (addFlight) {
				itinerariOrariVoli.add(itinerarioRender);
			}
		}
		hasItinerary = (itinerariOrariVoli.size()>0)?true:false;
		return itinerariOrariVoli;
	}

	private boolean isMinusTwoPlusSevenDay(String day) throws ParseException {

		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(new Date());
		cal1.add(Calendar.DATE, -3);

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(new Date());
		cal2.add(Calendar.DATE, 6);

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
		df.setLenient(false);
		Date date = df.parse(day);
		Calendar cal = Calendar.getInstance();

		cal.setTime(date);

		return cal.after(cal1) && cal.before(cal2);

	}

	private String getOrarioFormattato(Duration orario) {
		String ora = String.valueOf(orario.getHours());
		String minuti = String.valueOf(orario.getMinutes());
		minuti = (minuti.length() > 1 ? "" : "0") + minuti;
		return ora + ":" + minuti;
	}
	
	private Boolean checkFasciaOraria (int partenza,  String fasciaOraria) {
		switch (fasciaOraria) {
		case "mattina":
			return 8<=partenza && partenza<12;
		case "pomeriggio":
			return 12<=partenza && partenza<19;
		case "sera":
			return 19<=partenza || partenza<=8;
		default:
			return true;
		}
	}

	private String getMile (Calendar cal, String flightNum, String vector) {
		SlingScriptHelper helper = getSlingScriptHelper();
		FlightStatusDelegate flightStatusDelegate = helper.getService(FlightStatusDelegate.class);
		RetrieveFlightDetailsResponse serviceResponseDetail = null;
		RetrieveFlightDetailsRequest serviceRequestDetail = new RetrieveFlightDetailsRequest ();
		FlightDetailsModelData flightDetailsModelData = new FlightDetailsModelData();
		flightDetailsModelData.setFlightDate(cal);
		flightDetailsModelData.setFlightNumber(flightNum);
		flightDetailsModelData.setVector(vector);
		serviceRequestDetail.setFlightDetailsModelData(flightDetailsModelData);
		serviceResponseDetail = flightStatusDelegate.retrieveFlightDetails(serviceRequestDetail);
		return "a";//serviceResponseDetail.getFlightDetailsModelData().getFlySegments().get(0).getMilesFromStart().toString();

	}
}
