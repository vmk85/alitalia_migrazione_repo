package com.alitalia.aem.business.web.flightinfo.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.common.data.home.FlightInfoData;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoRequest;
import com.alitalia.aem.common.messages.home.RetrieveFlightInfoResponse;
import com.alitalia.aem.web.component.flightstatus.delegate.FlightStatusDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "flightinfo" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.")
})
@SuppressWarnings("serial")
public class FlightInfoServlet extends GenericFormValidatorServlet {
	
	private ComponentContext componentContext;
	
	@Reference
	private FlightStatusDelegate flightStatusDelegate;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		Validator validator = new Validator();
		
		// validate flight number
		validator.addDirectParameter("flightnum",
				request.getParameter("flightnum"),
				Constants.MESSAGE_FLIGHTNUMBER_NOT_VALID, "isFlightNumber");
		
		// validate departure time
		validator.addDirectParameter("infodeptime",
				request.getParameter("infodeptime"),
				Constants.MESSAGE_DATE_NOT_VALID, "isDate",
				"isMinusTwoPlusSevenDayIncluded");
		
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		
		RetrieveFlightInfoResponse serviceResponse = null;
		
		response.setContentType("application/json");
		TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
		
		try {
			RetrieveFlightInfoRequest serviceRequest = new RetrieveFlightInfoRequest();
			FlightInfoData flightInfoData = new FlightInfoData();
			flightInfoData.setFlightNumber(request.getParameter("flightnum"));
			flightInfoData.setVector(request.getParameter("initials"));
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
			df.setLenient(false);
			
			Date departureDate = df.parse(request.getParameter("infodeptime"));
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(departureDate);
			flightInfoData.setDepartureDateTime(calendar);
			
			serviceRequest.setFlightInfoData(flightInfoData);
			
			serviceResponse = flightStatusDelegate.retrieveFlightInfo(serviceRequest);
		
			out.object();
			
			 if (serviceResponse != null && serviceResponse.getFlightInfoData() != null) {
				 if (serviceResponse.getFlightInfoData().getErrorDescription() != null &&
						 serviceResponse.getFlightInfoData().getErrorDescription().equals("INVALID FLIGHT NUMBER")) {
					 out.key("result").value(false);
					 out.key("message").value(Constants.MESSAGE_FLIGHTNUMBER_NOT_VALID);
					}
				 else{
					 out.key("result").value(true);
					 out.key("flightNumber").value("AZ"+serviceResponse.getFlightInfoData().getFlightNumber());
					 out.key("departure").value(serviceResponse.getFlightInfoData().getDepartureAirport().getLocationCode());
					 out.key("arrival").value(serviceResponse.getFlightInfoData().getArrivalAirport().getLocationCode());
					 out.key("departureTime").value(getDate(serviceResponse.getFlightInfoData().getDepartureGapTime()));
					 out.key("arrivalTime").value(getDate(serviceResponse.getFlightInfoData().getArrivalGapTime()));
					 out.key("departureEffectiveTime").value(getDate(serviceResponse.getFlightInfoData().getDepartureDateTime()));
					 out.key("arrivalEffectiveTime").value(getDate(serviceResponse.getFlightInfoData().getArrivalDateTime()));
					 out.key("flightDetails").value(getState(serviceResponse.getFlightInfoData()));
				 }
			 }
			 else{
				 out.key("result").value(false);
				 out.key("message").value(Constants.MESSAGE_SERVICE_ERROR);
			 }
			out.endObject();

		} catch (Exception e) {
			logger.error("unexpected error: ", e);
			String failurePage = (String) componentContext.getProperties().get("failure.page");
			try {
				out.key("redirect").value(failurePage);
				out.endObject();
			} catch (JSONException e1) {
				logger.error("Error creating JSON: ", e);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
	}
	
	private String getDate (Calendar cal) {
		Date date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH.mm");
        return sdf.format( date );
	}
	
	private String getState (FlightInfoData flightInfo) {
		String state = "";
		String statoPartenza = flightInfo.getDepartureDetails().getCodeStatus();
		String statoArrivo = flightInfo.getArrivalDetails().getCodeStatus();
		HashMap <String,String> errorState = new HashMap <String,String> ();
		errorState.put("FLT CNLD FOR FLT/DATE", "Cancellato");
		errorState.put("INVLD FLT NB", "Numero del volo non valido");
		errorState.put("FLT NOOP FOR FLT/DAT", "Non operativo");
		
		if (statoArrivo != null) {
			if (statoArrivo.equals("A")){
				state = "Atterrato";
			}
			else if (statoPartenza.equals("E") 
					&& statoArrivo.equals("E")) {
				state = "Prima della partenza";
			}
			else if ((statoPartenza.equals("A") && statoArrivo.equals("E"))
					|| (statoPartenza.equals("T") && (statoArrivo.equals("T") || statoArrivo.equals("S")))){
				state = "In volo";
			}
			else if (statoArrivo.equals("E") && (statoPartenza.equals("T") || statoPartenza.equals("S"))) {
				state = "In arrivo";
			}
		}
		else if (errorState.get(flightInfo.getErrorDescription())!=null){
			state=errorState.get(flightInfo.getErrorDescription());
		}
		
		return state;
	}
	
}
