package com.alitalia.aem.business.web.booking.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.model.FlightSelection;
import com.alitalia.aem.business.web.booking.render.AvailableFlightsSelectionRender;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;

public class BookingDati extends BookingSessionGenericController {
	
	private static int MIN_YEAR_ADT;
	private static int MAX_YEAR_ADT;
	private static int MIN_YEAR_CHD;
	private static int MAX_YEAR_CHD;
	private static int MIN_YEAR_INF;
	private static int MAX_YEAR_INF;
	
	private String inputSelectDefaultValue;
	private String[] numAdulti;
	private String[] numBambini;
	private String[] numNeonati;
	private Boolean isSecureFlight;
	private Boolean isSecureFlightESTA;
	private Boolean isApis;
	private Boolean isApisCanada;
	private Boolean isTariffaLight;
	private List<AvailableFlightsSelectionRender> flightSelectionsDetails;
	private List<CountryData> countries;
	private List<PhonePrefixData> phonePrefixes;
	private List<MealData> mealTypes;
	private List<FrequentFlyerTypeData> frequentFlyerTypes;
	private List<Integer> birthDateYearsADT;
	private List<Integer> birthDateYearsCHD;
	private List<Integer> birthDateYearsINF;
	private boolean multileg;
	private String cug;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void activate() throws Exception {
		super.activate();
		
		try {
			if (isIllegalCTXState(getResponse())){
				return;
			}
			
			SlingScriptHelper helper = getSlingScriptHelper();
			AlitaliaTradeConfigurationHolder configuration = helper.getService(AlitaliaTradeConfigurationHolder.class);
			setPropertiesByConfiguration(configuration);
			
			this.inputSelectDefaultValue = Constants.INPUT_SELECT_DEFAULT_VALUE;
			
			this.isSecureFlight = ctx.isSecureFlight;
			this.isSecureFlightESTA = ctx.isSecureFlightESTA;
			this.isApis = ctx.isApis;
			this.isApisCanada = ctx.isApisCanada;
			this.multileg = ctx.searchKind == BookingSearchKindEnum.MULTILEG;
			this.cug = ctx.cug.value();
			this.isTariffaLight = false;
			for (FlightSelection flightSelection : ctx.flightSelections) {
				if (flightSelection.isTariffaLight()) {
					this.isTariffaLight = true;
					break;
				}
			}
			
			int adulti = 0;
			if (ctx.searchPassengersNumber.getNumYoung() > 0) {
				adulti = ctx.searchPassengersNumber.getNumYoung();
			} else {
				adulti = ctx.searchPassengersNumber.getNumAdults();
			}
			int bambini = ctx.searchPassengersNumber.getNumChildren();
			int neonati = ctx.searchPassengersNumber.getNumInfants();
			
			int numeroPasseggeri = 1; // l'applicant (cioe' il primo adulto) è sempre presente
			
			// numAdulti, numBambini e numNeonati sono array la cui lunghezza riflette il numero di
			// passeggeri del relativo tipo, e con in ciascun valore l'indice passegero come stringa
			// (gestendo il caso del primo adulto che e' gestito ad hoc in quanto applicant),
			this.numAdulti = new String[(adulti - 1)];
			for (int i = 0; i < (numAdulti.length); i++) { 
				numeroPasseggeri++;
				numAdulti[i] = Integer.toString(numeroPasseggeri);
			}
			this.numBambini = new String[bambini];
			for (int i = 0; i < numBambini.length; i++) {
				numeroPasseggeri++;
				numBambini[i] = Integer.toString(numeroPasseggeri);
			}
			this.numNeonati = new String[neonati];
			for (int i = 0; i < numNeonati.length; i++) {
				numeroPasseggeri++;
				numNeonati[i] = Integer.toString(numeroPasseggeri);
			}
			
			this.countries = ctx.countries;
			this.phonePrefixes = ctx.phonePrefixes;
			this.mealTypes = ctx.mealTypes;
			this.frequentFlyerTypes = ctx.frequentFlyerTypes;
			
			birthDateYearsADT = new ArrayList<Integer>();
			birthDateYearsCHD = new ArrayList<Integer>();
			birthDateYearsINF = new ArrayList<Integer>();
			
			setBirthDateSelect(birthDateYearsADT,PassengerTypeEnum.ADULT);
			if (numBambini.length > 0) {
				setBirthDateSelect(birthDateYearsCHD,PassengerTypeEnum.CHILD);
			}
			if (numNeonati.length > 0) {
				setBirthDateSelect(birthDateYearsINF,PassengerTypeEnum.INFANT);
			}
			
		} catch(Exception e) {
			logger.error("Unexpected error: ", e);
			throw (e);
		}

	}
	
	public String getInputSelectDefaultValue() {
		return inputSelectDefaultValue;
	}
	
	public Boolean getIsSecureFlight() {
		return isSecureFlight;
	}
	
	public Boolean getIsSecureFlightESTA() {
		return isSecureFlightESTA;
	}
	
	public Boolean getIsApis() {
		return isApis;
	}
	
	public Boolean getIsApisCanada() {
		return isApisCanada;
	}
	
	public Boolean getIsTariffaLight() {
		return isTariffaLight;
	}
	
	public String[] getNumAdulti() {
		return numAdulti;
	}
	
	public String[] getNumBambini() {
		return numBambini;
	}
	
	public String[] getNumNeonati() {
		return numNeonati;
	}
	
	public List<AvailableFlightsSelectionRender> getFlightSelectionsDetails() {
		return flightSelectionsDetails;
	}
	
	public List<CountryData> getCountries() {
		return countries;
	}
	
	public List<MealData> getMealTypes() {
		return mealTypes;
	}

	public List<FrequentFlyerTypeData> getFrequentFlyerTypes() {
		return frequentFlyerTypes;
	}

	public List<PhonePrefixData> getPhonePrefixes() {
		return phonePrefixes;
	}
	
	public List<Integer> getBirthDateYearsADT() {
		return birthDateYearsADT;
	}
	
	public List<Integer> getBirthDateYearsCHD() {
		return birthDateYearsCHD;
	}
	
	public List<Integer> getBirthDateYearsINF() {
		return birthDateYearsINF;
	}
	
	public boolean getMultileg() {
		return multileg;
	}
	
	public String getCug(){
		return cug;
	}
	
	/**
	 * It set the correct possible values for the passenger birth year, 
	 * according to the constraints on the passengers
	 * @param birthDateYears
	 * @param passengerType
	 */
	private void setBirthDateSelect(List<Integer> birthDateYears,
			PassengerTypeEnum passengerType) {
		
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int lowerBound=0;
		int upperBound=0;
		
		if (passengerType == PassengerTypeEnum.ADULT) {
			upperBound = currentYear - BookingDati.MIN_YEAR_ADT;
			lowerBound = currentYear - BookingDati.MAX_YEAR_ADT;
		}
		if (passengerType == PassengerTypeEnum.CHILD) {
			upperBound = currentYear - BookingDati.MIN_YEAR_CHD;
			lowerBound = currentYear - BookingDati.MAX_YEAR_CHD;
		}
		if (passengerType == PassengerTypeEnum.INFANT) {
			upperBound = currentYear - BookingDati.MIN_YEAR_INF;
			lowerBound = currentYear - BookingDati.MAX_YEAR_INF;
		}
		
		for (int y = upperBound; y >= lowerBound; y--) {
			birthDateYears.add(y);
		}
	}

	/**
	 * It set the lower and upper bounds for each passenger, according to configuration.
	 * @param configuration
	 */
	private void setPropertiesByConfiguration(AlitaliaTradeConfigurationHolder configuration) {
		BookingDati.MIN_YEAR_ADT = Integer.parseInt(configuration.getMinYearADT());
		BookingDati.MAX_YEAR_ADT = Integer.parseInt(configuration.getMaxYearADT());
		BookingDati.MIN_YEAR_CHD = Integer.parseInt(configuration.getMinYearCHD());
		BookingDati.MAX_YEAR_CHD = Integer.parseInt(configuration.getMaxYearCHD());
		BookingDati.MIN_YEAR_INF = Integer.parseInt(configuration.getMinYearINF());
		BookingDati.MAX_YEAR_INF = Integer.parseInt(configuration.getMaxYearINF());
	}
	
}
