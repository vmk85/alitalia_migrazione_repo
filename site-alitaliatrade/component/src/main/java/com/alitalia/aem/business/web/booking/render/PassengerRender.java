package com.alitalia.aem.business.web.booking.render;

import java.util.List;
import java.util.stream.Collectors;

import com.alitalia.aem.common.data.home.TicketInfoData;
import com.alitalia.aem.common.data.home.PassengerApisSecureFlightInfoData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PassengerSecureFlightInfoData;

public class PassengerRender {
	
	private String name;
	private String secondName;
	private String lastName;
	private List<String> tickets;
	
	public PassengerRender(PassengerBaseData pax) {
		this.name = pax.getName();
		this.lastName = pax.getLastName();
		if(pax.getTickets() != null){
			this.tickets = pax.getTickets().stream()
					.map(TicketInfoData::getTicketNumber)
					.collect(Collectors.toList());
		}
		if(pax.getInfo() instanceof PassengerSecureFlightInfoData){
			this.secondName = ((PassengerSecureFlightInfoData) pax.getInfo()).getSecondName();
		}
		else if(pax.getInfo() instanceof PassengerApisSecureFlightInfoData){
			this.secondName = ((PassengerApisSecureFlightInfoData) pax.getInfo()).getSecondName();
		} 
		else{
			this.secondName = "";
		}
	}

	public String getName() {
		return name;
	}

	public String getSecondName() {
		return secondName;
	}

	public String getLastName() {
		return lastName;
	}

	public List<String> getTickets() {
		return tickets;
	}

}
