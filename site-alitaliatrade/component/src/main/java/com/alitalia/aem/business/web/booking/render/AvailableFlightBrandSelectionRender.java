package com.alitalia.aem.business.web.booking.render;

import java.math.BigDecimal;
import java.util.Locale;

import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.common.data.home.BrandData;

public class AvailableFlightBrandSelectionRender{

	private BrandData brandData;
	private BrandRender brand;
	private GenericPriceRender flightFare;
	private boolean isBetter;
	private int seatsAvailable;
	private String solutionId;
	private boolean isEnabled;
	private boolean isSelected;
	
	public AvailableFlightBrandSelectionRender(BrandData brandData, Locale locale, boolean isSelected, BookingSessionContext ctx){
		this.brandData = brandData;
		this.brand = null;
		this.flightFare = null;
		this.isBetter = false;
		this.seatsAvailable = 0;
		this.solutionId = "";
		this.isEnabled = true;
		this.isSelected = isSelected;
		init(locale,ctx);
	}
	
	private void init(Locale locale, BookingSessionContext ctx){
		this.brand = new BrandRender(this.brandData,ctx);
		this.flightFare = computeFlightFare(locale);
		this.isBetter = this.brandData.isBestFare();
		this.isEnabled = this.brandData.isEnabled();
		this.seatsAvailable = this.brandData.getSeatsAvailable();
		this.solutionId = computeSolutionId();
	}
	

	public BrandData getBrandData() {
		return brandData;
	}

	public BrandRender getBrand() {
		return brand;
	}

	public GenericPriceRender getFlightFare() {
		return flightFare;
	}
	
	public void setFlightFare(GenericPriceRender flightFare) {
		this.flightFare = flightFare;
	}

	public boolean isBetter() {
		return isBetter;
	}

	public int getSeatsAvailable() {
		return seatsAvailable;
	}

	public String getSolutionId() {
		return solutionId;
	}
	
	public boolean isEnabled() {
		return isEnabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.isEnabled = enabled;
	}
	
	public boolean isSelected() {
		return isSelected;
	}

	
	/*private methods to build the Render*/
	
	private GenericPriceRender computeFlightFare(Locale locale) {
		BigDecimal fare = this.brandData.getGrossFare();
		return new GenericPriceRender(fare, this.brandData.getCurrency(), locale);
	}
	
	private String computeSolutionId() {
		if( this.brandData.getRefreshSolutionId()!=null && !("").equals(this.brandData.getRefreshSolutionId()) ){
			return  this.brandData.getRefreshSolutionId();
		}else{
			return  this.brandData.getSolutionId();
		}
	}

}
