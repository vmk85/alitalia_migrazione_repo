package com.alitalia.aem.business.web.utils;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

public class MultifieldUtils {

	//Metodo d'utilità per prendere i dati dai nodi jcr creati dal multifield
	@SuppressWarnings("rawtypes")
	static public Map fromJcrMultifieldToMapArrayObject(Resource resource, String childName) throws JSONException {
		
		Resource item = resource.getChild(childName);
		
		Map<String, Object> multifield = new LinkedHashMap<String, Object>();
		Map<String, Object> field;
		int i = 0;
		
		for (Resource multifieldResource : item.getChildren()) {
			ValueMap multifieldMap = multifieldResource.getValueMap();
			String[] multifieldMapKeys = 
					(String[]) multifieldMap.keySet().toArray(new String[multifieldMap.keySet().size()]); 
			String fieldValue = "";
			
			field = new LinkedHashMap<String, Object>();

			for (String valueMapKey : multifieldMapKeys) {
				if (!valueMapKey.equals("jcr:primaryType")) {
					fieldValue = multifieldMap.get(valueMapKey).toString();
					
					Map<String, Object> nestedMultifield = null;
					
					if (Pattern.matches("^\\[(\\{(\"[A-z0-9\\-\\.]*\"\\:\".*\"\\,?)*\\}\\,?)*\\]", fieldValue)) {
						//Se è un multifield nested
						JSONArray jsonArray = new JSONArray(fieldValue);

						nestedMultifield = new LinkedHashMap<String, Object>();
						Map<String, String> nestedFieldValue;
						
						for (int j = 0; j < jsonArray.length(); j++) {
							JSONObject jsonObject = jsonArray.getJSONObject(j);
							Iterator<String> keysIterator = jsonObject.keys();
							
							nestedFieldValue = new LinkedHashMap<String, String>();
							
							while (keysIterator.hasNext()) {
								String nextNestedKey = keysIterator.next();
								nestedFieldValue.put(nextNestedKey, jsonObject.getString(nextNestedKey));
							}
							
							nestedMultifield.put(Integer.toString(j), nestedFieldValue);
						}
						
						field.put(valueMapKey, nestedMultifield);
						
						
					} else {
						field.put(valueMapKey, fieldValue);
					}
				}
			}
			
			multifield.put(Integer.toString(i++), field);
		}
		
		return multifield;
	}
	
}
