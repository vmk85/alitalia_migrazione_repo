package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingPaymentException;
import com.alitalia.aem.business.web.booking.BookingPhaseEnum;
import com.alitalia.aem.business.web.booking.BookingSession;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "bookingpurchasereturn" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }),
	@Property(name = "success.page"), @Property(name = "failure.page") })
@SuppressWarnings("serial")
public class BookingPurchaseReturnServlet extends SlingSafeMethodsServlet {

	private ComponentContext componentContext;

	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private BookingSession bookingSession;

	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}

	@Override
	protected void doGet(SlingHttpServletRequest request, 
			SlingHttpServletResponse response) throws ServletException, IOException {


		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());

		try {

			BookingSessionContext ctx = (BookingSessionContext) 
					request.getSession(true).getAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE);

			logger.debug("ctx.isAuthorizePaymentInvoked: ["+ctx.isAuthorizePaymentInvoked+"]");
			if (!ctx.isAuthorizePaymentInvoked){

				try {

					logger.debug("PaymentData Type: ["+ctx.paymentData.getType().value()+"]");
					// complete payment!
					ResourceBundle resourceBundle = request.getResourceBundle(ctx.locale);
					I18n i18n = new I18n(resourceBundle);
					ctx.isAuthorizePaymentInvoked = true; 

					if (ctx.paymentData.getType() == PaymentTypeEnum.CREDIT_CARD && ctx.paymentData.getProcess().getRedirectUrl()!=null){ //3ds
						bookingSession.completePayment3ds(ctx.paymentData, ctx, ctx.paymentTid, i18n);
					}else
						bookingSession.completePayment(ctx.paymentData, ctx, ctx.paymentTid, i18n);

				} catch (BookingPaymentException e) {
					logger.error("Error during payment step (Bonifico): ", e);

					// open JSON response
					json.object();
					json.key("result").value(false);

					// evaluate specific error
					switch (e.getCode()) {
					case BookingPaymentException.BOOKING_ERROR_AUTHORIZEPAYMENT :
						manageErrorAuthorizePayment(json, ctx);
						break;
					case BookingPaymentException.BOOKING_ERROR_CHECKPAYMENT :
						manageErrorCheckPayment(json, ctx);
						break;
					case BookingPaymentException.BOOKING_ERROR_RETRIEVETICKETS :
						manageErrorRetrieveTickets(json, ctx);
						break;
					default:
						manageGenericError(json);
						break;
					}

					// close JSON response
					json.endObject();
					return;
				}
				// open JSON response
				json.object();
				json.key("result").value(true);
				json.key("redirect").value((String) componentContext.getProperties().get("success.page"));
				json.endObject();

			} else {
				json.object();
				json.key("authorizeAlreadyInvoked").value(true);
				json.endObject();
			}

		} catch (Exception e) {
			logger.error("Unexpected", e);

			// open JSON response
			try {
				json.object();
				json.key("result").value(false);
				manageGenericError(json);
				json.endObject();
			} catch (JSONException e1) {
				logger.error("Error creating JSON ", e1);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}

		}


	}

	/**
	 * Manage error on authorizePayment
	 * @param json
	 * @param typeOfPayment 
	 * @throws JSONException
	 */
	private void manageErrorAuthorizePayment(JSONWriter json, BookingSessionContext ctx) throws JSONException {
		json.key("redirect").value((String) componentContext.getProperties().get("failure.page"));
	}

	/**
	 * Manage error on checkPayment
	 * @param json
	 * @throws JSONException
	 */
	private void manageErrorCheckPayment(JSONWriter json, BookingSessionContext ctx) throws JSONException {
		String redirect = (String) componentContext.getProperties().get("success.page");
		redirect += "?showCallCenterMsg=true";
		json.key("redirect").value(redirect);
		ctx.phase = BookingPhaseEnum.DONE;
	}

	/**
	 * Manage error on retrieveTickets. Both for CDC and Bonifico 
	 * if an error occurs during this payment setp, the ticket was confirmed and the email is sent
	 * @param json
	 * @throws JSONException
	 */
	private void manageErrorRetrieveTickets(JSONWriter json, BookingSessionContext ctx) throws JSONException {
		String redirect = (String) componentContext.getProperties().get("success.page");
		redirect += "?showCallCenterMsg=true";
		json.key("redirect").value(redirect);
		ctx.phase = BookingPhaseEnum.DONE;
	}

	/**
	 * Manage generic error
	 * @param json
	 * @throws JSONException
	 */
	private void manageGenericError(JSONWriter json) throws JSONException {
		String redirect = (String) componentContext.getProperties().get("failure.page");
		json.key("redirect").value(redirect);
	}
}

