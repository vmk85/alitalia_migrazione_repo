package com.alitalia.aem.business.web.controllers;

import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.*;
import org.apache.sling.models.annotations.injectorspecific.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Session;

@Model(adaptables = {SlingHttpServletRequest.class})
public class UsersSfdcController {

    @Self
    private SlingHttpServletRequest request;

    @Inject
    private SlingHttpServletResponse response;

    public String getUsername() {
        return username;
    }

    private String username = null;

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostConstruct
    protected void initModel() {

        UserProperties profileProperties = getUserPropertyes();
        if (profileProperties != null) {
            this.username = getProperty(profileProperties,"ragioneSociale");
        }

    }

    private String getProperty(UserProperties profileProperties, String property) {

        String data = null;
        try {
            data = profileProperties.getProperty(property);
        } catch (Exception e) {
            logger.error("Error to try retrieve credential userLogged", e);
        }
        return data;
    }

    private UserProperties getUserPropertyes() {
        UserProperties profileProperties = null;
        try {

            ResourceResolver resourceResolver = request.getResourceResolver();
            Session session = resourceResolver.adaptTo(Session.class);
            UserPropertiesManager userPropertiesManager = resourceResolver.adaptTo(UserPropertiesManager.class);
            profileProperties = userPropertiesManager.getUserProperties(session.getUserID(), "profile");

        } catch (Exception e) {
            logger.error("Error to try retrieve credential userLogged", e);
        }
        return profileProperties;
    }
}
