package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.booking.BookingPaymentException;
import com.alitalia.aem.business.web.booking.BookingPhaseEnum;
import com.alitalia.aem.business.web.booking.BookingSession;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.booking.model.InvoiceType;
import com.alitalia.aem.business.web.booking.model.PaymentType;
import com.alitalia.aem.business.web.statistics.utils.TradeStatisticUtils;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.PaymentComunicationBancaIntesaData;
import com.alitalia.aem.common.data.home.PaymentData;
import com.alitalia.aem.common.data.home.PaymentProcessInfoData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.InitializePaymentErrorEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;
import com.alitalia.aem.common.utils.IDFactory;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.i18n.I18n;

@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "bookingpurchasedata" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "success.page"), @Property(name = "failure.page"),
	@Property(name = "returnUrl.page"), @Property(name = "errorUrl.page"),
	@Property(name = "cancelUrl.page"), @Property(name = "globalCollectUrl.page"),
	@Property(name = "unicredit.errorUrl.page")})
@SuppressWarnings("serial")
public class BookingPurchaseDataServlet extends GenericFormValidatorServlet {

	private ComponentContext componentContext;

	@Reference
	private BookingSession bookingSession;

	@Reference
	private AlitaliaTradeConfigurationHolder configuration;

	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}

	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {

		try {

			BookingSessionContext ctx = (BookingSessionContext) 
					request.getSession(true).getAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE);

			Validator validator = new Validator();

			// type of payment
			String tipoPagamento = request.getParameter("tipoPagamento");

			//prepare countries list for validation
			String[] countries = new String[ctx.countries.size()];
			int i = 0;
			for(CountryData country : ctx.countries) {
				countries[i] = country.getCode();
				i++;
			}

			// credit card
			if (PaymentType.CDC.toString().equals(tipoPagamento)) {

				/* get Values */
				String tipologiaCarta = request.getParameter("tipologiaCarta");
				String numeroCarta = request.getParameter("numeroCarta");
				String cvc = request.getParameter("cvc");
				String meseScadenza = request.getParameter("meseScadenza");
				String annoScadenza = request.getParameter("annoScadenza");

				String nome = request.getParameter("nome");
				String cognome = request.getParameter("cognome");
				String indirizzo = request.getParameter("indirizzo");
				String cap = request.getParameter("cap");
				String citta = request.getParameter("citta");
				String paese = request.getParameter("paese");

				// validate
				validator.setAllowedValuesMessagePattern("tipologiaCarta", tipologiaCarta,
						populateCreditCards().toArray(), Constants.MESSAGE_GENERIC_EMPTY_FIELD,
						"Tipologia di carta");
				validator.addDirectParameterMessagePattern("numeroCarta", numeroCarta,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Numero di carta", "isNotEmpty");
				validator.addDirectParameterMessagePattern("numeroCarta", numeroCarta,
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Numero di carta", "isNumber");
				validator.addDirectParameterMessagePattern("cvc", cvc,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "CVC", "isNotEmpty");
				validator.addDirectParameterMessagePattern("cvc", cvc,
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "CVC", "isNumber");

				if (tipologiaCarta.equalsIgnoreCase(CreditCardTypeEnum.AMERICAN_EXPRESS.value())) {
					validator.addDirectParameter("cvc", cvc,
							Constants.MESSAGE_CVC_NOT_VALID, "isCVCAMEX");
				} else if (tipologiaCarta.equalsIgnoreCase(CreditCardTypeEnum.VISA.value())) {
					validator.addDirectParameter("cvc", cvc,
							Constants.MESSAGE_CVC_NOT_VALID, "isCVCVISA");
				} else if (tipologiaCarta.equalsIgnoreCase(CreditCardTypeEnum.MASTER_CARD.value())) {
					validator.addDirectParameter("cvc", cvc,
							Constants.MESSAGE_CVC_NOT_VALID, "isCVCMASTERCARD");
				}



				validator.addDirectParameterMessagePattern("meseScadenza", meseScadenza,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Mese scadenza", "isNotEmpty");

				String[] valoriMesi = new String[] { "01", "02", "03", "04", "05", "06", "07",
						"08", "09", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
				validator.setAllowedValuesMessagePattern("meseScadenza", meseScadenza, valoriMesi,
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Mese scadenza");
				validator.addDirectParameterMessagePattern("annoScadenza", annoScadenza,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Anno scadenza", "isNotEmpty");
				validator.addDirectParameterMessagePattern("annoScadenza", annoScadenza,
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Anno scadenza", "isYear");

				if (meseScadenza != null && annoScadenza != null && !meseScadenza.isEmpty() && 
						!annoScadenza.isEmpty()) {
					try {
						int meseScad = Integer.parseInt(meseScadenza);
						int annoScad = Integer.parseInt(annoScadenza);
						if (meseScad == 12) {
							meseScad = 1;
							annoScad++;
						} else {
							meseScad++;
						}
						validator.addDirectParameter(
								"meseScadenza", "01/" + meseScad + "/" + annoScad,
								Constants.MESSAGE_DATE_FUTURE, "isFuture");
					} catch (Exception e) {
						logger.warn("Errore valori meseScadenza, annoScadenza", e);
					}
				}

				validator.addDirectParameterMessagePattern("nome", nome,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Nome titolare carta", "isNotEmpty");
				validator.addDirectParameterMessagePattern("cognome", cognome,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Cognome titolare carta",
						"isNotEmpty");

				// check extra fields for American Express
				if (!tipologiaCarta.equals("") &&
						CreditCardTypeEnum.fromValue(tipologiaCarta) != null &&
						CreditCardTypeEnum.AMERICAN_EXPRESS.equals(
								CreditCardTypeEnum.fromValue(tipologiaCarta))) {

					validator.addDirectParameterMessagePattern("indirizzo", indirizzo,
							Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Indirizzo", "isNotEmpty");
					validator.addDirectParameterMessagePattern("cap", cap,
							Constants.MESSAGE_GENERIC_EMPTY_FIELD, "C.A.P.", "isNotEmpty");
					validator.addDirectParameterMessagePattern("cap", cap,
							Constants.MESSAGE_GENERIC_INVALID_FIELD, "C.A.P.", "isNumber");
					validator.addDirectParameterMessagePattern("citta", citta,
							Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Citt&agrave;", "isNotEmpty");
					validator.addDirectParameterMessagePattern("paese", paese,
							Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Paese", "isNotEmpty");
					validator.setAllowedValuesMessagePattern("paese", paese,
							countries, Constants.MESSAGE_NATION_NOT_VALID,
							"Paese");
				}
			}

			// bank transfer
			else {

				// values
				String nomeBanca = request.getParameter("nomeBanca");

				// validate!
				validator.setAllowedValuesMessagePattern("nomeBanca", nomeBanca,
						populateBanks().toArray(), Constants.MESSAGE_GENERIC_EMPTY_FIELD,
						"Tipologia di banca");
			}

			// invoice ?
			if (request.getParameter("richiediFattura") != null && 
					!request.getParameter("richiediFattura").isEmpty()) {

				// values
				String tipoIntestatario = request.getParameter("tipoIntestatario");
				String nomeFattura = request.getParameter("nomeFattura");
				String cognomeFattura = request.getParameter("cognomeFattura");
				String codiceFiscaleFattura = request.getParameter("codiceFiscaleFattura");
				String intestatarioFattura = request.getParameter("intestatarioFattura");
				String indirizzoFattura = request.getParameter("indirizzoFattura");
				String capFattura = request.getParameter("capFattura");
				String cittaFattura = request.getParameter("cittaFattura");
				String provFattura = request.getParameter("provFattura");
				String paeseFattura = request.getParameter("paeseFattura");
				String emailFattura = request.getParameter("emailFattura");

				// validate!
				validator.setAllowedValuesMessagePattern("tipoIntestatario", tipoIntestatario,
						InvoiceType.values(), Constants.MESSAGE_GENERIC_EMPTY_FIELD,
						"Tipologia intestatario");
				validator.addDirectParameterMessagePattern("nomeFattura", nomeFattura,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Nome", "isNotEmpty");
				validator.addDirectParameterMessagePattern("cognomeFattura", cognomeFattura,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Cognome", "isNotEmpty");

				validator.addDirectParameterMessagePattern("codiceFiscaleFattura",
						codiceFiscaleFattura, Constants.MESSAGE_GENERIC_EMPTY_FIELD,
						"Codice fiscale o P.IVA", "isNotEmpty");

				if (codiceFiscaleFattura.matches("^([0-9])*$")) {
					validator.addDirectParameterMessagePattern("codiceFiscaleFattura",
							codiceFiscaleFattura, Constants.MESSAGE_GENERIC_INVALID_FIELD,
							"Codice fiscale o P.IVA", "isPartitaIVA");
				} else {
					String validatorCF = nomeFattura + "|" + cognomeFattura + "|" +  codiceFiscaleFattura;
					validator.addDirectParameterMessagePattern("codiceFiscaleFattura",
							validatorCF, Constants.MESSAGE_GENERIC_INVALID_FIELD,
							"Codice fiscale o P.IVA", "isValidCodiceFiscaleByConcatNomeCognomeCF");
				}

				if (InvoiceType.fromValue(tipoIntestatario) != InvoiceType.PF) {
					validator.addDirectParameterMessagePattern("intestatarioFattura",
							intestatarioFattura, Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Intestatario",
							"isNotEmpty");
				}

				validator.addDirectParameterMessagePattern("indirizzoFattura", indirizzoFattura,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Indirizzo", "isNotEmpty");
				validator.addDirectParameterMessagePattern("capFattura", capFattura,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "C.A.P.", "isNotEmpty");
				validator.addDirectParameterMessagePattern("capFattura", capFattura,
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "C.A.P.", "isNumber");
				validator.addDirectParameterMessagePattern("cittaFattura", cittaFattura,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Citt&agrave;", "isNotEmpty");
				validator.addDirectParameterMessagePattern("emailFattura", emailFattura,
						Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Email", "isNotEmpty");
				validator.addDirectParameterMessagePattern("emailFattura", emailFattura,
						Constants.MESSAGE_GENERIC_INVALID_FIELD, "Email", "isEmail");
				validator.setAllowedValuesMessagePattern("paeseFattura", paeseFattura,
						countries, Constants.MESSAGE_NATION_NOT_VALID,
						"Paese Fattura"); 
				if (paeseFattura.equals("IT")) {
					validator.addDirectParameterMessagePattern("provFattura", provFattura,
							Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Provincia", "isNotEmpty");
				}
			}

			// validate!
			ResultValidation resultValidation = validator.validate();
			return resultValidation;

		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Error during validation form payment", e);
			throw e;
		}
	}

	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {

		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		String typeOfPayment = "";
		String nomeBanca = "";
		try {
			Map<String,String> httpHeaders = getHttpHeaders(request, ";");
			String jscString = request.getParameter("beirutString");
			String csrfToken = request.getHeader("CSRF-Token");

			BookingSessionContext ctx = (BookingSessionContext) 
					request.getSession(true).getAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE);
			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				json.object();
				json.key("result").value(false);
				json.key("redirect").value((String) componentContext.getProperties().get("failure.page"));
				json.endObject();
				return;
			}

			ResourceBundle resourceBundle = request.getResourceBundle(
					LocalizationUtils.getLocaleByResource(request.getResource(), LocalizationUtils.getLanguage(ctx.locale.toLanguageTag())));
			final I18n i18n = new I18n(resourceBundle);

			// invoice ?
			String billingName = null, billingSurname = null, billingFormaSocietaria = null,
					billingCfPax = null, billingIndSped = null, billingCAP = null,
					billingLocSped = null, billingPaese = null, billingEmailInt = null,
					billingProv = null, billingIntFattura = null;
			ctx.invoiceRequired = (request.getParameter("richiediFattura") != null && 
					!request.getParameter("richiediFattura").isEmpty());
			if (ctx.invoiceRequired) {
				billingName = request.getParameter("nomeFattura");
				billingSurname = request.getParameter("cognomeFattura");
				billingCfPax = request.getParameter("codiceFiscaleFattura");
				billingIndSped = request.getParameter("indirizzoFattura");
				billingCAP = request.getParameter("capFattura");
				billingLocSped = request.getParameter("cittaFattura");
				billingPaese = request.getParameter("paeseFattura");
				billingProv = request.getParameter("provFattura");
				billingEmailInt = request.getParameter("emailFattura");
				billingFormaSocietaria = request.getParameter("tipoIntestatario");
				billingIntFattura = request.getParameter("intestatarioFattura");
			}

			// type of payment
			typeOfPayment = request.getParameter("tipoPagamento");
			nomeBanca = request.getParameter("nomeBanca");

			String protocol = request.getScheme();
			String serverName = request.getServerName();
//			int port = request.getServerPort();

//			if (port != -1) {
//				ctx.domain = protocol + "://" + serverName + ":" + port;
//			} else {
				ctx.domain = protocol + "://" + serverName;
//			}

			// credit card
			if (PaymentType.CDC.toString().equals(typeOfPayment)) {
				String creditCardType = request.getParameter("tipologiaCarta");
				String creditCardNumber = request.getParameter("numeroCarta");
				String creditCardCVV = request.getParameter("cvc");
				String creditCardName = request.getParameter("nome");
				String creditCardLastName = request.getParameter("cognome");
				Short creditCardExpiryMonth = Short.parseShort(request.getParameter("meseScadenza"));
				Short creditCardExpiryYear = Short.parseShort(request.getParameter("annoScadenza"));
				String zip = null, city = null, country = null, address = null, state = null;
				if (!creditCardType.equals("") &&
						CreditCardTypeEnum.fromValue(creditCardType) != null &&
						CreditCardTypeEnum.AMERICAN_EXPRESS.equals(
								CreditCardTypeEnum.fromValue(creditCardType))) {
					zip = request.getParameter("cap");
					city = request.getParameter("citta");
					country = request.getParameter("paese");
					address = request.getParameter("indirizzo");
					// FIXME [booking/purchaseDataServlet] capire se la provincia o lo stato
					// sono veramente obbligatori ed in caso che cos'e' che ci va messo dentro,
					// al momento nell'HTML non c'e' proprio il campo
					// es: USA -> stato, IT -> provincia
					// state = request.getParameter("provincia");
				}

				String errorUrl = ctx.domain + "/content/alitaliatrade/main/booking/.redirectPaymentBusinessServlet"; //(String) componentContext.getProperties().get("errorUrl.page");
				String returnUrl = ctx.domain + "/content/alitaliatrade/main/booking/.redirectPaymentBusinessServlet";
				if(!"undefined".equals(csrfToken)){
					errorUrl += (errorUrl.contains("?") ? "&" : "?") + ":cq_csrf_token=" + csrfToken;
					returnUrl += "?:cq_csrf_token=" + csrfToken;
				}
				bookingSession.preparePaymentWithCreditCard(ctx, creditCardNumber,
						creditCardCVV, creditCardExpiryMonth, creditCardExpiryYear,
						creditCardType, creditCardName, creditCardLastName, billingCAP,
						billingCfPax, billingEmailInt, billingFormaSocietaria, billingIndSped,
						billingLocSped, billingPaese, billingProv, billingName, billingSurname,
						billingIntFattura, zip, city, country, address, state, 
						TradeStatisticUtils.getClientIP(configuration.getClientIpHeaders(), request), request.getHeader("User-Agent"),
						errorUrl, returnUrl, i18n);
			}

			else if (nomeBanca != null && PaymentTypeEnum.STS.value().equals(nomeBanca)) {
				String errorUrl = ctx.domain + (String) componentContext.getProperties().get("paymentUrl.page"); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectErrorPayment"
				String returnUrl = ctx.domain + (String) componentContext.getProperties().get("returnUrl.page"); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment";
				String cancelUrl = ctx.domain + (String) componentContext.getProperties().get("cancelUrl.page"); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectCancelPayment";
				String globalCollectUrl = ctx.domain + (String) componentContext.getProperties().get("globalCollectUrl.page"); //"https://booking.uat1.az/Booking/IT_IT/GlobalCollect/Return";					
				bookingSession.preparePaymentWithSTS(ctx, billingCAP,
						billingCfPax, billingEmailInt, billingFormaSocietaria, billingIndSped,
						billingLocSped, billingPaese, billingProv, billingName, billingSurname,
						billingIntFattura, nomeBanca, TradeStatisticUtils.getClientIP(configuration.getClientIpHeaders(), request), request.getHeader("User-Agent"),
						errorUrl, returnUrl, cancelUrl, globalCollectUrl,i18n);

			}

			// bank transfer
			else {
				String errorUrl = ctx.domain + (String) componentContext.getProperties().get("errorUrl.page"); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectErrorPayment"
				String returnUrl = ctx.domain + (String) componentContext.getProperties().get("returnUrl.page"); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectOkPayment";
				String cancelUrl = ctx.domain + (String) componentContext.getProperties().get("cancelUrl.page"); //"https://booking.uat1.az/Booking/IT_IT/Checkout/RedirectCancelPayment";
				String globalCollectUrl = ctx.domain + (String) componentContext.getProperties().get("globalCollectUrl.page"); //"https://booking.uat1.az/Booking/IT_IT/GlobalCollect/Return";
				//String unicreditReturnUrl = (String) componentContext.getProperties().get("unicredit.returnUrl.page"); 
				String unicreditErrorUrl = (String) componentContext.getProperties().get("unicredit.errorUrl.page");

				if (PaymentTypeEnum.fromValue(nomeBanca) == PaymentTypeEnum.UNICREDIT) {
					//returnUrl = unicreditReturnUrl;
					errorUrl = unicreditErrorUrl;
					if(!"undefined".equals(csrfToken)){
						errorUrl += (errorUrl.contains("?") ? "&" : "?") + ":cq_csrf_token=" + csrfToken;
					}
				}

				bookingSession.preparePaymentWithBankTransfer(ctx, billingCAP,
						billingCfPax, billingEmailInt, billingFormaSocietaria, billingIndSped,
						billingLocSped, billingPaese, billingProv, billingName, billingSurname,
						billingIntFattura, nomeBanca, TradeStatisticUtils.getClientIP(configuration.getClientIpHeaders(), request),
						errorUrl, returnUrl, cancelUrl, globalCollectUrl,i18n);
			}

			// perform payment
			try {
				bookingSession.performPayment(ctx, TradeStatisticUtils.getClientIP(configuration.getClientIpHeaders(), request), 
						request.getHeader("User-Agent"), IDFactory.getTid(), typeOfPayment, i18n, httpHeaders, jscString);
			} catch (BookingPaymentException e) {
				logger.error("Error during payment step: ", e);

				// open JSON response
				json.object();
				json.key("result").value(false);

				// evaluate specific error
				switch (e.getCode()) {
				case BookingPaymentException.BOOKING_ERROR_INITIALIZEPAYMENT :
					manageErrorInitializePayment(json, e.getInitializePaymentEnum(), typeOfPayment, e.getMessage());
					break;
				case BookingPaymentException.BOOKING_ERROR_AUTHORIZEPAYMENT :
					manageErrorAuthorizePayment(json, ctx, typeOfPayment);
					break;
				case BookingPaymentException.BOOKING_ERROR_CHECKPAYMENT :
					manageErrorCheckPayment(json, ctx);
					break;
				case BookingPaymentException.BOOKING_ERROR_RETRIEVETICKETS :
					manageErrorRetrieveTickets(json, ctx);
					break;
				default:
					manageGenericError(json);
					break;
				}

				// close JSON response
				json.endObject();
				return;
			}

			// open JSON response
			json.object();
			json.key("result").value(true);

			// payment to be completed later?
			if (ctx.paymentData != null) {
				PaymentData paymentData = ctx.paymentData;
				PaymentProcessInfoData process = paymentData.getProcess();
				json.key("complete");
				json.object();
				json.key("redirect").value(process.getRedirectUrl());
				String method = computeSubmitMethod(nomeBanca);
				json.key("method").value(PaymentType.CDC.toString().equals(typeOfPayment) && process.getRedirectUrl()!=null ? "GET" : method);
				if (paymentData.getProvider() instanceof PaymentProviderCreditCardData) {
					json.key("params");
					json.object();
					if(process.getTokenId() != null && process.getTokenId().length() > 0){
						String[] parameters = process.getTokenId().split("&");
						for (String p : parameters){
							String name = p.substring(0, p.indexOf("="));
							String value = p.substring(1+p.indexOf("="));
							if (name.equals("PaRequest")){
								json.key("PaReq").value(value);
							} else {
								json.key(name).value(value);
							}
						}		
					}
					json.key("TermUrl").value(paymentData.getProvider().getComunication().getReturnUrl());
					json.key("CartId").value(process.getTransactionId());
					json.endObject();
				}
				if (paymentData.getType() == PaymentTypeEnum.BANCA_INTESA) {
					PaymentComunicationBancaIntesaData comunicationBancaIntesa = (PaymentComunicationBancaIntesaData) paymentData.getProvider().getComunication();
					Map<String, String> requestData = comunicationBancaIntesa.getRequestData();
					if (requestData != null && !requestData.isEmpty()) {
						json.key("params");
						json.object();
						for (String key : requestData.keySet()) {
							String value = requestData.get(key);
							if (!key.equals("action")) {
								json.key(key).value(value);
							}
						}
						json.endObject();
					}
				}
				json.endObject();
			}
			// all done!
			else {
				json.key("redirect").value((String) componentContext.getProperties().get("success.page"));
			}

			// close JSON response
			json.endObject();

		} catch(Exception e) {
			logger.error("Unexpected", e);

			// open JSON response
			try {
				json.object();
				json.key("result").value(false);
				manageGenericError(json);
				json.endObject();
			} catch (JSONException e1) {
				logger.error("Error creating JSON ", e1);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
	}

	/**
	 * Return a map that contains all HTTP Headers present in the request.
	 * @param request
	 * @param separator used to concat in case of headers with multiple values
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Map<String, String> getHttpHeaders(SlingHttpServletRequest request, String separator) {
		// TODO Auto-generated method stub
		Map<String, String> httpHeaders = new HashMap<String,String>();
		Enumeration<String> headerNames = request.getHeaderNames();
		while(headerNames.hasMoreElements()){
			String headerName = headerNames.nextElement();
			if(headerName != null && headerName.length() > 0){
				Enumeration<String> headerValues = request.getHeaders(headerName);
				String headerValue = concatEnumeration(headerValues, separator);
				/*Insert into map*/
				httpHeaders.put(headerName, headerValue);
			}
		}
		return httpHeaders;
	}

	private String concatEnumeration(Enumeration<String> enumeration, String separator){
		String concat = "";
		while(enumeration.hasMoreElements()){
			String value = enumeration.nextElement();
			concat += value + separator;
		}
		/*remove trailing separator*/
		if(concat.length() > 0){
			concat = concat.substring(0, concat.lastIndexOf(separator));
		}
		return concat;
	}

	/**
	 * Populate an ArrayList of supported credit cards
	 * @return ArrayList<String> credit cards
	 */
	private ArrayList<String> populateCreditCards() {
		ArrayList<String> creditCards = new ArrayList<String>();
		creditCards.add(CreditCardTypeEnum.AMERICAN_EXPRESS.value());
		creditCards.add(CreditCardTypeEnum.MASTER_CARD.value());
		creditCards.add(CreditCardTypeEnum.VISA.value());
		return creditCards;
	}

	/**
	 * Populate an ArrayList of supported credit cards
	 * @return ArrayList<String> credit cards
	 */
	private ArrayList<String> populateBanks() {
		ArrayList<String> banks = new ArrayList<String>();
		//banks.add(PaymentTypeEnum.BANCA_INTESA.value());
		//banks.add(PaymentTypeEnum.UNICREDIT.value());
		banks.add(PaymentTypeEnum.GLOBAL_COLLECT.value());
		banks.add(PaymentTypeEnum.STS.value());
		return banks;
	}

	private String computeSubmitMethod(String nomeBanca) {
		logger.debug("Executing computeSubmitMethod. nomeBanca: ["+nomeBanca+"]");
		if (PaymentTypeEnum.STS.value().equals(nomeBanca)) {
			return "GET";
		}
		return "POST";
	}

	/**
	 * Manage error on initializePayment
	 * @param json
	 * @throws JSONException
	 */
	private void manageErrorInitializePayment(JSONWriter json, InitializePaymentErrorEnum initPaymentError, String paymentType, String errorMessage) throws JSONException {
		logger.debug("[manageErrorInitializePayment] paymentType: {}", paymentType );
		if (PaymentType.BonificoOnLine.toString().equals(paymentType)) {
			json.key("message").value(Constants.MESSAGE_ERROR_AUTHORIZE);
			String redirect = (String) componentContext.getProperties().get("errorUrl.page");
			String ec = null;
			if (InitializePaymentErrorEnum.INSUFFICIENT_PLAFOND == initPaymentError){
				ec="21";
			} else if (InitializePaymentErrorEnum.INVALID_MERCHANT_ID == initPaymentError){
				ec="22";
			} else if (InitializePaymentErrorEnum.BLOCKED_MERCHANT_ID == initPaymentError){
				ec="23";
			}
			if(ec!=null)
				redirect = redirect.contains("?") ? redirect+"&ec="+ec : redirect+"?ec="+ec;
			json.key("redirect").value(redirect);
		} else {
			json.key("fields");
			json.object();
			json.key("tipologiaCarta").value(MessageFormat.format(
					Constants.MESSAGE_GENERIC_CHECK_FIELD, "Tipologia di carta"));
			json.key("numeroCarta").value(MessageFormat.format(
					Constants.MESSAGE_GENERIC_CHECK_FIELD, "Numero di carta"));
			json.key("cvc").value(MessageFormat.format(
					Constants.MESSAGE_GENERIC_CHECK_FIELD, "CVC"));
			json.key("meseScadenza").value(MessageFormat.format(
					Constants.MESSAGE_GENERIC_CHECK_FIELD, "Mese scadenza"));
			json.key("annoScadenza").value(MessageFormat.format(
					Constants.MESSAGE_GENERIC_CHECK_FIELD, "Anno scadenza"));
			json.key("nome").value(MessageFormat.format(
					Constants.MESSAGE_GENERIC_CHECK_FIELD, "Nome titolare carta"));
			json.key("cognome").value(MessageFormat.format(
					Constants.MESSAGE_GENERIC_CHECK_FIELD, "Cognome titolare carta"));
			json.endObject();
			if (initPaymentError == null){
				if (paymentType != null && !paymentType.equals("") && PaymentType.BonificoOnLine.toString().equals(paymentType)) {
					json.key("message").value(Constants.MESSAGE_ERROR_AUTHORIZE);
				} else {
					json.key("message").value(Constants.MESSAGE_ERROR_INITIALIZE_CREDIT_CARD);
				}
			} else {
				switch (initPaymentError) {
				case INFANT_INVENTORY_UNAVAILABLE :
					logger.debug("Trade manageErrorInitializePayment case INFANT_INVENTORY_UNAVAILABLE");
					json.key("message").value(Constants.MESSAGE_ERROR_INITIALIZE_CREDIT_CARD_INFANT_INVENTORY_UNAVAILABLE);
					break;
				default :
					json.key("message").value(Constants.MESSAGE_ERROR_INITIALIZE_CREDIT_CARD);
					break;
				}
			}
		}
	}

	/**
	 * Manage error on authorizePayment
	 * @param json
	 * @param typeOfPayment 
	 * @throws JSONException
	 */
	private void manageErrorAuthorizePayment(JSONWriter json, BookingSessionContext ctx, String typeOfPayment) throws JSONException {
		logger.debug("[manageErrorAuthorizePayment] paymentType: {}", typeOfPayment );
		json.key("message").value(Constants.MESSAGE_ERROR_AUTHORIZE);
		if (typeOfPayment != null && !typeOfPayment.equals("")) {
			if (PaymentType.CDC.toString().equals(typeOfPayment)) {
				String redirect = (String) componentContext.getProperties().get("success.page");
				redirect += "?showCallCenterMsg=true";
				json.key("redirect").value(redirect);
				ctx.phase = BookingPhaseEnum.DONE;
			} else if (PaymentType.BonificoOnLine.toString().equals(typeOfPayment)) {
				manageErrorInitializePayment(json, null, PaymentType.BonificoOnLine.toString(), null);
			} else {
				manageGenericError(json);
			}
		} else {
			manageGenericError(json);
		}
	}

	/**
	 * Manage error on checkPayment
	 * @param json
	 * @throws JSONException
	 */
	private void manageErrorCheckPayment(JSONWriter json, BookingSessionContext ctx) throws JSONException {
		logger.debug("[manageErrorCheckPayment]" );
		String redirect = (String) componentContext.getProperties().get("success.page");
		redirect += "?showCallCenterMsg=true";
		json.key("redirect").value(redirect);
		ctx.phase = BookingPhaseEnum.DONE;
	}

	/**
	 * Manage error on retrieveTickets. Both for CDC and Bonifico 
	 * if an error occurs during this payment setp, the ticket was confirmed and the email is sent
	 * @param json
	 * @throws JSONException
	 */
	private void manageErrorRetrieveTickets(JSONWriter json, BookingSessionContext ctx) throws JSONException {
		logger.debug("[manageErrorRetrieveTickets]" );
		String redirect = (String) componentContext.getProperties().get("success.page");
		redirect += "?showCallCenterMsg=true";
		json.key("redirect").value(redirect);
		ctx.phase = BookingPhaseEnum.DONE;
	}

	/**
	 * Manage generic error
	 * @param json
	 * @throws JSONException
	 */
	private void manageGenericError(JSONWriter json) throws JSONException {
		logger.debug("[manageGenericError]" );
		String redirect = (String) componentContext.getProperties().get("failure.page");
		json.key("redirect").value(redirect);
	}
}
