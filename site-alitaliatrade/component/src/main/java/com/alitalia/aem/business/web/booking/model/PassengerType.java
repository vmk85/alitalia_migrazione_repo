package com.alitalia.aem.business.web.booking.model;

public enum PassengerType {
	ADULT,
	CHILD,
	INFANT
}
