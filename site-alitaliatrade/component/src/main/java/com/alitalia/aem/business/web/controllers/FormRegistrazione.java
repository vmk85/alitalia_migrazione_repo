package com.alitalia.aem.business.web.controllers;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.scripting.SlingScriptHelper;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;

public class FormRegistrazione extends WCMUse {
	
	private Map<String, String> mapStati;
	private Map<String, String> mapProvince;
	
	@Override
    public void activate() throws Exception {
		
		mapStati = new LinkedHashMap<String, String>();
		mapStati.put("ITA", "Italia");
		mapStati.put("SM", "San Marino");
		
		SlingScriptHelper helper = getSlingScriptHelper();
		StaticDataDelegate staticDataDelegate = helper.getService(StaticDataDelegate.class);
		
		List<CountryData> list = null;
		
		RetrieveProvincesRequest serviceRequest = new RetrieveProvincesRequest(IDFactory.getTid(), IDFactory.getSid(getRequest()));
		serviceRequest.setLanguageCode("IT");
		serviceRequest.setMarket("IT");
		
		RetrieveProvincesResponse serviceResponse = staticDataDelegate.retrieveProvinces(serviceRequest);
		list = serviceResponse.getProvinces();
		
		mapProvince = new LinkedHashMap<String, String>();
		if (list != null && !list.isEmpty()) {
			for (CountryData item : list) {
				mapProvince.put(item.getStateCode(), item.getStateDescription());
			}
		}
	}
	
	public Map<String, String> getMapStati() {
		return mapStati;
	}
	
	public Map<String, String> getMapProvince() {
		return mapProvince;
	}
}