package com.alitalia.aem.business.web.controllers;

import java.util.*;

import org.slf4j.*;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.*;

public class CheckMercato extends WCMUse {
	
	private Logger logger = LoggerFactory.getLogger(CheckMercato.class);
	private String language;
	private String market;
	private HashMap<String,String> geoResult;

	@Override
	public void activate() throws Exception {

		try {
			
			geoResult = LocalizationUtils.getUserMarketLanguage(getSlingScriptHelper(), getRequest());

			market = geoResult.get("market");
			language = geoResult.get("language");
			
			logger.debug("Market: {}, Language: {}",new Object[] {market, language});
		
		} catch (Exception e) {
			logger.error("Unexpected error: {}", e);
			throw e;
		}
		
	}

	public String getLanguage() {
		return language;
	}

	public String getMarket() {
		return market;
	}

}
