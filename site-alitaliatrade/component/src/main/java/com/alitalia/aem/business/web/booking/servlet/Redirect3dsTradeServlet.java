package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.utils.Constants;


@Component
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "redirectPaymentTradeServlet" }),
	@Property(name = "sling.servlet.methods", value = { "POST" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }) })
@SuppressWarnings("serial")
public class Redirect3dsTradeServlet extends SlingAllMethodsServlet {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Reference
	private AlitaliaTradeConfigurationHolder configuration;

	private ComponentContext componentContext;

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {

		logger.info("Executing redirectPaymentBusinessServlet");
		
		BookingSessionContext ctx = (BookingSessionContext) request.getSession(true).getAttribute(
				Constants.BOOKING_CONTEXT_ATTRIBUTE);

		if (ctx == null) {
			logger.error("Cannot find booking session context in session.");
			throw new IllegalStateException("Booking session context not found.");
		}
		
		String paRes = request.getParameter("PaRes");
		logger.debug("Parameters received from 3ds page: PaRes["+paRes+"]");
		ctx.paymentData.getProcess().setPaRes(paRes);

		String returnUrl = ctx.domain + (String) componentContext.getProperties().get("returnUrl.page");
		String errorUrl = ctx.domain + (String) componentContext.getProperties().get("errorUrl.page");

		try {
			response.sendRedirect(returnUrl);
		} catch(Exception e) {
			logger.error("Unexpected error: ", e);
			response.sendRedirect(errorUrl);
		}
	}

}
