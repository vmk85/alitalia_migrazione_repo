package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.BookingSession;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.statistics.utils.TradeStatisticUtils;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "bookingflightdata" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.") })
@SuppressWarnings("serial")
public class BookingFlightDataServlet extends GenericFormValidatorServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private ComponentContext componentContext;
	
	@Reference
	private BookingSession bookingSession;
	
	@Reference
	private AlitaliaTradeConfigurationHolder configuration;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
			
		BookingSessionContext ctx = (BookingSessionContext) request.getSession(true).getAttribute(
				Constants.BOOKING_CONTEXT_ATTRIBUTE);
		if (ctx == null) {
			logger.error("Cannot find booking session context in session.");
			throw new IllegalStateException("Booking session context not found.");
		}
		
		Validator validator = new Validator();
		
		String solutionIDAndata = request.getParameter("andataSelection");
		validator.addDirectParameter("andataSelection", solutionIDAndata, Constants.MESSAGE_GENERIC_EMPTY_FIELD,
				"isNotEmpty");
		
		if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP) {
			String solutionIDRitorno = request.getParameter("ritornoSelection");
			validator.addDirectParameter("ritornoSelection", solutionIDRitorno, Constants.MESSAGE_GENERIC_EMPTY_FIELD,
					"isNotEmpty");
		}
		
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		try {
		
			BookingSessionContext ctx = (BookingSessionContext) request.getSession(true).getAttribute(
					Constants.BOOKING_CONTEXT_ATTRIBUTE);
			
			//INIZIO *** MODIFICA IP ***
			String[] clientIpHeaders = configuration.getClientIpHeaders();
			String ipAddress = "";
			ipAddress = TradeStatisticUtils.getClientIP(clientIpHeaders, request);
			if (ipAddress != null && !ipAddress.isEmpty()) {
				ctx.ipAddress = ipAddress;
			}
			//FINE.
			
			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				throw new IllegalStateException("Booking session context not found.");
			}
			
			String solutionId = "";
			if(ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
				solutionId = request.getParameter("andataSelection");
			}
			
			bookingSession.confirmFlightSelections(ctx,solutionId);
			
			if (ctx.readyToPassengersDataPhase) {
				String successPage = (String) componentContext.getProperties().get("success.page");
				if (successPage != null && !"".equals(successPage)) {
					response.sendRedirect(response.encodeRedirectURL(successPage));
				}
			}			
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			
			if (!response.isCommitted()) {
				String failurePage = (String) componentContext.getProperties().get("failure.page");
				if (failurePage != null && !"".equals(failurePage)) {
					response.sendRedirect(response.encodeRedirectURL(failurePage));
				} else {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
			}
		}
	}
	
}
