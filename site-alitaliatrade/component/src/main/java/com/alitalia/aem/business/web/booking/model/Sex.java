package com.alitalia.aem.business.web.booking.model;

public enum Sex {
	MALE,
	FEMALE
}
