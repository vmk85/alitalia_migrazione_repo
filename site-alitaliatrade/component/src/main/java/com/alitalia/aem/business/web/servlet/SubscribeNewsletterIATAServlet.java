package com.alitalia.aem.business.web.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.web.component.newsletter.delegate.NewsletterDelegate;
import com.alitalia.aem.business.web.utils.AlitaliaTradeAuthUtils;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.common.data.home.NLUserData;
import com.alitalia.aem.common.data.home.enumerations.InsertStatusEnum;
import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterRequest;
import com.alitalia.aem.common.messages.home.RegisterUserToAgencyNewsletterResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "subscribenewsletteriata" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.") })
@SuppressWarnings("serial")
public class SubscribeNewsletterIATAServlet extends GenericFormValidatorServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private ComponentContext componentContext;
	
	@Reference
	private NewsletterDelegate newsletterDelegate;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)  {
		
		try {
			Validator validator = new Validator();
			
			boolean isLogged = Boolean.parseBoolean(AlitaliaTradeAuthUtils.getProperty(request, "isLogged"));
			
			String codiceIata = request.getParameter("codice-iata");
			if (isLogged) {
				codiceIata = AlitaliaTradeAuthUtils.getProperty(request, "codiceAgenzia");
			}
			if (codiceIata != null && !("").equals(codiceIata) ) {
				validator.addDirectParameter("codice-iata", codiceIata, Constants.MESSAGE_IATA_NOT_VALID, "isIata");
			} else {
				validator.addDirectParameterMessagePattern("codice-iata", codiceIata , Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Codice Iata", "isNotEmpty");
			}
			
			validator.addDirectParameterMessagePattern("nome", request.getParameter("nome"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Nome", "isNotEmpty");
			validator.addDirectParameterMessagePattern("nome", request.getParameter("nome"), Constants.MESSAGE_GENERIC_INVALID_FIELD, "Nome", "isAlphabetic");
			validator.addDirectParameterMessagePattern("cognome", request.getParameter("cognome"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Cognome", "isNotEmpty");
			validator.addDirectParameterMessagePattern("cognome", request.getParameter("cognome"), Constants.MESSAGE_GENERIC_INVALID_FIELD, "Cognome", "isAlphabetic");
			validator.setAllowedValuesMessagePattern("usertype", request.getParameter("usertype"), new String[] { "B", "T" }, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Tipo di account");
			
			String mail = request.getParameter("email");
			if (mail != null && !("").equals(mail) ) {
				validator.addDirectParameter("email", mail, Constants.MESSAGE_EMAIL_NOT_VALID, "isEmail");
			} else {
				validator.addDirectParameterMessagePattern("email", mail, Constants.MESSAGE_GENERIC_EMPTY_FIELD,  "Indirizzo e-mail", "isNotEmpty");
			}
			
			return validator.validate();
		} catch (Exception e) {
			logger.error("unexpected error during validation", e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		
		response.setContentType("application/json");
		TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
		try {
			out.object();
			NLUserData user = new NLUserData();
			user.setName(request.getParameter("nome"));
			user.setLastname(request.getParameter("cognome"));
			String ruolo = request.getParameter("usertype");
			if (ruolo.equals("B")) {
				user.setRole(1);
			}
			if (ruolo.equals("T")) {
				user.setRole(0);
			}
			user.setEmail(request.getParameter("email"));
			List<NLUserData> users = new ArrayList<NLUserData>();
			users.add(user);
			
			boolean isLogged = Boolean.parseBoolean(AlitaliaTradeAuthUtils.getProperty(request, "isLogged"));
			String codiceIata = request.getParameter("codice-iata");
			if (isLogged) {
				codiceIata = AlitaliaTradeAuthUtils.getProperty(request, "codiceAgenzia");
			}
			
			RegisterUserToAgencyNewsletterRequest serviceRequest = new RegisterUserToAgencyNewsletterRequest();
			serviceRequest.setTid(IDFactory.getTid());
			serviceRequest.setSid(IDFactory.getSid(request));
			serviceRequest.setAgencyId(codiceIata);
			serviceRequest.setUsers(users);
			RegisterUserToAgencyNewsletterResponse serviceResponse = newsletterDelegate.registerUserToAgencyNewsletter(serviceRequest);
			if (serviceResponse.getUsers().get(0).getStatus() == InsertStatusEnum.INSERTED) {
				logger.debug("Agency {} succesfully registered to newsletter.", codiceIata);
				String successPage = (String) componentContext.getProperties().get("success.page");
				if (successPage != null) {
					out.key("redirect").value(successPage);
				}
			
			}
			else {
				if  (serviceResponse.getUsers().get(0).getStatus() == InsertStatusEnum.EXISTING) {
					logger.debug("Error during registration to newsletter for agency {}: User already exists.", codiceIata);
					out.key("existing").value(true);
				}
				else {
					logger.debug("Error during registration to newsletter for agency {}: Result False.", codiceIata);
					out.key("notinserted").value(true);
				}
				
			}
			out.endObject();
		} catch (Exception e) {
			logger.error("Error during registration to newsletter: ", e);
			String failurePage = (String) componentContext.getProperties().get("failure.page");
			try {
				if (failurePage != null) {
					out.key("redirect").value(failurePage);
					out.endObject();
				}
			} catch (JSONException e1) {
				logger.error("Unexpected error: ", e1);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
	}
	
}
