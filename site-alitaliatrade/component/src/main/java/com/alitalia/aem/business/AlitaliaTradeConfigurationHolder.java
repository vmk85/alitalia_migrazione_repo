package com.alitalia.aem.business;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;

import com.adobe.granite.crypto.CryptoException;
import com.adobe.granite.crypto.CryptoSupport;

/**
 * Generic configuration service used to hold and expose
 * a set of misc properties (configurable through OSGI).
 * 
 * <p>This component can be used, for example, to define system-wide 
 * properties whose value must be managed through OSGI nodes, or
 * other configurations not tied to specific components.</p>
 * 
 * <p>Properties values can be reached by page also by using
 * the <code>AlitaliaTradeConfigurationModel</model> Sling model.</p>
 */

@Component(metatype = true, immediate = true)
@Service(value=AlitaliaTradeConfigurationHolder.class)
@Properties({
	@Property(name = AlitaliaTradeConfigurationHolder.HTTPS_ENABLED, boolValue = true),
	@Property(name = AlitaliaTradeConfigurationHolder.ERROR_NAVIGATION_PAGE, description = "Users will be redirected to the specified page after a 404 error."),
	@Property(name = AlitaliaTradeConfigurationHolder.ERROR_NAVIGATION_PAGE_IT, description = "Users will be redirected to the specified page after a 404 error."),
	@Property(name = AlitaliaTradeConfigurationHolder.ERROR_NAVIGATION_PAGE_EN, description = "Users will be redirected to the specified page after a 404 error."),
	@Property(name = AlitaliaTradeConfigurationHolder.LINK_GRUPPI, description = "Users will be redirected to the specified page for groups."),
	@Property(name = AlitaliaTradeConfigurationHolder.BOOKING_SEARCH_PAGE, description = "Users will be redirected to the search page."),
	@Property(name = AlitaliaTradeConfigurationHolder.BOOKING_PASSENGERS_PAGE, description = "Users will be redirected to the passengers data page."),
	@Property(name = AlitaliaTradeConfigurationHolder.BOOKING_PAYMENT_PAGE, description = "Users will be redirected to the payment page."),
	@Property(name = AlitaliaTradeConfigurationHolder.BOOKING_PAYMENT_3DS_PAGE, description = "Users will be redirected to the payment 3DS page."),
	@Property(name = AlitaliaTradeConfigurationHolder.BOOKING_CONFIRM_PAGE, description = "Users will be redirected to the confirm page."),
	@Property(name = AlitaliaTradeConfigurationHolder.BOOKING_ERROR_PAGE, description = "Users will be redirected to the booking error page."),
	@Property(name = AlitaliaTradeConfigurationHolder.BOOKING_UNICREDIT_REDIRECT_FAILURE, description = "Users will be redirected to the booking error page with unicredit payment."),
	@Property(name = AlitaliaTradeConfigurationHolder.MIN_YEAR_ADT, description = "Lower bound for Adult passenger birth year"),
	@Property(name = AlitaliaTradeConfigurationHolder.MAX_YEAR_ADT, description = "Upper bound for Adult passenger birth year"),
	@Property(name = AlitaliaTradeConfigurationHolder.MIN_YEAR_CHD, description = "Lower bound for Child passenger birth year"),
	@Property(name = AlitaliaTradeConfigurationHolder.MAX_YEAR_CHD, description = "Upper bound for Child passenger birth year."),
	@Property(name = AlitaliaTradeConfigurationHolder.MIN_YEAR_INF, description = "Lower bound for Infant passenger birth year."),
	@Property(name = AlitaliaTradeConfigurationHolder.MAX_YEAR_INF, description = "Upper bound for Infant passenger birth year."),
	@Property(name = AlitaliaTradeConfigurationHolder.KEEPALIVE_NUMBER_OF_TIMEOUT, description = "Number of calls to refresh the session"),
	@Property(name = AlitaliaTradeConfigurationHolder.KEEPALIVE_TIMEOUT, description = "Timeout to call the refresh the session."),
	@Property(name = AlitaliaTradeConfigurationHolder.CLIENT_IP_HEADERS, description = "Headers to find the clientIP", value = {}),
	@Property(name = AlitaliaTradeConfigurationHolder.MAC_3DS_STRING, value = ""),
	@Property(name = AlitaliaTradeConfigurationHolder.MAIL_CONFIRMATION_ALITALIA_TRADE, description = "Sender address of confimation email"),
	@Property(name = AlitaliaTradeConfigurationHolder.EGON_USER, value = ""),
	@Property(name = AlitaliaTradeConfigurationHolder.EGON_PASSWORD, value = ""),
	@Property(name = AlitaliaTradeConfigurationHolder.EGON_DISABLED, value = ""),
	@Property(name = AlitaliaTradeConfigurationHolder.FARERULES_SATIC_ON, value = ""),
	@Property(name = AlitaliaTradeConfigurationHolder.LISTA_VOLI_SOGGETTI_APPROVAZIONE_GOVERNATIVA, value= ""),
})

public class AlitaliaTradeConfigurationHolder {
	
	public static final String HTTPS_ENABLED = "https.enabled";
	protected static final String ERROR_NAVIGATION_PAGE = "errorNavigation.page";
	protected static final String ERROR_NAVIGATION_PAGE_IT = "errorNavigationIT.page";
	protected static final String ERROR_NAVIGATION_PAGE_EN = "errorNavigationEN.page";
	protected static final String LINK_GRUPPI = "gruppi.link";
	protected static final String BOOKING_SEARCH_PAGE = "booking.search.page";
	protected static final String BOOKING_PASSENGERS_PAGE = "booking.passengers.page";
	protected static final String BOOKING_PAYMENT_PAGE = "booking.payment.page";
	protected static final String BOOKING_PAYMENT_3DS_PAGE = "booking.payment3ds.page";
	protected static final String BOOKING_CONFIRM_PAGE = "booking.confirm.page";
	protected static final String BOOKING_ERROR_PAGE = "booking.error.page";
	protected static final String BOOKING_UNICREDIT_REDIRECT_FAILURE = "booking.unicredit.redirect.failure";
	protected static final String MIN_YEAR_ADT = "min.year.adt";
	protected static final String MAX_YEAR_ADT = "max.year.adt";
	protected static final String MIN_YEAR_CHD = "min.year.chd";
	protected static final String MAX_YEAR_CHD = "max.year.chd";
	protected static final String MIN_YEAR_INF = "min.year.inf";
	protected static final String MAX_YEAR_INF = "max.year.inf";
	protected static final String KEEPALIVE_NUMBER_OF_TIMEOUT = "keepalivesession.numberOfTimeout";
	protected static final String KEEPALIVE_TIMEOUT = "keepalivesession.millisecondsTimeout";
	public static final String CLIENT_IP_HEADERS = "clientIp.headers";
	public static final String MAC_3DS_STRING = "sabre.3ds.mac";
	public static final String MAIL_CONFIRMATION_ALITALIA_TRADE = "mail.confirmation.alitaliatrade";
	public static final String EGON_USER = "egon.user";
	public static final String EGON_PASSWORD = "egon.password";
	public static final String EGON_DISABLED = "egon.disabled";
	public static final String FARERULES_SATIC_ON = "farerules.static.on";
	public static final String STS_MERCHANT_ID = "booking.sts.merchantid";

    public static final String LISTA_VOLI_SOGGETTI_APPROVAZIONE_GOVERNATIVA="voli.soggetti.approvazione.governativa";

	public String getStsMerchantId() {
		return PropertiesUtil.toString(
				getConfigurationProperty(STS_MERCHANT_ID), "");
	}
	
    @Reference
	private CryptoSupport cryptoSupport;
	
	/* EGON START */
	public String getEgonUser() {
		return PropertiesUtil.toString(
				getConfigurationProperty(EGON_USER), "");
	}
	
	public String getEgonPassword() throws CryptoException {
		return unprotect(PropertiesUtil.toString(
				getConfigurationProperty(EGON_PASSWORD), ""));
	}
	
	public boolean getEgonDisabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(EGON_DISABLED), false);
	}
	
	private String unprotect(String protectedValue) throws CryptoException {
		if (!protectedValue.startsWith("{")) {
			protectedValue = "{" + protectedValue + "}";
		}
		return cryptoSupport.unprotect(protectedValue);
	}
	/* EGON END */
	
	public boolean getHttpsEnabled() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(HTTPS_ENABLED), true);
	}
	
	public String getErrorNavigationPage() {
		return PropertiesUtil.toString(getConfigurationProperty(ERROR_NAVIGATION_PAGE), "");
	}
	
	public String getErrorNavigationPage_IT() {
		return PropertiesUtil.toString(getConfigurationProperty(ERROR_NAVIGATION_PAGE_IT), "");
	}
	
	public String getErrorNavigationPage_EN() {
		return PropertiesUtil.toString(getConfigurationProperty(ERROR_NAVIGATION_PAGE_EN), "");
	}
	
	public String getLinkGruppiPage() {
		return PropertiesUtil.toString(getConfigurationProperty(LINK_GRUPPI), "");
	}
	
	public String getBookingSearchPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_SEARCH_PAGE), "");
	}
	
	public String getBookingPassengersPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_PASSENGERS_PAGE), "");
	}
	
	public String getBookingConfirmPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_CONFIRM_PAGE), "");
	}
	
	public String getBookingPaymentPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_PAGE), "");
	}
	
	public String getBookingPayment3DSPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_PAYMENT_3DS_PAGE), "");
	}
	
	public String getBookingErrorPage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_ERROR_PAGE), "");
	}
	
	public String getBookingUnicreditRedirectFailurePage() {
		return PropertiesUtil.toString(getConfigurationProperty(BOOKING_UNICREDIT_REDIRECT_FAILURE), "");
	}
	
	public String getMinYearADT() {
		return PropertiesUtil.toString(getConfigurationProperty(MIN_YEAR_ADT), "");
	}
	
	public String getMaxYearADT() {
		return PropertiesUtil.toString(getConfigurationProperty(MAX_YEAR_ADT), "");
	}
	
	public String getMinYearCHD() {
		return PropertiesUtil.toString(getConfigurationProperty(MIN_YEAR_CHD), "");
	}
	
	public String getMaxYearCHD() {
		return PropertiesUtil.toString(getConfigurationProperty(MAX_YEAR_CHD), "");
	}
	
	public String getMinYearINF() {
		return PropertiesUtil.toString(getConfigurationProperty(MIN_YEAR_INF), "");
	}
	
	public String getMaxYearINF() {
		return PropertiesUtil.toString(getConfigurationProperty(MAX_YEAR_INF), "");
	}
	
	public int getNumberOfTimeout() {
		return PropertiesUtil.toInteger(getConfigurationProperty(KEEPALIVE_NUMBER_OF_TIMEOUT), 0);
	}
	
	public int getTimeout() {
		return PropertiesUtil.toInteger(getConfigurationProperty(KEEPALIVE_TIMEOUT), 0);
	}
	
	public String[] getClientIpHeaders() {
		return PropertiesUtil.toStringArray(getConfigurationProperty(CLIENT_IP_HEADERS), new String[0]);
	}
	
	public String get3dsMacString() {
		return PropertiesUtil.toString(
				getConfigurationProperty(MAC_3DS_STRING), "");
	}
	
	public String getMailConfirmationAlitaliaTrade(){
		return PropertiesUtil.toString(getConfigurationProperty(MAIL_CONFIRMATION_ALITALIA_TRADE), "confirmation@alitaliatrade.it");
	}
	
	public boolean isFareRulesStaticOn() {
		return PropertiesUtil.toBoolean(
				getConfigurationProperty(FARERULES_SATIC_ON), false);
	}

    public String getListaVoliSoggettiApprovazioneGovernativa() {
        return PropertiesUtil.toString(
                getConfigurationProperty(LISTA_VOLI_SOGGETTI_APPROVAZIONE_GOVERNATIVA) , "");
    }

	/* internals */
	
	private ComponentContext ctx = null;
	
	protected Object getConfigurationProperty(String propertyName) {
		if (ctx != null) {
			return ctx.getProperties().get(propertyName);
		}
		return null;
	}
	
	@Activate
	protected void activate(final ComponentContext ctx) {
		this.ctx = ctx;
	}
	
	@Modified
	private void modified(final ComponentContext ctx) {
		this.ctx = ctx;
	}

}