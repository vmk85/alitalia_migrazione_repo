package com.alitalia.aem.business.web.booking.model;

public enum PaymentType {
	CDC,
	BonificoOnLine
}
