package com.alitalia.aem.business.web.utils;

public class Constants {
	
	public static final String CONTENT_PATH = "/content/alitaliatrade";
	public static final String BOOKING_CONTEXT_ATTRIBUTE = "alitaliaTradeBookingSessionContext";
	
	public static final String AIRPORTS_JSON = "/content/alitaliatrade/airports.json";
	public static final String ERROR_PAGE = "/content/alitaliatrade/main/error.html";
	
	public static final String MESSAGE_GENERIC_EMPTY_FIELD = "Inserire {0}";
	public static final String MESSAGE_GENERIC_INVALID_FIELD = "{0} non valido";
	public static final String MESSAGE_GENERIC_CHECK_FIELD = "Verificare {0}";
	public static final String MESSAGE_GENERIC_ERROR = "Siamo spiacenti ma non &egrave; stato possibile procedere con l'operazione richiesta. Ti invitiamo a riprovare pi&ugrave; tardi";
	public static final String MESSAGE_GENERIC_INVALID_OPERATION =  "Siamo spiacenti ma non &egrave; possibile soddisfare la tua richiesta. Ti invitiamo a controllare i dati inseriti";
	public static final String MESSAGE_CHECK_NECESSARY = "E' necessario accettare le condizioni";
	public static final String MESSAGE_CHECK_PERSONAL_INFO = "E' necessario accettare il trattamento dei dati prsonali";
	public static final String MESSAGE_GENERIC_RADIO_EMPTY = "Selezionare un'opzione";
	
	public static final String MESSAGE_CVC_NOT_VALID = "Lunghezza CVC non valida";
	public static final String MESSAGE_DATE_NOT_VALID = "Data non valida";
	public static final String MESSAGE_IVA_NOT_VALID = "Partita iva non valida";
	public static final String MESSAGE_CODE_NOT_VALID = "Codice fiscale non valido";
	public static final String MESSAGE_CITY_NOT_VALID = "Città non valida";
	public static final String MESSAGE_NATION_NOT_VALID = "Stato non valido";
	public static final String MESSAGE_PROVINCE_NOT_VALID = "Provincia non è valida";
	public static final String MESSAGE_CAP_NOT_VALID = "Cap non valido";
	public static final String MESSAGE_EMAIL_NOT_VALID = "Indirizzo mail non valido";
	public static final String MESSAGE_DATE_PAST = "Data antecedente ad oggi";
	public static final String MESSAGE_DATE_FUTURE = "Data successiva ad oggi";
	public static final String MESSAGE_DATE_AFTER_NOT_VALID = "Data di partenza maggiore della data di arrivo";
	public static final String MESSAGE_CUG_NOT_VALID = "CUG non valido";
	public static final String MESSAGE_NADULTS_NOT_VALID = "Numero di adulti non valido";
	public static final String MESSAGE_NYOUNGS_NOT_VALID = "Numero di giovani non valido";
	public static final String MESSAGE_NMILITARY_NOT_VALID = "Numero di Militari non valido";
	public static final String MESSAGE_NKINDS_NOT_VALID = "Numero di bambini non valido";
	public static final String MESSAGE_NBABIES_NOT_VALID = "Numero di nenonati non valido";
	public static final String MESSAGE_SUM_ADULTS_KIDS_NOT_VALID = "Il Numero di adulti e di bambini deve essere inferiore a 7";
	public static final String MESSAGE_AIRPORT_FROM_NOT_VALID = "Aeroporto di partenza non valido";
	public static final String MESSAGE_AIRPORT_TO_NOT_VALID = "Aeroporto di arrivo non valido";
	public static final String MESSAGE_TIME_SLOT_NOT_VALID = "Fascia oraria non valida";
	public static final String MESSAGE_MAX_LENGTH = "Lunghezza massima superata";
	public static final String MESSAGE_INITIALS_NON_VALID = "Iniziali non valide";
	public static final String MESSAGE_FLIGHTNUMBER_NOT_VALID = "Numero volo non valido";
	public static final String MESSAGE_FROM_TO_DIFF = "Aeroporto di partenza e arrivo uguali";
	public static final String MESSAGE_PASSWORD_NOT_VALID = "Formato Password invalido.<br/> La password può contenere solamente caratteri alfanumerici e deve contenere almeno 8 caratteri di cui almeno una lettera maiuscola e un numero";
	public static final String MESSAGE_NUMBER_NOT_VALID = "Numero non valido";
	public static final String MESSAGE_BABY_PER_ADULT_NOT_VALID = "Numero di bambini maggiore del numero di adulti";
	public static final String MESSAGE_EMAILS_NOT_EQUALS = "Le e-mail non corrispondono";
	public static final String MESSAGE_PASSWORD_NOT_EQUALS = "Le password non corrispondono";
	public static final String MESSAGE_RESPONSIBLE_ADULTS_NOT_VALID = "Ogni adulto può essere responsabile solamente di un neonato";
	public static final String MESSAGE_EQUALS_NAMES = "Siamo spiacenti, ma non è possibile prenotare on line un biglietto per passeggeri con lo stesso nome e cognome";
	public static final String MESSAGE_IATA_NOT_VALID = "Siamo spiacenti, ma l’iscrizione non è andata a buon fine: errato codice Iata";
	public static final String MESSAGE_RADIO_CHOICE = "Scegli almeno un'opzione";	
	public static final String MESSAGE_EMPTY_FIELD = "Da inserire";
	public static final String MESSAGE_DATE_AFTER_NOT_VALID_MULTITRATTA = "Data tratta {0} precedente alla precedente";
	public static final String MESSAGE_SERVICE_ERROR = "Errore nel servizio";
	
	public static final String INPUT_SELECT_DEFAULT_VALUE = "Seleziona";
	
	public static final String MESSAGE_ERROR_INITIALIZE_CREDIT_CARD = "Ti invitiamo a riscrivere i dati della carta di credito";
	public static final String MESSAGE_ERROR_INITIALIZE_CREDIT_CARD_INFANT_INVENTORY_UNAVAILABLE = "Siamo spiacenti ma non possiamo procedere con l'emissione perch&egrave; il numero di neonati eccede il numero disponibile per questo volo.";
	public static final String MESSAGE_ERROR_AUTHORIZE = "Siamo spiacenti ma non &egrave; possibile procedere al pagamento online mediante il metodo da te scelto. Ti invitiamo a riprovare, utilizzando eventualmente un’altra forma di pagamento tra quelle previste";

	public static final String MESSAGE_ERROR_STS_GENERIC = "Siamo spiacenti ma non &egrave; stato possibile procedere con l'operazione richiesta. Ti invitiamo a riprovare.";
	public static final String MESSAGE_ERROR_INSUFFICIENT_PLAFOND = "Siamo spiacenti, ma l’importo della transazione supera il plafond attualmente a tua disposizione. Ti invitiamo a utilizzare  un’altra forma di pagamento tra quelle previste.";
	public static final String MESSAGE_ERROR_REJECTED_TRANSACTION = "Siamo spiacenti, la transazione &egrave; stata rifiutata. Ti invitiamo a contattare il back office STS al numero 06 89671111.";
	public static final String MESSAGE_ERROR_WRONG_DATA = "Siamo spiacenti ma non &egrave; stato possibile procedere con l’operazione richiesta. Ti invitiamo a controllare i dati inseriti o a utilizzare  un’altra forma di pagamento tra quelle previste.";
	public static final String MESSAGE_ERROR_TIME_OUT = "";
	public static final String MESSAGE_ERROR_FAILED_PAYMENT = "Gentile Agente, siamo spiacenti, ma per un problema tecnico non &egrave; stato possibile completare l'emissione del biglietto. La prenotazione del tuo cliente &egrave; invece confermata e verr&agrave; mantenuta per le prossime 24 ore. Chiama il numero 06 65640, seleziona il tasto 1 per assistenza web e comunica il codice della prenotazione, {PH_RES_CODE}. Potrai cos&igrave; finalizzare l'emissione del biglietto.";

    // cookies
    public static final String COOKIE_NO_THIRDPARTY_COOKIES = "no3rdparty";
    public static final String COOKIE_NO_MARKETING_COOKIES = "nomarketing";
	
	public static final int REPOSITORY_PATH_LANGUAGE_DEPTH = 4;
}
