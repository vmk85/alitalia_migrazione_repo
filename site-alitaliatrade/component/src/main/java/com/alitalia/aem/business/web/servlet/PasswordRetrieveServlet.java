package com.alitalia.aem.business.web.servlet;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.statistics.utils.TradeStatisticUtils;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.common.component.behaviour.exc.BehaviourException;
import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;
import com.alitalia.aem.common.data.home.enumerations.TradeStatisticDataTypeEnum;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordRequest;
import com.alitalia.aem.common.messages.home.AgencyRetrievePasswordResponse;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.common.messages.home.TradeRegisterStatisticRequest;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.service.api.home.exc.businessloginservice.RetrievePasswordByIDException;
import com.alitalia.aem.service.api.home.exc.businessloginservice.RetrievePasswordByVATException;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.alitalia.aem.web.component.tradestatistics.delegate.RegisterTradeStatisticsDelegate;
import com.day.cq.i18n.I18n;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = {"passwordretrieve"}),
	@Property(name = "sling.servlet.methods", value = { "POST"}),
	@Property(name = "sling.servlet.resourceTypes", value = {"cq/Page"}),
	@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
	@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.")
})
@SuppressWarnings("serial")
public class PasswordRetrieveServlet extends GenericFormValidatorServlet {
	
	private ComponentContext componentContext;
	
	@Reference
	private BusinessLoginDelegate businessLoginDelegate;
	
	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile RegisterTradeStatisticsDelegate registerStatisticsDelegate; 
	
	@Reference
	private AlitaliaTradeConfigurationHolder configuration;
	
	private String errorLanguage;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	private I18n createI18n(SlingHttpServletRequest request) 
	{
		errorLanguage = LocalizationUtils.getLanguageStatic();
		Locale locale = new Locale(errorLanguage);
		ResourceBundle resourceBundle = request.getResourceBundle(locale);
		I18n i18n = new I18n(resourceBundle);
				
		return i18n;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) 
			throws IOException 
	{
		I18n i18n = createI18n(request);
		String emptyField = i18n.get("Enter") + " {0}";			
		String usernameEmpty = i18n.get("Username_or_VAT_number");
		String fieldNotValid = "{0} " + i18n.get("not_valid");
		String accountType = i18n.get("Account_Type");
		String genericError = i18n.get("GenericError");
		
		usernameEmpty = usernameEmpty.replaceAll("_", " ");
		fieldNotValid = fieldNotValid.replaceAll("_", " ");
		accountType = accountType.replaceAll("_", " ");

		Validator validator = new Validator();
		validator.addDirectParameterMessagePattern("username", request.getParameter("username"), 
				emptyField, usernameEmpty, "isNotEmpty");
		validator.addDirectParameterMessagePattern("username", request.getParameter("username"), 
				genericError, "", "isUsername");
		validator.setAllowedValuesMessagePattern("usertype", request.getParameter("usertype"), 
				new String[] { "B", "T" }, fieldNotValid, accountType);
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws IOException {
		String tid = IDFactory.getTid();
		String sid = IDFactory.getSid(request);
		String codiceAgenzia = request.getParameter("username");
		String usertype = request.getParameter("usertype");
		try {
			AlitaliaTradeUserType ruolo = null;
			if ("B".equals(usertype)) {
				ruolo = AlitaliaTradeUserType.BANCONISTA;
			} else if ("T".equals(usertype)) {
				ruolo = AlitaliaTradeUserType.TITOLARE;
			} else {
				logger.error("Invalid role {0}", usertype);
				throw new RuntimeException("Invalid usertype.");
			}
			AgencyRetrievePasswordRequest serviceRequest = new AgencyRetrievePasswordRequest(tid, sid);
			serviceRequest.setIdentificativo(codiceAgenzia);
			serviceRequest.setRuolo(ruolo);
			
			String errorPage = (String) componentContext.getProperties().get("failure.page");
			
			AgencyRetrievePasswordResponse serviceResponse = new AgencyRetrievePasswordResponse();
			serviceResponse.setRetrieveSuccessful(false);
			try {
				serviceResponse = businessLoginDelegate.retrievePassword(serviceRequest);
			} catch (BehaviourException e)	{
				if (e.getCause() instanceof RetrievePasswordByVATException) {
					logger.error("Denied password retrieve by VAT for user {} ({}).", codiceAgenzia, usertype);
					errorPage = errorPage + "?retriveError=vat";
				} else if (e.getCause() instanceof RetrievePasswordByIDException){
					logger.error("Denied password retrieve by ID for user {} ({}).", codiceAgenzia, usertype);
					errorPage = errorPage + "?retriveError=id";
				} else {
					throw e;
				}
			
			} catch (RetrievePasswordByVATException e) {
				logger.error("Denied password retrieve by VAT for user {} ({}).", codiceAgenzia, usertype);
				errorPage = errorPage + "?retriveError=vat";
			} catch (RetrievePasswordByIDException e) {
				logger.error("Denied password retrieve by ID for user {} ({}).", codiceAgenzia, usertype);
				errorPage = errorPage + "?retriveError=id";
			}
			if (!serviceResponse.isRetrieveSuccessful()) {
				if (errorPage != null) {
					response.setStatus(301);
			        response.setHeader("Location", errorPage);
			        response.setHeader("Connection", "close");
				}
			} else {
				logger.debug("Granted password retrieve for user {} ({}).", codiceAgenzia, usertype);
				String successPage = (String) componentContext.getProperties().get("success.page");
				
				TradeRegisterStatisticRequest tradeStatisticrequest = TradeStatisticUtils.createStatisticRequet(TradeStatisticDataTypeEnum.GET_PSW, 
						request, true, null, usertype + ":" + codiceAgenzia, configuration.getClientIpHeaders());
				
				try {
					RegisterStatisticsResponse statisticResponse = registerStatisticsDelegate.registerStatistics(tradeStatisticrequest);
					if (statisticResponse == null) {
						logger.error("Statistic Service Failed");
					}
				} catch(Exception e) {
					logger.error("Statistic Service Failed");
				}
				
				if (successPage != null) {
					response.setStatus(301);
			        response.setHeader("Location", successPage);
			        response.setHeader("Connection", "close");
				}
			}
			
		} catch (Exception e) {
			logger.error("Error executing form submission.", e);
			String errorPage = (String) componentContext.getProperties().get("failure.page");
			if (errorPage != null) {
				response.setStatus(301);
		        response.setHeader("Location", errorPage);
		        response.setHeader("Connection", "close");
			}
		}
	}
	
}
