package com.alitalia.aem.business.web.utils;

public class HistogramItem {
	
	private String price;
	private String height;
	private String day;
	private String dayName;
	private String month;
	private String monthName;
	private String year;
	private String returnDate;
	private boolean isBestPrice;
	
	public HistogramItem() {
	}
	
	public HistogramItem(String price, String height, String day, String dayname, String returnDate) {
		super();
		this.price = price;
		this.height = height;
		this.day = day;
		this.dayName = dayname;
		this.returnDate = returnDate;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	public String getHeight() {
		return height;
	}
	
	public void setHeight(String height) {
		this.height = height;
	}
	
	public String getDay() {
		return day;
	}
	
	public void setDay(String day) {
		this.day = day;
	}
	
	public String getDayName() {
		return dayName;
	}
	
	public void setDayName(String dayName) {
		this.dayName = dayName;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonthName() {
		return monthName;
	}

	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}

	public boolean isBestPrice() {
		return isBestPrice;
	}

	public void setBestPrice(boolean isBestPrice) {
		this.isBestPrice = isBestPrice;
	}
	
	public String getReturnDate() {
		return returnDate;
	}

	public String getPrice() {
		return price;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;		
	}
}
