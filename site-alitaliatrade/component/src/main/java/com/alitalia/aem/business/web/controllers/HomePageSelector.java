package com.alitalia.aem.business.web.controllers;

import java.util.*;

import javax.servlet.http.*;

import org.apache.sling.api.*;
import org.apache.sling.api.resource.*;
import org.apache.sling.api.scripting.*;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.*;

public class HomePageSelector extends WCMUse {

	private static Logger logger = LoggerFactory.getLogger(HomePageSelector.class);

	public String language, market;
	private HashMap<String, String> geoResult;
	private String COOKIE_NAME = "alitaliatrade-consumer-geo";
	private int COOKIE_MAX_AGE = 7776000; // 90 days
	private String cookieValue = null;

	@Override
	public void activate() throws Exception {

		String queryString = "";

		try {

			logger.debug("Verifying home page redirection...");

			// load configuration
			SlingScriptHelper helper = getSlingScriptHelper();

			SlingSettingsService slingSettings = helper.getService(SlingSettingsService.class);
			if (slingSettings == null || !slingSettings.getRunModes().contains("publish")) {
				logger.debug("Not on a publish instance, ignoring.");
				return;
			}
			SlingHttpServletRequest request = getRequest();
			// geolocalizzazione
			geoResult = LocalizationUtils.getUserMarketLanguage(helper, request);
			language = geoResult.get("language");
			market = geoResult.get("market");

			ValueMap pageProperties = getCurrentPage().getProperties();

			boolean isIata = Boolean.parseBoolean(AlitaliaTradeAuthUtils.getProperty(request, "isIata"));
			queryString = getRequestQueryString(request);

			logger.debug("isIata attribute: {}", isIata);
			logger.debug("queryString attribute: {}", queryString);

			// modificare url (placeholder <language>)
			String link = null;
			String tempLink;
			if (isIata) {
				tempLink = (String) pageProperties.get("linkiata");
			} else {
				tempLink = (String) pageProperties.get("linknoniata");
			}

            if (language != null && language.equalsIgnoreCase("it")) {
                link = tempLink.replaceAll("<language>", language + "/loggato");
            }

            if (language != null && language.equalsIgnoreCase("en")) {
                link = tempLink.replaceAll("<language>", language + "/logged");
            }
            

            if (link != null) {
                logger.debug("Redirect to {}", link);
                SlingHttpServletResponse response = getResponse();
                response.setStatus(302);
                response.setHeader("Location", link + ".html" + queryString);
                response.setHeader("Connection", "close");
                if (response != null) {
//				    finche' non torna a funzionare la geolocalizzazione di Egon, non scriviamo cookie inutili e con contenuto vuoto
//					cookieValue = CookieUtils.toCookieString(market, language);
//					CookieUtils.setCookieValue(response, COOKIE_NAME, null, "/", COOKIE_MAX_AGE, cookieValue, false, false);
                }
                return;
            } else {
                link = tempLink.replaceAll("<language>", "it/loggato");
                if(link.equals("") || link != null) {
//                    if (isIata) {
//                        link = "/content/alitaliatrade/it/loggato/hp-iata";
//                    } else {
                        link = "/content/alitaliatrade";
//                    }
                }
                logger.debug("Redirect to {}", link);
                SlingHttpServletResponse response = getResponse();
                response.setStatus(302);
                response.setHeader("Location", link + ".html" + queryString);
                response.setHeader("Connection", "close");
                if (response != null) {
//				    finche' non torna a funzionare la geolocalizzazione di Egon, non scriviamo cookie inutili e con contenuto vuoto
//                    cookieValue = CookieUtils.toCookieString(market, language);
//                    CookieUtils.setCookieValue(response, COOKIE_NAME, null, "/", COOKIE_MAX_AGE, cookieValue, false, false);
                }
                return;
            }

		} catch (Exception e) {
			logger.error("Unexpected error", e);
			throw e;
		}
	}

	private String getRequestQueryString(HttpServletRequest request) {
		String queryString = request.getQueryString();
		if (queryString != null && queryString != "") {
			return "?" + queryString;
		} else {
			return "";
		}
	}

}
