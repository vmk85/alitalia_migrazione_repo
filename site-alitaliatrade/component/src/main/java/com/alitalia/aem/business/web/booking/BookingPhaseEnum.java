package com.alitalia.aem.business.web.booking;

public enum BookingPhaseEnum {
	
	INITIAL("initial"),
	FLIGHTS_SEARCH("flightsSearch"),
    FLIGHTS_SELECTION("flightsSelection"),
    PASSENGERS_DATA("passengersData"),
    PAYMENT("payment"),
    DONE("done");
    private final String value;
    
    BookingPhaseEnum(String v) {
        value = v;
    }
    
    public String value() {
    	return this.value;
    }
    
    public static BookingPhaseEnum fromValue(String v) {
        for (BookingPhaseEnum c: BookingPhaseEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}