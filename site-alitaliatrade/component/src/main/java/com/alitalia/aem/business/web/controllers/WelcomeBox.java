package com.alitalia.aem.business.web.controllers;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUse;

public class WelcomeBox extends WCMUse {

	private String urlimg;

	@Override
    public void activate() throws Exception {
    	Resource resource = getResource().getChild("image");
		ValueMap imageProperties = resource.getValueMap();
		urlimg = imageProperties.get("fileReference", String.class);
	}
	
	public String getUrlimg() {
		return urlimg;
	}

}