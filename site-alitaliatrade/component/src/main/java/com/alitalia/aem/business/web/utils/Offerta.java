package com.alitalia.aem.business.web.utils;

public class Offerta {

	private int id;
	private String destinazione;
	private String tipo;
	private String prezzo;
	private String link;
	
	public Offerta() {}
	
	public Offerta(String destinazione, String tipo, String prezzo, String link) {
		this.destinazione = destinazione;
		this.tipo = tipo;
		this.prezzo = prezzo;
		this.link = link;
	}
	
	public String getDestinazione() {
		return destinazione;
	}
	
	public void setDestinazione(String destinazione) {
		this.destinazione = destinazione;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getPrezzo() {
		return prezzo;
	}
	
	public void setPrezzo(String prezzo) {
		this.prezzo = prezzo;
	}
	
	public String getLink() {
		return link;
	}
	
	public void setLink(String link) {
		this.link = link;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
