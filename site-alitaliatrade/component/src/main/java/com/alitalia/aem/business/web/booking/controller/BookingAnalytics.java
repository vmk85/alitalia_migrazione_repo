package com.alitalia.aem.business.web.booking.controller;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.apache.sling.api.scripting.SlingScriptHelper;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.booking.BookingSearchCUGEnum;
import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.booking.model.FlightSelection;
import com.alitalia.aem.business.web.booking.model.Passenger;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.PassengerBaseData;
import com.alitalia.aem.common.data.home.PaymentProviderCreditCardData;
import com.alitalia.aem.common.data.home.ResultBookingDetailsData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;

public class BookingAnalytics extends BookingSessionGenericController{
	
	
	private static final String HYPHEN_FLIGHT_DATE_FORMAT = "yyyy-MM-dd";
	private static final String HYPHEN_EU_FLIGHT_DATE_FORMAT = "dd-MM-yy";
	private static final String EU_FLIGHT_DATE_FORMAT = "dd/MM/yyyy";
	private static final float FROM_MILLIS_TO_DAYS = 1000 * 60 * 60 * 24;
	
	
	private int step = -1;
	private String sabreSessionId = "";
	
	/*Phase 1*/
	private String travelType="";
	private int numAdults = 0;
	private int numYoung = 0;
	private int numChildren = 0;
	private int numInfant = 0;
	private int dayOfWeekA = 0;
	private int dayOfWeekR = 0;
	private String boApt = "";
	private String boCity = "";
	private String boCountry = "";
	private String arApt = "";
	private String arCity = "";
	private String arCountry = "";
	private String depDate = "";
	private String retDate = "";
	private String network = "";
	private String deltaBoToday = "";
	private String deltaBoAr = "";
	
	/*Phase 2*/
	private String eCoupon = "";
	private String totalPrice = "";
	private String fare = "";
	private String taxes = "";
	private String surcharges = "";
	private String depHour = "";
	private String depMinutes = "";
	private String depCost = "";
	private String depFlightNumber = "";
	private String depBrand = "";
	private String depFareBasis = "";
	private String retHour = "";
	private String retMinutes = "";
	private String retCost = "";
	private String retFlightNumber = "";
	private String retBrand = "";
	private String retFareBasis = "";
	private String netRevenue = "";
	private String netRevenueEuro = ""; /*tasso di conversione???*/
	private String totalPriceEuro = "";
	private String discount = "";
	
	/*Phase 3*/
	private String email = "";
	private String frequentFlyer = "";
	
	/*Phase 4*/
	private String tktPerPNR = "";
	private String tktNumber = "";
	private String ccType = "";
	private String invoice = "";
	private String idOrd = ""; /*receipt step only*/
	
	/*Formats and other*/
	private DecimalFormat priceFormat;
	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateFormat_eu_hyphen;
	private SimpleDateFormat dateFormat_eu;
	private Calendar today;
	
	@Override
	public void activate() {
		try {
			try {
				SlingScriptHelper helper = getSlingScriptHelper();
		    	configuration = helper.getService(AlitaliaTradeConfigurationHolder.class);
				ctx = (BookingSessionContext) getRequest().getSession(true).getAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE);
			} catch (Exception e) {
				ctx = null;
			}
			if(ctx != null){
				step = computeCurrentStep();
				initFormats();
				setPhase1Info();
				setPhase2Info();
				setPhase3Info();
				setPhase4Info();
				if(ctx.cookie != null){
					this.sabreSessionId = computeSabreSessionID(ctx.cookie);
				}
			}
		}
		// an error occurred...
		catch (Exception e) {
			logger.error("Unexpected error: ", e);
		}
	}



	private void initFormats() {
		priceFormat = new DecimalFormat("#0.00");
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setCurrencySymbol(""); 
		priceFormat.setDecimalFormatSymbols(otherSymbols);
		priceFormat.setGroupingUsed(false);
		priceFormat.setPositivePrefix("");
		priceFormat.setNegativePrefix("");
		dateFormat = new SimpleDateFormat(HYPHEN_FLIGHT_DATE_FORMAT);
		dateFormat.setLenient(false);
		dateFormat_eu_hyphen = new SimpleDateFormat(HYPHEN_EU_FLIGHT_DATE_FORMAT);
		dateFormat_eu_hyphen.setLenient(false);
		dateFormat_eu = new SimpleDateFormat(EU_FLIGHT_DATE_FORMAT);
		dateFormat_eu.setLenient(false);
		today = Calendar.getInstance();
		today.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));		
	}



	private void setPhase1Info() {
		/*info ricerca*/
		travelType = ctx.searchKind != null ? (BookingSearchKindEnum.SIMPLE.equals(ctx.searchKind) 
				? "OneWay" : "RoundTrip") : "";
		numAdults = ctx.searchPassengersNumber.getNumAdults();
		numYoung = ctx.searchPassengersNumber.getNumYoung();
		numChildren = ctx.searchPassengersNumber.getNumChildren();
		numInfant = ctx.searchPassengersNumber.getNumInfants();
		/*Flight Search info*/
		if (ctx.searchElements != null && !ctx.searchElements.isEmpty()) {
			boApt = ctx.searchElements.get(0).getFrom().getAirportCode();
			boCity = ctx.searchElements.get(0).getFrom().getCityCode();
			boCountry = ctx.searchElements.get(0).getFrom().getCountryCode();
			depDate = dateFormat.format(ctx.searchElements.get(0).getDepartureDate().getTime());
			dayOfWeekA = Math.floorMod(ctx.searchElements.get(0).getDepartureDate().get(Calendar.DAY_OF_WEEK) -2, 7) +1;
			long diffInMillies = ctx.searchElements.get(0).getDepartureDate().getTimeInMillis()
					- today.getTimeInMillis();
			deltaBoToday = String.valueOf((int)Math.ceil(diffInMillies / FROM_MILLIS_TO_DAYS));
			arApt = ctx.searchElements.get(0).getTo().getAirportCode();
			arCity = ctx.searchElements.get(0).getTo().getCityCode();
			arCountry = ctx.searchElements.get(0).getTo().getCountryCode();
			if (ctx.searchElements.size() > 1) {
				retDate = dateFormat.format(ctx.searchElements.get(1).getDepartureDate().getTime());
				dayOfWeekR = Math.floorMod(ctx.searchElements.get(1).getDepartureDate().get(Calendar.DAY_OF_WEEK) -2, 7) +1;				
				diffInMillies = ctx.searchElements.get(1).getDepartureDate().getTime().getTime()
						- ctx.searchElements.get(0).getDepartureDate().getTime().getTime();
				deltaBoAr = String.valueOf((int)Math.ceil(diffInMillies / FROM_MILLIS_TO_DAYS));
			}
		}
		network = computeNetwork();
		
	}
	

	private void setPhase2Info() {
		/*info volo selezionato e tariffe, eventuali modifiche alla data x ribbon*/
		eCoupon = (step > 1 ? (ctx.coupon == null ?  "NO" : "YES") : "");
		totalPrice = ctx.grossAmount != null ? priceFormat.format(ctx.grossAmount) : "";
		fare = ctx.netAmount != null ? priceFormat.format(ctx.netAmount) : "";
		taxes = ctx.totalTaxes != null ? priceFormat.format(ctx.totalTaxes) : "";
		surcharges = ctx.totalExtras != null ? priceFormat.format(ctx.totalExtras) : "";
		netRevenue = ctx.netAmount != null && ctx.totalExtras != null 
				? priceFormat.format(ctx.netAmount.add(ctx.totalExtras)) : "";
		discount = ctx.totalCouponPrice != null ? priceFormat.format(ctx.totalCouponPrice) : "";
		
		if (ctx.flightSelections != null) {
			FlightSelection departure = ctx.flightSelections[0];
			Calendar departureDateForDelta = Calendar.getInstance();
			if (departure != null) {
				DirectFlightData flight;
				if (departure.getFlightData() instanceof DirectFlightData) {
					flight = (DirectFlightData) departure.getFlightData();
					depFlightNumber = flight.getCarrier() + flight.getFlightNumber();
				} else {
					flight = (DirectFlightData) ((ConnectingFlightData) departure.getFlightData()).getFlights().get(0);
					for(FlightData fl : ((ConnectingFlightData) departure.getFlightData()).getFlights()){
						DirectFlightData dirFl = (DirectFlightData) fl;
						depFlightNumber += dirFl.getCarrier() + dirFl.getFlightNumber() + ",";
					}
					/*Remove last comma (,)*/
					if(depFlightNumber.length() > 0){
						depFlightNumber = depFlightNumber.substring(0, depFlightNumber.length() -1);
					}
				}
				depDate = dateFormat.format(flight.getDepartureDate().getTime());
				departureDateForDelta = flight.getDepartureDate();
				depHour = String.valueOf(flight.getDepartureDate().get(Calendar.HOUR_OF_DAY));
				depMinutes = String.valueOf(flight.getDepartureDate().get(Calendar.MINUTE));
				long diffInMillies = flight.getDepartureDate().getTime().getTime() - today.getTime().getTime();
				deltaBoToday = String.valueOf((int)Math.ceil(diffInMillies / FROM_MILLIS_TO_DAYS));
				dayOfWeekA = Math.floorMod(flight.getDepartureDate().get(Calendar.DAY_OF_WEEK) -2, 7) +1;
				// search for selected brand
				for (BrandData brand : flight.getBrands()) {
					if (brand.getCode().equals(departure.getSelectedBrandCode())) {
						depCost = priceFormat.format(brand.getGrossFare());
						depBrand = computeBrandName(ctx.cug, brand);
					}
				}
			}
			if (ctx.flightSelections.length > 1) {
				FlightSelection arrival = ctx.flightSelections[1];
				if (arrival != null) {
					DirectFlightData flight;
					if (arrival.getFlightData() instanceof DirectFlightData) {
						flight = (DirectFlightData) arrival.getFlightData();
						retFlightNumber = flight.getCarrier() + flight.getFlightNumber();
					} else {
						flight = (DirectFlightData) ((ConnectingFlightData) arrival.getFlightData()).getFlights()
								.get(0);
						for(FlightData fl : ((ConnectingFlightData) arrival.getFlightData()).getFlights()){
							DirectFlightData dirFl = (DirectFlightData) fl;
							retFlightNumber += dirFl.getCarrier() + dirFl.getFlightNumber() + ",";
						}
						/*Remove last comma (,)*/
						if(retFlightNumber.length() > 0){
							retFlightNumber = retFlightNumber.substring(0, retFlightNumber.length() -1);
						}
					}
					retDate = dateFormat.format(flight.getDepartureDate().getTime());
					retHour = String.valueOf(flight.getDepartureDate().get(Calendar.HOUR_OF_DAY));
					retMinutes = String.valueOf(flight.getDepartureDate().get(Calendar.MINUTE));
					long diffInMillies = flight.getDepartureDate().getTime().getTime()
							- departureDateForDelta.getTime().getTime();
					deltaBoAr = String.valueOf((int)Math.ceil(diffInMillies / FROM_MILLIS_TO_DAYS));
					dayOfWeekR = Math.floorMod(flight.getDepartureDate().get(Calendar.DAY_OF_WEEK) -2, 7) +1;
					// search for selected brand
					for (BrandData brand : flight.getBrands()) {
						if (brand.getCode().equals(arrival.getSelectedBrandCode())) {
							retCost = priceFormat.format(brand.getGrossFare());
							retBrand = computeBrandName(ctx.cug, brand);
						}
					}

				}
			}
		}
		/*Fare Basis*/
		try{
			if(ctx.selectionRoutes != null){
				ResultBookingDetailsData bookingData = (ResultBookingDetailsData) 
						ctx.selectionRoutes.getProperties().get("BookingDetails");
				depFareBasis = bookingData.getSolutionField().get(0)
						.getPricingField().get(0).getFareField().get(0).getExtendedFareCodeField();
				if(!ctx.searchKind.equals(BookingSearchKindEnum.SIMPLE)){
					retFareBasis = bookingData.getSolutionField().get(0)
							.getPricingField().get(0).getFareField().get(1).getExtendedFareCodeField();
				}
			}
		}
		catch(Exception e){
			logger.error("Unable to get Fare Basis Info");
		}
	}
	
	private void setPhase3Info() {
		/*info pax*/
		if(ctx.passengersData != null){
			email = ctx.passengersData.getEmail();
			for(Passenger pax : ctx.passengersData.getPassengersList()){
				if(pax.getFrequentFlyerProgram() != null 
						&& pax.getFrequentFlyerProgram().length() >0)
					frequentFlyer += computeFrequentFlyerDescription(pax.getFrequentFlyerProgram()) + ";";
			}
			if(frequentFlyer.length() > 0){
				/*remove last ";"*/
				frequentFlyer = frequentFlyer.substring(0, frequentFlyer.length()-1);
			}
		}
	}
	
	private void setPhase4Info() {
		/*info payment method and receipt*/
		/*Phase 4*/
		/*private String tktPerPNR = "";
		private String tktNumber = "";
		private String ccType = "";
		private String invoice = "";
		private String idOrd = "";*/
		RoutesData prenotation = ctx.prenotation;
		
		invoice = ctx.invoiceRequired != null ? (ctx.invoiceRequired ? "1" : "0") : "";
		
		if (prenotation != null) {
			String PNR = prenotation.getPnr();
			Calendar now = Calendar.getInstance();
			now.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
			idOrd = dateFormat_eu.format(now.getTime()) + PNR; 
			
			if(prenotation.getPayment() != null && prenotation.getPayment().getProvider() != null 
					&& prenotation.getPayment().getProvider() instanceof PaymentProviderCreditCardData){
					ccType = ((PaymentProviderCreditCardData)prenotation.getPayment().getProvider())
							.getType().value();
			}
			if(prenotation.getPassengers() != null){
				if(prenotation.getPassengers().get(0).getTickets() != null){
					tktNumber = prenotation.getPassengers().stream()
							.flatMap(pass -> pass.getTickets().stream())
							.map(tkt -> tkt.getTicketNumber()).distinct()
							.collect(Collectors.joining(","));
					int tktPerPNR = 0;
					for(PassengerBaseData pax : prenotation.getPassengers()){
						tktPerPNR += pax.getTickets().stream().map( t -> t.getTicketNumber()).distinct().count();
					}
					this.tktPerPNR = String.valueOf(tktPerPNR);				
				}
				
			}
		}
 	}
	

	private int computeCurrentStep() {
		int currentStep = 0;
		if(ctx.phase != null){
			switch(ctx.phase){
			case INITIAL:
			case FLIGHTS_SEARCH:
			case FLIGHTS_SELECTION:
				currentStep = 1;
				break;
			case PASSENGERS_DATA:
				currentStep = 2;
				break;
			case PAYMENT:
				currentStep = 3;
				break;
			case DONE:
				currentStep = 4;
			}
		}
		return currentStep;
	}
	
	/**
	 * 
	 * @return "NAZ" se volo domestico, "INZ" se internazionale, "INC" se intercontinentale,
	 * 		"" (blank) in caso di errore
	 */
	private String computeNetwork() {
		String netw="";
		if (ctx.availableFlights != null && ctx.availableFlights.getRoutes() != null 
				&& !ctx.availableFlights.getRoutes().isEmpty()) {
			FlightData flightData = ctx.availableFlights.getRoutes().get(0).getFlights().get(0);
			AreaValueEnum areaFrom = null;
			AreaValueEnum areaTo = null;
			if (flightData instanceof DirectFlightData) {
				DirectFlightData directFlightData = (DirectFlightData) flightData;
				areaFrom = directFlightData.getFrom().getArea();
				areaTo = directFlightData.getTo().getArea();
			} else {
				ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
				DirectFlightData first = (DirectFlightData) connectingFlightData.getFlights().get(0);
				DirectFlightData last = (DirectFlightData) connectingFlightData.getFlights().get(connectingFlightData.getFlights().size() - 1);
				areaFrom = first.getFrom().getArea();
				areaTo = last.getTo().getArea();
			}
			AreaValueEnum flightType = areaFrom == AreaValueEnum.DOM ? areaTo : areaFrom;
			switch(flightType){
			case DOM:
				netw = "NAZ";
				break;
			case INTZ:
				netw = "INZ";
				break;
			case INTC:
				netw = "INC";
				break;
			}
		}
		return netw;
	}
	
	private String computeBrandName(BookingSearchCUGEnum cug, BrandData brandData) {
		String brandName ="";
		if ( brandData.getCode().toLowerCase().equals("yc") ) {
			if (cug == BookingSearchCUGEnum.MILITARY) {
				return "MILITARI POLIZIA";
			} else { //caso ADT
				return "ECONOMY CLASSIC";
			}
		}
		if ( brandData.getCode().toLowerCase().equals("fa") ) {
			if (cug == BookingSearchCUGEnum.FAMILY) {
				return "FAMILY";
			} else { //caso ADT
				return "ECONOMY BASIC";
			}
		}
		switch(brandData.getCode().toLowerCase()){
			case "economybasic":{
				brandName="ECONOMY BASIC";
			}break;
			case "yl":{
				brandName="ECONOMY LIGHT";
			}break;
			case "yc":{
				brandName="ECONOMY CLASSIC";
			}break;
			case "yf":{
				brandName="ECONOMY FLEX";
			}break;
			case "co":{
				brandName="COMFORT FLEX";
			}break; 
			case "yp":{
				brandName="PREMIUM ECONOMY";
			}break;
			case "yd":{
				brandName="ECONOMY PROMO";
			}break;
			case "ys":{
				brandName="ECONOMY SAVER";
			}break;
			case "jc":{
				brandName="BUSINESS CLASSIC";
			}break;
			case "jf":{
				brandName="BUSINESS FLEX";
			}break;
			case "zz":{
				brandName="GIOVANI";
			}break;
			case "tc":{
				brandName="CONTINUITà TERRITORIALE";
			}break;
		}
		return brandName;
	}
	
	private String computeFrequentFlyerDescription(String code){
		String description = "";
		boolean trovato = false;
		for(int i= 0; i<ctx.frequentFlyerTypes.size() && !trovato ; i++){
			if(ctx.frequentFlyerTypes.get(i).getCode().equals(code)){
				description = ctx.frequentFlyerTypes.get(i).getDescription();
				trovato = true;
			}
		}
		return description;
	}
	
	private String computeSabreSessionID(String cookie) {
		String jsession = "";
		int index = cookie.indexOf("JSESSIONID=");
		if(index >= 0 && cookie.length() >= index + 11 + 32){
			jsession = cookie.substring(index + 11, index + 11 + 32);
			/*JSESSIONID vuoto o non valido*/
			if(jsession.matches(".*[\",.;=].*")){
				jsession = "";
			}
		}
		return jsession;
	}

	

	public int getStep() {
		return step;
	}



	public String getTravelType() {
		return travelType;
	}



	public int getNumAdults() {
		return numAdults;
	}



	public int getNumYoung() {
		return numYoung;
	}



	public int getNumChildren() {
		return numChildren;
	}



	public int getNumInfant() {
		return numInfant;
	}



	public int getDayOfWeekA() {
		return dayOfWeekA;
	}



	public int getDayOfWeekR() {
		return dayOfWeekR;
	}



	public String getBoApt() {
		return boApt;
	}



	public String getBoCity() {
		return boCity;
	}



	public String getBoCountry() {
		return boCountry;
	}



	public String getArApt() {
		return arApt;
	}



	public String getArCity() {
		return arCity;
	}



	public String getArCountry() {
		return arCountry;
	}



	public String getDepDate() {
		return depDate;
	}



	public String getRetDate() {
		return retDate;
	}



	public String getNetwork() {
		return network;
	}



	public String getDeltaBoToday() {
		return deltaBoToday;
	}



	public String getDeltaBoAr() {
		return deltaBoAr;
	}



	public String getECoupon() {
		return eCoupon;
	}



	public String getTotalPrice() {
		return totalPrice;
	}



	public String getFare() {
		return fare;
	}



	public String getTaxes() {
		return taxes;
	}



	public String getSurcharges() {
		return surcharges;
	}



	public String getDepHour() {
		return depHour;
	}



	public String getDepMinutes() {
		return depMinutes;
	}



	public String getDepCost() {
		return depCost;
	}



	public String getDepFlightNumber() {
		return depFlightNumber;
	}



	public String getDepBrand() {
		return depBrand;
	}



	public String getDepFareBasis() {
		return depFareBasis;
	}



	public String getRetHour() {
		return retHour;
	}



	public String getRetMinutes() {
		return retMinutes;
	}



	public String getRetCost() {
		return retCost;
	}



	public String getRetFlightNumber() {
		return retFlightNumber;
	}



	public String getRetBrand() {
		return retBrand;
	}



	public String getRetFareBasis() {
		return retFareBasis;
	}



	public String getNetRevenue() {
		return netRevenue;
	}



	public String getNetRevenueEuro() {
		return netRevenueEuro;
	}



	public String getTotalPriceEuro() {
		return totalPriceEuro;
	}



	public String getDiscount() {
		return discount;
	}



	public String getEmail() {
		return email;
	}



	public String getFrequentFlyer() {
		return frequentFlyer;
	}



	public String getTktPerPNR() {
		return tktPerPNR;
	}



	public String getTktNumber() {
		return tktNumber;
	}



	public String getCcType() {
		return ccType;
	}



	public String getInvoice() {
		return invoice;
	}



	public String getIdOrd() {
		return idOrd;
	}



	public String getSabreSessionId() {
		return sabreSessionId;
	}
}
