package com.alitalia.aem.business.web.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.scripting.SlingBindings;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.ComponentContext;

import com.alitalia.aem.business.web.utils.AlitaliaTradeAuthUtils;
import com.alitalia.aem.business.web.utils.AnagraficaUtils;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.MunicipalityData;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataRequest;
import com.alitalia.aem.common.messages.home.AgencyUpdateDataResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "agencyprofileupdate" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.")
})
@SuppressWarnings("serial")
public class AgencyProfileUpdateServlet extends GenericFormValidatorServlet {
	
	private static String COD_ITA = "ITA";
	
	private ComponentContext componentContext;
	
	@Reference
	private BusinessLoginDelegate businessLoginDelegate;
	
	@Reference
	private StaticDataDelegate staticDataDelegate;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request)
			throws IOException {
		
		Validator validator = new Validator();
		String market = "IT";
		String language= LocalizationUtils.getLocaleByResource(request.getResource(), "it").toString().toUpperCase();
		
		/*String ragioneSociale = request.getParameter("ragioneSociale");
		validator.addDirectParameterMessagePattern("ragioneSociale", ragioneSociale, Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Ragione sociale", "isNotEmpty");
		validator.addDirectParameter("ragioneSociale", ragioneSociale, Constants.MESSAGE_MAX_LENGTH, "maxLengthAddress");*/
		
		String stato = request.getParameter("stato");
		String partitaIva = request.getParameter("partitaIva");
		
		if (stato!= null && stato.equals(COD_ITA)) {
			validator.addDirectParameterMessagePattern("partitaIva", partitaIva, Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Partita Iva", "isNotEmpty");
		}
		if (stato!= null && stato.equals(COD_ITA) || (partitaIva != null && !partitaIva.isEmpty())) {
			validator.addDirectParameterMessagePattern("partitaIva", partitaIva, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Partita Iva", "isIva");
		}
		
		String codiceFiscale = request.getParameter("codiceFiscale");
		
		if (codiceFiscale!= null && !codiceFiscale.isEmpty()) {
			validator.addDirectParameterMessagePattern("codiceFiscale", codiceFiscale, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Codice Fiscale", "isCodiceFiscale");
		}
		
		/* Validate the parameter is in the select*/
		Map<String, String> mapStati = AnagraficaUtils.obtainMapState();
		String[] allowedStateValues = (String[]) mapStati.keySet().toArray(new String[0]);
		validator.setAllowedValuesMessagePattern("stato", request.getParameter("stato"), allowedStateValues, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Stato");
		
		List<CountryData> provinces = AnagraficaUtils.obtainListOfProvinces(staticDataDelegate,request,market,language);
		String[] allowedProvincesValues = new String[provinces.size()];
		for (int i=0; i < provinces.size(); i++){
			allowedProvincesValues[i] = provinces.get(i).getStateCode();
		}
		validator.setAllowedValuesMessagePattern("provincia", request.getParameter("provincia"), allowedProvincesValues, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Provincia");

		List<MunicipalityData> councils = AnagraficaUtils.obtainCouncilsList(staticDataDelegate,request,market,language);
		String[] allowedCouncilsValues = new String[councils.size()];
		for (int i=0; i < councils.size(); i++){
			allowedCouncilsValues[i] = councils.get(i).getDescription();
		}
		validator.setAllowedValuesMessagePattern("citta", request.getParameter("citta"), allowedCouncilsValues, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Citt&agrave;");
		/* end */
		
		validator.addDirectParameterMessagePattern("stato", request.getParameter("stato"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Stato", "isNotEmpty");
		validator.addDirectParameterMessagePattern("provincia", request.getParameter("provincia"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Provincia", "isNotEmpty");
		validator.addDirectParameterMessagePattern("citta", request.getParameter("citta"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Citt&agrave;", "isNotEmpty");
		
		validator.addDirectParameterMessagePattern("cap", request.getParameter("cap"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "C.A.P.", "isNotEmpty");
		validator.addDirectParameterMessagePattern("cap", request.getParameter("cap"), Constants.MESSAGE_GENERIC_INVALID_FIELD, "C.A.P.", "isCAP");
		
		validator.addDirectParameterMessagePattern("indirizzo", request.getParameter("indirizzo"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Indirizzo", "isNotEmpty");
		validator.addDirectParameter("indirizzo", request.getParameter("indirizzo"), Constants.MESSAGE_MAX_LENGTH, "maxLengthAddress");
		
		validator.addDirectParameterMessagePattern("telefono", request.getParameter("telefono"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "Telefono", "isNotEmpty");
		validator.addDirectParameterMessagePattern("telefono", request.getParameter("telefono"), Constants.MESSAGE_GENERIC_INVALID_FIELD, "Telefono", "isNumber");
		
		String fax = request.getParameter("fax");
		if (fax != null && !fax.isEmpty()) {
			validator.addDirectParameterMessagePattern("fax", fax, Constants.MESSAGE_GENERIC_INVALID_FIELD, "Fax", "isNumber");
		}
		
		validator.addDirectParameterMessagePattern("emailB", request.getParameter("emailB"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "E-mail", "isNotEmpty");
		validator.addDirectParameterMessagePattern("emailB", request.getParameter("emailB"), Constants.MESSAGE_GENERIC_INVALID_FIELD, "E-mail", "isEmail");
		
		validator.addDirectParameterMessagePattern("emailT", request.getParameter("emailT"), Constants.MESSAGE_GENERIC_EMPTY_FIELD, "E-mail", "isNotEmpty");
		validator.addDirectParameterMessagePattern("emailT", request.getParameter("emailT"), Constants.MESSAGE_GENERIC_INVALID_FIELD, "E-mail", "isEmail");
		
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		
		try {
			// geolocalizzazione
			String language = "it";
			try {
				SlingBindings bindings = (SlingBindings) request.getAttribute(SlingBindings.class.getName());
		        SlingScriptHelper scriptHelper = bindings.getSling();
				HashMap<String, String> geoResult = LocalizationUtils.getUserMarketLanguage(scriptHelper,request);
				language = geoResult.get("language");
			}
			catch(Exception e) {
				logger.error(e.getMessage());
				
			}
			
			
			AgencyUpdateDataRequest serviceRequest = new AgencyUpdateDataRequest(IDFactory.getTid(), IDFactory.getSid(request));
			
			String codiceAgenzia = AlitaliaTradeAuthUtils.getProperty(request, "codiceAgenzia");
			serviceRequest.setCodiceAgenzia(codiceAgenzia);
			serviceRequest.setRagioneSociale(request.getParameter("ragioneSociale").trim());
			serviceRequest.setPartitaIVA(request.getParameter("partitaIva").trim());
			serviceRequest.setCodiceFiscale(request.getParameter("codiceFiscale").trim());
			serviceRequest.setStato(request.getParameter("stato").trim());
			serviceRequest.setProvincia(request.getParameter("provincia").trim());
			serviceRequest.setCitta(request.getParameter("citta").trim());
			serviceRequest.setZip(request.getParameter("cap").trim());
			serviceRequest.setIndirizzo(request.getParameter("indirizzo").trim());
			serviceRequest.setTelefono(request.getParameter("telefono").trim());
			serviceRequest.setFax(request.getParameter("fax").trim());
			serviceRequest.setEmailOperatore(request.getParameter("emailB").trim());
			serviceRequest.setEmailTitolare(request.getParameter("emailT").trim());
			serviceRequest.setFlagSendMail(false);
			
			//retrive extra information to correctly update agency information
			@SuppressWarnings("unchecked")
			Map<String,Object> extraAgencyInfo = (Map<String, Object>) request.getSession().getAttribute("extraAgencyInfo");
			serviceRequest.setCodiceAccordo((String)extraAgencyInfo.get("codiceAccordo"));
			serviceRequest.setCodiceSirax((String)extraAgencyInfo.get("codiceSirax"));
			if (extraAgencyInfo.get("dataValidita") != null ) {
				Date data = (Date)extraAgencyInfo.get("dataValidita");
				serviceRequest.setDataValiditaAccordo(data);
			} else {
				serviceRequest.setDataValiditaAccordo(null);
			}
			
			serviceRequest.setCodFamCon((String)extraAgencyInfo.get("codiceFamCom"));
			
			AgencyUpdateDataResponse serviceResponse = businessLoginDelegate.updateAgencyData(serviceRequest);
			
			
			if (serviceResponse.isSuccessful() != null && serviceResponse.isSuccessful()) {
				
				logger.debug("Profile updated for user {}.", codiceAgenzia);
				String successPage = (String) componentContext.getProperties().get("success.page");				
				if (language.equalsIgnoreCase("it")) {
					successPage = successPage.replaceAll("main/loggato", "it/loggato");
				}				
				else {
					successPage = successPage.replaceAll("main/loggato", "en/logged");
				}				
				if (successPage != null) {
					response.sendRedirect(response.encodeRedirectURL(successPage));
				}
				
			} else {
				logger.debug("Error during profile update for user {}.", codiceAgenzia);
				
				String errorPage = (String) componentContext.getProperties().get("failure.page");
				if (language.equalsIgnoreCase("it")) {
					errorPage = errorPage.replaceAll("main/loggato", "it/loggato");
				}				
				else {
					errorPage = errorPage.replaceAll("main/loggato", "en/logged");
				}	
				if (errorPage != null) {
					response.sendRedirect(response.encodeRedirectURL(errorPage));
				}
			}
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			String errorPage = (String) componentContext.getProperties().get("failure.page");
			if (errorPage != null) {
				response.sendRedirect(response.encodeRedirectURL(errorPage));
			}
		}
	}
}
