package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.utils.AlitaliaTradeAuthUtils;
import com.day.cq.commons.TidyJSONWriter;

@SuppressWarnings("serial")
@Component
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.selectors", value = { "agencyuser" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }) })
public class AgencyUserServlet extends SlingSafeMethodsServlet {

	private static final Logger logger = LoggerFactory.getLogger(AgencyUserServlet.class);
	
	@Override
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {

		response.setContentType("application/json");
		TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
		
		try {
			out.object();
			String username = "";
			try {
				username = AlitaliaTradeAuthUtils.getProperty(request, "ragioneSociale");
			} catch (Exception e) {
			}
			out.key("username").value(username);
			out.endObject();
		} catch (Exception e) {
			logger.error("", e);
		}
	}
}
