package com.alitalia.aem.business.web.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.AnagraficaUtils;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.MunicipalityData;
import com.alitalia.aem.common.messages.home.AgencyRetrieveDataResponse;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;

public class ModificaAnagrafica extends WCMUse {
	
	private Map<String, String> agenzia;
	private Map<String, String> mapStati;
	private Map<String, String> mapProvince;
	private Map<String, String> mapCitta;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
    public void activate() throws Exception {
		
		agenzia = null;
		Map<String, Object> extraAgencyInfo = new LinkedHashMap<String, Object>();
		mapProvince = new LinkedHashMap<String, String>();
		mapStati = new LinkedHashMap<String, String>();
		mapCitta = new LinkedHashMap<String, String>();
		
		try{
			SlingHttpServletRequest request = getRequest();
			SlingScriptHelper helper = getSlingScriptHelper();
			String market = "IT";
			String language= LocalizationUtils.getLocaleByResource(request.getResource(), "it").toString().toUpperCase();
			
			StaticDataDelegate staticDataDelegate = helper.getService(StaticDataDelegate.class);
			BusinessLoginDelegate businessLoginDelegate = helper.getService(BusinessLoginDelegate.class);
			
			//obtain the Agency Information
			AgencyRetrieveDataResponse serviceResponse = AnagraficaUtils.obtainAgencyInformation(businessLoginDelegate,request);
			if (serviceResponse != null) {
				
				agenzia = new HashMap<String, String>();
				agenzia.put("ragSociale", serviceResponse.getRagioneSociale().trim());
				agenzia.put("piva", serviceResponse.getPartitaIVA().trim());
				agenzia.put("cf", serviceResponse.getCodiceFiscale().trim());
				agenzia.put("indirizzo", serviceResponse.getIndirizzo().trim());
				
				//obtain the correct state
				mapStati=AnagraficaUtils.obtainMapState();
				String stato = mapStati.get(serviceResponse.getStato().trim());
				String statoValidato = "Seleziona uno stato";
				if (stato.equalsIgnoreCase("italia")) {
					statoValidato = "ITA";
				}
				if (stato.equalsIgnoreCase("san marino")) {
					statoValidato = "SM";
				}
				agenzia.put("stato", statoValidato);
				
				//Obtain the correct Province by the stateCode
				List<CountryData> listProvinces = AnagraficaUtils.obtainListOfProvinces(staticDataDelegate,request,market,language);
				if (listProvinces != null && !listProvinces.isEmpty()) {
					for (CountryData item : listProvinces) {
						mapProvince.put(item.getStateCode(), item.getStateDescription());
					}
				}
				String provincia = serviceResponse.getProvincia().trim();
				String defaultProvinceText = "Selezionare una provincia";
				String provinciaValidata = defaultProvinceText;
				if (listProvinces != null && !listProvinces.isEmpty()) {
					for (CountryData item : listProvinces) {
						if (item.getStateCode().equalsIgnoreCase(provincia)) {
							provinciaValidata = item.getStateCode();
							break;
						}
					}
				}
				agenzia.put("provincia", provinciaValidata);
				
				String cittaValidata = "Selezionare una citta";
				if (!provinciaValidata.equalsIgnoreCase(defaultProvinceText)) {
					//Obtain the correct City by the councilCode
					List<MunicipalityData> allCouncilsList = AnagraficaUtils.obtainCouncilsList(staticDataDelegate,request,market,language);
					List<MunicipalityData> councilsList = new ArrayList<MunicipalityData>();
					if (allCouncilsList != null && !allCouncilsList.isEmpty()) {
						for (MunicipalityData city : allCouncilsList) {
							if (city.getProvinceCode().equalsIgnoreCase(provinciaValidata)) {
								councilsList.add(city);
								mapCitta.put(city.getDescription(), city.getDescription());
							}
						}
					}
					String citta = serviceResponse.getCitta().trim();
					if (councilsList != null && !councilsList.isEmpty()) {
						for (MunicipalityData item : councilsList) {
							if (item.getDescription().equalsIgnoreCase(citta)) {
								cittaValidata = item.getDescription();
								break;
							}
						}
					}
				}
				agenzia.put("citta", cittaValidata);
				
				agenzia.put("cap", serviceResponse.getZip().trim());
				agenzia.put("tel", serviceResponse.getTelefono().trim());
				agenzia.put("fax", serviceResponse.getFax().trim());
				agenzia.put("emailTitolare", serviceResponse.getEmailTitolare().trim());
				agenzia.put("emailBanconista", serviceResponse.getEmailPubblica().trim());
				
				//save extra information to correctly update the agency information
				String codiceAccordo = serviceResponse.getCodiceAccordo();
				if (codiceAccordo != null) {
					codiceAccordo = codiceAccordo.trim();
				} else {
					codiceAccordo = "";
				}
				extraAgencyInfo.put("codiceAccordo", codiceAccordo);
				
				String codiceSirax = serviceResponse.getCodiceSirax();
				if (codiceSirax != null) {
					codiceSirax = codiceSirax.trim();
				} else {
					codiceSirax = "";
				}
				extraAgencyInfo.put("codiceSirax", codiceSirax);
				
				Date dataValidita = serviceResponse.getDataValiditaAccordo();
				extraAgencyInfo.put("dataValidita", dataValidita);
				
				String codiceFamCom = serviceResponse.getCodFamCom();
				if (codiceFamCom != null) {
					codiceFamCom = codiceFamCom.trim();
				} else {
					codiceFamCom = "";
				}
				extraAgencyInfo.put("codiceFamCom", codiceFamCom);
				
				getRequest().getSession(true).setAttribute("extraAgencyInfo", extraAgencyInfo);
			}
				
		}catch(Exception e){
			logger.error("Unexpected error: ", e);
			throw e;
		}
		
	}
	
	public Map<String, String> getAgenzia() {
		return agenzia;
	}
	
	public Map<String, String> getMapStati() {
		return mapStati;
	}
	
	public Map<String, String> getMapProvince() {
		return mapProvince;
	}
	
	public Map<String, String> getMapCitta() {
		return mapCitta;
	}
	
}