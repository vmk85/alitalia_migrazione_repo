package com.alitalia.aem.business.web.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingSession;
import com.day.cq.commons.TidyJSONWriter;

@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "keepalivesession" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "sling.servlet.methods", value = { "GET" }),
	@Property(name = "failure.page") })
@SuppressWarnings("serial")
public class KeepAliveSessionServlet extends SlingSafeMethodsServlet {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Reference
	private BookingSession bookingSession;
	
	@Override
	protected void doGet(SlingHttpServletRequest request, 
			SlingHttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		try {
			json.object();
			json.endObject();
		} catch (Exception e) {
			logger.error("Unexpected error creating JSON", e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
}
	
