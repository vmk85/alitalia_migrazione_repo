package com.alitalia.aem.business.web.utils;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;

import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;

public class AlitaliaTradeAuthUtils {
	
	public static Authorizable getAuthorizable(SlingHttpServletRequest request) {
		
		ResourceResolver resourceResolver = request.getResourceResolver();
		Session session = resourceResolver.adaptTo(Session.class);
		UserManager userManager = resourceResolver.adaptTo(UserManager.class);
		String userID = session.getUserID();
	    try {
			return userManager.getAuthorizable(userID);
			
		} catch (RepositoryException e) {
			LoggerFactory.getLogger(AlitaliaTradeAuthUtils.class).error("Error getAuthorizable: ", e);
		}
	    return null;
	}
	
	public static String getProperty(SlingHttpServletRequest request, String name) throws ValueFormatException, RepositoryException {
		Authorizable auth = getAuthorizable(request);
		return getProperty(auth, name);
	}
	
	public static AlitaliaTradeUserType getRuolo(SlingHttpServletRequest request) throws ValueFormatException, RepositoryException {
		Authorizable auth = getAuthorizable(request);
		String ruolo = getProperty(auth, "ruolo");
		return ruolo == null ? null : AlitaliaTradeUserType.fromValue(ruolo);
	}
	
	private static String getProperty(Authorizable auth, String name) throws ValueFormatException, RepositoryException {
		if (auth.getProperty(name) == null || auth.getProperty(name).length == 0 || auth.getProperty(name) == null) {
			return null;
		}
		return auth.getProperty(name)[0].getString();
	}
}
