package com.alitalia.aem.business.web.booking.render;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class GenericPriceRender{

	private NumberFormat numberFormat;
	private BigDecimal fare;
	private BigDecimal fareChildren;
	private BigDecimal fareInfant;
	private BigDecimal taxes;
	private BigDecimal extra;
	private BigDecimal extraCharge;
	private BigDecimal totalPrice;
	private String currency;
	private Map<String,String> simboloValuta;

	
	public GenericPriceRender(BigDecimal fare, String currency, Locale locale) {
		this.fare = fare;
		this.currency = currency;
		init(locale);
	}
	
	public GenericPriceRender(BigDecimal fare, BigDecimal taxes, BigDecimal extra, String currency, Locale locale) {
		this.fare = fare;
		this.taxes = taxes;
		this.extra = extra;
		this.currency = currency;
		init(locale);
	}
	
	public GenericPriceRender(BigDecimal fare, BigDecimal taxes, BigDecimal extra, BigDecimal extraCharge, String currency, Locale locale) {
		this.fare = fare;
		this.taxes = taxes;
		this.extra = extra;
		this.extraCharge = extraCharge;
		this.currency = currency;
		init(locale);
	}
	
	public GenericPriceRender(BigDecimal fare, BigDecimal taxes, BigDecimal extra, BigDecimal extraCharge, BigDecimal totalPrice, String currency, Locale locale) {
		this.fare = fare;
		this.taxes = taxes;
		this.extra = extra;
		this.extraCharge = extraCharge;
		this.totalPrice = totalPrice;
		this.currency = currency;
		init(locale);
	}
	
	public GenericPriceRender(BigDecimal fare, BigDecimal fareChildren, BigDecimal taxes, BigDecimal extra, BigDecimal extraCharge, BigDecimal totalPrice, String currency, Locale locale) {
		this.fare = fare;
		this.fareChildren = fareChildren;
		this.taxes = taxes;
		this.extra = extra;
		this.extraCharge = extraCharge;
		this.totalPrice = totalPrice;
		this.currency = currency;
		init(locale);
	}
	
	public GenericPriceRender(BigDecimal fare, BigDecimal fareChildren, BigDecimal fareInfant,  BigDecimal taxes, BigDecimal extra, BigDecimal extraCharge, BigDecimal totalPrice, String currency, Locale locale) {
		this.fare = fare;
		this.fareChildren = fareChildren;
		this.fareInfant = fareInfant;
		this.taxes = taxes;
		this.extra = extra;
		this.extraCharge = extraCharge;
		this.totalPrice = totalPrice;
		this.currency = currency;
		init(locale);
	}

	private void init(Locale locale){
		this.numberFormat = NumberFormat.getInstance(locale);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
		simboloValuta = new HashMap<String,String>();
		simboloValuta.put("EUR","€");
	}
	
	public String getCurrency(){
		return this.currency;
	}

	public String getSymbolCurrency(){
		return simboloValuta.get(this.currency);
	}
	
	public String getFare() {
		return numberFormat.format(this.fare.doubleValue());
	}
	
	public String getFareChildren() {
		return numberFormat.format(this.fareChildren.doubleValue());
	}
	
	public String getFareInfant() {
		return numberFormat.format(this.fareInfant.doubleValue());
	}

	public String getTaxes() {
		return numberFormat.format(this.taxes.doubleValue());
	}

	public String getExtra() {
		return numberFormat.format(this.extra.doubleValue());
	}

	public String getExtraCharge() {
		return numberFormat.format(this.extraCharge.doubleValue());
	}
	
	public String getTotalPrice(){
		return numberFormat.format(this.totalPrice.doubleValue());
	}

}
