package com.alitalia.aem.business.web.booking.render;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.utils.DateUtils;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.utils.FlightDataUtils;
import com.day.cq.i18n.I18n;

public class AvailableFlightsSelectionRender {
	
	private FlightData flightData;
	private SearchElementDetailRender from;
	private SearchElementDetailRender to;
	private String duration;
	private Long nextDays;
	private List<DirectFlightDataRender> stops;
	private int stopsNumber;
	private String carriers;
	private ArrayList<String> classLogoCarrier;
	private List<AvailableFlightBrandSelectionRender> availableFlightBrandSelectionRenderList;
	private String multileg;
	private List<Integer> connectingFlightsWait;

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public AvailableFlightsSelectionRender(FlightData flightData, Locale locale, I18n i18n, boolean refreshed, FlightData selectedFlightData, BrandData selectedBrandData, BookingSessionContext ctx) {
		this.flightData = flightData;
		this.duration = "";
		this.nextDays = null;
		this.stops = new ArrayList<DirectFlightDataRender>();
		this.stopsNumber = 0;
		this.carriers = "";
		this.classLogoCarrier = new ArrayList<String>();
		this.availableFlightBrandSelectionRenderList = new ArrayList<AvailableFlightBrandSelectionRender>();
		this.multileg = "";
		init(locale, i18n, refreshed, selectedFlightData, selectedBrandData, ctx);
	}
	
	private void init(Locale locale, I18n i18n, boolean refreshed, FlightData selectedFlightData, BrandData selectedBrandData, BookingSessionContext ctx) {
		DirectFlightData directFlightData = null;
		stops = new ArrayList<DirectFlightDataRender>();
		connectingFlightsWait = FlightDataUtils.computeConnectingFlightsWait(this.flightData);

		if (this.flightData.getFlightType() == FlightTypeEnum.CONNECTING) {
			int index = 0;
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) this.flightData;
			for (FlightData flightData : connectingFlightData.getFlights()) {
				directFlightData = (DirectFlightData) flightData;
				if ( index == 0 ) {
					this.from = new SearchElementDetailRender(new DateRender(directFlightData.getDepartureDate()),
							directFlightData.getFrom().getCode(), directFlightData.getFrom().getCityCode(),
							i18n.get("airportsData." + directFlightData.getFrom().getCode() + ".city"));
				}
				if ( index == connectingFlightData.getFlights().size() - 1 ) {
					this.to = new SearchElementDetailRender(new DateRender(directFlightData.getArrivalDate()),
							directFlightData.getTo().getCode(), directFlightData.getTo().getCityCode(),
							i18n.get("airportsData." + directFlightData.getTo().getCode() + ".city"));
				}

				Integer waiting = null;
				if (connectingFlightsWait != null) {
					waiting = connectingFlightsWait.get(index++);
				}
				stops.add(new DirectFlightDataRender(directFlightData, waiting, i18n, 0));
			}
		} else {
			directFlightData = (DirectFlightData) this.flightData;
			stops.add(new DirectFlightDataRender(directFlightData, null, i18n, 0));
			this.from = new SearchElementDetailRender(new DateRender(directFlightData.getDepartureDate()),
					directFlightData.getFrom().getCode(), directFlightData.getFrom().getCityCode(),
					i18n.get("airportsData." + directFlightData.getFrom().getCode() + ".city"));
			this.to = new SearchElementDetailRender(new DateRender(directFlightData.getArrivalDate()),
					directFlightData.getTo().getCode(), directFlightData.getTo().getCityCode(),
					i18n.get("airportsData." + directFlightData.getTo().getCode() + ".city"));
		}
		
		duration = computeDurationString();
		stopsNumber = stops.size()-1;
		carriers = composeCarrierString(stops);
		classLogoCarrier = computeLogoCarrier(this.flightData);
		nextDays = computeNextDays(this.flightData);
		availableFlightBrandSelectionRenderList = addAvailableBrandSelection(locale,selectedFlightData,selectedBrandData,ctx);
		setAvailabilityBrands(refreshed);
		multileg = obtainCssClassForMultileg();
	}
	
	public FlightData getFlightData() {
		return flightData;
	}

	public SearchElementDetailRender getFrom() {
		return from;
	}

	public SearchElementDetailRender getTo() {
		return to;
	}
	
	public String getDuration() {
		return duration;
	}

	public long getNextDays() {
		if(nextDays == null){
			return 0;
		}
		return nextDays.longValue();
	}

	public List<DirectFlightDataRender> getStops() {
		return stops;
	}
	
	public int getStopsNumber() {
		return stopsNumber;
	}

	public String getCarriers() {
		return carriers;
	}
	
	public ArrayList<String> getClassLogoCarrier() {
		return classLogoCarrier;
	}

	public List<AvailableFlightBrandSelectionRender> getAvailableFlightBrandSelectionRenderList() {
		return availableFlightBrandSelectionRenderList;
	}

	public String getMultileg() {
		return multileg;
	}

	
	/* private methods */
	
	private ArrayList<String> computeLogoCarrier(FlightData flight) {
		ArrayList<String> carrierLogoList = new ArrayList<String>();
		if (flight instanceof ConnectingFlightData) {
			for (FlightData flightData : ((ConnectingFlightData) flight).getFlights()) {
				DirectFlightData directFlightData = (DirectFlightData) flightData;
				carrierLogoList.add(directFlightData.getCarrier().toLowerCase() + "_small_logo");
			}
		} else {
			DirectFlightData directFlightData = (DirectFlightData) flight;
			carrierLogoList.add(directFlightData.getCarrier().toLowerCase() + "_small_logo");
		}
		return carrierLogoList;
	}

	private void setAvailabilityBrands(boolean refreshed) {
		if (refreshed) {
			for ( AvailableFlightBrandSelectionRender brandSelectionRender : this.availableFlightBrandSelectionRenderList ) {
				if ( brandSelectionRender.getBrandData().getRefreshSolutionId() != null ) {
					 if(brandSelectionRender.getBrandData().getRefreshSolutionId().equals("")) {
						 brandSelectionRender.setEnabled(false);
					 }
				}
			}
		}
	}
	
	private String computeDurationString() {
		String hours = this.flightData.getDurationHour().toString(); 
		String minutes = this.flightData.getDurationMinutes().toString();
		
		if(flightData.getDurationHour().intValue()<10){
			hours = "0" + hours;
		}
		if(flightData.getDurationMinutes().intValue()<10){
			minutes = "0" + minutes;
		}
		
		return hours + "H:" + minutes;
	}
	
	private String composeCarrierString(List<DirectFlightDataRender> stops) {
		String carriers = "";
		if (!stops.isEmpty()) {
			Iterator<DirectFlightDataRender> it = stops.iterator();
			do{
				DirectFlightDataRender stop = it.next();
				if (!carriers.contains(stop.getCarrier())) {
					if(!stop.getCarrier().equals("")){
						if (carriers.equals("")) {
							carriers = stop.getCarrier();
						} else {
							carriers = carriers + " | " + stop.getCarrier();
						}
					}
				}
			}while(it.hasNext());
		}
		return carriers;
	}
	
	private Long computeNextDays(FlightData flightData) {
		Calendar departureDate = null;
		Calendar arrivalDate = null;
		if (flightData instanceof DirectFlightData) {
			DirectFlightData directFlight = (DirectFlightData) flightData;
			departureDate = directFlight.getDepartureDate();
			arrivalDate = directFlight.getArrivalDate();
		} else {
			ConnectingFlightData connectingFlight = (ConnectingFlightData) flightData;
			ArrayList<FlightData> flights = connectingFlight.getFlights();
			departureDate = ((DirectFlightData)flights.get(0)).getDepartureDate();
			arrivalDate = ((DirectFlightData)flights.get(flights.size()-1)).getArrivalDate();
		}
		try {
			long daysBetween = DateUtils.daysBetween(departureDate, arrivalDate);
			return (daysBetween != 0 ? daysBetween : null);
		} catch (Exception e) {
			logger.error("Unexpected error in computeNextDays: ", e);
			throw e;
		}
	}
	
	private List<AvailableFlightBrandSelectionRender> addAvailableBrandSelection(Locale locale, FlightData selectedFilghtData, BrandData selectedBrandData, BookingSessionContext ctx) {
		
		String selectedFlightNumber = "";
		String currentFlightNumber = "";
		if (selectedFilghtData!=null) {
			if (selectedFilghtData.getFlightType() == FlightTypeEnum.CONNECTING) {
				selectedFlightNumber = ((DirectFlightData) ((ConnectingFlightData)selectedFilghtData).getFlights().get(0)).getFlightNumber();
			} else {
				selectedFlightNumber = ((DirectFlightData)selectedFilghtData).getFlightNumber();
			}
			
			if (this.flightData.getFlightType() == FlightTypeEnum.CONNECTING) {
				currentFlightNumber = ((DirectFlightData) ((ConnectingFlightData)this.flightData).getFlights().get(0)).getFlightNumber();
			} else {
				currentFlightNumber = ((DirectFlightData)this.flightData).getFlightNumber();
			}
		}
		
		for(BrandData brandData : this.flightData.getBrands()){
			AvailableFlightBrandSelectionRender availableBrandSelectionRender = null;
			if ( selectedBrandData!=null && currentFlightNumber.equals(selectedFlightNumber) ) {
				
				if (brandData.getCode().equals(selectedBrandData.getCode())) {
					availableBrandSelectionRender = new AvailableFlightBrandSelectionRender(brandData,locale,true,ctx);
				} else {
					availableBrandSelectionRender = new AvailableFlightBrandSelectionRender(brandData,locale,false,ctx);
				}
				
			} else {
				availableBrandSelectionRender = new AvailableFlightBrandSelectionRender(brandData,locale,false,ctx);
			}
			availableFlightBrandSelectionRenderList.add(availableBrandSelectionRender);
			
		}
		return availableFlightBrandSelectionRenderList;
	}
	
	private String obtainCssClassForMultileg() {
		if(this.flightData.getFlightType() == FlightTypeEnum.CONNECTING){
			return "extra-tratte";
		}
		return "";
	}

}

