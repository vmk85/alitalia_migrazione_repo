package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.BookingSession;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.utils.Constants;
import com.day.cq.commons.TidyJSONWriter;

@Component(immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "isReadyToPassengersDataPhase" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "error.page", description = "Users will be redirected to the specified page after a failed outcome."),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class BookingIsReadyToPassengersDataPhaseServlet extends SlingSafeMethodsServlet {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private ComponentContext componentContext;

	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Reference
	private BookingSession bookingSession;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		String genericErrorPagePlain = "#";
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		
		try {
			json.object();
			genericErrorPagePlain = (String) 
					componentContext.getProperties().get("error.page");
						
			BookingSessionContext ctx = (BookingSessionContext) 
					request.getSession(true).getAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE);
			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				if (!response.isCommitted()) {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Booking session context not found.");
				}
				return;
			}
			
			response.setContentType("application/json");
			json.key("ready").value(ctx.readyToPassengersDataPhase);
			if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
				json.key("func").value(true);
			} else {
				json.key("func").value(false);
			}
			json.endObject();

		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			if (!response.isCommitted()) {
				response.setContentType("application/json");
				try {
					json.key("redirect").value(genericErrorPagePlain);
					json.endObject();
				} catch (JSONException e1) {
					logger.error("Error building JSON");
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
				}
			}
		}
		
	}

}

