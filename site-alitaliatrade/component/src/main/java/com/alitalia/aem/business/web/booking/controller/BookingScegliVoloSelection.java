package com.alitalia.aem.business.web.booking.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.BookingSession;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.booking.model.FlightSelection;
import com.alitalia.aem.business.web.booking.model.SearchElement;
import com.alitalia.aem.business.web.booking.render.AvailableFlightBrandSelectionRender;
import com.alitalia.aem.business.web.booking.render.AvailableFlightsSelectionRender;
import com.alitalia.aem.business.web.booking.render.BrandRender;
import com.alitalia.aem.business.web.booking.render.GenericPriceRender;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.day.cq.i18n.I18n;

public class BookingScegliVoloSelection extends BookingSessionGenericController {

	private SearchElement searchElement;
	private List<AvailableFlightsSelectionRender> availableFlightSelections;
	private List<BrandRender> brandHeaderList;
	private int routeIndex;
	private boolean solutionForSelectedData;
	private boolean showMatrix;
	private boolean showMoreFlights;
	private boolean isMultileg;
	private BookingSession bookingSession;
	
	private String analytics_network = "";
	private String analytics_sabreSessionId = "";
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Override
	public void activate() throws Exception {
		super.activate();
		
		try{
			bookingSession = getSlingScriptHelper().getService(BookingSession.class);
			
			if (isIllegalCTXState(getResponse())){
				return;
			}
			
			SlingHttpServletRequest request = getRequest();
			ResourceBundle resourceBundle = request.getResourceBundle(
					LocalizationUtils.getLocaleByResource(request.getResource(), LocalizationUtils.getLanguage(ctx.locale.toLanguageTag())));
			final I18n i18n = new I18n(resourceBundle);
			
			if( ( getRequest().getParameter("direction")==null || 
					("").equals(getRequest().getParameter("direction")) ) || 
					( !("1").equals(getRequest().getParameter("direction")) && 
							!("0").equals(getRequest().getParameter("direction")) ) ){
				logger.error("direction of the route not found");
				throw new RuntimeException("direction of the route not found");
			}
			routeIndex = Integer.parseInt(getRequest().getParameter("direction"));
			
			boolean moreFlights = false;
			String more = getRequest().getParameter("more");
			if (more!=null) {
				if (!more.equals("1")) {
					logger.error("more flights parameter is wrong");
					throw new RuntimeException("more flights parameter is wrong");
				} else {
					moreFlights = true;
				}
			}
			
			//retrieve the search element by the route index
			searchElement = ctx.searchElements.get(routeIndex);
			
			//checking for returned solution
			showMatrix = false;
			solutionForSelectedData = false;
			if(ctx.searchKind == BookingSearchKindEnum.MULTILEG) { 
				solutionForSelectedData = true;
				showMatrix = true;
				isMultileg = true;
			} else {
				isMultileg = false;
				if (ctx.solutionForSelectedDate[routeIndex]) {
					solutionForSelectedData = true;
					showMatrix = true;
				}
			}
			if (ctx.availableFlights == null) {
				showMatrix = false;
			} else {
				if (ctx.availableFlights.getRoutes() == null) {
					showMatrix = false;
				} else if (ctx.availableFlights.getRoutes().isEmpty()){
					showMatrix = false;
				}
			}
			
			//populate the list rows with each row of the choosing selection flight matrix
			if(showMatrix){
				availableFlightSelections = prepareAvailableFlightSelections(ctx, i18n, moreFlights);
				if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
					addExtraChargeToFare(ctx.totalExtraCharges, ctx);
				} else {
					if (routeIndex == ctx.totalSlices-1) {
						addExtraChargeToFare(ctx.totalExtraCharges, ctx);
					}
				}
				brandHeaderList = setBrandHeader(ctx,routeIndex);
			}
			
			analytics_network = computeNetwork();
			analytics_sabreSessionId = "";
			if(ctx.cookie != null){
				analytics_sabreSessionId = computeSabreSessionID(ctx.cookie);
			}
			
		} catch(Exception e) {
			logger.error("Unexpected error", e);
			throw e;
		}
	}
	
	/**
	 * It sum the extra charge price to the flight fare
	 * @param totalExtraCharges
	 * @param ctx
	 */
	private void addExtraChargeToFare(BigDecimal totalExtraCharges, BookingSessionContext ctx) {
		for (AvailableFlightsSelectionRender availableFlightRender : this.availableFlightSelections) {
			for (AvailableFlightBrandSelectionRender availableFlightBrandRender 
					: availableFlightRender.getAvailableFlightBrandSelectionRenderList()) {
				
				GenericPriceRender currentPriceRender = availableFlightBrandRender.getFlightFare();
				BigDecimal newPrice = new BigDecimal(0);
				newPrice = newPrice.add(availableFlightBrandRender.getBrandData().getGrossFare());
				newPrice = newPrice.add(totalExtraCharges);
				GenericPriceRender newPriceRender = new GenericPriceRender(
						newPrice, currentPriceRender.getCurrency(), ctx.locale);
				availableFlightBrandRender.setFlightFare(newPriceRender);
			}
		}
		
	}
	
	private List<AvailableFlightsSelectionRender> prepareAvailableFlightSelections(BookingSessionContext ctx, I18n i18n, boolean moreFlights) {
		
		List<AvailableFlightsSelectionRender> availableFlightSelections = new ArrayList<AvailableFlightsSelectionRender>(); 
		
		AvailableFlightsData availableFlights = ctx.availableFlights;
		if(availableFlights==null){
			logger.error("Unexpected error - availableFligths not in session");
			throw new RuntimeException("Unexpected error - availableFligths not in session");
		}
		if(availableFlights.getRoutes()==null){
			logger.error("Unexpected error - Routes not available");
			throw new RuntimeException("Unexpected error - Routes not available");
		}
		
		
		RouteData route =  availableFlights.getRoutes().get(routeIndex);
		if(route.getFlights()==null){
			logger.error("Unexpected error - Flights not available");
			throw new RuntimeException("Unexpected error - Flights not available");
		}
		if (ctx.searchKind == BookingSearchKindEnum.MULTILEG){
			route = availableFlights.getRoutes().get(0);
		} else {
			if(routeIndex==0 && route.getType() == RouteTypeEnum.RETURN){
				route = availableFlights.getRoutes().get(1);
			}
			if(routeIndex==1 && route.getType() == RouteTypeEnum.OUTBOUND){
				route = availableFlights.getRoutes().get(0);
			}
		}
		
		BrandData selectedBrandData = null;
		FlightData selectedFlightData = null;
		if(ctx.flightSelections[routeIndex] != null){
			FlightSelection selectedFlight = ctx.flightSelections[routeIndex];
			selectedFlightData = selectedFlight.getFlightData();
			for(BrandData brandData : selectedFlight.getFlightData().getBrands() ) {
				if ( brandData.getCode().equalsIgnoreCase(
						selectedFlight.getSelectedBrandCode()) ) {
					selectedBrandData = brandData;
					break;
				}
			}
		}
		
		List<FlightData> currentFlightToShow = bookingSession.obtainFlightsToShown(ctx, routeIndex, route, moreFlights);
		showMoreFlights = (route.getFlights().size() != currentFlightToShow.size());
		
		Iterator<FlightData> itFlight = currentFlightToShow.iterator();
		while(itFlight.hasNext()){
			FlightData flightData = itFlight.next();
			boolean isRefreshedSlice = false;
			if (ctx.isRefreshed) {
				if (ctx.firstSelectionIndex != routeIndex) {
					isRefreshedSlice = true;
				}
			}
            Boolean isSoggettoAppGov=computeIsSoggettoAppGov(obtainFlightNumbers(flightData));
            flightData.setIsSoggettoApprovazioneGovernativa(isSoggettoAppGov);

			availableFlightSelections.add(new AvailableFlightsSelectionRender(flightData, 
					ctx.locale, i18n, isRefreshedSlice, selectedFlightData, selectedBrandData, ctx));
		}
		 
		return availableFlightSelections;
	}

    private Boolean computeIsSoggettoAppGov(String numeroVolo) {
        String[] arrayVolo =configuration.getListaVoliSoggettiApprovazioneGovernativa().split(";");
        for (int i=0; i<arrayVolo.length; i++){
            if (arrayVolo[i].equals(numeroVolo)) {return true;}
        }
        return false;
    }

    private String obtainFlightNumbers(FlightData flightData) {
        String result = "";
        //FlightData flightData = flightSelection.getFlightData();
        if (flightData instanceof DirectFlightData) {
            DirectFlightData directFlightData = (DirectFlightData) flightData;
            return directFlightData.getCarrier() + directFlightData.getFlightNumber();
        } else {
            ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
            for (FlightData flight : connectingFlightData.getFlights()) {
                DirectFlightData directFlightData = (DirectFlightData) flight;
                String number = directFlightData.getCarrier() + directFlightData.getFlightNumber();
                if (!result.equals("")) {
                    result += ", " + number;
                } else {
                    result = number;
                }
            }
        }
        return result;
    }

	/**
	 * 
	 * @param availableFlightsSelectionRender is the first available flight selection and it uses to
	 * build the header list of brands
	 * @return the brands header
	 */
	private List<BrandRender> setBrandHeader(BookingSessionContext ctx, int routeIndex){
		
		List<BrandRender> brandHeaderList = new ArrayList<BrandRender>();
		
		RouteData selectedRoute = null;
		if(BookingSearchKindEnum.MULTILEG.equals(ctx.searchKind )){
			selectedRoute = ctx.availableFlights.getRoutes().get(0);
		}
		else{
			for (RouteData routeData : ctx.availableFlights.getRoutes()) {
				if(routeIndex==0 && routeData.getType() == RouteTypeEnum.OUTBOUND) {
					selectedRoute = routeData;
					break;
				}
				if(routeIndex==1 && routeData.getType() == RouteTypeEnum.RETURN) {
					selectedRoute = routeData;
					break;
				}
			}
		}
		for (BrandData brandData : selectedRoute.getFlights().get(0).getBrands()) {
			brandHeaderList.add(new BrandRender(brandData, ctx));
			}
		return brandHeaderList;
	}
	

	/**
	 * 
	 * @return "NAZ" se volo domestico, "INZ" se internazionale, "INC" se intercontinentale,
	 * 		"" (blank) in caso di errore
	 */
	private String computeNetwork() {
		String netw="";
		if (ctx.availableFlights != null && ctx.availableFlights.getRoutes() != null 
				&& !ctx.availableFlights.getRoutes().isEmpty()) {
			FlightData flightData = ctx.availableFlights.getRoutes().get(0).getFlights().get(0);
			AreaValueEnum areaFrom = null;
			AreaValueEnum areaTo = null;
			if (flightData instanceof DirectFlightData) {
				DirectFlightData directFlightData = (DirectFlightData) flightData;
				areaFrom = directFlightData.getFrom().getArea();
				areaTo = directFlightData.getTo().getArea();
			} else {
				ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
				DirectFlightData first = (DirectFlightData) connectingFlightData.getFlights().get(0);
				DirectFlightData last = (DirectFlightData) connectingFlightData.getFlights().get(connectingFlightData.getFlights().size() - 1);
				areaFrom = first.getFrom().getArea();
				areaTo = last.getTo().getArea();
			}
			AreaValueEnum flightType = areaFrom == AreaValueEnum.DOM ? areaTo : areaFrom;
			switch(flightType){
			case DOM:
				netw = "NAZ";
				break;
			case INTZ:
				netw = "INZ";
				break;
			case INTC:
				netw = "INC";
				break;
			}
		}
		return netw;
	}
	
	private String computeSabreSessionID(String cookie) {
		String jsession = "";
		int index = cookie.indexOf("JSESSIONID=");
		if(index >= 0 && cookie.length() >= index + 11 + 32){
			jsession = cookie.substring(index + 11, index + 11 + 32);
			/*JSESSIONID vuoto o non valido*/
			if(jsession.matches(".*[\",.;=].*")){
				jsession = "";
			}
		}
		return jsession;
	}

	public List<AvailableFlightsSelectionRender> getAvailableFlightSelections() {
		return availableFlightSelections;
	}

	public SearchElement getSearchElement() {
		return searchElement;
	}

	public List<BrandRender> getBrandHeaderList() {
		return brandHeaderList;
	}

	public int getRouteIndex() {
		return routeIndex;
	}

	public boolean getSolutionForSelectedData() {
		return solutionForSelectedData;
	}
	
	public boolean getShowMatrix() {
		return showMatrix;
	}
	
	public boolean getShowMoreFlights() {
		return showMoreFlights;
	}

	public String getAnalytics_network() {
		return analytics_network;
	}

	public boolean isMultileg() {
		return isMultileg;
	}

	public String getAnalytics_sabreSessionId() {
		return analytics_sabreSessionId;
	}


	
	
	
	
	
	
	

	
	

	
}
