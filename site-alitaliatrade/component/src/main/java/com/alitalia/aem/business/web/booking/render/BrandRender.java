package com.alitalia.aem.business.web.booking.render;

import com.alitalia.aem.business.web.booking.BookingSearchCUGEnum;
import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.common.data.home.BrandData;
import java.util.HashSet;
import java.util.Set;

public class BrandRender{
	
	private BrandData brandData;
	private String brandName;
	private boolean isTariffaLight;
	private boolean isOnlyHandBaggage;
	private String classCssName;
	private boolean isMultileg;

	private boolean airportsBelongToSets(String from, String to, Set<String> airportCodesA, Set<String> airportCodesB) {
		if ( airportCodesA.contains(from) ) {
			if ( airportCodesB.contains(to) ) {
				return true;
			}
		} else if ( airportCodesB.contains(from) ) {
			if ( airportCodesA.contains(to) ) {
				return true;
			}
		}
		return false;
	}
	
	public BrandRender(BrandData brandData, BookingSessionContext ctx){
		this.brandData = brandData;
		this.brandName = obtainBrandNameByCode(ctx.cug);
		this.isTariffaLight = computeIsTariffaLight();
		this.isOnlyHandBaggage = computeIsOnlyHandBaggage(ctx.onlyHandBaggage);
		this.classCssName = computeClassCssName(ctx.cug, ctx);
		this.isMultileg = ctx.searchKind == BookingSearchKindEnum.MULTILEG;
	}
	

	public BrandData getBrandData() {
		return brandData;
	}

	public String getBrandName() {
		return brandName;
	}

	public boolean isTariffaLight() {
		return isTariffaLight;
	}

	public boolean isOnlyHandBaggage() {
		return isOnlyHandBaggage;
	}

	public String getClassCssName() {
		return classCssName;
	}
	
	public boolean isMultileg() {
		return isMultileg;
	}
	
	
	
	
	/* private methods */
	
	private String obtainBrandNameByCode(BookingSearchCUGEnum cug) {
		String brandName ="";
		if ( brandData.getCode().toLowerCase().equals("yc") ) {
			if (cug == BookingSearchCUGEnum.MILITARY) {
				return "MILITARI POLIZIA";
			} else { //caso ADT
				return "ECONOMY CLASSIC";
			}
			
		}
		if ( brandData.getCode().toLowerCase().equals("fa") ) {
			if (cug == BookingSearchCUGEnum.FAMILY) {
				return "FAMILY";
			} else { //caso ADT
				return "ECONOMY BASIC";
			}
			
		}
		switch(brandData.getCode().toLowerCase()){
			case "economybasic":{
				brandName="ECONOMY BASIC";
			}break;
			case "yl":{
				brandName="ECONOMY LIGHT";
			}break;
			case "yc":{
				brandName="ECONOMY CLASSIC";
			}break;
			case "yf":{
				brandName="ECONOMY FLEX";
			}break;
			case "co":{
				brandName="COMFORT FLEX";
			}break; 
			case "yp":{
				brandName="PREMIUM ECONOMY";
			}break;
			case "yd":{
				brandName="ECONOMY PROMO";
			}break;
			case "ys":{
				brandName="ECONOMY SAVER";
			}break;
			case "jc":{
				brandName="BUSINESS CLASSIC";
			}break;
			case "jf":{
				brandName="BUSINESS FLEX";
			}break;
			case "zz":{
				brandName="GIOVANI";
			}break;
			case "tc":{
				brandName="CONTINUITà TERRITORIALE";
			}break;
			case "yx":{
				brandName="ECONOMY CLASSIC PLUS";
			}break;
		}
		return brandName;
	}

	private boolean computeIsTariffaLight() {
		return brandData.getCode().equalsIgnoreCase("YL"); 
	}
	
	private boolean computeIsOnlyHandBaggage(boolean onlyHandBaggage) {
		if (this.isTariffaLight && onlyHandBaggage) {
			return true;
		} else {
			return false;
		}
	}
	
	private String computeClassCssName(BookingSearchCUGEnum cug, BookingSessionContext ctx){
		Set<String> airportCodesA = new HashSet<String>();
		airportCodesA.add("CAG");
		airportCodesA.add("AHO");
		airportCodesA.add("OLB");
		Set<String> airportCodesB = new HashSet<String>();
		airportCodesB.add("LIN");
		airportCodesB.add("MIL");
		airportCodesB.add("ROM");
		airportCodesB.add("FCO");
		String from = ctx.searchElements.get(0).getFrom().getAirportCode();
		String to = ctx.searchElements.get(0).getTo().getAirportCode();
		if (airportsBelongToSets(from, to, airportCodesA, airportCodesB)) {
			return "continuity";
		}
		if ( brandData.getCode().equalsIgnoreCase("ZZ") ) {
			return "youth";
		}
		if ( brandData.getCode().equalsIgnoreCase("YC") ) {
			if (cug == BookingSearchCUGEnum.MILITARY) {
				return "army";
			}
		}
		if ( brandData.getCode().equalsIgnoreCase("FA") ) {
			if (cug == BookingSearchCUGEnum.FAMILY) {
				return "family";
			}
		}
		return brandName.toLowerCase().replace(" ", "-");
	}


	
	
	
}