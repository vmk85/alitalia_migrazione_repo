package com.alitalia.aem.business.web.controllers;

import java.util.ArrayList;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUse;

public class InfoDestinazione extends WCMUse {

	private String titolo;
	private String urlimg;
	
	@Override
    public void activate() throws Exception {
		
		String destinazione = getRequest().getParameter("to");
		
		// TODO recuperare da servizio
		ArrayList<String> airportList = new ArrayList<String>();
		airportList.add("ROM");
		airportList.add("MIL");
		
		if (airportList.contains(destinazione)) {
			titolo = "airport.city." + destinazione;
		}
		
		Resource resource = getResource().getChild("image");
		
		if (resource != null) {
			ValueMap imageProperties = resource.getValueMap();
			urlimg = imageProperties.get("fileReference", String.class);
		}
	}
	
	public String getTitolo() {
		return titolo;
	}

	public String getUrlimg() {
		return urlimg;
	}
}