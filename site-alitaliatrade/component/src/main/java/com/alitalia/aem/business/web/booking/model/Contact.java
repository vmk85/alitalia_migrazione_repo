package com.alitalia.aem.business.web.booking.model;


public class Contact {
	
	private ContactType contactType;
	private String internationalPrefix;
	private String phoneNumber;
	
	public Contact() {
		
	}
	
	public Contact(ContactType contactType, String internationalPrefix, String phoneNumber) {
		this.contactType = contactType;
		this.internationalPrefix = internationalPrefix;
		this.phoneNumber = phoneNumber;
	}

	public ContactType getContactType() {
		return contactType;
	}

	public void setContactType(ContactType contactType) {
		this.contactType = contactType;
	}

	public String getInternationalPrefix() {
		return internationalPrefix;
	}

	public void setInternationalPrefix(String internationalPrefix) {
		this.internationalPrefix = internationalPrefix;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
