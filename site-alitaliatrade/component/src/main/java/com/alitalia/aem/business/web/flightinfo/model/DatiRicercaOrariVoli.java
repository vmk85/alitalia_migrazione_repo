package com.alitalia.aem.business.web.flightinfo.model;


public class DatiRicercaOrariVoli {

	private String codiceAereoportoPartenza;
	private String cittaPartenza;
	private String statoPartenza;
	private String codiceAereoportoArrivo;
	private String cittaArrivo;
	private String statoArrivo;
	private String dataPartenza;
	private String fasciaOraria;
	private String giornoPartenza;
	private String mesePartenza;
	private String annoPartenza;
	
	public DatiRicercaOrariVoli(String codiceAereoportoPartenza, String codiceAereoportoArrivo, String dataPartenza, String fasciaOraria, String codiceStatoPartenza, String codiceStatoArrivo) {
		this.codiceAereoportoPartenza = codiceAereoportoPartenza;
		this.codiceAereoportoArrivo = codiceAereoportoArrivo;
		this.dataPartenza = dataPartenza;
		this.fasciaOraria = fasciaOraria;
		this.cittaPartenza = "airportsData." + codiceAereoportoPartenza + ".city";
		this.cittaArrivo = "airportsData." + codiceAereoportoArrivo + ".city";
		this.statoPartenza = "airportsData." + codiceStatoPartenza + ".country";
		this.statoArrivo = "airportsData." + codiceStatoArrivo + ".country";
		this.giornoPartenza = dataPartenza.substring(0,2);
		this.mesePartenza = dataPartenza.substring(3,5);
		this.annoPartenza = dataPartenza.substring(6,8);
	}
	
	public String getCittaPartenza() {
		return cittaPartenza;
	}

	public void setCittaPartenza(String cittaPartenza) {
		this.cittaPartenza = cittaPartenza;
	}

	public String getCittaArrivo() {
		return cittaArrivo;
	}

	public void setCittaArrivo(String cittaArrivo) {
		this.cittaArrivo = cittaArrivo;
	}

	public String getDataPartenza() {
		return dataPartenza;
	}

	public void setDataPartenza(String dataPartenza) {
		this.dataPartenza = dataPartenza;
	}

	public String getFasciaOraria() {
		return fasciaOraria;
	}

	public void setFasciaOraria(String fasciaOraria) {
		this.fasciaOraria = fasciaOraria;
	}
	
	public String getCodiceAereoportoPartenza() {
		return codiceAereoportoPartenza;
	}

	public void setCodiceAereoportoPartenza(String codiceAereoportoPartenza) {
		this.codiceAereoportoPartenza = codiceAereoportoPartenza;
	}

	public String getCodiceAereoportoArrivo() {
		return codiceAereoportoArrivo;
	}

	public void setCodiceAereoportoArrivo(String codiceAereoportoArrivo) {
		this.codiceAereoportoArrivo = codiceAereoportoArrivo;
	}

	public String getGiornoPartenza() {
		return giornoPartenza;
	}

	public void setGiornoPartenza(String giornoPartenza) {
		this.giornoPartenza = giornoPartenza;
	}

	public String getMesePartenza() {
		return mesePartenza;
	}

	public void setMesePartenza(String mesePartenza) {
		this.mesePartenza = mesePartenza;
	}

	public String getAnnoPartenza() {
		return annoPartenza;
	}

	public void setAnnoPartenza(String annoPartenza) {
		this.annoPartenza = annoPartenza;
	}

	public String getStatoPartenza() {
		return statoPartenza;
	}

	public void setStatoPartenza(String statoPartenza) {
		this.statoPartenza = statoPartenza;
	}

	public String getStatoArrivo() {
		return statoArrivo;
	}

	public void setStatoArrivo(String statoArrivo) {
		this.statoArrivo = statoArrivo;
	}
	
}
