package com.alitalia.aem.business.web.logout.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.statistics.utils.TradeStatisticUtils;
import com.alitalia.aem.common.data.home.enumerations.TradeStatisticDataTypeEnum;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.common.messages.home.TradeRegisterStatisticRequest;
import com.alitalia.aem.web.component.tradestatistics.delegate.RegisterTradeStatisticsDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "agencylogout" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.")
})
@SuppressWarnings("serial")
public class AgencyLogoutServlet extends SlingSafeMethodsServlet {
	
	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile RegisterTradeStatisticsDelegate registerStatisticsDelegate;
	
	@Reference
	private AlitaliaTradeConfigurationHolder configuration;
	
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private ComponentContext componentContext;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException { 
		logger.debug("Logout");
		
		String successPage = null;
		String failurePage = null;
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		
		try{
			successPage = (String) componentContext.getProperties().get("success.page");
			failurePage = (String) componentContext.getProperties().get("failure.page");
			json.object();
			
			Cookie loginCookie = request.getCookie("login-token");
			loginCookie.setValue(null);
			loginCookie.setPath("/");
			loginCookie.setMaxAge(0);
			response.addCookie(loginCookie);
			TradeStatisticDataTypeEnum statisticDataType = TradeStatisticDataTypeEnum.LOGOUT;
			TradeRegisterStatisticRequest statisticRequest = 
					TradeStatisticUtils.createStatisticRequet(statisticDataType, 
					request, true, null, null, configuration.getClientIpHeaders());
			
			try {
				RegisterStatisticsResponse statisticResponse = registerStatisticsDelegate.registerStatistics(statisticRequest);
				if (statisticResponse == null) {
					logger.error("Statistic Service Failed");
				}
			} catch(Exception e) {
				logger.error("Statistic Service Failed");
			}
			
			json.key("redirect").value(successPage);
			json.endObject();
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			if (!response.isCommitted()) {
				response.setContentType("application/json");
				try {
					json.key("redirect").value(failurePage);
					json.endObject();
				} catch (JSONException e1) {
					logger.error("Error building JSON");
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
				}
			}
		}
	}
	
}

