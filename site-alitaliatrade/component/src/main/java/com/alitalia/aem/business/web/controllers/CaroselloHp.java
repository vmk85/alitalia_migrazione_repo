package com.alitalia.aem.business.web.controllers;

import java.util.Map;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.MultifieldUtils;

public class CaroselloHp extends WCMUse {

	@SuppressWarnings("rawtypes")
	private Map fieldList;
	
	@Override
    public void activate() throws Exception {
		fieldList = MultifieldUtils.fromJcrMultifieldToMapArrayObject(getResource(), "items");
	}
	
	@SuppressWarnings("rawtypes")
	public Map getFieldList() {
        return fieldList;
    }
	
}
