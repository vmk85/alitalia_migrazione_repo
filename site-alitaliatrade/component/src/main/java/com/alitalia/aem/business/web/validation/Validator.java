package com.alitalia.aem.business.web.validation;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("unused")
public class Validator {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public static final String MESSAGE_GENERIC_ERROR = "Errore esecuzione metodo";

	private Map<String, List<Element>> directParameters = new HashMap<String, List<Element>>();
	private Map<String, List<Element>> crossParameters = new HashMap<String, List<Element>>();

	/**
	 * Add a new direct element to validate
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param message
	 *            error message
	 * @param methods
	 *            element methods (to execute)
	 */
	public void addDirectParameter(String key, String value, String message, String... methods) {
		addParameter(key, value, null, message, null, null, methods);
	}

	/**
	 * Add a new direct element to validate with message pattern
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param messagePattern
	 *            error message with a pattern
	 * @param messageArgument
	 *            argument for message
	 * @param methods
	 *            element methods (to execute)
	 */
	public void addDirectParameterMessagePattern(String key, String value, String messagePattern, String messageArgument, String... methods) {
		addParameter(key, value, null, messagePattern, messageArgument, null, methods);
	}

	/**
	 * Add a new cross element to validate
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param comparingValue
	 *            element to compare to
	 * @param message
	 *            error message
	 * @param methods
	 *            element methods (to execute)
	 */
	public void addCrossParameter(String key, String value, String comparingValue, String message, String... methods) {
		addParameter(key, value, comparingValue, message, null, null, methods);
	}

	/**
	 * Add a new cross element to validate with message pattern
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param comparingValue
	 *            element to compare to
	 * @param messagePattern
	 *            error message with a pattern
	 * @param messageArgument
	 *            argument for message
	 * @param methods
	 *            element methods (to execute)
	 */
	public void addCrossParameterMessagePattern(String key, String value, String comparingValue, String messagePattern, String messageArgument, String... methods) {
		addParameter(key, value, comparingValue, messagePattern, messageArgument, null, methods);
	}

	/**
	 * Add a new element to validate against a set of allowed values
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param allowedValues
	 *            allowed values
	 * @param message
	 *            error message
	 */
	public void setAllowedValues(String key, String value, Object[] allowedValues, String message) {
		addParameter(key, value, null, message, null, allowedValues, new String[] {});
	}

	/**
	 * Add a new element to validate against a set of allowed values with message pattern
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param allowedValues
	 *            allowed values
	 * @param messagePattern
	 *            error message with a pattern
	 * @param messageArgument
	 *            argument for message
	 */
	public void setAllowedValuesMessagePattern(String key, String value, Object[] allowedValues, String messagePattern, String messageArgument) {
		addParameter(key, value, null, messagePattern, messageArgument, allowedValues, new String[] {});
	}

	/**
	 * Add a new element to validate with cross validation
	 * 
	 * @param key
	 *            element key
	 * @param value
	 *            element value
	 * @param comparingValue
	 *            element comparing value
	 * @param methods
	 *            element methods (to execute)
	 */
	private void addParameter(String key, String value, String comparingValue, String messagePattern, String messageArgument, Object[] allowedValues, String... methods) {

		// create parameter element
		Element e = new Element();

		// set value
		e.setValue(value);

		// set allowedValues OR methods
		if (allowedValues != null) {
			e.setAllowedValues(allowedValues);
		} else {
			ArrayList<String> methodsList = new ArrayList<String>();
			for (String method : methods) {
				methodsList.add(method);
			}
			e.setMethods(methodsList);
		}

		// set message
		if (messageArgument != null) {
			messagePattern = MessageFormat.format(messagePattern, messageArgument);
		}
		e.setMessage(messagePattern);

		Map<String, List<Element>> parameters = directParameters;

		// set comparing value
		if (comparingValue != null && allowedValues == null) {
			e.setComparingValue(comparingValue);
			parameters = crossParameters;
		}

		// set key
		if (parameters.containsKey(key)) {
			// use existing element
			List<Element> elementList = parameters.get(key);
			elementList.add(e);
			parameters.put(key, elementList);

		} else {
			// put new element
			ArrayList<Element> elementList = new ArrayList<Element>();
			elementList.add(e);
			parameters.put(key, elementList);
		}
	}

	/**
	 * Validate parameters
	 * 
	 * @return result of validation
	 */
	public ResultValidation validate() {

		// result of validation
		ResultValidation r = new ResultValidation();
		boolean valid = true;

		// cycle on all available keys
		Set<String> keySet = new HashSet<String>(directParameters.keySet());
		keySet.addAll(crossParameters.keySet());
		
		for (String key : keySet) {

			// result of directValidation
			boolean directValidation = true;

			// validate direct parameters
			if (directParameters.containsKey(key)) {

				// cycle on direct parameters
				List<Element> elementList = directParameters.get(key);
				for (Element e : elementList) {

					// validate parameter
					try {

						// check allowed values or use specific methods
						if ((e.getAllowedValues() != null && !this.isIn(e.getValue(), e.getAllowedValues())) || (e.getAllowedValues() == null && !this.isValid(e.getMethods(), e.getValue()))) {

							// on error add to result and block cross
							// comparisons
							r.addField(key, e.getMessage());
							directValidation = false;

							// show only one error message
							break;
						}
					}

					// an error occurred...
					catch (Exception ex) {
						r.addField(key, MESSAGE_GENERIC_ERROR);
						logger.error("Errore validazione direct", ex);

						// block cross comparisons
						directValidation = false;

						// show only one error message
						break;
					}
				}
			}

			// validate cross parameters
			if (directValidation && crossParameters.containsKey(key)) {

				// cycle on cross parameters
				List<Element> elementList = crossParameters.get(key);
				for (Element e : elementList) {

					// validate parameter
					try {

						// use specific methods
						if (!this.areValid(e.getMethods(), e.getValue(), e.getComparingValue())) {

							// on error add to result
							r.addField(key, e.getMessage());

							// show only one error message
							break;
						}
					}

					// an error occurred...
					catch (Exception ex) {
						r.addField(key, MESSAGE_GENERIC_ERROR);
						logger.error("Errore validazione cross", ex);

						// show only one error message
						break;
					}
				}
			}
		}

		// if there are some errors, then validation is NOK
		if (!r.getFields().entrySet().isEmpty()) {
			valid = false;
		}
		r.setResult(valid);

		// return result of validation
		return r;
	}

	/**
	 * Check if parameter is valid against a list of checker methods
	 * 
	 * @param methods
	 *            list of checker methods
	 * @param value
	 *            value to check
	 * @return boolean TRUE if valid, FALSE otherwise
	 * @throws Exception
	 */
	private boolean isValid(List<String> methods, String value) throws Exception {

		Method method;
		for (String m : methods) {
			method = this.getClass().getDeclaredMethod(m, String.class);

			// reflection on method
			if (!(boolean) method.invoke(this, value)) {
				// stop checking on wrong validation
				return false;
			}
		}

		// validation OK
		return true;
	}

	/**
	 * Check if value is allowed
	 * 
	 * @param value
	 *            value to check
	 * @param allowedValues
	 *            set of allowed values
	 * @return
	 */
	private boolean isIn(String value, Object[] allowedValues) {

		for (Object val : allowedValues) {
			if (val.toString().equalsIgnoreCase(value)) {
				// if is in then OK
				return true;
			}
		}

		// value not allowed
		return false;
	}

	/**
	 * Check if values are valid
	 * 
	 * @param methods
	 *            list of checker methods
	 * @param value0
	 *            first value to compare
	 * @param value1
	 *            second value to compare
	 * @return boolean TRUE if valid, FALSE otherwise
	 * @throws Exception
	 */
	private boolean areValid(List<String> methods, String value0, String value1) throws Exception {

		Method method;
		for (String m : methods) {
			method = this.getClass().getDeclaredMethod(m, String.class, String.class);

			// reflection on method
			if (!(boolean) method.invoke(this, value0, value1)) {
				// stop checking on wrong validation
				return false;
			}
		}

		// validation OK
		return true;
	}

	/*************************************************************************************************
	 * CHECKING METHODS
	 ************************************************************************************************/

	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean isNotEmpty(String string) {
		return string != null && !string.isEmpty();
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean isIva(String string) {
		String regex = "^[0-9]{11}$";
		return string != null && string.matches(regex);
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean isCodiceFiscale(String string) {
		String regex = "^[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z]$";
		return string != null && string.matches(regex);
	}

	/*
	 * public boolean isNation(String string) { if (isAlphabetic(string) &&
	 * !string.equals(non_scelta)) { return null; } else { return
	 * MESSAGE_NATION_NOT_VALID; } }
	 * 
	 * public String isCity(String string) { if (isAlphabetic(string) &&
	 * !string.equals(non_scelta)) { return null; } else { return
	 * MESSAGE_CITY_NOT_VALID; } }
	 * 
	 * public String isProvince(String string) {
	 * 
	 * String regex = "[A-Z]{2}"; if (string.matches(regex) &&
	 * !string.equals(non_scelta)) { return null; } else { return
	 * MESSAGE_PROVINCE_NOT_VALID; } }
	 */

	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean isCAP(String string) {
		String regex = "^[0-9]{5}$";
		return string != null && string.matches(regex);
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean maxLengthAddress(String string) {
		return string != null && string.length() > 0 && string.length() <= 20;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean isEmail(String string) {
		String regex = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$";
		return string != null && string.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isAirport(String value) {
		if (isAlphabetic(value))
			return true;
		else
			return false;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	private boolean isDate(String string) {

		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
			df.setLenient(false);
			df.parse(string);
		} catch (ParseException e) {
			logger.warn("Error during parse date", e);
			return false;
		}

		return true;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isFuture(String value) {
//		Date now = new Date();
//		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
//		try {
//			Date date = df.parse(value);
//			if (date.before(now)) {
//				return false;
//			}
//		} catch (ParseException e) {
//			logger.warn("Error during parse date", e);
//			return false;
//		}
//
//		return true;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
		df.setLenient(false);
		Calendar cal  = Calendar.getInstance();
		Calendar yesterday  = Calendar.getInstance();
		yesterday.add(Calendar.DAY_OF_MONTH, -1);
		
		try {
			cal.setTime(df.parse(value));
			if (cal.before(yesterday)){
				return false;
			}
		} catch (ParseException e) {
			logger.warn("Error during parse date", e);
			return false;
		}

		return true;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isBeforeToday(String value) {
		Date now = new Date();
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
		df.setLenient(false);
		try {
			Date date = df.parse(value);
			if (date.after(now)) {
				return false;
			}
		} catch (ParseException e) {
			logger.warn("Error during parse date", e);
			return false;
		}

		return true;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isRangeAdults(String value) {
		String regex = "^[1-7]{1}$";
		return value != null && value.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isNumber(String value) {
		String regex = "^[0-9]+$";
		return value != null && value.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isIata(String value) {
		String regex1 = "38[0-9]+";
		String regex2 = "70[0-9]+";
		return (value.matches(regex1) || value.matches(regex2));
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isPassword(String value) {
		String regex_uc = "^[a-zA-Z0-9]*[A-Z]+[a-zA-Z0-9]*$";
		String regex_nm = "^[a-zA-Z0-9]*[0-9]+[a-zA-Z0-9]*$";
		if (value == null) {
			return false;
		}
		if (value.length() > 7 && value.matches(regex_uc) && value.matches(regex_nm)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isRangeKids(String value) {
		String regex = "^[0-6]{1}$";
		return value != null && value.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isRangeBabies(String value) {
		String regex = "^[0-7]{1}$";
		return value != null && value.matches(regex);
	}

	/**
	 * 
	 * @param nadults
	 * @param nkids
	 * @return
	 */
	private boolean underMaxSumAdultsKids(String nadults, String nkids) {
		return Integer.parseInt(nadults) + Integer.parseInt(nkids) <= 7;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isAlphabetic(String value) {
		String regex = "^[a-zA-Z\\s]+$";
		return value != null && value.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isAlphaNumeric(String value) {
		String regex = "^[a-zA-Z0-9]+$";
		return value != null && value.matches(regex);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isFlightNumber(String value) {
		String regex = "^[0-9]{1,4}$";
		return value != null && value.matches(regex);
	}

	/**
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	private boolean beforeThen(String start, String end) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
		df.setLenient(false);
		try {
			Date date0 = df.parse(start);
			Date date1 = df.parse(end);

			if (date0.after(date1))
				return false;
		} catch (ParseException e) {
			logger.warn("Error during parse date", e);
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param nadults
	 * @param nbabies
	 * @return
	 */
	private boolean babyPerAdult(String nadults, String nbabies) {
		return Integer.parseInt(nadults) >= Integer.parseInt(nbabies);
	}
	
	/**
	 * It checks if the adults ar in the range [2-7]
	 * @param nadults
	 * @return
	 */
	private boolean isRangeAdultForFamily(String nadults) {
		String regex = "^[2-7]{1}$";
		return nadults != null && nadults.matches(regex);
	}
	
	/**
	 * It checks if at least one kids or one babies is selected
	 * @param value
	 * @return
	 */
	private boolean isRangeKidsAndBabiesForFamily(String nkids, String nbabies) {
		return Integer.parseInt(nkids) > 0 || Integer.parseInt(nbabies) > 0;
	}

	/**
	 * 
	 * @param value1
	 * @param value2
	 * @return
	 */
	private boolean areEqual(String value1, String value2) {
		return value1 != null && value2 != null && value1.equals(value2);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isTimeSlot(String value) {
		String[] slots = new String[] { "tutto", "mattina", "pomeriggio", "sera" };
		for (String s : slots){
			if (s.equals(value.toLowerCase())){
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean areInitials(String value) {
		return "AZ".equals(value);
	}

	/**
	 * 
	 * @param value1
	 * @param value2
	 * @return
	 */
	private boolean areDifferent(String value1, String value2) {
		return !areEqual(value1, value2);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isNumeroTelefonoValido(String value) {
		return value.matches("^[0-9]{5,15}");
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isCug(String value) {
		return value == null || ("YTH").equals(value) || ("ADT").equals(value) || ("MIL").equals(value) || ("FAM").equals(value);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isNomeValido(String value) {
		return value.length() >= 2 && 
				value.matches("^[^\\\u00B6\\\"\\\"\\!\\#\\$\\%\\&\\\u00A3\\(\\)\\*\\+\\,\\-\\.\\/\\:\\;\\<\\=\\>\\?\\@\\[\\\\\\]\\^\\_\\`\\{\\|\\}\\~\\\u00A6\\\u00F8\\\u00BF\\\u00AC\\\u00BD\\\u00BC\\\u00AB\\\u00BB\\\u00A6\\+\\-\\\u00A4\\\u00F0\\\u00AF\\\u00FE\\\u00DE\\\u00AF\\\u00B4\\\u00AC\\\u00B1\\=\\\u00BE\\\u00F7\\\u00B8\\\u00A8\\\u20221234567890\u20AC]{0,27}$");
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isCodiceBluebizValido(String value) {
		return value.matches("^[a-zA-Z0-9]{1,9}$");
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean isUsername(String value) {
		boolean code = isNumber(value) && (value.length() == 6 || value.length() == 7);
		boolean iva = isIva(value);
		return code || iva;
	}
	
	
	private boolean isMinusTwoPlusSevenDay(String day) throws ParseException{
		
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(new Date());
		cal1.add(Calendar.DATE, -3);
		
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(new Date());
		cal2.add(Calendar.DATE, 6);
		
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
		df.setLenient(false);
		Date date = df.parse(day);
		Calendar cal = Calendar.getInstance();
		
		cal.setTime(date);
		
		return cal.after(cal1) && cal.before(cal2);
		
	}
	
private boolean isMinusTwoPlusSevenDayIncluded(String day) throws ParseException{
		
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(new Date());
		cal1.add(Calendar.DATE, -3);
		
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(new Date());
		cal2.add(Calendar.DATE, 7);
		
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
		df.setLenient(false);
		Date date = df.parse(day);
		Calendar cal = Calendar.getInstance();
		
		cal.setTime(date);
		
		return cal.after(cal1) && cal.before(cal2);
		
	}
	
	private boolean isYear(String value) {
		return value != null && value.matches("[0-9][0-9][0-9][0-9]");
	}
	
	private boolean isValidCodiceFiscaleByConcatNomeCognomeCF(String validatorCF) {
		
		validatorCF = validatorCF.replace(" ","");
		validatorCF = validatorCF.toUpperCase();
		String[] params = validatorCF.split("\\|");
		String nome = params[0];
		String cognome = params[1];
		String CF = params[2];
		
		final List<String> vocali = new ArrayList<String>();
		final String[] vocArray = {"A","E","I","O","U"};
		for (String v : vocArray ) {
			vocali.add(v);
		}
		String cogToCompare = "";
		int cont = 0;
		
		if (CF.length() < 6) {
			return false;
		}
		if (nome == null || ("").equals(nome) || 
				cognome == null || ("").equals(cognome)) {
			return false;
		}
		
		/*cognome minore di 3 lettere*/
		if (cognome.length()<3){
			cogToCompare += cognome;
			while (cogToCompare.length()<3) {
				cogToCompare += "X";
			}
		} else {
		
			/*caso normale - le prime tre consonanti*/
			for (int i=0; i<cognome.length(); i++) {
				if (cont==3) {
					break;
				}
				if (!vocali.contains(Character.toString(cognome.charAt(i)))) {
					cogToCompare += cognome.charAt(i);
					cont++;
				}
			}
		
			/* caso meno di 3 consonanti*/
			while (cont<3) {
				for (int i=0; i<cognome.length(); i++) {
					if (cont==3) {
						break;
					}
					if (vocali.contains(Character.toString(cognome.charAt(i)))) {
						cogToCompare += cognome.charAt(i);
						cont++;
					}
				}
			}
		}
		
		/*lettere nome*/
		cont = 0;
		String nomToCompare = "";
		/*caso nome minore di 3 lettere*/
		if (nome.length()<3) {
			nomToCompare += nome;
			while (nomToCompare.length()<3) {
				nomToCompare+= "X";
			}
			cont=3;
		}
		
		String nomeCom = nome;
		for (String car : vocArray) {
			nomeCom = nomeCom.replaceAll(car, "");
		}
		if (nomeCom.length() >= 4) {
			/*caso normale consonanti >= 4 -- 1^ 3^ 4^ consonante*/
			char[] conInName = new char[3];
			conInName[0] =  nomeCom.charAt(0);
			conInName[1] =  nomeCom.charAt(2);
			conInName[2] =  nomeCom.charAt(3);
			nomToCompare = new String(conInName);
		} else if (nomeCom.length() == 3) {
			nomToCompare = nomeCom;
		} else {
			/* caso meno di 3 consonanti*/
			nomToCompare = nomeCom;
			cont = nomToCompare.length();
			for (int i=0;i<nome.length();i++) {
				if (cont==3) {
					break;
				}
				if (vocali.contains(Character.toString(nome.charAt(i)))) {
					nomToCompare += nome.charAt(i);
					cont++;
				}
			}
		}
		String cFCog = CF.substring(0, 3);
		String cFNome = CF.substring(3, 6);
		
		return (cFCog.equals(cogToCompare) && cFNome.equals(nomToCompare));
	}
	
	private boolean isPartitaIVA(String piva){
		return piva.matches("^([0-9]){11}$");
	}
	
	private boolean isNotEqualsPassengerNames(String inputNames, String concatPassengerNames){
		inputNames = inputNames.replaceAll(" ", "");
		String[] passengersNames = concatPassengerNames.split("\\|");
		int cont = 0;
		for (String name : passengersNames) {
			if (name.equalsIgnoreCase(inputNames)) {
				cont++;
			}
		}
		return (cont == 1);
	}
	
	private boolean isCVCAMEX(String cvc){
		return cvc.length() == 4;
	}
	
	private boolean isCVCVISA(String cvc){
		return cvc.length() == 3;
	}
	
	private boolean isCVCMASTERCARD(String cvc){
		return cvc.length() == 3;
	}
	
	
	
}
