package com.alitalia.aem.business.web.booking.controller;

import java.util.ArrayList;
import java.util.ResourceBundle;

import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.model.FlightSelection;
import com.alitalia.aem.business.web.booking.render.SelectedFlightInfoRender;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.alitalia.aem.common.data.home.FlightData;
import com.day.cq.i18n.I18n;

public class BookingScegliVoloTravelInfo extends BookingSessionGenericController {

	private SelectedFlightInfoRender selectedFlightInfo;
	private int indexRoute;
	private boolean showButtonReturn;
	private boolean showDisclaimer;
	private boolean showFareRules;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private boolean fareRulesStatic;
	
	@Override
	public void activate() throws Exception {
		super.activate();
		
		try {
			if (isIllegalCTXState(getResponse())) {
				return;
			}
			showFareRules = true;
			fareRulesStatic = configuration.isFareRulesStaticOn();
			SlingHttpServletRequest request = getRequest();
			ResourceBundle resourceBundle = request.getResourceBundle(
					LocalizationUtils.getLocaleByResource(request.getResource(), LocalizationUtils.getLanguage(ctx.locale.toLanguageTag())));
			final I18n i18n = new I18n(resourceBundle);
			
			if( ( getRequest().getParameter("indexRoute")==null || ("").equals(getRequest().getParameter("indexRoute")) ) || ( !("1").equals(getRequest().getParameter("indexRoute")) && !("0").equals(getRequest().getParameter("indexRoute")) ) ){
				logger.error("indexRoute not found");
				throw new RuntimeException();		
			}
			indexRoute = Integer.parseInt(getRequest().getParameter("indexRoute"));
			
			int searchElementIndex;
			if (ctx.searchKind == BookingSearchKindEnum.MULTILEG) {
				showFareRules = false;
				searchElementIndex = ctx.currentSliceIndex - 1;
			} else {
				searchElementIndex = indexRoute;
			}
			int indexFlight = Integer.parseInt(getRequest().getParameter("indexFlight"));
			if ( getRequest().getParameter("selectedFlight").equals("") || ctx.flightSelections[searchElementIndex] == null ) {
				FlightData flightData = ctx.availableFlights.getRoutes().get(indexRoute).getFlights().get(indexFlight);

				Boolean isSoggettoAppGov=computeIsSoggettoAppGov(obtainFlightNumbers(flightData));
                flightData.setIsSoggettoApprovazioneGovernativa(isSoggettoAppGov);

				FlightSelection partialFlightSelection = new FlightSelection(flightData, "", null, null, null, false, -1, ctx.availableFlights.getSolutionId());
				selectedFlightInfo = new SelectedFlightInfoRender(partialFlightSelection,
						i18n, new ArrayList<Integer>());
				showDisclaimer = false;
			} else {
				selectedFlightInfo = new SelectedFlightInfoRender(ctx.flightSelections[searchElementIndex],
						i18n, ctx.flightSelections[searchElementIndex].getFlightsMileage());
				showDisclaimer = true;
			}
			
			showButtonReturn = false;
			if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP && indexRoute == 0) {
				showButtonReturn = true;
			}
			
		} catch (Exception e) {
			logger.error("Error FlightInfoController.java ", e);
		}
	}

	private Boolean computeIsSoggettoAppGov(String numeroVolo) {
		String[] arrayVolo =configuration.getListaVoliSoggettiApprovazioneGovernativa().split(";");
		for (int i=0; i<arrayVolo.length; i++){
			if (arrayVolo[i].equals(numeroVolo)) {return true;}
		}
		return false;
	}

	private String obtainFlightNumbers(FlightData flightData) {
		String result = "";
		//FlightData flightData = flightSelection.getFlightData();
		if (flightData instanceof DirectFlightData) {
			DirectFlightData directFlightData = (DirectFlightData) flightData;
			return directFlightData.getCarrier() + directFlightData.getFlightNumber();
		} else {
			ConnectingFlightData connectingFlightData = (ConnectingFlightData) flightData;
			for (FlightData flight : connectingFlightData.getFlights()) {
				DirectFlightData directFlightData = (DirectFlightData) flight;
				String number = directFlightData.getCarrier() + directFlightData.getFlightNumber();
				if (!result.equals("")) {
					result += ", " + number;
				} else {
					result = number;
				}
			}
		}
		return result;
	}

	public int getIndexRoute() {
		return indexRoute;
	}

	public SelectedFlightInfoRender getSelectedFlightInfo() {
		return selectedFlightInfo;
	}

	public boolean getShowButtonReturn() {
		return showButtonReturn;
	}
	
	public boolean getShowDisclaimer() {
		return showDisclaimer;
	}
	
	public boolean getShowFareRules() {
		return showFareRules;
	}

	public boolean isFareRulesStatic() {
		return fareRulesStatic;
	}

}
