package com.alitalia.aem.business.web.booking;

public enum BookingSearchCUGEnum {
	
	ADT("ADT"),
	MILITARY("MIL"),
    YOUTH("YTH"),
    FAMILY("FAM");
	
    private final String value;
    
    BookingSearchCUGEnum(String v) {
        value = v;
    }
    
    public String value() {
    	return this.value;
    }
    
    public static BookingSearchCUGEnum fromValue(String v) {
        for (BookingSearchCUGEnum c: BookingSearchCUGEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}