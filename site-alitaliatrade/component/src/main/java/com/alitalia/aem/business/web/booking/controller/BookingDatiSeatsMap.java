package com.alitalia.aem.business.web.booking.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.model.Passenger;
import com.alitalia.aem.business.web.booking.model.PassengerType;
import com.alitalia.aem.business.web.booking.model.PassengersData;
import com.alitalia.aem.business.web.booking.render.DirectFlightRender;
import com.alitalia.aem.business.web.booking.render.SeatsMapRender;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.SeatMapData;

public class BookingDatiSeatsMap extends BookingSessionGenericController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private ArrayList<DirectFlightRender> flightsList;
	
	private PassengersData passengersData;
	
	@Override
	public void activate() throws Exception {
		super.activate();
		try {
			
			this.passengersData = new PassengersData();
			if (ctx.passengersData != null) {
				if (ctx.passengersData.getContact1() != null) {
					this.passengersData.setContact1(
							ctx.passengersData.getContact1().getContactType(),
							ctx.passengersData.getContact1().getInternationalPrefix(),
							ctx.passengersData.getContact1().getPhoneNumber());
				}
				if (ctx.passengersData.getContact2() != null) {
					this.passengersData.setContact2(
							ctx.passengersData.getContact2().getContactType(),
							ctx.passengersData.getContact2().getInternationalPrefix(),
							ctx.passengersData.getContact2().getPhoneNumber());
				}
				this.passengersData.setEmail(ctx.passengersData.getEmail());
				this.passengersData.setPassengersList(new ArrayList<Passenger>());
				if (ctx.passengersData.getPassengersList() != null) {
					for (Passenger pax : ctx.passengersData.getPassengersList()) {
						if (pax.getPassengerType() != PassengerType.INFANT) {
							this.passengersData.addPassenger(pax);
						}
					}
				}
			}
			
			this.flightsList = new ArrayList<DirectFlightRender>();
			for (Entry<DirectFlightData, SeatMapData> entry : ctx.seatMapsByFlight.entrySet()) {
				DirectFlightRender voloRender = new DirectFlightRender(entry.getKey());
				SeatsMapRender mappaPosti = new SeatsMapRender(entry.getValue());
				voloRender.setMappaPosti(mappaPosti);
				this.flightsList.add(voloRender);
			}
			
		} catch (Exception e) {
			logger.error("Unexpected exception:", e);
			throw (e);
		}
	}
	
	public List<DirectFlightRender> getFlightsList() {
		return this.flightsList;
	}

	public PassengersData getPassengersData() {
		return passengersData;
	}
	
}
