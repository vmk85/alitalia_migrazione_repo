package com.alitalia.aem.business.web.controllers;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;

@SuppressWarnings("deprecation")
public class UnsubscribeNewsletterIATAProperties extends WCMUse {

	private Logger logger = LoggerFactory.getLogger(UnsubscribeNewsletterIATAProperties.class);
	
	private String username;
	private String id;
	
	@Override
    public void activate() throws Exception {
		
		try {
			SlingHttpServletRequest request = getRequest();
			username = request.getParameter("username");
			id = request.getParameter("id");
			
			logger.debug("UnsubscribeNewsletterIATAProperties read username: "+ username +" and id: "+ id);
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
