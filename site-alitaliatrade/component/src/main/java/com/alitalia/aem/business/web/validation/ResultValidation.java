package com.alitalia.aem.business.web.validation;

import java.util.LinkedHashMap;
import java.util.Map;

public class ResultValidation {
	private boolean result;
	private Map<String, String> fields = new LinkedHashMap<String, String>();

	public boolean getResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public Map<String, String> getFields() {
		return this.fields;
	}

	public void setFields(Map<String, String> fields) {
		this.fields = fields;
	}

	public void addField(String key, String value) {
		this.fields.put(key, value);
	}

}
