package com.alitalia.aem.business.web.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.controller.BookingSessionGenericController;
import com.alitalia.aem.business.web.booking.model.FlightSelection;
import com.alitalia.aem.business.web.booking.render.RegoleTariffarieTrattaRender;

public class RegoleTariffarieTratte extends BookingSessionGenericController{
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private List<RegoleTariffarieTrattaRender> flightSelectionFareRules;
	private boolean showFareRules;
	private boolean fareRulesStatic;
	
	@Override
	public void activate() throws Exception {
		super.activate();
		
		try{
			if (isIllegalCTXState(getResponse())){
				return;
			}
			showFareRules = ctx.searchKind != BookingSearchKindEnum.MULTILEG;
			fareRulesStatic = configuration.isFareRulesStaticOn();
			flightSelectionFareRules = new ArrayList<RegoleTariffarieTrattaRender>();
			
			int index = 0;
			String titolo = "";
			for ( FlightSelection flightSelction : ctx.flightSelections ) {
				if (ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP 
						|| ctx.searchKind == BookingSearchKindEnum.SIMPLE) {
					
					if (index==0) { //andata
						titolo = "REGOLE TARIFFARIE PER IL VOLO DI ANDATA";
					} else { //ritorno
						titolo = "REGOLE TARIFFARIE PER IL VOLO DI RITORNO";
					}
					
				}else{ //Multitratta
					titolo = "REGOLE TARIFFARIE PER LA TRATTA " + Integer.toString(index+1);
				}
				flightSelectionFareRules.add(setFlightSelectionFareRules(flightSelction.getFareRules(), titolo));
				index++;
			}
		}catch(Exception e){
			logger.error("Unexpected error: ", e);
			throw e;
		}
		
	}
	
	public List<RegoleTariffarieTrattaRender> getFlightSelectionFareRules() {
		return flightSelectionFareRules;
	}
	
	public boolean getShowFareRules() {
		return showFareRules;
	}
	
	public boolean isFareRulesStatic() {
		return fareRulesStatic;
	}

	
	/*private methods*/

	private RegoleTariffarieTrattaRender setFlightSelectionFareRules(Map<String, String> selectionFareRules, String titolo) {
		
		if(selectionFareRules==null || selectionFareRules.isEmpty()){
			return null;
		}
		Map<String,String> fareRulesFirstColumn = new LinkedHashMap<String, String>();
		Map<String,String> fareRulesSecondColumn = new LinkedHashMap<String, String>();
		
		int len = selectionFareRules.size();
		int i=0;
		Set<Entry<String,String>> entries =  selectionFareRules.entrySet();
		Iterator<Entry<String,String>> it = entries.iterator();
		while(i<len/2){
			Entry<String,String> entry = it.next();
			fareRulesFirstColumn.put(entry.getKey(), entry.getValue());
			i++;
		}
		while(i<len){
			Entry<String,String> entry = it.next();
			fareRulesSecondColumn.put(entry.getKey(), entry.getValue());
			i++;
		}
		
		return new RegoleTariffarieTrattaRender(fareRulesFirstColumn,fareRulesSecondColumn,titolo);
			
	}
}
