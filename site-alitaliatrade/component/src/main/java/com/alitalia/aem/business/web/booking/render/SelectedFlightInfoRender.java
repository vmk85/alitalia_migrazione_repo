package com.alitalia.aem.business.web.booking.render;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.alitalia.aem.business.web.booking.model.FlightSelection;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.enumerations.FlightTypeEnum;
import com.alitalia.aem.common.utils.FlightDataUtils;
import com.day.cq.i18n.I18n;

public class SelectedFlightInfoRender{
	
	private FlightSelection flightSelection;
	private List<DirectFlightDataRender> usedFligthsInSelection;
	private List<Integer> connectingFlightsWait;
	private Map<String,String> fareRulesFirstColumn;
	private Map<String,String> fareRulesSecondColumn;
	private boolean showFareRules;
	private List<Integer> mileage;

	public SelectedFlightInfoRender(FlightSelection flightSelection, I18n i18n, List<Integer> mileage){
		this.flightSelection = flightSelection;
		this.usedFligthsInSelection = new ArrayList<DirectFlightDataRender>();
		this.fareRulesFirstColumn = new LinkedHashMap<String, String>();
		this.fareRulesSecondColumn = new LinkedHashMap<String, String>();
		this.showFareRules = false;
		this.mileage = mileage;
		init(i18n);
	}
	
	public FlightSelection getFlightSelection() {
		return flightSelection;
	}

	public List<DirectFlightDataRender> getUsedFligthsInSelection() {
		return usedFligthsInSelection;
	}

	public Map<String, String> getFareRulesFirstColumn() {
		return fareRulesFirstColumn;
	}

	public Map<String, String> getFareRulesSecondColumn() {
		return fareRulesSecondColumn;
	}
	
	public boolean getShowFareRules() {
		return showFareRules;
	}

	
	/*private methods*/
	
	private void init(I18n i18n) {
		connectingFlightsWait = FlightDataUtils.computeConnectingFlightsWait(flightSelection.getFlightData());
		usedFligthsInSelection = computeUsedFlightsInSelection(flightSelection.getFlightData(), i18n, mileage);
		setFareRulesInTwoColumns(flightSelection.getFareRules());
		
	}


	private void setFareRulesInTwoColumns(Map<String, String> fareRules) {
		if(fareRules==null || fareRules.isEmpty()){
			this.showFareRules = false;
			return;
		}
		int len = fareRules.size();
		int i=0;
		Set<Entry<String,String>> entries =  fareRules.entrySet();
		Iterator<Entry<String,String>> it = entries.iterator();
		while(i<len/2){
			Entry<String,String> entry = it.next();
			this.fareRulesFirstColumn.put(entry.getKey(), entry.getValue());
			i++;
		}
		while(i<len){
			Entry<String,String> entry = it.next();
			this.fareRulesSecondColumn.put(entry.getKey(), entry.getValue());
			i++;
		}
		this.showFareRules = true;
			
	}

	private List<DirectFlightDataRender> computeUsedFlightsInSelection(FlightData flightData, I18n i18n, List<Integer> mileage) {
		
		List<DirectFlightDataRender> directFlightsRenderList = new ArrayList<DirectFlightDataRender>();
		int miglia = 0;
		if(flightData.getFlightType() == FlightTypeEnum.CONNECTING){
			ConnectingFlightData connectingFlights = (ConnectingFlightData) flightData;
			int index = 0;
			for(FlightData flight : connectingFlights.getFlights()){
				DirectFlightData directFlight = (DirectFlightData) flight;
				if(this.mileage!=null && !this.mileage.isEmpty()){
					miglia = this.mileage.get(index).intValue();
				}
				directFlightsRenderList.add(
						new DirectFlightDataRender(directFlight, this.connectingFlightsWait.get(index), i18n, miglia));
				index++;
			}
		}else{ //direct flight
			DirectFlightData directFlight = (DirectFlightData) flightData;
			if(this.mileage!=null && !this.mileage.isEmpty()){
				miglia = this.mileage.get(0).intValue();
			}
			directFlightsRenderList.add(
					new DirectFlightDataRender(directFlight, this.connectingFlightsWait.get(0), i18n, miglia));
		}
		
		return directFlightsRenderList;
	}

}
