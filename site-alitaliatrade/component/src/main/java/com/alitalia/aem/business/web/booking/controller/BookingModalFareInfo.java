package com.alitalia.aem.business.web.booking.controller;

import com.alitalia.aem.ws.booking.infopassengerservice.xsd2.DirectFlight;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.common.data.home.BrandData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;

import java.util.ArrayList;
import java.util.List;

public class BookingModalFareInfo extends WCMUse {
	
	private String modal;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void activate() throws Exception {
		
		try{
			modal="";
			BookingSessionContext ctx = (BookingSessionContext) getRequest().getSession().getAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE);

			//aggiungo tutti i voli risultato in due liste: voli diretti e voli con connessione
            List<DirectFlightData> allConnectingFlights = new ArrayList<DirectFlightData>();
            List<DirectFlightData> allDirectFlights = new ArrayList<DirectFlightData>();
            for ( RouteData routesData : ctx.availableFlights.getRoutes() ){
                for ( FlightData flightData : routesData.getFlights()){
                    if (flightData instanceof DirectFlightData){
                        allDirectFlights.add((DirectFlightData) flightData);
                    } else if (flightData instanceof ConnectingFlightData){
                        for (FlightData flight : ((ConnectingFlightData) flightData).getFlights()){
                            if (flight instanceof DirectFlightData){
                                DirectFlightData directFlightData = (DirectFlightData) flight;
                                allConnectingFlights.add(directFlightData);
                            }
                        }
                    }
                }
            }

            //controllo l'area di tutti i voli diretti leggendola dalla citta "from" e "to"
            AreaValueEnum directArea;
            int maxDirectArea = 0;
            for (DirectFlightData singleDirectFlight : allDirectFlights){
                directArea = singleDirectFlight.getFrom().getArea();
                switch (directArea.value()){
                    case "DOM":
                        maxDirectArea = Math.max(maxDirectArea, 1);
                        break;
                    case "INTZ":
                        maxDirectArea = Math.max(maxDirectArea, 2);
                        break;
                    case "INTC":
                        maxDirectArea = Math.max(maxDirectArea, 3);
                        break;
                    default:
                        break;
                }
                directArea = singleDirectFlight.getTo().getArea();
                switch (directArea.value()){
                    case "DOM":
                        maxDirectArea = Math.max(maxDirectArea, 1);
                        break;
                    case "INTZ":
                        maxDirectArea = Math.max(maxDirectArea, 2);
                        break;
                    case "INTC":
                        maxDirectArea = Math.max(maxDirectArea, 3);
                        break;
                    default:
                        break;
                }
            }

            AreaValueEnum connectingArea;
            int maxConnectingArea = 0;
            if(maxDirectArea == 0) { //se non esistono voli diretti controllo l'area di tutti i voli di connessione
                for (DirectFlightData singleConnectingFlight : allConnectingFlights) {
                    connectingArea = singleConnectingFlight.getFrom().getArea();
                    switch (connectingArea.value()) {
                        case "DOM":
                            maxConnectingArea = Math.max(maxConnectingArea, 1);
                            break;
                        case "INTZ":
                            maxConnectingArea = Math.max(maxConnectingArea, 2);
                            break;
                        case "INTC":
                            maxConnectingArea = Math.max(maxConnectingArea, 3);
                            break;
                        default:
                            break;
                    }
                    connectingArea = singleConnectingFlight.getTo().getArea();
                    switch (connectingArea.value()) {
                        case "DOM":
                            maxConnectingArea = Math.max(maxConnectingArea, 1);
                            break;
                        case "INTZ":
                            maxConnectingArea = Math.max(maxConnectingArea, 2);
                            break;
                        case "INTC":
                            maxConnectingArea = Math.max(maxConnectingArea, 3);
                            break;
                        default:
                            break;
                    }
                }
            }
            int maxArea = Math.max(maxDirectArea, maxConnectingArea);

            //adesso: in relazione al max area, controllo se esiste brand light e scelgo il modale da mostrare

            if(modal.equals("")) {
                switch (maxArea) {
                    case 2:
                        modal = "internazionale";
                        break;
                    case 3:
                        modal = "intercontinentale";
                        break;
                    default:
                        modal = "nazionale";
                        break;
                }
                if(modal.equals("intercontinentale")) {
                    if (this.checkForBrand(ctx.availableFlights.getRoutes().get(0).getFlights().get(0), "yx")){
                        modal = "intercontinentale-light-semiflex";
                    } else if (this.checkForBrand(ctx.availableFlights.getRoutes().get(0).getFlights().get(0), "yl")){
                        modal = "intercontinentale-light";
                    }
                }
            }

            logger.debug("LOG MODALE ALITALIATRADE: " + modal);
		}catch(Exception e){
			logger.error("Errore Booking dati.html: ", e);
		}

	}

	private String computeModalForFCOLIN(FlightData flightData) {
		return (this.checkForBrand(flightData, "fcolin") ? "fcolin" : "");
	}

	private Boolean checkForBrand(FlightData flightData, String brandCode) {
		for(BrandData brandData : flightData.getBrands()){
			if(brandData.getCode().toLowerCase().contains(brandCode)){
                logger.debug("LOG MODALE ALITALIATRADE: " + brandData.getCode().toLowerCase());

                return true;
			}
		}
		return false;
	}

//	private String computeModalBySliceType(DirectFlightData directFlight) {
//		if(directFlight.getFrom().getArea() == AreaValueEnum.INTC
//				|| directFlight.getTo().getArea() == AreaValueEnum.INTC){
//			return "intercontinentale";
//		}
//		if(directFlight.getFrom().getArea() == AreaValueEnum.INTZ
//				|| directFlight.getTo().getArea() == AreaValueEnum.INTZ){
//			return "internazionale";
//		}
//		if(directFlight.getFrom().getArea() == AreaValueEnum.DOM
//				&& directFlight.getTo().getArea() == AreaValueEnum.DOM){
//			return "nazionale";
//		}
//		return "";
//	}

	public String getModal() {
		return modal;
	}
	
}
