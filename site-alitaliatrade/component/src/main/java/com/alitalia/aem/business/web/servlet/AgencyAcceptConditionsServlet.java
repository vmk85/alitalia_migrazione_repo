package com.alitalia.aem.business.web.servlet;

import java.io.IOException;

import javax.jcr.SimpleCredentials;
import javax.security.auth.login.LoginException;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityException;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityProvider;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalUser;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.identityprovider.AlitaliaTradeLoginException;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.common.messages.home.AgencyInsertConsentRequest;
import com.alitalia.aem.common.messages.home.AgencyInsertConsentResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "agencyacceptconditions" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.")
})
@SuppressWarnings("serial")
public class AgencyAcceptConditionsServlet extends GenericFormValidatorServlet {
	
	@SuppressWarnings("unused")
	private ComponentContext componentContext;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Reference(target = "(isAlitaliaTradeIDP=true)", policy = ReferencePolicy.DYNAMIC)
	private volatile ExternalIdentityProvider identityProvider;
	
	
	@Reference
	private BusinessLoginDelegate businessLoginDelegate; 
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		
		Validator validator = new Validator();
		String check = request.getParameter("checkAccept");
		validator.addDirectParameter("checkCondizioni", check, Constants.MESSAGE_CHECK_NECESSARY, "isNotEmpty");
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
			boolean result = false;
			SimpleCredentials simpleCredentials = null;
		
			String userName = request.getParameter("j_username").substring(2); // il primo carattere è il ruolo
			String password = request.getParameter("j_password");
			simpleCredentials = new SimpleCredentials(userName, password.toCharArray());
		
			try {
				ExternalUser user = identityProvider.authenticate(simpleCredentials);
				
				if (user != null) {
					AgencyInsertConsentRequest consentRequest = new AgencyInsertConsentRequest();
					consentRequest.setTid(IDFactory.getTid());
					consentRequest.setSid(IDFactory.getSid(request));
					consentRequest.setIdAgenzia(userName);
					consentRequest.setFlagCondizioni(true);
					consentRequest.setFlagDatiPersonali(true); // forse va gestito 
					AgencyInsertConsentResponse consentResponse = businessLoginDelegate.insertConsent(consentRequest);
					result = consentResponse.isResult();
				}
				else {
					logger.error("AgencyAcceptConditions : User Not Found");
				}
			} catch (LoginException e) {
					if (e instanceof AlitaliaTradeLoginException) {
						AlitaliaTradeLoginException atle = (AlitaliaTradeLoginException) e;
						logger.error("AlitaliaTadeExcpetion. " + atle.toString());
						
					}
			} catch (ExternalIdentityException e) {
					logger.error("Error trying to check login.", e);
			}
			
			AgencyInsertConsentRequest consentRequest = new AgencyInsertConsentRequest();
			consentRequest.setTid(IDFactory.getTid());
			consentRequest.setSid(IDFactory.getSid(request));
			consentRequest.setIdAgenzia(userName);
			consentRequest.setFlagCondizioni(true);
			consentRequest.setFlagDatiPersonali(true); // forse va gestito 
			AgencyInsertConsentResponse consentResponse = businessLoginDelegate.insertConsent(consentRequest);
			result = consentResponse.isResult();
		
			response.setContentType("application/json");

			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			
			try {
				out.object();
				out.key("result").value(result);
				out.endObject();

			} catch (Exception e) {
				logger.error("Unexpected error: ", e);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		 
	}
}
