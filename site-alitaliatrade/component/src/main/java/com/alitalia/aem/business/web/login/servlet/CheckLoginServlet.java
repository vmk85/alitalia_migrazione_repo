package com.alitalia.aem.business.web.login.servlet;

import java.io.IOException;

import javax.jcr.SimpleCredentials;
import javax.security.auth.login.LoginException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityException;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalIdentityProvider;
import org.apache.jackrabbit.oak.spi.security.authentication.external.ExternalUser;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.identityprovider.AlitaliaTradeLoginException;
import com.alitalia.aem.business.web.statistics.utils.TradeStatisticUtils;
import com.alitalia.aem.common.data.home.enumerations.TradeStatisticDataTypeEnum;
import com.alitalia.aem.common.messages.home.RegisterStatisticsResponse;
import com.alitalia.aem.common.messages.home.TradeRegisterStatisticRequest;
import com.alitalia.aem.web.component.tradestatistics.delegate.RegisterTradeStatisticsDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true, immediate = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "checklogin" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "sling.servlet.extensions", value = { "json" })
})
@SuppressWarnings("serial")
public class CheckLoginServlet extends SlingAllMethodsServlet {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@SuppressWarnings("unused")
	private ComponentContext componentContext;

	@Reference(target = "(isAlitaliaTradeIDP=true)", policy = ReferencePolicy.DYNAMIC)
	private volatile ExternalIdentityProvider identityProvider;

	@Reference(policy=ReferencePolicy.DYNAMIC)
	private volatile RegisterTradeStatisticsDelegate registerStatisticsDelegate;

	@Reference
	private AlitaliaTradeConfigurationHolder configuration;

	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		logger.debug("Pre-login check requested.");
		ExternalUser user = null;
		boolean credentialsFound = false;
		boolean locked = false;
		boolean expired = false;
		boolean acceptRequired = false;
		boolean firstAccess = false;
		boolean isGruppiEnabled = true;
		String userName = "";
		if (identityProvider == null) {
			logger.error("Cannot check login data, proper identity provider not found.");
		} else {
			userName = request.getParameter("j_username");
			String password = request.getParameter("j_password");
			SimpleCredentials simpleCredentials = new SimpleCredentials(userName, password.toCharArray());
			try {
				user = identityProvider.authenticate(simpleCredentials);
				if (user != null) {
					credentialsFound = true;
				}
			} catch (LoginException e) {
				if (e instanceof AlitaliaTradeLoginException) {
					AlitaliaTradeLoginException atle = (AlitaliaTradeLoginException) e;
					credentialsFound = atle.isCredentialsFound();
					locked = atle.isLocked();
					expired = atle.isExpired();
					acceptRequired = atle.isAcceptRequired();
					firstAccess = atle.isFirstAccess();
					isGruppiEnabled = atle.isGruppiEnabled();
					logger.info("User Found = " + credentialsFound + " , User Locked = " + locked + " , First Access = " + firstAccess + " , Conditions not Accepted = " + acceptRequired + " , Password Expired = " + expired);
				}
			} catch (ExternalIdentityException e) {
				logger.error("Error trying to check login.", e);
			}
		}
		response.setContentType("application/json");
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		try {
			TradeRegisterStatisticRequest statisticRequest = null;
			TradeStatisticDataTypeEnum statisticDataType = TradeStatisticDataTypeEnum.LOGIN;
			if (credentialsFound && !locked && !expired && !firstAccess && !acceptRequired) {
				statisticRequest = TradeStatisticUtils.createStatisticRequet(statisticDataType,
						request, true, null, userName, configuration.getClientIpHeaders());
			} else {
				String errorDesc = "";
				if (locked) {
					errorDesc = "UserLocked";
				} else if (expired) {
					errorDesc = "PasswordExpired";
				} else if (firstAccess) {
					errorDesc = "IsFirstAccess";
				} else if (acceptRequired) {
					errorDesc = "conditionAcceptRequired";
				} else {
					errorDesc = "UserNotFound";
				}
				statisticRequest = TradeStatisticUtils.createStatisticRequet(statisticDataType,
						request, false, errorDesc, userName, configuration.getClientIpHeaders());
			}
			try {
				RegisterStatisticsResponse statisticResponse = registerStatisticsDelegate.registerStatistics(statisticRequest);
				if (statisticResponse == null) {
					logger.error("Statistic Service Failed");
				}
			} catch(Exception e) {
				logger.error("Statistic Service Failed");
			}

			json.object();
			json.key("credentialsFound");
			json.value(credentialsFound);
			json.key("isGruppiEnabled");
			json.value(isGruppiEnabled);
			json.key("locked");
			json.value(locked);
			json.key("expired");
			json.value(expired);
			json.key("firstAccess");
			json.value(firstAccess);
			json.key("acceptRequired");
			json.value(acceptRequired);
			json.endObject();
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

}
