package com.alitalia.aem.business.web.booking.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.booking.BookingPhaseEnum;
import com.alitalia.aem.business.web.booking.BookingSession;
import com.alitalia.aem.business.web.booking.BookingSessionContext;
import com.alitalia.aem.business.web.utils.Constants;
import com.day.cq.commons.TidyJSONWriter;

@Component(immediate = true, metatype = false)
@Service(Servlet.class)
@Properties({
	@Property(name = "sling.servlet.selectors", value = { "bookingchecknavigationprocess" }),
	@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
	@Property(name = "sling.servlet.extensions", value = { "json" }),
	@Property(name = "error.page", description = "Users will be redirected to the specified page after a failed outcome."),
	@Property(name = "sling.servlet.methods", value = { "GET" }) })
@SuppressWarnings("serial")
public class BookingCheckNavigationProcessServlet extends SlingSafeMethodsServlet {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	private BookingSession bookingSession;
	
	@Reference
	private AlitaliaTradeConfigurationHolder configuration;
	
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		String genericErrorPagePlain = "#";
		TidyJSONWriter json = new TidyJSONWriter(response.getWriter());
		response.setContentType("application/json");
		
		try {
			json.object();
			genericErrorPagePlain = configuration.getErrorNavigationPage();
			
			BookingSessionContext ctx = (BookingSessionContext) 
					request.getSession(true).getAttribute(Constants.BOOKING_CONTEXT_ATTRIBUTE);
			if (ctx == null) {
				logger.error("Cannot find booking session context in session.");
				throw new IllegalArgumentException("Cannot find booking session context in session.");
			}
			
			String destPhase = request.getParameter("destPhase");
			BookingPhaseEnum destinationPhase = BookingPhaseEnum.fromValue(destPhase);
			
			String residencyValue = (String) request.getParameter("residency");
			Boolean residency = new Boolean(residencyValue);
			String nocache = (String) request.getParameter("nocache");
			
			String callCenter = (String) request.getParameter("showCallCenterMsg");
			
			String redirect = bookingSession.forwardNavigationProcess(ctx, destinationPhase, residency, nocache, callCenter);
			if (redirect == null) {
				json.key("redirect").value(genericErrorPagePlain);
			}
			if (redirect != null && !("").equals(redirect)) {
				json.key("redirect").value(redirect);
			}
			json.endObject();
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			if (!response.isCommitted()) {
				try {
					json.key("redirect").value(genericErrorPagePlain);
					json.endObject();
				} catch (JSONException e1) {
					logger.error("Error building JSON");
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
				}
			}
		}
		
	}

}
