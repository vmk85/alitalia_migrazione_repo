package com.alitalia.aem.business.web.controllers;

import java.util.List;

import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.News;
import com.alitalia.aem.common.messages.home.RetrieveNewsDetailsRequest;
import com.alitalia.aem.common.messages.home.RetrieveNewsDetailsResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.news.delegate.SearchNewsDelegate;

public class DetailNews extends WCMUse {

	private String title;
	private String date;
	private String category;
	private String content;
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
    public void activate() throws Exception {
		
		SlingScriptHelper helper = getSlingScriptHelper();
		SearchNewsDelegate searchNewsDelegate = helper.getService(SearchNewsDelegate.class);
		
		String item = getRequest().getParameter("item");
		
		if (item != null) {
			
			try {
				
				int index = Integer.parseInt(item);
				
				@SuppressWarnings("unchecked")
				List<News> list = (List<News>) getRequest().getSession().getAttribute("newsList");
				if (list != null && !list.isEmpty() && list.size() > index) {
					
					News news = list.get(index);
					title = news.getTitle();
					date = news.getDay() + "/" + news.getMonth() + "/" + news.getYear();
					category = news.getCategory();
					
					RetrieveNewsDetailsRequest newsRequest = new RetrieveNewsDetailsRequest();
					newsRequest.setTid(IDFactory.getTid());
					newsRequest.setSid(IDFactory.getSid(getRequest()));
					newsRequest.setId(news.getId());
		
					RetrieveNewsDetailsResponse newsResponse = searchNewsDelegate.searchNewsDetails(newsRequest);
					
					content = newsResponse.getDetailedNews().getText();
				}
				
				
			
			} catch (NumberFormatException e) {
				logger.error("Parameter item not numeric", e);
			}
		} else {
			logger.error("Parameter item is null");
		}
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getDate() {
		return date;
	}
	
	public String getCategory() {
		return category;
	}
	
	public String getContent() {
		return content;
	}

}
