package com.alitalia.aem.business.web.booking;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.alitalia.aem.business.web.booking.model.FlightSelection;
import com.alitalia.aem.business.web.booking.model.PassengersData;
import com.alitalia.aem.business.web.booking.model.SearchElement;
import com.alitalia.aem.business.web.booking.model.SearchPassengersNumber;
import com.alitalia.aem.common.data.PassengerBase;
import com.alitalia.aem.common.data.home.AvailableFlightsData;
import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.ECouponData;
import com.alitalia.aem.common.data.home.FlightSeatMapData;
import com.alitalia.aem.common.data.home.FrequentFlyerTypeData;
import com.alitalia.aem.common.data.home.InfoCarnetData;
import com.alitalia.aem.common.data.home.MealData;
import com.alitalia.aem.common.data.home.PaymentData;
import com.alitalia.aem.common.data.home.PhonePrefixData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.SeatMapData;
import com.alitalia.aem.common.data.home.TaxData;
import com.alitalia.aem.common.data.home.enumerations.CabinEnum;
import com.alitalia.aem.common.data.home.enumerations.ResidencyTypeEnum;
import com.day.cq.i18n.I18n;

/**
 * The <code>BookingSessionContext</code> class contains the state of a booking
 * session, in order to be stored in the proper way (e.g. in HTTP session).
 * 
 * </p>It is meant to be initialized by the <code>BookingSession</code>
 * component service, and then provided to it as requested for booking
 * process management methods.</p>
 * 
 * <p>The values managed in the context shouldn't be modified
 * externally to the <code>BookingSession</code> component service.</p>
 */
public class BookingSessionContext {
	
	/* common data */

	public String sid;
	
	public String execution;
	
	public String cookie;
	
	public String sabreGateWayAuthToken;
	
	public String ipAddress;
	
	public BookingPhaseEnum phase;
	
	public Locale locale;
	
	/* info per statistiche */
	public String userID;
	
	public String clientIP;

	/**
	 * Contains the domain extracted from URL
	 */
	public String domain;
	
	/* search data */
	
	public boolean newSearch;
	
	public BookingSearchKindEnum searchKind;
	
	public List<SearchElement> searchElements;
	
	public SearchPassengersNumber searchPassengersNumber;
	
	public BookingSearchCUGEnum cug;
	
	public ResidencyTypeEnum residency;
	
	public String market;
	
	public String site;

	public String tradeAgencyCode;
	
	public String tradeAgencyContractCode;
	
	public String agencyVatNumber;

	public String currency;
	
	public int[] selectedDepartureDateChoices;
	
	
	/* search result data */
	
	public Boolean isAllowedContinuitaTerritoriale;
	
	public Boolean isSelectedContinuitaTerritoriale;
	
	public List<PassengerBase> extraChargePassengerList;

	public AvailableFlightsData availableFlights;
	
	public Boolean isApis;
	
	public Boolean isApisCanada;
	
	public Boolean isSecureFlight;
	
	public Boolean isSecureFlightESTA;
	
	public Boolean youthSolutionFound;
	
	public Boolean familySolutionFound;
	
	public Boolean militarySolutionFound;
	
	public boolean territorialContinuitySolutionFound;
	
	public Boolean solutionForSelectedDate[];
	
	public Boolean solutionForAllDates;
	
	public int numberOfShownFlights[];
	
	public int numberOfMoreFlights;
	
	public int initialNumberOfFlightsToShow;
	
	public int totalSlices;

	
	/* flight selections data */
	
	public FlightSelection[] flightSelections;
	
	public RoutesData selectionRoutes;
	
	public List<TaxData> selectionTaxes;
	
	public List<DirectFlightData> selectionAllDirectFlights;
	
	public List<DirectFlightData>[] selectionSearchElementDirectFlights;
	
	public CabinEnum selectionCabinClass;
	
	public boolean isRefreshed;
	
	public BigDecimal grossAmount;
	
	public BigDecimal grossAmountNoDiscount;
	
	public BigDecimal netAmountForPayment;
	
	public BigDecimal netAmount;
	
	public BigDecimal totalTaxes;
	
	public BigDecimal totalExtras;
	
	public BigDecimal totalExtraCharges;
	
	public BigDecimal totalCouponPrice;
	
	public int currentSliceIndex;
	
	public boolean readyToPassengersDataPhase;
	
	public boolean onlyHandBaggage;
	
	public ArrayList<String> fullTextFareRules;
	
	/* e-coupon data */
	
	public ECouponData coupon;
	
	public Boolean isCouponValid;
	
	public boolean isECouponWithTariffaLight;
	
	public int firstSelectionIndex;
	
	
	/* passengers and seats data */
	
	public Boolean isSeatMapSelectionAllowed;
	
	public List<FlightSeatMapData> seatMaps;
	
	public Map<DirectFlightData, SeatMapData> seatMapsByFlight;
	
	public List<CountryData> countries;
	
	public List<CountryData> districts;
	
	public List<PhonePrefixData> phonePrefixes;
	
	public List<MealData> mealTypes;
	
	public List<FrequentFlyerTypeData> frequentFlyerTypes;
	
	public PassengersData passengersData;
	
	
	/* prenotation data */
	
	public RoutesData prenotation;
	
	public InfoCarnetData carnetInfo;
	
	public PaymentData paymentData;
	
	public boolean isAuthorizePaymentInvoked;

	public String paymentTid;

	/* invoicing flags data */

	/**
	 * Indicates if user has requested invoice generation
	 */
	public Boolean invoiceRequired;

	/**
	 * Indicates if invoice generation request was successful
	 */
	public Boolean invoiceRequestSuccessful;
	
	public String brandCodeToRemove;

	public I18n i18n;

	
	@Override
	public String toString() {
		return "BookingSessionContext [sid=" + sid + ", execution=" + execution
				+ ", cookie=" + cookie + ", sabreGateWayAuthToken="
				+ sabreGateWayAuthToken + ", phase=" + phase + ", locale="
				+ locale + ", userID=" + userID + ", clientIP=" + clientIP
				+ ", domain=" + domain + ", newSearch=" + newSearch
				+ ", searchKind=" + searchKind + ", searchElements="
				+ searchElements + ", searchPassengersNumber="
				+ searchPassengersNumber + ", cug=" + cug + ", residency="
				+ residency + ", market=" + market + ", site=" + site
				+ ", tradeAgencyCode=" + tradeAgencyCode
				+ ", tradeAgencyContractCode=" + tradeAgencyContractCode
				+ ", currency=" + currency + ", selectedDepartureDateChoices="
				+ Arrays.toString(selectedDepartureDateChoices)
				+ ", isAllowedContinuitaTerritoriale="
				+ isAllowedContinuitaTerritoriale
				+ ", isSelectedContinuitaTerritoriale="
				+ isSelectedContinuitaTerritoriale
				+ ", extraChargePassengerList=" + extraChargePassengerList
				+ ", availableFlights=" + availableFlights + ", isApis="
				+ isApis + ", isApisCanada=" + isApisCanada
				+ ", isSecureFlight=" + isSecureFlight
				+ ", isSecureFlightESTA=" + isSecureFlightESTA
				+ ", youthSolutionFound=" + youthSolutionFound
				+ ", familySolutionFound=" + familySolutionFound
				+ ", militarySolutionFound=" + militarySolutionFound
				+ ", territorialContinuitySolutionFound="
				+ territorialContinuitySolutionFound
				+ ", solutionForSelectedDate="
				+ Arrays.toString(solutionForSelectedDate)
				+ ", solutionForAllDates=" + solutionForAllDates
				+ ", numberOfShownFlights="
				+ Arrays.toString(numberOfShownFlights)
				+ ", numberOfMoreFlights=" + numberOfMoreFlights
				+ ", initialNumberOfFlightsToShow="
				+ initialNumberOfFlightsToShow + ", totalSlices=" + totalSlices
				+ ", flightSelections=" + Arrays.toString(flightSelections)
				+ ", selectionRoutes=" + selectionRoutes + ", selectionTaxes="
				+ selectionTaxes + ", selectionAllDirectFlights="
				+ selectionAllDirectFlights
				+ ", selectionSearchElementDirectFlights="
				+ Arrays.toString(selectionSearchElementDirectFlights)
				+ ", selectionCabinClass=" + selectionCabinClass
				+ ", isRefreshed=" + isRefreshed + ", grossAmount="
				+ grossAmount + ", grossAmountNoDiscount="
				+ grossAmountNoDiscount + ", netAmountForPayment="
				+ netAmountForPayment + ", netAmount=" + netAmount
				+ ", totalTaxes=" + totalTaxes + ", totalExtras=" + totalExtras
				+ ", totalExtraCharges=" + totalExtraCharges
				+ ", totalCouponPrice=" + totalCouponPrice
				+ ", currentSliceIndex=" + currentSliceIndex
				+ ", readyToPassengersDataPhase=" + readyToPassengersDataPhase
				+ ", onlyHandBaggage=" + onlyHandBaggage
				+ ", fullTextFareRules=" + fullTextFareRules + ", coupon="
				+ coupon + ", isCouponValid=" + isCouponValid
				+ ", isECouponWithTariffaLight=" + isECouponWithTariffaLight
				+ ", firstSelectionIndex=" + firstSelectionIndex
				+ ", isSeatMapSelectionAllowed=" + isSeatMapSelectionAllowed
				+ ", seatMaps=" + seatMaps + ", seatMapsByFlight="
				+ seatMapsByFlight + ", countries=" + countries
				+ ", districts=" + districts + ", phonePrefixes="
				+ phonePrefixes + ", mealTypes=" + mealTypes
				+ ", frequentFlyerTypes=" + frequentFlyerTypes
				+ ", passengersData=" + passengersData + ", prenotation="
				+ prenotation + ", carnetInfo=" + carnetInfo + ", paymentData="
				+ paymentData + ", paymentTid=" + paymentTid
				+ ", invoiceRequired=" + invoiceRequired
				+ ", invoiceRequestSuccessful=" + invoiceRequestSuccessful
				+ ", brandCodeToRemove=" + brandCodeToRemove + ", i18n=" + i18n
				+ ", ipAddress=" + ipAddress
				+ "]";
	}
	
	

}