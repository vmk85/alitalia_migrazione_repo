package com.alitalia.aem.business.web.utils;

import java.util.Calendar;

public class SearchNewsData {
	
	private Calendar dateFrom;
	private Calendar dateTo;
	private String category;
	private String title;
	
	public Calendar getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Calendar dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Calendar getDateTo() {
		return dateTo;
	}

	public void setDateTo(Calendar dateTo) {
		this.dateTo = dateTo;
	}

	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "NewsSearchData [dateFrom=" + dateFrom + ", dateTo=" + dateTo
				+ ", category=" + category + ", title=" + title + "]";
	}
}
