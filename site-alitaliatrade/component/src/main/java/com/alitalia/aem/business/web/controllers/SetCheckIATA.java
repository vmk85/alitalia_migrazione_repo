package com.alitalia.aem.business.web.controllers;

import java.util.Iterator;

import javax.jcr.Session;

import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;

public class SetCheckIATA extends WCMUse {
	
	private static Logger logger = LoggerFactory.getLogger(SetCheckIATA.class);
	
	@Override
    public void activate() throws Exception {
    	//FIXME la classe non è più utilizzata, la logica è stata gestina usando l'AlitaliaTradeAuthUtils
		logger.debug("Verifying user info...");
		
		boolean iata = false;
		boolean gruppi = false;
		boolean logged = false;
		String ruolo = null;
		String ragioneSociale = null;
		String user = "";
		String codiceAgenzia = "";
		String codiceAccordo = "";
		
		SlingHttpServletRequest request = getRequest();
		ResourceResolver resourceResolver = request.getResourceResolver();
		Session session = resourceResolver.adaptTo(Session.class);
		UserManager userManager = resourceResolver.adaptTo(UserManager.class);
		String userID = session.getUserID();
	    Authorizable auth = userManager.getAuthorizable(userID);
	    Iterator<Group> groups = auth.memberOf(); 
	    user = userID;
	    
	    logger.debug("Checking groups for user {}", user);
	    
	    boolean trovato=false;
	    while(groups.hasNext() && !trovato){
	    	String group = groups.next().getID();
	    	
	    	logger.debug("Found group {} for user {}", group, user);
			
			if (!logged) {
				logged = group.equals("alitaliatrade-users");
			}
			if (!iata) {
				iata = group.equals("alitaliatrade-users-iata");
			}
			if (!gruppi) {
				gruppi = group.equals("alitaliatrade-users-groupsmgmt");
			}
			trovato = (logged && iata && gruppi);
	    }
	    
	    if (auth.getProperty("ragioneSociale") != null 
	    		&& auth.getProperty("ragioneSociale").length > 0) {
	    	ragioneSociale = auth.getProperty("ragioneSociale")[0].getString();
	    }
	    
	    if (auth.getProperty("ruolo") != null 
	    		&& auth.getProperty("ruolo").length > 0) {
	    	ruolo = auth.getProperty("ruolo")[0].getString();
	    }
	    
	    if (auth.getProperty("codiceAgenzia") != null 
	    		&& auth.getProperty("codiceAgenzia").length > 0) {
	    	codiceAgenzia = auth.getProperty("codiceAgenzia")[0].getString();
	    }
	    
	    if (auth.getProperty("codiceAccordo") != null 
	    		&& auth.getProperty("codiceAccordo").length > 0) {
	    	codiceAccordo = auth.getProperty("codiceAccordo")[0].getString();
	    }
	    
	    /*request.setAttribute("isLogged", logged);
	    request.setAttribute("isIata", iata);
	    request.setAttribute("isGruppi", gruppi);
	    request.setAttribute("ragioneSociale", ragioneSociale);
	    request.setAttribute("ruolo", ruolo);
	    request.setAttribute("user", user);*/
	    //request.setAttribute("codiceAgenzia", codiceAgenzia);
	    //request.setAttribute("codiceAccordo", codiceAccordo);
	    
	    logger.debug("User {} - isLogged attribute set to {}", user, logged);
	    logger.debug("User {} - isIata attribute set to {}", user, iata);
	    logger.debug("User {} - isGruppi attribute set to {}", user, gruppi);
	}
	
}
