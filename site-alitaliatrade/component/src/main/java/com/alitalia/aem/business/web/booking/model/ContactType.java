package com.alitalia.aem.business.web.booking.model;

public enum ContactType {
	MOBILE,
	HOME,
	OFFICE,
	HOTEL
}
