package com.alitalia.aem.business.web.utils;

import java.util.HashMap;
import java.util.Map;

public class StsErrorMap {
	
	private static StsErrorMap stsErrorMap;
	private static Map<String, String> errorMap;

	private StsErrorMap(){
		errorMap = new HashMap<>();
		errorMap.put("21", Constants.MESSAGE_ERROR_INSUFFICIENT_PLAFOND);
		errorMap.put("22", Constants.MESSAGE_ERROR_STS_GENERIC);
		errorMap.put("25", Constants.MESSAGE_ERROR_STS_GENERIC);
		errorMap.put("23", Constants.MESSAGE_ERROR_REJECTED_TRANSACTION);
		errorMap.put("24", Constants.MESSAGE_ERROR_REJECTED_TRANSACTION);
		errorMap.put("31", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("32", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("33", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("34", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("35", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("36", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("37", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("38", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("39", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("310", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("311", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("312", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("313", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("314", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("315", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("316", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("317", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("318", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("319", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("320", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("321", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("322", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("323", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("324", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("325", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("326", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("327", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("328", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("329", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("330", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("331", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("340", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("341", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("370", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("371", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("372", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("395", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("396", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("399", Constants.MESSAGE_ERROR_WRONG_DATA);
		errorMap.put("390", Constants.MESSAGE_ERROR_STS_GENERIC);
		errorMap.put("392", Constants.MESSAGE_ERROR_STS_GENERIC);
		errorMap.put("503", Constants.MESSAGE_ERROR_STS_GENERIC);
		errorMap.put("391", Constants.MESSAGE_ERROR_AUTHORIZE); 
		errorMap.put("394", Constants.MESSAGE_ERROR_AUTHORIZE); 
		errorMap.put("393", Constants.MESSAGE_ERROR_FAILED_PAYMENT);
	}
	
	public static StsErrorMap getInstance(){
		if (stsErrorMap == null)
			stsErrorMap = new StsErrorMap();
		return stsErrorMap;
	}
	
	public Map<String, String> getErrorMap() {
		return errorMap;
	}
}