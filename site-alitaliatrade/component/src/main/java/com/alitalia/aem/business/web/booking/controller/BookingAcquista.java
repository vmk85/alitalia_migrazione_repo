package com.alitalia.aem.business.web.booking.controller;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.booking.model.InvoiceType;
import com.alitalia.aem.business.web.booking.render.GenericPriceRender;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.business.web.utils.StsErrorMap;
import com.alitalia.aem.common.data.home.enumerations.CreditCardTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PaymentTypeEnum;

public class BookingAcquista extends BookingSessionGenericController {

	private String grandTotal;
	private ArrayList<String> banks;
	private ArrayList<String> creditCards;
	private String billingTypeCF = InvoiceType.PF.toString();
	private String billingTypePiva = InvoiceType.PG.toString();
	private String message = "";
	private String redirectFailurePageCDC = "";
	private String redirectFailurePageBonifico = "";
	private String redirectFailureValidationPage = "";
	private String payment3DSPage = "";
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	
	private AlitaliaTradeConfigurationHolder configuration = null;
	
	@Override
	public void activate() throws Exception {

		super.activate();
		

		try {
			if (isIllegalCTXState(getResponse())){
				return;
			}
			configuration = getSlingScriptHelper().getService(AlitaliaTradeConfigurationHolder.class);
			redirectFailurePageCDC = configuration.getBookingConfirmPage() + "?showCallCenterMsg=true";
			redirectFailurePageBonifico = configuration.getBookingPaymentPage() + "?error=true";
			redirectFailureValidationPage = configuration.getBookingErrorPage();
			// set message on error
			if (getRequest().getParameter("error") != null && 
					!("").equals(getRequest().getParameter("error"))) {
				this.message = Constants.MESSAGE_ERROR_AUTHORIZE;
			}
			
			if (getRequest().getParameter("ec") != null && !("").equals(getRequest().getParameter("ec"))) {
				String stsErrorCode = (String) (getRequest().getParameter("ec"));
				logger.debug("Error Code STS: ["+stsErrorCode+"]");
				String errorMsg = StsErrorMap.getInstance().getErrorMap().get(stsErrorCode);
				if (Constants.MESSAGE_ERROR_FAILED_PAYMENT.equals(errorMsg)){
					String patternPH = "\\{PH_RES_CODE\\}";
			        Pattern pattern = Pattern.compile(patternPH);
			        Matcher matcher = pattern.matcher(errorMsg);
			        errorMsg = matcher.replaceAll(ctx.prenotation.getPnr());
				}
				logger.debug("Error Message STS: ["+errorMsg+"]");
				this.message = errorMsg != null ? errorMsg : Constants.MESSAGE_ERROR_AUTHORIZE;
			}
			
			// calculate grand total
			GenericPriceRender genericPriceRender = new GenericPriceRender(
					ctx.grossAmount, ctx.currency, ctx.locale);
			grandTotal = genericPriceRender.getFare();
			
			// retrieve list of available credit cards
			creditCards = populateCreditCards();
			
			// retrieve list of available banks
			banks = populateBanks();
			
			payment3DSPage = ctx.paymentData != null && ctx.paymentData.getHtmlData3ds()!=null ? 
					ctx.paymentData.getHtmlData3ds().replace("<html>", "").replace("</html>", "").replace("<body>", "").replace("</body>", "").replace("<head>", "").replace("</head>", "") : "";
		}

		// an error occurred...
		catch (Exception e) {
			logger.error("Unexpected error: ", e);
			redirectFailurePageCDC = configuration.getBookingConfirmPage() + "?showCallCenterMsg=true";
			redirectFailurePageBonifico = configuration.getBookingPaymentPage() + "?error=true";
		}
	}
	
	/**
	 * Populate an ArrayList of supported credit cards
	 * @return ArrayList<String> credit cards
	 */
	private ArrayList<String> populateCreditCards() {
		ArrayList<String> creditCards = new ArrayList<String>();
		creditCards.add(CreditCardTypeEnum.AMERICAN_EXPRESS.value());
		creditCards.add(CreditCardTypeEnum.MASTER_CARD.value());
		creditCards.add(CreditCardTypeEnum.VISA.value());
		return creditCards;
	}

	/**
	 * Populate a LinkedHashMap of supported banks
	 * @return LinkedHashMap<String, String> banks
	 */
	private ArrayList<String> populateBanks() {
		ArrayList<String> banks = new ArrayList<String>();
		//banks.add(PaymentTypeEnum.BANCA_INTESA.value());
		//banks.add(PaymentTypeEnum.UNICREDIT.value());
		banks.add(PaymentTypeEnum.GLOBAL_COLLECT.value());
//		banks.add(PaymentTypeEnum.STS.value());
		return banks;
	}
	
	public ArrayList<String> getBanks() {
		return banks;
	}

	public ArrayList<String> getCreditCards() {
		return creditCards;
	}

	public String getGrandTotal() {
		return grandTotal;
	}

	public String getBillingTypeCF() {
		return billingTypeCF;
	}

	public String getBillingTypePiva() {
		return billingTypePiva;
	}

	public String getMessage() {
		return message;
	}

	public String getRedirectFailurePageCDC() {
		return redirectFailurePageCDC;
	}
	
	public String getRedirectFailurePageBonifico() {
		return redirectFailurePageBonifico;
	}
	
	public String getRedirectFailureValidationPage() {
		return redirectFailureValidationPage;
	}

	public String getPayment3DSPage() {
		return payment3DSPage;
	}

}