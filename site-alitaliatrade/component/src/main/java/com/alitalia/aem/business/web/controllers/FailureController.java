package com.alitalia.aem.business.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;

public class FailureController extends WCMUse {

	private boolean vat;
	private boolean id;

	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
    public void activate() throws Exception {
		
		try{
			vat = false;
			id = false;
			String retriveError = getRequest().getParameter("retriveError");
			if (retriveError != null) {
				if (retriveError.equals("vat")) {
					vat = true;
				}
				if (retriveError.equals("id")) {
					id = true;
				}
			}
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
		}
		
	}
	
	public boolean isVat(){
		return vat;
	}
	
	public boolean isId(){
		return id;
	}
	

}
