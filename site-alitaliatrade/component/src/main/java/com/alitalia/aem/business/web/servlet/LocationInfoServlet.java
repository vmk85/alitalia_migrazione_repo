package com.alitalia.aem.business.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.CountryData;
import com.alitalia.aem.common.data.home.MunicipalityData;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsRequest;
import com.alitalia.aem.common.messages.home.RetrieveCouncilsResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;
import com.day.cq.commons.TidyJSONWriter;

@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "locationinfo" }),
		@Property(name = "sling.servlet.methods", value = { "GET" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class LocationInfoServlet extends SlingSafeMethodsServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@SuppressWarnings("unused")
	private ComponentContext componentContext;
	
	@Reference
	private StaticDataDelegate staticDataDelegate;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected void doGet(SlingHttpServletRequest request, 
			SlingHttpServletResponse response) throws ServletException, IOException {
		
		try {
			response.setContentType("application/json");
			List<CountryData> provincesList = null;
			List<MunicipalityData> councilsList = null;
			
			String type = request.getParameter("type");
			String filter = request.getParameter("filter");
			
			try {
				
				if (type != null && !type.isEmpty()) {
					
					if (type.equals("prov")) {
					
						RetrieveProvincesRequest serviceRequest = new RetrieveProvincesRequest(IDFactory.getTid(), IDFactory.getSid(request));
						serviceRequest.setLanguageCode("IT");
						serviceRequest.setMarket("IT");
						
						RetrieveProvincesResponse serviceResponse = staticDataDelegate.retrieveProvinces(serviceRequest);
						provincesList = serviceResponse.getProvinces();
						
					} else if (type.equals("city")) {
						
						RetrieveCouncilsRequest serviceRequest = new RetrieveCouncilsRequest(IDFactory.getTid(), IDFactory.getSid(request));
						serviceRequest.setLanguageCode("IT");
						serviceRequest.setMarket("IT");
						
						RetrieveCouncilsResponse serviceResponse = staticDataDelegate.retrieveCouncils(serviceRequest);
						councilsList = serviceResponse.getCouncils();
					}
				}
			
			
			} catch (Exception e) {
				logger.error("Retrieve Provinces error", e);
			}
	
			TidyJSONWriter out = new TidyJSONWriter(response.getWriter());
			out.object();
			
			if (provincesList != null && !provincesList.isEmpty()) {
				out.key("result");
				out.object();
				
				for (CountryData item : provincesList) {
					out.key(item.getStateCode()).value(item.getStateDescription());
				}
				
				out.endObject();
				
			} else if (councilsList != null && !councilsList.isEmpty() && filter != null && !filter.isEmpty()) {
				out.key("result");
				out.object();
				
				for (MunicipalityData item : councilsList) {
					if (item.getProvinceCode().equals(filter)) {
						out.key(item.getDescription()).value(item.getDescription());
					}
				}
				
				out.endObject();
				
			} else {
				out.key("result").value(false);
			}
			out.endObject();

		} catch (Exception e) {
			logger.error("Error LocationInfoServlet: ", e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
	
}
