package com.alitalia.aem.business.web.controllers;

import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;
import com.alitalia.aem.business.web.utils.AlitaliaTradeAuthUtils;
import com.alitalia.aem.common.data.home.enumerations.AlitaliaTradeUserType;

public class CheckIATA extends WCMUse {

	private Logger logger = LoggerFactory.getLogger(CheckIATA.class);
	
	private boolean logged;
	private boolean iata;
	private boolean gruppi;
	private String user;
	private String ragioneSociale;
	private String ruolo;
	private String linkGruppi;
	
	@Override
    public void activate() throws Exception {
		
		try {
			SlingScriptHelper helper = getSlingScriptHelper();
	    	AlitaliaTradeConfigurationHolder configuration = 
	    			helper.getService(AlitaliaTradeConfigurationHolder.class);
	    	
			SlingHttpServletRequest request = getRequest();
			logged = Boolean.parseBoolean(AlitaliaTradeAuthUtils.getProperty(request, "isLogged"));
			iata = Boolean.parseBoolean(AlitaliaTradeAuthUtils.getProperty(request, "isIata"));
			gruppi = Boolean.parseBoolean(AlitaliaTradeAuthUtils.getProperty(request, "isGruppi"));
			linkGruppi = configuration.getLinkGruppiPage();
			ragioneSociale = AlitaliaTradeAuthUtils.getProperty(request, "ragioneSociale");
			AlitaliaTradeUserType typeRuolo = AlitaliaTradeAuthUtils.getRuolo(request);
			if (typeRuolo != null) {
				ruolo = typeRuolo.value();
			} else {
				ruolo = null;
			}
			
			Authorizable auth = AlitaliaTradeAuthUtils.getAuthorizable(request);
			if (auth != null) {
				user = auth.getID();
			} else {
				user = null;
			}
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			throw e;
		}
	}
	
	public boolean isLogged() {
		return logged;
	}
	
	public boolean isIata() {
        return iata;
    }
	
	public boolean isGruppi() {
        return gruppi;
    }
	
	public String getUser() {
        return user;
    }

	public String getRagioneSociale() {
		return ragioneSociale;
	}

	public String getRuolo() {
		return ruolo;
	}

	public String getLinkGruppi() {
		return linkGruppi;
	}
	
}
