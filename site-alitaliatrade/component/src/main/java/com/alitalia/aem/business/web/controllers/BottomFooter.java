package com.alitalia.aem.business.web.controllers;

import java.util.Map;

import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUse;
import com.alitalia.aem.business.web.utils.MultifieldUtils;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;

public class BottomFooter extends WCMUse {

	@SuppressWarnings("rawtypes")
	private Map fieldList;
	private String infolegali;
	private String log;

	
	@Override
    public void activate() throws Exception {
    	
		ValueMap properties = getProperties();
		fieldList = MultifieldUtils.fromJcrMultifieldToMapArrayObject(getResource(), "items");
		infolegali = properties.get("infolegali", String.class);
		if(infolegali==null)
			infolegali="&copy;Copyright Alitalia 2019  Società Aerea Italiana S.p.A. in a.s. Tutti i diritti riservati.";
		
		InheritanceValueMap inVM = new HierarchyNodeInheritanceValueMap(getCurrentPage().getContentResource());
		log = inVM.getInherited("loggato", String.class);	
		
	}
	
	
	@SuppressWarnings("rawtypes")
	public Map getFieldList() {
        return fieldList;
    }
	
	public String getInfolegali(){
		return infolegali;
	}
	public String getLog(){
		return log;
	}
	
}
