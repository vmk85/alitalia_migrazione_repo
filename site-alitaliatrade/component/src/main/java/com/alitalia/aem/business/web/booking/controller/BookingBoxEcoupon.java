package com.alitalia.aem.business.web.booking.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.booking.BookingPhaseEnum;
import com.alitalia.aem.business.web.booking.BookingSearchKindEnum;
import com.alitalia.aem.business.web.booking.model.FlightSelection;
import com.alitalia.aem.business.web.utils.LocalizationUtils;
import com.alitalia.aem.common.data.home.ECouponData;
import com.day.cq.i18n.I18n;

public class BookingBoxEcoupon extends BookingSessionGenericController {
	
	private Logger logger = LoggerFactory.getLogger(getClass()); 
	
	private boolean couponHidden = false;
	
	private boolean couponNotYetAvailable = false;
	
	private boolean couponAvailable = false;
	
	private boolean couponSpecified = false;
	
	private ECouponData couponData = null;
	
	private String couponCode = "";
	
	private boolean couponInvalid = false;
	
	private String couponInvalidErrorMessage = null;

	private boolean insertedNow;
	
	private boolean showCCMessage;


	@Override
    public void activate() throws Exception {
		super.activate();
		
		try {
			Locale locale = LocalizationUtils.getLocaleByResource(getRequest().getResource(), "it");
			ResourceBundle resourceBundle = getRequest().getResourceBundle(locale);
			final I18n i18n = new I18n(resourceBundle);
			
			if (isIllegalCTXState(getResponse())){
				return;
			}
			
			couponHidden = false;
			couponNotYetAvailable = false;
			couponAvailable = false;
			couponSpecified = false;
			couponData = null;
			couponCode = "";
			couponInvalid = false;
			couponInvalidErrorMessage = null;
			String insertNow = getRequest().getParameter("insertNow");
			if (insertNow!=null) {
				insertedNow = Boolean.parseBoolean(insertNow);
			} else {
				insertedNow = false;
			}
			
			if (ctx.phase == BookingPhaseEnum.FLIGHTS_SEARCH
					|| ctx.phase == BookingPhaseEnum.FLIGHTS_SELECTION 
					|| ctx.phase == BookingPhaseEnum.PASSENGERS_DATA) {
				/*Temporaneamente blocchiamo ecoupon in presenza di piu' di un passeggero*/
				if((ctx.searchPassengersNumber != null 
						&&  (ctx.searchPassengersNumber.getNumChildren() 
								+ ctx.searchPassengersNumber.getNumInfants()
								+ ctx.searchPassengersNumber.getNumAdults() 
								+ ctx.searchPassengersNumber.getNumYoung()) > 1)){
					this.couponHidden = false;
					this.couponAvailable = false;
					this.couponNotYetAvailable = false;
					this.showCCMessage = true;
				}
				else if ((ctx.searchKind == BookingSearchKindEnum.SIMPLE ||
						ctx.searchKind == BookingSearchKindEnum.ROUNDTRIP)) {
					
					if (ctx.isECouponWithTariffaLight) {
						couponAvailable = true;
						couponInvalidErrorMessage = i18n.get("booking.trade.couponNotApplicableWithTariffaLight.message");
						couponInvalid = true;
					} else if (ctx.coupon != null) {
						
						couponSpecified = true;
						couponData = ctx.coupon;
						couponCode = ctx.coupon.getCode();
						couponInvalid = !ctx.isCouponValid;
						
						if (couponInvalid) {
							couponAvailable = true;
							
							String minNumPax = couponData.getAnyPaxMinNumPax();
							String maxNumPax = couponData.getAnyPaxMinNumPax();
							String minAmount = couponData.getMinAmount();
							String flightDateFrom = calendarToDateString(couponData.getDatVolFrm());
							String flightDateTo = calendarToDateString(couponData.getDatVolTo());
							String flightTimeFrom = calendarToTimeString(couponData.getDatVolFrm());
							String flightTimeTo = calendarToTimeString(couponData.getDatVolTo());
							String flightDateExclusion1From = calendarToDateString(couponData.getDatBlkFrm());
							String flightDateExclusion1To = calendarToDateString(couponData.getDatBlkTo());
							String flightDateExclusion2From = calendarToDateString(couponData.getDatBlkFrmDue());
							String flightDateExclusion2To = calendarToDateString(couponData.getDatBlkToDue());
							String flightDateExclusion3From = calendarToDateString(couponData.getDatBlkFrmTre());
							String flightDateExclusion3To = calendarToDateString(couponData.getDatBlkToTre());
							String flightWeekDays = couponData.getWeekTravellDay();
							String useDateFrom = calendarToDateString(couponData.getDatUseFrm());
							String useDateTo = calendarToDateString(couponData.getDatUseTo());
							String useWeekDays = couponData.getWeekTravellDay();
							
							int errorCode = couponData.getErrorCode().intValue();
							
							switch (errorCode) {
								
								case 2000:
									couponInvalidErrorMessage = "Errore codice ecoupon già usato";
									break;
								
								case 1100:
									couponInvalidErrorMessage = "Il codice inserito non è valido per la destinazione"
											+ " selezionata, ti preghiamo di verificare i voli inclusi nella promozione"
											+ " consultabile alla pagina dedicata";
									break;
								
								case 1500:
									couponInvalidErrorMessage = "l'e-coupon inserito è valido solo per: <ul>";
									if (notNullOrEmpty(flightDateFrom, flightDateTo)) {
										couponInvalidErrorMessage += "<li>viaggi effettuati tra il " + flightDateFrom + " e il " + flightDateTo;
										if (notNullOrEmpty(flightDateExclusion1From, flightDateExclusion1To)) {
											couponInvalidErrorMessage += ", escluso periodo dal " + flightDateExclusion1From 
													+ " al " + flightDateExclusion1To + " o ad un adulto con neonato";
										}
										
										if (notNullOrEmpty(flightDateExclusion2From, flightDateExclusion2To)) {
											couponInvalidErrorMessage += ", escluso periodo dal " + flightDateExclusion2From 
													+ " al " + flightDateExclusion2To + " o ad un adulto con neonato";
										}
										
										if (notNullOrEmpty(flightDateExclusion3From, flightDateExclusion3To)) {
											couponInvalidErrorMessage += ", escluso periodo dal " + flightDateExclusion3From 
													+ " al " + flightDateExclusion3To + " o ad un adulto con neonato";
										}
										couponInvalidErrorMessage += "</li>";
									}
									if (notNullOrEmpty(flightDateFrom, flightDateTo)) {
										couponInvalidErrorMessage += "<li>biglietti acquistati tra il " + useDateFrom + " e il " + useDateTo + "</li>";
									}
									// TODO [booking/coupon] verificare msg errore: <li>viaggio in {0}</li>
									// TODO [booking/coupon] verificare msg errore: <li>voli {1}</li><li>voli operati da alitalia e gruppo cai</li>{0}
									couponInvalidErrorMessage += "</ul>";
									couponInvalidErrorMessage += "Caratteristiche e limitazioni incluse nella promozione consultabile alla pagina dedicata.";
									break;
								
								case 1400:
									couponInvalidErrorMessage = "Errore codice ecoupon scaduto";
									break;
								
								case 1000:
									couponInvalidErrorMessage = "L'ecoupon inserito non è corretto, ti preghiamo di"
											+ " controllare il codice e riprovare";
									break;
								
								case 1300:
									couponInvalidErrorMessage = "Il codice inserito è valido per prenotazioni comprese tra "
											+ "un numero minimo " + minNumPax + " ed un numero massimo " + maxNumPax 
											+ " di passeggeri.";
									break;
								
								case 1200:
									couponInvalidErrorMessage = "Il codice inserito è valido per una selezione con importo "
											+ "complessivo superiore ai " + minAmount + " Euro";
									break;
								
								case 1600:
									couponInvalidErrorMessage = "Viaggi effettuati tra il " + flightDateFrom + " ed il " + flightDateTo + ". "
											+ "Valido solamente nei giorni della settimana: " + flightWeekDays + ". "
											+ "Voli compresi nella fascia oraria dalle ore " + flightTimeFrom + " alle ore " + flightTimeTo + ". "
											+ "Caratteristiche e limitazioni incluse nella promozione consultabile alla pagina dedicata.";
									break;
								
								case 1700:
									couponInvalidErrorMessage = "Biglietti acquistati tra il " + useDateFrom + " ed il " + useDateTo + ". "
											+ "Valido solamente nei giorni della settimana: " + useWeekDays + ". "
											+ "Caratteristiche e limitazioni incluse nella promozione consultabile alla pagina dedicata.";
									break;
								
								case 1800:
									couponInvalidErrorMessage = "Il codice inserito è valido per voli di sola andata";
									break;
									
								case 1900:
									couponInvalidErrorMessage = "Il codice inserito è valido per voli di andata e ritorno.";
									break;	
								
								default:
									logger.error("Unrecognized error code received for coupon {0}: {1}", errorCode, 
											couponData.getErrorDescription());
									couponInvalidErrorMessage = "Coupon non valido (" + errorCode + ").";
									break;
							}
						}
					} else {
						boolean flightSelectionsPerformed = true;
						for (FlightSelection selection : ctx.flightSelections) {
							if (selection == null) {
								flightSelectionsPerformed = false;
								break;
							}
						}
						if (flightSelectionsPerformed) {
							couponAvailable = true;
						} else {
							couponNotYetAvailable = true;
						}
					}
				} else {
					couponHidden = true;
				}
			} else {
				couponHidden = true;
			}
			
		} catch (Exception e) {
			logger.error("Unexpected error: " + e);
			throw e;
		}
	}
	
	public boolean getCouponHidden() {
		return couponHidden;
	}
	
	public boolean getCouponNotYetAvailable() {
		return couponNotYetAvailable;
	}

	public boolean getCouponAvailable() {
		return couponAvailable;
	}

	public boolean getCouponSpecified() {
		return couponSpecified;
	}
	
	public ECouponData getCouponData() {
		return couponData;
	}

	public boolean getCouponInvalid() {
		return couponInvalid;
	}

	public String getCouponInvalidErrorMessage() {
		return couponInvalidErrorMessage;
	}

	public String getCouponCode() {
		return couponCode;
	}
	
	public boolean getInsertedNow() {
		return insertedNow;
	}
	
	/* utility methods */
	
	private String calendarToDateString(Calendar source) {
		if (source == null || source.get(Calendar.YEAR) <= 1) {
			return "";
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(source.getTime());
		}
	}
	
	private String calendarToTimeString(Calendar source) {
		if (source == null || source.get(Calendar.YEAR) <= 1) {
			return "";
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
			return sdf.format(source.getTime());
		}
	}
	
	private boolean notNullOrEmpty(Object... arguments) {
		if (arguments != null) {
			for (Object object : arguments) {
				if (object == null || "".equals(object)) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean isShowCCMessage() {
		return showCCMessage;
	}
}