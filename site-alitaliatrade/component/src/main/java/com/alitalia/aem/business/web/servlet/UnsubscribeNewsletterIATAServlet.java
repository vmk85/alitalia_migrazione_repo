package com.alitalia.aem.business.web.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.messages.home.AgencyUnsubscribeNewsLetterIataRequest;
import com.alitalia.aem.common.messages.home.AgencyUnsubscribeNewsLetterIataResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.businesslogin.delegate.BusinessLoginDelegate;


@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "unsubscribenewsletteriata" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" })
})
@SuppressWarnings("serial")
public class UnsubscribeNewsletterIATAServlet extends SlingAllMethodsServlet {
	
	private String username;
	private String id;
	private ComponentContext componentContext;
	
	@Reference
	private BusinessLoginDelegate businessLoginDelegate;
	
	// logger
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected void doPost(SlingHttpServletRequest request, 
			SlingHttpServletResponse response) throws ServletException, IOException {

		username = request.getParameter("username");
		id = request.getParameter("id");
		logger.debug("UnsubscribeNewsletterIATAServlet Post read username: "+ username +" and id: "+ id);
        
		String error= "error";
		
		try
		{
			
			AgencyUnsubscribeNewsLetterIataRequest consentRequest = new AgencyUnsubscribeNewsLetterIataRequest();
			
			logger.debug("UnsubscribeNewsletterIATAServlet object tradeLoginClient ok");
			consentRequest.setTid(IDFactory.getTid());
			consentRequest.setSid(IDFactory.getSid(request));
			consentRequest.setIdAgenzia(id);
			AgencyUnsubscribeNewsLetterIataResponse consentResponse = businessLoginDelegate.unsubscribeNewsLetterIata(consentRequest);
			boolean result = consentResponse.isResult();
			error = ""+result;
			logger.debug("UnsubscribeNewsletterIATAServlet result: "+ result);
		}
		catch(Exception e)
		{
			logger.debug("UnsubscribeNewsletterIATAServlet errore: "+ e.getMessage());
		}
		
		response.setStatus(302);
        response.setHeader("Location", "newsletter_unsubscribe_completed.html?exit="+error);
        response.setHeader("Connection", "close");
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ComponentContext getComponentContext() {
		return componentContext;
	}

	public void setComponentContext(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
}
