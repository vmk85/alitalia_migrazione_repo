package com.alitalia.aem.business.web.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.data.home.BestPriceData;
import com.alitalia.aem.common.data.home.OfferData;
import com.alitalia.aem.common.data.home.enumerations.AreaValueEnum;
import com.alitalia.aem.common.data.home.enumerations.BestPriceTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.utils.AlitaliaCommonUtils;

public class ListOffersUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(ListOffersUtils.class);
	
	/**
	 * It filter the offers according to the following rules:
	 * <ul>
	 * <li>DOM Offers only OUTBOUND and the price type is DAY</li>
	 * <li>INZ/INTC Offers only ROUNDTRIP and the price type is MONTH</li>
	 * </ul>
	 * @param offerDataList
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<OfferData> filterOffers(List<OfferData> offersList) {
		
		List<OfferData> offerDataList = null;
		try {
			offerDataList = (List<OfferData>) AlitaliaCommonUtils.deepClone(offersList);
		} catch (IOException e) {
			logger.error("Error cloning object", e);
			return null;
		}
		List<OfferData> filteredOffers = new ArrayList<OfferData>();
		for (OfferData offerData : offerDataList) {
			if (offerData.getArea() == AreaValueEnum.DOM) {
				if (offerData.getRouteType() == RouteTypeEnum.OUTBOUND) {
					List<BestPriceData> newPrices = new ArrayList<BestPriceData>();
					for (BestPriceData bestPriceData : offerData.getBestPrices()) {
						if (bestPriceData.getType() == BestPriceTypeEnum.DAY) {
							newPrices.add(bestPriceData);
						}
					}
					if (!newPrices.isEmpty()) {
						offerData.setBestPrices(newPrices);
						filteredOffers.add(offerData);
					}
				}
			} else {
				if (offerData.getRouteType() == RouteTypeEnum.RETURN) {
					List<BestPriceData> newPrices = new ArrayList<BestPriceData>();
					for (BestPriceData bestPriceData : offerData.getBestPrices()) {
						if (bestPriceData.getType() == BestPriceTypeEnum.MONTH) {
							newPrices.add(bestPriceData);
						}
					}
					if (!newPrices.isEmpty()) {
						offerData.setBestPrices(newPrices);
						filteredOffers.add(offerData);
					}
				}
			}
		}
		return filteredOffers;
	}

}
