package com.alitalia.aem.business.web.flightinfo.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.business.web.flightinfo.model.DatiRicercaOrariVoli;
import com.alitalia.aem.business.web.utils.Constants;
import com.alitalia.aem.business.web.validation.ResultValidation;
import com.alitalia.aem.business.web.validation.Validator;
import com.alitalia.aem.business.web.validation.servlet.GenericFormValidatorServlet;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.FlightItinerayData;
import com.alitalia.aem.common.messages.home.RetrieveAirportsRequest;
import com.alitalia.aem.common.messages.home.RetrieveAirportsResponse;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesRequest;
import com.alitalia.aem.common.messages.home.RetrieveItinerariesResponse;
import com.alitalia.aem.common.utils.IDFactory;
import com.alitalia.aem.web.component.flightstatus.delegate.FlightStatusDelegate;
import com.alitalia.aem.web.component.staticdata.delegate.StaticDataDelegate;


@Component(metatype = true)
@Service(Servlet.class)
@Properties({
		@Property(name = "sling.servlet.selectors", value = { "flightinfolist" }),
		@Property(name = "sling.servlet.methods", value = { "POST" }),
		@Property(name = "sling.servlet.resourceTypes", value = { "cq/Page" }),
		@Property(name = "success.page", description = "Users will be redirected to the specified page after a successful outcome."),
		@Property(name = "failure.page", description = "Users will be redirected to the specified page after a failed outcome.")
})
@SuppressWarnings("serial")
public class FlightInfoListServlet extends GenericFormValidatorServlet {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private ComponentContext componentContext;
	
	@Reference
	private FlightStatusDelegate flightStatusDelegate;
	
	// Search Flight Delegate
	@Reference(policy = ReferencePolicy.DYNAMIC)
	private volatile StaticDataDelegate staticDataDelegate;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		this.componentContext = componentContext;
	}
	
	@Override
	protected ResultValidation validateForm(SlingHttpServletRequest request) {
		Validator validator = new Validator();
		
		// airport from
		validator.addDirectParameter("timefrom",
				request.getParameter("timefrom"),
				Constants.MESSAGE_AIRPORT_FROM_NOT_VALID, "isAirport");
		
		// airport to
		validator.addDirectParameter("timeto", request.getParameter("timeto"),
				Constants.MESSAGE_AIRPORT_TO_NOT_VALID, "isAirport");
		
		// date
		validator.addDirectParameter("infofrom",
				request.getParameter("infofrom"),
				Constants.MESSAGE_DATE_NOT_VALID, "isDate");
		validator.addDirectParameter("infofrom",
				request.getParameter("infofrom"), Constants.MESSAGE_DATE_PAST,
				"isFuture");
		
		// fascia oraria
		validator.addDirectParameter("fasciaoraria",
				request.getParameter("fasciaoraria"),
				Constants.MESSAGE_TIME_SLOT_NOT_VALID, "isTimeSlot");
		
		// data di partenza
		validator.addCrossParameter("timefrom",
				request.getParameter("timefrom"),
				request.getParameter("timeto"),
				Constants.MESSAGE_AIRPORT_TO_NOT_VALID, "areDifferent");
		
		return validator.validate();
	}
	
	@Override
	protected void performSubmit(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws IOException {
		
		try {
		
			DatiRicercaOrariVoli datiRicercaOrariVoli = buildDatiRicercaOrariVoli(request);
			HttpSession session = request.getSession();
			session.setAttribute("datiRicercaOrariVoli", datiRicercaOrariVoli);
			session.setAttribute("dataRicerca", request.getParameter("infofrom"));
	
			RetrieveItinerariesResponse serviceResponse = null;
			RetrieveItinerariesRequest serviceRequest = new RetrieveItinerariesRequest();
			
			FlightItinerayData flightItinerayData = new FlightItinerayData();
			flightItinerayData.setDeparture(request.getParameter("timefrom"));
			flightItinerayData.setArrival(request.getParameter("timeto"));
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
			df.setLenient(false);
			
			Date departureDate = df.parse(request.getParameter("infofrom"));
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(departureDate);
			flightItinerayData.setDepartureDate(calendar);
			flightItinerayData.setArrivalDate(calendar);//data arrivo deve essere uguale a data partenza
			
			serviceRequest.setFlight(flightItinerayData);
			
			serviceResponse = flightStatusDelegate.retrieveItineraries(serviceRequest);
			
			
			if (serviceResponse != null && serviceResponse.getItineraries() != null) {
				session.setAttribute("itinerariOrariVoli", serviceResponse.getItineraries());
				session.setAttribute("fasciaOraria", request.getParameter("fasciaoraria"));
				
			}
			
			String successPage = (String) componentContext.getProperties().get("success.page");
			if (successPage != null && !"".equals(successPage)) {
				response.sendRedirect(response.encodeRedirectURL(successPage));
			}
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			
			if (!response.isCommitted()) {
				String failurePage = (String) componentContext.getProperties().get("failure.page");
				if (failurePage != null && !"".equals(failurePage)) {
					response.sendRedirect(response.encodeRedirectURL(failurePage + ".html"));
				} else {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
			}
		}
		
	}
	
	private DatiRicercaOrariVoli buildDatiRicercaOrariVoli(SlingHttpServletRequest request){
		String codiceAereoportoPartenza = request.getParameter("timefrom");
		String codiceAereoportoArrivo = request.getParameter("timeto");
		String dataPartenza = request.getParameter("infofrom");
		String fasciaOraria = request.getParameter("fasciaoraria");
		String codiceStatoPartenza = "";
		String codiceStatoArrivo = "";
		
		// call service to retrieve airport county code
		RetrieveAirportsRequest req = new RetrieveAirportsRequest(IDFactory.getTid(),
				IDFactory.getSid(request));
		req.setMarket("IT");
		req.setLanguageCode("IT");
		RetrieveAirportsResponse res = staticDataDelegate.retrieveAirports(req);
		List<AirportData> list = res.getAirports();
		for (AirportData airportData : list) {
			if (airportData.getCode().equals(codiceAereoportoPartenza)) {
				codiceStatoPartenza = airportData.getCountryCode();				
			}
			if (airportData.getCode().equals(codiceAereoportoArrivo)) {
				codiceStatoArrivo = airportData.getCountryCode();				
			}
		}
		DatiRicercaOrariVoli datiVoli = new DatiRicercaOrariVoli(codiceAereoportoPartenza, codiceAereoportoArrivo, dataPartenza, fasciaOraria, codiceStatoPartenza, codiceStatoArrivo);
		return datiVoli;
	}
	
}
