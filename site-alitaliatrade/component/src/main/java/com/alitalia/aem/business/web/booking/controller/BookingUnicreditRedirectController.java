package com.alitalia.aem.business.web.booking.controller;

import com.alitalia.aem.business.AlitaliaTradeConfigurationHolder;

public class BookingUnicreditRedirectController extends BookingSessionGenericController {

	private String redirectFailurePage = "";
	
	private AlitaliaTradeConfigurationHolder configuration = null;
	
	@Override
	public void activate() throws Exception {

		super.activate();

		try {
			if (isIllegalCTXState(getResponse())){
				return;
			}
			configuration = getSlingScriptHelper().getService(AlitaliaTradeConfigurationHolder.class);
			redirectFailurePage = configuration.getBookingUnicreditRedirectFailurePage();
			
		} catch (Exception e) {
			logger.error("Unexpected error: ", e);
			redirectFailurePage = configuration.getBookingErrorPage();
		}
	}
	
	public String getRedirectFailurePage(){
		return redirectFailurePage;
	}
	
}