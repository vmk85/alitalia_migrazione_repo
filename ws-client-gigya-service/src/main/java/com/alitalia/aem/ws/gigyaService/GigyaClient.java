/** Client connessione servizi HTTPS Gigya
 * [D.V.] */

package com.alitalia.aem.ws.gigyaService;

import com.gigya.socialize.GSObject;
import com.gigya.socialize.GSRequest;
import com.gigya.socialize.GSResponse;

import org.apache.felix.scr.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;


import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.security.KeyStore;

import java.util.Map;

@Service(value = GigyaClient.class)
@Component(immediate = true, metatype = false)
@Properties({
        @Property(name = "service.vendor", value = "Reply")
})
public class GigyaClient {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Property(description = "Property used to set identity store path.")
    private static final String WS_IDENTITY_STORE_PATH_PROPERTY = "ws.identity_store_path";

    @Property(description = "Property used to set identity store password.")
    private static final String WS_IDENTITY_STORE_PASSWORD_PROPERTY = "ws.identity_store_password";

    @Property(description = "Property used to set identity key password.")
    private static final String WS_IDENTITY_KEY_PASSWORD_PROPERTY = "ws.identity_key_password";

    @Property(description = "Property used to set trust store path.")
    private static final String WS_TRUST_STORE_PATH_PROPERTY = "ws.trust_store_path";

    @Property(description = "Property used to set trust store password.")
    private static final String WS_TRUST_STORE_PASSWORD_PROPERTY = "ws.trust_store_password";

    @Property(description = "Property used to set request timout")
    private static final String WS_SSL_PROTOCL = "ws.ssl_protocol";

    @Property(description = "Property used to set request timout")
    private static final String WS_ENDPOINT = "ws.endpoint";

    @Property(description = "Property used to set request timout")
    private static final String WS_APIKEY = "ws.apiKey";

    @Property(description = "Property used to set request timout")
    private static final String WS_SECRETKEY = "ws.secretKey";

    @Property(description = "Property used to set request timout")
    private static final String WS_USERKEY = "ws.userKey";

    private String apiKey;
    private String secretKey;
    private String userKey;
    private String identityStorePath;
    private String identityStorePassword;
    private String identityKeyPassword;
    private String trustStorePath;
    private String trustStorePassword;
    private String sslProtocol;
    private String endpoint;

    private boolean initRequired = true;

    @Activate
    protected void activate(final Map<String, Object> config) {
        String identityStorePath = null;
        String identityStorePassword = null;
        String identityKeyPassword = null;
        String trustStorePath = null;
        String trustStorePassword = null;
        String sslProtocol = null;
        String endpoint = null;
        String apiKey;
        String secretKey;
        String userKey;

        try {

            identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
            this.identityStorePath = identityStorePath;

            identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
            this.identityStorePassword = identityStorePassword;

            identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
            this.identityKeyPassword = identityKeyPassword;

            trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
            this.trustStorePath = trustStorePath;

            trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
            this.trustStorePassword = trustStorePassword;

            sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCL));
            this.sslProtocol = sslProtocol;

            endpoint = String.valueOf(config.get(WS_ENDPOINT));
            this.endpoint = endpoint;

            apiKey = String.valueOf(config.get(WS_APIKEY));
            this.apiKey = apiKey;

            secretKey = String.valueOf(config.get(WS_SECRETKEY));
            this.secretKey = secretKey;

            userKey = String.valueOf(config.get(WS_USERKEY));
            this.userKey = userKey;

        } catch (Exception e) {
            logger.error("Error to read custom configuration to activate StaticDataServiceClient - default configuration used", e);
        }

        try {
            init();
            this.initRequired = false;
        } catch (Exception e) {
            this.initRequired = true;
            logger.error("Error initializing GigyaClient", e);
        }
    }

    @Modified
    protected void modified(final Map<String, Object> config) {
        String identityStorePath = null;
        String identityStorePassword = null;
        String identityKeyPassword = null;
        String trustStorePath = null;
        String trustStorePassword = null;
        String sslProtocol = null;
        String endpoint = null;
        String apiKey;
        String secretKey;
        String userKey;

        try {

            identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
            this.identityStorePath = identityStorePath;

            identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
            this.identityStorePassword = identityStorePassword;

            identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
            this.identityKeyPassword = identityKeyPassword;

            trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
            this.trustStorePath = trustStorePath;

            trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
            this.trustStorePassword = trustStorePassword;

            sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCL));
            this.sslProtocol = sslProtocol;

            endpoint = String.valueOf(config.get(WS_ENDPOINT));
            this.endpoint = endpoint;

            apiKey = String.valueOf(config.get(WS_APIKEY));
            this.apiKey = apiKey;

            secretKey = String.valueOf(config.get(WS_SECRETKEY));
            this.secretKey = secretKey;

            userKey = String.valueOf(config.get(WS_USERKEY));
            this.userKey = userKey;

        } catch (Exception e) {
            logger.error("Error to read custom configuration to activate StaticDataServiceClient - default configuration used", e);
        }

        try {
            init();
            this.initRequired = false;
        } catch (Exception e) {
            this.initRequired = true;
            logger.error("Error initializing GigyaClient", e);
        }
    }

    private void init(){

        FileInputStream fisTruststore,fisIdentitystore = null;

        try {

            logger.info("gigyaService; trustStorePath : " + trustStorePath + " trustStorePassword : " + trustStorePassword + " identityStorePath : " + identityStorePath + " identityStorePassword : " + identityStorePassword + " identityKeyPassword : " + identityKeyPassword + " sslProtocol : " + sslProtocol + " TrustManagerFactory.getDefaultAlgorithm() : " + TrustManagerFactory.getDefaultAlgorithm());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            KeyStore trustKeystore = KeyStore.getInstance("jks");
            fisTruststore = new FileInputStream(trustStorePath);
            trustKeystore.load(fisTruststore, trustStorePassword.toCharArray());
            tmf.init(trustKeystore);

            KeyStore identityKeystore = KeyStore.getInstance("jks");
            fisIdentitystore = new FileInputStream(identityStorePath);
            identityKeystore.load(fisIdentitystore, identityStorePassword.toCharArray());

            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(identityKeystore, identityKeyPassword.toCharArray());


            SSLContext sc = SSLContext.getInstance(sslProtocol);
            sc.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            logger.error("[GigyaClient.class] errore inizzializzazione certificato chiamata a Gigya" + e.getMessage());
        }

    }

    public GSResponse execute(String method, GSObject gsObject){

        if(this.initRequired){
            init();
        }

        GSRequest req = new GSRequest(apiKey, secretKey, method, gsObject, true, userKey);
        req.setAPIDomain(endpoint);
        GSResponse resp = req.send();

        return resp;
    }

    public String getIdentityStorePath() {
        return identityStorePath;
    }

    public String getIdentityStorePassword() {
        return identityStorePassword;
    }

    public String getIdentityKeyPassword() {
        return identityKeyPassword;
    }

    public String getTrustStorePath() {
        return trustStorePath;
    }

    public String getTrustStorePassword() {
        return trustStorePassword;
    }

    public String getSslProtocol() {
        return sslProtocol;
    }

}
