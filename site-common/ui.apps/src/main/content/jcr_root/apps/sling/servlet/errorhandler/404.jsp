<%@ page session="false"%>
<%@ page import="com.alitalia.aem.business.web.utils.LocalizationUtils,com.alitalia.aem.consumer.utils.AlitaliaUtils,com.alitalia.aem.business.AlitaliaTradeConfigurationHolder,com.alitalia.aem.consumer.AlitaliaConfigurationHolder"%>
<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<%@include file="/libs/foundation/global.jsp"%>
<%
	String url = "";
	try{
		String contextPath = slingRequest.getRequestPathInfo().getResourcePath();
		if (contextPath.contains("alitaliatrade")) {
			AlitaliaTradeConfigurationHolder configuration = 
					sling.getService(AlitaliaTradeConfigurationHolder.class);
			String language = LocalizationUtils.getUserMarketLanguage(sling, slingRequest).get("language");
			if(language.equalsIgnoreCase("IT")){
				url = configuration.getErrorNavigationPage_IT(); // "/content/alitaliatrade/it/errore-navigazione.html"
			}
			if(language.equalsIgnoreCase("EN")){
				url = configuration.getErrorNavigationPage_EN(); // "/content/alitaliatrade/en/errore-navigazione.html"
			}
		} else if (contextPath.contains("alitalia")){
			AlitaliaConfigurationHolder configuration =
					sling.getService(AlitaliaConfigurationHolder.class);
			url = AlitaliaUtils.findSiteBaseRepositoryPath(slingRequest.getResource()) +
					configuration.getErrorNavigationPage();
		}
		
		slingRequest.getRequestDispatcher(url).include(slingRequest, response);
	} catch(Exception e){
// 		out.println(e);
	}
%>