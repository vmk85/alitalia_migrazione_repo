<%@page session="false"%>
<%-- copia modificata per usare HTTPS nell'accesso alla client context 
	 (supporto secure cookie) ed in modalita' asincrona --%>
<%@ page import="com.day.cq.wcm.api.AuthoringUIMode, com.day.cq.wcm.api.WCMMode, com.alitalia.aem.consumer.AlitaliaConfigurationHolder" %><%!
%><%@include file="/libs/foundation/global.jsp"%><%

    if (WCMMode.fromRequest(request) == WCMMode.DISABLED) {
        %><cq:includeClientLib categories="personalization.kernel"/><%
    } else {
        /* temporary, see CQ5-29809 */
        AuthoringUIMode authoringUIMode = AuthoringUIMode.fromRequest(slingRequest);
        if (authoringUIMode != null && authoringUIMode != AuthoringUIMode.CLASSIC) {
            /* to avoid cq.widgets dependency */
            %><cq:includeClientLib categories="personalization.ui,personalization.kernel"/><%
        } else {
            /* include personalization editing widgets */
            %><cq:includeClientLib categories="cq.personalization"/><%
        }
    }

	String segmentsPath = currentStyle.get("segmentPath", "/etc/segmentation");
	String ccPath = currentStyle.get("path","/etc/clientcontext/default");
	String currentPath = currentPage != null ? currentPage.getPath() : "";
	String protocol = "http";

	AlitaliaConfigurationHolder alitaliaConfig = sling.getService(AlitaliaConfigurationHolder.class);
	if (alitaliaConfig != null && alitaliaConfig.getHttpsEnabled()) {
		protocol = "https";
	}

%><script type="text/javascript">
	function loadCQAnalytics(handler) {
		var baseUrl = "<%=protocol%>://" + location.hostname + (location.port ? ':' + location.port : '');
        CQ_Analytics.ClientContextMgr.PATH = baseUrl + "<%= xssAPI.encodeForJSString(ccPath) %>";
        CQ_Analytics.ClientContextMgr.loadConfig(null, true);
        var a=CQ.shared.HTTP.externalize(baseUrl + "<%= xssAPI.encodeForJSString(ccPath) %>" + "/content/jcr:content/stores.init.js");
        a=CQ.shared.HTTP.addParameter(a, "path", "<%= xssAPI.encodeForJSString(currentPath) %>");
        a=CQ.shared.HTTP.noCaching(a);
        var c=CQ.shared.HTTP.get(a, handler || function() { });
        <%
            if (WCMMode.fromRequest(request) != WCMMode.DISABLED) {
                %>CQ_Analytics.ClientContextUtils.initUI("<%= xssAPI.encodeForJSString(ccPath) %>","<%= xssAPI.encodeForJSString(currentPath) %>", <%= !AuthoringUIMode.TOUCH.equals(AuthoringUIMode.fromRequest(slingRequest)) %>);<%
            }
        %>
	}

    $CQ(function() {
    	loadCQAnalytics();
    });
</script>