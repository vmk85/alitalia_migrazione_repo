package com.alitalia.aem.ws.b2bv2;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.namespace.QName;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.ws.b2bv2.wsdl.B2BV2;
import com.alitalia.aem.ws.b2bv2.wsdl.IB2BV2;
import com.alitalia.aem.ws.b2bv2.xsd.GetAnagraficaAgenziaResponse;
import com.alitalia.aem.ws.b2bv2.xsd.LoginB2BResponse;
import com.alitalia.aem.ws.b2bv2.xsd.RetrievePasswordByPartitaIvaResponse;
import com.alitalia.aem.ws.b2bv2.xsd.RetrievePasswordResponse;
import com.alitalia.aem.ws.common.exception.WSClientException;
import com.alitalia.aem.ws.common.logginghandler.SOAPLoggingHandler;
import com.alitalia.aem.ws.common.messagehandler.SOAPMessageHandler;
import com.alitalia.aem.ws.common.utils.WSClientConstants;

@Service(value = BusinessToBusinessServiceClient.class)
@Component(immediate = true, metatype = true)
@Properties({ @Property(name = "service.vendor", value = "Reply") })
public class BusinessToBusinessServiceClient {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private boolean initRequired = true;

	@Property(description = "URL of the 'Static Data Web Service WSDL' used to invoke the Web Service.")
	private static final String WS_ENDPOINT_PROPERTY = "ws.endpoint";

	@Property(description = "Flag used to invoke Web Service in HTTPS.")
	private static final String WS_HTTPS_MODE_PROPERTY = "ws.https_mode";

	@Property(description = "Flag used to load identity store.")
	private static final String WS_LOAD_IDENTITY_STORE_PROPERTY = "ws.load_identity_store";

	@Property(description = "Flag used to enable hostname verifier.")
	private static final String WS_HOSTNAME_VERIFIER_PROPERTY = "ws.hostname_verifier";

	@Property(description = "Property used to set identity store path.")
	private static final String WS_IDENTITY_STORE_PATH_PROPERTY = "ws.identity_store_path";

	@Property(description = "Property used to set identity store password.")
	private static final String WS_IDENTITY_STORE_PASSWORD_PROPERTY = "ws.identity_store_password";

	@Property(description = "Property used to set identity key password.")
	private static final String WS_IDENTITY_KEY_PASSWORD_PROPERTY = "ws.identity_key_password";

	@Property(description = "Property used to set trust store path.")
	private static final String WS_TRUST_STORE_PATH_PROPERTY = "ws.trust_store_path";

	@Property(description = "Property used to set trust store password.")
	private static final String WS_TRUST_STORE_PASSWORD_PROPERTY = "ws.trust_store_password";

	@Property(description = "Property used to set connection timout")
	private static final String WS_CONNECTION_TIMEOUT_PROPERTY = "ws.connection_timeout";

	@Property(description = "Property used to set request timout")
	private static final String WS_REQUEST_TIMEOUT_PROPERTY = "ws.request_timeout";

	@Property(description = "Property used to set request timout")
	private static final String WS_SSL_PROTOCL = "ws.ssl_protocol";

	@Property(description = "Property used to print the request")
	private static final String WS_PRINT_REQUET = "ws.print_request";
	
	@Property(description = "Property used to print the response")
	private static final String WS_PRINT_RESPONSE = "ws.print_response";
	
	private String serviceEndpoint;
	private String serviceQNameNamespace;
	private String serviceQNameLocalPart;
	private Integer connectionTimeout;
	private Integer requestTimeout;
	private String identityStorePath;
	private String identityStorePassword;
	private String identityKeyPassword;
	private String trustStorePath;
	private String trustStorePassword;
	private String sslProtocol;
	private boolean hostnameVerifier;
	private boolean loadIdentityStore;
	private boolean httpsMode;
	private boolean printRequest;
	private boolean printResponse;

	private B2BV2 b2BV2;
	private IB2BV2 iB2BV2;
	
	private static final String[] ELEMENT_TO_ANONIMIZE_PATTERN_LIST = {"(<.*Password>)(.*)(</.*Password>)"};

	private static final String[] ELEMENT_TO_ANONIMIZE_REPLACEMENT_LIST = {"$1********$3"};

	public BusinessToBusinessServiceClient() {
		this.serviceQNameLocalPart = "B2BV2";
		this.serviceQNameNamespace = "http://tempuri.org/";
		setDefault();
	}

	public BusinessToBusinessServiceClient(String serviceEndpoint) throws WSClientException {
		this.serviceQNameLocalPart = "B2BV2";
		this.serviceQNameNamespace = "http://tempuri.org/";
		setDefault();

		this.serviceEndpoint = serviceEndpoint;
		
		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("Error initializing BusinessToBusinessServiceClient", e);
		}
	}

	public BusinessToBusinessServiceClient(String serviceEndpoint, Integer connectionTimeout, Integer requestTimeout)
			throws WSClientException {
		this.serviceQNameLocalPart = "B2BV2";
		this.serviceQNameNamespace = "http://tempuri.org/";
		setDefault();

		this.serviceEndpoint = serviceEndpoint;
		this.connectionTimeout = connectionTimeout;
		this.requestTimeout = requestTimeout;
		
		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("Error initializing BusinessToBusinessServiceClient", e);
		}
	}

	public BusinessToBusinessServiceClient(String serviceEndpoint, Integer connectionTimeout, Integer requestTimeout,
			Boolean httpsMode, Boolean hostnameVerifier, Boolean loadIdentityStore, String identityStorePath,
			String identityStorePassword, String identityKeyPassword, String trustStorePath, String trustStorePassword, String sslProtocol) throws WSClientException {
		this.serviceEndpoint = serviceEndpoint;
		this.connectionTimeout = connectionTimeout;
		this.requestTimeout = requestTimeout;
		this.httpsMode = httpsMode;
		this.hostnameVerifier = hostnameVerifier;
		this.loadIdentityStore = loadIdentityStore;
		this.identityStorePath = identityStorePath;
		this.identityKeyPassword = identityKeyPassword;
		this.identityStorePassword = identityStorePassword;
		this.trustStorePath = trustStorePath;
		this.trustStorePassword = trustStorePassword;
		this.sslProtocol = sslProtocol;

		this.serviceQNameLocalPart = "B2BV2";
		this.serviceQNameNamespace = "http://tempuri.org/";
		
		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("Error initializing BusinessToBusinessServiceClient", e);
		}
	}

	public void init() throws WSClientException {
		FileInputStream fisTruststore = null;
		FileInputStream fisIdentitystore = null;
		try {
			URL classURL = BusinessToBusinessServiceClient.class.getClassLoader().getResource(this.serviceEndpoint);
			QName serviceQName = new QName(serviceQNameNamespace, serviceQNameLocalPart);

			b2BV2 = new B2BV2(classURL, serviceQName);
			iB2BV2 = b2BV2.getBasicHttpBindingIB2BV2();

			BindingProvider bindingProviderNetwork = (BindingProvider) iB2BV2;
			Binding bindingNetwork = bindingProviderNetwork.getBinding();

			@SuppressWarnings("rawtypes")
			List<Handler> handlerListNetwork = bindingNetwork.getHandlerChain();
			SOAPLoggingHandler soapLoggingHandlerNetwork = new SOAPLoggingHandler(this.printRequest, this.printResponse, true,
					new ArrayList<String>(Arrays.asList(ELEMENT_TO_ANONIMIZE_PATTERN_LIST)), 
					new ArrayList<String>(Arrays.asList(ELEMENT_TO_ANONIMIZE_REPLACEMENT_LIST))
					);
			SOAPMessageHandler soapMessageHandler = new SOAPMessageHandler(true);
			handlerListNetwork.add(soapLoggingHandlerNetwork);
			handlerListNetwork.add(soapMessageHandler);
			
			bindingNetwork.setHandlerChain(handlerListNetwork);

			logger.debug("Https mode [" + httpsMode + "]");

			if (httpsMode) {
				logger.debug("Loaded identity store path [" + identityStorePath + "], trust store path [" + trustStorePath + "], " 
					+ "hostname verifier [" + hostnameVerifier + "] and loading identity store flag [" + loadIdentityStore + "]");

				SSLContext sslContext = SSLContext.getInstance(sslProtocol);

				TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				KeyStore trustKeystore = KeyStore.getInstance(KeyStore.getDefaultType());
				fisTruststore = new FileInputStream(trustStorePath);
				trustKeystore.load(fisTruststore, trustStorePassword.toCharArray());
				tmf.init(trustKeystore);

				if (loadIdentityStore) {
					KeyStore identityKeystore = KeyStore.getInstance(WSClientConstants.keystoreTypeDefault);
					fisIdentitystore = new FileInputStream(identityStorePath);
					identityKeystore.load(fisIdentitystore, identityStorePassword.toCharArray());

					KeyManagerFactory kmf = KeyManagerFactory.getInstance(WSClientConstants.keymanagerFactoryAlgorithmDefault);
					kmf.init(identityKeystore, identityKeyPassword.toCharArray());
					sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

					logger.debug("Loaded identity and trust store");
				} else {
					sslContext.init(null, tmf.getTrustManagers(), null);
					logger.debug("Loaded trust store only");
				}

				SSLContext.setDefault(sslContext);
				bindingProviderNetwork.getRequestContext().put(WSClientConstants.JAXWS_SSL_SOCKET_FACTORY_PROPERTY,	sslContext.getSocketFactory());

				if (!hostnameVerifier) {
					bindingProviderNetwork.getRequestContext().put(WSClientConstants.JAXWS_HOSTNAME_VERIFIER_PROPERTY,
							new HostnameVerifier() {

								@Override
								public boolean verify(String arg0, SSLSession arg1) {
									return true;
								}

							});
				}
			}

			((BindingProvider) iB2BV2).getRequestContext().put(WSClientConstants.BINDING_PROVIDER_CONNECT_TIMEOUT_PROPERTY,
					connectionTimeout);
			((BindingProvider) iB2BV2).getRequestContext().put(WSClientConstants.BINDING_PROVIDER_REQUEST_TIMEOUT_PROPERTY,
					requestTimeout);

			logger.debug("Set up request timeout ["
					+ ((BindingProvider) iB2BV2).getRequestContext().get(WSClientConstants.BINDING_PROVIDER_REQUEST_TIMEOUT_PROPERTY)
					+ "] and " + "connection timeout ["
					+ ((BindingProvider) iB2BV2).getRequestContext().get(WSClientConstants.BINDING_PROVIDER_CONNECT_TIMEOUT_PROPERTY)
					+ "]");
		} catch (java.net.MalformedURLException e) {
			logger.error("Error to initialize BusinessToBusinessServiceClient, invalid endpoint = {}", this.serviceEndpoint);
			throw new WSClientException("Error to initialize BusinessToBusinessServiceClient", e);
		} catch (Exception e) {
			logger.error("Error to initialize B2BV2Client", e);
			throw new WSClientException("Error to initialize B2BV2Client", e);
		} finally {
			if (fisIdentitystore != null) {
				try {
					fisIdentitystore.close();
				} catch (IOException e) {
					logger.error("Error to close input stream", e);
				}
			}
			
			if (fisTruststore != null) {
				try {
					fisTruststore.close();
				} catch (IOException e) {
					logger.error("Error to close input stream", e);
				}
			}
			
		}
	}

	@Activate
	protected void activate(final Map<String, Object> config) {
		String serviceEndpoint = null;
		boolean httpsMode;
		boolean hostnameVerifier;
		boolean loadIdentityStore;
		boolean printRequest;
		boolean printResponse;
		String identityStorePath = null;
		String identityStorePassword = null;
		String identityKeyPassword = null;
		String trustStorePath = null;
		String trustStorePassword = null;
		Integer connectionTimeout = null;
		Integer requestTimeout = null;
		String sslProtocol = null;

		try {			
			httpsMode = Boolean.valueOf(String.valueOf(config.get(WS_HTTPS_MODE_PROPERTY)));
			this.httpsMode = httpsMode;
			
			hostnameVerifier = Boolean.valueOf(String.valueOf(config.get(WS_HOSTNAME_VERIFIER_PROPERTY)));
			this.hostnameVerifier = hostnameVerifier;
			
			loadIdentityStore = Boolean.valueOf(String.valueOf(config.get(WS_LOAD_IDENTITY_STORE_PROPERTY)));
			this.loadIdentityStore = loadIdentityStore;
			
			identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
			this.identityStorePath = identityStorePath;
			
			identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
			this.identityStorePassword = identityStorePassword;
			
			identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
			this.identityKeyPassword = identityKeyPassword;
			
			trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
			this.trustStorePath = trustStorePath;
			
			trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
			this.trustStorePassword = trustStorePassword;
			
			connectionTimeout = Integer.valueOf(String.valueOf(config.get(WS_CONNECTION_TIMEOUT_PROPERTY)));
			this.connectionTimeout = connectionTimeout;	
			
			requestTimeout = Integer.valueOf(String.valueOf(config.get(WS_REQUEST_TIMEOUT_PROPERTY)));
			this.requestTimeout = requestTimeout;	
			
			sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCL));
			this.sslProtocol = sslProtocol;	
			
			printRequest = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_REQUET)));
			this.printRequest = printRequest;	
			
			printResponse = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_RESPONSE)));
			this.printResponse = printResponse;	
		} catch (Exception e) {
			logger.error("Error to read custom configuration to activate BusinessToBusinessServiceClient - default configuration used", e);
			setDefault();
		}
		
		try {			
			serviceEndpoint = String.valueOf(config.get(WS_ENDPOINT_PROPERTY));
			this.serviceEndpoint = serviceEndpoint;
			
			logger.debug("Modified StaticDataServiceClient: "
					+ "WS_ENDPOINT_PROPERTY=[" + this.serviceEndpoint + "], "
					+ "WS_HTTPS_MODE_PROPERTY=[" + this.httpsMode + "], "
					+ "WS_HOSTNAME_VERIFIER_PROPERTY=[" + this.hostnameVerifier +"], "
					+ "WS_LOAD_IDENTITY_STORE_PROPERTY=[" + this.loadIdentityStore + "], "
					+ "WS_IDENTITY_STORE_PATH_PROPERTY=[" + this.identityStorePath + "], "
					+ "WS_TRUST_STORE_PATH_PROPERTY=[" + this.trustStorePath + "], "
					+ "WS_CONNECTION_TIMEOUT_PROPERTY=[" + this.connectionTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_SSL_PROTOCOL=[" + this.sslProtocol + "], "
					+ "WS_PRINT_RESPONSE=[" + this.printRequest + "], "
					+ "WS_PRINT_REQUET=[" + this.printResponse + "]");
		} catch (Exception e) {
			logger.error("Error to read WS_ENDPOINT_PROPERTY to activate BusinessToBusinessServiceClient", e);
			throw new WSClientException("Error to initialize BusinessToBusinessServiceClient", e);
		}
		
		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("Error initializing BusinessToBusinessServiceClient", e);
		}
	}

	@Modified
	protected void modified(final Map<String, Object> config) {
		String serviceEndpoint = null;
		boolean httpsMode;
		boolean hostnameVerifier;
		boolean loadIdentityStore;
		boolean printRequest;
		boolean printResponse;
		String identityStorePath = null;
		String identityStorePassword = null;
		String identityKeyPassword = null;
		String trustStorePath = null;
		String trustStorePassword = null;
		Integer connectionTimeout = null;
		Integer requestTimeout = null;
		String sslProtocol = null;

		try {			
			httpsMode = Boolean.valueOf(String.valueOf(config.get(WS_HTTPS_MODE_PROPERTY)));
			this.httpsMode = httpsMode;
			
			hostnameVerifier = Boolean.valueOf(String.valueOf(config.get(WS_HOSTNAME_VERIFIER_PROPERTY)));
			this.hostnameVerifier = hostnameVerifier;
			
			loadIdentityStore = Boolean.valueOf(String.valueOf(config.get(WS_LOAD_IDENTITY_STORE_PROPERTY)));
			this.loadIdentityStore = loadIdentityStore;
			
			identityStorePath = String.valueOf(config.get(WS_IDENTITY_STORE_PATH_PROPERTY));
			this.identityStorePath = identityStorePath;
			
			identityStorePassword = String.valueOf(config.get(WS_IDENTITY_STORE_PASSWORD_PROPERTY));
			this.identityStorePassword = identityStorePassword;
			
			identityKeyPassword = String.valueOf(config.get(WS_IDENTITY_KEY_PASSWORD_PROPERTY));
			this.identityKeyPassword = identityKeyPassword;
			
			trustStorePath = String.valueOf(config.get(WS_TRUST_STORE_PATH_PROPERTY));
			this.trustStorePath = trustStorePath;
			
			trustStorePassword = String.valueOf(config.get(WS_TRUST_STORE_PASSWORD_PROPERTY));
			this.trustStorePassword = trustStorePassword;
			
			connectionTimeout = Integer.valueOf(String.valueOf(config.get(WS_CONNECTION_TIMEOUT_PROPERTY)));
			this.connectionTimeout = connectionTimeout;	
			
			requestTimeout = Integer.valueOf(String.valueOf(config.get(WS_REQUEST_TIMEOUT_PROPERTY)));
			this.requestTimeout = requestTimeout;	
			
			sslProtocol = String.valueOf(config.get(WS_SSL_PROTOCL));
			this.sslProtocol = sslProtocol;	
			
			printRequest = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_REQUET)));
			this.printRequest = printRequest;	
			
			printResponse = Boolean.valueOf(String.valueOf(config.get(WS_PRINT_RESPONSE)));
			this.printResponse = printResponse;	
		} catch (Exception e) {
			logger.error("Error to read custom configuration to modify BusinessToBusinessServiceClient - default configuration used", e);
			setDefault();
		}
		
		try {			
			serviceEndpoint = String.valueOf(config.get(WS_ENDPOINT_PROPERTY));
			this.serviceEndpoint = serviceEndpoint;
			
			logger.info("Modified StaticDataServiceClient: "
					+ "WS_ENDPOINT_PROPERTY=[" + this.serviceEndpoint + "], "
					+ "WS_HTTPS_MODE_PROPERTY=[" + this.httpsMode + "], "
					+ "WS_HOSTNAME_VERIFIER_PROPERTY=[" + this.hostnameVerifier +"], "
					+ "WS_LOAD_IDENTITY_STORE_PROPERTY=[" + this.loadIdentityStore + "], "
					+ "WS_IDENTITY_STORE_PATH_PROPERTY=[" + this.identityStorePath + "], "
					+ "WS_TRUST_STORE_PATH_PROPERTY=[" + this.trustStorePath + "], "
					+ "WS_CONNECTION_TIMEOUT_PROPERTY=[" + this.connectionTimeout + "], "
					+ "WS_REQUEST_TIMEOUT_PROPERTY=[" + this.requestTimeout + "], "
					+ "WS_SSL_PROTOCOL=[" + this.sslProtocol + "], "
					+ "WS_PRINT_RESPONSE=[" + this.printRequest + "], "
					+ "WS_PRINT_REQUET=[" + this.printResponse + "]");
		} catch (Exception e) {
			logger.error("Error to read WS_ENDPOINT_PROPERTY to modify BusinessToBusinessServiceClient", e);
			throw new WSClientException("Error to initialize BusinessToBusinessServiceClient", e);
		}

		this.b2BV2 = null;
		
		try {
			init();
			this.initRequired = false;
		} catch (Exception e) {
			this.initRequired = true;
			logger.error("Error initializing BusinessToBusinessServiceClient", e);
		}
	}
	
	private void setDefault() {
		this.httpsMode = WSClientConstants.httpsModeDefault;
		this.hostnameVerifier = WSClientConstants.hostnameVerifierDefault;
		this.loadIdentityStore = WSClientConstants.loadIdentityStoreDefault;
		this.connectionTimeout = WSClientConstants.connectionTimeoutDefault;
		this.requestTimeout = WSClientConstants.requestTimeoutDefault;
		this.sslProtocol = WSClientConstants.sslProtocolDefault;
		this.printRequest = WSClientConstants.printRequest;
		this.printResponse = WSClientConstants.printResponse;
	}

	public boolean insertUserNonIataV2(String codiceAgenzia, String ragioneSociale, String partitaIVA,
			String codiceFiscale, String indirizzo, String citta, String provincia, String zip, String regione,
			String stato, String telefono, String fax, String codfamcon, String emailTitolare, String emailOperatore,
			String codiceAccordo, boolean flagSendMail, String codiceSirax, String dataValiditaAccordo) {
		if (this.initRequired){
			init();
			this.initRequired = false;
		}
		
		try {
			logger.info("Calling WS-B2BV2 Operation-insertUserNonIataV2...");
			long before = System.currentTimeMillis();
			boolean response = iB2BV2.insertUserNonIataV2(codiceAgenzia, ragioneSociale, partitaIVA, codiceFiscale, indirizzo, citta,
					provincia, zip, regione, stato, telefono, fax, codfamcon, emailTitolare, emailOperatore, codiceAccordo,
					flagSendMail, codiceSirax, dataValiditaAccordo);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-B2BV2 Operation-insertUserNonIataV2...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-B2BV2 Operation-insertUserNonIataV2", e);
		}
	}

	public LoginB2BResponse.LoginB2BResult loginB2B(String idRuolo, String codiceAgenzia, String password) {
		if (this.initRequired){
			init();
			this.initRequired = false;
		}

		try {
			logger.info("Calling WS-B2BV2 Operation-loginB2B...");
			long before = System.currentTimeMillis();
			LoginB2BResponse.LoginB2BResult response = iB2BV2.loginB2B(idRuolo, codiceAgenzia, password);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-B2BV2 Operation-loginB2B...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-B2BV2 Operation-loginB2B", e);
		}
	}

	public boolean changePassword(String idRuolo, String codiceAgenzia, String oldPassword, String newPassword) {
		if (this.initRequired){
			init();
			this.initRequired = false;
		}

		try {
			logger.info("Calling WS-B2BV2 Operation-changePassword...");
			long before = System.currentTimeMillis();
			boolean response = iB2BV2.changePassword(idRuolo, codiceAgenzia, oldPassword, newPassword);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-B2BV2 Operation-changePassword...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-B2BV2 Operation-changePassword", e);
		}
	}

	public RetrievePasswordResponse.RetrievePasswordResult retrievePassword(String idRuolo, String codiceAgenzia) {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}

		try {
			logger.info("Calling WS-B2BV2 Operation-retrievePassword...");
			long before = System.currentTimeMillis();
			RetrievePasswordResponse.RetrievePasswordResult response = iB2BV2.retrievePassword(idRuolo, codiceAgenzia);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-B2BV2 Operation-retrievePassword...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-B2BV2 Operation-retrievePassword", e);
		}
	}

	public RetrievePasswordByPartitaIvaResponse.RetrievePasswordByPartitaIvaResult retrievePasswordByPartitaIva(
			String idRuolo, String partitaIva) {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}

		try {
			logger.info("Calling WS-B2BV2 Operation-retrievePasswordByPartitaIva...");
			long before = System.currentTimeMillis();
			RetrievePasswordByPartitaIvaResponse.RetrievePasswordByPartitaIvaResult response = iB2BV2.retrievePasswordByPartitaIva(idRuolo, partitaIva);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-B2BV2 Operation-retrievePasswordByPartitaIva...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-B2BV2 Operation-retrievePasswordByPartitaIva", e);
		}
	}

	public GetAnagraficaAgenziaResponse.GetAnagraficaAgenziaResult retrieveAnagraficaAgenzia(String idAgenzia) {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}

		try {
			logger.info("Calling WS-B2BV2 Operation-retrieveAnagraficaAgenzia...");
			long before = System.currentTimeMillis();
			GetAnagraficaAgenziaResponse.GetAnagraficaAgenziaResult response = iB2BV2.getAnagraficaAgenzia(idAgenzia);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-B2BV2 Operation-retrieveAnagraficaAgenzia...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-B2BV2 Operation-retrieveAnagraficaAgenzia", e);
		}
	}
	
	public boolean insertConsensiAgenzia(String idAgenzia, boolean flagCondizioni, boolean flagDatiPersonali) {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}

		try {
			logger.info("Calling WS-B2BV2 Operation-insertConsensiAgenzia...");
			long before = System.currentTimeMillis();
			boolean response = iB2BV2.insertConsensiAgenzia(idAgenzia, flagCondizioni, flagDatiPersonali);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-B2BV2 Operation-insertConsensiAgenzia...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-B2BV2 Operation-insertConsensiAgenzia", e);
		}
	}
	
	
	public boolean insAgyEst(String idAgenzia) {
		if (this.initRequired) {
			init();
			this.initRequired = false;
		}

		try {
			logger.info("Calling WS-B2BV2 Operation-insAgyEst...");
			long before = System.currentTimeMillis();
			boolean response = iB2BV2.insAgyEst(idAgenzia);
			long after = System.currentTimeMillis();
			long execTime = after - before;

			logger.info("Calling WS-B2BV2 Operation-insAgyEst...done execution time=" + execTime);
			return response;
		} catch (Exception e) {
			throw new WSClientException("Exception while executing WS-B2BV2 Operation-insAgyEst", e);
		}
	}
	
	public Integer getConnectionTimeout() {
		return connectionTimeout;
	}

	public Integer getRequestTimeout() {
		return requestTimeout;
	}

	public String getIdentityStorePath() {
		return identityStorePath;
	}

	public String getIdentityStorePassword() {
		return identityStorePassword;
	}
	
	public String getIdentityKeyPassword() {
		return identityKeyPassword;
	}

	public String getTrustStorePath() {
		return trustStorePath;
	}

	public String getTrustStorePassword() {
		return trustStorePassword;
	}

	public boolean getHostnameVerifier() {
		return hostnameVerifier;
	}

	public boolean getLoadIdentityStore() {
		return loadIdentityStore;
	}

	public boolean getHttpsMode() {
		return httpsMode;
	}
	
	public String getSslProtocol() {
		return sslProtocol;
	}
	
	public boolean isPrintRequest() {
		return printRequest;
	}

	public boolean isPrintResponse() {
		return printResponse;
	}	
	
}
