import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.alitalia.aem.ws.b2bv2.BusinessToBusinessServiceClient;
import com.alitalia.aem.ws.b2bv2.xsd.GetAnagraficaAgenziaResponse;
import com.alitalia.aem.ws.common.messagehandler.RemoveInboundUnusedUnwantedNamespaces;

public class TestClientB2BV2LoginB2B {
	
	private BusinessToBusinessServiceClient tradeLoginClient;
//	private String serviceEndpoint = "http://172.31.32.24:9000/Wcf_B2BV2new/B2BV2.svc";
	private String serviceEndpoint = "wsdl4env/localdev-B2BV2.wsdl";
	private static final Logger log = LoggerFactory.getLogger(TestClientB2BV2LoginB2B.class);
	
	public TestClientB2BV2LoginB2B() throws Exception {
		try {
			tradeLoginClient = new BusinessToBusinessServiceClient(serviceEndpoint, 10000, 10000, false, false, false, "E:/progetti/PortaleAlitalia/certificati/identitystore/dev/identitystore.jks",
					"alikey", "alitalia.sai", "E:/progetti/PortaleAlitalia/certificati/truststore/dev/truststore.jks", "alitrust", "TLSv1");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() {
		try {
//			DatatypeFactory dtf = DatatypeFactory.newInstance();
//			com.alitalia.aem.ws.b2bv2.xsd.ObjectFactory factory = new com.alitalia.aem.ws.b2bv2.xsd.ObjectFactory();
			String idRuolo = "0";
			String codiceAgenzia = "160354";
			String password = "lavinia";

			GetAnagraficaAgenziaResponse.GetAnagraficaAgenziaResult response = tradeLoginClient.retrieveAnagraficaAgenzia(codiceAgenzia);
			GetAnagraficaAgenziaToAgencyRetrieveDataResponse getAnagraficaAgenziaConverter = new GetAnagraficaAgenziaToAgencyRetrieveDataResponse();
			AgencyRetrieveDataResponse resp = getAnagraficaAgenziaConverter.convert(response);
			System.out.println("Response unmarshalled: "+resp);
			
//			AgencyRetrieveDataResponse response = new AgencyRetrieveDataResponse(request.getTid(), request.getSid());
//			result = businessLoginClient.retrieveAnagraficaAgenzia(idAgenzia);
//			if (result != null)
//				response = getAnagraficaAgenziaConverter.convert(result);
//			JAXBContext loginContext = JAXBContext.newInstance(GetAnagraficaAgenziaResponse.class);
//			Unmarshaller loginUnmashaller = loginContext.createUnmarshaller();
//			GetAnagraficaAgenziaResponse unmarshalledObj = null;
//			Node responseElement = (Node) response.getAny();
//			unmarshalledObj = (GetAnagraficaAgenziaResponse) loginUnmashaller.unmarshal(responseElement);
//			AlitaliaTrade unmarshalledObj = (AlitaliaTrade) response.getAny();

			log.info("call ok: " + response);
		} catch (Exception e) {
			log.info("error");
			e.printStackTrace();
		}
	}
	
	public static void main(String arg []) throws Exception {
		log.info("calling..");
		TestClientB2BV2LoginB2B client = new TestClientB2BV2LoginB2B();
		client.execute();
		log.info("call completed");
	} 
}
