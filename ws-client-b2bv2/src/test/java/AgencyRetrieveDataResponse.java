

import java.util.Date;

public class AgencyRetrieveDataResponse extends BaseResponse {
	
	String codiceAgenzia;
	String ragioneSociale;
	String partitaIVA;
	String codiceFiscale;
	String indirizzo;
	String citta;
	String provincia;
	String zip;
	String regione;
	String stato;
	String telefono;
	String fax;
	String emailPubblica;
	String emailTitolare;
	String codFamCon;
	String emailAlitalia;
	Boolean abModuli;
	Boolean abReportVenduto;
	Boolean abAccordi;
	String codiceAccordo;
	String dataAccordo;
	String codiceSirax;
	Date dataValiditaAccordo;
	String cittaAccordo;
	Boolean abTariffaDedicata;
	
	public AgencyRetrieveDataResponse() {
		super();
	}
	
	public AgencyRetrieveDataResponse(String tid, String sid) {
		super(tid, sid);
	}
	
	public AgencyRetrieveDataResponse(String codiceAgenzia,
			String ragioneSociale, String partitaIVA, String codiceFiscale,
			String indirizzo, String citta, String provincia, String zip,
			String regione, String stato, String telefono, String fax,
			String emailPubblica, String emailTitolare, String codFamCon,
			String emailAlitalia, Boolean abModuli, Boolean abReportVenduto,
			Boolean abAccordi, String codiceAccordo, String dataAccordo,
			String codiceSirax, Date dataValiditaAccordo, String cittaAccordo,
			Boolean abTariffaDedicata) {
		super();
		this.codiceAgenzia = codiceAgenzia;
		this.ragioneSociale = ragioneSociale;
		this.partitaIVA = partitaIVA;
		this.codiceFiscale = codiceFiscale;
		this.indirizzo = indirizzo;
		this.citta = citta;
		this.provincia = provincia;
		this.zip = zip;
		this.regione = regione;
		this.stato = stato;
		this.telefono = telefono;
		this.fax = fax;
		this.emailPubblica = emailPubblica;
		this.emailTitolare = emailTitolare;
		this.codFamCon = codFamCon;
		this.emailAlitalia = emailAlitalia;
		this.abModuli = abModuli;
		this.abReportVenduto = abReportVenduto;
		this.abAccordi = abAccordi;
		this.codiceAccordo = codiceAccordo;
		this.dataAccordo = dataAccordo;
		this.codiceSirax = codiceSirax;
		this.dataValiditaAccordo = dataValiditaAccordo;
		this.cittaAccordo = cittaAccordo;
		this.abTariffaDedicata = abTariffaDedicata;
	}
	
	public AgencyRetrieveDataResponse(String tid, String sid, String codiceAgenzia,
			String ragioneSociale, String partitaIVA, String codiceFiscale,
			String indirizzo, String citta, String provincia, String zip,
			String regione, String stato, String telefono, String fax,
			String emailPubblica, String emailTitolare, String codFamCon,
			String emailAlitalia, Boolean abModuli, Boolean abReportVenduto,
			Boolean abAccordi, String codiceAccordo, String dataAccordo,
			String codiceSirax, Date dataValiditaAccordo, String cittaAccordo,
			Boolean abTariffaDedicata) {
		super(tid, sid);
		this.codiceAgenzia = codiceAgenzia;
		this.ragioneSociale = ragioneSociale;
		this.partitaIVA = partitaIVA;
		this.codiceFiscale = codiceFiscale;
		this.indirizzo = indirizzo;
		this.citta = citta;
		this.provincia = provincia;
		this.zip = zip;
		this.regione = regione;
		this.stato = stato;
		this.telefono = telefono;
		this.fax = fax;
		this.emailPubblica = emailPubblica;
		this.emailTitolare = emailTitolare;
		this.codFamCon = codFamCon;
		this.emailAlitalia = emailAlitalia;
		this.abModuli = abModuli;
		this.abReportVenduto = abReportVenduto;
		this.abAccordi = abAccordi;
		this.codiceAccordo = codiceAccordo;
		this.dataAccordo = dataAccordo;
		this.codiceSirax = codiceSirax;
		this.dataValiditaAccordo = dataValiditaAccordo;
		this.cittaAccordo = cittaAccordo;
		this.abTariffaDedicata = abTariffaDedicata;
	}

	public String getCodiceAgenzia() {
		return codiceAgenzia;
	}
	
	public void setCodiceAgenzia(String codiceAgenzia) {
		this.codiceAgenzia = codiceAgenzia;
	}
	
	public String getRagioneSociale() {
		return ragioneSociale;
	}
	
	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}
	
	public String getPartitaIVA() {
		return partitaIVA;
	}
	
	public void setPartitaIVA(String partitaIVA) {
		this.partitaIVA = partitaIVA;
	}
	
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	
	public String getIndirizzo() {
		return indirizzo;
	}
	
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	
	public String getCitta() {
		return citta;
	}
	
	public void setCitta(String citta) {
		this.citta = citta;
	}
	
	public String getProvincia() {
		return provincia;
	}
	
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
	public String getRegione() {
		return regione;
	}
	
	public void setRegione(String regione) {
		this.regione = regione;
	}
	
	public String getStato() {
		return stato;
	}
	
	public void setStato(String stato) {
		this.stato = stato;
	}
	
	public String getZip() {
		return zip;
	}
	
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public String getCodFamCom() {
		return codFamCon;
	}
	
	public void setCodFamCon(String codFamCon) {
		this.codFamCon = codFamCon;
	}
	
	public String getCodiceAccordo() {
		return codiceAccordo;
	}
	
	public void setCodiceAccordo(String codiceAccordo) {
		this.codiceAccordo = codiceAccordo;
	}
	
	public String getEmailAlitalia() {
		return emailAlitalia;
	}
	
	public void setEmailAlitalia(String emailAlitalia) {
		this.emailAlitalia = emailAlitalia;
	}
	
	public String getCodiceSirax() {
		return codiceSirax;
	}
	
	public void setCodiceSirax(String codiceSirax) {
		this.codiceSirax = codiceSirax;
	}
	
	public Date getDataValiditaAccordo() {
		return dataValiditaAccordo;
	}
	
	public void setDataValiditaAccordo(Date dataValiditaAccordo) {
		this.dataValiditaAccordo = dataValiditaAccordo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmailPubblica() {
		return emailPubblica;
	}

	public void setEmailPubblica(String emailPubblica) {
		this.emailPubblica = emailPubblica;
	}

	public String getEmailTitolare() {
		return emailTitolare;
	}

	public void setEmailTitolare(String emailTitolare) {
		this.emailTitolare = emailTitolare;
	}

	public Boolean getAbModuli() {
		return abModuli;
	}

	public void setAbModuli(Boolean abModuli) {
		this.abModuli = abModuli;
	}

	public Boolean getAbReportVenduto() {
		return abReportVenduto;
	}

	public void setAbReportVenduto(Boolean abReportVenduto) {
		this.abReportVenduto = abReportVenduto;
	}

	public Boolean getAbAccordi() {
		return abAccordi;
	}

	public void setAbAccordi(Boolean abAccordi) {
		this.abAccordi = abAccordi;
	}

	public String getDataAccordo() {
		return dataAccordo;
	}

	public void setDataAccordo(String dataAccordo) {
		this.dataAccordo = dataAccordo;
	}

	public String getCittaAccordo() {
		return cittaAccordo;
	}

	public void setCittaAccordo(String cittaAccordo) {
		this.cittaAccordo = cittaAccordo;
	}

	public Boolean getAbTariffaDedicata() {
		return abTariffaDedicata;
	}

	public void setAbTariffaDedicata(Boolean abTariffaDedicata) {
		this.abTariffaDedicata = abTariffaDedicata;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codiceAgenzia == null) ? 0 : codiceAgenzia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyRetrieveDataResponse other = (AgencyRetrieveDataResponse) obj;
		if (codiceAgenzia == null) {
			if (other.codiceAgenzia != null)
				return false;
		} else if (!codiceAgenzia.equals(other.codiceAgenzia))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AgencyRetrieveDataResponse [codiceAgenzia=" + codiceAgenzia
				+ ", ragioneSociale=" + ragioneSociale + ", partitaIVA="
				+ partitaIVA + ", codiceFiscale=" + codiceFiscale
				+ ", indirizzo=" + indirizzo + ", citta=" + citta
				+ ", provincia=" + provincia + ", zip=" + zip + ", regione="
				+ regione + ", stato=" + stato + ", telefono=" + telefono
				+ ", fax=" + fax + ", emailPubblica=" + emailPubblica
				+ ", emailTitolare=" + emailTitolare + ", codFamCon="
				+ codFamCon + ", emailAlitalia=" + emailAlitalia
				+ ", abModuli=" + abModuli + ", abReportVenduto="
				+ abReportVenduto + ", abAccordi=" + abAccordi
				+ ", codiceAccordo=" + codiceAccordo + ", dataAccordo="
				+ dataAccordo + ", codiceSirax=" + codiceSirax
				+ ", dataValiditaAccordo=" + dataValiditaAccordo
				+ ", cittaAccordo=" + cittaAccordo + ", abTariffaDedicata="
				+ abTariffaDedicata + "]";
	}
	
}
