package com.alitalia.aem.web.component.myalitalia.delegate;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDRequest;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDResponse;
import com.alitalia.aem.web.component.myalitalia.behaviour.GetDatiSicurezzaProfiloOLDBehaviour;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate=true, metatype=false)
@Service
public class SimpleMyAlitaliaDelegate implements MyAlitaliaDelegate {

    @Reference
    private GetDatiSicurezzaProfiloOLDBehaviour getDatiSicurezzaProfiloOLDBehaviour;
    @Override
    public GetDatiSicurezzaProfiloOLDResponse getDatiSicurezzaProfiloOLD(GetDatiSicurezzaProfiloOLDRequest request) {
        BehaviourExecutor<GetDatiSicurezzaProfiloOLDBehaviour, GetDatiSicurezzaProfiloOLDRequest, GetDatiSicurezzaProfiloOLDResponse> executor = new BehaviourExecutor<>();
        return executor.executeBehaviour(getDatiSicurezzaProfiloOLDBehaviour, request);
    }
}
