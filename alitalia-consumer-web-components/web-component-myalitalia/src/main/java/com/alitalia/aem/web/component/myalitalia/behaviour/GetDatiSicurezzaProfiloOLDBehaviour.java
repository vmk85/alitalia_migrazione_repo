package com.alitalia.aem.web.component.myalitalia.behaviour;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDRequest;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate=true, metatype=false)
@Service(value=GetDatiSicurezzaProfiloOLDBehaviour.class)
public class GetDatiSicurezzaProfiloOLDBehaviour extends Behaviour<GetDatiSicurezzaProfiloOLDRequest,GetDatiSicurezzaProfiloOLDResponse> {

    @Reference
    private MyPersonalAreaMilleMigliaService myPersonalAreaMilleMigliaService;

    @Override
    protected GetDatiSicurezzaProfiloOLDResponse executeOrchestration(GetDatiSicurezzaProfiloOLDRequest request) {
        if (request == null)
            throw new IllegalArgumentException("MmbAncillaryAddToCartRequest is null.");
        return myPersonalAreaMilleMigliaService.GetDatiSicurezzaProfiloOLD(request);
    }
}
