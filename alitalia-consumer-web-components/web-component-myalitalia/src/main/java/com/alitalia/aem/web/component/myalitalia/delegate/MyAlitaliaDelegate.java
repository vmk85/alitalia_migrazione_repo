package com.alitalia.aem.web.component.myalitalia.delegate;

import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDRequest;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloOLDResponse;

public interface MyAlitaliaDelegate {

    GetDatiSicurezzaProfiloOLDResponse getDatiSicurezzaProfiloOLD (GetDatiSicurezzaProfiloOLDRequest request);
}
