package com.alitalia.aem.web.component.crmdata.behaviourrest;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.crmdatarest.*;
import com.alitalia.aem.service.api.home.IServiceCRMData;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = SetCRMDataInfoBehaviour.class)
public class SetCRMDataInfoBehaviour extends Behaviour<SetCrmDataInfoRequest, SetCrmDataInfoResponse> {

    @Reference
    private IServiceCRMData serviceCRMData;

    @Override
    protected SetCrmDataInfoResponse executeOrchestration(SetCrmDataInfoRequest request) {
        if(request == null)
            throw new IllegalArgumentException("Set CRM Data request is null.");
        return serviceCRMData.setCRMDataInfo(request);
    }
}