package com.alitalia.aem.web.component.crmdata.delegaterest;

import com.alitalia.aem.common.messages.crmdatarest.*;

public interface ICRMDataDelegate {
    
    SetCRMDataNewsLetterResponse setCRMDataNewsLetter(SetCRMDataNewsLetterRequest request);

    SetCrmDataInfoResponse setAzCrmDataInfo(SetCrmDataInfoRequest request);

    GetCrmDataInfoResponse getAzCrmDataInfo(GetCrmDataInfoRequest request);
}