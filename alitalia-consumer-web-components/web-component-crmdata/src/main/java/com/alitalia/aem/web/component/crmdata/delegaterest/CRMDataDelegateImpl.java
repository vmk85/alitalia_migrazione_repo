package com.alitalia.aem.web.component.crmdata.delegaterest;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.crmdatarest.*;
import com.alitalia.aem.web.component.crmdata.behaviourrest.GetCRMDataInfoBehaviour;
import com.alitalia.aem.web.component.crmdata.behaviourrest.SetCRMDataInfoBehaviour;
import com.alitalia.aem.web.component.crmdata.behaviourrest.SetCRMDataNewsLetterBehaviour;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate=true, metatype=false)
@Properties({
	@Property( name = CRMDataDelegateImpl.CACHE_REFRESH_INTERVAL, label = "Refresh interval of the cache." ),
	@Property( name = CRMDataDelegateImpl.IS_CACHE_ENABLED, label = "Flag to enable cache." )
})
@Service
public class CRMDataDelegateImpl implements ICRMDataDelegate {
	
	@Reference
	private SetCRMDataNewsLetterBehaviour setCRMDataNewsLetterBehaviour;

	@Reference
	private GetCRMDataInfoBehaviour getCRMDataInfoBehaviour;


	@Reference
	private SetCRMDataInfoBehaviour setCRMDataInfoBehaviour;


	private static final Logger logger = LoggerFactory.getLogger(CRMDataDelegateImpl.class);
	
	public static final String CACHE_REFRESH_INTERVAL = "cache.refresh.interval";
	public static final String IS_CACHE_ENABLED = "cache.is_enabled";
	
	private long refreshInterval = 3600000L;
	private boolean isCacheEnabled = false;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		try {		
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify CRMDataDelegateImpl - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Activated CRMDataDelegateImpl: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		try {
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify CRMDataDelegateImpl - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Modified CRMDataDelegateImpl: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}
	


	@Override
	public SetCRMDataNewsLetterResponse setCRMDataNewsLetter(SetCRMDataNewsLetterRequest request) {
		SetCRMDataNewsLetterResponse response = null;
		BehaviourExecutor<SetCRMDataNewsLetterBehaviour, SetCRMDataNewsLetterRequest, SetCRMDataNewsLetterResponse> executor = new BehaviourExecutor<SetCRMDataNewsLetterBehaviour, SetCRMDataNewsLetterRequest, SetCRMDataNewsLetterResponse>();
		try {
			logger.debug("Executing delegate SetCRMDataNewsLetterResponse");
			response = executor.executeBehaviour(setCRMDataNewsLetterBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CRMDataDelegateImpl - SetCRMDataNewsLetterResponse] - Errore durante il set delle informazioni CRM.", e);
		}
		return response;
	}

	@Override
	public SetCrmDataInfoResponse setAzCrmDataInfo(SetCrmDataInfoRequest request) {
		SetCrmDataInfoResponse response = null;
		BehaviourExecutor<SetCRMDataInfoBehaviour, SetCrmDataInfoRequest, SetCrmDataInfoResponse> executor = new BehaviourExecutor<SetCRMDataInfoBehaviour, SetCrmDataInfoRequest, SetCrmDataInfoResponse>();
		try {
			logger.debug("Executing delegate SetCRMDataNewsLetterResponse");
			response = executor.executeBehaviour(setCRMDataInfoBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CRMDataDelegateImpl - SetCRMDataNewsLetterResponse] - Errore durante il set delle informazioni CRM.", e);
		}
		return response;
	}

    @Override
    public GetCrmDataInfoResponse getAzCrmDataInfo(GetCrmDataInfoRequest request) {
        GetCrmDataInfoResponse response = null;
        BehaviourExecutor<GetCRMDataInfoBehaviour, GetCrmDataInfoRequest, GetCrmDataInfoResponse> executor = new BehaviourExecutor<GetCRMDataInfoBehaviour, GetCrmDataInfoRequest, GetCrmDataInfoResponse>();
        try {
            logger.debug("Executing delegate SetCRMDataNewsLetterResponse");
            response = executor.executeBehaviour(getCRMDataInfoBehaviour, request);
        }
        catch (Exception e) {
            logger.error("[CRMDataDelegateImpl - SetCRMDataNewsLetterResponse] - Errore durante il set delle informazioni CRM.", e);
        }
        return response;
    }


}
