package com.alitalia.aem.web.component.egon.delegate;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.home.LocateRequest;
import com.alitalia.aem.common.messages.home.LocateResponse;
import com.alitalia.aem.web.component.egon.behaviour.LocateBehaviour;

@Component(immediate = true, metatype = false)
@Service(value = EgonDelegate.class)
public class SimpleEgonDelegate implements EgonDelegate {

	@Reference
	private LocateBehaviour locateBehaviour;
	
	@Override
	public LocateResponse locate(LocateRequest request) {
		BehaviourExecutor<LocateBehaviour, LocateRequest, LocateResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(locateBehaviour, request);
	}
}