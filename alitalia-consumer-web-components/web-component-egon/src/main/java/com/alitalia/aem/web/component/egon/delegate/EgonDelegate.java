package com.alitalia.aem.web.component.egon.delegate;

import com.alitalia.aem.common.messages.home.LocateRequest;
import com.alitalia.aem.common.messages.home.LocateResponse;

public interface EgonDelegate {

	LocateResponse locate(LocateRequest request);
}