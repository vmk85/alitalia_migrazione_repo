package com.alitalia.aem.web.component.bookingaward.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AwardValueRequest;
import com.alitalia.aem.common.messages.home.AwardValueResponse;
import com.alitalia.aem.service.api.home.BookingAwardSearchService;

@Component(immediate=true, metatype=false)
@Service(value=RetrieveAwardValueBehaviour.class)
public class RetrieveAwardValueBehaviour extends Behaviour<AwardValueRequest, AwardValueResponse> {

	@Reference
	private BookingAwardSearchService bookingAwardSearchService;

	@Override
	protected AwardValueResponse executeOrchestration(AwardValueRequest request) {
		if (request == null)
			throw new IllegalArgumentException("AwardValueRequest is null in RetrieveAwardValueBehaviour executeOrchestration.");
		return bookingAwardSearchService.retrieveAwardValue(request);
	}

}
