package com.alitalia.aem.web.component.bookingaward.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.SearchBookingSolutionResponse;
import com.alitalia.aem.common.messages.home.SearchFlightSolutionRequest;
import com.alitalia.aem.service.api.home.BookingAwardSearchService;

@Component(immediate=true, metatype=false)
@Service(value=RetrieveAwardSellupInformationBehaviour.class)
public class RetrieveAwardSellupInformationBehaviour extends Behaviour<SearchFlightSolutionRequest, SearchBookingSolutionResponse> {

	@Reference
	private BookingAwardSearchService bookingAwardSearchService;

	@Override
	protected SearchBookingSolutionResponse executeOrchestration(SearchFlightSolutionRequest request) {
		if (request == null)
			throw new IllegalArgumentException("SearchFlightSolutionRequest is null in RetrieveAwardSellupInformationBehaviour executeOrchestration.");
		return bookingAwardSearchService.executeAwardSellupSearch(request);
	}

}
