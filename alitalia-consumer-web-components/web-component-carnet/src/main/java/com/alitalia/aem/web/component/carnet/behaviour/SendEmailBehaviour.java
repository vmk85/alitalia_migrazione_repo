package com.alitalia.aem.web.component.carnet.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CarnetSendEmailRequest;
import com.alitalia.aem.common.messages.home.CarnetSendEmailResponse;
import com.alitalia.aem.service.api.home.CarnetPaymentService;

@Component(immediate = true, metatype = false)
@Service(value = SendEmailBehaviour.class)
public class SendEmailBehaviour extends Behaviour<CarnetSendEmailRequest, CarnetSendEmailResponse> {

	@Reference
	private CarnetPaymentService carnetPaymentService;
	
	@Override
	public CarnetSendEmailResponse executeOrchestration(CarnetSendEmailRequest request) {
		if (request == null)
			throw new IllegalArgumentException("CarnetSendEmailRequest is null.");
		return carnetPaymentService.sendEmail(request);
	}
}