package com.alitalia.aem.web.component.carnet.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeRequest;
import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeResponse;
import com.alitalia.aem.service.api.home.CarnetService;

@Component(immediate = true, metatype = false)
@Service(value = CarnetRecoveryCodeBehaviour.class)
public class CarnetRecoveryCodeBehaviour extends Behaviour<CarnetRecoveryCodeRequest, CarnetRecoveryCodeResponse> {

	@Reference
	private CarnetService carnetService;
	
	@Override
	public CarnetRecoveryCodeResponse executeOrchestration(CarnetRecoveryCodeRequest request) {
		if (request == null)
			throw new IllegalArgumentException("CarnetRecoveryCodeRequest is null.");
		return carnetService.recoveryCarnetCode(request);
	}
}