package com.alitalia.aem.web.component.carnet.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CarnetGetListRequest;
import com.alitalia.aem.common.messages.home.CarnetGetListResponse;
import com.alitalia.aem.service.api.home.CarnetService;

@Component(immediate = true, metatype = false)
@Service(value = CarnetGetListBehaviour.class)
public class CarnetGetListBehaviour extends Behaviour<CarnetGetListRequest, CarnetGetListResponse> {

	@Reference
	private CarnetService carnetService;
	
	@Override
	public CarnetGetListResponse executeOrchestration(CarnetGetListRequest request) {
		if (request == null)
			throw new IllegalArgumentException("CarnetGetListRequest is null.");
		return carnetService.getCarnetList(request);
	}
}