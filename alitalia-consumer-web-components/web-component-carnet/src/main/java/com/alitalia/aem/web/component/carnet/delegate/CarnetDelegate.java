package com.alitalia.aem.web.component.carnet.delegate;

import com.alitalia.aem.common.messages.home.CarnetBuyCarnetRequest;
import com.alitalia.aem.common.messages.home.CarnetBuyCarnetResponse;
import com.alitalia.aem.common.messages.home.CarnetCountryStaticDataResponse;
import com.alitalia.aem.common.messages.home.CarnetGetListRequest;
import com.alitalia.aem.common.messages.home.CarnetGetListResponse;
import com.alitalia.aem.common.messages.home.CarnetLoginRequest;
import com.alitalia.aem.common.messages.home.CarnetLoginResponse;
import com.alitalia.aem.common.messages.home.CarnetPaymentTypeResponse;
import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeRequest;
import com.alitalia.aem.common.messages.home.CarnetRecoveryCodeResponse;
import com.alitalia.aem.common.messages.home.CarnetSendEmailRequest;
import com.alitalia.aem.common.messages.home.CarnetSendEmailResponse;
import com.alitalia.aem.common.messages.home.CarnetStaticDataRequest;
import com.alitalia.aem.common.messages.home.CarnetUpdateRequest;
import com.alitalia.aem.common.messages.home.CarnetUpdateResponse;

public interface CarnetDelegate {
	
	//Payment Service
	CarnetBuyCarnetResponse buyCarnet(CarnetBuyCarnetRequest request);
	CarnetSendEmailResponse sendEmail(CarnetSendEmailRequest request);
	
	//Carnet Service
	CarnetGetListResponse getCarnetList(CarnetGetListRequest request);
	CarnetLoginResponse loginCarnet(CarnetLoginRequest request);
	CarnetRecoveryCodeResponse recoveryCarnetCode(CarnetRecoveryCodeRequest request);
	CarnetUpdateResponse updateCarnet(CarnetUpdateRequest request);
	
	//Static Data Service
	CarnetPaymentTypeResponse getPaymentList(CarnetStaticDataRequest request);
	CarnetCountryStaticDataResponse getCreditCardCountryList(CarnetStaticDataRequest request);
	CarnetCountryStaticDataResponse getStateList(CarnetStaticDataRequest request);
}