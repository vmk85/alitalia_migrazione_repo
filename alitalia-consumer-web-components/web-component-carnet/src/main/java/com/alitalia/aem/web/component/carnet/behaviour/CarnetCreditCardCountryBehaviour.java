package com.alitalia.aem.web.component.carnet.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CarnetCountryStaticDataResponse;
import com.alitalia.aem.common.messages.home.CarnetStaticDataRequest;
import com.alitalia.aem.service.api.home.CarnetStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = CarnetCreditCardCountryBehaviour.class)
public class CarnetCreditCardCountryBehaviour extends Behaviour<CarnetStaticDataRequest, CarnetCountryStaticDataResponse> {

	@Reference
	private CarnetStaticDataService carnetStaticDataService;
	
	@Override
	public CarnetCountryStaticDataResponse executeOrchestration(CarnetStaticDataRequest request) {
		if (request == null)
			throw new IllegalArgumentException("CarnetStaticDataRequest is null.");
		return carnetStaticDataService.getCreditCardCountryList(request);
	}
}