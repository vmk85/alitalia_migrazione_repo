package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MailinglistRegistrationRequest;
import com.alitalia.aem.common.messages.home.MailinglistRegistrationResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = MailinglistRegistrationBehaviour.class)
public class MailinglistRegistrationBehaviour extends Behaviour<MailinglistRegistrationRequest, MailinglistRegistrationResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;
	
	//@Reference
	//private ConsumerLoginDelegate consumerLoginDelegate;

	@Override
	public MailinglistRegistrationResponse executeOrchestration(MailinglistRegistrationRequest request) {
		if(request == null) throw new IllegalArgumentException("Request is null.");
		return  personalAreaMilleMigliaService.registerToMailinglist(request);
	}
}