package com.alitalia.aem.web.component.consumerlogin.behaviour;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.*;
//import com.alitalia.aem.common.messages.home.MillemigliaSendEmailRequest;
//import com.alitalia.aem.common.messages.home.MillemigliaSendEmailResponse;
//import com.alitalia.aem.common.messages.home.SubscribeSARequest;
//import com.alitalia.aem.common.messages.home.SubscribeSAResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = SubscribeSACheckEmailUserNameDuplicateBehaviour.class)
public class SubscribeSACheckEmailUserNameDuplicateBehaviour extends Behaviour<SubscribeSACheckEmailUserNameDuplicateRequest, SubscribeSACheckEmailUserNameDuplicateResponse> {
  
  @Reference
  private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

  @Override
  public SubscribeSACheckEmailUserNameDuplicateResponse executeOrchestration(SubscribeSACheckEmailUserNameDuplicateRequest request) {
    if(request == null) throw new IllegalArgumentException("Request is null.");

    SubscribeSACheckEmailUserNameDuplicateResponse response = personalAreaMilleMigliaService.CheckEmailUserNameDuplicate(request);

    return response;
  }
}