package com.alitalia.aem.web.component.consumerlogin.behaviour;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.common.messages.home.SubscribeSACheckEmailUserNameDuplicateResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

//import com.alitalia.aem.common.messages.home.MillemigliaSendEmailRequest;
//import com.alitalia.aem.common.messages.home.MillemigliaSendEmailResponse;
//import com.alitalia.aem.common.messages.home.SubscribeSARequest;
//import com.alitalia.aem.common.messages.home.SubscribeSAResponse;

@Component(immediate = true, metatype = false)
@Service(value = SubscribeSACheckUserNameDuplicateBehaviour.class)
public class SubscribeSACheckUserNameDuplicateBehaviour extends Behaviour<SubscribeSACheckUserNameDuplicateRequest, SubscribeSACheckUserNameDuplicateResponse> {
  
  @Reference
  private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

  @Override
  public SubscribeSACheckUserNameDuplicateResponse executeOrchestration(SubscribeSACheckUserNameDuplicateRequest request) {
    if(request == null) throw new IllegalArgumentException("Request is null.");

    SubscribeSACheckUserNameDuplicateResponse response = personalAreaMilleMigliaService.CheckUserNameDuplicate(request);

    return response;
  }
}