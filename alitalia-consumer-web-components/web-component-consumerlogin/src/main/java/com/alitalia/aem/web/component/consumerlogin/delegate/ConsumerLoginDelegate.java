package com.alitalia.aem.web.component.consumerlogin.delegate;

import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloResponse;

public interface ConsumerLoginDelegate {

	MilleMigliaLoginResponse milleMigliaLogin(MilleMigliaLoginRequest request);
	MilleMigliaLoginSAResponse milleMigliaLoginSA(MilleMigliaLoginSARequest request);
	SocialLoginResponse socialLogin(SocialLoginRequest request);
	SetSocialAccountResponse setSocialAccount(SetSocialAccountRequest request);
	ProfileResponse getProfile(ProfileRequest request);
	AddressesResponse getAddresses(AddressesRequest request);
	MessagesResponse getMessages(MessagesRequest request);
	BalanceResponse getBalance(BalanceRequest request);
	UpdateUserProfileResponse updateProfile(UpdateUserProfileRequest request);
	StatementByDateResponse getStatementByDate(StatementByDateRequest request);
	AddFlightActivityResponse addFlightActivity(AddFlightActivityRequest request);
	SocialAccountsResponse getSocialAccounts(SocialAccountsRequest getSocialAccountRequest);
	UpdatePinResponse updatePin(UpdatePinRequest request);
	SubscribeResponse subscribeByCustomer(SubscribeRequest request);
	UpdateNicknameResponse updateNickname(UpdateNicknameRequest request);
	RetrievePinResponse retrievePinByCustomerNumber(RetrievePinRequest request);
	RetrievePinResponse retrievePinByNickname(RetrievePinRequest request);
	MillemigliaSendEmailResponse sendNewRegistrationEmail(MillemigliaSendEmailRequest request);
	MillemigliaSendEmailResponse sendNewMailingListRegistrationMail(SendNewMailingListRegistrationMailRequest request);
	MailinglistRegistrationResponse registerToMailinglist(MailinglistRegistrationRequest request);
	DeletePanResponse deletePan(DeletePanRequest request);
	RemoveSocialAccountsResponse removeUidGigya(RemoveSocialAccountsRequest request);
	CreditCardsListResponse getCreditCardList(CreditCardsListRequest request);
	MMPaybackAccrualResponse paybackAccrual(MMPaybackAccrualRequest request);

	SubscribeSAResponse subscribeSA(SubscribeSARequest request);
	SubscribeSACheckEmailUserNameDuplicateResponse CheckEmailUserNameDuplicate (SubscribeSACheckEmailUserNameDuplicateRequest request);
	SubscribeSACheckUserNameDuplicateResponse CheckUserNameDuplicate (SubscribeSACheckUserNameDuplicateRequest request);

	SubscribeSASendMailResponse sendRetrieveMail(SubscribeSASendMailRequest request);

	com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloResponse SetDatiSicurezzaProfilo(com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloRequest request);
	GetDatiSicurezzaProfiloResponseLocal GetDatiSicurezzaProfilo(GetDatiSicurezzaProfiloRequestLocal request);

	FindProfileResponseLocal FindProfile(FindProfileRequestLocal request);


}