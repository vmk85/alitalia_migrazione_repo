package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MillemigliaSendEmailResponse;
import com.alitalia.aem.common.messages.home.SendNewMailingListRegistrationMailRequest;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SendMailRequest;

@Component(immediate = true, metatype = false)
@Service(value = SendNewMailingListRegistrationMailBehaviour.class)
public class SendNewMailingListRegistrationMailBehaviour extends Behaviour<SendNewMailingListRegistrationMailRequest, MillemigliaSendEmailResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public MillemigliaSendEmailResponse executeOrchestration(SendNewMailingListRegistrationMailRequest request) {
		if(request == null) throw new IllegalArgumentException("Request is null.");
		return personalAreaMilleMigliaService.sendNewMailingListRegistrationMail(request);
	}
}