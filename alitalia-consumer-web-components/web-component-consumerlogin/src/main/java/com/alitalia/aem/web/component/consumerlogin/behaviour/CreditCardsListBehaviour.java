package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CreditCardsListRequest;
import com.alitalia.aem.common.messages.home.CreditCardsListResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = CreditCardsListBehaviour.class)
public class CreditCardsListBehaviour extends Behaviour<CreditCardsListRequest, CreditCardsListResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public CreditCardsListResponse executeOrchestration(CreditCardsListRequest request) {
		if(request == null) throw new IllegalArgumentException("Request is null.");
		return personalAreaMilleMigliaService.getCreditCardsList(request);
	}
}