package com.alitalia.aem.web.component.consumerlogin.behaviour;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

//import com.alitalia.aem.common.messages.home.MillemigliaSendEmailRequest;
//import com.alitalia.aem.common.messages.home.MillemigliaSendEmailResponse;
//import com.alitalia.aem.common.messages.home.SubscribeSARequest;
//import com.alitalia.aem.common.messages.home.SubscribeSAResponse;

@Component(immediate = true, metatype = false)
@Service(value = SubscribeSASendMailBehaviour.class)
public class SubscribeSASendMailBehaviour extends Behaviour<SubscribeSASendMailRequest, SubscribeSASendMailResponse> {
  
  @Reference
  private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

  @Override
  public SubscribeSASendMailResponse executeOrchestration(SubscribeSASendMailRequest request) {
    if(request == null) throw new IllegalArgumentException("Request is null.");

    SubscribeSASendMailResponse response = personalAreaMilleMigliaService.SubscribeSASendMail(request);

    return response;
  }
}