package com.alitalia.aem.web.component.consumerlogin.behaviour;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloRequestLocal;
import com.alitalia.aem.common.messages.home.GetDatiSicurezzaProfiloResponseLocal;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

//import com.alitalia.aem.common.messages.home.MillemigliaSendEmailRequest;
//import com.alitalia.aem.common.messages.home.MillemigliaSendEmailResponse;
//import com.alitalia.aem.common.messages.home.SubscribeSARequest;
//import com.alitalia.aem.common.messages.home.SubscribeSAResponse;

@Component(immediate = true, metatype = false)
@Service(value = GetDatiSicurezzaProfiloBehaviour.class)
public class GetDatiSicurezzaProfiloBehaviour extends Behaviour<GetDatiSicurezzaProfiloRequestLocal, GetDatiSicurezzaProfiloResponseLocal> {
  
  @Reference
  private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

  @Override
  public GetDatiSicurezzaProfiloResponseLocal executeOrchestration(GetDatiSicurezzaProfiloRequestLocal request) {
    if(request == null) throw new IllegalArgumentException("Request is null.");

    GetDatiSicurezzaProfiloResponseLocal response = personalAreaMilleMigliaService.GetDatiSicurezzaProfilo(request);
    return response;
  }
}