package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.SocialAccountsRequest;
import com.alitalia.aem.common.messages.home.SocialAccountsResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = GetSocialAccountsBehaviour.class)
public class GetSocialAccountsBehaviour extends Behaviour<SocialAccountsRequest, SocialAccountsResponse> {
	
	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public SocialAccountsResponse executeOrchestration(SocialAccountsRequest request) {
		if(request == null) throw new IllegalArgumentException("Request is null.");
		return personalAreaMilleMigliaService.getSocialAccounts(request);
	}
}