package com.alitalia.aem.web.component.consumerlogin.behaviour;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.data.home.MMCustomerProfileData;
import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.ObjectFactory;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import java.util.logging.Logger;

@Component(immediate = true, metatype = false)
@Service(value = SubscribeSABehaviour.class)
public class SubscribeSABehaviour extends Behaviour<SubscribeSARequest, SubscribeSAResponse> {
  
  @Reference
  private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

  @Override
  public SubscribeSAResponse executeOrchestration(SubscribeSARequest request) {
    if(request == null) throw new IllegalArgumentException("Request is null.");
    SubscribeSAResponse response = personalAreaMilleMigliaService.subscribeSA(request);

    //invio email di Subscribe
    if(response.getProfileID() != null){


      String body = request.getBodyWelcomeMail();
      String objMail = request.getObjectMail(); 
      if(objMail == null || (objMail != null && "".equals(objMail))){
        objMail = "Benvenuto a bordo";
      }

      // **** strong auth  AS-IS  ******
      //      body = ((body.replace("{PLACEHOLDER_PIN}", response.getProfileID())
      //          ).replace("{PLACEHOLDER_CUSTOMERNAME}", response.getCustomerProfile().getCustomerName())
      //          ).replace("{PLACEHOLDER_CUSTOMERSURNAME}", response.getCustomerProfile().getCustomerSurname());
      // **********************************

      //*** strong auth  TO-BE ******
      MMCustomerProfileData customerProfile = request.getCustomerProfile();

      String customerName = "";
      if(customerProfile.getCustomerName() != null){
        customerName=customerProfile.getCustomerName();
      }

      String customerSurname = "";
      if(customerProfile.getCustomerSurname() != null){
        customerSurname=customerProfile.getCustomerSurname();
      }

      String customerUsername = "";
      if(customerProfile.getUsername() != null){
        customerUsername=customerProfile.getUsername();
      }

      String profileID = "";
      if(response.getProfileID() != null){
        profileID=response.getProfileID();
      }

      String pinNumber = "";
      if(customerProfile.getCustomerPinCode() != null){
        pinNumber=customerProfile.getCustomerPinCode();
      }else{
        try {

          //recupero il pin appena creato dell'utente
          String customerPassword=customerProfile.getPassword();

          ObjectFactory ob=new ObjectFactory();
          MilleMigliaLoginSARequest reqLoginSA=new MilleMigliaLoginSARequest();
          reqLoginSA.setUsername(customerUsername);
          reqLoginSA.setPassword(customerPassword);

          MilleMigliaLoginSAResponse respLoginSA = personalAreaMilleMigliaService.loginSA(reqLoginSA);
          if(respLoginSA.getCustomerSA()!=null){
            if(respLoginSA.getCustomerSA().getMmCustomerProfileData()!=null){
              if(respLoginSA.getCustomerSA().getMmCustomerProfileData().getCustomerPinCode()!=null){
                pinNumber=respLoginSA.getCustomerSA().getMmCustomerProfileData().getCustomerPinCode();
              }
            }
          }
        }catch (Exception ex){

        }
      }

      body = ((body.replace("{PLACEHOLDER_PIN}", pinNumber)
      ).replace("{PLACEHOLDER_CUSTOMERNAME}", customerName)
      ).replace("{PLACEHOLDER_CUSTOMERSURNAME}", customerSurname)
              .replace("{PLACEHOLDER_USERNAME}", customerUsername)
              .replace("{PLACEHOLDER_MMCODE}", profileID)
      ;
      // **********************************

      MillemigliaSendEmailRequest mailRequest = new MillemigliaSendEmailRequest();
      body = body.replaceAll("\r", "").replaceAll("\n", "");
      mailRequest.setMailBody(body);

      mailRequest.setCustomerProfile(request.getCustomerProfile());

      objMail = objMail.replaceAll("\r", "").replaceAll("\n", "");
      mailRequest.setMailSubject(objMail);

      MillemigliaSendEmailResponse mailResponse = personalAreaMilleMigliaService.sendNewRegistrationEmail(mailRequest);
      String testoPerStop = "";
    }
    
    return response;
  }
}