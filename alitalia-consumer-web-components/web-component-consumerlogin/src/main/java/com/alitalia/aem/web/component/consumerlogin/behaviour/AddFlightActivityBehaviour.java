package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.AddFlightActivityRequest;
import com.alitalia.aem.common.messages.home.AddFlightActivityResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = AddFlightActivityBehaviour.class)
public class AddFlightActivityBehaviour extends Behaviour<AddFlightActivityRequest, AddFlightActivityResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public AddFlightActivityResponse executeOrchestration(AddFlightActivityRequest request) {
		if(request == null) throw new IllegalArgumentException("Request is null.");
		return personalAreaMilleMigliaService.addFlightActivity(request);
	}
}