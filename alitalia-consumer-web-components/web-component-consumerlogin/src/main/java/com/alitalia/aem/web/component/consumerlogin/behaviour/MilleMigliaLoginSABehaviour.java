package com.alitalia.aem.web.component.consumerlogin.behaviour;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginRequest;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginResponse;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginSARequest;
import com.alitalia.aem.common.messages.home.MilleMigliaLoginSAResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = MilleMigliaLoginSABehaviour.class)
public class MilleMigliaLoginSABehaviour
		extends Behaviour<MilleMigliaLoginSARequest, MilleMigliaLoginSAResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public MilleMigliaLoginSAResponse executeOrchestration(MilleMigliaLoginSARequest request) {
		if(request == null)
			throw new IllegalArgumentException("Request is null.");

		MilleMigliaLoginSAResponse response = personalAreaMilleMigliaService.loginSA(request);
		
		return response;
	}

}
