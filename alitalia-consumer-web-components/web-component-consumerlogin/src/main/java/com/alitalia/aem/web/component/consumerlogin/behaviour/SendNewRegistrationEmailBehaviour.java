package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MillemigliaSendEmailRequest;
import com.alitalia.aem.common.messages.home.MillemigliaSendEmailResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = SendNewRegistrationEmailBehaviour.class)
public class SendNewRegistrationEmailBehaviour extends Behaviour<MillemigliaSendEmailRequest, MillemigliaSendEmailResponse> {

	@Reference
	private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

	@Override
	public MillemigliaSendEmailResponse executeOrchestration(MillemigliaSendEmailRequest request) {
		if(request == null) throw new IllegalArgumentException("Request is null.");
		return personalAreaMilleMigliaService.sendNewRegistrationEmail(request);
	}
}