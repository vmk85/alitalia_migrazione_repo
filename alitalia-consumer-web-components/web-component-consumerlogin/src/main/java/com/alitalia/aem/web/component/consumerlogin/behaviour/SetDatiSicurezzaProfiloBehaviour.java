package com.alitalia.aem.web.component.consumerlogin.behaviour;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

//import com.alitalia.aem.common.messages.home.MillemigliaSendEmailRequest;
//import com.alitalia.aem.common.messages.home.MillemigliaSendEmailResponse;
//import com.alitalia.aem.common.messages.home.SubscribeSARequest;
//import com.alitalia.aem.common.messages.home.SubscribeSAResponse;

@Component(immediate = true, metatype = false)
@Service(value = SetDatiSicurezzaProfiloBehaviour.class)
public class SetDatiSicurezzaProfiloBehaviour extends Behaviour<SetDatiSicurezzaProfiloRequest, SetDatiSicurezzaProfiloResponse> {
  
  @Reference
  private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

  @Override
  public SetDatiSicurezzaProfiloResponse executeOrchestration(SetDatiSicurezzaProfiloRequest request) {
    if(request == null) throw new IllegalArgumentException("Request is null.");

    SetDatiSicurezzaProfiloResponse response = personalAreaMilleMigliaService.SetDatiSicurezzaProfilo(request);

    return response;
  }
}