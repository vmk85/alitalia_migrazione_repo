package com.alitalia.aem.web.component.consumerlogin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MillemigliaSendEmailRequest;
import com.alitalia.aem.common.messages.home.MillemigliaSendEmailResponse;
import com.alitalia.aem.common.messages.home.SubscribeRequest;
import com.alitalia.aem.common.messages.home.SubscribeResponse;
import com.alitalia.aem.service.api.home.MyPersonalAreaMilleMigliaService;

@Component(immediate = true, metatype = false)
@Service(value = SubscribeBehaviour.class)
public class SubscribeBehaviour extends Behaviour<SubscribeRequest, SubscribeResponse> {
  
  @Reference
  private MyPersonalAreaMilleMigliaService personalAreaMilleMigliaService;

  @Override
  public SubscribeResponse executeOrchestration(SubscribeRequest request) {
    if(request == null) throw new IllegalArgumentException("Request is null.");
    SubscribeResponse response = personalAreaMilleMigliaService.subscribeByCustomer(request);
    if(response.getCustomerProfile() != null){
      String body = request.getBodyWelcomeMail();
      String objMail = request.getObjectMail(); 
      if(objMail == null || (objMail != null && "".equals(objMail))){
        objMail = "Benvenuto a bordo";
      }
      body = ((body.replace("{PLACEHOLDER_PIN}", response.getCustomerProfile().getCustomerPinCode())
          ).replace("{PLACEHOLDER_CUSTOMERNAME}", response.getCustomerProfile().getCustomerName())
          ).replace("{PLACEHOLDER_CUSTOMERSURNAME}", response.getCustomerProfile().getCustomerSurname());
      MillemigliaSendEmailRequest mailRequest = new MillemigliaSendEmailRequest();
      mailRequest.setMailBody(body);
      mailRequest.setCustomerProfile(response.getCustomerProfile());
      mailRequest.setMailSubject(objMail);
      MillemigliaSendEmailResponse mailResponse = personalAreaMilleMigliaService.sendNewRegistrationEmail(mailRequest);
      // TODO gestire la risposta dell'invio mail, segnalare all'utente se ci sono stati problemi
    }
    
    return response;
  }
}