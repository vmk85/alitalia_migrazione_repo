package com.alitalia.aem.web.component.consumerlogin.delegate;

import com.alitalia.aem.common.messages.home.*;
import com.alitalia.aem.web.component.consumerlogin.behaviour.*;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloRequest;
import com.alitalia.aem.ws.mypersonalarea.millemigliaservice.xsd1.SetDatiSicurezzaProfiloResponse;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;

@Service
@Component(immediate = true, metatype = false)
public class SimpleConsumerLoginDelegate implements ConsumerLoginDelegate {
	
	private static final Logger logger = LoggerFactory.getLogger(SimpleConsumerLoginDelegate.class);
	
	@Reference
	private MilleMigliaLoginBehaviour milleMigliaLoginBehaviour;

	@Reference
	private MilleMigliaLoginSABehaviour milleMigliaLoginSABehaviour;

	@Reference
	private SocialLoginBehaviour socialLoginBehaviour;
	
	@Reference
	private SetSocialAccountBehaviour setSocialAccountBehavior;
	
	@Reference
	private GetProfileBehaviour getProfileBehaviour;
	
	@Reference
	private GetAddressesBehaviour getAddressesBehaviour;
	
	@Reference
	private GetMessagesBehaviour getMessagesBehaviour;
	
	@Reference
	private GetBalanceBehaviour getBalanceBehaviour;
	
	@Reference
	private UpdateUserProfileBehaviour updateUserProfileBehaviour;
	
	@Reference
	private GetStatementByDateBehaviour getStatementByDateBehaviour;
	
	@Reference
	private AddFlightActivityBehaviour addFlightActivityBehaviour;
	
	@Reference
	private UpdatePinBehaviour updatePinBehaviour;
	
	@Reference
	private GetSocialAccountsBehaviour getSocialAccountsBehaviour;
	
	@Reference
	private SubscribeBehaviour subscribeBehaviour;
	
	@Reference
	private UpdateNicknameBehaviour updateNicknameBehaviour;
	
	@Reference
	private RetrievePinByCustomerNumberBehaviour retrievePinByCustomerNumberBehaviour;
	
	@Reference
	private RetrievePinByNicknameBehaviour retrievePinByNicknameBehaviour;
	
	@Reference
	private SendNewRegistrationEmailBehaviour sendNewRegistrationEmailBehaviour;
	
	@Reference
	private SendNewMailingListRegistrationMailBehaviour sendNewMailingListRegistrationMailBehaviour;
	
	@Reference
	private MailinglistRegistrationBehaviour mailinglistRegistrationBehaviour;
	
	@Reference
	private DeletePanBehaviour deletePanBehaviour;
	
	@Reference
	private RemoveUidGigyaBehaviour removeUidGigyaBehaviour;
	
	@Reference
	private CreditCardsListBehaviour creditCardsListBehaviour;
	
	@Reference
	private PaybackAccrualBehaviour paybackAccrualBehaviour;

	@Reference
	private SubscribeSABehaviour subscribeSABehaviour;

	@Reference
	private SubscribeSACheckEmailUserNameDuplicateBehaviour subscribeSACheckEmailUserNameDuplicateBehaviour;

	@Reference
	private SubscribeSACheckUserNameDuplicateBehaviour subscribeSACheckUserNameDuplicateBehaviour;

	@Reference
	private SubscribeSASendMailBehaviour subscribeSASendMailBehaviour;

	@Reference
	private SetDatiSicurezzaProfiloBehaviour setDatiSicurezzaProfiloBehaviour;

	@Reference
	private GetDatiSicurezzaProfiloBehaviour getDatiSicurezzaProfiloBehaviour;

	@Reference
	private FindProfileBehaviour findProfileBehaviour;



	@Override
	public MilleMigliaLoginResponse milleMigliaLogin(MilleMigliaLoginRequest request) {
		BehaviourExecutor<MilleMigliaLoginBehaviour, MilleMigliaLoginRequest, 
				MilleMigliaLoginResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(milleMigliaLoginBehaviour, request);
	}

	@Override
	public MilleMigliaLoginSAResponse milleMigliaLoginSA(MilleMigliaLoginSARequest request) {
		BehaviourExecutor<MilleMigliaLoginSABehaviour, MilleMigliaLoginSARequest,
				MilleMigliaLoginSAResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(milleMigliaLoginSABehaviour, request);
	}

	@Override
	public SocialLoginResponse socialLogin(SocialLoginRequest request) {
		BehaviourExecutor<SocialLoginBehaviour, SocialLoginRequest, SocialLoginResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(socialLoginBehaviour, request);
	}
	
	@Override
	public SetSocialAccountResponse setSocialAccount(SetSocialAccountRequest request) {
		BehaviourExecutor<SetSocialAccountBehaviour, SetSocialAccountRequest, SetSocialAccountResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(setSocialAccountBehavior, request);
	}
	
	@Override
	public ProfileResponse getProfile(ProfileRequest request) {
		BehaviourExecutor<GetProfileBehaviour, ProfileRequest, ProfileResponse> executor = new BehaviourExecutor<GetProfileBehaviour, ProfileRequest, ProfileResponse>();
		return executor.executeBehaviour(getProfileBehaviour, request);
	}

	@Override
	public AddressesResponse getAddresses(AddressesRequest request) {
		BehaviourExecutor<GetAddressesBehaviour, AddressesRequest, AddressesResponse> executor = new BehaviourExecutor<GetAddressesBehaviour, AddressesRequest, AddressesResponse>();
		return executor.executeBehaviour(getAddressesBehaviour, request);
	}

	@Override
	public UpdateUserProfileResponse updateProfile(UpdateUserProfileRequest request) {
		BehaviourExecutor<UpdateUserProfileBehaviour, UpdateUserProfileRequest, UpdateUserProfileResponse> executor = new BehaviourExecutor<UpdateUserProfileBehaviour, UpdateUserProfileRequest, UpdateUserProfileResponse>();
		return executor.executeBehaviour(updateUserProfileBehaviour, request);
	}

	@Override
	public MessagesResponse getMessages(MessagesRequest request) {
		BehaviourExecutor<GetMessagesBehaviour, MessagesRequest, MessagesResponse> executor = new BehaviourExecutor<GetMessagesBehaviour, MessagesRequest, MessagesResponse>();
		return executor.executeBehaviour(getMessagesBehaviour, request);
	}

	@Override
	public BalanceResponse getBalance(BalanceRequest request) {
		BehaviourExecutor<GetBalanceBehaviour, BalanceRequest, BalanceResponse> executor = new BehaviourExecutor<GetBalanceBehaviour, BalanceRequest, BalanceResponse>();
		return executor.executeBehaviour(getBalanceBehaviour, request);
	}

	@Override
	public StatementByDateResponse getStatementByDate(StatementByDateRequest request) {
		BehaviourExecutor<GetStatementByDateBehaviour, StatementByDateRequest, StatementByDateResponse> executor = new BehaviourExecutor<GetStatementByDateBehaviour, StatementByDateRequest, StatementByDateResponse>();
		return executor.executeBehaviour(getStatementByDateBehaviour, request);
	}

	@Override
	public AddFlightActivityResponse addFlightActivity(AddFlightActivityRequest request) {
		BehaviourExecutor<AddFlightActivityBehaviour, AddFlightActivityRequest, AddFlightActivityResponse> executor = new BehaviourExecutor<AddFlightActivityBehaviour, AddFlightActivityRequest, AddFlightActivityResponse>();
		return executor.executeBehaviour(addFlightActivityBehaviour, request);
	}

	@Override
	public UpdatePinResponse updatePin(UpdatePinRequest request) {
		BehaviourExecutor<UpdatePinBehaviour, UpdatePinRequest, UpdatePinResponse> executor = new BehaviourExecutor<UpdatePinBehaviour, UpdatePinRequest, UpdatePinResponse>();
		return executor.executeBehaviour(updatePinBehaviour, request);
	}

	@Override
	public SocialAccountsResponse getSocialAccounts(SocialAccountsRequest request) {
		BehaviourExecutor<GetSocialAccountsBehaviour, SocialAccountsRequest, SocialAccountsResponse> executor = new BehaviourExecutor<GetSocialAccountsBehaviour, SocialAccountsRequest, SocialAccountsResponse>();
		return executor.executeBehaviour(getSocialAccountsBehaviour, request);
	}

	@Override
	public SubscribeResponse subscribeByCustomer(SubscribeRequest request) {
		BehaviourExecutor<SubscribeBehaviour, SubscribeRequest, SubscribeResponse> executor = new BehaviourExecutor<SubscribeBehaviour, SubscribeRequest, SubscribeResponse>();
		return executor.executeBehaviour(subscribeBehaviour, request);
	}

	@Override
	public UpdateNicknameResponse updateNickname(UpdateNicknameRequest request) {
		BehaviourExecutor<UpdateNicknameBehaviour, UpdateNicknameRequest, UpdateNicknameResponse> executor = new BehaviourExecutor<UpdateNicknameBehaviour, UpdateNicknameRequest, UpdateNicknameResponse>();
		return executor.executeBehaviour(updateNicknameBehaviour, request);
	}

	@Override
	public RetrievePinResponse retrievePinByCustomerNumber(RetrievePinRequest request) {
		BehaviourExecutor<RetrievePinByCustomerNumberBehaviour, RetrievePinRequest, RetrievePinResponse> executor = new BehaviourExecutor<RetrievePinByCustomerNumberBehaviour, RetrievePinRequest, RetrievePinResponse>();
		return executor.executeBehaviour(retrievePinByCustomerNumberBehaviour, request);
	}

	@Override
	public RetrievePinResponse retrievePinByNickname(RetrievePinRequest request) {
		BehaviourExecutor<RetrievePinByNicknameBehaviour, RetrievePinRequest, RetrievePinResponse> executor = new BehaviourExecutor<RetrievePinByNicknameBehaviour, RetrievePinRequest, RetrievePinResponse>();
		return executor.executeBehaviour(retrievePinByNicknameBehaviour, request);
	}

	@Override
	public MillemigliaSendEmailResponse sendNewRegistrationEmail(MillemigliaSendEmailRequest request) {
		BehaviourExecutor<SendNewRegistrationEmailBehaviour, MillemigliaSendEmailRequest, MillemigliaSendEmailResponse> executor = new BehaviourExecutor<SendNewRegistrationEmailBehaviour, MillemigliaSendEmailRequest, MillemigliaSendEmailResponse>();
		return executor.executeBehaviour(sendNewRegistrationEmailBehaviour, request);
	}

	@Override
	public MillemigliaSendEmailResponse sendNewMailingListRegistrationMail(SendNewMailingListRegistrationMailRequest request) {
		BehaviourExecutor<SendNewMailingListRegistrationMailBehaviour, SendNewMailingListRegistrationMailRequest, MillemigliaSendEmailResponse> executor = new BehaviourExecutor<SendNewMailingListRegistrationMailBehaviour, SendNewMailingListRegistrationMailRequest, MillemigliaSendEmailResponse>();
		return executor.executeBehaviour(sendNewMailingListRegistrationMailBehaviour, request);
	}

	@Override
	public MailinglistRegistrationResponse registerToMailinglist(MailinglistRegistrationRequest request) {
		BehaviourExecutor<MailinglistRegistrationBehaviour, MailinglistRegistrationRequest, MailinglistRegistrationResponse> executor = new BehaviourExecutor<MailinglistRegistrationBehaviour, MailinglistRegistrationRequest, MailinglistRegistrationResponse>();
		
		MailinglistRegistrationResponse mailinglistRegistrationResponse = executor.executeBehaviour(mailinglistRegistrationBehaviour, request);
		
		if(mailinglistRegistrationResponse != null && mailinglistRegistrationResponse.isRegistrationIsSuccessful()){


			if ( request.getAction().value().equals("Register")){


					String body = request.getBodyWelcomeMail();
					SendNewMailingListRegistrationMailRequest sendEmailRequest = new SendNewMailingListRegistrationMailRequest();
					sendEmailRequest.setCustomer(request.getCustomerProfile());
					sendEmailRequest.setMailBody(body);
					sendEmailRequest.setMailSubject(request.getSubjectMail());
					MillemigliaSendEmailResponse sendNewMailingListRegistrationMail = sendNewMailingListRegistrationMail(sendEmailRequest);
					if (sendNewMailingListRegistrationMail != null && sendNewMailingListRegistrationMail.isMailSent()) {
						logger.debug("[NewsletterSubscribeEmail] - success");
					} else {
						logger.error("[NewsletterSubscribeEmail] - fail");
					}
			}else{
				logger.debug("[NewsletterDeleteEmail] - success");
			}
		}

		return mailinglistRegistrationResponse;
	}

	@Override
	public DeletePanResponse deletePan(DeletePanRequest request) {
		BehaviourExecutor<DeletePanBehaviour, DeletePanRequest, DeletePanResponse> executor = new BehaviourExecutor<DeletePanBehaviour, DeletePanRequest, DeletePanResponse>();
		return executor.executeBehaviour(deletePanBehaviour, request);
	}
	
	@Override
	public RemoveSocialAccountsResponse removeUidGigya(RemoveSocialAccountsRequest request) {
		BehaviourExecutor<RemoveUidGigyaBehaviour, RemoveSocialAccountsRequest, RemoveSocialAccountsResponse> executor = new BehaviourExecutor<RemoveUidGigyaBehaviour, RemoveSocialAccountsRequest, RemoveSocialAccountsResponse>();
		return executor.executeBehaviour(removeUidGigyaBehaviour, request);
	}

	@Override
	public CreditCardsListResponse getCreditCardList(CreditCardsListRequest request) {
		BehaviourExecutor<CreditCardsListBehaviour, CreditCardsListRequest, CreditCardsListResponse> executor = new BehaviourExecutor<CreditCardsListBehaviour, CreditCardsListRequest, CreditCardsListResponse>();
		return executor.executeBehaviour(creditCardsListBehaviour, request);
	}
	
	@Override
	public MMPaybackAccrualResponse paybackAccrual(MMPaybackAccrualRequest request) {
		BehaviourExecutor<PaybackAccrualBehaviour, MMPaybackAccrualRequest, MMPaybackAccrualResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(paybackAccrualBehaviour, request);
	}

	@Override
	public SubscribeSAResponse subscribeSA(SubscribeSARequest request) {
		BehaviourExecutor<SubscribeSABehaviour, SubscribeSARequest, SubscribeSAResponse> executor =
				new BehaviourExecutor<SubscribeSABehaviour, SubscribeSARequest, SubscribeSAResponse>();
		return executor.executeBehaviour(subscribeSABehaviour, request);
	}

	@Override
	public SubscribeSACheckEmailUserNameDuplicateResponse CheckEmailUserNameDuplicate(SubscribeSACheckEmailUserNameDuplicateRequest request) {
		BehaviourExecutor<SubscribeSACheckEmailUserNameDuplicateBehaviour, SubscribeSACheckEmailUserNameDuplicateRequest, SubscribeSACheckEmailUserNameDuplicateResponse> executor
				= new BehaviourExecutor<SubscribeSACheckEmailUserNameDuplicateBehaviour,SubscribeSACheckEmailUserNameDuplicateRequest,SubscribeSACheckEmailUserNameDuplicateResponse>();
		return executor.executeBehaviour(subscribeSACheckEmailUserNameDuplicateBehaviour, request);
	}

	@Override
	public SubscribeSACheckUserNameDuplicateResponse CheckUserNameDuplicate(SubscribeSACheckUserNameDuplicateRequest request) {
		BehaviourExecutor<SubscribeSACheckUserNameDuplicateBehaviour, SubscribeSACheckUserNameDuplicateRequest, SubscribeSACheckUserNameDuplicateResponse> executor
				= new BehaviourExecutor<SubscribeSACheckUserNameDuplicateBehaviour,SubscribeSACheckUserNameDuplicateRequest,SubscribeSACheckUserNameDuplicateResponse>();
		return executor.executeBehaviour(subscribeSACheckUserNameDuplicateBehaviour, request);
	}

	@Override
	public SubscribeSASendMailResponse sendRetrieveMail(SubscribeSASendMailRequest request) {
		BehaviourExecutor<SubscribeSASendMailBehaviour, SubscribeSASendMailRequest, SubscribeSASendMailResponse> executor
				= new BehaviourExecutor<SubscribeSASendMailBehaviour,SubscribeSASendMailRequest,SubscribeSASendMailResponse>();
		return executor.executeBehaviour(subscribeSASendMailBehaviour, request);
	}



	@Override
	public com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloResponse SetDatiSicurezzaProfilo(com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloRequest request) {
		BehaviourExecutor<SetDatiSicurezzaProfiloBehaviour, com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloRequest, com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloResponse> executor
				= new BehaviourExecutor<SetDatiSicurezzaProfiloBehaviour,com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloRequest,com.alitalia.aem.common.messages.home.SetDatiSicurezzaProfiloResponse>();
		return executor.executeBehaviour(setDatiSicurezzaProfiloBehaviour, request);
	}


	@Override
	public GetDatiSicurezzaProfiloResponseLocal GetDatiSicurezzaProfilo(GetDatiSicurezzaProfiloRequestLocal request) {
		BehaviourExecutor<GetDatiSicurezzaProfiloBehaviour, GetDatiSicurezzaProfiloRequestLocal, GetDatiSicurezzaProfiloResponseLocal> executor
				= new BehaviourExecutor<GetDatiSicurezzaProfiloBehaviour,GetDatiSicurezzaProfiloRequestLocal,GetDatiSicurezzaProfiloResponseLocal>();
		return executor.executeBehaviour(getDatiSicurezzaProfiloBehaviour, request);
	}



	@Override
	public FindProfileResponseLocal FindProfile(FindProfileRequestLocal request) {
		BehaviourExecutor<FindProfileBehaviour, FindProfileRequestLocal, FindProfileResponseLocal> executor
				= new BehaviourExecutor<FindProfileBehaviour,FindProfileRequestLocal,FindProfileResponseLocal>();
		return executor.executeBehaviour(findProfileBehaviour, request);
	}

}