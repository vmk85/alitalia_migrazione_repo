package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceResponse;
import com.alitalia.aem.service.api.home.IServiceInsurance;

@Component(immediate = true, metatype = false)
@Service(value = DeleteInsuranceBehaviour.class)
public class DeleteInsuranceBehaviour extends Behaviour<DeleteInsuranceRequest, DeleteInsuranceResponse> {

	@Reference
	private IServiceInsurance serviceInsurance;
	
	@Override
	protected DeleteInsuranceResponse executeOrchestration(DeleteInsuranceRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Delete Insurance request is null.");
		return serviceInsurance.deleteInsurance(request);
	}

}
