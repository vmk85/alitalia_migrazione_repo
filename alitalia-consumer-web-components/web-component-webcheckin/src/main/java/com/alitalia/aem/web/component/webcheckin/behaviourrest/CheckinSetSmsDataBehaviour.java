package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetSmsDataRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetSmsDataResponse;
import com.alitalia.aem.service.api.home.ICheckinService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = CheckinSetSmsDataBehaviour.class)
public class CheckinSetSmsDataBehaviour extends Behaviour<CheckinSetSmsDataRequest, CheckinSetSmsDataResponse> {

    @Reference
    private ICheckinService checkinServiceRest;

    @Override
    protected CheckinSetSmsDataResponse executeOrchestration(CheckinSetSmsDataRequest request) {
        if(request == null)
            throw new IllegalArgumentException("Retrieve SMS data request is null.");
        return checkinServiceRest.setSmsData(request);
    }
}
