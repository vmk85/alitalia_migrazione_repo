package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.ExtraBaggagePolicyCheckinRequest;
import com.alitalia.aem.common.messages.home.ExtraBaggagePolicyCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinExtraBaggageService;

@Component(immediate = true, metatype = false)
@Service(value = GetBaggagePolicyBehaviour.class)
public class GetBaggagePolicyBehaviour extends Behaviour<ExtraBaggagePolicyCheckinRequest, ExtraBaggagePolicyCheckinResponse> {

	@Reference
	private WebCheckinExtraBaggageService webcheckinExtraBaggageService;

	@Override
	public ExtraBaggagePolicyCheckinResponse executeOrchestration(ExtraBaggagePolicyCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("ExtraBaggagePolicyCheckinRequest is null.");
		return webcheckinExtraBaggageService.getBaggagePolicy(request);
	}
}
