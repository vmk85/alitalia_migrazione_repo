package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinAuthorize3dRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinAuthorize3dResponse;
import com.alitalia.aem.service.api.home.ICheckinPaymentRestService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = CheckinAuthorize3dBehaviour.class)
public class CheckinAuthorize3dBehaviour extends Behaviour<CheckinAuthorize3dRequest, CheckinAuthorize3dResponse> {

    @Reference
    private ICheckinPaymentRestService checkinPaymentRestService;

    @Override
    public CheckinAuthorize3dResponse executeOrchestration(CheckinAuthorize3dRequest request) {
        if(request == null)
            throw new IllegalArgumentException("authorize3d request is null.");
        return checkinPaymentRestService.authorize3d(request);
    }
}
