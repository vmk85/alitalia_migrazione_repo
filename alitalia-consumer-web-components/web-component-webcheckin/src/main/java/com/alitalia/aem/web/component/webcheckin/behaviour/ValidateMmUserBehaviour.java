package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.ValidateMmUserCheckinRequest;
import com.alitalia.aem.common.messages.home.ValidateMmUserCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinSearchService;

@Component(immediate = true, metatype = false)
@Service(value = ValidateMmUserBehaviour.class)
public class ValidateMmUserBehaviour extends Behaviour<ValidateMmUserCheckinRequest, ValidateMmUserCheckinResponse> {
	
	@Reference
	private WebCheckinSearchService webcheckinValidateService;
	
	@Override
	public ValidateMmUserCheckinResponse executeOrchestration(ValidateMmUserCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("ValidateMmUserCheckinRequest is null.");
		return webcheckinValidateService.validateUser(request);
	}

}
