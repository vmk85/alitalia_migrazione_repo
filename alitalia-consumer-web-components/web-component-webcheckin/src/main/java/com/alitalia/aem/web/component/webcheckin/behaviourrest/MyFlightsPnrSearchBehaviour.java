package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.service.api.home.ICheckinService;
import com.alitalia.aem.service.api.home.IMyFlightsService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = MyFlightsPnrSearchBehaviour.class)
public class MyFlightsPnrSearchBehaviour extends Behaviour<CheckinPnrSearchRequest, CheckinPnrSearchResponse> {
	
	@Reference
	private IMyFlightsService myFlightsServiceRest;
	
	@Override
	public CheckinPnrSearchResponse executeOrchestration(CheckinPnrSearchRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve PnrSearch request is null.");
		return myFlightsServiceRest.retrieveCheckinPnr(request);
	}

}
