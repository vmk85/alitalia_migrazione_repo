package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinInitPaymentRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinInitPaymentResponse;
import com.alitalia.aem.service.api.home.ICheckinPaymentRestService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinInitPaymentRecurringAdyenBehaviour.class)
public class CheckinInitPaymentRecurringAdyenBehaviour extends Behaviour<CheckinInitPaymentRequest, CheckinInitPaymentResponse> {

	@Reference
	private ICheckinPaymentRestService checkinPaymentRestService;

	@Override
	public CheckinInitPaymentResponse executeOrchestration(CheckinInitPaymentRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("initPayment request is null.");
		return checkinPaymentRestService.initPaymentRecurringAdyen(request);
	}
}


