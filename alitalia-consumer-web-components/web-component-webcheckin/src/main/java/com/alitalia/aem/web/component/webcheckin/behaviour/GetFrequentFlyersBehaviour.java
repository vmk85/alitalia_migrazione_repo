package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.GetCountryRequest;
import com.alitalia.aem.common.messages.home.WebCheckinFrequentFlyerResponse;
import com.alitalia.aem.service.api.home.WebCheckinStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = GetFrequentFlyersBehaviour.class)
public class GetFrequentFlyersBehaviour extends Behaviour<GetCountryRequest, WebCheckinFrequentFlyerResponse> {
	
	@Reference
	private WebCheckinStaticDataService webcheckinStaticDataService;
	
	@Override
	public WebCheckinFrequentFlyerResponse executeOrchestration(GetCountryRequest request) {
		if (request == null)
			throw new IllegalArgumentException("GetCountryRequest is null.");
		return webcheckinStaticDataService.getFrequentFlyers(request);
	}
}