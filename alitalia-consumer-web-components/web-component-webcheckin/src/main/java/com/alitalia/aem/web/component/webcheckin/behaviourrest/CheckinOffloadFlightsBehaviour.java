package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadFlightsRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadFlightsResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadResponse;
import com.alitalia.aem.service.api.home.ICheckinService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;


@Component(immediate = true, metatype = false)
@Service(value = CheckinOffloadFlightsBehaviour.class)
public class CheckinOffloadFlightsBehaviour extends Behaviour<CheckinOffloadFlightsRequest, CheckinOffloadFlightsResponse> {

    @Reference
    private ICheckinService checkinServiceRest;

    @Override
    public CheckinOffloadFlightsResponse executeOrchestration(CheckinOffloadFlightsRequest request) {
        if(request == null)
            throw new IllegalArgumentException("Checkin Offload request is null.");
        return checkinServiceRest.offloadFlights(request);
    }
}
