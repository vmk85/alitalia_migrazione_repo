package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdateSecurityInfoRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdateSecurityInfoResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinUpdateSecurityInfoBehaviour.class)
public class CheckinUpdateSecurityInfoBehaviour extends Behaviour<CheckinUpdateSecurityInfoRequest, CheckinUpdateSecurityInfoResponse> {
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinUpdateSecurityInfoResponse executeOrchestration(CheckinUpdateSecurityInfoRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve Boarding Pass request is null.");
		return checkinServiceRest.updateSecurityInfo(request);
	}
}
