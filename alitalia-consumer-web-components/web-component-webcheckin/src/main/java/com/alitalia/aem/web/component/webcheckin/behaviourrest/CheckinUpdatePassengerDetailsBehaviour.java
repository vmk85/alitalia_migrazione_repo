package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdatePassengerDetailsRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdatePassengerDetailsResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinUpdatePassengerDetailsBehaviour.class)
public class CheckinUpdatePassengerDetailsBehaviour extends Behaviour<CheckinUpdatePassengerDetailsRequest, CheckinUpdatePassengerDetailsResponse> {
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinUpdatePassengerDetailsResponse executeOrchestration(CheckinUpdatePassengerDetailsRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve Boarding Pass request is null.");
		return checkinServiceRest.updatePassengerDetails(request);
	}
}
