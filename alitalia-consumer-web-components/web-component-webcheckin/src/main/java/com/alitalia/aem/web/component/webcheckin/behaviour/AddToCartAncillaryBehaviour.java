package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinAddToCartRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryResponse;
import com.alitalia.aem.service.api.home.WebCheckinAncillaryService;

@Component(immediate = true, metatype = false)
@Service(value = AddToCartAncillaryBehaviour.class)
public class AddToCartAncillaryBehaviour extends Behaviour<WebCheckinAddToCartRequest, WebCheckinAncillaryResponse> {
	
	@Reference
	private WebCheckinAncillaryService webCheckinAncillaryService;
	
	@Override
	public WebCheckinAncillaryResponse executeOrchestration(WebCheckinAddToCartRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinAddToCartRequest is null.");
		return webCheckinAncillaryService.addToCart(request);
	}
}