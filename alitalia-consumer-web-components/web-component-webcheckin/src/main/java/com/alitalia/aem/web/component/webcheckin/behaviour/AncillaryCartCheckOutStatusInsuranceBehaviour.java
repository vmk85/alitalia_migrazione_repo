package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusResponse;
import com.alitalia.aem.service.api.home.WebCheckinAncillaryService;

@Component(immediate=true, metatype=false)
@Service(value=AncillaryCartCheckOutStatusInsuranceBehaviour.class)
public class AncillaryCartCheckOutStatusInsuranceBehaviour extends
		Behaviour<MmbAncillaryCheckOutStatusRequest, MmbAncillaryCheckOutStatusResponse> {

	@Reference
	private WebCheckinAncillaryService webCheckinAncillaryService;

	@Override
	protected MmbAncillaryCheckOutStatusResponse executeOrchestration(MmbAncillaryCheckOutStatusRequest request) {
		if (request == null)
			throw new IllegalArgumentException("MmbAncillaryCheckOutStatusRequest is null.");
		return webCheckinAncillaryService.getCheckOutStatus(request);
	}

}
