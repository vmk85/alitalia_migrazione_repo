package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetLoungeRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetLoungeResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinSetLoungeBehaviour.class)
public class CheckinSetLoungeBehaviour extends Behaviour<CheckinSetLoungeRequest, CheckinSetLoungeResponse>{
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	protected CheckinSetLoungeResponse executeOrchestration(
			CheckinSetLoungeRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Set fast track request is null.");
		return checkinServiceRest.setLounge(request);
	}
}
