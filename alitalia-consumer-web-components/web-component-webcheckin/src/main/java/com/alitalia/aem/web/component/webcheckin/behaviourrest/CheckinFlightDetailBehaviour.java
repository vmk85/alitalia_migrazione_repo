package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinFlightDetailRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinFlightDetailResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinFlightDetailBehaviour.class)
public class CheckinFlightDetailBehaviour extends Behaviour<CheckinFlightDetailRequest, CheckinFlightDetailResponse>{
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinFlightDetailResponse executeOrchestration(CheckinFlightDetailRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve flight detail request is null.");
		return checkinServiceRest.retrieveFlightDetail(request);
	}

}
