package com.alitalia.aem.web.component.webcheckin.delegaterest;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.checkinrest.*;
import com.alitalia.aem.web.component.webcheckin.behaviourrest.*;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component(immediate=true, metatype=false)
@Properties({
	@Property( name = MyFlightsDelegateImpl.CACHE_REFRESH_INTERVAL, label = "Refresh interval of the cache." ),
	@Property( name = MyFlightsDelegateImpl.IS_CACHE_ENABLED, label = "Flag to enable cache." )
})
@Service
public class MyFlightsDelegateImpl implements IMyFlightsDelegate {

	@Reference
	private MyFlightsPnrSearchBehaviour myFlightsPnrSearchBehaviour;

	private static final Logger logger = LoggerFactory.getLogger(MyFlightsDelegateImpl.class);
	
	public static final String CACHE_REFRESH_INTERVAL = "cache.refresh.interval";
	public static final String IS_CACHE_ENABLED = "cache.is_enabled";
	
	private long refreshInterval = 3600000L;
	private boolean isCacheEnabled = false;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		try {		
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify CheckinDelegateImpl - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Activated CheckinDelegateImpl: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		try {
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify CheckinDelegateImpl - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Modified CheckinDelegateImpl: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}

	@Override
	public CheckinPnrSearchResponse retrieveCheckinPnr(CheckinPnrSearchRequest request) {
		CheckinPnrSearchResponse response = null;
		BehaviourExecutor<MyFlightsPnrSearchBehaviour, CheckinPnrSearchRequest, CheckinPnrSearchResponse> executor = new BehaviourExecutor<MyFlightsPnrSearchBehaviour, CheckinPnrSearchRequest, CheckinPnrSearchResponse>();
		
		try {
			logger.debug("Executing delegate retrieveCheckinPnr");
			response = executor.executeBehaviour(myFlightsPnrSearchBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - retrieveCheckinPnr] - Errore durante il recupero delle informazioni di viaggio.", e);
		}
		
		return response;
	}

}
