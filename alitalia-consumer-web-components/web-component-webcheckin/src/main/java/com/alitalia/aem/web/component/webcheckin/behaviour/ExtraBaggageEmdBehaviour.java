package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinEmdRequest;
import com.alitalia.aem.common.messages.home.WebCheckinEmdResponse;
import com.alitalia.aem.service.api.home.WebCheckinEmdService;

@Component(immediate = true, metatype = false)
@Service(value = ExtraBaggageEmdBehaviour.class)
public class ExtraBaggageEmdBehaviour extends Behaviour<WebCheckinEmdRequest, WebCheckinEmdResponse> {
	
	@Reference
	private WebCheckinEmdService webcheckinEmdService;
	
	@Override
	public WebCheckinEmdResponse executeOrchestration(WebCheckinEmdRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinEmdRequest is null.");
		return webcheckinEmdService.extraBaggageEmd(request);
	}
}