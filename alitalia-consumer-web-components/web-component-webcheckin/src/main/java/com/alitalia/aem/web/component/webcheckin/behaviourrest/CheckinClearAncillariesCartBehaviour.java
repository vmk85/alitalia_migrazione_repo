package com.alitalia.aem.web.component.webcheckin.behaviourrest;


import com.alitalia.aem.common.messages.checkinrest.CheckinClearAncillariesCartRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinClearAncillariesCartResponse;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;

import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinClearAncillariesCartBehaviour.class)
public class CheckinClearAncillariesCartBehaviour extends Behaviour<CheckinClearAncillariesCartRequest, CheckinClearAncillariesCartResponse>{
    @Reference
    private ICheckinService checkinServiceRest;

    @Override
    protected CheckinClearAncillariesCartResponse executeOrchestration(
            CheckinClearAncillariesCartRequest request) {
        if(request == null)
            throw new IllegalArgumentException("Set clear ancillaries cart request is null.");
        return checkinServiceRest.clearAncillariesCart(request);
    }
}
