package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinClearAncillarySessionEndRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinClearAncillarySessionEndResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinClearAncillarySessionEndBehaviour.class)
public class CheckinClearAncillarySessionEndBehaviour extends Behaviour<CheckinClearAncillarySessionEndRequest, CheckinClearAncillarySessionEndResponse> {

	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinClearAncillarySessionEndResponse executeOrchestration(CheckinClearAncillarySessionEndRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Clear Ancillary and End Session request is null.");
		return checkinServiceRest.clearAcillayEndSession(request);
	}
}
