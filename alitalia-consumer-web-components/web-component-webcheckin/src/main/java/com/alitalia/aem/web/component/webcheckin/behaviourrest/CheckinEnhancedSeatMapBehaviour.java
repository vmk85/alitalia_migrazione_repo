package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinEnhancedSeatMapRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinEnhancedSeatMapResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinEnhancedSeatMapBehaviour.class)
public class CheckinEnhancedSeatMapBehaviour  extends Behaviour<CheckinEnhancedSeatMapRequest, CheckinEnhancedSeatMapResponse> {
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinEnhancedSeatMapResponse executeOrchestration(CheckinEnhancedSeatMapRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve Boarding Pass request is null.");
		return checkinServiceRest.enhancedSeatMap(request);
	}
}
