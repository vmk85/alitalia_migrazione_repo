package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetBoardingPassInfoByNameRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetBoardingPassInfoByNameResponse;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetBoardingPassInfoResponse;
import com.alitalia.aem.service.api.home.ICheckinService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = CheckinGetBoardingPassInfoByNameBehaviour.class)
public class CheckinGetBoardingPassInfoByNameBehaviour extends Behaviour<CheckinGetBoardingPassInfoByNameRequest, CheckinGetBoardingPassInfoByNameResponse> {
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	protected CheckinGetBoardingPassInfoByNameResponse executeOrchestration(CheckinGetBoardingPassInfoByNameRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve boarding pass info request is null.");
		return checkinServiceRest.getBoardingPassInfoByName(request);
	}
}
