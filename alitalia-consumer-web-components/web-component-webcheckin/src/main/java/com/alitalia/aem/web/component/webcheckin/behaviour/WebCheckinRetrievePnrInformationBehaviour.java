package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrievePnrInformationCheckinResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.service.api.home.WebCheckinSearchService;

@Component(immediate = true, metatype = false)
@Service(value = WebCheckinRetrievePnrInformationBehaviour.class)
public class WebCheckinRetrievePnrInformationBehaviour extends Behaviour<RetriveMmbPnrInformationRequest, RetrievePnrInformationCheckinResponse> {

	@Reference
	private WebCheckinSearchService webcheckinSearchService;
	
	@Override
	public RetrievePnrInformationCheckinResponse executeOrchestration(RetriveMmbPnrInformationRequest request) {
		if (request == null)
			throw new IllegalArgumentException("RetriveMmbPnrInformationRequest is null.");
		return webcheckinSearchService.retrievePnrInformation(request);
	}

}
