package com.alitalia.aem.web.component.webcheckin.delegaterest;

import com.alitalia.aem.common.messages.checkinrest.*;

public interface IMyFlightsDelegate {
	CheckinPnrSearchResponse retrieveCheckinPnr(CheckinPnrSearchRequest request);

}
