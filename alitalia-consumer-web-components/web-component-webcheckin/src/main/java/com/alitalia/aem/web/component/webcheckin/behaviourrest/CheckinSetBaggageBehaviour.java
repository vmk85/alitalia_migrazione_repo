package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetBaggageRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetBaggageResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinSetBaggageBehaviour.class)
public class CheckinSetBaggageBehaviour  extends Behaviour<CheckinSetBaggageRequest, CheckinSetBaggageResponse> {
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinSetBaggageResponse executeOrchestration(CheckinSetBaggageRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve Boarding Pass request is null.");
		return checkinServiceRest.setBaggage(request);
	}

}
