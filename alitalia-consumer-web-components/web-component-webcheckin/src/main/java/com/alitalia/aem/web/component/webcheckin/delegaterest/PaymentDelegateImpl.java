package com.alitalia.aem.web.component.webcheckin.delegaterest;

import com.alitalia.aem.common.messages.checkinrest.*;
import com.alitalia.aem.web.component.webcheckin.behaviourrest.*;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;

@Component(immediate=true, metatype=false)
@Properties({
	@Property( name = CheckinDelegateImpl.CACHE_REFRESH_INTERVAL, label = "Refresh interval of the cache." ),
	@Property( name = CheckinDelegateImpl.IS_CACHE_ENABLED, label = "Flag to enable cache." )
})
@Service
public class PaymentDelegateImpl implements IPaymentDelegate{
	
	
	@Reference
	private CheckinGetCartBehaviour checkinGetCartBehaviour;
	
	@Reference
	private CheckinInitPaymentBehaviour checkinInitPaymentBehaviour;
	
	@Reference
	private CheckinEndPaymentBehaviour checkinEndPaymentBehaviour;
	
	//Tolentino - Inizio
	@Reference
	private CheckinInitPaymentRecurringAdyenBehaviour checkinInitPaymentRecurringAdyenBehaviour;
	//Tolentino - Fine

	@Reference
	private  CheckinAuthorize3dBehaviour checkinAuthorize3dBehaviour;
	
	private static final Logger logger = LoggerFactory.getLogger(PaymentDelegateImpl.class);
	
	public static final String CACHE_REFRESH_INTERVAL = "cache.refresh.interval";
	public static final String IS_CACHE_ENABLED = "cache.is_enabled";
	
	private long refreshInterval = 3600000L;
	private boolean isCacheEnabled = false;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		try {		
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify PaymentDelegateImpl - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Activated PaymentDelegateImpl: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		try {
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify PaymentDelegateImpl - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Modified PaymentDelegateImpl: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}
	

	
	@Override
	public CheckinGetCartResponse getCart(CheckinGetCartRequest request) {
		CheckinGetCartResponse response = null;
		BehaviourExecutor<CheckinGetCartBehaviour, CheckinGetCartRequest, CheckinGetCartResponse> executor = new BehaviourExecutor<CheckinGetCartBehaviour, CheckinGetCartRequest, CheckinGetCartResponse>();
		try {
			logger.debug("Executing delegate getCart");
			response = executor.executeBehaviour(checkinGetCartBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[PaymentDelegateImpl - getCart] - Errore durante il servizio di Payment : getCart.", e);
		}
		return response;
	}

	@Override
	public CheckinInitPaymentResponse initPayment(CheckinInitPaymentRequest request) {
		CheckinInitPaymentResponse response = null;
		BehaviourExecutor<CheckinInitPaymentBehaviour, CheckinInitPaymentRequest, CheckinInitPaymentResponse> executor = new BehaviourExecutor<CheckinInitPaymentBehaviour, CheckinInitPaymentRequest, CheckinInitPaymentResponse>();
		try {
			logger.debug("Executing delegate initPayment");
			response = executor.executeBehaviour(checkinInitPaymentBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[PaymentDelegateImpl - initPayment] - Errore durante il servizio di Payment : initPayment.", e);
		}
		return response;
	}

	@Override
	public CheckinEndPaymentResponse endPayment(CheckinEndPaymentRequest request) {
		CheckinEndPaymentResponse response = null;
		BehaviourExecutor<CheckinEndPaymentBehaviour, CheckinEndPaymentRequest, CheckinEndPaymentResponse> executor = new BehaviourExecutor<CheckinEndPaymentBehaviour, CheckinEndPaymentRequest, CheckinEndPaymentResponse>();
		try {
			logger.debug("Executing delegate endPayment");
			response = executor.executeBehaviour(checkinEndPaymentBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[PaymentDelegateImpl - endPayment] - Errore durante il servizio di Payment : endPayment.", e);
		}
		return response;
	}

	//Tolentino - Inizio
	@Override
	public CheckinInitPaymentResponse initPaymentRecurringAdyen(CheckinInitPaymentRequest request) {
		CheckinInitPaymentResponse response = null;
		BehaviourExecutor<CheckinInitPaymentRecurringAdyenBehaviour, CheckinInitPaymentRequest, CheckinInitPaymentResponse> executor = new BehaviourExecutor<CheckinInitPaymentRecurringAdyenBehaviour, CheckinInitPaymentRequest, CheckinInitPaymentResponse>();
		try {
			logger.debug("MAPayment - Executing delegate initPaymentRecurringAdyen");
			response = executor.executeBehaviour(checkinInitPaymentRecurringAdyenBehaviour, request);
		}
		catch (Exception e) {
			logger.error("MAPayment - [PaymentDelegateImpl - initPaymentRecurringAdyen] - Errore durante il servizio di Payment : initPaymentRecurringAdyen.", e);
		}
		return response;
	}
	//Tolentino - Fine

	@Override
	public CheckinAuthorize3dResponse authorize3d(CheckinAuthorize3dRequest request) {
		CheckinAuthorize3dResponse response = null;
		BehaviourExecutor<CheckinAuthorize3dBehaviour, CheckinAuthorize3dRequest, CheckinAuthorize3dResponse> executor = new BehaviourExecutor<CheckinAuthorize3dBehaviour, CheckinAuthorize3dRequest, CheckinAuthorize3dResponse>();
		try {
			logger.debug("MAPayment - Executing delegate authorize3d");
			response = executor.executeBehaviour(checkinAuthorize3dBehaviour, request);
		} catch (Exception e) {
			logger.error("MAPayment - [PaymentDelegateImpl - authorize3d] - Errore durante il servizio di Payment : authorize3d.", e);
		}
		return response;
	}
}
