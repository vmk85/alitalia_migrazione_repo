package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinSelectedPassengerRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSelectedPassengerResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinSelectedPassengerBehaviour.class)
public class CheckinSelectedPassengerBehaviour extends Behaviour<CheckinSelectedPassengerRequest, CheckinSelectedPassengerResponse> {
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinSelectedPassengerResponse executeOrchestration(CheckinSelectedPassengerRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve SelectedPassenger request is null.");
		return checkinServiceRest.checkinSelectedPassenger(request);
	}

}
