package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.SendReminderCheckinRequest;
import com.alitalia.aem.common.messages.home.SendReminderCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinBoardingPassService;

@Component(immediate = true, metatype = false)
@Service(value = SendReminderBehaviour.class)
public class SendReminderBehaviour extends Behaviour<SendReminderCheckinRequest, SendReminderCheckinResponse> {

	@Reference
	private WebCheckinBoardingPassService webcheckinBoardingPassService;

	@Override
	public SendReminderCheckinResponse executeOrchestration(SendReminderCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("SendReminderCheckinRequest is null.");
		return webcheckinBoardingPassService.sendReminder(request);
	}

}
