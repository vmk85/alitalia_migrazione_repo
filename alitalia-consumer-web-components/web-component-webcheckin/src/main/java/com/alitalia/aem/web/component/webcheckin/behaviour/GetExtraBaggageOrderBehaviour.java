package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.ExtraBaggageOrderCheckinRequest;
import com.alitalia.aem.common.messages.home.ExtraBaggageOrderCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinExtraBaggageService;

@Component(immediate = true, metatype = false)
@Service(value = GetExtraBaggageOrderBehaviour.class)
public class GetExtraBaggageOrderBehaviour extends Behaviour<ExtraBaggageOrderCheckinRequest, ExtraBaggageOrderCheckinResponse> {

	@Reference
	private WebCheckinExtraBaggageService webcheckinExtraBaggageService;

	@Override
	public ExtraBaggageOrderCheckinResponse executeOrchestration(ExtraBaggageOrderCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("ExtraBaggageOrderCheckinRequest is null.");
		return webcheckinExtraBaggageService.getExtraBaggageOrder(request);
	}
}
