package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinIsBoardingPassPrintableRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinIsBoardingPassPrintableResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinBoardingPassPrintableBehaviour.class)
public class CheckinBoardingPassPrintableBehaviour extends Behaviour<CheckinIsBoardingPassPrintableRequest, CheckinIsBoardingPassPrintableResponse> {
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	protected CheckinIsBoardingPassPrintableResponse executeOrchestration(
			CheckinIsBoardingPassPrintableRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve boarding pass pritable is null.");
		return checkinServiceRest.retrieveBoardingPassPrintable(request);
	}

}
