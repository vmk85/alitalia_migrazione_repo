package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinUndoManyCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinService;

@Component(immediate = true, metatype = false)
@Service(value = UndoManyCheckinBehaviour.class)
public class UndoManyCheckinBehaviour extends Behaviour<WebCheckinUndoManyCheckinRequest, WebCheckinUndoManyCheckinResponse> {

	@Reference
	private WebCheckinService webcheckinService;
	
	@Override
	public WebCheckinUndoManyCheckinResponse executeOrchestration(WebCheckinUndoManyCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinUndoManyCheckinRequest is null.");
		return webcheckinService.undoManyCheckin(request);
	}

}
