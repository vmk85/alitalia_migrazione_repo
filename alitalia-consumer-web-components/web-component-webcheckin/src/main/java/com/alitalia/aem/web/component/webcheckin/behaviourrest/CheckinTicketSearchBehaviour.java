package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinTicketSearchBehaviour.class)
public class CheckinTicketSearchBehaviour extends Behaviour<CheckinTicketSearchRequest, CheckinTicketSearchResponse> {

	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinTicketSearchResponse executeOrchestration(CheckinTicketSearchRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve TicketSearch request is null.");
		return checkinServiceRest.retrieveCheckinTicket(request);
	}

}
