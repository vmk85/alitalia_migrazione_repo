package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryCartResponse;
import com.alitalia.aem.common.messages.home.WebCheckinGetCatalogRequest;
import com.alitalia.aem.service.api.home.WebCheckinAncillaryService;

@Component(immediate = true, metatype = false)
@Service(value = GetCatalogAncillaryBehaviour.class)
public class GetCatalogAncillaryBehaviour extends Behaviour<WebCheckinGetCatalogRequest, WebCheckinAncillaryCartResponse> {
	
	@Reference
	private WebCheckinAncillaryService webCheckinAncillaryService;
	
	@Override
	public WebCheckinAncillaryCartResponse executeOrchestration(WebCheckinGetCatalogRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinGetCatalogRequest is null.");
		return webCheckinAncillaryService.getCatalog(request);
	}
}