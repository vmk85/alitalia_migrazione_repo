package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerRequest;
import com.alitalia.aem.common.messages.home.WebCheckinNewPassengerResponse;
import com.alitalia.aem.service.api.home.WebCheckinPassengerService;

@Component(immediate = true, metatype = false)
@Service(value = NewPassengerBehaviour.class)
public class NewPassengerBehaviour extends Behaviour<WebCheckinNewPassengerRequest, WebCheckinNewPassengerResponse> {
	
	@Reference
	private WebCheckinPassengerService webcheckinPassengerService;
	
	@Override
	public WebCheckinNewPassengerResponse executeOrchestration(WebCheckinNewPassengerRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinNewPassengerRequest is null.");
		return webcheckinPassengerService.getNewPassenger(request);
	}

}
