package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinUndoCheckinRequest;
import com.alitalia.aem.common.messages.home.WebCheckinUndoCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinService;

@Component(immediate = true, metatype = false)
@Service(value = UndoCheckinBehaviour.class)
public class UndoCheckinBehaviour extends Behaviour<WebCheckinUndoCheckinRequest, WebCheckinUndoCheckinResponse> {

	@Reference
	private WebCheckinService webcheckinService;
	
	@Override
	public WebCheckinUndoCheckinResponse executeOrchestration(WebCheckinUndoCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinUndoCheckinRequest is null.");
		return webcheckinService.undoCheckin(request);
	}

}
