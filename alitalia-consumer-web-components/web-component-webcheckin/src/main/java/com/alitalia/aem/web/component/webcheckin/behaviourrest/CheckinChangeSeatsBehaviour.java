package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinChangeSeatsRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinChangeSeatsResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinChangeSeatsBehaviour.class)
public class CheckinChangeSeatsBehaviour extends Behaviour<CheckinChangeSeatsRequest, CheckinChangeSeatsResponse> {
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinChangeSeatsResponse executeOrchestration(CheckinChangeSeatsRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve Boarding Pass request is null.");
		return checkinServiceRest.changeSeats(request);
	}
}
