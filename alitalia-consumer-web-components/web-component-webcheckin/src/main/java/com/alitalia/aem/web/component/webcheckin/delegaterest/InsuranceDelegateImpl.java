package com.alitalia.aem.web.component.webcheckin.delegaterest;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceResponse;
import com.alitalia.aem.common.messages.checkinrest.GetInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.GetInsuranceResponse;
import com.alitalia.aem.common.messages.checkinrest.SetInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.SetInsuranceResponse;
import com.alitalia.aem.web.component.webcheckin.behaviourrest.DeleteInsuranceBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviourrest.GetInsuranceBehaviour;
import com.alitalia.aem.web.component.webcheckin.behaviourrest.SetInsuranceBehaviour;

@Component(immediate=true, metatype=false)
@Properties({
	@Property( name = CheckinDelegateImpl.CACHE_REFRESH_INTERVAL, label = "Refresh interval of the cache." ),
	@Property( name = CheckinDelegateImpl.IS_CACHE_ENABLED, label = "Flag to enable cache." )
})
@Service
public class InsuranceDelegateImpl implements IInsuranceDelegate {
	
	@Reference
	private GetInsuranceBehaviour getInsuranceBehaviour;
	
	@Reference
	private SetInsuranceBehaviour setInsuranceBehaviour;
	
	@Reference
	private DeleteInsuranceBehaviour deleteInsuranceBehaviour;
	
	private static final Logger logger = LoggerFactory.getLogger(CheckinDelegateImpl.class);
	
	public static final String CACHE_REFRESH_INTERVAL = "cache.refresh.interval";
	public static final String IS_CACHE_ENABLED = "cache.is_enabled";
	
	private long refreshInterval = 3600000L;
	private boolean isCacheEnabled = false;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		try {		
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify InsuranceDelegateImpl - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Activated InsuranceDelegateImpl: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		try {
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify InsuranceDelegateImpl - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Modified InsuranceDelegateImpl: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}
	

	@Override
	public GetInsuranceResponse getInsurance(GetInsuranceRequest request) {
		GetInsuranceResponse response = null;
		BehaviourExecutor<GetInsuranceBehaviour, GetInsuranceRequest, GetInsuranceResponse> executor = new BehaviourExecutor<GetInsuranceBehaviour, GetInsuranceRequest, GetInsuranceResponse>();
		try {
			logger.debug("Executing delegate getInsurance");
			response = executor.executeBehaviour(getInsuranceBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[DelegateInsuranceImpl - getInsurance] - Errore durante il get delle informazioni Insurance.", e);
		}
		return response;
	}

	@Override
	public SetInsuranceResponse setInsurance(SetInsuranceRequest request) {
		SetInsuranceResponse response = null;
		BehaviourExecutor<SetInsuranceBehaviour, SetInsuranceRequest, SetInsuranceResponse> executor = new BehaviourExecutor<SetInsuranceBehaviour, SetInsuranceRequest, SetInsuranceResponse>();
		try {
			logger.debug("Executing delegate setInsurance");
			response = executor.executeBehaviour(setInsuranceBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[DelegateInsuranceImpl - setInsurance] - Errore durante il set delle informazioni Insurance.", e);
		}
		return response;
	}

	@Override
	public DeleteInsuranceResponse deleteInsurance(DeleteInsuranceRequest request) {
		DeleteInsuranceResponse response = null;
		BehaviourExecutor<DeleteInsuranceBehaviour, DeleteInsuranceRequest, DeleteInsuranceResponse> executor = new BehaviourExecutor<DeleteInsuranceBehaviour, DeleteInsuranceRequest, DeleteInsuranceResponse>();
		try {
//			logger.debug("Executing delegate deleteInsurance");
			response = executor.executeBehaviour(deleteInsuranceBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[DelegateInsuranceImpl - deleteInsurance] - Errore durante la cancellazione delle informazioni Insurance.", e);
		}
		return response;
	}

}
