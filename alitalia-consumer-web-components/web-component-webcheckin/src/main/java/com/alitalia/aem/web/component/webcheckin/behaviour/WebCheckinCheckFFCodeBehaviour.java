package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesResponse;
import com.alitalia.aem.service.api.home.WebCheckinPassengerService;

@Component(immediate = true, metatype = false)
@Service(value = WebCheckinCheckFFCodeBehaviour.class)
public class WebCheckinCheckFFCodeBehaviour extends Behaviour<MmbCheckFrequentFlyerCodesRequest, MmbCheckFrequentFlyerCodesResponse> {

	@Reference
	private WebCheckinPassengerService webcheckinPassengerService;
	
	@Override
	public MmbCheckFrequentFlyerCodesResponse executeOrchestration(MmbCheckFrequentFlyerCodesRequest request) {
		if (request == null)
			throw new IllegalArgumentException("MmbCheckFrequentFlyerCodesRequest is null.");
		return webcheckinPassengerService.checkFFCode(request);
	}
}