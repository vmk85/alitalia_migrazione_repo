package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.BoardingPassCheckinRequest;
import com.alitalia.aem.common.messages.home.BoardingPassCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinBoardingPassService;

@Component(immediate = true, metatype = false)
@Service(value = GetBoardingPassBehaviour.class)
public class GetBoardingPassBehaviour extends Behaviour<BoardingPassCheckinRequest, BoardingPassCheckinResponse> {
	
	@Reference
	private WebCheckinBoardingPassService webcheckinBoardingPassService;
	
	@Override
	public BoardingPassCheckinResponse executeOrchestration(BoardingPassCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("BoardingPassCheckinRequest is null.");
		return webcheckinBoardingPassService.getBoardingPass(request);
	}

}
