package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinOffloadResponse;
import com.alitalia.aem.service.api.home.ICheckinService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

/**
 * Created by ggadaleta on 05/02/2018.
 */

@Component(immediate = true, metatype = false)
@Service(value = CheckinOffloadBehaviour.class)
public class CheckinOffloadBehaviour extends Behaviour<CheckinOffloadRequest, CheckinOffloadResponse> {

    @Reference
    private ICheckinService checkinServiceRest;

    @Override
    public CheckinOffloadResponse executeOrchestration(CheckinOffloadRequest request) {
        if(request == null)
            throw new IllegalArgumentException("Checkin Offload request is null.");
        return checkinServiceRest.offload(request);
    }
}
