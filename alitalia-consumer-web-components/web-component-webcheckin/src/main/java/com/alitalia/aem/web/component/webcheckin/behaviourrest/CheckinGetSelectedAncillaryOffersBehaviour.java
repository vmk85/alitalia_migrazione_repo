package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSelectedAncillaryOffersRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSelectedAncillaryOffersResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinGetSelectedAncillaryOffersBehaviour.class)
public class CheckinGetSelectedAncillaryOffersBehaviour extends Behaviour<CheckinGetSelectedAncillaryOffersRequest, CheckinGetSelectedAncillaryOffersResponse>{
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	protected CheckinGetSelectedAncillaryOffersResponse executeOrchestration(
			CheckinGetSelectedAncillaryOffersRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve selected ancillary request is null.");
		return checkinServiceRest.retrieveSelectedAncillaryOffers(request);
	}

}
