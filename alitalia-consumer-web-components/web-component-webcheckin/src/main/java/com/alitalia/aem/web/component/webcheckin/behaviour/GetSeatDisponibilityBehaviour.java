package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.GetSeatDisponibilityCheckinRequest;
import com.alitalia.aem.common.messages.home.GetSeatDisponibilityCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinSeatMapService;

@Component(immediate = true, metatype = false)
@Service(value = GetSeatDisponibilityBehaviour.class)
public class GetSeatDisponibilityBehaviour extends Behaviour<GetSeatDisponibilityCheckinRequest, GetSeatDisponibilityCheckinResponse> {
	
	@Reference
	private WebCheckinSeatMapService webcheckinSeatMapService;
	
	@Override
	public GetSeatDisponibilityCheckinResponse executeOrchestration(GetSeatDisponibilityCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("GetSeatDisponibilityCheckinRequest is null.");
		return webcheckinSeatMapService.getSeatDisponibility(request);
	}

}
