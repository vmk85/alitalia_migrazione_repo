package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinRequest;
import com.alitalia.aem.common.messages.home.CanAddPassengerCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinPassengerService;

@Component(immediate = true, metatype = false)
@Service(value = CanAddPassengerBehaviour.class)
public class CanAddPassengerBehaviour extends Behaviour<CanAddPassengerCheckinRequest, CanAddPassengerCheckinResponse> {
	
	@Reference
	private WebCheckinPassengerService webcheckinPassengerService;
	
	@Override
	public CanAddPassengerCheckinResponse executeOrchestration(CanAddPassengerCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("CanAddPassengerCheckinRequest is null.");
		return webcheckinPassengerService.canAddPassenger(request);
	}

}
