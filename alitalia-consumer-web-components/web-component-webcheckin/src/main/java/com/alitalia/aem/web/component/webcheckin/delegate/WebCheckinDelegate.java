package com.alitalia.aem.web.component.webcheckin.delegate;

import com.alitalia.aem.common.messages.BaseRequest;
import com.alitalia.aem.common.messages.BaseResponse;
import com.alitalia.aem.common.messages.home.*;

public interface WebCheckinDelegate {
	
	//Passenger Service
	PassengerListCheckinResponse getPassengerList(PassengerListCheckinRequest request);
	CanAddPassengerCheckinResponse canAddPassenger(CanAddPassengerCheckinRequest request);
	WebCheckinNewPassengerResponse getNewPassenger(WebCheckinNewPassengerRequest request);
	MmbCheckFrequentFlyerCodesResponse checkFFCode(MmbCheckFrequentFlyerCodesRequest request);

	// Search Service
	RetrievePnrInformationCheckinResponse retrievePnrInformation(RetriveMmbPnrInformationRequest request);
	ValidateMmUserCheckinResponse validateMmUser(ValidateMmUserCheckinRequest request);
	
	// Checkin Service
	WebCheckinResponse checkIn(WebCheckinRequest request);
	WebCheckinUndoCheckinResponse undoCheckin(WebCheckinUndoCheckinRequest request);
	WebCheckinUndoManyCheckinResponse undoManyCheckin(WebCheckinUndoManyCheckinRequest request);
	
	// Static data Service
	GetCountryResponse getCountries(GetCountryRequest request);
	RetrievePhonePrefixResponse getPhonePrefixes(GetCountryRequest request);
	WebCheckinBPPermissionResponse getBoardingPassPermissions(WebCheckinBPPermissionRequest request);
	WebCheckinFrequentFlyerResponse getFrequentFlyers(GetCountryRequest request);
	WebCheckinStatesResponse getAmericanStates(BaseRequest request);
	WebCheckinStatesResponse getStates(WebCheckinStatesRequest request);
	WebCheckinAirportNameResponse getAirportNames(WebCheckinAirportNameRequest request);
	
	//BoardingPassService
	BoardingPassCheckinResponse getBoardingPass(BoardingPassCheckinRequest request);
	SendBoardingPassCheckinResponse sendBoardingPass(SendBoardingPassCheckinRequest request);
	SendReminderCheckinResponse sendReminder(SendReminderCheckinRequest request);
	
	CheckinInsuranceResponse getInsuranceInfo(WebCheckinGetInsuranceRequest request);
	CheckinInsuranceResponse payInsurance(WebCheckinPayInsuranceRequest request);
	
	//Ancillary Service
	WebCheckinAncillaryCartResponse getCatalog(WebCheckinGetCatalogRequest request);
	WebCheckinAncillaryResponse addToCart(WebCheckinAddToCartRequest request);
	WebCheckinAncillaryResponse updateCart(WebCheckinUpdateCartRequest request);
	WebCheckinAncillaryResponse removeFromCart(WebCheckinRemoveFromCartRequest request);
	WebCheckinAncillaryCartResponse getCart(WebCheckinGetCartRequest request);
	MmbAncillaryCheckOutInitResponse checkOutInit(MmbAncillaryCheckOutInitRequest request);
	MmbAncillaryCheckOutStatusResponse getCheckOutStatus(MmbAncillaryCheckOutStatusRequest request);
	WebCheckinCancelCartResponse cancelCart(WebCheckinCancelCartRequest request);
	
	//ExtraBaggageService
	ExtraBaggageOrderCheckinResponse getExtraBaggageOrder(ExtraBaggageOrderCheckinRequest request);
	ExtraBaggagePolicyCheckinResponse getBaggagePolicy(ExtraBaggagePolicyCheckinRequest request);
	
	//PaymentService
	PaymentCheckinResponse executePayment(PaymentCheckinRequest request);
	GenericServiceResponse sendEmailReceipt(SendReceiptCheckinRequest request);
	
	//EMDService
	WebCheckinEmdResponse extraBaggageEmd(WebCheckinEmdRequest request);
	WebCheckinEmdResponse comfortSeatEmd(WebCheckinEmdRequest request);
	WebCheckinEmdResponse cabinUpgradeEmd(WebCheckinEmdRequest request);
	WebCheckinReasonForIssuanceResponse getReasonForIssuance(WebCheckinReasonForIssuanceRequest request);
	
	//ComfortSeatService
	ComfortSeatPurchaseCheckinResponse selectComfortSeatPurchase(ComfortSeatPurchaseCheckinRequest request);
	ComfortSeatPriceCheckinResponse getComfortSeatPrice(ComfortSeatPriceCheckinRequest request);
	GenericServiceResponse existsComfortSeatPurchaseIntoBO(ComfortSeatOrderCheckinRequest request);
	BaseResponse updateComfortSeatPurchaseIntoBO(ComfortSeatOrderUpdateCheckinRequest request);
	
	//SeatMapService
	GetSeatMapCheckinResponse getSeatMap(GetSeatMapCheckinRequest request);
	GetSeatDisponibilityCheckinResponse getSeatDisponibility(GetSeatDisponibilityCheckinRequest request);
	ChangeSeatCheckinResponse changeSeat(ChangeSeatCheckinRequest request);
	ChangeCabinCheckinResponse changeCabin(ChangeCabinCheckinRequest request);
	UpgradePolicyCheckinResponse getUpgradePolicy(UpgradePolicyCheckinRequest request);
	
	//AlertService
	WebCheckinAlertResponse submitUser(WebCheckinAlertRequest request);

	CheckinSendSmsResponse sendSms(CheckinSendSmsRequest request);
}