package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinPnrSearchResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinPnrSearchBehaviour.class)
public class CheckinPnrSearchBehaviour extends Behaviour<CheckinPnrSearchRequest, CheckinPnrSearchResponse> {
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinPnrSearchResponse executeOrchestration(CheckinPnrSearchRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve PnrSearch request is null.");
		return checkinServiceRest.retrieveCheckinPnr(request);
	}

}
