package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetCartRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetCartResponse;
import com.alitalia.aem.service.api.home.ICheckinPaymentRestService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinGetCartBehaviour.class)
public class CheckinGetCartBehaviour extends Behaviour<CheckinGetCartRequest, CheckinGetCartResponse> {
	
	@Reference
	private ICheckinPaymentRestService checkinPaymentRestService;
	
	@Override
	public CheckinGetCartResponse executeOrchestration(CheckinGetCartRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("getCart request is null.");
		return checkinPaymentRestService.getCart(request);
	}


}
