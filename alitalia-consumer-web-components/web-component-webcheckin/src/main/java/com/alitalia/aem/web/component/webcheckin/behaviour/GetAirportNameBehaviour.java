package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinAirportNameRequest;
import com.alitalia.aem.common.messages.home.WebCheckinAirportNameResponse;
import com.alitalia.aem.service.api.home.WebCheckinStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = GetAirportNameBehaviour.class)
public class GetAirportNameBehaviour extends Behaviour<WebCheckinAirportNameRequest, WebCheckinAirportNameResponse> {
	
	@Reference
	private WebCheckinStaticDataService webcheckinStaticDataService;
	
	@Override
	public WebCheckinAirportNameResponse executeOrchestration(WebCheckinAirportNameRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinAirportNameRequest is null.");
		return webcheckinStaticDataService.getAirportNames(request);
	}
}