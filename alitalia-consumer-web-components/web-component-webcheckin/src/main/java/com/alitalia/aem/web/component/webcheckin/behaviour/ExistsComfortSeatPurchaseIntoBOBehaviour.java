package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.ComfortSeatOrderCheckinRequest;
import com.alitalia.aem.common.messages.home.GenericServiceResponse;
import com.alitalia.aem.service.api.home.WebCheckinComfortSeatService;

@Component(immediate = true, metatype = false)
@Service(value = ExistsComfortSeatPurchaseIntoBOBehaviour.class)
public class ExistsComfortSeatPurchaseIntoBOBehaviour extends Behaviour<ComfortSeatOrderCheckinRequest, GenericServiceResponse> {
	
	@Reference
	private WebCheckinComfortSeatService webcheckinComfortSeatService;
	
	@Override
	public GenericServiceResponse executeOrchestration(ComfortSeatOrderCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("ComfortSeatOrderCheckinRequest is null.");
		return webcheckinComfortSeatService.existsComfortSeatPurchaseIntoBO(request);
	}

}
