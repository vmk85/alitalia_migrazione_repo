package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.PaymentCheckinRequest;
import com.alitalia.aem.common.messages.home.PaymentCheckinResponse;
import com.alitalia.aem.service.api.home.WebCheckinPaymentService;

@Component(immediate = true, metatype = false)
@Service(value = ExecutePaymentBehaviour.class)
public class ExecutePaymentBehaviour extends Behaviour<PaymentCheckinRequest, PaymentCheckinResponse> {
	
	@Reference
	private WebCheckinPaymentService webcheckinPaymentService;
	
	@Override
	public PaymentCheckinResponse executeOrchestration(PaymentCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("PaymentCheckinRequest is null.");
		return webcheckinPaymentService.executePayment(request);
	}

}
