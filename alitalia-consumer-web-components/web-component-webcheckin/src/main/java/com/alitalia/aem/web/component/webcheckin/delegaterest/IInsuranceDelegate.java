package com.alitalia.aem.web.component.webcheckin.delegaterest;

import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.DeleteInsuranceResponse;
import com.alitalia.aem.common.messages.checkinrest.GetInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.GetInsuranceResponse;
import com.alitalia.aem.common.messages.checkinrest.SetInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.SetInsuranceResponse;

public interface IInsuranceDelegate {
	
	GetInsuranceResponse getInsurance(GetInsuranceRequest request);
	SetInsuranceResponse setInsurance(SetInsuranceRequest request);
	DeleteInsuranceResponse deleteInsurance(DeleteInsuranceRequest request);

}
