package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetBoardingPassInfoRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetBoardingPassInfoResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinGetBoardingPassInfoBehaviour.class)
public class CheckinGetBoardingPassInfoBehaviour extends Behaviour<CheckinGetBoardingPassInfoRequest, CheckinGetBoardingPassInfoResponse> {
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	protected CheckinGetBoardingPassInfoResponse executeOrchestration(CheckinGetBoardingPassInfoRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve boarding pass info request is null.");
		return checkinServiceRest.getBoardingPassInfo(request);
	}

}
