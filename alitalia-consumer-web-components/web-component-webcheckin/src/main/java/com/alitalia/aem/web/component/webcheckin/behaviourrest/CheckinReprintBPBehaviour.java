package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinReprintBPRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinReprintBPResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinReprintBPBehaviour.class)
public class CheckinReprintBPBehaviour  extends Behaviour<CheckinReprintBPRequest, CheckinReprintBPResponse> {
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinReprintBPResponse executeOrchestration(CheckinReprintBPRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve Boarding Pass request is null.");
		return checkinServiceRest.retrieveCheckinNewBP(request);
	}

}
