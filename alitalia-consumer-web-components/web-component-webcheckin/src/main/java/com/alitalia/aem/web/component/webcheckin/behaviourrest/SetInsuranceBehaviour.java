package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.SetInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.SetInsuranceResponse;
import com.alitalia.aem.service.api.home.IServiceInsurance;

@Component(immediate = true, metatype = false)
@Service(value = SetInsuranceBehaviour.class)
public class SetInsuranceBehaviour extends Behaviour<SetInsuranceRequest, SetInsuranceResponse> {
	
	@Reference
	private IServiceInsurance serviceInsurance;

	@Override
	protected SetInsuranceResponse executeOrchestration(SetInsuranceRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Set Insurance request is null.");
		return serviceInsurance.setInsurance(request);
	}
	
}
