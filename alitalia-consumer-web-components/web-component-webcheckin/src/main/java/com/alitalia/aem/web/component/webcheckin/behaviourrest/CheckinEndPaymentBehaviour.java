package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinEndPaymentRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinEndPaymentResponse;
import com.alitalia.aem.service.api.home.ICheckinPaymentRestService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinEndPaymentBehaviour.class)
public class CheckinEndPaymentBehaviour extends Behaviour<CheckinEndPaymentRequest, CheckinEndPaymentResponse> {
	
	@Reference
	private ICheckinPaymentRestService checkinPaymentRestService;
	
	@Override
	public CheckinEndPaymentResponse executeOrchestration(CheckinEndPaymentRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("endPayment request is null.");
		return checkinPaymentRestService.endPayment(request);
	}
}
