package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinCreateBoardingPassRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinCreateBoardingPassResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinCreateBoardingPassBehaviour.class)
public class CheckinCreateBoardingPassBehaviour extends Behaviour<CheckinCreateBoardingPassRequest, CheckinCreateBoardingPassResponse> {

	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	protected CheckinCreateBoardingPassResponse executeOrchestration(
			CheckinCreateBoardingPassRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve boarding pass request is null.");
		return checkinServiceRest.retrieveBoardingPass(request);
	}

}
