package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinFrequentFlyerSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinFrequentFlyerSearchResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinFrequentFlyerSearchBehaviour.class)
public class CheckinFrequentFlyerSearchBehaviour extends Behaviour<CheckinFrequentFlyerSearchRequest, CheckinFrequentFlyerSearchResponse> {

	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinFrequentFlyerSearchResponse executeOrchestration(CheckinFrequentFlyerSearchRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve FrequentFlyerSearch request is null.");
		return checkinServiceRest.retrieveCheckinFrequentFlyer(request);
	}

}
