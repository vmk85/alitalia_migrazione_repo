package com.alitalia.aem.web.component.webcheckin.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.WebCheckinAncillaryResponse;
import com.alitalia.aem.common.messages.home.WebCheckinRemoveFromCartRequest;
import com.alitalia.aem.service.api.home.WebCheckinAncillaryService;

@Component(immediate = true, metatype = false)
@Service(value = RemoveFromCartAncillaryBehaviour.class)
public class RemoveFromCartAncillaryBehaviour extends Behaviour<WebCheckinRemoveFromCartRequest, WebCheckinAncillaryResponse> {
	
	@Reference
	private WebCheckinAncillaryService webCheckinAncillaryService;
	
	@Override
	public WebCheckinAncillaryResponse executeOrchestration(WebCheckinRemoveFromCartRequest request) {
		if (request == null)
			throw new IllegalArgumentException("WebCheckinRemoveFromCartRequest is null.");
		return webCheckinAncillaryService.removeFromCart(request);
	}
}