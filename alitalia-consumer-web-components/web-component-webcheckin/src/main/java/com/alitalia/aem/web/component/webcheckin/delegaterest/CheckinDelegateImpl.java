package com.alitalia.aem.web.component.webcheckin.delegaterest;

import com.alitalia.aem.common.messages.checkinrest.*;
import com.alitalia.aem.web.component.webcheckin.behaviourrest.*;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;


@Component(immediate=true, metatype=false)
@Properties({
	@Property( name = CheckinDelegateImpl.CACHE_REFRESH_INTERVAL, label = "Refresh interval of the cache." ),
	@Property( name = CheckinDelegateImpl.IS_CACHE_ENABLED, label = "Flag to enable cache." )
})
@Service
public class CheckinDelegateImpl implements ICheckinDelegate{

	@Reference
	private CheckinSetSmsDataBehaviour checkinSetSmsDataBehaviour;

	@Reference
	private CheckinGetSmsDataBehaviour checkinGetSmsDataBehaviour;
	
	@Reference
	private CheckinTripSearchBehaviour checkinTripSearchBehaviour;
	
	@Reference
	private CheckinPnrSearchBehaviour checkinPnrSearchBehaviour;
	
	@Reference
	private CheckinGetPassengerDataBehaviour checkinGetPassengerDataBehaviour;
	
	@Reference
	private CheckinTicketSearchBehaviour checkinTicketSearchBehaviour;

	@Reference
	private CheckinAddPassengerBehaviour checkinAddPassengerBehaviour;

	@Reference
	private CheckinFrequentFlyerSearchBehaviour checkinFrequentFlyerSearchBehaviour;
	
	@Reference
	private CheckinSelectedPassengerBehaviour checkinSelectedPassengerBehaviour;
	
	@Reference
	private CheckinFlightDetailBehaviour checkinFlightDetailBehaviour;
	
	@Reference 
	private CheckinPassengerBehaviour checkinPassengerBehaviour;
	
	@Reference
	private CheckinReprintBPBehaviour checkinReprintBPBehaviour;
	
	@Reference
	private CheckinBoardingPassPrintableBehaviour checkinBoardingPassPrintableBehaviour;
	
	@Reference
	private CheckinCreateBoardingPassBehaviour checkinCreateBoardingPassBehaviour;
	
	@Reference
	private CheckinGetBoardingPassInfoBehaviour checkinGetBoardingPassInfoBehaviour;

	@Reference
	private CheckinGetBoardingPassInfoByNameBehaviour checkinGetBoardingPassInfoByNameBehaviour;
	
	@Reference
	private CheckinSetBaggageBehaviour checkinSetBaggageBehaviour;
	
	@Reference
	private CheckinGetSelectedAncillaryOffersBehaviour checkinGetSelectedAncillaryOffersBehaviour;
	
	@Reference
	private CheckinEnhancedSeatMapBehaviour checkinEnhancedSeatMapBehaviour;
	
	@Reference
	private CheckinChangeSeatsBehaviour checkinChangeSeatsBehaviour;
	
	@Reference
	private CheckinUpdateSecurityInfoBehaviour checkinUpdateSecurityInfoBehaviour;
	
	@Reference
	private CheckinSetFastTrackBehaviour checkinSetFastTrackBehaviour;
	
	@Reference
	private CheckinSetLoungeBehaviour checkinSetLoungeBehaviour;

	@Reference
	private CheckinClearAncillariesCartBehaviour checkinClearAncillariesCartBehaviour;
	
	@Reference
	private CheckinUpdatePassengerDetailsBehaviour checkinUpdatePassengerDetailsBehaviour;
	
	@Reference
	private CheckinUpdatePassengerBehaviour checkinUpdatePassengerBehaviour;
	
	@Reference
	private CheckinClearAncillarySessionEndBehaviour checkinClearAncillarySessionEndBehaviour;

	@Reference
	private CheckinOffloadBehaviour checkinOffloadBehaviour;

	@Reference
	private CheckinOffloadFlightsBehaviour checkinOffloadFlightsBehaviour;

	private static final Logger logger = LoggerFactory.getLogger(CheckinDelegateImpl.class);
	
	public static final String CACHE_REFRESH_INTERVAL = "cache.refresh.interval";
	public static final String IS_CACHE_ENABLED = "cache.is_enabled";
	
	private long refreshInterval = 3600000L;
	private boolean isCacheEnabled = false;
	
	@Activate
	private void activate(ComponentContext componentContext) {
		try {		
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify CheckinDelegateImpl - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Activated CheckinDelegateImpl: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}

	@Modified
	private void modified(ComponentContext componentContext) {
		try {
			isCacheEnabled = PropertiesUtil.toBoolean(
					componentContext.getProperties().get(IS_CACHE_ENABLED), false);
			refreshInterval = PropertiesUtil.toLong(
					componentContext.getProperties().get(CACHE_REFRESH_INTERVAL), 3600000);
		} catch (Exception e) {
			logger.error("Error to modify CheckinDelegateImpl - Load default value", e);
			isCacheEnabled = false;			
		}

		logger.info("Modified CheckinDelegateImpl: "
				+ "IS_CACHE_ENABLED=[" + isCacheEnabled + "], "
				+ "CACHE_REFRESH_INTERVAL=[" + refreshInterval + "]");
	}
	
	@Override
	public CheckinTripSearchResponse retrieveCheckinTrip(CheckinTripSearchRequest request) {
		CheckinTripSearchResponse response = null;
		BehaviourExecutor<CheckinTripSearchBehaviour, CheckinTripSearchRequest, CheckinTripSearchResponse> executor = new BehaviourExecutor<CheckinTripSearchBehaviour, CheckinTripSearchRequest, CheckinTripSearchResponse>();
		
		try{
			logger.debug("Executing delegate retrieveCheckinTrip");
			response = executor.executeBehaviour(checkinTripSearchBehaviour, request);
		}
		
		catch(Exception e){
			logger.error("[CheckinDelegateImpl] - Errore durante il recupero delle informazioni sul viaggio.", e);
		}
		
		return response;
	}

	@Override
	public CheckinGetPassengerDataResponse retrieveCheckinPassengerData(CheckinGetPassengerDataRequest request) {
		CheckinGetPassengerDataResponse response = null;
		BehaviourExecutor<CheckinGetPassengerDataBehaviour, CheckinGetPassengerDataRequest, CheckinGetPassengerDataResponse> executor = new BehaviourExecutor<CheckinGetPassengerDataBehaviour, CheckinGetPassengerDataRequest, CheckinGetPassengerDataResponse>();
		try {
			logger.debug("Executing delegate retrieveCheckinPassengerData");
			response = executor.executeBehaviour(checkinGetPassengerDataBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - retrieveCheckinPassengerData] - Errore durante il recupero delle informazioni dei dati del passeggero.", e);
		}
		return response;
	}
	
	@Override
	public CheckinPnrSearchResponse retrieveCheckinPnr(CheckinPnrSearchRequest request) {
		CheckinPnrSearchResponse response = null;
		BehaviourExecutor<CheckinPnrSearchBehaviour, CheckinPnrSearchRequest, CheckinPnrSearchResponse> executor = new BehaviourExecutor<CheckinPnrSearchBehaviour, CheckinPnrSearchRequest, CheckinPnrSearchResponse>();
		
		try {
			logger.debug("Executing delegate retrieveCheckinPnr");
			response = executor.executeBehaviour(checkinPnrSearchBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - retrieveCheckinPnr] - Errore durante il recupero delle informazioni di viaggio.", e);
		}
		
		return response;
	}

	@Override
	public CheckinAddPassengerResponse retrieveCheckinAddPassenger(CheckinAddPassengerRequest request) {
		CheckinAddPassengerResponse response = null;
		BehaviourExecutor<CheckinAddPassengerBehaviour, CheckinAddPassengerRequest, CheckinAddPassengerResponse> executor = new BehaviourExecutor<CheckinAddPassengerBehaviour, CheckinAddPassengerRequest, CheckinAddPassengerResponse>();

		try {
			logger.debug("Executing delegate retrieveAddPassenger");
			response = executor.executeBehaviour(checkinAddPassengerBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - retrieveAddPassenger] - Errore durante il recupero delle informazioni di viaggio.", e);
		}

		return response;
	}

	@Override
	public CheckinTicketSearchResponse retrieveCheckinTicket(CheckinTicketSearchRequest request) {
		CheckinTicketSearchResponse response = null;
		BehaviourExecutor<CheckinTicketSearchBehaviour, CheckinTicketSearchRequest, CheckinTicketSearchResponse> executor = new BehaviourExecutor<CheckinTicketSearchBehaviour, CheckinTicketSearchRequest, CheckinTicketSearchResponse>();
		
		try {
			logger.debug("Executing delegate retrieveCheckinTicket");
			response = executor.executeBehaviour(checkinTicketSearchBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - retrieveCheckinTicketSearch] - Errore durante il recupero delle informazioni di viaggio.", e);
		}
		
		return response;
	}

	@Override
	public CheckinFrequentFlyerSearchResponse retrieveCheckinFrequentFlyer(CheckinFrequentFlyerSearchRequest request) {
		CheckinFrequentFlyerSearchResponse response = null;
		BehaviourExecutor<CheckinFrequentFlyerSearchBehaviour, CheckinFrequentFlyerSearchRequest, CheckinFrequentFlyerSearchResponse> executor = new BehaviourExecutor<CheckinFrequentFlyerSearchBehaviour, CheckinFrequentFlyerSearchRequest, CheckinFrequentFlyerSearchResponse>();
		
		try {
			logger.debug("Executing delegate retrieveCheckinFrequentFlyer");
			response = executor.executeBehaviour(checkinFrequentFlyerSearchBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - retrieveCheckinFrequentFlyerSearch] - Errore durante il recupero delle informazioni di viaggio.", e);
		}
		return response;
	}

	@Override
	public CheckinSelectedPassengerResponse retrieveCheckinSelectedPassenger(CheckinSelectedPassengerRequest request) {
		CheckinSelectedPassengerResponse response = null;
		BehaviourExecutor<CheckinSelectedPassengerBehaviour, CheckinSelectedPassengerRequest, CheckinSelectedPassengerResponse> executor = new BehaviourExecutor<CheckinSelectedPassengerBehaviour, CheckinSelectedPassengerRequest, CheckinSelectedPassengerResponse>();
		
		try {
			logger.debug("Executing delegate retrieveCheckinSelectedPassenger");
			response = executor.executeBehaviour(checkinSelectedPassengerBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - retrieveCheckinSelectedPassenger] - Errore durante il recupero delle informazioni di viaggio.", e);
		}
		return response;
	}

	@Override
	public CheckinFlightDetailResponse retrieveCheckinFlightDetail(CheckinFlightDetailRequest request) {
		CheckinFlightDetailResponse response = null;
		BehaviourExecutor<CheckinFlightDetailBehaviour, CheckinFlightDetailRequest, CheckinFlightDetailResponse> executor = new BehaviourExecutor<CheckinFlightDetailBehaviour, CheckinFlightDetailRequest, CheckinFlightDetailResponse>();
		try {
			logger.debug("Executing delegate retrieveCheckinFlightDetail");
			response = executor.executeBehaviour(checkinFlightDetailBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - retrieveCheckinFlightDetail] - Errore durante il recupero delle informazioni dei dati del volo.", e);
		}
		return response;
	}

	@Override
	public CheckinPassengerResponse retrieveCheckinPassenger(CheckinPassengerRequest request) {
		CheckinPassengerResponse response = null;
		BehaviourExecutor<CheckinPassengerBehaviour, CheckinPassengerRequest, CheckinPassengerResponse> executor = new BehaviourExecutor<CheckinPassengerBehaviour, CheckinPassengerRequest, CheckinPassengerResponse>();
		try {
			logger.debug("Executing delegate retrieveCheckinPassenger");
			response = executor.executeBehaviour(checkinPassengerBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - retrieveCheckinPassenger] - Errore durante il recupero delle informazioni dei dati volo e passeggero.", e);
		}
		return response;
	}
	
	@Override
	public CheckinReprintBPResponse retrieveNewBP(CheckinReprintBPRequest request) {
		CheckinReprintBPResponse response = null;
		BehaviourExecutor<CheckinReprintBPBehaviour, CheckinReprintBPRequest, CheckinReprintBPResponse> executor = new BehaviourExecutor<CheckinReprintBPBehaviour, CheckinReprintBPRequest, CheckinReprintBPResponse>();
		try {
			logger.debug("Executing delegate retrieveCheckinPassenger");
			response = executor.executeBehaviour(checkinReprintBPBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - retrieveCheckinPassenger] - Errore durante il recupero delle informazioni dei dati volo e passeggero.", e);
		}
		return response;
	}
	
	@Override
	public CheckinIsBoardingPassPrintableResponse retrieveBoardingPassPrintable(
			CheckinIsBoardingPassPrintableRequest request) {
		CheckinIsBoardingPassPrintableResponse response = null;
		BehaviourExecutor<CheckinBoardingPassPrintableBehaviour, CheckinIsBoardingPassPrintableRequest, CheckinIsBoardingPassPrintableResponse> executor = new BehaviourExecutor<CheckinBoardingPassPrintableBehaviour, CheckinIsBoardingPassPrintableRequest, CheckinIsBoardingPassPrintableResponse>();
		try {
			logger.debug("Executing delegate retrieveBoardingPassPrintable");
			response = executor.executeBehaviour(checkinBoardingPassPrintableBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[StaticDataDelegateImpl - retrieveBoardingPassPrintable] - Errore durante il recupero dell'informazione relativa alla possibilita' di stampare il boarding pass.", e);
		}
		return response;
	}
	
	@Override
	public CheckinCreateBoardingPassResponse retrieveBoardingPass(CheckinCreateBoardingPassRequest request) {
		CheckinCreateBoardingPassResponse response = null;
		BehaviourExecutor<CheckinCreateBoardingPassBehaviour, CheckinCreateBoardingPassRequest, CheckinCreateBoardingPassResponse> executor = new BehaviourExecutor<CheckinCreateBoardingPassBehaviour, CheckinCreateBoardingPassRequest, CheckinCreateBoardingPassResponse>();
		try {
			logger.debug("Executing delegate retrieveBoardingPass");
			response = executor.executeBehaviour(checkinCreateBoardingPassBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[StaticDataDelegateImpl - retrieveBoardingPass] - Errore durante il recupero del boarding pass.", e);
		}
		return response;
	}
	
	@Override
	public CheckinGetSelectedAncillaryOffersResponse retrieveSelectedAncillaryOffers(
			CheckinGetSelectedAncillaryOffersRequest request) {
		CheckinGetSelectedAncillaryOffersResponse response = null;
		BehaviourExecutor<CheckinGetSelectedAncillaryOffersBehaviour, CheckinGetSelectedAncillaryOffersRequest, CheckinGetSelectedAncillaryOffersResponse> executor = new BehaviourExecutor<CheckinGetSelectedAncillaryOffersBehaviour, CheckinGetSelectedAncillaryOffersRequest, CheckinGetSelectedAncillaryOffersResponse>();
		try {
			logger.debug("Executing delegate retrieveSelectedAncillaryOffers");
			response = executor.executeBehaviour(checkinGetSelectedAncillaryOffersBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[StaticDataDelegateImpl - retrieveSelectedAncillaryOffers] - Errore durante il recupero degli ancillary selezionati.", e);
		}
		return response;
	}
	
	@Override
	public CheckinSetBaggageResponse setBaggage(CheckinSetBaggageRequest request) {
		CheckinSetBaggageResponse response = null;
		BehaviourExecutor<CheckinSetBaggageBehaviour, CheckinSetBaggageRequest, CheckinSetBaggageResponse> executor = new BehaviourExecutor<CheckinSetBaggageBehaviour, CheckinSetBaggageRequest, CheckinSetBaggageResponse>();
		try {
			logger.debug("Executing delegate retrieveCheckinPassenger");
			response = executor.executeBehaviour(checkinSetBaggageBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - setBaggage] - Errore durante il recupero delle informazioni dei dati bagaglio.", e);
		}
		return response;
	}

	@Override
	public CheckinEnhancedSeatMapResponse enhancedSeatMap(CheckinEnhancedSeatMapRequest request) {
		CheckinEnhancedSeatMapResponse response = null;
		BehaviourExecutor<CheckinEnhancedSeatMapBehaviour, CheckinEnhancedSeatMapRequest, CheckinEnhancedSeatMapResponse> executor = new BehaviourExecutor<CheckinEnhancedSeatMapBehaviour, CheckinEnhancedSeatMapRequest, CheckinEnhancedSeatMapResponse>();
		try {
			logger.debug("Executing delegate enhancedSeatMap");
			response = executor.executeBehaviour(checkinEnhancedSeatMapBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - enhancedSeatMap] - Errore durante il recupero delle informazioni dei dati della Seat-Map.", e);
		}
		return response;
	}

	@Override
	public CheckinChangeSeatsResponse changeSeats(CheckinChangeSeatsRequest request) {
		CheckinChangeSeatsResponse response = null;
		BehaviourExecutor<CheckinChangeSeatsBehaviour, CheckinChangeSeatsRequest, CheckinChangeSeatsResponse> executor = new BehaviourExecutor<CheckinChangeSeatsBehaviour, CheckinChangeSeatsRequest, CheckinChangeSeatsResponse>();
		try {
			logger.debug("Executing delegate changeSeats");
			response = executor.executeBehaviour(checkinChangeSeatsBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - changeSeats] - Errore durante la modifica delle informazioni dei dati della Seat-Map.", e);
		}
		return response;
	}

	@Override
	public CheckinUpdateSecurityInfoResponse updateSecurityInfo(CheckinUpdateSecurityInfoRequest request) {
		CheckinUpdateSecurityInfoResponse response = null;
		BehaviourExecutor<CheckinUpdateSecurityInfoBehaviour, CheckinUpdateSecurityInfoRequest, CheckinUpdateSecurityInfoResponse> executor = new BehaviourExecutor<CheckinUpdateSecurityInfoBehaviour, CheckinUpdateSecurityInfoRequest, CheckinUpdateSecurityInfoResponse>();
		try {
			logger.debug("Executing delegate updateSecurityInfo");
			response = executor.executeBehaviour(checkinUpdateSecurityInfoBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - updateSecurityInfo] - Errore durante la modifica delle informazioni.", e);
		}
		return response;
	}

	@Override
	public CheckinSetFastTrackResponse setFastTrack(CheckinSetFastTrackRequest request) {
		CheckinSetFastTrackResponse response = null;
		BehaviourExecutor<CheckinSetFastTrackBehaviour, CheckinSetFastTrackRequest, CheckinSetFastTrackResponse> executor = new BehaviourExecutor<CheckinSetFastTrackBehaviour, CheckinSetFastTrackRequest, CheckinSetFastTrackResponse>();
		try {
			logger.debug("Executing delegate setFastTrack");
			response = executor.executeBehaviour(checkinSetFastTrackBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[StaticDataDelegateImpl - setFastTrack] - Errore durante il set del fast track.", e);
		}
		return response;
	}

	@Override
	public CheckinSetLoungeResponse setLounge(CheckinSetLoungeRequest request) {
		CheckinSetLoungeResponse response = null;
		BehaviourExecutor<CheckinSetLoungeBehaviour, CheckinSetLoungeRequest, CheckinSetLoungeResponse> executor = new BehaviourExecutor<CheckinSetLoungeBehaviour, CheckinSetLoungeRequest, CheckinSetLoungeResponse>();
		try {
			logger.debug("Executing delegate setLounge");
			response = executor.executeBehaviour(checkinSetLoungeBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[StaticDataDelegateImpl - setLounge] - Errore durante il set lounge.", e);
		}
		return response;
	}

	@Override
	public CheckinClearAncillariesCartResponse clearAncillariesCart(CheckinClearAncillariesCartRequest request) {
		CheckinClearAncillariesCartResponse response = null;
		BehaviourExecutor<CheckinClearAncillariesCartBehaviour, CheckinClearAncillariesCartRequest, CheckinClearAncillariesCartResponse> executor = new BehaviourExecutor<CheckinClearAncillariesCartBehaviour, CheckinClearAncillariesCartRequest, CheckinClearAncillariesCartResponse>();
		try {
			logger.debug("Executing delegate setLounge");
			response = executor.executeBehaviour(checkinClearAncillariesCartBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[StaticDataDelegateImpl - setLounge] - Errore durante il set lounge.", e);
		}
		return response;
	}

	@Override
	public CheckinUpdatePassengerDetailsResponse updatePassengerDetails(CheckinUpdatePassengerDetailsRequest request) {
		CheckinUpdatePassengerDetailsResponse response = null;
		BehaviourExecutor<CheckinUpdatePassengerDetailsBehaviour, CheckinUpdatePassengerDetailsRequest, CheckinUpdatePassengerDetailsResponse> executor = new BehaviourExecutor<CheckinUpdatePassengerDetailsBehaviour, CheckinUpdatePassengerDetailsRequest, CheckinUpdatePassengerDetailsResponse>();
		try {
			logger.debug("Executing delegate updatePassengerDetails");
			response = executor.executeBehaviour(checkinUpdatePassengerDetailsBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - updatePassengerDetails] - Errore durante la modifica delle informazioni Passeggeri.", e);
		}
		return response;
	}

	@Override
	public CheckinUpdatePassengerResponse updatePassenger(CheckinUpdatePassengerRequest request) {
		CheckinUpdatePassengerResponse response = null;
		BehaviourExecutor<CheckinUpdatePassengerBehaviour, CheckinUpdatePassengerRequest, CheckinUpdatePassengerResponse> executor = new BehaviourExecutor<CheckinUpdatePassengerBehaviour, CheckinUpdatePassengerRequest, CheckinUpdatePassengerResponse>();
		try {
			logger.debug("Executing delegate updatePassenger");
			response = executor.executeBehaviour(checkinUpdatePassengerBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - updatePassenger] - Errore durante la modifica delle informazioni Passeggero.", e);
		}
		return response;
	}

	@Override
	public CheckinSelectedPassengerResponse executePassengerCheckin(CheckinSelectedPassengerRequest request){
		CheckinSelectedPassengerResponse response = null;
		BehaviourExecutor<CheckinSelectedPassengerBehaviour, CheckinSelectedPassengerRequest, CheckinSelectedPassengerResponse> executor = new BehaviourExecutor<CheckinSelectedPassengerBehaviour, CheckinSelectedPassengerRequest, CheckinSelectedPassengerResponse>();
		try {
			logger.debug("Executing delegate executePassengerCheckin");
			response = executor.executeBehaviour(checkinSelectedPassengerBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - executePassengerCheckin] - Errore durante la modifica delle informazioni Passeggero.", e);
		}
		return response;
	}

	@Override
	public CheckinGetBoardingPassInfoResponse getBoardingPassInfo(CheckinGetBoardingPassInfoRequest request) {
		CheckinGetBoardingPassInfoResponse response = null;
		BehaviourExecutor<CheckinGetBoardingPassInfoBehaviour, CheckinGetBoardingPassInfoRequest, CheckinGetBoardingPassInfoResponse> executor = new BehaviourExecutor<CheckinGetBoardingPassInfoBehaviour, CheckinGetBoardingPassInfoRequest, CheckinGetBoardingPassInfoResponse>();
		try {
			logger.debug("Executing delegate getBoardingPassInfo");
			response = executor.executeBehaviour(checkinGetBoardingPassInfoBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - getBoardingPassInfo] - Errore durante il recupero delle informazioni della carta d'imbarco.", e);
		}
		return response;
	}

	@Override
	public CheckinClearAncillarySessionEndResponse clearAncillaySessionEnd(
			CheckinClearAncillarySessionEndRequest request) {
		CheckinClearAncillarySessionEndResponse response = null;
		BehaviourExecutor<CheckinClearAncillarySessionEndBehaviour, CheckinClearAncillarySessionEndRequest, CheckinClearAncillarySessionEndResponse> executor = new BehaviourExecutor<CheckinClearAncillarySessionEndBehaviour, CheckinClearAncillarySessionEndRequest, CheckinClearAncillarySessionEndResponse>();
		try {
			logger.debug("Executing delegate clearAncillaySessionEnd");
			response = executor.executeBehaviour(checkinClearAncillarySessionEndBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - clearAncillaySessionEnd] - Errore durante la chiusura e il cancellamento dati della sessione", e);
		}
		return response;
	}

    @Override
    public CheckinOffloadResponse offload(CheckinOffloadRequest request) {
        CheckinOffloadResponse response = null;
        BehaviourExecutor<CheckinOffloadBehaviour, CheckinOffloadRequest, CheckinOffloadResponse> executor = new BehaviourExecutor<>();
        try {
            logger.debug("Executing delegate offload");
            response = executor.executeBehaviour(checkinOffloadBehaviour, request);
        }
        catch (Exception e) {
            logger.error("[CheckinDelegateImpl - offload] - Errore durante offload dati passeggero checkin");
        }
        return response;
    }

	@Override
	public CheckinOffloadFlightsResponse offloadFlights(CheckinOffloadFlightsRequest request) {
		CheckinOffloadFlightsResponse response = null;
		BehaviourExecutor<CheckinOffloadFlightsBehaviour, CheckinOffloadFlightsRequest, CheckinOffloadFlightsResponse> executor = new BehaviourExecutor<>();
		try {
			logger.debug("Executing delegate offload");
			response = executor.executeBehaviour(checkinOffloadFlightsBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - offload] - Errore durante offload dati passeggero checkin");
		}
		return response;
	}

	@Override
	public CheckinGetBoardingPassInfoByNameResponse getBoardingPassInfoByName(CheckinGetBoardingPassInfoByNameRequest serviceRequest) {
		CheckinGetBoardingPassInfoByNameResponse response = null;
		BehaviourExecutor<CheckinGetBoardingPassInfoByNameBehaviour, CheckinGetBoardingPassInfoByNameRequest, CheckinGetBoardingPassInfoByNameResponse> executor = new BehaviourExecutor<>();
		try {
			logger.debug("Executing delegate getBoardingPassInfoByName");
			response = executor.executeBehaviour(checkinGetBoardingPassInfoByNameBehaviour, serviceRequest);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - getBoardingPassInfoByName] - Errore durante il recupero delle informazioni della carta d'imbarco.", e);
		}
		return response;
	}

	@Override
	public CheckinSetSmsDataResponse setSmsData(CheckinSetSmsDataRequest request) {
		CheckinSetSmsDataResponse response = null;
		BehaviourExecutor<CheckinSetSmsDataBehaviour, CheckinSetSmsDataRequest, CheckinSetSmsDataResponse> executor = new BehaviourExecutor<CheckinSetSmsDataBehaviour, CheckinSetSmsDataRequest, CheckinSetSmsDataResponse>();
		try {
			logger.debug("Executing delegate setSmsData");
			response = executor.executeBehaviour(checkinSetSmsDataBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - setSmsData] - Errore durante il salvataggio delle informazioni della carta d'imbarco.", e);
		}
		return response;
	}

	@Override
	public CheckinGetSmsDataResponse getSmsData(CheckinGetSmsDataRequest request) {
		CheckinGetSmsDataResponse response = null;
		BehaviourExecutor<CheckinGetSmsDataBehaviour, CheckinGetSmsDataRequest, CheckinGetSmsDataResponse> executor = new BehaviourExecutor<CheckinGetSmsDataBehaviour, CheckinGetSmsDataRequest, CheckinGetSmsDataResponse>();
		try {
			logger.debug("Executing delegate getBoardingPassInfo");
			response = executor.executeBehaviour(checkinGetSmsDataBehaviour, request);
		}
		catch (Exception e) {
			logger.error("[CheckinDelegateImpl - getBoardingPassInfo] - Errore durante il recupero delle informazioni della carta d'imbarco.", e);
		}
		return response;
	}
}
