package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetPassengerDataRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetPassengerDataResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinGetPassengerDataBehaviour.class)
public class CheckinGetPassengerDataBehaviour extends Behaviour<CheckinGetPassengerDataRequest, CheckinGetPassengerDataResponse>{
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinGetPassengerDataResponse executeOrchestration(CheckinGetPassengerDataRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve Passenger Data request is null.");
		return checkinServiceRest.retrievePassengerData(request);
	}
}
