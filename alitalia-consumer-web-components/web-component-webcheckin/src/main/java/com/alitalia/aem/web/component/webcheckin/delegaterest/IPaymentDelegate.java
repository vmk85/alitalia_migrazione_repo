package com.alitalia.aem.web.component.webcheckin.delegaterest;

import com.alitalia.aem.common.messages.checkinrest.*;

public interface IPaymentDelegate {
	
	// Payment 25/01/2018
	CheckinGetCartResponse getCart(CheckinGetCartRequest serviceRequest);
	CheckinInitPaymentResponse initPayment(CheckinInitPaymentRequest serviceRequest);
	CheckinEndPaymentResponse endPayment(CheckinEndPaymentRequest serviceRequest);
	//Tolentino - Inizio
	CheckinInitPaymentResponse initPaymentRecurringAdyen(CheckinInitPaymentRequest serviceRequest);
	//Tolentino - Fine
	CheckinAuthorize3dResponse authorize3d(CheckinAuthorize3dRequest serviceRequest);

}
