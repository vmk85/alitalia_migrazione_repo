package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import com.alitalia.aem.common.messages.checkinrest.CheckinAddPassengerRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinAddPassengerResponse;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinTicketSearchResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinAddPassengerBehaviour.class)
public class CheckinAddPassengerBehaviour extends Behaviour<CheckinAddPassengerRequest, CheckinAddPassengerResponse> {

    @Reference
    private ICheckinService checkinServiceRest;

    @Override
    public CheckinAddPassengerResponse executeOrchestration(CheckinAddPassengerRequest request) {
        if(request == null)
            throw new IllegalArgumentException("Retrieve TicketSearch request is null.");
        return checkinServiceRest.retrieveCheckinAddPassenger(request);
    }

}
