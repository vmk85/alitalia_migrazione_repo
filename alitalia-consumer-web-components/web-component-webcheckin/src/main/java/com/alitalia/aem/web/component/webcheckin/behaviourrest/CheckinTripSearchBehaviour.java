package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinTripSearchRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinTripSearchResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinTripSearchBehaviour.class)
public class CheckinTripSearchBehaviour extends Behaviour<CheckinTripSearchRequest, CheckinTripSearchResponse>{
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinTripSearchResponse executeOrchestration(CheckinTripSearchRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve TripSearch request is null.");
		return checkinServiceRest.retrieveCheckinTrip(request);
	}
}
