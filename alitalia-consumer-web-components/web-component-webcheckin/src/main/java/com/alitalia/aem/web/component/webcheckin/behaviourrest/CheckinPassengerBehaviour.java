package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinPassengerRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinPassengerResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinPassengerBehaviour.class)
public class CheckinPassengerBehaviour extends Behaviour<CheckinPassengerRequest, CheckinPassengerResponse> {

	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinPassengerResponse executeOrchestration(CheckinPassengerRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve Passenger request is null.");
		return checkinServiceRest.retrieveCheckinPassenger(request);
	}

}
