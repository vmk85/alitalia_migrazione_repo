package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdatePassengerRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinUpdatePassengerResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinUpdatePassengerBehaviour.class)
public class CheckinUpdatePassengerBehaviour extends Behaviour<CheckinUpdatePassengerRequest, CheckinUpdatePassengerResponse> {
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	public CheckinUpdatePassengerResponse executeOrchestration(CheckinUpdatePassengerRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Retrieve Boarding Pass request is null.");
		return checkinServiceRest.updatePassenger(request);
	}
}
