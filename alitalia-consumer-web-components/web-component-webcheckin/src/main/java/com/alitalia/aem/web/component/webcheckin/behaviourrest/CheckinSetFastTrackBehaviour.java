package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetFastTrackRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinSetFastTrackResponse;
import com.alitalia.aem.service.api.home.ICheckinService;

@Component(immediate = true, metatype = false)
@Service(value = CheckinSetFastTrackBehaviour.class)
public class CheckinSetFastTrackBehaviour extends Behaviour<CheckinSetFastTrackRequest, CheckinSetFastTrackResponse>{
	
	@Reference
	private ICheckinService checkinServiceRest;
	
	@Override
	protected CheckinSetFastTrackResponse executeOrchestration(
			CheckinSetFastTrackRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Set fast track request is null.");
		return checkinServiceRest.setFastTrack(request);
	}

}
