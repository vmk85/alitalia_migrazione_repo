package com.alitalia.aem.web.component.webcheckin.behaviour;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CheckinSendSmsRequest;
import com.alitalia.aem.common.messages.home.CheckinSendSmsResponse;
import com.alitalia.aem.service.api.home.CheckinSendSmsService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = CheckinSendSmsBehavior.class)
public class CheckinSendSmsBehavior extends Behaviour<CheckinSendSmsRequest, CheckinSendSmsResponse> {

    @Reference
    private CheckinSendSmsService checkinSendSmsService;

    @Override
    public CheckinSendSmsResponse executeOrchestration(CheckinSendSmsRequest request) {
        if (request == null)
            throw new IllegalArgumentException("CheckinSendSmsBehavior is null.");
        return checkinSendSmsService.sendSms(request);
    }
}
