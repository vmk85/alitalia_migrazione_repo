package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSmsDataRequest;
import com.alitalia.aem.common.messages.checkinrest.CheckinGetSmsDataResponse;
import com.alitalia.aem.service.api.home.ICheckinService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

@Component(immediate = true, metatype = false)
@Service(value = CheckinGetSmsDataBehaviour.class)
public class CheckinGetSmsDataBehaviour extends Behaviour<CheckinGetSmsDataRequest, CheckinGetSmsDataResponse> {

    @Reference
    private ICheckinService checkinServiceRest;

    @Override
    protected CheckinGetSmsDataResponse executeOrchestration(CheckinGetSmsDataRequest request) {
        if(request == null)
            throw new IllegalArgumentException("Retrieve SMS data request is null.");
        return checkinServiceRest.getSmsData(request);
    }
}
