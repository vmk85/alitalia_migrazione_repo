package com.alitalia.aem.web.component.webcheckin.behaviourrest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.checkinrest.GetInsuranceRequest;
import com.alitalia.aem.common.messages.checkinrest.GetInsuranceResponse;
import com.alitalia.aem.service.api.home.IServiceInsurance;

@Component(immediate = true, metatype = false)
@Service(value = GetInsuranceBehaviour.class)
public class GetInsuranceBehaviour extends Behaviour<GetInsuranceRequest, GetInsuranceResponse> {

	@Reference
	private IServiceInsurance serviceInsurance;
	
	@Override
	protected GetInsuranceResponse executeOrchestration(GetInsuranceRequest request) {
		if(request == null) 
			throw new IllegalArgumentException("Get Insurance request is null.");
		return serviceInsurance.getInsurance(request);
	}

}
