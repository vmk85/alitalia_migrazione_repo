package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.SapInvoiceRequest;
import com.alitalia.aem.common.messages.home.SapInvoiceResponse;
import com.alitalia.aem.service.api.home.MmbTicketService;

@Component(immediate=true, metatype=false)
@Service(value=GenerateSapInvoiceBehaviour.class)
public class GenerateSapInvoiceBehaviour extends Behaviour<SapInvoiceRequest, SapInvoiceResponse> {

	@Reference
	private MmbTicketService mmbTicketService;

	@Override
	protected SapInvoiceResponse executeOrchestration(SapInvoiceRequest request) {
		if (request == null)
			throw new IllegalArgumentException("SapInvoiceRequest is null.");
		return mmbTicketService.callSAPService(request);
	}

}
