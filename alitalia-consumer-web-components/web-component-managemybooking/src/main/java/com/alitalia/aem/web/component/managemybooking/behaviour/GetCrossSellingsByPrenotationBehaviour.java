package com.alitalia.aem.web.component.managemybooking.behaviour;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.data.home.AdultPassengerData;
import com.alitalia.aem.common.data.home.AirportData;
import com.alitalia.aem.common.data.home.ConnectingFlightData;
import com.alitalia.aem.common.data.home.CrossSellingsData;
import com.alitalia.aem.common.data.home.DirectFlightData;
import com.alitalia.aem.common.data.home.FlightData;
import com.alitalia.aem.common.data.home.RouteData;
import com.alitalia.aem.common.data.home.RoutesData;
import com.alitalia.aem.common.data.home.enumerations.ContactTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbAQQTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbApisTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbChannelsEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbCompartimentalClassEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLegTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLinkTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbMealAllowedEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPassengerNoteEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbPassengerStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbRouteStatusEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbUpdateSSREnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.RouteTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbAirportData;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.common.messages.home.MmbCrossSellingsRequest;
import com.alitalia.aem.service.api.home.MmbSearchService;
import com.alitalia.aem.service.api.home.MmbTicketService;

@Component(immediate=true, metatype=false)
@Service(value=GetCrossSellingsByPrenotationBehaviour.class)
public class GetCrossSellingsByPrenotationBehaviour extends Behaviour<CrossSellingsRequest, CrossSellingsResponse> {

	@Reference
	private MmbSearchService mmbSearchService;

	@Reference
	private MmbTicketService mmbTicketService;

	@Override
	protected CrossSellingsResponse executeOrchestration(CrossSellingsRequest request) {
		if (request == null)
			throw new IllegalArgumentException("MmbCrossSellingsRequest is null.");

		CrossSellingsResponse crossSellingsResponse = null;
		RoutesData prenotation = request.getPrenotation();

		MmbRequestBaseInfoData baseInfo = new MmbRequestBaseInfoData();
		baseInfo.setClientIp(request.getIpAddress());
		baseInfo.setSessionId(request.getSid());
		baseInfo.setSiteCode(request.getMarketCode());

		List<MmbRouteData> routesList = createRouteData(prenotation);

		if (routesList != null && !routesList.isEmpty()) {
			MmbRouteData mmbPrenotation = routesList.get(0);

			MmbCrossSellingsRequest crossSellingsRequest = new MmbCrossSellingsRequest();
			crossSellingsRequest.setIpAddress(request.getIpAddress());
			crossSellingsRequest.setLanguageCode(request.getLanguageCode());
			crossSellingsRequest.setMarketCode(request.getMarketCode());
			crossSellingsRequest.setPrenotation(mmbPrenotation);
			crossSellingsRequest.setSid(request.getSid());
			crossSellingsRequest.setTid(request.getTid());

			crossSellingsResponse = mmbTicketService.getCrossSellings(crossSellingsRequest);
		}
		else
		{
			CrossSellingsData crossSellingData = new CrossSellingsData();
			crossSellingData.setCrossSellingInformations(null);
			crossSellingData.setItems(null);
			
			crossSellingsResponse = new CrossSellingsResponse();
			crossSellingsResponse.setCrossSellingsData(crossSellingData);
			crossSellingsResponse.setSid(request.getSid());
			crossSellingsResponse.setTid(request.getTid());
		}

		return crossSellingsResponse;
	}

	/*
	 * MmbRouteData crafted in substitution of GetRouteListNew Service
	 */
	private List<MmbRouteData> createRouteData(RoutesData prenotation) {
		List<MmbRouteData> mmbRoutes = new LinkedList<MmbRouteData>();
		MmbRouteData mmbRoute = new MmbRouteData();
		List<MmbFlightData> mmbFlightData = new LinkedList<MmbFlightData>();
		for (RouteData route : prenotation.getRoutesList()){
			/*per evitare errori dell'auto-unwrapping nel converter*/
			mmbRoute.setCartFastTrackBox(false);
			mmbRoute.setCartVipLoungeBox(false);
			mmbRoute.setCheckinEnabled(false);
			mmbRoute.setConfirmed(false);
			mmbRoute.setId(0);
			mmbRoute.setMealAvailable(false);
			mmbRoute.setSelected(false);
			mmbRoute.setUndoEnabled(false);
			mmbRoute.setVisibleApis(false);
			/*per evitare errori in fase di de-serializzazione della request*/
			mmbRoute.setApisTypeRequired(MmbApisTypeEnum.NONE);
			mmbRoute.setChannel(MmbChannelsEnum.UNKNOW);
			mmbRoute.setMealAllowed(MmbMealAllowedEnum.FALSE);
			mmbRoute.setStatus(MmbRouteStatusEnum.REGULAR);
			mmbRoute.setDateTimePaymentLimit(Calendar.getInstance());
			
			for(FlightData flight : route.getFlights()){
				if(flight instanceof DirectFlightData){
					DirectFlightData directFlight = (DirectFlightData) flight;
					MmbFlightData mmbFlight = createFlightData(directFlight, route.getType());
					
					mmbFlight.setPassengers(createPassengerList(prenotation));
					mmbFlightData.add(mmbFlight);
				}
				else if(flight instanceof ConnectingFlightData){
					ConnectingFlightData connectingFlight = (ConnectingFlightData) flight;
					for(FlightData genericFlight : connectingFlight.getFlights()){
						DirectFlightData directFlight = (DirectFlightData) genericFlight;
						
						MmbFlightData mmbFlight = createFlightData(directFlight, route.getType());
						
						mmbFlight.setPassengers(createPassengerList(prenotation));
						mmbFlightData.add(mmbFlight);
					}
				}
				
			}
			
		}
		mmbRoute.setFlights(mmbFlightData);
		mmbRoutes.add(mmbRoute);
		return mmbRoutes;
	}
	
	
	/*
	 * set From code, To code, departure date and arrival date
	 */
	private MmbFlightData createFlightData(DirectFlightData directFlight, RouteTypeEnum routeType){
		MmbFlightData mmbFlight = new MmbFlightData();
		/*per evitare errori dell'auto-unwrapping nel converter*/
		mmbFlight.setBaggageAllow(0);
		mmbFlight.setComfortSeatPaid(false);
		mmbFlight.setEnabledSeatMap(false);
		mmbFlight.setExtraBaggage(0);
		mmbFlight.setFirstFlight(false);
		mmbFlight.setComfortSeatFare(BigDecimal.ZERO);
		mmbFlight.setHasComfortSeat(false);
		mmbFlight.setId(0);
		mmbFlight.setIndex(0);
		mmbFlight.setMiniFare(false);
		mmbFlight.setMultitratta(0);
		mmbFlight.setRouteId(0);
		mmbFlight.setRph(0);
		/*per evitare errori in fase di de-serializzazione della request*/
		mmbFlight.setApisTypeRequired(MmbApisTypeEnum.NONE);
		mmbFlight.setLegType(MmbLegTypeEnum.NONE);
		mmbFlight.setSeatClass(MmbCompartimentalClassEnum.UNKNOWN);
		mmbFlight.setStatus(MmbFlightStatusEnum.UNKNOWN);
		
		mmbFlight.setType(routeType);
		mmbFlight.setCabin(directFlight.getCabin());
		mmbFlight.setFrom(createAirportData(directFlight.getFrom()));
		mmbFlight.setTo(createAirportData(directFlight.getTo()));
		mmbFlight.setDepartureDateTime(directFlight.getDepartureDate());
		mmbFlight.setArrivalDateTime(directFlight.getArrivalDate());
		return mmbFlight;
		
	}
	
	/*
	 * set only the airport code
	 */
	private MmbAirportData createAirportData(AirportData aptData){
		MmbAirportData mmbAptData = new MmbAirportData();
		/*per evitare errori dell'auto-unwrapping nel converter*/
		mmbAptData.setAzDeserves(false);
		mmbAptData.setEligibility(false);
		mmbAptData.setHiddenInFirstDeparture(false);
		mmbAptData.setId(0);
		mmbAptData.setIdMsg(0);
		mmbAptData.setArea(aptData.getArea());
		mmbAptData.setCode(aptData.getCode());
		return mmbAptData;
	}
	
	/*
	 * set only the applicant passenger
	 */
	private List<MmbPassengerData> createPassengerList(RoutesData prenotation){
		List<MmbPassengerData> mmbPassengerData = new LinkedList<MmbPassengerData>();
		MmbPassengerData applicantPassenger = new MmbPassengerData();
		/*per evitare errori dell'auto-unwrapping nel converter*/
		applicantPassenger.setBaggageAllowanceQuantity(0);
		applicantPassenger.setBoardingPassViaEmail(false);
		applicantPassenger.setBoardingPassViaSms(false);
		applicantPassenger.setComfortSeatFree(false);
		applicantPassenger.setCorporate(false);
		applicantPassenger.setId(0);
		applicantPassenger.setPaymentViaEmail(false);
		applicantPassenger.setRouteId(0);
		applicantPassenger.setSelected(false);
		applicantPassenger.setSpecialFare(false);
		/*per evitare errori in fase di de-serializzazione della request*/
		applicantPassenger.setAuthorityPermission(MmbAQQTypeEnum.CLEARED);
		applicantPassenger.setContactType(ContactTypeEnum.A);
		applicantPassenger.setLinkType(MmbLinkTypeEnum.NONE);
		applicantPassenger.setNote(MmbPassengerNoteEnum.NONE);
		applicantPassenger.setSeatClass(MmbCompartimentalClassEnum.UNKNOWN);
		applicantPassenger.setStatus(MmbPassengerStatusEnum.REGULAR);
		applicantPassenger.setUpdate(MmbUpdateSSREnum.NONE);
		
		applicantPassenger.setType(PassengerTypeEnum.APPLICANT);
		if(prenotation.getPassengers() != null && prenotation.getPassengers().size() > 0
				&& prenotation.getPassengers().get(0) instanceof AdultPassengerData){
			applicantPassenger.setFrequentFlyerCode(
					((AdultPassengerData) prenotation.getPassengers().get(0)).getFrequentFlyerCode());
		}
		mmbPassengerData.add(applicantPassenger);
		return mmbPassengerData;
	}

}
