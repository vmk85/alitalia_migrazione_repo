package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerRequest;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerResponse;
import com.alitalia.aem.service.api.home.MmbSearchService;

@Component(immediate = true, metatype = false)
@Service(value = UpdatePnrFrequentFlyerBehaviour.class)
public class UpdatePnrFrequentFlyerBehaviour extends Behaviour<UpdateMmbPnrFrequentFlyerRequest, UpdateMmbPnrFrequentFlyerResponse> {

	@Reference
	private MmbSearchService mmbSearchService;
	
	@Override
	public UpdateMmbPnrFrequentFlyerResponse executeOrchestration(UpdateMmbPnrFrequentFlyerRequest request) {
		if (request == null)
			throw new IllegalArgumentException("UpdateMmbPnrFrequentFlyerRequest is null.");
		return mmbSearchService.updatePnrFrequentFlyer(request);
	}

}
