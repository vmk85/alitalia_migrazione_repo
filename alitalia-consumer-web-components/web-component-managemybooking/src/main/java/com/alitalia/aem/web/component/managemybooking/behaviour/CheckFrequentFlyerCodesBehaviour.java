package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesResponse;
import com.alitalia.aem.service.api.home.MmbPassengerService;

@Component(immediate=true, metatype=false)
@Service(value=CheckFrequentFlyerCodesBehaviour.class)
public class CheckFrequentFlyerCodesBehaviour extends Behaviour<MmbCheckFrequentFlyerCodesRequest, MmbCheckFrequentFlyerCodesResponse> {

	@Reference
	private MmbPassengerService mmbPassengerService;

	@Override
	protected MmbCheckFrequentFlyerCodesResponse executeOrchestration(MmbCheckFrequentFlyerCodesRequest request) {
		if (request == null)
			throw new IllegalArgumentException("MmbCheckFrequentFlyerCodesRequest is null.");
		return mmbPassengerService.checkFrequentFlyerCodes(request);
	}

}
