package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinResponse;
import com.alitalia.aem.service.api.home.MmbStaticDataService;

@Component(immediate = true, metatype = false)
@Service(value = RetrieveMmbAirportCheckinBehaviour.class)
public class RetrieveMmbAirportCheckinBehaviour extends Behaviour<RetrieveMmbAirportCheckinRequest, RetrieveMmbAirportCheckinResponse> {

	@Reference
	private MmbStaticDataService mmbStaticDataService;
	
	@Override
	public RetrieveMmbAirportCheckinResponse executeOrchestration(RetrieveMmbAirportCheckinRequest request) {
		if (request == null)
			throw new IllegalArgumentException("RetrieveProvincesRequest is null.");
		return mmbStaticDataService.retrieveAirportCheckIn(request);
	}

}
