package com.alitalia.aem.web.component.managemybooking.delegate;

import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.common.messages.home.GetSabrePNRRequest;
import com.alitalia.aem.common.messages.home.GetSabrePNRResponse;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutEndRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutEndResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryRemoveFromCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryRemoveFromCartResponse;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryUpdateCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryUpdateCartResponse;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesResponse;
import com.alitalia.aem.common.messages.home.MmbCrossSellingsRequest;
import com.alitalia.aem.common.messages.home.MmbInsuranceInfoRequest;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoRequest;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoResponse;
import com.alitalia.aem.common.messages.home.MmbPayInsuranceRequest;
import com.alitalia.aem.common.messages.home.MmbSendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbSendEmailResponse;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbFrequentFlyerCarriersResponse;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationResponse;
import com.alitalia.aem.common.messages.home.SapInvoiceRequest;
import com.alitalia.aem.common.messages.home.SapInvoiceResponse;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsRequest;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsResponse;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerRequest;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerResponse;

public interface ManageMyBookingDelegate {

	// Seat map
	RetrieveMmbFlightSeatMapResponse getFlightSeatMap(RetrieveMmbFlightSeatMapRequest request);

	// Static data
	RetrieveProvincesResponse getProvinces(RetrieveProvincesRequest request);
	RetrieveCreditCardCountriesResponse getCreditCardCountries(RetrieveCreditCardCountriesRequest request);
	RetrieveCountriesResponse getCountries(RetrieveCountriesRequest request);
	RetrievePhonePrefixResponse getPhonePrefix(RetrievePhonePrefixRequest request);
	RetrievePaymentTypeItemResponse getPaymentTypeItem(RetrievePaymentTypeItemRequest request);
	RetrieveMealsResponse getMeals(RetrieveMealsRequest request);
	RetrieveMmbFrequentFlyerCarriersResponse getFrequentFlyers(RetrieveFrequentFlayerRequest request);
	RetrieveMmbAirportCheckinResponse getAirportCheckIn(RetrieveMmbAirportCheckinRequest request);
	RetrieveMmbAmericanStatesResponse getAmericanStates(RetrieveMmbAmericanStatesRequest request);

	// Search and PNR update
	RetriveMmbPnrInformationResponse retrivePnrInformation(RetriveMmbPnrInformationRequest request);
	UpdateMmbPnrContactsResponse updatePnrContacts(UpdateMmbPnrContactsRequest request);
	UpdateMmbPnrFrequentFlyerResponse updatePnrFrequentFlyer(UpdateMmbPnrFrequentFlyerRequest request);
	MmbModifyPnrApisInfoResponse modifyPnrPassengerApisInfo(MmbModifyPnrApisInfoRequest request);
	GetSabrePNRResponse getSabrePNR(GetSabrePNRRequest request);

	//Insurance
	InsuranceResponse getInsurance(MmbInsuranceInfoRequest request);
	InsuranceResponse payInsurance(MmbPayInsuranceRequest request);

	// Ancillary
	RetrieveMmbAncillaryCatalogRequest buildCatalogRequestFromMmbRouteData(
			MmbRequestBaseInfoData baseInfo, String client, String language,
			String machineName, String market, String pnr, MmbRouteData routeData);

	RetrieveMmbAncillaryCatalogResponse getCatalog(RetrieveMmbAncillaryCatalogRequest request);
	RetrieveMmbAncillaryCartResponse getCart(RetrieveMmbAncillaryCartRequest request);
	MmbAncillaryAddToCartResponse addToCart(MmbAncillaryAddToCartRequest request);
	MmbAncillaryUpdateCartResponse updateCart(MmbAncillaryUpdateCartRequest request);
	MmbAncillaryRemoveFromCartResponse removeFromCart(MmbAncillaryRemoveFromCartRequest request);
	MmbAncillaryCheckOutInitResponse checkOutInit(MmbAncillaryCheckOutInitRequest request);
	MmbAncillaryCheckOutEndResponse checkOutEnd(MmbAncillaryCheckOutEndRequest request);
	MmbAncillaryCheckOutStatusResponse getCheckOutStatus(MmbAncillaryCheckOutStatusRequest request);
	MmbAncillarySendEmailResponse sendPurchasedAncillaryEmail(MmbAncillarySendEmailRequest request);

	//Passenger
	MmbCheckFrequentFlyerCodesResponse checkFrequentFlyerCodes(MmbCheckFrequentFlyerCodesRequest request);

	//Ticketing - SAP invoice
	SapInvoiceResponse callSAPService(SapInvoiceRequest request);
	CrossSellingsResponse getMmbCrossSellings(MmbCrossSellingsRequest request);
	CrossSellingsResponse getCrossSellingsByPrenotation(CrossSellingsRequest request);

	//Mail
	MmbSendEmailResponse sendMail(MmbSendEmailRequest request);

}