package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MmbAncillaryRemoveFromCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryRemoveFromCartResponse;
import com.alitalia.aem.service.api.home.MmbAncillaryService;

@Component(immediate=true, metatype=false)
@Service(value=RemoveFromAncillaryCartBehaviour.class)
public class RemoveFromAncillaryCartBehaviour extends Behaviour<MmbAncillaryRemoveFromCartRequest, MmbAncillaryRemoveFromCartResponse> {

	@Reference
	private MmbAncillaryService mmbAncillaryService;

	@Override
	protected MmbAncillaryRemoveFromCartResponse executeOrchestration(MmbAncillaryRemoveFromCartRequest request) {
		if (request == null)
			throw new IllegalArgumentException("MmbAncillaryRemoveFromCartRequest is null.");
		return mmbAncillaryService.removeFromCart(request);
	}

}
