package com.alitalia.aem.web.component.managemybooking.delegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alitalia.aem.common.component.AlitaliaWebComponentCommon;
import com.alitalia.aem.common.component.behaviour.executor.BehaviourExecutor;
import com.alitalia.aem.common.data.home.enumerations.MmbFlightTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.MmbLinkTypeEnum;
import com.alitalia.aem.common.data.home.enumerations.PassengerTypeEnum;
import com.alitalia.aem.common.data.home.mmb.MmbFlightData;
import com.alitalia.aem.common.data.home.mmb.MmbPassengerData;
import com.alitalia.aem.common.data.home.mmb.MmbRequestBaseInfoData;
import com.alitalia.aem.common.data.home.mmb.MmbRouteData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryCouponData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryEticketData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryFlightData;
import com.alitalia.aem.common.data.home.mmb.ancillary.MmbAncillaryPassengerData;
import com.alitalia.aem.common.messages.home.CrossSellingsRequest;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.common.messages.home.GetSabrePNRRequest;
import com.alitalia.aem.common.messages.home.GetSabrePNRResponse;
import com.alitalia.aem.common.messages.home.InsuranceResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutEndRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutEndResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutInitResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutStatusResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryRemoveFromCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryRemoveFromCartResponse;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbAncillarySendEmailResponse;
import com.alitalia.aem.common.messages.home.MmbAncillaryUpdateCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryUpdateCartResponse;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesRequest;
import com.alitalia.aem.common.messages.home.MmbCheckFrequentFlyerCodesResponse;
import com.alitalia.aem.common.messages.home.MmbCrossSellingsRequest;
import com.alitalia.aem.common.messages.home.MmbInsuranceInfoRequest;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoRequest;
import com.alitalia.aem.common.messages.home.MmbModifyPnrApisInfoResponse;
import com.alitalia.aem.common.messages.home.MmbPayInsuranceRequest;
import com.alitalia.aem.common.messages.home.MmbSendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbSendEmailResponse;
import com.alitalia.aem.common.messages.home.RetrieveCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesRequest;
import com.alitalia.aem.common.messages.home.RetrieveCreditCardCountriesResponse;
import com.alitalia.aem.common.messages.home.RetrieveFrequentFlayerRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsRequest;
import com.alitalia.aem.common.messages.home.RetrieveMealsResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAirportCheckinResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAmericanStatesResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCartResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbAncillaryCatalogResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapRequest;
import com.alitalia.aem.common.messages.home.RetrieveMmbFlightSeatMapResponse;
import com.alitalia.aem.common.messages.home.RetrieveMmbFrequentFlyerCarriersResponse;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemRequest;
import com.alitalia.aem.common.messages.home.RetrievePaymentTypeItemResponse;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixRequest;
import com.alitalia.aem.common.messages.home.RetrievePhonePrefixResponse;
import com.alitalia.aem.common.messages.home.RetrieveProvincesRequest;
import com.alitalia.aem.common.messages.home.RetrieveProvincesResponse;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationResponse;
import com.alitalia.aem.common.messages.home.SapInvoiceRequest;
import com.alitalia.aem.common.messages.home.SapInvoiceResponse;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsRequest;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrContactsResponse;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerRequest;
import com.alitalia.aem.common.messages.home.UpdateMmbPnrFrequentFlyerResponse;
import com.alitalia.aem.web.component.managemybooking.behaviour.AddToAncillaryCartBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.AncillaryCartCheckOutEndBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.AncillaryCartCheckOutInitBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.AncillaryCartCheckOutStatusInsuranceBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.CheckFrequentFlyerCodesBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.GenerateSapInvoiceBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.GetAncillaryCartBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.GetAncillaryCatalogBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.GetCrossSellingsByPrenotationBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.GetMmbCrossSellingsBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.GetSabrePNRBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.ModifyPnrPassengerApisInfoBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.PayInsuranceBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.RemoveFromAncillaryCartBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.RetrieveCountriesBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.RetrieveCreditCardCountriesBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.RetrieveFlightSeatMapBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.RetrieveInsuranceInfoBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.RetrieveMealsBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.RetrieveMmbAirportCheckinBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.RetrieveMmbAmericanStatesBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.RetrieveMmbFrequentFlyerCarriersBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.RetrievePaymentTypeItemsBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.RetrievePhonePrefixesBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.RetrieveProvincesBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.RetrivePnrInformationBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.SendEmailBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.SendEmailForPurchasedAncillaryBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.UpdateAncillaryCartBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.UpdatePnrContactsBehaviour;
import com.alitalia.aem.web.component.managemybooking.behaviour.UpdatePnrFrequentFlyerBehaviour;

@Component(immediate=true, metatype=false)
@Service
public class SimpleManageMyBookingDelegate implements ManageMyBookingDelegate {

	private static Logger logger = LoggerFactory.getLogger(SimpleManageMyBookingDelegate.class);
	
	@Reference
	private AlitaliaWebComponentCommon alitaliaWebComponentCommon;
	
	@Reference
	private RetrieveFlightSeatMapBehaviour retrieveFlightSeatMapBehaviours;

	@Reference
	private RetrieveProvincesBehaviour retrieveProvincesBehaviour;

	@Reference
	private RetrieveCreditCardCountriesBehaviour retrieveCreditCardCountriesBehaviour;

	@Reference
	private RetrieveCountriesBehaviour retrieveCountriesBehaviour;

	@Reference
	private RetrieveMealsBehaviour retrieveMealsBehaviour;

	@Reference
	private RetrieveMmbAirportCheckinBehaviour retrieveMmbAirportCheckinBehaviour;

	@Reference
	private RetrieveMmbAmericanStatesBehaviour retrieveMmbAmericanStatesBehaviour;

	@Reference
	private RetrieveMmbFrequentFlyerCarriersBehaviour retrieveMmbFrequentFlyerCarriersBehaviour;

	@Reference
	private RetrievePaymentTypeItemsBehaviour retrievePaymentTypeItemsBehaviour;

	@Reference
	private RetrievePhonePrefixesBehaviour retrievePhonePrefixesBehaviour;

	@Reference
	private RetrivePnrInformationBehaviour retrivePnrInformationBehaviour;

	@Reference
	private UpdatePnrContactsBehaviour updatePnrContactsBehaviour;

	@Reference
	private UpdatePnrFrequentFlyerBehaviour updatePnrFrequentFlyerBehaviour;

	@Reference
	private ModifyPnrPassengerApisInfoBehaviour modifyPnrPassengerApisInfoBehaviour;

	@Reference
	private RetrieveInsuranceInfoBehaviour retrieveInsuranceInfoBehaviour;

	@Reference
	private PayInsuranceBehaviour payInsuranceBehaviour;

	@Reference
	private GetAncillaryCatalogBehaviour getAncillaryCatalogBehaviour;

	@Reference
	private GetAncillaryCartBehaviour getAncillaryCartBehaviour;

	@Reference
	private AddToAncillaryCartBehaviour addToAncillaryCartBehaviour;

	@Reference
	private UpdateAncillaryCartBehaviour updateAncillaryCartBehaviour;

	@Reference
	private RemoveFromAncillaryCartBehaviour removeFromAncillaryCartBehaviour;

	@Reference
	private AncillaryCartCheckOutInitBehaviour ancillaryCartCheckOutInitBehaviour;

	@Reference
	private AncillaryCartCheckOutEndBehaviour ancillaryCartCheckOutEndBehaviour;

	@Reference
	private AncillaryCartCheckOutStatusInsuranceBehaviour ancillaryCartCheckOutStatusInsuranceBehaviour;

	@Reference
	private SendEmailForPurchasedAncillaryBehaviour sendPurchasedAncillaryEmailBehaviour;

	@Reference
	private CheckFrequentFlyerCodesBehaviour checkFrequentFlyerCodesBehaviour;

	@Reference
	private GenerateSapInvoiceBehaviour generateSapInvoiceBehaviour;

	@Reference
	private GetMmbCrossSellingsBehaviour getMmbCrossSellingsBehaviour;

	@Reference
	private GetCrossSellingsByPrenotationBehaviour getCrossSellingsByPrenotationBehaviour;

	@Reference
	private SendEmailBehaviour sendEmailBehaviour;
	
	@Reference
	private GetSabrePNRBehaviour getSabrePNRBehaviour;

	@Override
	public RetrieveMmbFlightSeatMapResponse getFlightSeatMap(RetrieveMmbFlightSeatMapRequest request) {
		BehaviourExecutor<RetrieveFlightSeatMapBehaviour, RetrieveMmbFlightSeatMapRequest, RetrieveMmbFlightSeatMapResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveFlightSeatMapBehaviours, request);
	}

	@Override
	public RetrieveProvincesResponse getProvinces(RetrieveProvincesRequest request) {
		BehaviourExecutor<RetrieveProvincesBehaviour, RetrieveProvincesRequest, RetrieveProvincesResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveProvincesBehaviour, request);
	}

	@Override
	public RetrieveCreditCardCountriesResponse getCreditCardCountries(RetrieveCreditCardCountriesRequest request) {
		BehaviourExecutor<RetrieveCreditCardCountriesBehaviour, RetrieveCreditCardCountriesRequest, RetrieveCreditCardCountriesResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveCreditCardCountriesBehaviour, request);
	}

	@Override
	public RetrieveCountriesResponse getCountries(RetrieveCountriesRequest request) {
		BehaviourExecutor<RetrieveCountriesBehaviour, RetrieveCountriesRequest, RetrieveCountriesResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveCountriesBehaviour, request);
	}

	@Override
	public RetrievePhonePrefixResponse getPhonePrefix(RetrievePhonePrefixRequest request) {
		BehaviourExecutor<RetrievePhonePrefixesBehaviour, RetrievePhonePrefixRequest, RetrievePhonePrefixResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrievePhonePrefixesBehaviour, request);
	}

	@Override
	public RetrievePaymentTypeItemResponse getPaymentTypeItem(RetrievePaymentTypeItemRequest request) {
		BehaviourExecutor<RetrievePaymentTypeItemsBehaviour, RetrievePaymentTypeItemRequest, RetrievePaymentTypeItemResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrievePaymentTypeItemsBehaviour, request);
	}

	@Override
	public RetrieveMealsResponse getMeals(RetrieveMealsRequest request) {
		BehaviourExecutor<RetrieveMealsBehaviour, RetrieveMealsRequest, RetrieveMealsResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveMealsBehaviour, request);
	}

	@Override
	public RetrieveMmbFrequentFlyerCarriersResponse getFrequentFlyers(RetrieveFrequentFlayerRequest request) {
		BehaviourExecutor<RetrieveMmbFrequentFlyerCarriersBehaviour, RetrieveFrequentFlayerRequest, RetrieveMmbFrequentFlyerCarriersResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveMmbFrequentFlyerCarriersBehaviour, request);
	}

	@Override
	public RetrieveMmbAirportCheckinResponse getAirportCheckIn(RetrieveMmbAirportCheckinRequest request) {
		BehaviourExecutor<RetrieveMmbAirportCheckinBehaviour, RetrieveMmbAirportCheckinRequest, RetrieveMmbAirportCheckinResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveMmbAirportCheckinBehaviour, request);
	}

	@Override
	public RetrieveMmbAmericanStatesResponse getAmericanStates(RetrieveMmbAmericanStatesRequest request) {
		BehaviourExecutor<RetrieveMmbAmericanStatesBehaviour, RetrieveMmbAmericanStatesRequest, RetrieveMmbAmericanStatesResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveMmbAmericanStatesBehaviour, request);
	}

	@Override
	public RetriveMmbPnrInformationResponse retrivePnrInformation(RetriveMmbPnrInformationRequest request) {
		BehaviourExecutor<RetrivePnrInformationBehaviour, RetriveMmbPnrInformationRequest, RetriveMmbPnrInformationResponse> executor = new BehaviourExecutor<>();
		
		RetriveMmbPnrInformationResponse mmbPnrInformationResponse = executor.executeBehaviour(retrivePnrInformationBehaviour, request);
		if (mmbPnrInformationResponse != null) {
			searchForABus(mmbPnrInformationResponse);
		}
		
		return mmbPnrInformationResponse;
	}
	
	@Override
	public UpdateMmbPnrContactsResponse updatePnrContacts(UpdateMmbPnrContactsRequest request) {
		BehaviourExecutor<UpdatePnrContactsBehaviour, UpdateMmbPnrContactsRequest, UpdateMmbPnrContactsResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(updatePnrContactsBehaviour, request);
	}

	@Override
	public UpdateMmbPnrFrequentFlyerResponse updatePnrFrequentFlyer(UpdateMmbPnrFrequentFlyerRequest request) {
		BehaviourExecutor<UpdatePnrFrequentFlyerBehaviour, UpdateMmbPnrFrequentFlyerRequest, UpdateMmbPnrFrequentFlyerResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(updatePnrFrequentFlyerBehaviour, request);
	}

	@Override
	public MmbModifyPnrApisInfoResponse modifyPnrPassengerApisInfo(MmbModifyPnrApisInfoRequest request) {
		BehaviourExecutor<ModifyPnrPassengerApisInfoBehaviour, MmbModifyPnrApisInfoRequest, MmbModifyPnrApisInfoResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(modifyPnrPassengerApisInfoBehaviour, request);
	}

	@Override
	public InsuranceResponse getInsurance(MmbInsuranceInfoRequest request) {
		BehaviourExecutor<RetrieveInsuranceInfoBehaviour, MmbInsuranceInfoRequest, InsuranceResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(retrieveInsuranceInfoBehaviour, request);
	}

	@Override
	public InsuranceResponse payInsurance(MmbPayInsuranceRequest request) {
		BehaviourExecutor<PayInsuranceBehaviour, MmbPayInsuranceRequest, InsuranceResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(payInsuranceBehaviour, request);
	}

	@Override
	public RetrieveMmbAncillaryCatalogResponse getCatalog(RetrieveMmbAncillaryCatalogRequest request) {
		BehaviourExecutor<GetAncillaryCatalogBehaviour, RetrieveMmbAncillaryCatalogRequest, RetrieveMmbAncillaryCatalogResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(getAncillaryCatalogBehaviour, request);
	}

	@Override
	public RetrieveMmbAncillaryCartResponse getCart(RetrieveMmbAncillaryCartRequest request) {
		BehaviourExecutor<GetAncillaryCartBehaviour, RetrieveMmbAncillaryCartRequest, RetrieveMmbAncillaryCartResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(getAncillaryCartBehaviour, request);
	}

	@Override
	public MmbAncillaryAddToCartResponse addToCart(MmbAncillaryAddToCartRequest request) {
		BehaviourExecutor<AddToAncillaryCartBehaviour, MmbAncillaryAddToCartRequest, MmbAncillaryAddToCartResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(addToAncillaryCartBehaviour, request);
	}

	@Override
	public MmbAncillaryUpdateCartResponse updateCart(MmbAncillaryUpdateCartRequest request) {
		BehaviourExecutor<UpdateAncillaryCartBehaviour, MmbAncillaryUpdateCartRequest, MmbAncillaryUpdateCartResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(updateAncillaryCartBehaviour, request);
	}

	@Override
	public MmbAncillaryRemoveFromCartResponse removeFromCart(MmbAncillaryRemoveFromCartRequest request) {
		BehaviourExecutor<RemoveFromAncillaryCartBehaviour, MmbAncillaryRemoveFromCartRequest, MmbAncillaryRemoveFromCartResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(removeFromAncillaryCartBehaviour, request);
	}

	@Override
	public MmbAncillaryCheckOutInitResponse checkOutInit(MmbAncillaryCheckOutInitRequest request) {
		BehaviourExecutor<AncillaryCartCheckOutInitBehaviour, MmbAncillaryCheckOutInitRequest, MmbAncillaryCheckOutInitResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(ancillaryCartCheckOutInitBehaviour, request);
	}

	@Override
	public MmbAncillaryCheckOutEndResponse checkOutEnd(MmbAncillaryCheckOutEndRequest request) {
		BehaviourExecutor<AncillaryCartCheckOutEndBehaviour, MmbAncillaryCheckOutEndRequest, MmbAncillaryCheckOutEndResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(ancillaryCartCheckOutEndBehaviour, request);
	}

	@Override
	public MmbAncillaryCheckOutStatusResponse getCheckOutStatus(MmbAncillaryCheckOutStatusRequest request) {
		BehaviourExecutor<AncillaryCartCheckOutStatusInsuranceBehaviour, MmbAncillaryCheckOutStatusRequest, MmbAncillaryCheckOutStatusResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(ancillaryCartCheckOutStatusInsuranceBehaviour, request);
	}

	@Override
	public MmbAncillarySendEmailResponse sendPurchasedAncillaryEmail(MmbAncillarySendEmailRequest request) {
		BehaviourExecutor<SendEmailForPurchasedAncillaryBehaviour, MmbAncillarySendEmailRequest, MmbAncillarySendEmailResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(sendPurchasedAncillaryEmailBehaviour, request);
	}

	@Override
	public MmbCheckFrequentFlyerCodesResponse checkFrequentFlyerCodes(MmbCheckFrequentFlyerCodesRequest request) {
		BehaviourExecutor<CheckFrequentFlyerCodesBehaviour, MmbCheckFrequentFlyerCodesRequest, MmbCheckFrequentFlyerCodesResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(checkFrequentFlyerCodesBehaviour, request);
	}

	@Override
	public SapInvoiceResponse callSAPService(SapInvoiceRequest request) {
		BehaviourExecutor<GenerateSapInvoiceBehaviour, SapInvoiceRequest, SapInvoiceResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(generateSapInvoiceBehaviour, request);
	}

	@Override
	public CrossSellingsResponse getMmbCrossSellings(MmbCrossSellingsRequest request) {
		BehaviourExecutor<GetMmbCrossSellingsBehaviour, MmbCrossSellingsRequest, CrossSellingsResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(getMmbCrossSellingsBehaviour, request);
	}

	@Override
	public CrossSellingsResponse getCrossSellingsByPrenotation(CrossSellingsRequest request) {
		BehaviourExecutor<GetCrossSellingsByPrenotationBehaviour, CrossSellingsRequest, CrossSellingsResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(getCrossSellingsByPrenotationBehaviour, request);
	}

	@Override
	public MmbSendEmailResponse sendMail(MmbSendEmailRequest request) {
		BehaviourExecutor<SendEmailBehaviour, MmbSendEmailRequest, MmbSendEmailResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(sendEmailBehaviour, request);
	}
	
	@Override
	public GetSabrePNRResponse getSabrePNR(GetSabrePNRRequest request) {
		BehaviourExecutor<GetSabrePNRBehaviour, GetSabrePNRRequest, GetSabrePNRResponse> executor = new BehaviourExecutor<>();
		return executor.executeBehaviour(getSabrePNRBehaviour, request);
	}

	@Override
	public RetrieveMmbAncillaryCatalogRequest buildCatalogRequestFromMmbRouteData(MmbRequestBaseInfoData baseInfo, String client, 
			String language, String machineName, String market, String pnr, MmbRouteData routeData) {

		RetrieveMmbAncillaryCatalogRequest ancillaryCatalogRequest = new RetrieveMmbAncillaryCatalogRequest();

		ancillaryCatalogRequest.setBaseInfo(baseInfo);
		ancillaryCatalogRequest.setClient(client);
		ancillaryCatalogRequest.setLanguage(language);
		ancillaryCatalogRequest.setMachineName(machineName);
		ancillaryCatalogRequest.setMarket(market);
		ancillaryCatalogRequest.setPnr(pnr);

		List<MmbAncillaryFlightData> flights = new ArrayList<MmbAncillaryFlightData>();
		List<MmbAncillaryPassengerData> passengers = new ArrayList<MmbAncillaryPassengerData>();
		Map<Integer, String> lastCouponNumberByPassengerId = new HashMap<>();
		int flightCounter = 0;
		boolean isFirstRouteFlight = false;
		MmbFlightData previousMmbFlight = null;
		
		for (MmbFlightData mmbFlight : routeData.getFlights()) {
			flightCounter++;
			int passengerCounter = 0;
			
			if (previousMmbFlight == null || 
					!previousMmbFlight.getType().equals(mmbFlight.getType()) ||
					(previousMmbFlight.getRouteId() == null && mmbFlight.getRouteId() != null) ||
					(!previousMmbFlight.getRouteId().equals(mmbFlight.getRouteId()))) {
				isFirstRouteFlight = true;
			} else {
				isFirstRouteFlight = false;
			}
			
			MmbAncillaryFlightData ancillaryFlight = new MmbAncillaryFlightData();
			ancillaryFlight.setCarrier(mmbFlight.getCarrier());
			ancillaryFlight.setDepartureDate(mmbFlight.getDepartureDateTime());
			ancillaryFlight.setDestination(mmbFlight.getTo().getCode());

			switch (mmbFlight.getLegType()) {
				case DOM:
					ancillaryFlight.setFlightType(MmbFlightTypeEnum.DOMESTIC);
					break;
				case INZ:
					ancillaryFlight.setFlightType(MmbFlightTypeEnum.INTERNATIONAL);
					break;
				case INC:
					ancillaryFlight.setFlightType(MmbFlightTypeEnum.INTERCONTINENTAL);
					break;
				default:
					ancillaryFlight.setFlightType(MmbFlightTypeEnum.DOMESTIC);
			}

			ancillaryFlight.setId(flightCounter);
			ancillaryFlight.setNumber(mmbFlight.getFlightNumber());
			ancillaryFlight.setOperationalFlightCarrier(mmbFlight.getOperatingCarrier());
			ancillaryFlight.setOperationalFlightNumber("");
			ancillaryFlight.setOrigin(mmbFlight.getFrom().getCode());
			ancillaryFlight.setReferenceNumber(flightCounter);

			for (MmbPassengerData passengerData : mmbFlight.getPassengers()) {
				passengerCounter++;
				int eticketIndex = 0;
				if (flightCounter == 1) {
					MmbAncillaryPassengerData ancillaryPassenger = new MmbAncillaryPassengerData();
					ancillaryPassenger.setId(passengerCounter);
					ancillaryPassenger.setLastName(passengerData.getLastName());
					ancillaryPassenger.setName(passengerData.getName());
					ancillaryPassenger.setReferenceNumber(passengerCounter);
					ancillaryPassenger.setEtickets(new ArrayList<MmbAncillaryEticketData>());
					passengers.add((passengerCounter - 1), ancillaryPassenger);
				}
				if ((flightCounter % 4) == 1) {
					eticketIndex = Math.floorDiv(flightCounter, 4);
					MmbAncillaryEticketData eticket = new MmbAncillaryEticketData();
					eticket.setPnr(mmbFlight.getPnr());
					if (passengerData.getEticket() != null && passengerData.getEticket().length() > 0) {
						eticket.setNumber(passengerData.getEticket().substring(0, passengerData.getEticket().indexOf("C")));
					} else {
						eticket.setNumber("");
					}
					eticket.setCoupons(new ArrayList<MmbAncillaryCouponData>());
					passengers.get(passengerCounter - 1).getEtickets().add(eticketIndex, eticket);
				}
				MmbAncillaryCouponData couponData = new MmbAncillaryCouponData();
				// su indicazione IT Alitalia, il campo significativo per il baggage allowance
				// è quello ricevuto a livello di mmb flight, non quello di ciascun passenger
				couponData.setBaggageAllowance(mmbFlight.getBaggageAllow());
				couponData.setCabin(mmbFlight.getCabin());
				couponData.setChild(passengerData.getType() == PassengerTypeEnum.CHILD);
				couponData.setFareClass(mmbFlight.getFareClass());
				couponData.setFlightId(flightCounter);
				couponData.setFlightNumber(0);
				if (isFirstRouteFlight) {
					couponData.setInboundCouponNumber("");
				} else {
					String inboundCouponNumber = lastCouponNumberByPassengerId.get(passengerCounter - 1);
					couponData.setInboundCouponNumber(inboundCouponNumber);
				}
				couponData.setInfant(passengerData.getType() == PassengerTypeEnum.INFANT);
				couponData.setInfantAccompanist(!couponData.isInfant() && (passengerData.getLinkType() == MmbLinkTypeEnum.INFANT_ACCOMPANIST));
				couponData.setNumber(passengerData.getEticket());
				couponData.setPassengerNumber(0);
				couponData.setSpecialPassenger(false);
				passengers.get(passengerCounter - 1).getEtickets().get(eticketIndex).getCoupons().add(couponData);
				lastCouponNumberByPassengerId.put(passengerCounter - 1, couponData.getNumber());
			}
			flights.add(ancillaryFlight);
			previousMmbFlight = mmbFlight;
		}
		ancillaryCatalogRequest.setFlights(flights);
		ancillaryCatalogRequest.setPassengers(passengers);
		ancillaryCatalogRequest.setSessionId(baseInfo.getSessionId());

		return ancillaryCatalogRequest;
	}
	
	/**
	 * It obtains a list of directFligth for the current response
	 * @param response
	 */
	private void searchForABus(RetriveMmbPnrInformationResponse response) {
		boolean isBusCarrier = false;
		
		if (!alitaliaWebComponentCommon.getMmbBusEnabled()) {
			return;
		}
		
		if (response != null) {
			List<MmbRouteData> routes = response.getRoutesList();
			if (routes != null) {
				for (MmbRouteData route : routes) {
					if (manageMmbFlightDataForBus(route.getFlights())) {
						isBusCarrier = true;
					}
				}
			}
			response.setBusCarrier(isBusCarrier);
		}
	}
	
	/**
	 * It sets the bus attribute of a DirectFlightData
	 * according to its fligthNumber and the range provided by configuration 
	 * @param flightList
	 */
	private boolean manageMmbFlightDataForBus(List<MmbFlightData> flightList) {
		boolean isBusCarrier = false;
		if (flightList != null) {
			for (MmbFlightData flight : flightList) {
				int fligthNumber = -1;
				try {
					fligthNumber = Integer.parseInt(flight.getFlightNumber());
				} catch (NumberFormatException e) {
					logger.error("Error converting fligthNumber in Integer: ", e);
				}
				boolean bus = alitaliaWebComponentCommon.isBusNumber(fligthNumber);
				boolean fromBusStation = false;
				boolean toBusStation = false;
				if (bus) {
					isBusCarrier = true;
					fromBusStation = alitaliaWebComponentCommon.isBusStation(flight.getFrom().getCode());
					toBusStation = alitaliaWebComponentCommon.isBusStation(flight.getTo().getCode());
				}
				flight.setBus(bus);
				flight.setFromBusStation(fromBusStation);
				flight.setToBusStation(toBusStation);
			}
		}
		return isBusCarrier;
	}

}
