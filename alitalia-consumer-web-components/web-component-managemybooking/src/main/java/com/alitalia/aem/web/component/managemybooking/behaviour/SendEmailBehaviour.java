package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MmbSendEmailRequest;
import com.alitalia.aem.common.messages.home.MmbSendEmailResponse;
import com.alitalia.aem.service.api.home.MmbMailService;

@Component(immediate=true, metatype=false)
@Service(value=SendEmailBehaviour.class)
public class SendEmailBehaviour extends Behaviour<MmbSendEmailRequest, MmbSendEmailResponse> {

	@Reference
	private MmbMailService mmbMailService;

	@Override
	protected MmbSendEmailResponse executeOrchestration(MmbSendEmailRequest request) {
		if (request == null)
			throw new IllegalArgumentException("MmbSendEmailRequest is null.");
		return mmbMailService.sendMail(request);
	}

}
