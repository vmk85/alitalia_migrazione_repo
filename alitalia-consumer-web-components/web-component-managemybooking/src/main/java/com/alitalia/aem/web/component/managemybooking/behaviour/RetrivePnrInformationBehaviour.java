package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationRequest;
import com.alitalia.aem.common.messages.home.RetriveMmbPnrInformationResponse;
import com.alitalia.aem.service.api.home.MmbSearchService;

@Component(immediate = true, metatype = false)
@Service(value = RetrivePnrInformationBehaviour.class)
public class RetrivePnrInformationBehaviour extends Behaviour<RetriveMmbPnrInformationRequest, RetriveMmbPnrInformationResponse> {

	@Reference
	private MmbSearchService mmbSearchService;
	
	@Override
	public RetriveMmbPnrInformationResponse executeOrchestration(RetriveMmbPnrInformationRequest request) {
		if (request == null)
			throw new IllegalArgumentException("RetriveMmbPnrInformationRequest is null.");
		return mmbSearchService.retrivePnrInformation(request);
	}

}
