package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryAddToCartResponse;
import com.alitalia.aem.service.api.home.MmbAncillaryService;

@Component(immediate=true, metatype=false)
@Service(value=AddToAncillaryCartBehaviour.class)
public class AddToAncillaryCartBehaviour extends Behaviour<MmbAncillaryAddToCartRequest, MmbAncillaryAddToCartResponse> {

	@Reference
	private MmbAncillaryService mmbAncillaryService;

	@Override
	protected MmbAncillaryAddToCartResponse executeOrchestration(MmbAncillaryAddToCartRequest request) {
		if (request == null)
			throw new IllegalArgumentException("MmbAncillaryAddToCartRequest is null.");
		return mmbAncillaryService.addToCart(request);
	}

}
