package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.CrossSellingsResponse;
import com.alitalia.aem.common.messages.home.MmbCrossSellingsRequest;
import com.alitalia.aem.service.api.home.MmbTicketService;

@Component(immediate=true, metatype=false)
@Service(value=GetMmbCrossSellingsBehaviour.class)
public class GetMmbCrossSellingsBehaviour extends Behaviour<MmbCrossSellingsRequest, CrossSellingsResponse> {

	@Reference
	private MmbTicketService mmbTicketService;

	@Override
	protected CrossSellingsResponse executeOrchestration(MmbCrossSellingsRequest request) {
		if (request == null)
			throw new IllegalArgumentException("MmbCrossSellingsRequest is null.");
		return mmbTicketService.getCrossSellings(request);
	}

}
