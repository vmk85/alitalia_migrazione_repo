package com.alitalia.aem.web.component.managemybooking.behaviour;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;

import com.alitalia.aem.common.component.behaviour.Behaviour;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutEndRequest;
import com.alitalia.aem.common.messages.home.MmbAncillaryCheckOutEndResponse;
import com.alitalia.aem.service.api.home.MmbAncillaryService;

@Component(immediate=true, metatype=false)
@Service(value=AncillaryCartCheckOutEndBehaviour.class)
public class AncillaryCartCheckOutEndBehaviour extends Behaviour<MmbAncillaryCheckOutEndRequest, MmbAncillaryCheckOutEndResponse> {

	@Reference
	private MmbAncillaryService mmbAncillaryService;

	@Override
	protected MmbAncillaryCheckOutEndResponse executeOrchestration(MmbAncillaryCheckOutEndRequest request) {
		if (request == null)
			throw new IllegalArgumentException("MmbAncillaryCheckOutEndRequest is null.");
		return mmbAncillaryService.checkOutEnd(request);
	}

}
